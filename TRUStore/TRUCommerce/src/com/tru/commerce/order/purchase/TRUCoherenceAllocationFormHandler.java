/**
 * 
 */
package com.tru.commerce.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;

import com.tru.commerce.inventory.TRUCoherenceInventoryManager;

import atg.commerce.inventory.InventoryException;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * @author PA
 * @version 1.0
 */
public class TRUCoherenceAllocationFormHandler extends GenericFormHandler {
	
	/** The Constant INVENTORY_ALLOCATION_FAILURE. */
	private static final String INVENTORY_ALLOCATION_FAILURE = "Inventory Allocation Failure";

	/** The Constant INT_ONE. */
	private static final int INT_ONE = 1;

	/** The Coherence inventory manager. */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	
	/** The Catalog ref id. */
	private String mItemName;
	
	/** The Quantity. */
	private long mQuantity;
	
	/** The Location id. */
	private String mFacilityName;
	
	/** property to hold mRestSuccessURL. */
	private String mRestSuccessURL;
	
	/** property to hold mRestErrorURL. */
	private String mRestErrorURL;
	
	/**
	 * @return the restSuccessURL
	 */
	public String getRestSuccessURL() {
		return mRestSuccessURL;
	}

	/**
	 * @param pRestSuccessURL the restSuccessURL to set
	 */
	public void setRestSuccessURL(String pRestSuccessURL) {
		mRestSuccessURL = pRestSuccessURL;
	}

	/**
	 * @return the restErrorURL
	 */
	public String getRestErrorURL() {
		return mRestErrorURL;
	}

	/**
	 * @param pRestErrorURL the restErrorURL to set
	 */
	public void setRestErrorURL(String pRestErrorURL) {
		mRestErrorURL = pRestErrorURL;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return mItemName;
	}

	/**
	 * @param pItemName the itemName to set
	 */
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}

	/**
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}

	/**
	 * @param pQuantity the quantity to set
	 */
	public void setQuantity(long pQuantity) {
		mQuantity = pQuantity;
	}

	/**
	 * @return the facilityName
	 */
	public String getFacilityName() {
		return mFacilityName;
	}

	/**
	 * @param pFacilityName the facilityName to set
	 */
	public void setFacilityName(String pFacilityName) {
		mFacilityName = pFacilityName;
	}

	/**
	 * @return the coherenceInventoryManager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * @param pCoherenceInventoryManager the coherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 * Handle coherence qty reduction.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean handleCoherenceAllocation(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if(isLoggingDebug()){
			logDebug("@method handleCoherenceAllocation -- START");
			vlogDebug("ItemName - {0}, FacilityName - {1}, Quantity - {2}", getItemName(),getFacilityName(),getQuantity());
		}
			
		try {
			int response = getCoherenceInventoryManager().purchase(getItemName(), getQuantity(), getFacilityName());
			if(response < INT_ONE){
				addFormException(new DropletException(INVENTORY_ALLOCATION_FAILURE));
			}
		} catch (InventoryException e) {
			if(isLoggingError()){
				logError("InventoryException in handleCoherenceAllocation",e);
			}
		}
		if(isLoggingDebug()){
			logDebug("@method handleCoherenceAllocation -- END");
		}
		return checkFormRedirect(getRestSuccessURL(), getRestErrorURL(), pRequest, pResponse);
	}
}
