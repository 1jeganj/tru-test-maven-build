package com.tru.commerce.order.purchase;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupNotFoundException;
import atg.commerce.order.PaymentGroupOrderRelationship;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.multisite.SiteContextManager;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.endeca.infront.shaded.org.apache.commons.lang.math.NumberUtils;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardInfo;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.commerce.payment.TRUPaymentManager;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.validator.fhl.IValidator;

/**
 * @author PA
 * @version 1.0
 * 
 */
public class TRULayawayPaymentInfoFormHandler extends PurchaseProcessFormHandler {
	
	/** property to hold browserAcceptEncoding. */
	private String mBrowserAcceptEncoding;
	
	/** property to hold user session component. */
	private TRUUserSession mUserSession;
	
	/** property to hold email address in order level. */
	private String mDeviceID;
	
	/** list to hold error keys. */
	private List<String> mErrorKeyList;
	
	/** property: Reference to the BillingProcessHelper component. */
	private TRUBillingProcessHelper mBillingHelper;
	
	/** property to hold email address in order level. */
	private String mEmail;

	/** member holds the gift card number. */
	private double mPaymentAmount;
	
	/** member holds the gift card number. */
	private String mBillingAddressNickName;

	/** member holds the gift card number. */
	private String mGiftCardNumber;

	/** member holds the gift card pin. */
	private String mGiftCardPin;

	/** member holds the layaway number. */
	private String mLayawayNumber;
	
	/** property to hold OrderTools. */
	private TRUOrderTools mOrderTools;
	
	/** property to hold the mErrorHandler.*/
	private IErrorHandler mErrorHandler;
	
	/** property to hold the mValidator.*/
	private IValidator mValidator;
	
	/** property to hold ProfileManager.*/
	private TRUProfileManager mProfileManager;
	
	/**property to hold mTRUConfiguration. */
 	private TRUConfiguration mTRUConfiguration;
	
 	/**property to hold mTRUConfiguration. */
 	private TRULayawayProcessHelper mLayawayProcessHelper;
 	
 	/** property to hold ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();
	
	/** map to hold credit card info. */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();

	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();
	
 	/** member holds the payment Manager. */
 	private TRUPaymentManager mPaymentManager;
 	
 	/** member holds the gift Card Balance.  */
 	private double mGiftCardBalance;
 	
 	/** member holds the card Id. */
 	private String mCardId;
 	
 	/** member holds the mLayawaySuccessUrl. */
 	private String mLayawaySuccessUrl;
 	
 	/** member holds the mLayawayErrorUrl. */
 	private String mLayawayErrorUrl;
 	
 	/** member holds the mAmountDue. */
 	private double mAmountDue;
 	
 	/** member holds the mProcessLayawayOrderChainId. */
 	private String mProcessLayawayOrderChainId;
 	
 	/** member holds the mProcessLayawayOrderChainId. */
 	private String mLayawayOMSId;
 	
 	/** member holds the mProcessLayawayOrderChainId. */
 	private String mLayawayCustomerName;
 	
 	/** member holds the mPropertyManager. */
 	private TRUPropertyManager mPropertyManager;
 	
 	/** member holds the integrationStatusUtil. */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
 	
	/** member holds the mAddressDoctorProcessStatus. */
 	private String mAddressDoctorProcessStatus;
 	
 	/** member holds the mAddressDoctorResponseVO. */
 	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
 	
 	/** boolean property to hold if address is validated by address doctor or not. */
 	private String mAddressValidated;
 	
 	/**
	 * This method is used to get browserAcceptEncoding.
	 * @return browserAcceptEncoding String
	 */
	public String getBrowserAcceptEncoding() {
		return mBrowserAcceptEncoding;
	}

	/**
	 *This method is used to set browserAcceptEncoding.
	 *@param pBrowserAcceptEncoding - BrowserAcceptEncoding String
	 */
	public void setBrowserAcceptEncoding(String pBrowserAcceptEncoding) {
		mBrowserAcceptEncoding = pBrowserAcceptEncoding;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	/**
	 * @return the mLayawayCustomerName
	 */
	public String getLayawayCustomerName() {
		return mLayawayCustomerName;
	}

	/**
	 * @param pLayawayCustomerName the mLayawayCustomerName to set
	 */
	public void setLayawayCustomerName(String pLayawayCustomerName) {
		this.mLayawayCustomerName = pLayawayCustomerName;
	}

	/**
	 * @return the mLayawayOMSId
	 */
	public String getLayawayOMSId() {
		return mLayawayOMSId;
	}

	/**
	 * @param pLayawayOMSId the mLayawayOMSId to set
	 */
	public void setLayawayOMSId(String pLayawayOMSId) {
		this.mLayawayOMSId = pLayawayOMSId;
	}

	/**
	 * This method is used for validating user submitted payment amount value.
	 * @throws ServletException servlet exception
	 * @throws IOException i/o exception
	 */
	protected void preOrderPaymentAmount() throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.preOrderPaymentAmount method");
		}
		if(getPaymentAmount()==TRUConstants.DOUBLE_ZERO || getPaymentAmount() > getAmountDue()){
			if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_LAYAWAY_LAYAWAYPAYMENTAMOUNT)){
				setChannelError(TRUErrorKeys.TRU_ERROR_LAYAWAY_LAYAWAYPAYMENTAMOUNT);
			}else{
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_INVALID_PAYMENT_AMOUNT, null);
	 			getErrorHandler().processException(mVe, this);	
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentFormHandler.preOrderPaymentAmount method");
		}
	}
	
	/**
	 * This method is used to create a new layaway order and assign user submitted payment amount in it.
	 * @param pRequest 
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @return boolean true/false
	 */
	public boolean handleOrderPaymentAmount(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) 
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.handleOrderPaymentAmount method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "handleOrderPaymentAmount";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();			
				preOrderPaymentAmount();
				
				if(getFormError()){
					return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
				}
				double layawayDueAmount = TRUConstants.DOUBLE_ZERO;
				TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
				TRULayawayOrderImpl order = orderManager.createLayawayOrder(getProfile().getRepositoryId());
				layawayDueAmount = (getAmountDue() - getPaymentAmount());
				order.setPaymentAmount(getPaymentAmount());
				order.setRemainingAmount(getPaymentAmount());
				order.setLayawayOMSOrderId(getLayawayOMSId());
				order.setLayawayCustName(getLayawayCustomerName());
				order.setLayawayDueAmount(layawayDueAmount);
				((TRUOrderHolder) getShoppingCart()).setLayawayOrder(order);
				
			} catch (CommerceException commEx) {
				if(isLoggingError()) {
					logError("CommerceException in @Class::TRULayawayPaymentFormHandler::@method::handleOrderPaymentAmount()",commEx);
				}
				if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_LAYAWAY_LAYAWAYPAYMENTAMOUNT)){
					setChannelError(TRUErrorKeys.TRU_ERROR_LAYAWAY_LAYAWAYPAYMENTAMOUNT);
				}else{
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_INVALID_PAYMENT_AMOUNT, null);
		 			getErrorHandler().processException(mVe, this);	
				}
			}  finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentFormHandler.handleOrderPaymentAmount method");
		}
		return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
	}
	
	/**
	 * This method commits the layaway order.
	 * @param pRequest request
	 * @param pResponse response
	 * @return boolean true/false
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public boolean handleCommitLayawayOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.handleCommitLayawayOrder method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRULayawayPaymentFormHandler.handleCommitLayawayOrder";
		boolean isEnableAddressDoctor = false;
		String enteredBy = null;
		String enteredLocation = null;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(myHandleMethod);
			}
			
			try {
				tr = ensureTransaction();
				if (getUserLocale() == null){
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
				
				if (order == null || order.getPaymentAmount() == TRUConstants.DOUBLE_ZERO) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_NO_PAYMENT_SUBMITTED, null);
					getErrorHandler().processException(mVe, this);
					return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
				}
				synchronized (order) {
					preCommitLayawayOrder(pRequest, pResponse);
					TRULayawayProcessHelper layawayProcessHelper = getLayawayProcessHelper();
					if(getFormError()){
						return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
					}
					boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
					if(SiteContextManager.getCurrentSite() != null){
						isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
					}
					
					if(TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry())){
						isAddressValidated = addressDoctorValidation(isAddressValidated,isEnableAddressDoctor);
					}
					
					if(isRestService()){
						order.setSourceOfOrder(getTRUConfiguration().getMobileSourceOrderType());
					}else if(layawayProcessHelper.getPurchaseProcessHelper().isSOSOrder(pRequest)){
						enteredBy = layawayProcessHelper.getPurchaseProcessHelper().getEnteredByValue(pRequest);
						enteredLocation = layawayProcessHelper.getPurchaseProcessHelper().getEnteredLocation(pRequest);
						if(StringUtils.isNotBlank(enteredBy)){
							order.setEnteredBy(enteredBy);
						}
						if(StringUtils.isNotBlank(enteredLocation)){
							order.setEnteredLocation(enteredLocation);
						}
						order.setSourceOfOrder(getTRUConfiguration().getSosSourceOrderType());
					}else{
						order.setSourceOfOrder(getTRUConfiguration().getWebSourceOrderType());
					}
					
					if (!getFormError() && (isAddressValidated || !TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressInputFields().getCountry()))) {
						commitLayawayOrder(pRequest, pResponse);
					}
					
					if(getFormError()){
						return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
					}
				}
				if (isLoggingDebug()) { 
					logDebug("END TRULayawayPaymentInfoFormHandler:: handleCommitLayawayOrder method");
				}
				return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);

			} finally {
				if (tr != null){
					commitTransaction(tr);	
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(myHandleMethod);
				}
			}
		} else {
			return false;
		}
	}
	
	/**
	 * AD service.
	 *
	 * @param pAddressValidated - address validated
	 * @param pEnableAddressDoctor - enable address doctor
	 * @return isAddressValidated - boolean
	 */
	private boolean addressDoctorValidation(boolean pAddressValidated, boolean pEnableAddressDoctor) {
		
		boolean isAddressValidated = pAddressValidated;
		boolean isEnableAddressDoctor =	pEnableAddressDoctor;	
		
		// Validate address with AddressDoctor Service
		if (!isAddressValidated && isEnableAddressDoctor) {
			String addressStatus = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).validateAddress(getAddressInputFields(), getOrder(), getProfile(), TRUConstants.LAYAWAY,TRUConstants.ADD_ADDRESS);
			setAddressDoctorProcessStatus(addressStatus);
			setAddressDoctorResponseVO(((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO());
			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
				isAddressValidated = Boolean.TRUE;
			} else if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
				String derivedStatus = getAddressDoctorResponseVO().getDerivedStatus();
				if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
					isAddressValidated = Boolean.TRUE;
					ContactInfo contactInfo = getAddressInputFields();
					if(!StringUtils.isBlank(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getAddress1())){
					contactInfo.setAddress1(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getAddress1());}
					if(!StringUtils.isBlank(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getAddress2())){
					contactInfo.setAddress2(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getAddress2());}
					if(!StringUtils.isBlank(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getCity())){
					contactInfo.setCity(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getCity());}
					if(!StringUtils.isBlank(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getState())){
					contactInfo.setState(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getState());}
					if(!StringUtils.isBlank(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getPostalCode())){
					contactInfo.setPostalCode(getAddressDoctorResponseVO().getAddressDoctorVO().get(0).getPostalCode());
					}
					setAddressInputFields(contactInfo);
				}
			}
			else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				setAddressDoctorResponseVO(addressDoctorResponseVO);
			}else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				setAddressDoctorResponseVO(addressDoctorResponseVO);
			}
		} else if (!isEnableAddressDoctor && !isAddressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			setAddressDoctorResponseVO(addressDoctorResponseVO);
		}
		
		return isAddressValidated;
	}

	/**
	 * commit Payment.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	private void commitLayawayOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("START TRULayawayPaymentInfoFormHandler:: commitLayawayOrder method");
		}
		TRUOrderHolder orderHolder = (TRUOrderHolder) getShoppingCart();
		PipelineResult result;
		TRULayawayOrderImpl layawayOrder = orderHolder.getLayawayOrder();
		try {
			getLayawayProcessHelper().populateBillingAddress(layawayOrder, getAddressInputFields(),getEmail());
			double remainingAmount = layawayOrder.getRemainingAmount();
			if(remainingAmount > TRUConstants.DOUBLE_ZERO) {
				updateCreditCardToOrder();
			} else{
				removePaymentGroup(layawayOrder);
			}

			String processLayawayChainId = getProcessLayawayOrderChainId();
			if (processLayawayChainId == null) {
				result = getOrderManager().processOrder(layawayOrder, getProcessOrderMap(getUserLocale()));
			} else {
				result = getOrderManager().processOrder(layawayOrder, processLayawayChainId, getProcessOrderMap(getUserLocale()));
			}
			if ((!(processPipelineErrors(result))) && (!(isTransactionMarkedAsRollBack())) && (getShoppingCart() != null)) {
				((TRUOrderHolder)getShoppingCart()).setLastLayawayOrder(layawayOrder);
				((TRUOrderHolder)getShoppingCart()).setLayawayOrder(null);
				
				String browserAcceptEncode = TRUConstants.EMPTY;
				if(getBrowserAcceptEncoding() != null){
					browserAcceptEncode=getBrowserAcceptEncoding();
				}
				((TRUOrderManager)getOrderManager()).updateLayawayOrderWithFraudParameters(pRequest, layawayOrder, getProfile(), getDeviceID(), getUserSession().getUserSessionStartTime(),browserAcceptEncode,isRestService());
			}
		} catch (CommerceException exc) {
			if (isLoggingError()) {
				logError("CommerceException occured during committing @Class::TRULayawayPaymentFormHandler::@method::commitLayawayOrder() " + 
						"for order Id : " + layawayOrder.getId(), exc);
			}
			processException(exc, TRUCheckoutConstants.ERROR_COMMITTING_ORDER, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END TRULayawayPaymentInfoFormHandler:: commitLayawayOrder method");
		}
	}
	
	/**
	 * remove Payment Group.
	 *
	 * @param pLayawayOrder the order
	 * @throws RelationshipNotFoundException the servlet exception
	 * @throws InvalidParameterException invalidparameter
	 * @throws PaymentGroupNotFoundException paymentgroupnotfound
	 */
	private void removePaymentGroup(Order pLayawayOrder) throws RelationshipNotFoundException, InvalidParameterException, 
		PaymentGroupNotFoundException {
		//remove credit card from payment group if remainingAmount is Zero
		List<PaymentGroup> paymentGroups = pLayawayOrder.getPaymentGroups();
		/*for (PaymentGroup pg : paymentGroups)*/ 
		for (int i = 0; i < paymentGroups.size(); i++) {
			PaymentGroup pg=paymentGroups.get(i);
			if (pg instanceof TRUCreditCard) {
				List<Relationship> paymentRelationships = pLayawayOrder.getPaymentGroupRelationships();
				for(Relationship paymentRelationship : paymentRelationships) {
					if (paymentRelationship instanceof PaymentGroupOrderRelationship) {
						String relpayId = ((PaymentGroupOrderRelationship)paymentRelationship).getPaymentGroup().getId();
						if (relpayId.equals(pg.getId())) {
							pLayawayOrder.removeRelationship(paymentRelationship.getId());
							pLayawayOrder.removePaymentGroup(pg.getId());
						}
					}
				}
			}
		}
	
	}
	
	/**
	 * Pre Commit payment.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void preCommitLayawayOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("START TRULayawayPaymentInfoFormHandler::preCommitLayawayOrder method");
		}
		TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		
		if(order != null){
				validateBillingAddress();
			if(order.getRemainingAmount() > TRUConstants.DOUBLE_ZERO){
				validateCreditCardFields();
			}
		}
		if (isLoggingDebug()) {
			logDebug("END TRULayawayPaymentInfoFormHandler::preCommitLayawayOrder method");
		}
	}
	
	/**
	 * This method is used to create the process order map.
	 * @param pLocale locale
	 * @return poMap HashMap
	 * @throws CommerceException commercexception
	 */
	@SuppressWarnings("rawtypes")
	private HashMap getProcessOrderMap(Locale pLocale) throws CommerceException {
		HashMap poMap = getOrderManager().getProcessOrderMap(pLocale, null);
		return poMap;
	}
	
	/**
	 * validate billing address.
	 *
	 * @throws ServletException the servlet exception
	 */
	private void validateBillingAddress() throws ServletException{
		if (isLoggingDebug()) {
			logDebug("START TRULayawayPaymentInfoFormHandler:: validateBillingAddress method");
		}
		TRUProfileTools profileTools = (TRUProfileTools) getOrderTools().getProfileTools();
		String lEmail = TRUConstants.EMPTY_STRING; 
		if(getProfile().isTransient()){
			lEmail = getEmail().trim();
		} else {
			lEmail = ((String) getProfile().getPropertyValue(profileTools.getPropertyManager().getEmailAddressPropertyName())).trim();
			setEmail(lEmail);
		}
		
		//Method to Trim the input field values.
		 trimContactInfoValues(getAddressInputFields());
		
		if (((TRUOrderManager) getOrderManager()).isAnyAddressFieldEmpty(getAddressInputFields()) || StringUtils.isEmpty(lEmail)) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_ADDRESS_MISS_INFO, null);
			getErrorHandler().processException(mVe, this);
		} 
			
		if (isLoggingDebug()) {
			logDebug("END TRULayawayPaymentInfoFormHandler:: validateBillingAddress method");
		}

	}


	/**
	 * Commit order from payment.
	 *
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws CommerceException the commerce exception
	 */
	private void updateCreditCardToOrder() throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("START TRULayawayPaymentInfoFormHandler:: commitOrderFromPayment method");
		}
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) orderManager.getPaymentGroupManager();
		TRULayawayOrderImpl layawayOrder = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		
		//Getting Credit card
		TRUCreditCard creditCard = (TRUCreditCard) orderManager.getCreditCard(layawayOrder);
		if (creditCard == null) {
			creditCard = (TRUCreditCard) paymentGroupManager.createPaymentGroup();
			paymentGroupManager.addPaymentGroupToOrder(layawayOrder, creditCard);
			orderManager.addRemainingOrderAmountToPaymentGroup(layawayOrder, creditCard.getId());
		}
	
		// updating order credit card
		getLayawayProcessHelper().updateCreditCard(layawayOrder,creditCard, getCreditCardInfoMap(), getAddressInputFields());
		creditCard.setBillingAddressNickName(getBillingAddressNickName());
		creditCard.setAmount(layawayOrder.getRemainingAmount());
		creditCard.setCurrencyCode(TRUConstants.CURRENCY_USD);
		
		PaymentGroupOrderRelationship paymentGroupOrderRelationship = getPaymentGroupManager().
				getPaymentGroupOrderRelationship(layawayOrder, creditCard.getId());
		paymentGroupOrderRelationship.setAmount(layawayOrder.getRemainingAmount());
			
		if (isLoggingDebug()) {
			logDebug("END TRULayawayPaymentInfoFormHandler:: commitOrderFromPayment method");
		}
	}
	
	/**
	 * This method is used to validate credit card fields.
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	private void validateCreditCardFields() throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentInfoFormHandler.validateCreditCardFields method");
		}
		TRUPropertyManager propertyManager = getPropertyManager();
		if(isRestService() && getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(getTRUConfiguration()
				.getCardNumberPLCC())){
			getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),getTRUConfiguration()
					.getExpMonthAdjustmentForPLCC());
			getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),getTRUConfiguration()
					.getExpYearAdjustmentForPLCC());
		}
		
		//Trim all the credit card Map values.
		getLayawayProcessHelper().trimMapValues(getCreditCardInfoMap());
		
		if(getLayawayProcessHelper().isAllCreditCardFieldEmpty(getCreditCardInfoMap())){
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_INVALID_PAYMENT_SUBMITTED, null);
			getErrorHandler().processException(mVe, this);
		}else if (((TRUOrderManager) getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap())) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_PAYMENT_MISS_INFO,null);
			getErrorHandler().processException(mVe, this);
		}
		
		validateCreditCard();
		
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentInfoFormHandler.validateCreditCardFields method");
		}
	}
	
	/**
	 * This method validates the credit card input details entered by the user.
	 * 
	 * @throws ServletException ServletException
	 */
	private void validateCreditCard() throws ServletException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingInfoFormHandler:validateCreditCard method");
		}
		int validationResult = getOrderTools().validateCreditCard(getCreditCardInfoMap());
		if (validationResult != 0) {
			switch (validationResult) {
			case TRUCommerceConstants.CARD_EXPIRED:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_HAS_INVALID_CHARS:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_HAS_INVALID_CHARS,null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_DOESNT_MATCH_TYPE:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_DOESNT_MATCH_TYPE,null);
				break;
			case TRUCommerceConstants.CARD_LENGTH_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_LENGTH_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_INFO_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_INFO_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_EXP_DATE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_EXP_DATE_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_TYPE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_TYPE_NOT_VALID, null);
				break;
			}
			getErrorHandler().processException(mVe, this);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUBillingInfoFormHandler:validateCreditCard method");
		}
	}
	
	
	/**
	 * This method is used for validating user Gift Card.
	 * @param pRequest request
	 * @param pResponse response
	 * @throws ServletException servletexception
	 * @throws IOException i/o exception
	 */
	protected void preApplyGiftCard(DynamoHttpServletRequest pRequest,
		DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.preApplyGiftCard method");
		}
		try {
			TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
			
			if (order == null || order.getPaymentAmount() == TRUConstants.DOUBLE_ZERO) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_NO_PAYMENT_SUBMITTED, null);
				getErrorHandler().processException(mVe, this);
			}
			
			if (!getFormError()) {
				if (order.getRemainingAmount() <= TRUConstants.DOUBLE_ZERO) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_INSUFFICIENT_ORDER_AMOUNT, null);
					getErrorHandler().processException(mVe, this);
				} else {
					checkOrderPaymentGroups(order);
				}
			}
			
			if (!getFormError()) {
				validateGiftCard(getGiftCardNumber(), getGiftCardPin());
			}

		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError("CommerceException occured in  preApplyGiftCard @Class::TRULayawayPaymentFormHandler::@method::preApplyGiftCard()", ce);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentFormHandler.preApplyGiftCard method");
		}
	}

	/**
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @return boolean true/false
	 */
	public boolean handleApplyGiftCard(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentInfoFormHandler/handleApplyGiftCard method");
		}
		TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		String myHandleMethod = "LayawayPaymentInfoFormHandler.handleApplyGiftCard";
		RepeatingRequestMonitor l_oRepeatingRequestMonitor = getRepeatingRequestMonitor();
		if ((l_oRepeatingRequestMonitor == null) || (l_oRepeatingRequestMonitor.isUniqueRequestEntry(myHandleMethod))){
		resetFormExceptions();	
		Transaction tr = null;
		try
		{
			preApplyGiftCard(pRequest, pResponse);
			
			if(getFormError()){
				return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
			}
			processGetGiftCardBalance();
	        if(!getFormError())
	        {
	        	if(getGiftCardBalance() == TRUConstants.DOUBLE_ZERO){
		        	mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_BALANCE_ZERO, null);
		 			getErrorHandler().processException(mVe, this);
		 			return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
		        }
	        	tr = ensureTransaction();
				synchronized (order)
				{
					createAndApplyGiftCard(getGiftCardBalance(),order.getRemainingAmount());
					postApplyGiftCard();
				}
	        }
		}
		catch (CommerceException comExp) {
			if (isLoggingError()) {
				logError("Exception in TRULayawayPaymentInfoFormHandler/handleApplyGiftCard method ", comExp);
			}
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
 			getErrorHandler().processException(mVe, this);
		}
		finally {
			if (tr != null) {
				commitTransaction(tr);
			}
			if (l_oRepeatingRequestMonitor != null) {
				l_oRepeatingRequestMonitor.removeRequestEntry(myHandleMethod);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentInfoFormHandler/handleApplyGiftCard method");
		}
		}
		return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
	}
	
	/**
	 * This method is used for post activities after user applied Gift Card.
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void postApplyGiftCard() throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.postApplyGiftCard method");
		}
		TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		TRUOrderTools l_oOrderTools = (TRUOrderTools)getPaymentGroupManager().getOrderTools();
		l_oOrderTools.updateRemainingAmountInLayawayOrder(order);
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentFormHandler.postApplyGiftCard method");
		}
	}

	/**
	 * This method is used to validate gift card data.
	 * @param pGiftCardNumber giftcardnumber
	 * @param pGiftCardPin giftcardpin
	 * @return validationFailed true/false
	 * @throws ServletException ServletException
	 */
	private boolean validateGiftCard(String pGiftCardNumber, String pGiftCardPin) throws ServletException {
        boolean validationFailed = true;
        if (isLoggingDebug()) {
            logDebug("Inside into GiftCardFormHandler.validateGiftCard");
        }
        if (!getFormError()) {
        	 if(StringUtils.isBlank(pGiftCardNumber) && StringUtils.isBlank(pGiftCardPin)){
        		mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_ENTER_REQUIRED_GIFT_CARD, null);
     			getErrorHandler().processException(mVe, this);
        	 }
        	 else if(StringUtils.isBlank(pGiftCardNumber)){
        		mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_NUMBER, null);
     			getErrorHandler().processException(mVe, this);
        	 }
        	 else if(!getLayawayProcessHelper().isValidGCNumberLength(pGiftCardNumber)){
        		 if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER)){
        			 setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER);
        		 }else{
	        		 mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_GCNUMBER_INVALID_CARDNUMBER, null);
	      			 getErrorHandler().processException(mVe, this);
        		 }
             }
        	 else if(!NumberUtils.isNumber(pGiftCardNumber)) {
        		 mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_NUMBER_NUMERIC, null);
      			 getErrorHandler().processException(mVe, this);
             }
        	 else if(StringUtils.isBlank(pGiftCardPin)){
        		mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_PIN, null);
      			getErrorHandler().processException(mVe, this);
        	 }
        	 else if(!getLayawayProcessHelper().isValidGCPinLength(pGiftCardPin)) {
        		 if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_PIN_INVALID)){
        			 setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PIN_INVALID);
        		 }else{
        			 mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PIN_INVALID, null);
          			 getErrorHandler().processException(mVe, this);
        		 }
             }
        	 else if(!NumberUtils.isNumber(pGiftCardPin)){
        		 mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ENTER_GIFT_CARD_PIN_NUMERIC, null);
      			 getErrorHandler().processException(mVe, this);
             }
        }
        if (!getFormError()) {
        	validationFailed = false;
        }
        return validationFailed;
    }
	
    
    /**
     * This method is to validate gift card max limit and if same card was added already.
     *
     * @param pOrder Order.
	 * @throws ServletException servletException
	 * @throws CommerceException commerceException
     */
    @SuppressWarnings("unchecked")
	private void checkOrderPaymentGroups(TRULayawayOrderImpl pOrder) throws CommerceException, ServletException {

        if (isLoggingDebug()) {
            logDebug(" invoked TRULayawayPaymentInfoFormHandler.checkOrderPaymentGroups() method");
        }
        List<PaymentGroup> l_oPaymentGroupList = pOrder.getPaymentGroups();
		int l_iPaymentGroupSize = l_oPaymentGroupList.size();
		String l_sGiftCardNumber = getGiftCardNumber();
		PaymentGroup l_oPaymentGroup = null;
		TRUGiftCard l_oGiftCard = null;
		int l_iGiftCardSize = 0;
		String l_sGiftCardNo = null;
		// checks whether added gift card is already set as payment method.
		for (int l_iCount = 0; l_iCount < l_iPaymentGroupSize; l_iCount++) {
		    l_oPaymentGroup = l_oPaymentGroupList.get(l_iCount);
		    if (l_oPaymentGroup instanceof TRUGiftCard) {
		        l_oGiftCard = (TRUGiftCard)l_oPaymentGroup;
		        l_sGiftCardNo = l_oGiftCard.getGiftCardNumber();
		        if (l_sGiftCardNo.equals(l_sGiftCardNumber)) {
		        	mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_ALREADY_APPLIED, null);
		 			getErrorHandler().processException(mVe, this);
		        }
		        l_iGiftCardSize++;
		    }
		}
		int l_iMaxGiftCardSize = getTRUConfiguration().getMaxGiftCards();
		// checks maximum Limit of Gift cards.
		if (l_iGiftCardSize >= l_iMaxGiftCardSize) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ADD_MAXIMUM_FIVE_GIFT_CARDS, null);
			getErrorHandler().processException(mVe, this);
		}
        if (isLoggingDebug()) {
            logDebug(" end TRUGiftCardFormHandler.checkOrderPaymentGroups() method");
        }
    }
    
    /**
     * This method is used to process gift card balance.
     * @throws ServletException ServletException
     */
    private void processGetGiftCardBalance() throws ServletException 
	{
		TRUGiftCardInfo giftCardInfo = new TRUGiftCardInfo();
		TRUGiftCardStatus  giftCardStatus= null;
		giftCardInfo.setGiftCardNumber(getGiftCardNumber());
		giftCardInfo.setPin(getGiftCardPin());
		giftCardInfo.setCardHolderName(TRUCheckoutConstants.JOE_SMITH);
		giftCardInfo.setOrder(((TRUOrderHolder) getShoppingCart()).getLayawayOrder());
		giftCardStatus=getPaymentManager().getGiftCardBalance(giftCardInfo);
		if (isLoggingDebug()) {
			logDebug("giftCardStatus = "+giftCardStatus);
		}
		if(!giftCardStatus.getTransactionSuccess()) {
	        if(!StringUtils.isBlank(giftCardStatus.getErrorMessage())) 
	        {
	        	mVe.addValidationError(giftCardStatus.getErrorMessage(), null);
	 			getErrorHandler().processException(mVe, this);
	        }
		} else {
        	setGiftCardBalance(giftCardStatus.getBalanceAmount().doubleValue());	
        	if(getGiftCardBalance() == TRUConstants.DOUBLE_ZERO){
        		mVe.addValidationError(TRUErrorKeys.TRU_ERROR_GIFT_CARD_BALANCE_ZERO, null);
     			getErrorHandler().processException(mVe, this);	
        	}
        }
	}
    
    /**
     * This method is used to create payment group of type gift card.
     *
     * @param pGiftCardBalAmt gift card balance amount
     * @param pRemainingOrderAmount remaining order amount
     * @throws CommerceException throws CommerceException
     *
     */
    private void createAndApplyGiftCard(double pGiftCardBalAmt,double pRemainingOrderAmount) throws CommerceException {
        if (isLoggingDebug()) {
            logDebug("Entered into TRUGiftCardFormHandler.createGiftCard");
        }
        TRUGiftCard l_oGiftCard = (TRUGiftCard)getPaymentGroupManager().createPaymentGroup(TRUPaymentConstants.GIFT_CARD);
        if (pGiftCardBalAmt >= pRemainingOrderAmount) {
                l_oGiftCard.setAmount(pRemainingOrderAmount);
            } else {
                l_oGiftCard.setAmount(pGiftCardBalAmt);
        }
        l_oGiftCard.setRemainingBalance(pGiftCardBalAmt-l_oGiftCard.getAmount());
        l_oGiftCard.setGiftCardNumber(getGiftCardNumber());
        l_oGiftCard.setGiftCardPin(getGiftCardPin());
        l_oGiftCard.setCurrencyCode(TRUConstants.CURRENCY_USD);
        getPaymentGroupManager().addPaymentGroupToOrder(((TRUOrderHolder) getShoppingCart()).getLayawayOrder(), l_oGiftCard);
        getOrderManager().addOrderAmountToPaymentGroup(((TRUOrderHolder) getShoppingCart()).getLayawayOrder(), l_oGiftCard.getId(),
        		l_oGiftCard.getAmount());
        if (isLoggingDebug()) {
            logDebug("Exit TRUGiftCardFormHandler.createGiftCard");
        }
    }
    
	/**
	 * removes gift card from layaway order.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @return boolean true/false
	 */
	public boolean handleRemoveGiftCard(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUGiftCardFormHandler/handleRemoveGiftCard method");
		}
		String myHandleMethod = "GiftCardFormHandler.handleRemoveGiftCard";
		TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		RepeatingRequestMonitor l_oRepeatingRequestMonitor = getRepeatingRequestMonitor();
		if ((l_oRepeatingRequestMonitor == null) || (l_oRepeatingRequestMonitor.isUniqueRequestEntry(myHandleMethod))) {
         	if (isLoggingDebug()) {
    			logDebug("CardId is "+getCardId());
    		}	
		Transaction tr = null;
		resetFormExceptions();
		tr = ensureTransaction();
		try {			
			synchronized (order) {
				getOrderManager().getPaymentGroupManager().removePaymentGroupFromOrder(order, getCardId());
				postRemoveGiftCard();
			}
		}
		catch (CommerceException comExp) {
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
 			getErrorHandler().processException(mVe, this);
			if (isLoggingError()) {
			logError("Exception in TRUGiftCardFormHandler/handleRemoveGiftCard method ", comExp);
			}
		}
		finally {
			if (tr != null) {
				commitTransaction(tr);
			}
			if (l_oRepeatingRequestMonitor != null) {
				l_oRepeatingRequestMonitor.removeRequestEntry(myHandleMethod);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUGiftCardFormHandler/handleRemoveGiftCard method");
		}
		}
		return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
	}
	
	/**
     * This method is used to remove all gift card payment groups from layaway order if user confirms to edit payment amount.
     * @param pRequest DynamoHttpServletRequest
     * @param pResponse DynamoHttpServletResponse
     * @return boolean
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
     public boolean handleRemoveAllGiftCard(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse)
    		 throws ServletException,IOException {
            if (isLoggingDebug()) {
                   logDebug("Start of TRULayawayPaymentFormHandler.handleRemoveAllGiftCard method");
            }
            RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
            String myHandleMethod = "handleOrderPaymentAmount";
            if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
                   Transaction tr = null;
                   tr = ensureTransaction();
                   resetFormExceptions();
                   TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
                   TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
                   if(order==null){
                	   mVe.addValidationError(TRUErrorKeys.TRU_ERROR_LAYAWAY_NO_ORDER_IN_ACCOUNT, null);
            		   getErrorHandler().processException(mVe, this); 
                	   return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
                   }
                   try{
                         synchronized (order) {
                        	 orderManager.removeAllGiftCardFromOrder(order);
                        	 order.setPaymentAmount(TRUConstants.DOUBLE_ZERO);
                         }
                   } catch (CommerceException comExp) {
                         mVe.addValidationError(TRUErrorKeys.TRU_ERROR_WHILE_PROCESSING_REQUEST, null);
                         getErrorHandler().processException(mVe, this);
                         if (isLoggingError()) {
                                logError("Exception in TRULayawayPaymentFormHandler/handleRemoveGiftCard method ", comExp);
                         }
                   } finally {
                         if (tr != null) {
                                commitTransaction(tr);
                         }
                         if (rrm != null) {
                                rrm.removeRequestEntry(myHandleMethod);
                         }
                   }
            }
            if (isLoggingDebug()) {
                   logDebug("End of TRULayawayPaymentFormHandler.handleRemoveAllGiftCard method");
            }
            return checkFormRedirect(getLayawaySuccessUrl(), getLayawayErrorUrl(), pRequest, pResponse);
     }

	/**
	 * This method is used for post activities after user removed the Gift Card.
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	protected void postRemoveGiftCard() throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRULayawayPaymentFormHandler.postRemoveGiftCard method");
		}
		TRULayawayOrderImpl order = ((TRUOrderHolder) getShoppingCart()).getLayawayOrder();
		TRUOrderTools l_oOrderTools = (TRUOrderTools)getPaymentGroupManager().getOrderTools();
		l_oOrderTools.updateRemainingAmountInLayawayOrder(order);
		if (isLoggingDebug()) {
			logDebug("End of TRULayawayPaymentFormHandler.postRemoveGiftCard method");
		}
	}
	
	
	/**
	 * This method used to trim the address values in a contactInfo bean.
	 * 
	 * @param pAddress
	 *            The map to hold the address details entered by the user
	 */
	public void trimContactInfoValues(ContactInfo pAddress) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRULayawayProcessHelper.trimContactInfoValues method");
		}
		getAddressInputFields().setFirstName(pAddress.getFirstName().trim());
		getAddressInputFields().setLastName(pAddress.getLastName().trim());
		getAddressInputFields().setAddress1(pAddress.getAddress1().trim());
		if (StringUtils.isNotBlank(pAddress.getAddress2())) {
			getAddressInputFields().setAddress2(pAddress.getAddress2().trim());
		}
		getAddressInputFields().setCity(pAddress.getCity().trim());
		getAddressInputFields().setState(pAddress.getState().trim());
		getAddressInputFields().setPostalCode(pAddress.getPostalCode().trim());
		getAddressInputFields().setPhoneNumber(pAddress.getPhoneNumber().trim());
		getAddressInputFields().setCountry(pAddress.getCountry().trim());
	}
    
	/**
	 * @return the mLayawaySuccessUrl
	 */
	public String getLayawaySuccessUrl() {
		return mLayawaySuccessUrl;
	}

	/**
	 * @param pLayawaySuccessUrl the mLayawaySuccessUrl to set
	 */
	public void setLayawaySuccessUrl(String pLayawaySuccessUrl) {
		this.mLayawaySuccessUrl = pLayawaySuccessUrl;
	}

	/**
	 * @return the mLayawayErrorUrl
	 */
	public String getLayawayErrorUrl() {
		return mLayawayErrorUrl;
	}

	/**
	 * @param pLayawayErrorUrl the mLayawayErrorUrl to set
	 */
	public void setLayawayErrorUrl(String pLayawayErrorUrl) {
		this.mLayawayErrorUrl = pLayawayErrorUrl;
	}
	
	/**
	 * @return the mCardId
	 */
 	public String getCardId() {
		return mCardId;
	}
 	
 	/**
	 * @param pCardId the cardId to set
	 */
	public void setCardId(String pCardId) {
		this.mCardId = pCardId;
	}

 	/**
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}
	
	/**
	 * @param pEmail the mEmail to set
	 */
	public void setEmail(String pEmail) {
		this.mEmail = pEmail;
	}
	
	/**
	 * @return the mBillingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}
	
	/**
	 * @param pBillingHelper the mBillingHelper to set
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		this.mBillingHelper = pBillingHelper;
	}
	
	/**
	 * @return the mGiftCardBalance
	 */
	public double getGiftCardBalance() {
		return mGiftCardBalance;
	}
	
	/**
	 * @param pGiftCardBalance the mGiftCardBalance to set
	 */
	public void setGiftCardBalance(double pGiftCardBalance) {
		this.mGiftCardBalance = pGiftCardBalance;
	}
	
	/**
	 * @return the mPaymentManager
	 */
	public TRUPaymentManager getPaymentManager() {
		return mPaymentManager;
	}
	
	/**
	 * @param pPaymentManager the mPaymentManager to set
	 */
	public void setPaymentManager(TRUPaymentManager pPaymentManager) {
		this.mPaymentManager = pPaymentManager;
	}
	
	/**
	 * @return the mBillingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}
	
	/**
	 * @param pBillingAddressNickName the mBillingAddressNickName to set
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		this.mBillingAddressNickName = pBillingAddressNickName;
	}
	
	/**
	 * @return the mLayawayProcessHelper
	 */
	public TRULayawayProcessHelper getLayawayProcessHelper() {
		return mLayawayProcessHelper;
	}
	
	/**
	 * @param pLayawayProcessHelper the mLayawayProcessHelper to set
	 */
	public void setLayawayProcessHelper(TRULayawayProcessHelper pLayawayProcessHelper) {
		this.mLayawayProcessHelper = pLayawayProcessHelper;
	}

	/**
	 * @return the giftCardNumber
	 */
	public String getGiftCardNumber() {
		return mGiftCardNumber;
	}
	
	/**
	 * @param pGiftCardNumber
	 *            the giftCardNumber to set
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		mGiftCardNumber = pGiftCardNumber;
	}

	/**
	 * @return the mGiftCardPin
	 */
	public String getGiftCardPin() {
		return mGiftCardPin;
	}

	/**
	 * @param pGiftCardPin the giftCardPin to set
	 */
	public void setGiftCardPin(String pGiftCardPin) {
		mGiftCardPin = pGiftCardPin;
	}

	/**
	 * @return the mLayawayNumber
	 */
	public String getLayawayNumber() {
		return mLayawayNumber;
	}

	/**
	 * @param pLayawayNumber
	 *            the mLayawayNumber to set
	 */
	public void setLayawayNumber(String pLayawayNumber) {
		this.mLayawayNumber = pLayawayNumber;
	}

	/**
	 * @return the mCreditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * @param pCreditCardInfoMap
	 *            the mCreditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		this.mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * @return the mPaymentAmount
	 */
	public double getPaymentAmount() {
		return mPaymentAmount;
	}

	/**
	 * @param pPaymentAmount
	 *            the mPaymentAmount to set
	 */
	public void setPaymentAmount(double pPaymentAmount) {
		this.mPaymentAmount = pPaymentAmount;
	}

	/**
	 * @return the mAddressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * @param pAddressInputFields
	 *            the mAddressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		this.mAddressInputFields = pAddressInputFields;
	}
	
	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * Sets property OrderTools
	 * 
	 * @param pOrderTools - the orderTools to set
	 **/
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}
	
	/**
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler
	 *            the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}
	
	/**
	 * @return the IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * @param pValidator
	 *            the IValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		this.mValidator = pValidator;
	}
	
	/**
	 * @return mProfileManager profile manager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}
	
	/**
	 * @param pProfileManager parameter
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}
	
	/**
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}
	
	/**
	 * @return the mAmountDue
	 */
	public double getAmountDue() {
		return mAmountDue;
	}
	/**
	 * @param pAmountDue the mAmountDue to set
	 */
	public void setAmountDue(double pAmountDue) {
		this.mAmountDue = pAmountDue;
	}

	/**
	 * @return the mProcessLayawayOrderChainId
	 */
	public String getProcessLayawayOrderChainId() {
		return mProcessLayawayOrderChainId;
	}

	/**
	 * @param pProcessLayawayOrderChainId the mProcessLayawayOrderChainId to set
	 */
	public void setProcessLayawayOrderChainId(String pProcessLayawayOrderChainId) {
		this.mProcessLayawayOrderChainId = pProcessLayawayOrderChainId;
	}
	
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;

	/**
	 * @return the restService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * @param pRestService the restService to set
	 */
	public void setRestService(boolean pRestService) {
		mRestService = pRestService;
	}

	/**
	 * @return the mIntegrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * @param pIntegrationStatusUtil the mIntegrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		this.mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * @return the mAddressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * @param pAddressDoctorProcessStatus the mAddressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(
			String pAddressDoctorProcessStatus) {
		this.mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * @return the mAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * @param pAddressDoctorResponseVO the mAddressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		this.mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * @return the mAddressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * @param pAddressValidated the mAddressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		this.mAddressValidated = pAddressValidated;
	}

	/**
	 * @return the errorKeyList
	 */
	public List<String> getErrorKeyList() {
		return mErrorKeyList;
	}

	/**
	 * @param pErrorKeyList the errorKeyList to set
	 */
	public void setErrorKeyList(List<String> pErrorKeyList) {
		mErrorKeyList = pErrorKeyList;
	}

	/**
	 * This method is used to get deviceID.
	 * @return deviceID String
	 */
	public String getDeviceID() {
		return mDeviceID;
	}

	/**
	 * This method is used to set deviceID.
	 *
	 * @param pDeviceID the new device id
	 */
	public void setDeviceID(String pDeviceID) {
		mDeviceID = pDeviceID;
	}

	/**
	 * This method is used to get userSession.
	 * @return userSession String
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * This method is used to set userSession.
	 *
	 * @param pUserSession the new user session
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}
	/**
	 * This method will set error .
	 * 
	 * @param pErrorMessage - ErrorMessage.
	 * @throws ServletException - If any ServletException.
	 * 
	 */
	private void setChannelError(String pErrorMessage) throws ServletException {
		if(isRestService()) {
			mVe.addValidationError(pErrorMessage, null);
			getErrorHandler().processException(mVe, this);
		} else {
			addFormException(new DropletException(pErrorMessage));
		}
	}
}
