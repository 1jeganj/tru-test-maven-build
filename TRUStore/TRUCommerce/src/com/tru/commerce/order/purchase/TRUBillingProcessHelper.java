package com.tru.commerce.order.purchase;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class holds the helper methods for payment related methods.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUBillingProcessHelper extends GenericService {
	
	/** property to hold ProfileManager. */
	private TRUProfileManager mProfileManager;
	
	/** hold mPurchaseProcessHelper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	
	
	/**
	 * @return the mPurchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		this.mPurchaseProcessHelper = pPurchaseProcessHelper;
	}
	
	/**
	 * @return mProfileManager profile manager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}
	/**
	 * @param pProfileManager parameter
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}
	
	/**
	 * <p>adds new credit card to the container and also to the profile for logged in user.</p>
	 *
	 * @param pOrder order
	 * @param pProfile profile
	 * @param pNewCreditCard newCreditCard
	 * @param pBillingAddressNickName billingAddressNickName
	 * @param pBillingAddress billingAddress
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @param pAddToProfile the add to profile
	 * @throws CommerceException CommerceException
	 */
	public void addCreditCard(Order pOrder, RepositoryItem pProfile, Map<String,String> pNewCreditCard,
			String pBillingAddressNickName, Address pBillingAddress, PaymentGroupMapContainer pPaymentGroupMapContainer,
			boolean pAddToProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.addCreditCard method");
		}
		final PaymentGroupManager pgm = getPurchaseProcessHelper().getOrderManager().getPaymentGroupManager();
		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		TRUCreditCard creditCard = null;
        //TRUCreditCard orderCreditCard = (TRUCreditCard) ((TRUOrderManager) getPurchaseProcessHelper().getOrderManager()).getCreditCard(pOrder);
        // Create a new payment group and update properties.
		//if (StringUtils.isNotBlank(orderCreditCard.getCreditCardNumber())) {
			creditCard = (TRUCreditCard) pgm.createPaymentGroup();
		//} else {	
			//creditCard = (TRUCreditCard) ((TRUOrderManager) getPurchaseProcessHelper().getOrderManager()).getCreditCard(pOrder);
		//}
		
		try {
			profileTools.copyShallowCreditCardProperties(pNewCreditCard, creditCard);
			creditCard.setCreditCardNumber(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TOKEN));
			creditCard.setCreditCardType(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TYPE));
			//creditCard.setPropertyValue(getProfileManager().getPropertyManager().getTransientRetryCount(), TRUConstants.INT_ZERO);
			if(pOrder instanceof TRUOrderImpl){
			if(!creditCard.getCreditCardNumber().equals(((TRUOrderImpl)pOrder).getPreviousCreditCardNumber())){
				((TRUOrderImpl)pOrder).setRetryCount(TRUConstants.INT_ZERO);
			}
			}if(pOrder instanceof TRULayawayOrderImpl){
				if(!creditCard.getCreditCardNumber().equals(((TRULayawayOrderImpl)pOrder).getPreviousCreditCardNumber())){
					((TRULayawayOrderImpl)pOrder).setRetryCount(TRUConstants.INT_ZERO);
				}	
			}
		} catch (PropertyNotFoundException pnfe) {
			if (isLoggingError()) {
				logError("Exception in @Class:::TRUBillingProcessHelper:::@method:::addCreditCard()",pnfe);
			}
		}
		creditCard.setBillingAddressNickName(pBillingAddressNickName);
		creditCard.setBillingAddress(pBillingAddress);	
		
		//START: TUW-51312
		List<String> creditCardNickNames = new ArrayList<String>();
		if(pPaymentGroupMapContainer.getPaymentGroupMap() != null && !pPaymentGroupMapContainer.getPaymentGroupMap().isEmpty()){
			Set keySet = pPaymentGroupMapContainer.getPaymentGroupMap().keySet();
			Iterator iterator = keySet.iterator();
			while (iterator.hasNext()) {
				creditCardNickNames.add((String) iterator.next());
			}
		}
		//String nickName= getCCNickName(creditCard);
		String nickName= profileTools.getUniqueCreditCardNickname(creditCard, pProfile, null);
		if(!creditCardNickNames.isEmpty() && creditCardNickNames.size() > TRUCheckoutConstants.INT_ONE) {
			nickName = profileTools.getUniqueCreditCardNickname(creditCard, creditCardNickNames, nickName);
		}
		PaymentGroup paymentGroup= pPaymentGroupMapContainer.getPaymentGroup(nickName);
		if(null != paymentGroup){
		//	nickName = profileTools.generateUniqueNickname(nickName);
			paymentGroup= pPaymentGroupMapContainer.getPaymentGroup(nickName);
		}
		//END: TUW-51312
		
		if(null == paymentGroup){
			// get unique nickname
			final String paymentGroupName = getNewPaymentGroupName(creditCard, pPaymentGroupMapContainer);
			if(isLoggingDebug()){
				logDebug("Before pPaymentGroupMapContainer" +pPaymentGroupMapContainer.getPaymentGroupMap().keySet());
			}
			pPaymentGroupMapContainer.setDefaultPaymentGroupName(paymentGroupName);
			if(isLoggingDebug()){
				logDebug("After pPaymentGroupMapContainer" +pPaymentGroupMapContainer.getPaymentGroupMap().keySet());
			}
			// Put the new payment group in the container.
			creditCard.setLastActivity(new Timestamp((new Date()).getTime()));
			pPaymentGroupMapContainer.addPaymentGroup(paymentGroupName, creditCard);		    		    
		} 

		final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);	
		RepositoryItem ccItem=  profileTools.getCreditCardByNickname(nickName,pProfile);
		if (!isAnonymousUser && ccItem==null && pAddToProfile) {
			profileTools.createProfileCreditCard(pProfile, pNewCreditCard, pBillingAddressNickName);
		}
		((MutableRepositoryItem)pProfile).setPropertyValue(getPurchaseProcessHelper().getPropertyManager().getSelectedCCNickNamePropertyName(), nickName);
		if (isLoggingDebug()) {
			vlogDebug("END of TRUBillingProcessHelper.addCreditCard method");
		}
	}
	
	/**
	 * <p>edit credit card to the container and also to the profile for logged in user.</p>
	 * 
	 * @param pProfile profile
	 * @param pEditCreditCard editCreditCard
	 * @param pCreditCardName creditCardName
	 * @param pBillingAddressNickName billingAddressNickName
	 * @param pBillingAddress billingAddress
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @throws CommerceException CommerceException
	 */
	public void editCreditCard(RepositoryItem pProfile, Map<String,String> pEditCreditCard, String pCreditCardName,
			String pBillingAddressNickName, Address pBillingAddress, PaymentGroupMapContainer pPaymentGroupMapContainer) 
					throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.editCreditCard method");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		final TRUPropertyManager propertyManager = getPurchaseProcessHelper().getPropertyManager();
		if (StringUtils.isNotBlank(pCreditCardName)) {
			TRUCreditCard creditCard = (TRUCreditCard) pPaymentGroupMapContainer.getPaymentGroup(pCreditCardName);
			if (creditCard != null) {
				creditCard.setBillingAddressNickName(pBillingAddressNickName);
				creditCard.setBillingAddress(pBillingAddress);
				creditCard.setLastActivity(new Timestamp((new Date()).getTime()));
				pEditCreditCard.put(propertyManager.getCreditCardTypePropertyName(), creditCard.getCreditCardType());
				try {
					profileTools.copyShallowCreditCardProperties(pEditCreditCard, creditCard);
				} catch (PropertyNotFoundException pnfe) {
					if (isLoggingError()) {
						logError("Exception in @Class:::TRUBillingProcessHelper:::@method:::editCreditCard()",pnfe);
					}
				}
			}
		}
    	final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
	    if (!isAnonymousUser) {
	    	profileTools.updateProfileCreditCard(pProfile, pCreditCardName, pEditCreditCard, pBillingAddressNickName);
	    }
	    if (isLoggingDebug()) {
	    	vlogDebug("END of TRUBillingProcessHelper.editCreditCard method");
	    }
	}
	
	/**
	 * returns unique payment group name.
	 * 
	 * @param pPaymentGroup paymentGroup
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @return uniqueCreditCardNickname
	 */
	@SuppressWarnings("rawtypes")
	private String getNewPaymentGroupName(CreditCard pPaymentGroup,
			PaymentGroupMapContainer pPaymentGroupMapContainer) {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.getNewPaymentGroupName method");
		}
		if (!(pPaymentGroup instanceof CreditCard)){
			return null;
		}
		final TRUProfileTools profileTools = (TRUProfileTools)getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		

		
		Map paymentGroupMap = pPaymentGroupMapContainer.getPaymentGroupMap();
		Collection paymentGroupNicknames = null;
		if (paymentGroupMap != null) {
			paymentGroupNicknames = paymentGroupMap.keySet();
		}
		return profileTools.getUniqueCreditCardNickname(pPaymentGroup, paymentGroupNicknames, null);
	}
	
	/**
	 * Updates the order credit card with the input map values and billing addess ContactInfo object.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pCreditCardInputMap
	 *            the credit card input map
	 * @param pBillingAddress
	 *            the billing address
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void updateCreditCard(Order pOrder, Map<String, String> pCreditCardInputMap, ContactInfo pBillingAddress)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.updateCreditCard method");
			vlogDebug("INPUT PARAMS OF updateOrderWithPayment() pOrder: {0} pCreditCardInputMap: {1} pBillingAddress: {2}", pOrder,
					pCreditCardInputMap, pBillingAddress);
		}
		final TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
		final TRUCreditCard creditCard = (TRUCreditCard) orderManager.getCreditCard(pOrder);

		if (creditCard != null) {
			if (isLoggingDebug()) {
				vlogDebug("Updating the creditCard details for order");
			}
			final TRUPropertyManager pm = getPurchaseProcessHelper().getPropertyManager();

			if (!StringUtils.isBlank(pCreditCardInputMap.get(pm.getCreditCardTokenPropertyName()))) {
				creditCard.setCreditCardNumber(pCreditCardInputMap.get(pm.getCreditCardTokenPropertyName()));
			}else {
				creditCard.setCreditCardNumber((String) pCreditCardInputMap.get(pm.getCreditCardNumberPropertyName()));
			}
			creditCard.setCreditCardType(pCreditCardInputMap.get(pm.getCreditCardTypePropertyName()));
			creditCard.setExpirationMonth(pCreditCardInputMap.get(pm.getCreditCardExpirationMonthPropertyName()));
			creditCard.setExpirationYear(pCreditCardInputMap.get(pm.getCreditCardExpirationYearPropertyName()));
			creditCard.setCardVerificationNumber(pCreditCardInputMap.get(pm.getCreditCardVerificationNumberPropertyName()));
			creditCard.setNameOnCard(pCreditCardInputMap.get(pm.getNameOnCardPropertyName()));
			//creditCard.setPropertyValue(getProfileManager().getPropertyManager().getTransientRetryCount(), TRUConstants.INT_ZERO);
			
			if(!creditCard.getCreditCardNumber().equals(((TRUOrderImpl)pOrder).getPreviousCreditCardNumber())){
				((TRUOrderImpl)pOrder).setRetryCount(TRUConstants.INT_ZERO);
				((TRUOrderImpl)pOrder).setPreviousResponseCode(null);
			}
			
			// Updating the credit card addresses.
			((TRUOrderTools) orderManager.getOrderTools()).updateCreditCardAddress(pBillingAddress, creditCard);
			
			orderManager.updateCardDataInOrder(pOrder, creditCard.getCreditCardType());
			if (isLoggingDebug()) {
				vlogDebug("TRUBillingProcessHelper.updateCreditCard :: credit card from the order updated with the input map and billing address provided.");
			}
		} else  if(isLoggingError()) {
				logError("Exception in @Class::TRUBillingProcessHelper::@method::updateCreditCard(): Credit Card is NULL");
		}

		if (isLoggingDebug()) {
			vlogDebug("End of updateCreditCard method");
		}
	}
	
	/**
	 * Updates the order paymentGroup credit card object with the given credit card.
	 *
	 * @param pOrder the order
	 * @param pCreditCard the credit card
	 * @return orderCreditCard the CreditCard
	 * @throws CommerceException the commerce exception
	 */
	public CreditCard updateOrderWithCreditCard(Order pOrder, CreditCard pCreditCard) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.updateOrderWithCreditCard method");
			vlogDebug("INPUT PARAMS OF updateOrderWithCreditCard() pOrder: {0} pCreditCard: {1}",
					pOrder, pCreditCard);
		}
		final TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
		
		CreditCard orderCreditCard = null;
		if(pOrder != null && pCreditCard != null) {
			orderCreditCard = orderManager.getCreditCard(pOrder);
			if (orderCreditCard != null) {
				this.copyCardInfo(pCreditCard, orderCreditCard);
				//Update card type in profile
				orderManager.updateCardDataInOrder(pOrder, orderCreditCard.getCreditCardType());
				if (isLoggingDebug()) {
					vlogDebug("TRUBillingProcessHelper.updateOrderWithCreditCard : End");
				}
			} else if(isLoggingError()) {
					logError("Exception in @Class::TRUBillingProcessHelper::@method::updateOrderWithCreditCard(): Credit Card is NULL");
			}
		}
		else if (isLoggingDebug()) {
				vlogDebug("TRUBillingProcessHelper.updateOrderWithCreditCard : CreditCard found empty");
		}
		
		return orderCreditCard;
	}

	/**
	 * Copies the property values of the RepositoryItem of type creditCard to CreditCard object.
	 *
	 * @param pSourceCreditCard the credit card item
	 * @param pTargetCreditCard the credit card object
	 * @return CreditCard
	 * @throws CommerceException 
	 */
	public CreditCard copyCardInfo(CreditCard pSourceCreditCard, CreditCard pTargetCreditCard) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUPurchaseProcessHelper.copyCardInfo method");
			vlogDebug("INPUT PARAMS OF copyCardInfo() pSourceCreditCard: {0} pTargetCreditCard: {1}",
					pSourceCreditCard, pTargetCreditCard);
		}
		if(pSourceCreditCard != null && pTargetCreditCard != null) {
			final TRUCreditCard sourceCreditCard = (TRUCreditCard) pSourceCreditCard;
			final TRUCreditCard targetCreditCard = (TRUCreditCard) pTargetCreditCard;
			
			targetCreditCard.setCreditCardNumber(sourceCreditCard.getCreditCardNumber());
			targetCreditCard.setCreditCardType(sourceCreditCard.getCreditCardType());
			targetCreditCard.setExpirationMonth(sourceCreditCard.getExpirationMonth());
			targetCreditCard.setExpirationYear(sourceCreditCard.getExpirationYear());
			//targetCreditCard.setCardVerficationNumber(sourceCreditCard.getCardVerficationNumber());
			targetCreditCard.setNameOnCard(sourceCreditCard.getNameOnCard());
			//targetCreditCard.setPropertyValue(getProfileManager().getPropertyManager().getTransientRetryCount(), TRUConstants.INT_ZERO);
			targetCreditCard.setBillingAddressNickName(sourceCreditCard.getBillingAddressNickName());
			
			if (targetCreditCard.getBillingAddress() != null && 
					sourceCreditCard.getBillingAddress() != null) {
				OrderTools.copyAddress(sourceCreditCard.getBillingAddress(), targetCreditCard.getBillingAddress());
			} else {
				targetCreditCard.setBillingAddress(sourceCreditCard.getBillingAddress());
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUPurchaseprocessHelper.copyCardInfo : CreditCard set with the values");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of copyCardInfo method and returns pCreditCardObject: {0} ", pTargetCreditCard);
		}
		return pTargetCreditCard;
	}
	
	/**
	 * Updates the order paymentGroup credit card object with the given credit card.
	 *
	 * @param pOrder the order
	 * @param pCreditCard the credit card
	 * @param pPaymentGroupContainer PaymentGroupMapContainer
	 * @return orderCreditCard the CreditCard
	 * @throws CommerceException the commerce exception
	 */
	public CreditCard changeCreditCardInOrder(Order pOrder, CreditCard pCreditCard, PaymentGroupMapContainer pPaymentGroupContainer) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUBillingProcessHelper.changeCreditCardInOrder method");
			vlogDebug("INPUT PARAMS OF changeCreditCardInOrder() pOrder: {0} pCreditCard: {1}",
					pOrder, pCreditCard);
		}
		final TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
		final PaymentGroupManager paymentGroupManager = orderManager.getPaymentGroupManager();
		final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		TRUCreditCard orderCreditCard = null;
		if(pOrder != null && pCreditCard != null) {
			orderCreditCard = (TRUCreditCard) orderManager.getCreditCard(pOrder);
			String orderCardNickName = orderManager.getOrderCreditCardNickName(orderCreditCard, pPaymentGroupContainer);
			if (orderCreditCard != null) {
				TRUCreditCard creditCard = (TRUCreditCard) paymentGroupManager.createPaymentGroup();
				try {
					profileTools.copyShallowCreditCardProperties(orderCreditCard, creditCard);
					creditCard.setBillingAddress(orderCreditCard.getBillingAddress());
					creditCard.setBillingAddressNickName(orderCreditCard.getBillingAddressNickName());
				} catch (PropertyNotFoundException pnfExp) {
					if (isLoggingError()) {
						logError("Exception in @Class:::TRUBillingProcessHelper:::@method:::changeCreditCardInOrder()",pnfExp);
					}
				}
				this.copyCardInfo(pCreditCard, orderCreditCard);
				pPaymentGroupContainer.removePaymentGroup(orderCardNickName);
				pPaymentGroupContainer.addPaymentGroup(orderCardNickName, creditCard);
				//Update card type in profile
				orderManager.updateCardDataInOrder(pOrder, orderCreditCard.getCreditCardType());
				if (isLoggingDebug()) {
					vlogDebug("TRUBillingProcessHelper.updateOrderWithCreditCard : End");
				}
			} else if(isLoggingError()) {
					logError("Exception in @Class::TRUBillingProcessHelper::@method::changeCreditCardInOrder(): Credit Card is NULL");
			}
		}
		else if (isLoggingDebug()) {
				vlogDebug("TRUBillingProcessHelper.changeCreditCardInOrder : CreditCard found empty");
		}
		
		return orderCreditCard;
	}
	
	/**
	 * Ensures payment group in order with the given payment group type.
	 * @param pOrder the order
	 * @param pPaymentGroupType the payment group type
	 * @throws CommerceException 
	 */
	public void ensurePaymentGroup(Order pOrder, String pPaymentGroupType) throws CommerceException {
		final TRUOrderManager orderManager = (TRUOrderManager) getPurchaseProcessHelper().getOrderManager();
		if(TRUCheckoutConstants.IN_STORE_PAYMENT.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensureInStorePayment(pOrder);
			((TRUOrderImpl)pOrder).setPayPalOrder(Boolean.FALSE);
			((TRUOrderImpl)pOrder).setCreditCardType(null);
		} else if(TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensureCreditCard(pOrder);
			((TRUOrderImpl)pOrder).setPayPalOrder(Boolean.FALSE);
		} else if(TRUCheckoutConstants.PAYPAL_CARD.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensurePayPal(pOrder);
			((TRUOrderImpl)pOrder).setCreditCardType(null);
		}
		orderManager.setCCTypeNullIfPGAsGC(((TRUOrderImpl)pOrder));
	}
	
	/**
	 * Validate order remaining amount.
	 *
	 * @param pOrder the order
	 * @param pActionType the action type
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean validateOrderRemainingAmount(Order pOrder, String pActionType)  {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			vlogDebug("START TRUBillingProcessHelper:: validateOrderRemainingAmount method");
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		boolean isCreditCardExist = false;
		boolean isPayPalExist = false;
		boolean isPayInStoreExist = false;
		TRUCreditCard creditCardPG = null;
		InStorePayment inStorePG = null;
		TRUPayPal payPalPG = null;
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		PaymentGroupManager pgm = getPurchaseProcessHelper().getOrderManager().getPaymentGroupManager();
		for (PaymentGroup pg : listOfPaymentGroup) {
			if(pg instanceof TRUCreditCard){
				creditCardPG = (TRUCreditCard)pg;
				isCreditCardExist = true;
				break;
			}
			if(pg instanceof TRUPayPal){
				payPalPG = (TRUPayPal) pg;
				isPayPalExist = true;
				break;
			}
			if(pg instanceof InStorePayment){
				inStorePG = (InStorePayment) pg;
				isPayInStoreExist = true;
				break;
			}
		}
		double orderRemaningAmount = ((TRUOrderManager)getPurchaseProcessHelper().getOrderManager()).getOrderRemaningAmount(order);		
		if (orderRemaningAmount ==  TRUConstants.DOUBLE_ZERO) {
			try {
				if(isCreditCardExist) {
					pgm.removePaymentGroupFromOrder(order, creditCardPG.getId());
					//((TRUOrderImpl)pOrder).setCreditCardType(null);
					order.setPgRemoved(Boolean.TRUE);
				} else if(isPayPalExist) {
					pgm.removePaymentGroupFromOrder(order, payPalPG.getId());
					order.setPgRemoved(Boolean.TRUE);
				} else if(isPayInStoreExist) {
					pgm.removePaymentGroupFromOrder(order, inStorePG.getId());
					order.setPgRemoved(Boolean.TRUE);
				}
			} catch (CommerceException comExp) {
				if(isLoggingError()){
					vlogError(" CommerceException occured at TRUBillingProcessHelper.validateOrderRemainingAmount(),OrderId:{0} profileId:{1}" + 
							" CommerceException:{2}", order.getId(),order.getProfileId(),comExp);
				}
			}
		}
		if ((!(isPayPalExist || isCreditCardExist || isPayInStoreExist)) && orderRemaningAmount >  TRUConstants.DOUBLE_ZERO) {
			return false;
		}
		if (isLoggingDebug()) {
			vlogDebug("END TRUBillingProcessHelper:: validateOrderRemainingAmount method");
		}
		return true;
	}
	
	//Added here as part of fixing PMD NcssTypeCount
	/**
	 * <p>
	 * </p>
	 * @param pContactInfo ContactInfo
	 * @return the addressPhoneNumber
	 */
	public String getAddressPhoneNumber(ContactInfo pContactInfo ) {
		String addressPhoneNumber = (pContactInfo.getPhoneNumber());
		if (!StringUtils.isBlank(addressPhoneNumber)) {
			addressPhoneNumber = addressPhoneNumber.replaceAll(
					TRUConstants.HYPHEN, TRUConstants.EMPTY);
			addressPhoneNumber = addressPhoneNumber.trim();
			pContactInfo.setPhoneNumber(addressPhoneNumber);
		}
		return addressPhoneNumber;
	}

	
	/**
	 * 
	 * @param pCreditCard CreditCard
	 * @return nickname String
	 *//*
	private String getCCNickName(CreditCard pCreditCard){
		String nickname=null;
		String creditCardType = ((CreditCard) pCreditCard).getCreditCardType();
		String creditCardNumber = ((CreditCard) pCreditCard).getCreditCardNumber();
		if (StringUtils.isNotBlank(creditCardType) && StringUtils.isNotBlank(creditCardNumber) ) {
			StringBuffer sb = new StringBuffer(creditCardType);
			int cardNumberLength = creditCardNumber.length();
			String lastFour = creditCardNumber.substring(cardNumberLength - TRUConstants.FOUR, cardNumberLength);
			sb.append(HYPHEN + lastFour);
			if (isLoggingDebug()) {
				logDebug("New nickname for CC ending with  "+lastFour+" is :" + sb.toString());
			}
			nickname = sb.toString();
		} 
		return nickname;
	}*/
	
	/**
	 * Gets the credit card type.
	 *
	 * @param pCreditCard the credit card
	 * @return the credit card type
	 */
	public String getCreditCardType(RepositoryItem pCreditCard){
		String creditCardType = null;
		creditCardType = (String) pCreditCard.getPropertyValue(getPurchaseProcessHelper().getPropertyManager().getCreditCardTypePropertyName());
		return creditCardType;
	}
	
	/**
	 * Gets the credit card type.
	 *
	 * @param pCreditCard CreditCard
	 * @return creditCardType String
	 */
	public String getCCTypeFromPaymentGroup(CreditCard pCreditCard){
		String creditCardType = null;
		if(pCreditCard != null){
		creditCardType = pCreditCard.getCreditCardType();
		}
		return creditCardType;
	}
}
