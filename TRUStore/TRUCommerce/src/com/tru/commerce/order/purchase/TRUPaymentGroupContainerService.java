package com.tru.commerce.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderTools;
import atg.commerce.order.RepositoryContactInfo;
import atg.commerce.order.purchase.PaymentGroupContainerService;
import atg.core.util.Address;
import atg.core.util.ContactInfo;

/**
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUPaymentGroupContainerService extends PaymentGroupContainerService {

	/**
	 * Initially set to 1L.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Initially set the mBillingAddress.
	 */
	private ContactInfo mBillingAddress;
	
	/**
	 * property: flag indicating whether billing address will be same as.
	 * shipping address.
	 */
	private boolean mBillingAddressSameAsShippingAddress;

	/**
	 * @return the billingAddressSameAsShippingAddress
	 */
	public boolean isBillingAddressSameAsShippingAddress() {
		return mBillingAddressSameAsShippingAddress;
	}

	/**
	 * @param pBillingAddressSameAsShippingAddress
	 *            the billingAddressSameAsShippingAddress to set
	 */
	public void setBillingAddressSameAsShippingAddress(
			boolean pBillingAddressSameAsShippingAddress) {
		mBillingAddressSameAsShippingAddress = pBillingAddressSameAsShippingAddress;
	}
	
	/**
	 * This method sets the Shipping Address.
	 * @return billingAddress 
	 */
	public ContactInfo getBillingAddress()
	{
		return this.mBillingAddress;
	}

	/**
	 * @param pBillingAddress the billingAddress to set
	 * @throws CommerceException  CommerceException
	 */
	public void setBillingAddress(ContactInfo pBillingAddress) throws CommerceException
	{
		if ((pBillingAddress instanceof RepositoryContactInfo) || (pBillingAddress instanceof Address) || (pBillingAddress == null)) {
			if (this.mBillingAddress != null){
				this.mBillingAddress.deleteObservers();
			}
			this.mBillingAddress = pBillingAddress;
		} else {
			try {
				if (this.mBillingAddress == null){
					this.mBillingAddress = ((ContactInfo)pBillingAddress.getClass().newInstance());
				}
				OrderTools.copyAddress(pBillingAddress, this.mBillingAddress);
			} catch (InstantiationException ie) {
				throw new CommerceException(ie.getMessage(),ie);
			} catch (IllegalAccessException iae) {
				throw new CommerceException(iae.getMessage(),iae);
			}
		}
	}



}
