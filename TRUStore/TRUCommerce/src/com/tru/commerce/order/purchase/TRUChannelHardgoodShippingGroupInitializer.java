package com.tru.commerce.order.purchase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.HardgoodShippingGroupInitializer;
import atg.commerce.order.purchase.ShippingGroupInitializationException;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.cart.vo.TRURegistrantDetailsVO;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.common.TRUConstants;

/**
 * The Class TRUChannelHardgoodShippingGroupInitializer.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUChannelHardgoodShippingGroupInitializer extends HardgoodShippingGroupInitializer {

	/** holds current order. **/
	private Order mOrder;

	/** holds shipping process helper instance. **/
	private TRUShippingProcessHelper mShippingHelper;

	/**
	 * Gets the order.
	 * 
	 * @return the order
	 */
	public Order getOrder() {
		return mOrder;
	}

	/**
	 * Sets the order.
	 * 
	 * @param pOrder
	 *            the order to set
	 */
	public void setOrder(Order pOrder) {
		mOrder = pOrder;
	}

	/**
	 * Gets the shipping helper.
	 * 
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 * 
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * gets the addresses from registry service and adds to the container.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pShippingGroupMapContainer
	 *            ShippingGroupMapContainer
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @throws ShippingGroupInitializationException
	 *             exception
	 */
	@Override
	public void initializeShippingGroups(Profile pProfile, ShippingGroupMapContainer pShippingGroupMapContainer,
			DynamoHttpServletRequest pRequest) throws ShippingGroupInitializationException {
		if (getOrder() != null) {
			final TRUOrderImpl order = (TRUOrderImpl) getOrder();
			if (order.getCommerceItemCount() > 0) {
				try {

					final List<TRUChannelDetailsVo> channelDetailsVos = new ArrayList<TRUChannelDetailsVo>();
					// get unique channel detail vo having channel id and
					// channel type
					final List<ShippingGroup> shippingGroups = order.getShippingGroups();
					for (ShippingGroup shippingGroup : shippingGroups) {
						if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
							TRUChannelHardgoodShippingGroup orderHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) shippingGroup;
							final String channelId = orderHardgoodShippingGroup.getChannelId();
							final String channelType = orderHardgoodShippingGroup.getChannelType();

							boolean isChannelFound = false;
							for (TRUChannelDetailsVo channelDetailsVo : channelDetailsVos) {
								if (StringUtils.isNotBlank(channelId) && StringUtils.isNotBlank(channelType)
										&& channelId.equals(channelDetailsVo.getChannelId())
										&& channelType.equals(channelDetailsVo.getChannelType())) {
									isChannelFound = true;
								}
							}
							if (!isChannelFound) {
								final TRUChannelDetailsVo channelDetailsVo = new TRUChannelDetailsVo();
								channelDetailsVo.setChannelId(channelId);
								channelDetailsVo.setChannelType(channelType);
								channelDetailsVo.setChannelName(orderHardgoodShippingGroup.getChannelName());
								channelDetailsVo.setChannelUserName(orderHardgoodShippingGroup.getChannelUserName());
								channelDetailsVo.setChannelCoUserName(orderHardgoodShippingGroup.getChannelCoUserName());
								channelDetailsVo.setSourceChannel(orderHardgoodShippingGroup.getSourceChannel());
								channelDetailsVos.add(channelDetailsVo);
							}
						}
					}

					setRegistryShippingGrpDetails(pShippingGroupMapContainer, channelDetailsVos, pProfile);
				} catch (CommerceException exc) {
					if (isLoggingError()) {
						vlogError("commerce exception occured in @Class:::TRUChannelHardgoodShippingGroupInitializer::@method::"
								+ "initializeShippingGroups()", exc);
					}
					throw new ShippingGroupInitializationException(exc);

				}
			}
		}
	}

	/**
	 * Sets the registry shipping grp details.
	 *
	 * @param pShippingGroupMapContainer the shipping group map container
	 * @param pChannelDetailsVos the channel details vos
	 * @param pProfile profile
	 * @throws CommerceException the commerce exception
	 */
	@SuppressWarnings("unchecked")
	private void setRegistryShippingGrpDetails(ShippingGroupMapContainer pShippingGroupMapContainer,
			final List<TRUChannelDetailsVo> pChannelDetailsVos, Profile pProfile) throws CommerceException {
		final TRUShippingGroupContainerService shipppingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		final TRUOrderTools orderTools = (TRUOrderTools) getShippingHelper().getPurchaseProcessHelper().getOrderManager().getOrderTools();
		final TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingHelper().getPurchaseProcessHelper().getOrderManager()
				.getShippingGroupManager();
		final List<String> registryShippingGroupNames = new ArrayList<String>();
		// get addresses from service and update to container
		// shipping groups
		for (TRUChannelDetailsVo channelDetailsVo : pChannelDetailsVos) {
			final String channelId = channelDetailsVo.getChannelId();
			final String channelType = channelDetailsVo.getChannelType();

			// update registrant details thorugh service
			final boolean isRegistrantDetailsFound = getShippingHelper().updateRegistrantDetails(channelDetailsVo);
			if (!isRegistrantDetailsFound) {
				Map<String, ShippingGroup> shippingGroupMap = pShippingGroupMapContainer.getShippingGroupMap();
				if (pProfile.isTransient() && shippingGroupMap != null && !shippingGroupMap.isEmpty()) {
					Set<String> shippingGroupKeys = shippingGroupMap.keySet();
					for (String shippingGroupKey : shippingGroupKeys) {
							ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMap.get(shippingGroupKey);
							if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
								shippingGroupMap.remove(shippingGroupKey);
							}
					}
				}
				continue;
			}
			List<TRURegistrantDetailsVO> registrantDetailsVOs = channelDetailsVo.getRegistrantDetailsVOs();
			if (registrantDetailsVOs != null && !registrantDetailsVOs.isEmpty()) {
				for (TRURegistrantDetailsVO registrantDetailsVO : registrantDetailsVOs) {
					final ShippingGroup sg = shippingGroupManager.createShippingGroup(orderTools
							.getChannelHardgoodShippingGroupType());
					if (sg instanceof TRUChannelHardgoodShippingGroup) {
						final TRUChannelHardgoodShippingGroup containerShippingGroup = (TRUChannelHardgoodShippingGroup) sg;
						final String nickName = channelId + TRUConstants.UNDER_SCORE + registrantDetailsVO.getRegistrantId();
						final ContactInfo contactInfo = registrantDetailsVO.getAddress();

						OrderTools.copyAddress(contactInfo, containerShippingGroup.getShippingAddress());
						containerShippingGroup.setChannelId(channelId);
						containerShippingGroup.setChannelType(channelType);
						containerShippingGroup.setChannelName(channelDetailsVo.getChannelName());
						containerShippingGroup.setChannelUserName(channelDetailsVo.getChannelUserName());
						containerShippingGroup.setChannelCoUserName(channelDetailsVo.getChannelCoUserName());
						containerShippingGroup.setRegistrantId(registrantDetailsVO.getRegistrantId());
						containerShippingGroup.setNickName(nickName);
						containerShippingGroup.setRegistryAddress(Boolean.TRUE);
						containerShippingGroup.setSourceChannel(channelDetailsVo.getSourceChannel());
						
						shipppingGroupMapContainer.addShippingGroup(nickName, containerShippingGroup);
						registryShippingGroupNames.add(nickName);
					}
				}
			}
		}

		shipppingGroupMapContainer.setNonDisplayableShippingGroups(registryShippingGroupNames);
	}
}
