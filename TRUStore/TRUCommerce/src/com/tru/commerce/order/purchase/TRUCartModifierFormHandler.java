/*
 * 
 */
package com.tru.commerce.order.purchase;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.processor.BillingAddrValidator;
import atg.commerce.order.processor.BillingAddrValidatorImpl;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.PurchaseUserMessage;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.TRUPaymentGroupManager;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUStoreConfiguration;
import com.tru.common.TRUUrlUtil;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.payment.paypal.TRUPaypalUtilityManager;
import com.tru.radial.commerce.payment.paypal.TRUPaypalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalConstants;
import com.tru.radial.commerce.payment.paypal.util.TRUPayPalException;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * This class is used to add,update,edit,remove item from the order.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCartModifierFormHandler extends TRUBaseCartModifierFormHandler {
	
	

	/** Static property to HANDLE SHIPPING GROUP CHANGE METHOD. */
	private static final String HANDLE_SHIPPING_GROUP_CHANGE_METHOD = "CartModifierOrderFormHandler.handleShippingGroupChange";

	/** Static property to HANDLE_UPDATE_COMMERCE_ITEM. */
	private static final String HANDLE_UPDATE_COMMERCE_ITEM = "CartModifierOrderFormHandler.handleUpdateCommerceItem";

	/** Static property to HANDLE_REMOVE_OUT_OF_STOCK_ITEM. */
	private static final String HANDLE_REMOVE_OUT_OF_STOCK_ITEM = "CartModifierOrderFormHandler.handleRemoveOutOfStockItem";

	/** Static property to HANDLE_ADD_UPDATE_BPP_ITEM. */
	private static final String HANDLE_ADD_UPDATE_BPP_ITEM = "CartModifierOrderFormHandler.handleAddUpdateBppItem";

	/** The Constant HANDLE_ADD_DONATION_ITEM. */
	private static final String HANDLE_ADD_DONATION_ITEM = "CartModifierOrderFormHandler.handleAddDonationItemToOrder";

	private static final String COMMERCE_ITEM_COUNT_ZERO = "commerceItemCountZero";
	/** Property to hold mAddShopLocalItemToOrderSuccessURL. */
	private String mAddShopLocalItemToOrderSuccessURL;

	/** Property to Hold add gift wrap to item error url. */
	private String mAddGiftWrapToItemErrorURL;

	/** Property to Hold add gift wrap to item success url. */
	private String mAddGiftWrapToItemSuccessURL;

	/** Property to Hold item id for gift wrap. */
	private String mItemIdForGiftWrap;

	/** property to Hold payPal utility manager. */
	private TRUPaypalUtilityManager mPayPalUtilityManager;

	/** property to hold Wrap Item Quantity. */
	private String mPrevWrapItemQuantity;

	/** property to out of stock id. */
	private String mOutOfStockId;

	/** Property To hold Bpp item Id. */
	private String mBppItemId;
	
	/** The m buy now with apple pay error URL. */
	private String mBuyNowWithApplePayErrorURL;
	
	/** The m buy now with apple pay success URL. */
	private String mBuyNowWithApplePaySuccessURL;


	/** Property To hold Bpp sku Id. */
	private String mBppSkuId;

	/** The BPP info id. */
	private String mBPPInfoId;	

	/** The m radial configuration. */
	private TRURadialConfiguration mRadialConfiguration;

	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;

	/** Property to hold moveToConfirmationChainId. */
	private String mMoveToConfirmationChainId;

	/** Holds String PayerID. */
	private String mPayerId;

	/** Holds String PayPal Token. */
	private String mPayPalToken;

	/**
	 * property to hold mBillingAddressValidator.
	 */
	private BillingAddrValidator mBillingAddressValidator;

	/**
	 * property to hold mPaypalRedirectURL.
	 */
	private String mPaypalRedirectURL;

	/** Property to hold commerce property manager. */
	private TRUPropertyManager mPropertyManager;

	/** The Update commer item ids. */
	private String[] mUpdateCommerItemIds;
	
	/** Property to hold  meta info id. */
	private String mMetaInfoId;
	
	/** String holds the chain id. */
	private String mMoveToConfirmChainId;

	/** The Store configuration. */
	private TRUStoreConfiguration mStoreConfiguration;
	/**
	 * Holds String page name.
	 */
	private String mPageName;
	
	/**
	 * Holds new relation id. 
	 */
	private String mNewRelationId;
	/** property to hold mApplepay. */
	private boolean mApplepay;
	
	
	/** The validate shipping groups chain id. */
	private String mValidateShippingGroupsChainId;
	
	
	/**
	 * Gets the validate shipping groups chain id.
	 *
	 * @return the mValidateShippingGroupsChainId
	 */
	public String getValidateShippingGroupsChainId() {
		return mValidateShippingGroupsChainId;
	}

	/**
	 * Sets the validate shipping groups chain id.
	 *
	 * @param pValidateShippingGroupsChainId the new validate shipping groups chain id
	 */
	public void setValidateShippingGroupsChainId(
			String pValidateShippingGroupsChainId) {
		this.mValidateShippingGroupsChainId = pValidateShippingGroupsChainId;
	}

	/**
	 * Checks if is Apple pay.
	 * 
	 * @return the mApplepay
	 */
	public boolean isApplepay() {
		return mApplepay;
	}

	/**
	 * Sets the Apple pay.
	 * 
	 * @param pApplepay
	 *            the pApplepay to set
	 */
	public void setApplepay(boolean pApplepay) {
		this.mApplepay = pApplepay;
	}
	
	/**
	 * Gets the store configuration.
	 *
	 * @return the store configuration
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the store configuration.
	 *
	 * @param pStoreConfiguration the new store configuration
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * Gets the paypal redirect url.
	 * 
	 * @return the mPaypalRedirectURL
	 */
	public String getPaypalRedirectURL() {
		return mPaypalRedirectURL;
	}

	/**
	 * Sets the paypal redirect url.
	 * 
	 * @param pPaypalRedirectURL
	 *            the pPaypalRedirectURL to set
	 */
	public void setPaypalRedirectURL(String pPaypalRedirectURL) {
		this.mPaypalRedirectURL = pPaypalRedirectURL;
	}

	/**
	 * Gets the billing address validator.
	 * 
	 * @return the billing address validator
	 */
	public BillingAddrValidator getBillingAddressValidator() {
		return mBillingAddressValidator;
	}

	/**
	 * Sets the billing address validator.
	 * 
	 * @param pBillingAddressValidator
	 *            the pBillingAddressValidator to set
	 */
	public void setBillingAddressValidator(BillingAddrValidator pBillingAddressValidator) {
		this.mBillingAddressValidator = pBillingAddressValidator;
	}

	/**
	 * Gets the pay pal token.
	 * 
	 * @return the mPayPalToken
	 */
	public String getPayPalToken() {
		return mPayPalToken;
	}

	/**
	 * Sets the pay pal token.
	 * 
	 * @param pPayPalToken
	 *            the pPayPalToken to set
	 */
	public void setPayPalToken(String pPayPalToken) {
		this.mPayPalToken = pPayPalToken;
	}

	/**
	 * Gets the payer id.
	 * 
	 * @return the mPayerId
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * Sets the payer id.
	 * 
	 * @param pPayerId
	 *            the pPayerId to set
	 */
	public void setPayerId(String pPayerId) {
		this.mPayerId = pPayerId;
	}

	/**
	 * Gets the radial configuration.
	 *
	 * @return the mRadialConfiguration
	 */
	public TRURadialConfiguration getRadialConfiguration() {
		return mRadialConfiguration;
	}

	/**
	 * Sets the radial configuration.
	 *
	 * @param pRadialConfiguration the new radial configuration
	 */
	public void setRadialConfiguration(TRURadialConfiguration pRadialConfiguration) {
		this.mRadialConfiguration = pRadialConfiguration;
	}

	/**
	 * This method gets moveToConfirmationChainId.
	 * 
	 * @return the moveToConfirmationChainId
	 */
	public String getMoveToConfirmationChainId() {
		return mMoveToConfirmationChainId;
	}

	/**
	 * This method sets moveToConfirmationChainId.
	 * 
	 * @param pMoveToConfirmationChainId
	 *            the moveToConfirmationChainId to set
	 */
	public void setMoveToConfirmationChainId(String pMoveToConfirmationChainId) {
		mMoveToConfirmationChainId = pMoveToConfirmationChainId;
	}

	/**
	 * Returns the this property hold reference for TRUConfiguration.
	 * 
	 * @return the mTruConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * sets commerce TRUConfiguration.
	 * 
	 * @param pTruConfiguration
	 *            the TRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}

	/**
	 * Gets the prev wrap item quantity.
	 * 
	 * @return the prevWrapItemQuantity
	 */
	public String getPrevWrapItemQuantity() {
		return mPrevWrapItemQuantity;
	}

	/**
	 * Sets the prev wrap item quantity.
	 * 
	 * @param pPrevWrapItemQuantity
	 *            the prevWrapItemQuantity to set
	 */
	public void setPrevWrapItemQuantity(String pPrevWrapItemQuantity) {
		mPrevWrapItemQuantity = pPrevWrapItemQuantity;
	}

	/**
	 * Gets the page name.
	 *
	 * @return the pageName
	 */
	public String getPageName() {
		return mPageName;
	}
	
	/**
	 * Sets the page name.
	 *
	 * @param pPageName            the pageName to set.
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}

	/**
	 * This method is used to change the shipping option (ship to home/store pickup). 
	 * If the commerce item shipping grouprelationship already exits with in the same commerce item,
	 *  the quantity will added/merged with that relationship. 
	 * If the commerce item shipping group relationship does not exits with in the same commerce item, 
	 * it will create new commerce item
	 * shipping group relationship.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleShippingGroupChange(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : BEGIN");
		}
		ShippingGroupCommerceItemRelationship selectedShippingGroupRel =null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final Order order = getOrder();
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : Relationship Id : {0} : Location Id : {1}",
					getRelationShipId(), getLocationId());
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_SHIPPING_GROUP_CHANGE_METHOD))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (null != order && StringUtils.isBlank(getRelationShipId())) {
					String infoMessage = null;
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
					setAddUpdateItem(Boolean.FALSE);
					addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				}
				if (null != order && !getFormError() && null != order.getRelationship(getRelationShipId())) {
					selectedShippingGroupRel = (ShippingGroupCommerceItemRelationship) order.getRelationship(getRelationShipId());
					if (!isSameISPUShippingGroupSelected(selectedShippingGroupRel)) {
						if (isRestService()) {
							return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
						}
						return Boolean.FALSE;
					}
					synchronized (order) {
						setQuantity(selectedShippingGroupRel.getQuantity());
						preShippingGroupChange(pRequest, pResponse);
						if (isLoggingDebug()) {
							vlogDebug(
									"@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : isAddUpdateItem() : {0}",
									isAddUpdateItem());
						}
						if (!isAddUpdateItem()) {
							
							if (isRestService()) {
								return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
							}
							return Boolean.FALSE;
						}
						shippingGroupChange(order, selectedShippingGroupRel);
						try {
							Map extraParams = createRepriceParameterMap();
							getPurchaseProcessHelper().runProcessRepriceOrder(
									getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
									getUserLocale(), getProfile(), extraParams, this);
						} catch (RunProcessException e) {
							if (isLoggingError()) {
								logError(
										"RunProcessException in @Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange()",
										e);
							}
						}
						updateOrder(order, TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
						postShippingGroupChange(pRequest, pResponse);
					}
				}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("CommerceException in @Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange()", e);
				}
			} catch (SystemException e) {
				if (isLoggingError()) {
					logError(" SystemException in @Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() :", e);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError("RepositoryException in @Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange()",
							re);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_SHIPPING_GROUP_CHANGE_METHOD);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Checks if is same ispu shipping group selected.
	 *
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @return true, if is same ispu shipping group selected
	 */
	private boolean isSameISPUShippingGroupSelected (ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::isSameISPUShippingGroupSelected() : BEGIN");
		}
		if (StringUtils.isNotBlank(getLocationId()) && 
				((pSelectedShippingGroupRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup && 
						((TRUInStorePickupShippingGroup) pSelectedShippingGroupRel
						.getShippingGroup()).getLocationId().equalsIgnoreCase(getLocationId())) || (pSelectedShippingGroupRel
						.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup &&
						((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel
						.getShippingGroup()).getLocationId().equalsIgnoreCase(getLocationId())))) {
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUCartModifierFormHandler::@method::isSameISPUShippingGroupSelected() : selectedShippingGroupRel" + 
						" Location Id and Location Id in request are equal");
			}
			return Boolean.FALSE;
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::isSameISPUShippingGroupSelected() : END");
		}
		return Boolean.TRUE;
	}

	/**
	 * Pre shipping group change. This method is used to check the relationship id in order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void preShippingGroupChange(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() : BEGIN : getRelationShipId() : {0}",
					getRelationShipId());
		}
		String locationId = null;
		TRUShippingGroupCommerceItemRelationship selectedShippingGroupRel = null;
		String infoMessage = null;
		long relQuantity = 0;
		String skuId = null;
		String displayName = null;
		setAddUpdateItem(Boolean.TRUE);
		final Order order = getOrder();
		if (null != order) {
			if (StringUtils.isNotBlank(getLocationId())) {
				locationId = getLocationId();
			} else {
				locationId = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
			}
			try {
				selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) order.getRelationship(getRelationShipId());
				if (null != selectedShippingGroupRel) {
					List<TRUCommerceItemMetaInfo> metaInfos = selectedShippingGroupRel.getMetaInfo();
					if (!metaInfos.isEmpty()) {
						for (TRUCommerceItemMetaInfo metaInfo : metaInfos) {
							if (StringUtils.isNotBlank(getMetaInfoId()) && getMetaInfoId().equals(metaInfo.getId())) {
								relQuantity = (long) metaInfo.getPropertyValue(getShoppingCartUtils().getPropertyManager()
										.getQuantityPropertyName());
								break;
							}
						}
					}
					skuId = selectedShippingGroupRel.getCommerceItem().getCatalogRefId();
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() : skuId : {0} locationId: {1}",
								skuId, locationId);
					}
					if (!StringUtils.isBlank(skuId)) {
						displayName = getShoppingCartUtils().getDisplayNameForSku(skuId);
						final int invenotryStatus = getShoppingCartUtils().getInventoryStatus(skuId, locationId);
						if (isLoggingDebug()) {
							vlogDebug(
									"@Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() : invenotryStatus : {0}",
									invenotryStatus);
						}
						if (invenotryStatus == TRUCommerceConstants.INT_IN_STOCK || invenotryStatus == TRUCommerceConstants.INT_LIMITED_STOCK || 
								invenotryStatus == TRUCommerceConstants.INT_PRE_ORDER) {
							performQtyAutocorrectionForSGChange(order, relQuantity, skuId, locationId, displayName,
									invenotryStatus);
						} else if (invenotryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK || invenotryStatus == TRUCommerceConstants.NUM_ZERO) {
							if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome()
									.equalsIgnoreCase(locationId)) {
								infoMessage = getErrorHandlerManager().getErrorMessage(
										TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
							} else {
								infoMessage = getErrorHandlerManager().getErrorMessage(
										TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
							}
							infoMessage = MessageFormat.format(infoMessage, displayName);
							setAddUpdateItem(Boolean.FALSE);
							addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
						}
					}
				}
			} catch (RelationshipNotFoundException e) {
				if (isLoggingError()) {
					logError(
							" RelationshipNotFoundException in @Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() :",
							e);
				}
			} catch (InvalidParameterException e) {
				if (isLoggingError()) {
					logError(
							" InvalidParameterException in @Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() :",
							e);
				}
			} catch (SystemException e) {
				if (isLoggingError()) {
					logError(" SystemException in @Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() :", e);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(" RepositoryException in @Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() :",
							re);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preShippingGroupChange() : END");
		}
	}

	/**
	 * Perform qty autocorrection for sg change.
	 *
	 * @param pOrder            the order
	 * @param pRelQuantity            the rel quantity
	 * @param pSkuId            the sku id
	 * @param pLocationId            the location id
	 * @param pDisplayName            the display name
	 * @param pInventoryStatus            the inventory status
	 * @throws RepositoryException             the repository exception
	 * @throws SystemException             the system exception
	 */
	protected void performQtyAutocorrectionForSGChange(Order pOrder, long pRelQuantity, String pSkuId, String pLocationId,
			String pDisplayName, int pInventoryStatus) throws RepositoryException, SystemException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::performQtyAutocorrectionForSGChange() : BEGIN");
		}
		long inventoryQty = 0;
		String infoMessage = null;
		String inlineInfoMessage=null;
		long relQuantityPerItemInCart = 0;
		inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, pInventoryStatus);
		relQuantityPerItemInCart = getShoppingCartUtils().getItemQtyInCartByRelShip(pSkuId, pLocationId, pOrder);
		final long inventoryLimit = inventoryQty - relQuantityPerItemInCart;
		if (isLoggingDebug()) {
			vlogDebug("pRelQuantity : {0} skuId : {1} : locationId : {2} displayName : {3} inventoryQty : {4} :", pRelQuantity,
					pSkuId, pLocationId, pDisplayName, inventoryQty);
			vlogDebug("@Class::TRUCartModifierFormHandler::@method::performQtyAutocorrectionForSGChange() : {0}",
					inventoryLimit);
		}
		if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
			infoMessage = getErrorHandlerManager().getErrorMessage(
					TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
			infoMessage = MessageFormat.format(infoMessage, pDisplayName, inventoryQty);
			setAddUpdateItem(Boolean.FALSE);
		} else if (inventoryLimit < pRelQuantity) {
			if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, pDisplayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(inlineInfoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			} else {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
				infoMessage = MessageFormat.format(infoMessage, pDisplayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			}
			setQuantity(inventoryLimit);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::performQtyAutocorrectionForSGChange() : END");
		}
	}

	/**
	 * This method is used to change the shipping option (ship to home/store pickup). 
	 * If the commerce item shipping group	 * relationship already exits with in the same commerce item, 
	 * the quantity will added/merged with that relationship. If the commerce item shipping group relationship 
	 * does not exits with in the same commerce item, it will create new commerce item
	 * shipping group relationship.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSelectedShippingGroupRel
	 *            the selected shipping group rel
	 * @throws CommerceException
	 *             the commerce exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	@SuppressWarnings("unchecked")
	private void shippingGroupChange(Order pOrder, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel)
			throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChange() : BEGIN");
		}
		List<ShippingGroupCommerceItemRelationship> sgCiRels = null;
		sgCiRels = (List<ShippingGroupCommerceItemRelationship>) pOrder.getCommerceItem(
				pSelectedShippingGroupRel.getCommerceItem().getId()).getShippingGroupRelationships();
		if (StringUtils.isBlank(getLocationId())) {
			shippingGroupChangeFromISPUToHG(pOrder, pSelectedShippingGroupRel, sgCiRels);
		} else if (!StringUtils.isBlank(getLocationId()) && 
				!getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(getLocationId())) {
			shippingGroupChangeFromHGToISPU(pOrder, pSelectedShippingGroupRel, sgCiRels);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChange() : END");
		}
	}

	/**
	 * Shipping group change from Hardgood to InstorePickup.
	 *
	 * @param pOrder the order
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @param pSgCiRels the sg ci rels
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	private void shippingGroupChangeFromHGToISPU(Order pOrder, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel,
			List<ShippingGroupCommerceItemRelationship> pSgCiRels) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : BEGIN");
		}
		ShippingGroupCommerceItemRelationship selectedShippingGroupRelToMerge = null;
		ShippingGroup shippingGroup = null;
		long qtyToMerge = getQuantity();
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : location id : {0} : qtyToMerge : {1}",
					getLocationId(), qtyToMerge);
		}
		for (ShippingGroupCommerceItemRelationship sgRel : pSgCiRels) {
			if (!getRelationShipId().equalsIgnoreCase(sgRel.getId())
					&& !getRelationShipId().equalsIgnoreCase(sgRel.getId())
					&& sgRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup
					&& ((TRUChannelInStorePickupShippingGroup) sgRel.getShippingGroup()).getLocationId()
							.equalsIgnoreCase(getLocationId())
					&& ((pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup && ((TRUChannelInStorePickupShippingGroup) sgRel
							.getShippingGroup()).getChannelId().equalsIgnoreCase(
							((TRUChannelHardgoodShippingGroup) pSelectedShippingGroupRel.getShippingGroup())
									.getChannelId())) || (pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup && ((TRUChannelInStorePickupShippingGroup) sgRel
							.getShippingGroup()).getChannelId().equalsIgnoreCase(
							((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel.getShippingGroup())
									.getChannelId())))) {
				selectedShippingGroupRelToMerge = sgRel;
				if (isLoggingDebug()) {
					vlogDebug(
							"TRUChannelInStorePickupShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
							selectedShippingGroupRelToMerge.getId());
				}
				break;
			}
			if (selectedShippingGroupRelToMerge == null
					&& sgRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup
					&& ((TRUInStorePickupShippingGroup) sgRel.getShippingGroup()).getLocationId().equalsIgnoreCase(
							getLocationId())
					&& sgRel.getShippingGroup().getShippingGroupClassType()
							.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType())) {
				selectedShippingGroupRelToMerge = sgRel;
				if (isLoggingDebug()) {
					vlogDebug(
							"TRUInStorePickupShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
							selectedShippingGroupRelToMerge.getId());
				}
				break;
			}
		}
		if (selectedShippingGroupRelToMerge == null && !pOrder.getShippingGroups().isEmpty()) {
			for (ShippingGroup sg : (List<ShippingGroup>) pOrder.getShippingGroups()) {
				if (shippingGroup == null && sg instanceof TRUChannelInStorePickupShippingGroup && 
						((TRUChannelInStorePickupShippingGroup) sg).getLocationId().equalsIgnoreCase(getLocationId()) && 
						((pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup &&
								((TRUChannelInStorePickupShippingGroup) sg).getChannelId().equalsIgnoreCase(
								((TRUChannelHardgoodShippingGroup) pSelectedShippingGroupRel.getShippingGroup()).getChannelId())) || 
								(pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup && 
												((TRUChannelInStorePickupShippingGroup) sg)
								.getChannelId().equalsIgnoreCase(
										((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel.getShippingGroup())
												.getChannelId())))) {
					shippingGroup = sg;
					if (isLoggingDebug()) {
						vlogDebug(
								"TRUChannelInStorePickupShippingGroup is already exists to merge :: shippingGroup id :: {0}",
								sg.getId());
					}
					break;
				}
				if (shippingGroup == null && sg instanceof TRUInStorePickupShippingGroup && !(sg instanceof TRUChannelInStorePickupShippingGroup) && 
						((TRUInStorePickupShippingGroup) sg).getLocationId().equalsIgnoreCase(getLocationId()) && 
						(getOrderManager().getOrderTools().getDefaultShippingGroupType()
								.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType()) || 
								TRUCommerceConstants.INSTORE_PICKUP_SG
								.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType()))) {
					shippingGroup = sg;
					if (isLoggingDebug()) {
						vlogDebug("TRUInStorePickupShippingGroup is already exists to merge :: shippingGroup id :: {0}",
								sg.getId());
					}
					break;
				}
			}
			if (shippingGroup == null) {
				shippingGroup = createMissedIspuSGsInOrderBasedOnSG(
						pOrder, pSelectedShippingGroupRel.getShippingGroup(), getLocationId());
			}
		}
		qtyToMerge = getGiftingProcessHelper().updateSGCIRelBasedOnGWAndPromoSplit(pSelectedShippingGroupRel,
				qtyToMerge, getOrder(), getMetaInfoId());
		addUpdateShippingGroupRelationaShip(pOrder, pSelectedShippingGroupRel, qtyToMerge, shippingGroup,
				selectedShippingGroupRelToMerge);
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : END");
		}
	}


	/**
	 * Shipping group change from InstorePickup to Hardgood.
	 *
	 * @param pOrder the order
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @param pSgCiRels the sg ci rels
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	private void shippingGroupChangeFromISPUToHG(Order pOrder, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel,
			List<ShippingGroupCommerceItemRelationship> pSgCiRels) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromISPUToHG() : START");
		}
		ShippingGroupCommerceItemRelationship selectedShippingGroupRelToMerge = null;
		ShippingGroup shippingGroup = null;
		long qtyToMerge = getQuantity();
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromISPUToHG() : location id : {0} : qtyToMerge : {1}",
					getLocationId(), qtyToMerge);
		}
		for (ShippingGroupCommerceItemRelationship sgRel : pSgCiRels) {
			if (!getRelationShipId().equalsIgnoreCase(sgRel.getId()) && sgRel.getShippingGroup() instanceof TRUHardgoodShippingGroup && 
					(TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup()
							.getShippingGroupClassType()))) {
				selectedShippingGroupRelToMerge = sgRel;
				vlogDebug("TRUHardgoodShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
						selectedShippingGroupRelToMerge.getId());
				break;
			}
			if (selectedShippingGroupRelToMerge == null && !getRelationShipId().equalsIgnoreCase(sgRel.getId()) && 
					sgRel.getShippingGroup() instanceof TRUChannelHardgoodShippingGroup && 
					pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup && 
					((TRUChannelHardgoodShippingGroup) sgRel.getShippingGroup()).getChannelId().equalsIgnoreCase(
							((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel.getShippingGroup())
									.getChannelId())) {
				selectedShippingGroupRelToMerge = sgRel;
				vlogDebug(
						"TRUChannelHardgoodShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
						selectedShippingGroupRelToMerge.getId());
				break;
			}
		}
		if (selectedShippingGroupRelToMerge == null && !pOrder.getShippingGroups().isEmpty()) {
			if (shippingGroup == null && (TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(pSelectedShippingGroupRel
							.getShippingGroup().getShippingGroupClassType()))) {
				shippingGroup = getPurchaseProcessHelper().getFirstShippingGroup(pOrder);
				if (isLoggingDebug()) {
					vlogDebug("TRUHardgoodShippingGroup is already exists to merge :: shippingGroup id :: {0}",
							shippingGroup.getId());
				}
			}
			if (shippingGroup == null) {
				for (ShippingGroup sg : (List<ShippingGroup>) pOrder.getShippingGroups()) {
					if (shippingGroup == null && sg instanceof TRUChannelHardgoodShippingGroup && 
							pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup && 
							((TRUChannelHardgoodShippingGroup) sg).getChannelId().equalsIgnoreCase(
									((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel.getShippingGroup()).getChannelId())) {
						shippingGroup = sg;
						if (isLoggingDebug()) {
							vlogDebug("TRUChannelHardgoodShippingGroup is already exists to merge :: shippingGroup id :: {0}", sg.getId());
						}
						break;
					}
				}
			}
			if (shippingGroup == null && pSelectedShippingGroupRel.getShippingGroup() instanceof TRUChannelInStorePickupShippingGroup) {
				if (isLoggingDebug()) {
					logDebug("There is no TRUHardgoodShippingGroup or TRUChannelHardgoodShippingGroup is exists to merge :: " + 
				"hence creating a new shipping group");
				}
				final TRUChannelDetailsVo channelDetailsVo = createChannelDetailVO((TRUChannelInStorePickupShippingGroup) pSelectedShippingGroupRel
								.getShippingGroup());
				shippingGroup = ((TRUShippingGroupManager) getShippingGroupManager()).addChannelHardgoodShippingGroupToOrder(
						pOrder, channelDetailsVo);
			}
		}
		qtyToMerge = getGiftingProcessHelper().updateSGCIRelBasedOnGWAndPromoSplit(pSelectedShippingGroupRel, qtyToMerge, getOrder(),
				getMetaInfoId());
		addUpdateShippingGroupRelationaShip(pOrder, pSelectedShippingGroupRel, qtyToMerge, shippingGroup,
				selectedShippingGroupRelToMerge);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromISPUToHG() : END");
		}
	}

	/**
	 * Update shipping group relationa ship.
	 *
	 * @param pOrder            the order
	 * @param pSelectedShippingGroupRel            the selected shipping group rel
	 * @param pQtyToMerge            the qty to merge
	 * @param pShippingGroup            the shipping group
	 * @param pSelectedShippingGroupRelToMerge            the selected shipping group rel to merge
	 * @throws CommerceException             the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	protected void addUpdateShippingGroupRelationaShip(Order pOrder,
			ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel, long pQtyToMerge, ShippingGroup pShippingGroup,
			ShippingGroupCommerceItemRelationship pSelectedShippingGroupRelToMerge) throws CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::addUpdateShippingGroupRelationaShip() : BEGIN");
		}
		ShippingGroup oldSG = pSelectedShippingGroupRel.getShippingGroup();
		if (null != pOrder && null != pSelectedShippingGroupRel) {
			String existingBppSkuId = null;
			String existingBppItemId = null;
			String relationshipId = null;
			TRUBppItemInfo bppItemInfo = ((TRUShippingGroupCommerceItemRelationship) pSelectedShippingGroupRel).getBppItemInfo();
			if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())) {
				existingBppSkuId = bppItemInfo.getBppSkuId();
				existingBppItemId = bppItemInfo.getBppItemId();
			}
			if (isLoggingDebug()) {
				vlogDebug(
						"pOrder id : {0} : pSelectedShippingGroupRel id : {1} : pSelectedShippingGroupRel qty : {2} : pQtyToMerge : {3}",
						pOrder.getId(), pSelectedShippingGroupRel.getId(), pSelectedShippingGroupRel.getQuantity(), pQtyToMerge);
			}
			if (null != pSelectedShippingGroupRelToMerge) {
				getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder,
						pSelectedShippingGroupRel.getCommerceItem().getId(), pSelectedShippingGroupRel.getShippingGroup().getId(),
						pQtyToMerge);
				vlogDebug("pSelectedShippingGroupRelToMerge id : {0}", pSelectedShippingGroupRelToMerge.getId());
				getCommerceItemManager().addItemQuantityToShippingGroup(pOrder,
						pSelectedShippingGroupRelToMerge.getCommerceItem().getId(),
						pSelectedShippingGroupRelToMerge.getShippingGroup().getId(), pQtyToMerge);
				relationshipId = pSelectedShippingGroupRelToMerge.getId();
			} else if (null != pShippingGroup && pSelectedShippingGroupRel.getQuantity()==pQtyToMerge) {
				vlogDebug("pShippingGroup id : {0}", pShippingGroup.getId());
				oldSG.removeCommerceItemRelationship(pSelectedShippingGroupRel.getId());
				pSelectedShippingGroupRel.setShippingGroup(pShippingGroup);
			}
			else if(null != pShippingGroup && pSelectedShippingGroupRel.getQuantity()!=pQtyToMerge){
				getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder,
						pSelectedShippingGroupRel.getCommerceItem().getId(), pSelectedShippingGroupRel.getShippingGroup().getId(),
						pQtyToMerge);
				getCommerceItemManager().addItemQuantityToShippingGroup(pOrder,
						pSelectedShippingGroupRel.getCommerceItem().getId(), pShippingGroup.getId(), pQtyToMerge);
				ShippingGroupCommerceItemRelationship cretedSGCIRel = getShippingGroupManager()
						.getShippingGroupCommerceItemRelationship(pOrder, pSelectedShippingGroupRel.getCommerceItem().getId(),
								pShippingGroup.getId());
				if (null != cretedSGCIRel) {
					relationshipId = cretedSGCIRel.getId();
					setNewRelationId(relationshipId);
				}
			}
			//update Shipping Group pickup and alternative pickup person details
			
			if (oldSG instanceof TRUInStorePickupShippingGroup && pShippingGroup instanceof TRUInStorePickupShippingGroup) {
				
				TRUInStorePickupShippingGroup oldShippingGroup = (TRUInStorePickupShippingGroup) oldSG;
				TRUInStorePickupShippingGroup currentShippingGroup = (TRUInStorePickupShippingGroup) pShippingGroup;
				
				TRUOrderImpl truOrderImpl=(TRUOrderImpl)getOrder();
				Map<String,TRUStorePickUpInfo> map=truOrderImpl.getStorePickUpInfos();
				if(null!=map){
					TRUStorePickUpInfo oldInfoVO= map.get(oldShippingGroup.getId());
					TRUStorePickUpInfo newInfoVO= map.get(currentShippingGroup.getId());
					if(newInfoVO==null && oldInfoVO!=null){
						map.put(currentShippingGroup.getId(), oldInfoVO);
					}
					truOrderImpl.setStorePickUpInfos(map);				
					((TRUOrderManager)getOrderManager()).updateInstoreShippingGroupPickUpInfo(oldShippingGroup, currentShippingGroup);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("existingBppSku id : {0} : existingBppItem id : {1} : relationship id : {2}", existingBppSkuId,
						existingBppItemId, relationshipId);
			}
			if (StringUtils.isNotBlank(existingBppSkuId) && StringUtils.isNotBlank(existingBppItemId) && StringUtils.isNotBlank(relationshipId)) {
				((TRUOrderTools) getOrderManager().getOrderTools()).addBppItemToOrder(pOrder, relationshipId, existingBppSkuId,
						existingBppItemId);
			}
			getShippingGroupManager().removeEmptyShippingGroups(pOrder);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::addUpdateShippingGroupRelationaShip() : END");
		}
	}
	
	/**
	 * Post shipping group change.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void postShippingGroupChange(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::postShippingGroupChange() : BEGIN");
		}
		super.postSetOrderByCommerceId(pRequest, pResponse);
		createCartCookies(getOrder(), pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::postShippingGroupChange() : END");
		}
	}

	/**
	 * Handle update commerce item. This method is used to update the commerce item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleUpdateCommerceItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleUpdateCommerceItem() : BEGIN");
			vlogDebug("CommerItemIds : {0} : RelationShipId : {1} : Quantity : {2} : CatalogRefIds : {3}",
					getUpdateCommerItemIds(), getRelationShipId(), getQuantity(), getCatalogRefIds());
		}
		String commId = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_REMOVE_OUT_OF_STOCK_ITEM))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				final boolean isExist = preUpdateCommerceItem();
				if (!getFormError()) {
					commId = getUpdateCommerItemIds()[TRUCommerceConstants.NUM_ZERO];
					synchronized (getOrder()) {
						if (isExist) {
							if (!StringUtils.isBlank(getPageName()) && getPageName().equals(TRUCommerceConstants.SHIPPING)) {
								ShippingGroupCommerceItemRelationship selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) getOrder().
										getRelationship(getRelationShipId());
								long relQuantity = selectedShippingGroupRel.getQuantity();
								setQuantity(relQuantity+(getQuantity()-TRUCheckoutConstants._1));
							}
							if (TRUCommerceConstants.REVIEW_PAGE_NAME.equals(getEditCartFromPage())) {
								TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
								orderImpl.setValidationFailed(Boolean.FALSE);				
								Map parameters = new HashMap();
								TRUUserSession sessionComponent = getUserSession();
								parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
								parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.MOVE_TO_PURCHASE_INFO);
								parameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE, true);
								
								((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(),
										getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
								if(orderImpl.isValidationFailed()){
									addFormException(new DropletException(String.valueOf(Boolean.FALSE)));
									runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
									((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
								}
							}
							pRequest.setParameter(getRelationShipId(), getQuantity());
							preSetOrderByRelationshipId(pRequest, pResponse);
							setQuantity(TRUCommerceConstants.NUM_ZERO);
							modifyOrderByRelationshipId(pRequest, pResponse);
							processEditCartFromReview();
							// Start :: TUW-54321
							//processMoveToConfirmation(pRequest,pResponse);
							// End :: TUW-54321
							updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
						} else {
							TRUShippingGroupCommerceItemRelationship rel = (TRUShippingGroupCommerceItemRelationship) getOrder()
									.getRelationship(getRelationShipId());
							if (null != rel) {
								getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(), commId,
										rel.getShippingGroup().getId(), rel.getQuantity());
							}
							final CommerceItem updatedItem = getOrder().getCommerceItem(commId); 
							if (updatedItem.getQuantity() - rel.getQuantity() > TRUCommerceConstants.NUM_ZERO) {
								updatedItem.setQuantity(updatedItem.getQuantity() - rel.getQuantity());
							} else {
								setRemovalCommerceIds(getUpdateCommerItemIds());
								deleteItems(pRequest, pResponse);
							}
							if (null != getCatalogRefIds() && StringUtils.isNotBlank(getCatalogRefIds()[TRUCommerceConstants.NUM_ZERO])) {
								pRequest.setParameter(getCatalogRefIds()[TRUCommerceConstants.NUM_ZERO], getQuantity());
								preAddItemToOrder(pRequest, pResponse);
								addItemToOrder(pRequest, pResponse);
								postAddItemToOrder(pRequest, pResponse);
							}
						}
					}
				}
			} catch (CommerceItemNotFoundException cinfe) {
				if (isLoggingError()) {
					vlogError(
							"CommerceItemNotFoundException @Class::TRUCartModifierFormHandler.handleUpdateCommerceItem() for Order {0} and details {1}",
							getOrder().getId() ,cinfe);
				}
			} catch (InvalidParameterException ipe) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException @Class::TRUCartModifierFormHandler.handleUpdateCommerceItem() for Order {0} and details {1}",
							getOrder().getId() ,ipe);
				}
			} catch (RunProcessException rpe) {
				if (isLoggingError()) {
					vlogError("RunProcessException @Class::TRUCartModifierFormHandler.handleUpdateCommerceItem() for Order {0} and details {1}", 
							getOrder().getId() ,rpe);
				}
			} catch (CommerceException ce) {
				if (isLoggingError()) {
					vlogError("CommerceException @Class::TRUCartModifierFormHandler.handleUpdateCommerceItem() for Order {0} and details {1}", 
							getOrder().getId() ,ce);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_REMOVE_OUT_OF_STOCK_ITEM);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleUpdateCommerceItem() : END");
		}
		return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
	}

	/**
	 * Pre update commerce item. This method is used to check the updated commerce item and sku item is exist in order or not.
	 * 
	 * @return true, if successful
	 * @throws CommerceItemNotFoundException
	 *             the commerce item not found exception
	 * @throws InvalidParameterException
	 *             the invalid parameter exception
	 */
	@SuppressWarnings({ "unchecked" })
	private boolean preUpdateCommerceItem() throws CommerceItemNotFoundException, InvalidParameterException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preUpdateCommerceItem() : BEGIN");
		}
		boolean isExist = false;
		String infoMessage = null;
		if(isRestService() && StringUtils.isNotBlank(getPageName()) && getPageName().equals(TRUCommerceConstants.REVIEW_PAGE_NAME)){
			setEditCartFromPage(getPageName());
			}
		if (null != getOrder() && null != getUpdateCommerItemIds() && null != getRelationShipId() && null != getCatalogRefIds()
				&& getUpdateCommerItemIds().length > TRUCommerceConstants.NUM_ZERO) {
			String commId = getUpdateCommerItemIds()[TRUCommerceConstants.NUM_ZERO];
			final List<CommerceItem> commItems = getOrder().getCommerceItems();
			final CommerceItem updatedItem = getOrder().getCommerceItem(commId);
			if (null != commItems && null != updatedItem && commItems.contains(updatedItem) && null != getCatalogRefIds()
					&& StringUtils.isNotBlank(getCatalogRefIds()[TRUCommerceConstants.NUM_ZERO])) {
				final String skuId = updatedItem.getCatalogRefId();
				if (getCatalogRefIds() != null && getCatalogRefIds()[TRUCommerceConstants.NUM_ZERO] != null
						&& getCatalogRefIds()[TRUCommerceConstants.NUM_ZERO].equals(skuId)) {
					isExist = true;
				}
			}
		} else {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException se) {
				if (isLoggingError()) {
					logError("SystemException in @Class::TRUCartModifierFormHandler::@method::preUpdateCommerceItem()", se);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierFormHandler::@method::preUpdateCommerceItem() : END : isExist : {0}", isExist);
		}
		return isExist;
	}

	/**
	 * Handle update commerce item. This method is used to update the commerce item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleRemoveOutOfStockItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItem() : BEGIN : OutOfStockId : {0}",
					getOutOfStockId());
		}
		if (null != getOrder() && StringUtils.isNotBlank(getOutOfStockId())) {
			final String commId = getOutOfStockId();
			final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_UPDATE_COMMERCE_ITEM))) {
				Transaction tr = null;
				try {
					final boolean isExist = preRemoveOutOfStockItem(pRequest, pResponse);
					if (isLoggingDebug()) {
						vlogDebug(
								"@Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItem() : isExist : {0} : commerce item id : {1}",
								isExist, commId);
					}
					tr = ensureTransaction();
					synchronized (getOrder()) {
						if (isExist) {
							removeOutOfStockItem(getOrder(), commId);
							processEditCartFromReview();
						}
						updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
					}
				} catch (RepositoryException re) {
					if (isLoggingError()) {
						logError("RepositoryException @Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItem()",
								re);
					}
				} catch (CommerceException ce) {
					if (isLoggingError()) {
						logError("CommerceException @Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItem()",
								ce);
					}
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(HANDLE_UPDATE_COMMERCE_ITEM);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItem() : END");
		}
		return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
	}

	/**
	 * Handle update commerce item. This method is used to update the commerce item.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @return true, if successful
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws RepositoryException the repository exception
	 * @throws CommerceException the commerce exception
	 */
	public boolean handleRemoveOutOfStockItemsForMobile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItemsForMobile() : BEGIN : OutOfStockId : {0}", getOutOfStockId());
		}
		if (null != getOrder() && StringUtils.isNotBlank(getOutOfStockId())) {
			final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_UPDATE_COMMERCE_ITEM))) {
				Transaction tr = null;
				try {
					final boolean isExist = preRemoveOutOfStockItemForMobile(pRequest, pResponse);
						tr = ensureTransaction();
						synchronized (getOrder()) {
						if (isExist) {
								removeAllOutOfStock(getOutOfStockId());
								updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
							}
						}
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(HANDLE_UPDATE_COMMERCE_ITEM);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveOutOfStockItemsForMobile() : END");
		}
		return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
	}
	/**
	/**
	 * Removes the out of stock item.
	 *
	 * @param pOrder the order
	 * @param pCommId            the comm id
	 * @throws RepositoryException             the repository exception
	 */
	private void removeOutOfStockItem(Order pOrder, String pCommId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::removeOutOfStockItem() : BEGIN");
		}
		if (null != getOrder() && null != ((TRUOrderImpl) getOrder()).getOutOfStockItems() && 
				!((TRUOrderImpl) getOrder()).getOutOfStockItems().isEmpty()) {
			List<TRUOutOfStockItem> outOfStockList = ((TRUOrderImpl) getOrder()).getOutOfStockItems();
			for (TRUOutOfStockItem truOutOfStockItem : outOfStockList) {
				if (null != truOutOfStockItem && pCommId.equals(truOutOfStockItem.getId())) {
					if (outOfStockList.size() ==TRUCommerceConstants.INT_ONE) {
						outOfStockList = null;
					} else if (outOfStockList.size() > TRUCommerceConstants.INT_ONE) {
						outOfStockList.remove(truOutOfStockItem);
					}
					break;
				}
			}
			((TRUOrderImpl) pOrder).setOutOfStockItems(outOfStockList);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::removeOutOfStockItem() : END");
		}
	}

	/**
	 * This method will remove all the outofStockItems for mobile channel.
	 *
	 * @param pCommId - CommId
	 * @throws RepositoryException the RepositoryException
	 * @throws CommerceException the CommerceException
	 */
	private void removeAllOutOfStock(String pCommId) throws RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::removeAllOutOfStock() : BEGIN");
		}
		String[] relId = pCommId.split(TRUCommerceConstants.SPLIT_PIPE);
		for (int i = 0; i < relId.length; i++) {
			removeOutOfStockItem(getOrder(), relId[i]);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::removeAllOutOfStock() : END");
		}	
	}
	/**
	/**
	 * Pre remove out of stock item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 */
	public boolean preRemoveOutOfStockItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		boolean isExists = Boolean.FALSE;
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preRemoveOutOfStockItem() : BEGIN");
		}
		String commId = getOutOfStockId();
		List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) getOrder()).getOutOfStockItems();
		if (StringUtils.isNotBlank(commId) && null != outOfStockItems && !outOfStockItems.isEmpty()) {
			vlogDebug("commerceId : {0} : outOfStockItems size : {1}", commId, outOfStockItems.size());
			for (TRUOutOfStockItem item : outOfStockItems) {
				if (null != item.getId() && commId.equalsIgnoreCase(item.getId())) {
					isExists = Boolean.TRUE;
					break;
				}

			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preRemoveOutOfStockItem() : END");
		}
		return isExists;
	}

	/**
	/**
	 * Pre remove out of stock item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 */
	public boolean preRemoveOutOfStockItemForMobile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		boolean isExists = Boolean.FALSE;
		String infoMessage = null;
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preRemoveOutOfStockItemForMobile() : BEGIN");
		}
		String[] relId = getOutOfStockId().split(TRUCommerceConstants.SPLIT_PIPE);
		for (int i = 0; i < relId.length; i++) {
			List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) getOrder()).getOutOfStockItems();
			if (StringUtils.isNotBlank(relId[i]) && null != outOfStockItems && !outOfStockItems.isEmpty()) {
				vlogDebug("commerceId : {0} : outOfStockItems size : {1}", relId[i], outOfStockItems.size());
				for (TRUOutOfStockItem item : outOfStockItems) {
					if (null != item.getId() && relId[i].equalsIgnoreCase(item.getId())) {
						isExists = Boolean.TRUE;
						break;
					}
				}
			}	
		}
		if (!isExists) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.INVALID_OUTOFSTOCK_ID);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException systemException) {
				if (isLoggingError()) {
					logError("OutOfStockItem Not Found: ", systemException);
				}	
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preRemoveOutOfStockItemForMobile() : END");
		}
		return isExists;
	}	
	/**
	 * Gets the donation commerce id.
	 * 
	 * @return the donation commerce id
	 */
	private String getDonationCommerceId() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getDonationCommerceId() : BEGIN");
		}
		if (null != getOrder()) {
			final List<CommerceItem> commerceItems = getOrder().getCommerceItems();
			if (!commerceItems.isEmpty()) {
				for (CommerceItem commerceItem : commerceItems) {
					if (commerceItem instanceof TRUDonationCommerceItem) {
						return commerceItem.getId();
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getDonationCommerceId() : END");
		}
		return null;
	}

	/**
	 * Handle add donation item to order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleAddDonationItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddDonationItemToOrder() : BEGIN");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_ADD_DONATION_ITEM))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final String donationCommerceId = getDonationCommerceId();
					if (isLoggingDebug()) {
						vlogDebug("donationCommerceId : {0} : DonationAmount : {1}", donationCommerceId, getDonationAmount());
					}
					if (donationCommerceId != null) {
						final CommerceItem commerceItem = getOrder().getCommerceItem(donationCommerceId);
						if (commerceItem != null) {
						final Double totalDonationAmount = ((TRUDonationCommerceItem) commerceItem).getDonationAmount() + getDonationAmount();
							((TRUDonationCommerceItem) commerceItem).setDonationAmount(totalDonationAmount);
							getPurchaseProcessHelper().runProcessRepriceOrder(
									getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
									getUserLocale(), getProfile(), createRepriceParameterMap(), this);
							updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
						}
					} else {
						setSiteId(getStoreConfiguration().getToysRUsSiteId());
						super.handleAddItemToOrder(pRequest, pResponse);
					}
				}
			} catch (CommerceItemNotFoundException cinfe) {
				if (isLoggingError()) {
					logError("CommerceItemNotFoundException in handleAddDonationItemToOrder : ", cinfe);
				}
			} catch (InvalidParameterException ipe) {
				if (isLoggingError()) {
					logError("InvalidParameterException in handleAddDonationItemToOrder : ", ipe);
				}
			} catch (RunProcessException runProcessException) {
				if (isLoggingError()) {
					logError("RunProcessException in handleAddDonationItemToOrder : ", runProcessException);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_ADD_DONATION_ITEM);
				}
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddDonationItemToOrder() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getRestSuccessURL(), getRestErrorURL(), pRequest, pResponse);
		}
		return false;
	}

	/**
	 * Handle add update bpp item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean handleAddUpdateBppItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddUpdateBppItem() : BEGIN");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final Order order = getOrder();
		if (null != order) {
			if (isLoggingDebug()) {
				vlogDebug("order id : {0} : BPPInfoId : {1} : RelationShipId : {2} : BppSkuId : {3} : BppItemId : {4}",
						order.getId(), getBPPInfoId(), getRelationShipId(), getBppSkuId(), getBppItemId());
			}
			if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_ADD_UPDATE_BPP_ITEM))) {
				Transaction tr = null;
				try {
					tr = ensureTransaction();
					synchronized (order) {
						preAddUpdateBppItem(pRequest, pResponse);
						if (!getFormError()) {
							if (StringUtils.isBlank(getBPPInfoId())) {
								((TRUOrderTools) getOrderManager().getOrderTools()).addBppItemToOrder(order, getRelationShipId(),
										getBppSkuId(), getBppItemId());
							} else if (StringUtils.isNotBlank(getBPPInfoId())) {
								((TRUOrderTools) getOrderManager().getOrderTools()).updateBppItemInOrder(order, getRelationShipId(),
										getBppSkuId(), getBppItemId());
							}
							Map extraParams= createRepriceParameterMap();
							if(extraParams==null){
								extraParams=new HashMap();
							}
							extraParams.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
							extraParams.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE,Boolean.FALSE);
							runRepricingProcess(getRepriceOrderChainId(), PricingConstants.OP_REPRICE_ORDER_TOTAL,getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), extraParams);							
							updateOrder(order, TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
						}
					}
				} catch (RelationshipNotFoundException rnfe) {
					if (isLoggingError()) {
						logError("RelationshipNotFoundException in handleAddUpdateBppItem : ", rnfe);
					}
				} catch (InvalidParameterException ipe) {
					if (isLoggingError()) {
						logError("InvalidParameterException in handleAddUpdateBppItem : ", ipe);
					}
				} catch (RepositoryException re) {
					if (isLoggingError()) {
						logError("RepositoryException in handleAddUpdateBppItem : ", re);
					}
				} catch (RunProcessException rpe) {
					if (isLoggingError()) {
						logError("RunProcessException in handleAddUpdateBppItem : ", rpe);
					}
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(HANDLE_ADD_UPDATE_BPP_ITEM);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddUpdateBppItem() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return Boolean.FALSE;
	}

	/**
	 * Pre add update bpp item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	public void preAddUpdateBppItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCartModifierFormHandler::@method::preAddUpdateBppItem() : BEGIN : getRelationShipId() : {0}",
					getRelationShipId());
		}
		if (StringUtils.isBlank(getRelationShipId()) && StringUtils.isBlank(getBppSkuId()) && StringUtils.isBlank(getBppItemId())) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException se) {
				if (isLoggingError()) {
					logError("SystemException in @Class::TRUCartModifierFormHandler::@method::preAddUpdateBppItem()", se);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preAddUpdateBppItem() : END");
		}
	}

	/**
	 * Handle remove bpp item relationship.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleRemoveBPPItemRelationship(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveBPPItemRelationship() : BEGIN");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUCommerceConstants.HANDLE_REMOVE_BPP_ITEM_METHOD;
		if (null != getOrder()) {
			vlogDebug("order id : {0} : RelationShipId : {1}", getOrder().getId(), getRelationShipId());
			if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
				Transaction tr = null;
				try {
					tr = ensureTransaction();
					preRemoveBPPItemRelationship(pRequest, pResponse);
					if (!getFormError()) {
						synchronized (getOrder()) {
							((TRUOrderTools) getOrderManager().getOrderTools())
									.removeBPPItemInfo(getOrder(), getRelationShipId());
							getPurchaseProcessHelper().runProcessRepriceOrder(
									getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
									getUserLocale(), getProfile(), createRepriceParameterMap(), this);
							getOrderManager().updateOrder(getOrder());
						}
					}
				} catch (RelationshipNotFoundException rnfe) {
					if (isLoggingError()) {
						vlogError(rnfe, "RelationshipNotFoundException in @method: handleRemoveBPPItemRelationship");
					}
				} catch (InvalidParameterException ipe) {
					if (isLoggingError()) {
						vlogError(ipe, "InvalidParameterException in @method: handleRemoveBPPItemRelationship");
					}
				} catch (RepositoryException repositoryException) {
					if (isLoggingError()) {
						vlogError(repositoryException, "RepositoryException in @method: handleRemoveBPPItemRelationship");
					}
				} catch (CommerceException commerceException) {
					if (isLoggingError()) {
						vlogError(commerceException, "CommerceException in @method: handleRemoveBPPItemRelationship");
					}
				} catch (RunProcessException runProcessException) {
					if (isLoggingError()) {
						vlogError(runProcessException, "RunProcessException in @method: handleRemoveBPPItemRelationship");
					}
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleRemoveBPPItemRelationship() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return false;
	}

	/**
	 * Pre remove bpp item relationship.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	public void preRemoveBPPItemRelationship(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCartModifierFormHandler::@method::preRemoveBPPItemRelationship() : BEGIN : getRelationShipId() : {0}",
					getRelationShipId());
		}
		if (StringUtils.isBlank(getRelationShipId())) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException se) {
				if (isLoggingError()) {
					logError("SystemException in @Class::TRUCartModifierFormHandler::@method::preRemoveBPPItemRelationship()", se);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preRemoveBPPItemRelationship() : END");
		}
	}

	/**
	 * Gets the BPP info id.
	 * 
	 * @return the BPP info id
	 */
	public String getBPPInfoId() {
		return mBPPInfoId;
	}

	/**
	 * Sets the BPP info id.
	 * 
	 * @param pBPPInfoId
	 *            the new BPP info id
	 */
	public void setBPPInfoId(String pBPPInfoId) {
		mBPPInfoId = pBPPInfoId;
	}

	/**
	 * Gets the update commer item ids.
	 * 
	 * @return the UpdateCommerItemIds
	 */
	public String[] getUpdateCommerItemIds() {
		return mUpdateCommerItemIds;
	}

	/**
	 * Sets the update commer item ids.
	 * 
	 * @param pUpdateCommerItemIds
	 *            the UpdateCommerItemIds
	 */
	public void setUpdateCommerItemIds(String[] pUpdateCommerItemIds) {
		this.mUpdateCommerItemIds = pUpdateCommerItemIds;
	}

	/**
	 * Gets the adds the gift wrap to item error url.
	 * 
	 * @return the adds the gift wrap to item error url
	 */
	public String getAddGiftWrapToItemErrorURL() {
		return mAddGiftWrapToItemErrorURL;
	}

	/**
	 * Sets the adds the gift wrap to item error url.
	 * 
	 * @param pAddGiftWrapToItemErrorURL
	 *            the new adds the gift wrap to item error url
	 */
	public void setAddGiftWrapToItemErrorURL(String pAddGiftWrapToItemErrorURL) {
		mAddGiftWrapToItemErrorURL = pAddGiftWrapToItemErrorURL;
	}

	/**
	 * Gets the adds the gift wrap to item success url.
	 * 
	 * @return the adds the gift wrap to item success url
	 */
	public String getAddGiftWrapToItemSuccessURL() {
		return mAddGiftWrapToItemSuccessURL;
	}

	/**
	 * Sets the adds the gift wrap to item success url.
	 * 
	 * @param pAddGiftWrapToItemSuccessURL
	 *            the new adds the gift wrap to item success url
	 */
	public void setAddGiftWrapToItemSuccessURL(String pAddGiftWrapToItemSuccessURL) {
		mAddGiftWrapToItemSuccessURL = pAddGiftWrapToItemSuccessURL;
	}

	/**
	 * Gets the item id for gift wrap.
	 * 
	 * @return the item id for gift wrap
	 */
	public String getItemIdForGiftWrap() {
		return mItemIdForGiftWrap;
	}

	/**
	 * Sets the item id for gift wrap.
	 * 
	 * @param pItemIdForGiftWrap
	 *            the new item id for gift wrap
	 */
	public void setItemIdForGiftWrap(String pItemIdForGiftWrap) {
		mItemIdForGiftWrap = pItemIdForGiftWrap;
	}


	/**
	 * Gets the PayPal Utility Manager.
	 * 
	 * @return the PayPal Utility Manager.
	 */
	public TRUPaypalUtilityManager getPayPalUtilityManager() {
		return mPayPalUtilityManager;
	}

	/**
	 * Sets the PayPal Utility Manager.
	 * 
	 * @param pPayPalUtilityManager
	 *            the new PayPal Utility Manager.
	 */
	public void setPayPalUtilityManager(TRUPaypalUtilityManager pPayPalUtilityManager) {
		this.mPayPalUtilityManager = pPayPalUtilityManager;
	}	
	
	/**
	 * get shipping group.
	 * 
	 * @return the shipping group
	 */
	@Override
	public ShippingGroup getShippingGroup() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getShippingGroup() : BEGIN");
		}
		ShippingGroup sg = null;
		try {
			final TRUPurchaseProcessHelper purchaseProcessHelper = (TRUPurchaseProcessHelper) getPurchaseProcessHelper();
			final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
			final Map<String, Address> addressMap = getProfileTools().findDefaultAddressMap(getProfile(),
					getShippingGroupMapContainer());
			// getting default nickName
			String nickName = null;
			if (addressMap != null && !addressMap.isEmpty()) {
				for (String key : addressMap.keySet()) {
					nickName = key;
					break;
				}
			}
			if(StringUtils.isNotBlank(nickName)) {
				sg = orderManager.getShipGroupByNickName(getOrder(),
						nickName);
			}
			if (sg == null) {
				sg= purchaseProcessHelper.getFirstHardgoodShippingGroup(getOrder());
			}			
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				logError("Commerce Exception in TRUCartModifierFormHandler", commerceException);
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getShippingGroup() : END");
		}
		return sg;
	}

	/**
	 * This method is used to fetch if the order has only InstorepickupSG.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void postMoveToCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::postMoveToCheckout() : BEGIN");
		}
		TRUOrderImpl order=  (TRUOrderImpl) getOrder();
		((TRUOrderImpl) getOrder()).setActiveTabs(null);
		((TRUOrderImpl) getOrder()).setApplyGiftingOptions(Boolean.FALSE);
		if(order.isValidationFailed()) {
			createCartCookies(order, pRequest, pResponse);
		}
		//Start :: TUW-43619/46853 defect changes.
		Profile profile = (Profile)getProfile();
		String selectedCCNickName = (String)profile.getPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName());
		RepositoryItem defaultCreditCard = getProfileTools().getDefaultCreditCard(getProfile());
		if (selectedCCNickName == null && defaultCreditCard != null) {
			String profileCCNickName = getProfileTools().getCreditCardNickname(profile, defaultCreditCard);
			if (profileCCNickName != null) {
				profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(),profileCCNickName);
			}
		}
		if(profile.isTransient()){
			String defaultCreditCardName = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
			profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(),defaultCreditCardName);
		}
		//setCreditCardTypeInOrder();
		//End :: TUW-43619/46853 defect changes.
		final boolean isOrderOnlyInStorePickupItems = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper())
				.isOrderHavingOnlyInstorePickupItems(order);
		final boolean isOrderOnlyDonationItems = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper())
				.isOrderHavingOnlyDonationItems(order);
		//((TRUOrderImpl)getOrder()).isShowMeGiftingOptions();
		((TRUOrderManager)getOrderManager()).updateShowMeGiftingOptions(order);
		if (isOrderOnlyInStorePickupItems) {
			order.setShowMeGiftingOptions(false);
			order.setCurrentTab(TRUCheckoutConstants.PICKUP_TAB);
		} else if (isOrderOnlyDonationItems) {
			order.setShowMeGiftingOptions(false);
			order.setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
		} else {
			final boolean orderMandateForMultiShipping = order.isOrderMandateForMultiShipping();
			if (orderMandateForMultiShipping) {
				order.setOrderIsInMultiShip(true);
			} else {
				order.setOrderIsInMultiShip(false);
			}
			//START: TUW-52846
			if(order.getShipToHomeItemsCount() > TRUConstants._0) {
				order.setCurrentTab(TRUCheckoutConstants.SHIPPING_TAB);
			} else if(order.isOrderIsInStorePickUp()) {
				order.setShowMeGiftingOptions(false);
				order.setCurrentTab(TRUCheckoutConstants.PICKUP_TAB);
			}
			//END: TUW-52846
		}
		//Start : This method will check if all the cart items having eligible ship methods or nor 
		((TRUShippingGroupManager)getShippingGroupManager()).validateShippingMethodAndPrice(order);
		//End : This method will check if all the cart items having eligible ship methods or nor
		//Start : CR161
		if(order.getStorePickUpInfos()!=null && !order.getStorePickUpInfos().isEmpty()){
			order.getStorePickUpInfos().clear();
		}
		//End  : CR161
		//Start : code added for defect:TUW-64041
		if(((TRUOrderImpl)getOrder()).isOrderIsEligibleForFinancing()){
			validateFinanceAgreement(order);
		}
		//End :code added for defect:TUW-64041
		try {
			getShippingGroupManager().removeEmptyShippingGroups(order);
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				vlogError(commerceException, "RunProcessException in @method: postMoveToCheckout");
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::postMoveToCheckout() : END");
		}
	}

	

	/**
	 * This method will validate finance agreement for synchrony.
	 * @param pOrder - order
	 */
	private void validateFinanceAgreement(TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::validateFinanceAgreement() : BEGIN");
		}
		int prev_financingTermValue = ((TRUOrderImpl)getOrder()).getFinancingTermsAvailable();
		//boolean prev_financeAgreed = ((TRUOrderImpl)getOrder()).isFinanceAgreed();
		boolean clear = Boolean.FALSE;
		((TRUOrderTools) getOrderManager().getOrderTools()).updateOrderIfEligibleForFinancing(pOrder);
		int current_financingTermValue = ((TRUOrderImpl)getOrder()).getFinancingTermsAvailable();
		if(prev_financingTermValue == TRUCheckoutConstants.CHECKOUT_FINANCING_SIX_MONTHS && current_financingTermValue == TRUCheckoutConstants.CHECKOUT_FINANCING_TWELVE_MONTHS){
			((TRUOrderImpl)getOrder()).setFinanceAgreed(false);
			clear =Boolean.TRUE;
		}

		if(prev_financingTermValue == TRUCheckoutConstants.CHECKOUT_FINANCING_TWELVE_MONTHS && current_financingTermValue == TRUCheckoutConstants.CHECKOUT_FINANCING_SIX_MONTHS){
			((TRUOrderImpl)getOrder()).setFinanceAgreed(false);	
			clear =Boolean.TRUE;
		}

		if(clear && !((TRUOrderImpl)getOrder()).getActiveAgreementsVO().isEmpty()){
			for (String cardNumber : ((TRUOrderImpl)getOrder()).getActiveAgreementsVO().keySet()) {
				((TRUOrderImpl)getOrder()).getActiveAgreementsVO().get(cardNumber).setFinanceAgreed(TRUConstants.INTEGER_NUMBER_ZERO);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::validateFinanceAgreement() : END");
		}
	}

	/**
	 * Gets the out of stock id.
	 * 
	 * @return the out of stock id
	 */
	public String getOutOfStockId() {
		return mOutOfStockId;
	}

	/**
	 * Sets the out of stock id.
	 * 
	 * @param pOutOfStockId
	 *            the new out of stock id
	 */
	public void setOutOfStockId(String pOutOfStockId) {
		this.mOutOfStockId = pOutOfStockId;
	}

	/**
	 * This method is used to handle express checkout with PAYPAL if token is already captured, user is redirected to order review
	 * page.
	 * 
	 * @param pRequest
	 *            - the request
	 * @param pResponse
	 *            - the response
	 * @return the boolean value boolean value
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean handleCartExpressCheckoutWithPayPal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleCartExpressCheckoutWithPayPal() : BEGIN");
		}
		boolean rollbackFlag = false;
		final String payPalErrKey = TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		String payPalErrMessage = null;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUPayPalConstants.PAYPAL_HANDLER_CART_EXPRESS_CHECKOUT;
		final TransactionDemarcation td = new TransactionDemarcation();
		String successURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(),getTruConfiguration().getCartCheckoutWithPayPalSuccessURL(),null);
		String errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme() ,getTruConfiguration().getCartCheckoutWithPayPalErrorURL(),null);
		
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " \n PayPal Return Failure URL :: "+errorURL);
			logDebug("PayPalToken already captured and is not expired.");
		}
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(myHandleMethod);
				}
				// starting transaction for order update
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				//Start TUW-56572
				if(StringUtils.isBlank(getPayPalToken())){		
					setPayPalToken(((TRUOrderImpl)getOrder()).getPaypalToken());
				}			
				GetExpressCheckoutResponse getExpressResponse = null;
				synchronized (getOrder()) {					
					((TRUOrderImpl)getOrder()).setValidationFailed(Boolean.FALSE);				 
					Map parameters = new HashMap();					
					TRUUserSession sessionComponent = getUserSession();
					parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
					parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.MOVE_TO_PURCHASE_INFO);
					((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(),
							getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
					if(((TRUOrderImpl)getOrder()).isValidationFailed()){
						if(isLoggingInfo()){
							vlogInfo("Out Of Stock items found for the order {0} and redirecting to cart page", getOrder().getId());
						}
						if (isRestService()) {
							errorURL = getAddItemToOrderErrorURL();
						}
						//To redirect to shoppingCart
						successURL = errorURL;
					}
					//If no OOS found update the payap PG to order
					if(!((TRUOrderImpl)getOrder()).isValidationFailed()){
						//Create/Update Paypal PG
						updatePaypalPGToOrder();
						//Get the paypal shipping/billing address
						getExpressResponse = getPayPalUtilityManager().payPalGetExpressCheckout(getOrder(), getProfile(), TRUPayPalConstants.CART_PAGE_NAME, 
										getShippingGroupMapContainer(), getPaymentGroupMapContainer());	
						if (getExpressResponse != null && getExpressResponse.isSuccess()) {
							fetchPayPalPGFromOrder(pRequest, pResponse, rollbackFlag, payPalErrMessage);
							if(getFormError()){
								((TRUOrderImpl)getOrder()).setCurrentTab(TRUCheckoutConstants.SHIPPING_TAB);
								errorURL = successURL;
								if(isLoggingDebug()){
									vlogDebug("Found shipping restrictions :{0} and reditecting to {1}",getFormExceptions(),errorURL);
								}	
							}
						} else {
							rollbackFlag = true;
							getPayPalUtilityManager().addPayPalErrorMessage(getExpressResponse, this);
						}	
					}				
					if(!getFormError()){														
						//Repricing
						getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(),
								getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap(), this);
						if(((TRUOrderImpl)getOrder()).isValidationFailed()){
							((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
						}
						//Update Order
						if(isLoggingDebug()){
							logDebug("Before Update :"+((TRUOrderImpl)getOrder()).getVersion() );
						}
						getOrderManager().updateOrder(getOrder());	
						if(isLoggingDebug()){
							logDebug("After Update :"+((TRUOrderImpl)getOrder()).getVersion() );
						}
					}
					
				}//End of the synchronized block		
											
			} catch (RunProcessException | SystemException |TransactionDemarcationException | InstantiationException | IllegalAccessException | ClassNotFoundException | CommerceException |
					TRUPayPalException | IntrospectionException exe) {
				rollbackFlag = true;
				addFormException(new DropletException(payPalErrMessage));
				if (isLoggingError()) {
					logError("Exception occured while updating the order during PayPal payment for order Id : " + getOrder().getId(), exe);
				}
			}finally {
				try {
					if (td != null) {
						td.end(rollbackFlag);
					}
				} catch (TransactionDemarcationException tdE) {
					addFormException(new DropletException(payPalErrMessage));
					if (isLoggingError()) {
						logError("TRUCartModifierFormHandler.handleCartExpressCheckoutWithPayPal", tdE);
					}
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(myHandleMethod);
				}
			}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(myHandleMethod);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleCartExpressCheckoutWithPayPal() : END");
		}
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return checkFormRedirect(successURL, errorURL, pRequest, pResponse);
	}
	
	/**
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws RunProcessException
	 *             RunProcessException
	 * @throws IOException - IOException
	 * @throws ServletException - ServletException
	 * @return true or false
	 */
	protected boolean runProcessValidateShippingGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RunProcessException, ServletException, IOException {
		Map<String, Object> extraParams = new HashMap<String, Object>();
		//extraParams.put(TRUConstants.TRU_SHIPPING_MANAGER, getOrderManager());
		//extraParams.put(TRUConstants.TRU_SHIPPING_TOOLS, getShippingManager().getShippingTools());
		final PipelineResult result = runProcess(getValidateShippingGroupsChainId(), getOrder(), getUserPricingModels(),
				getUserLocale(pRequest, pResponse), getProfile(), extraParams);
		return processPipelineErrors(result);
	}

	
	/**
	 * This form handler validates all the form required business rules for Apple pay.
	 * 
	 * @param pRequest -- request
	 * @param pResponse -- response
	 * @return -- a boolean value
	 * @throws ServletException -- servlet exception
	 * @throws IOException -- IO exception
	 */
	public boolean handleBuyNowWithApplePay(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse)throws ServletException, IOException {

		String methodname = "handleBuyNowWithApplePay";
		if(isLoggingDebug()) {
			logDebug("TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay()method:STARTS");
		}
		RepeatingRequestMonitor repeatingRequestMonitor = getRepeatingRequestMonitor();     
		if ((repeatingRequestMonitor == null) || (repeatingRequestMonitor.isUniqueRequestEntry(methodname))) {
			Transaction tr = null;
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(methodname);
					}
				tr = ensureTransaction();
				if (!checkFormRedirect(null, getBuyNowWithApplePayErrorURL(), pRequest, pResponse)) {
					if (isLoggingDebug()) {
						logDebug("Form error at beginning of TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay, redirecting.");
					}
					return false;
				}
				preBuyNowWithApplePay(pRequest, pResponse);
				if (getFormError()) {
					return checkFormRedirect(null, getBuyNowWithApplePayErrorURL(),pRequest, pResponse);
				}
				TRUOrderHolder shoppingCart = (TRUOrderHolder)getShoppingCart();
				shoppingCart.setBuyNowWithAP(null);
				TRUOrderImpl order = (TRUOrderImpl)getOrderManager().createOrder(getProfile().getRepositoryId());
				if (order == null) {
					String msg = formatUserMessage(MSG_NO_ORDER_TO_MODIFY, pRequest, pResponse);
					throw new ServletException(msg);
				}
				order.setApplePayPDP(true);
				shoppingCart.setBuyNowWithAP(order);
				setOrder(order);
				synchronized(order) {

					doAddItemToApplepayOrder(order,pRequest, pResponse);

					//If any form errors found, redirect to error URL:
					if (! checkFormRedirect(null, getBuyNowWithApplePayErrorURL(), pRequest, pResponse)) {
						return false;
					}
					postBuyNowWithApplePay(pRequest, pResponse);

					updateOrder(getOrder(), MSG_ERROR_UPDATE_ORDER, pRequest, pResponse);
				} // synchronized



			} catch (CommerceException ce) {
				processException(ce, MSG_ERROR_ADDING_TO_ORDER, pRequest, pResponse);
				PerformanceMonitor.cancelOperation(methodname);
			}finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(methodname);
				}
				if (repeatingRequestMonitor != null) {
					repeatingRequestMonitor.removeRequestEntry(methodname); 
				}                 
			}
		}
		if(isLoggingDebug()) {
			logDebug("TRUApplePayCheckoutFormHandler.handleBuyNowWithApplePay()method:ENDS");	
		}

		return checkFormRedirect(getBuyNowWithApplePaySuccessURL(),getBuyNowWithApplePayErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * Do add item to applepay order.
	 *
	 * @param pOrder the order
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	protected void doAddItemToApplepayOrder(TRUOrderImpl pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()) {
			logDebug("Starting doAddItemsToOrder");
		}
		if (pOrder == null) {
			String msg = formatUserMessage(MSG_NO_ORDER_TO_MODIFY, pRequest, pResponse);
			throw new ServletException(msg);
		}

		Map extraParams = createRepriceParameterMap();
		if(extraParams == null){
			extraParams = new HashMap();
		}
		extraParams.put(TRUCommerceConstants.PAYMENT_METHOD, TRUCommerceConstants.APPLE_PAY);
		try {
			
		    String [] skuIds = getCatalogRefIds();
		    //String productId = getProductId();

		      // Allocate an items array and fill it in from other properties.
		      //
		      setAddItemCount(skuIds.length);
		      for (int index = 0; index < skuIds.length; index++) {
		        AddCommerceItemInfo input = getItems()[index];
		        input.setCatalogRefId(skuIds[index].trim());
		        input.setQuantity(getQuantity());
				input.setProductId(getProductId());
		        input.setCommerceItemType(getCommerceItemType());
		        input.setSiteId(getSiteId());
				if (getLocationId() != null) {
		        	input.setLocationId(getLocationId());
		        	  Double wareHouseLocationId = getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(getLocationId());
		        	  if (wareHouseLocationId != null) {
							long longValue = wareHouseLocationId.longValue();
							((TRUAddCommerceItemInfo) input).setWarehouseLocationCode(String.valueOf(longValue));
						}	  
		        }
		      }
		      
			ShippingGroup sg = getShippingGroupForApplePayOrder(pOrder);
			List<CommerceItem> items = ((TRUPurchaseProcessHelper)getPurchaseProcessHelper())
					.addItemToApplePayOrder(pOrder, sg, getProfile(),
							  getItems(), getUserLocale(),
							  getCatalogKey(pRequest, pResponse),
							  getUserPricingModels(), this,extraParams);
			
			setAddItemsToOrderResult(items);
		}
		catch (CommerceException ce) {
			processException(ce, MSG_ERROR_ADDING_TO_ORDER, pRequest, pResponse);
		} catch (RepositoryException e) {
			processException(e, MSG_ERROR_ADDING_TO_ORDER, pRequest, pResponse);
		}
}


	/**
	 * This fetch pay pal payment group from order.
	 * @param pRequest - request
	 * @param pResponse - rResponse
	 * @param pRollbackFlag - rollbackFlag
	 * @param pPayPalErrMessage - payPal errMessage
	 * @throws CommerceException - commerce exception
	 * @throws RunProcessException - runProcess exception
	 * @throws ServletException - servlet exception
	 * @throws IOException - IOException
	 */
	private void fetchPayPalPGFromOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, boolean pRollbackFlag,
			String pPayPalErrMessage) throws CommerceException,
			RunProcessException, ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::fetchPayPalPGFromOrder() : BEGIN");
		}
		if (!getFormError() && !pRollbackFlag) {
			// fetch PAYPAL payment group
			final TRUPayPal payPalPG = ((TRUOrderManager) getOrderManager()).fetchPayPalPaymentGroupForOrder(getOrder());
			if (payPalPG != null && payPalPG.getBillingAddress() != null && getBillingAddressValidator() != null) {
				final BillingAddrValidator bav = getBillingAddressValidator();
				if (bav instanceof BillingAddrValidatorImpl) {
					final Collection addressCollection = ((BillingAddrValidatorImpl) bav).validateAddress(payPalPG
							.getBillingAddress());
					if (addressCollection.isEmpty()) {
						// calling run process move to confirmation pipeline
						getShippingGroupManager().removeEmptyShippingGroups(getOrder());
						runProcessMoveToConfirmation(pPayPalErrMessage, pRequest, pResponse);
						getUserSession().setSignInHidden(Boolean.TRUE);
						if (isLoggingDebug()) {
							logDebug("Move to Order Review page.");
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::fetchPayPalPGFromOrder() : END");
		}
	}

	/**
	 * This method is used to handle set express checkout with PAYPAL.
	 * 
	 * @param pRequest
	 *            - the request
	 * @param pResponse
	 *            - the response
	 * @return the boolean value boolean value
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	public boolean handleSetExpressCheckoutWithPayPal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleSetExpressCheckoutWithPayPal() : BEGIN");
		}
		boolean rollbackFlag = false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionDemarcation td = new TransactionDemarcation();
		String errorURL = TRUUrlUtil.getConstructedUrl(pRequest, getSchemeDetectionUtil().getScheme(), getTruConfiguration().getPayPalExpChkoutCancelUrl(), null);
		String successURL = TRUUrlUtil.getConstructedUrl(pRequest,  getSchemeDetectionUtil().getScheme(),getTruConfiguration().getPayPalExpChkoutSuccessUrl(),null);
		String methodName = TRUPayPalConstants.PAYPAL_SET_HANDLER_EXPRESS_CHECKOUT;
		if (isLoggingDebug()) {
			logDebug("PayPal Return Success URL :: "+successURL+ " \nPayPal Return Failure URL "+errorURL);
		}
		final String payPalErrKey =  TRUErrorKeys.TRU_PAYPAL_GENERIC_ERR_KEY;
		String payPalErrMessage = null;
		TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(methodName))) {
			SetExpressCheckoutResponse setExpressCheckoutResponse = null;
			try {	
				//Get the default error message
				payPalErrMessage = getErrorHandlerManager().getErrorMessage(payPalErrKey);
				if(orderImpl.getCommerceItemCount() == TRUConstants._0){
					if(isLoggingInfo()){
						vlogDebug("orderImpl.getTotalItemCount() is 0 for  profile {0} , order {1} and redirecting to shoppingCart ", getProfile().getRepositoryId(), orderImpl.getId());
					}	
					if (isRestService()) {
						return checkFormRedirect(null, getAddItemToOrderErrorURL(), pRequest, pResponse);
					}
					return checkFormRedirect(errorURL, null, pRequest, pResponse);
				}											
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRUPayPalConstants.PAYPAL_SET_HANDLER_EXPRESS_CHECKOUT);
				}
				// starting transaction for order update
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);				
				preMoveToCheckout(pRequest, pResponse);
				if(orderImpl.isValidationFailed()){	
					addFormException(new DropletException(ORDER_VALIDATION_FAILED));
					if(isLoggingInfo()){
						vlogInfo("Out Of Stock items found for the order {0} and redirecting to cart page", orderImpl.getId());
					}
					if (isRestService()) {
						return checkFormRedirect(null, getAddItemToOrderErrorURL(), pRequest, pResponse);
					}
					return false;
				}	
				//Setting paypal express checkout flag
				orderImpl.setExpressPayPal(Boolean.TRUE);
				orderImpl.setCurrentTab(TRUCommerceConstants.CART);
				//SetExpress API call
				setExpressCheckoutResponse = ((TRUPaypalUtilityManager) getPayPalUtilityManager()).payPalSetExpressCheckout(
						getPaymentGroupMapContainer(), pRequest.getLocale().toString(), getOrder(), errorURL, successURL,getProfile(), false);
				if (isLoggingDebug()) {
					vlogDebug("SetExpressCheckoutResponse : {0}" , setExpressCheckoutResponse);
				}
				if (!getFormError() && setExpressCheckoutResponse != null && setExpressCheckoutResponse.isSuccess()) {
					rollbackFlag = processPayPal(payPalErrMessage, setExpressCheckoutResponse);
					if(!rollbackFlag) {
						getUserSession().setSignInHidden(Boolean.TRUE);
					}
				} else {
					//rollbackFlag = true;
					getPayPalUtilityManager().addPayPalErrorMessage(setExpressCheckoutResponse, this);
				}
			} catch (SystemException |TRUPayPalException | CommerceException | TransactionDemarcationException exe) {
				rollbackFlag = true;
				getPayPalUtilityManager().addPayPalErrorMessage(setExpressCheckoutResponse, this);
				if (isLoggingError()) {
					logError("Exception occurred while updating the order during PayPal payment for order Id : " + 
							getOrder().getId(), exe);
				}
			} finally {
				try {
					if (td != null) {
						td.end(rollbackFlag);
					}
				} catch (TransactionDemarcationException e) {
					//Reset form exceptions if any
					resetFormExceptions();
					addFormException(new DropletException(payPalErrMessage));
					if (isLoggingError()) {
						logError("@Class::TRUCartModifierFormHandler::TransactionDemarcationException occurred", e);
					}
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRUPayPalConstants.PAYPAL_SET_HANDLER_EXPRESS_CHECKOUT);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(methodName);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleSetExpressCheckoutWithPayPal() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return false;
	}
	
	
	/**
	 * This method is used to Initiate Apple pay Checkout.
	 * 
	 * @param pRequest
	 *            - the request
	 * @param pResponse
	 *            - the response
	 * @return the boolean value boolean value
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	public boolean handleInitiateApplepayCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleInitiateApplepayCheckout() : BEGIN");
		}
		boolean rollbackFlag = false;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionDemarcation td = new TransactionDemarcation();
		TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUCheckoutConstants.APPLEPAY_CHECKOUT))) {
			try {	
				//Get the default error message
				if(orderImpl.getCommerceItemCount() == TRUConstants._0){
					addFormException(new DropletException(COMMERCE_ITEM_COUNT_ZERO));
					if(isLoggingInfo()){
						vlogDebug("orderImpl.getTotalItemCount() is 0 for  profile {0} , order {1} and redirecting to shoppingCart ", getProfile().getRepositoryId(), orderImpl.getId());
					}	
					return checkFormRedirect(getMoveToPurchaseInfoSuccessURL(), getMoveToPurchaseInfoErrorURL(), pRequest, pResponse);
				}											
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRUCheckoutConstants.APPLEPAY_CHECKOUT);
				}
				// starting transaction for order update
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);				
				preMoveToCheckout(pRequest, pResponse);
				if(orderImpl.isValidationFailed()){	
					addFormException(new DropletException(ORDER_VALIDATION_FAILED));
					if(isLoggingInfo()){
						vlogInfo("Out Of Stock items found for the order {0} and redirecting to cart page", orderImpl.getId());
					}						
					return checkFormRedirect(getMoveToPurchaseInfoSuccessURL(), getMoveToPurchaseInfoErrorURL(), pRequest, pResponse);
				}	
				TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
				
				synchronized (orderImpl) {
					if(orderImpl instanceof TRUOrderImpl){
						orderImpl.setCreditCardType(null);
						orderImpl.setApplepay(Boolean.TRUE);
					}
					//validation of payment groups 
					orderManager.ensureOnlyApplePayPamentGroup(orderImpl);
					// calling REPRICE ORDER chain
					getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, orderImpl,
							getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap(), this);
					if (isLoggingDebug()) {
						logDebug("@Class::TRUCartModifierFormHandler::@method::handleInitiateApplepayCheckout() : OrderAmount"
								+ getOrder().getPriceInfo());
					}
					orderManager.updateOrder(getOrder());
				}
				
			} catch (RunProcessException | CommerceException | TransactionDemarcationException exe) {
				rollbackFlag = true;
				processException(exe, TRUCommerceConstants.ERROR_MOVE_TO_PURCHASEINFO_MSG_ID, pRequest, pResponse);
				
				if (isLoggingError()) {
					logError("Exception occurred while updating the order during Initiating Apple pay for order Id : " + 
							getOrder().getId(), exe);
				}
			} finally {
				try {
					if (td != null) {
						td.end(rollbackFlag);
					}
				} catch (TransactionDemarcationException transactionDemarcationException) {
					processException(transactionDemarcationException, TRUCommerceConstants.ERROR_MOVE_TO_PURCHASEINFO_MSG_ID, pRequest, pResponse);
					if (isLoggingError()) {
						logError("TRUCartModifierFormHandler.handleInitiateApplepayCheckout", transactionDemarcationException);
					}
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRUCheckoutConstants.APPLEPAY_CHECKOUT);
				}
			}
		}
		if (rrm != null) {
			rrm.removeRequestEntry(TRUCheckoutConstants.APPLEPAY_CHECKOUT);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleInitiateApplepayCheckout() : END");
		}
		
		return checkFormRedirect(getMoveToPurchaseInfoSuccessURL(), getMoveToPurchaseInfoErrorURL(), pRequest, pResponse);
		
	}

	/**
	 * Process pay pal.
	 *
	 * @param pPayPalErrMessage the pay pal err message
	 * @param pSetExpressCheckoutResponse the set express checkout response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private boolean processPayPal(String pPayPalErrMessage, SetExpressCheckoutResponse pSetExpressCheckoutResponse)
			throws ServletException, IOException {
		boolean rollbackFlag;
		String payPalToken = pSetExpressCheckoutResponse.getToken();
		String timeStamp = pSetExpressCheckoutResponse.getTimeStamp();
		if (isLoggingDebug()) {
			logDebug("payPalToken : " + payPalToken);
			logDebug("payPalTokenTimeStamp : " + timeStamp);
		}
		// capturing PAYPAL details in user session component
		if (StringUtils.isNotBlank(payPalToken) && StringUtils.isNotBlank(timeStamp) && getUserSession() != null) {
			Map<String, String> paypalPaymentDetailsMap = getUserSession().getPaypalPaymentDetailsMap();
			if (paypalPaymentDetailsMap == null) {
				paypalPaymentDetailsMap = new HashMap<String, String>();
			}
			paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN, payPalToken);
			paypalPaymentDetailsMap.put(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP, timeStamp);
			getUserSession().setPaypalPaymentDetailsMap(paypalPaymentDetailsMap);
			setPayPalToken(payPalToken);
			if (isRestService()) {
				setPaypalRedirectURL(getRadialConfiguration().getPaypalRedirectURL()+payPalToken);
			}
			// handle update order with PAYPAL
			rollbackFlag = updateOrderWithPayPal(pPayPalErrMessage);
		} else {
			rollbackFlag = true;
			addFormException(new DropletException(pPayPalErrMessage));
			if (isLoggingError()) {
				logError("Paypal token is empty/null " + payPalToken);
			}
		}
		return rollbackFlag;
	}

	/**
	 * This method is used to update order with PAYPAL token and create PAYPAL payment group in Order.
	 *  It also updates the tabs if PAYPAL token is not yet expired.
	 * 
	 * @return the boolean value boolean value
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */

	protected boolean updateOrderPayPalDetails() throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : BEGIN");
		}
		boolean rollbackFlag = false;
		final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		try {
			synchronized (getOrder()) {
			//Updating the credit card type in order
			if(getOrder() instanceof TRUOrderImpl){
				TRUOrderImpl order = (TRUOrderImpl) getOrder();
				order.setCreditCardType(null);
			}
			// calling REPRICE ORDER chain
			/*getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(),
					getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap(), this);*/
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : OrderAmount" + getOrder().getPriceInfo());
			}
			// updating order & payment groups			
				TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
				paymentGroupManager.removeOtherPaymentGroups(getOrder());
				orderManager.addPayPalPaymentGroupToOrder(getOrder(), getPaymentGroupMapContainer(), getPayPalToken());

				orderManager.updatePayPalOrderDetails(getOrder());
				//orderManager.updateOrder(getOrder());
			}
		} catch (CommerceException cE) {
			rollbackFlag = true;
		//	addFormException(new DropletException(pPayPalErrMessage));
			if (isLoggingError()) {
				logError("CommerceException occurred while updating the order during cart paypal express checkout", cE);
			}
		} /*catch (RunProcessException rpE) {
			rollbackFlag = true;
			//addFormException(new DropletException(pPayPalErrMessage));
			if (isLoggingError()) {
				logError("CommerceException occurred while updating the order during cart paypal express checkout", rpE);
			}
		}*/
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : END");
		}
		return rollbackFlag;
	}

	/**
	 * This method is used to update order with PAYPAL token and create PAYPAL payment group in Order.
	 * 
	 * @param pPayPalErrMessage
	 *            the pay pal err message
	 * @return the boolean value boolean value
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */

	protected boolean updateOrderWithPayPal(String pPayPalErrMessage) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleUpdateOrderWithPayPal() : BEGIN");
		}
		boolean rollbackFlag = false;
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		try {
			// updating order & payment groups
			synchronized (getOrder()) {
				TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
				paymentGroupManager.removeOtherPaymentGroups(getOrder());
				orderManager.addPayPalPaymentGroupToOrder(getOrder(), getPaymentGroupMapContainer(), getPayPalToken());
				// calling REPRICE ORDER chain
				getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(),
						getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap(), this);
				if (isLoggingDebug()) {
					logDebug("@Class::TRUCartModifierFormHandler::@method::handleUpdateOrderWithPayPal() : OrderAmount"
							+ getOrder().getPriceInfo());
				}
				orderManager.updateOrder(getOrder());
			}
		} catch (CommerceException cE) {
			rollbackFlag = true;
			addFormException(new DropletException(pPayPalErrMessage));
			if (isLoggingError()) {
				logError("CommerceException occurred while updating the order during cart paypal express checkout", cE);
			}
		} catch (RunProcessException rpE) {
			rollbackFlag = true;
			addFormException(new DropletException(pPayPalErrMessage));
			if (isLoggingError()) {
				logError("CommerceException occurred while updating the order during cart paypal express checkout", rpE);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleUpdateOrderWithPayPal() : END");
		}
		return rollbackFlag;
	}

	/**
	 * This method will check if PAYPAL Token already captured.
	 * 
	 * @param pRequest
	 *            - the request.
	 * @param pResponse
	 *            - the response
	 * @return isAlreadyToken if PAYPAL Token already captured.
	 * @throws ServletException
	 *             - the ServletException
	 * @throws IOException
	 *             - the IOException
	 */

	protected boolean isPaypalTokenAlreadyCaptured(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::isPaypalTokenAlreadyCaptured() : BEGIN");
		}
		boolean isAlreadyToken = false;
		String payPalToken = null;
		String payPalPayerId = null;
		String tokenTimeStamp = null;
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
		// fetch PAYPAL details map from session
		Map<String, String> paypalPaymentDetailsMap = getUserSession().getPaypalPaymentDetailsMap();
		if (paypalPaymentDetailsMap != null && !paypalPaymentDetailsMap.isEmpty()) {
			payPalToken = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN);
			payPalPayerId = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYER_ID);
			tokenTimeStamp = paypalPaymentDetailsMap.get(TRUPayPalConstants.PAYPAL_TOKEN_TIMESTAMP);
			if (!StringUtils.isBlank(payPalToken) && !StringUtils.isBlank(tokenTimeStamp)) {
				DateFormat formatter = null;
				Date tokenDate = null;
				formatter = new SimpleDateFormat(TRUPaypalConstants.ORDER_DATE_FORMAT, Locale.getDefault());
				try {
					tokenDate = (Date) formatter.parse(tokenTimeStamp);
				} catch (ParseException pE) {
					if (isLoggingError()) {
						logError("ParseException in isPaypalTokenAlreadyCaptured " + "while parsing string" + tokenTimeStamp, pE);
					}
				}
				if (tokenDate != null) {
					final long diff = orderManager.getDateDiff(tokenDate, new Date(), TimeUnit.MINUTES);
					if (diff > Long.valueOf(getTruConfiguration().getPayPalTokenExpiryTime())) {
						if (isLoggingDebug()) {
							logDebug("Paypal Token captured has already expired, need to authenticate again.");
						}
						return false;
					}
					setPayPalToken(payPalToken);
					if (!StringUtils.isBlank(payPalPayerId) && !getFormError()) {
						setPayerId(payPalPayerId);
						isAlreadyToken = true;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::isPaypalTokenAlreadyCaptured() : END");
		}
		return isAlreadyToken;
	}

	/**
	 * This method invokes the pipeline chain "moveToConfirmation" to process the order.
	 * 
	 * @param pPayPalErrMessage
	 *            - the payPalErrMessage
	 * @param pRequest
	 *            - the request.
	 * @param pResponse
	 *            - the response
	 * @throws RunProcessException
	 *             - the RunProcessException
	 * @throws ServletException
	 *             - the ServletException
	 * @throws IOException
	 *             - the IOException
	 */
	public void runProcessMoveToConfirmation(String pPayPalErrMessage, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RunProcessException, ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::runProcessMoveToConfirmation() : BEGIN : moveToConfirmationChainId : START");
		}
		Map params = new HashMap(TRUPayPalConstants.DIGIT_ELEVEN);
		params.put(PricingConstants.PRICING_OPERATION_PARAM, PricingConstants.OP_REPRICE_ORDER_TOTAL);
		final PipelineResult result = runProcess(getMoveToConfirmationChainId(), getOrder(), getUserPricingModels(),
				getUserLocale(pRequest, pResponse), getProfile(), params, null);
		if (processPipelineErrors(result)) {
			for (Object error : result.getErrorKeys()) {
				if (isLoggingError()) {
					logError("ProfileId=" + getProfile().getRepositoryId() + " Pipeline error :: " + result.getError(error));
				}
			}
			//addFormException(new DropletException(pPayPalErrMessage));
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::runProcessMoveToConfirmation() : END");
		}
	}

	/**
	 * This method is override to change the order ConcurrentUpdateAttempt error message.
	 * 
	 * @param pKey
	 *            the key
	 * @param pParam
	 *            the param
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return the string
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public String formatUserMessage(String pKey, Object pParam, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		if (pKey.contains(TRUCommerceConstants.SHOPPINGCART_CONCURRENT_UPDATE_ATTEMPT)) {
			try {
				final String infoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.TRU_CONCURRENT_ORDER_UPDATE);
				return infoMessage;
			} catch (SystemException e) {
				if (isLoggingError()) {
					logError(" SystemException in @Class::TRUCartModifierFormHandler::@method::formatUserMessage() :", e);
				}
			}

		}
		return PurchaseUserMessage.format(pKey, getUserLocale(pRequest, pResponse));
	}

	/**
	 * Gets the adds the shop local item to order success url.
	 * 
	 * @return the mAddShopLocalItemToOrderSuccessURL
	 */
	public String getAddShopLocalItemToOrderSuccessURL() {
		return mAddShopLocalItemToOrderSuccessURL;
	}

	/**
	 * Sets the adds the shop local item to order success url.
	 * 
	 * @param pAddShopLocalItemToOrderSuccessURL
	 *            the pAddShopLocalItemToOrderSuccessURL to set
	 */
	public void setAddShopLocalItemToOrderSuccessURL(String pAddShopLocalItemToOrderSuccessURL) {
		this.mAddShopLocalItemToOrderSuccessURL = pAddShopLocalItemToOrderSuccessURL;
	}

	/**
	 * Gets the bpp sku id.
	 * 
	 * @return the bpp sku id
	 */
	public String getBppSkuId() {
		return mBppSkuId;
	}

	/**
	 * Sets the bpp sku id.
	 * 
	 * @param pBppSkuId
	 *            the new bpp sku id
	 */
	public void setBppSkuId(String pBppSkuId) {
		mBppSkuId = pBppSkuId;
	}

	/**
	 * Gets the bpp item id.
	 * 
	 * @return the bpp item id
	 */
	public String getBppItemId() {
		return mBppItemId;
	}

	/**
	 * Sets the bpp item id.
	 * 
	 * @param pBppItemId
	 *            the new bpp item id
	 */
	public void setBppItemId(String pBppItemId) {
		mBppItemId = pBppItemId;
	}

	/**
	 * Pre move to purchase info.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void preMoveToCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preMoveToCheckout() : START");
		}
		try {			
			TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();
			synchronized (orderImpl) {
				
				orderImpl.setValidationFailed(Boolean.FALSE);				 
				Map parameters = new HashMap();
				
				TRUUserSession sessionComponent = getUserSession();
				parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
				parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.MOVE_TO_PURCHASE_INFO);
				((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(),
						getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
				if(orderImpl.isValidationFailed()){
					runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
					((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
					getOrderManager().updateOrder(getOrder());
				}				
			}
		}catch (RunProcessException rpE) {
			if (isLoggingError()) {
				logError("RunProcessException occurred TRUCartModifierFormHandler::@method::preMoveToCheckout()", rpE);
			}
			
		}catch (CommerceException rpE) {
			if (isLoggingError()) {
				logError("CommerceException occurred TRUCartModifierFormHandler::@method::preMoveToCheckout()", rpE);
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::preMoveToCheckout() : END");
		}
	}

	/**
	 * Creates the missed ispu s gs in order based on sg.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pSelShippingGroup
	 *            the sel shipping group
	 * @param pLocationId
	 *            the location id
	 * @return the shipping group
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public ShippingGroup createMissedIspuSGsInOrderBasedOnSG(Order pOrder, ShippingGroup pSelShippingGroup, String pLocationId)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrderBasedOnSG() : BEGIN");
		}

		ShippingGroup shippingGroup = null;
		try {
			if (pSelShippingGroup instanceof TRUChannelHardgoodShippingGroup) {
				if (isLoggingDebug()) {
					vlogDebug("There is no TRUPurchaseProcessHelper is exists to merge :: hence creating a new shipping group");
				}
				final TRUChannelDetailsVo channelDetailsVo = ((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).
						createChannelDetailVO((TRUChannelHardgoodShippingGroup) pSelShippingGroup);
				shippingGroup = ((TRUShippingGroupManager) getShippingGroupManager()).addChannelISPUShippingGroupToOrder(pOrder,
						channelDetailsVo, pLocationId);
				if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup ispsg = (TRUInStorePickupShippingGroup) shippingGroup;
					ispsg.setLocationId(pLocationId);
					ispsg.setWarhouseLocationCode(Long.toString(getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(pLocationId)
							.longValue()));
				}
				return shippingGroup;
			} else if (pSelShippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
				if (isLoggingDebug()) {
					vlogDebug("There is no TRUPurchaseProcessHelper is exists to merge :: hence creating a new shipping group");
				}
				final TRUChannelDetailsVo channelDetailsVo =createChannelDetailVO((TRUChannelInStorePickupShippingGroup) pSelShippingGroup);
				shippingGroup = ((TRUShippingGroupManager) getShippingGroupManager()).addChannelISPUShippingGroupToOrder(pOrder,
						channelDetailsVo, pLocationId);
				TRUChannelInStorePickupShippingGroup ispuChannel = (TRUChannelInStorePickupShippingGroup) pSelShippingGroup;
				ispuChannel.setWarhouseLocationCode(Long.toString(getShoppingCartUtils().getTRUCartModifierHelper().
						getWareHouseLocationId(pLocationId).longValue()));

				return shippingGroup;
			} else if (pSelShippingGroup instanceof TRUHardgoodShippingGroup || pSelShippingGroup instanceof TRUInStorePickupShippingGroup) {
				if (isLoggingDebug()) {
					vlogDebug("There is no TRUPurchaseProcessHelper is exists to merge :: hence creating a new shipping group");
				}
				shippingGroup = getShippingGroupManager().createShippingGroup(
						((TRUOrderTools) getOrderManager().getOrderTools()).getInStorePickupShippingGroupType());

				if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup ispsg = (TRUInStorePickupShippingGroup) shippingGroup;
					ispsg.setLocationId(pLocationId);
					ispsg.setWarhouseLocationCode(Long.toString(getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(pLocationId)
							.longValue()));
				}

				getShippingGroupManager().addShippingGroupToOrder(pOrder, shippingGroup);
				return shippingGroup;
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError(
						"RepositoryException while setting warehouse location code @Class::TRUPurchaseProcessHelper::@method::" + 
								"createMissedIspuSGsInOrderBasedOnSG() ",
						repositoryException);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrderBasedOnSG() : END");
		}

		return shippingGroup;
	}
	
	
	/**
	 * Creates the channel detail vo.
	 * 
	 * @param pChannelInStorePickupShippingGroup
	 *            the channel in store pickup shipping group
	 * @return the TRU channel details vo
	 */
	private TRUChannelDetailsVo createChannelDetailVO(TRUChannelInStorePickupShippingGroup pChannelInStorePickupShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : BEGIN ");
		}
		final TRUChannelDetailsVo channelDetailsVo = new TRUChannelDetailsVo();
		channelDetailsVo.setChannelId(pChannelInStorePickupShippingGroup.getChannelId());
		channelDetailsVo.setChannelType(pChannelInStorePickupShippingGroup.getChannelType());
		channelDetailsVo.setChannelName(pChannelInStorePickupShippingGroup.getChannelName());
		channelDetailsVo.setChannelCoUserName(pChannelInStorePickupShippingGroup.getChannelCoUserName());
		channelDetailsVo.setChannelUserName(pChannelInStorePickupShippingGroup.getChannelUserName());
		channelDetailsVo.setSourceChannel(pChannelInStorePickupShippingGroup.getSourceChannel());
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createChannelDetailVO() : END ");
		}
		return channelDetailsVo;
	}
	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	
	/**
	 * Handle move to checkout.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	public boolean handleMoveToCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleMoveToCheckout() : BEGIN");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "CartModifierOrderFormHandler.handleMoveToCheckout";
		boolean hasException = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				 if (PerformanceMonitor.isEnabled()){
						PerformanceMonitor.startOperation(myHandleMethod);
					}
				
				tr = ensureTransaction();
				if (getUserLocale() == null) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				TRUOrderImpl orderImpl=(TRUOrderImpl)getOrder();
				if (orderImpl == null) {
					String msg = formatUserMessage(TRUCommerceConstants.NO_ORDER_TO_MODIFY_MSG, pRequest, pResponse);
					throw new ServletException(msg);
				}
				synchronized (orderImpl) {	
					if(orderImpl.getTotalItemCount() == TRUConstants._0 && 
							orderImpl.getOutOfStockItems()!=null &&
							orderImpl.getOutOfStockItems().size()==TRUConstants._0){
						if(isLoggingInfo()){
							vlogDebug("orderImpl.getTotalItemCount() is 0 for  profile {0} , order {1} and redirecting to shoppingCart ", getProfile().getRepositoryId(), orderImpl.getId());
						}	
						return false;
					}
					preMoveToCheckout(pRequest, pResponse);
					if(orderImpl.isValidationFailed()){
						if(isLoggingInfo()){
							vlogInfo("Out Of Stock items found for the order {0} and redirecting to cart page", orderImpl.getId());
						}						
						return false;
					}
					getShippingProcessHelper().updateShippingMethodAndPrice(orderImpl,getShippingGroupMapContainer());
					//if (!getProfileTools().isAnonymousUser(getProfile())) {												
						final boolean isDefaultAddressChanged = getProfileTools().isDefaultShippingAddressChangedOnProfile(
								getProfile(), getShippingGroupMapContainer().getDefaultShippingGroupName());
						if (isDefaultAddressChanged) {
							getShippingProcessHelper().updateDefaultShippingAddress(getProfile(), orderImpl, getShippingGroupMapContainer());
							getProfileTools().updatePreviousShippingAddress((MutableRepositoryItem) getProfile(),
									getShippingGroupMapContainer().getDefaultShippingGroupName());
						}
					//}
					postMoveToCheckout(pRequest, pResponse);
					Map parameters = super.createRepriceParameterMap();
					if (parameters == null) {
						parameters = new HashMap();
					}
					parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
					runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), parameters);
					((TRUOrderManager)getOrderManager()).updateOrderBillingAddress((TRUOrderHolder)getShoppingCart(), orderImpl,getShippingGroupMapContainer());
					//Commented out the code as per the Pasquale confirmation.
/*					((TRUOrderManager)getOrderManager()).promoFinancingReset(getOrder());
*/					getOrderManager().updateOrder(getOrder());
					if(orderImpl.isValidationFailed()){
						if(isLoggingInfo()){
							vlogDebug("runProcessTRUValidateForCheckoutChain failed for profile {0} , order {1} and redirecting to shoppingCart ", getProfile().getRepositoryId(), orderImpl.getId());
						}	
						return checkFormRedirect(TRUCommerceConstants.SHOPPING_CART_PAGE_NAME, null, pRequest, pResponse);
					}					
				}
			} catch (RunProcessException exc) {
				if(isLoggingError()){
					vlogError("RunProcessException occured for order : {0} and details {1}", getOrder().getId(),exc);
				}
				hasException = true;
				processException(exc, TRUCommerceConstants.ERROR_MOVE_TO_PURCHASEINFO_MSG_ID, pRequest, pResponse);	
				PerformanceMonitor.cancelOperation(myHandleMethod);
			}catch (CommerceException exc) {
				if(isLoggingError()){
					vlogError("CommerceException occured for order : {0} and details {1}", getOrder().getId(),exc);
				}
				hasException = true;
				processException(exc, TRUCommerceConstants.ERROR_MOVE_TO_PURCHASEINFO_MSG_ID, pRequest, pResponse);
				PerformanceMonitor.cancelOperation(myHandleMethod);
			}   finally {				
				if (tr != null) {								
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (javax.transaction.SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleMoveToCheckout() : END");
		}
		return checkFormRedirect(getMoveToPurchaseInfoSuccessURL(), getMoveToPurchaseInfoErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * pre method for handleBuyNowWithApplePay
	 * sets success and error URLs.
	 *
	 * @param pRequest -- request
	 * @param pResponse - response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void preBuyNowWithApplePay(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if(isLoggingDebug()) {
			logDebug("TRUCartModifierFormHandler.preBuyNowWithApplePay() method.STARTS");
		}

		String [] skuIds = getCatalogRefIds();
		String productId = getProductId();
		long quantity = getQuantity();
		//String locationId = getLocationId();

		if ((skuIds == null) || (skuIds.length == 0)) {
			String msg = formatUserMessage(MSG_NO_ITEMS_TO_ADD, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCommerceConstants.CATALOG_REF_IDS);
			addFormException(new DropletFormException(msg, propertyPath, MSG_NO_ITEMS_TO_ADD));
		}

		if (StringUtils.isBlank(productId)) {
			String msg = formatUserMessage(MSG_NO_ITEMS_TO_ADD, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCommerceConstants.PRODUCT_IDS);
			addFormException(new DropletFormException(msg, propertyPath, MSG_NO_ITEMS_TO_ADD));
		}

		if (quantity ==  0) {
			String msg = formatUserMessage(MSG_NO_ITEMS_TO_ADD, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCommerceConstants.QUANTITY_STRING);
			addFormException(new DropletFormException(msg, propertyPath, MSG_NO_ITEMS_TO_ADD));
		}

	/*	if (StringUtils.isEmpty(locationId)) {
			String msg = formatUserMessage(MSG_NO_ITEMS_TO_ADD, pRequest, pResponse);
			String propertyPath = generatePropertyPath(TRUCommerceConstants.LOCATION_ID);
			addFormException(new DropletFormException(msg, propertyPath, MSG_NO_ITEMS_TO_ADD));
		}
		*/
		if(isLoggingDebug()) {
			logDebug("TRUCartModifierFormHandler.preBuyNowWithApplePay() method.END");
		}	
	}
	
	/**
	 * Gets the shipping group for apple pay order.
	 *
	 * @param pOrder the order
	 * @return the shipping group for apple pay order
	 * @throws CommerceException the commerce exception
	 */
	private ShippingGroup getShippingGroupForApplePayOrder(TRUOrderImpl pOrder) throws CommerceException {

		if(isLoggingDebug()) {
			logDebug("TRUCartModifierFormHandler.getShippingGroupForApplePayOrder() method.START");
		}	
		// Check the shipping group and payment group for the new items.
		//
		ShippingGroup sg = null;
		if ((pOrder.getShippingGroups() != null) && (pOrder.getShippingGroups().size() > 0)) {
			sg = (ShippingGroup)pOrder.getShippingGroups().get(0);
		} else {
			pOrder.addShippingGroup(getShippingGroupManager().createShippingGroup());
	        sg = (ShippingGroup)pOrder.getShippingGroups().get(0);
		}

		if(isLoggingDebug()) {
			logDebug("TRUCartModifierFormHandler.getShippingGroupForApplePayOrder() method.END");
		}	
		
		return sg;

	}

	
	/**
	   * Called after all work is done by the handleBuyNowWithApplePay method.  It currently
	   * does nothing.
	   *
	   * @param pRequest the request object
	   * @param pResponse the response object
	   * @exception ServletException if an error occurs
	   * @exception IOException if an error occurs
	   */
	  public void postBuyNowWithApplePay(DynamoHttpServletRequest pRequest,
	                                 DynamoHttpServletResponse pResponse)
	    throws ServletException, IOException
	  {
	  }
	
	

	/**
	 * Gets the buy now with apple pay error URL.
	 *
	 * @return the buy now with apple pay error URL
	 */
	public String getBuyNowWithApplePayErrorURL() {
		return mBuyNowWithApplePayErrorURL;
	}

	/**
	 * Sets the buy now with apple pay error URL.
	 *
	 * @param pBuyNowWithApplePayErrorURL the new buy now with apple pay error URL
	 */
	public void setBuyNowWithApplePayErrorURL(
			String pBuyNowWithApplePayErrorURL) {
		this.mBuyNowWithApplePayErrorURL = pBuyNowWithApplePayErrorURL;
	}

	/**
	 * Gets the buy now with apple pay success URL.
	 *
	 * @return the buy now with apple pay success URL
	 */
	public String getBuyNowWithApplePaySuccessURL() {
		return mBuyNowWithApplePaySuccessURL;
	}

	/**
	 * Sets the buy now with apple pay success URL.
	 *
	 * @param pBuyNowWithApplePaySuccessURL the new buy now with apple pay success URL
	 */
	public void setBuyNowWithApplePaySuccessURL(
			String pBuyNowWithApplePaySuccessURL) {
		this.mBuyNowWithApplePaySuccessURL = pBuyNowWithApplePaySuccessURL;
	}

	
	/**
	 * Gets the meta info id.
	 *
	 * @return the meta info id
	 */
	public String getMetaInfoId() {
		return mMetaInfoId;
	}

	/**
	 * Sets the meta info id.
	 *
	 * @param pMetaInfoId the new meta info id
	 */
	public void setMetaInfoId(String pMetaInfoId) {
		this.mMetaInfoId = pMetaInfoId;
	}
	
	 /**
		 * Gets the move to confirm chain id.
		 *
		 * @return the moveToConfirmChainId
		 */
		public String getMoveToConfirmChainId() {
			return mMoveToConfirmChainId;
		}
	
		/**
		 * Sets the move to confirm chain id.
		 *
		 * @param pMoveToConfirmChainId the moveToConfirmChainId to set
		 */
		public void setMoveToConfirmChainId(String pMoveToConfirmChainId) {
			mMoveToConfirmChainId = pMoveToConfirmChainId;
		}

		/** The m shipping process helper. */
		private TRUShippingProcessHelper  mShippingProcessHelper;

		/**
		 * Gets the shipping process helper.
		 *
		 * @return the shippingProcessHelper
		 */
		public TRUShippingProcessHelper getShippingProcessHelper() {
			return mShippingProcessHelper;
		}

		/**
		 * Sets the shipping process helper.
		 *
		 * @param pShippingProcessHelper the shippingProcessHelper to set
		 */
		public void setShippingProcessHelper(TRUShippingProcessHelper pShippingProcessHelper) {
			this.mShippingProcessHelper = pShippingProcessHelper;
		}

		/**
		 * Gets the new relation id.
		 *
		 * @return the new relation id
		 */
		public String getNewRelationId() {
			return mNewRelationId;
		}

		/**
		 * Sets the new relation id.
		 *
		 * @param pNewRelationId the new new relation id
		 */
		public void setNewRelationId(String pNewRelationId) {
			this.mNewRelationId = pNewRelationId;
		}
		
		/** The Constant ORDER_VALIDATION_FAILED. */
		private static final String ORDER_VALIDATION_FAILED="orderValidationFailed";
		
		/** The m scheme detection util. */
		private TRUSchemeDetectionUtil mSchemeDetectionUtil;

		/**
		 * Gets the scheme detection util.
		 *
		 * @return the scheme detection util
		 */
		public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
			return mSchemeDetectionUtil;
		}

		/**
		 * Sets the scheme detection util.
		 *
		 * @param pSchemeDetectionUtil the new scheme detection util
		 */
		public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
			this.mSchemeDetectionUtil = pSchemeDetectionUtil;
		}
		
		
		/**
		 * This method is used to update order with PAYPAL token and create PAYPAL payment group in Order.
		 *  It also updates the tabs if PAYPAL token is not yet expired.
		 * @throws SystemException 
		 */

		protected void updatePaypalPGToOrder() throws SystemException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : BEGIN");
			}
			final TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
			try {
				//Updating the credit card type in order
				if(getOrder() instanceof TRUOrderImpl){
					TRUOrderImpl order = (TRUOrderImpl) getOrder();
					order.setCreditCardType(null);
				}				
				if (isLoggingDebug()) {
					logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : OrderAmount" + getOrder().getPriceInfo());
				}
				// updating order & payment groups			
					TRUPaymentGroupManager paymentGroupManager = (TRUPaymentGroupManager) getPaymentGroupManager();
					paymentGroupManager.removeOtherPaymentGroups(getOrder());
					orderManager.addPayPalPaymentGroupToOrder(getOrder(), getPaymentGroupMapContainer(), getPayPalToken());
					orderManager.updatePayPalOrderDetails(getOrder());
			} catch (CommerceException cE) {
				if (isLoggingError()) {
					logError("CommerceException occurred while updating the order during cart paypal express checkout", cE);
				}
				throw new SystemException(cE.getMessage());
			} 
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::updateOrderPayPalDetails() : END");
			}
		}
}