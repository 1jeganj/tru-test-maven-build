package com.tru.commerce.order.purchase;

import atg.core.util.ContactInfo;
import atg.nucleus.GenericService;

/**
 * The Class TRUApplePayForm.
 */
public class TRUApplePayForm extends GenericService {
	
	/** The m personal email. */
	private String mPersonalEmail;
	
	/** The m personal phone. */
	private String mPersonalPhone;
	
	/** Holds billing address values. */
	private ContactInfo mBillingAddress  = new ContactInfo();
	
	/** Holds shipping address values. */
	private ContactInfo mShippingAddress  = new ContactInfo();
	
	/** The m apple pay transaction id. */
	private String mApplePayTransactionId;
	
	/** The m ephemeral public key. */
	private String mEphemeralPublicKey;
	
	/** The m public key hash. */
	private String mPublicKeyHash;
	
	/** The m data. */
	private String mData;
	
	/** The m signature. */
	private String mSignature;
	
	/** The m version. */
	private String mVersion;
	

	/**
	 * Gets the personal email.
	 *
	 * @return the personal email
	 */
	public String getPersonalEmail() {
		return mPersonalEmail;
	}

	/**
	 * Sets the personal email.
	 *
	 * @param pPersonalEmail the new personal email
	 */
	public void setPersonalEmail(String pPersonalEmail) {
		this.mPersonalEmail = pPersonalEmail;
	}

	/**
	 * Gets the personal phone.
	 *
	 * @return the personal phone
	 */
	public String getPersonalPhone() {
		return mPersonalPhone;
	}

	/**
	 * Sets the personal phone.
	 *
	 * @param pPersonalPhone the new personal phone
	 */
	public void setPersonalPhone(String pPersonalPhone) {
		this.mPersonalPhone = pPersonalPhone;
	}

	/**
	 * Gets the billing address.
	 *
	 * @return the billing address
	 */
	public ContactInfo getBillingAddress() {
		return mBillingAddress;
	}

	/**
	 * Sets the billing address.
	 *
	 * @param pBillingAddress the new billing address
	 */
	public void setBillingAddress(ContactInfo pBillingAddress) {
		this.mBillingAddress = pBillingAddress;
	}

	/**
	 * Gets the apple pay transaction id.
	 *
	 * @return the apple pay transaction id
	 */
	public String getApplePayTransactionId() {
		return mApplePayTransactionId;
	}

	/**
	 * Sets the apple pay transaction id.
	 *
	 * @param pApplePayTransactionId the new apple pay transaction id
	 */
	public void setApplePayTransactionId(String pApplePayTransactionId) {
		this.mApplePayTransactionId = pApplePayTransactionId;
	}

	/**
	 * Gets the ephemeral public key.
	 *
	 * @return the ephemeral public key
	 */
	public String getEphemeralPublicKey() {
		return mEphemeralPublicKey;
	}

	/**
	 * Sets the ephemeral public key.
	 *
	 * @param pEphemeralPublicKey the new ephemeral public key
	 */
	public void setEphemeralPublicKey(String pEphemeralPublicKey) {
		this.mEphemeralPublicKey = pEphemeralPublicKey;
	}

	/**
	 * Gets the public key hash.
	 *
	 * @return the public key hash
	 */
	public String getPublicKeyHash() {
		return mPublicKeyHash;
	}

	/**
	 * Sets the public key hash.
	 *
	 * @param pPublicKeyHash the new public key hash
	 */
	public void setPublicKeyHash(String pPublicKeyHash) {
		this.mPublicKeyHash = pPublicKeyHash;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public String getData() {
		return mData;
	}

	/**
	 * Sets the data.
	 *
	 * @param pData the new data
	 */
	public void setData(String pData) {
		this.mData = pData;
	}

	/**
	 * Gets the signature.
	 *
	 * @return the signature
	 */
	public String getSignature() {
		return mSignature;
	}

	/**
	 * Sets the signature.
	 *
	 * @param pSignature the new signature
	 */
	public void setSignature(String pSignature) {
		this.mSignature = pSignature;
	}

	/**
	 * Gets the version.
	 *
	 * @return the mVersion
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * Sets the version.
	 *
	 * @param pVersion the new version
	 */
	public void setVersion(String pVersion) {
		this.mVersion = pVersion;
	}

	/**
	 * @return the mShippingAddress
	 */
	public ContactInfo getShippingAddress() {
		return mShippingAddress;
	}

	/**
	 * @param pShippingAddress the mShippingAddress to set
	 */
	public void setShippingAddress(ContactInfo pShippingAddress) {
		this.mShippingAddress = pShippingAddress;
	}
	

}
