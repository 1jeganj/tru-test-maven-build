/**
 * 
 */
package com.tru.commerce.order;

import atg.commerce.order.InStorePickupShippingGroup;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;

/**
 * The Class TRUInStorePickupShippingGroup.
 *
 * @author PA.
 * @version 1.0.
 */
public class TRUInStorePickupShippingGroup extends InStorePickupShippingGroup{
	
	/** serial id *. */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the phone number.
	 *
	 * @return the PhoneNumber.
	 */
	public String getPhoneNumber() {
		return (String) getPropertyValue(TRUCheckoutConstants.PHONE_NUMBER);
	}

	/**
	 * Sets the phone number.
	 *
	 * @param pPhoneNumber - the PhoneNumber
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		setPropertyValue(TRUCheckoutConstants.PHONE_NUMBER, pPhoneNumber);
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email.
	 */
	public String getEmail() {
		return (String) getPropertyValue(TRUCheckoutConstants.EMAIL);
	}

	/**
	 * Sets the email.
	 *
	 * @param pEmail - the Email
	 */
	public void setEmail(String pEmail) {
		setPropertyValue(TRUCheckoutConstants.EMAIL, pEmail);
	}
	
	/**
	 * Checks if is alt addr required.
	 *
	 * @param pIsAltAddrReq - the is alternate address required
	 */
	public void setAltAddrRequired(boolean pIsAltAddrReq) {
		 setPropertyValue(TRUCheckoutConstants.IS_ALT_ADDR_REQ,pIsAltAddrReq);
	}
	
	/**
	 * Gets the alt addr required.
	 *
	 * @return the alt addr required
	 */
	public Boolean isAltAddrRequired() {
		return (Boolean)getPropertyValue(TRUCheckoutConstants.IS_ALT_ADDR_REQ);
	}
	
	/**
	 * Gets the alt first name.
	 *
	 * @return the alternate first name.
	 */
	public String getAltFirstName() {
		return (String) getPropertyValue(TRUCheckoutConstants.ALT_FIRST_NAME);
	}

	/**
	 * Sets the alt first name.
	 *
	 * @param pAltFirstName - the alternate first name
	 */
	public void setAltFirstName(String pAltFirstName) {
		setPropertyValue(TRUCheckoutConstants.ALT_FIRST_NAME, pAltFirstName);
	}
	
	/**
	 * Gets the alt last name.
	 *
	 * @return the alternate last name.
	 */
	public String getAltLastName() {
		return (String) getPropertyValue(TRUCheckoutConstants.ALT_LAST_NAME);
	}

	/**
	 * Sets the alt last name.
	 *
	 * @param pAltLastName - the alternate last name
	 */
	public void setAltLastName(String pAltLastName) {
		setPropertyValue(TRUCheckoutConstants.ALT_LAST_NAME, pAltLastName);
	}
	
	/**
	 * Gets the alt phone number.
	 *
	 * @return the alternate phone number.
	 */
	public String getAltPhoneNumber() {
		return (String) getPropertyValue(TRUCheckoutConstants.ALT_PHONE_NUMBER);
	}

	/**
	 * Sets the alt phone number.
	 *
	 * @param pAltPhoneNumber - the alternate phone number
	 */
	public void setAltPhoneNumber(String pAltPhoneNumber) {
		setPropertyValue(TRUCheckoutConstants.ALT_PHONE_NUMBER, pAltPhoneNumber);
	}
	
	/**
	 * Gets the alt email.
	 *
	 * @return the alternate email.
	 */
	public String getAltEmail() {
		return (String) getPropertyValue(TRUCheckoutConstants.ALT_EMAIL);
	}
	
	/**
	 * Sets the alt email.
	 *
	 * @param pAltEmail - the alternate email
	 */
	public void setAltEmail(String pAltEmail) {
		setPropertyValue(TRUCheckoutConstants.ALT_EMAIL, pAltEmail);
	}

	
	/**
	 * Gets the warhouse location code.
	 *
	 * @return the warhouse location code
	 */
	public String getWarhouseLocationCode() {
		return (String) getPropertyValue(TRUCommerceConstants.WAREHOUSE_LOCATION_CODE);
	}
	
	/**
	 * Sets the warhouse location code.
	 *
	 * @param pWebStoreId the new warhouse location code
	 */
	public void setWarhouseLocationCode(String pWebStoreId) {
		setPropertyValue(TRUCommerceConstants.WAREHOUSE_LOCATION_CODE, pWebStoreId);
	}
	
	/**
	 * Gets the ewaste state.
	 *
	 * @return the ewaste state
	 */
	public String getEwasteState() {
		return (String) getPropertyValue(TRUCommerceConstants.EWASTE_STATE);
	}

	/**
	 * Sets the ewaste state.
	 *
	 * @param pEwasteState the new ewaste state
	 */
	public void setEwasteState(String pEwasteState) {
		setPropertyValue(TRUCommerceConstants.EWASTE_STATE, pEwasteState);
	}
	
	/**
	 * Gets the ship to name.
	 *
	 * @return the mShipToName
	 */
	public String getShipToName() {
		return (String)getPropertyValue(TRUCommerceConstants.PROP_SHIP_TO_NAME);
	}

	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		setPropertyValue(TRUCommerceConstants.PROP_SHIP_TO_NAME, pShipToName);
	}
	
}
