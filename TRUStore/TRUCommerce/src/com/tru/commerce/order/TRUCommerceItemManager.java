package com.tru.commerce.order;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemContainer;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;

import com.tru.common.TRUConstants;

/**
 * The Class TRUCommerceItemManager.
 * @author PA
 * @version 1.0
 */
public class TRUCommerceItemManager extends CommerceItemManager {

	
	/**
	 * This Method is overridden to add the donation amount if already donation item exist, else call super method.
	 *
	 * @param pSrcOrder the src order
	 * @param pDstOrder the dst order
	 * @param pItem the item
	 * @return CommerceItem
	 * @throws CommerceException the commerce exception
	 */
	
	protected CommerceItem mergeOrdersCopyCommerceItem(Order pSrcOrder, Order pDstOrder, CommerceItem pItem)
			throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCommerceItemManager::@method::mergeOrdersCopyCommerceItem() : BEGIN");
			vlogDebug(
					"@Class::TRUCommerceItemManager::@method::mergeOrdersCopyCommerceItem() : pSrcOrder : {0} , pDstOrder : {1} , pItem : {2}",
					pSrcOrder, pDstOrder, pItem);
		}
		if (pItem instanceof TRUDonationCommerceItem) {
			CommerceItem commerceItem = findMatchingItem(pDstOrder, pItem);
			if (commerceItem instanceof TRUDonationCommerceItem) {
				Double destDonationAmount = TRUConstants.DOUBLE_ZERO;
				if(((TRUDonationCommerceItem) commerceItem).getDonationAmount() != null){
					destDonationAmount = ((TRUDonationCommerceItem) commerceItem).getDonationAmount();
				}
				final Double totalDonationAmount = ((TRUDonationCommerceItem) pItem).getDonationAmount()
						+ destDonationAmount;
				((TRUDonationCommerceItem) commerceItem).setDonationAmount(totalDonationAmount);
			} else {
				commerceItem = super.mergeOrdersCopyCommerceItem(pSrcOrder, pDstOrder, pItem);
				((TRUDonationCommerceItem) commerceItem).setDonationAmount(((TRUDonationCommerceItem) pItem)
						.getDonationAmount());
				
			}
			if (isLoggingDebug()) {
				vlogDebug(
						"@Class::TRUCommerceItemManager::@method::mergeOrdersCopyCommerceItem() : Entered into donation"
								+ " item block and donation item id is :", commerceItem.getId());
			}
			return commerceItem;
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCommerceItemManager::@method::mergeOrdersCopyCommerceItem() : END");
		}
		return super.mergeOrdersCopyCommerceItem(pSrcOrder, pDstOrder, pItem);
	}
	
	/**
	 * This Method is overridden to remove the site validation because same item added from different sites
	 * should merge as single line item.
	 *
	 * @param pExistingItem the existing item
	 * @param pNewItem the new item
	 * @return true, if successful
	 */
	
	protected boolean shouldMergeItems(CommerceItem pExistingItem, CommerceItem pNewItem)
	  {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCommerceItemManager::@method::shouldMergeItems() : BEGIN");
			vlogDebug(
					"@Class::TRUCommerceItemManager::@method::shouldMergeItems() : pExistingItem : {0} , pNewItem : {1} ",
					pExistingItem, pNewItem);
		}
	    if (pExistingItem.getClass() != pNewItem.getClass()) {
	      return false;
	    }

	    String existing_id = pExistingItem.getAuxiliaryData().getProductId();
	    String new_id = pNewItem.getAuxiliaryData().getProductId();

	    if (existing_id == null) {
	      if (new_id != null) {
	        return false;
	      }
	    }
	    else if (!(existing_id.equals(new_id))) {
	      return false;
	    }
	    
	    existing_id = pExistingItem.getAuxiliaryData().getSiteId();
	    new_id = pNewItem.getAuxiliaryData().getSiteId();

	    if (existing_id == null && new_id != null) {
	        return false;
	    }
	    
	    if (pExistingItem instanceof CommerceItemContainer) {
	      boolean merge = shouldMergeItemContainers((CommerceItemContainer)pExistingItem, (CommerceItemContainer)pNewItem);

	      if (!(merge)) {
	        return false;
	      }
	    }
	    if (isLoggingDebug()) {
			logDebug("@Class::TRUCommerceItemManager::@method::shouldMergeItems() : END");
		}
	    return true;
	  }
}
