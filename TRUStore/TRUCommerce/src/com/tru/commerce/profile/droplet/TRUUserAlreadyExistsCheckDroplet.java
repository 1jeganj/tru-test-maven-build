package com.tru.commerce.profile.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.profile.TRUProfileTools;

/**
 * This droplet checks whether the User Already Exists or not.
 * @author Professional Access
 * @version 1.0
 */
public class TRUUserAlreadyExistsCheckDroplet extends DynamoServlet{

	/**
	 * Property to hold profileTools.
	 */
	private TRUProfileTools mProfileTools;

	/** The m login profile type. */
	private String mLoginProfileType;

	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * Sets the login profile type.
	 *
	 * @param pLoginProfileType the new login profile type
	 */
	public void setLoginProfileType(String pLoginProfileType)
	{
		mLoginProfileType = pLoginProfileType;
	}

	/**
	 * Gets the login profile type.
	 *
	 * @return the login profile type
	 */
	public String getLoginProfileType()
	{
		return mLoginProfileType;
	}

	/**
	 * To check whether the User Already Exists or not by Email id.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String email = null;
		boolean isUserAlreadyExists = Boolean.FALSE;
		email = (String) pRequest.getLocalParameter(TRUCheckoutConstants.EMAIL);
		if(StringUtils.isNotBlank(email)){
			RepositoryItem user = getProfileTools().getItem(email.toLowerCase().trim(), null, getLoginProfileType());
			if (user != null){
				isUserAlreadyExists = Boolean.TRUE;
			}
		}
		pRequest.setParameter(TRUCommerceConstants.IS_USER_ALREADY_EXISTS, isUserAlreadyExists);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
	}
}