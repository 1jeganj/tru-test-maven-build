package com.tru.commerce.profile.droplet;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;

/**
 * This droplet checks whether the reset password link is expired or not.
 * @author Professional Access
 * @version 1.0
 */
public class TRUResetPasswordLinkExpiryCheckDroplet extends DynamoServlet{

	/**
	 * Property to hold profileTools.
	 */
	private TRUProfileTools mProfileTools;
	
	/**
	 * property for configuration.
	 */
	private TRUConfiguration mConfiguration;
	
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String encodedEmail = null;
		String decodedEmail = null;
		String encodedTimeStamp = null;
		String decodedTimeStamp = null;
		boolean passwordGenerated = Boolean.TRUE;
		boolean linkExpired = Boolean.FALSE;
		
		encodedEmail = (String) pRequest.getLocalParameter(TRUCommerceConstants.ENCODED_EMAIL);
		encodedTimeStamp = (String) pRequest.getLocalParameter(TRUCommerceConstants.ENCODED_TIMESTAMP);
		
		if(StringUtils.isNotBlank(encodedEmail)){
			decodedEmail = getProfileTools().getBase64DecodedString(encodedEmail);
		}
		if(StringUtils.isNotBlank(decodedEmail)){
			passwordGenerated  = getProfileTools().isGeneratedPassword(decodedEmail);
		}
		
		if(StringUtils.isNotBlank(encodedTimeStamp)){
			decodedTimeStamp = getProfileTools().getBase64DecodedString(encodedTimeStamp);
		}
		
		if(decodedTimeStamp != null){
			String currentTimestampString = getProfileTools().getServerTimeStamp();
			Timestamp currentTimestamp = Timestamp.valueOf(currentTimestampString);
			
			Timestamp emailTime = Timestamp.valueOf(decodedTimeStamp);
			Calendar calender = Calendar.getInstance();
			calender.setTimeInMillis(emailTime.getTime());			
			calender.add(Calendar.HOUR_OF_DAY, getConfiguration().getResetPasswordLinkExpiryTime());
			Timestamp emailExpiryTime = new Timestamp(calender.getTimeInMillis());
			
			if(currentTimestamp.after(emailExpiryTime)){
				linkExpired = Boolean.TRUE;
			}
		}
		
		pRequest.setParameter(TRUCommerceConstants.PARAM_GENERATED_PASSWORD, passwordGenerated);
		pRequest.setParameter(TRUCommerceConstants.PARAM_LINK_EXPIRED, linkExpired);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
	}

	/**
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
}