package com.tru.commerce.profile;

import java.beans.IntrospectionException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.jms.JMSException;

import atg.apache.soap.encoding.soapenc.Base64;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.promotion.PromotionTools;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.payment.creditcard.ExtendableCreditCardTools;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfo;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.common.vo.TRUAddressDoctorVO;
import com.tru.integrations.addressdoctor.TRUAddressDoctorService;
import com.tru.integrations.constants.TRUAddressDoctorConstants;
import com.tru.logging.TRUSETALogger;
import com.tru.messaging.TRUEpslonAccountSource;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * TRUProfileTools - Extensions to CommerceProfileTools and also contain repository specific methods and lower level helper
 * methods for ProfileFormHandler.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUProfileTools extends CommerceProfileTools {

	/**
	 * property to hold mDummyAdResponse.
	 */
    private String mDummyAdResponse;
	
	/**
	 * property to hold mExtendableCreditCardTools.
	 */
	private TRUAddressDoctorService mAddressDoctorService;

	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;

	/**
	 * property to hold mStoreConfigurationRepositroy.
	 */
	private Repository mStoreConfigRepository;

	/**
	 * property to hold mExtendableCreditCardTools.
	 */
	private ExtendableCreditCardTools mExtendableCreditCardTools;

	/**
	 * property to hold TRU configuration.
	 */
	private TRUConfiguration mTRUConfiguration;
	/**
	 * property to hold EpslonAccountSource.
	 */
	private TRUEpslonAccountSource mEpslonAccountSource;
	
	/**
	 * property to hold addressCompareExcludedProperties.
	 */
	private List<String> mAddressCompareExcludedProperties;
	
	/**
	 * This property holds SETALogger component ref.
	 */
	private TRUSETALogger mSETALogger;
	
	/**
	 * property to hold enableValidateCard.
	 */
	private boolean mEnableValidateCard;
	
	/**
	 * property to hold mCheckoutUrl.
	 */
	private String mCheckoutUrl;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	

	/**
	 * This method used to create the credit card with the address selected by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pSelectedAddress
	 *            The selected address to be set as billing address
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pAddressValue
	 *            address value map to pass to the update second. address method
	 * @param pPaymentGroupMapContainer 
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 */
	public void addCardWithExistingAddress(Profile pProfile, String pSelectedAddress, Map<String, String> pCreditCardInfoMap,
			Map<String, String> pAddressValue, PaymentGroupMapContainer pPaymentGroupMapContainer) throws RepositoryException, IntrospectionException, 
			InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithExistingAddress) : STARTS");
		}
		updateProfileRepositoryAddress(pProfile, pAddressValue);
		final RepositoryItem billingAddress = getSecondaryAddressByNickName(pProfile, pSelectedAddress);
		if (isLoggingDebug()) {
			logDebug("Billing Address " + billingAddress);
		}
		try {
			createProfileCreditCard(pProfile, pCreditCardInfoMap, null, billingAddress, pPaymentGroupMapContainer);
			String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
			if(!pProfile.isTransient()){
				getSETALogger().logAddCreditCard(login);
				getSETALogger().logAddBillingAddress(login, (String)billingAddress.getRepositoryId());
			}
		} catch (PropertyNotFoundException e) {
			vlogError("PropertyNotFoundException occured while adding credit card with exception : {0}", e);
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithExistingAddress) : ENDS");
		}
	}

	/**
	 * This method used to create the credit card with the new address entered by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pBillingAddressNickName
	 *            The billing address nick name to add address to secondary address
	 * @param pPaymentGroupMapContainer 
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 */
	public void addCardWithNewAddress(Profile pProfile, Map<String, String> pAddressValue,
			Map<String, String> pCreditCardInfoMap, String pBillingAddressNickName, PaymentGroupMapContainer pPaymentGroupMapContainer) throws RepositoryException,
			IntrospectionException, PropertyNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithNewAddress) : STARTS");
		}
		final String creditCardNickName = createProfileCreditCard(pProfile, pCreditCardInfoMap, null, pAddressValue, pPaymentGroupMapContainer);
		final RepositoryItem creditCard = getCreditCardByNickname(creditCardNickName, pProfile);
		if (creditCard != null) {
			RepositoryItem billingAddressFromCard = (RepositoryItem) creditCard
					.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName());
			addProfileRepositoryAddress(pProfile, pBillingAddressNickName, billingAddressFromCard);
			((MutableRepositoryItem)billingAddressFromCard).setPropertyValue(((TRUPropertyManager) getPropertyManager()).getIsBillingAddressPropertyName(), Boolean.TRUE);
			String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
			if(!pProfile.isTransient()){
				getSETALogger().logAddCreditCard(login);
				getSETALogger().logAddBillingAddress(login, (String)billingAddressFromCard.getRepositoryId());
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithNewAddress) : ENDS");
		}
	}
	
	/**
	 * get key from value in a map.
	 *
	 * @param pMapData - Map
	 * @param pValue - Object
	 * @return itrObject - Object
	 */
	@SuppressWarnings("rawtypes")
	public Object getKeyFromValue(Map pMapData, Object pValue) {
	    for (Object itrObject : pMapData.keySet()) {
	      if (pMapData.get(itrObject).equals(pValue)) {
	        return itrObject;
	      }
	    }
		return null;
	}
	
	/**
	 * Remove credit card from profile along will billing address
	 *
	 * @param pProfile the profile
	 * @param pCreditCardName the remove cc nick name
	 * @param pShippingGroupMapContainer 
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("rawtypes")
	public void removeProfileCreditCard(RepositoryItem pProfile, String pCreditCardName, ShippingGroupMapContainer pShippingGroupMapContainer) throws RepositoryException {
		RepositoryItem card = getCreditCardByNickname(pCreditCardName, pProfile);
		String repositoryId = null;
		boolean addressUsedInOtherCard = false;
		if(card != null) {
			TRUPropertyManager propertyManager = (TRUPropertyManager)getPropertyManager();
			RepositoryItem currentbillingAddress = (RepositoryItem)card.getPropertyValue(propertyManager.getBillingAddressPropertyName());
			String currentBillingAddressNickName = getProfileAddressName(pProfile, currentbillingAddress);

			RepositoryItem defaultBillingAddress = (RepositoryItem)pProfile.getPropertyValue(propertyManager.getBillingAddressPropertyName());
			if ((defaultBillingAddress != null) && (defaultBillingAddress.getRepositoryId().equals(currentbillingAddress.getRepositoryId()))) {
				updateProperty(propertyManager.getBillingAddressPropertyName(), null, pProfile);
			}

			Map secondaryAddressMap = (Map)pProfile.getPropertyValue(propertyManager.getSecondaryAddressPropertyName());
			String billingAddressNickName = getKeyFromValue(secondaryAddressMap, currentbillingAddress).toString();


			repositoryId = card.getRepositoryId();

			RepositoryItem defaultCreditCard = getDefaultCreditCard(pProfile);

			if ((defaultCreditCard != null) && (repositoryId.equalsIgnoreCase(defaultCreditCard.getRepositoryId()))){
				updateProperty(propertyManager.getDefaultCreditCardPropertyName(), null, pProfile);
			}

			Map creditCards = getUsersCreditCardMap(pProfile);
			creditCards.remove(pCreditCardName);
			Collection creditCardMapValues = creditCards.values();
			if (!creditCardMapValues.isEmpty()) {
				Iterator creditCarderator = creditCardMapValues.iterator();
				while (creditCarderator.hasNext()) {
					RepositoryItem creditCardDetails = (RepositoryItem)creditCarderator.next();
					RepositoryItem creditCardAddress = (RepositoryItem)creditCardDetails.getPropertyValue(propertyManager.getBillingAddressPropertyName());
					String ccBillingAddressNickName = getProfileAddressName(pProfile, creditCardAddress);
					if (currentBillingAddressNickName.equals(ccBillingAddressNickName)) {
						addressUsedInOtherCard = true;
						break;
					}
				}
			}
			if(!addressUsedInOtherCard){
				removeProfileRepositoryAddress(pProfile, billingAddressNickName);
				if(pShippingGroupMapContainer != null && !pShippingGroupMapContainer.getShippingGroupMap().isEmpty()){
					final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) pShippingGroupMapContainer
							.getShippingGroup(billingAddressNickName);
					if(hgsg != null) {
						if (billingAddressNickName.equalsIgnoreCase(pShippingGroupMapContainer.getDefaultShippingGroupName())) {
							pShippingGroupMapContainer.setDefaultShippingGroupName(null);
						}
						pShippingGroupMapContainer.removeShippingGroup(billingAddressNickName);
					}
				}	
				
			}
			updateProperty(propertyManager.getCreditCardPropertyName(), creditCards, pProfile);
			//SETA Logging 
			String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
			if(!pProfile.isTransient()){
				getSETALogger().logRemoveCreditCard(login);
			}
		}
	}
	 
	
	/**
	 * This method used to create the credit card with the address selected by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pSelectedAddress
	 *            The selected address to be set as billing address
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pCreditCardNickName
	 *            To hold the nick name of the credit card to be updated
	 * @param pAddressValue
	 *            address value map to pass to the update second. address method
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 */
	public void updateCardWithExistingAddress(Profile pProfile, String pSelectedAddress, Map<String, String> pCreditCardInfoMap,
			String pCreditCardNickName, Map<String, String> pAddressValue) throws RepositoryException, IntrospectionException,
			PropertyNotFoundException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (updateCardWithExistingAddress) : STARTS");
		}
		final RepositoryItem creditCard = getCreditCardByNickname(pCreditCardNickName, pProfile);
		updateProfileRepositoryAddress(pProfile, pAddressValue);
		final RepositoryItem billingAddress = getSecondaryAddressByNickName(pProfile, pSelectedAddress);
		final MutableRepository repository = (MutableRepository) pProfile.getRepository();
		final MutableRepositoryItem creditCardToUpdate = (MutableRepositoryItem) creditCard;
		creditCardToUpdate.setPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName(),
				billingAddress);
		repository.updateItem(creditCardToUpdate);
		updateProfileCreditCard(creditCard, pProfile, pCreditCardInfoMap, null, null, null);
		String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
		if(!pProfile.isTransient()){
			getSETALogger().logUpdateCreditCard(login);
			getSETALogger().logUpdateBillingAddress((String)pProfile.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getLoginPropertyName()), billingAddress.getRepositoryId() );
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (updateCardWithExistingAddress) : ENDS");
		}
	}

	/**
	 * This method used to create the credit card with the new address entered by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pBillingAddressNickName
	 *            The billing address nick name to add address to secondary address
	 * @param pCreditCardNickName
	 *            To hold the nick name of the credit card to be updated
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 */
	public void updateCardWithNewAddress(Profile pProfile, Map<String, String> pAddressValue,
			Map<String, String> pCreditCardInfoMap, String pBillingAddressNickName, String pCreditCardNickName)
			throws RepositoryException, IntrospectionException, PropertyNotFoundException, InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (updateCardWithNewAddress) : STARTS");
		}
		final RepositoryItem creditCard = getCreditCardByNickname(pCreditCardNickName, pProfile);
		final Address addressObject = AddressTools.createAddressFromMap(pAddressValue, TRUConstants.ADDRESS_CLASS_NAME);
		createProfileRepositorySecondaryAddress(pProfile, pBillingAddressNickName, addressObject);
		final RepositoryItem billingAddress = getSecondaryAddressByNickName(pProfile, pBillingAddressNickName);
		((MutableRepositoryItem)billingAddress).setPropertyValue(((TRUPropertyManager) getPropertyManager()).getIsBillingAddressPropertyName(), Boolean.TRUE);
		final MutableRepository repository = (MutableRepository) pProfile.getRepository();
		final MutableRepositoryItem creditCardToUpdate = (MutableRepositoryItem) creditCard;
		creditCardToUpdate.setPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName(),
				billingAddress);
		repository.updateItem(creditCardToUpdate);
		updateProfileCreditCard(creditCard, pProfile, pCreditCardInfoMap, null, null, null);
		String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
		if(!pProfile.isTransient()){
			getSETALogger().logUpdateCreditCard(login);
			getSETALogger().logAddBillingAddress((String)pProfile.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getLoginPropertyName()), billingAddress.getRepositoryId() );
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (updateCardWithNewAddress) : ENDS");
		}
	}

	/**
	 * Updates the profile credit card with the creditCardNickname and billingAddressNickname passed.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pAddressObject
	 *            addressObject
	 * @param pCreditCardInfoMap
	 *            creditCardInfoMap
	 * @param pBillingAddressNickName
	 *            billingAddressNickName
	 * @param pCreditCardNickName
	 *            creditCardNickName
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 */
	public void updateCreditCard(RepositoryItem pProfile, ContactInfo pAddressObject, Map<String, String> pCreditCardInfoMap,
			String pBillingAddressNickName, String pCreditCardNickName) throws RepositoryException, IntrospectionException,
			PropertyNotFoundException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		vlogDebug("TRUProfileTools (updateCardWithNewAddress) : STARTS : Card nickname :", pCreditCardNickName);
		if (StringUtils.isNotBlank(pCreditCardNickName) && StringUtils.isNotBlank(pBillingAddressNickName)) {
			final RepositoryItem creditCard = getCreditCardByNickname(pCreditCardNickName, pProfile);
			createProfileRepositorySecondaryAddress(pProfile, pBillingAddressNickName, pAddressObject);
			final RepositoryItem billingAddress = getSecondaryAddressByNickName(pProfile, pBillingAddressNickName);
			final MutableRepository repository = (MutableRepository) pProfile.getRepository();
			final MutableRepositoryItem creditCardToUpdate = (MutableRepositoryItem) creditCard;
			creditCardToUpdate.setPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName(),
					billingAddress);
			repository.updateItem(creditCardToUpdate);
			updateProfileCreditCard(creditCard, pProfile, pCreditCardInfoMap, null, null, null);
		}
		vlogDebug("TRUProfileTools (updateCardWithNewAddress) : ENDS");
	}

	/**
	 * This method used to update profile repository address.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws RepositoryException
	 *             RepositoryException
	 */
	public void updateProfileRepositoryAddress(Profile pProfile, Map<String, String> pAddressValue)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException,
			RepositoryException {
		final Map secondaryAddresses = (Map) pProfile.getPropertyValue(((TRUPropertyManager) getPropertyManager())
				.getSecondaryAddressPropertyName());
		final RepositoryItem secondaryAddress = (RepositoryItem) secondaryAddresses.get(pAddressValue
				.get(TRUConstants.ADDRESS_NICK_NAME));
		final Address address = AddressTools.createAddressFromMap(pAddressValue, TRUConstants.ADDRESS_CLASS_NAME);
		updateProfileRepositoryAddress(secondaryAddress, address);
		if (isLoggingDebug()) {
			logDebug("The Addess Updated in TRUProfileFormHandler.handleEditShippingAddress: ");
		}
	}

	/**
	 * This method used to change the value of Country from UNITED STATES to US and check for other State.
	 * 
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 */
	public void changeCountryValue(Map<String, String> pAddressValue) {
		if ((pAddressValue.containsKey(TRUConstants.COUNTRY_TO_ADD))
				&& pAddressValue.containsValue(TRUConstants.FULL_UNITED_STATES)) {
			pAddressValue.remove(TRUConstants.COUNTRY_TO_ADD);
			pAddressValue.put(TRUConstants.COUNTRY_TO_ADD, TRUConstants.UNITED_STATES);
		}

		if(pAddressValue.containsKey(TRUConstants.OTHER_STATE)){
			String internationaStateValue = pAddressValue.get(TRUConstants.OTHER_STATE);
			if(StringUtils.isNotBlank(internationaStateValue) && !TRUConstants.UNITED_STATES.equals(pAddressValue.get(TRUConstants.COUNTRY_TO_ADD))){
				pAddressValue.remove(TRUConstants.STATE);
				pAddressValue.put(TRUConstants.STATE,internationaStateValue);
			}else if(StringUtils.isBlank(internationaStateValue) &&  !TRUConstants.UNITED_STATES.equals(pAddressValue.get(TRUConstants.COUNTRY_TO_ADD))) {
				pAddressValue.remove(TRUConstants.STATE);
				pAddressValue.put(TRUConstants.STATE, TRUConstants.NA);
			} else {
				pAddressValue.remove(TRUConstants.OTHER_STATE);
			}
		}
	}
	
	/**
	 * This method used to update profile repository and add the ownerId- defect TUW-52324.
	 * 
	 * @param pRepositoryAddress
	 *            repItem
	 * @param pAddress
	 *            The map to hold the address details entered by the user
	 * @throws RepositoryException 
	 * 				RepositoryException
	 */
	public void updateProfileRepositoryAddress(RepositoryItem pRepositoryAddress, Address pAddress)throws RepositoryException {
		
		MutableRepositoryItem item = RepositoryUtils.getMutableRepositoryItem(pRepositoryAddress);
		if (item == null || pAddress == null) {
			return;
		}
		try {
			String OwnerId = (String) item.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getOwnerIdPropertyName());
			OrderTools.copyAddress(pAddress, item);
			item.setPropertyValue(((TRUPropertyManager) getPropertyManager()).getOwnerIdPropertyName(), OwnerId);
		} catch (CommerceException ce) {
			Throwable src = ce.getSourceException();
			if (src instanceof RepositoryException) {
				if (isLoggingError()) {
					vlogError("CommerceException occured while updating profile address with exception : {0}", ce);
				}
				throw ((RepositoryException) src);
			}
			if (isLoggingError()) {
				vlogError("CommerceException occured while updating profile address with exception : {0}", ce);
			}
			throw new RepositoryException(src);
		}
	}


	/**
	 * This method used to trim the string values in a map.
	 * 
	 * @param pMap
	 *            The map to hold the address details entered by the user
	 */
	public void trimMapValues(Map<String, String> pMap) {
		String trimmedValue = null;
		for (Entry<String, String> entry : pMap.entrySet()) {
			if(StringUtils.isNotBlank(entry.getValue())){
				trimmedValue = entry.getValue().trim();
				pMap.remove(entry);
				pMap.put(entry.getKey(), trimmedValue);
			}
		}
	}

	/**
	 * This method used to validate the address properties typed by the customer by calling AddressDoctorService.
	 * 
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 * @return addressStatus
	 */
	public String validateAddress(Map<String, String> pAddressValue) {
		String addressStatus = null;
		if (getAddressDoctorService() != null) {
			String addressDoctorResponse = null;
			 
			if(getTRUConfiguration().isTestAddressDoctor()){
				 addressDoctorResponse = getDummyAdResponse();
			}else{
				 addressDoctorResponse = getAddressDoctorService().validateAddress(pAddressValue);
			}
			if (addressDoctorResponse != null && (!addressDoctorResponse.isEmpty()) && 
					(!addressDoctorResponse.equals(TRUAddressDoctorConstants.ERROR_CODE))) {
				final TRUAddressDoctorResponseVO addressDoctorResponseVO = getAddressDoctorResponseVO(addressDoctorResponse);
				if (isLoggingDebug()) {
					logDebug("Address Doctor VO Respose " + addressDoctorResponseVO);
				}
				if(addressDoctorResponseVO !=null){
					addressStatus = addressDoctorResponseVO.getProcessStatus();
					if(addressStatus != null){
						if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressDoctorResponseVO.getProcessStatus())) {
							setAddressDoctorResponseVO(addressDoctorResponseVO);
							pAddressValue.put(TRUConstants.POSTAL_CODE_ADDRESS, addressDoctorResponseVO.getAddressDoctorVO().get(0)
									.getPostalCode());
						} else if (TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressDoctorResponseVO
								.getProcessStatus())) {
							setAddressDoctorResponseVO(addressDoctorResponseVO);
						} else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressDoctorResponseVO
								.getProcessStatus())) {
							addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
							setAddressDoctorResponseVO(addressDoctorResponseVO);
						}
					}
				}
			} else {
				addressStatus = TRUConstants.CONNECTION_ERROR;
			}
		}
		return addressStatus;
	}

	/**
	 * This method used to send the email to user by calling the EpslonAccountSource.
	 * 
	 * @param pEmail
	 *            - String email property
	 */
	public void sendEpslonEmailToUser(String pEmail) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEpslonEmailToUser) : STARTS");
		}
		vlogDebug("EpslonAccountSource()" + getEpslonAccountSource());
		if (getEpslonAccountSource() != null) {
			try {
				getEpslonAccountSource().sendNewAccountEmail(pEmail);
			} catch (JMSException jmsException) {
				if (isLoggingError()) {
					vlogError("JSONException occured while sending email to epsilon with exception : {0}", jmsException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEpslonEmailToUser) : ENDS");
		}
	}

	/**
	 * This method is used to fetch the secondary address based on the nick name while adding/updating the credit cards.
	 * 
	 * @param pProfile
	 *            RepositoryItem
	 * @param pNickName
	 *            String
	 * @return addressItem RepositoryItem
	 */
	public RepositoryItem getSecondaryAddressByNickName(RepositoryItem pProfile, String pNickName) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (getSecondaryAddressByNickName) : STARTS");
		}
		RepositoryItem addressItem = null;
		final CommercePropertyManager cpmgr = (CommercePropertyManager) getPropertyManager();
		if (pProfile != null) {
			Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) pProfile.getPropertyValue(cpmgr
					.getSecondaryAddressPropertyName());
			if (secondaryAddresses != null && !secondaryAddresses.isEmpty()) {
				addressItem = secondaryAddresses.get(pNickName);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (getSecondaryAddressByNickName) : ENDS" + addressItem);
		}

		return addressItem;
	}

	/**
	 * getValueForProperty is for getting the RepositoryItem based on pPropertyName.
	 * 
	 * @param pItem
	 *            repository item from which value is to be fetched
	 * @return returns the object
	 */
	public Object getValueForProperty(RepositoryItem pItem) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (getValueForProperty) : Starts");
		}
		if (isLoggingDebug()) {
			vlogDebug("pPropertyName : {0} and Item is : {1}",
					((TRUPropertyManager) getPropertyManager()).getSecondaryAddressPropertyName(), pItem);
		}
		Object value = null;
		if (pItem != null) {
			value = pItem.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getSecondaryAddressPropertyName());
		}
		if (isLoggingDebug()) {
			vlogDebug("Value for the pPropertyName : {0} is : {1} Item is : {2}",
					((TRUPropertyManager) getPropertyManager()).getSecondaryAddressPropertyName(), value, pItem);
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (getValueForProperty) : ENDS");
		}
		return value;
	}

	/**
	 * method to retrieve the TRUPropertyManager Object.
	 * 
	 * @return property manager
	 */
	public TRUPropertyManager getTRUPropertyManager() {
		return (TRUPropertyManager) getPropertyManager();
	}

	/**
	 * Overrides CommerceProfileTools.isDuplicateAddressNickName. A case insensitive nickname check.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pNewNickName
	 *            An address nickname
	 * @return true if the nickname is duplicated, false otherwise.
	 */

	@Override
	public boolean isDuplicateAddressNickName(RepositoryItem pProfile, String pNewNickName) {
		final Map secondaryAddressMap = (Map) pProfile
				.getPropertyValue(getTRUPropertyManager().getSecondaryAddressPropertyName());
		return checkForDuplicates(secondaryAddressMap, pNewNickName);
	}

	/**
	 * Checks for duplicates between pKey and the mPropertyMap.keySet().
	 * 
	 * @param pPropertyMap
	 *            is Map input type
	 * @param pKey
	 *            is String input type
	 * @return boolean return type
	 */
	protected boolean checkForDuplicates(Map pPropertyMap, String pKey) {
		final Collection secondaryAddressKeys = pPropertyMap.keySet();

		final List<String> profileNames = new ArrayList<String>();
		final Iterator<String> iterator = secondaryAddressKeys.iterator();
		while (iterator.hasNext()) {
			profileNames.add(iterator.next());
		}

		for (String profileNickname : profileNames) {
			if (profileNickname.equalsIgnoreCase(pKey)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for duplicates between pKey and the mPropertyMap.keySet().
	 * 
	 * @param pProfile
	 *            profile repository item
	 * @param pNewCreditCard
	 *            new credit card map
	 * @param pCreditCardNickname
	 *            credit card nick name
	 * @param pBillingAddress
	 *            billing address object
	 * @param pPaymentGroupMapContainer 
	 * @return String return type
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 */
	public String createProfileCreditCard(RepositoryItem pProfile, Map pNewCreditCard, String pCreditCardNickname,
			Object pBillingAddress, PaymentGroupMapContainer pPaymentGroupMapContainer) throws RepositoryException, IntrospectionException, PropertyNotFoundException {
		final MutableRepository repository = (MutableRepository) pProfile.getRepository();
		final CommercePropertyManager propertyManager = (CommercePropertyManager) getPropertyManager();

		String creditCardNickname = pCreditCardNickname;
		if (StringUtils.isBlank(creditCardNickname)) {
			creditCardNickname = getUniqueCreditCardNickname(pNewCreditCard, pProfile, null);
		}

		MutableRepositoryItem newCreditCard = null;
		// replace card number with token number to save
		pNewCreditCard.put(TRUConstants.CREDIT_CARD_NUMBER, pNewCreditCard.get(TRUConstants.CREDIT_CARD_NUMBER));
		if (pBillingAddress instanceof RepositoryItem) {
			newCreditCard = createCreditCardItem(pProfile, pNewCreditCard, (RepositoryItem) pBillingAddress);
		} else {
			newCreditCard = createCreditCardItem(pProfile, pNewCreditCard);
			if (pBillingAddress != null) {
				final MutableRepositoryItem billingAddress = (MutableRepositoryItem) newCreditCard
						.getPropertyValue(propertyManager.getCreditCardItemDescriptorBillingAddressPropertyName());

				final Set excludedProperties = new HashSet();
				excludedProperties.add(TRUConstants.ID);

				AddressTools.copyObject(pBillingAddress, billingAddress, excludedProperties);
				repository.updateItem(billingAddress);

				if (getDefaultBillingAddress(pProfile) == null) {
					updateProperty(propertyManager.getBillingAddressPropertyName(), billingAddress, pProfile);
				}

				repository.updateItem(newCreditCard);

			}
		}
		
		if (StringUtils.isBlank(creditCardNickname)) {
			creditCardNickname = getUniqueCreditCardNickname(newCreditCard, pProfile, null);
		}
		List<String> creditCardNickNames = new ArrayList<String>();
		if(pPaymentGroupMapContainer != null && pPaymentGroupMapContainer.getPaymentGroupMap() != null && !pPaymentGroupMapContainer.getPaymentGroupMap().isEmpty()){
			Set keySet = pPaymentGroupMapContainer.getPaymentGroupMap().keySet();
			Iterator iterator = keySet.iterator();
			while (iterator.hasNext()) {
				creditCardNickNames.add((String) iterator.next());
			}
		}
		if(!creditCardNickNames.isEmpty() && creditCardNickNames.size() > TRUCheckoutConstants.INT_ONE) {
			creditCardNickname = getUniqueCreditCardNickname(newCreditCard, creditCardNickNames, creditCardNickname);
		}
		addCreditCardToUsersMap(pProfile, newCreditCard, creditCardNickname);
		return creditCardNickname;
	}

	/**
	 * This method creates the credit card repository item.
	 * 
	 * @param pProfile
	 *            profile RepositoryItem
	 * @param pNewCreditCard
	 * 			  credit card object map
	 * @param pBillingAddress
	 *            billing address RepositoryItem
	 * @return creditCardRepositoryItem MutableRepositoryItem
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws PropertyNotFoundException 
	 */
	public MutableRepositoryItem createCreditCardItem(RepositoryItem pProfile, Object pNewCreditCard, RepositoryItem pBillingAddress)
			throws RepositoryException, PropertyNotFoundException {
		if (pProfile != null) {
			final CommercePropertyManager cpmgr = (CommercePropertyManager) getPropertyManager();
			final MutableRepository repository = (MutableRepository) pProfile.getRepository();
			final MutableRepositoryItem creditCardRepositoryItem = repository.createItem(cpmgr.getCreditCardItemDescriptorName());			
			copyShallowCreditCardProperties(pNewCreditCard, creditCardRepositoryItem);
			creditCardRepositoryItem.setPropertyValue(cpmgr.getCreditCardItemDescriptorBillingAddressPropertyName(),
					pBillingAddress);
			repository.addItem(creditCardRepositoryItem);
			return creditCardRepositoryItem;
		}
		return null;
	}
	
	/**
	 * This method creates the credit card repository item with empty billing address.
	 * 
	 * @param pProfile
	 *            profile RepositoryItem
	 * @param pNewCreditCard
	 * 			  credit card object map
	 * @return creditCardRepositoryItem MutableRepositoryItem
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws PropertyNotFoundException 
	 */
	public MutableRepositoryItem createCreditCardItem(RepositoryItem pProfile, Object pNewCreditCard)
			throws RepositoryException, PropertyNotFoundException {
		if (pProfile != null) {
			final CommercePropertyManager cpmgr = (CommercePropertyManager) getPropertyManager();
			final MutableRepository repository = (MutableRepository) pProfile.getRepository();
			final MutableRepositoryItem creditCardRepositoryItem = repository.createItem(cpmgr.getCreditCardItemDescriptorName());			
			copyShallowCreditCardProperties(pNewCreditCard, creditCardRepositoryItem);
			MutableRepositoryItem contactInfoRepositoryItem = repository.createItem(cpmgr.getContactInfoItemDescriptorName());
			creditCardRepositoryItem.setPropertyValue(cpmgr.getCreditCardItemDescriptorBillingAddressPropertyName(), contactInfoRepositoryItem);
			repository.addItem(contactInfoRepositoryItem);
			repository.addItem(creditCardRepositoryItem);
			return creditCardRepositoryItem;
		}
		return null;
	}
	
	/**
	 * This method is overridden to copy the custom property before creating
	 * credit card item.
	 * 
	 * @param pCreditCard
	 *            Credit Card Object
	 * @param pProfile
	 *            Profile Repository Item.
	 * @param pNickName
	 *            String - Nick Name
	 */
	@Override
	public void copyCreditCardToProfile(CreditCard pCreditCard,
			RepositoryItem pProfile, String pNickName) {
		MutableRepository repository = (MutableRepository) pProfile
				.getRepository();
		if (!(isCreditCardEmpty(pCreditCard))) {
			if (isLoggingDebug()){
				logDebug("adding credit card to users profile");
			}
			try {
				TRUCreditCard truCard = (TRUCreditCard) pCreditCard;
				MutableRepositoryItem creditCardRepositoryItem = createCreditCardItem(
						pProfile, truCard);
				copyCreditCard(pCreditCard, creditCardRepositoryItem);
				repository.updateItem(creditCardRepositoryItem);
				addCreditCardToUsersMap(pProfile, creditCardRepositoryItem,
						pNickName);
			} catch (RepositoryException re) {
					if (isLoggingError()) {
						vlogError("RepositoryException : {0}", re);
				}
			} catch (PropertyNotFoundException e) {
				if (isLoggingError()) {
					vlogError("PropertyNotFoundException : {0}", e);
				}
			}
		}
	}

	/**
	 * Checks the Address Doctor Process Status.
	 * 
	 * @param pJsonAddressDoctorResponse
	 *            json String
	 * @return addressDoctorResponseVO of type TRUAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO(String pJsonAddressDoctorResponse) {

		TRUAddressDoctorResponseVO addressDoctorResponseVO = null;

		try {
			JSONObject json = new JSONObject(pJsonAddressDoctorResponse);
			final String processStatus = json.getString(TRUConstants.ADDRESS_DOCTOR_STATUS_PROCESS_STATUS);
			final String adPocessStatus = json.getString(TRUConstants.AD_PROCESS_STATUS);
			final String derivedStatus = json.getString(TRUConstants.DERIVED_STATUS);

			addressDoctorResponseVO = new TRUAddressDoctorResponseVO();

			addressDoctorResponseVO.setProcessStatus(processStatus);
			addressDoctorResponseVO.setAddressDoctorMessage(json
					.getString(TRUConstants.ADDRESS_DOCTOR_STATUS_ADDRESS_DOCTOR_MESSAGE));
			
			addressDoctorResponseVO.setAdProcessStatus(adPocessStatus);
			addressDoctorResponseVO.setDerivedStatus(derivedStatus);
			
			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(processStatus)) {
				final JSONObject jsonObject = json.getJSONObject(TRUConstants.ADDRESS_DOCTOR_RESULTS);

				final TRUAddressDoctorVO addressDoctorVO = new TRUAddressDoctorVO();
				final Object jsonDeliveryAddressLine = jsonObject.optJSONArray(TRUConstants.DELIVERY_ADDRESS_LINE);
				if (jsonDeliveryAddressLine instanceof JSONArray) {
					addressDoctorVO.setAddress1(jsonObject.getJSONArray(TRUConstants.DELIVERY_ADDRESS_LINE).getJSONObject(0)
							.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
				} else {
					addressDoctorVO.setAddress1(jsonObject.getJSONObject(TRUConstants.DELIVERY_ADDRESS_LINE).getString(
							TRUConstants.ADDRESS_DOCTOR_CONTENT));
				}
				if (jsonObject.has(TRUConstants.SUB_BUILIDING)) {
					final Object jsonSubBuilding = jsonObject.optJSONArray(TRUConstants.SUB_BUILIDING);
					if (jsonSubBuilding instanceof JSONArray) {
						addressDoctorVO.setAddress2(jsonObject.getJSONArray(TRUConstants.SUB_BUILIDING).getJSONObject(0)
								.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
					} else {
						addressDoctorVO.setAddress2(jsonObject.getJSONObject(TRUConstants.SUB_BUILIDING).getString(
								TRUConstants.ADDRESS_DOCTOR_CONTENT));
					}
				}
				addressDoctorVO.setCity(jsonObject.getJSONObject(TRUConstants.LOCALITY).getString(
						TRUConstants.ADDRESS_DOCTOR_CONTENT));

				JSONArray provinceJsonArray = jsonObject.getJSONArray(TRUConstants.PROVINCE);
				if (provinceJsonArray != null) {
					for (int i = 0; i < provinceJsonArray.length(); i++) {
						if(jsonObject.getJSONArray(TRUConstants.PROVINCE).get(i) instanceof JSONObject){
							addressDoctorVO.setState(jsonObject.getJSONArray(TRUConstants.PROVINCE).getJSONObject(i)
									.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
							break;
						}
					}
				}

				addressDoctorVO.setCountry(jsonObject.getString(TRUConstants.COUNTRY));
				addressDoctorVO.setPostalCode(jsonObject.getString(TRUConstants.POSTAL_CODE));
				addressDoctorVO.setFullAddressName(jsonObject.getJSONObject(TRUConstants.FULL_ADDRESS).getString(
						TRUConstants.ADDRESS_DOCTOR_CONTENT));
				addressDoctorVO.setCount(jsonObject.getInt(TRUConstants.ADDRESS_DOCTOR_COUNT));
				if(jsonObject.getString(TRUConstants.MAILABILITY_SCORE) != null){
				addressDoctorVO.setMailabilityScore(jsonObject.getString(TRUConstants.MAILABILITY_SCORE));
				}
				if(jsonObject.getString(TRUConstants.ELEMENT_STATUS_RESULT) != null){
				addressDoctorVO.setElementResultStatus(jsonObject.getString(TRUConstants.ELEMENT_STATUS_RESULT));
				}

				addressDoctorResponseVO.getAddressDoctorVO().add(addressDoctorVO);
			} else if (TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(processStatus)) {
				final Object jsonAddressDoctorResults = json.optJSONArray(TRUConstants.ADDRESS_DOCTOR_RESULTS);
				JSONArray jsonArray = new JSONArray();
				if (jsonAddressDoctorResults instanceof JSONArray) {
					jsonArray = json.getJSONArray(TRUConstants.ADDRESS_DOCTOR_RESULTS);
				} else {
					jsonArray.add(0, json.get(TRUConstants.ADDRESS_DOCTOR_RESULTS));
				}
				if (jsonArray != null) {
					TRUAddressDoctorVO addressDoctorVO = null;
					for (int i = 0; i < jsonArray.length(); i++) {
						addressDoctorVO = new TRUAddressDoctorVO();
						addressDoctorVO.setAddress1(jsonArray.getJSONObject(i).getJSONObject(TRUConstants.DELIVERY_ADDRESS_LINE)
								.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
						if (jsonArray.getJSONObject(i).has(TRUConstants.SUB_BUILIDING)) {
							addressDoctorVO.setAddress2(jsonArray.getJSONObject(i).getJSONObject(TRUConstants.SUB_BUILIDING)
									.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
						}
						addressDoctorVO.setCity(jsonArray.getJSONObject(i).getJSONObject(TRUConstants.LOCALITY)
								.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
						
						JSONArray provinceJsonArray = jsonArray.getJSONObject(i).getJSONArray(TRUConstants.PROVINCE);
						if (provinceJsonArray != null) {
							for (int j = 0; j < provinceJsonArray.length(); j++) {
								if(jsonArray.getJSONObject(i).getJSONArray(TRUConstants.PROVINCE).get(j) instanceof JSONObject){
									addressDoctorVO.setState(jsonArray.getJSONObject(i).getJSONArray(TRUConstants.PROVINCE).getJSONObject(j)
											.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
									break;
								}
							}
						}
						
						addressDoctorVO.setCountry(jsonArray.getJSONObject(i).getString(TRUConstants.COUNTRY));
						addressDoctorVO.setPostalCode(jsonArray.getJSONObject(i).getString(TRUConstants.POSTAL_CODE));
						addressDoctorVO.setFullAddressName(jsonArray.getJSONObject(i).getJSONObject(TRUConstants.FULL_ADDRESS)
								.getString(TRUConstants.ADDRESS_DOCTOR_CONTENT));
						addressDoctorVO.setCount(jsonArray.getJSONObject(i).getInt(TRUConstants.ADDRESS_DOCTOR_COUNT));
						if(jsonArray.getJSONObject(i).getString(TRUConstants.MAILABILITY_SCORE) != null){
						addressDoctorVO.setMailabilityScore(jsonArray.getJSONObject(i).getString(TRUConstants.MAILABILITY_SCORE));
						}
						if(jsonArray.getJSONObject(i).getString(TRUConstants.ELEMENT_STATUS_RESULT) != null){
						addressDoctorVO.setElementResultStatus(jsonArray.getJSONObject(i).getString(TRUConstants.ELEMENT_STATUS_RESULT));
						}
						addressDoctorResponseVO.getAddressDoctorVO().add(addressDoctorVO);
					}
				}
			}
		} catch (JSONException e) {
			if (isLoggingError()) {
				vlogError("JSONException occured while getting address doctor response with exception : {0}", e);
			}
		}

		return addressDoctorResponseVO;
	}

	/**
	 * This method is used to generate the url.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pRedirectPage
	 *            redirection page url
	 * @return String - returns to final url for redirection on click
	 */
	public String generateURL(DynamoHttpServletRequest pRequest, String pRedirectPage) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (generateURL) : Starts");
		}
		String lURL = null;
		String siteParam = null;
		Site site = SiteContextManager.getCurrentSite();
		if(site != null){
			String siteId = site.getId();
			siteParam = (String) getKeyFromValue(getTRUConfiguration().getCustomParamToSiteIdMapping(), siteId);
		}
		final String lHost = pRequest.getHeader(TRUConstants.HOST);
		String scheme = getSchemeDetectionUtil().getScheme();
		if(pRedirectPage.contains(getCheckoutUrl()) || siteParam == null) {
			boolean appendSchemaForSynchronyReturnUrl = getTRUConfiguration().isAppendSchemaForSynchronyReturnUrl();
			if(appendSchemaForSynchronyReturnUrl) {
				lURL = scheme + lHost + TRUConstants.PATH + pRedirectPage;
			} else {
				lURL = lHost + TRUConstants.PATH + pRedirectPage;
			}
		} else {
			lURL = scheme + lHost + TRUConstants.PATH + pRedirectPage + TRUConstants.QUESTION_MARK_STRING + TRUCommerceConstants.CURRENT_SITE +
					TRUConstants.EQUALS_SYMBOL_STRING + siteParam;
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (generateURL) : Ends");
		}
		return lURL;
	}

	/**
	 * Overriding this method to stop from sending email. All the emails will be sent using epsilon third party integration.
	 * 
	 * @param pProfile
	 *            profile mutable item
	 * @param pRunInSeparateThread
	 *            boolean
	 * @param pPersist
	 *            boolean
	 * @param pTemplateEmailSender
	 *            TemplateEmailSender
	 * @param pTemplateEmailInfo
	 *            TemplateEmailInfo
	 * @param pTemplateParameters
	 *            template parameters
	 * @throws TemplateEmailException
	 *             TemplateEmailException
	 */
	public void sendEmailToUser(MutableRepositoryItem pProfile, boolean pRunInSeparateThread, boolean pPersist,
			TemplateEmailSender pTemplateEmailSender, TemplateEmailInfo pTemplateEmailInfo, Map pTemplateParameters)
			throws TemplateEmailException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEmailToUser) : Starts");
		}
		vlogInfo("overriding the OOTB method ProfileTools.sendEmailToUser to stop from sending email");
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEmailToUser) : Ends");
		}
	}

	/**
	 * Overriding this method to stop from sending email. All the emails will be sent using epsilon third party integration.
	 * 
	 * @param pCreditCardNumber
	 *            credit card number String
	 * @return creditCardType String
	 */
	public String getCreditCardType(String pCreditCardNumber) {
		String creditCardType = null;
		final Enumeration e = getExtendableCreditCardTools().getCardPrefixesMap().propertyNames();
		String cardPrefixValue = null;
		while (e.hasMoreElements()) {
			String cardPrefix = (String) e.nextElement();
			if (pCreditCardNumber.startsWith(cardPrefix)) {
				cardPrefixValue = getExtendableCreditCardTools().getCardPrefixesMap().getProperty(cardPrefix);
				creditCardType = getExtendableCreditCardTools().getCardTypesMap().getProperty(cardPrefixValue);
			}
		}
		return creditCardType;
	}

	/**
	 * This method checks the valid credit card based on bin range and returns creditCardType.
	 * 
	 * @param pCreditCardNumber
	 *            - credit card number String
	 * @param pCardLength
	 *            - card length String
	 * @return String creditCardType
	 */
	public String getCreditCardTypeFromRepository(String pCreditCardNumber, String pCardLength) {
		vlogDebug("TRUProfileTools (getCreditCardTypeFromRepository) : Start");
		String creditCardType = null;
		try {
			Boolean isCreditCardFound = false;

			int sixDigitCCNumber = Integer.parseInt(pCreditCardNumber.substring(TRUConstants.ZERO, TRUConstants.SIX));

			RepositoryView view = getStoreConfigRepository().getView(TRUConstants.ITEM_DESCRIPTOR_BIN_RANGE);

			QueryBuilder builder = view.getQueryBuilder();

			QueryExpression propertyLowerRange = builder.createPropertyQueryExpression(TRUConstants.LOWER_RANGE);

			QueryExpression constantLowerRange = builder.createConstantQueryExpression(sixDigitCCNumber);

			Query lowerRangeQuery = builder.createComparisonQuery(propertyLowerRange, constantLowerRange,
					QueryBuilder.LESS_THAN_OR_EQUALS);

			QueryExpression propertyHigherRange = builder.createPropertyQueryExpression(TRUConstants.UPPER_RANGE);

			QueryExpression constantHigherRange = builder.createConstantQueryExpression(sixDigitCCNumber);

			Query higherRangeQuery = builder.createComparisonQuery(propertyHigherRange, constantHigherRange,
					QueryBuilder.GREATER_THAN_OR_EQUALS);

			Query[] arrayOfRanges = { lowerRangeQuery, higherRangeQuery };

			Query rangesQuery = builder.createAndQuery(arrayOfRanges);

			// execute the query and get the results
			RepositoryItem[] repositoryItems = view.executeQuery(rangesQuery);
			// vlogDebug(" Repository Items :{0}",repositoryItems);
			String cardType = null;
			if (repositoryItems != null) {
				String cardLength = null;
				for (RepositoryItem rangeItem : repositoryItems) {
					cardType = (String) rangeItem.getPropertyValue(getTRUPropertyManager().getCreditCardTypePropertyName());
					if (cardType != null) {
						cardLength = (String) getExtendableCreditCardTools().getCardLengthsMap().get(cardType);
						String[] splitCardLengthTypes = cardLength.split(TRUConstants.FORWARD_SLASH);
						if (splitCardLengthTypes.length > TRUConstants.SIZE_ONE) {
							for (String repLength : splitCardLengthTypes) {
								if (Integer.valueOf(pCardLength) == Integer.parseInt(repLength)) {
									creditCardType = cardType;
									isCreditCardFound = true;
									break;
								}
							}
						} else {
							if (Integer.valueOf(pCardLength) == Integer.parseInt(cardLength)) {
								creditCardType = cardType;
								isCreditCardFound = true;
								break;
							}
						}
						if (isCreditCardFound) {
							break;
						}
					}
				}
			}
			if (creditCardType == null) {
				throw new RepositoryException(TRUConstants.CREDIT_CARD_TYPE_REPOSITORY_EXCEPTION);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("Repository Exception occured while getting credit card type from repository : {0}", e);
			}
		}
		vlogDebug(" CreditCard Type :{0}", creditCardType);
		vlogDebug("TRUProfileTools (getCreditCardTypeFromRepository) : End");
		
		return creditCardType;
	}

	/**
	 * This method checks the card expiration.
	 * 
	 * @param pExpireMonth
	 *            String
	 * @param pExpirationYear
	 *            String
	 * @return boolean true if the card is expired
	 */
	public boolean isValidateCardExpiration(String pExpireMonth, String pExpirationYear) {
		final Calendar cal = Calendar.getInstance();
		final int month = cal.get(Calendar.MONTH) + TRUConstants.SIZE_ONE;
		final int year = cal.get(Calendar.YEAR);
		final int expMonth = Integer.parseInt(pExpireMonth);
		final int expYear = Integer.parseInt(pExpirationYear);
		boolean isExpired = true;
		if (expYear > year) {
			isExpired = false;
		} else if ((expYear == year) && (expMonth >= month)) {
			isExpired = false;
		}
		return isExpired;
	}

	/**
	 * This method sets the first/last address added/left to/in profile as default address if it is a US address.
	 * 
	 * @param pProfile
	 *            profile RepositoryItem
	 * @throws RepositoryException
	 *             RepositoryException
	 */
	public void setProfileDefaultAddress(RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileTools.setProfileDefaultAddress() ");
		}
		final Map secondaryAddressMap = (Map) pProfile
				.getPropertyValue(getTRUPropertyManager().getSecondaryAddressPropertyName());
		final Iterator entries = secondaryAddressMap.entrySet().iterator();
		int i = TRUConstants._0;
		RepositoryItem newDefaultShippingAddress = null;
		RepositoryItem allShippingAddress = null;
		Entry lDefaultAddressMap = null;
		String country = null;
		boolean isBillingAddressInContact = false;
		while (entries.hasNext()) {
			lDefaultAddressMap = (Entry) entries.next();
			allShippingAddress = (RepositoryItem) lDefaultAddressMap.getValue();
			isBillingAddressInContact = (boolean) allShippingAddress.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getIsBillingAddressPropertyName());
			country = (String) allShippingAddress.getPropertyValue(((TRUPropertyManager) getPropertyManager())
					.getAddressCountryPropertyName());
			if (((TRUConstants.UNITED_STATES).equalsIgnoreCase(country) || (TRUConstants.USA).equalsIgnoreCase(country))
					&&  !isBillingAddressInContact){
				newDefaultShippingAddress = (RepositoryItem) lDefaultAddressMap.getValue();
				i++;
			}
		}
		if (i == TRUConstants.ONE) {
			updateProperty(((TRUPropertyManager) getPropertyManager()).getShippingAddressPropertyName(),
					newDefaultShippingAddress, pProfile);			
			//updatePreviousShippingAddress((MutableRepositoryItem) pProfile, defaultShipNickName);
		}
		// If Removes all the address ,then remove the updatePreviousShippingAddress
		if (i == TRUConstants._0) {
			updateProperty(((TRUPropertyManager) getPropertyManager()).getShippingAddressPropertyName(), null, pProfile);
			updatePreviousShippingAddress((MutableRepositoryItem) pProfile, null);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileTools.setProfileDefaultAddress() ");
		}
	}

	/**
	 * This method is used to check the user status.
	 * 
	 * @param pProfile
	 *            RepositoryItem
	 * @return true/false
	 */
	public boolean isAnonymousUser(RepositoryItem pProfile) {
		vlogDebug("@Class TRUProfileTools @method isAnonymousUser() : STARTS");
		final TRUPropertyManager propertyManager = getTRUPropertyManager();
		if (propertyManager == null) {
			if (isLoggingError()) {
				vlogError("propertyManager is null");
			}
			return false;
		}
		if (pProfile == null) {
			if (isLoggingError()) {
				vlogError("profile is null");
			}
			return true;
		}
		final int securityStatusAnonymous = propertyManager.getSecurityStatusAnonymous();
		Object securityStatusObj = pProfile.getPropertyValue(propertyManager.getSecurityStatusPropertyName());
		if (securityStatusObj != null) {
			final Integer securityStatus = (Integer) securityStatusObj;

			// If user security status is greater than anonymous, return false
			if (securityStatus.intValue() > securityStatusAnonymous) {
				return Boolean.FALSE;
			}
		}
		vlogDebug("@Class TRUProfileTools @method isAnonymousUser() : ENDS");
		return Boolean.TRUE;
	}

	/**
	 * Gets the extendable credit card tools.
	 * 
	 * @return the extendableCreditCardTools
	 */
	public ExtendableCreditCardTools getExtendableCreditCardTools() {
		return mExtendableCreditCardTools;
	}

	/**
	 * Sets the extendable credit card tools.
	 * 
	 * @param pExtendableCreditCardTools
	 *            the extendableCreditCardTools to set
	 */
	public void setExtendableCreditCardTools(ExtendableCreditCardTools pExtendableCreditCardTools) {
		mExtendableCreditCardTools = pExtendableCreditCardTools;
	}

	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTRUConfiguration
	 *            the mTRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		this.mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the store config repository.
	 * 
	 * @return the mStoreConfigRepository
	 */
	public Repository getStoreConfigRepository() {
		return mStoreConfigRepository;
	}

	/**
	 * Sets the store config repository.
	 * 
	 * @param pStoreConfigRepository
	 *            the mStoreConfigRepository to set
	 */
	public void setStoreConfigRepository(Repository pStoreConfigRepository) {
		this.mStoreConfigRepository = pStoreConfigRepository;
	}

	/**
	 * Gets the address doctor response vo.
	 * 
	 * @return the addressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * Sets the address doctor response vo.
	 * 
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * Gets the address doctor service.
	 * 
	 * @return the addressDoctorService
	 */
	public TRUAddressDoctorService getAddressDoctorService() {
		return mAddressDoctorService;
	}

	/**
	 * Sets the address doctor service.
	 * 
	 * @param pAddressDoctorService
	 *            the addressDoctorService to set
	 */
	public void setAddressDoctorService(TRUAddressDoctorService pAddressDoctorService) {
		mAddressDoctorService = pAddressDoctorService;
	}
	
	/**
	 * Gets the address Compare Excluded Properties list.
	 * 
	 * @return the addressCompareExcludedProperties
	 */
	public List<String> getAddressCompareExcludedProperties() {
		return mAddressCompareExcludedProperties;
	}

	/**
	 * Sets the address Compare Excluded Properties list.
	 * 
	 * @param pAddressCompareExcludedProperties
	 *            the addressCompareExcludedProperties to set
	 */
	public void setAddressCompareExcludedProperties(
			List<String> pAddressCompareExcludedProperties) {
		this.mAddressCompareExcludedProperties = pAddressCompareExcludedProperties;
	}

	/**
	 * This method is used to add credit card.
	 * 
	 * @param pProfile
	 *            RepositoryItem
	 * @param pCreditCard
	 *            creditCard
	 * @param pNickName
	 *            nickName
	 */
	public void addCreditCardToUsersMap(RepositoryItem pProfile, RepositoryItem pCreditCard, String pNickName) {
		if (pCreditCard != null) {
			final Map ccMap = getUsersCreditCardMap(pProfile);
			if (StringUtils.isEmpty(pNickName)) {
				ccMap.put(getUniqueCreditCardNickname(pCreditCard, pProfile, pNickName), pCreditCard);
			} else {
				if (ccMap != null && ccMap.isEmpty()) {
					ccMap.put(pNickName, pCreditCard);
					try {						
						if(setDefaultCreditCard(pProfile, pNickName)){
							updateProperty(TRUConstants.SELECTED_CC_NICKNAME, pNickName, pProfile);
						}
					} catch (RepositoryException repExp) {
						if (isLoggingError()) {
							vlogError("Error while updating addCreditCardToUsersMap proeprty value", repExp);
						}
					}
				} else {
					ccMap.put(pNickName, pCreditCard);
				}
			}
		}
	}

	/**
	 * Gets the epslon account source.
	 * 
	 * @return the epslonAccountSource
	 */
	public TRUEpslonAccountSource getEpslonAccountSource() {
		return mEpslonAccountSource;
	}

	/**
	 * Sets the epslon account source.
	 * 
	 * @param pEpslonAccountSource
	 *            the epslonAccountSource to set
	 */
	public void setEpslonAccountSource(TRUEpslonAccountSource pEpslonAccountSource) {
		mEpslonAccountSource = pEpslonAccountSource;
	}

	/**
	 * This method is used to create credit card when adding from payment page.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pNewCreditCard
	 *            newCreditCard
	 * @param pBillingAddressNickName
	 *            billingAddressNickName
	 * @return creditCardNickName
	 */
	public String createProfileCreditCard(RepositoryItem pProfile, Map<String, String> pNewCreditCard,
			String pBillingAddressNickName) {
		if (isLoggingDebug()) {
			logDebug("START of TRUProfileTools().createProfileCreditCard method");
		}

		String creditCardNickName = null;
		// save token number inplace of credit card number
		if (null != pNewCreditCard && !StringUtils.isBlank(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TOKEN))) {
			pNewCreditCard.put(TRUConstants.CREDIT_CARD_NUMBER, pNewCreditCard.get(TRUConstants.CREDIT_CARD_TOKEN));
		}
		try {
			final RepositoryItem billingAddress = getProfileAddress(pProfile, pBillingAddressNickName);
			final MutableRepositoryItem newCreditCard = createCreditCardItem(pProfile, pNewCreditCard, billingAddress);

			creditCardNickName = getUniqueCreditCardNickname(newCreditCard, pProfile, null);
			addCreditCardToUsersMap(pProfile, newCreditCard, creditCardNickName);

			return creditCardNickName;
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		} catch (PropertyNotFoundException pnfe) {
			if (isLoggingError()) {
				logError(pnfe);
			}
		}

		vlogDebug("End of TRUProfileTools().createProfileCreditCard method and returns creditCardNickname: {0} ",
				creditCardNickName);
		return creditCardNickName;
	}

	/**
	 * This method is used to update credit card.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pCreditCardName
	 *            creditCardName
	 * @param pEditCreditCard
	 *            editCreditCard
	 * @param pBillingAddressNickName
	 *            billingAddressNickName
	 */
	public void updateProfileCreditCard(RepositoryItem pProfile, String pCreditCardName, Map<String, String> pEditCreditCard,
			String pBillingAddressNickName) {
		if (isLoggingDebug()) {
			logDebug("START of TRUProfileTools().updateProfileCreditCard method");
		}
		if (pProfile == null) {
			return;
		}

		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		try {
			final MutableRepository repository = (MutableRepository) pProfile.getRepository();

			final RepositoryItem billingAddress = getProfileAddress(pProfile, pBillingAddressNickName);
			final MutableRepositoryItem creditCard = (MutableRepositoryItem) getCreditCardByNickname(pCreditCardName, pProfile);
			if (creditCard != null && billingAddress != null) {
				creditCard.setPropertyValue(propertyManager.getCreditCardItemDescriptorBillingAddressPropertyName(),
						billingAddress);
				copyShallowCreditCardProperties(pEditCreditCard, creditCard);
			}
			repository.updateItem(creditCard);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		} catch (PropertyNotFoundException pnfe) {
			if (isLoggingError()) {
				logError(pnfe);
			}
		}
		vlogDebug("End of TRUProfileTools().updateProfileCreditCard method");
	}

	/**
	 * Overridden for profile Migration along with ID.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pNewCreditCard
	 * 			  new credit card map       
	 * @param pBillingAddress
	 *            billingAddress
	 * @param pCreditCardId
	 *            creditCardId
	 * @return mutableRepositoryItem
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws PropertyNotFoundException 
	 */
	public MutableRepositoryItem createCreditCardItem(RepositoryItem pProfile, Object pNewCreditCard, RepositoryItem pBillingAddress,
			String pCreditCardId) throws RepositoryException, PropertyNotFoundException {
		if (pProfile != null) {
			final CommercePropertyManager cpmgr = (CommercePropertyManager) getPropertyManager();
			final MutableRepository repository = (MutableRepository) pProfile.getRepository();
			final MutableRepositoryItem creditCardRepositoryItem = repository.createItem(pCreditCardId,
					cpmgr.getCreditCardItemDescriptorName());
			copyShallowCreditCardProperties(pNewCreditCard, creditCardRepositoryItem);
			creditCardRepositoryItem.setPropertyValue(cpmgr.getCreditCardItemDescriptorBillingAddressPropertyName(),
					pBillingAddress);
			repository.addItem(creditCardRepositoryItem);
			return creditCardRepositoryItem;
		}
		return null;
	}

	/**
	 * Overridden for dataMaigration with ID Checks for duplicates between pKey and the mPropertyMap.keySet().
	 * 
	 * @param pProfile
	 *            profile repository item
	 * @param pNewCreditCard
	 *            new credit card map
	 * @param pCreditCardNickname
	 *            credit card nick name
	 * @param pBillingAddress
	 *            billing address object
	 * @param pCreditcardId
	 *            creditcardId
	 * @return String return type
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public String createProfileCreditCardwithID(RepositoryItem pProfile, Map pNewCreditCard, String pCreditCardNickname,
			Object pBillingAddress, String pCreditcardId) throws RepositoryException, IntrospectionException,
			PropertyNotFoundException {
		final MutableRepository repository = (MutableRepository) pProfile.getRepository();
		final CommercePropertyManager propertyManager = (CommercePropertyManager) getPropertyManager();

		String creditCardNickname = pCreditCardNickname;
		if (StringUtils.isBlank(creditCardNickname)) {
			creditCardNickname = getUniqueCreditCardNickname(pNewCreditCard, pProfile, null);
		}

		MutableRepositoryItem newCreditCard = null;
		if (pBillingAddress instanceof RepositoryItem) {
			newCreditCard = createCreditCardItem(pProfile, pNewCreditCard, (RepositoryItem) pBillingAddress, pCreditcardId);
		} else {
			newCreditCard = createCreditCardItem(pProfile, pNewCreditCard);
			if (pBillingAddress != null) {
				final MutableRepositoryItem billingAddress = (MutableRepositoryItem) newCreditCard
						.getPropertyValue(propertyManager.getCreditCardItemDescriptorBillingAddressPropertyName());

				final Set excludedProperties = new HashSet();
				excludedProperties.add(TRUConstants.ID);

				AddressTools.copyObject(pBillingAddress, billingAddress, excludedProperties);
				repository.updateItem(billingAddress);

				if (getDefaultBillingAddress(pProfile) == null) {
					updateProperty(propertyManager.getBillingAddressPropertyName(), billingAddress, pProfile);
				}

				repository.updateItem(newCreditCard);

			}
		}
		if (StringUtils.isBlank(creditCardNickname)) {
			creditCardNickname = getUniqueCreditCardNickname(newCreditCard, pProfile, null);
		}
		addCreditCardToUsersMap(pProfile, newCreditCard, creditCardNickname);

		return creditCardNickname;
	}

	/**
	 * This method sets the first/last credit card added/left to/in profile as default card.
	 * 
	 * @param pProfile
	 *            profile RepositoryItem
	 * @throws RepositoryException
	 *             RepositoryException
	 */
	public void setProfileDefaultCard(RepositoryItem pProfile) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileTools.setProfileDefaultCard() ");
		}
		final Map creditCards = (Map) pProfile.getPropertyValue(getTRUPropertyManager().getCreditCardPropertyName());
		if (creditCards.size() == TRUConstants.INTEGER_NUMBER_ONE) {
			final Iterator entries = creditCards.entrySet().iterator();
			RepositoryItem defaultCreditCard = null;
			while (entries.hasNext()) {
				final Entry lDefaultCreditCardMap = (Entry) entries.next();
				defaultCreditCard = (RepositoryItem) lDefaultCreditCardMap.getValue();
			}
			if (null != defaultCreditCard) {
				updateProperty(((TRUPropertyManager) getPropertyManager()).getDefaultCreditCardPropertyName(), defaultCreditCard,
						pProfile);
			}
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUProfileTools.setProfileDefaultCard() ");
		}
	}

	/**
	 * This method is used to get property value from any repository item based on the property name.
	 * 
	 * @param pRepItem
	 *            RepItem
	 * @param pPropertyName
	 *            PropertyName
	 * @return String
	 * @throws RepositoryException
	 *             RepositoryException
	 */
	public String getRepoItemPropertyValue(RepositoryItem pRepItem, String pPropertyName) throws RepositoryException {
		return (String) pRepItem.getPropertyValue(pPropertyName);
	}

	/**
	 * <p>
	 * This method will check whether profile's default shipping address modified or not.
	 * </p>
	 * 
	 * @param pProfile
	 *            - RepositoryItem
	 * @param pDefaultShippingnickname
	 *            - String
	 * @return isDefaultShippingAddressChanged - boolean
	 */
	public boolean isDefaultShippingAddressChangedOnProfile(RepositoryItem pProfile, String pDefaultShippingnickname) {
		if (isLoggingDebug()) {
			logDebug("Start isDefaultShippingAddressChangedOnProfile()");
		}
		boolean isDefaultShippingAddressChanged = false;
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		String prevAddressNickName = null;
		if (isAnonymousUser(pProfile)) {
			prevAddressNickName = (String) pProfile.getPropertyValue(propertyManager
					.getAnonymousPrevDefaultNickNamePropertyName());
			if (StringUtils.isNotBlank(pDefaultShippingnickname) && !pDefaultShippingnickname.equals(prevAddressNickName)) {
				isDefaultShippingAddressChanged = true;
			}
		} else {
			// RepositoryItem
			// prevShippingAddress=(RepositoryItem)pProfile.getPropertyValue(propertyManager.getPreviousShippingAddressPropertyName());
			prevAddressNickName = (String) pProfile
					.getPropertyValue(propertyManager.getPrevShippingAddressNickNamePropertyName());
			if (StringUtils.isNotBlank(pDefaultShippingnickname) && !pDefaultShippingnickname.equals(prevAddressNickName)) {
				isDefaultShippingAddressChanged = true;
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("End isDefaultShippingAddressChangedOnProfile ? {0}", isDefaultShippingAddressChanged);
		}
		return isDefaultShippingAddressChanged;
	}

	/**
	 * Gets the default shipping address nickname.
	 * 
	 * @param pProfile
	 *            - RepositoryItem
	 * @return currentAddressNickName - String
	 */
	public String getDefaultShippingAddressNickname(RepositoryItem pProfile) {
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		final RepositoryItem shipAddressItem = (RepositoryItem) pProfile.getPropertyValue(propertyManager
				.getShippingAddressPropertyName());
		String shipAddressNickName=TRUConstants.EMPTY;
		if(shipAddressItem!=null){
			shipAddressNickName= getDefaultAddressName(pProfile, shipAddressItem);
		}
		if (isLoggingDebug()) {
			vlogDebug("getDefaultShippingAddressNickname\t? {0}", shipAddressNickName);
		}
		return shipAddressNickName;
	}

	/**
	 * <p>
	 * This will update the PreviousShippingAddress on profile.
	 * </p>
	 * 
	 * @param pProfile
	 *            Profile
	 * @param pDefaultShippingNickname
	 *            DefaultShippingNickname
	 */
	public void updatePreviousShippingAddress(MutableRepositoryItem pProfile, String pDefaultShippingNickname) {
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		if (isAnonymousUser(pProfile)) {
			pProfile.setPropertyValue(propertyManager.getAnonymousPrevDefaultNickNamePropertyName(), pDefaultShippingNickname);
		} else {
			try {
				final RepositoryItem currentShippingAddress = getSecondaryAddressByNickName(pProfile, pDefaultShippingNickname);
				updateProperty(propertyManager.getPreviousShippingAddressPropertyName(), currentShippingAddress, pProfile);
				updateProperty(propertyManager.getPrevShippingAddressNickNamePropertyName(), pDefaultShippingNickname, pProfile);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("RepositoryException while updating previousShippingAddress proeprty value", e);
				}
			}
		}
	}

	/**
	 * This method fetches the profile with the provided email and checks whether the generated password property is true or
	 * false.
	 * 
	 * @param pEmail
	 *            - String
	 * @return passwordGenerated - boolean
	 */
	public boolean isGeneratedPassword(String pEmail) {
		boolean passwordGenerated = Boolean.TRUE;
		final MutableRepositoryItem profileItem = (MutableRepositoryItem) getItemFromEmail(pEmail);
		if (profileItem != null) {
			passwordGenerated = (boolean) profileItem.getPropertyValue(getPropertyManager().getGeneratedPasswordPropertyName());
		}
		return passwordGenerated;
	}

	/**
	 * This method decodes the encoded input String using Base64 API.
	 * 
	 * @param pEncodedString
	 *            - String
	 * @return String
	 */
	public String getBase64DecodedString(String pEncodedString) {
		String decodeString = null;
		final byte[] decodedByte = Base64.decode(pEncodedString);
		decodeString = new String(decodedByte);
		return decodeString;
	}

	/**
	 * This method is used to get the current server time stamp in String format.
	 * 
	 * @return serverTimeStamp - String
	 */
	public String getServerTimeStamp() {
		String serverTimeStamp = null;
		final Calendar cal = Calendar.getInstance();
		final Date date = cal.getTime();
		final String dateFormat = TRUCommerceConstants.SERVER_DATE_FORMAT;
		final DateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone(TRUCommerceConstants.SERVER_TIME_ZONE));
		serverTimeStamp = formatter.format(date);
		return serverTimeStamp;
	}

	/**
	 * <p>
	 * 
	 * </p>
	 * .
	 * 
	 * @param pProfile
	 *            the profile
	 * @return the prev shipping address nick name
	 */
	public String getPrevShippingAddressNickName(RepositoryItem pProfile) {
		String prevAddressNickName = TRUConstants.EMPTY_STRING;
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		if (isAnonymousUser(pProfile)) {
			prevAddressNickName = (String) pProfile.getPropertyValue(propertyManager
					.getAnonymousPrevDefaultNickNamePropertyName());
		} else {
			prevAddressNickName = (String) pProfile
					.getPropertyValue(propertyManager.getPrevShippingAddressNickNamePropertyName());
		}
		return prevAddressNickName;
	}

	/**
	 * *.
	 * 
	 * @param pLogin
	 *            the login value
	 * @return boolean
	 */
	public boolean checkForMigratedProfile(String pLogin) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileTools.checkForMigratedProfile()");
		}
		final MutableRepositoryItem profileItem = (MutableRepositoryItem) getItemFromEmail(pLogin);
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
		boolean isProfileMigrated = false;
		boolean isPasswordReset = false;
		if (profileItem != null) {
			if (isLoggingDebug()) {
				logDebug("TRUProfileFormHandler.checkForMigratedProfile() profileItem inside");
			}
			if (null != profileItem.getPropertyValue(propertyManager.getMigratedProfile())) {
				isProfileMigrated = (boolean) profileItem.getPropertyValue(propertyManager.getMigratedProfile());
				if (isLoggingDebug()) {
					logDebug("TRUProfileFormHandler.checkForMigratedProfile() profileItem inside isProfileMigrated =="
							+ isProfileMigrated);
				}
			}
			if (null != profileItem.getPropertyValue(propertyManager.getMigratedProfileResetPwd())) {
				isPasswordReset = (boolean) profileItem.getPropertyValue(propertyManager.getMigratedProfileResetPwd());
				if (isLoggingDebug()) {
					logDebug("TRUProfileFormHandler.checkForMigratedProfile() profileItem inside isPasswordReset =="
							+ isPasswordReset);
				}
			}

		}
		if (!isPasswordReset && isProfileMigrated) {
			if (isLoggingDebug()) {
				logDebug("TRUProfileFormHandler.checkForMigratedProfile() is migrated Profile");
			}
			return true;
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.checkForMigratedProfile()");
		}
		return false;
	}

	/**
	 * Update pwd reset flag mig profile.
	 * 
	 * @param pProfileItem
	 *            the ProfileItem value
	 */
	public void updatePwdResetFlagMigProfile(MutableRepositoryItem pProfileItem) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileTools.updatePwdResetFlagMigProfile()");
		}
		if (pProfileItem != null) {
			final MutableRepository repository = (MutableRepository) pProfileItem.getRepository();
			final String profileId = pProfileItem.getRepositoryId();
			MutableRepositoryItem mutableItem = null;
			String typeName = null;
			try {

				typeName = pProfileItem.getItemDescriptor().getItemDescriptorName();
				if (repository != null) {
					if (isLoggingDebug()) {
						logDebug("Start: TRUProfileTools.repository null check");
					}
					mutableItem = repository.getItemForUpdate(profileId, typeName);
				}
				final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager();
				if (mutableItem != null) {
					if (isLoggingDebug()) {
						logDebug("Start: TRUProfileTools.mutableItem null check");
					}
					mutableItem.setPropertyValue(propertyManager.getMigratedProfileResetPwd(), true);
					repository.updateItem(mutableItem);
				}
			} catch (RepositoryException rep) {
				if (isLoggingError()) {
					vlogError("Repository Exception occured while updating migrated profile: {0}", rep);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUProfileTools.updatePwdResetFlagMigProfile()");
		}
	}
	
	/**
	 * returns default address nickName.
	 * 
	 * @param pProfile
	 *            profile
	 * @param pShippingGroupMapContainer
	 *            shipping group container
	 * @return addressMap default address
	 * @throws CommerceException
	 *             commerce exception
	 */
	public Map<String, Address> findDefaultAddressMap(RepositoryItem pProfile,
			ShippingGroupMapContainer pShippingGroupMapContainer) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUProfileTools.findDefaultAddressNickName method");
		}
		Map<String, Address> addressMap = new HashMap<String, Address>();

		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		if (profileTools.isAnonymousUser(pProfile)) {
			addressMap = findDefaultAddressMap(pShippingGroupMapContainer);
		} else {
			addressMap = findDefaultAddressMap(pProfile);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUProfileTools.findDefaultAddressNickName method");
		}
		return addressMap;
	}

	/**
	 * returns default address nickName.
	 * 
	 * @param pProfile
	 *            profile
	 * @return addressMap address map
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map<String, Address> findDefaultAddressMap(RepositoryItem pProfile) throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUProfileTools.findDefaultAddressNickName method");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) getPropertyManager(); 
		Map<String, Address> addressMap = new HashMap<String, Address>();

		RepositoryItem addressItem = profileTools.getDefaultShippingAddress(pProfile);
		if (addressItem != null) {
			String country = (String) addressItem.getPropertyValue(propertyManager.getAddressCountryPropertyName());
			if (TRUConstants.UNITED_STATES.equalsIgnoreCase(country)) {
				String nickName = profileTools.getProfileAddressName(pProfile, addressItem);
				if (StringUtils.isNotBlank(nickName)) {
					ContactInfo contactInfo = new ContactInfo();
					OrderTools.copyAddress(addressItem, contactInfo);
					addressMap.put(nickName, contactInfo);
					return addressMap;
				}
			}
		}
		Map<String, RepositoryItem> secondaryAddresMap = (Map) pProfile.getPropertyValue(propertyManager
				.getSecondaryAddressPropertyName());
		if (secondaryAddresMap != null && !secondaryAddresMap.isEmpty()) {
			Set<String> nickNames = secondaryAddresMap.keySet();
			if (nickNames != null && !nickNames.isEmpty()) {
				for (String nickName : nickNames) {
					if (StringUtils.isNotBlank(nickName)) {
						addressItem = secondaryAddresMap.get(nickNames);
						if (addressItem != null) {
							String country = (String) addressItem.getPropertyValue(propertyManager
									.getAddressCountryPropertyName());
							if (TRUConstants.UNITED_STATES.equalsIgnoreCase(country)) {
								ContactInfo contactInfo = new ContactInfo();
								OrderTools.copyAddress(addressItem, contactInfo);
								addressMap.put(nickName, contactInfo);
								return addressMap;
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUProfileTools.findDefaultAddressNickName method");
		}
		return addressMap;
	}

	/**
	 * returns default address nickName.
	 * 
	 * @param pShippingGroupMapContainer
	 *            shipping group map container
	 * @return address map
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Address> findDefaultAddressMap(ShippingGroupMapContainer pShippingGroupMapContainer)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("START of TRUProfileTools.findDefaultShippingAddress method");
			vlogDebug("INPUT PARAMS OF findDefaultShippingAddress() pShippingGroupMapContainer: {0}", pShippingGroupMapContainer);
		}
		Map<String, Address> addressMap = new HashMap<String, Address>();

		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		Map<String, ShippingGroup> shippingGroupMapForDisplay = shippingGroupMapContainer.getShippingGroupMapForDisplay();

		String defaultShippingAddressNickName = shippingGroupMapContainer.getDefaultShippingGroupName();
		if (StringUtils.isNotBlank(defaultShippingAddressNickName)) {
			ShippingGroup shippingGroup = shippingGroupMapForDisplay.get(defaultShippingAddressNickName);
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				if (hardgoodShippingGroup.getShippingAddress() != null && 
						TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress().getCountry())) {
					if (isLoggingDebug()) {
						vlogDebug("End of findDefaultShippingAddress method and returns defaultShippingAddressNickName: {0}",
								defaultShippingAddressNickName);
					}
					addressMap.put(defaultShippingAddressNickName, hardgoodShippingGroup.getShippingAddress());
					return addressMap;
				}
			}
		}

		Set<String> nickNames = shippingGroupMapForDisplay.keySet();
		Iterator<String> iterator = nickNames.iterator();
		while (iterator.hasNext()) {
			String nickName = iterator.next();
			if (StringUtils.isNotBlank(nickName)) {
				ShippingGroup shippingGroup = shippingGroupMapForDisplay.get(nickName);
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if (hardgoodShippingGroup.getShippingAddress() != null && 
							TRUConstants.UNITED_STATES.equalsIgnoreCase(hardgoodShippingGroup.getShippingAddress()
									.getCountry())) {
						if (isLoggingDebug()) {
							vlogDebug("End of findDefaultShippingAddress method and returns nickName: {0}", nickName);
						}
						addressMap.put(nickName, hardgoodShippingGroup.getShippingAddress());
						return addressMap;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End of findDefaultShippingAddress method and returns nickName returns null");
		}
		return addressMap;
	}
	
	
	/**
	 * Gets the default address name.
	 *
	 * @param pProfile the profile
	 * @param pAddress the address
	 * @return the default address name
	 */
	@SuppressWarnings("rawtypes")
	public String getDefaultAddressName(RepositoryItem pProfile, RepositoryItem pAddress)
	  {
	    CommercePropertyManager cpmgr = (CommercePropertyManager)getPropertyManager();
	    Map secondaryAddresses = (Map)pProfile.getPropertyValue(cpmgr.getSecondaryAddressPropertyName());
	    String nickname = TRUConstants.EMPTY;
	    String addressId = pAddress.getRepositoryId();
	    Set entries = secondaryAddresses.entrySet();
	    Iterator it = entries.iterator();
	    Map.Entry entry = null;

	    while (it.hasNext()) {
	      entry = (Map.Entry)it.next();
	      String entryId = ((RepositoryItem)entry.getValue()).getRepositoryId();
	      if (entryId.equals(addressId)) {
	        nickname = (String)entry.getKey();
	        if (isLoggingDebug()) {
	        	logDebug("Nickname for secondary address " + pAddress + " found: " + nickname);
	        }
	        break;	
	      }

	    }
	    return nickname;
	  }
	
	/**
	 * This method is used to check if two addresses are matching field by field or not.
	 * @param pAddress1 Object
	 * @param pAddress2 Object
	 * @return isAddressesMatched boolean
	 */
	public boolean compareAddresses(Object pAddress1, Object pAddress2){
		boolean isAddressesMatched = false;
		List<String> addressCompareExcludedProperties = getAddressCompareExcludedProperties();
		Set<String> excludedProperties = new HashSet<String>();
		int i=0;
		while(i < addressCompareExcludedProperties.size()){
			excludedProperties.add(addressCompareExcludedProperties.get(i).toUpperCase());
			i++;
		}
		try {
			isAddressesMatched = AddressTools.compareObjects(pAddress1, pAddress2, excludedProperties);
		} catch (IntrospectionException introExp) {
			if (isLoggingError()) {
				vlogError("IntrospectionException occured while comparing addresses : {0}", introExp);
			}
		}
		return isAddressesMatched;
	}
	
	/**
	 * This method gets mSETALogger value
	 *
	 * @return the mSETALogger value
	 */
	public TRUSETALogger getSETALogger() {
		return mSETALogger;
	}
	

	/**
	 * This method sets the mSETALogger with pSETALogger value
	 *
	 * @param pSETALogger the pSetaLogger to set
	 */
	public void setSETALogger(TRUSETALogger pSETALogger) {
		mSETALogger = pSETALogger;
	}
	/**
	 * @param pCreditCardNickName - CreditCardNickName
	 * @param pProfile - Profile
	 * @param pSecondaryAddressByNickName - SecondaryAddressByNickName
	 */
	public void setCreditCardBillingAddress(String pCreditCardNickName,
			RepositoryItem pSecondaryAddressByNickName,Profile pProfile) {
		 MutableRepositoryItem creditCard = (MutableRepositoryItem) (getCreditCardByNickname(pCreditCardNickName,pProfile));
		creditCard.setPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName(),
				pSecondaryAddressByNickName);
	}

	/**
	 * Gets the enableValidateCard.
	 * 
	 * @return the mEnableValidateCard
	 */
	public boolean isEnableValidateCard() {
		return mEnableValidateCard;
	}

	/**
	 * Sets the enableValidateCard.
	 * 
	 * @param pEnableValidateCard
	 *            the mEnableValidateCard to set
	 */
	public void setEnableValidateCard(boolean pEnableValidateCard) {
		this.mEnableValidateCard = pEnableValidateCard;
	}
	
	/**
	 * Gets the dummyAdResponse.
	 * 
	 * @return the mDummyAdResponse
	 */
	public String getDummyAdResponse() {
		return mDummyAdResponse;
	}

	/**
	 * Sets the dummyAdResponse.
	 * 
	 * @param pDummyAdResponse
	 *            the mDummyAdResponse to set
	 */
	public void setDummyAdResponse(String pDummyAdResponse) {
		mDummyAdResponse = pDummyAdResponse;
	}
	
	/**
	 * Gets the checkoutUrl.
	 * 
	 * @return the mCheckoutUrl
	 */
	public String getCheckoutUrl() {
		return mCheckoutUrl;
	}

	/**
	 * Sets the checkoutUrl.
	 * 
	 * @param pCheckoutUrl
	 *            the mCheckoutUrl to set
	 */
	public void setCheckoutUrl(String pCheckoutUrl) {
		this.mCheckoutUrl = pCheckoutUrl;
	}
	
	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}

	/**
	 * Overridden method to set the SUCN code to Promotion Status.
	 * 
	 * @param pGuestUser - RepositoryItem
	 * @param pAuthenticatedUser - RepositoryItem
	 * 
	 * @throws RepositoryException - If any.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void addActivePromotions(RepositoryItem pGuestUser,
			RepositoryItem pAuthenticatedUser) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProfileTools  method: addActivePromotions]");
			vlogDebug("pGuestUser : {0} pAuthenticatedUser : {1}", pGuestUser,pAuthenticatedUser);
		}
		PromotionTools promotionTools = getPromotionTools();
		String activePromosProp = promotionTools.getActivePromotionsProperty();
		Object guestPromoStatuses = pGuestUser.getPropertyValue(activePromosProp);
		if (isLoggingDebug()) {
			vlogDebug("guestPromoStatuses : {0}", guestPromoStatuses);
		}
		if(!(guestPromoStatuses instanceof Collection)){
			return;
		}
		//MutableRepository repository = getProfileRepository();
		MutableRepositoryItem mutableItem = RepositoryUtils.getMutableRepositoryItem(pAuthenticatedUser);
		Object authenticatedPromoStatuses = pAuthenticatedUser.getPropertyValue(promotionTools.getActivePromotionsProperty());
		if (isLoggingDebug()) {
			vlogDebug("authenticatedPromoStatuses : {0}", authenticatedPromoStatuses);
		}
		TRUPropertyManager propertyManager = ((TRUOrderManager)getOrderManager()).getPropertyManager();
		if (guestPromoStatuses != null) {
			Iterator promoterator = ((Collection)guestPromoStatuses).iterator();
			RepositoryItem promotionStatus = null;
			while (promoterator.hasNext()) {
				promotionStatus = (RepositoryItem)promoterator.next();
				RepositoryItem promotion = promotionTools.convertSinglePromoStatusToPromo(promotionStatus);
				if (isLoggingDebug()) {
					vlogDebug("promotion : {0}", promotion);
				}
				if(!(authenticatedPromoStatuses instanceof Collection)){
					continue;
				}
				Collection promotionStatuses = (Collection)authenticatedPromoStatuses;
				if (!(promotionTools.isPromotionInPromotionStatuses(promotion, promotionStatuses))){
					Object appliedCodeValue = promotionStatus.getPropertyValue(propertyManager.getAppliedCodeValuePropertyName());
					Object isCodeApplied = promotionStatus.getPropertyValue(propertyManager.getAppliedCodePropertyName());
					if (isLoggingDebug()) {
						vlogDebug("appliedCodeValue : {0} isCodeApplied : {1}", appliedCodeValue,isCodeApplied);
					}
					if(appliedCodeValue != null && isCodeApplied != null && (boolean)isCodeApplied){
						((MutableRepositoryItem)pAuthenticatedUser).setPropertyValue(propertyManager.getSucnCodePropertyName(),
								(String)appliedCodeValue);
					}
					List<RepositoryItem> coupons = (List)promotionStatus.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName());
					if ((coupons != null) && (coupons.size() > TRUConstants.ZERO)){
						for(RepositoryItem coupon : coupons){
							try {
								String profileId = pAuthenticatedUser.getRepositoryId();
								String couponId = coupon.getRepositoryId();
								if (getClaimableManager().canClaimCoupon(profileId, couponId, true)) {
									if (isLoggingDebug()){
										logDebug("Coupon:" + coupon.getRepositoryId() + " can be claimed.");
									}
									getClaimableManager().claimCoupon(pAuthenticatedUser.getRepositoryId(), coupon.getRepositoryId());
								}
							} catch (ClaimableException e) {
								if (isLoggingDebug()){
									vlogError("Coupon:" + coupon.getRepositoryId() + " not added to authenticated profile from guest profile during merge.", e);
								}
							}
						}
					}
					else {
						promotionTools.addPromotion(mutableItem, promotion, TRUConstants.COPIED_DURING_LOGIN);
						promotionTools.sendPromotionRevokedEvent(pGuestUser, promotion, TRUConstants.REVOKED_DURING_LOGIN);
					}
				}
			}
		}
	}
	
	/**
	 * This method checks the first 6 digit card number falls under the valid bin rages or not.
	 * 
	 * @param pCreditCardNumber - String
	 * @return - boolean
	 */
	public boolean isBingeRangeAvailable(String pCreditCardNumber) {
		vlogDebug("TRUProfileTools (isBingeRangeAvailable) : Start");
		try {

			RepositoryView view = getStoreConfigRepository().getView(TRUConstants.ITEM_DESCRIPTOR_BIN_RANGE);

			QueryBuilder builder = view.getQueryBuilder();

			QueryExpression propertyLowerRange = builder.createPropertyQueryExpression(TRUConstants.LOWER_RANGE);

			QueryExpression constantLowerRange = builder.createConstantQueryExpression(Integer.parseInt(pCreditCardNumber));

			Query lowerRangeQuery = builder.createComparisonQuery(propertyLowerRange, constantLowerRange,
					QueryBuilder.LESS_THAN_OR_EQUALS);

			QueryExpression propertyHigherRange = builder.createPropertyQueryExpression(TRUConstants.UPPER_RANGE);

			QueryExpression constantHigherRange = builder.createConstantQueryExpression(Integer.parseInt(pCreditCardNumber));

			Query higherRangeQuery = builder.createComparisonQuery(propertyHigherRange, constantHigherRange,
					QueryBuilder.GREATER_THAN_OR_EQUALS);

			Query[] arrayOfRanges = { lowerRangeQuery, higherRangeQuery };

			Query rangesQuery = builder.createAndQuery(arrayOfRanges);

			// execute the query and get the results
			RepositoryItem[] repositoryItems = view.executeQuery(rangesQuery);
			// vlogDebug(" Repository Items :{0}",repositoryItems);
			if (repositoryItems != null && repositoryItems.length > TRUConstants.ZERO) {
				vlogDebug("TRUProfileTools (isBingeRangeAvailable) Bin Ranges available....................");
				return true;
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("Repository Exception occured while getting credit card type from repository : {0}", e);
			}
		}
		vlogDebug("TRUProfileTools (isBingeRangeAvailable) : End");
		return false;
	}
	
}