
package com.tru.commerce.profile;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import atg.apache.soap.encoding.soapenc.Base64;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.profile.CommerceProfileFormHandler;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.util.NoLockNameException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.commerce.util.TransactionLockService;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.lockmanager.DeadlockException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.userprofiling.PasswordChangeException;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileServices;
import atg.userprofiling.address.AddressTools;

import com.globalsportsinc.crypt.CryptKeeper;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRULockManager;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.common.vo.TRUAddressDoctorVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.logging.TRUSETALogger;
import com.tru.messaging.TRUEpslonChangeEmailSource;
import com.tru.messaging.TRUEpslonChangePasswordSource;
import com.tru.messaging.TRUEpslonPasswordResetSuccessSource;
import com.tru.messaging.oms.TRUCustomerMasterImportSource;
import com.tru.regex.EmailPasswordValidator;
import com.tru.userprofiling.TRUCookieManager;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.utils.TRUStoreUtils;
import com.tru.validator.fhl.IValidator;

/**
 * Profile form handler class for managing the account related operation. Extensions to CommerceProfileFormHandler.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUProfileFormHandler extends CommerceProfileFormHandler {

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_SET_CREDIT_CARD_AS_DEFAULT. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_SET_CREDIT_CARD_AS_DEFAULT = "TRUProfileFormHandler.handleSetCreditCardAsDefault";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS = "TRUProfileFormHandler.handleRemoveShippingAddress";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS = "TRUProfileFormHandler.handleEditShippingAddress";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS = "TRUProfileFormHandler.handleAddShippingAddress";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER = "TRUProfileFormHandler.handleRemoveRewardMember";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_REWARD_MEMBER. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_REWARD_MEMBER = "TRUProfileFormHandler.handleAddRewardMember";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD = "TRUProfileFormHandler.handleResetPassword";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO = "TRUProfileFormHandler.handleUpdateProfileInfo";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD = "TRUProfileFormHandler.handleChangePassword";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_LOGIN. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_LOGIN = "TRUProfileFormHandler.handleLogin";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE = "TRUProfileFormHandler.handleCreate";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_DELETE_CREDIT_CARD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_DELETE_CREDIT_CARD = "TRUProfileFormHandler.handleDeleteCreditCard";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD = "TRUProfileFormHandler.handleAddCreditCard";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD = "TRUProfileFormHandler.handleUpdateCreditCard";
	
	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE. */
	private static final String TRU_PROFILE_FORM_HANDLER_PERFORM_LOGOUT = "TRUProfileFormHandler.performLogout";


	/**
	 * Property to hold mUserOldEmailAddress.
	 */
	private String mUserOldEmailAddress;

	/**
	 * mCustomerMasterImportSource.
	 */
	private TRUCustomerMasterImportSource mCustomerMasterImportSource;

	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
	/**
	 * Property to hold address doctor response status.
	 */
	private String mAddressDoctorProcessStatus;
	/**
	 * property to hold billing address nick name.
	 */
	private String mBillingAddressNickName;
	/**
	 * property to hold mEditCardNickName.
	 */
	private String mEditCardNickName;

	/**
	 * property to hold selected billing address.
	 */
	private String mSelectedCardBillingAddress;

	/**
	 * property to hold Reset Password error url.
	 */
	private String mResetPasswordErrorURL;

	/**
	 * property to hold Reset Password Success url.
	 */
	private String mResetPasswordSuccessURL;

	/**
	 * property to hold reward number of profile.
	 */
	private String mRewardNumber;

	/**
	 * property to hold confirmEmail.
	 */
	private String mConfirmEmail;

	/**
	 * property to hold emailUpdate.
	 */
	private boolean mEmailUpdate;

	/**
	 * property to hold TRUPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * property to hold ProfileServices.
	 */
	private ProfileServices mProfileServices;
	/**
	 * property to hold TRUCookieManager.
	 */
	private TRUCookieManager mCookieManager;

	/**
	 * property to hold RepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	/**
	 * Holds address values.
	 */
	private Map<String, String> mAddressValue = new HashMap<String, String>();
	/**
	 * map to hold credit card info.
	 */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();
	/**
	 * list to hold error keys.
	 */
	private List<String> mErrorKeysList = new ArrayList<String>();
	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	/**
	 * property to hold the nick name of the card to be deleted.
	 */
	private String mDeleteCardNickName;

	/**
	 * Property to holds mTRUProfileManager.
	 */
	private TRUProfileManager mProfileManager;

	/**
	 * Property to holds mEmailPasswordValidator.
	 */
	private EmailPasswordValidator mEmailPasswordValidator;

	/**
	 * property to hold the mValidator.
	 */
	private IValidator mValidator;
	/**
	 * property to hold the mErrorHandler.
	 */
	private IErrorHandler mErrorHandler;
	/**
	 * property to hold ValidationException.
	 */
	private ValidationExceptions mVE = new ValidationExceptions();

	/**
	 * property to hold changeEmailSource.
	 */
	private TRUEpslonChangeEmailSource mChangeEmailSource;

	/**
	 * property to hold changePasswordSource.
	 */
	private TRUEpslonChangePasswordSource mChangePasswordSource;

	/**
	 * property to hold passwordResetSuccessSource.
	 */
	private TRUEpslonPasswordResetSuccessSource mPasswordResetSuccessSource;

	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * Property to hold ajax.
	 */
	private boolean mAjax;
	/**
	 * property to hold credit card CVV.
	 */
	private String mCreditCardCVV;

	/**
	 * property to hold Reward Number Valid flag.
	 */
	private String mRewardNumberValid;

	/**
	 * property to hold islastoptionselected.
	 */
	private boolean mLastOptionSelected;

	/**
	 * property to hold shipping process helper instance.
	 */
	private TRUShippingProcessHelper mShippingHelper;

	/**
	 * property to hold ShippingGroupMapContainer instance.
	 */
	private ShippingGroupMapContainer mShippingGroupMapContainer;

	/**
	 * property to hold PaymentGroupMapContainer instance.
	 */
	private PaymentGroupMapContainer mPaymentGroupMapContainer;
	
	/**
	 * Property to hold the Site Context Manager.
	 */
	private SiteContextManager mSiteContextManager;

	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;

	/**
	 * Property to holds mMyAccountURL.
	 */
	private String mMyAccountURL;
	/**
	 * property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/**
	 * The Password reset url.
	 * 
	 * @return the mPasswordResetURL
	 */
	private String mPasswordResetURL;
	
	private TRUSETALogger mSetaLogger;
	/**
	 * The order id.
	 * 
	 * @return the mOrderId
	 */
	private String mOrderId;
	
	/**
	 * property to hold TRUStoreUtils.
	 */
	private TRUStoreUtils mStoreUtils;

	/** This holds the reference for tru configuration. */
	private TRULockManager mLockManager;
	
	/**
	 * @return the lockManager
	 */
	public TRULockManager getLockManager() {
		return mLockManager;
	}

	/**
	 * @param pLockManager the lockManager to set
	 */
	public void setLockManager(TRULockManager pLockManager) {
		mLockManager = pLockManager;
	}

	/**
	 * Gets the my account url.
	 *
	 * @return the my account url
	 */
	public String getMyAccountURL() {
		return mMyAccountURL;
	}

	/**
	 * Sets the my account url.
	 * 
	 * @param pMyAccountURL
	 *            the mMyAccountURL to set
	 */
	public void setMyAccountURL(String pMyAccountURL) {
		this.mMyAccountURL = pMyAccountURL;
	}

	/**
	 * Gets the site context manager.
	 * 
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the site context manager.
	 * 
	 * @param pSiteContextManager
	 *            the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * property to hold mPurchaseProcessHelper.
	 */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	
	/**
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}
	
	/**
	 * This method is used for doing backend validation on new credit card before adding.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preUpdateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of preUpdateCreditCard method");
		}
		try {
			TRUPropertyManager propertyManager = getPropertyManager();
			// moved this code up to avoid NPE in Rest flows for creditcardtype property
	
			if (!getProfile().isTransient()) {
				RepositoryItem creditCard = ((TRUProfileTools) getProfileTools()).getCreditCardByNickname(getEditCardNickName(),
						getProfile());
				if (creditCard != null) {
					getCreditCardInfoMap().put(
							propertyManager.getCreditCardTypePropertyName(),
							((TRUProfileTools) getProfileTools()).getRepoItemPropertyValue(creditCard,
									propertyManager.getCreditCardTypePropertyName()));
				}else{
					mVE.addValidationError(TRUErrorKeys.EDIT_CREDIT_CARD_NAME, null);
					getErrorHandler().processException(mVE, this);
					return;
				}
			}
			if(StringUtils.isBlank(getBillingAddressNickName()) && StringUtils.isNotBlank(getAddressFirstName())
					&& StringUtils.isNotBlank(getAddressLastName())) {
					String billingAddressNickName = getAddressFirstName()+TRUConstants.WHITE_SPACE+getAddressLastName();
					billingAddressNickName = ((TRUProfileTools)getProfileTools()).
								getUniqueShippingAddressNickname(getAddressValue(), getProfileItem(), billingAddressNickName);
					setBillingAddressNickName(billingAddressNickName);
					getAddressValue().put(TRUConstants.ADDRESS_NICK_NAME, billingAddressNickName);
			}
			
			if (getCreditCardInfoMap()!=null && getCreditCardInfoMap().get(propertyManager.getCreditCardTypePropertyName()).equals(
					TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
						getTRUConfiguration().getExpMonthAdjustmentForPLCC());
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
						getTRUConfiguration().getExpYearAdjustmentForPLCC());
			}

			// Change Country Value from UNITED STATES to US
			if (!getAddressValue().isEmpty()) {
				((TRUProfileTools) getProfileTools()).trimMapValues(getAddressValue());
				((TRUProfileTools) getProfileTools()).trimMapValues(getCreditCardInfoMap());
				((TRUProfileTools) getProfileTools()).changeCountryValue(getAddressValue());
			}
			
			if (getAddressValue().get(TRUConstants.BILLING_ADDRESS_VISIBLE).equalsIgnoreCase(TRUConstants.TRUE)) {
				if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getExpirationMonth())
						|| StringUtils.isBlank(getExpirationYear()) || StringUtils.isBlank(getBillingAddressNickName())
						|| StringUtils.isBlank(getAddressFirstName()) || StringUtils.isBlank(getAddressLastName())
						|| StringUtils.isBlank(getAddressAddressOne()) || StringUtils.isBlank(getAddressCity())
						|| StringUtils.isBlank(getAddressState()) || StringUtils.isBlank(getAddressPostalCode())
						|| StringUtils.isBlank(getAddressCountry()) || StringUtils.isBlank(getAddressPhoneNumber())
						|| StringUtils.isBlank(getCreditCardCVV())) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVE, this);
				} else {
					String nickName = getBillingAddressNickName();
					if(isRestService() && StringUtils.isNotBlank(getBillingAddressNickName())) {
						boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(getBillingAddressNickName());
						if(invalidNkName) {
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
							getErrorHandler().processException(mVE, this);
							return;
						}
					}
					boolean isCardExpired = getProfileManager().validateCardExpiration(
							getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
							getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
					boolean isDuplicateNickName = getProfileManager().validateDuplicateAddressNickname(getProfile(), nickName);
					if (StringUtils.isBlank(getSelectedCardBillingAddress()) && isDuplicateNickName) {
						if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)){
							setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS);
						}else{
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_DUPLICATE_ADDR_NICKNAME, null);
							getErrorHandler().processException(mVE, this);
						}
					} else if (isCardExpired) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
						getErrorHandler().processException(mVE, this);
					}
				}
			} else {
				if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getCreditCardNumber())
						|| StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
						|| StringUtils.isBlank(getCreditCardCVV()) || StringUtils.isBlank(getSelectedCardBillingAddress())) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVE, this);
				}
			}
			if (!StringUtils.isEmpty(getEditCardNickName())) {
				RepositoryItem creditCardNickName = ((TRUProfileTools) getProfileTools()).getCreditCardByNickname(
						getEditCardNickName(), getProfile());
				if (creditCardNickName == null) {
					mVE.addValidationError(TRUErrorKeys.EDIT_CREDIT_CARD_NAME, null);
					getErrorHandler().processException(mVE, this);
				}
			}
		} catch (RepositoryException repExc) {
			vlogError("Repository exception occured: {0}", repExc);
		}
		if (isLoggingDebug()) {
			logDebug("End of preUpdateCreditCard method");
		}
	}

	/**
	 * This method is for adding new credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleUpdateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of handleUpdateCreditCard method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		preUpdateCreditCard(pRequest, pResponse);
		if (((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD)))
				&& (!getFormError())) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
				}
				final String creditCardNickName = getEditCardNickName();
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					boolean isAddressValidated = true;
					if (TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressCountry())) {
						isAddressValidated = addressDoctorValidation(pRequest,TRUConstants.UPDATE_ADDRESS);
					}
					
					callZeroAuthForCardVerification(isAddressValidated);
					
					if (!getFormError() && isAddressValidated) {
						if (StringUtils.isBlank(getSelectedCardBillingAddress())) {
								((TRUProfileTools) getProfileTools()).updateCardWithNewAddress(getProfile(), getAddressValue(),
										getCreditCardInfoMap(), getBillingAddressNickName(), creditCardNickName);
						} else {
								((TRUProfileTools) getProfileTools()).updateCardWithExistingAddress(getProfile(),
										getSelectedCardBillingAddress(), getCreditCardInfoMap(), creditCardNickName,
										getAddressValue());
						}
					}
				}
			}catch (InstantiationException iexe) {
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", iexe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}catch(IllegalAccessException ilexe){
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", ilexe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}
			catch(ClassNotFoundException cexe){
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", cexe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}catch (RepositoryException e) {
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", e);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}catch(IntrospectionException inexe){
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", inexe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}
			catch(TransactionDemarcationException texe){
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", texe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}
			catch(PropertyNotFoundException pexe){
				lRollbackFlag = true;
				vlogError("Exception occured while updating credit card with exception : {0}", pexe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
			}finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException occured while updating credit card with exception: {0} ", tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_CREDIT_CARD);
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method is used Call the Zero authorization for card verification.
	 * 
	 * @param pAddressValidated
	 *            boolean value
	 * @throws ServletException
	 *             ServletException
	 */
	private void callZeroAuthForCardVerification(boolean pAddressValidated) throws ServletException {
		
		// Calling the Zero authorization for card verification.
		if (!getFormError() && pAddressValidated && ((TRUProfileTools)getProfileTools()).isEnableValidateCard()) {
			ContactInfo contactInfo = new ContactInfo();
			contactInfo = getProfileManager().populateContactInfo(contactInfo, getAddressValue());
			contactInfo.setEmail((String) getProfile().getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
			if (StringUtils.isBlank(getCreditCardInfoMap().get(getPropertyManager().getCreditCardTokenPropertyName()))) {
				getCreditCardInfoMap().put(getPropertyManager().getCreditCardTokenPropertyName(),
						getCreditCardInfoMap().get(getPropertyManager().getCreditCardNumberPropertyName()));
			}
			TRUCreditCardStatus creditCardStatus = ((TRUPurchaseProcessHelper) getShippingHelper().getPurchaseProcessHelper()).
					verifyZeroAuthorization(TRUPaymentConstants.DOUBLE_INITIAL_VALUE, getSiteContextManager().getCurrentSite().getId(), 
							getCreditCardInfoMap(), contactInfo, getProfile().getRepositoryId());
			// For any error adding it to the form errors.
			if (creditCardStatus != null && !creditCardStatus.getTransactionSuccess()) {
				mVE.addValidationError(creditCardStatus.getErrorMessage(), null);
				getErrorHandler().processException(mVE, this);
			}
		}
		
	}

	/**
	 * This method is used to delete the credit card from the profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleDeleteCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("Start of handleDeleteCreditCard method, Nick Name is {0}", getDeleteCardNickName());
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_DELETE_CREDIT_CARD))) {
			try {
				preDeleteCreditCard(pRequest, pResponse);
				if ((!getFormError()) && (tm != null)) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					((TRUProfileTools) getProfileTools()).removeProfileCreditCard(getProfile(), getDeleteCardNickName(),getShippingGroupMapContainer());

					// Start :: TUW-43619/46853 defect changes.
					Profile profile = getProfile();
					final String selectedCCNickName = (String) profile.getPropertyValue(getPropertyManager()
							.getSelectedCCNickNamePropertyName());
					if ((selectedCCNickName != null && getDeleteCardNickName() != null)
							&& selectedCCNickName.equalsIgnoreCase(getDeleteCardNickName())) {
						profile.setPropertyValue(getPropertyManager().getSelectedCCNickNamePropertyName(), TRUConstants.EMPTY);
					}
					// End :: TUW-43619/46853 defect changes.
				}
			} catch (RepositoryException repoEx) {
				lRollbackFlag = true;
				vlogError("Repository Exception occured while deleting the credit card with exception : {0}", repoEx);
			} catch (TransactionDemarcationException tdExe) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while updating credit card with exception : {0}", tdExe);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException occured while updating credit card with exception: {0} ", tD);
					}
				}
			}
			postDeleteCreditCard(pRequest, pResponse);
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for the actions to be performed before deleting credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 */
	protected void preDeleteCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preDeleteCreditCard()");
		}
		if (!StringUtils.isEmpty(getDeleteCardNickName())) {
			final RepositoryItem creditCardNickName = ((TRUProfileTools) getProfileTools()).getCreditCardByNickname(
					getDeleteCardNickName(), getProfile());
			if (creditCardNickName == null) {
				mVE.addValidationError(TRUErrorKeys.REMOVE_CREDIT_CARD_NAME, null);
				getErrorHandler().processException(mVE, this);
			}

		}
		if (StringUtils.isEmpty(getDeleteCardNickName())) {
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
			getErrorHandler().processException(mVE, this);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preDeleteCreditCard()");
		}
	}

	/**
	 * This method is for the actions to be performed after deleting credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 */
	protected void postDeleteCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postDeleteCreditCard()");
		}
		try {
			((TRUProfileTools) getProfileTools()).setProfileDefaultCard(getProfileItem());
			PaymentGroupMapContainer  paymentGroupMapContainer = getPaymentGroupMapContainer();
			final CreditCard creditCard = (CreditCard) paymentGroupMapContainer.getPaymentGroup((getDeleteCardNickName()));
			if (creditCard != null && paymentGroupMapContainer !=null) {
				if (getDeleteCardNickName().equalsIgnoreCase(paymentGroupMapContainer.getDefaultPaymentGroupName())) {
					paymentGroupMapContainer.setDefaultPaymentGroupName(null);
				}
				paymentGroupMapContainer.removePaymentGroup(getDeleteCardNickName());
			}	
		} catch (RepositoryException e) {
			vlogError("Repository exception occured in TRUProfileFormHandler.postDeleteCreditCard: {0}", e);
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postDeleteCreditCard()");
		}
	}

	/**
	 * This method is used for doing back-end validation on new credit card before adding.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of preAddCreditCard method");
		}
		try {
			String ccType = null;
			final String creditCardNumber = getCreditCardInfoMap().get(getPropertyManager().getCreditCardNumberPropertyName());
			final String cardLength = getCreditCardInfoMap().get(TRUConstants.CARD_LENGTH);
			TRUPropertyManager propertyManager = getPropertyManager();
			if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(
					getTRUConfiguration().getCardNumberPLCC())) {
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
						getTRUConfiguration().getExpMonthAdjustmentForPLCC());
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
						getTRUConfiguration().getExpYearAdjustmentForPLCC());
			}
			if(StringUtils.isBlank(getBillingAddressNickName()) && StringUtils.isNotBlank(getAddressFirstName())
				&& StringUtils.isNotBlank(getAddressLastName())) {
				String billingAddressNickName = getAddressFirstName()+TRUConstants.WHITE_SPACE+getAddressLastName();
				billingAddressNickName = ((TRUProfileTools)getProfileTools()).
							getUniqueShippingAddressNickname(getAddressValue(), getProfileItem(), billingAddressNickName);
				setBillingAddressNickName(billingAddressNickName);
				getAddressValue().put(TRUConstants.ADDRESS_NICK_NAME, billingAddressNickName);
			}
			if(isRestService() && StringUtils.isNotBlank(getBillingAddressNickName())) {
				boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(getBillingAddressNickName());
				if(invalidNkName) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
					getErrorHandler().processException(mVE, this);
					return;
				}
			}if (getAddressValue().get(TRUConstants.BILLING_ADDRESS_VISIBLE).equalsIgnoreCase(TRUConstants.TRUE)) {
				getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITH_ADDR_FORM, this);
				if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getCreditCardNumber())
						|| StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
						|| StringUtils.isBlank(getBillingAddressNickName()) || StringUtils.isBlank(getAddressFirstName())
						|| StringUtils.isBlank(getAddressLastName()) || StringUtils.isBlank(getAddressAddressOne())
						|| StringUtils.isBlank(getAddressCity()) || StringUtils.isBlank(getAddressState())
						|| StringUtils.isBlank(getAddressPostalCode()) || StringUtils.isBlank(getAddressCountry())
						|| StringUtils.isBlank(getAddressPhoneNumber()) || StringUtils.isBlank(getCreditCardCVV())) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVE, this);
				} else {
					ccType = ((TRUProfileTools) getProfileTools()).getCreditCardTypeFromRepository(creditCardNumber, cardLength);
					final String nickName = getBillingAddressNickName();
					final boolean isCardExpired = getProfileManager().validateCardExpiration(
							getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
							getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
					final boolean isDuplicateNickName = getProfileManager().validateDuplicateAddressNickname(getProfile(),
							nickName);
					if (StringUtils.isBlank(ccType)) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_INVALID, null);
						getErrorHandler().processException(mVE, this);
					} else if (isCardExpired) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
						getErrorHandler().processException(mVE, this);
					} else if (isLastOptionSelected() && isDuplicateNickName) {
						if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)){
							setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS);
						}else{
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_DUPLICATE_ADDR_NICKNAME, null);
							getErrorHandler().processException(mVE, this);
						}
					}
					if (StringUtils.isNotBlank(ccType)) {
						getCreditCardInfoMap().put(getPropertyManager().getCreditCardTypePropertyName(), ccType);
					}
				}
			} else {
				getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
				if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getCreditCardNumber())
						|| StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
						|| StringUtils.isBlank(getCreditCardCVV()) || StringUtils.isBlank(getSelectedCardBillingAddress())) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVE, this);
				}
			}
			// Change Country from UNITED STATES to US.
			if (!getAddressValue().isEmpty()) {
				((TRUProfileTools) getProfileTools()).trimMapValues(getAddressValue());
				((TRUProfileTools) getProfileTools()).trimMapValues(getCreditCardInfoMap());
				((TRUProfileTools) getProfileTools()).changeCountryValue(getAddressValue());
			}

		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End of preAddCreditCard method");
		}
	}

	/**
	 * This method is for adding new credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start of handleAddCreditCard method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD))) {
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
			}
			preAddCreditCard(pRequest, pResponse);
			if (!getFormError() && (tm != null)) {
				try {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					boolean isAddressValidated = true;
					if (TRUConstants.UNITED_STATES.equalsIgnoreCase(getAddressCountry())) {
						isAddressValidated = addressDoctorValidation(pRequest,TRUConstants.ADD_ADDRESS);
					}
					
					callZeroAuthForCardVerification(isAddressValidated);
					
					if (!getFormError() && isAddressValidated) {
						if (getCreditCardInfoMap() != null
								&& !StringUtils.isBlank(getCreditCardInfoMap().get(TRUConstants.CREDIT_CARD_TOKEN))) {
							getCreditCardInfoMap().put(TRUConstants.CREDIT_CARD_NUMBER,
									getCreditCardInfoMap().get(TRUConstants.CREDIT_CARD_TOKEN));
						}
						 RepositoryItem billingAddress = ((TRUProfileTools) getProfileTools()).getSecondaryAddressByNickName(getProfile(), getSelectedCardBillingAddress());
						if (billingAddress==null || StringUtils.isBlank(getSelectedCardBillingAddress())) {
							((TRUProfileTools) getProfileTools()).addCardWithNewAddress(getProfile(), getAddressValue(),
									getCreditCardInfoMap(), getBillingAddressNickName(), getPaymentGroupMapContainer());
						} else {
							vlogDebug("Adding  as billing address for credit card :{0}", getSelectedCardBillingAddress());
								((TRUProfileTools) getProfileTools()).addCardWithExistingAddress(getProfile(),
										getSelectedCardBillingAddress(), getCreditCardInfoMap(), getAddressValue(), getPaymentGroupMapContainer());
						}
					}
				} catch (RepositoryException rexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", rexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (IntrospectionException iexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", iexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (PropertyNotFoundException pexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", pexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (InstantiationException inexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", inexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (IllegalAccessException ilexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", ilexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (ClassNotFoundException cexe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", cexe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (TransactionDemarcationException texe) {
					lRollbackFlag = true;
					vlogError("Exception occured while adding credit card with exception : {0}", texe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} finally {
					if (tm != null) {
						try {
							td.end(lRollbackFlag);
						} catch (TransactionDemarcationException tD) {
							vlogError("TransactionDemarcationException occured while adding credit card with exception: {0}", tD);
							PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
						}
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
					}
					if (PerformanceMonitor.isEnabled()) {
						PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
					}
				}
				if (isLoggingDebug()) {
					logDebug("End: TRUProfileFormHandler.handleAddCreditCard()");
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * address Doctor Validation.
	 *
	 * @param pRequest - request
	 * @param pTaskName the task name
	 * @return isAddressValidated - is address validated
	 * @throws ServletException - servlet exception
	 */
	private boolean addressDoctorValidation(DynamoHttpServletRequest pRequest, String pTaskName)
			throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start of addressDoctorValidation method");
		}
		boolean isEnableAddressDoctor = false;
		boolean isAddressValidated = true;		
			if (SiteContextManager.getCurrentSite() != null) {
				isEnableAddressDoctor = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
			}

			if (isEnableAddressDoctor) {
				isAddressValidated = Boolean.parseBoolean(getAddressValue().get(TRUConstants.ADDRESS_VALIDATED));
			}
			
			TRUOrderImpl order = (TRUOrderImpl) getShoppingCart().getCurrent();
			getAddressValue().put(TRUConstants.ORDER_ID, order.getId());
			getAddressValue().put(TRUConstants.USER_ID, getProfile().getRepositoryId());
			getAddressValue().put(TRUConstants.PAGE, TRUConstants.MY_ACCOUNT_PAGE);
			getAddressValue().put(TRUConstants.TASK_NAME, pTaskName);
			if (!isAddressValidated) {
				final String addressStatus = ((TRUProfileTools) getProfileTools()).validateAddress(getAddressValue());
				setAddressDoctorProcessStatus(addressStatus);
				setAddressDoctorResponseVO(((TRUProfileTools) getProfileTools()).getAddressDoctorResponseVO());
				String derivedStatus = getAddressDoctorResponseVO().getDerivedStatus();
				if (addressStatus != null && TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
					isAddressValidated = true;
				} else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
					setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
					TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
					addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
					setAddressDoctorResponseVO(addressDoctorResponseVO);
				} else if(derivedStatus.equalsIgnoreCase(TRUConstants.NO)){
					isAddressValidated = true;
					if(TRUConstants.ADDRESS_DOCTOR_STATUS_CORRECTED.equalsIgnoreCase(addressStatus)){
						addVOAdressToMap(getAddressDoctorResponseVO().getAddressDoctorVO().get(0), getAddressValue());
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("End of addressDoctorValidation method");
			}
		return isAddressValidated;
	}
	
	/**
	 * address of VO added to Map.
	 * @param pTruAddressDoctorVO - TRUAddressDoctorVO
	 * @param pAddressValue - Map<String, String>
	 */
	private void addVOAdressToMap(TRUAddressDoctorVO pTruAddressDoctorVO, Map<String, String> pAddressValue) {
		pAddressValue.remove(TRUConstants.ADDRESS_1);
		
		pAddressValue.remove(TRUConstants.CITY);
		pAddressValue.remove(TRUConstants.STATE);
		pAddressValue.remove(TRUConstants.POSTAL_CODE_FOR_ADD);
		pAddressValue.put(TRUConstants.ADDRESS_1,pTruAddressDoctorVO.getAddress1());
		if(!StringUtils.isBlank(pTruAddressDoctorVO.getAddress2())){
			pAddressValue.remove(TRUConstants.ADDRESS_2);	
		   pAddressValue.put(TRUConstants.ADDRESS_2,pTruAddressDoctorVO.getAddress2());
		}
		pAddressValue.put(TRUConstants.CITY,pTruAddressDoctorVO.getCity());
		pAddressValue.put(TRUConstants.STATE,pTruAddressDoctorVO.getState());
		pAddressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD,pTruAddressDoctorVO.getPostalCode());
	}

	/**
	 * This method is for the actions to be performed after adding credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 */
	protected void postAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postAddCreditCard()");
		}
		try {
			((TRUProfileTools) getProfileTools()).setProfileDefaultCard(getProfileItem());
		} catch (RepositoryException e) {
			vlogError("Repository exception occured in TRUProfileFormHandler.postAddCreditCard: {0}", e);
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postAddCreditCard()");
		}
	}

	/**
	 * This method overrides the pre-login user, copies the value of login provided by the user to email as well.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void preLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preLoginUser()");
		}

		if(isUserLoggedIn()){
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_RESTRICT_LOGIN, null);  
			getErrorHandler().processException(mVE, this);
		} else {
			if (StringUtils.isBlank(getLogin()) || StringUtils.isBlank(getPassword())) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_LOGIN_MISSING_MANDATORY_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else if (getLogin().length() < TRUConstants.SIX) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_LOGIN_INVALID_EMAIL, null);
				getErrorHandler().processException(mVE, this);
			} else {
				if (getEmailPasswordValidator().validateEmail(getLogin())) {
					if (isUserAlreadyExists(getLogin(), pRequest, pResponse)) {
	
						if (getProfileTools().isValidCredentials(getLogin(), getPassword())) {
							super.preLoginUser(pRequest, pResponse);
						} else {
	
							final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
							boolean isPassword = profileTools.checkForMigratedProfile(getLogin());
							if (isPassword) {
								mVE.addValidationError(TRUErrorKeys.TRU_RESENT_PASSWORD, null);
								getErrorHandler().processException(mVE, this);
								return;
							}
							if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_WRONG_PASSWORD)){
								setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_WRONG_PASSWORD);
							}
							else{
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_WRONG_PASSWORD, null);
								getErrorHandler().processException(mVE, this);
							}
						}
					} else {
						if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_NOT_REGISTERED)){
							setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_NOT_REGISTERED);
						}else{
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_USER_NOT_FOUND, null);
							getErrorHandler().processException(mVE, this);
						}
					}
				} else {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_LOGIN_INVALID_EMAIL, null);
					getErrorHandler().processException(mVE, this);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preLoginUser()");
		}
	}

	/**
	 * This method overrides the pre-create user, copies the value of login provided by the user to email as well.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void preCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preCreateUser()");
		}

		boolean lValidLogin = true;
		boolean lValidPassword = true;
		final String login = getStringValueProperty(getPropertyManager().getLoginPropertyName()).toLowerCase().trim();
		final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName()).trim();
		final String confirmPassword = getStringValueProperty(getPropertyManager().getConfirmpasswordPropertyName()).trim();

		if(isUserLoggedIn()){
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_RESTRICT_LOGIN, null);  
			getErrorHandler().processException(mVE, this);
		} else {
			if (StringUtils.isBlank(login)) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REGISTRATION_MISSING_MANDATORY_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else {
				if (StringUtils.isBlank(password) || StringUtils.isBlank(confirmPassword)) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REGISTRATION_MISSING_PASSWORD, null);
					getErrorHandler().processException(mVE, this);
				} else if (login.length() < TRUConstants.SIX) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_LOGIN_INVALID_EMAIL, null);
					getErrorHandler().processException(mVE, this);
				} else {
					if (getEmailPasswordValidator().validateEmail(login)) {
						if (getProfileManager().validatePasswordMatch(password, confirmPassword)) {
							if (!getEmailPasswordValidator().validatePassword(password) && isUserAlreadyExists(login, pRequest, pResponse)) {
								lValidPassword = false;
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_REGISTERED, null);
								getErrorHandler().processException(mVE, this);
							}else if(!getEmailPasswordValidator().validatePassword(password)){
								lValidPassword = false;
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_INVALID_PASSWORD, null);
								getErrorHandler().processException(mVE, this);
							}
							if (lValidLogin && lValidPassword && isUserAlreadyExists(login, pRequest, pResponse)) {
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ALREADY_REGISTERED, null);
								getErrorHandler().processException(mVE, this);
							}
						} else {
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_MISMATCH, null);
							getErrorHandler().processException(mVE, this);
						}
					} else {
						lValidLogin = false;
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REGISTRATION_INVALID_EMAIL, null);
						getErrorHandler().processException(mVE, this);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preCreateUser()");
		}
	}

	/**
	 * This method is to check whether user is already registered or not.
	 * 
	 * @param pLogin
	 *            String
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/failure
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	private boolean isUserAlreadyExists(String pLogin, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.isUserAlreadyExists()");
		}
		boolean isUserExists = false;
		try {
			isUserExists = userAlreadyExists(pLogin, null, pRequest, pResponse);
		} catch (RepositoryException repoExc) {
			vlogError("RepositoryException occured while checking whether user exists or not  with exception : {0}", repoExc);
		}
		return isUserExists;
	}

	/**
	 * This method overrides the OOTB post create user. It generates the cookie for profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void postCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postCreateUser()");
		}
		getCookieManager().generateProfileNameCookie(getProfile());//Address orderBillingAddress = ((TRUOrderImpl)pCurrentOrder).getBillingAddress();
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
		RepositoryItem  billingAddressItem=null;
		final String emailId = (String) getValueProperty(propertyManager.getEmailAddressPropertyName());
		boolean isEpslonEnabled = Boolean.FALSE;
		isEpslonEnabled = getProfileManager().getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
		if (getTRUConfiguration() != null && isEpslonEnabled) {
			profileTools.sendEpslonEmailToUser(emailId);
		}
		final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName()).trim();
		try {
			profileTools.updateProperty(propertyManager.getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), getProfile());
			profileTools.updateProperty(((TRUPropertyManager)profileTools.getPropertyManager()).getLastProfileUpdatePropertyName(), new Date(), getProfile());
		} catch (RepositoryException e) {
			vlogError("Repository exception occured in TRUProfileFormHandler.postCreateUser: {0}", e);
		}
		super.postCreateUser(pRequest, pResponse);
		//Start:Code Added for defect TUW-66525
		TransactionLockService lockService = null;
		TRUOrderImpl order = (TRUOrderImpl) getShoppingCart().getCurrent();
		try {
			lockService = getLockManager().acquireLock();
			synchronized (order) {
				if(order !=null && StringUtils.isNotBlank(order.getBillingAddressNickName())){
					boolean isOrderUpdated = Boolean.FALSE;
					billingAddressItem = getPurchaseProcessHelper().getProfileTools().getSecondaryAddressByNickName(getProfile(), order.getBillingAddressNickName());
					if(billingAddressItem == null) {
						order.setBillingAddress(null);
						order.setBillingAddressNickName(null);
						if(!getProfile().getRepositoryId().equalsIgnoreCase(order.getProfileId())) {
							order.setProfileId(getProfile().getRepositoryId());
						}
						isOrderUpdated = Boolean.TRUE;
					}
					if(isOrderUpdated) {
						((TRUOrderManager)getOrderManager()).getPaymentGroupManager().recalculatePaymentGroupAmounts(order);
						getOrderManager().updateOrder(order);
					}
				}
			}
		} catch (CommerceException commerceException) {
			if (isLoggingError()) {
				vlogError("CommerceException in @method: TRUProfileFormHandler.postCreateUser: for orderId:{0} profileId:{1}", order.getId(), getProfile().getRepositoryId());
				vlogError("CommerceException in @method: TRUProfileFormHandler.postCreateUser: {0}", commerceException);
			}
		} catch (NoLockNameException ne) {
			if (isLoggingError()) {
				vlogError("NoLockNameException in @method: TRUProfileFormHandler.postCreateUser: {0}", ne);
			}
		} catch (DeadlockException de) {
			if (isLoggingError()) {
				vlogError("DeadlockException in @method: TRUProfileFormHandler.postCreateUser: {0}", de);
			}
		} finally {
			if (lockService != null) {
				getLockManager().releaseLock(lockService);
			}
		}
		//End:Code Added for defect TUW-66525
		// For Removing the cart cookie
		getCookieManager().removeCartCookie(pRequest, pResponse);
		//This is for order update service call
		String orderId = getOrderId();
		// Send Customer Master Import Message
		if (getCustomerMasterImportSource() != null) {
			getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), orderId);
		}
		setOrderId(null);
		// setting the last updated time to profile 
		try {
			profileTools.updateProperty(propertyManager.getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), getProfile());
			profileTools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
		} catch (RepositoryException repoExc) {
			vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
		}
		getSetaLogger().logCreateAccount(emailId);
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postCreateUser()");
		}
	}

	/**
	 * This method is for clearing List of recent viewed products of user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean true
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleClearRecentViewedProducts(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUConstants.HANDLER_NAME_RECNTLY_VIEVWED;
		List<String> recentProducts = null;
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.ClearRecentViwedProducts()");
		}
		try {
			Profile profile = getProfile();
			if (profile != null) {
				recentProducts = (List<String>) profile.getPropertyValue(getPropertyManager().getRecentlyViewedPropertyName());
			}
			if (recentProducts != null && !recentProducts.isEmpty()) {
				recentProducts.clear();
			}
		} finally {
			if (rrm != null) {
				rrm.removeRequestEntry(myHandleMethod);
			}
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.endOperation(myHandleMethod);
			}
		}

		return true;

	}

	/**
	 * This method is for registering an user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	public boolean handleCreate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleCreate()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE))) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE);
				}
				preCreateUser(pRequest, pResponse);
				if (!getFormError()) {
					final String loginPropertyName = getPropertyManager().getLoginPropertyName();
					final String login = getStringValueProperty(loginPropertyName);
					final String emailPropertyName = getPropertyManager().getEmailAddressPropertyName();
					final String rewardNumberPropertyName = getPropertyManager().getRewardNumberPropertyName();
					setValueProperty(emailPropertyName, login.toLowerCase().trim());
					setValueProperty(loginPropertyName, login.toLowerCase().trim());
					setValueProperty(rewardNumberPropertyName, TRUConstants.EMPTY_STRING);
					super.handleCreate(pRequest, pResponse);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleCreate()");
		}
		if (isAjax()) {
			return true;
		} else {
			return checkFormRedirect(getCreateSuccessURL(), getCreateErrorURL(), pRequest, pResponse);
		}
	}

	/**
	 * This method is for making the user to login.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	public boolean handleLogin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("STARTED :: TRUProfileFormHandler ::  LoginUser");
		}
		final RepeatingRequestMonitor repeatRequestMonitor = getRepeatingRequestMonitor();
		if (repeatRequestMonitor == null || repeatRequestMonitor.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_LOGIN)) {
			try {
				preLoginUser(pRequest, pResponse);
				
				String login = getLogin();
				
				if (!getFormError()) {
					super.handleLogin(pRequest, pResponse);
				}

				if (getFormError()) {
					//log for login failure
					getSetaLogger().logLoginFailure(login);
				}else{
					getSetaLogger().logLoginSuccess(login);
				}
				
			} finally {
				if (repeatRequestMonitor != null) {
					repeatRequestMonitor.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_LOGIN);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("ENDED :: TRUProfileFormHandler ::  LoginUser");
		}
		if (isAjax()) {
			return true;
		} else {
			return checkFormRedirect(getLoginSuccessURL(), getLoginErrorURL(), pRequest, pResponse);
		}
	}

	/**
	 * This method is for the actions to be performed once the user is logging in.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void postLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("STARTED :: TRUProfileFormHandler ::  postLoginUser");
		}
		super.postLoginUser(pRequest, pResponse);
		getCookieManager().generateProfileNameCookie(getProfile());
		getCookieManager().removeCartCookie(pRequest, pResponse);
		OrderHolder shoppingCart = getShoppingCart();
		TransactionLockService lockService = null;
		TRUOrderImpl order = null;
		Transaction tr = null;
		Boolean isRollBack = Boolean.FALSE;
		TransactionManager transactionManager = getTransactionManager();
		try{
			lockService = getLockManager().acquireLock();
			tr = ensureTransaction();
			synchronized (shoppingCart) {
				if (getShoppingCart().getCurrent() == null) {
					if (isLoggingDebug()) {
						logDebug("END :: TRUProfileFormHandler getShoppingCart().getCurrent() is null::  postLoginUser");
					}
				}

				order = (TRUOrderImpl) shoppingCart.getCurrent();
				if(!getProfile().getRepositoryId().equalsIgnoreCase(order.getProfileId())) {
					order.setProfileId(getProfile().getRepositoryId());
				}
				//synchronized (order) {
				boolean isOrderOnlyInStorePickupItems = getShippingHelper().getPurchaseProcessHelper().isOrderHavingOnlyInstorePickupItems(order);
				boolean isOrderOnlyDonationItems = getShippingHelper().getPurchaseProcessHelper().isOrderHavingOnlyDonationItems(order);
				if (isOrderOnlyInStorePickupItems) {
					order.setShowMeGiftingOptions(false);
					order.setCurrentTab(TRUCheckoutConstants.PICKUP_TAB);
				} else if (isOrderOnlyDonationItems) {
					order.setShowMeGiftingOptions(false);
					order.setCurrentTab(TRUCheckoutConstants.PAYMENT_TAB);
				} else {
					boolean orderMandateForMultiShipping = order.isOrderMandateForMultiShipping();
					if (orderMandateForMultiShipping) {
						order.setOrderIsInMultiShip(true);
					} else {
						order.setOrderIsInMultiShip(false);
					}

					if(order.getShipToHomeItemsCount() > TRUConstants._0) {
						order.setCurrentTab(TRUCheckoutConstants.SHIPPING_TAB);
					} else if(order.isOrderIsInStorePickUp()) {
						order.setShowMeGiftingOptions(false);
						order.setCurrentTab(TRUCheckoutConstants.PICKUP_TAB);
					}
					//order.setCurrentTab(TRUCheckoutConstants.SHIPPING_TAB);
				}
				//Prcing:Setting CCTYpe from default PG
				List<PaymentGroup> paymentGroups = order.getPaymentGroups();
				if (paymentGroups != null) {
					for (PaymentGroup paymentGroup : paymentGroups) {
						if (paymentGroup instanceof CreditCard) {
							CreditCard creditCard = (CreditCard) paymentGroup;
							order.setCreditCardType(creditCard.getCreditCardType());
						}
					}
				}
				getCookieManager().createCartCookie(order, pRequest, pResponse, TRUConstants.LOGGEDEIN_CART_COOKIE_NAME);
				getShippingHelper().getPurchaseProcessHelper().getShippingGroupManager().removeEmptyShippingGroups(order);
				if (order.getCommerceItemCount() > 0 ||
						(null != ((TRUOrderImpl) order).getOutOfStockItems() && !((TRUOrderImpl) order).getOutOfStockItems().isEmpty())) {
					((TRUOrderManager)getOrderManager()).updateOrderBillingAddress((TRUOrderHolder)getShoppingCart(), order, getShippingGroupMapContainer());
				}
				// Calling method to remove old sucn codes.
				removeSingleUseCouponsFromProfile(pRequest);
				((TRUOrderManager)getOrderManager()).getPaymentGroupManager().recalculatePaymentGroupAmounts(order);
				Map extraParameters = new HashMap();
				final PricingModelHolder userPricingModels = getUserPricingModels();
				userPricingModels.initializePricingModels();
				getPurchaseProcessHelper().runProcessRepriceOrder(getTRUConfiguration().getOrderRepricingOption(),
						order, userPricingModels, getUserLocale(pRequest), getProfile(), extraParameters, null);
				getOrderManager().updateOrder(order);
				// ((TRUCookieManager)getCookieManager()).createCookieForOrderItemCount(getShoppingCart().getCurrent(),pRequest,pResponse);
				// ((TRUCookieManager)getCookieManager()).createCookieForFreeShippingProgressBar(getShoppingCart().getCurrent(),pRequest,pResponse);
				//}
			
			}
		} catch (CommerceException commerceException) {
			isRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("CommerceException in @method: TRUProfileFormHandler.postLoginUser: for orderId:{0} profileId:{1}", order.getId(), getProfile().getRepositoryId());
				vlogError("CommerceException in @method: TRUProfileFormHandler.postLoginUser: {0}", commerceException);
			}
		} catch (NoLockNameException ne) {
			isRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("NoLockNameException in @method: TRUProfileFormHandler.postLoginUser: {0}", ne);
			}
		} catch (DeadlockException de) {
			isRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("DeadlockException in @method: TRUProfileFormHandler.postLoginUser: {0}", de);
			}
		} catch (RepositoryException repoException) {
			isRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("RepositoryException in @method: TRUProfileFormHandler.postLoginUser: {0}", repoException);
			}
		} catch (RunProcessException re) {
			isRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("RunProcessException in @method: TRUProfileFormHandler.postLoginUser: {0}", re);
			}
		} finally {
			if(isRollBack && transactionManager != null){
				try {
					Transaction transaction = transactionManager.getTransaction();
					if(transaction != null){
						transaction.setRollbackOnly();
					}
				} catch (javax.transaction.SystemException e) {
					e.printStackTrace();
				}
			}
			if (tr != null) {
				commitTransaction(tr);
			}
			if (lockService != null) {
				getLockManager().releaseLock(lockService);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END :: TRUProfileFormHandler ::  postLoginUser");
		}
	}
	
	/**
	 * This method is to remove single use coupon.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @throws RepositoryException - RepositoryException
	 * @throws CommerceException - CommerceException
	 */
	@SuppressWarnings("unchecked")
	private void removeSingleUseCouponsFromProfile(DynamoHttpServletRequest pRequest) throws RepositoryException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("START :: TRUProfileFormHandler ::  removeSingleUseCouponsFromProfile");
		}
		if (getShoppingCart().getCurrent() == null) {
			return;
		}
		TRUOrderImpl order = (TRUOrderImpl) getShoppingCart().getCurrent();
		Profile profile = getProfile();
		List<String> giftCardPGList = new ArrayList<String>();
		//Changes for CR's: SUCN and Gift Card
		//Changes for CR's: Gift Card and SUCN coupon codes.
		((TRUOrderManager)getOrderManager()).removeOldDataFromProfile(order,profile);
		List<PaymentGroup> paymentGroups = order.getPaymentGroups();
		if (paymentGroups != null && !paymentGroups.isEmpty()) {
			for (final PaymentGroup paymentGroup : paymentGroups) {
				if (paymentGroup instanceof TRUGiftCard && TRUCheckoutConstants.GIFTCARD.equalsIgnoreCase(paymentGroup.getPaymentGroupClassType())) {
					giftCardPGList.add(paymentGroup.getId());
				}
			}
		}
		if (!giftCardPGList.isEmpty()) {
			for (String paymentGroupId : giftCardPGList) {
				getOrderManager().getPaymentGroupManager().removePaymentGroupFromOrder(order, paymentGroupId);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END :: TRUProfileFormHandler ::  removeSingleUseCouponsFromProfile");
		}
	}
	
	/**
	 * This method is for performing removing profile name cookie after logging out user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	public void postLogoutUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postLogoutUser()");
		}
		getCookieManager().removeCartCookie(pRequest, pResponse);
		getCookieManager().expireProfileNameCookie(TRUConstants.PROFILE_COOKIE_NAME);
		getCookieManager().expireProfileNameCookie(TRUConstants.PROFILE_FIRST_NAME);
		getCookieManager().setLogoutCookieForTealium(TRUConstants.DPS_LOGOUT_COOKIE);
		getCookieManager().initializingAsZero(TRUConstants.ITEM_COUNT_COOKIE, pRequest, pResponse);
		getCookieManager().initializingAsZero(TRUConstants.FREE_SHIPPING_PROGRESS_BAR_COOKIE, pRequest, pResponse);
		super.postLogoutUser(pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postLogoutUser()");
		}
	}

	/**
	 * This method is for changing the password for the users account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	public boolean handleChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleChangePassword()");
		}
		final RepeatingRequestMonitor repeatRequestMonitor = getRepeatingRequestMonitor();
		if (repeatRequestMonitor == null || 
				repeatRequestMonitor.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD)) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD);
				}
				preChangePassword(pRequest, pResponse);
				if (!getFormError()) {
					super.changePassword(pRequest, pResponse);
					postChangePassword(pRequest, pResponse);
				}

			} finally {
				if (repeatRequestMonitor != null) {
					repeatRequestMonitor.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_CHANGE_PASSWORD);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleChangePassword()");
		}
		return checkFormRedirect(getChangePasswordSuccessURL(), getChangePasswordErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method overrides the preChangePassword.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void preChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preChangePassword()");
		}
		try {
			final String oldPassword = getStringValueProperty(getPropertyManager().getOldpasswordPropertyName());
			final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName());
			final String confirmPassword = getStringValueProperty(getPropertyManager().getConfirmpasswordPropertyName());
			getValidator().validate(TRUConstants.MY_ACCOUNT_CHANGE_PASSWORD_FORM, this);
			if (StringUtils.isBlank(oldPassword) || StringUtils.isBlank(password) || StringUtils.isBlank(confirmPassword)) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CHANGE_PASSWORD_MISSING_MANDATORY_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else {
				if (getProfileManager().isValidOldPassword(oldPassword, getProfile())) {
					if (!(getProfileManager().validatePasswordMatch(password, confirmPassword))) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_MISMATCH, null);
						getErrorHandler().processException(mVE, this);
					} else if (!getEmailPasswordValidator().validatePassword(password)) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_INVALID_PASSWORD, null);
						getErrorHandler().processException(mVE, this);
					}
				} else {
					if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_OLD_PASSWORD_WRONG)){
						setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_OLD_PASSWORD_WRONG);
					}else{
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_OLD_PASSWORD_WRONG, null);
					getErrorHandler().processException(mVE, this);
					}
				}
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preChangePassword()");
		}
	}

	/**
	 * This method overrides the postChangePassword.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	@Override
	protected void postChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postChangePassword()");
		}
		try {
			boolean isEpslonEnabled = Boolean.FALSE;
			isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
			final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
			final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
			if (getTRUConfiguration() != null && isEpslonEnabled) {
				final String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
				if (getProfile() != null) {
					final String emailAddress = (String) getProfile().getPropertyValue(emailAddressPropertyName);
					final String lMyAccountURL = ((TRUProfileTools) getProfileTools()).generateURL(pRequest, getMyAccountURL());
					if ((!StringUtils.isBlank(emailAddress)) && (getChangePasswordSource() != null)) {
						getChangePasswordSource().sendEmailOnPasswordChange(emailAddress, lMyAccountURL);
					}
				}
			}
			// setting the last updated time to profile 
			final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName()).trim();
			try {
				profileTools.updateProperty(propertyManager.getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), getProfile());
				profileTools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
			} catch (RepositoryException repoExc) {
				vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
			}
			
		} catch (JMSException e) {
			vlogError("JMS EXCEPTION: {0}", e);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postChangePassword()");
		}
	}

	/**
	 * This method overrides the preUpdateProfileInfo.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preUpdateProfileInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preUpdateProfileInfo()");
		}
		try {

			if (isRestService()) {
				final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
				final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
				final String firstNamePropertyName = propertyManager.getFirstNamePropertyName();
				final String firstName = getStringValueProperty(firstNamePropertyName);
				final String lastNamePropertyName = propertyManager.getLastNamePropertyName();
				final String lastName = getStringValueProperty(lastNamePropertyName);
				final String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
				final String emailAddress = getStringValueProperty(emailAddressPropertyName);
				final String confirmEmail = getConfirmEmail();
				if ((StringUtils.isNotBlank(firstName) || StringUtils.isNotBlank(lastName)) && StringUtils.isBlank(emailAddress)
						&& StringUtils.isBlank(confirmEmail)) {
					setEmailUpdate(Boolean.FALSE);
				} else if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName)
						&& StringUtils.isNotBlank(emailAddress) && StringUtils.isNotBlank(confirmEmail)) {
					setEmailUpdate(Boolean.TRUE);
				} else {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_UPDATE_INVALID_REQUEST, null);
					getErrorHandler().processException(mVE, this);
					return;
				}

			}

			if (isEmailUpdate()) {
				if (StringUtils.isBlank(getStringValueProperty(getPropertyManager().getEmailAddressPropertyName()))) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CHANGE_EMAIL_MISSING_INFO, null);
					getErrorHandler().processException(mVE, this);
				} else {
					final String login = getStringValueProperty(getPropertyManager().getEmailAddressPropertyName()).toLowerCase();

					getValidator().validate(TRUConstants.MY_ACCOUNT_UPDATE_EMAIL_FORM, this);
					if (StringUtils.isBlank(getEmail()) || StringUtils.isBlank(getConfirmEmail())) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CHANGE_EMAIL_MISSING_INFO, null);
						getErrorHandler().processException(mVE, this);
					} else {
						if (getEmailPasswordValidator().validateEmail(getEmail().toLowerCase())) {
							final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
							final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
							final String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
							final String emailAddress = getStringValueProperty(emailAddressPropertyName).toLowerCase().trim();
							final String confirmEmail = getConfirmEmail().toLowerCase().trim();
							if (!(getProfileManager().validateEmailMatch(emailAddress, confirmEmail))) {
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_MISMATCH, null);
								getErrorHandler().processException(mVE, this);
							} else if (isUserAlreadyExists(login.trim(), pRequest, pResponse)) {
								mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_ALREADY_EXISTS, null);
								getErrorHandler().processException(mVE, this);
							}
						} else {
							mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_CHANGE_INVALID_PATTERN, null);
							getErrorHandler().processException(mVE, this);
						}
					}
				}
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preUpdateProfileInfo()");
		}
	}

	/**
	 * This method is for editing firstName/LastName and Email.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleUpdateProfileInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleUpdateProfileInfo()");
		}
		final RepeatingRequestMonitor repeatRequestMonitor = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if (repeatRequestMonitor == null ||
				repeatRequestMonitor.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO)) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
				}
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
					final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
					preUpdateProfileInfo(pRequest, pResponse);
					if (!getFormError() && !isEmailUpdate()) {
						final String firstNamePropertyName = propertyManager.getFirstNamePropertyName();
						final String firstName = getStringValueProperty(firstNamePropertyName);
						final String lastNamePropertyName = propertyManager.getLastNamePropertyName();
						final String lastName = getStringValueProperty(lastNamePropertyName);
						profileTools.updateProperty(firstNamePropertyName, firstName, getProfileItem());
						profileTools.updateProperty(lastNamePropertyName, lastName, getProfileItem());
					} else if (!getFormError() && isEmailUpdate()) {
						String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
						String emailAddress = getStringValueProperty(emailAddressPropertyName).toLowerCase().trim();
						String loginPropertyName = propertyManager.getLoginPropertyName();

						profileTools.updateProperty(emailAddressPropertyName, emailAddress, getProfileItem());
						profileTools.updateProperty(loginPropertyName, emailAddress, getProfileItem());
					}
				}
				postUpdateProfileInfo(pRequest, pResponse);
			} catch (RepositoryException repoExc) {
				lRollbackFlag = true;
				vlogError("RepositoryException occured while changing password with exception : {0}", repoExc);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
			} catch (TransactionDemarcationException tdExe) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured changing password with exception : {0}", tdExe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException occured changing password with exception {0}", tD);
						PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
					}
				}
				if (repeatRequestMonitor != null) {
					repeatRequestMonitor.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_UPDATE_PROFILE_INFO);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleUpdateProfileInfo()");
		}
		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for sending email change confirmation email to user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postUpdateProfileInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		boolean isEpslonEnabled = Boolean.FALSE;
		isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
		if (!getFormError() && isEmailUpdate() && getTRUConfiguration() != null && isEpslonEnabled) {
			final String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
			final String emailAddress = getStringValueProperty(emailAddressPropertyName);
			final String lMyAccountURL = ((TRUProfileTools) getProfileTools()).generateURL(pRequest, getMyAccountURL());
			if (!StringUtils.isBlank(emailAddress)) {
				if (getChangeEmailSource() == null) {
					vlogDebug("ChangeEmailSource Component is : {0}", getChangeEmailSource());
				} else {
					try {
						final String oldEmail = getUserOldEmailAddress();
						final String updatedEmail = emailAddress;
						getChangeEmailSource().sendEmailOnEmailChange(oldEmail, updatedEmail, lMyAccountURL);
					} catch (JMSException e) {
						vlogError("JMS EXCEPTION {0}", e);
					}
				}
			}
		}
		// Send Customer Master Import Message
		if (getCustomerMasterImportSource() != null) {
			getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), null);
		}
		// setting the last updated time to profile 
		try {
			profileTools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
		} catch (RepositoryException repoExc) {
			vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
		}
	}

	/**
	 * Reset the password - in case of forgot password.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleResetPassword()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD))) {
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD);
			}
			final TransactionManager tm = getTransactionManager();
			final TransactionDemarcation td = getTransactionDemarcation();
			MutableRepositoryItem profileItem = null;
			boolean lRollbackFlag = false;
			String lMyAccountURL = TRUConstants.EMPTY;
			preResetPassword(pRequest, pResponse);
			final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
			if (getFormError()) {
				return checkFormRedirect(getResetPasswordSuccessURL(), getResetPasswordErrorURL(), pRequest, pResponse);
			} else {
				final String lEncryptedEmail = getStringValueProperty(getPropertyManager().getEmailAddressPropertyName());
				final byte[] decodedBytes = Base64.decode(lEncryptedEmail);
				final String lDecryptedEmail = new String(decodedBytes);
				try {
					profileItem = (MutableRepositoryItem) getProfileTools().getItemFromEmail(lDecryptedEmail);
					if (profileItem == null) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_NOT_FOUND, null);
						getErrorHandler().processException(mVE, this);
					}
					if (getFormError()) {
						return checkFormRedirect(getResetPasswordSuccessURL(), getResetPasswordErrorURL(), pRequest, pResponse);
					}
					lMyAccountURL = ((TRUProfileTools) getProfileTools()).generateURL(pRequest, getMyAccountURL());
					vlogDebug("Email address to reset the password : {0} and profile item is : {1}", lDecryptedEmail, profileItem);
					if (tm == null || profileItem == null) {
						return checkFormRedirect(null, getResetPasswordErrorURL(), pRequest, pResponse);
					} else {
						td.begin(tm, TransactionDemarcation.REQUIRED);
						final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName());
						final String confirmPassword = getStringValueProperty(getPropertyManager().getConfirmPasswordPropertyName());
						getProfileTools().changePassword(profileItem, password, confirmPassword, null, true, false); 
						getProfileTools().updateProperty(getPropertyManager().getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), profileItem);
						getProfileTools().updateProperty(getPropertyManager().getLastProfileUpdatePropertyName(), new Date(), profileItem);
						if (getFormError()) {
							return checkFormRedirect(getResetPasswordSuccessURL(), getResetPasswordErrorURL(), pRequest,
									pResponse);
						} else {
							migratedProfile(pRequest, profileItem, lMyAccountURL,
									profileTools, lDecryptedEmail, password);
							return checkFormRedirect(getResetPasswordSuccessURL(), getResetPasswordErrorURL(), pRequest,
									pResponse);
						}
					}
				} catch (RepositoryException | TransactionDemarcationException | PasswordChangeException | PropertyNotFoundException exe) {
					lRollbackFlag = true;
					vlogError("Exception occured while changing password with exception : {0}", exe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD);
				}finally {
					if (tm != null) {
						try {
							td.end(lRollbackFlag);
						} catch (TransactionDemarcationException tD) {
							vlogError("TransactionDemarcationException {0}", tD);
							PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD);
						}
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD);
					}
					if (PerformanceMonitor.isEnabled()) {
						PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_RESET_PASSWORD);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleResetPassword()");
		}
		return checkFormRedirect(getResetPasswordSuccessURL(), getResetPasswordErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * 
	 * @param pRequest - request
	 * @param pProfileItem - profile item
	 * @param pMyAccountURL - MyAccountURL
	 * @param pProfileTools - profile tools
	 * @param pDecryptedEmail - DecryptedEmail
	 * @param pPassword - password
	 * @throws ServletException - servlet exception
	 * @throws RepositoryException - RepositoryException
	 */
	private void migratedProfile(DynamoHttpServletRequest pRequest,
			MutableRepositoryItem pProfileItem, String pMyAccountURL,
			final TRUProfileTools pProfileTools, final String pDecryptedEmail,
			final String pPassword) throws ServletException,RepositoryException{
		final boolean isMigratedProfile = pProfileTools.checkForMigratedProfile(pDecryptedEmail);
		if (isMigratedProfile) {
			if (isLoggingDebug()) {
				logDebug("TRUProfileFormHandler.migratedProfile() isMigratedProfile is true");
			}

			pProfileTools.updatePwdResetFlagMigProfile(pProfileItem);
		} else {
			if (isLoggingDebug()) {
				logDebug("Start: TRUProfileFormHandler.migratedProfile() isMigratedProfile is false");
			}
		}
		getProfileServices().loginUser(pDecryptedEmail, pPassword);
		boolean isEpslonEnabled = Boolean.FALSE;
		isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
		if (getTRUConfiguration() != null && isEpslonEnabled) {
			if (getPasswordResetSuccessSource() == null) {
				vlogDebug("PasswordResetSuccess Component is :", getPasswordResetSuccessSource());
			} else {
				try {
					getPasswordResetSuccessSource().sendPasswordResetSuccessEmail(pDecryptedEmail,
							pMyAccountURL);
				} catch (JMSException e) {
					vlogError("JMS EXCEPTION {0}", e);
				}
			}
		}
	}

	/**
	 * This method is for performing validations before resetting password.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (preResetPassword) : STARTS");
		}
		try {
			final String password = getStringValueProperty(getPropertyManager().getPasswordPropertyName());
			final String confirmPassword = getStringValueProperty(getPropertyManager().getConfirmPasswordPropertyName());
			getValidator().validate(TRUConstants.MY_ACCOUNT_RESET_PASSWORD_FORM, this);
			if (StringUtils.isBlank(password) || StringUtils.isBlank(confirmPassword)) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_RES_PASS_MISSING_MANDATORY_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else {
				if (!(getProfileManager().validatePasswordMatch(password, confirmPassword))) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_PASSWORD_MISMATCH, null);
					getErrorHandler().processException(mVE, this);
				} else if (!getEmailPasswordValidator().validatePassword(password)) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_INVALID_PASSWORD, null);
					getErrorHandler().processException(mVE, this);
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (preResetPassword): Ends");
		}
	}

	/**
	 * This method is for adding the reward number for the user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleAddRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleAddRewardMember()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_REWARD_MEMBER))) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preAddRewardMember(pRequest, pResponse);
					if (!getFormError()) {
						getProfileTools().updateProperty(getPropertyManager().getRewardNumberPropertyName(), getRewardNumber(),
								getProfileItem());
						getProfileTools().updateProperty(getPropertyManager().getloyaltyCustomer(), Boolean.TRUE,
								getProfileItem());
						postAddRewardMember(pRequest, pResponse);
					}
				}
			} catch (RepositoryException e) {
				lRollbackFlag = true;
				vlogError("Repository Exception", e);
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while adding rewardnumber with exception {0}", trDem);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException", tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_REWARD_MEMBER);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleAddRewardMember()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for performing validations before adding reward number pertaining to user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preAddRewardMember()");
		}
		try {
			getValidator().validate(TRUConstants.ADD_UPDATE_REWARDS_CARD_FORM, this);
			if (StringUtils.isBlank(getRewardNumber())) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REWARD_NUMBER_MISSING_INFO, null);
				getErrorHandler().processException(mVE, this);
			}
			else if (isAjax() && getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
				if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_MEMBERSHIPID_INVALID)){
					setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_MEMBERSHIPID_INVALID);
				}else{
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_MEMBERSHIPID_INVALID, null);
					getErrorHandler().processException(mVE, this);
				}
			} else if (!isAjax() && !getProfileManager().validateDenaliAccountNum(getRewardNumber())) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_MEMBERSHIPID_INVALID, null);
				getErrorHandler().processException(mVE, this);
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preAddRewardMember()");
		}
	}

	/**
	 * This method is for the required tasks to be done after adding reward number to user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postAddRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postAddRewardMember()");
		}
		//update/add the rewards number in order also if the user add/update in profile
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();
		TRUOrderImpl currentOrder = null;
		if (null != getShoppingCart().getCurrent()) {
			currentOrder = (TRUOrderImpl) getShoppingCart().getCurrent();
		}
		if (currentOrder != null) {
			TransactionLockService lockService = null;
			try {
				lockService = getLockManager().acquireLock();
				synchronized (currentOrder) {
					currentOrder.setRewardNumber((String) getProfile().getPropertyValue(
							getPropertyManager().getRewardNumberPropertyName()));
					getOrderManager().updateOrder(currentOrder);
				}
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					vlogError("CommerceException in @method: TRUProfileFormHandler.postAddRewardMember: for orderId:{0} profileId:{1}", currentOrder.getId(), getProfile().getRepositoryId());
					vlogError("CommerceException in @method: TRUProfileFormHandler.postAddRewardMember: {0}", commerceException);
				}
			} catch (NoLockNameException ne) {
				if (isLoggingError()) {
					vlogError("NoLockNameException in @method: TRUProfileFormHandler.postAddRewardMember: {0}", ne);
				}
			} catch (DeadlockException de) {
				if (isLoggingError()) {
					vlogError("DeadlockException in @method: TRUProfileFormHandler.postAddRewardMember: {0}", de);
				}
			} finally {
				if (lockService != null) {
					getLockManager().releaseLock(lockService);
				}
			}
		}
		// Send Customer Master Import Message
		if (getCustomerMasterImportSource() != null) {
			getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), null);
		}
		// setting the last updated time to profile 
		try {
			profileTools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
		} catch (RepositoryException repoExc) {
			vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postAddRewardMember()");
		}
	}

	/**
	 * This method is invoked when user confirms the request to delete the rewards number from profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleRemoveRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleRemoveRewardMember()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER))) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preRemoveRewardMember(pRequest, pResponse);
					getProfileTools().updateProperty(getPropertyManager().getRewardNumberPropertyName(), TRUConstants.EMPTY,
							getProfileItem());
					getProfileTools().updateProperty(getPropertyManager().getloyaltyCustomer(), Boolean.FALSE, getProfileItem());
					postRemoveRewardMember(pRequest, pResponse);
				}
			} catch (RepositoryException rExc) {
				vlogError("Repository Exception occured while removing the rewatd card number from profile with exception {0}",
						rExc);
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while removing reward number with exception {0}", trDem);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException", tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleRemoveRewardMember()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * pre method for handleRemoveRewardMember.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preRemoveRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preRemoveRewardMember()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preRemoveRewardMember()");
		}
	}

	/**
	 * post method for handleRemoveRewardMember.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postRemoveRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postRemoveRewardMember()");
		}
		//remove the rewards number from order also if the user removes it from profile
		TRUOrderImpl currentOrder = null;
		if (null != getShoppingCart().getCurrent()) {
			currentOrder = (TRUOrderImpl) getShoppingCart().getCurrent();
		}

		if (currentOrder != null) {
			TransactionLockService lockService = null;
			try {
				lockService = getLockManager().acquireLock();
				synchronized (currentOrder) {
					currentOrder.setRewardNumber((String) getProfile().getPropertyValue(
							getPropertyManager().getRewardNumberPropertyName()));
					getOrderManager().updateOrder(currentOrder);
				}
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					vlogError("CommerceException in @method: TRUProfileFormHandler.postRemoveRewardMember: for orderId:{0} profileId:{1}", currentOrder.getId(), getProfile().getRepositoryId());
					vlogError("CommerceException in @method: TRUProfileFormHandler.postRemoveRewardMember: {0}", commerceException);
				}
			} catch (NoLockNameException ne) {
				if (isLoggingError()) {
					vlogError("NoLockNameException in @method: TRUProfileFormHandler.postRemoveRewardMember: {0}", ne);
				}
			} catch (DeadlockException de) {
				if (isLoggingError()) {
					vlogError("DeadlockException in @method: TRUProfileFormHandler.postRemoveRewardMember: {0}", de);
				}
			} finally {
				if (lockService != null) {
					getLockManager().releaseLock(lockService);
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postRemoveRewardMember()");
		}
	}

	/**
	 * This method is for adding the shipping address to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleAddShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (handleAddShippingAddress): START");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS))) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preAddShippingAddress(pRequest, pResponse);
					if (!getFormError()) {
						boolean isAddressValidated = true;
						isAddressValidated = addressDoctorValidation(pRequest, TRUConstants.ADD_ADDRESS);
						if (isAddressValidated) {
							final Address addressObject = AddressTools.createAddressFromMap(getAddressValue(),
									TRUConstants.ADDRESS_CLASS_NAME);
							final String newAddressId = ((TRUProfileTools) getProfileTools())
									.createProfileRepositorySecondaryAddress(getProfile(),
											getAddressValue().get(TRUConstants.ADDRESS_NICK_NAME), addressObject);
							vlogDebug("New Addess Id in TRUProfileFormHandler.handleAddShippingAddress: " + newAddressId);
							((TRUProfileTools) getProfileTools()).setProfileDefaultAddress((getProfileItem()));
						}
				  }
				}
			} catch (TransactionDemarcationException e) {
				lRollbackFlag = true;
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				throw new ServletException(e);
			} catch (InstantiationException exe) {
				lRollbackFlag = true;
				vlogError(
						"IException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			}catch(IllegalAccessException exe){
				lRollbackFlag = true;
				vlogError(
						"IException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			}catch(ClassNotFoundException exe){
				lRollbackFlag = true;
				vlogError(
						"IException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			}catch(RepositoryException exe){
				lRollbackFlag = true;
				vlogError(
						"IException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			}catch(IntrospectionException exe){
				lRollbackFlag = true;
				vlogError(
						"IException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			}finally {
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				try {
					if (tm != null) {
						td.end(lRollbackFlag);
					}
				} catch (TransactionDemarcationException e) {
					vlogError("TransactionDemarcationException in TRUProfileFormHandler.handleEditShippingAddress: {0}", e);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (handleAddShippingAddress): END");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	

	/**
	 * This method is for doing the basic validations before adding the shipping address to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preAddShippingAddress()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		try {
			getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_ADDRESS_FORM, this);
			if (StringUtils.isBlank(getAddressNickName()) || StringUtils.isBlank(getAddressFirstName())
					|| StringUtils.isBlank(getAddressLastName()) || StringUtils.isBlank(getAddressAddressOne())
					|| StringUtils.isBlank(getAddressCity()) || StringUtils.isBlank(getAddressState())
					|| StringUtils.isBlank(getAddressPostalCode()) || StringUtils.isBlank(getAddressPhoneNumber())
					|| StringUtils.isBlank(getAddressCountry())) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else {
				if (!getAddressValue().isEmpty()) {
					profileTools.trimMapValues(getAddressValue());
					profileTools.changeCountryValue(getAddressValue());
				}
				final String nickName = (getAddressValue()).get(profileTools.getTRUPropertyManager().getNicknamePropertyName());
				
				if(isRestService() && StringUtils.isNotBlank(nickName) ) {
					boolean invalidNkName =  getStoreUtils().containsSpecialCharacter(nickName);
					if(invalidNkName) {
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME, null);
						getErrorHandler().processException(mVE, this);
						return;
					}
					
				}
				if (getProfileManager().validateDuplicateAddressNickname(getProfile(), nickName)) {
					if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)){
						setChannelError(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS);
					}else{
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME, null);
						getErrorHandler().processException(mVE, this);
					}
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preAddShippingAddress()");
		}
	}

	/**
	 * This method will set error 
	 * 
	 * @param pErrorMessage - ErrorMessage
	 * @throws ServletException - If any ServletException.
	 */
	private void setChannelError(String pErrorMessage) throws ServletException {
		if(isRestService()) {
			mVE.addValidationError(pErrorMessage, null);
			getErrorHandler().processException(mVE, this);
		} else {
			addFormException(new DropletException(pErrorMessage));
		}
	}

	/**
	 * This method is for editing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (handleEditShippingAddress): START");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS))) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preEditShippingAddress(pRequest, pResponse);
					if (!getFormError()) {
						boolean isAddressValidated = addressDoctorValidation(pRequest, TRUConstants.UPDATE_ADDRESS);
						if(isAddressValidated){
							((TRUProfileTools) getProfileTools()).updateProfileRepositoryAddress(getProfile(),
									getAddressValue());
						}
					}
				}
			} catch (TransactionDemarcationException e) {
				lRollbackFlag = true;
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				throw new ServletException(e);
			} catch (InstantiationException exe) {
				lRollbackFlag = true;
				vlogError(
						"Exception occured  in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			}catch(IllegalAccessException exe){
				lRollbackFlag = true;
				vlogError(
						"Exception occured  in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			}catch(ClassNotFoundException exe){
				lRollbackFlag = true;
				vlogError(
						"Exception occured  in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			}catch(RepositoryException exe){
				lRollbackFlag = true;
				vlogError(
						"Exception occured  in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			}catch(IntrospectionException exe){
				lRollbackFlag = true;
				vlogError(
						"Exception occured  in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						exe);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			}finally {
				if (isLoggingDebug()) {
					logDebug("<-- Remove Request Entry of RepeatingRequestMonitor from TRUProfileFormHandler.handleEditShippingAddress -->");
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				try {
					if (tm != null) {
						td.end(lRollbackFlag);
					}
				} catch (TransactionDemarcationException e) {
					vlogError("TransactionDemarcationException in TRUProfileFormHandler.handleEditShippingAddress: {0}", e);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileFormHandler (handleEditShippingAddress): END");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * This method is for the required tasks to be done before editing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preEditShippingAddress()");
		}
		try {
			getValidator().validate(TRUConstants.MY_ACCOUNT_EDIT_ADDRESS_FORM, this);
			if (StringUtils.isBlank(getAddressFirstName()) || StringUtils.isBlank(getAddressLastName())
					|| StringUtils.isBlank(getAddressAddressOne()) || StringUtils.isBlank(getAddressCity())
					|| StringUtils.isBlank(getAddressState()) || StringUtils.isBlank(getAddressPostalCode())
					|| StringUtils.isBlank(getAddressPhoneNumber()) || StringUtils.isBlank(getAddressCountry())
					|| StringUtils.isBlank(getAddressNickName())) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
				getErrorHandler().processException(mVE, this);
			}
			if (!getAddressValue().isEmpty()) {
				((TRUProfileTools) getProfileTools()).trimMapValues(getAddressValue());
				((TRUProfileTools) getProfileTools()).changeCountryValue(getAddressValue());
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preEditShippingAddress()");
		}
	}

	/**
	 * This method is invoked when user submits a request to remove the address from profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleRemoveShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleRemoveShippingAddress()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS))) {
			try {
				synchronized (getShoppingCart().getCurrent()) {
					if (tm != null) {
						td.begin(tm, TransactionDemarcation.REQUIRED);
						preRemoveShippingAddress(pRequest, pResponse);
						final String lAddressNickName = getStringValueProperty(TRUConstants.ADDRESS_NICK_NAME);

						if(isLoggingDebug()){
							vlogDebug("Nick name of address selected to remove {0} ", lAddressNickName);
						}
						if(StringUtils.isNotBlank(lAddressNickName)) {
							getShippingHelper().removeShippingAddress(getShoppingCart().getCurrent(), getProfileItem(), lAddressNickName,
									getShippingGroupMapContainer());
							postRemoveShippingAddress(pRequest, pResponse);
							getShippingHelper().getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL , getShoppingCart().getCurrent(), null, null,getProfileItem() , null, null);
							getOrderManager().updateOrder(getShoppingCart().getCurrent());
						}
					}
				}
			}catch (InvalidParameterException inExc) {
				vlogError(
						"InvalidParameterException occured in TRUProfileFormHandler.handleRemoveShippingAddress with exception: {0}",
						inExc);
			}catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError(
						"TransactionDemarcationException occured in TRUProfileFormHandler.handleRemoveShippingAddress with exception: {0}",
						trDem);
			} catch (RepositoryException rExc) {
				lRollbackFlag = true;
				vlogError("Repository Exception occured in TRUProfileFormHandler.handleRemoveShippingAddress with exception: {0}",
						rExc);
			} catch (CommerceException cExc) {
				lRollbackFlag = true;
				vlogError("Commerce Exception occured in TRUProfileFormHandler.handleRemoveShippingAddress with exception: {0}",
						cExc);
			} catch (RunProcessException rExc) {
				lRollbackFlag = true;
				vlogError("RunProcessException occured in TRUProfileFormHandler.handleRemoveShippingAddress with exception: {0}",
						rExc);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError(
								"TransactionDemarcationException in finally block of TRUProfileFormHandler.handleRemoveShippingAddress: {0} ",
								tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleRemoveShippingAddress()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for the required tasks to be done before removing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preRemoveShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preRemoveShippingAddress()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preRemoveShippingAddress()");
		}
	}

	/**
	 * This method is for the required tasks to be done after removing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postRemoveShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postRemoveShippingAddress()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postRemoveShippingAddress()");
		}
	}

	/**
	 * This method is invoked when user clicks on set as default for a particular address. The address selected is set as default
	 * in the profile and previous default address if any is no more a default address.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleSetAddressAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleSetAddrAsDefault()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUProfileFormHandler.handleSetAddrAsDefault";
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preSetAddrAsDefault(pRequest, pResponse);
					final String lAddressNickName = getStringValueProperty(TRUConstants.ADDRESS_NICK_NAME);
					vlogDebug("value of nick name selected for setting as default {0} ", lAddressNickName);
					final RepositoryItem newDefaultShippingAddress = profileTools.getProfileAddress(getProfile(), lAddressNickName);
					getProfileTools().updateProperty(getPropertyManager().getShippingAddressPropertyName(),
							newDefaultShippingAddress, getProfile());
					final RepositoryItem prevShippingAddress = (RepositoryItem) getProfile().getPropertyValue(
							getPropertyManager().getPreviousShippingAddressPropertyName());
					if (null == prevShippingAddress) {
						// First time Update current address to prev address
						getProfileTools().updateProperty(getPropertyManager().getPreviousShippingAddressPropertyName(),
								newDefaultShippingAddress, getProfile());
						getProfileTools().updateProperty(getPropertyManager().getPrevShippingAddressNickNamePropertyName(),
								lAddressNickName, getProfile());
					}					
					postSetAddrAsDefault(pRequest, pResponse);
				}
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while setting address as default with exception: {0}", trDem);
			} catch (RepositoryException rExc) {
				lRollbackFlag = true;
				vlogError("Repository Exception occured in TRUProfileFormHandler.handleSetAddressAsDefault with exception: {0}",
						rExc);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError(
								"TransactionDemarcationException occured in TRUProfileFormHandler.handleSetAddressAsDefault with exception: {0}",
								tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUProfileFormHandler.handleSetAddrAsDefault()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for the required tasks to be done before setting the address as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preSetAddrAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preSetAddrAsDefault()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preSetAddrAsDefault()");
		}
	}

	/**
	 * This method is for the required tasks to be done after setting the address as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postSetAddrAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postSetAddrAsDefault()");
		}
		//Start : Code change for setting defaultNickName in container as part of #TUW-75654
		final String lAddressNickName = getStringValueProperty(TRUConstants.ADDRESS_NICK_NAME);
		if(getShippingGroupMapContainer()!=null){
			getShippingGroupMapContainer().setDefaultShippingGroupName(lAddressNickName);
		}		
		//End : Code change for setting defaultNickName in container as part of #TUW-75654
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postSetAddrAsDefault()");
		}
	}

	/**
	 * This method is invoked when user clicks on set as default for a particular credit card. The credit card selected is set as
	 * default in the profile and previous default card if any is set as not a default card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleSetCreditCardAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.handleSetCreditCardAsDefault()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_SET_CREDIT_CARD_AS_DEFAULT))) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preSetCreditCardAsDefault(pRequest, pResponse);
					final String lCreditCardNickName = getStringValueProperty(TRUConstants.CREDIT_CARD_NICK_NAME);
					profileTools.setDefaultCreditCard(getProfile(), lCreditCardNickName);
					// Start :: TUW-43619/46853 defect changes.
					profileTools.updateProperty(TRUConstants.SELECTED_CC_NICKNAME, lCreditCardNickName, getProfile());
					// End :: TUW-43619/46853 defect changes.
					postSetCreditCardAsDefault(pRequest, pResponse);
				}
			} catch (RepositoryException rExc) {
				lRollbackFlag = true;
				vlogError("Repository Exception occured while setting credit card as defaultwith exception: {0}", rExc);
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while setting credit card as default with exception: {0}",
						trDem);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError(
								"TransactionDemarcationException occured while setting credit card as default with exception {0}",
								tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_SET_CREDIT_CARD_AS_DEFAULT);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.handleSetCreditCardAsDefault()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for the required tasks to be done before setting the credit card as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preSetCreditCardAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.preSetCreditCardAsDefault()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.preSetCreditCardAsDefault()");
		}
	}

	/**
	 * This method is for the required tasks to be done after setting the credit card as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postSetCreditCardAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUProfileFormHandler.postSetCreditCardAsDefault()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUProfileFormHandler.postSetCreditCardAsDefault()");
		}
	}

	/**
	 * Gets the repeating request monitor.
	 * 
	 * @return the mRepeatingRequestMonitor
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * Gets the confirm email.
	 * 
	 * @return the mConfirmEmail
	 */
	public String getConfirmEmail() {
		return mConfirmEmail;
	}

	/**
	 * Sets the confirm email.
	 * 
	 * @param pConfirmEmail
	 *            the mConfirmEmail to set
	 */
	public void setConfirmEmail(String pConfirmEmail) {
		this.mConfirmEmail = pConfirmEmail;
	}

	/**
	 * Sets the repeating request monitor.
	 * 
	 * @param pRepeatingRequestMonitor
	 *            the RepeatingRequestMonitor to set
	 */
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		this.mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	/**
	 * Gets the cookie manager.
	 * 
	 * @return the mTRUCookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * Sets the cookie manager.
	 * 
	 * @param pCookieManager
	 *            the mCookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		this.mCookieManager = pCookieManager;
	}

	/**
	 * Gets the profile services.
	 * 
	 * @return the mProfileServices
	 */
	public ProfileServices getProfileServices() {
		return mProfileServices;
	}

	/**
	 * Sets the profile services.
	 * 
	 * @param pProfileServices
	 *            the mProfileServices to set
	 */
	public void setProfileServices(ProfileServices pProfileServices) {
		this.mProfileServices = pProfileServices;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * Gets the reward number.
	 * 
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}

	/**
	 * Sets the reward number.
	 * 
	 * @param pRewardNumber
	 *            the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		this.mRewardNumber = pRewardNumber;
	}

	/**
	 * Gets the reset password error url.
	 * 
	 * @return the mResetPasswordErrorURL
	 */
	public String getResetPasswordErrorURL() {
		return mResetPasswordErrorURL;
	}

	/**
	 * Sets the reset password error url.
	 * 
	 * @param pResetPasswordErrorURL
	 *            the mResetPasswordErrorURL to set
	 */
	public void setResetPasswordErrorURL(String pResetPasswordErrorURL) {
		this.mResetPasswordErrorURL = pResetPasswordErrorURL;
	}

	/**
	 * Checks if is email update.
	 * 
	 * @return the mEmailUpdate
	 */
	public boolean isEmailUpdate() {
		return mEmailUpdate;
	}

	/**
	 * Sets the email update.
	 * 
	 * @param pEmailUpdate
	 *            the mEmailUpdate to set
	 */
	public void setEmailUpdate(boolean pEmailUpdate) {
		this.mEmailUpdate = pEmailUpdate;
	}

	/**
	 * Gets the reset password success url.
	 * 
	 * @return the mResetPasswordSuccessURL
	 */
	public String getResetPasswordSuccessURL() {
		return mResetPasswordSuccessURL;
	}

	/**
	 * Sets the reset password success url.
	 * 
	 * @param pResetPasswordSuccessURL
	 *            the mResetPasswordSuccessURL to set
	 */
	public void setResetPasswordSuccessURL(String pResetPasswordSuccessURL) {
		this.mResetPasswordSuccessURL = pResetPasswordSuccessURL;
	}

	/**
	 * Sets property AddressValue map.
	 * 
	 * @param pAddressValue
	 *            AddressValue
	 **/
	public void setAddressValue(Map<String, String> pAddressValue) {
		mAddressValue = pAddressValue;
	}

	/**
	 * Gets the address value.
	 * 
	 * @return This is a map that stores the value of address properties.
	 */
	public Map<String, String> getAddressValue() {
		return mAddressValue;
	}

	/**
	 * Gets the selected card billing address.
	 * 
	 * @return the selectedCardBillingAddress
	 */
	public String getSelectedCardBillingAddress() {
		return mSelectedCardBillingAddress;
	}

	/**
	 * Sets the selected card billing address.
	 * 
	 * @param pSelectedCardBillingAddress
	 *            the mSelectedCardBillingAddress to set
	 */
	public void setSelectedCardBillingAddress(String pSelectedCardBillingAddress) {
		mSelectedCardBillingAddress = pSelectedCardBillingAddress;
	}

	/**
	 * Gets the credit card info map.
	 * 
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * Sets the credit card info map.
	 * 
	 * @param pCreditCardInfoMap
	 *            the mCreditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * Gets the delete card nick name.
	 * 
	 * @return the deleteCardNickName
	 */
	public String getDeleteCardNickName() {
		return mDeleteCardNickName;
	}

	/**
	 * Sets the delete card nick name.
	 * 
	 * @param pDeleteCardNickName
	 *            the mDeleteCardNickName to set
	 */
	public void setDeleteCardNickName(String pDeleteCardNickName) {
		mDeleteCardNickName = pDeleteCardNickName;
	}

	/**
	 * Gets the profile manager.
	 * 
	 * @return TRUProfileManager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}

	/**
	 * Sets the profile manager.
	 * 
	 * @param pProfileManager
	 *            set property mProfileManager, used for business logic validation
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}

	/**
	 * Gets the validator.
	 * 
	 * @return IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * Sets the validator.
	 * 
	 * @param pValidator
	 *            the mValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		mValidator = pValidator;
	}

	/**
	 * Gets the error handler.
	 * 
	 * @return the errorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 * 
	 * @param pErrorHandler
	 *            the errorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin() {
		String login = (String) (getValue()).get(getProfileTools().getPropertyManager().getLoginPropertyName());
		setValueProperty(getProfileTools().getPropertyManager().getLoginPropertyName(), login.toLowerCase().trim());
		return (String) (getValue()).get(getProfileTools().getPropertyManager().getLoginPropertyName());
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		String password = (String) (getValue()).get(getProfileTools().getPropertyManager().getPasswordPropertyName());
		setValueProperty(getProfileTools().getPropertyManager().getPasswordPropertyName(), password.trim());
		return (String) (getValue()).get(getProfileTools().getPropertyManager().getPasswordPropertyName());
	}

	/**
	 * Gets the confirm password.
	 * 
	 * @return confirm password property
	 */
	public String getConfirmPassword() {
		String confirmpassword = (String) (getValue()).get(((TRUPropertyManager) getProfileTools().getPropertyManager()).getConfirmPasswordPropertyName());
		setValueProperty(((TRUPropertyManager) getProfileTools().getPropertyManager()).getConfirmPasswordPropertyName(), confirmpassword.trim());
		return (String) (getValue()).get(((TRUPropertyManager) getProfileTools().getPropertyManager()).getConfirmPasswordPropertyName());
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		String lEmail = (String) (getValue()).get((getProfileTools().getPropertyManager()).getEmailAddressPropertyName());
		if (lEmail != null) {
			lEmail = lEmail.trim();
		}
		return lEmail;
	}

	/**
	 * Gets the address nick name.
	 * 
	 * @return the nickName
	 */
	public String getAddressNickName() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getNicknamePropertyName());
	}

	/**
	 * Gets the address first name.
	 * 
	 * @return the Address - FirstName
	 */
	public String getAddressFirstName() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getFirstNamePropertyName());
	}

	/**
	 * Gets the address last name.
	 * 
	 * @return the Address - LastName
	 */
	public String getAddressLastName() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getLastNamePropertyName());
	}

	/**
	 * Gets the address country.
	 * 
	 * @return the country
	 */
	public String getAddressCountry() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressCountryPropertyName());
	}

	/**
	 * Gets the address address one.
	 * 
	 * @return the address1
	 */
	public String getAddressAddressOne() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressLineOnePropertyName());
	}

	/**
	 * Gets the address city.
	 * 
	 * @return the city
	 */
	public String getAddressCity() {
		return (getAddressValue())
				.get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getAddressCityPropertyName());
	}

	/**
	 * Gets the address state.
	 * 
	 * @return the state
	 */
	public String getAddressState() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressStatePropertyName());
	}

	/**
	 * Gets the address postal code.
	 * 
	 * @return the PostalCodeCode
	 */
	public String getAddressPostalCode() {
		return (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressPostalCodePropertyName());
	}

	/**
	 * Gets the address phone number.
	 * 
	 * @return the PhoneNumber
	 */
	public String getAddressPhoneNumber() {
		String addressPhoneNumber = (getAddressValue()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressPhoneNumberPropertyName());
		if (!StringUtils.isBlank(addressPhoneNumber)) {
			addressPhoneNumber = addressPhoneNumber.replaceAll(TRUConstants.HYPHEN, TRUConstants.EMPTY);
			addressPhoneNumber = addressPhoneNumber.trim();
			getAddressValue().put(TRUConstants.PHONE_NUMBER, addressPhoneNumber);
		}
		return addressPhoneNumber;
	}
	
	/**
	 * This method is to check whether user is already logged in or not. 
	 * @return boolean success/failure
	 */
	private boolean isUserLoggedIn()  {
		if(isLoggingDebug()){
			logDebug("Start: TRUProfileFormHandler.isUserLoggedIn()");
		}
		boolean isUserLoggedIn = false;
		Integer securityStatus;	
			try {
				securityStatus = getProfile().getProfileTools().getSecurityStatus(getProfile());
                if (securityStatus != null && securityStatus.intValue() >= getProfile().getProfileTools().getPropertyManager().getSecurityStatusLogin()) {
                	isUserLoggedIn=true;   
				}
			} catch (PropertyNotFoundException propEx) {
				vlogError("PropertyNotFoundException occured while updating credit card with exception : {0}", propEx);
			}
		return isUserLoggedIn;	
	}

	/**
	 * Gets the name on card.
	 * 
	 * @return the nameOnCard
	 */
	public String getNameOnCard() {
		return (getCreditCardInfoMap()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getNameOnCardPropertyName());
	}

	/**
	 * Gets the credit card number.
	 * 
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return (getCreditCardInfoMap()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardNumberPropertyName());
	}

	/**
	 * Gets the expiration month.
	 * 
	 * @return the expirationMonth
	 */
	public String getExpirationMonth() {
		return (getCreditCardInfoMap()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardExpirationMonthPropertyName());
	}

	/**
	 * Gets the expiration year.
	 * 
	 * @return the expirationYear
	 */
	public String getExpirationYear() {
		return (getCreditCardInfoMap()).get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardExpirationYearPropertyName());
	}

	/**
	 * Gets the oldpassword.
	 * 
	 * @return the old password
	 */
	public String getOldpassword() {
		return (String) (getValue()).get(((TRUPropertyManager) getProfileTools().getPropertyManager())
				.getOldpasswordPropertyName());
	}

	/**
	 * Gets the confirmpassword.
	 * 
	 * @return confirm password property
	 */
	public String getConfirmpassword() {
		return (String) (getValue()).get(((TRUPropertyManager) getProfileTools().getPropertyManager())
				.getConfirmpasswordPropertyName());
	}

	/**
	 * Gets the change email source.
	 * 
	 * @return the changeEmailSource
	 */
	public TRUEpslonChangeEmailSource getChangeEmailSource() {
		return mChangeEmailSource;
	}

	/**
	 * Sets the change email source.
	 * 
	 * @param pChangeEmailSource
	 *            the changeEmailSource to set
	 */
	public void setChangeEmailSource(TRUEpslonChangeEmailSource pChangeEmailSource) {
		mChangeEmailSource = pChangeEmailSource;
	}

	/**
	 * Gets the change password source.
	 * 
	 * @return the changePasswordSource
	 */
	public TRUEpslonChangePasswordSource getChangePasswordSource() {
		return mChangePasswordSource;
	}

	/**
	 * Sets the change password source.
	 * 
	 * @param pChangePasswordSource
	 *            the changePasswordSource to set
	 */
	public void setChangePasswordSource(TRUEpslonChangePasswordSource pChangePasswordSource) {
		mChangePasswordSource = pChangePasswordSource;
	}

	/**
	 * Gets the password reset success source.
	 * 
	 * @return the passwordResetSuccessSource
	 */
	public TRUEpslonPasswordResetSuccessSource getPasswordResetSuccessSource() {
		return mPasswordResetSuccessSource;
	}

	/**
	 * Sets the password reset success source.
	 * 
	 * @param pPasswordResetSuccessSource
	 *            the passwordResetSuccessSource to set
	 */
	public void setPasswordResetSuccessSource(TRUEpslonPasswordResetSuccessSource pPasswordResetSuccessSource) {
		mPasswordResetSuccessSource = pPasswordResetSuccessSource;
	}

	/**
	 * Gets the TRU configuration.
	 * 
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 * 
	 * @param pTRUConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * Gets the edits the card nick name.
	 * 
	 * @return the editCardNickName
	 */
	public String getEditCardNickName() {
		return mEditCardNickName;
	}

	/**
	 * Sets the edits the card nick name.
	 * 
	 * @param pEditCardNickName
	 *            the editCardNickName to set
	 */
	public void setEditCardNickName(String pEditCardNickName) {
		mEditCardNickName = pEditCardNickName;
	}

	/**
	 * Gets the billing address nick name.
	 * 
	 * @return the billingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * Sets the billing address nick name.
	 * 
	 * @param pBillingAddressNickName
	 *            the billingAddressNickName to set
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}

	/**
	 * Gets the common success url.
	 * 
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 * 
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 * 
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * Sets the common error url.
	 * 
	 * @param pCommonErrorURL
	 *            the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * Gets the credit card cvv.
	 * 
	 * @return the creditCardCVV
	 */
	public String getCreditCardCVV() {
		return mCreditCardCVV;
	}

	/**
	 * Sets the credit card cvv.
	 * 
	 * @param pCreditCardCVV
	 *            the creditCardCVV to set
	 */
	public void setCreditCardCVV(String pCreditCardCVV) {
		mCreditCardCVV = pCreditCardCVV;
	}

	/**
	 * Gets the email password validator.
	 * 
	 * @return the emailPasswordValidator
	 */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}

	/**
	 * Sets the email password validator.
	 * 
	 * @param pEmailPasswordValidator
	 *            the emailPasswordValidator to set
	 */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}

	/**
	 * Checks if is ajax.
	 * 
	 * @return the mAjax
	 */
	public boolean isAjax() {
		return mAjax;
	}

	/**
	 * Sets the ajax.
	 * 
	 * @param pAjax
	 *            the ajax to set
	 */
	public void setAjax(boolean pAjax) {
		this.mAjax = pAjax;
	}

	/**
	 * Gets the reward number valid.
	 * 
	 * @return the mRewardNumberValid
	 */
	public String getRewardNumberValid() {
		return mRewardNumberValid;
	}

	/**
	 * Sets the reward number valid.
	 * 
	 * @param pRewardNumberValid
	 *            the rewardNumberValid to set
	 */
	public void setRewardNumberValid(String pRewardNumberValid) {
		this.mRewardNumberValid = pRewardNumberValid;
	}

	/**
	 * Gets the address doctor process status.
	 * 
	 * @return the addressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * Sets the address doctor process status.
	 * 
	 * @param pAddressDoctorProcessStatus
	 *            the addressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * Gets the address doctor response vo.
	 * 
	 * @return the addressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * Sets the address doctor response vo.
	 * 
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * Checks if is last option selected.
	 * 
	 * @return the lastOptionSelected
	 */
	public boolean isLastOptionSelected() {
		return mLastOptionSelected;
	}

	/**
	 * Sets the last option selected.
	 * 
	 * @param pLastOptionSelected
	 *            the lastOptionSelected to set
	 */
	public void setLastOptionSelected(boolean pLastOptionSelected) {
		mLastOptionSelected = pLastOptionSelected;
	}

	/**
	 * Gets the shipping helper.
	 * 
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 * 
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Gets the shipping group map container.
	 * 
	 * @return the shippingGroupMapContainer
	 */
	public ShippingGroupMapContainer getShippingGroupMapContainer() {
		return mShippingGroupMapContainer;
	}

	/**
	 * Sets the shipping group map container.
	 * 
	 * @param pShippingGroupMapContainer
	 *            the shippingGroupMapContainer to set
	 */
	public void setShippingGroupMapContainer(ShippingGroupMapContainer pShippingGroupMapContainer) {
		mShippingGroupMapContainer = pShippingGroupMapContainer;
	}

	/**
	 * @return the paymentGroupMapContainer
	 */
	public PaymentGroupMapContainer getPaymentGroupMapContainer() {
		return mPaymentGroupMapContainer;
	}

	/**
	 * @param pPaymentGroupMapContainer the paymentGroupMapContainer to set
	 */
	public void setPaymentGroupMapContainer(
			PaymentGroupMapContainer pPaymentGroupMapContainer) {
		mPaymentGroupMapContainer = pPaymentGroupMapContainer;
	}

	/**
	 * Checks if is rest service.
	 * 
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 * 
	 * @param pRestService
	 *            the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}

	/**
	 * This method returns the customerMasterImportSource value.
	 * 
	 * @return the customerMasterImportSource
	 */
	public TRUCustomerMasterImportSource getCustomerMasterImportSource() {
		return mCustomerMasterImportSource;
	}

	/**
	 * This method sets the customerMasterImportSource with parameter value pCustomerMasterImportSource.
	 * 
	 * @param pCustomerMasterImportSource
	 *            the customerMasterImportSource to set
	 */
	public void setCustomerMasterImportSource(TRUCustomerMasterImportSource pCustomerMasterImportSource) {
		mCustomerMasterImportSource = pCustomerMasterImportSource;
	}

	/**
	 * Gets the user old email address.
	 * 
	 * @return the userOldEmailAddress
	 */
	public String getUserOldEmailAddress() {
		return mUserOldEmailAddress;
	}

	/**
	 * Sets the user old email address.
	 * 
	 * @param pUserOldEmailAddress
	 *            the user old email address
	 */
	public void setUserOldEmailAddress(String pUserOldEmailAddress) {
		mUserOldEmailAddress = pUserOldEmailAddress;
	}

	/**
	 * Gets the integration status util.
	 * 
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * Sets the integration status util.
	 * 
	 * @param pIntegrationStatusUtil
	 *            the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * Gets the password reset url.
	 * 
	 * @return the mPasswordResetURL
	 */
	public String getPasswordResetURL() {
		return mPasswordResetURL;
	}

	/**
	 * Sets the password reset url.
	 * 
	 * @param pPasswordResetURL
	 *            the PasswordResetURL to set
	 */
	public void setPasswordResetURL(String pPasswordResetURL) {
		this.mPasswordResetURL = pPasswordResetURL;
	}

	/**
	 * @return the errorKeysList
	 */
	public List<String> getErrorKeysList() {
		return mErrorKeysList;
	}

	/**
	 * @param pErrorKeysList the errorKeysList to set
	 */
	public void setErrorKeysList(List<String> pErrorKeysList) {
		mErrorKeysList = pErrorKeysList;
	}

	/**
	 * This method gets orderId value
	 *
	 * @return the orderId value
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * This method sets the orderId with pOrderId value
	 *
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}
	
	/**
	 * This method gets mSetaLogger value
	 *
	 * @return the mSetaLogger value
	 */
	public TRUSETALogger getSetaLogger() {
		return mSetaLogger;
	}
	
	/**
	 * This method sets the mSetaLogger with pSetaLogger value
	 *
	 * @param pSetaLogger the pSetaLogger to set
	 */
	public void setSetaLogger(TRUSETALogger pSetaLogger) {
		mSetaLogger = pSetaLogger;
	}
	/**
	 * Gets the TRU Store Util.
	 * 
	 * @return the mStoreUtils
	 */
	public TRUStoreUtils getStoreUtils() {
		return mStoreUtils;
	}

	/**
	 * Sets the TRU Store Util.
	 * 
	 * @param pStoreUtils
	 *            the mStoreUtils to set
	 */
	public void setStoreUtils(TRUStoreUtils pStoreUtils) {
		mStoreUtils = pStoreUtils;
	}
	
	/**
	 * Method is overridden to revoke the old sucn codes from profile.
	 * 
	 * @param pLogin -  String Login
	 * @param pPassword - String Password
	 * @param pRequest - DynamoHttpServletRequest Object
	 * @param pResponse - DynamoHttpServletResponse Object
	 * 
	 * @return RepositoryItem - User
	 * 
	 * @throws RepositoryException - RepositoryException if any
	 * @throws ServletException - ServletException if any
	 * @throws IOException - IOException if any
	 */
	@Override
	protected RepositoryItem findUser(String pLogin, String pPassword,
			Repository pProfileRepository, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RepositoryException,
			ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProfileFormHandler  method: findUser]");
			vlogDebug("pLogin : {0}", pLogin);
		}
		RepositoryItem profileItem = null;
		// Calling Super Method
		profileItem = super.findUser(pLogin, pPassword, pProfileRepository, pRequest,
				pResponse);
		// Calling method to revoke the old sucn coupons from Profile.
		((TRUPromotionTools)getPromotionTools()).revokeSingleUseCoupons(profileItem);
		if (isLoggingDebug()) {
			vlogDebug("profileItem : {0}", profileItem);
			logDebug("Exit from [Class: TRUProfileFormHandler  method: findUser]");
		}
		return profileItem ;
	}
	
	/**
	 * Method to logout the user from cross domain Calls.
	 * 
	 * @return Flag - logged out or not
	 * 
	 * @throws ServletException - ServletException if any
	 * @throws IOException - IOException if any
	 */
	public boolean performLogout() throws 
			ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("Start of performLogout method");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_PERFORM_LOGOUT))) {
		    TransactionManager tm = getTransactionManager();
		    TransactionDemarcation td = getTransactionDemarcation();
		    try {
		      if (tm != null){
		    	  td.begin(tm, TransactionDemarcation.REQUIRED);
		      } 
		      DynamoHttpServletRequest  pRequest=ServletUtil.getCurrentRequest();
		      DynamoHttpServletResponse  pResponse=ServletUtil.getCurrentResponse();
		      RepositoryItem preLogoutDataSource = getCurrentDataSource();
		      preLogoutUser(pRequest, pResponse);
		      checkLogoutError(pRequest);
		      if (getFormError()) {
		         return false;
		      }
		      sendProfileSwapEvent(TRUConstants.INTEGER_NUMBER_ONE, preLogoutDataSource, getCurrentDataSource());
		      checkLogoutError(pRequest);
		      if (getFormError()) {
		         return false;
		      }
		      postLogoutUser(pRequest, pResponse);
		    } catch (TransactionDemarcationException e)    {
		    	vlogError("Exception occured while perform Logout with exception : {0}", e);
		    	
		    }
		    finally
		    {
		      try
		      {
		        if (tm != null){
		        	td.end(); 
		        } 
		        if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_CREATE);
				}
		      }
		      catch (TransactionDemarcationException e)  {  
		    	    vlogError("Exception occured while performLogout with exception : {0}", e);
		      }
		    }
		    
		}
		if (isLoggingDebug()) {
			vlogDebug("End of performLogout method");
		}
		return true;
	}
	
	/**
	 * Method is overridden to revoke the old sucn codes from profile.
	 * 
	 * @param pRequest - DynamoHttpServletRequest Object
	 * 
	 */
	private void checkLogoutError(DynamoHttpServletRequest pRequest){
		if (getProfile() == null) {
	          addFormException(new DropletException(formatUserMessage("missingProfile", pRequest)));
	        }
	      if (getProfileTools() == null) {
	          addFormException(new DropletException(formatUserMessage("missingProfileTools", pRequest)));
	        }
		
	}
	
}
