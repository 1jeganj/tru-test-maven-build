/*
 * 
 */
package com.tru.commerce;

import atg.nucleus.GenericService;


/**
 * The Class TRUCommercePropertyManager.
 * @author PA.
 * @version 1.0.
 */
public class TRUCommercePropertyManager extends GenericService {

	/** Holds enableRadialPropertyName site configuration. */
	
	private String mEnableRadialPropertyName;

	/** The m customer purchase limit. */
	private String mCustomerPurchaseLimit;

	/** The m display name. */
	private String mDisplayName;

	/** The m can be gift wrapped. */
	private String mCanBeGiftWrapped;

	/** The m color upc level. */
	private String mColorUpcLevel;

	/** The m juvenile product size. */
	private String mJuvenileProductSize;

	/** The m wishlist. */
	private String mWishlist;

	/** The m registry. */
	private String mRegistry;

	/** The m location id for ship to home. */
	private String mLocationIdForShipToHome;

	/** The Channel availability. */
	private String mChannelAvailability;

	/** The Atc quantity. */
	private String mAtcQuantity;

	/** The m ship window max. */
	private String mShipWindowMax;

	/** The m ship window min. */
	private String mShipWindowMin;

	/** Holds item descriptor name for commerceItemMetaInfo. */
	private String mCommerceItemMetaInfoDescriptorName;

	/** Holds id property name for commerceItemMetaInfo. */
	private String mCommerceItemMetaInfoIdPropertyName;

	/** Holds catalog ref id property name for commerceItemMetaInfo. */
	private String mCatalogRefIdPropertyName;

	/** Holds product id property name for commerceItemMetaInfo. */
	private String mProductIdPropertyName;

	/** Holds quantity property name for commerceItemMetaInfo. */
	private String mQuantityPropertyName;

	/** Holds price property name for commerceItemMetaInfo. */
	private String mPricePropertyName;

	/** Holds isAdjusted property name for commerceItemMetaInfo. */
	private String mIsAdjustedPropertyName;

	/** Holds TotalSavedAmount property name for commerceItemMetaInfo. */
	private String mTotalSavedAmount;

	/** Holds TotalSavedPercentage property name for commerceItemMetaInfo. */
	private String mTotalSavedPercentage;

	/** Holds StreetDate property name for SKU item. */
	private String mStreetDate;

	// freightClass item properties
	/** The Freight class property name. */
	private String mFreightClassPropertyName;

	/** The Applicable shipping methods property name. */
	private String mApplicableShippingMethodsPropertyName;

	// shipMethod item's properties
	/** Holds the id property value. */
	private String mIdPropertyName;

	/** Holds shipMethodCode Property Name. */
	private String mShipMethodCodePropertyName;

	/** Holds shipMethodName PropertyName. */
	private String mShipMethodNamePropertyName;

	/** Hold mFullfillmentMsg PropertyName. */
	private String mFullfillmentMsgPropertyName;

	/** Holds shipRegion PropertyName. */
	private String mShipRegionPropertyName;

	/** Hold the regionCode. */
	private String mRegionCodePropertyName;

	/** Hold the ship method's details. */
	private String mDetailsPropertyName;

	/** The Ship rate property name. */
	private String mShipRatePropertyName;

	/** The mhip method property name. */
	private String mShipMethodPropertyName;

	/** The Lower order bound property name. */
	private String mLowerOrderBoundPropertyName;

	/** The Upper order bound property name. */
	private String mUpperOrderBoundPropertyName;

	/** The Ship price property name. */
	private String mShipPricePropertyName;

	/** Holds mNonQualifiedStateCodes value. */
	private String mNonQualifiedStateCodesPropertyName;

	/** Holds errorMessage. */
	private String mErrorMessagePropertyName;

	/** Hold product image. */
	private String mPrimaryImage;

	/** Hold preorder level. */
	private String mPreorderLevel;
	/** Hold m super display flag. */
	private String mSuperDisplayFlag;

	/** Hold is deleted. */
	private String mIsDeleted;

	/** Holds price property name for UnitPrice. */
	private String mUnitPricePropertyName;

	/** Holds price property name for GiftWrapItem. */
	private String mGiftWrapItemPropertyName;

	/** Holds price property name for GiftWrapItem. */
	private String mCommerceItemPropertyName;

	/** Holds price property name for mShipItemRelIdPropertyName. */
	private String mShipItemRelIdPropertyName;

	/** Holds web display flag. */
	private String mWebDisplayFlag;

	/** Holds MetaInfoId property name for mMetaInfoIdPropertyName. */
	private String mMetaInfoIdPropertyName;
	
	/** Holds promotion id property name for mPromotionIdPropertyName. */
	private String mPromotionIdPropertyName;
	
	/** The style dimensions. */
	private String mStyleDimensionsPropertyName;
	
		/** Holds out of stock item. */
	private String mOutOfStockItemDescriptorName;
	
	/** Holdslist price property name. */
	private String mListPricePropertyName;
	
	/** Holds sale price property name. */
	private String mSalePricePropertyName;
	
	/** Holds location id property name. */
	private String mLocationIdPropertyName;
	
	/** Holds property for  un cartable. */
	private String mUnCartable;
	
	/** Holds property for channel name. */
	private String mChannelName;
	
	/** Holds property for channel id. */
	private String mChannelId;
	
	/** Holds property for channel user name. */
	private String mChannelUserName;
	
	/** Holds property for channel co user name. */
	private String mChannelCoUserName;
	
	/** Holds property for  channel type. */
	private String mChannelType;
	
	/** The Source channel. */
	private String mSourceChannel;
	
	/** Holds property for item in store pick up. */
	private String mItemInStorePickUp;
	
	/** Holds property for ship to store eligible. */
	private String mShipToStoreEligible;

	/** The Display name property name. */
	private String mDisplayNamePropertyName;
	
	/** The Image property name. */
	private String mImagePropertyName;
	
	/** The Path property name. */
	private String mPathPropertyName;
	
	/**  Holds bpp eligible property name. */
	private String mBppEligiblePropertyName;
	
	/**  Holds bpp name property name. */
	private String mBppNamePropertyName;
	
	/** The m bpp cart description. */
	private String mBppCartDescriptionPropertyName;
	
	/**  Holds bpp item id property name. */
	private String mBppItemIdPropertyName;
	
	/**  Holds bpp price property name. */
	private String mBppPricePropertyName;
	
	/**  Holds bpp retail property name. */
	private String mBppRetailPropertyName;
	
	/**  Holds bpp sku id property name. */
	private String mBppSkuIdPropertyName;
	
	/**  Holds property giftWrapCommItemId. */
	private String mGiftWrapCommItemId;
	
	/**  Holds property giftWrapQuantity. */
	private String mGiftWrapQuantity;
	
	/**  Holds property orderId. */
	private String mOrderId;
	
	/**  Holds property shippingGroupId. */
	private String mShippingGroupId;
	
	/**  Holds property giftWrapCatalogRefId. */
	private String mGiftWrapCatalogRefId;
	
	/** The Long description. */
	private String mLongDescription;
	
	/**  Holds property giftWrapProductId. */
	private String mGiftWrapProductId;
	
	/**  Holds property giftItem. */
	private String mGiftItem;
	
	/**  Holds property StoreName. */
	private String mStoreName;
	
	/**  Holds property CityName. */
	private String mCityName;
	
	/**  Holds property StoreAddress1. */
	private String mStoreAddress1;
	
	/**  Holds property StoreStateAddress. */
	private String mStoreStateAddress;
	
	/**  Holds property StorePostalCode. */
	private String mStorePostalCode;
	
	/**  Holds bpp term property name. */
	private String mBppTermPropertyName;
	
	/** The m noti fy me. */
	private String mNotiFyMe;
	
	/**  Holds user item descriptor name. */
	private String mUserItemDescriptorName;
	
	/**  Holds registration Date property name. */
	private String mRegistrationDatePropertyName;
	
	/**  Holds description property name. */
	private String mDescriptionPropertyName;
	
	/**  Holds longDescription property name. */
	private String mLongDescriptionPropertyName;
	
	/**  Holds parentCategories property name. */
	private String mParentCategoriesPropertyName;
	
	/**  Holds brand property name. */
	private String mBrandPropertyName;
	
	/**  Holds firstName property name. */
	private String mFirstNamePropertyName;
	
	/**  Holds payeezy enabled property name. */
	private String mEnablePayeezyPropertyName;
	/**  Holds Inventory enabled property name. */
	private String mEnableInventoryPropertyName;
	
	/** The Item descriptor audit message. */
	private String mItemDescriptorAuditMessage;
	
	/** The Message type. */
	private String mMessageType;
	
	/** The Message data. */
	private String mMessageData;
	
	/** The Audit date. */
	private String mAuditDate;
	
	/** Holds property  site id property name. */
	private String mSiteIdPropertyName;
	
	/** Holds property type property name. */
	private String  mTypePropertyName;
	
	/** property hold vendorFunded. */
	private String mVendorFundedPropertyName;
	
	/** property hold taxCodePropertyName. */
	private String mTaxCodePropertyName;
	
	/** property hold locationItemType. */
	private String mLocationItemType;
	
	/** property hold cityPropertyName. */
	private String mCityPropertyName;
	
	/** property hold stateAddressPropertyName. */
	private String mStateAddressPropertyName;
	
	/** property hold postalCodePropertyName. */
	private String mPostalCodePropertyName;
	
	/** property hold mUserPropertyName. */
	private String mUserPropertyName;
	
	/** property hold siteStatusPropertyName. */
	private String mSiteStatusPropertyName;
	
	/** property hold mColorNamePropertyName. */
	private String mColorNamePropertyName;
	
	/** property hold mSizeDescriptionPropertyName. */
	private String mSizeDescriptionPropertyName;
	
	/** The m store address2. */
	private String mStoreAddress2;
	
	/** The m store country. */
	private String mStoreCountry;
	
	/** The m store phone number. */
	private String mStorePhoneNumber;
	
	/** The m meta info. */
	private String mMetaInfo;
	
	/** The Out of stock items property name. */
	private String mOutOfStockItemsPropertyName;
	/** The Eligible freight classes property name. */
	private String mEligibleFreightClassesPropertyName;
	
	/** The Eligible ship regions property name. */
	private String mEligibleShipRegionsPropertyName;
	
	/** The Ship region name property name. */
	private String mShipRegionNamePropertyName;
	
	/** The Order cut off price property name. */
	private String mOrderCutOffPricePropertyName;
	
	/** The Delivery time min days property name. */
	private String mDeliveryTimeMinDaysPropertyName;
	
	/** The Delivery time max days property name. */
	private String mDeliveryTimeMaxDaysPropertyName;
	
	/** The time in transit min days property name. */
	private String mTimeInTransitMinDaysPropertyName;
	
	/** The time in transit max days property name. */
	private String mTimeInTransitMaxDaysPropertyName;
	
	/** The Ship min price property name. */
	private String mShipMinPricePropertyName;
	
	/** The Ship max price property name. */
	private String mShipMaxPricePropertyName;
	
	/** The Payment groups property name. */
	private String mPaymentGroupsPropertyName;
	/** Holds quantity property name for geoCodePropertyName. */
	private String mGeoCodePropertyName;
	/** Holds quantity property name for enterpriseZonePropertyName. */
	private String mEnterpriseZonePropertyName;
	
	/** Holds the seoTitleText Property Name. */
	private String mSeoTitleTextPropertyName;
	
	/** Holds the seoMetaDesc Property Name. */
	private String mSeoMetaDescPropertyName;
	
	/** Holds the seoMetaKeyword Property Name. */
	private String mSeoMetaKeywordPropertyName;
	
	/** Holds the seoAltDesc Property Name. */
	private String mSeoAltDescPropertyName;
	
	/** Holds the articleBodyPropertyName. */
	private String mArticleBodyPropertyName;
	
	/** Holds the article item. */
	private String mArticleItemDescriptorName;
	
	/** Holds the article name Property Name. */
	private String mArticleName;
	/** Holds the Parent product  Property Name. */
	private String mParentProducts;
	
	/** The Ewaste state property name. */
	private String mEwasteStatePropertyName;
	
	/** The  promotion details property name. */
	private String mPromotionDetailsPropertyName;
	
	
	/** The m restricted zip codes property name. */
	private String mRestrictedZipCodesPropertyName;
	
	/**Property name to hold the SkuItem */
	private String mSkuItem;
	
	/** The Back order status. */
	private String mBackOrderStatus;
	
	/** The m all credit cards prop name. */
	private String mAllCreditCardsPropName;
	
	/** The m selected CC nick name prop name. */
	private String mSelectedCCNickNamePropName;
	
	/** The m credit card number prop name. */
	private String mCreditCardNumberPropName;
	
	/** The m credit card type prop name. */
	private String mCreditCardTypePropName;
	
	/** The m name on card prop name. */
	private String mNameOnCardPropName;
	
	/** The m expiration year prop name. */
	private String mExpirationYearPropName;
	
	/** The m expiration month prop name. */
	private String mExpirationMonthPropName;
	
	/** The m expiration day of month prop name. */
	private String mExpirationDayOfMonthPropName;
	
	/** The m billing address prop name. */
	private String mBillingAddressPropName;
	
	/** The m default credit card prop name. */
	private String mDefaultCreditCardPropName;
	
	/** The Enable coherence override property name. */
	private String mCoherenceOverridePropertyName;
	
	/** The m qualifier column name. */
	private String mQualifierColumnName;
	
	/** The m no of items column name. */
	private String mNoOfItemsColumnName;
	
	/** The m advanced item template property name. */
	private String mAdvancedItemTemplatePropertyName;
	
	/** The m advanced mBppSKUPropertyName. */
	private String mBppSKUPropertyName; 
	
	/** The m advanced mSubTypePropertyName. */
	private String mSubTypePropertyName; 
	
	/**
	 * @return the bppSKUPropertyName
	 */
	public String getBppSKUPropertyName() {
		return mBppSKUPropertyName;
	}

	/**
	 * @param pBppSKUPropertyName the bppSKUPropertyName to set
	 */
	public void setBppSKUPropertyName(String pBppSKUPropertyName) {
		mBppSKUPropertyName = pBppSKUPropertyName;
	}

	/**
	 * @return the subTypePropertyName
	 */
	public String getSubTypePropertyName() {
		return mSubTypePropertyName;
	}

	/**
	 * @param pSubTypePropertyName the subTypePropertyName to set
	 */
	public void setSubTypePropertyName(String pSubTypePropertyName) {
		mSubTypePropertyName = pSubTypePropertyName;
	}

	private String mEvaluationLimitValueColumnName;
	
	/**
	 * Gets the coherence override property name.
	 *
	 * @return the coherence override property name
	 */
	public String getCoherenceOverridePropertyName() {
		return mCoherenceOverridePropertyName;
	}

	/**
	 * Sets the coherence override property name.
	 *
	 * @param pCoherenceOverridePropertyName the new coherence override property name
	 */
	public void setCoherenceOverridePropertyName(
			String pCoherenceOverridePropertyName) {
		mCoherenceOverridePropertyName = pCoherenceOverridePropertyName;
	}

	/**
	 * @return the mExpirationDayOfMonthPropName
	 */
	public String getExpirationDayOfMonthPropName() {
		return mExpirationDayOfMonthPropName;
	}

	/**
	 * @param pMExpirationDayOfMonthPropName the mExpirationDayOfMonthPropName to set
	 */
	public void setExpirationDayOfMonthPropName(
			String pMExpirationDayOfMonthPropName) {
		mExpirationDayOfMonthPropName = pMExpirationDayOfMonthPropName;
	}

	/**
	 * @return the mBillingAddressPropName
	 */
	public String getBillingAddressPropName() {
		return mBillingAddressPropName;
	}

	/**
	 * @param pMBillingAddressPropName the mBillingAddressPropName to set
	 */
	public void setBillingAddressPropName(String pMBillingAddressPropName) {
		mBillingAddressPropName = pMBillingAddressPropName;
	}

	/**
	 * @return the mDefaultCreditCardPropName
	 */
	public String getDefaultCreditCardPropName() {
		return mDefaultCreditCardPropName;
	}

	/**
	 * @param pMDefaultCreditCardPropName the mDefaultCreditCardPropName to set
	 */
	public void setDefaultCreditCardPropName(String pMDefaultCreditCardPropName) {
		mDefaultCreditCardPropName = pMDefaultCreditCardPropName;
	}

	/**
	 * Gets the back order status.
	 *
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 *
	 * @param pBackOrderStatus the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		mBackOrderStatus = pBackOrderStatus;
	}

	/**
	 * @return the skuItem
	 */
	public String getSkuItem() {
		return mSkuItem;
	}

	/**
	 * @param pSkuItem the skuItem to set
	 */
	public void setSkuItem(String pSkuItem) {
		mSkuItem = pSkuItem;
	}

	
	/**
	 * Gets the restricted zip codes property name.
	 *
	 * @return the restricted zip codes property name
	 */
	public String getRestrictedZipCodesPropertyName() {
		return mRestrictedZipCodesPropertyName;
	}

	/**
	 * Sets the restricted zip codes property name.
	 *
	 * @param pRestrictedZipCodesPropertyName the new restricted zip codes property name
	 */
	public void setRestrictedZipCodesPropertyName(
			String pRestrictedZipCodesPropertyName) {
		this.mRestrictedZipCodesPropertyName = pRestrictedZipCodesPropertyName;
	}

	/**
	 * Gets the ewaste state property name.
	 *
	 * @return the ewaste state property name
	 */
	public String getEwasteStatePropertyName() {
		return mEwasteStatePropertyName;
	}

	/**
	 * Sets the ewaste state property name.
	 *
	 * @param pEwasteStatePropertyName the new ewaste state property name
	 */
	public void setEwasteStatePropertyName(String pEwasteStatePropertyName) {
		mEwasteStatePropertyName = pEwasteStatePropertyName;
	}

	/**
	 * Gets the out of stock items property name.
	 *
	 * @return the out of stock items property name
	 */
	public String getOutOfStockItemsPropertyName() {
		return mOutOfStockItemsPropertyName;
	}

	/**
	 * Sets the out of stock items property name.
	 *
	 * @param pOutOfStockItemsPropertyName the new out of stock items property name
	 */
	public void setOutOfStockItemsPropertyName(String pOutOfStockItemsPropertyName) {
		mOutOfStockItemsPropertyName = pOutOfStockItemsPropertyName;
	}

	/**
	 * Gets the meta info.
	 *
	 * @return the meta info
	 */
	public String getMetaInfo() {
		return mMetaInfo;
	}

	/**
	 * Sets the meta info.
	 *
	 * @param pMetaInfo the new meta info
	 */
	public void setMetaInfo(String pMetaInfo) {
		this.mMetaInfo = pMetaInfo;
	}

	/**
	 * Gets the audit date.
	 *
	 * @return the auditDate
	 */
	public String getAuditDate() {
		return mAuditDate;
	}

	/**
	 * Sets the audit date.
	 *
	 * @param pAuditDate the auditDate to set
	 */
	public void setAuditDate(String pAuditDate) {
		mAuditDate = pAuditDate;
	}

	/**
	 * Gets the item descriptor audit message.
	 *
	 * @return the itemDescriptorAuditMessage
	 */
	public String getItemDescriptorAuditMessage() {
		return mItemDescriptorAuditMessage;
	}

	/**
	 * Sets the item descriptor audit message.
	 *
	 * @param pItemDescriptorAuditMessage the itemDescriptorAuditMessage to set
	 */
	public void setItemDescriptorAuditMessage(String pItemDescriptorAuditMessage) {
		mItemDescriptorAuditMessage = pItemDescriptorAuditMessage;
	}

	/**
	 * Gets the message type.
	 *
	 * @return the messageType
	 */
	public String getMessageType() {
		return mMessageType;
	}

	/**
	 * Sets the message type.
	 *
	 * @param pMessageType the messageType to set
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}

	/**
	 * Gets the message data.
	 *
	 * @return the messageData
	 */
	public String getMessageData() {
		return mMessageData;
	}

	/**
	 * Sets the message data.
	 *
	 * @param pMessageData the messageData to set
	 */
	public void setMessageData(String pMessageData) {
		mMessageData = pMessageData;
	}

	/**
	 * Gets the long description.
	 *
	 * @return the long description
	 */
	public String getLongDescription() {
		return mLongDescription;
	}

	/**
	 * Sets the long description.
	 *
	 * @param pLongDescription the new long description
	 */
	public void setLongDescription(String pLongDescription) {
		mLongDescription = pLongDescription;
	}

	/**
	 * Gets the display name property name.
	 *
	 * @return the display name property name
	 */
	public String getDisplayNamePropertyName() {
		return mDisplayNamePropertyName;
	}

	/** Holds property warehouse location code. */
	private String mWarehouseLocationCode;
	
	/** Holds property store item descriptor name. */
	private String mStoreItemDescriptorName;

	/**
	 * Sets the display name property name.
	 *
	 * @param pDisplayNamePropertyName the new display name property name
	 */
	public void setDisplayNamePropertyName(String pDisplayNamePropertyName) {
		mDisplayNamePropertyName = pDisplayNamePropertyName;
	}

	/**
	 * Gets the image property name.
	 *
	 * @return the image property name
	 */
	public String getImagePropertyName() {
		return mImagePropertyName;
	}

	/**
	 * Sets the image property name.
	 *
	 * @param pImagePropertyName the new image property name
	 */
	public void setImagePropertyName(String pImagePropertyName) {
		mImagePropertyName = pImagePropertyName;
	}

	/**
	 * Gets the path property name.
	 *
	 * @return the path property name
	 */
	public String getPathPropertyName() {
		return mPathPropertyName;
	}

	/**
	 * Sets the path property name.
	 *
	 * @param pPathPropertyName the new path property name
	 */
	public void setPathPropertyName(String pPathPropertyName) {
		mPathPropertyName = pPathPropertyName;
	}

	/**
	 * Gets the ship window max.
	 * 
	 * @return the ship window max
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * Sets the ship window max.
	 * 
	 * @param pShipWindowMax
	 *            the new ship window max
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * Gets the ship window min.
	 * 
	 * @return the ship window min
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * Sets the ship window min.
	 * 
	 * @param pShipWindowMin
	 *            the new ship window min
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * Gets the customer purchase limit.
	 * 
	 * @return the customer purchase limit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Sets the customer purchase limit.
	 * 
	 * @param pCustomerPurchaseLimit
	 *            the new customer purchase limit
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		this.mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * Gets the display name.
	 * 
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Sets the display name.
	 * 
	 * @param pDisplayName
	 *            the new display name
	 */
	public void setDisplayName(String pDisplayName) {
		this.mDisplayName = pDisplayName;
	}

	/**
	 * Gets the can be gift wrapped.
	 * 
	 * @return the can be gift wrapped
	 */
	public String getCanBeGiftWrapped() {
		return mCanBeGiftWrapped;
	}

	/**
	 * Sets the can be gift wrapped.
	 * 
	 * @param pCanBeGiftWrapped
	 *            the new can be gift wrapped
	 */
	public void setCanBeGiftWrapped(String pCanBeGiftWrapped) {
		this.mCanBeGiftWrapped = pCanBeGiftWrapped;
	}

	/**
	 * Gets the color upc level.
	 * 
	 * @return the color upc level
	 */
	public String getColorUpcLevel() {
		return mColorUpcLevel;
	}

	/**
	 * Sets the color upc level.
	 * 
	 * @param pColorUpcLevel
	 *            the new color upc level
	 */
	public void setColorUpcLevel(String pColorUpcLevel) {
		this.mColorUpcLevel = pColorUpcLevel;
	}

	/**
	 * Gets the juvenile product size.
	 * 
	 * @return the juvenile product size
	 */
	public String getJuvenileProductSize() {
		return mJuvenileProductSize;
	}

	/**
	 * Sets the juvenile product size.
	 * 
	 * @param pJuvenileProductSize
	 *            the new juvenile product size
	 */
	public void setJuvenileProductSize(String pJuvenileProductSize) {
		this.mJuvenileProductSize = pJuvenileProductSize;
	}

	/**
	 * Gets the wishlist.
	 * 
	 * @return the wishlist
	 */
	public String getWishlist() {
		return mWishlist;
	}

	/**
	 * Sets the wishlist.
	 * 
	 * @param pWishlist
	 *            the new wishlist
	 */
	public void setWishlist(String pWishlist) {
		mWishlist = pWishlist;
	}

	/**
	 * Gets the registry.
	 * 
	 * @return the registry
	 */
	public String getRegistry() {
		return mRegistry;
	}

	/**
	 * Sets the registry.
	 * 
	 * @param pRegistry
	 *            the new registry
	 */
	public void setRegistry(String pRegistry) {
		mRegistry = pRegistry;
	}

	/**
	 * Gets the location id for ship to home.
	 * 
	 * @return the location id for ship to home
	 */
	public String getLocationIdForShipToHome() {
		return mLocationIdForShipToHome;
	}

	/**
	 * Sets the location id for ship to home.
	 * 
	 * @param pLocationIdForShipToHome
	 *            the new location id for ship to home
	 */
	public void setLocationIdForShipToHome(String pLocationIdForShipToHome) {
		mLocationIdForShipToHome = pLocationIdForShipToHome;
	}

	/**
	 * Gets the channel availability.
	 * 
	 * @return the channel availability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * Sets the channel availability.
	 * 
	 * @param pChannelAvailability
	 *            the new channel availability
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * Gets the atc quantity.
	 * 
	 * @return the atc quantity
	 */
	public String getAtcQuantity() {
		return mAtcQuantity;
	}

	/**
	 * Sets the atc quantity.
	 * 
	 * @param pAtcQuantity
	 *            the new atc quantity
	 */
	public void setAtcQuantity(String pAtcQuantity) {
		mAtcQuantity = pAtcQuantity;
	}

	/**
	 * Returns the commerceItemMetaInfoDescriptorName.
	 * 
	 * @return the commerceItemMetaInfoDescriptorName
	 */
	public String getCommerceItemMetaInfoDescriptorName() {
		return mCommerceItemMetaInfoDescriptorName;
	}

	/**
	 * Sets/updates the commerceItemMetaInfoDescriptorName.
	 * 
	 * @param pCommerceItemMetaInfoDescriptorName
	 *            the commerceItemMetaInfoDescriptorName to set
	 */
	public void setCommerceItemMetaInfoDescriptorName(String pCommerceItemMetaInfoDescriptorName) {
		mCommerceItemMetaInfoDescriptorName = pCommerceItemMetaInfoDescriptorName;
	}

	/**
	 * Returns the commerceItemMetaInfoIdPropertyName.
	 * 
	 * @return the commerceItemMetaInfoIdPropertyName
	 */
	public String getCommerceItemMetaInfoIdPropertyName() {
		return mCommerceItemMetaInfoIdPropertyName;
	}

	/**
	 * Sets/updates the commerceItemMetaInfoIdPropertyName.
	 * 
	 * @param pCommerceItemMetaInfoIdPropertyName
	 *            the commerceItemMetaInfoIdPropertyName to set
	 */
	public void setCommerceItemMetaInfoIdPropertyName(String pCommerceItemMetaInfoIdPropertyName) {
		mCommerceItemMetaInfoIdPropertyName = pCommerceItemMetaInfoIdPropertyName;
	}

	/**
	 * Gets the style dimensions property name.
	 *
	 * @return the mStyleDimensionsPropertyName
	 */
	public String getStyleDimensionsPropertyName() {
		return mStyleDimensionsPropertyName;
	}

	/**
	 * Sets the style dimensions property name.
	 *
	 * @param pStyleDimensionsPropertyName the mStyleDimensionsPropertyName to set
	 */
	public void setStyleDimensionsPropertyName(String pStyleDimensionsPropertyName) {
		this.mStyleDimensionsPropertyName = pStyleDimensionsPropertyName;
	}

	/**
	 * Returns the catalogRefIdPropertyName.
	 * 
	 * @return the catalogRefIdPropertyName
	 */
	public String getCatalogRefIdPropertyName() {
		return mCatalogRefIdPropertyName;
	}

	/**
	 * Sets/updates the catalogRefIdPropertyName.
	 * 
	 * @param pCatalogRefIdPropertyName
	 *            the catalogRefIdPropertyName to set
	 */
	public void setCatalogRefIdPropertyName(String pCatalogRefIdPropertyName) {
		mCatalogRefIdPropertyName = pCatalogRefIdPropertyName;
	}

	/**
	 * Returns the productIdPropertyName.
	 * 
	 * @return the productIdPropertyName
	 */
	public String getProductIdPropertyName() {
		return mProductIdPropertyName;
	}

	/**
	 * Sets/updates the productIdPropertyName.
	 * 
	 * @param pProductIdPropertyName
	 *            the productIdPropertyName to set
	 */
	public void setProductIdPropertyName(String pProductIdPropertyName) {
		mProductIdPropertyName = pProductIdPropertyName;
	}

	/**
	 * Returns the quantityPropertyName.
	 * 
	 * @return the quantityPropertyName
	 */
	public String getQuantityPropertyName() {
		return mQuantityPropertyName;
	}

	/**
	 * Sets/updates the quantityPropertyName.
	 * 
	 * @param pQuantityPropertyName
	 *            the quantityPropertyName to set
	 */
	public void setQuantityPropertyName(String pQuantityPropertyName) {
		mQuantityPropertyName = pQuantityPropertyName;
	}

	/**
	 * Returns the pricePropertyName.
	 * 
	 * @return the pricePropertyName
	 */
	public String getPricePropertyName() {
		return mPricePropertyName;
	}

	/**
	 * Sets/updates the pricePropertyName.
	 * 
	 * @param pPricePropertyName
	 *            the pricePropertyName to set
	 */
	public void setPricePropertyName(String pPricePropertyName) {
		mPricePropertyName = pPricePropertyName;
	}

	/**
	 * Returns the isAdjustedPropertyName.
	 * 
	 * @return the isAdjustedPropertyName
	 */
	public String getIsAdjustedPropertyName() {
		return mIsAdjustedPropertyName;
	}

	/**
	 * Sets/updates the isAdjustedPropertyName.
	 * 
	 * @param pIsAdjustedPropertyName
	 *            the isAdjustedPropertyName to set
	 */
	public void setIsAdjustedPropertyName(String pIsAdjustedPropertyName) {
		mIsAdjustedPropertyName = pIsAdjustedPropertyName;
	}

	/**
	 * Gets the primary image.
	 * 
	 * @return the mPrimaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * Sets the primary image.
	 * 
	 * @param pPrimaryImage
	 *            the mPrimaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		this.mPrimaryImage = pPrimaryImage;
	}

	/**
	 * Gets the total saved amount.
	 * 
	 * @return the total saved amount
	 */
	public String getTotalSavedAmount() {
		return mTotalSavedAmount;
	}

	/**
	 * Sets the total saved amount.
	 * 
	 * @param pTotalSavedAmount
	 *            the new total saved amount
	 */
	public void setTotalSavedAmount(String pTotalSavedAmount) {
		mTotalSavedAmount = pTotalSavedAmount;
	}

	/**
	 * Gets the total saved percentage.
	 * 
	 * @return the total saved percentage
	 */
	public String getTotalSavedPercentage() {
		return mTotalSavedPercentage;
	}

	/**
	 * Sets the total saved percentage.
	 * 
	 * @param pTotalSavedPercentage
	 *            the new total saved percentage
	 */
	public void setTotalSavedPercentage(String pTotalSavedPercentage) {
		mTotalSavedPercentage = pTotalSavedPercentage;
	}

	/**
	 * gets the freightCalssProperty name.
	 * 
	 * @return the freight class property name
	 */
	public String getFreightClassPropertyName() {
		return mFreightClassPropertyName;
	}

	/**
	 * sets the freightCalssProperty name.
	 * 
	 * @param pFreightClassPropertyName
	 *            the new freight class property name
	 */
	public void setFreightClassPropertyName(String pFreightClassPropertyName) {
		this.mFreightClassPropertyName = pFreightClassPropertyName;
	}

	/**
	 * Gets the applicable shipping methods property name.
	 * 
	 * @return the applicable shipping methods property name
	 */
	public String getApplicableShippingMethodsPropertyName() {
		return mApplicableShippingMethodsPropertyName;
	}

	/**
	 * Sets the applicable shipping methods property name.
	 * 
	 * @param pApplicableShippingMethodsPropertyName
	 *            the new applicable shipping methods property name
	 */
	public void setApplicableShippingMethodsPropertyName(String pApplicableShippingMethodsPropertyName) {
		this.mApplicableShippingMethodsPropertyName = pApplicableShippingMethodsPropertyName;
	}

	/**
	 * Gets the fullfillment msg property name.
	 * 
	 * @return the fullfillment msg property name
	 */
	public String getFullfillmentMsgPropertyName() {
		return mFullfillmentMsgPropertyName;
	}

	/**
	 * Sets the fullfillment msg property name.
	 * 
	 * @param pFullfillmentMsgPropertyName
	 *            the new fullfillment msg property name
	 */
	public void setFullfillmentMsgPropertyName(String pFullfillmentMsgPropertyName) {
		this.mFullfillmentMsgPropertyName = pFullfillmentMsgPropertyName;
	}

	/**
	 * Gets the ship region property name.
	 * 
	 * @return the ship region property name
	 */
	public String getShipRegionPropertyName() {
		return mShipRegionPropertyName;
	}

	/**
	 * Sets the ship region property name.
	 * 
	 * @param pShipRegionPropertyName
	 *            the new ship region property name
	 */
	public void setShipRegionPropertyName(String pShipRegionPropertyName) {
		this.mShipRegionPropertyName = pShipRegionPropertyName;
	}

	/**
	 * Gets the region code property name.
	 * 
	 * @return the region code property name
	 */
	public String getRegionCodePropertyName() {
		return mRegionCodePropertyName;
	}

	/**
	 * Sets the region code property name.
	 * 
	 * @param pRegionCodePropertyName
	 *            the new region code property name
	 */
	public void setRegionCodePropertyName(String pRegionCodePropertyName) {
		this.mRegionCodePropertyName = pRegionCodePropertyName;
	}

	/**
	 * Gets the id property name.
	 * 
	 * @return the id property name
	 */
	public String getIdPropertyName() {
		return mIdPropertyName;
	}

	/**
	 * Sets the id property name.
	 * 
	 * @param pIdPropertyName
	 *            the new id property name
	 */
	public void setIdPropertyName(String pIdPropertyName) {
		this.mIdPropertyName = pIdPropertyName;
	}

	/**
	 * Gets the ship method code property name.
	 * 
	 * @return the ship method code property name
	 */
	public String getShipMethodCodePropertyName() {
		return mShipMethodCodePropertyName;
	}

	/**
	 * Sets the ship method code property name.
	 * 
	 * @param pShipMethodCodePropertyName
	 *            the new ship method code property name
	 */
	public void setShipMethodCodePropertyName(String pShipMethodCodePropertyName) {
		this.mShipMethodCodePropertyName = pShipMethodCodePropertyName;
	}

	/**
	 * Gets the ship method name property name.
	 * 
	 * @return the ship method name property name
	 */
	public String getShipMethodNamePropertyName() {
		return mShipMethodNamePropertyName;
	}

	/**
	 * Sets the ship method name property name.
	 * 
	 * @param pShipMethodNamePropertyName
	 *            the new ship method name property name
	 */
	public void setShipMethodNamePropertyName(String pShipMethodNamePropertyName) {
		this.mShipMethodNamePropertyName = pShipMethodNamePropertyName;
	}

	/**
	 * Gets the ship rate property name.
	 * 
	 * @return the ship rate property name
	 */
	public String getShipRatePropertyName() {
		return mShipRatePropertyName;
	}

	/**
	 * Sets the ship rate property name.
	 * 
	 * @param pShipRatePropertyName
	 *            the new ship rate property name
	 */
	public void setShipRatePropertyName(String pShipRatePropertyName) {
		this.mShipRatePropertyName = pShipRatePropertyName;
	}

	/**
	 * Gets the ship method property name.
	 * 
	 * @return the ship method property name
	 */
	public String getShipMethodPropertyName() {
		return mShipMethodPropertyName;
	}

	/**
	 * Sets the ship method property name.
	 * 
	 * @param pShipMethodPropertyName
	 *            the new ship method property name
	 */
	public void setShipMethodPropertyName(String pShipMethodPropertyName) {
		this.mShipMethodPropertyName = pShipMethodPropertyName;
	}

	/**
	 * Gets the lower order bound property name.
	 * 
	 * @return the lower order bound property name
	 */
	public String getLowerOrderBoundPropertyName() {
		return mLowerOrderBoundPropertyName;
	}

	/**
	 * Sets the lower order bound property name.
	 * 
	 * @param pLowerOrderBoundPropertyName
	 *            the new lower order bound property name
	 */
	public void setLowerOrderBoundPropertyName(String pLowerOrderBoundPropertyName) {
		this.mLowerOrderBoundPropertyName = pLowerOrderBoundPropertyName;
	}

	/**
	 * Gets the upper order bound property name.
	 * 
	 * @return the upper order bound property name
	 */
	public String getUpperOrderBoundPropertyName() {
		return mUpperOrderBoundPropertyName;
	}

	/**
	 * Sets the upper order bound property name.
	 * 
	 * @param pUpperOrderBoundPropertyName
	 *            the new upper order bound property name
	 */
	public void setUpperOrderBoundPropertyName(String pUpperOrderBoundPropertyName) {
		this.mUpperOrderBoundPropertyName = pUpperOrderBoundPropertyName;
	}

	/**
	 * Gets the ship price property name.
	 * 
	 * @return the ship price property name
	 */
	public String getShipPricePropertyName() {
		return mShipPricePropertyName;
	}

	/**
	 * Sets the ship price property name.
	 * 
	 * @param pShipPricePropertyName
	 *            the new ship price property name
	 */
	public void setShipPricePropertyName(String pShipPricePropertyName) {
		this.mShipPricePropertyName = pShipPricePropertyName;
	}

	/**
	 * Gets the error message property name.
	 * 
	 * @return the error message property name
	 */
	public String getErrorMessagePropertyName() {
		return mErrorMessagePropertyName;
	}

	/**
	 * Sets the error message property name.
	 * 
	 * @param pErrorMessagePropertyName
	 *            the new error message property name
	 */
	public void setErrorMessagePropertyName(String pErrorMessagePropertyName) {
		this.mErrorMessagePropertyName = pErrorMessagePropertyName;
	}

	/**
	 * getter for nonQualifiedStateCodes.
	 * 
	 * @return the non qualified state codes property name
	 */
	public String getNonQualifiedStateCodesPropertyName() {
		return mNonQualifiedStateCodesPropertyName;
	}

	/**
	 * setter for nonQualifiedStateCodes.
	 * 
	 * @param pNonQualifiedStateCodesPropertyName
	 *            the new non qualified state codes property name
	 */
	public void setNonQualifiedStateCodesPropertyName(String pNonQualifiedStateCodesPropertyName) {
		this.mNonQualifiedStateCodesPropertyName = pNonQualifiedStateCodesPropertyName;
	}

	/**
	 * Gets the preorder level.
	 * 
	 * @return the preorder level
	 */
	public String getPreorderLevel() {
		return mPreorderLevel;
	}

	/**
	 * Sets the preorder level.
	 * 
	 * @param pPreorderLevel
	 *            the new preorder level
	 */
	public void setPreorderLevel(String pPreorderLevel) {
		mPreorderLevel = pPreorderLevel;
	}

	/**
	 * Gets the details property name.
	 * 
	 * @return the details property name
	 */
	public String getDetailsPropertyName() {
		return mDetailsPropertyName;
	}

	/**
	 * Sets the details property name.
	 * 
	 * @param pDetailsPropertyName
	 *            the new details property name
	 */
	public void setDetailsPropertyName(String pDetailsPropertyName) {
		this.mDetailsPropertyName = pDetailsPropertyName;
	}

	/**
	 * Gets the street date.
	 * 
	 * @return the street date
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 * 
	 * @param pStreetDate
	 *            the new street date
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * Gets the super display flag.
	 * 
	 * @return the super display flag
	 */
	public String getSuperDisplayFlag() {
		return mSuperDisplayFlag;
	}

	/**
	 * Sets the super display flag.
	 * 
	 * @param pSuperDisplayFlag
	 *            the new super display flag
	 */
	public void setSuperDisplayFlag(String pSuperDisplayFlag) {
		mSuperDisplayFlag = pSuperDisplayFlag;
	}

	/**
	 * Gets the checks if is deleted.
	 * 
	 * @return the checks if is deleted
	 */
	public String getIsDeleted() {
		return mIsDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 * 
	 * @param pIsDeleted
	 *            the new checks if is deleted
	 */
	public void setIsDeleted(String pIsDeleted) {
		mIsDeleted = pIsDeleted;
	}

	/**
	 * Gets the unit price property name.
	 * 
	 * @return the unitPricePropertyName
	 */
	public String getUnitPricePropertyName() {
		return mUnitPricePropertyName;
	}

	/**
	 * Sets the unit price property name.
	 * 
	 * @param pUnitPricePropertyName
	 *            the unitPricePropertyName to set
	 */
	public void setUnitPricePropertyName(String pUnitPricePropertyName) {
		mUnitPricePropertyName = pUnitPricePropertyName;
	}

	/**
	 * Gets the gift wrap item property name.
	 * 
	 * @return the giftWrapItemPropertyName
	 */
	public String getGiftWrapItemPropertyName() {
		return mGiftWrapItemPropertyName;
	}

	/**
	 * Sets the gift wrap item property name.
	 * 
	 * @param pGiftWrapItemPropertyName
	 *            the giftWrapItemPropertyName to set
	 */
	public void setGiftWrapItemPropertyName(String pGiftWrapItemPropertyName) {
		mGiftWrapItemPropertyName = pGiftWrapItemPropertyName;
	}

	/**
	 * Gets the commerce item property name.
	 * 
	 * @return the commerceItemPropertyName
	 */
	public String getCommerceItemPropertyName() {
		return mCommerceItemPropertyName;
	}

	/**
	 * Sets the commerce item property name.
	 * 
	 * @param pCommerceItemPropertyName
	 *            the commerceItemPropertyName to set
	 */
	public void setCommerceItemPropertyName(String pCommerceItemPropertyName) {
		mCommerceItemPropertyName = pCommerceItemPropertyName;
	}

	/**
	 * Gets the ship item rel id property name.
	 * 
	 * @return the shipItemRelIdPropertyName
	 */
	public String getShipItemRelIdPropertyName() {
		return mShipItemRelIdPropertyName;
	}

	/**
	 * Sets the ship item rel id property name.
	 * 
	 * @param pShipItemRelIdPropertyName
	 *            the shipItemRelIdPropertyName to set
	 */
	public void setShipItemRelIdPropertyName(String pShipItemRelIdPropertyName) {
		mShipItemRelIdPropertyName = pShipItemRelIdPropertyName;
	}

	/**
	 * Gets the web display flag.
	 * 
	 * @return the web display flag
	 */
	public String getWebDisplayFlag() {
		return mWebDisplayFlag;
	}

	/**
	 * Sets the web display flag.
	 * 
	 * @param pWebDisplayFlag
	 *            the new web display flag
	 */
	public void setWebDisplayFlag(String pWebDisplayFlag) {
		mWebDisplayFlag = pWebDisplayFlag;
	}

	/**
	 * Gets the meta info id property name.
	 * 
	 * @return the meta info id property name
	 */
	public String getMetaInfoIdPropertyName() {
		return mMetaInfoIdPropertyName;
	}

	/**
	 * Sets the meta info id property name.
	 * 
	 * @param pMetaInfoIdPropertyName
	 *            the new meta info id property name
	 */
	public void setMetaInfoIdPropertyName(String pMetaInfoIdPropertyName) {
		mMetaInfoIdPropertyName = pMetaInfoIdPropertyName;
	}
	
	/**
	 * Gets the payment groups property name.
	 *
	 * @return the paymentGroupsPropertyName
	 */
	public String getPaymentGroupsPropertyName() {
		return mPaymentGroupsPropertyName;
	}

	/**
	 * Sets the payment groups property name.
	 *
	 * @param pPaymentGroupsPropertyName the paymentGroupsPropertyName to set
	 */
	public void setPaymentGroupsPropertyName(String pPaymentGroupsPropertyName) {
		mPaymentGroupsPropertyName = pPaymentGroupsPropertyName;
	}

	/**
	 * Gets the out of stock item descriptor name.
	 *
	 * @return the out of stock item descriptor name
	 */
	public String getOutOfStockItemDescriptorName() {
		return mOutOfStockItemDescriptorName;
	}

	/**
	 * Sets the out of stock item descriptor name.
	 *
	 * @param pOutOfStockItemDescriptorName the new out of stock item descriptor name
	 */
	public void setOutOfStockItemDescriptorName(
			String pOutOfStockItemDescriptorName) {
		mOutOfStockItemDescriptorName = pOutOfStockItemDescriptorName;
	}

	/**
	 * Gets the list price property name.
	 *
	 * @return the list price property name
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}

	/**
	 * Sets the list price property name.
	 *
	 * @param pListPricePropertyName the new list price property name
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		mListPricePropertyName = pListPricePropertyName;
	}

	/**
	 * Gets the sale price property name.
	 *
	 * @return the sale price property name
	 */
	public String getSalePricePropertyName() {
		return mSalePricePropertyName;
	}

	/**
	 * Sets the sale price property name.
	 *
	 * @param pSalePricePropertyName the new sale price property name
	 */
	public void setSalePricePropertyName(String pSalePricePropertyName) {
		this.mSalePricePropertyName = pSalePricePropertyName;
	}
	
	/**
	 * Gets the location id property name.
	 *
	 * @return the location id property name
	 */
	public String getLocationIdPropertyName() {
		return mLocationIdPropertyName;
	}

	/**
	 * Sets the location id property name.
	 *
	 * @param pLocationIdPropertyName the new location id property name
	 */
	public void setLocationIdPropertyName(String pLocationIdPropertyName) {
		mLocationIdPropertyName = pLocationIdPropertyName;
	}
	/**
	 * Gets the location id property name.
	 *
	 * @return the location id property name
	 */
	public String getPromotionIdPropertyName() {
		return mPromotionIdPropertyName;
	}

	/**
	 * Sets the promotion id property name.
	 *
	 * @param pPromotionIdPropertyName the promotionIdPropertyName to set
	 */
	public void setPromotionIdPropertyName(String pPromotionIdPropertyName) {
		mPromotionIdPropertyName = pPromotionIdPropertyName;
	}

	/**
	 * Gets the un cartable.
	 *
	 * @return the un cartable
	 */
	public String getUnCartable() {
		return mUnCartable;
	}

	/**
	 * Sets the un cartable.
	 *
	 * @param pUnCartable the new un cartable
	 */
	public void setUnCartable(String pUnCartable) {
		this.mUnCartable = pUnCartable;
	}

	/**
	 * Gets the channel name.
	 *
	 * @return the channel name
	 */
	public String getChannelName() {
		return mChannelName;
	}

	/**
	 * Sets the channel name.
	 *
	 * @param pChannelName the new channel name
	 */
	public void setChannelName(String pChannelName) {
		this.mChannelName = pChannelName;
	}

	/**
	 * Gets the channel id.
	 *
	 * @return the channel id
	 */
	public String getChannelId() {
		return mChannelId;
	}

	/**
	 * Sets the channel id.
	 *
	 * @param pChannelId the new channel id
	 */
	public void setChannelId(String pChannelId) {
		this.mChannelId = pChannelId;
	}

	/**
	 * Gets the channel user name.
	 *
	 * @return the channel user name
	 */
	public String getChannelUserName() {
		return mChannelUserName;
	}

	/**
	 * Sets the channel user name.
	 *
	 * @param pChannelUserName the new channel user name
	 */
	public void setChannelUserName(String pChannelUserName) {
		this.mChannelUserName = pChannelUserName;
	}

	/**
	 * Gets the channel co user name.
	 *
	 * @return the channel co user name
	 */
	public String getChannelCoUserName() {
		return mChannelCoUserName;
	}

	/**
	 * Sets the channel co user name.
	 *
	 * @param pChannelCoUserName the new channel co user name
	 */
	public void setChannelCoUserName(String pChannelCoUserName) {
		this.mChannelCoUserName = pChannelCoUserName;
	}

	/**
	 * Gets the channel type.
	 *
	 * @return the channel type
	 */
	public String getChannelType() {
		return mChannelType;
	}

	/**
	 * Sets the channel type.
	 *
	 * @param pChannelType the new channel type
	 */
	public void setChannelType(String pChannelType) {
		this.mChannelType = pChannelType;
	}
	
	/**
	 * Gets the source channel.
	 *
	 * @return the source channel
	 */
	public String getSourceChannel() {
		return mSourceChannel;
	}

	/**
	 * Sets the source channel.
	 *
	 * @param pSourceChannel the new source channel
	 */
	public void setSourceChannel(String pSourceChannel) {
		mSourceChannel = pSourceChannel;
	}

	/**
	 * Gets the item in store pick up.
	 *
	 * @return the item in store pick up
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 *
	 * @param pItemInStorePickUp the new item in store pick up
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		this.mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * Gets the ship from store eligible.
	 *
	 * @return the ship from store eligible
	 */
	public String getShipToStoreEligible() {
		return mShipToStoreEligible;
	}

	/**
	 * Sets the ship from store eligible.
	 *
	 * @param pShipToStoreEligible the new ship to store eligible
	 */
	public void setShipToStoreEligible(String pShipToStoreEligible) {
		this.mShipToStoreEligible = pShipToStoreEligible;
	}

	
	/**
	 * Gets the eligible freight classes property name.
	 *
	 * @return eligibleFreightClassesPropertyName
	 */
	public String getEligibleFreightClassesPropertyName() {
		return mEligibleFreightClassesPropertyName;
	}
	
	/**
	 * Sets the eligible freight classes property name.
	 *
	 * @param pEligibleFreightClassesPropertyName the eligibleFreightClassesPropertyName to set
	 */
	public void setEligibleFreightClassesPropertyName(
			String pEligibleFreightClassesPropertyName) {
		this.mEligibleFreightClassesPropertyName = pEligibleFreightClassesPropertyName;
	}

	/**
	 * Gets the eligible ship regions property name.
	 *
	 * @return eligibleShipRegionsPropertyName
	 */
	public String getEligibleShipRegionsPropertyName() {
		return mEligibleShipRegionsPropertyName;
	}
	
	/**
	 * Sets the eligible ship regions property name.
	 *
	 * @param pEligibleShipRegionsPropertyName the eligibleShipRegionsPropertyName to set
	 */
	public void setEligibleShipRegionsPropertyName(
			String pEligibleShipRegionsPropertyName) {
		this.mEligibleShipRegionsPropertyName = pEligibleShipRegionsPropertyName;
	}
	
	/**
	 * Gets the ship region name property name.
	 *
	 * @return shipRegionNamePropertyName
	 */
	public String getShipRegionNamePropertyName() {
		return mShipRegionNamePropertyName;
	}
	
	/**
	 * Sets the ship region name property name.
	 *
	 * @param pShipRegionNamePropertyName the shipRegionNamePropertyName to set
	 */
	public void setShipRegionNamePropertyName(String pShipRegionNamePropertyName) {
		this.mShipRegionNamePropertyName = pShipRegionNamePropertyName;
	}
	
	/**
	 * Gets the order cut off price property name.
	 *
	 * @return orderCutOffPricePropertyName
	 */
	public String getOrderCutOffPricePropertyName() {
		return mOrderCutOffPricePropertyName;
	}
	
	/**
	 * Sets the order cut off price property name.
	 *
	 * @param pOrderCutOffPricePropertyName the orderCutOffPricePropertyName to set
	 */
	public void setOrderCutOffPricePropertyName(String pOrderCutOffPricePropertyName) {
		this.mOrderCutOffPricePropertyName = pOrderCutOffPricePropertyName;
	}
	
	/**
	 * Gets the delivery time min days property name.
	 *
	 * @return deliveryTimeMinDaysPropertyName
	 */
	public String getDeliveryTimeMinDaysPropertyName() {
		return mDeliveryTimeMinDaysPropertyName;
	}
	
	/**
	 * Sets the delivery time min days property name.
	 *
	 * @param pDeliveryTimeMinDaysPropertyName the deliveryTimeMinDaysPropertyName to set
	 */
	public void setDeliveryTimeMinDaysPropertyName(
			String pDeliveryTimeMinDaysPropertyName) {
		this.mDeliveryTimeMinDaysPropertyName = pDeliveryTimeMinDaysPropertyName;
	}
	
	/**
	 * Gets the delivery time max days property name.
	 *
	 * @return deliveryTimeMaxDaysPropertyName
	 */
	public String getDeliveryTimeMaxDaysPropertyName() {
		return mDeliveryTimeMaxDaysPropertyName;
	}
	
	/**
	 * Sets the delivery time max days property name.
	 *
	 * @param pDeliveryTimeMaxDaysPropertyName the deliveryTimeMaxDaysPropertyName to set
	 */
	public void setDeliveryTimeMaxDaysPropertyName(
			String pDeliveryTimeMaxDaysPropertyName) {
		this.mDeliveryTimeMaxDaysPropertyName = pDeliveryTimeMaxDaysPropertyName;
	}
	
	/**
	 * Gets the time in transit min days property name.
	 *
	 * @return the timeInTransitMinDaysPropertyName
	 */
	public String getTimeInTransitMinDaysPropertyName() {
		return mTimeInTransitMinDaysPropertyName;
	}

	/**
	 * Sets the time in transit min days property name.
	 *
	 * @param pTimeInTransitMinDaysPropertyName the timeInTransitMinDaysPropertyName to set
	 */
	public void setTimeInTransitMinDaysPropertyName(
			String pTimeInTransitMinDaysPropertyName) {
		mTimeInTransitMinDaysPropertyName = pTimeInTransitMinDaysPropertyName;
	}

	/**
	 * Gets the time in transit max days property name.
	 *
	 * @return the timeInTransitMaxDaysPropertyName
	 */
	public String getTimeInTransitMaxDaysPropertyName() {
		return mTimeInTransitMaxDaysPropertyName;
	}

	/**
	 * Sets the time in transit max days property name.
	 *
	 * @param pTimeInTransitMaxDaysPropertyName the timeInTransitMaxDaysPropertyName to set
	 */
	public void setTimeInTransitMaxDaysPropertyName(
			String pTimeInTransitMaxDaysPropertyName) {
		mTimeInTransitMaxDaysPropertyName = pTimeInTransitMaxDaysPropertyName;
	}

	/**
	 * Gets the ship min price property name.
	 *
	 * @return shipMinPricePropertyName
	 */
	public String getShipMinPricePropertyName() {
		return mShipMinPricePropertyName;
	}
	
	/**
	 * Sets the ship min price property name.
	 *
	 * @param pShipMinPricePropertyName the shipMinPricePropertyName to set
	 */
	public void setShipMinPricePropertyName(String pShipMinPricePropertyName) {
		this.mShipMinPricePropertyName = pShipMinPricePropertyName;
	}
	
	/**
	 * Gets the ship max price property name.
	 *
	 * @return shipMaxPricePropertyName
	 */
	public String getShipMaxPricePropertyName() {
		return mShipMaxPricePropertyName;
	}
	
	/**
	 * Sets the ship max price property name.
	 *
	 * @param pShipMaxPricePropertyName the shipMaxPricePropertyName to set
	 */
	public void setShipMaxPricePropertyName(String pShipMaxPricePropertyName) {
		this.mShipMaxPricePropertyName = pShipMaxPricePropertyName;
	}
	
	
	/** The Sku id property name. */
	private String mSkuIdPropertyName;
	
	/** The Zip code property name. */
	private String  mZipCodePropertyName;
	
	/**
	 * Gets the sku id property name.
	 *
	 * @return the skuIdPropertyName
	 */	
	public String getSkuIdPropertyName() {
		return mSkuIdPropertyName;
	}
	
	/**
	 * Sets the sku id property name.
	 *
	 * @param pSkuIdPropertyName the skuIdPropertyName to set
	 */	
	public void setSkuIdPropertyName(String pSkuIdPropertyName) {
		this.mSkuIdPropertyName = pSkuIdPropertyName;
	}
	
	/**
	 * Gets the zip code property name.
	 *
	 * @return the zip code property name
	 */
	public String getZipCodePropertyName() {
		return mZipCodePropertyName;
	}
	
	/**
	 * Sets the zip code property name.
	 *
	 * @param pZipCodePropertyName the new zip code property name
	 */
	public void setZipCodePropertyName(String pZipCodePropertyName) {
		this.mZipCodePropertyName = pZipCodePropertyName;
	}

	/**
	 * Gets the bpp eligible property name.
	 *
	 * @return the bpp eligible property name
	 */
	public String getBppEligiblePropertyName() {
		return mBppEligiblePropertyName;
	}

	/**
	 * Sets the bpp eligible property name.
	 *
	 * @param pBppEligiblePropertyName the new bpp eligible property name
	 */
	public void setBppEligiblePropertyName(String pBppEligiblePropertyName) {
		mBppEligiblePropertyName = pBppEligiblePropertyName;
	}

	/**
	 * Gets the bpp name property name.
	 *
	 * @return the bpp name property name
	 */
	public String getBppNamePropertyName() {
		return mBppNamePropertyName;
	}

	/**
	 * Sets the bpp name property name.
	 *
	 * @param pBppNamePropertyName the new bpp name property name
	 */
	public void setBppNamePropertyName(String pBppNamePropertyName) {
		mBppNamePropertyName = pBppNamePropertyName;
	}

	/**
	 * Gets the bpp cart description property name.
	 *
	 * @return the bpp cart description property name
	 */
	public String getBppCartDescriptionPropertyName() {
		return mBppCartDescriptionPropertyName;
	}

	/**
	 * Sets the bpp cart description property name.
	 *
	 * @param pBppCartDescriptionPropertyName the new bpp cart description property name
	 */
	public void setBppCartDescriptionPropertyName(String pBppCartDescriptionPropertyName) {
		this.mBppCartDescriptionPropertyName = pBppCartDescriptionPropertyName;
	}

	/**
	 * Gets the bpp item id property name.
	 *
	 * @return the bpp item id property name
	 */
	public String getBppItemIdPropertyName() {
		return mBppItemIdPropertyName;
	}

	/**
	 * Sets the bpp item id property name.
	 *
	 * @param pBppItemIdPropertyName the new bpp item id property name
	 */
	public void setBppItemIdPropertyName(String pBppItemIdPropertyName) {
		mBppItemIdPropertyName = pBppItemIdPropertyName;
	}

	/**
	 * Gets the bpp price property name.
	 *
	 * @return the bpp price property name
	 */
	public String getBppPricePropertyName() {
		return mBppPricePropertyName;
	}

	/**
	 * Sets the bpp price property name.
	 *
	 * @param pBppPricePropertyName the new bpp price property name
	 */
	public void setBppPricePropertyName(String pBppPricePropertyName) {
		mBppPricePropertyName = pBppPricePropertyName;
	}

	/**
	 * Gets the bpp sku id property name.
	 *
	 * @return the bpp sku id property name
	 */
	public String getBppSkuIdPropertyName() {
		return mBppSkuIdPropertyName;
	}

	/**
	 * Sets the bpp sku id property name.
	 *
	 * @param pBppSkuIdPropertyName the new bpp sku id property name
	 */
	public void setBppSkuIdPropertyName(String pBppSkuIdPropertyName) {
		mBppSkuIdPropertyName = pBppSkuIdPropertyName;
	}
	
	
			


	/**
	 * Gets the warehouse location code.
	 *
	 * @return the warehouse location code
	 */
	public String getWarehouseLocationCode() {
		return mWarehouseLocationCode;
	}

	/**
	 * Sets the warehouse location code.
	 *
	 * @param pWarehouseLocationCode the new warehouse location code
	 */
	public void setWarehouseLocationCode(String pWarehouseLocationCode) {
		this.mWarehouseLocationCode = pWarehouseLocationCode;
	}

	/**
	 * Gets the store item descriptor name.
	 *
	 * @return the store item descriptor name
	 */
	public String getStoreItemDescriptorName() {
		return mStoreItemDescriptorName;
	}

	/**
	 * Sets the store item descriptor name.
	 *
	 * @param pStoreItemDescriptorName the new store item descriptor name
	 */
	public void setStoreItemDescriptorName(String pStoreItemDescriptorName) {
		this.mStoreItemDescriptorName = pStoreItemDescriptorName;
	}

	/**
	 * Gets the bpp retail property name.
	 *
	 * @return the bpp retail property name
	 */
	public String getBppRetailPropertyName() {
		return mBppRetailPropertyName;
	}

	/**
	 * Sets the bpp retail property name.
	 *
	 * @param pBppRetailPropertyName the new bpp retail property name
	 */
	public void setBppRetailPropertyName(String pBppRetailPropertyName) {
		mBppRetailPropertyName = pBppRetailPropertyName;
	}

	/**
	 * Gets the gift wrap comm item id.
	 *
	 * @return the giftWrapCommItemId
	 */
	public String getGiftWrapCommItemId() {
		return mGiftWrapCommItemId;
	}

	/**
	 * Sets the gift wrap comm item id.
	 *
	 * @param pGiftWrapCommItemId the giftWrapCommItemId to set
	 */
	public void setGiftWrapCommItemId(String pGiftWrapCommItemId) {
		mGiftWrapCommItemId = pGiftWrapCommItemId;
	}

	/**
	 * Gets the gift wrap quantity.
	 *
	 * @return the giftWrapQuantity
	 */
	public String getGiftWrapQuantity() {
		return mGiftWrapQuantity;
	}

	/**
	 * Sets the gift wrap quantity.
	 *
	 * @param pGiftWrapQuantity the giftWrapQuantity to set
	 */
	public void setGiftWrapQuantity(String pGiftWrapQuantity) {
		mGiftWrapQuantity = pGiftWrapQuantity;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * Gets the shipping group id.
	 *
	 * @return the shippingGroupId
	 */
	public String getShippingGroupId() {
		return mShippingGroupId;
	}

	/**
	 * Sets the shipping group id.
	 *
	 * @param pShippingGroupId the shippingGroupId to set
	 */
	public void setShippingGroupId(String pShippingGroupId) {
		mShippingGroupId = pShippingGroupId;
	}

	/**
	 * Gets the gift wrap catalog ref id.
	 *
	 * @return the giftWrapCatalogRefId
	 */
	public String getGiftWrapCatalogRefId() {
		return mGiftWrapCatalogRefId;
	}

	/**
	 * Sets the gift wrap catalog ref id.
	 *
	 * @param pGiftWrapCatalogRefId the giftWrapCatalogRefId to set
	 */
	public void setGiftWrapCatalogRefId(String pGiftWrapCatalogRefId) {
		mGiftWrapCatalogRefId = pGiftWrapCatalogRefId;
	}

	/**
	 * Gets the gift wrap product id.
	 *
	 * @return the giftWrapProductId
	 */
	public String getGiftWrapProductId() {
		return mGiftWrapProductId;
	}

	/**
	 * Sets the gift wrap product id.
	 *
	 * @param pGiftWrapProductId the giftWrapProductId to set
	 */
	public void setGiftWrapProductId(String pGiftWrapProductId) {
		mGiftWrapProductId = pGiftWrapProductId;
	}

	/**
	 * Gets the gift item.
	 *
	 * @return the giftItem
	 */
	public String getGiftItem() {
		return mGiftItem;
	}

	/**
	 * Sets the gift item.
	 *
	 * @param pGiftItem the giftItem to set
	 */
	public void setGiftItem(String pGiftItem) {
		mGiftItem = pGiftItem;
	}

	/**
	 * Gets the store name.
	 *
	 * @return the mStoreName
	 */
	public String getStoreName() {
		return mStoreName;
	}

	/**
	 * Sets the store name.
	 *
	 * @param pStoreName the new store name
	 */
	public void setStoreName(String pStoreName) {
		this.mStoreName = pStoreName;
	}

	/**
	 * Gets the store address1.
	 *
	 * @return the mStoreAddress1
	 */
	public String getStoreAddress1() {
		return mStoreAddress1;
	}

	/**
	 * Sets the store address1.
	 *
	 * @param pStoreAddress1 the new store address1
	 */
	public void setStoreAddress1(String pStoreAddress1) {
		this.mStoreAddress1 = pStoreAddress1;
	}

	/**
	 * Gets the store state address.
	 *
	 * @return the mStoreStateAddress
	 */
	public String getStoreStateAddress() {
		return mStoreStateAddress;
	}

	/**
	 * Sets the store state address.
	 *
	 * @param pStoreStateAddress the new store state address
	 */
	public void setStoreStateAddress(String pStoreStateAddress) {
		this.mStoreStateAddress = pStoreStateAddress;
	}

	/**
	 * Gets the store postal code.
	 *
	 * @return the mStorePostalCode
	 */
	public String getStorePostalCode() {
		return mStorePostalCode;
	}

	/**
	 * Sets the store postal code.
	 *
	 * @param pStorePostalCode the new store postal code
	 */
	public void setStorePostalCode(String pStorePostalCode) {
		this.mStorePostalCode = pStorePostalCode;
	}

	/**
	 * Gets the city name.
	 *
	 * @return the mCityName
	 */
	public String getCityName() {
		return mCityName;
	}

	/**
	 * Sets the city name.
	 *
	 * @param pCityName the new city name
	 */
	public void setCityName(String pCityName) {
		this.mCityName = pCityName;
	}

	/**
	 * Gets the bpp term property name.
	 *
	 * @return the bpp term property name
	 */
	public String getBppTermPropertyName() {
		return mBppTermPropertyName;
	}

	/**
	 * Sets the bpp term property name.
	 *
	 * @param pBppTermPropertyName the new bpp term property name
	 */
	public void setBppTermPropertyName(String pBppTermPropertyName) {
		mBppTermPropertyName = pBppTermPropertyName;
	}
	
		/**
		 * Gets the noti fy me.
		 *
		 * @return the mNotiFyMe
		 */
	public String getNotiFyMe() {
		return mNotiFyMe;
	}

	/**
	 * Sets the noti fy me.
	 *
	 * @param pNotiFyMe the mNotiFyMe to set
	 */
	public void setNotiFyMe(String pNotiFyMe) {
		this.mNotiFyMe = pNotiFyMe;
	}
	
	
		
	/**
	 * Gets the seo title text property name.
	 *
	 * @return the mSeoTitleTextPropertyName
	 */
	public String getSeoTitleTextPropertyName() {
		return mSeoTitleTextPropertyName;
	}
	
	/**
	 * Sets the seo title text property name.
	 *
	 * @param pSeoTitleTextPropertyName the new seo title text property name
	 */
	public void setSeoTitleTextPropertyName(String pSeoTitleTextPropertyName) {
		this.mSeoTitleTextPropertyName = pSeoTitleTextPropertyName;
	}
	
	/**
	 * Gets the enable Inventory property name.
	 *
	 * @return the mEnableInventoryPropertyName
	 */
	public String getEnableInventoryPropertyName() {
		return mEnableInventoryPropertyName;
	}

	/**
	 * Sets the enable Inventory property name.
	 *
	 * @param pEnableInventoryPropertyName the enableInventoryPropertyName to set
	 */
	public void setEnableInventoryPropertyName(String pEnableInventoryPropertyName) {
		mEnableInventoryPropertyName = pEnableInventoryPropertyName;
	}
	/**
	 * Gets the enable payeezy property name.
	 *
	 * @return the mSeoMetaDescPropertyName
	 */
	public String getEnablePayeezyPropertyName() {
		return mEnablePayeezyPropertyName;
	}

	/**
	 * Sets the enable payeezy property name.
	 *
	 * @param pEnablePayeezyPropertyName the enablePayeezyPropertyName to set
	 */
	public void setEnablePayeezyPropertyName(String pEnablePayeezyPropertyName) {
		mEnablePayeezyPropertyName = pEnablePayeezyPropertyName;
	}
		
		/**
		 * Gets the seo meta desc property name.
		 *
		 * @return the mSeoMetaDescPropertyName
		 */
	public String getSeoMetaDescPropertyName() {
		return mSeoMetaDescPropertyName;
	}

	/**
	 * Sets the seo meta desc property name.
	 *
	 * @param pSeoMetaDescPropertyName the new seo meta desc property name
	 */
	public void setSeoMetaDescPropertyName(String pSeoMetaDescPropertyName) {
		this.mSeoMetaDescPropertyName = pSeoMetaDescPropertyName;
	}
	
	/**
	 * Gets the seo meta keyword property name.
	 *
	 * @return the mSeoMetaKeywordPropertyName
	 */
	public String getSeoMetaKeywordPropertyName() {
		return mSeoMetaKeywordPropertyName;
	}

	/**
	 * Sets the seo meta keyword property name.
	 *
	 * @param pSeoMetaKeywordPropertyName the new seo meta keyword property name
	 */
	public void setSeoMetaKeywordPropertyName(String pSeoMetaKeywordPropertyName) {
		this.mSeoMetaKeywordPropertyName = pSeoMetaKeywordPropertyName;
	}
	
	/**
	 * Gets the seo alt desc property name.
	 *
	 * @return the mSeoAltDescPropertyName
	 */
	public String getSeoAltDescPropertyName() {
		return mSeoAltDescPropertyName;
	}

	/**
	 * Sets the seo alt desc property name.
	 *
	 * @param pSeoAltDescPropertyName the new seo alt desc property name
	 */
	public void setSeoAltDescPropertyName(String pSeoAltDescPropertyName) {
		this.mSeoAltDescPropertyName = pSeoAltDescPropertyName;
	}
	
	/**
	 * Gets the article body property name.
	 *
	 * @return the mArticleBodyPropertyName
	 */
	public String getArticleBodyPropertyName() {
		return mArticleBodyPropertyName;
	}

	/**
	 * Sets the article body property name.
	 *
	 * @param pArticleBodyPropertyName the new article body property name
	 */
	public void setArticleBodyPropertyName(String pArticleBodyPropertyName) {
		this.mArticleBodyPropertyName = pArticleBodyPropertyName;
	}
	
	/**
	 * Gets the article item descriptor name.
	 *
	 * @return the mArticleItemDescriptorName
	 */
	public String getArticleItemDescriptorName() {
		return mArticleItemDescriptorName;
	}

	/**
	 * Sets the article item descriptor name.
	 *
	 * @param pArticleItemDescriptorName the new article item descriptor name
	 */
	public void setArticleItemDescriptorName(String pArticleItemDescriptorName) {
		this.mArticleItemDescriptorName = pArticleItemDescriptorName;
	}
	
	/**
	 * Gets the article name.
	 *
	 * @return the mArticleName
	 */
	public String getArticleName() {
		return mArticleName;
	}
	
	/**
	 * Sets the article name.
	 *
	 * @param pArticleName the new article name
	 */
	public void setArticleName(String pArticleName) {
		this.mArticleName = pArticleName;
	}

	/**
	 * Gets the user item descriptor name.
	 *
	 * @return the userItemDescriptorName
	 */
	public String getUserItemDescriptorName() {
		return mUserItemDescriptorName;
	}

	/**
	 * Sets the user item descriptor name.
	 *
	 * @param pUserItemDescriptorName the userItemDescriptorName to set
	 */
	public void setUserItemDescriptorName(String pUserItemDescriptorName) {
		mUserItemDescriptorName = pUserItemDescriptorName;
	}

	/**
	 * Gets the registration date property name.
	 *
	 * @return the registrationDatePropertyName
	 */
	public String getRegistrationDatePropertyName() {
		return mRegistrationDatePropertyName;
	}

	/**
	 * Sets the registration date property name.
	 *
	 * @param pRegistrationDatePropertyName the registrationDatePropertyName to set
	 */
	public void setRegistrationDatePropertyName(
			String pRegistrationDatePropertyName) {
		mRegistrationDatePropertyName = pRegistrationDatePropertyName;
	}

	/**
	 * Gets the description property name.
	 *
	 * @return the descriptionPropertyName
	 */
	public String getDescriptionPropertyName() {
		return mDescriptionPropertyName;
	}

	/**
	 * Sets the description property name.
	 *
	 * @param pDescriptionPropertyName the descriptionPropertyName to set
	 */
	public void setDescriptionPropertyName(String pDescriptionPropertyName) {
		mDescriptionPropertyName = pDescriptionPropertyName;
	}

	/**
	 * Gets the long description property name.
	 *
	 * @return the longDescriptionPropertyName
	 */
	public String getLongDescriptionPropertyName() {
		return mLongDescriptionPropertyName;
	}

	/**
	 * Sets the long description property name.
	 *
	 * @param pLongDescriptionPropertyName the longDescriptionPropertyName to set
	 */
	public void setLongDescriptionPropertyName(
			String pLongDescriptionPropertyName) {
		mLongDescriptionPropertyName = pLongDescriptionPropertyName;
	}


	/**
	 * Gets the parent categories property name.
	 *
	 * @return the parentCategoriesPropertyName
	 */
	public String getParentCategoriesPropertyName() {
		return mParentCategoriesPropertyName;
	}

	/**
	 * Sets the parent categories property name.
	 *
	 * @param pParentCategoriesPropertyName the parentCategoriesPropertyName to set
	 */
	public void setParentCategoriesPropertyName(
			String pParentCategoriesPropertyName) {
		mParentCategoriesPropertyName = pParentCategoriesPropertyName;
	}

	/**
	 * Gets the brand property name.
	 *
	 * @return the brandPropertyName
	 */
	public String getBrandPropertyName() {
		return mBrandPropertyName;
	}

	/**
	 * Sets the brand property name.
	 *
	 * @param pBrandPropertyName the brandPropertyName to set
	 */
	public void setBrandPropertyName(String pBrandPropertyName) {
		mBrandPropertyName = pBrandPropertyName;
	}

	/**
	 * Gets the first name property name.
	 *
	 * @return the firstNamePropertyName
	 */
	public String getFirstNamePropertyName() {
		return mFirstNamePropertyName;
	}

	/**
	 * Sets the first name property name.
	 *
	 * @param pFirstNamePropertyName the firstNamePropertyName to set
	 */
	public void setFirstNamePropertyName(String pFirstNamePropertyName) {
		mFirstNamePropertyName = pFirstNamePropertyName;
	}
		/**
	 * Gets the site id property name.
	 *
	 * @return the site id property name
	 */
	public String getSiteIdPropertyName() {
		return mSiteIdPropertyName;
	}

	/**
	 * Sets the site id property name.
	 *
	 * @param pSiteIdPropertyName the new site id property name
	 */
	public void setSiteIdPropertyName(String pSiteIdPropertyName) {
		mSiteIdPropertyName = pSiteIdPropertyName;
	}

	/**
	 * Gets the type property name.
	 *
	 * @return the type property name
	 */
	public String getTypePropertyName() {
		return mTypePropertyName;
	}

	/**
	 * Sets the type property name.
	 *
	 * @param pTypePropertyName the new type property name
	 */
	public void setTypePropertyName(String pTypePropertyName) {
		mTypePropertyName = pTypePropertyName;
	}

	/**
	 * Gets the vendor funded property name.
	 *
	 * @return the vendorFundedPropertyName
	 */
	public String getVendorFundedPropertyName() {
		return mVendorFundedPropertyName;
	}

	/**
	 * Sets the vendor funded property name.
	 *
	 * @param pVendorFundedPropertyName the vendorFundedPropertyName to set
	 */
	public void setVendorFundedPropertyName(String pVendorFundedPropertyName) {
		mVendorFundedPropertyName = pVendorFundedPropertyName;
	}

	/**
	 * Gets the tax code property name.
	 *
	 * @return the taxCodePropertyName
	 */
	public String getTaxCodePropertyName() {
		return mTaxCodePropertyName;
	}

	/**
	 * Sets the tax code property name.
	 *
	 * @param pTaxCodePropertyName the taxCodePropertyName to set
	 */
	public void setTaxCodePropertyName(String pTaxCodePropertyName) {
		mTaxCodePropertyName = pTaxCodePropertyName;
	}

	/**
	 * Gets the location item type.
	 *
	 * @return the locationItemType
	 */
	public String getLocationItemType() {
		return mLocationItemType;
	}

	/**
	 * Sets the location item type.
	 *
	 * @param pLocationItemType the locationItemType to set
	 */
	public void setLocationItemType(String pLocationItemType) {
		mLocationItemType = pLocationItemType;
	}

	/**
	 * Gets the city property name.
	 *
	 * @return the cityPropertyName
	 */
	public String getCityPropertyName() {
		return mCityPropertyName;
	}

	/**
	 * Sets the city property name.
	 *
	 * @param pCityPropertyName the cityPropertyName to set
	 */
	public void setCityPropertyName(String pCityPropertyName) {
		mCityPropertyName = pCityPropertyName;
	}

	/**
	 * Gets the state address property name.
	 *
	 * @return the stateAddressPropertyName
	 */
	public String getStateAddressPropertyName() {
		return mStateAddressPropertyName;
	}

	/**
	 * Sets the state address property name.
	 *
	 * @param pStateAddressPropertyName the stateAddressPropertyName to set
	 */
	public void setStateAddressPropertyName(String pStateAddressPropertyName) {
		mStateAddressPropertyName = pStateAddressPropertyName;
	}

	/**
	 * Gets the postal code property name.
	 *
	 * @return the postalCodePropertyName
	 */
	public String getPostalCodePropertyName() {
		return mPostalCodePropertyName;
	}

	/**
	 * Sets the postal code property name.
	 *
	 * @param pPostalCodePropertyName the postalCodePropertyName to set
	 */
	public void setPostalCodePropertyName(String pPostalCodePropertyName) {
		mPostalCodePropertyName = pPostalCodePropertyName;
	}

	/**
	 * Gets the user property name.
	 *
	 * @return the userPropertyName
	 */
	public String getUserPropertyName() {
		return mUserPropertyName;
	}

	/**
	 * Sets the user property name.
	 *
	 * @param pUserPropertyName the userPropertyName to set
	 */
	public void setUserPropertyName(String pUserPropertyName) {
		mUserPropertyName = pUserPropertyName;
	}

	/**
	 * Gets the site status property name.
	 *
	 * @return the siteStatusPropertyName
	 */
	public String getSiteStatusPropertyName() {
		return mSiteStatusPropertyName;
	}

	/**
	 * Sets the site status property name.
	 *
	 * @param pSiteStatusPropertyName the siteStatusPropertyName to set
	 */
	public void setSiteStatusPropertyName(String pSiteStatusPropertyName) {
		mSiteStatusPropertyName = pSiteStatusPropertyName;
	}

	/**
	 * Gets the color name property name.
	 *
	 * @return the color name property name
	 */
	public String getColorNamePropertyName() {
		return mColorNamePropertyName;
	}

	/**
	 * Sets the color name property name.
	 *
	 * @param pColorNamePropertyName the new color name property name
	 */
	public void setColorNamePropertyName(String pColorNamePropertyName) {
		mColorNamePropertyName = pColorNamePropertyName;
	}

	/**
	 * Gets the size description property name.
	 *
	 * @return the size description property name
	 */
	public String getSizeDescriptionPropertyName() {
		return mSizeDescriptionPropertyName;
	}

	/**
	 * Sets the size description property name.
	 *
	 * @param pSizeDescriptionPropertyName the new size description property name
	 */
	public void setSizeDescriptionPropertyName(String pSizeDescriptionPropertyName) {
		mSizeDescriptionPropertyName = pSizeDescriptionPropertyName;
	}

	/**
	 * Gets the geo code property name.
	 *
	 * @return the geoCodePropertyName
	 */
	public String getGeoCodePropertyName() {
		return mGeoCodePropertyName;
	}

	/**
	 * Sets the geo code property name.
	 *
	 * @param pGeoCodePropertyName the geoCodePropertyName to set
	 */
	public void setGeoCodePropertyName(String pGeoCodePropertyName) {
		mGeoCodePropertyName = pGeoCodePropertyName;
	}

	/**
	 * Gets the enterprise zone property name.
	 *
	 * @return the enterpriseZonePropertyName
	 */
	public String getEnterpriseZonePropertyName() {
		return mEnterpriseZonePropertyName;
	}

	/**
	 * Sets the enterprise zone property name.
	 *
	 * @param pEnterpriseZonePropertyName the enterpriseZonePropertyName to set
	 */
	public void setEnterpriseZonePropertyName(String pEnterpriseZonePropertyName) {
		mEnterpriseZonePropertyName = pEnterpriseZonePropertyName;
	}
	/**  Holds bpp mBppItemInfoPropertyName. */
	private String mBppItemInfoPropertyName;

	/**
	 * Gets the bpp item info property name.
	 *
	 * @return the bppItemInfoPropertyName
	 */
	public String getBppItemInfoPropertyName() {
		return mBppItemInfoPropertyName;
	}

	/**
	 * Sets the bpp item info property name.
	 *
	 * @param pBppItemInfoPropertyName the bppItemInfoPropertyName to set
	 */
	public void setBppItemInfoPropertyName(String pBppItemInfoPropertyName) {
		mBppItemInfoPropertyName = pBppItemInfoPropertyName;
	}

	/**
	 * Gets the store address2.
	 *
	 * @return the mStoreAddress2
	 */
	public String getStoreAddress2() {
		return mStoreAddress2;
	}

	/**
	 * Sets the store address2.
	 *
	 * @param pStoreAddress2 the new store address2
	 */
	public void setStoreAddress2(String pStoreAddress2) {
		this.mStoreAddress2 = pStoreAddress2;
	}

	/**
	 * Gets the store country.
	 *
	 * @return the mStoreCountry
	 */
	public String getStoreCountry() {
		return mStoreCountry;
	}

	/**
	 * Sets the store country.
	 *
	 * @param pStoreCountry the new store country
	 */
	public void setStoreCountry(String pStoreCountry) {
		this.mStoreCountry = pStoreCountry;
	}

	/**
	 * Gets the store phone number.
	 *
	 * @return the mStorePhoneNumber
	 */
	public String getStorePhoneNumber() {
		return mStorePhoneNumber;
	}

	/**
	 * Sets the store phone number.
	 *
	 * @param pStorePhoneNumber the new store phone number
	 */
	public void setStorePhoneNumber(String pStorePhoneNumber) {
		this.mStorePhoneNumber = pStorePhoneNumber;
	}

	/**
	 * @return the mParentProducts
	 */
	public String getParentProducts() {
		return mParentProducts;
	}

	/**
	 * @param pParentProducts the ParentProducts to set
	 */
	public void setParentProducts(String pParentProducts) {
		this.mParentProducts = pParentProducts;
	}
	
	/** The Billing address is same. */
	private String mBillingAddressIsSame;

	/**
	 * @return the billingAddressIsSame
	 */
	public String getBillingAddressIsSame() {
		return mBillingAddressIsSame;
	}

	/**
	 * @param pBillingAddressIsSame the billingAddressIsSame to set
	 */
	public void setBillingAddressIsSame(String pBillingAddressIsSame) {
		mBillingAddressIsSame = pBillingAddressIsSame;
	}
	
	/**
	 * Holds the mIsActivePropertyName value for Holiday Item
	 */
	private String mIsActivePropertyName;

	/**
	 * Holds the holidayDatePropertyName value for Holiday Item
	 */
	private String mHolidayDatePropertyName;
	
	
	/**
	 * 
	 * @return mHolidayDatePropertyName
	 */
	public String getHolidayDatePropertyName() {
		return mHolidayDatePropertyName;
	}

	/**
	 * 
	 * @param pHolidayDatePropertyName the holidayDatePropertyName to set
	 */
	public void setHolidayDatePropertyName(String pHolidayDatePropertyName) {
		this.mHolidayDatePropertyName = pHolidayDatePropertyName;
	}

	/**
	 * 
	 * @return mIsActivePropertyName
	 */
	public String getIsActivePropertyName() {
		return mIsActivePropertyName;
	}

	/**
	 * 
	 * @param pIsActivePropertyName the IsActivePropertyName to set
	 */
	public void setIsActivePropertyName(String pIsActivePropertyName) {
		this.mIsActivePropertyName = pIsActivePropertyName;
	}

	/**
	 * Gets the promotion details property name.
	 *
	 * @return the promotion details property name
	 */
	public String getPromotionDetailsPropertyName() {
		return mPromotionDetailsPropertyName;
	}

	/**
	 * Sets the promotion details property name.
	 *
	 * @param pPromotionDetailsPropertyName the new promotion details property name
	 */
	public void setPromotionDetailsPropertyName(String pPromotionDetailsPropertyName) {
		this.mPromotionDetailsPropertyName = pPromotionDetailsPropertyName;
	}
	
	private String mShipItemRelPropertyName;
	private String mFailedUpdateRegistryItem;

	/**
	 * @return the shipItemRelPropertyName
	 */
	public String getShipItemRelPropertyName() {
		return mShipItemRelPropertyName;
	}

	/**
	 * @param pShipItemRelPropertyName the shipItemRelPropertyName to set
	 */
	public void setShipItemRelPropertyName(String pShipItemRelPropertyName) {
		mShipItemRelPropertyName = pShipItemRelPropertyName;
	}

	/**
	 * @return the failedUpdateRegistryItem
	 */
	public String getFailedUpdateRegistryItem() {
		return mFailedUpdateRegistryItem;
	}

	/**
	 * @param pFailedUpdateRegistryItem the failedUpdateRegistryItem to set
	 */
	public void setFailedUpdateRegistryItem(String pFailedUpdateRegistryItem) {
		mFailedUpdateRegistryItem = pFailedUpdateRegistryItem;
	}
	
	/** The m gift wrap price. */
	private String mGiftWrapPrice;

	/**
	 * Gets the m gift wrap price.
	 *
	 * @return the m gift wrap price
	 */
	public String getGiftWrapPrice() {
		return mGiftWrapPrice;
	}

	/**
	 * Sets the m gift wrap price.
	 *
	 * @param pGiftWrapPrice the new m gift wrap price
	 */
	public void setGiftWrapPrice(String pGiftWrapPrice) {
		this.mGiftWrapPrice = pGiftWrapPrice;
	}
	
	/** The Registry failed item details. */
	private String mRegistryFailedItemDetails;
	
	/** The Failed item details. */
	private String mFailedItemDetails;
	
	/** The Order id prop name. */
	private String mOrderIdPropName;
	
	/** The Product id prop name. */
	private String mProductIdPropName;
	
	/** The Sku id prop name. */
	private String mSkuIdPropName;
	
	/** The Quantity prop name. */
	private String mQuantityPropName;
	
	/** The Registry failed item prop name. */
	private String mRegistryFailedItemPropName;
	
	/** The Registry id prop name. */
	private String mRegistryIdPropName;
	
	/** The Registry type prop name. */
	private String mRegistryTypePropName;
	
	/** The Date submitted prop name. */
	private String mDateSubmittedPropName;
	
	/** The Status prop name. */
	private String mStatusPropName;

	/**
	 * @return the registryFailedItemDetails
	 */
	public String getRegistryFailedItemDetails() {
		return mRegistryFailedItemDetails;
	}

	/**
	 * @param pRegistryFailedItemDetails the registryFailedItemDetails to set
	 */
	public void setRegistryFailedItemDetails(String pRegistryFailedItemDetails) {
		mRegistryFailedItemDetails = pRegistryFailedItemDetails;
	}

	/**
	 * @return the failedItemDetails
	 */
	public String getFailedItemDetails() {
		return mFailedItemDetails;
	}

	/**
	 * @param pFailedItemDetails the failedItemDetails to set
	 */
	public void setFailedItemDetails(String pFailedItemDetails) {
		mFailedItemDetails = pFailedItemDetails;
	}

	/**
	 * @return the orderIdPropName
	 */
	public String getOrderIdPropName() {
		return mOrderIdPropName;
	}

	/**
	 * @param pOrderIdPropName the orderIdPropName to set
	 */
	public void setOrderIdPropName(String pOrderIdPropName) {
		mOrderIdPropName = pOrderIdPropName;
	}

	/**
	 * @return the productIdPropName
	 */
	public String getProductIdPropName() {
		return mProductIdPropName;
	}

	/**
	 * @param pProductIdPropName the productIdPropName to set
	 */
	public void setProductIdPropName(String pProductIdPropName) {
		mProductIdPropName = pProductIdPropName;
	}

	/**
	 * @return the skuIdPropName
	 */
	public String getSkuIdPropName() {
		return mSkuIdPropName;
	}

	/**
	 * @param pSkuIdPropName the skuIdPropName to set
	 */
	public void setSkuIdPropName(String pSkuIdPropName) {
		mSkuIdPropName = pSkuIdPropName;
	}

	/**
	 * @return the quantityPropName
	 */
	public String getQuantityPropName() {
		return mQuantityPropName;
	}

	/**
	 * @param pQuantityPropName the quantityPropName to set
	 */
	public void setQuantityPropName(String pQuantityPropName) {
		mQuantityPropName = pQuantityPropName;
	}

	/**
	 * @return the registryFailedItemPropName
	 */
	public String getRegistryFailedItemPropName() {
		return mRegistryFailedItemPropName;
	}

	/**
	 * @param pRegistryFailedItemPropName the registryFailedItemPropName to set
	 */
	public void setRegistryFailedItemPropName(String pRegistryFailedItemPropName) {
		mRegistryFailedItemPropName = pRegistryFailedItemPropName;
	}

	/**
	 * @return the registryIdPropName
	 */
	public String getRegistryIdPropName() {
		return mRegistryIdPropName;
	}

	/**
	 * @param pRegistryIdPropName the registryIdPropName to set
	 */
	public void setRegistryIdPropName(String pRegistryIdPropName) {
		mRegistryIdPropName = pRegistryIdPropName;
	}

	/**
	 * @return the registryTypePropName
	 */
	public String getRegistryTypePropName() {
		return mRegistryTypePropName;
	}

	/**
	 * @param pRegistryTypePropName the registryTypePropName to set
	 */
	public void setRegistryTypePropName(String pRegistryTypePropName) {
		mRegistryTypePropName = pRegistryTypePropName;
	}

	/**
	 * @return the dateSubmittedPropName
	 */
	public String getDateSubmittedPropName() {
		return mDateSubmittedPropName;
	}

	/**
	 * @param pDateSubmittedPropName the dateSubmittedPropName to set
	 */
	public void setDateSubmittedPropName(String pDateSubmittedPropName) {
		mDateSubmittedPropName = pDateSubmittedPropName;
	}

	/**
	 * @return the statusPropName
	 */
	public String getStatusPropName() {
		return mStatusPropName;
	}

	/**
	 * @param pStatusPropName the statusPropName to set
	 */
	public void setStatusPropName(String pStatusPropName) {
		mStatusPropName = pStatusPropName;
	}
	
	private String mDateCreatedPropName;

	/**
	 * @return the dateCreatedPropName
	 */
	public String getDateCreatedPropName() {
		return mDateCreatedPropName;
	}

	/**
	 * @param pDateCreatedPropName the dateCreatedPropName to set
	 */
	public void setDateCreatedPropName(String pDateCreatedPropName) {
		mDateCreatedPropName = pDateCreatedPropName;
	}

	/**
	 * This method is used to get enableRadialPropertyName.
	 * @return enableRadialPropertyName String
	 */
	public String getEnableRadialPropertyName() {
		return mEnableRadialPropertyName;
	}

	/**
	 *This method is used to set enableRadialPropertyName.
	 *@param pEnableRadialPropertyName String
	 */
	public void setEnableRadialPropertyName(String pEnableRadialPropertyName) {
		mEnableRadialPropertyName = pEnableRadialPropertyName;
	}

	/**
	 * @return the mExpirationMonthPropName
	 */
	public String getExpirationMonthPropName() {
		return mExpirationMonthPropName;
	}

	/**
	 * @param pMExpirationMonthPropName the mExpirationMonthPropName to set
	 */
	public void setExpirationMonthPropName(String pMExpirationMonthPropName) {
		mExpirationMonthPropName = pMExpirationMonthPropName;
	}

	/**
	 * @return the mExpirationYearPropName
	 */
	public String getExpirationYearPropName() {
		return mExpirationYearPropName;
	}

	/**
	 * @param pMExpirationYearPropName the mExpirationYearPropName to set
	 */
	public void setExpirationYearPropName(String pMExpirationYearPropName) {
		mExpirationYearPropName = pMExpirationYearPropName;
	}

	/**
	 * @return the mNameOnCardPropName
	 */
	public String getNameOnCardPropName() {
		return mNameOnCardPropName;
	}

	/**
	 * @param pMNameOnCardPropName the mNameOnCardPropName to set
	 */
	public void setNameOnCardPropName(String pMNameOnCardPropName) {
		mNameOnCardPropName = pMNameOnCardPropName;
	}

	/**
	 * @return the mCreditCardTypePropName
	 */
	public String getCreditCardTypePropName() {
		return mCreditCardTypePropName;
	}

	/**
	 * @param pMCreditCardTypePropName the mCreditCardTypePropName to set
	 */
	public void setCreditCardTypePropName(String pMCreditCardTypePropName) {
		mCreditCardTypePropName = pMCreditCardTypePropName;
	}

	/**
	 * @return the mCreditCardNumberPropName
	 */
	public String getCreditCardNumberPropName() {
		return mCreditCardNumberPropName;
	}

	/**
	 * @param pMCreditCardNumberPropName the mCreditCardNumberPropName to set
	 */
	public void setCreditCardNumberPropName(String pMCreditCardNumberPropName) {
		mCreditCardNumberPropName = pMCreditCardNumberPropName;
	}

	/**
	 * @return the mSelectedCCNickNamePropName
	 */
	public String getSelectedCCNickNamePropName() {
		return mSelectedCCNickNamePropName;
	}

	/**
	 * @param pMSelectedCCNickNamePropName the mSelectedCCNickNamePropName to set
	 */
	public void setSelectedCCNickNamePropName(
			String pMSelectedCCNickNamePropName) {
		mSelectedCCNickNamePropName = pMSelectedCCNickNamePropName;
	}

	/**
	 * @return the mAllCreditCardsPropName
	 */
	public String getAllCreditCardsPropName() {
		return mAllCreditCardsPropName;
	}

	/**
	 * @param pMAllCreditCardsPropName the mAllCreditCardsPropName to set
	 */
	public void setAllCreditCardsPropName(String pMAllCreditCardsPropName) {
		mAllCreditCardsPropName = pMAllCreditCardsPropName;
	}

	/**
	 * Gets the qualifier column name.
	 *
	 * @return the qualifier column name
	 */
	public String getQualifierColumnName() {
		return mQualifierColumnName;
	}

	/**
	 * Sets the qualifier column name.
	 *
	 * @param pQualifierColumnName the new qualifier column name
	 */
	public void setQualifierColumnName(String pQualifierColumnName) {
		this.mQualifierColumnName = pQualifierColumnName;
	}

	/**
	 * Gets the no of items column name.
	 *
	 * @return the no of items column name
	 */
	public String getNoOfItemsColumnName() {
		return mNoOfItemsColumnName;
	}

	/**
	 * Sets the no of items column name.
	 *
	 * @param pNoOfItemsColumnName the new no of items column name
	 */
	public void setNoOfItemsColumnName(String pNoOfItemsColumnName) {
		this.mNoOfItemsColumnName = pNoOfItemsColumnName;
	}

	/**
	 * Gets the advanced item template property name.
	 *
	 * @return the advanced item template property name
	 */
	public String getAdvancedItemTemplatePropertyName() {
		return mAdvancedItemTemplatePropertyName;
	}

	/**
	 * Sets the advanced item template property name.
	 *
	 * @param pAdvancedItemTemplatePropertyName the new advanced item template property name
	 */
	public void setAdvancedItemTemplatePropertyName(String pAdvancedItemTemplatePropertyName) {
		this.mAdvancedItemTemplatePropertyName = pAdvancedItemTemplatePropertyName;
	}

	/**
	 * Gets the evaluation limit value column name.
	 *
	 * @return the evaluation limit value column name
	 */
	public String getEvaluationLimitValueColumnName() {
		return mEvaluationLimitValueColumnName;
	}

	/**
	 * Sets the evaluation limit value column name.
	 *
	 * @param pEvaluationLimitValueColumnName the new evaluation limit value column name
	 */
	public void setEvaluationLimitValueColumnName(String pEvaluationLimitValueColumnName) {
		this.mEvaluationLimitValueColumnName = pEvaluationLimitValueColumnName;
	}
	
	/** The Cardinal request id property name. */
	private String mCardinalRequestIdPropertyName;

	/**
	 * @return the cardinalRequestIdPropertyName
	 */
	public String getCardinalRequestIdPropertyName() {
		return mCardinalRequestIdPropertyName;
	}

	/**
	 * @param pCardinalRequestIdPropertyName the cardinalRequestIdPropertyName to set
	 */
	public void setCardinalRequestIdPropertyName(
			String pCardinalRequestIdPropertyName) {
		mCardinalRequestIdPropertyName = pCardinalRequestIdPropertyName;
	}
	
}
