package com.tru.commerce.locations;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;

/**
 * This class extends the OOTB DynamoServlet class.This class is used to get the cookie information.
 * @author PA
 * @version 1.0
 */
public class TRUGetCookieDroplet extends DynamoServlet {

	/**
	 * This method is used get the cookie information for the given cookie.
	 * 
	 * @param pRequest
	 *            - http request.
	 * @param pResponse
	 *            - http response.
	 * @throws ServletException
	 *             if an error occurs.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start : TRUGetCookieDroplet.service()");
		}
		final String cookieName = (String) pRequest.getLocalParameter(TRUCheckoutConstants.COOKIE_NAME);
		final String cookieValue = pRequest.getCookieParameter(cookieName);
		String locationId = null;
		if (StringUtils.isBlank(cookieValue)) {
			pRequest.serviceParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		} else {
			final String[] favStoreArry = StringUtils.splitStringAtCharacter(cookieValue, TRUCheckoutConstants.CHAR_OR);
			if (favStoreArry != null && favStoreArry.length >= TRUCheckoutConstants.INDEX_TWO) {
					locationId = favStoreArry[TRUCheckoutConstants.INT_ONE];
					pRequest.setParameter(TRUCheckoutConstants.LOCATION_ID, locationId);
			}
			pRequest.setParameter(TRUCheckoutConstants.COOKIE_VALUE, cookieValue);
			pRequest.serviceParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUGetCookieDroplet.service()");
		}
	}

}