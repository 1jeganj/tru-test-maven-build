package com.tru.commerce.locations;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUStoreServicesVO;
import com.tru.storelocator.cml.StoreLocatorConstants;

/**
 * This class is used to get the store details in the Json format.
 * @author PA
 * @version 1.0
 */
public class TRUStoreDetailsDroplet extends DynamoServlet {
	
	/** Constant for STORE_ID. */
	public static final String STORE_ID = "storeId";
	
	/** Constant for STORE_ITEM. */
	public static final String STORE_ITEM = "storeItem";
	
	/** Constant for SERVICES_JSONFORMAT. */
	public static final String SERVICES_JSONFORMAT = "returnStoreServicesInJsonFormat";
	
	/** Constant for OUTPUT. */
	public static final String OUTPUT = "output";
	
	/** Constant for STORE_SERVICES. */
	public static final String STORE_SERVICES = "storeServices";
	
	/** Constant for STORE_HOURS_MAP. */
	public static final String STORE_HOURS_MAP = "storeHoursMap";
	
	/** Constant for STORE_CLOSING_TIME. */
	public static final String STORE_CLOSING_TIME = "storeClosingTime";
	
	/** Constant for STORE_SERVICES_VO_LIST. */
	public static final String STORE_SERVICES_VO_LIST = "storeServicesVOList";
	
	/** Constant for APPLICATION_JSON. */
	private static final String APPLICATION_JSON = "application/json";
	
	
	/**
	 * Holds the mLocationTools.
	 */
	private TRUStoreLocatorTools mLocationTools;
	
	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools.
	 */
	public TRUStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}
	
	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools the locationTools to set
	 */
	public void setLocationTools(TRUStoreLocatorTools pLocationTools) {
		this.mLocationTools = pLocationTools;
	}	
	

	/**
	 * This method is used to get the store details in the Json format.
	 * It calls cookie manager & creates a cookie as myStore.
	 *  
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException 
	 * @throws IOException 
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreDetailsDroplet.service method Begins");
		}
		RepositoryItem storeItem = (RepositoryItem)pRequest.getObjectParameter(STORE_ITEM);
		String returnStoreServicesInJsonFormat = (String)pRequest.getLocalParameter(SERVICES_JSONFORMAT);
		if(storeItem == null){
			return ;
		}
		
		if(TRUConstants.TRUE.equalsIgnoreCase(returnStoreServicesInJsonFormat)){
			JSONArray jsonArray = getLocationTools().getStoreServicesInJsonFormat(storeItem);
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put(STORE_ID, (String)(getLocationTools().getPropertyValue(storeItem, getLocationTools().
						getLocationPropertyManager().getLocationIdPropertyName())) );
				jsonObj.put(STORE_SERVICES, jsonArray);
			} catch (JSONException e) {
				if(isLoggingError()){
					vlogError(e,"JSONException occurred while accessing the jsonObj.put() method ");
				}
			}
			setJsonDataToResponseObject(pRequest,pResponse,jsonObj);
			return ;
		}else{
			//This droplet will prapare the VO objects for the given store item
			Map<String,String> storeHoursMap = getLocationTools().getStoreWorkingHours(storeItem);
			String storeClosingTime = getLocationTools().getStoreClosingTime(storeHoursMap);
			List<TRUStoreServicesVO> storeServicesVOList = getLocationTools().getStoreServicesInVOFormat(storeItem);
			
			boolean isFavStoreEligible = getLocationTools().getFavStoreEligibility(storeItem);
			pRequest.setParameter(StoreLocatorConstants.IS_ELIGIBLE_FOR_FAV_STORE, isFavStoreEligible);
			//Get store hours
			boolean showTingsFromMonToFriInOneLine = getLocationTools().showTingsFromMonToFriInOneLine(storeItem);
			if(showTingsFromMonToFriInOneLine){
				//Prepare Mon-Fri day timings in one key value pair
				if(storeHoursMap != null && !storeHoursMap.isEmpty()){
					String monTimings = storeHoursMap.get(StoreLocatorConstants.WEEK_NAME_MON);
					Map<String,String> monToFriHoursMap = new LinkedHashMap<String,String>();
					monToFriHoursMap.put(StoreLocatorConstants.MON_FRI, monTimings);
					monToFriHoursMap.put(StoreLocatorConstants.WEEK_NAME_SAT, storeHoursMap.get(
							StoreLocatorConstants.WEEK_NAME_SAT));
					monToFriHoursMap.put(StoreLocatorConstants.WEEK_NAME_SUN, storeHoursMap.get(
							StoreLocatorConstants.WEEK_NAME_SUN));
					pRequest.setParameter(STORE_HOURS_MAP, monToFriHoursMap);
				}else{
					pRequest.setParameter(STORE_HOURS_MAP, storeHoursMap);
				}
				
			}else{
				pRequest.setParameter(STORE_HOURS_MAP, storeHoursMap);
			}
			
			pRequest.setParameter(STORE_CLOSING_TIME, storeClosingTime);
			pRequest.setParameter(STORE_SERVICES_VO_LIST, storeServicesVOList);
			
			pRequest.serviceParameter(OUTPUT, pRequest, pResponse);
		}
		
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreDetailsDroplet.service method Ends");
		}
					
	}
	
	/**
	 * This method is used to set the json data to the response object.
	 * 
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @param pJSONResponse - json response
	 * @throws ServletException - throws ServletException
	 * @throws IOException - throws IOException
	 */
	public void setJsonDataToResponseObject(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse,
			JSONObject pJSONResponse) throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreDetailsDroplet (setJsonDataToResponseObject)");
		}
		pResponse.setContentType(APPLICATION_JSON);
		PrintWriter out;
		out = pResponse.getWriter();
		out.print(pJSONResponse.toString());
		out.flush();
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreDetailsDroplet (setJsonDataToResponseObject)");
		}
	}
}
