package com.tru.commerce.locations;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.userprofiling.TRUCookieManager;

/**
 * This class is used to get the list of json stores map based on distance and locationResults.
 * The stores fetched from the repository are converted to an array of JSON objects and
 * sets the JSONArray as output param.
 *
 * @author PA.
 * @version 1.0
 */
public class TRUMakeMyStoreDroplet extends DynamoServlet {

	/** The Constant ADDRESS. */
	private static final String ADDRESS = "address";

	/** The Constant POSTAL_CODE. */
	private static final String POSTAL_CODE = "postalCode";
	/**
	 * holds the cookieManager.
	 */
	private TRUCookieManager mCookieManager;

	/**
	 * @return the cookieManager.
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * @param pCookieManager the cookieManager to set.
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		this.mCookieManager = pCookieManager;
	}

	/**
	 * This method used to make the selected store as my favorite store.
	 * It calls cookie manager & creates a cookie as myStore.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException - ServletException.
	 * @throws IOException - IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::GenerateJSONResponseDroplet.service");
		}
		String storeId = (String) pRequest.getLocalParameter(TRUCommerceConstants.STORE_ID);
		String storeName = (String) pRequest.getLocalParameter(TRUCommerceConstants.STORE_NAME);
		String address = (String) pRequest.getLocalParameter(ADDRESS);
		String postalCode = (String) pRequest.getLocalParameter(POSTAL_CODE);

		if(!StringUtils.isBlank(storeId) && !StringUtils.isBlank(address) && getCookieManager() != null) {
			getCookieManager().createMyStoreCookie(storeId, pRequest, pResponse);
			getCookieManager().createMyStoreAddressCookie(address, pRequest, pResponse);
			getCookieManager().createFavStoreCookie(postalCode+TRUCommerceConstants.PIPELINE+storeId, pRequest, pResponse);
			getCookieManager().createMyStoreNameCookie(storeName, pRequest, pResponse);
			// setting output param
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			// setting error param
			pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End::GenerateJSONResponseDroplet.service");
		}

	}

}
