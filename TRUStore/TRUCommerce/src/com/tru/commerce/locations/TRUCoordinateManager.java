package com.tru.commerce.locations;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import atg.commerce.locations.Coordinate;
import atg.commerce.locations.CoordinateManager;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.storelocator.cml.GeoLocatorConfigurations;
import com.tru.storelocator.cml.StoreLocatorConstants;

/**
 * This class is used to get the list of stores in a sorted manner  starting from nearest distance to the farthest.
 * 
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUCoordinateManager extends CoordinateManager {
	
	/**
	 * Holds the distanceinMiles.
	 */
	private boolean mDistanceinMiles;
		
	/**
	 * Holds the geoConfigurations.
	 */
	private GeoLocatorConfigurations mGeoConfigurations;
	
	/**
	 * Gets the geo configurations.
	 *
	 * @return the geoConfigurations.
	 */
	public GeoLocatorConfigurations getGeoConfigurations() {
		return mGeoConfigurations;
	}

	/**
	 * Sets the geo configurations.
	 *
	 * @param pGeoConfigurations the geoConfigurations to set.
	 */
	public void setGeoConfigurations(GeoLocatorConfigurations pGeoConfigurations) {
		mGeoConfigurations = pGeoConfigurations;
	}	

	/**
	 * Checks if is distancein miles.
	 *
	 * @return the distanceinMiles.
	 */
	public boolean isDistanceinMiles() {
		boolean miles= (mGeoConfigurations != null)?mGeoConfigurations.isDistanceinMiles():mDistanceinMiles;	
		return miles;
	}

	/**
	 * Sets the distancein miles.
	 *
	 * @param pDistanceinMiles the distanceinMiles to set.
	 */
	public void setDistanceinMiles(boolean pDistanceinMiles) {
		mDistanceinMiles = pDistanceinMiles;
	}
	
	/**
	 * This method used to make the filtering of stores by distance.
	 * It arranges the stores from nearest to farthest.
	 *  
	 * @param pItems RepositoryItem[].
	 * @param pCoordinate Coordinate.
	 * @param pDistance double.
	 * 
	 * @return filteredItems RepositoryItem[].
	 */
	protected RepositoryItem[] filterByDistance(RepositoryItem[] pItems, 
 Coordinate pCoordinate, double pDistance) {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUCoordinateManager.filterByDistance");
		}
		if (pDistance < TRUCheckoutConstants.MINIMUM_PRICE) {
			return pItems;
		}
		final List filteredItems = new ArrayList();
		for (RepositoryItem item : pItems) {
			Double distance = Double.valueOf(getDistanceBetweenCoordinates(pCoordinate, item));
			if (distance.doubleValue() < pDistance) {
				// If the distance is in miles, converting from miles to meters
				if (isDistanceinMiles()) {
					distance = convertMetersToMiles(distance);
				} else {
					// If isDistanceinMiles is false, converting from kilometers
					// to meters
					distance = convertKmsToMeters(distance);
				}
				final DecimalFormat df = new DecimalFormat(TRUCheckoutConstants.NUMBER_SYMBOL);
				distance = Double.valueOf(df.format(distance));
				if ((getDistancePropertyName() != null) && (item instanceof MutableRepositoryItem)) {
					((MutableRepositoryItem) item).setPropertyValue(getDistancePropertyName(), distance);
				}
				filteredItems.add(item);
			} else {
				vlogDebug("Filtered {0} for having a distance of: {1} which is greater than the max distance of: {2}",
						new Object[] { item.getItemDisplayName(), distance, Double.valueOf(pDistance) });
			}
		}

		if (filteredItems.isEmpty()) {
			return null;
		}
		if (isLoggingDebug()) {
			logDebug("End::TRUCoordinateManager.filterByDistance");
		}
		return (RepositoryItem[]) filteredItems.toArray(new RepositoryItem[0]);
	}
	
	/**
	 * This method is used to convert distance from Miles to Meters.
	 * @param pDistance Double.
	 * @return miles - distance in miles.
	 * 
	 */
	private double convertMetersToMiles(Double pDistance) {
		if (isLoggingDebug()) {
			logDebug("TRUCoordinateManager :convertMilesToMeters : Starts");
		}
		if (isLoggingDebug()) {
			logDebug("Distance in Miles: " + pDistance);
		}
		final double kms = pDistance / StoreLocatorConstants.KMS_TO_METER_CONVERTER;
		final double miles = kms * StoreLocatorConstants.KMS_TO_MILES_CONVERTER;
		if (isLoggingDebug()) {
			logDebug("Distance in Meters: " + miles);
		}
		if (isLoggingDebug()) {
			logDebug("TRUCoordinateManager :convertMilesToMeters : Ends");
		}
		return miles;
	}
	
	/**
	 * This method is used to convert distance from kiloMeters to Meters.
	 * 
	 * @param pDistance Double.
	 * @return miles - distance in miles.
	 */
	private double convertKmsToMeters(Double pDistance) {
		if (isLoggingDebug()) {
			logDebug("TRUCoordinateManager :convertKmsToMeters : Starts");
		}
		if (isLoggingDebug()) {
			logDebug("Distance in KiloMeters: "+pDistance);
		}
		final double meters = pDistance * StoreLocatorConstants.KMS_TO_METER_CONVERTER;
		if (isLoggingDebug()) {
			logDebug("Distance in Meters: "+meters);
		}
		if (isLoggingDebug()) {
			logDebug("TRUCoordinateManager :convertKmsToMeters : Ends");
		}
		return meters;
	}
}
