

package com.tru.commerce.locations;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.inventory.InventoryException;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * This class is TRUStoreAvailabilityDroplet.
 * @author PA
 * @version 1.0
 */
public class TRUStoreAvailabilityDroplet extends DynamoServlet {
  

  /** The input parameter name for the product and skuId to check. */
  public static final ParameterName LOCATION_ID = ParameterName.getParameterName("locationId");

  /**
   * Sku id parameter name.
   */
  public static final ParameterName SKU_ID = ParameterName.getParameterName("skuId");

  /** The output parameter name for the availabilityDate to set. */
  public static final String AVAILABILITY_DATE = "availabilityDate";

  /** The oparam name rendered once if the item is a preorderable item. */
  public static final String OPARAM_OUTPUT_PREORDERABLE = "preorderable";

  /** The oparam name rendered once if the item is not preorderable and is in stock. */
  public static final String OPARAM_OUTPUT_AVAILABLE = "available";

  /** The oparam name rendered once if the item is not preorderable, is not in stock  and is backorderable. */
  public static final String OPARAM_OUTPUT_BACKORDERABLE = "backorderable";

  /** The oparam name rendered once if the item is none of the above. */
  public static final String OPARAM_OUTPUT_UNAVAILABLE = "unavailable";

  /** The oparam name rendered once if the provided skuId can not be looked up in the inventory repository. */
  public static final String OPARAM_OUTPUT_ERROR = "error";

  /** The oparam name rendered once if none of the above open parameters exists. */
  public static final String OPARAM_OUTPUT_DEFAULT = "default";

  /**
   * Inventory manager.
   */
  private TRUCoherenceInventoryManager mInventoryManager;
  
  /** Property to hold storeAvailableMessage. */
  private String mStoreAvailableMessage;
  
  /** Property to hold storeNotAvailableMessage. */
  private String mStoreNotAvailableMessage;
  
  /** The m rest service. */
  private boolean mRestService;
  
  /** currentDate. */
  private CurrentDate mCurrentDate;
  
  /** Holds the mLocationTools. */
  private PARALStoreLocatorTools mLocationTools;
  
	/**
	 * Checks if is rest service.
	 *
	 * @return the mRestService.
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 *
	 * @param pRestService the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
   /**
    * Gets the store not available message.
    *
    * @return the storeNotAvailableMessage.
    */
	public String getStoreNotAvailableMessage() {
		return mStoreNotAvailableMessage;
	}
	
	/**
	 * Sets the store not available message.
	 *
	 * @param pStoreNotAvailableMessage the storeNotAvailableMessage to set.
	 */
	public void setStoreNotAvailableMessage(String pStoreNotAvailableMessage) {
		mStoreNotAvailableMessage = pStoreNotAvailableMessage;
	}

/**
 * Gets the store available message.
 *
 * @return the storeAvailableMessage.
 */
	public String getStoreAvailableMessage() {
		return mStoreAvailableMessage;
	}
	
	/**
	 * Sets the store available message.
	 *
	 * @param pStoreAvailableMessage the storeAvailableMessage to set.
	 */
	public void setStoreAvailableMessage(String pStoreAvailableMessage) {
		mStoreAvailableMessage = pStoreAvailableMessage;
	}
	
	/**
	 * Gets the inventory manager.
	 *
	 * @return the inventoryManager.
	 */
  public TRUCoherenceInventoryManager getInventoryManager() {
    return mInventoryManager;
  }

  /**
   * Sets the inventory manager.
   *
   * @param pInventoryManager - the inventoryManager to set.
   */
  public void setInventoryManager(TRUCoherenceInventoryManager pInventoryManager) {
    mInventoryManager = pInventoryManager;
  }
  
  
  
  /**
   * Gets the location tools.
   *
   * @return the locationTools.
   */
	public PARALStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}
	
	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools   the locationTools to set.
	 */
	public void setLocationTools(PARALStoreLocatorTools pLocationTools) {
		this.mLocationTools = pLocationTools;
	}	
	
	
  /**
   * Sets the current date.
   *
   * @param pCurrentDate Sets the CurrentDate component.
   */
  public void setCurrentDate(CurrentDate pCurrentDate) { 
    mCurrentDate = pCurrentDate; 
  }
  
  /**
   * Gets the current date.
   *
   * @return currentDate.
   */
  public CurrentDate getCurrentDate() { 
    return mCurrentDate; 
  }
  
  
  /**
   * Determines if the item is preorderable.
   * @param pRequest - http request.
   * @param pResponse - http response.
   * @throws ServletException if an error occurs.
   * @throws IOException if an error occurs.
   */
  public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
    throws ServletException, IOException {
    boolean handled = false;
    Object locationIdParam = null;
    String isReqFromAjax = (String)pRequest.getLocalParameter(TRUCheckoutConstants.IS_REQ_FROM_AJAX);
    
		if (pRequest.getContextPath().startsWith(TRUCheckoutConstants.REST_CONTEXT_PATH)) {
			isReqFromAjax=TRUCheckoutConstants.TRUE;
		}
    if(TRUCheckoutConstants.TRUE.equalsIgnoreCase(isReqFromAjax)){
    	final String locationIdFromJsp = (String)pRequest.getLocalParameter(TRUCheckoutConstants.LOCATION_ID);
    	if(!StringUtils.isBlank(locationIdFromJsp)){
    		locationIdParam = locationIdFromJsp;
    	}else{
    		//Get the locationId from the cookie
        	final String favStore = pRequest.getCookieParameter(TRUCheckoutConstants.FAV_STORE);
        	if(!StringUtils.isBlank(favStore)){
        		pRequest.setParameter(TRUCheckoutConstants.FAV_STORE, favStore);
        		final String[] favStoreArry = StringUtils.splitStringAtCharacter(favStore, TRUCheckoutConstants.CHAR_OR);
        		if(favStoreArry != null && favStoreArry.length >= TRUCheckoutConstants.INDEX_TWO){
        			locationIdParam = favStoreArry[TRUCheckoutConstants.INT_ONE];
        		}
        	}
    	}
    	
    }else{
    	locationIdParam = pRequest.getObjectParameter(LOCATION_ID);
    }
    
    final Object skuIdParam = pRequest.getObjectParameter(SKU_ID);

    // Check for valid input
    if (locationIdParam == null) {
      if (isLoggingDebug()) {
        logDebug("MISSING PARAM: no location id  supplied");
      }

      return;
    } else if (!(locationIdParam instanceof String)) {
      if (isLoggingDebug()) {
        logDebug("INCORRECT PARAM: location id argument not a string");
      }

      return;
    }

    if (skuIdParam == null) {
      if (isLoggingDebug()) {
        logDebug("MISSING PARAM: no sku id supplied");
      }

      return;
    } else if (!(skuIdParam instanceof String)) {
      if (isLoggingDebug()) {
        logDebug("INCORRECT PARAM: sku id argument is not a string");
      }

      return;
    }
    final TRUCoherenceInventoryManager invManager = getInventoryManager();

    // Call InventoryManager to do all the work and convert results into correctly rendered output
    try {
      int availability = invManager.queryAvailabilityStatus((String) skuIdParam, (String) locationIdParam);
      if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK){
    	  final TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getLocationTools());
    	  final RepositoryItem storeItem=locatorTools.getStoreItem(locationIdParam.toString());
    	  final Double storeItemWareHouseLocationId = (Double)locatorTools.getPropertyValue(storeItem, locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
          availability=invManager.queryAvailabilityStatus(skuIdParam.toString(),Long.toString(Math.round(storeItemWareHouseLocationId)));
      }
      if (availability == TRUCommerceConstants.INT_IN_STOCK) {
    	  setAvailMsg(isReqFromAjax,(String)locationIdParam,pRequest);
          handled = pRequest.serviceLocalParameter(OPARAM_OUTPUT_AVAILABLE, pRequest, pResponse);
      }else {
    	  setUnAvailMsg(isReqFromAjax,(String)locationIdParam,pRequest);
        // For store, default everything else to unavailable
        handled = pRequest.serviceLocalParameter(OPARAM_OUTPUT_UNAVAILABLE, pRequest, pResponse);
      }
    } catch (InventoryException iexc) {
    	if (isLoggingError()) {
			logError("InventoryException in TRUStoreAvailabilityDroplet.service", iexc);
		}
    	setUnAvailMsg(isReqFromAjax,(String)locationIdParam,pRequest);
      handled = pRequest.serviceLocalParameter(OPARAM_OUTPUT_ERROR, pRequest, pResponse);
    }
    if (!handled) {
    	setUnAvailMsg(isReqFromAjax,(String)locationIdParam,pRequest);
      handled = pRequest.serviceLocalParameter(OPARAM_OUTPUT_DEFAULT, pRequest, pResponse);
    }
  }
  
  /**

   * This method is used to add the unAvailMsg to the Request object.
   * @param pIsReqFromAjax - boolean property.
   * @param pLocationId - The storeId.
   * @param pRequest - The input request object.
   */
  public void setUnAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest){
	  if(isLoggingDebug()) {
		logDebug("Start : TRUStoreAvailabilityDroplet :setUnAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
	  }
	  final TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getLocationTools());
	  if(TRUCheckoutConstants.TRUE.equalsIgnoreCase(pIsReqFromAjax) && !StringUtils.isBlank(pLocationId)){
      	  
		//Get the store item
		  final RepositoryItem storeRepoItem = locatorTools.getStoreItem(pLocationId);
	      final String address1 = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
	    		  getAddress1PropertyName());
	      	
	      final String chainCode = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
	    		  getChainCodePropertyName());
	      if(StringUtils.isNotBlank(chainCode)){
	    	  final String chianName = locatorTools.getToysMap().get(chainCode.trim());
		      final StringBuilder builder = new StringBuilder();
		      builder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
	       	//Prepare the store availability message
      	if(!StringUtils.isBlank(address1)){
      		final String storeAvailMsg = StringUtils.replace(getStoreNotAvailableMessage(), TRUCheckoutConstants.ZERO_BRACES, 
      				builder.toString());
      		pRequest.setParameter(TRUCheckoutConstants.STORE_AVAI_MSG, storeAvailMsg);
      		pRequest.setParameter( TRUCheckoutConstants.ADDRESS_WITHCHAINNAME, builder);
      	}
      }
	  }
	  if(isLoggingDebug()) {
		logDebug("End : TRUStoreAvailabilityDroplet :setUnAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
	  }
  }
  
  /**
   * Sets the avail msg.
   *
   * @param pIsReqFromAjax - boolean property.
   * @param pLocationId - The storeId.
   * @param pRequest - The input request object.
   */
  public void setAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest){
	  if(isLoggingDebug()) {
		logDebug("Start : TRUStoreAvailabilityDroplet :setAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
	  }
	  final TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getLocationTools());
	  
	  if(TRUCheckoutConstants.TRUE.equalsIgnoreCase(pIsReqFromAjax) && !StringUtils.isBlank(pLocationId)){
      	//Get the store item
      	final RepositoryItem storeRepoItem = locatorTools.getStoreItem(pLocationId);
      	final String address1 = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
      			getAddress1PropertyName());
      	
      	final String chainCode = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
      			getChainCodePropertyName());
      	 if(StringUtils.isNotBlank(chainCode)){
      	final String chianName = locatorTools.getToysMap().get(chainCode.trim());
      	final StringBuilder builder = new StringBuilder();
      	builder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
      	
      	//Prepare the store availability message
      	if(!StringUtils.isBlank(address1)){
      		final String storeAvailMsg = StringUtils.replace(getStoreAvailableMessage(), TRUCheckoutConstants.ZERO_BRACES, builder.toString());
      		pRequest.setParameter(TRUCheckoutConstants.STORE_AVAI_MSG, storeAvailMsg);
      		pRequest.setParameter(TRUCheckoutConstants.ADDRESS_WITHCHAINNAME, builder);
      	}
      }
	  }
	  if(isLoggingDebug()) {
		logDebug("End : TRUStoreAvailabilityDroplet :setAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
	  }
  }
  
  
  
}
