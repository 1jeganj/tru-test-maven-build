package com.tru.commerce.locations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.commerce.inventory.InventoryException;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONObject;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUStoreItemVO;
import com.tru.common.vo.TRUStoreServicesVO;
import com.tru.resourcebundle.mgl.DefaultResourceBundleManager;
import com.tru.storelocator.cml.StoreLocatorConstants;
import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * The PARALStoreLocatorTools class is used to get interacts with location Repository.
 *
 * @author PA
 * @version 1.0
 */
public class TRUStoreLocatorTools extends PARALStoreLocatorTools{

	 /** Property To Hold stocklevel. */
	 private double mStocklevel;

	/** Property To Hold mStoreLocatorTools. */
		private PARALStoreLocatorTools mStoreLocatorTools;


	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;


	/** Property To Hold location repository. */
	private Repository mLocationRepository;

	/** Property To Hold commerce property manager. */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/**
	 * property to hold StoreItemDescriptor property name.
	 */
	private String mStoreItemDescriptorPropertyName;

	/** Property To Hold mResourceBundleManager. */
	private DefaultResourceBundleManager mResourceBundleManager;

	/** Property To Hold mCoherenceInventoryManager. */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;

	/** Property To Hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;


	/**
	 * @return the mCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the commerce property manager.
	 *
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets the commerce property manager.
	 *
	 * @param pCommercePropertyManager the new commerce property manager
	 */
	public void setCommercePropertyManager(
			TRUCommercePropertyManager pCommercePropertyManager) {
		this.mCommercePropertyManager = pCommercePropertyManager;
	}


	/**
	 * Gets the coherence inventory manager.
	 *
	 * @return the CoherenceInventoryManager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the coherence inventory manager.
	 *
	 * @param pCoherenceInventoryManager the CoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(
			TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 * Gets the resource bundle manager.
	 *
	 * @return the resourceBundleManager
	 */
	public DefaultResourceBundleManager getResourceBundleManager() {
		return mResourceBundleManager;
	}

	/**
	 * Sets the resource bundle manager.
	 *
	 * @param pResourceBundleManager the resourceBundleManager to set
	 */
	public void setResourceBundleManager(
			DefaultResourceBundleManager pResourceBundleManager) {
		mResourceBundleManager = pResourceBundleManager;
	}

	/**
	 * Gets the location repository.
	 *
	 * @return the mLocationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Sets the location repository.
	 *
	 * @param pLocationRepository the mLocationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	/**
	 * Gets the store item descriptor property name.
	 *
	 * @return the storeItemDescriptorPropertyName
	 */
	public String getStoreItemDescriptorPropertyName() {
		return mStoreItemDescriptorPropertyName;
	}

	/**
	 * Sets the store item descriptor property name.
	 *
	 * @param pStoreItemDescriptorPropertyName the storeItemDescriptorPropertyName to set
	 */
	public void setStoreItemDescriptorPropertyName(
			String pStoreItemDescriptorPropertyName) {
		mStoreItemDescriptorPropertyName = pStoreItemDescriptorPropertyName;
	}


	 /**
	  * Gets stocklevel
	 * @return mStockLevel
	 */
	public double getStocklevel() {
		return mStocklevel;
	}

	/**
	 * sets stock level parameter.
	 * @param pStocklevel Stocklevel
	 */
	public void setStocklevel(double pStocklevel) {
		mStocklevel = pStocklevel;
	}

	/**
	 * This method is used to get the onHandQty map for the given storeItems and skuId.
	 *
	 * @param pStoreItems - The store items
	 * @param pSkuId - The input skuId
	 * @return onHanandQtyMap - The onHandQty map
	 */
	public Map<String,TRUStoreItemVO> getOnHandQtyMap(Collection<RepositoryItem> pStoreItems,String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getOnHandQtyMap ()");
		}
		Map<String,TRUStoreItemVO> onHanandQtyMap = null;
		if(StringUtils.isBlank(pSkuId)){
			return null;
		}
		if(pStoreItems == null || pStoreItems.isEmpty()){
			return null;
		}
		final List<String> locationsIds = getLocationIds(pStoreItems);
		//get all items from the inventory manager
		Map<String,TRUInventoryInfo> inventoryInformationList=null;
		inventoryInformationList = getCoherenceInventoryManager().validateMultiStoreInventoryForStoreLocator(pSkuId, locationsIds);
		//List<RepositoryItem> invItemsList = getInventoryManager().getAllItems(pSkuId);

/*		if(inventoryInformationList == null || inventoryInformationList.isEmpty()){
			return null;
		}*/

		onHanandQtyMap = new HashMap<String,TRUStoreItemVO>();

		for(RepositoryItem storeItem : pStoreItems){
			if(storeItem == null){
				continue;
			}
			final TRUStoreItemVO storeItemVO = getOnHandQty(inventoryInformationList, storeItem,pSkuId);
			onHanandQtyMap.put(storeItem.getRepositoryId(), storeItemVO);

		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getOnHandQtyMap ()");
		}
		return onHanandQtyMap;
	}


	/**
	 * This method is used to get the list of location ids.
	 *
	 * @param pStoreItems - store item
	 * @return locationIds -  location ids
	 */
	private List<String> getLocationIds(Collection<RepositoryItem> pStoreItems) {
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getLocationIds ()");
		}
		List<String> locationIds = null;
		if (pStoreItems != null) {
			locationIds = new ArrayList<String>();
			for(RepositoryItem storeItem :pStoreItems) {
				if (storeItem != null) {
					final String locationId = (String) storeItem.getPropertyValue(
							getCommercePropertyManager().getLocationIdPropertyName());
					if(!StringUtils.isEmpty(locationId)) {
						locationIds.add(locationId);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getLocationIds ()");
		}
		return locationIds;
	}


	/**
	 * Gets the location ids for all stores.
	 *
	 * @return the location ids for all stores
	 */
	public List<String> getLocationIdsForAllStores(){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getLocationIdsForAllStores ()");
		}
		RepositoryView storView = null;
		RepositoryItem[] storeItems=null;
		try {
			storView = getLocationRepository().getView(getStoreItemDescriptorPropertyName());
			final QueryBuilder queryBuilder = storView.getQueryBuilder();
			final Query unconstrainedQuery=queryBuilder.createUnconstrainedQuery();
			storeItems=storView.executeQuery(unconstrainedQuery);
			if(null!=storeItems){
				final Collection<RepositoryItem> storeResults=  getEligibleStoreItems( Arrays.asList(storeItems));
				return getLocationIds(storeResults);
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException occured in TRUStoreLocatorTools.getLocationIdsForAllStores", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getLocationIdsForAllStores ()");
		}
		return null;
	}
	/**
	 * This method is used to get the onHandQty for the given store item and sku id.
	 *
	 * @param pInvInfos - The inventory items
	 * @param pStoreItem - The input store item
	 * @param pSkuId - The skuId
	 * @return onHanandQty - The onHandQty
	 */
	public TRUStoreItemVO getOnHandQty(Map<String,TRUInventoryInfo> pInvInfos,RepositoryItem pStoreItem,String pSkuId) {
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getOnHandQty ()");
		}
		final TRUStoreItemVO storeItemVO = new TRUStoreItemVO();
		try{
		storeItemVO.setStoreItemRepositoryId(pStoreItem.getRepositoryId());
		storeItemVO.setSkuId(pSkuId);

		final String storeItemLocationId = (String)pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationIdPropertyName());
		if(pInvInfos.containsKey(storeItemLocationId)){
			storeItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
			storeItemVO.setDelivery(StoreLocatorConstants.SAME_DAY);
			storeItemVO.setOnHandQty(pInvInfos.get(storeItemLocationId).getQuantity());
		}else{
			Double storeItemWareHouseLocationId = (Double)pStoreItem.getPropertyValue(getLocationPropertyManager().
					getWarehouseLocationCodePropertyName());
			if(storeItemWareHouseLocationId == null){
				 storeItemVO.setInventoryItemStatus(TRUCommerceConstants.OUT_OF_STOCK);
			}
			else {
				 TRUInventoryInfo invInfo = getCoherenceInventoryManager().queryInventoryInfo(storeItemVO.getSkuId(),Long.toString(Math.round(storeItemWareHouseLocationId)));
				 if(isLoggingDebug()){
					 logDebug("Inventory qunatity available -- "+invInfo.getQuantity());
				 }
				 if(invInfo.getQuantity() > 0l){
					 storeItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
					 storeItemVO.setWareHouseInventoryFlag(true);
					 storeItemVO.setOnHandQty(invInfo.getQuantity());
				 }else{
					 storeItemVO.setInventoryItemStatus(TRUCommerceConstants.OUT_OF_STOCK);
					 storeItemVO.setOnHandQty(invInfo.getQuantity());
				}
			}
		}
		if(storeItemVO.getInventoryItemStatus()!=null && storeItemVO.getInventoryItemStatus().equalsIgnoreCase(
				TRUCommerceConstants.IN_STOCK)){
			storeItemVO.setDelivery(StoreLocatorConstants.NEXT_DAY);
		}else{
			storeItemVO.setDelivery(StoreLocatorConstants.EMPTY_STRING);
		}

		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getOnHandQty ()");
		}
		}catch(InventoryException exception){
			if (isLoggingError()) {
				logError("InventoryException occured", exception);
			}
		}
		return storeItemVO;
	}

	/**
	 * This method used to populate the Json object with the given store items.
	 * @param pLocationItems - The store items
	 * @param pDistance - The distance
	 * @param pSkuId - The skuId
	 * @param pMyStoreId - The favStore id
	 * @return storeArray - The store results array
	 */
	public JSONArray generateJSONResponse(Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId,String pMyStoreId){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.generateJSONResponse(Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId) ");
		}
		JSONObject storeArray[] = null;
		final List<JSONObject> jsonObjectList = new ArrayList<JSONObject>();

		//Filter the items based on the Store End Date
		Collection<RepositoryItem> lLocationItems = pLocationItems;
		lLocationItems = getEligibleStoreItems(pLocationItems);

		if(lLocationItems != null & !lLocationItems.isEmpty()){

			 //storeArray = new JSONObject[pLocationItems.size()];
			 RepositoryItem storeItem = null;
				Iterator<RepositoryItem> locateList = null;
				TRUStoreItemVO storeItemVO=null;
					if(lLocationItems != null){
						//Check the store item eligibility from the inventory repository
						final Map<String,TRUStoreItemVO> onHandQtyMap = getOnHandQtyMap(lLocationItems, pSkuId);
						locateList = lLocationItems.iterator();
						while(locateList.hasNext()){
							storeItem = (RepositoryItem) locateList.next();
							if(storeItem != null){
								//TODO : Check the storeItem eligibility from the Location repository

								if(isEnableOnHandQtyEligibility() && (onHandQtyMap == null ||
										onHandQtyMap.get(storeItem.getRepositoryId()) == null ||
										onHandQtyMap.get(
												storeItem.getRepositoryId()).getInventoryItemStatus() != TRUCommerceConstants.IN_STOCK )){
									//Check the store item eligibility from the inventory repository
									continue;
								}
								if(onHandQtyMap!=null){
									storeItemVO = onHandQtyMap.get(storeItem.getRepositoryId());
								}
								final double dist = convertMetersToMiles(pDistance);
								final Long distance = Math.round(dist);

								jsonObjectList.add(new JSONObject(populateStoreItemToMap(storeItem, getLocationConfiguration().
										getZoomLevelMap().get(distance.toString()),pSkuId,storeItemVO)));

							}
						}

					}
			}
		storeArray = jsonObjectList.toArray(new JSONObject[jsonObjectList.size()]);
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.generateJSONResponse(Collection<RepositoryItem> pLocationItems,Double pDistance,String pSkuId) ");
		}
		return new JSONArray(storeArray);

	}


	/**
	 * This method is used to populate the store item map.
	 * @param pStoreItem - The store item
	 * @param pZoomLevel - The zoom level
	 * @param pSkuId - The skuId
	 * @param pTRUStoteItemVO - The StoteItemVO
	 * @return storeItemMap - The store item map
	 */
	public Map<String, Object> populateStoreItemToMap(
			RepositoryItem pStoreItem, String pZoomLevel,String pSkuId,TRUStoreItemVO pTRUStoteItemVO) {
		if (isLoggingDebug()) {
			logDebug("Start : populateStoreItemToMap(RepositoryItem pStoreItem, String pZoomLevel,String pSkuId,TRUStoreItemVO pTRUStoteItemVO) ");
		}
		if(StringUtils.isBlank(pSkuId)){
			return null;
		}
		final Map<String, Object> storeItemMap = new HashMap<String, Object>();
		RepositoryItem skuItem = null;
		Boolean unCartable = Boolean.FALSE;
		try
		{
			skuItem=getCatalogTools().findSKU(pSkuId);
		}
		catch(RepositoryException e) {
			if(isLoggingError()){
				vlogError(e,"RepositoryException occurred getMsgOnPdpSkuFlag method skuItem {0}  is ",skuItem);
			}
		}
		if(skuItem!=null && skuItem.getPropertyValue(getCommercePropertyManager().getUnCartable())!=null)
		{
		 unCartable = (boolean)skuItem.getPropertyValue(getCommercePropertyManager().getUnCartable());
		}
		if (null != getLocationPropertyManager()) {
			storeItemMap.put(getLocationPropertyManager().getStoreNamePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreNamePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getLatitudePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getLatitudePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getLongitudePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getLongitudePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getPhoneNumberPropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getPhoneNumberPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getCityPropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getCityPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getStateAddressPropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getStateAddressPropertyName()));
			storeItemMap.put(getLocationPropertyManager().getPostalCodePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getPostalCodePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getTypePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getTypePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getChainCodePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getChainCodePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getStoreDistancePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreDistancePropertyName()));
			storeItemMap.put(getLocationPropertyManager().getWarehouseLocationCodePropertyName(),
					pStoreItem.getPropertyValue(getLocationPropertyManager().getWarehouseLocationCodePropertyName()));
				Double doubleObj = (Double)pStoreItem.getPropertyValue(getLocationPropertyManager().getStoreDistancePropertyName());
			if(doubleObj == null){
				doubleObj = StoreLocatorConstants.DOUBLE_ZERO;
			}
			storeItemMap.put(StoreLocatorConstants.DISTANCE_IN_MILES, String.valueOf(doubleObj));
			if (!StringUtils.isBlank(pZoomLevel)) {
				storeItemMap.put(StoreLocatorConstants.ZOOM_LEVEL, pZoomLevel);
			}
			final String address1 = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getAddress1PropertyName());
			final String address2 = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getAddress2PropertyName());
			String storeAddress = null;
			if (!StringUtils.isBlank(address2)) {
				storeAddress = new StringBuilder(address1).append(StoreLocatorConstants.SPACE).append(address2).toString();
			} else {
				storeAddress = address1;
			}
			storeItemMap.put(StoreLocatorConstants.STREET_ADDRESS, storeAddress);
			String storeId = (String) pStoreItem.getPropertyValue(getLocationPropertyManager().getLocationIdPropertyName());
			storeItemMap.put(StoreLocatorConstants.STORE_ID, storeId);
			storeItemMap.put(StoreLocatorConstants.SKU_ID, pSkuId);
			storeItemMap.put(getCommercePropertyManager().getUnCartable(), unCartable);
			if(pTRUStoteItemVO != null){
				storeItemMap.put(StoreLocatorConstants.ON_HAND_QTY, pTRUStoteItemVO.getOnHandQty());
				storeItemMap.put(StoreLocatorConstants.DELIVERY, pTRUStoteItemVO.getDelivery());
				storeItemMap.put(StoreLocatorConstants.STORE_ITEM_REPO_ID, pTRUStoteItemVO.getStoreItemRepositoryId());
			}
			//populate the store working hours
			final Map<String,String> storeHoursMap = getStoreWorkingHours(pStoreItem);
			if(storeHoursMap == null || storeHoursMap.isEmpty()){
				storeItemMap.put(StoreLocatorConstants.STORE_HOURS, new JSONObject());
			}else{
				storeItemMap.put(StoreLocatorConstants.STORE_HOURS, new JSONObject(storeHoursMap));
			}
			//storeItemMap.put(StoreLocatorConstants.STORE_HOURS, getStoreWorkingHoursInJsonFormat(pStoreItem));
			final String closingTimeMsg = getStoreClosingTime(storeHoursMap);
			storeItemMap.put(StoreLocatorConstants.CLOSING_TIME_MSG, closingTimeMsg);
			storeItemMap.put(StoreLocatorConstants.STORE_SERVICES, getStoreServicesInJsonFormat(pStoreItem));
			storeItemMap.put(StoreLocatorConstants.IS_ELIGIBLE_FOR_FAV_STORE, getFavStoreEligibility(pStoreItem));
				//Add Store hours msg
			storeItemMap.put(StoreLocatorConstants.STORE_HOURS_MSG, getStoreHoursMsg(pStoreItem,pTRUStoteItemVO));

			storeItemMap.put(StoreLocatorConstants.SHOW_TIMINGS_FROM_MON_TO_FRI_IN_ONE_LINE, showTingsFromMonToFriInOneLine(pStoreItem));

			storeItemMap.put(StoreLocatorConstants.IS_SUPER_STORE_SERVICE_AVAIL,
					isTheGivenServiceConfiguredForGivenStore(pStoreItem,getSuperStoreServiceName()));

			final Date stoteItemEndDate = (Date)pStoreItem.getPropertyValue(getLocationPropertyManager().getEndDatePropertyName());
			storeItemMap.put(StoreLocatorConstants.STORE_END_DATE, stoteItemEndDate);
		}
		if (isLoggingDebug()) {
			logDebug("End : populateStoreItemToMap(RepositoryItem pStoreItem, String pZoomLevel,String pSkuId,TRUStoreItemVO pTRUStoteItemVO) ");
		}
		return storeItemMap;
	}

	/**
	 * This method is used to get store item for the given storeId.
	 * @param pStoreId - The storeId
	 * @return object - The store item
	 */
	public RepositoryItem getStoreItem(String pStoreId){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getStoreItem(String pStoreId)");
		}
		if(StringUtils.isBlank(pStoreId)){
			return null;
		}
		RepositoryView locationRepositoryView = null;
		RepositoryItem[] invItems=null;
		List<RepositoryItem> invItemsList = null;
		RepositoryItem storeItem = null;
		try {
			locationRepositoryView = getLocationRepository().getView(StoreLocatorConstants.STORE);
			final QueryBuilder queryBuilder = locationRepositoryView.getQueryBuilder();

			final QueryExpression locationIdPropertyExpression = queryBuilder.createPropertyQueryExpression(getLocationPropertyManager().
					getLocationIdPropertyName());
			final QueryExpression locationIdConstantExpression = queryBuilder.createConstantQueryExpression(pStoreId);

			final Query locationIdQuery=queryBuilder.createComparisonQuery(locationIdPropertyExpression, locationIdConstantExpression,
					QueryBuilder.EQUALS);

			invItems = locationRepositoryView.executeQuery(locationIdQuery);

			if(invItems != null && invItems.length > TRUConstants.ZERO){
				invItemsList = Arrays.asList(invItems);
				storeItem = invItemsList.get(StoreLocatorConstants.INT_ZERO);
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException occured", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getStoreItem(String pStoreId)");
		}
		return storeItem;
	}

	/**
	 * This method is used to get the property value for the given store item and property name.
	 * @param pStoreItem - The store item
	 * @param pPropertyName - The property name
	 * @return object - The property value object
	 */
	public Object getPropertyValue(RepositoryItem pStoreItem,String pPropertyName){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getPropertyValue(RepositoryItem pStoreItem,String pPropertyName)");
		}
		if(pStoreItem == null){
			return null;
		}
		final Object object = pStoreItem.getPropertyValue(pPropertyName);
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getPropertyValue(RepositoryItem pStoreItem,String pPropertyName)");
		}
		return object;
	}

	/**
	 * This method is used to get the store Hours Msg for the given store item.
	 * @param pStoreItem - The store item
	 * @param pTRUStoteItemVO - The stoteItemVO
	 * @return storeHoursMsg - The storeHoursMsg
	 */
	@SuppressWarnings("unchecked")
	public String getStoreHoursMsg(RepositoryItem pStoreItem,TRUStoreItemVO pTRUStoteItemVO){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getStoreHoursMsg(RepositoryItem pStoreItem,TRUStoreItemVO pTRUStoteItemVO)");
		}
		if(pStoreItem == null){
			return null;
		}
		if(pTRUStoteItemVO == null){
			return null;
		}
		boolean storePickupEligible = false;
		boolean ShipToStoreEligible = false;
		String storeHoursMsg = null;
		String keyName = null;
		Boolean stockFromDC = pTRUStoteItemVO.isWareHouseInventoryFlag();
		if(pTRUStoteItemVO.getInventoryItemStatus()!=null && !TRUCommerceConstants.IN_STOCK.equalsIgnoreCase(pTRUStoteItemVO.getInventoryItemStatus())){
			//out of stock error msg from the Resource Bundle Repository
			return getOutStockMsg();
		}else
		{
			RepositoryItem skuItem = null;
			String ispu = null;
			try
			{
				skuItem=getCatalogTools().findSKU(pTRUStoteItemVO.getSkuId());
			}
			catch(RepositoryException e) {
				if(isLoggingError()){
					vlogError(e,"RepositoryException occurred getStoreHoursMsg method skuItem {0}  is ",skuItem);
				}
			}


			if (skuItem != null) {
				ShipToStoreEligible = (Boolean) skuItem.getPropertyValue(getCommercePropertyManager()
						.getShipToStoreEligible());
				if (skuItem.getPropertyValue(getCommercePropertyManager().getItemInStorePickUp()) != null) {
					ispu = (String) skuItem.getPropertyValue(getCommercePropertyManager().getItemInStorePickUp());
				}

				if (ispu != null && ispu.contains(TRUCommerceConstants.YES)) {
					storePickupEligible = true;
				}
			}
		}
		
		if ((storePickupEligible || ShipToStoreEligible) && !stockFromDC
				&& TRUCommerceConstants.IN_STOCK.equalsIgnoreCase(pTRUStoteItemVO.getInventoryItemStatus())) {
			// Ready in an hour
			keyName = getStoreHoursMsgList().get(StoreLocatorConstants.INT_ZERO);
		} else if ((storePickupEligible || ShipToStoreEligible)
				&& TRUCommerceConstants.IN_STOCK.equalsIgnoreCase(pTRUStoteItemVO.getInventoryItemStatus())) {
			// Free store pickup available in 5-10 days
			keyName = getStoreHoursMsgList().get(StoreLocatorConstants.INT_ONE);
		} else {
			// out of stock
			keyName = getStoreHoursMsgList().get(StoreLocatorConstants.INT_TWO);
		}
		
		try {
			storeHoursMsg = getResourceBundleManager().getResourceBundleTools().getKeyValue(keyName, null);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError(e,"RepositoryException occurred while accessing the getResourceBundleManager()."
						+ "getResourceBundleTools().getKeyValue(keyName, null) for the storeitem : {0} ",pStoreItem);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End : TRUStoreLocatorTools.getStoreHoursMsg(RepositoryItem pStoreItem,TRUStoreItemVO pTRUStoteItemVO)");
		}
		return storeHoursMsg;

	}

	/**
	 * This method is used to get out of stock messages for store.
	 * @return  outStockMsg - The out of stock message for store
	 */
	private String getOutStockMsg() {
		String outStockMsg = null;
		try {

			outStockMsg = getResourceBundleManager().getResourceBundleTools().getKeyValue(getStoreHoursMsgList().
					get(StoreLocatorConstants.INT_TWO), null);
		} catch (RepositoryException e) {
			if(isLoggingDebug()){
				vlogDebug(e,"RepositoryException occurred while accessing the ResourceBundle Key : storelocator.itemOutOfStock ");
			}
			if (isLoggingError()) {
				logError("RepositoryException occurred", e);
			}
		}
		return outStockMsg;
	}

	/**
	 * This method is used to get the store services in the VO format for the given storeItem.
	 * @param pStoreItem - The store item
	 * @return storeServicesVOList - The store services VO List
	 */
	public List<TRUStoreServicesVO> getStoreServicesInVOFormat(RepositoryItem pStoreItem){
		if (isLoggingDebug()) {
			logDebug("Start : TRUStoreLocatorTools.getStoreServicesInVOFormat(RepositoryItem pStoreItem)");
		}
		final List<TRUStoreServicesVO> storeServicesVOList = new ArrayList<TRUStoreServicesVO>();
		if(pStoreItem == null){
			if(isLoggingWarning()){
				logWarning("The given StoreItem is NULL");
			}
			return storeServicesVOList;
		}
		final List<RepositoryItem> serviceRepoItems = (List)(pStoreItem.getPropertyValue(
				getLocationPropertyManager().getLocationServicesPropertyName()));
		if(serviceRepoItems == null || serviceRepoItems.isEmpty()){
			return storeServicesVOList;
		}
		for(RepositoryItem serviceItem : serviceRepoItems){
			if(serviceItem == null){
				continue;
			}
			String serviceName=(String) serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName());
			List<String> serviceNameList = getServiceNameList();
			if(!serviceNameList.contains(serviceName)){
				if (isLoggingDebug()) {
					vlogDebug("The excluded serviceName is : ",serviceName);
				}
				continue;
			}
			final TRUStoreServicesVO servicesVO = new TRUStoreServicesVO();
			servicesVO.setServiceName((String)serviceItem.getPropertyValue(getLocationPropertyManager().getServiceNamePropertyName()));
			servicesVO.setServiceDesc((String)serviceItem.getPropertyValue(
					getLocationPropertyManager().getServiceDescriptionPropertyName()));
			servicesVO.setLearnMore((String)serviceItem.getPropertyValue(getLocationPropertyManager().getLearnMorePropertyName()));
			storeServicesVOList.add(servicesVO);
		}
		if (isLoggingDebug()) {
			vlogDebug("The storeServicesVOList value is : {0} for the given item is : {1}",storeServicesVOList,pStoreItem);
			logDebug("End : TRUStoreLocatorTools.getStoreServicesInVOFormat(RepositoryItem pStoreItem)");
		}
		return storeServicesVOList;
	}

	/**
	 * @return the mShoppingCartUtils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * @param pShoppingCartUtils the mShoppingCartUtils to set
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * @return the mStoreLocatorTools
	 */
	public PARALStoreLocatorTools getStoreLocatorTools() {
		return mStoreLocatorTools;
	}

	/**
	 * @param pStoreLocatorTools the mStoreLocatorTools to set
	 */
	public void setStoreLocatorTools(PARALStoreLocatorTools pStoreLocatorTools) {
		this.mStoreLocatorTools = pStoreLocatorTools;
	}

}