package com.tru.commerce.locations;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUCookieManager;


/**
 * This class is used to get the list of json stores map based on distance and locationResults.
 * The stores fetched from the repository are converted to an array of JSON objects and 
 * sets the JSONArray as output param.
 *
 * @author PA.
 * @version 1.0
 */
public class TRUMyStoreDroplet extends DynamoServlet {
	
	/**
	 * hold constant MY_STORE_ADDRESS.
	 */
	private static final String MY_STORE_ADDRESS = "myStoreAddress";
	/**
	 * holds the cookieManager.
	 */
	private TRUCookieManager mCookieManager;
	
	/**
	 * @return the cookieManager.
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}

	/**
	 * @param pCookieManager the cookieManager to set.
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		this.mCookieManager = pCookieManager;
	}

	/**
	 * This method used to make the selected store as my favorite store.
	 * It calls cookie manager & creates a cookie as myStore.
	 *  
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUMyStoreDroplet.service method Begins");
		}
		String storeAddress = pRequest
				.getCookieParameter(TRUConstants.MY_STORE_ADDRESS_COOKIE);
		if (StringUtils.isBlank(storeAddress)) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM,
					pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("@Class::TRUMyStoreDroplet::@method::service() : decryptedCookie is null");
			}

		} else {
			// setting parameter
			pRequest.setParameter(MY_STORE_ADDRESS, storeAddress);
			// setting output param
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM,
					pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("@Class::TRUMyStoreDroplet::@method::service() : decryptedCookie is"
						+ storeAddress);
			}
		}
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUMyStoreDroplet.service method Ends");
		}

	}
	
}
