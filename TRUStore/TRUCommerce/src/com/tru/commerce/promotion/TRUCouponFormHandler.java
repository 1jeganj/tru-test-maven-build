package com.tru.commerce.promotion;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.order.Order;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.CouponFormHandler;
import atg.commerce.promotion.PromotionException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.order.TRUOrderHolder;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.integrations.sucn.common.SUCNIntegrationException;
import com.tru.integrations.sucn.couponlookup.TRUSUCNLookupService;
import com.tru.integrations.sucn.couponlookup.response.CouponLookupResponse;
import com.tru.preview.TRUSchedulableDate;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;

/**
 * TRUCouponFormHandler class for managing the coupon related operation Extensions to CouponFormHandler.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUCouponFormHandler extends CouponFormHandler {
	/** Number regex. */
	public static final String NUMBER_REGEX = "^[0-9]+";
	
	/** Static constant for 20. */
	private  static final int INT_TWENTY = 20;
	/** ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();
	
	/**Holds the  mSchedulableDate. */
	private TRUSchedulableDate mSchedulableDate;
	
	/**
	 * @return the schedulableDate
	 */
	public TRUSchedulableDate getSchedulableDate() {
		return mSchedulableDate;
	}
	/**
	 * @param pSchedulableDate the schedulableDate to set
	 */
	public void setSchedulableDate(TRUSchedulableDate pSchedulableDate) {
		mSchedulableDate = pSchedulableDate;
	}
	/**
	 * property to hold mTRUSUCNLookupService.
	 */
	private TRUSUCNLookupService mSucnLookupService;

	/**
	 * property to hold mOrderId.
	 */
	private String mOrderId;

	/**
	 * This holds the value for single use coupon code entered by customer.
	 */
	private String mSingleUseCouponCode;

	/**
	 * property to hold mUserPricingModels.
	 */
	private PricingModelHolder mUserPricingModels;

	/**
	 * property to hold mShoppingCart.
	 */
	private TRUOrderHolder mShoppingCart;

	/**
	 * property to hold mPurchaseProcessHelper.
	 */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;
	/**
	 * property to hold RepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	/**
	 * property to hold SuccessErrorUrlMap.
	 */
	private Map<String, String> mSuccessErrorUrlMap;

	/**
	 * property to hold mCouponConfiguration.
	 */
	private TRUConfiguration mCouponConfiguration;

	/**
	 * property to hold ErrorHandler.
	 */
	private IErrorHandler mErrorHandler;

	/**
	 * property to hold OrderManager.
	 */
	private TRUOrderManager mOrderManager;
	
	/** Field hold displayName. */
	private String mDisplayName;
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	
	/**
	 *  property to hold mSucnErrorMessage.
	 */
	private String mSucnErrorMessage;
	
	/**
	 *  property to hold mSucnCode.
	 */
	private int  mSucnCode;
	
	/**
	 * List to hold Error Keys.
	 */
	private List<String> mErrorKeyList;
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	/**
	 * Checks if is rest service.
	 * 
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 * 
	 * @param pRestService
	 *            the mRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}

	
	/**
	 * Gets the error key list.
	 * 
	 * @return the errorKeyList
	 */
	public List<String> getErrorKeyList() {
		return mErrorKeyList;
	}

	/**
	 * Sets the error key list.
	 * 
	 * @param pErrorKeyList
	 *            the errorKeyList to set
	 */
	public void setErrorKeyList(List<String> pErrorKeyList) {
		mErrorKeyList = pErrorKeyList;
	}

	/**
	 * Claim the specified coupon, register a form exception if the coupon is invalid or an error occurs.
	 * 
	 * @param pRequest
	 *            - current HTTP servlet request.
	 * @param pResponse
	 *            - current HTTP servlet response.
	 * 
	 * @return true if coupon has been claimed; false otherwise.
	 * 
	 * @throws ServletException
	 *             if an error occurred during claiming the coupon.
	 * @throws IOException
	 *             if an error occurred during claiming the coupon.
	 */
	@Override
	public boolean handleClaimCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCouponFormHandler.claimCoupon() ");
		}
		String couponCodeId = pRequest.getQueryParameter(TRUConstants.COUPON_ID);
		if(!StringUtils.isBlank(couponCodeId)){
			setCouponClaimCode(couponCodeId);
		}
		String tenderdCouponCode = getCouponClaimCode();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean isSingleUseCoupon = Boolean.FALSE;
		final String myHandleMethod = TRUConstants.HANDLE_CLAIM_COUPON;
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			String tenderdCouponClaimCode = getCouponClaimCode();
		final boolean isCouponServiceEnabled =  getIntegrationStatusUtil().getCouponServiceIntStatus(pRequest);
		if (StringUtils.isEmpty(tenderdCouponClaimCode)) { 
			if (isLoggingError()) {
				logError("User enterd empty coupon code... Adding validation exception");
			}
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EMPTY, null);
			getErrorHandler().processException(mVe, this);
		}
		final TRUOrderImpl currentOrder = (TRUOrderImpl) getShoppingCart().getCurrent();
		Profile profile = getProfile();
		TRUPropertyManager propertyManager = getOrderManager().getPropertyManager();
		//For MVP changes
		if (StringUtils.isNotBlank(tenderdCouponClaimCode) && tenderdCouponClaimCode.length() != INT_TWENTY ) {
			tenderdCouponCode = tenderdCouponClaimCode.toUpperCase();
			setCouponClaimCode(tenderdCouponCode);
		}
		boolean isSUCNEnabled = ((TRUClaimableTools)getClaimableTools()).checkSucnEnabled(tenderdCouponCode);
		if(tenderdCouponCode.length() != INT_TWENTY  && isSUCNEnabled){
			if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN) && isPaymentPage()){
				setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN);
				
				}else{
					mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
					getErrorHandler().processException(mVe, this);
				}
			if (isLoggingError()) {
				logError("User enterd not valid coupon code... Adding validation exception");
			}
			
		}
		if (tenderdCouponCode.length() == INT_TWENTY && isCouponServiceEnabled && tenderdCouponCode.matches(NUMBER_REGEX)) {
			createSingleUseCoupon(tenderdCouponCode);
			isSingleUseCoupon = Boolean.TRUE;
		}else{
			tenderdCouponCode = tenderdCouponClaimCode.toUpperCase();
			setCouponClaimCode(tenderdCouponCode);
		}
		setClaimCouponErrorURL(getSuccessOrErrorURL(getClaimCouponErrorURL()));
		setClaimCouponSuccessURL(getSuccessOrErrorURL(getClaimCouponSuccessURL()));
		boolean isCouponUsed = preClaimCoupon(isSingleUseCoupon,tenderdCouponCode);// calling this to validate user entered coupon
		if (!isCouponUsed && !getFormError()){
			final TRUOrderManager orderManager = getOrderManager();
			final TransactionDemarcation td = new TransactionDemarcation();
			final TransactionManager tm = getTransactionManager();
			boolean rollback = true;
			try {
				td.begin(tm, TransactionDemarcation.REQUIRED);
				//check if PG as giftCard then set creditCardType as null for not to apply promotion.
				orderManager.setCCTypeNullIfPGAsGC(currentOrder);
				if(isSingleUseCoupon){
					profile.setPropertyValue(propertyManager.getSucnCodePropertyName(), tenderdCouponCode);
				}
				Map<String, String> singleUseCoupon = currentOrder.getSingleUseCoupon();
				singleUseCoupon.put(tenderdCouponCode, getCouponClaimCode());
				claimCoupon(pRequest, pResponse);
				getPurchaseProcessHelper().runProcessRepriceOrder(
						getCouponConfiguration().getOrderRepricingOption(), currentOrder, getUserPricingModels(),
						getUserLocale(pRequest), getProfile(), null, null);
				adjustOrderPaymentGroups(currentOrder);
				synchronized (currentOrder) {
					orderManager.updateOrder(currentOrder);
				}
				rollback = false;
			} catch (ClaimableException ce) {
				final Throwable sourceException = ce.getSourceException();
				if (sourceException instanceof PromotionException) {
					final PromotionException pe = (PromotionException) sourceException;
					if (isLoggingError()) {
						vlogError("PromotionException  while claiming coupon : {0}", pe);
					}
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED) && isPaymentPage()){
						setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED);
					}else{
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
						getErrorHandler().processException(mVe, this);
					}
					return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
				} else {
					if (isLoggingError()) {
						vlogError("ClaimableException  while claiming coupon : {0}", ce);
					}
					
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_NOT_FOUND) && isPaymentPage()){
						setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_NOT_FOUND);
					}else{
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_NOT_FOUND, null);
						getErrorHandler().processException(mVe, this);
					}					
					rollback = false;
				}
				return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					vlogError("CommerceException  while claiming coupon : {0}", commerceException);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} catch (TransactionDemarcationException tde) {
				if (isLoggingError()) {
					vlogError("TransactionDemarcationException  while claiming coupon:{0} ", tde);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					vlogError("RunProcessException  while claiming coupon:{0} ", e);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} finally {
				profile.setPropertyValue(propertyManager.getSucnCodePropertyName(), null);
				try {
					if (tm != null) {
						td.end(rollback);
					}
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						vlogError("TransactionDemarcationException in finally while claiming coupon:{0} ", e);
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("Exiting from class:[TRUCouponFormHandler] method:[handleClaimCoupon]");
			}
		}
	}
		return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
	}

	/**
	 * Creates the single use coupon.
	 * This method will create a new sucn code with the user entered code and the respective promotion is applied for the order.
	 *
	 * @param pTenderedCouponCode the tendered coupon code
	 * @throws ServletException the servlet exception
	 */
	public void createSingleUseCoupon(String pTenderedCouponCode) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCouponFormHandler.createOrUpdateSingleUseCoupon() ");
		}
		String promoId = null;
		int code = 0;
		String sucnErrorMessage = null;
		CouponLookupResponse lookupCouponDetails = null;
		TRUClaimableTools claimableTools = (TRUClaimableTools) getClaimableTools();
		if (!StringUtils.isBlank(pTenderedCouponCode)) {
			try {
				String pageName = isPaymentPage() ? TRUConstants.PAYMENT :TRUConstants.STR_CART_PAGE;
				lookupCouponDetails =getSucnLookupService().lookupCouponDetails(getCouponClaimCode(), getOrderId(),pageName);
				if (lookupCouponDetails != null) {
					if (isLoggingDebug()) {
						vlogDebug("lookupCouponDetailsl:{0}", lookupCouponDetails.toString());
					}
					promoId = lookupCouponDetails.getPromoId();
					if (!(claimableTools.checkSucnEnabled(promoId))) {
						if (isLoggingError()) {
							logError("User entered not valid coupon code... Adding validation exception");
						}
						if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN) && isPaymentPage()){
							setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN);
						}else{
							mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
							getErrorHandler().processException(mVe, this);
						}
					}
					if (getFormError()) {
						return;
					}
					code = lookupCouponDetails.getCode();
					sucnErrorMessage = lookupCouponDetails.getText();
					setSucnCode(code);
					setSucnErrorMessage(sucnErrorMessage);
					if (isLoggingDebug()) {
						vlogDebug("sucnErrorMessage:{0} :: sucncode:{1}", sucnErrorMessage, code);
					}
					setCouponClaimCode(promoId);
				}

			} catch (SUCNIntegrationException e) {
				if (isLoggingError()) {
					logError("While getting the webservice Integration error ", e);
				}
			} catch (ServletException exc) {
				if (isLoggingError()) {
					logError("ServletException : While getting error message ", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUCouponFormHandler.createOrUpdateSingleUseCoupon() ");
		}
	}
	

	/**
	* This method is used to check weather coupon is expired or not.
	* 
	* @param pCouponItem
	*            - coupon code.
	* @return boolean.
	* @throws RepositoryException RepositoryException.
	*/
	private boolean canClaimCoupon(RepositoryItem pCouponItem) throws RepositoryException {
	     if (isLoggingDebug()) {
	            logDebug("Start: TRUCouponFormHandler.claimCoupon() ");
	     }
	     Boolean canClaim = false;
	     if (pCouponItem == null) {
	            canClaim = false;
	     } else {
	            return getClaimableManager().checkClaimableDates(pCouponItem);
	     }
	     if (isLoggingDebug()) {
	            logDebug("Exiting from class:[TRUCouponFormHandler] method:[handleClaimCoupon]");
	     }
	     return canClaim;
	}


	/**
	 * Register a form exception if the coupon is empty .
	 * @param pIsSingleUseCoupon - boolean.
	 * @param pSUCNCoupon - SUCN coupon Code.
	 * 
	 * @return boolean value
	 * @throws ServletException
	 *             if an error occurred during validating the coupon.
	 * @throws IOException
	 *             if an error occurred during validating the coupon.
	 */

	public boolean preClaimCoupon(boolean pIsSingleUseCoupon, String pSUCNCoupon)
			throws ServletException, IOException {
		if (isLoggingDebug()){
			logDebug("Entering into class:[TRUCouponFormHandler] method:[preClaimCoupon]");
		}
		final ValidationExceptions ve = new ValidationExceptions(); 
		RepositoryItem coupon = null;
		final String tenderdCouponCode = getCouponClaimCode();
		final String sucnErrorMessage = getSucnErrorMessage();
		final Profile profile = getProfile();
		boolean couponUsed = false;
		try {
		if (getSucnCode() != 0 && !StringUtils.isEmpty(sucnErrorMessage)) {//This fix provided as per defect 51850
			if ( getSucnCode() == TRUConstants.FOUR) {
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED) && isPaymentPage()){
						setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED);
					}else{
						ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
						getErrorHandler().processException(ve, this);
					}
				} else {
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN) && isPaymentPage()){
						setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN);
					}else{
						ve.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
						getErrorHandler().processException(ve, this);
					}
				}
		}
		boolean isValid = Boolean.TRUE;
		final TRUPricingTools pricingTools = (TRUPricingTools) getPromotionTools().getPricingTools();
			isValid = getOrderManager().getCouponCode(profile,tenderdCouponCode,pIsSingleUseCoupon,pSUCNCoupon);
			// If this coupon has already been claimed just return
			if (!StringUtils.isEmpty(tenderdCouponCode) && !isValid) {
				couponUsed = true;
				return couponUsed;
			}
			if (!StringUtils.isEmpty(tenderdCouponCode)) { 
				coupon = getClaimableTools().getClaimableItem(tenderdCouponCode);
				if (coupon != null) {
					final String couponName = (String) pricingTools.getValueForProperty(getDisplayName(), coupon);
					if (!StringUtils.isEmpty(couponName) && !canClaimCoupon(coupon)) {
						if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED) && isPaymentPage()){
							setChannelError(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED);
						}else{
							ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
							getErrorHandler().processException(ve, this);
						}
					}
					Integer value = (Integer)coupon.getPropertyValue(getClaimableTools().getUsesPropertyName());
			        int uses = (value != null) ? value.intValue() : TRUConstants.INT_ZERO;
			        value = (Integer)coupon.getPropertyValue(getClaimableTools().getMaxUsesPropertyName());
			        int maxUses = (value != null) ? value.intValue() : TRUConstants.INT_MINUS_ONE;
			        if (maxUses >TRUConstants.INT_MINUS_ONE&& uses >= maxUses) {
			        	if (isLoggingError()) {
							logError("Reached Max Limit for applying coupons... Adding validation exception");
						}
			  		    ve.addValidationError(TRUErrorKeys.TRU_ERROR_COUPON_MAX_LIMIT, null);
						getErrorHandler().processException(ve, this);
				}
				}
			}
			// Modified for defect - TUW-64607
			//final boolean isMaxLimitReached = getOrderManager().checkMaxCouponLimit(profile);
			final boolean isMaxLimitReached = getOrderManager().checkOrderMaxCouponLimit(getShoppingCart().getCurrent());
			if(isMaxLimitReached ){
				if (isLoggingError()) {
					logError("Reached Max Limit for applying coupons... Adding validation exception");
				}
	  		    ve.addValidationError(TRUErrorKeys.TRU_ERROR_COUPON_MAX_LIMIT, null);
				getErrorHandler().processException(ve, this);
	  	  	}
		} catch (CommerceException exc) {
			if (isLoggingError()) {
				vlogError("CommerceException : while getting coupon from profile : {0} with exception : {1}", profile, exc);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RunProcessException  while claiming coupon:{0} ", e);
			}
			ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
			getErrorHandler().processException(ve, this);
		} 
		if (getFormError()) {
			return false;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from class:[TRUCouponFormHandler] method:[preClaimCoupon]");
		}
		return false;
	}

	/**
	 * This method is used to get the error URL and Success URL from the map.
	 * 
	 * @param pValue
	 *            - success or error value.
	 * @return url - success or error url.
	 */
	public String getSuccessOrErrorURL(String pValue) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCouponFormHandler.getSuccessOrErrorURL() ");
			vlogDebug("Success or Error value: {0}", pValue);
		}
		String url = null;
		if (pValue != null) {
			url = getSuccessErrorUrlMap().get(pValue);
		}
		if (url == null) {
			url = pValue;
		} else {
			vlogDebug("Success or Error URL value is null : {0}", pValue);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCouponFormHandler.getSuccessOrErrorURL() ");
		}
		return url;
	}

	/**
	 *  This method is used to remove the coupon which user applied.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse.
	 * @return
	 * 				- true if coupon has been removed; false otherwise.
	 * @throws ServletException
	 *             - If any ServletException.
	 * @throws IOException
	 *             - If any IOException.
	 * @throws RunProcessException
	 * 				- If any RunProcessException.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean handleRemoveCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, RunProcessException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCouponFormHandler.handleRemoveCoupon()");
		}
		final ValidationExceptions ve = new ValidationExceptions();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUConstants.HANDLE_REMOVE_COUPON;
		final TRUOrderManager orderManager = getOrderManager();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		String couponCodeId = pRequest.getQueryParameter(TRUConstants.COUPON_ID);
		if(!StringUtils.isBlank(couponCodeId)){
			setCouponClaimCode(couponCodeId);
		}
		
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			vlogDebug("Success URL value : {0} Error URL value : {1}", getClaimCouponSuccessURL(),
					getClaimCouponErrorURL());
			setClaimCouponSuccessURL(getSuccessOrErrorURL(getClaimCouponSuccessURL()));
			setClaimCouponErrorURL(getSuccessOrErrorURL(getClaimCouponErrorURL()));
			vlogDebug("Success URL value : {0} Error URL value : {1}", getClaimCouponSuccessURL(),
					getClaimCouponErrorURL());
			preRemoveCoupon(pRequest, pResponse);
			if(!getFormError()){
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					String tenderdCouponCode = getCouponClaimCode();
					getPurchaseProcessHelper().removeCouponItem(tenderdCouponCode, getProfile(),
							getUserLocale(pRequest));
					Map extraParameters = new HashMap();
					extraParameters.put(TRUConstants.REMOVED_COUPON_CODE, tenderdCouponCode);
					final PricingModelHolder userPricingModels = getUserPricingModels();
					final Order currentOrder = getShoppingCart().getCurrent();
					userPricingModels.initializePricingModels();
					getPurchaseProcessHelper().runProcessRepriceOrder(getCouponConfiguration().mOrderRepricingOption,
							currentOrder, userPricingModels, getUserLocale(pRequest), getProfile(), extraParameters, null);
					adjustOrderPaymentGroups(currentOrder);
					RepositoryItem coupon= getClaimableTools().getClaimableItem(tenderdCouponCode);
					if(null!=coupon){
						((TRUClaimableTools)getClaimableTools()).updateCouponUses(coupon);
					}
					synchronized (currentOrder) {
						orderManager.updateOrder(currentOrder);
					}
				}
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				if (isLoggingError()) {
					logError("TransactionDemarcationException occured while removing reward number with exception {0}",
							trDem);
				}
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					vlogError("CommerceException  while claiming coupon : {0}", commerceException);
				}
				ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(ve, this);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError("RepositoryException  while claiming coupon : {0}", e);
				}
				ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(ve, this);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						if (isLoggingError()) {
							logError("TransactionDemarcationException", tD);
						}
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
			if (isLoggingDebug()) {
				logDebug("End: TRUCouponFormHandler.handleRemoveCoupon()");
			}
		}
		}
		return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
	}
	
	
	/**
	 * Register a form exception if the coupon is empty or invalid.
	 * 
	 * @param pRequest
	 *            - current HTTP servlet request.
	 * @param pResponse
	 *            - current HTTP servlet response.
	 * 
	 * @throws ServletException
	 *             if an error occurred during validating the coupon.
	 * @throws IOException
	 *             if an error occurred during validating the coupon.
	 */
	public void preRemoveCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCouponFormHandler] method:[preRemoveCoupon]");
		}
		final ValidationExceptions ve = new ValidationExceptions();
		RepositoryItem coupon = null;
		final String tenderdCouponCode = getCouponClaimCode();
		if (StringUtils.isEmpty(tenderdCouponCode)) {
			if (isLoggingError()) {
				logError("User enterd empty coupon code... Adding validation exception");
			}
			ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EMPTY, null);
			getErrorHandler().processException(ve, this);
		}
		if (!StringUtils.isEmpty(tenderdCouponCode)) {
			try {
				coupon = getClaimableTools().getClaimableItem(tenderdCouponCode);
				if (null == coupon) {
					if (isLoggingError()) {
						logError("User enterd invalid coupon code... Adding validation exception");
					}
					ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_NOT_FOUND, null);
                    getErrorHandler().processException(ve, this);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					vlogError("RunProcessException  while removing coupon:{0} ", re);
				}
			}
		}
	}

	/**
	 * This method is used to get the coupon code by calling SUCN Service.
	 */
	/*private void updateCouponCode() {
		String lPromoId = null;
		int code = 0;
		String sucnErrorMessage = null;
		CouponLookupResponse lLookupCouponDetailsl = null;
		try {
			Pattern pattern = Pattern.compile(NUMBER_REGEX);
			if(!pattern.matcher(getCouponClaimCode()).matches()){
				if (isLoggingError()) {
					logError("User enterd invalid single use coupon code... Coupon has letters in coupon ocde... Adding validation exception");
				}
				mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
				getErrorHandler().processException(mVe, this);
				return;
			}
			lLookupCouponDetailsl = getSucnLookupService().lookupCouponDetails(
					getCouponClaimCode(), getOrderId());
			if (lLookupCouponDetailsl != null) {
				lPromoId = lLookupCouponDetailsl.getPromoId();
				if(!((TRUClaimableTools)getClaimableTools()).checkSucnEnabled(lPromoId)){
					if (isLoggingError()) {
						logError("User enterd not valid coupon code... Adding validation exception");
					}
					mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
					getErrorHandler().processException(mVe, this);
				}
				if (getFormError()) {
					return;
				}
				code = lLookupCouponDetailsl.getCode();
				sucnErrorMessage = lLookupCouponDetailsl.getText();
				setSucnCode(code);
				setSucnErrorMessage(sucnErrorMessage);
				if (!StringUtils.isEmpty(lPromoId) && code == 0) {
					setCouponClaimCode(lPromoId);
				} 
			}
		} catch (SUCNIntegrationException e) {
			if (isLoggingError()) {
				logError("While getting the webservice Integration error ", e);
			}
		} catch (ServletException exc) {
			if (isLoggingError()) {
				logError("ServletException : While getting error message ", exc);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Six digit response code : {0}", lPromoId);
		}
	}*/

	/**
	 * @return TransactionDemarcation object.
	 */
	protected TransactionDemarcation getTransactionDemarcation() {
		return new TransactionDemarcation();
	}

	/**
	 * @return the SucnLookupService.
	 */
	public TRUSUCNLookupService getSucnLookupService() {
		return mSucnLookupService;
	}

	/**
	 * @param pSucnLookupService
	 *            the SucnLookupService to set.
	 */
	public void setSucnLookupService(TRUSUCNLookupService pSucnLookupService) {
		mSucnLookupService = pSucnLookupService;
	}

	/**
	 * @return the orderId.
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId
	 *            the orderId to set.
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * Returns the singleUseCouponCode.
	 * 
	 * @return the singleUseCouponCode.
	 */
	public String getSingleUseCouponCode() {
		return mSingleUseCouponCode;
	}

	/**
	 * Sets/updates the singleUseCouponCode.
	 * 
	 * @param pSingleUseCouponCode
	 *            the singleUseCouponCode to set.
	 */
	public void setSingleUseCouponCode(String pSingleUseCouponCode) {
		mSingleUseCouponCode = pSingleUseCouponCode;
	}

	/**
	 * @return the mUserPricingModels.
	 */
	public PricingModelHolder getUserPricingModels() {
		return mUserPricingModels;
	}

	/**
	 * @param pUserPricingModels
	 *            the mUserPricingModels to set.
	 */
	public void setUserPricingModels(PricingModelHolder pUserPricingModels) {
		mUserPricingModels = pUserPricingModels;
	}

	/**
	 * @return the shoppingCart.
	 */
	public TRUOrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	/**
	 * @param pShoppingCartp
	 *            the shoppingCart to set.
	 */
	public void setShoppingCart(TRUOrderHolder pShoppingCartp) {
		mShoppingCart = pShoppingCartp;
	}

	/**
	 * @return the mSuccessErrorUrlMap.
	 */
	public Map<String, String> getSuccessErrorUrlMap() {
		return mSuccessErrorUrlMap;
	}

	/**
	 * @param pSuccessErrorUrlMap
	 *            the mSuccessErrorUrlMap to set.
	 */
	public void setSuccessErrorUrlMap(Map<String, String> pSuccessErrorUrlMap) {
		mSuccessErrorUrlMap = pSuccessErrorUrlMap;
	}

	/**
	 * @return the mRepeatingRequestMonitor.
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @param pRepeatingRequestMonitor
	 *            the mRepeatingRequestMonitor to set.
	 */
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	/**
	 * @return the mTRUPurchaseProcessHelper.
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * This method will set mPurchaseProcessHelper with pPurchaseProcessHelper.
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set.
	 */
	public void setPurchaseProcessHelper(TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * @return the couponConfiguration.
	 */
	public TRUConfiguration getCouponConfiguration() {
		return mCouponConfiguration;
	}

	/**
	 * @param pCouponConfiguration
	 *            the couponConfiguration to set.
	 */
	public void setCouponConfiguration(TRUConfiguration pCouponConfiguration) {
		mCouponConfiguration = pCouponConfiguration;
	}

	/**
	 * This method will return ErrorHandler.
	 * 
	 * @return mErrorHandler.
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * This method will set ErrorHandlers with pErrorHandler.
	 * 
	 * @param pErrorHandler
	 *            refers the IErrorHandler.
	 */

	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * This Method will return the TRUOrderManager.
	 * 
	 * @return the mOrderManager.
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * This method will set OrderManager to pOrderManager.
	 * 
	 * @param pOrderManager
	 *            the mOrderManager to set.
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}

	/**
	 * @return the mdisplayName.
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * @param pDisplayNamep the displayNamep to set.
	 */
	public void setDisplayName(String pDisplayNamep) {
		mDisplayName = pDisplayNamep;
	}

	/**
	 * @return the errorHandlerManager.
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager the errorHandlerManager to set.
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * This method is to adjust order payment groups.
	 * @param pOrder - Order.
	 * @throws CommerceException - if any.
	 */
	private void adjustOrderPaymentGroups(Order pOrder) throws CommerceException {
		PipelineResult result;
		result = ((TRUOrderManager)getOrderManager()).adjustPaymentGroups(pOrder);
		if (isLoggingDebug()) {
			logDebug("TRUGiftCardFormHandler/adjustPaymentGroups method "+result);
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUGiftCardFormHandler/adjustPaymentGroups method");
		}
	}
	
	

	/**
	 * @return the integrationStatusUtil.
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set.
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * @return the mSucnCode.
	 */
	public int getSucnCode() {
		return mSucnCode;
	}

	/**
	 * @param pSucnCode the mSucnCode to set.
	 */
	public void setSucnCode(int pSucnCode) {
		this.mSucnCode = pSucnCode;
	}
	
	/**
	 * @return the mSucnErrorMessage.
	 */
	public String getSucnErrorMessage() {
		return mSucnErrorMessage;
	}

	/**
	 * @param pSucnErrorMessage the mSucnErrorMessage to set.
	 */
	public void setSucnErrorMessage(String pSucnErrorMessage) {
		this.mSucnErrorMessage = pSucnErrorMessage;
	}
	/**
	 * property to hold mPaymentPage.
	 */
	private boolean mPaymentPage;

	/**
	 * @return the paymentPage
	 */
	public boolean isPaymentPage() {
		return mPaymentPage;
	}
	/**
	 * @param pPaymentPage the paymentPage to set
	 */
	public void setPaymentPage(boolean pPaymentPage) {
		mPaymentPage = pPaymentPage;
	}
	/**
	 * This method will set error 
	 * 
	 * @param pErrorMessage - ErrorMessage
	 * @throws ServletException - If any ServletException.
	 */
	private void setChannelError(String pErrorMessage) throws ServletException {
		if(isRestService()) {
			mVe.addValidationError(pErrorMessage, null);
			getErrorHandler().processException(mVe, this);
		} else {
			addFormException(new DropletException(pErrorMessage));
		}
	}
}
