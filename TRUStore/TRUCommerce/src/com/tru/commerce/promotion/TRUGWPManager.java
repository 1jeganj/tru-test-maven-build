package com.tru.commerce.promotion;

import java.util.List;
import java.util.Map;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.promotion.GWPManager;
import atg.commerce.promotion.GWPMarkerManager;
import atg.commerce.promotion.GiftWithPurchaseSelectionChoice;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUOrderImpl;

/**
 * This class is extended for TRU business extension.
 * 
 * @author PA
 * @version 1.0.
 */
public class TRUGWPManager extends GWPManager {

	/** Property To mCoherenceInventoryManager . */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;

	/**
	 * Gets the coherence inventory manager.
	 * 
	 * @return the coherence inventory manager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the coherence inventory manager.
	 * 
	 * @param pCoherenceInventoryManager
	 *            the CoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(
			TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	/**
	 * <p>
	 * Override commerce implementation to validate required field of gift item
	 * marked as auto add.
	 * </p>
	 * *
	 * <p>
	 * Mark item as failed if validation fail.
	 * </p>
	 * 
	 * @param pOrderMarker
	 *            - the order marker.
	 * @param pQuantityToAdd
	 *            - the quantity to add.
	 * @param pNewItemInfos
	 *            - List of AddCommerceItemInfo objects.
	 * @param pNewItems
	 *            - List of CommerceItem objects.
	 * @param pPricingContext
	 *            - the PricingContext to be used.
	 * @param pExtraParameters
	 *            - Optional map of extra parameters.
	 * 
	 * @return the newly added commerce item.
	 * 
	 * @throws PricingException
	 *             when there is an error auto-adding the item.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public CommerceItem processAutoAdd(RepositoryItem pOrderMarker,
			long pQuantityToAdd, List<AddCommerceItemInfo> pNewItemInfos,
			List<CommerceItem> pNewItems, PricingContext pPricingContext,
			Map pExtraParameters) throws PricingException {
		if (isLoggingDebug()) {
			logDebug("Entering processAutoAdd() method of TRUGWPManager....");
			logDebug("pOrderMarker:" + pOrderMarker);
			logDebug("pQuantityToAdd:" + pQuantityToAdd);
			logDebug("pNewItemInfos:" + pNewItemInfos);
			logDebug("pNewItems:" + pNewItems);
			logDebug("pPricingContext:" + pPricingContext);
			logDebug("pExtraParameters:" + pExtraParameters);
		}
		CommerceItem newItem = null;
		Boolean isValid = Boolean.FALSE;
		// Added logic to restrict the GWP promotion for Apple Pay order created from PDP.
		if(pPricingContext != null && pPricingContext.getOrder() instanceof TRUOrderImpl){
			TRUOrderImpl orderImpl = (TRUOrderImpl) pPricingContext.getOrder();
			if(isLoggingInfo()){
				vlogDebug("Order is ApplePay and Created From PDP : {0}",orderImpl.isApplePayPDP());
			}
			if(orderImpl.isApplePayPDP()){
				return newItem;
			}
		}
		// END.
		isValid = preProcessAutoAdd(pOrderMarker, pExtraParameters,
				pQuantityToAdd,pPricingContext);
		if (isValid) {
			newItem = super
					.processAutoAdd(pOrderMarker, pQuantityToAdd,
							pNewItemInfos, pNewItems, pPricingContext,
							pExtraParameters);
			((TRUCommerceItemImpl)newItem).setGiftItem(Boolean.TRUE);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting processAutoAdd() method of TRUGWPManager with new Item ::: "+ newItem);
		}
		return newItem;
	}

	/**
	 * This method is used to validate against the inventory from coherence
	 * cache if GWP item is Instock or limited stock it will return true else
	 * false.
	 *
	 * @param pOrderMarker            - order marker
	 * @param pExtraParameters            - extra parameters
	 * @param pQuantityToAdd            - quantity
	 * @param pPricingContext the pricing context
	 * @return true, if successful
	 */
	@SuppressWarnings("rawtypes")
	private boolean preProcessAutoAdd(RepositoryItem pOrderMarker,
			Map pExtraParameters, long pQuantityToAdd,PricingContext pPricingContext) {
		if (isLoggingDebug()) {
			logDebug("Entering preProcessAutoAdd() method of TRUGWPManager....");
		}
		Boolean isValid = Boolean.FALSE;
		GWPMarkerManager manager = getGwpMarkerManager();
		GiftWithPurchaseSelectionChoice selectionChoice = null;
		long inventoryQty = 0;
		long itemQtyInCart = 0;
		long itemRelQtyInCart = 0;
		long customerLimit = 0;
		long limit=0;
		try {
			selectionChoice = getAutoAddGiftSelectionChoice(
					manager.getGiftType(pOrderMarker),
					manager.getGiftDetail(pOrderMarker), pExtraParameters);
			if (selectionChoice != null) {
				RepositoryItem sku = selectionChoice.getSkus().iterator()
						.next();
				String skuId = sku.getRepositoryId();
				int availabilityStatus = getCoherenceInventoryManager()
						.queryAvailabilityStatus(skuId);
				 customerLimit=getShoppingCartUtils().getCustomerPurchaseLimit(skuId);
				if (availabilityStatus == TRUCommerceConstants.INT_IN_STOCK) {
					inventoryQty = getShoppingCartUtils().getItemInventory(skuId, null, availabilityStatus);
					itemRelQtyInCart = getShoppingCartUtils().getItemQtyInCartByRelShip(skuId, null, pPricingContext.getOrder());
					customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(skuId);
					itemQtyInCart = getShoppingCartUtils().getItemQtyInCart(skuId, pPricingContext.getOrder());
					long purchaseLimit = customerLimit - itemQtyInCart;
					long inventoryLimit = inventoryQty - itemRelQtyInCart;
					if(purchaseLimit<inventoryLimit){
						limit=purchaseLimit;
					}
					else{
						limit=inventoryLimit;
					}
					if(pQuantityToAdd<=limit){
						isValid = Boolean.TRUE;
					}
				} else {
					// Gift item is out of stock, mark as failed.
					if (isLoggingDebug()) {
						logDebug("Auto add item is out of stock or has invalid dates, set quantity "
								+ pQuantityToAdd + " as failed");
					}
				}
			}
		} catch (CommerceException e) {
			if(isLoggingError()){
				vlogError(e, "Failed to auto add quantity.");
			}
		} catch (NumberFormatException e) {
			if(isLoggingError()){
				vlogError(e, "Failed to auto add quantity.");
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError(e, "Failed to auto add quantity.");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting preProcessAutoAdd() method of TRUGWPManager with new Item ::: ");
		}
		return isValid;
	}
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}
}
