package com.tru.commerce.promotion;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;

import atg.commerce.pricing.ItemPricingEngineImpl;
import atg.commerce.pricing.OrderPricingEngineImpl;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.TRUPromotionTools;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.pricing.TRUShippingPricingEngine;
import com.tru.preview.TRUCurrentDate;
import com.tru.preview.TRUSchedulableDate;
import com.tru.userprofiling.TRUCookieManager;


/**
 * Class will set the future date given by user to present date
 */
public class TRUPreviewFormHandler extends GenericFormHandler {
	
	/**
	 * Constant to hold mPreviewDateSuccessURL
	 */
	private String mPreviewDateSuccessURL;
	
	/**
	 * Constant to hold mPromotionTools
	 */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * Constant to hold mSystemDate
	 */
	private Timestamp mSystemDate;
	
	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}
	/**
	 * @return the previewDateSuccessURL
	 */
	public String getPreviewDateSuccessURL() {
		return mPreviewDateSuccessURL;
	}
	/**
	 * @param pPreviewDateSuccessURL the previewDateSuccessURL to set
	 */
	public void setPreviewDateSuccessURL(String pPreviewDateSuccessURL) {
		mPreviewDateSuccessURL = pPreviewDateSuccessURL;
	}
	
	/**Holds the  mSchedulableDate. */
	private TRUSchedulableDate mSchedulableDate;
	
	/**
	 * @return the schedulableDate
	 */
	public TRUSchedulableDate getSchedulableDate() {
		return mSchedulableDate;
	}
	/**
	 * @param pSchedulableDate the schedulableDate to set
	 */
	public void setSchedulableDate(TRUSchedulableDate pSchedulableDate) {
		mSchedulableDate = pSchedulableDate;
	}
	
	/**
	 * it holds the value of mRedirectionUrl
	 */
	private String mRedirectionUrl;
	/**
	 * it holds the value of mPreviewDate
	 */
	private String mPreviewDate;
	/**
	 * it holds the value of mPreviewDateErrorURL
	 */
	private String mPreviewDateErrorURL;
	/**
	 * @return the previewDateErrorURL
	 */
	public String getPreviewDateErrorURL() {
		return mPreviewDateErrorURL;
	}
	/**
	 * @param pPreviewDateErrorURL the previewDateErrorURL to set
	 */
	public void setPreviewDateErrorURL(String pPreviewDateErrorURL) {
		mPreviewDateErrorURL = pPreviewDateErrorURL;
	}
	/**
	 * @return the previewDate
	 */
	public String getPreviewDate() {
		return mPreviewDate;
	}
	/**
	 * @param pPreviewDate the previewDate to set
	 */
	public void setPreviewDate(String pPreviewDate) {
		SimpleDateFormat userFormat = new SimpleDateFormat(TRUCommerceConstants.USER_DATE_FORMAT_YYYY_MM_DD_HH_MM);
		SimpleDateFormat acceptFormat = new SimpleDateFormat(TRUCommerceConstants.ACCEPTED_DATE_FORMAT_MM_DDYYYY_HH_MM_SS);
		String reformattedDate = null;
		try {
			reformattedDate = acceptFormat.format(userFormat.parse(pPreviewDate));
		} 
		catch (ParseException exce) {
			if (isLoggingError()) {
				logError("ParseException:  While parsing user's Date", exce);
			}
		}
		mPreviewDate = reformattedDate != null ? reformattedDate : pPreviewDate;
	}
	private TRUCookieManager mCookieManager;
	
	/**
	 * @return the mCookieManager
	 */
	public TRUCookieManager getCookieManager() {
		return mCookieManager;
	}
	/**
	 * @param pCookieManager the 
	 * pCookieManager to set
	 */
	public void setCookieManager(TRUCookieManager pCookieManager) {
		 mCookieManager = pCookieManager;
	}
	/**
	 * Getting the value of mRedirectionUrl
	 * @return mRedirectionUrl
	 */
	public String getRedirectionUrl() {
		return mRedirectionUrl;
	}
	/**
	 * Setting the value of pRedirectionUrl
	 * @param pRedirectionUrl
	 * 				- pRedirectionUrl
	 */
	public void setRedirectionUrl(String pRedirectionUrl) {
		this.mRedirectionUrl = pRedirectionUrl;
	}
	/**
	 * Defined the Constant as mPreviewDateParamName
	 */
	private String mPreviewDateParamName;
	/**
	 * 
	 * @return mPreviewDateParamName - PreviewDateParamName
	 */
	public String getPreviewDateParamName() {
		return mPreviewDateParamName;
	}
	/**
	 * 
	 * @param pPreviewDateParamName
	 * 				- pPreviewDateParamName
	 */
	public void setPreviewDateParamName(String pPreviewDateParamName) {
		mPreviewDateParamName = pPreviewDateParamName;
	}
	/**
	 * it holds the CurrentDate.
	 */
	private TRUCurrentDate mCurrentDate;
	/**
	 * 
	 * @return mCurrentDate
	 */
	public TRUCurrentDate getCurrentDate() {
		return mCurrentDate;
	}
	/**
	 * 
	 * @param pCurrentDate
	 * 				- pCurrentDate
	 */
	public void setCurrentDate(TRUCurrentDate pCurrentDate) {
		this.mCurrentDate = pCurrentDate;
	}
	/**
	 * it holds the UserPricingModelHolder.
	 */
	private PricingModelHolder mUserPricingModelHolderPath;
	/**
	 * 
	 * @return mUserPricingModelHolderPath
	 */
	public PricingModelHolder getUserPricingModelHolderPath() {
		return mUserPricingModelHolderPath;
	}
	/**
	 * 
	 * @param pUserPricingModelHolderPath
	 * 				- pUserPricingModelHolderPath
	 */
	public void setUserPricingModelHolderPath(
			PricingModelHolder pUserPricingModelHolderPath) {
		this.mUserPricingModelHolderPath = pUserPricingModelHolderPath;
	}
	/**
	 * Handle updating the future promotion Date.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws PricingException 
	 *             PricingException if any
	 */
	public boolean handleUpdatePreviewDate(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException, IOException, PricingException{
		
		if (isLoggingDebug()) {
			logDebug("@Class::TRUPreviewFormHandler::@method::handleUpdatePreviewDate() : START");
			vlogDebug("pRequest : {0},pResponse : {1}", pRequest,
					pResponse);
		}
	
		// test if the param was found
		if (getPreviewDate() == null) {
			if (isLoggingDebug()) {
				logDebug("previewDate param not found.");
			}
		} else {
			// test that the format is valid must be MMDDYYYY-HH:MM:SS
			if (getPreviewDate().length() != TRUCommerceConstants.DATE_LENGHT_SEVENTEEN) {
				if (isLoggingError()) {
					logError("getPreviewDate() not correct format, its not the right length.  Use MMDDYYYY-HH:MM:SS");
				}
			} else if (getPreviewDate().charAt(TRUCommerceConstants.DATE_LENGHT_EIGHT) != TRUCommerceConstants.DASH) {
				if (isLoggingError()) {
					logError("previewDateString not correct format, dash is not in position 8.  Use MMDDYYYY-HH:MM:SS");
				}
			} else if (getPreviewDate().charAt(TRUCommerceConstants.DATE_LENGHT_ELEVAN) != TRUCommerceConstants.COLON) {
				if (isLoggingError()) {
					logError("previewDateString not correct format, colon is not in position 11.  Use MMDDYYYY-HH:MM:SS");
				}
			} else if (getPreviewDate().charAt(TRUCommerceConstants.DATE_LENGHT_FOURTEEN) != TRUCommerceConstants.COLON) {
				if (isLoggingError()) {
					logError("previewDateString not correct format, colon is not in position 14.  Use MMDDYYYY-HH:MM:SS");
				}
			} else {
				// create new Date object to represent the 'previewDate'
				Calendar previewDate = Calendar.getInstance();
				String year = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_FOUR, TRUCommerceConstants.DATE_LENGHT_EIGHT);
				
				int iYear = Integer.valueOf(year);
				String month = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_ZERO, TRUCommerceConstants.DATE_LENGHT_TWO);

				int iMonth = Integer.valueOf(month);
				iMonth -= TRUCommerceConstants.DATE_LENGHT_ONE;
				String day = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_TWO, TRUCommerceConstants.DATE_LENGHT_FOUR);
				
				int iDay = Integer.valueOf(day);

				String hour = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_NINE, TRUCommerceConstants.DATE_LENGHT_ELEVAN);
				
				int iHour = Integer.valueOf(hour);
				String min = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_TWELVE, TRUCommerceConstants.DATE_LENGHT_FOURTEEN);
				
				int iMin = Integer.valueOf(min);
				String sec = getPreviewDate().substring(TRUCommerceConstants.DATE_LENGHT_FIFTEEN, TRUCommerceConstants.DATE_LENGHT_SEVENTEEN);
				
				int iSec =Integer.valueOf(sec);
				if (isLoggingDebug()) {
					logDebug("iYear=" + iYear);
					logDebug("iMonth=" + iMonth);
					logDebug("iDay=" + iDay);
					logDebug("iHour=" + iHour);
					logDebug("iMin=" + iMin);
					logDebug("iSec=" + iSec);
				}
				previewDate.set(iYear, iMonth, iDay, iHour, iMin, iSec);
				TRUCurrentDate cd = getCurrentDate();
				cd.setPreviewFlag(true);
				if(isLoggingDebug()){
					logDebug("PreviewFlag value :: "+ cd.isPreviewFlag());
				}
				cd.setTime(previewDate.getTimeInMillis());
				cd.getTimeAsTimestamp();

				if (isLoggingDebug()) {
					logDebug("new date=" + cd.getTimeAsDate().toString());
				}
				SimpleDateFormat dateFormat = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT_PREVEW);
				String formattedDate = dateFormat.format(cd.getTimeAsDate());
				String finalUrl =  getRedirectionUrl() + formattedDate;
				getCookieManager().setCurrentDate(cd);
				getSchedulableDate().performSetTimeAsDate();
				PricingModelHolder pricingHolder = getUserPricingModelHolderPath();
				if(getPromotionTools().isLoadPricingEngine()){
					getItemPricingEngine().loadGlobalPromotions();
					getOrderPricingEngine().loadGlobalPromotions();
					getShippingPricingEngine().loadGlobalPromotions();
					pricingHolder.initializeItemPricingModels();
					pricingHolder.initializeOrderPricingModels();
					pricingHolder.initializeShippingPricingModels();
				}else{
					pricingHolder.initializeAllPromotions();	
				}
				
				if(isLoggingDebug()){
					vlogDebug("finalUrl value ::{0} " + finalUrl);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUPreviewFormHandler::@method::handleUpdatePreviewDate() : END");
		}
		return  checkFormRedirect(getPreviewDateSuccessURL(), getPreviewDateErrorURL(), pRequest, pResponse);
	}
	
	/**
	 * @return the systemDate
	 */
	public Timestamp getSystemDate() {
		if (mSystemDate == null) {
			Date currentDate = new Date();
			mSystemDate = new Timestamp(currentDate.getTime());
		}
		return mSystemDate;
	}
	/**
	 * @param pSystemDate the systemDate to set
	 */
	public void setSystemDate(Timestamp pSystemDate) {
		mSystemDate = pSystemDate;
	}
	
	/**Holds the  mItemPricingEngine. */
	private ItemPricingEngineImpl mItemPricingEngine;

	/**Holds the  mOrderPricingEngine. */
	private OrderPricingEngineImpl mOrderPricingEngine;

	/**Holds the  mShippingPricingEngine. */
	private TRUShippingPricingEngine mShippingPricingEngine;

	/**
	 * @return the itemPricingEngine
	 */
	public ItemPricingEngineImpl getItemPricingEngine() {
		return mItemPricingEngine;
	}

	/**
	 * @param pItemPricingEngine the itemPricingEngine to set
	 */
	public void setItemPricingEngine(ItemPricingEngineImpl pItemPricingEngine) {
		mItemPricingEngine = pItemPricingEngine;
	}

	/**
	 * @return the orderPricingEngine
	 */
	public OrderPricingEngineImpl getOrderPricingEngine() {
		return mOrderPricingEngine;
	}

	/**
	 * @param pOrderPricingEngine the orderPricingEngine to set
	 */
	public void setOrderPricingEngine(OrderPricingEngineImpl pOrderPricingEngine) {
		mOrderPricingEngine = pOrderPricingEngine;
	}

	/**
	 * @return the shippingPricingEngine
	 */
	public TRUShippingPricingEngine getShippingPricingEngine() {
		return mShippingPricingEngine;
	}

	/**
	 * @param pShippingPricingEngine the shippingPricingEngine to set
	 */
	public void setShippingPricingEngine(
			TRUShippingPricingEngine pShippingPricingEngine) {
		mShippingPricingEngine = pShippingPricingEngine;
	}
}
