package com.tru.commerce.promotion.scheduler;

import atg.commerce.promotion.TRUPromotionTools;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * This class is used to send the budget limit report on hourly basis to the configure 
 * recipient email address.
 * 
 * 
 */
public class TRUBudgetLimitReportEmailScheduler extends SingletonSchedulableService {

	/**
	 * Holds reference for mEnable
	 */
	private boolean mEnable;

	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;

	/**
	 * This method is used to send the budget limit report on hourly basis.
	 * 
	 * @param pParamScheduler
	 *            - Scheduler Object
	 * @param pParamScheduledJob
	 *            - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("TRUBudgetLimitReportEmailScheduler :: doScheduledTask() method :: STARTS ");
		}
		if (isEnable()) {
			sendBudgetReport();
		}
		if (isLoggingDebug()) {
			logDebug("TRUBudgetLimitReportEmailScheduler :: doScheduledTask() method :: END ");
		}
	}

	/**
	 * Send budget report.
	 */
	public void sendBudgetReport() {
		if (isLoggingDebug()) {
			logDebug("TRUBudgetLimitReportEmailScheduler :: sendBudgetReport() method :: STARTS ");
		}
		getPromotionTools().sendBudgetPromoListInEmail();

		if (isLoggingDebug()) {
			logDebug("TRUBudgetLimitReportEmailScheduler :: sendBudgetReport() method :: END ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable
	 *            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the promotion tools.
	 * 
	 * @return the promotionTools.
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 * 
	 * @param pPromotionTools
	 *            the promotionTools to set.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
}
