package com.tru.commerce.promotion.email;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUFeedEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPromotionEmailConfiguration extends GenericService {


	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mSuccessTemplateEmailInfo;

	/** Property to hold Recipients. */
	private List<String> mRecipients;

	/** Property to hold mMessageSubject. */
	private String mMessageSubject;
	
	/** Property to hold mHourlyMessageSubject. */
	private String mHourlyMessageSubject;
	
	/**
	 * @return the hourlyMessageSubject
	 */
	public String getHourlyMessageSubject() {
		return mHourlyMessageSubject;
	}

	/**
	 * @param pHourlyMessageSubject the hourlyMessageSubject to set
	 */
	public void setHourlyMessageSubject(String pHourlyMessageSubject) {
		mHourlyMessageSubject = pHourlyMessageSubject;
	}

	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getSuccessTemplateEmailInfo() {
		return mSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 * 
	 * @param pSuccessTemplateEmailInfo
	 *            the successTemplateEmailInfo to set
	 */
	public void setSuccessTemplateEmailInfo(TemplateEmailInfoImpl pSuccessTemplateEmailInfo) {
		mSuccessTemplateEmailInfo = pSuccessTemplateEmailInfo;
	}

	/**
	 * @return the messageSubject
	 */
	public String getMessageSubject() {
		return mMessageSubject;
	}

	/**
	 * @param pMessageSubject the messageSubject to set
	 */
	public void setMessageSubject(String pMessageSubject) {
		mMessageSubject = pMessageSubject;
	}

}
