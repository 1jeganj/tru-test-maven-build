/**
 * 
 */
package com.tru.commerce.constants;



/**
 * The Class TRUCheckoutConstants.
 *
 * @author PA
 * @version 1.0
 */
public class TRUCheckoutConstants {
	/** Constant for NO_INSTORE_PICKUP. */
	public static final String NO_INSTORE_PICKUP = "InStorePickupShippingGroup is not there";
	/** Constant for BRAND_ITEM. */
	public static final String PARAM_ORDER = "Order";
	/** Constant for PHONE_NUMBER. */
	public static final String PHONE_NUMBER = "phoneNumber";
	/** Constant for email. */
	public static final String EMAIL = "email";
	/** Constant for alternate first name. */
	public static final String ALT_FIRST_NAME = "altFirstName";
	/** Constant for alternate last name. */
	public static final String ALT_LAST_NAME = "altLastName";
	/** Constant for alternate address required. */
	public static final String IS_ALT_ADDR_REQ = "altAddrRequired";
	/** Constant for alternate phone number. */
	public static final String ALT_PHONE_NUMBER = "altPhoneNumber";
	/** Constant for alternate email. */
	public static final String ALT_EMAIL = "altEmail";
	/** Constant for PAYMENTG_GROUP_MANAGER. */
	public static final String PAYMENTG_GROUP_MANAGER = "PaymentGroupManager";
	/** Holds string  Constant showMeGiftingOptions. */
	public static final String SHOW_ME_GIFTING_OPTIONS= "showMeGiftingOptions";
	/** Holds string  Constant shipping-tab. */
	public static final String SHIPPING_TAB= "shipping-tab";
	/** Holds string  Constant gifting_tab. */
	public static final String GIFTING_TAB= "gifting-tab";
	/** Holds string  Constant pickup-tab. */
	public static final String PICKUP_TAB= "pickup-tab";
	/** Holds string  Constant payment-tab. */
	public static final String PAYMENT_TAB= "payment-tab";
	/** Holds string  Constant review-tab. */
	public static final String REVIEW_TAB= "review-tab";
	/** Constant for ORDER. */
	public static final String ORDER = "order";
	/** Constant for ORDER. */
	public static final int SIZE_ZERO = 0 ;
	/** Constant for Int sixteen. */
	public static final int INT_SIXTEEN = 16 ;
	/** Constant for PARAM PICKUP SHIPPING GRUOUP. */
	public static final String IN_STORE_PICKUP_SHIP_GRP  = "inStorePickupShipGrps";
	/** Constant for output OPARAM. */
	public static final String OPARAM_OUTPUT  = "output";
	/** Constant for empty OPARAM. */
	public static final String OPARAM_EMPTY  = "empty";
	/** Constant for empty Phone Number. */
	public static final String PROP_PHONE_NUMBER  = "phoneNumber";
	/** Constant for empty Email. */
	public static final String PROP_EMAIL  = "email";
	/** Constant for count zero. */
	public static final int COUNT_ZERO  = 0;
	/** Holds string  Constant reward number. */
	public static final String REWARD_NUMBER= "rewardNumber";
	/** Holds string  Constant ITEM DESCRIPTOR. */
	public static final String PROP_ITEM_DESCRIPTOR= "itemDescriptor";
	/** Holds string  Constant property id. */
	public static final String PROP_ID = "id";
	/** Holds string  Constant element Name. */
	public static final String ELEMENT_NAME = "elementName";
	
	/** Constant for MINIMUM PRICE . **/
	public static final Double MINIMUM_PRICE = 0.0;	
	/** Holds string  Constant element Name. */
	public static final String NUMBER_SYMBOL = "#.##";
	
	/**
	 * Constant to hold payPal
	 */
	public static final String PAYPAL_CARD = "payPal";
	/**
	 * Constant to hold applepay
	 */
	public static final String APPLE_PAY  = "applePay";
	/** Constant for in store payment method. */
	public static final String IN_STORE_PAYMENT = "inStorePayment";
	/** Constant to hold property orderId. */
	public static final String PROP_ORDER_ID = "orderId";
	/** Constant to hold property messageType. */
	public static final String PROP_MESSAGE_TYPE = "messageType";
	/** Constant to hold property messageData. */
	public static final String PROP_MESSAGE_DATA = "messageData";
	/** Constant to hold property auditDate. */
	public static final String PROP_AUDIT_DATE = "auditDate";
	/** Constant to hold property emailAuditId. */
	public static final String PROP_EMAIL_AUDIT_ID = "emailAuditId";
	/** Constant to hold property emailSender. */
	public static final String PROP_EMAIL_SENDER = "emailSender";
	/** Constant to hold property emailType. */
	public static final String PROP_EMAIL_TYPE = "emailType";
	/** Constant to hold property status. */
	public static final String PROP_STATUS = "status";
	/** Constant to hold property messageSendingResponse. */
	public static final String PROP_MES_SENDING_RESPONSE = "messageSendingResponse";
	/** Constant to hold property createdTimestamp. */
	public static final String PROP_CREATED_TIME_STAMP = "createdTimestamp";
	
	/** Stores constant for space. **/
	public static final String SPACE = " ";
	
	/**
     * Constant to hold the USER_MESSAGE_BUNDLE.
     */
    public static final String USER_MESSAGE_BUNDLE = "atg.commerce.order.UserMessages";
    
    /** Stores constant for EMPTY_STRING. **/
	public static final String EMPTY_STRING = "";
	/** Constant holds empty gift card details form name. */
	public static final String MY_CHECKOUT_EMPTY_GIFT_CARD_FORM = "giftCardWithEmptyForm";
	/** Constant holds InventoryManager. */
	public static final String INVENTORY_MANAGER = "InventoryManager";
	/** Constant holds CatalogTools. */
	public static final Object CATALOG_TOOLS = "CatalogTools";
	/** Constant holds six month. */
	public static final int CHECKOUT_FINANCING_SIX_MONTHS = 6;
	/** Constant holds twelve month. */
	public static final int CHECKOUT_FINANCING_TWELVE_MONTHS = 12;
	/** Constant holds six month financing threshold property name. */
	public static final String THRES_SIX_MONTH_PROP_NAME = "thresholdSixMonthFinancing";
	/** Constant holds twelve month financing threshold property name. */
	public static final String THRES_TWELVE_MONTH_PROP_NAME = "thresholdTwelveMonthFinancing";
	/** Constant holds index two. */
	public static final int INDEX_TWO = 2;
	/** Constant holds index four. */
	public static final int INDEX_FOUR = 4;

	/** Constant for INT_ONE. */
	public static final int INT_ONE = 1;
	
	/** Constant for INT_ONE. */
	public static final String CHAR_DOT = ".";
	/** Constant for SDP_PARAM. */
	public static final String SDP_PARAM="SDP";
	/** Constant for SYNCHRONY_SDP_RESPONSE_OBJ. */
	public static final String SYNCHRONY_SDP_RESPONSE_OBJ="synchronySDPResponseObj";
	/** Constant for UTF_EIGHT. */
	public static final String UTF_EIGHT="UTF-8";
	/** Constant for SYNCHRONY_APP_DECISSION_A. */
	public static final String SYNCHRONY_APP_DECISSION_A = "A";
	/** Constant for NULL. */
	public static final String NULL = "NULL";
	/** Constant for IS_REQ_FROM_AJAX. */
	public static final String IS_REQ_FROM_AJAX = "isReqFromAjax";
	/** Constant for LOCATION_ID. */
	public static final String LOCATION_ID = "locationId";
	/** Constant for FAV_STORE. */
	public static final String FAV_STORE = "favStore";
	/** Constant for TRUE. */
	public static final String TRUE = "true";
	/** Constant for COMMA_SEPERATOR. */
	public static final String COMMA_SEPERATOR = ",";
	/** Constant for STORE_AVAI_MSG. */
	public static final String STORE_AVAI_MSG = "storeAvailMsg";
	/** Constant for CHAR_PLUS. */
	public static final String CHAR_PLUS = "+";
	/** Constant for FORMAT_FOURTEEN_DIGIT. */
	public static final String FORMAT_FOURTEEN_DIGIT = "%014d";
	
	/** Constant for DATE_FORMAT. */
	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	
	/** The Constant CREDITCARD. */
	public static final String CREDITCARD = "creditCard";
	
	/** The Constant REVIEW_PAGE. */
	public static final String REVIEW_PAGE = "Review";
	
	/** The Constant GIFT_CARD. */
	public static final String GIFT_CARD = "Gift Card:";
	
	/** The Constant CREDIT_CARD. */
	public static final String CREDIT_CARD = "Credit Card:";
	
	/** The Constant PAY_PAL. */
	public static final String PAY_PAL = "Pay Pal:";
	
	/** The Constant ORDER_RESOURCES. */
	public static final String ORDER_RESOURCES = "atg.commerce.order.OrderResources";
	
	/** The Constant LOCALE. */
	public static final String LOCALE = "Locale";
	
	/** The Constant CANT_FIND_PRODUCT. */
	public static final String CANT_FIND_PRODUCT = "CantFindProduct";
	
	/** The Constant INVALID_ORDER_PARAMETER. */
	public static final String INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	
	/** The Constant PROC_VALIDATE_CHANNEL_HARDGOOD_SHIPPING_GROUP. */
	public static final String PROC_VALIDATE_CHANNEL_HARDGOOD_SHIPPING_GROUP = "ProcValidateChannelHardgoodShippingGroup";
	
	/** The Constant INVALID_SHIPPING_GROUP_PARAMETER. */
	public static final String INVALID_SHIPPING_GROUP_PARAMETER = "InvalidShippingGroupParameter";
	
	/** The Constant OUT_OF_STOCK_ITEMS. */
	public static final String OUT_OF_STOCK_ITEMS = "outOfStockItems";
	/** The Constant IP_ADDRESS. */
	public static final String IP_ADDRESS = "ipAddress";
	/** The Constant PLEASE_TRY_AGAIN_AFTER_SOME_TIME. */
	public static final String PLEASE_TRY_AGAIN_AFTER_SOME_TIME = "Please try again after some time.";
	/** The Constant INVALID_GIFT_CARD_PIN_ENTERTED_PLEASE_ENTER_CORRECT_GIFT_CARD_PIN. */
	public static final String INVALID_GIFT_CARD_PIN_ENTERTED_PLEASE_ENTER_CORRECT_GIFT_CARD_PIN = 
			"Invalid gift card pin enterted.Please enter correct gift card pin.";
	
	/** The Constant INVALID_GIFT_CARD_NUMBER_ENTERTED_PLEASE_ENTER_VALID_GIFT_CARD_NUMBER. */
	public static final String INVALID_GIFT_CARD_NUMBER_ENTERTED_PLEASE_ENTER_VALID_GIFT_CARD_NUMBER =
			"Invalid gift card number enterted.Please enter valid gift card number.";
	/** The Constant REVERSE_AUTHORIZE. */
	public static final String REVERSE_AUTHORIZE = "reverseAuthorize:";
	/** The Constant ATTEMPT_TO_REVERSE_MORE_THAN_AUTHORIZE_AMT. */
	public static final String ATTEMPT_TO_REVERSE_MORE_THAN_AUTHORIZE_AMT = "AttemptToReverseMoreThanAuthorizeAmt";
	/** The Constant AUTHORIZED. */
	public static final String AUTHORIZED = "authorized";
	/** The Constant REV_AUTH_FAILURE. */
	public static final String REV_AUTH_FAILURE = "REV_AUTH_FAILURE";
	/** The Constant JOE_SMITH. */
	public static final String JOE_SMITH = "Joe Smith";
	/** The Constant TRU_CHECKOUT_ERROR_GENERAL_ERROR. */
	public static final String TRU_CHECKOUT_ERROR_GENERAL_ERROR = "tru_error_checkout_transaction_status_empty";
	/** The Constant USD. */
	public static final String USD = "USD";
	/** The Constant TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR. */
	public static final String TRU_ERROR_CHECKOUT_CREDIT_GENERAL_ERROR = "tru_error_checkout_paymentprocessor_timeout";
	/** The Constant TRU_ERROR_CHECKOUT_CREDIT_TRANSACTION_STATUS_EMTPY. */
	public static final String TRU_ERROR_CHECKOUT_CREDIT_TRANSACTION_STATUS_EMTPY = "tru_error_checkout_credit_transaction_status_empty";
	/** The Constant TRU_ERROR_UNABLE_TO_ADD_THIS_CARD. */
	public static final String TRU_ERROR_UNABLE_TO_ADD_THIS_CARD = "tru_error_unable_to_add_card";
	/** The Constant TRU_ERROR_CARD_CAN_NOT_BE_USED. */
	public static final String TRU_ERROR_CARD_CAN_NOT_BE_USED = "tru_error_card_can_not_be_used";
	/** The Constant CURRENT_ORDER. */
	public static final String CURRENT_ORDER = "currentOrder";
	/** The Constant SHIPPING_GROUP_MAP_CONTAINER. */
	public static final String SHIPPING_GROUP_MAP_CONTAINER = "shippingGroupMapContainer";
	/** The Constant PAYMENT_GROUP_MAP_CONTAINER. */
	public static final String PAYMENT_GROUP_MAP_CONTAINER = "paymentGroupMapContainer";
	/** The Constant STATE2. */
	public static final String STATE2 = "state";
	/** The Constant ADDRESS12. */
	public static final String ADDRESS12 = "address1";
	/** The Constant ORDER2. */
	public static final String ORDER2 = "Order";
	/** The Constant CHAR9. */
	public static final char CHAR9 = '9';
	/** The Constant CHAR8. */
	public static final char CHAR8 = '8';
	/** The Constant CHAR7. */
	public static final char CHAR7 = '7';
	/** The Constant CHAR6. */
	public static final char CHAR6 = '6';
	/** The Constant CHAR5. */
	public static final char CHAR5 = '5';
	/** The Constant CHAR4. */
	public static final char CHAR4 = '4';
	/** The Constant CHAR3. */
	public static final char CHAR3 = '3';
	/** The Constant CHAR2. */
	public static final char CHAR2 = '2';
	/** The Constant CHAR1. */
	public static final char CHAR1 = '1';
	
	/** The Constant CHAR_OR. */
	public static final char CHAR_OR = '|';
	
	
	/** The Constant 0. */
	public static final int INT_ZERO = 0;
	/** The Constant CHAR0. */
	public static final char CHAR0 = '0';
	
	/** The Constant ERROR_COUSERNAME_EMPTY. */
	public static final String ERROR_COUSERNAME_EMPTY = "tru_error_checkout_channel_co_user_name_empty";
	
	/** The Constant CHANNEL_COUSERNAME. */
	public static final String CO_USERNAME = "channelCoUserName";
	
	/** The Constant ERROR_CHANNELID_EMPTY. */
	public static final String ERROR_CHANNELID_EMPTY = "tru_error_checkout_channel_id_empty";
	
	/** The Constant CHANNEL_CHANNELID. */
	public static final String CHANNEL_ID = "channelId";
	
	/** The Constant ERROR_CHANNELNAME_EMPTY. */
	public static final String ERROR_CHANNELNAME_EMPTY = "tru_error_checkout_channel_name_empty";
	
	/** The Constant CHANNEL_CHANNELNAME. */
	public static final String CHANNEL_NAME = "channelName";
	
	/** The Constant ERROR_CHANNELTYPE_EMPTY. */
	public static final String ERROR_CHANNELTYPE_EMPTY = "tru_error_checkout_channel_type_empty";
	
	/** The Constant CHANNEL_CHANNELTYPE. */
	public static final String CHANNEL_TYPE = "channelType";
	
	/** The Constant ERROR_CHANNELUSERNAME_EMPTY. */
	public static final String ERROR_CHANNELUSERNAME_EMPTY = "tru_error_checkout_channel_user_name_empty";
	
	/** The Constant CHANNEL_CHANNELUSERNAME. */
	public static final String USER_NAME = "channelUserName";

	/** The Constant PROC_VALIDATE_CHANNEL_INSTORE_PICKUP_SHIPPING_GROUP. */
	public static final String PROC_VALIDATE_CHANNEL_INSTORE_PICKUP_SHIPPING_GROUP = "ProcValidateChannelInStorePickupShippingGroup";

	
	/** The Constant SHIP_LOCATION_MISSING. */
	public static final String SHIP_LOCATION_MISSING = "ShipLocationMissing";
	
	/** The Constant MISSING_SHIPPING_LOCATION_ID. */
	public static final String MISSING_SHIPPING_LOCATION_ID = "MissingShippingLocationId";
	
	/** The Constant SHIPPING_LOCATION_INVALID. */
	public static final String SHIPPING_LOCATION_INVALID = "ShipLocationInvalid";
	
	/** The Constant INVALID_SHIPPING_LOCATION_ID. */
	public static final String INVALID_SHIPPING_LOCATION_ID = "InvalidShippingLocationId";
	
	/** The Constant ONLINE_ONLY_ITEM_WITH_INSTORE_SHIPPING. */
	public static final String ONLINE_ONLY_ITEM_WITH_INSTORE_SHIPPING = "OnlineOnlyItemWithInStoreShipping";
	
	/** The Constant PROC_VALIDATE_INVENTORY_FOR_CHECKOUT. */
	public static final String PROC_VALIDATE_INVENTORY_FOR_CHECKOUT = "ProcValidateInventoryForCheckout";
	
	/** The Constant PROC_VALIDATE_ONLY_INSTORE_FOR_CHECKOUT. */
	public static final String PROC_VALIDATE_ONLY_INSTORE_FOR_CHECKOUT = "ProcTRUValidateOnlyInStoreForCheckout";
	
	/** Constant for ORDER_MANAGER. */
	public static final String ORDER_MANAGER = "OrderManager";
	
	/** Constant for INVALID_INSTORE_PAYMENT_USE. */
	public static final String INVALID_INSTORE_PAYMENT_USE = "InvalidInStorePaymentUse";
	/** Constant for DOUBLE ZERO. */
	public static final double DOUBLE_ZERO = 0.0;
	/** Constant for GIFTCARD. */
	public static final String GIFTCARD = "giftcard";
	/** Constant for ID_0. */
	public static final String ID_0 = "id = ?0";
	/** Constant for TYPE2. */
	public static final String TYPE2 = "type";
	/** Constant for CART_ITEM_DETAIL_VO */
	public static final String CART_ITEM_DETAIL_VO = "cartItemDetailVO";
	
	/** Constant for VALID_SHIPPING_GROUP_MAP */
	public static final String VALID_SHIPPING_GROUP_MAP= "validShippingGroupsMapForDisplay";
	
	/** Constant for VALID_SHIPPING_GROUP_MAP */
	public static final String SHIP_GROUP_KEY_MAP= "shipGroupKeyMap";
	
	/** Constant for VALID_SHIPPING_ADDRESS_MAP */
	public static final String VALID_SHIPPING_ADDRESS_MAP= "validShippingAddressMap";
	
	/** Constant for SELECTED_ADDRESS_NICK_NAME */
	public static final String SELECTED_ADDRESS_NICK_NAME= "selectedAddressNickName";
	
	/** Constant for SELECTED_SHIPPING_ADDRESS */
	public static final String SELECTED_SHIPPING_ADDRESS= "selectedShippingAddress";
	
	/** Constant for INT_TWENTY_FOUR */
	public static final int INT_TWENTY_FOUR = 24;
	
	/** Constant for PROP_SHIP_WINDOW_MAX */
	public static final String PROP_SHIP_WINDOW_MAX = "shipWindowMax";
	
	/** Constant for PROP_ELIGIBLE_SHIP_REGIONS */
	public static final String PROP_ELIGIBLE_SHIP_REGIONS = "eligibleShipRegions";
	
	/** Constant for PROP_SHIP_WINDOW_MIN */
	public static final String PROP_SHIP_WINDOW_MIN = "shipWindowMin";
	
	/** Constant for COUNT_COOKIE */
	public static final String COUNT_COOKIE = "countcookie";
	
	/** Constant for COOKIE_NAME */
	public static final String COOKIE_NAME = "cookieName";
	
	/** Constant for COOKIE_VALUE */
	public static final String COOKIE_VALUE = "cookieValue";
	
	/** Constant for GIFT_FINDER */
	public static final String GIFT_FINDER = "giftFinder";
	
	/** The Constant HASH_SYMBOL. */
	public static final String SLASH_SYMBOL = "/";
	
	/** The Constant INVALID_GIFT_CARD_NO. */
	public static final String INVALID_GIFT_CARD_NO = "tru_error_invalid_gift_card_no";
	
	/** The Constant GIFT_CARD_ERR. */
	public static final String GIFT_CARD_ERR = "GIFT_CARD_ERR";
	
	/** The Constant INVALID_GIFT_CARD. */
	public static final String INVALID_GIFT_CARD_PIN = "tru_error_invalid_gift_card_pin";
	
	/** The Constant GIFT_CARD_BALANCE. */
	public static final String GIFT_CARD_BALANCE = "tru_error_gift_card_balance";
	
	/** The Constant INSUFF_GIFT_CARD_BALANCE. */
	public static final String INSUFF_GIFT_CARD_BALANCE = "tru_error_insuff_gift_card_balance";
	
	/** The Constant EXPECTED_GIFT_CARD. */
	public static final String EXPECTED_GIFT_CARD = "Expected a GiftCard payment group, but got";
	
	/** The Constant INSTEAD. */
	public static final String INSTEAD = " instead.";
	 
	/** The Constant INSTEAD. */
	public static final String CLASS_NOT_RECOGNIZED = "ClassNotRecognized";
	
	/** The Constant CREDIT_CARD_NUMBER. */
	public static final String CREDIT_CARD_NUMBER = "creditCardNumber";
	
	/** The Constant PROBLEM_WITH_PAYPAL. */
	public static final String PROBLEM_WITH_PAYPAL = "There is a problem using PayPal. Please try with other payment method.";
	
	/** The Constant ZERO_BRACES. */
	public static final String ZERO_BRACES = "{0}";
	
	/** The Constant ADDRESS_WITHCHAINNAME. */
	public static final String ADDRESS_WITHCHAINNAME = "addressWithChianName";
	
	/** The Constant cvv */
	public static final String TRU_CHECKOUT_ERROR_CVV_NOT_DEFINED_IN_LIST = "tru_error_checkout_credit_card_cvv_not_defined_in_list";

	/** The Constant avs. */
	public static final String TRU_CHECKOUT_ERROR_AVS_NOT_DEFINED_IN_LIST = "tru_error_checkout_credit_card_avs_not_defined_in_list";
	
	/** The Constant DEFAULT_SHIPPING_ADDRESS. */
	public static final String DEFAULT_SHIPPING_ADDRESS = "defaultShippingAddress";
	
	/** The Constant REGISTRANT_ID. */
	public static final String REGISTRANT_ID = "registrantId";
	
	/** The Constant CHANNEL REGISTRANT ID. */
	public static final String ERROR_REGISTRANT_ID = "tru_error_checkout_channel_registrant_id_empty";
	
	public static final String ACTION_CODE = "SUCCESS";
	public static final String ECI_FLAG = "07";
    /** The Constant error for validation status not in list */
    public static final String TRU_CHECKOUT_ERROR_VALIDATION_STATUS_NOT_IN_LIST = "tru_error_checkout_validation_status_not_in_list";
    
    /** The Constant error for transaction status not in list */
    public static final String TRU_CHECKOUT_ERROR_TRANSACTION_STATUS_NOT_IN_LIST = "tru_error_checkout_transaction_status_not_in_list";
    
    /** The Constant PROP_LAYAWAY_OMS_ORDER_ID. */
	public static final String PROP_LAYAWAY_ORDER_ID = "layawayOrderid";
    
    /** The Constant PROP_LAYAWAY_OMS_ORDER_ID. */
	public static final String PROP_LAYAWAY_OMS_ORDER_ID = "layawayOMSOrderId";
	
	/** The Constant PROP_LAYAWAY_CUST_NAME. */
	public static final String PROP_LAYAWAY_CUST_NAME = "layawayCustName";
	
	/** The Constant PROP_LAYAWAY_PROFILE_ID. */
	public static final String PROP_LAYAWAY_PAYMENT_AMOUNT = "paymentAmount";
	
	/** The Constant PROP_LAYAWAY_DUE_AMOUNT. */
	public static final String PROP_LAYAWAY_DUE_AMOUNT = "layawayDueAmount";
	
	/** The Constant LAYAWAY_CUST_EMAIL. */
	public static final String PROP_LAYAWAY_PROFILE_ID = "profileId";
	
	/** The Constant PROP_ORDER_CLASS_TYPE. */
	public static final String PROP_LAYAWAY_ORDER_CLASS_TYPE = "orderClassType";
	
	/** The Constant PROP_LAYAWAY_ORDER_SITE_ID. */
	public static final String PROP_LAYAWAY_ORDER_SITE_ID = "siteId";
	
	/** The Constant PROP_LAYAWAY_FIRST_NAME. */
	public static final String PROP_LAYAWAY_FIRST_NAME = "firstName";
	
	/** The Constant PROP_LAYAWAY_LAST_NAME. */
	public static final String PROP_LAYAWAY_LAST_NAME = "lastName";
	
	/** The Constant PROP_LAYAWAY_ADDRESS1. */
	public static final String PROP_LAYAWAY_ADDRESS1 = "address1";
	
	/** The Constant PROP_LAYAWAY_ADDRESS2. */
	public static final String PROP_LAYAWAY_ADDRESS2 = "address2";
	
	/** The Constant PROP_LAYAWAY_CITY. */
	public static final String PROP_LAYAWAY_CITY = "city";
	
	/** The Constant PROP_LAYAWAY_STATE. */
	public static final String PROP_LAYAWAY_STATE = "addrState";
	
	/** The Constant PROP_LAYAWAY_POSTAL_CODE. */
	public static final String PROP_LAYAWAY_POSTAL_CODE = "postalCode";
	
	/** The Constant PROP_LAYAWAY_COUNTRY. */
	public static final String PROP_LAYAWAY_COUNTRY = "country";
	
	/** The Constant PROP_LAYAWAY_PHONE_NUMBER. */
	public static final String PROP_LAYAWAY_PHONE_NUMBER = "phoneNumber";
	
	/** The Constant PROP_LAYAWAY_EMAIL. */
	public static final String PROP_LAYAWAY_EMAIL = "email";
	
	/** The Constant for creditCardsMap */
	public static final String CREDIT_CARDS_MAP = "creditCardsMap";
	
	/** The Constant for defaultCreditCardName */
	public static final String DEFAULT_CREDIT_CARD_NAME = "defaultCreditCardName";
	
	/** The Constant for REST_CONTEXT_PATH */
	public static final String REST_CONTEXT_PATH = "/rest";

	/** The Constant for showMergeCartOverlay */
	public static final String SHOW_MERGE_CART_OVERLAY = "showMergeCartOverlay";

	/** The Constant for redirectToShipping */
	public static final String REDIRECT_TO_SHIPPING = "redirectToShipping";
	/** The Constant for _0L */
	public static final long _0L = 0L;
	/** The Constant for TRU_VALIDATE_FOR_CHECKOUT_CHAIN */
	public static final String TRU_VALIDATE_FOR_CHECKOUT_CHAIN = "truValidateForCheckoutChain";
	/** The Constant for STRING_UNDERSCORE */
	public static final String STRING_UNDERSCORE = "-";
	/** The Constant for TRU_ERROR_CHECKOUT_NEGATIVE_FULL_AUTHORIZATION_FAILED */
	public static final String TRU_ERROR_CHECKOUT_NEGATIVE_FULL_AUTHORIZATION_FAILED = "tru_error_checkout_negative_full_authorization_failed";
	/** The Constant for FAILED_USE_COUPON */
	public static final String FAILED_USE_COUPON = "failedUseCoupon";
	/** The Constant for ALREADY_USED */
	public static final String ALREADY_USED = " already used";
	/** The Constant for THE_COUPON_NO */
	public static final String THE_COUPON_NO = "The Coupon No ";
	/** The Constant for VOID_TRANSACTION_FAILED */
	public static final String VOID_TRANSACTION_FAILED = "VOID_TRANSACTION_FAILED";

	/** The Constant commitOrder. */
	public static final String COMMIT_ORDER = "commitOrder";
	/** The Constant moveToReviewOrder. */
	public static final String MOVE_TO_REVIEW_ORDER = "moveToReviewOrder";
	/** The Constant sourceOfOrder. */
	public static final String SOURCE_OF_ORDER = "sourceOfOrder";
	/** The Constant INVENTORY_UPDATE_FAILED. */
	public static final String INVENTORY_UPDATE_FAILED = "Inventory Update Failed";
	/** The Constant INVENTORY. */
	public static final Object INVENTORY = "Inventory";
	/** The Constant ORDER_ID. */
	public static final String ORDER_ID = "Order Id:";
	/** The Constant INVENTORY_STATUS_INSUFFICIENT_SUPPLY. */
	public static final String INVENTORY_STATUS_INSUFFICIENT_SUPPLY = "tru_error_inventory_notavailable";

	/** The Constant allowEmptyOrders. */
	public static final String ALLOW_EMPTY_ORDERS = "allowEmptyOrders";
	
	/** The Constant DECLINED. */
	public static final String DECLINED = "Declined";
	
	/** The Constant ERROR_COMMITTING_ORDER. */
	public static final String ERROR_COMMITTING_ORDER = "errorCommittingOrder";
	
	/** The Constant VALIDATE_PAYMENT_GROUP. */
	public static final String VALIDATE_PAYMENT_GROUP = "validatePaymentGroup";
	
	/** The Constant PROC_TRU_VALIDATE_LAYAWAY_ORDER_PG. */
	public static final String PROC_TRU_VALIDATE_LAYAWAY_ORDER_PG = "ProcTRUValidateLayawayPGForCheckout";
	
	/** The Constant for FAILED. */
	public static final String FAILED = "failed";
	
	/** The Constant for FINANCE_AGREED. */
	public static final String FINANCE_AGREED = "financeAgreed";
	
	/** The Constant for FINANCING_TERMS_AVAILED. */
	public static final String FINANCING_TERMS_AVAILED = "financingTermsAvailed";
	
	/** Constant to hold CODE TWO_ZEROS_STRING. */
	public static final String TWO_ZEROS_STRING = "00";
	
	/** Constant to hold CODE RUS_COB_MASTER_CARD. */
	public static final String RUS_COB_MASTER_CARD = "RUSCoBrandedMasterCard";
	
	/** Constant to hold CODE RUS_PRIVATE_LABEL_CARD. */
	public static final String RUS_PRIVATE_LABEL_CARD = "RUSPrivateLabelCard";
	
	/** Constant to hold CODE PL_TRU. */
	public static final String PL_TRU = "PL_TRU";
	
	/** Constant to hold CODE TIME_ZONE. */
	public static final String TIME_ZONE = "America/New_York";
	/** Constant to hold TEST. */
	public static final String TEST = "test";
	/** Constant to hold TRUE_CLIENT_IP. */
	public static final String TRUE_CLIENT_IP = "True-Client-IP";
	/** Constant to hold _10. */
	public static final int _10 = 10;
	/** Constant to hold SUCCESS. */
	public static final String SUCCESS = "success";
	/** The Constant NO_ACCOUNT. */
	public static final String NO_ACCOUNT = "No Account";
	/** The Constant Restraint. */
	public static final String RESTRAINT = "Restraint";
	/** The Constant enteredBy. */
	public static final String ENTERED_BY = "enteredBy";
	/** The Constant enteredLocation. */
	public static final String ENTERED_LOCATION = "enteredLocation";	
	
	/** The Constant ORDER_IS_NULL. */
	public static final String ORDER_IS_NULL = "Order is null";
	
	/** The Constant PROFILE_IS_NULL. */
	public static final String PROFILE_IS_NULL = "Profile is null";
	
	/** The Constant SGMC_IS_NULL. */
	public static final String SGMC_IS_NULL = "ShippingGroupMapContainer is null";
	
	/** The Constant NICKNAME_IS_NULL. */
	public static final String NICKNAME_IS_NULL = "RemoveShippingAddressNickName is null/empty";
	
	/** The Constant for APPLY_EXPRESS_CHECKOUT. */
	public static final String APPLY_EXPRESS_CHECKOUT = "applyExpressCheckout";
	/** The Constant for one. */
	public static final int _1 = 1;
	
	/** The Constant AUTHORIZE_SCORE. */
	public static final String AUTHORIZE_SCORE = "authorize_score";
	
	/** The Constant DEVICE_ID. */
	public static final String DEVICE_ID = "deviceID";
	
	/** The Constant BROWSER_ID. */
	public static final String BROWSER_ID = "browserID";
	
	/** The Constant BROWSER_SESSION_ID. */
	public static final String BROWSER_SESSION_ID = "browserSessionId";
	
	/** The Constant BROWSER_CONNECTION. */
	public static final String BROWSER_CONNECTION = "browserConnection";
	
	/** The Constant BROWSER_ACCEPT. */
	public static final String BROWSER_ACCEPT = "browserAccept";
	
	/** The Constant BROWSER_ACCEPT_ENCODING. */
	public static final String BROWSER_ACCEPT_ENCODING = "browserAcceptEncoding";
	
	/** The Constant BROWSER_ACCEPT_CHARSET. */
	public static final String BROWSER_ACCEPT_CHARSET = "browserAcceptCharset";
	
	/** The Constant BROWSER_ID_LANGUAGE_CODE. */
	public static final String BROWSER_ID_LANGUAGE_CODE = "browserIdLanguageCode";
	
	/** The Constant BROWSER_COOKIE. */
	public static final String BROWSER_COOKIE = "browserCookie";
	
	/** The Constant BROWSER_REFERER. */
	public static final String BROWSER_REFERER = "browserReferer";
	
	/** The Constant CUSTOMER_IP_ADDRESS. */
	public static final String CUSTOMER_IP_ADDRESS = "customerIPAddress";
	
	/** The Constant SERVER_DATE_TIME. */
	public static final String SERVER_DATE_TIME = "serverDateTime";
	
	/** The Constant TIME_SPENT_ONSITE. */
	public static final String TIME_SPENT_ONSITE = "timeSpentOnSite";
	
	/** The Constant USER_AGENT. */
	public static final String USER_AGENT = "User-Agent";
	
	/** The Constant JSESSIONID. */
	public static final String JSESSIONID = "JSESSIONID";
	
	/** The Constant TRUSESSIONID. */
	public static final String TRUSESSIONID = "TRUSESSIONID";
	
	/** The Constant CONNECTION. */
	public static final String CONNECTION = "Connection";
	
	/** The Constant ACCEPT. */
	public static final String ACCEPT = "Accept";
	
	/** The Constant ACCEPT_ENCODING. */
	public static final String ACCEPT_ENCODING = "Accept-Encoding";
	
	/** The Constant CONTENT_ENCODING. */
	public static final String CONTENT_ENCODING = "Content-Encoding";
	
	/** The Constant CONTENT_TYPE. */
	public static final String CONTENT_TYPE = "Content-Type"; 
	
	/** The Constant ACCEPT_LANGUAGE. */
	public static final String ACCEPT_LANGUAGE = "Accept-Language";
	
	/** The Constant COOKIE. */
	public static final String COOKIE = "Cookie";
	
	/** The Constant REFERER. */
	public static final String REFERER = "Referer";
	
	/** Constant for Int 4000. */
	public static final int INT_FOUR_ZERO_ZERO_ZERO = 4000;
	
	/** The Constant HH_MM_SS. */
	public static final String HH_MM_SS = "HH:mm:ss";
	
	/** The Constant TIME_SPENT_ONSITE_FORMAT. */
	public static final String TIME_SPENT_ONSITE_FORMAT = "%02d%02d%02d";
	
	/** Constant for Int 60. */
	public static final int INT_SIX_ZERO = 60;
	
	/** The Constant HOST_NAME. */
	public static final String HOST_NAME = "hostName";
	
	/** The Constant SOFT_AMOUNT_ALLOCATED. */
	public static final String SOFT_AMOUNT_ALLOCATED = "softAmountAllocated";
	
	/** The Constant RESERVATION_ID. */
	public static final String RESERVATION_ID = "reservationId";
	
		/** The status success. */
	public static final String STATUS_SUCCESS = "SUCCESS";
	
	/** The status failed. */
	public static final String STATUS_FAILED = "FAILED";
	
	/** The status new. */
	public static final String STATUS_NEW = "NEW";
	
	/** The status retry. */
	public static final String STATUS_RETRY = "RETRY";
	
	/** The is merge order. */
	public static final String IS_MERGE_ORDER = "isMergeOrder";
	
	/** The is OrderBillingAddress. */
	public static final String ORDER_BILLING_ADDRESS = "OrderBillingAddress";
	
	/** Constant for Int Ninety. */
	public static final int INT_NINETY = 90 ;
	
	/** Constant for JAXB_FORMATTED_OUTPUT. */
	public static final String JAXB_FORMATTED_OUTPUT = "jaxb.formatted.output";
	
	/** Constant for STRING_BA. */
	public static final String STRING_BA = "_BA";
	
	/** Constant for REST_SERVICE. */
	public static final String REST_SERVICE ="restService";
	
	/** The Constant NONCE. */
	public static final String NONCE ="nonce";
	
	/** The Constant EXPIRES_IN_SECONDS. */
	public static final String EXPIRES_IN_SECONDS ="expires_in_seconds";
	
	/** The Constant JWT. */
	public static final String JWT ="jwt";
	
	/** The Constant BILLING_ADDRESS_NICK_NAME. */
	public static final String BILLING_ADDRESS_NICK_NAME = "billingAddressNickName";
	
	/** The Constant APPLEPAYPDP. */
	public static final String APPLE_PAY_PDP ="applePayPDP";
	
	/** The Constant double_colon. */
	public static final String DOUBLE_COLON = "::";
	/** The Constant APPLE_PAY_ORDER_INFO. */
	public static final String APPLE_PAY_ORDER_INFO ="applePayOrderInfo";
	/**
     * Constant to hold APPLEPAY_CHECKOUT.
     */
	public static final String APPLEPAY_CHECKOUT = "handleInitiateApplepayCheckout";
	/**
	 * Constant for CO_BROWSER_ID.
	 */
	public static final String CO_BROWSER_ID = "CO_BROWSER_ID";
	/**
	 * Constant for CO_BROWSER_SESSION_ID.
	 */
	public static final String CO_BROWSER_SESSION_ID = "CO_BROWSER_SESSION_ID";
	/**
	 * Constant for CO_BROWSER_CONNECTION.
	 */
	public static final String CO_BROWSER_CONNECTION = "CO_BROWSER_CONNECTION";
	/**
	 * Constant for CO_BROWSER_ACCEPT_ENCODING.
	 */
	public static final String CO_BROWSER_ACCEPT_ENCODING = "CO_BROWSER_ACCEPT_ENCODING";
	/**
	 * Constant for CO_BROWSER_ACCEPT_CHARSET.
	 */
	public static final String CO_BROWSER_ACCEPT_CHARSET = "CO_BROWSER_ACCEPT_CHARSET";
	/**
	 * Constant for CO_BROWSER_ID_LANGUAGE_CODE.
	 */
	public static final String CO_BROWSER_ID_LANGUAGE_CODE = "CO_BROWSER_ID_LANGUAGE_CODE";
	/**
	 * Constant for CO_BROWSER_COOKIE.
	 */
	public static final String CO_BROWSER_COOKIE = "CO_BROWSER_COOKIE";
	/**
	 * Constant for CO_CUSTOMER_IP_ADDRESS.
	 */
	public static final String CO_CUSTOMER_IP_ADDRESS = "CO_CUSTOMER_IP_ADDRESS";
	/**
	 * Constant for CO_TIME_SPENT_ON_SITE.
	 */
	public static final String CO_TIME_SPENT_ON_SITE = "CO_TIME_SPENT_ON_SITE";
	/**
	 * Constant for CO_BROWSER_ACCEPT.
	 */
	public static final String CO_BROWSER_ACCEPT = "CO_BROWSER_ACCEPT";
	/**
	 * Constant for CO_BROWSER_REFERRER.
	 */
	public static final String CO_BROWSER_REFERRER = "CO_BROWSER_REFERRER";
	
	/** The Constant TRU_RADIAL_TAX_CALCULATOR_CALCULATE_TAX. */
	public static final String TRU_RADIAL_TAX_CALCULATOR_CALCULATE_TAX = "TRURadialTaxProcessorTaxCalculator.calculateTax()";
	
	/** The Constant RADIAL_PAYMENT_ERROR_KEY. */
	public static final String RADIAL_PAYMENT_ERROR_KEY = "50002";
	/** The Constant NICKNAME MAX LENGTH. */
	public static final int NICKNAME_MAX_LENGTH = 37;
	
	/** The Constant TRU_ERROR_RADIAL_CREDIT_CARD_DISABLED. */
	public static final String TRU_ERROR_RADIAL_CREDIT_CARD_DISABLED = "tru_error_radial_credit_card_disabled";
	
	/** The Constant PRE_COMMIT_ORDER. */
	public static final String PRE_COMMIT_ORDER = "preCommitOrder";
	
	/** The Constant POST_COMMIT_ORDER. */
	public static final String POST_COMMIT_ORDER = "postCommitOrder";
}
