/**
 * 
 */
package com.tru.commerce.security;

import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.security.service.TRUTokenization;

/**
 * This custom class extend the OTB GSAPropertyDescriptor to set and get the encrypted/decrypted gift card number.
 * @author Professional Access
 * @version 1.0
 */
public class GiftCardTokenizationPropertyDescriptor extends GSAPropertyDescriptor{

	private static final long serialVersionUID = 1L;
	/**
	 * hold to property TYPE_NAME.
	 */
	private static final String TYPE_NAME = "encrypted";
	/**
	 * hold to property TOKENIZATION_COMPONENT.
	 */
	private static final String TOKENIZATION_COMPONENT = "tokenization";
	
	/**
	 * hold to property isValidGiftCard.
	 */
	boolean mIsValidGiftCard = Boolean.TRUE;

	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME, GiftCardTokenizationPropertyDescriptor.class);
	}
	private transient TRUTokenization mTokenizationService;


	/**
	 * @return property Queryable
	 */
	@Override
	public boolean isQueryable() {
		/*
		 * Encrypted properties can't be included into any repository searches.
		 * That's why <code>isQueryable</code> method must return <code>false</code>.
		 */
		return false;
	}

	/**
	 * @param pItem the item
	 * @param pValue the value to set   
	 */
	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		if (pValue == null) {
			return;
		}
		try {
			String giftCardNumber = pValue.toString();
			String tokenCardNumber = null;
			String encryptedCardNumber = null;
			if(pValue != null && StringUtils.isAlphaNumOnly(giftCardNumber)){
				try {
					encryptedCardNumber = mTokenizationService.encryptCardNumber(giftCardNumber.substring(TRUCommerceConstants.INT_FOUR, giftCardNumber.length()- TRUCommerceConstants.INT_FOUR ));
					tokenCardNumber = giftCardNumber.substring(TRUCommerceConstants.INT_ZERO, TRUCommerceConstants.INT_FOUR)+encryptedCardNumber+giftCardNumber.substring(giftCardNumber.length()- TRUCommerceConstants.INT_FOUR, giftCardNumber.length());

				} catch (RepositoryException repExe) {
					if(isLoggingError()) {
						logError(repExe);
					}
				}
			} else if(pValue != null && !StringUtils.isAlphaNumOnly(giftCardNumber)){
				mIsValidGiftCard = Boolean.FALSE;
				tokenCardNumber = giftCardNumber;
				if(isLoggingError()){
					logError(new Exception("INVALID CHARACTERS IN GIFT CARD TO SAVE : " + pValue));
				}
				//return;
			}

			super.setPropertyValue(pItem, tokenCardNumber);
		} catch (TRUIntegrationException intExce) {
			if(isLoggingError()) {
				logError(intExce);
			}
		}
	}

	/**
	 * @param pItem the item
	 * @param pValue the value to get
	 * @return the property value   
	 */
	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if((pValue == null) || (pValue.equals(RepositoryItemImpl.NULL_OBJECT))) {
			return null;
		}
		String tokenCardNumber = null;
		String giftCardNumber = pValue.toString();
		String decryptedCardNumber = null;
		try {
			if(pValue != null && mIsValidGiftCard){
				try {
					decryptedCardNumber = mTokenizationService.decryptCardNumber(giftCardNumber.substring(TRUCommerceConstants.INT_FOUR,giftCardNumber.length()- TRUCommerceConstants.INT_FOUR ));
					tokenCardNumber = giftCardNumber.substring(TRUCommerceConstants.INT_ZERO, TRUCommerceConstants.INT_FOUR)+decryptedCardNumber+giftCardNumber.substring(giftCardNumber.length()- TRUCommerceConstants.INT_FOUR,giftCardNumber.length());

				} catch (RepositoryException repExe) {
					if(isLoggingError()) {
						logError(repExe);
					}
				}
			} else {
				tokenCardNumber = giftCardNumber;
			}
		} catch (TRUIntegrationException intExce) {
			if(isLoggingError()) {
				logError(intExce);
			}
		}
		return super.getPropertyValue(pItem, tokenCardNumber);
	}
	
	  /**
	   * Associate a named attribute with this feature.
	   * @param pAttributeName the named attribute
	   * @param pValue the value
	   */
	  @Override
	  public void setValue(String pAttributeName, Object pValue) {
		  super.setValue(pAttributeName, pValue);

		  if((pValue == null) || (pAttributeName == null)) {
			  return;
		  }
		  if(pAttributeName.equalsIgnoreCase(TOKENIZATION_COMPONENT)) {
			  resolveTokenizationComponent(pValue.toString());
		  }
	  }

	  /**
	   * @param pComponentName the component name to resolve
	   */
	  private void resolveTokenizationComponent(String pComponentName) {
		  try {
			  Nucleus nucleus = Nucleus.getGlobalNucleus();
			  if (nucleus != null) {
				  mTokenizationService = (TRUTokenization) nucleus.resolveName(pComponentName);
			  }
		  } catch (ClassCastException classCastExce) {
			  if(isLoggingError()) {
				  logError(classCastExce);
			  }

		  }
	  }
}
