package com.tru.commerce;

import atg.nucleus.naming.ParameterName;

/**
 * This class hold all commerce related constants.
 * @author PA
 * @version 1.0 
 */
public class TRUCommerceConstants {

	/**
	 * Constant for DOUBLE_DOT.
	 */
	public static final String HYPHEN = "-";
	/**
	 * Constant for CHILD_WEIGHT_MAX.
	 */
	public static final String CHILD_WEIGHT_MAX = "childWeightMax";
	/**
	 * Constant for CHILD_WEIGHT_MIN.
	 */
	public static final String CHILD_WEIGHT_MIN = "childWeightMin";
	/**
	 * Constant for DOUBLE_DOT.
	 */
	public static final String DOUBLE_DOT = "..";

	/** property to hold siteIds property. */ 
	public static final String SITE_IDS = "siteIds";

	/**
	 * Constant for DOT.
	 */
	public static final String DOT = ".";

	/** constant for holding computedCatalogs. */
	public static final String COMPUTED_CATALOGS = "computedCatalogs";

	/**
	 * Constant for DEFAULT_LOCALE.
	 */
	public static final String DEFAULT_SITE_LOCALE = "en__US";
	/**
	 * Constant for BRAND_ITEM.
	 */
	public static final String BRAND_ITEM = "brandItem";
	/**
	 * Constant for BRAND_NAME.
	 */
	public static final ParameterName BRAND_NAME = ParameterName.getParameterName("brandName");
	
	/**
	 * Constant for allskuPromo.
	 */
	public static final ParameterName ALL_SKUS_PROMO = ParameterName.getParameterName("allskuPromo");
	

	/** Constant to hold the catalogId. */
	public static final String CATALOG_ID = "catalogId";

	/** Constant to hold the elementName. */
	public static final String ELEMENT_NAME = "elementName";

	/** Constant to hold the element. */
	public static final String ELEMENT = "element";

	/** Constant to hold megaMenu. */
	public static final String MEGA_MENU = "megaMenu";

	/** Constant to hold emailToFriend. */
	public static final String EMAIL_TO_FRIEND = "emailToFriend";

	/** Constant to hold the locale. */
	public static final ParameterName LOCALE = ParameterName.getParameterName("locale");

	/** Constant to hold output. */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	/** Constant to hold output. */
	public static final ParameterName OUTPUT_START = ParameterName.getParameterName("outputStart");

	/** Constant to hold empty. */
	public static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	/** Constant to hold error. */
	public static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");

	/** Constant to hold one. */
	public static final int INT_ONE = 1;

	/** Constant to hold one. */
	public static final int INT_TWO = 2;

	/** Constant to hold one. */
	public static final int INT_THREE = 3;

	/** Constant to hold one. */
	public static final int INT_FOUR = 4;

	/** Constant to hold one. */
	public static final int INT_FIVE = 5;
	
	/** The Constant INT_SIX. */
	public static final int INT_SIX=6;

	/** Constant to hold one. */
	public static final int INT_HUNDRED = 100;

	/** Constant to hold selectiveCacheInvalidationEvents. */
	public static final String MAP_KEY_SCI_EVENTS = "selectiveCacheInvalidationEvents";

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName PRODUCT_LIST = ParameterName.getParameterName("productList");

	/**
	 * Parameter Name to hold productId.
	 */
	public static final ParameterName PRODUCT_ID = ParameterName.getParameterName("productId");
	
	/**
	 * Parameter Name to hold onlinePID.
	 */
	public static final ParameterName ONLINEPID = ParameterName.getParameterName("onlineProductId");
	/**
	 * Parameter Name to hold productId.
	 */
	public static final ParameterName SKU_ID = ParameterName.getParameterName("skuId");
	/**
	 * Parameter Name to hold productId.
	 */
	public static final ParameterName QUANTITY = ParameterName.getParameterName("quantity");
	/**
	 * Parameter Name to hold collectionId.
	 */
	public static final ParameterName COLLECTION_ID = ParameterName.getParameterName("collectionId");

	/** Holds string  Constant COLLECTION. */
	public static final String COLLECTION = "collection";

	/** Holds string  Constant product. */
	public static final String TYPE_PRODUCT = "product";

	/** Holds string  Constant collectionProduct. */
	public static final String TYPE_COLLECTIONPRODUCT = "collectionProduct";
	
	/** Holds string  Constant HIDDEN. */
	public static final String DISPLAY_STATUS_HIDDEN = "HIDDEN";
	
	/** Holds string  Constant ACTIVE. */
	public static final String DISPLAY_STATUS_ACTIVE = "ACTIVE";

	/**
	 * Parameter Name to hold product.
	 */
	public static final ParameterName PRODUCT = ParameterName.getParameterName("product");

	/**
	 * Parameter Name to hold true.
	 */
	public static final ParameterName TRUE = ParameterName.getParameterName("true");

	/**
	 * Parameter Name to hold false.
	 */
	public static final ParameterName FALSE = ParameterName.getParameterName("false");

	/**
	 * Parameter Name to hold categoryId.
	 */
	public static final ParameterName CATEGORY_ID = ParameterName.getParameterName("categoryId");

	/**
	 * Constant to hold String |.
	 */
	public static final String PIPELINE = "|";
	public static final String AT = "at";

	/** Static property to hold question mark. */
	public static final String QUESTION_MARK = "?";

	/** Static property to hold equal. */
	public static final String EQUAL = "=";

	/** Static property to hold ampersand. */
	public static final String AMPERSAND = "&";

	/** Static property to hold empty string. */
	public static final String EMPTY_STRING = "";

	/** Static property to hold empty string. */
	public static final int ONE = 1;

	/**
	 * Parameter Name to hold errorMessageKey.
	 */
	public static final ParameterName ERROR_MESSAGE_KEY = ParameterName.getParameterName("errorMessageKey");

	/**
	 * Constant for src.
	 */
	public static final ParameterName SRC = ParameterName.getParameterName("src");

	/** Static property to hold hash code primary constant. */
	public static final int HASH_CODE_PRIME = 31;

	/** Static property to hold - DISPLAY_NAME. */
	public static final String DISPLAY_NAME = "displayName";

	/** Static property to hold - TITLE. */
	public static final String TITLE = "title";

	/** Static property to hold - META_DESCRIPTION. */
	public static final String META_DESCRIPTION = "metaDescription";

	/** Static property to hold - META_KEYWORDS. */
	public static final String META_KEYWORDS = "metaKeywords";

	/** Static property to meta info map size. */
	public static final int META_INFO_MAP_SIZE = 4;

	/**
	 * Parameter Name to hold pageKey.
	 */
	public static final ParameterName PAGE_KEY = ParameterName.getParameterName("pageKey");

	/**
	 * Parameter Name to hold siteId.
	 */
	public static final ParameterName SITE_ID = ParameterName.getParameterName("siteId");

	/** Constant to hold Collection product type. */
	public static final String PRODUCT_TYPE_COLLECTION = "Collection";

	/** Constant to hold Mobile product type. */
	public static final String PRODUCT_TYPE_MOBILE = "Mobile";

	/** Constant to hold ColourSize product type. */
	public static final String PRODUCT_TYPE_COLOR_SIZE = "ColourSize";

	/** Constant to hold NoColourNoSize product type. */
	public static final String PRODUCT_TYPE_NO_COLOR_NO_SIZE = "NoColourNoSize";

	/** Constant to hold ColourNoSize product type. */
	public static final String PRODUCT_TYPE_COLOR_NO_SIZE = "ColourNoSize";

	/** Constant to hold defaultColor. */
	public static final String DEFAULT_COLOR = "defaultColor";

	/** Constant to hold defaultColor. */
	public static final String DEFAULT_COLOR_ID = "defaultColorId";

	/** Constant to hold availableColors. */
	public static final String AVAILABLE_COLORS = "availableColors";

	/** Constant to hold collectionItems. */
	public static final String COLLECTION_ITEMS = "collectionItems";

	/**
	 * Constant to hold availableSizes.
	 */
	public static final String AVAILABLE_SIZES = "availableSizes";

	/** Constant to hold productType. */
	public static final String PRODUCT_TYPE = "productType";

	/** Constant to hold serviceProviders. */
	public static final String SERVICE_PROVIDERS = "serviceProviders";

	/** Constant to hold selectedColor. */
	public static final ParameterName SELECTED_COLOR = ParameterName.getParameterName("selectedColor");

	/** Constant to hold selectedColor. */
	public static final ParameterName MOBILE_PRODUCT = ParameterName.getParameterName("mobileProduct");

	/** Static property to hold String isSuccess. */
	public static final String IS_SUCCESS = "isSuccess";

	/** Static property to hold String application/json. */
	public static final String APPLICATION_JSON = "application/json";

	/** Static property to hold String accept. */
	public static final String ACCEPT = "accept";

	/** Static property to hold String text/json. */
	public static final String TEXT_JSON = "text/json";

	/** Static property to hold String text/. */
	public static final String TEXT_SLASH = "text/";

	/** Static property to hold String application/. */
	public static final String APPLICATION_SLASH = "application/";

	/** Static property to hold string not found value. */
	public static final int STRING_NOT_FOUND = -1;

	/**
	 * Constant to hold String "".
	 */
	public static final String PROPERTY_PATH_ARG = "";

	/** Constant to hold String skuAvailableInWishlist. */
	public static final String SKU_AVAILABLE_IN_WISHLIST = "skuAvailableInWishlist";

	/** Constant to hold the profile. */
	public static final String PROFILE = "profile";

	/**
	 * Parameter Name to hold catalogRefId.
	 */
	public static final ParameterName CATALOG_REF_ID = ParameterName.getParameterName("catalogRefId");

	/** Static property to hold - productAtributeValues. */
	public static final String PRODUCT_ATTRIBUTE_VALUES = "productAtributeValues";

	/** Static property to hold - productAtributeNameValueMap. */
	public static final String PRODUCT_ATTRIBUTE_NAME_VALUE_MAP = "productAtributeNameValueMap";
	
	/** Static property to hold - attriMap. */
	public static final String ATTRIBUTE_MAP = "attriMap";

	/** Constant to hold the profile. */
	public static final int IMAGE_MAP_SIZE = 2;

	/** Constant to hold the large. */
	public static final String LARGE_IMAGE = "large";

	/** Constant to hold the thumb. */
	public static final String THUMB_IMAGE = "thumb";

	/** Constant to hold the videoURL. */
	public static final String VIDEO_URL = "videoURL";

	/** Constant to hold selectedSKU. */
	public static final String SELECT_SKU = "selectedSKU";

	/** Constant to hold skuImages. */
	public static final String SKU_IMAGES = "skuImages";

	/** Constant to hold selectedSize. */
	public static final ParameterName SELECTED_SIZE = ParameterName.getParameterName("selectedSize");

	/** Constant to hold colorAvailability. */
	public static final String COLOR_AVAILABILITY = "colorAvailability";

	/** Static property to hold - productList. */
	public static final String PRODUCT_LIST_NAME = "productList";

	/** Static property to hold - |. */
	public static final String PIPELINE_SEPERATOR = "\\|";

	/** Static property to hold - !. */
	public static final String NOT_SEPERATOR = "\\!";

	/** Static property to hold - !. */
	public static final String NOT_SYMBOL = "!";

	/** Static property to hold - requestType. */
	public static final String REQUEST_TYPE = "requestType";

	/** Static property to hold - addProduct. */
	public static final String ADD_REQUEST_TYPE = "addProduct";

	/** Static property to hold - remove Product. */
	public static final String REMOVE_REQUEST_TYPE = "removeProduct";

	/** Static property to hold - commerceItemCount. */
	public static final String COMMERCE_ITEM_COUNT = "commerceItemCount";

	/**
	 * Static property to hold UTF-8 encoding.
	 */
	public static final String UTF_ENCODE = "UTF-8";

	/**
	 * Static property to hold data.
	 */
	public static final String DATA = "data";

	/**
	 * Static property to hold enocdeData.
	 */
	public static final String ENCODE_DATA = "enocdeData";
	
	/**
	 * Static property to hold enocdeData.
	 */
	public static final String ESCAPE_DATA = "escapeData";


	/**
	 * Static property to hold decodeData.
	 */
	public static final String DECODE_DATA = "decodeData";

	/**
	 * Static property to hold valueMap.
	 */
	public static final String VALUE_MAP = "valueMap";

	/**
	 * Static property to hold setOfAttributeName.
	 */
	public static final String SET_OF_ATTRIBUTENAME = "setOfAttributeName";
	
	/**
	 * Static property to hold compareAttributeList.
	 */
	public static final String COMPARE_ATTRIBUTE_LIST = "compareAttributeList";

	/**
	 * Static property to hold attributeMapForeachProduct.
	 */
	public static final String ATTRIBUTE_MAP_FOREACH_PRODUCT = "attributeMapForeachProduct";


	/**
	 * Static property to hold attributeGroupItems.
	 */
	public static final String ATTRIBUTE_GROUP_ITEMS = "attributeGroupItems";

	/**
	 * Static property to hold attributeGroupItems.
	 */
	public static final String IS_REQUIRES_RICA = "isRequiresRicaAddress";

	/**
	 * Static property to hold attributeGroupItems.
	 */
	public static final String PARAM_ORDER = "order";
	
	/**
	 * Static property to hold inputEmail.
	 */
	public static final String INPUT_EMAIL = "inputEmail";
	/**
	 * Static property to hold activeProductList.
	 */
	public static final String ACTIVE_PRODUCT_LIST = "activeProductList";
	/**
	 * Static property to hold true.
	 */
	public static final String STRING_TRUE = "true";
	/**
	 * Static property to hold comma.
	 */
	public static final String COMMA = ",";

	/** Constant to hold zero. */
	public static final int INT_ZERO = 0;

	/** Constant to hold Brand. */
	public static final String BRAND = "Brand";
	
	/** The Constant STRING_R. */
	public static final String STRING_R = "R";
	
	/** Constant to hold PDP_BRAND. */
	public static final String PDP_BRAND = "product.brand";

	/** Constant to hold _ symbol. */
	public static final String UNDER_SCORE = "_";

	/** Constant to hold brand URL. */
	public static final String BRAND_URL = "brandURL";

	/**
	 * Constant for SUCCESS_MESSAGE.
	 */
	public static final String SUCCESS_MESSAGE = "successMessage";

	/** Constant to hold productInfo. */
	public static final String PRODUCT_INFO = "productInfo";

	/** Constant to hold collectionProductInfo. */

	public static final String COLLECTION_PRODUCT_INFO = "collectionProductInfo";


	/** Constant to hold compareProductInfo. */

	public static final String COMPARE_PRODUCT_INFO = "compareProductInfo";

	/**
	 * Constant to hold "PARENT_SKU".
	 */
	public static final String PARENT_SKU = "PARENT_SKU";

	/**
	 * Constant to hold "ITEM_DESCRIPTION".
	 */
	public static final String ITEM_DESCRIPTION = "ITEM_DESCRIPTION";

	/**
	 * Constant to hold "MERCH_BRAND_NAME".
	 */
	public static final String MERCH_BRAND_NAME = "MERCH_BRAND_NAME";

	/**
	 * Constant to hold "PRODUCT_INFO_VO".
	 */
	public static final String PRODUCT_INFO_VO = "PRODUCT_INFO_VO";

	/**
	 * The constant for "PATH_SEPERATOR" symbol.
	 */
	public static final String PATH_SEPERATOR = "/";

	/**
	 * Constant for Endeca Ntt parameter.
	 */
	public static final String NTT_PARAM = "Ntt";

	/**
	 * Constant for Endeca HYFUN_AMPERSAND_HYFUN parameter.
	 */
	public static final String HYFUN_AMPERSAND_HYFUN = "-and-";

	/** Constant to hold productInfo. */
	public static final ParameterName PRODUCT_INFO_PARAM = ParameterName.getParameterName("productInfo");

	/** Static property to hold String wishlistCount. */
	public static final String WISHLIST_COUNT = "wishlistCount";

	/** Static property to hold String COLON_SYMBOL. */
	public static final String COLON_SYMBOL = ":";

	/** Static property to hold String HTTP_SCHEME. */
	public static final String HTTP_SCHEME = "http";

	/** Static property to hold String HTTPS_SCHEME. */
	public static final String HTTPS_SCHEME = "https";

	/** Static property to hold String FORWARD_SLASH. */
	public static final String FORWARD_SLASH = "//";

	/** The Constant NUM_TEN. */
	public static final String NUM_TEN = "10";

	/** The Constant SEMICOLON. */
	public static final String SEMICOLON = ";";

	/** The Constant NUM_0. */
	public static final int NUM_ZERO = 0;

	/** Static property to hold Constant ITEMS. */
	public static final String ITEMS = "items";

	/** The Constant ERROR_UPDATING_ORDER. */
	public static final String ERROR_UPDATING_ORDER = "errorUpdatingOrder";

	/** The Constant YES. */
	public static final String YES = "Y";
	
	/** The Constant YES_STRING. */
	public static final String YES_STRING = "YES";

	/** The Constant SHIP_TO_HOME. */
	public static final String SHIP_TO_HOME = "ShipToHome";

	/** The Constant IN_STORE. */
	public static final String IN_STORE = "InStore";
	
	/** The Constant TRUE_FLAG. */
	public static final String TRUE_FLAG = "true";

	/** The Constant TEMPLATE_PARAMETER_REGARDS. */
	public static final String TEMPLATE_PARAMETER_REGARDS = "regards";

	/** constant for HIDDEN. */
	public static final String HIDDEN = "HIDDEN";
	
	/** constant for NO_DISPLAY. */
	public static final String NO_DISPLAY = "NO DISPLAY";
	

	/** The Constant SUBMIT. */
	public static final String SUBMIT = "SUBMIT";

	/** The Constant lightBoxCookieExpirationDate. */
	public static final String LIGHT_BOX_COOKIE_EXPIRATION_DATE = "lightBoxCookieExpirationDate";

	/** The Constant dateformat. */
	public static final String DATE_FORMAT1 = "dd/MM/yyyy";

	/** The Constant donotshow. */
	public static final String DO_NOT_SHOW = "donotshow";

	/** The Constant cancel. */
	public static final String CANCEL = "cancel";

	/** The Constant isSubscriptionPopUpRequired. */
	public static final String IS_SUBSCRIPTION_POP_UP_REQUIRED = "isSubscriptionPopUpRequired";

	/** The Constant SHIP_TO_HOME. */
	public static final String STRING_ZERO = "0";

	/** The Constant IN_STORE. */
	public static final String STRING_ONE = "1";
	
	/** The Constant STRING_TWO. */
	public static final String STRING_TWO = "2";

	/** The Constant SPACE. */
	public static final String SPACE = " ";

	/** The Constant AND. */
	public static final String AND = "and";

	/** The Constant IN_STOCK. */
	public static final String IN_STOCK = "inStock";

	/** The Constant OUT_OF_STOCK. */
	public static final String OUT_OF_STOCK = "outOfStock";

	/** The Constant OUT_OF_STOCK. */
	public static final String CAPS_OUT_OF_STOCK = "OUTOFSTOCK";

	/** The Constant PRE_ORDER. */
	public static final String PRE_ORDER = "preOrder";

	/** The Constant INT_IN_STOCK. */
	public static final int INT_IN_STOCK = 1000;

	/** The Constant INT_OUT_OF_STOCK. */
	public static final int INT_OUT_OF_STOCK = 1001;

	/** The Constant INT_PRE_ORDER. */
	public static final int INT_PRE_ORDER = 1002;

	/** The Constant INT_LIMITED_STOCK. */
	public static final int INT_LIMITED_STOCK = 1006;

	/** The Constant ITEM_COUNT. */
	public static final String ITEM_COUNT = "itemCount";

	/** The Constant RAW_SUBTOTAL. */
	public static final String RAW_SUBTOTAL = "rawSubTotal";

	/** The Constant SUBTOTAL. */
	public static final String SUBTOTAL = "subTotal";

	/** The Constant ESTIMATED_SHIPPING. */
	public static final String ESTIMATED_SHIPPING = "estimatedShipping";

	/** The Constant SHIPPING_SURCHARGE. */
	public static final String SHIPPING_SURCHARGE = "shippingSurcharge";

	/** The Constant ORDER_TOTAL. */
	public static final String ORDER_TOTAL = "orderTotal";

	/** The Constant SAVED_AMOUNT. */
	public static final String SAVED_AMOUNT = "savedAmount";

	/** The Constant SAVED_PERCENTAGE. */
	public static final String SAVED_PERCENTAGE = "savedPercentage";

	/** The Constant FROM_OVERLAY. */
	public static final String FROM_OVERLAY = "fromOverlay";

	/** The Constant HANDLE_UPDATE_CARD_DATA_IN_ORDER. */
	public static final String HANDLE_UPDATE_CARD_DATA_IN_ORDER = "TRUBillingInfoFormHandler.handleUpdateCardDataInOrder";
	
	/** The Constant HANDLE_UPDATE_SYNCHRONY_DATA_INORDER. */
	public static final String HANDLE_UPDATE_SYNCHRONY_DATA_INORDER = "TRUBillingInfoFormHandler.handleUpdateSynchronyDataInOrder";

	/** The Constant ONLY_COLOR_VARIANTS. */
	public static final String ONLY_COLOR_VARIANTS = "onlyColorVariants";

	/** The Constant ONLY_SIZE_VARIANTS. */
	public static final String ONLY_SIZE_VARIANTS = "onlySizeVariants";

	/** The Constant BOTH_COLOR_SIZE_AVAILABLE. */
	public static final String BOTH_COLOR_SIZE_AVAILABLE = "bothColorSizeAvailable";

	/** The Constant NO_COLOR_SIZE_AVAILABLE. */
	public static final String NO_COLOR_SIZE_AVAILABLE = "noColorSizeAvailable";

	/** Static property to hold errorUpdatingOrder. */
	public static final String ERROR_UPDATING_ORDER_MSG = "errorUpdatingOrder";

	/** Static property to hold noOrderToModify. */
	public static final String NO_ORDER_TO_MODIFY_MSG = "noOrderToModify";

	/** Static property to hold errorMovingToPurchaseInfo. */
	public static final String ERROR_MOVE_TO_PURCHASEINFO_MSG_ID = "errorMovingToPurchaseInfo";


	/** The Constant COLOR. */
	public static final String COLOR = "color";
	
	/** The Constant COLOR. */
	public static final String CAPITOL_COLOR = "Color";

	/** The Constant SIZE. */
	public static final String SIZE = "size";

	/** The Constant LONG_ZERO. */
	public static final Long LONG_ZERO = 0L;

	/** The Constant LONG_MINUS_ONE. */
	public static final Long LONG_MINUS_ONE = -1L;

	/** The Constant DOUBLE_ZERO. */
	public static final double DOUBLE_ZERO = 0.0d;

	/** The Constant REGISTRY_USER_NAME. */
	public static final String REGISTRY_USER_NAME = "registryUserName";

	/** The Constant WISHLIST_USER_NAME. */
	public static final String WISHLIST_USER_NAME = "wishlistUserName";

	/** The Constant REGISTRY_URL. */
	public static final String REGISTRY_URL = "registryUrl";

	/** The Constant WISHLIST_URL. */
	public static final String WISHLIST_URL = "wishlistUrl";
	
	/** The Constant REGISTRY_TYPE. */
	public static final String REGISTRY_TYPE = "&registryType=B";
	
	/** The Constant WISHLIST_TYPE. */
	public static final String WISHLIST_TYPE = "&registryType=G";
	
	/** The Constant SOS_COOKIE_NAME. */
	public static final String SOS_COOKIE_NAME = "isSOS";
	
	/** The Constant EARLY_CHILD. */
	public static final String EARLY_CHILD = "tru_productdetail_label_earlychildhood";

	/** The Constant EVERYONE. */
	public static final String EVERYONE = "tru_productdetail_label_everyonesixyearsup";

	/** The Constant EVERYONETENPLUS. */
	public static final String EVERYONETENPLUS = "tru_productdetail_label_everyonetenup";

	/** The Constant MATURE. */
	public static final String MATURE = "tru_productdetail_label_mature";

	/** The Constant RATING_PENDING. */
	public static final String RATING_PENDING = "tru_productdetail_label_ratingpending";

	/** The Constant TEEN. */
	public static final String TEEN = "tru_productdetail_label_teen";
	
	/** The Constant ADULTS_ONLY. */
	public static final String ADULTS_ONLY="tru_productdetail_label_adultsonly";

	/** The Constant NINE_HUNDRED_NINGHTY_NINE. */
	public static final long NINE_HUNDRED_NINGHTY_NINE = 999;
	
	/** The Constant TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY = "tru_shoppingcart_item_limit_enforcement_inline_info_message";

	/** The Constant SHOPPINGCART_QTY_ADJUSTMENT_INFO_MESSAGE_KEY. */
	public static final String SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY = "tru_shoppingcart_qty_adjustment_info_message";

	/** The Constant SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY. */
	public static final String SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY = "tru_shoppingcart_qty_limit_reached_info_message";
	
	/** The Constant SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY. */
	public static final String SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY = "tru_shoppingcart_qty_limit_reached_inline_info_message";

	/** The Constant TRU_SHOPPINGCART_NO_INVENTORY_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_NO_INVENTORY_MESSAGE_KEY = "tru_shoppingcart_no_inventory_message";

	/** The Constant TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY = "tru_shoppingcart_item_limit_enforcement_info_message";

	/** The Constant TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY = 
			"tru_shoppingcart_item_inventory_enforcement_info_message";

	/** The Constant TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY = 
			"tru_shoppingcart_item_inventory_reached_info_message";

	/** The Constant TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY = 
			"tru_shoppingcart_no_inventory_at_selected_store_info_message";

	/** The Constant TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY. */
	public static final String TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY =
			"tru_shoppingcart_item_inventory_at_selected_store_enforcement_info_message";

	/** The Constant TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY = 
			"tru_shoppingcart_no_online_inventory_info_message";
	
	/** The Constant TRU_SHOPPINGCART_ADD_MULIPLE_ITEMS_GENERIC_INFO_MESSAGE. */
	public static final String TRU_SHOPPINGCART_ADD_MULIPLE_ITEMS_GENERIC_INFO_MESSAGE = 
			"tru_shoppingcart_add_muliple_items_generic_info_message";
	
	/** The Constant TRU_SHOPPINGCART_UNAVAILABLE_IN_STORE_PICKUP_INFO_MESSAGE. */
	public static final String TRU_SHOPPINGCART_UNAVAILABLE_IN_STORE_PICKUP_INFO_MESSAGE_KEY=
			"tru_shoppingcart_unavailable_in_store_pickup_info_message"; 
	
	/** The Constant ASSEMBLED_DEPTH. */
	public static final String ASSEMBLED_DEPTH = "assembledDepth";

	/** The Constant ASSEMBLED_HEIGHT. */
	public static final String ASSEMBLED_HEIGHT = "assembledHeight";

	/** The Constant ASSEMBLED_WEIGHT. */
	public static final String ASSEMBLED_WEIGHT = "assembledWeight";

	/** The Constant ASSEMBLED_WIDTH. */
	public static final String ASSEMBLED_WIDTH = "assembledWidth";

	/** The Constant ASSEMBLY_REQUIRED. */
	public static final String ASSEMBLY_REQUIRED = "assemblyRequired";

	/** The Constant ITEM_DEPTH. */
	public static final String ITEM_DEPTH = "itemDepth";

	/** The Constant ITEM_HEIGHT. */
	public static final String ITEM_HEIGHT = "itemHeight";

	/** The Constant ITEM_WIDTH. */
	public static final String ITEM_WIDTH = "itemWidth";

	/** The Constant ITEM_WEIGHT. */
	public static final String ITEM_WEIGHT = "itemWeight";
	/**
	 * Constant for CARD_EXPIRED.
	 */
	public static final int CARD_EXPIRED = 1;
	/**
	 * Constant for CARD_NUMBER_HAS_INVALID_CHARS.
	 */
	public static final int CARD_NUMBER_HAS_INVALID_CHARS = 2;
	/**
	 * Constant for CARD_NUMBER_DOESNT_MATCH_TYPE.
	 */
	public static final int CARD_NUMBER_DOESNT_MATCH_TYPE = 3;
	/**
	 * Constant for CARD_LENGTH_NOT_VALID.
	 */
	public static final int CARD_LENGTH_NOT_VALID = 4;
	/**
	 * Constant for CARD_NUMBER_NOT_VALID.
	 */
	public static final int CARD_NUMBER_NOT_VALID = 5;
	/**
	 * Constant for CARD_INFO_NOT_VALID.
	 */
	public static final int CARD_INFO_NOT_VALID = 6;
	/**
	 * Constant for CARD_EXP_DATE_NOT_VALID.
	 */
	public static final int CARD_EXP_DATE_NOT_VALID = 7;
	/**
	 * Constant for CARD_TYPE_NOT_VALID.
	 */
	public static final int CARD_TYPE_NOT_VALID = 8;
	/**
	 * Constant to hold String creditCard.
	 */
	public static final String CREDIT_CARD = "creditCard";
	/**
	 * Constant for value.
	 */
	public static final String VALUE = "value";

	/**
	 * Constant for DATE_FORMAT.
	 */
	public static final String DATE_FORMAT = "MM/dd/yyyy";

	/**
	 * Constant for siteId.
	 */
	public static final String SITE = "siteId";
	/**
	 * Constant for defaultCatalog.
	 */
	public static final String DEFAULT_CATALOG = "defaultCatalog";
	/**
	 * Constant for id.
	 */
	public static final String ID = "id";
	/**
	 * Constant for siteName.
	 */
	public static final String SITE_NAME = "siteName";
	/**
	 * Constant for STORE_ID.
	 */
	public static final String STORE_ID = "storeId";
	/**
	 * Constant for STORE_ID.
	 */
	public static final String STORE_NAME = "storeName";
	/**
	 * Constant for COOKIE_STORE_ID.
	 */
	public static final String COOKIE_STORE_ID = "cookieStoreId";

	/** Constant for MAKE_MYSTORE_COOKIE. */
	public static final String MAKE_MYSTORE_COOKIE = "myStoreCookie";
	
	
	/** Constant for MAKE_MYSTORE_COOKIE. */
	public static final String MAKE_MYSTORENAME_COOKIE = "myStoreName";

	/** The Constant CART_TOP_MSG. */
	public static final String CART_TOP_MSG = "top_msg";

	/** The Constant CART_INLINE_MSG. */
	public static final String CART_INLINE_MSG = "inline_msg";

	/** The Constant SHOW_PAYPAL_CHECKOUT_FLAG. */
	public static final String SHOW_PAYPAL_CHECKOUT_FLAG = "showPaypalCheckOutFlag";

	/** The Constant RELATIONSHIP_ID. */
	public static final String RELATIONSHIP_ID= "relationShipId";

	/** The Constant RELATIONSHIP_ID. */
	public static final String METAINFO_ID= "metaInfoId";

	/** The Constant ITEM. */
	public static final String ITEM = "item";

	/** The Constant INVENTORY_ONLINE. */
	public static final String INVENTORY_ONLINE = "online";

	/** The Constant CHANNEL_ITEM. */
	public static final String CHANNEL_ITEM = "channelItem";

	/** Hold order string constant. */
	public static final String ORDER = "order";

	/** Hold userSession string constant. */
	public static final String USER_SESSION = "userSession";

	/**  Holds string constant isOrderReconcileRequired. */
	public static final String IS_ORDER_RECONCILE_REQUIRED = "isOrderReconcileRequired";

	/**  Holds string constant Review. */
	public static final String REVIEW_PAGE_NAME = "Review";

	/** The Constant SHOPPING_CART_COMPONENT_NAME. */
	public static final String SHOPPING_CART_COMPONENT_NAME="/atg/commerce/ShoppingCart";
	/** The Constant CATALOG_PROPERTIES_COMPONENT_NAME. */
	public static final String CATALOG_PROPERTIES_COMPONENT_NAME="/atg/commerce/catalog/custom/CatalogProperties";

	/** The Constant ITEMS_REMOVED_INFO_MESSAGE. */
	public static final String ITEMS_REMOVED_INFO_MESSAGE = "removedInfoMessage";

	/** The Constant DELETED_SKU_LIST. */
	public static final String DELETED_SKU_LIST = "deletedSkuList";

	/** The Constant REMOVAL_ITEM_LIST. */
	public static final String REMOVAL_ITEM_LIST = "removalItemList";

	/** The Constant TRU_SHOPPINGCART_REMOVED_IETM_FROM_BCC. */
	public static final String TRU_SHOPPINGCART_REMOVED_IETM_FROM_BCC = "tru_shoppingcart_removed_item_from_bcc";

	/** The Constant TRU_SHOPPINGCART_NO_INVENTORY_INFO_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_NO_INVENTORY_INFO_MESSAGE_KEY = "tru_shoppingcart_no_inventory_info_message";

	/**  Holds string constant giftOptionsDetails. */
	public static final String GIFT_OPTIONS_DETAILS = "giftOptionsDetails";

	/** Holds string  Constant SHOPPING_CART_PAGE_NAME. */
	public static final String SHOPPING_CART_PAGE_NAME="cart/shoppingCart.jsp";

	/** Holds string  Constant productId. */
	public static final String PRODUCT_ID_1 = "productId";

	/** Holds string  Constant COLLECTION_PRODUCT. */
	public static final String COLLECTION_PRODUCT = "collectionProduct";

	/** Holds string  Constant TYPE. */
	public static final String TYPE = "type";

	/** property to hold productPromotions. */

	public static final String PRODUCT_PROMOTION = "productPromotions";

	/** property to hold promotionDetails. */

	public static final String PROMOTION_DETAILS = "promotionDetails";
	
	/** property to hold promotionDisplay. */

	public static final String PROMOTION_DISPLAY = "promotionDisplay";
	
	
	public static final String PROMOTION_INFO = "promotionInfo";

	/** property to hold description. */

	public static final String PROMO_DESCRIPTION = "description";

	/** property to hold THINGS_TO_KNOW_ONLINE_KEY. */

	public static final String THINGS_TO_KNOW_ONLINE_KEY = "tru.productdetail.label.thingstoknowonline";

	/** property to hold THINGS_TO_KNOW_STORE_KEY. */
	public static final String THINGS_TO_KNOW_STORE_KEY = "tru.productdetail.label.thingstoknowstore";

	/** property to hold THINGS_TO_KNOW_BATTERIES_KEY. */
	public static final String THINGS_TO_KNOW_BATTERIES_KEY = "tru.productdetail.label.thingstoknowbatteries";

	/** property to hold THINGS_TO_KNOW_GIFTWRAP_AVAILABLE_KEY. */
	public static final String THINGS_TO_KNOW_GIFTWRAP_AVAILABLE_KEY = "tru.productdetail.label.thingstoknowygiftwrap";

	/** property to hold THINGS_TO_KNOW_GIFTWRAP_NOT_AVAILABLE_KEY. */
	public static final String THINGS_TO_KNOW_GIFTWRAP_NOT_AVAILABLE_KEY = "tru.productdetail.label.thingstoknowngiftwrap";

	/** property to hold THINGS_TO_KNOW_SHIPIN_KEY. */
	public static final String THINGS_TO_KNOW_SHIPIN_KEY = "tru.productdetail.label.thingstoknowshipin";

	/** property to hold ONLINE_ONLY. */
	public static final String ONLINE_ONLY = "Online Only";

	/** property to hold IN_STORE_ONLY. */
	public static final String IN_STORE_ONLY = "In Store Only";

	/** property to hold NOT_APPLICABLE. */
	public static final String NOT_APPLICABLE = "N/A";

	/** property to hold BATTERY_REQUIRED_NO. */
	public static final String BATTERY_REQUIRED_NO = "N";

	/** property to hold GIFTWRAP_NO. */
	public static final String GIFTWRAP_NO = "N";

	/** property to hold GIFTWRAP_YES. */
	public static final String GIFTWRAP_YES = "Y";

	/** Constant to hold collectionLineItemInfo. */

	public static final String COLLECTION_LINE_ITEM_INFO = "collectionLineItemInfo";

	/** property to hold the rest context path. */
	public static final String REST_CONTEXT_PATH = "/rest";

	/** property to hold the tru context path. */

	public static final String TRU_CONTEXT_PATH = "/tru";

	/** property to hold the bru context path. */

	public static final String BRU_CONTEXT_PATH = "/bru";

	/** property to hold the NO. */
	public static final String NO = "N";

	/** property to hold the NO_STRING. */
	public static final String NO_STRING = "NO";

	/**  Holds ABANDON_CART_THRESHOLD. */
	public static final String ABANDON_CART_THRESHOLD = "abandonCartThreshold";

	/**  Holds WELCOME_BACK_CHECKBOX_COOKIE. */
	public static final String WELCOME_BACK_CHECKBOX_COOKIE = "welcomeBackCheckboxCookie";

	/**  Holds WELCOME_BACK_CHECKBOX_VALUE. */
	public static final String WELCOME_BACK_CHECKBOX_VALUE = "welcomeBackCheckboxValue";

	/**  Holds SHOW_WELCOME_BACK. */
	public static final String SHOW_WELCOME_BACK = "showWelcomeBack";
	
	/**  Holds SITE_REMINDER_COUNT_COOKIE . */
	public static final String SITE_REMINDER_COUNT_COOKIE = "siteReminderCount";

	/**
	 * property to hold PAY_IN_STORE.
	 */
	public static final String PAY_IN_STORE = "payInStore";
	/**
	 * property to hold PAY_IN_STORE_PAYMENT_GROUP.
	 */
	public static final String PAY_IN_STORE_PAYMENT_GROUP = "payInStorePaymentGroup";
	/**
	 * property to hold PAY_IN_STORE_PAYMENT_GROUP.
	 */
	public static final String STORE_PICKUP_SHIP_GROUP = "storePickUpShipGroup";

	/** The Constant DONATION_SKU_ITEM. */
	public static final String DONATION_SKU_ITEM = "donationSKUItem";

	/** The Constant DONATION. */
	public static final String DONATION = "DONATION";

	/** The Constant SUB_TYPE. */
	public static final String SUB_TYPE = "subType";

	/** The Constant NON_MERCH_SKU. */
	public static final String NON_MERCH_SKU = "nonMerchSKU";

	/** The Constant BPP. */
	public static final String BPP = "bpp";

	/** The Constant BPP_SKU_TYPE. */
	public static final String BPP_SKU_TYPE = "BPP";

	/** The Constant BPP_NAME. */
	public static final String BPP_NAME = "bppName";

	/** The Constant BPP_LOW_PROPERTY. */
	public static final String BPP_LOW_PROPERTY = "low";

	/** The Constant BPP_HIGH_PROPERTY. */
	public static final String BPP_HIGH_PROPERTY = "high";

	/** The Constant DONATION_PRODUCT_ITEM. */
	public static final String DONATION_PRODUCT_ITEM = "donationProductItem";

	/** The Constant PARENT_PRODUCTS. */
	public static final String PARENT_PRODUCTS = "parentProducts";

	/** The Constant DONATION_AMOUNT. */
	public static final String DONATION_AMOUNT = "donationAmount";

	/**
	 * property to hold giftOptionsInfoCount.
	 */
	public static final String GIFT_OPTIONS_INFO_COUNT = "giftOptionsInfoCount";

	/** The Constant to hold SHOP_LOCAL. */
	public static final String SHOP_LOCAL = "shopLocal";

	/** The Constant to hold SHOP_LOCAL_LIST. */
	public static final String SHOP_LOCAL_LIST = "shopLocalList";

	/**  The Constant to hold prod_. */
	public static final String PROD = "prod_";

	/**  The Constant to hold qty_. */
	public static final String QTY = "qty_";
	/**
	 * property to hold giftOptionsInfoCount.
	 */
	public static final String FINAL_REMAINING_AMOUNT = "finalRemaniningAmount";

	/** The Constant Shipping. */
	public static final String SHIPPING = "Shipping";
	/** The Constant Shipping. */
	public static final String STORE_PICKUP = "store-pickup";
	
	/**  The Constant to GIFT_FINDER. */
	public static final String GIFT_FINDER = "giftFinder";
	
	/**  The Constant to GIFT_FINDER. */
	public static final String GT_FINDER = "GiftFinder";
	
	/**  The Constant to GIFT_FINDER. */
	public static final String FINDER_KEY = "fiderKey";

	/** The Constant HANDLE_REMOVE_BPP_ITEM_METHOD. */
	public static final String HANDLE_REMOVE_BPP_ITEM_METHOD = "CartModifierOrderFormHandler.handleRemoveBPPItemRelationship";

	/** The Constant TRU_SHOPPINGCART_AUTOCORRECT_WITH_INVENTORY. */
	public static final String TRU_SHOPPINGCART_AUTOCORRECT_WITH_INVENTORY = "inventory autocorrect";

	/** The Constant TRU_SHOPPINGCART_AUTOCORRECT_WITH_PURCHASE_LIMIT. */
	public static final String TRU_SHOPPINGCART_AUTOCORRECT_WITH_PURCHASE_LIMIT = "purchase limit autocorrect";

	/** The Constant ITEMS_QTY_ADJUSTED_INFO_MESSAGE. */
	public static final String ITEMS_QTY_ADJUSTED_INFO_MESSAGE = "qtyAdjustedInfoMessage";

	/** The Constant QTY_ADJUSTED_LIST. */
	public static final String QTY_ADJUSTED_LIST = "qtyAdjustedSkuList";

	/** The Constant TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_PURCHASE_LIMIT. */
	public static final String TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_PURCHASE_LIMIT=
			"tru_shoppingcart_qty_adjusted_in_merge_with_purchase_limit";
	/** The Constant TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_INVENTORY_LIMIT. */
	public static final String TRU_SHOPPINGCART_QTY_ADJUSTED_IN_MERGE_WITH_INVENTORY_LIMIT =
			"tru_shoppingcart_qty_adjusted_in_merge_with_inventory_limit";

	/** property to hold the GT. */
	public static final String GT = "<gt/>";

	/** property to hold the LT. */
	public static final String LT = "<lt/>";
	
	/** property to hold the REG_SYMBOL. */
	public static final String REG_SYMBOL = "(R)";
	
	/** property to hold the REG_ENTITY. */
	public static final String REG_ENTITY = "&reg;";

	/** property to hold the GT_SYMBOL. */
	public static final String GT_SYMBOL = ">";

	/** property to hold the LT_SYMBOL. */
	public static final String LT_SYMBOL = "<";

	/** property to hold the VARIANT_NAME. */
	public static final String VARIANT_NAME = "variantName";

	/** property to hold the INT_FIVE_THOUSAND. */
	public static final int INT_FIVE_THOUSAND = 5000;

	/** property to hold the INT_THREE_HUNDRED. */
	public static final int INT_THREE_HUNDRED = 300;

	/** property to hold the INT_TWELVE. */
	public static final int INT_TWELVE = 12;

	/** property to hold the INT_TWENTY_FOUR. */
	public static final int INT_TWENTY_FOUR = 24;

	/** property to hold the CHAR_HYPHEN. */
	public static final char CHAR_HYPHEN = '-';

	/** property to hold the BIRTH. */
	public static final String BIRTH = "Birth - ";

	/** property to hold the VARIANT_NAME. */
	public static final String BIRTH_AND_UP = "Birth and up";

	/** property to hold the MONTHS. */
	public static final String MONTHS = " months";

	/** property to hold the YEARS. */
	public static final String YEARS = " years";

	/** property to hold the MONTHS_AND_UP. */
	public static final String MONTHS_AND_UP = " months and up";

	/** property to hold the YEARS_AND_UP. */
	public static final String YEARS_AND_UP = " years and up";

	/** property to hold the HYPHEN_SPACE. */
	public static final String HYPHEN_SPACE = " - ";

	/** The Constant SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY. */
	public static final String SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY = "tru_shoppingcart_qty_empty_info_message";

	/** The Constant INVALID_OUTOFSTOCK_ID. */
	public static final String INVALID_OUTOFSTOCK_ID = "tru_invalid_outofstock_id_info_message";
	/**  Static property to hold page param. */
	public static final String PARAM_PAGE = "page";

	/** property to hold the PARAM SHIPPINGVO. */
	public static final String PARAM_SHIPPINGVO = "shippingDestinationsVO";

	/** property to hold the PARAM_DONATION_ITEM_LIST. */
	public static final String PARAM_DONATION_ITEM_LIST = "donationItemsList";

	/** property to hold the PARAM_OUT_OF_STOCK_ITEMS_LIST. */
	public static final String PARAM_OUT_OF_STOCK_ITEMS_LIST = "outOfStockItemsList";

	/** property to hold the PARAM_redirection url. */
	public static final String PARAM_REDIRECTION_URL = "redirectionUrl";

	/** property to hold the isOrder has gift wrap. */
	public static final String IS_ORDER_HAS_GW_ITEM = "isOrderHasGiftWrappedItem";

	/**  property to hold gift options class name. */
	public static final String GIFT_OPTIONS_INFO_CLASS_NAME = "com.tru.commerce.order.vo.TRUGiftOptionsInfo";

	/**  property to hold store pick up class name. */
	public static final String STORE_PICKUP_INFO_CLASS_NAME = "com.tru.commerce.order.vo.TRUStorePickUpInfo";

	/** property to hold the cart string. */
	public static final String CART = "cart";

	/**  property to hold credit cards string. */
	public static final String CREDIT_CARDS = "creditCards";

	/**  property to hold inStorePickupShippingGroup string. */
	public static final String INSTORE_PICKUP_SG = "inStorePickupShippingGroup";

	/**  property to hold channelInStorePickupShippingGroup string. */
	public static final String CHANNEL_INSTORE_SG = "channelInStorePickupShippingGroup";

	/** property to hold shippingaddress.ownerid string */
	public static final String SHIPPING_ADR_OWNERID = "shippingAddress.ownerId";

	/**  property to hold wishlist string. */
	public static final String WISHLIST = "wishlist";

	/**  property to hold registry string. */
	public static final String REGISTRY = "registry";
	
	/**  property to hold registry string. */
	public static final String REGISTRY_STORE = "regstore";

	/**  property to hold giftWrapCommItemId string. */
	public static final String GIFTWRAP_COMM_ITEM_ID = "giftWrapCommItemId";

	/**  property to hold giftCardAppliedAmount string. */
	public static final String GIFT_CARD_APPLIED_AMOUNT = "giftCardAppliedAmount";

	/**  property to hold balance amount param string. */
	public static final String BALANCE_AMOUNT = "balanceAmount";

	/** The Constant Confirm. */
	public static final String CONFIRM = "Confirm";

	/**  property to hold shippingGroupMapContainer string. */
	public static final String SHIPPING_GROUP_MAP_CONTAINER = "shippingGroupMapContainer";

	/**  property to hold updateShippingMethodAndPrice string. */
	public static final String UPDATE_SHIP_METHOD_PRICE = "updateShippingMethodAndPrice";
	
	/**  property to hold shippingGroupMapContainer string. */
	public static final String SHIPPING_GROUP_MAP = "ShippingGroupMapContainer";
	
	/**  property to hold errorUpdateShippingGroup string. */
	public static final String ERROR_UPDATE_SG = "errorUpdateShippingGroup";


	/**  constant to hold char pattern. */
	public static final String CHAR_PATTERN ="[^a-z0-9 ]";
	
	/**  property to hold giftWrapCommerceItem string. */
	public static final String GIFT_WRAP_COMMERCE_ITEM = "giftWrapCommerceItem";

	/**  property to hold RETURN_SUCCESS. */
	public static final int RETURN_SUCCESS = 1;
	
	/**  property to hold INVALID_SHIPPINGGROUP_PARAMETER. */
	public static final String INVALID_SHIPPINGGROUP_PARAMETER = "InvalidShippingGroupParameter";
	
	/**  property to hold SYNCHRONY_SDRESPONSE_OBJECT. */
	public static final String SYNCHRONY_SDRESPONSE_OBJECT = "synchronySDPResponseObj";
	
	/**  property to hold INVALID_ORDER_PARAMETER. */
	public static final String INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	
	/**  property to hold ORDER_RESOURCES. */
	public static final String ORDER_RESOURCES = "atg.commerce.order.OrderResources";

	/**  property to hold ProcTRUValidateOnlyInStoreForCheckout string. */
	public static final String VALIDATE_INSTORE_CHECKOUT = "ProcTRUValidateOnlyInStoreForCheckout";

	/**  property to hold InvalidInStorePaymentUse string. */
	public static final String INVALID_INSTORE_PAYMENTUSE = "InvalidInStorePaymentUse";

	/**  property to hold errorUpdateShippingGroup string. */
	public static final String ERROR_UPDATING_SHIPPING_GROUP = "errorUpdateShippingGroup";

	/**  property to hold updateShippingMethodAndPrice string. */
	public static final String UPDATE_SHIPPING_PRICE = "updateShippingMethodAndPrice";

	/**  property to hold shipMethodName_ string. */
	public static final String SHIP_METHOD_NAME = "shipMethodName_";

	/**  property to hold shippingMethod_ string. */
	public static final String SHIP_METHOD = "shippingMethod_";
	
	/**  property to hold shippingPrice_ string. */
	public static final String SHIP_PRICE = "shippingPrice_";
	
	/**  property to hold regionCode_ string. */
	public static final String REGION_CODE = "regionCode_";

	/**  property to hold isPayInStoreSelected string. */
	public static final String IS_PAY_IN_STORE_SELECTED = "isPayInStoreSelected";

	/** The Constant TRU_CONCURRENT_ORDER_UPDATE. */
	public static final String TRU_CONCURRENT_ORDER_UPDATE = "tru_concurrent_order_update_error_message";

	/** The Constant SHOPPINGCART_CONCURRENT_UPDATE_ATTEMPT. */
	public static final String SHOPPINGCART_CONCURRENT_UPDATE_ATTEMPT = "ConcurrentUpdateAttempt";

	/**  property to hold isPayInStoreEligible string. */
	public static final String PAY_IN_STORE_ELIGIBLE = "isPayInStoreEligible";

	/**  property to hold TRU_SITE_CONTEXT_MATCH. */
	public static final String TRU_SITE_CONTEXT_MATCH = "(.*)tru(.*)";

	/**  property to hold BRU_SITE_CONTEXT_MATCH. */
	public static final String BRU_SITE_CONTEXT_MATCH = "(.*)bru(.*)";

	/**  property to hold ONLINE_PID. */
	public static final String ONLINE_PID = "onlinePID";

	/**  property to hold PAYPAL_DATE_FORMAT. */
	public static final String PAYPAL_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";


	/** The Constant PLEASE_TRY_AGAIN. */
	public static final String PLEASE_TRY_AGAIN = "Please Try again";

	/** The Constant USER_PRICING_MODELS_COMPONENT_NAME. */
	public static final String USER_PRICING_MODELS_COMPONENT_NAME ="/atg/commerce/pricing/UserPricingModels";

	/** The Constant LIMITED_STOCK. */
	public static final String LIMITED_STOCK = "LIMITEDSTOCK";

	/** The Constant LIMITED_STOCK. */
	public static final long DEFAULT_STOCK_LEVEL = 80;

	/** The Constant LIMITED_STOCK. */
	public static final int DEFAULT_STOCK_LEVEL_DUMMY = 100;

	/** The Constant LIMITED_STOCK. */
	public static final String DEFAULT_STATUS = "INSTOCK";

	/** The Constant LIMITED_STOCK. */
	public static final long DEFAULT_DATE = 20151001;
	/** The Constant GET_ITEM_ID. */
	public static final String GET_ITEM_ID = "getItemId";
	/** The Constant GET_STATUS. */
	public static final String GET_STATUS = "getStatus";
	/** The Constant IN_STOCK_CAP. */
	public static final Object IN_STOCK_CAP = "INSTOCK";
	/** The Constant GET_STOCK_LEVEL. */
	public static final String GET_STOCK_LEVEL = "getStockLevel";
	/** The Constant BOTH_ONLINE_STORE. */
	public static final String BOTH_ONLINE_STORE = "both";
	/** The Constant PERFORMANCE_MONITOR_FLUSH. */
	public static final String PERFORMANCE_MONITOR_FLUSH = "PERFORMANCE_MONITOR_FLUSH";
	/** The Constant LOCATION_ID. */
	public static final String LOCATION_ID = "locationId";
	
	/** The Constant DOLLAR_WITH_STAR. */
	public static final String DOLLAR_WITH_STAR = "*$";
	/** The Constant STAR. */
	public static final String STAR = "*";

	/** The Constant REVIEW_RATING. */
	public static final String REVIEW_RATING = "reviewRating";

	/** The Constant PRIMARY_IMAGE. */
	public static final String PRIMARY_IMAGE = "primaryImage";
	
	/** The Constant PRESELLABLE. */
	public static final String PRESELLABLE = "presellable";

	/** The Constant SUGGESTED_AGE_MIN. */
	public static final String SUGGESTED_AGE_MIN = "suggestedAgeMin";

	/** The Constant PDP_URL. */
	public static final String PDP_URL = "pdpUrl";

	/** The Constant FAV_STORE. */
	public static final String FAV_STORE="favStore";
	/** The Constant PRODUCT_IDS. */
	public static final String PRODUCT_IDS="productIds";
	/** The Constant COMA_SYMBOL. */
	public static final String COMA_SYMBOL=",";
	/** The Constant INVENTORY_AND_PRICE_DETAILS. */
	public static final String INVENTORY_AND_PRICE_DETAILS="invenventoryAndPriceDetails";
	/** The Constant BATTERY_INVENTORY_AND_PRICE_DETAILS. */
	public static final String BATTERY_INVENTORY_AND_PRICE_DETAILS="batteryInventoryAndPriceDetails";
	/** The Constant PROMOTION_DETAILS_PDP. */
	public static final String PROMOTION_DETAILS_PDP="PromotionDetails";
	/** The Constant COMBINE_STRING_MAP_JSON. */
	public static final String COMBINE_STRING_MAP_JSON="combineStringMapJson";
	/** The Constant LIST_PRICE. */
	public static final String LIST_PRICE="listPrice";
	/** The Constant SALE_PRICE. */
	public static final String SALE_PRICE="salePrice";
	/** The Constant SKU_ID_PDP. */
	public static final String SKU_ID_PDP="skuId";
	/** The Constant STORE_ID_PDP. */
	public static final String STORE_ID_PDP="storeId";
	/** The Constant PROMO_NAME. */
	public static final String PROMO_NAME="promoName";
	/** The Constant PROMO_NAME. */
	public static final String PROMO_ID="promoId";
	/** The Constant PROMO_DETAILS. */
	public static final String PROMO_DETAILS="promoDetails";
	/** The Constant INVENTORY_STATUS_ONLINE. */
	public static final String INVENTORY_STATUS_ONLINE="inventoryStatusOnline";
	/** The Constant INVETORY_LEVEL_ONLINE. */
	public static final String INVETORY_LEVEL_ONLINE="inventoryLevelOnline";
	/** The Constant INVENTORY_STATUS_STORE. */
	public static final String INVENTORY_STATUS_STORE="inventoryStatusStore";
	/** The Constant INVETORY_LEVEL_STORE. */
	public static final String INVETORY_LEVEL_STORE="inventoryLevelStore";
	/** The Constant SELECTED_COLOR_PDP. */
	public static final String SELECTED_COLOR_PDP="selectedColor";
	/** The Constant SELECTED_SIZE_PDP. */
	public static final String SELECTED_SIZE_PDP="selectedSize";
	/** The Constant SELECTED_SKUID_PDP. */
	public static final String SELECTED_SKUID_PDP="selectedSkuId";
	
	/** The Constant STORE_AVAILABLE_MESSAGES. */
	public static final String STORE_AVAILABLE_MESSAGES="storeAvailableMessages";
	
	/** The Constant INVENTORY_STORE_MESSAGE. */
	public static final String INVENTORY_STORE_MESSAGE="inventoryStoreMessage";
	
	public static final String INVENTORY_STORE_MESSAGE1="inventoryStoreMessage1";
	
	/** The Constant LIMITED_STOCK. */
	public static final long ZERO = 0;
	
	/**  property to hold NORMAL_PRODUCT. */
	public static final String NORMAL_PRODUCT = "product";
	/** The Constant ARTICLE_BODY. */
	public static final String ARTICLE_BODY = "articleBody";
	/** The Constant SEO_TITLE_TEXT. */
	public static final String SEO_TITLE_TEXT = "seoTitleText";
	/** The Constant SEO_META_DESC. */
	public static final String SEO_META_DESC = "seoMetaDesc";
	/** The Constant SEO_META_KEYWORD. */
	public static final String SEO_META_KEYWORD = "seoMetaKeyword";
	/** The Constant SEO_ALT_DESC. */
	public static final String SEO_ALT_DESC = "seoAltDesc";
	/** The Constant ARTICLE_CONTENT_KEY. */
	public static final String ARTICLE_CONTENT_KEY = "articleContentKey";
	
	
	/** The Constant CMSSPOT. */
	public static final String CMSSPOT = "CMSSPOT";
	/** The Constant CMSSPOT1. */
	public static final String CMSSPOT1 = "CMSSPOT1";
	/** The Constant CMSSPOT2. */
	public static final String CMSSPOT2 = "CMSSPOT2";
	/** The Constant CMSSPOT3. */
	public static final String CMSSPOT3 = "CMSSPOT3";
	/** The Constant CMSSPOT4. */
	public static final String CMSSPOT4 = "CMSSPOT4";
	
	/** The Constant No.of CMSSPOTS. */
	public static final String NO_CMS_SPOTS = "noOFCMSSPOTS";

	/** Constant to hold 16. */
	public static final int INT_SIXITHEEN = 16;
	/** Constant to hold 255. */
	public static final int INT_TWO_FIFTY_FIVE = 255;
			/** The Constant ONLINE. */
	public static final String ONLINE = "online";
	/** Constant to hold 256. */
	public static final int INT_TWO_FIFTY_SIX = 256;
	/** Static property to hold String SINGLE_FORWARD_SLASH. */
	public static final String SINGLE_FORWARD_SLASH = "/";
	/** The Constant WAREHOUSE_LOCATION_CODE. */
	public static final String WAREHOUSE_LOCATION_CODE = "warehouseLocationCode";
	
	/** The Constant EWASTE_STATE. */
	public static final String EWASTE_STATE = "ewasteState";
	
	  /** The Constant PROP_SHIP_TO_NAME. */
    public static final String PROP_SHIP_TO_NAME = "shipToName";
    
	/**  property to hold PRODUCTVO_JSON. */
	public static final String PRODUCTVO_JSON = "productVoJson";
	

	/** The Constant REST_REQUEST. */
	public static final String REST_REQUEST = "restRequest";
	
	/** The Constant SYNCHRONY_REQ_PARAM. */
	public static final String SYNCHRONY_REQ_PARAM = "synchronyReqParam";
	
	/** The Constant TRU_SESSION_SCOPE_COMPONENT_NAME. */
	public static final String TRU_SESSION_SCOPE_COMPONENT_NAME="/com/tru/common/TRUUserSession";
	
	/** The Constant WAREHOUSE_PICKUP. */
	public static final String WAREHOUSE_PICKUP_FLAG = "warehousePickupFlag";
	/** The Constant UNDEFINED. */
	public static final String UNDEFINED = "undefined";

	/** The Constant SITE_URL. */
	public static final String SITE_URL = "siteUrl";
	
	/** The Constant ADDITIOONAL_PRODUCTION_URLS. */
	public static final String ADDITIOONAL_PRODUCTION_URLS = "additionalProductionURLs";
	
	/** The Constant STORE. */
	public static final String STORE = "store";
	
	/** The Constant SOS. */
	public static final String SOS = "sos";
	
	/** The Constant SELECTED_PAYMENT_GRP_TYPE. */
	public static final String SELECTED_PAYMENT_GRP_TYPE = "selectedPaymentGroupType";
	
	/** The Constant Recent_Viewed_Items_Param. */
	public static final String RECENT_VIEWED_ITEMS_PARAM = "recentViewedItemsParam";
	
	/** The Constant NO_GIFT_WRAP. */
	public static final String NO_GIFT_WRAP = "No Gift Wrap";

	/**
	 * property to hold CLASSIFICATION property.
	 */ 
	public static final String CLASSIFICATION ="classification";
	/**
	 * property to hold fixedChildCategories property.
	 */ 
	public static final String FIXED_CHILDCATEGORIES ="fixedChildCategories";

	/**
	 * Constant for site.
	 */
	public static final String CURRENT_SITE = "site";
	
	/**
	 * property to hold REGISTRY_CLASSIFICATION property.
	 */ 
	public static final String REGISTRY_CLASSIFICATION ="registryClassification";
	
	/**
	 * Constant for 9999.
	 */
	public static final int INT_9999 = 9999;
	
	/** The Constant REPOSITORY_ITEM. */
	public static final String REPOSITORY_ITEM = "repositoryItem";
	
	/** The Constant BPP_ITEM_ID. */
	public static final String BPP_ITEM_ID = "bppItemId";
	
	/** The Constant BPP_PRICE. */
	public static final String BPP_PRICE = "bppPrice";

	/** The Constant BPP_SKU_ID. */
	public static final String BPP_SKU_ID = "bppSkuId";
	
	/** The Constant CONCURRENT_UPDATE_ATTEMPT. */
	public static final String CONCURRENT_UPDATE_ATTEMPT = "Concurrent Update Attempt";
	
	/** The Constant OBSERVABLE_UPDATE_FOR. */
	public static final String OBSERVABLE_UPDATE_FOR = "Observable update for  ";
	
	/** The Constant WAS_RECEIVED_WITH_ARG_TYPE. */
	public static final String WAS_RECEIVED_WITH_ARG_TYPE = " was received with arg type  ";
	
	/**
	 * property to hold EMPTY_SPACE.
	 */
	public static final String EMPTY_SPACE = "";
	
	/** The Constant OPEN.
	 */
	public static final String OPEN="\\[";
	
	/** The Constant CLOSE. */
	public static final String CLOSE="\\]";
	
	/**
	 * property to hold SPLITE_PRODUCTIDS.
	 */
	public static final String SPLIT_PIPE = "\\|";
	
	/**
	 * property to hold SPLITE_PRODUCTIDS.
	 */
	public static final String SPLIT_PLUS = "\\+";
	
	/**
	 * property to hold NOT_FOUND_IN_REPOSITORY.
	 */
	public static final String NOT_FOUND_IN_REPOSITORY = "Not Found in Repository";
	
	/** The Constant SKU_STRING. */
	public static final String SKU_STRING = "sku";
	
	/** The Constant PARENT_SKU_STRING. */
	public static final String PARENT_SKU_STRING = "parentSku";
	
	/** The Constant QUANTITY_STRING. */
	public static final String QUANTITY_STRING = "quantity";
	
	/** The Constant PRICE. */
	public static final String PRICE = "price";
	
	/** The Constant REGULAR_PRICE. */
	public static final String REGULAR_PRICE = "regularPrice";
	
	/** The Constant ORDER_SUB_TOTAL. */
	public static final String ORDER_SUB_TOTAL = "orderSubTotal";
	
	/** The Constant ORDER_ID. */
	public static final String ORDER_ID = "orderId";
	
	/** Constant to hold webDisplayFlag. */
	public static final String WEB_DISPLAY_FLAG = "webDisplayFlag";
	
	/** CONSTANT TO HOLD supressInSearch. */
	public static final String SUPRESS_IN_SEARCH = "supressInSearch";
	
	/** CONSTANT TO HOLD REFINEMENT_CRUMBS. */
	public static final String REFINEMENT_CRUMBS = "refinementcrumbs";
	
	/** CONSTANT TO HOLD REFINEMENTS. */
	public static final String REFINEMENTS = "refinements";
	
	/** CONSTANT TO HOLD DIMENSION_NAME. */
	public static final String DIMENSION_NAME = "dimensionname";
	
	/** CONSTANT TO HOLD SELECTED_REFINEMENT. */
	public static final String SELECTED_REFINEMENT = "selectedRefinement";
	
	/** CONSTANT TO HOLD SELECTED. */
	public static final String SELECTED = "selected";
	
	/** CONSTANT TO HOLD REFINEMENT. */
	public static final String REFINEMENT = "refinement";
	
	/** CONSTANT TO HOLD NON_SELECTED. */
	public static final String NON_SELECTED = "nonselected";
	
	/** CONSTANT TO HOLD ERROR_GETTING_SITE. */
	public static final String ERROR_GETTING_SITE = "Error getting site ";
	
	/** CONSTANT TO HOLD ERROR_MESSAGE. */
	public static final String ERROR_MESSAGE = "errorMessage";
	
	/** CONSTANT TO HOLD HOOK_LOGIC_DETAILS. */
	public static final String HOOK_LOGIC_DETAILS = "hookLogicDetails";
	
	/** property to hold the PARAM_GENERATED_PASSWORD. */
	public static final String PARAM_GENERATED_PASSWORD = "generatedPassword";
	
	/** property to hold the ENCODED_EMAIL. */
	public static final String ENCODED_EMAIL = "encodedEmail";
	
	/** property to hold the ENCODED_TIMESTAMP. */
	public static final String ENCODED_TIMESTAMP = "encodedTimeStamp";
	
	/** property to hold the PARAM_LINK_EXPIRED. */
	public static final String PARAM_LINK_EXPIRED = "linkExpired";
	
	/**  Constant for SERVER_DATE_FORMAT. */
	public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
	
	/** Constant to hold CODE SERVER_TIME_ZONE. */
	public static final String SERVER_TIME_ZONE = "America/New_York";

	
	/** CONSTANT TO HOLD commerceItemMetaInfo. */
	public static final String COMMERCE_ITEM_METAINFO = "metaInfo";


	
	/** The Constant STATE. */
	public static final String STATE = "state";
	
	/** The Constant SUBMITTED. */
	public static final String SUBMITTED = "SUBMITTED";
	
	/** The Constant SUBMITTED_DATE. */
	public static final String SUBMITTED_DATE = "submittedDate";
	
	/** The Constant OUT_OF_STOCK_ITEMS. */
	public static final String OUT_OF_STOCK_ITEMS = "outOfStockItems";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_1. */
	public static final String STORE_MESSAGE_PDP_1 = "tru.productdetail.label.storeasearlyastoday";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_2. */
	public static final String STORE_MESSAGE_PDP_2 = "tru.productdetail.label.freestorepickup";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_3. */
	public static final String STORE_MESSAGE_PDP_3 = "tru.productdetail.label.freestorepickdays";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_4. */
	public static final String STORE_MESSAGE_PDP_4 = "tru.productdetail.label.freestorepickupnotavailable";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_5. */
	public static final String STORE_MESSAGE_PDP_5 = "tru.productdetail.label.freeStorePickupTodayAt";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_6. */
	public static final String STORE_MESSAGE_PDP_6 = "tru.productdetail.label.freeStorePickupInSomeDaysAt";
	
	/** Constant to hold CODE STORE_MESSAGE_PDP_7. */
	public static final String STORE_MESSAGE_PDP_7 = "tru.productdetail.label.freeStorePickupNotAvailableAt";
	
	public static final String STORE_MESSAGE_NEW_PDP_5 = "free store pickup today";
	public static final String STORE_MESSAGE_NEW_PDP_6 = "free store pickup in 5 to 10 days";
	public static final String STORE_MESSAGE_NEW_PDP_7 = "free store pickup not available";
	/**
	 * constant for CATEGORY_IMAGE_URL.
	 */
	public static final String CATEGORY_IMAGE_URL = "CategoryImageURL";
	/**
	 * constant for CATEGORYID_EQUALS_QUESTION.
	 */
	public static final String  CATEGORYID_EQUALS_QUESTION_ZERO = "categoryId = ?0";
	/**
	 * constant for IMAGE_URL.
	 */
	public static final String IMAGE_URL = "imageUrl";	
	/**  property to hold PAYPAL_DATE_FORMAT. */
	public static final String PROMOTION_END_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
	/**
	 * constant for HASH.
	 */
	public static final String HASH = "#";	
	
	/**  property to hold INVALID_CHANNEL_PARAMETER. */
	public static final String INVALID_CHANNEL_PARAMETER = "InvalidChannelParameter";
	/** The Constant TRU_SHOPPINGCART_CHANNEL_TYPE_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_CHANNEL_TYPE_MESSAGE_KEY = "tru_shoppingcart_channel_type_null";
	/** The Constant TRU_SHOPPINGCART_CHANNEL_USER_NAME_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_CHANNEL_USER_NAME_MESSAGE_KEY = "tru_shoppingcart_channel_user_name_null";
	
	/** The Constant HANDLE_INIT_REWARD_NUMBER_IN_ORDER. */
	public static final String HANDLE_INIT_REWARD_NUMBER_IN_ORDER="TRUBillingInfoFormHandler.handleInitRewardNumberInOrder";
	
	/** The Constant HANDLE_CHECKOUT_WITH_PAYPAL. */
	public static final String HANDLE_CHECKOUT_WITH_PAYPAL="TRUBillingInfoFormHandler.handleCheckoutWithPayPal";
	/** The Constant noProductList. */
	public static final String NO_PRODUCT_LIST="noProductList";
	
	/** The Constant DEFAULT_SKN_ORIGIN. */
	public static final String DEFAULT_SKN_ORIGIN="defaultSknOrigin";
	/** The Constant HANDLE_FETCH_GIFT_CARD_BALANCE. */
	public static final String HANDLE_FETCH_GIFT_CARD_BALANCE="GiftCardFormHandler.fetchGiftCardBalance";
	
	/** The Constant HANDLE_APPLY_GIFT_CARD. */
	public static final String HANDLE_APPLY_GIFT_CARD="GiftCardFormHandler.handleApplyGiftCard";
	
	/** The Constant HANDLE_REMOVE_GIFT_CARD. */
	public static final String HANDLE_REMOVE_GIFT_CARD="GiftCardFormHandler.handleRemoveGiftCard";
	
	/** The Constant HANDLE_ADJUST_PAYMENT_GROUPS. */
	public static final String HANDLE_ADJUST_PAYMENT_GROUPS="GiftCardFormHandler.handleAdjustPaymentGroups";
	
	/** The Constant productCatId. */
	public static final String PRODUCT_CATEGORY_NAME="productCatName";
	
	/** The Constant Navigation_TRU. */
	public static final String NAVIGATION_TRU="Navigation_TRU";
	
	/** The Constant ancestorCategoryIds. */
	public static final String ANCESTOR_CATEGORY_IDS="ancestorCategoryIds";
	
	/** Constant to hold productIdsForPromo. */
	public static final String PRODUCT_IDS_FOR_PROMO = "productIdsForPromo";
	
	/** The Constant TAIL_OF_REGISTRY_NAME. */
	public static final String TAIL_OF_REGISTRY_NAME = "'s";
	
	/** The Constant Navigation_BRU. */
	public static final String NAVIGATION_BRU="Navigation_BRU";
	
	/** The Constant LIST_CLASS. */
	public static final String LIST_CLASS="List";
	
	/** The Constant ARRAYLIST_CLASS. */
	public static final String ARRAYLIST_CLASS="ArrayList";
	
	/** The Constant CHANGE_AWARE_LIST_CLASS. */
	public static final String CHANGE_AWARE_LIST_CLASS="ChangeAwareList";
	
	/** The Constant SET_CLASS. */
	public static final String SET_CLASS="Set";
	
	/** The Constant HASHSET_CLASS. */
	public static final String HASHSET_CLASS="HashSet";
	
	/** The Constant CHANGE_AWARE_SET_CLASS. */
	public static final String CHANGE_AWARE_SET_CLASS="ChangeAwareSet";
	
	/** The Constant MAP_CLASS. */
	public static final String MAP_CLASS="Map";
	
	/** The Constant HASH_MAP_CLASS. */
	public static final String HASH_MAP_CLASS="HashMap";
	
	/** The Constant CHANGE_AWARE_MAP_CLASS. */
	public static final String CHANGE_AWARE_MAP_CLASS="ChangeAwareMap";
	
	/** The Constant CURRENT_SERVER_TIME. */
	public static final String CURRENT_SERVER_TIME = "currentTime";
	
	/** The Constant SERVER_TIME_FLAG. */
	public static final String SERVER_TIME_FLAG = "serverTimeFlag";
	
	/** The integer DIGIT_EIGHT. */
	public static final int DIGIT_EIGHT = 8;
	
	/** The integer DIGIT_THREE. */
	public static final int DIGIT_THREE = 3;
	
	/** The Constant TSP_CARD_NUMBER. */
	public static final String TSP_CARD_NUMBER = "tspCardNumber";
	
	/** The Constant TSP_CVV. */
	public static final String TSP_CVV = "tspCvv";
	
	/** The Constant TSP_EXP_MONTH. */
	public static final String TSP_EXP_MONTH = "tspExpMonth";
	
	/** The Constant TSP_EXP_YEAR. */
	public static final String TSP_EXP_YEAR = "tspExpYear";
	
	/** The Constant TSP_SINGLE_USE_COUPON_CODE. */
	public static final String TSP_SINGLE_USE_COUPON_CODE = "singleUseCouponCode";
	
	/** The Constant TSP_OLSON_REWARD_NUMBER. */
	public static final String TSP_OLSON_REWARD_NUMBER = "olsonRewardNumber";
	
	/** The Constant TSP_APPLICATION_DECISSION. */
	public static final String TSP_APPLICATION_DECISSION = "applicationDecision";
	
	/** The Constant ORDER_HAS_RUS_CARD. */
	public static final String ORDER_HAS_RUS_CARD = "hasRUSCard";
	
	/** The Constant TRU_SHOPPINGCART_NO_SKU_AVAILABLE. */
	public static final String TRU_SHOPPINGCART_NO_SKU_AVAILABLE = 
			"tru_shoppingcart_no_sku_available";
	
	/** The Constant ADDRESS1_PARAM. */
	public static final String ADDRESS1_PARAM = "address1";
	
	/** The Constant ADDRESS2_PARAM. */
	public static final String ADDRESS2_PARAM = "address2";
	
	/** The Constant PRODUCT_SUBCAT_NAME. */
	public static final String PRODUCT_SUBCAT_NAME = "productSubcategoryName";
	
	/** The Constant DATE_FORMAT_PROMOTION. */
	public static final String DATE_FORMAT_PROMOTION = "yyyy-MM-dd hh:mm:ss";
	
	/** The property to hold PARSE_EXCEPTION. */
	public static final String PARSE_EXCEPTION = "parsing exception :";
	
	/** The property to hold PROMOTION_ID. */
	public static final String PROMOTION_ID = "promoId";
	
	/** The property to hold PROMOTION_NAME. */
	public static final String PROMOTION_NAME = "promoName";
	
	/** The property to hold START_DATE. */
	public static final String START_DATE = "startDate";
	
	/** The property to hold END_DATE. */
	public static final String END_DATE = "endDate";
	
	/** The property to hold PRIORITY. */
	public static final String PRIORITY = "DISPLAY_PRIORITY";
	
	/** The property to hold PROMOTIONS. */
	public static final String PROMOTIONS = "promotions";
	
	/** The property to hold SKU_PROMOTION_DROPLET. */
	public static final String SKU_PROMOTION_DROPLET = "SKU PROMOTION DROPLET";
	/**
	 * it holds the value of DATE_LENGHT_SEVENTEEN.
	 */
	public final static  int DATE_LENGHT_SEVENTEEN =17;
	
	/**
	 * it holds the value of DATE_LENGHT_EIGHT.
	 */
	public final static  int DATE_LENGHT_EIGHT =8;
	/**
	 * it holds the value of DATE_LENGHT_ELEVAN.
	 */
	public final static  int DATE_LENGHT_ELEVAN =11;
	/**
	 * it holds the value of DATE_LENGHT_FOURTEEN.
	 */
	public final static  int DATE_LENGHT_FOURTEEN =14;
	/**
	 * it holds the value of DATE_LENGHT_FOUR.
	 */
	public final static  int DATE_LENGHT_FOUR =4;
	/**
	 * it holds the value of DATE_LENGHT_ZERO.
	 */
	public final static  int DATE_LENGHT_ZERO =0;
	/**
	 * it holds the value of DATE_LENGHT_TWO.
	 */
	public final static  int DATE_LENGHT_TWO =2;
	/**
	 * it holds the value of DATE_LENGHT_NINE.
	 */
	public final static  int DATE_LENGHT_NINE =9;
	/**
	 * it holds the value of DATE_LENGHT_TWELVE.
	 */
	public final static  int DATE_LENGHT_TWELVE =12;
	/**
	 * it holds the value of DATE_LENGHT_FIFTEEN.
	 */
	public final static  int DATE_LENGHT_FIFTEEN =15;
	/**
	 * it holds the value of DATE_LENGHT_ONE.
	 */
	public final static  int DATE_LENGHT_ONE =1;
	/**
	 * it holds the value of DASH.
	 */
	public final static  char DASH ='-';
	/**
	 * it holds the value of COLON.
	 */
	public final static  char COLON =':';
	// Fix for PMD Warning - AvoidHardcodingRule - START
	/**
	 * Constant to hold DATE_FORMAT
	 */
	public static final String  DATE_FORMAT_PREVEW= "yyyy-MM-dd_HH:mm:ss";
	
	/**
	 * Constant to hold DATE_12_FORMAT_PREVEW
	 */
	public static final String  USER_DATE_FORMAT_YYYY_MM_DD_HH_MM= "MM/dd/yyyy hh:mm a";
	
	/**
	 * Constant to hold DATE_24_FORMAT_PREVEW
	 */
	public static final String  ACCEPTED_DATE_FORMAT_MM_DDYYYY_HH_MM_SS= "MMddyyyy-HH:mm:ss";
	
	/** The property to hold OUTOFSTOCKINFO. */
	public static final String OUTOFSTOCKINFO = "outofstockinfo";
	
	/** Holds string  Constant CHANNEL_AVAILABILITY_STATUS. */
	
	public static final String CHANNEL_AVAILABILITY_STATUS = "NA";
	
	/** Holds string  Constant SHOW_GIFT_CARD_FLAG. */
	
	public static final String SHOW_GIFT_CARD_FLAG = "showGiftCardFlag";
	
		/** The Constant IS_USER_ALREADY_EXISTS. */
	public static final String IS_USER_ALREADY_EXISTS = "isUserAlreadyExists";
	
	/** The Constant CHILD_PRODUCTS. */
	public static final String CHILD_PRODUCTS = "childProducts";
	/** The Constant CHILD_SKUS. */
	public static final String CHILD_SKUS = "childSKUs";
	
	/** The Constant ONLINE_COLLECTION_NAME. */
	public static final String ONLINE_COLLECTION_NAME = "onlineCollectionName";
	
	/** Property to hold AJAX_REQUEST_URL  */
	public static final String AJAX_REQUEST_URL = "requestURL";
	
	/** Property to hold EXCEPTION_DETAILS  */
	public static final String EXCEPTION_DETAILS = "exceptionDetails";
	
	/** Property to hold EXCEPTIONS  */
	public static final String EXCEPTIONS ="exceptions";
	/** The Constant MAX_COUPON_LIMIT. */
	public static final String MAX_COUPON_LIMIT="maxCouponLimit";
	
	/** The Constant SKU_ID_COLUMN. */
	public static final String SKU_ID_COLUMN = "SKU_ID";
	
	/** The Constant DISPLAY_NAME_COLUMN. */
	public static final String PROMOTION_ID_COLUMN = "PROMOTION_ID"; 
	
	/** The Constant PROMOTION_ID_COLUMN. */
	public static final String DISPLAY_NAME_COLUMN = "DISPLAY_NAME"; 
	
		/** The Constant PROMOTION_ID_COLUMN. */
	public static final String DETAILS_COLUMN = "PROMO_DETAILS"; 
	
	/** The Constant DESCRIPTION. */
	public static final String DESCRIPTION_COLUMN = "DESCRIPTION";
	
	/** The Constant REPLACE_PIPE. */
	public static final String REPLACE_PIPE = "(^.|.$)";
	
	/** The Constant BACK_SLASHE. */
	public static final String BACK_SLASHE = "\'";
	/** The Constant BACK_SLASHES. */
	public static final CharSequence BACK_SLASHES = "\',\'";
	
	public static final CharSequence COMMA_WITH_SPACE = ", ";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_SUBJECT = "messageSubject";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_SUCC = "succmessageSub";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_FAIL = "failmessageSub";
	
	/** The Constant SITE_MAP . */
	public static final String SITE_MAP = "siteMap";

	/** The Constant CATEGORY_PAGE. */
	public static final String CATEGORY_PAGE = "category";

	/** The Constant SUB_CATEGORY_PAGE. */
	public static final String SUB_CATEGORY_PAGE = "subcat";
	
	/** The Constant FAMILY_PAGE. */
	public static final String FAMILY_PAGE = "family";
	
	/**
	 * constant for QUESTION_CATEGORYID_EQUALS.
	 */
	public static final String  QUESTION_CATEGORYID_EQUALS= "?categoryid=";
	
	/**
	 * constant for TAXONOMYCATEGORY.
	 */
	public static final String  TAXONOMYCATEGORY= "TAXONOMYCATEGORY";
	
	/**
	 * constant for TAXONOMYSUBCATEGORY.
	 */
	public static final String  TAXONOMYSUBCATEGORY= "TAXONOMYSUBCATEGORY";
	
	/**
	 * constant for TAXONOMYNODE.
	 */
	public static final String  TAXONOMYNODE= "TAXONOMYNODE";
	
	/**
	 * constant for TAXONOMYFILTEREDLANDINGPAGE.
	 */
	public static final String  TAXONOMYFILTEREDLANDINGPAGE= "TAXONOMYFILTEREDLANDINGPAGE";
	/**
	 * constant for PRIMARY_PATH_CATEGORYVO_LIST.
	 */
	public static final String PRIMARY_PATH_CATEGORYVO_LIST =	"primaryPathCategoryVOList";
	
	/**
	 * constant for END_USABLE.
	 */
	public static final String END_USABLE = "END_USABLE";
	
	/**
	 * constant for BEGIN_USABLE.
	 */
	public static final String BEGIN_USABLE = "BEGIN_USABLE";
	
	
	/** The Constant COLLECTION_IMAGE. */
	public static final String COLLECTION_IMAGE = "collectionImage";
	
	/** The Constant COLLECTION_URL. */
	public static final String COLLECTION_URL = "collectionUrl";
	
	/** The Constant SKU_STORE_ELIGIBLE_ARRAY. */
	public static final String SKU_STORE_ELIGIBLE_ARRAY = "skuStoreEligibleArray";
	
	/** The Constant DOT_SEPERATOR. */
	public static final String DOT_SEPERATOR = "\\.";
	
	/** The Constant INVENTORY_AVAILABLE. */
	public static final String INVENTORY_AVAILABLE = "InventoryAvalialble";
	
	/** The store available. */
	public static final String STORE_AVAILABLE = "StoreAvailable";
	/** The DISABLE. */
	public static final String DISABLE = "disable";
	
	/** Constant for BASELINE_ENDECA_INDEXING.*/
	public static final String BASELINE_ENDECA_INDEXING = "baseLineEndecaIndexing";
	
	/** Constant forPARTIAL_ENDECA_INDEXING. */
	public static final String PARTIAL_ENDECA_INDEXING = "partialEndecaIndexing";
	
	/** Constant for PARAMETRIC_FEED. */
	public static final String PARAMETRIC_FEED = "parametericFeed";
	
	/** Constant for PARAMETRIC_CSV. */
	public static final String PARAMETRIC_CSV= "parametricCSV";
	
	/** Constant for COLLECTION_PRICE. */
	public static final String COLLECTION_PRICE= "collectionprice";
	
	/** Constant for MASK_CHARACTER. */
	public static final String MASK_CHARACTER= "maskcharacter";
	
	/** Constant for NUM_CHARS_UNMASKED. */
	public static final String NUM_CHARS_UNMASKED= "numcharsunmasked";
	
	/** Constant for GROUPING_SIZE. */
	public static final String GROUPING_SIZE= "groupingsize";
	
	/** Constant for STRING_X. */
	public static final String STRING_X= "X";

	/** Constant for PAYMENT_METHOD. */
	public static final String PAYMENT_METHOD= "paymentMethod";
	
	/** Constant for APPLE_PAY. */
	public static final String APPLE_PAY= "applepay";
	
	/** Constant for CATALOG_REF_IDS. */
	public static final String CATALOG_REF_IDS= "catalogRefIds";
	
	/** Constant for APPLE_PAY_CART. */
	public static final String APPLE_PAY_CART= "ApplePayCart";
	
	/** Constant for APPLE_PAY_PDP. */
	public static final String APPLE_PAY_PDP= "ApplePayPdp";
	
	/** Constant for ORDER_EMPTY. */
	public static final String ORDER_EMPTY = "orderEmpty";
	/** Constant for ALLOW_EMPTY_ORDERS. */
	public static final String ALLOW_EMPTY_ORDERS = "allowEmptyOrders";
	/** The Constant API_CHANNEL. */
	public static final String API_CHANNEL = "X-APP-API_CHANNEL";
	/** The Constant B. */
	public static final String B = "B";
	/** The Constant B. */
	public static final String N = "N";
	/** The Constant B. */
	public static final String R = "R";
	/** Constant to hold TEST_MODE_STOCK_LEVEL. */
	public static final long TEST_MODE_STOCK_LEVEL = 99L;
	/** Constant to hold lazy. */
	public static final String LAZY = "lazy";
	public static final String INCOMPLETE = "INCOMPLETE";
	/** Constant for CART_EMPTY. */
	public static final String CART_EMPTY = "cart is empty";
	/** Constant for SHIPPINGGROUP_CONTAINER_SERVICE_COMPONENT_NAME. */
	public static final String SHIPPINGGROUP_CONTAINER_SERVICE_COMPONENT_NAME = "/atg/commerce/order/purchase/ShippingGroupContainerService";
	/** 
	 * Constant for IS_STYLIZED
	 */
	public static final String IS_STYLIZED = "isStylized";
	/** 
	 * Constant for IS_COLLECTION_PRODUCT
	 */
	public static final String IS_COLLECTION_PRODUCT = "isCollectionProduct";
	/** 
	 * Constant for COLLECTION_SKUS
	 */
	public static final String COLLECTION_SKUS = "collectionSkus";
	/**
	 * constants for HasPriceRange.
	 */
	public static final String HAS_PRICE_RANGE = "hasPriceRange";
	
	/**
	 * constants for true.
	 */
	public static final String TRUE_STRING = "true";
	
	/**
	 * constants for false.
	 */
	public static final String FALSE_STRING = "false";
	/**
	 * constants for sku.activePrice.
	 */
	public static final String SKU_ACTIVEPRICE = "sku.activePrice";
	/** The Constant IS_PRICE_EXISTS. */
	public static final String IS_PRICE_EXISTS ="IsPriceExists";
	/**
	 * property to hold min listPrice constant.
	 */
	public static final String MIN_PRICE = "minimumPrice";
	/**
	 * property to hold max listPrice constant.
	 */
	public static final String MAX_PRICE = "maximumPrice";
	/**
	 * Property to hold String PRICE_ASCENDING_SORT_PARAMETER.
	 */
	public static final String PRICE_ASCENDING_SORT_PARAMETER = "priceAscendingSort";

	/**
	 * Property to hold String PRICE_DESCENDING_SORT_PARAMETER.
	 */
	public static final String PRICE_DESCENDING_SORT_PARAMETER = "priceDescendingSort";
	/**
	 * constants for SKU_listPrice.
	 */
	public static final String SKU_LISTPRICE = "sku.listPrice";
	/**
	 * constants for SKU_salePrice.
	 */
	public static final String SKU_SALEPRICE = "sku.salePrice";
	/**
	 * constant for holding Strike-Through.
	 */
	public static final String STRIKE_THROUGH = "isStrikeThrough";
	/**
     * constants for COLLECTION_SKU_LISTSIZE.
     */
     public static final String COLLECTION_SKU_LISTSIZE = "skuListSize";
     /**
      * constants for Milliseconds.
      */
      public static final String STR_MILLISECONDS = "MSec";
      
      /** The Constant IS_LAND_TAX. */
      public static final String IS_LAND_TAX = "islandTax";
      
      /** The Constant LOCAL_TAX. */
      public static final String LOCAL_TAX = "localTax";
      
      /**
  	 * constants for BACK_PARENTHESES .
  	 */
  	public static final String REGISTRY_REQ  = "registry";
  	
  	/**
	 * constants for BACK_PARENTHESES .
	 */
	public static final String ONE_STR  = "1";
    
	/** The Constant CARD_NUMBER. */
	public static final String CARD_NUMBER = "cardNumber";
	
	/** The Constant IS_PRE_SELLABLE. */
	public static final String IS_PRE_SELLABLE = "presellable";
	
	/** The Constant ONLINEPID. */
	public static final String ONLINEPID_SKU = "onlinePidSku";
	
	/** The Constant STORE_INVENTORY_AVAILABLE. */
	public static final String STORE_INVENTORY_AVAILABLE = "StoreInventoryAvalialble";
	
	/** The Constant MYACCOUNT. */
	 public static final String MYACCOUNT = "myAccount";

	/** Constant to hold COHERENCE_OUT_OF_STOCK. */
	public static final int COHERENCE_OUT_OF_STOCK = 16770;

	/** The Constant for promomap. */
	public static final String PROMOTION_DISPLAY_MAP = "promotionDisplayMap";

	/** Constant to hold BILLINGFIRSTNAME. */
	public static final String BILLINGFIRSTNAME = "BillingFirstName";

	/** Constant to hold BILLINGLASTNAME. */
	public static final String BILLINGLASTNAME = "BillingLastName";

	/** Constant to hold FILTERED_ITEM_LIST. */

	public static final String FILTERED_ITEM_LIST = "filtereditemsList";

	/** Constant to hold TRU_GIFT_WRAP. */
	public static final String TRU_GIFT_WRAP = "TRUGiftWrap";

	/** Constant to hold BRU_GIFT_WRAP. */
	public static final String BRU_GIFT_WRAP = "BRUGiftWrap";

	/** Constant to hold CHILD_SKU_LIST. */
	public static final String CHILD_SKU_LIST = "childSkuList";
	
	/** The Constant TRU_SHOPPINGCART_ITEM_SKU_NOT_AVAILABLE_CATALOG_MESSAGE_KEY. */
	public static final String TRU_SHOPPINGCART_ITEM_SKU_NOT_AVAILABLE_CATALOG_MESSAGE_KEY = 
			"tru_shoppingcart_item_not_avail_info_message";

}