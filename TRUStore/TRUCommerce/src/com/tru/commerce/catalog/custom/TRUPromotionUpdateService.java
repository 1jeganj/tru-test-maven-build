package com.tru.commerce.catalog.custom;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import javax.sql.DataSource;

import atg.adapter.gsa.GSAItemDescriptor;
import atg.adapter.gsa.GSARepository;
import atg.commerce.catalog.custom.CatalogProperties;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.service.jdbc.SwitchingDataSource;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.TRUConstants;

/**
 *This class is to update the promotions and product associations
 */
public class TRUPromotionUpdateService extends GenericService {

	public static final String SKU_ID = "sku_id";

	public static final int MINUS_ONE = -1;

	public static final long _2000L = 2000L;

	/**
	 * Holds ProductPromoGenerator
	 */
	public static final String PRODUCT_PROMO_GENERATOR = "ProductPromoGenerator";

	/**
	 * Holds TRUPromotionUpdateService
	 */
	public static final String TRU_PROMOTION_UPDATE_SERVICE = "TRUPromotionUpdateService";

	/**
	 * Holds procedure name
	 */
	private String mUpdateProductPromoSQL;

	/**
	 * Holds value to whether update promotions or not
	 */
	private boolean mUpdatePromotionsProperty;
	/**
	 * Holds value to skusToInvalidateSQL
	 */
	private String mSkusToInvalidateSQL;
	/**
	 * Holds value to mInvalidatedSKUAssetIdsCountSQL
	 */
	private String mInvalidatedSKUAssetIdsCountSQL;
	/**
	 * Holds value to invalidateFetchSize
	 */
	private int mInvalidateFetchSize;
	/**
	 * Holds value to mClearCMSWorkingDataTimeout
	 */
	private int mClearCMSWorkingDataTimeout;
	/**
	 * Holds value to truncateInvalidSkuIdsTableSQL
	 */
	private String mTruncateInvalidSkuIdsTableSQL;
	/**
	 * Holds value to mAlwaysWalkTree
	 */
	private boolean mAlwaysWalkTree;
	/**
	 * Holds value to mCacheInvalidationThresholdAlreadyReached
	 */
	private boolean mCacheInvalidationThresholdAlreadyReached;
	/**
	 * Holds value to cacheInvalidationThreshold
	 */
	private int mCacheInvalidationThreshold;
	/** mCatalogTools */
	private TRUCatalogTools mCatalogTools;
	/** mCatalogProperties */
	private TRUCatalogProperties mCatalogProperties;
	/** mRepository */
	private GSARepository mRepository;

	/**
	 * This holds the name to log debug
	 * @return String - Log Name
	 */
	public String getNameToLog()
	{
		return TRU_PROMOTION_UPDATE_SERVICE;
	}

	/**
	 * This method is to update the SKUs with associated promotions by running a stored procedure.
	 */
	public void updatePromotions() {
		if(isLoggingDebug()){
			logDebug("Enter into : TRUPromotionUpdateService and method : updatePromotions ");
		}
		if(!isUpdatePromotionsProperty()){
			if(isLoggingDebug()){
				vlogDebug("isUpdatePromotionsProperty :: {0}", isUpdatePromotionsProperty());
			}
			return;
		}
		Connection dbConnection = null;
		CallableStatement stmt = null;
		try {
			DataSource dataSource = ((GSARepository)getRepository()).getDataSource();
			SwitchingDataSource switchingDS = (SwitchingDataSource) dataSource;
			DataSource stagingDataSource = (DataSource) switchingDS.getStagingDataStore();
			dbConnection = stagingDataSource.getConnection();
			if(isLoggingDebug()){
				vlogDebug("DB Schema : {0}", dbConnection.getSchema());
			}
			boolean autoCommit = dbConnection.getAutoCommit();

			dbConnection.setAutoCommit(false);

			stmt = dbConnection.prepareCall(getUpdateProductPromoSQL());

			if (isLoggingInfo()) {
				logInfo("Started update product promotions.");
			}
			stmt.executeUpdate();
			if (isLoggingInfo()) {
				logInfo("Completed update product promotions.");
			}
			stmt.close();
			dbConnection.commit();
			dbConnection.setAutoCommit(autoCommit);

		} catch (SQLException exc) {
			if(isLoggingError()){
				vlogError("SQLException: While connecting to data base : {0}", exc);
			}
		}
		if (dbConnection != null){
			((GSARepository)getRepository()).close(dbConnection);
		}
		try {
			int invalidateCount = invalidateCaches();
			if (isLoggingInfo()) {
				if (invalidateCount >= 0) {
					logInfo("Cache invalidation finished. Invalidated " + invalidateCount + " assets.");
				}
				else {
					logInfo("Cache invalidation finished. Reached threshold so entire cache for product and sku assets was invalidated.");
				}
			}
			clearWorkingData();
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("Repository Exception : {0}", e);
			}
		}
		
		if(isLoggingDebug()){
			logDebug("Exit from : TRUPromotionUpdateService and method : updatePromotions ");
		}		
	}

	/**
	 * Invalidate caches.
	 *
	 * @return the int
	 * @throws RepositoryException the repository exception
	 */
	protected int invalidateCaches()
			throws RepositoryException
 {
		CatalogProperties catalogProperties = getCatalogProperties();
		GSARepository repository = (GSARepository) getRepository();
		GSAItemDescriptor skuDesc = (GSAItemDescriptor) repository
				.getItemDescriptor(catalogProperties.getSkuItemName());
		if(isLoggingDebug()){
			logDebug("Invalidating sku caches : START");
		}
		skuDesc.invalidateCaches();
		if(isLoggingDebug()){
			logDebug("Invalidating sku cache : END");
		}
		return MINUS_ONE;

	}

	/**
	 * Should invalidate by id.
	 *
	 * @return true, if successful
	 * @throws RepositoryException the repository exception
	 */
	protected boolean shouldInvalidateById()
			throws RepositoryException
			{
		if (getCacheInvalidationThreshold() == MINUS_ONE) {
			return true;


		}

		if ((isCacheInvalidationThresholdAlreadyReached()) || (getCacheInvalidationThreshold() == 0)) {
			return false;


		}

		Connection dbConnection = null;
		ResultSet rs = null;

		PreparedStatement getInvalidSKUIdsStatement = null;

		try
		{
			DataSource dataSource = ((GSARepository)getRepository()).getDataSource();
			SwitchingDataSource switchingDS = (SwitchingDataSource) dataSource;
			DataSource stagingDataSource = (DataSource) switchingDS.getStagingDataStore();
			dbConnection = stagingDataSource.getConnection();

			int skusToInvalidate = 0;





			if (skusToInvalidate > getCacheInvalidationThreshold())

			{
				setCacheInvalidationThresholdAlreadyReached(true);
				return false;
			}
			getInvalidSKUIdsStatement = dbConnection.prepareStatement(getInvalidatedSKUAssetIdsCountSQL());
			rs = getInvalidSKUIdsStatement.executeQuery();

			while ((rs != null) && 
					(rs.next())) {
				skusToInvalidate = rs.getInt(TRUConstants._1);

			}

			if (skusToInvalidate  > getCacheInvalidationThreshold()) {
				setCacheInvalidationThresholdAlreadyReached(true);
				return false;   
			}
		}    catch (SQLException e)    {   
			if(isLoggingError()){
				vlogError("SQLException : {0}", e);
			}
		}
		finally
		{
			try
			{
				if ((rs != null) && 
						(!(rs.isClosed()))) {
					rs.close();
				}
			}
			catch (SQLException e)
			{
				if(isLoggingError()){
					vlogError("SQLException : {0}", e);
				}
			}

			try
			{
				if (getInvalidSKUIdsStatement != null) {
					getInvalidSKUIdsStatement.close();
				}
			}
			catch (SQLException e)
			{
				if(isLoggingError()){
					vlogError("SQLException : {0}", e);
				}
			}
			 ((GSARepository)getRepository()).close(dbConnection);
		}
		return Boolean.FALSE;
			}

	/**
	 * Clear working data.
	 *
	 * @throws RepositoryException the repository exception
	 */
	protected void clearWorkingData()
			throws RepositoryException
	{
		if (!(isAlwaysWalkTree())) {
			setCacheInvalidationThresholdAlreadyReached(true);
			//getDynamicProductsByCategoryMap().clear();

			Connection dbConnection = null;
			Connection dbConnectionSkus = null;

			CallableStatement truncateInvalidSkuIdsStmt = null;

			long timeNow = Calendar.getInstance().getTimeInMillis();
			boolean workingDataCleared = false;
			boolean timeoutOccurred = false;
			try {
				while ((!(workingDataCleared)) && (!(timeoutOccurred))){
					DataSource dataSource = ((GSARepository)getRepository()).getDataSource();
					SwitchingDataSource switchingDS = (SwitchingDataSource) dataSource;
					DataSource stagingDataSource = (DataSource) switchingDS.getStagingDataStore();
					dbConnection = stagingDataSource.getConnection();
					boolean autoCommit = dbConnection.getAutoCommit();


					dbConnectionSkus = ((GSARepository)getRepository()).getConnection();

					truncateInvalidSkuIdsStmt = dbConnectionSkus.prepareCall(getTruncateInvalidSkuIdsTableSQL());
					truncateInvalidSkuIdsStmt.execute();
					if (!(autoCommit)) {
						dbConnection.commit();
					}
					workingDataCleared = true;
				}
			}catch (SQLException e) {
				if (Calendar.getInstance().getTimeInMillis() - timeNow > getClearCMSWorkingDataTimeout()) {
					timeoutOccurred = true;
					if (isLoggingInfo()) {
						logInfo("Unable to successfully clean up CMS data, and cms clean up timeout has been reached.");
					}
					throw new RepositoryException(e);
				}
				if (isLoggingInfo()){
					logInfo("Unable to successfully clean up CMS data. Clean up will be retried until timeout is reached.");
				}
				try
				{
					Thread.sleep(_2000L);
				} catch (InterruptedException ex) {
					if (isLoggingError()) {
						vlogError("Thread sleep between attempts to clear CMS workin data interrupted. : {0}", ex);
					}
				}
			}
			finally
			{

				if (truncateInvalidSkuIdsStmt != null) {
					try {
						truncateInvalidSkuIdsStmt.close();
					}
					catch (SQLException e)
					{
						if(isLoggingError()){
							vlogError("SQLException : {0}", e);
						}
					}
				}
				if (dbConnection != null) {
					((GSARepository)getRepository()).close(dbConnection);
				}

				if (dbConnectionSkus != null){
					((GSARepository)getRepository()).close(dbConnectionSkus);
				}
			}
		}
	}

	/**
	 * @return the updatePromotionsProperty
	 */
	public boolean isUpdatePromotionsProperty() {
		return mUpdatePromotionsProperty;
	}

	/**
	 * @param pUpdatePromotionsProperty the updatePromotionsProperty to set
	 */
	public void setUpdatePromotionsProperty(boolean pUpdatePromotionsProperty) {
		mUpdatePromotionsProperty = pUpdatePromotionsProperty;
	}

	/**
	 * @return the updateProductPromoSQL
	 */
	public String getUpdateProductPromoSQL() {
		return mUpdateProductPromoSQL;
	}

	/**
	 * @param pUpdateProductPromoSQL the updateProductPromoSQL to set
	 */
	public void setUpdateProductPromoSQL(String pUpdateProductPromoSQL) {
		mUpdateProductPromoSQL = pUpdateProductPromoSQL;
	}


	/**
	 * This method returns the skusToInvalidateSQL value
	 *
	 * @return the skusToInvalidateSQL
	 */
	public String getSkusToInvalidateSQL() {
		return mSkusToInvalidateSQL;
	}


	/**
	 * This method sets the skusToInvalidateSQL with parameter value pSkusToInvalidateSQL
	 *
	 * @param pSkusToInvalidateSQL the skusToInvalidateSQL to set
	 */
	public void setSkusToInvalidateSQL(String pSkusToInvalidateSQL) {
		mSkusToInvalidateSQL = pSkusToInvalidateSQL;
	}


	/**
	 * This method returns the invalidatedSKUAssetIdsCountSQL value
	 *
	 * @return the invalidatedSKUAssetIdsCountSQL
	 */
	public String getInvalidatedSKUAssetIdsCountSQL() {
		return mInvalidatedSKUAssetIdsCountSQL;
	}


	/**
	 * This method sets the invalidatedSKUAssetIdsCountSQL with parameter value pInvalidatedSKUAssetIdsCountSQL
	 *
	 * @param pInvalidatedSKUAssetIdsCountSQL the invalidatedSKUAssetIdsCountSQL to set
	 */
	public void setInvalidatedSKUAssetIdsCountSQL(
			String pInvalidatedSKUAssetIdsCountSQL) {
		mInvalidatedSKUAssetIdsCountSQL = pInvalidatedSKUAssetIdsCountSQL;
	}


	/**
	 * This method returns the invalidateFetchSize value
	 *
	 * @return the invalidateFetchSize
	 */
	public int getInvalidateFetchSize() {
		return mInvalidateFetchSize;
	}


	/**
	 * This method sets the invalidateFetchSize with parameter value pInvalidateFetchSize
	 *
	 * @param pInvalidateFetchSize the invalidateFetchSize to set
	 */
	public void setInvalidateFetchSize(int pInvalidateFetchSize) {
		mInvalidateFetchSize = pInvalidateFetchSize;
	}


	/**
	 * This method returns the clearCMSWorkingDataTimeout value
	 *
	 * @return the clearCMSWorkingDataTimeout
	 */
	public int getClearCMSWorkingDataTimeout() {
		return mClearCMSWorkingDataTimeout;
	}


	/**
	 * This method sets the clearCMSWorkingDataTimeout with parameter value pClearCMSWorkingDataTimeout
	 *
	 * @param pClearCMSWorkingDataTimeout the clearCMSWorkingDataTimeout to set
	 */
	public void setClearCMSWorkingDataTimeout(int pClearCMSWorkingDataTimeout) {
		mClearCMSWorkingDataTimeout = pClearCMSWorkingDataTimeout;
	}


	/**
	 * This method returns the truncateInvalidSkuIdsTableSQL value
	 *
	 * @return the truncateInvalidSkuIdsTableSQL
	 */
	public String getTruncateInvalidSkuIdsTableSQL() {
		return mTruncateInvalidSkuIdsTableSQL;
	}


	/**
	 * This method sets the truncateInvalidSkuIdsTableSQL with parameter value pTruncateInvalidSkuIdsTableSQL
	 *
	 * @param pTruncateInvalidSkuIdsTableSQL the truncateInvalidSkuIdsTableSQL to set
	 */
	public void setTruncateInvalidSkuIdsTableSQL(
			String pTruncateInvalidSkuIdsTableSQL) {
		mTruncateInvalidSkuIdsTableSQL = pTruncateInvalidSkuIdsTableSQL;
	}


	/**
	 * This method returns the alwaysWalkTree value
	 *
	 * @return the alwaysWalkTree
	 */
	public boolean isAlwaysWalkTree() {
		return mAlwaysWalkTree;
	}


	/**
	 * This method sets the alwaysWalkTree with parameter value pAlwaysWalkTree
	 *
	 * @param pAlwaysWalkTree the alwaysWalkTree to set
	 */
	public void setAlwaysWalkTree(boolean pAlwaysWalkTree) {
		mAlwaysWalkTree = pAlwaysWalkTree;
	}


	/**
	 * This method returns the cacheInvalidationThresholdAlreadyReached value
	 *
	 * @return the cacheInvalidationThresholdAlreadyReached
	 */
	public boolean isCacheInvalidationThresholdAlreadyReached() {
		return mCacheInvalidationThresholdAlreadyReached;
	}


	/**
	 * This method sets the cacheInvalidationThresholdAlreadyReached with parameter value pCacheInvalidationThresholdAlreadyReached
	 *
	 * @param pCacheInvalidationThresholdAlreadyReached the cacheInvalidationThresholdAlreadyReached to set
	 */
	public void setCacheInvalidationThresholdAlreadyReached(
			boolean pCacheInvalidationThresholdAlreadyReached) {
		mCacheInvalidationThresholdAlreadyReached = pCacheInvalidationThresholdAlreadyReached;
	}


	/**
	 * This method returns the cacheInvalidationThreshold value
	 *
	 * @return the cacheInvalidationThreshold
	 */
	public int getCacheInvalidationThreshold() {
		return mCacheInvalidationThreshold;
	}


	/**
	 * This method sets the cacheInvalidationThreshold with parameter value pCacheInvalidationThreshold
	 *
	 * @param pCacheInvalidationThreshold the cacheInvalidationThreshold to set
	 */
	public void setCacheInvalidationThreshold(int pCacheInvalidationThreshold) {
		mCacheInvalidationThreshold = pCacheInvalidationThreshold;
	}

	/**
	 * This method is to get catalogTools
	 *
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * This method sets catalogTools with pCatalogTools
	 *
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method is to get catalogProperties
	 *
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * This method sets catalogProperties with pCatalogProperties
	 *
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * This method is to get repository
	 *
	 * @return the repository
	 */
	public GSARepository getRepository() {
		return mRepository;
	}

	/**
	 * This method sets repository with pRepository
	 *
	 * @param pRepository the repository to set
	 */
	public void setRepository(GSARepository pRepository) {
		mRepository = pRepository;
	}

}
