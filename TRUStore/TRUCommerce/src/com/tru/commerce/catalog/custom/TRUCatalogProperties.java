package com.tru.commerce.catalog.custom;

import atg.commerce.catalog.custom.CatalogProperties;

/**
 * This class used to configure product catalog related item descriptor names
 * and property names.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCatalogProperties extends CatalogProperties {
	
	
	/**
	 * Property to hold mPrimaryParentCategoryPropertyName.
	 */
	private String mProductGenderPropertyName;
	/**
	 * Property to hold mPrimaryParentCategoryPropertyName.
	 */
	private String mPrimaryParentCategoryPropertyName;
	
	/**
	 * Property to hold mPrimaryPathCategoryPropertyName.
	 */
	private String mPrimaryPathCategoryPropertyName;
	
	/**
	 * Property to hold mSiteSpecificStaticPageUrlPropertyName.
	 */
	private String mSiteSpecificStaticPageUrlPropertyName;
	
	/**
	 * Property to hold mUserTypeId.
	 */
	private String mUserTypeId;
	
	/**
	 * Property to hold mCollectionSkusuId.
	 */
	private String mCollectionSkus;
	/**
	 * Property to hold mCollectionID.
	 */
	private String mCollectionID;
	
/**
	 *Property to hold  ancestorCategoryIdProperty.
	 */
	private String mAncestorCategoryIdProperty;
	/**
	 *Property to hold  skuQualifiedForPromos.
	 */
	private String mSkuPromoPropertyName;
	/**
	 * Property to hold emailAddress.
	 */
	private String mEmailAddressPropertyName;

	/**
	 * Property to hold productId.
	 */
	private String mProductIdPropertyName;

	/**
	 * Property to hold skuId.
	 */
	private String mSkuIdPropertyName;

	/**
	 * Property to hold NMWAReqDate.
	 */
	private String mNMWAReqDatePropertyName;
	/**
	 * Property to hold NmwaAlert.
	 */
	private String mNmwaAlertPropertyName;
	/**
	 * Property to hold siteIds.
	 */
	private String mSiteIds;
	
	 /**Property to hold  mCrossSellSku.
	 */
	private String mCrossSellSkuId;
	
	 /**Property to hold  mCrosssellBatteries.
		 */
	private String mCrosssellBatteries;
	
	 /**Property to hold  mCrossSellsBatteries.
	 */
	private String mCrossSellsBatteries;
	
	 /**Property to hold  mUidBatteryCrossSell.
		 */
	private String mUidBatteryCrossSell;
	/**
	 * Gets the mSkuPromoPropertyName.
	 *
	 * @return the mSkuPromoPropertyName
	 */
	public String getSkuPromoPropertyName() {
		return mSkuPromoPropertyName;
	}
	/**
	 * Sets the sku promotion name.
	 *
	 * @param pSkuPromoPropertyName the SkuPromoPropertyName to set
	 */
	public void setSkuPromoPropertyName(String pSkuPromoPropertyName) {
		this.mSkuPromoPropertyName = pSkuPromoPropertyName;
	}

	/** Property to hold Product Attributes Property Name. */
	private String mProductAttributesPropertyName;

	/** Property to hold attributeName Property Name. */
	private String mAttributeNamePropertyName;

	/** Property to hold attributeValue Property Name. */
	private String mAttributeValuePropertyName;

	/** Property to hold attributeGroup Property Name. */
	private String mAttributeGroupPropertyName;

	/** Property to hold attributeGroupId Property Name. */
	private String mAttributeGroupIdPropertyName;

	/**
	 * Property to hold CategoryDisplayNamePropertyName.
	 */
	private String mCategoryDisplayNamePropertyName;

	/**
	 * Property to hold mProductPromotions.
	 */
	private String mProductPromotions;

	/**
	 * Property to hold mProductQualifiedForPromotions.
	 */
	private String mProductQualifiedForPromotions;

	/**
	 * Property to hold mPromotionDetails.
	 */
	private String mPromotionDetails;

	/**
	 * Property to hold mAdditionalProductionURLs.
	 */
	private String mAdditionalProductionURLs;
	/**
	 * Property to hold mDescription.
	 */
	private String mDescription;

	/**
	 * Property to hold mColorName.
	 */
	private String mColorName;

	/**
	 * Property to hold mColorDisplaySequence.
	 */
	private String mColorDisplaySequence;

	/**
	 * Property to hold mSizeDescription.
	 */
	private String mSizeDescription;

	/**
	 * Property to hold mSizeDisplaySequence.
	 */
	private String mSizeDisplaySequence;

	/**
	*Property to hold  mSkuQualifiedForPromos.
	*/
	private String mSkuQualifiedForPromos;
	/**
	 * Property to hold ClassificationItemDescPropertyName.
	 */
	private String mClassificationItemDescPropertyName;
	/**
	 * Property to hold collectionProductItemDescPropertyName.
	 */
	private String mCollectionProductItemDescPropertyName;
	/**
	 * Property to hold ChildClassificationsItemPropertyName.
	 */

	private String mChildClassificationsItemPropertyName;

	/**
	 * Property to hold RootParentCategoryItemPropertyName.
	 */
	private String mParentClassificationsItemPropertyName;

	/**
	 * Property to hold ClassificationParentCategoryPropertyName.
	 */
	private String mClassificationParentCategoryPropertyName;

	/**
	 * Property to hold Product DisplayName.
	 */
	private String mProductDisplayNamePropertyName;

	/** Property to hold productDescriptionPropertyName. */
	private String mProductDescriptionPropertyName;

	/**
	 * Property to hold mDisplayStatusPropertyName.
	 */
	private String mDisplayStatusPropertyName;

	/**
	 * Property to hold deletedPropertyName.
	 */
	private String mDeletedPropertyName;

	/**
	 * The media banner key .
	 */
	private String mBannerKey;
	/**
	 * The related media content.
	 */
	private String mRelatedMediaContent;

	/**
	 * The image url.
	 */
	private String mImageUrl;

	/** Property to hold mProductLongDescription. */
	private String mProductLongDescription;

	/** Property to hold mProductMfrSuggestedAgeMax. */
	private String mProductMfrSuggestedAgeMax;

	/** Property to hold mProductMfrSuggestedAgeMin. */
	private String mProductMfrSuggestedAgeMin;

	/** Property to hold mProductSafetyWarnings. */
	private String mProductSafetyWarnings;

	/** Property to hold mProductBrandNamePrimary. */

	private String mProductBrandNamePrimary;

	/** Property to hold mProductRusItemNumber. */
	private String mProductRusItemNumber;

	/** Property to hold mProductSizeChartName. */
	private String mProductSizeChartName;

	/** Property to hold mProductManufacturerStyleNumber. */
	private String mProductManufacturerStyleNumber;

	/** Property to hold mSkuColorCode. */
	private String mSkuColorCode;

	/** Property to hold mSkuJuvenileSize. */
	private String mSkuJuvenileSize;

	/** Property to hold productFeature. */
	private String mProductFeature;

	/** Property to hold mUpcNumbers. */
	private String mUpcNumbers;

	/** Property to hold mSkuType. */
	private String mSkuType;

	/** Property to hold mSkuAlternateImages. */
	private String mSkuAlternateImages;
	
	/** Property to hold mSkuAlternateCanonicalImageUrl. */
	private String mSkuAlternateCanonicalImageUrl;
	
	/** Property to hold mSkuPrimaryCanonicalImageUrl. */
	private String mSkuPrimaryCanonicalImageUrl;
	
	/** Property to hold mSkuSecondaryCanonicalImageUrl. */
	private String mSkuSecondaryCanonicalImageUrl;
	
	/** Property to hold mSkuSwatchCanonicalImageUrl. */
	private String mSkuSwatchCanonicalImageUrl;
	

	/** Property to hold mSkuPrimaryImage. */
	private String mSkuPrimaryImage;

	/** Property to hold mSkuSecondaryImage. */
	private String mSkuSecondaryImage;

	/** Property to hold mSkuSwatchImage. */
	private String mSkuSwatchImage;

	/** Property to hold mProductStyleDimension. */
	private String mProductStyleDimension;

	/** Property to hold mSkuPromotionalStickerDisplay. */
	private String mSkuPromotionalStickerDisplay;

	/** Property to hold mIsSkuDeleted. */
	private String mIsSkuDeleted;

	/** Property to hold mSkuVideoGameEsrb. */
	private String mSkuVideoGameEsrb;
	/**
	 * Property to hold mBrand.
	 */
	private String mBrand;
	/**
	 * Property to hold mBatteryRequired.
	 */
	private String mBatteryRequired;
	/**
	 * Property to hold mBatteryIncluded.
	 */
	private String mBatteryIncluded;
	/**
	 * Property to hold mEwasteSurchargeSku.
	 */
	private String mEwasteSurchargeSku;

	/** Property to hold mSkuVideoGameSKU. */
	private String mSkuVideoGameSKU;
	/**
	 * Property to hold mBatteryRequiredPropertyName.
	 */
	private String mBatteryRequiredPropertyName;
	/**
	 * Property to hold mBatteryIncludedPropertyName.
	 */
	private String mBatteryIncludedPropertyName;
	/**
	 * Property to hold mEwasteSurchargeSkuPropertyName.
	 */
	private String mEwasteSurchargeSkuPropertyName;
	/**
	 * Property to hold mProductBrand.
	 */
	private String mProductBrand;

	/** Property to hold mBatteryType. */
	private String mBatteryType;

	/** Property to hold mBatteries. */
	private String mBatteries;

	/** Property to hold mBatteryQuantity. */
	private String mBatteryQuantity;

	/** Property to hold mItemDimensionMap. */
	private String mItemDimensionMap;

	/** Property to hold mAssemblyMap. */
	private String mAssemblyMap;

	/**
	 * Property to hold mPriceDisplayPropertyName.
	 */
	private String mPriceDisplayPropertyName;

	/**
	 * Property to hold mCanBeGiftWrapped.
	 */
	private String mCanBeGiftWrapped;

	/**
	 * Property to hold mVendorsProductDemoUrl.
	 */
	private String mVendorsProductDemoUrl;

	/**
	 * Property to hold mUnCartable.
	 */
	private String mUnCartable;

	/**
	 * Property to hold mPresellQuantityUnits.
	 */
	private String mPresellQuantityUnits;

	/**
	 * Property to hold mStreetDate.
	 */
	private String mStreetDate;

	/**
	 * Property to hold mChannelAvailability.
	 */
	private String mChannelAvailability;

	/**
	 * Property to hold mDropShipFlag.
	 */
	private String mDropShipFlag;

	/**
	 * Property to hold mFreightClass.
	 */
	private String mFreightClass;

	/**
	 * Property to hold mBppEligible.
	 */
	private String mBppEligible;

	/**
	 * Property to hold mBppName.
	 */
	private String mBppName;

	/**
	 * Property to hold mBppId.
	 */
	private String mBppId;
	/**
	 * Property to hold mLower48MapPropertyName.
	 */
	private String mLower48MapPropertyName;
	/**
	 * Property to hold mAkhiMapPropertyName.
	 */
	private String mAkhiMapPropertyName;

	/**
	 * Property to hold mWhyWeLoveIt.
	 */
	private String mWhyWeLoveIt;

	/**
	 * Property to hold mNotiFyMe.
	 */
	private String mNotiFyMe;

	/**
	 * Property to hold mOnlineCollectionName.
	 */
	private String mOnlineCollectionName;

	/**
	 * Property to hold mEwasteSurchargeStatePropertyName.
	 */
	private String mEwasteSurchargeStatePropertyName;

	/**
	 * Property to hold mRegularSKUItemDescPropertyName.
	 */
	private String mRegularSKUItemDescPropertyName;
	/**
	 * Property to hold mcreationDate.
	 */
	private String mCreationDate;
	/**
	 * Property to hold mStartDate.
	 */
	private String mStartDate;

	/**
	 * Property to hold mInVentoryDate.
	 */
	private String mInVentoryDate;

	/**
	 * Property to hold mS2s.
	 */
	private String mS2s;

	/**
	 * Property to hold mIspu.
	 */
	private String mIspu;

	/**
	 * Property to hold skuReviewRating.
	 */
	private String mSkuReviewRating;

	/**
	 * Property to hold mRegistryEligibility.
	 */
	private String mSkuRegistryEligibility;

	/**
	 * Property to hold mRegistryWarningIndicator.
	 */
	private String mRegistryWarningIndicator;

	/**
	 * Property to hold mNewProductThreshold.
	 */
	private String mNewProductThreshold;
	/**
	 * Property to hold mChildProductsPropertyName.
	 */
	private String mChildProductsPropertyName;

	/**
	 * Property to hold mHowToGetItMessages.
	 */
	private String mHowToGetItMessages;

	/**
	 * Property to hold mProductHTGITMsg.
	 */
	private String mProductHTGITMsg;

	/**
	 * Property to hold mStoreMessageKey.
	 */
	private String mStoreMessageKey;

	/**
	 * Property to hold mFreightClassKey.
	 */
	private String mFreightClassKey;

	/**
	 * Property to hold mStoreMessage.
	 */
	private String mStoreMessage;

	/**
	 * Property to hold mFreightMessage.
	 */
	private String mFreightMessage;
	/**
	 * Property to hold mShipInOrigContainer.
	 */
	private String mShipInOrigContainer;

	/**
	 * Property to hold mShipWindowMax.
	 */
	private String mShipWindowMax;

	/**
	 * Property to hold mShipWindowMin.
	 */
	private String mShipWindowMin;

	/**
	 * Property to hold collectionImage.
	 */
	private String mCollectionImage;

	/**
	 * Property to hold mQualifiedPromotionsPropertyName.
	 */
	private String mQualifiedPromotionsPropertyName;
	/**
	 * Property to hold mTargetedPromotionsPropertyName.
	 */
	private String mTargetedPromotionsPropertyName;
	/**
	 * Property to hold mTargetedPromotionsPropertyName.
	 */
	private String mCategoryPromotionsPropertyName;

	/**
	 * Property to hold mNmwaPercentage.
	 */
	private String mNmwaPercentage;
	/**
	 * Property to hold mGiftFinderStackListPropertyName.
	 */
	private String mGiftFinderStackListPropertyName;

	/** The m online pid. */
	private String mOnlinePID;

	/** The m back order status. */
	private String mBackOrderStatus;

	/** The m web display flag. */
	private String mWebDisplayFlag;

	/** The m supress in search. */
	private String mSupressInSearch;

	/** The m super display flag. */
	private String mSuperDisplayFlag;

	/** The m ship to store eeligible. */
	private String mShipToStoreEeligible;

	/** The m ship from store eligible lov. */
	private String mShipFromStoreEligibleLov;

	/** The m item in store pick up. */
	private String mItemInStorePickUp;

	/**
	 * Property to hold mSiteIDPropertyName.
	 */
	private String mSiteIDPropertyName;

	/**
	 * Property to hold mComputedCatalogPropertyName.
	 */
	private String mComputedCatalogPropertyName;
	
	/**
	 * Property to hold mComputedCatalogPropertyName.
	 */
	private String mContextCatalogPropertyName;

	/**
	 * Gets the context catalog property name.
	 *
	 * @return the context catalog property name
	 */
	public String getContextCatalogPropertyName() {
		return mContextCatalogPropertyName;
	}
	
	/**
	 * Sets the context catalog property name.
	 *
	 * @param pContextCatalogPropertyName the new context catalog property name
	 */
	public void setContextCatalogPropertyName(String pContextCatalogPropertyName) {
		this.mContextCatalogPropertyName = pContextCatalogPropertyName;
	}

	/**
	 * Property to hold mProductionURLPropertyName.
	 */
	private String mProductionURLPropertyName;
	/**
	 * Property to hold mChildSKUsPropertyName.
	 */
	private String mChildSKUsPropertyName;
	/**
	 * Property to hold mSlapperItemPropertyName.
	 */
	private String mSlapperItemPropertyName;

	/**
	 * Property to hold mInlineFile.
	 */
	private String mInlineFile;
	/**
	 * Property to hold mNonMerchSKUType.
	 */
	private String mNonMerchSKUType;

	/**
	 * Property to hold mInventoryFirstAvailableDate.
	 */
	private String mInventoryFirstAvailableDate;

	/**
	 * Property to hold mDefaultCatalog.
	 */
	private String mDefaultCatalog;

	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;

	/**
	 * Property to hold mLocationId.
	 */
	private String mLocationId;

	/**
	 * Property to hold mProductImageUrl.
	 */
	private String mProductImageUrl;

	/**
	 * Property to hold mThresholdSixMonthFinancing.
	 */
	private String mThresholdSixMonthFinancing;

	/**
	 * Property to hold mThresholdTwelveMonthFinancing.
	 */
	private String mThresholdTwelveMonthFinancing;

	/**
	 * Property to hold mLocationItemDescriptor.
	 */
	private String mLocationItemDescriptor;

	/**
	 * Property to hold mLayawayOrderid.
	 */
	private String mLayawayOrderid;
	/**
	 * The media promo key .
	 */
	private String mPromoKey;

	/**
	 * This property hold reference for promoHtmlContent.
	 */
	private String mPromoHtmlContent;

	/**
	 * This property hold reference for type.
	 */
	private String mTypePropertyName;

	/**
	 * This property hold reference for mRmsSizeCode.
	 */
	private String mRmsSizeCode;
	
	/**
	 * This property hold reference for mRmsColorCode.
	 */
	private String mRmsColorCode;
	
	/**
	 * This property hold reference for mOriginalParentProduct.
	 */
	private String mOriginalParentProduct;
	
	/**
	 * This property hold reference for mAttributeType.
	 */
	private String mAttributeType;
	
	/**
	 * This property hold reference for mAttributeProperty.
	 */
	private String mAttributeProperty;
	
	/**
	 * This property hold reference for mComparableAttributes.
	 */
	private String mComparableAttributes;
	
	/**
	 * This property hold reference for mAttributeNameDimension.
	 */
	private String mAttributeNameDimension;
	
	/**
	 * This property hold reference for mAttributeNameChildWeight.
	 */
	private String mAttributeNameChildWeight;
	/**
	 * This property hold reference for mChildWeightMin.
	 */
	private String mChildWeightMin;
	/**
	 * This property hold reference for mChildWeightMax.
	 */
	private String mChildWeightMax;
	/**
	 * This property hold reference for mSkuItemDescriptorPropertyName.
	 */
	private String mSkuItemDescriptorPropertyName;
	
	/** The m parent products. */
	private String mParentProducts;
	
	/** property to hold mRegistryClassificationItemName property. */
	private String mRegistryClassificationItemName;
	
	/** property to hold mCrossReferencePropertyName property. */
	private String mCrossReferencePropertyName;
	
	/**
	 * Gets the registry classification item name.
	 *
	 * @return the registryClassificationItemName
	 */
	public String getRegistryClassificationItemName() {
		return mRegistryClassificationItemName;
	}

	/**
	 * Sets the registry classification item name.
	 *
	 * @param pRegistryClassificationItemName the registryClassificationItemName to set
	 */
	public void setRegistryClassificationItemName(
			String pRegistryClassificationItemName) {
		mRegistryClassificationItemName = pRegistryClassificationItemName;
	}
	
	/**
	 * Gets the cross reference property name.
	 *
	 * @return the crossReferencePropertyName
	 */
	public String getCrossReferencePropertyName() {
		return mCrossReferencePropertyName;
	}

	/**
	 * Sets the cross reference property name.
	 *
	 * @param pCrossReferencePropertyName the crossReferencePropertyName to set
	 */
	public void setCrossReferencePropertyName(String pCrossReferencePropertyName) {
		mCrossReferencePropertyName = pCrossReferencePropertyName;
	}
	
	/**
	 * Gets the sku item descriptor property name.
	 *
	 * @return the skuItemDescriptorPropertyName
	 */
	public String getSkuItemDescriptorPropertyName() {
		return mSkuItemDescriptorPropertyName;
	}
	
	/**
	 * Sets the sku item descriptor property name.
	 *
	 * @param pSkuItemDescriptorPropertyName the skuItemDescriptorPropertyName to set
	 */
	public void setSkuItemDescriptorPropertyName(
			String pSkuItemDescriptorPropertyName) {
		mSkuItemDescriptorPropertyName = pSkuItemDescriptorPropertyName;
	}
	
	/**
	 * Gets the child weight min.
	 *
	 * @return the mChildWeightMin
	 */
	public String getChildWeightMin() {
		return mChildWeightMin;
	}
	
	/**
	 * Sets the child weight min.
	 *
	 * @param pChildWeightMin the ChildWeightMin to set
	 */
	public void setChildWeightMin(String pChildWeightMin) {
		mChildWeightMin = pChildWeightMin;
	}
	
	/**
	 * Gets the child weight max.
	 *
	 * @return the mChildWeightMax
	 */
	public String getChildWeightMax() {
		return mChildWeightMax;
	}
	
	/**
	 * Sets the child weight max.
	 *
	 * @param pChildWeightMax the childWeightMax to set
	 */
	public void setChildWeightMax(String pChildWeightMax) {
		mChildWeightMax = pChildWeightMax;
	}
	
	/**
	 * Gets the attribute name dimension.
	 *
	 * @return the mAttributeNameDimension
	 */
	public String getAttributeNameDimension() {
		return mAttributeNameDimension;
	}
	
	/**
	 * Sets the attribute name dimension.
	 *
	 * @param pAttributeNameDimension the attributeNameDimenssion to set
	 */
	public void setAttributeNameDimension(String pAttributeNameDimension) {
		mAttributeNameDimension = pAttributeNameDimension;
	}
	
	/**
	 * Gets the attribute name child weight.
	 *
	 * @return the mAttributeNameChildWeight
	 */
	public String getAttributeNameChildWeight() {
		return mAttributeNameChildWeight;
	}
	
	/**
	 * Sets the attribute name child weight.
	 *
	 * @param pAttributeNameChildWeight the attributeNameChildWeight to set
	 */
	public void setAttributeNameChildWeight(String pAttributeNameChildWeight) {
		mAttributeNameChildWeight = pAttributeNameChildWeight;
	}
	/**
	 * Gets the production url property name.
	 * 
	 * @return the mProductionURLPropertyName
	 */
	public String getProductionURLPropertyName() {
		return mProductionURLPropertyName;
	}

	/**
	 * Sets the production url property name.
	 * 
	 * @param pProductionURLPropertyName
	 *            the mProductionURLPropertyName to set
	 */
	public void setProductionURLPropertyName(String pProductionURLPropertyName) {
		mProductionURLPropertyName = pProductionURLPropertyName;
	}

	/**
	 * Gets the mSkuVideoGameSKU.
	 * 
	 * @return the mSkuVideoGameSKU
	 */
	public String getSkuVideoGameSKU() {
		return mSkuVideoGameSKU;
	}

	/**
	 * Sets the pSkuVideoGameSKU.
	 * 
	 * @param pSkuVideoGameSKU
	 *            the mSkuVideoGameSKU
	 */

	public void setSkuVideoGameSKU(String pSkuVideoGameSKU) {
		this.mSkuVideoGameSKU = pSkuVideoGameSKU;
	}

	/**
	 * Gets the mSkuVideoGameEsrb.
	 * 
	 * @return the mSkuVideoGameEsrb
	 */

	public String getSkuVideoGameEsrb() {
		return mSkuVideoGameEsrb;
	}

	/**
	 * Sets the pSkuVideoGameEsrb.
	 * 
	 * @param pSkuVideoGameEsrb
	 *            the mSkuVideoGameEsrb
	 */
	public void setSkuVideoGameEsrb(String pSkuVideoGameEsrb) {
		this.mSkuVideoGameEsrb = pSkuVideoGameEsrb;
	}

	/**
	 * Gets the mSkuPromotionalStickerDisplay.
	 * 
	 * @return the mSkuPromotionalStickerDisplay
	 */

	public String getSkuPromotionalStickerDisplay() {
		return mSkuPromotionalStickerDisplay;
	}

	/**
	 * Gets the registry warning indicator.
	 * 
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * Sets the registry warning indicator.
	 * 
	 * @param pRegistryWarningIndicator
	 *            the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}

	/**
	 * Sets the pSkuPromotionalStickerDisplay.
	 * 
	 * @param pSkuPromotionalStickerDisplay
	 *            the mSkuPromotionalStickerDisplay
	 */

	public void setSkuPromotionalStickerDisplay(
			String pSkuPromotionalStickerDisplay) {
		mSkuPromotionalStickerDisplay = pSkuPromotionalStickerDisplay;
	}

	/** Property to hold mCustomerPurchaseLimit. */
	private String mCustomerPurchaseLimit;

	/**
	 * Gets the mProductRusItemNumber.
	 * 
	 * @return the mProductRusItemNumber
	 */

	public String getProductRusItemNumber() {
		return mProductRusItemNumber;
	}

	/**
	 * Sets the pProductRusItemNumber.
	 * 
	 * @param pProductRusItemNumber
	 *            the mProductRusItemNumber
	 */

	public void setProductRusItemNumber(String pProductRusItemNumber) {
		this.mProductRusItemNumber = pProductRusItemNumber;
	}

	/**
	 * Gets the mProductSizeChartName.
	 * 
	 * @return the mProductSizeChartName
	 */

	public String getProductSizeChartName() {
		return mProductSizeChartName;
	}

	/**
	 * Sets the pProductSizeChartName.
	 * 
	 * @param pProductSizeChartName
	 *            the mProductSizeChartName
	 */

	public void setProductSizeChartName(String pProductSizeChartName) {
		this.mProductSizeChartName = pProductSizeChartName;
	}

	/**
	 * Gets the mProductManufacturerStyleNumber.
	 * 
	 * @return the mProductManufacturerStyleNumber
	 */

	public String getProductManufacturerStyleNumber() {
		return mProductManufacturerStyleNumber;
	}

	/**
	 * Sets the pProductManufacturerStyleNumber.
	 * 
	 * @param pProductManufacturerStyleNumber
	 *            the mProductManufacturerStyleNumber
	 */

	public void setProductManufacturerStyleNumber(
			String pProductManufacturerStyleNumber) {
		this.mProductManufacturerStyleNumber = pProductManufacturerStyleNumber;
	}

	/**
	 * Gets the mProductBrandNamePrimary.
	 * 
	 * @return the mProductBrandNamePrimary
	 */

	public String getProductBrandNamePrimary() {
		return mProductBrandNamePrimary;
	}

	/**
	 * Sets the pProductBrandNamePrimary.
	 * 
	 * @param pProductBrandNamePrimary
	 *            the mProductBrandNamePrimary
	 */

	public void setProductBrandNamePrimary(String pProductBrandNamePrimary) {
		this.mProductBrandNamePrimary = pProductBrandNamePrimary;
	}

	/**
	 * Gets the mProductLongDescription.
	 * 
	 * @return the mProductLongDescription
	 */

	public String getProductLongDescription() {
		return mProductLongDescription;
	}

	/**
	 * Sets the pProductLongDescription.
	 * 
	 * @param pProductLongDescription
	 *            the mProductLongDescription
	 */

	public void setProductLongDescription(String pProductLongDescription) {
		this.mProductLongDescription = pProductLongDescription;
	}

	/**
	 * Gets the mProductMfrSuggestedAgeMax.
	 * 
	 * @return the mProductMfrSuggestedAgeMax
	 */

	public String getProductMfrSuggestedAgeMax() {
		return mProductMfrSuggestedAgeMax;
	}

	/**
	 * Sets the pProductMfrSuggestedAgeMax.
	 * 
	 * @param pProductMfrSuggestedAgeMax
	 *            the mProductMfrSuggestedAgeMax
	 */

	public void setProductMfrSuggestedAgeMax(String pProductMfrSuggestedAgeMax) {
		this.mProductMfrSuggestedAgeMax = pProductMfrSuggestedAgeMax;
	}

	/**
	 * Gets the mProductMfrSuggestedAgeMin.
	 * 
	 * @return the mProductMfrSuggestedAgeMin
	 */

	public String getProductMfrSuggestedAgeMin() {
		return mProductMfrSuggestedAgeMin;
	}

	/**
	 * Gets the battery required property name.
	 * 
	 * @return the batteryRequiredPropertyName
	 */
	public String getBatteryRequiredPropertyName() {
		return mBatteryRequiredPropertyName;
	}

	/**
	 * Sets the battery required property name.
	 * 
	 * @param pBatteryRequiredPropertyName
	 *            the batteryRequiredPropertyName to set
	 */
	public void setBatteryRequiredPropertyName(String pBatteryRequiredPropertyName) {
		mBatteryRequiredPropertyName = pBatteryRequiredPropertyName;
	}

	/**
	 * Gets the battery included property name.
	 * 
	 * @return the batteryIncludedPropertyName
	 */
	public String getBatteryIncludedPropertyName() {
		return mBatteryIncludedPropertyName;
	}

	/**
	 * Sets the battery included property name.
	 * 
	 * @param pBatteryIncludedPropertyName
	 *            the batteryIncludedPropertyName to set
	 */
	public void setBatteryIncludedPropertyName(String pBatteryIncludedPropertyName) {
		mBatteryIncludedPropertyName = pBatteryIncludedPropertyName;
	}

	/**
	 * Gets the ewaste surcharge sku property name.
	 * 
	 * @return the ewasteSurchargeSkuPropertyName
	 */
	public String getEwasteSurchargeSkuPropertyName() {
		return mEwasteSurchargeSkuPropertyName;
	}

	/**
	 * Sets the ewaste surcharge sku property name.
	 * 
	 * @param pEwasteSurchargeSkuPropertyName
	 *            the ewasteSurchargeSkuPropertyName to set
	 */
	public void setEwasteSurchargeSkuPropertyName(
			String pEwasteSurchargeSkuPropertyName) {
		mEwasteSurchargeSkuPropertyName = pEwasteSurchargeSkuPropertyName;
	}

	/**
	 * Gets the product brand.
	 * 
	 * @return the productBrand
	 */
	public String getProductBrand() {
		return mProductBrand;
	}

	/**
	 * Sets the product brand.
	 * 
	 * @param pProductBrand
	 *            the productBrand to set
	 */
	public void setProductBrand(String pProductBrand) {
		mProductBrand = pProductBrand;
	}

	/**
	 * Sets the pProductMfrSuggestedAgeMin.
	 * 
	 * @param pProductMfrSuggestedAgeMin
	 *            the mProductMfrSuggestedAgeMin
	 */

	public void setProductMfrSuggestedAgeMin(String pProductMfrSuggestedAgeMin) {
		this.mProductMfrSuggestedAgeMin = pProductMfrSuggestedAgeMin;
	}

	/**
	 * Gets the mProductSafetyWarnings.
	 * 
	 * @return the mProductSafetyWarnings
	 */

	public String getProductSafetyWarnings() {
		return mProductSafetyWarnings;
	}

	/**
	 * Sets the pProductSafetyWarnings.
	 * 
	 * @param pProductSafetyWarnings
	 *            the mProductSafetyWarnings
	 */

	public void setProductSafetyWarnings(String pProductSafetyWarnings) {
		this.mProductSafetyWarnings = pProductSafetyWarnings;
	}

	/**
	 * Gets the related media content.
	 * 
	 * @return the related media content
	 */
	public String getRelatedMediaContent() {
		return mRelatedMediaContent;
	}

	/**
	 * Sets the related media content.
	 * 
	 * @param pRelatedMediaContent
	 *            the new related media content
	 */
	public void setRelatedMediaContent(String pRelatedMediaContent) {
		mRelatedMediaContent = pRelatedMediaContent;
	}

	/**
	 * Gets the brand.
	 * 
	 * @return the brand
	 */
	public String getBrand() {
		return mBrand;
	}

	/**
	 * Sets the brand.
	 * 
	 * @param pBrand
	 *            the new brand
	 */
	public void setBrand(String pBrand) {
		mBrand = pBrand;
	}

	/**
	 * Gets the battery required.
	 * 
	 * @return the battery required
	 */
	public String getBatteryRequired() {
		return mBatteryRequired;
	}

	/**
	 * Sets the battery required.
	 * 
	 * @param pBatteryRequired
	 *            the new battery required
	 */
	public void setBatteryRequired(String pBatteryRequired) {
		mBatteryRequired = pBatteryRequired;
	}

	/**
	 * Gets the battery included.
	 * 
	 * @return the battery included
	 */
	public String getBatteryIncluded() {
		return mBatteryIncluded;
	}

	/**
	 * Sets the battery included.
	 * 
	 * @param pBatteryIncluded
	 *            the new battery included
	 */
	public void setBatteryIncluded(String pBatteryIncluded) {
		mBatteryIncluded = pBatteryIncluded;
	}

	/**
	 * Gets the ewaste surcharge sku.
	 * 
	 * @return the ewaste surcharge sku
	 */
	public String getEwasteSurchargeSku() {
		return mEwasteSurchargeSku;
	}

	/**
	 * Sets the ewaste surcharge sku.
	 * 
	 * @param pEwasteSurchargeSku
	 *            the new ewaste surcharge sku
	 */
	public void setEwasteSurchargeSku(String pEwasteSurchargeSku) {
		mEwasteSurchargeSku = pEwasteSurchargeSku;
	}

	/**
	 * Gets the deletedPropertyName.
	 * 
	 * @return the mDeletedPropertyName
	 */
	public String getDeletedPropertyName() {
		return mDeletedPropertyName;
	}

	/**
	 * Sets the deletedPropertyName.
	 * 
	 * @param pDeletedPropertyName
	 *            the new deletedPropertyName
	 */
	public void setDeletedPropertyName(String pDeletedPropertyName) {
		mDeletedPropertyName = pDeletedPropertyName;
	}

	/**
	 * Gets the image url.
	 * 
	 * @return the image url
	 */
	public String getImageUrl() {
		return mImageUrl;
	}

	/**
	 * Sets the image url.
	 * 
	 * @param pImageUrl
	 *            the new image url
	 */
	public void setImageUrl(String pImageUrl) {
		mImageUrl = pImageUrl;
	}

	/**
	 * Gets the banner key.
	 * 
	 * @return mBannerKey the banner key
	 */
	public String getBannerKey() {
		return mBannerKey;
	}

	/**
	 * sets the banner key.
	 * 
	 * @param pBannerKey
	 *            the bannerKey to set
	 */
	public void setBannerKey(String pBannerKey) {
		mBannerKey = pBannerKey;
	}

	/**
	 * Gets the displayStatusPropertyName.
	 * 
	 * @return the displayStatusPropertyName
	 */
	public String getDisplayStatusPropertyName() {
		return mDisplayStatusPropertyName;
	}

	/**
	 * Sets the displayStatusPropertyName.
	 * 
	 * @param pDisplayStatusPropertyName
	 *            the displayStatusPropertyName to set
	 */
	public void setDisplayStatusPropertyName(String pDisplayStatusPropertyName) {
		mDisplayStatusPropertyName = pDisplayStatusPropertyName;
	}

	/**
	 * Gets the productDescriptionPropertyName.
	 * 
	 * @return the productDescriptionPropertyName
	 */
	public String getProductDescriptionPropertyName() {
		return mProductDescriptionPropertyName;
	}

	/**
	 * Sets the productDescriptionPropertyName.
	 * 
	 * @param pProductDescriptionPropertyName
	 *            the productDescriptionPropertyName to set
	 */
	public void setProductDescriptionPropertyName(String pProductDescriptionPropertyName) {
		mProductDescriptionPropertyName = pProductDescriptionPropertyName;
	}

	/**
	 * Gets the productDisplayNamePropertyName.
	 * 
	 * @return the productDisplayNamePropertyName
	 */
	public String getProductDisplayNamePropertyName() {
		return mProductDisplayNamePropertyName;
	}

	/**
	 * Sets the productDisplayNamePropertyName.
	 * 
	 * @param pProductDisplayNamePropertyName
	 *            the productDisplayNamePropertyName to set
	 */
	public void setProductDisplayNamePropertyName(String pProductDisplayNamePropertyName) {
		mProductDisplayNamePropertyName = pProductDisplayNamePropertyName;
	}

	/**
	 * Gets the ClassificationParentCategoryPropertyName.
	 * 
	 * @return the mClassificationParentCategoryPropertyName
	 */
	public String getClassificationParentCategoryPropertyName() {
		return mClassificationParentCategoryPropertyName;
	}

	/**
	 * Sets the ClassificationParentCategoryPropertyName.
	 * 
	 * @param pClassificationParentCategoryPropertyName
	 *            the ClassificationParentCategoryPropertyName to set
	 */
	public void setClassificationParentCategoryPropertyName(
			String pClassificationParentCategoryPropertyName) {
		this.mClassificationParentCategoryPropertyName = pClassificationParentCategoryPropertyName;
	}

	/**
	 * Gets the ChildClassificationsItemPropertyName.
	 * 
	 * @return the mChildClassificationsItemPropertyName
	 */
	public String getChildClassificationsItemPropertyName() {
		return mChildClassificationsItemPropertyName;
	}

	/**
	 * Sets the ChildClassificationsItemPropertyName.
	 * 
	 * @param pChildClassificationsItemPropertyName
	 *            the ChildClassificationsItemPropertyName to set
	 */
	public void setChildClassificationsItemPropertyName(
			String pChildClassificationsItemPropertyName) {
		mChildClassificationsItemPropertyName = pChildClassificationsItemPropertyName;
	}

	/**
	 * Gets the mParentClassificationsItemPropertyName.
	 * 
	 * @return the mParentClassificationsItemPropertyName
	 */
	public String getParentClassificationsItemPropertyName() {
		return mParentClassificationsItemPropertyName;
	}

	/**
	 * Sets the ParentClassificationsItemPropertyName.
	 * 
	 * @param pParentClassificationsItemPropertyName
	 *            the ParentClassificationsItemPropertyName to set
	 */
	public void setParentClassificationsItemPropertyName(
			String pParentClassificationsItemPropertyName) {
		mParentClassificationsItemPropertyName = pParentClassificationsItemPropertyName;
	}

	/**
	 * Gets the ClassificationItemDescPropertyName.
	 * 
	 * @return the mClassificationItemDescPropertyName
	 */
	public String getClassificationItemDescPropertyName() {
		return mClassificationItemDescPropertyName;
	}

	/**
	 * Sets the classificationItemDescPropertyName.
	 * 
	 * @param pClassificationItemDescPropertyName
	 *            the classificationItemDescPropertyName to set
	 */
	public void setClassificationItemDescPropertyName(
			String pClassificationItemDescPropertyName) {
		mClassificationItemDescPropertyName = pClassificationItemDescPropertyName;
	}

	/**
	 * Gets the collection product item desc property name.
	 * 
	 * @return the collectionProductItemDescPropertyName
	 */
	public String getCollectionProductItemDescPropertyName() {
		return mCollectionProductItemDescPropertyName;
	}

	/**
	 * Sets the collection product item desc property name.
	 * 
	 * @param pCollectionProductItemDescPropertyName
	 *            the collectionProductItemDescPropertyName to set
	 */
	public void setCollectionProductItemDescPropertyName(
			String pCollectionProductItemDescPropertyName) {
		mCollectionProductItemDescPropertyName = pCollectionProductItemDescPropertyName;
	}

	/**
	 * Gets the categoryDisplayNamePropertyName.
	 * 
	 * @return the mCategoryDisplayNamePropertyName
	 */
	public String getCategoryDisplayNamePropertyName() {
		return mCategoryDisplayNamePropertyName;
	}

	/**
	 * Sets the categoryDisplayNamePropertyName.
	 * 
	 * @param pCategoryDisplayNamePropertyName
	 *            the categoryDisplayNamePropertyName to set
	 */
	public void setCategoryDisplayNamePropertyName(String pCategoryDisplayNamePropertyName) {
		mCategoryDisplayNamePropertyName = pCategoryDisplayNamePropertyName;
	}

	/**
	 * Gets the sku color code.
	 * 
	 * @return the skuColorCode
	 */
	public String getSkuColorCode() {
		return mSkuColorCode;
	}

	/**
	 * Sets the sku color code.
	 * 
	 * @param pSkuColorCode
	 *            the skuColorCode to set
	 */
	public void setSkuColorCode(String pSkuColorCode) {
		mSkuColorCode = pSkuColorCode;
	}

	/**
	 * Gets the sku juvenile size.
	 * 
	 * @return the skuJuvenileSize
	 */
	public String getSkuJuvenileSize() {
		return mSkuJuvenileSize;
	}

	/**
	 * Sets the sku juvenile size.
	 * 
	 * @param pSkuJuvenileSize
	 *            the skuJuvenileSize to set
	 */
	public void setSkuJuvenileSize(String pSkuJuvenileSize) {
		mSkuJuvenileSize = pSkuJuvenileSize;
	}

	/**
	 * Gets the product feature.
	 * 
	 * @return the productFeature
	 */
	public String getProductFeature() {
		return mProductFeature;
	}

	/**
	 * Sets the product feature.
	 * 
	 * @param pProductFeature
	 *            the productFeature to set
	 */
	public void setProductFeature(String pProductFeature) {
		mProductFeature = pProductFeature;
	}

	/**
	 * Gets the upc numbers.
	 * 
	 * @return the upcNumbers
	 */
	public String getUpcNumbers() {
		return mUpcNumbers;
	}

	/**
	 * Sets the upc numbers.
	 * 
	 * @param pUpcNumbers
	 *            the upcNumbers to set
	 */
	public void setUpcNumbers(String pUpcNumbers) {
		mUpcNumbers = pUpcNumbers;
	}

	/**
	 * Gets the sku type.
	 * 
	 * @return the skuType
	 */
	public String getSkuType() {
		return mSkuType;
	}

	/**
	 * Sets the sku type.
	 * 
	 * @param pSkuType
	 *            the skuType to set
	 */
	public void setSkuType(String pSkuType) {
		mSkuType = pSkuType;
	}

	/**
	 * Gets the sku alternate images.
	 * 
	 * @return the skuAlternateImages
	 */
	public String getSkuAlternateImages() {
		return mSkuAlternateImages;
	}

	/**
	 * Sets the sku alternate images.
	 * 
	 * @param pSkuAlternateImages
	 *            the skuAlternateImages to set
	 */
	public void setSkuAlternateImages(String pSkuAlternateImages) {
		mSkuAlternateImages = pSkuAlternateImages;
	}

	/**
	 * Gets the sku primary image.
	 * 
	 * @return the skuPrimaryImage
	 */
	public String getSkuPrimaryImage() {
		return mSkuPrimaryImage;
	}

	/**
	 * Sets the sku primary image.
	 * 
	 * @param pSkuPrimaryImage
	 *            the skuPrimaryImage to set
	 */
	public void setSkuPrimaryImage(String pSkuPrimaryImage) {
		mSkuPrimaryImage = pSkuPrimaryImage;
	}

	/**
	 * Gets the sku secondary image.
	 * 
	 * @return the skuSecondaryImage
	 */
	public String getSkuSecondaryImage() {
		return mSkuSecondaryImage;
	}

	/**
	 * Sets the sku secondary image.
	 * 
	 * @param pSkuSecondaryImage
	 *            the skuSecondaryImage to set
	 */
	public void setSkuSecondaryImage(String pSkuSecondaryImage) {
		mSkuSecondaryImage = pSkuSecondaryImage;
	}

	/**
	 * Gets the sku swatch image.
	 * 
	 * @return the skuSwatchImage
	 */
	public String getSkuSwatchImage() {
		return mSkuSwatchImage;
	}

	/**
	 * Sets the sku swatch image.
	 * 
	 * @param pSkuSwatchImage
	 *            the skuSwatchImage to set
	 */
	public void setSkuSwatchImage(String pSkuSwatchImage) {
		mSkuSwatchImage = pSkuSwatchImage;
	}

	/**
	 * Gets the product style dimension.
	 * 
	 * @return the productStyleDimension
	 */
	public String getProductStyleDimension() {
		return mProductStyleDimension;
	}

	/**
	 * Sets the product style dimension.
	 * 
	 * @param pProductStyleDimension
	 *            the productStyleDimension to set
	 */
	public void setProductStyleDimension(String pProductStyleDimension) {
		mProductStyleDimension = pProductStyleDimension;
	}

	/**
	 * Gets the promoHtmlContent.
	 * 
	 * @return the mPromoHtmlContent
	 */
	public String getPromoHtmlContent() {
		return mPromoHtmlContent;
	}

	/**
	 * Sets the promoHtmlContent.
	 * 
	 * @param pPromoHtmlContent
	 *            the promoHtmlContent to set
	 */
	public void setPromoHtmlContent(String pPromoHtmlContent) {
		mPromoHtmlContent = pPromoHtmlContent;
	}

	/**
	 * Gets the promoKey.
	 * 
	 * @return the promoKey
	 */
	public String getPromoKey() {
		return mPromoKey;
	}

	/**
	 * Sets the promoKey.
	 * 
	 * @param pPromoKey
	 *            the promoKey to set
	 */
	public void setPromoKey(String pPromoKey) {
		mPromoKey = pPromoKey;
	}

	/**
	 * Gets the checks if is sku deleted.
	 * 
	 * @return the isSkuDeleted
	 */
	public String getIsSkuDeleted() {
		return mIsSkuDeleted;
	}

	/**
	 * Sets the checks if is sku deleted.
	 * 
	 * @param pIsSkuDeleted
	 *            the isSkuDeleted to set
	 */
	public void setIsSkuDeleted(String pIsSkuDeleted) {
		mIsSkuDeleted = pIsSkuDeleted;
	}

	/**
	 * Gets the customer purchase limit.
	 * 
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Sets the customer purchase limit.
	 * 
	 * @param pCustomerPurchaseLimit
	 *            the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * Gets the productAttributesPropertyName.
	 * 
	 * @return the productAttributesPropertyName
	 */
	public String getProductAttributesPropertyName() {
		return mProductAttributesPropertyName;
	}

	/**
	 * Sets the productAttributesPropertyName.
	 * 
	 * @param pProductAttributesPropertyName
	 *            the productAttributesPropertyName to set
	 */
	public void setProductAttributesPropertyName(String pProductAttributesPropertyName) {
		mProductAttributesPropertyName = pProductAttributesPropertyName;
	}

	/**
	 * Gets the attributeNamePropertyName.
	 * 
	 * @return the attributeNamePropertyName
	 */
	public String getAttributeNamePropertyName() {
		return mAttributeNamePropertyName;
	}

	/**
	 * Sets the attributeNamePropertyName.
	 * 
	 * @param pAttributeNamePropertyName
	 *            the attributeNamePropertyName to set
	 */
	public void setAttributeNamePropertyName(String pAttributeNamePropertyName) {
		mAttributeNamePropertyName = pAttributeNamePropertyName;
	}

	/**
	 * Gets the attributeValuePropertyName.
	 * 
	 * @return the attributeValuePropertyName
	 */
	public String getAttributeValuePropertyName() {
		return mAttributeValuePropertyName;
	}

	/**
	 * Sets the attributeValuePropertyName.
	 * 
	 * @param pAttributeValuePropertyName
	 *            the attributeValuePropertyName to set
	 */
	public void setAttributeValuePropertyName(String pAttributeValuePropertyName) {
		mAttributeValuePropertyName = pAttributeValuePropertyName;
	}

	/**
	 * Gets the attributeGroupPropertyName.
	 * 
	 * @return the attributeGroupPropertyName
	 */
	public String getAttributeGroupPropertyName() {
		return mAttributeGroupPropertyName;
	}

	/**
	 * Sets the attributeGroupPropertyName.
	 * 
	 * @param pAttributeGroupPropertyName
	 *            the attributeGroupPropertyName to set
	 */
	public void setAttributeGroupPropertyName(String pAttributeGroupPropertyName) {
		mAttributeGroupPropertyName = pAttributeGroupPropertyName;
	}

	/**
	 * Gets the attributeGroupIdPropertyName.
	 * 
	 * @return the attributeGroupIdPropertyName
	 */
	public String getAttributeGroupIdPropertyName() {
		return mAttributeGroupIdPropertyName;
	}

	/**
	 * Sets the attributeGroupIdPropertyName.
	 * 
	 * @param pAttributeGroupIdPropertyName
	 *            the attributeGroupIdPropertyName to set
	 */
	public void setAttributeGroupIdPropertyName(String pAttributeGroupIdPropertyName) {
		mAttributeGroupIdPropertyName = pAttributeGroupIdPropertyName;
	}

	/**
	 * Gets the battery type.
	 * 
	 * @return the batteryType
	 */
	public String getBatteryType() {
		return mBatteryType;
	}

	/**
	 * Sets the battery type.
	 * 
	 * @param pBatteryType
	 *            the batteryType to set
	 */
	public void setBatteryType(String pBatteryType) {
		mBatteryType = pBatteryType;
	}

	/**
	 * Gets the batteries.
	 * 
	 * @return the batteries
	 */
	public String getBatteries() {
		return mBatteries;
	}

	/**
	 * Sets the batteries.
	 * 
	 * @param pBatteries
	 *            the Batteries to set
	 */
	public void setBatteries(String pBatteries) {
		mBatteries = pBatteries;
	}

	/**
	 * Gets the battery quantity.
	 * 
	 * @return the batteryQuantity
	 */
	public String getBatteryQuantity() {
		return mBatteryQuantity;
	}

	/**
	 * Sets the battery quantity.
	 * 
	 * @param pBatteryQuantity
	 *            the batteryQuantity to set
	 */
	public void setBatteryQuantity(String pBatteryQuantity) {
		mBatteryQuantity = pBatteryQuantity;
	}

	/**
	 * Gets the item dimension map.
	 * 
	 * @return the itemDimensionMap
	 */
	public String getItemDimensionMap() {
		return mItemDimensionMap;
	}

	/**
	 * Sets the item dimension map.
	 * 
	 * @param pItemDimensionMap
	 *            the itemDimensionMap to set
	 */
	public void setItemDimensionMap(String pItemDimensionMap) {
		mItemDimensionMap = pItemDimensionMap;
	}

	/**
	 * Gets the assembly map.
	 * 
	 * @return the assemblyMap
	 */
	public String getAssemblyMap() {
		return mAssemblyMap;
	}

	/**
	 * Sets the assembly map.
	 * 
	 * @param pAssemblyMap
	 *            the assemblyMap to set
	 */
	public void setAssemblyMap(String pAssemblyMap) {
		mAssemblyMap = pAssemblyMap;
	}

	/**
	 * Returns the priceDisplayPropertyName.
	 * 
	 * @return the priceDisplayPropertyName
	 */
	public String getPriceDisplayPropertyName() {
		return mPriceDisplayPropertyName;
	}

	/**
	 * Sets/updates the priceDisplayPropertyName.
	 * 
	 * @param pPriceDisplayPropertyName
	 *            the priceDisplayPropertyName to set
	 */
	public void setPriceDisplayPropertyName(String pPriceDisplayPropertyName) {
		mPriceDisplayPropertyName = pPriceDisplayPropertyName;
	}

	/**
	 * Gets the can be gift wrapped.
	 * 
	 * @return the canBeGiftWrapped
	 */
	public String getCanBeGiftWrapped() {
		return mCanBeGiftWrapped;
	}

	/**
	 * Sets the can be gift wrapped.
	 * 
	 * @param pCanBeGiftWrapped
	 *            the canBeGiftWrapped to set
	 */
	public void setCanBeGiftWrapped(String pCanBeGiftWrapped) {
		mCanBeGiftWrapped = pCanBeGiftWrapped;
	}

	/**
	 * Gets the vendors product demo url.
	 * 
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * Sets the vendors product demo url.
	 * 
	 * @param pVendorsProductDemoUrl
	 *            the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}

	/**
	 * Gets the un cartable.
	 * 
	 * @return the unCartable
	 */
	public String getUnCartable() {
		return mUnCartable;
	}

	/**
	 * Sets the un cartable.
	 * 
	 * @param pUnCartable
	 *            the unCartable to set
	 */
	public void setUnCartable(String pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * Gets the presell quantity units.
	 * 
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}

	/**
	 * Sets the presell quantity units.
	 * 
	 * @param pPresellQuantityUnits
	 *            the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}

	/**
	 * Gets the street date.
	 * 
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 * 
	 * @param pStreetDate
	 *            the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * Gets the channel availability.
	 * 
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * Sets the channel availability.
	 * 
	 * @param pChannelAvailability
	 *            the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * Gets the drop ship flag.
	 * 
	 * @return the dropShipFlag
	 */
	public String getDropShipFlag() {
		return mDropShipFlag;
	}

	/**
	 * Sets the drop ship flag.
	 * 
	 * @param pDropShipFlag
	 *            the new drop ship flag
	 */
	public void setDropShipFlag(String pDropShipFlag) {
		mDropShipFlag = pDropShipFlag;
	}

	/**
	 * Gets the freight class.
	 * 
	 * @return the freightClass
	 */
	public String getFreightClass() {
		return mFreightClass;
	}

	/**
	 * Sets the freight class.
	 * 
	 * @param pFreightClass
	 *            the freightClass to set
	 */
	public void setFreightClass(String pFreightClass) {
		mFreightClass = pFreightClass;
	}

	/**
	 * Gets the bpp eligible.
	 * 
	 * @return the bppEligible
	 */
	public String getBppEligible() {
		return mBppEligible;
	}

	/**
	 * Sets the bpp eligible.
	 * 
	 * @param pBppEligible
	 *            the bppEligible to set
	 */
	public void setBppEligible(String pBppEligible) {
		mBppEligible = pBppEligible;
	}

	/**
	 * Gets the bpp name.
	 * 
	 * @return the bppName
	 */
	public String getBppName() {
		return mBppName;
	}

	/**
	 * Sets the bpp name.
	 * 
	 * @param pBppName
	 *            the bppName to set
	 */
	public void setBppName(String pBppName) {
		mBppName = pBppName;
	}

	/**
	 * Gets the bpp id.
	 * 
	 * @return the bppId
	 */
	public String getBppId() {
		return mBppId;
	}

	/**
	 * Sets the bpp id.
	 * 
	 * @param pBppId
	 *            the bppId to set
	 */
	public void setBppId(String pBppId) {
		mBppId = pBppId;
	}

	/**
	 * Gets the lower48 map property name.
	 * 
	 * @return the lower48MapPropertyName
	 */
	public String getLower48MapPropertyName() {
		return mLower48MapPropertyName;
	}

	/**
	 * Sets the lower48 map property name.
	 * 
	 * @param pLower48MapPropertyNamep
	 *            the lower48MapPropertyName to set
	 */
	public void setLower48MapPropertyName(String pLower48MapPropertyNamep) {
		mLower48MapPropertyName = pLower48MapPropertyNamep;
	}

	/**
	 * Gets the akhi map property name.
	 * 
	 * @return the akhiMapPropertyName
	 */
	public String getAkhiMapPropertyName() {
		return mAkhiMapPropertyName;
	}

	/**
	 * Sets the akhi map property name.
	 * 
	 * @param pAkhiMapPropertyNamep
	 *            the akhiMapPropertyName to set
	 */
	public void setAkhiMapPropertyName(String pAkhiMapPropertyNamep) {
		mAkhiMapPropertyName = pAkhiMapPropertyNamep;
	}

	/**
	 * Gets the why we love it.
	 * 
	 * @return the whyWeLoveIt
	 */
	public String getWhyWeLoveIt() {
		return mWhyWeLoveIt;
	}

	/**
	 * Sets the why we love it.
	 * 
	 * @param pWhyWeLoveIt
	 *            the whyWeLoveIt to set
	 */
	public void setWhyWeLoveIt(String pWhyWeLoveIt) {
		mWhyWeLoveIt = pWhyWeLoveIt;
	}

	/**
	 * Gets the noti fy me.
	 * 
	 * @return the notiFyMe
	 */
	public String getNotiFyMe() {
		return mNotiFyMe;
	}

	/**
	 * Sets the noti fy me.
	 * 
	 * @param pNotiFyMe
	 *            the notiFyMe to set
	 */
	public void setNotiFyMe(String pNotiFyMe) {
		mNotiFyMe = pNotiFyMe;
	}

	/**
	 * Gets the online collection name.
	 * 
	 * @return the onlineCollectionName
	 */
	public String getOnlineCollectionName() {
		return mOnlineCollectionName;
	}

	/**
	 * Sets the online collection name.
	 * 
	 * @param pOnlineCollectionName
	 *            the onlineCollectionName to set
	 */
	public void setOnlineCollectionName(String pOnlineCollectionName) {
		mOnlineCollectionName = pOnlineCollectionName;
	}

	/**
	 * Gets the ewaste surcharge state property name.
	 * 
	 * @return the ewasteSurchargeStatePropertyName
	 */
	public String getEwasteSurchargeStatePropertyName() {
		return mEwasteSurchargeStatePropertyName;
	}

	/**
	 * Sets the ewaste surcharge state property name.
	 * 
	 * @param pEwasteSurchargeStatePropertyName
	 *            the ewasteSurchargeStatePropertyName to set
	 */
	public void setEwasteSurchargeStatePropertyName(
			String pEwasteSurchargeStatePropertyName) {
		this.mEwasteSurchargeStatePropertyName = pEwasteSurchargeStatePropertyName;
	}

	/**
	 * Gets the regular sku item desc property name.
	 * 
	 * @return the regularSKUItemDescPropertyName
	 */
	public String getRegularSKUItemDescPropertyName() {
		return mRegularSKUItemDescPropertyName;
	}

	/**
	 * Sets the regular sku item desc property name.
	 * 
	 * @param pRegularSKUItemDescPropertyName
	 *            the regularSKUItemDescPropertyName to set
	 */
	public void setRegularSKUItemDescPropertyName(
			String pRegularSKUItemDescPropertyName) {
		mRegularSKUItemDescPropertyName = pRegularSKUItemDescPropertyName;
	}

	/**
	 * Gets the creation date.
	 * 
	 * @return the creation date
	 */
	public String getCreationDate() {
		return mCreationDate;
	}

	/**
	 * Sets the creation date.
	 * 
	 * @param pCreationDate
	 *            the new creation date
	 */
	public void setCreationDate(String pCreationDate) {
		this.mCreationDate = pCreationDate;
	}

	/**
	 * Gets the start date.
	 * 
	 * @return the start date
	 */
	public String getStartDate() {
		return mStartDate;
	}

	/**
	 * Sets the start date.
	 * 
	 * @param pStartDate
	 *            the new start date
	 */
	public void setStartDate(String pStartDate) {
		this.mStartDate = pStartDate;
	}

	/**
	 * Gets the in ventory date.
	 * 
	 * @return the inVentoryDate
	 */
	public String getInVentoryDate() {
		return mInVentoryDate;
	}

	/**
	 * Sets the in ventory date.
	 * 
	 * @param pInVentoryDate
	 *            the inVentoryDate to set
	 */
	public void setInVentoryDate(String pInVentoryDate) {
		mInVentoryDate = pInVentoryDate;
	}

	/**
	 * Gets the s2s.
	 * 
	 * @return the s2s
	 */
	public String getS2s() {
		return mS2s;
	}

	/**
	 * Sets the s2s.
	 * 
	 * @param pS2s
	 *            the s2s to set
	 */
	public void setS2s(String pS2s) {
		mS2s = pS2s;
	}

	/**
	 * Gets the ispu.
	 * 
	 * @return the ispu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * Sets the ispu.
	 * 
	 * @param pIspu
	 *            the ispu to set
	 */
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}

	/**
	 * Gets the review rating.
	 * 
	 * @return mSkuReviewRating
	 */
	public String getSkuReviewRating() {
		return mSkuReviewRating;
	}

	/**
	 * Sets the pSkuReviewRating.
	 * 
	 * @param pSkuReviewRating
	 *            to set
	 */
	public void setSkuReviewRating(String pSkuReviewRating) {
		mSkuReviewRating = pSkuReviewRating;
	}

	/**
	 * Gets the sku registry eligibility.
	 * 
	 * @return the mSkuRegistryEligibility
	 */
	public String getSkuRegistryEligibility() {
		return mSkuRegistryEligibility;
	}

	/**
	 * Sets the sku registry eligibility.
	 * 
	 * @param pSkuRegistryEligibility
	 *            to set
	 */
	public void setSkuRegistryEligibility(String pSkuRegistryEligibility) {
		this.mSkuRegistryEligibility = pSkuRegistryEligibility;
	}

	/**
	 * Gets the new product threshold.
	 * 
	 * @return the newProductThreshold
	 */
	public String getNewProductThreshold() {
		return mNewProductThreshold;
	}

	/**
	 * Sets the new product threshold.
	 * 
	 * @param pNewProductThreshold
	 *            the newProductThreshold to set
	 */
	public void setNewProductThreshold(String pNewProductThreshold) {
		mNewProductThreshold = pNewProductThreshold;
	}

	/**
	 * Gets the child products property name.
	 * 
	 * @return the childProductsPropertyName
	 */
	public String getChildProductsPropertyName() {
		return mChildProductsPropertyName;
	}

	/**
	 * Sets the child products property name.
	 * 
	 * @param pChildProductsPropertyName
	 *            the childProductsPropertyName to set
	 */
	public void setChildProductsPropertyName(String pChildProductsPropertyName) {
		mChildProductsPropertyName = pChildProductsPropertyName;
	}

	/**
	 * Gets the how to get it messages.
	 * 
	 * @return the howToGetItMessages
	 */
	public String getHowToGetItMessages() {
		return mHowToGetItMessages;
	}

	/**
	 * Sets the how to get it messages.
	 * 
	 * @param pHowToGetItMessages
	 *            the howToGetItMessages to set
	 */
	public void setHowToGetItMessages(String pHowToGetItMessages) {
		mHowToGetItMessages = pHowToGetItMessages;
	}

	/**
	 * Gets the product htgit msg.
	 * 
	 * @return the productHTGITMsg
	 */
	public String getProductHTGITMsg() {
		return mProductHTGITMsg;
	}

	/**
	 * Sets the product htgit msg.
	 * 
	 * @param pProductHTGITMsg
	 *            the productHTGITMsg to set
	 */
	public void setProductHTGITMsg(String pProductHTGITMsg) {
		mProductHTGITMsg = pProductHTGITMsg;
	}

	/**
	 * Gets the store message key.
	 * 
	 * @return the storeMessageKey
	 */
	public String getStoreMessageKey() {
		return mStoreMessageKey;
	}

	/**
	 * Sets the store message key.
	 * 
	 * @param pStoreMessageKey
	 *            the storeMessageKey to set
	 */
	public void setStoreMessageKey(String pStoreMessageKey) {
		mStoreMessageKey = pStoreMessageKey;
	}

	/**
	 * Gets the freight class key.
	 * 
	 * @return the freightClassKey
	 */
	public String getFreightClassKey() {
		return mFreightClassKey;
	}

	/**
	 * Sets the freight class key.
	 * 
	 * @param pFreightClassKey
	 *            the freightClassKey to set
	 */
	public void setFreightClassKey(String pFreightClassKey) {
		mFreightClassKey = pFreightClassKey;
	}

	/**
	 * Gets the store message.
	 * 
	 * @return the storeMessage
	 */
	public String getStoreMessage() {
		return mStoreMessage;
	}

	/**
	 * Sets the store message.
	 * 
	 * @param pStoreMessage
	 *            the storeMessage to set
	 */
	public void setStoreMessage(String pStoreMessage) {
		mStoreMessage = pStoreMessage;
	}

	/**
	 * Gets the freight message.
	 * 
	 * @return the freightMessage
	 */
	public String getFreightMessage() {
		return mFreightMessage;
	}

	/**
	 * Sets the freight message.
	 * 
	 * @param pFreightMessage
	 *            the freightMessage to set
	 */
	public void setFreightMessage(String pFreightMessage) {
		mFreightMessage = pFreightMessage;
	}

	/**
	 * Gets the ship in orig container.
	 * 
	 * @return the ShipInOrigContainer
	 */
	public String getShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * Sets the ship in orig container.
	 * 
	 * @param pShipInOrigContainer
	 *            the ShipInOrigContainer to set
	 */
	public void setShipInOrigContainer(String pShipInOrigContainer) {
		this.mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * Gets the ship window max.
	 * 
	 * @return the shipWindowMax
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * Sets the ship window max.
	 * 
	 * @param pShipWindowMax
	 *            the shipWindowMax to set
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * Gets the ship window min.
	 * 
	 * @return the shipWindowMin
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * Sets the ship window min.
	 * 
	 * @param pShipWindowMin
	 *            the shipWindowMin to set
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * Gets the collection image.
	 * 
	 * @return the String
	 */
	public String getCollectionImage() {
		return mCollectionImage;
	}

	/**
	 * Sets the collection image.
	 * 
	 * @param pCollectionImage
	 *            the String to set
	 */
	public void setCollectionImage(String pCollectionImage) {
		mCollectionImage = pCollectionImage;
	}

	/**
	 * Gets the QualifiedPromotionsPropertyName property name.
	 * 
	 * @return the QualifiedPromotionsPropertyName
	 */
	public String getQualifiedPromotionsPropertyName() {
		return mQualifiedPromotionsPropertyName;
	}

	/**
	 * Sets the qualified promotions property name.
	 * 
	 * @param pQualifiedPromotionsPropertyName
	 *            the String to set mQualifiedPromotionsPropertyName
	 */
	public void setQualifiedPromotionsPropertyName(
			String pQualifiedPromotionsPropertyName) {
		mQualifiedPromotionsPropertyName = pQualifiedPromotionsPropertyName;
	}

	/**
	 * Gets the TargetedPromotionsPropertyName property name.
	 * 
	 * @return the TargetedPromotionsPropertyName
	 */
	public String getTargetedPromotionsPropertyName() {
		return mTargetedPromotionsPropertyName;
	}

	/**
	 * Sets the targeted promotions property name.
	 * 
	 * @param pTargetedPromotionsPropertyName
	 *            the String to set mTargetedPromotionsPropertyName
	 */
	public void setTargetedPromotionsPropertyName(
			String pTargetedPromotionsPropertyName) {
		mTargetedPromotionsPropertyName = pTargetedPromotionsPropertyName;
	}

	/**
	 * Gets the CategoryPromotionsPropertyName property name.
	 * 
	 * @return the CategoryPromotionsPropertyName
	 */
	public String getCategoryPromotionsPropertyName() {
		return mCategoryPromotionsPropertyName;
	}

	/**
	 * Sets the category promotions property name.
	 * 
	 * @param pCategoryPromotionsPropertyName
	 *            the String to set mCategoryPromotionsPropertyName
	 */
	public void setCategoryPromotionsPropertyName(
			String pCategoryPromotionsPropertyName) {
		mCategoryPromotionsPropertyName = pCategoryPromotionsPropertyName;
	}

	/**
	 * Gets the nmwa percentage.
	 * 
	 * @return the nmwaPercentage
	 */
	public String getNmwaPercentage() {
		return mNmwaPercentage;
	}

	/**
	 * Sets the nmwa percentage.
	 * 
	 * @param pNmwaPercentage
	 *            the nmwaPercentage to set
	 */
	public void setNmwaPercentage(String pNmwaPercentage) {
		mNmwaPercentage = pNmwaPercentage;
	}

	/**
	 * Gets the type property name.
	 * 
	 * @return the typePropertyName
	 */
	public String getTypePropertyName() {
		return mTypePropertyName;
	}

	/**
	 * Sets the type property name.
	 * 
	 * @param pTypePropertyName
	 *            the typePropertyName to set
	 */
	public void setTypePropertyName(String pTypePropertyName) {
		mTypePropertyName = pTypePropertyName;
	}

	/**
	 * Gets the product promotions.
	 * 
	 * @return the productPromotions
	 */
	public String getProductPromotions() {
		return mProductPromotions;
	}

	/**
	 * Sets the product promotions.
	 * 
	 * @param pProductPromotions
	 *            the productPromotions to set
	 */
	public void setProductPromotions(String pProductPromotions) {
		mProductPromotions = pProductPromotions;
	}

	/**
	 * Sets the ProductQualifiedForPromotions.
	 * 
	 * @param pProductQualifiedForPromotions
	 *            the ProductQualifiedForPromotions to set
	 */
	public void setProductQualifiedForPromotions(String pProductQualifiedForPromotions) {
		mProductQualifiedForPromotions = pProductQualifiedForPromotions;
	}

	/**
	 * Gets the ProductQualifiedForPromotions.
	 * 
	 * @return the ProductQualifiedForPromotions
	 */
	public String getProductQualifiedForPromotions() {
		return mProductQualifiedForPromotions;
	}

	/**
	 * Gets the promotion details.
	 * 
	 * @return the promotionDetails
	 */
	public String getPromotionDetails() {
		return mPromotionDetails;
	}

	/**
	 * Sets the promotion details.
	 * 
	 * @param pPromotionDetails
	 *            the promotionDetails to set
	 */
	public void setPromotionDetails(String pPromotionDetails) {
		mPromotionDetails = pPromotionDetails;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * Sets the description.
	 * 
	 * @param pDescription
	 *            the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}

	/**
	 * Gets the gift finder stack list property name.
	 * 
	 * @return the mGiftFinderStackListPropertyName
	 */
	public String getGiftFinderStackListPropertyName() {
		return mGiftFinderStackListPropertyName;
	}

	/**
	 * Sets the gift finder stack list property name.
	 * 
	 * @param pGiftFinderStackListPropertyName
	 *            the mGiftFinderStackListPropertyName to set
	 */
	public void setGiftFinderStackListPropertyName(
			String pGiftFinderStackListPropertyName) {
		this.mGiftFinderStackListPropertyName = pGiftFinderStackListPropertyName;
	}

	/**
	 * Gets the online pid.
	 * 
	 * @return the online pid
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * Sets the online pid.
	 * 
	 * @param pOnlinePID
	 *            the new online pid
	 */
	public void setOnlinePID(String pOnlinePID) {
		this.mOnlinePID = pOnlinePID;
	}

	/**
	 * Gets the back order status.
	 * 
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		this.mBackOrderStatus = pBackOrderStatus;
	}

	/**
	 * Gets the web display flag.
	 * 
	 * @return the web display flag
	 */
	public String getWebDisplayFlag() {
		return mWebDisplayFlag;
	}

	/**
	 * Sets the web display flag.
	 * 
	 * @param pWebDisplayFlag
	 *            the new web display flag
	 */
	public void setWebDisplayFlag(String pWebDisplayFlag) {
		this.mWebDisplayFlag = pWebDisplayFlag;
	}

	/**
	 * Gets the supress in search.
	 * 
	 * @return the supress in search
	 */
	public String getSupressInSearch() {
		return mSupressInSearch;
	}

	/**
	 * Sets the supress in search.
	 * 
	 * @param pSupressInSearch
	 *            the new supress in search
	 */
	public void setSupressInSearch(String pSupressInSearch) {
		this.mSupressInSearch = pSupressInSearch;
	}

	/**
	 * Gets the super display flag.
	 * 
	 * @return the super display flag
	 */
	public String getSuperDisplayFlag() {
		return mSuperDisplayFlag;
	}

	/**
	 * Sets the super display flag.
	 * 
	 * @param pSuperDisplayFlag
	 *            the new super display flag
	 */
	public void setSuperDisplayFlag(String pSuperDisplayFlag) {
		this.mSuperDisplayFlag = pSuperDisplayFlag;
	}

	/**
	 * Gets the ship to store eeligible.
	 * 
	 * @return the ship to store eeligible
	 */
	public String getShipToStoreEeligible() {
		return mShipToStoreEeligible;
	}

	/**
	 * Sets the ship to store eeligible.
	 * 
	 * @param pShipToStoreEeligible
	 *            the new ship to store eeligible
	 */
	public void setShipToStoreEeligible(String pShipToStoreEeligible) {
		this.mShipToStoreEeligible = pShipToStoreEeligible;
	}

	/**
	 * Gets the ship from store eligible lov.
	 * 
	 * @return the ship from store eligible lov
	 */
	public String getShipFromStoreEligibleLov() {
		return mShipFromStoreEligibleLov;
	}

	/**
	 * Sets the ship from store eligible lov.
	 * 
	 * @param pShipFromStoreEligibleLov
	 *            the new ship from store eligible lov
	 */
	public void setShipFromStoreEligibleLov(String pShipFromStoreEligibleLov) {
		this.mShipFromStoreEligibleLov = pShipFromStoreEligibleLov;
	}

	/**
	 * Gets the item in store pick up.
	 * 
	 * @return the item in store pick up
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 * 
	 * @param pItemInStorePickUp
	 *            the new item in store pick up
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		this.mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * Gets the site id property name.
	 * 
	 * @return the mSiteIDPropertyName
	 */
	public String getSiteIDPropertyName() {
		return mSiteIDPropertyName;
	}

	/**
	 * Sets the site id property name.
	 * 
	 * @param pSiteIDPropertyName
	 *            the mSiteIDPropertyName to set
	 */
	public void setSiteIDPropertyName(String pSiteIDPropertyName) {
		this.mSiteIDPropertyName = pSiteIDPropertyName;
	}

	/**
	 * Gets the computed catalog property name.
	 * 
	 * @return the mComputedCatalogPropertyName
	 */
	public String getComputedCatalogPropertyName() {
		return mComputedCatalogPropertyName;
	}

	/**
	 * Sets the computed catalog property name.
	 * 
	 * @param pComputedCatalogPropertyName
	 *            the mComputedCatalogPropertyName to set
	 */
	public void setComputedCatalogPropertyName(String pComputedCatalogPropertyName) {
		this.mComputedCatalogPropertyName = pComputedCatalogPropertyName;
	}

	/**
	 * Gets the child sk us property name.
	 * 
	 * @return the mChildSKUsPropertyName
	 */
	public String getChildSKUsPropertyName() {
		return mChildSKUsPropertyName;
	}

	/**
	 * Sets the child sk us property name.
	 * 
	 * @param pChildSKUsPropertyName
	 *            the mChildSKUsPropertyName to set
	 */
	public void setChildSKUsPropertyName(String pChildSKUsPropertyName) {
		mChildSKUsPropertyName = pChildSKUsPropertyName;
	}

	/**
	 * Gets the inline file.
	 * 
	 * @return the inlineFile
	 */
	public String getInlineFile() {
		return mInlineFile;
	}

	/**
	 * Sets the inline file.
	 * 
	 * @param pInlineFile
	 *            the inlineFile to set
	 */
	public void setInlineFile(String pInlineFile) {
		mInlineFile = pInlineFile;
	}

	/**
	 * Gets the email address property name.
	 * 
	 * @return the emailAddressPropertyName
	 */
	public String getEmailAddressPropertyName() {
		return mEmailAddressPropertyName;
	}

	/**
	 * Sets the email address property name.
	 * 
	 * @param pEmailAddressPropertyName
	 *            the emailAddressPropertyName to set
	 */
	public void setEmailAddressPropertyName(String pEmailAddressPropertyName) {
		mEmailAddressPropertyName = pEmailAddressPropertyName;
	}

	/**
	 * Gets the product id property name.
	 * 
	 * @return the productIdPropertyName
	 */
	public String getProductIdPropertyName() {
		return mProductIdPropertyName;
	}

	/**
	 * Sets the product id property name.
	 * 
	 * @param pProductIdPropertyName
	 *            the productIdPropertyName to set
	 */
	public void setProductIdPropertyName(String pProductIdPropertyName) {
		mProductIdPropertyName = pProductIdPropertyName;
	}

	/**
	 * Gets the sku id property name.
	 * 
	 * @return the skuIdPropertyName
	 */
	public String getSkuIdPropertyName() {
		return mSkuIdPropertyName;
	}

	/**
	 * Sets the sku id property name.
	 * 
	 * @param pSkuIdPropertyName
	 *            the skuIdPropertyName to set
	 */
	public void setSkuIdPropertyName(String pSkuIdPropertyName) {
		mSkuIdPropertyName = pSkuIdPropertyName;
	}

	/**
	 * Gets the nMWA req date property name.
	 * 
	 * @return the nMWAReqDatePropertyName
	 */
	public String getNMWAReqDatePropertyName() {
		return mNMWAReqDatePropertyName;
	}

	/**
	 * Sets the nMWA req date property name.
	 * 
	 * @param pNMWAReqDatePropertyName
	 *            the nMWAReqDatePropertyName to set
	 */
	public void setNMWAReqDatePropertyName(String pNMWAReqDatePropertyName) {
		mNMWAReqDatePropertyName = pNMWAReqDatePropertyName;
	}

	/**
	 * Gets the nmwa alert property name.
	 * 
	 * @return the nmwaAlertPropertyName
	 */
	public String getNmwaAlertPropertyName() {
		return mNmwaAlertPropertyName;
	}

	/**
	 * Sets the nmwa alert property name.
	 * 
	 * @param pNmwaAlertPropertyName
	 *            the nmwaAlertPropertyName to set
	 */
	public void setNmwaAlertPropertyName(String pNmwaAlertPropertyName) {
		mNmwaAlertPropertyName = pNmwaAlertPropertyName;
	}

	/**
	 * Gets the slapper item property name.
	 * 
	 * @return the slapperItemPropertyName
	 */
	public String getSlapperItemPropertyName() {
		return mSlapperItemPropertyName;
	}

	/**
	 * Sets the slapper item property name.
	 * 
	 * @param pSlapperItemPropertyName
	 *            the slapperItemPropertyName to set
	 */
	public void setSlapperItemPropertyName(String pSlapperItemPropertyName) {
		mSlapperItemPropertyName = pSlapperItemPropertyName;
	}

	/**
	 * Gets the non merch sku type.
	 * 
	 * @return the nonMerchSKUType
	 */
	public String getNonMerchSKUType() {
		return mNonMerchSKUType;
	}

	/**
	 * Sets the non merch sku type.
	 * 
	 * @param pNonMerchSKUType
	 *            the nonMerchSKUType to set
	 */
	public void setNonMerchSKUType(String pNonMerchSKUType) {
		mNonMerchSKUType = pNonMerchSKUType;
	}

	/**
	 * Gets the inventory first available date.
	 * 
	 * @return the String
	 */
	public String getInventoryFirstAvailableDate() {
		return mInventoryFirstAvailableDate;
	}

	/**
	 * Sets the inventory first available date.
	 * 
	 * @param pInventoryFirstAvailableDate
	 *            the String to set
	 */
	public void setInventoryFirstAvailableDate(String pInventoryFirstAvailableDate) {
		mInventoryFirstAvailableDate = pInventoryFirstAvailableDate;
	}

	/**
	 * Gets the color name.
	 * 
	 * @return the colorName
	 */
	public String getColorName() {
		return mColorName;
	}

	/**
	 * Sets the color name.
	 * 
	 * @param pColorName
	 *            the colorName to set
	 */
	public void setColorName(String pColorName) {
		mColorName = pColorName;
	}

	/**
	 * Gets the color display sequence.
	 * 
	 * @return the colorDisplaySequence
	 */
	public String getColorDisplaySequence() {
		return mColorDisplaySequence;
	}

	/**
	 * Sets the color display sequence.
	 * 
	 * @param pColorDisplaySequence
	 *            the colorDisplaySequence to set
	 */
	public void setColorDisplaySequence(String pColorDisplaySequence) {
		mColorDisplaySequence = pColorDisplaySequence;
	}

	/**
	 * Gets the size description.
	 * 
	 * @return the sizeDescription
	 */
	public String getSizeDescription() {
		return mSizeDescription;
	}

	/**
	 * Sets the size description.
	 * 
	 * @param pSizeDescription
	 *            the sizeDescription to set
	 */
	public void setSizeDescription(String pSizeDescription) {
		mSizeDescription = pSizeDescription;
	}

	/**
	 * Gets the size display sequence.
	 * 
	 * @return the sizeDisplaySequence
	 */
	public String getSizeDisplaySequence() {
		return mSizeDisplaySequence;
	}

	/**
	 * Sets the size display sequence.
	 * 
	 * @param pSizeDisplaySequence
	 *            the sizeDisplaySequence to set
	 */
	public void setSizeDisplaySequence(String pSizeDisplaySequence) {
		mSizeDisplaySequence = pSizeDisplaySequence;
	}

	/**
	 * Gets the default catalog.
	 * 
	 * @return the mDefaultCatalog
	 */
	public String getDefaultCatalog() {
		return mDefaultCatalog;
	}

	/**
	 * Sets the default catalog.
	 * 
	 * @param pDefaultCatalog
	 *            the new default catalog
	 */
	public void setDefaultCatalog(String pDefaultCatalog) {
		this.mDefaultCatalog = pDefaultCatalog;
	}

	/**
	 * Gets the site id.
	 * 
	 * @return the mSiteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the site id.
	 * 
	 * @param pSiteId
	 *            the new site id
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}

	/**
	 * Gets the threshold six month financing.
	 * 
	 * @return the mThresholdSixMonthFinancing
	 */
	public String getThresholdSixMonthFinancing() {
		return mThresholdSixMonthFinancing;
	}

	/**
	 * Sets the threshold six month financing.
	 * 
	 * @param pThresholdSixMonthFinancing
	 *            the new threshold six month financing
	 */
	public void setThresholdSixMonthFinancing(String pThresholdSixMonthFinancing) {
		this.mThresholdSixMonthFinancing = pThresholdSixMonthFinancing;
	}

	/**
	 * Gets the threshold twelve month financing.
	 * 
	 * @return the mThresholdTwelveMonthFinancing
	 */
	public String getThresholdTwelveMonthFinancing() {
		return mThresholdTwelveMonthFinancing;
	}

	/**
	 * Sets the threshold twelve month financing.
	 * 
	 * @param pThresholdTwelveMonthFinancing
	 *            the new threshold twelve month financing
	 */
	public void setThresholdTwelveMonthFinancing(String pThresholdTwelveMonthFinancing) {
		this.mThresholdTwelveMonthFinancing = pThresholdTwelveMonthFinancing;
	}

	/**
	 * Gets the layaway orderid.
	 * 
	 * @return the mLayawayOrderid
	 */
	public String getLayawayOrderid() {
		return mLayawayOrderid;
	}

	/**
	 * Sets the layaway orderid.
	 * 
	 * @param pLayawayOrderid
	 *            the new layaway orderid
	 */
	public void setLayawayOrderid(String pLayawayOrderid) {
		this.mLayawayOrderid = pLayawayOrderid;
	}

	/**
	 * Gets the location id.
	 * 
	 * @return the mLocationId
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * Sets the location id.
	 * 
	 * @param pLocationId
	 *            the new location id
	 */
	public void setLocationId(String pLocationId) {
		this.mLocationId = pLocationId;
	}

	/**
	 * Gets the product image url.
	 * 
	 * @return the mProductImageUrl
	 */
	public String getProductImageUrl() {
		return mProductImageUrl;
	}

	/**
	 * Sets the product image url.
	 * 
	 * @param pProductImageUrl
	 *            the new product image url
	 */
	public void setProductImageUrl(String pProductImageUrl) {
		this.mProductImageUrl = pProductImageUrl;
	}

	/**
	 * Gets the location item descriptor.
	 * 
	 * @return the mLocationItemDescriptor
	 */
	public String getLocationItemDescriptor() {
		return mLocationItemDescriptor;
	}

	/**
	 * Sets the location item descriptor.
	 * 
	 * @param pLocationItemDescriptor
	 *            the new location item descriptor
	 */
	public void setLocationItemDescriptor(String pLocationItemDescriptor) {
		this.mLocationItemDescriptor = pLocationItemDescriptor;
	}

	/**
	 * Gets the additional production ur ls.
	 * 
	 * @return the mAdditionalProductionURLs
	 */
	public String getAdditionalProductionURLs() {
		return mAdditionalProductionURLs;
	}

	/**
	 * Sets the additional production ur ls.
	 * 
	 * @param pAdditionalProductionURLs
	 *            the new additional production ur ls
	 */
	public void setAdditionalProductionURLs(String pAdditionalProductionURLs) {
		this.mAdditionalProductionURLs = pAdditionalProductionURLs;
	}
	
	/**
	 * Gets the sku qualified for promos.
	 *
	 * @return the skuQualifiedForPromos
	 */
	public String getSkuQualifiedForPromos() {
		return mSkuQualifiedForPromos;
	}

	/**
	 * Sets the sku qualified for promos.
	 *
	 * @param pSkuQualifiedForPromos the skuQualifiedForPromos to set
	 */
	public void setSkuQualifiedForPromos(String pSkuQualifiedForPromos) {
		mSkuQualifiedForPromos = pSkuQualifiedForPromos;
	}
	
	/**
	 * Gets the additional production ur ls.
	 * 
	 * @return the mSiteIds
	 */
	public String getSiteIds() {
		return mSiteIds;
	}

	/**
	 * Sets the site ids.
	 *
	 * @param pSiteIds the mSiteIds to set.
	 */
	public void setSiteIds(String pSiteIds) {
		this.mSiteIds = pSiteIds;
	}

	/**
	 * Gets the cross sell sku id.
	 *
	 * @return the mCrossSellSkuId.
	 */
	public String getCrossSellSkuId() {
		return mCrossSellSkuId;
	}

	/**
	 * Sets the cross sell sku id.
	 *
	 * @param pCrossSellSkuId the mCrossSellSkuId to set
	 */
	public void setCrossSellSkuId(String pCrossSellSkuId) {
		mCrossSellSkuId = pCrossSellSkuId;
	}
	
	
	/**
	 * Gets the uid battery cross sell.
	 *
	 * @return the mUidBatteryCrossSell.
	 */
	public String getUidBatteryCrossSell() {
		return mUidBatteryCrossSell;
	}

	/**
	 * Sets the uid battery cross sell.
	 *
	 * @param pUidBatteryCrossSell the mUidBatteryCrossSell to set
	 */
	public void setUidBatteryCrossSell(String pUidBatteryCrossSell) {
		mUidBatteryCrossSell = pUidBatteryCrossSell;
	}
	
	/**
	 * Gets the cross sells batteries.
	 *
	 * @return the mCrossSellsBatteries.
	 */
	public String getCrossSellsBatteries() {
		return mCrossSellsBatteries;
	}

	/**
	 * Sets the cross sells batteries.
	 *
	 * @param pCrossSellsBatteries the mCrossSellsBatteries to set
	 */
	public void setCrossSellsBatteries(String pCrossSellsBatteries) {
		mCrossSellsBatteries = pCrossSellsBatteries;
	}
	
	/**
	 * Gets the crosssell batteries.
	 *
	 * @return the mCrosssellBatteries.
	 */
	public String getCrosssellBatteries() {
		return mCrosssellBatteries;
	}

	/**
	 * Sets the crosssell batteries.
	 *
	 * @param pCrosssellBatteries the mCrosssellBatteries to set
	 */
	public void setCrosssellBatteries(String pCrosssellBatteries) {
		mCrosssellBatteries = pCrosssellBatteries;
	}
	
	/**
	 * Gets the rms size code.
	 *
	 * @return the rmsSizeCode
	 */
	public String getRmsSizeCode() {
		return mRmsSizeCode;
	}
	
	/**
	 * Sets the rms size code.
	 *
	 * @param pRmsSizeCode the rmsSizeCode to set
	 */
	public void setRmsSizeCode(String pRmsSizeCode) {
		mRmsSizeCode = pRmsSizeCode;
	}
	
	/**
	 * Gets the rms color code.
	 *
	 * @return the rmsColorCode
	 */
	public String getRmsColorCode() {
		return mRmsColorCode;
	}
	
	/**
	 * Sets the rms color code.
	 *
	 * @param pRmsColorCode the rmsColorCode to set
	 */
	public void setRmsColorCode(String pRmsColorCode) {
		mRmsColorCode = pRmsColorCode;
	}

	/**
	 * Gets the original parent product.
	 *
	 * @return the originalParentProduct
	 */
	public String getOriginalParentProduct() {
		return mOriginalParentProduct;
	}
	
	/**
	 * Sets the original parent product.
	 *
	 * @param pOriginalParentProduct the originalParentProduct to set
	 */
	public void setOriginalParentProduct(String pOriginalParentProduct) {
		mOriginalParentProduct = pOriginalParentProduct;
	}
	
	/**
	 * Gets the attribute type.
	 *
	 * @return the attributeType
	 */
	public String getAttributeType() {
		return mAttributeType;
	}
	
	/**
	 * Sets the attribute type.
	 *
	 * @param pAttributeType the attributeType to set
	 */
	public void setAttributeType(String pAttributeType) {
		mAttributeType = pAttributeType;
	}
	
	/**
	 * Gets the attribute property.
	 *
	 * @return the attributeProperty
	 */
	public String getAttributeProperty() {
		return mAttributeProperty;
	}
	
	/**
	 * Sets the attribute property.
	 *
	 * @param pAttributeProperty the attributeProperty to set
	 */
	public void setAttributeProperty(String pAttributeProperty) {
		mAttributeProperty = pAttributeProperty;
	}
	
	/**
	 * Gets the comparable attributes.
	 *
	 * @return the comparableAttributes
	 */
	public String getComparableAttributes() {
		return mComparableAttributes;
	}
	
	/**
	 * Sets the comparable attributes.
	 *
	 * @param pComparableAttributes the comparableAttributes to set
	 */
	public void setComparableAttributes(String pComparableAttributes) {
		mComparableAttributes = pComparableAttributes;
	}
	/**
	 *Property to hold  subTypePropertyName.
	 */
	private String mSubTypePropertyName;
	
	/**
	 * Gets the sub type property name.
	 *
	 * @return the subTypePropertyName
	 */
	public String getSubTypePropertyName() {
		return mSubTypePropertyName;
	}
	
	/**
	 * Sets the sub type property name.
	 *
	 * @param pSubTypePropertyName the subTypePropertyName to set
	 */
	public void setSubTypePropertyName(String pSubTypePropertyName) {
		mSubTypePropertyName = pSubTypePropertyName;
	}
	
	/**
	 * Gets the ancestor category id property.
	 *
	 * @return the ancestorCategoryIdProperty
	 */
	public String getAncestorCategoryIdProperty() {
		return mAncestorCategoryIdProperty;
	}
	
	/**
	 * Sets the ancestor category id property.
	 *
	 * @param pAncestorCategoryIdProperty the ancestorCategoryIdProperty to set
	 */
	public void setAncestorCategoryIdProperty(String pAncestorCategoryIdProperty) {
		mAncestorCategoryIdProperty = pAncestorCategoryIdProperty;
	}
	
	/**
	 * Gets the sku alternate canonical image url.
	 *
	 * @return the mSkuAlternateCanonicalImageUrl
	 */
	public String getSkuAlternateCanonicalImageUrl() {
		return mSkuAlternateCanonicalImageUrl;
	}
	
	/**
	 * Sets the sku alternate canonical image url.
	 *
	 * @param pSkuAlternateCanonicalImageUrl the mSkuAlternateCanonicalImageUrl to set
	 */
	public void setSkuAlternateCanonicalImageUrl(String pSkuAlternateCanonicalImageUrl) {
		mSkuAlternateCanonicalImageUrl = pSkuAlternateCanonicalImageUrl;
	}
	
	/**
	 * Gets the sku primary canonical image url.
	 *
	 * @return the mSkuPrimaryCanonicalImageUrl
	 */
	public String getSkuPrimaryCanonicalImageUrl() {
		return mSkuPrimaryCanonicalImageUrl;
	}
	
	/**
	 * Sets the sku primary canonical image url.
	 *
	 * @param pSkuPrimaryCanonicalImageUrl the mSkuPrimaryCanonicalImageUrl to set
	 */
	public void setSkuPrimaryCanonicalImageUrl(String pSkuPrimaryCanonicalImageUrl) {
		mSkuPrimaryCanonicalImageUrl = pSkuPrimaryCanonicalImageUrl;
	}	
	
	/**
	 * Gets the sku secondary canonical image url.
	 *
	 * @return the mSkuSecondaryCanonicalImageUrl
	 */
	public String getSkuSecondaryCanonicalImageUrl() {
		return mSkuSecondaryCanonicalImageUrl;
	}
	
	/**
	 * Sets the sku secondary canonical image url.
	 *
	 * @param pSkuSecondaryCanonicalImageUrl the mSkuSecondaryCanonicalImageUrl to set
	 */
	public void setSkuSecondaryCanonicalImageUrl(String pSkuSecondaryCanonicalImageUrl) {
		mSkuSecondaryCanonicalImageUrl = pSkuSecondaryCanonicalImageUrl;
	}
	
	/**
	 * Gets the sku swatch canonical image url.
	 *
	 * @return the mSkuSwatchCanonicalImageUrl
	 */
	public String getSkuSwatchCanonicalImageUrl() {
		return mSkuSwatchCanonicalImageUrl;
	}
	
	/**
	 * Sets the sku swatch canonical image url.
	 *
	 * @param pSkuSwatchCanonicalImageUrl the mSkuSwatchCanonicalImageUrl to set
	 */
	public void setSkuSwatchCanonicalImageUrl(String pSkuSwatchCanonicalImageUrl) {
		mSkuSwatchCanonicalImageUrl = pSkuSwatchCanonicalImageUrl;
	}
	
	/**
	 * Gets the collection skus.
	 *
	 * @return the collectionSkus
	 */
	public String getCollectionSkus() {
		return mCollectionSkus;
	}
	
	/**
	 * Sets the collection skus.
	 *
	 * @param pCollectionSkus the collectionSkus to set
	 */
	public void setCollectionSkus(String pCollectionSkus) {
		mCollectionSkus = pCollectionSkus;
	}
	
	/**
	 * Gets the collection id.
	 *
	 * @return the collectionID
	 */
	public String getCollectionID() {
		return mCollectionID;
	}
	
	/**
	 * Sets the collection id.
	 *
	 * @param pCollectionID the collectionID to set
	 */
	public void setCollectionID(String pCollectionID) {
		mCollectionID = pCollectionID;
	}
	
	/**
	 * Gets the user type id.
	 *
	 * @return the userTypeId
	 */
	public String getUserTypeId() {
		return mUserTypeId;
	}
	
	/**
	 * Sets the user type id.
	 *
	 * @param pUserTypeId the userTypeId to set
	 */
	public void setUserTypeId(String pUserTypeId) {
		mUserTypeId = pUserTypeId;
	}
	
	/**
	 * Gets the Site Specific Static Page Url Property Name.
	 *
	 * @return the SiteSpecificStaticPageUrlPropertyName
	 */
	public String getSiteSpecificStaticPageUrlPropertyName() {
		return mSiteSpecificStaticPageUrlPropertyName;
	}
	
	/**
	 * Sets the Site Specific Static Page Url Property Name.
	 *
	 * @param pSiteSpecificStaticPageUrlPropertyName the SiteSpecificStaticPageUrlPropertyName to set
	 */
	public void setSiteSpecificStaticPageUrlPropertyName(
			String pSiteSpecificStaticPageUrlPropertyName) {
		mSiteSpecificStaticPageUrlPropertyName = pSiteSpecificStaticPageUrlPropertyName;
	}
	
	/**
	 * Gets the primary path category property name.
	 *
	 * @return the primaryPathCategoryPropertyName
	 */
	public String getPrimaryPathCategoryPropertyName() {
		return mPrimaryPathCategoryPropertyName;
	}
	
	/**
	 * Sets the primary path category property name.
	 *
	 * @param pPrimaryPathCategoryPropertyName the primaryPathCategoryPropertyName to set
	 */
	public void setPrimaryPathCategoryPropertyName(
			String pPrimaryPathCategoryPropertyName) {
		mPrimaryPathCategoryPropertyName = pPrimaryPathCategoryPropertyName;
	}
	
	/**
	 * Gets the parent products.
	 * 
	 * @return the parentProducts
	 */
	public String getParentProducts() {
		return mParentProducts;
	}
	/**
	 * Sets the parent products.
	 * 
	 * @param pParentProducts
	 *            the parentProducts to set
	 */
	public void setParentProducts(String pParentProducts) {
		mParentProducts = pParentProducts;
	}
	/**
	 * @return the primaryParentCategoryPropertyName
	 */
	public String getPrimaryParentCategoryPropertyName() {
		return mPrimaryParentCategoryPropertyName;
	}
	/**
	 * @param pPrimaryParentCategoryPropertyName the primaryParentCategoryPropertyName to set
	 */
	public void setPrimaryParentCategoryPropertyName(
			String pPrimaryParentCategoryPropertyName) {
		mPrimaryParentCategoryPropertyName = pPrimaryParentCategoryPropertyName;
	}
	/**
	 * @return the productGenderPropertyName
	 */
	public String getProductGenderPropertyName() {
		return mProductGenderPropertyName;
	}
	/**
	 * @param pProductGenderPropertyName the productGenderPropertyName to set
	 */
	public void setProductGenderPropertyName(String pProductGenderPropertyName) {
		mProductGenderPropertyName = pProductGenderPropertyName;
	}
}
