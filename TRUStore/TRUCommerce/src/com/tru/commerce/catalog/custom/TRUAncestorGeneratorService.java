package com.tru.commerce.catalog.custom;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import atg.adapter.gsa.GSARepository;
import atg.commerce.catalog.custom.AncestorGeneratorService;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;

/**
 *This is class is overridden to update the child products for categories.
 *
 *@author PA
 *@version 1.0
 */
public class TRUAncestorGeneratorService extends AncestorGeneratorService {
	
	/**
	 * Holds boolean property.
	 */
	private boolean mUpdateCatChldPrds;
	
	/**
	 * Holds boolean property.
	 */
	private boolean mUpdateDelCatChldSKUs;
	
	/**
	 * Holds CheckHiddenPropertyFlag property.
	 */
	private boolean mCheckHiddenPropertyFlag;
	
	/**
	 * Holds the procedure name for updating child products for category.
	 */
	private String mUpdateChildProductsSQL;
	
	/**
	 * Holds the procedure name for updating hidden flag for category, product.
	 */
	private String mUpdateHiddenPropertySQL;
	
	/**
	 * Holds the procedure name for updating child SKUs for category.
	 */
	private String mUpdateDelChildSKUSQL;
	
	/** 
	 * This is method is overridden to update the child products for category.
	 * @throws RepositoryException - if any
	 * @throws TransactionDemarcationException - if any
	 */
	@Override
	protected void updateProducts() throws RepositoryException,
	TransactionDemarcationException {
		if(isLoggingDebug()){
			logDebug("Enter into : TRUAncestorGeneratorService and method : updateProducts ");
		}
		if(isUpdateCatChldPrds()){	
			executeSqlStatement(getUpdateChildProductsSQL());
		}
		super.updateProducts();
		if(isLoggingDebug()){
			logDebug("Exit from : TRUAncestorGeneratorService and method : updateProducts ");
		}
	}
	
	/** 
	 * This is method is overridden to update the child SKUs for category.
	 * @throws RepositoryException - if any
	 * @throws TransactionDemarcationException - if any
	 */
	protected void updateSkus() throws RepositoryException,
	TransactionDemarcationException {
		if(isLoggingDebug()){
			logDebug("Enter into : TRUAncestorGeneratorService and method : updateSkus ");
		}
		super.updateSkus();
		if(isUpdateDelCatChldSKUs()){
			executeSqlStatement(getUpdateDelChildSKUSQL());
		}
		if(isCheckHiddenPropertyFlag()){
			executeSqlStatement(getUpdateHiddenPropertySQL());
		}
		if(isLoggingDebug()){
			logDebug("Exit from : TRUAncestorGeneratorService and method : updateSkus ");
		}
	}
	
	
	/**
	 * Execute sql statement.
	 *
	 * @param pProcedureName the procedure name
	 */
	private void executeSqlStatement(String pProcedureName){
		Connection dbConnection = null;
		CallableStatement stmt = null;
		try {
			dbConnection = ((GSARepository)getRepository()).getConnection();

			boolean autoCommit = dbConnection.getAutoCommit();

			dbConnection.setAutoCommit(false);
			
			if(isLoggingDebug()){
				vlogDebug("Procedure name : {0}", pProcedureName);
			}
			
			stmt = dbConnection.prepareCall(pProcedureName);
			
			if (isLoggingInfo()) {
				vlogInfo("Started updating is hidden flag for categories, product and SKUs : Procedure Name {0} ",pProcedureName);
			}
			stmt.executeUpdate();
			if (isLoggingInfo()) {
				vlogInfo("Completed updating is hidden flag for categories, product and SKUs : Procedure Name {0} ",pProcedureName);
			}
			stmt.close();
			dbConnection.commit();
			dbConnection.setAutoCommit(autoCommit);

		} catch (SQLException exc) {
			if(isLoggingError()){
				vlogError("SQLException: While connecting to data base : {0}", exc);
			}
		}
		if (dbConnection != null){
			((GSARepository)getRepository()).close(dbConnection);
		}	

	}

		/**
		 * Gets the update child products sql.
		 *
		 * @return the updateChildProductsSQL
		 */
		public String getUpdateChildProductsSQL() {
			return mUpdateChildProductsSQL;
		}

		/**
		 * Sets the update child products sql.
		 *
		 * @param pUpdateChildProductsSQL the updateChildProductsSQL to set
		 */
		public void setUpdateChildProductsSQL(String pUpdateChildProductsSQL) {
			mUpdateChildProductsSQL = pUpdateChildProductsSQL;
		}

		/**
		 * Checks if is update cat chld prds.
		 *
		 * @return the updateCatChldPrds
		 */
		public boolean isUpdateCatChldPrds() {
			return mUpdateCatChldPrds;
		}

		/**
		 * Sets the update cat chld prds.
		 *
		 * @param pUpdateCatChldPrds the updateCatChldPrds to set
		 */
		public void setUpdateCatChldPrds(boolean pUpdateCatChldPrds) {
			mUpdateCatChldPrds = pUpdateCatChldPrds;
		}
		
		/**
		 * Gets the update del child skusql.
		 *
		 * @return the getUpdateDelChildSKUSQL
		 */

		public String getUpdateDelChildSKUSQL() {
			return mUpdateDelChildSKUSQL;
		}
		
		/**
		 * Sets the update del child skusql.
		 *
		 * @param pUpdateDelChildSKUSQL the updateDelChildSKUSQL to set
		 */

		public void setUpdateDelChildSKUSQL(String pUpdateDelChildSKUSQL) {
			mUpdateDelChildSKUSQL = pUpdateDelChildSKUSQL;
		}

		/**
		 * Checks if is update del cat chld sk us.
		 *
		 * @return the updateDelCatChldSKUs
		 */
		public boolean isUpdateDelCatChldSKUs() {
			return mUpdateDelCatChldSKUs;
		}
		
		/**
		 * Sets the update del cat chld sk us.
		 *
		 * @param pUpdateDelCatChldSKUs the updateDelCatChldSKUs to set
		 */

		public void setUpdateDelCatChldSKUs(boolean pUpdateDelCatChldSKUs) {
			mUpdateDelCatChldSKUs = pUpdateDelCatChldSKUs;
		}

		/**
		 * Checks if is check hidden property flag.
		 *
		 * @return true, if is check hidden property flag
		 */
		public boolean isCheckHiddenPropertyFlag() {
			return mCheckHiddenPropertyFlag;
		}

		/**
		 * Sets the check hidden property flag.
		 *
		 * @param pCheckHiddenPropertyFlag the new check hidden property flag
		 */
		public void setCheckHiddenPropertyFlag(boolean pCheckHiddenPropertyFlag) {
			mCheckHiddenPropertyFlag = pCheckHiddenPropertyFlag;
		}

		/**
		 * Gets the update hidden property sql.
		 *
		 * @return the mUpdateHiddenPropertySQL
		 */
		public String getUpdateHiddenPropertySQL() {
			return mUpdateHiddenPropertySQL;
		}

		/**
		 * Sets the update hidden property sql.
		 *
		 * @param pUpdateHiddenPropertySQL the mUpdateHiddenPropertySQL to set
		 */
		public void setUpdateHiddenPropertySQL(String pUpdateHiddenPropertySQL) {
			mUpdateHiddenPropertySQL = pUpdateHiddenPropertySQL;
		}

}
