package com.tru.commerce.catalog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import atg.adapter.gsa.ChangeAwareSet;
import atg.adapter.gsa.LoadingStrategyContext;
import atg.adapter.gsa.query.Clause;
import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.inventory.InventoryException;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;

import com.google.gson.Gson;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.BatteryInfoJsonVO;
import com.tru.commerce.catalog.vo.BatteryProductInfoVO;
import com.tru.commerce.catalog.vo.BatteryVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.CompareProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoJsonVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.ProductPromoInfoVO;
import com.tru.commerce.catalog.vo.RelatedProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoJsonVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.review.TRUPowerReviewTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.common.tol.IResourceBundleTools;
import com.tru.common.vo.ResourceBundleVO;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * This class extends the OOTB CustomCatalogTools class. 
 * This class contains series of helper methods for accessing CustomCatalog.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCatalogTools extends CustomCatalogTools {
	
	/** 
	 * The mFilterSkuList. 
	 */
	private boolean mFilterSkuList;
	/** 
	 * The mCheckSuperDisplayFlag. 
	 */
	private boolean mCheckSuperDisplayFlag;
	
	/** property to hold powerReviewTools. */
	private TRUPowerReviewTools mPowerReviewTools;
	
	/** Property to hold is mCatalogConfiguration. */
	private TRUConfiguration mCatalogConfiguration;

	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;

	/** Holds the constant of IResourceBundleTools. */
	private IResourceBundleTools mResourceBundleTools;
	/** The m tru coherence inventory manager. */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;

	/** Property to hold BppDescriptorName. */

	private String mBppDescriptorName;
	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	/**
	 *  Holds the mStoreConfig.
	 */
	private TRUStoreConfiguration mStoreConfig;
	
	/**
	 *  Holds the mStoreLocatorTools.
	 */
	private PARALStoreLocatorTools mStoreLocatorTools; 
	

	/** This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;
	
	/** The TRU cart modifier helper. */
    private TRUCartModifierHelper mCartModifierHelper;
    /**
    * Gets the cart modifier helper.
    *
     * @return the cart modifier helper
    */
    public TRUCartModifierHelper getCartModifierHelper() {
           return mCartModifierHelper;
    }

    /**
    * Sets the cart modifier helper.
    *
     * @param pCartModifierHelper
    *            the new cart modifier helper
    */
    public void setCartModifierHelper(TRUCartModifierHelper pCartModifierHelper) {
           this.mCartModifierHelper = pCartModifierHelper;
    }
	/**
	 * Gets the pricing tools.
	 *
	 * @return the mPricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * Sets the pricing tools.
	 *
	 * @param pPricingTools the mPricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}


	/**
	 * Gets the store locator tools.
	 *
	 * @return the storeLocatorTools
	 */
	public PARALStoreLocatorTools getStoreLocatorTools() {
		return mStoreLocatorTools;
	}

	/**
	 * Sets the store locator tools.
	 *
	 * @param pStoreLocatorTools the mStoreLocatorTools to set
	 */
	public void setStoreLocatorTools(PARALStoreLocatorTools pStoreLocatorTools) {
		this.mStoreLocatorTools = pStoreLocatorTools;
	}

	/**
	 * Gets the store config.
	 *
	 * @return the storeConfig
	 */
	public TRUStoreConfiguration getStoreConfig() {
		return mStoreConfig;
	}

	/**
	 * Sets the store config.
	 *
	 * @param pStoreConfig the storeConfig to set
	 */
	public void setStoreConfig(TRUStoreConfiguration pStoreConfig) {
		mStoreConfig = pStoreConfig;
	}

	/**
	 * Gets the product page util.
	 *
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * Sets the product page util.
	 *
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}

	/**
	 * Gets the TRU coherence inventory manager.
	 * 
	 * @return the TRU coherence inventory manager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}

	/**
	 * Sets the TRU coherence inventory manager.
	 * 
	 * @param pCoherenceInventoryManager
	 *            the new TRU coherence inventory manager
	 */
	public void setCoherenceInventoryManager(
			TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/** Property to hold TRU siteContext. */
	private SiteContextManager mSiteContextManager;
	
	/**
	 *  Gets the getPowerReviewTools.
	 *  
	 * @return the powerReviewTools
	 */
	public TRUPowerReviewTools getPowerReviewTools() {
		return mPowerReviewTools;
	}

	/**
	 * Sets the TRU getPowerReviewTools.
	 *
	 * @param pPowerReviewTools the powerReviewTools to set
	 */
	public void setPowerReviewTools(TRUPowerReviewTools pPowerReviewTools) {
		mPowerReviewTools = pPowerReviewTools;
	} 
	

	
	/**
	 * This Method is used for get the List of All Root Categories.
	 * 
	 * @param pCatalogItem
	 *            - Catalog item
	 * @return allRootCategoriesList - All available root categories for the
	 *         catalog
	 * @throws RepositoryException
	 *             - If any RepositoryException occurs
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getCatalogsAllRootCategories(
			RepositoryItem pCatalogItem) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getCatalogsAllRootCategories(pCatalogItem).");
		}
		List<RepositoryItem> allRootCategoriesList = null;
		if (pCatalogItem == null) {
			return allRootCategoriesList;
		}
		if (isLoggingDebug()) {
			vlogDebug("pCatalogItem id : {0}", pCatalogItem.getRepositoryId());
		}
		Collection<Object> rootCategories = getRootCategoriesForCatalog(
				pCatalogItem, getCatalog());
		if (rootCategories != null && !rootCategories.isEmpty()) {
			RepositoryItem categoryItem = null;
			if (isLoggingDebug()) {
				vlogDebug("rootCategories size : {0}", rootCategories.size());
			}
			for (Object rootCategory : rootCategories) {
				if (rootCategory instanceof RepositoryItem) {
					categoryItem = (RepositoryItem) rootCategory;
					if (allRootCategoriesList == null) {
						allRootCategoriesList = new ArrayList<RepositoryItem>();
					}
					allRootCategoriesList.add(categoryItem);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getCatalogsAllRootCategories().");
		}
		return allRootCategoriesList;
	}

	/**
	 * This Method is used for get the List of All Classifications.
	 * 
	 * @param pRootCategoryItems
	 *            - category item
	 * @return allClassificationList - All available Classification list
	 * @throws RepositoryException
	 *             - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getAllClassifications(
			List<RepositoryItem> pRootCategoryItems) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getAllClassifications method..");
		}
		List<RepositoryItem> allClassificationList = null;
		if (isLoggingDebug()) {
			vlogDebug("Root categories size : {0}", pRootCategoryItems.size());
		}
		for (RepositoryItem rootCategoryItem : pRootCategoryItems) {
			allClassificationList = getAllTRURootClassification(rootCategoryItem);
			if (allClassificationList != null) {
				break;
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getAllClassifications method..");
		}
		return allClassificationList;
	}

	/**
	 * This Method is used for get the All TRU root classification items.
	 * 
	 * @param pCatalogItem
	 *            - Select classification Id
	 * @return allTRUClassificationList - All TRU root classification items
	 * @throws RepositoryException
	 *             - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getAllTRURootClassification(
			RepositoryItem pCatalogItem) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getAllTRUClassification(pCatalogItem).");
		}
		List<RepositoryItem> allTRUClassificationList = null;
		if (pCatalogItem == null) {
			return allTRUClassificationList;
		}
		if (isLoggingDebug()) {
			vlogDebug("pCatalogItem id : {0}", pCatalogItem.getRepositoryId());
		}
		// Query Builder Logic
		RepositoryItem[] classificationItems = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			RepositoryItemDescriptor classification = getCatalog()
					.getItemDescriptor(
							catalogProperties
									.getClassificationItemDescPropertyName());
			if (classification != null) {				
				RepositoryView repositoryView = classification.getRepositoryView();
				if (isLoggingDebug()) {
					logDebug("Getting the repository view for classification item descriptor : "
							+ classification.getRepositoryView());
				}
				if (repositoryView != null) {					
					QueryBuilder queryBuilder = repositoryView.getQueryBuilder();
					if (isLoggingDebug()) {
						logDebug("building a query for getting list of classification from repositoryView: ");
					}
					QueryExpression paramProperty = queryBuilder
							.createPropertyQueryExpression(catalogProperties
									.getClassificationParentCategoryPropertyName());
					QueryExpression paramValue = queryBuilder
							.createConstantQueryExpression(pCatalogItem
									.getRepositoryId());
					Query query = queryBuilder.createComparisonQuery(
							paramProperty, paramValue, QueryBuilder.EQUALS);
					classificationItems = repositoryView.executeQuery(query);
					if (classificationItems != null && classificationItems.length > 0) {
						if (isLoggingDebug()) {
							logDebug("Retrieving the array of ClassificationItems : "
									+ classificationItems);
						}
						if (allTRUClassificationList == null) {
							allTRUClassificationList = new ArrayList<RepositoryItem>();
						}
						allTRUClassificationList = Arrays
								.asList(classificationItems);
					}
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getAllTRUClassification().");
		}
		return allTRUClassificationList;
	}

	/**
	 * This Method is used for get the All Collection Product.
	 *
	 * @param pProductId            - Product Id
	 * @param pCollectionProductInfoVO            - CollectionProductInfoVO
	 * @param pChannelType            - ChannelType
	 * @return collectionProductItems - List<RepositoryItem>
	 * @throws RepositoryException             - repositoryException
	 */
	public List<RepositoryItem> findCollectionProduct(String pProductId,
			CollectionProductInfoVO pCollectionProductInfoVO, boolean pChannelType)
			throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.findCollectionProduct(pProductId).");
		}
		if (pProductId == null) {
			return null;
		}
		RepositoryItem collectionProduct = null;
		List<RepositoryItem> collectionProductItems = null;
		List<RepositoryItem> childSku = null;
		String collectionDisplayName = null;
		String collectionImage = null;
		String collectionLongDescription = null;
		String displayStatus=null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			if (isLoggingDebug()) {
				logDebug("Getting the collection product item :" + pProductId);
			}
			collectionProduct = getCatalog().getItem(
					pProductId,
					catalogProperties
							.getCollectionProductItemDescPropertyName());
			if (collectionProduct != null) {
				collectionProductItems = (List<RepositoryItem>) collectionProduct
						.getPropertyValue(catalogProperties
								.getChildProductsPropertyName());
				pCollectionProductInfoVO.setCollectionId(collectionProduct
						.getRepositoryId());
				if (collectionProduct.getPropertyValue(catalogProperties
						.getProductDisplayNamePropertyName()) != null) {
					collectionDisplayName = (String) collectionProduct
							.getPropertyValue(catalogProperties
									.getProductDisplayNamePropertyName());
					pCollectionProductInfoVO
							.setCollectionName(collectionDisplayName);
				}
				if (collectionProduct.getPropertyValue(catalogProperties
						.getCollectionImage()) != null) {
					collectionImage = (String) collectionProduct
							.getPropertyValue(catalogProperties
									.getCollectionImage());
					pCollectionProductInfoVO
							.setCollectionImage(collectionImage);
				}
				if (collectionProduct.getPropertyValue(catalogProperties
						.getProductLongDescription()) != null) {
					collectionLongDescription = (String) collectionProduct
							.getPropertyValue(catalogProperties
									.getProductLongDescription());
					pCollectionProductInfoVO
							.setCollectionDescription(collectionLongDescription);
				}
				if (collectionProduct.getPropertyValue(catalogProperties.getDisplayStatusPropertyName()) != null) {
					displayStatus = (String) collectionProduct.getPropertyValue(catalogProperties.getDisplayStatusPropertyName());
					if((TRUCommerceConstants.DISPLAY_STATUS_HIDDEN.equalsIgnoreCase(displayStatus))){
						pCollectionProductInfoVO.setDisplayStatus(TRUCommerceConstants.DISPLAY_STATUS_HIDDEN);
					}else{
						pCollectionProductInfoVO.setDisplayStatus(TRUCommerceConstants.DISPLAY_STATUS_ACTIVE);
					}	
				}else{
					pCollectionProductInfoVO.setDisplayStatus(TRUCommerceConstants.DISPLAY_STATUS_ACTIVE);
				}
				if (pChannelType && collectionProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName()) != null) {
					String childSkuId = null;
					childSku = (List<RepositoryItem>) collectionProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());
					if (childSku != null && !childSku.isEmpty()) {
						childSkuId = childSku.get(TRUCommerceConstants.INT_ZERO).getRepositoryId();
						if(StringUtils.isNotEmpty(childSkuId)){
							pCollectionProductInfoVO.setChildSkuId(childSkuId);	
						}	
					}
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.findCollectionProduct(pProductId).");
		}
		return collectionProductItems;
	}

	/**
	 * This Method is used for get the All parent classification items.
	 * 
	 * @param pClassificationId
	 *            - Select classification Id
	 * @return allParentClassificationItem - All available parent
	 *         classifications item
	 */
	@SuppressWarnings("unused")
	public List<RepositoryItem> findParentClassificationItem(
			String pClassificationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.findParentClassificationItem(pClassificationId).");
		}
		List<RepositoryItem> parentClassificationItem = null;
		if (pClassificationId == null) {
			return parentClassificationItem;
		}
		if (isLoggingDebug()) {
			vlogDebug("pclassificationId ", pClassificationId);
		}
		// Query Builder Logic
		RepositoryItem[] classificationItems = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			RepositoryItemDescriptor classification = getCatalog()
					.getItemDescriptor(
							catalogProperties
									.getClassificationItemDescPropertyName());
			if (classification != null) {
				RepositoryView repositoryView = classification.getRepositoryView();
				if (isLoggingDebug()) {
					logDebug("Getting the repository view for classification item descriptor : "
							+ classification.getRepositoryView());
				}
				if (repositoryView != null) {
					if (isLoggingDebug()) {
						logDebug("building a query for getting list of classification from repositoryView: ");
					}
					QueryBuilder queryBuilder = repositoryView
							.getQueryBuilder();
					QueryExpression paramProperty = queryBuilder
							.createPropertyQueryExpression(catalogProperties
									.getParentClassificationsItemPropertyName());
					QueryExpression paramValue = queryBuilder
							.createConstantQueryExpression(pClassificationId);
					Query query = queryBuilder.createComparisonQuery(
							paramProperty, paramValue, QueryBuilder.EQUALS);
					classificationItems = repositoryView.executeQuery(query);
					if (classificationItems != null
							&& classificationItems.length > 0) {
						if (isLoggingDebug()) {
							logDebug("Retrieving the array of ClassificationItems : "
									+ classificationItems);
						}
						if (parentClassificationItem != null) {
							parentClassificationItem = new ArrayList<RepositoryItem>();
						}
						parentClassificationItem = Arrays
								.asList(classificationItems);
					}
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.findParentClassificationItem().");
		}
		return parentClassificationItem;
	}

	/**
	 * This Method is used for get the All child classification items.
	 * 
	 * @param pClassificationId
	 *            - Select classification Id
	 * @return allChildClassificationItem - All available child classifications
	 *         item
	 */
	@SuppressWarnings("unused")
	public List<RepositoryItem> findChildClassificationItem(
			String pClassificationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.findChildClassificationItem(pclassificationId).");
		}
		List<RepositoryItem> childClassificationItem = null;
		if (pClassificationId == null) {
			return childClassificationItem;
		}
		if (isLoggingDebug()) {
			vlogDebug("pclassificationId ", pClassificationId);
		}
		// Query Builder Logic
		RepositoryItem[] classificationItems = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			RepositoryItemDescriptor classification = getCatalog()
					.getItemDescriptor(
							catalogProperties
									.getClassificationItemDescPropertyName());
			if (classification != null) {
				RepositoryView repositoryView = classification.getRepositoryView();
				if (isLoggingDebug()) {
					logDebug("Getting the repository view for classification item descriptor : "
							+ classification.getRepositoryView());
				}
				if (repositoryView != null) {
					QueryBuilder queryBuilder = repositoryView
							.getQueryBuilder();
					if (isLoggingDebug()) {
						logDebug("building a query for getting list of classification from repositoryView: ");
					}
					QueryExpression paramProperty = queryBuilder
							.createPropertyQueryExpression(catalogProperties
									.getChildClassificationsItemPropertyName());
					QueryExpression paramValue = queryBuilder
							.createConstantQueryExpression(pClassificationId);
					Query query = queryBuilder.createComparisonQuery(
							paramProperty, paramValue, QueryBuilder.EQUALS);
					classificationItems = repositoryView.executeQuery(query);
					if (classificationItems != null
							&& classificationItems.length > 0) {
						if (isLoggingDebug()) {
							logDebug("Retrieving the array of ClassificationItems : "
									+ classificationItems);
						}
						if (childClassificationItem != null) {
							childClassificationItem = new ArrayList<RepositoryItem>();
						}
						childClassificationItem = Arrays
								.asList(classificationItems);
					}
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.findChildClassificationItem(pclassificationId).");
		}
		return childClassificationItem;
	}

	/**
	 * This Method is used for get the All child classifications.
	 *
	 * @param pClassificationId            - Select classification Id
	 * @return allChildClassifications - All available child classifications
	 * @throws RepositoryException - RepositoryException.
	 */
	public int findFixedChildClassificationItems(
            String pClassificationId) throws RepositoryException {
     TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
     List<RepositoryItem> fixedChildCategories = null;
     int noOfChildClassifications=0;
     RepositoryItem classificationItem = getCatalog().getItem(pClassificationId, catalogProperties.getClassificationItemDescPropertyName());   
     if(classificationItem != null){
         fixedChildCategories = (List<RepositoryItem>) classificationItem.getPropertyValue(catalogProperties.getFixedChildCategoriesPropertyName());
         noOfChildClassifications=fixedChildCategories.size();
     }
     return noOfChildClassifications;
}


	/**
	 * This method used to create product info object which will be used to
	 * display the details in PDP page.
	 * 
	 * @param pProductItem
	 *            - Product item
	 * @param pChannelType - ChannelType
	 * @param pCatalogRefIds - CatalogRefIds
	 * @param pProductType - ProductType
	 * @param pCollectionProductInfoVO - CollectionProductInfoVO
	 * @return ProductInfoVO - product info object
	 */
	@SuppressWarnings("unchecked")
	public ProductInfoVO createProductInfo(RepositoryItem pProductItem, boolean pChannelType, List<String> pCatalogRefIds,String  pProductType,CollectionProductInfoVO pCollectionProductInfoVO) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createProductInfo method..");
		}
		ProductInfoVO productInfo = new ProductInfoVO();
		List<RepositoryItem> allSkus = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pProductItem != null) {
			populateProductBasicInfo(pProductItem, productInfo);
			if (isLoggingDebug()) {
				vlogDebug(
						"Product Id : {0}  Type : {1}  isActive : {2}  isNavigable : {3}",
						productInfo.getProductId(),
						productInfo.getProductType(), productInfo.isActive());
			}
			List<RepositoryItem> childSKUs = (List<RepositoryItem>) pProductItem.getPropertyValue(catalogProperties.getChildSkusPropertyName());
			if (pChannelType && pCatalogRefIds != null) {
			allSkus = childSKUs;
			}
			List<RepositoryItem> activeChildSKUs = getActiveChildSKUs(childSKUs, Boolean.TRUE,pProductType,pCollectionProductInfoVO,pProductItem.getRepositoryId());
			// List<RepositoryItem> inStockSKUs =
			// getAllInstockItems(activeChildSKUs,Boolean.TRUE,TRUCommerceConstants.INVENTORY_ONLINE);
			// List<RepositoryItem> outOfStockSKUs =
			// getAllInstockItems(activeChildSKUs,Boolean.FALSE,TRUCommerceConstants.INVENTORY_ONLINE);
			if (pChannelType && pCatalogRefIds != null) {
				activeChildSKUs = allSkus;
			}
			if (activeChildSKUs != null && !activeChildSKUs.isEmpty()) {

				SKUInfoVO skuVo = null;
				List<SKUInfoVO> childSkuList = new ArrayList<SKUInfoVO>();
				List<SKUInfoVO> inStockChildSkuList = new ArrayList<SKUInfoVO>();
				List<SKUInfoVO> outOfStockChildSkuList = new ArrayList<SKUInfoVO>();
				for (RepositoryItem childSKU : activeChildSKUs) {
					skuVo = createSKUInfo(childSKU, pProductItem, pChannelType);
					if (pChannelType && pCatalogRefIds != null) {
						pCatalogRefIds.add(skuVo.getId());
					}
					childSkuList.add(skuVo);
					String inventoryStatus = skuVo.getInventoryStatus();
					if (TRUCommerceConstants.IN_STOCK.equalsIgnoreCase(inventoryStatus)) {
						inStockChildSkuList.add(skuVo);
						productInfo.setProductStatus(TRUCommerceConstants.IN_STOCK);
					} else if (TRUCommerceConstants.OUT_OF_STOCK.equalsIgnoreCase(inventoryStatus)) {
						outOfStockChildSkuList.add(skuVo);
						productInfo.setProductStatus(TRUCommerceConstants.OUT_OF_STOCK);
					}
				}
				productInfo.setChildSKUsList(childSkuList);
				productInfo.setActiveSKUsList(inStockChildSkuList);
				productInfo.setInActiveSKUsList(outOfStockChildSkuList);
				if (inStockChildSkuList != null && !inStockChildSkuList.isEmpty() &&!pChannelType) {
					// Based on Highest Inventory check
					productInfo.setDefaultSKU(getDefaultSkuInfo(inStockChildSkuList,TRUCommerceConstants.INVENTORY_ONLINE));
				} if(!pChannelType) {
					productInfo.setDefaultSKU(getDefaultSkuInfo(childSkuList,TRUCommerceConstants.INVENTORY_ONLINE));
				}
				productInfo.setActive(Boolean.TRUE);
				if (!TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equalsIgnoreCase(productInfo.getProductType())) {
					// This method will populate all the variants related
					// information
					populateVariantsInformation(productInfo, activeChildSKUs,pProductItem);
				}
			} else {
				// If SKU is not active system should not display PDP page
				productInfo.setActive(Boolean.FALSE);
			}
		} else {
			// If SKU is not active system should not display PDP page
			productInfo.setActive(Boolean.FALSE);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createProductInfo method.. , productInfo::"
					+ productInfo);
		}
		return productInfo;
	}

	/**
	 * This method used to create product info object which will be used to
	 * display the details in PDP page.
	 * 
	 * @param pCollectionProductItem
	 *            - collectionProductItem
	 * @param pCollectionProductInfoVO
	 *            - collectionProductInfoVO
	 * @return pCollectionProductInfoVO
	 */
	public CollectionProductInfoVO createCollectionProductInfo(
			List<RepositoryItem> pCollectionProductItem,
			CollectionProductInfoVO pCollectionProductInfoVO) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createCollectionProductInfo method..");
		}
		ProductInfoVO productInfoVO = null;
		List<ProductInfoVO> productInfoVOList = null;
		if (pCollectionProductInfoVO != null && pCollectionProductItem != null
				&& !pCollectionProductItem.isEmpty()) {
			productInfoVOList = new ArrayList<ProductInfoVO>();
			for (RepositoryItem collectionProductItem : pCollectionProductItem) {
				productInfoVO = createProductInfo(collectionProductItem, false, null,TRUCommerceConstants.TYPE_COLLECTIONPRODUCT,pCollectionProductInfoVO);
				productInfoVOList.add(productInfoVO);
			}
			pCollectionProductInfoVO.setActiveProductInfoList(productInfoVOList);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createCollectionProductInfo method..");
		}
		return pCollectionProductInfoVO;
	}

	/**
	 * This method used to create product info object which will be used to
	 * display the details in PDP page.
	 * 
	 * @param pCompareProductItem
	 *            - compareProductItem
	 * @param pCompareFlag
	 *            compareFlag
	 * @return compareProductInfoVO - product info object
	 */
	public CompareProductInfoVO createCompareProductInfo(
			List<RepositoryItem> pCompareProductItem, boolean pCompareFlag) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createCompareProductInfo method..");
		}
		ProductInfoVO productInfoVO = null;
		List<ProductInfoVO> productInfoVOList = null;
		CompareProductInfoVO compareProductInfoVO = null;
		if (pCompareProductItem != null && !pCompareProductItem.isEmpty()) {
			compareProductInfoVO = new CompareProductInfoVO();
			productInfoVOList = new ArrayList<ProductInfoVO>();
			for (RepositoryItem compareProductItem : pCompareProductItem) {
				productInfoVO = createProductInfo(compareProductItem, false, null,TRUCommerceConstants.TYPE_PRODUCT,null);
				productInfoVOList.add(productInfoVO);
			}
			compareProductInfoVO.setProductInfoList(productInfoVOList);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createCompareProductInfo method..");
		}
		return compareProductInfoVO;
	}

	/**
	 * This method used to get defaultSku based on stock.
	 * 
	 * @param pSKUItems
	 *            - SKUItems
	 * @param pLocationId
	 *            - LocationId
	 * @return defaultSku - defaultSku
	 */
	public SKUInfoVO getDefaultSkuInfo(List<SKUInfoVO> pSKUItems,
			String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUDisplayProductInfoDroplet.getDefaultSkuVo method.. ");
		}
		SKUInfoVO defaultSku = null;
		List<Long> inventoryList = new ArrayList<Long>();
		Map<Long, SKUInfoVO> stockToItemMap = new HashMap<Long, SKUInfoVO>();
		for (SKUInfoVO skuItem : pSKUItems) {
			long inventory = skuItem.getAvailableInventory();
			stockToItemMap.put(inventory, skuItem);
			inventoryList.add(inventory);
		}
		long maxinventory = Collections.max(inventoryList);
		defaultSku = stockToItemMap.get(maxinventory);

		if (isLoggingDebug()) {
			logDebug("END:: TRUDisplayProductInfoDroplet.getDefaultSkuVo method.. ");
		}
		return defaultSku;
	}

	/**
	 * to populate inventory information and return default sku.
	 *
	 * @param pStoreId - pStoreId
	 * @param pProductInfo - pProductInfo
	 * @return SKUInfoVO - SKUInfoVO
	 */
	public SKUInfoVO getDefaultSkuInfoAndPopulateInvetoryInformation(
			String pStoreId, ProductInfoVO pProductInfo) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getDefaultSkuInfoAndPopulateInvetoryInformation method..");
		}
		List<Long> inventoryList = new ArrayList<Long>();
		List<Long> inStockInventoryList = new ArrayList<Long>();
		Map<Long, SKUInfoVO> stockToItemMap = new HashMap<Long, SKUInfoVO>();
		Map<Long, SKUInfoVO> inStockToItemMap = new HashMap<Long, SKUInfoVO>();
		SKUInfoVO defaultSku = null;
		List<SKUInfoVO> inStockChildSkuList = new ArrayList<SKUInfoVO>();
		List<SKUInfoVO> outOfStockChildSkuList = new ArrayList<SKUInfoVO>();
		List<SKUInfoVO> skuVos = pProductInfo.getChildSKUsList();
		String productStatus = TRUCommerceConstants.OUT_OF_STOCK;
		if(skuVos!=null){
		try {
				for (SKUInfoVO skuInfoVO : skuVos) {
				String inventoryStatusForStore = null;
				String inventoryStatusOnline = null;
				long availableInventoryLevelstore = TRUCommerceConstants.INT_ZERO;
				int inventoryStatusOnlineInt=TRUCommerceConstants.INT_ZERO;
				try{
					inventoryStatusOnlineInt = getCoherenceInventoryManager().queryAvailabilityStatus(skuInfoVO.getId());
				}catch(InventoryException e){
					vlogError("ERROR:: TRUCatalogTools.getDefaultSkuInfoAndPopulateInvetoryInformation method..", e);
					inventoryStatusOnlineInt=TRUCommerceConstants.INT_ZERO;
				}
				if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_IN_STOCK 
						|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER 
						|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
					inventoryStatusOnline = TRUCommerceConstants.IN_STOCK;
					} else if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||
							inventoryStatusOnlineInt == TRUCommerceConstants.INT_ZERO) {
					inventoryStatusOnline = TRUCommerceConstants.OUT_OF_STOCK;
				} 
				if (inventoryStatusOnline != null) {
					skuInfoVO.setInventoryStatus(inventoryStatusOnline);
				}
			//	boolean presellable = skuInfoVO.isPresellable();
				long availableInventoryLevelOnline = getCoherenceInventoryManager().queryStockLevel(
						skuInfoVO.getId());
				skuInfoVO.setAvailableInventory(availableInventoryLevelOnline);
				//skuInfoVO.setNewItem(isNewlyAddedInventory(skuInfoVO.getId()));
				inventoryList.add(availableInventoryLevelOnline);
				stockToItemMap.put(availableInventoryLevelOnline, skuInfoVO);
				if (!TRUCommerceConstants.OUT_OF_STOCK
						.equalsIgnoreCase(inventoryStatusOnline)) {
					inStockInventoryList.add(availableInventoryLevelOnline);
					inStockToItemMap.put(availableInventoryLevelOnline, skuInfoVO);
				}
				if (pStoreId != null) {
					int inventoryStatusForStoreInt = TRUCommerceConstants.INT_ZERO;
					try{
					inventoryStatusForStoreInt=getCoherenceInventoryManager().queryAvailabilityStatus(skuInfoVO.getId(),pStoreId);
					if(inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
						inventoryStatusForStoreInt = getInventoryStatusFromWareHouse(pStoreId, skuInfoVO.getId());
							}
					}catch(InventoryException e){
						vlogError("ERROR:: TRUCatalogTools.getDefaultSkuInfoAndPopulateInvetoryInformation method..", e);
						inventoryStatusForStoreInt=TRUCommerceConstants.INT_ZERO;
					}
					if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_IN_STOCK ||
							inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER ||
							inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
						inventoryStatusForStore = TRUCommerceConstants.IN_STOCK;
						skuInfoVO.setInventoryStatusInStore(inventoryStatusForStore);
					} else if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||
							inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
						inventoryStatusForStore = TRUCommerceConstants.OUT_OF_STOCK;
						skuInfoVO.setInventoryStatusInStore(inventoryStatusForStore);
					}
					if (inventoryStatusForStore != null
							&& !TRUCommerceConstants.OUT_OF_STOCK
									.equalsIgnoreCase(inventoryStatusForStore)) {
						availableInventoryLevelstore = getCoherenceInventoryManager().queryStockLevel(
								skuInfoVO.getId(), pStoreId);
						skuInfoVO
								.setAvailableInventoryInStore(availableInventoryLevelstore);
					}
				if (availableInventoryLevelstore != TRUCommerceConstants.INT_ZERO) {
					inventoryList.add(availableInventoryLevelstore);
					stockToItemMap.put(availableInventoryLevelstore, skuInfoVO);
					inStockToItemMap.put(availableInventoryLevelstore, skuInfoVO);
					inStockInventoryList.add(availableInventoryLevelstore);
				}
	
				// String inventoryStatus=skuInfoVO.getInventoryStatus();
				if (TRUCommerceConstants.IN_STOCK
						.equalsIgnoreCase(inventoryStatusOnline)
						|| TRUCommerceConstants.PRE_ORDER
								.equalsIgnoreCase(inventoryStatusOnline)) {
					inStockChildSkuList.add(skuInfoVO);
					productStatus = TRUCommerceConstants.IN_STOCK;
				} else if (TRUCommerceConstants.OUT_OF_STOCK
						.equalsIgnoreCase(inventoryStatusOnline)) {
					outOfStockChildSkuList.add(skuInfoVO);
						}
				}else if(getStoreConfig().isInventoryStatus()){
						if (TRUCommerceConstants.IN_STOCK.equalsIgnoreCase(inventoryStatusOnline) || TRUCommerceConstants.PRE_ORDER.equalsIgnoreCase(inventoryStatusOnline)) {
							inStockChildSkuList.add(skuInfoVO);
							productStatus = TRUCommerceConstants.IN_STOCK;
						} else if (TRUCommerceConstants.OUT_OF_STOCK.equalsIgnoreCase(inventoryStatusOnline)) {
							outOfStockChildSkuList.add(skuInfoVO);
						}
					}
			  }
			} catch (InventoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException occured", e);
			}
		}
		}
		if (isLoggingDebug()) {
			logDebug("TRUCatalogTools.getDefaultSkuInfoAndPopulateInvetoryInformation method..inStockChildSkuList::"
					+ inStockChildSkuList + ", productStatus::" + productStatus);
		}
		pProductInfo.setInStockSKUsList(inStockChildSkuList);
		pProductInfo.setOutofStockSKUsList(outOfStockChildSkuList);
		pProductInfo.setProductStatus(productStatus);
		long maxinventory = TRUCommerceConstants.INT_ZERO;
		if (inStockInventoryList !=null && !inStockInventoryList.isEmpty()) {
			if (inStockInventoryList
					.contains(TRUCommerceConstants.LONG_MINUS_ONE)) {
				defaultSku = inStockToItemMap
						.get(TRUCommerceConstants.LONG_MINUS_ONE);
			} else {
				maxinventory = Collections.max(inStockInventoryList);
				defaultSku = inStockToItemMap.get(maxinventory);
			}
		} else if(inventoryList!=null && !inventoryList.isEmpty()) {
			if (inventoryList.contains(TRUCommerceConstants.LONG_MINUS_ONE)) {
				defaultSku = stockToItemMap
						.get(TRUCommerceConstants.LONG_MINUS_ONE);
			} else {
				maxinventory = Collections.max(inventoryList);
				defaultSku = stockToItemMap.get(maxinventory);
			}
		}
		BatteryProductInfoVO batteryProductInfoVO=populateBatteryInfoFromProductInfo(pProductInfo);
		if(batteryProductInfoVO!=null ){
			populateBatteryProductInventoryInfo(pStoreId,pProductInfo);
		}
		if (isLoggingDebug()) {
			logDebug(" END: TRUCatalogTools.getDefaultSkuInfoAndPopulateInvetoryInformation method..defaultSku::"+ defaultSku);
		}
		return defaultSku;
	}
	
	/**
	 * This method is used to populate battery product inventory info.
	 * 
	 * @param pStoreId - pStoreId
	 * @param pProductInfo - pProductInfo
	 */
	public void populateBatteryProductInventoryInfo(String pStoreId, ProductInfoVO pProductInfo) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateBatteryProductInventoryInfo method..");
		}
		List<SKUInfoVO> childSKUInfo = pProductInfo.getChildSKUsList();
		if(childSKUInfo!=null && !childSKUInfo.isEmpty()){
			for (SKUInfoVO skuInfoVO : childSKUInfo) {
				if(skuInfoVO.getBatteryProducts()!=null && !skuInfoVO.getBatteryProducts().isEmpty()){
					populateBatteryInventoryInfo(skuInfoVO.getBatteryProducts(),pStoreId);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug(" END: TRUCatalogTools.populateBatteryProductInventoryInfo method..");
		}
	}


	
	/**
	 * to populate the inventory in a map.
	 * @param pSkuId - pSkuId
	 * @param pStoreId - pStoreId
	 * @param pInventoryMap - pInventoryMap
	 * @return Map<String,String>
	 */
	public Map<String,String> populateInventory(String pSkuId,String pStoreId,Map pInventoryMap){
		vlogDebug(" BEGIN: TRUCatalogTools.populateInventory method..SKUID::"+pSkuId);
		String inventoryStatusForStore = TRUCommerceConstants.EMPTY_STRING;
		String inventoryStatusOnline = null;
		long availableInventoryLevelstore = TRUCommerceConstants.INT_ZERO;
		long availableInventoryLevelOnline=TRUCommerceConstants.INT_ZERO;
		int inventoryStatusOnlineInt= TRUCommerceConstants.INT_ZERO;
		try {
			inventoryStatusOnlineInt = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId);
		if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_IN_STOCK ||
				inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER ||
				inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
			inventoryStatusOnline = TRUCommerceConstants.IN_STOCK;
		} else if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_OUT_OF_STOCK) {			
			inventoryStatusOnline = TRUCommerceConstants.OUT_OF_STOCK;
		} 
		
		availableInventoryLevelOnline = getCoherenceInventoryManager().queryStockLevel(pSkuId);
		
		if (pStoreId != null) {
			int inventoryStatusForStoreInt = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId,pStoreId);
			if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_IN_STOCK ||
					 inventoryStatusForStoreInt == TRUCommerceConstants.INT_PRE_ORDER ||
					 inventoryStatusForStoreInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
				inventoryStatusForStore = TRUCommerceConstants.IN_STOCK;
			} else if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
					inventoryStatusForStoreInt = getInventoryStatusFromWareHouse(pStoreId, pSkuId);
				if(inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
					inventoryStatusForStore = TRUCommerceConstants.OUT_OF_STOCK;
				}else{
					inventoryStatusForStore = TRUCommerceConstants.IN_STOCK;
				}
			} 
			if (inventoryStatusForStore != null && !TRUCommerceConstants.OUT_OF_STOCK.equalsIgnoreCase(inventoryStatusForStore)) {
					availableInventoryLevelstore = getCoherenceInventoryManager().queryStockLevel(pSkuId, pStoreId);
					if(availableInventoryLevelstore <= TRUCommerceConstants.INT_ZERO){
						availableInventoryLevelstore = getInventoryStockLevelFromWareHouse(pStoreId, pSkuId);
					}
				} 
			}
		}catch (InventoryException e) {
			vlogError("TRUCatalogTools.populateInventory method..", e);
		}
		pInventoryMap.put(TRUCommerceConstants.INVENTORY_STATUS_ONLINE, inventoryStatusOnline);
		pInventoryMap.put(TRUCommerceConstants.INVETORY_LEVEL_ONLINE, availableInventoryLevelOnline);
		pInventoryMap.put(TRUCommerceConstants.INVENTORY_STATUS_STORE, inventoryStatusForStore);
		pInventoryMap.put(TRUCommerceConstants.INVETORY_LEVEL_STORE, availableInventoryLevelstore);
		vlogDebug(" END: TRUCatalogTools.populateInventory method..pInventoryMap::"+pInventoryMap.size());
		return pInventoryMap;
	}
	
	
	/**
	 * This method will populate all the variants related information.
	 * 
	 * @param pProductInfo
	 *            - ProductInfo
	 * @param pSkuItems
	 *            - SkuItems
	 * @param pProductItem
	 *            - ProductItem
	 */
	public void populateVariantsInformation(ProductInfoVO pProductInfo,
			List<RepositoryItem> pSkuItems, RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateVariantsInformation method..::");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Set<String> allColorSwatchImageList = new HashSet<String>();
		// Set<String> allColorCodeList=new HashSet<String>();
		Map<String, String> colorCodeToSwatchImageMap = new HashMap<String, String>();
		Set<Integer> sizeSequenceList = new HashSet<Integer>();
		Set<Integer> colorSequenceList = new HashSet<Integer>();
		Set<String> sizeSet = new HashSet<String>();
		Set<String> colorSet = new HashSet<String>();

		Map<Integer, String> sequenceTosizeMap = new HashMap<Integer, String>();
		Map<Integer, String> sequenceToColorMap = new HashMap<Integer, String>();

		String colorCode = null;
		String colorSwatchImage = null;
		String colorSequence=null;
		String sizeSequence=null;
		String juvenileSize = null;
		String juvenileSizeDesc = null;
		Integer colorSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer sizeSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer count = TRUCommerceConstants.INT_FIVE_THOUSAND;
		for (RepositoryItem childSKU : pSkuItems) {
			RepositoryItem color=(RepositoryItem) childSKU.getPropertyValue(catalogProperties.getSkuColorCode());
			if(color!=null){
			colorCode=(String) color.getPropertyValue(catalogProperties.getColorName());
			colorSequence=(String) color.getPropertyValue(catalogProperties.getColorDisplaySequence());
			}
			colorSwatchImage = (String) childSKU
					.getPropertyValue(catalogProperties.getSkuSwatchImage());
			RepositoryItem size = (RepositoryItem)childSKU.getPropertyValue(catalogProperties.getSkuJuvenileSize());
			if(size!=null){
				juvenileSize=(String) size.getPropertyValue(catalogProperties.getSizeDescription());
				sizeSequence=(String) size.getPropertyValue(catalogProperties.getSizeDisplaySequence());
				if(size.getPropertyValue(catalogProperties.getDescription())!=null){
				juvenileSizeDesc=(String) size.getPropertyValue(catalogProperties.getDescription());
				}
			}
			if (colorSwatchImage != null) {
				allColorSwatchImageList.add(colorSwatchImage);
			}
			if (colorCode != null) {
				colorCode = colorCode.toLowerCase();
				colorCodeToSwatchImageMap.put(colorCode, colorSwatchImage);
				//Map<String, String> colorSequenceMap = getCatalogConfiguration().getColorSequenceMap();
			//populateSortedListInfo(sequenceToColorMap, colorSequenceList,	colorCode, colorSet, colorSequenceMap, count);
				if(colorSequence!=null){
					colorSequenceNumber=Integer.parseInt(colorSequence);
				}else{
					colorSequenceNumber=count;
					count++;
				}
				colorSequenceList.add(colorSequenceNumber);
				sequenceToColorMap.put(colorSequenceNumber, colorCode);
			}

				if (juvenileSizeDesc != null) {
					juvenileSizeDesc = juvenileSizeDesc.toUpperCase();
					//Map<String, String> sizeSequenceMap=getCatalogConfiguration().getSizeSequenceMap();
					//populateSortedListInfo(sequenceTosizeMap,sizeSequenceList,juvenileSize,sizeSet,sizeSequenceMap,count);
					if (sizeSequence!=null) {
						sizeSequenceNumber=Integer.parseInt(sizeSequence);
					} else {
						sizeSequenceNumber=count;
						count++;
					}
					sizeSequenceList.add(sizeSequenceNumber);
					sequenceTosizeMap.put(sizeSequenceNumber, juvenileSizeDesc);
				}else if(juvenileSize!=null){
					juvenileSize = juvenileSize.toUpperCase();
					if (sizeSequence!=null) {
						sizeSequenceNumber=Integer.parseInt(sizeSequence);
					} else {
						sizeSequenceNumber=count;
						count++;
					}
					sizeSequenceList.add(sizeSequenceNumber);
					sequenceTosizeMap.put(sizeSequenceNumber, juvenileSize);
				}

		}
		List<String> sortedColorList = makeSortedListFromCollection(
				colorSequenceList, sequenceToColorMap, colorSet);
		List<String> sortedSizeList = makeSortedListFromCollection(
				sizeSequenceList, sequenceTosizeMap, sizeSet);

		if (isLoggingDebug()) {
			logDebug(" TRUCatalogTools.populateVariantsInformation method..::Sorted size list:::"
					+ sortedSizeList);
		}
		pProductInfo.setColorSwatchImageList(allColorSwatchImageList);
		pProductInfo.setColorCodeList(sortedColorList);
		pProductInfo.setColorCodeToSwatchImageMap(colorCodeToSwatchImageMap);
		pProductInfo.setJuvenileSizesList(sortedSizeList);
		pProductInfo.setActive(Boolean.TRUE);
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateVariantsInformation method..::");
		}
	}

	/**
	 * To populate the map of Sequence To size Or ColorMap.
	 *
	 * @param pSequenceTosizeOrColorMap            - SequenceTosizeOrColorMap
	 * @param pSequenceList            - SequenceList
	 * @param pColorOrSizeCode            - ColorOrSizeCode
	 * @param pColorOrSizeSet            - ColorOrSizeSet
	 * @param pColorOrSizeSequenceMap            -ColorOrSizeSequenceMap
	 * @param pCount            - Count
	 * @param pFilterSet            - pFilterSet
	 * @return pcount
	 */
	public Integer populateSortedListInfo(
			Map<Integer, String> pSequenceTosizeOrColorMap,
			Set<Integer> pSequenceList, String pColorOrSizeCode,
			Set<String> pColorOrSizeSet,
			Map<String, String> pColorOrSizeSequenceMap, Integer pCount, Set<String> pFilterSet) {
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateSortedListInfo method..::");
		}
		Integer lCount = pCount;
		if (getCatalogConfiguration().isSortingEnabled()) {
			
			String upperCase = pColorOrSizeCode.toUpperCase();
			if (isLoggingDebug()) {
				logDebug("Sorting Enabled:: TRUCatalogTools.populateSortedListInfo method..::");
			}
			String sequenceString = null;
			if (pColorOrSizeSequenceMap != null) {
				sequenceString = pColorOrSizeSequenceMap.get(upperCase);
			}
			int intSequence = TRUCommerceConstants.INT_ZERO;
			if (sequenceString != null) {
				intSequence = Integer.parseInt(sequenceString);
				Integer sequence = intSequence;
				pSequenceList.add(sequence);
				pSequenceTosizeOrColorMap.put(sequence, pColorOrSizeCode);
			} else {
				if(!pFilterSet.contains(pColorOrSizeCode)){
				pSequenceTosizeOrColorMap.put(lCount, pColorOrSizeCode);
				pSequenceList.add(lCount);
				pFilterSet.add(pColorOrSizeCode);
				lCount++;
				}
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Sorting disabled:: TRUCatalogTools.populateSortedListInfo method..::");
			}
			pColorOrSizeSet.add(pColorOrSizeCode);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateSortedListInfo method..::");
		}
		return lCount;
	}

	/**
	 * To get the sorted list of color or size.
	 * 
	 * @param pSequenceList
	 *            - SequenceList
	 * @param pSequenceTosizeMap
	 *            - SequenceTosizeMap
	 * @param pColorOrSizeSet
	 *            - colorOrSizeSet
	 * @return sortedList - sortedList
	 */
	public List<String> makeSortedListFromCollection(
			Set<Integer> pSequenceList,
			Map<Integer, String> pSequenceTosizeMap, Set<String> pColorOrSizeSet) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.makeSortedListFromCollection method..:: not empty pSequenceTosizeMap::"
					+ pSequenceTosizeMap);
		}
		List<Integer> sortedSequenceList = null;
		List<String> sortedList = new ArrayList<String>();
		if (!pSequenceList.isEmpty()
				&& getCatalogConfiguration().isSortingEnabled()) {
			
			sortedSequenceList = new ArrayList<Integer>(pSequenceList);
			if (isLoggingDebug()) {
				logDebug("Sorting Enabled:: TRUCatalogTools.makeSortedListFromCollection method..:: not empty pSequenceList::"
						+ pSequenceList);
			}
			Collections.sort(sortedSequenceList);
			for (Integer sequenceNumber : sortedSequenceList) {
				String elementValue = pSequenceTosizeMap.get(sequenceNumber);
				sortedList.add(elementValue);
			}
		} else {
			if (pColorOrSizeSet != null && !pColorOrSizeSet.isEmpty()) {
				sortedList = new ArrayList<String>(pColorOrSizeSet);
				if (isLoggingDebug()) {
					logDebug("Sorting Disabled:: TRUCatalogTools.makeSortedListFromCollection method..:: not empty pColorOrSizeSet::"
							+ pColorOrSizeSet);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.makeSortedListFromCollection method..:: not empty pSequenceTosizeMap::"
					+ pSequenceTosizeMap);
		}
		return sortedList;
	}

	/**
	 * Method returns the inventory status.
	 * 
	 * @param pSkuItem
	 *            - SkuItem
	 * @return inventoryStatus - inventoryStatus
	 */
	public String getInventoryStatus(RepositoryItem pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getInventoryStatus method.. , pSKUItem::"
					+ pSkuItem);
		}
		String inventoryStatus = null;
		int inventoryStatusCode = TRUCommerceConstants.INT_ZERO;
		try {
			inventoryStatusCode = getCoherenceInventoryManager()
					.queryAvailabilityStatus(pSkuItem.getRepositoryId());
			if (inventoryStatusCode == TRUCommerceConstants.INT_IN_STOCK
					|| inventoryStatusCode == TRUCommerceConstants.INT_PRE_ORDER 
					|| inventoryStatusCode == TRUCommerceConstants.INT_LIMITED_STOCK) {
				inventoryStatus = TRUCommerceConstants.IN_STOCK;
			} else if (inventoryStatusCode == TRUCommerceConstants.INT_OUT_OF_STOCK) {
				inventoryStatus = TRUCommerceConstants.OUT_OF_STOCK;
			} 
		} catch (InventoryException exception) {
			if (isLoggingError()) {
				logError("RepositoryException occured", exception);
			}
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getInventoryStatus method.. ,  invetoryStatus Id:"
					+ inventoryStatus);
		}
		return inventoryStatus;
	}

	/**
	 * This method used to get in stock or out of stock items.
	 * 
	 * @param pSKUItems
	 *            - SKUItems
	 * @param pStockFlag
	 *            - StockFlag
	 * @param pLocationId
	 *            - LocationId
	 * @return outOfStockItems - outOfStockItems
	 */
	public List<RepositoryItem> getAllInstockItems(
			List<RepositoryItem> pSKUItems, boolean pStockFlag,
			String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getAllInstockItems method.. ,  pSKUItems :"
					+ pSKUItems
					+ ", pStockFlag:"
					+ pStockFlag
					+ ", pLocationId::" + pLocationId);
		}
		List<RepositoryItem> stockItems = new ArrayList<RepositoryItem>();
		List<RepositoryItem> outOfStockItems = new ArrayList<RepositoryItem>();
		if (pSKUItems == null) {
			return pSKUItems;
		}
		for (RepositoryItem skuItem : pSKUItems) {
			String inventoryStatus = getInventoryStatus(skuItem);
			if (TRUCommerceConstants.IN_STOCK.equals(inventoryStatus)) {
				stockItems.add(skuItem);
			} else if (TRUCommerceConstants.OUT_OF_STOCK
					.equals(inventoryStatus)) {
				outOfStockItems.add(skuItem);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getAllInstockItems method.. ,  stockItems:"
					+ stockItems + ", outOfStockItems::" + outOfStockItems);
		}
		if (pStockFlag) {
			return stockItems;
		} else {
			return outOfStockItems;
		}
	}

	/**
	 * This method used to get defaultSku based on stock.
	 * 
	 * @param pSKUItems
	 *            - SKUItems
	 * @param pLocationId
	 *            - LocationId
	 * @return defaultSku - defaultSku
	 */
	public RepositoryItem getDefaultSkuItem(List<RepositoryItem> pSKUItems,
			String pLocationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getDefaultSkuItem method.. ");
		}
		RepositoryItem defaultSku = null;
		List<Long> inventoryList = new ArrayList<Long>();
		Map<Long, RepositoryItem> stockToItemMap = new HashMap<Long, RepositoryItem>();
		for (RepositoryItem skuItem : pSKUItems) {
			long inventory = getInventory(skuItem);
			stockToItemMap.put(inventory, skuItem);
			inventoryList.add(inventory);
		}
		long maxinventory = Collections.max(inventoryList);
		defaultSku = stockToItemMap.get(maxinventory);

		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getDefaultSkuItem method.. ");
		}
		return defaultSku;
	}

	/**
	 * 
	 * This method used to get inventory.
	 * 
	 * @param pSKUItem
	 *            - SKUItem
	 * @return inventory - inventory
	 */
	public long getInventory(RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getInVentory method.. ");
		}
		long inventory = TRUCommerceConstants.LONG_MINUS_ONE;
		try {
			inventory = getCoherenceInventoryManager().queryStockLevel(
					pSKUItem.getRepositoryId());

		} catch (InventoryException exception) {
			inventory = TRUCommerceConstants.LONG_MINUS_ONE;
			if (isLoggingError()) {
				logError("RepositoryException occured", exception);
			}
		}
		return inventory;
	}

	/**
	 * This method is used to check the sku is presellable.
	 * 
	 * @param pSKUItem
	 *            - SKUItem
	 * @return preSellable - preSellable
	 */
	public boolean preSellable(RepositoryItem pSKUItem) {
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		boolean preSellable = Boolean.FALSE;
		Date streetDate = (Date) pSKUItem.getPropertyValue(catalogProperties
				.getStreetDate());
		Date currentDate = new Date();
		if (streetDate != null && currentDate.before(streetDate)) {
			preSellable = Boolean.TRUE;
		}
		return preSellable;

	}

	/**
	 * This method used to populate SKu related information into the VO object.
	 *
	 * @param pSKUItem            - SKUItem
	 * @param pProductItem            - ProductItem
	 * @param pChannelType the Channel type
	 * @return skuInfoVO - skuInfoVO
	 */

	/**
	 * This method used to populate SKu related information into the VO object.
	 * 
	 * @param pSKUItem  - SKUItem
	 * @param pProductItem  - ProductItem
	 * @param pChannelType - channelType
	 * @return skuInfoVO - skuInfoVO
	 */
	@SuppressWarnings("unchecked")
	public SKUInfoVO createSKUInfo(RepositoryItem pSKUItem,
			RepositoryItem pProductItem, boolean pChannelType) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createSKUInfo method.. ");
		}
		SKUInfoVO skuInfoVO = null;
		List<String> promos = null;
		if (pSKUItem != null) {
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			if (isLoggingDebug()) {
				vlogDebug("SKU Id : {0}", pSKUItem.getRepositoryId());
			}
			skuInfoVO = new SKUInfoVO();
			skuInfoVO.setId(pSKUItem.getRepositoryId());
			String skuType = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuType());
			skuInfoVO.setSkuType(skuType);
			if (pSKUItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName()) != null) {
				String displayName = (String) pSKUItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
				skuInfoVO.setDisplayName(displayName);
			}
			if (pSKUItem.getPropertyValue(catalogProperties.getProductBrandNamePrimary()) != null) {
				skuInfoVO.setBrandName((String) pSKUItem.getPropertyValue(catalogProperties.getProductBrandNamePrimary()));
			}
			if (pSKUItem.getPropertyValue(catalogProperties.getChannelAvailability()) != null) {
				skuInfoVO.setChannelAvailability((String) pSKUItem.getPropertyValue(catalogProperties.getChannelAvailability()));
			}
			if (pSKUItem.getPropertyValue(catalogProperties.getIspu()) != null) {
				String ispu= (String) pSKUItem.getPropertyValue(catalogProperties.getIspu());
				String ispuString=null;
				if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
					ispuString=TRUCommerceConstants.YES;
				}else{
					ispuString=TRUCommerceConstants.NO;
				}
				skuInfoVO.setIspu(ispuString);
			}else{
				skuInfoVO.setIspu(TRUCommerceConstants.NO);
			}			
			if (pSKUItem.getPropertyValue(catalogProperties.getProductLongDescription()) != null) {
				skuInfoVO.setLongDescription((String) pSKUItem.getPropertyValue(catalogProperties.getProductLongDescription()));
			}
			if (!pChannelType && getInventoryStatus(pSKUItem) != null) {
				skuInfoVO.setInventoryStatus(getInventoryStatus(pSKUItem));
			}
			if (!pChannelType) {
				skuInfoVO.setAvailableInventory(getInventory(pSKUItem));	
			}
			promos = (List<String>) pSKUItem.getPropertyValue(catalogProperties.getSkuQualifiedForPromos());
			skuInfoVO.setPromoString(promos);
			skuInfoVO.setNewItem(isNewlyAddedInventory(pSKUItem));
			populateSkuDetailsBySkuType(skuType, skuInfoVO, pSKUItem,pProductItem);
			if (pSKUItem.getPropertyValue(catalogProperties.getVendorsProductDemoUrl()) != null) {
				skuInfoVO.setVendorsProductDemoUrl((String) pSKUItem.getPropertyValue(catalogProperties.getVendorsProductDemoUrl()));
			}

		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createSKUInfo method.. ");
		}
		return skuInfoVO;
	}

	/**
	 * This method used to Enable/Disable the Registry/wishlist buttons in PDP
	 * page.
	 * 
	 * @param pSkuInfoVO
	 *            skuInfoVO
	 * @param pSKUItem
	 *            sKUItem
	 */
	private void populateRegistryDetails(SKUInfoVO pSkuInfoVO,
			RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateRegistryDetails method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pSKUItem.getPropertyValue(catalogProperties
				.getRegistryWarningIndicator()) != null) {
			String registryWarningIndicator = (String) (pSKUItem
					.getPropertyValue(catalogProperties
							.getRegistryWarningIndicator())).toString().toUpperCase();
			pSkuInfoVO.setRegistryWarningIndicator(registryWarningIndicator
					.toUpperCase());
		}
		if (pSKUItem.getPropertyValue(catalogProperties
				.getSkuRegistryEligibility()) != null) {
			pSkuInfoVO.setSkuRegistryEligibility((boolean) pSKUItem
					.getPropertyValue(catalogProperties
							.getSkuRegistryEligibility()));
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateRegistryDetails method.. ");
		}
	}

	/**
	 * The method is used to new flag on PDP.
	 * @param pSKUItem
	 *            - SKUItem
	 * @return newProductFlag - newProductFlag
	 */
	public boolean isNewlyAddedInventory(RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.isNewlyAddedInventory.");
		}

		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		boolean newProductFlag = Boolean.FALSE;
		Date inventorydate = null;
		inventorydate=(Date)pSKUItem.getPropertyValue(catalogProperties.getInventoryFirstAvailableDate());
	    Date currentDate = new Date();
		int diffInDays = TRUConstants.INT_MINUS_ONE;
		if (inventorydate != null) {
			diffInDays = (int) ((currentDate.getTime() - inventorydate.getTime()) / (TRUConstants.INT_THOUSAND
					* TRUConstants.INT_SIXTY * TRUConstants.INT_SIXTY * TRUConstants.INT_TWENTY_FOUR));
		}
		int limitOfDays = TRUCommerceConstants.INT_ZERO;// getConfigurationPath().getLimitOfdaysToTellAProductNew();
		Site site = SiteContextManager.getCurrentSite();
		String daysLimitThresold = null;
		if (site != null) {
			daysLimitThresold = (String) site.getPropertyValue(catalogProperties.getNewProductThreshold());
			if (daysLimitThresold != null) {
				limitOfDays = Integer.parseInt(daysLimitThresold);
			}
		}
		if (diffInDays >= TRUCommerceConstants.INT_ZERO && diffInDays <= limitOfDays) {
			newProductFlag = Boolean.TRUE;
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.isNewlyAddedInventory.");
		}
		return newProductFlag;
	}

	/**
	 * 
	 * This method is used to populate sku properties based on sku type.
	 * 
	 * @param pSkuType
	 *            - SkuType
	 * @param pSkuInfoVO
	 *            - SkuInfoVO
	 * @param pSKUItem
	 *            - SKUItem
	 * @param pProductItem
	 *            - ProductItem
	 */
	@SuppressWarnings("unchecked")
	private void populateSkuDetailsBySkuType(String pSkuType,
			SKUInfoVO pSkuInfoVO, RepositoryItem pSKUItem,
			RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateSkuDetailsBySkuType method.. ");
		}
		Map<String,String> collectionNames=null;
		boolean unCartable = Boolean.FALSE;
		boolean preOrderable = Boolean.FALSE;
		boolean dropShipFlag = Boolean.FALSE;
		boolean registryEligibility = Boolean.FALSE;
		boolean shipInOrigContainer = Boolean.FALSE;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (getCatalogConfiguration().getTruSkuTypes().contains(pSkuType)) {
			String mfgMinAge = (String) pSKUItem
					.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMin());
			String mfgMaxAge = (String) pSKUItem
					.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMax());
			if (mfgMaxAge != null) {
				pSkuInfoVO.setMfrSuggestedAgeMax(mfgMaxAge);
			}
			if (mfgMinAge != null) {
				pSkuInfoVO.setMfrSuggestedAgeMin(mfgMinAge);
			}
			String mfgAgeMessage = getMinAndMaxMFGAgeMessage(mfgMinAge,mfgMaxAge);
			if (mfgAgeMessage != null) {
				pSkuInfoVO.setSuggestAgeMessage(mfgAgeMessage);
			}
			//if (pSkuType.equalsIgnoreCase(catalogProperties.getSkuVideoGameSKU())) {
				populateESRBRating(pSkuInfoVO, pSKUItem);
			//}
			populateDimensionMapDetails(pSKUItem, pSkuInfoVO);
			populateAssemblyMapDetails(pSKUItem, pSkuInfoVO);
			if (pSKUItem.getPropertyValue(catalogProperties.getCanBeGiftWrapped()) != null) {
				pSkuInfoVO.setCanBeGiftWrapped((String) pSKUItem.getPropertyValue(catalogProperties.getCanBeGiftWrapped()));
			}
			if (pSKUItem.getPropertyValue(catalogProperties.getUnCartable()) != null) {
				unCartable = (boolean) pSKUItem.getPropertyValue(catalogProperties.getUnCartable());
			}
			pSkuInfoVO.setUnCartable(unCartable);
			if (pSKUItem.getPropertyValue(catalogProperties.getShipInOrigContainer()) != null) {
				shipInOrigContainer = (boolean) pSKUItem.getPropertyValue(catalogProperties.getShipInOrigContainer());
			}
			pSkuInfoVO.setShipInOrigContainer(shipInOrigContainer);

			if (pSKUItem.getPropertyValue(catalogProperties.getSkuRegistryEligibility()) != null) {
				registryEligibility = (boolean) pSKUItem.getPropertyValue(catalogProperties.getSkuRegistryEligibility());
			}
			pSkuInfoVO.setRegistryEligibility(registryEligibility);
			if (pSKUItem.getPropertyValue(catalogProperties.getPresellQuantityUnits()) != null || 
				pSKUItem.getPropertyValue(catalogProperties.getStreetDate()) != null) {
				preOrderable = populateAndReturnPresellProperties(pSkuInfoVO,pSKUItem,null,null);
			}
			pSkuInfoVO.setPresellable(preOrderable);
			if (pSKUItem.getPropertyValue(catalogProperties.getDropShipFlag()) != null) {
				dropShipFlag = (boolean) pSKUItem.getPropertyValue(catalogProperties.getDropShipFlag());
			}
			pSkuInfoVO.setDropShipFlag(dropShipFlag);
			populateHowToGetItInfo(pSKUItem,pSkuInfoVO,pProductItem);
			if(pSKUItem.getPropertyValue(catalogProperties.getOnlineCollectionName())!=null){
				collectionNames = (Map<String,String>)pSKUItem.getPropertyValue(catalogProperties.getOnlineCollectionName());
				pSkuInfoVO.setCollectionNames(collectionNames);
			}
			populateSkuVoDetailsBySkuType(pSkuInfoVO, pSKUItem);
			populateRegistryDetails(pSkuInfoVO, pSKUItem);
			populateCrossSellBatteries(pSkuInfoVO, pSKUItem);
			List<String> crossSellSkuIds= (List<String>) pSKUItem.getPropertyValue(catalogProperties.getCrossSellSkuId());
			if(crossSellSkuIds!= null && !crossSellSkuIds.isEmpty()){
				List<RelatedProductInfoVO> relatedproducts = getCrossSellproductsFromSkuIds(crossSellSkuIds);
				if(relatedproducts != null && !relatedproducts.isEmpty()){
					pSkuInfoVO.setRelatedProducts(relatedproducts);
				}
			}
			String rmsColor = (String) pSKUItem
					.getPropertyValue(catalogProperties.getRmsColorCode());
			String rmsSize = (String) pSKUItem
					.getPropertyValue(catalogProperties.getRmsSizeCode());
			if (rmsColor != null) {
				pSkuInfoVO.setRmsColorCode(rmsColor);
			}
			if (rmsSize != null) {
				pSkuInfoVO.setRmsSizeCode(rmsSize);
			}
			if (pSKUItem.getPropertyValue(catalogProperties.getOriginalParentProduct()) != null) {
				String originalParentProductId = (String) pSKUItem.getPropertyValue(catalogProperties.getOriginalParentProduct());
				pSkuInfoVO.setOriginalproductIdOrSKN(originalParentProductId);
			}
			if(StringUtils.isNotBlank(rmsSize) || StringUtils.isNotBlank(rmsColor)){
				pSkuInfoVO.setSknOrigin(getStoreConfig().getRmsSKNOrigin());
			}else{
				Map registryMap=getStoreConfig().getRegistryWishlistkeyMap();
				if(registryMap != null){
					String skuOriginDefault=(String) registryMap.get(TRUCommerceConstants.DEFAULT_SKN_ORIGIN);
					if(skuOriginDefault != null){
					pSkuInfoVO.setSknOrigin(Integer.parseInt(skuOriginDefault));
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateSkuDetailsBySkuType method.. ");
		}

	}
	
	/**
	 * This method is used to populate CrossSell batteries.
	 * 
	 * @param pSkuInfoVO
	 *            - SkuInfoVO
	 * @param pSKUItem
	 *            - SKUItem
	 */
	@SuppressWarnings("unchecked")
	private void populateCrossSellBatteries(SKUInfoVO pSkuInfoVO,
			RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateCrosssellBatteries method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List<RepositoryItem> crossSellRepositoryItem= (List<RepositoryItem>) pSKUItem.getPropertyValue(catalogProperties.getCrosssellBatteries());
		List<String> crossSellSkuIds = populateBatteryItems(crossSellRepositoryItem);
		if(crossSellSkuIds!= null && !crossSellSkuIds.isEmpty()){
			List<BatteryProductInfoVO> batteryProducts = getCrossSellBatteryproductsFromSkuIds(crossSellSkuIds);
			if(batteryProducts != null && !batteryProducts.isEmpty()){
				pSkuInfoVO.setBatteryProducts(batteryProducts);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateCrosssellBatteries method.. ");
		}
	}
	
	/**
	 * This method is used to populate battery item.
	 * 
	 * @param pCrossSellRepositoryItem
	 * 			- crossSellRepositoryItem
	 * @return List batterySkuIdList
	 *            - List BatterySkuIdList
	 */
	private List<String> populateBatteryItems(List<RepositoryItem> pCrossSellRepositoryItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateBatteryItems method.. ");
		}
		List<String> batterySkuIdList = new ArrayList<String>();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		for (RepositoryItem batteryItem : pCrossSellRepositoryItem) {
			//String batteryIncluded =(String)batteryItem.getPropertyValue(catalogProperties.getCrossSellsBatteries());
			String batteryUids =(String)batteryItem.getPropertyValue(catalogProperties.getUidBatteryCrossSell());
			if (StringUtils.isNotBlank(batteryUids)) {
				batterySkuIdList.add(batteryUids);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateBatteryItems method.. ");
		}
		return batterySkuIdList;
	}

	/**
	 * This method is used to populate CrossSellproducts ids for related produts.
	 * 
	 * @param pCrossSellSkuIds
	 *            - crossSellSkuIds
	 * @return List RelatedProductInfoVO
	 *            - List RelatedProductInfoVO
	 */
	@SuppressWarnings("unchecked")
	private List<RelatedProductInfoVO> getCrossSellproductsFromSkuIds(
			List<String> pCrossSellSkuIds) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getCrossSellproductsFromSkuIds method.. pCrossSellSkuIds:"+ pCrossSellSkuIds);
		}
		TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
		List<RelatedProductInfoVO> crossellProducts = new ArrayList<RelatedProductInfoVO>();
		for (String crossSellSkuId : pCrossSellSkuIds) {
			try {
				RepositoryItem skuItem= findSKU(crossSellSkuId);
				if(skuItem != null){
				Set<RepositoryItem> parentProduts = (Set<RepositoryItem>) skuItem.getPropertyValue(catalogProperties.getParentProductsPropertyName());
					if (parentProduts != null && !parentProduts.isEmpty()) {
						RepositoryItem parentProduct = parentProduts.iterator().next();
						RelatedProductInfoVO relatedProductVo = createRelatedProductInfos(parentProduct, skuItem);
						if (relatedProductVo != null) {
							crossellProducts.add(relatedProductVo);
						}
					}
				}
			} catch (RepositoryException e) {
				vlogError("Repository Exception occured while getting cross sell product from skuids for related products with exception : {0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getCrossSellproductsFromSkuIds method.. crossellProducts:: "+ crossellProducts);
		}
		return crossellProducts;
	}
	
	/**
	 * This method is used to populate CrossSellProducts ids for related produts.
	 * 
	 * @param pCrossSellBatterySkuIds
	 *            - CrossSellBatterySkuIds
	 * @return List BatteryProductInfoVO
	 *            - List BatteryProductInfoVO
	 */
	@SuppressWarnings("unchecked")
	private List<BatteryProductInfoVO> getCrossSellBatteryproductsFromSkuIds(
			List<String> pCrossSellBatterySkuIds) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getCrossSellBatteryproductsFromSkuIds method.. pCrossSellBatterySkuIds:"+ pCrossSellBatterySkuIds);
		}
		TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
		List<BatteryProductInfoVO> crossellProducts = new ArrayList<BatteryProductInfoVO>();
		for (String crossSellBatterySkuId : pCrossSellBatterySkuIds) {
			try {
				RepositoryItem skuItem= findSKU(crossSellBatterySkuId);
				if(skuItem != null){
				Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) skuItem.getPropertyValue(catalogProperties.getParentProductsPropertyName());
					if (parentProducts != null && !parentProducts.isEmpty()) {
						RepositoryItem parentProduct = parentProducts.iterator().next();
						BatteryProductInfoVO relatedProductVo = createBatteryProductInfos(parentProduct, skuItem);
						if (relatedProductVo != null) {
							crossellProducts.add(relatedProductVo);
						}
					}
				}
			} catch (RepositoryException e) {
				vlogError("Repository Exception occured while getting cross sell product from skuids for related products with exception : {0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getCrossSellBatteryproductsFromSkuIds method.. crossellProducts:: "+ crossellProducts);
		}
		return crossellProducts;
	}
	
	/**
	 * This method is used to populate sku details based on skuType.
	 * 
	 * @param pSkuInfoVO
	 *            - SkuInfoVO
	 * @param pSKUItem
	 *            - SKUItem
	 */
	private void populateSkuVoDetailsBySkuType(SKUInfoVO pSkuInfoVO,
			RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateSkuVoDetailsBySkuType method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if(pSKUItem.getPropertyValue(catalogProperties.getSkuColorCode())!=null){
			RepositoryItem colorItem = (RepositoryItem)pSKUItem.getPropertyValue(catalogProperties.getSkuColorCode());
			if(colorItem!=null && colorItem.getPropertyValue(catalogProperties.getColorName())!=null){
			String colorCode=(String) colorItem.getPropertyValue(catalogProperties.getColorName());
			pSkuInfoVO.setColorCode(colorCode.toLowerCase());
			}
		}
		if(pSKUItem.getPropertyValue(catalogProperties.getSkuJuvenileSize())!=null){
			RepositoryItem sizeItem = (RepositoryItem)pSKUItem.getPropertyValue(catalogProperties.getSkuJuvenileSize());
			if(sizeItem!=null && sizeItem.getPropertyValue(catalogProperties.getDescription())!=null){
			String juvenileSize=(String)sizeItem.getPropertyValue(catalogProperties.getDescription());
			pSkuInfoVO.setJuvenileSize(juvenileSize.toUpperCase());
			}
		}
		if(pSKUItem.getPropertyValue(catalogProperties.getProductSafetyWarnings())!=null){
			pSkuInfoVO.setSafetyWarning((List<String>)pSKUItem.getPropertyValue(catalogProperties.getProductSafetyWarnings()));
		}
		if(pSKUItem.getPropertyValue(catalogProperties.getUpcNumbers())!=null){
			pSkuInfoVO.setUpcNumbers((Set<String>)pSKUItem.getPropertyValue(catalogProperties.getUpcNumbers()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getUpcNumbers()) != null) {
			pSkuInfoVO.setUpcNumbers((Set<String>) pSKUItem
					.getPropertyValue(catalogProperties.getUpcNumbers()));
		}
		populateSkuImages(pSkuInfoVO, pSKUItem);
		if (pSKUItem.getPropertyValue(catalogProperties
				.getSkuPromotionalStickerDisplay()) != null) {
			String sticker = (String) pSKUItem
					.getPropertyValue(catalogProperties
							.getSkuPromotionalStickerDisplay());
			pSkuInfoVO.setPromotionalStickerDisplay(sticker);
		}
		if (pSKUItem.getPropertyValue(catalogProperties
				.getCustomerPurchaseLimit()) != null) {
			pSkuInfoVO.setCustomerPurchaseLimit((String) pSKUItem
					.getPropertyValue(catalogProperties
							.getCustomerPurchaseLimit()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties
				.getPriceDisplayPropertyName()) != null) {
			pSkuInfoVO.setPriceDisplay((String) pSKUItem
					.getPropertyValue(catalogProperties
							.getPriceDisplayPropertyName()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getBppId()) != null) {
			pSkuInfoVO.setBppId((Float) pSKUItem
					.getPropertyValue(catalogProperties.getBppId()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getBppEligible()) != null) {
			pSkuInfoVO.setBppEligible((Boolean) pSKUItem
					.getPropertyValue(catalogProperties.getBppEligible()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getBppName()) != null) {
			pSkuInfoVO.setBppName((String) pSKUItem
					.getPropertyValue(catalogProperties.getBppName()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getNotiFyMe()) != null) {
			pSkuInfoVO.setNotifyMe((String) pSKUItem
					.getPropertyValue(catalogProperties.getNotiFyMe()));
		}
		populateReviewRatings(pSkuInfoVO, pSKUItem);
		if (pSKUItem.getPropertyValue(catalogProperties.getProductFeature()) != null && getStoreConfig().isEnableProductFeatureSorting()) {
			Map<String,String>sortedFeatureMap=new TreeMap<String,String>((Map<String, String>)pSKUItem.getPropertyValue(catalogProperties.getProductFeature()));
			if(!sortedFeatureMap.isEmpty() && !sortedFeatureMap.keySet().isEmpty() && !sortedFeatureMap.values().isEmpty()){
				pSkuInfoVO.setFeatures(sortedFeatureMap);	
			}
		}else if(pSKUItem.getPropertyValue(catalogProperties.getProductFeature()) != null){
			pSkuInfoVO.setFeatures((Map<String, String>) pSKUItem.getPropertyValue(catalogProperties.getProductFeature()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getShipWindowMax()) != null) {
			pSkuInfoVO.setShipWindowMax((String) pSKUItem
					.getPropertyValue(catalogProperties.getShipWindowMax()));
		}
		else{
			pSkuInfoVO.setShipWindowMax(TRUCommerceConstants.STRING_TWO);
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getShipWindowMin()) != null) {
			pSkuInfoVO.setShipWindowMin((String) pSKUItem
					.getPropertyValue(catalogProperties.getShipWindowMin()));
		}
		else{
			pSkuInfoVO.setShipWindowMin(TRUCommerceConstants.STRING_ONE);
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getNmwaPercentage()) != null) {
			Float nmwaPercentage = (Float) pSKUItem
					.getPropertyValue(catalogProperties.getNmwaPercentage());
			pSkuInfoVO.setNmwaPercentage(nmwaPercentage.floatValue());
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getInlineFile()) != null) {
			pSkuInfoVO.setInlineFile((String) pSKUItem
					.getPropertyValue(catalogProperties.getInlineFile()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getOnlinePID()) != null) {
			pSkuInfoVO.setOnlinePID((String) pSKUItem
					.getPropertyValue(catalogProperties.getOnlinePID()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getWebDisplayFlag()) != null) {
			pSkuInfoVO.setDisplayable((Boolean) pSKUItem
					.getPropertyValue(catalogProperties.getWebDisplayFlag()));
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getSupressInSearch()) != null) {
			pSkuInfoVO.setSearchable((Boolean) pSKUItem
					.getPropertyValue(catalogProperties.getSupressInSearch()));
		}
		//if (pSKUItem.getPropertyValue(catalogProperties.getShipToStoreEeligible()) != null) {
			Boolean s2sValue=(Boolean) pSKUItem
					.getPropertyValue(catalogProperties.getShipToStoreEeligible());
			String s2s=null;
			if(s2sValue!=null && s2sValue.booleanValue())
			{
				s2s=TRUCommerceConstants.YES;
			}else{
				s2s=TRUCommerceConstants.NO;
			}
			pSkuInfoVO.setS2s(s2s);
		//}
	}
	
	/**
	 * This method is used to populate review ratings.
	 * 
	 * @param pSkuInfoVO - SkuInfoVO
	 * @param pSKUItem  - SKUItem
	 */
	private void populateReviewRatings(SKUInfoVO pSkuInfoVO,
			RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateReviewRatings method.. ");
		}	
			if(pSKUItem.getRepositoryId()!=null){
				Float reviewRating = (Float) getPowerReviewTools().getPowerReviewRating(pSKUItem.getRepositoryId());
				if(reviewRating != null){
					float rating=reviewRating.floatValue();
					int ratingInteger=(int) rating;
					pSkuInfoVO.setReviewRating(ratingInteger);
					pSkuInfoVO.setReviewRatingFraction(rating-ratingInteger);			
				}
			}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateReviewRatings method.. ");
		}
		
	}

	/**
	 * This method is used to populate Sku images.
	 *
	 * @param pSkuInfoVO - SkuInfoVO
	 * @param pSKUItem  - SKUItem
	 */
	private void populateSkuImages(SKUInfoVO pSkuInfoVO, RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateSkuImages method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pSKUItem.getPropertyValue(catalogProperties.getSkuAlternateCanonicalImageUrl()) != null) {
			List<String> alternateCanonicalImage = (List<String>) pSKUItem.getPropertyValue(catalogProperties.getSkuAlternateCanonicalImageUrl());
			pSkuInfoVO.setAlterNateCanonicalImages(alternateCanonicalImage);
		} 
		if (pSKUItem.getPropertyValue(catalogProperties.getSkuAlternateImages()) != null) {
			List<String> alternateImage = (List<String>) pSKUItem.getPropertyValue(catalogProperties.getSkuAlternateImages());
			pSkuInfoVO.setAlterNateImages(alternateImage);
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getSkuPrimaryCanonicalImageUrl()) != null) {
			String primaryCanonicalImage = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuPrimaryCanonicalImageUrl());
			pSkuInfoVO.setPrimaryCanonicalImage(primaryCanonicalImage);
		}
		if( pSKUItem.getPropertyValue(catalogProperties.getSkuPrimaryImage()) != null) {
			String primaryImage = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuPrimaryImage());
			pSkuInfoVO.setPrimaryImage(primaryImage);
		}
		if (pSKUItem.getPropertyValue(catalogProperties.getSkuSecondaryCanonicalImageUrl()) != null) {
			String secondaryCanonicalImage = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuSecondaryCanonicalImageUrl());
			pSkuInfoVO.setSecondaryCanonicalImage(secondaryCanonicalImage);
		}
		if(pSKUItem.getPropertyValue(catalogProperties.getSkuSecondaryImage()) != null) {
			String secondaryImage = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuSecondaryImage());
			pSkuInfoVO.setSecondaryImage(secondaryImage);
		}
		if(pSKUItem.getPropertyValue(catalogProperties.getSkuSwatchImage()) != null) {
			String swatchImage = (String) pSKUItem.getPropertyValue(catalogProperties.getSkuSwatchImage());
			pSkuInfoVO.setSwatchImage(swatchImage);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateSkuImages method.. ");
		}
	}

	/**
	 * To get the Age message.
	 * 
	 * @param pMfgMinAge
	 *            - MfgMinAge
	 * @param pMfgMaxAge
	 *            - MfgMaxAge
	 * @return message - message
	 */
	public String getMinAndMaxMFGAgeMessage(String pMfgMinAge, String pMfgMaxAge) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. pMfgMinAge::" + pMfgMinAge + ".. pMfgMaxAge::"
					+ pMfgMaxAge);
		}
		String message = null;

		if (pMfgMinAge != null && pMfgMaxAge != null) {
			String[] mfgMinAgeArry = StringUtils.splitStringAtCharacter(pMfgMinAge, TRUCommerceConstants.CHAR_HYPHEN);
			String[] mfgMaxAgeArry = StringUtils.splitStringAtCharacter(pMfgMaxAge, TRUCommerceConstants.CHAR_HYPHEN);
			String mfgMinAgeMonths = null;
			String mfgMaxAgeMonths = null;
			int mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			int mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			if (isLoggingDebug()) {
				logDebug("TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. mfgMinAgeArry::" + mfgMinAgeArry + ".. mfgMaxAgeArry::"
						+ mfgMaxAgeArry);
			}
			if (mfgMinAgeArry != null && mfgMinAgeArry.length >= TRUCommerceConstants.INT_TWO) {
				mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ONE];
			}
			if (mfgMaxAgeArry != null && mfgMaxAgeArry.length >= TRUCommerceConstants.INT_TWO) {
				mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ONE];
			}
			if (mfgMinAgeArry != null && mfgMinAgeArry.length == TRUCommerceConstants.INT_ONE) {
				mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ZERO];
			}
			if (mfgMaxAgeArry != null && mfgMaxAgeArry.length == TRUCommerceConstants.INT_ONE) {
				mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ZERO];
			}
			if (mfgMinAgeMonths != null && mfgMaxAgeMonths != null) {
				try {
					mfgMinAgeMonthsNum = Integer.parseInt(mfgMinAgeMonths);
				} catch (NumberFormatException e) {
					if (isLoggingError()) {
						logError(" TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. ", e);
					}
					mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
				}
				try {
					mfgMaxAgeMonthsNum = Integer.parseInt(mfgMaxAgeMonths);
				} catch (NumberFormatException e) {
					if (isLoggingError()) {
						logError(" TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. ", e);
					}
					mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
				}
			}
			if (isLoggingDebug()) {
				logDebug("TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. mfgMinAgeMonthsNum::" + mfgMinAgeMonthsNum
						+ ".. mfgMaxAgeMonthsNum::" + mfgMaxAgeMonthsNum);
			}
			if ((mfgMinAgeMonthsNum > TRUCommerceConstants.INT_TWENTY_FOUR && 
					mfgMinAgeMonthsNum < TRUCommerceConstants.INT_THREE_HUNDRED && mfgMinAgeMonthsNum
					% TRUCommerceConstants.INT_TWELVE != TRUCommerceConstants.INT_ZERO)
					|| (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_TWENTY_FOUR
							&& mfgMaxAgeMonthsNum < TRUCommerceConstants.INT_THREE_HUNDRED && mfgMaxAgeMonthsNum
							% TRUCommerceConstants.INT_TWELVE != TRUCommerceConstants.INT_ZERO)) {
				return message;
			}
			if (mfgMinAgeMonthsNum < mfgMaxAgeMonthsNum && mfgMinAgeMonthsNum > TRUCommerceConstants.INT_ZERO) {
				int mfgMaxAgeYearsNum = mfgMaxAgeMonthsNum / TRUCommerceConstants.INT_TWELVE;
				int mfgMinAgeYearsNum = mfgMinAgeMonthsNum / TRUCommerceConstants.INT_TWELVE;
				if (mfgMinAgeMonthsNum > TRUCommerceConstants.INT_ZERO && mfgMinAgeMonthsNum == TRUCommerceConstants.INT_ONE) {
					if (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_ZERO && mfgMaxAgeMonthsNum <= TRUCommerceConstants.INT_TWENTY_FOUR) {
						message = TRUCommerceConstants.BIRTH + mfgMaxAgeMonthsNum + TRUCommerceConstants.MONTHS;
					} else if (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_TWENTY_FOUR
							&& mfgMaxAgeMonthsNum < TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = TRUCommerceConstants.BIRTH + mfgMaxAgeYearsNum + TRUCommerceConstants.YEARS;
					} else if (mfgMaxAgeMonthsNum >= TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = TRUCommerceConstants.BIRTH_AND_UP;
					}
				} else if (mfgMinAgeMonthsNum > TRUCommerceConstants.INT_ONE && mfgMinAgeMonthsNum < TRUCommerceConstants.INT_TWENTY_FOUR
						&& mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_ZERO) {
					if (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_ZERO && mfgMaxAgeMonthsNum <= TRUCommerceConstants.INT_TWENTY_FOUR) {
						message = mfgMinAgeMonthsNum + TRUCommerceConstants.HYPHEN_SPACE + mfgMaxAgeMonthsNum + TRUCommerceConstants.MONTHS;
					} else if (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_TWENTY_FOUR
							&& mfgMaxAgeMonthsNum < TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = mfgMinAgeMonthsNum + TRUCommerceConstants.MONTHS + TRUCommerceConstants.HYPHEN_SPACE + mfgMaxAgeYearsNum
								+ TRUCommerceConstants.YEARS;
					} else if (mfgMaxAgeMonthsNum >= TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = mfgMinAgeMonthsNum + TRUCommerceConstants.MONTHS_AND_UP;
					}
				} else if (mfgMinAgeMonthsNum >= TRUCommerceConstants.INT_TWENTY_FOUR) {
					if (mfgMaxAgeMonthsNum > TRUCommerceConstants.INT_TWENTY_FOUR
							&& mfgMaxAgeMonthsNum < TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = mfgMinAgeYearsNum + TRUCommerceConstants.YEARS + TRUCommerceConstants.HYPHEN_SPACE + mfgMaxAgeYearsNum
								+ TRUCommerceConstants.YEARS;
					} else if (mfgMaxAgeMonthsNum >= TRUCommerceConstants.INT_THREE_HUNDRED) {
						message = mfgMinAgeYearsNum + TRUCommerceConstants.YEARS_AND_UP;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getMinAndMaxMFGAgeMessage method.. message::" + message);
		}
		return message;
	}

	/**
	 * To populate How to get it section.
	 * 
	 * @param pSKUItem
	 *            - SKUItem
	 * @param pSkuVO
	 *            - SkuVO
	 * @param pProductItem
	 *            - ProductItem
	 */
	public void populateHowToGetItInfo(RepositoryItem pSKUItem,
			SKUInfoVO pSkuVO, RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateHowToGetItInfo method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String freightClass = (String) pSKUItem.getPropertyValue(catalogProperties.getFreightClass());
		pSkuVO.setFreightClass(freightClass);
		RepositoryItem[] howTogetMessageItems = queryHowTogetMessageItems();
		String productHTGITMsg = (String) pSKUItem.getPropertyValue(catalogProperties.getProductHTGITMsg());
		if (howTogetMessageItems != null) {
			for (RepositoryItem howTogetMessageItem : howTogetMessageItems) {
				String storeMessageKey = (String) howTogetMessageItem.getPropertyValue(catalogProperties.getStoreMessageKey());
				String freightClassKey = (String) howTogetMessageItem.getPropertyValue(catalogProperties.getFreightClassKey());
				if (productHTGITMsg != null && productHTGITMsg.equalsIgnoreCase(storeMessageKey) && freightClass != null && freightClass.equalsIgnoreCase(freightClassKey) ) {
					String freightClassMessage = (String) howTogetMessageItem.getPropertyValue(catalogProperties.getFreightMessage());
					String storeMessage = (String) howTogetMessageItem.getPropertyValue(catalogProperties.getStoreMessage());
					if(freightClassMessage!= null){
						pSkuVO.setFreightClassMessage(freightClassMessage);
					}
					if(storeMessage!= null){
						pSkuVO.setStoreMessage(storeMessage);
					}
					
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateHowToGetItInfo method.. ");
		}

	}

	/**
	 * To query all the how to get it items.
	 * 
	 * @return items - items
	 */
	public RepositoryItem[] queryHowTogetMessageItems() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.queryHowTogetMessageItems method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		RepositoryView view = null;
		Query query = null;
		RepositoryItem[] items = null;
		try {
			view = getCatalog().getView(
					catalogProperties.getHowToGetItMessages());
			query = view.getQueryBuilder().createUnconstrainedQuery();
			items = view.executeQuery(query);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("TRUCatalogTools.queryHowTogetMessageItems method.. ",
						e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.queryHowTogetMessageItems method.. ");
		}
		return items;
	}

	/**
	 * This method is used to populate the assembly details under additional
	 * Info Section.
	 * 
	 * @param pSKUItem
	 *            - SKUItem
	 * @param pSkuVO
	 *            - skuVO
	 */
	@SuppressWarnings("unchecked")
	private void populateAssemblyMapDetails(RepositoryItem pSKUItem,
			SKUInfoVO pSkuVO) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUCatalogTools.populateAssemblyMapDetails method.. ");
		}
		Map<String, String> assemblyDimensionMap = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pSKUItem.getPropertyValue(catalogProperties.getAssemblyMap()) != null) {
			assemblyDimensionMap = (Map<String, String>) pSKUItem
					.getPropertyValue(catalogProperties.getAssemblyMap());
			if (assemblyDimensionMap != null && !assemblyDimensionMap.isEmpty()) {
				pSkuVO.setAssemblyRequired(assemblyDimensionMap
						.get(TRUCommerceConstants.ASSEMBLY_REQUIRED));
				pSkuVO.setAssembledWeight(assemblyDimensionMap
						.get(TRUCommerceConstants.ASSEMBLED_WEIGHT));
				pSkuVO.setAssemblyDimenssionLength(assemblyDimensionMap
						.get(TRUCommerceConstants.ASSEMBLED_DEPTH));
				pSkuVO.setAssemblyDimenssionWidth(assemblyDimensionMap
						.get(TRUCommerceConstants.ASSEMBLED_WIDTH));
				pSkuVO.setAssemblyDimenssionHeight(assemblyDimensionMap
						.get(TRUCommerceConstants.ASSEMBLED_HEIGHT));
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateAssemblyMapDetails method.. ");
		}

	}

	/**
	 * 
	 * This method is used to populate the dimensionMapDetails under
	 * AdditionalInfo section.
	 * 
	 * @param pSKUItem
	 *            - SKUItem
	 * @param pSkuVO
	 *            - SkuVO
	 */
	@SuppressWarnings("unchecked")
	private void populateDimensionMapDetails(RepositoryItem pSKUItem,
			SKUInfoVO pSkuVO) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUCatalogTools.populateDimensionMapDetails method.. ");
		}
		Map<String, String> dimensionMap = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pSKUItem.getPropertyValue(catalogProperties.getItemDimensionMap()) != null) {
			dimensionMap = (Map<String, String>) pSKUItem
					.getPropertyValue(catalogProperties.getItemDimensionMap());
			if (dimensionMap != null && !dimensionMap.isEmpty()) {
				pSkuVO.setProductDimenssionLength(dimensionMap
						.get(TRUCommerceConstants.ITEM_DEPTH));
				pSkuVO.setProductDimenssionHeight(dimensionMap
						.get(TRUCommerceConstants.ITEM_HEIGHT));
				pSkuVO.setProductDimenssionWidth(dimensionMap
						.get(TRUCommerceConstants.ITEM_WIDTH));
				pSkuVO.setProductItemWeight(dimensionMap
						.get(TRUCommerceConstants.ITEM_WEIGHT));
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateDimensionMapDetails method.. ");
		}
	}

	/**
	 * This method is used to populate battery details.
	 *
	 * @param pProductItem the product item
	 * @param pProductInfo            - pProductInfo
	 */
	@SuppressWarnings("unchecked")
	private void populateBatteryDetails(RepositoryItem pProductItem,
			ProductInfoVO pProductInfo) {
		if (isLoggingDebug()) {
			logDebug("START:: TRUCatalogTools.populateBatteryDetails method.. ");
		}
		BatteryVO batteryVO = null;
		List<BatteryVO> batteryVOs = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pProductItem.getPropertyValue(catalogProperties.getBatteries()) != null) {
			batteryVOs = new ArrayList<BatteryVO>();
			List<RepositoryItem> batteries = (List<RepositoryItem>) pProductItem.getPropertyValue(catalogProperties.getBatteries());
			for (RepositoryItem battery : batteries) {
				batteryVO = new BatteryVO();
				populateBatteryVO(battery, batteryVO);
				batteryVOs.add(batteryVO);
			}
			pProductInfo.setBatteries(batteryVOs);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateBatteryDetails method.. ");
		}
	}

	/**
	 * This method used to populate presell text into the VO object.
	 *
	 * @param pSkuVO            - SkuVO
	 * @param pSKUItem            - SKUItem
	 * @param pRelatedProductVo             - RelatedProductVo
	 *            
	 * @param pBatteryProductInfo the Battery product info
	 * @return preOrderable - preOrderable
	 */
	private boolean populateAndReturnPresellProperties(SKUInfoVO pSkuVO,
			RepositoryItem pSKUItem,RelatedProductInfoVO pRelatedProductVo,BatteryProductInfoVO pBatteryProductInfo ) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateAndReturnPresellProperties method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		boolean preOrderable = Boolean.FALSE;
		String presellQuantityUnits = (String) pSKUItem.getPropertyValue(catalogProperties.getPresellQuantityUnits());
		String backOrderStatus = (String) pSKUItem.getPropertyValue(catalogProperties.getBackOrderStatus());
		Date streetDate = (Date) pSKUItem.getPropertyValue(catalogProperties.getStreetDate());
		SimpleDateFormat sdf = new SimpleDateFormat(TRUCommerceConstants.DATE_FORMAT);
		Date currentDate = new Date();
		String formattedDate = null;
		if (streetDate != null && currentDate.before(streetDate)) {
			formattedDate = sdf.format(streetDate);
			preOrderable = Boolean.TRUE;
		}
		if(pSkuVO!=null) {
			pSkuVO.setStreetDate(formattedDate);
		}
		if(pRelatedProductVo!=null){
			pRelatedProductVo.setStreetDate(formattedDate);
		}
		if(pBatteryProductInfo!=null){
			if(presellQuantityUnits != null){
			pBatteryProductInfo.setPresellQuantityUnits(presellQuantityUnits);
			}
			if(backOrderStatus != null){
			pBatteryProductInfo.setBackOrderStatus(backOrderStatus);
			}
		}
		
		if (presellQuantityUnits != null) {
			if(pSkuVO!=null) {
			pSkuVO.setPresellQuantityUnits(presellQuantityUnits);
			}
			if(pRelatedProductVo !=null){
			pRelatedProductVo.setPresellQuantityUnits(presellQuantityUnits);
			}
		}
		if (backOrderStatus != null) {
			if(pSkuVO!=null) {
			pSkuVO.setBackOrderStatus(backOrderStatus);
			}
			if(pRelatedProductVo !=null){
			pRelatedProductVo.setBackOrderStatus(backOrderStatus);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateAndReturnPresellProperties method.. "+preOrderable);
		}
		return preOrderable;
	}

	/**
	 * This method used to populate Battery Related information into the VO
	 * object.
	 * 
	 * @param pBatteryItem
	 *            - BatteryItem
	 * @param pBatteryVO
	 *            - BatteryVO
	 */
	private void populateBatteryVO(RepositoryItem pBatteryItem,
			BatteryVO pBatteryVO) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateBatteryVO method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String batteryType = (String) pBatteryItem.getPropertyValue(catalogProperties.getBatteryType());
		Integer batteryQuantity = (Integer) pBatteryItem.getPropertyValue(catalogProperties.getBatteryQuantity());
		int quantity = 0;
		if (batteryQuantity != null) {
			quantity = batteryQuantity.intValue();
		}
		pBatteryVO.setBatteryType(batteryType);
		pBatteryVO.setBatteryQuantity(quantity);
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateBatteryVO method.. ");
		}
	}

	/**
	 * This method used to populate esrb rating for vedio skus.
	 * 
	 * @param pSkuVO
	 *            - SkuVO
	 * @param pSKUItem
	 *            - SKUItem
	 */
	private void populateESRBRating(SKUInfoVO pSkuVO, RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateESRBRating method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();

		if (pSKUItem.getPropertyValue(catalogProperties.getSkuVideoGameEsrb()) != null) {
			String videoGamesESrb = (String) pSKUItem
					.getPropertyValue(catalogProperties.getSkuVideoGameEsrb());
			if (videoGamesESrb != null) {
				pSkuVO.setVideoGameEsrb(videoGamesESrb);
			}
			if (getCatalogConfiguration().getTruVideoGameEsrb().get(0)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.EARLY_CHILD);
			} else if (getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_ONE)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.EVERYONE);
			} else if (getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_TWO)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.EVERYONETENPLUS);
			} else if (getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_THREE)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.MATURE);
			} else if (getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_FOUR)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.RATING_PENDING);
			} else if (getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_FIVE)
					.equalsIgnoreCase(videoGamesESrb)) {
				pSkuVO.setEsrbrating(TRUCommerceConstants.TEEN);
			}
			else if(getCatalogConfiguration().getTruVideoGameEsrb()
					.get(TRUCommerceConstants.INT_SIX)
					.equalsIgnoreCase(videoGamesESrb)){
				pSkuVO.setEsrbrating(TRUCommerceConstants.ADULTS_ONLY);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateESRBRating method.. ");
		}

	}

	/**
	 * This method will return active childSKUs for the specified product.
	 * 
	 * @param pChildSKUs
	 *            - ChildSKUs
	 * @param pActiveList
	 *            - ActiveList
	 * @param pProductType
	 *            - ProductType
	 * @param pCollectionProductInfoVO
	 * 			  - CollectionProductInfoVO
	 * @param pProductId
	 * 			  - ProductId
	 * @return activeChildSKUs - activeChildSKUs
	 */
	public List<RepositoryItem> getActiveChildSKUs(List<RepositoryItem> pChildSKUs, boolean pActiveList,String pProductType,CollectionProductInfoVO pCollectionProductInfoVO,String pProductId) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogTools.getActiveChildSKUs method.. ");
		}
		List<RepositoryItem> activeChildSKUs = null;
		List<RepositoryItem> inActiveChildSKUs = null;
		if (isLoggingDebug()) {
			vlogDebug("childSKUs id : {0} productType {1}",pChildSKUs.get(TRUCommerceConstants.INT_ZERO).getRepositoryId(),pProductType);
		}
		if (pChildSKUs != null && !pChildSKUs.isEmpty()) {
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			Boolean isDeleted = Boolean.FALSE;
			Boolean webDisplay = Boolean.TRUE;
			Boolean supressInSearch= Boolean.FALSE;
			Boolean isPriceAvailable = Boolean.TRUE;
			activeChildSKUs = new ArrayList<RepositoryItem>();
			inActiveChildSKUs = new ArrayList<RepositoryItem>();
			Boolean superDisplayFlag = Boolean.TRUE;
			for (RepositoryItem sku : pChildSKUs) {
				String skuType = (String) sku.getPropertyValue(catalogProperties.getSkuType());
				if (getCatalogConfiguration().getTruSkuTypes().contains(skuType)) {
					isDeleted = (Boolean) sku.getPropertyValue(catalogProperties.getIsSkuDeleted());
					webDisplay = (Boolean) sku.getPropertyValue(catalogProperties.getWebDisplayFlag());
					superDisplayFlag = (Boolean) sku.getPropertyValue(catalogProperties.getSuperDisplayFlag());
					isPriceAvailable = isZeroPriceAvailable(pProductId,sku.getRepositoryId());
					if (isDeleted == null) {
						isDeleted = Boolean.FALSE;
					}
					if (superDisplayFlag == null) {
						superDisplayFlag = Boolean.TRUE;
					} 
					if (webDisplay == null || ((sku.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName())==null) && getStoreConfig().isProductTitleFlag())) {
						webDisplay = Boolean.TRUE;
					}
					if(isPriceAvailable==null){
						isPriceAvailable = Boolean.FALSE;
					}
					if (!isDeleted.booleanValue() && webDisplay.booleanValue() && pProductType!=null 
							&& pProductType.equalsIgnoreCase(TRUCommerceConstants.TYPE_COLLECTIONPRODUCT)
							&& ((pCollectionProductInfoVO!=null  && pCollectionProductInfoVO.getDisplayStatus()!=null && pCollectionProductInfoVO.getDisplayStatus().equalsIgnoreCase(TRUCommerceConstants.DISPLAY_STATUS_ACTIVE)) &&  (superDisplayFlag.booleanValue()) && (!isPriceAvailable.booleanValue()))) {
							Map onlineCollectionName = (Map) sku.getPropertyValue(catalogProperties.getOnlineCollectionName());
							if(onlineCollectionName != null && !onlineCollectionName.isEmpty()){
								Set keySet = onlineCollectionName.keySet();
								if (keySet!= null && !keySet.isEmpty() && pCollectionProductInfoVO.getCollectionId()!=null && keySet.contains(pCollectionProductInfoVO.getCollectionId())) {
									activeChildSKUs.add(sku);
								}
							}
						}
					else if (!isDeleted.booleanValue() && webDisplay.booleanValue() && pProductType!=null && pProductType.equalsIgnoreCase(TRUCommerceConstants.TYPE_PRODUCT) &&  (superDisplayFlag.booleanValue()) && (!isPriceAvailable.booleanValue())) {
						activeChildSKUs.add(sku);
					}
					else{
						inActiveChildSKUs.add(sku);
					}
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("childSKUs size : {0}  activeChildSKUs size : {1} inActiveChidSKUs size: {2}",pChildSKUs.size(), activeChildSKUs.size(),inActiveChildSKUs.size());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.getActiveChildSKUs method.. ");
		}
		if (pActiveList) {
			return activeChildSKUs;
		} else {
			return inActiveChildSKUs;
		}
	}

	/**
	 * This method used to check whether price is available for current Sku which will be used.
	 * to decide active sku.
	 * 
	 * @param pProductId
	 *            - Product Id
	 * @param pSkuId - Sku Id
	 * @return boolean - is price available for current sku
	 */
	public Boolean isZeroPriceAvailable(String pProductId, String pSkuId) {
		if (isLoggingDebug()) {
			vlogDebug("START:: CatalogTools.isPriceAvailable method..ProductId: {0} SkuId: {1}",pProductId,pSkuId);
		}
		Site site= SiteContextManager.getCurrentSite();
		if(pProductId !=null && pSkuId !=null && site !=null){
			if (isLoggingDebug()) {
				vlogDebug("START:: CatalogTools.isPriceAvailable method.. ");
			}
			double listPrice = getPricingTools().getListPriceForSKU(pSkuId, pProductId, site);
			double salePrice = getPricingTools().getSalePriceForSKU(pSkuId, pProductId, site);
			if (isLoggingDebug()) {
				vlogDebug("listPrice: {0} salePrice: {1} site: {2}",listPrice,salePrice,site);
			}
			if(salePrice == TRUConstants.DOUBLE_ZERO){
				return true;
			}
		}
		return false;
		
	}

	/**
	 * This method used to create related product info object which will be used
	 * to display the related products selection in PDP page.
	 * 
	 * @param pProductItem
	 *            - Product item
	 * @param pSkuItem - pSkuItem
	 * @return RelatedProductInfos - List of active related products
	 */
	@SuppressWarnings("unchecked")
	public RelatedProductInfoVO createRelatedProductInfos(
			RepositoryItem pProductItem,RepositoryItem pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createRelatedProductInfos method..pSkuItem::"+ pSkuItem);
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
				RelatedProductInfoVO relatedProductInfo = null;
				List<RepositoryItem> allSkus = new ArrayList<RepositoryItem>();
				List<RepositoryItem> activeChildSKUs = null;
				String primaryImage = null;
				String suggestedAgeMax = null;
				String suggestedAgeMin = null;
				Float reviewRating = null;
				List<String> promos = null;
					boolean isActive = Boolean.FALSE;
					boolean preOrderable = Boolean.FALSE;
						allSkus.add(pSkuItem);
						if(pProductItem !=null){
						activeChildSKUs = getActiveChildSKUs(allSkus,Boolean.TRUE,TRUCommerceConstants.TYPE_PRODUCT,null,pProductItem.getRepositoryId());
						if(activeChildSKUs != null && !activeChildSKUs.isEmpty()){
							RepositoryItem defaultSku = pSkuItem;
							relatedProductInfo = new RelatedProductInfoVO();
							relatedProductInfo.setProductId(pProductItem.getRepositoryId());
							//Start of populateRelatedProductInfo(defaultSku,relatedProductInfo)
							Site site=SiteContextManager.getCurrentSite();
							String onlinePid=(String) defaultSku.getPropertyValue(catalogProperties.getOnlinePID());
							if(site!=null && StringUtils.isNotBlank(onlinePid)){
								String pdpURLWithOnlinePID=getProductPageUtil().constructPDPURLWithOnlinePID(onlinePid, site.toString());
								relatedProductInfo.setPdpURL(pdpURLWithOnlinePID);
								relatedProductInfo.setOnlinePid(onlinePid);
							}
							relatedProductInfo.setSkuId(defaultSku.getRepositoryId());
							relatedProductInfo.setDisplayName((String) defaultSku.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName()));
							primaryImage = ((String) defaultSku.getPropertyValue(catalogProperties.getSkuPrimaryImage()));
							if (primaryImage != null) {
								relatedProductInfo.setPrimaryImage(primaryImage);
							}
							suggestedAgeMax = ((String) defaultSku.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMax()));
							if (suggestedAgeMax != null) {
								relatedProductInfo.setSuggestedAgeMax(suggestedAgeMax);
							}
							suggestedAgeMin = ((String) defaultSku.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMin()));
							if (suggestedAgeMin != null) {
								relatedProductInfo.setSuggestedAgeMin(suggestedAgeMin);
							}
							relatedProductInfo.setSuggestedAgeMessage(getMinAndMaxMFGAgeMessage(suggestedAgeMin,suggestedAgeMax));
							if (defaultSku.getPropertyValue(catalogProperties.getPresellQuantityUnits()) != null || 
								defaultSku.getPropertyValue(catalogProperties.getStreetDate()) != null) {
								preOrderable = populateAndReturnPresellProperties(null,defaultSku,relatedProductInfo,null);
							}
							relatedProductInfo.setPresellable(preOrderable);
							if (defaultSku.getPropertyValue(catalogProperties.getPriceDisplayPropertyName()) != null) {
								relatedProductInfo.setPriceDisplay((String) defaultSku.getPropertyValue(catalogProperties.getPriceDisplayPropertyName()));
							}
							reviewRating = ((Float) defaultSku.getPropertyValue(catalogProperties.getSkuReviewRating()));
							if (reviewRating != null) {
								Float reviewRatingFloat = (Float) defaultSku.getPropertyValue(catalogProperties.getSkuReviewRating());
								if(reviewRatingFloat != null){
									float rating=reviewRatingFloat.floatValue();
									int ratingInteger=(int) rating;
									relatedProductInfo.setReviewRating(ratingInteger);
									relatedProductInfo.setReviewRatingFraction(rating-ratingInteger);
							}
							}
							//End of populateRelatedProductInfo(defaultSku,relatedProductInfo)
							isActive = Boolean.TRUE;
							relatedProductInfo.setActiveProduct(isActive);
							promos = (List<String>) defaultSku.getPropertyValue(catalogProperties.getSkuQualifiedForPromos());
							if (promos != null && !promos.isEmpty()) {
								relatedProductInfo.setPromoString(promos);
							}
						}
	}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createRelatedProductInfos method..relatedProductInfo::"+ relatedProductInfo);
		}
		return relatedProductInfo;
	}
	
	
	/**
	 * This method used to create battery product info object which will be used
	 * to display the battery products selection in PDP page.
	 * 
	 * @param pProductItem
	 *            - Product item
	 * @param pSkuItem - pSkuItem
	 * @return BatteryProductInfoVO - List of active battery products
	 */
	@SuppressWarnings("unchecked")
	public BatteryProductInfoVO createBatteryProductInfos(
			RepositoryItem pProductItem,RepositoryItem pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.createBatteryProductInfos method..pSkuItem::"+ pSkuItem);
		}
			BatteryProductInfoVO batteryProductInfo = null;
				List<RepositoryItem> allSkus = new ArrayList<RepositoryItem>();
				List<RepositoryItem> activeChildSKUs = null;
				boolean isActive = Boolean.FALSE;
				allSkus.add(pSkuItem);
				if(pProductItem !=null){
				activeChildSKUs = getActiveChildSKUs(allSkus,Boolean.TRUE,TRUCommerceConstants.TYPE_PRODUCT,null,pProductItem.getRepositoryId());
				if(activeChildSKUs != null && !activeChildSKUs.isEmpty()){
					RepositoryItem batterySkuItem = pSkuItem;
					batteryProductInfo = new BatteryProductInfoVO();
					batteryProductInfo.setProductId(pProductItem.getRepositoryId());
					populateBatteryProductInfo(batterySkuItem,batteryProductInfo);
					isActive = Boolean.TRUE;
					batteryProductInfo.setActiveProduct(isActive);
				}
				}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.createBatteryProductInfos method..batteryProductInfo::"+ batteryProductInfo);
		}
		return batteryProductInfo;
	}
	
	/**
	 * This method used to populate battery related information into product VO
	 * object.
	 * 
	 * @param pBatterySkuItem
	 *            - SkuItem
	 * @param pBatteryProductInfo
	 *            - BatteryProductInfoVO
	 */
	private void populateBatteryProductInfo(RepositoryItem pBatterySkuItem,
			BatteryProductInfoVO pBatteryProductInfo) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateBatteryProductInfo method..");
		}
		boolean unCartable = Boolean.FALSE;
		boolean preOrderable = Boolean.FALSE;
		String primaryImage = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Site site=SiteContextManager.getCurrentSite();
		String onlinePid=(String) pBatterySkuItem.getPropertyValue(catalogProperties.getOnlinePID());
		if(site!=null && StringUtils.isNotBlank(onlinePid)){
			String pdpURLWithOnlinePID=getProductPageUtil().constructPDPURLWithOnlinePID(onlinePid, site.toString());
			pBatteryProductInfo.setPdpURL(pdpURLWithOnlinePID);
			pBatteryProductInfo.setOnlinePid(onlinePid);
		}
		pBatteryProductInfo.setSkuId(pBatterySkuItem.getRepositoryId());
		pBatteryProductInfo.setDisplayName((String) pBatterySkuItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName()));
		primaryImage = ((String) pBatterySkuItem.getPropertyValue(catalogProperties.getSkuPrimaryCanonicalImageUrl()));
		if (primaryImage != null) {
			pBatteryProductInfo.setPrimaryImage(primaryImage);
		}
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getChannelAvailability()) != null) {
			pBatteryProductInfo.setChannelAvailability((String) pBatterySkuItem.getPropertyValue(catalogProperties.getChannelAvailability()));
		}
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getIspu()) != null) {
			String ispu= (String) pBatterySkuItem.getPropertyValue(catalogProperties.getIspu());
			String ispuString=null;
			if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
				ispuString=TRUCommerceConstants.YES;
			}else{
				ispuString=TRUCommerceConstants.NO;
			}
			pBatteryProductInfo.setIspu(ispuString);
		}else{
			pBatteryProductInfo.setIspu(TRUCommerceConstants.NO);
		}
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getUnCartable()) != null) {
			unCartable = (boolean) pBatterySkuItem.getPropertyValue(catalogProperties.getUnCartable());
		}
		pBatteryProductInfo.setUnCartable(unCartable);
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getNotiFyMe()) != null) {
			pBatteryProductInfo.setNotifyMe((String) pBatterySkuItem.getPropertyValue(catalogProperties.getNotiFyMe()));
		}
		Boolean s2sValue=(Boolean) pBatterySkuItem.getPropertyValue(catalogProperties.getShipToStoreEeligible());
		String s2s=null;
		if(s2sValue!=null && s2sValue.booleanValue())
		{
			s2s=TRUCommerceConstants.YES;
		}else{
			s2s=TRUCommerceConstants.NO;
		}
		pBatteryProductInfo.setS2s(s2s);
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getPriceDisplayPropertyName()) != null) {
			pBatteryProductInfo.setPriceDisplay((String) pBatterySkuItem.getPropertyValue(catalogProperties.getPriceDisplayPropertyName()));
		}
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getPresellQuantityUnits()) != null || 
						pBatterySkuItem.getPropertyValue(catalogProperties.getStreetDate()) != null) {
			preOrderable = populateAndReturnPresellProperties(null,pBatterySkuItem,null,pBatteryProductInfo);
		}
		pBatteryProductInfo.setPresellable(preOrderable);
		
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getProductLongDescription()) != null) {
			pBatteryProductInfo.setLongDescription((String) pBatterySkuItem.getPropertyValue(catalogProperties.getProductLongDescription()));
		}
		if (pBatterySkuItem.getPropertyValue(catalogProperties.getOriginalParentProduct()) != null) {
			String originalParentProductId = (String) pBatterySkuItem.getPropertyValue(catalogProperties.getOriginalParentProduct());
			pBatteryProductInfo.setOriginalproductIdOrSKN(originalParentProductId);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateBatteryProductInfo method..");
		}
	}

	/**
	 * This method used to populate product related information into product VO
	 * object.
	 * 
	 * @param pProductItem
	 *            - ProductItem
	 * @param pProductInfo
	 *            - ProductInfo
	 */
	public void populateProductBasicInfo(RepositoryItem pProductItem,
			ProductInfoVO pProductInfo) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateProductBasicInfo method..");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		pProductInfo.setProductId(pProductItem.getRepositoryId());
		populateProductType(pProductInfo, pProductItem);
		pProductInfo.setColorSizeVariantsAvailableStatus(pProductInfo
				.getProductType());
		if (pProductItem.getPropertyValue(catalogProperties
				.getProductDisplayNamePropertyName()) != null) {
			String displayName = (String) pProductItem
					
					.getPropertyValue(catalogProperties
							.getProductDisplayNamePropertyName());
			pProductInfo.setDisplayName(displayName);
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getProductDescriptionPropertyName()) != null) {
			pProductInfo.setDescription((String) pProductItem
					.getPropertyValue(catalogProperties
							.getProductDescriptionPropertyName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getProductSizeChartName()) != null) {
			pProductInfo.setSizeChart((String) pProductItem
					.getPropertyValue(catalogProperties
							.getProductSizeChartName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getProductRusItemNumber()) != null) {
			pProductInfo.setRusItemNumber(String.valueOf(pProductItem
					.getPropertyValue(catalogProperties
							.getProductRusItemNumber())));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getProductManufacturerStyleNumber()) != null) {
			pProductInfo.setManufacturerStyleNumber((String) pProductItem
					.getPropertyValue(catalogProperties
							.getProductManufacturerStyleNumber()));
		}
		/*if (pProductItem.getPropertyValue(catalogProperties.getProductBrand()) != null) {
			pProductInfo.setBrandName((String) pProductItem
					.getPropertyValue(catalogProperties.getProductBrand()));
		}*/
		if (pProductItem.getPropertyValue(catalogProperties
				.getBatteryRequiredPropertyName()) != null) {
			pProductInfo.setBatteryRequired((String) pProductItem
					.getPropertyValue(catalogProperties
							.getBatteryRequiredPropertyName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getBatteryIncludedPropertyName()) != null) {
			pProductInfo.setBatteryIncluded((String) pProductItem
					.getPropertyValue(catalogProperties
							.getBatteryIncludedPropertyName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getEwasteSurchargeSkuPropertyName()) != null) {
			pProductInfo.setEwasteSurchargeSku((String) pProductItem
					.getPropertyValue(catalogProperties
							.getEwasteSurchargeSkuPropertyName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties.getEwasteSurchargeStatePropertyName()) != null) {
			pProductInfo.setEwasteSurchargeState((String) pProductItem.getPropertyValue(catalogProperties.getEwasteSurchargeStatePropertyName()));
		}
		if (pProductItem.getPropertyValue(catalogProperties
				.getVendorsProductDemoUrl()) != null) {
			pProductInfo.setVendorsProductDemoUrl((String) pProductItem
					.getPropertyValue(catalogProperties
							.getVendorsProductDemoUrl()));
		}
		/*if (pProductItem.getPropertyValue(catalogProperties
				.getChannelAvailability()) != null) {
			pProductInfo.setChannelAvailability((String) pProductItem
					.getPropertyValue(catalogProperties
							.getChannelAvailability()));
		}*/
		if (pProductItem.getPropertyValue(catalogProperties.getWhyWeLoveIt()) != null) {
			pProductInfo.setWhyWeloveIt((String) pProductItem
					.getPropertyValue(catalogProperties.getWhyWeLoveIt()));
		}
		if (pProductItem.getPropertyValue(catalogProperties.getS2s()) != null) {
			String sfs= (String) pProductItem
					.getPropertyValue(catalogProperties.getS2s());
			String sfsString=null;
		if (TRUCommerceConstants.YES.equalsIgnoreCase(sfs) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(sfs) ||
				 TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(sfs)){
				sfsString=TRUCommerceConstants.YES;
			}else{
				sfsString=TRUCommerceConstants.NO;
			}
			pProductInfo.setSfs(sfsString);
		}
		populateBatteryDetails(pProductItem, pProductInfo);
		/*if (pProductItem.getPropertyValue(catalogProperties.getIspu()) != null) {
			String ispu= (String) pProductItem
					.getPropertyValue(catalogProperties.getIspu());
			String ispuString=null;
		if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) ||
				TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
				ispuString=TRUCommerceConstants.YES;
			}else{
				ispuString=TRUCommerceConstants.NO;
			}
			pProductInfo.setIspu(ispuString);
		}*/
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateProductBasicInfo method..");
		}
	}

	/**
	 * This method is used to populate the productType.
	 *
	 * @param pProductInfo            - ProductInfoVO
	 * @param pProductItem            - ProductItem
	 */
	@SuppressWarnings("unchecked")
	public void populateProductType(ProductInfoVO pProductInfo,
			RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.populateProductType method..");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List<String> styleDimensions = (List<String>) pProductItem
				.getPropertyValue(catalogProperties.getProductStyleDimension());
		if (styleDimensions != null && !styleDimensions.isEmpty()) {
			boolean colorFlag = Boolean.FALSE;
			boolean sizeFlag = Boolean.FALSE;
			for (String styleDimension : styleDimensions) {
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.COLOR)) {
					colorFlag = Boolean.TRUE;
				}
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.SIZE)) {
					sizeFlag = Boolean.TRUE;
				}
			}
			if (colorFlag && sizeFlag) {
				pProductInfo
						.setProductType(TRUCommerceConstants.BOTH_COLOR_SIZE_AVAILABLE);
			} else if (colorFlag && !sizeFlag) {
				pProductInfo
						.setProductType(TRUCommerceConstants.ONLY_COLOR_VARIANTS);
			} else if (!colorFlag && sizeFlag) {
				pProductInfo
						.setProductType(TRUCommerceConstants.ONLY_SIZE_VARIANTS);
			}
		} else {
			pProductInfo
					.setProductType(TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.populateProductType method..");
		}
	}

	/**
	 * This method used to validate category is valid or not.
	 * 
	 * @param pCategoryItem
	 *            - Category item to fetch the repository data
	 * @return boolean - Return true if valid category otherwise false
	 */
	public boolean isValidCategory(RepositoryItem pCategoryItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.isValidCategory method..");
		}
		boolean isValidCategory = Boolean.FALSE;
		if (pCategoryItem != null) {
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			String displayStatus = (String) pCategoryItem
					.getPropertyValue(catalogProperties
							.getDisplayStatusPropertyName());
			if (displayStatus != null
					&& displayStatus
							.equalsIgnoreCase(TRUCommerceConstants.HIDDEN)) {
				isValidCategory = Boolean.FALSE;
			} else {
				isValidCategory = Boolean.TRUE;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.isValidCategory method.. {0}",
					isValidCategory);
		}
		return isValidCategory;
	}

	/**
	 * Method to Get Set parentProducts for provided SKU item.
	 * 
	 * @param pSkuItem
	 *            - Sku Repository Item
	 * @return - Set of ParentProducts.
	 */
	@SuppressWarnings("unchecked")
	public Set<RepositoryItem> getParentProducts(RepositoryItem pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("Entered into TRUCatalogTools.getParentProducts method");
			logDebug("pSkuItem:" + pSkuItem);
		}
		if (pSkuItem == null) {
			return null;
		}
		Set<RepositoryItem> items = (Set<RepositoryItem>) pSkuItem
				.getPropertyValue(getCatalogProperties()
						.getParentProductsPropertyName());
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCatalogTools.getParentProducts method with Set of Parent Products : "
					+ items);
		}
		return items;
	}

	/**
	 * Method to Get Default Product item ie. 1st Product item from set of
	 * Products for provided SKU item.
	 * 
	 * @param pSkuItem
	 *            - Sku Repository Item
	 * @return - Repository Item - ParentProduct.
	 */
	@SuppressWarnings("unchecked")
	public RepositoryItem getDefaultParentProducts(RepositoryItem pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("Entered into TRUCatalogTools.getDefaultParentProducts method");
			logDebug("pSkuItem:" + pSkuItem);
		}
		RepositoryItem defaultProdItem = null;
		if (pSkuItem == null) {
			return defaultProdItem;
		}
		Set<RepositoryItem> items = (Set<RepositoryItem>) pSkuItem
				.getPropertyValue(getCatalogProperties()
						.getParentProductsPropertyName());
		if (items != null && !items.isEmpty()) {
			defaultProdItem = items.iterator().next();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCatalogTools.getDefaultParentProducts method with Set of Parent Products : "
					+ defaultProdItem);
		}
		return defaultProdItem;
	}

	/**
	 * This method is used to get the productId based on the UPC number.
	 * 
	 * @param pUPCNumber - UPCNumber
	 * @return - product number
	 */
	public Map<String,List<String>> getProductIdFromUPCNumber(String pUPCNumber) {
		RepositoryView skuView = null;
		Map<String,List<String>> productMap=new HashMap<String,List<String>>();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (!StringUtils.isEmpty(pUPCNumber)) {
		try {
			 skuView = getCatalog().getView(catalogProperties.getRegularSKUItemDescPropertyName());
			 QueryBuilder queryBuilder = skuView.getQueryBuilder();
			 QueryExpression propertyExpression = queryBuilder.createPropertyQueryExpression(catalogProperties.getUpcNumbers());
			 QueryExpression UpcExpression = queryBuilder.createConstantQueryExpression(pUPCNumber);
			 Query includesQuery = queryBuilder.createIncludesQuery(propertyExpression, UpcExpression);
			 includesQuery = removeQuerySiteConstraints(queryBuilder, includesQuery);
			 RepositoryItem[] skuItem = skuView.executeQuery(includesQuery);
				if (skuItem != null) {
					for (int count = TRUCommerceConstants.INT_ZERO; count < skuItem.length; count++) {
						Set<RepositoryItem> productIdsSet=null;
						productIdsSet =  (Set<RepositoryItem>) skuItem[count].getPropertyValue(getCatalogProperties().getParentProductsPropertyName());
						if(productIdsSet.isEmpty()){
							return productMap;
						}
						for (RepositoryItem productItem : productIdsSet) {
							String productId=productItem.getRepositoryId();
							List<RepositoryItem> childSKUs = (List<RepositoryItem>) productItem.getPropertyValue(catalogProperties.getChildSkusPropertyName());
							if(!childSKUs.isEmpty()){
								List<String> skuCodeListlocal = new ArrayList<String>();
								for (RepositoryItem sku : childSKUs) {
									if(sku.getRepositoryId().equals(skuItem[count].getRepositoryId())){
										skuCodeListlocal.add(sku.getRepositoryId());
									}
									
								}
								if(StringUtils.isNotBlank(productId)){
									productMap.put(productId, skuCodeListlocal);
								}
							}
						}
						
					}
					
				}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException occured", e);
			}
		}
	}
		return productMap;
	}
	
	/*
	*//**
	 * This method will populate the promotion on PDP page.
	 *
	 * @param pProductInfoVO the Product info vo
	 * @param pSiteId the Site id
	 * @return - ProductPromoInfoVO
	 * @throws RepositoryException the repository exception
	 *//*
	@SuppressWarnings("unchecked")
	public ProductPromoInfoVO populatePromotions(String pProductId) {
		RepositoryItem productItem = null;
		ProductPromoInfoVO promoInfoVo = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			productItem = findProduct(pProductId);
			if(productItem!=null){
				Set<String> promotionsIds = (Set<String>) productItem
						.getPropertyValue(catalogProperties.getProductPromotions());
				
				if (promotionsIds != null && !promotionsIds.isEmpty()) {
					promoInfoVo = new ProductPromoInfoVO();
					ArrayList<String> promotionDetails = new ArrayList<String>();
					ArrayList<String> promoDescription = new ArrayList<String>();
					
					int promoCount = TRUCommerceConstants.INT_ZERO;
					RepositoryItem promotionItem = null;
					TRUPromotionTools promotionTools = getPromotionTools();
					Repository promoRepository = promotionTools.getPromotions();
					TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools()
							.getPricingModelProperties();
					if (promoRepository != null) {
						Date now = getPromotionTools().getCurrentDate()
								.getTimeAsDate();
						for (String lPromoId : promotionsIds) {
							// checking promotion is expired or not
							promotionItem = promoRepository.getItem(lPromoId,
									pricingModelProperties
											.getPromotionItemDescName());
							if (promotionItem != null &&
									!promotionTools.checkPromotionExpiration(
											promotionItem, now)) {

								promoCount++;
								promotionDetails.add((String) promotionItem
										.getPropertyValue(catalogProperties
												.getPromotionDetails()));
								promoDescription.add((String) promotionItem
										.getPropertyValue(catalogProperties
												.getDescription()));
							}
						}
						promoInfoVo.setDetails(promotionDetails);
						promoInfoVo.setDescription(promoDescription);
						promoInfoVo.setPromotionCount(promoCount);
					}
				}
			}
			
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException : ::::::", e);
			}
		}
		return promoInfoVo;
	}*/

	/**
	 * This method used to populate thigsToKnow on PDP mobile service.
	 * 
	 * @param pProductInfoVO
	 *            - productInfoVO
	 * @param pSiteId
	 *            - SiteId
	 * @return pProductInfoVO - ProductInfoVO
	 * @throws RepositoryException
	 *             - RepositoryException
	 */
	public ProductInfoVO populateThingsToKnow(ProductInfoVO pProductInfoVO,
			String pSiteId) throws RepositoryException {
		String resourceValue = null;
		List<String> thingsToKnowList = new ArrayList<String>();
		if (pProductInfoVO.getDefaultSKU() != null &&
				((pProductInfoVO.getDefaultSKU().getCanBeGiftWrapped() != null && 
				!pProductInfoVO.getDefaultSKU().getCanBeGiftWrapped().equalsIgnoreCase(TRUCommerceConstants.GIFTWRAP_NO))||
						(pProductInfoVO.getBatteryRequired() != null && 
						!pProductInfoVO.getBatteryRequired().equalsIgnoreCase(TRUCommerceConstants.BATTERY_REQUIRED_NO)) || 
						pProductInfoVO.getDefaultSKU().getCollectionNames() != null)) {

			if (pProductInfoVO.getDefaultSKU().getChannelAvailability() != null
					&& pProductInfoVO.getDefaultSKU().getChannelAvailability().equalsIgnoreCase(TRUCommerceConstants.ONLINE_ONLY)) {
				resourceValue = getResourceBundleTools().getKeyValue(TRUCommerceConstants.THINGS_TO_KNOW_ONLINE_KEY,pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			if (pProductInfoVO.getDefaultSKU().getChannelAvailability().equalsIgnoreCase(TRUCommerceConstants.CHANNEL_AVAILABILITY_STATUS) || 
					pProductInfoVO.getDefaultSKU().getChannelAvailability().equalsIgnoreCase(TRUCommerceConstants.NOT_APPLICABLE) || pProductInfoVO
							.getDefaultSKU().getChannelAvailability().equalsIgnoreCase(TRUCommerceConstants.IN_STORE_ONLY)) {
				resourceValue = getResourceBundleTools().getKeyValue(
						TRUCommerceConstants.THINGS_TO_KNOW_STORE_KEY, pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			if (pProductInfoVO.getBatteryRequired() != null &&
					!pProductInfoVO.getBatteryRequired().equalsIgnoreCase(TRUCommerceConstants.BATTERY_REQUIRED_NO)) {
				resourceValue = getResourceBundleTools().getKeyValue(
						TRUCommerceConstants.THINGS_TO_KNOW_BATTERIES_KEY,
						pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			if (pProductInfoVO.getDefaultSKU().getCanBeGiftWrapped() != null
					&& pProductInfoVO
							.getDefaultSKU()
							.getCanBeGiftWrapped()
							.equalsIgnoreCase(TRUCommerceConstants.GIFTWRAP_YES)) {
				resourceValue = getResourceBundleTools()
						.getKeyValue(
								TRUCommerceConstants.THINGS_TO_KNOW_GIFTWRAP_AVAILABLE_KEY,
								pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			if (pProductInfoVO.getDefaultSKU().getCanBeGiftWrapped() != null
					&& pProductInfoVO.getDefaultSKU().getCanBeGiftWrapped()
							.equalsIgnoreCase(TRUCommerceConstants.GIFTWRAP_NO)) {
				resourceValue = getResourceBundleTools()
						.getKeyValue(
								TRUCommerceConstants.THINGS_TO_KNOW_GIFTWRAP_NOT_AVAILABLE_KEY,
								pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			if (pProductInfoVO.getDefaultSKU().isShipInOrigContainer()) {
				resourceValue = getResourceBundleTools()
						.getKeyValue(
								TRUCommerceConstants.THINGS_TO_KNOW_SHIPIN_KEY,
								pSiteId);
				thingsToKnowList.add(resourceValue);
			}
			pProductInfoVO.setThingsToKnow(thingsToKnowList);
		}

		return pProductInfoVO;
	}

	/**
	 * This method will check for the active flag based on the item and returns
	 * true/false.
	 * 
	 * @param pItem
	 *            the item
	 * @return {@code true} if is active, {@code false} otherwise
	 */
	public boolean isActive(RepositoryItem pItem) {
		if (isLoggingDebug()) {
			logDebug("Entering into class :[TRUCatalogTools]  method:[isActive]");
			vlogDebug("ProductItem:{0}", pItem);
		}
		boolean isActive = true;
		if (pItem == null) {
			return isActive;
		}
		// isActive = (Boolean)
		// pItem.getPropertyValue(catalogProperties.getDeletedPropertyName());
		if (isLoggingDebug()) {
			vlogDebug("isActive:{0}", isActive);
			logDebug("Exiting from class :[TRUCatalogTools]  method:[isActive]");
		}
		return isActive;
	}
	/**
	 * This Method is used for get the All classification attribute name items.
	 * 
	 * @param pClassificationId - Select classification Id
	 * @return allAttributeNames - All attribute names related to category
	 */
	@SuppressWarnings("unused")
	public Set<RepositoryItem> getClassificationAttributeNames(String pClassificationId) {

		if (pClassificationId == null) {
			return null;
		}
		RepositoryItem classificationItem = null;
		Set<RepositoryItem> classificationAttributeNameItems= null;
		try
		{
			if (isLoggingDebug()) {
				logDebug("BEGIN:: TRUCatalogTools.getClassificationAttributeNames(pClassificationId).");
			}
			if (isLoggingDebug()) {
				vlogDebug("pclassificationId ",pClassificationId);
			}
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			classificationItem=getCatalog().getItem(pClassificationId, catalogProperties.getClassificationItemDescPropertyName());
			classificationAttributeNameItems = (Set<RepositoryItem>) classificationItem.getPropertyValue(catalogProperties
					.getAttributeNamePropertyName());
			
		  }catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException occured", repositoryException);
				}
			}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogTools.getClassificationAttributeNames(pClassificationId).");
		}
		return classificationAttributeNameItems;
	}

	/**
	 * This method is used to create the product attributes and the value of those attribute and then return those values in map.
	 * 
	 * @param pProductList
	 *            -ProductList to get products.
	 * @param pClassificationAttributeNamesList
	 *            - reference to ClassificationAttributeNamesList.
	 * @return attriValueMap-map of attributes and values
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map createAttributeAndValueMap(List<RepositoryItem> pProductList,Set<RepositoryItem> pClassificationAttributeNamesList) {
		if (isLoggingDebug()) {
			logDebug("Entering into class :[TRUCatalogTools]  method:[createAttributeAndValueMap]");
		}
		Map attriValueMap = new HashMap();
		List<Map<String, String>> productAtributeValues = new ArrayList<Map<String, String>>();
		Map<String, Map<String, String>> productAtributeValuesMap = new HashMap<String, Map<String, String>>();
		if (pProductList != null && !pProductList.isEmpty()) {
			HashMap<String, String> valueMap;
			List<String> attributeList;
			String attributeName = null;
			String attributeValue = null;
			String attributeGroupId = null;
			RepositoryItem attributeNameItem = null;
			RepositoryItem attributeGroup = null;
			Set<String> attributeNames = new HashSet<String>();
			HashMap<String, List<String>> attriMap = new HashMap<String, List<String>>();
			List<RepositoryItem> productAttributes = new ArrayList<RepositoryItem>();
			Set<RepositoryItem> attributeGroupItems = new HashSet<RepositoryItem>();
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			for (RepositoryItem product : pProductList) {
				productAttributes = (List<RepositoryItem>) product
						.getPropertyValue(catalogProperties
								.getProductAttributesPropertyName());
				valueMap = new HashMap<String, String>();
				if (productAttributes != null && !productAttributes.isEmpty()) {
					for (RepositoryItem productAttribute : productAttributes) {
						attributeNameItem = (RepositoryItem) productAttribute
								.getPropertyValue(catalogProperties
										.getAttributeNamePropertyName());
						if (attributeNameItem != null) {
							attributeName = (String) attributeNameItem
									.getPropertyValue(catalogProperties
											.getAttributeNamePropertyName());
							for(RepositoryItem classificationAttributeNameItem:pClassificationAttributeNamesList)
 {
								String classificationAttributeName = (String) classificationAttributeNameItem
										.getPropertyValue(catalogProperties.getAttributeNamePropertyName());
								if (classificationAttributeName.equalsIgnoreCase(attributeName)) {
									attributeValue = (String) productAttribute.getPropertyValue(catalogProperties
											.getAttributeValuePropertyName());
									attributeGroup = (RepositoryItem) attributeNameItem.getPropertyValue(catalogProperties
											.getAttributeGroupPropertyName());
									if (attributeGroup != null) {
										attributeGroupId = (String) attributeGroup.getPropertyValue(catalogProperties
												.getAttributeGroupIdPropertyName());
										attributeList = attriMap.get(attributeGroupId);
										if (attributeList != null && !attributeList.isEmpty() &&
											attributeList.contains(attributeName) == false) {
											attributeList.add(attributeName);
										} else {
											attributeList = new ArrayList<String>();
											attributeList.add(attributeName);
											attriMap.put(attributeGroupId, attributeList);
											attributeGroupItems.add(attributeGroup);
										}
										if (isLoggingDebug()) {
											vlogDebug("attributeGroupId : {0}  attributeValue : {1} attributeName : {2}", attributeGroupId,
													attributeName);
										}
									}
									valueMap.put(attributeName, attributeValue);
									attributeNames.add(attributeName);
								}
							}
						}
					}
				}
				productAtributeValues.add(valueMap);
				productAtributeValuesMap.put(product.getRepositoryId(),
						valueMap);
			}
			attriValueMap.put(TRUCommerceConstants.PRODUCT_ATTRIBUTE_VALUES,
					productAtributeValues);
			attriValueMap.put(TRUCommerceConstants.ATTRIBUTE_MAP, attriMap);
			attriValueMap.put(TRUCommerceConstants.ATTRIBUTE_GROUP_ITEMS,
					attributeGroupItems);
			attriValueMap.put(
					TRUCommerceConstants.ATTRIBUTE_MAP_FOREACH_PRODUCT,
					productAtributeValuesMap);
			attriValueMap.put(TRUCommerceConstants.SET_OF_ATTRIBUTENAME,
					attributeNames);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into class :[TRUCatalogTools]  method:[createAttributeAndValueMap]");
		}
		return attriValueMap;
	}

	/**
	 * This method has taken the products as input whatever user has selected
	 * for comparison. This method will check that products are available in the
	 * repository or not, if the product item is available in repository then
	 * product item has added into the list, otherwise the product will not be
	 * added into list. Finally this list of product items will be returned to
	 * calling method.
	 * 
	 * @param pProductComparisonList
	 *            - used to get the product items.
	 * @param pProductIdSkuItemMap
	 *            - reference to ProductIdSkuItemMap.
	 * @param pCategoryId - pCategoryId
	 *     
	 * @return productItems - List of product item
	 */
	public List<RepositoryItem> getProductItems(List pProductComparisonList, Map<String, String> pProductIdSkuItemMap,String pCategoryId) {
		if (isLoggingDebug()) {
			logDebug("Entering into class :[TRUCatalogTools]  method:[getProductItems]");
		}
		if (pProductComparisonList == null || pProductComparisonList.isEmpty()) {
			return null;
		}
		if(pProductIdSkuItemMap == null){
			pProductIdSkuItemMap= new HashMap<String, String>();
		}
		List<RepositoryItem> productItems = new ArrayList<RepositoryItem>();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		RepositoryItem productItem = null;
		RepositoryItem skuItem = null;
		RepositoryItem categoryItem = null;
		int listSize = pProductComparisonList.size();
		Object bean = null;
		if (isLoggingDebug()) {
			vlogDebug("listSize {0}: ", listSize);
		}
		int count= TRUCommerceConstants.INT_ZERO;
		for (int index = 0; index < listSize; index++) {
			// Fetching the bean from the pProductItems.
			bean = pProductComparisonList.get(index);
			try {
				// Fetching the bean from the catalog.
				productItem = (RepositoryItem) DynamicBeans
						.getSubPropertyValue(bean, getCatalogProperties()
								.getProductItemName());
				skuItem = (RepositoryItem) DynamicBeans
						.getSubPropertyValue(bean, getCatalogProperties()
								.getSkuItemName());
				categoryItem = (RepositoryItem) DynamicBeans
						.getSubPropertyValue(bean, catalogProperties.getCategoryItemName());
				
			} catch (PropertyNotFoundException propertyNotFoundException) {
				if (isLoggingError()) {
					logError("PropertyNotFoundException in getProductsItems()",
							propertyNotFoundException);
				}
			}
			// Adding it to the list.
			if(categoryItem != null && categoryItem.getRepositoryId().equals(pCategoryId)){
			if (productItem != null && isActive(productItem)) {
				productItems.add(productItem);
				if(skuItem !=null){
				String key= productItem.getRepositoryId()+TRUCommerceConstants.UNDER_SCORE+count;
				pProductIdSkuItemMap.put(key, skuItem.getRepositoryId());
				}
			}
			count++;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into class :[TRUCatalogTools]  method:[getProductItems]");
		}
		return productItems;
	}
	
	/**
	 * To clear the compare Items by categoryId.
	 * @param pProductComparisonList - list of compare items
	 * @param pCategoryId - category Id
	 * @return itemlist - list of compare items
	 */
	public List clearedCompareItemListBasedOnCategory(List pProductComparisonList,String pCategoryId){
		vlogDebug("BEGIN :[TRUCatalogTools]  method:[clearedCompareItemListBasedOnCategory],pCategoryId "+pCategoryId);
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pProductComparisonList == null || pProductComparisonList.isEmpty()) {
			return null;
		}
		if(StringUtils.isBlank(pCategoryId)){
			return pProductComparisonList;
		}
		List itemlist=  new ArrayList();
		int listSize = pProductComparisonList.size();
		RepositoryItem categoryItem = null;
		Object bean = null;
		if (isLoggingDebug()) {
			vlogDebug("listSize {0}: ", listSize);
		}
		
		for (int index = 0; index < listSize; index++) {
			// Fetching the bean from the pProductItems.
			bean = pProductComparisonList.get(index);
			try {
				categoryItem = (RepositoryItem) DynamicBeans
						.getSubPropertyValue(bean, catalogProperties.getCategoryItemName());
				if(categoryItem!= null && !categoryItem.getRepositoryId().equals(pCategoryId)){
					itemlist.add(bean);
				}
			} catch (PropertyNotFoundException e) {
				if (isLoggingError()) {
					logError("PropertyNotFoundException in TRUCatalogTools @method: clearedCompareItemListBasedOnCategory",e);
				}
			}
		}
		vlogDebug("END :[TRUCatalogTools]  method:[clearedCompareItemListBasedOnCategory],itemlist "+itemlist);
		return itemlist;
	}
	

	/**
	 * Gets the non merch sku item.
	 * 
	 * @param pSkuTypeName
	 *            the sku type name
	 * @return the non merch sku item
	 */
	public RepositoryItem getDonationOrBPPSKUItem(String pSkuTypeName) {
		if (isLoggingDebug()) {
			logDebug("START: @method getDonationOrBPPSKUItem @class: TRUCatalogTools");
			vlogDebug("pSkuTypeName : {0}", pSkuTypeName);
		}
		Repository catalog = getCatalog();
		if (catalog != null) {
			RepositoryItemDescriptor regularSKU;
			try {
				regularSKU = catalog
						.getItemDescriptor(TRUConstants.REGULAR_SKU);
				RepositoryItem[] regularSKUItems = null;
				if (regularSKU != null) {
					RepositoryView repositoryView = regularSKU
							.getRepositoryView();
					if (repositoryView != null) {
						QueryBuilder queryBuilder = repositoryView
								.getQueryBuilder();
						if (isLoggingDebug()) {
							logDebug("building a query for getting list of regularSKUs from repositoryView: ");
						}
						QueryExpression paramProperty = queryBuilder
								.createPropertyQueryExpression(TRUCommerceConstants.SUB_TYPE);
						QueryExpression paramValue = queryBuilder
								.createConstantQueryExpression(pSkuTypeName);
						Query query = queryBuilder.createComparisonQuery(
								paramProperty, paramValue, QueryBuilder.EQUALS);
						regularSKUItems = repositoryView.executeQuery(query);
						if (isLoggingDebug()) {
							logDebug("Retrieving the array of regularSKUItems : "
									+ regularSKUItems);
						}
						if (regularSKUItems != null
								&& regularSKUItems.length > TRUCommerceConstants.INT_ZERO) {
							return regularSKUItems[0];
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError(
							"Repository Exception in TRUCatalogTools @method: getDonationOrBPPSKUItem",
							repositoryException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: @method getDonationOrBPPSKUItem @class: TRUCatalogTools");
		}
		return null;
	}

	/**
	 * Gets the bpp items based on bpp name.
	 * 
	 * @param pBppName
	 *            the bpp name
	 * @param pSkuPrice
	 *            the sku price
	 * @return the bpp items based on bpp name
	 */
	public RepositoryItem[] getBppItemsBasedOnBppName(String pBppName,
			double pSkuPrice) {
		if (isLoggingDebug()) {
			logDebug("START: @method getBppItemsBasedOnBppName() @class: TRUCatalogTools");
		}
		Repository catalog = getCatalog();
		RepositoryItem[] bppItems = null;
		if (catalog != null) {
			RepositoryItemDescriptor bppItem;
			try {
				bppItem = catalog.getItemDescriptor(TRUCommerceConstants.BPP);
				if (bppItem != null) {
					RepositoryView repositoryView = bppItem.getRepositoryView();
					if (repositoryView != null) {
						if (isLoggingDebug()) {
							logDebug("building a query for getting list of bpp items from repositoryView: ");
						}
						QueryBuilder queryBuilder = repositoryView
								.getQueryBuilder();
						QueryExpression bppNameProperty = queryBuilder
								.createPropertyQueryExpression(TRUCommerceConstants.BPP_NAME);
						QueryExpression bppNameValue = queryBuilder
								.createConstantQueryExpression(pBppName);
						Query bppNameEqualquery = queryBuilder
								.createComparisonQuery(bppNameProperty,
										bppNameValue, QueryBuilder.EQUALS);

						QueryExpression lowParamProperty = queryBuilder
								.createPropertyQueryExpression(TRUCommerceConstants.BPP_LOW_PROPERTY);
						QueryExpression lowParamValue = queryBuilder
								.createConstantQueryExpression(pSkuPrice);
						Query bppLowPriceQuery = queryBuilder
								.createComparisonQuery(lowParamProperty,
										lowParamValue,
										QueryBuilder.LESS_THAN_OR_EQUALS);

						QueryExpression highParamProperty = queryBuilder
								.createPropertyQueryExpression(TRUCommerceConstants.BPP_HIGH_PROPERTY);
						QueryExpression highParamValue = queryBuilder
								.createConstantQueryExpression(pSkuPrice);
						Query bppHighPriceQuery = queryBuilder
								.createComparisonQuery(highParamProperty,
										highParamValue,
										QueryBuilder.GREATER_THAN_OR_EQUALS);

						Query[] finalQuery = { bppNameEqualquery,
								bppLowPriceQuery, bppHighPriceQuery };
						Query andQuery = queryBuilder
								.createAndQuery(finalQuery);

						bppItems = repositoryView.executeQuery(andQuery);
						if (bppItems != null
								&& bppItems.length > TRUCommerceConstants.INT_ZERO) {
							if (isLoggingDebug()) {
								logDebug("Retrieving the array of bppItems : "
										+ bppItems);
							}
							return bppItems;
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError(
							"Repository Exception in TRUCatalogTools @method: getBppItemsBasedOnBppName()",
							repositoryException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: @method getBppItemsBasedOnBppName() @class: TRUCatalogTools");
		}
		return bppItems;
	}

	/**
	 * Gets the active child skus from product id.
	 * 
	 * @param pProductId
	 *            the product id
	 * @return the active child skus from product id
	 */
	public List<RepositoryItem> getActiveChildSkusFromProductId(
			String pProductId) {
		List<RepositoryItem> activeSKUs = null;
		RepositoryItem productItem = null;
		try {
			productItem = findProduct(pProductId);
		} catch (RepositoryException repoEx) {
			vlogError(
					"Repository Exception occured while getting the request parameters from shoplocal with exception : {0}",
					repoEx);
		}
		if (productItem != null) {
			List<RepositoryItem> listOfSku = (List<RepositoryItem>) productItem.getPropertyValue(getCatalogProperties().getChildSkusPropertyName());
			activeSKUs = getActiveChildSKUs(listOfSku, true,TRUCommerceConstants.TYPE_PRODUCT,null,productItem.getRepositoryId());
		}
		return activeSKUs;
	}

	/**
	 * This method used to determine the product Id is PDP or Collection product.
	 *
	 * @param pProductId            -productid
	 * @return productType -product
	 * @throws RepositoryException             -rep exception
	 */
	public String getProductType(String pProductId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("START: @method getProductType() @class: TRUCatalogTools");
		}
		String productType = null;
		RepositoryItem itemType = findProduct(pProductId);
		if (itemType != null) {
			productType = itemType.getItemDescriptor().getItemDescriptorName();
		}

		if (isLoggingDebug()) {
			logDebug("END: @method getProductType() @class: TRUCatalogTools");
		}
		return productType;
	}

	/**
	 * This method retrieve catalogId based on the siteId.
	 *
	 * @param pSiteId            siteId
	 * @return string
	 */
	public String getCatalogId(String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("START: @method getCatalogId() @class: TRUCatalogTools");
		}
		Site site = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			site = getSiteContextManager().getSite(pSiteId);
		} catch (SiteContextException siteException) {
			if (isLoggingError()) {
				vlogError(
						"SiteContextException Exception occured while getting site name : {0}",
						siteException);
			}
		}
		
		RepositoryItem catalogId = (RepositoryItem) site
				.getPropertyValue(catalogProperties.getDefaultCatalog());
		if (isLoggingDebug()) {
			logDebug("END: @method getCatalogId() @class: TRUCatalogTools");
		}
		if (catalogId != null) {
			return (String) catalogId.getPropertyValue(catalogProperties.getSiteId());
		} else {
			return null;
		}
		
	}

	/**
	 * This method is used to fetch the.
	 *
	 * @param pProductOnlinePID            - The Product online PId
	 * @return the boolean value
	 */
	@SuppressWarnings("static-access")
	public RepositoryItem[] getSKUFromOnlinePID(String pProductOnlinePID) {
		RepositoryItem[] skuList = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		try {
			Repository skuRepository = getCatalog();
			RepositoryView skusReporitoryView = skuRepository
					.getView(catalogProperties.getSkuItemName());
			QueryBuilder skuQueryBuilder = skusReporitoryView.getQueryBuilder();
			QueryExpression onlinePIDPropertyName = skuQueryBuilder
					.createPropertyQueryExpression(TRUCommerceConstants.ONLINE_PID);
			QueryExpression onlinePIDValue = skuQueryBuilder
					.createConstantQueryExpression(pProductOnlinePID);
			Query onlinePIDQuery = skuQueryBuilder.createComparisonQuery(
					onlinePIDPropertyName, onlinePIDValue,
					skuQueryBuilder.EQUALS);
			if (onlinePIDQuery instanceof Clause) {
				((Clause) onlinePIDQuery).setContextFilterApplied(true);
			}
			skuList = skusReporitoryView.executeQuery(onlinePIDQuery);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e, "RepositoryException occured in  TRUCatalogTools: @Method: getSKUFromOnlinePID(): ");
			}
		}
		return skuList;
	}

	/**
	 * To create online pdpURL from online PID.
	 *
	 * @param pOnlinePid - onlinePid
	 * @return productURL  -productURL
	 */
	@SuppressWarnings("unchecked")
	public String createPDPUrlFromOnlinePid(String pOnlinePid) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUCustomCatalogTools.createPDPUrlFromOnlinePid method() pOnlinePid:: {0}  ",pOnlinePid);
		}
		StringBuilder productURL = new StringBuilder();
		RepositoryItem[] skuList = getSKUFromOnlinePID(pOnlinePid);
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		
		// Filter invalid SKU item, for stylized Product .
		if(isFilterSkuList() && skuList != null && skuList.length > TRUCommerceConstants.INT_ONE){
			skuList = filterInvalidSkuItem(skuList);
		}// End of filter sku.
		if (skuList == null || skuList.length <= TRUConstants.ZERO
				&& skuList[0] == null) {
			return null;
		}
		RepositoryItem currentSku = skuList[0];
		String skuType = (String) currentSku.getPropertyValue(catalogProperties.getSkuType());
		String skuId = currentSku.getRepositoryId();
		String productId = null;
		String catalogId = null;
		Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) currentSku.getPropertyValue(catalogProperties.getParentProductsPropertyName());
		Set<RepositoryItem> computedCatalogs = (Set<RepositoryItem>) currentSku.getPropertyValue(catalogProperties.getComputedCatalogsPropertyName());

		if (parentProducts != null && !parentProducts.isEmpty()) {
			for (RepositoryItem parentProduct : parentProducts) {
				if (parentProduct != null) {
					productId = parentProduct.getRepositoryId();
					break;
				}
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("TRUCustomCatalogTools.createPDPUrlFromOnlinePid method() parentProducts - {0} :: Parent productId - {1}  ",parentProducts,productId);
		}
		if (computedCatalogs != null && !computedCatalogs.isEmpty()) {
			for (RepositoryItem catalog : computedCatalogs) {
				if (catalog != null) {
					catalogId = catalog.getRepositoryId();
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCustomCatalogTools.createPDPUrlFromOnlinePid method() computedCatalogs - {0} :: catalogId {1} ",computedCatalogs,catalogId);
		}
		productURL.append(skuType).append(TRUCommerceConstants.HYPHEN)
				.append(skuId).append(TRUCommerceConstants.DOUBLE_DOT)
				.append(productId);
		productURL.append(TRUCommerceConstants.DOT).append(catalogId)
				.append(TRUCommerceConstants.DOT)
				.append(TRUCommerceConstants.DEFAULT_SITE_LOCALE);
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUCustomCatalogTools.createPDPUrlFromOnlinePid method() productURL:: {0}   ",productURL.toString());
		}
		return productURL.toString();
	}

	/**
	 * This method is used to Filter invalid SKU item, if one onlinePID returns more than one sku.
	 * @param pSkuList
	 * 			- the sku List
	 * @return RepositoryItem[] 
	 * 				- the RepositoryItem Array.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private RepositoryItem[] filterInvalidSkuItem(RepositoryItem[] pSkuList){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUCustomCatalogTools.filterInvalidSkuItem method() ");
		}
		RepositoryItem[] skuListArray = null; 
		List <RepositoryItem> skuAsList = null;
		List <RepositoryItem> skuListNew = new ArrayList<RepositoryItem>();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		
		if(pSkuList != null && pSkuList.length > TRUCommerceConstants.INT_ZERO){
			skuAsList = Arrays.asList(pSkuList);
			if (isLoggingDebug()) {
				vlogDebug("TRUCustomCatalogTools.filterInvalidSkuItem method() Intial skuAsList :: {0}  ",skuAsList);
			}
		}
		if(skuAsList != null && !skuAsList.isEmpty()){
			for (RepositoryItem skuItem : skuAsList) {
				if(skuItem != null){
					String skuType = (String) skuItem.getPropertyValue(catalogProperties.getSkuType());
					if (isLoggingDebug()) {
						vlogDebug(" TRUCustomCatalogTools.filterInvalidSkuItem method() skuType :: {0}  ",skuType);
					}
					boolean isDeletedFlag =	(boolean)skuItem.getPropertyValue(catalogProperties.getIsSkuDeleted());
					boolean isWebDisplayFlag = (boolean)skuItem.getPropertyValue(catalogProperties.getWebDisplayFlag());
					@SuppressWarnings("rawtypes")
					Set siteIds = (Set)skuItem.getPropertyValue(catalogProperties.getSiteIds());
					Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) skuItem.getPropertyValue(catalogProperties.getParentProductsPropertyName());
					Set<RepositoryItem> computedCatalogs = (Set<RepositoryItem>) skuItem.getPropertyValue(catalogProperties.getComputedCatalogsPropertyName());
					boolean superDisplayFlag = false;
					if(skuItem.getPropertyValue(catalogProperties.getSuperDisplayFlag()) == null) {
						superDisplayFlag = true;
					} else {
						superDisplayFlag = (boolean) skuItem.getPropertyValue(catalogProperties.getSuperDisplayFlag());
					}
					
					if(isDeletedFlag || !isWebDisplayFlag || siteIds.isEmpty() || !superDisplayFlag || computedCatalogs == null  || computedCatalogs.isEmpty() || parentProducts == null || parentProducts.isEmpty()){
						if (isLoggingDebug()) {
							vlogDebug("TRUCustomCatalogTools.filterInvalidSkuItem method() ---- isDeleted - {0} :: webDisplay - {1} :: siteIds - {2} :: superDisplayFlag - {3} :: computedCatalogs - {4} :: parentProducts - {5} ",isDeletedFlag,isWebDisplayFlag,siteIds,superDisplayFlag,computedCatalogs,parentProducts);
						}	
						 continue;
					}
					else{
						skuListNew.add(skuItem);
						break;
					}	
				}
			}	
		}
		if(skuListNew != null && !skuListNew.isEmpty())	{
			if (isLoggingDebug()) {
				vlogDebug(" TRUCustomCatalogTools.filterInvalidSkuItem method() Intial skuListNew after Filter :: {0}  ",skuListNew);
			}
			skuListArray = new RepositoryItem[skuListNew.size()];
			skuListArray = skuListNew.toArray(skuListArray);
			pSkuList = skuListArray;
		} 
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUCustomCatalogTools.filterInvalidSkuItem method() ");
		}
		return pSkuList;
	}
	/**
	 * To get the product list from online PID.
	 * @param pOnlinePIds OnlinePIds
	 * @return onlineProductIdList onlineProductIdList
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getProductIdsFromOnlinePid(String pOnlinePIds) {
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List<String> onlinePIdList = null;
		Set<String> onlineProductIdSet=new HashSet<String>();
		List<String> onlineProductIdList = new ArrayList<String>();
		onlinePIdList = spliteProductId(pOnlinePIds);
		for (String onlinePid : onlinePIdList) {
			RepositoryItem[] skuList = getSKUFromOnlinePID(onlinePid);
			if(skuList == null || skuList.length == TRUCommerceConstants.INT_ZERO || skuList[TRUCommerceConstants.INT_ZERO] == null) {
				continue;
			}
		for (RepositoryItem skuItem : skuList) {
			String productId = null;
			Set<RepositoryItem> parentProducts =  (Set<RepositoryItem>) skuItem.getPropertyValue(catalogProperties.getParentProductsPropertyName());
			if(parentProducts != null && !parentProducts.isEmpty()) {
				Iterator iter = parentProducts.iterator();
				RepositoryItem productItem = (RepositoryItem) iter.next();
				productId=productItem.getRepositoryId();
					if (StringUtils.isNotBlank(productId)) {
					onlineProductIdSet.add(productId);
					}
				}
			}
		}
		onlineProductIdList=new ArrayList<String>(onlineProductIdSet);
		return onlineProductIdList;
	}
	/**
	 * This method will split the skuCode or upc or productId from the request.
	 * @param pSpliteIds SpliteIds
	 * @return productIdList productIdList
	 */
		public List<String> spliteProductId(String pSpliteIds) {
			if (isLoggingDebug()) {
				logDebug("BEGIN: @method spliteProductId() @class: TRUCatalogTools" +pSpliteIds);
			}
			String sliteId = null;
			String[] productIdArray;
			List<String> productIdList = new ArrayList<String>();
			sliteId = pSpliteIds.replaceAll(TRUCommerceConstants.OPEN, TRUCommerceConstants.EMPTY_SPACE).replaceAll(TRUCommerceConstants.CLOSE, TRUCommerceConstants.EMPTY_SPACE);
			productIdArray = sliteId.split(TRUCommerceConstants.SPLIT_PIPE);
			productIdList = Arrays.asList(productIdArray);
			if (isLoggingDebug()) {
				logDebug("END: @method spliteProductId() @class: TRUCatalogTools");
			}
			return productIdList;
		}
	/**
	 * Gets the catalog configuration.
	 * 
	 * @return the catalogConfiguration
	 */
	public TRUConfiguration getCatalogConfiguration() {
		return mCatalogConfiguration;
	}

	/**
	 * This method will return the productId from OnlinePID.
	 *
	 * @param pOnlineId String
	 * @return productId
	 */
	@SuppressWarnings("unchecked")
	public String getProductIdFromOnlinePID(String pOnlineId) {
		if (isLoggingDebug()) {
			logDebug("START: @method getProductIdFromOnlinePid() @class: TRUCatalogTools");
		}
		String productId = null;
		RepositoryItem firstSkuItem = null;
		RepositoryItem firstProductItem = null;
		if (pOnlineId == null) {
			return productId;
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		RepositoryItem[] SKUsFromOnlinePID = getSKUFromOnlinePID(pOnlineId);
		if (SKUsFromOnlinePID != null && SKUsFromOnlinePID.length > TRUCommerceConstants.INT_ZERO) {
			firstSkuItem = SKUsFromOnlinePID[TRUCommerceConstants.INT_ZERO];
			Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) firstSkuItem.getPropertyValue(catalogProperties
					.getParentProductsPropertyName());
			if (parentProducts != null && !parentProducts.isEmpty()) {
				firstProductItem = parentProducts.iterator().next();
				productId = firstProductItem.getRepositoryId();
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: @method getProductIdFromOnlinePID() @class: TRUCatalogTools");
		}
		return productId;
	}
	
	/**
	 * This method will return the skuId from OnlinePID.
	 *
	 * @param pOnlineId String
	 * @return skuId
	 */
	@SuppressWarnings("unchecked")
	public String getSkuIdFromOnlinePID(String pOnlineId) {
		if (isLoggingDebug()) {
			logDebug("START: @method getSkuIdFromOnlinePID() @class: TRUCatalogTools");
		}
		String skuId = null;
		RepositoryItem firstSkuItem = null;
		if (pOnlineId == null) {
			return skuId;
		}
		RepositoryItem[] SKUsFromOnlinePID = getSKUFromOnlinePID(pOnlineId);
		if (SKUsFromOnlinePID != null && SKUsFromOnlinePID.length > TRUCommerceConstants.INT_ZERO) {
			firstSkuItem = SKUsFromOnlinePID[TRUCommerceConstants.INT_ZERO];
			skuId=firstSkuItem.getRepositoryId();
		}
		if (isLoggingDebug()) {
			logDebug("END: @method getSkuIdFromOnlinePID() @class: TRUCatalogTools");
		}
		return skuId;
	}
	
	/**
	 * Sets the catalog configuration.
	 * 
	 * @param pCatalogConfiguration
	 *            the catalogConfiguration to set
	 */
	public void setCatalogConfiguration(TRUConfiguration pCatalogConfiguration) {
		mCatalogConfiguration = pCatalogConfiguration;
	}

	/**
	 * Gets the promotion tools.
	 * 
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 * 
	 * @param pPromotionTools
	 *            the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	/**
	 * Gets the resource bundle tools.
	 * 
	 * @return the resourceBundleTools
	 */
	public IResourceBundleTools getResourceBundleTools() {
		return mResourceBundleTools;
	}

	/**
	 * Sets the resource bundle tools.
	 * 
	 * @param pResourceBundleTools
	 *            the resourceBundleTools to set
	 */
	public void setResourceBundleTools(IResourceBundleTools pResourceBundleTools) {
		mResourceBundleTools = pResourceBundleTools;
	}

	/**
	 * Gets the bpp descriptor name.
	 * 
	 * @return the bpp descriptor name
	 */
	public String getBppDescriptorName() {
		return mBppDescriptorName;
	}

	/**
	 * Sets the bpp descriptor name.
	 * 
	 * @param pBppDescriptorName
	 *            the new bpp descriptor name
	 */
	public void setBppDescriptorName(String pBppDescriptorName) {
		mBppDescriptorName = pBppDescriptorName;
	}

	/**
	 * Gets the site context manager.
	 *
	 * @return the mSiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the site context manager.
	 *
	 * @param pSiteContextManager            the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		this.mSiteContextManager = pSiteContextManager;
	}
	
	/**
	 * To get the type of product from the product Id.
	 *
	 * @param pOnlinePIds - pOnlinePIds
	 * @return type - type
	 */
	public String toFindTheProductTypeFromInput(List<String> pOnlinePIds) {
		if (isLoggingDebug()) {
			logDebug("Start: @method toFindTheProductTypeFromInput() @class: TRUCatalogTools");
		}
		String type = TRUCommerceConstants.NORMAL_PRODUCT;
		String productType = TRUCommerceConstants.NORMAL_PRODUCT;
		if (pOnlinePIds != null && !pOnlinePIds.isEmpty()) {
			for (String id : pOnlinePIds) {		
				RepositoryItem[] skuList = getSKUFromOnlinePID(id);
				RepositoryItem productItem=null;
				
				if(skuList!=null && skuList.length>TRUCommerceConstants.INT_ZERO) {
					return type;
				}
				try {
					productItem = findProduct(id);
					if (productType != null && productItem!=null) {
						productType = productItem.getItemDescriptor().getItemDescriptorName();	
					}
				} catch (RepositoryException e) {
					vlogError("ERROR:@method toFindTheProductTypeFromInput() @class: TRUCatalogTools", e);
				}
				if (productItem != null && productType.equalsIgnoreCase(TRUCommerceConstants.COLLECTION_PRODUCT)) {
					type=TRUCommerceConstants.COLLECTION_PRODUCT;
					return type;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: @method toFindTheProductTypeFromInput() @class: TRUCatalogTools");
		}
		return type;
	}

	/**
	 * to convert it into json object.
	 *
	 * @param pProductInfo - pProductInfo
	 * @return mapJsonStr - mapJsonStr
	 */
	public String convertVOToJsonObject(ProductInfoJsonVO pProductInfo) {
			vlogDebug("Start: @method convertVOToJsonObject() @class: TRUCatalogTools");
			Gson gson=new Gson();
			String mapJsonStr=gson.toJson(pProductInfo);
			vlogDebug("END: @method convertVOToJsonObject() @class: TRUCatalogTools");
		return mapJsonStr;
	}

	/**
	 * get the default sku from selected sku.
	 * @param pProductInfo - pProductInfo
	 * @param pSkuId - pSkuId
	 * @return defaultSku - defaultSku
	 */
	public SKUInfoVO getDefaultFromSelectedSku(ProductInfoVO pProductInfo,String pSkuId) {
		vlogDebug("Start: @method getDefaultFromSelectedSku() @class: TRUCatalogTools");
		SKUInfoVO defaultSku=null;
		if(pProductInfo.getActiveSKUsList() !=null){
			for (SKUInfoVO sKUInfoVO : pProductInfo.getActiveSKUsList()) {
				if(sKUInfoVO.getId().equals(pSkuId)){
					defaultSku=sKUInfoVO;
					break;
				}
				
			}
		}
		vlogDebug("END: @method getDefaultFromSelectedSku() @class: TRUCatalogTools");
		return defaultSku;
	}

	/**
	 * to get the resource bundle value.
	 *
	 * @param pKeys - pKeys
	 * @return keyMessageList - keyMessageList
	 */
	public List<ResourceBundleVO> getResourceBundleMessage(List<String> pKeys){
		List<ResourceBundleVO> keyMessageList = new ArrayList<ResourceBundleVO>();
		ResourceBundleVO resourceBundle;
		String key;
		String message;
		ListIterator<String> iterate=pKeys.listIterator();
		String siteId=SiteContextManager.getCurrentSiteId();
		if(StringUtils.isNotBlank(siteId)){
			while(iterate.hasNext()){
				try {
					resourceBundle = new ResourceBundleVO();
					key=iterate.next();
					resourceBundle.setKey(key);
					message=getResourceBundleTools().getKeyValue(key, siteId);
					if(message.contains(TRUCommerceConstants.NOT_FOUND_IN_REPOSITORY)){
						resourceBundle.setMessage(key);
					}else{
						resourceBundle.setMessage(message);
					}
					keyMessageList.add(resourceBundle);
				} catch (RepositoryException e) {
					vlogError("ERROR:@method getResourceBundleMessage() @class: TRUCatalogTools", e);
				}
			}
		}
		return keyMessageList;
	}

	/**
	 * this method is to get the product Info-json-vo from product-infovo.
	 * @param pProductInfo - pProductInfo
	 * @return productInfoJsonVO - productInfoJsonVO
	 */
	public ProductInfoJsonVO populateProductJsonVO(ProductInfoVO pProductInfo) {
		vlogDebug("BEGIN: @method populateProductJsonVO() @class: TRUCatalogTools, pProductInfo::"+pProductInfo);
		ProductInfoJsonVO productInfoJsonVO = new ProductInfoJsonVO();
		if(pProductInfo!=null){
		productInfoJsonVO.setActive(pProductInfo.isActive());
		//productInfoJsonVO.setChannelAvailability(pProductInfo.getChannelAvailability());
		productInfoJsonVO.setColorCodeList(pProductInfo.getColorCodeList());
		productInfoJsonVO.setColorSizeVariantsAvailableStatus(pProductInfo.getColorSizeVariantsAvailableStatus());
		productInfoJsonVO.setDisplayName(pProductInfo.getDisplayName());
		if(pProductInfo.getInStockSKUsList()!=null){
			productInfoJsonVO.setInStockSKUsListLength(pProductInfo.getInStockSKUsList().size());
		}
		//productInfoJsonVO.setIspu(pProductInfo.getIspu());
		productInfoJsonVO.setRusItemNumber(pProductInfo.getRusItemNumber());
		productInfoJsonVO.setJuvenileSizesList(pProductInfo.getJuvenileSizesList());
		productInfoJsonVO.setProductId(pProductInfo.getProductId());
		productInfoJsonVO.setProductStatus(pProductInfo.getProductStatus());
		productInfoJsonVO.setProductType(pProductInfo.getProductType());
		productInfoJsonVO.setSfs(pProductInfo.getSfs());
		List<SKUInfoJsonVO> activeSKUsListJson= new ArrayList<SKUInfoJsonVO>();
		SKUInfoJsonVO sKUInfoJsonVO=null;
		List<SKUInfoVO> activeSKUsList= pProductInfo.getChildSKUsList();
		if(activeSKUsList!=null){
			for (SKUInfoVO skuInfoVO : activeSKUsList) {
				sKUInfoJsonVO=populateSkuInfoJsonVO(skuInfoVO);
				activeSKUsListJson.add(sKUInfoJsonVO);
			}
		}
		productInfoJsonVO.setActiveSKUsList(activeSKUsListJson);
		productInfoJsonVO.setDefaultSKU(populateSkuInfoJsonVO(pProductInfo.getDefaultSKU()));
		List<SKUInfoJsonVO> selectedColorSKUsListJson= new ArrayList<SKUInfoJsonVO>();
		SKUInfoJsonVO selectedColorsKUInfoJsonVO = null;
		List<SKUInfoVO> selectedColorSKUsList= pProductInfo.getSelectedColorSKUsList();
		if(selectedColorSKUsList!=null){
			for (SKUInfoVO skuInfoVO : selectedColorSKUsList) {
				selectedColorsKUInfoJsonVO=populateSkuInfoJsonVO(skuInfoVO);
				selectedColorSKUsListJson.add(selectedColorsKUInfoJsonVO);
			}
		}
		productInfoJsonVO.setSelectedColorSKUsList(selectedColorSKUsListJson);
		List<SKUInfoJsonVO> selectedSizeSKUsListJson= new ArrayList<SKUInfoJsonVO>();
		SKUInfoJsonVO selectedSizeKUInfoJsonVO=null;
		List<SKUInfoVO> selectedSizeSKUsList= pProductInfo.getSelectedSizeSKUsList();
		if(selectedColorSKUsList!=null){
			for (SKUInfoVO skuInfoVO : selectedSizeSKUsList) {
				selectedSizeKUInfoJsonVO=populateSkuInfoJsonVO(skuInfoVO);
				selectedSizeSKUsListJson.add(selectedSizeKUInfoJsonVO);
			}
		}
		productInfoJsonVO.setSelectedSizeSKUsList(selectedSizeSKUsListJson);
		}
		vlogDebug("END: @method populateProductJsonVO() @class: TRUCatalogTools, productInfoJsonVO::"+productInfoJsonVO);
		return productInfoJsonVO;
	}
	
	/**
	 * Populate battery info from product info.
	 *
	 * @param pProductInfo - pProductInfo
	 * @return batteryProductInfoVO
	 */
	public BatteryProductInfoVO populateBatteryInfoFromProductInfo(ProductInfoVO pProductInfo) {
		if (isLoggingDebug()) {
			logDebug("START: @method populateBatteryProductInfoVOFromProductInfoVO() @class: TRUCatalogTools");
		}
		List<SKUInfoVO> activeSKUsList= pProductInfo.getChildSKUsList();
		BatteryProductInfoVO batteryProductInfoVO=null;
		if(activeSKUsList!=null && !activeSKUsList.isEmpty()){
			for (SKUInfoVO skuInfoVO : activeSKUsList) {
				List<BatteryProductInfoVO> batteryProductSkusList= skuInfoVO.getBatteryProducts();
				if(batteryProductSkusList!=null && !batteryProductSkusList.isEmpty()){
					for (BatteryProductInfoVO batteryInfoVO : batteryProductSkusList) {
						batteryProductInfoVO=batteryInfoVO;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: @method populateBatteryProductInfoVOFromProductInfoVO() @class: TRUCatalogTools");
		}
		return batteryProductInfoVO;
	}
	
	/**
	 * This method is used to populate batteryInfo.
	 *
	 * @param pBatteryProductInfoVOs the Battery product info v os
	 * @param pStoreId -StoreId
	 */
	public void populateBatteryInventoryInfo(List<BatteryProductInfoVO> pBatteryProductInfoVOs,String pStoreId) {
		if (isLoggingDebug()) {
			logDebug("START: populateBatteryInventoryInfo():::");
		}
				if(pBatteryProductInfoVOs!=null && !pBatteryProductInfoVOs.isEmpty()){
					for (BatteryProductInfoVO batteryProductInfoVO : pBatteryProductInfoVOs) {
						try{
							String inventoryStatusForStore = null;
							String inventoryStatusOnline = null;
							long availableInventoryLevelOnline;
							long availableInventoryLevelstore = TRUCommerceConstants.INT_ZERO;
							int inventoryStatusOnlineInt=TRUCommerceConstants.INT_ZERO;
							try{
								inventoryStatusOnlineInt = getCoherenceInventoryManager().queryAvailabilityStatus(batteryProductInfoVO.getSkuId());
							}catch(InventoryException e){
								vlogError("ERROR:: TRUCatalogTools.populateBatteryInventoryInfo method..", e);
								inventoryStatusOnlineInt=TRUCommerceConstants.INT_ZERO;
							}
							if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_IN_STOCK 
									|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER 
									|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
								inventoryStatusOnline = TRUCommerceConstants.IN_STOCK;
								} else if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||
										inventoryStatusOnlineInt == TRUCommerceConstants.INT_ZERO) {
								inventoryStatusOnline = TRUCommerceConstants.OUT_OF_STOCK;
							} 
							if (inventoryStatusOnline != null) {
								batteryProductInfoVO.setInventoryStatus(inventoryStatusOnline);
							}
							if (inventoryStatusOnline != null && !TRUCommerceConstants.OUT_OF_STOCK.equalsIgnoreCase(inventoryStatusOnline)) {
								availableInventoryLevelOnline = getCoherenceInventoryManager().queryStockLevel(batteryProductInfoVO.getSkuId());
								batteryProductInfoVO.setAvailableInventory(availableInventoryLevelOnline);
							}
							if (pStoreId != null) {
									int inventoryStatusForStoreInt = TRUCommerceConstants.INT_ZERO;
									try{
										inventoryStatusForStoreInt=getCoherenceInventoryManager().queryAvailabilityStatus(batteryProductInfoVO.getSkuId(),pStoreId);
										if(inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK ||inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
										inventoryStatusForStoreInt = getInventoryStatusFromWareHouse(pStoreId, batteryProductInfoVO.getSkuId());
										}
									}catch(InventoryException e){
										vlogError("ERROR:: TRUCatalogTools.populateBatteryInventoryInfo method..", e);
										inventoryStatusForStoreInt=TRUCommerceConstants.INT_ZERO;
									}
									if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_IN_STOCK ||
											inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER ||
											inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
										    inventoryStatusForStore = TRUCommerceConstants.IN_STOCK;
									} else if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK || inventoryStatusForStoreInt == TRUCommerceConstants.INT_ZERO) {
											inventoryStatusForStore = TRUCommerceConstants.OUT_OF_STOCK;
									}	
									if(inventoryStatusForStore!=null){
										batteryProductInfoVO.setInventoryStatusInStore(inventoryStatusForStore);
									}
									if (inventoryStatusForStore != null && !TRUCommerceConstants.OUT_OF_STOCK.equalsIgnoreCase(inventoryStatusForStore)) {
										availableInventoryLevelstore = getCoherenceInventoryManager().queryStockLevel(batteryProductInfoVO.getSkuId(), pStoreId);
										batteryProductInfoVO.setAvailableInventoryInStore(availableInventoryLevelstore);
									}
								}
						} catch (InventoryException e) {
							if (isLoggingError()) {
								logError("Exception occured while generating sitemap...", e);
							}
						}
					}
		}
		if (isLoggingDebug()) {
			logDebug("END: populateBatteryInventoryInfo():::");
		}
	}

	/**
	 * this method is to get the sku Info-json-vo from sku-infovo.
	 * @param pSKUInfoVO - pSKUInfoVO
	 * @return sKUInfoJsonVO - sKUInfoJsonVO
	 */
	private SKUInfoJsonVO populateSkuInfoJsonVO(SKUInfoVO pSKUInfoVO) {
		vlogDebug("BEGIN: @method populateSkuInfoJsonVO() @class: TRUCatalogTools, pSKUInfoVO::"+pSKUInfoVO);
		SKUInfoJsonVO sKUInfoJsonVO = new SKUInfoJsonVO();
		if (pSKUInfoVO != null) {
			sKUInfoJsonVO.setActive(pSKUInfoVO.getActive());
			sKUInfoJsonVO.setChannelAvailability(pSKUInfoVO.getChannelAvailability());
			sKUInfoJsonVO.setAlterNateImages(pSKUInfoVO	.getAlterNateImages());
			sKUInfoJsonVO.setAvailableInventory(pSKUInfoVO.getAvailableInventory());
			sKUInfoJsonVO.setColorCode(pSKUInfoVO.getColorCode());
			sKUInfoJsonVO.setDeleted(pSKUInfoVO.isDeleted());
			sKUInfoJsonVO.setDisplayable(pSKUInfoVO.isDisplayable());
			sKUInfoJsonVO.setDisplayName(pSKUInfoVO.getDisplayName());
			sKUInfoJsonVO.setDropShipFlag(pSKUInfoVO.isDropShipFlag());
			sKUInfoJsonVO.setId(pSKUInfoVO.getId());
			sKUInfoJsonVO.setInventoryStatus(pSKUInfoVO.getInventoryStatus());
			sKUInfoJsonVO.setInventoryStatusInStore(pSKUInfoVO.getInventoryStatusInStore());
			sKUInfoJsonVO.setJuvenileSize(pSKUInfoVO.getJuvenileSize());
			sKUInfoJsonVO.setListPrice(pSKUInfoVO.getListPrice());
			sKUInfoJsonVO.setNewItem(pSKUInfoVO.isNewItem());
			sKUInfoJsonVO.setNotifyMe(pSKUInfoVO.getNotifyMe());
			sKUInfoJsonVO.setPresellable(pSKUInfoVO.isPresellable());
			sKUInfoJsonVO.setBackOrderStatus(pSKUInfoVO.getBackOrderStatus());
			sKUInfoJsonVO.setPresellQuantityUnits(pSKUInfoVO.getPresellQuantityUnits());
			sKUInfoJsonVO.setPriceDisplay(pSKUInfoVO.getPriceDisplay());
			sKUInfoJsonVO.setPriceSavingAmount(pSKUInfoVO.getPriceSavingAmount());
			sKUInfoJsonVO.setPriceSavingPercentage(pSKUInfoVO.getPriceSavingPercentage());
			sKUInfoJsonVO.setPrimaryImage(pSKUInfoVO.getPrimaryImage());
			sKUInfoJsonVO.setSalePrice(pSKUInfoVO.getSalePrice());
			sKUInfoJsonVO.setSearchable(pSKUInfoVO.isSearchable());
			sKUInfoJsonVO.setIspu(pSKUInfoVO.getIspu());
			sKUInfoJsonVO.setSkuRegistryEligibility(pSKUInfoVO.isSkuRegistryEligibility());
			sKUInfoJsonVO.setRegistryWarningIndicator(pSKUInfoVO.getRegistryWarningIndicator());
			sKUInfoJsonVO.setSecondaryImage(pSKUInfoVO.getSecondaryImage());
			sKUInfoJsonVO.setSecondaryCanonicalImage(pSKUInfoVO.getSecondaryCanonicalImage());
			sKUInfoJsonVO.setPrimaryCanonicalImage(pSKUInfoVO.getPrimaryCanonicalImage());
			sKUInfoJsonVO.setAlterNateCanonicalImages(pSKUInfoVO.getAlterNateCanonicalImages());
			sKUInfoJsonVO.setAlterNateImages(pSKUInfoVO.getAlterNateImages());
			sKUInfoJsonVO.setVendorsProductDemoUrl(pSKUInfoVO.getVendorsProductDemoUrl());
			sKUInfoJsonVO.setSkuType(pSKUInfoVO.getSkuType());
			//sKUInfoJsonVO.setStoreMessage(pSKUInfoVO.getStoreMessage());
			sKUInfoJsonVO.setStreetDate(pSKUInfoVO.getStreetDate());
			sKUInfoJsonVO.setSuggestAgeMessage(pSKUInfoVO.getSuggestAgeMessage());
			sKUInfoJsonVO.setSwatchImage(pSKUInfoVO.getSwatchImage());
			sKUInfoJsonVO.setUnCartable(pSKUInfoVO.isUnCartable());
			sKUInfoJsonVO.setStoreAvailablityMessage(pSKUInfoVO.getStoreAvailablityMessage());
			sKUInfoJsonVO.setBrandName(pSKUInfoVO.getBrandName());
			sKUInfoJsonVO.setS2s(pSKUInfoVO.getS2s());
			sKUInfoJsonVO.setEsrbrating(pSKUInfoVO.getEsrbrating());
			sKUInfoJsonVO.setRmsColorCode(pSKUInfoVO.getRmsColorCode());
			sKUInfoJsonVO.setRmsSizeCode(pSKUInfoVO.getRmsSizeCode());
			sKUInfoJsonVO.setSknId(pSKUInfoVO.getOriginalproductIdOrSKN());
			sKUInfoJsonVO.setSknOriginId(pSKUInfoVO.getSknOrigin());
			sKUInfoJsonVO.setSkuUpcNumbers(pSKUInfoVO.getUpcNumbers());
			sKUInfoJsonVO.setShipWindowMin(pSKUInfoVO.getShipWindowMin());
			sKUInfoJsonVO.setShipWindowMax(pSKUInfoVO.getShipWindowMax());
			List<BatteryInfoJsonVO> batterySKUsListJson= new ArrayList<BatteryInfoJsonVO>();
			List<BatteryProductInfoVO> batteryProductSKUsList= pSKUInfoVO.getBatteryProducts();
			BatteryInfoJsonVO batteryInfoJsonVO=null;
			if(batteryProductSKUsList!=null){
				for (BatteryProductInfoVO skuInfoVO : batteryProductSKUsList) {
					batteryInfoJsonVO=populateBatteryInfoJsonVO(skuInfoVO);
					batterySKUsListJson.add(batteryInfoJsonVO);
				}
			}
			sKUInfoJsonVO.setBatteryInfoJsonVO(batterySKUsListJson);
		}
		vlogDebug("END: @method populateSkuInfoJsonVO() @class: TRUCatalogTools, sKUInfoJsonVO::"+sKUInfoJsonVO);
		return sKUInfoJsonVO;
	}
	
	/**
	 * This method is used to set the batteryInfoJsonVo from batteryProductInfoVo.
	 * 
	 * @param pBatteryProductInfoVO -batteryProductInfoVo
	 * @return BatteryInfoJsonVO -  batteryInfoJsonVo
	 */
	private BatteryInfoJsonVO populateBatteryInfoJsonVO(BatteryProductInfoVO pBatteryProductInfoVO) {
		vlogDebug("BEGIN: @method populateBatteryInfoJsonVO() @class: TRUCatalogTools, pBatteryProductInfoVO::"+pBatteryProductInfoVO);
		BatteryInfoJsonVO batteryInfoJsonVO = new BatteryInfoJsonVO();
		if (pBatteryProductInfoVO != null) {
			batteryInfoJsonVO.setChannelAvailability(pBatteryProductInfoVO.getChannelAvailability());
			batteryInfoJsonVO.setDisplayName(pBatteryProductInfoVO.getDisplayName());
			batteryInfoJsonVO.setSkuId(pBatteryProductInfoVO.getSkuId());
			///Online Inventory
			batteryInfoJsonVO.setInventoryStatus(pBatteryProductInfoVO.getInventoryStatus());
			batteryInfoJsonVO.setAvailableInventory(pBatteryProductInfoVO.getAvailableInventory());
			//Store Inventory
			batteryInfoJsonVO.setInventoryStatusInStore(pBatteryProductInfoVO.getInventoryStatusInStore());
			batteryInfoJsonVO.setAvailableInventoryInStore(pBatteryProductInfoVO.getAvailableInventoryInStore());
			batteryInfoJsonVO.setListPrice(pBatteryProductInfoVO.getListPrice());
			batteryInfoJsonVO.setNotifyMe(pBatteryProductInfoVO.getNotifyMe());
			batteryInfoJsonVO.setPresellable(pBatteryProductInfoVO.isPresellable());
			batteryInfoJsonVO.setBackOrderStatus(pBatteryProductInfoVO.getBackOrderStatus());
			batteryInfoJsonVO.setPrimaryImage(pBatteryProductInfoVO.getPrimaryImage());
		//	batteryInfoJsonVO.setLongDescription(pBatteryProductInfoVO.getLongDescription());
			batteryInfoJsonVO.setOriginalproductIdOrSKN(pBatteryProductInfoVO.getOriginalproductIdOrSKN());
			batteryInfoJsonVO.setProductId(pBatteryProductInfoVO.getProductId());
			batteryInfoJsonVO.setPdpURL(pBatteryProductInfoVO.getPdpURL());
			batteryInfoJsonVO.setPresellQuantityUnits(pBatteryProductInfoVO.getPresellQuantityUnits());
			batteryInfoJsonVO.setPriceDisplay(pBatteryProductInfoVO.getPriceDisplay());
			batteryInfoJsonVO.setSalePrice(pBatteryProductInfoVO.getSalePrice());
			batteryInfoJsonVO.setIspu(pBatteryProductInfoVO.getIspu());
			batteryInfoJsonVO.setChannelAvailability(pBatteryProductInfoVO.getChannelAvailability());
		//	batteryInfoJsonVO.setStreetDate(pBatteryProductInfoVO.getStreetDate());
			batteryInfoJsonVO.setUnCartable(pBatteryProductInfoVO.isUnCartable());
			batteryInfoJsonVO.setS2s(pBatteryProductInfoVO.getS2s());
		//	batteryInfoJsonVO.setEsrbrating(pBatteryProductInfoVO.getEsrbrating());
		}
		vlogDebug("END: @method populateSkuInfoJsonVO() @class: TRUCatalogTools, BatteryInfoJsonVO::"+batteryInfoJsonVO);
		return batteryInfoJsonVO;
	}
	
	
	/**
	 * Method to Get Default sku item ie. 1st sku item from list of
	 * SKu for provided Product item.
	 * 
	 * @param pProductItem
	 *            - Product Repository Item
	 * @return - Repository Item - Sku Item.
	 */
	@SuppressWarnings("unchecked")
	public RepositoryItem getDefaultChildSku(RepositoryItem pProductItem) {
		if (isLoggingDebug()) {
			logDebug("Entered into TRUCatalogTools.getDefaultChildSku method");
			logDebug("pSkuItem:" + pProductItem);
		}
		RepositoryItem defaultSKUItem = null;
		if (pProductItem == null) {
			return defaultSKUItem;
		}
		List<RepositoryItem> items = (List<RepositoryItem>) pProductItem
				.getPropertyValue(getCatalogProperties()
						.getChildSkusPropertyName());
		if (items != null && !items.isEmpty()) {
			defaultSKUItem = items.iterator().next();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUCatalogTools.getDefaultChildSku method with Default SKU : "
					+ defaultSKUItem);
		}
		return defaultSKUItem;
	}

	
	/**
	 * Find default sku using online pid.
	 *
	 * @param pOnlinePid - pOnlinePid
	 * @param pChildSkus - pChildSkus
	 * @return skuVo
	 */
	public SKUInfoVO findDefaultSkuUsingOnlinePID(String pOnlinePid, List<SKUInfoVO> pChildSkus) {
		vlogDebug("BEGIN: TRUCatalogTools.findDefaultSkuUsingOnlinePID , pOnlinePid:", pOnlinePid);
		SKUInfoVO skuVo=null;
		if(StringUtils.isNotBlank(pOnlinePid) && pChildSkus != null && !pChildSkus.isEmpty()){
		for (SKUInfoVO skuInfoVO : pChildSkus) {
			if(pOnlinePid.equals(skuInfoVO.getOnlinePID())){
				skuVo=skuInfoVO;
				break;
			}
		}
		}
		vlogDebug("End: TRUCatalogTools.findDefaultSkuUsingOnlinePID , skuVo:", skuVo);
		return skuVo;
	} 
 
	/**
	 * This method is used to get the inventory Status of a sku for a particular store.
	 * @param pStoreId - StoreId
	 * @param pSkuId - SkuId
	 * @return inventoryStatusForStore -reference to InventoryStatusForStore.
	 */
	public String getInventoryStatusForStore(String pStoreId,String pSkuId)
	{
		vlogDebug("Begin: TRUCatalogTools.getInventoryStatusForStore , pSkuId:", pSkuId);
		String inventoryStatusForStore = TRUCommerceConstants.OUT_OF_STOCK;
		int inventoryStatusOnlineInt= TRUCommerceConstants.INT_ZERO;
		try {
		if (StringUtils.isNotBlank(pStoreId) && StringUtils.isNotBlank(pSkuId) ) {
			vlogDebug("TRUCatalogTools.getInventoryStatusForStore , pStoreId:", pStoreId);
			int inventoryStatusForStoreInt = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId,pStoreId);
			if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_IN_STOCK
					|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER
					|| inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
				inventoryStatusForStore = TRUCommerceConstants.IN_STOCK;
			} else if (inventoryStatusForStoreInt == TRUCommerceConstants.INT_OUT_OF_STOCK) {
				inventoryStatusForStore = TRUCommerceConstants.OUT_OF_STOCK;
			} 
			}
		}catch (InventoryException e) {
			vlogError("TRUCatalogTools.populateInventory method..", e);
			inventoryStatusForStore=TRUCommerceConstants.OUT_OF_STOCK;
		}
		vlogDebug("END: TRUCatalogTools.getInventoryStatusForStore , inventoryStatusForStore:", inventoryStatusForStore);
		return inventoryStatusForStore;
	}
	
	/**
	 * Method to get the EWaste Sku id(UID).
	 * @param pProductRef : RepositoryItem Object
	 * @return String - Return ewaste sku id.
	 */
	public String getEWasteUID(RepositoryItem pProductRef){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCatalogTools  method: getEWasteUID]");
			vlogDebug("Product Item : {0} pProductRef",pProductRef);
		}
		String ewasteUID = null;
		if(pProductRef == null){
			return ewasteUID;
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String eWasteSurchargeProdId = (String) pProductRef.getPropertyValue(catalogProperties.getEwasteSurchargeSkuPropertyName());
		if(StringUtils.isBlank(eWasteSurchargeProdId)){
			vlogDebug("EWate Product Id is NUll, Returing with ewasteUID : {0}",ewasteUID);
			return ewasteUID;
		}
		RepositoryItem eWasteSKUItem = null;
		RepositoryItem ewasteProdItem = null;
		try {
			ewasteProdItem = findProduct(eWasteSurchargeProdId);
			if(ewasteProdItem != null){
				eWasteSKUItem = getDefaultChildSku(ewasteProdItem);
			}
			if(eWasteSKUItem != null){
				ewasteUID = eWasteSKUItem.getRepositoryId();
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException:  While getting sku or product item : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCatalogTools  method: getEWasteUID]");
			vlogDebug("EWaste UID : {0} ewasteUID",ewasteUID);
		}
		return ewasteUID;
	}

	/**
	 * to get the default Sku using skuId.
	 * @param pProductInfo - pProductInfo
	 * @param pAddedSkuId - pAddedSkuId
	 * @return defaultSkuInfoVO - DefaultSkuInfoVO
	 */
	public SKUInfoVO findDefaultSkuFormSkuId(ProductInfoVO pProductInfo, String pAddedSkuId) {
		vlogDebug("BEGIN :[TRUCatalogTools]  method:[findDefaultSkuFormSkuId], pAddedSkuId:"+pAddedSkuId);
		List<SKUInfoVO> skuVos = pProductInfo.getChildSKUsList();
		SKUInfoVO defaultSkuInfoVO =null;
		if(skuVos!=null && !skuVos.isEmpty()){
		for (SKUInfoVO skuInfoVO : skuVos) {
			if(skuInfoVO.getId().equals(pAddedSkuId)){
				defaultSkuInfoVO=skuInfoVO;
				break;
			}
		}
		}
		vlogDebug("END :[TRUCatalogTools]  method:[findDefaultSkuFormSkuId], defaultSkuInfoVO:"+defaultSkuInfoVO);
		return defaultSkuInfoVO;
	}
	
	/**
     * This method will return the OnlinePID from the skuId.
     * @param pSkuId - pSkuId
     * @return onlinePid
     */
     public String getOnlinePIDFromSKUId(String pSkuId) {
            if (isLoggingDebug()) {
                   logDebug("Enter into [Class: TRUCatalogTools  method: getOnlinePIDFromSKUId]");
            }
            RepositoryItem skuItem =null;
            TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
            String onlinePid = null;
            try{
                   skuItem = getCartModifierHelper().findSkuItem(pSkuId);
                   if(skuItem != null) {
                         onlinePid=(String) skuItem.getPropertyValue(catalogProperties.getOnlinePID());
                   }
            } catch (RepositoryException exc) {
                   if (isLoggingError()) {
                         vlogError("RepositoryException:  While getting OnlinePID: {0}", exc);
                   }
            }
            if (isLoggingDebug()) {
                   logDebug("Exit from [Class: TRUCatalogTools  method: getOnlinePIDFromSKUId]");
            }
            return onlinePid;
     }

	/**
	 * This method is used to get the parent category promos.
	 * @param pItem RepositoryItemImpl
	 * @param pAllPromo Set<String>
	 * @return pAllPromo Set<String>
	 */
	public Set<String> getParentCatPromos(RepositoryItemImpl pItem, Set<String> pAllPromo){
		if (isLoggingDebug()) {
			logDebug("Enter into TRUCatalogTools.getParentCatPromos");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogProperties();		
		// Getting all fixed Parent Categories
		Set<RepositoryItem> parentCat = (Set<RepositoryItem>) pItem.getPropertyValue(catalogProperties.getFixedParentCategoriesPropertyName());
		if (isLoggingDebug()) {
			logDebug("Parent Categories: " + parentCat);
		}
		if(parentCat != null && !parentCat.isEmpty()){
			Set<String> catPromo =null;
			for(RepositoryItem lRepoItem : parentCat){
				catPromo = (Set<String>) lRepoItem.getPropertyValue(catalogProperties.getCategoryPromotionsPropertyName());
				if (isLoggingDebug()) {
					logDebug("Promotion Associated to Category : " + catPromo);
				}
				if((catPromo != null && !catPromo.isEmpty()) && pAllPromo == null){
					pAllPromo = new HashSet<String>();
					pAllPromo.addAll(catPromo);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Enter into TRUCatalogTools.getParentCatPromos");
		}
		return pAllPromo;
	}

	/**
	 * This method is used to get all the comparable properties.
	 * @param pActiveProduct - pActiveProduct
	 * @param pDefaultSku - pDefaultSku
	 * @param pClassificationAttributeNamesList - pClassificationAttributeNamesList
	 * @return attrMap - Attribute map
	 */
	public Map getAllComparableProperties(RepositoryItem pActiveProduct,
			RepositoryItem pDefaultSku, List<RepositoryItem> pClassificationAttributeNamesList) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCatalogTools.getAllComparableProperties");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogProperties();	
		Map attrMap= new HashMap<>();
		if(pActiveProduct != null && pDefaultSku != null && pClassificationAttributeNamesList != null &&
				 !pClassificationAttributeNamesList.isEmpty()){
			if (isLoggingDebug()) {
				logDebug("TRUCatalogTools.getAllComparableProperties"+pActiveProduct.getRepositoryId());
			}
			List<String> attributeNameList = new ArrayList<String>();
			List attributeValueList = new ArrayList<String>();
			Map<String, Object> attributeNameValuemap= new HashMap<String, Object>();
			for (RepositoryItem attribute : pClassificationAttributeNamesList) {
				String attributeType= (String) attribute.getPropertyValue(catalogProperties.getAttributeType());
				String attributeName= (String) attribute.getPropertyValue(catalogProperties.getAttributeNamePropertyName());
				String attributePropertyName= (String) attribute.getPropertyValue(catalogProperties.getAttributeProperty());
				if(attributeType != null && attributeName != null && attributePropertyName != null){
					Object attrValue=null;
				try{ 
				if( attributeType.equalsIgnoreCase(getCatalogConfiguration().getAttrPropertyTypeProduct())){
					if(pActiveProduct.getPropertyValue(attributePropertyName)!=null){
					attrValue= pActiveProduct.getPropertyValue(attributePropertyName);
					}
				 }else if( attributeType.equalsIgnoreCase(getCatalogConfiguration().getAttrPropertyTypeSku()) ||
						getCatalogConfiguration().getTruSkuTypes().contains(attributeType)){
					String attrProKeyName =null;
					if(attributePropertyName.contains(TRUCommerceConstants.PIPELINE)){
						String [] attributePropertyNameArray = attributePropertyName.split(TRUCommerceConstants.PIPELINE_SEPERATOR);
						String attrProName= attributePropertyNameArray[TRUCommerceConstants.INT_ZERO];
						 attrProKeyName= attributePropertyNameArray[TRUCommerceConstants.INT_ONE];
						 Map attributeMap=(Map)pDefaultSku.getPropertyValue(attrProName);
						 attrValue= attributeMap.get(attrProKeyName);
						 
					}
					else{
						attrValue= pDefaultSku.getPropertyValue(attributePropertyName);
					}
				}if( attributeType.equalsIgnoreCase(getCatalogConfiguration().getAttrPropertyTypeDerivedSku()) ){
					
					if(attributePropertyName.equalsIgnoreCase(catalogProperties.getAttributeNameDimension())){
					Map dimenssionValue= (Map) pDefaultSku.getPropertyValue(catalogProperties.getAttributeNameDimension());
					String itemDepth=(String) dimenssionValue.get(TRUCommerceConstants.ITEM_DEPTH);
					String itemWidth=(String) dimenssionValue.get(TRUCommerceConstants.ITEM_WIDTH);
					String itemHeight=(String) dimenssionValue.get(TRUCommerceConstants.ITEM_HEIGHT);
					if(itemDepth!=null && itemWidth!=null &&  itemHeight!=null){
					StringBuffer dimenssion=  new StringBuffer();
					dimenssion.append(itemDepth);
					dimenssion.append(TRUCommerceConstants.STAR);
					dimenssion.append(itemWidth);
					dimenssion.append(TRUCommerceConstants.STAR);
					dimenssion.append(itemHeight);
					attrValue=dimenssion.toString();
					}
				 }else if(attributePropertyName.equalsIgnoreCase(catalogProperties.getAttributeNameChildWeight()) && pDefaultSku.getPropertyValue(catalogProperties.getChildWeightMin())!=null && pDefaultSku.getPropertyValue(catalogProperties.getChildWeightMax())!=null ){
					 String ageMin= (String) pDefaultSku.getPropertyValue(catalogProperties.getChildWeightMin());
					 String ageMax= (String) pDefaultSku.getPropertyValue(catalogProperties.getChildWeightMax());
					 StringBuffer ageBuffer=  new StringBuffer();
					 ageBuffer.append(ageMin);
					 ageBuffer.append(TRUCommerceConstants.SINGLE_FORWARD_SLASH);
					 ageBuffer.append(ageMax);
					 attrValue=ageBuffer.toString();
					 }
				}
				 
			}catch(Exception e){
					if(isLoggingError()){
						logError("ERROR: TRUCatalogTools.getAllComparableProperties: ",e);
					}
					attrValue =TRUCommerceConstants.HYPHEN;
				}
				attributeNameList.add(attributeName);
				if(attrValue != null){
				if(attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.ARRAYLIST_CLASS) || 
						 attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.LIST_CLASS) ||
						 attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.CHANGE_AWARE_LIST_CLASS)){
					List attrvalueListType=(List) attrValue;
					StringBuffer compareProductBufferList= new StringBuffer();
					for (int i = 0; i < attrvalueListType.size(); i++) {
						compareProductBufferList.append(attrvalueListType.get(i));
						if(i != attrvalueListType.size()-TRUCommerceConstants.ONE){
							compareProductBufferList.append(TRUCommerceConstants.COMA_SYMBOL);
						}
					} 
					attrValue =	compareProductBufferList.toString();	
				}else if(attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.SET_CLASS) 
						|| attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.HASHSET_CLASS) ||
						attrValue.getClass().getSimpleName().equalsIgnoreCase(TRUCommerceConstants.CHANGE_AWARE_SET_CLASS)){
					Set attrvalueSetType=(Set) attrValue;
					StringBuffer compareProductBufferSet= new StringBuffer();
					Iterator iterator = attrvalueSetType.iterator(); 
					   while (iterator.hasNext()){
						   compareProductBufferSet.append(iterator.next()) ;
						   compareProductBufferSet.append(TRUCommerceConstants.COMA_SYMBOL) ;
					   }
					   attrValue =compareProductBufferSet.toString();	
				}
				}
				if(attrValue == null){
					attrValue =TRUCommerceConstants.HYPHEN;
				}
					attributeValueList.add(attrValue);
					attributeNameValuemap.put(attributeName, attrValue);
				}
			}
			if (isLoggingDebug()) {
				logDebug("TRUCatalogTools.getAllComparableProperties.  attributeNameValuemap::"+attributeNameValuemap.size());
			}
			attrMap.put(TRUCommerceConstants.SET_OF_ATTRIBUTENAME, attributeNameList);
			attrMap.put(TRUCommerceConstants.PRODUCT_ATTRIBUTE_VALUES, attributeValueList);
			attrMap.put(TRUCommerceConstants.PRODUCT_ATTRIBUTE_NAME_VALUE_MAP, attributeNameValuemap);
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUCatalogTools.getAllComparableProperties.  attrMap::"+attrMap.size());
		}
		return attrMap;
	}

	/**
	 * This method returns attribute list of a category.
	 * @param pCategoryId - pCategoryId
	 * @return attrItemList - attrItemList
	 */
	public List getAllClassificationAttributeNamesList(String pCategoryId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUCatalogTools.getAllClassificationAttributeNamesList.  pCategoryId::"+pCategoryId);
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List attrItemList = null;
		try {
			RepositoryItem classificationItem =classificationItem = getCatalog().getItem(pCategoryId, catalogProperties.getClassificationItemDescPropertyName());
			if(classificationItem== null){
				return attrItemList;
			}
			attrItemList=(List<RepositoryItem>) classificationItem.getPropertyValue(catalogProperties.getComparableAttributes());
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("ERROR: TRUCatalogTools.getAllClassificationAttributeNamesList: ",e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUCatalogTools.getAllClassificationAttributeNamesList.  attrItemList::"+attrItemList);
		}
		return attrItemList;
	}
	
	/**
	 * This method returns one map of attributes.
	 * @param pActiveProduct - pActiveProduct
	 * @param pDefaultSku - pDefaultSku
	 * @param pClassificationAttributeNamesList - pClassificationAttributeNamesList
	 * @return valueMap - valueMap
	 */
	public Map getCompareableProperties(RepositoryItem pActiveProduct,
			 SKUInfoVO pDefaultSku,List<RepositoryItem> pClassificationAttributeNamesList) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogTools.getCompareableProperties method.. ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
			Map valueMap= new HashMap();
			if(pActiveProduct != null){
				List<RepositoryItem> childSkus= (List<RepositoryItem>) pActiveProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());
				if(childSkus !=  null){
					RepositoryItem temp_defaultSku= null;
					for (RepositoryItem sku : childSkus) {
						if(pDefaultSku != null && sku.getRepositoryId().equals(pDefaultSku.getId())){
							temp_defaultSku=sku;
							break;
						}
					}
				valueMap= getAllComparableProperties(pActiveProduct,temp_defaultSku,pClassificationAttributeNamesList);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("END:: TRUCatalogTools.getCompareableProperties method.. valueMap:::"+ valueMap);
			}
		return valueMap;
	}

	/**
	 * This method returns the category name for the product.
	 * @param pProductID - String
	 * @return productCategoryName - String
	 */	
	
    public String getProductCategoryName(String pProductID){

		if (isLoggingDebug()) {
			vlogDebug("START:: TRUCatalogTools.getProductCategoryName method..");
		}
		String productCategoryId =  null;
		String productCategoryName =  TRUCommerceConstants.EMPTY_SPACE;
		RepositoryItem ancestorCategoryItem= null;
		ChangeAwareSet ancestorCategoryList = null;
		boolean categoryFound = false;
		if(StringUtils.isEmpty(pProductID)) {
			return productCategoryName;
		}
		try {
			RepositoryItem productItem = findProduct(pProductID);
				TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogProperties();
			if(productItem != null && productItem.getPropertyValue(catalogProperties.getAncestorCategoryIdProperty()) != null) {
				ancestorCategoryList = (ChangeAwareSet) productItem.getPropertyValue(catalogProperties.getAncestorCategoryIdProperty());
				if(ancestorCategoryList != null && !ancestorCategoryList.isEmpty()) {
					for (Object ancestorCategoryid : ancestorCategoryList) {
						ancestorCategoryItem = findCategory(ancestorCategoryid.toString());
						if(ancestorCategoryItem != null) {
							Set<RepositoryItem> parentCat = (Set<RepositoryItem>) ancestorCategoryItem.getPropertyValue(catalogProperties.getFixedParentCategoriesPropertyName());
							if(parentCat != null && !parentCat.isEmpty()) {
								for (Iterator iterator = parentCat.iterator(); iterator.hasNext();) {
									RepositoryItem parentCatItem = (RepositoryItem) iterator.next();
									if(parentCatItem != null) {
										productCategoryId = parentCatItem.getRepositoryId();
										if(!StringUtils.isBlank(productCategoryId) && (productCategoryId.equalsIgnoreCase(TRUCommerceConstants.NAVIGATION_TRU) || productCategoryId.equalsIgnoreCase(TRUCommerceConstants.NAVIGATION_BRU))) {
											if(ancestorCategoryItem.getItemDisplayName() != null){
											productCategoryName = ancestorCategoryItem.getItemDisplayName();
											categoryFound = true;
											break;
											}
										}
									}					

								}
							}
						}
						if(categoryFound){
							break;
						}

					}
				}


			}
		} catch (RepositoryException repositryEx) {
			if (isLoggingError()) {	
				vlogError(repositryEx, "Repository Exception occured in TRUProductCategoryInfoDroplet Exception ---");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.getProductCategoryName method..");
		}
		return productCategoryName;
	}
    /**
     * This method will return the woreHouse inventoryStatus for the select storeId and skuId.
     * @param pStoreId - StoreId
     * @param pSkuId - SkuId
     * @return inventoryStatusForStoreInt
     * @throws InventoryException - InventoryException
     */
    public int getInventoryStatusFromWareHouse(String pStoreId, String pSkuId) throws InventoryException {
    	if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogTools.getInventoryStatusFromWareHouse method..");
		}
    	int inventoryStatusForStoreInt = TRUCommerceConstants.INT_OUT_OF_STOCK;
    	
    	TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getStoreLocatorTools());
  	   	RepositoryItem storeItem = locatorTools.getStoreItem(pStoreId);
  	   	Double storeItemWareHouseLocationId = (Double)locatorTools.getPropertyValue(storeItem, locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
    		if (storeItem != null && storeItemWareHouseLocationId != null) {
    			if (isLoggingDebug()) {
    				vlogDebug("storeItemWareHouseLocationId : {0} pSkuId : {1}", storeItemWareHouseLocationId,pSkuId);
    			}
    			inventoryStatusForStoreInt = getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId,Long.toString(Math.round(storeItemWareHouseLocationId)));
				 return inventoryStatusForStoreInt;
    		}
    		if (isLoggingDebug()) {
    			vlogDebug("END:: TRUCatalogTools.getInventoryStatusFromWareHouse method..");
    		}
    		return inventoryStatusForStoreInt;
    	}
    
    /**
     * This method will return the woreHouse inventoryStockLevel for the select storeId and skuId.
     * @param pStoreId - StoreId
     * @param pSkuId - SkuId
     * @return availableInventoryLevelstore
     * @throws InventoryException - InventoryException
     */
    public long getInventoryStockLevelFromWareHouse(String pStoreId, String pSkuId) throws InventoryException {
    	long availableInventoryLevelstore = TRUCommerceConstants.INT_ZERO;
    	
    	TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getStoreLocatorTools());
  	   	RepositoryItem storeItem = locatorTools.getStoreItem(pStoreId);
  	   	Double storeItemWareHouseLocationId = (Double)locatorTools.getPropertyValue(storeItem, locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
    		if (storeItem != null && storeItemWareHouseLocationId != null) {	
    			availableInventoryLevelstore = getCoherenceInventoryManager().queryStockLevel(pSkuId,Long.toString(Math.round(storeItemWareHouseLocationId)));
				 return availableInventoryLevelstore;
    		}
    		return availableInventoryLevelstore;
    	}

/**
 * This method will return multiple SkuItem.
 *
 * @param pSkus - Skus
 * @return skuAndOnlinePIdMap
 * @throws RepositoryException - RepositoryException
 */
	public Map<String,String> findSkus(String[] pSkus) throws RepositoryException {
		RepositoryItem[] skuItems = findSKUs(pSkus, null);
		String onlinePID = null;
		Map<String,String> skuAndOnlinePIdMap = new HashMap<String, String>();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (skuItems != null) {
			for (RepositoryItem skuItem : skuItems) {
				if (skuItem.getPropertyValue(catalogProperties.getOnlinePID()) != null && skuItem.getRepositoryId() != null) {
					onlinePID = (String) skuItem.getPropertyValue(catalogProperties.getOnlinePID());
					skuAndOnlinePIdMap.put(skuItem.getRepositoryId(), onlinePID);
				}
			}
			if (skuAndOnlinePIdMap != null && !skuAndOnlinePIdMap.isEmpty()) {
				return skuAndOnlinePIdMap;
			}
		}
		return skuAndOnlinePIdMap;
	}
	
	/**
	 * This method returns the category name for the product.
	 * @param pProductID - String
	 * @param pCategoryName - String
	 * @return subCatName - String
	 */	
	
    public String getProductSubCategory(String pProductID,String pCategoryName){
    	
    	if (isLoggingDebug()) {
			vlogDebug("START:: TRUCatalogTools.getProductSubCategory method..");
		}
    	boolean subCategoryFound = false;
		String productCategoryId =  null;
		RepositoryItem ancestorCategoryItem= null;
		ChangeAwareSet ancestorCategoryList = null;
		String  subCatName =TRUCommerceConstants.EMPTY_SPACE;
		if (!StringUtils.isBlank(pProductID)) {
			try {
				RepositoryItem productItem = findProduct(pProductID);
				TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogProperties();
				if(productItem!=null && (productItem.getPropertyValue(catalogProperties.getAncestorCategoryIdProperty()) !=null)){
				ancestorCategoryList = (ChangeAwareSet) productItem.getPropertyValue(catalogProperties.getAncestorCategoryIdProperty());
					if(ancestorCategoryList!=null){
				for (Object ancestorCategoryid : ancestorCategoryList) {
					ancestorCategoryItem = findCategory(ancestorCategoryid.toString());
						if(ancestorCategoryItem!=null && (ancestorCategoryItem.getPropertyValue(catalogProperties.getFixedParentCategoriesPropertyName()) !=null)){
					Set<RepositoryItem> parentCat = (Set<RepositoryItem>) ancestorCategoryItem.getPropertyValue(catalogProperties.getFixedParentCategoriesPropertyName());
					if(parentCat!=null){
					for (Iterator iterator = parentCat.iterator(); iterator.hasNext();) 
					{
						RepositoryItem parentCatItem = (RepositoryItem) iterator.next();
						if(parentCatItem != null && parentCatItem.getItemDisplayName() != null ){
							productCategoryId = parentCatItem.getItemDisplayName();
							 if(StringUtils.isNotBlank(productCategoryId) && (productCategoryId.equalsIgnoreCase(pCategoryName)) && ancestorCategoryItem.getItemDisplayName()!=null){
								subCatName = ancestorCategoryItem.getItemDisplayName();
								subCategoryFound = true;
								break;
							}
						}					
					}
				   }
					if(subCategoryFound){
						break;
					}
						}
					}
				 }
				}
	
			} catch (RepositoryException repositryEx) {
				if (isLoggingError()) {	
					logError("TRUProductCategoryInfoDroplet Exception ---", repositryEx);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.getProductSubCategory method..");
		}
		return subCatName;
  }	
    
    
    
  /**
   * Gets the category display name.
   *
   * @param pCategoryId - the category Id
   * @return categoryDisplayName - the category display name
   * @throws RepositoryException - the repository Exception
   */
public String   getCategoryDisplayName(String pCategoryId )throws RepositoryException
  
  {
	  
	  RepositoryItem categoryItem = null;
	  
		categoryItem = findCategory(pCategoryId);
		
		String categoryDisplayName=null;
		
		if(categoryItem != null)
		{
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
	
			
			categoryDisplayName = (String) categoryItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
		}
	  
	  return categoryDisplayName;
  }

/**
 * This method should validate the PID for the currentSite .
 *
 * @param pOnliePID - OnliePID
 * @return PIDNotFoundInCurrentSite - PIDNotFoundInCurrentSite
 */
	@SuppressWarnings("unchecked")
	public boolean checkSiteValidationForPID(String pOnliePID) {
		boolean PIDNotFoundInCurrentSite = false;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		RepositoryItem[] skuItem = getSKUFromOnlinePID(pOnliePID);
		if (skuItem != null && skuItem.length > TRUCommerceConstants.INT_ZERO) {
			Set<String> siteIdList = (Set<String>) skuItem[TRUCommerceConstants.INT_ZERO].getPropertyValue(catalogProperties.getSiteIDPropertyName());
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			if(siteIdList != null && !siteIdList.isEmpty() && currentSiteId != null  && !StringUtils.isEmpty(currentSiteId) && !siteIdList.contains(currentSiteId)) {
				PIDNotFoundInCurrentSite = true;
			}
		}
		return PIDNotFoundInCurrentSite;
	}

/**
 * Checks if is check super display flag.
 *
 * @return the CheckSuperDisplayFlag
 */
public boolean isCheckSuperDisplayFlag() {
	return mCheckSuperDisplayFlag;
}

/**
 * Sets the check super display flag.
 *
 * @param pCheckSuperDisplayFlag the CheckSuperDisplayFlag to set
 */
public void setCheckSuperDisplayFlag(boolean pCheckSuperDisplayFlag) {
	mCheckSuperDisplayFlag = pCheckSuperDisplayFlag;
}

/**
 * Checks if is filter sku list.
 *
 * @return the filterSkuList
 */
public boolean isFilterSkuList() {
	return mFilterSkuList;
}

/**
 * Sets the filter sku list.
 *
 * @param pFilterSkuList the filterSkuList to set
 */
public void setFilterSkuList(boolean pFilterSkuList) {
	mFilterSkuList = pFilterSkuList;
}

/**
 * This method will return the parent product id for a sku.
 *
 * @param pSkus - SKUs array
 * @return the parent productfrom sk us
 */
public String getParentProductfromSKUs(String[] pSkus){
	RepositoryItem firstSkuItem = null;
	RepositoryItem firstProductItem = null;
	if (pSkus != null && pSkus.length > TRUCommerceConstants.INT_ZERO) {
		
		try {
			firstSkuItem = findSKU(pSkus[TRUCommerceConstants.INT_ZERO]);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError("RepositoryException in CatalogTools.findSKU() method", re);
			}
		}
	}
	 if(firstSkuItem != null){
		 Set<RepositoryItem> parentProducts = getParentProducts(firstSkuItem) ;
		 if (parentProducts != null && !parentProducts.isEmpty()) {
				firstProductItem = parentProducts.iterator().next();
				if(firstProductItem != null){
					return firstProductItem.getRepositoryId() ;
					}
			}
	 }
	
	return null;
}

/**
 * This Method is used for get the All Parent classifications.
 *
 * @param pClassificationId            - Select classification Id
 * @return fixedParentCategories - All available fixed parent categories
 * @throws RepositoryException - RepositoryException.
 */
public Set<RepositoryItem> findFixedParentClassificationItems(String pClassificationId) throws RepositoryException {
	if (isLoggingDebug()) {
		vlogDebug("BEGIN:: TRUCatalogTools.findFixedParentClassificationItems method..");
	}
	TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
	Set<RepositoryItem> fixedParentCategories = null;
	RepositoryItem classificationItem = getCatalog().getItem(pClassificationId, catalogProperties.getClassificationItemDescPropertyName());   
	if(classificationItem != null){
	 fixedParentCategories = (Set<RepositoryItem>) classificationItem.getPropertyValue(catalogProperties.getFixedParentCategoriesPropertyName());
	}
	if (isLoggingDebug()) {
		vlogDebug("END:: TRUCatalogTools.findFixedParentClassificationItems method..");
	}
	return fixedParentCategories;
}

/**
 * This Method is used for get the userTypeId.
 *
 * @param pClassificationId            - Select classification Id
 * @return userTypeId - the usertype Id for a category
 * @throws RepositoryException - RepositoryException.
 */
public String findUserTypeId(String pClassificationId) throws RepositoryException {
	if (isLoggingDebug()) {
		vlogDebug("BEGIN:: TRUCatalogTools.findUserTypeId method..");
	}
	 TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
	 String userTypeId = null;
	 RepositoryItem classificationItem = getCatalog().getItem(pClassificationId, catalogProperties.getClassificationItemDescPropertyName());   
	 if(classificationItem != null){
		 userTypeId = (String) classificationItem.getPropertyValue(catalogProperties.getUserTypeId());
	 }
	 if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.findUserTypeId method..");
		}
	 return userTypeId;
}

/**
 * This Method is used for get the userTypeId.
 *
 * @param pClassificationId            - Select classification Id
 * @return displayStatus - the displayStatus  for a category
 * @throws RepositoryException - RepositoryException.
 */
public String findCategoryDisplayStatus(String pClassificationId) throws RepositoryException {
	if (isLoggingDebug()) {
		vlogDebug("BEGIN:: TRUCatalogTools.findCategoryDisplayStatus method..");
	}
	 TRUCatalogProperties catalogProperties= (TRUCatalogProperties) getCatalogProperties();
	 String displayStatus = null;
	 RepositoryItem classificationItem = getCatalog().getItem(pClassificationId, catalogProperties.getClassificationItemDescPropertyName());   
	 if(classificationItem != null){
		 displayStatus = (String) classificationItem.getPropertyValue(catalogProperties.getDisplayStatusPropertyName());
	 }
	 if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.findCategoryDisplayStatus method..");
		}
	 return displayStatus;
}

/**
 * Gets the all catalog items.
 *
 * @param ItemType - Catalog Item Type
 * @param pStartingIndex the Starting index
 * @param pEndingIndex the Ending index
 * @return list of Repository Items
 */
public List<RepositoryItem> getAllCatalogItems(String ItemType,int pStartingIndex, int pEndingIndex){
	if (isLoggingDebug()) {
		vlogDebug("BEGIN:: TRUCatalogTools.getAllCatalogItems method..");
	}
	try {
		Repository catalog = getCatalog();
		String sortProp = getCatalogProperties().getIdPropertyName();
	    SortDirective sd = new SortDirective(sortProp,TRUCommerceConstants.INT_ONE);
	    SortDirectives sds = new SortDirectives();
	    sds.addDirective(sd);
		RepositoryView reporitoryView = catalog.getView(ItemType);
		QueryBuilder queryBuilder = reporitoryView.getQueryBuilder();
		QueryExpression propNameExp = queryBuilder.createPropertyQueryExpression(((TRUCatalogProperties)getCatalogProperties()).getWebDisplayFlag());
		QueryExpression propValeExp = queryBuilder.createConstantQueryExpression(Boolean.parseBoolean(TRUCommerceConstants.TRUE_FLAG));
		Query query = queryBuilder.createComparisonQuery(propNameExp, propValeExp, QueryBuilder.EQUALS);
		QueryOptions qops = new QueryOptions(pStartingIndex, pEndingIndex, sds, null);
		LoadingStrategyContext.pushLoadStrategy(TRUCommerceConstants.LAZY);
		RepositoryItem[] items = reporitoryView.executeQuery(query,qops);
		LoadingStrategyContext.popLoadStrategy();
		if(items != null && items.length>0){
			return Arrays.asList(items);
		}
	} catch (RepositoryException e) {
		if (isLoggingError()) {
			vlogError(e, "RepositoryException occured in  TRUCatalogTools: @Method: getAllCatalogItems");
		}
	}
	if (isLoggingDebug()) {
		vlogDebug("END:: TRUCatalogTools.getAllCatalogItems method..");
	}
	return null;
	}

	/**
	 * Gets the item count query.
	 *  
	 * @param ItemType - String- Item Type
	 * @param pPropertyExpression - String
	 * @param pPropertyValue - String
	 * @param pQueryOption - int
	 * @return int - Item Count
	 */
	public int getItemCountQuery(String ItemType,String pPropertyExpression,Object pPropertyValue,int pQueryOption ) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogTools.getAllCatalogItems method..");
		}
		int count = 0;
		try {
			Repository catalog = getCatalog();
			RepositoryView reporitoryView = catalog.getView(ItemType);
			QueryBuilder queryBuilder = reporitoryView.getQueryBuilder();
			Query query = null;
			if(QueryBuilder.EQUALS == pQueryOption){
				QueryExpression propNameExp = queryBuilder.createPropertyQueryExpression(pPropertyExpression);
				QueryExpression propValeExp = queryBuilder.createConstantQueryExpression(pPropertyValue);
				query = queryBuilder.createComparisonQuery(propNameExp, propValeExp, QueryBuilder.EQUALS);
				
			}else{
				query = queryBuilder.createUnconstrainedQuery();
			}
			count = reporitoryView.executeCountQuery(query);
			
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e,"RepositoryException occured in  TRUCatalogTools: @Method: getAllCatalogItems");
			}
			return count;
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogTools.getAllCatalogItems method..");
		}
		return count;
	} 
 
}
