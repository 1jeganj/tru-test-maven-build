/**
 * 
 */
package com.tru.commerce.catalog.customdescriptor;

import java.util.List;

import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/**
 * This custom class extend the OTB GSAPropertyDescriptor to set and get the tax code for stylized products.
 * @author Professional Access
 * @version 1.0
 */
public class TaxCodePropertyDescriptor extends GSAPropertyDescriptor {

	public static final String PRODUCT2 = "product";

	public static final String TAX_CODE = "taxCode";

	public static final String ORIGINAL_PARENT_PRODUCT = "originalParentProduct";

	/** The Constant CHILD_SKUS. */
	public static final String CHILD_SKUS = "childSKUs";

	/** The Constant STYLE_DIMENSIONS. */
	public static final String STYLE_DIMENSIONS = "styleDimensions";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6483464020681271586L;
	
	/** The Constant TYPE_NAME. */
	private static final String TYPE_NAME = "string";
	
	static {
		RepositoryPropertyDescriptor.registerPropertyDescriptorClass(TYPE_NAME, TaxCodePropertyDescriptor.class);
	}
	
	
	
	/* (non-Javadoc)
	 * @see atg.adapter.gsa.GSAPropertyDescriptor#getPropertyValue(atg.repository.RepositoryItemImpl, java.lang.Object)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		
		List<String> styleDims = (List<String>) pItem.getPropertyValue(TaxCodePropertyDescriptor.STYLE_DIMENSIONS);
		
		if(styleDims != null && !styleDims.isEmpty()){
			try {
				List<RepositoryItem> childSKUs = (List<RepositoryItem>) pItem.getPropertyValue(TaxCodePropertyDescriptor.CHILD_SKUS);
				for (RepositoryItem sku : childSKUs) {
					String originalParentProduct = (String) sku.getPropertyValue(TaxCodePropertyDescriptor.ORIGINAL_PARENT_PRODUCT);
					if(StringUtils.isNotBlank(originalParentProduct)){
						RepositoryItem product = getRepository().getItem(originalParentProduct, TaxCodePropertyDescriptor.PRODUCT2);
						if(product != null){
							String taxCode = (String) product.getPropertyValue(TaxCodePropertyDescriptor.TAX_CODE);
							if(StringUtils.isNotBlank(taxCode)){
								return taxCode;
							}
						}
					}
				}
			} catch (RepositoryException e) {
				if(isLoggingError()){
					logError(e);
				}
			}
		}
		
		return super.getPropertyValue(pItem, pValue);
	}

}
