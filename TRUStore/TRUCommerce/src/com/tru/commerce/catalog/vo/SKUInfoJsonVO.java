package com.tru.commerce.catalog.vo;

import java.util.List;
import java.util.Set;

/**
 * This class holds SKU related information.
 * 
 * @author PA
 * @version 1.0
 */
public class SKUInfoJsonVO {

	/**
	 * Property to hold is active.
	 */
	private Boolean mActive;

	/**
	 * Property to mAlterNateImages.
	 */
	private List<String> mAlterNateImages;
	
	/**
	 * Property to mAlterNateCanonicalImages.
	 */
	private List<String> mAlterNateCanonicalImages;

	
	/**
	 * Property to hold mAvailableInventory.
	 */
	private long mAvailableInventory;
	

	/**
	 * Property to hold mAvailableInventoryInStore.
	 */
	private long mAvailableInventoryInStore;

	/**
	 * Property to hold mColorCode.
	 */
	private String mColorCode;

	/**
	 * Property to hold mDeleted.
	 */
	private boolean mDeleted;

	/**
	 * Property to hold mIspu.
	 */
	private String mIspu;
	/**
	 * Property to hold mDisplayable.
	 */
	private boolean mDisplayable;

	/**
	 * Property to hold display name.
	 */
	private String mDisplayName;

	/**
	 * Property to hold mDropShipFlag.
	 */
	private boolean mDropShipFlag;

	/**
	 * Property to hold SKU id.
	 */
	private String mId;
	
	/**
	 * Property to hold mInventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**

	 * Property to hold mInventoryStatusInStore.
	 */
	private String mInventoryStatusInStore;
	
	/**
	 * Property to hold mJuvenileSize.
	 */
	private String mJuvenileSize;
	
	/**
	 * Property to hold mListPrice.
	 */
	private double mListPrice;
	
	/**
	 * Property to hold mNewItem.
	 */
	private boolean mNewItem;
	
	/**
	 * Property to hold mNotifyMe.
	 */
	private String mNotifyMe;
	
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mPresellable;
	
	/**
	 * Property to hold mEsrbrating.
	 */
	private String mEsrbrating;
	

	/** The m back order status. */
	private String mBackOrderStatus;
	
	/**
	 * Property to hold mVendorsProductDemoUrl.
	 */
	private String mVendorsProductDemoUrl;
	
	/**
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * @param pVendorsProductDemoUrl the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}
	
	/**
	 * Gets the mEsrbrating.
	 * 
	 * @return the mEsrbrating
	 */

	public String getEsrbrating() {
		return mEsrbrating;
	}
	
	/**
	 * Sets the mEsrbrating.
	 * 
	 * @param pEsrbrating the mEsrbrating to set
	 */

	public void setEsrbrating(String pEsrbrating) {
		this.mEsrbrating = pEsrbrating;
	}
	
	/**
	 * Gets the back order status.
	 * 
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		this.mBackOrderStatus = pBackOrderStatus;
	}
	
	/**
	 * Property to hold mPresellQuantityUnits.
	 */
	private String mPresellQuantityUnits;
	
	/**
	 * Property to hold mPriceDisplay.
	 */
	private String mPriceDisplay;

	/**
	 * Property to hold mPriceSavingAmount.
	 */
	private double mPriceSavingAmount;
	
	/**
	 * Property to hold mPriceSavingPercentage.
	 */
	private double mPriceSavingPercentage;
	
	/**
	 * Property to mPrimaryImage.
	 */
	private String mPrimaryImage;
	/**
	 * Property to mPrimaryCanonicalImage.
	 */
	private String mPrimaryCanonicalImage;
	
	/**
	 * Property to hold mS2s.
	 */
	private String mS2s;
	
	/**
	 * Property to hold mSalePrice.
	 */
	private double mSalePrice;
	
	/**
	 * Property to hold mSearchable.
	 */
	private boolean mSearchable;
	
	/**
	 * Property to mSecondaryImage.
	 */
	private String mSecondaryImage;
	/**
	 * Property to mSecondaryCanonicalImage.
	 */
	private String mSecondaryCanonicalImage;
	
	/**
	 * Property to hold mSkuType.
	 */
	private String mSkuType;
	
	/**
	 * Property to hold mStoreAvailablityMessage.
	 */
	private String mStoreAvailablityMessage;
	
	/**
	 * Property to hold mStoreMessage.
	 */
	private String mStoreMessage;
	
	/**
	 * Property to mPromoInfoList.
	 */
	private List<PromoInfoVO> mPromoInfoList;
	
	/**
	 * Property to hold mBatteryInfoJsonVO.
	 */
	private List<BatteryInfoJsonVO> mBatteryInfoJsonVO;
	
	/**
	 * Property to mPromotionCount.
	 */
	private int mPromotionCount;

	/**
	 * Property to hold mStreetDate.
	 */
	private String mStreetDate;
	
	/**
	 * Property to hold mSuggestAgeMessage.
	 */
	private String mSuggestAgeMessage;
	
	
	/**
	 * Property to mSwatchImage.
	 */
	private String mSwatchImage;
	
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mUnCartable;
	
	/**
	 * This property hold reference for mRmsSizeCode.
	 */
	private String mRmsSizeCode;
	
	/**
	 * This property hold reference for mRmsColorCode.
	 */
	private String mRmsColorCode;
	
	/**
	 * This property hold reference for mOriginalproductIdOrSKN.
	 */
	private String mSknId;
	
	/**
	 * This property hold reference for mSknOriginId.
	 */
	private int mSknOriginId;
	
	/**
	 * Property to mUpcNumbers.
	 */
	private Set<String> mSkuUpcNumbers;
	
	
	/**
	 * Property to hold  mShipWindowMax.
	 */
	private String mShipWindowMax;
	
	/**
	 * Property to hold  mShipWindowMin.
	 */
	private String mShipWindowMin;
	/**

	 * Property to hold  mSkuRegistryEligibility.
	 */
	private boolean mSkuRegistryEligibility;
	/**
	 * Property to hold  mRegistryWarningIndicator.
	 */
	private String mRegistryWarningIndicator;
	
	/**
	 * Property to hold mBrandName.
	 */
	private String mBrandName;
	
	/**
	 * Property to hold mChannelAvailability.
	 */
	private String mChannelAvailability;
	
	/**
	 * Gets the brandName.
	 * 
	 * @return the brandName
	 */
	public String getBrandName() {
		return mBrandName;
	}

	/**
	 * Sets the brandName.
	 * 
	 * @param pBrandName the brandName to set
	 */
	public void setBrandName(String pBrandName) {
		mBrandName = pBrandName;
	}
	
	/**
	 * @return the mBatteryInfoJsonVO
	 */
	public List<BatteryInfoJsonVO> getBatteryInfoJsonVO() {
		return mBatteryInfoJsonVO;
	}

	/**
	 * @param pBatteryInfoJsonVO the mBatteryInfoJsonVO to set
	 */
	public void setBatteryInfoJsonVO(List<BatteryInfoJsonVO> pBatteryInfoJsonVO) {
		mBatteryInfoJsonVO = pBatteryInfoJsonVO;
	}

	/**
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * @param pChannelAvailability the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * @return the ispu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the ispu to set
	 */
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}
	/**
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}
	/**
	 * @param pRegistryWarningIndicator the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}
	/**
	 * @return the skuRegistryEligibility
	 */
	public boolean isSkuRegistryEligibility() {
		return mSkuRegistryEligibility;
	}
	/**
	 * @param pSkuRegistryEligibility the skuRegistryEligibility to set
	 */
	public void setSkuRegistryEligibility(boolean pSkuRegistryEligibility) {
		mSkuRegistryEligibility = pSkuRegistryEligibility;
	}
	/**
	 * @return the alterNateCanonicalImages
	 */
	public List<String> getAlterNateCanonicalImages() {
		return mAlterNateCanonicalImages;
	}
	
	/**
	 * @param pAlterNateCanonicalImages the AlterNateCanonicalImages to set
	 */
	public void setAlterNateCanonicalImages(List<String> pAlterNateCanonicalImages) {
		mAlterNateCanonicalImages = pAlterNateCanonicalImages;
	}
	/**
	 * @return the secondaryCanonicalImage
	 */
	public String getSecondaryCanonicalImage() {
		return mSecondaryCanonicalImage;
	}
	
	/**
	 * @param pSecondaryCanonicalImage the secondaryCanonicalImage to set
	 */
	public void setSecondaryCanonicalImage(String pSecondaryCanonicalImage) {
		mSecondaryCanonicalImage = pSecondaryCanonicalImage;
	}
	/**
	 * @return the primaryCanonicalImage
	 */
	
	public String getPrimaryCanonicalImage(){
		return mPrimaryCanonicalImage;
	}
	/**
	 * @param pPrimaryCanonicalImage the primaryCanonicalImage to set
	 */
	public void setPrimaryCanonicalImage(String pPrimaryCanonicalImage) {
		mPrimaryCanonicalImage = pPrimaryCanonicalImage;
	}
	
	/**
	 * @return the shipWindowMax
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * @return the shipWindowMin
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}
	
	/**
	 * @param pShipWindowMax the shipWindowMax to set
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * @param pShipWindowMin the shipWindowMin to set
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	
	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public Boolean getActive() {
		return mActive;
	}

	/**
	 * @return the alterNateImages
	 */
	public List<String> getAlterNateImages() {
		return mAlterNateImages;
	}
	
	/**
	 * @return the availableInventory
	 */
	public long getAvailableInventory() {
		return mAvailableInventory;
	}

	/**
	 * @return the availableInventoryInStore
	 */
	public long getAvailableInventoryInStore() {
		return mAvailableInventoryInStore;
	}

	/**
	 * @return the colorCode
	 */
	public String getColorCode() {
		return mColorCode;
	}

	/**
	 * Gets the displayName.
	 * 
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * @return the inventoryStatus
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	
	/**
	 * @return the inventoryStatusInStore
	 */
	public String getInventoryStatusInStore() {
		return mInventoryStatusInStore;
	}

	/**
	 * @return the juvenileSize
	 */
	public String getJuvenileSize() {
		return mJuvenileSize;
	}

	
	/**
	 * @return the mListPrice
	 */
	public double getListPrice() {
		return mListPrice;
	}

	/**
	 * @return the notifyMe
	 */
	public String getNotifyMe() {
		return mNotifyMe;
	}

	/**
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}

	/**
	 * Returns the priceDisplay
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}

	/**
	 * @return the mPriceSavingAmount
	 */
	public double getPriceSavingAmount() {
		return mPriceSavingAmount;
	}

	/**
	 * @return the mPriceSavingPercentage
	 */
	public double getPriceSavingPercentage() {
		return mPriceSavingPercentage;
	}

	/**
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * @return the s2s
	 */
	public String getS2s() {
		return mS2s;
	}


	/**
	 * @return the mSalePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * @return the secondaryImage.
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}

	/**
	 * @return the skuType
	 */
	public String getSkuType() {
		return mSkuType;
	}

	/**
	 * @return the storeAvailablityMessage
	 */
	public String getStoreAvailablityMessage() {
		return mStoreAvailablityMessage;
	}

	/**
	 * @return the storeMessage
	 */
	public String getStoreMessage() {
		return mStoreMessage;
	}

	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}
	

	/**
	 * @return the suggestAgeMessage
	 */
	public String getSuggestAgeMessage() {
		return mSuggestAgeMessage;
	}

	/**
	 * @return the swatchImage.
	 */
	public String getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * @return the mDeleted
	 */
	public boolean isDeleted() {
		return mDeleted;
	}

	/**
	 * @return the mDisplayable
	 */
	public boolean isDisplayable() {
		return mDisplayable;
	}

	/**
	 * @return the dropShipFlag
	 */
	public boolean isDropShipFlag() {
		return mDropShipFlag;
	}

	/**
	 * @return the newItem
	 */
	public boolean isNewItem() {
		return mNewItem;
	}

	/**
	 * @return the presellable
	 */
	public boolean isPresellable() {
		return mPresellable;
	}

	/**
	 * @return the mSearchable
	 */
	public boolean isSearchable() {
		return mSearchable;
	}

	/**
	 * @return the unCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}

	/**
	 * Sets the active.
	 * 
	 * @param pActive the active to set
	 */
	public void setActive(Boolean pActive) {
		mActive = pActive;
	}

	/**
	 * @param pAlterNateImages the alterNateImages to set
	 */
	public void setAlterNateImages(List<String> pAlterNateImages) {
		mAlterNateImages = pAlterNateImages;
	}

	/**
	 * @param pAvailableInventory the availableInventory to set
	 */
	public void setAvailableInventory(long pAvailableInventory) {
		mAvailableInventory = pAvailableInventory;
	}
	
	/**
	 * @param pAvailableInventoryInStore the availableInventoryInStore to set
	 */
	public void setAvailableInventoryInStore(long pAvailableInventoryInStore) {
		mAvailableInventoryInStore = pAvailableInventoryInStore;
	}

	/**
	 * @param pColorCode the colorCode to set
	 */
	public void setColorCode(String pColorCode) {
		mColorCode = pColorCode;
	}

	/**
	 * @param pDeleted the mDeleted to set
	 */
	public void setDeleted(boolean pDeleted) {
		this.mDeleted = pDeleted;
	}


	/**
	 * @param pDisplayable the mDisplayable to set
	 */
	public void setDisplayable(boolean pDisplayable) {
		this.mDisplayable = pDisplayable;
	}

	/**
	 * Sets the displayName.
	 * 
	 * @param pDisplayName the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * @param pDropShipFlag the dropShipFlag to set
	 */
	public void setDropShipFlag(boolean pDropShipFlag) {
		mDropShipFlag = pDropShipFlag;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}


	/**
	 * @param pInventoryStatus the inventoryStatus to set
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		mInventoryStatus = pInventoryStatus;
	}

	/**
	 * @param pInventoryStatusInStore the inventoryStatusInStore to set
	 */
	public void setInventoryStatusInStore(String pInventoryStatusInStore) {
		mInventoryStatusInStore = pInventoryStatusInStore;
	}

	/**
	 * @param pJuvenileSize the juvenileSize to set
	 */
	public void setJuvenileSize(String pJuvenileSize) {
		mJuvenileSize = pJuvenileSize;
	}

	/**
	 * @param pListPrice the mListPrice to set
	 */
	public void setListPrice(double pListPrice) {
		this.mListPrice = pListPrice;
	}

	/**
	 * @param pNewItem the newItem to set
	 */
	public void setNewItem(boolean pNewItem) {
		mNewItem = pNewItem;
	}

	/**
	 * @param pNotifyMe the notifyMe to set
	 */
	public void setNotifyMe(String pNotifyMe) {
		mNotifyMe = pNotifyMe;
	}


	/**
	 * @param pPresellable the presellable to set
	 */
	public void setPresellable(boolean pPresellable) {
		mPresellable = pPresellable;
	}

	/**
	 * @param pPresellQuantityUnits the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}

	/**
	 * Sets/updates the priceDisplay.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * @param pPriceSavingAmount the mPriceSavingAmount to set
	 */
	public void setPriceSavingAmount(double pPriceSavingAmount) {
		this.mPriceSavingAmount = pPriceSavingAmount;
	}

	/**
	 * @param pPriceSavingPercentage the mPriceSavingPercentage to set
	 */
	public void setPriceSavingPercentage(double pPriceSavingPercentage) {
		this.mPriceSavingPercentage = pPriceSavingPercentage;
	}

	/**
	 * @param pPrimaryImage the primaryImage to set.
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}

	/**
	 * @param pS2s the s2s to set
	 */
	public void setS2s(String pS2s) {
		mS2s = pS2s;
	}

	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}

	/**
	 * @param pSearchable the mSearchable to set
	 */
	public void setSearchable(boolean pSearchable) {
		this.mSearchable = pSearchable;
	}

	/**
	 * @param pSecondaryImage the secondaryImage to set.
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}

	/**
	 * @param pSkuType the skuType to set
	 */
	public void setSkuType(String pSkuType) {
		mSkuType = pSkuType;
	}

	/**
	 * @param pStoreAvailablityMessage the storeAvailablityMessage to set
	 */
	public void setStoreAvailablityMessage(String pStoreAvailablityMessage) {
		mStoreAvailablityMessage = pStoreAvailablityMessage;
	}
	
	/**
	 * @param pStoreMessage the storeMessage to set
	 */
	public void setStoreMessage(String pStoreMessage) {
		mStoreMessage = pStoreMessage;
	}

	/**
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @param pSuggestAgeMessage the suggestAgeMessage to set
	 */
	public void setSuggestAgeMessage(String pSuggestAgeMessage) {
		mSuggestAgeMessage = pSuggestAgeMessage;
	}

	/**
	 * @param pSwatchImage the swatchImage to set.
	 */
	public void setSwatchImage(String pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}

	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mId;
	}
	
	/**
	 * @return the promotionCount
	 */
	public int getPromotionCount() {
		return mPromotionCount;
	}

	/**
	 * @param pPromotionCount the promotionCount to set
	 */
	public void setPromotionCount(int pPromotionCount) {
		mPromotionCount = pPromotionCount;
	}

	/**
	 * @return the promoInfoList
	 */
	public List<PromoInfoVO> getPromoInfoList() {
		return mPromoInfoList;
	}

	/**
	 * @param pPromoInfoList the promoInfoList to set
	 */
	public void setPromoInfoList(List<PromoInfoVO> pPromoInfoList) {
		mPromoInfoList = pPromoInfoList;
	}

	/**
	 * @return the rmsSizeCode
	 */
	public String getRmsSizeCode() {
		return mRmsSizeCode;
	}

	/**
	 * @param pRmsSizeCode the rmsSizeCode to set
	 */
	public void setRmsSizeCode(String pRmsSizeCode) {
		mRmsSizeCode = pRmsSizeCode;
	}

	/**
	 * @return the rmsColorCode
	 */
	public String getRmsColorCode() {
		return mRmsColorCode;
	}

	/**
	 * @param pRmsColorCode the rmsColorCode to set
	 */
	public void setRmsColorCode(String pRmsColorCode) {
		mRmsColorCode = pRmsColorCode;
	}

	/**
	 * @return the sknId
	 */
	public String getSknId() {
		return mSknId;
	}

	/**
	 * @param pSknId the sknId to set
	 */
	public void setSknId(String pSknId) {
		mSknId = pSknId;
	}

	/**
	 * @return the sknOriginId
	 */
	public int getSknOriginId() {
		return mSknOriginId;
	}

	/**
	 * @param pSknOriginId the sknOriginId to set
	 */
	public void setSknOriginId(int pSknOriginId) {
		mSknOriginId = pSknOriginId;
	}

	/**
	 * @return the skuUpcNumbers
	 */
	public Set<String> getSkuUpcNumbers() {
		return mSkuUpcNumbers;
	}

	/**
	 * @param pSkuUpcNumbers the skuUpcNumbers to set
	 */
	public void setSkuUpcNumbers(Set<String> pSkuUpcNumbers) {
		mSkuUpcNumbers = pSkuUpcNumbers;
	}
	
}
