package com.tru.commerce.catalog.vo;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This class holds Color related information.
 * @author Professional Access
 * @version 1.0
 */
public class ColorInfoVO {

	/** Property to hold Color id.
	 * 
	 */
	private String mId;

	/**
	 * 
	 * 
	 * Property to hold color name.
	 */
	private String mName;

	/**
	 * Property to hold image URl.
	 */
	private String mImageURL;
	/** 
	 * Gets the id.
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * 
	 * Sets the id.
	 * @param pId - the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * 
	 * Gets the name.
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * 
	 * Sets the name.
	 * @param pName the name to set
	 */
	public void setName(String pName) {
		mName = pName;
	}

	/**
	 * 
	 * Gets the imageURL.
	 * @return the imageURL
	 */
	public String getImageURL() {
		return mImageURL;
	}

	/**
	 * 
	 * Sets the imageURL.
	 * @param pImageURL - the imageURL to set
	 */
	public void setImageURL(String pImageURL) {
		mImageURL = pImageURL;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		final int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + (mId == null ? 0 : mId.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		ColorInfoVO other = (ColorInfoVO) pObject;
		if (mId == null) {
			if (other.mId != null) {
				return false;
			}
		} else if (!mId.equals(other.mId)) {
			return false;
		}
		return true;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mId;
	}
}
