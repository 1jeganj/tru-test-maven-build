package com.tru.commerce.catalog.vo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class holds product related information.
 * 
 * @author PA
 * @version 1.0
 */
public class ProductInfoVO extends CommonPropertyVO {
	
	/**
	 * Property to hold mDescription.
	 */
	private String mDescription;

	/**
	 * Property to hold mBrandName.
	 */
	private String mBrandName;

	/**
	 * Property to hold mBrandLandingURL.
	 */
	private String mBrandLandingURL;

	/**
	 * Property to hold Model.
	 */
	private String mModel;

	/**
	 * Property to hold mVideoURL.
	 */
	private String mVideoURL;

	/**
	 * Property to hold mDefaultColorId.
	 */
	private String mDefaultColorId;

	/**
	 * Property to hold mDefaultColorId.
	 */
	private String mSizeChartURL;

	/**
	 * Property to hold mActive.
	 */
	/*private boolean mActive;*/

	/**
	 * Property to hold mDefaultSKU.
	 */
	private SKUInfoVO mDefaultSKU;

	/**
	 * Property to hold mSeoTitle.
	 */
	private String mSeoTitle;

	/**
	 * Property to hold mSeoDisplayName.
	 */
	private String mSeoDisplayName;

	/**
	 * Property to hold mSeoDescription.
	 */
	private String mSeoDescription;

	/**
	 * Property to hold mSeoKeywords.
	 */
	private String mSeoKeywords;

	/**
	 * Property to hold mPdpURL.
	 */
	private String mPdpURL;
	
	/**
	 * Property to hold mOnlinePid.
	 */
	private String mOnlinePid;
	
	/**
	 * Property to hold mBatteries.
	 */
	private List<BatteryVO> mBatteries;
	
	/**
	 * @return the batteries
	 */
	public List<BatteryVO> getBatteries() {
		return mBatteries;
	}
	
	/**
	 * @param pBatteries the batteries to set
	 */
	public void setBatteries(List<BatteryVO> pBatteries) {
		mBatteries = pBatteries;
	}

	/**
	 * Property to hold mColorSkusMap.
	 */
	private Map<String, List<SKUInfoVO>> mColorSkusMap;

	/**
	 * Property to hold mSizeSkusMap.
	 */
	private Map<String, List<SKUInfoVO>> mSizeSkusMap;

	/**
	 * Property to hold mColorSwatchImageList.
	 */
	private  Set<String> mColorSwatchImageList;
	
	/**
	 * Property to hold mColorCodeList.
	 */
	/*private  List<String> mColorCodeList;
	
	
	*//**
	 * Property to hold mJuvenileSizesList.
	 *//*
	private List<String> mJuvenileSizesList;
	*/
	
	/**
	 * Property to hold mChildSKUsList.
	 */
	private  List<SKUInfoVO> mChildSKUsList;

	/**
	 * Property to hold mActiveSKUsList.
	 */
	private  List<SKUInfoVO> mActiveSKUsList;
	
	/**
	 * Property to hold mInActiveSKUsList.
	 */
	private  List<SKUInfoVO> mInActiveSKUsList;
	
	/**
	 * Property to hold mInStockSKUsList.
	 */
	private  List<SKUInfoVO> mInStockSKUsList;
	

	/**
	 * Property to hold mOutofStockSKUsList.
	 */
	private  List<SKUInfoVO> mOutofStockSKUsList;
	
	/**
	 * Property to hold mSelectedColorSKUsList.
	 */
	private  List<SKUInfoVO> mSelectedColorSKUsList;
	
	/**
	 * Property to hold mSelectedSizeSKUsList.
	 */
	private  List<SKUInfoVO> mSelectedSizeSKUsList;

	/**
	 * Property to hold mColorSizes.
	 */
	private Map<ColorInfoVO, List<SKUInfoVO>> mColorSizes;

	/**
	 * Property to hold mColorSizeVariantsAvailableStatus.
	 */
	/*private  String mColorSizeVariantsAvailableStatus;*/
	
	/**
	 * Property to hold mColorCodeToSwatchImageMap.
	 */
	private Map<String, String> mColorCodeToSwatchImageMap;

	
	/**
	 * Property to hold mRusItemNumber.
	 */
	/*private String mRusItemNumber;*/
	/**
	 * Property to hold mManufacturerStyleNumber.
	 */
	private String mManufacturerStyleNumber;
	/**
	 * Property to hold mSizeChartName.
	 */
	private String mSizeChartName;
	/**
	 * Property to hold HaveRelateditems. 
	 */
	private boolean mHaveRelateditems;
	
	/**
	 * Property to hold size chart.
	 */
	private String mSizeChart;
	/**
	 * Property to hold mBatteryRequired.
	 */
	private String mBatteryRequired;
	/**
	 * Property to hold mBatteryRequired.
	 */
	private String mBatteryIncluded;
	/**
	 * Property to hold mBatteryRequired.
	 */
	private String mEwasteSurchargeSku;
	
	/**
	 * Property to hold mEwasteSurchargeState.
	 */
	private String mEwasteSurchargeState;
	
	/**
	 * Property to hold mBrand.
	 */
	private String mBrand;
	
	/**
	 * Property to hold mVendorsProductDemoUrl.
	 */
	private String mVendorsProductDemoUrl;
	
	/**
	 * Property to hold mChannelAvailability.
	 */
	/*private String mChannelAvailability;*/
	
	/**
	 * Property to hold mWhyWeloveIt.
	 */
	private String mWhyWeloveIt;
	
	/**
	 * Property to hold mSfs.
	 */
	/*private String mSfs;*/
	
	/**
	 * Property to hold mIspu.
	 */
	/*private String mIspu;*/
	
	/**
	 * Property to hold mProductStatus.
	 */
	/*private String mProductStatus;*/
	
		
	
	/**
	 * Property to hold mThingsToKnow.
	 */
	
	private List<String> mThingsToKnow;
	
	/**
	 * Property to hold mBreadcrumb.
	 */
	private List<BreadcrumbInfoVO> mBreadcrumb;
	
	/**
	 * Property to hold mComparableMap.
	 */
	private Map mComparableMap;

	/**
	 * Property to hold mDisplayable.
	 */
	private boolean mDisplayable;
	
	/**
	 * Property to hold mSearchable.
	 */
	private boolean mSearchable;
	
	/**
	 * Property to hold siteId.
	 */
	private String mSiteId;
	
	/**
	 * @return the mDisplayable
	 */
	public boolean isDisplayable() {
		return mDisplayable;
	}

	/**
	 * @param pDisplayable the mDisplayable to set
	 */
	public void setDisplayable(boolean pDisplayable) {
		this.mDisplayable = pDisplayable;
	}

	/**
	 * @return the mSearchable
	 */
	public boolean isSearchable() {
		return mSearchable;
	}

	/**
	 * @param pSearchable the mSearchable to set
	 */
	public void setSearchable(boolean pSearchable) {
		this.mSearchable = pSearchable;
	}

	
	/**
	 * 
	 * @return mSizeChart
	 */
	public String getSizeChart() {
		return mSizeChart;
	}
	
	/**
	 * Sets the sizeChart.
	 * 
	 * @param pSizeChart the sizeChart to set
	 */
	public void setSizeChart(String pSizeChart) {
		this.mSizeChart = pSizeChart;
	}
	
	/**
	 * Gets the sizeChartName.
	 * 
	 * @return the sizeChartName
	 */
	public String getSizeChartName() {
		return mSizeChartName;
	}
	
	/**
	 * Sets the sizeChartName.
	 * 
	 * @param pSizeChartName the sizeChartName to set
	 */
	public void setSizeChartName(String pSizeChartName) {
		mSizeChartName = pSizeChartName;
	}
	
	/**
	 * Gets the rusItemNumber.
	 * 
	 * @return the rusItemNumber
	 */
	/*public String getRusItemNumber() {
		return mRusItemNumber;
	}
		
	*//**
	 * Sets the rusItemNumber.
	 * 
	 * @param pRusItemNumber the rusItemNumber to set
	 *//*
	public void setRusItemNumber(String pRusItemNumber) {
		mRusItemNumber = pRusItemNumber;
	}*/
	
	/**
	 * Gets the manufacturerStyleNumber.
	 * 
	 * @return the manufacturerStyleNumber
	 */
	public String getManufacturerStyleNumber() {
		return mManufacturerStyleNumber;
	}
	
	/**
	 * Sets the manufacturerStyleNumber.
	 * 
	 * @param pManufacturerStyleNumber the manufacturerStyleNumber to set
	 */
	public void setManufacturerStyleNumber(String pManufacturerStyleNumber) {
		mManufacturerStyleNumber = pManufacturerStyleNumber;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * Sets the description.
	 * 
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}
	
	/**
	 * Gets the brand.
	 * 
	 * @return the brand
	 */
	public String getBrand() {
		return mBrand;
	}
	
	/**
	 * Sets the brand.
	 * 
	 * @param pBrand the brand to set
	 */
	public void setBrand(String pBrand) {
		mBrand = pBrand;
	}

	/**
	 * Gets the brandName.
	 * 
	 * @return the brandName
	 */
	public String getBrandName() {
		return mBrandName;
	}

	/**
	 * Sets the brandName.
	 * 
	 * @param pBrandName the brandName to set
	 */
	public void setBrandName(String pBrandName) {
		mBrandName = pBrandName;
	}

	/**
	 * Gets the brandLandingURL.
	 * 
	 * @return the brandLandingURL
	 */
	public String getBrandLandingURL() {
		return mBrandLandingURL;
	}

	/**
	 * Sets the brandLandingURL.
	 * 
	 * @param pBrandLandingURL the brandLandingURL to set
	 */
	public void setBrandLandingURL(String pBrandLandingURL) {
		mBrandLandingURL = pBrandLandingURL;
	}

	/**
	 * Gets the model.
	 * 
	 * @return the model
	 */
	public String getModel() {
		return mModel;
	}

	/**
	 * Sets the model.
	 * 
	 * @param pModel the model to set
	 */
	public void setModel(String pModel) {
		mModel = pModel;
	}
	
	/**
	 * Gets the batteryIncluded.
	 * 
	 * @return the batteryIncluded
	 */
	public String getBatteryIncluded() {
		return mBatteryIncluded;
	}

	/**
	 * Sets the batteryIncluded.
	 * 
	 * @param pBatteryIncluded the batteryIncluded to set
	 */
	public void setBatteryIncluded(String pBatteryIncluded) {
		mBatteryIncluded = pBatteryIncluded;
	}
	/**
	 * Gets the ewasteSurchargeSku.
	 * 
	 * @return the ewasteSurchargeSku
	 */
	public String getEwasteSurchargeSku() {
		return mEwasteSurchargeSku;
	}

	/**
	 * Sets the ewasteSurchargeSku.
	 * 
	 * @param pEwasteSurchargeSku the ewasteSurchargeSku to set
	 */
	public void setEwasteSurchargeSku(String pEwasteSurchargeSku) {
		mEwasteSurchargeSku = pEwasteSurchargeSku;
	}
	
	/**
	 * Gets the ewasteSurchargeState.
	 * 
	 * @return the ewasteSurchargeState
	 */
	public String getEwasteSurchargeState() {
		return mEwasteSurchargeState;
	}

	/**
	 * Sets the ewasteSurchargeSku.
	 * 
	 * @param pEwasteSurchargeState the ewasteSurchargeState to set
	 */
	public void setEwasteSurchargeState(String pEwasteSurchargeState) {
		mEwasteSurchargeState = pEwasteSurchargeState;
	}

	/**
	 * Gets the videoURL.
	 * 
	 * @return the videoURL
	 */
	public String getVideoURL() {
		return mVideoURL;
	}

	/**
	 * Sets the videoURL.
	 * 
	 * @param pVideoURL the videoURL to set
	 */
	public void setVideoURL(String pVideoURL) {
		mVideoURL = pVideoURL;
	}
	
	/**
	 * Gets the batteryIncluded.
	 * 
	 * @return the batteryIncluded
	 */
	public String getBatteryRequired() {
		return mBatteryRequired;
	}
	
	/**
	 * Sets the batteryRequired.
	 * 
	 * @param pBatteryRequired the batteryRequired to set
	 */
	public void setBatteryRequired(String pBatteryRequired) {
		mBatteryRequired = pBatteryRequired;
	}
	
	/**
	 * Gets the haveRelateditems.
	 * 
	 * @return the haveRelateditems
	 */
	public boolean isHaveRelateditems() {
		return mHaveRelateditems;
	}
	
	/**
	 * Sets the haveRelateditems.
	 * 
	 * @param pHaveRelateditems the haveRelateditems to set
	 */
	public void setHaveRelateditems(boolean pHaveRelateditems) {
		mHaveRelateditems = pHaveRelateditems;
	}

	/**
	 * Gets the defaultColorId.
	 * 
	 * @return the defaultColorId
	 */
	public String getDefaultColorId() {
		return mDefaultColorId;
	}

	/**
	 * Sets the defaultColorId.
	 * 
	 * @param pDefaultColorId the defaultColorId to set
	 */
	public void setDefaultColorId(String pDefaultColorId) {
		mDefaultColorId = pDefaultColorId;
	}


	/**
	 * Gets the sizeChartURL.
	 * 
	 * @return the sizeChartURL
	 */
	public String getSizeChartURL() {
		return mSizeChartURL;
	}

	/**
	 * Sets the sizeChartURL.
	 * 
	 * @param pSizeChartURL the sizeChartURL to set
	 */
	public void setSizeChartURL(String pSizeChartURL) {
		mSizeChartURL = pSizeChartURL;
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 *//*
	public boolean isActive() {
		return mActive;
	}

	*//**
	 * Sets the active.
	 * 
	 * @param pActive the active to set
	 *//*
	public void setActive(boolean pActive) {
		mActive = pActive;
	}*/


	/**
	 * Gets the defaultSKU.
	 * 
	 * @return the defaultSKU
	 */
	public SKUInfoVO getDefaultSKU() {
		return mDefaultSKU;
	}

	/**
	 * Sets the defaultSKU.
	 * 
	 * @param pDefaultSKU the defaultSKU to set
	 */
	public void setDefaultSKU(SKUInfoVO pDefaultSKU) {
		mDefaultSKU = pDefaultSKU;
	}

	/**
	 * Gets the seoTitle.
	 * 
	 * @return the seoTitle
	 */
	public String getSeoTitle() {
		return mSeoTitle;
	}

	/**
	 * Sets the seoTitle.
	 * 
	 * @param pSeoTitle the seoTitle to set
	 */
	public void setSeoTitle(String pSeoTitle) {
		mSeoTitle = pSeoTitle;
	}

	/**
	 * Gets the seoDisplayName.
	 * 
	 * @return the seoDisplayName
	 */
	public String getSeoDisplayName() {
		return mSeoDisplayName;
	}

	/**
	 * Sets the seoDisplayName.
	 * 
	 * @param pSeoDisplayName the seoDisplayName to set
	 */
	public void setSeoDisplayName(String pSeoDisplayName) {
		mSeoDisplayName = pSeoDisplayName;
	}

	/**
	 * Gets the seoDescription.
	 * 
	 * @return the seoDescription
	 */
	public String getSeoDescription() {
		return mSeoDescription;
	}

	/**
	 * Sets the seoDescription.
	 * 
	 * @param pSeoDescription the seoDescription to set
	 */
	public void setSeoDescription(String pSeoDescription) {
		mSeoDescription = pSeoDescription;
	}

	/**
	 * Gets the seoKeywords.
	 * 
	 * @return the seoKeywords
	 */
	public String getSeoKeywords() {
		return mSeoKeywords;
	}

	/**
	 * Sets the seoKeywords.
	 * 
	 * @param pSeoKeywords the seoKeywords to set
	 */
	public void setSeoKeywords(String pSeoKeywords) {
		mSeoKeywords = pSeoKeywords;
	}

	/**
	 * Gets the pdpURL.
	 * 
	 * @return the pdpURL
	 */
	public String getPdpURL() {
		return mPdpURL;
	}

	/**
	 * Sets the pdpURL.
	 * 
	 * @param pPdpURL the pdpURL to set
	 */
	public void setPdpURL(String pPdpURL) {
		mPdpURL = pPdpURL;
	}

	/**
	 * Gets the colorSizes
	 * 
	 * @return the colorSizes
	 */
	public Map<ColorInfoVO, List<SKUInfoVO>> getColorSizes() {
		return mColorSizes;
	}

	/**
	 * Sets the colorSizes.
	 * 
	 * @param pColorSizes the colorSizes to set
	 */
	public void setColorSizes(Map<ColorInfoVO, List<SKUInfoVO>> pColorSizes) {
		mColorSizes = pColorSizes;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mProductId;
	}

	/**
	 * @return the colorSkusMap
	 */
	public Map<String, List<SKUInfoVO>> getColorSkusMap() {
		return mColorSkusMap;
	}

	/**
	 * @param pColorSkusMap the colorSkusMap to set
	 */
	public void setColorSkusMap(Map<String, List<SKUInfoVO>> pColorSkusMap) {
		mColorSkusMap = pColorSkusMap;
	}

	/**
	 * @return the sizeSkusMap
	 */
	public Map<String, List<SKUInfoVO>> getSizeSkusMap() {
		return mSizeSkusMap;
	}

	/**
	 * @param pSizeSkusMap the sizeSkusMap to set
	 */
	public void setSizeSkusMap(Map<String, List<SKUInfoVO>> pSizeSkusMap) {
		mSizeSkusMap = pSizeSkusMap;
	}


	/**
	 * @return the juvenileSizesList
	 *//*
	public List<String> getJuvenileSizesList() {
		return mJuvenileSizesList;
	}

	*//**
	 * @param pJuvenileSizesList the juvenileSizesList to set
	 *//*
	public void setJuvenileSizesList(List<String> pJuvenileSizesList) {
		mJuvenileSizesList = pJuvenileSizesList;
	}*/

	/**
	 * @return the childSKUsList
	 */
	public List<SKUInfoVO> getChildSKUsList() {
		return mChildSKUsList;
	}

	/**
	 * @param pChildSKUsList the childSKUsList to set
	 */
	public void setChildSKUsList(List<SKUInfoVO> pChildSKUsList) {
		mChildSKUsList = pChildSKUsList;
	}

	/**
	 * @return the activeSKUsList
	 */
	public List<SKUInfoVO> getActiveSKUsList() {
		return mActiveSKUsList;
	}

	/**
	 * @param pActiveSKUsList the activeSKUsList to set
	 */
	public void setActiveSKUsList(List<SKUInfoVO> pActiveSKUsList) {
		mActiveSKUsList = pActiveSKUsList;
	}

	/**
	 * @return the colorSizeVariantsAvailableStatus
	 *//*
	public String getColorSizeVariantsAvailableStatus() {
		return mColorSizeVariantsAvailableStatus;
	}

	*//**
	 * @param pColorSizeVariantsAvailableStatus the colorSizeVariantsAvailableStatus to set
	 *//*
	public void setColorSizeVariantsAvailableStatus(
			String pColorSizeVariantsAvailableStatus) {
		mColorSizeVariantsAvailableStatus = pColorSizeVariantsAvailableStatus;
	}*/

	/**
	 * @return the colorSwatchImageList
	 */
	public Set<String> getColorSwatchImageList() {
		return mColorSwatchImageList;
	}

	/**
	 * @param pColorSwatchImageList the colorSwatchImageList to set
	 */
	public void setColorSwatchImageList(Set<String> pColorSwatchImageList) {
		mColorSwatchImageList = pColorSwatchImageList;
	}

	

	/**
	 * @return the colorCodeList
	 *//*
	public List<String> getColorCodeList() {
		return mColorCodeList;
	}

	*//**
	 * @param pColorCodeList the colorCodeList to set
	 *//*
	public void setColorCodeList(List<String> pColorCodeList) {
		mColorCodeList = pColorCodeList;
	}*/

	/**
	 * @return the selectedColorSKUsList
	 */
	public List<SKUInfoVO> getSelectedColorSKUsList() {
		return mSelectedColorSKUsList;
	}

	/**
	 * @param pSelectedColorSKUsList the selectedColorSKUsList to set
	 */
	public void setSelectedColorSKUsList(List<SKUInfoVO> pSelectedColorSKUsList) {
		mSelectedColorSKUsList = pSelectedColorSKUsList;
	}

	/**
	 * @return the selectedSizeSKUsList
	 */
	public List<SKUInfoVO> getSelectedSizeSKUsList() {
		return mSelectedSizeSKUsList;
	}

	/**
	 * @param pSelectedSizeSKUsList the selectedSizeSKUsList to set
	 */
	public void setSelectedSizeSKUsList(List<SKUInfoVO> pSelectedSizeSKUsList) {
		mSelectedSizeSKUsList = pSelectedSizeSKUsList;
	}

	/**
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * @param pVendorsProductDemoUrl the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}

	/**
	 * @return the channelAvailability
	 */
	/*public String getChannelAvailability() {
		return mChannelAvailability;
	}

	*//**
	 * @param pChannelAvailability the channelAvailability to set
	 *//*
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}*/

	/**
	 * @return the colorCodeToSwatchImageMap
	 */
	public Map<String, String> getColorCodeToSwatchImageMap() {
		return mColorCodeToSwatchImageMap;
	}

	/**
	 * @param pColorCodeToSwatchImageMap the colorCodeToSwatchImageMap to set
	 */
	public void setColorCodeToSwatchImageMap(
			Map<String, String> pColorCodeToSwatchImageMap) {
		mColorCodeToSwatchImageMap = pColorCodeToSwatchImageMap;
	}

	/**
	 * @return the whyWeloveIt
	 */
	public String getWhyWeloveIt() {
		return mWhyWeloveIt;
	}

	/**
	 * @param pWhyWeloveIt the whyWeloveIt to set
	 */
	public void setWhyWeloveIt(String pWhyWeloveIt) {
		mWhyWeloveIt = pWhyWeloveIt;
	}

	/**
	 * @return the inActiveSKUsList
	 */
	public List<SKUInfoVO> getInActiveSKUsList() {
		return mInActiveSKUsList;
	}

	/**
	 * @param pInActiveSKUsList the inActiveSKUsList to set
	 */
	public void setInActiveSKUsList(List<SKUInfoVO> pInActiveSKUsList) {
		mInActiveSKUsList = pInActiveSKUsList;
	}

	/**
	 * @return the sfs
	 *//*
	public String getSfs() {
		return mSfs;
	}

	*//**
	 * @param pSfs the sfs to set
	 *//*
	public void setSfs(String pSfs) {
		mSfs = pSfs;
	}*/

	/**
	 * @return the ispu
	 *//*
	public String getIspu() {
		return mIspu;
	}

	*//**
	 * @param pIspu the ispu to set
	 *//*
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}*/

	/**
	 * @return the mProductStatus
	 */
	/*public String getProductStatus() {
		return mProductStatus;
	}

	*//**
	 * @param pProductStatus to set
	 *//*
	public void setProductStatus(String pProductStatus) {
		this.mProductStatus = pProductStatus;
	}*/

	/**
	 * @return the breadcrumb
	 */
	public List<BreadcrumbInfoVO> getBreadcrumb() {
		return mBreadcrumb;
	}

	/**
	 * @param pBreadcrumb to set
	 */
	public void setBreadcrumb(List<BreadcrumbInfoVO> pBreadcrumb) {
		mBreadcrumb = pBreadcrumb;
	}

	/**
	 * @return the thingsToKnow
	 */
	public List<String> getThingsToKnow() {
		return mThingsToKnow;
	}

	/**
	 * @param pThingsToKnow the thingsToKnow to set
	 */
	public void setThingsToKnow(List<String> pThingsToKnow) {
		mThingsToKnow = pThingsToKnow;
	}

	/**
	 * @return the mComparableMap
	 */
	public Map getComparableMap() {
		return mComparableMap;
	}

	/**
	 * @param pComparableMap the ComparableMap to set
	 */
	public void setComparableMap(Map pComparableMap) {
		this.mComparableMap = pComparableMap;
	}

	/**
	 * @return the mInStockSKUsList
	 */
	public List<SKUInfoVO> getInStockSKUsList() {
		return mInStockSKUsList;
	}

	/**
	 * @param pInStockSKUsList the mInStockSKUsList to set
	 */
	public void setInStockSKUsList(List<SKUInfoVO> pInStockSKUsList) {
		this.mInStockSKUsList = pInStockSKUsList;
	}

	/**
	 * @return the mOutofStockSKUsList
	 */
	public List<SKUInfoVO> getOutofStockSKUsList() {
		return mOutofStockSKUsList;
	}

	/**
	 * @param pOutofStockSKUsList the mOutofStockSKUsList to set
	 */
	public void setOutofStockSKUsList(List<SKUInfoVO> pOutofStockSKUsList) {
		this.mOutofStockSKUsList = pOutofStockSKUsList;
	}

	/**
	 * @return the mOnlinePid
	 */
	public String getOnlinePid() {
		return mOnlinePid;
	}

	/**
	 * @param pOnlinePid the mOnlinePid to set
	 */
	public void setOnlinePid(String pOnlinePid) {
		this.mOnlinePid = pOnlinePid;
	}

	/**
	 * @return the mSiteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * @param pSiteId the mSiteId to set
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}
	/**
     * This property hold reference for mSeoCanonicalURL.
     */
    private String mSeoCanonicalURL;
    /**
     * This property hold reference for mSeoAlternateURL.
     */
    private String mSeoAlternateURL;    
	  /**
     * @return the seoCanonicalURL
     */
    public String getSeoCanonicalURL() {
            return mSeoCanonicalURL;
    }

    /**
     * @param pSeoCanonicalURL the seoCanonicalURL to set
     */
    public void setSeoCanonicalURL(String pSeoCanonicalURL) {
            mSeoCanonicalURL = pSeoCanonicalURL;
    }

    /**
     * @return the seoAlternateURL
     */
    public String getSeoAlternateURL() {
            return mSeoAlternateURL;
    }

    /**
     * @param pSeoAlternateURL the seoAlternateURL to set
     */
    public void setSeoAlternateURL(String pSeoAlternateURL) {
            mSeoAlternateURL = pSeoAlternateURL;
    }
	
}
