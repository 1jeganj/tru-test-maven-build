package com.tru.commerce.catalog.vo;

import java.util.List;

/**
 * This class holds collection product related information.
 * 
 * @author PA
 * @version 1.0
 */
public class CompareProductInfoVO {


	/**
	 * Property to hold  product Info.
	 */
	private List<ProductInfoVO> mProductInfoList;
	
	/**
	 * 
	 * @return mProductInfoList
	 */
	public List<ProductInfoVO> getProductInfoList() {
		return mProductInfoList;
	}

	/**
	 * 
	 * @param pProductInfoList productInfoList
	 */
	public void setProductInfoList(List<ProductInfoVO> pProductInfoList) {
		mProductInfoList = pProductInfoList;
	}


	
}
