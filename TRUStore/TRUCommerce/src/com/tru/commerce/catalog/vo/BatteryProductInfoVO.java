package com.tru.commerce.catalog.vo;


/**
 * This class holds battery product related information.
 * @author PA
 * @version 1.0
 */
public class BatteryProductInfoVO  extends CommonPropertyVO{

	/**
	 * Property to hold sku id.
	 */
	private String mSkuId;
	/**
	 * Property to mPrimaryImage.
	 */
	private String mPrimaryImage;
	
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mUnCartable;

	/**
	 * Property to hold mDisplayName.
	 */
	private String mPdpURL;
	
	/**
	 * Property to hold mOnlinePid.
	 */
	private String mOnlinePid;

	/**
	 * Property to hold mActiveProduct.
	 */
	private boolean mActiveProduct;
	
	/**
	 * Property to hold listPrice.
	 */
	private double mListPrice;
	/**
	 * Property to hold salePrice.
	 */
	private double mSalePrice;
	
	/**
	 * Property to hold mNotifyMe.
	 */
	private String mNotifyMe;
	/**
	 * Property to hold mPresellable.
	 */
	private boolean mPresellable;
	/**
	 * Property to hold mS2s.
	 */
	private String mS2s;
	
	/**
	 * Property to hold mAvailableInventory.
	 */
	private long mAvailableInventory;

	/**
	 * Property to hold mAvailableInventoryInStore.
	 */
	private long mAvailableInventoryInStore;
	
	/**
	 * Property to hold mInventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**

	 * Property to hold mInventoryStatusInStore.
	 */
	private String mInventoryStatusInStore;
	
	
	/**
	 * Property to hold mStreetDate.
	 */
	private String mStreetDate;

	/**
	 * Property to hold mPresellQuantityUnits.
	 */
	private String mPresellQuantityUnits;
	

	/** The m back order status. */
	private String mBackOrderStatus;

	/**
	 * Property to hold LongDescription.
	 */
	private String mLongDescription;
	/**
	 * This property hold reference for mOriginalproductIdOrSKN.
	 */
	private String mOriginalproductIdOrSKN;
	/**
	 * @return the originalproductIdOrSKN
	 */
	public String getOriginalproductIdOrSKN() {
		return mOriginalproductIdOrSKN;
	}

	/**
	 * @param pOriginalproductIdOrSKN the originalproductIdOrSKN to set
	 */
	public void setOriginalproductIdOrSKN(String pOriginalproductIdOrSKN) {
		mOriginalproductIdOrSKN = pOriginalproductIdOrSKN;
	}
	
	/**
	 * Gets the back order status.
	 * 
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		this.mBackOrderStatus = pBackOrderStatus;
	}
	
	/**
	 * Gets the longDescription.
	 * 
	 * @return the mLongDescription
	 */
	public String getLongDescription() {
		return mLongDescription;
	}
	/**
	 * Sets the longDescription.
	 * 
	 * @param pLongDescription the longDescription to set
	 */
	public void setLongDescription(String pLongDescription) {
		mLongDescription = pLongDescription;
	}
	

	/**
	 * @param pPresellQuantityUnits the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}
	
	/**
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}
	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	} 
	
	/**
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	
	/**
	/**
	 * @return the s2s
	 */
	public String getS2s() {
		return mS2s;
	}
	/**
	 * @param pS2s the s2s to set
	 */
	public void setS2s(String pS2s) {
		mS2s = pS2s;
	}
	/**
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}
	/**
	 * @return the availableInventory
	 */
	public long getAvailableInventory() {
		return mAvailableInventory;
	}

	/**
	 * @return the availableInventoryInStore
	 */
	public long getAvailableInventoryInStore() {
		return mAvailableInventoryInStore;
	}

	/**
	 * @return the presellable
	 */
	public boolean isPresellable() {
		return mPresellable;
	}
	/**
	 * @param pPresellable the presellable to set
	 */
	public void setPresellable(boolean pPresellable) {
		mPresellable = pPresellable;
	}
	/**
	 * @param pPrimaryImage the primaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}
	/**
	 * @return the notifyMe
	 */
	public String getNotifyMe() {
		return mNotifyMe;
	}
	/**
	 * Property to hold mPriceDisplay.
	 */
	private String mPriceDisplay;
	
	/**
	 * Returns the priceDisplay.
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}
	/**
	 * Sets/updates the priceDisplay.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}
	/**
	 * @param pNotifyMe the notifyMe to set
	 */
	public void setNotifyMe(String pNotifyMe) {
		mNotifyMe = pNotifyMe;
	}
	
	/**
	 * @return the unCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}
	
	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * @param pAvailableInventory the availableInventory to set
	 */
	public void setAvailableInventory(long pAvailableInventory) {
		mAvailableInventory = pAvailableInventory;
	}

	/**
	 * @param pAvailableInventoryInStore the availableInventoryInStore to set
	 */
	public void setAvailableInventoryInStore(long pAvailableInventoryInStore) {
		mAvailableInventoryInStore = pAvailableInventoryInStore;
	}

	/**
	 * Gets the skuId.
	 * 
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	
	/**
	 * Sets the skuId.
	 * 
	 * @param pSkuId the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}


	/**
	 * Gets the pdpURL.
	 * 
	 * @return the pdpURL
	 */
	public String getPdpURL() {
		return mPdpURL;
	}

	/**
	 * Sets the pdpURL.
	 * 
	 * @param pPdpURL the pdpURL to set
	 */
	public void setPdpURL(String pPdpURL) {
		mPdpURL = pPdpURL;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mProductId;
	}

	/**
	 * @return the activeProduct
	 */
	public boolean isActiveProduct() {
		return mActiveProduct;
	}

	/**
	 * @param pActiveProduct the activeProduct to set
	 */
	public void setActiveProduct(boolean pActiveProduct) {
		mActiveProduct = pActiveProduct;
	}

	/**
	 * @return the mListPrice
	 */
	public double getListPrice() {
		return mListPrice;
	}

	/**
	 * @param pListPrice the mListPrice to set
	 */
	public void setListPrice(double pListPrice) {
		this.mListPrice = pListPrice;
	}

	/**
	 * @return the mSalePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}

	/**
	 * @return the mOnlinePid
	 */
	public String getOnlinePid() {
		return mOnlinePid;
	}
	
	/**
	 * @return the inventoryStatus
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * @return the inventoryStatusInStore
	 */
	public String getInventoryStatusInStore() {
		return mInventoryStatusInStore;
	}

	/**
	 * @param pOnlinePid the mOnlinePid to set
	 */
	public void setOnlinePid(String pOnlinePid) {
		this.mOnlinePid = pOnlinePid;
	}
	
	/**
	 * @param pInventoryStatus the inventoryStatus to set
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		mInventoryStatus = pInventoryStatus;
	}

	/**
	 * @param pInventoryStatusInStore the inventoryStatusInStore to set
	 */
	public void setInventoryStatusInStore(String pInventoryStatusInStore) {
		mInventoryStatusInStore = pInventoryStatusInStore;
	}
		
	
}
