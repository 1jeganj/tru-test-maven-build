package com.tru.commerce.catalog.vo;

import java.util.List;

/**
 * This class holds product  related information.
 * 
 * @author PA
 * @version 1.0
 */
public class ProductInfoJsonVO extends CommonPropertyVO{
	
	/**
	 * Property to hold mActive.
	 */
	/*private boolean mActive;*/

	/**
	 * Property to hold mDefaultSKU.
	 */
	private SKUInfoJsonVO mDefaultSKU;

	
	/**
	 * Property to hold mColorCodeList.
	 */
	/*private  List<String> mColorCodeList;
	
	*/
	/**
	 * Property to hold mJuvenileSizesList.
	 */
	/*
	

	/**
	 * Property to hold mActiveSKUsList.
	 */
	private  List<SKUInfoJsonVO> mActiveSKUsList;
	/**
	 * Property to hold mInStockSKUsListLength.
	 */
	private int mInStockSKUsListLength;
	
	/**
	 * Property to hold mSelectedColorSKUsList.
	 */
	private  List<SKUInfoJsonVO> mSelectedColorSKUsList;
	
	/**
	 * Property to hold mSelectedSizeSKUsList.
	 */
	private  List<SKUInfoJsonVO> mSelectedSizeSKUsList;


	/**
	 * Property to hold mColorSizeVariantsAvailableStatus.
	 */
	/*private  String mColorSizeVariantsAvailableStatus;*/
	
	/**
	 * Property to hold mSfs.
	 */
	/*private String mSfs;*/
	
	/**
	 * Property to hold mIspu.
	 */
	/*private String mIspu;*/
	
	/**
	 * Property to hold mProductStatus.
	 */
	/*private String mProductStatus;*/

	/**
	 * Property to hold mChannelAvailability.
	 */
	/*private String mChannelAvailability;*/
	
	/**
	 * Property to hold mRusItemNumber.
	 */
	/*private String mRusItemNumber;*/
	
	
	/**
	 * @return the mInStockSKUsListLength
	 */
	public int getInStockSKUsListLength() {
		return mInStockSKUsListLength;
	}

	/**
	 * @param pInStockSKUsListLength the mInStockSKUsListLength to set.
	 */
	public void setInStockSKUsListLength(int pInStockSKUsListLength) {
		this.mInStockSKUsListLength = pInStockSKUsListLength;
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 *//*
	public boolean isActive() {
		return mActive;
	}

	*//**
	 * Sets the active.
	 * 
	 * @param pActive the active to set
	 *//*
	public void setActive(boolean pActive) {
		mActive = pActive;
	}*/


	/**
	 * Gets the defaultSKU.
	 * 
	 * @return the defaultSKU
	 */
	public SKUInfoJsonVO getDefaultSKU() {
		return mDefaultSKU;
	}

	/**
	 * Sets the defaultSKU.
	 * 
	 * @param pDefaultSKU the defaultSKU to set
	 */
	public void setDefaultSKU(SKUInfoJsonVO pDefaultSKU) {
		mDefaultSKU = pDefaultSKU;
	}

	
	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mProductId;
	}

	/**
	 * @return the juvenileSizesList
	 *//*
	public List<String> getJuvenileSizesList() {
		return mJuvenileSizesList;
	}

	*//**
	 * @param pJuvenileSizesList the juvenileSizesList to set
	 *//*
	public void setJuvenileSizesList(List<String> pJuvenileSizesList) {
		mJuvenileSizesList = pJuvenileSizesList;
	}*/


	/**
	 * @return the activeSKUsList
	 */
	public List<SKUInfoJsonVO> getActiveSKUsList() {
		return mActiveSKUsList;
	}

	/**
	 * @param pActiveSKUsList the activeSKUsList to set
	 */
	public void setActiveSKUsList(List<SKUInfoJsonVO> pActiveSKUsList) {
		mActiveSKUsList = pActiveSKUsList;
	}

	/**
	 * @return the colorSizeVariantsAvailableStatus
	 *//*
	public String getColorSizeVariantsAvailableStatus() {
		return mColorSizeVariantsAvailableStatus;
	}

	*//**
	 * @param pColorSizeVariantsAvailableStatus the colorSizeVariantsAvailableStatus to set
	 *//*
	public void setColorSizeVariantsAvailableStatus(
			String pColorSizeVariantsAvailableStatus) {
		mColorSizeVariantsAvailableStatus = pColorSizeVariantsAvailableStatus;
	}*/


	/**
	 * @return the colorCodeList
	 *//*
	public List<String> getColorCodeList() {
		return mColorCodeList;
	}

	*//**
	 * @param pColorCodeList the colorCodeList to set
	 *//*
	public void setColorCodeList(List<String> pColorCodeList) {
		mColorCodeList = pColorCodeList;
	}*/

	/**
	 * @return the selectedColorSKUsList
	 */
	public List<SKUInfoJsonVO> getSelectedColorSKUsList() {
		return mSelectedColorSKUsList;
	}

	/**
	 * @param pSelectedColorSKUsList the selectedColorSKUsList to set
	 */
	public void setSelectedColorSKUsList(List<SKUInfoJsonVO> pSelectedColorSKUsList) {
		mSelectedColorSKUsList = pSelectedColorSKUsList;
	}

	/**
	 * @return the selectedSizeSKUsList
	 */
	public List<SKUInfoJsonVO> getSelectedSizeSKUsList() {
		return mSelectedSizeSKUsList;
	}

	/**
	 * @param pSelectedSizeSKUsList the selectedSizeSKUsList to set
	 */
	public void setSelectedSizeSKUsList(List<SKUInfoJsonVO> pSelectedSizeSKUsList) {
		mSelectedSizeSKUsList = pSelectedSizeSKUsList;
	}


	/**
	 * @return the sfs
	 */
	/*public String getSfs() {
		return mSfs;
	}

	*//**
	 * @param pSfs the sfs to set
	 *//*
	public void setSfs(String pSfs) {
		mSfs = pSfs;
	}*/

	/**
	 * @return the ispu
	 *//*
	public String getIspu() {
		return mIspu;
	}

	*//**
	 * @param pIspu the ispu to set
	 *//*
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}*/

	/**
	 * @return the mProductStatus
	 *//*
	public String getProductStatus() {
		return mProductStatus;
	}

	*//**
	 * @param pProductStatus to set
	 *//*
	public void setProductStatus(String pProductStatus) {
		this.mProductStatus = pProductStatus;
	}*/
	
	/**
	 * @return the channelAvailability
	 *//*
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	*//**
	 * @param pChannelAvailability the channelAvailability to set
	 *//*
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}*/

	/**
	 * @return the rusItemNumber
	 *//*
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	*//**
	 * @param pRusItemNumber the rusItemNumber to set
	 *//*
	public void setRusItemNumber(String pRusItemNumber) {
		mRusItemNumber = pRusItemNumber;
	}*/

	
}
