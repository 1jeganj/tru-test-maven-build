package com.tru.commerce.catalog.vo;

import com.tru.common.TRUConstants;

/**
 * This class holds SKUlevel price information.
 * @author Professional Access
 * @version 1.0
 */
public class PriceInfoVO {

	/**
	 * Property to hold Sku mListPrice.
	 */
	private double mListPrice = TRUConstants.DOUBLE_ZERO;
	/**
	 * Property to hold Sku mSalePrice.
	 */
	private double mSalePrice = TRUConstants.DOUBLE_ZERO;
	/**
	 * Property to hold Sku mSavedAmount.
	 */
	private double mSavedAmount=TRUConstants.DOUBLE_ZERO;;
	/**
	 * Property to hold Sku mSavedPercentage.
	 */
	private double mSavedPercentage=TRUConstants.DOUBLE_ZERO;
	/**
	 * @return the mListPrice
	 */
	public double getListPrice() {
		return mListPrice;
	}
	/**
	 * @param pListPrice to set
	 */
	public void setListPrice(double pListPrice) {
		this.mListPrice = pListPrice;
	}
	/**
	 * @return the mSalePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}
	/**
	 * @param pSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}
	/**
	 * @return the mSavedAmount
	 */
	public double getSavedAmount() {
		return mSavedAmount;
	}
	/**
	 * @param pSavedAmount to set
	 */
	public void setSavedAmount(double pSavedAmount) {
		this.mSavedAmount = pSavedAmount;
	}
	/**
	 * @return the mSavedPercentage
	 */
	public double getSavedPercentage() {
		return mSavedPercentage;
	}
	/**
	 * @param pSavedPercentage to set
	 */
	public void setSavedPercentage(double pSavedPercentage) {
		this.mSavedPercentage = pSavedPercentage;
	}
	
}
