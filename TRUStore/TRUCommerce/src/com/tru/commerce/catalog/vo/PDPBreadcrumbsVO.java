package com.tru.commerce.catalog.vo;

/**
 * This class holds category related information.
 * 
 * @author PA
 * @version 1.0
 */
 
public class PDPBreadcrumbsVO {
	
	/** 
	 * Property to hold categoryURL.
	 */
	
	private String mCategoryURL;
	
	/**
	 * Property to hold mCategoryId.
	 */
	
	
	private String mCategoryId;

	 /** 
		 * Property to hold categoryURL.
		 */

	private String mCategoryName;
     /**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}
	/**
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}
	/**
	 * @param pCategoryName the categoryName to set
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}
	/**
	 * @return the categoryURL
	 */
	public String getCategoryURL() {
		return mCategoryURL;
	}
	/**
	 * @param pCategoryURL the categoryURL to set
	 */
	public void setCategoryURL(String pCategoryURL) {
		mCategoryURL = pCategoryURL;
	}
	


}
