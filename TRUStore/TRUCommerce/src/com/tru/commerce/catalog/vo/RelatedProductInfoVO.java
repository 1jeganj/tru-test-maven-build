package com.tru.commerce.catalog.vo;

import java.util.List;



/**
 * This class holds product related information.
 * @author PA
 * @version 1.0
 */
public class RelatedProductInfoVO  extends CommonPropertyVO{

	/**
	 * Property to hold sku id.
	 */
	private String mSkuId;

	/**
	 * Property to hold mDisplayName.
	 */
	private String mPrimaryImage;

	/**
	 * Property to hold mDisplayName.
	 */
	private String mPdpURL;
	
	/**
	 * Property to hold mOnlinePid.
	 */
	private String mOnlinePid;

	/**
	 * Property to hold mActiveProduct.
	 */
	private boolean mActiveProduct;
	
	/**
	 * Property to hold mSuggestedAgeMax.
	 */
	private String mSuggestedAgeMax;
	
	/**
	 * Property to hold mSuggestedAgeMin.
	 */
	private String mSuggestedAgeMin;
	
	/**
	 * Property to hold mSuggestedAgeMessage.
	 */
	private String mSuggestedAgeMessage;
	
	/**
	 * Property to hold skuReviewRating.
	 */
	private int mReviewRating;
	
	/**
	 * Property to hold mReviewRatingFraction.
	 */
	private float mReviewRatingFraction;
	
	/**
	 * Property to hold listPrice.
	 */
	private double mListPrice;
	/**
	 * Property to hold salePrice.
	 */
	private double mSalePrice;
	
	/**
	 * Property to hold mStreetDate.
	 */
	private String mStreetDate;

	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mPresellable;
	
	/**
	 * Property to hold mPresellQuantityUnits.
	 */
	private String mPresellQuantityUnits;
	
	/** The m back order status. */
	private String mBackOrderStatus;
	/**
	 * Property to hold mPriceDisplay.
	 */
	private String mPriceDisplay;
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mUnCartable;
	
	/**
	 * Property to hold mPromoString.
	 */
	private List<String> mPromoString;
	
	/**
	 * @return the unCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}
	
	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		mUnCartable = pUnCartable;
	}
	
	/**
	 * Returns the priceDisplay.
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}
	/**
	 * Sets/updates the priceDisplay.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}
	

	/**
	 * Gets the back order status.
	 * 
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		this.mBackOrderStatus = pBackOrderStatus;
	}
	
	/**
	 * @return the presellable
	 */
	public boolean isPresellable() {
		return mPresellable;
	}

	
	/**
	 * @param pPresellable the presellable to set
	 */
	public void setPresellable(boolean pPresellable) {
		mPresellable = pPresellable;
	}

	/**
	 * @param pPresellQuantityUnits the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}
	
	/**
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}
	
	/**
	 * Property to hold mSkuPromos.
	 */
	private List<ProductPromoInfoVO> mPromos;

	

	/**
	 * @return the promos
	 */
	public List<ProductPromoInfoVO> getPromos() {
		return mPromos;
	}

	/**
	 * @param pPromos the promos to set
	 */
	public void setPromos(List<ProductPromoInfoVO> pPromos) {
		mPromos = pPromos;
	}

	/**
	 * Gets the skuId.
	 * 
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	
	/**
	 * Sets the skuId.
	 * 
	 * @param pSkuId the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the primaryImage.
	 * 
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * Sets the primaryImage.
	 * 
	 * @param pPrimaryImage the primaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}

	/**
	 * Gets the pdpURL.
	 * 
	 * @return the pdpURL
	 */
	public String getPdpURL() {
		return mPdpURL;
	}

	/**
	 * Sets the pdpURL.
	 * 
	 * @param pPdpURL the pdpURL to set
	 */
	public void setPdpURL(String pPdpURL) {
		mPdpURL = pPdpURL;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mProductId;
	}

	/**
	 * @return the activeProduct
	 */
	public boolean isActiveProduct() {
		return mActiveProduct;
	}

	/**
	 * @param pActiveProduct the activeProduct to set
	 */
	public void setActiveProduct(boolean pActiveProduct) {
		mActiveProduct = pActiveProduct;
	}

	/**
	 * @return the suggestedAgeMax
	 */
	public String getSuggestedAgeMax() {
		return mSuggestedAgeMax;
	}

	/**
	 * @param pSuggestedAgeMax the suggestedAgeMax to set
	 */
	public void setSuggestedAgeMax(String pSuggestedAgeMax) {
		mSuggestedAgeMax = pSuggestedAgeMax;
	}

	/**
	 * @return the suggestedAgeMin
	 */
	public String getSuggestedAgeMin() {
		return mSuggestedAgeMin;
	}

	/**
	 * @param pSuggestedAgeMin the suggestedAgeMin to set
	 */
	public void setSuggestedAgeMin(String pSuggestedAgeMin) {
		mSuggestedAgeMin = pSuggestedAgeMin;
	}
    
	/**
	 * @return the mReviewRating
	 */
	public int getReviewRating() {
		return mReviewRating;
	}
    
	/**
	 * @param pReviewRating to set
	 */
	public void setReviewRating(int pReviewRating) {
		mReviewRating = pReviewRating;
	}
	
	/**
	 * @return the mListPrice
	 */
	public double getListPrice() {
		return mListPrice;
	}

	/**
	 * @param pListPrice the mListPrice to set
	 */
	public void setListPrice(double pListPrice) {
		this.mListPrice = pListPrice;
	}

	/**
	 * @return the mSalePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}

	/**
	 * @return the mOnlinePid
	 */
	public String getOnlinePid() {
		return mOnlinePid;
	}

	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	} 
	
	/**
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @param pOnlinePid the mOnlinePid to set
	 */
	public void setOnlinePid(String pOnlinePid) {
		this.mOnlinePid = pOnlinePid;
	}

	/**
	 * @return the reviewRatingFraction
	 */
	public float getReviewRatingFraction() {
		return mReviewRatingFraction;
	}

	/**
	 * @param pReviewRatingFraction the reviewRatingFraction to set
	 */
	public void setReviewRatingFraction(float pReviewRatingFraction) {
		mReviewRatingFraction = pReviewRatingFraction;
	}

	/**
	 * @return the suggestedAgeMessage
	 */
	public String getSuggestedAgeMessage() {
		return mSuggestedAgeMessage;
	}

	/**
	 * @param pSuggestedAgeMessage the suggestedAgeMessage to set
	 */
	public void setSuggestedAgeMessage(String pSuggestedAgeMessage) {
		mSuggestedAgeMessage = pSuggestedAgeMessage;
	}

	/**
	 * @return the mPromoString
	 */
	public List<String> getPromoString() {
		return mPromoString;
	}

	/**
	 * @param pPromoString the mPromoString to set
	 */
	public void setPromoString(List<String> pPromoString) {
		mPromoString = pPromoString;
	}


		
	
}
