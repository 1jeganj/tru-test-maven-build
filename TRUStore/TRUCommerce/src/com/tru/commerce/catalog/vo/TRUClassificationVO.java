package com.tru.commerce.catalog.vo;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @author PA
 * The Class TRUClassificationVO.
 */
public class TRUClassificationVO {
	
	/** The m claasification displayOrder. */
	private int mClassificationDisplayOrder;
	
	/** The m claasification displayOrder. */
	private String mClassificationDisplayStatus;
	

	/** The m claasification id. */
	private String mClaasificationId;
	
	/** The m claasification name. */
	private String mClaasificationName;
	
	/** The m child classifications. */
	private List<String> mChildClassifications;
	
	/** The m parent classifications. */
	private List<String> mParentClassifications;
	
	/** The m classification long desc. */
	private String mClassificationLongDesc;
	
	/** The m user type. */
	private String mUserType;
	
	/** The m related media content. */
	private Map<String,TRUClassificationMediaVO> mRelatedMediaContent;
	
	/** The m classification Image Url. */
	private String mClassificationImageUrl;
	
	/**
	 * @return the classificationDisplayOrder
	 */
	public int getClassificationDisplayOrder() {
		return mClassificationDisplayOrder;
	}

	/**
	 * @param pClassificationDisplayOrder the classificationDisplayOrder to set
	 */
	public void setClassificationDisplayOrder(int pClassificationDisplayOrder) {
		mClassificationDisplayOrder = pClassificationDisplayOrder;
	}

	
	/**
	 * Gets the claasification id.
	 *
	 * @return the mClaasificationId
	 */
	public String getClaasificationId() {
		return mClaasificationId;
	}
	
	/**
	 * Sets the claasification id.
	 *
	 * @param pClaasificationId the mClaasificationId to set
	 */
	public void setClaasificationId(String pClaasificationId) {
		this.mClaasificationId = pClaasificationId;
	}
	
	/**
	 * Gets the claasification name.
	 *
	 * @return the mClaasificationName
	 */
	public String getClaasificationName() {
		return mClaasificationName;
	}
	
	/**
	 * Sets the claasification name.
	 *
	 * @param pClaasificationName the mClaasificationName to set
	 */
	public void setClaasificationName(String pClaasificationName) {
		this.mClaasificationName = pClaasificationName;
	}
	
	/**
	 * Gets the child classifications.
	 *
	 * @return the mChildClassifications
	 */
	public List<String> getChildClassifications() {
		return mChildClassifications;
	}
	
	/**
	 * Sets the child classifications.
	 *
	 * @param pChildClassifications the mChildClassifications to set
	 */
	public void setChildClassifications(List<String> pChildClassifications) {
		this.mChildClassifications = pChildClassifications;
	}
	
	/**
	 * Gets the parent classifications.
	 *
	 * @return the mRootParentClassifications
	 */
	public List<String> getParentClassifications() {
		return mParentClassifications;
	}
	
	/**
	 * Sets the parent classifications.
	 *
	 * @param pParentClassifications the mRootParentClassifications to set
	 */
	public void setParentClassifications(
			List<String> pParentClassifications) {
		this.mParentClassifications = pParentClassifications;
	}
	
	/**
	 * Gets the user type.
	 *
	 * @return the mUserType
	 */
	public String getUserType() {
		return mUserType;
	}
	
	/**
	 * Sets the user type.
	 *
	 * @param pUserType the mUserType to set
	 */
	public void setUserType(String pUserType) {
		this.mUserType = pUserType;
	}
	
	/**
	 * Gets the related media content.
	 *
	 * @return the mRelatedMediaContent
	 */
	public Map<String, TRUClassificationMediaVO> getRelatedMediaContent() {
		return mRelatedMediaContent;
	}
	
	/**
	 * Sets the related media content.
	 *
	 * @param pRelatedMediaContent the mRelatedMediaContent to set
	 */
	public void setRelatedMediaContent(
			Map<String, TRUClassificationMediaVO> pRelatedMediaContent) {
		this.mRelatedMediaContent = pRelatedMediaContent;
	}
	
	/**
	 * Gets the classification long desc.
	 *
	 * @return the classification long desc
	 */
	public String getClassificationLongDesc() {
		return mClassificationLongDesc;
	}
	
	/**
	 * Sets the classification long desc.
	 *
	 * @param pClassificationLongDesc the new classification long desc
	 */
	public void setClassificationLongDesc(String pClassificationLongDesc) {
		this.mClassificationLongDesc = pClassificationLongDesc;
	}

	/**
	 * @return the classificationImageUrl
	 */
	public String getClassificationImageUrl() {
		return mClassificationImageUrl;
	}

	/**
	 * @param pClassificationImageUrl the classificationImageUrl to set
	 */
	public void setClassificationImageUrl(String pClassificationImageUrl) {
		mClassificationImageUrl = pClassificationImageUrl;
	}

	/**
	 * @return the classificationDisplayStatus
	 */
	public String getClassificationDisplayStatus() {
		return mClassificationDisplayStatus;
	}

	/**
	 * @param pClassificationDisplayStatus the classificationDisplayStatus to set
	 */
	public void setClassificationDisplayStatus(String pClassificationDisplayStatus) {
		mClassificationDisplayStatus = pClassificationDisplayStatus;
	}

	
	
	
	

}
