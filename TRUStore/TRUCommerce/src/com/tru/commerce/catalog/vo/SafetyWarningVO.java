/*
 * @(#)SKU.java	1.0
 *
 * Copyright PA, Inc. All rights reserved.
 * PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.tru.commerce.catalog.vo;


/**
 * This class holds SafetyWarningVO related information.
 * 
 * @author PA.
 * @version 1.0.
 */
public class SafetyWarningVO {

	/**
	 * Property to hold SKU id.
	 */
	private String mId;

	
	/**
	 * Property to productSafetyWarnings.
	 */
	private String mProductSafetyWarnings;
	

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * Gets the ProductSafetyWarnings.
	 * 
	 * @return the ProductSafetyWarnings
	 */
	public String getProductSafetyWarnings() {
		return mProductSafetyWarnings;
	}
	/**
	 * Sets the ProductSafetyWarnings.
	 * 
	 * @param pProductSafetyWarnings the ProductSafetyWarnings to set
	 */
	public void setProductSafetyWarnings(String pProductSafetyWarnings) {
		mProductSafetyWarnings = pProductSafetyWarnings;
	}

	
	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mId;
	}
}
