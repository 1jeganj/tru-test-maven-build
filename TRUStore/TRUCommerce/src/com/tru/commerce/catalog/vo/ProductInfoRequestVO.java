package com.tru.commerce.catalog.vo;

import com.tru.commerce.TRUCommerceConstants;


/**
 * The Class ProductInfoRequestVO.
 * @author Professional Access
 * @version 1.0
 */
public class ProductInfoRequestVO extends CommonPropertyVO{

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		final int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + (mLocale == null ? 0 : mLocale.hashCode());
		result = prime * result + (mProductId == null ? 0 : mProductId.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		ProductInfoRequestVO other = (ProductInfoRequestVO) pObject;
		if (mLocale == null) {
			if (other.mLocale != null) {
				return false;
			}
		} else if (!mLocale.equals(other.mLocale)) {
			return false;
		}
		if (mProductId == null) {
			if (other.mProductId != null) {
				return false;
			}
		} else if (!mProductId.equals(other.mProductId)) {
			return false;
		}
		return true;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mProductId;
	}
}
