/**
 * 
 */
package com.tru.commerce.catalog.vo;

/**
 * @author PA
 *
 */
public class PromoInfoVO {

	/**
	 * Property to mPromoId.
	 */
	private String mPromoId;
	
	/**
	 * Property to mPromoDescription.
	 */
	private String mPromoDescription;
	
	/**
	 * Property to mPromoDetails.
	 */
	private String mPromoDetails;
	
	
	/**
	 * @return the promoId
	 */
	public String getPromoId() {
		return mPromoId;
	}


	/**
	 * @param pPromoId the promoId to set
	 */
	public void setPromoId(String pPromoId) {
		mPromoId = pPromoId;
	}


	/**
	 * @return the promoDescription
	 */
	public String getPromoDescription() {
		return mPromoDescription;
	}


	/**
	 * @param pPromoDescription the promoDescription to set
	 */
	public void setPromoDescription(String pPromoDescription) {
		mPromoDescription = pPromoDescription;
	}


	/**
	 * @return the promoDetails
	 */
	public String getPromoDetails() {
		return mPromoDetails;
	}


	/**
	 * @param pPromoDetails the promoDetails to set
	 */
	public void setPromoDetails(String pPromoDetails) {
		mPromoDetails = pPromoDetails;
	}


	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mPromoId;
	}
}
