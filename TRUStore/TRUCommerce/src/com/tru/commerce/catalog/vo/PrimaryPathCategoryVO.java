package com.tru.commerce.catalog.vo;

/**
 * This class holds category related information.
 * 
 * @author PA
 * @version 1.0
 */
 
public class PrimaryPathCategoryVO {
	/**  
	 * Property to hold categoryId.
	 */
	private String mCategoryId;
	/**
	 * Property to hold categoryName.
	 */
	private String mCategoryName;
     /** 
	 * Property to hold categoryURL.
	 */
	private String mCategoryURL;
	
	/** The m category display status. */
	private String mCategoryDisplayStatus;
	/** 
	 * Gets the categoryId.
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}
	/** 
	 * Gets the categoryName.
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}
	/** 
	 * Gets the categoryURL.
	 * @return the categoryURL
	 */
	public String getCategoryURL() {
		return mCategoryURL;
	}
	/**
	 * Sets the categoryId.
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}
	/** 
	 * Sets the categoryName.
	 * @param pCategoryName the categoryName to set
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}

	/** 
	 * Sets the categoryURL.
	 * @param pCategoryURL the categoryURL to set
	 */
	public void setCategoryURL(String pCategoryURL) {
		mCategoryURL = pCategoryURL;
	}
	/**
	 * @return the categoryDisplayStatus
	 */
	public String getCategoryDisplayStatus() {
		return mCategoryDisplayStatus;
	}
	/**
	 * @param pCategoryDisplayStatus the categoryDisplayStatus to set
	 */
	public void setCategoryDisplayStatus(String pCategoryDisplayStatus) {
		mCategoryDisplayStatus = pCategoryDisplayStatus;
	}
}
