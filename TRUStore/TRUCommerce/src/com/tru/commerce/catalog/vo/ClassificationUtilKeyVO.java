package com.tru.commerce.catalog.vo;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This class holds Classification Util Key related information.
 * 
 * @author PA
 * @version 1.0
 */
public class ClassificationUtilKeyVO {

	/**
	 * Property to hold RootCategoryId.
	 */
	private String mRootCategoryId;

	/**
	 * Property to hold ClassificationId.
	 */
	private String mClassificationId;
	
	/**
	 * Property to hold ParentClassificationId.
	 */
	private String mParentClassificationId;
	
	/**
	 * Property to hold level.
	 */
	private int mLevel;

	/**
	 * Gets the RootCategoryId.
	 * 
	 * @return the RootCategoryId
	 */
	public String getRootCategoryId() {
		return mRootCategoryId;
	}

	/**
	 * Sets the RootCategoryId.
	 * 
	 * @param pRootCategoryId the RootCategoryId to set
	 */
	public void setRootCategoryId(String pRootCategoryId) {
		mRootCategoryId = pRootCategoryId;
	}

	/**
	 * Gets the ParentClassificationId.
	 * 
	 * @return the ParentClassificationId
	 */
	public String getParentClassificationId() {
		return mParentClassificationId;
	}

	/**
	 * Sets the ParentClassificationId.
	 * 
	 * @param pParentClassificationId the ParentClassificationId to set
	 */
	public void setParentClassificationId(String pParentClassificationId) {
		mParentClassificationId = pParentClassificationId;
	}
	
	/**
	 * Gets the ClassificationId.
	 * 
	 * @return the ClassificationId
	 */
	public String getClassificationId() {
		return mClassificationId;
	}

	/**
	 * Sets the ClassificationId.
	 * 
	 * @param pClassificationId the ClassificationId to set
	 */
	public void setClassificationId(String pClassificationId) {
		mClassificationId = pClassificationId;
	}

	/**
	 * Gets the Level.
	 * 
	 * @return the Level
	 */
	public int getLevel() {
		return mLevel;
	}

	/**
	 * Sets the Level.
	 * 
	 * @param pLevel the Level to set
	 */
	public void setLevel(int pLevel) {
		mLevel = pLevel;
	}
	
	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = result + mLevel;
		result = prime * result + (mRootCategoryId == null ? 0 : mRootCategoryId.hashCode());
		result = prime * result + (mClassificationId == null ? 0 : mClassificationId.hashCode());
		result = prime * result + (mParentClassificationId == null ? 0 : mParentClassificationId.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		ClassificationUtilKeyVO other = (ClassificationUtilKeyVO) pObject;
		
		if(mLevel != other.mLevel) {
			return false;
		}
		
		//Catalog Id
		if (mRootCategoryId == null) {
			if (other.mRootCategoryId != null) {
				return false;
			}
		} else if (!mRootCategoryId.equals(other.mRootCategoryId)) {
			return false;
		}
		//Locale
		if (mClassificationId == null) {
			if (other.mClassificationId != null) {
				return false;
			}
		} else if (!mClassificationId.equals(other.mClassificationId)) {
			return false;
		}
		
		//Catalog Id
		if (mParentClassificationId == null) {
			if (other.mParentClassificationId != null) {
				return false;
			}
		} else if (!mParentClassificationId.equals(other.mParentClassificationId)) {
			return false;
		}
				
		return true;
	}
	
}
