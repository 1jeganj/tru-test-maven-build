package com.tru.commerce.catalog.vo;

import java.util.List;
import java.util.Map;

/**
 * This class holds collection product related information.
 * 
 * @author PA
 * @version 1.0
 */
public class CollectionProductInfoVO {

	/**
	 * Property to hold collection id.
	 */
	private String mCollectionId;

	/**
	 * Property to hold mCollectionName.
	 */
	private String mCollectionName;
	/**
	 * Property to hold mDisplayStatus.
	 */
	private String mDisplayStatus;
	
	/**
	 * Property to hold mDescription.
	 */
	private String mCollectionDescription;
	
	/**
	 * Property to hold mCollectionImage.
	 */
	private String mCollectionImage;

	/**
	 * Property to hold  product Info.
	 */
	private List<ProductInfoVO> mProductInfoList;
	
	/**
	 * Property to hold List of Active product Info.
	 */
	private List<ProductInfoVO> mActiveProductInfoList;
	
	/**
	 * Property to hold  mProductInfoJsonmap.
	 */
	private Map mProductInfoJsonMap;
	
	/**
	 * Property to hold breadcrumb.
	 */
	private List<BreadcrumbInfoVO> mBreadcrumb;
	
	/**
	 * Property to hold mParentCategoryId.
	 */
	private String mParentCategoryId;
	
	/**
	 * Property to hold mParentCategoryDescription.
	 */
	private String mParentCategoryDescription;
	
	/**
	 * Property to hold siteId.
	 */
	private String mSiteId;
	
	/**
	 * Property to hold ChildSkuId.
	 */
	private String mChildSkuId;
	
	/**
	 * @return the mProductInfoJsonMap
	 */
	public Map getProductInfoJsonMap() {
		return mProductInfoJsonMap;
	}

	/**
	 * @param pProductInfoJsonMap the mProductInfoJsonMap to set
	 */
	public void setProductInfoJsonMap(Map pProductInfoJsonMap) {
		this.mProductInfoJsonMap = pProductInfoJsonMap;
	}

	/** 
	 * @return mCollectionId
	 */
	public String getCollectionId() {
		return mCollectionId;
	}

	/**
	 * 
	 * @param pCollectionId collectionId
	 */
	public void setCollectionId(String pCollectionId) {
		mCollectionId = pCollectionId;
	}

	/**
	 * @return mCollectionName
	 */
	public String getCollectionName() {
		return mCollectionName;
	}

	/**
	 * @param pCollectionName collectionName
	 */
	public void setCollectionName(String pCollectionName) {
		mCollectionName = pCollectionName;
	}

	/**
	 * @return mCollectionDescription
	 */
	public String getCollectionDescription() {
		return mCollectionDescription;
	}

	/** 
	 * @param pCollectionDescription collectionDescription
	 */
	public void setCollectionDescription(String pCollectionDescription) {
		mCollectionDescription = pCollectionDescription;
	}

	/**
	 * @return collectionImage
	 */
	public String getCollectionImage() {
		return mCollectionImage;
	}

	/** 
	 * @param pCollectionImage collectionImage
	 */
	public void setCollectionImage(String pCollectionImage) {
		mCollectionImage = pCollectionImage;
	}

	/**
	 * @return productInfoList
	 */
	public List<ProductInfoVO> getProductInfoList() {
		return mProductInfoList;
	}

	/**
	 * @param pProductInfoList productInfoList
	 */
	public void setProductInfoList(List<ProductInfoVO> pProductInfoList) {
		mProductInfoList = pProductInfoList;
	}

	/**
	 * @return the breadcrumb
	 */
	public List<BreadcrumbInfoVO> getBreadcrumb() {
		return mBreadcrumb;
	}

	/**
	 * @param pBreadcrumb the breadcrumb to set
	 */
	public void setBreadcrumb(List<BreadcrumbInfoVO> pBreadcrumb) {
		this.mBreadcrumb = pBreadcrumb;
	}

	/**
	 * @return the mParentCategoryId
	 */
	public String getParentCategoryId() {
		return mParentCategoryId;
	}

	/**
	 * @param pParentCategoryId the ParentCategoryId to set
	 */
	public void setParentCategoryId(String pParentCategoryId) {
		this.mParentCategoryId = pParentCategoryId;
	}

	/**
	 * @return the mParentCategoryDescription
	 */
	public String getParentCategoryDescription() {
		return mParentCategoryDescription;
	}

	/**
	 * @param pParentCategoryDescription the mParentCategoryDescription to set
	 */
	public void setParentCategoryDescription(String pParentCategoryDescription) {
		this.mParentCategoryDescription = pParentCategoryDescription;
	}

	/**
	 * @return the mSiteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * @param pSiteId the mSiteId to set
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}
	/**
	 * Gets the DisplayStatus.
	 *
	 * @return the DisplayStatus
	 */
	public String getDisplayStatus() {
		return mDisplayStatus;
	}
	
	/**
	 * Sets the DisplayStatus.
	 *
	 * @param pDisplayStatus the mDisplayStatus to set
	 */
	public void setDisplayStatus(String pDisplayStatus) {
		mDisplayStatus = pDisplayStatus;
	}
	/**
	 * @return the activeProductInfoList
	 */
	public List<ProductInfoVO> getActiveProductInfoList() {
		return mActiveProductInfoList;
	}

	/**
	 * @param pActiveProductInfoList the activeProductInfoList to set
	 */
	public void setActiveProductInfoList(List<ProductInfoVO> pActiveProductInfoList) {
		mActiveProductInfoList = pActiveProductInfoList;
	}

	/**
	 * @return the childSkuId
	 */
	public String getChildSkuId() {
		return mChildSkuId;
	}

	/**
	 * @param pChildSkuId the childSkuId to set
	 */
	public void setChildSkuId(String pChildSkuId) {
		mChildSkuId = pChildSkuId;
	}
	
}
