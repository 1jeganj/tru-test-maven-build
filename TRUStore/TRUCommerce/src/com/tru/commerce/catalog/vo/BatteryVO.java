package com.tru.commerce.catalog.vo;


/**
 * This class holds Battery related information.
 * 
 * @author PA
 * @version 1.0
 */
public class BatteryVO {

	/**
	 * Property to hold battery id.
	 */
	private String mBatteryId;
	
	/**
	 * Property to hold mBatteryType.
	 */
	private String mBatteryType;
	
	/**
	 * Property to hold mBatteryQuantity.
	 */
	private int mBatteryQuantity;

	/**
	 * @return the batteryId.
	 */
	public String getBatteryId() {
		return mBatteryId;
	}

	/**
	 * @param pBatteryId the batteryId to set.
	 */
	public void setBatteryId(String pBatteryId) {
		mBatteryId = pBatteryId;
	}

	/**
	 * @return the batteryType
	 */
	public String getBatteryType() {
		return mBatteryType;
	}

	/**
	 * @param pBatteryType the batteryType to set.
	 */
	public void setBatteryType(String pBatteryType) {
		mBatteryType = pBatteryType;
	}

	/**
	 * @return the batteryQuantity.
	 */
	public int getBatteryQuantity() {
		return mBatteryQuantity;
	}

	/**
	 * @param pBatteryQuantity the batteryQuantity to set.
	 */
	public void setBatteryQuantity(int pBatteryQuantity) {
		mBatteryQuantity = pBatteryQuantity;
	}
	
	
}
