package com.tru.commerce.catalog.vo;





/**
 * This class holds promotion related information.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class ProductPromoInfoVO {

	/**
	 * This property hold reference for mDescription.
	 */
	private String mDescription;
	/**
	 * Property to hold details.
	 */
	private String mDetails;
	/**
	 * Property to hold details.
	 */
	private String mEndDate;
	
	/**
	 * Property to hold details.
	 */
	private String mStartDate;
	
	/**
	 * Property to hold mPromotionId.
	 */
	private String mPromotionId;
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}
	/**
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return mStartDate;
	}
	/**
	 * @param pStartDate the startDate to set
	 */
	public void setStartDate(String pStartDate) {
		mStartDate = pStartDate;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return mDetails;
	}
	/**
	 * @param pDetails the details to set
	 */
	public void setDetails(String pDetails) {
		mDetails = pDetails;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return mEndDate;
	}
	/**
	 * @param pEndDate the endDate to set
	 */
	public void setEndDate(String pEndDate) {
		mEndDate = pEndDate;
	}
	/**
	 * @return the promotionId
	 */
	public String getPromotionId() {
		return mPromotionId;
	}
	/**
	 * @param pPromotionId the mPromotionId to set
	 */
	public void setPromotionId(String pPromotionId) {
		mPromotionId = pPromotionId;
	}
	
}
