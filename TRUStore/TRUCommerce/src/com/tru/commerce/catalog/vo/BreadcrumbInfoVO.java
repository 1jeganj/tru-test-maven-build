package com.tru.commerce.catalog.vo;

/**
 * This class holds breadcrumb details on PDP mobile service.
 * @author PA
 * @version 1.0
 */
public class BreadcrumbInfoVO {
	/**
	 * Property to hold navigationState.
	 */
	private String mNavigationState;
	/**
	 * Property to hold label.
	 */
	private String mLabel;
	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}
	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return mLabel;
	}
	/**
	 * @param pLabel the label to set
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}
}
