package com.tru.commerce.catalog.vo;

import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.repository.RepositoryItem;

/**
 * This class holds SKU related information.
 * 
 * @author PA
 * @version 1.0
 */
public class SKUInfoVO {
	
	/** The sku item. */
	private RepositoryItem mSkuItem;
	
	/** The color sequence number. */
	private int mColorSequenceNumber;
	
	/** The juvinile sizedesc. */
	private String mJuninileSizeDesc;
	
	/** The size sequence number. */
	private int mSizeSequenceNumber;

	/** The boolean flag color variant exists. */
	private boolean mColorVariantExists;

	/** The boolean flag size variant exists. */
	private boolean mSizeVariantExists;

	/**
	 * Property to hold is active.
	 */
	private Boolean mActive;

	/**
	 * Property to mAlterNateImages.
	 */
	private List<String> mAlterNateImages;
	
	/**
	 * Property to mAlterNateCanonicalImages.
	 */
	private List<String> mAlterNateCanonicalImages;

	/**
	 * Property to hold mAssembledWeight.
	 */
	private String mAssembledWeight;
	
	/**
	 * Property to hold mVendorsProductDemoUrl.
	 */
	private String mVendorsProductDemoUrl;
	
	/**
	 * Property to hold mBrandName.
	 */
	private String mBrandName;
	
	/**
	 * Property to hold mChannelAvailability.
	 */
	private String mChannelAvailability;
	
	/**
	 * Property to hold mIspu.
	 */
	private String mIspu;
	
	/**
	 * Property to hold mBrandLandingURL.
	 */
	private String mBrandLandingURL;
	
	/**
	/**
	 * Property to hold mAssemblyDimenssionHeight.
	 */
	
	private String mAssemblyDimenssionHeight;
	
	/**
	 * Property to hold mAssemblyDimenssionLength.
	 */
	private String mAssemblyDimenssionLength;
	/**
	 * Property to hold mAssemblyDimenssionWidth.
	 */
	private String mAssemblyDimenssionWidth;
	/**
	 * Property to hold mAssemblyRequired.
	 */
	private String mAssemblyRequired;

	/**
	 * Property to hold mAvailableInventory.
	 */
	private long mAvailableInventory;

	/**
	 * Property to hold mAvailableInventoryInStore.
	 */
	private long mAvailableInventoryInStore;


	/**
	 * Property to hold mBppEligible.
	 */
	private Boolean mBppEligible;

	/**
	 * Property to hold mBppId.
	 */
	private Float mBppId;


	/**
	 * Property to hold mBppName.
	 */
	private String mBppName;

	/**
	 * Property to hold mCanBeGiftWrapped.
	 */
	private String mCanBeGiftWrapped;

	/**
	 * Property to hold mCollectionNames.
	 */
	private Map<String,String> mCollectionNames;

	/**
	 * Property to hold mColor.
	 */
	private ColorInfoVO mColor;

	/**
	 * Property to hold mColorCode.
	 */
	private String mColorCode;

	/**
	 * Property to hold  mCribMaterialTypes.
	 */
	private List<String> mCribMaterialTypes;

	/**
	 * Property to hold  mCribStyles.
	 */
	private List<String> mCribStyles;

	/**
	 * Property to hold  mCribTypes.
	 */
	private List<String> mCribTypes;

	/**
	 * Property to hold  mCribWhatIsImp.
	 */
	private List<String> mCribWhatIsImp;


	/**
	 * Property to mCustomerPurchaseLimit.
	 */
	private String mCustomerPurchaseLimit;

	/**
	 * Property to hold mDeleted.
	 */
	private boolean mDeleted;

	/**
	 * Property to hold mDisplayable.
	 */
	private boolean mDisplayable;

	/**
	 * Property to hold display name.
	 */
	private String mDisplayName;

	/**
	 * Property to hold mDropShipFlag.
	 */
	private boolean mDropShipFlag;

	/**
	 * Property to hold mEsrbrating.
	 */
	private String mEsrbrating;

	/**
	 * Property to mFeatures.
	 */
	private Map<String,String> mFeatures;

	/**
	 * Property to hold mFreightClass.
	 */
	private String mFreightClass;

	/**
	 * Property to hold mFreightClassMessage.
	 */
	private String mFreightClassMessage;

	/**
	 * Property to hold SKU id.
	 */
	private String mId;

	/**
	 * Property to hold mImages.
	 */
	private List<Map<String, String>> mImages;

	/**
	 * Property to hold  mInlineFile.
	 */
	private String mInlineFile;
	
	/**
	 * Property to hold mInventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**

	 * Property to hold mInventoryStatusInStore.
	 */
	private String mInventoryStatusInStore;
	
	/**
	 * Property to hold mJuvenileSize.
	 */
	private String mJuvenileSize;
	
	/**
	 * Property to hold mListPrice.
	 */
	private double mListPrice;
	
	/**
	 * Property to hold LongDescription.
	 */
	private String mLongDescription;
	
	/**
	 * Property to hold mfrSuggestedAgeMax.
	 */
	private String mMfrSuggestedAgeMax;
	
	/**
	 * Property to hold mfrSuggestedAgeMin.
	 */
	private String mMfrSuggestedAgeMin;
	
	/**
	 * Property to hold mNewItem.
	 */
	private boolean mNewItem;
	
	/**
	 * Property to hold  mNmwaPercentage.
	 */
	private float mNmwaPercentage;
	
	/**
	 * Property to hold mNotifyMe.
	 */
	private String mNotifyMe;
	
	/**
	 * Property to hold  mOnlinePID.
	 */
	private String mOnlinePID;
	
	/**
	 * Property to hold mParentCategoryDescription.
	 */
	private String mParentCategoryDescription;
	
	/**
	 * Property to hold mParentCategoryId.
	 */
	private String mParentCategoryId;
	
	/**
	 * Property to hold mPresellable.
	 */
	private boolean mPresellable;
	
	/**
	 * Property to hold mPresellQuantityUnits.
	 */
	private String mPresellQuantityUnits;
	
	/**
	 * Property to hold mPriceDisplay.
	 */
	private String mPriceDisplay;
	
	/**
	 * Property to hold  mPriceInfoVO.
	 */
	private PriceInfoVO mPriceInfo;
	
	/**
	 * Property to hold mPriceSavingAmount.
	 */
	private double mPriceSavingAmount;
	
	/**
	 * Property to hold mPriceSavingPercentage.
	 */
	private double mPriceSavingPercentage;
	
	/**
	 * Property to mPrimaryImage.
	 */
	private String mPrimaryImage;
	
	/**
	 * Property to mPrimaryCanonicalImage.
	 */
	private String mPrimaryCanonicalImage;
	
	/**
	 * Property to hold mProductDimenssionHeight.
	 */
	private String mProductDimenssionHeight;

	/**
	 * Property to hold mProductDimenssionLength.
	 */
	private String mProductDimenssionLength;
	
	/**
	 * Property to hold mProductDimenssionWidth.
	 */
	private String mProductDimenssionWidth;
	
	/**
	 * Property to hold mProductItemWeight.
	 */
	private String mProductItemWeight;
	
	/**
	 * Property to hold mPromotionalStickerDisplay.
	 */
	private String mPromotionalStickerDisplay;
	
	/**
	 * Property to hold mRegistryEligibility.
	 */
	
	private boolean mRegistryEligibility;
	
	/**
	 * Property to hold  mRegistryWarningIndicator.
	 */
	private String mRegistryWarningIndicator;
	
	/**
	 * Property to hold skuReviewRating.
	 */
	private int mReviewRating;
	
	/**
	 * Property to hold mReviewRatingFraction.
	 */
	private float mReviewRatingFraction;
	
	/**
	 * Property to hold mS2s.
	 */
	private String mS2s;
	
	private List<String> mSafetyWarning;
	
	/**
	 * Property to hold mSalePrice.
	 */
	private double mSalePrice;
	
	/**
	 * Property to hold mSearchable.
	 */
	private boolean mSearchable;
	
	/**
	 * Property to hold mServiceProviderImageURL.
	 */
	private String mSearchPrimaryImageURL;
	
	/**
	 * Property to mSecondaryImage.
	 */
	private String mSecondaryImage;
	
	/**
	 * Property to mSecondaryCanonicalImage.
	 */
	private String mSecondaryCanonicalImage;
	
	/**
	 * Property to hold mShipInOrigContainer.
	 */
	private boolean mShipInOrigContainer;
	
	/**
	 * Property to hold  mShipWindowMax.
	 */
	private String mShipWindowMax;
	
	/**
	 * Property to hold  mShipWindowMin.
	 */
	private String mShipWindowMin;
	/**

	 * Property to hold  mSkuRegistryEligibility.
	 */
	private boolean mSkuRegistryEligibility;
	

	/** The m back order status. */
	private String mBackOrderStatus;
	
	/**
	 * Property to hold mSkuType.
	 */
	private String mSkuType;
	/**
	 * Property to hold mStoreAvailablityMessage.
	 */
	private String mStoreAvailablityMessage;
	/**
	 * Property to hold mStoreMessage.
	 */
	private String mStoreMessage;
	
	/**
	 * Property to hold mStreetDate.
	 */
	private String mStreetDate;
	
	/**
	 * Property to hold mSuggestAgeMessage.
	 */
	private String mSuggestAgeMessage;
	
	/**
	 * Property to mSwatchImage.
	 */
	private String mSwatchImage;
	
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mUnCartable;
	
	
	/**
	 * Property to mUpcNumbers.
	 */
	private Set<String> mUpcNumbers;
	
	/**
	 * Property to hold mVideoGameEsrb.
	 */
	private String mVideoGameEsrb;
	
	/**
	 * Property to hold related products.
	 */
	private List<RelatedProductInfoVO> mRelatedProducts;
	
	/**
	 * Property to hold battery products.
	 */
	private List<BatteryProductInfoVO> mBatteryProducts;
	
	/**
	 * This property hold reference for mRmsSizeCode.
	 */
	private String mRmsSizeCode;
	
	/**
	 * This property hold reference for mRmsColorCode.
	 */
	private String mRmsColorCode;
	
	/**
	 * This property hold reference for mOriginalproductIdOrSKN.
	 */
	private String mOriginalproductIdOrSKN;
	
	/**
	 * This property hold reference for mSknOrigin.
	 */
	private int mSknOrigin;
	/**
	 * Property to hold mSkuPromos.
	 */
	private List<ProductPromoInfoVO> mPromos;
	
	/**
	 * Property to hold mPromoString.
	 */
	private List<String> mPromoString;
	/**
	 * Gets the brandLandingURL.
	 * 
	 * @return the brandLandingURL
	 */
	public String getBrandLandingURL() {
		return mBrandLandingURL;
	}

	/**
	 * Sets the brandLandingURL.
	 * 
	 * @param pBrandLandingURL the brandLandingURL to set
	 */
	public void setBrandLandingURL(String pBrandLandingURL) {
		mBrandLandingURL = pBrandLandingURL;
	}
	
	/**
	 * Gets the back order status.
	 * 
	 * @return the back order status
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the new back order status
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		this.mBackOrderStatus = pBackOrderStatus;
	}

	/** @return the promos
	 */
	public List<ProductPromoInfoVO> getPromos() {
		return mPromos;
	}

	/**
	 * @param pPromos the promos to set
	 */
	public void setPromos(List<ProductPromoInfoVO> pPromos) {
		mPromos = pPromos;
	}
	
	
	/**
	 * Gets the brandName.
	 * 
	 * @return the brandName
	 */
	public String getBrandName() {
		return mBrandName;
	}

	/**
	 * Sets the brandName.
	 * 
	 * @param pBrandName the brandName to set
	 */
	public void setBrandName(String pBrandName) {
		mBrandName = pBrandName;
	}

	/**
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * @param pChannelAvailability the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * @return the ispu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the ispu to set
	 */
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}

	/**
	 * @return the mRelatedProducts
	 */
	public List<RelatedProductInfoVO> getRelatedProducts() {
		return mRelatedProducts;
	}

	/**
	 * @param pRelatedProducts the mRelatedProducts to set
	 */
	public void setRelatedProducts(List<RelatedProductInfoVO> pRelatedProducts) {
		mRelatedProducts = pRelatedProducts;
	}
	
	/**
	 * @return the mBatteryProducts
	 */
	public List<BatteryProductInfoVO> getBatteryProducts() {
		return mBatteryProducts;
	}

	/**
	 * @param pBatteryProducts the mBatteryProducts to set
	 */
	public void setBatteryProducts(List<BatteryProductInfoVO> pBatteryProducts) {
		mBatteryProducts = pBatteryProducts;
	}
	/**
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * @param pVendorsProductDemoUrl the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}
	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public Boolean getActive() {
		return mActive;
	}

	/**
	 * @return the alterNateImages
	 */
	public List<String> getAlterNateImages() {
		return mAlterNateImages;
	}
	
	/**
	 * @return the alterNateCanonicalImages
	 */
	public List<String> getAlterNateCanonicalImages() {
		return mAlterNateCanonicalImages;
	}

	/**
	 * Gets the color.
	 * 
	 * @return the assembledWeight
	 */
	public String getAssembledWeight() {
		return mAssembledWeight;
	}

	/**
	 * @return the assemblyDimenssionHeight
	 */
	public String getAssemblyDimenssionHeight() {
		return mAssemblyDimenssionHeight;
	}

	/**
	 * @return the assemblyDimenssionLength
	 */
	public String getAssemblyDimenssionLength() {
		return mAssemblyDimenssionLength;
	}

	/**
	 * @return the assemblyDimenssionWidth
	 */
	public String getAssemblyDimenssionWidth() {
		return mAssemblyDimenssionWidth;
	}

	
	/**
	 * @return the assemblyRequired
	 */
	public String getAssemblyRequired() {
		return mAssemblyRequired;
	}

	/**
	 * @return the availableInventory
	 */
	public long getAvailableInventory() {
		return mAvailableInventory;
	}

	/**
	 * @return the availableInventoryInStore
	 */
	public long getAvailableInventoryInStore() {
		return mAvailableInventoryInStore;
	}

	/**
	 * @return the bppEligible
	 */
	public Boolean getBppEligible() {
		return mBppEligible;
	}
	
	/**
	 * @return the bppId
	 */
	public Float getBppId() {
		return mBppId;
	}
	
	/**
	 * @return the bppName
	 */
	public String getBppName() {
		return mBppName;
	}
	
	/**
	 * @return the canBeGiftWrapped
	 */
	public String getCanBeGiftWrapped() {
		return mCanBeGiftWrapped;
	}

	/**
	 * @return the collectionNames
	 */
	public Map<String,String> getCollectionNames() {
		return mCollectionNames;
	}
	
	/**
	 * Gets the color.
	 * 
	 * @return the color
	 */
	public ColorInfoVO getColor() {
		return mColor;
	}
	
	/**
	 * @return the colorCode
	 */
	public String getColorCode() {
		return mColorCode;
	}
	
	/**
	 * Gets the mCribMaterialTypes.
	 * 
	 * @return the mCribMaterialTypes
	 */
	public List<String> getCribMaterialTypes() {
		return mCribMaterialTypes;
	}

	
	
	/**
	 * Gets the mCribStyles.
	 * 
	 * @return the mCribStyles
	 */
	public List<String> getCribStyles() {
		return mCribStyles;
	}

	/**
	 * Gets the mCribTypes.
	 * 
	 * @return the mCribTypes
	 */
	public List<String> getCribTypes() {
		return mCribTypes;
	}

	/**
	 * Gets the mCribWhatIsImp.
	 * 
	 * @return the mCribWhatIsImp
	 */
	public List<String> getCribWhatIsImp() {
		return mCribWhatIsImp;
	}

	/**
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Gets the displayName.
	 * 
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Gets the mEsrbrating.
	 * 
	 * @return the mEsrbrating
	 */

	public String getEsrbrating() {
		return mEsrbrating;
	}

	/**
	 * @return the features
	 */
	public Map<String, String> getFeatures() {
		return mFeatures;
	}

	/**
	 * @return the freightClass
	 */
	public String getFreightClass() {
		return mFreightClass;
	}

	/**
	 * @return the freightClassMessage
	 */
	public String getFreightClassMessage() {
		return mFreightClassMessage;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Gets the images.
	 * 
	 * @return the images
	 */
	public List<Map<String, String>> getImages() {
		return mImages;
	}

	/**
	 * @return the inlineFile
	 */
	public String getInlineFile() {
		return mInlineFile;
	}

	/**
	 * @return the inventoryStatus
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * @return the inventoryStatusInStore
	 */
	public String getInventoryStatusInStore() {
		return mInventoryStatusInStore;
	}

	/**
	 * @return the juvenileSize
	 */
	public String getJuvenileSize() {
		return mJuvenileSize;
	}

	/**
	 * @return the mListPrice
	 */
	public double getListPrice() {
		return mListPrice;
	}

	/**
	 * Gets the longDescription.
	 * 
	 * @return the mLongDescription
	 */
	public String getLongDescription() {
		return mLongDescription;
	}

	/**
	 * Gets the mfrSuggestedAgeMax.
	 * 
	 * @return the mfrSuggestedAgeMax
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * Gets the mfrSuggestedAgeMin.
	 * 
	 * @return the mfrSuggestedAgeMin
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * @return the nmwaPercentage
	 */
	public float getNmwaPercentage() {
		return mNmwaPercentage;
	}

	/**
	 * @return the notifyMe
	 */
	public String getNotifyMe() {
		return mNotifyMe;
	}

	/**
	 * @return the mOnlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}
	
	/**
	 * @return the mParentCategoryDescription
	 */
	public String getParentCategoryDescription() {
		return mParentCategoryDescription;
	}
	
	/**
	 * @return the mParentCategoryId
	 */
	public String getParentCategoryId() {
		return mParentCategoryId;
	}
	
	/**
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}
	
	/**
	 * Returns the priceDisplay.
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}


	/**
	 * @return the mPriceInfo
	 */
	public PriceInfoVO getPriceInfo() {
		return mPriceInfo;
	}

	/**
	 * @return the mPriceSavingAmount
	 */
	public double getPriceSavingAmount() {
		return mPriceSavingAmount;
	}
	
	/**
	 * @return the mPriceSavingPercentage
	 */
	public double getPriceSavingPercentage() {
		return mPriceSavingPercentage;
	}
	

	/**
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}
	
	/**
	 * @return the primaryCanonicalImage
	 */
	public String getPrimaryCanonicalImage() {
		return mPrimaryCanonicalImage;
	}

	/**
	 * @return the productDimenssionHeight
	 */
	public String getProductDimenssionHeight() {
		return mProductDimenssionHeight;
	}

	/**
	 * @return the productDimenssionLength
	 */
	public String getProductDimenssionLength() {
		return mProductDimenssionLength;
	}

	/**
	 * @return the productDimenssionWidth
	 */
	public String getProductDimenssionWidth() {
		return mProductDimenssionWidth;
	}

	/**
	 * @return the productItemWeight
	 */
	public String getProductItemWeight() {
		return mProductItemWeight;
	}



	/**
	 * Gets the mPromotionalStickerDisplay.
	 * 
	 * @return the mPromotionalStickerDisplay
	 */

	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}

	/**
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * @return the mReviewRating
	 */
	public int getReviewRating() {
		return mReviewRating;
	}

	/**
	 * @return the s2s
	 */
	public String getS2s() {
		return mS2s;
	}

	/**
	 * Gets the mSafetyWarning.
	 * 
	 * @return the mSafetyWarning
	 */

	public List<String> getSafetyWarning() {
		return mSafetyWarning;
	}

	/**
	 * @return the mSalePrice
	 */
	public double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * Gets the searchPrimaryImageURL.
	 * 
	 * @return the searchPrimaryImageURL
	 */
	public String getSearchPrimaryImageURL() {
		return mSearchPrimaryImageURL;
	}

	/**
	 * @return the secondaryImage
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}
	
	/**
	 * @return the secondaryCanonicalImage
	 */
	public String getSecondaryCanonicalImage() {
		return mSecondaryCanonicalImage;
	}

	/**
	 * @return the shipWindowMax
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * @return the shipWindowMin
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * @return the skuType
	 */
	public String getSkuType() {
		return mSkuType;
	}

	/**
	 * @return the storeAvailablityMessage
	 */
	public String getStoreAvailablityMessage() {
		return mStoreAvailablityMessage;
	}

	/**
	 * @return the storeMessage
	 */
	public String getStoreMessage() {
		return mStoreMessage;
	}

	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * @return the suggestAgeMessage
	 */
	public String getSuggestAgeMessage() {
		return mSuggestAgeMessage;
	}

	/**
	 * @return the swatchImage
	 */
	public String getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * @return the upcNumbers
	 */
	public Set<String> getUpcNumbers() {
		return mUpcNumbers;
	}

	/**

	 * Gets the mVideoGameEsrb.
	 * 
	 * @return the mVideoGameEsrb
	 */

	public String getVideoGameEsrb() {
		return mVideoGameEsrb;
	}
	/**
	 * @return the mDeleted
	 */
	public boolean isDeleted() {
		return mDeleted;
	}

	/**
	 * @return the mDisplayable
	 */
	public boolean isDisplayable() {
		return mDisplayable;
	}

	/**
	 * @return the dropShipFlag
	 */
	public boolean isDropShipFlag() {
		return mDropShipFlag;
	}

	/**
	 * @return the newItem
	 */
	public boolean isNewItem() {
		return mNewItem;
	}

	/**
	 * @return the presellable
	 */
	public boolean isPresellable() {
		return mPresellable;
	}

	/**
	 * @return the mRegistryEligibility
	 */
	public boolean isRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * @return the mSearchable
	 */
	public boolean isSearchable() {
		return mSearchable;
	}

	/**
	 * @return the mShipInOrigContainer
	 */
	
	public boolean isShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * @return the skuRegistryEligibility
	 */
	public boolean isSkuRegistryEligibility() {
		return mSkuRegistryEligibility;
	}

	/**
	 * @return the unCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}

	/**
	 * Sets the active.
	 * 
	 * @param pActive the active to set
	 */
	public void setActive(Boolean pActive) {
		mActive = pActive;
	}

	/**
	 * @param pAlterNateImages the alterNateImages to set
	 */
	public void setAlterNateImages(List<String> pAlterNateImages) {
		mAlterNateImages = pAlterNateImages;
	}
	/**
	 * @param pAlterNateCanonicalImages the AlterNateCanonicalImages to set
	 */
	public void setAlterNateCanonicalImages(List<String> pAlterNateCanonicalImages) {
		mAlterNateCanonicalImages = pAlterNateCanonicalImages;
	}

	/**
	 * Sets the assembledWeight.
	 * 
	 * @param pAssembledWeight the assembledWeight to set
	 */
	public void setAssembledWeight(String pAssembledWeight) {
		mAssembledWeight = pAssembledWeight;
	}

	/**
	 * @param pAssemblyDimenssionHeight the assemblyDimenssionHeight to set
	 */
	public void setAssemblyDimenssionHeight(String pAssemblyDimenssionHeight) {
		mAssemblyDimenssionHeight = pAssemblyDimenssionHeight;
	}

	/**
	 * @param pAssemblyDimenssionLength the assemblyDimenssionLength to set
	 */
	public void setAssemblyDimenssionLength(String pAssemblyDimenssionLength) {
		mAssemblyDimenssionLength = pAssemblyDimenssionLength;
	}

	/**
	 * @param pAssemblyDimenssionWidth the assemblyDimenssionWidth to set
	 */
	public void setAssemblyDimenssionWidth(String pAssemblyDimenssionWidth) {
		mAssemblyDimenssionWidth = pAssemblyDimenssionWidth;
	}

	/**
	 * @param pAssemblyRequired the assemblyRequired to set
	 */
	public void setAssemblyRequired(String pAssemblyRequired) {
		mAssemblyRequired = pAssemblyRequired;
	}

	/**
	 * @param pAvailableInventory the availableInventory to set
	 */
	public void setAvailableInventory(long pAvailableInventory) {
		mAvailableInventory = pAvailableInventory;
	}

	/**
	 * @param pAvailableInventoryInStore the availableInventoryInStore to set
	 */
	public void setAvailableInventoryInStore(long pAvailableInventoryInStore) {
		mAvailableInventoryInStore = pAvailableInventoryInStore;
	}

	/**
	 * @param pBppEligible the bppEligible to set
	 */
	public void setBppEligible(Boolean pBppEligible) {
		mBppEligible = pBppEligible;
	}

	/**
	 * @param pBppId the bppId to set
	 */
	public void setBppId(Float pBppId) {
		mBppId = pBppId;
	}

	/**
	 * @param pBppName the bppName to set
	 */
	public void setBppName(String pBppName) {
		mBppName = pBppName;
	}

	/**
	 * @param pCanBeGiftWrapped the canBeGiftWrapped to set
	 */
	public void setCanBeGiftWrapped(String pCanBeGiftWrapped) {
		mCanBeGiftWrapped = pCanBeGiftWrapped;
	}

	/**
	 * @param pCollectionNames the collectionNames to set
	 */
	public void setCollectionNames(Map<String,String> pCollectionNames) {
		mCollectionNames = pCollectionNames;
	}

	/**
	 * Sets the color.
	 * 
	 * @param pColor the color to set
	 */
	public void setColor(ColorInfoVO pColor) {
		mColor = pColor;
	}

	/**
	 * @param pColorCode the colorCode to set
	 */
	public void setColorCode(String pColorCode) {
		mColorCode = pColorCode;
	}

	/**
	 * Sets the pCribMaterialTypes.
	 * 
	 * @param pCribMaterialTypes the id to set
	 */
	public void setCribMaterialTypes(List<String> pCribMaterialTypes) {
		this.mCribMaterialTypes = pCribMaterialTypes;
	}

	/**
	 * Sets the pCribStyles.
	 * 
	 * @param pCribStyles the id to set
	 */
	public void setCribStyles(List<String> pCribStyles) {
		this.mCribStyles = pCribStyles;
	}

	/**
	 * Sets the pCribTypes.
	 * 
	 * @param pCribTypes the id to set
	 */
	public void setCribTypes(List<String> pCribTypes) {
		this.mCribTypes = pCribTypes;
	}

	/**
	 * Sets the pCribWhatIsImp.
	 * 
	 * @param pCribWhatIsImp the id to set
	 */
	public void setCribWhatIsImp(List<String> pCribWhatIsImp) {
		this.mCribWhatIsImp = pCribWhatIsImp;
	}

	/**
	 * @param pCustomerPurchaseLimit the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * @param pDeleted the mDeleted to set
	 */
	public void setDeleted(boolean pDeleted) {
		this.mDeleted = pDeleted;
	}


	/**
	 * @param pDisplayable the mDisplayable to set
	 */
	public void setDisplayable(boolean pDisplayable) {
		this.mDisplayable = pDisplayable;
	}

	/**
	 * Sets the displayName.
	 * 
	 * @param pDisplayName the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * @param pDropShipFlag the dropShipFlag to set
	 */
	public void setDropShipFlag(boolean pDropShipFlag) {
		mDropShipFlag = pDropShipFlag;
	}

	/**
	 * Sets the mEsrbrating.
	 * 
	 * @param pEsrbrating the mEsrbrating to set
	 */

	public void setEsrbrating(String pEsrbrating) {
		this.mEsrbrating = pEsrbrating;
	}

	/**
	 * @param pFeatures the features to set
	 */
	public void setFeatures(Map<String, String> pFeatures) {
		mFeatures = pFeatures;
	}

	/**
	 * @param pFreightClass the freightClass to set
	 */
	public void setFreightClass(String pFreightClass) {
		mFreightClass = pFreightClass;
	}

	/**
	 * @param pFreightClassMessage the freightClassMessage to set
	 */
	public void setFreightClassMessage(String pFreightClassMessage) {
		mFreightClassMessage = pFreightClassMessage;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * Sets the images.
	 * 
	 * @param pImages the images to set
	 */
	public void setImages(List<Map<String, String>> pImages) {
		mImages = pImages;
	}

	/**
	 * @param pInlineFile the inlineFile to set
	 */
	public void setInlineFile(String pInlineFile) {
		mInlineFile = pInlineFile;
	}

	/**
	 * @param pInventoryStatus the inventoryStatus to set
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		mInventoryStatus = pInventoryStatus;
	}

	/**
	 * @param pInventoryStatusInStore the inventoryStatusInStore to set
	 */
	public void setInventoryStatusInStore(String pInventoryStatusInStore) {
		mInventoryStatusInStore = pInventoryStatusInStore;
	}

	/**
	 * @param pJuvenileSize the juvenileSize to set
	 */
	public void setJuvenileSize(String pJuvenileSize) {
		mJuvenileSize = pJuvenileSize;
	}

	/**
	 * @param pListPrice the mListPrice to set
	 */
	public void setListPrice(double pListPrice) {
		this.mListPrice = pListPrice;
	}


	/**
	 * Sets the longDescription.
	 * 
	 * @param pLongDescription the longDescription to set
	 */
	public void setLongDescription(String pLongDescription) {
		mLongDescription = pLongDescription;
	}

	/**
	 * Sets the mfrSuggestedAgeMax.
	 * 
	 * @param pMfrSuggestedAgeMax the mfrSuggestedAgeMax to set
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}

	/**
	 * Sets the mfrSuggestedAgeMin.
	 * 
	 * @param pMfrSuggestedAgeMin the mfrSuggestedAgeMin to set
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * @param pNewItem the newItem to set
	 */
	public void setNewItem(boolean pNewItem) {
		mNewItem = pNewItem;
	}
	/**
	 * @param pNmwaPercentage the nmwaPercentage to set
	 */
	public void setNmwaPercentage(float pNmwaPercentage) {
		mNmwaPercentage = pNmwaPercentage;
	}
    
	/**
	 * @param pNotifyMe the notifyMe to set
	 */
	public void setNotifyMe(String pNotifyMe) {
		mNotifyMe = pNotifyMe;
	}
	/**
	 * @param pOnlinePID the OnlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		this.mOnlinePID = pOnlinePID;
	}

	/**
	 * @param pParentCategoryDescription the mParentCategoryDescription to set
	 */
	public void setParentCategoryDescription(String pParentCategoryDescription) {
		this.mParentCategoryDescription = pParentCategoryDescription;
	}

	/**
	 * @param pParentCategoryId the mParentCategoryId to set
	 */
	public void setParentCategoryId(String pParentCategoryId) {
		this.mParentCategoryId = pParentCategoryId;
	}

	/**
	 * @param pPresellable the presellable to set
	 */
	public void setPresellable(boolean pPresellable) {
		mPresellable = pPresellable;
	}

	/**
	 * @param pPresellQuantityUnits the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}

	/**
	 * Sets/updates the priceDisplay.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * @param pPriceInfo to set
	 */
	public void setPriceInfo(PriceInfoVO pPriceInfo) {
		this.mPriceInfo = pPriceInfo;
	}

	/**
	 * @param pPriceSavingAmount the mPriceSavingAmount to set
	 */
	public void setPriceSavingAmount(double pPriceSavingAmount) {
		this.mPriceSavingAmount = pPriceSavingAmount;
	}

	/**
	 * @param pPriceSavingPercentage the mPriceSavingPercentage to set
	 */
	public void setPriceSavingPercentage(double pPriceSavingPercentage) {
		this.mPriceSavingPercentage = pPriceSavingPercentage;
	}

	/**
	 * @param pPrimaryImage the primaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}
	
	/**
	 * @param pPrimaryCanonicalImage the primaryCanonicalImage to set
	 */
	public void setPrimaryCanonicalImage(String pPrimaryCanonicalImage) {
		mPrimaryCanonicalImage = pPrimaryCanonicalImage;
	}


	/**
	 * @param pProductDimenssionHeight the productDimenssionHeight to set
	 */
	public void setProductDimenssionHeight(String pProductDimenssionHeight) {
		mProductDimenssionHeight = pProductDimenssionHeight;
	}

	/**
	 * @param pProductDimenssionLength the productDimenssionLength to set
	 */
	public void setProductDimenssionLength(String pProductDimenssionLength) {
		mProductDimenssionLength = pProductDimenssionLength;
	}

	/**
	 * @param pProductDimenssionWidth the productDimenssionWidth to set
	 */
	public void setProductDimenssionWidth(String pProductDimenssionWidth) {
		mProductDimenssionWidth = pProductDimenssionWidth;
	}

	/**
	 * @param pProductItemWeight the productItemWeight to set
	 */
	public void setProductItemWeight(String pProductItemWeight) {
		mProductItemWeight = pProductItemWeight;
	}
	/**
	 * Sets the mPromotionalStickerDisplay.
	 * 
	 * @param pPromotionalStickerDisplay the mPromotionalStickerDisplay to set
	 */

	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		this.mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}
	/**
	 * @param pRegistryEligibility the mRegistryEligibility to set
	 */
	public void setRegistryEligibility(boolean pRegistryEligibility) {
		this.mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * @param pRegistryWarningIndicator the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}

	/**
	 * @param pReviewRating to set
	 */
	public void setReviewRating(int pReviewRating) {
		mReviewRating = pReviewRating;
	}

	/**
	 * @param pS2s the s2s to set
	 */
	public void setS2s(String pS2s) {
		mS2s = pS2s;
	}

	/**
	 * Sets the mSafetyWarning.
	 * 
	 * @param pSafetyWarning the mSafetyWarning to set
	 */

	public void setSafetyWarning(List<String> pSafetyWarning) {
		this.mSafetyWarning = pSafetyWarning;
	}

	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}

	/**
	 * @param pSearchable the mSearchable to set
	 */
	public void setSearchable(boolean pSearchable) {
		this.mSearchable = pSearchable;
	}

	/**
	 * Sets the searchPrimaryImageURL.
	 * 
	 * @param pSearchPrimaryImageURL the searchPrimaryImageURL to set
	 */
	public void setSearchPrimaryImageURL(String pSearchPrimaryImageURL) {
		mSearchPrimaryImageURL = pSearchPrimaryImageURL;
	}

	/**
	 * @param pSecondaryImage the secondaryImage to set
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}
	

	/**
	 * @param pSecondaryCanonicalImage the secondaryCanonicalImage to set
	 */
	public void setSecondaryCanonicalImage(String pSecondaryCanonicalImage) {
		mSecondaryCanonicalImage = pSecondaryCanonicalImage;
	}


	/**
	 * @param pShipInOrigContainer the ShipInOrigContainer to set
	 */
	public void setShipInOrigContainer(boolean pShipInOrigContainer) {
		this.mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * @param pShipWindowMax the shipWindowMax to set
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * @param pShipWindowMin the shipWindowMin to set
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * @param pSkuRegistryEligibility the skuRegistryEligibility to set
	 */
	public void setSkuRegistryEligibility(boolean pSkuRegistryEligibility) {
		mSkuRegistryEligibility = pSkuRegistryEligibility;
	}

	/**
	 * @param pSkuType the skuType to set
	 */
	public void setSkuType(String pSkuType) {
		mSkuType = pSkuType;
	}

	/**
	 * @param pStoreAvailablityMessage the storeAvailablityMessage to set
	 */
	public void setStoreAvailablityMessage(String pStoreAvailablityMessage) {
		mStoreAvailablityMessage = pStoreAvailablityMessage;
	}

	/**
	 * @param pStoreMessage the storeMessage to set
	 */
	public void setStoreMessage(String pStoreMessage) {
		mStoreMessage = pStoreMessage;
	}

	/**
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @param pSuggestAgeMessage the suggestAgeMessage to set
	 */
	public void setSuggestAgeMessage(String pSuggestAgeMessage) {
		mSuggestAgeMessage = pSuggestAgeMessage;
	}

	/**
	 * @param pSwatchImage the swatchImage to set
	 */
	public void setSwatchImage(String pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}

	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * @param pUpcNumbers the upcNumbers to set
	 */
	public void setUpcNumbers(Set<String> pUpcNumbers) {
		mUpcNumbers = pUpcNumbers;
	}

	/**
	 * Sets the pVideoGameEsrb.
	 * 
	 * @param pVideoGameEsrb the id to set
	 */

	public void setVideoGameEsrb(String pVideoGameEsrb) {
		this.mVideoGameEsrb = pVideoGameEsrb;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mId;
	}

	/**
	 * @return the rmsSizeCode
	 */
	public String getRmsSizeCode() {
		return mRmsSizeCode;
	}

	/**
	 * @param pRmsSizeCode the rmsSizeCode to set
	 */
	public void setRmsSizeCode(String pRmsSizeCode) {
		mRmsSizeCode = pRmsSizeCode;
	}

	/**
	 * @return the rmsColorCode
	 */
	public String getRmsColorCode() {
		return mRmsColorCode;
	}

	/**
	 * @param pRmsColorCode the rmsColorCode to set
	 */
	public void setRmsColorCode(String pRmsColorCode) {
		mRmsColorCode = pRmsColorCode;
	}

	/**
	 * @return the originalproductIdOrSKN
	 */
	public String getOriginalproductIdOrSKN() {
		return mOriginalproductIdOrSKN;
	}

	/**
	 * @param pOriginalproductIdOrSKN the originalproductIdOrSKN to set
	 */
	public void setOriginalproductIdOrSKN(String pOriginalproductIdOrSKN) {
		mOriginalproductIdOrSKN = pOriginalproductIdOrSKN;
	}

	/**
	 * @return the sknOrigin
	 */
	public int getSknOrigin() {
		return mSknOrigin;
	}

	/**
	 * @param pSknOrigin the sknOrigin to set
	 */
	public void setSknOrigin(int pSknOrigin) {
		mSknOrigin = pSknOrigin;
	}

	/**
	 * @return the reviewRatingFraction
	 */
	public float getReviewRatingFraction() {
		return mReviewRatingFraction;
	}

	/**
	 * @param pReviewRatingFraction the reviewRatingFraction to set
	 */
	public void setReviewRatingFraction(float pReviewRatingFraction) {
		mReviewRatingFraction = pReviewRatingFraction;
	}
	

	/**
	 * @return mColorVariantExists
	 */
	public boolean isColorVariantExists() {
		return mColorVariantExists;
	}

	/**
	 * @param pColorVariantExists to set mColorVariantExists
	 */
	public void setColorVariantExists(boolean pColorVariantExists) {
		mColorVariantExists = pColorVariantExists;
	}

	/**
	 * @return mSizeVariantExists
	 */
	public boolean isSizeVariantExists() {
		return mSizeVariantExists;
	}

	/**
	 * @param pSizeVariantExists to set mSizeVariantExists
	 */
	public void setSizeVariantExists(boolean pSizeVariantExists) {
		mSizeVariantExists = pSizeVariantExists;
	}

	
	/**
	 * @return the mSizeSequenceNumber
	 */
	public int getSizeSequenceNumber() {
		return mSizeSequenceNumber;
	}

	/**
	 * @param pSizeSequenceNumber the mSizeSequenceNumber to set
	 */
	public void setSizeSequenceNumber(int pSizeSequenceNumber) {
		mSizeSequenceNumber = pSizeSequenceNumber;
	}

	/**
	 * @return mJuninileSizeDesc
	 */
	public String getJuninileSizeDesc() {
		return mJuninileSizeDesc;
	}

	
	/**
	 * @param pJuninileSizeDesc to set mJuninileSizeDesc
	 */
	public void setJuninileSizeDesc(String pJuninileSizeDesc) {
		mJuninileSizeDesc = pJuninileSizeDesc;
	}

	/**
	 * @return mColorSequenceNumber
	 */
	public int getColorSequenceNumber() {
		return mColorSequenceNumber;
	}

	/**
	 * @param pColorSequenceNumber to set mColorSequenceNumber
	 */
	public void setColorSequenceNumber(int pColorSequenceNumber) {
		mColorSequenceNumber = pColorSequenceNumber;
	}

	/**
	 * @return mSkuItem
	 */
	public RepositoryItem getSkuItem() {
		return mSkuItem;
	}

	
	/**
	 * @param pSkuItem to set mSkuItem
	 */
	public void setSkuItem(RepositoryItem pSkuItem) {
		mSkuItem = pSkuItem;
	}

	/**
	 * @return the mPromoString
	 */
	public List<String> getPromoString() {
		return mPromoString;
	}

	/**
	 * @param pPromoString the mPromoString to set
	 */
	public void setPromoString(List<String> pPromoString) {
		mPromoString = pPromoString;
	}

}
