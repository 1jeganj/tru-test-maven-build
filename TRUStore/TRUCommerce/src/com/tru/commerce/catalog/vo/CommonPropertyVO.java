/**
 * 
 */
package com.tru.commerce.catalog.vo;

import java.util.List;


/**
 * @author PA
 * @version 1.0
 *
 */
public class CommonPropertyVO {
	/**
	 * Property to hold product id.
	 */
	public String mProductId;
	/**
	 * Property to hold mLocale.
	 */
	public String mLocale;
	
	/**
	 * Property to hold product type.
	 */
	private String mProductType;

	/**
	 * Property to hold mDisplayName.
	 */
	private String mDisplayName;
	/**
	 * Property to hold mActive.
	 */
	private boolean mActive;

	/**
	 * Property to hold mColorCodeList.
	 */
	private  List<String> mColorCodeList;
	/**
	 * Property to hold mJuvenileSizesList.
	 */
	private List<String> mJuvenileSizesList;
	
	/**
	 * Property to hold mColorSizeVariantsAvailableStatus.
	 */
	private  String mColorSizeVariantsAvailableStatus;
	/**
	 * Property to hold mSfs.
	 */
	private String mSfs;
	/**
	 * Property to hold mIspu.
	 */
	private String mIspu;
	/**
	 * Property to hold mProductStatus.
	 */
	private String mProductStatus;
	/**
	 * Property to hold mChannelAvailability.
	 */
	private String mChannelAvailability;

	/**
	 * Property to hold mRusItemNumber.
	 */
	private String mRusItemNumber;
	
	/**
	 * Gets the productType.
	 * 
	 * @return the productType
	 */
	public String getProductType() {
		return mProductType;
	}

	/**
	 * Sets the productType.
	 * 
	 * @param pProductType the productType to set
	 */
	public void setProductType(String pProductType) {
		mProductType = pProductType;
	}

	/**
	 * Gets the displayName.
	 * 
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Sets the displayName.
	 * 
	 * @param pDisplayName the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return mProductId;
	}

	/**
	 * @param pProductId the productId to set
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * @param pLocale the locale to set
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}
	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean isActive() {
		return mActive;
	}

	/**
	 * Sets the active.
	 * 
	 * @param pActive the active to set
	 */
	public void setActive(boolean pActive) {
		mActive = pActive;
	}
	/**
	 * @return the colorCodeList
	 */
	public List<String> getColorCodeList() {
		return mColorCodeList;
	}

	/**
	 * @param pColorCodeList the colorCodeList to set
	 */
	public void setColorCodeList(List<String> pColorCodeList) {
		mColorCodeList = pColorCodeList;
	}
	/**
	 * @return the juvenileSizesList
	 */
	public List<String> getJuvenileSizesList() {
		return mJuvenileSizesList;
	}

	/**
	 * @param pJuvenileSizesList the juvenileSizesList to set
	 */
	public void setJuvenileSizesList(List<String> pJuvenileSizesList) {
		mJuvenileSizesList = pJuvenileSizesList;
	}
	
	/**
	 * @return the colorSizeVariantsAvailableStatus
	 */
	public String getColorSizeVariantsAvailableStatus() {
		return mColorSizeVariantsAvailableStatus;
	}

	/**
	 * @param pColorSizeVariantsAvailableStatus the colorSizeVariantsAvailableStatus to set
	 */
	public void setColorSizeVariantsAvailableStatus(
			String pColorSizeVariantsAvailableStatus) {
		mColorSizeVariantsAvailableStatus = pColorSizeVariantsAvailableStatus;
	}
	/**
	 * @return the sfs
	 */
	public String getSfs() {
		return mSfs;
	}

	/**
	 * @param pSfs the sfs to set
	 */
	public void setSfs(String pSfs) {
		mSfs = pSfs;
	}
	/**
	 * @return the ispu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the ispu to set
	 */
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}
	/**
	 * @return the mProductStatus
	 */
	public String getProductStatus() {
		return mProductStatus;
	}

	/**
	 * @param pProductStatus to set
	 */
	public void setProductStatus(String pProductStatus) {
		this.mProductStatus = pProductStatus;
	}
	/**
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * @param pChannelAvailability the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}
	/**
	 * @return the rusItemNumber
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	/**
	 * @param pRusItemNumber the rusItemNumber to set
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		mRusItemNumber = pRusItemNumber;
	}
}
