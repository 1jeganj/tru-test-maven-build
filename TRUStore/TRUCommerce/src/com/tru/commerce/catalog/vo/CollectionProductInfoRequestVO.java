package com.tru.commerce.catalog.vo;

import com.tru.commerce.TRUCommerceConstants;

/**
 * @version 1.0
 * @author PA
 */
public class CollectionProductInfoRequestVO extends CommonPropertyVO{

	/**
	 * Property to hold collection id.
	 */
	private String mCollectionId;

	/**
	 * Gets the collectionId.
	 * 
	 * @return the collectionId
	 */
	public String getCollectionId() {
		return mCollectionId;
	}

	/**
	 * Sets the collectionId.
	 * 
	 * @param pCollectionId the productId to set
	 */
	public void setCollectionId(String pCollectionId) {
		mCollectionId = pCollectionId;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		final int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + (mLocale == null ? 0 : mLocale.hashCode());
		result = prime * result + (mCollectionId == null ? 0 : mCollectionId.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		CollectionProductInfoRequestVO other = (CollectionProductInfoRequestVO) pObject;
		if (mLocale == null) {
			if (other.mLocale != null) {
				return false;
			}
		} else if (!mLocale.equals(other.mLocale)) {
			return false;
		}
		if (mCollectionId == null) {
			if (other.mCollectionId != null) {
				return false;
			}
		} else if (!mCollectionId.equals(other.mCollectionId)) {
			return false;
		}
		return true;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return String - String output
	 */
	@Override
	public String toString() {
		return this.mCollectionId;
	}
}
