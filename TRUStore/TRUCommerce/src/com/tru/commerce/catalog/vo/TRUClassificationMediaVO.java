/**
 * 
 */
package com.tru.commerce.catalog.vo;

/**
 * @author PA
 * @version 1.0
 *
 */
public class TRUClassificationMediaVO {

	private String mMediaId;
	private String mMediaName;
	private String mMediaType;
	private String mMediaURL;
	private String mMediaHTMLContent;
	/**
	 * @return the mMediaId
	 */
	public String getMediaId() {
		return mMediaId;
	}
	/**
	 * @param pMediaId the mMediaId to set
	 */
	public void setMediaId(String pMediaId) {
		this.mMediaId = pMediaId;
	}
	/**
	 * @return the mMediaName
	 */
	public String getMediaName() {
		return mMediaName;
	}
	/**
	 * @param pMediaName the mMediaName to set
	 */
	public void setMediaName(String pMediaName) {
		this.mMediaName = pMediaName;
	}
	/**
	 * @return the mMediaType
	 */
	public String getMediaType() {
		return mMediaType;
	}
	/**
	 * @param pMediaType the mMediaType to set
	 */
	public void setMediaType(String pMediaType) {
		this.mMediaType = pMediaType;
	}
	/**
	 * @return the mMediaURL
	 */
	public String getMediaURL() {
		return mMediaURL;
	}
	/**
	 * @param pMediaURL the mMediaURL to set
	 */
	public void setMediaURL(String pMediaURL) {
		this.mMediaURL = pMediaURL;
	}
	/**
	 * @return the mMediaHTMLContent
	 */
	public String getMediaHTMLContent() {
		return mMediaHTMLContent;
	}
	/**
	 * @param pMediaHTMLContent the mMediaHTMLContent to set
	 */
	public void setMediaHTMLContent(String pMediaHTMLContent) {
		this.mMediaHTMLContent = pMediaHTMLContent;
	}
	
}
