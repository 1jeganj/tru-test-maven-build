package com.tru.commerce.catalog.vo;

import java.util.ArrayList;
import java.util.Map;

/**
 * This class is used to get and set collection Products with sku map.
 * @author PA
 * @version 1.0
 */
public class CollectionInfoVO {
	/**
	 * Property to hold  collection Products with sku map.
	 */
	private Map<String, ArrayList<String>> mProductWithSkuMap;
	
	/**
	 * @return mProductWithSkuMap
	 */
	public Map<String, ArrayList<String>> getProductWithSkuMap() {
		return mProductWithSkuMap;
	}
	
	/**
	 * @param pProductWithSkuMap the productWithSkuMap to set.
	 */
	public void setProductWithSkuMap(
			Map<String, ArrayList<String>> pProductWithSkuMap) {
		mProductWithSkuMap = pProductWithSkuMap;
	}
}
