package com.tru.commerce.catalog.vo;

import java.util.List;

import com.tru.common.TRUConstants;
/**
 * This class holds category related information.
 * 
 * @author PA
 * @version 1.0
 */
 
public class CategoryVO {
	
	/** The m category display status. */
	private String mCategoryDisplayStatus;
	
	/** The m category displayOrder. */
	private int mCategoryDisplayOrder;
	
	/**
      *  Property to hold categoryHtmlContent.
	  */
	private String mCategoryHtmlContent;

	/**  
	 * Property to hold categoryId.
	 */
	
	private String mCategoryId;

	/** 
	 * Property to hold categoryImage.
	 */
	
	private String mCategoryImage;
	/**
	 * Property to hold categoryName.
	 */
	private String mCategoryName;
     /** 
	 * Property to hold categoryURL.
	 */
	
	private String mCategoryURL;

	/**
	 * Property to hold MegaMenuSortPriority.
	 */
	private Integer mMegaMenuSortPriority;

	/**
	 *  Property to hold subCategories.
	 */
	private List<CategoryVO> mSubCategories;
	
	/**
	 * Property to hold mSubCategoryCount.
	 * 
	 */
	private Integer mSubCategoryCount = Integer.valueOf(TRUConstants.ZERO);
	
	/**
	 * @return the classificationDisplayOrder
	 */
	public int getCategoryDisplayOrder() {
		return mCategoryDisplayOrder;
	}

	/** 
	 * Gets the categoryHtmlContent.
	 * @return the mCategoryHtmlContent
	 */
	public String getCategoryHtmlContent() {
		return mCategoryHtmlContent;
	}

	/** 
	 * Gets the categoryId.
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}

	/** 
	 * Gets the Category Image.
	 * @return  category Image
	 */
	public String getCategoryImage() {
		return mCategoryImage;
	}

	/** 
	 * Gets the categoryName.
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}

	/** 
	 * Gets the categoryURL.
	 * @return the categoryURL
	 */
	public String getCategoryURL() {
		return mCategoryURL;
	}

	/** 
	 * Gets the megaMenuSortPriority.
	 * @return the megaMenuSortPriority
	 */
	public Integer getMegaMenuSortPriority() {
		return mMegaMenuSortPriority;
	}

	/** 
	 * Gets the subCategories.
	 * @return the subCategories
	 */
	public List<CategoryVO> getSubCategories() {
		return mSubCategories;
	}
	
	/**
	 * @return the subCategoryCount
	 */
	public Integer getSubCategoryCount() {
		return mSubCategoryCount;
	}
	
	/**
	 * @param pCategoryDisplayOrder the categoryDisplayOrder to set
	 */
	public void setCategoryDisplayOrder(int pCategoryDisplayOrder) {
		mCategoryDisplayOrder = pCategoryDisplayOrder;
	}

	/** 
	 * Sets the categoryHtmlContent.
	 * @param pCategoryHtmlContent the categoryHtmlContent to set
	 */
	public void setCategoryHtmlContent(String pCategoryHtmlContent) {
		mCategoryHtmlContent = pCategoryHtmlContent;
	}

	/**
	 * Sets the categoryId.
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}

	/** 
	 * sets the Category Image.
	 * @param pCategoryImage the category image to set
	 */
	public void setCategoryImage(String pCategoryImage) {
		this.mCategoryImage = pCategoryImage;
	}

	/** 
	 * Sets the categoryName.
	 * @param pCategoryName the categoryName to set
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}


	/** 
	 * Sets the categoryURL.
	 * @param pCategoryURL the categoryURL to set
	 */
	public void setCategoryURL(String pCategoryURL) {
		mCategoryURL = pCategoryURL;
	}

	/** 
	 * Sets the megaMenuSortPriority.
	 * @param pMegaMenuSortPriority the megaMenuSortPriority to set
	 */
	public void setMegaMenuSortPriority(Integer pMegaMenuSortPriority) {
		mMegaMenuSortPriority = pMegaMenuSortPriority;
	}

	/** 
	 * Sets the subCategories.
	 * @param pSubCategories the subCategories to set
	 */
	public void setSubCategories(List<CategoryVO> pSubCategories) {
		mSubCategories = pSubCategories;
	}

	/**
	 * @param pSubCategoryCount the subCategoryCount to set
	 */
	public void setSubCategoryCount(Integer pSubCategoryCount) {
		mSubCategoryCount = pSubCategoryCount;
	}

	/**
	 * @return the categoryDisplayStatus
	 */
	public String getCategoryDisplayStatus() {
		return mCategoryDisplayStatus;
	}

	/**
	 * @param pCategoryDisplayStatus - the categoryDisplayStatus to set
	 */
	public void setCategoryDisplayStatus(String pCategoryDisplayStatus) {
		mCategoryDisplayStatus = pCategoryDisplayStatus;
	}	
}
