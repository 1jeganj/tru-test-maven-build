package com.tru.commerce.catalog.repository;

import java.util.HashSet;
import java.util.Set;

import atg.nucleus.Nucleus;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;

/**
 * TRUCategoryPromosPropertyDescriptor class extends the OOTB RepositoryPropertyDescriptor.
 * 
 * This class used to get associated promotions to the category.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCategoryPromosPropertyDescriptor extends RepositoryPropertyDescriptor{
	/**
	 * property to store serial version UID. 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * property to hold mLogger
	 */
	private static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(
			TRUCategoryPromosPropertyDescriptor.class);
	
	/**
	 * property to hold CatalogTools.
	 */
	private static TRUCatalogTools mCatalogTools;
	
	/** Constant to hold the catalogTools value. */
	private static final String CATALOG_TOOLS = "/atg/commerce/catalog/CatalogTools";
	
	/**
	 * This method returns the Set of Promotion Id associated to the category.
	 *  
	 * @param pItem repository item.
	 * @param pValue cached value.
	 * @return returns object : Set of Promotion Id.
	 */	
	@SuppressWarnings("unchecked")
	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("TRUCategoryPromosPropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) begins");
		}
		Set<String> allPromo = null;
		if(pItem == null && (mLogger != null && mLogger.isLoggingDebug())){
			mLogger.logDebug("Category Item is Null so Retuning with All Promo : " + allPromo);
			return allPromo;
		}
		if(mCatalogTools == null){
			mCatalogTools = (TRUCatalogTools) Nucleus.getGlobalNucleus().resolveName(CATALOG_TOOLS);
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) mCatalogTools.getCatalogProperties();
		Set<String> targetedPromos = (Set<String>) pItem.getPropertyValue(catalogProperties.getTargetedPromotionsPropertyName());
		Set<String> qualifiedPromos = (Set<String>) pItem.getPropertyValue(catalogProperties.getQualifiedPromotionsPropertyName());
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("Category Targeted Promotions : " + targetedPromos);
			mLogger.logDebug("Category Qualified Promotions : " + qualifiedPromos);
		}
		if(targetedPromos != null && !targetedPromos.isEmpty()){
			allPromo = new HashSet<String>();
			allPromo.addAll(targetedPromos);
		}
		if((qualifiedPromos != null && !qualifiedPromos.isEmpty()) && allPromo == null){
			allPromo = new HashSet<String>();
			allPromo.addAll(qualifiedPromos);
		}
		allPromo = mCatalogTools.getParentCatPromos(pItem,allPromo);
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("TRUCategoryPromosPropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) ends");
		}
		return allPromo;
	}

}
