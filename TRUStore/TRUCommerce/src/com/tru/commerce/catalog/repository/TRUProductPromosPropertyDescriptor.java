package com.tru.commerce.catalog.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.nucleus.Nucleus;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
/**
 * TRUProductPromosPropertyDescriptor class extends the OOTB RepositoryPropertyDescriptor.
 * 
 * This class used to get associated promotions to the products and the parent category.
 *
 */
public class TRUProductPromosPropertyDescriptor extends RepositoryPropertyDescriptor{

	/**
	 * property to store serial version UID. 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * property to hold mLogger
	 */
	private static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(
			TRUProductPromosPropertyDescriptor.class);
	/**
	 * property to hold ProductCatalog.
	 */
	private static Repository mProductCatalog;
	/**
	 * property to hold CatalogTools.
	 */
	private static TRUCatalogTools mCatalogTools;
	
	/** Constant to hold the catalogTools value. */
	public static final String CATALOG_TOOLS = "/atg/commerce/catalog/CatalogTools";
	
	/**
	 * This method returns the Set of Promotion Id associated to the products and parent categories.
	 *  
	 * @param pItem repository item.
	 * @param pValue cached value.
	 * @return returns object : Set of Promotion Id.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("TRUProductPromosPropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) begins");
		}
		Set<String> allPromo = null;
		if(pItem == null){
			if(mLogger != null && mLogger.isLoggingDebug()){
				mLogger.logDebug("Product Item is Null so Retuning with All Promo : " + allPromo);
			}
			return allPromo;
		}
		if(mCatalogTools == null){
			mCatalogTools = (TRUCatalogTools) Nucleus.getGlobalNucleus().resolveName(CATALOG_TOOLS);
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) mCatalogTools.getCatalogProperties();
		Set<String> targetedPromos = (Set<String>) pItem.getPropertyValue(catalogProperties.getTargetedPromotionsPropertyName());
		Set<String> qualifiedPromos = (Set<String>) pItem.getPropertyValue(catalogProperties.getQualifiedPromotionsPropertyName());
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("Product Targeted Promotions : " + targetedPromos);
			mLogger.logDebug("Product Qualified Promotions : " + qualifiedPromos);
		}
		if(targetedPromos != null && !targetedPromos.isEmpty()){
			allPromo = new HashSet<String>();
			allPromo.addAll(targetedPromos);
		}
		if(qualifiedPromos != null && !qualifiedPromos.isEmpty()){
			if(allPromo == null){
				allPromo = new HashSet<String>();
			}
			allPromo.addAll(qualifiedPromos);
		}
		List<String> parentCat = (List<String>) pItem.getPropertyValue(catalogProperties.getParentClassificationsItemPropertyName());
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("Product Parent Classification Item IDs : " + parentCat);
		}
		if(parentCat != null && !parentCat.isEmpty()){
			if(mProductCatalog == null){
				mProductCatalog = pItem.getRepository();
			}
			if(mLogger != null && mLogger.isLoggingDebug()){
				mLogger.logDebug("Product Catalog Repository : " + mProductCatalog);
			}
			RepositoryItem classificationItem = null;
			Set<String> catPromo = new HashSet<String>();
			for(String lClasId : parentCat){
				try {
					classificationItem = mProductCatalog.getItem(lClasId, catalogProperties.getClassificationItemDescPropertyName());
					if(classificationItem != null){
						catPromo = (Set<String>) classificationItem.getPropertyValue(catalogProperties.getCategoryPromotionsPropertyName());
						if(mLogger != null && mLogger.isLoggingDebug()){
							mLogger.logDebug("Promotion Associated to Category : " + catPromo);
						}
					}
					if(catPromo != null && !catPromo.isEmpty()){
						if(allPromo == null){
							allPromo = new HashSet<String>();
						}
						allPromo.addAll(catPromo);
					}
				} catch (RepositoryException exc) {
					if(mLogger != null && mLogger.isLoggingError()){
						mLogger.logError("Classification Item not found for id :"+lClasId,exc);
					}
				}
			}
		}
		if(mLogger != null && mLogger.isLoggingDebug()){
			mLogger.logDebug("All Product Level Promotion : " + allPromo);
			mLogger.logDebug("TRUProductPromosPropertyDescriptor.getPropertyValue(RepositoryItemImpl pItem, Object pValue) ends");
		}
		return allPromo;
	}
}
