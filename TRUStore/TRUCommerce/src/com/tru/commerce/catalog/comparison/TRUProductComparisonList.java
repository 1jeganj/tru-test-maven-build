package com.tru.commerce.catalog.comparison;


import atg.commerce.catalog.comparison.ProductComparisonList;
import atg.repository.RepositoryItem;

/**
 * This class extends OOTB ProductComparisonList. It adds custom property mCurrentCategoryId which is used to
 * maintain the current category Id.
 */
public class TRUProductComparisonList extends ProductComparisonList {

	/**
	 * Property to hold mCurrentCategoryId
	 */
	private String mCurrentCategoryId;

	/**
	 * Property to hold PreviousPageUrl
	 */
	private String mPreviousPageUrl;

	/**
	 * Getter for mPreviousPageUrl
	 * 
	 * @return the previousPageUrl
	 */
	public String getPreviousPageUrl() {
		return mPreviousPageUrl;
	}

	/**
	 *
	 * 
	 * @param pPreviousPageUrl
	 *            the previousPageUrl to set
	 */
	public void setPreviousPageUrl(String pPreviousPageUrl) {
		mPreviousPageUrl = pPreviousPageUrl;
	}

	/**
	 * Getter for mCurrentCategoryId
	 * 
	 * @return mCurrentCategoryId - CurrentCategoryId
	 */
	public String getCurrentCategoryId() {
		return mCurrentCategoryId;
	}

	/**
	 * Sets mCurrentCategoryId
	 * 
	 * @param pCurrentCategoryId - CurrentCategoryId
	 */
	public void setCurrentCategoryId(String pCurrentCategoryId) {
		this.mCurrentCategoryId = pCurrentCategoryId;
	}

	/**
	 * Create a product comparison list Entry for the given category, product and sku.
	 * 
	 * @param pCategory - Holds the Category RepositoryItem.
	 * @param pProduct - Holds the Product RepositoryItem.
	 * @param pSku - Holds the SKU RepositoryItem
	 * @return Entry - Holds the Entry Map Object.
	 */
	@Override
	protected Entry createListEntry(RepositoryItem pCategory, RepositoryItem pProduct, RepositoryItem pSku) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductComparisonList.createListEntry");
		}
		Entry entry = new Entry();
		entry.setCategory(pCategory);
		entry.setProduct(pProduct);
		entry.setSku(pSku);
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductComparisonList.createListEntry");
		}
		return entry;
	}
}
