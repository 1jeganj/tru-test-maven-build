package com.tru.commerce.catalog.comparison;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.catalog.comparison.ProductComparisonList;
import atg.commerce.catalog.comparison.ProductComparisonList.Entry;
import atg.commerce.catalog.comparison.ProductListHandler;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.TRUStoreConfiguration;

/**
 * This class extends ProductListHandler and will perform the following Operations:
 * 
 * 1) Added handleCompare method to redirect the user to the comparison page after appending products added and category
 * id to the url.
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUProductListHandler extends ProductListHandler {

	/**
	 * Property to hold success compareSuccessURL
	 */
	private String mCompareSuccessURL;

	/**
	 * Property to hold success compareErrorURL
	 */
	private String mCompareErrorURL;

	/**
	 * Property to hold StoreConfiguration
	 */
	private TRUStoreConfiguration mStoreConfiguration;

	/**
	 * Property to hold mCatalogTools
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method returns CompareErrorURL String.
	 * 
	 * @return the compareErrorURL
	 */
	public String getCompareErrorURL() {
		return mCompareErrorURL;
	}

	/**
	 * 
	 * Set the compareErrorURL property.
	 * 
	 * @param pCompareErrorURL the compareErrorURL to set
	 */
	public void setCompareErrorURL(String pCompareErrorURL) {
		mCompareErrorURL = pCompareErrorURL;
	}

	/**
	 * This method returns compareSuccessURL String.
	 * 
	 * @return the compareSuccessURL
	 */
	public String getCompareSuccessURL() {
		return mCompareSuccessURL;
	}

	/**
	 * Set the pCompareSuccessURL property.
	 * 
	 * @param pCompareSuccessURL the compareSuccessURL to set
	 */
	public void setCompareSuccessURL(String pCompareSuccessURL) {
		mCompareSuccessURL = pCompareSuccessURL;
	}

	/**
	 * Gets the storeConfiguration
	 * 
	 * @return the storeConfiguration
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the storeConfiguration
	 * 
	 * @param pStoreConfiguration the storeConfiguration to set
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}

	/**
	 * This method will be used to perform the Compare process validations .
	 * 
	 * @param pRequest - the servlet's request
	 * @param pResponse - the servlet's response
	 * @exception ServletException - if there was an error while executing the code
	 */
	protected void preCompare(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("EXECUTING:: TRUProductListHandler.preCompare method..");
		}
	}

	/**
	 * This method will get the productList and constructs a productIds String which will hold the products added to
	 * comparison list. ProductIds string will hold the productIds separated with "|". It also appends categoryId as one
	 * more parameter to the query string. It then appends this productIds and categoryId to the success url and
	 * redirects to the compare page.
	 * 
	 * @param pRequest - Holds the DynamoHttpServletRequest.
	 * @param pResponse - Holds the DynamoHttpServletResponse.
	 * @return boolean Value.
	 * @throws IOException - IOException if an error occurs.
	 * @throws ServletException - ServletException if an error occurs.
	 */
	public boolean handleCompare(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductListHandler.handleCompare method..");
		}
		String productIdString = generateProductIds();
		if (!StringUtils.isBlank(productIdString)) {
			TRUProductComparisonList productList = (TRUProductComparisonList) getProductList();
			if (!StringUtils.isBlank(productList.getCurrentCategoryId())) {
				setCategoryID(productList.getCurrentCategoryId());
			}
			StringBuffer compareURL = new StringBuffer();
			compareURL.append(getCompareSuccessURL());
			if (!StringUtils.isBlank(getCategoryID())) {
				compareURL.append(TRUCommerceConstants.QUESTION_MARK);
				compareURL.append(TRUCommerceConstants.CATEGORY_ID);
				compareURL.append(TRUCommerceConstants.EQUAL);
				compareURL.append(getCategoryID());
				compareURL.append(TRUCommerceConstants.AMPERSAND);
			} else {
				compareURL.append(TRUCommerceConstants.QUESTION_MARK);
			}
			compareURL.append(getStoreConfiguration().getProductIdsParameterName());
			compareURL.append(TRUCommerceConstants.EQUAL);
			compareURL.append(productIdString);

			setCompareSuccessURL(compareURL.toString());
			if (isLoggingDebug()) {
				vlogDebug("Compare URL String : {0}", compareURL);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductListHandler.handleCompare method..");
		}
		return checkFormRedirect(getCompareSuccessURL(),
				getCompareErrorURL(), pRequest, pResponse);
	}

	/**
	 *  This method used to generate product ids string separated with not symbol
	 * @return productIdString
	 */
	@SuppressWarnings("rawtypes")
	private String generateProductIds() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductListHandler.generateProductIds method..");
		}
		String productIdString = null;
		List items = getProductList().getItems();
		if (items != null && !items.isEmpty()) {
			int productIdsLength = items.size();
			Entry entry = null;
			int index = 0;
			String productId = null;
			productIdString = TRUCommerceConstants.EMPTY_STRING;
			for (Iterator iterator = items.iterator(); iterator.hasNext();) {
				entry = (Entry) iterator.next();
				productId = entry.getProduct().getRepositoryId();
				if (index < productIdsLength - TRUCommerceConstants.ONE) {
					productIdString = new StringBuffer(productIdString).append(productId)
							.append(TRUCommerceConstants.NOT_SYMBOL).toString();
				} else {
					productIdString = new StringBuffer(productIdString).append(productId).toString();
				}
				index++;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductListHandler.generateProductIds method.. productIdString : {0}", productIdString);
		}
		return productIdString;
	}

	/**
	 * This method is overridden to append remove product error and success url's with category Id and product ids.
	 * 
	 * @param pRequest - Holds the DynamoHttpServletRequest.
	 * @param pResponse - Holds the DynamoHttpServletResponse.
	 * @throws IOException - IOException if an error occurs.
	 * @throws ServletException - ServletException if an error occurs.
	 */
	@Override
	public void preRemoveProduct(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductListHandler.preRemoveProduct method..");
		}
		StringBuffer removeProductSuccessURL = new StringBuffer();
		removeProductSuccessURL.append(getRemoveProductSuccessURL());
		removeProductSuccessURL.append(TRUCommerceConstants.QUESTION_MARK);
		removeProductSuccessURL.append(TRUCommerceConstants.CATEGORY_ID);
		removeProductSuccessURL.append(TRUCommerceConstants.EQUAL);
		removeProductSuccessURL.append(getCategoryID());
		setRemoveProductSuccessURL(removeProductSuccessURL.toString());

		StringBuffer removeProductErrorURL = new StringBuffer();
		removeProductErrorURL.append(getRemoveProductErrorURL());
		removeProductErrorURL.append(TRUCommerceConstants.QUESTION_MARK);
		removeProductErrorURL.append(TRUCommerceConstants.CATEGORY_ID);
		removeProductErrorURL.append(TRUCommerceConstants.EQUAL);
		removeProductErrorURL.append(getCategoryID());
		removeProductErrorURL.append(TRUCommerceConstants.AMPERSAND);
		removeProductErrorURL.append(getStoreConfiguration().getProductIdsParameterName());
		removeProductErrorURL.append(TRUCommerceConstants.EQUAL);
		removeProductErrorURL.append(generateProductIds());
		setRemoveProductErrorURL(removeProductErrorURL.toString());
		super.preRemoveProduct(pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductListHandler.preRemoveProduct method.. removeProductSuccessURL : {0}"
					+ " removeProductErrorURL : {1}", getRemoveProductSuccessURL(), getRemoveProductErrorURL());
		}
	}

	/**
	 * This method is overridden to append success URL with updated product ids.
	 * 
	 * @param pRequest - Holds the DynamoHttpServletRequest.
	 * @param pResponse - Holds the DynamoHttpServletResponse.
	 * @throws IOException - IOException if an error occurs.
	 * @throws ServletException - ServletException if an error occurs.
	 */
	@Override
	public void postRemoveProduct(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductListHandler.postRemoveProduct method..");
		}
		StringBuffer removeProductSuccessURL = new StringBuffer();
		removeProductSuccessURL.append(getRemoveProductSuccessURL());
		removeProductSuccessURL.append(TRUCommerceConstants.AMPERSAND);
		removeProductSuccessURL.append(getStoreConfiguration().getProductIdsParameterName());
		removeProductSuccessURL.append(TRUCommerceConstants.EQUAL);
		removeProductSuccessURL.append(generateProductIds());
		setRemoveProductSuccessURL(removeProductSuccessURL.toString());
		super.postRemoveCategory(pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUProductListHandler.postRemoveProduct method.. removeProductSuccessURL : {0}",
					getRemoveProductSuccessURL());
		}
	}
	
	/**
	 * This method is used to clear all compare element based on category Id.
	 * @param pRequest - Holds the DynamoHttpServletRequest.
	 * @param pResponse - Holds the DynamoHttpServletResponse.
	 * @throws ServletException - ServletException if an error occurs.
	 * @throws IOException - IOException if an error occurs.
	 * @return checkFormRedirect
	 */
	@Override
	public boolean handleClearList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException
		  {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUProductListHandler.handleClearList method..");
		}
		    ProductComparisonList list = getProductList();

		    if (list == null) {
		      addFormException(new DropletException(getUserMessage(TRUCommerceConstants.NO_PRODUCT_LIST, pRequest), TRUCommerceConstants.NO_PRODUCT_LIST));
		      if (!(checkFormRedirect(null, getClearListErrorURL(), pRequest, pResponse))){
		    	  return false;
		      }
		    }

		    preClearList(pRequest, pResponse);
		    if (!(checkFormRedirect(null, getClearListErrorURL(), pRequest, pResponse))){
		    	return false;
		    }

		    if (list != null ) {
		    	List compareList=getCatalogTools().clearedCompareItemListBasedOnCategory(list.getItems(),getCategoryID());
		    	if (isLoggingDebug()) {
					vlogDebug("TRUProductListHandler.handleClearList method.. ,compareList "+ compareList);
				}
		    	list.setItems(compareList);
		    	setProductList(list);
		    }
		    postClearList(pRequest, pResponse);
		    if (isLoggingDebug()) {
				vlogDebug("END:: TRUProductListHandler.handleClearList method.. , ");
			}
		    return checkFormRedirect(getClearListSuccessURL(), getClearListErrorURL(), pRequest, pResponse);
		  }
	
}