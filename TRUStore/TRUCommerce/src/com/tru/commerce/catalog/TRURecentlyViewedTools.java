package com.tru.commerce.catalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import atg.commerce.pricing.PricingModelComparator;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.tru.cache.TRUCollectionProductCache;
import com.tru.cache.TRUProductCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.CollectionProductInfoRequestVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoRequestVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.exception.TRUCommerceException;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.userprofiling.TRUPropertyManager;



/**
 * This class will have all supporting method to perform profile recently viewed operations.
 *
 * @author Professional Access
 * @version 1.0
 */

public class TRURecentlyViewedTools extends GenericService {

	/** Property to hold SiteMaxAllowedRecentlyViewedItems. */
	private String mSiteMaxAllowedRecentlyViewedItems;

	/** Property to hold PropertyManager. */
	private TRUPropertyManager mPropertyManager;

	/** property to hold Saved amount. */
	private String mSavedAmount;

	/** property to hold percentage. */
	private String mSavedPercentage;

	/** property to hold price. */
	private String mListPrice;

	/** property to hold price. */
	private String mSalePrice;

	/**  Holds the mProductPageUrl. */
	private String mProductPageUrl;

	/**  Holds the mProductPageUtil. */
	private TRUProductPageUtil mProductPageUtil;

	/** Property to hold TRUProductCache. */
	private TRUProductCache mProductCache;

	/** Property to hold TRUCollectionProductCache. */
	private TRUCollectionProductCache mCollectionProductCache;

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * This property holds CatalogProperties variable catalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/** Holds reference for TRUConfiguration. */
	private TRUConfiguration mTruConfiguration;
	
	


	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;

	/** The Rank one promotion. */
	private RepositoryItem mRankOnePromotion;

	/** The Promotion count. */
	private int mPromotionCount;

	/** The Promotions. */
	private List<RepositoryItem> mPromotions;

	/**
	 * This method used to add recently viewed product into profile.
	 *
	 * @param pProfile The user whose recentlyViewedProduct list will be retrieved.
	 * @param pProductId The product that will be set in the recentlyViewedItem 'productId' property.
	 * @throws RepositoryException if repository transaction fails
	 */
	public void addRecentlyViewedProduct(Profile pProfile, String pProductId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("In Recently viewed tools class");
		}
		if (pProfile == null || StringUtils.isBlank(pProductId)) {
			return;
		}
		if (!pProfile.isTransient()) {
			@SuppressWarnings("unchecked")
			List<String> recentlyViewedProductIdsList = (List<String>) pProfile.getPropertyValue(getPropertyManager()
					.getRecentlyViewedPropertyName());
			if (recentlyViewedProductIdsList == null) {
				recentlyViewedProductIdsList = new ArrayList<String>();
			} else {
				final Date currentDate = new Date();
				Date lastUpdateddate = (Date) pProfile.getPropertyValue(getPropertyManager().getRecentlyViewedDate());
				int diffInDays = TRUConstants.INT_MINUS_ONE;
				if (lastUpdateddate != null) {
					final Site site = SiteContextManager.getCurrentSite();
					final String thresholdDays = (String) site
							.getPropertyValue(getPropertyManager().getRecentlyViewedThreshold());
					int thresholdDaysNum = TRUConstants.INT_MINUS_ONE;
					if (thresholdDays != null) {
						try {
							thresholdDaysNum = Integer.parseInt(thresholdDays);
						} catch (NumberFormatException e) {
							vlogError("Error:TRURecentlyViewedTools.addRecentlyViewedProduct method", e);
							thresholdDaysNum = TRUConstants.INT_MINUS_ONE;
						}
						diffInDays = (int) ((currentDate.getTime() - lastUpdateddate.getTime()) / (TRUConstants.INT_THOUSAND
								* TRUConstants.INT_SIXTY * TRUConstants.INT_SIXTY * TRUConstants.INT_TWENTY_FOUR));
						if (diffInDays >= TRUConstants.ZERO && diffInDays > thresholdDaysNum) {
							recentlyViewedProductIdsList.clear();
						}
					}
				}
				createRecentlyViewedItemAfterValidation(pProfile, pProductId, recentlyViewedProductIdsList);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRURecentlyViewedTools.addRecentlyViewedProduct method..");
		}
	}

	/**
	 * This method used to create recently viewed item for specified product id.
	 * 
	 * @param pProfile - The profile of the user whose recently viewed list is to be updated.
	 * @param pProductId The product that will be set in the recentlyViewedItem 'productId' property.
	 * @param pSiteRecentlyViewedProductIds - Site specific recently viewed product id lists
	 * @throws RepositoryException - If an error occurred while processing the repository operations
	 */
	private void createRecentlyViewedItemAfterValidation(Profile pProfile, String pProductId,
			List<String> pSiteRecentlyViewedProductIds) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUecentlyViewedTools.createRecentlyViewedItemAfterValidation method..");
		}
		if (pSiteRecentlyViewedProductIds.contains(pProductId)) {
			pSiteRecentlyViewedProductIds.remove(pProductId);

		} else if (pSiteRecentlyViewedProductIds.size() >= (int) SiteContextManager.getCurrentSite().getPropertyValue(
				getSiteMaxAllowedRecentlyViewedItems())) {
			pSiteRecentlyViewedProductIds.remove(0);

		}
		pSiteRecentlyViewedProductIds.add(pProductId);
		final Date date = new Date();
		pProfile.setPropertyValue(getPropertyManager().getRecentlyViewedPropertyName(), pSiteRecentlyViewedProductIds);
		pProfile.setPropertyValue(getPropertyManager().getRecentlyViewedDate(), date);

		if (isLoggingDebug()) {
			logDebug("END:: TRURecentlyViewedTools.createRecentlyViewedProductItemFromCookie method..");
		}
	}

	/**
	 * Product page url.
	 *
	 * @param pSiteId the site id
	 * @param pProductInfoVO the product info vo
	 */
	public void setProductUrl(String pSiteId, ProductInfoVO pProductInfoVO){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRURecentlyViewedTools.productPageUrl method..");
		}
		if(pProductInfoVO != null) {
			final String siteId = pSiteId;
			final String onlinePID = pProductInfoVO.getDefaultSKU().getOnlinePID();
			String productPageUrl = null;
			if(!StringUtils.isEmpty(onlinePID)) {
				if(!StringUtils.isEmpty(siteId)) {
					productPageUrl = getProductPageUtil().getProductPageURL(onlinePID,siteId);
				} else {
					productPageUrl = getProductPageUtil().getProductPageURL(onlinePID);
				}
			}

			if (isLoggingDebug()) {
				logDebug("END:: TRURecentlyViewedTools.productPageUrl method..");
			}

			pProductInfoVO.setPdpURL(productPageUrl);
		}

	}

	/**  This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;

	/**
	 * This method populates the prices to bean.
	 *
	 * @param pProductVO the product vo
	 * @param pJson - pJson
	 * @return the price info
	 * @throws JSONException JSONException
	 */

	public StringBuilder setProductPrice(ProductInfoVO pProductVO, JSONObject pJson) throws JSONException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUPriceDroplet  method: service]");
		}
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		final Site site = SiteContextManager.getCurrentSite();
		// Need to get from jsp
		final String skuId = pProductVO.getDefaultSKU().getId();
		// Need to get from jsp
		final String productId = pProductVO.getProductId();

		// Get prices for SKU
		listPrice = getPricingTools().getListPriceForSKU(skuId, productId, site);
		salePrice = getPricingTools().getSalePriceForSKU(skuId, productId, site);
		final StringBuilder priceInfoStorage = new StringBuilder();
		vlogDebug("List Price for SKU : {0} is : {1}", skuId, listPrice);
		vlogDebug("Sale Price for SKU : {0} is : {1}", skuId, salePrice);
		if (listPrice != TRUConstants.DOUBLE_ZERO && salePrice != TRUConstants.DOUBLE_ZERO) {

			// This method will validates whether strike through price need to be displayed or not.
			final boolean calculateSavings = getPricingTools().validateThresholdPrices(site, listPrice, salePrice);
			// This method will calculate the Savings and updates the values in bean.
			if (calculateSavings) {
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				setSavedAmount(String.valueOf(savingsMap.get(TRUConstants.SAVED_AMOUNT)));

				setSavedPercentage(String.valueOf(savingsMap.get(TRUConstants.SAVED_PERCENTAGE)));

				setListPrice(String.valueOf(listPrice));

				setSalePrice(String.valueOf(salePrice));

				priceInfoStorage.append(TRUConstants.PIPE_STRING).append(getSavedPercentage()).append(TRUConstants.PIPE_STRING)
				.append(getListPrice()).append(TRUConstants.PIPE_STRING).append(getSalePrice())
				.append(TRUConstants.PIPE_STRING).append(getSavedAmount());

				pJson.put(TRUConstants.SAVED_PERCENTAGE, getSavedPercentage());
				pJson.put(TRUConstants.LIST_PRICE, getListPrice());
				pJson.put(TRUConstants.SALE_PRICE, getSalePrice());
				pJson.put(TRUConstants.SAVED_AMOUNT, getSavedAmount());

			} else {
				setListPrice(String.valueOf(listPrice));
				setSalePrice(String.valueOf(salePrice));

				priceInfoStorage.append(getListPrice()).append(TRUConstants.PIPE_STRING).append(getSalePrice());

				pJson.put(TRUConstants.LIST_PRICE, getListPrice());
				pJson.put(TRUConstants.SALE_PRICE, getSalePrice());
			}
		} else {
			setListPrice(String.valueOf(listPrice));
			setSalePrice(String.valueOf(salePrice));
			priceInfoStorage.append(getListPrice()).append(TRUConstants.PIPE_STRING).append(getSalePrice());

			pJson.put(TRUConstants.LIST_PRICE, getListPrice());
			pJson.put(TRUConstants.SALE_PRICE, getSalePrice());
		}

		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUPriceDroplet  method: service]");
		}
		return priceInfoStorage;
	}

	/**
	 * This method returns the Collection Price in the Range format.
	 *
	 * @param pCollectionVO the CollectionInfoVo vo
	 * @param pJson - pJson
	 * @return the price info
	 * @throws JSONException JSONException
	 */
	public String setCollectionPrice(CollectionProductInfoVO pCollectionVO, JSONObject pJson) throws JSONException {

		if (isLoggingDebug()) {
			logDebug("BEGIN ::: Enter into TRURecentlyViewedTools.setCollectionPrice Method");
		}

		if(pCollectionVO == null) {
			return null;
		}
		StringBuilder priceInfoStorage = new StringBuilder();
		Site site = SiteContextManager.getCurrentSite();
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;

		List<Double> allListPriceList = new ArrayList<Double>();
		List<Double> allSalePriceList = new ArrayList<Double>();
		double minlist = TRUConstants.DOUBLE_ZERO;
		double maxlist = TRUConstants.DOUBLE_ZERO;
		double minsale = TRUConstants.DOUBLE_ZERO;
		double maxsale = TRUConstants.DOUBLE_ZERO;
		String skuId = null;
		List<ProductInfoVO> productInfoList = pCollectionVO.getProductInfoList();

		if(productInfoList == null || productInfoList.isEmpty()) {
			return null;
		}
		for(ProductInfoVO productVO : productInfoList) {
			String productId = productVO.getProductId();
			List<SKUInfoVO> skuInfoVOList = productVO.getChildSKUsList();
			if(skuInfoVOList == null || skuInfoVOList.isEmpty()) {
				continue;
			}
			for(SKUInfoVO skuVO : skuInfoVOList) {
				skuId = skuVO.getId();
				if(StringUtils.isNotEmpty(skuId) && StringUtils.isNotEmpty(productId) && site != null) {
					listPrice = (double) getPricingTools().getListPriceForSKU(skuId, productId, site);
					if(listPrice > 0) {
						allListPriceList.add(listPrice);
					}
					salePrice = getPricingTools().getSalePriceForSKU(skuId, productId, site);
					if(salePrice > 0) {
						allSalePriceList.add(salePrice);

					}
				}
			}
		}


		if (!allListPriceList.isEmpty()) {
			Collections.sort(allListPriceList);
			if (isLoggingDebug()) {
				vlogDebug("all list prices  after sorting : {0} ", allListPriceList);
			}
			minlist = allListPriceList.get(0);
			maxlist = allListPriceList.get(allListPriceList.size() - TRUCommerceConstants.INT_ONE);
		}

		if (!allSalePriceList.isEmpty()) {
			Collections.sort(allSalePriceList);
			if (isLoggingDebug()) {
				vlogDebug("all sale prices  after sorting :: {0}", allSalePriceList);
			}
			minsale = allSalePriceList.get(0);
			maxsale = allSalePriceList.get(allSalePriceList.size() - TRUCommerceConstants.INT_ONE);
		}


		if(minsale < 0 && maxsale < 0){
			if(minlist == maxlist){
				priceInfoStorage.append(TRUConstants.PIPE_STRING).append(minlist);  
			}
			priceInfoStorage.append(TRUConstants.PIPE_STRING).append(minlist).append(TRUConstants.PIPE_STRING).append(maxlist); 
		}

		else{
			if(minsale == maxsale){
				priceInfoStorage.append(TRUConstants.DOLLER).append(minsale);  
			}
			else{
				if(minsale>0 && maxsale>0)
				{
					priceInfoStorage.append(TRUConstants.DOLLER).append(minsale).append(TRUCommerceConstants.SPACE).append(TRUConstants.HYPHEN).append(TRUCommerceConstants.SPACE).append(TRUConstants.DOLLER).append(maxsale);
				}
				else{
					priceInfoStorage=new StringBuilder(TRUCommerceConstants.EMPTY_STRING);
				}
			}
		}

		return priceInfoStorage.toString();
	}

	/**
	 * This method is used to get Recently viewed item list.
	 *
	 * @param pProfile - pProfile
	 * @return productInfoList
	 */

	@SuppressWarnings({ "unchecked" })
	public List<String> getRecentlyViewedItemsList(Profile pProfile) {

		if (isLoggingDebug()) {
			logDebug("BEGIN:: getRecentlyViewedItemsList method..");
		}
		List<String> jsonList= new ArrayList<String>();
		// Fetching available items from the profile object
		final List<String> siteRecentlyViewedProductIds = (List<String>) pProfile.getPropertyValue(getPropertyManager().getRecentlyViewedPropertyName());
		if (siteRecentlyViewedProductIds != null && !siteRecentlyViewedProductIds.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("siteRecentlyViewedProductIds : {0}", siteRecentlyViewedProductIds);
			}

			// START: Date thresHold check for logged-in user
			final Date currentDate = new Date();
			final Date lastUpdateddate = (Date) pProfile.getPropertyValue(getPropertyManager().getRecentlyViewedDate());
			int diffInDays = TRUConstants.INT_MINUS_ONE;
			if (lastUpdateddate != null) {
				final Site site = SiteContextManager.getCurrentSite();
				final String thresholdDays = (String) site.getPropertyValue(getPropertyManager().getRecentlyViewedThreshold());
				int thresholdDaysNum = TRUConstants.INT_MINUS_ONE;
				if (thresholdDays != null) {
					try {
						thresholdDaysNum = Integer.parseInt(thresholdDays);
					} catch (NumberFormatException e) {
						vlogError("Error:TRURecentlyViewedTools.addRecentlyViewedProduct method", e);
						thresholdDaysNum = TRUConstants.INT_MINUS_ONE;
					}
					diffInDays = (int) ((currentDate.getTime() - lastUpdateddate.getTime()) / (TRUConstants.INT_THOUSAND
							* TRUConstants.INT_SIXTY * TRUConstants.INT_SIXTY * TRUConstants.INT_TWENTY_FOUR));
					if (diffInDays >= TRUConstants.ZERO && diffInDays > thresholdDaysNum) {
						siteRecentlyViewedProductIds.clear();
					}
				}
			}
			pProfile.setPropertyValue(getPropertyManager().getRecentlyViewedPropertyName(), siteRecentlyViewedProductIds);

			// END: Date thresHold check for logged-in user

			// Creating request VO to get the item from the cache object
			try {
				for (String recentlyViewedProductId : siteRecentlyViewedProductIds) {

					String productId = getCatalogTools().getProductIdFromOnlinePID(recentlyViewedProductId);
					ProductInfoVO productInfo = null;
					if(StringUtils.isNotEmpty(productId)) {
						ProductInfoRequestVO requestVO = new ProductInfoRequestVO();
						requestVO.setProductId(productId);
						productInfo = (ProductInfoVO) getProductCache().get(requestVO);

						if (productInfo != null) {
							List<SKUInfoVO> activeSkuList = productInfo.getChildSKUsList();
							SKUInfoVO defaultSku = getCatalogTools().findDefaultSkuUsingOnlinePID(recentlyViewedProductId, activeSkuList);
							if (defaultSku!=null) {
								productInfo.setDefaultSKU(defaultSku);
							}
							jsonList.add(getViewedItemDetail(productInfo));
						}
					} else {

						CollectionProductInfoVO  collectionProductInfoVO = getCollectionVo(recentlyViewedProductId);
						if(collectionProductInfoVO != null) {
							jsonList.add(getCollectionViewedItemDetail(collectionProductInfoVO));
						}
					}
				} 
			} catch (TRUCommerceException commerceException) {
				if (isLoggingError()) {
					vlogError(commerceException, "TRUCommerceException in TRUDisplayRecentlyViewedItemsDroplet.service method.");
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException, "TRUCommerceException in TRUDisplayRecentlyViewedItemsDroplet.service method.");
				}
			}

		}


		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDisplayRecentlyViewedItemsDroplet.service method. recentlyViewedItemsAvailable ");
		}
		return jsonList;
	}

	/**
	 * Sets the one rank promotion.
	 * 
	 * @param pProductId - producId
	 * @param pJson - json
	 * @param pSkuId -skuId
	 * @throws RepositoryException -throws repositoryException
	 * @throws JSONException -throws JSONException
	 */
	@SuppressWarnings("unchecked")
	public void setOneRankPromotion(String pProductId, JSONObject pJson,String pSkuId) throws RepositoryException, JSONException {

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURecentlyViewedTools  method: setOneRankPromotion]");
		}

		final String promotionCountinPDP = getTruConfiguration().getPromotionCountinPDP();
		int promoCount = TRUConstants.SIZE_ONE;

		final RepositoryItem productItem = getCatalogTools().findProduct(pProductId);
		if (productItem != null) {
			List<String> promotionsIds = null;
			List<RepositoryItem> childSkus= (List<RepositoryItem>) productItem.getPropertyValue(getCatalogProperties().getChildSKUsPropertyName());
			if(childSkus == null || childSkus.isEmpty()){
				return;
			}
			for (RepositoryItem skuItem : childSkus) {
				if(skuItem.getRepositoryId().equals(pSkuId)){
					promotionsIds=(List<String>) skuItem.getPropertyValue(getCatalogProperties().getSkuQualifiedForPromos());
					break;
				}

			}
			if (!StringUtils.isBlank(promotionCountinPDP)) {
				try {
					promoCount = Integer.parseInt(promotionCountinPDP);
				} catch (NumberFormatException nfe) {
					if (isLoggingError()) {
						vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP, nfe);
					}
				}
			}
			if (promotionsIds != null && !promotionsIds.isEmpty()) {
				List<RepositoryItem> promoList = new ArrayList<RepositoryItem>();
				RepositoryItem promotionItem = null;
				final TRUPromotionTools promotionTools = getPromotionTools();
				final Repository promoRepository = promotionTools.getPromotions();
				final TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools()
						.getPricingModelProperties();
				if (promoRepository != null) {
					final Date now = getPromotionTools().getCurrentDate().getTimeAsDate();
					for (String lPromoId : promotionsIds) {
						int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
						String promoId = lPromoId.substring(0,indexOf);
						try {
							promotionItem = promotionTools.getItemForId(pricingModelProperties.getPromotionItemDescName(),
									promoId);
							// checking promotion is expired or not
							if (promotionItem != null && !promotionTools.checkPromotionExpiration(promotionItem, now)) {
								promoList.add(promotionItem);
							}
						} catch (RepositoryException exc) {
							if (isLoggingError()) {
								vlogError("No Promotion Found for Promotion ID : {0}", lPromoId);
							}
						}
					}
				}
				Collections.sort(promoList, new PricingModelComparator());
				if (promoList != null && !promoList.isEmpty() && promoList.size() > promoCount) {

					String promotionName = (String) promoList.get(TRUConstants.ZERO).getPropertyValue(getCatalogProperties().getDescription());
					if (promotionName == null){
						promotionName = (String) promoList.get(TRUConstants.ZERO).getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
					}
					pJson.put(pProductId, promotionName);
					promoList = promoList.subList(TRUConstants.INTEGER_NUMBER_ONE, promoCount);
					setPromotionCount(promoList.size());
					setPromotions(promoList);
				} else if (promoList != null && !promoList.isEmpty()) {

					String promotionName = (String) promoList.get(TRUConstants.ZERO).getPropertyValue(getCatalogProperties().getDescription());
					if (promotionName == null){
						promotionName = (String) promoList.get(TRUConstants.ZERO).getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
					}

					pJson.put(pProductId, promotionName);
					promoList.remove(TRUConstants.ZERO);
					setPromotionCount(promoList.size());
					setPromotions(promoList);
				} else {
					pJson.put(pProductId, TRUConstants.EMPTY);
				}
			} else {
				pJson.put(pProductId, TRUConstants.EMPTY);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURecentlyViewedTools  method: setOneRankPromotion]");
		}
	}

	/**
	 * Gets the product vo.
	 *
	 * @param pProductId the product id
	 * @param pOnlinePID the OnlinePID 
	 * @return productInfoVO ProductInfoVO
	 * 
	 */
	public ProductInfoVO getProductVo(String pProductId, String pOnlinePID)  {
		ProductInfoVO productInfoVO = null;
		final ProductInfoRequestVO requestVO = new ProductInfoRequestVO();
		requestVO.setProductId(pProductId);
		try {
			productInfoVO = (ProductInfoVO) getProductCache().get(requestVO);
			if(productInfoVO != null) {
				String productType=productInfoVO.getColorSizeVariantsAvailableStatus();
				List<SKUInfoVO> childSkus=productInfoVO.getChildSKUsList();
				SKUInfoVO defaultSku = getCatalogTools().findDefaultSkuUsingOnlinePID(pOnlinePID, childSkus);
				if (StringUtils.isNotBlank(pOnlinePID) && TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equalsIgnoreCase(productType) && childSkus != null && childSkus.size() > TRUConstants._1 && defaultSku!=null) {
					productInfoVO.setDefaultSKU(defaultSku);
				}
			}
		} catch (TRUCommerceException e) {
			if (isLoggingError()) {
				logError("TRUCommerceException in TRURecentlyViewedTools.getProductVo method..",e);
			}
		}
		return productInfoVO;
	}


	/**
	 * Gets the collection vo.
	 *
	 * @param pCollectionId the collection id
	 * @return the collection vo
	 * setCollectionId
	 */
	public CollectionProductInfoVO getCollectionVo(String pCollectionId)  {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN::: TRURecentlyViewedTools.getCollectionVo method. Collection Id = {0}", pCollectionId);
		}
		CollectionProductInfoVO  collectionProductInfoVO = null;
		final CollectionProductInfoRequestVO requestVO = new CollectionProductInfoRequestVO();
		requestVO.setCollectionId(pCollectionId);
		try {
			collectionProductInfoVO = (CollectionProductInfoVO) getCollectionProductCache().get(requestVO);
		} catch (TRUCommerceException truException) {
			if (isLoggingError()) {
				vlogError(truException, "TRUCommerceException in TRURecentlyViewedTools.getCollectionVo method..");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRURecentlyViewedTools.getCollectionVo method.");
		}
		return collectionProductInfoVO;
	}

	/**
	 * Gets the Collection url.
	 *
	 * @param pCollectionId - the collection id
	 * 
	 * @return collectionUrl
	 * 
	 */
	public String getCollectionUrl(String pCollectionId) {
		if (isLoggingDebug()) {
			vlogDebug("START::: TRURecentlyViewedHistoryCollectorDroplet.getCollectionUrl method");
		}

		StringBuffer collectionUrl = new StringBuffer();

		if(StringUtils.isNotEmpty(pCollectionId)) {
			collectionUrl.append(getStoreConfig().getCollectionUrl());
			collectionUrl.append(pCollectionId);
		}

		if (isLoggingDebug()) {
			vlogDebug("END::: TRURecentlyViewedHistoryCollectorDroplet.getCollectionUrl method, COllection url - {0}", collectionUrl);
		}

		return collectionUrl.toString();
	}


	/**
	 * Gets the viewed item detail.
	 *
	 * @param pProductInfo the product info
	 * @return the viewed item detail
	 * @throws RepositoryException the repository exception
	 */
	public String getViewedItemDetail(ProductInfoVO pProductInfo) throws RepositoryException {

		final JSONObject json = new JSONObject();
		if(pProductInfo != null && pProductInfo.getDefaultSKU()!=null) {
			String displayName = pProductInfo.getDefaultSKU().getDisplayName();
			if(displayName !=null){
				displayName = displayName.replace(TRUConstants.SINGLE_QUOTE, TRUCommerceConstants.DOLLAR_WITH_STAR);
			}

			try {
				json.put(TRUConstants.PRODUCT_ID, pProductInfo.getProductId());
				json.put(TRUCommerceConstants.ONLINE_PID, pProductInfo.getDefaultSKU().getOnlinePID());
				json.put(TRUCommerceConstants.DISPLAY_NAME, displayName);
				json.put(TRUCommerceConstants.REVIEW_RATING, pProductInfo.getDefaultSKU().getReviewRating());
				json.put(TRUCommerceConstants.PRIMARY_IMAGE, pProductInfo.getDefaultSKU().getPrimaryImage());
				json.put(TRUCommerceConstants.PRODUCT_TYPE,TRUConstants.PDP);


				String maxAge=pProductInfo.getDefaultSKU().getMfrSuggestedAgeMax();
				String minAge=pProductInfo.getDefaultSKU().getMfrSuggestedAgeMin();
				String mfgAgeMessage = getCatalogTools().getMinAndMaxMFGAgeMessage(minAge, maxAge);

				json.put(TRUCommerceConstants.SUGGESTED_AGE_MIN, mfgAgeMessage);
				setProductPrice(pProductInfo,json);
				json.put(TRUCommerceConstants.PDP_URL, getProductPageUtil().getProductPageURL(pProductInfo.getDefaultSKU().getOnlinePID()));
				if( pProductInfo.getDefaultSKU().isPresellable()){
					json.put(TRUCommerceConstants.PRESELLABLE, pProductInfo.getDefaultSKU().isPresellable());
				}
				setOneRankPromotion(pProductInfo.getProductId(), json,pProductInfo.getDefaultSKU().getId());
			} catch (JSONException jsonException) {
				if (isLoggingError()) {
					logError("jsonException in TRURecentlyViewedHistoryCollectorDroplet.getViewedItemDetail method..",
							jsonException);
				}
			}
		}
		return json.toString();
	}

	/**
	 * Gets the Collection viewed item detail.
	 *
	 * @param pCollectionProductInfo the product info
	 * @return the viewed item detail
	 * @throws RepositoryException the repository exception
	 */
	public String getCollectionViewedItemDetail(CollectionProductInfoVO pCollectionProductInfo) throws RepositoryException {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN::: TRURecentlyViewedHistoryCollectorDroplet.getCollectionViewedItemDetail method");
		}
		final JSONObject json = new JSONObject();
		if(pCollectionProductInfo != null) {
			String displayName = pCollectionProductInfo.getCollectionName();

			if(displayName !=null){
				displayName = displayName.replace(TRUConstants.SINGLE_QUOTE, TRUCommerceConstants.DOLLAR_WITH_STAR);
			}
			String collectionId = pCollectionProductInfo.getCollectionId();

			try {
				json.put(TRUConstants.COLLECTION_ID, pCollectionProductInfo.getCollectionId());
				json.put(TRUCommerceConstants.DISPLAY_NAME, displayName);
				
				
				if (StringUtils.isNotBlank(pCollectionProductInfo.getCollectionImage()))
				{
					
					json.put(TRUCommerceConstants.PRIMARY_IMAGE, pCollectionProductInfo.getCollectionImage());
				}
				
				else
				{
					json.put(TRUCommerceConstants.PRIMARY_IMAGE, getStoreConfig().getNoImageUrl());
				}
			
				String price = setCollectionPrice(pCollectionProductInfo, json);

				json.put(TRUCommerceConstants.COLLECTION_PRICE, price);
				String collectionUrl = getCollectionUrl(collectionId);
				json.put(TRUCommerceConstants.COLLECTION_URL, collectionUrl);
				json.put(TRUCommerceConstants.PRODUCT_TYPE, TRUConstants.COLLECTION);
			} catch (JSONException jsonException) {
				if (isLoggingError()) {
					logError("jsonException in TRURecentlyViewedHistoryCollectorDroplet.getViewedItemDetail method..",
							jsonException);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRURecentlyViewedHistoryCollectorDroplet.getCollectionViewedItemDetail method");
		}
		return json.toString();
	}

	/**
	 * This method will return pricingTools.
	 *
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * This method will set mpricingTools with pPricingTools.
	 *
	 * @param pPricingTools the mpricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}



	/**
	 * Gets the product page url.
	 *
	 * @return the productPageUrl
	 */
	public String getProductPageUrl() {
		return mProductPageUrl;
	}


	/**
	 * Sets the product page url.
	 *
	 * @param pProductPageUrl the productPageUrl to set
	 */
	public void setProductPageUrl(String pProductPageUrl) {
		mProductPageUrl = pProductPageUrl;
	}

	/**
	 * Gets the siteMaxAllowedRecentlyViewedItems.
	 *
	 * @return the siteMaxAllowedRecentlyViewedItems
	 */
	public String getSiteMaxAllowedRecentlyViewedItems() {
		return mSiteMaxAllowedRecentlyViewedItems;
	}

	/**
	 * Sets the siteMaxAllowedRecentlyViewedItems.
	 *
	 * @param pSiteMaxAllowedRecentlyViewedItems the siteMaxAllowedRecentlyViewedItems to set
	 */
	public void setSiteMaxAllowedRecentlyViewedItems(String pSiteMaxAllowedRecentlyViewedItems) {
		mSiteMaxAllowedRecentlyViewedItems = pSiteMaxAllowedRecentlyViewedItems;
	}

	/**
	 * Gets the propertyManager.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the propertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * Gets the product page util.
	 *
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}


	/**
	 * Sets the product page util.
	 *
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}

	/**
	 * Gets the saved amount.
	 *
	 * @return the savedAmount
	 */
	public String getSavedAmount() {
		return mSavedAmount;
	}


	/**
	 * Sets the saved amount.
	 *
	 * @param pSavedAmount the savedAmount to set
	 */
	public void setSavedAmount(String pSavedAmount) {
		mSavedAmount = pSavedAmount;
	}


	/**
	 * Gets the saved percentage.
	 *
	 * @return the savedPercentage
	 */
	public String getSavedPercentage() {
		return mSavedPercentage;
	}


	/**
	 * Sets the saved percentage.
	 *
	 * @param pSavedPercentage the savedPercentage to set
	 */
	public void setSavedPercentage(String pSavedPercentage) {
		mSavedPercentage = pSavedPercentage;
	}


	/**
	 * Gets the list price.
	 *
	 * @return the listPrice
	 */
	public String getListPrice() {
		return mListPrice;
	}


	/**
	 * Sets the list price.
	 *
	 * @param pListPrice the listPrice to set
	 */
	public void setListPrice(String pListPrice) {
		mListPrice = pListPrice;
	}


	/**
	 * Gets the sale price.
	 *
	 * @return the salePrice
	 */
	public String getSalePrice() {
		return mSalePrice;
	}

	/**
	 * Sets the sale price.
	 *
	 * @param pSalePrice the salePrice to set
	 */
	public void setSalePrice(String pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * Gets the product cache.
	 *
	 * @return the mProductCache
	 */
	public TRUProductCache getProductCache() {
		return mProductCache;
	}

	/**
	 * Sets the product cache.
	 *
	 * @param pProductCache the ProductCache to set
	 */
	public void setProductCache(TRUProductCache pProductCache) {
		this.mProductCache = pProductCache;
	}

	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}


	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Returns the truConfiguration.
	 *
	 * @return the truConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets/updates the truConfiguration.
	 *
	 * @param pTruConfiguration            the truConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}

	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * Gets the mRankOnePromotion.
	 *
	 * @return the mRankOnePromotion
	 */
	public RepositoryItem getRankOnePromotion() {
		return mRankOnePromotion;
	}

	/**
	 * Sets the mRankOnePromotion.
	 *
	 * @param pRepositoryItem the mRankOnePromotion to set
	 */
	public void setRankOnePromotion(RepositoryItem pRepositoryItem) {
		this.mRankOnePromotion = pRepositoryItem;
	}

	/**
	 * Gets the promotion count.
	 *
	 * @return the mPromotionCount
	 */
	public int getPromotionCount() {
		return mPromotionCount;
	}

	/**
	 * Sets the promotion count.
	 *
	 * @param pI the mPromotionCount to set
	 */
	public void setPromotionCount(int pI) {
		this.mPromotionCount = pI;
	}

	/**
	 * Gets the promotion.
	 *
	 * @return the mPromotions
	 */
	public List<RepositoryItem> getPromotions() {
		return mPromotions;
	}

	/**
	 * Sets the promotion.
	 *
	 * @param pPromoList the mPromotions to set
	 */
	public void setPromotions(List<RepositoryItem> pPromoList) {
		this.mPromotions = pPromoList;
	}

	/**
	 * Gets the collection product cache.
	 *
	 * @return the collectionProductCache
	 */
	public TRUCollectionProductCache getCollectionProductCache() {
		return mCollectionProductCache;
	}

	/**
	 * Sets the collection product cache.
	 *
	 * @param pCollectionProductCache the collectionProductCache to set
	 */
	public void setCollectionProductCache(TRUCollectionProductCache pCollectionProductCache) {
		mCollectionProductCache = pCollectionProductCache;
	}

	/**
	 * Property to hold the StoreConfig.
	 */
	private TRUStoreConfiguration mStoreConfig;

	/**
	 * Gets the store config.
	 * 
	 * @return the storeConfig
	 */
	public TRUStoreConfiguration getStoreConfig() {
		return mStoreConfig;
	}

	/**
	 * Sets the store config.
	 * 
	 * @param pStoreConfig
	 *            the storeConfig to set
	 */
	public void setStoreConfig(TRUStoreConfiguration pStoreConfig) {
		mStoreConfig = pStoreConfig;
	}


}
