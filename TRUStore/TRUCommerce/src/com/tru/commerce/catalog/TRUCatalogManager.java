package com.tru.commerce.catalog;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.ColorInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;


/**
 * This class used to access catalog related information. This will invoke the catalog tools to fetch repository related information.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCatalogManager extends GenericService {
	
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This Method is used for get the All Root Categories from catalog.
	 * 
	 * @param pCatalogId - Select catalog Id
	 * @return allRootCategoriesList - All available root categories for the catalog
	 * @throws RepositoryException - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getAllClassifications(String pCatalogId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.getAllClassifications(pCatalogId : {0})", pCatalogId);
		}
		List<RepositoryItem> allRootCategoriesList = null;
		List<RepositoryItem> allClassificationList=null;
		if (!StringUtils.isBlank(pCatalogId)) {
			RepositoryItem catalogItem = getCatalogTools().findCatalogItem(pCatalogId);
			if (catalogItem != null) {
					allRootCategoriesList = getCatalogTools().getCatalogsAllRootCategories(catalogItem);
				if(allRootCategoriesList!=null){
					allClassificationList= getCatalogTools().getAllClassifications(allRootCategoriesList);
					}
				}
		   }
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogManager.getAllClassifications(pCatalogId)");
		}
		return allClassificationList;
	}
	
	/**
	 * This Method is used for get the All parent classification items.
	 * 
	 * @param pClassificationId - Select classification Id
	 * @return allParentClassificationItem - All available parent classifications item
	 * @throws RepositoryException - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getParentClassifications(String pClassificationId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.getParentClassifications()", pClassificationId);
		}
		List<RepositoryItem> allParentClassificationItem = null;
		if (!StringUtils.isBlank(pClassificationId)) {
			allParentClassificationItem = getCatalogTools().findParentClassificationItem(pClassificationId);
				}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogManager.getParentClassifications()");
		}
		return allParentClassificationItem;
	}
	
	
	/**
	 * This Method is used for get the All child classification items.
	 * 
	 * @param pClassificationId - Select classification Id
	 * @return allChildClassificationItem - All available child classifications item
	 * @throws RepositoryException - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getChildClassifications(String pClassificationId) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.getChildClassifications()", pClassificationId);
		}
		List<RepositoryItem> allChildClassificationItem = null;
		if (!StringUtils.isBlank(pClassificationId)) {
			allChildClassificationItem = getCatalogTools().findChildClassificationItem(pClassificationId);
				}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCatalogManager.getChildClassifications(pclassificationId)");
		}
		return allChildClassificationItem;
	}
	
	
	/**
	 * This method used to create product related info object which will be used to render the data in PDP pages.
	 *
	 * @param pProductId - Product id
	 * @param pChannelType ChannelType
	 * @param pCatalogRefIds CatalogRefIds
	 * @param pProductType ProductType
	 * @param pCollectionProductInfoVO CollectionProductInfoVO
	 * @return ProductInfoVO - product info object
	 * @throws RepositoryException - If any RepositoryException occurs
	 */
	public ProductInfoVO createProductInfo(String pProductId, boolean pChannelType, List<String> pCatalogRefIds,String pProductType,CollectionProductInfoVO pCollectionProductInfoVO) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.createProductInfo.method..", pProductId);
		}
		if (StringUtils.isBlank(pProductId)) {
			return null;
		}
		RepositoryItem productItem = getCatalogTools().findProduct(pProductId);
		ProductInfoVO productInfo = null;
		if (productItem != null) {
			productInfo = getCatalogTools().createProductInfo(productItem, pChannelType, pCatalogRefIds,pProductType,pCollectionProductInfoVO);
			/*if(productInfo !=null){
				productInfo.setRelatedProducts(getCatalogTools().createRelatedProductInfos(productItem));
		}*/
		}
		return productInfo;
	}
	
	
	/**
	 * This method used to create product related info object which will be used to render the data in PDP pages.
	 *
	 * @param pCollectionId - Collection id
	 * @param pChannelType - ChannelType
	 * @return CollectionProductInfoVO - collection product info object
	 * @throws RepositoryException - If any RepositoryException occurs
	 */
	public CollectionProductInfoVO createCollectionProductInfo(String pCollectionId, boolean pChannelType) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.createCollectionProductInfo.method..", pCollectionId);
		}
		if (StringUtils.isBlank(pCollectionId)) {
			return null;
		}
			CollectionProductInfoVO collectionProductInfoVO=new CollectionProductInfoVO();
			List<RepositoryItem> collectionProductItem = getCatalogTools().findCollectionProduct(pCollectionId, collectionProductInfoVO, pChannelType);
			CollectionProductInfoVO collectionProductInfo = null;
			if (collectionProductItem != null && collectionProductInfoVO.getDisplayStatus()!=null && TRUCommerceConstants.DISPLAY_STATUS_ACTIVE.equalsIgnoreCase(collectionProductInfoVO.getDisplayStatus())) {
				collectionProductInfo = getCatalogTools().createCollectionProductInfo(collectionProductItem,collectionProductInfoVO);
			}
			if (isLoggingDebug()) {
				vlogDebug("END:: TRUCatalogManager.createCollectionProductInfo.method..", pCollectionId);
			}
			return collectionProductInfo;
	}
	
	/**
	 * This method used to fetch selected color related information based on color id.
	 *
	 * @param pColorSizes - Available color sizes
	 * @param pSelectedColorId - Selected color id to fetch the color info
	 * @return ColorInfoVO - Return matched color item if found otherwise empty
	 */
	public ColorInfoVO getSelectedColor(Map<ColorInfoVO, List<SKUInfoVO>> pColorSizes, String pSelectedColorId) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCatalogManager.getSelectedColor method..  {0}", pSelectedColorId);
		}
		ColorInfoVO selectedColor = null;
		if (pColorSizes != null && !pColorSizes.isEmpty() && !StringUtils.isEmpty(pSelectedColorId)) {
			for (ColorInfoVO uniqueColor : pColorSizes.keySet()) {
				if (pSelectedColorId.equals(uniqueColor.getId())) {
					selectedColor = uniqueColor;
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCatalogManager.getSelectedColor method.. {0}", selectedColor);
		}
		return selectedColor;
	}
	
	/**
	 * This method used to populate category VO object from category item.
	 * 
	 * @param pCategoryItem - Category item to fetch the repository data
	 * @return boolean - Return true if valid category otherwise false
	 */
	public boolean isValidCategory(RepositoryItem pCategoryItem) {
		if (isLoggingDebug()) {
			logDebug("TRUCatalogManager.isValidCategorymethod..");
		}
		return getCatalogTools().isValidCategory(pCategoryItem);
	}
	
	
}
