package com.tru.commerce;

import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.ExtendableCreditCardTools;

import com.tru.commerce.constants.TRUCheckoutConstants;

/**
 * This class is TRUExtendableCreditCardTools.
 *
 * @author PA
 * @version 1.0
 */
public class TRUExtendableCreditCardTools extends ExtendableCreditCardTools {
	
	
	/**
	 * Checks if is number.
	 *
	 * @param pNumber number
	 * @return boolean
	 */
	 protected boolean isNumber(String pNumber)
	  {
	    if (pNumber == null) {
	      return false;

	    }

	    for (int k = TRUCheckoutConstants.INT_ZERO; (pNumber != null) && (k < pNumber.length()); ++k)
	    {
	      final char c = pNumber.charAt(k);
			switch (c) {
			case TRUCheckoutConstants.CHAR0:
			case TRUCheckoutConstants.CHAR1:
			case TRUCheckoutConstants.CHAR2:
			case TRUCheckoutConstants.CHAR3:
			case TRUCheckoutConstants.CHAR4:
			case TRUCheckoutConstants.CHAR5:
			case TRUCheckoutConstants.CHAR6:
			case TRUCheckoutConstants.CHAR7:
			case TRUCheckoutConstants.CHAR8:
			case TRUCheckoutConstants.CHAR9:
				break;
			default:
				return false;
			}
	    }

	    return true;
	  }
		
		/**
		 * Verify credit card.
		 *
		 * @param pCreditCard creditCard
		 * @param pNumber number
		 * @param pType type
		 * @return integer
		 */
	 public int verifyCreditCard(CreditCardInfo pCreditCard, String pNumber, String pType)
	  {
	    return 0;
	  }

}
