
package com.tru.messaging;

import java.io.Serializable;
import java.util.List;

/**
 * Message TRUBudgetEmailMessage.
 * @author PA
 * @version 1.0
 */
public class TRUBudgetEmailMessage implements Serializable{
	
	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * mCustomerEmail.
	 */
	private List<String> mPromotionIds;

	/**
	 * @return the promotionIds
	 */
	public List<String> getPromotionIds() {
		return mPromotionIds;
	}

	/**
	 * @param pPromotionIds the promotionIds to set
	 */
	public void setPromotionIds(List<String> pPromotionIds) {
		mPromotionIds = pPromotionIds;
	}
	

}
