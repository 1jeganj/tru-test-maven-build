package com.tru.messaging;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

/**
 * TRUBudgetPromoDisableMessageSource send order to check with the tender based promotion with rest call from qux instance 
 * and if it reaches the threshold limit it will create project to disable the promotion with auto deploy.
 *
 * @author PA
 * @version 1.0
 */
public class TRUBudgetPromoDisableMessageSource extends GenericService implements
		MessageSource {

	/**
	 * mPortName.
	 */
	private String mPortName;

	/**
	 * Started.
	 */
	private boolean mStarted;

	/**
	 * mMessageSourceContext.
	 */
	private MessageSourceContext mMessageSourceContext;

	/**
	 * This method is to send the master import message.
	 * @param pDisablePromotionList 
	 *
	 */
	public void sendBudgetDisablePromoMessage(List<String> pDisablePromotionList){
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUBudgetPromoDisableMessageSource method : sendBudgetDisablePromoMessage");
		}
		if(pDisablePromotionList == null || pDisablePromotionList.isEmpty()){
			return;
		}
		ObjectMessage objectMessage = null;
		TRUBudgetDisablePromoMessage disableMessage = new TRUBudgetDisablePromoMessage();
		//Create object message
		try {
			objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
			disableMessage.setPromotionIds(pDisablePromotionList);
			//Set Object message
			objectMessage.setObject(disableMessage);
			if (isLoggingDebug()) {
				logDebug("objectMessage in sendBudgetDisablePromoMessage() Method...." + objectMessage);
			}
			//Send Customer promotion details
			getMessageSourceContext().sendMessage(getPortName(), objectMessage);
		} catch (JMSException e) {
			if(isLoggingError()){
				vlogError("JMSException: While creating object message and execption details are : {0}", e);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUBudgetPromoDisableMessageSource method : sendBudgetDisablePromoMessage");
		}
	}

	/**
	 * MessageSourceContext.
	 * @param pMessageSourceContext - MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pMessageSourceContext) {
		mMessageSourceContext = pMessageSourceContext;
	}

	/**
	 * startMessageSource.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUBudgetEmailMessageSource method : startMessageSource");
		}
		mStarted = true;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUBudgetEmailMessageSource method : startMessageSource");
		}
	}

	/**
	 * stopMessageSource.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUBudgetEmailMessageSource method : stopMessageSource");
		}
		mStarted = false;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUBudgetEmailMessageSource method : stopMessageSource");
		}
	}

	/**
	 * This method returns the started value.
	 *
	 * @return the started
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method sets the started with parameter value pStarted.
	 *
	 * @param pStarted the started to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * This method returns the context value.
	 *
	 * @return the context
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mMessageSourceContext;
	}

	/**
	 * This method returns the portName value.
	 *
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * This method sets the portName with parameter value pPortName.
	 *
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

}