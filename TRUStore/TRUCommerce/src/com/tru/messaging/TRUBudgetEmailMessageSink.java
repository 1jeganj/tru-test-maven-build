package com.tru.messaging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.commerce.promotion.TRUPromotionTools;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

/**
 *This Sink is to receive the message to send promotion disable email.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBudgetEmailMessageSink extends GenericService implements
		MessageSink {
	
	/**
//	 * This holds the promotionTools
	 */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * This is to receive the message and processes to disable the promotion.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUBudgetEmailMessageSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUBudgetEmailMessage budgetEmailMessage =  null;
		List<RepositoryItem> tenderItems = null;
		if(objMessage != null && objMessage.getObject() instanceof TRUBudgetEmailMessage){
			budgetEmailMessage =  (TRUBudgetEmailMessage) objMessage.getObject();
			List<String> promotionIds = budgetEmailMessage.getPromotionIds();
			if (promotionIds != null && !promotionIds.isEmpty()) {
				tenderItems = new ArrayList<RepositoryItem>();
				for(String promtoId: promotionIds){
					RepositoryItem[] tenderItem=getPromotionTools().getTenderTypeItem(promtoId);
					if(null!=tenderItem){
						tenderItems.addAll(Arrays.asList(tenderItem));
					}
				}
				if(null!=tenderItems){
					getPromotionTools().sendTenderPromotionDetailsAsEmail(tenderItems,null,Boolean.TRUE);
					getPromotionTools().removeTenderTypeItem(tenderItems);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUBudgetEmailMessageSink method : receiveMessage");
		}
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

}
