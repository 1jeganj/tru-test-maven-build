package com.tru.messaging;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.commerce.promotion.TRUPromotionTools;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

/**
 *This Sink is to receive the message to disable the promotion.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBudgetDisablePromoMessageSink extends GenericService implements
		MessageSink {
	
	/**
//	 * This holds the promotionTools
	 */
	private TRUPromotionTools mPromotionTools;
	
	/**
	 * This is to receive the message and processes to disable the promotion.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUBudgetEmailMessageSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUBudgetDisablePromoMessage budgetDisablePromoMessage =  null;
		if(objMessage != null && objMessage.getObject() instanceof TRUBudgetDisablePromoMessage){
			budgetDisablePromoMessage =  (TRUBudgetDisablePromoMessage) objMessage.getObject();
			List<String> promotionIds = budgetDisablePromoMessage.getPromotionIds();
			if (promotionIds != null && !promotionIds.isEmpty()) {
				// Making rest call to disable the promotion.
				getPromotionTools().restDisablePromotion(promotionIds);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUBudgetEmailMessageSink method : receiveMessage");
		}
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

}
