package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;
import atg.userprofiling.Profile;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.userprofiling.TRUPropertyManager;

/**
 *This class is a source to send the customer order details to Sink.
 * @author PA
 * @version 1.0
 */
public class TRUCustomerOrderUpdateSource extends GenericService implements
		MessageSource {
	
	/**
	 * mPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * mPortName.
	 */
	private String mPortName;
	
	/**
	 * Started.
	 */
	private boolean mStarted;
	
	/**
	 * mMessageSourceContext.
	 */
	private MessageSourceContext mMessageSourceContext;
	
	/**
	 * @param pProfile - Profile
	 * @param pOrder - TRUOrderImpl
	 */
	public void sendCustomerOrderUpdate(Profile pProfile, TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateSource method : sendCustomerOrderUpdate");
		}
		ObjectMessage objectMessage = null;
		if (pProfile != null && pOrder != null && getMessageSourceContext() != null) {
			try {
				TRUCustomerOrderUpdateMessage customerOrderUpdateMessage = new TRUCustomerOrderUpdateMessage();
				TRUPropertyManager propertyManager = getPropertyManager();
				customerOrderUpdateMessage.setCustomerEmail((String) pProfile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerOrderUpdateMessage.setExternalCustomerId(pProfile.getRepositoryId());
				customerOrderUpdateMessage.setExternalOrderNumber(pOrder.getId());
				//Create object message
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				//Set Object message
				objectMessage.setObject(customerOrderUpdateMessage);
				if (isLoggingDebug()) {
					logDebug("objectMessage in sendNewAccountEmail() Method...." + objectMessage);
				}
				//Send Customer Master Import Message
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
			} catch (JMSException exc) {
				if(isLoggingError()){
					vlogError(exc, "JMSException in method sendCustomerOrderUpdate");
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateSource method : sendCustomerMasterImport");
		}
	}

	/** 
	 * MessageSourceContext.
	 * @param pMessageSourceContext - MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pMessageSourceContext) {
		mMessageSourceContext = pMessageSourceContext;
	}

	/**
	 * startMessageSource.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateSource method : startMessageSource");
		}
		mStarted = true;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderUpdateSource method : startMessageSource");
		}
	}

	/**
	 * stopMessageSource.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateSource method : stopMessageSource");
		}
		mStarted = false;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderUpdateSource method : stopMessageSource");
		}
	}

	/**
	 * This method returns the started value.
	 *
	 * @return the started
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method sets the started with parameter value pStarted.
	 *
	 * @param pStarted the started to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * This method returns the context value.
	 *
	 * @return the context
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mMessageSourceContext;
	}

	/**
	 * This method returns the portName value.
	 *
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * This method sets the portName with parameter value pPortName.
	 *
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

}
