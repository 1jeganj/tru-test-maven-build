package com.tru.messaging.oms;

import java.util.Calendar;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.userprofiling.Profile;

import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * TRUCustomerMasterImportSource.
 *
 * @author PA
 * @version 1.0
 */
public class TRUCustomerMasterImportSource extends GenericService implements
		MessageSource {
	
	/**
	 * mPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * mPortName.
	 */
	private String mPortName;
	
	/**
	 * Started.
	 */
	private boolean mStarted;
	
	/**
	 * Holds reference for mTransactionManager.
	 */
	private TransactionManager mTransactionManager;
	
	/**
	 * mMessageSourceContext.
	 */
	private MessageSourceContext mMessageSourceContext;
	
	/**
	 * mOmsErrorRepository.
	 */
	private GSARepository mOmsErrorRepository;
	
	/**
	 * Holds mTransactionToRollback.
	 */
	private boolean mTransactionToRollback;
	
	/**
	 * This method is to send the master import message.
	 * @param pProfile - Profile
	 * @param pOrderId - OrderId
	 */
	public void sendCustomerMasterImport(Profile pProfile, String pOrderId){
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : sendCustomerMasterImport");
		}
		ObjectMessage objectMessage = null;
		if (pProfile != null && getMessageSourceContext() != null) {
			try {
				TRUCustomerMasterImportMessage customerMasterImportMessage = new TRUCustomerMasterImportMessage();
				TRUPropertyManager propertyManager = getPropertyManager();
				customerMasterImportMessage.setCustomerFirstName((String) pProfile.getPropertyValue(propertyManager.getFirstNamePropertyName()));
				customerMasterImportMessage.setCustomerLastName((String) pProfile.getPropertyValue(propertyManager.getLastNamePropertyName()));
				customerMasterImportMessage.setCustomerEmail((String) pProfile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerMasterImportMessage.setExternalCustomerId(pProfile.getRepositoryId());
				customerMasterImportMessage.setOrderId(pOrderId);
				String rewardNumber = (String) pProfile.getPropertyValue(propertyManager.getRewardNumberPropertyName());
				if(!StringUtils.isBlank(rewardNumber)){
					customerMasterImportMessage.setReferenceField10( (String) pProfile.getPropertyValue(propertyManager.getRewardNumberPropertyName()));
				}
				//Create object message
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				//Set Object message
				objectMessage.setObject(customerMasterImportMessage);
				if (isLoggingDebug()) {
					logDebug("objectMessage in sendNewAccountEmail() Method...." + objectMessage);
				}
				//Send Customer Master Import Message
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
			} catch (JMSException exc) {
				updateOmsErrorItemForProfile(pProfile);
				if(isLoggingError()){
					vlogError(exc, "JMSException in method sendCustomerMasterImport");
				}
			}
		}
		if (pProfile != null && getMessageSourceContext() == null) {
			updateOmsErrorItemForProfile(pProfile);
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSource method : sendCustomerMasterImport");
		}
	}

	/**
	 * Update oms error item for profile.
	 *
	 * @param pProfile the profile
	 */
	public void updateOmsErrorItemForProfile(Profile pProfile) {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : updateOmsErrorItemForProfile");
		}
		MutableRepositoryItem omsErrorItem = null;
		MutableRepository omsRepository = getOmsErrorRepository();
		TransactionManager transactionManager  =  getTransactionManager();
		TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		Boolean commitTransaction = Boolean.TRUE;
		if (omsRepository != null) {
			try {
				if (transactionManager != null) {
					if(isLoggingDebug()){
						vlogDebug("Transaction status : {0}", transactionManager.getTransaction().getStatus());
					}
					if(isTransactionToRollback()){
						int status = transactionManager.getStatus();
						if(status == TRUConstants.ONE){
							transactionManager.getTransaction().rollback();
						}
					}
				transactionDemarcation.begin(transactionManager, TransactionDemarcation.REQUIRED);
				omsErrorItem = omsRepository.createItem(getPropertyManager().getOmsErrorMessageItemName());
				omsErrorItem.setPropertyValue(getPropertyManager().getTypePropertyName(), TRUConstants.CUSTOMER_MASTER_IMPORT);
				omsErrorItem.setPropertyValue(getPropertyManager().getMessagePropertyName(), pProfile.getRepositoryId());
				Calendar cal = Calendar.getInstance();
				omsErrorItem.setPropertyValue(getPropertyManager().getTransactionTimePropertyName(), cal.getTime());
				omsRepository.addItem(omsErrorItem);
				}
			} catch (RepositoryException re) {
				commitTransaction = Boolean.FALSE;
				if(isLoggingError()){
					vlogError(re, "RepositoryException in method updateOmsErrorItemForProfile");
				}
			} catch (TransactionDemarcationException e) {
				commitTransaction = Boolean.FALSE;
				if (isLoggingError()) {
					vlogError(
							"TransactionDemarcationException in TRUCustomerMasterImportSource method : updateOmsErrorItemForProfile ProfileId:{0} TransactionDemarcationException:{1}",pProfile.getRepositoryId(), e);
				}
			} catch (SystemException sysExc) {
				if(isLoggingError()){
					vlogError("SystemException : While getting the status of the transaction  : {0}",  sysExc);
				}
			} finally {
				try {
					if (isLoggingInfo() && !commitTransaction && transactionDemarcation != null) {
						vlogInfo("Transaction to be roll back in auditMessage");
					}
					if (transactionDemarcation != null) {
						transactionDemarcation.end(!commitTransaction);
					}
				} catch (TransactionDemarcationException demarcationException) {
					if (isLoggingError()) {
						vlogError("Transaction demarcation Exception TRUCustomerMasterImportSource method : updateOmsErrorItemForProfile ProfileId:{0} tranDmrExp:{1}",pProfile.getRepositoryId(), demarcationException);
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : updateOmsErrorItemForProfile");
		}
	}
	
	/** 
	 * MessageSourceContext.
	 * @param pMessageSourceContext - MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pMessageSourceContext) {
		mMessageSourceContext = pMessageSourceContext;
	}

	/**
	 * startMessageSource.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : startMessageSource");
		}
		mStarted = true;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSource method : startMessageSource");
		}
	}

	/**
	 * stopMessageSource.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : stopMessageSource");
		}
		mStarted = false;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSource method : stopMessageSource");
		}
	}

	/**
	 * This method returns the started value.
	 *
	 * @return the started
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method sets the started with parameter value pStarted.
	 *
	 * @param pStarted the started to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * This method returns the context value.
	 *
	 * @return the context
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mMessageSourceContext;
	}

	/**
	 * This method returns the portName value.
	 *
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * This method sets the portName with parameter value pPortName.
	 *
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the oms error repository.
	 *
	 * @return the oms error repository
	 */
	public GSARepository getOmsErrorRepository() {
		return mOmsErrorRepository;
	}

	/**
	 * Sets the oms error repository.
	 *
	 * @param pOmsErrorRepository the new oms error repository
	 */
	public void setOmsErrorRepository(GSARepository pOmsErrorRepository) {
		mOmsErrorRepository = pOmsErrorRepository;
	}
	
	/**
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	
	/**
	 * This method returns the transactionToRollback value.
	 *
	 * @return the transactionToRollback
	 */
	public boolean isTransactionToRollback() {
		return mTransactionToRollback;
	}

	/**
	 * This method sets the transactionToRollback with parameter value pTransactionToRollback.
	 *
	 * @param pTransactionToRollback the transactionToRollback to set
	 */
	public void setTransactionToRollback(boolean pTransactionToRollback) {
		mTransactionToRollback = pTransactionToRollback;
	}
}
