
package com.tru.messaging.oms;

import java.io.Serializable;

/**
 * Message TRUCustomerOrderUpdateMessage.
 * @author PA
 * @version 1.0
 */
public class TRUCustomerOrderUpdateMessage implements Serializable{
	
	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * mCustomerEmail.
	 */
	private String mExternalCustomerId;
	
	/**
	 * mCustomerEmail.
	 */
	private String mCustomerEmail;
	
	/**
	 * mExternalOrderNumber.
	 */
	private String mExternalOrderNumber;

	/**
	 * This method returns the externalCustomerId value.
	 *
	 * @return the externalCustomerId
	 */
	public String getExternalCustomerId() {
		return mExternalCustomerId;
	}

	/**
	 * This method sets the externalCustomerId with parameter value pExternalCustomerId.
	 *
	 * @param pExternalCustomerId the externalCustomerId to set
	 */
	public void setExternalCustomerId(String pExternalCustomerId) {
		mExternalCustomerId = pExternalCustomerId;
	}

	/**
	 * This method returns the customerEmail value.
	 *
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return mCustomerEmail;
	}

	/**
	 * This method sets the customerEmail with parameter value pCustomerEmail.
	 *
	 * @param pCustomerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String pCustomerEmail) {
		mCustomerEmail = pCustomerEmail;
	}

	/**
	 * This method returns the externalOrderNumber value.
	 *
	 * @return the externalOrderNumber
	 */
	public String getExternalOrderNumber() {
		return mExternalOrderNumber;
	}

	/**
	 * This method sets the externalOrderNumber with parameter value pExternalOrderNumber.
	 *
	 * @param pExternalOrderNumber the externalOrderNumber to set
	 */
	public void setExternalOrderNumber(String pExternalOrderNumber) {
		mExternalOrderNumber = pExternalOrderNumber;
	}

}
