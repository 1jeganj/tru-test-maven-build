
package com.tru.messaging.oms;

import java.io.Serializable;

/**
 * Class TRUCustomerMasterImportMessage.
 * @author PA
 * @version 1.0
 */
public class TRUCustomerMasterImportMessage implements Serializable{
	
	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * mCustomerEmail.
	 */
	private String mExternalCustomerId;
	
	/**
	 * mCustomerEmail.
	 */
	private String mCustomerFirstName;
	
	/**
	 * mCustomerEmail.
	 */
	private String mCustomerLastName;
	
	/**
	 * mCustomerEmail.
	 */
	private String mCustomerEmail;
	
	/**
	 * mReferenceField10.
	 */
	private String mReferenceField10;
	/**
	 * mOrderId.
	 */
	private String mOrderId;

	/**
	 * This method returns the externalCustomerId value.
	 *
	 * @return the externalCustomerId
	 */
	public String getExternalCustomerId() {
		return mExternalCustomerId;
	}

	/**
	 * This method sets the externalCustomerId with parameter value pExternalCustomerId.
	 *
	 * @param pExternalCustomerId the externalCustomerId to set
	 */
	public void setExternalCustomerId(String pExternalCustomerId) {
		mExternalCustomerId = pExternalCustomerId;
	}

	/**
	 * This method returns the customerFirstName value.
	 *
	 * @return the customerFirstName
	 */
	public String getCustomerFirstName() {
		return mCustomerFirstName;
	}

	/**
	 * This method sets the customerFirstName with parameter value pCustomerFirstName.
	 *
	 * @param pCustomerFirstName the customerFirstName to set
	 */
	public void setCustomerFirstName(String pCustomerFirstName) {
		mCustomerFirstName = pCustomerFirstName;
	}

	/**
	 * This method returns the customerLastName value.
	 *
	 * @return the customerLastName
	 */
	public String getCustomerLastName() {
		return mCustomerLastName;
	}

	/**
	 * This method sets the customerLastName with parameter value pCustomerLastName.
	 *
	 * @param pCustomerLastName the customerLastName to set
	 */
	public void setCustomerLastName(String pCustomerLastName) {
		mCustomerLastName = pCustomerLastName;
	}

	/**
	 * This method returns the customerEmail value.
	 *
	 * @return the customerEmail
	 */
	public String getCustomerEmail() {
		return mCustomerEmail;
	}

	/**
	 * This method sets the customerEmail with parameter value pCustomerEmail.
	 *
	 * @param pCustomerEmail the customerEmail to set
	 */
	public void setCustomerEmail(String pCustomerEmail) {
		mCustomerEmail = pCustomerEmail;
	}

	/**
	 * This method returns the referenceField10 value.
	 *
	 * @return the referenceField10
	 */
	public String getReferenceField10() {
		return mReferenceField10;
	}

	/**
	 * This method sets the referenceField10 with parameter value pReferenceField10.
	 *
	 * @param pReferenceField10 the referenceField10 to set
	 */
	public void setReferenceField10(String pReferenceField10) {
		mReferenceField10 = pReferenceField10;
	}

	/**
	 * This method gets orderId value
	 *
	 * @return the orderId value
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * This method sets the orderId with pOrderId value
	 *
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

}
