package com.tru.shipping.registry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUChannelDetailsVo;
import com.tru.commerce.cart.vo.TRURegistrantDetailsVO;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.vo.TRUFailedRegistryDetails;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.Locale;
import com.tru.integrations.registry.beans.People;
import com.tru.integrations.registry.beans.Product;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryService;
import com.tru.integrations.registry.util.TRURegistryConstant;
/**
 * This class TRURegistryServiceProcessor .
 * @author Professional Access
 * @version 1.0
 */
public class TRURegistryServiceProcessor extends GenericService {
	
	/** The Constant PHONE_NO. */
	private static final String PHONE_NO = "8568533200";
	
	/** The Constant WOODBURY_ZIPCODE. */
	private static final String WOODBURY_ZIPCODE = "08096";
	
	/** The Constant CITY_WOODBURY. */
	private static final String CITY_WOODBURY = "Woodbury";
	/**
	 * property to Hold RegistryDetailsEnabled.
	 */
	private boolean mRegistryDetailsEnabled;
	/**
	 * property to Hold RegistryService.
	 */
	private RegistryService mRegistryService;
	/**
	 *  property to Hold shopping cart utils.
	 */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/** Property to hold mCurrentDate. */
  	private CurrentDate mCurrentDate;
  	
	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * @param pCurrentDate the currentDate to set
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * @return the registryDetailsEnabled
	 */
	public boolean isRegistryDetailsEnabled() {
		return mRegistryDetailsEnabled;
	}

	/**
	 * @param pRegistryDetailsEnabled the registryDetailsEnabled to set
	 */
	public void setRegistryDetailsEnabled(boolean pRegistryDetailsEnabled) {
		mRegistryDetailsEnabled = pRegistryDetailsEnabled;
	}

	/**
	 * @return the registryService
	 */
	public RegistryService getRegistryService() {
		return mRegistryService;
	}

	/**
	 * @param pRegistryService the registryService to set
	 */
	public void setRegistryService(RegistryService pRegistryService) {
		mRegistryService = pRegistryService;
	}
	
	/** holds TRUConfiguration instance. **/
	private TRUConfiguration mConfiguration;

	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
	
	/**
	 * @return the shoppingCartUtils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * @param pShoppingCartUtils the shoppingCartUtils to set
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		mShoppingCartUtils = pShoppingCartUtils;
	}
	
	/**
	 *  Holds the mStoreConfig.
	 */
	private TRUStoreConfiguration mStoreConfig;
	/**
	 * @return the storeConfig
	 */
	public TRUStoreConfiguration getStoreConfig() {
		return mStoreConfig;
	}
	/**
	 * @param pStoreConfig the storeConfig to set
	 */
	public void setStoreConfig(TRUStoreConfiguration pStoreConfig) {
		mStoreConfig = pStoreConfig;
	}

	/** The Eligible contact types. */
	private List<String> mEligibleContactTypes;
	
	/**
	 * @return the eligibleContactTypes
	 */
	public List<String> getEligibleContactTypes() {
		return mEligibleContactTypes;
	}
	/**
	 * @param pEligibleContactTypes the eligibleContactTypes to set
	 */
	public void setEligibleContactTypes(List<String> pEligibleContactTypes) {
		mEligibleContactTypes = pEligibleContactTypes;
	}

	/**
	 * Get registry details.
	 *
	 * @param pChannelDetailsVo the channel details vo
	 * @return true, if successful
	 */
	public boolean getRegistryDetails(TRUChannelDetailsVo pChannelDetailsVo){
		if (isLoggingDebug()) {
			vlogDebug("START: TRURegistryServiceProcessor.getRegistryDetails()");
		}
		
		if(isRegistryDetailsEnabled()){
			try {
				String registryType = getConfiguration().getChannelTypeMap().get(pChannelDetailsVo.getChannelType());
				if (isLoggingDebug()) {
					vlogDebug("TRURegistryServiceProcessor.getRegistryDetails() registryType: {0}, channelId: {1}", registryType, pChannelDetailsVo.getChannelId());
				}
				if (StringUtils.isNotBlank(pChannelDetailsVo.getChannelId()) && StringUtils.isNotBlank(registryType)) {
					RegistryResponse registryResponse = getRegistryService().getDetails(pChannelDetailsVo.getChannelId(), registryType);
					if (registryResponse != null) {
						constructRegistryGetDetailResponse(registryResponse , pChannelDetailsVo);
						if (isLoggingDebug()) {
							vlogDebug("END: TRURegistryServiceProcessor.getRegistryDetails() returns true");
						}
						return true;
					} else {
						if (isLoggingDebug()) {
							vlogDebug("END: TRURegistryServiceProcessor.getRegistryDetails() returns false");
						}
						return false;
					}
				}
			} catch (RegistryIntegrationException e) {
				if (isLoggingError()) {
					logError("RunProcessException in TRURegistryServiceProcessor.getRegistryDetails", e);
				}
				return false;
			}
		} else {
			constructRegistryDummyGetDetailResponse(pChannelDetailsVo);
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRURegistryServiceProcessor.getRegistryDetails()");
		}
		return true;
	}
	
	
	/**
	 * @param pRegistryResponse - RegistryResponse
	 * @param pChannelDetailsVo - TRUChannelDetailsVo
	 */
	private void constructRegistryGetDetailResponse(RegistryResponse pRegistryResponse,	TRUChannelDetailsVo pChannelDetailsVo) {
		if (isLoggingDebug()) {
			vlogDebug("START: TRURegistryServiceProcessor.constructRegistryGetDetailResponse()");
		}
		if (pRegistryResponse.getRegistryDetail() != null && pRegistryResponse.getRegistryDetail().getPeople() != null) {
			int count = pRegistryResponse.getRegistryDetail().getPeople().size();
			if (count > TRUConstants.ZERO) {
				List<TRURegistrantDetailsVO> registrantDetailsVOs = new ArrayList<TRURegistrantDetailsVO>(count);
				for(People people : pRegistryResponse.getRegistryDetail().getPeople()){
					String contactType = people.getContactType();
					if (people != null && StringUtils.isNotBlank(contactType) && getEligibleContactTypes().contains(contactType)) {
						TRURegistrantDetailsVO registrantDetailsVO = new TRURegistrantDetailsVO();
						registrantDetailsVO.setRegistrantId(String.valueOf(people.getPeopleId()));

						ContactInfo contactInfo = new ContactInfo();
						contactInfo.setFirstName(people.getFirstName());
						contactInfo.setLastName(people.getLastName());
						contactInfo.setAddress1(people.getAddressOne());
						contactInfo.setAddress2(people.getAddressTwo());
						contactInfo.setCity(people.getCity());
						contactInfo.setState(people.getState());
						contactInfo.setCountry(TRUConstants.UNITED_STATES);
						contactInfo.setPostalCode(people.getZip());
						contactInfo.setPhoneNumber(String.valueOf(people.getPhoneNumber()));
						contactInfo.setEmail(people.getEmail());

						registrantDetailsVO.setAddress(contactInfo);
						if (isLoggingDebug()) {
							vlogDebug("END: TRURegistryServiceProcessor.constructRegistryGetDetailResponse method ContactInfo:{0}",contactInfo);
						}
						registrantDetailsVOs.add(registrantDetailsVO);
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("END: TRURegistryServiceProcessor.constructRegistryGetDetailResponse method registryDetailsVos :{0}",registrantDetailsVOs);
				}
				pChannelDetailsVo.setRegistrantDetailsVOs(registrantDetailsVOs);
			}
		}
	}
	
	/**
	 * @param pChannelDetailsVo TRUChannelDetailsVo
	 */
	private void constructRegistryDummyGetDetailResponse(TRUChannelDetailsVo pChannelDetailsVo) {
		List<TRURegistrantDetailsVO> registrantDetailsVOs = new ArrayList<TRURegistrantDetailsVO>();
		if(TRUConstants.WISHLIST_CHANNEL_TYPE.equalsIgnoreCase(pChannelDetailsVo.getChannelType())){
			TRURegistrantDetailsVO registrantDetailsVO1 = new TRURegistrantDetailsVO();
			List<ContactInfo> registryAddresses = new ArrayList<ContactInfo>();
			ContactInfo registryAddress1 = new ContactInfo();
			registryAddress1.setFirstName(TRUConstants.WISHLIST_CHANNEL_TYPE);
			registryAddress1.setLastName(TRUConstants.ADDRESS1);
			registryAddress1.setAddress1(TRUConstants.ADDR1);
			registryAddress1.setAddress2(TRUConstants.ADDR2);
			registryAddress1.setCity(TRUConstants.CITY_NY);
			registryAddress1.setState(TRUConstants.STATE_NY);
			registryAddress1.setCountry(TRUConstants.UNITED_STATES);
			registryAddress1.setPostalCode(TRUConstants.ADDRESS_POSTAL_CODE);
			registryAddress1.setPhoneNumber(TRUConstants.ADDRESS_PHONE_NUMBER);
			registryAddresses.add(registryAddress1);
			registryAddress1.setEmail(TRUConstants.ADDRESS_EMAIL);
			
			registrantDetailsVO1.setAddress(registryAddress1);
			registrantDetailsVO1.setRegistrantId(TRUConstants.REGISTANT_ID1);
			registrantDetailsVOs.add(registrantDetailsVO1);

			TRURegistrantDetailsVO registrantDetailsVO2 = new TRURegistrantDetailsVO();
			ContactInfo registryAddress2 = new ContactInfo();
			registryAddress2.setFirstName(TRUConstants.WISHLIST_CHANNEL_TYPE);
			registryAddress2.setLastName(TRUConstants.ADDRESS_2);
			registryAddress2.setAddress1(TRUConstants.ADDR1);
			registryAddress2.setAddress2(TRUConstants.ADDR2);
			registryAddress2.setCity(CITY_WOODBURY);
			registryAddress2.setState(TRUConstants.STATE_NY);
			registryAddress2.setCountry(TRUConstants.UNITED_STATES);
			registryAddress2.setPostalCode(WOODBURY_ZIPCODE);
			registryAddress2.setPhoneNumber(PHONE_NO);
			registryAddress2.setEmail(TRUConstants.ADDRESS_EMAIL);
			registryAddresses.add(registryAddress2);

			registrantDetailsVO2.setAddress(registryAddress2);
			registrantDetailsVO2.setRegistrantId(TRUConstants.REGISTANT_ID2);
			//registrantDetailsVOs.add(registrantDetailsVO2);
		}else{
			TRURegistrantDetailsVO registrantDetailsVO1 = new TRURegistrantDetailsVO();
			List<ContactInfo> registryAddresses = new ArrayList<ContactInfo>();
			ContactInfo registryAddress1 = new ContactInfo();
			registryAddress1.setFirstName(TRUConstants.REGISTRY_CHANNEL_TYPE);
			registryAddress1.setLastName(TRUConstants.ADDRESS_1);
			registryAddress1.setAddress1(TRUConstants.ADDR1);
			registryAddress1.setAddress2(TRUConstants.ADDR2);
			registryAddress1.setCity(TRUConstants.CITY_NY);
			registryAddress1.setState(TRUConstants.STATE_NY);
			registryAddress1.setCountry(TRUConstants.UNITED_STATES);
			registryAddress1.setPostalCode(TRUConstants.ADDRESS_POSTAL_CODE);
			registryAddress1.setPhoneNumber(TRUConstants.ADDRESS_PHONE_NUMBER);
			registryAddress1.setEmail(TRUConstants.ADDRESS_EMAIL);
			registryAddresses.add(registryAddress1);
			
			
			registrantDetailsVO1.setAddress(registryAddress1);
			registrantDetailsVO1.setRegistrantId(TRUConstants.REGISTANT_ID1);
			registrantDetailsVOs.add(registrantDetailsVO1);

			TRURegistrantDetailsVO registrantDetailsVO2 = new TRURegistrantDetailsVO();
			ContactInfo registryAddress2 = new ContactInfo();
			registryAddress2.setFirstName(TRUConstants.REGISTRY_CHANNEL_TYPE);
			registryAddress2.setLastName(TRUConstants.ADDRESS_2);
			registryAddress2.setAddress1(TRUConstants.ADDR1);
			registryAddress2.setAddress2(TRUConstants.ADDR2);
			registryAddress2.setCity(TRUConstants.CITY_NY);
			registryAddress2.setState(TRUConstants.STATE_NY);
			registryAddress2.setCountry(TRUConstants.UNITED_STATES);
			registryAddress2.setPostalCode(TRUConstants.ADDRESS_POSTAL_CODE);
			registryAddress2.setPhoneNumber(TRUConstants.ADDRESS_PHONE_NUMBER);
			registryAddress2.setEmail(TRUConstants.ADDRESS_EMAIL);
			registryAddresses.add(registryAddress2);

			registrantDetailsVO2.setAddress(registryAddress2);
			registrantDetailsVO2.setRegistrantId(TRUConstants.REGISTANT_ID2);
			registrantDetailsVOs.add(registrantDetailsVO2);
		}
		
		
		pChannelDetailsVo.setRegistrantDetailsVOs(registrantDetailsVOs);
	}
	
	/** 
	 * update registry item purchase to service .
	 * @param pOrderId - orderId
	 * @param pProductId - productId
	 * @param pSkuId - skuId
	 * @param pQuantity - quantity
	 * @throws RepositoryException 
	 * @return true or false
	 **/
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Product constrctUpdateRegistryItemProduct(String pOrderId, String pProductId, String pSkuId, Long pQuantity) throws RepositoryException {
		if (isLoggingDebug()) {
			vlogDebug("Start for TRURegistryServiceProcessor.constrctUpdateRegistryItemProduct() method");
		}
		if (pProductId == null || pSkuId == null || pQuantity == null || pOrderId == null) {
			return null;
		}
		Product product = new Product();
		String purchaseUpdateIndicator = TRURegistryConstant.PURCHASE_UPDATE_INDICATOR_I;
		Long quantity = pQuantity;
		Long upcNumber = null;
		String skn = pProductId;
		String uid = pSkuId;
		RepositoryItem skuItem = null;
		TRUShoppingCartUtils shoppingCartUtils = getShoppingCartUtils();
		TRUCatalogProperties catalogProperties = shoppingCartUtils.getCatalogProperties();
		skuItem = shoppingCartUtils.getTRUCartModifierHelper().findSkuItem(pSkuId);
		if (skuItem != null) {
			Set<String> upcNumbers = (Set<String>) skuItem.getPropertyValue(catalogProperties.getUpcNumbers());
			if (upcNumbers != null && !upcNumbers.isEmpty()) {
				for (String number: upcNumbers) {
					try {
						upcNumber = Long.parseLong(number);
						break;
					} catch(NumberFormatException nfExc) {
						vlogError("NumberFormatException occured: TRURegistryServiceProcessor.constrctUpdateRegistryItemProduct", nfExc);
					}
				}
			}
		}

		boolean flag = quantity == null ||
				StringUtils.isBlank(skn) || StringUtils.isBlank(uid) || upcNumber == null;
		if (!flag) {
			String rmsSize = null;
			String rmsColor = null;
			product.setUid(uid);
			product.setUpc(upcNumber.toString());
			product.setItemPurchased(quantity.toString());
			product.setPurchaseUpdateIndicator(purchaseUpdateIndicator);
			product.setWebOrderNumber(pOrderId);

			//START: CR # 85
			if(skuItem != null) {
				rmsSize = (String) skuItem.getPropertyValue(catalogProperties.getRmsSizeCode());
				rmsColor = (String) skuItem.getPropertyValue(catalogProperties.getRmsColorCode());
				if (skuItem.getPropertyValue(catalogProperties.getOriginalParentProduct()) != null) {
					String originalParentProductId = (String) skuItem
							.getPropertyValue(catalogProperties.getOriginalParentProduct());
					product.setSkn(originalParentProductId);
				}
			}
			if (rmsColor != null) {
				product.setColor(rmsColor);
			}
			if (rmsSize != null) {
				product.setSize(rmsSize);
			}
			if(StringUtils.isNotBlank(rmsSize) || StringUtils.isNotBlank(rmsColor)) {
				product.setSknOrigin(Integer.toString(getStoreConfig().getRmsSKNOrigin()));
			} else {
				Map registryMap = getStoreConfig().getRegistryWishlistkeyMap();
				if(registryMap != null) {
					String skuOriginDefault = (String) registryMap.get(TRUCommerceConstants.DEFAULT_SKN_ORIGIN);
					if(skuOriginDefault != null) {
						product.setSknOrigin(skuOriginDefault);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRURegistryServiceProcessor.constrctUpdateRegistryItemProduct() method");
		}
		return product;
	}
	
	/**
	 * Update registry item by shipping groups.
	 *
	 * @param pOrder the order
	 * @param pShippingGroups the shipping groups
	 */
	@SuppressWarnings("unchecked")
	public void updateRegistryItemByShippingGroups(Order pOrder, List<ShippingGroup> pShippingGroups) {
		if (isLoggingDebug()) {
			vlogDebug("Start for TRURegistryServiceProcessor.updateRegistryItemByShippingGroups() method");
		}
		if(pOrder == null || pShippingGroups == null || pShippingGroups.isEmpty()){
			return;
		}
		TRUFailedRegistryDetails failedRegistryDetails = new TRUFailedRegistryDetails();
		String registryNumber = null;
		String registryType = null;
		if (pShippingGroups.get(TRUConstants._0) != null) {
			ShippingGroup shippingGroup = pShippingGroups.get(TRUConstants._0);
			if (shippingGroup instanceof TRUChannelHardgoodShippingGroup) {
				TRUChannelHardgoodShippingGroup channelHardgoodShippingGroup = (TRUChannelHardgoodShippingGroup) shippingGroup;
				registryNumber = channelHardgoodShippingGroup.getChannelId();
				// Start - fetching registry type
				registryType = getConfiguration().getChannelTypeMap().get(channelHardgoodShippingGroup.getChannelType());
				if (StringUtils.isBlank(registryType)) {
					registryType = channelHardgoodShippingGroup.getChannelType();
				}
				// End - fetching registry type
			} else if (shippingGroup instanceof TRUChannelInStorePickupShippingGroup) {
				TRUChannelInStorePickupShippingGroup channelInStorePickupShippingGroup = (TRUChannelInStorePickupShippingGroup) shippingGroup;
				registryNumber = channelInStorePickupShippingGroup.getChannelId();
				// Start - fetching registry type
				registryType = getConfiguration().getChannelTypeMap().get(channelInStorePickupShippingGroup.getChannelType());
				if (StringUtils.isBlank(registryType)) {
					registryType = channelInStorePickupShippingGroup.getChannelType();
				}
				// End - fetching registry type
			}
		}
		ItemInfoRequest itemInfoRequest = new ItemInfoRequest();
		itemInfoRequest.setRegistryNumber(registryNumber);
		itemInfoRequest.setRegistryType(registryType);
		Locale locale = new Locale();
		locale.setCountry(TRURegistryConstant.COUNTRY_US);
		locale.setLanguage(TRURegistryConstant.LANGUAGE_EN);
		itemInfoRequest.setLocale(locale);
		failedRegistryDetails.setOrderId(pOrder.getId());
		failedRegistryDetails.setDateCreated(getCurrentDate().getTimeAsTimestamp());
		failedRegistryDetails.setDateSubmitted(getCurrentDate().getTimeAsTimestamp());
		failedRegistryDetails.setRegistryId(registryNumber);
		failedRegistryDetails.setRegistryType(registryType);
		List<Product> products = new ArrayList<Product>();
		try{
			for (ShippingGroup shippingGroup : pShippingGroups) {
				List<CommerceItemRelationship> ciRels = shippingGroup.getCommerceItemRelationships();
				for (CommerceItemRelationship commerceItemRelationship : ciRels) {
					TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) commerceItemRelationship;
					CommerceItem commerceItem = sgCiRel.getCommerceItem();
					Long quantity = sgCiRel.getQuantity();
					String skn = commerceItem.getAuxiliaryData().getProductId();
					String uid = commerceItem.getCatalogRefId();
					Product product = constrctUpdateRegistryItemProduct(pOrder.getId(),skn, uid, quantity);
					products.add(product);
				}
			}
			if(!products.isEmpty()) {
				itemInfoRequest.setProduct(products);
				failedRegistryDetails.setpProducts(itemInfoRequest.getProduct());
				if(!isRegistryDetailsEnabled()){
					//Status NEW
					failedRegistryDetails.setStatus(TRUCheckoutConstants.STATUS_NEW);
					updateFailedRegistryDetails(failedRegistryDetails);
					return;
				}
				RegistryResponse registryResponse = getRegistryService().updateItems(itemInfoRequest);
				if (registryResponse != null && registryResponse.getResponseStatus() != null &&
						!TRURegistryConstant.RESPONSE_STATUS_SUCCESS.equalsIgnoreCase(registryResponse.getResponseStatus().getIndicator())) {
					//Status FAILED
					failedRegistryDetails.setStatus(TRUCheckoutConstants.STATUS_FAILED);
					updateFailedRegistryDetails(failedRegistryDetails);
				}
			}
		} catch (RegistryIntegrationException e) {
			//Status FAILED
			failedRegistryDetails.setStatus(TRUCheckoutConstants.STATUS_FAILED);
			updateFailedRegistryDetails(failedRegistryDetails);
			if (isLoggingError()) {
				logError("RunProcessException in TRURegistryServiceProcessor.updateRegistryItemByShippingGroups", e);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in TRURegistryServiceProcessor.updateRegistryItemByShippingGroups", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRURegistryServiceProcessor.updateRegistryItemByShippingGroups() method");
		}
	}
	
	/**
	 * Update registry item by failed item.
	 *
	 * @param pRepositoryItem the repository item
	 */
	@SuppressWarnings("unchecked")
	public void updateRegistryItemByFailedItem(RepositoryItem pRepositoryItem) {
		if (isLoggingDebug()) {
			vlogDebug("Start for TRURegistryServiceProcessor.updateRegistryItemByFailedItem() method");
		}
		if(pRepositoryItem == null){
			return;
		}
		TRUCommercePropertyManager propertyManager = getShoppingCartUtils().getPropertyManager();
		String registryNumber = (String) pRepositoryItem.getPropertyValue(propertyManager.getRegistryIdPropName());
		String registryType = (String) pRepositoryItem.getPropertyValue(propertyManager.getRegistryTypePropName());
		String orderId = (String) pRepositoryItem.getPropertyValue(propertyManager.getOrderIdPropName());
		ItemInfoRequest itemInfoRequest = new ItemInfoRequest();
		itemInfoRequest.setRegistryNumber(registryNumber);
		itemInfoRequest.setRegistryType(registryType);
		Locale locale = new Locale();
		locale.setCountry(TRURegistryConstant.COUNTRY_US);
		locale.setLanguage(TRURegistryConstant.LANGUAGE_EN);
		itemInfoRequest.setLocale(locale);
		List<Product> products = new ArrayList<Product>();
		List<RepositoryItem> productItems = (List<RepositoryItem>) pRepositoryItem.getPropertyValue(propertyManager.getFailedItemDetails());
		try{
			for (RepositoryItem productItem : productItems) {
				Long quantity = (Long) productItem.getPropertyValue(propertyManager.getQuantityPropName());
				String skn = (String) productItem.getPropertyValue(propertyManager.getProductIdPropName());
				String uid = (String) productItem.getPropertyValue(propertyManager.getSkuIdPropName());
				Product product = constrctUpdateRegistryItemProduct(orderId,skn, uid, quantity);
				products.add(product);
			}
			if(!products.isEmpty()) {
				itemInfoRequest.setProduct(products);
				if(!isRegistryDetailsEnabled()){
					//No change in status
					((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getDateSubmittedPropName(), new Date(getCurrentDate().getTime()));
					return;
				}
				RegistryResponse registryResponse = getRegistryService().updateItems(itemInfoRequest);
				if (registryResponse != null && registryResponse.getResponseStatus() != null &&
						!TRURegistryConstant.RESPONSE_STATUS_SUCCESS.equalsIgnoreCase(registryResponse.getResponseStatus().getIndicator())) {
					//Status FAILED
					((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getStatusPropName(), TRUCheckoutConstants.STATUS_RETRY);
					((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getDateSubmittedPropName(), new Date(getCurrentDate().getTime()));
				} else {
					//Status SUCCESS
					((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getStatusPropName(), TRUCheckoutConstants.STATUS_SUCCESS);
					((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getDateSubmittedPropName(), new Date(getCurrentDate().getTime()));
				}
			}
		} catch (RegistryIntegrationException e) {
			//Status FAILED
			((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getStatusPropName(), TRUCheckoutConstants.STATUS_RETRY);
			((MutableRepositoryItem)pRepositoryItem).setPropertyValue(propertyManager.getDateSubmittedPropName(), new Date(getCurrentDate().getTime()));
			if (isLoggingError()) {
				logError("RunProcessException in TRURegistryServiceProcessor.updateRegistryItemByFailedItem", e);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in TRURegistryServiceProcessor.updateRegistryItemByFailedItem", e);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Exiting from TRURegistryServiceProcessor.updateRegistryItemByFailedItem() method");
		}
	}
	
	/**
	 * Update failed registry details.
	 *
	 * @param pFailedRegistryDetails the failed registry details
	 */
	public void updateFailedRegistryDetails(TRUFailedRegistryDetails pFailedRegistryDetails) {
		if (isLoggingDebug()) {
			vlogDebug("Start for TRURegistryServiceProcessor.updateFailedRegistryDetails() method");
		}
		TRUCommercePropertyManager propertyManager = getShoppingCartUtils().getPropertyManager();
		OrderTools orderTools = getShoppingCartUtils().getPricingTools().getOrderTools();
		MutableRepository orderRepository = (MutableRepository) orderTools.getOrderRepository();
		try {
			List<RepositoryItem> productItems = new ArrayList<RepositoryItem>();
			if(!pFailedRegistryDetails.getpProducts().isEmpty()) {
				for (Product product : pFailedRegistryDetails.getpProducts()) {
					MutableRepositoryItem registryFailedItemDetails = orderRepository.createItem(propertyManager.getRegistryFailedItemDetails());
					registryFailedItemDetails.setPropertyValue(propertyManager.getOrderIdPropName(), pFailedRegistryDetails.getOrderId());
					registryFailedItemDetails.setPropertyValue(propertyManager.getProductIdPropName(), product.getSkn());
					registryFailedItemDetails.setPropertyValue(propertyManager.getSkuIdPropName(), product.getUid());
					registryFailedItemDetails.setPropertyValue(propertyManager.getQuantityPropName(), Long.valueOf(product.getItemPurchased()));
					orderRepository.addItem(registryFailedItemDetails);
					orderRepository.updateItem(registryFailedItemDetails);
					productItems.add(registryFailedItemDetails);
				}
			}
			
			MutableRepositoryItem registryFailedItem = orderRepository.createItem(propertyManager.getRegistryFailedItemPropName());
			registryFailedItem.setPropertyValue(propertyManager.getRegistryIdPropName(), pFailedRegistryDetails.getRegistryId());
			registryFailedItem.setPropertyValue(propertyManager.getRegistryTypePropName(), pFailedRegistryDetails.getRegistryType());
			registryFailedItem.setPropertyValue(propertyManager.getOrderIdPropName(), pFailedRegistryDetails.getOrderId());
			registryFailedItem.setPropertyValue(propertyManager.getDateCreatedPropName(), pFailedRegistryDetails.getDateCreated());
			registryFailedItem.setPropertyValue(propertyManager.getDateSubmittedPropName(), pFailedRegistryDetails.getDateSubmitted());
			registryFailedItem.setPropertyValue(propertyManager.getStatusPropName(), pFailedRegistryDetails.getStatus());
			registryFailedItem.setPropertyValue(propertyManager.getFailedItemDetails(), productItems);
			orderRepository.addItem(registryFailedItem);
			orderRepository.updateItem(registryFailedItem);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in TRURegistryServiceProcessor.updateFailedRegistryDetails", e);
			}
		}
	}
	
}
