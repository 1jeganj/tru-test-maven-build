package com.tru.droplet;

import atg.droplet.TagConverterManager;

/**
 * The Class TRUTagConverterManager.
 */
public class TRUTagConverterManager extends TagConverterManager {
	 static
	  {
	    registerTagConverter(new TRUCreditCardTagConverter());
	  }
}
