package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * The Class TRUSchemeDetectionDroplet used to check origin scheme header.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSchemeDetectionDroplet extends DynamoServlet {

	/**
	 * scheme constant.
	 */
	public static final String SCHEME = "scheme";

	/**
	 * Scheme Detection Util.
	 */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Non Secure protocol constant.
	 */
	public static final String NON_SECURE_PROTOCOL = "http";
	
	/**
	 * Secure protocol constant.
	 */
	public static final String SECURE_PROTOCOL = "https";

	/**
	 * This method used to check origin scheme header.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		vlogDebug("===BEGIN===:: TRUSchemeDetectionDroplet.service() method:: ");

		if (pRequest != null) {
			String scheme = getSchemeDetectionUtil().getScheme();
				pRequest.setParameter(SCHEME, scheme);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM,pRequest, pResponse);
		}
		vlogDebug("===END===:: TRUSchemeDetectionDroplet.service() method:: ");
	}
	
	/**
	 * @return the schemeDetectionUtil .
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * @param pSchemeDetectionUtil - the schemeDetectionUtil status to set.
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	

}