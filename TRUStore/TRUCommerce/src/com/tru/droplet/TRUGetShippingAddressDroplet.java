package com.tru.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupRelationship;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.service.idgen.IdGeneratorException;
import atg.service.idgen.SQLIdGenerator;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.vo.ShippingAddressVO;
import com.tru.common.TRUConstants;


/**
 * The Class TRUGetShippingAddressDroplet used to get the shipping address of order.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUGetShippingAddressDroplet extends DynamoServlet {

	/** Property to hold the ShippingHelper */
	private TRUShippingAddressHelper mShippingHelper;

	/**
	 * service() method is used to get shipping address in order
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 * @throws ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("START : TRUGetShippingAddressDroplet Service() :");
		}
		String shippAddress = TRUConstants.EMPTY;
		
		Order order = (Order) pRequest.getObjectParameter(TRUConstants.PAYPAL_ORDER);
		if(order != null && order instanceof TRUOrderImpl) {
			List<CommerceItem> commItems = order.getCommerceItems();
			Double maxAmount = TRUConstants.DOUBLE_ZERO;
			CommerceItem maxAmtCommItem = null;
			for (CommerceItem ci : commItems) {
				if (ci instanceof TRUDonationCommerceItem || ci instanceof TRUGiftWrapCommerceItem) {
					continue;
				}
				if (null!=ci && null!=ci.getPriceInfo() && ci.getPriceInfo().getAmount() > maxAmount) {
					maxAmount = ci.getPriceInfo().getAmount();
					maxAmtCommItem = ci;
				}
			}
			if (null != maxAmtCommItem) {
				ShippingGroupRelationship shipGrpRel = (ShippingGroupRelationship) maxAmtCommItem.getShippingGroupRelationships().get(
						TRUConstants.INTEGER_NUMBER_ZERO);
				ShippingGroup shipGrp = shipGrpRel.getShippingGroup();

				ShippingAddressVO lShippingAddress = new ShippingAddressVO();
				if (shipGrp instanceof HardgoodShippingGroup) {
					Address address = ((HardgoodShippingGroup) shipGrp).getShippingAddress();
					lShippingAddress.setFirstName(checkForEmpty(address.getFirstName()));
					lShippingAddress.setMiddleName(checkForEmpty(address.getMiddleName()));
					lShippingAddress.setLastName(checkForEmpty(address.getLastName()));
					lShippingAddress.setAddress1(checkForEmpty(address.getAddress1()));
					lShippingAddress.setAddress2(checkForEmpty(address.getAddress2()));
					lShippingAddress.setAddress3(checkForEmpty(address.getAddress3()));
					lShippingAddress.setCity(checkForEmpty(address.getCity()));
					lShippingAddress.setState(checkForEmpty(address.getState()));
					lShippingAddress.setPostalCode(checkForEmpty(address.getPostalCode()));
					lShippingAddress.setCountryCode(checkForEmpty(address.getCountry()));
					if(address instanceof ContactInfo){
						lShippingAddress.setPhone1(((ContactInfo)address).getPhoneNumber());
					}
					lShippingAddress.setPhone2(TRUConstants.EMPTY);
					if (isLoggingDebug()) {
						logDebug("TRUGetShippingAddressDroplet Service() : shipping address retrieved from HardgoodShippingGroup item");
					}
				} else if (shipGrp instanceof InStorePickupShippingGroup) {
					TRUInStorePickupShippingGroup inStoreShipGroup = (TRUInStorePickupShippingGroup) shipGrp;
					String storeLocationId = inStoreShipGroup.getLocationId();
					lShippingAddress.setFirstName(checkForEmpty(inStoreShipGroup.getFirstName()));
					lShippingAddress.setMiddleName(checkForEmpty(inStoreShipGroup.getMiddleName()));
					lShippingAddress.setLastName(checkForEmpty(inStoreShipGroup.getLastName()));
					getShippingHelper().updateStoreAddress(storeLocationId, lShippingAddress);
					lShippingAddress.setAddress3(TRUConstants.EMPTY);
					lShippingAddress.setPhone2(TRUConstants.EMPTY);
					if (isLoggingDebug()) {
						logDebug("TRUGetShippingAddressDroplet Service() : shipping address retrieved from InStorePickupShippingGroup item");
					}
				}
				shippAddress = prepareJSONObject(lShippingAddress);
			}
		}
		if(StringUtils.isEmpty(shippAddress)) {
			shippAddress = prepareJSONObject(TRUConstants.EMPTY_STRING);
		}
		String cardinalRequestId = null;
		try {
			cardinalRequestId = getIdGenerator().generateStringId(getShippingHelper().getCommercePropertyManager().getCardinalRequestIdPropertyName());
			cardinalRequestId = prepareJSONObject(cardinalRequestId);
		} catch (IdGeneratorException e) {
			if (isLoggingError()) {
				logError("Unable to get UniquePaymentId from IdGenerator.", e);
			}
		}
		pRequest.setParameter(getShippingHelper().getCommercePropertyManager().getCardinalRequestIdPropertyName(), cardinalRequestId);
		pRequest.setParameter(TRUConstants.SHIPP_ADDRESS, shippAddress);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("END : TRUGetShippingAddressDroplet Service() :");
		}
	}

	/**
	 * prepare the JSONObject
	 * @param pData - data
	 * @return request json object as string
	 *             
	 */
	public String prepareJSONObject(Object pData) {
		if (isLoggingDebug()) {
			logDebug("TRUGetShippingAddressDroplet : prepareJSONObject(): START");
		}
		String request = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			request = objectMapper.writeValueAsString(pData);
		} catch (JsonProcessingException e) {
			if (isLoggingError()) {
				logError("JsonProcessingException occured at method prepareJSONObject() in class TRUGetShippingAddressDroplet: {0}", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUGetShippingAddressDroplet : prepareJSONObject(): END");
		}
		return request;
	}

	/**
	 * check for empty or null and return empty
	 * @param pVal value
	 * @return retValue return value if not empty else empty constant
	 * 
	 */
	public String checkForEmpty(String pVal) {
		String retValue = TRUConstants.EMPTY;
		if (StringUtils.isNotBlank(pVal)) {
			retValue = pVal;
		}
		return retValue;
	}

	/**
	 * @return mShippingHelper the shippingHelper
	 */
	public TRUShippingAddressHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingAddressHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Holds component for IDGenerator.
	 */
	private SQLIdGenerator mIdGenerator;

	/**
	 * @return the idGenerator
	 */
	public SQLIdGenerator getIdGenerator() {
		return mIdGenerator;
	}

	/**
	 * @param pIdGenerator the idGenerator to set
	 */
	public void setIdGenerator(SQLIdGenerator pIdGenerator) {
		mIdGenerator = pIdGenerator;
	}
}
