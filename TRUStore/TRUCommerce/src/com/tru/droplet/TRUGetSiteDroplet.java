package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.utils.TRUGetSiteTypeUtil;


/**
 * The Class TRUGetSiteDroplet used to set the site URL to request object for current request.
 *
 * @author PA
 * @version 1.0
 */
public class TRUGetSiteDroplet extends DynamoServlet{


	/** The m tru get site type mTRUGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

/**
 * This method used to set the site URL to request object for current request.
 *
 * @param pRequest the request
 * @param pResponse the response
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		
		vlogDebug("===BEGIN===:: TRUGetSiteDroplet.service() method:: ");
		
		 Object siteId = pRequest.getLocalParameter(TRUCommerceConstants.SITE_ID);

		    Site site = null;
		    String siteUrl = null;
		    if (siteId != null) {
		      try {
		        site = getTRUGetSiteTypeUtil().getSite(siteId.toString());
		      }
		      catch (SiteContextException e) {
		        vlogError(e, "Error getting site {0}", new Object[] { siteId });

		        pRequest.setParameter(TRUCommerceConstants.ERROR_MESSAGE, TRUCommerceConstants.ERROR_GETTING_SITE + siteId);
		        pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
		        return;
		      }
		    }

		    if (site == null) {
		      vlogDebug("Cound not find site {0}.", new Object[] { siteId });
		      pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		    }
		    else{
		      
		      TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(pRequest,site);
		      if(truSiteVO != null){
		    	  siteUrl = truSiteVO.getSiteURL();
		      }
		      
		      pRequest.setParameter(TRUCommerceConstants.SITE_URL, siteUrl);
		      pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		   }	
		    vlogDebug("===END===:: TRUGetSiteDroplet.service() method:: ");
	}


	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}
	
	
}