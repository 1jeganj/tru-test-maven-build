package com.tru.droplet;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.order.vo.ShippingAddressVO;
import com.tru.common.TRUConstants;

/**
 * The Class TRUGetShippingAddressDroplet used to get the shipping from repository for store orders.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUShippingAddressHelper extends GenericService {

	/** Property to hold the LocationRepository */
	private Repository mLocationRepository;
	/** Property to hold the LocationItemType */
	private String mLocationItemType;
	/** Property to hold the commerceProperty manager */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/**
	 * method is used to get shipping address from repository
	 * 
	 * @param pStoreId
	 *            - pStoreId
	 * @param pShipAddressVO
	 *            - pShipAddressVO
	 */
	public void updateStoreAddress(String pStoreId, ShippingAddressVO pShipAddressVO) {
		if (isLoggingDebug()) {
			logDebug("START : TRUShippingAddressHelper TRUShippingAddressHelper() :");
		}
		RepositoryItem location;
		try {
			location = getLocationRepository().getItem(pStoreId, getLocationItemType());
			if (null != location) {
				String address1 = (String) location.getPropertyValue(getCommercePropertyManager().getStoreAddress1());
				String address2 = (String) location.getPropertyValue(getCommercePropertyManager().getStoreAddress2());
				String city = (String) location.getPropertyValue(getCommercePropertyManager().getCityName());
				String state = (String) location.getPropertyValue(getCommercePropertyManager().getStoreStateAddress());
				String postalCode = (String) location.getPropertyValue(getCommercePropertyManager().getStorePostalCode());
				String country = (String) location.getPropertyValue(getCommercePropertyManager().getStoreCountry());
				String phone = (String) location.getPropertyValue(getCommercePropertyManager().getStorePhoneNumber());
				pShipAddressVO.setAddress1(address1);
				pShipAddressVO.setAddress2(address2);
				pShipAddressVO.setCity(city);
				pShipAddressVO.setState(state);
				pShipAddressVO.setPostalCode(postalCode);
				if (null == country) {
					pShipAddressVO.setCountryCode(TRUConstants.UNITED_STATES);
				} else {
					pShipAddressVO.setCountryCode(TRUConstants.UNITED_STATES);
				}
				pShipAddressVO.setPhone1(phone);
			}
		} catch (RepositoryException e) {
			vlogError("RepositoryException thrown in TRUShippingAddressHelper.updateStoreAddress", e);
		}

		if (isLoggingDebug()) {
			logDebug("END : TRUShippingAddressHelper TRUShippingAddressHelper() :");
		}
	}

	/**
	 * @return the locationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * @param pLocationRepository
	 *            the locationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	/**
	 * @return the locationItemType
	 */
	public String getLocationItemType() {
		return mLocationItemType;
	}

	/**
	 * @param pLocationItemType
	 *            the locationItemType to set
	 */
	public void setLocationItemType(String pLocationItemType) {
		mLocationItemType = pLocationItemType;
	}

	/**
	 * @return the commercePropertyManager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * @param pCommercePropertyManager
	 *            the commercePropertyManager to set
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		mCommercePropertyManager = pCommercePropertyManager;
	}
}
