package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;


/**
* <p>
* This class provides the implementation for the displaying all the console logs at page level.
* </p>
*
* @author Professional Access
* @version 1.0
*
*/
public class TRUAjaxLogsLookupDroplet extends DynamoServlet {

	/**
	 * This method Fetches the browser console logs and display in the server logs.
	 * @param pRequest          - DynamoHttpServletRequest
     * @param pResponse         - DynamoHttpServletResponse
     * @throws ServletException - if an exception occurred while processing the servlet
     * @throws IOException      - if an exception occurred while reading and writing the servlet request
     */	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if(isLoggingDebug()){
			logDebug("::: TRUConsoleLogsLookupDroplet.service Start :::");
		}
		String requestURL = (String) pRequest.getLocalParameter(TRUCommerceConstants.AJAX_REQUEST_URL);
		String exception = (String) pRequest.getLocalParameter(TRUCommerceConstants.EXCEPTION_DETAILS);
		
		vlogInfo("Console Errors Occured:  AjaxRequestURL:{0}, Exception:{1} ",requestURL, exception);
		if(isLoggingDebug()){
			logDebug("::: TRUConsoleLogsLookupDroplet.service End :::");
		}
	}
}
