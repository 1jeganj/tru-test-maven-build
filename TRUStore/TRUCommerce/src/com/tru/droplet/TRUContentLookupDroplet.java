package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUContentTools;
import com.tru.common.vo.TRUContentVO;


/**
* <p>
* This class provides the implementation for the displaying of the static content based on the selected page.
* This droplet renders the static content configured in BCC making use of Endeca EM pages based on the 
* selected page.
* </p>
*
* Input Parameters: <br>
* <ul>
* <li>articleContentKey - represents the content key configured for the page.</li>
* </ul>
* Open Parameters: <br>
* <ul>
* <li>output - Value rendered after check.</li>
* </ul>
* Output Parameters: <br>
* <ul>
* <li>seoTitleText - configured seoTitleText in article repository</li>
* <li>seoMetaDesc - configured seoMetaDesc in article repository</li>
* <li>seoMetaKeyword - configured seoMetaKeyword in article repository</li>
* <li>seoAltDesc - configured seoAltDesc in article repository</li>
* <li>articleBody - configured articleBody in article repository</li>
* </ul>
* <p>
* 
* Usage: 
* &lt;dsp:droplet name="TRUContentLookupDroplet"&gt;
* &lt;dsp:param name="articleContentKey" value=${contentKey}/&gt;
* &lt;dsp:oparam name="output"&gt;
* &lt;/dsp:oparam&gt;
* &lt;dsp:droplet&gt;
*
* </p>
*
* @author Professional Access
* @version 1.0
*
*/
public class TRUContentLookupDroplet extends DynamoServlet {
	/** Property to hold mContentTools. */
	private TRUContentTools mContentTools;
	
	/**
	 * Gets the ContentTools.
	 * 
	 * @return the mContentTools
	 */
	public TRUContentTools getContentTools() {
		return mContentTools;
	}

	/**
	 * Sets the contentTools.
	 * 
	 * @param pContentTools
	 *            the new ContentTools
	 */
	public void setContentTools(TRUContentTools pContentTools) {
		mContentTools = pContentTools;
	}

	/**
	 * This method Fetches the BCC configured static content making use of endeca EM based on the key provided.
	 * @param pRequest          - DynamoHttpServletRequest
     * @param pResponse         - DynamoHttpServletResponse
     * @throws ServletException - if an exception occurred while processing the servlet
     * @throws IOException      - if an exception occurred while reading and writing the servlet request
     */	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if(isLoggingDebug()){
			logDebug("::: TRUContentLookupDroplet.service Start :::");
		}
		String articleName = (String) pRequest.getLocalParameter(TRUCommerceConstants.ARTICLE_CONTENT_KEY);
		//fetches based on key passsed
		if(articleName!=null){
			TRUContentVO contentVO=getContentTools().fetchContent(articleName);
				if(contentVO!=null){
				//sets output parameters
				pRequest.setParameter(TRUCommerceConstants.SEO_TITLE_TEXT, contentVO.getSeoTitleText());
				pRequest.setParameter(TRUCommerceConstants.SEO_META_DESC,contentVO.getSeoMetaDesc());
				pRequest.setParameter(TRUCommerceConstants.SEO_META_KEYWORD,contentVO.getSeoMetaKeyword());
				pRequest.setParameter(TRUCommerceConstants.SEO_ALT_DESC, contentVO.getSeoAltDesc());
				pRequest.setParameter(TRUCommerceConstants.ARTICLE_BODY,contentVO.getArticleBody());
			}
		}
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("::: TRUContentLookupDroplet.service End :::");
		}
	}
}
