package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;
/**
 * This class is intended for creating the Registry cookie. parameters to be
 * passed by user, 'addToRegistryCookie' is a required parameter.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>addToRegistryCookie</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>addToRegistryCookie</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/droplet/TRUSetRegistryCookieDroplet"&gt;<br>
 *          &lt;dspel:param name="addToRegistryCookie" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 */
public class TRUSetRegistryCookieDroplet extends DynamoServlet {

	/**
	 * Constant to hold HYPHEN_SYMBOL.
	 */
	public static final String PATH = "/";

	/**
	 * This method is overridden to set addToRegistryCookie
	 * 
	 * @param pReq
	 *            - DynamoHttpServletRequest
	 * @param pRes
	 *            - DynamoHttpServletResponse
	 * @throws IOException
	 *             -
	 * @throws ServletException
	 *             -
	 */
	public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes) throws ServletException,	IOException {
		boolean retrieveCookies = false;
		String pageName = (String) pReq.getLocalParameter(TRUConstants.PAGENAME);
		String addToRegistryCookie = (String) pReq.getLocalParameter(TRUConstants.ADDTOREGISTRY_COOKIE);
		String retrieveRegistryCookie=(String)pReq.getLocalParameter(TRUConstants.RETRIEVE_REGISTRY_COOKIE);
		if (StringUtils.isNotBlank(retrieveRegistryCookie) && retrieveRegistryCookie.equals(TRUConstants.TRUE)) {
			retrieveCookies = true;
		}
		Cookie newcookie;
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSetRegistryCookieDroplet.service method..");
		}
		if(pReq.getCookies() == null){
			newcookie = new Cookie(TRUConstants.ADDTOREGISTRY_COOKIE,addToRegistryCookie);
			newcookie.setPath(PATH);
			newcookie.setDomain(TRUConstants.DOMAINNAME_PREFIX);
			pRes.addCookie(newcookie);
		}else{
			setRegistryDetails(pReq, pRes, pageName,addToRegistryCookie,TRUConstants.DOMAINNAME_PREFIX);
		}
		if (retrieveCookies && pReq.getCookies() != null) {
			for (Cookie cookie : pReq.getCookies()) {
				if (cookie.getName().equals(TRUConstants.ADDTOREGISTRY_COOKIE)) {
					String addToRegistryCookieFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRUConstants.ADDTOREGISTRY_COOKIE, addToRegistryCookieFromCookie);
				}
			  }
		}
		pReq.serviceLocalParameter(TRUConstants.OUTPUT_PARAM, pReq, pRes);
		if (isLoggingDebug()) {
			logDebug("END:: TRUSetRegistryCookieDroplet.service method..");
		}
	}
	/**
	 * This method will set Registry  details.
	 * 
	 * 
	 * @param pReq -  Request
	 * @param pRes - Response
	 * @param pPageName - PageName 
	 * @param pAddToRegistryCookie - AddToRegistryCookie
	 * @param pDomainNamePrefix - DomainNamePrefix
	 */
	private void setRegistryDetails(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes, String pPageName,String pAddToRegistryCookie, String pDomainNamePrefix) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSetRegistryCookieDroplet.setRegistryDetails method..");
		}
		boolean isCreateAddToRegistryCookie=true;
		Cookie newcookie;
		for (Cookie cookie : pReq.getCookies()) {
			if (cookie.getName().equals(TRUConstants.ADDTOREGISTRY_COOKIE)	&& StringUtils.isNotBlank(pAddToRegistryCookie)) {
				cookie.setValue(pAddToRegistryCookie);
				cookie.setPath(PATH);
				cookie.setDomain(TRUConstants.DOMAINNAME_PREFIX);
				pRes.addCookie(cookie);
				pReq.setParameter(TRUConstants.ADDTOREGISTRY_COOKIE, pAddToRegistryCookie);
				isCreateAddToRegistryCookie=false;
			}
		}
		if(isCreateAddToRegistryCookie && StringUtils.isNotBlank(pAddToRegistryCookie)){
			newcookie = new Cookie(TRUConstants.ADDTOREGISTRY_COOKIE,pAddToRegistryCookie);
			newcookie.setPath(PATH);
			newcookie.setDomain(TRUConstants.DOMAINNAME_PREFIX);
			pRes.addCookie(newcookie);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSetRegistryCookieDroplet.setRegistryDetails method..");
		}
	}

}
