package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;

/**
 * This class is intended for creating the Registry cookie. parameters to be
 * passed by user, 'addToWishListCookie' is a required parameter.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>addToWishListCookie</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>addToWishListCookie</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/droplet/TRUAddWishlistCookieDroplet"&gt;<br>
 *          &lt;dspel:param name="registry_id" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 */
public class TRUAddWishlistCookieDroplet extends DynamoServlet {

	/**
	 * Constant to hold HYPHEN_SYMBOL.
	 */
	public static final String PATH = "/";

	/**
	 * This method is overridden to set cookie addToWishListCookie
	 * 
	 * @param pReq
	 *            - DynamoHttpServletRequest
	 * @param pRes
	 *            - DynamoHttpServletResponse
	 * @throws IOException
	 *             -
	 * @throws ServletException
	 *             -
	 */
	public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes) throws ServletException,	IOException {
		boolean retrieveCookies = false;
		String pageName = (String) pReq.getLocalParameter(TRUConstants.PAGENAME);
		String addToWishListCookie = (String) pReq.getLocalParameter(TRUConstants.ADDTOWISHLIST_COOKIE);
		String retrieveRegistryCookie=(String)pReq.getLocalParameter(TRUConstants.RETRIEVE_REGISTRY_COOKIE);
		if (StringUtils.isNotBlank(retrieveRegistryCookie) && retrieveRegistryCookie.equals(TRUConstants.TRUE)) {
			retrieveCookies = true;
		}
		String domainName = (String)pReq.getServerName();
		String domainNamePrefix=null;
		if (StringUtils.isNotBlank(domainName))
		{
         domainNamePrefix = domainName.substring(domainName.indexOf(TRUConstants.DOT), domainName.length());
		}
		Cookie newcookie;
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUAddWishlistCookieDroplet.service method..");
		}
		if(pReq.getCookies() == null){
			newcookie = new Cookie(TRUConstants.ADDTOWISHLIST_COOKIE,addToWishListCookie);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
		}else{
			setRegistryDetails(pReq, pRes, pageName,addToWishListCookie,domainNamePrefix);
		}
		if (retrieveCookies && pReq.getCookies() != null) {
			for (Cookie cookie : pReq.getCookies()) {
				if (cookie.getName().equals(TRUConstants.ADDTOWISHLIST_COOKIE)) {
					String addToWishListCookieFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRUConstants.ADDTOWISHLIST_COOKIE, addToWishListCookieFromCookie);
				}
			  }
		}
		pReq.serviceLocalParameter(TRUConstants.OUTPUT_PARAM, pReq, pRes);
		if (isLoggingDebug()) {
			logDebug("END:: TRUAddWishlistCookieDroplet.service method..");
		}
	}
	/**
	 * This method will set Registry  details.
	 * 
	 * 
	 * @param pReq -  Request
	 * @param pRes - Response
	 * @param pPageName - PageName 
	 * @param pAddToWishListCookie - AddToWishlistCookie
	 * @param pDomainNamePrefix - DomainNamePrefix
	 */
	private void setRegistryDetails(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes, String pPageName,String pAddToWishListCookie, String pDomainNamePrefix) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUAddWishlistCookieDroplet.setRegistryDetails method..");
		}
		boolean isCreateCnameCookie=true;
		Cookie newcookie;
		for (Cookie cookie : pReq.getCookies()) {
			if (cookie.getName().equals(TRUConstants.ADDTOWISHLIST_COOKIE)	&& StringUtils.isNotBlank(pAddToWishListCookie)) {
				cookie.setValue(pAddToWishListCookie);
				cookie.setPath(PATH);
				cookie.setDomain(pDomainNamePrefix);
				pRes.addCookie(cookie);
				pReq.setParameter(TRUConstants.ADDTOWISHLIST_COOKIE, pAddToWishListCookie);
				isCreateCnameCookie=false;
			}
		}
		if(isCreateCnameCookie && StringUtils.isNotBlank(pAddToWishListCookie)){
			newcookie = new Cookie(TRUConstants.ADDTOWISHLIST_COOKIE,pAddToWishListCookie);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUAddWishlistCookieDroplet.setRegistryDetails method..");
		}
	}

}
