package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * The Class TRUGetSiteTypeDroplet used to get the site type to request object for current request.
 *
 * @author PA
 * @version 1.0
 */
public class TRUGetSiteTypeDroplet extends DynamoServlet {
	
	/** The m tru get site type mTRUGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/**
	 * @return the tRUGetSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}

	/**
	 * @param pTRUGetSiteTypeUtil the tRUGetSiteTypeUtil to set
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}

	/**
	 * This method used to get the site type to request object for current request.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
		
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if (isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUGetSiteTypeDroplet.service() method:: ");
		}
		Object siteId = pRequest.getLocalParameter(TRUCommerceConstants.SITE_ID);
		Site site = null;
		String currentSite = null;
		if (siteId != null) {
			try {
		        site = getTRUGetSiteTypeUtil().getSite(siteId.toString());
		      }
		      catch (SiteContextException e) {
		    	  if (isLoggingError()) {
		    		  vlogError(e, "Error getting site {0}", new Object[] { siteId });
		    	  }
		    	  pRequest.setParameter(TRUCommerceConstants.ERROR_MESSAGE, TRUCommerceConstants.ERROR_GETTING_SITE + siteId);
		    	  pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
		    	  return;
		      }
		}
		if (site == null) {
			if (isLoggingDebug()) {
				vlogDebug("Cound not find site {0}.", new Object[] { siteId });
			}
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}
		else{
			TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(pRequest,site);
			if(truSiteVO != null){
				currentSite = truSiteVO.getSiteStatus();
			}
			pRequest.setParameter(TRUCommerceConstants.CURRENT_SITE, currentSite);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("===END===:: TRUGetSiteTypeDroplet.service() method:: ");
		}
	}
}
