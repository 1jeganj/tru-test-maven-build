package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.commerce.promotion.TRUPromotionTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;

/**
 * The Class TRUGetSiteDroplet used to set the site URL to request object for current request.
 * 
 * @author PA
 * @version 1.0
 */
public class TenderBasedCheckDroplet extends DynamoServlet {
	
	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;

	/**
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug("Entering into TenderBasedCheckDroplet.updatePromotionItem() method");
		}
		Order order = (Order) pRequest.getObjectParameter(TRUCommerceConstants.PARAM_ORDER);
		Boolean tenderTypePromotion = Boolean.FALSE;
		if (order != null) {
			tenderTypePromotion = getPromotionTools().checkTenderTypePromotion(order);
			pRequest.setParameter(TRUCommerceConstants.SHOW_GIFT_CARD_FLAG, tenderTypePromotion);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TenderBasedCheckDroplet.updatePromotionItem() method");
		}

	}

	/**
	 * Gets the promotion tools.
	 * 
	 * @return the promotionTools.
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * Sets the promotion tools.
	 * 
	 * @param pPromotionTools
	 *            the promotionTools to set.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

}