package com.tru.droplet;

import java.util.Properties;

import atg.droplet.CreditCardTagConverter;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.core.util.TRUNumberFormat;

/**
 * The Class TRUCreditCardTagConverter.
 */
public class TRUCreditCardTagConverter extends CreditCardTagConverter {
	
	/**
	 * Gets the mask character.
	 *
	 * @param pAttributes the attributes
	 * @return the mask character
	 */
	String getMaskCharacter(Properties pAttributes)
	{
		String maskChar = pAttributes.getProperty(TRUCommerceConstants.MASK_CHARACTER);
		if (maskChar != null) {
			return maskChar;
		}
		return TRUCommerceConstants.STRING_X;
	}

	/**
	 * Gets the number characters unmasked.
	 *
	 * @param pAttributes the attributes
	 * @return the number characters unmasked
	 */
	int getNumberCharactersUnmasked(Properties pAttributes)
	{
		String numUnmasked = pAttributes.getProperty(TRUCommerceConstants.NUM_CHARS_UNMASKED);
		if (numUnmasked != null) {
			return Integer.parseInt(numUnmasked);
		}
		return TRUCommerceConstants.INT_FOUR;
	}

	/**
	 * Gets the size of grouping.
	 *
	 * @param pAttributes the attributes
	 * @return the size of grouping
	 */
	int getSizeOfGrouping(Properties pAttributes)
	{
		String groupSize = pAttributes.getProperty(TRUCommerceConstants.GROUPING_SIZE);
		if (groupSize != null) {
			return Integer.parseInt(groupSize);
		}
		return TRUCommerceConstants.INT_ZERO;
	}

	/* (non-Javadoc)
	 * @see atg.droplet.CreditCardTagConverter#formatCreditCard(java.lang.Object, java.util.Properties)
	 */
	@Override
	public String formatCreditCard(Object pCreditCard, Properties pAttributes)
	{
		String maskCharacter = getMaskCharacter(pAttributes);
		int numUnmasked = getNumberCharactersUnmasked(pAttributes);
		int groupingSize = getSizeOfGrouping(pAttributes);
		String creditCardNumber = TRUNumberFormat.formatCreditCardNumber(pCreditCard.toString(), maskCharacter, numUnmasked, groupingSize);
		return creditCardNumber;
	}
}
