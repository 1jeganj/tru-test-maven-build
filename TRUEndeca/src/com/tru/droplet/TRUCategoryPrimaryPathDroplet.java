package com.tru.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.vo.PrimaryPathCategoryVO;
import com.tru.endeca.category.primarypath.TRUCategoryPrimaryPathManager;


/**
 * This droplet is used to render The Primary Path for Sku.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUCategoryPrimaryPathDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCategoryPrimaryPathManager mPrimaryPathManager;

	/** 
	 * property hold catalogTools. 
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * This method will get the skuId from the JSP as a parameter and it will retun the primaryPath for the perticular sku.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUCategoryPrimaryPathDroplet.service method..");
		}
		List<PrimaryPathCategoryVO> primaryPathCategoryVOList = null;
		String skuId = null;
		String collectionId =  null;
		//TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogTools().getCatalogProperties();

		if(pRequest != null) {
			skuId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SKU_ID);
			collectionId = (String) pRequest.getLocalParameter(TRUCommerceConstants.COLLECTION_ID);

			if(StringUtils.isNotEmpty(collectionId)) {
				try {
					RepositoryItem collectionProduct = getCatalogTools().findProduct(collectionId);
					if (collectionProduct != null) {
						
						List<RepositoryItem> childSku =	getPrimaryPathManager().getChildSku(collectionProduct);
						//List<RepositoryItem> childSku = (List<RepositoryItem>) collectionProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());
						if (childSku != null && !childSku.isEmpty()) {
							String childSkuId = childSku.get(TRUCommerceConstants.INT_ZERO).getRepositoryId();
							if (StringUtils.isNotEmpty(childSkuId)) {
								primaryPathCategoryVOList= getPrimaryPathManager().populatePrimaryPathVOList(childSkuId);
							}
						}
					}
				} catch (RepositoryException reporitoryException) {
					if (isLoggingError()) {
						vlogError(reporitoryException, "Reporitory Exception occured in TRUCategoryPrimaryPathDroplet.service Method");
					}
				}
			} else if(StringUtils.isNotEmpty(skuId)){
				primaryPathCategoryVOList= getPrimaryPathManager().populatePrimaryPathVOList(skuId);
			}
		}
		
		if(primaryPathCategoryVOList != null && !primaryPathCategoryVOList.isEmpty()){
			pRequest.setParameter(TRUCommerceConstants.PRIMARY_PATH_CATEGORYVO_LIST, primaryPathCategoryVOList);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter( TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathDroplet.service method.. ");
		}
	}

	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return mPrimaryPathManager
	 */
	public TRUCategoryPrimaryPathManager getPrimaryPathManager() {
		return mPrimaryPathManager;
	}
	/**
	 * @param pPrimaryPathManager -- the PrimaryPathManager
	 */
	public void setPrimaryPathManager(TRUCategoryPrimaryPathManager pPrimaryPathManager) {
		mPrimaryPathManager = pPrimaryPathManager;
	}


}
