package com.tru.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class set the SEO related data in request object based on Page type.
 * @author PA
 * @version 1.0
 *
 */
public class TRUSEODroplet extends DynamoServlet{
	
	/** property to hold TRUSEOManager instance. */
	private TRUSEOManager mTruSeoManager;
	
	/**
	 * Droplet to return category and product meta informations.
	 *
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSEODroplet.service()");
		}
		Map<String,Object> seoTagMap = null;
		String siteId = null ;
		String page = null;
		String id = null;
		
		if(pRequest != null){
			 siteId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SITE_ID);
			 page = (String) pRequest.getLocalParameter(TRUCommerceConstants.PARAM_PAGE);
			 if(page.equals(TRUEndecaConstants.HOME)){
				id = getTruSeoManager().getCatalogIdForHomePage(pRequest);
			 }else{
				 id = (String) pRequest.getLocalParameter(TRUCommerceConstants.ID);
			 }
			
		}
		try{
			if(!StringUtils.isEmpty(page) && !StringUtils.isEmpty(id)){	
				if(!StringUtils.isEmpty(siteId) && getTruSeoManager() != null){
					seoTagMap = getTruSeoManager().fetchSEOTags(page, id, siteId);
				}
				
				if(seoTagMap != null && !seoTagMap.isEmpty()){
					if(page.equals(TRUEndecaConstants.CAT) || page.equals(TRUEndecaConstants.SUBCAT) || page.equals(TRUEndecaConstants.FAMILY)){
						setSeoForAllCatPage(pRequest, pResponse, seoTagMap);
					}else if(page.equals(TRUEndecaConstants.PDP)){
						setSeoForPdpPage(pRequest, pResponse, seoTagMap);
					}else if(page.equals(TRUEndecaConstants.HOME)){
						setSeoForHomePage(pRequest, pResponse, seoTagMap);
					}
				}else{
					pRequest.serviceLocalParameter(TRUEndecaConstants.EMPTY_PARAM, pRequest, pResponse);
				}
			}else{
				pRequest.serviceLocalParameter(TRUEndecaConstants.EMPTY_PARAM, pRequest, pResponse);
			}
		}catch (IOException ioException){
			if (isLoggingError()) {
				vlogError(ioException,"IOException Exception TRUSEODroplet.service() method");		
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("END:: TRUSEODroplet.service() method");
		}
	}
	
	
	/**
	 * This method is used to set SEOForHomePage.
	 * 
	 * @param pRequest -request
	 * @param pResponse -response
	 * @param pSeoTagMap -seoTagmap
	 * @throws ServletException - servlet Exception
	 * @throws IOException -IOException
	 */
	private void setSeoForHomePage(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, Map<String, Object> pSeoTagMap)  throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSEODroplet.setSeoForHomePage()");
		}
		pRequest.setParameter(TRUEndecaConstants.HOME_PAGE_META_DESCRIPTION, pSeoTagMap.get(TRUEndecaConstants.HOME_PAGE_META_DESCRIPTION));
		pRequest.setParameter(TRUEndecaConstants.KEYWORDS, pSeoTagMap.get(TRUEndecaConstants.KEYWORDS));
		pRequest.setParameter(TRUEndecaConstants.HOME_SEO_TITLE, pSeoTagMap.get(TRUEndecaConstants.HOME_SEO_TITLE));
		pRequest.setParameter(TRUEndecaConstants.HOME_SEO_H1, pSeoTagMap.get(TRUEndecaConstants.HOME_SEO_H1));
		
		pRequest.serviceLocalParameter(TRUEndecaConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("END:: TRUSEODroplet.setSeoForHomePage()");
		}
	}

	/**
	 * Sets the seo for all cat page(cat,family,brand,subcat).
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @param pSeoTagMap the seo tag map
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void setSeoForAllCatPage(DynamoHttpServletRequest pRequest,  DynamoHttpServletResponse pResponse, Map<String,Object> pSeoTagMap) throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSEODroplet.setSeoForAllCatPage()");
		}
		pRequest.setParameter(TRUEndecaConstants.CAT_SEO_TITLE, pSeoTagMap.get(TRUEndecaConstants.CAT_SEO_TITLE));
		pRequest.setParameter(TRUEndecaConstants.CAT_SEO_H1_TEXT, pSeoTagMap.get(TRUEndecaConstants.CAT_SEO_H1_TEXT));
		pRequest.setParameter(TRUEndecaConstants.CAT_SEO_DESCRIPTION, pSeoTagMap.get(TRUEndecaConstants.CAT_SEO_DESCRIPTION));
		pRequest.setParameter(TRUEndecaConstants.KEYWORDS, pSeoTagMap.get(TRUEndecaConstants.KEYWORDS));
		pRequest.setParameter(TRUEndecaConstants.CANONICAL_URL, pSeoTagMap.get(TRUEndecaConstants.CANONICAL_URL));
		
		pRequest.serviceLocalParameter(TRUEndecaConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("END:: TRUSEODroplet.setSeoForAllCatPage()");
		}
	}
	
	/**
	 * Sets the seo for pdp page.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @param pSeoTagMap the seo tag map
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void setSeoForPdpPage(DynamoHttpServletRequest pRequest,  DynamoHttpServletResponse pResponse, Map<String,Object> pSeoTagMap) throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSEODroplet.setSeoForPdpPage()");
		}
		pRequest.setParameter(TRUEndecaConstants.PRODUCT_SEO_TITLE, pSeoTagMap.get(TRUEndecaConstants.PRODUCT_SEO_TITLE));
		pRequest.setParameter(TRUEndecaConstants.PRODUCT_SEO_H1_TEXT, pSeoTagMap.get(TRUEndecaConstants.PRODUCT_SEO_H1_TEXT));
		pRequest.setParameter(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION, pSeoTagMap.get(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION));
		pRequest.setParameter(TRUEndecaConstants.KEYWORDS, pSeoTagMap.get(TRUEndecaConstants.KEYWORDS));
		pRequest.setParameter(TRUEndecaConstants.CANONICAL_URL, pSeoTagMap.get(TRUEndecaConstants.CANONICAL_URL));
		pRequest.setParameter(TRUEndecaConstants.AGE_RANGE, pSeoTagMap.get(TRUEndecaConstants.AGE_RANGE));
		pRequest.setParameter(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION_DEFAULT, pSeoTagMap.get(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION_DEFAULT));
		pRequest.setParameter(TRUEndecaConstants.PRODUCT_SEO_TITLE_DEFAULT, pSeoTagMap.get(TRUEndecaConstants.PRODUCT_SEO_TITLE_DEFAULT));
		
		pRequest.serviceLocalParameter(TRUEndecaConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("END:: TRUSEODroplet.setSeoForPdpPage()");
		}
	}

	/**
	 * Gets the tru seo manager.
	 *
	 * @return the mTruSeoManager
	 */
	public TRUSEOManager getTruSeoManager() {
		return mTruSeoManager;
	}

	/**
	 * Sets the tru seo manager.
	 *
	 * @param pTruSeoManager the mTruSeoManager to set
	 */
	public void setTruSeoManager(TRUSEOManager pTruSeoManager) {
		mTruSeoManager = pTruSeoManager;
	}

}
