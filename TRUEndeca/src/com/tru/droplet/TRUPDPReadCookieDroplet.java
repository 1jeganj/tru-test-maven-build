package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.PDPBreadcrumbsVO;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class set the PDP BreadCrumb related data .
 * @author PA
 * @version 1.0
 *
 */
public class TRUPDPReadCookieDroplet extends DynamoServlet{

	/**  Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	/**
	 * This property holds CatalogProperties variable catalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * Gets the catalog tools.
	 *
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/** Property Holds TRUDimensionValueCache. */
	private TRUDimensionValueCache mDimensionValueCache;
	/**
	 * Returns TRUDimensionValueCache.
	 *
	 * @return the TRUDimensionValueCache.
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}
	/**
	 * sets TRUDimensionValueCache.
	 *
	 * @param pDimensionValueCache the TRUDimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		this.mDimensionValueCache = pDimensionValueCache;
	}
	/**
	 * Droplet to return  product breadcrumbs informations.
	 *
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPDPReadCookieDroplet.service()");
		}
		String pdpBreadcrumbCookie	= pRequest.getCookieParameter(TRUEndecaConstants.PDP_BREADCRUMB_REFRESH_COOKIE);
		String pdpRefreshCookie	= pRequest.getCookieParameter(TRUEndecaConstants.PDP_REFRESH_COOKIE);
		ArrayList<PDPBreadcrumbsVO> breadcrumbvolist = new ArrayList<PDPBreadcrumbsVO>();
		PDPBreadcrumbsVO breadcrumbVO = null;
		String breadcrumbValue = null;

		if(pdpRefreshCookie != null && (!pdpRefreshCookie.isEmpty()) && (!pdpRefreshCookie.equalsIgnoreCase(TRUEndecaConstants.NULL))) {
			breadcrumbValue = pdpRefreshCookie;
		} else if(StringUtils.isNotBlank(pdpBreadcrumbCookie) && (!pdpRefreshCookie.equalsIgnoreCase(TRUEndecaConstants.NULL))) {
			breadcrumbValue = pdpBreadcrumbCookie;
		}

		if(StringUtils.isNotBlank(breadcrumbValue))	{
			String[] splitedbreadcrumbArray=breadcrumbValue.split(TRUEndecaConstants.DOUBLE_COLON);
			String categoryID = null;
			for(int i = TRUEndecaConstants.INT_ZERO; i < splitedbreadcrumbArray.length; i++) {
				categoryID=splitedbreadcrumbArray[i];
				breadcrumbVO = populatePdpBreadcrumbsVO(categoryID);
				breadcrumbvolist.add(breadcrumbVO);
			}
		}
		if(breadcrumbvolist != null && !breadcrumbvolist.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug(" TRUPDPReadCookieDroplet.service():: breadcrumbvolist = {0}",breadcrumbvolist);
			}
			pRequest.setParameter(TRUEndecaConstants.BREADCRUMB_VO_LIST, breadcrumbvolist);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUPDPReadCookieDroplet.service()");
		}
	}

	/**
	 * populate the pdpBreadcrumbvo with  category details.
	 * @param pCategoryId
	 * 				- the category Id
	 * @return
	 * 		- PDPBreadcrumbsVO the Vo contains breadcrumb info.
	 */
	public PDPBreadcrumbsVO populatePdpBreadcrumbsVO(String pCategoryId)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUPDPReadCookieDroplet.populatePdpBreadcrumbsVO()");
		}
		PDPBreadcrumbsVO breadcrumbsVO= new PDPBreadcrumbsVO();
		String categoryDisplayName = null;
		String categoryURL = null;
		TRUDimensionValueCache dimCacheTools = null;
		DimensionValueCacheObject dimCacheObject = null;
		try {
			if (StringUtils.isNotBlank(pCategoryId))
			{
				dimCacheTools = getDimensionValueCache();
				categoryDisplayName=	getCatalogTools().getCategoryDisplayName(pCategoryId);

			}
			if(dimCacheTools != null)
			{
				dimCacheObject =	dimCacheTools.getCacheObject(pCategoryId);
			}
		} catch (RepositoryException e) {
			if(isLoggingError())
			{
				vlogError(e,"RepositoryException  TRUPDPReadCookieDroplet.service() method");
			}
		}
		if(dimCacheObject != null)
		{
			categoryURL = dimCacheObject.getUrl();
		}
		if (StringUtils.isNotBlank(pCategoryId)){

			breadcrumbsVO.setCategoryId(pCategoryId);
		}
		if (StringUtils.isNotBlank(categoryURL)){

			breadcrumbsVO.setCategoryURL(categoryURL);
		}
		if (StringUtils.isNotBlank(categoryDisplayName)){

			breadcrumbsVO.setCategoryName(categoryDisplayName);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUPDPReadCookieDroplet.populatePdpBreadcrumbsVO()");
		}
		return breadcrumbsVO;
	}
}

