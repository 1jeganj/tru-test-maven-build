package com.tru.droplet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.DynamoEnv;
import atg.nucleus.Nucleus;
import atg.service.dynamo.DAFConfiguration;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.endeca.constants.TRUEndecaConstants;
/**
 * 	This class used to get EAR and current server instance name.
 *  @author PA
 * 
 */
public class TRUFetchServerAndEarNameDroplet extends DynamoServlet{
	
	public static final String APP_DATE = "appDate";
	/**
	 * property to hold earPath 
	 */
	private String mEarPath;
	/**
	 * property to hold serverName 
	 */
	private String mServerName;
	/**
	 * property to hold DAFConfiguration instance
	 */
	private DAFConfiguration mDynamoConfiguration;
	
	/**
	 * Droplet to return server and ear  informations
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUFetchServerAndEarNameDroplet.service() method.. ");
		}
		String filePathForEARTemp = null;
		String earTRUString = null;
		String atgEARName = null;
		String atgServerName = null;
		
		String filePath = (String)Nucleus.getSystemNucleus().getRelativePathPrefix();
		boolean isStandalone = getDynamoConfiguration().isStandaloneAssembledEarFile();
		if (isLoggingDebug()) {
			logDebug("filePath:: "+filePath+" isStandalone: "+isStandalone);
		}
		if(isStandalone){
			if(!StringUtils.isEmpty(filePath)){
				if(filePath.contains(TRUEndecaConstants.TRU_STORE))	{
					filePathForEARTemp = filePath.substring(TRUEndecaConstants.INT_ZERO,filePath.lastIndexOf(TRUEndecaConstants.TRU_STORE));
				}
					atgServerName=filePath.substring(filePath.lastIndexOf(File.separator)+TRUEndecaConstants.INT_ONE,filePath.length());
			}
			if(!StringUtils.isEmpty(filePathForEARTemp)){
				  earTRUString = filePath.substring(filePathForEARTemp.lastIndexOf(File.separator)+TRUEndecaConstants.INT_ONE,filePath.length());  
			}
			if(!StringUtils.isEmpty(earTRUString)){
				 atgEARName = earTRUString.substring(TRUEndecaConstants.INT_ZERO, earTRUString.indexOf(File.separator));
			}
		}
		else{
			 atgServerName = DynamoEnv.getProperty(getServerName());
		}
		
		
		if (isLoggingDebug()) {
			logDebug("atgServerName :: "+atgServerName+" atgEARName: "+atgEARName);
		}
			pRequest.setParameter(TRUEndecaConstants.EAR_NAME, atgEARName);
		    pRequest.setParameter(TRUEndecaConstants.SERVER_NAME, atgServerName);
		    pRequest.setParameter(APP_DATE, new java.util.Date());
			pRequest.serviceLocalParameter(TRUEndecaConstants.OUTPUT_PARAM, pRequest, pResponse);
			if (isLoggingDebug()) {
				logDebug("END:: TRUFetchServerAndEarNameDroplet.service() method");
			}
	}
	

	/**
	 * @return the dynamoConfiguration
	 */
	public DAFConfiguration getDynamoConfiguration() {
		return mDynamoConfiguration;
	}


	/**
	 * @param pDynamoConfiguration the dynamoConfiguration to set
	 */
	public void setDynamoConfiguration(DAFConfiguration pDynamoConfiguration) {
		this.mDynamoConfiguration = pDynamoConfiguration;
	}


	/**
	 * @return the earPath
	 */
	public String getEarPath() {
		return mEarPath;
	}


	/**
	 * @param pEarPath the earPath to set
	 */
	public void setEarPath(String pEarPath) {
		mEarPath = pEarPath;
	}


	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return mServerName;
	}


	/**
	 * @param pServerName the serverName to set
	 */
	public void setServerName(String pServerName) {
		mServerName = pServerName;
	}

}
