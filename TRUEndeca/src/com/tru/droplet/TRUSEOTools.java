package com.tru.droplet;

import java.util.HashMap;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class used to get the SEO override value from seoTagOverride item descriptor.
 *
 * @author PA
 * @version 1.0
 */
public class TRUSEOTools extends GenericService{

	/** The m default catalog. */
	private String mDefaultCatalog;
	
	/** The m category. */
	private String mCategory;
	
	/** The m seo tag override. */
	private String mSeoTagOverride;
	
	/** The m home page seo tag override. */
	private String mHomePageSeoTagOverride;
	
	/** The m seo title. */
	private String mSeoTitle;
	
	/** The m seo h1 text. */
	private String mSeoH1Text;
	
	/** The m seo description. */
	private String mSeoDescription;
	
	/** The m canonicalUrl. */
	private String mCanonicalUrl;
	/** The m altUrl. */
	private String mAltUrl;
	
	/** Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 *  Property to hold mMinTargetAgeTruWebsites.
	 */
	private String mMfrSuggestedAgeMin;
	/**
	 * Property to hold mMaxTargetAgeTruWebsites.
	 */
	private String mMfrSuggestedAgeMax;
	
	/** 
	 * The product mSku. 
	 */
	private String mSku;
	/** The mSeoKeyWords. */
	private String mSeoKeyWords;
	
	/** 
	 * The product catalog. 
	 */
	private MutableRepository mProductCatalog;
	/**
	 * This property hold reference for CatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * Based on page type , this method get the data from seoTagOverride Item Descriptor
	 * and set into Map.
	 *
	 * @param pPage the page
	 * @param pId the id
	 * @param pSiteId the site id
	 * @return Map
	 */
	public Map<String,Object> fetchSEOTags(String pPage,String pId,String pSiteId)
	{
		if (isLoggingDebug()) {
			vlogDebug("Begin: TRUSEOTools.fetchSEOTags()  method.{0} :: pPage : "+pPage+" pId{1}: "+pId+" pSiteId{2}: "+pSiteId);
		}
		
		Map<String,Object> seoTagMap = new HashMap<String,Object>();
		if(!StringUtils.isEmpty(pPage)){
			if(pPage.equalsIgnoreCase(TRUEndecaConstants.CAT) || pPage.equalsIgnoreCase(TRUEndecaConstants.SUBCAT) || pPage.equalsIgnoreCase(TRUEndecaConstants.FAMILY)){
					seoTagMap = getCategorySeoOverride(pId);
			}else if(pPage.equalsIgnoreCase(TRUEndecaConstants.PDP)){
					seoTagMap = getSkuSeoOverride(pId);
			}else if(pPage.equalsIgnoreCase(TRUEndecaConstants.HOME)){
					seoTagMap = getHomePageSeoOverride(pId);
			}	
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUSEOTools.fetchSEOTags()  method : seoTagMap{0} :: "+seoTagMap);
		}
		return seoTagMap;	
	}
	
	
	/**
	 * Gets the home page seo override.
	 *
	 * @param pCatalogId the catalog id
	 * @return the home page seo override
	 */
	public Map<String, Object> getHomePageSeoOverride(String pCatalogId) {
		if (isLoggingDebug()) {
			vlogDebug("Begin:: TRUSEOTools.getHomePageSeoOverride()  method");
		}
		Map<String,Object> homePageSeoTagMap = null;
		if(!StringUtils.isEmpty(pCatalogId)){
			homePageSeoTagMap = new HashMap<String,Object>();
			try {
				Repository productCatalog =getProductCatalog();
				if(productCatalog == null){
					return homePageSeoTagMap;
				}
				RepositoryItem productCatalogRepository = productCatalog.getItem(pCatalogId, getCatalogProperties().getCatalogItemName());
					if(productCatalogRepository == null){
						return homePageSeoTagMap;
					}
					RepositoryItem repositoryItem = (RepositoryItem)productCatalogRepository.getPropertyValue(getHomePageSeoTagOverride());
						if(repositoryItem != null){	
							String homeSeoDescription = (String) repositoryItem.getPropertyValue(getSeoDescription());
							String homeSeoKeywords = (String) repositoryItem.getPropertyValue(getSeoKeyWords());
							String homeSeoTitle = (String) repositoryItem.getPropertyValue(getSeoTitle());
							String homeSeoH1 = (String) repositoryItem.getPropertyValue(getSeoH1Text());
							
							if(!StringUtils.isEmpty(homeSeoTitle)){
								homePageSeoTagMap.put(TRUEndecaConstants.HOME_SEO_TITLE, homeSeoTitle);
							}
							if(!StringUtils.isEmpty(homeSeoH1)){
								homePageSeoTagMap.put(TRUEndecaConstants.HOME_SEO_H1, homeSeoH1);
							}
							if(!StringUtils.isEmpty(homeSeoDescription)){
								homePageSeoTagMap.put(TRUEndecaConstants.HOME_PAGE_META_DESCRIPTION, homeSeoDescription);
							}
							if(!StringUtils.isEmpty(homeSeoKeywords)){
								homePageSeoTagMap.put(TRUEndecaConstants.KEYWORDS, homeSeoKeywords);
							}
							
						}
			}catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException,"Repository Exception occured while fetching the list of Catalogs TRUSEOTools.getHomePageSeoOverride() method for Home Page");		
				}
			}
		}
		return homePageSeoTagMap;
	}

	/**
	 * Gets the seo override for all type of category page(cat,family,brand,subcat).
	 *
	 * @param pCatId the catId
	 * @return the category seo override
	 */
	public Map<String,Object> getCategorySeoOverride(String pCatId)
	{
		if (isLoggingDebug()) {
			vlogDebug("Begin:: TRUSEOTools.getCategorySeoOverride()  method");
		}
		Map<String,Object> catSeoTagMap = null;
		if(!StringUtils.isBlank(pCatId)){
			catSeoTagMap = new HashMap<String,Object>();
			try {
				Repository productCatalog =getProductCatalog();
				if(productCatalog != null){
					RepositoryItem productCatalogRepository= productCatalog.getItem(pCatId, getCatalogProperties().getClassificationItemDescPropertyName());
					if(productCatalogRepository != null){
						RepositoryItem repositoryItem = (RepositoryItem)productCatalogRepository.getPropertyValue(getSeoTagOverride());
						if(repositoryItem != null){	
							String catSeoTitle = (String) repositoryItem.getPropertyValue(getSeoTitle());
							String catSeoH1Text = (String) repositoryItem.getPropertyValue(getSeoH1Text());
							String catSeoDescription = (String) repositoryItem.getPropertyValue(getSeoDescription());
							String catSeoKeywords = (String) repositoryItem.getPropertyValue(getSeoKeyWords());
							String canonicalUrl = (String) repositoryItem.getPropertyValue(getCanonicalUrl());
							String alternateUrl = (String) repositoryItem.getPropertyValue(getAltUrl());
					
							if(!StringUtils.isEmpty(catSeoTitle)){
								catSeoTagMap.put(TRUEndecaConstants.CAT_SEO_TITLE, catSeoTitle);
							}
							if(!StringUtils.isEmpty(catSeoH1Text)){
								catSeoTagMap.put(TRUEndecaConstants.CAT_SEO_H1_TEXT, catSeoH1Text);
							}
							if(!StringUtils.isEmpty(catSeoDescription)){
								catSeoTagMap.put(TRUEndecaConstants.CAT_SEO_DESCRIPTION, catSeoDescription);
							}
							if(!StringUtils.isEmpty(catSeoKeywords)){
								catSeoTagMap.put(TRUEndecaConstants.KEYWORDS, catSeoKeywords);
							}
							if(!StringUtils.isEmpty(canonicalUrl)){
								catSeoTagMap.put(TRUEndecaConstants.CANONICAL_URL, canonicalUrl);
							}
							if(!StringUtils.isEmpty(alternateUrl)){
								catSeoTagMap.put(TRUEndecaConstants.ALT_URL, alternateUrl);
							}
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException,"Repository Exception occured while fetching the list of Catalogs TRUSEOTools.getCategorySeoOverride() method for category");		
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUSEOTools.getCategorySeoOverride  method.. :: catSeoTagMap{0} : "+catSeoTagMap);
		}
		return catSeoTagMap;
	}
	
	
	/**
	 * Gets the seo override for sku.
	 *
	 * @param pOnlinePid the online pid
	 * @return the product seo override
	 */
	public Map<String,Object> getSkuSeoOverride(String pOnlinePid){
		if (isLoggingDebug()) {
			vlogDebug("Begin:: TRUSEOTools.getSkuSeoOverride()  method. ");
		}
		Map<String,Object> skuSeoTagMap = null;
		String skuId = null;
		String ageRange = null;
		String skuSeoTitle = null;
		String skuSeoH1Text = null;
		String skuSeoDescription = null;
		String skuSeoKeywords = null;
		String canonicalUrl = null;
		String defaultSeoTitle = null;
		String longDescripton = null;
		String alternateUrl = null;
		
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		
		if(!StringUtils.isBlank(pOnlinePid)){
			RepositoryItem[] skuList = getCatalogTools().getSKUFromOnlinePID(pOnlinePid);
			skuSeoTagMap = new HashMap<String,Object>();
			
			if(skuList == null){
				return null;
			}
			try {
				if(skuList.length > 0) {
					RepositoryItem skuItem = skuList[0];
					skuId = skuItem.getRepositoryId();
				}
				
				Repository productCatalog =getProductCatalog();
				if(productCatalog != null && !StringUtils.isEmpty(skuId)){
					RepositoryItem skuRepository= productCatalog.getItem(skuId, getSku());
					
					if(skuRepository != null){
						String minAge = (String)skuRepository.getPropertyValue(getMfrSuggestedAgeMin());
						String maxAge = (String)skuRepository.getPropertyValue(getMfrSuggestedAgeMax());
						
						ageRange = getCatalogTools().getMinAndMaxMFGAgeMessage(minAge, maxAge);
						
						if(!StringUtils.isEmpty(ageRange)){
							skuSeoTagMap.put(TRUEndecaConstants.AGE_RANGE, ageRange);
						}
						
						RepositoryItem repositoryItem = (RepositoryItem)skuRepository.getPropertyValue(getSeoTagOverride());
						if(repositoryItem != null){
							 skuSeoTitle = (String) repositoryItem.getPropertyValue(getSeoTitle());
							 skuSeoH1Text = (String) repositoryItem.getPropertyValue(getSeoH1Text());
							 skuSeoDescription = (String) repositoryItem.getPropertyValue(getSeoDescription());
							 skuSeoKeywords = (String) repositoryItem.getPropertyValue(getSeoKeyWords());
							 canonicalUrl = (String) repositoryItem.getPropertyValue(getCanonicalUrl());
							 alternateUrl = (String) repositoryItem.getPropertyValue(getAltUrl());
						}
							
							if(!StringUtils.isEmpty(skuSeoTitle)){
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_TITLE, skuSeoTitle);
							}else{
								defaultSeoTitle = (String)skuRepository.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_TITLE_DEFAULT, defaultSeoTitle);
							}
							
							if(!StringUtils.isEmpty(skuSeoH1Text)){
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_H1_TEXT, skuSeoH1Text);
							}else{
								defaultSeoTitle = (String)skuRepository.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_H1_TEXT, defaultSeoTitle);
							}
							
							if(!StringUtils.isEmpty(skuSeoDescription)){
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION, skuSeoDescription);
							}else{
								longDescripton = (String)skuRepository.getPropertyValue(catalogProperties.getProductLongDescription());
								skuSeoTagMap.put(TRUEndecaConstants.PRODUCT_SEO_DESCRIPTION_DEFAULT, longDescripton);
							}
							
							if(!StringUtils.isEmpty(skuSeoKeywords)){
								skuSeoTagMap.put(TRUEndecaConstants.KEYWORDS, skuSeoKeywords);
							}
							if(!StringUtils.isEmpty(canonicalUrl)){
								skuSeoTagMap.put(TRUEndecaConstants.CANONICAL_URL, canonicalUrl);
							}
							if(!StringUtils.isEmpty(alternateUrl)){
								skuSeoTagMap.put(TRUEndecaConstants.ALT_URL, alternateUrl);
							}
					}
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					vlogError(repositoryException,"Repository Exception occured while fetching the list of Catalogs TRUSEOTools.getProductSeoOverride() method for PDP");		
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUSEOTools.getSkuSeoOverride  method. :: productSeoTagMap{0} : "+skuSeoTagMap);
		}
		return skuSeoTagMap;
	}
	/**
	 * Gets the product catalog.
	 *
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets the product catalog.
	 *
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}
	
	/**
	 * Returns the this property hold reference for CatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the this property hold reference for CatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	
	/**
	 * Gets the default catalog.
	 *
	 * @return the default catalog
	 */
	public String getDefaultCatalog() {
		return mDefaultCatalog;
	}

	/**
	 * Sets the default catalog.
	 *
	 * @param pDefaultCatalog the new default catalog
	 */
	public void setDefaultCatalog(String pDefaultCatalog) {
		this.mDefaultCatalog = pDefaultCatalog;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public String getCategory() {
		return mCategory;
	}

	/**
	 * Sets the category.
	 *
	 * @param pCategory the new category
	 */
	public void setCategory(String pCategory) {
		this.mCategory = pCategory;
	}

	/**
	 * Gets the seo tag override.
	 *
	 * @return the seo tag override
	 */
	public String getSeoTagOverride() {
		return mSeoTagOverride;
	}

	/**
	 * Sets the seo tag override.
	 *
	 * @param pSeoTagOverride the new seo tag override
	 */
	public void setSeoTagOverride(String pSeoTagOverride) {
		this.mSeoTagOverride = pSeoTagOverride;
	}

	/**
	 * Gets the seo title.
	 *
	 * @return the seo title
	 */
	public String getSeoTitle() {
		return mSeoTitle;
	}

	/**
	 * Sets the seo title.
	 *
	 * @param pSeoTitle the new seo title
	 */
	public void setSeoTitle(String pSeoTitle) {
		this.mSeoTitle = pSeoTitle;
	}

	/**
	 * Gets the seo h1 text.
	 *
	 * @return the seo h1 text
	 */
	public String getSeoH1Text() {
		return mSeoH1Text;
	}

	/**
	 * Sets the seo h1 text.
	 *
	 * @param pSeoH1Text the new seo h1 text
	 */
	public void setSeoH1Text(String pSeoH1Text) {
		this.mSeoH1Text = pSeoH1Text;
	}
	/**
	 * Gets the seo description.
	 *
	 * @return the seo description
	 */
	public String getSeoDescription() {
		return mSeoDescription;
	}

	/**
	 * Sets the seo description.
	 *
	 * @param pSeoDescription the new seo description
	 */
	public void setSeoDescription(String pSeoDescription) {
		this.mSeoDescription = pSeoDescription;
	}

	/**
	 * Gets the sku.
	 *
	 * @return the sku
	 */
	public String getSku() {
		return mSku;
	}

	/**
	 * Sets the sku.
	 *
	 * @param pSku the new sku
	 */
	public void setSku(String pSku) {
		mSku = pSku;
	}


	/**
	 * Gets the seo key words.
	 *
	 * @return the seo key words
	 */
	public String getSeoKeyWords() {
		return mSeoKeyWords;
	}


	/**
	 * Sets the seo key words.
	 *
	 * @param pSeoKeyWords the new seo key words
	 */
	public void setSeoKeyWords(String pSeoKeyWords) {
		mSeoKeyWords = pSeoKeyWords;
	}

	/**
	 * Gets the Home Page SeoTag Override.
	 *
	 * @return the Home Page SeoTag
	 */
	public String getHomePageSeoTagOverride() {
		return mHomePageSeoTagOverride;
	}


	/**
	 * Sets the home page seo tag override.
	 *
	 * @param pHomePageSeoTagOverride the new home page seo tag override
	 */
	public void setHomePageSeoTagOverride(String pHomePageSeoTagOverride) {
		mHomePageSeoTagOverride = pHomePageSeoTagOverride;
	}


	/**
	 * @return the mCanonicalUrl.
	 */
	public String getCanonicalUrl() {
		return mCanonicalUrl;
	}


	/**
	 * @param pCanonicalUrl the mCanonicalUrl to set.
	 */
	public void setCanonicalUrl(String pCanonicalUrl) {
		this.mCanonicalUrl = pCanonicalUrl;
	}
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	/**
	 * Gets the mfr suggested age min.
	 *
	 * @return the mfr suggested age min
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * Sets the mfr suggested age min.
	 *
	 * @param pMfrSuggestedAgeMin the new mfr suggested age min
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		this.mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * Gets the mfr suggested age max.
	 *
	 * @return the mfr suggested age max
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * Sets the mfr suggested age max.
	 *
	 * @param pMfrSuggestedAgeMax the new mfr suggested age max
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}


	/**
	 * @return the altUrl
	 */
	public String getAltUrl() {
		return mAltUrl;
	}


	/**
	 * @param pAltUrl the altUrl to set
	 */
	public void setAltUrl(String pAltUrl) {
		mAltUrl = pAltUrl;
	}

}
