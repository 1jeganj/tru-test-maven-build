package com.tru.droplet;

import java.util.Map;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.TRUCommerceConstants;

/**
 * The Class TRUSeoManager.
 * @author PA
 */
public class TRUSEOManager extends GenericService{
	
	/** The tru seo tools. */
	private TRUSEOTools  mTruSeoTools;

	/**
	 * Fetch seo tags.
	 *
	 * @param pPage the page
	 * @param pId the id
	 * @param pSiteId the site id
	 * @return the map
	 */
	public Map<String,Object> fetchSEOTags(String pPage,String pId,String pSiteId)
	{
		Map<String,Object> seoTagMap = null;
		if(getTruSeoTools() != null){
			seoTagMap = getTruSeoTools().fetchSEOTags(pPage, pId, pSiteId);
		}
		return seoTagMap;	
	}
	
	/**
	 * Gets the catalog id for home page.
	 *
	 * @param pRequest the request
	 * @return the catalog id for home page
	 */
	protected String getCatalogIdForHomePage(DynamoHttpServletRequest pRequest){
		 RepositoryItem productCatalogRepository = (RepositoryItem)pRequest.getLocalParameter(TRUCommerceConstants.ID);
		 String id = null;
		 if(productCatalogRepository != null){
			 id =  productCatalogRepository.getRepositoryId();
		 }
		 return id;
	}
	/**
	 * Gets the tru seo tools.
	 *
	 * @return the truSeoTools
	 */
	public TRUSEOTools getTruSeoTools() {
		return mTruSeoTools;
	}

	/**
	 * Sets the tru seo tools.
	 *
	 * @param pTruSeoTools the truSeoTools to set
	 */
	public void setTruSeoTools(TRUSEOTools pTruSeoTools) {
		mTruSeoTools = pTruSeoTools;
	}
}
