package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class set the PDP BreadCrumb related data .
 * @author PA
 * @version 1.0
 *
 */
public class TRUPDPBreadcrumbsCookieDroplet extends DynamoServlet{

	/**
	 * Droplet to return  product breadcrumbs informations.
	 *
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPDPBreadcrumbsCookieDroplet.service()");
		}
		String pageName  = (String) pRequest.getLocalParameter(TRUEndecaConstants.URL_PAGENAME);
		String categoryId = (String) pRequest.getLocalParameter(TRUEndecaConstants.URL_CAT_ID);
		String abParam = (String) pRequest.getLocalParameter(TRUEndecaConstants.URL_AB);
		String cat = (String) pRequest.getLocalParameter(TRUEndecaConstants.CAT);
		StringBuffer bufferCokie = new StringBuffer();
		String pdpBreadcrumbCookie = pRequest.getCookieParameter(TRUEndecaConstants.PDP_BREADCRUMB_REFRESH_COOKIE);
		String pdpRefreshCookie = TRUEndecaConstants.EMPTY_STRING;
		if(StringUtils.isNotEmpty(categoryId)) {
			if(StringUtils.isNotEmpty(abParam) || (StringUtils.isNotEmpty(pageName) && pageName.equals(TRUEndecaConstants.CATEGORY_PAGE_NAME))) {
				bufferCokie.append(categoryId);
			} else if(StringUtils.isNotEmpty(pdpBreadcrumbCookie)) {
				String[] oldBreadcrumbs = pdpBreadcrumbCookie.split(TRUEndecaConstants.DOUBLE_COLON);
				for(int i = TRUEndecaConstants.INT_ZERO; i < oldBreadcrumbs.length;i++) {
					if(!categoryId.equals(oldBreadcrumbs[i])) {
						bufferCokie.append(oldBreadcrumbs[i]).append(TRUEndecaConstants.DOUBLE_COLON);
					}
				}
				bufferCokie.append(categoryId);
			} else {
				bufferCokie.append(categoryId);
			}
		} else if(((TRUEndecaConstants.PRODUCT).equals(pageName) || (TRUEndecaConstants.COLLECTION).equals(pageName)) && StringUtils.isNotEmpty(cat)) {
			
			pdpRefreshCookie = pRequest.getCookieParameter(TRUEndecaConstants.PDP_REFRESH_COOKIE);
			if(StringUtils.isEmpty(pdpRefreshCookie) && StringUtils.isNotEmpty(pdpBreadcrumbCookie)) {
				pdpRefreshCookie = pdpBreadcrumbCookie;
			}
		}

		Cookie pdpCookie = new Cookie(TRUEndecaConstants.PDP_BREADCRUMB_REFRESH_COOKIE, bufferCokie.toString());
		Cookie pdpRefCookie = new Cookie(TRUEndecaConstants.PDP_REFRESH_COOKIE, pdpRefreshCookie);
		pdpCookie.setPath(TRUEndecaConstants.PATH_SEPERATOR);
		pdpRefCookie.setPath(TRUEndecaConstants.PATH_SEPERATOR);


		pResponse.addCookie(pdpCookie);
		pResponse.addCookie(pdpRefCookie);

		if (isLoggingDebug()) {
			vlogDebug("TRUPDPBreadcrumbsCookieDroplet.service() method ..pageName :: {0} and categoryId :: {1} and abParam :: {2}", pageName, categoryId, abParam);
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUPDPBreadcrumbsCookieDroplet.service() method ..");
		}
	}
}
