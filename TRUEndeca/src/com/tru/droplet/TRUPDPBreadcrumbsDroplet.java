package com.tru.droplet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.catalog.CatalogNavHistory;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class set the PDP BreadCrumb related data .
 * @author PA
 * @version 1.0
 *
 */
public class TRUPDPBreadcrumbsDroplet extends DynamoServlet{
	
	/**
	 * This property hold reference for CatalogNavHistory.
	 */
	private CatalogNavHistory mCatalogNavHistory;
	
	/**
	 * Droplet to return  product breadcrumbs informations.
	 *
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
			if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUPDPBreadcrumbsDroplet.service()");
			}
		 Map<String,String> breadCrumbsMap = null;
		 List<Map<String,String>>  breadCrumbsMapList  = null;
		 String  jsonString   = (String)pRequest.getObjectParameter(TRUEndecaConstants.ALL_CATEGORY_MAP);
		 JSONObject json = null;
		 if(!StringUtils.isBlank(jsonString))
		 {
			 jsonString=  URLDecoder.decode(jsonString);
			 jsonString = jsonString.replaceAll(TRUEndecaConstants.AMPERSAND_QUOTE_SEMICOLON, TRUEndecaConstants.SLASH_QUOTE);
			 jsonString = jsonString.replaceAll(TRUEndecaConstants.NEW_LINE, TRUEndecaConstants.EMPTY_STRING);
			 jsonString = jsonString.replaceAll(TRUEndecaConstants.AMPERSAND_VAL, TRUEndecaConstants.AMPERSAND);
			 jsonString = jsonString.replaceAll(TRUEndecaConstants.HASH, TRUEndecaConstants.HASH_SYMBOL);
			 jsonString = jsonString.replaceAll(TRUEndecaConstants.PLUS, TRUEndecaConstants.PLUS_SYMBOL);
			Map<String,Object> categoryMap = new LinkedHashMap<String,Object>();
			try {
					json = new JSONObject(jsonString);
					if(json != null) 
						 {
						   categoryMap = jsonToMap(json);
						 }
				} catch (JSONException jsonException) {
					if (isLoggingError()) {
						vlogError(jsonException,"JsonException Exception TRUPDPBreadcrumbsDroplet.service() method");		
					}
				}
			  String value = null;
			  if(categoryMap != null && !categoryMap.isEmpty())
			  {
				 value =  getCategoryDetailsForBreadcrumbs(categoryMap); 
				 if (isLoggingDebug())
					{
						vlogDebug(" TRUPDPBreadcrumbsDroplet.service method: value .. {0}",value);
					}
			  }
			  if(!StringUtils.isBlank(value))
				{
				  value = value.replace(TRUEndecaConstants.LEFT_SQUARE_BRACKET, TRUEndecaConstants.EMPTY_STRING);
				  value = value.replace(TRUEndecaConstants.RIGHT_SQUARE_BRACKET, TRUEndecaConstants.EMPTY_STRING);
				  String dataArray[] = null;
				  breadCrumbsMapList  = new ArrayList();
				  if(value.contains(TRUEndecaConstants.PIPE_SYMBOL))
				  	{
					  value = value.replace(TRUEndecaConstants.PIPE_SYMBOL, TRUEndecaConstants.DOUBLE_UNDERSCORE);
					  dataArray = value.split(TRUEndecaConstants.DOUBLE_UNDERSCORE_COMMA);
				  	}
	
				  if(dataArray !=null )
				  {
					 for (String stringData : dataArray) 
					 {
							 breadCrumbsMap = populateBreadCrumbsMap(stringData);
							 if(breadCrumbsMap != null && !breadCrumbsMap.isEmpty())
							 {
								 breadCrumbsMapList.add(breadCrumbsMap);
							 }
					}
				}
			}
			  
			 if(breadCrumbsMapList != null && !breadCrumbsMapList.isEmpty())
			 {
			 	pRequest.setParameter(TRUEndecaConstants.BREADCRUMS_MAP_LIST, breadCrumbsMapList);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			 }
			if (isLoggingDebug())
			{
				vlogDebug(" TRUPDPBreadcrumbsDroplet.service method : breadCrumbsMapList .. {0}",breadCrumbsMapList);
			}
			if (isLoggingDebug())
			{
				logDebug("END:: TRUPDPBreadcrumbsDroplet.service() method ..");
			}
		}
		
	}
	
	/**
	 * This method returns the Breadcrumbs map.
	 * 
	 * @param pData the data
	 * @return dataMap , the map contains the category Details.
	 */
	public Map<String,String> populateBreadCrumbsMap(String pData)
	{
		Map<String,String> dataMap = null;
		String dataArray[] = null;
		String data = pData;
		if(!StringUtils.isBlank(data))
		{
			data = data.trim();
			 dataArray = data.split(TRUEndecaConstants.DOUBLE_UNDERSCORE);
			 if(dataArray != null && dataArray.length >= TRUEndecaConstants.INT_THREE )
			 {
				 dataMap = new LinkedHashMap<String,String>();
				 dataMap.put(TRUEndecaConstants.CATEGORY_ID, dataArray[TRUEndecaConstants.INT_ZERO]);
				 dataMap.put(TRUEndecaConstants.CATEGORY_URL, dataArray[TRUEndecaConstants.INT_ONE]);
				 dataMap.put(TRUEndecaConstants.CATEGORY_NAME, dataArray[TRUEndecaConstants.INT_TWO]);
			 }
		}
		return dataMap;
	}
	
	/**
	 * This method  returns the map.
	 *  
	 * @param pJsonObject the json object
	 * @return retMap, the  map
	 * @throws JSONException
	 * 					- JsonException occured
	 */
	public Map<String, Object> jsonToMap(JSONObject pJsonObject) throws JSONException
	{
		Map<String,Object> retMap = new LinkedHashMap<String, Object>();
	    if(pJsonObject != JSONObject.NULL) {
	        try {
				retMap = toMap(pJsonObject);
			} catch (JSONException e) {
				if (isLoggingError()) {
					vlogError(e,"JsonException Exception TRUPDPBreadcrumbsDroplet.jsonToMap() method");		
				}
			}
	    }
	    return retMap;
	}

	/**
	 * This method returns the Map.
	 * 
	 * @param pJsonObject
	 * 				- the Json object.
	 * @return 
	 * 		- the map
	 * @throws JSONException
	 * 						- JsonException occured
	 */
	public  Map<String, Object> toMap(JSONObject pJsonObject) throws JSONException 
	{
	    Map<String, Object> map = new LinkedHashMap<String, Object>();
	    Iterator<String> keysItr = pJsonObject.keys();
	    Object value = null;
	    while(keysItr.hasNext()) {
		        String key = keysItr.next();
		       
				try {
					value = pJsonObject.get(key);
					if(value instanceof JSONArray) 
			        {
			            value = toList((JSONArray) value);
			        }
			        else if(value instanceof JSONObject)
			        {
			            value = toMap((JSONObject) value);
			        }
				} catch (JSONException e) {
					if (isLoggingError()) {
						vlogError(e,"JsonException Exception TRUPDPBreadcrumbsDroplet.toMap() method");		
					}
				}
		        
		        map.put(key, value);
	    }
	   return map;
	}
	
	/**
	 * This method returns the List.
	 * 
	 * @param pJsonArray the json array.
	 * @return list
	 * @throws JSONException
	 * 						- JsonException occured
	 */
	public  List<Object> toList(JSONArray pJsonArray) throws JSONException
	{
	    List<Object> list = new ArrayList<Object>();
	    for(int i = 0; i < pJsonArray.length(); i++) 
	    {
	        Object value = pJsonArray.get(i);
	        if(value instanceof JSONArray)
	        {
	            value = toList((JSONArray) value);
	        }
	        else if(value instanceof JSONObject)
	        {
	            try {
					value = toMap((JSONObject) value);
				} catch (JSONException e) {
					if (isLoggingError()) {
						vlogError(e,"JsonException Exception TRUPDPBreadcrumbsDroplet.toList() method");		
					}
				}
	        }
	        list.add(value);
	    }
	    return list;
	}
	
	/**
	 * This method returns the breaddcrumbs details.
	 * 
	 * @param pCategoryMap
	 * 					- the categoryMap
	 * @return 
	 * 		- data, contains the category info.
	 */
	public String getCategoryDetailsForBreadcrumbs(Map<String,Object> pCategoryMap) {

		if (isLoggingDebug()) {
			logDebug("=====BEGIN=====:::TRUPDPBreadcrumbsDroplet.getCategoryIdForBreadcrumb method");
		}
		List navHistoryCollected = getCatalogNavHistory().getNavHistory();
		String navHistory = null;
		boolean flag = false;
		if(navHistoryCollected != null && !navHistoryCollected.isEmpty()) {
			navHistory = (String) navHistoryCollected.get(navHistoryCollected.size()-TRUEndecaConstants.CONSTANT_ONE);
		}
		String data = null;
		Set keys  = pCategoryMap.keySet();
		if(!StringUtils.isBlank(navHistory))
		{
			if(keys.contains(navHistory))
			{
				for (Map.Entry<String, Object> entry : pCategoryMap.entrySet())
				{
					 if(entry.getKey().equals(navHistory)) 
					 {
						 data = (String)entry.getValue();
						 flag = true;
						 break;
					 }
				}
			}
			else
			{
				for (Map.Entry<String, Object> entry : pCategoryMap.entrySet())
				{
					 data = (String)entry.getValue();
					 if(!StringUtils.isBlank(data) && data.contains(navHistory))
					 {
						 flag = true;
						 break;
					 }
					 
				}
				
				
			}
		}
		
		if(!flag)
		{
			for (Map.Entry<String, Object> entry : pCategoryMap.entrySet())
			{
				 data = (String)entry.getValue();
				 break;
			}
		}
		
		/*if(!StringUtils.isBlank(navHistory) && keys.contains(navHistory))
		{
			for (Map.Entry<String, Object> entry : pCategoryMap.entrySet())
			{
				 if(entry.getKey().equals(navHistory)) {
					 data = (String)entry.getValue();
					 break;
					}
			}
		}
		else
		{
			for (Map.Entry<String, Object> entry : pCategoryMap.entrySet())
			{
				 data = (String)entry.getValue();
				 break;
			}
		}*/
		if (isLoggingDebug()) {
			vlogDebug("======END=======:::TRUPDPBreadcrumbsDroplet.getCategoryIdForBreadcrumb method::data.. {0}",data);
		}
		
		return data;
	}
	
	
	/**
	 * Gets the CatalogNavHistory.
	 * 
	 * @return CatalogNavHistory
	 */
	public CatalogNavHistory getCatalogNavHistory() {
		return mCatalogNavHistory;
	}

	/**
	 * set the CatalogNavHistory.
	 * 
	 * @param pCatalogNavHistory
	 *            - CatalogNavHistory
	 */
	public void setCatalogNavHistory(CatalogNavHistory pCatalogNavHistory) {
		mCatalogNavHistory = pCatalogNavHistory;
	}
}
