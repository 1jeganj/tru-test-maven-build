package com.tru.droplet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.constants.TRUEndecaConstants;
/**
 * This class set the PDP BreadCrumb related data .
 * @author PA
 * @version 1.0
 *
 */
public class TRUConvertCategoryMapToJsonDroplet extends DynamoServlet{
	
	/**
	 * Droplet to convert categoryVOMap and to Json.
	 *
	 * @param pRequest the  request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		
		if (isLoggingDebug())
		{
			logDebug("BEGIN:: TRUConvertCategoryMapToJsonDroplet.service()");
		}
		Map<String,List<String>> allCategoryMap = (LinkedHashMap<String,List<String>>)pRequest.getObjectParameter(TRUEndecaConstants.ALL_CATEGORY_MAP);
		JSONObject json = null;
		if(allCategoryMap != null && !allCategoryMap.isEmpty())
		{
		    json = new JSONObject(allCategoryMap);
			if (isLoggingDebug()) 
			{
				vlogDebug(" TRUConvertCategoryMapToJsonDroplet.service method.. {0}",json);
			}
		}
		pRequest.setParameter(TRUEndecaConstants.ALL_CATEGORY_MAP_JSON, json);
		pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		if (isLoggingDebug()) 
		{
			logDebug("END:: TRUConvertCategoryMapToJsonDroplet.service() method");
		}
	}
}
