package com.tru.endeca.constants;

/**
 * The Class holds the all constants related to Endeca Module.
 * 
 * @version 1.0
 * @author Professional Access.
 */

public class TRUEndecaConstants {
	
	/**
	 * constant for holding colorName.
	 */
	//public static final String SKU_COLNAME = "colorName";
	/**
	 * constant for holding colorUpcLevel.
	 */
	//public static final String SKU_UPCLEVEL = "colorUpcLevel";
	/**
	 * constant for holding product.type.
	 */
	public static final String PROD_TYPE = "product.type";
	
	/**
	 * constant for holding defaultSortOption.
	 */
	public static final String DEFAULT_SORT_OPTION = "defaultSortOption";
	
	/**
	 * constant for holding COLLECTION_PRODUCT.
	 */
	public static final String COLLECTION_PRODUCT = "collectionProduct";
	
	/**
	 * constant for holding LINK_JSON.
	 */
	public static final String LINK_JSON = "/link.json";
	
	/**
	 * constant for holding ENDECA_CHANGE_SETS.
	 */
	public static final String ENDECA_CHANGE_SETS = "Endeca_change_sets";
	
	/**
	 * constant for holding ENDECA_USER_SEGMENTS.
	 */
	public static final String ENDECA_USER_SEGMENTS = "Endeca_user_segments";
	
	/**
	 * constant for holding productType.
	 */
	public static final String PRODUCT_TYPE = "productType";
	
	
	/**
	 * constant for holding PRODUCT_ANALYTICS_ID.
	 */
	public static final String PRODUCT_ANALYTICS_ID = "id";
	
	
	/**
	 * constant for holding onlinePID.
	 */
	public static final String ONLINE_PID = "onlinePID";
	
	/**
	 * constant for holding computedCatalogs.
	 */
	public static final String COMPUTED_CATALOGS = "computedCatalogs";
	
	/**
	 * constant for holding CONTEXT_CATALOG_VALUE.
	 */
	public static final String CONTEXT_CATALOG_VALUE = "atg.commerce.search.IndexConstants.CATALOG";
	
	
	/**
	 * constant for holding QUANTITY.
	 */
	public static final String QUANTITY = "quantity";
	
	/**
	 * constant for holding priceAscendingSort.
	 */
	//public static final String PRICE_ASCENDING_SORT = "priceAscendingSort";
	
	/**
	 * constant for holding priceDescendingSort.
	 */
	//public static final String PRICE_DESCENDING_SORT = "priceDescendingSort";
	
	/**
	 * constant for holding isPriceRange.
	 */
	//public static final String IS_PRICE_RANGE = "isPriceRange";
	
	/**
	 * constant for holding displaySinglePrice.
	 */
	//public static final String DISPLAY_SINGLE_PRICE = "displaySinglePrice";
	
	/**
	 * constant for holding MAX_AGE.
	 */
	//public static final String MAX_AGE = "maxAge";
	/**
	 * constant for holding MIN_AGE.
	 */
	//public static final String MIN_AGE = "minAge";
	/**
	 * constant for holding AGE_RANGE.
	 */
	public static final String AGE_RANGE = "ageRange";
	/**
	 * constant for holding EMPTY_SPACE.
	 */
	//public static final String EMPTY_SPACE = " ";
	/**
	 * constant for holding Strike-Through.
	 */
	public static final String STRIKE_THROUGH = "isStrikeThrough";
	
	/**
	 * constant for LEVEL_TWO_CATEGORY_MAP.
	 */
	public static final String LEVEL_TWO_CATEGORY_MAP ="twoLevelCategoryTreeMap";
	/**
	 * constant for LOCALE.
	 */
	public static final String LOCALE = "";
	/**
	 * Constant for Empty String.
	 */
	public static final String EMPTY_STRING = "";
	/**
	 * Constant for one.
	 */
	public static final int INT_ONE = 1;
	
	/**
	 * Constant for INT_THOUSAND.
	 */
	public static final int INT_THOUSAND = 1000;
	/**
	 * Constant for 9999.
	 */
	//public static final int INT_9999 = 9999;
	/**
	 * Constant for SWATCH_COLOR_DELIMETER.
	 */
	public static final String SWATCH_COLOR_DELIMETER = ":";
	/**
	 * Constant for Zero.
	 */
	public static final int INT_ZERO = 0;
	
	/**
	 * Constant for INT_FOUR_ZERO_FOUR.
	 */
	public static final int INT_FOUR_ZERO_FOUR = 404;
	
	/**
	 * Constant for SITE_TYPE.
	 */
	//public static final String SITE_TYPE = "siteType";
	/**
	 * Constant for Endeca Ntt parameter.
	 */
	public static final String NTT_PARAM = "keyword";
	/**
	 * Constant for SITE_TYPE_SD.
	 */
	//public static final String SITE_TYPE_SD = "sd";
	/**
	 * Constant for AUTO_CORRECTION.
	 */
	public static final String AUTO_CORRECTION = "autoCorrection";
	/**
	 * Constant for CONSTANT_MINUS_ONE.
	 */
	public static final int CONSTANT_MINUS_ONE = -1;
	/**
	 * The constant for YYYY-MM-DDHHmmss.
	 */
	public static final String DATE_MONTH_YEAR = "yyyy-MM-ddHHmmss";
	/**
	 * The constant for REQUEST_ACCEPT_HEADER_TEXT_JSON.
	 */
	//public static final String REQUEST_ACCEPT_HEADER_TEXT_JSON = "text/json";
	/**
	 * The constant for REQUEST_HEADER_ACCEPT.
	 */
	public static final String REQUEST_HEADER_ACCEPT = "Accept";
	/**
	 * The constant for REQUEST_ACCEPT_HEADER_APPLICATION_JSON.
	 */
	//public static final String REQUEST_ACCEPT_HEADER_APPLICATION_JSON = "application/json";
	/**
	 * Constant for Two.
	 */
	public static final int INT_TWO = 2;
	/**
	 * Constant for PRODUCT_CATEGORY_DIMENSION_NAME.
	 */
	public static final String PRODUCT_CATEGORY_DIMENSION_NAME = "product.category";
	
	/**
	 * Constant for DIMENSIONS.
	 */
	//public static final String DIMENSIONS = "dimensions";
	
	/**
	 * Constant for VALUE.
	 */
	//public static final String VALUE = "value";
	
	/**
	 * Constant for UNDERSCORE.
	 */
	public static final String UNDERSCORE = "_";
	/**
	 * Constant to hold Endeca XM Level 2 category Page URL.
	 */
	public static final String PLP_PAGE_PREFIX = "ProductListingPage";
	/**
	 * Constant to hold Endeca XM Root category Page URL.
	 */
	public static final String RCLP_PAGE_PREFIX = "RootCategoryListingPage";
	/**
	 * Constant to hold Endeca XM Level 1 category Page URL.
	 */
	public static final String SCLP_PAGE_PREFIX = "SubCategoryListingPage";
	/**
	 * Constant for UNDERSCORE_NPARAM.
	 */
	public static final String UNDERSCORE_NPARAM = "_/N-";
	/**
	 * Constant for QUESTION_MARK_SYMBOL.
	 */
	public static final String QUESTION_MARK_SYMBOL = "?";
	/**
	 * Property to hold String ASSEMBLER_CONTENT_COLLECTION.
	 */
	public static final String ASSEMBLER_CONTENT_COLLECTION = "assemblerContentCollection";
	/**
	 * Property to hold String ASSEMBLER_RULE_LIMIT.
	 */
	public static final String ASSEMBLER_RULE_LIMIT = "assemblerRuleLimit";

	/**
	 * Constant for content Item.
	 */
	public static final String CONTENT_ITEM_PARAMETER = "contentItem";
	/**
	 * Constant for error Key.
	 */
	public static final String ERROR_KEY = "@error";
	
	/**
	 * constants for SKU_listPrice.
	 */
	public static final String SKU_LISTPRICE = "sku.listPrice";
	/**
	 * constants for SKU_salePrice.
	 */
	public static final String SKU_SALEPRICE = "sku.salePrice";
	/**
	 * constants for sku.sortPrice.
	 */
	//public static final String SKU_SORTPRICE = "sku.activePrice";
	
	/**
	 * Property to hold String PRICE_ASCENDING_SORT_PARAMETER.
	 */
	public static final String PRICE_ASCENDING_SORT_PARAMETER = "priceAscendingSort";

	/**
	 * Property to hold String PRICE_DESCENDING_SORT_PARAMETER.
	 */
	public static final String PRICE_DESCENDING_SORT_PARAMETER = "priceDescendingSort";
	/**
	 * Property to hold String rootContentItem.
	 */
	public static final String ROOT_CONTENT_ITEM = "rootContentItem";
	/**
	 * Property to hold String endeca:redirect.
	 */
	public static final String ENDECA_REDIRECT = "endeca:redirect";
	/**
	 * Property to hold String link.
	 */
	public static final String LINK = "link";
	/**
	 * Constant for NRPP_PARAM.
	 */
	public static final String NRPP_PARAM = "Nrpp";
	/**
	 * Constant for NS_PARAM.
	 */
	public static final String NS_PARAM = "Ns";
	/**
	 * The constant for / symbol.
	 */
	public static final String PATH_SEPERATOR = "/";
	/**
	 * Property to hold String https.
	 */
	public static final String HTTPS = "https://";
	/**
	 * Property to hold String http.
	 */
	public static final String HTTP = "http://";
	
	/**
	 * constant for ROOT_CLASSIFICATION.
	 */
	public static final String  ROOT_CLASSIFICATION= "rootClassifications";
	/**
	 * constant for BREADCRUMBS.
	 */
	public static final String  BREADCRUMBS= "breadCrumbs";
	/**
	 * constant for PRODUCTION_URL.
	 */
	public static final String  PRODUCTION_URL= "productionURL";
	
	/**
	 * constant for SKU_SITEID.
	 */
	public static final String  SKU_SITEID= "sku.siteId";
	
	/**
	 * constant for AMPERSAND_N_EQUALS.
	 */
	public static final String  AMPERSAND_N_EQUALS= "&N=";
	
	/**
	 * constant for QUESTION_CATEGORYID_EQUALS.
	 */
	public static final String  QUESTION_CATEGORYID_EQUALS= "?categoryid=";
	/**
	 * constant for CATEGORYID_EQUALS.
	 */
	public static final String  CATEGORYID_EQUALS= "categoryid=";
	
	/** constant for parentClassificationMap. */
	public static final String  PARENT_CLASSIFICATION_MAP= "parentClassificationMap";
	
	/**
	 * Constant for ERROR_MSG_PDP_BREADCRUMB.
	 */
	public static final String ERROR_MSG_PDP_BREADCRUMB =
			"No record state found. Check the configuration for this cartridge.";
	
	/**
	 * Property to hold String PDP_BREADCRUMBS_PRODUCTID.
	 */
	public static final String PDP_BREADCRUMBS_PRODUCTID = "productId";
	
	/**
	 * Property to hold String PRODUCT_REPOSITORYID.
	 */
	public static final String PRODUCT_REPOSITORYID = "product.repositoryId";
	
	/**
	 * Constant for PROD_ID.
	 */
	public static final String PROD_ID = "prodId";
	
	/**
	 * Property to hold String PDP_BREADCRUMBS_PRODUCT_DISPLAYNAME.
	 */
	public static final String PDP_BREADCRUMBS_PRODUCT_DISPLAYNAME = "product.displayName";
	
	/**
	 * Property to hold String PDP_PRODUCT_DISPLAYNAME.
	 */
	public static final String PRODUCT_DISPLAYNAME = "productDisplayName";
	
	
	/**
	 * Property to hold String PDP_BREADCRUMB_LIST.
	 */
	public static final String PDP_BREADCRUMB_LIST = "PDPBreadCrumbList";
	
	/**
	 * Constant for CATEGORY_REPOSITORYID.
	 */
	public static final String CATEGORY_REPOSITORYID = "category.repositoryId";
	
	
	
	
	/**
	 * Constant for double constant 0.
	 */
	public static final double DOUBLE_CONSTANT_ZERO = 0.0D;
	
	/**
	 * Constant for Zero.
	 */
	public static final int CONSTANT_ZERO = 0;
	
	/**
	 * Constant for one.
	 */
	public static final int CONSTANT_ONE = 1;
	
	/**
	 * Constant for two.
	 */
	//public static final int CONSTANT_TWO = 2;
	/**
	 * Constant for three.
	 */
	//public static final int CONSTANT_THREE = 3;
	
	
	
	
	/**
	 * constant for allClassificationMap.
	 */
	//public static final String  ALL_CLASSIFICATION_MAP= "allClassificationMap";
	
	/**
	 * constant for ACTIVE.
	 */
	public static final String  ACTIVE= "ACTIVE";
		/**
	 * constant for DIMVAL_USERTYPE_ID.
	 */
	public static final String  DIMVAL_USERTYPE_ID= "dimval.prop.category.userTypeId";
	
	/**
	 * constant for TAXONOMYCATEGORY.
	 */
	public static final String  TAXONOMYCATEGORY= "TAXONOMYCATEGORY";
	
	/**
	 * constant for TAXONOMYSUBCATEGORY.
	 */
	public static final String  TAXONOMYSUBCATEGORY= "TAXONOMYSUBCATEGORY";
	
	/**
	 * constant for TAXONOMYNODE.
	 */
	public static final String  TAXONOMYNODE= "TAXONOMYNODE";
	
	/**
	 * constant for TAXONOMYFILTEREDLANDINGPAGE.
	 */
	public static final String  TAXONOMYFILTEREDLANDINGPAGE= "TAXONOMYFILTEREDLANDINGPAGE";
	
	/**
	 * property to hold min listPrice constant.
	 */
	public static final String MIN_PRICE = "minimumPrice";
	/**
	 * property to hold max listPrice constant.
	 */
	public static final String MAX_PRICE = "maximumPrice";
	/**
	 * property to hold min salePrice constant.
	 */
	//public static final String MIN_SALEPRICE = "minimumSalePrice";
	/**
	 * property to hold max salePrice constant.
	 */
	//public static final String MAX_SALEPRICE = "maximumSalePrice";
	
	/** Constant to hold ONE .*/
	// public static final int ONE = 1;
	 
	 /** The property to double zero. */
	//public static final double ZERO=0.0;
	/**
	 * Constant for Three.
	 */
	//public static final int INT_THREE = 3;
	/**
	 * property to hold childSKU's property.
	 */ 
	//public static final String CHILD_SKUS = "childSKUs";
	/**
	 * property to hold CURRENT_CAT_ID property.
	 */ 
	public static final String CURRENT_CAT_ID = "CurrentCategoryId";
	
	/**
	 * property to hold CATEGORY_ID property.
	 */ 
	public static final String CATEGORY_ID = "categoryid";
	
	/**
	 * property to hold PATTERN_QUESTION_MARK property.
	 */ 
	public static final String PATTERN_QUESTION_MARK = "\\?";
	
	/**
	 * property to hold COLLECTION_NAME property.
	 */ 
	public static final String COLLECTION_NAME = "CollectionName";
	/**
	 * property to hold COLLECTION_DESC property.
	 */ 
	
	public static final String COLLECTION_DESC="CollectionDesc";
	/**
	 * property to hold COLLECTION_IAMGE_URL property.
	 */ 
	public static final String COLLECTION_IAMGE_URL="CollectionImageUrl";
	
	/**
	 * property to hold CLASSIFICATION_HERO_BANNER property.
	 */ 
	public static final String CLASSIFICATION_HERO_BANNER="ClassificationHeroBanner";
	
	/**
	 * property to hold CLASSIFICATION_SEO_TEXT property.
	 */ 
	public static final String CLASSIFICATION_SEO_TEXT="ClassificationSeoText";
	/**
	 * property to hold CLASSIFICATION property.
	 */ 
	//public static final String CLASSIFICATION ="classification";
	
	/**
	 * property to hold REGISTRY_CLASSIFICATION property.
	 */ 
	//public static final String REGISTRY_CLASSIFICATION ="registryClassification";
	/**
	 * property to hold HIDDEN property.
	 */ 
	public static final String HIDDEN ="HIDDEN";
	/**
	 * property to hold SERACH property.
	 */ 
	//public static final String SERACH ="/search";
	
	/**
	 * property to hold the rest context path.
	 */
	public static final String REST_CONTEXT_PATH = "/rest";
	
	/**
	 * property to hold the rest parameter.
	 */
	public static final String REST_DEPTH ="depth";
	
	/**
	 * property to hold CURRENT_CATEGORY_NAME property.
	 */ 
	public static final String CURRENT_CAT_NAME = "CurrentCategoryName";
	
	/**
	 * Constant for OUTPUT PARAM.
	 */
	public static final String OUTPUT_PARAM = "output";
	
	/**
	 * Constant for Allow EMPTY_PARAM.
	 */

	public static final String EMPTY_PARAM = "empty";
	
	/**
	 * property to hold DIMENSION_ID property.
	 */ 
	public static final String DIMENSION_ID = "dimensionId";
	
	/**
	 * property to hold NAVIGATION_ZERO property.
	 */ 
	public static final String NAVIGATION_ZERO = "N=0";
	
	/**
	 * property to hold ENCODING_FORMATTER property.
	 */ 
	public static final String ENCODING_FORMATTER = "UTF-8";
	
	/**
	 * property to hold PRODUCT_SITE_ID property.
	 */ 
	public static final String PRODUCT_SITE_ID = "product.siteId";
	
	/**
	 * constants for SKU_SITE_ID.
	 */
	public static final String SKU_SITE_ID = "sku.siteId";
	
	/**
	 * constants for DimensionsList.
	 */
	public static final String DIMENSIONS_LIST = "dimensionsList";
	
	/**
	 * constants for HasPriceRange.
	 */
	public static final String HAS_PRICE_RANGE = "hasPriceRange";
	
	/**
	 * constants for true.
	 */
	public static final String TRUE = "true";
	
	/**
	 * constants for false.
	 */
	public static final String FALSE = "false";
	/**
	 * constants for sku.activePrice.
	 */
	public static final String SKU_ACTIVEPRICE = "sku.activePrice";
	/**
	 * constants for sku.isNew.
	 */
	public static final String SKU_ISNEW = "sku.isNew";
	
	/**
	 * constants for siteConfiguration.
	 */
	//public static final String SITE_CONFIGURATION = "siteConfiguration";
	
	/**
	 * constants for id=?0.
	 */
	//public static final String ID_0 = "id=?0";
	/**
	 * constants for X-Requested-With.
	 */
	public static final String X_REQUESTED_WITH = "X-Requested-With";
	/**
	 * constants for XMLHttpRequest.
	 */
	public static final String XML_HTTP_REQUEST = "XMLHttpRequest";
	/**
	 * constants for sku.
	 */
	public static final String SKU = "sku";
	/**
	 * constants for nonMerchSKU.
	 */
	public static final String NON_MERCH_SKU = "nonMerchSKU";
	/**
	 * constants for Sponsored.
	 */
	//public static final String SPONSORED = "Sponsored";
	/**
	 * constants for R Exclusive.
	 */
	//public static final String R_EXCLUSIVE = "R Exclusive";
	/**
	 * constants for No.
	 */
	public static final String NO = "No";
	/**
	 * constants for _USA_PARAMETRIC_PROFILE.xml.
	 */
	public static final String USA_PARAMETRIC_PROFILE = "_USA_PARAMETRIC_PROFILE.xml";
	/**
	 * constants for TRU_STORE.
	 */
	public static final String TRU_STORE= "trustore";
	/**
	 * constants for TRU_STORE.
	 */
	public static final String CAT= "cat";
	/**
	 * constants for TRU_STORE.
	 */
	public static final String SUBCAT= "subcat";
	/**
	 * constants for TRU_STORE.
	 */
	public static final String FAMILY= "family";
	/**
	 * constants for TRU_STORE.
	 */
	//public static final String SHOPBY= "shopby";
	/**
	 * constants for PDP.
	 */
	public static final String PDP = "pdp";
	/**
	 * constants for CAT_SEO_TITLE.
	 */
	
	public static final String CAT_SEO_TITLE = "catSeoTitle";
	
	/**
	 * constants for CAT_SEO_H1_TEXT.
	 */
	
	public static final String CAT_SEO_H1_TEXT = "catSeoH1Text";
	/**
	 * constants for CAT_SEO_DESCRIPTION.
	 */
	
	public static final String CAT_SEO_DESCRIPTION = "catSeoDescription";
	/**
	 * constants for PRODUCT_SEO_TITLE.
	 */
	
	public static final String PRODUCT_SEO_TITLE = "productSeoTitle";
	
	/**
	 * constants for PRODUCT_SEO_TITLE_DEFAULT.
	 */
	
	public static final String PRODUCT_SEO_TITLE_DEFAULT = "productSeoTitleDefault";
	/**
	 * constants for PRODUCT_SEO_H1_TEXT.
	 */
	
	public static final String PRODUCT_SEO_H1_TEXT = "productSeoH1Text";
	/**
	 * constants for PRODUCT_SEO_DESCRIPTION.
	 */
	
	public static final String PRODUCT_SEO_DESCRIPTION = "productSeoDescription";
	/**
	 * constants for PRODUCT_SEO_DESCRIPTION_DEFAULT.
	 */
	
	public static final String PRODUCT_SEO_DESCRIPTION_DEFAULT = "productSeoDescriptionDefault";
	/**
	 * constants for CANONICAL_URL.
	 */
	
	public static final String CANONICAL_URL = "canonicalUrl";
	/**
	 * constants for HOME_PAGE_META_DESCRIPTION.
	 */
	public static final String HOME_PAGE_META_DESCRIPTION = "homePageMetaDescription";
	/**
	 * constants for EAR_NAME.
	 */
	
	public static final String EAR_NAME = "earName";
	/**
	 * constants for SERVER_NAME.
	 */
	
	public static final String SERVER_NAME = "serverName";
	/**
	 * constants for HOME.
	 */
	
	public static final String HOME = "home";
	/**
	 * constants for HOME_SEO_TITLE.
	 */
	
	public static final String HOME_SEO_TITLE = "homeSeoTitle";
	
	/**
	 * constants for HOME_SEO_H1.
	 */
	
	public static final String HOME_SEO_H1 = "homeSeoH1";
	/**
	 * constants for KEYWORDS.
	 */
	public static final String KEYWORDS = "keywords";
	/**
	 * constants for STRING_COMMA.
	 */
	//public static final String STRING_COMMA = ",";
	
	/**
	 * Constant for DIMENSION_SEARCH_GROUPS.
	 */
	public static final String DIMENSION_SEARCH_GROUPS = "dimensionSearchGroups";
	
	/**
	 * constants for SOS_LOGIN_URL.
	 */
	public static final String SOS_LOGIN_URL = "sos/index.jsp";
	
	/**
	 * constants for TRU_HOME.
	 */
	public static final String TRU_HOME = "/home";
	
	/**
	 * constants for EQUALS.
	 */
	public static final String EQUALS = "=";
	
	/**
	 * constants for REMAINING_CATEGORIES.
	 */
	
	
	public static final String REMAINING_CATEGORIES = "remainingCategories";
	
	/**
	 * constants for DISPLAY_SEE_MORE_CATEGORIES .
	 */
	
	public static final String DISPLAY_SEE_MORE_CATEGORIES = "displaySeeMoreCategory";
	
	
	/**
	 * constants for INVENTORY_FIRST_AVAILABLE_DATE .
	 */
	
	//public static final String INVENTORY_FIRST_AVAILABLE_DATE = "inventoryFirstAvailableDate";
	
	/**
	 * constants for SIMPLE_DATE_FORMAT .
	 */
	public static final String SIMPLE_DATE_FORMAT = "yy/MM/dd HH:mm:ss";
	
	/**
	 * constants for PRIMARY_IMAGE .
	 */
	//public static final String PRIMARY_IMAGE = "primaryImage";
	
	/**
	 * constants for COLOR_CODE.
	 */
	//public static final String COLOR_CODE = "colorCode";
	/**
	 * constants for SWATCH_IMAGE .
	 */
	//public static final String SWATCH_IMAGE = "swatchImage";
	
	/**
	 * constants for SEMI_COLON .
	 */
	public static final String SEMI_COLON  = ";";
	
	/**
	 * constants for PIPE_SYMBOL .
	 */
	public static final String PIPE_SYMBOL  = "||";
	/**
	 * constants for SKU_INVENTORY_AVAILABLE_DAYS .
	 */
	public static final String SKU_INVENTORY_AVAILABLE_DAYS  = "sku.inventoryAvailableDays";
	
	/**
	 * constants for AND_FORWARD_PARENTHESES .
	 */
	public static final String AND_FORWARD_PARENTHESES  = "AND(";
	
	/**
	 * constants for BACK_PARENTHESES .
	 */
	public static final String BACK_PARENTHESES  = ")";
	
	/**
	 * constants for BACK_PARENTHESES .
	 */
	public static final String ONE_STR  = "1";
	
	/**
	 * constants for USER_AGENT .
	 */
	public static final String USER_AGENT = "User-Agent";
	
	/**
	 * constants for BACK_PARENTHESES .
	 */
	public static final String MOZILLA_COMPATIBLE  = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";
	/**
	 * constants for IS_DELETED .
	 */
	//public static final String IS_DELETED  = "isDeleted";
	/**
	 * constants for WEB_DISPLAY .
	 */
	//public static final String WEB_DISPLAY  = "webDisplayFlag";
	/**
	 * constants for REGULAR_SKU .
	 */
	public static final String REGULAR_SKU  = "regularSKU";
	/**
	 * constants for CONTENTS .
	 */
	public static final String CONTENTS  = "contents";
	/**
	 * constants for MAIN_PRODUCT .
	 */
	public static final String MAIN_PRODUCT  = "MainProduct";
	/**
	 * constants for MAIN_CONTENT .
	 */
	public static final String MAIN_CONTENT  = "MainContent";
	/**
	 * constants for TOTAL_NUM_RECS .
	 */
	public static final String TOTAL_NUM_RECS  = "totalNumRecs";
	/**
	 * constants for SKU_DISPLAYNAME .
	 */
	public static final String SKU_DISPLAYNAME  = "sku.displayName";
	/**
	 * constants for SKUDISPLAYNAME .
	 */
	public static final String SKUDISPLAYNAME  = "skuDisplayName";
	/**
	 * constants for SKU_LONG_DESC .
	 */
	public static final String SKU_LONG_DESC  = "sku.longDescription";
	/**
	 * constants for SKU_LONG_DESC .
	 */
	public static final String SKU_REPOSITORY_ID  = "sku.repositoryId";
	/**
	 * constants for SKU_LONG_DESC .
	 */
	public static final String SKU_ID  = "skuId";
	/**
	 * constants for SKULONGDESC .
	 */
	public static final String SKULONGDESC  = "skuLongDescription";
	
	/**
	 * constants for UTF-8 .
	 */
	//public static final String 	UTF_EIGHT  = "UTF-8";
	/**
	 * constants for AMPERSAND .
	 */
	public static final String 	AMPERSAND  = "&";
	
	/**
	 * constants for AMPERSAND_VAL .
	 */
	public static final String 	AMPERSAND_VAL  = "ampersand";

	/**
	 * constants for AB_PARAM .
	 */
	public static final String 	AB_PARAM  = "ab";
	
	/**
	 * constants for AB_URL .
	 */
	
	public static final String 	AB_URL  = "ab=";
	/**
	 * Constant for N.
	 */
	public static final String N_PARAMETER = "N";
	/**
	 * Constant for N_EQUALS.
	 */
	public static final String N_EQUALS = "N=";
	
	/**
	 * Constant for YES.
	 */
	//public static final String YES = "Yes";
	/**
	 * Constant for Y.
	 */
	//public static final String Y = "Y";
	/**
	 * Constant for YES.
	 */
	//public static final String NO_VAL = "No";
	/**
	 * Constant for Y.
	 */
	//public static final String N_VAL = "N";
	
	/** Constant for RECORD_SPEC. */
	public static final String RECORD_SPEC = "recordspec";
	
	/**
	 * constants for sku.isPreSellable.
	 */
	public static final String SKU_ISPRESELLABLE = "sku.isPreSellable";
	
	
	/**
	 * constants for DISPLAY_STATUS.
	 */
	public static final String DISPLAY_STATUS = "dimval.prop.category.displayStatus";
	
	/** The Constant GET_METHOD. */
	public static final String GET_METHOD = "GET";
	
	/** The Constant RESPONSE_CODE_200. */
	public static final int RESPONSE_CODE_200 = 200;
	/**
	 * Constant for CATEGORY_SITEIDS
	 */
	public static final String CATEGORY_SITEIDS = "category.siteId";
	
	/**
	 * Constant for NEW_LINE
	 */
	public static final String NEW_LINE = "\n";
	
	/**
	 * Constant for AMPERSAND_QUOTE_SEMICOLON
	 */
	public static final String AMPERSAND_QUOTE_SEMICOLON = "&quot;";
	/**
	 * Constant for SLASH_QUOTE
	 */
	public static final String SLASH_QUOTE = "\"";
	
	/**
	 * Constant for DOUBLE_UNDERSCORE
	 */
	public static final String DOUBLE_UNDERSCORE = "__";
	/**
	 * Constant for DOUBLE_UNDERSCORE_COMMA
	 */
	public static final String DOUBLE_UNDERSCORE_COMMA = "__,";
	
	/**
	 * Constant for INT_THREE.
	 */
	public static final int INT_THREE = 3;
	
	/**
	 * Constant for CATEGORY_URL.
	 */
	public static final String CATEGORY_URL = "categoryURL";
	
	/**
	 * property to hold CATEGORY_NAME property.
	 */ 
	public static final String CATEGORY_NAME = "categoryName";
	
	/**
	 * property to hold LEFT_SQUARE_BRACKET property.
	 */ 
	public static final String LEFT_SQUARE_BRACKET = "[";
	/**
	 * property to hold RIGHT_SQUARE_BRACKET property.
	 */ 
	public static final String RIGHT_SQUARE_BRACKET = "]";
	/**
	 * constant for HASH_SYMBOL.
	 */
	public static final String HASH_SYMBOL = "#";	
	
	/**
	 * constant for HASH.
	 */
	public static final String HASH = "Hash";	
	
	/**
	 * constant for PLUS_SYMBOL.
	 */
	public static final String PLUS_SYMBOL = "+";	
	
	/**
	 * constant for PLUS.
	 */
	public static final String PLUS = "Plus";
	
	/**
	 * constant for ALL_CATEGORY_MAP.
	 */
	public static final String ALL_CATEGORY_MAP = "allCategoryMap";
	
	/**
	 * constant for ALL_CATEGORY_MAP_JSON.
	 */
	public static final String ALL_CATEGORY_MAP_JSON = "allCategoryMapJson";
	
	/**
	 * constant for ALL_CATEGORY_MAP_JSON.
	 */
	public static final String BREADCRUMS_MAP_LIST = "breadCrumbsMapList";
	
	/**
	 * Constant forZERO
	 */
	public static final int ZERO = 0;
	/** 
	 * Constant for PROD_ID_ARRAY
	 */
	public static final String PROD_ID_ARRAY = "prodIdArray";
	
	/** 
	 * Constant for SKU_ID_ARRAY
	 */
	public static final String SKU_ID_ARRAY = "skuIdArray";
	
	/** 
	 * Constant for PDP_BREADCRUMB_REFRESH_COOKIE
	 */
	public static final String PDP_BREADCRUMB_REFRESH_COOKIE = "pdpBreadcrumbRefreshCookie";
	
	/** 
	 * Constant for PDP_BREADCRUMB_REFRESH_COOKIE
	 */
	public static final String PDP_REFRESH_COOKIE = "pdpRefreshCookie";
	
	/** 
	 * Constant for BREADCRUMB_VO_LIST
	 */
	public static final String BREADCRUMB_VO_LIST = "breadcrumbvolist";
	
	
	/** 
	 * Constant for CATEGORY_PAGE_NAME
	 */
	public static final String CATEGORY_PAGE_NAME = "category";
	/** 
	 * Constant for URL_AB
	 */
	public static final String URL_AB = "urlAB";
	/** 
	 * Constant for URL_CAT_ID
	 */
	public static final String URL_CAT_ID = "urlCatID";
	/** 
	 * Constant for URL_PAGENAME
	 */
	public static final String URL_PAGENAME = "urlPagename";
	
	/** 
	 * Constant for DOUBLE_COLON
	 */
	public static final String DOUBLE_COLON = "::";
	
	/** 
	 * Constant for PRODUCT
	 */
	public static final String PRODUCT = "product";
	
	/** 
	 * Constant for COLLECTION
	 */
	public static final String COLLECTION = "collection";
	
	/** 
	 * Constant for TEMP_COOKIE
	 */
	public static final String TEMP_COOKIE = "tempCookie";
	
	
	/** 
	 * Constant for AMPERSAND_CAT_EQUAL_ONE
	 */
	public static final String AMPERSAND_CAT_EQUAL_ONE = "&cat=1";
	/** 
	 * Constant for PROMOTION
	 */
	public static final String PROMOTION = "Promotion";
	/** 
	 * Constant for GWP_PROMOTION
	 */
	public static final String GWP_PROMOTION = "GWPPromotion";
	
	/** 
	 * Constant for PROMOTION_ID
	 */
	public static final String PROMOTION_ID = "promoid";
	
	/** 
	 * Constant for GWP_PROMOTION_ID
	 */
	public static final String GWP_PROMOTION_ID = "gwppromoid";
	/** 
	 * Constant for PROMOTION_UNDERSCORE
	 */
	public static final String PROMOTION_UNDERSCORE = "Promotion_";
	/** 
	 * Constant for GWP_PROMOTION_UNDERSCORE
	 */
	public static final String GWP_PROMOTION_UNDERSCORE = "GWPPromotion_";
	
	/** 
	 * Constant for PROMOTION_URL
	 */
	public static final String PROMOTION_URL = "/promotion";
	
	/**
	 Constant for ASSEMBLER
*/
	public static final String ASSEMBLER = "assembler";

	/** 
	 * Constant for PRODUCT_URL
	 */
	public static final String PRODUCT_URL = "/product";
	/** 
	 * Constant for PARENT_PRODUCT_ID
	 */
	public static final String PARENT_PRODUCT_ID = "parentProductId";
	/** 
	/** 
	 * Constant for IS_STYLIZED
	 */
	public static final String IS_STYLIZED = "isStylized";
	/** 
	 * Constant for IS_COLLECTION_PRODUCT
	 */
	public static final String IS_COLLECTION_PRODUCT = "isCollectionProduct";
	/** 
	 * Constant for COLLECTION_SKUS
	 */
	public static final String COLLECTION_SKUS = "collectionSkus";
	/** 
	 * Constant for DISPLAY_NAME
	 */
	public static final String DISPLAY_NAME = "displayName";
	/** 
	 * Constant for COLLECTION_IMAGE
	 */
	public static final String COLLECTION_IMAGE = "collectionImage";
	
	/**
     * constants for COLLECTION_SKU_LISTSIZE.
     */
     public static final String COLLECTION_SKU_LISTSIZE = "skuListSize";
 	/**
      * constants for CUSTOM_ROOLUP_KEY.
      */
      public static final String CUSTOM_ROOLUP_KEY = "customrollupKey";
   	/**
       * constants for COLLECTION_ID.
       */
       public static final String COLLECTION_ID = "id";
    

	/** 
	 * Constant for NULL
	 */
	public static final String NULL = "null";
	/**
	 * Constant for Allow ERROR_INFO.
	 */
	public static final String ERROR_INFO = "errorInfo";
	/**
	 * Constant for ERR_ENTER_VALID_SITE.
	 */
	public static final String ERR_ENTER_VALID_SITE = "Please use alternate site";
	
	/**
	 * Constant for SITE_MAP.
	 */
	public static final String SITE_MAP = "siteMap";

	/**Constant for USER_TYPE_ID.*/
	public static final String USER_TYPE_ID = "userTypeId";
	
	/** property to hold siteIds property.*/ 
	public static final String SITE_IDS = "siteIds";
	
	/** property to hold TRU_CATEGORYLANDING property.*/ 
	public static final String TRU_CATEGORYLANDING = "TruCategoryLanding";
	
	/** property to hold TRU_SUBCATEGORYLANDING property.*/ 
	public static final String TRU_SUBCATEGORYLANDING = "TruSubCategoryLanding";
	
	/** property to hold TRU_FAMILYLANDING property.*/ 
	public static final String TRU_FAMILYLANDING = "TruFamilyLanding";
	
	/** property to hold STRING_H property.*/ 
	public static final String STRING_H = "H";
			
	/** property to hold FORMAT_JSON property.*/ 
	public static final String FORMAT_JSON = "&format=json";
	
	/** property to hold REFINEMENTCRUMBS property.*/ 
	public static final String REFINEMENTCRUMBS = "refinementCrumbs";
	
	/** property to hold PROPERTIES property.*/ 
	public static final String PROPERTIES = "properties";
	
	/** property to hold CATEGORY_DISPLAYS_STATUS property.*/ 
	public static final String CATEGORY_DISPLAYS_STATUS = "dimval.prop.category.displayStatus";
	
	/** property to hold STRING_A property.*/ 
	public static final String STRING_A = "A";
	
	/** property to hold STRING_ND property.*/ 
	public static final String STRING_ND = "ND";
	
	/** property to hold ANCESTORS property.*/ 
	public static final String ANCESTORS = "ancestors";
	
	/** property to hold LABEL property.*/ 
	public static final String LABEL = "label";
	
	/** property to hold GREATERTHEN property.*/ 
	public static final String GREATERTHEN = ">";
	
	/** property to hold MAIN_HEADER property.*/ 
	public static final String MAIN_HEADER = "MainHeader";
	
	/** property to hold NAVIGATION property.*/ 
	public static final String NAVIGATION = "navigation";
	
	/** property to hold NAME property.*/ 
	public static final String NAME = "name";
	
	/** property to hold NO_DISPLAY property.*/ 
	public static final String NO_DISPLAY = "NO DISPLAY";
	
	/** property to hold supressInSearch property.*/ 
	public static final String SUPRESS_FILTER= "supressInSearch:0";
	
	/** property to hold IsPriceExists property.*/ 
	public static final String IS_PRICE_EXISTS_FILTER= "IsPriceExists:true";
		
	/** property to hold UNIFTP property.*/ 
	public static final String UNIFTP = "uniftp ";
	
	/** property to hold HUNIFTP property.*/ 
	public static final String HUNIFTP = " huniftp"; 
	
	/**
	 * constant for BREADCRUMB.
	 */
	public static final String  BREADCRUMB= "breadCrumb";
	/**
	 * constant for CATALOGID.
	 */
	public static final String  CATALOGID= "catalogId";
	/**
	 * constant for CATEGORY_STATUS_CODE.
	 */
	public static final String  CATEGORY_DISPLAY_STATUS_CODE= "catalogStatusCD";
	/**
	 * constant for PAGE_NAME_DISPLAY.
	 */
	public static final String  PAGE_NAME_DISPLAY_TEXT= "pageNameDisplayTxt";
	
	/**
	 * constant for PAGE_NAME_NAV_TEXT.
	 */
	public static final String  PAGE_NAME_NAV_TEXT= "parametricProfileNavTxt";
	/**
	 * constant for CATEGORY_CSV_RESPONSE.
	 */
	public static final String  CATEGORY_PROFILE= "categoryProfile";
	
	/** The Constant IS_PRICE_EXISTS. */
	public static final String IS_PRICE_EXISTS ="IsPriceExists";
	
	/**
	 * The constant for YYYY-MM-DDHHmmss.
	 */
	public static final String MONTH_DATE_YEAR_TIME = "MMddyyyy-HH:mm:ss";
	
	/**
	 * Constant for BASELINE_ENDECA_INDEXING.
	 */
	public static final String BASELINE_ENDECA_INDEXING = "baseLineEndecaIndexing";
	
	/**
	 * Constant for PARTIAL_ENDECA_INDEXING.
	 */
	public static final String PARTIAL_ENDECA_INDEXING = "partialEndecaIndexing";
	/**
	 * Constant for ENDECA_INDEXING.
	 */
	public static final String ENDECA_INDEXING = "endecaIndexing";
	/**
	 * Constant for indexingStatus.
	 */
	public static final String INDEXING_STATUS = "indexingStatus";
	/**
	 * Constant for indexingException.
	 */
	public static final String INDEXING_EXCEPTION = "indexingException";
	/**
	 * Constant for integrationException.
	 */
	public static final String INTEGRATION_EXCEPTION = "integrationException";
	/**
	 * Constant for akamaiCacheInvalidationJob.
	 */
	public static final String AKAMAI_CACHE_INVALIDATION = "akamaiCacheInvalidationJob";
	/**
	 * Constant for cacheInvalidation.
	 */
	public static final String CACHE_INVALIDATION = "cacheInvalidation";
	/**
	 * Constant for PARAMETRIC_FEED.
	 */
	public static final String PARAMETRIC_FEED = "parametericFeed";
	
	/**
	 * Constant for PARAMETRIC_CSV.
	 */
	public static final String PARAMETRIC_CSV= "parametricCSV";
	
	/** The Constant SHIP_TO_ELIGIBLE. */
	public static final String SHIP_TO_ELIGIBLE ="shipToStoreEligible";
	
	/** The Constant SKU_ITEM_STORE_PICKUP. */
	public static final String SKU_ITEM_STORE_PICKUP ="sku.itemInStorePickUp";
	/** The Constant SKU_CHANNEL_AVAILABILITY. */
	public static final String SKU_CHANNEL_AVAILABILITY ="sku.channelAvailability";
	

	
	/** The Constant SKU_STORE_ELIGIBLE_ARRAY. */
	public static final String SKU_STORE_ELIGIBLE_ARRAY ="skuStoreEligibleArray";
	
	/** The Constant AMPERSAND_SEARCHKEY_EQUALS. */
	public static final String AMPERSAND_SEARCHKEY_EQUALS = "&searchKey=";
	
	/** The Constant UN_CARTABLE. */
	public static final String UN_CARTABLE="unCartable";
	
	/** The Constant MIN_AGE_IN_MONTHS. */
	public static final String MIN_AGE_IN_MONTHS="minAgeInMonths";
	
	/** The Constant MAX_AGE_IN_MONTHS. */
	public static final String MAX_AGE_IN_MONTHS="maxAgeInMonths";
	
	/** The Constant EMPTY_JSON. */
	public static final String EMPTY_JSON = "{}";
	
	/** The Constant EMPTY_JSON. */
	public static final String AMPERSAND_PUSHSITE_EQUAL = "&pushSite=";
	
	/** The Constant CATEGORY_COOKIE. */
	public static final String CATEGORY_COOKIE = "categoryCookie";
	
	/** The Constant CAT_REFRESH_COOKIE */
	public static final String CAT_REFRESH_COOKIE = "catrefreshcookie";
	
	/** The Constant CAT_DIM */
	public static final String CAT_DIM = "catdim";
	/**
     * constants for ALT_URL.
     */    
    public static final String ALT_URL = "altUrl";
    
    /**
     * constants for DIVIDER.
     */    
    public static final String DIVIDER =  "|";
   
    /**
     * constants for SKU_PRICE_ARRAY.
     */    
    public static final String SKU_PRICE_ARRAY =  "SKU_PRICE_ARRAY";

    /** The Constant REVIEW_COUNT. */
	public static final String REVIEW_COUNT = "sku.reviewCount";
    
	/** The Constant REVIEW_COUNT_DECIMAL. */
	public static final String REVIEW_COUNT_DECIMAL = "sku.reviewCountDecimal";

	/** The Constant REVIEW_RATING. */
	public static final String REVIEW_RATING = "sku.reviewRating";
	
	/** The Constant CATEGORY_ID_CONSTANT. */
	public static final String CATEGORY_ID_CONSTANT = "CATEGORY_ID";
	
	/** The Constant CHILD_CAT_ID_CONSTANT. */
	public static final String CHILD_CAT_ID_CONSTANT = "CHILD_CAT_ID";
	
	/** The Constant NAVIGATION_CONTAINER. */
	public static final String NAVIGATION_CONTAINER = "Navigation Container";
	/** The Constant REVIEW_COUNT_PROPERTY. */
	public static final String REVIEW_COUNT_PROPERTY = "reviewCount";
	
	/** The Constant REVIEW_RATING_PROPERTY. */
	public static final String REVIEW_RATING_PROPERTY = "reviewRating";
	
	/** The Constant REVIEW_COUNT_DECIMAL_PROPERTY. */
	public static final String REVIEW_COUNT_DECIMAL_PROPERTY = "reviewCountDecimal";

	/** The Constant COBRAND_PAGE. */
	public static final CharSequence COBRAND_PAGE = "/cobrand/";
	
	/** The Constant HTML_TAG. */
	public static final String HTML_TAG = "<html>";
	
	/** The Constant CURLYBRACKET_START. */
	public static final String CURLYBRACKET_START = "{";
	
	/** The Constant CURLYBRACKET_END. */
	public static final String CURLYBRACKET_END = "}";
	
	/**
	 * Constant for SLASH_SINGLE_QUOTE
	 */
	public static final String SLASH_SINGLE_QUOTE = "\'";
	
	/**
	 * Constant for NBSP
	 */
	public static final String NBSP = "&nbsp;";
	
	/**
	 * Constant for CARRIAGE_RETURN
	 */
	public static final String CARRIAGE_RETURN = "\\r";
	/**
	 * Constant for NEWLINE_RETURN
	 */
	public static final String NEWLINE_RETURN = "\\n";
	/**
	 * constant for holding E_SPACE.
	 */
	public static final String E_SPACE = " ";
	
	/**
	 * constant for holding SSL.
	 */
	public static final String SSL = "SSL";
	
	/**
	 * constant for holding LOCATION.
	 */
	public static final String LOCATION = "Location";
	
	/** 
	 * Constant for COLLECTION_URL
	 */
	public static final String COLLECTION_URL = "/collection";
	
	/** 
	 * Constant for BOTH
	 */
	public static final String BOTH = "Both";
	
	/** 
	 * Constant for BOY
	 */
	public static final String BOY = "Boy";
	
	/** 
	 * Constant for GIRL
	 */
	public static final String GIRL = "Girl";
	
}