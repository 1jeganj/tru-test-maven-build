
package com.tru.endeca.index;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class extends OOTB PropertyAccessorImpl to generate the Newest product date.
 * @version 1.0
 * @author PA
 * 
 */
public class TRUNewestProductAccessor extends GenerativePropertyAccessor {

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * This property hold reference for TRUCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**  This holds the reference for SiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * Returns the this property hold reference for TRUCatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	/**
	 * Sets the this property hold reference for TRUCatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * This method will return SiteContextManager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * This method will set mpricingTools with pSiteContextManager.
	 *
	 * @param pSiteContextManager the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This method overrides OOTB getTextOrMetaPropertyValue method to generate
	 * the isNewItems boolean dimensions based on the product startDate and
	 * number of days specified in daysToShowTheItemIsNew .
	 * 
	 * @param pContext
	 *            the context
	 * @param pItem
	 *            the item
	 * @param pPropertyName
	 *            the property name
	 * @param pType
	 *            the type
	 * @param pIsMeta
	 *            the isMeta
	 * @return isNewItem
	 */
	@Override
	public Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType, boolean pIsMeta) {
		
		if (isLoggingDebug()) {
			vlogDebug("Begin :: TRUProductClassificationAccessor.getMetaPropertyValue method: RepositoryItem :{0}", pItem);
		}
		
		Map<String,Object> newSkuMap=null;
		newSkuMap = new HashMap<String, Object>();
		boolean preSellable = Boolean.FALSE;
		String isNew = TRUEndecaConstants.FALSE;
		
		if(pItem != null) {
			String idate = null;
			preSellable = getCatalogTools().preSellable(pItem);
			if (isLoggingDebug()) {
				vlogDebug("The presellable flag value is : {0}", preSellable);
			}
			newSkuMap.put(TRUEndecaConstants.SKU_ISPRESELLABLE, preSellable);
			
			Date itemDate =(Date)pItem.getPropertyValue(getCatalogProperties().getInventoryFirstAvailableDate());

			SimpleDateFormat sDateFormat = new SimpleDateFormat(TRUEndecaConstants.SIMPLE_DATE_FORMAT);
			Calendar calander = Calendar.getInstance();
			String today = sDateFormat.format(calander.getTime());
			if(itemDate!=null) {
				idate=sDateFormat.format(itemDate);
			}
			SimpleDateFormat format = new SimpleDateFormat(TRUEndecaConstants.SIMPLE_DATE_FORMAT);
			Date inventryDate = null;
			Date currentDate = null;
			if(idate!=null && today!=null) {
				try {
					inventryDate = format.parse(idate);
					currentDate = format.parse(today);
					if (isLoggingDebug()) {
						vlogDebug("Subscription Cookie has been created by date : {0} and the current date is : {1}", inventryDate, currentDate);
					}
				} catch (ParseException e) {
					if (isLoggingError()) {
						vlogError(e, "exception occured in date parsing");
					}				
				}
			}

			if(inventryDate!=null && currentDate!=null){

				int differInDays = (int) ((currentDate.getTime() - inventryDate.getTime()) / (TRUConstants.INT_THOUSAND	* TRUConstants.INT_SIXTY * TRUConstants.INT_SIXTY * TRUConstants.INT_TWENTY_FOUR));

				newSkuMap.put(TRUEndecaConstants.SKU_INVENTORY_AVAILABLE_DAYS, differInDays);
				if (differInDays >= TRUCommerceConstants.INT_ZERO && differInDays <= getCalulationDays(pItem)) {
					isNew = TRUEndecaConstants.TRUE;
					newSkuMap.put(TRUEndecaConstants.SKU_ISNEW, isNew);
				}
				else {
					newSkuMap.put(TRUEndecaConstants.SKU_ISNEW, isNew);
				}
			}
			else {
				newSkuMap.put(TRUEndecaConstants.SKU_ISNEW, isNew);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUProductClassificationAccessor.getMetaPropertyValue method");
		}
		return newSkuMap;
	}

	@Override
	public boolean ownsDynamicPropertyName(String pArg0,
			IndexingOutputConfig pArg1, OutputProperty pArg2) {
		return false;
	}



	/**
	 * Gets the calulation days.
	 *
	 * @param pItem the item
	 * @return the calulation days
	 */
	@SuppressWarnings("unchecked")
	public int getCalulationDays(RepositoryItem pItem) {
		
		if (isLoggingDebug()) {
			logDebug("Begin :: TRUProductClassificationAccessor.getCalulationDays method: RepositoryItem");
		}

		Set<String> siteSet = (Set<String>) pItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
		List<String> siteList = new ArrayList<String>(siteSet);

		String siteID = null;
		Site site = null;

		if (siteList != null && !siteList.isEmpty()) {
			siteID = (String) siteList.get(TRUEndecaConstants.INT_ZERO);
			try {
				site = getSiteContextManager().getSite(siteID);
			} catch (SiteContextException e) {
				if (isLoggingError()) {
					vlogError(e, "TRUListPricePropertyAccessor.getPropertyNamesAndValues method Exception occured");
				}
			}
		}
		int limitOfDays = TRUCommerceConstants.INT_ZERO;

		if (site != null) {
			String daysLimitThresold = (String) site.getPropertyValue(getCatalogProperties().getNewProductThreshold());
			if (daysLimitThresold != null) {
				limitOfDays = Integer.parseInt(daysLimitThresold);
			}
		}
		
		
		if (isLoggingDebug()) {
			logDebug("END :: TRUProductClassificationAccessor.getCalulationDays method: RepositoryItem");
		}
		return  limitOfDays;
	}

}
