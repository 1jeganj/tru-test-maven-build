package com.tru.endeca.index;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaUtils;




/**
 * The Class TRUActivePricePropertyAccessor.
 * @version 1.0
 * @author PA
 */
public class TRUActivePricePropertyAccessor extends GenerativePropertyAccessor{

	/**  This holds the reference for SiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**  This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;

	/**  This holds the reference for mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * This method will return SiteContextManager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * This method will set mpricingTools with pSiteContextManager.
	 *
	 * @param pSiteContextManager the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * This method will return pricingTools.
	 *
	 * @return the pricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * This method will set mpricingTools with pPricingTools.
	 *
	 * @param pPricingTools the mpricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}
	
	
	/**
	 * This property hold reference for TRUEndecaUtils.
	 */
	private TRUEndecaUtils mTRUEndecaUtils;
	
	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}



	/**
	 * adds the ability to generate price range for the product based on child skus.
	 *
	 * @param pContext            - Context
	 * @param pItem            - Extended Specification Repository Item
	 * @param pPropertyName            - pParamString
	 * @param pType            - Property Type
	 * @param pIsMeta            - whether property is Meta or not
	 * @return Map - Map with key & value pairs
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pItem,
			String pPropertyName, PropertyTypeEnum pType, boolean pIsMeta) {
		
		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUActivePricePropertyAccessor.getPropertyNamesAndValues method");
		}
		Map<String, Object> priceMap = null;
		String siteID = null;
		Site site = null;
		RepositoryItemDescriptor collectionProduct = null;
		List<RepositoryItem> skuList = null;
		List<RepositoryItem> collectionSkuList = null;
		boolean isCollectionProduct = false;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();

		if(pItem == null || pContext == null) {
			return null;
		}

		String parentSkuId = pItem.getRepositoryId();
		RepositoryItem parentProduct = pContext.getParent();
		if (parentProduct == null) {
			return null; 
		}
		String productId = parentProduct.getRepositoryId();
		List<String> styleDimensions = (List<String>) parentProduct.getPropertyValue(getCatalogProperties().getProductStyleDimension());

		try {
			collectionProduct = parentProduct.getItemDescriptor();
			if (TRUEndecaConstants.COLLECTION_PRODUCT.equals(collectionProduct.getItemDescriptorName())) {
				isCollectionProduct = true;
			}
		} catch (RepositoryException exception) {
			if (isLoggingError()) {
				vlogError(exception, "An exception occurred while getting collectionProduct", new Object[] { collectionProduct });
			}
		}

		boolean isStylizedProduct = checkForStylizedProduct(styleDimensions);
		Set<String> siteSet = (Set<String>) pItem.getPropertyValue(catalogProperties.getSiteIDPropertyName());
		List<String> siteList = new ArrayList<String>(siteSet);
		if (siteList != null && !siteList.isEmpty()) {
			siteID = (String) siteList.get(TRUEndecaConstants.INT_ZERO);
			try {
				site = getSiteContextManager().getSite(siteID);
			} catch (SiteContextException e) {
				if (isLoggingError()) {
					vlogError(e, "TRUListPricePropertyAccessor.getPropertyNamesAndValues method Exception occured");
				}
			}
		}
		skuList = (List<RepositoryItem>) parentProduct.getPropertyValue(getCatalogProperties().getChildSKUsPropertyName());
		if(isCollectionProduct)	{
			collectionSkuList = (List<RepositoryItem>) parentProduct.getPropertyValue(catalogProperties.getCollectionSkus());
		}
		if(skuList == null || skuList.isEmpty() || site == null) {
			return null;
		}
		
		if(isCollectionProduct) {
			priceMap = getPriceMapForStylisedItems(parentSkuId, collectionSkuList, productId, site);
			
		} else if(skuList.size() == TRUEndecaConstants.INT_ONE || !isStylizedProduct) {
			priceMap = getPriceMapForNonStylisedItems(parentSkuId, productId, site);
		} else  {
			priceMap = getPriceMapForStylisedItems(parentSkuId, skuList, productId, site);
		}
		
		priceMap.put(TRUEndecaConstants.IS_STYLIZED, isStylizedProduct);
		priceMap.put(TRUEndecaConstants.IS_COLLECTION_PRODUCT, isCollectionProduct);

		if(collectionSkuList != null && !collectionSkuList.isEmpty()){
			priceMap.put(TRUEndecaConstants.COLLECTION_SKU_LISTSIZE, collectionSkuList.size());
		}
		
		return priceMap;
	}


	/**
	 * Checks whether the Product is stylized or not.
	 * 
	 * @param pStyleDimensions 
	 * 				- List of Style Dimensions associated with the product.
	 * @return isStylizedProduct
	 * 				- Boolean property to determine whether the product is stylized.
	 */
	private boolean checkForStylizedProduct(List<String> pStyleDimensions) {

		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUActivePricePropertyAccessor.checkForStylizedProduct method");
		}
		if(!pStyleDimensions.isEmpty()) {
			Set<String> setStyleDimensions=  new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
			setStyleDimensions.addAll(pStyleDimensions);
			if(setStyleDimensions.contains(TRUCommerceConstants.COLOR) || setStyleDimensions.contains(TRUCommerceConstants.SIZE)) {
				if (isLoggingDebug()) {
					vlogDebug("The product is a Stylized product.");
				}
				return true;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUActivePricePropertyAccessor.checkForStylizedProduct method");
		}

		return false;
	}

	/**
	 * Get the Price related Properties for non-Stylised items.
	 * 
	 * @param pParentSkuId 
	 * 				- The Parent Sku Id.
	 * @param pProductId 
	 * 				- product Id.
	 * @param pSite 
	 * 				- The Site Item to which the product belongs
	 * @return priceMap
	 * 				- PriceMap with all the Properties required for Price Indexing.
	 */
	private Map<String, Object> getPriceMapForNonStylisedItems(String pParentSkuId, String pProductId, Site pSite) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN::: TRUActivePricePropertyAccessor.getPriceMapForNonStylisedItems method");
		}
		Map<String, Object> priceMap = new HashMap<String, Object>();
		double salePrice = TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double listPrice=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;

		priceMap.put(TRUEndecaConstants.HAS_PRICE_RANGE, TRUEndecaConstants.FALSE);

		listPrice = (double) getPricingTools().getListPriceForSKU(pParentSkuId, pProductId, pSite);
		if(listPrice > 0) {
			priceMap.put(TRUEndecaConstants.SKU_LISTPRICE, listPrice);
		}

		salePrice = (double) getPricingTools().getSalePriceForSKU(pParentSkuId, pProductId, pSite);
		if(salePrice > 0) {
			priceMap.put(TRUEndecaConstants.SKU_SALEPRICE, salePrice);
		}

		boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
		if (isLoggingDebug()) {
			vlogDebug("listPrice for sku id : {0} and saleprice for sku id : {1}  and Strike Thorugh Price : {2}" , listPrice, salePrice, calculateSavings);
		}
		priceMap.put(TRUEndecaConstants.STRIKE_THROUGH, calculateSavings);
		Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);

		if (calculateSavings && savingsMap != null) {
			priceMap.put(TRUConstants.SAVED_AMOUNT, savingsMap.get(TRUConstants.SAVED_AMOUNT));
			priceMap.put(TRUConstants.SAVED_PERCENTAGE, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
		} 

		if(salePrice > 0 && (listPrice > salePrice||listPrice <=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO)) {
			priceMap.put(TRUEndecaConstants.PRICE_ASCENDING_SORT_PARAMETER, salePrice);
			priceMap.put(TRUEndecaConstants.PRICE_DESCENDING_SORT_PARAMETER, salePrice);
			priceMap.put(TRUEndecaConstants.IS_PRICE_EXISTS,TRUEndecaConstants.TRUE);
			priceMap.put(TRUEndecaConstants.SKU_ACTIVEPRICE,salePrice);
		}else {
			priceMap.put(TRUEndecaConstants.PRICE_ASCENDING_SORT_PARAMETER, listPrice);
			priceMap.put(TRUEndecaConstants.PRICE_DESCENDING_SORT_PARAMETER, listPrice);
			priceMap.put(TRUEndecaConstants.SKU_ACTIVEPRICE,listPrice); 
			if(listPrice == TRUEndecaConstants.DOUBLE_CONSTANT_ZERO){
				priceMap.put(TRUEndecaConstants.IS_PRICE_EXISTS,TRUEndecaConstants.FALSE); 
			}else{
				priceMap.put(TRUEndecaConstants.IS_PRICE_EXISTS,TRUEndecaConstants.TRUE);
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END::: TRUActivePricePropertyAccessor.getPriceMapForNonStylisedItems method");
		}

		return priceMap;
	}

	/**
	 * Get the Price related Properties for non-Stylised items.
	 * 
	 * @param pParentSkuId 
	 * 				- pParentSkuId.
	 *  @param pSkuList 
	 * 				- pSkuList.
	 *  @param pProductId 
	 * 				- pProductId.
	 * @param pSite 
	 * 				- pSite.
	 * @return  PriceMapForStylisedItems
	 */
	private Map<String, Object> getPriceMapForStylisedItems(String pParentSkuId, List<RepositoryItem> pSkuList, String pProductId, Site pSite) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN::: TRUActivePricePropertyAccessor.getPriceMapForStylisedItems method");
		}
		Map<String, Object> priceMap = new HashMap<String, Object>();
		double salePrice=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double listPrice=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		List<Double> allListPriceList = new ArrayList<Double>();
		List<Double> allSalePriceList = new ArrayList<Double>();
		double minlist=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double maxlist=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double minsale=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double maxsale=TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;

		priceMap.put(TRUEndecaConstants.HAS_PRICE_RANGE, TRUEndecaConstants.TRUE);
		for (RepositoryItem sku : pSkuList) {
			String skuId = sku.getRepositoryId();
			boolean isActiveSku = getTRUEndecaUtils().isActiveSku(sku);
			if (!StringUtils.isEmpty(skuId) && !StringUtils.isEmpty(pProductId) && pSite != null && isActiveSku) {
				// List price
				listPrice = (double) getPricingTools().getListPriceForSKU(skuId, pProductId, pSite);
				if(listPrice > 0) {
					allListPriceList.add(listPrice);
				}
				// Sale price
				salePrice = (double) getPricingTools().getSalePriceForSKU(skuId, pProductId, pSite);
				if(salePrice > 0) {
					allSalePriceList.add(salePrice);
				}
				if(skuId.equals(pParentSkuId)) {
					if(listPrice > 0) {
						priceMap.put(TRUEndecaConstants.SKU_LISTPRICE, listPrice);
					}
					if(salePrice > 0) {
						priceMap.put(TRUEndecaConstants.SKU_SALEPRICE, salePrice);
					}
					
					boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
					if (calculateSavings) {
						priceMap.put(TRUEndecaConstants.SKU_ACTIVEPRICE,salePrice);
					} else {
						priceMap.put(TRUEndecaConstants.SKU_ACTIVEPRICE,listPrice); 
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("For the Sku Id : {0} --- listPrice is : {1} and saleprice is : {2}" , skuId, listPrice, salePrice);
				}
			}
		}

		if (!allListPriceList.isEmpty()) {
			Collections.sort(allListPriceList);
			if (isLoggingDebug()) {
				logDebug("all list prices  after sorting " + allListPriceList);
			}
			minlist=allListPriceList.get(0);
			maxlist=allListPriceList.get(allListPriceList.size() - TRUEndecaConstants.INT_ONE);
		}

		if (!allSalePriceList.isEmpty()) {
			Collections.sort(allSalePriceList);
			if (isLoggingDebug()) {
				logDebug("all sale prices  after sorting " + allSalePriceList);
			}
			minsale=allSalePriceList.get(0);
			maxsale=allSalePriceList.get(allSalePriceList.size() - TRUEndecaConstants.INT_ONE);
		}
		if(minlist > 0 && maxlist > 0){
			priceMap.put(TRUEndecaConstants.IS_PRICE_EXISTS,TRUEndecaConstants.TRUE); 
			if(minsale <= minlist && maxsale <= maxlist) {
				priceMap.put(TRUEndecaConstants.MIN_PRICE, minsale);
				priceMap.put(TRUEndecaConstants.MAX_PRICE, maxsale);
				priceMap.put(TRUEndecaConstants.PRICE_ASCENDING_SORT_PARAMETER, minsale);
				priceMap.put(TRUEndecaConstants.PRICE_DESCENDING_SORT_PARAMETER, maxsale);
			} else {
				priceMap.put(TRUEndecaConstants.MIN_PRICE, minlist);
				priceMap.put(TRUEndecaConstants.MAX_PRICE, maxlist);
				priceMap.put(TRUEndecaConstants.PRICE_ASCENDING_SORT_PARAMETER, minlist);
				priceMap.put(TRUEndecaConstants.PRICE_DESCENDING_SORT_PARAMETER, maxlist);
			}
		}else{
			priceMap.put(TRUEndecaConstants.IS_PRICE_EXISTS,TRUEndecaConstants.FALSE); 
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUActivePricePropertyAccessor.getPriceMapForStylisedItems method");
		}
		return priceMap;
	}



	
	/**
	 * This method has overridden and this will return true value always.
	 *
	 * @param pOutputPropertyName            - OutPut property name
	 * @param pConfig            - IndexingOutputConfig instance
	 * @param pOutputProperty            - Output Property name
	 * @return the boolean value
	 */
	@Override
	public boolean ownsDynamicPropertyName(String pOutputPropertyName,
			IndexingOutputConfig pConfig, OutputProperty pOutputProperty) {

		return false;
	}
}
