/*
 * Copyright (C) 2014, Professional Access Pvt Ltd. All Rights Reserved. No use,
 * copying or distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included on all
 * copies, modifications and derivatives of this work.
 */
package com.tru.endeca.index;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.utils.TRUEndecaUtils;

/**
 * The Class TRUColorAccessor property Accessor iterates over the over all the Child SKUs, fetches the Color and return
 * as a multi valued Product level property.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSwatchColorPropertyAccessor extends PropertyAccessorImpl {

	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mConfiguration;

	/**
	 * Property Holds endecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * This property hold reference for TRUCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * This property hold reference for TRUEndecaUtils.
	 */
	private TRUEndecaUtils mTRUEndecaUtils;

	/** 
	 *  This holds the reference for SiteContextManager. 
	 */
	private SiteContextManager mSiteContextManager;

	/**  This holds the reference for TRUPricingTools. */
	private TRUPricingTools mPricingTools;

	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}

	/**
	 * Returns the this property hold reference for TRUConfiguration.
	 * 
	 * @return the mConfiguration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * sets commerce TRUConfiguration.
	 * @param pConfiguration the TRUConfiguration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/**
	 * Returns the this property hold reference for TRUCatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * returns the Endeca Configurations.
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Sets the this property hold reference for TRUCatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * Gets the pricing tools.
	 *
	 * @return the pricing tools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * Sets the pricing tools.
	 *
	 * @param pPricingTools the new pricing tools
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}

	/**
	 * Gets the site context manager.
	 *
	 * @return the site context manager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the site context manager.
	 *
	 * @param pSiteContextManager the new site context manager
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		this.mSiteContextManager = pSiteContextManager;
	}



	/**
	 * adds the ability to generate multiple color swatch properties at Product level.
	 * 
	 * @param pContext - Context
	 * @param pItem - Extended Specification Repository Item
	 * @param pPropertyName - pParamString
	 * @param pType - Property Type
	 * @return Map - Map with key & value pairs
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {

		if (isLoggingDebug()) {
			logDebug("======START:::===== TRUSwatchColorPropertyAccessor.getMetaPropertyValue");
		}
		List<String> colorSwatchImageList = new ArrayList<String>();

		if(pItem != null) {
			List<String> styleDimensions = (List<String>) pItem.getPropertyValue(getCatalogProperties().getProductStyleDimension());
			Set<String> setStyleDimensions=  new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
			setStyleDimensions.addAll(styleDimensions);
			if(styleDimensions != null && !styleDimensions.isEmpty() && (setStyleDimensions.contains(TRUCommerceConstants.COLOR))) {
				List<RepositoryItem> skuItems = (List<RepositoryItem>) pItem.getPropertyValue(getCatalogProperties().getChildSkusPropertyName());

				if (skuItems != null && !skuItems.isEmpty()) {
					if (isLoggingDebug()) {
						vlogDebug("Child SKU's {0}: for Product {1}", skuItems,pItem.getRepositoryId());
					}
					colorSwatchImageList = getColorSwatchProperty(skuItems , pItem);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("=======END:::======= TRUSwatchColorPropertyAccessor.getMetaPropertyValue");
		}
		if(colorSwatchImageList != null && !colorSwatchImageList.isEmpty()) {
			return colorSwatchImageList;
		}
		return null;

	}

	/**
	 * Constructs the Color Swatch property from the list of Child skus and  Color
	 * Item Dexceriptor.
	 * 
	 * @param pSkuItems 
	 * 				- List of Child Skus repository Items
	 * @param pItem 
	 * @return defaultColorIndex
	 * 				- Index of the Default Color item
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<String> getColorSwatchProperty(List<RepositoryItem> pSkuItems, RepositoryItem pItem) {

		if (isLoggingDebug()) {
			vlogDebug("======START:::===== TRUSwatchColorPropertyAccessor.getColorSwatchProperty");
		}

		List<String> skucolorlist = new ArrayList<String>();
		//	List<String> uniqueColorRepositoryIds = new ArrayList<String>();
		String itemDesc = null;
		List<String> colorString = new ArrayList<String>();

		for (RepositoryItem skuItem : pSkuItems) {
			try {
				if(skuItem == null){
					continue;
				}
				itemDesc = skuItem.getItemDescriptor().getItemDescriptorName();
			} catch (RepositoryException exc) {
				vlogError("RepositoryException in TRUSwatchColorPropertyAccessor.getColorSwatchProperty():" , exc);
			}

			StringBuffer colorAttributeValue = new StringBuffer();
			StringBuffer promovalue=new StringBuffer();
			boolean colorexists = false;
			boolean checkSKUActivePrice = Boolean.FALSE;
			boolean isActicveSku = getTRUEndecaUtils().isActiveSku(skuItem);
			if(isActicveSku){
				checkSKUActivePrice = checkSKUActivePrice(skuItem,pItem);
			}
			// Get SKU level image from the SKU item
			if (skuItem != null && getConfiguration().getTruSkuTypes().contains(itemDesc) && checkSKUActivePrice) {
				RepositoryItem clorItem=(RepositoryItem) skuItem.getPropertyValue(getCatalogProperties().getSkuColorCode());

				String skuImageUrl = (String) skuItem.getPropertyValue(getCatalogProperties().getSkuPrimaryImage());
				List<String> skuPromos = (List<String>) skuItem.getPropertyValue(getCatalogProperties().getSkuPromoPropertyName());
				Iterator itr=skuPromos.iterator();
                while(itr.hasNext()){
                	promovalue.append(itr.next());
                	promovalue.append(TRUEndecaConstants.PIPE_SYMBOL);
                }				
				String colorCode=null;
				if(clorItem!=null){
					colorCode =(String)clorItem.getPropertyValue(getCatalogProperties().getColorName());
				}
				String colorSwatchImageUrl=(String)skuItem.getPropertyValue(getCatalogProperties().getSkuSwatchImage());

				if(!StringUtils.isEmpty(skuImageUrl)) {
					colorAttributeValue.append(skuImageUrl);
				} 

				colorAttributeValue.append(TRUEndecaConstants.SEMI_COLON);

				if (!StringUtils.isEmpty(colorCode) && colorCode != null) {
					if(colorString.contains(colorCode)) {
						colorexists = true;
					} else {
						colorString.add(colorCode);
					}
					colorAttributeValue.append(colorCode);
				}
				colorAttributeValue.append(TRUEndecaConstants.SEMI_COLON);
				if(!StringUtils.isEmpty(colorSwatchImageUrl)) {
					colorAttributeValue.append(colorSwatchImageUrl);
				}
				colorAttributeValue.append(TRUEndecaConstants.SEMI_COLON);
				if(promovalue!=null){
					colorAttributeValue.append(promovalue);
				}
			}

			if(colorAttributeValue != null && colorAttributeValue.length() > 0 && !colorexists) {
				skucolorlist.add(colorAttributeValue.toString());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("==END:::= TRUSwatchColorPropertyAccessor.getColorSwatchProperty. ");
		}
		return skucolorlist;
	}

	/**
	 * Check sku active price.
	 *
	 * @param pSkuItem the sku item
	 * @param pItem the item
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	private boolean checkSKUActivePrice(RepositoryItem pSkuItem,RepositoryItem pItem) {
		if(isLoggingDebug()){
			logDebug("TRUSwatchColorPropertyAccessor :start of checkSKUActivePrice()");
		}
		String siteID = null;
		double listPrice = TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		double salePrice = TRUEndecaConstants.DOUBLE_CONSTANT_ZERO;
		boolean isActivePrice = Boolean.FALSE;
		Site site = null;
		Set<String> siteSet = (Set<String>) pSkuItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
		Iterator<String> iterator = siteSet.iterator();
		if (iterator.hasNext()) {
			siteID = (String) iterator.next();
			try {
				site = getSiteContextManager().getSite(siteID);
			} catch (SiteContextException e) {
				if (isLoggingError()) {
					vlogError(e, "TRUSwatchColorPropertyAccessor.checkSKUActivePrice() Exception occured ,could not retrive Site");
				}
			}
		}
		if(site!= null){
			listPrice = (double) getPricingTools().getListPriceForSKU(pSkuItem.getRepositoryId(), pItem.getRepositoryId(), site);
			salePrice = (double) getPricingTools().getSalePriceForSKU(pSkuItem.getRepositoryId(), pItem.getRepositoryId(), site);
		}		
		if(salePrice > 0 || listPrice > 0) {
			isActivePrice = Boolean.TRUE;
		}
		if(isLoggingDebug()){
			vlogDebug("TRUSwatchColorPropertyAccessor :End of checkSKUActivePrice() does sku have active price : {0}",isActivePrice);
		}
		return isActivePrice;
	}
	
}