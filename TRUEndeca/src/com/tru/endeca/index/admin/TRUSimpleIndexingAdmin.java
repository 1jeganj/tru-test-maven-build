package com.tru.endeca.index.admin;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.endeca.index.admin.SimpleIndexingAdmin;
import atg.repository.search.indexing.IndexingException;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.email.conf.TRUBaseLineEndecaIndexEmailConfiguration;
import com.tru.email.conf.TRUPartialEndecaIndexEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.integrations.akamai.scheduler.AkamaiCacheInvalidationJob;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.logging.TRUAlertLogger;

/**
 * TRUSimpleIndexingAdmin will initiate Full & partial baseline indexing. Akamai service cache will be purged after
 * successful indexing
 *
 */
public class TRUSimpleIndexingAdmin extends SimpleIndexingAdmin {
	
	/**
	 * Property to hold mAkamaiCacheInvalidationJob.
	 */
	private AkamaiCacheInvalidationJob mAkamaiCacheInvalidationJob;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	/** The m store configuration file extension. */
	private TRUStoreConfiguration mTRUStoreConfiguration;
	
	/** The m baseline endeca index email configuration. */
	private TRUBaseLineEndecaIndexEmailConfiguration mBaseLineEndecaIndexEmailConfiguration;
	
	/** The m baseline endeca index email configuration. */
	private TRUPartialEndecaIndexEmailConfiguration mPartialEndecaIndexEmailConfiguration;
	
	/** 
	 * The m index enableAkamaiPurging. 
	 */
	private boolean mEnableAkamaiPurging;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	
	/**
	 * Indexes Partial Endeca Baseline
	 * 
	 * @return boolean
	 */
	@Override
	public boolean indexPartial() {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSimpleIndexingAdmin.indexPartial method..");
		}
		boolean status = false;
		try {
			status = super.indexPartial();
			getTRUEmailServiceUtils().sendSiteMapSuccessEmail(TRUEndecaConstants.PARTIAL_ENDECA_INDEXING ,setPartialIndexSucessEmailTemplate());
		} catch (IndexingException indexingException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARTIAL_ENDECA_INDEXING,
					setPartialIndexFailureEmailTemplate(getPartialEndecaIndexEmailConfiguration().getFailureExceptionMessage(),indexingException));
			if (isLoggingError()) {
				vlogError(indexingException, "Indexing Exception occured in TRUSimpleIndexingAdmin.indexPartial");	
			}
			
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			extra.put(TRUEndecaConstants.INDEXING_STATUS, String.valueOf(status));
			extra.put(TRUEndecaConstants.INDEXING_EXCEPTION,indexingException.toString());			
			getAlertLogger().logFailure(TRUEndecaConstants.ENDECA_INDEXING, TRUEndecaConstants.PARTIAL_ENDECA_INDEXING, extra);
			
			
		} finally {
			if (!status) {
				if (isLoggingDebug()) {
					logDebug(" Partial Indexing is Failed");
				}
			} else {
				if (isLoggingDebug()) {
					logDebug(" Partial Indexing is Success");
				}
				//Call the method to purge the Akamai Cache
				if (isEnableAkamaiPurging()) {
					if (isLoggingDebug()) {
						logDebug("START: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() on Partial Indexing");
					}
					try {
						getAkamaiCacheInvalidationJob().cacheInvalidation();
					} catch (TRUIntegrationException integrationException) {
						if (isLoggingError()) {
							vlogError(integrationException,"TRUIntegrationException occured in method TRUSimpleIndexingAdmin.indexPartial");
						}
						Map<String, String> extra = new ConcurrentHashMap<String, String>();
						extra.put(TRUEndecaConstants.INTEGRATION_EXCEPTION,integrationException.toString());			
						getAlertLogger().logFailure(TRUEndecaConstants.AKAMAI_CACHE_INVALIDATION, TRUEndecaConstants.CACHE_INVALIDATION, extra);
					}
					if (isLoggingDebug()) {
						logDebug("END: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() on Partial Indexing");
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.indexPartial method..");
		}
		return status;

	}

	/**
	 * Indexes Full Endeca Baseline
	 * 
	 * @return boolean
	 */
	@Override
	public boolean indexBaseline() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.indexBaseline method..");
		}
		boolean status = false;
		try {
			status = super.indexBaseline();
			getTRUEmailServiceUtils().sendSiteMapSuccessEmail(TRUEndecaConstants.BASELINE_ENDECA_INDEXING,setBaseLineIndexSucessEmailTemplate());
		} catch (IndexingException indexingException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.BASELINE_ENDECA_INDEXING,
					setBaseLineIndexFailureEmailTemplate(getBaseLineEndecaIndexEmailConfiguration().getFailureExceptionMessage(),indexingException));
			if (isLoggingError()) {
				vlogError(indexingException, "Indexing Exception occured in TRUSimpleIndexingAdmin.indexBaseline ");
			}
			
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			extra.put(TRUEndecaConstants.INDEXING_STATUS, String.valueOf(status));
			extra.put(TRUEndecaConstants.INDEXING_EXCEPTION,indexingException.toString());			
			getAlertLogger().logFailure(TRUEndecaConstants.ENDECA_INDEXING, TRUEndecaConstants.BASELINE_ENDECA_INDEXING, extra);
			
		} finally {
			if (!status) {
				if (isLoggingDebug()) {
					logDebug(" Baseline Indexing is Failed");
				}
			} else if (status) {
				if (isLoggingDebug()) {
					logDebug(" Indexing is Success");
				}
				//Call the method to purge the Akamai Cache
				if (isEnableAkamaiPurging()) {
					if (isLoggingDebug()) {
						logDebug("START: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() on Baseline Indexing");
					}
					try {
						getAkamaiCacheInvalidationJob().cacheInvalidation();
					} catch (TRUIntegrationException integrationException) {
						if (isLoggingError()) {
							vlogError(integrationException,"TRUIntegrationException occured in method TRUSimpleIndexingAdmin.indexBaseline");
						}
						Map<String, String> extra = new ConcurrentHashMap<String, String>();
						extra.put(TRUEndecaConstants.INTEGRATION_EXCEPTION,integrationException.toString());			
						getAlertLogger().logFailure(TRUEndecaConstants.AKAMAI_CACHE_INVALIDATION, TRUEndecaConstants.CACHE_INVALIDATION, extra);
						
					}
					if (isLoggingDebug()) {
						logDebug("END: Calling getAkamaiCacheInvalidationJob().cacheInvalidation() on Baseline Indexing");
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.indexBaseline method..");
		}
		return status;

	}
	
	/**
	 * Sets the base line indexing success email template.
	 *
	 * @return the map
	 */
	private Map<String, Object> setBaseLineIndexSucessEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.setBaseLineIndexSucessEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		StringBuilder sb=new StringBuilder(getBaseLineEndecaIndexEmailConfiguration().getSuccessBaseLineIndexEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUCC, getBaseLineEndecaIndexEmailConfiguration().getSuccBaseLineIndexMessage());
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.setBaseLineIndexSucessEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	
	/**
	 * Sets the base line indexing success email template.
	 *
	 * @return the map
	 */
	private Map<String, Object> setPartialIndexSucessEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.setPartialIndexSucessEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		StringBuilder sb=new StringBuilder(getPartialEndecaIndexEmailConfiguration().getSuccessPartialIndexEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUCC, getPartialEndecaIndexEmailConfiguration().getSuccPartialIndexMessage());
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.setPartialIndexSucessEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Sets the base line index failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setBaseLineIndexFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.setBaseLineIndexFailureEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getBaseLineEndecaIndexEmailConfiguration().getFailureBaseLineIndexEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.setBaseLineIndexFailureEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Sets the Partial index failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setPartialIndexFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.setPartialIndexFailureEmailTemplate method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getPartialEndecaIndexEmailConfiguration().getFailureBaseLineIndexEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.setPartialIndexFailureEmailTemplate method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Gets the stack trace.
	 *
	 * @param pThrowable the throwable
	 * @return the stack trace
	 */
	private String getStackTrace(Throwable pThrowable) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSimpleIndexingAdmin.getStackTrace() method.. ");
		}
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    pThrowable.printStackTrace(printWriter); 
	    if(result.toString().length() > TRUEndecaConstants.INT_THOUSAND){
	    	if (isLoggingDebug()) {
				logDebug("END:: TRUSimpleIndexingAdmin.getStackTrace() method.. ");
			}
	    	return result.toString().substring(TRUEndecaConstants.INT_ZERO, TRUEndecaConstants.INT_THOUSAND);
	    }
	    if (isLoggingDebug()) {
			logDebug("END:: TRUSimpleIndexingAdmin.getStackTrace() method.. ");
		}
	    return result.toString();
	  }
	
	

	/**
	 * @return the baseLineEndecaIndexEmailConfiguration
	 */
	public TRUBaseLineEndecaIndexEmailConfiguration getBaseLineEndecaIndexEmailConfiguration() {
		return mBaseLineEndecaIndexEmailConfiguration;
	}

	/**
	 * @param pBaseLineEndecaIndexEmailConfiguration the baseLineEndecaIndexEmailConfiguration to set
	 */
	public void setBaseLineEndecaIndexEmailConfiguration(
			TRUBaseLineEndecaIndexEmailConfiguration pBaseLineEndecaIndexEmailConfiguration) {
		mBaseLineEndecaIndexEmailConfiguration = pBaseLineEndecaIndexEmailConfiguration;
	}

	/**
	 * @return the partialEndecaIndexEmailConfiguration
	 */
	public TRUPartialEndecaIndexEmailConfiguration getPartialEndecaIndexEmailConfiguration() {
		return mPartialEndecaIndexEmailConfiguration;
	}

	/**
	 * @param pPartialEndecaIndexEmailConfiguration the partialEndecaIndexEmailConfiguration to set
	 */
	public void setPartialEndecaIndexEmailConfiguration(
			TRUPartialEndecaIndexEmailConfiguration pPartialEndecaIndexEmailConfiguration) {
		mPartialEndecaIndexEmailConfiguration = pPartialEndecaIndexEmailConfiguration;
	}
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	
	/**
	 * Gets the TRU store configuration.
	 *
	 * @return the tRUStoreConfiguration
	 */
	public TRUStoreConfiguration getTRUStoreConfiguration() {
		return mTRUStoreConfiguration;
	}

	/**
	 * Sets the TRU store configuration.
	 *
	 * @param pTRUStoreConfiguration the tRUStoreConfiguration to set
	 */
	public void setTRUStoreConfiguration(TRUStoreConfiguration pTRUStoreConfiguration) {
		mTRUStoreConfiguration = pTRUStoreConfiguration;
	}


	

	/**
	 * Gets the AkamaiCacheInvalidationJob
	 * 
	 * @return the AkamaiCacheInvalidationJob
	 */
	public AkamaiCacheInvalidationJob getAkamaiCacheInvalidationJob() {
		return mAkamaiCacheInvalidationJob;
	}

	/**
	 * Sets the AkamaiCacheInvalidationJob
	 * 
	 * @param pAkamaiCacheInvalidationJob the AkamaiCacheInvalidationJob to set
	 */
	public void setAkamaiCacheInvalidationJob(AkamaiCacheInvalidationJob pAkamaiCacheInvalidationJob) {
		mAkamaiCacheInvalidationJob = pAkamaiCacheInvalidationJob;
	}
	
	/**
	 * Checks if is EnableAkamaiPurging.
	 *
	 * @return true, if is EnableAkamaiPurging
	 */
	public boolean isEnableAkamaiPurging() {
		return mEnableAkamaiPurging;
	}

	/**
	 * Sets the EnableAkamaiPurging.
	 *
	 * @param pEnableAkamaiPurging the new EnableAkamaiPurging
	 */
	public void setEnableAkamaiPurging(boolean pEnableAkamaiPurging) {
		mEnableAkamaiPurging = pEnableAkamaiPurging;
	}
}
