package com.tru.endeca.index;

import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;


/**
 * The TRUCustomRollupKeyPropertyAccessor is used to fetch the Custom Roll Up key.
 * @version 1.0
 * @author PA
 * 
 */
public class TRUCustomRollupKeyPropertyAccessor extends PropertyAccessorImpl {

	/**  
	 * 
	 * This holds the reference for mCatalogProperties. 
	 */
	private TRUCatalogProperties mCatalogProperties;


	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations
	 *            - Endeca Configurations.
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}


	/**
	 * This method overrides OOTB getTextOrMetaPropertyValue method to generate
	 * This decide the custom Rollupkey for sku 
	 * if it is product the customrollupkey will be onlinePID 
	 * and for collection customrollup key will be collection ID.
	 * 
	 * @param pContext
	 *            the context
	 * @param pItem
	 *            the item
	 * @param pPropertyName
	 *            the property name
	 * @param pType
	 *            the type
	 * @return Object
	 * 			- the customrollupkey
	 */

	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {

		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUCustomRollupKeyPropertyAccessor.getTextOrMetaPropertyValue method");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		RepositoryItemDescriptor collectionProduct = null;
		String customrollupkey = TRUEndecaConstants.EMPTY_STRING;

		boolean isCollectionProduct = false;
		String itemDesc = null;
		boolean isElligbeItem = false;

		if(pItem != null) {
			List <String> skuType = getEndecaConfigurations().getSkuType();
			try {
				itemDesc = pItem.getItemDescriptor().getItemDescriptorName();

				if(!StringUtils.isEmpty(itemDesc) && (TRUEndecaConstants.REGULAR_SKU.equalsIgnoreCase(itemDesc) || (skuType != null && !skuType.isEmpty() &&  skuType.contains(itemDesc)))) {
					isElligbeItem=true;
				}
			} catch (RepositoryException exception) {
				if (isLoggingError()) {
					vlogError(exception, "An exception occurred while getting the Item Descriptor item");
				}
			}

			RepositoryItem parentProduct = pContext.getParent();
			if (parentProduct == null  || ! isElligbeItem) {
				return null; 
			}

			String collectionId = null;
			String onlinePID = (String) pItem.getPropertyValue(catalogProperties.getOnlinePID());

			try {
				collectionProduct = parentProduct.getItemDescriptor();
				if (TRUEndecaConstants.COLLECTION_PRODUCT.equals(collectionProduct.getItemDescriptorName())) {
					isCollectionProduct = true;
				}
			} 
			catch (RepositoryException exception) {
				if (isLoggingError()) {
					vlogError(exception, "An exception occurred while getting collectionProduct:: {0}", collectionProduct);
				}
			}

			if(isCollectionProduct) {
				collectionId = parentProduct.getRepositoryId();
				customrollupkey=collectionId;
			} else {
				customrollupkey=onlinePID;
			}
			if(isLoggingDebug()) {
				vlogDebug("CustomRollUp Key value for the sku id - {0} is ::: {1}", pItem.getRepositoryId(), customrollupkey);
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END::: TRUCustomRollupKeyPropertyAccessor.getTextOrMetaPropertyValue method");
		}
		return customrollupkey;
	}
}
