package com.tru.endeca.index;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * The Class TRUGenderPropertyAccessor.
 */
public class TRUGenderPropertyAccessor extends PropertyAccessorImpl {
	
	/**
	 * This property hold reference for TRUCatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	/**
	 * Returns the this property hold reference for TRUCatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	
	/**
	 * Sets the this property hold reference for TRUCatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * adds the ability to generate gender List at Product level.
	 * 
	 * @param pContext - Context
	 * @param pItem - Extended Specification Repository Item
	 * @param pPropertyName - pParamString
	 * @param pType - Property Type
	 * @return List - the gender List
	 */
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType)
	  {
		 if (isLoggingDebug()) {
				vlogDebug("BEGIN :: TRUGenderPropertyAccessor.getTextOrMetaPropertyValue method:: Input params pItem :: {0}",
						pItem);
			}
		 if(pItem == null) {
				return null;
			}
		 	List<String> genderList = null;
			 String gender = (String)pItem.getPropertyValue(getCatalogProperties().getProductGenderPropertyName());
			 if(StringUtils.isNotEmpty(gender)){
				 genderList = new ArrayList();
				 if(TRUEndecaConstants.BOTH.equalsIgnoreCase(gender)) {
					 genderList.add(TRUEndecaConstants.BOY);
					 genderList.add(TRUEndecaConstants.GIRL);
				 }
				 else {
					 genderList.add(gender);
				 }
				 
			 }
			 if (isLoggingDebug()) {
					vlogDebug("END :: TRUGenderPropertyAccessor.getTextOrMetaPropertyValue method:: GenderList= {0}",genderList);
				}
			if (isLoggingDebug()) {
				vlogDebug("END :: TRUGenderPropertyAccessor.getTextOrMetaPropertyValue method");
			}
			return genderList;
	  }
	
}
