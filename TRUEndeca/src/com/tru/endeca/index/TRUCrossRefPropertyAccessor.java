package com.tru.endeca.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.common.TRUClassificationConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;



/**
 * The Class TRUCrossRefPropertyAccessor.
 */
public class TRUCrossRefPropertyAccessor extends PropertyAccessorImpl {
	
	/** 
	 * The m index enable. 
	 */
	private boolean mEnable;
	/**
	 * Gets the mEnable.
	 *
	 * @return the mEnable
	 */
	public boolean isEnable() {
		return mEnable;
	}
	/**
	 * Sets the classification configuration.
	 *
	 * @param pEnable the new mEnable 
	 */
	public void setEnable(boolean pEnable) {
		this.mEnable = pEnable;
	}

	/** 
	 * The endeca Configuration constant.
	 */
	private TRUClassificationConfiguration mClassificationConfiguration;
	/**
	 * Gets the classification configuration.
	 *
	 * @return the classification configuration
	 */
	public TRUClassificationConfiguration getClassificationConfiguration() {
		return mClassificationConfiguration;
	}

	/**
	 * Sets the classification configuration.
	 *
	 * @param pClassificationConfiguration the new classification configuration
	 */
	public void setClassificationConfiguration(
			TRUClassificationConfiguration pClassificationConfiguration) {
		this.mClassificationConfiguration = pClassificationConfiguration;
	}
	
	@SuppressWarnings("null")
	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType)
	
	{
		List<String> catIDNameList = null;
		
		if(isEnable()){
		if (pItem == null) {
			return null;
		}

		@SuppressWarnings("unchecked")
		Map<String, String> crossRefDisplayNameMap = (Map<String, String>) pItem.getPropertyValue(getClassificationConfiguration().getClassificationCrossRefDisplayName());

		if (crossRefDisplayNameMap != null) {
			String classificationID = null;
			String classificationName = null;
			String classIDAndName = null;
			catIDNameList=new ArrayList<String>();
			for (Map.Entry<String, String> entry : crossRefDisplayNameMap.entrySet()) {
				
				classificationID = entry.getKey();
				classificationName = entry.getValue();
				classIDAndName = classificationID +TRUEndecaConstants.PIPE_SYMBOL + classificationName;
				if(classIDAndName!=null){
				catIDNameList.add(classIDAndName);
			}
				}
		}
		}
		return catIDNameList;
	}

}
