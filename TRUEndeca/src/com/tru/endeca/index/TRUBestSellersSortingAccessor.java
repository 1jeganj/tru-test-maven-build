package com.tru.endeca.index;

import atg.adapter.gsa.query.Clause;
import atg.core.util.StringUtils;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;


/**
 * The TRUBestSellersSortingAccessor is used to fetch the Record sort properties BestSellerSort.
 * @version 1.0
 * @author PA
 * 
 */
public class TRUBestSellersSortingAccessor extends PropertyAccessorImpl {

	/**
	 * This property hold reference for enabled.
	 */
	public boolean mEnabled;
	
	/**
	 * Holds the reference to ProductsAnalyticsRepository.
	 */
	private Repository mProductsAnalyticsRepository;

	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Holds the reference to productsAnalyticsRQLQuery.
	 */
	private String mProductsAnalyticsRQLQuery;
	
	/**
	 * Holds the reference to ProductAnalyticsItemName.
	 */
	private String mProductAnalyticsItemName;

	/**
	 * Gets the endeca configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * Sets the endeca configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	

	/**
	 * Returns the boolean property Enabled.
	 * 
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the boolean value to property enabled .
	 *
	 * @param pEnabled            the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	
	/**
	 * Returns the reference to ProductsAnalyticsRepository.
	 * 
	 * @return the ProductsAnalyticsRepository
	 */
	public Repository getProductsAnalyticsRepository() {
		return mProductsAnalyticsRepository;
	}


	/**
	 * Sets the reference of ProductsAnalyticsRepository.
	 * 
	 * @param pProductsAnalyticsRepository
	 *            the ProductsAnalyticsRepository to set
	 */
	public void setProductsAnalyticsRepository(Repository pProductsAnalyticsRepository) {
		mProductsAnalyticsRepository = pProductsAnalyticsRepository;
	}



	/**
	 * Returns the reference to ProductsAnalyticsRQLQuery.
	 * 
	 * @return the ProductsAnalyticsRQLQuery
	 */
	public String getProductsAnalyticsRQLQuery() {
		return mProductsAnalyticsRQLQuery;
	}

	/**
	 * Sets the reference of ProductsAnalyticsRQLQuery.
	 * 
	 * @param pProductsAnalyticsRQLQuery
	 *            the ProductsAnalyticsRQLQuery to set
	 */
	public void setProductsAnalyticsRQLQuery(String pProductsAnalyticsRQLQuery) {
		mProductsAnalyticsRQLQuery = pProductsAnalyticsRQLQuery;
	}


	/**
	 * Returns the reference to ProductAnalyticsItemName.
	 * 
	 * @return the ProductAnalyticsItemName
	 */
	public String getProductAnalyticsItemName() {
		return mProductAnalyticsItemName;
	}

	/**
	 * Sets the reference of ProductAnalyticsItemName.
	 * 
	 * @param pProductAnalyticsItemName
	 *            the ProductAnalyticsItemName to set
	 */
	public void setProductAnalyticsItemName(String pProductAnalyticsItemName) {
		mProductAnalyticsItemName = pProductAnalyticsItemName;
	}


	/**
	 * This method overrides OOTB getTextOrMetaPropertyValue method to generate
	 * the BestSeller sort property which is used as Sort property for Best Selling Sort.
	 * 
	 * @param pContext
	 *            the context
	 * @param pItem
	 *            the item
	 * @param pPropertyName
	 *            the property name
	 * @param pType
	 *            the type
	 * @return isNewItem
	 */
	@SuppressWarnings("static-access")
	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {

		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUBestSellersSortingAccessor.getPropertyNamesAndValues method");
		}
		long bestSellerSortValue = 0;
		RepositoryItem[] productsAnalyticsList = null;
		if(pItem != null) {
			String productId = pItem.getRepositoryId();
			if(!StringUtils.isEmpty(productId)) {
				try {
					RepositoryView productsAnalyticsView = getProductsAnalyticsRepository().getView(getProductAnalyticsItemName());
					QueryBuilder productAnalyticsBuilder = productsAnalyticsView.getQueryBuilder();
					QueryExpression productIdPropertyName = productAnalyticsBuilder.createPropertyQueryExpression(TRUEndecaConstants.PRODUCT_ANALYTICS_ID);
					QueryExpression productIdValue = productAnalyticsBuilder.createConstantQueryExpression(productId);
					Query productAnalyticsQuery = productAnalyticsBuilder.createComparisonQuery(
							productIdPropertyName, productIdValue, productAnalyticsBuilder.EQUALS);
					if (productAnalyticsQuery instanceof Clause) {
						((Clause) productAnalyticsQuery).setContextFilterApplied(true);
					}
					productsAnalyticsList = productsAnalyticsView.executeQuery(productAnalyticsQuery);
					
					bestSellerSortValue = getBestSellerSortValue(productsAnalyticsList);
				} catch (RepositoryException e) {
					vlogError(e,"RepositoryException occured in  TRUBestSellersSortingAccessor: @Method: getPropertyNamesAndValues(): "); 
				} 
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUBestSellersSortingAccessor.getPropertyNamesAndValues method");
		}
		return bestSellerSortValue;
	}
	
	/**
	 * Gets the best seller sort value.
	 *
	 * @param pProductsAnalyticsList the products analytics list
	 * @return the best seller sort value
	 */
	private long getBestSellerSortValue(RepositoryItem[] pProductsAnalyticsList){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN::: TRUBestSellersSortingAccessor.getBestSellerSortValue() method");
		}
		long bestSellerSortValue = 0;
		if(pProductsAnalyticsList != null) {
			for(int i = 0; i < pProductsAnalyticsList.length; i++) {
				if(pProductsAnalyticsList[i] != null) {
					bestSellerSortValue = (long) pProductsAnalyticsList[i].getPropertyValue(TRUEndecaConstants.QUANTITY);
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUBestSellersSortingAccessor.getBestSellerSortValue() method");
		}
		return bestSellerSortValue;
	}

}
