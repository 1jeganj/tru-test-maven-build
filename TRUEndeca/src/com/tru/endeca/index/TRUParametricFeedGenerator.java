package com.tru.endeca.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import atg.core.util.StringUtils;
import atg.multisite.SiteManager;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.email.conf.TRUParametericFeedEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.sitemap.TRUSiteMapDomainUrl;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.proxy.ProxyConfiguration;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * This class is TRUParametricFeedGenerator.
 * @version 1.0
 * @author PA
 *
 */
public class TRUParametricFeedGenerator extends GenericService {
	
	/** The Constant GET_METHOD. */
	public static final String GET_METHOD = "GET";
	/** The Constant CONTENT_LENGTH. */
	public static final String CONTENT_LENGTH = "Content-Length";
	/** The Constant CONTENT_LENGTH_VAL. */
	public static final String CONTENT_LENGTH_VAL = "0";
	/** The Constant X_APP_API_KEY. */
	public static final String X_APP_API_KEY = "X-APP-API_KEY";
	/** The Constant X_APP_API_CHANNEL. */
	public static final String X_APP_API_CHANNEL = "X-APP-API_CHANNEL";
	/** The Constant TRU_ACCESS. */
	public static final String TRU_ACCESS = "TRU-Access";
	/** The Constant API_KEY. */
	public static final String API_KEY = "apiKey";
	/** The Constant API_CHANNEL. */
	public static final String API_CHANNEL = "mobile";
	/** The Constant TRU_ACCESS_VAL. */
	public static final String TRU_ACCESS_VAL = "true";
	/** The Constant LEFT_ANGULAR_BRACKET. */
	public static final String LEFT_ANGULAR_BRACKET = "<";
	/** The Constant LEFT_ANGULAR_BRACKET. */
	public static final String LEFT_ANGULAR_BRACKET_BACK_SHLAS = "<\\";
	/** The Constant PARAMETRIC_PROFILE_RESPONSE. */
	public static final String PARAMETRIC_PROFILE_RESPONSE = "categoryProfile\":\"";
	/** The Constant PARAMETRICPROFILESTART. */
	private static final String PARAMETRICPROFILESTART = "<PARAMETRIC_PROFILE>";
	/** The Constant PARAMETRICPROFILEEND. */
	private static final String PARAMETRICPROFILEEND = "</PARAMETRIC_PROFILE>";
	/** The m site manager. */
	private SiteManager mSiteManager;
	/** The m feed output path. */
	private String mFeedOutputPath;
	/** The m file name pattern. */
	private String mFileNamePattern;
	/** This holds the reference for PROFILE_ID *. */
	public static final String PROFILE_ID = "profileId";
	/** This holds the reference for AMPERSAND *. */
	public static final String AMPERSAND = "&";
	/** This holds the reference for EQUAL_SYMBOL *. */
	public static final String EQUAL_SYMBOL = "=";
	/**  Holds the mCatalogProperties */
	private TRUCatalogProperties mCatalogProperties;
	/**  Holds the mProxyConfiguration */
	private ProxyConfiguration mProxyConfiguration;
	/**  Holds the mConnectionTimeOut */
	private int mConnectionTimeOut;
	/**  Holds the mProxyRequired */
	private boolean mProxyRequired;
	/**  Holds the mFileNameWithExtn */
	private String mFileNameWithExtn;
	
	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;
	
	/**
	 * This property hold reference for mTRUStoreConfiguration.
	 */
	private TRUStoreConfiguration mTRUStoreConfiguration;
	/**
	 * Property to hold mParametricProfileRestServiceURL.
	 */
	private String mParametricProfileRestServiceURL;
	
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	
	/** The m Parametricfeed email configuration. */
	private TRUParametericFeedEmailConfiguration mParametericFeedEmailConfiguration;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	
	/** The m product catalog. */
	private MutableRepository mProductCatalog;
	
	/** The m tru site map domain url. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	
	/**
	 * @return the ParametericFeedEmailConfiguration
	 */
	public TRUParametericFeedEmailConfiguration getParametericFeedEmailConfiguration() {
		return mParametericFeedEmailConfiguration;
	}

	/**
	 * @param pParametericFeedEmailConfiguration the ParametericFeedEmailConfiguration to set
	 */
	public void setParametericFeedEmailConfiguration(
			TRUParametericFeedEmailConfiguration pParametericFeedEmailConfiguration) {
		mParametericFeedEmailConfiguration = pParametericFeedEmailConfiguration;
	}

	/** 
	 * The m tru endeca configurations.
	 */
	private TRUEndecaConfigurations mTRUEndecaConfigurations;

	
	/**
	 * Gets the TRUEndecaConfigurations.
	 *
	 * @return the TRUEndecaConfigurations
	 */
	
	public TRUEndecaConfigurations getTRUEndecaConfigurations() {
		return mTRUEndecaConfigurations;
	}

	/**
	 * Sets the TRUEndecaConfigurations.
	 * 
	 * @param pTRUEndecaConfigurations the TRUEndecaConfigurations to set
	 */

	public void setTRUEndecaConfigurations(TRUEndecaConfigurations pTRUEndecaConfigurations) {
		mTRUEndecaConfigurations = pTRUEndecaConfigurations;
	}


	/**
	 * Process facets.
	 */
	@SuppressWarnings("unchecked")
	public void processFacets() {

		if (isLoggingDebug()) {
			vlogDebug("===BEGIN===::: TRUParametricFeedGenerator.processFacets()");
		}
		List<String> siteIdList = null;
		String domainUrl = null;
	
		StringBuilder paramProfileXML = new StringBuilder();
		try {
			paramProfileXML.append(PARAMETRICPROFILESTART);
			int profileId=TRUEndecaConstants.INT_ONE;
			Map<String, String> siteMap = new HashMap<String,String>();		

			RepositoryItem[] siteArray = getSiteManager().getActiveSites();
				if(siteArray == null){
					return;
				}
			for(int i = TRUEndecaConstants.INT_ZERO; i < siteArray.length; i++){	
					String siteId = siteArray[i].getRepositoryId();
					String siteUrl = populateSiteProductionUrl(siteArray[i]);
					siteMap.put(siteId, siteUrl);
				} 
			
			RepositoryItem[] categoryItems = getAllCategory();
			if(categoryItems== null || categoryItems.length == TRUEndecaConstants.INT_ZERO || siteMap.isEmpty()){
				return;
			}
			
			for (RepositoryItem categoryItem : categoryItems) {
				String categoryId = categoryItem.getRepositoryId();
				Set<String> categorySiteId = (Set<String>)categoryItem.getPropertyValue(getCatalogProperties().getSiteIds());
					
				if(categorySiteId == null || categorySiteId.isEmpty()){
					continue;
				}
				siteIdList = new ArrayList<String>(categorySiteId);
					for (String id : siteIdList) {
						domainUrl = siteMap.get(id);
						String categoryResponse = populateCategoryResponseFromRestService(categoryId,profileId,domainUrl,id);
							
						if(StringUtils.isEmpty(categoryResponse)){
							continue;
						}
						String res[] = categoryResponse.split(PARAMETRIC_PROFILE_RESPONSE);
							
						if(res != null && res.length > TRUEndecaConstants.INT_ONE) {
							String categoryProfile = res[TRUEndecaConstants.INT_ONE].substring(TRUEndecaConstants.INT_ZERO,res[TRUEndecaConstants.INT_ONE].length()-TRUEndecaConstants.INT_TWO);
								
						if(StringUtils.isEmpty(categoryProfile)) {
							 continue; 
						}
					
						paramProfileXML.append(categoryProfile);
						profileId++;
							}
						}					
				}		
			
			paramProfileXML.append(PARAMETRICPROFILEEND);
			if(isLoggingDebug()){
				vlogDebug(" The Value of paramProfileXML is :::: {0} ", paramProfileXML.toString());
			}
			if(paramProfileXML.length()>TRUEndecaConstants.INT_ONE){
				writeFeed(paramProfileXML);

			}
		}catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException to get Active sites in TRUParametricProfileFeed.getFacets method ",
						e);
			}
		}
		catch (MalformedURLException malFormedUrl) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_FEED,
					setParametricFeedFailureEmailTemplate(getParametericFeedEmailConfiguration().getFailureExceptionMessage(),malFormedUrl));
			if (isLoggingError()) {
				logError("MalformedURLException in TRUParametricProfileFeed.getFacets method ",
						malFormedUrl);
			}
		} catch (IOException ioException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_FEED,
					setParametricFeedFailureEmailTemplate(getParametericFeedEmailConfiguration().getFailureExceptionMessage(),ioException));
			if (isLoggingError()) {
				logError("IOException in TRUParametricProfileFeed.getFacets method ",
						ioException);
			}
		}catch (SAXException saxException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_FEED,
					setParametricFeedFailureEmailTemplate(getParametericFeedEmailConfiguration().getFailureExceptionMessage(),saxException));
			if (isLoggingError()) {
				logError("saxException in TRUParametricProfileFeed.getFacets method ",
						saxException);
			}
		} catch (ParserConfigurationException parserConfigException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_FEED,
					setParametricFeedFailureEmailTemplate(getParametericFeedEmailConfiguration().getFailureExceptionMessage(),parserConfigException));
			if (isLoggingError()) {
				logError("parserConfigException in TRUParametricProfileFeed.getFacets method ",
						parserConfigException);
			}
		} catch (TransformerException transformerException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_FEED,
					setParametricFeedFailureEmailTemplate(getParametericFeedEmailConfiguration().getFailureExceptionMessage(),transformerException));
			if (isLoggingError()) {
				logError("transformerException in TRUParametricProfileFeed.getFacets method ",
						transformerException);
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("===END===::: TRUParametricFeedGenerator.processFacets()");
		}
	}

	/**
	 *  getCategoryUrlList method returns the subCategory map and will be called recursively
	 * @param pParentCategoryVO -- the category VO
	 * @param pLevel -- the level 
	 * 	@return categoryMap -- The list of Active Categories with their url.
	 */
	public Map<String, String> getCategoryUrlList(CategoryVO pParentCategoryVO, int pLevel) {
		if (isLoggingDebug()) {
			vlogDebug("===BEGIN===::: TRUParametricFeedGenerator.getCategoryUrlList()");
		}

		Map<String, String> categoryMap = new HashMap<String,String>();
		List<CategoryVO> subCategoryList = pParentCategoryVO.getSubCategories();
		if (subCategoryList == null || subCategoryList.isEmpty() || pLevel > getTRUEndecaConfigurations().getMaximumCategoryLevelForParametricReport()) {
			return null;
		}

		for(CategoryVO subCategory : subCategoryList) {
			categoryMap.put(subCategory.getCategoryId(), subCategory.getCategoryURL());
			if (isLoggingDebug()) {
				vlogDebug("Fetching subCategory list for Category Id -- {0}", subCategory.getCategoryId());
			}
			Map<String, String> subCategoryMap = getCategoryUrlList(subCategory, pLevel+TRUEndecaConstants.INT_ONE);
			if(subCategoryMap != null && !subCategoryMap.isEmpty()) {
				categoryMap.putAll(subCategoryMap);
			}
		}


		if (isLoggingDebug()) {
			vlogDebug("===END===::: TRUParametricFeedGenerator.getCategoryUrlList()");
		}
		return categoryMap;
	}


	/**
	 * Write feed.
	 *
	 * @param pParamProfileStr paramProfileStr
	 * @throws SAXException SAXException
	 * @throws ParserConfigurationException ParserConfigurationException
	 * @throws IOException IOException
	 * @throws TransformerException TransformerException
	 */
	private void writeFeed(StringBuilder pParamProfileStr) throws SAXException, ParserConfigurationException, IOException, TransformerException{

		String paramProfileXML = pParamProfileStr.toString();
		String fileExtn = TRUEndecaConstants.USA_PARAMETRIC_PROFILE;
		if(StringUtils.isNotBlank(getFileNameWithExtn()))
		{
			fileExtn = getFileNameWithExtn();
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(paramProfileXML)));

		// Write the parsed String to an xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.DATE_MONTH_YEAR);
		Date date = new Date();

		StreamResult result =  new StreamResult(new File(getFeedOutputPath()+TRUEndecaConstants.PATH_SEPERATOR+getFileNamePattern()+dateFormat.format(date)+fileExtn));
		transformer.transform(source, result);

	}
	
	/**
	 * Sets the Parametric feed failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setParametricFeedFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricFeedGenerator.setParametricFeedFailureEmailTemplate method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getParametericFeedEmailConfiguration().getFailureParametericFeedEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricFeedGenerator.setParametricFeedFailureEmailTemplate method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Gets the stack trace.
	 *
	 * @param pThrowable the throwable
	 * @return the stack trace
	 */
	private String getStackTrace(Throwable pThrowable) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricFeedGenerator.getStackTrace() method.. ");
		}
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    pThrowable.printStackTrace(printWriter); 
	    if(result.toString().length() > TRUEndecaConstants.INT_THOUSAND){
	    	if (isLoggingDebug()) {
				logDebug("END:: TRUParametricFeedGenerator.getStackTrace() method.. ");
			}
	    	return result.toString().substring(TRUEndecaConstants.INT_ZERO, TRUEndecaConstants.INT_THOUSAND);
	    }
	    if (isLoggingDebug()) {
			logDebug("END:: TRUParametricFeedGenerator.getStackTrace() method.. ");
		}
	    return result.toString();
	  }
	
	

	/**
	 * Gets the site manager.
	 *
	 * @return the siteManager
	 */
	public SiteManager getSiteManager() {
		return mSiteManager;
	}

	/**
	 * Sets the site manager.
	 *
	 * @param pSiteManager the siteManager to set
	 */
	public void setSiteManager(SiteManager pSiteManager) {
		mSiteManager = pSiteManager;
	}

	/**
	 * Gets the feed output path.
	 *
	 * @return the feedOutputPath
	 */
	public String getFeedOutputPath() {
		return mFeedOutputPath;
	}


	/**
	 * Sets the feed output path.
	 *
	 * @param pFeedOutputPath the feedOutputPath to set
	 */
	public void setFeedOutputPath(String pFeedOutputPath) {
		mFeedOutputPath = pFeedOutputPath;
	}


	/**
	 * Gets the file name pattern.
	 *
	 * @return the fileNamePattern
	 */
	public String getFileNamePattern() {
		return mFileNamePattern;
	}


	/**
	 * Sets the file name pattern.
	 *
	 * @param pFileNamePattern the fileNamePattern to set
	 */
	public void setFileNamePattern(String pFileNamePattern) {
		mFileNamePattern = pFileNamePattern;
	}

	/**
	 * @return the parametricProfileRestServiceURL
	 */
	public String getParametricProfileRestServiceURL() {
		return mParametricProfileRestServiceURL;
	}

	/**
	 * @param pParametricProfileRestServiceURL the parametricProfileRestServiceURL to set
	 */
	public void setParametricProfileRestServiceURL(
			String pParametricProfileRestServiceURL) {
		mParametricProfileRestServiceURL = pParametricProfileRestServiceURL;
	}
	
	/**
	 * This method populates the Profile String Xml for category.
	 * @param pCategoryId -- the category Id
	 * @param pProfileId -- the profile Id for Category
	 * @param pCurrentSite -- the current Site
	 * @param pSite -- the Site
	 * @return categoryProfileResponse -- the category profile response
	 */
	public String populateCategoryResponseFromRestService(String pCategoryId,int pProfileId, String pCurrentSite, String pSite)
	{
		StringBuffer categoryResponse = new StringBuffer();
		String categoryProfileResponse = null;
		StringBuffer siteUrl  = new StringBuffer();
		//boolean redirect = false;
		BufferedReader responseBuffer = null;
		HttpsURLConnection conn = null;
		if (isLoggingDebug()) {
			logDebug("BEGIN::  TRUParametricFeedGenerator.populateJsonFromRestService() method");
		}
		if(StringUtils.isNotBlank(pCategoryId) && StringUtils.isNotBlank(pCurrentSite) && StringUtils.isNotEmpty(getParametricProfileRestServiceURL()))
		{
			try {
						if (isLoggingDebug()) {
							vlogDebug("TRUParametricFeedGenerator.populateJsonFromRestService():CategoryId ::{0} :: Site {1} :: Rest Configuration URL {2} :: ", pCategoryId,pCurrentSite,getParametricProfileRestServiceURL());
						}
						siteUrl.append(getTRUEndecaUtils().getProtocolScheme()).append(pCurrentSite);
						//final URL url = new URL(siteUrl1.toString()+getParametricProfileRestServiceURL() + pCategoryId+AMPERSAND+PROFILE_ID+EQUAL_SYMBOL+pProfileId+TRUEndecaConstants.AMPERSAND_PUSHSITE_EQUAL+pSite);
						final URL url = new URL(null, siteUrl.toString()+getParametricProfileRestServiceURL() + pCategoryId+AMPERSAND+PROFILE_ID+EQUAL_SYMBOL+pProfileId+TRUEndecaConstants.AMPERSAND_PUSHSITE_EQUAL+pSite,new sun.net.www.protocol.https.Handler());
						if (isLoggingDebug()) {
							vlogDebug("TRUParametricFeedGenerator.populateJsonFromRestService() - Final url :: {0}  ",url);
						}
						
						/*if(isProxyRequired() && getProxyConfiguration().isProxyEnabled())
						{
							Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort()));
							if (isLoggingDebug()) {
								vlogDebug("TRUParametricFeedGenerator.populateJsonFromRestService() --  proxy :: {0}  ",proxy);
							}
							 conn = (HttpURLConnection) url.openConnection(proxy);
						}
						else {*/
							conn = (HttpsURLConnection) url.openConnection();
						//}
						if (isLoggingDebug()) {
							vlogDebug("TRUParametricFeedGenerator.populateJsonFromRestService() :: Final conn --  {0} ",conn);
						}
						conn.setInstanceFollowRedirects(false);
						conn.setConnectTimeout(getConnectionTimeOut());
						conn.setDoOutput(Boolean.TRUE);
						conn.setDoInput(Boolean.TRUE);
						conn.setUseCaches(Boolean.FALSE);
						conn.setRequestMethod(GET_METHOD);
						conn.setRequestProperty(X_APP_API_KEY, API_KEY);
						conn.setRequestProperty(X_APP_API_CHANNEL, API_CHANNEL);
						conn.setRequestProperty(TRU_ACCESS, TRU_ACCESS_VAL);
						conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
						conn.connect();
						int status = conn.getResponseCode();
						if (isLoggingDebug()) {
							vlogDebug("TRUParametricFeedGenerator.populateJsonFromRestService() -- conn.getResponseCode() {0}  ", status);
						}
						/*if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
							redirect = true;
						}
						if (redirect) {
							// get redirect url from "location" header field
							String newUrl = conn.getHeaderField(TRUEndecaConstants.LOCATION);
							if (isLoggingDebug()) {
								vlogDebug(" Redirected Url:: {0} ", newUrl);
							}
							// open the new connnection again
							conn = (HttpURLConnection) new URL(newUrl).openConnection();
							conn.setConnectTimeout(getConnectionTimeOut());
							conn.setDoOutput(Boolean.TRUE);
							conn.setDoInput(Boolean.TRUE);
							conn.setUseCaches(Boolean.FALSE);
							conn.setRequestMethod(GET_METHOD);
							conn.setRequestProperty(X_APP_API_KEY, API_KEY);
							conn.setRequestProperty(X_APP_API_CHANNEL, API_CHANNEL);
							conn.setRequestProperty(TRU_ACCESS, TRU_ACCESS_VAL);
							conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
							conn.connect();
							if (isLoggingDebug()) {
								vlogDebug(" HTTPS Status code {0}  ", conn.getResponseCode());
							}
						}*/
						if (conn.getResponseCode() != HttpsURLConnection.HTTP_OK ) {
							if (isLoggingDebug()) {
								vlogDebug(" Disconnecting the Connection :: -- conn.getResponseCode() {0}  ", conn.getResponseCode());
							}
							//conn.disconnect();
							return categoryProfileResponse;
						}
						
					 responseBuffer = new BufferedReader(	new InputStreamReader(conn.getInputStream(),Charset.forName(TRUEndecaConstants.ENCODING_FORMATTER)));
					String output;
					while ((output = responseBuffer.readLine()) != null) {
						categoryResponse.append(output);
					}
					if (isLoggingDebug()){
						vlogDebug("Category Response:::: categoryResponse {0} ", categoryResponse.toString());
					}
					//conn.disconnect();
				
		} catch (MalformedURLException e) {
			if (isLoggingError()) {
				vlogError("MalformedURLException: While processing MalformedURLException with exception : {0}: CategoryId {1} ", e,pCategoryId);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				vlogError("IOException: While processing IOException with exception : {0} : CategoryId {1} ", e,pCategoryId);
			}
		}
			finally {
			    if (responseBuffer != null) {
			        try {
			        	responseBuffer.close();
			        } catch (IOException e) {
			        	if (isLoggingError()) {
							vlogError("IOException: TRUParametricFeedGenerator.populateJsonFromRestService() While Closing the BufferedReader : {0} ", e);
						}
			        }
			    }
			    if (conn != null) {
			        conn.disconnect();
			    }
			}
		}
		
		if(categoryResponse != null)
		{
			categoryProfileResponse = categoryResponse.toString();
		
		}
		if(StringUtils.isNotBlank(categoryProfileResponse) && categoryProfileResponse.contains(LEFT_ANGULAR_BRACKET_BACK_SHLAS))
		{
		categoryProfileResponse = categoryProfileResponse.replace(LEFT_ANGULAR_BRACKET_BACK_SHLAS, LEFT_ANGULAR_BRACKET);
		}
		if (isLoggingDebug()) {
			vlogDebug("END::  TRUParametricFeedGenerator.populateJsonFromRestService() method ");
		}
		return categoryProfileResponse;
	}
	
	/**
	 * This method determine the siteProduction URL.
	 * @param pSiteItem -- the site
	 * @return siteUrl - the siteProduction Url
	 */
	public String populateSiteProductionUrl(RepositoryItem pSiteItem){
		if (isLoggingDebug()) {
			logDebug("Start: TRUParametricFeedGenerator.populateSiteProductionUrl()");
		}
		String siteUrl = null;
		if(pSiteItem != null){
			String[] siteUrls = (String[]) pSiteItem.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
			
			if (siteUrls != null && siteUrls.length > TRUCommerceConstants.INT_ZERO) {
					siteUrl = siteUrls[TRUCommerceConstants.INT_ZERO];
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUParametricFeedGenerator.populateSiteProductionUrl()");
		}
		return siteUrl;
	}

	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * @return the tRUStoreConfiguration
	 */
	public TRUStoreConfiguration getTRUStoreConfiguration() {
		return mTRUStoreConfiguration;
	}

	/**
	 * @param pTRUStoreConfiguration the tRUStoreConfiguration to set
	 */
	public void setTRUStoreConfiguration(TRUStoreConfiguration pTRUStoreConfiguration) {
		mTRUStoreConfiguration = pTRUStoreConfiguration;
	}

	/**
	 * @return the proxyConfiguration
	 */
	public ProxyConfiguration getProxyConfiguration() {
		return mProxyConfiguration;
	}

	/**
	 * @param pProxyConfiguration the proxyConfiguration to set
	 */
	public void setProxyConfiguration(ProxyConfiguration pProxyConfiguration) {
		mProxyConfiguration = pProxyConfiguration;
	}

	/**
	 * @return the connectionTimeOut
	 */
	public int getConnectionTimeOut() {
		return mConnectionTimeOut;
	}

	/**
	 * @param pConnectionTimeOut the connectionTimeOut to set
	 */
	public void setConnectionTimeOut(int pConnectionTimeOut) {
		mConnectionTimeOut = pConnectionTimeOut;
	}

	/**
	 * @return the proxyRequired
	 */
	public boolean isProxyRequired() {
		return mProxyRequired;
	}

	/**
	 * @param pProxyRequired the proxyRequired to set
	 */
	public void setProxyRequired(boolean pProxyRequired) {
		mProxyRequired = pProxyRequired;
	}

	/**
	 * @return the fileNameWithExtn
	 */
	public String getFileNameWithExtn() {
		return mFileNameWithExtn;
	}

	/**
	 * @param pFileNameWithExtn the fileNameWithExtn to set
	 */
	public void setFileNameWithExtn(String pFileNameWithExtn) {
		mFileNameWithExtn = pFileNameWithExtn;
	}
	
	/**
	 * Gets the all category present in Catalog.
	 *
	 * @return the all category
	 */
	public RepositoryItem[] getAllCategory(){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUParametricFeedGenerator  :: getAllCategory() method ");
		}
		RepositoryItem[] categoryItems= null;
		Repository productCatalogRepository= getProductCatalog();

		try {
			RepositoryItemDescriptor categoryItemDescriptor = productCatalogRepository.getItemDescriptor(getCatalogProperties().getClassificationItemDescPropertyName());
			if(categoryItemDescriptor == null){
				if (isLoggingDebug()) {
					vlogDebug("END:TRUParametricFeedGenerator  :: getAllCategory() method ,classificationItemDescriptor is null");
				}
				return null;
			}
			RepositoryView categoryRepositoryView = categoryItemDescriptor.getRepositoryView();
			if (categoryRepositoryView != null ) {
				QueryBuilder categoryQueryBuilder = categoryRepositoryView.getQueryBuilder();
				Query query = categoryQueryBuilder.createUnconstrainedQuery();
				categoryItems = categoryRepositoryView.executeQuery(query);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e, "Class TRUParametricFeedGenerator :An exception occured during getting category from Catalog.");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:TRUParametricFeedGenerator  :: getAllCategory() method ");
		}
		return categoryItems;
	}
	
	/**
	 * Gets the product catalog.
	 *
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets the product catalog.
	 *
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}
	
	/**
	 * Gets the TRU site map domain url.
	 *
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * Sets the TRU site map domain url.
	 *
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		this.mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}
	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}
}
