package com.tru.endeca.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.TRUConstants;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * The Class TRUAgeRangePropertyAccessor used to get the age range based on MFG age.
 *
 * @author Professional Access
 * @version 1.0
 */

public class TRUAgeRangePropertyAccessor extends PropertyAccessorImpl {


	/**
	 *  Property to hold mMinTargetAgeTruWebsites.
	 */ 
	private LinkedHashMap<String,String> mAgeRangeMap;  

	/**
	 *  Property to hold mMinTargetAgeTruWebsites.
	 */
	private String mMinTargetAgeTruWebsites;

	/**
	 * Property to hold mMaxTargetAgeTruWebsites.
	 */
	private String mMaxTargetAgeTruWebsites;

	/** 
	 * The m tru catalog tools. 
	 */
	private TRUCatalogTools mTruCatalogTools;

	/**
	 * adds the ability to generate gender List at Product level.
	 * 
	 * @param pContext - Context
	 * @param pItem - Extended Specification Repository Item
	 * @param pPropertyName - pParamString
	 * @param pType - Property Type
	 * @return List - the gender List
	 */
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN :: TRUAgeRangePropertyAccessor.getTextOrMetaPropertyValue method");
		}

		Map<String,Object> minMaxAgeMap = null;
		int minAge = TRUEndecaConstants.INT_ZERO;
		int maxAge = TRUEndecaConstants.INT_ZERO;
		String itemDesc = null;
		List<String> ageRangeList = new ArrayList<String>();
		LinkedHashMap<String, String> ageRangeConfiguredMap = getAgeRangeMap();

		if(pItem == null || ageRangeConfiguredMap == null || ageRangeConfiguredMap.isEmpty()) {
			return null;
		}

		try {
			itemDesc = pItem.getItemDescriptor().getItemDescriptorName();
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError(exc, "TRUAgeRangePropertyAccessor.getPropertyNamesAndValues method Exception occured");
			}
		}

		if(pItem != null && itemDesc != null && !itemDesc.equals(TRUEndecaConstants.SKU) && !itemDesc.equals(TRUEndecaConstants.NON_MERCH_SKU)) {

			String targetAgeTruWebsitesMinAge = (String)pItem.getPropertyValue(getMinTargetAgeTruWebsites());
			String targetAgeTruWebsitesMaxAge = (String)pItem.getPropertyValue(getMaxTargetAgeTruWebsites());
			minMaxAgeMap = populateMinAndMaxAgeMap(targetAgeTruWebsitesMinAge, targetAgeTruWebsitesMaxAge);
			if(minMaxAgeMap != null && !minMaxAgeMap.isEmpty()) {
				if(minMaxAgeMap.containsKey(TRUEndecaConstants.MIN_AGE_IN_MONTHS)) {
					minAge = (int) minMaxAgeMap.get(TRUEndecaConstants.MIN_AGE_IN_MONTHS);
				}
				if(minMaxAgeMap.containsKey(TRUEndecaConstants.MAX_AGE_IN_MONTHS)) {
					maxAge = (int) minMaxAgeMap.get(TRUEndecaConstants.MAX_AGE_IN_MONTHS);
				}
			}
		}
		if(minAge <= TRUEndecaConstants.INT_ZERO && maxAge <= TRUEndecaConstants.INT_ZERO ) {
			return null;
		}
		boolean continueAgeValidation = false;
		for(Map.Entry<String, String> entry : ageRangeConfiguredMap.entrySet()) {

			String ageKey = entry.getKey();
			String ageValue = entry.getValue();
			int minAgeLevelVal = TRUEndecaConstants.INT_ZERO;
			int maxAgeLevelVal = TRUEndecaConstants.INT_ZERO;
			String keyArray [] = null;
			if(StringUtils.isNotEmpty(ageKey) && StringUtils.isNotEmpty(ageValue) && ageKey.contains(TRUConstants.HYPHEN)) {
				keyArray = ageKey.split(TRUConstants.HYPHEN);
			}

			if(keyArray != null && keyArray.length == TRUEndecaConstants.INT_TWO) {
				minAgeLevelVal = Integer.parseInt(keyArray[TRUEndecaConstants.INT_ZERO]);
				maxAgeLevelVal = Integer.parseInt(keyArray[TRUEndecaConstants.INT_ONE]);

				if(minAge >= minAgeLevelVal && maxAgeLevelVal >= minAge) {
					continueAgeValidation = true;
				}

				if(continueAgeValidation && maxAge >= minAgeLevelVal ) {
					continueAgeValidation = true;
				} else {
					continueAgeValidation = false;
				}

				if(continueAgeValidation) {
					ageRangeList.add(ageValue);
					if (isLoggingDebug()) {
						vlogDebug("TRUAgeRangePropertyAccessor.getTextOrMetaPropertyValue method:: Age = {0}",ageRangeList);
					}
				} else {
					//break;
				} 
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END :: TRUAgeRangePropertyAccessor.getTextOrMetaPropertyValue method");
		}
		return ageRangeList;
	}


	/**
	 * This method has overridden and this will return true value always.
	 *
	 * @param pOutputPropertyName            - OutPut property name
	 * @param pConfig            - IndexingOutputConfig instance
	 * @param pOutputProperty            - Output Property name
	 * @return the boolean value
	 */


	/**
	 * Gets the tru catalog tools.
	 *
	 * @return the tru catalog tools
	 */
	public TRUCatalogTools getTruCatalogTools() {
		return mTruCatalogTools;
	}

	/**
	 * Sets the tru catalog tools.
	 *
	 * @param pTruCatalogTools the new tru catalog tools
	 */
	public void setTruCatalogTools(TRUCatalogTools pTruCatalogTools) {
		this.mTruCatalogTools = pTruCatalogTools;
	}

	/**
	 * This method is used to create min and max age values Map.
	 * @param pMfgMinAge -- the min Age
	 * @param pMfgMaxAge -- the max Age
	 * @return ageMap -- the min and max age Map
	 */
	public Map<String, Object> populateMinAndMaxAgeMap(String pMinAge, String pMaxAge) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUAgeRangePropertyAccessor populateMinAndMaxAgeMap() method.. pMinAge:: {0} and pMaxAge:: {1}", pMinAge, pMaxAge);
		}
		Map<String, Object> ageMap = new HashMap<String, Object>();
		String[] mfgMinAgeArry = null;
		String[] mfgMaxAgeArry = null;
		if(pMinAge == null && pMaxAge == null) {
			return null;
		}
		
		if(pMinAge != null) {
			mfgMinAgeArry = StringUtils.splitStringAtCharacter(pMinAge, TRUCommerceConstants.CHAR_HYPHEN);
		}
		if(pMaxAge != null) {
			mfgMaxAgeArry = StringUtils.splitStringAtCharacter(pMaxAge, TRUCommerceConstants.CHAR_HYPHEN);
		}
		
		String mfgMinAgeMonths = null;
		String mfgMaxAgeMonths = null;
		int mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
		int mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
		if (isLoggingDebug()) {
			vlogDebug("TRUAgeRangePropertyAccessor populateMinAndMaxAgeMap() method.. mfgMinAgeArry:: {0} and .. mfgMaxAgeArry:: {1}", mfgMinAgeArry, mfgMaxAgeArry);
		}

		if (mfgMinAgeArry != null && mfgMinAgeArry.length >= TRUCommerceConstants.INT_TWO) {
			mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ONE];
		}
		if (mfgMaxAgeArry != null && mfgMaxAgeArry.length >= TRUCommerceConstants.INT_TWO) {
			mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ONE];
		}
		if (mfgMinAgeArry != null && mfgMinAgeArry.length == TRUCommerceConstants.INT_ONE) {
			mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ZERO];
		}
		if (mfgMaxAgeArry != null && mfgMaxAgeArry.length == TRUCommerceConstants.INT_ONE) {
			mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ZERO];
		}

		if (mfgMinAgeMonths != null) {
			try {
				mfgMinAgeMonthsNum = Integer.parseInt(mfgMinAgeMonths);
			} catch (NumberFormatException e) {
				if (isLoggingError()) {
					vlogError(e, " TRUAgeRangePropertyAccessor:: populateMinAndMaxAgeMap(--) method.. ");
				}
				mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			}
			ageMap.put(TRUEndecaConstants.MIN_AGE_IN_MONTHS, mfgMinAgeMonthsNum);	
		}

		if(mfgMaxAgeMonths != null) {
			try {
				mfgMaxAgeMonthsNum = Integer.parseInt(mfgMaxAgeMonths);
			} catch (NumberFormatException e) {
				if (isLoggingError()) {
					vlogError(e, " TRUAgeRangePropertyAccessor:: populateMinAndMaxAgeMap(--) method.. ");
				}
				mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			}
			ageMap.put(TRUEndecaConstants.MAX_AGE_IN_MONTHS, mfgMaxAgeMonthsNum);
		}

		if (isLoggingDebug()) {
			vlogDebug("TRUAgeRangePropertyAccessor:: populateMinAndMaxAgeMap() method.. mfgMinAgeMonthsNum:: {0} and mfgMaxAgeMonthsNum:: {1}", mfgMinAgeMonthsNum, mfgMaxAgeMonthsNum);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUAgeRangePropertyAccessor:: populateMinAndMaxAgeMap(--) method..::");
		}
		return ageMap;
	}

	/**
	 * @return the ageRangeMap
	 */
	public LinkedHashMap<String,String> getAgeRangeMap() {
		return mAgeRangeMap;
	}

	/**
	 * @param pAgeRangeMap the ageRangeMap to set
	 */
	public void setAgeRangeMap(LinkedHashMap<String,String> pAgeRangeMap) {
		mAgeRangeMap = pAgeRangeMap;
	}

	/**
	 * @return the minTargetAgeTruWebsites
	 */
	public String getMinTargetAgeTruWebsites() {
		return mMinTargetAgeTruWebsites;
	}

	/**
	 * @param pMinTargetAgeTruWebsites the minTargetAgeTruWebsites to set
	 */
	public void setMinTargetAgeTruWebsites(String pMinTargetAgeTruWebsites) {
		mMinTargetAgeTruWebsites = pMinTargetAgeTruWebsites;
	}

	/**
	 * @return the maxTargetAgeTruWebsites
	 */
	public String getMaxTargetAgeTruWebsites() {
		return mMaxTargetAgeTruWebsites;
	}

	/**
	 * @param pMaxTargetAgeTruWebsites the maxTargetAgeTruWebsites to set
	 */
	public void setMaxTargetAgeTruWebsites(String pMaxTargetAgeTruWebsites) {
		mMaxTargetAgeTruWebsites = pMaxTargetAgeTruWebsites;
	}

}
