package com.tru.endeca.index;

import java.util.HashMap;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.endeca.constants.TRUEndecaConstants;


// TODO: Auto-generated Javadoc
/**
 * The Class TRUMinAndMaxAgePropertyAccessor used to get the age range based on MFG age.
 *
 * @author Professional Access
 * @version 1.0
 */

public class TRUMinAndMaxAgePropertyAccessor extends GenerativePropertyAccessor {
	
	/**
	 *  Property to hold mMinTargetAgeTruWebsites.
	 */
	private String mMfrSuggestedAgeMin;
	/**
	 * Property to hold mMaxTargetAgeTruWebsites.
	 */
	private String mMfrSuggestedAgeMax;
	
	/** The m tru catalog tools. */
	private TRUCatalogTools mTruCatalogTools;
	@Override
	/**
	 * This method format min and max age for SKU.
	 *  @param pContext Context
	 *  @param pItem RepositoryItem
	 *  @return Map
	 */
	protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pItem,
			String pPropertyName, PropertyTypeEnum pType, boolean pIsMeta) {
		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUMinAndMaxAgePropertyAccessor.getPropertyNamesAndValues method");
		}
		Map<String, Object> ageMap = null;
		String itemDesc = null;
		try {
			itemDesc = pItem.getItemDescriptor().getItemDescriptorName();
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("TRUMinAndMaxAgePropertyAccessor.getPropertyNamesAndValues method Exception occured",exc);
			}
		}
		if(pItem!=null && itemDesc != null && !itemDesc.equals(TRUEndecaConstants.SKU) && !itemDesc.equals(TRUEndecaConstants.NON_MERCH_SKU)){

			String minAge=(String)pItem.getPropertyValue(getMfrSuggestedAgeMin());
			String maxAge=(String)pItem.getPropertyValue(getMfrSuggestedAgeMax());
			ageMap = populateMinAndMaxAgeMap(minAge, maxAge);
			String minAndMaxAgeMessage = getTruCatalogTools().getMinAndMaxMFGAgeMessage(minAge, maxAge);
			ageMap.put(TRUEndecaConstants.AGE_RANGE,minAndMaxAgeMessage);
		}
		if (isLoggingDebug()) {
			vlogDebug("END::: TRUMinAndMaxAgePropertyAccessor.getPropertyNamesAndValues method");
		}
		return ageMap;
	}

	/**
	 * This method has overridden and this will return true value always.
	 *
	 * @param pOutputPropertyName            - OutPut property name
	 * @param pConfig            - IndexingOutputConfig instance
	 * @param pOutputProperty            - Output Property name
	 * @return the boolean value
	 */
	@Override
	public boolean ownsDynamicPropertyName(String pOutputPropertyName,
			IndexingOutputConfig pConfig, OutputProperty pOutputProperty) {
		//Returning true, since the property names are not getting changed.
		return false;
	}

	
	/**
	 * Gets the tru catalog tools.
	 *
	 * @return the tru catalog tools
	 */
	public TRUCatalogTools getTruCatalogTools() {
		return mTruCatalogTools;
	}

	/**
	 * Sets the tru catalog tools.
	 *
	 * @param pTruCatalogTools the new tru catalog tools
	 */
	public void setTruCatalogTools(TRUCatalogTools pTruCatalogTools) {
		this.mTruCatalogTools = pTruCatalogTools;
	}

	/**
	 * Gets the mfr suggested age min.
	 *
	 * @return the mfr suggested age min
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * Sets the mfr suggested age min.
	 *
	 * @param pMfrSuggestedAgeMin the new mfr suggested age min
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		this.mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * Gets the mfr suggested age max.
	 *
	 * @return the mfr suggested age max
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * Sets the mfr suggested age max.
	 *
	 * @param pMfrSuggestedAgeMax the new mfr suggested age max
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}
	
	/**
	 * This method is used to create min and max age values Map.
	 * @param pMfgMinAge -- the min Age
	 * @param pMfgMaxAge -- the max Age
	 * @return ageMap -- the min and max age Map
	 */
	public Map<String, Object> populateMinAndMaxAgeMap(String pMfgMinAge, String pMfgMaxAge){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method.. pMfgMinAge::" + pMfgMinAge + ".. pMfgMaxAge::"
					+ pMfgMaxAge);
		}
		Map<String, Object> ageMap = new HashMap();
		if (pMfgMinAge != null && pMfgMaxAge != null) {
			String[] mfgMinAgeArry = StringUtils.splitStringAtCharacter(pMfgMinAge, TRUCommerceConstants.CHAR_HYPHEN);
			String[] mfgMaxAgeArry = StringUtils.splitStringAtCharacter(pMfgMaxAge, TRUCommerceConstants.CHAR_HYPHEN);
			String mfgMinAgeMonths = null;
			String mfgMaxAgeMonths = null;
			int mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			int mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
			if (isLoggingDebug()) {
				logDebug("TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method.. mfgMinAgeArry::" + mfgMinAgeArry + ".. mfgMaxAgeArry::"
						+ mfgMaxAgeArry);
			}
			if (mfgMinAgeArry != null && mfgMinAgeArry.length >= TRUCommerceConstants.INT_TWO) {
				mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ONE];
			}
			if (mfgMaxAgeArry != null && mfgMaxAgeArry.length >= TRUCommerceConstants.INT_TWO) {
				mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ONE];
			}
			if (mfgMinAgeArry != null && mfgMinAgeArry.length == TRUCommerceConstants.INT_ONE) {
				mfgMinAgeMonths = mfgMinAgeArry[TRUCommerceConstants.INT_ZERO];
			}
			if (mfgMaxAgeArry != null && mfgMaxAgeArry.length == TRUCommerceConstants.INT_ONE) {
				mfgMaxAgeMonths = mfgMaxAgeArry[TRUCommerceConstants.INT_ZERO];
			}
			if (mfgMinAgeMonths != null) {
				try {
					mfgMinAgeMonthsNum = Integer.parseInt(mfgMinAgeMonths);
				} catch (NumberFormatException e) {
					if (isLoggingError()) {
						logError(" TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method.. ", e);
					}
					mfgMinAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
				}

				ageMap.put(TRUEndecaConstants.MIN_AGE_IN_MONTHS, mfgMinAgeMonthsNum);	
			}
			if(mfgMaxAgeMonths != null) {
				try {
					mfgMaxAgeMonthsNum = Integer.parseInt(mfgMaxAgeMonths);
				} catch (NumberFormatException e) {
					if (isLoggingError()) {
						logError(" TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method.. ", e);
					}
					mfgMaxAgeMonthsNum = TRUCommerceConstants.INT_ZERO;
				}
				ageMap.put(TRUEndecaConstants.MAX_AGE_IN_MONTHS, mfgMaxAgeMonthsNum);
			}
			if (isLoggingDebug()) {
				logDebug("TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method.. mfgMinAgeMonthsNum::" + mfgMinAgeMonthsNum
						+ ".. mfgMaxAgeMonthsNum::" + mfgMaxAgeMonthsNum);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMinAndMaxAgePropertyAccessor populateMinAndMaxAgeMap(--) method..::");
		}
		return ageMap;
	}
	
}
