package com.tru.endeca.index;

import java.util.Set;

import atg.commerce.endeca.index.dimension.CategoryNodePropertyAccessor;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;

/**
 * This class extends OOTB PropertyAccessorImpl to generate the Newest product date.
 * @version 1.0
 * @author PA
 * 
 */
public class TRUCategoryNodePropertyAccessor extends CategoryNodePropertyAccessor {
	
	/** 
	 * The m enable Category To Site Validation. 
	 */
	private boolean mEnableCategoryToSiteValidation;
	
	/**
	 * @return the mEnableCategoryToSiteValidation
	 */
	public boolean isEnableCategoryToSiteValidation() {
		return mEnableCategoryToSiteValidation;
	}


	/**
	 * @param pEnableCategoryToSiteValidation the mEnableCategoryToSiteValidation to set
	 */
	public void setEnableCategoryToSiteValidation(
			boolean pEnableCategoryToSiteValidation) {
		this.mEnableCategoryToSiteValidation = pEnableCategoryToSiteValidation;
	}
	
	/** 
	 * property hold catalogTools. 
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * This method is overridden to validate whether the Category belongs to current CAtalog or not.
	 *
	 * @param pContext            - Context
	 * @param pItem            - Extended Specification Repository Item
	 * @param pPropertyName            - pParamString
	 * @param pType            - Property Type
	 * @return Object - Object with Output values.
	 */
	@SuppressWarnings("unchecked")
	public Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {

		if (isLoggingDebug()) {
			logDebug("===Begin :TRUCategoryNodePropertyAccessor.getTextOrMetaPropertyValue Method. ");
		}
		boolean callSuperMethod = false;
		
		
		if(pItem == null) {
			return null;
		}
		if (isLoggingDebug()) {
			vlogDebug("Associating Category with Product for Categorty Id :: {0}", pItem.getRepositoryId());
		}
		
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogTools().getCatalogProperties();
		
		if(isEnableCategoryToSiteValidation()) {
			Set<RepositoryItem> catalogList = (Set<RepositoryItem>) pItem.getPropertyValue(catalogProperties.getComputedCatalogPropertyName());
			RepositoryItem currentCatalog = (RepositoryItem) pContext.getAttributes().get(catalogProperties.getContextCatalogPropertyName());
			if(catalogList != null && !catalogList.isEmpty() && currentCatalog != null) {
				String currentCatalogId = currentCatalog.getRepositoryId();
				for(RepositoryItem catalog : catalogList) {
					if(StringUtils.isNotEmpty(currentCatalogId) && currentCatalogId.equals(catalog.getRepositoryId())) {
						callSuperMethod = true;
						break;
					}
				}
			}
		} else {
			callSuperMethod = true;
		}
		
		if(callSuperMethod) {
			if (isLoggingDebug()) {
				vlogDebug("Calling Super method to associate the Category with Id : {0}", pItem.getRepositoryId());
			}
			return super.getTextOrMetaPropertyValue(pContext, pItem, pPropertyName, pType);
		}
		if (isLoggingDebug()) {
			logDebug("===END :TRUCategoryNodePropertyAccessor.getTextOrMetaPropertyValue Method.");
		}
		
		return null;
	}


}
