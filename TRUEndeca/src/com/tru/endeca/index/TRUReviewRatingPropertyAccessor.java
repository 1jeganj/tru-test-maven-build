package com.tru.endeca.index;

import java.util.HashMap;
import java.util.Map;

import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * The Class TRUReviewRatingPropertyAccessor.
 */
public class TRUReviewRatingPropertyAccessor extends GenerativePropertyAccessor {
	
	/** The review rating repository. */
	private Repository mReviewRatingRepository;
	
	/** The review rating item descriptor name. */
	private String mReviewRatingItemDescriptorName;
	
	/** The m enabled. */
	private boolean mEnabled;

	/* (non-Javadoc)
	 * @see atg.repository.search.indexing.GenerativePropertyAccessor#getPropertyNamesAndValues(atg.repository.search.indexing.Context, atg.repository.RepositoryItem, java.lang.String, atg.repository.search.indexing.specifier.PropertyTypeEnum, boolean)
	 */
	@SuppressWarnings("static-access")
	@Override
	protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pRepositoryItem,String pString, PropertyTypeEnum pPropertyTypeEnum,boolean pBoolean) {
		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUReviewRatingPropertyAccessor.getPropertyNamesAndValues method");
		}
		Map<String, Object> reviewRatingMap = new HashMap<String, Object>();
		if(pRepositoryItem ==null ){
			return reviewRatingMap;
		}
	
		if(isEnabled()){
			try {
				RepositoryView view = getReviewRatingRepository().getView(getReviewRatingItemDescriptorName());
				QueryBuilder queryBuilder = view.getQueryBuilder();
				QueryExpression propertyQueryExpression = queryBuilder.createPropertyQueryExpression(TRUCommerceConstants.ID);
				QueryExpression constantQueryExpression = queryBuilder.createConstantQueryExpression(pRepositoryItem.getRepositoryId());
				Query query = queryBuilder.createComparisonQuery(propertyQueryExpression, constantQueryExpression,queryBuilder.EQUALS);
				RepositoryItem[] results = view.executeQuery(query);
				if(results == null || results.length <=0){
					if (isLoggingDebug()) {
						vlogDebug("power review repository item not found for skuId - {0}",pRepositoryItem.getRepositoryId());
					}
					return reviewRatingMap;
				}
				for (RepositoryItem repositoryItem : results) {
					Object reviewRatingValue = repositoryItem.getPropertyValue(TRUEndecaConstants.REVIEW_RATING_PROPERTY);
					 Object reviewCountValue = repositoryItem.getPropertyValue(TRUEndecaConstants.REVIEW_COUNT_PROPERTY);
					 Object reviewCountDecimalValue = repositoryItem.getPropertyValue(TRUEndecaConstants.REVIEW_COUNT_DECIMAL_PROPERTY);
					 if(reviewRatingValue != null){
						 reviewRatingMap.put(TRUEndecaConstants.REVIEW_RATING,(float)reviewRatingValue);
					 }
					 if(reviewCountValue != null){
						 reviewRatingMap.put(TRUEndecaConstants.REVIEW_COUNT,(float)reviewCountValue); 
					 }
					 if(reviewCountDecimalValue != null){
						 reviewRatingMap.put(TRUEndecaConstants.REVIEW_COUNT_DECIMAL,(float)reviewCountDecimalValue);
					 }
					break;
				}
				
			} catch (RepositoryException e) {
				if(isLoggingError()){
					vlogError("exception occured in TRUReviewRatingPropertyAccessor while processing the item - {0} ,{1}", pRepositoryItem.getRepositoryId(),e);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End::: TRUReviewRatingPropertyAccessor.getPropertyNamesAndValues method");
		}
		
		return reviewRatingMap;
	}

	/* (non-Javadoc)
	 * @see atg.repository.search.indexing.GenerativePropertyAccessor#ownsDynamicPropertyName(java.lang.String, atg.repository.search.indexing.IndexingOutputConfig, atg.repository.search.indexing.specifier.OutputProperty)
	 */
	@Override
	public boolean ownsDynamicPropertyName(String pParamString,
			IndexingOutputConfig pParamIndexingOutputConfig,
			OutputProperty pParamOutputProperty) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Gets the review rating repository.
	 *
	 * @return the review rating repository
	 */
	public Repository getReviewRatingRepository() {
		return mReviewRatingRepository;
	}

	/**
	 * Sets the review rating repository.
	 *
	 * @param pReviewRatingRepository the new review rating repository
	 */
	public void setReviewRatingRepository(Repository pReviewRatingRepository) {
		mReviewRatingRepository = pReviewRatingRepository;
	}

	/**
	 * Gets the review rating item descriptor name.
	 *
	 * @return the review rating item descriptor name
	 */
	public String getReviewRatingItemDescriptorName() {
		return mReviewRatingItemDescriptorName;
	}

	/**
	 * Sets the review rating item descriptor name.
	 *
	 * @param pReviewRatingItemDescriptorName the new review rating item descriptor name
	 */
	public void setReviewRatingItemDescriptorName(String pReviewRatingItemDescriptorName) {
		mReviewRatingItemDescriptorName = pReviewRatingItemDescriptorName;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	

}
