package com.tru.endeca.model.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;

//import com.tru.commerce.catalog.TRUCatalogManager;
/*import com.tru.commerce.catalog.custom.TRUCatalogProperties;*/
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.commerce.catalog.vo.ClassificationUtilKeyVO;
/*import com.tru.commerce.catalog.vo.TRUClassificationMediaVO;*/
import com.tru.commerce.catalog.vo.TRUClassificationVO;
//import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.SortByDisplayOrderComparator;
import com.tru.endeca.utils.TRUCategoryTreeUtils;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.vo.MegaMenuVO;
import com.tru.utils.TRUClassificationsTools;

/**
 * This Class is used for to render the global navigation for catalog.
 * This service uses the ATG OOTB CatalogTools for navigating through the category hierarachy.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUMegaMenuHandler extends GenericService {
	
	/**
	 * Property to hold SortByDisplayOrderComparator.
	 */
	private SortByDisplayOrderComparator mDisplayOrderComparator;
		
	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private TRUCategoryTreeUtils mCategoryUtils;
	/**
	 * The  media banner key .
	 */
	private String mBannerKey;

	/**
	 * The related media content.
	 */
	private String mRelatedMediaContent;

	/**
	 * The image url.
	 */
	private String mImageUrl;

	/**
	 * This property hold reference for TRUCatalogManager.
	 */
	/*private TRUCatalogManager mCatalogManager;*/

	/**
	 * This property hold reference for CatalogProperties.
	 */
	/*private TRUCatalogProperties mCatalogProperties;*/

	/**
	 * This property hold reference for TRUClassificationsTools.
	 */
	private TRUClassificationsTools mClassificationsTools;

	/**
	 * This property hold reference for TRUClassificationMediaVO.
	 */
/*	private TRUClassificationMediaVO mTRUClassificationMediaVO*/;

	/**
	 * This property hold reference for TRUDimensionValueCache.
	 */
/*	private TRUDimensionValueCache mDimensionValueCache;*/

	/**
	 * This property hold reference for UrlPrefix.
	 */
	private String mUrlPrefix;
	
	/**
	 * This property hold reference for MaximumCategoryLevelForMegaMenu.
	 */
	private int mMaximumCategoryLevelForMegaMenu;
	
	/**
	 * Property to hold MegaMenuMaxAllowedRootCategories.
	 */
	private String mMegaMenuMaxAllowedRootCategories;
	
	
	/** The m tru endeca configurations. */
	private TRUEndecaConfigurations mTRUEndecaConfigurations;
	/**
	 * Gets the category utils.
	 *
	 * @return gets the CategoryUtil
	 */
	public TRUCategoryTreeUtils getCategoryUtils() {
		return mCategoryUtils;
	}

	/**
	 * Sets the category utils.
	 *
	 * @param pCategoryUtils sets the CategoryUtil
	 */
	public void setCategoryUtils(TRUCategoryTreeUtils pCategoryUtils) {
		this.mCategoryUtils = pCategoryUtils;
	}
	
	/**
	 * Gets the display order comparator.
	 *
	 * @return the displayOrderComparator
	 */
	public SortByDisplayOrderComparator getDisplayOrderComparator() {
		return mDisplayOrderComparator;
	}


	/**
	 * Sets the display order comparator.
	 *
	 * @param pDisplayOrderComparator the displayOrderComparator to set
	 */
	public void setDisplayOrderComparator(
			SortByDisplayOrderComparator pDisplayOrderComparator) {
		mDisplayOrderComparator = pDisplayOrderComparator;
	}

	/**
	 * Returns the this property hold reference for MaximumCategoryLevelForMegaMenu.
	 * 
	 * @return the MaximumCategoryLevelForMegaMenu
	 */
	public int getMaximumCategoryLevelForMegaMenu() {
		return mMaximumCategoryLevelForMegaMenu;
	}
	
	/**
	 * Sets the this property hold reference for MaximumCategoryLevelForMegaMenu.
	 * 
	 * @param pMaximumCategoryLevelForMegaMenu the MaximumCategoryLevelForMegaMenu to set
	 */
	public void setMaximumCategoryLevelForMegaMenu(int pMaximumCategoryLevelForMegaMenu) {
		mMaximumCategoryLevelForMegaMenu = pMaximumCategoryLevelForMegaMenu;
	}

	
	/**
	 * sets the Dimension Value Cache.
	 * 
	 * @return the dimensionValueCache
	 */
/*	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	*//**
	 * gets Dimension Value Cache.
	 * 
	 * @param pDimensionValueCache
	 *            the dimensionValueCache to set
	 *//*
	public void setDimensionValueCache(
			TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}*/

	/**
	 * Gets the catalogManager.
	 * 
	 * @return the catalogManager
	 */
/*	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	*//**
	 * Sets the catalogManager.
	 * 
	 * @param pCatalogManager
	 *            the catalogManager to set
	 *//*
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		mCatalogManager = pCatalogManager;
	}*/

	/**
	 * Gets the catalogProperties.
	 * 
	 * @return the catalogProperties
	 */
/*	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	*//**
	 * Sets the catalogProperties.
	 * 
	 * @param pCatalogProperties
	 *            the catalogProperties to set
	 *//*
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}*/

	/**
	 * Gets the related media content.
	 * 
	 * @return the related media content
	 */
	public String getRelatedMediaContent() {
		return mRelatedMediaContent;
	}

	/**
	 * Sets the related media content.
	 * 
	 * @param pRelatedMediaContent
	 *            the new related media content
	 */
	public void setRelatedMediaContent(String pRelatedMediaContent) {
		mRelatedMediaContent = pRelatedMediaContent;
	}

	/**
	 * Gets the image url.
	 * 
	 * @return the image url
	 */
	public String getImageUrl() {
		return mImageUrl;
	}

	/**
	 * Sets the image url.
	 * 
	 * @param pImageUrl
	 *            the new image url
	 */
	public void setImageUrl(String pImageUrl) {
		mImageUrl = pImageUrl;
	}

	/**
	 * Gets the url prefix.
	 *
	 * @return the url prefix
	 */
	public String getUrlPrefix() {
		return mUrlPrefix;
	}

	/**
	 * Sets the url prefix.
	 *
	 * @param pUrlPrefix the new url prefix
	 */
	public void setUrlPrefix(String pUrlPrefix) {
		this.mUrlPrefix = pUrlPrefix;
	}

	/**
	 * Gets the banner key.
	 *
	 * @return mBannerKey the banner key
	 */
	public String getBannerKey() {
		return mBannerKey;
	}

	/**
	 * sets the banner key.
	 * @param pBannerKey the BannerKey to set
	 */
	public void setBannerKey(String pBannerKey) {
		mBannerKey = pBannerKey;
	}

	/**
	 * Gets the TRUClassificationsTools.
	 *
	 * @return the TRUClassificationsTools
	 */
	
	public TRUClassificationsTools getClassificationsTools() {
		return mClassificationsTools;
	}
	
	/**
	 * Sets the TRUClassificationsTools.
	 * 
	 * @param pClassificationsTools the TRUClassificationsTools to set
	 */

	public void setClassificationsTools(TRUClassificationsTools pClassificationsTools) {
		mClassificationsTools = pClassificationsTools;
	}

	/**
	 * Gets the TRUClassificationMediaVO.
	 *
	 * @return the TRUClassificationMediaVO
	 */
	
/*	public TRUClassificationMediaVO getTRUClassificationMediaVO() {
		return mTRUClassificationMediaVO;
	}
	
	*//**
	 * Sets the TRUClassificationMediaVO.
	 * 
	 * @param pTRUClassificationMediaVO the TRUClassificationMediaVO to set
	 *//*

	public void setTRUClassificationMediaVO(
			TRUClassificationMediaVO pTRUClassificationMediaVO) {
		mTRUClassificationMediaVO = pTRUClassificationMediaVO;
	}*/
	
	/**
	 * Gets the TRUEndecaConfigurations.
	 *
	 * @return the TRUEndecaConfigurations
	 */
	
	public TRUEndecaConfigurations getTRUEndecaConfigurations() {
		return mTRUEndecaConfigurations;
	}

	/**
	 * Sets the TRUEndecaConfigurations.
	 * 
	 * @param pTRUEndecaConfigurations the TRUEndecaConfigurations to set
	 */

	public void setTRUEndecaConfigurations(TRUEndecaConfigurations pTRUEndecaConfigurations) {
		mTRUEndecaConfigurations = pTRUEndecaConfigurations;
	}
	
	/**
	 * Gets the megaMenuMaxAllowedRootCategories.
	 * 
	 * @return the megaMenuMaxAllowedRootCategories
	 */
	public String getMegaMenuMaxAllowedRootCategories() {
		return mMegaMenuMaxAllowedRootCategories;
	}

	/**
	 * Sets the megaMenuMaxAllowedRootCategories.
	 * 
	 * @param pMegaMenuMaxAllowedRootCategories the megaMenuMaxAllowedRootCategories to set
	 */
	public void setMegaMenuMaxAllowedRootCategories(String pMegaMenuMaxAllowedRootCategories) {
		mMegaMenuMaxAllowedRootCategories = pMegaMenuMaxAllowedRootCategories;
	}


	/**
	 * This Method is used for create the Mega Menu Object.
	 *
	 * @param pSiteId            - pSiteId
	 * @param pMaxLevel the max level
	 * @return - MegaMenuVO
	 */
	public MegaMenuVO createMegaMenuObject(String pSiteId, int pMaxLevel) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUMegaMenuHandler.createMegaMenuObject method.. {0}",pSiteId);
		}
		MegaMenuVO megaMenuObject = null;
		List<CategoryVO> rootCategoryVOs = null;
		if(getClassificationsTools() != null) {
			Map<ClassificationUtilKeyVO, TRUClassificationVO> allClassificationMap = getClassificationsTools().getAllClassificationMap();
			Map<String, List<String>> rootCategories = getClassificationsTools().getRootCategoryForSite();
			if(!StringUtils.isEmpty(pSiteId) && allClassificationMap != null && !allClassificationMap.isEmpty() && rootCategories != null && !rootCategories.isEmpty()) {
				List<String> rootCategoryIds = rootCategories.get(pSiteId);
				if(rootCategoryIds != null && !rootCategoryIds.isEmpty()) {
					rootCategoryVOs = createRootCategoryVOs(rootCategoryIds, allClassificationMap, pMaxLevel);
					Collections.sort(rootCategoryVOs,getDisplayOrderComparator());
					
				}
			}

			if (rootCategoryVOs != null && !rootCategoryVOs.isEmpty()) {
				megaMenuObject = new MegaMenuVO();
				megaMenuObject.setRootCategories(rootCategoryVOs);
			}
			
			if (isLoggingDebug()) {
				logDebug("END:: TRUMegaMenuHandler.createMegaMenuObject method..");
			}
		}
		return megaMenuObject;

	}



	/**
	 * This Method is used to create the Root Category VO object.
	 *
	 * @param pRootCategoryIds        -pRootCategoryIds
	 * @param pAllClassification          -pAllClassification
	 *            - Catalog root categories list
	 * @param pMaxLevel the max level
	 * @return List - List of root category VO's
	 */
	private List<CategoryVO> createRootCategoryVOs(List<String> pRootCategoryIds, Map<ClassificationUtilKeyVO, TRUClassificationVO> pAllClassification, int pMaxLevel) {

		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUDisplayMegaMenuDroplet.createRootCategoryVOs method..");
		}
		List<CategoryVO> megaMenuVO = new ArrayList<CategoryVO>();
		if(pRootCategoryIds != null && !pRootCategoryIds.isEmpty()) {
			for(String categoryId : pRootCategoryIds) {
				ClassificationUtilKeyVO classificationKey = new ClassificationUtilKeyVO();
				classificationKey.setClassificationId(categoryId);
				classificationKey.setRootCategoryId(categoryId);
				classificationKey.setLevel(TRUEndecaConstants.CONSTANT_ZERO);
				//classificationKey.setParentClassificationId(categoryId);
				TRUClassificationVO classificationVO = pAllClassification.get(classificationKey);
				
				boolean checkCategoryStatus = checkCategoryStatus(classificationVO);
				if(classificationVO != null && classificationVO.getChildClassifications() != null && !classificationVO.getChildClassifications().isEmpty() && checkCategoryStatus) {
					List<String> childClassifications = classificationVO.getChildClassifications();
					megaMenuVO = populateMegaMenuHierarchy(childClassifications, pAllClassification, categoryId, categoryId, TRUEndecaConstants.CONSTANT_ONE, pMaxLevel);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMegaMenuHandler.createRootCategoryVOs method..");
		}

		return megaMenuVO;
	}

	/**
	 * Check category status.
	 *
	 * @param pClassificationVO the classification vo
	 * @return true, if successful
	 */
	private boolean checkCategoryStatus(TRUClassificationVO pClassificationVO){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMegaMenuHandler.checkCategoryStatus method..");
		}
		String checkCatStatus = null;
		if(pClassificationVO != null){
			 checkCatStatus = pClassificationVO.getClassificationDisplayStatus();
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMegaMenuHandler.checkCategoryStatus method..");
		}
		if(!StringUtils.isEmpty(checkCatStatus)	&& checkCatStatus.equalsIgnoreCase(TRUEndecaConstants.NO_DISPLAY)){
			return false;
		}
		return true;
	}


	/**
	 * populateLevelOneSubCategories method used to populate all the level two related categories
	 * details.
	 *
	 * @param pChildCategoryId        -pChildCategoryId
	 * @param pAllClassification        -pAllClassification
	 * @param pParentCategoryId the parent category id
	 * @param pRootCategoryId  		-pRootCategoryId
	 * @param pLevel            - Level two category list
	 * @param pMaxLevel the max level
	 * @return List - List of level two category VO objects
	 */
	
	private List<CategoryVO> populateMegaMenuHierarchy(List<String> pChildCategoryId, Map<ClassificationUtilKeyVO, TRUClassificationVO> pAllClassification, String pParentCategoryId, String pRootCategoryId, int pLevel, int pMaxLevel) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMegaMenuHandler.populateLeveOneSubCategories method..");
		}
		if(pChildCategoryId == null || pChildCategoryId.isEmpty() || pLevel > pMaxLevel) {
			return null;
		}
		List<CategoryVO> categoryVOList = new ArrayList<CategoryVO>();
		for(String categoryId : pChildCategoryId) {
			ClassificationUtilKeyVO classificationKey = new ClassificationUtilKeyVO();
			classificationKey.setClassificationId(categoryId);
			classificationKey.setLevel(pLevel);
			classificationKey.setParentClassificationId(pParentCategoryId);
			classificationKey.setRootCategoryId(pRootCategoryId);
			TRUClassificationVO classificationVO = pAllClassification.get(classificationKey);
			boolean checkCategoryStatus = checkCategoryStatus(classificationVO);
			if(classificationVO != null && checkCategoryStatus) {
				CategoryVO categoryVO = getCategoryVO(classificationVO);
				List<String> childCategoryId = classificationVO.getChildClassifications();
				if (categoryVO!= null && !StringUtils.isEmpty(categoryVO.getCategoryURL())) {
					List<CategoryVO> childCategoryVO = populateMegaMenuHierarchy(childCategoryId, pAllClassification, categoryId, pRootCategoryId, pLevel+TRUEndecaConstants.INT_ONE, pMaxLevel);
					if(childCategoryVO != null && !childCategoryVO.isEmpty()) {
						//Begin Sorting  L2 and L3 Category Hierarchy
						Collections.sort(childCategoryVO,getDisplayOrderComparator());
						//End Sorting L2 and L3 Category Hierarchy
						
						//Added for mobile service
						if (childCategoryVO != null && childCategoryVO.size() > TRUEndecaConstants.INT_ZERO) {
							categoryVO.setSubCategoryCount(childCategoryVO.size());
						}
						categoryVO.setSubCategories(childCategoryVO);
					}
					categoryVOList.add(categoryVO);
				//	count++;
					
					
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMegaMenuHandler.populateLevelOneSubCategories method..");
		}
		return categoryVOList;
	}

	/**
	 * Gets the CategoryVO vo.
	 *
	 * @param pClassificationVO the classification vo
	 * @return the Category vo
	 */
	private CategoryVO getCategoryVO(TRUClassificationVO pClassificationVO) {
		if (isLoggingDebug()){
			vlogDebug("BEGIN:: TRUMegaMenuHandler.getCategoryVO method");
		}
		 String categoryImage = null;
		CategoryVO categoryVO = new CategoryVO();
		
		if(pClassificationVO != null) {
			String url = getCategoryUtils().getURL(pClassificationVO.getClaasificationId());
			categoryVO.setCategoryId(pClassificationVO.getClaasificationId());
			categoryVO.setCategoryName(pClassificationVO.getClaasificationName());
			categoryVO.setCategoryURL(url);
			if(StringUtils.isNotBlank(pClassificationVO.getClassificationDisplayStatus())) {
				categoryVO.setCategoryDisplayStatus(pClassificationVO.getClassificationDisplayStatus());
			}
			categoryVO.setCategoryDisplayOrder(pClassificationVO.getClassificationDisplayOrder());
		    categoryImage = getCategoryUtils().populateRelatedMedia(
		    		pClassificationVO.getRelatedMediaContent(),getTRUEndecaConfigurations().getCategoryImageKey());
			if(categoryImage != null)	{
				categoryVO.setCategoryImage(categoryImage);
			}
		} else {
			return null;
		}
		if (isLoggingDebug()){
			vlogDebug("END:: TRUMegaMenuHandler.getCategoryVO method");
		}
		return categoryVO;
	}
	
	
	/**
	 * Gets the MegaMenu MaxAllowed RootCategories.
	 * 
	 * @return the MegaMenu MaxAllowed RootCategories
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public int MegaMenuMaxAllowedRootCategories() throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUMegaMenuHandler::@method::MegaMenuMaxAllowedRootCategories() : START");
		}
		int getMegaMenuMaxAllowedRootCategories = 0;
		if (SiteContextManager.getCurrentSite() != null) {
			getMegaMenuMaxAllowedRootCategories = (int)SiteContextManager.getCurrentSite().getPropertyValue(
					getMegaMenuMaxAllowedRootCategories());
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUMegaMenuHandler::@method::MegaMenuMaxAllowedRootCategories() : END");
		}
		return getMegaMenuMaxAllowedRootCategories;
	}


}
