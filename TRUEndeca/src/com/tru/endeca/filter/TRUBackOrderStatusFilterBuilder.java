package com.tru.endeca.filter;
import atg.endeca.assembler.navigation.filter.RecordFilterBuilderImpl;


/**
 * The Class TRUBackOrderStatusFilterBuilder.
 * @version 1.0
 * @author PA
 */
public class TRUBackOrderStatusFilterBuilder extends RecordFilterBuilderImpl
{
	
	/** Property Holds BackOrderStatusFilter. */
	private String mBackOrderStatusFilter;
	
	/** Property Holds EnableBackOrderStatusFilter. */
	private Boolean mEnableBackOrderStatusFilter;

	/**
	 * Gets the BackOrderStatusFilter.
	 *
	 * @return pBackOrderStatusFilter
	 */
	public String getBackOrderStatusFilter() {
		return mBackOrderStatusFilter;
	}

	/**
	 * Sets the BackOrderStatusFilter.
	 *
	 * @param pBackOrderStatusFilter            the BackOrderStatusFilter to set  
	 */
	public void setBackOrderStatusFilter(String pBackOrderStatusFilter)
	{
		this.mBackOrderStatusFilter = pBackOrderStatusFilter;
	}

	 /**
 	 *  gets EnableBackOrderStatusFilter.
 	 *
 	 * @return EnableBackOrderStatusFilter
 	 */
	public Boolean getEnableBackOrderStatusFilter() {
		return mEnableBackOrderStatusFilter;
	}
	
	/**
	 *  sets the EnableBackOrderStatusFilter.
	 *
	 * @param pEnableBackOrderStatusFilter             the EnableBackOrderStatusFilter is set
	 */
	public void setEnableBackOrderStatusFilter(Boolean pEnableBackOrderStatusFilter) {
		this.mEnableBackOrderStatusFilter = pEnableBackOrderStatusFilter;
	}

	
	
	/**
	 * This Method is used to determine results list filters,Which will
	 * check the BackOrder Status and filters out the out of stock products.
	 * 
	 * @return - getPreOrder , getInStoreOnly
	 */
	@Override
	public String buildRecordFilter() {

		Boolean enableFilterState = getEnableBackOrderStatusFilter();
		if(enableFilterState != null && enableFilterState){
			String backOrderStatusFilter = getBackOrderStatusFilter();
			if(backOrderStatusFilter != null && !backOrderStatusFilter.isEmpty()){
				return getBackOrderStatusFilter();
			}
		}
		return null;
	}

}
