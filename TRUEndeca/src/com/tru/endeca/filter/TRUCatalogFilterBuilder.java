package com.tru.endeca.filter;

import atg.commerce.endeca.assembler.navigation.filter.CatalogFilterBuilder;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.endeca.constants.TRUEndecaConstants;
/**
 * This Class TRUCatalogFilterBuilder Gives Catalog Filter.
 *	@version 1.0 
 *	@author Professional Access
 */
public class TRUCatalogFilterBuilder extends CatalogFilterBuilder{
	/**
	 * This method will get all Catalog Filter.
	 * 
	 * @return buildRecordFilter()
	 */
	public String buildRecordFilter()
	  {
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
		if (isLoggingDebug()) {
			vlogDebug(currentRequest.getQueryString(),"query string");
		}
		
		if(currentRequest.getQueryString()!=null && currentRequest.getQueryString().contains(TRUEndecaConstants.NTT_PARAM)||currentRequest.getQueryString()!=null &&currentRequest.getQueryString().contains(TRUEndecaConstants.PROMOTION_ID) ){
			return null;
		}else{
			
		return super.buildRecordFilter();	
		}
	  }
}
