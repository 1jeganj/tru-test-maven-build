package com.tru.endeca.filter;

import atg.core.util.StringUtils;
import atg.endeca.assembler.navigation.filter.FilterUtils;
import atg.endeca.assembler.navigation.filter.PropertyConstraint;
import atg.endeca.assembler.navigation.filter.RecordFilterBuilderImpl;

/**
 * The Class TRUPriceExistsFilterBuilder.
 */
public class TRUPriceExistsFilterBuilder extends RecordFilterBuilderImpl {
	
	
	/** Property Holds EnableSupressFlagFilter. */
	private Boolean mEnablePriceFilter;
	
	/** The m filter property name. */
	private String mFilterPropertyName;
	
	/** The m filter property value. */
	private String mFilterPropertyValue;
	/**
	 * Gets the filter property name.
	 *
	 * @return the filter property name
	 */
	public String getFilterPropertyName() {
		return mFilterPropertyName;
	}

	/**
	 * Sets the filter property name.
	 *
	 * @param pFilterPropertyName the new filter property name
	 */
	public void setFilterPropertyName(String pFilterPropertyName) {
		this.mFilterPropertyName = pFilterPropertyName;
	}

	/**
	 * Gets the filter property value.
	 *
	 * @return the filter property value
	 */
	public String getFilterPropertyValue() {
		return mFilterPropertyValue;
	}

	/**
	 * Sets the filter property value.
	 *
	 * @param pFilterPropertyValue the new filter property value
	 */
	public void setFilterPropertyValue(String pFilterPropertyValue) {
		this.mFilterPropertyValue = pFilterPropertyValue;
	}

	/**
	 * Gets the enable price filter.
	 *
	 * @return the enable price filter
	 */
	public Boolean getEnablePriceFilter() {
		return mEnablePriceFilter;
	}

	/**
	 * Sets the enable price filter.
	 *
	 * @param pEnablePriceFilter the new enable price filter
	 */
	public void setEnablePriceFilter(Boolean pEnablePriceFilter) {
		this.mEnablePriceFilter = pEnablePriceFilter;
	}

	/* (non-Javadoc)
	 * @see atg.endeca.assembler.navigation.filter.RecordFilterBuilder#buildRecordFilter()
	 */
	@Override
	public String buildRecordFilter() {
		if(isLoggingDebug()){
			vlogDebug("TRUPriceExistsFilterBuilder : start of buildRecordFilter(),is price record filter enabled :{0}",getEnablePriceFilter());
		}
		String isPriceExistsFilter=null;
		Boolean enableFilterState = getEnablePriceFilter();
		if(enableFilterState != null && enableFilterState && StringUtils.isNotEmpty(getFilterPropertyName()) && 
				StringUtils.isNotEmpty(getFilterPropertyValue())){
			/*if(StringUtils.isNotEmpty(getFilterPropertyName()) && 
					StringUtils.isNotEmpty(getFilterPropertyValue())){*/
				PropertyConstraint isPriceExists = new PropertyConstraint(getFilterPropertyName(), getFilterPropertyValue());
				isPriceExistsFilter = FilterUtils.and(isPriceExists);
			/*}*/
		}
		if(isLoggingDebug()){
			vlogDebug("TRUPriceExistsFilterBuilder : end of buildRecordFilter(),Record Filter value :{0}",isPriceExistsFilter);
		}
		return isPriceExistsFilter;
	}

}
