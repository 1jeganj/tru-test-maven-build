
package com.tru.endeca.filter;

import atg.endeca.assembler.navigation.filter.RecordFilterBuilderImpl;

import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * The Class TRUSupressFlagFilterBuilder used to suppress sku from result list, having SuppressInSearch property true. 
 * @author PA
 */
public class TRUSupressFlagFilterBuilder  extends RecordFilterBuilderImpl{
	/** Property Holds SupressFlag. */
	private String mSupressFlagFilter;
	
	/** Property Holds EnableSupressFlagFilter. */
	private Boolean mEnableSupressFlagFilter;
	
	/**
	 * Gets the suppress flag filter.
	 *
	 * @return the suppress flag filter
	 */
	public String getSupressFlagFilter() {
		return mSupressFlagFilter;
	}
	/**
	 * Sets the suppress flag filter.
	 *
	 * @param pSupressFlagFilter the new suppress flag filter
	 */
	public void setSupressFlagFilter(String pSupressFlagFilter) {
		this.mSupressFlagFilter = pSupressFlagFilter;
	}
	/**
	 * Gets the enable suppress flag filter.
	 *
	 * @return the enable suppress flag filter
	 */
	public Boolean getEnableSupressFlagFilter() {
		return mEnableSupressFlagFilter;
	}

	/**
	 * Sets the enable suppress flag filter.
	 *
	 * @param pEnableSupressFlagFilter the new enable suppress flag filter
	 */
	public void setEnableSupressFlagFilter(Boolean pEnableSupressFlagFilter) {
		this.mEnableSupressFlagFilter = pEnableSupressFlagFilter;
	}

	/**
	 * This Method is used to suppress sku from result list, having SuppressInSearch property true.
	 *
	 * @return the string
	 */
	@Override
	public String buildRecordFilter() {
		Boolean enableFilterState = getEnableSupressFlagFilter();
		if(enableFilterState != null && enableFilterState){
			String supressFlag = getSupressFlagFilter();
			if(supressFlag != null && !supressFlag.isEmpty()){
				return TRUEndecaConstants.AND_FORWARD_PARENTHESES+getSupressFlagFilter()+TRUEndecaConstants.BACK_PARENTHESES;
			}
		}
		return null;
	}
	
}
