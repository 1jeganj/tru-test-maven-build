package com.tru.endeca.filter;

import atg.endeca.assembler.navigation.filter.SiteFilterBuilder;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.endeca.constants.TRUEndecaConstants;
/**
 * This Class TRUSiteFilterBuilder Gives Site Filter.
 *	@version 1.0 
 *	@author PA
 */
public class TRUSiteFilterBuilder extends SiteFilterBuilder {
	/**
	 * used for  RecordFilter for Site.
	 * @param  Record.
	 * @return buildRecordFilter()
	 */
	public String buildRecordFilter()
	  {
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
		if (isLoggingDebug()) {
			vlogDebug("query string",currentRequest.getQueryString());
		}
		if(currentRequest.getQueryString()!=null && currentRequest.getQueryString().contains(TRUEndecaConstants.NTT_PARAM)||currentRequest.getQueryString()!=null &&currentRequest.getQueryString().contains(TRUEndecaConstants.PROMOTION_ID) ){
			return null;
		}else{
			
		return super.buildRecordFilter();	
		}
		
	  }
}
