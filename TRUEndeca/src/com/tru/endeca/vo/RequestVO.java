package com.tru.endeca.vo;

import com.tru.commerce.TRUCommerceConstants;

/**
 * This class hold mega menu request related information.
 *
 * @author Professional Access
 * @version 1.0
 */
public class RequestVO {

	/**
	 * Property to hold CatalogId.
	 */
	private String mCatalogId;
	
	/**
	 * Property to hold CategoryId.
	 */
	private String mCategoryId;
	
	/**
	 * Property to hold productId.
	 */
	private String mProductId;

	/**
	 * Property to hold Locale.
	 */
	private String mLocale;

	/**
	 * Property to hold RequestFor.
	 */
	private String mRequestFor;

	/**
	 * Property to hold Page Name.
	 */
	private String mPageName;
	
	/**
	 * Property to hold Site Id.
	 */
	private String mSiteId;
	/**
	 * Property to hold Endeca Navigation ID.
	 */
	private String mNavigationId;
	
	/**
	 * Property to hold mRequestFrom.
	 */
	private String mRequestFrom;
	
	/**
	 * Gets the catalogId.
	 * 
	 * @return the catalogId
	 */
	public String getCatalogId() {
		return mCatalogId;
	}

	/**
	 * Sets the catalogId.
	 * 
	 * @param pCatalogId the catalogId to set
	 */
	public void setCatalogId(String pCatalogId) {
		mCatalogId = pCatalogId;
	}

	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * Sets the locale.
	 * 
	 * @param pLocale the locale to set
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}

	/**
	 * Gets the requestFor.
	 * 
	 * @return the requestFor
	 */
	public String getRequestFor() {
		return mRequestFor;
	}

	/**
	 * Sets the requestFor.
	 * 
	 * @param pRequestFor the requestFor to set
	 */
	public void setRequestFor(String pRequestFor) {
		mRequestFor = pRequestFor;
	}	

	/**
	 * Gets the Page Name.
	 * @return the pageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the Page Name.
	 * @param pPageName the pageName to set
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}

	/**
	 * Gets Site Id.
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the site id.
	 *
	 * @param pSiteId the siteId to set.
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	/**
	 * Gets the navigation id.
	 *
	 * @return the navigationId.
	 */
	public String getNavigationId() {
		return mNavigationId;
	}

	/**
	 * Sets the navigation id.
	 *
	 * @param pNavigationId the navigationId to set.
	 */
	public void setNavigationId(String pNavigationId) {
		mNavigationId = pNavigationId;
	}

	/**
	 * This method over rides OOTB hashCode method.
	 * 
	 * @return int - Memory value
	 */
	@Override
	public int hashCode() {
		int prime = TRUCommerceConstants.HASH_CODE_PRIME;
		int result = TRUCommerceConstants.ONE;
		result = prime * result + (mCatalogId == null ? 0 : mCatalogId.hashCode());
		result = prime * result + (mLocale == null ? 0 : mLocale.hashCode());
		result = prime * result + (mRequestFor == null ? 0 : mRequestFor.hashCode());
		result = prime * result + (mPageName == null ? 0 : mPageName.hashCode());
		result = prime * result + (mSiteId == null ? 0 : mSiteId.hashCode());
		result = prime * result + (mNavigationId == null ? 0 : mNavigationId.hashCode());
		result = prime * result + (mCategoryId == null ? 0 : mCategoryId.hashCode());
		result = prime * result + (mProductId == null ? 0 : mProductId.hashCode());
		result = prime * result + (mRequestFrom == null ? 0 : mRequestFrom.hashCode());
		return result;
	}

	/**
	 * Over-ridden the OOTB equals method.
	 * 
	 * @param pObject - Object to compare
	 * @return boolean - Return true if both object content are equal otherwise false
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject) {
			return true;
		}
		if (pObject == null) {
			return false;
		}
		if (getClass() != pObject.getClass()) {
			return false;
		}
		RequestVO other = (RequestVO) pObject;
		//Catalog Id
		if (mCatalogId == null) {
			if (other.mCatalogId != null) {
				return false;
			}
		} else if (!mCatalogId.equals(other.mCatalogId)) {
			return false;
		}
		//CategoryId
		if (mCategoryId == null) {
			if (other.mCategoryId != null) {
				return false;
				}
			} else if (!mCategoryId.equals(other.mCategoryId)) {
				return false;
		}
		//ProductId
		if (mProductId == null) {
			if (other.mProductId != null) {
				return false;
				}
			} else if (!mProductId.equals(other.mProductId)) {
				return false;
		}
		//Locale
		if (mLocale == null) {
			if (other.mLocale != null) {
				return false;
			}
		} else if (!mLocale.equals(other.mLocale)) {
			return false;
		}
		//Request For
		if (mRequestFor == null) {
			if (other.mRequestFor != null) {
				return false;
			}
		} else if (!mRequestFor.equals(other.mRequestFor)) {
			return false;
		}
		//Page Name
		if (mPageName == null) {
			if (other.mPageName != null) {
				return false;
			}
		} else if (!mPageName.equals(other.mPageName)) {
			return false;
		}
		//Site Id
		if (mSiteId == null) {
			if (other.mSiteId != null) {
				return false;
			}
		} else if (!mSiteId.equals(other.mSiteId)) {
			return false;
		}
		//Endeca Navigation Id
		if (mNavigationId == null) {
			if (other.mNavigationId != null) {
				return false;
			}
		} else if (!mNavigationId.equals(other.mNavigationId)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the Product Id.
	 *
	 * @return the productId
	 */
	public String getProductId() {
		return mProductId;
	}

	/**
	 * Sets the Product Id.
	 *
	 * @param pProductId the new Product Id
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}
	
	/**
	 * Gets the category id.
	 *
	 * @return the category id
	 */
	public String getCategoryId() {
		return mCategoryId;
	}

	/**
	 * Sets the category id.
	 *
	 * @param pCategoryId the new category id
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}

	/**
	 * @return the requestFrom
	 */
	public String getRequestFrom() {
		return mRequestFrom;
	}

	/**
	 * @param pRequestFrom the requestFrom to set
	 */
	public void setRequestFrom(String pRequestFrom) {
		mRequestFrom = pRequestFrom;
	}
}
