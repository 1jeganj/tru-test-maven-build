package com.tru.endeca.vo;

import java.util.List;

import com.tru.commerce.catalog.vo.CategoryVO;

/** This class holds Mega Menu category related information.
 * 
 * 
 * @version 1.0
 * @author Professional Access
 */
public class MegaMenuVO extends EndecaResponseVO {

	/**
	 * Property to hold RootCategories.
	 */
	private List<CategoryVO> mRootCategories;

	/**
	 * Gets the rootCategories.
	 * 
	 * @return the rootCategories
	 */
	public List<CategoryVO> getRootCategories() {
		return mRootCategories;
	}

	/**
	 * Sets the rootCategories.
	 * 
	 * @param pRootCategories the rootCategories to set
	 */
	public void setRootCategories(List<CategoryVO> pRootCategories) {
		mRootCategories = pRootCategories;
	}
}
