package com.tru.endeca.vo;

import java.util.List;

import com.tru.commerce.catalog.vo.CategoryVO;


/**
 * This class holds Breadcrumbs  category related information.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class BreadCrumbsVO extends EndecaResponseVO {
	
	/**
	 * Holds the Category Name.
	 */
	private String mCategoryName;
	
	/**
	 * Property to hold category URL.
	 */
	private String mCategoryURL;

	
	/**
	 * Holds the Child Categories.
	 */
	private List<String> mChildCategories;
	
	/**
	 * Property to hold parent Categories.
	 */
	private List<String> mParentCategories;
		/**
	 * Property to hold RootCategories.
	 */
	private List<CategoryVO> mRootCategories;
	
	/**
	 * Holds the selected root classification Id
	 */
	private String mCategoryId;
	/**
	 * Holds the selected root classification Name.
	 */
	private String mSelectedRootCategory;
	
	/** Property to hold subCategories. */
	private List<CategoryVO> mSubCategories;
	
	/**
	 * Property to hold categoryImage.
	 */
	private String mCategoryImage;
	
	/** The m category displayOrder. */
	private int mCategoryDisplayOrder;
	
	/**
	 * @return the rootCategoryName.
	 */
	public String getCategoryName() {
		return mCategoryName;
	}

	/**
	 * @param pCategoryName the rootCategoryName to set.
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}

	/**
	 * Gets the categoryURL.
	 * 
	 * @return the categoryURL.
	 */
	
	public String getCategoryURL() {
		return mCategoryURL;
	}

	/**
	 * Sets the categoryURL.
	 * 
	 * @param pCategoryURL the categoryURL to set.
	 */
	public void setCategoryURL(String pCategoryURL) {
		mCategoryURL = pCategoryURL;
	}
	
	/**
	 * @return returns the parent root category Id.
	 */
	public String getCategoryId() {
		return mCategoryId;
	}

	/**
	 * @param pCategoryId sets the selected root category Id.
	 */
	public void setCategoryId(String pCategoryId) {
		this.mCategoryId = pCategoryId;
	}

	/**
	 * @return the root Category.
	 */
	public String getSelectedRootCategory() {
		return mSelectedRootCategory;
	}

	/**
	 * @param pSelectedRootCategory set the selected classification.
	 */
	public void setSelectedRootCategory(String pSelectedRootCategory) {
		this.mSelectedRootCategory = pSelectedRootCategory;
	}

	/**
	 * Gets the rootCategories.
	 * 
	 * @return rootCategories.
	 */
	public List<CategoryVO> getRootCategories() {
		return mRootCategories;
	}

	/**
	 * Sets the rootCategories.
	 * 
	 * @param pRootCategories the rootCategories to set.
	 */
	public void setRootCategories(List<CategoryVO> pRootCategories) {
		mRootCategories = pRootCategories;
	}
	/**
	 * Gets the subCategories.
	 *
	 * @return the subCategories.
	 */
	public List<CategoryVO> getSubCategories() {
		return mSubCategories;
	}

	/**
	 * Sets the subCategories.
	 *
	 * @param pSubCategories the subCategories to set.
	 */
	public void setSubCategories(List<CategoryVO> pSubCategories) {
		mSubCategories = pSubCategories;
	}
	/**
	 * Gets the Category Image.
	 * @return  category Image.
	 */
	public String getCategoryImage() {
		return mCategoryImage;
	}

	/**
	 * sets the Category Image.
	 * @param pCategoryImage the category image to set.
	 */
	public void setCategoryImage(String pCategoryImage) {
		this.mCategoryImage = pCategoryImage;
	}
	
	/**
	 * @return the parentCategories.
	 */
	public List<String> getParentCategories() {
		return mParentCategories;
	}

	/**
	 * @param pParentCategories the parentCategories to set.
	 */
	public void setParentCategories(List<String> pParentCategories) {
		mParentCategories = pParentCategories;
	}

	/**
	 * @return the childCategories.
	 */
	public List<String> getChildCategories() {
		return mChildCategories;
	}

	/**
	 * @param pChildCategories the childCategories to set.
	 */
	public void setChildCategories(List<String> pChildCategories) {
		mChildCategories = pChildCategories;
	}

	/**
	 * @return the categoryDisplayOrder
	 */
	public int getCategoryDisplayOrder() {
		return mCategoryDisplayOrder;
	}

	/**
	 * @param pCategoryDisplayOrder the categoryDisplayOrder to set
	 */
	public void setCategoryDisplayOrder(int pCategoryDisplayOrder) {
		mCategoryDisplayOrder = pCategoryDisplayOrder;
	}
}

