package com.tru.endeca.vo;

/**Represents the Endeca Response Value Object.
 * 
 * 
 * @version 1.0
 * @author Professional Access
 */
public class EndecaResponseVO {

	/**
	 * Property to hold Response For.
	 */
	private String mResponseFor;

	/**
	 * Gets the responseFor.
	 * 
	 * @return the responseFor
	 */
	public String getResponseFor() {
		return mResponseFor;
	}

	/**
	 * Sets the responseFor.
	 * 
	 * @param pResponseFor the responseFor to set
	 */
	public void setResponseFor(String pResponseFor) {
		mResponseFor = pResponseFor;
	}
}
