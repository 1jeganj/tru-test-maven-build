/**
 * 
 */
package com.tru.endeca.vo;
/**
 * This Class ShopByDimensionVO holds the property of DimensionId,DimensionName,mDimensionURL.
 * @version 1.0
 * @author PA
 */
public class ShopByDimensionVO {
	
	/**
	 * Property to hold DimensionId.
	 */
	private String mDimensionId;
	
	/**
	 * Property to hold DimensionName.
	 */
	private String mDimensionName;
	
	/**
	 * Property to hold DimensionURL.
	 */
	private String mDimensionURL;

	/**
	 * Gets the dimensionId.
	 * @return the dimensionId
	 */
	public String getDimensionId() {
		return mDimensionId;
	}

	/**
	 * Sets the dimensionId.
	 * @param pDimensionId the dimensionId to set
	 */
	public void setDimensionId(String pDimensionId) {
		mDimensionId = pDimensionId;
	}

	/**
	 * Gets the dimensionName.
	 * @return the dimensionName
	 */
	public String getDimensionName() {
		return mDimensionName;
	}

	/**
	 * Sets the dimensionName.
	 * @param pDimensionName the dimensionName to set
	 */
	public void setDimensionName(String pDimensionName) {
		mDimensionName = pDimensionName;
	}

	/**
	 * Gets the dimensionURL.
	 * @return the dimensionURL
	 */
	public String getDimensionURL() {
		return mDimensionURL;
	}

	/**
	 * Sets the dimensionURL.
	 * @param pDimensionURL the dimensionURL to set
	 */
	public void setDimensionURL(String pDimensionURL) {
		mDimensionURL = pDimensionURL;
	}

}
