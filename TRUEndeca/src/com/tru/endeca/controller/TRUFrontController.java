package com.tru.endeca.controller;

import com.tru.commerce.exception.TRUEndecaException;
import com.tru.endeca.vo.EndecaResponseVO;
import com.tru.endeca.vo.RequestVO;


/**
 * This interface used as a front controller for Endeca request.
 * 
 * @version 1.0
 * @author Professional Access
 */
public interface TRUFrontController {

	/**
	 * Dispatches the request to appropriate manager based on the type.
	 * 
	 * @param pRequestVO - pRequestVO
	 * @return EndecaResponseVO - EndecaResponseVO
	 * @throws TRUEndecaException - Custom Exception
	 */
	EndecaResponseVO dispatchRequest(RequestVO pRequestVO) throws TRUEndecaException;
}
