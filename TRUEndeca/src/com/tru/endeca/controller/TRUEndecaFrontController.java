package com.tru.endeca.controller;

import atg.nucleus.GenericService;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.exception.TRUEndecaException;
import com.tru.endeca.model.handler.TRUMegaMenuHandler;
import com.tru.endeca.vo.EndecaResponseVO;
import com.tru.endeca.vo.RequestVO;

/**
 * TRUEndecaFrontController acts as front controller to handle MegaMenu Requests. 
 * 
 * @version 1.0
 * @author Professional Access.
 * 
 */
public class TRUEndecaFrontController extends GenericService implements TRUFrontController {

	/**
	 * Property to hold MegaMenu handler.
	 */
	private TRUMegaMenuHandler mMegaMenuHandler;

	/**
	 * This method is used for dispatching the Request to appropriate manager.
	 * 
	 * @param pRequestVO - Request value Object
	 * @return EndecaResponseVO - Endeca Response Object
	 * @throws TRUEndecaException - Custom Exception
	 */
	@Override
	public EndecaResponseVO dispatchRequest(RequestVO pRequestVO) throws TRUEndecaException {
		if (pRequestVO == null) {
			return null;
		}
		//Handle MegaMenu Requests
		if (TRUCommerceConstants.MEGA_MENU.equals(pRequestVO.getRequestFor())) {
			return getMegaMenuHandler().createMegaMenuObject(pRequestVO.getSiteId(), getMegaMenuHandler().getMaximumCategoryLevelForMegaMenu());
		}
		return null;
	}

	/**
	 * Gets the megaMenuHandler.
	 * 
	 * @return the megaMenuHandler
	 */
	public TRUMegaMenuHandler getMegaMenuHandler() {
		return mMegaMenuHandler;
	}

	/**
	 * Sets the megaMenuHandler.
	 * 
	 * @param pMegaMenuHandler the megaMenuHandler to set
	 */
	public void setMegaMenuHandler(TRUMegaMenuHandler pMegaMenuHandler) {
		mMegaMenuHandler = pMegaMenuHandler;
	}
}
