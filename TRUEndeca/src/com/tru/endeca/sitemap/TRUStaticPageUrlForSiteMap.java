package com.tru.endeca.sitemap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * The Class TRUStaticPageUrlForSiteMap used to create Static page url dynamically. 
 * It generate URL based through Endeca promote content zip file and write to static.txt file under sitemap conf.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUStaticPageUrlForSiteMap extends GenericService{
	
	/** The Constant CURRENT_APPLICATION_CONFIG_TEXT_FILE. */
	private static final String CURRENT_APPLICATION_CONFIG_TEXT_FILE = "current_application_config.txt";
	
	/** The Constant ZIP_EXTENSION. */
	private static final String ZIP_EXTENSION = ".zip";

	/** The Constant STATIC_PAGE_FILE_NAME. */
	private static final String STATIC_PAGE_FILE_NAME = "staticpages.txt";
	
	/** The Constant FILE_SEPERATOR. */
	private static final char FILE_SEPERATOR = '/';
	
	/** The Constant PAGES. */
	private static final String PAGES = "pages";
	
	/** The m tru get site domain url mTRUSiteMapDomainUrl. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;
	
	/** The m source zip file location. */
	private File mSourceZipFileLocation;
	
	/** The m destination zip file location. */
	private String mDestinationZipFileLocation;
	
	/** The static page folder name. */
	private List<String> mStaticPageFolderName;
	
	/** The m static page url list. */
	private List<String> mStaticPageUrlList;
	
	/** The m sitemap conf file location. */
	private Map<String, String> mSitemapConfFileLocation;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	
	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;

	/**
	 * Copy and de-compress zip file.
	 *
	 * @return the string
	 */
	protected String copyAndDecompressZipFile(){
		String recentZipFileName = null;
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.copyAndDecompressZipFile method..");
		}
		try {
			recentZipFileName = getRecentZipFileName();
			if(recentZipFileName == null){
				if (isLoggingDebug()) {
					vlogDebug("END:: TRUStaticPageUrlForSiteMap.copyAndDecompressZipFile method..  recentZipFileName: {0}", recentZipFileName);
				}
				return null;
			}
			copyPromoteContentZipFile(recentZipFileName);
			decompressZipFile(recentZipFileName);
		} catch (IOException ioException) {
			if (isLoggingError()) {
				logError("Exception TRUStaticPageUrlForSiteMap.copyAndDecompressZipFile...", ioException);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUStaticPageUrlForSiteMap.copyAndDecompressZipFile method..recentZipFileName: {0}", recentZipFileName);
		}
		return recentZipFileName;
	}
	
	/**
	 * Generate static page url.
	 *
	 * @param pSiteName the site name
	 * @param pRecentZipFileName the recent zip file name
	 */
	protected void generateStaticPageUrl(String pSiteName, String pRecentZipFileName){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.generateStaticPageUrl() method..");
		}
		try {
			List<String> staticPageFolderName = getTRUSiteMapDomainUrl().getConfiguredStaticPageFolderName(pSiteName);
			
			if (isLoggingDebug()) {
				vlogDebug("TRUStaticPageUrlForSiteMap.All considered static folder..  staticFolderName: {0}", staticPageFolderName);
			}
			
			mStaticPageUrlList = new ArrayList<String>();
			filterStaticPageUrl(staticPageFolderName, pSiteName, pRecentZipFileName);
			
			List<String> staticPageUrlList = checkStaticPageUrlResponse(mStaticPageUrlList);
			
			if (isLoggingDebug()) {
				vlogDebug("TRUStaticPageUrlForSiteMap.All valid static page url..  staticPageUrlList: {0}", staticPageUrlList);
			}
			
			writeUrlToSiteMapStaticFile(staticPageUrlList,pSiteName);
			
		} catch (IOException ioException) {
			//send failure mail : Failure during static page url generation.
			
			if (isLoggingError()) {
				logError("Exception TRUStaticPageUrlForSiteMap.generateStaticPageUrl...", ioException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.generateStaticPageUrl() method..");
		}
	}
	
	
	/**
	 * Gets the recent zip file name.
	 *
	 * @return the recent zip file name
	 * @throws FileNotFoundException the file not found exception
	 */
	private String getRecentZipFileName() throws FileNotFoundException{
		String currentZipFileName = null;
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.getRecentZipFileName() method..");
		}
		if(getSourceZipFileLocation() == null) {
			return null;
		}
		String zipFileLocation = getSourceZipFileLocation().getAbsolutePath();
		if(StringUtils.isEmpty(zipFileLocation)){
			return null;
		}
		String currentAppConfigFilePath = zipFileLocation.concat(File.separator).concat(CURRENT_APPLICATION_CONFIG_TEXT_FILE);
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(new FileReader(currentAppConfigFilePath));
		while (in.hasNext()) { 
			currentZipFileName = in.next();
		} 
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.getRecentZipFileName() method..");
		}
		return currentZipFileName;
	}
	
	/**
	 * Copy promote content zip file.
	 *
	 * @param pRecentZipFileName the recent zip file name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void copyPromoteContentZipFile(String pRecentZipFileName) throws IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.copyPromoteContentZipFile() method..");
		}
		String zipFileLocation = getSourceZipFileLocation().getAbsolutePath();
		if(StringUtils.isEmpty(zipFileLocation)){
			return;
		}
		String sourceZipFilePath = zipFileLocation.concat(File.separator);
		String destinationPath =getDestinationZipFileLocation();
	
		File srcFile = new File(sourceZipFilePath.concat(pRecentZipFileName));
		File destDir = new File(destinationPath);
		
		FileUtils.copyFileToDirectory(srcFile, destDir);
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.copyPromoteContentZipFile() method..");
		}
	}
	
    /**
     * De-compress zip file.
     *
     * @param pZipFileName the zip file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
	private void decompressZipFile(String pZipFileName) throws IOException {
    	if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.decompressZipFile() method..");
		}
    	String outputFolder= getDestinationZipFileLocation().concat(File.separator);
    	 File ff = new File(outputFolder.concat(pZipFileName));
    	 String recentZipFileNameNoExtn = pZipFileName.replace(ZIP_EXTENSION, TRUEndecaConstants.EMPTY_STRING);
    	if(!ff.exists()){
    		if (isLoggingDebug()) {
    			logDebug("END:: TRUStaticPageUrlForSiteMap.decompressZipFile() method.Zip file not exist.");
    		}
    		return ;
    	}
    	String extrectedZippedFolder = outputFolder + File.separator + recentZipFileNameNoExtn + File.separator;
        ZipInputStream zis = new ZipInputStream(new FileInputStream(outputFolder.concat(pZipFileName)));
        
        ZipEntry ze = zis.getNextEntry();
        while(ze!=null){
            String entryName = ze.getName();
            File f = new File(extrectedZippedFolder +  entryName);
            f.getParentFile().mkdir();
            ze = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
      
        ff.delete();
        if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.decompressZipFile() method..");
		}
    }
	
	/**
	 * Filter static page url.
	 *
	 * @param pStaticPageFolederName the static page foleder name
	 * @param pSiteName the site name
	 * @param pRecentZipFileName the recent zip file name
	 */
	private void filterStaticPageUrl(List<String> pStaticPageFolederName, String pSiteName, String pRecentZipFileName){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.filterStaticPageUrl() method..");
		}
		if(pStaticPageFolederName == null || pStaticPageFolederName.isEmpty() || StringUtils.isEmpty(pRecentZipFileName)){
			if (isLoggingDebug()) {
				logDebug("END:: TRUStaticPageUrlForSiteMap.filterStaticPageUrl() method.No StaticPageFolederName or RecentZipFileName");
			}
			return ;
		}
		String recentZipFileNameNoExtn = pRecentZipFileName.replace(ZIP_EXTENSION, TRUEndecaConstants.EMPTY_STRING);
		StringBuilder pathName = new StringBuilder();
			pathName.append(getDestinationZipFileLocation());
			pathName.append(File.separator);
			pathName.append(recentZipFileNameNoExtn);
			pathName.append(File.separator);
			pathName.append(PAGES);
			pathName.append(File.separator);
			pathName.append(pSiteName);
			pathName.append(File.separator);
		String path = pathName.toString();
		
		for (String folderName : pStaticPageFolederName) {
			File file = new File(path + folderName);
			getStaticPageUrl(file,pSiteName);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.filterStaticPageUrl() method..");
		}
	}
	
	/**
	 * Gets the static page url.
	 *
	 * @param pStaticFolder the static folder
	 * @param pSiteName the site name
	 */
	private void getStaticPageUrl(File pStaticFolder, String pSiteName){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.getStaticPageUrl() method..");
		}
		if(null == pStaticFolder){
			return ;
		}
    	if(pStaticFolder.isDirectory()) {
    		String filePath = pStaticFolder.getAbsolutePath();
    		if(!StringUtils.isEmpty(filePath) && filePath.contains(File.separator)){
    			String url= filePath.replace(File.separatorChar, FILE_SEPERATOR);
    			url = url.substring(url.lastIndexOf(pSiteName) + pSiteName.length());
    			mStaticPageUrlList.add(url);
    		}
    		
        File[] children = pStaticFolder.listFiles();
        if(null != children) {
            for(File f : children) {
            		getStaticPageUrl(f,pSiteName);
            	}
        	}
    	}
    	if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.getStaticPageUrl() method..");
		}
	}
	
	/**
	 * Check static page url response.
	 *
	 * @param pStaticUrl the static url
	 * @return the list
	 */
	private List<String> checkStaticPageUrlResponse(List<String> pStaticUrl){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.checkStaticPageUrlResponse() method..");
		}
		List<String> response200UrlList = new ArrayList<String>();
		//String domainUrl = getSchemeDetectionUtil().getScheme().concat(getTRUSiteMapDomainUrl().getDomainUrl());
		String domainUrl = getTRUEndecaUtils().getProtocolScheme().concat(getTRUSiteMapDomainUrl().getDomainUrl());
		// For home page.
		response200UrlList.add(domainUrl);
		if(pStaticUrl == null || pStaticUrl.isEmpty() || StringUtils.isEmpty(domainUrl)){
			return response200UrlList;
		}
		for (String url : pStaticUrl) {
			int responseCode = getTRUSiteMapDomainUrl().hitUrlToInitializeDimensionValueCache(domainUrl+url);
			if(responseCode == TRUEndecaConstants.RESPONSE_CODE_200){
				response200UrlList.add(domainUrl+url);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("TRUStaticPageUrlForSiteMap.checkStaticPageUrlResponse() method .All valid static page url..  staticPageUrlList: {0}", response200UrlList);
		}
		return response200UrlList;
	}
	
	/**
	 * Write url to site map static file.
	 *
	 * @param pStaticUrlList the static url list
	 * @param pSiteName the site name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void writeUrlToSiteMapStaticFile(List<String> pStaticUrlList, String pSiteName) throws IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.writeUrlToSiteMapStaticFile() method..");
		}
		if(pStaticUrlList == null || pStaticUrlList.isEmpty() ){
			return ;
		}
		StringBuilder filePath = new StringBuilder();
			filePath.append(getSitemapConfFileLocation().get(pSiteName));
			filePath.append(File.separator);
			filePath.append(STATIC_PAGE_FILE_NAME);
			
			if (isLoggingDebug()) {
				vlogDebug("TRUStaticPageUrlForSiteMap.writeUrlToSiteMapStaticFile..  staticpages.txt path: {0}", filePath.toString());
			}
		
		File staticFile = new File(filePath.toString());
		
		if(!staticFile.exists()){
			staticFile.createNewFile();
		}
		
		FileWriter fw = new FileWriter(staticFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
			for (String url : pStaticUrlList) {
				bw.write(url);
				bw.newLine();
			}
		bw.close();
		mStaticPageUrlList.clear();
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.writeUrlToSiteMapStaticFile() method.Url written to staticpages.txt successfully.");
		}
	}
	
	/**
	 * Delete un zipped dir.
	 *
	 * @param pRecentZipFileName the recent zip file name
	 */
	protected void deleteUnZippedDir(String pRecentZipFileName){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.deleteUnZippedDir() method.");
		}
		if(StringUtils.isEmpty(pRecentZipFileName)){
			return ;
		}
		String recentZipFileNameNoExtn = pRecentZipFileName.replace(ZIP_EXTENSION, TRUEndecaConstants.EMPTY_STRING);
		
		String unZipFolderLocation = getDestinationZipFileLocation().concat(File.separator).concat(recentZipFileNameNoExtn);
		try {
			FileUtils.deleteDirectory(new File(unZipFolderLocation));
		} catch (IOException ioException) {
			if (isLoggingError()) {
				logError("Exception occured while delete unzipped directory...", ioException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.deleteUnZippedDir() method.UnZip folder deleted successfully.");
		}
	}
	
	/**
	 * Sets the site map failure email template.
	 *
	 * @return the map
	 */
	/*private Map<String, Object> setSiteMapFailureEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStaticPageUrlForSiteMap.setSiteMapFailureEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, TRUCommerceConstants.SITE_MAP);
		if (isLoggingDebug()) {
			logDebug("END:: TRUStaticPageUrlForSiteMap.setSiteMapFailureEmailTemplate() method.. ");
		}
		return templateParameters;
	}*/
	/**
	 * Gets the TRU site map domain url.
	 *
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * Sets the TRU site map domain url.
	 *
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}

	/**
	 * Gets the source zip file location.
	 *
	 * @return the source zip file location
	 */
	public File getSourceZipFileLocation() {
		return mSourceZipFileLocation;
	}

	/**
	 * Sets the source zip file location.
	 *
	 * @param pSourceZipFileLocation the new source zip file location
	 */
	public void setSourceZipFileLocation(File pSourceZipFileLocation) {
		mSourceZipFileLocation = pSourceZipFileLocation;
	}

	/**
	 * Gets the destination zip file location.
	 *
	 * @return the destination zip file location
	 */
	public String getDestinationZipFileLocation() {
		return mDestinationZipFileLocation;
	}

	/**
	 * Sets the destination zip file location.
	 *
	 * @param pDestinationZipFileLocation the new destination zip file location
	 */
	public void setDestinationZipFileLocation(
			String pDestinationZipFileLocation) {
		mDestinationZipFileLocation = pDestinationZipFileLocation;
	}

	/**
	 * Gets the static page folder name.
	 *
	 * @return the static page folder name
	 */
	public List<String> getStaticPageFolderName() {
		return mStaticPageFolderName;
	}

	/**
	 * Sets the static page folder name.
	 *
	 * @param pStaticPageFolderName the new static page folder name
	 */
	public void setStaticPageFolderName(List<String> pStaticPageFolderName) {
		mStaticPageFolderName = pStaticPageFolderName;
	}

	/**
	 * Sets the sitemap conf file location.
	 *
	 * @param pSitemapConfFileLocation the sitemap conf file location
	 */
	public void setSitemapConfFileLocation(
			Map<String, String> pSitemapConfFileLocation) {
		this.mSitemapConfFileLocation = pSitemapConfFileLocation;
	}

	/**
	 * Gets the sitemap conf file location.
	 *
	 * @return the sitemap conf file location
	 */
	public Map<String, String> getSitemapConfFileLocation() {
		return mSitemapConfFileLocation;
	}

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}

}
