package com.tru.endeca.sitemap;

import java.util.Iterator;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.repository.RepositoryException;

import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoNavStateFormatter;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * The Class TRUSeoNavStateFormatter used to generate Navigation link for SiteMap.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUSeoNavStateFormatter extends  SeoNavStateFormatter {


	/** The m tru get site domain url mTRUSiteMapDomainUrl. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;
	
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;
	
	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations - Endeca Configurations
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}
	
	/**
	 * Gets the CatalogTools.
	 * 
	 * @return the CatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the catalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}

	/**
	 * This method get the URL form DimensionValueCacheTools by passing the category id.
	 *
	 * @param pUrlState - UrlState
	 * @param pNavState - DimLocationList
	 * @return String
	 * @throws UrlFormatException - Url Format Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String formatNavState(UrlState pUrlState, DimLocationList pNavState)	throws UrlFormatException {
		
		if (isLoggingDebug()){
			vlogDebug("====BEGIN::  TRUSeoNavStateFormatter.formatNavState() method");
		}	

		if (null == pNavState || null == pUrlState) {
			return null;
		}
		pUrlState.removeParam(TRUEndecaConstants.N_PARAMETER);

		String categoryId = null;
		StringBuffer url = new StringBuffer();
	//	List<Ancestor> ancestors = null;

		for (Iterator iterator = pNavState.iterator(); iterator.hasNext(); ) {
			DimLocation dimLocation = (DimLocation) iterator.next();
			if(dimLocation == null){
				return null;
			}
		//	int ancestorsSize = 0;
			boolean isLeafCategory = false;

			DimVal dimval = dimLocation.getDimValue();
		//	ancestors = dimLocation.getAncestors();
			if(dimval == null || dimval.getProperties() == null) {
				return null;
			}
			String dimensionName = dimval.getDimensionName();
			Map<String,String> properties = dimval.getProperties();
			categoryId = (String) dimval.getProperties().get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
			
		
			String userTypeId = null;
			if(!properties.isEmpty()) {
				 userTypeId = properties.get(TRUEndecaConstants.DIMVAL_USERTYPE_ID);
				if(!StringUtils.isEmpty(userTypeId) && userTypeId.equals(TRUEndecaConstants.TAXONOMYNODE)) {
					isLeafCategory = true;
				} else if(TRUEndecaConstants.TAXONOMYFILTEREDLANDINGPAGE.equals(userTypeId)) {
					int childItems = 0;
					try {
						childItems = getCatalogTools().findFixedChildClassificationItems(categoryId);
					} catch (RepositoryException e) {
						if(isLoggingError()) {
							vlogError(e, "RepositoryException occured in TRUSeoNavStateFormatter.formatNavState() method");	
						}
					}
					if(childItems < TRUEndecaConstants.INT_TWO){
						isLeafCategory = true;
					}
				}
			}
			/*if (ancestors != null && !ancestors.isEmpty()) {
				ancestorsSize = ancestors.size();
			}*/
			
			String domainUrl = getTRUSiteMapDomainUrl().getDomainUrl();
		//	String pageType = identifyPageType(userTypeId ,ancestorsSize,isLeafCategory);
			String pageType = identifyPageType(userTypeId,isLeafCategory);
			String queryString = null;
			if(dimensionName.equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
				StringBuffer queryParameter = new StringBuffer();
				queryParameter.append(TRUEndecaConstants.QUESTION_CATEGORYID_EQUALS).append(categoryId);
				if(queryParameter != null && queryParameter.length() > 0) {
					queryString = queryParameter.toString();
				} 
			}
			
			if(StringUtils.isNotEmpty(domainUrl) && StringUtils.isNotEmpty(pageType) && StringUtils.isNotEmpty(queryString)) {
				url.append(domainUrl).append(pageType).append(queryString);
			}
			
			if (url != null && url.length() > 0) {
				if (isLoggingDebug()){
					vlogDebug("END::  TRUSeoNavStateFormatter.formatNavState() method. Value of Url :: {0}" + url.toString());
				}
				return url.toString();
			}
			
		}
		if (isLoggingDebug()){
			vlogDebug("END::  TRUSeoNavStateFormatter.formatNavState() method");
		}
		return null;
	}
	
	/**
	 * Identify page type.
	 * 
	 * @param pUserTypeId
	 * 			the UserType Id
	 * @param pIsLeafCategory
	 *            isLeafCategory
	 * @return the string
	 */
	private String identifyPageType(String pUserTypeId,boolean pIsLeafCategory) {
		
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("START:: TRUSeoNavStateFormatter.identifyPageType method..");
		}

		String pageType = null;

		if (StringUtils.isNotBlank(pUserTypeId) && pUserTypeId.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYCATEGORY)) {
			// Root Category Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames()
					.get(TRUEndecaConstants.RCLP_PAGE_PREFIX);
		} else if(StringUtils.isNotBlank(pUserTypeId) && pUserTypeId.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYSUBCATEGORY)){
			// sub Category Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames()
					.get(TRUEndecaConstants.SCLP_PAGE_PREFIX);
		} 
		if(pIsLeafCategory) {
			// Product Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames().get(TRUEndecaConstants.PLP_PAGE_PREFIX);
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("Page Type :{0}", pageType);
			AssemblerTools.getApplicationLogging().vlogDebug("END:: TRUSeoNavStateFormatter.identifyPageType method..");
		}
		return pageType;
	}

	/**
	 * Check whether the Assembler logging error is enabled.
	 *
	 * @return the assemblerApplicationConfig
	 */
	public boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();
	}
	/**
	 * Checks if is logging error.
	 *
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}

	/**
	 * Vlog debug.
	 *
	 * @param pMessage the message
	 */
	public void vlogDebug(String pMessage) {
		AssemblerTools.getApplicationLogging().vlogDebug(pMessage);
	}

	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	public void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}

	/**
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}
}


