package com.tru.endeca.sitemap;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;

import com.endeca.navigation.ERec;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoAggrERecFormatter;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;


/**
 * 
 * This Class extends OOTB SeoAggrERecFormatter to generate and customize the url for Records .
 * 
 *  @version 1.0
 *  @author PA
 */
public class TRUSiteMapSeoAggrERecFormatter extends SeoAggrERecFormatter {
	
	/** The m tru get site domain url mTRUSiteMapDomainUrl. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;
	
	/**
	 * Field to hold aggrERecKey.
	 */
	private String mAggrERecKey;
	
	/**
	 * Field to hold defaultAggrERecKey.
	 */
	private String mDefaultAggrERecKey;
	
	/**
	 * Field to hold repositoryIdProperty.
	 */
	private String mRepositoryIdProperty;
	
	/**
	 * Property Holds endecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}
	
	/**
	 * Gets the aggr e rec key.
	 *
	 * @return the aggrERecKey
	 */
	public String getAggrERecKey() {
		return mAggrERecKey;
	}

	/**
	 * Sets the aggr e rec key.
	 *
	 * @param pAggrERecKey the aggrERecKey to set
	 */
	public void setAggrERecKey(String pAggrERecKey) {
		mAggrERecKey = pAggrERecKey;
	}
	
	/**
	 * Gets the default aggr e rec key.
	 *
	 * @return the defaultAggrERecKey
	 */
	public String getDefaultAggrERecKey() {
		return mDefaultAggrERecKey;
	}

	/**
	 * Sets the default aggr e rec key.
	 *
	 * @param pDefaultAggrERecKey the defaultAggrERecKey to set
	 */
	public void setDefaultAggrERecKey(String pDefaultAggrERecKey) {
		mDefaultAggrERecKey = pDefaultAggrERecKey;
	}

	/**
	 * Gets the repository id property.
	 *
	 * @return the repositoryIdProperty
	 */
	public String getRepositoryIdProperty() {
		return mRepositoryIdProperty;
	}

	/**
	 * Sets the repository id property.
	 *
	 * @param pRepositoryIdProperty the repositoryIdProperty to set
	 */
	public void setRepositoryIdProperty(String pRepositoryIdProperty) {
		mRepositoryIdProperty = pRepositoryIdProperty;
	}

	/**
	 * To generate and customize the url for Each Records.
	 * 
	 * @param pUrlState
	 *            -Urlstate
	 * @param pERec
	 *            - ERec
	 * @return formatERec
	 * @throws UrlFormatException
	 *             - If Any
	 */
	@Override
	public String formatERec(UrlState pUrlState, ERec pERec)
			throws UrlFormatException {
		if (AssemblerTools.getApplicationLogging().isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN :::: Inside formatERec() method of TRUSiteMapSeoAggrERecFormatter class");
		}
		String recordUrlParam = null; 
		String productType = (String) pERec.getProperties().get(TRUEndecaConstants.PROD_TYPE);

		String customRollupKey = (String) pERec.getProperties().get(TRUEndecaConstants.CUSTOM_ROOLUP_KEY);
		if(!StringUtils.isEmpty(customRollupKey)){
			recordUrlParam = customRollupKey;
		}
		
		if (!StringUtils.isEmpty(recordUrlParam)) {
			pUrlState.setParam(getAggrERecKey(), recordUrlParam);
			pUrlState.removeParam(getDefaultAggrERecKey());
			pUrlState.removeParam(TRUEndecaConstants.CATEGORY_ID);
		}
		String productUrl = null;
		if(!StringUtils.isEmpty(productType) && productType.equals(TRUEndecaConstants.COLLECTION_PRODUCT))
		{
			productUrl = getEndecaConfigurations().getCollectionPageUrl();
			pUrlState.removeParam(productType);
		}
		else{
			productUrl = getEndecaConfigurations().getPdpPageUrl();
		}
		if (AssemblerTools.getApplicationLogging().isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END :::: Inside formatERec() method of TRUSiteMapSeoAggrERecFormatter class");
		}
		String domainUrl = getTRUSiteMapDomainUrl().getDomainUrl();
		if (!StringUtils.isEmpty(productUrl) && !StringUtils.isEmpty(domainUrl)) {
			return domainUrl + productUrl;
		}
		return null;
	}
	
	/**
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}

}