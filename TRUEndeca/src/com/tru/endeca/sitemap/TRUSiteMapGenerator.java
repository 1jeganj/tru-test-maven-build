package com.tru.endeca.sitemap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.FileUtils;

import atg.core.util.StringUtils;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.endeca.soleng.sitemap.SitemapMain;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.email.conf.TRUSiteMapEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class will generate the site map generation based on the schedule time.
 * @author PA
 * @version 1.0 
 */
public class TRUSiteMapGenerator extends SingletonSchedulableService {
	
	/** The Constant XML_EXTENSION. */
	private static final String XML_EXTENSION = ".xml";
	
	/** The Constant STRING_NOT_FOUND. */
	private static final int STRING_NOT_FOUND = -1; 

	/** The Constant BYTE_1024. */
	private static final int BYTE_1024 = 1024;
	
	/** The Constant TEMP_FOLDER. */
	private static final String TEMP_FOLDER = "/temp";
	
	/** The m tru get site domain url mTRUSiteMapDomainUrl. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;
	
	/** The m tru static page url for site map. */
	private TRUStaticPageUrlForSiteMap mTRUStaticPageUrlForSiteMap;
	
	/**
	 * This property holds mSitemapConfigFilePaths.
	 */
	private Map<String, String> mSitemapConfigFilePaths;
	
	/**
	 * This property holds mSitemapFolderPaths.
	 */
	private Map<String, String> mSitemapFolderPaths;
	
	/**
	 * This property holds mSitemapGZFolderPaths.
	 */
	private Map<String, String> mSitemapGZFolderPaths;
	
	/**
	 * This property holds mGzFileNamePrefix.
	 */
	private Map<String, String> mGzFileNamePrefix;
	
	/**
	 * This property holds mEnableGzip.
	 */
	private boolean mEnableGzip;
	
	/**
	 * This property holds mEnableSitemapFileRename.
	 */
	private boolean mEnableSitemapFileRename;
	
	/**
	 * This property holds mEnableSiteMapScheduler.
	 */
	private boolean mEnableSiteMapScheduler;
	
	/**
	 * This property holds mSitemapIndexFileNamePrefix.
	 */
	private String mSitemapIndexFileNamePrefix;
	
	/** The m destination zip file location. */
	private String mDestinationZipFileLocation;
	
	
	/** The m sitemap conf file location. */
	private Map<String, String> mSitemapConfFileLocation;
	
	
	/** The m site map file extension. */
	private String mSiteMapFileExtension;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	/** The m store configuration file extension. */
	private TRUStoreConfiguration mTRUStoreConfiguration;
	
	/** The m enable ftp flag. */
	private boolean mEnableFtpFlag;
	
	/** The m sitemap email configuration. */
	private TRUSiteMapEmailConfiguration mSitemapEmailConfiguration;
	
	/**
	 * This method is used to get the EnableSiteMapScheduler.
	 *
	 * @return the EnableSiteMapScheduler
	 */
	public boolean isEnableSiteMapScheduler() {
		return mEnableSiteMapScheduler;
	}
	
	/**
	 * This method is used to set the EnableSiteMapScheduler.
	 *
	 * @param pEnableSiteMapScheduler the mEnableSiteMapScheduler to set
	 */
	public void setEnableSiteMapScheduler(boolean pEnableSiteMapScheduler) {
		this.mEnableSiteMapScheduler = pEnableSiteMapScheduler;
	}
	
	/**
	 * This method triggers the site map generation based on the schedule.
	 * @param pScheduler - Scheduler.
	 * @param pScheduledJob - ScheduledJob.
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSiteMapGenerator.doScheduledTask() method");
		}
		
		generateSiteMap();
		
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUSiteMapGenerator.doScheduledTask() method");
		}
	}
	
	
	/**
	 * Generate site map xml.
	 */
	public void generateSiteMap() {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSiteMapGenerator.generateSiteMapXml() method");
		}
		if (isEnableSiteMapScheduler()) {
			String recentZipFileName = getTRUStaticPageUrlForSiteMap().copyAndDecompressZipFile();
			boolean siteMapGenerationFlag = generateSitemapXml(recentZipFileName);
			if (StringUtils.isNotEmpty(recentZipFileName)) {
			getTRUStaticPageUrlForSiteMap().deleteUnZippedDir(recentZipFileName);
			}
			if (isEnableFtpFlag() && siteMapGenerationFlag) {
				sendFileToFtpLocation();
				getTRUEmailServiceUtils().sendSiteMapSuccessEmail(TRUEndecaConstants.SITE_MAP,setSiteMapSucessEmailTemplate());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("End:: TRUSiteMapGenerator.generateSiteMapXml() method");
		}
	}
	
	/**
	 * This method used to generate site map.
	 *
	 * @param pRecentZipFileName the recent zip file name
	 * @return true, if successful
	 */
	@SuppressWarnings("rawtypes")
	private boolean generateSitemapXml(String pRecentZipFileName) {
			
		String siteId = null;
		String siteConfigPath = null;
		boolean siteMapGenerationFlag = false;
			
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSiteMapGenerator.generateSitemap method.. {0}",
					isEnableSiteMapScheduler());
		}
		if (getSitemapConfigFilePaths().isEmpty()) {
		//	getTRUSiteMapEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.SITE_MAP, setSiteMapFailureEmailTemplate());
			return false;
		}
			for (Map.Entry entry : getSitemapConfigFilePaths().entrySet()) {
				siteId = (String) entry.getKey();
				siteConfigPath = (String) entry.getValue();
				if (isLoggingDebug()) {
					vlogDebug("siteId : {0}  siteConfigPath : {1}", siteId, siteConfigPath);
				}
				if (!StringUtils.isBlank(siteConfigPath)) {
					try {
						SitemapMain sem = new SitemapMain();
						
						getTRUSiteMapDomainUrl().setDomainUrl(siteId);
						
						getTRUStaticPageUrlForSiteMap().generateStaticPageUrl(siteId,pRecentZipFileName);
						
						sem.execute(siteConfigPath);
							if (isLoggingDebug()) {
								logDebug("Successfully created site map files");
							}
							boolean checkCopyFileFlag = copySiteMapFile(siteId);
							if(!checkCopyFileFlag){
								return false;
							}
						
							if (isLoggingDebug()) {
								logDebug("Successfully file copy to temparory location");
							}
						if (isEnableSitemapFileRename()) {
							renameSitemapGeneratedFiles(siteId);
								if (isLoggingDebug()) {
									logDebug("Successfully renamed file to temparory location");
								}
						}
						if (isEnableGzip()) {
							gzipSitemapFiles(siteId);
								if (isLoggingDebug()) {
									logDebug("Successfully converted file to gz format");
								}
							String sitemapGZFolderPath = getSitemapGZFolderPaths().get(siteId);
							if(!StringUtils.isEmpty(sitemapGZFolderPath)){
								siteMapGenerationFlag = true;
								FileUtils.deleteDirectory((new File(sitemapGZFolderPath + TEMP_FOLDER)));
							}
						}
					} catch (Exception exception) {
					getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.SITE_MAP,setSiteMapFailureEmailTemplate(getSitemapEmailConfiguration().getFailureExceptionMessage(),exception));
						if (isLoggingError()) {
							logError("Exception occured while generating sitemap...", exception);
						}
					}
					
				} else if (isLoggingDebug()) {
					logDebug("Empty siteConfigPath");
				}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.generateSitemap method..");
		}
		return siteMapGenerationFlag;
	}
	
	/**
	 * Copy generated site map file to temporary location for renaming and converting to GZ format .
	 *
	 * @param pSiteId the site id
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private boolean copySiteMapFile(String pSiteId) throws IOException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.copySiteMapFile() method..");
		}
		boolean flagCheckDir = false;
		String sourceFilePath = getSitemapFolderPaths().get(pSiteId);
		String destinationFilePath = getSitemapGZFolderPaths().get(pSiteId);
		if(StringUtils.isEmpty(sourceFilePath) || StringUtils.isEmpty(destinationFilePath)){
			return flagCheckDir;
		}
		try {	
				File destinationFile = new File(destinationFilePath);
				destinationFile.mkdir();
				StringBuffer tempDestinationFilePath = new StringBuffer(destinationFilePath);
				tempDestinationFilePath.append(TEMP_FOLDER) ;
				File destinationFileTemp = new File(tempDestinationFilePath.toString());
				destinationFile.mkdir();
				if(!destinationFile.exists()){
					return flagCheckDir;
				}
				FileUtils.copyDirectory(new File(sourceFilePath), destinationFileTemp);
		} catch (IOException ioException) {
			getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.SITE_MAP,setSiteMapFailureEmailTemplate(getSitemapEmailConfiguration().getFailureExceptionMessage(),ioException));
			if (isLoggingError()) {
				logError("Exception occured while generating sitemap...", ioException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.copySiteMapFile() method..");
		}
		return true;
	}
	
	/**
	 * This method used to rename generate files with site specific name.
	 *
	 * @param pSiteId - Site id
	 */
	protected void renameSitemapGeneratedFiles(String pSiteId) {
		List<String> sitemapXMLFileNames = null;
		String sitemapGZFolderPathString = getSitemapGZFolderPaths().get(pSiteId);
		if(StringUtils.isEmpty(sitemapGZFolderPathString)){
			return;
		}
			StringBuffer sitemapGZFolderPath = new StringBuffer(sitemapGZFolderPathString);
			sitemapGZFolderPath.append(TEMP_FOLDER);
			sitemapXMLFileNames = getAvailableXMLFiles(sitemapGZFolderPath.toString());
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSiteMapGenerator.renameSitemapGeneratedFiles method.. "
					+ "pSiteId : {0}  sitemapGZFolderPath : {1}", pSiteId, sitemapGZFolderPath);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("sitemapXMLFileNames : {0}", sitemapXMLFileNames);
		}
		if (sitemapXMLFileNames == null || sitemapXMLFileNames.isEmpty()) {
		//	getTRUSiteMapEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.SITE_MAP, setSiteMapFailureEmailTemplate());
			if (isLoggingDebug()) {
				logDebug("END:: TRUSiteMapGenerator.renameSitemapGeneratedFiles method.. Empty files");
			}
			return;
		}
		File oldFile = null;
		File newFile = null;
		sitemapGZFolderPath.append(File.separator);
		boolean isFileRenamed = false;
		String updateFilePath = null;
		int count = TRUEndecaConstants.INT_ONE;
		for (String sitemapXMLFileName : sitemapXMLFileNames) {
			if (isLoggingDebug()) {
				vlogDebug("sitemapXMLFileName : {0}", sitemapXMLFileName);
			}
			if (sitemapXMLFileName.indexOf(getSitemapIndexFileNamePrefix()) == STRING_NOT_FOUND) {
				updateFilePath = generateCustomFileName(sitemapGZFolderPath , pSiteId, count);
				oldFile = new File(sitemapGZFolderPath + sitemapXMLFileName);
				newFile = new File(updateFilePath);
				isFileRenamed = oldFile.renameTo(newFile);
				if (isLoggingDebug()) {
					vlogDebug("oldFileName : {0}  updateFilePath : {1}  isFileRenamed : {2}",
							sitemapGZFolderPath + sitemapXMLFileName, updateFilePath, isFileRenamed);
				}
				count++;
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.renameSitemapGeneratedFiles method.. ");
		}
	}
	
	/**
	 * 
	 * This method used to generate site specific file names.
	 * 
	 * @param pFolderPath - Folder path
	 * @param pSiteId - Site id
	 * @param pCount - Count
	 * @return Custom site specific name
	 */
	private String generateCustomFileName(StringBuffer pFolderPath, String pSiteId, int pCount) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.generateCustomFileName() method.. ");
		}
		StringBuffer fileName = new StringBuffer();	
		String gzFileNamePrefix = getGzFileNamePrefix().get(pSiteId);
		fileName.append(pFolderPath);
		fileName.append(gzFileNamePrefix);
		fileName.append(pCount);
		fileName.append(XML_EXTENSION);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUSiteMapGenerator.generateCustomFileName() :renammedFileName : {0}", fileName.toString());
		}
		return fileName.toString();
	}

	/**
	 * This method used to rename generate files with site specific name.
	 *
	 * @param pSiteId - Site id
	 */
	protected void gzipSitemapFiles(String pSiteId) {
		List<String> sitemapXMLFileNames = null;
		String sitemapGZFolderPathString = getSitemapGZFolderPaths().get(pSiteId);
		if(StringUtils.isEmpty(sitemapGZFolderPathString)){
			return;
		}
		StringBuffer sitemapGZFolderPathTemp = new StringBuffer(sitemapGZFolderPathString);
		sitemapGZFolderPathTemp.append(TEMP_FOLDER);
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSiteMapGenerator.gzipSitemapFiles method.. "
					+ "pSiteId : {0}  sitemapFolderPath : {1}", pSiteId, sitemapGZFolderPathTemp);
		}
		
		sitemapXMLFileNames = getAvailableXMLFiles(sitemapGZFolderPathTemp.toString());

		if (isLoggingDebug()) {
			vlogDebug("sitemapXMLFileNames : {0}", sitemapXMLFileNames);
		}
		if (sitemapXMLFileNames == null || sitemapXMLFileNames.isEmpty()) {
			if (isLoggingDebug()) {
				logDebug("END:: TRUSiteMapGenerator.gzipSitemapFiles method.. Empty files");
			}
			return;
		}
		sitemapGZFolderPathTemp.append(File.separator);
		sitemapGZFolderPathString = sitemapGZFolderPathString.concat(File.separator);
		for (String sitemapXMLFileName : sitemapXMLFileNames) {
			if (sitemapXMLFileName.indexOf(getSitemapIndexFileNamePrefix()) == TRUEndecaConstants.INT_ZERO) {
				continue;
			}
			if (isLoggingDebug()) {
				vlogDebug("sitemapXMLFileName : {0}", sitemapXMLFileName);
			}
				try {
					gzipIt(sitemapGZFolderPathTemp + sitemapXMLFileName, sitemapGZFolderPathString + sitemapXMLFileName
							+ getSiteMapFileExtension());
					if (isLoggingDebug()) {
						logDebug("File gziped successfully");
					}
				} catch (IOException ioException) {
					getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.SITE_MAP,
					setSiteMapFailureEmailTemplate(getSitemapEmailConfiguration().getFailureExceptionMessage(),ioException));
					if (isLoggingError()) {
						logError("IOException in gzipSitemapFiles method.. ", ioException);
					}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.gzipSitemapFiles method.. ");
		}
	}
	
	
	/**
	 * 
	 * This method used to fetch all available XML files from specified directory.
	 * 
	 * @param pDirectoryPath - Directory path
	 * @return List<String> - List of available XMl files
	 */
	private List<String> getAvailableXMLFiles(String pDirectoryPath) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.getAvailableXMLFiles() method.. ");
		}
		List<String> results = new ArrayList<String>();
		File[] files = new File(pDirectoryPath).listFiles();
		if(files == null || files.length == TRUEndecaConstants.INT_ZERO){
			return null;
		}
		// If this pathname does not denote a directory, then listFiles() returns null.
		for (File file : files) {
			if (file.isFile() && file.getName().indexOf(XML_EXTENSION) != STRING_NOT_FOUND) {
				results.add(file.getName());
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.getAvailableXMLFiles() method.. ");
		}
		return results;
	}
	
	/**
	 * GZip it.
	 * 
	 * @param pSourceFile source file location
	 * @param pDestinationFile destination file location
	 * @throws IOException signals that an I/O exception has occurred.
	 */
	private  void gzipIt(String pSourceFile, String pDestinationFile) throws IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.gzipIt() method.. ");
		}
		byte[] buffer = new byte[BYTE_1024];

		GZIPOutputStream gzos =
				new GZIPOutputStream(new FileOutputStream(pDestinationFile));

		FileInputStream in =
				new FileInputStream(pSourceFile);

		int len;
		while ((len = in.read(buffer)) > TRUEndecaConstants.INT_ZERO) {
			gzos.write(buffer, TRUEndecaConstants.INT_ZERO, len);
		}

		in.close();

		gzos.finish();
		gzos.close();
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.gzipIt() method.. ");
		}
	}
	
	/**
	 * Sets the site map failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setSiteMapFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.setSiteMapFailureEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getSitemapEmailConfiguration().getFailureSiteMapEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.setSiteMapFailureEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	
		/**
	 * Sets the site map success email template.
	 *
	 * @return the map
	 */
	private Map<String, Object> setSiteMapSucessEmailTemplate() {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.setSiteMapSucessEmailTemplate() method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		StringBuilder sb=new StringBuilder(getSitemapEmailConfiguration().getSuccessSiteMapEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
		templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUCC, getSitemapEmailConfiguration().getSuccSiteMapMessage());
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.setSiteMapSucessEmailTemplate() method.. ");
		}
		return templateParameters;
	}
	
	
	/**
	 * Gets the stack trace.
	 *
	 * @param pThrowable the throwable
	 * @return the stack trace
	 */
	private String getStackTrace(Throwable pThrowable) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.setSiteMapFailureEmailTemplate() method.. ");
		}
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    pThrowable.printStackTrace(printWriter); 
	    if(result.toString().length() > TRUEndecaConstants.INT_THOUSAND){
	    	if (isLoggingDebug()) {
				logDebug("END:: TRUSiteMapGenerator.getStackTrace() method.. ");
			}
	    	return result.toString().substring(TRUEndecaConstants.INT_ZERO, TRUEndecaConstants.INT_THOUSAND);
	    }
	    if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.getStackTrace() method.. ");
		}
	    return result.toString();
	  }
	
	/**
	 * Send file to ftp location.
	 */
	private void sendFileToFtpLocation(){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.sendFileToFtpLocation() method.. ");
		}
		String gzFileLocation = null;
		
		Map<String, String> gzFileLocationMap = getSitemapGZFolderPaths();
		
		if(gzFileLocationMap == null || gzFileLocationMap.isEmpty()){
			return;
		}
		
		for (@SuppressWarnings("rawtypes") Map.Entry entry : gzFileLocationMap.entrySet()) {
			gzFileLocation = (String) entry.getValue();
		}
		
		if(StringUtils.isEmpty(gzFileLocation)){
			if (isLoggingDebug()) {
				logDebug("TRUSiteMapGenerator.sendFileToFtpLocation() method.. No file to send ");
			}
			return;
		}
		
		File[] files = new File(gzFileLocation).listFiles();
		
		if(files == null || files.length == TRUEndecaConstants.INT_ZERO){
			return;
		}
		
		for (File gzFile : files) {
			String filePath = gzFile.getPath();
			
			if(StringUtils.isEmpty(filePath)){
				continue;
			}
			
			executeUniFtpCommand(filePath);
		}
		if (isLoggingDebug()) {
			logDebug("End:: TRUSiteMapGenerator.sendFileToFtpLocation() method.. ");
		}
	}
	
	/**
	 * Execute uni ftp command.
	 *
	 * @param pFilePath the file path
	 */
	private void executeUniFtpCommand(String pFilePath){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteMapGenerator.executeUniFtpCommand() method.. ");
		}
		StringBuffer output = new StringBuffer();
		String line = TRUEndecaConstants.EMPTY_STRING;
		String command = TRUEndecaConstants.UNIFTP+ pFilePath + TRUEndecaConstants.HUNIFTP;
		if (isLoggingDebug()) {
			vlogDebug("Unix Command  : {0}", command);
		}
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader =  new BufferedReader(new InputStreamReader(p.getInputStream()));      
			
			while ((line = reader.readLine())!= null) {
				output.append(line + TRUEndecaConstants.NEW_LINE);
			}

		} catch (IOException ioException) {
			if (isLoggingError()) {
				logError("IOException in executeUniFtpCommand method.. ", ioException);
			}
		} catch (InterruptedException inException) {
			if (isLoggingError()) {
				logError("InterruptedException in executeUniFtpCommand method.. ", inException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteMapGenerator.executeUniFtpCommand() method.. ");
		}
	}

	/**
	 * Gets the sitemapConfigFilePaths.
	 *
	 * @return the sitemapConfigFilePaths
	 */
	public Map<String, String> getSitemapConfigFilePaths() {
		return mSitemapConfigFilePaths;
	}

	/**
	 * Sets the sitemapConfigFilePaths.
	 *
	 * @param pSitemapConfigFilePaths the sitemapConfigFilePaths to set
	 */
	public void setSitemapConfigFilePaths(Map<String, String> pSitemapConfigFilePaths) {
		mSitemapConfigFilePaths = pSitemapConfigFilePaths;
	}
	
	/**
	 * Gets the enableGzip.
	 *
	 * @return the enableGzip
	 */
	public boolean isEnableGzip() {
		return mEnableGzip;
	}

	/**
	 * Sets the enableGzip.
	 *
	 * @param pEnableGzip the enableGzip to set
	 */
	public void setEnableGzip(boolean pEnableGzip) {
		mEnableGzip = pEnableGzip;
	}

	/**
	 * Gets the enableSitemapFileRename.
	 *
	 * @return the enableSitemapFileRename
	 */
	public boolean isEnableSitemapFileRename() {
		return mEnableSitemapFileRename;
	}

	/**
	 * Sets the enableSitemapFileRename.
	 *
	 * @param pEnableSitemapFileRename the enableSitemapFileRename to set
	 */
	public void setEnableSitemapFileRename(boolean pEnableSitemapFileRename) {
		mEnableSitemapFileRename = pEnableSitemapFileRename;
	}
	
	/**
	 * Gets the sitemapFolderPaths.
	 *
	 * @return the sitemapFolderPaths
	 */
	public Map<String, String> getSitemapFolderPaths() {
		return mSitemapFolderPaths;
	}

	/**
	 * Sets the sitemapFolderPaths.
	 *
	 * @param pSitemapFolderPaths the sitemapFolderPaths to set
	 */
	public void setSitemapFolderPaths(Map<String, String> pSitemapFolderPaths) {
		mSitemapFolderPaths = pSitemapFolderPaths;
	}
	
	/**
	 * Gets the sitemapIndexFileNamePrefix.
	 *
	 * @return the sitemapIndexFileNamePrefix
	 */
	public String getSitemapIndexFileNamePrefix() {
		return mSitemapIndexFileNamePrefix;
	}

	/**
	 * Sets the sitemapIndexFileNamePrefix.
	 *
	 * @param pSitemapIndexFileNamePrefix the sitemapIndexFileNamePrefix to set
	 */
	public void setSitemapIndexFileNamePrefix(String pSitemapIndexFileNamePrefix) {
		mSitemapIndexFileNamePrefix = pSitemapIndexFileNamePrefix;
	}

	/**
	 * Gets the sitemap gz folder paths.
	 *
	 * @return the mSitemapGZFolderPaths
	 */
	public Map<String, String> getSitemapGZFolderPaths() {
		return mSitemapGZFolderPaths;
	}

	/**
	 * Sets the sitemap gz folder paths.
	 *
	 * @param pMSitemapGZFolderPaths the mSitemapGZFolderPaths to set
	 */
	public void setSitemapGZFolderPaths(Map<String, String> pMSitemapGZFolderPaths) {
		mSitemapGZFolderPaths = pMSitemapGZFolderPaths;
	}

	/**
	 * Gets the gz file name prefix.
	 *
	 * @return the mGzFileNamePrefix
	 */
	public Map<String, String> getGzFileNamePrefix() {
		return mGzFileNamePrefix;
	}

	/**
	 * Sets the gz file name prefix.
	 *
	 * @param pGzFileNamePrefix the mGzFileNamePrefix to set
	 */
	public void setGzFileNamePrefix(Map<String, String> pGzFileNamePrefix) {
		mGzFileNamePrefix = pGzFileNamePrefix;
	}
	
	/**
	 * Gets the TRU site map domain url.
	 *
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * Sets the TRU site map domain url.
	 *
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}

	/**
	 * Gets the TRU static page url for site map.
	 *
	 * @return the TRU static page url for site map
	 */
	public TRUStaticPageUrlForSiteMap getTRUStaticPageUrlForSiteMap() {
		return mTRUStaticPageUrlForSiteMap;
	}

	/**
	 * Sets the TRU static page url for site map.
	 *
	 * @param pTRUStaticPageUrlForSiteMap the new TRU static page url for site map
	 */
	public void setTRUStaticPageUrlForSiteMap(
			TRUStaticPageUrlForSiteMap pTRUStaticPageUrlForSiteMap) {
		mTRUStaticPageUrlForSiteMap = pTRUStaticPageUrlForSiteMap;
	}
	
	/**
	 * Gets the destination zip file location.
	 *
	 * @return the destination zip file location
	 */
	public String getDestinationZipFileLocation() {
		return mDestinationZipFileLocation;
	}

	/**
	 * Sets the destination zip file location.
	 *
	 * @param pDestinationZipFileLocation the new destination zip file location
	 */
	public void setDestinationZipFileLocation(
			String pDestinationZipFileLocation) {
		mDestinationZipFileLocation = pDestinationZipFileLocation;
	}
	
	/**
	 * Gets the sitemap conf file location.
	 *
	 * @return the sitemap conf file location
	 */
	public Map<String, String> getSitemapConfFileLocation() {
		return mSitemapConfFileLocation;
	}

	/**
	 * Sets the sitemap conf file location.
	 *
	 * @param pSitemapConfFileLocation the new sitemap conf file location
	 */
	public void setSitemapConfFileLocation(Map<String, String> pSitemapConfFileLocation) {
		mSitemapConfFileLocation = pSitemapConfFileLocation;
	}

	/**
	 * Gets the site map file extension.
	 *
	 * @return the mSiteMapFileExtension
	 */
	public String getSiteMapFileExtension() {
		return mSiteMapFileExtension;
	}

	/**
	 * Sets the site map file extension.
	 *
	 * @param pSiteMapFileExtension the mSiteMapFileExtension to set
	 */
	public void setSiteMapFileExtension(String pSiteMapFileExtension) {
		this.mSiteMapFileExtension = pSiteMapFileExtension;
	}

	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}

	/**
	 * Gets the TRU store configuration.
	 *
	 * @return the tRUStoreConfiguration
	 */
	public TRUStoreConfiguration getTRUStoreConfiguration() {
		return mTRUStoreConfiguration;
	}

	/**
	 * Sets the TRU store configuration.
	 *
	 * @param pTRUStoreConfiguration the tRUStoreConfiguration to set
	 */
	public void setTRUStoreConfiguration(TRUStoreConfiguration pTRUStoreConfiguration) {
		mTRUStoreConfiguration = pTRUStoreConfiguration;
	}

	/**
	 * Checks if is enable ftp flag.
	 *
	 * @return the mEnableFtpFlag
	 */
	public boolean isEnableFtpFlag() {
		return mEnableFtpFlag;
	}

	/**
	 * Sets the enable ftp flag.
	 *
	 * @param pEnableFtpFlag the mEnableFtpFlag to set
	 */
	public void setEnableFtpFlag(boolean pEnableFtpFlag) {
		this.mEnableFtpFlag = pEnableFtpFlag;
	}

	/**
	 * Gets the sitemap email configuration.
	 *
	 * @return the mSitemapEmailConfiguration
	 */
	public TRUSiteMapEmailConfiguration getSitemapEmailConfiguration() {
		return mSitemapEmailConfiguration;
	}

	/**
	 * Sets the sitemap email configuration.
	 *
	 * @param pSitemapEmailConfiguration the mSitemapEmailConfiguration to set
	 */
	public void setSitemapEmailConfiguration(TRUSiteMapEmailConfiguration pSitemapEmailConfiguration) {
		this.mSitemapEmailConfiguration = pSitemapEmailConfiguration;
	}
	
	

}
