package com.tru.endeca.sitemap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.ClosedChannelException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.utils.TRUGetSiteTypeUtil;

// TODO: Auto-generated Javadoc
/**
 * This class return the domain URL based on site id.
 * @author PA
 * @version 1.0
 *
 */
public class TRUSiteMapDomainUrl extends GenericService{
	
	/** The  domain URL. */
	private String mDomainUrl;
	
	/**   Holds the mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/** The m tru get site type mTRUGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;
	
	/** property to hold mSiteRepository. */
	private Repository mSiteRepository;
	
	/** The m static page folder. */
	private String mStaticPageFolder;
	
	/**
	 * Gets the domain url.
	 *
	 * @return the mDomainUrl
	 */
	public String getDomainUrl() {
		return mDomainUrl;
	}

	/**
	 * Sets the domain url.
	 *
	 * @param pDomainUrl the mDomainUrl to set
	 */
	public void setDomainUrl(String pDomainUrl) {
		String siteId = pDomainUrl;
		String siteUrl = null;
		 if (isLoggingDebug()){
	 			vlogDebug("BEGIN::  TRUSiteMapDomainUrl.setDomainUrl() method");
	 		}
		if(!StringUtils.isEmpty(siteId)){	
			try {
				Site siteObject = getTRUGetSiteTypeUtil().getSite(siteId);
				if(siteObject != null){
					String[] siteUrls = (String[]) siteObject.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
					if (siteUrls != null && siteUrls.length > TRUEndecaConstants.INT_ZERO){
						siteUrl = siteUrls[TRUEndecaConstants.INT_ZERO];
					}
				}
			} catch (SiteContextException siteContextException) {
				  if (isLoggingError()) {
		    			 vlogError(siteContextException,"SiteContextException in TRUSiteMapDomainUrl.setDomainUrl() method ");
		 			}  
			}
		}
		mDomainUrl = siteUrl;
		if (isLoggingDebug()){
	 			vlogDebug("END::  TRUSiteMapDomainUrl.setDomainUrl() method");
	 	}
	}
	
	
	 /**
 	 * This method hit configured URL, if DimensionValueCache map is empty.
 	 * @param pUrl the url
 	 * @return the int
 	 */
	protected int hitUrlToInitializeDimensionValueCache(String pUrl){
		int responseCode =TRUEndecaConstants.INT_ZERO;
		if (isLoggingDebug()){
			vlogDebug("BEGIN::  TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method");
		}
		//////
		TrustManager[] trustAllCerts = new TrustManager[] {
			       new X509TrustManager() {
			          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			            return null;
			          }

			          public void checkClientTrusted(X509Certificate[] pCerts, String pAuthType) { 
			        	  if (isLoggingDebug()) {
								vlogDebug("TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache().checkClientTrusted() ");
							}
			          }

			          public void checkServerTrusted(X509Certificate[] pCerts, String pAuthType) { 
			        	  if (isLoggingDebug()) {
								vlogDebug("TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache().checkServerTrusted() ");
							} 
			          }

					@Override
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] pParamArrayOfX509Certificate,
							String pParamString) throws CertificateException {
						if (isLoggingDebug()) {
							vlogDebug("TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache().checkClientTrusted(): override ");
						}
						
					}

					@Override
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] pParamArrayOfX509Certificate,
							String pParamString) throws CertificateException {
						if (isLoggingDebug()) {
							vlogDebug("TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache().checkServerTrusted(): override ");
						}
						
					}

			       }
			    };

			    SSLContext sc = null;
				try {
					sc = SSLContext.getInstance(TRUEndecaConstants.SSL);
				} catch (NoSuchAlgorithmException e) {
					if (isLoggingError()) {
						vlogError("NoSuchAlgorithmException in TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method :: {0} ",e);
					}
				}
			    try {
					sc.init(null, trustAllCerts, new java.security.SecureRandom());
				} catch (KeyManagementException e) {
					if (isLoggingError()) {
						vlogError("KeyManagementException in TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method :: {0} ",e);
					}
				}
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			    // Create all-trusting host name verifier
			    HostnameVerifier allHostsValid = new HostnameVerifier() {
			        public boolean verify(String pHostname, SSLSession pSession) {
			          return true;
			        }
			    };
			    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		//////
		try {
			//URL myURL = new URL(pUrl);
			URL myURL = new URL(null, pUrl,new sun.net.www.protocol.https.Handler());
			if(myURL != null){
				HttpsURLConnection myURLConnection = (HttpsURLConnection)myURL.openConnection();
				if(myURLConnection != null){
					myURLConnection.addRequestProperty(TRUEndecaConstants.USER_AGENT,
							TRUEndecaConstants.MOZILLA_COMPATIBLE);
					myURLConnection.setRequestMethod(TRUEndecaConstants.GET_METHOD);
					myURLConnection.connect();
					responseCode =  myURLConnection.getResponseCode();
					if (isLoggingDebug()){
						vlogDebug("TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() :  responseCode :: {0}" + responseCode);
					}
				}
			}
			if (isLoggingDebug()){
				vlogDebug("END::  TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method");
			}	
		} 
		catch (MalformedURLException malFormedUrl) { 
			if (isLoggingError()) {
				vlogError(malFormedUrl,"MalformedURLException in TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method ");
			} 
		}catch (ClosedChannelException closedChannelException) {
			if (isLoggingError()) {
				vlogError(closedChannelException,"ClosedChannelException in TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method ");
			}  
		}
		catch (IOException ioException) {   
			if (isLoggingError()) {
				vlogError(ioException,"IOException in TRUSiteMapDomainUrl.hitUrlToInitilizeDimensionValueCache() method ");
			}  
		}
		return responseCode;
	}
	
	
	/**
	 * Gets the configured static page folder name.
	 *
	 * @param pSiteName the site name
	 * @return the configured static page folder name
	 */
	@SuppressWarnings("unchecked")
	protected List<String> getConfiguredStaticPageFolderName(String pSiteName){
		if (isLoggingDebug()){
			vlogDebug("BEGIN::  TRUSiteMapDomainUrl.getConfiguredStaticPageFolderName() method");
		}	
		Repository siteRepository = getSiteRepository();
		List<String> staticFolderNameList = null;
		
		if(siteRepository == null){
			return null;
		}
		
		try {
			@SuppressWarnings("deprecation")
			RepositoryItem siteRepositoryItem = siteRepository.getItem(pSiteName);
			if(siteRepositoryItem == null){
				return null;
			}
			staticFolderNameList = (List<String>)siteRepositoryItem.getPropertyValue(getStaticPageFolder());
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError(repositoryException,"RepositoryException in TRUSiteMapDomainUrl.getConfiguredStaticPageFolderName() method ");
			} 
		}
		if (isLoggingDebug()){
			vlogDebug("END::  TRUSiteMapDomainUrl.getConfiguredStaticPageFolderName() method. StaticFolderNameList:{0}"+staticFolderNameList);
		}
		return staticFolderNameList;
	}

	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}

	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}
	
	
	/**
	 * Gets the catalog properties.
	 *
	 * @return mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the CatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * Gets the site repository.
	 * 
	 * @return the mSiteRepository
	 */

	public Repository getSiteRepository() {
		return mSiteRepository;
	}

	/**
	 * Sets the site repository.
	 * 
	 * @param pSiteRepository
	 *            the mSiteRepository to set
	 */

	public void setSiteRepository(Repository pSiteRepository) {
		this.mSiteRepository = pSiteRepository;
	}

	/**
	 * Gets the static page folder.
	 *
	 * @return the static page folder
	 */
	public String getStaticPageFolder() {
		return mStaticPageFolder;
	}

	/**
	 * Sets the static page folder.
	 *
	 * @param pStaticPageFolder the new static page folder
	 */
	public void setStaticPageFolder(String pStaticPageFolder) {
		mStaticPageFolder = pStaticPageFolder;
	}

}
