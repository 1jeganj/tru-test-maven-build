package com.tru.endeca.search.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.commerce.endeca.assembler.cartridge.handler.DimensionValueCacheRefreshHandler;
import atg.commerce.endeca.cache.DimensionValueCache;
import atg.core.net.URLUtils;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.repository.RepositoryException;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.url.UrlNavigationStateBuilder;
import com.endeca.soleng.urlformatter.basic.BasicUrlFormatter;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * TRUDimensionValueCacheRefreshHandler manages the Endeca Dimension Value Cache refresh strategy while indexing.
 * @version 1.0
 * @author Professional Access
 */
public class TRUDimensionValueCacheRefreshHandler extends DimensionValueCacheRefreshHandler {

	/**
	 * Property to Hold dimensionNames.
	 */
	private String[] mDimensionNames;

	/**
	 * Property to enable SEO.
	 */
	private boolean mSeoEnabled;
	/**
	 * Property to enable Endeca Experience Manager pages to categories.
	 */
	private boolean mEnableXmPage;

	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * This property hold reference for TRUCatalogManager.
	 */
	private CatalogProperties mCatalogProperties;
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * Gets the CatalogTools.
	 * 
	 * @return the CatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the catalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}

	/**
	 * This property hold reference for EnableCheckForLeafCategoryURL.
	 */
	private boolean mEnableCheckForLeafCategoryURL;

	/**
	 * Gets the dimensionNames.
	 * 
	 * @return the dimensionNames
	 */
	public String[] getDimensionNames() {
		return mDimensionNames;
	}

	/**
	 * Sets the dimensionNames.
	 * 
	 * @param pDimensionNames - the dimensionNames to set
	 */
	public void setDimensionNames(String[] pDimensionNames) {
		mDimensionNames = pDimensionNames;
	}

	/**
	 * Returns seoEnabled.
	 * 
	 * @return the seoEnabled
	 */
	public boolean isSeoEnabled() {
		return mSeoEnabled;
	}

	/**
	 * sets Seo Enabled flag.
	 * 
	 * @param pSeoEnabled the seoEnabled to set
	 */
	public void setSeoEnabled(boolean pSeoEnabled) {
		mSeoEnabled = pSeoEnabled;
	}

	/**
	 * Returns enable Endeca Experience manager custom Pages.
	 * 
	 * @return the enableXmPage
	 */
	public boolean isEnableXmPage() {
		return mEnableXmPage;
	}

	/**
	 * sets Endeca Experience manager custom Pages.
	 * 
	 * @param pEnableXmPage the enableXmPage to set
	 */
	public void setEnableXmPage(boolean pEnableXmPage) {
		mEnableXmPage = pEnableXmPage;
	}

	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations - Endeca Configurations
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Gets the catalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public CatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the catalogProperties to set.
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the enableCheckForLeafCategoryURL.
	 * 
	 * @return the enableCheckForLeafCategoryURL
	 */
	public boolean isEnableCheckForLeafCategoryURL() {
		return mEnableCheckForLeafCategoryURL;
	}

	/**
	 * @param pEnableCheckForLeafCategoryURL the enableCheckForLeafCategoryURL to set.
	 */
	public void setEnableCheckForLeafCategoryURL(boolean pEnableCheckForLeafCategoryURL) {
		mEnableCheckForLeafCategoryURL = pEnableCheckForLeafCategoryURL;
	}

	/**
	 * This Method is used for Process the request for all Dimensions.
	 * 
	 * @param pConfig - Dimension Search Results Config
	 * @return - DimensionSearchResults
	 * @throws CartridgeHandlerException - Endeca Exception
	 */
	@Override
	public DimensionSearchResults process(DimensionSearchResultsConfig pConfig) throws CartridgeHandlerException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"START:: TRUDimensionValueCacheRefreshHandler.process method..");

		}
		String dimvalId = null;
		DimensionSearchResults allDimensions = null;
		List<DimensionSearchGroup> dimensions = null;
		DimensionValueCache newCache = null;
		Iterator<DimensionSearchGroup> ii = null;
		DimensionSearchGroup dimension = null;
		String dimensionName = null;
		List<DimensionSearchValue> items = null;
		Iterator<DimensionSearchValue> itr = null;
		DimensionSearchValue item = null;
		String repositoryId = null;
		List<Ancestor> ancestors = null;
		StringBuilder url = null;
		int ancestorsSize = 0;
		String pageType = null;
		allDimensions = super.process(pConfig);
		dimensions = getDimensions(allDimensions);
		if (dimensions == null) {
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug("No dimension whose name matches {0} could be found.",
						new Object[] { getDimensionName() });
			}

			return allDimensions;
		}
		newCache = getDimensionValueCacheTools().createEmptyCache();// Create Empty Cache
		ii = dimensions.iterator();
		// For each configured Dimensions
		while (ii.hasNext()) {
			dimension = ii.next();
			dimensionName = dimension.getDimensionName();
			items = dimension.getDimensionSearchValues();
			itr = items.iterator();
			if (itr == null) {
				return null;
			}
			while (itr.hasNext()) {
				List<String> ancestorIds = null;
				item = itr.next();
				// Product.Category
				if (dimensionName.equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
					repositoryId = item.getProperties().get(getRepositoryIdProperty());
				} else {
					// Other Dimensions like Brand,Character etc
					repositoryId = dimensionName + TRUEndecaConstants.UNDERSCORE + item.getLabel();
				}

				if (repositoryId == null || repositoryId.isEmpty()) {
					if (isLoggingDebug()) {
						AssemblerTools.getApplicationLogging().vlogDebug(
								"An entry in the list of {0} dimension values does not have a {1} property. " + "This cannot be used as the key in the cache.",
								new Object[] { getDimensionName(), getRepositoryIdProperty() });
					}

				} else {

					dimvalId = null;
					if (isSeoEnabled()) { // check whether SEO is Enabled or not
						dimvalId = parseDimvalId(item.getNavigationState());
					} else {
						dimvalId = this.parseDimvalIdWithOutSEO(item.getNavigationState());
					}
					if (dimvalId == null || dimvalId.isEmpty()) {
						if (isLoggingDebug()) {
							AssemblerTools.getApplicationLogging().vlogError(
									"Cannot find the dimension value id for item {0}, this item will not be cached",
									new Object[] { repositoryId });
						}

					} else {
						ancestors = item.getAncestors();
						if (ancestors != null && !ancestors.isEmpty()) {// ancestors are available
							ancestorIds = new ArrayList<String>(ancestors.size());
							ancestorsSize = ancestors.size();
							for (int i = 0; i < ancestorsSize; i++) {
								ancestorIds.add(ancestors.get(i).getProperties().get(getRepositoryIdProperty()));
							}

						}
						url = new StringBuilder();
						String userTypeId = null;
						if (isEnableXmPage() && dimensionName.equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
							ancestorsSize = 0;
							boolean isLeafCategory = false;
							Map<String,String> properties = item.getProperties();
							if(!properties.isEmpty()) {
								userTypeId = properties.get(TRUEndecaConstants.DIMVAL_USERTYPE_ID);
								if(!StringUtils.isEmpty(userTypeId) && userTypeId.equals(TRUEndecaConstants.TAXONOMYNODE)) {

									isLeafCategory = true;
								}else if(TRUEndecaConstants.TAXONOMYFILTEREDLANDINGPAGE.equals(userTypeId)){
									int childItems=0;
									try {
										if(!repositoryId.isEmpty()){
											childItems=getCatalogTools().findFixedChildClassificationItems(repositoryId);	
										}

									} catch (RepositoryException e) {
										if(isLoggingError()) {
											vlogError(e, "RepositoryException");	
										}
									}
									if(childItems <TRUEndecaConstants.INT_TWO){
										isLeafCategory = true;
									}
								}
							}
							if (ancestors != null && !ancestors.isEmpty()) {
								// ancestors are available
								ancestorsSize = ancestorIds.size();
							}
							/*url.append(identifyPageType(ancestorsSize, repositoryId, isLeafCategory));*/
							//String pageTypeUrlPrefix = identifyPageType(userTypeId,ancestorsSize,isLeafCategory);
							String pageTypeUrlPrefix = identifyPageType(userTypeId,isLeafCategory);
							if(!StringUtils.isEmpty(pageTypeUrlPrefix)){
								url.append(pageTypeUrlPrefix);
							}
						} else if (isEnableXmPage() && dimensionName.equals(TRUEndecaConstants.PROMOTION)){
							pageType=TRUEndecaConstants.PROMOTION;
							url.append(pageType);
						} else if (isEnableXmPage() && dimensionName.equals(TRUEndecaConstants.GWP_PROMOTION)){
							pageType=TRUEndecaConstants.GWP_PROMOTION;
							url.append(pageType);
						}

						else {
							if (isEnableXmPage()) {

								pageType = getEndecaConfigurations().getShopByPageUrl();
								url.append(pageType);
							} else {
								url.append(item.getContentPath());
							}

						}

						if(dimensionName.equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
							StringBuffer queryParameter = new StringBuffer();
							queryParameter.append(TRUEndecaConstants.QUESTION_CATEGORYID_EQUALS).append(repositoryId);
							if(queryParameter != null && queryParameter.length() > 0) {
								url.append(queryParameter);
							} else {
								url.append(item.getNavigationState());
							}
						} else {				
							url.append(item.getNavigationState());
						}
						newCache.put(repositoryId, dimvalId, url.toString(), ancestorIds);
					}
				}
			}
		}
		if (newCache.getNumCacheEntries() > 0) {
			getDimensionValueCacheTools().swapCache(newCache);
		} else {
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogWarning(
						"A cache refresh occurred which returned 0 entries, cache will not be updated", new Object[0]);
			}

		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"END:: TRUDimensionValueCacheRefreshHandler.process method..");
		}
		return allDimensions;
	}

	/**
	 * Identify page type.
	 * 
	 * @param pUserTypeId
	 * 			the UserTypeId
	 * @param pIsLeafCategory
	 *            isLeafCategory
	 * @return the string
	 */
	private String identifyPageType(String pUserTypeId, boolean pIsLeafCategory) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"START:: TRUDimensionValueCacheRefreshHandler.identifyPageType method..");

		}
		String pageType = null;

		if(StringUtils.isEmpty(pUserTypeId)){
			return null;
		}
		if (pUserTypeId.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYCATEGORY)) {
			// Root Category Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames()
					.get(TRUEndecaConstants.RCLP_PAGE_PREFIX);
		} else if(pUserTypeId.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYSUBCATEGORY)){
			// sub Category Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames()
					.get(TRUEndecaConstants.SCLP_PAGE_PREFIX);
		} 
		if(pIsLeafCategory) {
			// Product Listing Page Landing URL
			pageType = getEndecaConfigurations().getCategoryPageNames().get(TRUEndecaConstants.PLP_PAGE_PREFIX);
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("Page Type :{0}", pageType);
			AssemblerTools.getApplicationLogging().vlogDebug(
					"END:: TRUDimensionValueCacheRefreshHandler.identifyPageType method..");

		}
		return pageType;
	}

	/**
	 * This Method is used for get the All Dimensions.
	 * 
	 * @param pAllDimensions - allDimensions
	 * @return - dimensionSearchGroups
	 */
	private List<DimensionSearchGroup> getDimensions(DimensionSearchResults pAllDimensions) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"START:: TRUDimensionValueCacheRefreshHandler.getDimensions method..");
			AssemblerTools.getApplicationLogging().vlogDebug("DimensionSearchGroup :: Get the All Dimensions  ",
					pAllDimensions);
		}
		List<DimensionSearchGroup> dimensionSearchGroups = null;
		DimensionSearchGroup dimension = null;
		String[] dimensionNames = null;
		List<DimensionSearchGroup> dimensions = null;
		dimensionSearchGroups = new ArrayList<DimensionSearchGroup>();
		dimensionNames = getDimensionNames();// Read Dimension Names from Configurations
		dimensions = pAllDimensions.getDimensionSearchGroups();
		for (DimensionSearchGroup dimensionSearchGroup : dimensions) {
			dimension = dimensionSearchGroup;
			for (String dimensionName : dimensionNames) {
				// If dimension name matches with configured values
				if (dimension.getDimensionName().equals(dimensionName)) {
					dimensionSearchGroups.add(dimension);
				}
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"END:: TRUDimensionValueCacheRefreshHandler.getDimensions method..");

		}
		return dimensionSearchGroups;
	}

	/**
	 * This Method is used for display the Dimension Value Id's for SEO Url's.
	 * 
	 * @param pNavState - Navigation State
	 * @return String - Parsed Dimension encoded value
	 * 
	 */

	@Override
	protected String parseDimvalId(String pNavState) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"BEGIN:: TRUDimensionValueCacheRefreshHandler.parseDimvalId method..");

		}
		AssemblerTools.getApplicationLogging().vlogDebug("DimensionSearchGroup :: Parse the Dimension values  ",
				pNavState);
		String dimvalId = null;
		/*String splitString = null;*/
		String splitString = TRUEndecaConstants.UNDERSCORE_NPARAM;// Path separator

		if (pNavState != null && splitString != null && pNavState.contains(splitString)) {
			String[] navStatePieces = pNavState.split(splitString);
			if (navStatePieces.length == TRUEndecaConstants.INT_TWO) {
				dimvalId = navStatePieces[TRUEndecaConstants.INT_ONE];

			}

		}else if(pNavState != null){
			dimvalId = parseDimvalIdWithOutSEO(pNavState);
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"END:: TRUDimensionValueCacheRefreshHandler.parseDimvalId method..");

		}
		return dimvalId;
	}

	/**
	 * parses the Navigation state for Non SEO friendly Url's.
	 * 
	 * @param pNavState - Navigation State
	 * @return String - Parsed Dimension encoded value
	 * 
	 */
	protected String parseDimvalIdWithOutSEO(String pNavState) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"BEGIN:: TRUDimensionValueCacheRefreshHandler.parseDimvalIdWithOutSEO method..");

		}
		String dimvalId = null;
		UrlNavigationStateBuilder navigationStateBuilder = null;
		String queryString = null;
		String dimvalParamName = null;
		Map<?, ?> table = null;
		queryString = pNavState;

		if (getNavigationStateBuilder() instanceof UrlNavigationStateBuilder) {
			navigationStateBuilder = (UrlNavigationStateBuilder)getNavigationStateBuilder();
			dimvalParamName = navigationStateBuilder.getNavigationFiltersParam();
			if (navigationStateBuilder.getUrlFormatter() instanceof BasicUrlFormatter) {
				// If formatter is Basic Url formatter in the configurations
				if (queryString.startsWith(TRUEndecaConstants.QUESTION_MARK_SYMBOL)) {
					queryString = queryString.substring(TRUEndecaConstants.INT_ONE);
				}
				table = URLUtils.parseQueryString(queryString);
				if (table.containsKey(dimvalParamName)) {
					dimvalId = ((String[]) table.get(dimvalParamName))[TRUEndecaConstants.INT_ZERO];
				}
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"END:: TRUDimensionValueCacheRefreshHandler.parseDimvalIdWithOutSEO method..");

		}
		return dimvalId;
	}
	
	/**
	 * This method is used to append record filter to navigation state.
	 * 
	 * @param pNavigationState - The Navigation State
	 * @return navigationState - The Navigation State
	 * 
	 */
	@Override
	public NavigationState createDimensionSearchNavigationState(NavigationState pNavigationState) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN:: TRUDimensionValueCacheRefreshHandler.createDimensionSearchNavigationState method..");

		}
		NavigationState navigationState = super.createDimensionSearchNavigationState(pNavigationState);
		FilterState filterState = navigationState.getFilterState().clone();
		String recordFilter = TRUEndecaConstants.SUPRESS_FILTER; 
		
		String priceRecordFilter = TRUEndecaConstants.IS_PRICE_EXISTS_FILTER; 
		List<String> recordFilters = navigationState.getFilterState().getRecordFilters();
		
		recordFilters.add(recordFilter);
		recordFilters.add(priceRecordFilter);
		filterState.setRecordFilters(recordFilters);
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END:: TRUDimensionValueCacheRefreshHandler.createDimensionSearchNavigationState method..");

		}
		return navigationState.updateFilterState(filterState);
	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();

	}

	/**
	 * Check whether the Assembler logging error is enabled .
	 * 
	 * @return the assemblerApplicationConfig
	 */
	public boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();
	}

	/**
	 * Log the error messages using the AssemblerTools logging .
	 * 
	 * @param pThrowable
	 *            the exception object
	 * @param pMessage
	 *            the custom message
	 */
	public void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}


}
