package com.tru.endeca.category.primarypath;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.PrimaryPathCategoryVO;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.utils.TRUGetSiteTypeUtil;


/**
 * This class used to access Primary Path  related information. This will invoke the CategoryPrimaryPathManager to fetch repository related information.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCategoryPrimaryPathManager extends GenericService {

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 *  The m tru get site type util. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/**   
	 * Holds the mCatalogProperties. 
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * Property to hold TRUCatalogManager.
	 */
	private boolean mCheckPrimaryCategory;

	/**
	 * Property to hold mLevel.
	 */
	private int mMaxLevel;

	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;

	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}

	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}	

	/** Property Holds siteContextManager. */
	private SiteContextManager mSiteContextManager;
	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the dimension value cache.
	 *
	 * @return the dimension value cache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets Dimension Value Cache.
	 * @param pDimensionValueCache
	 *            the DimensionValueCache to set
	 */
	public void setDimensionValueCache(
			TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}

	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}


	/**
	 * This method is used to return the PrimaryPath category list.
	 * 
	 * @param pSkuId -- The sku Id
	 * @return primaryPathCategoryVOList -- the PrimaryPath category list
	 */
	public List<PrimaryPathCategoryVO> populatePrimaryPathVOList(String pSkuId)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.populatePrimaryPathVOList() method..");
		}
		List<PrimaryPathCategoryVO> primaryPathCategoryVOList = null;
		String categoryId = null;
		if(StringUtils.isNotBlank(pSkuId)) {
			categoryId = getPrimaryCategoryId(pSkuId);
			if(StringUtils.isNotBlank(categoryId)) {
				primaryPathCategoryVOList = new ArrayList<PrimaryPathCategoryVO>();
				List<String> catIdListFromTreeService = getTRUEndecaUtils().generateCategoryHirarchy(categoryId);
				if (isLoggingDebug()) {
					vlogDebug("BEGIN:: TRUCategoryPrimaryPathManager.populatePrimaryPathVOList() :: {0} = pSkuId  {1} = categoryId  {2} = catIdListFromTreeService ",pSkuId,categoryId,catIdListFromTreeService);
				}
				if(catIdListFromTreeService != null && !catIdListFromTreeService.isEmpty()) {
					primaryPathCategoryVOList = constructCategoryVOListFromCategoryTreeService(catIdListFromTreeService);
				}
				else {
					primaryPathCategoryVOList = populateCategoryVOList(categoryId, primaryPathCategoryVOList, TRUCommerceConstants.INT_ONE);
					if(primaryPathCategoryVOList != null && !primaryPathCategoryVOList.isEmpty()) {
						Collections.reverse(primaryPathCategoryVOList);
					}
				}
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.populatePrimaryPathVOList() method..");
		}
		return primaryPathCategoryVOList;
	}

	/**
	 * This method is used to return the primary categoryId from skuId.
	 * @param pSkuId -- the skuId
	 * @return categoryId -- the category Id
	 */
	@SuppressWarnings("unchecked")
	public String getPrimaryCategoryId(String pSkuId) {

		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.getPrimaryCategoryId() method..");
		}
		String categoryId = null;
		if(StringUtils.isBlank(pSkuId)) {
			return null;
		}

		try {
			RepositoryItem skuItem = getCatalogTools().findSKU(pSkuId);
			if(skuItem == null) {
				return null;
			}

			categoryId = (String)skuItem.getPropertyValue(getCatalogProperties().getPrimaryPathCategoryPropertyName());

			if(isCheckPrimaryCategory() && StringUtils.isEmpty(categoryId)) {
				Object propertyValue = skuItem.getPropertyValue(getCatalogProperties().getParentClassificationsItemPropertyName());
				if (propertyValue!= null){
					List<RepositoryItem> parentCategoryList = (List<RepositoryItem>)propertyValue;
					if(parentCategoryList != null && !parentCategoryList.isEmpty()) {
						categoryId = parentCategoryList.get(TRUCommerceConstants.INT_ZERO).getRepositoryId();
					}
				}
			}

		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				vlogError(repositoryException,"Repository Exception occured while fetching the primary categoryId from sku");		
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.getPrimaryCategoryId() method..");
		}
		return categoryId;
	}

	/**
	 * This method is used return the CategoryVo List to display Breadcrumb Primary path.
	 * @param pCategoryId -- the category Id
	 * @param pPrimaryPathCategoryVOList -- the list to contain category VO 
	 * @param pLevel -- the hirarchy level
	 * @return pPrimaryPathCategoryVOList -- the list of PrimaryPathCategoryVO
	 */
	public List<PrimaryPathCategoryVO> populateCategoryVOList(String pCategoryId, List<PrimaryPathCategoryVO> pPrimaryPathCategoryVOList, int pLevel) {

		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.populateCategoryVOList() method..");
		}
		PrimaryPathCategoryVO primaryPathCategoryVO = null;
		String displayStatus = null;
		 List<PrimaryPathCategoryVO> primaryCategoryPathVoList = null;
		 int level = pLevel;

		if(StringUtils.isNotBlank(pCategoryId))	{
			try {
				RepositoryItem categoryItem = getCatalogTools().findCategory(pCategoryId);
				if (categoryItem != null) {
					displayStatus = (String) categoryItem.getPropertyValue(getCatalogProperties().getDisplayStatusPropertyName());
				}
				if(StringUtils.isNotBlank(displayStatus) && !(displayStatus.equalsIgnoreCase(TRUCommerceConstants.HIDDEN) || displayStatus.equalsIgnoreCase(TRUCommerceConstants.NO_DISPLAY))) {
					primaryPathCategoryVO = populateCategoryVO(pCategoryId,displayStatus);
				}
			} catch (RepositoryException repositoryException) {
				if(isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUCategoryPrimaryPathManager.populateCategoryVOList() method getting category Display Status");	
				}
			}
			if(primaryPathCategoryVO != null && pPrimaryPathCategoryVOList != null) {
				primaryCategoryPathVoList = pPrimaryPathCategoryVOList;
				primaryCategoryPathVoList.add(primaryPathCategoryVO);
			}
			if(StringUtils.isNotBlank(displayStatus) && !(displayStatus.equalsIgnoreCase(TRUCommerceConstants.HIDDEN)))	{
				Set<RepositoryItem> fixedParentCategoriesSet = null;
				List<RepositoryItem> fixedParentCategories = null; 
				try {
					fixedParentCategoriesSet = (Set<RepositoryItem>)getCatalogTools().findFixedParentClassificationItems(pCategoryId);
					if(fixedParentCategoriesSet != null && !fixedParentCategoriesSet.isEmpty())	{
						fixedParentCategories = new ArrayList<RepositoryItem>(fixedParentCategoriesSet);
					}

					if (fixedParentCategories != null && !fixedParentCategories.isEmpty() && !(fixedParentCategories.get(TRUCommerceConstants.INT_ZERO).getRepositoryId().equals(TRUCommerceConstants.NAVIGATION_TRU) || fixedParentCategories.get(TRUCommerceConstants.INT_ZERO).getRepositoryId().equals(TRUCommerceConstants.NAVIGATION_BRU)) && getMaxLevel() > level)	{
						level++;
						return populateCategoryVOList(fixedParentCategories.get(TRUCommerceConstants.INT_ZERO).getRepositoryId(),pPrimaryPathCategoryVOList, level);
					}
				} catch (RepositoryException repositoryException) {
					if(isLoggingError()) {
						vlogError(repositoryException, "RepositoryException occured in TRUCategoryPrimaryPathManager.populateCategoryVOList() method getting fixedParentCategoriesSet");	
					}
				}
			}
			else {
				primaryCategoryPathVoList = null;
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.populateCategoryVOList() method..");
		}
		return primaryCategoryPathVoList;
	}

	/**
	 * This method is used to populate PrimaryPathCategoryVO.
	 * @param pCategoryId -- the category Id
	 * @param pDisplayStatus -- the category Display Status
	 * @return primaryPathCategoryVO -- the PrimaryPathCategoryVO
	 */
	public PrimaryPathCategoryVO populateCategoryVO(String pCategoryId,String pDisplayStatus)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.populateCategoryVO() method..");
		}
		PrimaryPathCategoryVO primaryPathCategoryVO = null;
		String categoryName = null;
		String categoryURL = null;
		if(StringUtils.isNotBlank(pCategoryId))	{

			try {
				primaryPathCategoryVO = new PrimaryPathCategoryVO();
				primaryPathCategoryVO.setCategoryId(pCategoryId);
				categoryName = getCatalogTools().getCategoryDisplayName(pCategoryId);
				categoryURL = populateCategoryURL(pCategoryId);
				if(StringUtils.isNotBlank(categoryName)) {
					primaryPathCategoryVO.setCategoryName(categoryName);
				}
				if(StringUtils.isNotBlank(categoryURL))	{
					primaryPathCategoryVO.setCategoryURL(categoryURL);
				}
				if(StringUtils.isNotBlank(pDisplayStatus)) {
					primaryPathCategoryVO.setCategoryDisplayStatus(pDisplayStatus);
				}

			} catch (RepositoryException repositoryException) {
				if(isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUCategoryPrimaryPathManager.populateCategoryVO() method getting categoryName");	
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.populateCategoryVO() method..");
		}
		return primaryPathCategoryVO;
	}

	/**
	 * This method is used to generate the url for a particular category.
	 * @param pCategoryId -- the category Id
	 * @return url - URL String
	 */
	public String populateCategoryURL(String pCategoryId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.populateCategoryURL() method..");
		}
		StringBuffer redirectUrl = new StringBuffer();
		if(StringUtils.isNotEmpty(pCategoryId)) {
			DimensionValueCacheObject cacheObj = getDimensionValueCache().getCacheObject(pCategoryId);
			String contextPath = getTRUEndecaUtils().getCategoryContextPath(pCategoryId);
			if(StringUtils.isNotEmpty(contextPath)) {
				redirectUrl.append(contextPath);
				if (cacheObj != null && cacheObj.getUrl() != null) {
					redirectUrl.append(cacheObj.getUrl().toString());	
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.populateCategoryURL() method..");
		}
		return redirectUrl.toString();

	}
	
	/**
	 * This method is used to Identify page type.
	 *
	 * @param pSiteId the site id
	 * @return pageType -- the string
	 *//*
	private String identifyPageType(String pUserTypeId, boolean pIsLeafCategory)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.identifyPageType() method..");
		}
		String pageType = null;
		if (StringUtils.isNotBlank(pUserTypeId) && pUserTypeId.equalsIgnoreCase(TRUCommerceConstants.TAXONOMYCATEGORY)) {
			// Root Category Listing Page Landing URL
			pageType = TRUCommerceConstants.CATEGORY_PAGE;
		} else if(StringUtils.isNotBlank(pUserTypeId) && pUserTypeId.equalsIgnoreCase(TRUCommerceConstants.TAXONOMYSUBCATEGORY)){
			// sub Category Listing Page Landing URL
			pageType = TRUCommerceConstants.SUB_CATEGORY_PAGE;
		} 
		if(pIsLeafCategory) {
			// Product Listing Page Landing URL
			pageType = TRUCommerceConstants.FAMILY_PAGE;
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.identifyPageType() method..");
		}
		return pageType;
	}*/
	/**
	 * This method will return the ContextPath from the Site production url.
	 * 
	 * @param pSiteId - SIte Id
	 * @return String siteProductUrl
	 */
	public String getSiteProductionUrl(String pSiteId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.getSiteProductionUrl() method..");
		}
		String contextPath = null;
		Site site = null;
		if(!StringUtils.isEmpty(pSiteId)) {
			try {
				site = getSiteContextManager().getSite(pSiteId);
				if(site != null) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, site);
					if(truSiteVO != null ) {
						contextPath = truSiteVO.getSiteURL();
					}
				}
			} catch (SiteContextException siteContextException) {
				vlogError(siteContextException, "Site Context Exception occured in TRUCategoryPrimaryPathManager.getSiteProductionUrl");
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.getSiteProductionUrl() method..");
		}
		return contextPath;
	}
	
	
	/**
	 * This method is used to retrive the child skus for a collection product.
	 * @param pCollectionProduct -- the Collection Product
	 * @return childSku -- list of child skus
	 */
	@SuppressWarnings("unchecked")
	public List<RepositoryItem> getChildSku(RepositoryItem pCollectionProduct ) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.getChildSku() method..");
		}
		
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties)getCatalogTools().getCatalogProperties();
		List<RepositoryItem> childSku = (List<RepositoryItem>) pCollectionProduct.getPropertyValue(catalogProperties.getChildSKUsPropertyName());

		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.getChildSku() method..");
		}
		return childSku;
	}
	
	
	

	/**
	 * Gets the catalog properties.
	 *
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Checks if is check primary category.
	 *
	 * @return the checkPrimaryCategory
	 */
	public boolean isCheckPrimaryCategory() {
		return mCheckPrimaryCategory;
	}

	/**
	 * Sets the check primary category.
	 *
	 * @param pCheckPrimaryCategory the checkPrimaryCategory to set
	 */
	public void setCheckPrimaryCategory(boolean pCheckPrimaryCategory) {
		mCheckPrimaryCategory = pCheckPrimaryCategory;
	}

	/**
	 * Gets the max level.
	 *
	 * @return the maxLevel
	 */
	public int getMaxLevel() {
		return mMaxLevel;
	}

	/**
	 * Sets the max level.
	 *
	 * @param pMaxLevel the maxLevel to set
	 */
	public void setMaxLevel(int pMaxLevel) {
		mMaxLevel = pMaxLevel;
	}
	
	/**
	 * This method is used to generate the category Hierarchy from CategoryTree.
	 * @param pCategoryList -- the Category List
	 * @return primaryCategoryPathVoList -- the PrimaryPathCategoryVO List
	 */
	public List<PrimaryPathCategoryVO> constructCategoryVOListFromCategoryTreeService(List<String> pCategoryList) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryPrimaryPathManager.constructCategoryVOListFromCategoryTreeService() method..");
		}
		 List<PrimaryPathCategoryVO> primaryCategoryPathVoList = new ArrayList();
		if(pCategoryList != null && !pCategoryList.isEmpty()) {
			for (String categoryId : pCategoryList) {
				if(StringUtils.isNotBlank(categoryId))	{
					String displayStatus = null;
					
					PrimaryPathCategoryVO primaryPathCategoryVO = null;
					try {
						RepositoryItem categoryItem = getCatalogTools().findCategory(categoryId);
						if (categoryItem != null) {
							displayStatus = (String) categoryItem.getPropertyValue(getCatalogProperties().getDisplayStatusPropertyName());
						}
						if(StringUtils.isNotBlank(displayStatus) && !(displayStatus.equalsIgnoreCase(TRUCommerceConstants.HIDDEN) || displayStatus.equalsIgnoreCase(TRUCommerceConstants.NO_DISPLAY))) {
							primaryPathCategoryVO = populateCategoryVO(categoryId,displayStatus);
						}
					} catch (RepositoryException repositoryException) {
						if(isLoggingError()) {
							vlogError(repositoryException, "RepositoryException occured in TRUCategoryPrimaryPathManager.constructCategoryVOListFromCategoryTreeService() method getting category Display Status");	
						}
					}
					if(primaryPathCategoryVO != null) {
						primaryCategoryPathVoList.add(primaryPathCategoryVO);
					}
			}
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCategoryPrimaryPathManager.constructCategoryVOListFromCategoryTreeService() method..");
		}
		return primaryCategoryPathVoList;
	}

}
