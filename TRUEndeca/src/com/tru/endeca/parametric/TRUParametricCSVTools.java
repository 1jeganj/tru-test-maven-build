package com.tru.endeca.parametric;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.sql.DataSource;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.TRUConstants;
import com.tru.email.conf.TRUParametericCSVEmailConfiguration;
import com.tru.email.utils.TRUEmailServiceUtils;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.sitemap.TRUSiteMapDomainUrl;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.endeca.vo.ParametricProfileVO;
import com.tru.proxy.ProxyConfiguration;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * This class used to get Parametric report CSV related data.
 * @author PA
 * @version 1.1
 *
 */

public class TRUParametricCSVTools extends GenericService {

	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;
	/**
	 * Property to hold mMaxLevel.
	 */
	private int mMaxLevel;
	
	/** The Constant CONTENT_LENGTH. */
	public static final String CONTENT_LENGTH = "Content-Length";

	/** The Constant CONTENT_LENGTH_VAL. */
	public static final String CONTENT_LENGTH_VAL = "0";

	/** The Constant X_APP_API_KEY. */
	public static final String X_APP_API_KEY = "X-APP-API_KEY";

	/** The Constant X_APP_API_CHANNEL. */
	public static final String X_APP_API_CHANNEL = "X-APP-API_CHANNEL";

	/** The Constant TRU_ACCESS. */
	public static final String TRU_ACCESS = "TRU-Access";

	/** The Constant API_KEY. */
	public static final String API_KEY = "apiKey";

	/** The Constant API_CHANNEL. */
	public static final String API_CHANNEL = "mobile";

	/** The Constant TRU_ACCESS_VAL. */
	public static final String TRU_ACCESS_VAL = "true";

	/**  Holds the mProxyConfiguration */
	private ProxyConfiguration mProxyConfiguration;

	/**  Holds the mConnectionTimeOut */
	private int mConnectionTimeOut;

	/**  Holds the mProxyRequired */
	private boolean mProxyRequired;

	/** The mParametricCsvRestServiceURL. */
	private String mParametricCsvRestServiceURL;

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/** The Constant GET_METHOD. */
	public static final String GET_METHOD = "GET";

	/** The m product catalog. */
	private MutableRepository mProductCatalog;

	/** This property hold reference for CatalogProperties.*/
	private CatalogProperties mCatalogProperties;

	/** The m dimension value cache tools. */
	private DimensionValueCacheTools mDimensionValueCacheTools;

	/** The m tru site map domain url. */
	private TRUSiteMapDomainUrl mTRUSiteMapDomainUrl;

	/** The m tru catalog properties. */
	private TRUCatalogProperties mTRUCatalogProperties;
	
	/** The m email service utils file extension. */
	private TRUEmailServiceUtils mTRUEmailServiceUtils;
	
	/** The m ParametricCSV email configuration. */
	private TRUParametericCSVEmailConfiguration mParametericCSVEmailConfiguration;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * property to hold dataSource
	 */
	private DataSource mDataSource;
	
	/**
	 * property to hold mQuery
	 */
	private String mQuery;
	
	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * @return the ParametericCSVEmailConfiguration
	 */
	public TRUParametericCSVEmailConfiguration getParametericCSVEmailConfiguration() {
		return mParametericCSVEmailConfiguration;
	}

	/**
	 * @param pParametericCSVEmailConfiguration the ParametericCSVEmailConfiguration to set
	 */
	public void setParametericCSVEmailConfiguration(
			TRUParametericCSVEmailConfiguration pParametericCSVEmailConfiguration) {
		mParametericCSVEmailConfiguration = pParametericCSVEmailConfiguration;
	}
	
	
	/**
	 * Gets the TRU  email service utils.
	 *
	 * @return the TRUEmailServiceUtils
	 */
	public TRUEmailServiceUtils getTRUEmailServiceUtils() {
		return mTRUEmailServiceUtils;
	}

	/**
	 * Sets the TRU  email service utils.
	 *
	 * @param pTRUEmailServiceUtils the TRUEmailServiceUtils to set
	 */
	public void setTRUEmailServiceUtils(
			TRUEmailServiceUtils pTRUEmailServiceUtils) {
		mTRUEmailServiceUtils = pTRUEmailServiceUtils;
	}
	
	

	/**
	 * Gets the parametric csv data and set in to ParametricProfileVO object.
	 *
	 * @return the parametric csv data
	 */
	@SuppressWarnings("unchecked")
	public List<ParametricProfileVO> getParametricCsvData() {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUParametricCSVTools  :: getParametricCsvData() method ");
		}
		// Get all the category present in catalog.
		RepositoryItem[] categoryItems = getAllCategory();

		if(categoryItems == null || categoryItems.length == TRUEndecaConstants.INT_ZERO){
			return null;
		}
		List<ParametricProfileVO> parametricProfileVoList = new ArrayList<ParametricProfileVO>();
		Map<String,List<String>> hiddenCategoryMap = populateAllHiddenCategory();
		if (isLoggingDebug()) {
			vlogDebug("END:TRUParametricCSVTools  getParametricCsvData :: Hidden Category Map:{0}", hiddenCategoryMap);
		}
		DimensionValueCache dimValCache = getDimensionValueCacheTools().getCache();
		
		for (RepositoryItem categoryItem : categoryItems) {
			ParametricProfileVO parametricVo = new ParametricProfileVO();
			String categoryId = categoryItem.getRepositoryId();
			String userType = (String)categoryItem.getPropertyValue(getTRUCatalogProperties().getUserTypeId());

			// Identify page name display. 
			String pageName = getPageName(userType,categoryId);

			Set<String> siteId = (Set<String>)categoryItem.getPropertyValue(getTRUCatalogProperties().getSiteIds());

			parametricVo.setCatalogId(categoryId);
			parametricVo.setPageNameDisplayTxt(pageName);
			if(!TRUEndecaConstants.TRU_CATEGORYLANDING.equalsIgnoreCase(pageName)) {
				parametricVo.setParametricProfileNavTxt(TRUEndecaConstants.NAVIGATION_CONTAINER);
			}
			// Filter indexed category.
			filterIndexedCategory(dimValCache, categoryId, parametricVo,siteId, parametricProfileVoList,hiddenCategoryMap);
			
		}
		if (isLoggingDebug()) {
			vlogDebug("END:TRUParametricCSVTools  :: getParametricCsvData() method ");
		}
		return parametricProfileVoList;
	}

	/**
	 * Gets the all category present in Catalog.
	 *
	 * @return the all category
	 */
	public RepositoryItem[] getAllCategory(){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUParametricCSVTools  :: getAllCategory() method ");
		}
		RepositoryItem[] categoryItems= null;
		Repository productCatalogRepository= getProductCatalog();

		try {
			RepositoryItemDescriptor categoryItemDescriptor = productCatalogRepository.getItemDescriptor(getTRUCatalogProperties().getClassificationItemDescPropertyName());
			if(categoryItemDescriptor == null){
				if (isLoggingDebug()) {
					vlogDebug("END:TRUParametricCSVTools  :: getAllCategory() method ,classificationItemDescriptor is null");
				}
				return null;
			}
			RepositoryView categoryRepositoryView = categoryItemDescriptor.getRepositoryView();
			if (categoryRepositoryView != null ) {
				QueryBuilder categoryQueryBuilder = categoryRepositoryView.getQueryBuilder();
				Query query = categoryQueryBuilder.createUnconstrainedQuery();
				categoryItems = categoryRepositoryView.executeQuery(query);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e, "Class TRUParametricCSVTools :An exception occured during getting category from Catalog.");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:TRUParametricCSVTools  :: getAllCategory() method ");
		}
		return categoryItems;
	}

	/**
	 * Filter indexed category.
	 *
	 * @param pDimensionValueCache the dimension value cache
	 * @param pCategoryId the category id
	 * @param pParametricVo the parametric vo
	 * @param pSiteId the site id
	 * @param pPrmetricProfileVoList the parametric profile vo list
	 * @param pHiddenCategoryMap -- the hidden category Map
	 */
	private void filterIndexedCategory(DimensionValueCache pDimensionValueCache, String pCategoryId, ParametricProfileVO pParametricVo,Set<String> pSiteId,List<ParametricProfileVO> pPrmetricProfileVoList,Map<String,List<String>> pHiddenCategoryMap){

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUParametricCSVTools  :: filterIndexedCategory() method ");
		}
		String domainUrl = null;
		ParametricProfileVO parametricProfileVO = null;
		String displayStatus = null;
		List<DimensionValueCacheObject> dimValCacheObjList = null;
		String breadcrumb = null;
		List<String> primaryCategoryIdList = null;
		String categoryPrimaryBreadcrumb = null;
		if(pDimensionValueCache != null) {
			dimValCacheObjList = pDimensionValueCache.get(pCategoryId);
		}

		if(dimValCacheObjList == null || dimValCacheObjList.isEmpty() || pSiteId.isEmpty()) {
			try {
				displayStatus = getCatalogTools().findCategoryDisplayStatus(pCategoryId);
			} catch (RepositoryException e) {

				if (isLoggingError()) {
					vlogError(e, "Class TRUParametricCSVTools :An exception occured during getting category displayStatus.");
				}
			}
			
			populateCategoryDisplayStatus(displayStatus,pParametricVo);
			if (isLoggingDebug()) {
				vlogDebug("END:TRUParametricCSVTools  :: No category found in DimensionValueCacheTools : CategoryID{0}", pCategoryId);
			}
			if(getTRUEndecaUtils().isChekPrimaryCategory()) {
				 primaryCategoryIdList = getTRUEndecaUtils().populatePrimaryBreadcrumbPathList(pCategoryId);
				if(primaryCategoryIdList != null && !primaryCategoryIdList.isEmpty()) {
					 categoryPrimaryBreadcrumb = getTRUEndecaUtils().generateCategoryPrimaryPathBreadcrumb(primaryCategoryIdList);
					 if(StringUtils.isNotEmpty(categoryPrimaryBreadcrumb))
					 {
						 String finalBreadcrumb = removeSpecialCharacter(categoryPrimaryBreadcrumb);
						 if (isLoggingDebug()) {
								vlogDebug("TRUParametricCSVTools  ::filterIndexedCategory() : final Primay Category Breadcrumb {0}", finalBreadcrumb);
							}
						 pParametricVo.setBreadCrumb(finalBreadcrumb);
					 }
				}
			}
			if(StringUtils.isEmpty(categoryPrimaryBreadcrumb)){
			List<String> catIdListFromTreeService = null;
			List<String> parentCatList =null;
			String categoryDisplayName = null;
			if(TRUEndecaConstants.HIDDEN.equalsIgnoreCase(displayStatus))
			{
				 parentCatList = pHiddenCategoryMap.get(pCategoryId);
				 if (isLoggingDebug()) {
						vlogDebug("END:TRUParametricCSVTools  ::filterIndexedCategory() : parentCatList {0}", parentCatList);
					}
				if(parentCatList != null && !parentCatList.isEmpty())
				{
					for (String catId : parentCatList) {
						catIdListFromTreeService = getTRUEndecaUtils().generateCategoryHirarchy(catId);
						if(catIdListFromTreeService !=null && !catIdListFromTreeService.isEmpty()){
							break;
						}
					}
					 
				}
			}
			else
			{
				catIdListFromTreeService = getTRUEndecaUtils().generateCategoryHirarchy(pCategoryId);
			}
			if(catIdListFromTreeService != null && !catIdListFromTreeService.isEmpty()) {
				
				breadcrumb = constructCategoryBreadcrumbFromCategoryTreeService(catIdListFromTreeService);
				 
			}
			else{
				if(TRUEndecaConstants.HIDDEN.equalsIgnoreCase(displayStatus) && parentCatList!=null && !parentCatList.isEmpty())
				{
					breadcrumb = generateCategoryBreadcrumb(parentCatList.get(TRUEndecaConstants.INT_ZERO));
					String currentBreadCrumb = breadcrumb;
					try {
						 StringBuffer tempBread = new StringBuffer();
						 tempBread.append(currentBreadCrumb);
						 categoryDisplayName = getCatalogTools().getCategoryDisplayName(pCategoryId);
						 if(StringUtils.isNotBlank(categoryDisplayName))
							{
							 tempBread.append(TRUEndecaConstants.GREATERTHEN).append(categoryDisplayName);
							 breadcrumb = tempBread.toString();
							}
						 
					} catch (RepositoryException repositoryException) {
						
						if(isLoggingError()) {
							vlogError(repositoryException, "RepositoryException occured in TRUParametricCSVTools.populateCategoryBreadcrumb() method getting fixedParentCategoriesSet");	
						}
					}
					
				}
				else{
						breadcrumb = generateCategoryBreadcrumb(pCategoryId);
					}
										
			}
						
			if(StringUtils.isNotBlank(breadcrumb))
			{
				String currentBreadCrumb = breadcrumb;
				if(StringUtils.isNotBlank(categoryDisplayName) && !breadcrumb.contains(categoryDisplayName))
				{
					 StringBuffer tempBread = new StringBuffer();
					 tempBread.append(currentBreadCrumb);
					 tempBread.append(TRUEndecaConstants.GREATERTHEN).append(categoryDisplayName);
					 breadcrumb = tempBread.toString();
				}
				String trimmedBreadcrumb = removeSpecialCharacter(breadcrumb);
				if(StringUtils.isNotBlank(trimmedBreadcrumb)) {
					pParametricVo.setBreadCrumb(trimmedBreadcrumb);
				}
			}
			}
			pPrmetricProfileVoList.add(pParametricVo);
		}
			else {
				for (String site : pSiteId) {
					getTRUSiteMapDomainUrl().setDomainUrl(site);
					domainUrl = getTRUSiteMapDomainUrl().getDomainUrl();
					parametricProfileVO = populateParametricCSVData(domainUrl, pCategoryId, site);
					if(parametricProfileVO != null)	{
						pPrmetricProfileVoList.add(parametricProfileVO);
					}
					break;
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("END:TRUParametricCSVTools  :: filterIndexedCategory() method ");
		}
		
}


	/**
	 * This method is used to generate the CSV data for category using Rest service.
	 * @param pDomainUrl -- the domain Url
	 * @param pCategoryId -- the Category Id
	 * @param pSite -- the site
	 * @return pParametricVo -- the ParametricVo
	 */
	public ParametricProfileVO populateParametricCSVData(String pDomainUrl,String pCategoryId,String pSite) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN:::TRUParametricCSVTools.populateParametricCSVData() method. Value of categoryId is :: {0} and Domain Url is :: {1}", pCategoryId, pDomainUrl);
		}
		ParametricProfileVO parametricVo = null;
		StringBuffer siteUrl  = new StringBuffer();
		JSONObject json = null;
		//boolean redirect = false;
		HttpsURLConnection conn = null;
		BufferedReader responseBuffer = null;
		if(StringUtils.isNotBlank(pCategoryId) && StringUtils.isNotBlank(pDomainUrl) && StringUtils.isNotEmpty(getParametricCsvRestServiceURL())) {
			try {
				siteUrl.append(getTRUEndecaUtils().getProtocolScheme()).append(pDomainUrl);
				siteUrl.append(getParametricCsvRestServiceURL()).append(pCategoryId);
				if(StringUtils.isNotBlank(pSite))
				{
					siteUrl.append(TRUEndecaConstants.AMPERSAND_PUSHSITE_EQUAL).append(pSite);
				}
				//final URL url = new URL(siteUrl.toString());
				final URL url = new URL(null, siteUrl.toString(),new sun.net.www.protocol.https.Handler());
				if (isLoggingDebug()) {
					vlogDebug("TRUParametricCSVTools.populateParametricCSVData() - Final url :: {0}  ", url);
				}
				
				/*if(isProxyRequired() && getProxyConfiguration().isProxyEnabled()) {
					Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort()));
					if (isLoggingDebug()) {
						vlogDebug("TRUParametricCSVTools.populateParametricCSVData() --  proxy :: {0}  ",proxy);
					}
					conn = (HttpsURLConnection) url.openConnection(proxy);
				} else {*/
					conn = (HttpsURLConnection) url.openConnection();
				//}
				//conn.setInstanceFollowRedirects(false);
				conn.setConnectTimeout(getConnectionTimeOut());
				conn.setDoOutput(Boolean.TRUE);
				conn.setDoInput(Boolean.TRUE);
				conn.setUseCaches(Boolean.FALSE);
				conn.setRequestMethod(GET_METHOD);
				//conn.setRequestProperty(CONTENT_LENGTH, CONTENT_LENGTH_VAL);
				conn.setRequestProperty(X_APP_API_KEY, API_KEY);
				conn.setRequestProperty(X_APP_API_CHANNEL, API_CHANNEL);
				conn.setRequestProperty(TRU_ACCESS, TRU_ACCESS_VAL);
				conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
				conn.connect();
				int status = conn.getResponseCode();
				if (isLoggingDebug()) {
					vlogDebug("TRUParametricCSVTools.populateParametricCSVData() -- conn.getResponseCode() {0}  ", status);
				}
				/*if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
					redirect = true;
				}
				if (redirect) {
					// get redirect url from "location" header field
					String newUrl = conn.getHeaderField(TRUEndecaConstants.LOCATION);
					if (isLoggingDebug()) {
						vlogDebug(" Redirected Url:: {0} ", newUrl);
					}
					// open the new connnection again
					conn = (HttpsURLConnection) new URL(newUrl).openConnection();
					conn.setConnectTimeout(getConnectionTimeOut());
					conn.setDoOutput(Boolean.TRUE);
					conn.setDoInput(Boolean.TRUE);
					conn.setUseCaches(Boolean.FALSE);
					conn.setRequestMethod(GET_METHOD);
					conn.setRequestProperty(X_APP_API_KEY, API_KEY);
					conn.setRequestProperty(X_APP_API_CHANNEL, API_CHANNEL);
					conn.setRequestProperty(TRU_ACCESS, TRU_ACCESS_VAL);
					conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
					conn.connect();
					if (isLoggingDebug()) {
						vlogDebug("TRUParametricCSVTools.populateParametricCSVData() : HTTPS Status Code  :: {0}  ", conn.getResponseCode());
					}

				}*/
				if (conn.getResponseCode() != HttpsURLConnection.HTTP_OK ) {
					if (isLoggingDebug()) {
						vlogDebug(" Disconnecting the Connection :: -- conn.getResponseCode() {0}  ", conn.getResponseCode());
					}
					//conn.disconnect();
					return parametricVo;
				}
				responseBuffer = new BufferedReader(new InputStreamReader(conn.getInputStream(),Charset.forName(TRUEndecaConstants.ENCODING_FORMATTER)));
				String jsonText = readJson(responseBuffer);
				if (isLoggingDebug()) {
					vlogDebug("TRUParametricCSVTools.populateParametricCSVData() -- jsonText {0}  ", jsonText);
				}
				
				if(StringUtils.isNotEmpty(jsonText) && !TRUEndecaConstants.EMPTY_JSON.equals(jsonText) && !jsonText.contains(TRUEndecaConstants.HTML_TAG) && jsonText.startsWith(TRUEndecaConstants.CURLYBRACKET_START) && jsonText.endsWith(TRUEndecaConstants.CURLYBRACKET_END)) {
					json = new JSONObject(jsonText);
					if(json != null) {
						JSONObject categoryCsvJson = (JSONObject)json.get(TRUEndecaConstants.CATEGORY_PROFILE);
						if(categoryCsvJson != null)	{
							String breadCrumb = categoryCsvJson.getString(TRUEndecaConstants.BREADCRUMB);
							String catalogId = categoryCsvJson.getString(TRUEndecaConstants.CATALOGID);
							String displayStatus = categoryCsvJson.getString(TRUEndecaConstants.CATEGORY_DISPLAY_STATUS_CODE);
							String pageNameDisplayTxt = categoryCsvJson.getString(TRUEndecaConstants.PAGE_NAME_DISPLAY_TEXT);
							String parametricProfileNavTxt = categoryCsvJson.getString(TRUEndecaConstants.PAGE_NAME_NAV_TEXT);
							parametricVo = new ParametricProfileVO();
							if(StringUtils.isNotBlank(breadCrumb)) {
								String trimmedBreadcrumb = removeSpecialCharacter(breadCrumb);
								if(StringUtils.isNotBlank(trimmedBreadcrumb)) {
									parametricVo.setBreadCrumb(trimmedBreadcrumb);
								}
							}
							if(StringUtils.isNotBlank(catalogId)) {
								parametricVo.setCatalogId(catalogId);
							}
							if(StringUtils.isNotBlank(displayStatus)) {
								parametricVo.setCatalogStatusCD(displayStatus);
							}
							if(StringUtils.isNotBlank(pageNameDisplayTxt)) {
								parametricVo.setPageNameDisplayTxt(pageNameDisplayTxt);
							}
							if(StringUtils.isNotBlank(parametricProfileNavTxt) && !TRUEndecaConstants.NULL.equals(parametricProfileNavTxt))	{
								parametricVo.setParametricProfileNavTxt(parametricProfileNavTxt);
							}
						}
					}
				}
				//conn.disconnect();
			} catch (JSONException jsonException) {
				if (isLoggingError()) {
					vlogError("JSONException in TRUParametricCSVTools.populateParametricCSVData() method :: {0} : CategoryId {1} ",
							jsonException,pCategoryId);
				}
			}
			catch (MalformedURLException e) {
				getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_CSV,
						setParametricCSVFailureEmailTemplate(getParametericCSVEmailConfiguration().getFailureExceptionMessage(),e));
				if (isLoggingError()) {
					vlogError("MalformedURLException: TRUParametricCSVTools.populateParametricCSVData() While processing MalformedURLException with exception : {0}: CategoryId {1} ", e,pCategoryId);
				}
			} catch (IOException e) {
				getTRUEmailServiceUtils().sendSiteMapFailureEmail(TRUEndecaConstants.PARAMETRIC_CSV,
						setParametricCSVFailureEmailTemplate(getParametericCSVEmailConfiguration().getFailureExceptionMessage(),e));
				if (isLoggingError()) {
					vlogError("IOException: TRUParametricCSVTools.populateParametricCSVData() While processing IOException with exception : {0} : CategoryId {1} ", e,pCategoryId);
				}
			}
			 finally {
				    if (responseBuffer != null) {
				        try {
				        	responseBuffer.close();
				        } catch (IOException e) {
				        	if (isLoggingError()) {
								vlogError("IOException: TRUParametricCSVTools.populateParametricCSVData() While Closing the BufferedReader : {0} ", e);
							}
				        }
				    }
				    if (conn != null) {
				        conn.disconnect();
				    }
				}
		}
		if (isLoggingDebug()) {
			vlogDebug("END :TRUParametricCSVTools  :: populateParametricCSVData() method ");
		}
		return parametricVo;
	}


	/**
	 * Gets the page name.
	 *
	 * @param pUSerType the u ser type
	 * @param pCategoryId -- the Category Id
	 * @return the page name
	 */ 
	private String getPageName(String pUSerType,String pCategoryId) {
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:TRUParametricCSVTools  :: getPageName() method ");
		}
		String pageName = null; 
		if(StringUtils.isEmpty(pUSerType)) {
			return pageName;
		}
		if(pUSerType.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYCATEGORY)) {
			pageName = TRUEndecaConstants.TRU_CATEGORYLANDING;
		} else if(pUSerType.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYSUBCATEGORY)) {
			pageName =TRUEndecaConstants.TRU_SUBCATEGORYLANDING;
		} else if(pUSerType.equalsIgnoreCase(TRUEndecaConstants.TAXONOMYNODE)){
			pageName =TRUEndecaConstants.TRU_FAMILYLANDING;
		} else if(TRUCommerceConstants.TAXONOMYFILTEREDLANDINGPAGE.equals(pUSerType)) {
			int childItems = TRUCommerceConstants.INT_ZERO;
			try {
				childItems = getCatalogTools().findFixedChildClassificationItems(pCategoryId);
			} catch (RepositoryException e) {
				if(isLoggingError()) {
					vlogError(e, "RepositoryException occured in TRUCategoryPrimaryPathManager.populateCategoryURL() method getting findFixedChildClassificationItems");	
				}
			}
			if(childItems < TRUCommerceConstants.INT_TWO){
				pageName =TRUEndecaConstants.TRU_FAMILYLANDING;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Page Name {0}",pageName);
			vlogDebug("END:TRUParametricCSVTools  :: getPageName() method ");
		}
		return pageName;
	}

	/**
	 * Read json.
	 *
	 * @param pBufferedReader bufferedReader
	 * @return string
	 * @throws IOException IOException
	 */
	private  String readJson(BufferedReader pBufferedReader) throws IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:::TRUParametricCSVTools  :: readJson() method ");
		}
		StringBuilder jsonString = new StringBuilder();
		int count;
		while ((count = pBufferedReader.read()) != TRUEndecaConstants.CONSTANT_MINUS_ONE) {
			jsonString.append((char) count);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:::TRUParametricCSVTools  :: readJson() method ");
		}
		return jsonString.toString();
	}

	/**
	 * Gets the product catalog.
	 *
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets the product catalog.
	 *
	 * @param pProductCatalog the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}

	/**
	 * Returns the this property hold reference for CatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public CatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the this property hold reference for CatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Gets the dimension value cache tools.
	 *
	 * @return the dimension value cache tools
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools()  {
		return this.mDimensionValueCacheTools;
	}

	/**
	 * Sets the dimension value cache tools.
	 *
	 * @param pDimensionValueCacheTools the new dimension value cache tools
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools)  {
		this.mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	/**
	 * Gets the TRU site map domain url.
	 *
	 * @return the mTRUSiteMapDomainUrl
	 */
	public TRUSiteMapDomainUrl getTRUSiteMapDomainUrl() {
		return mTRUSiteMapDomainUrl;
	}

	/**
	 * Sets the TRU site map domain url.
	 *
	 * @param pTRUSiteMapDomainUrl the mTRUSiteMapDomainUrl to set
	 */
	public void setTRUSiteMapDomainUrl(TRUSiteMapDomainUrl pTRUSiteMapDomainUrl) {
		this.mTRUSiteMapDomainUrl = pTRUSiteMapDomainUrl;
	}


	/**
	 * Gets the TRU catalog properties.
	 *
	 * @return the mTRUCatalogProperties
	 */
	public TRUCatalogProperties getTRUCatalogProperties() {
		return mTRUCatalogProperties;
	}


	/**
	 * Sets the TRU catalog properties.
	 *
	 * @param pTRUCatalogProperties the mTRUCatalogProperties to set
	 */
	public void setTRUCatalogProperties(TRUCatalogProperties pTRUCatalogProperties) {
		this.mTRUCatalogProperties = pTRUCatalogProperties;
	}

	/**
	 * @return the parametricCsvRestServiceURL
	 */
	public String getParametricCsvRestServiceURL() {
		return mParametricCsvRestServiceURL;
	}

	/**
	 * @param pParametricCsvRestServiceURL the parametricCsvRestServiceURL to set
	 */
	public void setParametricCsvRestServiceURL(
			String pParametricCsvRestServiceURL) {
		mParametricCsvRestServiceURL = pParametricCsvRestServiceURL;
	}

	/**
	 * @return the proxyConfiguration
	 */
	public ProxyConfiguration getProxyConfiguration() {
		return mProxyConfiguration;
	}

	/**
	 * @param pProxyConfiguration the proxyConfiguration to set
	 */
	public void setProxyConfiguration(ProxyConfiguration pProxyConfiguration) {
		mProxyConfiguration = pProxyConfiguration;
	}

	/**
	 * @return the proxyRequired
	 */
	public boolean isProxyRequired() {
		return mProxyRequired;
	}

	/**
	 * @param pProxyRequired the proxyRequired to set
	 */
	public void setProxyRequired(boolean pProxyRequired) {
		mProxyRequired = pProxyRequired;
	}

	/**
	 * @return the connectionTimeOut
	 */
	public int getConnectionTimeOut() {
		return mConnectionTimeOut;
	}

	/**
	 * @param pConnectionTimeOut the connectionTimeOut to set
	 */
	public void setConnectionTimeOut(int pConnectionTimeOut) {
		mConnectionTimeOut = pConnectionTimeOut;
	}
	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Sets the category status for  category.
	 * @param pDisplayStatus -- the display status
	 * @param pParametricVo -- the parametric vo
	 */
	private void populateCategoryDisplayStatus(String pDisplayStatus, ParametricProfileVO pParametricVo){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUParametricCSVTools  :: generateCategoryDisplayStatus() method ");
		}
		if(StringUtils.isEmpty(pDisplayStatus)){
			pParametricVo.setCatalogStatusCD(pDisplayStatus);
			return;
		}
		if(pDisplayStatus.equals(TRUEndecaConstants.ACTIVE)) {
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_A);
		} else if(pDisplayStatus.equals(TRUEndecaConstants.NO_DISPLAY)) {
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_ND);
		} else if(pDisplayStatus.equals(TRUEndecaConstants.HIDDEN)) {
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_H);
		} else {
			pParametricVo.setCatalogStatusCD(pDisplayStatus);
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRUParametricCSVTools  :: generateCategoryDisplayStatus() method ");
		}
	}
	
	/**
	 * This method is used to return the category Breadcrumb .
	 * @param pCategoryId -- the category Id
	 * @return String -- the breadcrumb string
	 */
	public String generateCategoryBreadcrumb(String pCategoryId)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUParametricCSVTools  :: generateCategoryBreadcrumb() method ");
		}
		StringBuffer breadCrumbBuffer = new StringBuffer();
		List<String> breadCrumbList = new ArrayList<String>();
		List<String> finalBreadCrumbList = populateCategoryBreadcrumb(pCategoryId,breadCrumbList,TRUCommerceConstants.INT_ONE);
		Collections.reverse(finalBreadCrumbList);
		if (isLoggingDebug()) {
			vlogDebug("BreadCrumb List :: {0} ",finalBreadCrumbList);
		}
		int size = finalBreadCrumbList.size();
		for (int i = TRUEndecaConstants.INT_ZERO ; i < size ; i++) {
			String string = finalBreadCrumbList.get(i);
			breadCrumbBuffer.append(string);
			if(i != size - TRUEndecaConstants.INT_ONE)
			{
				breadCrumbBuffer.append(TRUEndecaConstants.GREATERTHEN);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUParametricCSVTools  :: generateCategoryBreadcrumb() method ");
		}
		return breadCrumbBuffer.toString();
	}
	
	/**
	 * This method create the breadCrumb string List.
	 * @param pCategoryId -- the category Id
	 * @param pBreadCrumbList -- the Breadcrumb List
	 * @param pLevel -- the level
	 * @return List -- the Breadcrumb list
	 */
	public List<String> populateCategoryBreadcrumb(String pCategoryId,List<String> pBreadCrumbList,int pLevel)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUParametricCSVTools  :: populateCategoryBreadcrumb() method ");
		}
		
		 int level = pLevel; 
		if(StringUtils.isNotBlank(pCategoryId))
		{
			String displayName;
			try {
				displayName = getCatalogTools().getCategoryDisplayName(pCategoryId);
				if(StringUtils.isNotBlank(displayName))
				{
					pBreadCrumbList.add(displayName);
				}
			} catch (RepositoryException repositoryException) {
				
				if(isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUParametricCSVTools.populateCategoryBreadcrumb() method getting fixedParentCategoriesSet");	
				}
			}
			
		}
		Set<RepositoryItem> fixedParentCategorySet = null;
		try {
			fixedParentCategorySet = getCatalogTools().findFixedParentClassificationItems(pCategoryId);
		} catch (RepositoryException repositoryException) {
			
			if(isLoggingError()) {
				vlogError(repositoryException, "RepositoryException occured in TRUParametricCSVTools.populateCategoryBreadcrumb() method getting fixedParentCategoriesSet");	
			}
		}
		List<RepositoryItem> fixedParentCategoryList = null;
		if(fixedParentCategorySet != null && !fixedParentCategorySet.isEmpty())
		{
			fixedParentCategoryList = new ArrayList<RepositoryItem>(fixedParentCategorySet);
		}
		if (fixedParentCategoryList != null && !fixedParentCategoryList.isEmpty() && !(fixedParentCategoryList.get(TRUCommerceConstants.INT_ZERO).getRepositoryId().equals(TRUCommerceConstants.NAVIGATION_TRU) || fixedParentCategoryList.get(TRUCommerceConstants.INT_ZERO).getRepositoryId().equals(TRUCommerceConstants.NAVIGATION_BRU)) && getMaxLevel() > level)	{
			level++;
			return populateCategoryBreadcrumb(fixedParentCategoryList.get(TRUCommerceConstants.INT_ZERO).getRepositoryId(),pBreadCrumbList,level);
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRUParametricCSVTools  :: populateCategoryBreadcrumb() method ");
		}
		return pBreadCrumbList;
	}
	
	
	/**
	 * Sets the Parametric CSV failure email template.
	 *
	 * @param pMessageKey the message key
	 * @param pParam the param
	 * @return the map
	 */
	private Map<String, Object> setParametricCSVFailureEmailTemplate(String pMessageKey,Object pParam) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricCSVTools.setParametricCSVFailureEmailTemplate method.. ");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String message = null;
		StringBuilder sb=new StringBuilder(getParametericCSVEmailConfiguration().getFailureParametericCSVEmailMessage());
		DateFormat dateFormat = new SimpleDateFormat(TRUEndecaConstants.MONTH_DATE_YEAR_TIME);
		Date date = new Date();
		dateFormat.format(date);
		sb.append(dateFormat.format(date));
		if(pParam == null){
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pMessageKey);
		}else {
			Exception e= (Exception)pParam;
			message = pMessageKey + getStackTrace(e);
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, sb.toString());
			templateParameters.put(TRUCommerceConstants.TEMPLATE_PARAMETER_MESSAGE_FAIL, message);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricCSVTools.setParametricCSVFailureEmailTemplate method.. ");
		}
		return templateParameters;
	}
	
	/**
	 * Gets the stack trace.
	 *
	 * @param pThrowable the throwable
	 * @return the stack trace
	 */
	private String getStackTrace(Throwable pThrowable) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricCSVTools.getStackTrace() method.. ");
		}
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    pThrowable.printStackTrace(printWriter); 
	    if(result.toString().length() > TRUEndecaConstants.INT_THOUSAND){
	    	if (isLoggingDebug()) {
				logDebug("END:: TRUParametricCSVTools.getStackTrace() method.. ");
			}
	    	return result.toString().substring(TRUEndecaConstants.INT_ZERO, TRUEndecaConstants.INT_THOUSAND);
	    }
	    if (isLoggingDebug()) {
			logDebug("END:: TRUParametricCSVTools.getStackTrace() method.. ");
		}
	    return result.toString();
	  }
	
	/**
	 * @return the maxLevel
	 */
	public int getMaxLevel() {
		return mMaxLevel;
	}

	/**
	 * @param pMaxLevel the maxLevel to set
	 */
	public void setMaxLevel(int pMaxLevel) {
		mMaxLevel = pMaxLevel;
	}
	
	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}
	
	/**
	 * @param pCategoryIdList -- the Category List
	 * @return breadcrumb -- the breadcrumb String
	 */
	public String constructCategoryBreadcrumbFromCategoryTreeService(List<String> pCategoryIdList)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricCSVTools.constructCategoryBreadcrumbFromCategoryTreeService() method.. ");
		}
		StringBuffer breadCrumb = new StringBuffer();
		int size = pCategoryIdList.size();
		for (int i = TRUEndecaConstants.INT_ZERO ; i < size ; i++) {
			String categoryId = pCategoryIdList.get(i);
			if(StringUtils.isNotBlank(categoryId))
			{
				String categoryDisplayName = null;
				try {
					 categoryDisplayName = getCatalogTools().getCategoryDisplayName(categoryId);
					if(StringUtils.isNotBlank(categoryDisplayName))
					{
						breadCrumb.append(categoryDisplayName);
						if(i != size - TRUEndecaConstants.INT_ONE)
						{
							breadCrumb.append(TRUEndecaConstants.GREATERTHEN);
						}
					}
				} catch (RepositoryException repositoryException) {
					
					if(isLoggingError()) {
						vlogError(repositoryException, "RepositoryException occured in TRUParametricCSVTools.populateCategoryBreadcrumb() method getting fixedParentCategoriesSet");	
					}
				}
				
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricCSVTools.constructCategoryBreadcrumbFromCategoryTreeService() method.. ");
		}
		return breadCrumb.toString();
	}
/**
 * This method is used to populate the Hidden category Map.
 * @return map -- the hiddenCategoryMap
 */
public Map<String,List<String>> populateAllHiddenCategory()
{
	if (isLoggingDebug()) {
		logDebug("BEGIN:: TRUParametricCSVTools.populateAllHiddenCategory() method.. ");
	}
	List<String> categoryList = null;
	Map<String,List<String>> hiddenCategoryMap = new HashMap();
	String parentCategoryId = null;
	String childCategoryId = null;
	Connection dbConnection = null;
	PreparedStatement preparedStatement = null;
	String query = getQuery();
	if (isLoggingDebug()) {
		vlogDebug("TRUParametricCSVTools.populateAllHiddenCategory() -- Query {0}  ", query);
	}
	try {
		dbConnection = getDataSource().getConnection();
		if (!StringUtils.isEmpty(query)) {
			preparedStatement = dbConnection.prepareStatement(query);
		}
		ResultSet rs = null;
		if (preparedStatement != null) {
				rs = preparedStatement.executeQuery();
		}
		if(rs != null) {
			
			while (rs.next()) {
				
				parentCategoryId = rs.getString(TRUEndecaConstants.CATEGORY_ID_CONSTANT);
				childCategoryId = rs.getString(TRUEndecaConstants.CHILD_CAT_ID_CONSTANT);
				if(hiddenCategoryMap != null)
				{
					if(!hiddenCategoryMap.isEmpty() && hiddenCategoryMap.containsKey(childCategoryId)) {
						categoryList = hiddenCategoryMap.get(childCategoryId);
						categoryList.add(parentCategoryId);
						hiddenCategoryMap.put(childCategoryId, categoryList);
					}
					else {
						categoryList = new ArrayList();
						categoryList.add(parentCategoryId);
						hiddenCategoryMap.put(childCategoryId, categoryList);
					}
				}
				
				
		}
			if (isLoggingDebug()) {
				vlogDebug("TRUParametricCSVTools.populateAllHiddenCategory() -- hiddenCategoryMap {0}  ", hiddenCategoryMap);
			}
		return hiddenCategoryMap;
	}
	} catch (SQLException sQLException) {
		vlogError("SQLException : {0}", sQLException);
	} finally {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			}
		}
		if (dbConnection != null) {
			try {
				dbConnection.close();
			} catch (SQLException sQLException) {
				vlogError("SQLException : {0}", sQLException);
			}
		}

	}
	if (isLoggingDebug()) {
		logDebug("END:: TRUParametricCSVTools.populateAllHiddenCategory() method.. ");
	}
	return null;

}
/**
 * @return the dataSource
 */
public DataSource getDataSource() {
	return mDataSource;
}

/**
 * @param pDataSource
 *            the dataSource to set
 */
public void setDataSource(DataSource pDataSource) {
	mDataSource = pDataSource;
}

/**
 * @return the query
 */
public String getQuery() {
	return mQuery;
}

/**
 * @param pQuery the query to set
 */
public void setQuery(String pQuery) {
	mQuery = pQuery;
}

/**
 * This method is used to remove special characters like quote.
 * @param pBreadCrumb -- the breadcrumb String
 * @return currentBreadCrumb -- the trimmed breadcrumb
 */
public String removeSpecialCharacter(String pBreadCrumb)
{
	if (isLoggingDebug()) {
		logDebug("BEGIN:: TRUParametricCSVTools.removeSpecialCharacter() method.. ");
	}
	String currentBreadCrumb = null;
	if(StringUtils.isNotBlank(pBreadCrumb))
	{
		 currentBreadCrumb = pBreadCrumb;
		 if (isLoggingDebug()) {
				vlogDebug("TRUParametricCSVTools.removeSpecialCharacter() method..:: Breadcrumb  :: {0} ",currentBreadCrumb);
			}
		if(currentBreadCrumb.contains(TRUEndecaConstants.SLASH_QUOTE)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.SLASH_QUOTE,TRUEndecaConstants.EMPTY_STRING);
		}
		if(currentBreadCrumb.contains(TRUEndecaConstants.SLASH_SINGLE_QUOTE)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.SLASH_SINGLE_QUOTE, TRUEndecaConstants.EMPTY_STRING);
		}
		if(currentBreadCrumb.contains(TRUEndecaConstants.NEWLINE_RETURN)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.NEWLINE_RETURN, TRUEndecaConstants.EMPTY_STRING);
		}
		if(currentBreadCrumb.contains(TRUEndecaConstants.CARRIAGE_RETURN)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.CARRIAGE_RETURN, TRUEndecaConstants.EMPTY_STRING);
		}
		if(currentBreadCrumb.contains(TRUEndecaConstants.DIVIDER)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.DIVIDER, TRUEndecaConstants.EMPTY_STRING);
		}	
		if(currentBreadCrumb.contains(TRUEndecaConstants.NBSP)) {
			currentBreadCrumb = currentBreadCrumb.replace(TRUEndecaConstants.NBSP, TRUEndecaConstants.E_SPACE);
		}	
		currentBreadCrumb = currentBreadCrumb.trim();
		if (isLoggingDebug()) {
			vlogDebug("TRUParametricCSVTools.removeSpecialCharacter() method..:: Breadcrumb after removing Special Character :: {0} ",currentBreadCrumb);
		}
	}
	
	if (isLoggingDebug()) {
		logDebug("END:: TRUParametricCSVTools.removeSpecialCharacter() method.. ");
	}
	return currentBreadCrumb;
}
}