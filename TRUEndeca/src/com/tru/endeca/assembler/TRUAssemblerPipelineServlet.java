package com.tru.endeca.assembler;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerPipelineServlet;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteManager;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.cartridge.model.UrlAction;
import com.tru.cache.TRUEndecaCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.commerce.exception.TRUEndecaException;
import com.tru.common.TRUConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.endeca.vo.RequestVO;
import com.tru.integrations.TRUIntegrationPropertiesConfig;

/**
 * Customized ATG pipeline to handle all Endeca related requests for cache implementation.
 * Prepares the cache key and checks the entry in cache send the Endeca content to presentation layer if exists,
 * if not store Endeca content in to cache for further Endeca page requests.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUAssemblerPipelineServlet extends AssemblerPipelineServlet {

	/**
	 * Constant for ASSEMBLER_CONTENT_COLLECTION.
	 */
	static final ParameterName ASSEMBLER_CONTENT_COLLECTION = ParameterName.getParameterName(TRUEndecaConstants.ASSEMBLER_CONTENT_COLLECTION);

	/**
	 * Constant for ASSEMBLER_RULE_LIMIT.
	 */
	static final ParameterName ASSEMBLER_RULE_LIMIT = ParameterName.getParameterName(TRUEndecaConstants.ASSEMBLER_RULE_LIMIT);

	/**
	 * Constant for CONTENT_ITEM_PARAMETER.
	 */
	static final ParameterName CONTENT_ITEM_PARAMETER = ParameterName.getParameterName(TRUEndecaConstants.CONTENT_ITEM_PARAMETER);

	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * This property hold reference for TRUEndecaProductCache.
	 */
	private TRUEndecaCache mEndecaProductCache;

	/**
	 * This property hold reference for TRUEndecaCache.
	 */
	private TRUEndecaCache mEndecaCache;
	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;

	/**
	 * This property hold reference for mEndecaSiteId.
	 */
	private String mEndecaSiteId;
	/**
	 * This property hold reference for mProductionUrl.
	 */
	private String mProductionUrl;
	
	/**
	 * This property hold reference for mContextRoot.
	 */
	private String mContextRoot;

	/**  Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;

	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mConfiguration;

	/** The m trusos integration properties config. */
	private TRUIntegrationPropertiesConfig mTRUIntegrationPropertiesConfig;

	/**   Holds the mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;

	/** The m enable custom content inculde. */
	private boolean mEnableCustomContentInculde;

	/** The mEnableURLValidation. */
	private boolean mEnableURLValidation;

	/** The m page site id. */
	private String mPageSiteId;

	/**
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}


	/**
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}
	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils the new TRU endeca utils
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}
	/**
	 * Gets the catalog tools.
	 *
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations
	 *            - Endeca Configurations.
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Returns endecaCache.
	 * 
	 * @return the endecaCache
	 */
	public TRUEndecaCache getEndecaCache() {
		return mEndecaCache;
	}

	/**
	 * Sets endecaCache.
	 * 
	 * @param pEndecaCache
	 *            the endecaCache to set.
	 */
	public void setEndecaCache(TRUEndecaCache pEndecaCache) {
		mEndecaCache = pEndecaCache;
	}

	/**
	 * Returns EndecaProductCache.
	 * 
	 * @return the EndecaProductCache
	 */
	public TRUEndecaCache getEndecaProductCache() {
		return mEndecaProductCache;
	}

	/**
	 * Sets EndecaProductCache.
	 * 
	 * @param pEndecaProductCache
	 *            the EndecaProductCache to set.
	 */
	public void setEndecaProductCache(TRUEndecaCache pEndecaProductCache) {
		mEndecaProductCache = pEndecaProductCache;
	}


	/**
	 * Returns the this property hold reference for TRUConfiguration.
	 * 
	 * @return the mConfiguration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * sets commerce TRUConfiguration.
	 *
	 * @param pConfiguration the TRUConfiguration to set.
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}

	/** Property Holds TRUDimensionValueCache. */
	private TRUDimensionValueCache mDimensionValueCache;
	/**
	 * Returns TRUDimensionValueCache.
	 *
	 * @return the TRUDimensionValueCache.
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}
	/**
	 * sets TRUDimensionValueCache.
	 *
	 * @param pDimensionValueCache the TRUDimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		this.mDimensionValueCache = pDimensionValueCache;
	}

	/**
	 * Service implementation of ATG servlet pipeline and handles all endeca related requests.
	 * 
	 * @param pRequest
	 *            - Dynamo Servlet Request
	 * @param pResponse
	 *            - Dynamo Servlet Response
	 * @throws IOException
	 *             - IO Exception
	 * @throws ServletException
	 *             - Default Servlet Exceptions
	 */

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException,
	ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Start: TRUAssemblerPipelineServlet :: service method ", pRequest);
		}
		//Check the mobile service context path starts with /rest
		if(StringUtils.isNotBlank(getSiteContext()) && !pRequest.getContextPath().startsWith(TRUEndecaConstants.REST_CONTEXT_PATH)){
			pRequest.setContextPath(getSiteContext());
		}

		if(pRequest.getRequestURI().contains(TRUEndecaConstants.SOS_LOGIN_URL) && !getConfiguration().isEnableSOS()) {
			pResponse.sendLocalRedirect(TRUEndecaConstants.TRU_HOME, pRequest);
			return;			
		}
		RequestVO cacheKey = null;
		Object cachedEndecaContent = null;
		boolean cacheCurrentPage = false;
		boolean enableCacheImpl = getEndecaConfigurations().isEnableEndecaContentCache();
		// Prepare Cache Key
		cacheKey = prepareCacheKey(pRequest);

		if (enableCacheImpl && !shouldIgnoreRequest(pRequest)) {
			String pageName = identifyPageName(pRequest);
			// check whether current page has to be cached or not
			cacheCurrentPage = shouldCacheCurrentPage(pRequest);
			// caching of current page

			if(isEnableURLValidation()) {
				validateRequestUrl(pRequest, pResponse);
			}

			if (cacheCurrentPage && cacheKey != null) {
				try {
					if(getEndecaConfigurations().getCacheKeyByProductId() != null && getEndecaConfigurations().getCacheKeyByProductId().contains(pageName)) {
						cachedEndecaContent = getEndecaProductCache().get(cacheKey);
					} else {
						cachedEndecaContent = getEndecaCache().get(cacheKey);
					}

				} catch (TRUEndecaException exception) {
					if (isLoggingError()) {
						vlogError(exception, "An exception occurred while getting cache from ATG {0}.", new Object[] { cacheKey });
					}
					passRequest(pRequest, pResponse);

					return;
				}
				if (cachedEndecaContent != null) {
					ContentItem contentItem = (ContentItem) cachedEndecaContent;

					//If any category page have only one record, navigate to PDP page
					if (getEndecaConfigurations().getCacheKeyByNavId() !=null && getEndecaConfigurations().getCacheKeyByNavId().contains(pageName)) {
						long totalNoOfRecords = getTotalNoOfRecords(contentItem);
						if(totalNoOfRecords == TRUEndecaConstants.INT_ONE || totalNoOfRecords == TRUEndecaConstants.ZERO){
							invokeAssembler(pRequest, pResponse);
							return;
						}	
					}
					// if cached object found return cached object
					pRequest.setParameter(TRUEndecaConstants.CONTENT_ITEM_PARAMETER, cachedEndecaContent);
					completeRequest(pRequest, pResponse, contentItem);
					if (isLoggingDebug()) {
						vlogDebug("{0} Page is cached : Servicing from ATG Cache: Cached Content Item{1} ", cacheKey.getPageName(), cachedEndecaContent);
					}

				} else {
					if (isLoggingDebug()) {
						vlogDebug("No cache found for page {0} : Calling AssemblerPipeline Servlet", cacheKey.getPageName());
					}
					// No cache Found
					invokeAssembler(pRequest, pResponse);
					// Set Endeca Assembler response in to Cache
					prepareCacheEntry(pRequest, cacheKey,pResponse);
				}
			} else {
				if (isLoggingDebug()) {
					vlogDebug("{0} URL  Need not to be cached : Calling AssemblerPipelineServlet.service() ", pRequest.getRequestURI());
				}

				invokeAssembler(pRequest, pResponse);
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("Caching has been disabled {0} :URL  is {1} : Calling AssemblerPipelineServlet.service() ", enableCacheImpl, pRequest.getRequestURI());
			}

			//Caching has been disabled for all Endeca pages
			invokeAssembler(pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			vlogDebug("End:TRUAssemblerPipelineServlet  :: service method ", pRequest);
		}
	}

	/**
	 * invokes the assembler for Endeca Experience Manager Pages.
	 * 
	 * @param pRequest
	 *            - Dynamo Http Servlet Request.
	 * @param pResponse
	 *            - Dynamo Http Servlet Response.
	 * @throws IOException
	 *             - IOException.
	 * @throws ServletException
	 *             - ServletException.
	 */
	public void invokeAssembler(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)	throws IOException, ServletException {

		if (isLoggingDebug()) {
			vlogDebug("Start:TRUAssemblerPipelineServlet  :: invokeAssembler method ", pRequest);
		}
		ContentItem responseContentItem = null;
		ContentItem contentItem = null;
		String content = null;
		int ruleLimit = TRUEndecaConstants.INT_ONE;
		String resourcePath = null;
		if(!isEnable() || shouldIgnoreRequest(pRequest)) {
			if (isLoggingDebug()) {
				vlogDebug("Ignoring (passing on) request for {0.requestURI}", new Object[] {pRequest});
			}
			passRequest(pRequest, pResponse);
			return;
		}

		if(isContentCollectionRequest(pRequest)) {
			content = pRequest.getParameter(ASSEMBLER_CONTENT_COLLECTION);
			String ruleLimitStr = pRequest.getParameter(ASSEMBLER_RULE_LIMIT);
			if(ruleLimitStr != null) {
				try	{
					ruleLimit = Integer.parseInt(ruleLimitStr);
				} catch(NumberFormatException nfe) {
					if (isLoggingError()) {
						vlogError(nfe, "An exception occurred while getting Rule Limit  {0}.", new Object[] {ruleLimitStr});
					}
					ruleLimit = TRUEndecaConstants.INT_ONE;
				}
			}
			contentItem = createContentSlotConfig(pRequest, content, ruleLimit);
		} else {
			resourcePath = getAssemblerTools().getContentPath(pRequest);
			List<String> categoryPageUrls = getEndecaConfigurations().getCategoryPageUrl();
			String promoId = pRequest.getQueryParameter(TRUEndecaConstants.PROMOTION_ID);
			String gwppromoId = pRequest.getQueryParameter(TRUEndecaConstants.GWP_PROMOTION_ID);
			String onlinePId = pRequest.getQueryParameter(TRUEndecaConstants.PDP_BREADCRUMBS_PRODUCTID);
			StringBuffer queryString = new StringBuffer();

			if(StringUtils.isEmpty(resourcePath)) {
				if (isLoggingDebug()) {
					vlogDebug("Ignoring (passing on) request for {0.requestURI}", new Object[] {pRequest});
				}
				passRequest(pRequest, pResponse);
				return;
			} else if(categoryPageUrls != null && categoryPageUrls.contains(resourcePath) && StringUtils.isEmpty(pRequest.getQueryParameter(TRUEndecaConstants.N_PARAMETER))) {
				boolean set404Status = true;

				redirectCategoryToAssociatedSite(pRequest, pResponse);

				String categoryId = null;

				if(pRequest.getRequestURI().contains(TRUEndecaConstants.LINK_JSON) || pRequest.getQueryString().contains(TRUEndecaConstants.ENDECA_CHANGE_SETS) ||
						pRequest.getQueryString().contains(TRUEndecaConstants.ENDECA_USER_SEGMENTS)) {
					set404Status = false;
				}

				if(!StringUtils.isEmpty(pRequest.getQueryParameter(TRUCommerceConstants.REGISTRY_REQ)) && pRequest.getQueryParameter(TRUCommerceConstants.REGISTRY_REQ).equals(TRUCommerceConstants.ONE_STR)) {
					String registryCategoryId = pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID);
					if(!StringUtils.isEmpty(registryCategoryId)) {
						categoryId = getCategoryIdFromRegistry(registryCategoryId);
					}
					if(!StringUtils.isEmpty(categoryId)) {
						queryString.append(TRUEndecaConstants.CATEGORY_ID).append(TRUEndecaConstants.EQUALS).append(categoryId);
					}
					set404Status = false;
				}else {
					categoryId = pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID);
					queryString.append(pRequest.getQueryString());
				}
				TRUDimensionValueCache dimCacheTools = getDimensionValueCache();
				if(!StringUtils.isEmpty(categoryId) && dimCacheTools != null) {
					DimensionValueCacheObject dimCacheObject = dimCacheTools.getCacheWithMultipleAncestors(categoryId);
					if(dimCacheObject != null) {
						String url = dimCacheObject.getUrl();
						String NValue = dimCacheObject.getDimvalId();
						if(!StringUtils.isEmpty(url) && !StringUtils.isEmpty(NValue)) {
							String[] subString = url.split(TRUEndecaConstants.PATTERN_QUESTION_MARK,TRUEndecaConstants.INT_ZERO);
							queryString.append(TRUEndecaConstants.AMPERSAND_N_EQUALS).append(NValue);
							if(subString.length > TRUEndecaConstants.INT_ONE && queryString != null && queryString.length() > TRUEndecaConstants.INT_ZERO) {
								pRequest.setRequestURI(subString[0]);
								pRequest.setQueryString(queryString.toString());
								set404Status = false;
							}
						}
					}
				}
				if(set404Status) {
					if (isLoggingDebug()) {
						vlogDebug("The CategoryId for Category Landing page is incorrect. So set 404 status.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
				}
			} else if(TRUEndecaConstants.PROMOTION_URL.equals(resourcePath) && StringUtils.isNotEmpty(promoId)) {
				boolean set404Status = true;
				TRUDimensionValueCache dimCacheTools = getDimensionValueCache();
				DimensionValueCacheObject dimCacheObjectforPromo = dimCacheTools.getCacheObject(TRUEndecaConstants.PROMOTION_UNDERSCORE+promoId);
				if(dimCacheObjectforPromo != null) {
					String NValue = dimCacheObjectforPromo.getDimvalId();
					String currentQueryString = pRequest.getQueryString();
					if(StringUtils.isNotEmpty(NValue) && StringUtils.isNotEmpty(currentQueryString)) {
						queryString.append(currentQueryString).append(TRUEndecaConstants.AMPERSAND_N_EQUALS).append(NValue);
						pRequest.setQueryString(queryString.toString());
						set404Status = false;
					}
				}
				if(set404Status) {
					if (isLoggingDebug()) {
						vlogDebug("The PromoId for Promotion Landing page is incorrect. So set 404 status.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
				}
			} else if(TRUEndecaConstants.PROMOTION_URL.equals(resourcePath) && StringUtils.isNotEmpty(gwppromoId)) {
				boolean set404Status = true;
				TRUDimensionValueCache dimCacheTools = getDimensionValueCache();
				DimensionValueCacheObject dimCacheObjectforPromo = dimCacheTools.getCacheObject(TRUEndecaConstants.GWP_PROMOTION_UNDERSCORE+gwppromoId);
				if(dimCacheObjectforPromo != null) {
					String NValue = dimCacheObjectforPromo.getDimvalId();
					String currentQueryString = pRequest.getQueryString();
					if(StringUtils.isNotEmpty(NValue) && StringUtils.isNotEmpty(currentQueryString)) {
						queryString.append(currentQueryString).append(TRUEndecaConstants.AMPERSAND_N_EQUALS).append(NValue);
						pRequest.setQueryString(queryString.toString());
						set404Status = false;
					}
				}
				if(set404Status) {
					if (isLoggingDebug()) {
						vlogDebug("The PromoId for Promotion Landing page is incorrect. So set 404 status.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
				}
			}
			else if(TRUEndecaConstants.PRODUCT_URL.equals(resourcePath) && StringUtils.isNotEmpty(onlinePId)) {
				boolean set404Status = true;
				boolean isActicveSku = false;
				String productId = null;
				TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();

				RepositoryItem[] skuItem = getCatalogTools().getSKUFromOnlinePID(onlinePId);

				if(skuItem != null && skuItem.length > 0) {
					for (RepositoryItem repositoryItem : skuItem) {
						if(repositoryItem == null) {
							continue;
						}
						isActicveSku = getTRUEndecaUtils().isActiveProductSku(repositoryItem);
						if(isActicveSku) {
							@SuppressWarnings("unchecked")
							Set<RepositoryItem> parentProducts =  (Set<RepositoryItem>) repositoryItem .getPropertyValue(catalogProperties.getParentProductsPropertyName());
							if(parentProducts != null && !parentProducts.isEmpty()) {
								Iterator<RepositoryItem> iter = parentProducts.iterator();
								RepositoryItem productItem = (RepositoryItem) iter.next();
								productId=productItem.getRepositoryId();
									if (StringUtils.isNotBlank(productId)) {
										pRequest.setAttribute(TRUEndecaConstants.PARENT_PRODUCT_ID, productId);
										set404Status = false;
										break;
									}
								}
							
						}
					}
				}

				if(set404Status) {
					if (isLoggingDebug()) {
						vlogDebug("The ProductId for PDP Landing page is incorrect. So set 404 status.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
				}
			}
			
			else if(TRUEndecaConstants.COLLECTION_URL.equals(resourcePath) && StringUtils.isNotEmpty(onlinePId)) {
				boolean set404Status = true;
				String  displayStatus = null;
				TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
				try {
					RepositoryItem collectionProduct = getCatalogTools().findProduct(onlinePId);
                       if (collectionProduct != null) {
                    	   String itemDesc = collectionProduct.getItemDescriptor().getItemDescriptorName();
                    	   if(StringUtils.isNotEmpty(itemDesc) && itemDesc.equals(TRUEndecaConstants.COLLECTION_PRODUCT)) {
                    		   displayStatus = (String) collectionProduct.getPropertyValue(catalogProperties.getDisplayStatusPropertyName());
                        	   if(!(TRUEndecaConstants.HIDDEN.equalsIgnoreCase(displayStatus))){
                        		   set404Status = false;
                        	   }
                    	   }
					}
                } catch (RepositoryException e) {
                	if (isLoggingError()) {
        				vlogError(e, "An exception occurred while getting collection product for product id");
        			}
				}

				if(set404Status) {
					if (isLoggingDebug()) {
						vlogDebug("The ProductId for PDP Landing page is incorrect. So set 404 status.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
				}
			}
			
			if(resourcePath.contains(TRUEndecaConstants.COBRAND_PAGE) && isEnableCustomContentInculde()){
				StringBuffer resourcePathBuff = new StringBuffer();
				resourcePathBuff.append(getAssemblerTools().getContentPathPrefix()).append(getPageSiteId()).append(resourcePath);
				contentItem = createCustomContentInclude(resourcePathBuff.toString());
			}else{
				contentItem = createContentInclude(pRequest, resourcePath);
			}

		}

		responseContentItem = null;
		try {
			responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
		} catch(AssemblerException ae) {
			if (isLoggingError()) {
				vlogError(ae, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] {contentItem});
			}
			passRequest(pRequest, pResponse);
			return;
		}

		if(responseContentItem != null && responseContentItem.containsKey(TRUEndecaConstants.ERROR_KEY) && pRequest.getRequestURI().contains(TRUEndecaConstants.LINK_JSON)) {
			passRequest(pRequest, pResponse);
			return;
		}
		//END : Redirect User to Error page when content contains error

		completeRequest(pRequest, pResponse, responseContentItem);
		if (isLoggingDebug()) {
			vlogDebug("End:TRUAssemblerPipelineServlet  :: invokeAssembler method ", pRequest);
		}
	}

	/**
	 * creates content Item with resource path
	 * 
	 * @param pResourcePath -- the resource path
	 * @return ContentItem
	 */
	private ContentItem createCustomContentInclude(String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}

	/**
	 * handles the json,XML format & keyword redirect requests.
	 * 
	 * @param pRequest
	 *            - Request.
	 * @param pResponse
	 *            - Response.
	 * @param pResponseContentItem
	 *            - Cached Content Item.
	 * @throws IOException
	 *             - IO Exception.
	 * @throws ServletException
	 *             - Servlet Exception.
	 */
	private void completeRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			ContentItem pResponseContentItem) throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("==BEGIN==TRUAssemblerPipelineServlet:completeRequest() ");
		}
		String returnFormat = null;
		UrlAction urlAction = null;
		String redirectRequestURL = null;
		ContentItem redirectRequest = null;

		//Return format
		returnFormat = pRequest.getParameter(getFormatParamName());
		Object rootRestore = pRequest.getLocalParameter(TRUEndecaConstants.ROOT_CONTENT_ITEM);
		pRequest.setAttribute(TRUEndecaConstants.ROOT_CONTENT_ITEM, pResponseContentItem);
		if (!StringUtils.isEmpty(returnFormat)) {
			//XML,Json Response
			getAssemblerTools().serializeContentItemToResponse(pResponseContentItem, pResponse, null, returnFormat);
		} else {
			//Redirects
			redirectRequest = (ContentItem) pResponseContentItem.get(TRUEndecaConstants.ENDECA_REDIRECT);
			if (redirectRequest != null) {
				urlAction = (UrlAction) redirectRequest.get(TRUEndecaConstants.LINK);
				redirectRequestURL = urlAction.getUrl();
				//Redirect to defined link
				redirectRequest(pRequest, pResponse, pResponseContentItem, redirectRequestURL);
			} else {
				forwardRequest(pRequest, pResponse, pResponseContentItem);
			}
		}
		if (rootRestore == null) {
			pRequest.removeAttribute(TRUEndecaConstants.ROOT_CONTENT_ITEM);
		} else {
			pRequest.setAttribute(TRUEndecaConstants.ROOT_CONTENT_ITEM, rootRestore);
		}
		if (isLoggingDebug()) {
			vlogDebug("==END==TRUAssemblerPipelineServlet:completeRequest() ");
		}

	}

	/**
	 * Prepares the Cache Key.
	 * 
	 * @param pRequest
	 *            - Request.
	 * @return RequestVO - Request Value Object as Cache key.
	 * @throws IOException
	 *             - IO Exception.
	 * @throws ServletException
	 *             - Servlet Exception.
	 */
	private RequestVO prepareCacheKey(DynamoHttpServletRequest pRequest)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Starts the TRUAssemblerPipelineServlet :: prepareCacheKey ", pRequest);
		}
		RequestVO requestVO = null;
		String siteId = null;
		String pageName = null;
		requestVO = new RequestVO();
		// Get Site Id
		siteId = SiteContextManager.getCurrentSiteId();
		// Identify Page Name
		pageName = identifyPageName(pRequest);
		if (pageName != null) {
			//Identify whether it is SOS request or not
			Site site = SiteContextManager.getCurrentSite();
			if(site != null){
				vlogDebug("Found site {0}", new Object[] { site.getId() });
				String[] siteUrls = (String[]) site.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
				List<String> sosConfiguredUrl = getTRUIntegrationPropertiesConfig().getSosConfiguredURLs();
				if (siteUrls != null && siteUrls.length > 0) {
					if (getConfiguration().isEnableSOS() && siteUrls != null
							&& !StringUtils.isBlank(pRequest.getServerName()) && sosConfiguredUrl != null &&
							!sosConfiguredUrl.isEmpty() && sosConfiguredUrl.contains(pRequest.getServerName())) {
						requestVO.setRequestFrom(TRUCommerceConstants.SOS);
					} else{
						requestVO.setRequestFrom(TRUCommerceConstants.STORE);
					}
				} 
			}

			// For Home Page
			if (getEndecaConfigurations().getCacheKeyByPageName() !=null && getEndecaConfigurations().getCacheKeyByPageName().contains(pageName)) {
				requestVO.setPageName(pageName);
				if (!StringUtils.isBlank(siteId)) {
					requestVO.setSiteId(siteId);
				}
			}
			// For RCLP,PLP(initial page load)
			else if (getEndecaConfigurations().getCacheKeyByNavId() !=null && getEndecaConfigurations().getCacheKeyByNavId().contains(pageName)) {
				requestVO.setPageName(pageName);
				if (!StringUtils.isBlank(siteId)) {
					requestVO.setSiteId(siteId);
				}
				String queryString = pRequest.getQueryString();

				if(!StringUtils.isEmpty(queryString) && queryString.contains(TRUEndecaConstants.CATEGORYID_EQUALS)) {
					String catId = pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID);
					requestVO.setCategoryId(catId);
				} else if(!StringUtils.isEmpty(queryString) && queryString.contains(TRUEndecaConstants.N_EQUALS)){
					String navID = pRequest.getQueryParameter(TRUEndecaConstants.N_PARAMETER);
					requestVO.setNavigationId(navID);
				}
			}
			//For PDP, Collection page Cacheing
			else if(getEndecaConfigurations().getCacheKeyByProductId() != null && getEndecaConfigurations().getCacheKeyByProductId().contains(pageName)) {
				if(getEndecaConfigurations().isEnableCacheForPDPPage()) {
					requestVO.setPageName(pageName);
					if (!StringUtils.isBlank(siteId)) {
						requestVO.setSiteId(siteId);
					}
					String productId = pRequest.getQueryParameter(TRUEndecaConstants.PDP_BREADCRUMBS_PRODUCTID);
					if(!StringUtils.isEmpty(productId)) {
						requestVO.setProductId(productId);
					}
					if (isLoggingDebug()) {
						vlogDebug("Cacheing for PDP page is enabled. Cache Key is : {0}",requestVO);
					}
				} else {
					return null;
				}
			}			

			if(getEndecaConfigurations().getPlpPageUrl().contains(pageName) || getEndecaConfigurations().getSclpPageUrl().contains(pageName)) {
				String requestUriWithQueryString = pRequest.getRequestURIWithQueryString();
				if(requestUriWithQueryString.contains(TRUEndecaConstants.NO) || requestUriWithQueryString.contains(TRUEndecaConstants.NRPP_PARAM) || requestUriWithQueryString.contains(TRUEndecaConstants.NS_PARAM)) {
					return null;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Ending the TRUAssemblerPipelineServlet :: prepareCacheKey ", pRequest);
		}
		return requestVO;
	}	

	/**
	 * Identifies the Page Name by passing Request URI.
	 * 
	 * @param pRequest
	 *            - Request
	 * @return String - Page Name
	 */
	private String identifyPageName(DynamoHttpServletRequest pRequest) {

		if (isLoggingDebug()) {
			vlogDebug("Starts the TRUAssemblerPipelineServlet :: identifyPageName ", pRequest);
		}
		String requestURI = null;
		String pageType = null;

		int splitStringsSize = TRUEndecaConstants.INT_ZERO;
		List<String> cachablePages = null;
		// URI
		requestURI = pRequest.getRequestURI();
		if (isLoggingDebug()) {
			logDebug("Current requestURI is " + requestURI);
		}

		String[]  splitStrings = requestURI.split(TRUEndecaConstants.PATH_SEPERATOR);
		// '/' as Seperator
		if (splitStrings != null && splitStrings.length > TRUEndecaConstants.INT_ZERO) {
			splitStringsSize = splitStrings.length;
			for (int i = 0; i < splitStringsSize; i++) {
				pageType = splitStrings[i];
				cachablePages = getEndecaConfigurations().getCacheAllowedPages();
				if (cachablePages != null && cachablePages.contains(pageType)) {// Can be cached
					if (isLoggingDebug()) {
						vlogDebug("TRUAssemblerPipelineServlet.identifyPageName,  Requested for Page{0}", pageType);
					}
					return pageType;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Ending the TRUAssemblerPipelineServlet :: identifyPageName ", pRequest);
		}

		return pageType;

	}

	/**
	 * Prepares the Value Object that is going to be cached.
	 *
	 * @param pRequest            -DynamoHttpServletRequest pRequest.
	 * @param pCacheKey 				-CacheKey
	 * @param pResponse the response
	 * @throws IOException             - throws IO Exception.
	 * @throws ServletException             - ServletException.
	 */
	private void prepareCacheEntry(DynamoHttpServletRequest pRequest, RequestVO pCacheKey,DynamoHttpServletResponse pResponse) throws IOException,
	ServletException {
		if (isLoggingDebug()) {
			vlogDebug("Starts the TRUAssemblerPipelineServlet :: prepareCacheEntry ", pRequest);
		}
		// If status is 404, don't make cache entry.
		if(pResponse.getStatus() == TRUEndecaConstants.INT_FOUR_ZERO_FOUR){
			return;
		}
		if (pCacheKey != null && !StringUtils.isBlank(pCacheKey.getPageName())
				&& !StringUtils.isBlank(pCacheKey.getSiteId())) {
			Object contentItem = pRequest.getLocalParameter(CONTENT_ITEM_PARAMETER);
			if (contentItem != null) {
				if(getEndecaConfigurations().getCacheKeyByProductId() != null && getEndecaConfigurations().getCacheKeyByProductId().contains(pCacheKey.getPageName())) {
					getEndecaProductCache().put(pCacheKey, contentItem);
				} else {
					getEndecaCache().put(pCacheKey, contentItem);
				}

			} else {
				if (isLoggingDebug()) {
					vlogDebug("No Response found from Assembler for Cache Key {0} :", new Object[] { pCacheKey });
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Ending the TRUAssemblerPipelineServlet :: prepareCacheEntry ", pRequest);
		}
	}

	/**
	 * Checks whether current page has to cached or not.
	 * 
	 * @param pRequest
	 *            - Request.
	 * @return boolean - boolean Flag.
	 */
	private boolean shouldCacheCurrentPage(DynamoHttpServletRequest pRequest) {

		String requestURI = null;
		String pageType = null;
		int splitStringsSize = TRUEndecaConstants.INT_ZERO;
		List<String> cachablePages = null;
		if (isLoggingDebug()) {
			vlogDebug("Starts the TRUAssemblerPipelineServlet :: shouldCacheCurrentPage ", pRequest);
		}
		// URI from Request
		requestURI = pRequest.getRequestURI();
		if (isLoggingDebug()) {
			logDebug("Current requestURI is " + requestURI);
		}
		List<String> categoryPageUrls = getEndecaConfigurations().getCategoryPageUrl();
		if(categoryPageUrls != null && !StringUtils.isEmpty(requestURI)) {
			for(String categoryPageUrl : categoryPageUrls) {
				if(!StringUtils.isEmpty(categoryPageUrl) && requestURI.contains(categoryPageUrl)
						&& !StringUtils.isEmpty(pRequest.getQueryParameter(TRUEndecaConstants.N_PARAMETER))) {
					return false;
				}
			}
		}

		String[] splitStrings = requestURI.split(TRUEndecaConstants.PATH_SEPERATOR);
		// '/' as Seperator
		if (splitStrings != null && splitStrings.length > TRUEndecaConstants.INT_ZERO) {
			splitStringsSize = splitStrings.length;
			for (int i = 0; i < splitStringsSize; i++) {
				pageType = splitStrings[i];
				cachablePages = getEndecaConfigurations().getCacheAllowedPages();
				if (cachablePages != null && cachablePages.contains(pageType)) {// Can be cached
					if (isLoggingDebug()) {
						vlogDebug("TRUAssemblerPipelineServlet.shouldCacheCurrentPage,  Requested for Page{0}", pageType);
					}// Process Browse & shop,Search requests
					return true;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Ending the TRUAssemblerPipelineServlet :: shouldCacheCurrentPage ", pRequest);
		}

		return false;

	}
	/**
	 * Checks whether the redirect url contains http, https or not.
	 * If it contains http then use absolute url otherwise use Site Specific Url redirect.
	 * 
	 * @param pRequest
	 * 			- DynamoHttpServletRequest request parameter.
	 *  @param pResponse
	 * 			- DynamoHttpServletRequest response parameter.
	 *  @param pContentItem
	 * 			- content Item object.
	 *  @param pRedirectURL
	 * 			- redirectUrl.
	 *
	 *  @throws IOException
	 *  		- throws IO Exception.
	 */
	@Override
	protected void redirectRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, ContentItem pContentItem, String pRedirectURL)
			throws IOException
			{
		if (isLoggingDebug())
		{
			vlogDebug("===BEGIN===:: TRUAssemblerPipelineServlet.redirectRequest method:: ");
		}
		String searchTerm  = pRequest.getQueryParameter(TRUEndecaConstants.NTT_PARAM);
		String endecaSiteId = null;
		String siteProductionUrl = null;
		String siteContextRoot = null;
		if(!StringUtils.isBlank(pRedirectURL)) {
			Site currentSite = SiteContextManager.getCurrentSite();
			if(currentSite != null) {
				endecaSiteId = (String) currentSite.getPropertyValue(getEndecaSiteId());
				siteProductionUrl = (String) currentSite.getPropertyValue(getProductionUrl());
				siteContextRoot = (String)currentSite.getPropertyValue(getContextRoot());
			}
			if(pRedirectURL.startsWith(TRUEndecaConstants.HTTP) || pRedirectURL.startsWith(TRUEndecaConstants.HTTPS)) {
				if(!StringUtils.isEmpty(pRedirectURL) && !pRedirectURL.contains(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS) && 
					pRedirectURL.contains(TRUEndecaConstants.QUESTION_MARK_SYMBOL)){
					pRedirectURL = pRedirectURL.concat(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS + searchTerm);
				}
				pResponse.sendRedirect(pRedirectURL,true);
			} else {
				String redirectUrl=null;

				if(!StringUtils.isEmpty(endecaSiteId) && pRedirectURL.startsWith(endecaSiteId)) {
					redirectUrl = pRedirectURL.replace(endecaSiteId, TRUEndecaConstants.EMPTY_STRING);
				}
				if(!StringUtils.isEmpty(siteContextRoot) && pRedirectURL.startsWith(siteContextRoot)){
					redirectUrl = pRedirectURL.replace(siteContextRoot, TRUEndecaConstants.EMPTY_STRING);
				}
				
				if(!StringUtils.isEmpty(siteProductionUrl) && !pRedirectURL.startsWith(siteProductionUrl)) {
					StringBuffer url = new StringBuffer();
					url.append(siteProductionUrl).append(pRedirectURL);
					redirectUrl = url.toString();
				}
				if(redirectUrl != null){
					if(!redirectUrl.contains(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS) && redirectUrl.contains(TRUEndecaConstants.QUESTION_MARK_SYMBOL)){
						redirectUrl = redirectUrl.concat(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS + searchTerm);
					}
					pResponse.sendRedirect(redirectUrl);
				}else{
					pResponse.sendRedirect(pRedirectURL);
				}	
			}
		}
		if (isLoggingDebug())
		{
			vlogDebug("===END===:: TRUAssemblerPipelineServlet.getContent method:: ");
		}
			}

	/**
	 * The forwardRequest Method has been overridden to achieve the Domain Based
	 * Site Url changes.
	 *
	 * @param pRequest 			- DynamoHttpServletRequest request parameter.
	 * @param pResponse 			- DynamoHttpServletRequest response parameter.
	 * @param pContentItem 			- content Item object.
	 * @throws ServletException             - ServletException.
	 * @throws IOException             - throws IO Exception.
	 */

	protected void forwardRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, ContentItem pContentItem) throws ServletException, IOException {

		if (isLoggingDebug()) {
			vlogDebug("=====BEGIN=====:: TRUAssemblerPipelineServlet.forwardRequest method:: ");
		}
		String requestUri = pRequest.getRequestURI();
		pRequest.setParameter(TRUEndecaConstants.CONTENT_ITEM_PARAMETER, pContentItem);

		String contentRenderer = getAssemblerTools().getRendererPathForContentItem(pRequest, pContentItem);

		RequestDispatcher dispatcher = null;
		String siteBaseURL = null;
		if ((SiteManager.getSiteManager() != null) && (SiteContextManager.getCurrentSite() != null)) {
			siteBaseURL = (String)SiteContextManager.getCurrentSite().getPropertyValue(SiteManager.getSiteManager().getProductionURLPropertyName());
		}

		if (siteBaseURL == null) {
			vlogDebug("No site base URL found, initializing dispatcher with path: {0}", new Object[] { contentRenderer });
			dispatcher = pRequest.getRequestDispatcher(contentRenderer);
		} else {
			String context = ServletUtil.getCurrentContextPath(ServletUtil.getCurrentRequest());

			if ((context != null) && (siteBaseURL.startsWith(context))) {
				siteBaseURL = siteBaseURL.substring(context.length(), siteBaseURL.length());
			}

			vlogDebug("Site base URL found, initializing dispatcher with path: {0}{1}", new Object[] { siteBaseURL, contentRenderer });
			dispatcher = pRequest.getRequestDispatcher(new StringBuilder().append(contentRenderer).toString());
		}
		pRequest.setRequestURI(requestUri);
		dispatcher.forward(pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("=====END=====:: TRUAssemblerPipelineServlet.forwardRequest method:: ");
		}
	}

	/**
	 * The getCategoryIdFromRegistry method will return the Category Id equivalent to the Registry Category Id.
	 *
	 * @param pCategoryId            - pCategoryId.
	 *  
	 * @return the Category Id equivalent to the Registry Category Id.
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String getCategoryIdFromRegistry(String pCategoryId) {
		RepositoryItem categoryItem = null;
		try {
			categoryItem = getCatalogTools().findCategory(pCategoryId);
			if(categoryItem!=null && categoryItem.getItemDescriptor().getItemDescriptorName().equals(getEndecaConfigurations().getRegistryClassificationItemName())){
				List<RepositoryItem> crossReferenceList = (List<RepositoryItem>) categoryItem.getPropertyValue(getEndecaConfigurations().getCrossReferencePropertyName());
				if(crossReferenceList!=null && !crossReferenceList.isEmpty()){
					String currentSiteId = SiteContextManager.getCurrentSiteId();
					Set siteIdsSet=null;
					for(RepositoryItem classificationItem:crossReferenceList){
						siteIdsSet = (Set) classificationItem.getPropertyValue(getEndecaConfigurations().getSiteIdsPropertyName());
						if(StringUtils.isNotBlank(currentSiteId) && siteIdsSet!=null && siteIdsSet.contains(currentSiteId)){
							return classificationItem.getRepositoryId();
						}
					}
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e, "An exception occurred while getting item for registry id");
			}
		}
		return null;
	}

	/**
	 * Gets the total no of records.
	 *
	 * @param pContentItem the content item.
	 * @return the total no of recods.
	 */
	@SuppressWarnings("rawtypes")
	private long getTotalNoOfRecords(ContentItem pContentItem){
		long totalNoOfrecords = 0;
		List contentsList = (List)pContentItem.get(TRUEndecaConstants.CONTENTS);
		if(contentsList != null &&  ! contentsList.isEmpty()){
			Map resultListMap = (Map)contentsList.get(TRUEndecaConstants.INT_ZERO);
			if(resultListMap != null){
				List mainProductList = (List)resultListMap.get(TRUEndecaConstants.MAIN_PRODUCT);
				if(mainProductList != null && !mainProductList.isEmpty()){
					for (Object resultListObject : mainProductList) {
						Map resultList = (Map)resultListObject;
						if(resultList != null){
							Set keySet = resultList.keySet();
							if(keySet!=null && !keySet.isEmpty() && keySet.contains(TRUEndecaConstants.TOTAL_NUM_RECS)){
								totalNoOfrecords = (Long)resultList.get(TRUEndecaConstants.TOTAL_NUM_RECS);
								break;
							}	
						}
					}
				}	
			}	
		}

		return totalNoOfrecords;
	}

	/**
	 * Redirect category to associated site.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void redirectCategoryToAssociatedSite(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN TRUAssemblerPipelineServlet.redirectCategoryToAssociatedSite");
		}

		if (!StringUtils.isEmpty(pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID))) {
			DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
			String contextPath = null;
			StringBuffer redirectUrl = new StringBuffer();
			if (StringUtils.isEmpty(contextPath)) {
				contextPath = currentRequest.getContextPath();
			}
			try {
				RepositoryItem categoryItem = getCatalogTools().findCategory(pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID));

				if (categoryItem != null) {
					@SuppressWarnings("unchecked")
					Set<String> siteIdList = (Set<String>) categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
					String currentSiteId = SiteContextManager.getCurrentSiteId();
					DimensionValueCacheObject cacheObj = getDimensionValueCache().getCacheObject(pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID));
					if(siteIdList != null && !siteIdList.isEmpty() && currentSiteId != null  && !StringUtils.isEmpty(currentSiteId) && !siteIdList.contains(currentSiteId)){
						String categoryContextPath =getTRUEndecaUtils().getCategoryContextPath(pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID));
						if(!StringUtils.isEmpty(categoryContextPath)) {
							contextPath = categoryContextPath;
							if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{
								redirectUrl.append(contextPath);
							}
							redirectUrl.append(cacheObj.getUrl().toString());
							pResponse.sendRedirect(redirectUrl.toString());

							if (isLoggingDebug()) {
								vlogDebug("Redrict URL  {0} :", redirectUrl.toString());
							}

						}else{
							pResponse.sendRedirect(getTRUEndecaUtils().getCategoryContextPath(pRequest.getQueryParameter(TRUEndecaConstants.CATEGORY_ID)));
						}
					}
				}

			}catch (RepositoryException repositoryException) {
				vlogError(repositoryException, "RepositoryException occured in TRUAssemblerPipelineServlet.getCategoryContextPath");
			}
			if (isLoggingDebug()) {
				vlogDebug("END TRUAssemblerPipelineServlet.redirectCategoryToAssociatedSite");
			}
		}
	}

	/**
	 * Gets the site context.
	 *
	 * @return the site context.
	 */
	private static String getSiteContext(){
		if( SiteContextManager.getCurrentSite()!=null){
			String siteProdUrl = (String)SiteContextManager.getCurrentSite().getPropertyValue(TRUEndecaConstants.PRODUCTION_URL);
			if(siteProdUrl !=null){ 
				return siteProdUrl;
			}
		}
		return null;
	}

	/**
	 * This method will validate the URL and set the 404 status if the URL is invalid.
	 * 
	 * @param pRequest the request
	 * @param pResponse the response
	 * 
	 * @return isValidUrl -- whether teh url is valid. 
	 */
	@SuppressWarnings("unchecked")
	public boolean validateRequestUrl(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		if (isLoggingDebug()) {
			vlogDebug("BEGIN ::: TRUAssemblerPipelineServlet.validateRequestUrl method");
		}
		boolean isValidUrl = true;
		String resourcePath = getAssemblerTools().getContentPath(pRequest);
		Map<String, String> queryParamsList = getEndecaConfigurations().getValidQueryParamsForEndecaPages();
		List<String> validEndecaUrlWithoutQueryParams = getEndecaConfigurations().getValidEndecaUrlWithoutQueryParams();
		
		if(StringUtils.isEmpty(resourcePath) || resourcePath.contains(TRUEndecaConstants.COBRAND_PAGE)) {
			return isValidUrl;
		}
		
		//Return the url as valid if the current page is Static page URL.
		Site currentSite = SiteContextManager.getCurrentSite();
		if(currentSite != null) {
			List<String> customSiteUrls = (List<String>) currentSite.getPropertyValue(getCatalogProperties().getSiteSpecificStaticPageUrlPropertyName());
			if(customSiteUrls != null && !customSiteUrls.isEmpty()) {
				for(String customUrl : customSiteUrls) {
					if(StringUtils.isNotEmpty(customUrl) && resourcePath.contains(customUrl)) {
						return isValidUrl;
					}
				}
			}
		}

		//URL validation for the Endeca pages without Query Parameter
		if(validEndecaUrlWithoutQueryParams != null && !validEndecaUrlWithoutQueryParams.isEmpty() && !validEndecaUrlWithoutQueryParams.contains(resourcePath)) {
			for(String endecaUrl : validEndecaUrlWithoutQueryParams) {
				if(StringUtils.isNotEmpty(endecaUrl) && resourcePath.contains(endecaUrl) && !resourcePath.equals(endecaUrl)) {
					if (isLoggingDebug()) {
						vlogDebug("The current url is Invalid for Endeca page without Query Paramter. So setting the 404 status for the Response.");
					}
					pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
					isValidUrl = false;
					break;
				}
			}
		}

		//URL validation for the Endeca pages which should have mandatory Query Parameters
		if(queryParamsList != null && !queryParamsList.isEmpty() && isValidUrl) {
			for(Entry<String, String> entry : queryParamsList.entrySet()) {
				String pageName = entry.getKey();
				String queryParamName = entry.getValue();
				if(StringUtils.isNotEmpty(pageName) && resourcePath.contains(pageName) && StringUtils.isNotEmpty(queryParamName)) {
					String queryParamValue = pRequest.getQueryParameter(queryParamName);
					if (isLoggingDebug()) {
						vlogDebug("The resourcePath:::{0} and the Endeca page name::: {1} and the Query Param::: {2}", resourcePath, pageName, queryParamName);
					}
					if(StringUtils.isEmpty(queryParamValue) || !resourcePath.equals(pageName)) {
						if (isLoggingDebug()) {
							vlogDebug("The current url is Invalid. So setting the 404 status for the Response.");
						}
						pResponse.setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
						isValidUrl = false;
						break;
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END ::: TRUAssemblerPipelineServlet.validateRequestUrl method");
		}
		return isValidUrl;

	}

	/**
	 * Gets the endeca site id.
	 *
	 * @return the endeca site id.
	 */
	public String getEndecaSiteId() {
		return mEndecaSiteId;
	}

	/**
	 * Sets the endeca site id.
	 *
	 * @param pEndecaSiteId the new endeca site id
	 */
	public void setEndecaSiteId(String pEndecaSiteId) {
		this.mEndecaSiteId = pEndecaSiteId;
	}

	/**
	 * Gets the production url.
	 *
	 * @return the production url.
	 */
	public String getProductionUrl() {
		return mProductionUrl;
	}

	/**
	 * Sets the production url.
	 *
	 * @param pProductionUrl the new production url.
	 */
	public void setProductionUrl(String pProductionUrl) {
		this.mProductionUrl = pProductionUrl;
	}

	/**
	 * Gets the TRU integration properties config.
	 *
	 * @return mTRUIntegrationPropertiesConfig the TRU integration properties config
	 */
	public TRUIntegrationPropertiesConfig getTRUIntegrationPropertiesConfig() {
		return mTRUIntegrationPropertiesConfig;
	}

	/**
	 * Sets the TRU integration properties config.
	 *
	 * @param pTRUIntegrationPropertiesConfig the new TRU integration properties config
	 */
	public void setTRUIntegrationPropertiesConfig(
			TRUIntegrationPropertiesConfig pTRUIntegrationPropertiesConfig) {
		mTRUIntegrationPropertiesConfig = pTRUIntegrationPropertiesConfig;
	}

	/**
	 * Gets the catalog properties.
	 *
	 * @return mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the CatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}


	/**
	 * Checks if is enable custom content inculde.
	 *
	 * @return true, if is enable custom content inculde
	 */
	public boolean isEnableCustomContentInculde() {
		return mEnableCustomContentInculde;
	}


	/**
	 * Sets the enable custom content inculde.
	 *
	 * @param pEnableCustomContentInculde the new enable custom content inculde
	 */
	public void setEnableCustomContentInculde(boolean pEnableCustomContentInculde) {
		mEnableCustomContentInculde = pEnableCustomContentInculde;
	}

	/**
	 * Checks if is Enable URL Validation.
	 *
	 * @return true, if is Enable URL Validation
	 */
	public boolean isEnableURLValidation() {
		return mEnableURLValidation;
	}


	/**
	 * Sets the Enable URL Validation.
	 *
	 * @param pEnableURLValidation the Enable URL Validation
	 */
	public void setEnableURLValidation(boolean pEnableURLValidation) {
		mEnableURLValidation = pEnableURLValidation;
	}


	/**
	 * Gets the page site id.
	 *
	 * @return the page site id
	 */
	public String getPageSiteId() {
		return mPageSiteId;
	}


	/**
	 * Sets the page site id.
	 *
	 * @param pPageSiteId the new page site id
	 */
	public void setPageSiteId(String pPageSiteId) {
		mPageSiteId = pPageSiteId;
	}


	/**
	 * @return the mContextRoot
	 */
	public String getContextRoot() {
		return mContextRoot;
	}


	/**
	 * @param pContextRoot the mContextRoot to set
	 */
	public void setContextRoot(String pContextRoot) {
		this.mContextRoot = pContextRoot;
	}
	
	
}