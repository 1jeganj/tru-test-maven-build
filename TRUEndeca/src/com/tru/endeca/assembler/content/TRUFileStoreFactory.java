package com.tru.endeca.assembler.content;

import java.io.IOException;

import atg.endeca.assembler.AssemblerTools;
import atg.endeca.assembler.content.ExtendedFileStoreFactory;

import com.endeca.infront.content.source.support.StoreUpdateResult;
import com.tru.cache.TRUEndecaCache;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.ral.performanceutil.fhl.cache.PARALCacheDroplet;


/**
 * Customized the implementation for Default File store factory.
 * Whenever we run promote content and update store method is called we will flush the Cache.
 * 
 */
public class TRUFileStoreFactory extends ExtendedFileStoreFactory {
	
	
	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;
	
	/**
	 * This property hold reference for TRUEndecaCache.
	 */
	private TRUEndecaCache mEndecaCache;
	
	/**
	 * This property hold reference for PARALCacheDroplet.
	 */
	private PARALCacheDroplet mHomePageCache;
	
	/**
	 * This property hold reference for TRUEndecaProductCache.
	 */
	private TRUEndecaCache mEndecaProductCache;
	
	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations
	 *            - Endeca Configurations
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Returns endecaCache.
	 * 
	 * @return the endecaCache
	 */
	public TRUEndecaCache getEndecaCache() {
		return mEndecaCache;
	}

	/**
	 * Sets endecaCache.
	 * 
	 * @param pEndecaCache
	 *            the endecaCache to set
	 */
	public void setEndecaCache(TRUEndecaCache pEndecaCache) {
		mEndecaCache = pEndecaCache;
	}
	
	/**
	 * Returns HomePageCache.
	 * 
	 * @return the HomePageCache
	 */
	public PARALCacheDroplet getHomePageCache() {
		return mHomePageCache;
	}

	/**
	 * Sets HomePageCache.
	 * 
	 * @param pHomePageCache
	 *            the HomePageCache to set
	 */
	public void setHomePageCache(PARALCacheDroplet pHomePageCache) {
		mHomePageCache = pHomePageCache;
	}
	
	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}
	
	/**
	 * Returns EndecaProductCache.
	 * 
	 * @return the EndecaProductCache
	 */
	public TRUEndecaCache getEndecaProductCache() {
		return mEndecaProductCache;
	}

	/**
	 * Sets EndecaProductCache.
	 * 
	 * @param pEndecaProductCache
	 *            the EndecaProductCache to set.
	 */
	public void setEndecaProductCache(TRUEndecaCache pEndecaProductCache) {
		mEndecaProductCache = pEndecaProductCache;
	}

	
	
	/**
	 * This method is used For updateStore.
	 *
	 *
	 * @throws IllegalStateException              -IllegalStateException
	 * @throws IOException               - IO Exception
	 * @return StoreUpdateResult  
	 */
	
	@Override
	public StoreUpdateResult updateStore() throws IllegalStateException, IOException {
		
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUFileStoreFactory.updateStore method:: ");
		}
		boolean enableCacheFlush = getEndecaConfigurations().isEnableCacheFlushForPromoteContent();
		if(enableCacheFlush) {
			getEndecaCache().flush();
			getHomePageCache().flushCache();
			getEndecaProductCache().flush();
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug("Flushing the contents of TRU Endeca Cache");
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUFileStoreFactory.updateStore method:: ");
		}
		return super.updateStore();
		
	}
	

}