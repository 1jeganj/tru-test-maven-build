package com.tru.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.List;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.endeca.assembler.configuration.AssemblerApplicationConfiguration;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.vo.ShopByDimensionVO;
/**
 * This Class TRUShopByDimensionHandler Gives Brand and Character Dimensions.
 *	@version 1.0 
 *	@author PA
 */

public class TRUShopByDimensionHandler extends NavigationCartridgeHandler<ContentItem, BasicContentItem> {
	
	/**
	 * This property holds ENEQuery object.
	 */
	private ENEQuery mQuery;
	
	/**
	 * This property hold reference assemblerApplicationConfig.
	 */
	private AssemblerApplicationConfiguration mAssemblerApplicationConfig;
	
	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;
	
	/**
	 * Check whether the Assembler logging error is enabled.
	 *
	 * @return the assemblerApplicationConfig
	 */
	private boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();
	}
	
	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	private void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}
	/**
	 * This method will process to get Brand/Character Dimension only.
	 * 
	 * @param pContentItem the results list.
	 * @throws CartridgeHandlerException this method is used to handle CartridgeHandlerException 
	 */
	public void preprocess(ContentItem pContentItem) throws CartridgeHandlerException {

		AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUShopByDimensionHandler.preprocess method:: ");
		DimValIdList dimValidList = null;
		String dimensionId = null;
		List<String> navRecordFilters = null;
		dimValidList = new DimValIdList();
		dimensionId = (String) pContentItem.get(TRUEndecaConstants.DIMENSION_ID);
		mQuery = null;
		try {
			// Prepare Endeca query
			mQuery = new UrlENEQuery(TRUEndecaConstants.NAVIGATION_ZERO, TRUEndecaConstants.ENCODING_FORMATTER);

		} catch (UrlENEQueryParseException urlENEQueryParseException) {
			if(isLoggingError()) {
				vlogError( urlENEQueryParseException,"::Exception while creating ENE Query from TRUShopByDimensionHandler:preprocess method::");
			}
			}
		if (mQuery != null) {
			mQuery.setNavAllRefinements(true);

			if (!StringUtils.isBlank(dimensionId)) {
				// Brand/Character Dimension N ID
				dimValidList.addDimValueId(Long.parseLong(dimensionId));
			}
			// Sets the offset to zero
			mQuery.setNavERecsOffset(TRUEndecaConstants.CONSTANT_ZERO);
			// Sets the Site Record Filter to get site specific
			// Brands/Characters
			navRecordFilters = getNavigationState().getFilterState().getRecordFilters();
			if (navRecordFilters != null & !navRecordFilters.isEmpty()) {
				for (String recFilter : navRecordFilters) {
					// Site Id filter
					if (recFilter.contains(TRUEndecaConstants.PRODUCT_SITE_ID) || recFilter.contains(TRUEndecaConstants.SKU_SITE_ID)) {
						mQuery.setNavRecordFilter(recFilter);
						break;
					}
				}
			}

			// Sets the maximum number of records
			// that can be returned for the navigation query.
			mQuery.setNavNumERecs(TRUEndecaConstants.CONSTANT_ZERO);
			// Get Brand/Character Dimension only
			mQuery.setNavDescriptors(dimValidList);
		}

		AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUShopByDimensionHandler.preprocess method:: ");

	}
	/**
	 * This method will get all Brand/Character Dimension Values from Dimension.
	 * 
	 * @param pContentItem the results list.
	 * @throws CartridgeHandlerException this method is used to handle CartridgeHandlerException 
	 * @return pContentItem.
	 */
	public BasicContentItem process(ContentItem pContentItem) throws CartridgeHandlerException {

		AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUShopByDimensionHandler.process method:: ");
		ENEQueryResults results = null;
		ENEConnection eneConnection = null;
		List<ShopByDimensionVO> dimensionsList = null;
		String dimensionId = null;
		DimValList dimValList = null;
		eneConnection = new HttpENEConnection(getAssemblerApplicationConfig().getCurrentMdexHostName().toString(), getAssemblerApplicationConfig().getCurrentMdexPort());
		dimensionsList = new ArrayList<ShopByDimensionVO>();
		// get Brands/Characters Data From Endeca
		try {
			results = eneConnection.query(mQuery);
		} catch (ENEQueryException eNEQueryException) {
			if(isLoggingError()) {
			vlogError( eNEQueryException,"::Exception while Executing ENE Query : TRUShopByDimensionHandler.process method::");
		
			}
			}
		dimensionId = (String) pContentItem.get(TRUEndecaConstants.DIMENSION_ID);

		if (results != null && results.getNavigation() != null && !StringUtils.isBlank(dimensionId)) {
			// Get Brand/Character Dimension
			Dimension dimension = results.getNavigation().getRefinementDimensions().getDimension(Long.parseLong(dimensionId));
			if (dimension != null && dimension.getRefinements() != null) {
				// Get all Brand/Character Dimension Values from Dimension
				// Object
				dimValList = dimension.getRefinements();
				if (dimValList != null && !dimValList.isEmpty()) {
					// Prepare a custom Value Object List
					dimensionsList = prepareDimensionsList(dimValList);
				}

			}
		}

		AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUShopByDimensionHandler.process method:: ");

		if (dimensionsList != null) {
			// Setting custom Brand value Object List to Content Item
			pContentItem.put(TRUEndecaConstants.DIMENSIONS_LIST, dimensionsList);
			return (BasicContentItem) pContentItem;
		}
		return null;
	}
	/**
	 * This method is  getting Brand/Character URL from ATG Dimension value cache.
	 * 
	 * @param pDimValList the results list.
	 * @return dimensionsList.
	 */
	private List<ShopByDimensionVO> prepareDimensionsList(DimValList pDimValList) {

		AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUShopByDimensionHandler.prepareDimensionsList method:: ");
		ShopByDimensionVO dimensionVO = null;
		DimVal dimVal = null;
		String dimensionName = null;
		String dimValId = null;
		DimensionValueCacheObject dimCacheObject = null;
		String dimensionUrl = null;
		List<ShopByDimensionVO> dimensionsList = null;
		dimensionsList = new ArrayList<ShopByDimensionVO>();
		final int size = pDimValList.size();
		AssemblerTools.getApplicationLogging().vlogDebug(" Brand/Character List Size ::{0} :", size);
		// For each Brand/Character Dimension Value
		for (int j = 0; j < size; j++) {
			dimVal = (DimVal) pDimValList.get(j);
			if (dimVal != null) {
				dimensionName = dimVal.getName();
				dimValId = String.valueOf(dimVal.getId());
			}
			// Getting Brand/Character URL from ATG Dimension value cache
			dimCacheObject = getDimensionValueCache().getCachedObjectForDimval(dimValId);
			if (dimCacheObject != null) {
				dimensionUrl = dimCacheObject.getUrl();
				if (!StringUtils.isBlank(dimensionName) && !StringUtils.isBlank(dimensionUrl)) {
					dimensionVO = new ShopByDimensionVO();
					dimensionVO.setDimensionId(dimValId);
					dimensionVO.setDimensionName(dimensionName);
					dimensionVO.setDimensionURL(dimensionUrl);
					AssemblerTools.getApplicationLogging().vlogDebug(" Brand/Character Url is ::{0} for Brand/Character {1} :", dimensionUrl, dimensionName);
					dimensionsList.add(dimensionVO);
				}
			} else {
				AssemblerTools.getApplicationLogging().vlogDebug("No Dimension value cache Entry Found {0} for :: ", dimValId);

			}

		}

		AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUShopByDimensionHandler.prepareDimensionsList method:: ");

		return dimensionsList;
	}
	
	/* (non-Javadoc)
	 * @see com.endeca.infront.cartridge.NavigationCartridgeHandler#wrapConfig(com.endeca.infront.assembler.ContentItem)
	 */
	@Override
	
	protected ContentItem wrapConfig(ContentItem pContentItem) {
		return new BasicContentItem(pContentItem);
	}

	/**
	 * Gets the query.
	 * @return the query
	 */
	public ENEQuery getQuery() {
		return mQuery;
	}

	/**
	 * Sets the query.
	 * @param pQuery the query to set
	 */
	public void setQuery(ENEQuery pQuery) {
		mQuery = pQuery;
	}

	/**
	 * Gets the assemblerApplicationConfig.
	 * @return the assemblerApplicationConfig
	 */
	public AssemblerApplicationConfiguration getAssemblerApplicationConfig() {
		return mAssemblerApplicationConfig;
	}

	/**
	 * Sets the assemblerApplicationConfig.
	 * @param pAssemblerApplicationConfig the assemblerApplicationConfig to set
	 */
	public void setAssemblerApplicationConfig(AssemblerApplicationConfiguration pAssemblerApplicationConfig) {
		mAssemblerApplicationConfig = pAssemblerApplicationConfig;
	}

	/**
	 * Gets the dimensionValueCache.
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets the dimensionValueCache.
	 * @param pDimensionValueCache the dimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}
	
}
