package com.tru.endeca.assembler.cartridge.handler;

import atg.endeca.assembler.AssemblerTools;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.BreadcrumbsConfig;
import com.endeca.infront.cartridge.BreadcrumbsHandler;
import com.endeca.infront.cartridge.support.BreadcrumbBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.site.model.SiteState;
import com.endeca.navigation.ENEQueryResults;
import com.tru.endeca.constants.TRUEndecaConstants;


/**
 * The Class TRUCrumbsHandler.
 * @version 1.0
 * @author PA
 */
public class TRUCrumbsHandler extends BreadcrumbsHandler {


	/** The m mdex request. */
	private MdexRequest mMdexRequest;

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();

	}
	/**
	 * used for pre process.
	 * @param  pCartridgeConfig the catridge config
	 * @throws CartridgeHandlerException 
	 *
	 */
	public void preprocess(BreadcrumbsConfig pCartridgeConfig)
			throws CartridgeHandlerException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===BEGIN===:: TRUCrumbsHandler.preprocess method:: ");
		}
		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUCrumbsHandler.preprocess method:: ");
		}
	}
	/**
	 * used for creating Breadcrumbs.
	 * @param  pCartridgeConfig the catridge config
	 * @throws CartridgeHandlerException 
	 * @see com.endeca.infront.cartridge.BreadcrumbsHandler#process(com.endeca.infront.cartridge.BreadcrumbsConfig)
	 * @return breadcrumbs.
	 */
	public Breadcrumbs process(BreadcrumbsConfig pCartridgeConfig)
			throws CartridgeHandlerException {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===BEGIN===:: TRUCrumbsHandler.process method:: ");
		}
		ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
		NavigationState navigationState = getNavigationState();
		navigationState.inform(results);
		Breadcrumbs breadcrumbs = new Breadcrumbs(pCartridgeConfig);
		SiteState siteState=getSiteState();
		if(results != null && results.getNavigation() != null && 
				results.getNavigation().getTotalNumAggrERecs() > TRUEndecaConstants.CONSTANT_ZERO) {
			BreadcrumbBuilder.createAllBreadcrumbs(breadcrumbs, results.getNavigation(), navigationState,siteState, getActionPathProvider());
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUCrumbsHandler.process method:: ");
		}
		return breadcrumbs;
	}


}
