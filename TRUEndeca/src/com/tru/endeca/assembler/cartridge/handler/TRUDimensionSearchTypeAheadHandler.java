/*
 * @(#) TRUDimensionSearchTypeAheadHandler.java 
 * 
 * Copyright 2014 PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.tru.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.cartridge.DimensionSearchResultsHandler;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.cartridge.support.DimensionSearchResultsBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.request.DimensionSearchMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * TRUDimensionSearchTypeAheadHandler fetches Dimension Search Results.
 * @version 1.0
 * @author PA
 */
public class TRUDimensionSearchTypeAheadHandler extends DimensionSearchResultsHandler {

	/**
	 * Holds the mdex request.
	 */
	private MdexRequest mMdexRequest;

	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;
	
	/** Property Holds siteContextManager. */
	private SiteContextManager mSiteContextManager;
	
	/**
	 *  The m tru get site type util. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	
	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * @return the DimensionValueCache
	 * 				returns the DimensionValueCache Object.
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets Dimension Value Cache.
	 * @param pDimensionValueCache
	 *            the DimensionValueCache to set.
	 */
	public void setDimensionValueCache(
			TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}

	/**
	 * Prepares the Query for Dimension Search.
	 * 
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 * 
	 */
	@Override
	public void preprocess(DimensionSearchResultsConfig pCartridgeConfig)
			throws CartridgeHandlerException{
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===BEGIN===:: TRUDimensionSearchTypeAheadHandler.preprocess method:: ");
		}
		NavigationState navigationState = getNavigationState();
		SearchFilter searchFilter = getDimensionSearchFilter(navigationState);
		if (searchFilter != null) {
			NavigationState dimNavState = createDimensionSearchNavigationState(navigationState);
			this.mMdexRequest =
					createMdexRequest(dimNavState.getFilterState(), buildMdexQuery(pCartridgeConfig, searchFilter));
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUDimensionSearchTypeAheadHandler.preprocess method:: ");
		}
	}

	/**
	 * Executes the Dimension Search query and provides type-ahead suggestions.
	 * 
	 * 
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 * @return DimensionSearchResults Object
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DimensionSearchResults process(DimensionSearchResultsConfig pCartridgeConfig)
			throws CartridgeHandlerException {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===BEGIN===:: TRUDimensionSearchTypeAheadHandler.process method:: ");
		}
		List<DimensionSearchValue> dimensionSearchValues = null;
		ENEQueryResults results = null;
		NavigationState navigationState = null;
		DimensionSearchResults dimensionSearchResults = null;
		List<DimensionSearchGroup> dimensionSearchGroups = null;
		if (this.mMdexRequest != null) {
			results = executeMdexRequest(this.mMdexRequest);
			navigationState = getNavigationState();
			navigationState.inform(results);

			dimensionSearchResults = new DimensionSearchResults(pCartridgeConfig);

			if(dimensionSearchResults instanceof DimensionSearchResults)
			{		
				DimensionSearchResultsBuilder.build(
						getActionPathProvider(),
						dimensionSearchResults,
						navigationState,
						null, results.getDimensionSearch(),
						pCartridgeConfig.getDimensionList(),
						pCartridgeConfig.getMaxResults(),
						pCartridgeConfig.isShowCountsEnabled());
				dimensionSearchGroups =
						(List<DimensionSearchGroup>) ((Map<String, Object>) dimensionSearchResults)
						.get(TRUEndecaConstants.DIMENSION_SEARCH_GROUPS);
				if (!dimensionSearchGroups.isEmpty()) {
					for (DimensionSearchGroup dimensionSearchGroup : dimensionSearchGroups) {
						if(dimensionSearchGroup.getDimensionName().equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)){
							dimensionSearchValues = dimensionSearchGroup.getDimensionSearchValues();
							List<DimensionSearchValue> dimSearchValList = new ArrayList<DimensionSearchValue>();
							for (DimensionSearchValue dimSearchVals : dimensionSearchValues) {
								isClickable(dimSearchVals.getProperties(), dimSearchVals, dimSearchValList);
							}
							dimensionSearchGroup.setDimensionSearchValues(dimSearchValList);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUDimensionSearchTypeAheadHandler.process method:: ");
		}
		return dimensionSearchResults;
	}

	/**
	 * Checks if is clickable.
	 * 
	 * @param pPropertiesMap
	 *            the properties map
	 * @param pDimSearchVals
	 *            the dim search vals
	 * @param pDimSearchValList
	 *            the dim search val list
	 */
	public void isClickable(Map<String, String> pPropertiesMap, DimensionSearchValue pDimSearchVals, List<DimensionSearchValue> pDimSearchValList) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUDimensionSearchTypeAheadHandler.isClickable method::");
		}
		//String isClickable = null;
		StringBuffer categoryPageUrl = null;
		DimensionValueCacheObject dimCacheObject = null;
		String categoryId = null;
		String displayStatus=null;
		String contextPath = null;
		String url = null;
		String finalcategoryUrl = null;
		if (!pPropertiesMap.isEmpty()) {
			//isClickable = pPropertiesMap.get(TRUEndecaConstants.CLICKABLE_CATEGORY);
			categoryId = pPropertiesMap.get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
			
			displayStatus=pPropertiesMap.get(TRUEndecaConstants.DISPLAY_STATUS);
			//Begin My Change
			categoryPageUrl = new StringBuffer();
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			Object siteIds =pPropertiesMap.get(TRUEndecaConstants.CATEGORY_SITEIDS);
			String sites=null;
			String[] siteListArray=null;
			List siteList=null;
			if (siteIds instanceof String) {
				sites=(String)siteIds;
				if (!StringUtils.isBlank(sites)) {
					siteListArray=sites.split(TRUCommerceConstants.COMMA);
					siteList   =Arrays.asList(siteListArray);
				}
			}
			if (siteList!=null  && !(siteList.isEmpty())){
					if(siteList.contains(currentSiteId)) 
					{
						contextPath = getSiteProductionUrl(currentSiteId);
						categoryPageUrl.append(getSchemeDetectionUtil().getScheme());
						categoryPageUrl.append(contextPath);
						
					}
					else
					{
						contextPath = getSiteProductionUrl((String)siteList.get(TRUEndecaConstants.INT_ZERO));
						categoryPageUrl.append(getSchemeDetectionUtil().getScheme());
						categoryPageUrl.append(contextPath);
					}
			}
			
			//End My Change
			
			if (!StringUtils.isBlank(categoryId)) {
				dimCacheObject = getDimensionValueCache().getCacheObject(categoryId);
				if (dimCacheObject != null) {
					url = dimCacheObject.getUrl();
					finalcategoryUrl= categoryPageUrl.append(url).toString();
					if (!StringUtils.isBlank(finalcategoryUrl)) {
						pDimSearchVals.setNavigationState(finalcategoryUrl);
					}
				}
			}

			if (pDimSearchVals != null  &&  ! (TRUEndecaConstants.HIDDEN.equalsIgnoreCase(displayStatus)|| (TRUCommerceConstants.NO_DISPLAY.equalsIgnoreCase(displayStatus))) ) {
				pDimSearchValList.add(pDimSearchVals);
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUDimensionSearchTypeAheadHandler.isClickable method:: finalcategoryUrl = {0}",finalcategoryUrl);
		}
	}

	/**
	 * Builds the mdex query.
	 * 
	 * @param pItemConfig
	 *            the item config.
	 * @param pSearch
	 *            the search.
	 * @return the dimension search mdex query.
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception.
	 */
	private static DimensionSearchMdexQuery buildMdexQuery(DimensionSearchResultsConfig pItemConfig,
			SearchFilter pSearch) throws CartridgeHandlerException {
		DimensionSearchMdexQuery query = new DimensionSearchMdexQuery();
		if (pItemConfig.getDimensionList() != null && pItemConfig.getDimensionList().size() > 0) {
			query.setDimensionValues(pItemConfig.getDimensionList());
		}

		query.setShowCountsEnabled(pItemConfig.isShowCountsEnabled());

		if (pItemConfig.getRelRankStrategy() == null ||
				 TRUEndecaConstants.EMPTY_STRING.equals(pItemConfig.getRelRankStrategy())) {
			query.setRelRankStrategy(null);
		} else {
			query.setRelRankStrategy(pItemConfig.getRelRankStrategy());
		}

		if (pSearch != null) {
			query.setTerms(pSearch.getTerms());
		}

		query.setMaxDvalsPerDimension(pItemConfig.getMaxResultsPerDimension());

		return query;
	}
	
	/**
	 * This method will return the ContextPath from the Site production url.
	 * 
	 * @param pSiteId - SIte Id
	 * @return String siteProductUrl
	 */
	public String getSiteProductionUrl(String pSiteId) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===START===:: TRUDimensionSearchTypeAheadHandler.getSiteProductionUrl method:: ");
		}
		String contextPath = null;
		Site site = null;
		if(!StringUtils.isEmpty(pSiteId)) {
			try {
				site = getSiteContextManager().getSite(pSiteId);
				if(site != null) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, site);
					if(truSiteVO != null ) {
						contextPath = truSiteVO.getSiteURL();
					}
				}
			} catch (SiteContextException siteContextException) {
				if (isLoggingError()) {
				vlogError(siteContextException,"Site Context Exception occured in TRUResultsListHandler.getSiteProductionUrl");
				}
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUDimensionSearchTypeAheadHandler.getSiteProductionUrl method:: Site Production Url = {0}",contextPath);
		}
		return contextPath;
	}

	/**
	 * Check whether the Assembler logging error is enabled.
	 * 
	 * @return the assemblerApplicationConfig
	 */
	@SuppressWarnings("unused")
	private boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();
	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();

	}
	
	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	private void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}
	

	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * @return the tRUGetSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}

	/**
	 * @param pTRUGetSiteTypeUtil the tRUGetSiteTypeUtil to set
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}

}
