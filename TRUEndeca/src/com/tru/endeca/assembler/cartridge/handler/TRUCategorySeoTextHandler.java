package com.tru.endeca.assembler.cartridge.handler;

import java.util.Map;

import atg.endeca.assembler.AssemblerTools;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;
import com.tru.commerce.catalog.vo.TRUClassificationMediaVO;
import com.tru.commerce.catalog.vo.TRUClassificationVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUCategoryTreeUtils;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * This Class used to get the SEO text for the current navigation.
 * @version 1.0
 * @author PA
 *
 */
public class TRUCategorySeoTextHandler  extends NavigationCartridgeHandler<ContentItem, ContentItem> {

	/**
	 * This property hold reference for MdexRequest.
	 */
	private MdexRequest mMdexRequest;

	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private TRUCategoryTreeUtils mCategoryUtils;
	
	/**   The endeca Configuration constant. */
	private TRUEndecaConfigurations mEndecaConfiguration;

	/**
	 * Gets the endeca configuration.
	 *
	 * @return the endecaConfiguration
	 */
	public TRUEndecaConfigurations getEndecaConfiguration() {
		return mEndecaConfiguration;
	}

	/**
	 * Sets the endeca configuration.
	 *
	 * @param pEndecaConfiguration the endecaConfiguration to set
	 */
	public void setEndecaConfiguration(TRUEndecaConfigurations pEndecaConfiguration) {
		mEndecaConfiguration = pEndecaConfiguration;
	} 

	/**
	 * Gets the category utils.
	 *
	 * @return gets the CategoryUtil
	 */
	public TRUCategoryTreeUtils getCategoryUtils() {
		return mCategoryUtils;
	}

	/**
	 * Sets the category utils.
	 *
	 * @param pCategoryUtils sets the CategoryUtil
	 */
	public void setCategoryUtils(TRUCategoryTreeUtils pCategoryUtils) {
		this.mCategoryUtils = pCategoryUtils;
	}
	
	
	/**
     * Checks if is logging error.
     *
      * @return the loggingError
     */
     public boolean isLoggingDebug() {
            return AssemblerTools.getApplicationLogging().isLoggingDebug();
     }
     
     /**
      * Vlog debug.
      *
      * @param pMessage the message
      */
     private void vlogDebug(String pMessage) {
         AssemblerTools.getApplicationLogging().vlogDebug(pMessage);
     }
     
     /**
      *  
      * This method create the Mdex request for the current navigation.
      *
      * @param pContentItem the content item
      * @throws CartridgeHandlerException the cartridge handler exception
      */
     public void preprocess(ContentItem pContentItem)
 			throws CartridgeHandlerException {
 		if (isLoggingDebug()){
 			vlogDebug("BEGIN::  TRUCategorySeoTextHandler.preprocess() method::");
 		}

 		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());
 		
 		if (isLoggingDebug()){
 			vlogDebug("END::  TRUCategorySeoTextHandler.preprocess() method::");
 		}
 	}
     
     /**
      * This method get classification seo text html content for current navigation.
      *
      * @param pContentItem the content item
      * @return pContentItem ContentItem
      * @throws CartridgeHandlerException the cartridge handler exception
      */
     @Override
     public ContentItem process(ContentItem pContentItem)throws CartridgeHandlerException {
    	 if (isLoggingDebug()){
    		 vlogDebug("BEGIN::  TRUCategorySeoTextHandler.process() method");
    	 } 
    	 ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
    	 TRUClassificationVO classificationVo = getCategoryUtils().getClassificationVOForCurrentNavigation(pContentItem, results);
    	 if(classificationVo != null){
    		 Map<String,TRUClassificationMediaVO> relatedMedia = classificationVo.getRelatedMediaContent();
    		 String seoHtmlContent =(String) getCategoryUtils().getCollectionMediaContentt(relatedMedia, getEndecaConfiguration().getSeoTextKey());
    		 pContentItem.put(TRUEndecaConstants.CLASSIFICATION_SEO_TEXT, seoHtmlContent);
    	 }
    	 if (isLoggingDebug()){
    		 vlogDebug("END::  TRUCategorySeoTextHandler.process() method");
    	 }
    	 return pContentItem;
     }
     
     /* 
 	 * @return ContentItem 
 	 */
 	@Override
 	protected ContentItem wrapConfig(ContentItem pContentItem) {
 		
 		return new BasicContentItem(pContentItem);
 	}
 	
}
