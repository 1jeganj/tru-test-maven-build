package com.tru.endeca.assembler.cartridge.handler;

import java.util.Map;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;
import com.tru.commerce.catalog.vo.TRUClassificationMediaVO;
import com.tru.commerce.catalog.vo.TRUClassificationVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUCategoryTreeUtils;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * This class used to get the Collection page data.
 * @version 1.0
 * @author PA.
 */
public class TruCollectionHeroContentHandler  extends NavigationCartridgeHandler<ContentItem, ContentItem> {

	/**
	 * This property hold reference for MdexRequest.
	 */
	private MdexRequest mMdexRequest;

	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private TRUCategoryTreeUtils mCategoryUtils;
	
	/**   The endeca Configuration constant. */
	private TRUEndecaConfigurations mEndecaConfiguration;
	
	/**   The DefaultCollectionImage. */
	private String mDefaultCollectionImage;

	/**
	 * Gets the default collection image.
	 *
	 * @return the default collection image
	 */
	public String getDefaultCollectionImage() {
		return mDefaultCollectionImage;
	}

	/**
	 * Sets the default collection image.
	 *
	 * @param pDefaultCollectionImage the new default collection image
	 */
	public void setDefaultCollectionImage(String pDefaultCollectionImage) {
		this.mDefaultCollectionImage = pDefaultCollectionImage;
	}

	/**
	 * Gets the endeca configuration.
	 *
	 * @return the endecaConfiguration
	 */
	public TRUEndecaConfigurations getEndecaConfiguration() {
		return mEndecaConfiguration;
	}

	/**
	 * Sets the endeca configuration.
	 *
	 * @param pEndecaConfiguration the endecaConfiguration to set
	 */
	public void setEndecaConfiguration(TRUEndecaConfigurations pEndecaConfiguration) {
		mEndecaConfiguration = pEndecaConfiguration;
	} 

	/**
	 * Gets the category utils.
	 *
	 * @return gets the CategoryUtil
	 */
	public TRUCategoryTreeUtils getCategoryUtils() {
		return mCategoryUtils;
	}

	/**
	 * Sets the category utils.
	 *
	 * @param pCategoryUtils sets the CategoryUtil
	 */
	public void setCategoryUtils(TRUCategoryTreeUtils pCategoryUtils) {
		this.mCategoryUtils = pCategoryUtils;
	}
	
	
	/**
     * Checks if is logging error.
     *
      * @return the loggingError
     */
     public boolean isLoggingDebug() {
            return AssemblerTools.getApplicationLogging().isLoggingDebug();
     }
     
     /**
      * Vlog debug.
      *
      * @param pMessage the message
      */
     private void vlogDebug(String pMessage) {
         AssemblerTools.getApplicationLogging().vlogDebug(pMessage);
     }
     
     /** 
      * This method create the Mdex request for the current navigation.
      * @param pContentItem the ContentItem
      * @throws CartridgeHandlerException the CartridgeHandlerException
      */
     public void preprocess(ContentItem pContentItem)
 			throws CartridgeHandlerException {
 		if (isLoggingDebug()){
 			vlogDebug("BEGIN::  TruCollectionHeroContentHandler.preprocess() method::");
 		}

 		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());
 		
 		if (isLoggingDebug()){
 			vlogDebug("END::  TruCollectionHeroContentHandler.preprocess() method::");
 		}
 	}
     
     /**
      * This method set the Collection page data for the current navigation.
      * @param pContentItem the ContentItem
      * @return pContentItem ContentItem
      * @throws CartridgeHandlerException the CartridgeHandlerException
      * 
      */
     @Override
     public ContentItem process(ContentItem pContentItem)throws CartridgeHandlerException {
    	 if (isLoggingDebug()){
    		 vlogDebug("BEGIN::  TruCollectionHeroContentHandler.process() method");
    	 } 
    	 ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
    	 TRUClassificationVO classificationVo = getCategoryUtils().getClassificationVOForCurrentNavigation(pContentItem, results);
    	 if(classificationVo != null){
    		 Map<String,TRUClassificationMediaVO> relatedMedia = classificationVo.getRelatedMediaContent();

    		 getCollectionImage(relatedMedia, pContentItem);
    		 pContentItem.put(TRUEndecaConstants.COLLECTION_NAME, classificationVo.getClaasificationName());
    		 pContentItem.put(TRUEndecaConstants.COLLECTION_DESC, classificationVo.getClassificationLongDesc());

    	 }
    	 if (isLoggingDebug()){
    		 vlogDebug("END::  TruCollectionHeroContentHandler.process() method");
    	 }
    	 return pContentItem;
     }
     
     /**
      * This method get the Collection Image form Related media content, if available. Otherwise set default image
      * @param pRelatedMedia the Map
      * @param pContentItem the ContentItem
      */
     private void getCollectionImage(Map<String,TRUClassificationMediaVO> pRelatedMedia, ContentItem pContentItem){
    	 if (isLoggingDebug()){
			 vlogDebug("BEGIN::  TruCollectionHeroContentHandler.getCollectionImage() method");
		 }
    	 if(pRelatedMedia != null && !pRelatedMedia.isEmpty()){
				TRUClassificationMediaVO mediaVo =  pRelatedMedia.get(getEndecaConfiguration().getCategoryImageKey());
				String collectionImageUrl = null;
				if(mediaVo != null){
					collectionImageUrl = mediaVo.getMediaURL();
				}
				if(!StringUtils.isEmpty(collectionImageUrl)){
					pContentItem.put(TRUEndecaConstants.COLLECTION_IAMGE_URL, collectionImageUrl);
				}else{
					pContentItem.put(TRUEndecaConstants.COLLECTION_IAMGE_URL, getDefaultCollectionImage());
				}	
			}else{
				pContentItem.put(TRUEndecaConstants.COLLECTION_IAMGE_URL, getDefaultCollectionImage());
			}
    	 if (isLoggingDebug()){
			 vlogDebug("End::  TruCollectionHeroContentHandler.getCollectionImage() method");
		 }
     }

     /* 
 	 * @return ContentItem
 	 */
 	@Override
 	protected ContentItem wrapConfig(ContentItem pContentItem) {
 		
 		return new BasicContentItem(pContentItem);
 	}
 	
}
