package com.tru.endeca.assembler.cartridge.handler;

import java.util.HashMap;
//import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.BreadcrumbsConfig;
import com.endeca.infront.cartridge.BreadcrumbsHandler;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUCategoryTreeUtils;
import com.tru.endeca.vo.BreadCrumbsVO;
import com.tru.endeca.vo.MegaMenuVO;

/**
 * This Class Creates the Navigation BreadCrumbs TRUBreadcrumbsHandler.java.
 *	@version 1.0 
 *  @author Professional Access
 */
public class TRUBreadcrumbsHandler extends BreadcrumbsHandler {
	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private TRUCategoryTreeUtils mCategoryUtils;

	/**
	 * @return gets the CategoryUtil
	 */
	public TRUCategoryTreeUtils getCategoryUtils() {
		return mCategoryUtils;
	}

	/**
	 * @param pCategoryUtils
	 *            sets the CategoryUtil
	 */
	public void setCategoryUtils(TRUCategoryTreeUtils pCategoryUtils) {
		this.mCategoryUtils = pCategoryUtils;
	}
	/**
	 * used for creating Breadcrumbs.
	 * @param  pCartridgeConfig the catridge config
	 * @throws CartridgeHandlerException this method is used to handle CartridgeHandlerException 
	 */
	@Override
	public void preprocess(BreadcrumbsConfig pCartridgeConfig)
			throws CartridgeHandlerException {
		AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUBreadcrumbsHandler.preprocess() method:: ");
		super.preprocess(pCartridgeConfig);
		AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUBreadcrumbsHandler.preprocess() method:: ");
	}
	/**
	 *  Used to create the BreadCrumbs.
	 *  @param pCartridgeConfig , the catridge config
	 *  @return  breadcrumbs , the breadcrumbs
	 *  @throws CartridgeHandlerException this method is used to handle CartridgeHandlerException 
	 */
	@Override
	public Breadcrumbs process(BreadcrumbsConfig pCartridgeConfig)
			throws CartridgeHandlerException {
		AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUBreadcrumbsHandler.process() method:: ");
		HashMap<String, Map<String, String>> rootClassifications = null;
        Breadcrumbs breadcrumbs = null;
		BreadCrumbsVO breadCrumbsVO = null;
		MegaMenuVO megaMenuVO = null;
		String classificationName = null;
		// Calling super method
		if(pCartridgeConfig != null)
		{
		 breadcrumbs = super.process(pCartridgeConfig);
		}
		
		if(breadcrumbs != null)
		{
		List<RefinementBreadcrumb> refineCrumbs = breadcrumbs
				.getRefinementCrumbs();
		if (refineCrumbs != null && !refineCrumbs.isEmpty()) {
			for (RefinementBreadcrumb refineCrumb : refineCrumbs) {
				if (refineCrumb != null && refineCrumb.getDimensionName().equals(
								TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
					classificationName = refineCrumb.getLabel();
					Map<String,String> refinementProperties = (HashMap<String,String>)refineCrumb.getProperties();
					if(refinementProperties != null && !refinementProperties.isEmpty() && refinementProperties.containsKey(TRUEndecaConstants.DISPLAY_STATUS) && (TRUCommerceConstants.NO_DISPLAY.equalsIgnoreCase(refinementProperties.get(TRUEndecaConstants.DISPLAY_STATUS)) || TRUCommerceConstants.HIDDEN.equalsIgnoreCase(refinementProperties.get(TRUEndecaConstants.DISPLAY_STATUS))))
					{
						ServletUtil.getCurrentResponse().setStatus(TRUEndecaConstants.INT_FOUR_ZERO_FOUR);
					}
					break;
				}
			}
		}
		}
		megaMenuVO = getCategoryUtils().getMegaMenuVO();
		AssemblerTools.getApplicationLogging().vlogDebug("===Breadcrumbs===::  MegaMenuVO :: {0}" + megaMenuVO);
		if(megaMenuVO !=null)
		{
		 rootClassifications = (HashMap<String, Map<String, String>>) getCategoryUtils()
				.getAllRootCategories(megaMenuVO);
	if(!StringUtils.isBlank(classificationName))
		{	
		breadCrumbsVO = getCategoryUtils().getAllSubCategories(
				megaMenuVO, classificationName);
			}
		}
		
		if (breadcrumbs != null && !breadcrumbs.isEmpty()) {
			if (rootClassifications != null && !rootClassifications.isEmpty()) {
				breadcrumbs.put(TRUEndecaConstants.ROOT_CLASSIFICATION, rootClassifications);
			}

			if (breadCrumbsVO != null) {
				breadcrumbs.put(TRUEndecaConstants.BREADCRUMBS, breadCrumbsVO);
			}

		}
		AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUBreadcrumbsHandler.process() method:: ");

		return breadcrumbs;
	}

}
