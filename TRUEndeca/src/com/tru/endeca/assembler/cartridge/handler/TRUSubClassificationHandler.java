package com.tru.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;
import com.tru.commerce.catalog.vo.ClassificationUtilKeyVO;
import com.tru.commerce.catalog.vo.TRUClassificationVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.SortCategoryByDisplayOrderComparator;
import com.tru.endeca.utils.TRUCategoryTreeUtils;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.vo.BreadCrumbsVO;

/**
 * This class provide the level of sub-classification tree hierarchy for the current classification. 
 * @version 1.0
 * @author Copyright PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
public class TRUSubClassificationHandler extends NavigationCartridgeHandler<ContentItem, ContentItem> {

	/**
	 * Property to hold SortCategoryByDisplayOrderComparator.
	 */
	private SortCategoryByDisplayOrderComparator mCategoryDisplayOrderComparator;

	/**
	 * This property hold reference for MdexRequest.
	 */
	private MdexRequest mMdexRequest;

	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private TRUCategoryTreeUtils mCategoryUtils;

	/**
	 * This property hold reference for TRUCategoryTreeUtils.
	 */
	private int mMaxLevelofSubClassification;

	/**   The endeca Configuration constant. */
	private TRUEndecaConfigurations mEndecaConfiguration;

	/**
	 * Gets the category utils.
	 *
	 * @return gets the CategoryUtil
	 */
	public TRUCategoryTreeUtils getCategoryUtils() {
		return mCategoryUtils;
	}

	/**
	 * Sets the category utils.
	 *
	 * @param pCategoryUtils sets the CategoryUtil
	 */
	public void setCategoryUtils(TRUCategoryTreeUtils pCategoryUtils) {
		this.mCategoryUtils = pCategoryUtils;
	}

	/**
	 * Checks if is logging error.
	 *
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}


	/**
	 * Gets the max levelof sub classification.
	 *
	 * @return the max levelof sub classification
	 */
	public int getMaxLevelofSubClassification() {
		return mMaxLevelofSubClassification;
	}

	/**
	 * Sets the max levelof sub classification.
	 *
	 * @param pMaxLevelofSubClassification the new max levelof sub classification
	 */
	public void setMaxLevelofSubClassification(int pMaxLevelofSubClassification) {
		this.mMaxLevelofSubClassification = pMaxLevelofSubClassification;
	}
	/**
	 * Gets the endeca configuration.
	 *
	 * @return the endecaConfiguration
	 */
	public TRUEndecaConfigurations getEndecaConfiguration() {
		return mEndecaConfiguration;
	}

	/**
	 * Sets the endeca configuration.
	 *
	 * @param pEndecaConfiguration the endecaConfiguration to set
	 */
	public void setEndecaConfiguration(TRUEndecaConfigurations pEndecaConfiguration) {
		mEndecaConfiguration = pEndecaConfiguration;
	} 
	/**
	 * Vlog debug.
	 *
	 * @param pMessage the message
	 */

	/**
	 * @return the categoryDisplayOrderComparator
	 */
	public SortCategoryByDisplayOrderComparator getCategoryDisplayOrderComparator() {
		return mCategoryDisplayOrderComparator;
	}

	/**
	 * @param pCategoryDisplayOrderComparator the categoryDisplayOrderComparator to set
	 */
	public void setCategoryDisplayOrderComparator(
			SortCategoryByDisplayOrderComparator pCategoryDisplayOrderComparator) {
		mCategoryDisplayOrderComparator = pCategoryDisplayOrderComparator;
	}

	/**
	 * Vlog debug.
	 *
	 * @param pMessage the message
	 */
	private void vlogDebug(String pMessage) {
		AssemblerTools.getApplicationLogging().vlogDebug(pMessage);
	}

	/* (non-Javadoc)
	 * @see com.endeca.infront.cartridge.NavigationCartridgeHandler#wrapConfig(com.endeca.infront.assembler.ContentItem)
	 */
	@Override
	protected ContentItem wrapConfig(ContentItem pContentItem) {
		return new BasicContentItem(pContentItem);
	}

	/**
	 * This method used to creating Mdex request for current navigation..
	 *
	 * @param pContentItem the content item
	 * @throws CartridgeHandlerException the cartridge handler exception
	 */
	public void preprocess(ContentItem pContentItem)
			throws CartridgeHandlerException {
		if (isLoggingDebug()){
			vlogDebug("BEGIN::  TRUSubClassificationHandler.preprocess() method::");
		}

		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());

		if (isLoggingDebug()){
			vlogDebug("END::  TRUSubClassificationHandler.preprocess() method::");
		}
	}

	/**
	 * This method create level of sub-classification tree hierarchy.
	 *
	 * @param pContentItem ContentItem
	 * @return ContentItem
	 * @throws CartridgeHandlerException the cartridge handler exception
	 */
	public ContentItem process(ContentItem pContentItem) throws CartridgeHandlerException {

		if (isLoggingDebug()) {
			vlogDebug("Begin ::TRUSubClassificationHandler.process method: ContentItem ");
		}
		Map<String, List<BreadCrumbsVO>> subClassificationTreeMap = new HashMap<String, List<BreadCrumbsVO>>();
		ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
		if(results == null || results.getNavigation() == null) {
			return pContentItem;
		}
		Navigation navigation = results.getNavigation();
		DimensionList dimList = navigation.getDescriptorDimensions();
		if(dimList == null || dimList.isEmpty()) {
			return pContentItem;
		}
		Dimension dimension = dimList.getDimension(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME);
		if(dimension == null || dimension.getAncestors() == null) {
			return pContentItem;
		}
		int level = 0;
        String ancestorId = null;
        DimValList ancestors = dimension.getAncestors();
        if(ancestors != null) {
              level = ancestors.size();
              if(level > TRUEndecaConstants.INT_ZERO) {
                     DimVal ancestor = (DimVal) ancestors.get(level - TRUEndecaConstants.INT_ONE);
                     if(ancestor != null) {
                            ancestorId = (String) ancestor.getProperties().get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
                     }
              }
        }
		DimVal dimval = dimension.getDescriptor();
		if(dimval != null) {
			String categoryId = (String) dimval.getProperties().get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
			if(!StringUtils.isEmpty(categoryId)) {
				pContentItem.put(TRUEndecaConstants.CURRENT_CAT_ID, categoryId);
				pContentItem.put(TRUEndecaConstants.CURRENT_CAT_NAME,dimval.getName());
				ClassificationUtilKeyVO classificationKey = getCategoryUtils().getClassificationKey(categoryId, level+TRUEndecaConstants.INT_ONE, ancestorId);
				TRUClassificationVO classificationVo = getCategoryUtils().getClassificationVOValue(classificationKey);
				if(classificationVo != null) {
					createSubClassificationMap(subClassificationTreeMap, classificationVo, level+TRUEndecaConstants.INT_ONE, TRUEndecaConstants.INT_ONE);
					pContentItem.put(TRUEndecaConstants.LEVEL_TWO_CATEGORY_MAP,subClassificationTreeMap);
				}
			}
		}
		if (isLoggingDebug()){
			vlogDebug("End :: TRUSubClassificationHandler.process method: returned ContentItem ");
		}

		return pContentItem;
	}

	/**
	 * This method create the level of sub-classification tree based on current classification.
	 *
	 * @param pSubClassificationTreeMap the two level category tree map
	 * @param pClassificationVo the classification vo
	 * @param pCurrentLevel the current level
	 * @param pCount the count
	 */
	private void createSubClassificationMap(Map<String, List<BreadCrumbsVO>> pSubClassificationTreeMap , TRUClassificationVO pClassificationVo,int pCurrentLevel,int pCount){
		if (isLoggingDebug()){
			vlogDebug("Begin ::TRUSubClassificationHandler.createSubClassificationMap method");
		}
		if(ServletUtil.getCurrentRequest().getContextPath().equals(TRUEndecaConstants.REST_CONTEXT_PATH)&&
				ServletUtil.getCurrentRequest().getLocalParameter(TRUEndecaConstants.REST_DEPTH)!=null) {
			setMaxLevelofSubClassification(Integer.parseInt((String) ServletUtil.getCurrentRequest().getLocalParameter(
					TRUEndecaConstants.REST_DEPTH)));
		}
		if(pClassificationVo != null && pCount <= getMaxLevelofSubClassification()) {
			String parentClassificationId = pClassificationVo.getClaasificationId();
			List<String> childCategoryIds = pClassificationVo.getChildClassifications();
			List<BreadCrumbsVO> breadcrumbVOList = new ArrayList<BreadCrumbsVO>();

			for (String childCategoryId : childCategoryIds) {
				ClassificationUtilKeyVO childClassificationVOKey = getCategoryUtils().getClassificationKey(
						childCategoryId, pCurrentLevel+TRUEndecaConstants.INT_ONE, parentClassificationId);
				if(childClassificationVOKey == null) {
					continue;
				}
				TRUClassificationVO childClassificationVo = getCategoryUtils().getClassificationVOValue(childClassificationVOKey);
				boolean checkCategoryStatus = checkCategoryStatus(childClassificationVo);
				if(childClassificationVo != null && checkCategoryStatus) {
					BreadCrumbsVO breadcrumbVO = createBreadcrumbVO(childClassificationVo);
					if(breadcrumbVO != null && !StringUtils.isEmpty(breadcrumbVO.getCategoryURL())) {
						breadcrumbVOList.add(breadcrumbVO);
						createSubClassificationMap(pSubClassificationTreeMap, childClassificationVo, pCurrentLevel+TRUEndecaConstants.INT_ONE, pCount+TRUEndecaConstants.INT_ONE);
					}
				}
			}
			if(!StringUtils.isEmpty(parentClassificationId) && breadcrumbVOList != null && !breadcrumbVOList.isEmpty()) {
				//Begin Sorting  sub Category in corousal block
				Collections.sort(breadcrumbVOList,getCategoryDisplayOrderComparator());
				//End Sorting sub Category in corousal block
				pSubClassificationTreeMap.put(parentClassificationId, breadcrumbVOList);
			}
		}
		if (isLoggingDebug()){
			vlogDebug("End ::TRUSubClassificationHandler.createSubClassificationMap method");
		}

	}

	/**
	 * Check category status.
	 *
	 * @param pClassificationVO the classification vo
	 * @return true, if successful
	 */
	private boolean checkCategoryStatus(TRUClassificationVO pClassificationVO){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUSubClassificationHandler.checkCategoryStatus method..");
		}
		String checkCatStatus = null;
		if(pClassificationVO != null){
			 checkCatStatus = pClassificationVO.getClassificationDisplayStatus();
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUSubClassificationHandler.checkCategoryStatus method..");
		}
		if(!StringUtils.isEmpty(checkCatStatus)	&& checkCatStatus.equalsIgnoreCase(TRUEndecaConstants.NO_DISPLAY)){
			return false;
		}
		return true;
	}
	/**
	 * Create BreadCrumbsVO object.
	 *
	 * @param pClassificationVO the classification vo
	 * @return the BreadCrumbsVO
	 */
	private BreadCrumbsVO createBreadcrumbVO(TRUClassificationVO pClassificationVO) {
		if (isLoggingDebug()){
			vlogDebug("BEGIN:: TRUSubClassificationHandler.createBreadcrumbVO method");
		}
		//	String categoryImage = null;
		BreadCrumbsVO breadCrumbVO = null;
		if(pClassificationVO != null) {
			breadCrumbVO = new BreadCrumbsVO();
			String url = getCategoryUtils().getURL(pClassificationVO.getClaasificationId());

			breadCrumbVO.setCategoryId(pClassificationVO.getClaasificationId());
			breadCrumbVO.setCategoryName(pClassificationVO.getClaasificationName());
			breadCrumbVO.setParentCategories(pClassificationVO.getParentClassifications());
			breadCrumbVO.setChildCategories(pClassificationVO.getChildClassifications());
			breadCrumbVO.setCategoryURL(url);
			breadCrumbVO.setCategoryDisplayOrder(pClassificationVO.getClassificationDisplayOrder());
			breadCrumbVO.setCategoryImage(pClassificationVO.getClassificationImageUrl());
		} 
		if (isLoggingDebug()){
			vlogDebug("END:: TRUSubClassificationHandler.createBreadcrumbVO method");
		}
		return breadCrumbVO;
	}

}
