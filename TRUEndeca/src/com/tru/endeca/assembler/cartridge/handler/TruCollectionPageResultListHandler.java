package com.tru.endeca.assembler.cartridge.handler;

import atg.endeca.assembler.cartridge.handler.ResultsListHandler;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
/**
 * The Class TruCollectionPageResultListHandler.
 *
 * @author Copyright PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL.
 *         Use is subject to license terms. TRUCategoryTreeUtils Utility class
 *         used for Breadcrumbs
 * @version 1.0
 * @author PA.
 *
 */
public class TruCollectionPageResultListHandler extends ResultsListHandler{

	/*
	@Override
	public void preprocess(ResultsListConfig pCartridgeConfig) throws CartridgeHandlerException {
		
		
		if(pCartridgeConfig != null && pCartridgeConfig.getSortOption() != null) {
			pCartridgeConfig.setSortOption(pCartridgeConfig.getSortOption());
		
		}
		super.preprocess(pCartridgeConfig);
	}*/
	/**
	 * @param pCartridgeConfig - CartridgeConfig
	 * @throws CartridgeHandlerException - the CartridgeHandlerException
	 * @return resultList Object
	 *
	 */
	@Override
	public ResultsList process(ResultsListConfig pCartridgeConfig) throws CartridgeHandlerException {
		ResultsList resultsList = super.process(pCartridgeConfig);
		return resultsList;
	}
	
}
