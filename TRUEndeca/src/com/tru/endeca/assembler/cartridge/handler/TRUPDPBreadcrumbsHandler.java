/*
 * Copyright (C) 2014, Professional Access Pvt Ltd. All Rights Reserved. No use,
 * copying or distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included on all
 * copies, modifications and derivatives of this work.
 */
package com.tru.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import atg.commerce.catalog.CatalogNavHistory;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RecordDetails;
import com.endeca.infront.cartridge.RecordDetailsConfig;
import com.endeca.infront.cartridge.RecordDetailsHandler;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordDetailsMdexQuery;
import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.PropertyMap;
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class will extend OOTB RecordDetailsHandler class and will fetch the record and navigation state.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */

public class TRUPDPBreadcrumbsHandler extends RecordDetailsHandler {

	/**
	 * Holds mdexRequest property of MdexRequest.
	 */
	private MdexRequest mMdexRequest;

	/**
	 * This property hold reference for CatalogNavHistory.
	 */
	private CatalogNavHistory mCatalogNavHistory;
	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;

	/**
	 * Gets the CatalogNavHistory.
	 * 
	 * @return CatalogNavHistory
	 */
	public CatalogNavHistory getCatalogNavHistory() {
		return mCatalogNavHistory;
	}

	/**
	 * set the CatalogNavHistory.
	 * 
	 * @param pCatalogNavHistory
	 *            - CatalogNavHistory
	 */
	public void setCatalogNavHistory(CatalogNavHistory pCatalogNavHistory) {
		mCatalogNavHistory = pCatalogNavHistory;
	}


	/**
	 * Gets the dimension value cache.
	 *
	 * @return the dimension value cache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets Dimension Value Cache.
	 * @param pDimensionValueCache
	 *            the DimensionValueCache to set
	 */
	public void setDimensionValueCache(
			TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();

	}

	/**
	 * Preprocess method is used to create the Mdex request.
	 * 
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 * 
	 */
	@Override
	public void preprocess(RecordDetailsConfig pCartridgeConfig) throws CartridgeHandlerException
	{
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN:::PDPBreadcrumbsHandler.preprocess method");
		}
		NavigationState navigationState = null;
		RecordDetailsMdexQuery query = null;
		if (this.mRecordState == null && !ServletUtil.getCurrentRequest().getContextPath().equals(TRUEndecaConstants.REST_CONTEXT_PATH)) {
			throw new CartridgeHandlerException(TRUEndecaConstants.ERROR_MSG_PDP_BREADCRUMB);
		}
		navigationState = getNavigationState();
		query = new RecordDetailsMdexQuery();
		if(StringUtils.isNotBlank(ServletUtil.getCurrentRequest().getParameter(TRUEndecaConstants.RECORD_SPEC))){
			query.setRecordSpec(ServletUtil.getCurrentRequest().getParameter(TRUEndecaConstants.RECORD_SPEC));
		} else {
			query.setRecordSpec(this.mRecordState.getRecordSpec());
		}
		query.setFieldNames(combineFieldNames(navigationState, pCartridgeConfig));

		this.mMdexRequest = createMdexRequest(getFilterState(navigationState), query);

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END:::PDPBreadcrumbsHandler.preprocess method");
		}
	}

	/**
	 * Process method executes the MDEX request and returns the PDP breadcrumbs content Item.
	 * 
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @return the record details
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 * @see com.endeca.infront.cartridge.RecordDetailsHandler#process(com.endeca.infront.cartridge.RecordDetailsConfig)
	 */
	@Override
	public RecordDetails process(RecordDetailsConfig pCartridgeConfig) throws CartridgeHandlerException
	{
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN:::PDPBreadcrumbsHandler.process method");
		}

		ENEQueryResults results = null;
		AggrERec aggrERec = null;
		ERec representative = null;
		PropertyMap properties = null;
		AssocDimLocationsList dimValues = null;
		AssocDimLocations assocDimLocation = null;

		results = executeMdexRequest(this.mMdexRequest);
		getNavigationState().inform(results);

		RecordDetails model = new RecordDetails(pCartridgeConfig);
		List<CategoryVO> breadCrumbList = new ArrayList<CategoryVO>();
		if (results != null) {

			aggrERec = results.getAggrERec();
			String specification = aggrERec.getSpec();
			ERecList eRecList = aggrERec.getERecs();
			for(int i=0; i < eRecList.size(); i++) {
				ERec eRec = (ERec) eRecList.get(i);
				if(eRec != null && !StringUtils.isEmpty(specification) && specification.equals(eRec.getSpec())) {
					representative = eRec;
				}
			}
			if(representative == null){
				representative = aggrERec.getRepresentative();
			}

			properties = representative.getProperties();
			if(properties != null && !properties.isEmpty()) {
				model.put(TRUEndecaConstants.PDP_BREADCRUMBS_PRODUCTID,properties.get(TRUEndecaConstants.PRODUCT_REPOSITORYID));
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				request.setAttribute(TRUEndecaConstants.PROD_ID, properties.get(TRUEndecaConstants.PRODUCT_REPOSITORYID) );
				model.put(TRUEndecaConstants.PRODUCT_DISPLAYNAME,properties.get(
						TRUEndecaConstants.PDP_BREADCRUMBS_PRODUCT_DISPLAYNAME));
				model.put(TRUEndecaConstants.PRODUCT_TYPE,properties.get(TRUEndecaConstants.PROD_TYPE));

				model.put(TRUEndecaConstants.ONLINE_PID,properties.get(TRUEndecaConstants.ONLINE_PID));

				/*Begin: Changes for SEO*/
				model.put(TRUEndecaConstants.AGE_RANGE,properties.get(TRUEndecaConstants.AGE_RANGE));
				model.put(TRUEndecaConstants.SKUDISPLAYNAME,properties.get(TRUEndecaConstants.SKU_DISPLAYNAME));
				model.put(TRUEndecaConstants.SKULONGDESC,properties.get(TRUEndecaConstants.SKU_LONG_DESC));
				model.put(TRUEndecaConstants.SKU_ID,properties.get(TRUEndecaConstants.SKU_REPOSITORY_ID));
				/*End: Changes for SEO*/
			}

			dimValues = representative.getDimValues();
			assocDimLocation = dimValues.getAssocDimLocations(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME);
			if(assocDimLocation != null && !assocDimLocation.isEmpty()) {
				breadCrumbList = getPDPCategoryBreadCrumbs(assocDimLocation);
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("The value of the breadCrumbList is :{0}",breadCrumbList);
		}
		if(breadCrumbList != null && !breadCrumbList.isEmpty()) {
			model.put(TRUEndecaConstants.PDP_BREADCRUMB_LIST, breadCrumbList);
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END:::PDPBreadcrumbsHandler.process method");
		}
		return model;
	}

	/**
	 * Creates the CategoryVO object from the DimVal object.
	 * 
	 * @param pAssocDimLocation
	 *            the AssocDimLocations Object
	 * @return categoryVO
	 * 				- the categoryVO object
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<CategoryVO> getPDPCategoryBreadCrumbs(AssocDimLocations pAssocDimLocation) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN:::PDPBreadcrumbsHandler.getPDPCategoryBreadCrumbs method");
		}
		String pdpCategoryId = getCategoryIdForBreadcrumb(pAssocDimLocation);
		List<CategoryVO> categoryVOList = new ArrayList<CategoryVO>();
		Iterator assocDimIterator = pAssocDimLocation.iterator();
		while (assocDimIterator.hasNext()) {
			DimLocation dimLoc = (DimLocation) assocDimIterator.next();
			if(dimLoc == null || dimLoc.getDimValue() == null) {
				continue;
			}
			DimValList ancestorDimValList = dimLoc.getAncestors();
			DimVal dimVal = dimLoc.getDimValue();
			PropertyMap propertiesMap = dimVal.getProperties();
			if(propertiesMap == null || propertiesMap.isEmpty()) {
				continue;
			}
	String categoryId = (String) propertiesMap.get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
			
	if(StringUtils.isEmpty(pdpCategoryId) || categoryId.equals(pdpCategoryId)) {
				if(ancestorDimValList != null) {
					for(DimVal ancestorDimVal : (List<DimVal>)ancestorDimValList) {
						if (isLoggingDebug()) {
							AssemblerTools.getApplicationLogging().vlogDebug("Adding an Ancestor to the CategoryVOList");
						}
						categoryVOList.add(createCategoryVO(ancestorDimVal));
					}
				}
				if (isLoggingDebug()) {
					AssemblerTools.getApplicationLogging().vlogDebug("Adding a DimVal to the CategoryVOList");
				}
				categoryVOList.add(createCategoryVO(dimVal));
				break;
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END:::PDPBreadcrumbsHandler.getPDPCategoryBreadCrumbs method");
		}
		return categoryVOList;
	}


	/**
	 * Gets the category id for breadcrumb.
	 *
	 * @param pAssocDimLocation the assoc dim location
	 * @return the category id for breadcrumb
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String getCategoryIdForBreadcrumb(AssocDimLocations pAssocDimLocation) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("=====BEGIN=====:::PDPBreadcrumbsHandler.getCategoryIdForBreadcrumb method");
		}
		List navHistoryCollected = getCatalogNavHistory().getNavHistory();
		String navHistory = null;
		if(navHistoryCollected != null && !navHistoryCollected.isEmpty()) {
			navHistory = (String) navHistoryCollected.get(TRUEndecaConstants.CONSTANT_ZERO);
		}
		if(StringUtils.isEmpty(navHistory)) {
			return null;
		}

		if(pAssocDimLocation == null || pAssocDimLocation.size() <= TRUEndecaConstants.CONSTANT_ONE) {
			return null;
		}
		Iterator assocDimIterator = pAssocDimLocation.iterator();
		while (assocDimIterator.hasNext()) {
			DimLocation dimLoc = (DimLocation) assocDimIterator.next();
			if(dimLoc == null || dimLoc.getDimValue() == null) {
				continue;
			}
			DimVal dimVal = dimLoc.getDimValue();
			PropertyMap propertiesMap = dimVal.getProperties();
			if(propertiesMap == null) {
				continue;
			}
			String categoryId = (String) propertiesMap.get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
			DimValList ancestorDimValList = dimLoc.getAncestors();
			if(StringUtils.isEmpty(categoryId)) {
				continue;
			}

			if(categoryId.equals(navHistory)) {
				return categoryId;
			} else if(ancestorDimValList != null && !ancestorDimValList.isEmpty()) {
				for(DimVal ancestorDimVal : (List<DimVal>)ancestorDimValList) {
					PropertyMap ancestorPropertiesMap = ancestorDimVal.getProperties();
					if(ancestorPropertiesMap == null || ancestorPropertiesMap.isEmpty()) {
						continue;
					}
					String ancestorCategoryId = (String) ancestorPropertiesMap.get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
					if(!StringUtils.isEmpty(ancestorCategoryId) && ancestorCategoryId.equals(navHistory)) {
						return categoryId;
					}
				}
			}			
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("======END=======:::PDPBreadcrumbsHandler.getCategoryIdForBreadcrumb method : Returning null");
		}
		return null;
	}

	/**
	 * Creates the CategoryVO object from the DimVal object.
	 * 
	 * @param pDimVal
	 *            the DimVal Object
	 * @return categoryVO
	 * 				- the categoryVO object
	 */
	public CategoryVO createCategoryVO(DimVal pDimVal) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("BEGIN:::PDPBreadcrumbsHandler.createCategoryVO method");
		}
		CategoryVO categoryVO = new CategoryVO();
		if(pDimVal != null) {
			PropertyMap propertiesMap = pDimVal.getProperties();
			String 	catDisplayStatus=(String)propertiesMap.get(TRUEndecaConstants.DISPLAY_STATUS);
			if(StringUtils.isNotBlank(catDisplayStatus))
			{
				categoryVO.setCategoryDisplayStatus(catDisplayStatus.toUpperCase());
			}
			String catName = pDimVal.getName();
			if(!StringUtils.isEmpty(catName) && propertiesMap != null) {
				categoryVO.setCategoryName(pDimVal.getName());
				String categoryId = (String) propertiesMap.get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
				if(!StringUtils.isEmpty(categoryId)) {
					categoryVO.setCategoryId(categoryId);
				}
				DimensionValueCacheObject dimCacheObject = getDimensionValueCache().getCacheObject(categoryId);
				if(dimCacheObject != null){
					categoryVO.setCategoryURL(dimCacheObject.getUrl());
				}
				if (isLoggingDebug()) {
					AssemblerTools.getApplicationLogging().vlogDebug("The CategoryVO being added to list is : ",categoryVO);
				}
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("END:::PDPBreadcrumbsHandler.createCategoryVO method");
		}
		return categoryVO;
	}

}