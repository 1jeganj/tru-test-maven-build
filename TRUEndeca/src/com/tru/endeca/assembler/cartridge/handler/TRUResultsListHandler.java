package com.tru.endeca.assembler.cartridge.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//import java.util.Set;
import java.util.Set;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.endeca.assembler.cartridge.handler.ResultsListHandler;
//import atg.multisite.Site;
//import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
//import atg.repository.RepositoryException;
//import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.model.Record;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUContentTools;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUContentVO;
//import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;


/**
 * The Class TRUResultsListHandler.
 * @version 1.0
 * @author PA
 */
public class TRUResultsListHandler extends ResultsListHandler {

	/** Property to hold contentTools. */
	private TRUContentTools mContentTools;

	/** Property Holds TRUDimensionValueCache. */
	private TRUDimensionValueCache mDimensionValueCache;

	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/** Property Holds siteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * This property holds CatalogProperties variable catalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 *  The m tru get site type util. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/**
	 *  The m tru get endeca util. 
	 *  
	 */
	private TRUEndecaUtils mTRUEndecaUtils;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}

	/**
	 * Gets the ContentTools.
	 * 
	 * @return the mContentTools
	 */
	public TRUContentTools getContentTools() {
		return mContentTools;
	}

	/**
	 * Sets the contentTools.
	 * 
	 * @param pContentTools
	 *            the new ContentTools
	 */
	public void setContentTools(TRUContentTools pContentTools) {
		mContentTools = pContentTools;
	}

	/**
	 * Returns TRUDimensionValueCache.
	 *
	 * @return the TRUDimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}
	/**
	 * sets TRUDimensionValueCache.
	 *
	 * @param pDimensionValueCache the TRUDimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		this.mDimensionValueCache = pDimensionValueCache;
	}


	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}


	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}	


	/**
	 * Check whether the Assembler logging error is enabled.
	 *
	 * @return the assemblerApplicationConfig
	 */
	private boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();
	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}

	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	private void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}


	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}



	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}


	/**
	 * This method is used For preprocess TRUResultsListHandler.
	 * 
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 */
	@Override
	public void preprocess(ResultsListConfig pCartridgeConfig) throws CartridgeHandlerException	{

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUResultsListHandler.preprocess method:: ");
		}
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		String queryString = request.getQueryString();
		if(!StringUtils.isEmpty(queryString) && queryString.contains(TRUEndecaConstants.NO) && queryString.contains(TRUEndecaConstants.NRPP_PARAM)) {
			super.preprocess(pCartridgeConfig);
		} else {
			String channelType = (String) request.getHeader(TRUConstants.API_CHANNEL);
			boolean displayCMS=true;
			if(getEndecaConfigurations().isDisableCMSSpots() && StringUtils.isNotEmpty(channelType) && getEndecaConfigurations().getCMSSpotsDisabledChannelList().contains(channelType)){
				displayCMS=false;
			}
			if(displayCMS) {
				int noOfArticles = 0;
				String articleCMSSPOT1 = (String) pCartridgeConfig.get(TRUCommerceConstants.CMSSPOT1);
				String articleCMSSPOT2 = (String) pCartridgeConfig.get(TRUCommerceConstants.CMSSPOT2);
				String articleCMSSPOT3 = (String) pCartridgeConfig.get(TRUCommerceConstants.CMSSPOT3);
				String articleCMSSPOT4 = (String) pCartridgeConfig.get(TRUCommerceConstants.CMSSPOT4);
				List<String> cmsSpots = new ArrayList<String>();
				int noOfRecXM = pCartridgeConfig.getRecordsPerPage();
				if(articleCMSSPOT1 != null){
					TRUContentVO contentVO = getContentTools().fetchContent(articleCMSSPOT1);
					if(contentVO != null) {
						cmsSpots.add(contentVO.getArticleBody());
						noOfArticles++;
					}
				}
				if(articleCMSSPOT2 != null) {
					TRUContentVO contentVO = getContentTools().fetchContent(articleCMSSPOT2);
					if(contentVO != null) {
						cmsSpots.add(contentVO.getArticleBody());
						noOfArticles++;
					}
				}
				if(articleCMSSPOT3!=null) {
					TRUContentVO contentVO=getContentTools().fetchContent(articleCMSSPOT3);
					if(contentVO!=null) {
						cmsSpots.add(contentVO.getArticleBody());
						noOfArticles++;
					}
				}
				if(articleCMSSPOT4 != null) {
					TRUContentVO contentVO = getContentTools().fetchContent(articleCMSSPOT4);
					if(contentVO != null){
						cmsSpots.add(contentVO.getArticleBody());
						noOfArticles++;
					}
				}
				if(cmsSpots != null && !cmsSpots.isEmpty()) {
					pCartridgeConfig.put(TRUCommerceConstants.CMSSPOT,cmsSpots);
					pCartridgeConfig.put(TRUCommerceConstants.NO_CMS_SPOTS, noOfArticles);
					pCartridgeConfig.setRecordsPerPage(noOfRecXM-noOfArticles);
				}
			}
			if(pCartridgeConfig.getSortOption() != null && !StringUtils.isEmpty(pCartridgeConfig.getSortOption().getLabel()) && pCartridgeConfig.getSortOption().isDefault()) {
				pCartridgeConfig.put(TRUEndecaConstants.DEFAULT_SORT_OPTION, pCartridgeConfig.getSortOption().getLabel());
			}
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.preprocess method:: ");
			}
			super.preprocess(pCartridgeConfig);
		}
	}

	/**
	 * Process the Results List queries. Based on the number of results returned redirects the user to PDP page - 1
	 * record returned No Search results page - 0 results
	 * 
	 * @param pCartridgeConfig the cartridge config
	 * @return the results list
	 * @throws CartridgeHandlerException the cartridge handler exception
	 */
	@Override
	public ResultsList process(ResultsListConfig pCartridgeConfig) throws CartridgeHandlerException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUResultsListHandler.process method:: ");
		}
		DynamoHttpServletRequest currentRequest = null;
		ResultsList resultsList = null;
		resultsList = super.process(pCartridgeConfig);

		//Get Dynamo Servlet Request
		currentRequest = ServletUtil.getCurrentRequest();

		//isAjaxRequest
		boolean isAjaxRequest = isAjaxRequest(currentRequest);
		boolean isRequestRedirected = false;
		if (!isAjaxRequest) {
			try {
				isRequestRedirected = redirectRequestUrl(resultsList);
			} catch (IOException ioException) {
				if(isLoggingError()) {
					vlogError(ioException, "Error occured in Results List Handler - while redirecting");
				}
			}
		}
		if(!isRequestRedirected) {
			constructPDPUrlForProduct(resultsList);
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.process method:: ");
		}
		return resultsList;
	}

	/**
	 * redirectRequestUrl method will redirect the user to PDP
	 * or No Results List page based results returned.
	 *
	 * @param pResultsList the results list
	 * @return Boolean
	 * @throws IOException IOException
	 */
	public boolean redirectRequestUrl(ResultsList pResultsList) throws IOException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===START===:: TRUResultsListHandler.redirectRequestUrl method:: ");
		}
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
		boolean isRequestRedirected = false;
		StringBuffer redirectUrl = new StringBuffer();
		//Site based URL
		String contextPath = null;
		if (StringUtils.isEmpty(contextPath)) {
			contextPath = currentRequest.getContextPath();
		}
		boolean isSearchPage = getNavigationState().getFilterState().getSearchFilters().isEmpty();

		if(!isSearchPage) {
			String term = getNavigationState().getFilterState().getSearchFilters().get(0).getTerms();
			DimensionValueCacheObject cacheObj = getDimensionValueCache().getCacheObject(term);
			if(!term.isEmpty() && term != null && cacheObj != null && !StringUtils.isBlank(cacheObj.getUrl().toString())) {
				String categoryId = cacheObj.getRepositoryId();
				String categoryContextPath = getTRUEndecaUtils().getCategoryContextPath(categoryId);
				if(!StringUtils.isEmpty(categoryContextPath)) {
					contextPath = categoryContextPath;
					if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{
						redirectUrl.append(contextPath);
					}
					redirectUrl.append(cacheObj.getUrl().toString());
					redirectUrl.append(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS+term);
					ServletUtil.getCurrentResponse().sendRedirect(redirectUrl.toString());
					isRequestRedirected = true;
				}
			}
		}
		if (pResultsList.getRecords() != null && !isRequestRedirected &&
				!ServletUtil.getCurrentRequest().getContextPath().equals(TRUEndecaConstants.REST_CONTEXT_PATH)) {

			if (pResultsList.getRecords().isEmpty() && !isSearchPage) {
				emptyRecordRedirectUrl();
				isRequestRedirected = true;
			} else if (pResultsList.getRecords().size() == TRUEndecaConstants.INT_ONE) {
				singleRecordRedirectUrl(pResultsList);
				isRequestRedirected = true;
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.redirectRequestUrl method:: ");
		}
		return isRequestRedirected;
	}

	/**
	 * emptyRecordRedirectUrl method will redirect the user to No Results List page
	 * if the number of results returned is 0
	 * 
	 * @throws IOException IOException
	 */
	public void emptyRecordRedirectUrl() throws IOException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUResultsListHandler.emptyRecordRedirectUrl method::");
		}
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
		String contextPath = null;
		
		String currentSiteId = SiteContextManager.getCurrentSiteId();
		if(StringUtils.isNotEmpty(currentSiteId)) {
			contextPath = getTRUEndecaUtils().getSiteProductionUrl(currentSiteId);
		}
		if (StringUtils.isEmpty(contextPath)) {
			contextPath = currentRequest.getContextPath();
		}
		StringBuffer redirectUrl = new StringBuffer();
		String noSearchResultsPageUrl = getEndecaConfigurations().getNoSearchResultsPageUrl();
		if(StringUtils.isNotEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR)) {
			redirectUrl.append(getSchemeDetectionUtil().getScheme()).append(contextPath);
		}
		redirectUrl.append(noSearchResultsPageUrl);
		redirectUrl.append(TRUEndecaConstants.QUESTION_MARK_SYMBOL);
		redirectUrl.append(currentRequest.getQueryString());

		if (!StringUtils.isBlank(redirectUrl.toString())) {
			ServletUtil.getCurrentResponse().sendRedirect(redirectUrl.toString());
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("The number of results returned for Search term is 0. The Redirect Url : {0}", redirectUrl.toString());
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.emptyRecordRedirectUrl method::");
		}
	}

	/**
	 * singleRecordRedirectUrl method will redirect the user to PDP page
	 * if the number of results returned is 1
	 *
	 * @param pResultsList
	 * 			- The ResultList
	 *
	 * @throws IOException IOException
	 */
	@SuppressWarnings("unchecked")
	public void singleRecordRedirectUrl(ResultsList pResultsList)throws IOException {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUResultsListHandler.singleRecordRedirectUrl method::");
		}
		DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();

		String contextPath = null;
		if (StringUtils.isEmpty(contextPath)) {
			contextPath = currentRequest.getContextPath();
		}
		StringBuffer redirectUrl = new StringBuffer();
		// Redirect to Product Details Page
		Record record = pResultsList.getRecords().get(TRUEndecaConstants.INT_ZERO);
		String productPageName = null;
		List<String> productType = record.getAttributes().get(TRUEndecaConstants.PROD_TYPE);


		List<String> siteIds=null;
		Set<String> siteIdsSet=null;
		RepositoryItem productitem=null;
		if (productType != null && !productType.isEmpty() && productType.contains(TRUEndecaConstants.COLLECTION_PRODUCT)) {

			try {
				productitem=getCatalogTools().findProduct(record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID).toString());

				siteIdsSet=(Set<String>)productitem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
				if(siteIdsSet!=null && !siteIdsSet.isEmpty()) {
					siteIds=new ArrayList<String>(siteIdsSet);
				}
			} catch (RepositoryException repositoryException) {
				if(isLoggingError()) {
					vlogError(repositoryException, "Repository Expection :singleRecordRedirectUrl method");
				}
			}
			productPageName = getEndecaConfigurations().getCollectionPageUrl();


		} else {
			productPageName = getEndecaConfigurations().getPdpPageUrl();
			//productPageName = getEndecaConfigurations().getPdpPageUrl();
			siteIds = (List<String>)record.getAttributes().get(TRUEndecaConstants.SKU_SITEID);
		}

		if (siteIds != null && !siteIds.isEmpty()) {
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			if (siteIds.contains(currentSiteId)) {
				contextPath = getTRUEndecaUtils().getSiteProductionUrl(currentSiteId);
				if (!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR)) {
					redirectUrl.append(getSchemeDetectionUtil().getScheme()).append(contextPath);
				}
				redirectUrl.append(productPageName);

				StringBuffer recstate = new StringBuffer(TRUEndecaConstants.EMPTY_STRING);
				recstate.append(record.getDetailsAction().getRecordState());

				String querysString = ServletUtil.getCurrentRequest().getQueryParameter(TRUEndecaConstants.AB_PARAM);

				if (querysString != null) {
					recstate.append(TRUEndecaConstants.AMPERSAND);
					recstate.append(TRUEndecaConstants.AB_URL);
					recstate.append(querysString);
				}

				redirectUrl.append(recstate.toString());

			} else {
				contextPath = getTRUEndecaUtils().getSiteProductionUrl(siteIds.get(0));
				if (!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR)) {
					redirectUrl.append(getSchemeDetectionUtil().getScheme()).append(contextPath);
				}
				redirectUrl.append(productPageName);
				redirectUrl.append(record.getDetailsAction().getRecordState());
			}
		}

		boolean isSearchPage = getNavigationState().getFilterState().getSearchFilters().isEmpty();
		if(!isSearchPage) {
			String searchTerm = getNavigationState().getFilterState().getSearchFilters().get(0).getTerms();
			redirectUrl.append(TRUEndecaConstants.AMPERSAND_SEARCHKEY_EQUALS+searchTerm);
		}
		if (!StringUtils.isBlank(redirectUrl.toString())) {
			ServletUtil.getCurrentResponse().sendRedirect(redirectUrl.toString());
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("The number of results returned for Search term is 1. Redirected to PDP Url : {0}", redirectUrl.toString());
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.singleRecordRedirectUrl method::");
		}
	}

	/**
	 * This method will construct the SIte Base URL to all products in the results returned.
	 * 
	 * @param pResultsList the results list
	 */
	@SuppressWarnings({ "unchecked" })
	public void constructPDPUrlForProduct(ResultsList pResultsList) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===START===:: TRUResultsListHandler.constructPDPUrlForProduct method:: ");
		}
		String contextPath = null;
		boolean pageFlag = false;
		if(pResultsList != null && !pResultsList.isEmpty() && !pResultsList.getRecords().isEmpty()) {
			List<Record> recordResults = pResultsList.getRecords();
			String [] prodIdArray=null;
			StringBuffer skuIdArray = new StringBuffer();
			StringBuffer skuStoreEligibleArray = new StringBuffer();
			StringBuffer skuPriceArray = new StringBuffer();
			

			int count=TRUEndecaConstants.ZERO;
			DynamoHttpServletRequest  currentRequest=ServletUtil.getCurrentRequest();

			if (recordResults!=null && recordResults.size()>TRUEndecaConstants.ZERO) {
				prodIdArray=new String[recordResults.size()];
			}
			for(Record record : recordResults) {
				if (record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID) != null) {
					prodIdArray[count]=record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID).toString();
					skuIdArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID).toString());
					skuIdArray.append(TRUConstants.PIPE_STRING);

					boolean isCollectionProduct=false;
					boolean isStylized=false;
					boolean unCartable=false;
					skuPriceArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID));
					skuPriceArray.append(TRUEndecaConstants.SWATCH_COLOR_DELIMETER);
					skuPriceArray.append(record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID));
					
					skuPriceArray.append(TRUEndecaConstants.DIVIDER);
				
					if (record.getAttributes().get(TRUEndecaConstants.IS_COLLECTION_PRODUCT)!=null && TRUEndecaConstants.TRUE.equalsIgnoreCase(record.getAttributes().get(TRUEndecaConstants.IS_COLLECTION_PRODUCT).toString())) {
						isCollectionProduct=true;
					}

					if (record.getAttributes().get(TRUEndecaConstants.UN_CARTABLE)!=null && TRUEndecaConstants.ONE_STR.equals(record.getAttributes().get(TRUEndecaConstants.UN_CARTABLE).toString()) ) {
						unCartable=true;
					}
					
					if (record.getAttributes().get(TRUEndecaConstants.IS_STYLIZED)!=null && TRUEndecaConstants.TRUE.equalsIgnoreCase(record.getAttributes().get(TRUEndecaConstants.IS_STYLIZED).toString())) {
						isStylized=true;
					}

					if (!isCollectionProduct &&!isStylized && !unCartable) {
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID));
						skuStoreEligibleArray.append(TRUConstants.COLON_STRING);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SHIP_TO_ELIGIBLE));
						skuStoreEligibleArray.append(TRUConstants.DOT);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_ITEM_STORE_PICKUP));
						skuStoreEligibleArray.append(TRUConstants.DOT);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_CHANNEL_AVAILABILITY));
						skuStoreEligibleArray.append(TRUConstants.PIPE_STRING);
					}
				}

				count++;
				StringBuffer pdpUrl = new StringBuffer();
				String productPageName = null;
				List<String> productType = record.getAttributes().get(TRUEndecaConstants.PROD_TYPE);
				int  collectionskusize=0;
				if (record.getAttributes().get(TRUEndecaConstants.COLLECTION_SKU_LISTSIZE)!=null) {
					collectionskusize	= Integer.parseInt(record.getAttributes().get(TRUEndecaConstants.COLLECTION_SKU_LISTSIZE).toString());
				}

				List<String> siteIds=null;
				Set<String> siteIdsSet=null;
				RepositoryItem productitem=null;

				if(productType != null && !productType.isEmpty() && productType.contains(TRUEndecaConstants.COLLECTION_PRODUCT) && collectionskusize >TRUEndecaConstants.INT_ONE) {
					try {
						productitem=getCatalogTools().findProduct(record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID).toString());

						siteIdsSet=(Set<String>)productitem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
						if(siteIdsSet!=null && !siteIdsSet.isEmpty()) {
							siteIds=new ArrayList<String>(siteIdsSet);
						}
					} catch (RepositoryException repositoryException) {
						if(isLoggingError()) {
							vlogError(repositoryException, "Repository Expection :constructPDPUrlForProduct method");
						}
					}

					productPageName = getEndecaConfigurations().getCollectionPageUrl();

				} else {
					productPageName = getEndecaConfigurations().getPdpPageUrl();
					siteIds = (List<String>)record.getAttributes().get(TRUEndecaConstants.SKU_SITEID);
				}

				if(siteIds != null && !siteIds.isEmpty()) {

					String currentSiteId = SiteContextManager.getCurrentSiteId();
					if(siteIds.contains(currentSiteId)) {
						contextPath = getTRUEndecaUtils().getSiteProductionUrl(currentSiteId);
						if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{
							pdpUrl.append(contextPath);
						}
						pdpUrl.append(productPageName);
						pdpUrl.append(record.getDetailsAction().getRecordState());

					} else {
						contextPath = getTRUEndecaUtils().getSiteProductionUrl(siteIds.get(0));
						if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{
							pdpUrl.append(contextPath);
						}
						pdpUrl.append(productPageName);
						pdpUrl.append(record.getDetailsAction().getRecordState());

					}

					if(getNavigationState().toString().contains(TRUEndecaConstants.CATEGORY_ID)) {
						pageFlag = true;

					}
					if(currentRequest.getRequestURI().contains(TRUEndecaConstants.SUBCAT) || currentRequest.getRequestURI().contains(TRUEndecaConstants.FAMILY) || (pageFlag == true && currentRequest.getRequestURI().contains(TRUEndecaConstants.ASSEMBLER)))	{
						pdpUrl.append(TRUEndecaConstants.AMPERSAND_CAT_EQUAL_ONE);
					}
				}
				if(pdpUrl != null && pdpUrl.length() > 0) {
					record.getDetailsAction().setRecordState(pdpUrl.toString());
				}
			}
			if (prodIdArray !=null && prodIdArray.length>0)	{
				pResultsList.put(TRUEndecaConstants.PROD_ID_ARRAY, prodIdArray);
			}

			if (skuIdArray !=null) {
				pResultsList.put(TRUEndecaConstants.SKU_ID_ARRAY, skuIdArray.toString());
			}
			if(skuStoreEligibleArray != null) {
				pResultsList.put(TRUEndecaConstants.SKU_STORE_ELIGIBLE_ARRAY, skuStoreEligibleArray.toString());
			}
			
			if(skuPriceArray != null) {
				pResultsList.put(TRUEndecaConstants.SKU_PRICE_ARRAY, skuPriceArray.toString());
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUResultsListHandler.constructPDPUrlForProduct method:: ");
		}
	}

	/**
	 * This method is used to check request is ajax request or not.
	 * 
	 * @param pRequest - DynamoHttpServletRequest
	 * @return boolean
	 */
	public static boolean isAjaxRequest(DynamoHttpServletRequest pRequest) {
		String acceptHeader = pRequest.getHeader(TRUEndecaConstants.REQUEST_HEADER_ACCEPT);
		boolean ajax = TRUEndecaConstants.XML_HTTP_REQUEST .equals(pRequest.getHeader(TRUEndecaConstants.X_REQUESTED_WITH));
		if (acceptHeader == null) {
			return false;
		}
		return ajax;
	}

}
