package com.tru.endeca.assembler.cartridge.handler;

import java.util.List;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RecordSpotlight;
import com.endeca.infront.cartridge.RecordSpotlightConfig;
import com.endeca.infront.cartridge.RecordSpotlightHandler;
import com.endeca.infront.cartridge.model.Record;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * The Class TRURecordSpotlightHandler.
 * @version 1.0
 * @author PA
 */
public class TRURecordSpotlightHandler extends RecordSpotlightHandler {

	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	private void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}

	/**
	 *  The m tru get site type util. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;

	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}

	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}	

	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}

	/** Property Holds siteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}


	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}

	/**
	 * This method is used For process TRURecordSpotlightHandler.
	 *
	 * @param pCartridgeConfig            the cartridge config
	 * @return the record spotlight
	 * @throws CartridgeHandlerException             the cartridge handler exception
	 */
	@SuppressWarnings("unchecked")
	public RecordSpotlight process(RecordSpotlightConfig pCartridgeConfig)
			throws CartridgeHandlerException {

		RecordSpotlight spotlight=null;
		spotlight =super.process(pCartridgeConfig);
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===START===:: TRURecordSpotlightHandler.process method:: ");
		}
		String contextPath = null;
		if(spotlight != null && !spotlight.isEmpty() && !spotlight.getRecords().isEmpty()) {
			List<Record> recordResults = spotlight.getRecords();
			StringBuffer skuIdArray = new StringBuffer();
			StringBuffer skuStoreEligibleArray = new StringBuffer();
			StringBuffer skuPriceArray = new StringBuffer();
			for(Record record : recordResults) {
				if (record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID)!=null)	{
					skuIdArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID).toString());
					skuIdArray.append(TRUConstants.PIPE_STRING);
					
					boolean isCollectionProduct=false;
					boolean isStylized=false;
					boolean unCartable=false;
					skuPriceArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID));
					skuPriceArray.append(TRUConstants.COLON_STRING);
					skuPriceArray.append(record.getAttributes().get(TRUEndecaConstants.PRODUCT_REPOSITORYID));
					skuPriceArray.append(TRUConstants.PIPE_STRING);
					
					if (record.getAttributes().get(TRUEndecaConstants.IS_COLLECTION_PRODUCT)!=null && TRUEndecaConstants.TRUE.equalsIgnoreCase(record.getAttributes().get(TRUEndecaConstants.IS_COLLECTION_PRODUCT).toString())) {
						isCollectionProduct=true;
					}

					if (record.getAttributes().get(TRUEndecaConstants.UN_CARTABLE)!=null && TRUEndecaConstants.ONE_STR.equals(record.getAttributes().get(TRUEndecaConstants.UN_CARTABLE).toString()) ) {
						unCartable=true;
					}
					
					if (record.getAttributes().get(TRUEndecaConstants.IS_STYLIZED)!=null && TRUEndecaConstants.TRUE.equalsIgnoreCase(record.getAttributes().get(TRUEndecaConstants.IS_STYLIZED).toString())) {
						isStylized=true;
					}

					if (!isCollectionProduct &&!isStylized && !unCartable) {
						
						
						

						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_REPOSITORY_ID));
						skuStoreEligibleArray.append(TRUConstants.COLON_STRING);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SHIP_TO_ELIGIBLE));
						skuStoreEligibleArray.append(TRUConstants.DOT);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_ITEM_STORE_PICKUP));
						skuStoreEligibleArray.append(TRUConstants.DOT);
						skuStoreEligibleArray.append(record.getAttributes().get(TRUEndecaConstants.SKU_CHANNEL_AVAILABILITY));
						skuStoreEligibleArray.append(TRUConstants.PIPE_STRING);
					
					}
				}
				StringBuffer pdpUrl = new StringBuffer();
				String productPageName = null;
				List<String> productType = record.getAttributes().get(TRUEndecaConstants.PROD_TYPE);
				int  collectionskusize=0;
				if (record.getAttributes().get(TRUEndecaConstants.COLLECTION_SKU_LISTSIZE) != null) {
					collectionskusize	= Integer.parseInt(record.getAttributes().get(TRUEndecaConstants.COLLECTION_SKU_LISTSIZE).toString());
				}

				if(productType != null && !productType.isEmpty() && productType.contains(TRUEndecaConstants.COLLECTION_PRODUCT  )&& collectionskusize>TRUEndecaConstants.INT_ONE) {
					productPageName = getEndecaConfigurations().getCollectionPageUrl();
				} else {
					productPageName = getEndecaConfigurations().getPdpPageUrl();
				}
				List<String> siteIds = record.getAttributes().get(TRUEndecaConstants.SKU_SITEID);
				if(siteIds != null && !siteIds.isEmpty()) {
					String currentSiteId = SiteContextManager.getCurrentSiteId();
					if(siteIds.contains(currentSiteId)) {
						contextPath = getSiteProductionUrl(siteIds.get(0));
						if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{		
							pdpUrl.append(getSchemeDetectionUtil().getScheme());
							pdpUrl.append(contextPath);
						}
						pdpUrl.append(productPageName);
						pdpUrl.append(record.getDetailsAction().getRecordState());
					} else {
						contextPath = getSiteProductionUrl(siteIds.get(0));
						if(!StringUtils.isEmpty(contextPath) && !contextPath.equals(TRUEndecaConstants.PATH_SEPERATOR))	{
							pdpUrl.append(getSchemeDetectionUtil().getScheme());
							pdpUrl.append(contextPath);
						}
						pdpUrl.append(productPageName);
						pdpUrl.append(record.getDetailsAction().getRecordState());
					}
				}
				if(pdpUrl != null && pdpUrl.length() > 0) {
					record.getDetailsAction().setRecordState(pdpUrl.toString());
				}
			}
			if (skuIdArray !=null )	{
				spotlight.put(TRUEndecaConstants.SKU_ID_ARRAY, skuIdArray.toString());
			}

			if(skuStoreEligibleArray != null) {
				spotlight.put(TRUEndecaConstants.SKU_STORE_ELIGIBLE_ARRAY, skuStoreEligibleArray.toString());
			}
			
			if(skuPriceArray != null) {
				spotlight.put(TRUEndecaConstants.SKU_PRICE_ARRAY, skuPriceArray.toString());
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRURecordSpotlightHandler.process method:: ");
		}
		return spotlight;
	}

	/**
	 * This method will return the ContextPath from the Site production url.
	 * 
	 * @param pSiteId - SIte Id
	 * @return String siteProductUrl
	 */
	public String getSiteProductionUrl(String pSiteId) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===START===:: TRUResultsListHandler.getSiteProductionUrl method:: ");
		}
		String contextPath = null;
		Site site = null;
		if(!StringUtils.isEmpty(pSiteId)) {
			try {
				site = getSiteContextManager().getSite(pSiteId);
				if(site != null) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, site);
					if(truSiteVO != null ) {
						contextPath = truSiteVO.getSiteURL();
					}
				}
			} catch (SiteContextException siteContextException) {
				vlogError(siteContextException, "Site Context Exception occured in TRUResultsListHandler.getSiteProductionUrl");
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUResultsListHandler.getSiteProductionUrl method:: Site Production Url = {0}",contextPath);
		}
		return contextPath;
	}

}
