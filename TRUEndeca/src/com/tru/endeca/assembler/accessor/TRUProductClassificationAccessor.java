package com.tru.endeca.assembler.accessor;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.tru.endeca.constants.TRUEndecaConstants;



/**
 * This class is used to index the parent classification.
 * 
 *  @version 1.0
 *  @author Professional Access
 */
public class TRUProductClassificationAccessor extends PropertyAccessorImpl{
	
	/** Field hold name. */
	private String mName;
	
	/** Field hold parent classifications. */
	private String mParentClassifications;
	
	/**
	 * This method used to get the all Parent Classification name, set at product level. 
	 * 
	 * @param pContext
	 *            - Context
	 * @param pItem
	 *            - RepositoryItem
	 * @param pPropertyName
	 *            - String
	 * @param pType
	 *            - PropertyTypeEnum
	 * @return classificationHierarchy - List of name of Parent Classification.
	 * 
	 */
	 @SuppressWarnings("unchecked")
	public Object getMetaPropertyValue(Context pContext, RepositoryItem pItem,
			String pPropertyName, PropertyTypeEnum pType) {
		 if (AssemblerTools.getApplicationLogging().isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug(
						"Begin :: TRUProductClassificationAccessor.getMetaPropertyValue method: RepositoryItem :{0}",pItem);
		}
		if ((pItem == null) || (StringUtils.isEmpty(pPropertyName))){
			return null;
		}
		
		List<String> classificationHierarchy = new ArrayList<String>();

		classificationHierarchy.add((String) pItem.getPropertyValue(getName()));

		List<RepositoryItem> subClassifications = (List<RepositoryItem>) pItem
				.getPropertyValue(getParentClassifications());
		while (!subClassifications.isEmpty()) {
			classificationHierarchy.add((String) subClassifications.get(TRUEndecaConstants.CONSTANT_ZERO)
					.getPropertyValue(getName()));
			subClassifications = (List<RepositoryItem>) subClassifications.get(TRUEndecaConstants.CONSTANT_ZERO)
					.getPropertyValue(getParentClassifications());
		}
		 if (AssemblerTools.getApplicationLogging().isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug(
						"End :: TRUProductClassificationAccessor.getMetaPropertyValue method: Classification Hierarchy Name :{0}",classificationHierarchy);
		}
		return classificationHierarchy;
	}

	 /**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 *
	 * @param pName the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the parent classifications.
	 *
	 * @return the parent classifications
	 */
	public String getParentClassifications() {
		return mParentClassifications;
	}

	/**
	 * Sets the parent classifications.
	 *
	 * @param pParentClassifications the new parent classifications
	 */
	public void setParentClassifications(String pParentClassifications) {
		this.mParentClassifications = pParentClassifications;
	}
	 
}
