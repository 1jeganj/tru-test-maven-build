package com.tru.endeca.utils;

import java.util.Comparator;

import com.tru.commerce.catalog.vo.CategoryVO;

/**
 * Class SortByDisplayOrderComparator.
 * @author PA
 * @version 1.0
 *
 */
public class SortByDisplayOrderComparator  implements Comparator<Object>{
	
	/**
	 * Compare the category sort order.
	 * @param pArg0 -  Object
	 * @param pArg1 -  Object
	 * @return returns int value
	 */
	public int compare(Object pArg0, Object pArg1) {
		CategoryVO categoryVo1 = (CategoryVO) pArg0;
		CategoryVO categoryVo2 = (CategoryVO) pArg1;
		Integer displayOrder1 = categoryVo1.getCategoryDisplayOrder();
		Integer displayOrder2 = categoryVo2.getCategoryDisplayOrder();

		return displayOrder1.compareTo(displayOrder2);

	}

}

