package com.tru.endeca.utils;

import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;

/**
 * Holds the Endeca Related Configurations.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUEndecaConfigurations extends GenericService {
	/**
	 * Property to mSkuType.
	 */
	private List<String> mSkuType;
	
	/**
	 * Property to mSponsoredSticker.
	 */
	private String mSponsoredSticker;
	
	/**
	 * Property to mShopByPageUrl.
	 */
	private String mShopByPageUrl;
	
	/**
	 * Property to mExclusiveSticker.
	 */
	private String mExclusiveSticker;
	
	/**
	 * Property to mNoOfGFRecentsearches.
	 */
	private int mNoOfGFRecentsearches;
	
	/**
	 * Property to mGender.
	 */
	private String mGender;
	
	/**
	 * Property to mSortPrice.
	 */
	private String mSortPrice;
	
	/**
	 * Property to mAgeSort.
	 */
	private String mAgeSort;
	
	/**  The giftFinder page. */
	private String mGiftFinder;
	
	/**  The header page. */
	private String mHeaderPage;
	
	/**  The footer page. */
	private String mFooterPage;
	

	/** The category image key. */
	private String mCategoryImageKey;
	
	/** The banner image key. */
	private String mBannerImageKey;
	
	/** The promo image key. */
	private String mPromoImageKey;
	
	/** The SeoTextKey. */
	private String mSeoTextKey;
	
	/**
	 * This property hold reference for MaximumCategoryLevelForParametricReport.
	 */
	private int mMaximumCategoryLevelForParametricReport;

	/**
	 * Property to Hold categorypageNames.
	 */
	private Map<String, String> mCategoryPageNames;
	/**
	 * Property to Hold ValidQueryParamsForEndecaPages.
	 */
	private Map<String, String> mValidQueryParamsForEndecaPages;
	
	/**
	 * Property to Hold mHomePage.
	 */
	private String mHomePage;
	/**
	 * Property to Hold flatCategoryDimensionNames.
	 */
	private List<String> mFlatCategoryDimensionNames;
	
	/**
	 *  Holds the site Enabled Property Name.
	 */
	private String mSiteEnabledPropertyName;
	
	/**
	 *  Holds the Endeca Search Interface Property Name.
	 */
	private String mEndecaSearchInterfacePropertyName;
	
	/**
	 *  Holds the endeca Indexable Property Name.
	 */
	private String mEndecaIndexablePropertyName;
	
	/**
	 *  Holds the default PriceList Propert yName.
	 */
	private String mDefaultPriceListPropertyName;
	
	/**
	 *  Holds the endeca User Segment Property Name.
	 */
	private String mEndecaUserSegmentPropertyName;
	
	/**
	 *  Holds the Dynamic Attributes Property Name.
	 */
	private String mDynamicAttributesPropertyName;
	
	/**
	 *  Holds the Default Catalog Property Name.
	 */
	private String mDefaultCatalogPropertyName;
	
	/**
	 * This property hold reference for mEnableRedirectionToErrorPage.
	 */
	private boolean mEnableRedirectionToErrorPage;
	
	/**
	 * This property hold reference for mEnableCacheFlushForPromoteContent.
	 */
	private boolean mEnableCacheFlushForPromoteContent;
	
	/**
	 * This property hold reference for mEnableCacheForPDPPage.
	 */
	private boolean mEnableCacheForPDPPage;

	/**
	 * Property to Hold page.
	 */
	private Map<String, String> mPage;

	/**
	 * Property to DimensionName page.
	 */
	private Map<String, String> mDimensionName;

	/**
	 * Property to Hold CacheAllowedPages.
	 */
	private List<String> mCacheAllowedPages;

	/**
	 * Property to Hold urlParameterRemoveList.
	 */
	private List<String> mUrlParameterRemoveList;
	
	/**
	 * Property to Hold excludedDynamicAttributesList.
	 */
	private List<String> mExcludedDynamicAttributesList;
	
	/**
	 * Property to Hold ValidEndecaUrlWithoutQueryParams.
	 */
	private List<String> mValidEndecaUrlWithoutQueryParams;

	/**
	 * Property to Hold CacheKeyByPageName.
	 */
	private List<String> mCacheKeyByPageName;

	/**
	 * Holds the price List Locale Property Name.
	 */
	private String mPriceListLocalePropertyName;

	/**
	 * Holds the price List Property Name.
	 */
	private String mPriceListPropertyName;

	/**
	 * Property to Hold cacheKeyByNavId.
	 */
	private List<String> mCacheKeyByNavId;
	
	/**
	 * Property to Hold cacheKeyByProductId.
	 */
	private List<String> mCacheKeyByProductId;

	/**
	 * Property to Hold searchPageUrl.
	 */
	private String mSearchPageUrl;

	/**
	 * Property to Hold pdpPageUrl.
	 */
	private String mPdpPageUrl;
	
	/**
	 * Property to Hold collectionPageUrl.
	 */
	private String mCollectionPageUrl;
	
	/**
	 * Property to Hold categoryPageUrl.
	 */
	private List<String> mCategoryPageUrl;

	/**
	 * Property to Hold salePricePropertyName.
	 */
	private String mSalePricePropertyName;

	/**
	 * Property to Hold listPricePropertyName.
	 */
	private String mListPricePropertyName;
	/**
	 * Property to Hold LocationIdConstantList.
	 */
	private List<String> mLocationIdConstantList;
	
	/**
	 * Property to Hold skuImagePropertyName.
	 */
	private String mSkuImagePropertyName;

	/**
	 * Holds the breadcrumb for rclp.
	 */
	private String mBreadcrumbForRCLP;

	/**
	 * Holds the prod category.
	 */
	private String mProdCategory;

	/**
	 * Holds the cat repository id.
	 */
	private String mCatRepositoryID;

	/**
	 * Property to Hold noSearchResultsPageUrl.
	 */
	private String mNoSearchResultsPageUrl;

	/**
	 * Property to Hold splitString.
	 */
	private String mSplitString;

	/**
	 * Holds the plp page url.
	 */
	private String mPlpPageUrl;

	/**
	 * Property to Hold CategoryRefinementMenu.
	 */
	private String mCatRefinementMenu;

	/**
	 * Property to Hold PriceSliderIntervalRange.
	 */
	private int mPriceSliderIntervalRange;

	/**
	 * Holds the page Size configuration.
	 */
	private int[] mPageSizeConfiguration;

	/**
	 * Property to Hold GuidedNavigation.
	 */
	private String mFacetNavigation;

	/**
	 * Property to Hold Sclp Url.
	 */
	private String mSclpPageUrl;

	/**
	 * Property to Hold Rclp PageUrl.
	 */
	private String mRclpPageUrl;
	
	/**
	 * Property to Hold Error PageUrl.
	 */
	private String mErrorPageUrl;

	/**
	 * Property to Maximum Pages Brand Index.
	 */
	private int mMaximumPagesBrandIndex;

	/**
	 * Property to Hold Home Bread Url.
	 */
	private String mHomeBread;

	/**
	 * Property to Hold Check for Child Categories.
	 */
	private boolean mCheckForChildCategories;
	
	/**
	 * Property to Hold enable Overridden Dimension Cache Refresh.
	 */
	private boolean mEnableOverriddenDimensionCacheRefresh;

	/**
	 * Property to Hold use Ajax Url For Compare Back Button.
	 */
	private boolean mUseAjaxUrlForCompareBackButton;
	
	/**
	 * Property to Hold use add Navigation Filters For Search.
	 */
	private boolean mAddNavigationFiltersForSearch;
	
	/**
	 * Property to Hold Format SEO PDP Url.
	 */
	private boolean mFormatSEOPDPUrl;
	
	/**
	 * Property to Hold Enable Site Specific Keyword Redirect.
	 */
	private boolean mEnableSiteSpecificKeywordRedirect;

	/**
	 * Property to Enable Endeca content cache.
	 */
	private boolean mEnableEndecaContentCache;

	/**
	 * Holds the allowed types for quick view.
	 */
	private List<String> mAllowedTypesForQuickView;

	/**
	 * Property for excludePropertyOrDimensionDuringIndexing.
	 */
	private List<String> mExcludePropertyOrDimensionDuringIndexing;

	/**
	 * Property for configuring records per page for view all.
	 */
	private int mRecordsPerPageForViewAll;

	/**
	 * Holds the prod brand.
	 */
	private String mProdBrand;
	
	/**
	 * Holds the prod character.
	 */
	private String mProdCharacter;

	/**
	 * Holds the Default SEO PDP Locale Url.
	 */
	private String mDefaultSEOPDPLocaleUrl;

	/**
	 * Holds the no color no size product type.
	 */
	private String mNoColorNoSizeProductType;
	
	/**
	 *  Holds the advertisement Copy Type Property Name.
	 */
	private String mAdvertisementCopyType;
	/**
	 *  Holds the advertisement Copy Dsc Property Name.
	 */
	private String mAdvertisementCopyDsc;
	
	/**
	 * Holds the SoftwareCompatibility.
	 */
	private String mSoftComp;
	
	/**
	 * Holds the bnbSiteId.
	 */
	private String mBnbSiteId;

	/**
	 * Property to Hold pdpPageUrl.
	 */
	private String mTypeAheadPageUrl;

	/**
	 * Property to Hold pdpPageUrl.
	 */
	private String mMinAutoSuggestInputLength;
	

	/**
	 * Property to Hold mFeatureCategoriesPropertyName.
	 */
	private String mFeatureCategoriesPropertyName;
	
	/**
	 * Property to Hold mSecurityLevelDimensionName.
	 */
	private String mSecurityLevelDimensionName;
	
	
	/**
	 * Property to Hold mHighSecurityDimensionValue.
	 */
	private String mHighSecurityDimensionValue;

	/**
	 * Property to Hold mHighSecurityProducts.
	 */
	private String mHighSecurityProducts;

	
	
	/**
	 * Property to Hold mIdPropertyName.
	 */
	private String mIdPropertyName;
	/**
	 * Property to Hold mDisplayNamePropertyName.
	 */
	private String mDisplayNamePropertyName;
	/**
	 * Property to Hold mThumbnailImagePropertyName.
	 */
	private String mThumbnailImagePropertyName;
	/**
	 * Property to Hold mPathPropertyName.
	 */
	private String mPathPropertyName;
	
	/**
	 * Property to Hold mUrlPropertyName.
	 */
	private String mUrlPropertyName;
	
	/**
	 * Property to Hold numberOfProductsPerRowInLocid.
	 */
	private String mNumberOfProductsPerRowInLocid;
	
	/**
	 * Property to Hold mHomePage.
	 */
	private String mSdSiteType;
	/**
	 * Property to Hold mLaserBusinessCheckLabel.
	 */
	private String mLaserBusinessCheckLabel;
	
	/**
	 * Property to Hold Dimension Name For All Level Categories.
	 */
	private String mDimNameForAllLevelCategories;
	
	/**
	 * Property to Hold Dimension Name For All Level Categories.
	 */
	private String mNonNavigableCategory;
	
	/**
	 * Property to Hold dimensionNamesForDisplay.
	 */
	private Map<String, String> mDimensionNamesForDisplay;
	
	/**
	 * Property to Hold dimensionNamesForDisplay.
	 */
	private Map<String, String> mDimNamesForLeftNavCss;
	
	/**
	 * Property to Hold headerFooterPage.
	 */
	private String mHeaderFooterPage;
	
	/**
	 * Property to Hold ParentCategory PropertyName.
	 */
	private String  mParentCategory;
	/**
	 * Property to Hold ClassificationItemDescriptorName.
	 */
	private String  mClassificationItemDescriptorName;
	
	/**
	 * Property to Hold SiteItemDescriptorName.
	 */
	private String	mSiteItemDescriptorName;
	
	/**
	 * Property to Hold SiteEnabled PropertyName.
	 */
	private String	mSiteEnabled;
	
	/**
	 * Property to Hold SiteDefaultCatalog PropertyName.
	 */
	private String	mSiteDefaultCatalog;
	
	/**
	 * Property to Hold SiteRootCategory PropertyName.
	 */
	private String	mSiteRootCategory;
	
	/**
	 * Property to Hold ClassificationRootParentCategory PropertyName.
	 */
	private String	mClassificationRootParentCategory;
	
	/**
	 * Property to Hold ClassificationIsDeleted PropertyName.
	 */
	private String	mClassificationIsDeleted;
	
	/**
	 * Property to Hold ClassificationDisplayStatus PropertyName.
	 */
	private String	mClassificationDisplayStatus;
	
	/**
	 * Property to Hold ClassificationName PropertyName.
	 */
	private String	mClassificationName;
	
	/**
	 * Property to Hold mChildClassifications PropertyName.
	 */
	private String	mChildClassifications;
	
	/**
	 * Property to Hold ParentClassifications PropertyName.
	 */
	private String	mParentClassifications;
	
	/**
	 * Property to Hold ClassificationUserTypeId PropertyName.
	 */
	private String	mClassificationUserTypeId;
	
	/**
	 * Property to Hold ClassificationRrelatedMediaContent PropertyName.
	 */
	private String	mClassificationRrelatedMediaContent;
	
	/**
	 * Property to Hold MediaName PropertyName.
	 */
	private String	mMediaName;
	
	/**
	 * Property to Hold MediaType PropertyName.
	 */
	private String	mMediaType;
	
	/**
	 * Property to Hold MediaUrl PropertyName.
	 */
	private String	mMediaUrl;
	
	/**
	 * Property to Hold MediaHtmlContent PropertyName.
	 */
	private String	mMediaHtmlContent;
	/**
	 * Property to Hold mClassificationLongDescription PropertyName.
	 */
	private String	mClassificationLongDescription;
	/**
	 * Property to Hold mType PropertyName.
	 */
	private String mType;
	/**
	 * Property to Hold mTruSiteMapTargeter PropertyName.
	 */
	private String mTruSiteMapTargeter;
	/**
	 * Property to Hold mBruSiteMapTargeter PropertyName.
	 */
	private String mBruSiteMapTargeter;
	/**
	 * Property to Hold mSiteMapToolTargeter PropertyName.
	 */
	private String mSiteMapToolTargeter;
	/**
	 * Property to Hold ClassificationDisplayOrder PropertyName.
	 */
	private String	mClassificationDisplayOrder;
	
	/** property to hold mSiteIdsPropertyName property. */
	private String mSiteIdsPropertyName;
	
	/** property to hold mRegistryClassificationItemName property. */
	private String mRegistryClassificationItemName;
	
	/** property to hold mCrossReferencePropertyName property. */
	private String mCrossReferencePropertyName;
	
	/**
	 * Property to Hold mGridView1divClassMapping.
	 */
	private Map<String, String> mGridView1divClassMapping;
	
	
	/**
	 * Property to Hold mGridView2divClassMapping.
	 */
	private Map<String, String> mGridView2divClassMapping;
	
	/**
	 * Gets the shopByPageUrl.
	 * @return the shopByPageUrl
	 */
	public String getShopByPageUrl() {
		return mShopByPageUrl;
	}

	/**
	 * Sets the shopByPageUrl.
	 * @param pShopByPageUrl the shopByPageUrl to set
	 */
	public void setShopByPageUrl(String pShopByPageUrl) {
		mShopByPageUrl = pShopByPageUrl;
	}

	/**
	 * Gets the Sponsored Sticker.
	 *
	 * @return the the Sponsored Sticker
	 */
	public String getSponsoredSticker() {
		return mSponsoredSticker;
	}
	
	/**
	 * Sets the Sponsored Sticker.
	 *
	 * @param pSponsoredSticker the Sponsored Sticker
	 */
	public void setSponsoredSticker(String pSponsoredSticker) {
		this.mSponsoredSticker = pSponsoredSticker;
	}
	

	
	/**
	 * Gets the Exclusive Sticker.
	 *
	 * @return the Exclusive Sticker
	 */
	public String getExclusiveSticker() {
		return mExclusiveSticker;
	}
	
	/**
	 * Sets the Exclusive Sticker.
	 *
	 * @param pExclusiveSticker the Exclusive Sticker
	 */
	public void setExclusiveSticker(String pExclusiveSticker) {
		this.mExclusiveSticker = pExclusiveSticker;
	}

	
	/**
	 * Gets the number of recent searches.
	 *
	 * @return the number of recent searches
	 */
	public int getNoOfGFRecentsearches() {
		return mNoOfGFRecentsearches;
	}
	
	/**
	 * Sets the number of recent searches.
	 *
	 * @param pNoOfGFRecentsearches the number of recent searches
	 */
	public void setNoOfGFRecentsearches(int pNoOfGFRecentsearches) {
		this.mNoOfGFRecentsearches = pNoOfGFRecentsearches;
	}
	

	
	/**
	 * Gets the Gender.
	 *
	 * @return the Gender
	 */
	public String getGender() {
		return mGender;
	}
	
	/**
	 * Sets the Gender.
	 *
	 * @param pGender the Gender
	 */
	public void setGender(String pGender) {
		this.mGender = pGender;
	}
	

	
	/**
	 * Gets the sort price.
	 *
	 * @return the sort price
	 */
	public String getSortPrice() {
		return mSortPrice;
	}
	
	/**
	 * Sets the sort price.
	 *
	 * @param pSortPrice the sort price
	 */
	public void setmSortPrice(String pSortPrice) {
		this.mSortPrice = pSortPrice;
	}
	

	
	/**
	 * Gets the Age sort.
	 *
	 * @return the Age sort
	 */
	public String getAgeSort() {
		return mAgeSort;
	}
	
	/**
	 * Sets the Age sort.
	 *
	 * @param pAgeSort the Age sort
	 */
	public void setAgeSort(String pAgeSort) {
		this.mAgeSort = pAgeSort;
	}


	
	/**
	 * Gets the giftFinder page.
	 *
	 * @return the giftFinder page
	 */
	public String getGiftFinder() {
		return mGiftFinder;
	}
	/**
	 * Sets the giftFinder page.
	 *
	 * @param pGiftFinder the new giftFinder page
	 */
	public void setGiftFinder(String pGiftFinder) {
		this.mGiftFinder = pGiftFinder;
	}


	
	/**
	 * Gets the header page.
	 *
	 * @return the header page
	 */
	public String getHeaderPage() {
		return mHeaderPage;
	}
	/**
	 * Sets the header page.
	 *
	 * @param pHeaderPage the new header page
	 */
	public void setHeaderPage(String pHeaderPage) {
		this.mHeaderPage = pHeaderPage;
	}


	
	/**
	 * Gets the footer page.
	 *
	 * @return the footer page
	 */
	public String getFooterPage() {
		return mFooterPage;
	}
	
	/**
	 * Sets the footer page.
	 *
	 * @param pFooterPage the new footer page
	 */
	public void setFooterPage(String pFooterPage) {
		this.mFooterPage = pFooterPage;
	}


	
	/**
	 * Gets the site ids property name.
	 *
	 * @return the siteIdsPropertyName
	 */
	public String getSiteIdsPropertyName() {
		return mSiteIdsPropertyName;
	}

	/**
	 * Sets the site ids property name.
	 *
	 * @param pSiteIdsPropertyName the siteIdsPropertyName to set
	 */
	public void setSiteIdsPropertyName(String pSiteIdsPropertyName) {
		mSiteIdsPropertyName = pSiteIdsPropertyName;
	}

	/**
	 * Gets the registry classification item name.
	 *
	 * @return the registryClassificationItemName
	 */
	public String getRegistryClassificationItemName() {
		return mRegistryClassificationItemName;
	}

	/**
	 * Sets the registry classification item name.
	 *
	 * @param pRegistryClassificationItemName the registryClassificationItemName to set
	 */
	public void setRegistryClassificationItemName(
			String pRegistryClassificationItemName) {
		mRegistryClassificationItemName = pRegistryClassificationItemName;
	}

	/**
	 * Gets the cross reference property name.
	 *
	 * @return the crossReferencePropertyName
	 */
	public String getCrossReferencePropertyName() {
		return mCrossReferencePropertyName;
	}

	/**
	 * Sets the cross reference property name.
	 *
	 * @param pCrossReferencePropertyName the crossReferencePropertyName to set
	 */
	public void setCrossReferencePropertyName(String pCrossReferencePropertyName) {
		mCrossReferencePropertyName = pCrossReferencePropertyName;
	}

	/**
	 * Gets the classification display order.
	 *
	 * @return the classificationDisplayOrder
	 */
	public String getClassificationDisplayOrder() {
		return mClassificationDisplayOrder;
	}
	
	/**
	 * Sets the classification display order.
	 *
	 * @param pClassificationDisplayOrder the classificationDisplayOrder to set
	 */
	public void setClassificationDisplayOrder(String pClassificationDisplayOrder) {
		mClassificationDisplayOrder = pClassificationDisplayOrder;
	}
	/**
	 * Gets the classification long description.
	 *
	 * @return the classification long description
	 */
	public String getClassificationLongDescription() {
		return mClassificationLongDescription;
	}
	
	/**
	 * Sets the classification long description.
	 *
	 * @param pClassificationLongDescription the new classification long description
	 */
	public void setClassificationLongDescription(
			String pClassificationLongDescription) {
		this.mClassificationLongDescription = pClassificationLongDescription;
	}
	/**
	 * Gets the header footer page.
	 *
	 * @return the mHeaderFooterPage
	 */
	
	public String getHeaderFooterPage() {
		return mHeaderFooterPage;
	}
	/**
	 * Sets the mHeaderFooterPage.
	 * 
	 * @param pHeaderFooterPage
	 *            the mHeaderFooterPage to set
	 */

	public void setHeaderFooterPage(String pHeaderFooterPage) {
		this.mHeaderFooterPage = pHeaderFooterPage;
	}


	
	
	/**
	 * Gets the grid view1div class mapping.
	 *
	 * @return the grid view1div class mapping
	 */
	public Map<String, String> getGridView1divClassMapping() {
		return mGridView1divClassMapping;
	}
	
	/**
	 * Sets the grid view1div class mapping.
	 *
	 * @param pGridView1divClassMapping the grid view1div class mapping
	 */
	public void setGridView1divClassMapping(
			Map<String, String> pGridView1divClassMapping) {
		this.mGridView1divClassMapping = pGridView1divClassMapping;
	}
	
	/**
	 * Gets the grid view2div class mapping.
	 *
	 * @return the grid view2div class mapping
	 */
	public Map<String, String> getGridView2divClassMapping() {
		return mGridView2divClassMapping;
	}
	
	/**
	 * Sets the grid view2div class mapping.
	 *
	 * @param pGridView2divClassMapping the grid view2div class mapping
	 */
	public void setGridView2divClassMapping(
			Map<String, String> pGridView2divClassMapping) {
		this.mGridView2divClassMapping = pGridView2divClassMapping;
	}
	
	
	
	
	
	
	
	/**
	 * Gets the dimension names for display.
	 *
	 * @return the mDimensionNamesForDisplay
	 */
	public Map<String, String> getDimensionNamesForDisplay() {
		return mDimensionNamesForDisplay;
	}

	/**
	 * Sets the dimension names for display.
	 *
	 * @param pDimensionNamesForDisplay the dimension names for display
	 */
	public void setDimensionNamesForDisplay(
			Map<String, String> pDimensionNamesForDisplay) {
		this.mDimensionNamesForDisplay = pDimensionNamesForDisplay;
	}
	
	/**
	 * Gets the dim names for left nav css.
	 *
	 * @return the mDimensionNamesForDisplay
	 */
	public Map<String, String> getDimNamesForLeftNavCss() {
		return mDimNamesForLeftNavCss;
	}

	/**
	 * Sets the dim names for left nav css.
	 *
	 * @param pDimNamesForLeftNavCss the dim names for left nav css
	 */
	public void setDimNamesForLeftNavCss(
			Map<String, String> pDimNamesForLeftNavCss) {
		this.mDimNamesForLeftNavCss = pDimNamesForLeftNavCss;
	}

	/**
	 * Returns the FeatureCategoriesPropertyName.
	 *
	 * @return the mFeatureCategoriesPropertyName
	 */
	public String getFeatureCategoriesPropertyName() {
		return mFeatureCategoriesPropertyName;
	}

	/**
	 * Sets the FeatureCategoriesPropertyName.
	 * 
	 * @param pFeatureCategoriesPropertyName
	 *            the FeatureCategoriesPropertyName to set
	 */
	public void setFeatureCategoriesPropertyName(String pFeatureCategoriesPropertyName) {
		mFeatureCategoriesPropertyName = pFeatureCategoriesPropertyName;
	}
	
	/**
	 * Returns the IdPropertyName.
	 *
	 * @return the mIdPropertyName
	 */
	public String getIdPropertyName() {
		return mIdPropertyName;
	}

	/**
	 * Sets the IdPropertyName.
	 * 
	 * @param pIdPropertyName
	 *            the IdPropertyName to set
	 */
	public void setIdPropertyName(String pIdPropertyName) {
		mIdPropertyName = pIdPropertyName;
	}
	
	/**
	 * Returns the DisplayNamePropertyName.
	 *
	 * @return the mDisplayNamePropertyName
	 */
	public String getDisplayNamePropertyName() {
		return mDisplayNamePropertyName;
	}

	/**
	 * Sets the DisplayNamePropertyName.
	 * 
	 * @param pDisplayNamePropertyName
	 *            the DisplayNamePropertyName to set
	 */
	public void setDisplayNamePropertyName(String pDisplayNamePropertyName) {
		mDisplayNamePropertyName = pDisplayNamePropertyName;
	}
	
	/**
	 * Returns the ThumbnailImagePropertyName.
	 *
	 * @return the mThumbnailImagePropertyName
	 */
	public String getThumbnailImagePropertyName() {
		return mThumbnailImagePropertyName;
	}

	/**
	 * Sets the thumbnail image property name.
	 *
	 * @param pThumbnailImagePropertyName the new thumbnail image property name
	 */
	public void setThumbnailImagePropertyName(String pThumbnailImagePropertyName) {
		mThumbnailImagePropertyName = pThumbnailImagePropertyName;
	}
	
	/**
	 * Returns the PathPropertyName.
	 *
	 * @return the mPathPropertyName
	 */
	public String getPathPropertyName() {
		return mPathPropertyName;
	}

	/**
	 * Sets the PathPropertyName.
	 * 
	 * @param pPathPropertyName
	 *            the PathPropertyName to set
	 */
	public void setPathPropertyName(String pPathPropertyName) {
		mPathPropertyName = pPathPropertyName;
	}
	
	/**
	 * Returns the UrlPropertyName.
	 *
	 * @return the mUrlPropertyName
	 */
	public String getUrlPropertyName() {
		return mUrlPropertyName;
	}

	/**
	 * Sets the UrlPropertyName.
	 * 
	 * @param pUrlPropertyName
	 *            the UrlPropertyName to set
	 */
	public void setUrlPropertyName(String pUrlPropertyName) {
		mUrlPropertyName = pUrlPropertyName;
	}
	
	/**
	 * Returns the LocationIdConstantList.
	 *
	 * @return the mLocationIdConstantList
	 */
	public List<String> getLocationIdConstantList(){
		return mLocationIdConstantList;
	}
	/**
	 * Sets the LocationIdConstantList.
	 * 
	 * @param pLocationIdConstantList
	 *            the LocationIdConstantList to set
	 */
	public void setLocationIdConstantList(List<String> pLocationIdConstantList) {
		mLocationIdConstantList = pLocationIdConstantList;
	}
	
	/**
	 * Returns the NumberOfProductsPerRowInLocid.
	 *
	 * @return the mNumberOfProductsPerRowInLocid
	 */
	public String getNumberOfProductsPerRowInLocid() {
		return mNumberOfProductsPerRowInLocid;
	}
	/**
	 * Sets the NumberOfProductsPerRowInLocid.
	 * 
	 * @param pNumberOfProductsPerRowInLocid
	 *            theNumberOfProductsPerRowInLocid to set
	 */
	public void setNumberOfProductsPerRowInLocid(String pNumberOfProductsPerRowInLocid) {
		this.mNumberOfProductsPerRowInLocid = pNumberOfProductsPerRowInLocid;
	}
	/**
	 * Returns the Maximum Pages Brand Index.
	 * 
	 * @return the MaximumPagesBrandIndex
	 */
	public int getMaximumPagesBrandIndex() {
		return mMaximumPagesBrandIndex;
	}

	/**
	 * Sets the Maximum Pages Brand Index.
	 * 
	 * @param pMaximumPagesBrandIndex
	 *            the MaximumPagesBrandIndex to set
	 */
	public void setMaximumPagesBrandIndex(int pMaximumPagesBrandIndex) {
		mMaximumPagesBrandIndex = pMaximumPagesBrandIndex;
	}

	/**
	 * Returns the Price Slider Interval Range.
	 * 
	 * @return the PriceSliderIntervalRange
	 */
	public int getPriceSliderIntervalRange() {
		return mPriceSliderIntervalRange;
	}

	/**
	 * Sets the Price Slider Interval Range.
	 * 
	 * @param pPriceSliderIntervalRange
	 *            the PriceSliderIntervalRange to set
	 */
	public void setPriceSliderIntervalRange(int pPriceSliderIntervalRange) {
		mPriceSliderIntervalRange = pPriceSliderIntervalRange;
	}

	/**
	 * Returns the page Size configuration.
	 * 
	 * @return the PageSizeConfiguration
	 */
	public int[] getPageSizeConfiguration() {
		return mPageSizeConfiguration;
	}

	/**
	 * Sets the page Size configuration.
	 * 
	 * @param pPageSizeConfiguration
	 *            the PageSizeConfiguration to set
	 */
	public void setPageSizeConfiguration(int[] pPageSizeConfiguration) {
		mPageSizeConfiguration = pPageSizeConfiguration;
	}

	/**
	 * Returns the property mPriceListLocalePropertyName.
	 * 
	 * @return the holds the PriceListLocalePropertyName
	 */
	public String getPriceListLocalePropertyName() {
		return mPriceListLocalePropertyName;
	}

	/**
	 * Sets the holds the Price List Locale Property Name.
	 * 
	 * @param pPriceListLocalePropertyName the new holds the Price List Locale Property Name
	 */
	public void setPriceListLocalePropertyName(String pPriceListLocalePropertyName) {
		mPriceListLocalePropertyName = pPriceListLocalePropertyName;
	}

	/**
	 * Returns the holds the Price List Property Name.
	 * 
	 * @return the holds the Price List Property Name
	 */
	public String getPriceListPropertyName() {
		return mPriceListPropertyName;
	}

	/**
	 * Sets the holds the Price List Property Name.
	 * 
	 * @param pPriceListPropertyName the new holds the Price List Property Name
	 */
	public void setPriceListPropertyName(String pPriceListPropertyName) {
		mPriceListPropertyName = pPriceListPropertyName;
	}

	/**
	 * Returns the holds the Default SEO PDP Locale Url.
	 * 
	 * @return the holds the Default SEO PDP Locale Url
	 */
	public String getDefaultSEOPDPLocaleUrl() {
		return mDefaultSEOPDPLocaleUrl;
	}

	/**
	 * Sets the holds the Default SEO PDP Locale Url.
	 * 
	 * @param pDefaultSEOPDPLocaleUrl the new holds the Price List Property Name
	 */
	public void setDefaultSEOPDPLocaleUrl(String pDefaultSEOPDPLocaleUrl) {
		mDefaultSEOPDPLocaleUrl = pDefaultSEOPDPLocaleUrl;
	}

	/**
	 * Returns the holds the CheckForChildCategories.
	 * 
	 * @return the CheckForChildCategories
	 */
	public boolean isCheckForChildCategories() {
		return mCheckForChildCategories;
	}

	/**
	 * Sets the CheckForChildCategories.
	 * 
	 * @param pCheckForChildCategories
	 *            the CheckForChildCategories to set
	 */
	public void setCheckForChildCategories(boolean pCheckForChildCategories) {
		mCheckForChildCategories = pCheckForChildCategories;
	}
	
	/**
	 * Returns the holds the EnableOverriddenDimensionCacheRefresh.
	 * 
	 * @return the EnableOverriddenDimensionCacheRefresh
	 */
	public boolean isEnableOverriddenDimensionCacheRefresh() {
		return mEnableOverriddenDimensionCacheRefresh;
	}

	/**
	 * Sets the EnableOverriddenDimensionCacheRefresh.
	 * 
	 * @param pEnableOverriddenDimensionCacheRefresh
	 *            the EnableOverriddenDimensionCacheRefresh to set
	 */
	public void setEnableOverriddenDimensionCacheRefresh(boolean pEnableOverriddenDimensionCacheRefresh) {
		mEnableOverriddenDimensionCacheRefresh = pEnableOverriddenDimensionCacheRefresh;
	}
	
	/**
	 * Returns the holds the addNavigationFiltersForSearch.
	 * 
	 * @return the addNavigationFiltersForSearch
	 */
	public boolean isAddNavigationFiltersForSearch() {
		return mAddNavigationFiltersForSearch;
	}

	/**
	 * Sets the AddNavigationFiltersForSearch.
	 * 
	 * @param pAddNavigationFiltersForSearch
	 *            the AddNavigationFiltersForSearch to set
	 */
	public void setAddNavigationFiltersForSearch(boolean pAddNavigationFiltersForSearch) {
		mAddNavigationFiltersForSearch = pAddNavigationFiltersForSearch;
	}
	
	/**
	 * Returns the holds the UseAjaxUrlForCompareBackButton.
	 * 
	 * @return the UseAjaxUrlForCompareBackButton
	 */
	public boolean isUseAjaxUrlForCompareBackButton() {
		return mUseAjaxUrlForCompareBackButton;
	}

	/**
	 * Sets the UseAjaxUrlForCompareBackButton.
	 * 
	 * @param pUseAjaxUrlForCompareBackButton
	 *            the UseAjaxUrlForCompareBackButton to set
	 */
	public void setUseAjaxUrlForCompareBackButton(boolean pUseAjaxUrlForCompareBackButton) {
		mUseAjaxUrlForCompareBackButton = pUseAjaxUrlForCompareBackButton;
	}
	
	/**
	 * Gets the mEnableRedirectionToErrorPage.
	 * 
	 * @return mEnableRedirectionToErrorPage
	 */
	public boolean isEnableRedirectionToErrorPage() {
		return mEnableRedirectionToErrorPage;
	}

	/**
	 * set the pEnableRedirectionToErrorPage.
	 *
	 * @param pEnableRedirectionToErrorPage the new enable redirection to error page
	 */
	public void setEnableRedirectionToErrorPage(boolean pEnableRedirectionToErrorPage) {
		mEnableRedirectionToErrorPage = pEnableRedirectionToErrorPage;
	}
	
	/**
	 * Gets the mEnableCacheFlushForPromoteContent.
	 * 
	 * @return mEnableCacheFlushForPromoteContent
	 */
	public boolean isEnableCacheFlushForPromoteContent() {
		return mEnableCacheFlushForPromoteContent;
	}

	/**
	 * set the pEnableCacheFlushForPromoteContent.
	 *
	 * @param pEnableCacheFlushForPromoteContent the Enable Cache Flush For Promote Content
	 */
	public void setEnableCacheFlushForPromoteContent(boolean pEnableCacheFlushForPromoteContent) {
		mEnableCacheFlushForPromoteContent = pEnableCacheFlushForPromoteContent;
	}
	
	/**
	 * Gets the mEnableCacheForPDPPage.
	 * 
	 * @return mEnableCacheForPDPPage
	 */
	public boolean isEnableCacheForPDPPage() {
		return mEnableCacheForPDPPage;
	}

	/**
	 * set the pEnableCacheForPDPPage.
	 *
	 * @param pEnableCacheForPDPPage the Enable Cache For PDP Page
	 */
	public void setEnableCacheForPDPPage(boolean pEnableCacheForPDPPage) {
		mEnableCacheForPDPPage = pEnableCacheForPDPPage;
	}

	/**
	 * Returns the holds the FormatSEOPDPUrl.
	 * 
	 * @return the FormatSEOPDPUrl
	 */
	public boolean isFormatSEOPDPUrl() {
		return mFormatSEOPDPUrl;
	}

	/**
	 * Sets the FormatSEOPDPUrl.
	 * 
	 * @param pFormatSEOPDPUrl
	 *            the FormatSEOPDPUrl to set
	 */
	public void setFormatSEOPDPUrl(boolean pFormatSEOPDPUrl) {
		mFormatSEOPDPUrl = pFormatSEOPDPUrl;
	}
	
	/**
	 * Returns the holds the EnableSiteSpecificKeywordRedirect.
	 * 
	 * @return the EnableSiteSpecificKeywordRedirect
	 */
	public boolean isEnableSiteSpecificKeywordRedirect() {
		return mEnableSiteSpecificKeywordRedirect;
	}

	/**
	 * Sets the EnableSiteSpecificKeywordRedirect.
	 * 
	 * @param pEnableSiteSpecificKeywordRedirect
	 *            the EnableSiteSpecificKeywordRedirect to set
	 */
	public void setEnableSiteSpecificKeywordRedirect(boolean pEnableSiteSpecificKeywordRedirect) {
		mEnableSiteSpecificKeywordRedirect = pEnableSiteSpecificKeywordRedirect;
	}

	/**
	 * Returns the Sku Image Property Name.
	 * 
	 * @return the SkuImagePropertyName
	 */
	public String getSkuImagePropertyName() {
		return mSkuImagePropertyName;
	}

	/**
	 * Sets the Sku Image Property Name.
	 * 
	 * @param pSkuImagePropertyName
	 *            the SkuImagePropertyName to set
	 */
	public void setSkuImagePropertyName(String pSkuImagePropertyName) {
		mSkuImagePropertyName = pSkuImagePropertyName;
	}

	/**
	 * Returns the rclp page url.
	 * 
	 * @return the RclpPageUrl
	 */
	public String getRclpPageUrl() {
		return mRclpPageUrl;
	}

	/**
	 * Sets the rclp page url.
	 * 
	 * @param pRclpPageUrl
	 *            the RclpPageUrl to set
	 */
	public void setRclpPageUrl(String pRclpPageUrl) {
		mRclpPageUrl = pRclpPageUrl;
	}
	
	/**
	 * Returns the Error page url.
	 * 
	 * @return the ErrorPageUrl
	 */
	public String getErrorPageUrl() {
		return mErrorPageUrl;
	}

	/**
	 * Sets the Error page url.
	 * 
	 * @param pErrorPageUrl
	 *            the ErrorPageUrl to set
	 */
	public void setErrorPageUrl(String pErrorPageUrl) {
		mErrorPageUrl = pErrorPageUrl;
	}

	/**
	 * Returns the salePrice Property Name.
	 * 
	 * @return the SalePricePropertyName
	 */
	public String getSalePricePropertyName() {
		return mSalePricePropertyName;
	}

	/**
	 * Sets the salePrice Property Name.
	 * 
	 * @param pSalePricePropertyName
	 *            the salePricePropertyName to set
	 */
	public void setsalePricePropertyName(String pSalePricePropertyName) {
		mSalePricePropertyName = pSalePricePropertyName;
	}

	/**
	 * Returns the ListPrice Property Name.
	 * 
	 * @return the ListPricePropertyName
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}

	/**
	 * Sets the ListPrice Property Name.
	 * 
	 * @param pListPricePropertyName
	 *            the listPricePropertyName to set
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		mListPricePropertyName = pListPricePropertyName;
	}

	/**
	 * Returns the plp page url.
	 * 
	 * @return the plpPageUrl
	 */
	public String getPlpPageUrl() {
		return mPlpPageUrl;
	}

	/**
	 * Sets the plp page url.
	 * 
	 * @param pPlpPageUrl
	 *            the plpPageUrl to set
	 */
	public void setPlpPageUrl(String pPlpPageUrl) {
		mPlpPageUrl = pPlpPageUrl;
	}

	/**
	 * Returns the property for No Search Results Page Url.
	 * 
	 * @return NoSearchResultsPageUrl - NoSearchResultsPageUrl
	 */
	public String getNoSearchResultsPageUrl() {
		return mNoSearchResultsPageUrl;
	}

	/**
	 * Sets the property for No Search Results Page Url.
	 * 
	 * @param pNoSearchResultsPageUrl
	 *            - No Search Results Page Url
	 */
	public void setNoSearchResultsPageUrl(String pNoSearchResultsPageUrl) {
		mNoSearchResultsPageUrl = pNoSearchResultsPageUrl;
	}

	/**
	 * Returns the property for Pdp Page Url.
	 * 
	 * @return PdpPageUrl - PdpPageUrl
	 */
	public String getPdpPageUrl() {
		return mPdpPageUrl;
	}

	/**
	 * Sets the property for Pdp Page Url.
	 * 
	 * @param pPdpPageUrl
	 *            - PdpPageUrl
	 */
	public void setPdpPageUrl(String pPdpPageUrl) {
		mPdpPageUrl = pPdpPageUrl;
	}
	
	/**
	 * Returns the property for Collection Page Url.
	 * 
	 * @return CollectionPageUrl - CollectionPageUrl
	 */
	public String getCollectionPageUrl() {
		return mCollectionPageUrl;
	}

	/**
	 * Sets the property for Collection Page Url.
	 * 
	 * @param pCollectionPageUrl
	 *            - CollectionPageUrl
	 */
	public void setCollectionPageUrl(String pCollectionPageUrl) {
		mCollectionPageUrl = pCollectionPageUrl;
	}
	
	/**
	 * Returns the property for category Page Url.
	 * 
	 * @return CategoryPageUrl - CategoryPageUrl
	 */
	public List<String> getCategoryPageUrl() {
		return mCategoryPageUrl;
	}

	/**
	 * Sets the property for Category Page Url.
	 * 
	 * @param pCategoryPageUrl
	 *            - CategoryPageUrl
	 */
	public void setCategoryPageUrl(List<String> pCategoryPageUrl) {
		mCategoryPageUrl = pCategoryPageUrl;
	}

	/**
	 * Returns the Search Page Url.
	 * 
	 * @return SearchPageUrl - SearchPageUrl
	 */
	public String getSearchPageUrl() {
		return mSearchPageUrl;
	}

	/**
	 * Sets the property for Search Page Url.
	 * 
	 * @param pSearchPageUrl
	 *            - Search Page Url
	 */
	public void setSearchPageUrl(String pSearchPageUrl) {
		mSearchPageUrl = pSearchPageUrl;
	}

	/**
	 * Returns the Category Page Names.
	 * 
	 * @return Map - CategoryPageNames
	 */
	public Map<String, String> getCategoryPageNames() {
		return mCategoryPageNames;
	}

	/**
	 * Sets the property for Category Page Names.
	 * 
	 * @param pCategoryPageNames
	 *            - Category Page Names
	 */
	public void setCategoryPageNames(Map<String, String> pCategoryPageNames) {
		mCategoryPageNames = pCategoryPageNames;
	}
	
	/**
	 * Returns the Valid Query Params For Endeca Pages.
	 * 
	 * @return Map - ValidQueryParamsForEndecaPages
	 */
	public Map<String, String> getValidQueryParamsForEndecaPages() {
		return mValidQueryParamsForEndecaPages;
	}

	/**
	 * Sets the property for ValidQueryParamsForEndecaPages.
	 * 
	 * @param pValidQueryParamsForEndecaPages
	 *            - Valid Query Params For Endeca Pages
	 */
	public void setValidQueryParamsForEndecaPages(Map<String, String> pValidQueryParamsForEndecaPages) {
		mValidQueryParamsForEndecaPages = pValidQueryParamsForEndecaPages;
	}
	
	/**
	 * Returns the flatCategoryDimensionNames.
	 * 
	 * @return Map - flatCategoryDimensionNames
	 */
	public List<String> getFlatCategoryDimensionNames() {
		return mFlatCategoryDimensionNames;
	}

	/**
	 * Sets the property for Flat Category Dimension Names.
	 * 
	 * @param pFlatCategoryDimensionNames
	 *            - Flat Category Dimension Names
	 */
	public void setFlatCategoryDimensionNames(List<String> pFlatCategoryDimensionNames) {
		mFlatCategoryDimensionNames = pFlatCategoryDimensionNames;
	}

	/**
	 * Returns the property for excludePropertyOrDimensionDuringIndexing.
	 * 
	 * @return the excludePropertyOrDimensionDuringIndexing
	 */
	public List<String> getExcludePropertyOrDimensionDuringIndexing() {
		return mExcludePropertyOrDimensionDuringIndexing;
	}

	/**
	 * Sets the property for excludePropertyOrDimensionDuringIndexing.
	 * 
	 * @param pExcludePropertyOrDimensionDuringIndexing
	 *            the excludePropertyOrDimensionDuringIndexing to set
	 */
	public void setExcludePropertyOrDimensionDuringIndexing(
			List<String> pExcludePropertyOrDimensionDuringIndexing) {
		mExcludePropertyOrDimensionDuringIndexing = pExcludePropertyOrDimensionDuringIndexing;
	}

	/**
	 * Returns the property for DimensionName page.
	 * 
	 * @return the dimensionName
	 */
	public Map<String, String> getDimensionName() {
		return mDimensionName;
	}

	/**
	 * Sets the property for DimensionName page.
	 * 
	 * @param pDimensionName
	 *            the dimensionName to set
	 */
	public void setDimensionName(Map<String, String> pDimensionName) {
		mDimensionName = pDimensionName;
	}

	/**
	 * Gets the splitString.
	 * 
	 * @return the splitString
	 */
	public String getSplitString() {
		return mSplitString;
	}

	/**
	 * Sets the splitString.
	 * 
	 * @param pSplitString
	 *            the splitString to set
	 */
	public void setSplitString(String pSplitString) {
		mSplitString = pSplitString;
	}

	/**
	 * Returns cache Allowed Pages.
	 * 
	 * @return the cacheAllowedPages
	 */
	public List<String> getCacheAllowedPages() {
		return mCacheAllowedPages;
	}

	/**
	 * Sets the cache Allowed Pages.
	 * 
	 * @param pCacheAllowedPages
	 *            the cacheAllowedPages to set
	 */
	public void setCacheAllowedPages(List<String> pCacheAllowedPages) {
		mCacheAllowedPages = pCacheAllowedPages;
	}

	/**
	 * Returns Url Parameter Remove List.
	 * 
	 * @return the UrlParameterRemoveList
	 */
	public List<String> getUrlParameterRemoveList() {
		return mUrlParameterRemoveList;
	}

	/**
	 * Sets the Url Parameter Remove List.
	 * 
	 * @param pUrlParameterRemoveList
	 *            the UrlParameterRemoveList to set
	 */
	public void setUrlParameterRemoveList(List<String> pUrlParameterRemoveList) {
		mUrlParameterRemoveList = pUrlParameterRemoveList;
	}
	
	/**
	 * Returns Valid Endeca Url Without Query Params.
	 * 
	 * @return the ValidEndecaUrlWithoutQueryParams
	 */
	public List<String> getValidEndecaUrlWithoutQueryParams() {
		return mValidEndecaUrlWithoutQueryParams;
	}

	/**
	 * Sets the Valid Endeca Url Without Query Params.
	 *
	 * @param pValidEndecaUrlWithoutQueryParams the ValidEndecaUrlWithoutQueryParams to set
	 */
	public void setValidEndecaUrlWithoutQueryParams(List<String> pValidEndecaUrlWithoutQueryParams) {
		mValidEndecaUrlWithoutQueryParams = pValidEndecaUrlWithoutQueryParams;
	}
	
	/**
	 * Returns Excluded Dynamic Attributes List.
	 * 
	 * @return the ExcludedDynamicAttributesList
	 */
	public List<String> getExcludedDynamicAttributesList() {
		return mExcludedDynamicAttributesList;
	}

	/**
	 * Sets the Excluded Dynamic Attributes List.
	 *
	 * @param pExcludedDynamicAttributesList the ExcludedDynamicAttributesList to set
	 */
	public void setExcludedDynamicAttributesList(List<String> pExcludedDynamicAttributesList) {
		mExcludedDynamicAttributesList = pExcludedDynamicAttributesList;
	}

	/**
	 * Returns cacheKeyByPageName.
	 * 
	 * @return the cacheKeyByPageName
	 */
	public List<String> getCacheKeyByPageName() {
		return mCacheKeyByPageName;
	}

	/**
	 * Sets the cacheKeyByPageName.
	 * 
	 * @param pCacheKeyByPageName
	 *            the cacheKeyByPageName to set
	 */
	public void setCacheKeyByPageName(List<String> pCacheKeyByPageName) {
		mCacheKeyByPageName = pCacheKeyByPageName;
	}

	/**
	 * Returns cacheKeyByNavId.
	 * 
	 * @return the cacheKeyByNavId
	 */
	public List<String> getCacheKeyByNavId() {
		return mCacheKeyByNavId;
	}

	/**
	 * sets cacheKeyByNavId.
	 * 
	 * @param pCacheKeyByNavId
	 *            the cacheKeyByNavId to set
	 */
	public void setCacheKeyByNavId(List<String> pCacheKeyByNavId) {
		mCacheKeyByNavId = pCacheKeyByNavId;
	}
	
	/**
	 * Returns CacheKeyByProductId.
	 * 
	 * @return the CacheKeyByProductId
	 */
	public List<String> getCacheKeyByProductId() {
		return mCacheKeyByProductId;
	}

	/**
	 * sets CacheKeyByProductId.
	 * 
	 * @param pCacheKeyByProductId
	 *            the CacheKeyByProductId to set
	 */
	public void setCacheKeyByProductId(List<String> pCacheKeyByProductId) {
		mCacheKeyByProductId = pCacheKeyByProductId;
	}

	/**
	 * Gets the page.
	 * 
	 * @return the page
	 */
	public Map<String, String> getPage() {
		return mPage;
	}

	/**
	 * Sets the page.
	 * 
	 * @param pPage
	 *            the page to set
	 */
	public void setPage(Map<String, String> pPage) {
		mPage = pPage;
	}

	/**
	 * Returns the bread crumb for Root Category Landing Page.
	 * 
	 * @return the breadcrumbForRCLP
	 */
	public String getBreadcrumbForRCLP() {
		return mBreadcrumbForRCLP;
	}

	/**
	 * Sets the bread crumb for Root Category Landing Page.
	 * 
	 * @param pBreadcrumbForRCLP
	 *            the breadcrumbForRCLP to set
	 */
	public void setBreadcrumbForRCLP(String pBreadcrumbForRCLP) {
		mBreadcrumbForRCLP = pBreadcrumbForRCLP;
	}

	/**
	 * Returns the product category.
	 * 
	 * @return the prodCategory
	 */
	public String getProdCategory() {
		return mProdCategory;
	}

	/**
	 * Sets the prod category.
	 * 
	 * @param pProdCategory
	 *            the prodCategory to set
	 */
	public void setProdCategory(String pProdCategory) {
		mProdCategory = pProdCategory;
	}

	/**
	 * Returns the cat repository id.
	 * 
	 * @return the catRepositoryID
	 */
	public String getCatRepositoryID() {
		return mCatRepositoryID;
	}

	/**
	 * Sets the cat repository id.
	 * 
	 * @param pCatRepositoryID
	 *            the catRepositoryID to set
	 */
	public void setCatRepositoryID(String pCatRepositoryID) {
		mCatRepositoryID = pCatRepositoryID;
	}

	/**
	 * gets facet Navigation.
	 * 
	 * @return the facetNavigation
	 */
	public String getFacetNavigation() {
		return mFacetNavigation;
	}

	/**
	 * sets Facet Navigation.
	 * 
	 * @param pFacetNavigation
	 *            the facetNavigation to set
	 */
	public void setFacetNavigation(String pFacetNavigation) {
		mFacetNavigation = pFacetNavigation;
	}

	/**
	 * returns Category Refinement Menu.
	 * 
	 * @return the catRefinementMenu
	 */
	public String getCatRefinementMenu() {
		return this.mCatRefinementMenu;
	}

	/**
	 * sets Category Refinement Menu.
	 * 
	 * @param pCatRefinementMenu
	 *            the catRefinementMenu to set
	 */
	public void setCatRefinementMenu(String pCatRefinementMenu) {
		this.mCatRefinementMenu = pCatRefinementMenu;
	}

	/**
	 * returns Sub category landing page Url.
	 * 
	 * @return the sclpPageUrl
	 */
	public String getSclpPageUrl() {
		return this.mSclpPageUrl;
	}

	/**
	 * sets Sub category landing page Url.
	 * 
	 * @param pSclpPageUrl
	 *            the sclpPageUrl to set
	 */
	public void setSclpPageUrl(String pSclpPageUrl) {
		this.mSclpPageUrl = pSclpPageUrl;
	}

	/**
	 * returns the homeBread.
	 * 
	 * @return the homeBread
	 */
	public String getHomeBread() {
		return mHomeBread;
	}

	/**
	 * Sets Home Bread.
	 * 
	 * @param pHomeBread
	 *            the homeBread to set
	 */
	public void setHomeBread(String pHomeBread) {
		mHomeBread = pHomeBread;
	}

	/**
	 * Returns enable Endeca Content Cache.
	 * 
	 * @return the enableEndecaContentCache
	 */
	public boolean isEnableEndecaContentCache() {
		return mEnableEndecaContentCache;
	}

	/**
	 * Sets the enableEndecaContentCache.
	 * 
	 * @param pEnableEndecaContentCache
	 *            the enableEndecaContentCache to set
	 */
	public void setEnableEndecaContentCache(boolean pEnableEndecaContentCache) {
		mEnableEndecaContentCache = pEnableEndecaContentCache;
	}

	/**
	 * Returns the holds the allowed types for quick view.
	 * 
	 * @return the allowedTypesForQuickView
	 */
	public List<String> getAllowedTypesForQuickView() {
		return mAllowedTypesForQuickView;
	}

	/**
	 * Sets the holds the allowed types for quick view.
	 * 
	 * @param pAllowedTypesForQuickView
	 *            the allowedTypesForQuickView to set
	 */
	public void setAllowedTypesForQuickView(List<String> pAllowedTypesForQuickView) {
		mAllowedTypesForQuickView = pAllowedTypesForQuickView;
	}

	/**
	 * Returns the records per page for view all.
	 * 
	 * @return the recordsPerPageForViewAll
	 */
	public int getRecordsPerPageForViewAll() {
		return mRecordsPerPageForViewAll;
	}

	/**
	 * Sets the records per page for view all.
	 * 
	 * @param pRecordsPerPageForViewAll
	 *            the recordsPerPageForViewAll to set
	 */
	public void setRecordsPerPageForViewAll(int pRecordsPerPageForViewAll) {
		mRecordsPerPageForViewAll = pRecordsPerPageForViewAll;
	}

	/**
	 * Returns the holds the prod brand.
	 * 
	 * @return the prodBrand
	 */
	public String getProdBrand() {
		return mProdBrand;
	}

	/**
	 * Sets the holds the prod brand.
	 * 
	 * @param pProdBrand
	 *            the prodBrand to set
	 */
	public void setProdBrand(String pProdBrand) {
		mProdBrand = pProdBrand;
	}
	
	/**
	 * Gets the prodCharacter.
	 *
	 * @return the prodCharacter
	 */
	public String getProdCharacter() {
		return mProdCharacter;
	}
	
	/**
	 * Sets the prodCharacter.
	 *
	 * @param pProdCharacter the prodCharacter to set
	 */
	public void setProdCharacter(String pProdCharacter) {
		mProdCharacter = pProdCharacter;
	}
	
	/**
	 * Returns the holds the no color no size product type.
	 * 
	 * @return the noColorNoSizeProductType
	 */
	public String getNoColorNoSizeProductType() {
		return mNoColorNoSizeProductType;
	}

	/**
	 * Sets the holds the no color no size product type.
	 * 
	 * @param pNoColorNoSizeProductType
	 *            the noColorNoSizeProductType to set
	 */
	public void setNoColorNoSizeProductType(String pNoColorNoSizeProductType) {
		mNoColorNoSizeProductType = pNoColorNoSizeProductType;
	}
	
	/**
	 * Returns the holds the Site Enabled property name.
	 * 
	 * @return the holds the Site Enabled Property Name
	 */
	public String getSiteEnabledPropertyName() {
		return mSiteEnabledPropertyName;
	}

	/**
	 * Sets the holds the Site Enabled Property Name.
	 * 
	 * @param pSiteEnabledPropertyName
	 * 				the new holds the SiteEnabled property name
	 */
	public void setSiteEnabledPropertyName(String pSiteEnabledPropertyName) {
		mSiteEnabledPropertyName = pSiteEnabledPropertyName;
	}
	
	
	/**
	 * Returns the holds the Endeca Search Interface Property Name.
	 * 
	 * @return the holds the Endeca Search Interface Property Name
	 */
	public String getEndecaSearchInterfacePropertyName() {
		return mEndecaSearchInterfacePropertyName;
	}

	/**
	 * Sets the holds the Endeca Search Interface Property Name.
	 * 
	 * @param pEndecaSearchInterfacePropertyName
	 * 				the new holds the Endeca Search Interface Property Name
	 */
	public void setEndecaSearchInterfacePropertyName(String pEndecaSearchInterfacePropertyName) {
		mEndecaSearchInterfacePropertyName = pEndecaSearchInterfacePropertyName;
	}
	
	/**
	 * Returns the holds the Endeca Indexable Property Name.
	 * 
	 * @return the holds the Endeca Indexable Property Name
	 */
	public String getEndecaIndexablePropertyName() {
		return mEndecaIndexablePropertyName;
	}

	/**
	 * Sets the holds the Endeca Indexable Property Name.
	 * 
	 * @param pEndecaIndexablePropertyName
	 * 				the new holds the Endeca Indexable Property Name
	 */
	public void setEndecaIndexablePropertyName(String pEndecaIndexablePropertyName) {
		mEndecaIndexablePropertyName = pEndecaIndexablePropertyName;
	}
	
	/**
	 * Returns the holds the Endeca User Segment Property Name.
	 * 
	 * @return the holds the EndecaUserSegmentPropertyName
	 */
	public String getEndecaUserSegmentPropertyName() {
		return mEndecaUserSegmentPropertyName;
	}

	/**
	 * Sets the holds the Endeca User Segment PropertyName.
	 * 
	 * @param pEndecaUserSegmentPropertyName
	 * 				the new holds the Endeca User Segment Property Name
	 */
	public void setEndecaUserSegmentPropertyName(String pEndecaUserSegmentPropertyName) {
		mEndecaUserSegmentPropertyName = pEndecaUserSegmentPropertyName;
	}
	
	
	/**
	 * Returns the holds the Default PriceList Property Name.
	 * 
	 * @return the holds the DefaultPriceListPropertyName
	 */
	public String getDefaultPriceListPropertyName() {
		return mDefaultPriceListPropertyName;
	}

	/**
	 * Sets the holds the Default PriceList Property Name.
	 * 
	 * @param pDefaultPriceListPropertyName
	 * 				the new holds the Default PriceList Property Name
	 */
	public void setDefaultPriceListPropertyName(String pDefaultPriceListPropertyName) {
		mDefaultPriceListPropertyName = pDefaultPriceListPropertyName;
	}
	
	
	/**
	 * Returns the holds the Dynamic Attributes Property Name.
	 * 
	 * @return the holds the DynamicAttributesPropertyName
	 */
	public String getDynamicAttributesPropertyName() {
		return mDynamicAttributesPropertyName;
	}

	/**
	 * Sets the holds the Dynamic Attributes Property Name.
	 * 
	 * @param pDynamicAttributesPropertyName
	 * 				the new holds the Dynamic Attributes Property Name
	 */
	public void setDynamicAttributesPropertyName(String pDynamicAttributesPropertyName) {
		mDynamicAttributesPropertyName = pDynamicAttributesPropertyName;
	}
	
	
	/**
	 * Returns the holds the DefaultCatalogPropertyName.
	 * 
	 * @return the holds the DefaultCatalogPropertyName
	 */
	public String getDefaultCatalogPropertyName() {
		return mDefaultCatalogPropertyName;
	}

	/**
	 * Sets the holds the Default Catalog Property Name.
	 * 
	 * @param pDefaultCatalogPropertyName
	 * 				the new holds the DefaultCatalog PropertyName
	 */
	public void setDefaultCatalogPropertyName(String pDefaultCatalogPropertyName) {
		mDefaultCatalogPropertyName = pDefaultCatalogPropertyName;
	}

	/**
	 * Gets the advertisement copy type.
	 *
	 * @return the madvertisementCopyType
	 */
	public String getAdvertisementCopyType() {
		return mAdvertisementCopyType;
	}

	/**
	 * Sets the advertisement copy type.
	 *
	 * @param pAdvertisementCopyType the new advertisement copy type
	 */
	public void setAdvertisementCopyType(String pAdvertisementCopyType) {
		mAdvertisementCopyType = pAdvertisementCopyType;
	}

	/**
	 * Sets the sale price property name.
	 *
	 * @param pSalePricePropertyName the salePricePropertyName to set
	 */
	public void setSalePricePropertyName(String pSalePricePropertyName) {
		mSalePricePropertyName = pSalePricePropertyName;
	}

	/**
	 * Gets the advertisement copy dsc.
	 *
	 * @return the madvertisementCopyDsc
	 */
	public String getAdvertisementCopyDsc() {
		return mAdvertisementCopyDsc;
	}

	/**
	 * Sets the advertisement copy dsc.
	 *
	 * @param pAdvertisementCopyDsc the new advertisement copy dsc
	 */
	public void setAdvertisementCopyDsc(String pAdvertisementCopyDsc) {
		mAdvertisementCopyDsc = pAdvertisementCopyDsc;
	}
	
	/**
	 * Returns the SoftwareCompatibility.
	 * 
	 * @return the SoftCom
	 */
	public String getSoftComp(){
		return mSoftComp;
	}
	
	/**
	 * Sets the SoftwareCompatibility.
	 * 
	 * @param pSoftComp
	 *            the SoftwareCompatibility to set
	 */
	public void setSoftComp(String  pSoftComp){
		mSoftComp = pSoftComp;
	}
	
	/**
	 * Returns the BnbSiteId.
	 * 
	 * @return the BnbSiteId
	 */
	public String getBnbSiteId(){
		return mBnbSiteId;
	}
	
	/**
	 * Sets the BnbSiteId.
	 * 
	 * @param pBnbSiteId
	 *            the BnbSiteId to set
	 */
	public void setBnbSiteId(String  pBnbSiteId){
		mBnbSiteId = pBnbSiteId;
	}
	
	/**
	 * Returns the HomePag.
	 * 
	 * @return the mHomePage
	 */

	public String getHomePage() {
		return mHomePage;
	}
	
	/**
	 * Sets the HomePage.
	 * 
	 * @param pHomePage
	 *            the SoftwareCompatibility to set
	 */

	public void setHomePage(String pHomePage) {
		this.mHomePage = pHomePage;
	}
	
	/**
	 * Returns the rclp page url.
	 * 
	 * @return the RclpPageUrl
	 */
	public String getTypeAheadPageUrl() {
		return mTypeAheadPageUrl;
	}

	/**
	 * Sets the rclp page url.
	 *
	 * @param pTypeAheadPageUrl the new type ahead page url
	 */
	public void setTypeAheadPageUrl(String pTypeAheadPageUrl) {
		mTypeAheadPageUrl = pTypeAheadPageUrl;
	}

	
	/**
	 * Returns the minAutoSuggestInputLength .
	 * 
	 * @return the minAutoSuggestInputLength
	 */
	public String getMinAutoSuggestInputLength() {
		return mMinAutoSuggestInputLength;
	}

	/**
	 * Sets the minAutoSuggestInputLength.
	 * 
	 * @param pMinAutoSuggestInputLength
	 *            the minAutoSuggestInputLength to set
	 */
	public void setMinAutoSuggestInputLength(String pMinAutoSuggestInputLength) {
		mMinAutoSuggestInputLength = pMinAutoSuggestInputLength;
	}
	
	/**
	 * Returns the mSecurityLevelDimensionName .
	 * 
	 * @return the mSecurityLevelDimensionName
	 */
	
	public String getSecurityLevelDimensionName() {
		return mSecurityLevelDimensionName;
	}
	
	/**
	 * Sets the mSecurityLevelDimensionName.
	 * 
	 * @param pSecurityLevelDimensionName
	 *            the mSecurityLevelDimensionName to set
	 */
	public void setSecurityLevelDimensionName(String pSecurityLevelDimensionName) {
		this.mSecurityLevelDimensionName = pSecurityLevelDimensionName;
	}

	/**
	 * Returns the mHighSecurityDimensionValue .
	 * 
	 * @return the mHighSecurityDimensionValue
	 */
	public String getHighSecurityDimensionValue() {
		return mHighSecurityDimensionValue;
	}
	/**
	 * Sets the mHighSecurityDimensionValue.
	 * 
	 * @param pHighSecurityDimensionValue
	 *            the mHighSecurityDimensionValue to set
	 */
	public void setHighSecurityDimensionValue(String pHighSecurityDimensionValue) {
		this.mHighSecurityDimensionValue = pHighSecurityDimensionValue;
	}
	
	/**
	 * Returns the mHighSecurityProducts .
	 * 
	 * @return the mHighSecurityProducts
	 */
	public String getHighSecurityProducts() {
		return mHighSecurityProducts;
	}
	
	/**
	 * Sets the mHighSecurityProducts.
	 * 
	 * @param pHighSecurityProducts
	 *            the mHighSecurityProducts to set
	 */
	public void setHighSecurityProducts(String pHighSecurityProducts) {
		this.mHighSecurityProducts = pHighSecurityProducts;
	}
	
	/**
	 * Returns the mSdSiteType .
	 * 
	 * @return the mSdSiteType
	 */
	public String getSdSiteType() {
		return mSdSiteType;
	}
	/**
	 * Sets the mSdSiteType.
	 * 
	 * @param pSdSiteType
	 *            the mSdSiteType to set
	 */

	public void setSdSiteType(String pSdSiteType) {
		this.mSdSiteType = pSdSiteType;
	}
	
	/**
	 * Returns the mLaserBusinessCheckLabel .
	 * 
	 * @return the mLaserBusinessCheckLabel
	 */
	public String getLaserBusinessCheckLabel() {
		return mLaserBusinessCheckLabel;
	}
	/**
	 * Sets the mLaserBusinessCheckLabel.
	 * 
	 * @param pLaserBusinessCheckLabel
	 *            the mLaserBusinessCheckLabel to set
	 */

	public void setLaserBusinessCheckLabel(String pLaserBusinessCheckLabel) {
		this.mLaserBusinessCheckLabel = pLaserBusinessCheckLabel;
	}

	/**
	 * Gets the dim name for all level categories.
	 *
	 * @return the mDimNameForAllLevelCategories
	 */
	public String getDimNameForAllLevelCategories() {
		return mDimNameForAllLevelCategories;
	}

	/**
	 * Sets the dim name for all level categories.
	 *
	 * @param pDimNameForAllLevelCategories the mDimNameForAllLevelCategories to set
	 */
	public void setDimNameForAllLevelCategories(String pDimNameForAllLevelCategories) {
		this.mDimNameForAllLevelCategories = pDimNameForAllLevelCategories;
	}
	
	/**
	 * Gets the non navigable category.
	 *
	 * @return the mNonNavigableCategory
	 */
	public String getNonNavigableCategory() {
		return mNonNavigableCategory;
	}

	/**
	 * Sets the non navigable category.
	 *
	 * @param pNonNavigableCategory the mNonNavigableCategory to set
	 */
	public void setNonNavigableCategory(String pNonNavigableCategory) {
		this.mNonNavigableCategory = pNonNavigableCategory;
	}
	
	/**
	 * Gets the parent category.
	 *
	 * @return the parentCategory
	 */
	public String getParentCategory() {
		return mParentCategory;
	}
	
	/**
	 * Sets the parent category.
	 *
	 * @param pParentCategory the parentCategory to set
	 */
	public void setParentCategory(String pParentCategory) {
		mParentCategory = pParentCategory;
	}
	
	/**
	 * Gets the classification item descriptor name.
	 *
	 * @return the classificationItemDescriptorName
	 */
	public String getClassificationItemDescriptorName() {
		return mClassificationItemDescriptorName;
	}
	
	/**
	 * Sets the classification item descriptor name.
	 *
	 * @param pClassificationItemDescriptorName the classificationItemDescriptorName to set
	 */
	public void setClassificationItemDescriptorName(
			String pClassificationItemDescriptorName) {
		mClassificationItemDescriptorName = pClassificationItemDescriptorName;
	}
	
	/**
	 * Gets the site item descriptor name.
	 *
	 * @return the siteItemDescriptorName
	 */
	public String getSiteItemDescriptorName() {
		return mSiteItemDescriptorName;
	}
	
	/**
	 * Sets the site item descriptor name.
	 *
	 * @param pSiteItemDescriptorName the siteItemDescriptorName to set
	 */
	public void setSiteItemDescriptorName(String pSiteItemDescriptorName) {
		mSiteItemDescriptorName = pSiteItemDescriptorName;
	}
	
	/**
	 * Gets the site enabled.
	 *
	 * @return the siteEnabled
	 */
	public String getSiteEnabled() {
		return mSiteEnabled;
	}
	
	/**
	 * Sets the site enabled.
	 *
	 * @param pSiteEnabled the siteEnabled to set
	 */
	public void setSiteEnabled(String pSiteEnabled) {
		mSiteEnabled = pSiteEnabled;
	}
	
	/**
	 * Gets the site default catalog.
	 *
	 * @return the siteDefaultCatalog
	 */
	public String getSiteDefaultCatalog() {
		return mSiteDefaultCatalog;
	}
	
	/**
	 * Sets the site default catalog.
	 *
	 * @param pSiteDefaultCatalog the siteDefaultCatalog to set
	 */
	public void setSiteDefaultCatalog(String pSiteDefaultCatalog) {
		mSiteDefaultCatalog = pSiteDefaultCatalog;
	}
	
	/**
	 * Gets the site root category.
	 *
	 * @return the siteRootCategory
	 */
	public String getSiteRootCategory() {
		return mSiteRootCategory;
	}
	
	/**
	 * Sets the site root category.
	 *
	 * @param pSiteRootCategory the siteRootCategory to set
	 */
	public void setSiteRootCategory(String pSiteRootCategory) {
		mSiteRootCategory = pSiteRootCategory;
	}
	
	/**
	 * Gets the classification root parent category.
	 *
	 * @return the classificationRootParentCategory
	 */
	public String getClassificationRootParentCategory() {
		return mClassificationRootParentCategory;
	}
	
	/**
	 * Sets the classification root parent category.
	 *
	 * @param pClassificationRootParentCategory the classificationRootParentCategory to set
	 */
	public void setClassificationRootParentCategory(
			String pClassificationRootParentCategory) {
		mClassificationRootParentCategory = pClassificationRootParentCategory;
	}
	
	/**
	 * Gets the classification is deleted.
	 *
	 * @return the classificationIsDeleted
	 */
	public String getClassificationIsDeleted() {
		return mClassificationIsDeleted;
	}
	
	/**
	 * Sets the classification is deleted.
	 *
	 * @param pClassificationIsDeleted the classificationIsDeleted to set
	 */
	public void setClassificationIsDeleted(String pClassificationIsDeleted) {
		mClassificationIsDeleted = pClassificationIsDeleted;
	}
	
	/**
	 * Gets the classification display status.
	 *
	 * @return the classificationDisplayStatus
	 */
	public String getClassificationDisplayStatus() {
		return mClassificationDisplayStatus;
	}
	
	/**
	 * Sets the classification display status.
	 *
	 * @param pClassificationDisplayStatus the classificationDisplayStatus to set
	 */
	public void setClassificationDisplayStatus(String pClassificationDisplayStatus) {
		mClassificationDisplayStatus = pClassificationDisplayStatus;
	}
	
	/**
	 * Gets the classification name.
	 *
	 * @return the classificationName
	 */
	public String getClassificationName() {
		return mClassificationName;
	}
	
	/**
	 * Sets the classification name.
	 *
	 * @param pClassificationName the classificationName to set
	 */
	public void setClassificationName(String pClassificationName) {
		mClassificationName = pClassificationName;
	}
	
	/**
	 * Gets the child classifications.
	 *
	 * @return the childClassifications
	 */
	public String getChildClassifications() {
		return mChildClassifications;
	}
	
	/**
	 * Sets the child classifications.
	 *
	 * @param pChildClassifications the childClassifications to set
	 */
	public void setChildClassifications(String pChildClassifications) {
		mChildClassifications = pChildClassifications;
	}
	
	/**
	 * Gets the parent classifications.
	 *
	 * @return the parentClassifications
	 */
	public String getParentClassifications() {
		return mParentClassifications;
	}
	
	/**
	 * Sets the parent classifications.
	 *
	 * @param pParentClassifications the parentClassifications to set
	 */
	public void setParentClassifications(String pParentClassifications) {
		mParentClassifications = pParentClassifications;
	}
	
	/**
	 * Gets the classification user type id.
	 *
	 * @return the classificationUserTypeId
	 */
	public String getClassificationUserTypeId() {
		return mClassificationUserTypeId;
	}
	
	/**
	 * Sets the classification user type id.
	 *
	 * @param pClassificationUserTypeId the classificationUserTypeId to set
	 */
	public void setClassificationUserTypeId(String pClassificationUserTypeId) {
		mClassificationUserTypeId = pClassificationUserTypeId;
	}
	
	/**
	 * Gets the classification rrelated media content.
	 *
	 * @return the classificationRrelatedMediaContent
	 */
	public String getClassificationRrelatedMediaContent() {
		return mClassificationRrelatedMediaContent;
	}
	
	/**
	 * Sets the classification rrelated media content.
	 *
	 * @param pClassificationRrelatedMediaContent the classificationRrelatedMediaContent to set
	 */
	public void setClassificationRrelatedMediaContent(
			String pClassificationRrelatedMediaContent) {
		mClassificationRrelatedMediaContent = pClassificationRrelatedMediaContent;
	}
	
	/**
	 * Gets the media name.
	 *
	 * @return the mediaName
	 */
	public String getMediaName() {
		return mMediaName;
	}
	
	/**
	 * Sets the media name.
	 *
	 * @param pMediaName the mediaName to set
	 */
	public void setMediaName(String pMediaName) {
		mMediaName = pMediaName;
	}
	
	/**
	 * Gets the media type.
	 *
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mMediaType;
	}
	
	/**
	 * Sets the media type.
	 *
	 * @param pMediaType the mediaType to set
	 */
	public void setMediaType(String pMediaType) {
		mMediaType = pMediaType;
	}
	
	/**
	 * Gets the media url.
	 *
	 * @return the mediaUrl
	 */
	public String getMediaUrl() {
		return mMediaUrl;
	}
	
	/**
	 * Sets the media url.
	 *
	 * @param pMediaUrl the mediaUrl to set
	 */
	public void setMediaUrl(String pMediaUrl) {
		mMediaUrl = pMediaUrl;
	}
	
	/**
	 * Gets the media html content.
	 *
	 * @return the mediaHtmlContent
	 */
	public String getMediaHtmlContent() {
		return mMediaHtmlContent;
	}
	
	/**
	 * Sets the media html content.
	 *
	 * @param pMediaHtmlContent the mediaHtmlContent to set
	 */
	public void setMediaHtmlContent(String pMediaHtmlContent) {
		mMediaHtmlContent = pMediaHtmlContent;
	}
	
	/**
	 * Gets the category image key.
	 *
	 * @return the categoryImageKey
	 */
	public String getCategoryImageKey() {
		return mCategoryImageKey;
	}
	
	/**
	 * Sets the category image key.
	 *
	 * @param pCategoryImageKey the categoryImageKey to set
	 */
	public void setCategoryImageKey(String pCategoryImageKey) {
		mCategoryImageKey = pCategoryImageKey;
	}
	
	/**
	 * Gets the banner image key.
	 *
	 * @return the bannerImageKey
	 */
	public String getBannerImageKey() {
		return mBannerImageKey;
	}
	
	/**
	 * Sets the banner image key.
	 *
	 * @param pBannerImageKey the bannerImageKey to set
	 */
	public void setBannerImageKey(String pBannerImageKey) {
		mBannerImageKey = pBannerImageKey;
	}
	
	/**
	 * Gets the promo image key.
	 *
	 * @return the promoImageKey
	 */
	public String getPromoImageKey() {
		return mPromoImageKey;
	}
	
	/**
	 * Sets the promo image key.
	 *
	 * @param pPromoImageKey the promoImageKey to set
	 */
	public void setPromoImageKey(String pPromoImageKey) {
		mPromoImageKey = pPromoImageKey;
	}
	
	/**
	 * Gets the seo text key.
	 *
	 * @return the seo text key
	 */
	public String getSeoTextKey() {
		return mSeoTextKey;
	}
	
	/**
	 * Sets the seo text key.
	 *
	 * @param pSeoTextKey the new seo text key
	 */
	public void setSeoTextKey(String pSeoTextKey) {
		this.mSeoTextKey = pSeoTextKey;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return mType;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param pType the new type
	 */
	public void setType(String pType) {
		this.mType = pType;
	}
	/**
	 * Returns the TruSiteMapTargeter.
	 *
	 * @return the mTruSiteMapTargeter
	 */
	public String getTruSiteMapTargeter() {
		return mTruSiteMapTargeter;
	}
	/**
	 * Sets the truSiteMapTargeter.
	 *
	 * @param pTruSiteMapTargeter the new type
	 */
	public void setTruSiteMapTargeter(String pTruSiteMapTargeter) {
		mTruSiteMapTargeter = pTruSiteMapTargeter;
	}
	/**
	 * Returns the BruSiteMapTargeter.
	 *
	 * @return the mBruSiteMapTargeter
	 */
	public String getBruSiteMapTargeter() {
		return mBruSiteMapTargeter;
	}
	/**
	 * Sets the bruSiteMapTargeter.
	 *
	 * @param pBruSiteMapTargeter the new type
	 */
	public void setBruSiteMapTargeter(String pBruSiteMapTargeter) {
		mBruSiteMapTargeter = pBruSiteMapTargeter;
	}
	/**
	 * Returns the SiteMapToolTargeter.
	 *
	 * @return the mSiteMapToolTargeter
	 */
	public String getSiteMapToolTargeter() {
		return mSiteMapToolTargeter;
	}
	/**
	 * Sets the siteMapToolTargeter.
	 *
	 * @param pSiteMapToolTargeter the new type
	 */
	public void setSiteMapToolTargeter(String pSiteMapToolTargeter) {
		mSiteMapToolTargeter = pSiteMapToolTargeter;
	}

	/**
	 * @return the skuType
	 */
	public List<String> getSkuType() {
		return mSkuType;
	}

	/**
	 * @param pSkuType the skuType to set
	 */
	public void setSkuType(List<String> pSkuType) {
		mSkuType = pSkuType;
	}
	
	/**
	 * Property to Hold mCMSSpotsDisabledChannelList.
	 */
	private List<String> mCMSSpotsDisabledChannelList;
	
	/**
	 * This property hold reference for mDisableCMSSpotsForRest.
	 */
	private boolean mDisableCMSSpots;
	
	
	private boolean mDisableEndecaPrice;
	
	/**
	 * This property hold reference for mBruSiteCheck.
	 */
	
	private boolean mBruSiteCheck;
	
	/**
	 * @return the disableCMSSpots
	 */
	public boolean isDisableCMSSpots() {
		return mDisableCMSSpots;
	}

	/**
	 * @return the disableEndecaPrice
	 */
	public boolean isDisableEndecaPrice() {
		return mDisableEndecaPrice;
	}

	/**
	 * @param pDisableEndecaPrice the disableEndecaPrice to set
	 */
	public void setDisableEndecaPrice(boolean pDisableEndecaPrice) {
		mDisableEndecaPrice = pDisableEndecaPrice;
	}

	/**
	 * @param pDisableCMSSpots the disableCMSSpots to set
	 */
	public void setDisableCMSSpots(boolean pDisableCMSSpots) {
		mDisableCMSSpots = pDisableCMSSpots;
	}

	/**
	 * @return the cMSSpotsDisabledChannelList
	 */
	public List<String> getCMSSpotsDisabledChannelList() {
		return mCMSSpotsDisabledChannelList;
	}

	/**
	 * @param pCMSSpotsDisabledChannelList the cMSSpotsDisabledChannelList to set
	 */
	public void setCMSSpotsDisabledChannelList(
			List<String> pCMSSpotsDisabledChannelList) {
		mCMSSpotsDisabledChannelList = pCMSSpotsDisabledChannelList;
	}
	
	/**
	 * Returns the this property hold reference for MaximumCategoryLevelForParametricReport.
	 * 
	 * @return the MaximumCategoryLevelForParametricReport
	 */
	public int getMaximumCategoryLevelForParametricReport() {
		return mMaximumCategoryLevelForParametricReport;
	}
	
	/**
	 * Sets the this property hold reference for MaximumCategoryLevelForParametricReport.
	 * 
	 * @param pMaximumCategoryLevelForParametricReport the MaximumCategoryLevelForParametricReport to set
	 */
	public void setMaximumCategoryLevelForParametricReport(int pMaximumCategoryLevelForParametricReport) {
		mMaximumCategoryLevelForParametricReport = pMaximumCategoryLevelForParametricReport;
	}

	/**
	 * @return the bruSiteCheck
	 */
	public boolean isBruSiteCheck() {
		return mBruSiteCheck;
	}

	/**
	 * @param pBruSiteCheck the bruSiteCheck to set
	 */
	public void setBruSiteCheck(boolean pBruSiteCheck) {
		mBruSiteCheck = pBruSiteCheck;
	}

}