package com.tru.endeca.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import atg.commerce.endeca.index.dimension.CategoryNode;
import atg.commerce.endeca.index.dimension.CategoryTree;
import atg.commerce.endeca.index.dimension.CategoryTreeService;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * @author PA
 * This class is used to populate the urls
 *
 */
public class TRUEndecaUtils extends GenericService {
	
	
	/**
	 * Property to hold mDomainUrl.
	 */
	 private String mDomainUrl;
	/**
	 * Property to hold mProtocolScheme.
	 */
	 private String mProtocolScheme;
	
	/**
	 * Property to hold mMaxLevel.
	 */
	private int mMaxLevel;
	
	private CategoryTreeService mCategoryTreeService;

	/**
	 *  The m tru get site type util. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;
	
	/**  Holds the mChekPrimaryCategory */
	private boolean mChekPrimaryCategory;
	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}	
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}

	/** Property Holds siteContextManager. */
	private SiteContextManager mSiteContextManager;
	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * This property holds CatalogProperties variable catalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}


	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;
	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}	
	/**
	 * This method will return the ContextPath when we pass the Category Id.
	 * 
	 * @param pCategoryId - CategoryId
	 * @return String siteProductUrl
	 */
	@SuppressWarnings("unchecked")
	public String getCategoryContextPath(String pCategoryId) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===START===:: TRUEndecaUtils.getCategoryContextPath method:: ");
		}
		StringBuffer contextPath = new StringBuffer(TRUEndecaConstants.EMPTY_STRING);
		if (!StringUtils.isEmpty(pCategoryId)) {
			try {
				RepositoryItem categoryItem = getCatalogTools().findCategory(pCategoryId);
				if (categoryItem != null) {
					Set<String> siteIdList = (Set<String>) categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
					String displayStatus = (String) categoryItem.getPropertyValue(getCatalogProperties().getDisplayStatusPropertyName());

					String currentSiteId = SiteContextManager.getCurrentSiteId();
					if (TRUCommerceConstants.HIDDEN.equalsIgnoreCase(displayStatus)||TRUCommerceConstants.NO_DISPLAY.equalsIgnoreCase(displayStatus)) {
						return null;
					}
					else if (siteIdList == null || siteIdList.isEmpty()) {
						return null;
					}

					if (!StringUtils.isEmpty(currentSiteId)	&& siteIdList.contains(currentSiteId)) {
						contextPath.append(getSchemeDetectionUtil().getScheme());
						contextPath = contextPath.append(getSiteProductionUrl(currentSiteId));
					} else {
						for (String siteId : siteIdList) {
							if (!StringUtils.isEmpty(siteId)) {
								contextPath.append(getSchemeDetectionUtil().getScheme());
								contextPath = contextPath.append(getSiteProductionUrl(siteId));
								break;
							}
						}
					}
				}
			} catch (RepositoryException repositoryException) {
				vlogError(repositoryException, "RepositoryException occured in TRUEndecaUtils.getCategoryContextPath");
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUEndecaUtils.getCategoryContextPath method:: Site Production Url = {0}", contextPath);
		}
		return contextPath.toString();

	}


	/**
	 * This method will return the ContextPath from the Site production url.
	 * 
	 * @param pSiteId - SIte Id
	 * @return String siteProductUrl
	 */
	public String getSiteProductionUrl(String pSiteId) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===START===:: TRUEndecaUtils.getSiteProductionUrl method:: ");
		}
		String contextPath = null;
		Site site = null;
		if(!StringUtils.isEmpty(pSiteId)) {
			try {
				site = getSiteContextManager().getSite(pSiteId);
				if(site != null) {
					DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
					TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, site);
					if(truSiteVO != null ) {
						contextPath = truSiteVO.getSiteURL();
					}
				}
			} catch (SiteContextException siteContextException) {
				vlogError(siteContextException, "Site Context Exception occured in TRUEndecaUtils.getSiteProductionUrl");
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"===END===:: TRUEndecaUtils.getSiteProductionUrl method:: Site Production Url = {0}",contextPath);
		}
		return contextPath;
	}


	/**
	 * The method will return whether the sku is active or not.
	 * 
	 * @param pChildSku - ChildSku repository item
	 * @return isActive - return boolean property whether sku is active or not
	 */
	public boolean isActiveSku(RepositoryItem pChildSku) {

		if (isLoggingDebug()) {
			vlogDebug("======BEGIN: ===== Inside TRUEndecaUtils.isActiveSku() method");
		}

		boolean isActive = false;
		if(pChildSku == null) {
			return isActive;
		}

		boolean isDeletedFlag =	(boolean)pChildSku.getPropertyValue((getCatalogProperties()).getIsSkuDeleted());
		boolean isSupressInSearchFlag =	(boolean)pChildSku.getPropertyValue((getCatalogProperties()).getSupressInSearch());
		boolean isWebDisplayFlag = (boolean)pChildSku.getPropertyValue((getCatalogProperties()).getWebDisplayFlag());
		String onlinePID = (String) pChildSku.getPropertyValue((getCatalogProperties()).getOnlinePID());
		boolean superDisplayFlag = false;
		if(pChildSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag()) == null) {
			superDisplayFlag = true;
		} else {
			superDisplayFlag = (boolean) pChildSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag());
		}

		if(!isDeletedFlag && isWebDisplayFlag && !StringUtils.isEmpty(onlinePID) && superDisplayFlag && !isSupressInSearchFlag) {
			isActive = true;
			if (isLoggingDebug()) {
				vlogDebug("The Sku Id : {0} is Active and adding the Color Swatches",pChildSku.getRepositoryId());
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("======END: ===== Inside TRUEndecaUtils.isActiveSku() method");
		}
		return isActive;

	}
	
	/**
	 * The method will return whether the sku is active or not.
	 * 
	 * @param pChildSku - ChildSku repository item
	 * @return isActive - return boolean property whether sku is active or not
	 */
	public boolean isActiveProductSku(RepositoryItem pChildSku) {

		if (isLoggingDebug()) {
			vlogDebug("======BEGIN: ===== Inside TRUEndecaUtils.isActiveSku() method");
		}

		boolean isActive = false;
		if(pChildSku == null) {
			return isActive;
		}

		boolean isDeletedFlag =	(boolean)pChildSku.getPropertyValue((getCatalogProperties()).getIsSkuDeleted());
		boolean isWebDisplayFlag = (boolean)pChildSku.getPropertyValue((getCatalogProperties()).getWebDisplayFlag());
		String onlinePID = (String) pChildSku.getPropertyValue((getCatalogProperties()).getOnlinePID());
		boolean superDisplayFlag = false;
		if(pChildSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag()) == null) {
			superDisplayFlag = true;
		} else {
			superDisplayFlag = (boolean) pChildSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag());
		}

		if(!isDeletedFlag && isWebDisplayFlag && !StringUtils.isEmpty(onlinePID) && superDisplayFlag ) {
			isActive = true;
			if (isLoggingDebug()) {
				vlogDebug("The Sku Id : {0} is Active and adding the Color Swatches",pChildSku.getRepositoryId());
			}
		}

		if (isLoggingDebug()) {
			vlogDebug("======END: ===== Inside TRUEndecaUtils.isActiveSku() method");
		}
		return isActive;

	}

	/**
	 * This method is used to return the category hierarchy from CategoryTree. 
	 * @param pCategoryId -- the category Id
	 * @return categoryHirarchyList -- the list of category
	 */
	public List<String> generateCategoryHirarchy(String pCategoryId)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:  TRUEndecaUtils.generateCategoryHirarchy() method");
		}
		List <String> categoryHirarchyList = null;
		CategoryTree categoryTree = null;
		List<CategoryNode> categoryNodeList = null;
		if(StringUtils.isNotBlank(pCategoryId))
		{
			categoryTree = getCategoryTreeService().getCategoryTreeBuildingIfNeeded();
			if(categoryTree != null) {
				categoryNodeList = categoryTree.getCategoryNodeIndex().get(pCategoryId);
			}
			
			if(categoryNodeList != null && !categoryNodeList.isEmpty()){
				CategoryNode categoryNode = categoryNodeList.get(TRUEndecaConstants.INT_ZERO);
					if (categoryNode != null) {
				        String categoryPath = categoryNode.getPathAsString();
				        if(StringUtils.isNotBlank(categoryPath) && categoryPath.contains(TRUEndecaConstants.PATH_SEPERATOR))
				        {
				        	String	path = categoryPath.substring(TRUEndecaConstants.INT_ONE, categoryPath.length());
				        	String pathArray[] = path.split(TRUEndecaConstants.PATH_SEPERATOR);
				        	categoryHirarchyList = Arrays.asList(pathArray);
				        }
				      }		
			}
			
			
		}
		if (isLoggingDebug()) {
			vlogDebug("END:  TRUEndecaUtils.generateCategoryHirarchy() method");
		}
		return categoryHirarchyList;
	}
	
	/**
	 * @return the categoryTreeService
	 */
	public CategoryTreeService getCategoryTreeService() {
		return mCategoryTreeService;
	}



	/**
	 * @param pCategoryTreeService the categoryTreeService to set
	 */
	public void setCategoryTreeService(CategoryTreeService pCategoryTreeService) {
		mCategoryTreeService = pCategoryTreeService;
	}
	
	/**
	 * This method is used to return the PrimaryPath category list.
	 * 
	 * @param pCategoryId -- The category Id
	 * @return primaryCategoryIdList -- the PrimaryPath category list
	 */
	public List<String> populatePrimaryBreadcrumbPathList(String pCategoryId)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUEndecaUtils.populatePrimaryBreadcrumbPathList() method..");
		}
		List<String> primaryCategoryIdList = null;
			if(StringUtils.isNotBlank(pCategoryId)) {
				primaryCategoryIdList = new ArrayList<String>();
				primaryCategoryIdList = generateCategoryPrimaryBreadcrumb(pCategoryId, primaryCategoryIdList, TRUCommerceConstants.INT_ONE);
					if(primaryCategoryIdList != null && !primaryCategoryIdList.isEmpty()) {
						Collections.reverse(primaryCategoryIdList);
					}
					if (isLoggingDebug()) {
						vlogDebug("TRUEndecaUtils.populatePrimaryPathVOList()-- primaryCategoryIdList :: {0}",primaryCategoryIdList);
					}	
				}
		if (isLoggingDebug()) {
			logDebug("END:: TRUEndecaUtils.populatePrimaryPathVOList() method..");
		}
		return primaryCategoryIdList;
	}
	/**
	 * This method is used to populate the Primary Breadcrumb for a Category.
	 * @param pCategoryId -- the category Id
	 * @param pCategoryIdList -- the category Id List
	 * @param pLevel -- the level
	 * @return breadcrumb -- the breadcrumb string
	 */
	public List<String> generateCategoryPrimaryBreadcrumb(String pCategoryId,List<String> pCategoryIdList,int pLevel) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:  TRUEndecaUtils.generateCategoryPrimaryBreadcrumb() method");
		}
		int level = pLevel;
		String primaryParentCategoryId = null;
		List<String> primaryCategoryIdList = null;
		if(StringUtils.isNotBlank(pCategoryId) && pCategoryIdList != null)	{
			primaryCategoryIdList = pCategoryIdList;
			primaryCategoryIdList.add(pCategoryId);
			try {
				RepositoryItem categoryItem = getCatalogTools().findCategory(pCategoryId);
				if (categoryItem != null) {
					primaryParentCategoryId = (String) categoryItem.getPropertyValue(getCatalogProperties().getPrimaryParentCategoryPropertyName());
				}
				if(StringUtils.isNotBlank(primaryParentCategoryId) && !(primaryParentCategoryId.equals(TRUCommerceConstants.NAVIGATION_TRU) || primaryParentCategoryId.equals(TRUCommerceConstants.NAVIGATION_BRU)) && getMaxLevel() > level) {
					level++;
					return generateCategoryPrimaryBreadcrumb(primaryParentCategoryId,primaryCategoryIdList, level);
				}
			} catch (RepositoryException repositoryException) {
				if(isLoggingError()) {
					vlogError(repositoryException, "RepositoryException occured in TRUCategoryPrimaryPathManager.generateCategoryPrimaryBreadcrumb(---) method getting primary Category");	
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:  TRUEndecaUtils.generateCategoryPrimaryBreadcrumb() method");
		}
		return primaryCategoryIdList;
	}
	
	/**
	 * This method is used to return the category Breadcrumb .
	 * @param pCategoryIdList -- the category Id List
	 * @return String -- the breadcrumb string
	 */
	public String generateCategoryPrimaryPathBreadcrumb(List<String> pCategoryIdList)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUEndecaUtils  :: generateCategoryPrimaryPathBreadcrumb() method ");
		}
		StringBuffer breadCrumbBuffer = new StringBuffer();
		List<String> finalBreadCrumbList = null;
		if(pCategoryIdList != null && !pCategoryIdList.isEmpty()) {
			finalBreadCrumbList = populatePrimaryCategoryBreadcrumb(pCategoryIdList);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUEndecaUtils  :: generateCategoryPrimaryPathBreadcrumb():: BreadCrumb List :: {0} ",finalBreadCrumbList);
		}
		int size = finalBreadCrumbList.size();
		for (int i = TRUEndecaConstants.INT_ZERO ; i < size ; i++) {
			String string = finalBreadCrumbList.get(i);
			breadCrumbBuffer.append(string);
			if(i != size - TRUEndecaConstants.INT_ONE)
			{
				breadCrumbBuffer.append(TRUEndecaConstants.GREATERTHEN);
			}
		}
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUEndecaUtils  :: generateCategoryPrimaryPathBreadcrumb() method ");
		}
		return breadCrumbBuffer.toString();
	}
	
	/**
	 * This method create the breadCrumb string List.
	 * @param pCategoryIdList -- the category Id List
	 * @return breadCrumbList -- the Breadcrumb list
	 */
	public List<String> populatePrimaryCategoryBreadcrumb(List<String> pCategoryIdList)
	{
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUEndecaUtils  :: populatePrimaryCategoryBreadcrumb() method ");
		}
		List<String> breadCrumbList = null;
		if(pCategoryIdList != null && !pCategoryIdList.isEmpty())
		{
			breadCrumbList = new ArrayList<String>();
			for (String categoryId : pCategoryIdList) {
				String displayName;
				try {
					displayName = getCatalogTools().getCategoryDisplayName(categoryId);
					if(StringUtils.isNotBlank(displayName))
					{
						breadCrumbList.add(displayName);
					}
				} catch (RepositoryException repositoryException) {
					
					if(isLoggingError()) {
						vlogError(repositoryException, "RepositoryException occured in TRUEndecaUtils.populatePrimaryCategoryBreadcrumb() method getting displayName");	
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRUEndecaUtils  :: populatePrimaryCategoryBreadcrumb() method ");
		}
		return breadCrumbList;
	}
	
	/**
	 * Gets the max level.
	 *
	 * @return the maxLevel
	 */
	public int getMaxLevel() {
		return mMaxLevel;
	}

	/**
	 * Sets the max level.
	 *
	 * @param pMaxLevel the maxLevel to set
	 */
	public void setMaxLevel(int pMaxLevel) {
		mMaxLevel = pMaxLevel;
	}
	
	/**
	 * @return the chekPrimaryCategory
	 */
	public boolean isChekPrimaryCategory() {
		return mChekPrimaryCategory;
	}

	/**
	 * @param pChekPrimaryCategory the chekPrimaryCategory to set
	 */
	public void setChekPrimaryCategory(boolean pChekPrimaryCategory) {
		mChekPrimaryCategory = pChekPrimaryCategory;
	}



	/**
	 * @return the protocolScheme
	 */
	public String getProtocolScheme() {
		return mProtocolScheme;
	}



	/**
	 * @param pProtocolScheme the protocolScheme to set
	 */
	public void setProtocolScheme(String pProtocolScheme) {
		mProtocolScheme = pProtocolScheme;
	}



	/**
	 * @return the domainUrl
	 */
	public String getDomainUrl() {
		return mDomainUrl;
	}



	/**
	 * @param pDomainUrl the domainUrl to set
	 */
	public void setDomainUrl(String pDomainUrl) {
		mDomainUrl = pDomainUrl;
	}
}
