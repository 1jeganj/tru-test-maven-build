package com.tru.endeca.utils;

import java.util.Comparator;

import com.tru.endeca.vo.BreadCrumbsVO;

/**
 * Class SortCategoryByDisplayOrderComparator.
 * @author PA
 * @version 1.0
 *
 */
public class SortCategoryByDisplayOrderComparator  implements Comparator<Object>{
	
	/**
	 * Compare the category sort order.
	 * @param pArg0 -  Object
	 * @param pArg1 -  Object
	 * @return returns int value
	 */
	public int compare(Object pArg0, Object pArg1) {
		BreadCrumbsVO categoryVo1 = (BreadCrumbsVO) pArg0;
		BreadCrumbsVO categoryVo2 = (BreadCrumbsVO) pArg1;
		Integer displayOrder1 = categoryVo1.getCategoryDisplayOrder();
		Integer displayOrder2 = categoryVo2.getCategoryDisplayOrder();

		return displayOrder1.compareTo(displayOrder2);

	}

}

