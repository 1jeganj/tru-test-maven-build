package com.tru.endeca.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.DimensionList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.Navigation;
import com.tru.cache.TRUEndecaCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.commerce.catalog.vo.ClassificationUtilKeyVO;
import com.tru.commerce.catalog.vo.TRUClassificationMediaVO;
import com.tru.commerce.catalog.vo.TRUClassificationVO;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.model.handler.TRUMegaMenuHandler;
import com.tru.endeca.vo.BreadCrumbsVO;
import com.tru.endeca.vo.MegaMenuVO;
import com.tru.endeca.vo.RequestVO;
import com.tru.utils.TRUClassificationsTools;

/**
 * The Class TRUCategoryTreeUtils.
 *
 * @author Copyright PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL.
 *         Use is subject to license terms. TRUCategoryTreeUtils Utility class
 *         used for Breadcrumbs
 *         @version 1.0
 *         @author Professional Access
 * 
 */
public class TRUCategoryTreeUtils extends GenericService {

	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * holds the default Catalog.
	 */
	private String mCatalog;

	/** Property to hold TRUEndecaCache. */

	private TRUEndecaCache mEndecaCache;

	/**
	 * This property hold reference for TRUMegaMenuHandler.
	 */
	private TRUMegaMenuHandler mMegaMenuHandler;

	/**
	 * This property hold the rootCategoryId.
	 */
	private String mCatalogId;
	/**
	 * This property hold reference for TRUClassificationsTools.
	 */
	private TRUClassificationsTools mClassificationsTools;
	/**
	 * This property hold reference for TRUDimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;

	/**
	 * Gets the mega menu handler.
	 *
	 * @return returns the MegaMenuHandler
	 */
	public TRUMegaMenuHandler getMegaMenuHandler() {
		return mMegaMenuHandler;
	}

	/**
	 * Sets the mega menu handler.
	 *
	 * @param pMegaMenuHandler set the MegaMenuHandler
	 */
	public void setMegaMenuHandler(TRUMegaMenuHandler pMegaMenuHandler) {
		this.mMegaMenuHandler = pMegaMenuHandler;
	}


	/**
	 * Gets the catalog id.
	 *
	 * @return the catalogId
	 */
	public String getCatalogId() {
		return mCatalogId;
	}

	/**
	 * Sets the catalog id.
	 *
	 * @param pCatalogId -- set the catalogId
	 */
	public void setCatalogId(String pCatalogId) {
		this.mCatalogId = pCatalogId;
	}

	/**
	 * Gets the endecaCache.
	 *
	 * @return the endecaCache
	 */
	public TRUEndecaCache getEndecaCache() {
		return mEndecaCache;
	}

	/**
	 * Sets the endecaCache.
	 *
	 * @param pEndecaCache            the endecaCache to set
	 */
	public void setEndecaCache(TRUEndecaCache pEndecaCache) {
		mEndecaCache = pEndecaCache;
	}


	/**
	 * sets the Dimension Value Cache.
	 * 
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * gets Dimension Value Cache.
	 * 
	 * @param pDimensionValueCache
	 *            the dimensionValueCache to set
	 */
	public void setDimensionValueCache(
			TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}


	/**
	 * Gets the catalog.
	 *
	 * @return String , the default catalog
	 */
	public String getCatalog() {
		return mCatalog;
	}

	/**
	 * Sets the catalog.
	 *
	 * @param pCatalog sets the catalog
	 */
	public void setCatalog(String pCatalog) {
		this.mCatalog = pCatalog;
	}

	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations
	 *            - Endeca Configurations
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * Gets the TRUClassificationsTools.
	 *
	 * @return the TRUClassificationsTools
	 */

	public TRUClassificationsTools getClassificationsTools() {
		return mClassificationsTools;
	}

	/**
	 * Sets the TRUClassificationsTools.
	 *
	 * @param pClassificationsTools the TRUClassificationsTools to set
	 */

	public void setClassificationsTools(TRUClassificationsTools pClassificationsTools) {
		mClassificationsTools = pClassificationsTools;
	}

	/**
	 * Checks if is logging error.
	 *
	 * @return the loggingError
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();
	}

	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	public void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}

	/**
	 * Vlog debug.
	 *
	 * @param pMessage the message
	 */
	private void vlogDebug(String pMessage) {
		AssemblerTools.getApplicationLogging().vlogDebug(pMessage);
	}

	/**
	 * Gets the classification map.
	 *
	 * @return the classification map
	 */
	public Map<String, Map<String, Map<String, TRUClassificationVO>>> getClassificationMap()
	{
		if (isLoggingDebug()){
			vlogDebug("BEGIN:: TRUCategoryTreeUtils.getClassificationMap()");
		}
		Map<String, Map<String, Map<String, TRUClassificationVO>>> siteLevelMap = null;
		if (isLoggingDebug()){
			vlogDebug("END:: TRUCategoryTreeUtils.getClassificationMap():: SiteLevelMap: {0}",siteLevelMap);
		}
		if (isLoggingDebug()){
			vlogDebug("END:: TRUCategoryTreeUtils.getClassificationMap()");
		}
		return siteLevelMap;
	}

	/**
	 * Gets the all root classifications.
	 *
	 * @param pSiteClassificationMap the site classification map
	 * @param pSiteId the site id
	 * @return the all root classifications
	 */
	public Map<String, Map<String, String>> getAllRootClassifications(Map<String, Map<String, Map<String, TRUClassificationVO>>> pSiteClassificationMap,String pSiteId)
	{
		if (isLoggingDebug()){
			vlogDebug("BEGIN:: TRUCategoryTreeUtils.getAllRootClassifications()");
			vlogDebug(" TRUCategoryTreeUtils.getAllRootClassifications() :: SiteClassificationMap : {0} SiteId {1} ",pSiteClassificationMap,pSiteId);
		}

		Map<String, Map<String, String>> rootClassifications = null;
		Map<String, String> rootCategory = null;
		Map<String, TRUClassificationVO> parentClassificationMap = null;
		rootClassifications = new HashMap<String, Map<String, String>>();
		if(pSiteClassificationMap != null && !pSiteClassificationMap.isEmpty() && !StringUtils.isBlank(pSiteId)) {
			parentClassificationMap = (Map<String, TRUClassificationVO>)pSiteClassificationMap.get(pSiteId)
					.get(TRUEndecaConstants.PARENT_CLASSIFICATION_MAP);
		}
		if(parentClassificationMap !=null && !parentClassificationMap.isEmpty()){
			for (Map.Entry<String,TRUClassificationVO> entry : parentClassificationMap.entrySet()) {
				TRUClassificationVO categoryVO = entry.getValue();
				rootCategory = new HashMap<String, String>();
				rootCategory.put(categoryVO.getClaasificationName(), categoryVO.getClaasificationName());
				rootClassifications.put(categoryVO.getClaasificationName(), rootCategory);
			}
		}

		if (isLoggingDebug()){
			vlogDebug("END:: TRUCategoryTreeUtils.getAllRootClassifications()");
		}
		return rootClassifications;
	}

	/**
	 * Populate related media image.
	 *
	 * @param pRelatedMediaContent the related media content
	 * @param pImageKey the image key
	 * @return the string
	 */
	public String populateRelatedMedia(Map<String,TRUClassificationMediaVO> pRelatedMediaContent,String pImageKey)
	{
		if (isLoggingDebug()){
			vlogDebug("BEGIN:: TRUCategoryTreeUtils.populateRelatedMedia()");
			vlogDebug(" TRUCategoryTreeUtils.populateRelatedMedia() :: RelatedMediaContent : {0} ",pRelatedMediaContent);
		}
		String mediaImageUrl = null;
		String htmlBannerContent = null;
		TRUClassificationMediaVO classificationMediaVO = null;
		if(!pRelatedMediaContent.isEmpty() && !StringUtils.isBlank(pImageKey) && pRelatedMediaContent.containsKey(pImageKey) ) {
			classificationMediaVO = (TRUClassificationMediaVO)pRelatedMediaContent.get(pImageKey);
			if(classificationMediaVO != null ) {
				if(pImageKey.equals(getEndecaConfigurations().getBannerImageKey()))	{
					htmlBannerContent = classificationMediaVO.getMediaHTMLContent();
					return htmlBannerContent;
				}
				else if(pImageKey.equals(getEndecaConfigurations().getCategoryImageKey())) {
					if(classificationMediaVO.getMediaHTMLContent() != null && !classificationMediaVO.getMediaHTMLContent().isEmpty()){
						htmlBannerContent = classificationMediaVO.getMediaHTMLContent();
						return htmlBannerContent;
					}
				}
				else {
					mediaImageUrl = classificationMediaVO.getMediaURL();
				}
			}


		}
		if (isLoggingDebug()){
			vlogDebug("END:: TRUCategoryTreeUtils.populateRelatedMedia()");
		}
		return mediaImageUrl;

	}


	/**
	 * Gets the mega menu vo.
	 *
	 * @return returns the Megamenu Object
	 */
	public MegaMenuVO getMegaMenuVO() {

		if(isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUCategoryTreeUtils.getMegaMenuVO() method:: ");
		}
		MegaMenuVO megaMenuVO = null;
		RequestVO megaMenuRequest = new RequestVO();
		Site currentSite = SiteContextManager.getCurrentSite();

		RepositoryItem catalog = (RepositoryItem) currentSite.getPropertyValue(getCatalog());
		if(isLoggingDebug()) {
			vlogDebug("TRUCategoryTreeUtils.getMegaMenuVO() method::currentSite {0} and Catalog is : {1}",currentSite, catalog);
		}
		if (catalog != null) {
			megaMenuRequest.setCatalogId(catalog.getRepositoryId());
		}
		megaMenuRequest.setLocale(TRUEndecaConstants.LOCALE);
		megaMenuRequest.setRequestFor(TRUCommerceConstants.MEGA_MENU);

		if (megaMenuVO == null && currentSite != null) {
			megaMenuVO = (MegaMenuVO) getMegaMenuHandler().createMegaMenuObject(currentSite.toString(), getMegaMenuHandler().getMaximumCategoryLevelForMegaMenu());
		}
		if(isLoggingDebug()) {
			vlogDebug("===END===:: TRUCategoryTreeUtils.getMegaMenuVO() method:: ");
		}

		return megaMenuVO;
	}

	/**
	 * Gets the all root categories.
	 *
	 * @param pMegaMenuVO            receives MegaMenuVO as parameter
	 * @return HashMap of all root level classifications
	 */
	public Map<String, Map<String, String>> getAllRootCategories(MegaMenuVO pMegaMenuVO) {

		if(isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUCategoryTreeUtils.getAllRootCategories(MegaMenuVO pMegaMenuVO) method:: ");
		}
		List<CategoryVO> rootCategory = null;
		if(pMegaMenuVO != null)	{
			rootCategory = pMegaMenuVO.getRootCategories();
		}
		if(isLoggingDebug()) {
			vlogDebug("TRUCategoryTreeUtils.getAllRootCategories() method::rootCategory {0}",rootCategory);
		}
		Map<String, Map<String, String>> rootClassifications = new HashMap<String, Map<String, String>>();
		Map<String,String> rootCategoryMap = null;
		if (rootCategory != null && !rootCategory.isEmpty()) {
			for (CategoryVO categoryVO : rootCategory) {
				if(categoryVO != null) {
					rootCategoryMap = new HashMap<String,String>();
					rootCategoryMap.put(categoryVO.getCategoryName(), categoryVO.getCategoryURL());
					rootClassifications.put(categoryVO.getCategoryName(),rootCategoryMap);
				}
			}
			if(isLoggingDebug()) {
				vlogDebug("TRUCategoryTreeUtils.getAllRootCategories() method::rootCategoryMap {0} ",rootCategoryMap);
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("===END===:: TRUCategoryTreeUtils.getAllRootCategories(MegaMenuVO pMegaMenuVO) method:: ");
		}
		return rootClassifications;
	}

	/**
	 * Gets the all sub categories.
	 *
	 * @param pMegaMenuVO            , input parameter MegaMenuVO object
	 * @param pCategoryId            root classification ID
	 * @return breadCrumbsVO BreadCrumbsVO the Breadcrumb
	 */
	public BreadCrumbsVO getAllSubCategories(MegaMenuVO pMegaMenuVO, String pCategoryId) {

		if(isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUCategoryTreeUtils.getAllSubCategories(MegaMenuVO pMegaMenuVO,String pCategoryId) method:: ");
		}

		List<CategoryVO> rootCategory = null;
		BreadCrumbsVO breadCrumbsVO = new BreadCrumbsVO();
		if (pCategoryId != null && !pCategoryId.isEmpty()) {
			breadCrumbsVO.setSelectedRootCategory(pCategoryId);
		}
		if(pMegaMenuVO != null)	{
			rootCategory = pMegaMenuVO.getRootCategories();
			if(isLoggingDebug()) {
				vlogDebug("TRUCategoryTreeUtils.getAllRootCategories() method::rootCategory  {0}",rootCategory);
			}
		}
		if (rootCategory != null && !rootCategory.isEmpty()) {
			for (CategoryVO categoryVO : rootCategory) {
				if(categoryVO != null && pCategoryId.equals(categoryVO.getCategoryName()))
				{
					List<CategoryVO> subCategory = categoryVO.getSubCategories();
					breadCrumbsVO.setCategoryURL(categoryVO.getCategoryURL());
					breadCrumbsVO.setSubCategories(subCategory);
					breadCrumbsVO.setCategoryId(categoryVO.getCategoryId());
					breadCrumbsVO.setCategoryImage(categoryVO.getCategoryImage());
				}
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("===END===:: TRUCategoryTreeUtils.getAllSubCategories(MegaMenuVO pMegaMenuVO,String pCategoryId) method:: ");
		}
		return breadCrumbsVO;
	}

	/**
	 * Create the ClassificationUtilKeyVO to get the data from Classification Map.
	 *
	 * @param pClassificationId the classification id
	 * @param pLevel the level
	 * @param pAncestorId the ancestor id
	 * @return ClassificationUtilKeyVO
	 */
	public ClassificationUtilKeyVO getClassificationKey(String pClassificationId, int pLevel, String pAncestorId){

		if(isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUCategoryTreeUtils.getClassificationKey() method:: ");
		}
		ClassificationUtilKeyVO classificationKey = new ClassificationUtilKeyVO();
		classificationKey.setClassificationId(pClassificationId);
		String currentSiteId = SiteContextManager.getCurrentSiteId();
		
		if(StringUtils.isNotEmpty(currentSiteId)) {
			String rootCategoryId = getClassificationsTools().getRootCategoryIdForSite(currentSiteId);
			if(StringUtils.isNotEmpty(rootCategoryId)) {
				classificationKey.setRootCategoryId(rootCategoryId);
				if(StringUtils.isNotEmpty(pAncestorId)) {
					classificationKey.setParentClassificationId(pAncestorId);
				} else {
					classificationKey.setParentClassificationId(rootCategoryId);
				}
			}
		}
		classificationKey.setLevel(pLevel);

		if(isLoggingDebug()) {
			vlogDebug("===END===:: TRUCategoryTreeUtils.getClassificationKey() method:: ");
		}
		return classificationKey;
	}

	/**
	 * Gets the TRUClassificationVO object from Classification Map by passing ClassificationUtilKeyVO as key.
	 *
	 * @param pClassificationKey the classification key
	 * @return the classification vo value
	 */
	public TRUClassificationVO getClassificationVOValue(ClassificationUtilKeyVO pClassificationKey) {

		if(isLoggingDebug()) {
			vlogDebug("===BEGIN===:: TRUCategoryTreeUtils.getClassificationVOValue() method:: ");
		}
		Map<ClassificationUtilKeyVO, TRUClassificationVO> allClassifications = getClassificationsTools().getAllClassificationMap();
		TRUClassificationVO classificationVO = null;
		if(pClassificationKey != null && allClassifications != null) {
			classificationVO = allClassifications.get(pClassificationKey);
		}
		if(isLoggingDebug()) {
			vlogDebug("===END===:: TRUCategoryTreeUtils.getClassificationVOValue() method:: ");
		}
		return classificationVO;
	}

	/**
	 * Identifies Cache Key and get the URl from Dimension Value cache based on
	 * the provided key.
	 * 
	 * @param pClassificationId
	 *            - Cache Key
	 * @return - URL String
	 */
	public String getURL(String pClassificationId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryTreeUtils.getURL method..");
		}
		String url = null;
		DimensionValueCacheObject cacheObject = null;
		if (pClassificationId != null) {
			cacheObject = getDimensionValueCache().getCacheObject(pClassificationId);
		}
		if (cacheObject != null) {
			url = cacheObject.getUrl();
			if (url != null && !StringUtils.isBlank(url)) {
				if (isLoggingDebug()) {vlogDebug("URL for Dimension Value Cache # {0}", url);
				}
				return url;
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("No URL found in Dimension Value Cache  for Category# {0}",pClassificationId);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUCategoryTreeUtils.getURL method.. URL Is {0}",url);
		}
		return url;
	}
	/**
	 * This method return the Classification object for the current navigation.
	 * 
	 * @param pContentItem ContentItem
	 * @param pResults ENEQueryResults
	 * @return TRUClassificationVO
	 */
	public TRUClassificationVO getClassificationVOForCurrentNavigation(ContentItem pContentItem, ENEQueryResults pResults){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCategoryTreeUtils.getClassificationVOForCurrentNavigation method..");
		}
		TRUClassificationVO classificationVo = null;
		if(pResults != null && pResults.getNavigation() != null) {
			Navigation navigation = pResults.getNavigation();
			if(navigation.getDescriptorDimensions() == null) {
				return classificationVo;
			}
			DimensionList dimList = navigation.getDescriptorDimensions();
			if(dimList == null || dimList.isEmpty()) {
				return classificationVo;
			}
			Dimension dimension = dimList.getDimension(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME);
			if(dimension == null || dimension.getAncestors() == null || dimension.getAncestors().isEmpty()) { 
				return classificationVo;
			}
			
			int level = 0;
            String ancestorId = null;
            DimValList ancestors = dimension.getAncestors();
            if(ancestors != null) {
                  level = ancestors.size();
                  if(level > 0) {
                         DimVal ancestor = (DimVal) ancestors.get(level - TRUEndecaConstants.INT_ONE);
                         if(ancestor != null) {
                                ancestorId = (String) ancestor.getProperties().get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
                         }
                  }
            }

			DimVal dimval = dimension.getDescriptor();
			if(dimval != null) {
				String categoryId = (String) dimval.getProperties().get(TRUEndecaConstants.CATEGORY_REPOSITORYID);
				pContentItem.put(TRUEndecaConstants.CURRENT_CAT_ID, categoryId);
				if(!StringUtils.isEmpty(categoryId)) {
					ClassificationUtilKeyVO classificationKey = getClassificationKey(categoryId, level, ancestorId);
					classificationVo = getClassificationVOValue(classificationKey);
				}
			}
			if (isLoggingDebug()) {
				logDebug("END:: TRUCategoryTreeUtils.getClassificationVOForCurrentNavigation method..");
			}
		}
	return classificationVo;
}

/**
 * This method get the Classification HTML content form Related media content.
 * @param pRelatedMedia the related media
 * @param pKey the String
 * @return the html content
 */
public Object getCollectionMediaContentt(Map<String,TRUClassificationMediaVO> pRelatedMedia, String pKey){
	if (isLoggingDebug()){
		vlogDebug("BEGIN::  TRUCategoryTreeUtils.getCollectionMediaContentt() method");
	}
	String htmlContent = null;
	if(pRelatedMedia != null && !pRelatedMedia.isEmpty()){
		TRUClassificationMediaVO mediaVo =  pRelatedMedia.get(pKey);
		if(mediaVo != null){
			htmlContent = mediaVo.getMediaHTMLContent();
		}
	}
	if (isLoggingDebug()){
		vlogDebug("End::  TRUCategoryTreeUtils.getCollectionMediaContentt() method");
	}
	return htmlContent;
}
}