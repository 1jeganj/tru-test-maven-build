package com.tru.endeca.urlformatter;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;

import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoUrlFormatter;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * SeoUrlFormatter will be managed to parse the input parameters and send the UrlState.
 *  @version 1.0
 *  @author PA
 */

public class TRUSeoUrlFormatter extends SeoUrlFormatter {


	/**
	 * This property hold reference for CatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;

	/**
	 * This property hold reference for CatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * Property Holds endecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Holds the reference to ProductCatalogRepository.
	 */
	private Repository mProductCatalogRepository;

	/**
	 * Holds the reference to OnlinePIDProductsRQLQuery.
	 */
	private String mOnlinePIDProductsRQLQuery;

	/**
	 * Property to mSkuType.
	 */
	private String mSkuType;

	/**
	 * Gets the catalogTools.
	 * 
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalogTools.
	 * 
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * Returns the this property hold reference for CatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the this property hold reference for CatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * Returns Endeca Configurations.
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * sets Endeca Configurations.
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}


	/**
	 * Returns the reference to ProductCatalogRepository.
	 * 
	 * @return the ProductCatalogRepository
	 */
	public Repository getProductCatalogRepository() {
		return mProductCatalogRepository;
	}

	/**
	 * Sets the reference of ProductCatalogRepository.
	 * 
	 * @param pProductCatalogRepository
	 *            the ProductCatalogRepository to set
	 */
	public void setProductCatalogRepository(Repository pProductCatalogRepository) {
		mProductCatalogRepository = pProductCatalogRepository;
	}

	/**
	 * Returns the reference to OnlinePIDProductsRQLQuery.
	 * 
	 * @return the OnlinePIDProductsRQLQuery
	 */
	public String getOnlinePIDProductsRQLQuery() {
		return mOnlinePIDProductsRQLQuery;
	}

	/**
	 * Sets the reference of OnlinePIDProductsRQLQuery.
	 * 
	 * @param pOnlinePIDProductsRQLQuery
	 *            the OnlinePIDProductsRQLQuery to set
	 */
	public void setOnlinePIDProductsRQLQuery(String pOnlinePIDProductsRQLQuery) {
		mOnlinePIDProductsRQLQuery = pOnlinePIDProductsRQLQuery;
	}

	/**
	 * Gets the sku type.
	 *
	 * @return the skuType
	 */
	public String getSkuType() {
		return mSkuType;
	}

	/**
	 * Sets the sku type.
	 *
	 * @param pSkuType the skuType to set
	 */
	public void setSkuType(String pSkuType) {
		mSkuType = pSkuType;
	}

	/**
	 * Parses the HttpServletResuest and provides the Endeca Url State based on the given parameters.
	 * 
	 * @param pRequest - HttpServletResuest
	 * @return UrlState - Parsed HttpResponse UrlState object
	 * @throws UrlFormatException - Endeca Exception
	 * 
	 */

	@Override
	public UrlState parseRequest(HttpServletRequest pRequest) throws UrlFormatException {
		StringBuffer servletPath = new StringBuffer(pRequest.getServletPath());

		if (pRequest.getPathInfo() != null) {
			// Get servlet Path
			servletPath = servletPath.append(pRequest.getPathInfo());
		}
		return parseRequest(pRequest.getQueryString(), servletPath.toString(), pRequest.getCharacterEncoding());
	}

	/**
	 * This method is used to format the url.
	 * @param pUrlState - UrlState
	 * @return formatUrl - String
	 * @throws UrlFormatException - UrlFormatException
	 */
	@Override
	public String formatUrl(UrlState pUrlState) throws UrlFormatException {
		List<String> urlParametersToRemove = getEndecaConfigurations().getUrlParameterRemoveList();
		if(urlParametersToRemove != null && !urlParametersToRemove.isEmpty()) {
			for(String removeParameter : urlParametersToRemove) {
				pUrlState.removeParam(removeParameter);
			}
		}
		return super.formatUrl(pUrlState);
	}

	/**
	 * This Method is used for parse query string based on URL query parameters.
	 * 
	 * @param pQueryString - Query String from the url
	 * @param pPathInfo - Url path information
	 * @param pCharacterEncoding - Character encoding to be used for url
	 * @return UrlState - Parsed HttpResponse UrlState object
	 * @throws UrlFormatException - Endeca Exception
	 * 
	 */

	@Override
	public UrlState parseRequest(String pQueryString, String pPathInfo, String pCharacterEncoding)
			throws UrlFormatException {
		// Get Current Request
		HttpServletRequest request = ServletUtil.getCurrentRequest();
		StringBuffer servletPath = new StringBuffer(request.getServletPath());
		if (request.getPathInfo() != null) {// Null check for path Info
			servletPath = servletPath.append(request.getPathInfo());
		}

		UrlState urlState = new UrlState(this, pCharacterEncoding);
		parseQueryString(urlState, pQueryString);// Parse Query String
		parsePathInfo(urlState, servletPath.toString());// Parse Path Info
		if(getEndecaConfigurations().isFormatSEOPDPUrl()) {
			if(isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug(
						"The FormatSEOPDPUrl is set to True. So Parse the PDP Url");
			}
			updateAggregateRecordParam(urlState);
		}
		return urlState;
	}


	/**
	 * Formatting URL for record to query to the MDex.
	 * 
	 * @param pUrlState
	 * 			- Url State object
	 */
	private void updateAggregateRecordParam(UrlState pUrlState) {
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"====BEGIN:=====Inside TRUSeoUrlFormatter.updateAggregateRecordParam() method");
		}
		String pdpUrl=null;
		if (pUrlState.containsAggrERec()) {
			String productId = pUrlState.getParam(getAggrERecParamKey());
			String requestUri = ServletUtil.getCurrentRequest().getRequestURI();
			if(!StringUtils.isEmpty(productId)) {
				if(!StringUtils.isEmpty(requestUri) && requestUri.equals(getEndecaConfigurations().getCollectionPageUrl())) {
					pdpUrl = createCollectionPageUrl(productId);
				} else {
					pdpUrl = getCatalogTools().createPDPUrlFromOnlinePid(productId);
				}

				if(!StringUtils.isEmpty(pdpUrl)) {
					pUrlState.setParam(getAggrERecParamKey(), pdpUrl);
				}
			}
			if (isLoggingDebug()) { 
				AssemblerTools.getApplicationLogging().vlogDebug(
						"Inside updateAggregateRecordParam() method of TRUSeoUrlFormatter class : product URL is"+pdpUrl);
			}
		}
		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"======END: ===== Inside TRUSeoUrlFormatter.updateAggregateRecordParam() method");
		}
	}


	/**
	 * The method createCollectionPageUrl will create the collection page url from productId.
	 * 
	 * @param pProductId - product id
	 * @return pdpUrl pdp URL
	 */
	@SuppressWarnings("unchecked")
	public String createCollectionPageUrl(String pProductId) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"======BEGIN: ===== Inside TRUSeoUrlFormatter.createCollectionPageUrl() method");
		}
		String pdpUrl = null;
		try {
			RepositoryItem product = getCatalogTools().findProduct(pProductId);
			if(product != null) {
				List<RepositoryItem> childSkus = (List<RepositoryItem>) product.getPropertyValue(getCatalogProperties().getChildSKUsPropertyName());
				if(childSkus == null || childSkus.isEmpty()) {
					return pdpUrl;
				}
				StringBuffer productURL = new StringBuffer();
				RepositoryItem childSku = getActiveSku(childSkus);
				if(childSku == null) {
					return pdpUrl;
				}
				String skuId = childSku.getRepositoryId();
				String catalogId = null;
				String skuType = (String) childSku.getPropertyValue(getCatalogProperties().getSkuType());
				Set<RepositoryItem> skuComputedCatalogs = (Set<RepositoryItem>) childSku.getPropertyValue(getCatalogProperties()
						.getComputedCatalogsPropertyName());
				Set<RepositoryItem> collectionComputedCatalogs = (Set<RepositoryItem>) product.getPropertyValue(getCatalogProperties()
						.getComputedCatalogsPropertyName());

				Set<RepositoryItem> computedCatalogs = null;
				
				if (collectionComputedCatalogs != null && !collectionComputedCatalogs.isEmpty()) {
					computedCatalogs = collectionComputedCatalogs;
				} else if(skuComputedCatalogs != null && !skuComputedCatalogs.isEmpty()) {
					computedCatalogs = skuComputedCatalogs;
				}
				
				if (collectionComputedCatalogs != null && !collectionComputedCatalogs.isEmpty()) {
					computedCatalogs = collectionComputedCatalogs;
				} else if(skuComputedCatalogs != null && !skuComputedCatalogs.isEmpty()) {
					computedCatalogs = skuComputedCatalogs;
				} 
				if(computedCatalogs != null && !computedCatalogs.isEmpty()) {
					for (RepositoryItem catalog : computedCatalogs) {
						if (catalog != null) {
							catalogId = catalog.getRepositoryId();
							break;
						}
					}
				}
				productURL.append(skuType).append(TRUCommerceConstants.HYPHEN).append(skuId).append(TRUCommerceConstants.DOUBLE_DOT).append(pProductId);
				productURL.append(TRUCommerceConstants.DOT).append(catalogId).append(TRUCommerceConstants.DOT).append(TRUCommerceConstants.DEFAULT_SITE_LOCALE);

				if(productURL != null && productURL.length() > 0) {
					pdpUrl = productURL.toString();
				}
			}
		} catch (RepositoryException repositoryException) {
			if(isLoggingError()) {
				vlogError(repositoryException, "RepositoryException occured in TRUSeoUrlFormatter.createCollectionPageUrl() method");
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"======END: ===== Inside TRUSeoUrlFormatter.createCollectionPageUrl() method");
		}

		return pdpUrl;
	}

	/**
	 * The method will return the Active sku from the list of childSkus.
	 * 
	 * @param pChildSkus - list of child SKU
	 * @return activeChildSku return active child sku
	 */
	private RepositoryItem getActiveSku(List<RepositoryItem> pChildSkus) {

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"======BEGIN: ===== Inside TRUSeoUrlFormatter.getActiveSku() method");
		}
		RepositoryItem activeChildSku = null;
		if(pChildSkus == null || pChildSkus.isEmpty()) {
			return null;
		}

		for(RepositoryItem childSku : pChildSkus) {
			if(childSku == null) {
				continue;
			}
			boolean isDeletedFlag =	(boolean)childSku.getPropertyValue((getCatalogProperties()).getIsSkuDeleted());
			boolean isWebDisplayFlag = (boolean)childSku.getPropertyValue((getCatalogProperties()).getWebDisplayFlag());
			String onlinePID = (String) childSku.getPropertyValue((getCatalogProperties()).getOnlinePID());
			boolean superDisplayFlag = false;
			if(childSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag()) == null) {
				superDisplayFlag = true;
			} else {
				superDisplayFlag = (boolean) childSku.getPropertyValue(getCatalogProperties().getSuperDisplayFlag());
			}
			if(!isDeletedFlag && isWebDisplayFlag && !StringUtils.isEmpty(onlinePID) && superDisplayFlag) {
				activeChildSku = childSku;
				if (isLoggingDebug()) {
					AssemblerTools.getApplicationLogging().vlogDebug(
							"Active Sku Id : {0}", activeChildSku.getRepositoryId());
				}
				break;
			}
		}

		if (isLoggingDebug()) {
			AssemblerTools.getApplicationLogging().vlogDebug(
					"======END: ===== Inside TRUSeoUrlFormatter.getActiveSku() method");
		}
		return activeChildSku;

	}

	/**
	 * Checks if is logging debug.
	 * 
	 * @return the loggingDebug
	 */
	public boolean isLoggingDebug() {
		return AssemblerTools.getApplicationLogging().isLoggingDebug();

	}

	/**
	 * Checks if is logging error.
	 * 
	 * @return the loggingError
	 */
	public boolean isLoggingError() {
		return AssemblerTools.getApplicationLogging().isLoggingError();

	}

	/**
	 * Log the error messages using the AssemblerTools logging.
	 *
	 * @param pThrowable            the exception object
	 * @param pMessage            the custom message
	 */
	private void vlogError(Throwable pThrowable, String pMessage) {
		AssemblerTools.getApplicationLogging().vlogError(pThrowable,pMessage);
	}

}