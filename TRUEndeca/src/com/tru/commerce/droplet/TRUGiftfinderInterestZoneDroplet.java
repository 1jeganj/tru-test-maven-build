package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;

/**
 * This class TRUGiftfinderInterestZoneDroplet.
 * @version 1.0
 * @author PA
 * 
 */
public class TRUGiftfinderInterestZoneDroplet extends DynamoServlet {
	
	/**
	 * Property to hold CatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;

	/**
	 * Gets the catalog properties.
	 *
	 * @return the mCatalogProperties.
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	
	/**
	 * Sets the catalog properties.
	 *
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}

	
	/**
	 * Service.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see atg.servlet.DynamoServlet#service(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		Site siteItem = SiteContextManager.getCurrentSite();
		List<RepositoryItem> giftFinderStackList = null;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if(siteItem != null) {
			giftFinderStackList = (List<RepositoryItem>) siteItem.getPropertyValue(catalogProperties.getGiftFinderStackListPropertyName());
			if(giftFinderStackList != null && !giftFinderStackList.isEmpty()) {
				pRequest.setParameter(catalogProperties.getGiftFinderStackListPropertyName(), giftFinderStackList);
				pRequest.serviceLocalParameter(TRUEndecaConstants.OUTPUT_PARAM, pRequest, pResponse);
			} else {
				pRequest.serviceLocalParameter(TRUEndecaConstants.EMPTY_PARAM, pRequest, pResponse);
			}
		}
	}
}