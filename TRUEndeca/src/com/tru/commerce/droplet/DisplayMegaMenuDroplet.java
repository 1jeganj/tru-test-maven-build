package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.cache.TRUEndecaCache;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.commerce.exception.TRUEndecaException;
import com.tru.common.TRUConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.model.handler.TRUMegaMenuHandler;
import com.tru.endeca.utils.SortByDisplayOrderComparator;
import com.tru.endeca.vo.MegaMenuVO;
import com.tru.endeca.vo.RequestVO;
import com.tru.integrations.TRUIntegrationPropertiesConfig;


/**
 * This class is intended for querying the MEGA MENU. This service uses OOTB service Cache
 * for caching mega menu objects retrieved from ENDECA. This service requires catalogId and locale input
 * parameters to be passed by user, 'elementName' is not a required parameter.
 * @version 1.0
 * @author Professional Access
 * 
 */
public class DisplayMegaMenuDroplet  extends DynamoServlet {
	
	/**
	 * Property to hold SortByDisplayOrderComparator.
	 */
	private SortByDisplayOrderComparator mDisplayOrderComparator;
	
	/**
	 * Property to hold TRUMegaMenuHandler.
	 */
	private TRUMegaMenuHandler mTRUMegaMenuHandler;
	
	/**
	 * Property to hold TRUEndecaCache.
	 */
	private TRUEndecaCache mEndecaCache;
	
	/**
	 * This property hold reference for TRUConfiguration.
	 */
	private TRUConfiguration mConfiguration;
	
	/** The m trusos integration properties config. */
	private TRUIntegrationPropertiesConfig mTRUIntegrationPropertiesConfig;
    
	/**  Holds the mCatalogProperties */
	private TRUCatalogProperties mCatalogProperties;
	/**
	 * @return the displayOrderComparator.
	 */
	public SortByDisplayOrderComparator getDisplayOrderComparator() {
		return mDisplayOrderComparator;
	}
	
	/**
	 * @param pDisplayOrderComparator the displayOrderComparator to set.
	 */
	public void setDisplayOrderComparator(
			SortByDisplayOrderComparator pDisplayOrderComparator) {
		mDisplayOrderComparator = pDisplayOrderComparator;
	}

	/**
	 * Gets the endecaCache.
	 * 
	 * @return the endecaCache
	 */
	public TRUEndecaCache getEndecaCache() {
		return mEndecaCache;
	}

	/**
	 * Sets the endecaCache.
	 * 
	 * @param pEndecaCache the endecaCache to set.
	 */
	public void setEndecaCache(TRUEndecaCache pEndecaCache) {
		mEndecaCache = pEndecaCache;
	}
	
	/**
	 * Gets the TRUMegaMenuHandler.
	 * 
	 * @return the TRUMegaMenuHandler
	 */
	
	public TRUMegaMenuHandler getTRUMegaMenuHandler() {
		return mTRUMegaMenuHandler;
	}
	
	/**
	 * Sets the TRUMegaMenuHandler.
	 * 
	 * @param pTRUMegaMenuHandler the TRUMegaMenuHandler to set.
	 */
	
	public void setTRUMegaMenuHandler(TRUMegaMenuHandler pTRUMegaMenuHandler) {
		mTRUMegaMenuHandler = pTRUMegaMenuHandler;
	}

   /**
	 * This method is used to get MegamenuContent.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUDisplayMegaMenuDroplet.service method..");
		}
		String catalogId = (String) pRequest.getLocalParameter(TRUCommerceConstants.CATALOG_ID);
		String siteId = (String) pRequest.getLocalParameter(TRUCommerceConstants.SITE_ID);
		String locale = (String) pRequest.getLocalParameter(TRUCommerceConstants.LOCALE);
		String elementName = (String) pRequest.getLocalParameter(TRUCommerceConstants.ELEMENT_NAME);
		List<CategoryVO> rooCatList = null;
		List<CategoryVO> remainingRooCatList = new ArrayList<CategoryVO>();
		List<CategoryVO> maxRootCategorys=new ArrayList<CategoryVO>();
		boolean displaySeeMoreCategory = false;
		if (!StringUtils.isBlank(catalogId)) {
			if (StringUtils.isBlank(elementName)) {
				elementName = TRUCommerceConstants.ELEMENT;
			}
			RequestVO megaMenuRequest = new RequestVO();
			megaMenuRequest.setCatalogId(catalogId);
			megaMenuRequest.setSiteId(siteId);
			megaMenuRequest.setLocale(locale);
			megaMenuRequest.setRequestFor(TRUCommerceConstants.MEGA_MENU);
			/* 
			 * Identify whether it is SOS request or not
			 */
			Site site = SiteContextManager.getCurrentSite();
			if(site != null){
				vlogDebug("Found site {0}", new Object[] { site.getId() });
				String[] siteUrls = (String[]) site.getPropertyValue(getCatalogProperties().getAdditionalProductionURLs());
				List<String> sosConfiguredUrl = getTRUIntegrationPropertiesConfig().getSosConfiguredURLs();
				if (siteUrls != null && siteUrls.length > 0) {
					if (getConfiguration().isEnableSOS() && siteUrls != null
							&& !StringUtils.isBlank(pRequest.getServerName()) && sosConfiguredUrl != null &&
							!sosConfiguredUrl.isEmpty() && (sosConfiguredUrl.contains(pRequest.getServerName()))) {
							megaMenuRequest.setRequestFrom(TRUCommerceConstants.SOS);
					} else {
						megaMenuRequest.setRequestFrom(TRUCommerceConstants.STORE);
					}

					} 
				}
			try {
				MegaMenuVO megaMenuVO = (MegaMenuVO) getEndecaCache().get(megaMenuRequest);
				if (isLoggingDebug()) {
					logDebug("TRUDisplayMegaMenuDroplet.service method..megaMenuVO "+megaMenuVO);
				}
				if (megaMenuVO != null && megaMenuVO.getRootCategories() != null) {
					//Begin Sorting L1 Category Hierarchy
					rooCatList = megaMenuVO.getRootCategories();
					//to avoid ConcurrentModificationException
					CopyOnWriteArrayList<CategoryVO> copyOnWriteArrayList = new CopyOnWriteArrayList<CategoryVO>(rooCatList);
					
					if(copyOnWriteArrayList != null && !copyOnWriteArrayList.isEmpty()){
						Collections.sort(copyOnWriteArrayList,getDisplayOrderComparator());	
					}
					//End Sorting L1 Category Hierarchy
					if (getTRUMegaMenuHandler().MegaMenuMaxAllowedRootCategories() <= rooCatList.size()) {
						int megaMenuMaxAllowed = getTRUMegaMenuHandler().MegaMenuMaxAllowedRootCategories();
						int rootCatListSize = rooCatList.size();
						for (int i = megaMenuMaxAllowed; i < rootCatListSize; i++) {
							remainingRooCatList.add(rooCatList.get(i));
							displaySeeMoreCategory = true;
						}
						
						for (int i = 0; i < megaMenuMaxAllowed; i++) {
							maxRootCategorys.add(rooCatList.get(i));
							//displaySeeMoreCategory = true;
						}

					}
					pRequest.setParameter(TRUEndecaConstants.REMAINING_CATEGORIES,remainingRooCatList);
					pRequest.setParameter(TRUEndecaConstants.DISPLAY_SEE_MORE_CATEGORIES,displaySeeMoreCategory);
					if (displaySeeMoreCategory) {
						pRequest.setParameter(elementName, maxRootCategorys);
					} else {
						pRequest.setParameter(elementName, rooCatList);
					}
					
					pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
				} else {
					pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
				}

			} catch (TRUEndecaException endecaException) {
				if (isLoggingError()) {
					logError("TRUEndecaException in TRUDisplayMegaMenuDroplet.service method ",
							endecaException);
				}
			} catch (RepositoryException repositoryException) {
				// TODO Auto-generated catch block
				if (isLoggingError()) {
					logError("RepositoryException in TRUDisplayMegaMenuDroplet.service method ",
							repositoryException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUDisplayMegaMenuDroplet.service method..");
		}
	}

/**
 * @return the configuration
 */
public TRUConfiguration getConfiguration() {
	return mConfiguration;
}

/**
 * @param pConfiguration the configuration to set
 */
public void setConfiguration(TRUConfiguration pConfiguration) {
	mConfiguration = pConfiguration;
}

/**
 * @return the tRUIntegrationPropertiesConfig
 */
public TRUIntegrationPropertiesConfig getTRUIntegrationPropertiesConfig() {
	return mTRUIntegrationPropertiesConfig;
}

/**
 * @param pTRUIntegrationPropertiesConfig the tRUIntegrationPropertiesConfig to set
 */
public void setTRUIntegrationPropertiesConfig(
		TRUIntegrationPropertiesConfig pTRUIntegrationPropertiesConfig) {
	mTRUIntegrationPropertiesConfig = pTRUIntegrationPropertiesConfig;
}

/**
 * @return the catalogProperties
 */
public TRUCatalogProperties getCatalogProperties() {
	return mCatalogProperties;
}

/**
 * @param pCatalogProperties the catalogProperties to set
 */
public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
	mCatalogProperties = pCatalogProperties;
}
}
