package com.tru.commerce.endeca.cache;

import java.util.List;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;


/**
 * The TRUDimensionValueCacheTools class overrides the OOTB  DimensionValueCacheTools, and is used to fetch 
 * Endeca Dimension value Id's for a give repository Id's and vice versa.
 * This Class will provide the dimension value ids and URL for corresponding ATG category and Classifications items.
 * @version 1.0
 * @author Professional Access
 */
public class TRUDimensionValueCacheTools extends DimensionValueCacheTools{
	
	/**
	 * gives the DimensionValueCacheObject based on cache key.
	 * @param pCacheKey - cacheKey
	 * @return List<DimensionValueCacheObject> - Dimension Value Cache Object List
	 */
	public List<DimensionValueCacheObject> getCacheObjectList(String pCacheKey) {
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUDimensionValueCacheTools.getCacheObjectList method.. {0}", pCacheKey);
		}
		List<DimensionValueCacheObject> dimCacheObjList = get(pCacheKey, false);
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDimensionValueCacheTools.getCacheObjectList method");
		}
		return dimCacheObjList;
	}
}
