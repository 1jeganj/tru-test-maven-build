package com.tru.commerce.endeca.cache;

import java.util.List;
import java.util.Set;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.utils.TRUClassificationsTools;

/**
 * The TRUDimensionValueCache class provides the Endeca Dimension value Id's for a give repository Id's and vice versa.
 * This Class will provide the dimension value ids and URL for corresponding ATG category and Classifications items.
 * @version 1.0
 * @author Professional Access
 */
public class TRUDimensionValueCache extends GenericService {

	/**
	 * This property hold reference of DimensionValueCacheTools.
	 */
	private TRUDimensionValueCacheTools mDimensionValueCacheTools;

	/**
	 * Gets the DimensionValueCacheTools.
	 * @return the dimensionValueCacheTools
	 */
	public TRUDimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/**
	 * Sets the DimensionValueCacheTools.
	 * @param pDimensionValueCacheTools the dimensionValueCacheTools to set
	 */
	public void setDimensionValueCacheTools(
			TRUDimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}
	
	/**  
	 * Property to hold mCatalogTools. 
	 * 
	 * */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	
	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * This property hold reference for TRUClassificationsTools.
	 */
	private TRUClassificationsTools mClassificationsTools;
	
	/**
	 * Gets the TRUClassificationsTools.
	 *
	 * @return the TRUClassificationsTools
	 */

	public TRUClassificationsTools getClassificationsTools() {
		return mClassificationsTools;
	}

	/**
	 * Sets the TRUClassificationsTools.
	 *
	 * @param pClassificationsTools the TRUClassificationsTools to set
	 */

	public void setClassificationsTools(TRUClassificationsTools pClassificationsTools) {
		mClassificationsTools = pClassificationsTools;
	}
	
	/**
	 * This property hold reference for CatalogProperties.
	 */
	private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * Returns the this property hold reference for CatalogProperties.
	 * 
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * Sets the this property hold reference for CatalogProperties.
	 * 
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * gives the DimensionValueCacheObject based on cache key.
	 * @param pCacheKey - cacheKey
	 * @return DimensionValueCacheObject - Dimension Value Cache Object
	 */
	public DimensionValueCacheObject getCacheObject(String pCacheKey) {
		if (isLoggingDebug()) {
			vlogDebug(
					"BEGIN:: TRUDimensionValueCache.getCacheObject method.. {0}",
					pCacheKey);
		}
		DimensionValueCacheObject cachedEntry = null;
		DimensionValueCacheTools dimValCacheTools = null;
		// Null Check
		if (!StringUtils.isBlank(pCacheKey)) {
			dimValCacheTools = getDimensionValueCacheTools();
			if (dimValCacheTools != null) {
				// Getting Dimension value cache for a Repository Id
				cachedEntry = dimValCacheTools.get(pCacheKey, null);
			}

		}
		if (cachedEntry == null) {
			if (isLoggingDebug()) {
				vlogDebug(
						"Repository value {0} was not found after cache refresh, returning null for key # ",
						new Object[] { pCacheKey });
			}
			return null;
		}
		// Logging for debug		
		logCacheObject(cachedEntry);

		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDimensionValueCache.getCacheObject method.. ");
		}
		return cachedEntry;
	}
	
	/**
	 * gives the DimensionValueCacheObject based on cache key.
	 * @param pCacheKey - cacheKey
	 * @return DimensionValueCacheObject - Dimension Value Cache Object
	 */
	@SuppressWarnings("unchecked")
	public DimensionValueCacheObject getCacheWithMultipleAncestors(String pCacheKey) {
		
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUDimensionValueCache.getCacheWithMultipleAncestors method.. {0}", pCacheKey);
		}
		List<DimensionValueCacheObject> dimCacheObjectList = getDimensionValueCacheTools().getCacheObjectList(pCacheKey);
		DimensionValueCacheObject dimCacheObject = null;
		if(dimCacheObjectList == null || dimCacheObjectList.isEmpty()) {
			return null;
		}
		
		dimCacheObject =  dimCacheObjectList.get(0);
		if(dimCacheObjectList.size() > TRUEndecaConstants.CONSTANT_ONE) {
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			String rootCategoryIdForSite = getClassificationsTools().getRootCategoryIdForSite(currentSiteId);
			for(DimensionValueCacheObject dimCache : dimCacheObjectList) {
				List<String> ancestorIds = dimCache.getAncestorRepositoryIds();
				if(ancestorIds == null || ancestorIds.isEmpty()) {
					continue;
				}
				String rootAncestorId = ancestorIds.get(0);
				if(StringUtils.isEmpty(rootAncestorId)) {
					continue;
				}
				try {
					RepositoryItem rootCategory = getCatalogTools().findCategory(rootAncestorId);
					boolean isValidCategoryHierarchy = getCategoryHierarchyStatus(ancestorIds);
					if(rootCategory == null || !isValidCategoryHierarchy) {
						continue;
					}
					Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) rootCategory.getPropertyValue(getCatalogProperties().getFixedParentCategoriesPropertyName());
					if(parentCategories == null || parentCategories.isEmpty()) {
						continue;
					}
					for(RepositoryItem category : parentCategories) {
						String reporitoryId = category.getRepositoryId();
						if(!StringUtils.isEmpty(reporitoryId) && !StringUtils.isEmpty(rootCategoryIdForSite) && reporitoryId.equals(rootCategoryIdForSite)) {
							return dimCache;
						}
					}
				} catch (RepositoryException repositoryException) {
					if(isLoggingError()) {
						vlogError(repositoryException, "Repository Exception occured in TRUDimensionValueCache.getCacheWithMultipleAncestors() method");
					}
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDimensionValueCache.getCacheWithMultipleAncestors method and DimensionValueCache Object : {0}", dimCacheObject);
		}
		return dimCacheObject;
	}
	

	/**
	 * gives the DimensionValueCacheObject based on the give Endeca N Id.
	 * @param pDimvalId - Dimension ID
	 * @return DimensionValueCacheObject - Cached Object
	 */
	public DimensionValueCacheObject getCachedObjectForDimval(String pDimvalId) {
		if (isLoggingDebug()) {
			vlogDebug(
					"BEGIN:: TRUDimensionValueCache.getCachedObjectForDimval method.. {0}",
					pDimvalId);
		}
		DimensionValueCacheObject cachedEntry = null;
		DimensionValueCacheTools dimValCacheTools = null;
		dimValCacheTools = getDimensionValueCacheTools();
		// Null Check
		if (dimValCacheTools != null && !StringUtils.isBlank(pDimvalId)) {
				// Getting Dimension value cache for an Endeca N  Id
				cachedEntry = dimValCacheTools
						.getCachedObjectForDimval(pDimvalId);			

		}
		if (cachedEntry == null && isLoggingDebug()) {
			vlogDebug(
						"Dimension value {0} was not found after cache refresh, returning null for key # ",
						new Object[] { pDimvalId });	
			return null;
		}
		// Logging for debug
		logCacheObject(cachedEntry);

		if (isLoggingDebug()) {
			vlogDebug("END:: TRUDimensionValueCache.getCachedObjectForDimval method.. ");
		}
		return cachedEntry;
	}

	
	/**
	 * gives the DimensionValueCacheObject based on the give Endeca N Id.
	 * @param pDimvalId - Dimension ID
	 * @return DimensionValueCacheObject - Cached Object
	 */
	public boolean getCategoryHierarchyStatus(List<String> pAncestorIds) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN::::: TRUDimensionValueCache.getCategoryHierarchyStatus method.. ");
		}
		boolean isValidCategoryHierarchy = true;
		if(pAncestorIds != null && !pAncestorIds.isEmpty()) {
			for(String ancestorId : pAncestorIds) {
				try {
					RepositoryItem categoryItem = getCatalogTools().findCategory(ancestorId);
					if(categoryItem == null || !isValidCategoryHierarchy) {
						isValidCategoryHierarchy = false;
						break;
					}
					String displayStatus = (String) categoryItem.getPropertyValue(getCatalogProperties().getDisplayStatusPropertyName());
					if(StringUtils.isNotEmpty(displayStatus)) {
						isValidCategoryHierarchy = !((displayStatus.equalsIgnoreCase(TRUCommerceConstants.HIDDEN)||displayStatus.equalsIgnoreCase(TRUCommerceConstants.NO_DISPLAY)));
					}
				} catch (RepositoryException repositoryException) {
					if(isLoggingError()) {
						vlogError(repositoryException, "Repository Exception occured in TRUDimensionValueCache.getCategoryHierarchyStatus() method");
					}
				}
			}
				
		}
		if (isLoggingDebug()) {
			vlogDebug("END::::: TRUDimensionValueCache.getCategoryHierarchyStatus method.. ");
		}
		return isValidCategoryHierarchy;
	}
	/**
	 * logs the required details to ATG logs if debug mode enabled.
	 * @param pCachedEntry - Dimension Value cache Object
	 */
	private void logCacheObject(DimensionValueCacheObject pCachedEntry) {

		if (isLoggingDebug()) {
			vlogDebug("Retrieved {0} from the cache",
					new Object[] { pCachedEntry });
			vlogDebug("RepositoryID  {0} ", pCachedEntry.getRepositoryId());
			vlogDebug("URL  {0} ", pCachedEntry.getUrl());
			vlogDebug("DimValID  {0} ", pCachedEntry.getDimvalId());

		}
	}
}
