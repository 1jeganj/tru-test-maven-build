package com.tru.commerce.exception;

import atg.process.ProcessException;

/**
 * This exception class used to throw endeca related exceptions.
 * 
 * @version 1.0
 * @author Professional Access
 */

public class TRUEndecaException extends ProcessException {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 * @param pSourceException - Throwable
	 */
	public TRUEndecaException(String pMessage, Throwable pSourceException) {
		super(pMessage, pSourceException);
	}

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 */
	public TRUEndecaException(String pMessage) {
		super(pMessage);
	}

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pSourceException - Throwable
	 */
	public TRUEndecaException(Throwable pSourceException) {
		super(pSourceException);
	}
}
