package com.tru.cache;

import javax.jms.JMSException;
import javax.jms.Message;

import atg.dms.patchbay.MessageSink;
import atg.service.cache.Cache;

import com.tru.commerce.exception.TRUEndecaException;

/**
 * This class extends OOTB service Cache and implements patch bay systems message sink interface.
 * This class is written to hold the endeca objects in cache. This class is a gateway for endeca integration in TRU.
 * Everytime a endeca query is fired it is fired through the get(pKey) method.
 * 
 * This classes uses CacheAdapter for querying items from Endeca if not found in this cache.
 * The Objects returned by Cache Adapter are cached by this class so they can returned from cache with out invoking the endeca interface.
 * 
 * 
 * This class implements OOTB Message Sink interface for flushing its cache content.
 * Whenever a message is received in receiveMessage() it triggers Cache Flush.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUEndecaCache extends Cache implements MessageSink {

	/**
	 * To hold serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used to flush the cache.
	 * 
	 * @param pParamString - String value.
	 * @param pParamMessage - value message.
	 * @throws JMSException - to throw the JMSException.
	 */
	@Override
	public void receiveMessage(String pParamString, Message pParamMessage) throws JMSException {
		if (isLoggingDebug()) {
			vlogDebug("TRUEndecaCache.receiveMessage method.. pParamString : {0} pParamMessage : {1}", pParamString, pParamMessage);
		}
		flush();
	}

	/**
	 * This method is used to get the value form cache Object.
	 * 
	 * @param pKey - Value of key object.
	 * @return Object - return the Object values.
	 * @throws TRUEndecaException - to throw the TRUEndecaException.
	 */
	@Override
	public Object get(Object pKey) throws TRUEndecaException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUEndecaCache.get method.. pKey : {0}", pKey);
		}
		Object value = null;
		try {
			value = super.get(pKey);
		} catch (Exception exception) {
			throw new TRUEndecaException(exception);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUEndecaCache.get method.. value : {0}", value);
		}
		return value;
	}
	/**
	 * This method is used to put the Object into Cache.
	 * 
	 * @param pKey - Value of key object.
	 * @param pValue - Value of object.
	 * 
	 */
	@Override
	public void put(Object pKey,Object pValue) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUEndecaCache.put method.. pKey : {0} pValue : {1}", pKey,pValue);
		}
		if(pKey != null && pValue != null){
			super.put(pKey,pValue);	
		}		
		
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUEndecaCache.put method.. pKey : {0} pValue : {1}", pKey,pValue);
		}
		
	}

}
