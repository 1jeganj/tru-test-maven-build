package com.tru.cache.adapter;

import atg.nucleus.GenericService;
import atg.service.cache.CacheAdapter;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.exception.TRUEndecaException;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.controller.TRUFrontController;
import com.tru.endeca.vo.EndecaResponseVO;
import com.tru.endeca.vo.RequestVO;

/**
 * This service is an implementation of CacheAdapter that caches Endeca Objects for purpose of showing the content on Home Page.
 * 
 * @version 1.0
 * @author Professional Access.
 */
public class TRUEndecaCacheAdapter extends GenericService implements CacheAdapter {

	/**
	 * Property to hold FrontController.
	 */
	private TRUFrontController mFrontController;

	/**
	 * This method invokes the ENDECA interface for querying results from ENDECA.
	 * 
	 * @param pKey - Request Object understood by ENDECA interface.
	 * @return Object - Object retrieved from ENDECA interface.
	 * @throws TRUEndecaException - If exception occurs at the ENDECA end.
	 */
	@Override
	public Object getCacheElement(Object pKey) throws TRUEndecaException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUEndecaCacheAdapter.getCacheElement method.. pKey : {0}", pKey);
		}

		EndecaResponseVO lEndecaResponseVO = null;
		if (pKey instanceof RequestVO) {
			lEndecaResponseVO = getFrontController().dispatchRequest((RequestVO) pKey);
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUEndecaCacheAdapter.getCacheElement method..");
		}
		return lEndecaResponseVO;
	}

	/**
	 * This method invokes the ENDECA interface for querying results from ENDECA.
	 * 
	 * @param pKeys - Request Objects understood by ENDECA interface.
	 * @return Object[] - Objects retrieved from ENDECA interface.
	 * @throws TRUEndecaException - If exception occurs at the ENDECA end.
	 */
	@Override
	public Object[] getCacheElements(Object[] pKeys) throws TRUEndecaException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUEndecaCacheAdapter.getCacheElements method.. pKeys : {0}", pKeys);
		}
		EndecaResponseVO[] endecaResponseVOs = null;
		if (pKeys == null || pKeys.length < TRUCommerceConstants.INT_ONE) {
			if (isLoggingDebug()) {
				logDebug("Empty keys");
			}
			return endecaResponseVOs;
		}

		EndecaResponseVO endecaResponseVO = null;
		for (int i = TRUEndecaConstants.INT_ZERO; i < pKeys.length; i++) {
			endecaResponseVO = (EndecaResponseVO) getCacheElement(pKeys[i]);
			if (endecaResponseVO != null) {
				if (endecaResponseVOs == null) {
					endecaResponseVOs = new EndecaResponseVO[pKeys.length];
				}
				endecaResponseVOs[i] = endecaResponseVO;
			}
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUEndecaCacheAdapter.getCacheElements method..");
		}
		return endecaResponseVOs;
	}

	/**
	 * Used to find the cache element size.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheElementSize(Object pParamObject1, Object pParamObject2) {
		return TRUEndecaConstants.INT_ZERO;
	}

	/**
	 * Used to find the cache key size.
	 * 
	 * @param pParamObject - Object
	 * @return int - Return integer value
	 */
	@Override
	public int getCacheKeySize(Object pParamObject) {
		return TRUEndecaConstants.INT_ZERO;
	}

	/**
	 * Used to remove cache element.
	 * 
	 * @param pParamObject1 - First object
	 * @param pParamObject2 - Second object
	 */
	@Override
	public void removeCacheElement(Object pParamObject1, Object pParamObject2) {
		//return;
	}

	/**
	 * Gets the frontController.
	 * 
	 * @return the frontController.
	 */
	public TRUFrontController getFrontController() {
		return mFrontController;
	}

	/**
	 * Sets the frontController.
	 * 
	 * @param pFrontController the frontController to set
	 */
	public void setFrontController(TRUFrontController pFrontController) {
		mFrontController = pFrontController;
	}
}
