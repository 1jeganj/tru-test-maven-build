package com.tru.common;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.droplet.ComponentExists;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;


/**
 * The Class TRUComponentExists.
 *  @version 1.0
 *  @author Professional Access
 */
public class TRUComponentExists extends ComponentExists {
	

	/**
	 * * This method is used to get Targeter Path..
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUComponentExists.service method..");
		}
		String path = pRequest.getParameter(PATH);
		final Object obj = pRequest.resolveName(path);

		if (obj != null) {
			if (isLoggingDebug()) {
				logDebug("Component " + path + " exists.");
			}
			pRequest.serviceLocalParameter(TRUE, pRequest, pResponse);
		} else {
			if (isLoggingDebug()) {
				logDebug("Component " + path + " does not exist.");
			}
			pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUComponentExists.service method..");
		}
	}

}


