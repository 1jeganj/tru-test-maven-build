/*
 * @(#) TRUStoreConfiguration.java 1.0
 * 
 * Copyright PA, Inc. All rights reserved. PA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.tru.common;



import atg.nucleus.GenericService;

/**
 * The class holds the TRUStoreConfiguration property getters and setters.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUEndecaStoreConfiguration extends GenericService {

	/**
	 * This property hold reference for UrlPrefix.
	 */
	private String mUrlPrefix;
	/**
	 * Property to hold MegaMenuMaxAllowedRootCategories.
	 */
	private int mMegaMenuMaxAllowedRootCategories;

	/**
	 * Property to hold MegaMenuMaxAllowedRootCategories.
	 */
	private int mMegaMenuMaxAllowedLevelOneCategories;

	/**
	 * Property to hold MegaMenuMaxAllowedRootCategories.
	 */
	private int mMegaMenuMaxAllowedLevelTwoCategories;

	/**
	 * Gets the megaMenuMaxAllowedRootCategories.
	 * 
	 * @return the megaMenuMaxAllowedRootCategories
	 */
	public int getMegaMenuMaxAllowedRootCategories() {
		return mMegaMenuMaxAllowedRootCategories;
	}

	/**
	 * Sets the megaMenuMaxAllowedRootCategories.
	 * 
	 * @param pMegaMenuMaxAllowedRootCategories the megaMenuMaxAllowedRootCategories to set
	 */
	public void setMegaMenuMaxAllowedRootCategories(int pMegaMenuMaxAllowedRootCategories) {
		mMegaMenuMaxAllowedRootCategories = pMegaMenuMaxAllowedRootCategories;
	}

	/**
	 * Gets the megaMenuMaxAllowedLevelOneCategories.
	 * 
	 * @return the megaMenuMaxAllowedLevelOneCategories
	 */
	public int getMegaMenuMaxAllowedLevelOneCategories() {
		return mMegaMenuMaxAllowedLevelOneCategories;
	}

	/**
	 * Sets the megaMenuMaxAllowedLevelOneCategories.
	 * 
	 * @param pMegaMenuMaxAllowedLevelOneCategories the megaMenuMaxAllowedLevelOneCategories to set
	 */
	public void setMegaMenuMaxAllowedLevelOneCategories(int pMegaMenuMaxAllowedLevelOneCategories) {
		mMegaMenuMaxAllowedLevelOneCategories = pMegaMenuMaxAllowedLevelOneCategories;
	}

	/**
	 * Gets the megaMenuMaxAllowedLevelTwoCategories.
	 * 
	 * @return the megaMenuMaxAllowedLevelTwoCategories
	 */
	public int getMegaMenuMaxAllowedLevelTwoCategories() {
		return mMegaMenuMaxAllowedLevelTwoCategories;
	}

	/**
	 * Sets the megaMenuMaxAllowedLevelTwoCategories.
	 * 
	 * @param pMegaMenuMaxAllowedLevelTwoCategories the megaMenuMaxAllowedLevelTwoCategories to set
	 */
	public void setMegaMenuMaxAllowedLevelTwoCategories(int pMegaMenuMaxAllowedLevelTwoCategories) {
		mMegaMenuMaxAllowedLevelTwoCategories = pMegaMenuMaxAllowedLevelTwoCategories;
	}

	/**
	 * Gets the url prefix.
	 *
	 * @return the url prefix
	 */
	public String getUrlPrefix() {
		return mUrlPrefix;
	}

	/**
	 * Sets the url prefix.
	 *
	 * @param pUrlPrefix the new url prefix
	 */
	public void setUrlPrefix(String pUrlPrefix) {
		this.mUrlPrefix = pUrlPrefix;
	}
}
