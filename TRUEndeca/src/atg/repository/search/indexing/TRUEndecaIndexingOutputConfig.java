/**
 * 
 */
package atg.repository.search.indexing;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.adapter.gsa.GSARepository;
import atg.endeca.index.EndecaIndexingOutputConfig;
import atg.repository.RepositoryItemDescriptor;

/**
 * @author Professional Access
 *
 */
public class TRUEndecaIndexingOutputConfig extends EndecaIndexingOutputConfig {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @throws RemoteException
	 */
	public TRUEndecaIndexingOutputConfig() throws RemoteException {
		super();
	}
	
	/* (non-Javadoc)
	 * @see atg.repository.search.indexing.IndexingOutputConfig#getMonitoredItemDescriptorProperties()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getMonitoredItemDescriptorProperties() {
		if(isLoggingDebug()){
			logDebug("Entered into TRUEndecaIndexingOutputConfig.getMonitoredItemDescriptorProperties()");
		}
		List monitoredItemDescriptorProperties = new ArrayList();
		String itemDescriptorName;
	    Set<RepositoryItemDescriptor> descriptors = getItemDescriptors();
	    if(isLoggingDebug()){
	    	vlogDebug("descriptors : {0}", descriptors); 
	    }
	    for (RepositoryItemDescriptor descriptor : descriptors) {
	      String repositoryPath = ((GSARepository)getRepository()).getAbsoluteName();
	      itemDescriptorName = descriptor.getItemDescriptorName();

	      Map propertyToConfig = getAnalyzer().getPropertyToConfigPathsForRepositoryAndItem(repositoryPath, itemDescriptorName);
	      if(propertyToConfig != null && !propertyToConfig.isEmpty()){
	    	  Set<String> keys = propertyToConfig.keySet();
	    	  for (String key : keys) {
	    		  monitoredItemDescriptorProperties.add(new StringBuilder().append(itemDescriptorName).append(".").append(key).toString());
	    	  }
	      }
	      if(isLoggingDebug()){
		    	vlogDebug("getPropertyToConfigPathsForRepositoryAndItem () ::: repositoryPath : {0}, itemDescriptorName : {1}", repositoryPath, itemDescriptorName); 
		    	vlogDebug("propertyToConfig : {0}", propertyToConfig);
		    }
	      Map refPropertyToConfig = getAnalyzer().getItemRefPropertyToConfigPathsForRepositoryAndItem(repositoryPath, itemDescriptorName);
	      if(isLoggingDebug()){
		    	vlogDebug("getItemRefPropertyToConfigPathsForRepositoryAndItem :::: repositoryPath : {0}, itemDescriptorName : {1}", repositoryPath, itemDescriptorName); 
		    	vlogDebug("refPropertyToConfig : {0}", refPropertyToConfig);
		    }
	      if(refPropertyToConfig != null && !refPropertyToConfig.isEmpty()){
	    	  Set<String>  keys = refPropertyToConfig.keySet();
	    	  for (String key : keys)
	    		  monitoredItemDescriptorProperties.add(new StringBuilder().append(itemDescriptorName).append(".").append(key).toString());
	      } else{
	    	  if(isLoggingDebug()){
	    		  logDebug("refPropertyToConfig is null");
	    		  vlogDebug("refPropertyToConfig : {0}", refPropertyToConfig);
	    	  }
	      }
	    }
	    if(isLoggingDebug()){
			logDebug("Exit from TRUEndecaIndexingOutputConfig.getMonitoredItemDescriptorProperties()");
		}
	    return monitoredItemDescriptorProperties;
	}

}
