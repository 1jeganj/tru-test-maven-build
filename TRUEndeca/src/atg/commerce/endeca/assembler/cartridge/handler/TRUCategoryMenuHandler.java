package atg.commerce.endeca.assembler.cartridge.handler;

import atg.endeca.assembler.AssemblerTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RefinementMenu;
import com.tru.endeca.constants.TRUEndecaConstants;

//import java.util.LinkedHashMap;

/**
 * This Class Creates the Navigation BreadCrumbs TRUBreadcrumbsHandler.java.
 *	@version 1.0 
 *  @author Professional Access
 */
public class TRUCategoryMenuHandler extends CategoryMenuHandler {
	
  @Override
	 public RefinementMenu filterFacets(RefinementMenu pMenu, String[] pSites, String[] pCatalogs)
			    throws CartridgeHandlerException
			  {
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug("===BEGIN===:: TRUCategoryMenuHandler.filterFacets : ");
			}
		 
			DynamoHttpServletRequest currentRequest = ServletUtil.getCurrentRequest();
			
			if(currentRequest.getQueryString()!=null && currentRequest.getQueryString().contains(TRUEndecaConstants.NTT_PARAM)||currentRequest.getQueryString()!=null &&currentRequest.getQueryString().contains(TRUEndecaConstants.PROMOTION_ID) ){
				if (isLoggingDebug()) {
					AssemblerTools.getApplicationLogging().vlogDebug(": inside search  condition:: ");
				}
				return pMenu;
			     }
			
			
			if (isLoggingDebug()) {
				AssemblerTools.getApplicationLogging().vlogDebug("===END===:: TRUCategoryMenuHandler.filterFacets before returning refine ment for category pages: ");
			}
		       return super.filterFacets(pMenu, pSites, pCatalogs);
			  }

	 
	 
	 /**
		 * Checks if is logging error.
		 * 
		 * @return the loggingError
		 */
		public boolean isLoggingDebug() {
			return AssemblerTools.getApplicationLogging().isLoggingDebug();
		}
}


