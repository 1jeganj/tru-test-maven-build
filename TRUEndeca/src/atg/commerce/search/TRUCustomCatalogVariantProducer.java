package atg.commerce.search;

import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.search.producer.CustomCatalogVariantProducer;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;

/**
 * The Class TRUCustomCatalogVariantProducer used to exclude those sku during Indexing having isDeleted true or webDisplay false.
 *
 */
public class TRUCustomCatalogVariantProducer extends CustomCatalogVariantProducer{
	
	/**
	 * This property hold reference for TRUEndecaConfigurations.
	 */
	private TRUEndecaConfigurations mEndecaConfigurations;
	/** 
	 * The m index all sku flag. 
	 */
	private boolean mIndexAllSkuFlag;
	
	/** 
	 * The mCheckSuperDisplayFlag. 
	 */
	private boolean mCheckSuperDisplayFlag;
	
	/**
	 * Checks if is index all sku flag.
	 *
	 * @return true, if is index all sku flag
	 */
	public boolean isIndexAllSkuFlag() {
		return mIndexAllSkuFlag;
	}

	/**
	 * Sets the index all sku flag.
	 *
	 * @param pIndexAllSkuFlag the new index all sku flag
	 */
	public void setIndexAllSkuFlag(boolean pIndexAllSkuFlag) {
		mIndexAllSkuFlag = pIndexAllSkuFlag;
	}
	
	/**
	 * @return the CheckSuperDisplayFlag
	 */
	public boolean isCheckSuperDisplayFlag() {
		return mCheckSuperDisplayFlag;
	}

	/**
	 * @param pCheckSuperDisplayFlag the CheckSuperDisplayFlag to set
	 */
	public void setCheckSuperDisplayFlag(boolean pCheckSuperDisplayFlag) {
		mCheckSuperDisplayFlag = pCheckSuperDisplayFlag;
	}
	
	/**
	 * Gets the EndecaConfigurations.
	 * 
	 * @return TRUEndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}

	/**
	 * set the EndecaConfigurations.
	 * 
	 * @param pEndecaConfigurations
	 *            - Endeca Configurations.
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}

	/**
	 * This method check and to exclude sku during Indexing having isDeleted true or webDisplay false.
	 *
	 * @param pContext the context
	 * @param pPropertyName the property name
	 * @param pItem the item
	 * @param pIndex the index
	 * @param pUniqueParams the unique params
	 * @return true, if successful
	 */
 	@SuppressWarnings("rawtypes")
	public boolean prepareNextVariant(Context pContext, String pPropertyName, RepositoryItem pItem, int pIndex, Map pUniqueParams) {
 		
 		if (isLoggingDebug()) {
			vlogDebug("Begin::: TRUCustomCatalogVariantProducer.prepareNextVariant method");
		}
		
 		if(isIndexAllSkuFlag()){
			 return super.prepareNextVariant(pContext, pPropertyName, pItem, pIndex, pUniqueParams);
		 }

		 if(pItem == null){
			 return false;
		 }
		 String itemDesc = null;
		 List <String> skuType = getEndecaConfigurations().getSkuType();
		 try {
				itemDesc = pItem.getItemDescriptor().getItemDescriptorName();
				if(!StringUtils.isEmpty(itemDesc) && (TRUEndecaConstants.REGULAR_SKU.equalsIgnoreCase(itemDesc) || (skuType != null && !skuType.isEmpty() &&  skuType.contains(itemDesc)))){
					String onlinePID = (String)pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getOnlinePID());
					boolean isDeletedFlag =	(boolean)pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getIsSkuDeleted());
					boolean isWebDisplayFlag = (boolean)pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getWebDisplayFlag());
					Set siteIds = (Set)pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getSiteIds());
					boolean superDisplayFlag = false;
					if(pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getSuperDisplayFlag()) == null) {
						superDisplayFlag = true;
					} else {
						superDisplayFlag = (boolean) pItem.getPropertyValue(((TRUCatalogProperties)getCatalogProperties()).getSuperDisplayFlag());
					}
					
					if(onlinePID == null || isDeletedFlag || !isWebDisplayFlag || siteIds.isEmpty() || (isCheckSuperDisplayFlag() && !superDisplayFlag)){
						if (isLoggingDebug()) {
							vlogDebug("END::: TRUCustomCatalogVariantProducer.prepareNextVariant method(){0} isDeleted {1} webDisplay ",isDeletedFlag,isWebDisplayFlag);
						}
						return false;
					}
				}
			} catch (RepositoryException re) {
				if(isLoggingError()){
					vlogError("RepositoryException while getting item descriptor name for item : {0} and exception is : {1}", pItem, re);
				}
			}
			
			if (isLoggingDebug()) {
				vlogDebug("END::: TRUCustomCatalogVariantProducer.prepareNextVariant method");
			}
		  return super.prepareNextVariant(pContext, pPropertyName, pItem, pIndex, pUniqueParams);
	  }

}
