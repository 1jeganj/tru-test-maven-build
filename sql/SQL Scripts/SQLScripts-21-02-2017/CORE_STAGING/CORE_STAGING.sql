CREATE TABLE tru_power_review_rating (
	sku_Id 			varchar2(254)	NOT NULL,
	online_pid 		varchar2(254)	NULL,
	review_rating 		number(28, 10)	NULL,
	review_count 		number(28, 10)	NULL,
	review_count_decimal 	number(28, 10)	NULL,
	PRIMARY KEY(sku_Id)
);


COMMIT;
