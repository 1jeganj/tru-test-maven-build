alter table tru_sku add color_family varchar2(254);

CREATE TABLE tru_size_family (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	sequence_num 		INTEGER	NOT NULL,
	size_family 		varchar2(254)	NULL,
	PRIMARY KEY(sku_id, sequence_num)
);

CREATE TABLE tru_character_theme (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	sequence_num 		INTEGER	NOT NULL,
	character_theme 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, sequence_num)
);

CREATE TABLE tru_collection_theme (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	sequence_num 		INTEGER	NOT NULL,
	collection_theme 	varchar2(254)	NULL,
	PRIMARY KEY(sku_id, sequence_num)
);

COMMIT;