ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_SEO NUMBER(1)	DEFAULT 1 CHECK (ENABLE_SEO IN (0, 1));

CREATE TABLE tru_seo_url_configurations (
	id 			varchar2(254) NOT NULL,
	 asset_version INTEGER NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	tru_seo_url 	varchar2(254)	NULL,
	PRIMARY KEY(id, sequence_num, asset_version)
);

alter table tru_promotion drop column campaign_id;

alter table tru_promotion add (campaign_id NUMBER(6) DEFAULT NULL);