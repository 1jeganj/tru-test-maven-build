create or replace procedure TRU_VIS_RPT_PRC
/*
***************************************************************TRU_VIS_RPT_PRC**********************************************************************************************************
**    i.  Purpose
**
**            This procedure use to generate csv file for Inventory Visibility report
**

**    ii. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          22/Dec/2016   Malleshwara.C              Initial version


*******************************************************************************************************************************************************************************************
*/

(
 P_FILE_NAME varchar2,
 P_DIR_NAME  varchar2,
 P_DATE		 varchar2,
 p_SEQ		 varchar2
)
is
    l_size number :=0;
	CSV_FILE UTL_FILE.FILE_TYPE;
	CURSOR INV_CUR IS 
			select 	Sku.Sku_Id
					,Nvl(Sth_Availibility,0) Sth_Availibility 
					,Nvl(Sts_Availibility,0) Sts_Availibility 
					,NVL(STS_AVAILIBILITY,0) ISPU_AVAILIBILITY
          ,LENGTH(Nvl(Sku.Sku_Id,0))+LENGTH(Nvl(Sth_Availibility,0))+LENGTH(Nvl(Sts_Availibility,0)) line_size
			from 
				Tru_Sku_Cata Sku,
				(select nvl(online_Sku.Item_Name,Store_Sku.Item_Name) Item_Name,nvl(Sth_Availibility,0) Sth_Availibility,nvl(Sts_Availibility,0) Sts_Availibility
				 From				
					(Select Item_Name,Case When  Atc_Quantity > 0 Then 1 Else 0 End Sth_Availibility   From I_AVAILABILITY Where Facility_Name ='online' ) Online_Sku
					full outer join
					(Select Item_Name,Case When  Atc_Quantity > 0 Then 1 Else 0 End Sts_Availibility 
					 From 
						(Select Item_Name, Sum(Atc_Quantity) Atc_Quantity   From I_AVAILABILITY Where Facility_Name <>'online' Group By Item_Name)
					)Store_Sku
				on online_Sku.Item_Name = Store_Sku.Item_Name
				) Inv_Sku
			where Sku.Sku_Id = Inv_Sku.Item_Name(+) and Sku.Web_Disp_Flag ='1';
			
	L_INV INV_CUR%ROWTYPE;
	L_FILE_NAME 	VARCHAR2(255);
	L_HEADER 		VARCHAR2(4000);
	L_TRIALER 		VARCHAR2(4000);
	L_COUNT 		NUMBER;


begin

	if P_FILE_NAME is null or P_DIR_NAME is null or P_DATE is null
	then
	 RAISE_APPLICATION_ERROR(-20101, 'please pass file_name , dir_name and date');
	end if;

	
	select P_FILE_NAME||'.dat' into L_FILE_NAME from DUAL;
	--dbms_output.put_line(L_FILE_NAME);

		csv_file                    := UTL_FILE.FOPEN(P_DIR_NAME,L_file_NAME,'w',32767);
		
		select P_DATE||'HOIDATG        TRUS_WEB_INVENTORY_VISIBILITY        '||p_SEQ into L_HEADER from DUAL;
		UTL_FILE.PUT(CSV_FILE,L_HEADER);
		UTL_FILE.NEW_LINE(CSV_FILE);
		
		open   INV_CUR ;
		LOOP
		  FETCH INV_CUR INTO L_INV;
		  EXIT when INV_CUR%NOTFOUND;
		  UTL_FILE.PUT(CSV_FILE,L_INV.Sku_Id);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Sth_Availibility);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Sts_Availibility);
		  UTL_FILE.NEW_LINE(CSV_FILE);
          l_size := L_INV.line_size + (l_size+2);
		end LOOP;
		
		L_COUNT := INV_CUR%ROWCOUNT;
		
		select P_DATE||'TOIDATG0000000000000'||l_size||'0000'||L_COUNT into L_TRIALER from DUAL;
		
		UTL_FILE.PUT(CSV_FILE,L_TRIALER);
		UTL_FILE.FCLOSE(CSV_FILE);
	EXCEPTiON when OTHERS then
	RAISE;
End;
/