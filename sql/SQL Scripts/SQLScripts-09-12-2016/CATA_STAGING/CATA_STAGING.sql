--Start - Added for Enable Apple Pay Flag - 9th Dec 2016
ALTER TABLE TRU_SITE_CONFIGURATION ADD enable_apple_pay NUMBER(1) DEFAULT 1;
--End - Added for Enable Apple Pay Flag - 9th Dec 2016

-- Start  -  Added for Enable Radial Service Flag- 07th Dec 2016

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_RADIAL NUMBER(1) DEFAULT 1;

-- End  -  Added for Enable Radial Service Flag - 07th Dec 2016