drop table tru_sku_gwp_promos;

CREATE TABLE tru_sku_gwp_promos (
	sku_id 			varchar2(40)	NOT NULL,
	sequence_num 		INTEGER	NOT NULL,
	promotion_id 		varchar2(40)	NULL,
	PRIMARY KEY(sku_id, sequence_num)
);