create or replace package body TRU_PROFILE_MIG_PKG
AS
    
  FUNCTION XML_FILEEXISTS
	(
		P_DIRNAME IN VARCHAR2,   
		P_FILENAME IN VARCHAR2
		) RETURN VARCHAR2
	is
	L_FILENAME varchar2(255);
	L_EXISTS VARCHAR2(1000) := NULL ;
	begin
	
  if P_DIRNAME is null or P_DIRNAME = ' '
  then
     return 'ENTER VALID DIRECTORY NAME';
  end if;
  
  if P_FILENAME is null or P_FILENAME= ' '
  then
      return 'ENTER VALID FILE NAME';
  END IF;
  
	 --This loop convert comma seprated values(P_FILENAME - list of XML file name) into  records and check the file exists or not in the directory
	 FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
							   FROM
								   (select xmltype (   '<rows><row>'
									|| REPLACE (P_FILENAME, ',', '</row><row>')
									|| '</row></rows>'
								   ) AS xmlval
									from DUAL) X,
								table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
                )
	  LOOP
								
								
	 G_BFILE := BFILENAME(UPPER(P_DIRNAME),I.FILE_NAME);
		  IF DBMS_LOB.FILEEXISTS(G_BFILE) = 0
		  THEN
			  L_EXISTS :=  L_EXISTS||','||I.FILE_NAME;
		  end if;
	  end LOOP;
    
    if L_EXISTS is null
    then
      return 'EXISTS';
    ELSE 
      return ltrim(L_EXISTS,',');
    END IF;
  
	end XML_FILEEXISTS;


	PROCEDURE PROFILE_MIG_STAGE_MAIN (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_SPLIT_THREAD number := 5;
	  L_SPLIT_num NUMBER;
	  L_START_NUM number := 0;
	  L_TOTAL_NUM number :=23;
	  L_JOB_STMNT VARCHAR2(4000);
	  L_FILE_NAME VARCHAR2(4000);
	  L_RPT_FILE VARCHAR2(40);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  L_USER         varchar2(40);
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			
			FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
							   FROM
								   (select xmltype (   '<rows><row>'
									|| REPLACE (L_FILE_EXISTS, ',', '</row><row>')
									|| '</row></rows>'
								   ) AS xmlval
									from DUAL) X,
								table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
                )
			 LOOP
					INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,START_DATE,END_DATE,ERROR_MSG,STATUS) VALUES (I.FILE_NAME,SYSTIMESTAMP,SYSTIMESTAMP,'File does not exists in the directory','file_check_failed');
			 END LOOP;
			 COMMIT;
			 
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS||' file does not exists');
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_MIG';
      
		  select SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA')  into L_USER FROM DUAL;
		  for tname in(SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() TAB_NAME 
								   FROM 
									   (select xmltype (   '<rows><row>'
										|| REPLACE ('TRU_TEMP_PROFILE_CREDITCARD,TRU_TEMP_PROFILE_ADDRESS,TRU_TEMP_PROFILE_USERINFO', ',', '</row><row>')
										|| '</row></rows>'
									   ) AS xmlval
										from DUAL) X,
									table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D)
			  LOOP
				 for cname in(select TABLE_NAME, CONSTRAINT_NAME, STATUS, OWNER
								from ALL_CONSTRAINTS
								where r_owner =l_user
								and CONSTRAINT_TYPE = 'R' 
								and r_constraint_name in
								 (
								   select constraint_name from all_constraints
								   where CONSTRAINT_TYPE in ('P', 'U')
								   and table_name = tname.TAB_NAME
								   and OWNER = L_USER
								 ))
				  LOOP
					  execute immediate ('alter table'||' '||CNAME.TABLE_NAME||' '||'disable constraint'||' '||CNAME.CONSTRAINT_NAME);
				  end LOOP;
				 execute immediate 'truncate table'||' '|| TNAME.TAB_NAME; 
				 for cname in(select TABLE_NAME, CONSTRAINT_NAME, STATUS, OWNER
								from ALL_CONSTRAINTS
								where r_owner =l_user
								and CONSTRAINT_TYPE = 'R' 
								and r_constraint_name in
								 (
								   select constraint_name from all_constraints
								   where CONSTRAINT_TYPE in ('P', 'U')
								   and table_name = tname.TAB_NAME
								   and OWNER = L_USER
								 ))
				  LOOP
					execute immediate ('alter table'||' '||CNAME.TABLE_NAME||' '||'enable constraint'||' '||CNAME.CONSTRAINT_NAME);
				  END LOOP;
			  END LOOP;
      
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_USERINFO';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_ADDRESS';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_CREDITCARD';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE DML_ERROR_LOG';
			--EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TMP_XML_SPLIT';
		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_MIG')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				
				EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_XML';
			
				INSERT INTO TRU_TEMP_PROFILE_XML(XML_DATA) VALUES( xmltype(bfilename(UPPER(P_DIR_NAME),I.FILE_NAME),nls_charset_id('AL32UTF8'))); 
				
				DBMS_OUTPUT.PUT_LINE('Inserted XML file Name : '||I.FILE_NAME); 
				
				INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,'PROF_MIG',SYSTIMESTAMP);
				
				COMMIT;
				
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,JOB_NAME,START_DATE,STATUS,ERROR_CODE,ERROR_MSG) VALUES(I.FILE_NAME,'PROF_MIG',SYSTIMESTAMP,'temp_xml_insert_failed',V_ERR_CD,V_ERR);
				commit;
				GOTO LOOP1;
			END;
			  				
			BEGIN
			
				PROFILE_XML_STAGE_LOAD(P_FILE_NAME => I.FILE_NAME);   

			EXCEPTION WHEN OTHERS THEN
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_STAGE_MAIN: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_STAGE_MAIN;
	
	
	PROCEDURE PROFILE_XML_STAGE_LOAD
	( 
	  P_FILE_NAME VARCHAR2
	)
    IS

		L_START NUMBER;
		L_END NUMBER;
		L_PROF_COUNT NUMBER :=0;
		L_ADDRS_COUNT NUMBER :=0;
		L_CC_COUNT NUMBER := 0;
		L_FILE_NAME VARCHAR2(255);
		V_ERR VARCHAR2(255);
		V_ERR_CD VARCHAR2(255);
   
    BEGIN
		
		l_START := DBMS_UTILITY.GET_TIME;
		
		
		L_FILE_NAME := P_FILE_NAME;
    

			

				INSERT INTO TRU_TEMP_PROFILE_USERINFO
				  (		
						USER_ID,
						FIRST_NAME,
						LAST_NAME,
						LASTPWDUPDATE,
						REGISTRATION_DATE,
						EMAIL,
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS,
						DEFAULT_CREDITCARD,
						--SECONDARY_ADDRESSES,
						REWARD_NUMBER,
						LASTACTIVITY_DATE,
						--STATUS,
						LOCALE,
						MIG_FILE_NAME,
						RADIAL_PASSWORD						
				  )  
				  SELECT   
						USER_ID,
						FIRST_NAME,
						LAST_NAME,
						TO_TIMESTAMP(LASTPWDUPDATE,'mm/dd/yyyy hh24:mi:ss'),
						TO_TIMESTAMP(REGISTRATION_DATE,'mm/dd/yyyy hh24:mi:ss'),
						lower(EMAIL),
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS,
						DEFAULT_CREDITCARD,
						--SECONDARY_ADDRESSES,
						REWARD_NUMBER,
						TO_TIMESTAMP(LASTACTIVITY_DATE,'mm/dd/yyyy hh24:mi:ss'),
						--DECODE(STATUS,'true',1,'false',0),
						DECODE(LOCALE ,'unset',0,
								'en_US',1,
								'fr_FR',2,
								'ja_JP',3,
								'de_DE',4,
								'zh_CN',2000,
								'nl',2001,
								'en',2002,
								'en_AU',2003,
								'en_CA',2004,
								'en_GB',2005,
								'fr',2006,
								'de',2007,
								'it',2008,
								'ja',2009,
								'pt_BR',2010,
								'es',2011,
								'sv',2012,
								'el_GR',2013,
								'tr_TR',2014,
								'ko_KR',2015,
								'zh_TW',2016,
								'ru_RU',2017,
								'hu_HU',2018,
								'cs_CZ',2019,
								'pl_PL',2020,
								'da_DK',2021,
								'fi_FI',2022,
								'pt_PT',2023,
								'th',2024,0
						),
						L_FILE_NAME,
						RADIAL_PASSWORD

					  FROM (SELECT XML_DATA 
							FROM TRU_TEMP_PROFILE_XML
								   )TMP,
							XMLTABLE('/data/profile'
										  PASSING TMP.XML_DATA
										  columns 	USER_ID                 	VARCHAR2(255) PATH 'userInfo/property[@name="id"]',
													FIRST_NAME              	VARCHAR2(255) PATH 'userInfo/property[@name="firstName"]',
													LAST_NAME               	VARCHAR2(255) PATH 'userInfo/property[@name="lastName"]',
													LASTPWDUPDATE           	VARCHAR2(255) PATH 'userInfo/property[@name="lastProfileUpdate"]',
													REGISTRATION_DATE       	VARCHAR2(255) PATH 'userInfo/property[@name="registrationDate"]',
													EMAIL                   	VARCHAR2(255) PATH 'userInfo/property[@name="email"]',
													DEFAULT_BILLING_ADDRESS 	VARCHAR2(255) PATH 'userInfo/property[@name="defaultBillingAddress"]',
													DEFAULT_SHIPPING_ADDRESS 	VARCHAR2(255) PATH 'userInfo/property[@name="defaultShippingAddress"]',
													DEFAULT_CREDITCARD      	VARCHAR2(255) PATH 'userInfo/property[@name="defaultCreditCard"]',
													--SECONDARY_ADDRESSES     	VARCHAR2(255) PATH 'userInfo/property[@name="secondaryAddresses"]',
													REWARD_NUMBER 			    VARCHAR2(255) PATH 'userInfo/property[@name="rewardNumber"]',
													--STATUS		  				VARCHAR2(255) PATH 'userInfo/property[@name="profileStatus"]',													
													LASTACTIVITY_DATE			VARCHAR2(255) PATH 'userInfo/property[@name="lastLoginDate"]',
													LOCALE						VARCHAR2(255) PATH 'userInfo/property[@name="locale"]',
													RADIAL_PASSWORD 			VARCHAR2(255) PATH 'userInfo/property[@name="password"]'
									)
				LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_USERINFO :'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				
			     L_PROF_COUNT := SQL%ROWCOUNT;
				
				COMMIT;
				
				  
				  INSERT INTO TRU_TEMP_PROFILE_ADDRESS
					(
						USER_ID,
						ADDRESS_ID,
						NICK_NAME,
						FIRST_NAME,
						LAST_NAME,
						address1,
						ADDRESS2,
						ADDRESS3,
						COUNTY,
						city,
						STATE,
						POSTAL_CODE,
						PHONE_NUMBER,
						COUNTRY,
						EMAIL,
						--STATUS,
						LAST_ACTIVITY,
						MIG_FILE_NAME
					)
					
				 SELECT USR.USER_ID,
						ADDRESS_ID,
						NICK_NAME,
						FIRST_NAME,
						LAST_NAME,
						ADDRESS1,
						ADDRESS2,
						ADDRESS3,
						COUNTY,
						CITY,
           CASE WHEN TRU_PROFILE_MIG_PKG.GET_MAPPED_COUNTRY_CODE(COUNTRY)='US' THEN TRU_PROFILE_MIG_PKG. GET_MAPPED_STATE_CODE(STATE)
                   else STATE end state, 
						POSTAL_CODE,
						PHONE_NUMBER,
						TRU_PROFILE_MIG_PKG.GET_MAPPED_COUNTRY_CODE(COUNTRY),
						LOWER(EMAIL),
						--DECODE(USR.STATUS,'true',1,'false',0) STATUS,
						TO_TIMESTAMP(USR.LASTACTIVITY_DATE,'mm/dd/yyyy hh24:mi:ss') LASTACTIVITY_DATE,
						L_FILE_NAME
				 FROM
					 (SELECT XML_DATA 
										FROM TRU_TEMP_PROFILE_XML
											   )TMP,
										XMLTABLE('/data/profile/addresses/address'
													  PASSING TMP.XML_DATA
													  COLUMNS 	USER_ID     		VARCHAR2(255) PATH './../../userInfo/property[@name="id"]',
																--STATUS		  		VARCHAR2(255) PATH './../../userInfo/property[@name="profileStatus"]',
																LASTACTIVITY_DATE	VARCHAR2(255) PATH './../../userInfo/property[@name="lastLoginDate"]',
																ADDRESS_ID      	VARCHAR2(255) PATH 'property[@name="id"]',
																NICK_NAME       	VARCHAR2(255) PATH 'property[@name="nickName"]',
																FIRST_NAME      	VARCHAR2(255) PATH 'property[@name="firstName"]',
																LAST_NAME       	VARCHAR2(255) PATH 'property[@name="lastName"]',
																ADDRESS1        	VARCHAR2(255) PATH 'property[@name="address1"]',
																ADDRESS2        	VARCHAR2(255) PATH 'property[@name="address2"]',
																ADDRESS3        	VARCHAR2(255) PATH 'property[@name="address3"]',
																COUNTY      	  	VARCHAR2(255) PATH 'property[@name="county"]', 
																CITY       	  		VARCHAR2(255) PATH 'property[@name="city"]',
																STATE       	  	VARCHAR2(255) PATH 'property[@name="state"]',
																POSTAL_CODE 	  	VARCHAR2(255) PATH 'property[@name="postalCode"]', 
																PHONE_NUMBER 	  	VARCHAR2(255) PATH 'property[@name="phoneNumber"]', 
																COUNTRY         	VARCHAR2(255) PATH 'property[@name="country"]', 
																EMAIL       	  	VARCHAR2(255) PATH 'property[@name="email"]'
												)USR
								  
				  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_ADDRESS:'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				  
				  L_ADDRS_COUNT := SQL%ROWCOUNT;
				  
				COMMIT;
				
				  INSERT INTO TRU_TEMP_PROFILE_CREDITCARD
					(
						USER_ID, 
						CREDIT_CARD_ID,
						NAME_ON_CARD,
						EXPIRATION_MONTH,
						CREDIT_CARD_NUMBER,
						BILLING_ADDRESS,
						EXPIRATION_YEAR,
						CREDIT_CARD_TYPE,
						--STATUS,
						MIG_FILE_NAME
					)
				  SELECT USR.USER_ID, 
						 CREDIT_CARD_ID,
						 NAME_ON_CARD,
						 EXPIRATION_MONTH,
						 CREDIT_CARD_NUMBER,
						 BILLING_ADDRESS,
						 EXPIRATION_YEAR,
						 CREDIT_CARD_TYPE,
						 --DECODE(USR.STATUS,'true',1,'false',0),
						 L_FILE_NAME

				  FROM
					(SELECT XML_DATA 
									FROM TRU_TEMP_PROFILE_XML
										   )TMP,
									XMLTABLE('/data/profile/creditCards/creditCard'
												  PASSING TMP.XML_DATA
												  COLUMNS 	USER_ID           	VARCHAR2(255) PATH './../../userInfo/property[@name="id"]',
															--STATUS		  	  	VARCHAR2(255) PATH './../../userInfo/property[@name="profileStatus"]',
															CREDIT_CARD_ID    	VARCHAR2(255) PATH 'property[@name="id"]', 
															NAME_ON_CARD      	VARCHAR2(255) PATH 'property[@name="nameOnCard"]',
															EXPIRATION_MONTH   	VARCHAR2(255) PATH 'property[@name="expirationMonth"]',
															CREDIT_CARD_NUMBER  VARCHAR2(255) PATH 'property[@name="creditCardNumber"]',
															BILLING_ADDRESS     VARCHAR2(255) PATH 'property[@name="billingAddress"]',
															EXPIRATION_YEAR     VARCHAR2(255) PATH 'property[@name="expirationYear"]',
															CREDIT_CARD_TYPE    VARCHAR2(255) PATH 'property[@name="creditCardType"]'
							  )USR
								
				  LOG ERRORS INTO DML_ERROR_LOG ('TRU_TEMP_PROFILE_CREDITCARD:'||L_FILE_NAME) REJECT LIMIT UNLIMITED;
				  
				  L_CC_COUNT := SQL%ROWCOUNT;
				 COMMIT;
	
    
		UPDATE TRU_TEMP_MIG_FILE_STATUS SET END_DATE = SYSTIMESTAMP, EXEC_TIME = SYSTIMESTAMP-START_DATE , STATUS = 'temp_load_completed', TOTAL_PROF_REC_LOADED_STAGE = L_PROF_COUNT,
		TOTAL_ADDS_REC_LOADED_STAGE = l_ADDRS_COUNT,TOTAL_CC_REC_LOADED_STAGE = l_CC_COUNT,FILE_SIZE = G_FILE_SIZE
		WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG' AND START_DATE = (SELECT MAX(START_DATE) FROM  TRU_TEMP_MIG_FILE_STATUS WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG');
		
		COMMIT;
   
		L_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_XML_STAGE_LOAD: Time Taken to execute ' || (L_END-l_START)/100||' '||'seconds');
		
		EXCEPTION
		WHEN OTHERS THEN
			V_ERR := SUBSTR(SQLERRM, 1, 200);
			V_ERR_CD := SQLCODE;
			UPDATE TRU_TEMP_MIG_FILE_STATUS SET END_DATE = SYSTIMESTAMP, EXEC_TIME = SYSTIMESTAMP-START_DATE , STATUS = 'temp_load_failed',
				   TOTAL_PROF_REC_LOADED_STAGE = L_PROF_COUNT,TOTAL_ADDS_REC_LOADED_STAGE = l_ADDRS_COUNT,TOTAL_CC_REC_LOADED_STAGE = l_CC_COUNT,
				   ERROR_CODE = V_ERR_CD, ERROR_MSG = V_ERR,FILE_SIZE = G_FILE_SIZE
			WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG' AND START_DATE = (SELECT MAX(START_DATE) FROM  TRU_TEMP_MIG_FILE_STATUS WHERE FILE_NAME = P_FILE_NAME AND JOB_NAME = 'PROF_MIG');
			commit;
		  RAISE;
	END PROFILE_XML_STAGE_LOAD;
	
	PROCEDURE PROFILE_MIG_TARGT(P_LAOD_TYPE  VARCHAR2)
    IS
		L_PROF_COUNT NUMBER;
		L_ADDRS_COUNT NUMBER;
		L_CC_COUNT NUMBER;
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF LOWER(P_LAOD_TYPE) not in('initial' ,'delta') or P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			INSERT INTO DPS_USER 
			(
				ID,
				LOGIN,
				AUTO_LOGIN,
				PASSWORD,
				PASSWORD_SALT,
				PASSWORD_KDF,
				--REALM_ID,
				MEMBER,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--USER_TYPE,
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				GENERATEDPWD,
				REGISTRATION_DATE,
				EMAIL,
				EMAIL_STATUS,
				RECEIVE_EMAIL
				--LAST_EMAILED,
				--GENDER,
				--DATE_OF_BIRTH,
				--SECURITYSTATUS,
				--DESCRIPTION
			)
		    SELECT 
				USER_ID,
				EMAIL,
				0,
				RADIAL_PASSWORD,
				G_SALT_PASWD,
				4001,
				--NULL
				0,
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				LOCALE,
				LASTACTIVITY_DATE,
				LASTPWDUPDATE,
				0,
				REGISTRATION_DATE,
				EMAIL,
				0,
				1
				--NULL
				--NULL
				--NULL
				--NULL
				--NULL
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			
			--MODIFIED FOR CR154 
			INSERT INTO TRUCORE_QC.RADIAL_USER
			(
				ID,
				RADIAL_PASSWORD
			)
			SELECT 
				 USER_ID,
				 RADIAL_PASSWORD
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('TRUCORE_QC.RADIAL_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('TRUCORE_QC.RADIAL_USER load completed');
			--MODIFIED FOR CR36 
			INSERT INTO TRU_DPSX_USER
			(
				ID,
				REWARD_NUMBER,
				--LOYAL_CUST,
				--MIG_PWD_RESET_FLAG,    
				MIG_PROFILE_FLAG,
				MIG_FILE_NAME
			)
			SELECT 
				 USER_ID,
				 REWARD_NUMBER, 
				 --NULL
				 --NULL
				 1,
				 MIG_FILE_NAME
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
			
			COMMIT;
				--MODIFIED FOR CR36 
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			INSERT INTO DPS_CONTACT_INFO 
			(
				ID,                   
				USER_ID,
				--PREFIX,
				FIRST_NAME,
				--MIDDLE_NAME,
				LAST_NAME,
				--SUFFIX,
				--JOB_TITLE,
				--COMPANY_NAME,
				ADDRESS1,
				ADDRESS2,
				ADDRESS3,
				CITY,
				STATE,
				POSTAL_CODE,
				COUNTY,
				COUNTRY,
				PHONE_NUMBER
				--FAX_NUMBER
			)
		    SELECT
				ADDRESS_ID,
				USER_ID,
				--NULL
				FIRST_NAME,
				--NULL
				LAST_NAME,
				--NULL
				--NULL
				--NULL
				address1,
				ADDRESS2,
				ADDRESS3,
				city,
				STATE,
				POSTAL_CODE,
				COUNTY,
				country,
				PHONE_NUMBER
				--NULL
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			--MODIFIED FOR CR36
			INSERT INTO TRU_DPSX_CONTACT_INFO
			(
				ID,
				LAST_ACTIVITY,
				STATUS_CD,
				MIG_PROFILE_FLAG,
				MIG_FILE_NAME
			)
			SELECT ADDRESS_ID,
				   LAST_ACTIVITY,
				   10,
				   1,
				   MIG_FILE_NAME
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			--MODIFIED FOR CR36
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
	
			INSERT INTO DPS_CREDIT_CARD
			(
				ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--EXP_DAY_OF_MONTH,
				EXPIRATION_YEAR,
				BILLING_ADDR
			)
			SELECT 
				CREDIT_CARD_ID,
				CREDIT_CARD_NUMBER,
				CREDIT_CARD_TYPE,
				EXPIRATION_MONTH,
				--NONE
				EXPIRATION_YEAR,
				BILLING_ADDRESS	 
			FROM TRU_TEMP_PROFILE_CREDITCARD 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
			
			--MODIFIED FOR CR36
			INSERT INTO TRU_DPSX_CREDIT_CARD
			(
				ID,
				NAME_ON_CARD,
				--CREDIT_CARD_TOKEN,
				--LAST_ACTIVITY
				MIG_PROFILE_FLAG,
				MIG_FILE_NAME
			)
			SELECT 
				   CREDIT_CARD_ID,
				   NAME_ON_CARD,
				   --NULL,
				   --NULL,
				   1,
				   MIG_FILE_NAME
			FROM  TRU_TEMP_PROFILE_CREDITCARD 
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				   
			COMMIT;
			--MODIFIED FOR CR36							
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');
			
			INSERT INTO DPS_USER_ADDRESS
			(
				ID,
				--HOME_ADDR_ID,
				BILLING_ADDR_ID,
				SHIPPING_ADDR_ID
			)
			SELECT 
				USER_ID,
				--NULL
				DEFAULT_BILLING_ADDRESS,
				DEFAULT_SHIPPING_ADDRESS
			FROM TRU_TEMP_PROFILE_USERINFO 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
		
			INSERT INTO DPS_OTHER_ADDR
			(
				USER_ID,
				TAG,
				ADDRESS_ID
			)
			SELECT
				USER_ID,
				NICK_NAME,
				ADDRESS_ID	
			FROM TRU_TEMP_PROFILE_ADDRESS 
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			INSERT INTO DPS_USR_CREDITCARD
			(
				USER_ID,
				TAG,
				CREDIT_CARD_ID
			)
			SELECT USER_ID,
				   case when rn =1 then  TAG else TAG||'##'||(rn-2) end TAG,
				   CREDIT_CARD_ID
			FROM
				   (SELECT 
						USER_ID,
						CREDIT_CARD_TYPE||' - '||SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) TAG,
						row_number() over (partition by USER_ID,SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) order by CREDIT_CARD_ID  desc) rn,
						CREDIT_CARD_ID
					FROM TRU_TEMP_PROFILE_CREDITCARD 
				    )
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
			
			MERGE INTO DPS_USER A
			USING
				(
				    SELECT 
						USER_ID,
						EMAIL,
						--0 AUTO_LOGIN,
						RADIAL_PASSWORD,
						--'ENCRIPT PSWD' PASSWORD_SALT
						--5 PASSWORD_KDF,
						--NULL REALM_ID
						--0 MEMBER,
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL USER_TYPE
						LOCALE,
						LASTACTIVITY_DATE,
						LASTPWDUPDATE,
						--0 GENERATEDPWD,
						REGISTRATION_DATE
						--EMAIL,
						--0 EMAIL_STATUS,
						--1 RECEIVE_EMAIL
						--NULL LAST_EMAILED
						--NULL GENDER
						--NULL DATE_OF_BIRTH
						--NULL SECURITYSTATUS
						--NULL DESCRIPTION
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LOGIN = B.EMAIL ,A.PASSWORD = B.RADIAL_PASSWORD ,A.FIRST_NAME = B.FIRST_NAME ,A.LAST_NAME = B.LAST_NAME ,A.LASTPWDUPDATE = B.LASTPWDUPDATE ,A.REGISTRATION_DATE = B.REGISTRATION_DATE ,A.EMAIL = B.EMAIL,A.LOCALE = B.LOCALE,A.LASTACTIVITY_DATE = B.LASTACTIVITY_DATE
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LOGIN,A.AUTO_LOGIN,A.PASSWORD,A.PASSWORD_SALT,A.PASSWORD_KDF,A.MEMBER,A.FIRST_NAME,A.LAST_NAME,A.LASTPWDUPDATE,A.GENERATEDPWD,A.REGISTRATION_DATE,A.EMAIL,A.EMAIL_STATUS,A.RECEIVE_EMAIL,A.LOCALE,A.LASTACTIVITY_DATE)
				VALUES (B.USER_ID,B.EMAIL,0,B.RADIAL_PASSWORD,G_SALT_PASWD,5,0,B.FIRST_NAME,B.LAST_NAME,B.LASTPWDUPDATE,0,B.REGISTRATION_DATE,B.EMAIL,0,1,B.LOCALE,B.LASTACTIVITY_DATE)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER merge completed');
			
			MERGE INTO TRUCORE_QC.RADIAL_USER A
			USING
				(
				    SELECT 
						 USER_ID,
						 RADIAL_PASSWORD 
						 --NULL LOYAL_CUST
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.RADIAL_PASSWORD = B.RADIAL_PASSWORD
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.RADIAL_PASSWORD)
				VALUES (B.USER_ID,B.RADIAL_PASSWORD)
			LOG ERRORS INTO DML_ERROR_LOG ('TRUCORE_QC.RADIAL_USER') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRUCORE_QC.RADIAL_USER merge completed');
			
			MERGE INTO TRU_DPSX_USER A
			USING
				(
				    SELECT 
						 USER_ID,
						 REWARD_NUMBER,MIG_FILE_NAME 
						 --NULL LOYAL_CUST
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.REWARD_NUMBER = B.REWARD_NUMBER , A.MIG_FILE_NAME = B.MIG_FILE_NAME,A.MIG_PROFILE_FLAG =1
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.REWARD_NUMBER,A.MIG_FILE_NAME,A.MIG_PROFILE_FLAG)
				VALUES (B.USER_ID,B.REWARD_NUMBER,B.MIG_FILE_NAME,1)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER merge completed');
			
			
						
			MERGE INTO DPS_CONTACT_INFO A
			USING
				(
				     SELECT
						ADDRESS_ID,
						USER_ID,
						--NULL PREFIX
						FIRST_NAME,
						--NULL MIDDLE_NAME
						LAST_NAME,
						--NULL SUFFIX
						--NULL JOB_TITLE
						--NULL COMPANY_NAME
						address1,
						ADDRESS2,
						ADDRESS3,
						city,
						STATE,
						POSTAL_CODE,
						COUNTY,
						country,
						PHONE_NUMBER
						--NULL FAX_NUMBER
					FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.USER_ID = B.USER_ID,A.FIRST_NAME = B.FIRST_NAME,A.LAST_NAME = B.LAST_NAME,A.ADDRESS1 = B.ADDRESS1,A.ADDRESS2 = B.ADDRESS2,A.ADDRESS3 = B.ADDRESS3,
						   A.CITY = B.CITY,A.STATE = B.STATE,A.POSTAL_CODE = B.POSTAL_CODE,A.COUNTY = B.COUNTY,A.country = B.country,A.PHONE_NUMBER = B.PHONE_NUMBER
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.USER_ID,A.FIRST_NAME,A.LAST_NAME,A.ADDRESS1,A.ADDRESS2,A.ADDRESS3,A.CITY,A.STATE,A.POSTAL_CODE,A.COUNTY,A.COUNTRY,A.PHONE_NUMBER)
				VALUES (B.ADDRESS_ID,B.USER_ID,B.FIRST_NAME,B.LAST_NAME,B.ADDRESS1,B.ADDRESS2,B.ADDRESS3,B.CITY,B.STATE,B.POSTAL_CODE,B.COUNTY,B.COUNTRY,B.PHONE_NUMBER)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO merge completed');
			
			MERGE INTO TRU_DPSX_CONTACT_INFO A
			USING
				(
				     SELECT ADDRESS_ID,
							LAST_ACTIVITY,MIG_FILE_NAME
							--NULL STATUS_CD
					 FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.ID=B.ADDRESS_ID)
			WHEN MATCHED THEN
				UPDATE SET A.LAST_ACTIVITY = B.LAST_ACTIVITY,A.MIG_FILE_NAME=B.MIG_FILE_NAME,A.MIG_PROFILE_FLAG =1
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.LAST_ACTIVITY,A.STATUS_CD,A.MIG_FILE_NAME,A.MIG_PROFILE_FLAG)
				VALUES (B.ADDRESS_ID,B.LAST_ACTIVITY,10,B.MIG_FILE_NAME,1)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO merge completed');
	
			MERGE INTO DPS_CREDIT_CARD A
			USING
				(
				     SELECT 
							CREDIT_CARD_ID,
							CREDIT_CARD_NUMBER,
							CREDIT_CARD_TYPE,
							EXPIRATION_MONTH,
							--NONE EXP_DAY_OF_MONTH
							EXPIRATION_YEAR,
							BILLING_ADDRESS	
				    FROM TRU_TEMP_PROFILE_CREDITCARD 
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.CREDIT_CARD_NUMBER = B.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE = B.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH = B.EXPIRATION_MONTH,A.EXPIRATION_YEAR = B.EXPIRATION_YEAR,A.BILLING_ADDR = B.BILLING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.CREDIT_CARD_NUMBER,A.CREDIT_CARD_TYPE,A.EXPIRATION_MONTH,A.EXPIRATION_YEAR,A.BILLING_ADDR)
				VALUES (B.CREDIT_CARD_ID,B.CREDIT_CARD_NUMBER,B.CREDIT_CARD_TYPE,B.EXPIRATION_MONTH,B.EXPIRATION_YEAR,B.BILLING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD merge completed');
			
			MERGE INTO TRU_DPSX_CREDIT_CARD A
			USING
				(
				    SELECT 
						   CREDIT_CARD_ID,
						   NAME_ON_CARD,MIG_FILE_NAME
						   --NULL CREDIT_CARD_TOKEN,
						   --NULL LAST_ACTIVITY
					FROM  TRU_TEMP_PROFILE_CREDITCARD 
				) B
			ON (A.ID=B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.NAME_ON_CARD = B.NAME_ON_CARD,A.MIG_FILE_NAME=B.MIG_FILE_NAME,A.MIG_PROFILE_FLAG =1
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.NAME_ON_CARD,A.MIG_FILE_NAME,A.MIG_PROFILE_FLAG)
				VALUES (B.CREDIT_CARD_ID,B.NAME_ON_CARD,B.MIG_FILE_NAME,1)
			LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;	

				   
			COMMIT;
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD merge completed');
			
			MERGE INTO DPS_USER_ADDRESS A
			USING
				(
				    SELECT 
						USER_ID,
						--NULL HOME_ADDR_ID
						DEFAULT_BILLING_ADDRESS,
						DEFAULT_SHIPPING_ADDRESS
					FROM TRU_TEMP_PROFILE_USERINFO 
				) B
			ON (A.ID=B.USER_ID)
			WHEN MATCHED THEN
				UPDATE SET A.BILLING_ADDR_ID = B.DEFAULT_BILLING_ADDRESS,A.SHIPPING_ADDR_ID = B.DEFAULT_SHIPPING_ADDRESS
			WHEN NOT MATCHED THEN
				INSERT (A.ID,A.BILLING_ADDR_ID,A.SHIPPING_ADDR_ID)
				VALUES (B.USER_ID,B.DEFAULT_BILLING_ADDRESS,B.DEFAULT_SHIPPING_ADDRESS)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS merge completed');
			
						
			MERGE INTO DPS_OTHER_ADDR A
			USING
				(
				    SELECT
						USER_ID,
						NICK_NAME,
						ADDRESS_ID	
					FROM TRU_TEMP_PROFILE_ADDRESS 
				) B
			ON (A.USER_ID=B.USER_ID AND A.TAG = B.NICK_NAME)
			WHEN MATCHED THEN
				UPDATE SET A.ADDRESS_ID = B.ADDRESS_ID
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.ADDRESS_ID)
				VALUES (B.USER_ID,B.NICK_NAME,B.ADDRESS_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;	

			
			COMMIT;
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR merge completed');
			
									
			MERGE INTO DPS_USR_CREDITCARD A
			USING
				(
				  SELECT USER_ID,
						 case when rn =1 then  TAG else TAG||'##'||(rn-2) end TAG,
						 CREDIT_CARD_ID
				  FROM
				   (SELECT 
						USER_ID,
						CREDIT_CARD_TYPE||' - '||SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) TAG,
						row_number() over (partition by USER_ID,SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) order by CREDIT_CARD_ID  desc) rn,
						CREDIT_CARD_ID
					FROM TRU_TEMP_PROFILE_CREDITCARD 
				    )
				) B
			ON (A.USER_ID=B.USER_ID AND A.CREDIT_CARD_ID = B.CREDIT_CARD_ID)
			WHEN MATCHED THEN
				UPDATE SET A.TAG = B.TAG
			WHEN NOT MATCHED THEN
				INSERT (A.USER_ID,A.TAG,A.CREDIT_CARD_ID)
				VALUES (B.USER_ID,B.TAG,B.CREDIT_CARD_ID)
			LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;	
			
			
			COMMIT;

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD merge completed');
			

		END IF;
		
	    MERGE INTO TRU_TEMP_MIG_FILE_STATUS A
		USING
		 (SELECT PROF.MIG_FILE_NAME,nvl(PROF_COUNT,0) PROF_COUNT,nvl(ADDR_COUNT,0) ADDR_COUNT,nvl(CC_COUNT,0) CC_COUNT
		  FROM
		  ( SELECT COUNT(1) PROF_COUNT,MIG_FILE_NAME FROM TRU_DPSX_USER A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_USERINFO B WHERE A.ID = B.USER_ID)
			GROUP BY MIG_FILE_NAME
		  )PROF,
		  ( SELECT COUNT(1) ADDR_COUNT,MIG_FILE_NAME FROM TRU_DPSX_CONTACT_INFO A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_ADDRESS B WHERE A.ID = B.ADDRESS_ID)
			GROUP BY MIG_FILE_NAME
		  )ADDRS,
		  ( SELECT COUNT(1) CC_COUNT,MIG_FILE_NAME FROM TRU_DPSX_CREDIT_CARD A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_CREDITCARD B WHERE A.ID = B.CREDIT_CARD_ID)
			GROUP BY MIG_FILE_NAME
		  )CC
		  WHERE PROF.MIG_FILE_NAME = ADDRS.MIG_FILE_NAME(+) AND
				PROF.MIG_FILE_NAME = CC.MIG_FILE_NAME(+)
		 )B
		 ON(A.FILE_NAME = B.MIG_FILE_NAME AND START_DATE = (SELECT MAX(START_DATE) FROM  TRU_TEMP_MIG_FILE_STATUS WHERE FILE_NAME = B.MIG_FILE_NAME AND JOB_NAME = 'PROF_MIG'))
		 WHEN MATCHED THEN UPDATE SET A.TOTAL_PROF_REC_LOADED_TRGT = B.PROF_COUNT ,TOTAL_ADDS_REC_LOADED_TRGT = B.ADDR_COUNT ,TOTAL_CC_REC_LOADED_TRGT = B.CC_COUNT,STATUS = 'target_load_completed';
		 COMMIT;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_MIG_TARGT;


	PROCEDURE PROFILE_MIG_TARGT_BATCH(P_LAOD_TYPE  VARCHAR2,P_COMMIT_INTRVL PLS_INTEGER)
    IS
		
		type l_rcrd is record 
			(
				USER_ID                 TRU_TEMP_PROFILE_USERINFO.USER_ID%type, 
				EMAIL                   TRU_TEMP_PROFILE_USERINFO.EMAIL%type,
				FIRST_NAME              TRU_TEMP_PROFILE_USERINFO.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_USERINFO.LAST_NAME%type,
				LOCALE					TRU_TEMP_PROFILE_USERINFO.LOCALE%type,
				LASTACTIVITY_DATE		TRU_TEMP_PROFILE_USERINFO.LASTACTIVITY_DATE%type,
				LASTPWDUPDATE           TRU_TEMP_PROFILE_USERINFO.LASTPWDUPDATE%type,
				REGISTRATION_DATE       TRU_TEMP_PROFILE_USERINFO.REGISTRATION_DATE%type,
				RADIAL_PASSWORD         TRU_TEMP_PROFILE_USERINFO.RADIAL_PASSWORD%type,
				REWARD_NUMBER 			TRU_TEMP_PROFILE_USERINFO.REWARD_NUMBER%type,
				DEFAULT_BILLING_ADDRESS	TRU_TEMP_PROFILE_USERINFO.DEFAULT_BILLING_ADDRESS%type,
				DEFAULT_SHIPPING_ADDRESS TRU_TEMP_PROFILE_USERINFO.DEFAULT_SHIPPING_ADDRESS%type,
				MIG_FILE_NAME			 TRU_TEMP_PROFILE_USERINFO.MIG_FILE_NAME%type
				
			);
		
		type l_rcrd1 is record 
			(
				ADDRESS_ID              TRU_TEMP_PROFILE_ADDRESS.ADDRESS_ID%type, 
				USER_ID                 TRU_TEMP_PROFILE_ADDRESS.USER_ID%type,
				FIRST_NAME              TRU_TEMP_PROFILE_ADDRESS.FIRST_NAME%type,
				LAST_NAME               TRU_TEMP_PROFILE_ADDRESS.LAST_NAME%type,
				address1           		TRU_TEMP_PROFILE_ADDRESS.address1%type,
				address2       			TRU_TEMP_PROFILE_ADDRESS.address2%type,
				address3 				TRU_TEMP_PROFILE_ADDRESS.address3%type,
				city					TRU_TEMP_PROFILE_ADDRESS.city%type,
				STATE					TRU_TEMP_PROFILE_ADDRESS.STATE%type,
				POSTAL_CODE				TRU_TEMP_PROFILE_ADDRESS.POSTAL_CODE%type,
				COUNTY					TRU_TEMP_PROFILE_ADDRESS.COUNTY%type,
				country					TRU_TEMP_PROFILE_ADDRESS.country%type,
				PHONE_NUMBER			TRU_TEMP_PROFILE_ADDRESS.PHONE_NUMBER%type,
				NICK_NAME				TRU_TEMP_PROFILE_ADDRESS.NICK_NAME%type,
				LAST_ACTIVITY		    TRU_TEMP_PROFILE_ADDRESS.LAST_ACTIVITY%type,
				MIG_FILE_NAME			TRU_TEMP_PROFILE_ADDRESS.MIG_FILE_NAME%type
			);
			
		type l_rcrd2 is record 
			(
				CREDIT_CARD_ID			TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_ID%TYPE,
				USER_ID             	TRU_TEMP_PROFILE_CREDITCARD.USER_ID%TYPE,
				CREDIT_CARD_NUMBER		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_NUMBER%type,
				CREDIT_CARD_TYPE		TRU_TEMP_PROFILE_CREDITCARD.CREDIT_CARD_TYPE%type,
				EXPIRATION_MONTH		TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_MONTH%type,
				EXPIRATION_YEAR			TRU_TEMP_PROFILE_CREDITCARD.EXPIRATION_YEAR%type,
				BILLING_ADDRESS			TRU_TEMP_PROFILE_CREDITCARD.BILLING_ADDRESS%TYPE,
				NAME_ON_CARD        	TRU_TEMP_PROFILE_CREDITCARD.NAME_ON_CARD%TYPE,
				TAG						VARCHAR2(255),
				MIG_FILE_NAME			TRU_TEMP_PROFILE_CREDITCARD.MIG_FILE_NAME%type
			);
				
		type USER_COL  is table of l_rcrd index by pls_integer; 
		L_USER_INFO USER_COL;
		
		type ADDRS_COL  is table of l_rcrd1 index by pls_integer; 
		L_USER_ADDRS ADDRS_COL;
		
		type CC_COL  is table of l_rcrd2 index by pls_integer; 
		L_USER_CC CC_COL;
		
		ROWS PLS_INTEGER;
		
		L_DYN_CUR SYS_REFCURSOR;
		
		
    BEGIN
		
		G_START :=DBMS_UTILITY.GET_TIME;
		
		IF P_COMMIT_INTRVL IS NULL OR P_COMMIT_INTRVL < 10000
		THEN
			
			RAISE_APPLICATION_ERROR(-20102, 'commit inerval value should be greater than or equal to 10000' );
			
		END IF;
		
		ROWS := P_COMMIT_INTRVL;
		
		IF LOWER(P_LAOD_TYPE) not in('initial','delta') or  P_LAOD_TYPE is null
		THEN
			
			RAISE_APPLICATION_ERROR(-20103, 'load type should be either initial or delta ' );
			
		END IF;
		
		IF lower(P_LAOD_TYPE) = 'initial'
		THEN
			
			 OPEN L_DYN_CUR FOR  SELECT 
												USER_ID,
												EMAIL,
												--0 AUTO_LOGIN,
												--ENCRIPT PSWD PASSWORD
												--ENCRIPT PSWD PASSWORD_SALT
												--5 PASSWORD_KDF,
												--NULL REALM_ID
												--0 MEMBER,
												FIRST_NAME,
												--NULL MIDDLE_NAME
												LAST_NAME,
												--NULL USER_TYPE
												LOCALE,
												LASTACTIVITY_DATE,
												LASTPWDUPDATE,
												--0 GENERATEDPWD,
												REGISTRATION_DATE,
												RADIAL_PASSWORD,
												--EMAIL,
												--0 EMAIL_STATUS,
												--1 RECEIVE_EMAIL
												--NULL LAST_EMAILED
												--NULL GENDER
												--NULL DATE_OF_BIRTH
												--NULL SECURITYSTATUS
												--NULL DESCRIPTION
												REWARD_NUMBER,
												DEFAULT_BILLING_ADDRESS,
												DEFAULT_SHIPPING_ADDRESS,
												MIG_FILE_NAME
											FROM TRU_TEMP_PROFILE_USERINFO 
                    ;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_INFO LIMIT ROWS;
                  EXIT WHEN L_USER_INFO.count=0;
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO DPS_USER 
						(ID,LOGIN,AUTO_LOGIN,PASSWORD,PASSWORD_SALT,PASSWORD_KDF,MEMBER,FIRST_NAME,LAST_NAME,LASTPWDUPDATE,GENERATEDPWD,REGISTRATION_DATE,EMAIL,EMAIL_STATUS,RECEIVE_EMAIL,
							LOCALE,LASTACTIVITY_DATE
						)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).EMAIL,0,L_USER_INFO(i).RADIAL_PASSWORD,G_SALT_PASWD,5,0,L_USER_INFO(i).FIRST_NAME,L_USER_INFO(i).LAST_NAME,L_USER_INFO(i).LASTPWDUPDATE,0,L_USER_INFO(i).REGISTRATION_DATE,L_USER_INFO(i).EMAIL,0,1,
						 L_USER_INFO(i).LOCALE,L_USER_INFO(i).LASTACTIVITY_DATE
						)
					  LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER') REJECT LIMIT UNLIMITED;
					
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO TRUCORE_QC.RADIAL_USER 
						(ID,RADIAL_PASSWORD)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).RADIAL_PASSWORD)
					  LOG ERRORS INTO DML_ERROR_LOG ('TRUCORE_QC.RADIAL_USER') REJECT LIMIT UNLIMITED;
					  
                  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST
					  INSERT INTO TRU_DPSX_USER 
						(ID,REWARD_NUMBER,MIG_FILE_NAME,MIG_PROFILE_FLAG)
					  values
						(L_USER_INFO(i).USER_ID,L_USER_INFO(i).REWARD_NUMBER,L_USER_INFO(i).MIG_FILE_NAME,1)
					  LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_USER') REJECT LIMIT UNLIMITED;
					  
				  FORALL I IN L_USER_INFO.FIRST .. L_USER_INFO.LAST 			
					INSERT INTO DPS_USER_ADDRESS
					(
						ID,
						--HOME_ADDR_ID,
						BILLING_ADDR_ID,
						SHIPPING_ADDR_ID
					)
					VALUES
					(
						L_USER_INFO(i).USER_ID,
						--NULL
						L_USER_INFO(I).DEFAULT_BILLING_ADDRESS,
						L_USER_INFO(i).DEFAULT_SHIPPING_ADDRESS
					)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_USER_ADDRESS') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_USER load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_USER_ADDRESS load completed');
			
			
			 OPEN L_DYN_CUR FOR 	SELECT
										ADDRESS_ID,
										USER_ID,
										--NULL PREFIX
										FIRST_NAME,
										--NULL MIDDLE_NAME
										LAST_NAME,
										--NULL SUFFIX
										--NULL JOB_TITLE
										--NULL COMPANY_NAME
										address1,
										ADDRESS2,
										ADDRESS3,
										city,
										STATE,
										POSTAL_CODE,
										COUNTY,
										country,
										PHONE_NUMBER,
										NICK_NAME,
										LAST_ACTIVITY,
										--NULL FAX_NUMBER
										MIG_FILE_NAME
									FROM TRU_TEMP_PROFILE_ADDRESS;

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_ADDRS LIMIT ROWS;
                  EXIT WHEN L_USER_ADDRS.count=0;
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO  DPS_CONTACT_INFO
						(ID,USER_ID,FIRST_NAME,LAST_NAME,ADDRESS1,ADDRESS2,ADDRESS3,CITY,STATE,POSTAL_CODE,COUNTY,COUNTRY,PHONE_NUMBER)
					VALUES 
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).USER_ID,L_USER_ADDRS(I).FIRST_NAME,L_USER_ADDRS(I).LAST_NAME,L_USER_ADDRS(I).ADDRESS1,L_USER_ADDRS(I).ADDRESS2,
						 L_USER_ADDRS(I).ADDRESS3,L_USER_ADDRS(I).CITY,L_USER_ADDRS(I).STATE,L_USER_ADDRS(I).POSTAL_CODE,L_USER_ADDRS(I).COUNTY,L_USER_ADDRS(I).COUNTRY,L_USER_ADDRS(I).PHONE_NUMBER)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_CONTACT_INFO') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
					INSERT INTO TRU_DPSX_CONTACT_INFO
						(
							ID,
							LAST_ACTIVITY,
							STATUS_CD,
							MIG_FILE_NAME,
							MIG_PROFILE_FLAG
						)
					VALUES
						(L_USER_ADDRS(I).ADDRESS_ID,L_USER_ADDRS(I).LAST_ACTIVITY,10,L_USER_ADDRS(I).MIG_FILE_NAME,1)
					LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CONTACT_INFO') REJECT LIMIT UNLIMITED;
				  
				  FORALL I IN L_USER_ADDRS.FIRST .. L_USER_ADDRS.LAST
							
					INSERT INTO DPS_OTHER_ADDR
						(
							USER_ID,
							TAG,
							ADDRESS_ID
						)
					VALUES
						(
							L_USER_ADDRS(I).USER_ID,
							L_USER_ADDRS(I).NICK_NAME,
							L_USER_ADDRS(I).ADDRESS_ID
						)
					LOG ERRORS INTO DML_ERROR_LOG ('DPS_OTHER_ADDR') REJECT LIMIT UNLIMITED;
         
                COMMIT;
            END LOOP;
			
			
			
			DBMS_OUTPUT.PUT_LINE ('DPS_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CONTACT_INFO load completed');
			
			DBMS_OUTPUT.PUT_LINE ('DPS_OTHER_ADDR load completed');
			
			
		
			 OPEN L_DYN_CUR FOR SELECT CREDIT_CARD_ID,
										USER_ID,
										CREDIT_CARD_NUMBER,
										CREDIT_CARD_TYPE,
										EXPIRATION_MONTH,
										--NONE EXP_DAY_OF_MONTH
										EXPIRATION_YEAR,
										BILLING_ADDRESS,
										NAME_ON_CARD,
										case when rn =1 then  TAG else TAG||'##'||(rn-2) end TAG,
										MIG_FILE_NAME
								FROM
			 
									(SELECT 
										CREDIT_CARD_ID,
										USER_ID,
										CREDIT_CARD_NUMBER,
										CREDIT_CARD_TYPE,
										EXPIRATION_MONTH,
										--NONE EXP_DAY_OF_MONTH
										EXPIRATION_YEAR,
										BILLING_ADDRESS,
										NAME_ON_CARD,
										CREDIT_CARD_TYPE||' - '||SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) TAG,
										row_number() over (partition by USER_ID,SUBSTR(CREDIT_CARD_NUMBER,LENGTH(CREDIT_CARD_NUMBER) -4 +1,LENGTH(CREDIT_CARD_NUMBER)) order by CREDIT_CARD_ID  desc) rn,
										MIG_FILE_NAME
									  FROM TRU_TEMP_PROFILE_CREDITCARD
									);

             LOOP
                  FETCH L_DYN_CUR BULK COLLECT INTO L_USER_CC LIMIT ROWS;
                  EXIT WHEN L_USER_CC.count=0;
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
						INSERT INTO DPS_CREDIT_CARD
							(
								ID,
								CREDIT_CARD_NUMBER,
								CREDIT_CARD_TYPE,
								EXPIRATION_MONTH,
								EXPIRATION_YEAR,
								BILLING_ADDR
							
							)
						VALUES
							( 
								L_USER_CC(I).CREDIT_CARD_ID,
								L_USER_CC(I).CREDIT_CARD_NUMBER,
								L_USER_CC(I).CREDIT_CARD_TYPE,
								L_USER_CC(I).EXPIRATION_MONTH,
								L_USER_CC(I).EXPIRATION_YEAR,
								L_USER_CC(I).BILLING_ADDRESS	
							)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_CREDIT_CARD') REJECT LIMIT UNLIMITED;
					
				  
                  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST
								
						INSERT INTO TRU_DPSX_CREDIT_CARD
						   (
								ID,
								NAME_ON_CARD,
								--CREDIT_CARD_TOKEN,
								--LAST_ACTIVITY
								MIG_FILE_NAME,
								MIG_PROFILE_FLAG
							)
						VALUES
							(
							   L_USER_CC(I).CREDIT_CARD_ID,
							   L_USER_CC(I).NAME_ON_CARD,
							   --NULL,
							   --NULL
							   L_USER_CC(I).MIG_FILE_NAME,
							   1
							)
						LOG ERRORS INTO DML_ERROR_LOG ('TRU_DPSX_CREDIT_CARD') REJECT LIMIT UNLIMITED;
				 
				  FORALL I IN L_USER_CC.FIRST .. L_USER_CC.LAST

						INSERT INTO DPS_USR_CREDITCARD
						(
							USER_ID,
							TAG,
							CREDIT_CARD_ID
						)
						VALUES
						(
							L_USER_CC(I).USER_ID,
							L_USER_CC(I).TAG,
							L_USER_CC(I).CREDIT_CARD_ID
						)
						LOG ERRORS INTO DML_ERROR_LOG ('DPS_USR_CREDITCARD') REJECT LIMIT UNLIMITED;

         
                COMMIT;
            END LOOP;
	

			
			DBMS_OUTPUT.PUT_LINE ('DPS_CREDIT_CARD load completed');
										
			DBMS_OUTPUT.PUT_LINE ('TRU_DPSX_CREDIT_CARD load completed');

			DBMS_OUTPUT.PUT_LINE ('DPS_USR_CREDITCARD load completed');
			
		END IF;
		
	    IF lower(P_LAOD_TYPE) = 'delta'
		THEN
					
			RAISE_APPLICATION_ERROR(-20104, 'Run delta load using TRU_PROFILE_MIG_PKG.PROFILE_MIG_TARGT procedure ' );

		END IF;
		
		MERGE INTO TRU_TEMP_MIG_FILE_STATUS A
		USING
		 (SELECT PROF.MIG_FILE_NAME,nvl(PROF_COUNT,0) PROF_COUNT,nvl(ADDR_COUNT,0) ADDR_COUNT,nvl(CC_COUNT,0) CC_COUNT
		  FROM
		  ( SELECT COUNT(1) PROF_COUNT,MIG_FILE_NAME FROM TRU_DPSX_USER A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_USERINFO B WHERE A.ID = B.USER_ID)
			GROUP BY MIG_FILE_NAME
		  )PROF,
		  ( SELECT COUNT(1) ADDR_COUNT,MIG_FILE_NAME FROM TRU_DPSX_CONTACT_INFO A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_ADDRESS B WHERE A.ID = B.ADDRESS_ID)
			GROUP BY MIG_FILE_NAME
		  )ADDRS,
		  ( SELECT COUNT(1) CC_COUNT,MIG_FILE_NAME FROM TRU_DPSX_CREDIT_CARD A WHERE EXISTS (SELECT 1 FROM TRU_TEMP_PROFILE_CREDITCARD B WHERE A.ID = B.CREDIT_CARD_ID)
			GROUP BY MIG_FILE_NAME
		  )CC
		  WHERE PROF.MIG_FILE_NAME = ADDRS.MIG_FILE_NAME(+) AND
				PROF.MIG_FILE_NAME = CC.MIG_FILE_NAME(+)
		 )B
		 ON(A.FILE_NAME = B.MIG_FILE_NAME AND START_DATE = (SELECT MAX(START_DATE) FROM  TRU_TEMP_MIG_FILE_STATUS WHERE FILE_NAME = B.MIG_FILE_NAME AND JOB_NAME = 'PROF_MIG'))
		 WHEN MATCHED THEN UPDATE SET A.TOTAL_PROF_REC_LOADED_TRGT = B.PROF_COUNT ,TOTAL_ADDS_REC_LOADED_TRGT = B.ADDR_COUNT ,TOTAL_CC_REC_LOADED_TRGT = B.CC_COUNT,
		  STATUS = 'target_load_completed';
		 COMMIT;
		
		DBMS_OUTPUT.PUT_LINE ('All Target table load completed');
		
		G_END :=DBMS_UTILITY.GET_TIME;
        DBMS_OUTPUT.PUT_LINE('PROFILE_MIG_TARGT_BATCH: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
	  EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	end PROFILE_MIG_TARGT_BATCH;
	
	FUNCTION  GET_MAPPED_STATE_CODE(P_STATE_CODE IN VARCHAR2) RETURN VARCHAR2
	IS
		V_MAPPED_VALUE VARCHAR2(255) := null;
		BEGIN
		FOR I IN ( select * from TRU_TEMP_PROFILE_STATE )
		LOOP
			IF   lower(P_STATE_CODE) = lower(I.STATE_CODE)
			THEN
			  V_MAPPED_VALUE :=  I.VALID_STATE_CODE;
			EXIT;
			end if;
		END LOOP;
		RETURN V_MAPPED_VALUE;
	EXCEPTION WHEN OTHERS THEN
	RETURN NULL;
	END GET_MAPPED_STATE_CODE;
	
	FUNCTION  GET_MAPPED_COUNTRY_CODE(P_COUNTRY_CODE IN VARCHAR2) RETURN VARCHAR2
	IS
		V_MAPPED_VALUE VARCHAR2(255) := null;
		BEGIN
		FOR I IN ( select * from TRU_TEMP_PROFILE_COUNTRY )
		LOOP
			IF   lower(P_COUNTRY_CODE) = lower(I.COUNTRY_CODE)
			THEN
			  V_MAPPED_VALUE := I.VALID_COUNTRY_CODE;
			EXIT;
			
			end if;
		END LOOP;
		RETURN V_MAPPED_VALUE;
	EXCEPTION WHEN OTHERS THEN
	RETURN NULL;
	END GET_MAPPED_COUNTRY_CODE; 
	
	PROCEDURE PROFILE_STAGE_COUNTRY_LOAD (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_FILE_NAME VARCHAR2(4000);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  FILE_HANDLE           UTL_FILE.FILE_TYPE;
	  V_COUNTRY_CODE TRU_TEMP_PROFILE_COUNTRY.COUNTRY_CODE%TYPE;
	  V_VALID_COUNTRY_CODE TRU_TEMP_PROFILE_COUNTRY.VALID_COUNTRY_CODE%TYPE;
	  V_STRING VARCHAR2(1000);
	  V_CNT NUMBER;
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_COUNTRY';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_COUNTRY';

		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_COUNTRY')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				 INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,FILE_SIZE,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,G_FILE_SIZE,'PROF_COUNTRY',SYSTIMESTAMP);
				 V_CNT := 0;
				 
				 insert into TRU_TEMP_PROFILE_COUNTRY (COUNTRY_CODE,VALID_COUNTRY_CODE) 
						 SELECT COUNTRY_CODE,VALID_COUNTRY_CODE
						 FROM
							(SELECT nvl(STRING_VAL,number_val) COUNTRY_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%A%') A_CELL
							FULL OUTER JOIN
							(SELECT nvl(STRING_VAL,number_val) VALID_COUNTRY_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%B%') B_CELL
							ON(A_CELL.CELL_VALUE = B_CELL.CELL_VALUE)
							WHERE A_CELL.CELL_VALUE <>1 ORDER BY A_CELL.CELL_VALUE;
						 V_CNT := sql%rowcount;
				 
				 UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_country_load_completed',end_date = systimestamp,exec_time = start_date -systimestamp, TOTAL_REC = V_CNT WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_COUNTRY';
				 COMMIT;
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_country_load_failed',ERROR_CODE=V_ERR_CD,ERROR_MSG=V_ERR  WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_COUNTRY';
				commit;
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_STAGE_COUNTRY_LOAD: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_STAGE_COUNTRY_LOAD;
	
	PROCEDURE PROFILE_STAGE_STATE_LOAD (P_DIR_NAME VARCHAR2,P_FILE_NAME VARCHAR2,P_TRUNCATE VARCHAR2)
    IS
	  L_FILE_EXISTS VARCHAR2(1000);
	  L_FILE_NAME VARCHAR2(4000);
	  l_ex BOOLEAN;
	  l_flen NUMBER;
	  l_bsize NUMBER;
	  V_ERR VARCHAR2(255);
	  V_ERR_CD VARCHAR2(255);
	  FILE_HANDLE           UTL_FILE.FILE_TYPE;
	  V_STATE_CODE TRU_TEMP_PROFILE_STATE.STATE_CODE%TYPE;
	  V_VALID_STATE_CODE TRU_TEMP_PROFILE_STATE.VALID_STATE_CODE%TYPE;
	  V_STRING VARCHAR2(1000);
	  V_CNT NUMBER;
    BEGIN

		G_START :=DBMS_UTILITY.GET_TIME;
		
		--Checking XML file name exists or not in the directory
		L_FILE_EXISTS := TRU_PROFILE_MIG_PKG.XML_FILEEXISTS(P_DIRNAME => P_DIR_NAME,P_FILENAME => P_FILE_NAME);
		
		IF L_FILE_EXISTS <> 'EXISTS'
		THEN
			RAISE_APPLICATION_ERROR(-20101, L_FILE_EXISTS );
		END IF;
		
		L_FILE_NAME := P_FILE_NAME;
		
		IF lower(P_TRUNCATE) not in('y','n') or P_TRUNCATE is null
		THEN
			RAISE_APPLICATION_ERROR(-20104, 'P_TRUNCATE value should be either y or n' );
		END IF;
		
		IF lower(P_TRUNCATE) = 'y'
		THEN
			--Clearing all records in profile migration staging tables
			DELETE FROM TRU_TEMP_MIG_FILE_STATUS WHERE  JOB_NAME = 'PROF_STATE';
			EXECUTE IMMEDIATE 'TRUNCATE TABLE TRU_TEMP_PROFILE_STATE';

		ELSIF lower(P_TRUNCATE) = 'n'
		THEN
			FOR V_FNAM IN (SELECT FILE_NAME||',' FILE_NAME FROM TRU_TEMP_MIG_FILE_STATUS WHERE JOB_NAME = 'PROF_STATE')
			LOOP
				SELECT REPLACE(L_FILE_NAME,V_FNAM.FILE_NAME,NULL) INTO L_FILE_NAME FROM DUAL;
				DBMS_OUTPUT.PUT_LINE('FILE_NAME TRIM : '||L_FILE_NAME); 
			END LOOP;
		END IF;
		

		FOR I IN (SELECT EXTRACT (VALUE (d), '//row/text()').getstringval() FILE_NAME 
					   FROM
						   (select xmltype (   '<rows><row>'
							|| REPLACE (L_FILE_NAME, ',', '</row><row>')
							|| '</row></rows>'
						   ) AS xmlval
							from DUAL) X,
						table (XMLSEQUENCE (extract (X.XMLVAL, '/rows/row')))D
				 )
		LOOP
			
			 --Gettin file zize in MB
			 UTL_FILE.FGETATTR(UPPER(P_DIR_NAME),I.FILE_NAME, L_EX, L_FLEN, L_BSIZE);
			 G_FILE_SIZE := round(L_FLEN/1000000,2);
			 
			BEGIN
				 INSERT INTO TRU_TEMP_MIG_FILE_STATUS(FILE_NAME,FILE_SIZE,JOB_NAME,START_DATE) VALUES(I.FILE_NAME,G_FILE_SIZE,'PROF_STATE',SYSTIMESTAMP);
				 V_CNT := 0;
					insert into TRU_TEMP_PROFILE_STATE (STATE_CODE,VALID_STATE_CODE) 
						 SELECT STATE_CODE,VALID_STATE_CODE
						 FROM
							(SELECT nvl(STRING_VAL,number_val) STATE_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%A%') A_CELL
							FULL OUTER JOIN
							(SELECT nvl(STRING_VAL,number_val) VALID_STATE_CODE,TO_NUMBER(REGEXP_SUBSTR(CELL, '[[:digit:]]+$')) CELL_VALUE FROM TABLE( AS_READ_XLSX.READ( AS_READ_XLSX.FILE2BLOB( P_DIR_NAME, I.FILE_NAME ) ) )
							WHERE CELL LIKE '%B%') B_CELL
							ON(A_CELL.CELL_VALUE = B_CELL.CELL_VALUE)
							WHERE A_CELL.CELL_VALUE <>1 ORDER BY A_CELL.CELL_VALUE;
						 V_CNT := sql%rowcount;
				 UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_state_load_completed',end_date = systimestamp,exec_time = start_date -systimestamp, TOTAL_REC = V_CNT WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_STATE';
				 COMMIT;
			EXCEPTION WHEN OTHERS THEN
				V_ERR := SUBSTR(SQLERRM, 1, 200);
				V_ERR_CD := SQLCODE;
				UPDATE TRU_TEMP_MIG_FILE_STATUS SET STATUS='temp_state_load_failed',ERROR_CODE=V_ERR_CD,ERROR_MSG=V_ERR  WHERE FILE_NAME = I.FILE_NAME AND JOB_NAME='PROF_STATE';
				commit;
				GOTO LOOP1;
			END;
			
            <<LOOP1>>
			null;
			
		END LOOP;	
        
		G_END :=DBMS_UTILITY.GET_TIME;
		DBMS_OUTPUT.PUT_LINE('PROFILE_STAGE_STATE_LOAD: Time Taken to execute ' || (G_END-G_START)/100||' '||'seconds');
		
		EXCEPTION WHEN OTHERS THEN
		RAISE;
		
	END PROFILE_STAGE_STATE_LOAD;
	
	
END TRU_PROFILE_MIG_PKG;
/