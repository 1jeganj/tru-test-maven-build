DROP TABLE radial_user;

CREATE TABLE radial_user (
	id 			varchar2(254)	NOT NULL REFERENCES dps_user(id),
	radial_user 		number(1)	NULL,
	radial_password 	varchar2(254)	NULL,
	CHECK (radial_user IN (0, 1)),
	PRIMARY KEY(id)
);
COMMIT;

ALTER TABLE TRU_DPSX_CONTACT_INFO ADD (IS_BILLING_ADDRESS NUMBER(1,0));

CREATE TABLE tru_bill_addr (
	id 			varchar2(254)	NOT NULL,
	user_id 		varchar2(254)	NULL,
	prefix 			varchar2(254)	NULL,
	first_name 		varchar2(254)	NULL,
	middle_name 		varchar2(254)	NULL,
	last_name 		varchar2(254)	NULL,
	suffix 			varchar2(254)	NULL,
	job_title 		varchar2(254)	NULL,
	company_name 		varchar2(254)	NULL,
	address1 		varchar2(254)	NULL,
	address2 		varchar2(254)	NULL,
	address3 		varchar2(254)	NULL,
	city 			varchar2(254)	NULL,
	state 			varchar2(254)	NULL,
	postal_code 		varchar2(254)	NULL,
	county 			varchar2(254)	NULL,
	country 		varchar2(254)	NULL,
	phone_number 		varchar2(254)	NULL,
	fax_number 		varchar2(254)	NULL,
	last_Activity 		DATE	NULL,
	status_cd 		INTEGER	NULL,
	billing_address_is_same number(1)	NULL,
	CHECK (billing_address_is_same IN (0, 1)),
	PRIMARY KEY(id)
);

Alter  TABLE tru_order add (
	billing_addr_id 	varchar2(254)	NULL REFERENCES tru_bill_addr(id)
);
COMMIT;
