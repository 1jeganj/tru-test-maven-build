CREATE TABLE tru_collection_display_status (
	product_id 		varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	display_status 		varchar2(40)	NULL,
	PRIMARY KEY(product_id, asset_version)
);