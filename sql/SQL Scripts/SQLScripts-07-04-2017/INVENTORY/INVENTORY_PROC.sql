create or replace procedure TRU_INV_RPT_PRC
/*
***************************************************************TRU_INV_RPT_PRC**********************************************************************************************************
**    i.  Purpose
**
**            This procedure use to generate csv file for Inventory report
**

**    ii. Creation History
**
**           Date         Created by                   Comments
**          ---------     -----------                ---------------
**          22/Dec/2016   Malleshwara.C              Initial version
**          02/Mar/2017   Yuvraj                     Jira Defect TSJ-8251


*******************************************************************************************************************************************************************************************
*/

(
 P_FILE_NAME varchar2,
 P_DIR_NAME  varchar2,
 P_DATE		 varchar2,
 p_SEQ		 varchar2
)
is

	CSV_FILE UTL_FILE.FILE_TYPE;
	CURSOR INV_CUR IS
			select 	Sku.Sku_Id
					,Sku.Online_Pid
					,Sku.Web_Disp_Flag Sthe_Eligibility
					,Sku.Ship_To_Store_Eligible
					,Nvl(decode(lower(Sku.Item_In_Store_Pick_Up),'yes',1,'no',0),0) Item_In_Store_Pick_Up
					,Nvl(Sth_Availibility,0) Sth_Availibility
					,Nvl(Sts_Availibility,0) Sts_Availibility
					,Nvl(Sts_Availibility,0) Ispu_Availibility
					,Sku.Web_Disp_Flag
			from
				Tru_Sku_Cata Sku,
				(select nvl(online_Sku.Item_Name,Store_Sku.Item_Name) Item_Name,nvl(Sth_Availibility,0) Sth_Availibility,nvl(Sts_Availibility,0) Sts_Availibility
				 From
					(Select Item_Name,Case When  Atc_Quantity > 0 Then 1 Else 0 End Sth_Availibility   From I_AVAILABILITY Where Facility_Name ='643' ) Online_Sku
					full outer join
					(Select Item_Name,Case When  Atc_Quantity > 0 Then 1 Else 0 End Sts_Availibility
					 From
						(Select Item_Name, Sum(Atc_Quantity) Atc_Quantity   From I_AVAILABILITY Where Facility_Name <>'643' Group By Item_Name)
					)Store_Sku
				on online_Sku.Item_Name = Store_Sku.Item_Name
				) Inv_Sku
			where Sku.Sku_Id = Inv_Sku.Item_Name(+);

	L_INV INV_CUR%ROWTYPE;
	L_FILE_NAME 	VARCHAR2(255);
	L_HEADER 		VARCHAR2(4000);
	L_TRIALER 		VARCHAR2(4000);
	L_COUNT 		NUMBER;


begin

	if P_FILE_NAME is null or P_DIR_NAME is null or P_DATE is null
	then
	 RAISE_APPLICATION_ERROR(-20101, 'please pass file_name , dir_name and date');
	end if;


	select P_FILE_NAME||'.csv' into L_FILE_NAME from DUAL;
	--dbms_output.put_line(L_FILE_NAME);

		csv_file                    := UTL_FILE.FOPEN(P_DIR_NAME,L_file_NAME,'w',32767);

		select 'HEADER|'||P_DATE||'|'||p_SEQ into L_HEADER from DUAL;
		UTL_FILE.PUT(CSV_FILE,L_HEADER);
		UTL_FILE.NEW_LINE(CSV_FILE);

		OPEN   INV_CUR ;

		LOOP
		  FETCH INV_CUR INTO L_INV;
		  EXIT when INV_CUR%NOTFOUND;
		  UTL_FILE.PUT(CSV_FILE,L_INV.Sku_Id);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Online_Pid);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Sthe_Eligibility);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Item_In_Store_Pick_Up);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Ship_To_Store_Eligible);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Sth_Availibility);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Sts_Availibility);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Ispu_Availibility);
		  UTL_FILE.PUT(CSV_FILE,'|'||L_INV.Web_Disp_Flag);
		  UTL_FILE.PUT(CSV_FILE,'|'||P_DATE);
		  UTL_FILE.NEW_LINE(CSV_FILE);
		END LOOP;

		L_COUNT := INV_CUR%ROWCOUNT;

		select 'TRAILER|'||'|'||L_COUNT into L_TRIALER from DUAL;

		UTL_FILE.PUT(CSV_FILE,L_TRIALER);
		UTL_FILE.FCLOSE(CSV_FILE);
	EXCEPTiON when OTHERS then
	RAISE;
End;
/