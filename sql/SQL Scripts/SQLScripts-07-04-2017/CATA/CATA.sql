alter table tru_promotion add web_store_display number(1);
update tru_promotion set web_store_display =1;
COMMIT ;

create or replace
procedure TRU_QUALIF_SKU_PROMOS_PRC

/*
***************************************************************TRU_QUALIF_SKU_PROMOS_PRC**********************************************************************************************************
**    i.  Purpose
**
**		  This procedure populates TRU_SKU_QUALIFIED_PROMOS table

**    ii. Procedures
**
**			i.TRU_QUALIF_SKU_PROMOS_PRC
**			---------------------------------------
**				It populates TRU_SKU_QUALIFIED_PROMOS  by using the tables (tru_qualifying_category,tru_qualifying_prod,tru_qualifying_sku) and
				exclude the skus which are  present in excluded table (tru_excluded_category,tru_excluded_prod,tru_excluded_sku).

**			Sample example how sku_id  mapping using category_id

**			Assume One  category contains 2 level of child category and the data like below
**			Parent category  :

**											 par_cat         prom1   prd1

**			child category    :

**							   level1 : par_cat is parent of child_cata1 and child_cata2

											  child_cata1     prom2  prd2
											  child_cata2     prom3  prd3

**							   level2: child_cata1 is parent of child_cata3

											  child_cata3     prom4  prd4

			product-sku relationship
			prd1	0	sku1
			prd2	0	sku2
			prd2	1	sku3
			prd3	0	sku4
			prd4	0	sku5

			TRU_EXCLUDED_PROD :
			prom4	0	prd4

			TRU_QUALIFYING_SKU :
			prom5	0	sku1


**			Final date in TRU_SKU_QUALIFIED_PROMO table like below

			PROMOTION_DATA is the concat of promotion_id|display_name|startdate|enddate|priority columns in DCS_PROMOTION table

			sku1  0  prom1
			sku1  1  prom5
			sku2  0  prom1
			sku2  1	 prom2
			sku3  0  prom1
			sku3  1	 prom2
			sku4  0  prom1
			sku4  1  prom3
			sku5  0	 prom2



**    iii. Creation History
**
**           Date         Created by           		Comments
**          ---------     -----------        		---------------
**	   		13/APR/2016   Malleshwara.C				Initial Version
*******************************************************************************************************************************************************************************************

*/

AS
	L_CATEGORY_ID DCS_CATEGORY.CATEGORY_ID%TYPE;
    L_PROMOTION_ID TRU_QUALIFYING_CATEGORY.PROMOTION_ID%TYPE;

    ---Variable to hold cursor operation
	CURSOR C1 IS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_QUALIFYING_CATEGORY
				 MINUS
				 SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_EXCLUDED_CATEGORY;
	L_COUNT number;

    ---Variable to hold dynamic sql statements
	L_STATEMENT_STR VARCHAR2(10000);
	L_STATEMENT_STR1 VARCHAR2(10000);
	L_STATEMENT_STR2 VARCHAR2(10000);
	L_STATEMENT_STR3 VARCHAR2(10000);
	L_STR varchar2(100);
	L_G_STR varchar2(1);
	L_STATEMENT_1 VARCHAR2(10000);
	L_COUNT1 number;

BEGIN
			execute immediate 'truncate table ORD_ERROR_LOG';
			execute immediate 'truncate table TRU_SKU_QUALIFIED_PROMOS';
			execute immediate 'truncate table TRU_SKU_ELGIBLE_PROMOS';
			--Cursor ci fetch the TRU_QUALIFYING_CATEGORY records
			OPEN  C1;
			LOOP
				  <<LOOP1>>
				  FETCH C1 into L_CATEGORY_ID,L_PROMOTION_ID;
				  EXIT when C1%NOTFOUND;

				  select COUNT(1) into L_COUNT1 from GT_PAR_CAT_PROM
				  where GT_PAR_CAT_PROM.CATEGORY_ID = L_CATEGORY_ID  AND GT_PAR_CAT_PROM.PROMOTION_ID =L_PROMOTION_ID;

				  --This  if condition use to skip the ctaegory_id which is already inserted in target table
				  DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||'::'||L_COUNT1);
				  IF L_COUNT1 > 0
				  then
						DBMS_OUTPUT.PUT_LINE(L_CATEGORY_ID ||'::'||L_PROMOTION_ID||': already inserted');
						GOTO LOOP1;
				  END IF;




				 --Finding the  child category level from qualifid category list
				 --SELECT COUNT(DISTINCT LVL) INTO L_COUNT
					--  FROM
					--   (SELECT
					--		 LEVEL LVL
					 --  FROM   DCS_CAT_CHLDCAT
					--		 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
					--		 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
					--		 ORDER SIBLINGS BY CHILD_CAT_ID
					 --  );


				--Populating product list for parent category list
							INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
							SELECT distinct CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
							from
							(SELECT DISTINCT CATEGORY_ID,L_PROMOTION_ID PROMOTION_ID
							from
							(SELECT  CATEGORY_ID,level lvl
											   FROM   DCS_CAT_CHLDCAT
													 START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
													 CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
													 ORDER SIBLINGS BY CHILD_CAT_ID
							)
							MINUS
							select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
							MINUS select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM
							) CAT_PROM
							inner join DCS_CAT_CHLDPRD on(CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID)
							 union
							select CHILD_PRD_ID PRODUCT_ID,L_PROMOTION_ID from  DCS_CAT_CHLDPRD where CATEGORY_ID = L_CATEGORY_ID;
							--DBMS_OUTPUT.PUT_LINE ('GT_PROD_PROM 1 :'||sql%ROWCOUNT);

						/*--mapping product_id with promotion_id using child category list  start
							  FOR J IN 1..L_COUNT
							  LOOP
									FOR K IN (
												  SELECT DISTINCT CATEGORY_ID,L_PROMOTION_ID PAR_PROMOTION_ID
												  from
												  (SELECT  DCS_CAT_CHLDCAT.CATEGORY_ID,level lvl
																	 FROM   DCS_CAT_CHLDCAT
																		   START WITH DCS_CAT_CHLDCAT.CATEGORY_ID = L_CATEGORY_ID
																		   CONNECT BY DCS_CAT_CHLDCAT.CATEGORY_ID = PRIOR CHILD_CAT_ID
																		   ORDER SIBLINGS BY CHILD_CAT_ID
												  ) WHERE LVL = J
												  minus select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
							  minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM
												)

					  LOOP
						*/
							   --DBMS_OUTPUT.PUT_LINE ('k loop '||J||':'||L_CATEGORY_ID);
								  L_STATEMENT_STR := 'SELECT CHILD_CAT_ID FROM (SELECT  CHILD_CAT_ID,'||''''||L_PROMOTION_ID||''''||' FROM DCS_CAT_CHLDCAT WHERE CATEGORY_ID  = '||''''||L_CATEGORY_ID||''''||
                                      'minus  select CATEGORY_ID,PROMOTION_ID from TRU_EXCLUDED_CATEGORY
                                       minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM)';
								  L_STATEMENT_STR1 := 'select child_cat_id,'||''''||L_PROMOTION_ID||''''||' from dcs_cat_chldcat where category_id in(';
								  L_STATEMENT_STR2 := 'select child_cat_id from dcs_cat_chldcat where category_id in(';
								  L_STR := ')';
								  l_g_str :=')';
								  L_STATEMENT_STR3 := null;

								  FOR L IN 1..50
								  LOOP

									  L_STATEMENT_1 :=  L_STATEMENT_STR1||L_STATEMENT_STR3||L_STATEMENT_STR||L_STR;

									  execute immediate 'insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1|| ' MINUS SELECT CATEGORY_ID,PROMOTION_ID FROM TRU_EXCLUDED_CATEGORY minus select CATEGORY_ID,PROMOTION_ID from GT_PAR_CAT_PROM';
									  --DBMS_OUTPUT.PUT_LINE('insert into GT_CAT_PROM(CATEGORY_ID,PROMOTION_ID)'||L_STATEMENT_1);
									  DBMS_OUTPUT.PUT_LINE ('GT_PROD_PROM '||L_CATEGORY_ID||' :'||SQL%ROWCOUNT);
									  IF SQL%ROWCOUNT = 0
									  THEN
										EXIT;
									  END IF;
									  L_STR := l_g_str||L_STR;
									  L_STATEMENT_STR3 := L_STATEMENT_STR2||L_STATEMENT_STR3;

								  end LOOP;
							--end LOOP;
					  --end LOOP;

					   --Populating category_id and promotion_id in global temporary table (GT_PAR_CAT_PROM) to validate category_id already inserted or not
						INSERT INTO GT_PAR_CAT_PROM(CATEGORY_ID,PROMOTION_ID)
						SELECT distinct par_cat_id,L_PROMOTION_ID
						FROM
						  (SELECT CHILD_CAT_ID,
						   DCS_CAT_CHLDCAT.CATEGORY_ID par_cat_id
						   from   DCS_CAT_CHLDCAT
							start with DCS_CAT_CHLDCAT.CATEGORY_ID =L_CATEGORY_ID
							connect by DCS_CAT_CHLDCAT.CATEGORY_ID = prior CHILD_CAT_ID
							order siblings by CHILD_CAT_ID
						  );
			END LOOP;

			--mapping product_id with promotion_id using child category list  end

			CLOSE C1;

			INSERT INTO GT_PROD_PROM (PRODUCT_ID,PROMOTION_ID)
			SELECT CHILD_PRD_ID PRODUCT_ID,PROMOTION_ID
			FROM GT_CAT_PROM INNER JOIN DCS_CAT_CHLDPRD ON(GT_CAT_PROM.CATEGORY_ID = DCS_CAT_CHLDPRD.CATEGORY_ID);

			INSERT INTO TRU_SKU_QUALIFIED_PROMOS(SKU_ID,SEQUENCE_NUM,PROMOTION_DATA)
			select SKU_PROM.SKU_ID,
				   ROW_NUMBER() over(partition by SKU_PROM.SKU_ID order by DCS_PROMOTION.PROMOTION_ID) -1 SEQUENCE_NUM,
				   DCS_PROMOTION.PROMOTION_ID||'|'||DISPLAY_NAME||'|'||NVL(TO_CHAR(BEGIN_USABLE,'YYYY-MM-DD HH24:MI:SS'),TO_CHAR(TO_TIMESTAMP('01-JAN-2016 12:00:00','DD-MM-YY HH24:MI:SS'),'YYYY-MM-DD HH24:MI:SS'))||'|'||NVL(TO_CHAR(END_USABLE,'YYYY-MM-DD HH24:MI:SS'),TO_CHAR(TO_TIMESTAMP('01-JAN-9999 12:00:00 AM','DD-MON-YY HH.MI.SS AM'),'YYYY-MM-DD HH24:MI:SS'))||'|'||NVL(tru_promotion.DISPLAY_PRIORITY,DCS_PROMOTION.PRIORITY)||'|'||tru_promotion.web_store_display PROMOTION_DATA
			from(DCS_PROMOTION inner join tru_promotion tru_promotion on (DCS_PROMOTION.PROMOTION_ID = tru_promotion.PROMOTION_ID)
				inner join
					 (
					 (select DCS_PRD_CHLDSKU.SKU_ID,PROMOTION_ID
					  FROM
					  DCS_PRD_CHLDSKU inner join
					  (
						(SELECT PRODUCT_ID,PROMOTION_ID FROM GT_PROD_PROM
						  UNION
						  select PRODUCT_ID,PROMOTION_ID from TRU_QUALIFYING_PROD
						)
						minus
						select PRODUCT_ID,PROMOTION_ID from TRU_EXCLUDED_PROD
					  )PROM_PRD on(DCS_PRD_CHLDSKU.PRODUCT_ID = PROM_PRD.PRODUCT_ID)
					  union
					  SELECT TRIM(SKU_LIST.SKU_ID) SKU_ID,PROMOTION_ID
						 FROM (select promotion_id,DISPLAY_PRIORITY, xmltype('<table><row>' || replace(BULK_QF_SKUS, ',', '</row><row>') || '</row></table>') xmldata
								FROM TRU_PROMOTION where BULK_QF_SKUS is not null
							   ) quli_sku
							  , xmltable
								(
								   '/table/row' passing quli_sku.xmldata
								   COLUMNS
									 SKU_ID VARCHAR2(255) PATH '.'
								) sku_list
						 where sku_list.sku_id is not null
					  union
					  select SKU_ID,PROMOTION_ID from TRU_QUALIFYING_SKU
					 )
					 minus
					(select SKU_ID,PROMOTION_ID from TRU_EXCLUDED_SKU
					 UNION ALL
					 SELECT TRIM(SKU_LIST.SKU_ID) SKU_ID,PROMOTION_ID
						 FROM (select promotion_id,DISPLAY_PRIORITY, xmltype('<table><row>' || replace(BULK_EXCLUDE_SKUS, ',', '</row><row>') || '</row></table>') xmldata
								FROM TRU_PROMOTION where BULK_EXCLUDE_SKUS is not null
							   ) quli_sku
							  , xmltable
								(
								   '/table/row' passing quli_sku.xmldata
								   COLUMNS
									 SKU_ID VARCHAR2(255) PATH '.'
								) sku_list
						 where sku_list.sku_id is not null
					 )
				  )SKU_PROM
					on (DCS_PROMOTION.PROMOTION_ID = SKU_PROM.PROMOTION_ID)
				  ) where DCS_PROMOTION.enabled =1 and (DCS_PROMOTION.END_USABLE > systimestamp or DCS_PROMOTION.END_USABLE is null)
			LOG ERRORS INTO ORD_ERROR_LOG ('TRU_SKU_QUALIFIED_PROMOS') REJECT LIMIT UNLIMITED;
        DBMS_OUTPUT.PUT_LINE ('TRU_SKU_QUALIFIED_PROMOS :'||sql%ROWCOUNT);
	  COMMIT;

 	INSERT INTO DCS_INVALIDATED_SKU_IDS (SKU_ID) SELECT  DISTINCT SKU_ID FROM TRU_SKU_QUALIFIED_PROMOS a where not exists (select 1 from DCS_INVALIDATED_SKU_IDS b where a.sku_id = b.sku_id) ;
  commit;
  --TRU_SKU_ELIGIBLE_PROMOTIONS  population started
   	INSERT INTO TRU_SKU_ELGIBLE_PROMOS(SKU_ID,SEQUENCE_NUM,ELGIBLE_PROMOTION_DATA)

	SELECT SKU_ID,SEQUENCE_NUM, SUBSTR(PROMOTION_DATA,1,INSTR(PROMOTION_DATA,'|',1)-1) PROMOTION_ID FROM TRU_SKU_QUALIFIED_PROMOS

	LOG ERRORS INTO ORD_ERROR_LOG ('TRU_SKU_ELGIBLE_PROMOS') REJECT LIMIT UNLIMITED;

	commit;

  --TRU_SKU_GWP_PROMOS  population started
  execute immediate 'truncate table TRU_SKU_GWP_PROMOS';

  INSERT INTO TRU_SKU_GWP_PROMOS (SKU_ID,SEQUENCE_NUM,PROMOTION_ID)
  SELECT SKU_ID
		 ,ROW_NUMBER() OVER(PARTITION BY SKU_ID ORDER BY PROMOTION_ID) -1 SEQUENCE_NUM
		 ,PROMOTION_ID
  FROM
	  TRU_GWP_SKU
	 LOG ERRORS INTO ORD_ERROR_LOG ('TRU_SKU_GWP_PROMOS') REJECT LIMIT UNLIMITED;
  commit;

EXCEPTION  WHEN OTHERS THEN
RAISE;

END TRU_QUALIF_SKU_PROMOS_PRC;
/
 