import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import com.tru.orcl.coh.entity.ItemQuantity;
import com.tru.orcl.coh.entity.ItemQuantityKey;

/**
 * The Class WriteCoherenceData.
 */
public class WriteCoherenceData {
	/** The Constant ONE. */
	private static final int ONE = 1;

	/** The Constant OUTPUT_MESSAGE. */
	private static final String OUTPUT_MESSAGE = "outputMessage";

	/** The Constant ACT_STATUS. */
	private static final String ACT_STATUS = "actStatus";

	/** The Constant ACT_QUANTITY. */
	private static final String ACT_QUANTITY = "actQuantity";

	/** The Constant FACILITY_NAME. */
	private static final String FACILITY_NAME = "facilityName";

	/** The Constant ITEM_NAME. */
	private static final String ITEM_NAME = "itemName";

	/** The Constant CREATED. */
	private static final String CREATED = "created";

	/** The Constant LAST_UPDATED. */
	private static final String LAST_UPDATED = "lastUpdated";

	/** The Constant DATE_FORMAT. */
	private static final String DATE_FORMAT = "dateFormat";

	/** The Constant ITEM_QUANTITY. */
	private static final String ITEM_QUANTITY = "itemQuantity";

	/** The Constant TABLE_NAME. */
	private static final String TABLE_NAME = "tableName";

	/** The Constant PASSWORD. */
	private static final String PASSWORD = "password";

	/** The Constant USER_NAME. */
	private static final String USER_NAME = "userName";

	/** The Constant DB_CONNECTION. */
	private static final String DB_CONNECTION = "dbConnection";

	/** The Constant CONNECTION_DRIVER. */
	private static final String CONNECTION_DRIVER = "connectionDriver";

	/** The Constant BATCH_SIZE. */
	private static final String BATCH_SIZE = "batchSize";

	/** The Constant PROPERTIES_FILE. */
	private static final String PROPERTIES_FILE = "TRUCoherenceDataConfig.properties";

	/** The prop. */
	private Properties mProperties = null;

	/** The Constant RECORD_UPDATED. */
	private static final String RECORD_UPDATED = "recordUpdated";

	/** The LOGGER */
	private static final Logger LOGGER = Logger.getLogger(WriteCoherenceData.class.getName());

	/**
	 * Instantiates a new write coherence data.
	 * 
	 * @throws IOException
	 *             - IO Exception
	 */
	public WriteCoherenceData() throws IOException {

		this.mProperties = new Properties();
		File configFile = new File(PROPERTIES_FILE);

		FileInputStream is = new FileInputStream(configFile);
		mProperties.load(is);

	}

	/**
	 * Gets the property value.
	 * 
	 * @param pKey
	 *            the key
	 * @return the property value
	 */
	public String getPropValue(String pKey) {
		return this.mProperties.getProperty(pKey);
	}

	/**
	 * The main method.
	 *
	 * @param pArgs the arguments
	 * @throws IOException - IOE Exception
	 * @throws SQLException - SQL Exception
	 * @throws ClassNotFoundException - ClassNotFound Exception
	 * @throws ParseException - Parse Exception
	 */
	public static void main(String pArgs[]) throws IOException, SQLException, ClassNotFoundException, ParseException {

		WriteCoherenceData rcd = new WriteCoherenceData();
		String batchString = rcd.getPropValue(BATCH_SIZE);
		int batchSize = Integer.parseInt(batchString);
		int temp = 0;
		// step1 load the driver class
		Class.forName(rcd.getPropValue(CONNECTION_DRIVER));
		// step2 create the connection object
		Connection con = DriverManager.getConnection(rcd.getPropValue(DB_CONNECTION),
				rcd.getPropValue(USER_NAME), rcd.getPropValue(PASSWORD));
		// step3 create the statement object
		Statement stmt = con.createStatement();
		// step4 execute query
		ResultSet records = stmt.executeQuery(rcd.getPropValue(TABLE_NAME));
		ResultSetMetaData md = records.getMetaData();
		int columns = md.getColumnCount();
		NamedCache cache = CacheFactory.getCache(rcd.getPropValue(ITEM_QUANTITY));

		Map<String, String> invMap = new HashMap(columns);
		Map<ItemQuantityKey, ItemQuantity> invMap1 = new HashMap();
		ItemQuantityKey itemQuantityKey = null;
		ItemQuantity itemQuantity = null;
		Date lastUpdateDate = null;
		Date createdDate = null;
		Date recordUpdateDate = null;
		while (records.next()) {

			for (int i = 1; i <= columns; i++) {

				if (records.getObject(i) != null) {
					invMap.put(md.getColumnName(i), records.getObject(i).toString());
				} else {
					invMap.put(md.getColumnName(i), null);
				}

				if (i == columns) {
					temp = temp + ONE;

					DateFormat dateFormat = new SimpleDateFormat(rcd.getPropValue(DATE_FORMAT));

					if (invMap.get(rcd.getPropValue(LAST_UPDATED)) != null) {
						lastUpdateDate = dateFormat.parse(invMap.get(rcd.getPropValue(LAST_UPDATED)));
					} else {
						lastUpdateDate = null;
					}
					if (invMap.get(rcd.getPropValue(CREATED)) != null) {
						createdDate = dateFormat.parse(invMap.get(rcd.getPropValue(CREATED)));
					} else {
						createdDate = null;
					}

					if (invMap.get(rcd.getPropValue(RECORD_UPDATED)) != null) {
						recordUpdateDate = dateFormat.parse(invMap.get(rcd.getPropValue(RECORD_UPDATED)));
					} else {
						recordUpdateDate = null;
					}

					itemQuantityKey = new ItemQuantityKey(invMap.get(rcd.getPropValue(ITEM_NAME)), invMap.get(rcd
							.getPropValue(FACILITY_NAME)));
					itemQuantity = new ItemQuantity(Long.parseLong(invMap.get(rcd.getPropValue(ACT_QUANTITY))),
							Integer.parseInt(invMap.get(rcd.getPropValue(ACT_STATUS))), createdDate,
							lastUpdateDate, recordUpdateDate);
					invMap1.put(itemQuantityKey, itemQuantity);

					if (temp == batchSize) {
						cache.putAll(invMap1);
						temp = 0;
						invMap1.clear();
					}
				}
			}
		}

		if (temp != 0) {
			cache.putAll(invMap1);
		}
		LOGGER.info(rcd.getPropValue(OUTPUT_MESSAGE));
		con.close();

	}
}