# Author:  Aleksandar Kocic
# Date:    2016-01-04
# Purpose: This WLST script configures Coherence domain.
#          Pre-requisites are a running WebLogic Server domain with Admin server.

property_file = '/export/home/wladmin/scripts/qa-tru.properties'

 
if len(sys.argv) == 2:
	properties=sys.argv[1]
else:
	properties=property_file
	
print 'Loading properties file: ', properties  

try:			
	file = open(properties, 'r')
	print 'Read properties file.'
	exec file
	print 'Executed properties file.'
	file.close
except:
    exit()

	
connect('weblogic', 'welcome1', admin_url)

edit()
startEdit()
domain = getMBean('/')

#Admin server settings
adminServer = domain.lookupServer(admin_server)
#Properties
machine = domain.lookupMachine(admin_machine)
if machine == None:
	machine = domain.createMachine(admin_machine)
	nodeManager = machine.getNodeManager()
	nodeManager.setListenAddress(nodemanager_lsAddress)
	nodeManager.setListenPort(nodemanager_lsPort)
adminServer.setMachine(machine)
adminServer.setListenAddress(admin_lsAddress)
#Logging
asLog = adminServer.getLog()
asLog.setFileName(log_file + os.sep + admin_server + '.log')
asLog.setLogFileSeverity(log_file_severity)


#Lookup or create Coherence cluster
cohCluster = domain.lookupCoherenceClusterSystemResource(coh_cluster)
if cohCluster == None:
	print 'Creating Coherence cluster: ', coh_cluster
	cohCluster = create(coh_cluster, 'CoherenceClusterSystemResource')
	cohMBean = cohCluster.getCoherenceClusterResource()
	
	#Parameters
	cohClusterParams = cohMBean.getCoherenceClusterParams()
	#cohClusterParams.setSecurityFrameworkEnabled(true)
	cohClusterParams.setClusterListenPort(coh_cluster_clusterListenPort)
	#cohClusterParams.setTransport(coh_cluster_transport)

	#WKAs
	cohWKAs = cohClusterParams.getCoherenceClusterWellKnownAddresses()	
	i = 0
	for wka_listenAddress in wka:
		print 'Creating WKA: ', wka_listenAddress
		wka = cohWKAs.createCoherenceClusterWellKnownAddress(wka_name + str(i))
		wka.setListenAddress(wka_listenAddress)
		#wka.setListenPort(wka_listenPort)
		i = i + 1
		
	#Address Providers
	cohAddressProviders = cohMBean.getCoherenceAddressProviders()	
	for cohSocketName, cohSocketPort in coh_socket_list.items():
		cohSocketName = app_name + cohSocketName
		print 'Creating Address Provider: ', cohSocketName, ' = ', coh_socket_address, ':', cohSocketPort
		cohAddressProvider = cohAddressProviders.createCoherenceAddressProvider(cohSocketName)
		cohSocket = cohAddressProvider.createCoherenceSocketAddress(cohSocketName)
		cohSocket.setAddress(coh_socket_address)
		cohSocket.setPort(cohSocketPort)
	
	#print 'Creating Address Provider: ', coh_socket_remote_replicate_name, ' = ', coh_socket_remote_replicate_address, ':', coh_socket_remote_replicate_port
	#cohAddressProvider = cohAddressProviders.createCoherenceAddressProvider(coh_socket_remote_replicate_name)
	#cohSocket = cohAddressProvider.createCoherenceSocketAddress(coh_socket_remote_replicate_name)
	#cohSocket.setAddress(coh_socket_remote_replicate_address)
	#cohSocket.setPort(coh_socket_remote_replicate_port)
	
			
	#Cache Configuration
	cacheConfiguration =  cohCluster.createCoherenceCacheConfig(cache_config_name)
	cacheConfiguration.setJNDIName(cache_config_jndi_name)
	cacheConfiguration.setCacheConfigurationFile(cache_config_file)
	cacheConfiguration.importCacheConfigurationFile()
	
	#Identity
	#cohKeystoreParams = cohClusterParams.getCoherenceKeystoreParams()
	#cohKeystoreParams.setCoherenceIdentityAlias(ms_identity_alias)
	#cohKeystoreParams.setCoherencePrivateKeyPassPhrase(ms_keypass_phrase)
	
	#Identity Asserter
	##cohIdentityAsserter = cohClusterParams.getCoherenceIdentityAsserter()
	#cohIdentityAsserter.setClassName(coh_identity_asserter_class)
	
		
#Lookup or create WLS storage clusters
cluster_num = 0		
for wls_cluster in wls_cluster_list:
	wlsClusterName = app_name + wls_cluster
	wlsCluster = domain.lookupCluster(wlsClusterName)
	if wlsCluster == None:
		print 'Creating cluster: ', wlsClusterName
		wlsCluster = create(wlsClusterName,'Cluster')
		wlsCluster.setCoherenceClusterSystemResource(cohCluster)
		#Add as a Coherence target
		cohCluster.addTarget(wlsCluster)
		if not wls_cluster_storage_list[cluster_num]:
			cohTier = wlsCluster.getCoherenceTier()
			cohTier.setLocalStorageEnabled(false)
			cacheConfiguration.addTarget(wlsCluster)

	wls_cluster_size = wls_cluster_size_list[cluster_num]
	ms_num = 0
	#Create WLS servers 
	try:
		machine_num = 0
		for machineName in machineNames:
			msPort = ms_lsPort
			machine = domain.lookupMachine(machineName)
			if machine == None:
				machine = domain.createMachine(machineName)
				nodeManager = machine.getNodeManager()
				nodeManager.setListenAddress(ms_lsAddress[machine_num])
				
			nodes_per_machine = wls_cluster_size / len(machineNames)			
			for i in range(nodes_per_machine):
				coh_server = app_name + coh_ms_list[cluster_num] + str(ms_num + 1)
				managedServer = domain.lookupServer(coh_server)
				if managedServer == None:
					print 'Server doesn\'t exist. Creating server:', coh_server
					managedServer = create(coh_server, 'Server')
					print 'Set machine: ', machine.getName()
					managedServer.setMachine(machine)
					print 'Set cluster: ', wlsCluster.getName()
					managedServer.setCluster(wlsCluster)
					print 'Set address: ', ms_lsAddress[machine_num]
					managedServer.setListenAddress(ms_lsAddress[machine_num])
					print 'Set port: ', msPort
					managedServer.setListenPort(msPort)
					#Coherence
					cohMemberConfig = managedServer.getCoherenceMemberConfig()
					cohMemberConfig.setUnicastListenAddress(ms_lsAddress[machine_num])
					cohMemberConfig.setUnicastListenPort(coh_cluster_unicastListenPort)
					cohMemberConfig.setUnicastPortAutoAdjust(true)
					cohMemberConfig.setSiteName(coh_site_name)
					#KeyStores
					#managedServer.setKeyStores(key_store)
					#managedServer.setCustomIdentityKeyStoreFileName(key_store_custom_identity_file)
					#managedServer.setCustomIdentityKeyStoreType(key_store_custom_identity_type)
					#managedServer.setCustomIdentityKeyStorePassPhrase(key_store_custom_identity_password)
					#managedServer.setCustomTrustKeyStoreFileName(key_store_custom_trust_file)
					#managedServer.setCustomTrustKeyStoreType(key_store_custom_trust_type)
					#managedServer.setCustomTrustKeyStorePassPhrase(key_store_custom_trust_password)
					#SSL
					#msSSL = managedServer.getSSL()
					#msSSL.setServerPrivateKeyAlias(ms_identity_alias)
					#msSSL.setServerPrivateKeyPassPhrase(ms_keypass_phrase)
					#Server Start
					msServerStart = managedServer.getServerStart()
					msServerStart.setArguments(coh_ms_arguments_list[cluster_num])
					#Logging
					msLog = managedServer.getLog()
					msLog.setFileName(log_file + os.sep + coh_server + '.log')
					msLog.setLogFileSeverity(log_file_severity)
					
				#Update port
				msPort = msPort + 2
				ms_num = ms_num + 1			
			#Update machine
			machine_num = machine_num + 1
		#Update cluster
		cluster_num = cluster_num + 1
		#Update port to continue per machine
		ms_lsPort = msPort
	except TypeError, err:
		print 'Create Error: ', err 
		undo('true', 'y')
		
activate()
disconnect()
print 'Finished.'
