package com.tru.orcl.coh.entity;

import com.tangosol.io.pof.annotation.Portable;
import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * The Class ItemQuantityKey.
 */
@Portable
public class ItemQuantityKey {
	
	private static final int INT_ZERO = 0;

	private static final int INT_ONE = 1;

	private static final int INT_THIRTY_ONE = 31;

	/** The item name. */
	@PortableProperty(0)
	private String mItemName;	
	
	/** The facility name. */
	@PortableProperty(1)
	private String mFacilityName;
	
	/**
	 * Instantiates a new item quantity key.
	 */
	public ItemQuantityKey () {
	
	}
	
	/**
	 * Instantiates a new item quantity key.
	 *
	 * @param pItemName the item name
	 * @param pFacilityName the facility name
	 */
	public ItemQuantityKey(String pItemName, String pFacilityName) {
		super();
		this.mItemName = pItemName;
		this.mFacilityName = pFacilityName;
	}

	/**
	 * Gets the item name.
	 *
	 * @return the item name
	 */
	public String getItemName() {
		return mItemName;
	}

	/**
	 * Sets the item name.
	 *
	 * @param pItemName the new item name
	 */
	public void setItemName(String pItemName) {
		this.mItemName = pItemName;
	}

	/**
	 * Gets the facility name.
	 *
	 * @return the facility name
	 */
	public String getFacilityName() {
		return mFacilityName;
	}

	/**
	 * Sets the facility name.
	 *
	 * @param pFacilityName the new facility name
	 */
	public void setFacilityName(String pFacilityName) {
		this.mFacilityName = pFacilityName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = INT_THIRTY_ONE;
		int result = INT_ONE;
		result = prime * result + ((mFacilityName == null) ? INT_ZERO : mFacilityName.hashCode());
		result = prime * result + ((mItemName == null) ? INT_ZERO : mItemName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObject) {
		if (this == pObject){
			return true;
		}
		if (pObject == null){
			return false;
		}
		if (getClass() != pObject.getClass()){
			return false;
		}
		ItemQuantityKey other = (ItemQuantityKey) pObject;
		if (mFacilityName == null && other.mFacilityName != null) {
				return false;
		} else if (!mFacilityName.equals(other.mFacilityName)){
			return false;
		}
		if (mItemName == null && other.mItemName != null) {
				return false;
		} else if (!mItemName.equals(other.mItemName)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemQuantityKey [itemName=" + mItemName + ", facilityName=" + mFacilityName + "]";
	}
	
}
