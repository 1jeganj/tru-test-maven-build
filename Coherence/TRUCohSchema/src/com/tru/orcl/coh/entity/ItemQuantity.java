package com.tru.orcl.coh.entity;

import java.util.Date;

import com.tangosol.io.pof.annotation.Portable;
import com.tangosol.io.pof.annotation.PortableProperty;

/**
 * The Class ItemQuantity.
 */
@Portable
public class ItemQuantity {

	/** The atc quantity. */
	@PortableProperty(0)
	private long mAtcQuantity;

	/** The atc status. */
	@PortableProperty(1)
	private int mAtcStatus;

	/** The last updated. */
	@PortableProperty(2)
	private Date mLastUpdated;

	/** The created. */
	@PortableProperty(3)
	private Date mCreated;

	/** The mRecordUpdated. */
	@PortableProperty(4)
	private Date mRecordUpdated;
	
	/**
	 * Instantiates a new item quantity.
	 */
	public ItemQuantity() {
	}

	/**
	 * Instantiates a new item quantity.
	 * 
	 * @param pAtcQuantity
	 *            the atc quantity
	 * @param pAtcStatus
	 *            the atc status
	 * @param pLastUpdated
	 *            the last updated
	 * @param pCreated
	 *            the created
	 * @param pRecordUpdated
	 *            the RecordUpdated
	 */
	public ItemQuantity(long pAtcQuantity, int pAtcStatus, Date pCreated, Date pLastUpdated, Date pRecordUpdated) {
		super();
		this.mAtcQuantity = pAtcQuantity;
		this.mAtcStatus = pAtcStatus;
		this.mCreated = pCreated;
		this.mLastUpdated = pLastUpdated;
		this.mRecordUpdated = pRecordUpdated;
	}

	/**
	 * Gets the atc quantity.
	 * 
	 * @return the atc quantity
	 */
	public long getAtcQuantity() {
		return mAtcQuantity;
	}

	/**
	 * Sets the atc quantity.
	 * 
	 * @param pAtcQuantity
	 *            the new atc quantity
	 */
	public void setAtcQuantity(long pAtcQuantity) {
		this.mAtcQuantity = pAtcQuantity;
	}

	/**
	 * Gets the atc status.
	 * 
	 * @return the atc status
	 */
	public int getAtcStatus() {
		return mAtcStatus;
	}

	/**
	 * Sets the atc status.
	 * 
	 * @param pAtcStatus
	 *            the new atc status
	 */
	public void setAtcStatus(int pAtcStatus) {
		this.mAtcStatus = pAtcStatus;
	}

	/**
	 * Gets the last updated.
	 * 
	 * @return the last updated
	 */
	public Date getLastUpdated() {
		return mLastUpdated;
	}

	/**
	 * Sets the last updated.
	 * 
	 * @param pLastUpdated
	 *            the new last updated
	 */
	public void setLastUpdated(Date pLastUpdated) {
		this.mLastUpdated = pLastUpdated;
	}

	/**
	 * Gets the created.
	 * 
	 * @return the created
	 */
	public Date getCreated() {
		return mCreated;
	}

	/**
	 * Sets the created.
	 * 
	 * @param pCreated
	 *            the new created
	 */
	public void setCreated(Date pCreated) {
		this.mCreated = pCreated;
	}

	/**
	 * Gets the record updated.
	 * 
	 * @return the record updated
	 */
	public Date getRecordUpdated() {
		return mRecordUpdated;
	}

	/**
	 * Sets the record updated.
	 * 
	 * @param pRecordUpdated
	 *            the new record updated
	 */
	public void setRecordUpdated(Date pRecordUpdated) {
		this.mRecordUpdated = pRecordUpdated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemQuantity [atcQuantity=" + mAtcQuantity + ", atcStatus=" + mAtcStatus + ", created=" + mCreated + ", lastUpdated="
				+ mLastUpdated + ", recordUpdated=" + mRecordUpdated + "]";
	}

}
