# Author:  Aleksandar Kocic
# Date:    2016-01-04
# Purpose: This WLST script creates Coherence domain.

#Create domain properties
domain_name = 'coh_domain'
domain_dir = '/data/atg/oracle/user_projects/domains/'
domain = domain_dir + domain_name

template_dir = '/data/atg/oracle/wlserver/common/templates/wls/'
wls_template = template_dir + 'wls.jar'
wls_coherence_template = template_dir + 'wls_coherence_template.jar'


#Create domain
print 'Creating domain: ', domain
print 'Template: ', wls_template
createDomain(wls_template, domain, 'weblogic', 'welcome1')
print 'Domain ', domain_name, ' created.'
readDomain(domain)
addTemplate(wls_coherence_template)
updateDomain()

print 'Domain ', domain_name, ' updated.'