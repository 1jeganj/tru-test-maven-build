package com.tru.messaging;



import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonBppInfoVO;
import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.epslon.TRUEpslonMessageBean;

/**This class perform the functionality of setting the request for myaccount activation email event.
 * @author PA
 * @version 1.0
 */
public class TRUEpslonOrderAcknowledgeSource extends GenericService  implements MessageSource {

	
	
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mPortName.
	 */
	private String mPortName;
	
	/**
	 * Holds the property value of mJmsId.
	 */
	private String mJmsId;
	
	/**
	 * Holds the property value of mContext.
	 */
	private MessageSourceContext mContext;
	/**
	 * Holds the property value of mStarted.
	 */
	boolean mStarted;
	
	/**
	 * This method is used to get email.
	 * @return email String
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 *This method is used to set email.
	 *@param pEmail String
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * This method is used to get epslonSource.
	 * @return epslonSource String
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}

	/**
	 *This method is used to set epslonSource.
	 *@param pEpslonSource String
	 */
	public void setEpslonSource(String pEpslonSource) {
		mEpslonSource = pEpslonSource;
	}

	/**
	 * This method is used to get epslonReferenceID.
	 * @return epslonReferenceID String
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}

	/**
	 *This method is used to set epslonReferenceID.
	 *@param pEpslonReferenceID String
	 */
	public void setEpslonReferenceID(String pEpslonReferenceID) {
		mEpslonReferenceID = pEpslonReferenceID;
	}

	/**
	 * This method is used to get epslonMessageType.
	 * @return epslonMessageType String
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}

	/**
	 *This method is used to set epslonMessageType.
	 *@param pEpslonMessageType String
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}

	/**
	 * This method is used to get epslonBrand.
	 * @return epslonBrand String
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}

	/**
	 *This method is used to set epslonBrand.
	 *@param pEpslonBrand String
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}

	/**
	 * This method is used to get epslonCompanyID.
	 * @return epslonCompanyID String
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}

	/**
	 *This method is used to set epslonCompanyID.
	 *@param pEpslonCompanyID String
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgLocale.
	 * @return epslonHeaderTypeMsgLocale String
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}

	/**
	 *This method is used to set epslonHeaderTypeMsgLocale.
	 *@param pEpslonHeaderTypeMsgLocale String
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgTimeZone.
	 * @return epslonHeaderTypeMsgTimeZone String
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}

	/**
	 *This method is used to set epslonHeaderTypeMsgTimeZone.
	 *@param pEpslonHeaderTypeMsgTimeZone String
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}

	/**
	 * This method is used to get epslonHeaderTypeDestination.
	 * @return epslonHeaderTypeDestination String
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}

	/**
	 *This method is used to set epslonHeaderTypeDestination.
	 *@param pEpslonHeaderTypeDestination String
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}

	/**
	 * This method is used to get epslonHeaderTypeInternalDateTimeStamp.
	 * @return epslonHeaderTypeInternalDateTimeStamp String
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}

	/**
	 *This method is used to set epslonHeaderTypeInternalDateTimeStamp.
	 *@param pEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}

	/**
	 * This method is used to get portName.
	 * @return portName String
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 *This method is used to set portName.
	 *@param pPortName String
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * This method is used to get jmsId.
	 * @return jmsId String
	 */
	public String getJmsId() {
		return mJmsId;
	}

	/**
	 *This method is used to set jmsId.
	 *@param pJmsId String
	 */
	public void setJmsId(String pJmsId) {
		mJmsId = pJmsId;
	}
	/**
	 * @param pContext the context to set
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;
	}
	
   /**
    * 
    * @return mContext
    */
	public MessageSourceContext getMessageSourceContext() {
		return mContext;
	}

	/**
	 * @return the context
	 */
	public MessageSourceContext getContext() {
		return mContext;
	}
	/**
	 * @param pContext the context to set
	 */
	public void setContext(MessageSourceContext pContext) {
		mContext = pContext;
	}
	
	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * This method is called in TRUEpslonAccountSource to receive the message and send in destination to be able to be received by queue.
	 * @param pOrderID - String
	 * @param pEmail - String 
	 * @param pEpslonBppItemMap - Map<String,TRUEpslonBppInfoVO> 
	 * @param pCustomerLoggedInstatus - boolean
	 * @param pShipDeliveryTimeMap - Map<String,String>
	 * @throws JMSException - JMSException
	 */
	public void sendOrderAcknowledgementEmail(String pOrderID,String pEmail,Map<String,TRUEpslonBppInfoVO> pEpslonBppItemMap,boolean pCustomerLoggedInstatus,Map<String,String> pShipDeliveryTimeMap,BigDecimal pIslandTax,BigDecimal pLocalTax)
			throws JMSException {
		
		if (isLoggingDebug()) {
			logDebug("START: TRUEpslonOrderAcknowledgeSource sendOrderAcknowledgementEmail() ");
		}
		
		
		ObjectMessage objectMessage;
		
		TRUEpslonMessageBean epslonObjMsg = new TRUEpslonMessageBean();
		
	    epslonObjMsg.setCustomerLoggedIn(!pCustomerLoggedInstatus);
	    if (isLoggingDebug()) {
			logDebug("Is Customer is Logged In::"+!pCustomerLoggedInstatus);
		}
		if(!pEpslonBppItemMap.isEmpty()){
			epslonObjMsg.setMessageBeanBppItemMap(pEpslonBppItemMap);
		}
		else{
			Map<String,TRUEpslonBppInfoVO> epslonBppItemMap = new HashMap<String, TRUEpslonBppInfoVO>();
			epslonObjMsg.setMessageBeanBppItemMap(epslonBppItemMap);
		}
		if(!pShipDeliveryTimeMap.isEmpty()){
			epslonObjMsg.setShippingDeliveryTimeMap(pShipDeliveryTimeMap);
		}
		else{
			Map<String,String> shipDeliveryTimeMap = new HashMap<String, String>();
			epslonObjMsg.setShippingDeliveryTimeMap(shipDeliveryTimeMap);
		}
		if(!StringUtils.isBlank(pEmail)){
			if (isLoggingDebug()) {
				logDebug("Recipient Email::"+pEmail);
			}
			epslonObjMsg.setEmail(pEmail);
			}
		
		if(!StringUtils.isBlank(getEpslonBrand())){
			epslonObjMsg.setEpslonBrand(getEpslonBrand());}
		if(!StringUtils.isBlank(getEpslonSource())){
			epslonObjMsg.setEpslonSource(getEpslonSource());}
		if(!StringUtils.isBlank(getEpslonReferenceID())){
			epslonObjMsg.setEpslonReferenceID(getEpslonReferenceID());}
		if(!StringUtils.isBlank(getEpslonCompanyID())){
			epslonObjMsg.setEpslonCompanyID(getEpslonCompanyID());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeDestination())){
			epslonObjMsg.setEpslonHeaderTypeDestination(getEpslonHeaderTypeDestination());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeInternalDateTimeStamp())){
			epslonObjMsg.setEpslonHeaderTypeInternalDateTimeStamp(getEpslonHeaderTypeInternalDateTimeStamp());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgTimeZone())){
			epslonObjMsg.setEpslonHeaderTypeMsgTimeZone(getEpslonHeaderTypeMsgTimeZone());}
		if(!StringUtils.isBlank(getEpslonMessageType())){
			epslonObjMsg.setEpslonMessageType(getEpslonMessageType());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgLocale())){
			epslonObjMsg.setEpslonHeaderTypeMsgLocale(getEpslonHeaderTypeMsgLocale());}
		if(!StringUtils.isBlank(pOrderID)){
			epslonObjMsg.setOrderID(pOrderID);
			}
		if(!StringUtils.isBlank(getEpslonConfiguration().getOrderSatusLinkType())){
		epslonObjMsg.setOrderSatusLinkType(getEpslonConfiguration().getOrderSatusLinkType());
		}
		if(epslonObjMsg.getPrIslandTax() != null){
			epslonObjMsg.setPrIslandTax(pIslandTax);
		}

		if(epslonObjMsg.getPrLocalTax() != null){
			epslonObjMsg.setPrLocalTax(pLocalTax);
		}
		try {
			if (getMessageSourceContext() != null) {
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				
				///objectMessage.setJMSMessageID(getJmsId());

				objectMessage.setObject(epslonObjMsg);

				if (isLoggingDebug()) {
					logDebug("objectMessage in sendOrderAcknowledgementEmail() Method...." + objectMessage);
				}
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				if (isLoggingDebug()) {
					logDebug("messageSourceContext...." + mContext);
				}
			}

		} catch (JMSException jmse) {
			if (isLoggingError()) {
				logError("JMS Exception in order acknowledgement Messaging::", jmse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUEpslonOrderAcknowledgeSource sendNewAccountEmail()");
		}
	}
	/**
	 * This Method stopMessageSource to log.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}
	/**
	 * This Method stopMessageSource to log.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * @param pStarted the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

}
