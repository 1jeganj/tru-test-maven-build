package com.tru.integrations.epslon.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

import com.tru.integrations.common.TRUEpslonBppInfoVO;
import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderCommon;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderCommon.OrderStatusURL;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderCommon.TaxDetails;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderCommon.TaxDetails.TaxDetail;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderLine;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderLine.S2HISPU;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderLine.S2HS2S;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderLine.S2HS2SISPU;
import com.tru.integrations.epslon.order.OrderAcknowledgement.Message.OrderLine.S2SISPU;
import com.tru.integrations.epslon.order.StoreInfoType.StoreHours;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Order Acknowledgement Email service.
 * 
 * @author PA.
 * @version 1.0
 *
 */
public class TRUEpslonOrderAcknowledgeService extends GenericService{
	
	/**
	 * Holds the mItemNamePropertyname.
	 */
	private String mItemNamePropertyname;
	/**
	 * Holds the mWarehousePickupFlagPropertyName.
	 */
	private String mWarehousePickupFlagPropertyName;
	/**
	 * Holds the mListPricePropertyName.
	 */
	private String mListPricePropertyName;
	/**
	 * Holds the mItemDescriptionPropertyname.
	 */
	private String mItemDescriptionPropertyname;
	/**
	 * Holds the mStoreItemDescName.
	 */
	private String mStoreItemDescName;
	
	/**
	 * Holds the mStoreNamePropertyName.
	 * 
	 */
	private String mStoreNamePropertyName;
	
	/**
	 * Holds the mStoreCityPropertyName.
	 */
	private String mStoreCityPropertyName;
	
	/**
	 * Holds the mStoreStatePropertyName.
	 */
	private String mStoreStatePropertyName;
	
	/**
	 * Holds the mStorePhonePropertyName.
	 */
	private String mStorePhonePropertyName;
	
	/**
	 * Holds the mStorePostalCodePropertyName.
	 */
	private String mStorePostalCodePropertyName;
	
	/**
	 * Holds the mStoreHoursPropertyName.
	 */
	private String mStoreHoursPropertyName;
	
	/**
	 * Holds the mStoreDayPropertyName.
	 */
	private String mStoreDayPropertyName;
	
	/**
	 * Holds the mStoreOpenHourPropertyName.
	 */
	private String mStoreOpenHourPropertyName;
	
	/**
	 * Holds the mStoreCloseHourPropertyName.
	 */
	private String mStoreCloseHourPropertyName;
	
	
	/**
	 * Holds the property value of mShipToHome.
	 */
	private String mShipToHome;
	/**
	 * Holds the property value of mShipToHomeAndIsPickUp.
	 */
	private String mShipToHomeAndIsPickUp;
	/**
	 * Holds the property value of mIsPickUp.
	 */
	private String mIsPickUp;
	/**
	 * Holds the property value of mShipToStore.
	 */
	private String mShipToStore;
	/**
	 * Holds the property value of mOrderManager.
	 */
	private OrderManager mOrderManager;
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private TRUEpslonHelper mEpslonHelper;
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private ProfileTools mProfileTools;
	
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	 /**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */

	private boolean mEnablePostMessageQueue;
	 
	 /**
	 * Holds the property value of mShipToHomeAndShipToStore.
	 */
	private String mShipToHomeAndShipToStore;
	
	/**
	 * Holds the property value of mShipToStoreAndInStorePickup.
	 */
	private String mShipToStoreAndInStorePickup;
	
	/**
	 * Holds the property value of mShipToHomeShipToStoreAndInStorePickup.
	 */
	private String mShipToHomeShipToStoreAndInStorePickup;
	/**
	 * This method is used to get itemNamePropertyname.
	 * @return mItemNamePropertyname String
	 */
	
	public String getItemNamePropertyname() {
		return mItemNamePropertyname;
	}
	/**
	 *This method is used to set itemNamePropertyname
	 *@param pItemNamePropertyname String
	 */
	public void setItemNamePropertyname(String pItemNamePropertyname) {
		mItemNamePropertyname = pItemNamePropertyname;
	}
	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}
	/**
	 *This method is used to set epslonHelper.
	 *@param pEpslonHelper TRUEpslonHelper
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}
	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}
	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}
	/**
	 * This method is used to get targetQueue.
	 * @return targetQueue String
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}
	/**
	 *This method is used to set targetQueue.
	 *@param pTargetQueue String
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	/**
	 * This method is used to get enablePostMessageQueue.
	 * @return enablePostMessageQueue boolean
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}
	/**
	 *This method is used to set enablePostMessageQueue.
	 *@param pEnablePostMessageQueue boolean
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}
	
	
	/**
	 * This method is used to get profileTools.
	 * @return profileTools ProfileTools
	 */
	public ProfileTools getProfileTools() {
		return mProfileTools;
	}
	/**
	 *This method is used to set profileTools.
	 *@param pProfileTools ProfileTools
	 */
	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	
	/**
	 * This method is used to get orderManager.
	 * @return orderManager OrderManager
	 */
	public OrderManager getOrderManager() {
		return mOrderManager;
	}
	
	/**
	 *This method is used to set orderManager.
	 *@param pOrderManager OrderManager
	 */
	
	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
	/**
	 * @return the shipToHome
	 */
	public String getShipToHome() {
		return mShipToHome;
	}
	/**
	 * @param pShipToHome the shipToHome to set
	 */
	public void setShipToHome(String pShipToHome) {
		mShipToHome = pShipToHome;
	}
	/**
	 * @return the shipToHomeAndIsPickUp
	 */
	public String getShipToHomeAndIsPickUp() {
		return mShipToHomeAndIsPickUp;
	}
	/**
	 * @param pShipToHomeAndIsPickUp the shipToHomeAndIsPickUp to set
	 */
	public void setShipToHomeAndIsPickUp(String pShipToHomeAndIsPickUp) {
		mShipToHomeAndIsPickUp = pShipToHomeAndIsPickUp;
	}
	/**
	 * @return the isPickUp
	 */
	public String getIsPickUp() {
		return mIsPickUp;
	}
	/**
	 * @param pIsPickUp the isPickUp to set
	 */
	public void setIsPickUp(String pIsPickUp) {
		mIsPickUp = pIsPickUp;
	}
	/**
	 * @return the shipToStore
	 */
	public String getShipToStore() {
		return mShipToStore;
	}
	/**
	 * @param pShipToStore the shipToStore to set
	 */
	public void setShipToStore(String pShipToStore) {
		mShipToStore = pShipToStore;
	}
	
	/**
	 * Holds the mLocationRepository
	 */
	private Repository mLocationRepository;

	/**
	 * @return the locationRepository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * @param pLocationRepository the locationRepository to set
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
	
	
	
	/**
	 * @return the storeNamePropertyName
	 */
	public String getStoreNamePropertyName() {
		return mStoreNamePropertyName;
	}
	/**
	 * @param pStoreNamePropertyName the storeNamePropertyName to set
	 */
	public void setStoreNamePropertyName(String pStoreNamePropertyName) {
		mStoreNamePropertyName = pStoreNamePropertyName;
	}
	/**
	 * @return the storeCityPropertyName
	 */
	public String getStoreCityPropertyName() {
		return mStoreCityPropertyName;
	}
	/**
	 * @param pStoreCityPropertyName the storeCityPropertyName to set
	 */
	public void setStoreCityPropertyName(String pStoreCityPropertyName) {
		mStoreCityPropertyName = pStoreCityPropertyName;
	}
	/**
	 * @return the storeStatePropertyName
	 */
	public String getStoreStatePropertyName() {
		return mStoreStatePropertyName;
	}
	/**
	 * @param pStoreStatePropertyName the storeStatePropertyName to set
	 */
	public void setStoreStatePropertyName(String pStoreStatePropertyName) {
		mStoreStatePropertyName = pStoreStatePropertyName;
	}
	/**
	 * @return the storePhonePropertyName
	 */
	public String getStorePhonePropertyName() {
		return mStorePhonePropertyName;
	}
	/**
	 * @param pStorePhonePropertyName the storePhonePropertyName to set
	 */
	public void setStorePhonePropertyName(String pStorePhonePropertyName) {
		mStorePhonePropertyName = pStorePhonePropertyName;
	}
	/**
	 * @return the storePostalCodePropertyName
	 */
	public String getStorePostalCodePropertyName() {
		return mStorePostalCodePropertyName;
	}
	/**
	 * @param pStorePostalCodePropertyName the storePostalCodePropertyName to set
	 */
	public void setStorePostalCodePropertyName(String pStorePostalCodePropertyName) {
		mStorePostalCodePropertyName = pStorePostalCodePropertyName;
	}
	/**
	 * @return the storeHoursPropertyName
	 */
	public String getStoreHoursPropertyName() {
		return mStoreHoursPropertyName;
	}
	/**
	 * @param pStoreHoursPropertyName the storeHoursPropertyName to set
	 */
	public void setStoreHoursPropertyName(String pStoreHoursPropertyName) {
		mStoreHoursPropertyName = pStoreHoursPropertyName;
	}
	/**
	 * @return the storeDayPropertyName
	 */
	public String getStoreDayPropertyName() {
		return mStoreDayPropertyName;
	}
	/**
	 * @param pStoreDayPropertyName the storeDayPropertyName to set
	 */
	public void setStoreDayPropertyName(String pStoreDayPropertyName) {
		mStoreDayPropertyName = pStoreDayPropertyName;
	}
	/**
	 * @return the storeOpenHourPropertyName
	 */
	public String getStoreOpenHourPropertyName() {
		return mStoreOpenHourPropertyName;
	}
	/**
	 * @param pStoreOpenHourPropertyName the storeOpenHourPropertyName to set
	 */
	public void setStoreOpenHourPropertyName(String pStoreOpenHourPropertyName) {
		mStoreOpenHourPropertyName = pStoreOpenHourPropertyName;
	}
	/**
	 * @return the storeCloseHourPropertyName
	 */
	public String getStoreCloseHourPropertyName() {
		return mStoreCloseHourPropertyName;
	}
	/**
	 * @param pStoreCloseHourPropertyName the storeCloseHourPropertyName to set
	 */
	public void setStoreCloseHourPropertyName(String pStoreCloseHourPropertyName) {
		mStoreCloseHourPropertyName = pStoreCloseHourPropertyName;
	}
	
	/**
	 * @return the storeItemDescName
	 */
	public String getStoreItemDescName() {
		return mStoreItemDescName;
	}
	/**
	 * @param pStoreItemDescName the storeItemDescName to set
	 */
	public void setStoreItemDescName(String pStoreItemDescName) {
		mStoreItemDescName = pStoreItemDescName;
	}
	
	/**
	 * @return the listPricePropertyName
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}
	/**
	 * @param pListPricePropertyName the listPricePropertyName to set
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		mListPricePropertyName = pListPricePropertyName;
	}
	/**
	 * @return the itemDescriptionPropertyname
	 */
	public String getItemDescriptionPropertyname() {
		return mItemDescriptionPropertyname;
	}
	/**
	 * @param pItemDescriptionPropertyname the itemDescriptionPropertyname to set
	 */
	public void setItemDescriptionPropertyname(String pItemDescriptionPropertyname) {
		mItemDescriptionPropertyname = pItemDescriptionPropertyname;
	}
	/**
	 * @return the shipToHomeAndShipToStore
	 */
	
	public String getShipToHomeAndShipToStore() {
		return mShipToHomeAndShipToStore;
	}
	/**
	 * @param pShipToHomeAndShipToStore the shipToHomeAndShipToStore to set
	 */
	
	public void setShipToHomeAndShipToStore(String pShipToHomeAndShipToStore) {
		mShipToHomeAndShipToStore = pShipToHomeAndShipToStore;
	}
	/**
	 * @return the shipToStoreAndInStorePickup
	 */
	public String getShipToStoreAndInStorePickup() {
		return mShipToStoreAndInStorePickup;
	}
	/**
	 * @param pShipToStoreAndInStorePickup the shipToStoreAndInStorePickup to set
	 */
	public void setShipToStoreAndInStorePickup(String pShipToStoreAndInStorePickup) {
		mShipToStoreAndInStorePickup = pShipToStoreAndInStorePickup;
	}
	/**
	 * @return the shipToHomeShipToStoreAndInStorePickup
	 */
	public String getShipToHomeShipToStoreAndInStorePickup() {
		return mShipToHomeShipToStoreAndInStorePickup;
	}
	/**
	 * @param pShipToHomeShipToStoreAndInStorePickup the shipToHomeShipToStoreAndInStorePickup to set
	 */
	public void setShipToHomeShipToStoreAndInStorePickup(
			String pShipToHomeShipToStoreAndInStorePickup) {
		mShipToHomeShipToStoreAndInStorePickup = pShipToHomeShipToStoreAndInStorePickup;
	}
	/**
	 * @return the warehousePickupFlagPropertyName
	 */
	
	public String getWarehousePickupFlagPropertyName() {
		return mWarehousePickupFlagPropertyName;
	}
	/**
	 * @param pWarehousePickupFlagPropertyName the warehousePickupFlagPropertyName to set
	 */
	public void setWarehousePickupFlagPropertyName(
			String pWarehousePickupFlagPropertyName) {
		mWarehousePickupFlagPropertyName = pWarehousePickupFlagPropertyName;
	}
	/**
	 * This method sets the message template for order acknowledgement email. 
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 */
	
	public void generateOrderAckRequest(TRUEpslonMessageBean pEpslonMessageBean){
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUepslonOrderAcknowledgeService generateOrderAckRequest()");
		}
		Order order = null;
		String orderID = pEpslonMessageBean.getOrderID();
		String email = pEpslonMessageBean.getEmail();
		boolean s2hIspuS2S = false;
		boolean s2hIsp = false;
		boolean s2sIsp = false;
		boolean s2hs2s = false;
		Boolean isSOS = false;
		String shippingType = TRUEpslonConstants.EMPTY_STRING;
		
		String orderAckTargetQueue = TRUEpslonConstants.EMPTY_STRING;
		String orderAckRequestXML = TRUEpslonConstants.EMPTY_STRING;
		
		if(!StringUtils.isBlank(orderID)){
			
			try {
				order =  getOrderManager().loadOrder(orderID);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError("CommerceException in TRUEpslonOrderAcknowledgeService generateOrderAckRequest() : ", e);
				}
			}
		}
		if (order == null){
			if (isLoggingDebug()) {
				vlogDebug("Order is NULL as the order ID is invalid");
			}
			return;
		}
		
		ObjectFactory factory = new ObjectFactory();	
        OrderAcknowledgement orderAck = factory.createOrderAcknowledgement();
        HeaderType header = getMessageHeader(pEpslonMessageBean,order);
        Message msg = factory.createOrderAcknowledgementMessage();
        LineItemType lineItem = factory.createLineItemType();
        OrderLine orderLine = factory.createOrderAcknowledgementMessageOrderLine();
		
		OrderCommon orderCommon = getOrderCommon(order,pEpslonMessageBean);
		
		S2HISPU s2hIspu = factory.createOrderAcknowledgementMessageOrderLineS2HISPU();
		S2HS2S s2HS2S = factory.createOrderAcknowledgementMessageOrderLineS2HS2S();
		S2SISPU s2SISPU = factory.createOrderAcknowledgementMessageOrderLineS2SISPU();
		
		S2HS2SISPU s2HS2SISPU = factory.createOrderAcknowledgementMessageOrderLineS2HS2SISPU();

		
		List<ShippingGroup> shippingGroupsPrior = order.getShippingGroups();
        Iterator<ShippingGroup> itrn = shippingGroupsPrior.iterator();
        ShippingGroup shippingGroupPrior = null;
        while (itrn.hasNext()) {
        	shippingGroupPrior = itrn.next();
        	if (shippingGroupPrior instanceof HardgoodShippingGroup) {
				shippingType = shippingType.concat(TRUEpslonConstants.HARDGOOD_SHIPP_CODE);

			}
			if (shippingGroupPrior instanceof InStorePickupShippingGroup) {
				if(getWarehousePickupFlagPropertyName() != null){				
        		isSOS = false;
        				//(Boolean) ((InStorePickupShippingGroup) shippingGroupPrior).getPropertyValue(getWarehousePickupFlagPropertyName());
        		}
        		if(isSOS){
        		shippingType = shippingType.concat(TRUEpslonConstants.SHIP_TO_STORE_CODE);
        		}
        		else{
				shippingType = shippingType.concat(TRUEpslonConstants.INSTORE_SHIP_CODE);
				}
			}
        }	
        
        if (shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE) &&
				 shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE) && shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)) {
        	if (isLoggingDebug()) {
				logDebug("Shipping contains all ISPU and S2H and S2S ::::");
			}
        	s2hIspuS2S = true;
        	orderLine.setS2HS2SISPU(s2HS2SISPU);
        	
		}
        else {
        	
        	if(shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE) &&
				 shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE)){
        	if (isLoggingDebug()) {
				logDebug("Shipping is both S2H and ISPU ::::");
			}
        	s2hIsp = true;
        	orderLine.setS2HISPU(s2hIspu);
           }
        	if(shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE) &&
    				 shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)){
            	if (isLoggingDebug()) {
    				logDebug("Shipping is both S2H and S2S::::");
    			}
            	s2hs2s = true;
            	orderLine.setS2HS2S(s2HS2S);
            }
        	if(shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE) &&
    				 shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE)){
            	if (isLoggingDebug()) {
    				logDebug("Shipping is both S2S and ISPU ::::");
    			}
            	s2sIsp = true;
            	orderLine.setS2SISPU(s2SISPU);
            }
	    }
        List<ShippingGroup> shippingGroups = order.getShippingGroups();
        Iterator<ShippingGroup> it = shippingGroups.iterator();
        ShippingGroup shippingGroup = null;
        while (it.hasNext()) {
        	shippingGroup = it.next();
			if (shippingGroup instanceof HardgoodShippingGroup) {
				
				if (isLoggingDebug()) {
					logDebug("Shipping Group is of type HardgoodShippingGroup");
				}
				if (isLoggingDebug()) {
					logDebug("Setting LineItem For ShipToHome");
				}
				
				ShipToHomeType shipToHomeType = factory.createShipToHomeType();
				ShipToDetailType shipToDetail = factory.createShipToDetailType();
				List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
				Iterator<CommerceItemRelationship> itr = itemList.iterator();
				int itemIndex = TRUEpslonConstants.ONE;
				while (itr.hasNext()) {
					
					CommerceItemRelationship cItemRel = itr.next();
					String relationShipId = cItemRel.getId();
					lineItem = getLineItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
					if (lineItem != null) {
						shipToHomeType.getLineItem().add(lineItem);
					}
					if(!pEpslonMessageBean.getMessageBeanBppItemMap().isEmpty() && pEpslonMessageBean.getMessageBeanBppItemMap().containsKey(relationShipId)){
    					LineItemType bppItem = factory.createLineItemType();
    					itemIndex++;
    					bppItem = getBppItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
    					if(bppItem != null){
    						shipToHomeType.getLineItem().add(bppItem);
    					}
    					}
					itemIndex++;
				}
				if (isLoggingDebug()) {
					logDebug("Setting ShipToDetail");
				}
				shipToDetail = getShipToDetail(order,shippingGroup,email);
				if (shipToDetail != null) {
					shipToHomeType.setShippingAddress(shipToDetail);
				}

				if (shipToHomeType != null) {
				if (s2hIspuS2S && orderLine.getS2HS2SISPU() != null){	
					orderLine.getS2HS2SISPU().getShipToHome().add(shipToHomeType);
				}
				else if(s2hIsp && orderLine.getS2HISPU() != null){
						orderLine.getS2HISPU().getShipToHome().add(shipToHomeType);
					}else if(s2hs2s && orderLine.getS2HS2S() != null){
						orderLine.getS2HS2S().getShipToHome().add(shipToHomeType);
					}else{
						orderLine.getShipToHome().add(shipToHomeType);
					}
				  }
				}				
        	if(shippingGroup instanceof InStorePickupShippingGroup){
        		if (isLoggingDebug()) {
        			logDebug("Shipping Group is of type InStorePickupShippingGroup ");
        		}
        		if(getWarehousePickupFlagPropertyName() != null){
        		isSOS = false;
        				//(Boolean) ((InStorePickupShippingGroup) shippingGroup).getPropertyValue(getWarehousePickupFlagPropertyName());
        		}
        		if(isSOS){
        			if (isLoggingDebug()) {
    					logDebug("Setting LineItem For ShipToStore");
    				}
            		S2SType s2SType = factory.createS2SType();
            		List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
    				Iterator<CommerceItemRelationship> itr = itemList.iterator();
    				int itemIndex = TRUEpslonConstants.ONE;
    				while (itr.hasNext()) {
    					CommerceItemRelationship cItemRel = itr.next();
    					String relationShipId = cItemRel.getId();
    					lineItem = getLineItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
    					if (lineItem != null) {
    						s2SType.getLineItem().add(lineItem);
    					}
    					if(!pEpslonMessageBean.getMessageBeanBppItemMap().isEmpty() && pEpslonMessageBean.getMessageBeanBppItemMap().containsKey(relationShipId)){
    					LineItemType bppItem = factory.createLineItemType();
    					itemIndex++;
    					bppItem = getBppItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
    					if(bppItem != null){
    						s2SType.getLineItem().add(bppItem);
    					}
    					}
    					itemIndex++;
    				}
            		if (isLoggingDebug()) {
            			logDebug("Setting the Store Info ");
            		}
            		StoreInfoType storeInfoType = getStoreInfo(shippingGroup);
            		if(storeInfoType != null){
            			s2SType.setStoreInfo(storeInfoType);
            			if(s2SType != null){
            				if (s2hIspuS2S && orderLine.getS2HS2SISPU() != null){	
            					orderLine.getS2HS2SISPU().getS2S().add(s2SType);
            				}
            				else if(s2hs2s && orderLine.getS2HS2S() != null){
    	        					orderLine.getS2HS2S().getS2S().add(s2SType);
    	        				}else if(s2sIsp && orderLine.getS2SISPU() != null){
    	        					orderLine.getS2SISPU().getS2S().add(s2SType);
    	        				}else{
    	        					orderLine.getS2S().add(s2SType);
    	        				}
            				}
            			}
            		}
        		else{
        			if (isLoggingDebug()) {
    					logDebug("Setting LineItem For InstorePickUP");
    				}
            		ISPUType ispu = factory.createISPUType();
            		List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
    				Iterator<CommerceItemRelationship> itr = itemList.iterator();
    				int itemIndex = TRUEpslonConstants.ONE;
    				while (itr.hasNext()) {
    					CommerceItemRelationship cItemRel = itr.next();
    					String relationShipId = cItemRel.getId();
    					lineItem = getLineItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
    					if (lineItem != null) {
    						ispu.getLineItem().add(lineItem);
    					}
    					if(!pEpslonMessageBean.getMessageBeanBppItemMap().isEmpty() && pEpslonMessageBean.getMessageBeanBppItemMap().containsKey(relationShipId)){
        					LineItemType bppItem = factory.createLineItemType();
        					itemIndex++;
        					bppItem = getBppItem(cItemRel,shippingGroup,itemIndex,pEpslonMessageBean);
        					if(bppItem != null){
        						ispu.getLineItem().add(bppItem);
        					}
        					}
    					itemIndex++;
    				}
            		if (isLoggingDebug()) {
            			logDebug("Setting the Store Info ");
            		}
            		StoreInfoType storeInfoType = getStoreInfo(shippingGroup);
            		if(storeInfoType != null){
            			ispu.setStoreInfo(storeInfoType);
            			if(ispu != null){
            				if (s2hIspuS2S && orderLine.getS2HS2SISPU() != null){	
            					orderLine.getS2HS2SISPU().getISPU().add(ispu);
            				}
            				else if(s2hIsp && orderLine.getS2HISPU() != null){
    	        					orderLine.getS2HISPU().getISPU().add(ispu);
    	        				}else if(s2sIsp && orderLine.getS2SISPU() != null){
    	        					       orderLine.getS2SISPU().getISPU().add(ispu);
			    	        				}else{
			    	        					orderLine.getISPU().add(ispu);
			    	        				}
            				       
            			     }
            		      }
        		      }
        	}
        }
		if(orderCommon != null){
		msg.setOrderCommon(orderCommon);
		}
		if(orderLine != null){
		msg.setOrderLine(orderLine);
		}
		if(header != null){
		orderAck.setHeader(header);
		}
		if(msg != null){
		orderAck.setMessage(msg);
		}
		String packageName = TRUEpslonConstants.ORDER_ACK_PACKAGE_NAME;
		if(getEpslonHelper() != null){
		orderAckRequestXML = getEpslonHelper().generateXML(orderAck,packageName);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Order Acknowledgement Request XML --->");
			logDebug(orderAckRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		orderAckTargetQueue =  getTargetQueue();
		}
		if(isEnablePostMessageQueue() && !StringUtils.isBlank(orderAckTargetQueue) && getEpslonHelper() != null){
			getEpslonHelper().postMessageToQueue(orderAckRequestXML,orderAckTargetQueue);
		}
		
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUepslonOrderAcknowledgeService generateOrderAckRequest()");
		}
        
	}
	
	/**
	 * This method sets the common details for order. 
	 * @param pOrder Order.
	 * @param pEpslonMessageBean TRUEpslonMessageBean.
	 * @return orderCommon OrderCommon.
	 */
	public OrderCommon getOrderCommon(Order pOrder,TRUEpslonMessageBean pEpslonMessageBean) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getOrderCommon()");
		}
		OrderCommon orderCommon = null;
		String firstName = TRUEpslonConstants.EMPTY_STRING;
		String lastName = TRUEpslonConstants.EMPTY_STRING;
		String orderStatURLLinkType = TRUEpslonConstants.EMPTY_STRING;
		String orderStatURLLinkValue=TRUEpslonConstants.EMPTY_STRING;
		String OrderNumber = TRUEpslonConstants.EMPTY_STRING;
		String email = TRUEpslonConstants.EMPTY_STRING;
		if(pOrder != null){
		ObjectFactory factory = new ObjectFactory();
		orderCommon = factory.createOrderAcknowledgementMessageOrderCommon();
		OrderStatusURL orderStatusURL= factory.createOrderAcknowledgementMessageOrderCommonOrderStatusURL();
		TaxDetails taxDetails = factory.createOrderAcknowledgementMessageOrderCommonTaxDetails();
		TaxDetail taxDetail = factory.createOrderAcknowledgementMessageOrderCommonTaxDetailsTaxDetail();
	    if(!StringUtils.isBlank(pEpslonMessageBean.getEmail())){
	    	email = pEpslonMessageBean.getEmail();
	    }
	    if(!StringUtils.isBlank(email)){
	    	
			RepositoryItem profileItem = getProfileItem(pOrder);
			
		    PropertyManager propertyManager = getProfileTools().getPropertyManager();
		   if(propertyManager.getEmailAddressPropertyName() != null){
		    if(profileItem != null && profileItem.getPropertyValue(propertyManager.getEmailAddressPropertyName()) != null){
		    	email = (String)profileItem.getPropertyValue(propertyManager.getEmailAddressPropertyName()).toString();
		    }else{
		    	if (isLoggingDebug()) {
					logDebug("Either ProfileItem is NULL or Email is Empty::");
				}
		    }
		   }
	    }
	    boolean customerLoogedInStatus = pEpslonMessageBean.isCustomerLoggedIn();
	    if (isLoggingDebug()) {
			logDebug("Is Customer is Logged In::"+customerLoogedInStatus);
		}
	    if(!StringUtils.isBlank(email) && getEpslonHelper() != null){
	    	if(customerLoogedInStatus){
	             firstName=getEpslonHelper().getFirstName(email);
	             lastName=getEpslonHelper().getLastName(email);
	    }else{
	    	firstName=getEpslonHelper().getFirstNameFromEmail(email);
	    	}
	    }
	    	    
		if (isLoggingDebug()) {
			logDebug("FirstName ::"+firstName);
		}
	    
	    if (isLoggingDebug()) {
			logDebug("LastName::"+lastName);
		}
		if(!StringUtils.isBlank(firstName)){
	    	orderCommon.setFirstName(firstName);
		}else{
			orderCommon.setFirstName(TRUEpslonConstants.EMPTY_STRING);
		}
		
		if(!StringUtils.isBlank(lastName)){
			orderCommon.setLastName(lastName);
		}else{
			orderCommon.setLastName(TRUEpslonConstants.EMPTY_STRING);
		}
	    if(!StringUtils.isBlank(pOrder.getId())){
	    	OrderNumber=pOrder.getId().toString();
	    }
	    orderCommon.setOrderNumber(OrderNumber);
		if (isLoggingDebug()) {
			logDebug("Order Number::"+OrderNumber);
		}
		BigDecimal orderTotal= BigDecimal.ZERO;
	
		taxDetail.setTaxType("Island Tax");
		taxDetail.setTaxAmount(pEpslonMessageBean.getPrIslandTax());
		
		orderCommon.getTaxDetails().getTaxDetail().add(taxDetail);
		
		taxDetail.setTaxType("PR Tax");
		taxDetail.setTaxAmount(pEpslonMessageBean.getPrLocalTax());
		
		orderCommon.getTaxDetails().getTaxDetail().add(taxDetail);
				
		BigDecimal estimatedTax = new  BigDecimal(TRUEpslonConstants.D_ZERO);
			if(pOrder.getPriceInfo() != null){
				double rawTotal = pOrder.getPriceInfo().getTotal();
				String orderTotals = Double.toString(rawTotal);
				orderTotal = new BigDecimal(orderTotals);
				orderTotal = orderTotal.setScale(TRUEpslonConstants.TWO, BigDecimal.ROUND_CEILING);
				double rawTax = pOrder.getPriceInfo().getTax();
				String tax = Double.toString(rawTax);
				estimatedTax = new BigDecimal(tax);
				estimatedTax = estimatedTax.setScale(TRUEpslonConstants.TWO, BigDecimal.ROUND_CEILING);
			}
		if (isLoggingDebug()) {
			logDebug("Order Total::"+orderTotal);
		}
		
		
		orderCommon.setOrderTotal(orderTotal);
		orderCommon.setEstimatedTax(estimatedTax);
		if(pEpslonMessageBean != null){
			if(!StringUtils.isBlank(pEpslonMessageBean.getOrderSatusLinkType())){
			orderStatURLLinkType=pEpslonMessageBean.getOrderSatusLinkType();
			}
			if(!StringUtils.isBlank(pEpslonMessageBean.getOrderSatusLinkValue())){
			orderStatURLLinkValue=pEpslonMessageBean.getOrderSatusLinkValue();
			}
		}
		orderStatusURL.getLinkType().add(orderStatURLLinkType);
		orderStatusURL.getLinkValue().add(orderStatURLLinkValue);
		orderCommon.getOrderStatusURL().add(orderStatusURL);
		
		
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpslonOrderAcknowledgeService getOrderCommon()");
		}
		}
		else{
			if (isLoggingDebug()) {
				logDebug("Order Object is NULL");
			}
		}
		return orderCommon;
	}
	
	/**
	 * This method sets the line item.
	 * @param pItemRel -CommerceItem
	 * @param pShippingGroup -ShippingGroup
	 * @param pItemIndex -int
	 * @param pEpslonMessageBean -TRUEpslonMessageBean
	 * @return lineItemType LineItemType
	 */
	public LineItemType getLineItem(CommerceItemRelationship pItemRel,ShippingGroup pShippingGroup,int pItemIndex,TRUEpslonMessageBean pEpslonMessageBean) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getLineItem()");
		}
		CommerceItem commerceItem = null;
		String shippingMethod = TRUEpslonConstants.EMPTY_STRING;
		String itemDescription = TRUEpslonConstants.EMPTY_STRING;
		String itemId = TRUEpslonConstants.EMPTY_STRING;
		BigDecimal itemPrice = BigDecimal.ZERO;
		long itemQty = 0;
		if(pItemRel != null){
			commerceItem = pItemRel.getCommerceItem();
			itemQty = pItemRel.getQuantity();  
		}
		ObjectFactory factory = new ObjectFactory();
		LineItemType lineItemType = factory.createLineItemType();
		if(commerceItem != null){
	    	if(!StringUtils.isBlank(commerceItem.getCatalogRefId())){
	         itemId = commerceItem.getCatalogRefId();
	    	}
			
		    RepositoryItem skuItem = (RepositoryItem)commerceItem.getAuxiliaryData().getCatalogRef();
		    if(skuItem != null && skuItem.getPropertyValue(getItemDescriptionPropertyname()) != null){
		    itemDescription = (String)skuItem.getPropertyValue(getItemDescriptionPropertyname());
		    itemDescription = formattedItemName(itemDescription);
		    }
		    else if(skuItem != null && skuItem.getPropertyValue(getItemNamePropertyname()) != null){
		    itemDescription = (String)skuItem.getPropertyValue(getItemNamePropertyname());
		    itemDescription = formattedItemName(itemDescription);
		    }
		    skuItem.getPropertyValue(getListPricePropertyName());
			if(commerceItem.getPriceInfo() != null){
				double salePrice = commerceItem.getPriceInfo().getSalePrice();
				String stringSalePrice = Double.toString(salePrice);
				itemPrice= new BigDecimal(stringSalePrice);
				itemPrice = itemPrice.setScale(TRUEpslonConstants.TWO, BigDecimal.ROUND_CEILING);
			}
			if(pShippingGroup != null){
			shippingMethod = getShippingMethod(pShippingGroup);
			
			if (pShippingGroup instanceof HardgoodShippingGroup) {
				String shippingGroupId = pShippingGroup.getId();
				String deliveryTime = TRUEpslonConstants.EMPTY_STRING;
				//hardGoodSG = (HardgoodShippingGroup) pShippingGroup;
				if(pEpslonMessageBean.getShippingDeliveryTimeMap() != null && (!(pEpslonMessageBean.getShippingDeliveryTimeMap().isEmpty())) && pEpslonMessageBean.getShippingDeliveryTimeMap().containsKey(shippingGroupId)){
					deliveryTime = pEpslonMessageBean.getShippingDeliveryTimeMap().get(shippingGroupId);
				}
				shippingMethod = shippingMethod.concat(TRUEpslonConstants.COLON).concat(TRUEpslonConstants.OPEN_BRACES).concat(deliveryTime).concat(TRUEpslonConstants.CLOSED_BRACES);
			}
			
			}
			BigDecimal itemQuantity = new  BigDecimal(itemQty);
	        lineItemType.setItemQuantity(itemQuantity);
	        lineItemType.setItemNumber(itemId);
	        lineItemType.setItemPrice(itemPrice);
	        lineItemType.setShippingMethod(shippingMethod);
			lineItemType.setItemDescription(itemDescription);
			lineItemType.setLineNumber(pItemIndex);
	        }
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonOrderAcknowledgeService getLineItem()");
		}
		return lineItemType;
	}

	/**
	 * This method sets the store information.
	 * @param pShippingGroup ShippingGroup  
	 * @return storeInfoType StoreInfoType
	 */
	
	public StoreInfoType getStoreInfo(ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getStoreInfo()");
		}
		
		ObjectFactory factory = new ObjectFactory();
		StoreInfoType  storeInfoType = factory.createStoreInfoType();
		String storeName=TRUEpslonConstants.EMPTY_STRING;
		String storePhoneNumber=TRUEpslonConstants.EMPTY_STRING;
		String storePostalCode=	TRUEpslonConstants.EMPTY_STRING;
		String storeCity=	TRUEpslonConstants.EMPTY_STRING;
		String storeState=	TRUEpslonConstants.EMPTY_STRING;
		int storeNumber = TRUEpslonConstants.TEST_STORE_NUMBER;
		
		if(pShippingGroup != null){
			InStorePickupShippingGroup inStoreSG = (InStorePickupShippingGroup) pShippingGroup;
			String storeLocID = inStoreSG.getLocationId();
			try {
			RepositoryItem item = (RepositoryItem)getLocationRepository().getItem(storeLocID,getStoreItemDescName());
			if(item != null){
			if(item.getPropertyValue(getStoreNamePropertyName()) != null){	
			
			 storeName=	(String)item.getPropertyValue(getStoreNamePropertyName());}
			if(item.getPropertyValue(getStorePhonePropertyName()) != null){	
			 storePhoneNumber=	(String) item.getPropertyValue(getStorePhonePropertyName());}
			if(item.getPropertyValue(getStorePostalCodePropertyName()) != null){	
			 storePostalCode=	(String)item.getPropertyValue(getStorePostalCodePropertyName());
			}
			if(item.getPropertyValue(getStoreCityPropertyName()) != null){	
			 storeCity=	(String)item.getPropertyValue(getStoreCityPropertyName());
			}
			if(item.getPropertyValue(getStoreStatePropertyName()) != null){	
			storeState=	(String)item.getPropertyValue(getStoreStatePropertyName());
			}
			if(!StringUtils.isBlank(storeLocID)){
				storeNumber = Integer.parseInt(storeLocID);
			}
			storeInfoType.setStoreCity(storeCity);
			storeInfoType.setStoreName(storeName);
			storeInfoType.setStoreNumber(storeNumber);
			storeInfoType.setStorePhone(storePhoneNumber);
			storeInfoType.setStorePostalCode(storePostalCode);
			storeInfoType.setStoreState(storeState);
			
			List<RepositoryItem> storeHoursList= new ArrayList<RepositoryItem>();
			storeHoursList =	(List<RepositoryItem>) item.getPropertyValue(getStoreHoursPropertyName());
			if (isLoggingDebug()) {
				logDebug("STORE HOURS LIST::"+storeHoursList);
			}
			Iterator itr = storeHoursList.iterator();
			while(itr.hasNext()){
				if (isLoggingDebug()) {
					logDebug("SETTING STORE HOURS: ");
				}
				StoreHours storeHours = factory.createStoreInfoTypeStoreHours();
				RepositoryItem storeHoursItem =	(RepositoryItem) itr.next();
				String storeDay = (String) storeHoursItem.getPropertyValue(getStoreDayPropertyName());
				Double storeOpenTime = (Double) storeHoursItem.getPropertyValue(getStoreOpenHourPropertyName());
				Double storeCloseTime =(Double) storeHoursItem.getPropertyValue(getStoreCloseHourPropertyName());
				
				if(storeDay != null){
					storeHours.setDay(storeDay);
				}
				if(storeOpenTime != null){
					String openingTime = getEpslonHelper().getStoreHourTimeInProperFormat(storeOpenTime.toString());
					storeHours.setStoreOpenTime(openingTime);
				}
				if(storeCloseTime != null){
					String closingTime = getEpslonHelper().getStoreHourTimeInProperFormat(storeCloseTime.toString());
					storeHours.setStoreCloseTime(closingTime);
				}
				storeInfoType.getStoreHours().add(storeHours);
			}
			if(storeHoursList.isEmpty()){
				if (isLoggingDebug()) {
					logDebug("STORE HOURS IS EMPTY: SETTING DEFAULT VALUE");
				}
				StoreHours storeHours = factory.createStoreInfoTypeStoreHours();
				int storeIndex=0;
				while(storeIndex <= TRUEpslonConstants.SIX){
				String storeDay= TRUEpslonConstants.STORE_DEFAULT_DAY;
				Double storeOpenTime = new Double(TRUEpslonConstants.DEFAULT_OPEN_TIME);
				Double storeCloseTime = new Double(TRUEpslonConstants.DEFAULT_CLOSE_TIME);
				String openingTime = getEpslonHelper().getStoreHourTimeInProperFormat(storeOpenTime.toString());
				String closingTime = getEpslonHelper().getStoreHourTimeInProperFormat(storeCloseTime.toString());
				storeHours.setDay(storeDay);
				storeHours.setStoreOpenTime(openingTime);
				storeHours.setStoreCloseTime(closingTime);
				storeInfoType.getStoreHours().add(storeHours);
				storeIndex++;
				}
			}
			}	
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUEpslonOrderAcknowledgeService getStoreInfo : ", e);
					}
			}
		}
		else{
			if (isLoggingDebug()) {
				logDebug("Shipping Group is Null in getStoreInfo() Method:::");
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpslonOrderAcknowledgeService getStoreInfo()");
		}
		return storeInfoType;
	}

	/**
	 * This method sets the ship details.
	 * @param pOrder Order
	 * @param pShippingGroup ShippingGroup
	 * @param pEmail String
	 * @return shipToDetailType ShipToDetailType
	 */
	public ShipToDetailType getShipToDetail(Order pOrder,ShippingGroup pShippingGroup,String pEmail) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getShipToDetail()");
		}
		String firstName = TRUEpslonConstants.EMPTY_STRING;
		String lastName=TRUEpslonConstants.EMPTY_STRING;
		String addressLine1=TRUEpslonConstants.EMPTY_STRING;
		String addressLine2=TRUEpslonConstants.EMPTY_STRING;
		String addressLine3=TRUEpslonConstants.EMPTY_STRING;
		String city=TRUEpslonConstants.EMPTY_STRING;
		String state=TRUEpslonConstants.EMPTY_STRING;
		String country=TRUEpslonConstants.COUNTRY;
		String postalCode=TRUEpslonConstants.EMPTY_STRING;
		String email = TRUEpslonConstants.EMPTY_STRING;
		String county = TRUEpslonConstants.EMPTY_STRING;
		String phoneNumber = TRUEpslonConstants.TEST_PHONE;
		String fax = TRUEpslonConstants.TEST_FAX;
		
		ObjectFactory factory = new ObjectFactory();
		ShipToDetailType shipToDetailType = factory.createShipToDetailType();
		
		

		 HardgoodShippingGroup hardGoodShipGroup = (HardgoodShippingGroup) pShippingGroup;
		
		if(hardGoodShipGroup != null){
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getFirstName())){	
		firstName = hardGoodShipGroup.getShippingAddress().getFirstName();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getLastName())){
		lastName=hardGoodShipGroup.getShippingAddress().getLastName();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getAddress1())){
		addressLine1=hardGoodShipGroup.getShippingAddress().getAddress1();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getAddress2())){
		addressLine2=hardGoodShipGroup.getShippingAddress().getAddress2();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getAddress3())){
		addressLine3=hardGoodShipGroup.getShippingAddress().getAddress3();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getCity())){
		city=hardGoodShipGroup.getShippingAddress().getCity();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getState())){
		state=hardGoodShipGroup.getShippingAddress().getState();
		}
		
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getCounty())){
			county=hardGoodShipGroup.getShippingAddress().getCounty();
		}
		if(!StringUtils.isBlank(hardGoodShipGroup.getShippingAddress().getPostalCode())){
		postalCode=	hardGoodShipGroup.getShippingAddress().getPostalCode();
		}
		if(hardGoodShipGroup.getShippingAddress() != null){
			ContactInfo contactInfo =(ContactInfo) hardGoodShipGroup.getShippingAddress();
			if(contactInfo != null && contactInfo.getFaxNumber() != null){
				fax = contactInfo.getFaxNumber();	
			}
			if(contactInfo != null && contactInfo.getPhoneNumber() != null){
				phoneNumber = contactInfo.getPhoneNumber();
			}			
			}
		}
		if(!StringUtils.isBlank(pEmail)){
			email=	pEmail;
			}
		shipToDetailType.setShipToFirstName(firstName);
		shipToDetailType.setShipToLastName(lastName);
		shipToDetailType.setShipToAddressLine1(addressLine1);
		shipToDetailType.setShipToAddressLine2(addressLine2);
		shipToDetailType.setShipToAddressLine3(addressLine3);
		shipToDetailType.setShipToCity(city);
		shipToDetailType.setShipToState(state);
		shipToDetailType.setShipToCountry(country);
		shipToDetailType.setShipToCounty(county);
		shipToDetailType.setShipToEmail(email);
		shipToDetailType.setShipToPostalCode(postalCode);
		
		if (isLoggingDebug()) {
			logDebug("TRUEpslonOrderAcknowledgeService ShipToFax:::" + fax);
		}
		shipToDetailType.setShipToFax(fax);
		shipToDetailType.setShipToPhone(phoneNumber);
		
		shipToDetailType.setShipToState(state);
		
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpslonOrderAcknowledgeService getShipToDetail()");
		}
		return shipToDetailType;
	}

	/**
	 * This method sets the message header.
	 * @param pOrder Order
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	 
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean,Order pOrder) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getMessageHeader()");
		}
		ObjectFactory factory = new ObjectFactory();
        HeaderType header = factory.createHeaderType();
        String messageType = TRUEpslonConstants.EMPTY_STRING;
        String email= TRUEpslonConstants.EMPTY_STRING;
        String epslonSource= TRUEpslonConstants.EMPTY_STRING;
        String epslonReferenceID= TRUEpslonConstants.EMPTY_STRING;
        String epslonBrand= TRUEpslonConstants.EMPTY_STRING;
        String epslonCompanyID= TRUEpslonConstants.EMPTY_STRING;
        String epslonHeaderTypeMsgLocale= TRUEpslonConstants.EMPTY_STRING;
        String epslonHeaderTypeMsgTimeZone= TRUEpslonConstants.EMPTY_STRING;
        String epslonHeaderTypeDestination= TRUEpslonConstants.EMPTY_STRING;
       
        if(!StringUtils.isBlank(pEpslonMessageBean.getEmail())){
           email = pEpslonMessageBean.getEmail();	
         }
   		if(pOrder != null){
        messageType = getMessageID(pOrder);
   		}
   		
		if(pEpslonMessageBean.getEpslonSource() != null){
			epslonSource =pEpslonMessageBean.getEpslonSource();
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
			epslonReferenceID = pEpslonMessageBean.getEpslonReferenceID();
		}
		
		if(pEpslonMessageBean.getEpslonBrand() != null){
			epslonBrand = pEpslonMessageBean.getEpslonBrand();
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
			epslonCompanyID = pEpslonMessageBean.getEpslonCompanyID();
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
			epslonHeaderTypeMsgLocale = pEpslonMessageBean.getEpslonHeaderTypeMsgLocale();
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
			epslonHeaderTypeMsgTimeZone =pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone();
		}
		
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
			epslonHeaderTypeDestination = pEpslonMessageBean.getEpslonHeaderTypeDestination();
		}
		if(!StringUtils.isBlank(email)){
		header.setRecepientEmailAddress(email);
		}
		header.setSource(epslonSource);
		header.setReferenceID(epslonReferenceID);
		header.setMessageType(messageType);
		header.setBrand(epslonBrand);
		header.setCompanyID(epslonCompanyID);
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(epslonHeaderTypeMsgLocale));
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(epslonHeaderTypeMsgTimeZone));
		header.setDestination(factory.createHeaderTypeDestination(epslonHeaderTypeDestination));
		
		
		if(getEpslonConfiguration() != null){
			header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
			}
		
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpslonOrderAcknowledgeService getMessageHeader()");
		}
		return header;
	}
   /**
	 * This method is used to get the shipping method from ShippingGroup.
	 * @param pShippingGroup ShippingGroup
	 * @return shippingMethod  String
	 */
   public String getShippingMethod (ShippingGroup pShippingGroup) {
		if (isLoggingDebug()) {
			logDebug("ENTER:::TRUEpslonOrderAcknowledgeService getShippingMethod()");
		}

		String shippingMethod = TRUEpslonConstants.EMPTY_STRING;
		if (pShippingGroup != null) {
			if (pShippingGroup instanceof HardgoodShippingGroup) {
				HardgoodShippingGroup hardGoodShipGroup = (HardgoodShippingGroup) pShippingGroup;
				shippingMethod = hardGoodShipGroup.getShippingMethod();
				if(!StringUtils.isBlank(shippingMethod) && getEpslonConfiguration() != null){
					if(getEpslonConfiguration().getEpslonShipMethodMap().get(shippingMethod) != null){
					shippingMethod = getEpslonConfiguration().getEpslonShipMethodMap().get(shippingMethod);
					}
				}else{
					if (isLoggingDebug()) {
						logDebug("Shipping method is Empty or EpslonConfiguration is not initialized");
					}
				}
			}
			if (pShippingGroup instanceof InStorePickupShippingGroup) {
				InStorePickupShippingGroup inStorePickupSG = (InStorePickupShippingGroup) pShippingGroup;
				shippingMethod = inStorePickupSG.getShippingMethod();
			}
		}
		if (isLoggingDebug()) {
			logDebug("EXIT:::TRUEpslonOrderAcknowledgeService getShippingMethod()");
		}
		return shippingMethod;
   }
   /**
	 * This method is get profileItem from Order.
	 * @param pOrder Order
	 * @return profileItem  RepositoryItem
	 */
   public RepositoryItem getProfileItem(Order pOrder){
		if (isLoggingDebug()) {
			logDebug("ENTER:::TRUEpslonOrderAcknowledgeService getProfileItem()");
		}

		RepositoryItem profileItem = null;
		String profileId = pOrder.getProfileId();
		if (!StringUtils.isBlank(profileId)) {
			try {
				profileItem = getProfileTools().getProfileRepository().getItem(
						profileId, TRUEpslonConstants.USER);
				
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("Repository Exception in getProfileItem method", e);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("EXIT:::TRUEpslonOrderAcknowledgeService getProfileItem()");
		}
		return profileItem;
   }
   /**
  	 * This method is used get the messageType according to the shipping type.
  	 * @param pOrder Order
  	 * @return messageID  String
  	 */
   
   public String getMessageID(Order pOrder){
		if (isLoggingDebug()) {
			logDebug("ENTER:::TRUEpslonOrderAcknowledgeService getMessageID()");
		}
		String messageID = TRUEpslonConstants.EMPTY_STRING;
		String shippingType = TRUEpslonConstants.EMPTY_STRING;
		Boolean isSOS = false;
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		Iterator<ShippingGroup> shippingGroupitr = shippingGroups.iterator();
		ShippingGroup sgGroup = null;
		while (shippingGroupitr.hasNext()) {
			sgGroup = (ShippingGroup) shippingGroupitr.next();
			if (sgGroup instanceof HardgoodShippingGroup) {
				shippingType = shippingType.concat(TRUEpslonConstants.HARDGOOD_SHIPP_CODE);

			}
			if (sgGroup instanceof InStorePickupShippingGroup) {
				if(getWarehousePickupFlagPropertyName() != null){
					isSOS = false;
							//(Boolean) ((InStorePickupShippingGroup) sgGroup).getPropertyValue(getWarehousePickupFlagPropertyName());
				}
        	
        		if(isSOS){
        		shippingType = shippingType.concat(TRUEpslonConstants.SHIP_TO_STORE_CODE);
        		}else{
				shippingType = shippingType.concat(TRUEpslonConstants.INSTORE_SHIP_CODE);
        		}
			}
		}
		if (shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE)
				&& shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE) && shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)) {
			messageID = getShipToHomeShipToStoreAndInStorePickup();
			
		}
		else if (shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE)
					&& shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE)) {
				messageID = getShipToHomeAndIsPickUp();
			}
		else if (shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE)
					&& shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)) {
				messageID = getShipToHomeAndShipToStore();
			}
		else if (shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE)
					&& shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)) {
				messageID =getShipToStoreAndInStorePickup();
			}
		else{
			if (shippingType.contains(TRUEpslonConstants.HARDGOOD_SHIPP_CODE)) {
				messageID = getShipToHome();
			}
			if (shippingType.contains(TRUEpslonConstants.INSTORE_SHIP_CODE)) {
				messageID = getIsPickUp();
			}
			if (shippingType.contains(TRUEpslonConstants.SHIP_TO_STORE_CODE)) {
				messageID = getShipToStore();
			}
		}
		if (isLoggingDebug()) {
			logDebug("Message Type:::" + messageID);
		}
		if (isLoggingDebug()) {
			logDebug("EXIT:::TRUEpslonOrderAcknowledgeService getMessageID()");
		}
		return messageID;
	}
   /**
	 * This method sets the BPP item.
	 * @param pItemRel -CommerceItem
	 * @param pShippingGroup -ShippingGroup
	 * @param pItemIndex -int
	 * @param pEpslonMessageBean -TRUEpslonMessageBean
	 * @return bppItemType LineItemType
	 */
	public LineItemType getBppItem(CommerceItemRelationship pItemRel,ShippingGroup pShippingGroup,int pItemIndex,TRUEpslonMessageBean pEpslonMessageBean) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonOrderAcknowledgeService getBppItem()");
		}
		
		String relationShipId = TRUEpslonConstants.EMPTY_STRING;
		String shippingMethod = TRUEpslonConstants.EMPTY_STRING;
		String bppSkuId = TRUEpslonConstants.EMPTY_STRING;
		BigDecimal itemPrice = BigDecimal.ZERO;
		TRUEpslonBppInfoVO epslonBppInfoVO = null;
		long itemQty = 0;
		if(pItemRel != null){
		
			relationShipId = pItemRel.getId();
			itemQty = pItemRel.getQuantity();  
		}
		
		ObjectFactory factory = new ObjectFactory();
		LineItemType bppItemType = factory.createLineItemType();
   	   if(pEpslonMessageBean != null && !pEpslonMessageBean.getMessageBeanBppItemMap().isEmpty()){
		epslonBppInfoVO = pEpslonMessageBean.getMessageBeanBppItemMap().get(relationShipId);
		}
   	   if(pShippingGroup != null){
   			shippingMethod = getShippingMethod(pShippingGroup);
   		}
   	  if(epslonBppInfoVO != null){
   		bppSkuId = epslonBppInfoVO.getBppSkuID();
   		String stringItemPrice = Double.toString(epslonBppInfoVO.getBppItemPrice());
   		itemPrice = new BigDecimal(stringItemPrice); 
   		itemPrice = itemPrice.setScale(TRUEpslonConstants.TWO, BigDecimal.ROUND_CEILING);
   		
   	  }
		
       BigDecimal itemQuantity = new  BigDecimal(itemQty);
       bppItemType.setItemQuantity(itemQuantity);
       bppItemType.setItemNumber(bppSkuId);
       bppItemType.setItemPrice(itemPrice);
       bppItemType.setShippingMethod(shippingMethod);
       bppItemType.setItemDescription(TRUEpslonConstants.BPP_ITEM_DEFAULT_NAME);
       bppItemType.setLineNumber(pItemIndex);
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonOrderAcknowledgeService getBppItem()");
		}
		return bppItemType;
	}
	/**
	 * This method formats the String if there is some special character.
	 * @param pItemName -String
	 * @return itemName String
	 */
	public String formattedItemName(String pItemName){
		String itemName = TRUEpslonConstants.EMPTY_STRING;
		if(!StringUtils.isBlank(pItemName)){
			itemName = pItemName;
			}
	    itemName = itemName.replaceAll(TRUEpslonConstants.CHAR_PATTERN, TRUEpslonConstants.SPACE_STRING);
	    return itemName;
	}	
	
}
