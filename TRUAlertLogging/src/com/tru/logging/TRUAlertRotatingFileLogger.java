package com.tru.logging;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.nucleus.logging.LogEvent;
import atg.nucleus.logging.RotatingFileLogger;

/**
 * Extends the RotatingFileLogger to write out TRUSAlertEvent objects to the Alert Format.
 *
 * @author PA
 * @version 1.0
 */
public class TRUAlertRotatingFileLogger extends RotatingFileLogger {
	
	//${dateTime}|${host}|${systemName}|${severityLevel}|${processName}|${taskName}|${status}|${FileName}|${AdditionalNotes}
	
	/** The m host name. */
	private String mHostName;
	
	/** The m system name. */
	private String mSystemName;
	
	/** The m date format. */
	private String mDateFormat;
	
	/** The m alert header format. */
	private String mAlertHeaderFormat;
	
	/** The m date formatter. */
	private SimpleDateFormat mDateFormatter;

	/**
	 * Write log event.
	 *
	 * @param pLogEvent :LogEvent
	 */
	@Override
	public synchronized void writeLogEvent(LogEvent pLogEvent) {
		TRUAlertEvent event = null;
		
		if(!(pLogEvent instanceof TRUAlertEvent)){
			if(isLoggingError()){
				logError("TRU AlertLogger was passed a non-Alert event.");
			}
			return;
		}else{
			event = (TRUAlertEvent)pLogEvent;
		}
	      
		String alertHeader = generateAlertHeader(event);
		String alertExtenstions = generateAlertExtenstions(event);
		String alertString = new StringBuffer().append(alertHeader).append(alertExtenstions).toString();
		getLogStream().println(alertString);
		//getLogStream().println();

	}
	
	/**
	 * Generate alert extenstions.
	 *
	 * @param pLogEvent :SETAEvent
	 * @return buffer the String
	 */
	private String generateAlertExtenstions(TRUAlertEvent pLogEvent){

		StringBuffer buffer = new StringBuffer();
		String fileName = pLogEvent.getFileName();
		if ((fileName != null) && ((fileName.length() != 0) || (fileName.trim().length() != 0))) {
			buffer.append(fileName).append(TRUAlertConstants.DIVIDER);
		}
		buffer.append(TRUAlertConstants.SQUAREBRACKET_LEFT);
		for(String key : pLogEvent.getExtensionMap().keySet()){
			buffer.append(key);
			buffer.append(TRUAlertConstants.EQUAL_SYMBOL);
			Object v = pLogEvent.getExtensionMap().get(key);
			String vs = TRUAlertConstants.TRIPLE_HASH_SYMBOL;
			if(v!=null){
				vs=v.toString();
			}
			buffer.append(vs);
			buffer.append(TRUAlertConstants.COMMA_STRING);
		}
		buffer.append(TRUAlertConstants.SQUAREBRACKET_RIGHT);
		return buffer.toString();
		
	}
	
	/**
	 * Generate alert header.
	 *
	 * @param pLogEvent :SETAEvent
	 * @return buffer the String
	 */
	private String generateAlertHeader(TRUAlertEvent pLogEvent){
		Pattern alertHeaderPattern=Pattern.compile(TRUAlertConstants.PATTERN_COMPILE);
		Matcher alertHeaderMatcher = alertHeaderPattern.matcher(mAlertHeaderFormat);

		StringBuffer buffer = new StringBuffer();
		HashMap<String,String> templateValues = new HashMap<String, String>();
		templateValues.put(TRUAlertConstants.DATE_TIME, mDateFormatter.format(new Date(pLogEvent.getTimeStamp())));
		templateValues.put(TRUAlertConstants.HOST, getHostName());
		templateValues.put(TRUAlertConstants.SYSTEMNAME, getSystemName());
		templateValues.put(TRUAlertConstants.SEVERITY_LEVEL, pLogEvent.getSeverity());
		templateValues.put(TRUAlertConstants.PROCESS_NAME, pLogEvent.getProcessName() );
		templateValues.put(TRUAlertConstants.TASKENAME, pLogEvent.getTaskName());
		templateValues.put(TRUAlertConstants.STATUS, pLogEvent.getStatus());
		
		while(alertHeaderMatcher.find()){
			if(templateValues.containsKey(alertHeaderMatcher.group(TRUAlertConstants.NUMERIC_ONE))){
				String sub = templateValues.get(alertHeaderMatcher.group(TRUAlertConstants.NUMERIC_ONE));
				alertHeaderMatcher.appendReplacement(buffer, sub!=null ? Matcher.quoteReplacement(sub) : TRUAlertConstants.NULL);
			}
		}
		alertHeaderMatcher.appendTail(buffer);
		return buffer.toString();
	}
	
	/**
	 * Gets the host name.
	 *
	 * @return the host name
	 */
	public String getHostName() {
		return mHostName;
	}

	/**
	 * Sets the host name.
	 *
	 * @param pHostName the new host name
	 */
	public void setHostName(String pHostName) {
		this.mHostName = pHostName;
	}

	/**
	 * Gets the system name.
	 *
	 * @return the system name
	 */
	public String getSystemName() {
		return mSystemName;
	}

	/**
	 * Sets the system name.
	 *
	 * @param pSystemName the new system name
	 */
	public void setSystemName(String pSystemName) {
		this.mSystemName = pSystemName;
	}

	/**
	 * Gets the date format.
	 *
	 * @return the date format
	 */
	public String getDateFormat() {
		return mDateFormat;
	}

	/**
	 * Sets the date format.
	 *
	 * @param pDateFormat the new date format
	 */
	public void setDateFormat(String pDateFormat) {
		this.mDateFormat = pDateFormat;
		this.mDateFormatter = new SimpleDateFormat(pDateFormat);
	}

	/**
	 * Gets the alert header format.
	 *
	 * @return the mAlertHeaderFormat
	 */
	public String getAlertHeaderFormat() {
		return mAlertHeaderFormat;
	}

	/**
	 * Sets the alert header format.
	 *
	 * @param pAlertHeaderFormat the mAlertHeaderFormat to set
	 */
	public void setAlertHeaderFormat(String pAlertHeaderFormat) {
		this.mAlertHeaderFormat = pAlertHeaderFormat;
	}
	
}
