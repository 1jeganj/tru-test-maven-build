package com.tru.logging;

import java.util.Map;

import atg.nucleus.GenericService;

/**
 * This class is used to log CEF logging.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUAlertLogger extends GenericService{

	/** Flag if logging info messages */
	private boolean mLoggingAlert;



	/**
	 * Log failure.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pExtenstions the extenstions
	 *//*
	public void logSuccess(String pProcessName, String pTask,Map<String, String> pExtenstions){

		if (isLoggingAlert()) {
			logAlertEvent(pProcessName, pTask, "SUCCESS","INFO",null,pExtenstions);
		}
	}*/

	/**
	 * 
	 * @param pProcessName pProcessName
	 * @param pTask pTask
	 * @param pExtenstions pExtenstions
	 */
	public void logFailure(String pProcessName, String pTask, Map<String, String> pExtenstions){
		if (isLoggingAlert()) {
			logAlertEvent(pProcessName, pTask, TRUAlertConstants.FAILED,TRUAlertConstants.ERROR,null,pExtenstions);
		}
	}


	/**
	 * Log feed success.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pFileName the file name
	 * @param pExtenstions the extenstions
	 */
	public void logFeedSuccess(String pProcessName, String pTask, String pFileName, Map<String, String> pExtenstions){

		if (isLoggingAlert()) {
			logAlertEvent(pProcessName, pTask,TRUAlertConstants.SUCCESS,TRUAlertConstants.INFO, pFileName, pExtenstions);
		}
	}

	/**
	 * Log feed faileds.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pFileName the file name
	 * @param pExtenstions the extenstions
	 */
	public void logFeedFaileds(String pProcessName, String pTask, String pFileName, Map<String, String> pExtenstions){

		if (isLoggingAlert()) {
			logAlertEvent(pProcessName, pTask,TRUAlertConstants.FAILED,TRUAlertConstants.ERROR, pFileName, pExtenstions);
		}
	}


	/**
	 * Log alert event.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pStatus the status
	 * @param pSeverity the severity
	 * @param pFileName the file name
	 * @param pExtenstions the extenstions
	 */
	private void logAlertEvent(String pProcessName, String pTask, String pStatus, String pSeverity, String pFileName, Map<String, String> pExtenstions) {

		TRUAlertEvent event = null;

		if (((pFileName == null) || (pFileName.length() == 0) || (pFileName.trim().length() == 0))) {
			event =  new TRUAlertEvent(pProcessName,pTask,pStatus,pSeverity,pExtenstions);
		} else {
			event =  new TRUAlertEvent(pProcessName,pTask,pStatus,pSeverity,pFileName,pExtenstions);
		}
		sendLogEvent(event);
	}



	/**
	 * @return the mLoggingAlert
	 */
	public boolean isLoggingAlert() {
		return mLoggingAlert;
	}



	/**
	 * @param pLoggingAlert the mLoggingAlert to set
	 */
	public void setLoggingAlert(boolean pLoggingAlert) {
		this.mLoggingAlert = pLoggingAlert;
	}


}
