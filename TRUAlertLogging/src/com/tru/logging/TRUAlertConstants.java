package com.tru.logging;
/**
 * 
 * 
 * @author PA
 * @version 1.0
 */
public class TRUAlertConstants {

	/**
	 * Constant to hold EQUAL_SYMBOL.
	 */
	public static final String EQUAL_SYMBOL = "=";
	/**
	 * Constant to hold TRIPLE_HASH_SYMBOL.
	 */
	public static final String TRIPLE_HASH_SYMBOL = "---";
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String EMPTY_STRING= " ";
	
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String COMMA_STRING= ",";
	
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String PATTERN_COMPILE= "\\$\\{(.+?)\\}";
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String DATE_TIME= "dateTime";
	/**
	 * Constant to hold HOST.
	 */		
	public static final String HOST="host";
	
	/**
	 * Constant to hold NUMERIC_ONE.
	 */
	public static final int NUMERIC_ONE = 1;
	
	/**
	 * Constant to hold NULL.
	 */
	public static final String NULL="null";
	
	/**
	 * Constant to hold NULL.
	 */
	public static final String ALERT_LOG_IDENTIFIER="***Alert Logging**";
	
	/** The Constant STR_COLON. */
	public static final String STR_COLON = ":";
	
	/** The Constant FAILED. */
	public static final String FAILED = "FAILED";
	
	/** The Constant ERROR. */
	public static final String ERROR ="ERROR";
	
	/** The Constant SUCCESS. */
	public static final String SUCCESS ="SUCCESS";
	
	/** The Constant INFO. */
	public static final String INFO = "INFO";
	
	/** The Constant SQUAREBRACKET_LEFT. */
	public static final String SQUAREBRACKET_LEFT = "[";
	
	/** The Constant SQUAREBRACKET_RIGHT. */
	public static final String SQUAREBRACKET_RIGHT = "]";
	
	/** The Constant SYSTEMNAME. */
	public static final String SYSTEMNAME = "systemName";
	
	/** The Constant SEVERITY_LEVEL. */
	public static final String SEVERITY_LEVEL = "severityLevel";
	
	/** The Constant PROCESS_NAME. */
	public static final String PROCESS_NAME = "processName";
	
	/** The Constant TASKENAME. */
	public static final String TASKENAME = "taskName";
	
	/** The Constant STATUS. */
	public static final String STATUS = "status";
	
	/** The Constant DIVIDER. */
	public static final String DIVIDER = "|";
	
	
}
