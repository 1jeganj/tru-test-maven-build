package com.tru.logging;

import java.sql.Date;
import java.text.SimpleDateFormat;

import atg.nucleus.logging.LogEvent;
import atg.nucleus.logging.RotatingFileLogger;

/**
 * Extends the RotatingFileLogger to write out TRUIntegrationEvent objects.
 *
 * @author PA
 * @version 1.0
 */
public class TRUIntegrationRotatingFileLogger extends RotatingFileLogger{

	private SimpleDateFormat mDateFormatter;
	
	private String mDateFormat;
	
	private String mIntegrationHeaderFormat;
	
	/**
	 * Gets the dateFormat.
	 * 
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return mDateFormat;
	}

	/**
	 * Sets the dateFormat.
	 * 
	 * @param pDateFormat the dateFormat to set
	 */
	public void setDateFormat(String pDateFormat) {
		this.mDateFormat = pDateFormat;
		this.mDateFormatter = new SimpleDateFormat(pDateFormat);
	}
	
	
	/**
	 * @return the mIntegrationHeaderFormat
	 */
	public String getIntegrationHeaderFormat() {
		return mIntegrationHeaderFormat;
	}

	/**
	 * @param pIntegrationHeaderFormat the mIntegrationHeaderFormat to set
	 */
	public void setIntegrationHeaderFormat(String pIntegrationHeaderFormat) {
		mIntegrationHeaderFormat = pIntegrationHeaderFormat;
	}

	/**
	 * Write log event.
	 *
	 * @param pLogEvent :LogEvent
	 */
	@Override
	public synchronized void writeLogEvent(LogEvent pLogEvent) {
		TRUIntegrationEvent event = null;
		
		if(!(pLogEvent instanceof TRUIntegrationEvent)){
			if(isLoggingError()){
				logError("TRUIntegrationEvent was passed a non-integration event.");
			}
			return;
		}else{
			event = (TRUIntegrationEvent)pLogEvent;
		}
	      
		String integrationHeader = generateIntegrationInfoHeader(event);
		String integrationExtenstions = generateIntegrationInfoExtenstions(event);
		getLogStream().print(integrationHeader+integrationExtenstions);
		getLogStream().println();

	}
	
	/**
	 * Generate integration extenstions.
	 *
	 * @param pEvent :TRUIntegrationEvent
	 * @return buffer the String
	 */
	private String generateIntegrationInfoExtenstions(TRUIntegrationEvent pEvent) {
		StringBuffer buffer = new StringBuffer();
			if(!pEvent.getExtensionValues().isEmpty()){
			for(String key : pEvent.getExtensionValues().keySet()){
				buffer.append(TRUAlertConstants.EMPTY_STRING);
				buffer.append(key);
				buffer.append(TRUAlertConstants.EQUAL_SYMBOL);
				Object v = pEvent.getExtensionValues().get(key);
				String vs = TRUAlertConstants.TRIPLE_HASH_SYMBOL;
				if(v!=null){
					vs=v.toString();
				}
				buffer.append(vs);
			}
		}
		return buffer.toString();
	}
	
	/**
	 * Generate integration header.
	 *
	 * @param pEvent :TRUIntegrationEvent
	 * @return buffer the String
	 */
	private String generateIntegrationInfoHeader(TRUIntegrationEvent pEvent) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(mDateFormatter.format(new Date(pEvent.getTimeStamp())) + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getThreadId() + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getHostName() + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getServiceName() + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getInterfaceName() + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getStatus() + TRUAlertConstants.DIVIDER);
		buffer.append(pEvent.getOrderId() + TRUAlertConstants.DIVIDER);
		return buffer.toString();
	}

}
