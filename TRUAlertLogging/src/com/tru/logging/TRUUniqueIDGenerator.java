package com.tru.logging;

import atg.nucleus.GenericService;
import atg.service.idgen.IdGeneratorException;
import atg.service.idgen.SQLIdGenerator;

/**
 * TRUUniqueIDGenerator is used populate the UID.
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUUniqueIDGenerator extends GenericService {

	/**
     * Holds component for IDGenerator.
     */
    private SQLIdGenerator mIdGenerator;
    
    /**
     * Holds idspaces space name for integartion.
     */
    private String mIntegrationIdspaceName;
    /**
     * @return the idGenerator
     */
    public SQLIdGenerator getIdGenerator() {
        return mIdGenerator;
    }


    /**
     * @param pIdGenerator the pIdGenerator to set
     */
    public void setIdGenerator(SQLIdGenerator pIdGenerator) {
        mIdGenerator = pIdGenerator;
    }
	
	/**
	 * Method to get Unique integration ID.
	 * 
	 * @return l_lIntegrationId long
	 */
	public long getIntegrationId() {
		long integrationId = 0;
		try {
			integrationId = getIdGenerator().generateLongId();
		} catch (IdGeneratorException e) {
			if (isLoggingError()) {
				logError("Unable to get UniqueIntegrationId from IdGenerator.", e);
			}
		}
			
		integrationId = truncateId(integrationId);
		
		return integrationId;
	}

	/**
	 * Method to get truncated Id.
	 * 
	 * @param pIntegrationId
	 *            long
	 * @return integrationID long
	 */
	private long truncateId(long pIntegrationId) {
		long integrationID = 0;
		String integrationIdString = String.valueOf(pIntegrationId);
		integrationIdString = integrationIdString.trim();
		//integrationIdString = integrationIdString.substring(integrationIdString.length());
		integrationID = Long.parseLong(integrationIdString);
		if (integrationID == 0) {
			integrationID = getIntegrationId();
		}
		return integrationID;
	}


	/**
	 * @return the mIntegrationIdspaceName
	 */
	public String getIntegrationIdspaceName() {
		return mIntegrationIdspaceName;
	}


	/**
	 * @param pIntegrationIdspaceName the mIntegrationIdspaceName to set
	 */
	public void setIntegrationIdspaceName(String pIntegrationIdspaceName) {
		mIntegrationIdspaceName = pIntegrationIdspaceName;
	}
}
