package com.tru.logging;

/**
 * TRUIntegrationConstant.
 *
 * @author PA
 * @version 1.0
 */
public class TRUIntegrationConstant {
	
	/** The Constant STATUS_ENTERED. */
	public static final String STATUS_ENTERED = "Entered";
	
	/** The Constant STATUS_EXITED. */
	public static final String STATUS_EXITED = "Exited";
	
	/** The Constant UNIQUE_ID. */
	public static final String THREAD_ID = "threadId";
	
	/** The Constant SERVICE_NAME. */
	public static final String SERVICE_NAME = "serviceName";
	
	/** The Constant INTERFACE_NAME. */
	public static final String INTERFACE_NAME = "interfaceName";
	
	/** The Constant STATUS. */
	public static final String STATUS = "status";
	
	/** The Constant USER_ID. */
	public static final String USER_ID = "userId";
	
	/** The Constant ORDER_ID. */
	public static final String ORDER_ID = "orderId";
	
	/** The Constant PROFILE. */
	public static final String PROFILE = "profile";
	
	/** The Constant RADIAL_PAYMENT. */
	public static final String RADIAL_PAYMENT = "radialPayment";
	
	/** The Constant PROFILE. */
	public static final String TOKEN = "token";
	
	/** The Constant ENTRY. */
	public static final String ENTRY = "entry";
	

}
