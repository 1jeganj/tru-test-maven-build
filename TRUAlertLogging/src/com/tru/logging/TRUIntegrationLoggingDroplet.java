package com.tru.logging;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUIntegrationLoggingDroplet extends DynamoServlet{
	
	private static final String OUTPUT = "output";
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * This service method is to call loggerservice for integration
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws IOException  IOException
	 * @throws ServletException ServletException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,	DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUIntegrationLoggingDroplet method : service");
		}
		
		String orderId = (String) pRequest.getLocalParameter(TRUIntegrationConstant.ORDER_ID);
		final Profile profile =(Profile) pRequest.getObjectParameter(TRUIntegrationConstant.PROFILE);
		String entry = (String) pRequest.getLocalParameter(TRUIntegrationConstant.ENTRY);
		String interfaceName = (String) pRequest.getLocalParameter(TRUIntegrationConstant.INTERFACE_NAME);
		
		if(Boolean.valueOf(entry)){
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(TRUIntegrationConstant.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(interfaceName), orderId, profile.getRepositoryId());
		}else{
			getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(TRUIntegrationConstant.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(interfaceName), orderId, profile.getRepositoryId());
		}
		
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
		
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUIntegrationLoggingDroplet method : service");
		}
	}
}
