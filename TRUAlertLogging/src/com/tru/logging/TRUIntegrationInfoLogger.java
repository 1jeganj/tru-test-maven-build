package com.tru.logging;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;
/**
 * This class is used to log Integration logging.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUIntegrationInfoLogger extends GenericService{

	/** Flag if logging info messages */
	private boolean mLoggingIntegration;
	
	/** Property to hold mServiceNameMap *. */
	private Map<String,String> mServiceNameMap;
	
	/** Property to hold mInterfaceNameMap *. */
	private Map<String,String> mInterfaceNameMap;
	
	/** Property to hold mEnableThreadNaming *. */
	private boolean mEnableThreadNaming;
	
	/**
	 * @return the mServiceNameMap
	 */
	public Map<String,String> getServiceNameMap() {
		return mServiceNameMap;
	}

	/**
	 * @param pServiceNameMap the mServiceNameMap to set
	 */
	public void setServiceNameMap(Map<String,String> pServiceNameMap) {
		mServiceNameMap = pServiceNameMap;
	}

	/**
	 * @return the mInterfaceNameMap
	 */
	public Map<String,String> getInterfaceNameMap() {
		return mInterfaceNameMap;
	}

	/**
	 * @param pInterfaceNameMap the mInterfaceNameMap to set
	 */
	public void setInterfaceNameMap(Map<String,String> pInterfaceNameMap) {
		mInterfaceNameMap = pInterfaceNameMap;
	}

	/**
	 * Log a entry to third party call.
	 * @param pServiceName serviceName
	 * @param pInterfaceName InterfaceName
	 * @param pOrderId pOrderId
	 * @param pUserId pUserId
	 */
	public void logIntegrationEntered(String pServiceName, String pInterfaceName,String pOrderId, String pUserId){
		if(isLoggingIntegration()){
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String status = TRUIntegrationConstant.STATUS_ENTERED;
			logIntergrationEvent(pServiceName,pInterfaceName,status,pOrderId,pUserId,extenstions);
		}
	}
	
	/**
	 * Log a exit from  third party call.
	 * @param pServiceName serviceName
	 * @param pInterfaceName InterfaceName
	 * @param pOrderId pOrderId
	 * @param pUserId pUserId
	 */
	public void logIntegrationExited(String pServiceName, String pInterfaceName,String pOrderId, String pUserId){
		if(isLoggingIntegration()){
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String status = TRUIntegrationConstant.STATUS_EXITED;
			logIntergrationEvent(pServiceName,pInterfaceName,status,pOrderId,pUserId,extenstions);
		}
	}
	
	
	/**
	 * Log a successful/Unsuccessful third party call.
	 * @param pServiceName serviceName
	 * @param pInterfaceName InterfaceName
	 * @param pStatus status
	 * @param pOrderId pOrderId
	 * @param pUserId pUserId
	 * @param pExtensionValues extensionvalues
	 */
	private void logIntergrationEvent(String pServiceName, String pInterfaceName, String pStatus,String pOrderId,String pUserId,Map<String, String> pExtensionValues) {
		if(pUserId != null && !isEnableThreadNaming()){
			pExtensionValues.put(TRUIntegrationConstant.USER_ID, pUserId);
		}
		TRUIntegrationEvent integrationEvent = new TRUIntegrationEvent(pServiceName,pInterfaceName,pStatus,pOrderId,pExtensionValues);
		sendLogEvent(integrationEvent);
	}

	/**
	 * @return the mLoggingIntegration
	 */
	public boolean isLoggingIntegration() {
		return mLoggingIntegration;
	}

	/**
	 * @param pLoggingIntegration the mLoggingIntegration to set
	 */
	public void setLoggingIntegration(boolean pLoggingIntegration) {
		this.mLoggingIntegration = pLoggingIntegration;
	}

	/**
	 * @return the mEnableThreadNaming
	 */
	public boolean isEnableThreadNaming() {
		return mEnableThreadNaming;
	}

	/**
	 * @param pEnableThreadNaming the mEnableThreadNaming to set
	 */
	public void setEnableThreadNaming(boolean pEnableThreadNaming) {
		mEnableThreadNaming = pEnableThreadNaming;
	}

}
