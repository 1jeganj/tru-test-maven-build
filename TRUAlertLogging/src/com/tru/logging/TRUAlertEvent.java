package com.tru.logging;

import java.util.Calendar;
import java.util.Map;

import atg.nucleus.logging.LogEvent;
/**
 * 
 * 
 * @author PA
 * @version 1.0
 */
public class TRUAlertEvent extends LogEvent {

	/**
	 * property to hold time stamp.
	 */
	private long mTimestamp;
	/**
	 * property to hold event Type.
	 */
	private String mProcessName;
	/**
	 * property to hold event Name.
	 */
	private String mTaskName;
	/**
	 * property to hold severity.
	 */
	private String mSeverity;
	/**
	 * property to hold extensionMap.
	 */
	private Map<String, String> mExtensionMap;
	
	/** The m status. */
	private String mStatus;
	
	/** The m file name. */
	private String mFileName;
	
	/**
	 * Gets the Time stamp
	 * 
	 * @return the mTimestamp
	 */
	public long getTimeStamp(){
		return this.mTimestamp;
	}
	
	/**
	 * Gets the Severity
	 * 
	 * @return the mSeverity
	 */
	public String getSeverity(){
		return this.mSeverity;
	}
	/**
	 * Gets the ExtensionMap
	 * 
	 * @return the mExtensionMap
	 */
	public Map<String, String> getExtensionMap(){
		return this.mExtensionMap;
	}

	/**
	 * Instantiates a new TRU alert event.
	 *
	 * @param pProcessName the process name
	 * @param pTaskName the task name
	 * @param pStatus the status
	 * @param pSeverity Severity
	 * @param pExtensionMap ExtensionMap
	 */
	public TRUAlertEvent(String pProcessName,String pTaskName,String pStatus,String pSeverity,Map<String,String> pExtensionMap) {
		super(new StringBuffer().append(pProcessName).append(TRUAlertConstants.STR_COLON).append(pTaskName).toString()); 
		mTimestamp = Calendar.getInstance().getTimeInMillis();
		this.setProcessName(pProcessName);
		this.setTaskName(pTaskName);
		this.setStatus(pStatus);
		this.mSeverity = pSeverity;
		this.mExtensionMap = pExtensionMap;
	}
	
	
	/**
	 * Instantiates a new TRU alert event.
	 *
	 * @param pProcessName the process name
	 * @param pTaskName the task name
	 * @param pStatus the status
	 * @param pSeverity Severity
	 * @param pFileName the file name
	 * @param pExtensionMap ExtensionMap
	 */
	public TRUAlertEvent(String pProcessName,String pTaskName,String pStatus,String pSeverity,String pFileName, Map<String,String> pExtensionMap) {
		super(new StringBuffer().append(pProcessName).append(TRUAlertConstants.STR_COLON).append(pTaskName).toString()); 
		mTimestamp = Calendar.getInstance().getTimeInMillis();
		this.setProcessName(pProcessName);
		this.setTaskName(pTaskName);
		this.setStatus(pStatus);
		this.setFileName(pFileName);
		this.mSeverity = pSeverity;
		this.mExtensionMap = pExtensionMap;
	}

	/**
	 * @return SETAlog
	 */
	public String getIdentifier()
	{
		return TRUAlertConstants.ALERT_LOG_IDENTIFIER;
	}

	/**
	 * @return the mTaskName
	 */
	public String getTaskName() {
		return mTaskName;
	}

	/**
	 * Sets the task name.
	 *
	 * @param pTaskName the new task name
	 */
	public void setTaskName(String pTaskName) {
		this.mTaskName = pTaskName;
	}

	/**
	 * @return the mProcessName
	 */
	public String getProcessName() {
		return mProcessName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param pProcessName the new process name
	 */
	public void setProcessName(String pProcessName) {
		this.mProcessName = pProcessName;
	}

	/**
	 * @return the mStatus
	 */
	public String getStatus() {
		return mStatus;
	}

	/**
	 * Sets the status.
	 *
	 * @param pStatus the new status
	 */
	public void setStatus(String pStatus) {
		this.mStatus = pStatus;
	}

	/**
	 * @return the mFileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the new file name
	 */
	public void setFileName(String pFileName) {
		this.mFileName = pFileName;
	}

}
