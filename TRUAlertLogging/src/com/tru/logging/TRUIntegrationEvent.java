package com.tru.logging;

import java.util.Map;

import atg.nucleus.logging.LogEvent;
import atg.servlet.ServletUtil;

/**
 * TRUIntegrationEvent.
 *
 * @author PA
 * @version 1.0
 */
public class TRUIntegrationEvent extends LogEvent{
	
	private String mThreadId;
	
	private long mTimeStamp;
	
	private String mHostName;
	
	private String mServiceName;
	
	private String mInterfaceName;
	
	private String mStatus;
	
	private String mOrderId;
	
	private Map<String, String> mExtensionValues;
	
	/**
	 * Instantiates a new TRUIntegrationEvent.
	 *
	 * @param pServiceName servicename
	 * @param pInterfaceName interfaceName
	 * @param pStatus the status
	 * @param pOrderId ordeid
	 * @param pExtensionValues ExtensionMap
	 */
	public TRUIntegrationEvent(String pServiceName, String pInterfaceName, String pStatus,String pOrderId,Map<String, String> pExtensionValues) {
		super(pServiceName);
		
		mTimeStamp = System.currentTimeMillis();
		this.mHostName = ServletUtil.getCurrentRequest().getServerName();
		this.mServiceName = pServiceName;
		this.mInterfaceName = pInterfaceName;
		this.mStatus = pStatus;
		this.mOrderId = pOrderId;
		this.mThreadId= Thread.currentThread().getName();
		this.mExtensionValues = pExtensionValues;
	}

	/**
	 * @return the mTimeStamp
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * @param pTimeStamp the mTimeStamp to set
	 */
	public void setTimeStamp(long pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}

	/**
	 * @return the mHostName
	 */
	public String getHostName() {
		return mHostName;
	}

	/**
	 * @param pHostName the mHostName to set
	 */
	public void setHostName(String pHostName) {
		mHostName = pHostName;
	}

	/**
	 * @return the mServiceName
	 */
	public String getServiceName() {
		return mServiceName;
	}

	/**
	 * @param pServiceName the mServiceName to set
	 */
	public void setServiceName(String pServiceName) {
		mServiceName = pServiceName;
	}

	/**
	 * @return the mInterfaceName
	 */
	public String getInterfaceName() {
		return mInterfaceName;
	}

	/**
	 * @param pInterfaceName the mInterfaceName to set
	 */
	public void setInterfaceName(String pInterfaceName) {
		mInterfaceName = pInterfaceName;
	}

	/**
	 * @return the mStatus
	 */
	public String getStatus() {
		return mStatus;
	}

	/**
	 * @param pStatus the mStatus to set
	 */
	public void setStatus(String pStatus) {
		mStatus = pStatus;
	}

	/**
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId the mOrderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * @return the mExtensionValues
	 */
	public Map<String, String> getExtensionValues() {
		return mExtensionValues;
	}

	/**
	 * @param pExtensionValues the mExtensionValues to set
	 */
	public void setExtensionValues(Map<String, String> pExtensionValues) {
		mExtensionValues = pExtensionValues;
	}

	/**
	 * @return the mThreadId
	 */
	public String getThreadId() {
		return mThreadId;
	}

}
