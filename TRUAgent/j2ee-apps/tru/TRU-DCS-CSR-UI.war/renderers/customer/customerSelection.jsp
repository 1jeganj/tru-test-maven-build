<%--
 This page defines the Customer Selection Popup
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/customer/customerSelection.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
  <dsp:importbean var="agentUIConfig" bean="/atg/svc/agent/ui/AgentUIConfiguration"/>
    <csr:renderer name="/atg/commerce/custsvc/ui/renderers/CustomerSearch">
      <jsp:attribute name="setPageData">
      </jsp:attribute>
    <jsp:body>
      <dsp:include src="${renderInfo.url}" otherContext="${agentUIConfig.truContextRoot}"/>
    </jsp:body>
    </csr:renderer>
    <csr:renderer name="/atg/commerce/custsvc/ui/renderers/CustomerSearchResults">
      <jsp:attribute name="setPageData">
      </jsp:attribute>
      <jsp:body>
        <dsp:include src="${renderInfo.url}" otherContext="${agentUIConfig.truContextRoot}">
          <dsp:param name="selectLinkPanelStack" value="['globalPanels', 'cmcShoppingCartPS']" />
        </dsp:include>
      </jsp:body>
    </csr:renderer>
    <div align="right">
      <input type="button" onclick="atg.commerce.csr.common.hidePopupWithReturn('atg_commerce_csr_catalog_customerSelectionPopup')" value="<fmt:message key='cart.customerSelection.close'/>"/>
    </div>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/customer/customerSelection.jsp#1 $$Change: 875535 $--%>
