<%--
 A page fragment that displays product image, name, ID, description
 and price range. Used on the ProductView panel.

 This page requires either a productId or a commerceItemId parameter

 @param productId - The ID of the product to display
 @param siteId - The ID of the site
 @param commerceItemId - The commerce item ID of the item in the current order
 @param hidePrice - parameter to decide whether to display price or not.
                    if this parameter is provided and it is true, then price 
                    will not be displayed. 

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/productInformation.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:importbean bean="/atg/userprofiling/Profile" var="profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/multisite/GetSiteDroplet" />
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
      <dsp:getvalueof var="productId" param="productId"/>
      <dsp:getvalueof var="commerceItemId" param="commerceItemId"/>
      <dsp:getvalueof var="hidePrice" param="hidePrice"/>
      <dsp:getvalueof var="currentSiteId" param="currentSiteId"/>
      <dsp:getvalueof var="site" param="site"/>
      <style type="text/css">
      	#atg_commerce_csr_catalog_product_info_image{
      	width: 130px;
    	height: 130px;
    	}
    	#learnMoreDDDiv{
		    	width: 100%;
			    float: left;
			    margin-top: 10px;
			    display: none;
				line-height: 20px;
		    }
		    #learnMoreDDDiv a{
		        float: left;
		    }

    	.shopping-cart-surprise-image-div{
	    		background-image: url("/TRU-DCS-CSR/images/dont-spoil.png");
			    background-repeat: no-repeat;
			    float: left;
			    width: 115px;
			    padding-left: 25px;
			    height: 20px;
		    }
		    #atg_commerce_csr_catalog_spoilDetailPopup p{
		    	text-align: justify;
		    }
	      </style>	    
		    
    <dsp:importbean bean="/atg/dynamo/droplet/multisite/SiteContextDroplet"/>
    <c:set var="validSiteId" value="${false}"/>
    <%-- if the siteId is null or site is deleted, then use the empty site context. --%> 
    <c:if test="${not empty siteId}">
     <dsp:droplet name="GetSiteDroplet">
      <dsp:param name="siteId" value="${siteId}" />
      <dsp:oparam name="output">
       <c:set var="validSiteId" value="${true}"/>
      </dsp:oparam>
      </dsp:droplet>    
    </c:if> 
    
    <dsp:droplet name="SiteContextDroplet">
      <dsp:param name="siteId" param="siteId" />
      <dsp:param name="emptySite" value="${!validSiteId}" />
      <dsp:oparam name="output">
        <csr:getProduct productId="${productId}" commerceItemId="${commerceItemId}">
          <div class="atg_commerce_csr_productViev">
            <c:if test="${not empty product}">
              <dsp:tomap var="productMap" value="${product}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_currSkuId" value=""/>
              <%--
              <dsp:img src="${image.url}" id="atg_commerce_csr_catalog_product_info_image"
                iclass="atg_commerce_csr_ProductViewImg" /> 
                --%>
                <!-- 	Added for Image		--> 
              <%--  <dsp:img src="${productMap.childSKUs[0].primaryImage}" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg"/> --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${productMap.childSKUs[0].primaryImage}" />
				<dsp:oparam name="false">
	            	<dsp:img src="${productMap.childSKUs[0].primaryImage}" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg" style="width:210px;"/>
				</dsp:oparam>
				<dsp:oparam name="true">
					<img  src="/TRU-DCS-CSR/images/no-image500.gif" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg"/>
				</dsp:oparam>
			</dsp:droplet>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_image" value="${productMap.childSKUs[0].primaryImage}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_display_name" value="${fn:escapeXml(productMap.displayName)}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_description" value="${fn:escapeXml(productMap.description)}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_repository_id" value="${fn:escapeXml(productMap.repositoryId)}"/>
              <c:if test="${hidePrice != true}">
              <c:set var="productPriceRange">
                <dsp:include src="/include/catalog/displayProductPriceRange.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dsp:param name="productToPrice" value="${product}"/>
                  <dsp:getvalueof var="currentSiteId" value="${currentSiteId}"/>
                  <dsp:getvalueof var="site" value="${site}"/>
                </dsp:include>
              </c:set>
              <div style="display:none" id="atg_commerce_csr_catalog_product_info_product_price">${productPriceRange}</div>
              </c:if>              
              <dl class="atg_commerce_csr_catalog_product_info_layout">
                <dt id="atg_commerce_csr_catalog_product_info_display_name" class="atg_commerce_csr_productVievTitle">
                  ${fn:escapeXml(productMap.displayName)}
                </dt>
                <dd id="atg_commerce_csr_catalog_product_info_repository_id" class="atg_commerce_csr_productVievID">
                  ${fn:escapeXml(productMap.repositoryId)}
                </dd>
                <dd id="atg_commerce_csr_catalog_product_info_description" class="atg_commerce_csr_productVievDesc">
                  ${fn:escapeXml(productMap.description)}
                </dd>
                <c:if test="${hidePrice != true}">
                 <dd id="atg_commerce_csr_catalog_product_info_price" class="atg_commerce_csr_productVievPrice">
                   ${productPriceRange}
                 </dd>
                </c:if>
                
                <dd id="promotionalStickerDisplay" style="display:none;height: 88px;width: 88px;top: 170px;left: 30px;background-color: #004ebc;text-align: center;border-radius: 50px;">
                <p id="promotionalStickercontent" style="color: #fff;position: relative;top: 30px;font-weight: bold;"></p>
                </dd>
                <dd id="esrbRating" style="display:none;width: 100%;margin-top: 7px;">
                </dd>
                <dd id="manufacturerStyleNumber" style="width: 100%;float: left;margin-top: 10px;">
                </dd>
                <dd id="productDescription" style="width: 100%;float: left;margin-top: 10px;">
                </dd>
                <dd id="channelAvailability" style="width: 100%;float: left;margin-top: 10px;">
                </dd>
                <dd id="skuPromoDetails" style="width: 100%;float: left;margin-top: 10px;">
                </dd>
                <dd id="">
                </dd>
                
              </dl>
            </c:if>
            <c:if test="${not empty product.sizeChartName}">
            	<a href="javascript:void(0);" onclick="showDetailsPopup('sizeChart','','${currentSiteId}');" class="sizeChart">size chart</a>
            </c:if>
          </div>
        </csr:getProduct>
      </dsp:oparam>
      
      <dsp:oparam name="error">
        <csr:getProduct productId="${productId}" commerceItemId="${commerceItemId}">
          <div class="atg_commerce_csr_productViev">
            <c:if test="${not empty product}">
              <dsp:tomap var="productMap" value="${product}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_currSkuId" value=""/>
               <%--
              <dsp:img src="${image.url}" id="atg_commerce_csr_catalog_product_info_image"
                iclass="atg_commerce_csr_ProductViewImg" /> 
                --%>
                <!-- 	Added for Image		--> 
               <%-- <dsp:img src="${productMap.childSKUs[0].primaryImage}" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg"/> --%>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${productMap.childSKUs[0].primaryImage}" />
				<dsp:oparam name="false">
	            	<dsp:img src="${productMap.childSKUs[0].primaryImage}" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg"/>
				</dsp:oparam>
				<dsp:oparam name="true">
					<img  src="/TRU-DCS-CSR/images/no-image500.gif" id="atg_commerce_csr_catalog_product_info_image" iclass="atg_commerce_csr_ProductViewImg"/>
				</dsp:oparam>
			</dsp:droplet>

              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_image" value="${productMap.childSKUs[0].primaryImage}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_display_name" value="${fn:escapeXml(productMap.displayName)}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_description" value="${fn:escapeXml(productMap.description)}"/>
              <input type="hidden" id="atg_commerce_csr_catalog_product_info_product_repository_id" value="${fn:escapeXml(productMap.repositoryId)}"/>
              <c:if test="${hidePrice != true}">
              <c:set var="productPriceRange">
                <dsp:include src="/include/catalog/displayProductPriceRange.jsp" otherContext="${CSRConfigurator.contextRoot}">
                  <dsp:param name="productToPrice" value="${product}"/>
                  <dsp:getvalueof var="currentSiteId" value="${currentSiteId}"/>
                  <dsp:getvalueof var="site" value="${site}"/>
                </dsp:include>
              </c:set>
              <div style="display:none" id="atg_commerce_csr_catalog_product_info_product_price">${productPriceRange}</div>
              </c:if>              
              <dl class="atg_commerce_csr_catalog_product_info_layout">
                <dt id="atg_commerce_csr_catalog_product_info_display_name" class="atg_commerce_csr_productVievTitle">
                  ${fn:escapeXml(productMap.displayName)}
                </dt>
                <dd id="atg_commerce_csr_catalog_product_info_repository_id" class="atg_commerce_csr_productVievID">
                  ${fn:escapeXml(productMap.repositoryId)}
                </dd>
                <dd id="atg_commerce_csr_catalog_product_info_description" class="atg_commerce_csr_productVievDesc">
                  ${fn:escapeXml(productMap.description)}
                </dd>
                <c:if test="${hidePrice != true}">
                 <dd id="atg_commerce_csr_catalog_product_info_price" class="atg_commerce_csr_productVievPrice">
                   ${productPriceRange}
                 </dd>
                 
                </c:if>
              </dl>
            </c:if>
            <c:if test="${not empty product.sizeChartName}">
            	<a href="javascript:void(0);" onclick="showDetailsPopup('sizeChart');" class="sizeChart">size chart</a>
            </c:if>
          </div>
        </csr:getProduct>
      </dsp:oparam>      
       
    </dsp:droplet>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/productInformation.jsp#1 $$Change: 875535 $--%>
