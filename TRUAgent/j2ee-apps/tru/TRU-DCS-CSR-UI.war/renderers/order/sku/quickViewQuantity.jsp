<%--
 
 This page renders the quantity column header and cells of the SKU table on the
 Product Quick View Popup.

 @param product - The product item
 @param sku - The SKU item belonging to the product
 @param property - The property name of the SKU to display
 @param area - The area to render, in "header" | "cell"
 @param renderInfo - The render info object
 @param trId - The DOM ID of the row or <tr> tag
 @param tdId - The DOM ID of the cell, or <td> or (<th> tag)
 @param loopTagStatus - The status object of the loop tag

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/quickViewQuantity.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
      <dsp:getvalueof var="area" param="area"/>
      <dsp:getvalueof var="renderInfo" param="renderInfo"/>
      <dsp:getvalueof var="status" param="loopTagStatus"/>
      <dsp:tomap var="sku" param="sku"/>
      <input type="hidden" id="skuId" value="${sku.id}"/>
      
      <c:choose>
        <c:when test="${area == 'cell'}">
        
        <c:choose>
	         	<c:when test="${sku.unCartable || !sku.webDisplayFlag || sku.deleted || sku.type=='nonMerchSKU' || sku.supressInSearch || !sku.superDisplayFlag}">
	         		 <input id="sku-${sku.id}-quantity" size="5" maxlength="5" type="text" disabled style="background-color: #e6e6e6;" class="productQuickView-quantity"></br>Product not available for Purchase</c:when>
	         	<c:otherwise>
	         <input id="sku-${sku.id}-quantity" class="productQuickView-quantity"
            size="5"
            maxlength="3"
            type="text">
            <c:if test="${not empty sku.customerPurchaseLimit}">
         	limit ${sku.customerPurchaseLimit} items per customer
          </c:if>
	         	</c:otherwise>
         	</c:choose>
          
        </c:when>
        <c:when test="${area == 'header'}">
          <fmt:message key="genericRenderer.qty"/>
        </c:when>
       
        
      </c:choose>
    </dsp:layeredBundle>
  </dsp:page>
  <script>
/*  dojo.addOnLoad(function(){
	  var skuId = $("#skuId").val();
	  var inventory=dojo.byId("inventoryStatus").value;
	   if(inventory == undefined || inventory == 'outofstock'){
		   var temp = "#sku-"+skuId+"-quantity";
		   $(document).find(temp).attr('disabled','disabled');
	  }
  });  */
  </script>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/quickViewQuantity.jsp#1 $$Change: 875535 $--%>
