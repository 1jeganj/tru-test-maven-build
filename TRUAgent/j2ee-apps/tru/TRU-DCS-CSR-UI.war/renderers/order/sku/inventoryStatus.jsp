<%--
 A SKU table page fragment that displays the inventory status of a SKU item

 @param product - The product item
 @param sku - The SKU item belonging to the product
 @param property - The property name of the SKU to display
 @param area - The area to render, in "header" | "cell"
 @param renderInfo - The render info object
 @param trId - The DOM ID of the row or <tr> tag
 @param tdId - The DOM ID of the cell, or <td> or (<th> tag)
 @param loopTagStatus - The status object of the loop tag

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/inventoryStatus.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
  <dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRValidatePreSellableDroplet"/>
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <dsp:importbean bean="/atg/multisite/Site" />
      <dsp:getvalueof var="currentSiteId" bean="Site.id" />
      <dsp:getvalueof var="area" param="area"/>
      <dsp:getvalueof var="renderInfo" param="renderInfo"/>
      <dsp:getvalueof var="skuItem" param="skuItem" />
      <c:choose>
        <c:when test="${area == 'cell'}">
          <dsp:getvalueof var="sku" param="sku"/>
          <dsp:tomap var="skumap" value="${sku}"/>
          
          <dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
													<dsp:param name="productId" value="${skumap.onlinePID}" />
													<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="productPageUrl"
															param="productPageUrl" />
													</dsp:oparam>
												</dsp:droplet>
												
					<c:choose>
					  <c:when test="${not empty sku.nmwa && (sku.nmwa eq 'Y')}">
			         		<c:set var="nmwaEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="nmwaEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
          
          <input type="hidden" id="itemProductId_${sku.id}" value="${skumap.originalParentProduct}">
		  <input type="hidden" id="catalogRefId_${sku.id}" value="${sku.id}">
		  <input type="hidden" id="currentSiteId_${sku.id}" value="${currentSiteId}">
		  <input type="hidden" id="productPageUrl_${sku.id}" value="${productPageUrl}"> 
		  <input type="hidden" id="skuDisplayName_${sku.id}" value="${skumap.displayName}">
		  <input type="hidden" id="description_${sku.id}" value="${skumap.description}">
		  <input type="hidden" id="onlinePID_${sku.id}" value="${skumap.onlinePID}">
		  
		  
		  
          <dsp:droplet name="TRUCSRValidatePreSellableDroplet">
							<dsp:param name="sku" value="${sku}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="preSellable" param="preSellable" />
								<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
						</dsp:droplet>
          <csr:fullInventoryStatus commerceItemId="${skumap.id}" preSellable="${preSellable}" preSellQtyUnits="${sku.presellQuantityUnits}" formattedDate="${formattedDate}" customerPurchaseLimit="${sku.customerPurchaseLimit}" nmwaEligible="${nmwaEligible}"/>
        </c:when>
        
        <c:when test="${area == 'header'}">
          <fmt:message key="inventoryStatusRenderer.status"/>
        </c:when>
      </c:choose>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/inventoryStatus.jsp#1 $$Change: 875535 $--%>
