<%--
 Renders the action bits of the SKU browser

 @renderInfo - The RenderInfo
 @product - The product

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/skuBrowserAction.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>

<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
      <dsp:importbean bean="/atg/commerce/custsvc/order/OrderIsModifiable"/>
      <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="shoppingCart"/>
   	  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>  
   	  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" /> 
	  <dsp:getvalueof var="renderInfo" param="renderInfo"/>
      <dsp:getvalueof var="productItem" param="product"/>
      <dsp:getvalueof var="stockLevel" param="stockLevel"/>
      <dsp:getvalueof var="s2s" param="s2s"/>
      <dsp:getvalueof var="ispu" param="ispu"/>
      <dsp:getvalueof var="sku" param="sku"/>
      <dsp:getvalueof var="skuItem" param="skuItem"/>
      <dsp:tomap var="product" value="${productItem}"/>
      <svc-ui:frameworkUrl var="url" splitChar="|" 
        panelStacks="${renderInfo.pageOptions.successPanelStacks}"
       />
      <c:if test="${empty commerceItemId}">
      <dsp:tomap var="ci" value="${commerceItem}"/>
      <c:set var="commerceItemId" value="${ci.id}"/>
    </c:if>
    
       <dsp:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
      <dsp:input type="hidden" value="${fn:length(product.childSKUs)}"
        priority="100"
        bean="${renderInfo.pageOptions.formHandler}.addItemCount"/>
      <dsp:input type="hidden" value="${url}"
        bean="${renderInfo.pageOptions.formHandler}.${renderInfo.pageOptions.successUrlProperty}"/>
       <dsp:input type="hidden" value="${url}"
        bean="${renderInfo.pageOptions.formHandler}.${renderInfo.pageOptions.errorUrlProperty}"/>
        <dsp:input type="hidden" value="pdp"
        bean="${renderInfo.pageOptions.formHandler}.pageName"/>
        <dsp:input type="hidden" value="dummy" priority="-100"
        bean="${renderInfo.pageOptions.formHandler}.addItemToOrder"/>
      <div class="atg_dataTableFooterActions">
        <div class="atg_actionTo">
          <c:set var="isOrderModifiableDisableAttribute" value=" disabled='disabled'"/>
          <c:if test="${!empty shoppingCart.originalOrder}">
            <dsp:droplet name="OrderIsModifiable">
              <dsp:param name="order" value="${shoppingCart.originalOrder}"/>
              <dsp:oparam name="true">
              	<c:if test="${inventoryStatus eq 'inStock'}">
                	<c:set var="isOrderModifiableDisableAttribute" value=""/>
                </c:if>
              </dsp:oparam>
            </dsp:droplet>
          </c:if>
          
                                           
             	                
            <input type="button" id="skuBrowserAction" value="<fmt:message key='genericRenderer.addToOrder'/>">
                                        
          
          <%-- Bug 16050193 fix --%>
          <script type="text/javascript">
            _container_.onLoadDeferred.addCallback(function () {
              document.getElementById('skuBrowserAction').onclick=function(){
            	  $(".error").remove();
            	  $('.quantity-input').removeAttr('style');
            	  var quantityExceedError = false;
            	  var customerPurchaseLimitError = false;
            	  var skuId ='';
            	  var purchaseLimit = '';
            	  var noQtySelected = true;
            	  var qtyZeroError = false;
            	  var onlineOnlyError=false;
            	  var isIspuOnlyError = false;
            	  var outOfStockError=false;
            	  var isForMatureOnly = false;
            	  $('.quantity-input').filter(function(){
            		 var skuid = $(this).attr('id'); 
            		 var qtyInputLevel = $(this).val();
            		 var skuStockLevel = $("#stockLevel_"+skuid).val();
            		 var customerpurchaselimit = $("#customerPurchaseLimit_"+skuid).val();
            		 var isispuOnly = $("#isEligibleIspu_"+skuid).val();
            		 var onlineEligible=$("#onlineEligible_"+skuid).val();
            		 var matureValue = $("#isAvailableOnlyForMature_"+skuid).val();
            		 if($.trim($(this).val()) != '')
            			 {
            			 	noQtySelected = false;
            			 }
            		 if(parseInt($.trim($(this).val())) == 0)
        			 {
            			 qtyZeroError = true;
            			 $(this).css({"border-color":"red"});
        			 }
            		 if(skuStockLevel=="" && qtyInputLevel>0)
        			 {
        				 outOfStockError = true;
        				 skuId = skuid;
        			 }
            		 if(skuStockLevel!="" && onlineEligible == 'false')
        			 {
        				 onlineOnlyError = true;
        			 }
            		 if(isispuOnly == 'true' &&  skuStockLevel == "" && ($.trim($(this).val()) != '' ||$.trim($(this).val()) != 0) )
	            		 {
	            			 isIspuOnlyError = true;
	            		 }
            		 
            		 else if(parseInt(qtyInputLevel) > parseInt(skuStockLevel) && isispuOnly == 'false')
     			 		{
     			 			quantityExceedError = true;
     			 			$(this).css({"border-color":"red"});
     			 		}
            		 else if(parseInt(customerpurchaselimit) > 0 && parseInt(qtyInputLevel) > parseInt(customerpurchaselimit))
	           			 {
	           			 	customerPurchaseLimitError = true;
	           			 	skuId = skuid;
	           			 	purchaseLimit = customerpurchaselimit;
	           			 	$(this).css({"border-color":"red"});
	           			 }
            		 else if(matureValue == 'true')
            			 {
            			 	isForMatureOnly = true;
            			 }
            		 
            	  });
            	  if(quantityExceedError || customerPurchaseLimitError || noQtySelected || qtyZeroError || isIspuOnlyError || isForMatureOnly || onlineOnlyError || outOfStockError)
            		  {
            		  	if(quantityExceedError)
            		  		{
            		  			$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">Quantity input exceeds stock level. Please enter valid quantity</span>');
            		  		}
            		  	else if(customerPurchaseLimitError)
            		  		{
	            		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">Customer purchase limit for <b style="color:black;">'+skuId+'</b> is <b style="color:black;">'+purchaseLimit+'</b> please select quantity accordingly</span>');
            		  		}
            		  	else if(noQtySelected)
		       		  		{
		           		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">Please select any sku quantity </span>');
		       		  			
		       		  		}
            		  	else if(qtyZeroError)
	        		  		{
	            		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">quantity should not be zero </span>');
	        		  			
	        		  		}
            		  	else if(outOfStockError)
        		  		{
            		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red"> <b style="color:red;">'+skuId+'</b> went out of stock</span>');
        		  		}
            		  	
            		  	else if(onlineOnlyError)
            		  		{
            		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">Not available for ship to home</span>');
            		  		}
            		  	else if(isIspuOnlyError)
	            		  	{
	            		  		$(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">This item only eligible for instore pickup</span>');
	            		  	}
            		  	else if(isForMatureOnly)
            		  		{
            		  		atg.commerce.csr.common.showPopupWithReturn({
            				 	title:'Mature Product',
            		            popupPaneId: 'atg_commerce_csr_catalog_productDetailPopup',
            		            url: "/TRU-DCS-CSR/include/catalog/esrbRating.jsp?displayEsrbrating="+isForMatureOnly,
            		            onClose: function(args) {
            		              
            		            }});
            		  		}
            		  	return false;
            		  }
            	  else
            		  {
            		  	atg.commerce.csr.order.skuBrowserAction('${renderInfo.pageOptions.successPanelStacks}', '<dsp:valueof param="productId"/>');
	                    return false;
            		  }
              };
            });
            function isNumberOnly(evt){
            	evt = (evt) ? evt : window.event;
            	var charCode = (evt.which) ? evt.which : evt.keyCode;
            	 if (navigator.userAgent.indexOf("Firefox") > 0) {
            	    	if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
            	    		return true;
            	        	}
            	    		return false;
            	    	}
            	    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
            	    	 return true;
            	    }
            	   
            	    return false;
            }
            function addproductToCart()
            {
            	atg.commerce.csr.order.skuBrowserAction('${renderInfo.pageOptions.successPanelStacks}', '<dsp:valueof param="productId"/>');
            	hidePopupWithResults('atg_commerce_csr_catalog_productDetailPopup', {result:'cancel'});
            }
            
          </script>
          <c:if test="${CSRConfigurator.usingInStorePickup}">
          <c:if test ="${!(ispu eq 'N' && s2s eq 'N')}"> 
            <script type="text/javascript">
              _container_.onLoadDeferred.addCallback(function () {
              window.skuArray = {};
              <c:forEach items="${product.childSKUs}" var="skuItem">
                <dsp:droplet name="/atg/commerce/catalog/OnlineOnlyDroplet">
                  <dsp:param name="product" value="${productItem}"/>
                  <dsp:param name="sku" value="${skuItem}"/>
                  <dsp:oparam name="true">
                    window.skuArray["${skuItem.repositoryId}"] = true;
                  </dsp:oparam>
                  <dsp:oparam name="false">
                    window.skuArray["${skuItem.repositoryId}"] = false;
                  </dsp:oparam>
                </dsp:droplet>
                <dsp:param name="state" value="${profile.shippingAddress.postalCode}"/>
              </c:forEach>
              
                            
              });

              if (!dijit.byId("pickupLocationsPopup")) {
                new dojox.Dialog({ id: "pickupLocationsPopup",
                                   cacheContent: false, 
                                   executeScripts:"true",
                                   "class":"atg_commerce_csr_popup"});                
              }
            </script>
            <c:set var="pickupInStoreURL" value="${CSRConfigurator.truContextRoot}/panels/catalog/pickupLocations.jsp?_windowid=${windowId}" /> 
            <br />
            <br />
            <span class="atg_commerce_csr_dataHighlight hidden" id="pickupInStoreLabel">
              <span id="ea_csc_instore_pickup_available"></span>
            </span>
            <fmt:message var='pickupInStoreTitle' key='genericRenderer.pickupInStoreTitle'/>
            <input type="button" 
              id="pickupInStoreAction"
              value="<fmt:message key='genericRenderer.pickupInStore'/>"
              onClick="atg.commerce.csr.catalog.pickupInStoreAction('${pickupInStoreTitle}', '${pickupInStoreURL}', '${product.repositoryId}','${profile.shippingAddress.postalCode}','${profile.shippingAddress.state}','${profile.shippingAddress.city}','${profile.shippingAddress.country}');return false;">
            <%-- Bug 16050193 fix --%>
            <script type="text/javascript">
              _container_.onLoadDeferred.addCallback(function () {
                document.getElementById('pickupInStoreAction').onclick=function(){
                  atg.commerce.csr.catalog.pickupInStoreAction('${pickupInStoreTitle}', '${pickupInStoreURL}', '${product.repositoryId}','${profile.shippingAddress.postalCode}','${profile.shippingAddress.state}','${profile.shippingAddress.city}','${profile.shippingAddress.country}');
                  return false;
                };
              });
            </script>
            </c:if>
            <br>
            <c:choose>
	            <c:when  test="${ispu eq 'Y' && s2s eq 'Y'}">
	            	<fmt:message key="productinfo.sku.storepickup.todayasap" bundle="${TRUCustomResources}"/>
	            </c:when>
	            <c:when  test="${ispu eq 'Y' && s2s eq 'N'}">
	            	<fmt:message key="productinfo.sku.storepickup.today" bundle="${TRUCustomResources}"/>
	            </c:when> 
	            <c:when  test="${ispu eq 'N' && s2s eq 'Y'}">
	            	<fmt:message key="productinfo.sku.storepickup.5or10days" bundle="${TRUCustomResources}"/>
	            </c:when>
	            <c:otherwise>
	            	<fmt:message key="productinfo.sku.storepickup.notavailable" bundle="${TRUCustomResources}"/>
	            </c:otherwise>
	        </c:choose> 
          </c:if>
        </div>
      </div>
    </dsp:layeredBundle>
    
  </dsp:page>
  <script>
  atg.commerce.csr.catalog.pickupInStoreAction=function(_355,_356,_357,_358,_359,_360,_361){
	  var quantity;
	  var skuId;
	  for(itemKey in window.skuArray){
			if(!window.skuArray[itemKey] && dojo.byId(itemKey) && dojo.byId(itemKey).value){
				skuId=itemKey;
				quantity=dojo.byId(itemKey).value;	
				break;
			}
		} 
	  if(quantity != undefined && quantity != null && quantity > 0){
		  atg.commerce.csr.common.showPopupWithReturn({popupPaneId:"pickupLocationsPopup",title:_355,url:_356+"&productId="+_357+"&skuId="+skuId+"&quantity="+quantity+"&pinCode="+_358+"&state="+_359+"&city="+_360+"&country="+_361});
	  }else{
		  $(".error").remove();
    	  $('.quantity-input').removeAttr('style');
     	  if(quantity == undefined || quantity.trim() == ""){
     		 $(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">Please select any sku quantity </span>');
     	  }else if(quantity <= 0){
     		 $(".atg_commerce_csr_coreProductViewData").prepend('<span class="error" style="color:red">quantity should not be zero </span>');
     	  }  	
     	return false;
     }  
	  
	};
	 
     _container_.onLoadDeferred.addCallback(function () {
       document.getElementById('pickupInStoreAction').onclick=function(){
         atg.commerce.csr.catalog.pickupInStoreAction('${pickupInStoreTitle}', '${pickupInStoreURL}', '${product.repositoryId}','${profile.shippingAddress.postalCode}','${profile.shippingAddress.state}','${profile.shippingAddress.city}','${profile.shippingAddress.country}');
         return false;
       };
     });
   
</script>
 <script>
 	 /* var quantityVal = function(element){
 		var $this = $(element);
 		var id=$(element).attr("id");
 		var stockLevel = $("#stockLevel_"+id).val();
 		var qty = $this.val();
 		
 		if(stockLevel != '' && stockLevel != undefined){
	 		if(qty == 0 || parseInt(qty) > parseInt(stockLevel) ){
	 			$('#skuBrowserAction').attr('disabled','disabled');
	 			$("#pickupInStoreAction").attr('disabled','disabled');
	 		}else{
	 			$('#skuBrowserAction').removeAttr('disabled');
	 			$('#pickupInStoreAction').removeAttr('disabled');
	 		}
 		}else{
 			$('#pickupInStoreAction').attr('disabled');
 			$('#skuBrowserAction').attr('disabled');
 		}
 	};  */
 	  $(document).ready(function(){
 		 if($('.quantity-input').length <= 0)
	 		{
	 			document.getElementById('skuBrowserAction').disabled = true;
	 			if($('#pickupInStoreAction').length)
	 				{
	 					document.getElementById('pickupInStoreAction').disabled = true;
	 				}
	 		}
 		 else
 			 {
 				document.getElementById('skuBrowserAction').disabled = false;
 				if($('#pickupInStoreAction').length)
 				{
 					document.getElementById('pickupInStoreAction').disabled = false;
 				}
 			 }
 		 $('.quantity-input').filter(function(){
 			 var skuid = $(this).attr('id');
 			 var inventoryStatus = $("#inventoryStatus_"+skuid).val();
 			 var isEligibleIspu = $("#isEligibleIspu_"+skuid).val();
 			 if(inventoryStatus == 'outofstock' && isEligibleIspu == 'false')
 				 {
 				 	$(this).attr('disabled','disabled');
 				 }
 		 });
 		 $(document).on('click','.storeResultsAddToCart',function(){
 			 $("#pickupInStoreAction").removeAttr("disabled");
 		 });
      });
 	/* $( document ).ready(function() {
 		var s2s = $("#s2s").val();
 		if(s2s == 'Y'){
 			$("#pickupInStoreAction").show();
 		}else{
 			$("#pickupInStoreAction").hide();
 		}
 	} */
 </script>

</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/skuBrowserAction.jsp#1 $$Change: 875535 $--%>