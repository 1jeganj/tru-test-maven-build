<%--
 A page fragment that displays something to click on to view the spcifiec SKU)

 @param product - The product item
 @param skuItem - The SKU item belonging to the product
 @param property - The property name of the SKU to display
 @param area - The area to render, in "header" | "cell"
 @param renderInfo - The render info object
 @param trId - The DOM ID of the row or <tr> tag
 @param tdId - The DOM ID of the cell, or <td> or (<th> tag)

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/viewItem.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
      <dsp:tomap var="skuMap" param="skuItem"/>
      <dsp:getvalueof var="productId" param="productId"/>
      <dsp:getvalueof var="imageUrl" value="${skuMap.primaryImage}"/>
      <dsp:getvalueof var="panelId" param="panelId"/>
      <dsp:getvalueof var="area" param="area"/>
      <dsp:getvalueof var="renderInfo" param="renderInfo"/>
      <dsp:getvalueof var="item" vartype="" param="item"/>
      <dsp:getvalueof value="false" var="matureValue"></dsp:getvalueof>
      <c:choose>
        <c:when test="${area == 'cell'}">
          <a href="#" class="atg_tableIcon atg_propertyView" 
            title="<fmt:message key='genericRenderer.viewProperty'/>"
            onclick="viewSkuDescription('<c:out value="${fn:escapeXml(skuMap.id)}"/>','<c:out value="${fn:escapeXml(productId)}"/>','<c:out value="${fn:escapeXml(skuMap.displayName)}"/>','<c:out value="${fn:escapeXml(skuMap.description)}"/>','${fn:escapeXml(imageUrl)}','${fn:escapeXml(panelId)}','${item.esrbrating}','${item.videoGameEsrb}','<c:out value="${fn:escapeXml(item.promotionalStickerDisplay)}"/>','<c:out value="${fn:escapeXml(item.channelAvailability)}"/>','<c:out value="${fn:escapeXml(item.presellable)}"/>','<c:out value="${fn:escapeXml(item.shipInOrigContainer)}"/>')">
            <fmt:message key="genericRenderer.view"/>
          </a>
          <input type="hidden" id="productDetails_${skuMap.id}" value="${item}"/>
          <c:if test="${not empty item.videoGameEsrb}">
	          <c:choose>
				<c:when test="${item.videoGameEsrb eq 'Mature'}">
          			<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
       			</c:when>
       			<c:when test="${item.videoGameEsrb eq 'Adults Only'}">
       				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
       			</c:when>
       		  </c:choose>
          </c:if>
          <input id="isAvailableOnlyForMature_${skuMap.id}" class="maturevalue" type="hidden" value="${matureValue}">  
        </c:when>
        <c:when test="${area == 'header'}">
          &nbsp;
        </c:when>
      </c:choose>
      
      
			
      <%--  <dsp:getvalueof var="adjustments" value="${skuMap.skuQualifiedForPromos}"/>
       ${adjustments} --%>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<script>
if (!dijit.byId("atg_commerce_csr_catalog_productDetailPopup")) {
    new dojox.Dialog({ id:"atg_commerce_csr_catalog_productDetailPopup",
                       cacheContent:"false", 
                       executeScripts:"true",
                       scriptHasHooks:"true",
                       duration: 100,
                       "class":"atg_commerce_csr_popup"});
  }
if (!dijit.byId("atg_commerce_csr_catalog_sizeChartPopup")) {
    new dojox.Dialog({ id:"atg_commerce_csr_catalog_sizeChartPopup",
                       cacheContent:"false", 
                       executeScripts:"true",
                       scriptHasHooks:"true",
                       duration: 100,
                       "class":"atg_commerce_csr_popup"});
  }
function viewSkuDescription(skuId, productId, displayName, description, imageURL, panelId, esrbRating, videoGameSKU, promotionalStickerDisplay, channelAvailability, presellable, shipInOrig) {
    //alert(shipInOrig);
    var currSkuId = document.getElementById("atg_commerce_csr_catalog_product_info_currSkuId");
    var infoImage = document.getElementById("atg_commerce_csr_catalog_product_info_image");
    var infoDisplayName = document.getElementById("atg_commerce_csr_catalog_product_info_display_name");
    var infoRepositoryId = document.getElementById("atg_commerce_csr_catalog_product_info_repository_id");
    var infoDescription = document.getElementById("atg_commerce_csr_catalog_product_info_description");
    var infoPrice = document.getElementById("atg_commerce_csr_catalog_product_info_price");
    var infoTest = document.getElementById("test");

    if (currSkuId.value && currSkuId.value == skuId) {
        var productImgUrl = document.getElementById("atg_commerce_csr_catalog_product_info_product_image").value;
        if (dojo.string.trim(productImgUrl) == "") {
            infoImage.src = "/TRU-DCS-CSR/images/no-image500.gif";
        } else {
            infoImage.src = productImgUrl;
        }
        infoDisplayName.innerHTML = document.getElementById("atg_commerce_csr_catalog_product_info_product_display_name").value;
        infoRepositoryId.innerHTML = document.getElementById("atg_commerce_csr_catalog_product_info_product_repository_id").value;
        infoDescription.innerHTML = document.getElementById("atg_commerce_csr_catalog_product_info_product_description").value;
        if (infoPrice) {
            infoPrice.innerHTML = document.getElementById("atg_commerce_csr_catalog_product_info_product_price").innerHTML;
        }
        currSkuId.value = "";
        $('#skuPromoDetails').hide();
        $('#productDescription').hide();
        $("#esrbRating").hide();
        $("#promotionalStickerDisplay").hide();
        $("#channelAvailability").hide();
        $(".atg_commerce_csr_catalog_product_info_layout #learnMoreDDDiv").hide();
    } else {
    	/* alert(2); */
    
        if (dojo.string.trim(imageURL) != "") {
            infoImage.src = imageURL;
        } else {
            var productImgUrl = document.getElementById("atg_commerce_csr_catalog_product_info_product_image").value;
            if (dojo.string.trim(productImgUrl) == "") {
                infoImage.src = "/TRU-DCS-CSR/images/no-image500.gif";
            } else {
                infoImage.src = productImgUrl;
            }
        }
        infoDisplayName.innerHTML = displayName;
        infoRepositoryId.innerHTML = skuId;
        infoDescription.innerHTML = description;
        if (!panelId || dojo.string.trim(panelId) == "") panelId = "productView";
        var price = document.getElementById(skuId + "-" + productId + "-" + panelId + "-price-td");
        if (price) {
            if (infoPrice) {
                infoPrice.innerHTML = price.innerHTML;
            }
        } else {
            if (infoPrice) {
                infoPrice.innerHTML = "";
            }
        }
        currSkuId.value = skuId;
        if ($.trim(esrbRating) != '' && $.trim(videoGameSKU) != '') {
            dojo.xhrPost({
                url: "/TRU-DCS-CSR/include/catalog/esrbRating.jsp?esrbrating=" + esrbRating + "&videoGameEsrb=" + videoGameSKU,
                content: {
                    _windowid: window.windowId
                },
                encoding: "utf-8",
                preventCache: true,
                handle: function(_1f, _2f) {
                    if (!(_1f instanceof Error)) {
                        $("#esrbRating").html(_1f).show();
                    }
                },
                mimetype: "text/html"
            });
        } else {
            $("#esrbRating").html("").hide();
        }
        if ($.trim(channelAvailability) != '') {
            var colorSizeAvailableStatus = $("#productColorSizeAvailStatus").val();

            dojo.xhrPost({
                url: "/TRU-DCS-CSR/include/catalog/displayChannelAvailabilityMsg.jsp?colorSizeVariantsAvail=" + colorSizeAvailableStatus + "&channelAvailability=" + channelAvailability + "&presellable=" + presellable,
                content: {
                    _windowid: window.windowId
                },
                encoding: "utf-8",
                preventCache: true,
                handle: function(_1f, _2f) {
                    if (!(_1f instanceof Error)) {
                        $("#channelAvailability").html(_1f).show();
                    }
                },
                mimetype: "text/html"
            });
        }
        if ($.trim(promotionalStickerDisplay) != '') {
            $("#promotionalStickercontent").html(promotionalStickerDisplay).show();
            $("#promotionalStickerDisplay").show();
        } else {
            $("#promotionalStickerDisplay").html("").hide();
        }

        if ($.trim(skuId) != '') {
            dojo.xhrPost({
                url: "/TRU-DCS-CSR/include/catalog/displayPromotionsOnPDP.jsp?skuId=" + skuId,
                content: {
                    _windowid: window.windowId
                },
                encoding: "utf-8",
                preventCache: true,
                handle: function(_1f, _2f) {
                    if (!(_1f instanceof Error)) {
                        $("#skuPromoDetails").html(_1f).show();
                    }
                },
                mimetype: "text/html"
            });
        } else {
            $("#skuPromoDetails").html("").hide();
        }
        var tag = "<a href='#' onclick='showProductDetailPopup();'>View Product Details</a>"
        $("#productDescription").html(tag).show();
        if (shipInOrig == "true") {
            var learnMoreDDDiv = '<dd id="learnMoreDDDiv" style="width: 100%; float: left; line-height: 20px; margin-top: 10px; display: block;" onclick="javascript:void(0)"><div class="shopping-cart-surprise-image-div">dont spoil the surprise</div> <a href="#" onclick="showSpoilDetails();" >Learn More</a></dd>';
            if (!$(".atg_commerce_csr_catalog_product_info_layout").find("#learnMoreDDDiv").length) {
                $(".atg_commerce_csr_catalog_product_info_layout").append(learnMoreDDDiv);
            }
            $(".atg_commerce_csr_catalog_product_info_layout #learnMoreDDDiv").show();
        }else{
        	$(".atg_commerce_csr_catalog_product_info_layout #learnMoreDDDiv").hide();
        }
        
    }
    $("#test").removeClass("hidden");
    /* infoTest.innerHTML = "testhtml"; */
}
	function showProductDetailPopup()
	{
		
		var skuId = $("#atg_commerce_csr_catalog_product_info_repository_id").html();
		var item = $("#productDetails_"+skuId).val();
		var manufacturerStyleNumber = $("#productManufacturerStyleNumber").val();
		var rusNumber = $("#productRusItemNumber").val();
		if($.trim(skuId) != "")
			{
			 atg.commerce.csr.common.showPopupWithReturn({
				 	title:'Item Details',
		            popupPaneId: 'atg_commerce_csr_catalog_productDetailPopup',
		            url: "/TRU-DCS-CSR/include/catalog/productDetailsBlock.jsp?skuId="+skuId+"&manufacturerStyleNumber="+manufacturerStyleNumber+"&rusItemNumber="+rusNumber,
		            onClose: function(args) {
		              
		            }});
			}
	}
	function showDetailsPopup(toBeDisplay, channelMode, siteId)
	{
		var title,pUrl;
		if(toBeDisplay == 'sizeChart')
			{
				sizeChartName = document.getElementById("productSizeChartName").value;
				title = 'Size Chart';
				pUrl = '/TRU-DCS-CSR/include/catalog/popupInfo.jsp?sizeChart='+sizeChartName+'&siteId='+siteId;
			}
		else if(toBeDisplay == 'channelAvailability')
			{
				title = 'Channel Availability Details';
				pUrl = '/TRU-DCS-CSR/include/catalog/popupInfo.jsp?channelAvailability=true&channelMode='+channelMode;
			}
		atg.commerce.csr.common.showPopupWithReturn({
		 	title: title,
            popupPaneId: 'atg_commerce_csr_catalog_sizeChartPopup',
            url: pUrl
            });
	}
	if (!dijit.byId("atg_commerce_csr_catalog_promotionDetailPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_promotionDetailPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
	function showPromotionDetails(promoId)
	{
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_promotionDetailPopup',
            title:'Promo Details',
            url: "/TRU-DCS-CSR/include/catalog/promotionDetailsPopup.jsp?promotionId="+promoId,	
            dataType:"html",
            onClose: function(args) {
            	
            }});
		
	}
</script>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/viewItem.jsp#1 $$Change: 875535 $--%>
