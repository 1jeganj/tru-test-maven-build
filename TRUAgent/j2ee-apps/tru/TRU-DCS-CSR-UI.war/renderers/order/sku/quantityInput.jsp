<%--
 A page fragment that takes SKU quantity input

 @param product - The product item
 @param sku - The SKU item belonging to the product
 @param property - The property name of the SKU to display
 @param area - The area to render, in "header" | "cell"
 @param renderInfo - The render info object
 @param trId - The DOM ID of the row or <tr> tag
 @param tdId - The DOM ID of the cell, or <td> or (<th> tag)
 @param loopTagStatus - The status object of the loop tag

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/quantityInput.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  	<dsp:page xml="true">
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <dsp:importbean bean="/com/tru/commerce/droplet/TRUCSRPriceDroplet" />
    <dsp:importbean var="site" bean="/atg/multisite/Site" />
     <dsp:getvalueof var="area" param="area"/>
     <dsp:getvalueof var="renderInfo" param="renderInfo"/>
     <dsp:getvalueof var="status" param="loopTagStatus"/>
     <c:set var="isQuantiyDisabled" value="true"/>
     <c:choose>
     	<c:when test="${area == 'cell'}">
        	<dsp:tomap var="product" param="product"/>
          	<dsp:tomap var="sku" param="sku"/>
          	<c:set var="ispuEligible" value="false"/>
          	<c:set var="s2s" value="false"/>
          	<c:set var="ispu" value="false"/>
			<c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
				<c:set var="s2s" value="true" />
			</c:if>
			<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
				<dsp:getvalueof var="ispu" value="true" />
			</c:if>
			<c:if test="${ispu eq 'true' || s2s eq 'true'}">
				<c:set var="ispuEligible" value="true"/>
			</c:if>
				
         	<c:set var="onlineEligible" value="false"/>
         	<c:if test="${empty sku.channelAvailability or sku.channelAvailability ne 'In Store Only'}">
         		<c:set var="onlineEligible" value="true"/>
         	</c:if>
         	
		    <input type="hidden" id="onlineEligible_${sku.id}" value="${onlineEligible}">
         	<input type="hidden" id="isEligibleIspu_${sku.id}" value="${ispuEligible}"/>
          	<dsp:input type="hidden" value="${product.id}" bean="${renderInfo.pageOptions.formHandler}.items[${status.index}].productId"/>
            
          	<dsp:input type="hidden" value="${sku.id}"
            bean="${renderInfo.pageOptions.formHandler}.items[${status.index}].catalogRefId"/>
            
         <%--  <dsp:input type="text" disabled="${isQuantiyDisabled}"
            bean="${renderInfo.pageOptions.formHandler}.items[${status.index}].quantity" 
            id="${sku.id}" 
            size="3" 
            number="#" 
            maxlength="5" 
            value=""
            onkeyup="quantityVal(this);">
            <dsp:tagAttribute name="class" value="quantity-input" />
          </dsp:input>  --%>
          
       <dsp:droplet name="TRUCSRPriceDroplet">
		<dsp:param name="skuId" value="${sku.id}"/>
		<dsp:param name="productId" value="${product.id}" />
		<dsp:param name="site" value="${site}" />
		<dsp:oparam name="true">
		</dsp:oparam>
		<dsp:oparam name="false">
		</dsp:oparam>
		<dsp:oparam name="empty">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		</dsp:oparam>
	</dsp:droplet>
          
          
          <c:choose>
           <c:when test="${sku.unCartable || !sku.webDisplayFlag || sku.deleted || sku.type=='nonMerchSKU' || sku.supressInSearch || !sku.superDisplayFlag || salePrice==0.0}">
           	Product not available for Purchase
          </c:when>
          <c:otherwise>
          <dsp:input type="text"
            bean="${renderInfo.pageOptions.formHandler}.items[${status.index}].quantity" 
            id="${sku.id}" 
            size="3" 
            number="#" 
            maxlength="3" 
            value=""  onkeypress="return isNumberOnly(event);">
            <dsp:tagAttribute name="class" value="quantity-input" />
          </dsp:input> </br>
          <c:if test="${not empty sku.customerPurchaseLimit}">
         	limit ${sku.customerPurchaseLimit} items per customer
          </c:if></c:otherwise>
          </c:choose>
          <%-- <dsp:input type="text" 
            bean="${renderInfo.pageOptions.formHandler}.items[${status.index}].quantity" 
            id="${sku.id}" 
            size="3" 
            number="#" 
            maxlength="5" 
            value=""
            >
            <dsp:tagAttribute name="class" value="quantity-input" />
          </dsp:input> --%>
        </c:when>
        <c:when test="${area == 'header'}">
          <fmt:message key="genericRenderer.qty"/>
        </c:when>
      </c:choose>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/sku/quantityInput.jsp#1 $$Change: 875535 $--%>