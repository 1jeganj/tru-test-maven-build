<%--
 A page fragment that displays product SKUs

 @param productId - The ID of the product to display
 @param commerceItemId - The ID of the product to display
 @param formSuffix - Identify
 @param panelId - The ID of the panel using this form
 @param skuId - Optional ID of SKU that should appear selected

 Either productId or commerceItemId must be specified

 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/productSkus.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>

<c:catch var="exception">
  <dsp:page xml="true">
  	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/com/tru/commerce/csr/catalog/ProductDetailsDroplet"/>
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
      <dsp:getvalueof var="panelId" param="panelId"/>
      <dsp:getvalueof var="productId" param="productId"/>
      <dsp:getvalueof var="commerceItemId" param="commerceItemId"/>
      <dsp:getvalueof var="formSuffix" param="formSuffix"/>
      <c:set var="s2s" value="N"/>
      <c:if test="${ not empty param.skuId }">
        <c:set var="skuId" value="${param.skuId}"/>
      </c:if>
      <csr:getProduct productId="${productId}" commerceItemId="${commerceItemId}">
        <c:set var="productItem" value="${product}"/>
        <dsp:tomap var="product" value="${productItem}"/>
        <c:choose>
	        <c:when test="${product.type ne 'collectionProduct'}">
	        <div class="atg_commerce_csr_coreProductView">
	          <dsp:form id="productSkuForm-${formSuffix}-${param.mode}"
	            formid="productSkuForm-${formSuffix}-${param.mode}" onsubmit="return false">
	            <%-- Include optional setup JSP --%>
	            <c:if test="${not empty renderInfo.pageOptions.pageSetup}">
	              <dsp:include src="${renderInfo.pageOptions.pageSetup}" otherContext="${renderInfo.contextRoot}">
	                <dsp:param name="renderInfo" value="${renderInfo}"/>
	                <dsp:param name="product" value="${productItem}"/>
	              </dsp:include>
	            </c:if>
	            <div class="atg_commerce_csr_coreProductViewData">
	            <c:set var="colorOrSizeVariantsAvailable" value="true"/>
	              <table id="skus-${productId}" class="atg_dataTable" cellpadding="0" cellspacing="0">
	                <%-- <c:forEach items="${product.childSKUs}" var="skuItem" varStatus="skuVs"> --%>
	                <dsp:droplet name="ProductDetailsDroplet">
	                	<dsp:param name="productId" value="${product.id}" />
	                	<dsp:oparam name="output">
	                		<dsp:getvalueof var="productInfo" param="productInfo"/>
	                		<dsp:getvalueof var="childSkus" value="${productInfo.childSKUsList}"/>
	                		<dsp:getvalueof var="hasColorVariant" param="hasColorVariant"/>
	                		<dsp:getvalueof var="hasSizeVariant" param="hasSizeVariant"/>
	                		<input id="productManufacturerStyleNumber" type="hidden" value="${productInfo.manufacturerStyleNumber}"/>
	                		<input id="productRusItemNumber" type="hidden" value="${productInfo.rusItemNumber}"/>
	                		<input id="productColorSizeAvailStatus" type="hidden" value="${productInfo.colorSizeVariantsAvailableStatus}"/>
	                		<input id="productSizeChartName" type="hidden" value="${productInfo.sizeChart}"/>
	                	</dsp:oparam>
	                	<dsp:oparam name="empty">
	                		There is a problem while fetching the product details. Please try again later.
	                	</dsp:oparam>
	                </dsp:droplet>
	                <c:choose>
		                <c:when test="${!hasColorVariant && !hasSizeVariant}">
		                	<c:set var="colorOrSizeVariantsAvailable" value="false"/>
		                </c:when>
		                <c:when test="${(hasColorVariant && !hasSizeVariant) || (!hasColorVariant && hasSizeVariant) }">
		                	<c:set var="onlyOneVariant" value="true"/>
		                </c:when>
		                <c:otherwise>
		                	<c:set var="bothVariantsAvailable" value="true"/>
		                </c:otherwise>
	                </c:choose>
	                <c:forEach items="${childSkus}" var="item" varStatus="skuVs">
	                  <dsp:tomap var="sku" value="${item.skuItem}"/>
	                  <c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
	                  	<c:set var="s2s" value="Y"/>
	                  </c:if>
	                  <c:if test="${empty sku.itemInStorePickUp || sku.itemInStorePickUp eq 'No' || sku.itemInStorePickUp eq 'N' ||  sku.itemInStorePickUp eq 'NO' }">
							<dsp:getvalueof value="N" var="ispu" />
					 	</c:if>
					 	<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
							<dsp:getvalueof value="Y" var="ispu" />
					 	</c:if>
	                  <c:if test="${skuVs.first}">
	                    <%-- 
	                      First time through the SKUs display the table column header. This
	                      happens inside the 'product.childSKUs' forEach loop so that the
	                      SKU's repository descriptor is available during the rendering of
	                      the column header.
	                     --%>
	                     <c:set var="proceedToDisplayHead" value="true"/>
	                     <c:set var="proceedToDisplayData" value="true"/>
	                    <thead>
	                      <c:set var="trId" value="trHead-${sku.id}"/>
	                      
	                      <tr id="${trId}">
	                        <%-- Display the configurable column headers --%>
	                        <c:forEach var="property" items="${renderInfo.properties}">
	                        <c:choose>
		                        <c:when test="${!colorOrSizeVariantsAvailable && (fn:containsIgnoreCase(renderInfo.renderer[property],'color') || fn:containsIgnoreCase(renderInfo.renderer[property],'size'))}">
		                        	<c:set var="proceedToDisplayHead" value="false"/>
		                        </c:when>
		                        <c:when test="${!hasColorVariant && fn:containsIgnoreCase(renderInfo.renderer[property],'color')}">
		                        	<c:set var="proceedToDisplayHead" value="false"/>
		                        </c:when>
		                        <c:when test="${!hasSizeVariant && fn:containsIgnoreCase(renderInfo.renderer[property],'size')}">
		                        	<c:set var="proceedToDisplayHead" value="false"/>
		                        </c:when>
		                        <c:otherwise>
		                        	<c:set var="proceedToDisplayHead" value="true"/>
		                        </c:otherwise>
	                        </c:choose>
	                         <c:if test="${proceedToDisplayHead}">
	                          <c:set var="css" 
	                            value='class="${renderInfo.cssThClass[property]}"'/>
	                          <c:set var="tdId" value="${property}-td-${sku.id}"/>
	                          <th scope="col" id="${tdId}"
	                            ${empty renderInfo.cssThClass[property] ? "" : css}>
	                            <c:choose>
	                              <c:when test="${not empty renderInfo.renderer[property]}">
	                                <%-- User renderer to render table column header --%>
	                                <dsp:include src="${renderInfo.renderer[property]}" otherContext="${CSRConfigurator.truContextRoot}">
	                                  <dsp:param name="product" value="${productItem}"/>
	                                  <%-- commerceItem supplied by csr:getProduct --%>
	                                  <dsp:param name="commerceItem" value="${commerceItem}"/>
	                                  <dsp:param name="commerceItemId" 
	                                    value="${commerceItemId}"/>
	                                  <dsp:param name="productId" value="${productId}"/>
	                                  <dsp:param name="sku" value="${sku}"/>
	                                  <dsp:param name="skuItem" value="${skuItem}"/>
	                                  <dsp:param name="property" value="${property}"/>
	                                  <dsp:param name="area" value="header"/>
	                                  <dsp:param name="renderInfo" value="${renderInfo}"/>
	                                  <dsp:param name="trId" value="${trId}"/>
	                                  <dsp:param name="tdId" value="${tdId}"/>
	                                  <dsp:param name="panelId" value="${panelId}"/>
	                                  <dsp:param name="loopTagStatus" value="${skuVs}"/>
	                                </dsp:include>
	                              </c:when>
	                              <c:otherwise>
	                                ${fn:escapeXml(renderInfo.displayName[property])}
	                              </c:otherwise>
	                            </c:choose>
	                          </th>
	                          </c:if>
	                        </c:forEach>
	                      </tr>
	                    </thead>
	                  </c:if>
	                  <c:if test="${skuVs.first}">
	                    <tbody>
	                  </c:if>
	                  <c:set var="trId" value="trBody-${panelId}-${sku.id}"/>
	                  
	                  <tr id="${trId}">
	                    <%-- Display the SKU properties --%>
	                    <c:forEach var="property" items="${renderInfo.properties}">
	                    <c:choose>
		                        <c:when test="${!colorOrSizeVariantsAvailable && (fn:containsIgnoreCase(renderInfo.renderer[property],'color') || fn:containsIgnoreCase(renderInfo.renderer[property],'size'))}">
		                        	<c:set var="proceedToDisplayData" value="false"/>
		                        </c:when>
		                        <c:when test="${!hasColorVariant && fn:containsIgnoreCase(renderInfo.renderer[property],'color')}">
		                        	<c:set var="proceedToDisplayData" value="false"/>
		                        </c:when>
		                        <c:when test="${!hasSizeVariant && fn:containsIgnoreCase(renderInfo.renderer[property],'size')}">
		                        	<c:set var="proceedToDisplayData" value="false"/>
		                        </c:when>
		                        <c:otherwise>
		                        	<c:set var="proceedToDisplayData" value="true"/>
		                        </c:otherwise>
	                        </c:choose>
	                     <c:if test="${proceedToDisplayData}">
	                      <c:set var="css" value='class="${renderInfo.cssTdClass[property]}"'/>
	                      <c:set var="tdId" value="${sku.id}-${productId}-${panelId}-${property}-td"/>
	                      <td ${empty renderInfo.cssTdClass[property] ? "" : css} id="${tdId}">
	                        <c:choose>
	                          <c:when test="${not empty renderInfo.renderer[property]}">
	                            <dsp:include src="${renderInfo.renderer[property]}" otherContext="${CSRConfigurator.truContextRoot}">
	                              <dsp:param name="product" value="${productItem}"/>
	                              <%-- commerceItem supplied by csr:getProduct --%>
	                              <dsp:param name="commerceItem" value="${commerceItem}"/>
	                              <dsp:param name="commerceItemId" value="${commerceItemId}"/>
	                              <dsp:param name="productId" value="${productId}"/>
	                              <dsp:param name="skuItem" value="${item.skuItem}"/>
	                              <dsp:param name="sku" value="${item.skuItem}"/>
	                              <dsp:param name="property" value="${property}"/>
	                              <dsp:param name="area" value="cell"/>
	                              <dsp:param name="trId" value="${trId}"/>
	                              <dsp:param name="tdId" value="${tdId}"/>
	                              <dsp:param name="panelId" value="${panelId}"/>
	                              <dsp:param name="loopTagStatus" value="${skuVs}"/>
	                               <dsp:param name="skuColor" value="${item.colorCode}"/>
	                              <dsp:param name="skuSize" value="${item.juninileSizeDesc}"/> 
	                              <dsp:param name="item" value="${item}"/>
	                            </dsp:include>
	                          </c:when>
	                          <c:when test="${fn:startsWith(property,'$')}">
	                            <c:out value="${item.skuItem[fn:substringAfter(property,'$')]}"/>
	                          </c:when>
	                          <c:otherwise>
	                            <c:out value="${sku[property]}"/>
	                          </c:otherwise>
	                        </c:choose>
	                      </td>
	                      </c:if>
	                    </c:forEach>
	                  </tr>
	                  <c:if test="${skuVs.last}">
	                    </tbody>
	                  </c:if>
	                 </c:forEach> 
	              </table>
	              <script type="text/javascript">
	              _container_.onLoadDeferred.addCallback(function () {
	                atg.keyboard.registerFormDefaultEnterKey("<c:out value='productSkuForm-${formSuffix}-${param.mode}' />","skuBrowserAction");
	              });
	              _container_.onUnloadDeferred.addCallback(function () {
	                atg.keyboard.unRegisterFormDefaultEnterKey("<c:out value='productSkuForm-${formSuffix}-${param.mode}' />");
	              });
	              </script>
	
	              <%-- Confirmation message --%>
	              <fmt:message key="catalogBrowse.searchResults.productAddedToOrder" var="addToOrderMsg">
	                <fmt:param value="${product.displayName}"/>
	              </fmt:message>
	              <input id="atg.successMessage" name="atg.successMessage" type="hidden" 
	                value="${fn:escapeXml(addToOrderMsg)}"/>
	              <%-- Include the bit that submits the form, if specified --%>
	              <c:if test="${not empty renderInfo.pageOptions.actionRenderer}">
	                <dsp:include src="${renderInfo.pageOptions.actionRenderer}" otherContext="${CSRConfigurator.truContextRoot}">
	                  <%-- commerceItem supplied by csr:getProduct --%>
	                  <dsp:param name="commerceItem" value="${commerceItem}"/>
	                  <dsp:param name="commerceItemId" value="${commerceItemId}"/>
	                  <dsp:param name="productId" value="${productId}"/>
	                  <dsp:param name="renderInfo" value="${renderInfo}"/>
	                  <dsp:param name="product" value="${productItem}"/>
	                  <dsp:param name="panelId" value="${panelId}"/>
	                  <dsp:param name="s2s" value="${s2s}"/>
	                  <dsp:param name="ispu" value="${ispu}"/>
	                  <dsp:param name="skuItem" value="${sku}"/>
	                  <dsp:param name="sku" value="${sku}"/>
	                </dsp:include>
	              </c:if>
	            </div>
	          </dsp:form>
	          
	          <!-- Display gift list controls -->
	          <c:if test="${not empty renderInfo.pageOptions.giftlistActionRenderer}">
	                <dsp:include src="${renderInfo.pageOptions.giftlistActionRenderer}" otherContext="${renderInfo.contextRoot}">
	                  <%-- commerceItem supplied by csr:getProduct --%>
	                  <dsp:param name="commerceItem" value="${commerceItem}"/>
	                  <dsp:param name="commerceItemId" value="${commerceItemId}"/>
	                  <dsp:param name="productId" value="${productId}"/>
	                  <dsp:param name="renderInfo" value="${renderInfo}"/>
	                  <dsp:param name="product" value="${productItem}"/>
	                  <dsp:param name="panelId" value="${panelId}"/>
	                </dsp:include>
	              </c:if>
	          
	        </div>
	        </c:when>
	        <c:otherwise>
	        	<fmt:message key="tru.productdetail.collectionnotallowed"  bundle="${TRUCustomResources}"/>
	        </c:otherwise>
        </c:choose>
      </csr:getProduct>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>

<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/productSkus.jsp#1 $$Change: 875535 $--%>
