
<%@ include file="/include/top.jspf" %>

<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:importbean bean="/atg/commerce/custsvc/returns/ReturnDroplet"/>
    <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
      <dsp:getvalueof var="order" param="currentOrder"/>
      <dsp:getvalueof var="isExistingOrderView" param="isExistingOrderView"/>
      
        
      <table class="atg_dataTable atg_commerce_csr_innerTable"
        cellspacing="0" cellpadding="0" summary="<fmt:message key='shoppingCartSummary.itemDetails' />">
        <thead>
          <tr>
            <c:if test="${isMultiSiteEnabled == true}">
              <th></th>
            </c:if>
            <th><fmt:message key='shoppingCartSummary.commerceItem.header.itemDesc' /></th>
            <th><fmt:message key='shoppingCartSummary.commerceItem.header.status' /></th>

            <th class="atg_numberValue"><fmt:message key='shoppingCartSummary.commerceItem.header.qty' /></th>
            <th class="atg_numberValue"><fmt:message key='shoppingCartSummary.commerceItem.header.priceEach' /></th>
            <th class="atg_numberValue"><fmt:message key='shoppingCartSummary.commerceItem.header.totalPrice' /></th>
            <th class="atg_numberValue"></th>
          </tr>
        </thead>
        <tbody>
          <%-- Iterate items in the cart --%>
          
            <dsp:include src="/panels/order/finish/finishOrderCartLineItem.jsp" otherContext="${CSRConfigurator.truContextRoot}"
              flush="false">
              <dsp:param name="order" value="${order}" />
                <dsp:param name="commerceItem" param="item"/>
              <dsp:param name="currencyCode"
                value="${order.priceInfo.currencyCode}" />
            </dsp:include>

          <%-- Render Returned Items if in Existing Order View --%>
          <c:if test="${isExistingOrderView}">
            <dsp:droplet name="ReturnDroplet">
              <dsp:param name="orderId" value="${order.id}" />
              <dsp:param name="resultName" value="returns" />
              <dsp:oparam name="error">
                <tr>
                  <td><dsp:valueof param="errorMessage">
                    <fmt:message key="shoppingCartSummary.returnListError" />
                  </dsp:valueof></td>
                </tr>
              </dsp:oparam>
              <dsp:oparam name="output">
                <dsp:getvalueof var="returnList" param="returns" />
                <c:choose>
                  <c:when test="${ not empty returnList }">
                    <%-- Loop Return Requests --%>
                    <c:forEach var="request" items="${returnList}" varStatus="rowCounter">
                      <dsp:tomap var="returnRequestMap" value="${request}" />

                      <dsp:droplet name="/atg/commerce/custsvc/returns/AdminReturnRequestLookup">
                        <dsp:param name="returnRequestId" value="${request.repositoryId}" />
                        <dsp:oparam name="output">

                          <dsp:getvalueof var="returnObject" param="result" />

                          <%-- Loop Items in the Return Request --%>
                          <c:forEach var="returnItem" items="${returnObject.returnItemList}" varStatus="riIndex">
                            <dsp:include src="/panels/order/finish/finishOrderReturnLineItem.jsp" otherContext="${CSRConfigurator.contextRoot}" flush="false">
                              <dsp:param name="returnItem" value="${returnItem}" />
                            </dsp:include>
                          </c:forEach>

                        </dsp:oparam>
                      </dsp:droplet>
                    </c:forEach>
                  </c:when>
                </c:choose>
              </dsp:oparam>
            </dsp:droplet>
          </c:if>

        </tbody>
      </table>
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) pageContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/renderers/order/itemDescription.jsp#1 $$Change: 875535 $--%>
