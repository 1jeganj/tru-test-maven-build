<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:getvalueof var="pageName" param="pageName" />
	<div class="checkout-review-product-block partial">
		<div class="product-information">
			<div class="product-information-header">
				<div class="product-header-title">
					<fmt:message key="checkout.review.toysForTotsDonation" />
				</div>
				<div class="product-header-price">
					<dsp:valueof param="donationItem.donationAmount"
						converter="currency" />
				</div>
			</div>
			<div class="product-information-content">
				<div class="row row-no-padding">
					<div class="col-md-2">
						<div>
							<img class="product-description-image"
								src="../../images/toys-for-tots.jpeg">
						</div>
					</div>
					<div class="col-md-6 donation-amount">
						<dsp:valueof param="donationItem.donationAmount"
							converter="currency" />
					</div>
					<div class="col-md-5 product-information-description">
						<fmt:message key="checkout.review.donationThankYouMessage" />
						<c:if test="${pageName ne 'Confirm'}">
							<div class="edit">
								<%-- edit item in cart --%>
								<dsp:include page="/cart/editItemFrag.jsp">
									<dsp:param name="item" param="donationItem" />
									<dsp:param name="pageName" param="pageName" />
								</dsp:include>
								<span>.</span>
								<%-- <dsp:include page="/cart/removeItemFrag.jsp">
									<dsp:param name="item" param="donationItem" />
									<dsp:param name="pageName" param="pageName" />
									<dsp:param name="donationItem" value="true"/>
								</dsp:include> --%>
								<dsp:getvalueof var="commerceItemId"
									param="donationItem.commerceItemId"></dsp:getvalueof>
								<a href="#"
									onclick="javascript: return removeDonationItemFromReview('${commerceItemId}');"><fmt:message
										key="checkout.review.remove" /></a>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>