<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
 <dsp:getvalueof var="item" param="item" />
 <dsp:getvalueof var="sku" param="sku" />
 <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
<c:if test="${sku.subType == 'DONATION'}">
	<tr class="${((vs.index % 2)==0) ? '' : 'atg_dataTable_altRow'}">
		<c:if test="${isMultiSiteEnabled == true}">
			<c:set var="siteId" value="${item.auxiliaryData.siteId}" />
			<td class="atg_commerce_csr_siteIcon"><csr:siteIcon
					siteId="${siteId}" /></td>
		</c:if>

		<%-- Item Description --%>
		<td><c:set var="cartShareableSite" value="${true}" />
			<ul class="atg_commerce_csr_itemDesc">
				<li>
				<li><fmt:message key="cart.toysFor.tots" bundle="${TRUCustomResources}"/></li>
				</li>
			</ul></td>
		<%-- Inventory status --%>
		<td class="atg_commerce_csr_inventoryStatus"><c:set
				var="locationIdSupplied" value="false" /> <c:choose>
				<c:when test="${envTools.siteAccessControlOn =='true' }">
					<c:set var="siteId" value="${item.auxiliaryData.siteId}" />
					<dsp:droplet name="IsSiteAccessibleDroplet">
						<dsp:param name="siteId" value="${siteId}" />
						<dsp:oparam name="true">
							<td class="atg_commerce_csr_quatity"></br> <c:if
									test="${not empty sku.customerPurchaseLimit}">
        					 limit ${sku.customerPurchaseLimit} items per customer
         					 </c:if></td>
						</dsp:oparam>
						<dsp:oparam name="false">
							<td class="atg_commerce_csr_quatity" />
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
					<td class="atg_commerce_csr_quatity"><input
						value="${item.quantity}" type="hidden" size="3" maxlength="3"
						name="${item.id}" id="${item.id}" class="updateCartItemQty"
						onkeypress="return isNumberOnly(event);" /></br></td>
				</c:otherwise>
			</c:choose> <%-- Price Each --%>
		<td class="atg_numberValue atg_commerce_csr_priceEach">
		<fmt:message key="cart.thank.you" bundle="${TRUCustomResources}"/>
		</td>
		<%-- Total Price --%>
		<td class="atg_numberValue atg_commerce_csr_totalPrice"><c:set
				var="pi" value="${item.priceInfo}" /> <web-ui:formatNumber
				value="${pi.amount}" type="currency" currencyCode="${currencyCode}" /></td>
		<c:choose>
			<c:when test="${envTools.siteAccessControlOn =='true' }">
				<c:set var="siteId" value="${item.auxiliaryData.siteId}" />
				<dsp:droplet name="IsSiteAccessibleDroplet">
					<dsp:param name="siteId" value="${siteId}" />
					<dsp:oparam name="true">
						<td class="atg_iconCell"><a
							class="atg_tableIcon atg_propertyClear"
							title="<fmt:message key='cart.items.delete'/>" href="#"
							onclick="atg.commerce.csr.cart.deleteCartItem('${item.id}');return false;">
								<fmt:message key="cart.items.delete" />
						</a></td>
					</dsp:oparam>
					<dsp:oparam name="false">
						<td class="atg_iconCell" />
					</dsp:oparam>
				</dsp:droplet>
			</c:when>
			<c:otherwise>
				<td class="atg_iconCell"><a
					class="atg_tableIcon atg_propertyClear"
					title="<fmt:message key='cart.items.delete'/>" href="#"
					onclick="atg.commerce.csr.cart.deleteCartItem('${item.id}');return false;">
						<fmt:message key="cart.items.delete" />
				</a></td>
			</c:otherwise>
		</c:choose>

	</tr>
</c:if>
</dsp:page>