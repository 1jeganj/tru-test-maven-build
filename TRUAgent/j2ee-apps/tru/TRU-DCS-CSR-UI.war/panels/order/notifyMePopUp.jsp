<%@ include file="/include/top.jspf"%>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
	<style type="text/css">
  	#successOrErrorMsg{
  	        margin: 10px;
		    float: left;
		    width:99%;
    }
    .notify-me-title
    {
    	margin-bottom: 5px;
    	font-size: 15px;
    }
  </style>
  <dsp:getvalueof param="skuId" var="skuId"></dsp:getvalueof>
  <dsp:getvalueof param="skuDisplayName" var="skuDisplayName"></dsp:getvalueof>
 	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
	<div style="min-height:100px;">
		<span id="successOrErrorMsg" ></span>
		<div id="notifyMe_content">
			<div class="notify-me-title" style="margin-left:10px;font-size: 15px;">notify me when this product is available</div>
			<div style="margin: 17px 10px;">
				<input type="text" id="notifyMeMailId" name="notifyMeMailId" maxlength="50"/>
				<input type="button" value="submit" id="notifyMeMailId_submit" onclick="submitNotifyMeDetails('${skuId}');"/>
				
			</div>
			<div style="margin: 17px 10px;">
				<input type="checkbox" id="notify-checkbox" name="checkbox" style="top: 3px;position: relative;"/><span style="margin-left:5px;">Send Toys "R" Us/Babies "R" Us news and special offers to this email address</span>
				<a href="javascript:void(0);" id="privacyDetails" onclick="showPrivacyDetails();" style="margin-left: 6px;color: #009ddb;text-decoration: underline;">privacy policy</a>
			</div>
		</div>
		<div style="float: right;position: absolute;top: 81px;left: 365px;width: 470px;font-size: 13px;">${skuDisplayName}</div>
	</div>
	</dsp:layeredBundle>
</dsp:page>