<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
 
 	<dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>
  	<dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupDroplet"/>
  	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
   	<dsp:importbean bean="/com/tru/commerce/AddressDoctor"/>
   	<c:set var="formId" value="singleShippingAddressForm"/>
 	<dsp:getvalueof var="formId" param="formId"/>
  	<dsp:getvalueof var="commonShippingGroupTypes" param="commonShippingGroupTypes"/>
  	<dsp:getvalueof var="selectedNickname" param="selectedNickname"/>
  	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

    <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingMethodPS" init="true"/>
    <svc-ui:frameworkUrl var="electronicShippingGroupSuccessURL" panelStacks="cmcBillingPS" init="true"/>

    <dsp:getvalueof var="sgType" param="sgTypeValue"/>
    <dsp:contains var="found" values="${commonShippingGroupTypes}" object="${sgType}"/>
     
    <dsp:getvalueof var="addrKey" param="aKey"/>
     <dsp:getvalueof var="nickName" param="nickName"/>
    <dsp:getvalueof var="pageName" param="pageName"/>
    <dsp:getvalueof var="adre1" param="adre1"/>
    <dsp:getvalueof var="adre2" param="adre2"/>
    <dsp:getvalueof var="city" param="city"/>
    <dsp:getvalueof var="country" param="country"/>
    <dsp:getvalueof var="state" param="state"/>
    <dsp:getvalueof var="postalCode" param="postalCode"/>
    <dsp:getvalueof var="shipId" param="id"/>
    <dsp:getvalueof var="success" param="success"/>
    <dsp:getvalueof var="formId" param="form"/>
    <dsp:getvalueof var="closeId" param="closeId"/>
    <dsp:getvalueof var="firstName" param="firstName"/>
    <dsp:getvalueof var="lastName" param="lastName"/>
    <dsp:getvalueof var="phoneNumber" param="phoneNo"/>
	<dsp:getvalueof var="address" param="sp.shippingAddress"/>
  	
   	<c:set var="closeButtonId" value="atg_commerce_csr_selectShippingAddress"/>     
  	<dsp:importbean bean="/com/tru/commerce/AddressDoctor"/>
  	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	
	 <dsp:getvalueof var="addressBean" param="addressBean"/>
	 	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />

  	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>

    <c:if test="${!found && sgType != 'inStorePickupShippingGroup'}">
      <script type="text/javascript">
        _container_.onLoadDeferred.addCallback(function() {
          //dijit.byId('${shippingGroup.id}').disableButton();
        });
      </script>
    </c:if>
    
	<dsp:getvalueof var="error" bean="/com/tru/commerce/AddressDoctor.taxwareError"/> 
	<c:choose>
	<c:when test="${error}">
		<span style="color:red;margin-bottom:15px;float: left;width: 100%;font-weight: bold;">Oops! Seems there is an issue with address. Please correct.</span>
	</c:when>
	<c:otherwise>
   	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO" />
	<dsp:oparam name="false">
	<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
	<dsp:getvalueof var="derivedStatus" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.derivedStatus" />
	</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
	<dsp:getvalueof var="derivedStatus" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.derivedStatus" />
		
	
	<p id="shipEdit" hidden="true">${derivedStatus}</p>
	<p id="shipEditVo" hidden="true">${addressRecognized}</p>

<c:if test="${pageName != 'shipping'}">
 	<c:choose>
		<c:when test="${not addressRecognized}">
			<p style="margin-left:0px;">
				<fmt:message key="myaccount.address.not.recognised" bundle="${TRUCustomResources}"/>
				<fmt:message key="myaccount.address.choose.existing.address" bundle="${TRUCustomResources}"/>
				<fmt:message key="myaccount.address.enter.new" bundle="${TRUCustomResources}" />
			</p>
		</c:when>
		</c:choose>	
	</c:if>
	
	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO" />
	<dsp:oparam name="false">
	
		<dsp:getvalueof var="container" bean="/atg/commerce/custsvc/order/ShippingGroupContainerService"/>
		
	<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
	<p style="margin-left:0;"><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
		
		
		<c:choose>
		<c:when test="${not addressRecognized}">
			<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-0" value="0"  checked style="float:left;position: relative;top: 2px;margin-right: 6px;">
		</c:when>
		<c:otherwise>
			<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-0" value="0"  style="float:left;position: relative;top: 2px;margin-right: 6px;">
		</c:otherwise>
		</c:choose> 
		
		<c:choose>
		<c:when test="${pageName eq 'edit'}">
			 ${adre1} ${adre2},${city},${country},${state}, ${postalCode},${phoneNumber}
		</c:when>
		<c:otherwise>
		 <p class="address-doctor-address" style="float:left;margin-left:0;">
		 <%--  ${address.address1} ${address.address2},${address.city},${address.country},${address.state}, ${address.postalCode}  --%>
		
 		   ${adre1} ${adre2},${city},${country},${state}, ${postalCode}  
		</p>
		</c:otherwise>
		</c:choose>
		<button onclick="closeModal();return false;" style="margin-top:-6px;"><fmt:message key="myaccount.edit" bundle="${TRUCustomResources}"/></button>		
			<div class="row" style="width:auto;">
						<div class="col-md-2">
							
						</div>
					<!-- 	<div class="col-md-10">
							<p>Did you mean one of the following</p>
						</div> -->
					</div>
		<input name="valid-address" type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
		<input name="valid-address" type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
		<input name="valid-address" type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
		<input name="valid-address" type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
		<input name="valid-address" type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
		<input name="valid-address" type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	
	
	
	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
	<dsp:oparam name="false">
		<div class="address-doctor-alt">
		<span class="address-doctor-msg" style="width: 100%;float: left;" >
			<fmt:message key="myaccount.did.you.mean.one.of.following" bundle="${TRUCustomResources}"/>	</span>
			
						
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
				<dsp:param name="elementName" value="addressDoctorVO" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="size" param="size"/>
					<dsp:getvalueof var="suggestedAddressCount" param="addressDoctorVO.count"/>
					<dsp:getvalueof var="addressDoctorVO" param="addressDoctorVO"/>
					<div style="margin-top:5px;">
					
					 		<c:choose>
						<c:when test="${size == 1}">
							<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}"  checked style="float:left;position: relative;top: 2px;margin-right: 6px;">
						</c:when>
						<c:otherwise>
							<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}" style="float:left;position: relative;top: 2px;margin-right: 6px;">
						</c:otherwise>
					</c:choose> 
					
						<p class="address-doctor-address">
							<dsp:valueof param="addressDoctorVO.fullAddressName" />
						</p>
						<div id="firstSuggestedAddressEdit">
						<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
						<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
						<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
						<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
						<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
						<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="US" />
					</div>
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</div>
	</dsp:oparam >
	</dsp:droplet>
	</dsp:oparam>
	
	</dsp:droplet> 
	
	
	<div class="modal-dialog modal-lg" id="atg_commerce_csr_selectShippingAddress">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
				<div class="pdp-modal1">
				<!--action-btns start-->
					<div class="action-btns warning-btns">
						<c:choose>
						<c:when test="${not addressRecognized}">
							 <!-- <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
						 -->
						<%-- 	<p><fmt:message key="myaccount.use.entered.address" /></p> --%>
							<p class="address-doctor-address">
							<%--  ${adre1} ${adre2},${city},${country},${state}, ${postalCode} --%>
							 
							<input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	 					
							</p>
						</c:when>
						<c:otherwise>
							  <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="hidden" id="suggestedAddressCount-0" value="0"> 
					 	</c:otherwise>
						</c:choose>
				
						
						  <c:choose>
							<c:when test="${not addressRecognized}">
							<!-- <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
							 -->
							<%-- <p><fmt:message key="myaccount.use.entered.address" /></p> --%>
							<p class="address-doctor-address">
							<%--  ${adre1} ${adre2},${city},${country},${state}, ${postalCode} --%>
							 
							 		<input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	 					
							</p>
						</c:when>
						<c:otherwise>
					 	 <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="hidden" id="suggestedAddressCount-0" value="0"> 
						 </c:otherwise>
						</c:choose> 

						<c:choose>
						<c:when test="${pageName eq 'edit'}">
						<c:choose>
						<c:when test="${not addressRecognized}">
						
							<div class="row">
								<div class="col-md-2">
									<button type="button" name="${formId}SaveButton" value="yes"
										class="im-mature-btn"
										onclick="atg.commerce.csr.order.shipping.editShippingAddress('/panels/order/shipping/editHardgoodShippingGroup.jsp');hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});return false;"
										dojoType="atg.widget.validation.SubmitButton" id="${shipId}"><fmt:message key="addressDoctor.save.button" bundle="${TRUCustomResources}"/></button>

							</div>
							</c:when>
							<c:otherwise>
							
							<div class="row">
								<div class="col-md-2">
									<button type="button" name="${formId}SaveButton" value="yes"
										class="im-mature-btn shippingAddBtn editSaveBtn"
										onclick="shipToAddressFromOverlayEdit();"
										dojoType="atg.widget.validation.SubmitButton" id="${shipId}"><fmt:message key="addressDoctor.save.button" bundle="${TRUCustomResources}"/></button>


									<div id="firstSuggestedAddressEdit">
									
											<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
											<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
											<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
											<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
											<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
											<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="${addressDoctorVO.country}" />
									</div>

									<%--     
									     <input type="button"
			                     name="${formId}SaveButton"
			                     action="${success}"
			                     value="<fmt:message key='common.save' />"
			                     onclick="atg.commerce.csr.order.shipping.editShippingAddress('${success}');return false;"
			                     dojoType="atg.widget.validation.SubmitButton"/> --%>
								</div>


								<%-- <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
							</div>
							</c:otherwise>
							</c:choose>
							
						</c:when>
		
						
						<c:when test="${pageName eq 'addAddress'}">

							<input type="button" name="addAddressButton"  id="addAddressButton" class="atg_commerce_csr_activeButton"
		                    onclick="atg.commerce.csr.order.shipping.addShippingAddress();return false;"
		                    value="<fmt:message key="newOrderSingleShipping.addShippingAddress.button.addAddress" bundle="${TRUCustomResources}"/>"
		                	form="${formId}"  dojoType="atg.widget.validation.SubmitButton"/>
	                       <div class="col-md-10">
							<%-- 	<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
							</div>
		                       
						</c:when>

						<c:when test="${pageName eq 'shipping'}">

						<%-- <div class="row">
								<div class="col-md-2">
									<button type="button" value="yes" class="im-mature-btn"
										onclick="atg.commerce.csr.order.shipping.shipToAddress('${addrKey}','${sgType == 'electronicShippingGroup'}');return false;"
										id="${shipId}">Proceed to Shipping Method</button>
										
										
										atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'});
								</div>
								<div class="col-md-10">
									<p>I would like to use entered address</p>
								</div>
							</div> --%>
							<c:choose>
					<c:when test="${error}">
					
					<span style="color:red;margin-bottom:15px;float: left;width: 100%;font-weight: bold;">Sorry, an error occured</span>
							
						</c:when>
						<c:otherwise>
						<div class="row">
							
								<c:choose>
								
								<c:when test="${not addressRecognized }">
								
								
									<div class="col-md-2">
									<%-- <button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}"><fmt:message key="proceed.to.shipping.method" /></button>										
								 --%>	<button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atgNavigate({
                   					 panelStack : 'cmcShippingMethodPS'});hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});"><fmt:message key="proceed.to.shipping.method" bundle="${TRUCustomResources}"/></button>										
									
									</div>
									
								</c:when>
								<c:otherwise>
						 		
										<div class="col-md-2">
										
										<button type="button" value="yes" class="im-mature-btn shippingAddBtn"
											onclick="shipToAddressFromOverlay();"
											id="${shipId}"><fmt:message key="proceed.to.checkout" bundle="${TRUCustomResources}"/></button>
											
									<%-- 		<button type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}">Proceed to Shipping Method</button>																					
								 --%>	</div>
						 
								
								</c:otherwise>
								</c:choose>
						
								<%-- <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
							</div>
				
						</c:otherwise>
						</c:choose>
						</c:when>
						<c:otherwise>
				<%-- 		<div class="row">
								<div class="col-md-2">
									<!-- <button type="button" name="${formId}SaveButton" value="yes"
										class="im-mature-btn"
										onclick="atg.commerce.csr.order.shipping.editShippingAddress('${successErrorURL}');return false;"
										dojoType="atg.widget.validation.SubmitButton" id="${shipId}">yes</button>-->
											   
									     <input type="button"
			                     name="${formId}SaveButton"
			                     action="${successErrorURL}"
			                     value="Save"
			                     onclick="atg.commerce.csr.order.shipping.editShippingAddress('${successErrorURL}');return false;"
			                     dojoType="atg.widget.validation.SubmitButton" class="shippingAddBtn"/> 
								</div>


								<div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div>
							</div>  --%>
							
									<c:choose>
					<c:when test="${error}">
					
					<span style="color:red;margin-bottom:15px;float: left;width: 100%;font-weight: bold;">Sorry, an error occured</span>
							
						</c:when>
						<c:otherwise>
						<div class="row">
							
								<c:choose>
								
								<c:when test="${not addressRecognized }">
								
								
									<div class="col-md-2">
									<%-- <button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}"><fmt:message key="proceed.to.shipping.method" /></button>										
								 --%>	<button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atgNavigate({
                   					 panelStack : 'cmcShippingMethodPS'});hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});"><fmt:message key="proceed.to.shipping.method" bundle="${TRUCustomResources}"/></button>										
									
									</div>
									
								</c:when>
								<c:otherwise>
						 		
										<div class="col-md-2">
										
										<button type="button" value="yes" class="im-mature-btn shippingAddBtn"
											onclick="shipToAddressFromOverlay();"
											id="${shipId}"><fmt:message key="proceed.to.checkout" bundle="${TRUCustomResources}"/></button>
											
									<%-- 		<button type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}">Proceed to Shipping Method</button>																					
								 --%>	</div>
						 
								
								</c:otherwise>
								</c:choose>
						
								<%-- <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
							</div>
				
						</c:otherwise>
						</c:choose>
							
						</c:otherwise>
					</c:choose>

				
				</div>					
			</div>				
		</div>
	</div>
</div>




	
  
    <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingAddressPS" init="true"/>
    <svc-ui:frameworkUrl var="errorURL" panelStacks="cmcShippingAddressPS"/>

	<dsp:form id="shipToAddressFromSuggested" method="POST">
	
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.firstName" name="firstNameAO" id="firstNameAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.lastName" name="lastNameAO" id="lastNameAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address1" name="address1AO" id="address1AO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address2" name="address2AO" id="address2AO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.city" name="cityAO" id="cityAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.state" name="stateAO" id="stateAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.postalCode" name="postalCodeAO" id="postalCodeAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.phoneNumber" name="phoneNumberAO" id="phoneNumberAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.country" name="countryAO" id="countryAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.newShipToAddressName" name="shipToNewAddressAO" id="shipToNewAddressAO"/>
		<input type="hidden" name="shippingMethod" value="${shippingMethod}" />
		<input type="hidden" name="shippingPrice" value="${shippingPrice}" />
		<input type="hidden" name="showMeGiftingOptions" value="${showMeGiftingOptions}" />
		
      <dsp:input type="hidden" priority="-10" value="" bean="ShippingGroupFormHandler.shipToNewAddress"/>
      <dsp:input type="hidden" value="${errorURL}" name="errorURL" bean="ShippingGroupFormHandler.commonErrorURL"/>
      
      <dsp:input type="hidden" value="${successURL}" name="successURL2" id="successURL2" bean="ShippingGroupFormHandler.commonSuccessURL"/>
	</dsp:form>
	
	
	
	
	<script type="text/javascript">
		function closeModal(){
			hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
		};
		
		function shipToAddressFromOverlay(){
			
			var form =  document.getElementById('shipToAddressFromSuggested'); 
			var selctedAddressRadio = $('#csrAddressDoctorFloatingPane input[name=valid-address]:checked');
			//var selctedAddressRadio = $('input[name=valid-address]:checked');
			var count = selctedAddressRadio.val();
			
			
			var firstName = '<c:out value="${firstName}"/>';			
			var lastName = '<c:out value="${lastName}"/>';			
			var phoneNumber = '<c:out value="${phoneNumber}"/>';			
			var selectParent = $(selctedAddressRadio).parent();
			var addres1 = selectParent.find('#suggestedAddress1-'+count).val();
			var addres2 = selectParent.find('#suggestedAddress2-'+count).val();
			var city1 = selectParent.find('#suggestedCity-'+count).val();
			var state1 = selectParent.find('#suggestedState-'+count).val();
			var postalCode1 = selectParent.find('#suggestedPostalCode-'+count).val();
			var country1 = selectParent.find('#suggestedCountry-'+count).val();
			//var country1 = 'US';
			
			//alert(country1);
			var addrKeyValue = '<c:out value="${nickName}"/>';
            //alert(addrKeyValue);
            //alert(phoneNumber);
            //alert(firstName);
			form.firstNameAO.value=firstName;
			form.lastNameAO.value=lastName;
			form.phoneNumberAO.value=phoneNumber;
			form.address1AO.value=addres1;
			form.address2AO.value=addres2;
			form.cityAO.value=city1;
			form.stateAO.value=state1;
			form.postalCodeAO.value=postalCode1;
			form.countryAO.value=country1; 
			form.shipToNewAddressAO.value=addrKeyValue;
			
			var checkValue = $('.select-valid-address:checked').length;
			var checkedRadioAddress = $('.select-valid-address:checked').attr('id');
			
			//alert("checkValue"+checkValue);
			if(count == 0){
				atg.commerce.csr.order.shipping.addShippingAddress();
				
				atgNavigate({panelStack : 'cmcShippingMethodPS'});
				hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
				return false;
				//atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'}, addrKeyValue);
			}else{
				if(checkedRadioAddress == 'suggestedAddressCount-0')
					{
						//atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'}, addrKeyValue);
					atgNavigate({panelStack : 'cmcShippingMethodPS'});
					}
				else
					{
						atgSubmitAction({form:dojo.byId(form)});
					}
			}
			
			
			hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			
		}
		function checkRadioBtnChecked()
		{
			setTimeout(function(){
				checkRadioBtn();
			},200);
		}
		function checkRadioBtn()
		{
			var selected = $("#csrAddressDoctorFloatingPane").find("input[type='radio']:checked").length;
			
			if(selected == 0)
				{
					$(".shippingAddBtn,.editSaveBtn").attr('disabled',true);
				}
			else
				{
					$(".shippingAddBtn,.editSaveBtn").attr('disabled',false);
				}
		}
		$(document).ready(function(){
			checkRadioBtn();
			$(".select-valid-address").on("click",function(){
				if($(this).is(":checked"))
					{
						$(".shippingAddBtn").attr('disabled',false);
					}
			})
		})
		
		function shipToAddressFromOverlayEdit(){
			
					
			var form =  document.getElementById('csrEditShippingAddressForm'); 
			
			var selctedAddressRadio = $('#csrAddressDoctorFloatingPane input[name=valid-address]:checked');
			var count = selctedAddressRadio.val();
			
			
			var firstName = '<c:out value="${firstName}"/>';
			var lastName = '<c:out value="${lastName}"/>';			
			var phoneNumber = '<c:out value="${phoneNumber}"/>';			
			var selectParent = $(selctedAddressRadio).parent();
			var addres1 = selectParent.find('#suggestedAddress1-'+count).val();
			var addres2 = selectParent.find('#suggestedAddress2-'+count).val();
			var city1 = selectParent.find('#suggestedCity-'+count).val();
			var state1 = selectParent.find('#suggestedState-'+count).val();
			var postalCode1 = selectParent.find('#suggestedPostalCode-'+count).val();
			var country1 = selectParent.find('#suggestedCountry-'+count).val();
			//alert(country1);
			//var country1 = 'US';
			var addrKeyValue = '<c:out value="${nickName}"/>';
            //alert(addrKeyValue);
		 	form.csrEditShippingAddressForm_firstName.value=firstName;
			form.csrEditShippingAddressForm_lastName.value=lastName;
			form.csrEditShippingAddressForm_phoneNumber.value=phoneNumber;
			form.csrEditShippingAddressForm_address1.value=addres1;
			form.csrEditShippingAddressForm_city.value=city1;
			form.csrEditShippingAddressForm_state.value=state1;
			form.csrEditShippingAddressForm_postalCode.value=postalCode1;
			form.csrEditShippingAddressForm_country.value=country1; 
			form.csrEditShippingAddressForm_nickName.value=addrKeyValue;
			
			
			var checkedRadioAddress = $('.select-valid-address:checked').attr('id');
			
			var checkValue = $('.select-valid-address:checked').length;
			
			
			if(count >= 1){
				
				shipToAddressFromOverlay()
				/* atgNavigate({panelStack : 'cmcShippingMethodPS'});
				hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'}); */
				
				//atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'}, addrKeyValue);
			}
			else if(checkedRadioAddress == 'suggestedAddressCount-0'){
				//alert("5");
				
				atg.commerce.csr.order.shipping.editShippingAddress('/TRU-DCS-CSR/panels/order/shipping/editHardgoodShippingGroup.jsp');
				 hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			}
			else{
				//atgSubmitAction({form:dojo.byId(form)});
				 atg.commerce.csr.common.submitPopup('/TRU-DCS-CSR/panels/order/shipping/editHardgoodShippingGroup.jsp', dojo.byId("csrEditShippingAddressForm"), dijit.byId("csrEditAddressFloatingPane"));
				 hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			}
			
			
			hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			
		}
		
	</script>
	</c:otherwise>
	</c:choose>
</dsp:page>