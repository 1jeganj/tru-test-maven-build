<%--
This page defines the address view
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/shippingGroupMethodView.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<style>
	.csc-gift-elegible-items,.hide {
		display: none;
	}
	.elegible-gift-item-wrap-container {
		display: none;
	}
	.elegible-gift-item-container {
		margin-bottom: 15px;	
	}
	.gift-wrap-container {
		display: inline-block;
		margin: 15px 25px 0 0;
		vertical-align: top;
		
	}
</style>
<script type="text/javascript">

$(document).on('click', '.csc-gift-options-checkbox', function(){
	var showMeGiftingOptions = $(this).is(':checked');
	$('#ShowMeGiftingOptions').val(showMeGiftingOptions);
	atgSubmitAction({form : document.getElementById('giftOptionsFormCSC')})
	if(showMeGiftingOptions){
		$(this).parent().find('.csc-gift-elegible-items').show();
	}else{
		$(this).parent().find('.csc-gift-elegible-items').hide();
	}
	
});

$(document).on('click', '.elegible-gift-item', function(){
	
	var showEligibleGiftItemWrap = $(this).is(':checked');
	try{
		var id = $(this).data('id');

		$('.elegible-gift-item-wrap-container.' + id + ' input').removeAttr('checked');
		
		if(showEligibleGiftItemWrap){
			$('.elegible-gift-item-wrap-container.' + id).show();
		}else{
			$('.elegible-gift-item-wrap-container.' + id).hide();
		}	
	}catch(e){}
	
});



$(document).ready(function(){
	var giftWrapSkuId = $("#giftWrapSkuId").val();
	if(giftWrapSkuId != null || giftWrapSkuId != '')
		{
			 $("#"+giftWrapSkuId).attr("checked","checked");
		}
	$(document).on('click','.giftWrapMessageFlag',function(){
		var checkFlag = $(this).is(":checked");
		if(checkFlag == false || checkFlag == 'false')
			{
				$(this).closest(".gifting-items-shipping-to-content").find('textarea[name="giftWrapMessageText"]').val("");
			}
	});
})
var maxLength = parseInt($('#giftMsgMaxLength').val());
$(document).on('change keyup','textarea[name="giftWrapMessageText"]',function(){
	var msg = $(this).val();
	var msgLength = parseInt(msg.length);
	if(msg != "" || msg != null){
		$(this).closest(".gifting-items-shipping-to-content").find(".giftWrapMessageFlag").attr("checked",true);
	}
	var charLeft = maxLength - msgLength;
	$(this).parent().find(".giftMsgLength").html(charLeft);
}); 
$(document).ready(function(){
	 $('textarea[name="giftWrapMessageText"]').each(function(){
		 var giftWrapMsg = $(this).val();
		 var msgLength = parseInt(giftWrapMsg.length);
		 if($.trim(giftWrapMsg) != '')
			{ 
				$(this).parent().parent().find(".giftWrapMessageFlag").attr("checked",true);
				var charLeft = maxLength - msgLength;
				$(this).parent().find(".giftMsgLength").html(charLeft);
			}
		 
	 });
})


function updateGiftItemOnShipping(obj,shipGroupId, commerceItemRelationshipId, commerceItemId, giftWrapProductId, giftWrapSkuId){
	
		var giftItem = $(obj).is(':checked');
		
		/*alert(giftItem);gift-wrap-container-wrapper*/
		$("#ShipGroupId").val(shipGroupId);
		$("#CommerceItemRelationshipId").val(commerceItemRelationshipId);
		$("#CommerceItemId").val(commerceItemId);
		
		$("#GiftWrapProductId").val(giftWrapProductId);
		$("#GiftWrapSkuId").val(giftWrapSkuId);
		$("#SelectionId").val(giftWrapSkuId);
		
		$("#GiftItem").val(giftItem);
		/*alert(CommerceItemRelationshipId);*/
		atgSubmitAction({form : document.getElementById('giftItemFormOnShipping')});
		if(giftItem == true)
			{
				$(obj).closest('.checkout-gifting-registry-product').find('.gift-wrap-container-wrapper').show();
			}
		if(giftItem == false)
		{
			$(obj).closest('.checkout-gifting-registry-product').find('.gift-wrap-container-wrapper').hide();
		}
	} 
	
</script>

  <dsp:page xml="true">
    <dsp:importbean var="pageFragment" bean="/atg/commerce/custsvc/ui/fragments/order/DisplayHardgoodShippingGroup"/>
    <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/com/tru/commerce/order/droplet/GiftOptonsDetailsDroplet"/>
	<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" />
    <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
    <dsp:getvalueof var="order" param="order"/>
      <dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
      <dsp:importbean bean="/com/tru/commerce/order/droplet/ShowMeGiftingOptionsDroplet" />
    <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
    

     
      
     <dsp:getvalueof var="showMeGiftingOptions" bean="ShoppingCart.current.showMeGiftingOptions" />
      <c:if test="${not showMeGiftingOptions}">
        
  		<dsp:droplet name="ShowMeGiftingOptionsDroplet">
			<dsp:param bean="ShoppingCart.current" name="order" />
			<dsp:oparam name="output">
				 <dsp:getvalueof var="showMeGiftingOptions" param="isOrderHasGiftWrappedItem"/>
			</dsp:oparam>
		</dsp:droplet>	 
	</c:if> 

      
      <div class="atg_commerce_csr_addressView">
      <h4>
            <fmt:message key="shoppingmethod.gifting.label" bundle="${TRUCustomResources}"/>
      </h4>
 
      <br>
   
   
   	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="order.shippingGroups" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="shipGroupId" param="element.id" />
				<dsp:getvalueof var="nickName" param="element.nickName" />
				
				<dsp:getvalueof var="commerceItemRelationships" param="element.commerceItemRelationships"/>
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" value="${commerceItemRelationships}" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="commerceItemRelationshipId" param="element.id"/>
						<dsp:getvalueof var="isGiftItem" param="element.isGiftItem"></dsp:getvalueof>
						<dsp:getvalueof var="commerceItemId" param="element.commerceItem.id"></dsp:getvalueof>
					</dsp:oparam>
				</dsp:droplet>
				</dsp:oparam>
		</dsp:droplet>
    <c:set var="style" value="display: none;"/>
    
   <c:choose>
   		<c:when test="${showMeGiftingOptions}">
   				<c:set var="style" value="display: block;"/>
   				<input type="checkbox" name="gift-options-checkbox" checked class="csc-gift-options-checkbox" style="display: block;" /> <fmt:message key="shoppingmethod.showme.label" bundle="${TRUCustomResources}"/><br>
   		</c:when>
   		<c:otherwise>
   				<input type="checkbox" name="gift-options-checkbox" class="csc-gift-options-checkbox" /> <fmt:message key="shoppingmethod.showme.label" bundle="${TRUCustomResources}"/><br>
   		</c:otherwise>
   </c:choose>
      <%--  ${order.commerceItems} --%>
       
     <%--   <dsp:include page="giftOptionsFragment.jsp" /> --%>
     
    
       <p><fmt:message key="shoppingmethod.gifting.message" bundle="${TRUCustomResources}"/></p>
      	<div class="csc-gift-elegible-items" style="${style}">
	<%-- 	 <c:forEach items="${order.commerceItems}" var="item" varStatus="vs">
            <dsp:tomap var="sku" value="${item.auxiliaryData.catalogRef}" />
            <dsp:tomap var="product" value="${item.auxiliaryData.productRef}" /> --%>
            	<div class="elegible-gift-item-container">
		              <%-- <div class="atg_commerce_csr_itemDesc"><input type="checkbox" name="elegible-gift-item-${sku.id}" class="elegible-gift-item" data-id="${sku.id}" /> ${sku.displayName}</div> --%>
		         <%--      <div class="atg_commerce_csr_quatity">Qty:${item.quantity}</div>
		              
		              <div class="elegible-gift-item-wrap-container ${sku.id}">
		              		<dsp:textarea bean="GiftOptionsFormHandler.applyGiftMessage" rows="10" cols="10"></dsp:textarea>
		              		<input type="radio" name="elegible-gift-item-wrap-${sku.id}" /> TRU
		              		<input type="radio" name="elegible-gift-item-wrap-${sku.id}" /> BRU
		              		<input type="radio" name="elegible-gift-item-wrap-${sku.id}" /> None
		              </div> --%>
		              
		        	
 		              
		        <dsp:droplet name="GiftOptonsDetailsDroplet">
							<dsp:param name="order" bean="ShoppingCart.current" />
							<dsp:oparam name="output">
								<dsp:setvalue bean="GiftOptionsFormHandler.giftOptionsInfoCount" paramvalue="giftOptionsInfoCount" />
					<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfoCount" priority="1000" paramvalue="giftOptionsInfoCount" />
					<dsp:getvalueof var="giftOptionsDetails" param="giftOptionsDetails"/>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="giftOptionsDetails" />
						<dsp:param name="elementName" value="giftOptionInfo" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="shippingAdressNickName" param="key"/>
							<dsp:getvalueof var="giftOptionId" param="giftOptionInfo.giftOptionId"/>
							<dsp:getvalueof var="index" param="index" />
							 <dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftOptionId" id="${giftOptionId}_giftOptionId1" default="${giftOptionId}" value="${giftOptionId}"/>
													    <%--<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].shippingAdressNickName" id="${giftOptionId}_shippingGroupId1" default="${shippingAdressNickName}" value="${shippingAdressNickName}"/> --%>
							   <c:if test="${shippingGroup.id eq giftOptionId || shippingAdressNickName eq shippingGroup.nickName}">
							  <div class="gifting-items-shipping-to">
							   
								 <div class="gifting-items-shipping-to-content" id="${shippingGroup.id}-gifting-items" >
								   <div class="row">
								      <%--  <dsp:include page="displayShippingAddress.jsp">
								       		<dsp:param name="shippingAddress" param="giftOptionInfo.shippingAddress"/>
								       		<dsp:param name="isRegistryAddress" param="giftOptionInfo.registryAddress"/>
								       </dsp:include>--%>
								     
								       <input type= "checkbox" id="giftWrapMessageFlag" class="giftWrapMessageFlag" >
								        <dsp:include page="giftNote.jsp">
									   		<dsp:param name="index" value="${index}"/>
									   		<dsp:param name="giftOptionInfo" param="giftOptionInfo"/>
									   		<dsp:param name="giftOptionId" value="${giftOptionId}"/>
									   		<dsp:param name="shipId" value="${shippingGroup.id}"/>
									   </dsp:include> 
								   </div>
									<div class="row">
										<div class="col-md-5" id="co-gift-options-item-checkbox">
											<dsp:droplet name="Switch">
												<dsp:param name="value" param="giftOptionInfo.giftReceipt" />
												<dsp:oparam name="true">
													<dsp:input type="hidden" id="${giftOptionId}_giftReceipt" iclass="giftReceipt" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftReceipt" value="true" name="giftReceipt"/>
												</dsp:oparam>
												<dsp:oparam name="false">
													<dsp:input type="hidden" id="${giftOptionId}_giftReceipt"  iclass="giftReceipt" bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftReceipt" value="false" name="giftReceipt"/>
												</dsp:oparam>
											</dsp:droplet>
										
										</div>
									</div>
								 </div>
							  </div>
							  <br>
							<dsp:getvalueof var="giftWrappedItems" param="giftOptionInfo.giftWrappedItems"/>
							<dsp:getvalueof var="eligibleGiftItems" param="giftOptionInfo.eligibleGiftItems"/>
							<dsp:getvalueof var="nonEligibleGiftItems" param="giftOptionInfo.nonEligibleGiftItems"/>
						
						
								<c:if test="${giftWrappedItems.size() > 0}">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" value="${giftWrappedItems}" />
										<dsp:param name="elementName" value="giftWrappedItem" />
										<dsp:param name="sortProperties" value="productDisplayName, wrapSkuId"/>
										<dsp:oparam name="output">
											<dsp:valueof param="giftWrappedItem.productDisplayName" />
											<dsp:droplet name="IsEmpty">
												<dsp:param name="value" param="giftWrappedItem.productColor" />
												<dsp:oparam name="false">
													<p>
														Color:
														<dsp:valueof param="giftWrappedItem.productColor" />
													</p>
												</dsp:oparam>
											</dsp:droplet>
											<dsp:droplet name="IsEmpty">
												<dsp:param name="value" param="giftWrappedItem.productSize" />
												<dsp:oparam name="false">
													<p>
														Size:
														<dsp:valueof param="giftWrappedItem.productSize" />
													</p>
												</dsp:oparam>
											</dsp:droplet>
											<br>
					        			QTY: <dsp:valueof param="giftWrappedItem.quantity" />
											</p>
											<div class="gift-wrap-container-wrapper">
												<dsp:include page="giftWrap.jsp">
													<dsp:param name="giftWrapItem" param="giftWrappedItem" />
													<dsp:param name="key" param="key"/>
												</dsp:include>
											</div>


										</dsp:oparam>
										
									</dsp:droplet>
									<%-- <dsp:include page="giftWrappedItems.jsp">
									<dsp:param name="giftWrappedItems" param="giftOptionInfo.giftWrappedItems"/>
								</dsp:include>  --%>
								</c:if>
								<c:if test="${eligibleGiftItems.size() > 0}">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" value="${eligibleGiftItems}" />
										<dsp:param name="elementName" value="eligibleGiftItem" />
										<dsp:param name="sortProperties" value="productDisplayName"/>
										<dsp:oparam name="output">
												<div class="checkout-gifting-registry-product"
													id='<dsp:valueof param="key"/>' style="float: left;width: 100%;margin-bottom: 15px;">
													<span style="float: left;width: 100%;"><dsp:valueof param="eligibleGiftItem.productDisplayName" />
														QTY:<dsp:valueof param="eligibleGiftItem.quantity" />
													</span>
													<div class="atg_commerce_csr_itemDesc" style="float: left;width: 100%;">
														<input type="checkbox" name="elegible-gift-item-${sku.id}"
															class="elegible-gift-item" data-id="${sku.id}" onclick="javascript:updateGiftItemOnShipping(this,
						            							'<dsp:valueof param="eligibleGiftItem.shippingGroupId"/>', 
						            							'<dsp:valueof param="eligibleGiftItem.commItemRelationshipId"/>',
						            							 '<dsp:valueof param="eligibleGiftItem.parentCommerceId"/>',
						            							  '<dsp:valueof param="key"/>',
				            							 			'<dsp:valueof param="key"/>' );"/>
															
										<fmt:message key="shoppingcart.gift.with.label" bundle="${TRUCustomResources}">
												<fmt:param value="${giftWrapName}"></fmt:param>
										</fmt:message>
													</div>
												
													<div class="gift-wrap-container-wrapper" style="display:none;">
													 <dsp:include page="giftWrap.jsp">
									 						<dsp:param name="giftWrapItem" param="eligibleGiftItem"/>
									 						<dsp:param name="key" param="key"/>
									 				</dsp:include> 
									 				</div>
													<%-- 	<div class="checkbox-sm-off add-gift-wrap-checkbox"  tabindex="0" onclick="updateGiftItem(this,
				            							'<dsp:valueof param="eligibleGiftItem.shippingGroupId"/>', 
				            							'<dsp:valueof param="eligibleGiftItem.commItemRelationshipId"/>',
				            							 '<dsp:valueof param="eligibleGiftItem.parentCommerceId"/>',
				            							 '<dsp:valueof param="key"/>',
				            							 '<dsp:valueof param="eligibleGiftItem.parentCommerceId"/>_main' );">
				            		</div> --%>
												</div>
											

										</dsp:oparam>
									</dsp:droplet>
									<%-- 	 <dsp:include page="giftWrapEligibleItems.jsp">
									<dsp:param name="eligibleGiftItems" param="giftOptionInfo.eligibleGiftItems"/>
								</dsp:include> --%>
								</c:if>
								<c:if test="${nonEligibleGiftItems.size() > 0}">
									<dsp:droplet name="ForEach">
										<dsp:param name="array" value="${nonEligibleGiftItems}" />
										<dsp:param name="elementName" value="nonEligibleGiftItem" />
										<dsp:param name="sortProperties" value="productDisplayName" />
										<dsp:oparam name="output">
											<dsp:valueof
													param="nonEligibleGiftItem.productDisplayName" />
											<br>
											QTY: <dsp:valueof
												param="nonEligibleGiftItem.quantity" /><br>
										</dsp:oparam>
									</dsp:droplet>
									<%-- 	<dsp:include page="giftWrapNonEligibleItems.jsp">
									<dsp:param name="nonEligibleGiftItems" param="giftOptionInfo.nonEligibleGiftItems"/>
								</dsp:include> --%>
								</c:if>
							</c:if>	
							</dsp:oparam>
					</dsp:droplet>
					</dsp:oparam>
							</dsp:droplet>
				</div>
     <%--       </c:forEach>   --%>

      	</div>
      </div><%-- ${shippingGroup.commerceItemRelationships} --%>
            		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="order.commerceItems" />
					<dsp:oparam name="output">
					
					<dsp:getvalueof var="nickName" param="element.id"/>
					
					</dsp:oparam></dsp:droplet>
      
      <div class="atg_commerce_csr_addressView">
      <h4>
        <dsp:include src="${pageFragment.URL}" otherContext="${pageFragment.servletContext}">
          <dsp:param name="shippingGroup" value="${shippingGroup}"/>
          <dsp:param name="propertyName" value="value1"/>
          <dsp:param name="displayHeading" value="${true}"/>
        </dsp:include>
      </h4>
      <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
        <dsp:include src="${pageFragment.URL}" otherContext="${pageFragment.servletContext}">
          <dsp:param name="shippingGroup" value="${shippingGroup}"/>
          <dsp:param name="propertyName" value="value1"/>
          <dsp:param name="displayValue" value="${true}"/>
        </dsp:include>
      </ul>
      </div>
    </dsp:layeredBundle>
  </dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/shippingGroupMethodView.jsp#1 $$Change: 875535 $--%>
