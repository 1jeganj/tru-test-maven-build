<%@ include file="/include/top.jspf"%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
<dsp:getvalueof var="giftWrapMessage" param="giftOptionInfo.giftWrapMessage"/>
<dsp:getvalueof var="giftOptionId" param="giftOptionId" />
<dsp:getvalueof var="shipId" param="shipId" />

<dsp:getvalueof var="index" param="index" />
	<div class="col-md-8">
	
	
	
		<%-- <div id="shipGroupId" class="hide giftShipGroupId"><dsp:valueof param="giftOptionId"/></div> --%>
		<input type="hidden" id="${shipId}_giftShipGroupId" value="${shipId}"/>
		include a free gift message. (<span class="giftMsgLength">180</span> characters remaining)
		<input type='hidden' id='giftMsgMaxLength' value='180'/>
		<dsp:textarea id="${shipId}_giftWrapMessage"  
			bean="GiftOptionsFormHandler.giftOptionsInfo[${index}].giftWrapMessage" name="giftWrapMessageText" maxlength="180">${giftWrapMessage}</dsp:textarea>
			<input type="button" class="saveGiftMsg_btn" value="Save Message" onclick="return applyGiftMessage('${shipId}');"/>
	</div>
</dsp:page>
