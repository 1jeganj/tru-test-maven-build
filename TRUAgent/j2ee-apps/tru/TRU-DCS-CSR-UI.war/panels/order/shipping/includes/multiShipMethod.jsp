<%@ include file="/include/top.jspf" %>

<dsp:page xml="true">
<style>
	.shippingMethodDetail_content{
			position: absolute;
		    background: #fff;
		    width: 135px;
		    padding: 10px;
		    border: 1px solid rgba(0,0,0,0.2);
		    display: none;
		    border-radius: 5px;
		    margin-top: 10px;
   			margin-left: -10px;
		}
		 .shippingMethodDetail_content .arrow
		    {
		    position: absolute;
		    top: -12px;
		    left: 14px;
		    }
</style>
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
    <dsp:importbean var="order" bean="/atg/commerce/custsvc/order/ShoppingCart"/>
<dsp:getvalueof var="shipmentVO" param="shipmentVO" />
<dsp:getvalueof var="item" param="cartItemDetailVO" />
<dsp:getvalueof var="shippingGroup" param="shippingGroup" />


 <c:forEach items="${shippingGroup.commerceItemRelationships}" var="ciRelationship">
 <c:set var="rel" value="${ciRelationship.id}"/>
  <c:if test="${rel eq item.id}">
  <strong>For SKU:${item.skuId} with quantity 1</strong></br>
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="shipmentVO.shipMethodVOs" />
				<dsp:param name="elementName" value="shipMethodVO" />
				<dsp:oparam name="output">
				    <dsp:getvalueof var="shipMethodName" param="shipMethodVO.shipMethodName" />
					<dsp:getvalueof var="shipMethodCode" param="shipMethodVO.shipMethodCode" />
					<dsp:getvalueof var="shippingPrice" param="shipMethodVO.shippingPrice"/>
					<dsp:getvalueof var="regionCode" param="shipMethodVO.regionCode"/>
					<dsp:getvalueof var="delivertTimeLine" param="shipMethodVO.delivertTimeLine"/>
					<dsp:getvalueof var="details" param="shipMethodVO.details"/>
			     	<dsp:getvalueof var="shipMethodSize" param="size" />
			     	
			     <div class="select-shipping-method-block <c:if test='${shipMethodCode eq defaultShippingMethod}'></c:if>">
								<c:choose>
									<c:when test="${item.selectedShippingMethod  eq shipMethodCode or shipMethodSize eq 1}">
										<input type="checkbox" class="shipping-method-checkbox" onchange="javascript:updateShippingMethod(this, '${item.shipGroupId}', '${item.id}', '${item.uniqueId}', '${item.wrapCommerceItemId}','${shipMethodCode}','${shippingPrice}','${regionCode}')"  checked="checked"/>	
									</c:when>						
									<c:otherwise>
										<input type="checkbox" class="shipping-method-checkbox" onchange="javascript:updateShippingMethod(this, '${item.shipGroupId}', '${item.id}', '${item.uniqueId}', '${item.wrapCommerceItemId}','${shipMethodCode}','${shippingPrice}','${regionCode}')" />
									</c:otherwise>
								</c:choose>
								<p class="shipping-type-header">
									${shipMethodName}
								</p>
								<p>
									${delivertTimeLine}
								</p>
								<p class="co-shipping-method-price">
											<c:choose>
		                                		<c:when test="${shippingPrice == 0.0}">
		                                			<fmt:message key="shoppingcart.free" bundle="${TRUCustomResources}"/>
		                                		</c:when>
		                                		<c:otherwise>
		                                			<dsp:valueof format="currency" value="${shippingPrice}" locale="en_US" converter="currency"/>
		                                		</c:otherwise>
		                                	</c:choose>
										
								</p>
								<c:choose>
								<c:when test="${shipMethodCode eq 'TRU_Std_Gnd' || shipMethodCode eq 'GROUND' || shipMethodCode eq 'ANY_2Day' || shipMethodCode eq '1DAY' || shipMethodCode eq 'USPS_PRIORITY' || shipMethodCode eq 'TRUCK'}">
										<dsp:getvalueof var="shipMethodCodePopover" value="one${fn:substring(shipMethodCode,1,fn:length(shipMethodCode))}" />
									</c:when>
									<c:otherwise>
										<dsp:getvalueof var="shipMethodCodePopover" value="${shipMethodCode}" />
									</c:otherwise>
								</c:choose>
								<a href="javascript:void(0);" id="${shipMethodName}" class="shippingMethodDetails">details</a>
								<div class='shippingMethodDetail_content ${shipMethodName}'>
									<img src="/TRU-DCS-CSR/images/up-arrow.png" class="arrow"/>
									<span class="hidden_shipping_details">${details}</span>
								</div>
							</div>	 	
				</dsp:oparam>
				<dsp:oparam name="empty">
					No Shipmethods found
				</dsp:oparam>
			</dsp:droplet>
			</c:if>
			
			 </c:forEach>
			
	</dsp:layeredBundle>
</dsp:page>