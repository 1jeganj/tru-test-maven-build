
<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">

.  <dsp:importbean
    bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>

  <svc-ui:frameworkUrl var="completeOrderPageURL"
                       panelStacks="cmcCompleteOrderPS"
                      />
  <svc-ui:frameworkUrl var="refundMethodsPageURL"
                       panelStacks="cmcRefundTypePS"
                      />
  <svc-ui:frameworkUrl var="successURL" panelStacks="cmcBillingPS"
                       init="true"
                      />
  <svc-ui:frameworkUrl var="shippingMethodURL"
                       panelStacks="cmcShippingMethodPS" init="true"
                      />

  <dsp:input id="successURLInput" type="hidden" value="${successURL}"
             bean="ShippingGroupFormHandler.applyShippingMethodsSuccessURL"/>

  <dsp:input type="hidden" value="${completeOrderPageURL }"
             name="completeOrderPageURL"
             bean="ShippingGroupFormHandler.completeOrderPageURL"/>

  <dsp:input type="hidden" value="${refundMethodsPageURL }"
             name="refundMethodsPageURL"
             bean="ShippingGroupFormHandler.refundMethodsPageURL"/>

  <dsp:input type="hidden" value="${shippingMethodURL }"
             name="shippingMethodURL"
             bean="ShippingGroupFormHandler.shippingMethodURL"/>

</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/shippingMethodNextStep.jsp#1 $$Change: 875535 $--%>

