<%@ include file="/include/top.jspf"%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean
		bean="/atg/registry/RepositoryTargeters/TRU/Checkout/GiftWrapItems" />
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
	<dsp:importbean bean="/atg/targeting/TargetingFirst" />
	<dsp:importbean bean="/atg/targeting/TargetingForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="previousGiftWrapProductId" param="giftWrapItem.wrapProductId" />
	<dsp:getvalueof var="previousGiftWrapSkuId" param="giftWrapItem.wrapSkuId" />
	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
	<dsp:getvalueof var="index" param="index" />

	<dsp:droplet name="TargetingFirst">
		<dsp:param name="targeter" bean="GiftWrapItems" />
		<dsp:param name="elementName" value="targetedProduct" />
		<dsp:oparam name="output">
		<dsp:droplet name="ForEach">
				<dsp:param name="array" param="targetedProduct.childSkus" />
				<dsp:param name="elementName" value="targetedSku" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="targetedSkuType" param="targetedSku.type" />
					<dsp:getvalueof var="targetedSkuSubType" param="targetedSku.subType" />
					<c:if test="${targetedSkuSubType eq 'TRUGiftWrap' or targetedSkuSubType eq 'BRUGiftWrap'}" >
			<dsp:getvalueof var="index" param="index" />
			<c:if test="${index eq 0}">
				<dsp:getvalueof var="firstWrapProductId"
					param="targetedProduct.repositoryId" />
				<dsp:getvalueof var="firstWrapSkuId"
					param="targetedSku.repositoryId" />
				<c:choose>
					<c:when test="${empty previousGiftWrapSkuId}">
						<dsp:getvalueof var="defaultWrapProductId"
							value="${firstWrapProductId}" />
						<dsp:getvalueof var="defaultWrapSkuId" value="${firstWrapSkuId}" />
					</c:when>
					<c:otherwise>
						<dsp:getvalueof var="defaultWrapProductId"
							value="${previousGiftWrapProductId}" />
						<dsp:getvalueof var="defaultWrapSkuId"
							value="${previousGiftWrapSkuId}" />
					</c:otherwise>
				</c:choose>
				<div id="gift-wrap-price">
					<dsp:include page="giftWrapPrice.jsp">
						<dsp:param name="defaultWrapProductId"
							value="${defaultWrapProductId}" />
						<dsp:param name="defaultWrapSkuId" value="${defaultWrapSkuId}" />
					</dsp:include>
				</div>
			</c:if>
				<dsp:getvalueof var="giftWrapImage" param="targetedSku.primaryImage" />
			<!-- <div class="gift-wrap-tru-block inline"> -->
			<div class="gift-wrap-container">
				<dsp:getvalueof var="wrapSkuId"
					param="targetedSku.repositoryId" />
				<dsp:getvalueof var="key2" param="key" />
				<c:choose>
					<c:when test="${previousGiftWrapSkuId eq wrapSkuId}">

						<input type="radio" checked="checked"
							class="wrapSkuId prevSelected"
							data-id="<dsp:valueof param="targetedSku.repositoryId"/>"
							name="elegible-gift-item-wrap-<dsp:valueof param="key"/>"
							onclick="javascript:handleRadioButtonClick(this,'<dsp:valueof param="giftWrapItem.shippingGroupId"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.commItemRelationshipId"/>',
                                                                           '<dsp:valueof param="key"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.parentCommerceId"/>',
                                                                           '<dsp:valueof param="targetedProduct.repositoryId"/>' ,
                                                                           '<dsp:valueof param="targetedSku.repositoryId"/>',
                                                                           '<dsp:valueof param="targetedProduct.repositoryId"/>' ,
                                                                           '<dsp:valueof param="targetedSku.repositoryId"/>','true')">
						<div class="gift-wrap-check" style="display: block"></div>
						<p>
							<dsp:valueof param="targetedSku.displayName" />
						</p>
						
						
						
						
						
							<div class="gift-wrap-border border-visible" id='<dsp:valueof param="targetedSku.repositoryId"/>' >
										<div class="gift-wrap-opaque">
											<div class="gift-wrap-tru">
												<dsp:droplet name="IsEmpty">
													<dsp:param name="value" value="${giftWrapImage}" />
													<dsp:oparam name="false">
													    <c:if test = "${fn:startsWith(giftWrapImage, '/images')}">
															<c:set var="giftWrapImage" value="/TRU-DCS-CSR${giftWrapImage}"/>
														</c:if>
														<img src="${giftWrapImage}" alt="gift wrap"  width="140" height="56"/>
													</dsp:oparam>
													<dsp:oparam name="true">
														<img src="${CSRConfigurator.truContextRoot}/images/no-image500.gif" alt="gift wrap"  width="140" height="56"/>
													</dsp:oparam>
												</dsp:droplet>
											</div>
										</div>
									</div>
							
						
						
						
						
						
						
						
						
					</c:when>
					<c:otherwise>

						<input type="radio" class="wrapSkuId"
							data-id="<dsp:valueof param="targetedSku.repositoryId"/>"
							name="elegible-gift-item-wrap-<dsp:valueof param="key"/>"
							onclick="javascript:handleRadioButtonClick(this,'<dsp:valueof param="giftWrapItem.shippingGroupId"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.commItemRelationshipId"/>',
                                                                           '<dsp:valueof param="key"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.parentCommerceId"/>',
                                                                           '<dsp:valueof param="targetedProduct.repositoryId"/>' ,
                                                                           '<dsp:valueof param="targetedSku.repositoryId"/>',
                                                                           '<dsp:valueof param="targetedProduct.repositoryId"/>' ,
                                                                           '<dsp:valueof param="targetedSku.repositoryId"/>','true')">
						<div class="gift-wrap-check"></div>
						<p>
							<dsp:valueof param="targetedSku.displayName" />
						</p>
										<div class="gift-wrap-border" id='<dsp:valueof param="targetedSku.repositoryId"/>' >
											<div class="gift-wrap-opaque">
												<div class="gift-wrap-tru" tabindex="0">
													<dsp:droplet name="IsEmpty">
														<dsp:param name="value" value="${giftWrapImage}" />
														<dsp:oparam name="false">
															<c:if test = "${fn:startsWith(giftWrapImage, '/images')}">
																<c:set var="giftWrapImage" value="/TRU-DCS-CSR${giftWrapImage}"/>
															</c:if>														
															<img src="${giftWrapImage}" alt="gift wrap"  width="140" height="56"/>
														</dsp:oparam>
														<dsp:oparam name="true">
															<img src="${CSRConfigurator.truContextRoot}/images/no-image500.gif" alt="gift wrap" width="140" height="56" />
														</dsp:oparam>
													</dsp:droplet>
												</div>
											</div>
										</div>
									
						
						
						
						
						
					</c:otherwise>
				</c:choose>
			</div>
			</c:if>
			</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<c:set var="giftWrapDivClass" value="" />
	<c:if test="${empty previousGiftWrapSkuId }">
		<c:set var="giftWrapDivClass" value="prevSelected" />
	</c:if>
	<div class="gift-wrap-none-block inline gift-wrap-container">
		<div>

			<input type="radio" class="wrapSkuId ${giftWrapDivClass}" data-id=""
				name="elegible-gift-item-wrap-<dsp:valueof param="key"/>"
				<c:if test="${not empty giftWrapDivClass}"> checked="checked" </c:if>
				onclick="javascript:handleRadioButtonClick(this,'<dsp:valueof param="giftWrapItem.shippingGroupId"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.commItemRelationshipId"/>',
                                                                           '<dsp:valueof param="key"/>', 
                                                                           '<dsp:valueof param="giftWrapItem.parentCommerceId"/>',
                                                                           '' ,
                                                                           '',
                                                                           '${firstWrapProductId}' ,
                                                                           '${firstWrapSkuId}','false')">

			<p>none</p>
		</div>
	</div>
</dsp:page>
