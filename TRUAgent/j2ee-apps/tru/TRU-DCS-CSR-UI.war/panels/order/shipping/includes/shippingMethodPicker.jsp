<%--
This page defines the shipping method picker
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/shippingMethodPicker.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>

  <dsp:page xml="true">
    <dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="agentTools"/>
    <dsp:importbean bean="/atg/commerce/custsvc/pricing/AvailablePricedShippingMethodsDroplet"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean var="order" bean="/atg/commerce/custsvc/order/ShoppingCart"/>
    <dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
    <dsp:importbean bean="/atg/commerce/pricing/AvailableShippingMethods"/>
	<dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService" />
    <dsp:getvalueof var="HGSGCount" param="HGSGCount"/>
    <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
    <dsp:getvalueof var="order" param="order"/>
    <dsp:getvalueof var="profile" bean="/atg/userprofiling/ActiveCustomerProfile"/>
    <dsp:getvalueof var="shippingPricingModels"
                    bean="/atg/commerce/custsvc/environment/CSREnvironmentTools.currentOrderPricingModelHolder.shippingPricingModels"/>
    <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
    <dsp:getvalueof var="currentShippingMethod"
                    bean="ShoppingCart.current.ShippingGroups[param:shippingGroupIndex].shippingMethod"/>
	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" var="CSRConfigurator" />
	<svc-ui:frameworkUrl var="alternateSuccessURL" panelStacks="cmcShippingMethodPS" />
	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
	<style>
		.shippingMethodDetail_content{
			position: absolute;
		    background: #fff;
		    width: 135px;
		    padding: 10px;
		    border: 1px solid rgba(0,0,0,0.2);
		    display: none;
		    border-radius: 5px;
		    margin-top: 10px;
   			margin-left: -10px;
		}
		 .shippingMethodDetail_content .arrow
		    {
		    position: absolute;
		    top: -12px;
		    left: 14px;
		    }
		.select-shipping-method-block{
			display: inline-block;
			width: 135px;
			height: 115px;
			background-color: #f3f3f3;
			border: 1px solid #cccccc;
			padding-left: 10px;
			padding-right: 10px;
		}
		.select-shipping-method-block input[type='checkbox']{
		  margin-top:6px;
		}  
	</style>
      <div class="atg_commerce_csr_shippingMethodPicker">
        <h4>
          <fmt:message key="shipping.chooseMethod.header"/>
        </h4>
        <ul class="shipMethod">
    <csr:getCurrencyCode order="${order}">
      <c:set var="currencyCode" value="${currencyCode}" scope="request" />
    </csr:getCurrencyCode>
   
    
    <c:if test="${HGSGCount == 1}">
	<c:choose>
		<c:when test="${CSRConfigurator.truShippingMethodAvailable eq 'true'}">
			<dsp:droplet name="AvailableShippingMethods">
			<dsp:param name="currentOrder" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
			<dsp:param name="profile" value="${profile}"/>		
			<dsp:param name="state" value="${shippingGroup.shippingAddress.state}"/>
			<dsp:param name="address1" value="${shippingGroup.shippingAddress.address1}"/>
			<dsp:param name="city" value="${shippingGroup.shippingAddress.city}"/>
			<dsp:param name="isNewAddressForm" value="true"/>	
			<dsp:param name="shippingGroupMapContainer" bean="ShippingGroupContainerService" />										
				<dsp:oparam name="output">			
					<dsp:getvalueof var="defaultShippingMethod" param="selectedShippingMethod" />			
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" param="availableShippingMethods" />
										
						<dsp:oparam name="output">
							<dsp:getvalueof var="shipMethodCode" param="element.shipMethodCode" />
							<dsp:getvalueof var="shippingPrice" param="element.shippingPrice"/>
							<dsp:getvalueof var="regionCode" param="element.regionCode"/>	
							<dsp:getvalueof var="details" param="element.details"/>
							<div class="select-shipping-method-block <c:if test='${shipMethodCode eq defaultShippingMethod}'></c:if>">
								<c:choose>
									<c:when test="${shipMethodCode eq defaultShippingMethod}">
										<input type="checkbox" class="shipping-method-checkbox" onchange= "javascript:selectShippingMethod(event,this);" name="${shipMethodCode}" id="${shipMethodCode}" checked="checked"/>	
									</c:when>						
									<c:otherwise>
										<input type="checkbox" class="shipping-method-checkbox" onchange= "javascript:selectShippingMethod(event,this);" name="${shipMethodCode}" id="${shipMethodCode}__${shippingPrice}__${regionCode}" />
									</c:otherwise>
								</c:choose>
								<p class="shipping-type-header">
									<dsp:valueof param="element.shipMethodName"/>
								</p>
								<p>
									(<dsp:valueof param="element.delivertTimeLine"/>)
								</p>
								<p class="co-shipping-method-price">
									<c:choose> 
										<c:when test="${(not empty order.priceInfo) and (not empty order.priceInfo.shipping) and (shipMethodCode eq defaultShippingMethod)}">
											<dsp:getvalueof var="shipping" value="${order.priceInfo.shipping}"/>
											<c:choose>
		                                		<c:when test="${shipping == 0.0}">
		                                			<fmt:message key="shoppingcart.free" bundle="${TRUCustomResources}"/>
		                                		</c:when>
		                                		<c:otherwise>
		                                			<dsp:valueof format="currency" value="${shippingPrice}" locale="en_US" converter="currency"/>
		                                		</c:otherwise>
		                                	</c:choose>
										</c:when>								
										<c:otherwise>
											<dsp:getvalueof var="shipping" param="element.shippingPrice"/>
											<c:choose>
		                                		<c:when test="${shipping == 0.0}">
		                                			<fmt:message key="shoppingcart.free"  bundle="${TRUCustomResources}"/>
		                                		</c:when>
		                                		<c:otherwise>
		                                			<dsp:valueof format="currency" value="${shippingPrice}" locale="en_US" converter="currency"/>
		                                		</c:otherwise>
		                                	</c:choose>
										</c:otherwise>
									</c:choose>
								</p>
								<c:choose>
									<c:when test="${shipMethodCode eq '1DAY'}">
										<dsp:getvalueof var="shipMethodCodePopover" value="one${fn:substring(shipMethodCode,1,fn:length(shipMethodCode))}" />
									</c:when>
									<c:otherwise>
										<dsp:getvalueof var="shipMethodCodePopover" value="${shipMethodCode}" />
									</c:otherwise>
								</c:choose>
								<a href="javascript:void(0);" id="<dsp:valueof param="element.shipMethodName"/>" class="shippingMethodDetails">details</a>
								<div class='shippingMethodDetail_content <dsp:valueof param="element.shipMethodName"/>'>
									<img src="/TRU-DCS-CSR/images/up-arrow.png" class="arrow"/>
									<span class="hidden_shipping_details">${details}</span>
								</div>
								
							</div>
						</dsp:oparam>				
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="error">
					<input type="hidden" class="shipMethodCode" value="">
					<input type="hidden" class="shippingPrice" value="0.0">
					<div class="errorContainerSingleShipping">
						<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
						<div class="errorDisplay">
							<dsp:valueof param="message"></dsp:valueof>
						</div>
					</div>
				</dsp:oparam>				
			</dsp:droplet>	
		</c:when>
		<c:otherwise>
	          <dsp:droplet name="AvailablePricedShippingMethodsDroplet">
	            <dsp:param name="order" value="${order}"/>
	            <dsp:param name="profile" value="${profile}"/>
	            <dsp:param name="pricingModels" value="${shippingPricingModels}"/>
	            <dsp:param name="shippingGroup" value="${shippingGroup}"/>
	            <dsp:oparam name="output">
	              <dsp:droplet name="ForEach">
	                <dsp:param name="array" param="availablePricedShippingMethods"/>
	                <dsp:oparam name="output">
	                  <dsp:getvalueof var="method" param="key"/>
	                  <dsp:getvalueof var="priceInfo" param="element"/>
	                  <dsp:getvalueof var="methodIndex" param="index"/>
	                  <c:choose>
	                    <c:when test="${!empty currentShippingMethod && currentShippingMethod != 'hardgoodShippingGroup'}">
	                      <c:choose>
	                        <c:when test="${method == currentShippingMethod}">
	                          <c:set var="checked" value="${true}"/>
	                        </c:when>
	                        <c:otherwise>
	                          <c:set var="checked" value=""/>
	                        </c:otherwise>
	                      </c:choose>
	                    </c:when>
	                    <c:otherwise>
	                      <c:choose>
	                        <c:when test="${methodIndex == 0}">
	                          <c:set var="checked" value="${true}"/>
	                        </c:when>
	                        <c:otherwise>
	                          <c:set var="checked" value=""/>
	                        </c:otherwise>
	                      </c:choose>
	                    </c:otherwise>
	                  </c:choose>
	                  <li>
	                    <dsp:input type="radio" checked="${checked}"
	                               bean="ShoppingCart.current.ShippingGroups[param:shippingGroupIndex].shippingMethod"
	                               value="${method}"/>
	                    &nbsp;${method}&nbsp;
	                    <web-ui:formatNumber value="${priceInfo.amount}" type="currency" currencyCode="${currencyCode}"/>
	                  </li>
	                </dsp:oparam>
	              </dsp:droplet>
	            </dsp:oparam>
	            <dsp:oparam name="error">
	              <dsp:getvalueof var="msg" param="errorMessage"/>
	              ${fn:escapeXml(msg)}
	            </dsp:oparam>
	          </dsp:droplet>
			</c:otherwise>
		</c:choose>
		</c:if>
		 <c:if test="${HGSGCount > 1}">
		 <dsp:droplet name="DisplayOrderDetailsDroplet">
		<dsp:param name="order" value="${order}" />
		<dsp:param name="profile" value="${profile}" />
		<dsp:param name="page" value="shipping" />
		<dsp:oparam name="output">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="shippingDestinationsVO" />
				<dsp:param name="elementName" value="shippingDestinationVO" />
				<dsp:oparam name="output">
						<dsp:droplet name="ForEach">
							<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
							<dsp:param name="elementName" value="shipmentVO" />
							<dsp:oparam name="output">
								<dsp:droplet name="ForEach">
									<dsp:param name="array"	param="shipmentVO.cartItemDetailVOs" />
									<dsp:param name="elementName" value="cartItemDetailVO" />
									<dsp:oparam name="output">
									<div class="shipMethodGroup" style="width:100%;">
										<dsp:include page="multiShipMethod.jsp">
											<dsp:param name="cartItemDetailVO" param="cartItemDetailVO" />
											<dsp:param name="shippingDestinationVO" param="shippingDestinationVO" />
											<dsp:param name="shipmentVO" param="shipmentVO" />
											<dsp:param name="pageName" value="Shipping" />
											<dsp:param name="shippingGroup" value="${shippingGroup}" />
										</dsp:include>
									</div>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
		 
		 </c:if>
		
        </ul>
      </div>
     <script type="text/javascript">
     	$(document).on("mouseover",".shippingMethodDetails",function(){
     		var shippingId = $(this).attr("id");
     		var htmlContent =$(this).next("."+shippingId).find(".hidden_shipping_details").html();
     		if($.trim(htmlContent) != '')
     			{
     				$(this).next("."+shippingId).fadeIn();
     			}
     		else
     			{
     				return false;
     			}
     	})
     	$(document).on("mouseout",".shippingMethodDetails",function(){
     		$(".shippingMethodDetail_content").fadeOut('fast').removeAttr("style");
     	})
     </script> 
    </dsp:layeredBundle>
  
  </dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/shippingMethodPicker.jsp#1 $$Change: 875535 $--%>

