<%--
This page defines the address view
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/itemTable.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
  <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
  <dsp:getvalueof var="order" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <dsp:importbean var="CSRConfigurator" bean="/atg/commerce/custsvc/util/CSRConfigurator"/>

  <dsp:importbean bean="/atg/multisite/Site"/> 
  <dsp:getvalueof var="currentSiteId" bean="Site.id"/>
    <table class="atg_dataTable atg_commerce_csr_innerTable">
      <thead>
      <%-- Site Icon Heading --%>
      <c:if test="${isMultiSiteEnabled == true}">
        <th class="atg_commerce_csr_siteIcon"></th>
      </c:if>
  <!--      <th style="width:30%">
       Message
      </th> -->
      <th>
        <fmt:message key="shippingSummary.commerceItem.header.itemDesc"/>
      </th>
     
      <th>
        <fmt:message key="shippingSummary.commerceItem.header.status"/>
      </th>
      <th class="atg_numberValue" style="width:15%">
        <fmt:message key="shippingSummary.commerceItem.header.qty"/>
      </th>
      </thead>
      <c:forEach items="${order.shippingGroups[shippingGroupIndex].commerceItemRelationships}" var="cisiItem">
        <dsp:tomap var="sku" value="${cisiItem.commerceItem.auxiliaryData.catalogRef}"/>
        <c:if test="${cisiItem.commerceItem.commerceItemClassType ne 'giftWrapCommerceItem' && cisiItem.commerceItem.commerceItemClassType ne 'donationCommerceItem'}">
        <tr>
          <c:if test="${isMultiSiteEnabled == true}">
            <c:set var="siteId" value="${cisiItem.commerceItem.auxiliaryData.siteId}"/>
            <td class="atg_commerce_csr_siteIcon">
              <csr:siteIcon siteId="${siteId}" />
            </td>
          </c:if>
           <%-- <td>
           
            <dsp:include page="/include/gwp/changeGiftLink.jsp"/> 
         <!--   <input type="checkbox"/> -->
           <textarea rows="15" style="width:200px;"></textarea>
            
           <dsp:include page="giftNote.jsp"/>
            </td> --%>
          <td>
            <ul class="atg_commerce_csr_itemDesc">
              <c:choose>
                <c:when test="${(isMultiSiteEnabled == true) && (isSiteDeleted != true)}">
                  <li><a title="<fmt:message key='cart.items.quickView' />" href="#" onclick="atg.commerce.csr.common.showPopupWithReturn(
                    {popupPaneId: 'productQuickViewPopup',
                     title: '${fn:escapeXml(sku.displayName)}',
                     url: '${CSRConfigurator.truContextRoot}/include/order/product/productReadOnly.jsp?_windowid=${windowId}&commerceItemId=${cisiItem.commerceItem.id}&siteId=${siteId}',
                     onClose: function(args) { }});return false;">${fn:escapeXml(sku.displayName)}</a></li>
                </c:when>
                <c:otherwise>
                  <li><a title="<fmt:message key='cart.items.quickView' />" href="#" onclick="atg.commerce.csr.common.showPopupWithReturn(
                    {popupPaneId: 'productQuickViewPopup',
                     title: '${fn:escapeXml(sku.displayName)}',
                     url: '${CSRConfigurator.truContextRoot}/include/order/product/productReadOnly.jsp?_windowid=${windowId}&commerceItemId=${cisiItem.commerceItem.id}',
                     onClose: function(args) {}});return false;">${fn:escapeXml(sku.displayName)}</a></li>
                </c:otherwise>
              </c:choose>
            <li>${cisiItem.commerceItem.catalogRefId}</li>
            <!-- Render Giftlist information -->
            <dsp:droplet var="fe" name="/atg/dynamo/droplet/ForEach">
              <dsp:param name="array" value="${order.shippingGroups[shippingGroupIndex].handlingInstructions}" />
              <dsp:oparam name="output">
                <c:if test="${fe.element.handlingInstructionClassType == 'giftlistHandlingInstruction'}">
                  <c:if test="${fe.element.commerceItemId == cisiItem.commerceItem.id}">
                    <dsp:droplet name="/atg/commerce/gifts/GiftlistLookupDroplet">
                      <dsp:param name="id" value="${fe.element.giftlistId}" />
                      <dsp:oparam name="output">
                        <li class="atg_commerce_csr_giftwishListName">
                          <dsp:setvalue paramvalue="element" param="giftlist" />
                          <dsp:getvalueof var="eventName" vartype="java.lang.String" param="giftlist.eventName" />
                          <dsp:valueof param="giftlist.owner.firstName" />&nbsp;
                          <dsp:valueof param="giftlist.owner.lastName" />, <c:out value="${eventName}" />
                        </li>
                      </dsp:oparam>
                    </dsp:droplet><%-- End Switch --%>
                  </c:if>
                </c:if>
              </dsp:oparam>
            </dsp:droplet><%-- End forEach --%>
          </ul>
          </td>
          <td>
            <csr:inventoryStatus commerceItemId="${cisiItem.commerceItem.catalogRefId}" locationIdSupplied="true" locationId="${order.shippingGroups[shippingGroupIndex].locationId}"/>
            <br><dsp:include src="/include/catalog/itemsPickUpAvailability.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dsp:param name="storeId" value="${order.shippingGroups[shippingGroupIndex].locationId}"/>
                  <dsp:param name="skuId" value="${cisiItem.commerceItem.catalogRefId}"/>
                  <dsp:param name="quantity" value="${cisiItem.quantity}"/>
                </dsp:include>
          </td>
          <td class="atg_numberValue">
           ${cisiItem.quantity}
          </td>
        </tr>
        </c:if>
      </c:forEach>
    </table>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/itemTable.jsp#1 $$Change: 875535 $--%>