<%--
This page defines the address view in the single shipping address page.

This page is basically used in the Ship To Address section which is the top page in the 
shipping address page.

Expected params
shippingGroup : The shipping group from which the shipping group information is going to be retrieved.
addressKey : The current shipping shipping group name or the map key.
formId : the current form id in which the form elements are rendered.
selectedNickname : the current shipping group name which is used to ship all the items. If all commerce
                   items in the order is pointing to the same shipping group name, then that shipping address
                   should be pre selected in UI. This shipping group name allows us to identify that shipping
                   group.
commonShippingGroupTypes : This parameter tells the common shipping group types (intersection of all commerce items 
                           shipping group types) for the entire order.


@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/addressView.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:importbean bean="/com/tru/commerce/AddressDoctor"/>
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
  <dsp:getvalueof var="addressKey" param="addressKey"/>
  <input type="hidden" id="addressKeyId" value="${addressKey}"/>
  <dsp:getvalueof var="commonShippingGroupTypes" param="commonShippingGroupTypes"/>
  <dsp:getvalueof var="formId" param="formId"/>
  <dsp:getvalueof var="selectedNickname" param="selectedNickname"/>
  <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
  <dsp:getvalueof var="shippingAddressSelected" param="shippingAddressSelected"/>
  <dsp:getvalueof var="sgType" param="shippingGroup.shippingGroupClassType"/>
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
  
   <dsp:getvalueof var="address" param="address"/>

<dsp:getvalueof var="address" param="shippingGroup.shippingAddress"/>
<%--  <dsp:getvalueof var="shipIddd" param="${shippingGroup.id}"/> --%>
 <c:set var="shipIddd" value="${shippingGroup.id}"/>
  <c:set var="pageName" value="shipping"/>
  <c:set var="closeId" value="csrAddressDoctorFloatingPane"/>
     

   <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
   <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingMethodPS" init="true"/>
   <svc-ui:frameworkUrl var="electronicShippingGroupSuccessURL" panelStacks="cmcBillingPS" init="true"/>
 
   <dsp:contains var="found" values="${commonShippingGroupTypes}" object="${sgType}"/>
   

    <c:if test="${!found && sgType != 'inStorePickupShippingGroup'}">
      <script type="text/javascript">
        _container_.onLoadDeferred.addCallback(function() {
          //dijit.byId('${shippingGroup.id}').disableButton();
        });
      </script>
    </c:if>
	<style>
		.atg_commerce_csr_addressView, address-doctor-address p{
			margin: 0 0 0 4px;
			width: 281px;
		}
	</style>
	
	 <dsp:importbean  bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler" />
	
    <div class="atg_commerce_csr_addressView">

      <fmt:message var="shipToSingleAddress" key="newOrderSingleShipping.button.shipToAddress"/>
      <dsp:getvalueof var="sgType" param="shippingGroup.shippingGroupClassType"/>
      
      <c:if test="${sgType != 'inStorePickupShippingGroup'}">
        <c:choose>
          <c:when test="${!empty addressKey && selectedNickname eq addressKey }">
            <c:set var="selectedAddressKey" value="${addressKey}" scope="request"/>
            <%-- Evaluated on shipping address page for the selected address --%>
            <ul id="atg_commerce_csr_neworder_ShippingAddressHome"  class="atg_svc_shipAddress">
          </c:when>
          <c:when test="${!(selectedNickname eq addressKey)}">
            <%-- Evaluated on shipping address page for an unselected address --%>
            <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
          </c:when>
          <c:otherwise>
            <%-- Evaluated on shipping method page --%>
            <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
          </c:otherwise>
        </c:choose>
      </c:if>
      <dsp:getvalueof var="sgTypeConfig" bean="CSRConfigurator.shippingGroupTypeConfigurationsAsMap.${sgType}"/>
      <c:if test="${sgTypeConfig != null && sgTypeConfig.displayPageFragment != null}">
        <dsp:include src="${sgTypeConfig.displayPageFragment.URL}"
                     otherContext="${CSRConfigurator.truContextRoot}">
          <dsp:param name="shippingGroup" param="shippingGroup"/>
          <dsp:param name="propertyName" value="value1"/>
          <dsp:param name="displayValue" value="${true}"/>
        </dsp:include>
      </c:if>

      <dsp:getvalueof var="sgTypeConfig" bean="CSRConfigurator.shippingGroupTypeConfigurationsAsMap.${sgType}"/>
      <c:if test="${sgTypeConfig != null && sgTypeConfig.editPageFragment != null}">
        <c:url var="editAddressURL" context="${CSRConfigurator.truContextRoot}"
               value="${sgTypeConfig.editPageFragment.URL}">
          <c:param name="nickname" value="${addressKey}"/>
              <c:param name="selectedNickname" value="${selectedNickname}"/>
          
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
        </c:url>
        
      <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/addressDoctor.jsp">
          <c:param name="addrKey" value="${addressKey}"/>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
            <c:param name="sp" value="${shippingGroup}"/>
        </c:url>

        <li class="atg_commerce_csr_editAddress">
        <span class="atg_svc_shipAddressControls">
          <a class="atg_tableIcon atg_propertyEdit" style="min-width:100px" title="<fmt:message key="common.address.edit.mouseover"/>" href="#"
               onclick="atg.commerce.csr.common.showPopupWithReturn({
                        popupPaneId: 'csrEditAddressFloatingPane',
                        title: '<fmt:message key="common.edit"/>',
                        url: '${editAddressURL}&adre1=${shippingGroup.shippingAddress.address1}&adre2=${shippingGroup.shippingAddress.address2}&city=${shippingGroup.shippingAddress.city}&country=${shippingGroup.shippingAddress.country}&state=${shippingGroup.shippingAddress.state}&postalCode=${shippingGroup.shippingAddress.postalCode}&addrKey=${addressKey}&id=${shippingGroup.id}&sgTypeValue=${sgType}',
                        onClose: function( args ) {
                          if ( args.result == 'ok' ) {
                            atgSubmitAction({
                              panelStack :[ 'cmcShippingAddressPS','globalPanels'],
                              form : document.getElementById('transformForm')
                            });
                          }
                        }});return false;">
             <fmt:message key="common.edit"/>
          </a>
        </span>
        </li>
      </c:if>
      <c:if test="${sgType != 'inStorePickupShippingGroup'}">
        <li class="atg_commerce_csr_shippingControls">
 <%--     <input type="button" name="shipToButton" value="${shipToSingleAddress}" onclick="atg.commerce.csr.common.showPopupWithReturn({
                        popupPaneId: 'csrAddressDoctorFloatingPane',
                        title: '<fmt:message key="common.edit"/>',
                        url: '${addressDoctorURL}&shippingGoupp=${shippingGroup.shippingAddress}&phoneNumber=${shippingGroup.shippingAddress.phoneNumber}&lastName=${shippingGroup.shippingAddress.lastName}&firstName=${shippingGroup.shippingAddress.firstName}&adre1=${shippingGroup.shippingAddress.address1}&adre2=${shippingGroup.shippingAddress.address2}&city=${shippingGroup.shippingAddress.city}&country=${shippingGroup.shippingAddress.country}&state=${shippingGroup.shippingAddress.state}&postalCode=${shippingGroup.shippingAddress.postalCode}&id=${shippingGroup.id}&sgTypeValue=${sgType}&pageName=${pageName}&closeId=${closeId}',
                        onClose: function( args ) {
                          if ( args.result == 'ok' ) {
                            atgSubmitAction({
                              panelStack :[ 'cmcShippingAddressPS','globalPanels'],
                              form : document.getElementById('transformForm')
                            });
                          }
                        }});return false;"
          id="${shippingGroup.id}" class="selectyesno"/>  --%> 
          <button class="address-doctor-select" id="${shippingGroup.id}" type="button" value="yes" onclick="addressDoctorSelectBtn('${addressKey}','${shippingGroup.id}','${shippingGroup.shippingAddress.address1}','${shippingGroup.shippingAddress.address2}','${shippingGroup.shippingAddress.city}','${shippingGroup.shippingAddress.state}','${shippingGroup.shippingAddress.country}','${shippingGroup.shippingAddress.postalCode}','${shippingGroup.shippingAddress.firstName}','${shippingGroup.shippingAddress.lastName}','${shippingGroup.shippingAddress.phoneNumber}');" />Submit</button> 
          
     <%--   <input type="button" name="shipToButton" value="${shipToSingleAddress}" dojoType="atg.widget.validation.SubmitButton" 
           onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'});"
          id="${shippingGroup.id}" class="selectyesno"/>  
 	--%>
        </li>
      </c:if>
    </ul>
</div> 
</dsp:layeredBundle>
</dsp:page>
	
 <c:if test="${sgType eq 'hardgoodShippingGroup' }">
	<script type="text/javascript">
		function addressDoctorSelectBtn(addrKey, id, address1, address2, city, state, country, postalCode, firstName, lastName,phoneNumber) {

			var form = document.getElementById('singleShippingAddressForm');
			
			//var phoneNo=$('#addressDoctorPhoneNumber').val();
			
			var adKey = addrKey;
			//alert(adKey);
			form.shipToAddressNicknameSG.value = adKey;
			form.locationId.value='';
			var shipId = "<c:out value="${shipIddd}"/>";
			var shippingId = id;
			atgSubmitAction({						
						form : document
								.getElementById('singleShippingAddressForm')
			});
			/* dojo.xhrPost({
						form : form,
						url : "${addressDoctorURL}&phoneNo="+phoneNumber+"&firstName="+firstName+"&lastName="+lastName+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&aKey="+adKey+"&country="+country+"&state="+state+"&postalCode="+postalCode+"&id="+id+"&sgTypeValue=${sgType}&pageName=${pageName}&closeId=${closeId}"+ window.windowId,
						encoding : "utf-8",
						preventCache : true,
						handle : function(_362, _363) {
							var successURL2 = $("#successURL").val();
							var form = document.getElementById('transformForm');
							
							form.commonSuccessURL.value = successURL2 ;
							atgSubmitAction({						
										form : document
												.getElementById('transformForm')
							});
						/* 	
								atg.commerce.csr.common
									.showPopupWithReturn({
										popupPaneId : 'csrAddressDoctorFloatingPane',
										title : 'Edit',
										url : "${addressDoctorURL}&phoneNo="+phoneNumber+"&firstName="+firstName+"&lastName="+lastName+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&aKey="+adKey+"&country="+country+"&state="+state+"&postalCode="+postalCode+"&id="+id+"&sgTypeValue=${sgType}&pageName=${pageName}&closeId=${closeId}"+ window.windowId,
										onClose : function(args) {
											if (args.result == 'ok') {
												atgSubmitAction({
													panelStack : [
															'cmcShippingAddressPS',
															'globalPanels' ],
													form : document
															.getElementById('transformForm')
												});
											}
										}
									}); */ 
							/* return false;
							if (document
									.getElementById("inStorePickupResultsss")) {
								document
										.getElementById("inStorePickupResultsss").innerHTML = _362;
							}

						},
						mimetype : "text/html"
					}); */ 
		}
	</script> 

</c:if>
<script type="text/javascript">

	
	atg.commerce.csr.order.shipping.shipToAddress = function(pParams, key) {
		var form = document.getElementById('singleShippingAddressForm');
		var locationId = $("#currentLocationId").val();
		var successURLISPU = $("#successURLISPU").val();
		var test = '<c:out value="${addressKey}"/>';
		var test1 = $("#addressKeyId").val();
		form.shipToAddressNicknameSG.value = key;
		form.locationId.value='';
		if(locationId != ''){
			form.locationId.value=locationId;
			form.shipToAddressNicknameSG.value = locationId;
			form.successURL2.value=successURLISPU;
		}
		/* var address2 = document.getElementById('address2').value; */
		atg.commerce.csr.common.prepareFormToPersistOrder(pParams);
		atgSubmitAction({
			form : dojo.byId(form)
		});
		//hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
	};

	dojo.addOnLoad(function() {
		var theForm = dojo.byId("${formId}");
		if ((theForm != null) && (theForm.shipToButton != null)
				&& (theForm.shipToButton.focus)) {
			theForm.shipToButton.focus();
		}
	});
</script>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/addressView.jsp#1 $$Change: 875535 $--%>