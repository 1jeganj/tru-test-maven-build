<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
 
 	<dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>
  	<dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupDroplet"/>
  	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
   	<dsp:importbean bean="/com/tru/commerce/AddressDoctor"/>
   	<c:set var="formId" value="singleShippingAddressForm"/>
 	<dsp:getvalueof var="formId" param="formId"/>
  	<dsp:getvalueof var="commonShippingGroupTypes" param="commonShippingGroupTypes"/>
  	<dsp:getvalueof var="selectedNickname" param="selectedNickname"/>
  	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

    <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingMethodPS" init="true"/>
    <svc-ui:frameworkUrl var="electronicShippingGroupSuccessURL" panelStacks="cmcBillingPS" init="true"/>

    <dsp:getvalueof var="sgType" param="sgTypeValue"/>
    <dsp:contains var="found" values="${commonShippingGroupTypes}" object="${sgType}"/>
     <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:getvalueof var="addrKey" param="aKey"/>
     <dsp:getvalueof var="nickName" param="nickName"/>
    <dsp:getvalueof var="pageName" param="pageName"/>
    <dsp:getvalueof var="adre1" param="adre1"/>
    <dsp:getvalueof var="adre2" param="adre2"/>
    <dsp:getvalueof var="city" param="city"/>
    <dsp:getvalueof var="country" param="country"/>
    <dsp:getvalueof var="state" param="state"/>
    <dsp:getvalueof var="postalCode" param="postalCode"/>
    <dsp:getvalueof var="shipId" param="id"/>
    <dsp:getvalueof var="success" param="success"/>
    <dsp:getvalueof var="formId" param="form"/>
    <dsp:getvalueof var="closeId" param="closeId"/>
    <dsp:getvalueof var="firstName" param="firstName"/>
    <dsp:getvalueof var="lastName" param="lastName"/>
    <dsp:getvalueof var="phoneNumber" param="phoneNo"/>
	<dsp:getvalueof var="address" param="sp.shippingAddress"/>
	<dsp:getvalueof var="accId" param="accId"/>
	<dsp:getvalueof var="paramId" param="paramId"/>
	<dsp:getvalueof var="maskedCreditCardNumberVar" param="maskedCreditCardNumberVar"/>
	<dsp:getvalueof var="paymentGroups" param="paymentGroups"/>
	<dsp:getvalueof var="nickName" param="nickName"/>
<%-- <c:set var="pageName" value="radialChange"/> --%>

	    <svc-ui:frameworkPopupUrl var="url"
      value="/include/addresses/addressEditor.jsp"
      context="/truagent"
      addressId="${param.addressId}"
      windowId="${windowId}"/>
	
	
	   <c:url var="urls" context="${CSRConfigurator.truContextRoot}"
             value="/include/creditcards/creditCardEditor.jsp">
     </c:url>  
	
	<%--      <svc-ui:frameworkPopupUrl var="urls"
        value="/include/creditcards/creditCardEditor.jsp"
        context="${CSRConfigurator.truContextRoot}"
         windowId="${windowId}"/> --%>
  	 	
  	<dsp:droplet name="ForEach">
	<dsp:param name="array" value="${paymentGroups}"/>
	<dsp:oparam name="output">
	  <dsp:getvalueof var="pgKey" param="key"/>
	  </dsp:oparam>
  </dsp:droplet> 

   	<c:set var="closeButtonId" value="atg_commerce_csr_selectShippingAddress"/>     
  	<dsp:importbean bean="/com/tru/commerce/AddressDoctor"/>
  	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	
	 <dsp:getvalueof var="addressBean" param="addressBean"/>
	 	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />

  	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>

    <c:if test="${!found && sgType != 'inStorePickupShippingGroup'}">
      <script type="text/javascript">
        _container_.onLoadDeferred.addCallback(function() {
          //dijit.byId('${shippingGroup.id}').disableButton();
        });
      </script>
    </c:if>
    
	<dsp:getvalueof var="error" bean="/com/tru/commerce/AddressDoctor.taxwareError"/> 
	<c:choose>
	<c:when test="${error}">
		<span style="color:red;margin-bottom:15px;float: left;width: 100%;font-weight: bold;">taxwareGeneralDataError</span>
	</c:when>
	<c:otherwise>
   	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO" />
	<dsp:oparam name="false">
	<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
	</dsp:oparam>
	</dsp:droplet>
	

	<c:choose>
		<c:when test="${not addressRecognized}">
			<p style="margin-left:0px;">
				<fmt:message key="myaccount.address.not.recognised" bundle="${TRUCustomResources}"/>
				<fmt:message key="myaccount.address.choose.existing.address" bundle="${TRUCustomResources}"/>
				<fmt:message key="myaccount.address.enter.new" bundle="${TRUCustomResources}" />
			</p>
		</c:when>
		</c:choose>	
	
	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO" />
	<dsp:oparam name="false">
	
		<dsp:getvalueof var="container" bean="/atg/commerce/custsvc/order/ShippingGroupContainerService"/>
		
	<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
	<p style="margin-left:0;"><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
		
		<c:choose>
		<c:when test="${not addressRecognized}">
			<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-0" value="0"  checked style="float:left;position: relative;top: 2px;margin-right: 6px;">
		</c:when>
		<c:otherwise>
			<input class="select-valid-address" checked name="valid-address"  type="radio" id="suggestedAddressCount-0" value="0"  style="float:left;position: relative;top: 2px;margin-right: 6px;">
		</c:otherwise>
		</c:choose> 
		
		<c:choose>
		<c:when test="${pageName eq 'edit'}">
			 ${adre1} ${adre2},${city},${country},${state}, ${postalCode},${phoneNumber}
		</c:when>
		<c:otherwise>
		 <p class="address-doctor-address" style="float:left;margin-left:0;">
		 <%--  ${address.address1} ${address.address2},${address.city},${address.country},${address.state}, ${address.postalCode}  --%>
 		   ${adre1} ${adre2},${city},${country},${state}, ${postalCode}  
		</p>
		</c:otherwise>
		</c:choose>
		<button onclick="closeModal();return false;" style="margin-top:-6px;"><fmt:message key="myaccount.edit" bundle="${TRUCustomResources}"/></button>		
			<div class="row" style="width:auto;">
						<div class="col-md-2">
							
						</div>
					<!-- 	<div class="col-md-10">
							<p>Did you mean one of the following</p>
						</div> -->
					</div>
		<input name="valid-address" type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
		<input name="valid-address" type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
		<input name="valid-address" type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
		<input name="valid-address" type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
		<input name="valid-address" type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
		<input name="valid-address" type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	
	
	
	<dsp:droplet name="IsEmpty">
	<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
	<dsp:oparam name="false">
		<div class="address-doctor-alt">
		<span class="address-doctor-msg" style="width: 100%;float: left;" >
			<fmt:message key="myaccount.did.you.mean.one.of.following" bundle="${TRUCustomResources}"/>	</span>
			
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
				<dsp:param name="elementName" value="addressDoctorVO" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="size" param="size"/>
					<dsp:getvalueof var="suggestedAddressCount" param="addressDoctorVO.count"/>
					<dsp:getvalueof var="addressDoctorVO" param="addressDoctorVO"/>
					<div style="margin-top:5px;">
					 		<c:choose>
						<c:when test="${size == 1}">
							<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}"  checked style="float:left;position: relative;top: 2px;margin-right: 6px;">
						</c:when>
						<c:otherwise>
							<input class="select-valid-address" name="valid-address"  type="radio" id="suggestedAddressCount-${suggestedAddressCount}" value="${suggestedAddressCount}" style="float:left;position: relative;top: 2px;margin-right: 6px;">
						</c:otherwise>
					</c:choose> 
					
						<p class="address-doctor-address">
							<dsp:valueof param="addressDoctorVO.fullAddressName" />
						</p>
						<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
						<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
						<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
						<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
						<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
						<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="US" />
					</div>
				</dsp:oparam>
			</dsp:droplet>
		</div>
	</dsp:oparam >
	</dsp:droplet>
	</dsp:oparam>
	
	</dsp:droplet> 
	
	
	<div class="modal-dialog modal-lg" id="atg_commerce_csr_selectShippingAddress">
		<div class="modal-dialog modal-lg">
			<div class="modal-content sharp-border">
				<div class="pdp-modal1">
				<!--action-btns start-->
					<div class="action-btns warning-btns">
						<c:choose>
						<c:when test="${not addressRecognized}">
							 <!-- <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
						 -->
						<%-- 	<p><fmt:message key="myaccount.use.entered.address" /></p> --%>
							<p class="address-doctor-address">
							<%--  ${adre1} ${adre2},${city},${country},${state}, ${postalCode} --%>
							 
							<input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	 					
							</p>
						</c:when>
						<c:otherwise>
							  <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="hidden" id="suggestedAddressCount-0" value="0"> 
					 	</c:otherwise>
						</c:choose>
				
						
						  <c:choose>
							<c:when test="${not addressRecognized}">
							<!-- <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="radio" id="suggestedAddressCount-0" value="0" checked>
							 -->
							<%-- <p><fmt:message key="myaccount.use.entered.address" /></p> --%>
							<p class="address-doctor-address">
							<%--  ${adre1} ${adre2},${city},${country},${state}, ${postalCode} --%>
							 
							 <input type="hidden" name="suggestedAddress1-0" id="suggestedAddress1-0" value="${adre1}" maxlength="50"/>
							<input type="hidden" name="suggestedAddress2-0" id="suggestedAddress2-0" value="${adre2}" maxlength="50"/>
							<input type="hidden" name="suggestedCity-0" id="suggestedCity-0" value="${city}" maxlength="30"/>
							<input type="hidden" name="suggestedState-0" id="suggestedState-0" value="${state}" />
							<input type="hidden" name="suggestedPostalCode-0" id="suggestedPostalCode-0" value="${postalCode}" />
							<input type="hidden" name="suggestedCountry-0" id="suggestedCountry-0" value="${country}" />
	 					
							</p>
						</c:when>
						<c:otherwise>
					 	 <input name="valid-address" onclick="suggestedShippingAddressSelect('1');" type="hidden" id="suggestedAddressCount-0" value="0"> 
						 </c:otherwise>
						</c:choose> 

						<c:choose>
				<c:when test="${pageName eq 'radialChange'}">

						
								
								
								<c:choose>
								<c:when test="${not addressRecognized }">
									
								<input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To add"
		                       onclick="inStorePaymentPopupSubmit();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                      <%--  <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
									
								</c:when>
								<c:otherwise>
								
								 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To add Address"
		                       onclick="inStorePaymentPopupSubmit();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                       <div class="col-md-10">
									<%-- <p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
								</div>
								
								</c:otherwise>
								</c:choose>
								
								
								
		                       
						</c:when>
								<c:when test="${pageName eq 'paymentPage'}">

								<c:choose>
								<c:when test="${not addressRecognized }">
									
								<input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="continue"
		                       onclick="continueToOrderReview();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                      <%--  <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
									
								</c:when>
								<c:otherwise>
								
								 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="continue"
		                       onclick="continueToOrderReview();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                       <div class="col-md-10">
									<%-- <p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
								</div>
								
								</c:otherwise>
								</c:choose>
								
								
								
		                       
						</c:when>
						<c:when test="${pageName eq 'billing'}">

						
								
								
								<c:choose>
								<c:when test="${not addressRecognized }">
									
								<input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To add"
		                       onclick="addAddress();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                      <%--  <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
									
								</c:when>
								<c:otherwise>
								
								 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To add Address"
		                       onclick="addAddress();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                       <div class="col-md-10">
									<%-- <p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
								</div>
								
								</c:otherwise>
								</c:choose>
								
								
								
		                       
						</c:when>
						
							<c:when test="${pageName eq 'editBilling'}">

								<c:choose>
								<c:when test="${not addressRecognized }">
									
								<input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To edit"
		                       onclick="editCreditCard();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                      <%--  <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
									
								</c:when>
								<c:otherwise>
								
								 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed To edit address"
		                       onclick="editCreditCard();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                       <div class="col-md-10">
									<%-- <p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
								</div>
								
								</c:otherwise>
								</c:choose>
		                       
						</c:when>
						<c:when test="${pageName eq 'account'}">
								<c:choose>
								<c:when test="${not addressRecognized }">
									
								<input type="button" id="profileAddressEditorFormbillingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed"
		                       onclick="if (${empty accId}) {
                              dojo.byId( 'addAddressAction' )['disabled'] = false;
                              dojo.byId( 'updateAddressAction' )['disabled'] = true;
                            }
                            else {
                              dojo.byId( 'addAddressAction' )['disabled'] = true;
                              dojo.byId( 'updateAddressAction' )['disabled'] = false;
                            } atgSubmitPopup({url: '${url}', 
		                              form: dojo.byId('profileAddressEditorForm'),
		                              popup: getEnclosingPopup('profileAddressEditorForm_nickName')});hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                      <%--  <div class="col-md-10">
									<p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p>
								</div> --%>
									
								</c:when>
								<c:otherwise>
								
								 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
		                       value="Proceed"
		                       onclick="if (${empty accId}) {
		                              dojo.byId( 'addAddressAction' )['disabled'] = false;
	                              dojo.byId( 'updateAddressAction' )['disabled'] = true;
	                            }
	                            else {
	                              dojo.byId( 'addAddressAction' )['disabled'] = true;
	                              dojo.byId( 'updateAddressAction' )['disabled'] = false;
	                            }addAccountAddress();"
		                       dojoType="atg.widget.validation.SubmitButton" />
		                       <div class="col-md-10">
									<%-- <p><fmt:message key="myaccount.use.entered.address" bundle="${TRUCustomResources}"/></p> --%>
								</div>
								
								</c:otherwise>
								</c:choose>
						</c:when>
						<c:when test="${pageName eq 'accountCredit'}">
								<c:choose>
								<c:when test="${not addressRecognized }">
								<%-- 	
									  <input value="<fmt:message key='creditCard.save.label'/>" 
              type="button" id="saveChoice" name="saveChoice" 
              onClick="atgSubmitPopup({url: '${urls}', 
                  form: document.getElementById('editCreditCardForm'),
                  popup: getEnclosingPopup('atg_commerce_csr_editCreditCard')});
                return false;"/>  --%>
                  <input value="Proceed" 
              type="button" id="saveChoice" name="saveChoice" 
              onClick="<c:choose><c:when test="${empty paramId ? 'true' : 'false'}">
               startTokenization();
               hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
               return false;
                </c:when><c:otherwise>
                document.getElementById('editCreditCardForm')['/atg/commerce/custsvc/repository/CreditCardFormHandler.value.creditCardNumber']['disabled'] = 
                    ('${maskedCreditCardNumberVar}' == dijit.byId('editCreditCardForm_maskedCreditCardNumber').getValue());
                  document.getElementById('editCreditCardForm').createCreditCardAction['disabled'] = true;
                  document.getElementById('editCreditCardForm').updateCreditCardAction['disabled'] = false;
                </c:otherwise></c:choose>atgSubmitPopup({url: '${urls}', 
                  form: document.getElementById('editCreditCardForm'),
                  popup: getEnclosingPopup('atg_commerce_csr_editCreditCard')});
                  hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
                return false;"/>
								</c:when>
								<c:otherwise>
							<%-- 	
							 <input value="<fmt:message key='creditCard.save.label'/>" 
              type="button" id="saveChoice" name="saveChoice" 
              onClick="addCreditCard();"/> --%>
                         <input value="Proceed" 
              type="button" id="saveChoice" name="saveChoice" 
              onClick="<c:choose><c:when test="${empty paramId ? 'true' : 'false'}">
               startTokenization();
               hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
               return false;
                </c:when><c:otherwise>
                document.getElementById('editCreditCardForm')['/atg/commerce/custsvc/repository/CreditCardFormHandler.value.creditCardNumber']['disabled'] = 
                    ('${maskedCreditCardNumberVar}' == dijit.byId('editCreditCardForm_maskedCreditCardNumber').getValue());
                  document.getElementById('editCreditCardForm').createCreditCardAction['disabled'] = true;
                  document.getElementById('editCreditCardForm').updateCreditCardAction['disabled'] = false;
                </c:otherwise></c:choose>
                editCreditCardAddress();"/>
								
								</c:otherwise>
								</c:choose>
						
						</c:when>
						
						<c:otherwise>
					<c:choose>
					<c:when test="${error}">
					
					<span style="color:red;margin-bottom:15px;float: left;width: 100%;font-weight: bold;">Sorry, an error occured</span>
						</c:when>
						<c:otherwise>
						<div class="row">
								<c:choose>
								<c:when test="${not addressRecognized }">
									<div class="col-md-2">
									<%-- <button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}"><fmt:message key="proceed.to.shipping.method" /></button>										
								 --%>	<button class="address-doctor-select" type="button" value="yes" class="im-mature-btn" onclick="atgNavigate({
                   					 panelStack : 'cmcShippingMethodPS'});hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});"><fmt:message key="proceed.to.shipping.method" bundle="${TRUCustomResources}"/></button>										
									</div>
								</c:when>
								<c:otherwise>
										<div class="col-md-2">
										
										<button type="button" value="yes" class="im-mature-btn shippingAddBtn"
											onclick="shipToAddressFromOverlay();"
											id="${shipId}"><fmt:message key="proceed.to.checkout" bundle="${TRUCustomResources}"/></button>
											
									<%-- 		<button type="button" value="yes" class="im-mature-btn" onclick="atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'},'${addrKey}');" id="${shipId}">Proceed to Shipping Method</button>																					
								 --%>	</div>
								</c:otherwise>
								</c:choose>
							</div>
						</c:otherwise>
						</c:choose>
						</c:otherwise>
					</c:choose>
				</div>					
			</div>				
		</div>
	</div>
</div>
    <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingMethodPS" init="true"/>
    <svc-ui:frameworkUrl var="errorURL" panelStacks="cmcShippingAddressPS"/>

	<dsp:form id="shipToAddressFromSuggested" method="POST">
	
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.firstName" name="firstNameAO" id="firstNameAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.lastName" name="lastNameAO" id="lastNameAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address1" name="address1AO" id="address1AO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address2" name="address2AO" id="address2AO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.city" name="cityAO" id="cityAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.state" name="stateAO" id="stateAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.postalCode" name="postalCodeAO" id="postalCodeAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.phoneNumber" name="phoneNumberAO" id="phoneNumberAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.country" name="countryAO" id="countryAO"/>
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.newShipToAddressName" name="shipToNewAddressAO" id="shipToNewAddressAO"/>
		<input type="hidden" name="shippingMethod" value="${shippingMethod}" />
		<input type="hidden" name="shippingPrice" value="${shippingPrice}" />
		<input type="hidden" name="showMeGiftingOptions" value="${showMeGiftingOptions}" />
		
      <dsp:input type="hidden" priority="-10" value="" bean="ShippingGroupFormHandler.shipToNewAddress"/>
      <dsp:input type="hidden" value="${errorURL}" name="errorURL" bean="ShippingGroupFormHandler.commonErrorURL"/>
      
      <dsp:input type="hidden" value="${successURL}" name="successURL2" id="successURL2" bean="ShippingGroupFormHandler.commonSuccessURL"/>
	</dsp:form>
	
	
	
	
	
	<script type="text/javascript">
		function closeModal(){
			hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
		};
		

		function checkRadioBtnChecked()
		{
			setTimeout(function(){
				checkRadioBtn();
			},200);
		}
		function checkRadioBtn()
		{
			var selected = $("#csrAddressDoctorFloatingPane").find("input[type='radio']:checked").length;
			
			if(selected == 0)
				{
					$(".shippingAddBtn,.editSaveBtn").attr('disabled',true);
				}
			else
				{
					$(".shippingAddBtn,.editSaveBtn").attr('disabled',false);
				}
		}
		$(document).ready(function(){
			checkRadioBtn();
			$(".select-valid-address").on("click",function(){
				if($(this).is(":checked"))
					{
						$(".shippingAddBtn").attr('disabled',false);
					}
			})
		})
		

		
		function addAddress(){
			
			var checkedRadioAddress = $('.select-valid-address:checked').attr('id');
			
			if(checkedRadioAddress == 'suggestedAddressCount-0')
			{
				//atg.commerce.csr.order.shipping.shipToAddress({form:'singleShippingAddressForm'}, addrKeyValue);
			//atgNavigate({panelStack : 'cmcBillingPS'});
				startTokenization();
			 	hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			}
			else{
				startTokenization();
			 	hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			}
			
		}
 		function addAccountAddress(){
 			var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
 	     	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
 	     	if(addressFlag != "0"){
 	     		 country= getSuggestedCountry(addressFlag);
 				 address1 = $('#suggestedAddress1-'+addressFlag).val();
 	   			 city = $('#suggestedCity-'+addressFlag).val();
 	   			 state = $('#suggestedState-'+addressFlag).val();
 	   			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
 	   			 address2 = $('#suggestedAddress2-'+addressFlag).val();
 	   			 phoneNumber = $('#profileAddressEditorForm_phoneNumber').val();
 	   			 firstName = $('#profileAddressEditorForm_firstName').val();
 	   			 middleName = $('#profileAddressEditorForm_middleName').val();
 	   			 lastName = $('#profileAddressEditorForm_lastName').val();
 	   			 state1 = state.split("-")[0];
 		     	 var state = $.trim(state1);
 		     	
				$("#profileAddressEditorForm_firstName").val(firstName);
		 		$("#profileAddressEditorForm_middleName").val(middleName);
		 		$("#profileAddressEditorForm_lastName").val(lastName);
		 		$("#profileAddressEditorForm_country").val(country);
		 		$("#profileAddressEditorForm_address1").val(address1);
		 		$("#profileAddressEditorForm_address2").val(address2);
		 		$("#profileAddressEditorForm_city").val(city);
		 		$("#profileAddressEditorForm_state").val(state);
		 		$("#profileAddressEditorForm_postalCode").val(postalCode);
		 		$("#profileAddressEditorForm_phoneNumber").val(phoneNumber); 
 		
	  			} 
 	     	else{
 	     		country = getCountry();
 	 	    	$("#profileAddressEditorForm_country").val(country);
 	     	}
 	     	atgSubmitPopup({url: '${url}',form: dojo.byId('profileAddressEditorForm'),
 	        popup: getEnclosingPopup('profileAddressEditorForm_nickName')});
 	     	hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
 	     	atgNavigate({panelStack:"customerPanels", queryParams: { init : 'true' }});return false;
 		}
		
		atg.commerce.csr.order.billing.editCreditCard = function(pURL){
			  atg.commerce.csr.common.submitPopup(pURL, document.getElementById("csrBillingEditCreditCard"), dijit.byId("editPaymentOptionFloatingPane"));
			  hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
		};
		function getSuggestedCountry(addressFlag)
        {
        	var countrySelected = $("#suggestedCountry-"+addressFlag).val();
        	var country;
        	
        	if(countrySelected == 'United States' || countrySelected =='UNITED STATES')
        		{
        			country ='US';
        		}
        	else
        		{
        			country = countrySelected;
        		}
        	return country;
        }
		
		
function addCreditCard(){
				startTokenization();
		}
		
function editCreditCardAddress(){
	
	var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
  	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
  	
  	if(addressFlag != "0"){
  		 country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
			 city = $('#suggestedCity-'+addressFlag).val();
			 state = $('#suggestedState-'+addressFlag).val();
			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
			 address2 = $('#suggestedAddress2-'+addressFlag).val();
			 phoneNumber = $('#editCreditCardForm_phoneNumber').val();
			 firstName = $('#editCreditCardForm_firstName').val();
			 middleName = $('#editCreditCardForm_middleName').val();
			 lastName = $('#editCreditCardForm_lastName').val();
			 state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
	     	
		$("#editCreditCardForm_firstName").val(firstName);
 		$("#editCreditCardForm_middleName").val(middleName);
 		$("#editCreditCardForm_lastName").val(lastName);
 		$("#editCreditCardForm_country").val(country);
 		$("#editCreditCardForm_address1").val(address1);
 		$("#editCreditCardForm_address2").val(address2);
 		$("#editCreditCardForm_city").val(city);
 		$("#editCreditCardForm_state").val(state);
 		$("#editCreditCardForm_postalCode").val(postalCode);
 		$("#editCreditCardForm_phoneNumber").val(phoneNumber); 
	
			} 
  		
    	atgSubmitPopup({url: '${urls}', 
        form: document.getElementById('editCreditCardForm'),
        popup: getEnclosingPopup('atg_commerce_csr_editCreditCard')});
    	hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
     
        return false;
}
function inStorePaymentPopupSubmit(){
	var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
  	if(addressFlag != "0"){
  		 country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
			 city = $('#suggestedCity-'+addressFlag).val();
			 state1 = $('#suggestedState-'+addressFlag).val();
			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
			 address2 = $('#suggestedAddress2-'+addressFlag).val();
			 phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
			 firstName = $('#csrBillingAddCreditCard_firstName').val();
			 middleName = $('#csrBillingAddCreditCard_middleName').val();
			 lastName = $('#csrBillingAddCreditCard_lastName').val();
			/*  state1 = state.split("-")[0];
	     	 var state = $.trim(state1);  */
  	}
  	else{
	
	     var firstName = $('#csrBillingAddCreditCard_firstName').val();
		 var middleName = $('#csrBillingAddCreditCard_middleName').val();
		 var lastName = $('#csrBillingAddCreditCard_lastName').val();
		 var country=dijit.byId("csrBillingAddCreditCard_country");
		 var address1 = $('#csrBillingAddCreditCard_address1').val();
		 var address2 = $('#csrBillingAddCreditCard_address2').val();
		 var city = $('#csrBillingAddCreditCard_city').val();
		 var state1 = $('#csrBillingAddCreditCard_state').val();
		 var postalCode = $('#csrBillingAddCreditCard_postalCode').val();
		 var phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
		 var address2 = $('#csrBillingAddCreditCard_address2').val();
		 
  	}
		if(country != 'US')
		{
			if(state1 == 'NA')
				{
					var state = 'NA'
				}
		}
		else{
			state1 = state1.split("-")[0];
			var state = $.trim(state1);
		}
	  
	 var form= document.getElementById('inStorePayment');
 	 $("#inStorePayment").find("#firstNameRA").val(firstName);
	 $("#inStorePayment").find("#lastNameRA").val(lastName);
	 $("#inStorePayment").find("#address1RA").val(address1);
	 $("#inStorePayment").find("#address2RA").val(address2);
	 $("#inStorePayment").find("#cityRA").val(city);
	 $("#inStorePayment").find("#stateRA").val(state);
	 $("#inStorePayment").find("#postalCodeRA").val(postalCode);
	 $("#inStorePayment").find("#phoneNumberRA").val(phoneNumber);
	 $("#inStorePayment").find("#countryRA").val(country);
	  
	 
	  if($('#email').length == 1){
	  var email=dojo.byId("email").value;
	   if(email == '')
		   {
		   $('.email').find("#emailErrMsg").remove();
		   	$('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Please enter your email address</span>");
		   	return false;
		   }
	   else
		   {
			 if (!isValidEmail(email)) {
				   $('.email').find("#emailErrMsg").remove();
				   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
				   return false;
			  }
		 }
	   form.emailInstorePayment.value=email; 
	  }
		 atgSubmitAction({form : form,panels : ['cmcCompleteOrderP'], panelStack : 'cmcCompleteOrderPS'   			
		});  
		 hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
	} 
function continueToOrderReview(){
	var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
  	
	if(addressFlag != "0"){
 		 country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
			 city = $('#suggestedCity-'+addressFlag).val();
			 state1 = $('#suggestedState-'+addressFlag).val();
			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
			 address2 = $('#suggestedAddress2-'+addressFlag).val();
			 phoneNumber = $('#editCreditCardForm_phoneNumber').val();
			 firstName = $('#editCreditCardForm_firstName').val();
			 middleName = $('#editCreditCardForm_middleName').val();
			 lastName = $('#editCreditCardForm_lastName').val();
			/*  state1 = state.split("-")[0];
	     	 var state = $.trim(state1); */
 	}
 	else{
	
    var firstName = $('#csrBillingAddCreditCard_firstName').val();
	 var middleName = $('#csrBillingAddCreditCard_middleName').val();
	 var lastName = $('#csrBillingAddCreditCard_lastName').val();
	 var country=dijit.byId("csrBillingAddCreditCard_country");
	 var address1 = $('#csrBillingAddCreditCard_address1').val();
	 var address2 = $('#csrBillingAddCreditCard_address2').val();
	 var city = $('#csrBillingAddCreditCard_city').val();
	 var state1 = $('#csrBillingAddCreditCard_state').val();
	 var postalCode = $('#csrBillingAddCreditCard_postalCode').val();
	 var phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
	 var address2 = $('#csrBillingAddCreditCard_address2').val();
 	}
	if(country != 'US')
	{
		if(state1 == 'NA')
			{
				var state = 'NA'
			}
	}
	else{
		state1 = state1.split("-")[0];
		var state = $.trim(state1);
	}
	
		 $("#csrBillingForm").find("#firstNameRA").val(firstName);
   	 $("#csrBillingForm").find("#lastNameRA").val(lastName);
   	 $("#csrBillingForm").find("#address1RA").val(address1);
   	 $("#csrBillingForm").find("#address2RA").val(address2);
   	 $("#csrBillingForm").find("#cityRA").val(city);
   	 $("#csrBillingForm").find("#stateRA").val(state);
   	 $("#csrBillingForm").find("#postalCodeRA").val(postalCode);
   	 $("#csrBillingForm").find("#phoneNumberRA").val(phoneNumber);
   	 $("#csrBillingForm").find("#countryRA").val(country);
		 atg.commerce.csr.order.billing.applyPaymentGroups({form:'csrBillingForm'});
		 hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
	
}
	</script>
	</c:otherwise>
	</c:choose>
</dsp:page>