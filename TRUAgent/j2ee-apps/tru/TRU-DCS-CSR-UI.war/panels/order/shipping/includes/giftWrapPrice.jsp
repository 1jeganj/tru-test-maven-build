<%@ include file="/include/top.jspf"%>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:page>
	<dsp:importbean bean="/com/tru/commerce/droplet/TRUPriceDroplet" />
	<dsp:droplet name="TRUPriceDroplet">
		<dsp:param name="productId" param="defaultWrapProductId" />
		<dsp:param name="skuId" param="defaultWrapSkuId" />
		<dsp:oparam name="true">
			<header>add gift wrap to this item for <dsp:valueof format="currency" param="salePrice" locale="en_US" converter="currency"/></header>
		</dsp:oparam>
		<dsp:oparam name="false">
			<header>add gift wrap to this item for <dsp:valueof format="currency" param="salePrice" locale="en_US" converter="currency"/></header>
		</dsp:oparam>
		 <dsp:oparam name="empty">
			<header>add gift wrap to this item for <dsp:valueof format="currency" param="salePrice" locale="en_US" converter="currency"/></header>
		</dsp:oparam> 
	</dsp:droplet>
</dsp:page>   