<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
 
<dsp:importbean bean="com/tru/commerce/order/droplet/TRUCSRAddressSuggestionsDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	

	<dsp:getvalueof var="address1" param="address1"/>
	<dsp:getvalueof var="address2" param="address2"/>
	<dsp:getvalueof var="city" param="city"/>
	<dsp:getvalueof var="state" param="state"/>
	
	<dsp:getvalueof var="postalCode" param="postalCode"/>
			 <dsp:droplet name="TRUCSRAddressSuggestionsDroplet">
				
				<dsp:param value="${address1}" name="address1" />
				<dsp:param value="${address2}" name="address2" />
				<dsp:param value="${city}" name="city" />
				<dsp:param value="${state}" name="state" />
				<dsp:param value="${postalCode}" name="postalCode" />
				<dsp:oparam name="output">
				</dsp:oparam>
			</dsp:droplet>  
			
			<dsp:getvalueof var="derivedStatus" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.derivedStatus" />
			<dsp:getvalueof var="addressRecognized" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressRecognized" />
			
			<dsp:getvalueof var="address" vartype="java.lang.boolean" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO" />
				<dsp:droplet name="IsEmpty">
						<dsp:param name="value" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
						<dsp:oparam name="false">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="/com/tru/commerce/AddressDoctor.addressDoctorResponseVO.addressDoctorVO" />
									<dsp:param name="elementName" value="addressDoctorVO" />
									<dsp:oparam name="output">
										<p id="derivedStatus">${derivedStatus}</p>
										<p id="addressDoctorVO1">${addressRecognized}</p>
										<dsp:getvalueof var="size" param="size"/>
										<dsp:getvalueof var="addressDoctorVO" param="addressDoctorVO"/>
												<dsp:valueof param="addressDoctorVO.address1" />
												<dsp:getvalueof var="suggestedAddressCount" param="addressDoctorVO.count"/>
										<div id="firstSuggestedAddress">
											<input type="hidden" name="suggestedAddress1-${suggestedAddressCount}" id="suggestedAddress1-${suggestedAddressCount}" value="${addressDoctorVO.address1}" maxlength="50"/>
											<input type="hidden" name="suggestedAddress2-${suggestedAddressCount}" id="suggestedAddress2-${suggestedAddressCount}" value="${addressDoctorVO.address2}" maxlength="50"/>
											<input type="hidden" name="suggestedCity-${suggestedAddressCount}" id="suggestedCity-${suggestedAddressCount}" value="${addressDoctorVO.city}" maxlength="30"/>
											<input type="hidden" name="suggestedState-${suggestedAddressCount}" id="suggestedState-${suggestedAddressCount}" value="${addressDoctorVO.state}" />
											<input type="hidden" name="suggestedPostalCode-${suggestedAddressCount}" id="suggestedPostalCode-${suggestedAddressCount}" value="${addressDoctorVO.postalCode}" />
											<input type="hidden" name="suggestedCountry-${suggestedAddressCount}" id="suggestedCountry-${suggestedAddressCount}" value="${addressDoctorVO.country}" />
										</div>
									</dsp:oparam>
								</dsp:droplet>
						</dsp:oparam >
						<dsp:oparam name="true">
							<p id="derivedStatus">${derivedStatus}</p>
							<p id="addressDoctorVO1">${addressRecognized}</p>
						</dsp:oparam>
					</dsp:droplet>
	
</dsp:page>