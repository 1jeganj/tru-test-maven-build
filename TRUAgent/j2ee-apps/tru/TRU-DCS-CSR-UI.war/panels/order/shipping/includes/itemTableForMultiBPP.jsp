<%--
This page defines the address view
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/itemTable.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" />
  <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:getvalueof var="order" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <dsp:importbean var="CSRConfigurator" bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:getvalueof var="shippingGroupId" param="shippingGroupId"/>
  <dsp:importbean bean="/atg/multisite/Site"/> 
  <dsp:getvalueof var="currentSiteId" bean="Site.id"/>

 
	

    <table class="atg_dataTable atg_commerce_csr_innerTable">
      <thead>
      <%-- Site Icon Heading --%>
      <c:if test="${isMultiSiteEnabled == true}">
        <th class="atg_commerce_csr_siteIcon"></th>
      </c:if>
  <!--      <th style="width:30%">
       Message
      </th> -->
      <th>
        <fmt:message key="shippingSummary.commerceItem.header.itemDesc"/>
      </th>
     
      <th>
        <fmt:message key="shippingSummary.commerceItem.header.status"/>
      </th>
      <th class="atg_numberValue" style="width:15%">
        <fmt:message key="shippingSummary.commerceItem.header.qty"/>
      </th>
      </thead>
      <%-- <c:forEach items="${order.shippingGroups[shippingGroupIndex].commerceItemRelationships}" var="cisiItem"> --%>
      <dsp:droplet name="CartItemDetailsDroplet">
						<dsp:param name="order" bean="ShoppingCart.current" />
						<dsp:param name="isCSCRequest" value="true"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="items" param="items" />
							</dsp:oparam>
							</dsp:droplet>
		<c:forEach items="${items}" var="item">
				<dsp:getvalueof var="skuItem" value="${item.skuItem}"/>
				<dsp:getvalueof var="itemSiteId" value="${tem.siteId}"/>
				<dsp:getvalueof var="catalogRefId" value="${item.skuId}"/>
				<dsp:getvalueof var="itemShippingGroupId" value="${item.shipGroupId}"/>
				<dsp:getvalueof var="itemMetaQty" value="${item.quantity}"/>
				<dsp:getvalueof var="itemQty" value="${item.relItemQtyInCart}"/>
				<c:if test="${shippingGroupId eq itemShippingGroupId}">
				
        <dsp:tomap var="sku" value="${skuItem}"/>
        
        <tr>
          <c:if test="${isMultiSiteEnabled == true}">
            <c:set var="siteId" value="${itemSiteId}"/>
            <td class="atg_commerce_csr_siteIcon">
              <csr:siteIcon siteId="${siteId}" />
            </td>
          </c:if>
           <td>
            <ul class="atg_commerce_csr_itemDesc">
              <c:choose>
                <c:when test="${(isMultiSiteEnabled == true) && (isSiteDeleted != true)}">
                  <li><a title="<fmt:message key='cart.items.quickView' />" href="#" onclick="atg.commerce.csr.common.showPopupWithReturn(
                    {popupPaneId: 'productQuickViewPopup',
                     title: '${fn:escapeXml(sku.displayName)}',
                     url: '${CSRConfigurator.truContextRoot}/include/order/product/productReadOnly.jsp?_windowid=${windowId}&commerceItemId=${cisiItem.commerceItem.id}&siteId=${siteId}',
                     onClose: function(args) { }});return false;">${fn:escapeXml(sku.displayName)}</a></li>
                </c:when>
                <c:otherwise>
                  <li><a title="<fmt:message key='cart.items.quickView' />" href="#" onclick="atg.commerce.csr.common.showPopupWithReturn(
                    {popupPaneId: 'productQuickViewPopup',
                     title: '${fn:escapeXml(sku.displayName)}',
                     url: '${CSRConfigurator.truContextRoot}/include/order/product/productReadOnly.jsp?_windowid=${windowId}&commerceItemId=${cisiItem.commerceItem.id}',
                     onClose: function(args) {}});return false;">${fn:escapeXml(sku.displayName)}</a></li>
                </c:otherwise>
              </c:choose>
            <li>${catalogRefId}</li>
            <li>
            	<dsp:include src="/include/gwp/buyersProtectionPlan.jsp"
										otherContext="${CSRConfigurator.truContextRoot}">
										<dsp:param name="pageName" value="Multishipping" />
										<dsp:param name="item" value="${item}" />
										<dsp:param name="multiShipping" value="true" />
									</dsp:include>
            </li>
             </ul>
          
          </td>
          <td>
            <csr:inventoryStatus commerceItemId="${catalogRefId}"/>
          </td>
          <td class="atg_numberValue">
           <c:choose>
							 <c:when test="${not empty itemMetaQty}">
								${itemMetaQty}
							</c:when>
							<c:otherwise>
								${itemQty}
							</c:otherwise>
					  </c:choose>
          </td>
        </tr>
        </c:if>
        </c:forEach>
      
    </table>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/itemTable.jsp#1 $$Change: 875535 $--%>