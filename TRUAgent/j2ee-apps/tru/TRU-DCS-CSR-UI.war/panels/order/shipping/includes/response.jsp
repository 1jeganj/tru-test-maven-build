<%--
This page defines the address view in the single shipping address page.

This page is basically used in the Ship To Address section which is the top page in the 
shipping address page.

Expected params
shippingGroup : The shipping group from which the shipping group information is going to be retrieved.
addressKey : The current shipping shipping group name or the map key.
formId : the current form id in which the form elements are rendered.
selectedNickname : the current shipping group name which is used to ship all the items. If all commerce
                   items in the order is pointing to the same shipping group name, then that shipping address
                   should be pre selected in UI. This shipping group name allows us to identify that shipping
                   group.
commonShippingGroupTypes : This parameter tells the common shipping group types (intersection of all commerce items 
                           shipping group types) for the entire order.


@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/addressView.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <div id="inStorePickupResultsss">
  </div>


</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/includes/addressView.jsp#1 $$Change: 875535 $--%>