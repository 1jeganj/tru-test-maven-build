<%--
This page defines the shipping method panel
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/shippingMethod.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>

<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>
    <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
    <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:getvalueof var="order" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
  <dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
  <dsp:importbean var="shippingMethodNextStep" bean="/atg/commerce/custsvc/ui/fragments/order/ShippingMethodNextStep"/>
  <dsp:importbean var="pageFragment" bean="/atg/commerce/custsvc/ui/fragments/order/DisplayInStorePickupShippingGroup"/>
    <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" />
  <dsp:setLayeredBundle basename="atg.commerce.csr.order.WebAppResources"/>
  <style>
  #csrSelectShippingMethods .atg_commerce_csr_addressView
  {
      width: 100%;
    margin-bottom: 10px;
  }
  </style>
  <script type="text/javascript">

function addUpdateBppItem(relationShipId, bppInfoId, bppSkuId, bppItemId,  pageName) {
	debugger;
  	   	$("#bppSkuId").val(bppSkuId);
  	  	$("#bppInfoId").val(bppInfoId);
  	  	$("#bppItemId").val(bppItemId);
  	  	$("#relationShipId").val(relationShipId);
  	  if(pageName == "cart"){
  	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});
  	  }
  	if(pageName == "Review"){
  	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});
  	  }
  	  if(pageName == "Multishipping"){
  		if($(".instorePickupPage").length){
  			dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
     			if(!(_1f instanceof Error)){
     				atgSubmitAction({
                        form :document.getElementById("instorePickupForm1"),
             			  panel : ['cmcShippingMethodP'],
             			  panelStack : 'cmcShippingMethodPS'
             			  });return false;
            		 }
    			},mimetype:"text/html"});
  		}
  		else{
  	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcShippingMethodPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});
  		}
  	  }
  	 }
    function removeBppItem(relationShipId, bppInfoId, pageName) {
  	   $("#relShipId").val(relationShipId);
  	  	$("#bppInfoId").val(bppInfoId);
  	  	$("#pageName").val(pageName);
  	  if(pageName == "cart"){
    	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
   			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
	        		 }
			},mimetype:"text/html"});
    	  }
    	if(pageName == "Review"){
    	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
   			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
	        		 }
			},mimetype:"text/html"});
    	  }
    	  if(pageName == "Multishipping"){
    		  if($(".instorePickupPage").length){
    			  dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
    	     			if(!(_1f instanceof Error)){
    	     				atgSubmitAction({
    	                        form :document.getElementById("instorePickupForm1"),
    	             			  panel : ['cmcShippingMethodP'],
    	             			  panelStack : 'cmcShippingMethodPS'
    	             			  });return false;
    	            		 }
    	    			},mimetype:"text/html"});
    	  		}
    	  		else{
    	  			dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
    	  	   			if(!(_1f instanceof Error)){
    	  		     				atgNavigate({panelStack:"cmcShippingMethodPS", queryParams: { init : 'true' }});return false;
    	  		        		 }
    	  				},mimetype:"text/html"});
    	  		}
    	  	
    	  }
  	  	
  	   	 }
    function selectProtectionPlan(e,obj){	  
    	  
   	   var e = e || window.event;
   	   var checkbox = $(obj);
   	   var pageName = $(obj).attr('data-pageName');
   	   if (checkbox.is(":checked")) {  
   		  var relId = $(obj).attr('data-relId');
   		  var bppSkuId = $(obj).attr('data-bppSkuId');
   		  var bppItem = $(obj).attr('data-bppItemId');
   	  	  var bppItemCount = $(obj).attr('data-bppItemCount');
   	  	  var pageName=$(obj).attr('data-pageName');
   		  var bppItemId;		  
   		  $('.bppPlanMultiShippingPage').attr('disabled', false);
   		  if(!isEmpty(bppItemCount) && parseInt(bppItemCount) == 1) { 
   			  bppItemId = $(obj).attr('data-bppItemId'); 
   			  addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName, obj);
   		  }		  
   	  }else{
   		    var pageName=$(obj).attr('data-pageName');
   			var bppItemInfoId = $(obj).attr('data-bppInfoId');
   			var relId = $(obj).attr('data-relId');
   			if(!isEmpty(bppItemInfoId)) {
   				removeBppItem(relId, bppItemInfoId, pageName, obj);
   			}
   	  }
   	}
    </script>
    
		  <dsp:form id="addUpdateBppItemForm" formid="addUpdateBppItemForm">
			<dsp:input type="hidden" bean="CartModifierFormHandler.bppSkuId"
				name="bppSkuId" id="bppSkuId" />
			<dsp:input type="hidden" bean="CartModifierFormHandler.bppInfoId"
				name="bppInfoId" id="bppInfoId" />
			<dsp:input type="hidden" bean="CartModifierFormHandler.bppItemId"
				name="bppItemId" id="bppItemId" />
			<dsp:input type="hidden"
				bean="CartModifierFormHandler.relationShipId"
				name="relationShipId" id="relationShipId" />
			<dsp:input type="hidden"
				bean="CartModifierFormHandler.addUpdateBppItem" value="true" />
			
		  </dsp:form>

			<dsp:form id="removeBppItemForm" formid="removeBppItemForm">
				<dsp:input type="hidden" bean="CartModifierFormHandler.relationShipId" name="relationShipId" id="relShipId" />
				<dsp:input type="hidden" bean="CartModifierFormHandler.removeBPPItemRelationship" value="true" />
			</dsp:form>
  
  
   <dsp:form id="shippingMethodForm" formid="shippingMethodForm">
    <dsp:input id="newShipMethodCode" name="newShipMethodCode" type="hidden"  bean="ShippingGroupFormHandler.shipMethodCode" value=""/>
	<dsp:input id="newRegionCode" name="newRegionCode" type="hidden"  bean="ShippingGroupFormHandler.regionCode" value=""/>
	<dsp:input id="newShippingPrice" name="newShippingPrice" type="hidden"  bean="ShippingGroupFormHandler.shippingPrice" value=""/>
	<dsp:input id="test" name="test" type="hidden" bean="ShippingGroupFormHandler.changeShippingMethod"  value="true"/>
	</dsp:form>

	<dsp:form id="multiShippingMethodForm" formid="multiShippingMethodForm">
	    <dsp:input id="multiShipMethodCode" name="newShipMethodCode" type="hidden"  bean="ShippingGroupFormHandler.shipMethodVO.shipMethodCode" value=""/>
		<dsp:input id="multiRegionCode" name="newRegionCode" type="hidden"  bean="ShippingGroupFormHandler.shipMethodVO.regionCode" value=""/>
		<dsp:input id="multiShippingPrice" name="newShippingPrice" type="hidden"  bean="ShippingGroupFormHandler.shipMethodVO.shippingPrice" value=""/>
		<dsp:input id="newWrapCommerceItemId" name="newWrapCommerceItemId" type="hidden"  bean="ShippingGroupFormHandler.wrapCommerceItemId" value=""/>
		<dsp:input id="newUniqueId" name="newUniqueId" type="hidden"  bean="ShippingGroupFormHandler.uniqueId" value=""/>
		<dsp:input id="newRelationShipId" name="newRelationShipId" type="hidden"  bean="ShippingGroupFormHandler.relationShipId" value=""/>
		<dsp:input id="newShippingGroupId" name="newShippingGroupId" type="hidden"  bean="ShippingGroupFormHandler.shippingGroupId" value=""/>
		<dsp:input id="test" name="test" type="hidden" bean="ShippingGroupFormHandler.updateShipMethod"  value="true"/>
	</dsp:form>
	<dsp:form id="applyGiftMessage" formid="applyGiftMessage">
			<dsp:input bean="GiftOptionsFormHandler.giftOptionsInfoCount" value="1" type="hidden" id="giftOptionsInfoCount" name="giftOptionsInfoCount"/>
			<dsp:input bean="GiftOptionsFormHandler.giftOptionsInfo[0].shippingGroupId" paramvalue="shipGroupId" name="shippingGroupId" type="hidden" id="shippingGroupId"/>
			<dsp:input bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftReceipt" value="true" type="hidden" id="giftReceipt" name="giftReceipt"/>
			<dsp:input bean="GiftOptionsFormHandler.giftOptionsInfo[0].giftWrapMessage" name="giftMessage" type="hidden" id="giftMessage" paramvalue="giftMessage"/>
			<dsp:input bean="GiftOptionsFormHandler.applyGiftMessage" value="true" type="hidden"/>
	</dsp:form>
	
	<dsp:form id="giftOptionsFormCSC" formid="giftOptionsFormCSC">
            <dsp:input type="hidden" bean="GiftOptionsFormHandler.showMeGiftingOptions" name="ShowMeGiftingOptions" id="ShowMeGiftingOptions"/>
			<dsp:input type="hidden"  bean="GiftOptionsFormHandler.updateGiftOptions" value="true" />
	</dsp:form>
	
		<dsp:form id="applyGiftWrap" formid="applyGiftWrap">
	        <dsp:input bean="GiftOptionsFormHandler.shipGroupId"  id="shipGroupId" name="shipGroupId" type="hidden" />
			<dsp:input bean="GiftOptionsFormHandler.commerceItemRelationshipId"	id="commerceItemRelationshipId" name="commerceItemRelationshipId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.selectedId" id="selectedId" name="selectedId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.commerceItemId" id="commerceItemId" name="commerceItemId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.giftWrapProductId" id="giftWrapProductId" name="giftWrapProductId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.giftWrapSkuId" id="giftWrapSkuId" name="giftWrapSkuId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.previousGiftWrapSkuId" id="previousGiftWrapSkuId" name="previousGiftWrapSkuId" type="hidden" />
			<dsp:input bean="GiftOptionsFormHandler.giftWrapSelected" id="giftWrapSelected" name="giftWrapSelected" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.showMeGiftingOptions" value="true" type="hidden"></dsp:input>
			<dsp:input bean="GiftOptionsFormHandler.applyGiftWrap" value="true" type="hidden"/>
	</dsp:form>
	
	<dsp:form name="giftItemFormOnShipping" id="giftItemFormOnShipping">
			<dsp:input bean="GiftOptionsFormHandler.shipGroupId" id="ShipGroupId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.commerceItemRelationshipId" id="CommerceItemRelationshipId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.commerceItemId" id="CommerceItemId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.quantity" value="1" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.giftWrapProductId"  id="GiftWrapProductId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.giftWrapSkuId" id="GiftWrapSkuId" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.giftItem" id="GiftItem" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.selectedId"  id="SelectionId" type="hidden"/> 
			<dsp:input bean="GiftOptionsFormHandler.currentPage" value="gifting" type="hidden"/>
			<dsp:input bean="GiftOptionsFormHandler.updateGiftItem" value="true" type="hidden"/>
		</dsp:form>
	<script type="text/javascript">	
        function selectShippingMethod(e,obj){
        	
        	if($(".instorePickupPage").length){
	        		var differred = atgSubmitAction({
	                    form :document.getElementById("instorePickupForm1"),
	         			  });
	        		differred.addCallback(function(results) { 
	        	    	  var e = e || window.event;
	        	    	  var checkbox = $(obj); 
	        	    	  if (!(checkbox.is(":checked"))) { 
	        	  		    	$(obj).attr('checked','checked');
	        	  	        	e.preventDefault();
	        	  	        	return false;
	        	  	       } 
	        	    	  var clickedId = ($(obj).attr('id')).split('__');
	        	    	  var shipMethodCode = clickedId[0];
	        		      var shippingPrice = clickedId[1];
	        	       	  var regionCode = clickedId[2];
	        	       	  var form = document.getElementById("shippingMethodForm");
	        	       	  form.newShipMethodCode.value=shipMethodCode;
	        	       	  form.newShippingPrice.value=shippingPrice;
	        	       	  form.newRegionCode.value=regionCode;
	        	    	  atgSubmitAction({
	        	              form : form,
	        	   			  panel : ['cmcShippingMethodP'],
	        	   			  panelStack : 'cmcShippingMethodPS'
	        	   		 });
	        	      	});
	        	
	        	
        	}
        	else{
		    	  var e = e || window.event;
		    	  var checkbox = $(obj); 
		    	  if (!(checkbox.is(":checked"))) { 
		  		    	$(obj).attr('checked','checked');
		  	        	e.preventDefault();
		  	        	return false;
		  	       } 
		    	  var clickedId = ($(obj).attr('id')).split('__');
		    	  var shipMethodCode = clickedId[0];
			      var shippingPrice = clickedId[1];
		       	  var regionCode = clickedId[2];
		       	  var form = document.getElementById("shippingMethodForm");
		       	  form.newShipMethodCode.value=shipMethodCode;
		       	  form.newShippingPrice.value=shippingPrice;
		       	  form.newRegionCode.value=regionCode;
		    	  atgSubmitAction({
		              form : form,
		   			  panel : ['cmcShippingMethodP'],
		   			  panelStack : 'cmcShippingMethodPS'
		   		 });
      
       	}
       }
        
        function updateShippingMethod(obj, shippingGroupId, relationShipId, uniqueId, wrapCommerceItemId,shipMethodCode,shippingPrice,regionCode) {
        	
        	if($(".instorePickupPage").length){
	        		var differred = atgSubmitAction({
	                    form :document.getElementById("instorePickupForm1"),
	         			  panel : ['cmcShippingMethodP'],
	         			  panelStack : 'cmcShippingMethodPS'
	         			  });
	        		differred.addCallback(function(results) { 
	        	        var form = document.getElementById("multiShippingMethodForm");
	        	        form.multiShipMethodCode.value=shipMethodCode;
	        	        form.multiShippingPrice.value=shippingPrice;
	        	        form.multiRegionCode.value=regionCode;
	        	        form.newWrapCommerceItemId.value=wrapCommerceItemId;
	        	        form.newUniqueId.value=uniqueId;
	        	        form.newRelationShipId.value=relationShipId;
	        	        form.newShippingGroupId.value=shippingGroupId;
	        	      	atgSubmitAction({
	        	               form : form,
	        	     			  panel : ['cmcShippingMethodP'],
	        	     			  panelStack : 'cmcShippingMethodPS'
	        	     		 });
	                	});
        	}
        	else{
	        var form = document.getElementById("multiShippingMethodForm");
	        form.multiShipMethodCode.value=shipMethodCode;
	        form.multiShippingPrice.value=shippingPrice;
	        form.multiRegionCode.value=regionCode;
	        form.newWrapCommerceItemId.value=wrapCommerceItemId;
	        form.newUniqueId.value=uniqueId;
	        form.newRelationShipId.value=relationShipId;
	        form.newShippingGroupId.value=shippingGroupId;
	      	atgSubmitAction({
	               form : form,
	     			  panel : ['cmcShippingMethodP'],
	     			  panelStack : 'cmcShippingMethodPS'
	     		 });
        	}
        }
      </script>
  <c:set var="hardgoodShippingGroupCount" value="${0}"/>
  <c:set var="inStorePickupShippingGroupCount" value="${0}"/>
  <c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="status">
    <c:if test="${shippingGroup.shippingGroupClassType == 'hardgoodShippingGroup'}">
      <c:set var="hardgoodShippingGroupCount" value="${hardgoodShippingGroupCount + 1}"/>
      <c:set var="hardgoodShippingGroupIndex" value="${status.index}"/>
      <c:set var="hardgoodShippingGroup" value="${shippingGroup}"/>
    </c:if>
    <c:if test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
      <c:set var="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount + 1}"/>
      <c:set var="inStorePickupShippingGroupIndex" value="${status.index}"/>
      <c:set var="inStorePickupShippingGroup" value="${shippingGroup}"/>
    </c:if>
  </c:forEach>
  
  <c:set var="multipleGroups" value="${hardgoodShippingGroupCount + inStorePickupShippingGroupCount > 1}" />
  <div class="atg_svc_content atg_commerce_csr_content">
    <svc-ui:frameworkUrl var="errorURL" panelStacks="cmcShippingMethodPS"/>
    <dsp:form name="instorePickupForm1" id="instorePickupForm1">
			<dsp:input type="hidden" value="${errorURL}"
                 bean="ShippingGroupFormHandler.applyShippingMethodsErrorURL"/>
			<dsp:input type="hidden" value="" priority="-10"
                 bean="ShippingGroupFormHandler.updateInStorePickUpData"/>
             <c:set var="shipmentIndex" value="${0}"/>
		<c:choose>
        <c:when test="${inStorePickupShippingGroupCount == 1}">
          <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
		   <%--  <dsp:getvalueof var="order" param="order"/> --%>
		    <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
		    <div class="atg_commerce_csr_addressView">
		      <h4>
		        <dsp:include src="${pageFragment.URL}" otherContext="${CSRConfigurator.truContextRoot}">
		          <dsp:param name="shippingGroup" value="${shippingGroup}"/>
		          <dsp:param name="propertyName" value="value1"/>
		          <dsp:param name="displayHeading" value="${true}"/>
		          <dsp:param name="shippingGroupIndex" value="${shippingGroupIndex}"/>
		          <dsp:param name="displaySelectButton" value="${false}"/>
		          <dsp:param name="displayAuthorizedForm" value="${true}"/>
		        </dsp:include>
		      </h4>
		      </div>
		       <c:if test="${multipleGroups}">
		      	<c:set var="shipmentIndex" value="${shipmentIndex + 1}"/>
		      </c:if>
        </c:when>

        <c:when test="${inStorePickupShippingGroupCount > 1}">
          <%-- Please note: There are intentionally 2 counters in this loop:
          "status" gets updated for each iteration so that we correctly index
          into the shipping group array in the submitted form.
          "shipmentIndex" gets updated only for hardgood iterations with a physical address --%>
         <%--  <c:set var="shipmentIndex" value="${0}"/> --%>
          <%-- <c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="status">
            <c:if test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
              <c:set var="inStorePickupShippingGroup" value="${shippingGroup}"/>
              <fieldset>
                <legend>
                  <fmt:message key="shipping.chooseMethod.shippingGroupNumber">
                    <fmt:param value="${shipmentIndex + 1}"/>
                  </fmt:message>
                </legend>
                
                <dsp:include src="/panels/order/shipping/includes/shippingGroupAuthorizedRecipientView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dsp:param name="shippingGroupIndex" value="${status.index}"/>
                  <dsp:param name="order" value="${order}"/>
                  <dsp:param name="shippingGroup" value="${inStorePickupShippingGroup}"/>
                </dsp:include>
                <dsp:include src="/panels/order/shipping/includes/itemTable.jsp" otherContext="${CSRConfigurator.truContextRoot}"> 
                  <dsp:param name="shippingGroupIndex" value="${shipmentIndex}"/>
                </dsp:include>
              </fieldset>
              <c:set var="shipmentIndex" value="${shipmentIndex + 1}"/>
            </c:if>
          </c:forEach> --%>
	           <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
			 <%--    <dsp:getvalueof var="order" param="order"/> --%>
			    <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
			    <div class="atg_commerce_csr_addressView">
			      <h4>
			        <dsp:include src="${pageFragment.URL}" otherContext="${CSRConfigurator.truContextRoot}">
			          <dsp:param name="shippingGroup" value="${shippingGroup}"/>
			          <dsp:param name="propertyName" value="value1"/>
			          <dsp:param name="displayHeading" value="${true}"/>
			          <dsp:param name="shippingGroupIndex" value="${shippingGroupIndex}"/>
			          <dsp:param name="displaySelectButton" value="${false}"/>
			          <dsp:param name="displayAuthorizedForm" value="${true}"/>
			        </dsp:include>
			      </h4>
			      </div>
        </c:when>
        <c:otherwise>
          <%-- No shipping groups --%>
        </c:otherwise>
      </c:choose>
	</dsp:form>
    <c:set var="formId" value="csrSelectShippingMethods"/>
    <dsp:form id="${formId}" formid="${formId}">
  
    
      <dsp:input type="hidden" value="${errorURL}"
                 bean="ShippingGroupFormHandler.applyShippingMethodsErrorURL"/>
		 <dsp:input id="hiddenShipInput" type="hidden" priority="10"
                 bean="ShippingGroupFormHandler.action" />
      <dsp:input type="hidden" value="" priority="-10"
                 bean="ShippingGroupFormHandler.applyShippingMethods"/>
      <dsp:input type="hidden" value="false"
                 bean="ShippingGroupFormHandler.persistOrder"/>
      <dsp:include src="${shippingMethodNextStep.URL}" otherContext="${shippingMethodNextStep.servletContext}">
      </dsp:include>
	      <c:choose>
        <c:when test="${hardgoodShippingGroupCount == 1 && inStorePickupShippingGroupCount == 0}">
       <p>
        <fmt:message key="newOrderSingleShipping.selectShippingMethod"/>
      </p>
          <c:if test="${multipleGroups}">
            <fieldset>
              <legend>
                <fmt:message key="shipping.chooseMethod.shippingGroupNumber">
                  <fmt:param value="${shipmentIndex + 1}"/>
                </fmt:message>
              </legend>
          </c:if>
         <dsp:include src="/panels/order/shipping/includes/shippingMethodPicker.jsp" otherContext="${CSRConfigurator.truContextRoot}">
        <dsp:param name="shippingGroupIndex" value="${hardgoodShippingGroupIndex}"/>
        <dsp:param name="order" value="${order}"/>
        <dsp:param name="shippingGroup" value="${hardgoodShippingGroup}"/>
        <dsp:param name="HGSGCount" value="${hardgoodShippingGroupCount}"/>
      </dsp:include>
          <dsp:include src="/panels/order/shipping/includes/shippingGroupMethodView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
            <dsp:param name="shippingGroupIndex" value="${hardgoodShippingGroupIndex}"/>
            <dsp:param name="order" value="${order}"/>
            <dsp:param name="shippingGroup" value="${hardgoodShippingGroup}"/>
          </dsp:include>
					<c:if test="${multipleGroups}">
						<%-- <dsp:include src="/panels/order/shipping/includes/itemTable.jsp"
							otherContext="${CSRConfigurator.truContextRoot}">
							<dsp:param name="shippingGroupIndex" value="${shipmentIndex}" />
						</dsp:include> --%>
						<dsp:include
							src="/panels/order/shipping/includes/itemTableForMultiBPP.jsp"
							otherContext="${CSRConfigurator.truContextRoot}">
							<dsp:param name="shippingGroupIndex" value="${shipmentIndex}" />
							<dsp:param name="shippingGroupId" value="${hardgoodShippingGroup.id}" />
							
						</dsp:include>
						<c:set var="shipmentIndex" value="${shipmentIndex + 1}" />
						</fieldset>
					</c:if>

				</c:when>
    <c:when test="${hardgoodShippingGroupCount == 1 && inStorePickupShippingGroupCount != 0}">
        <c:forEach items="${hardgoodShippingGroup.commerceItemRelationships}"
                      var="ciRelationship" varStatus="ciIndex">
        <c:if test="${ciRelationship.commerceItem.commerceItemClassType != 'donationCommerceItem'}">
       <p>
        <fmt:message key="newOrderSingleShipping.selectShippingMethod"/>
      </p>
          <c:if test="${multipleGroups}">
            <fieldset>
              <legend>
                <fmt:message key="shipping.chooseMethod.shippingGroupNumber">
                  <fmt:param value="${shipmentIndex + 1}"/>
                </fmt:message>
              </legend>
          </c:if>
         <dsp:include src="/panels/order/shipping/includes/shippingMethodPicker.jsp" otherContext="${CSRConfigurator.truContextRoot}">
        <dsp:param name="shippingGroupIndex" value="${hardgoodShippingGroupIndex}"/>
        <dsp:param name="order" value="${order}"/>
        <dsp:param name="shippingGroup" value="${hardgoodShippingGroup}"/>
        <dsp:param name="HGSGCount" value="${hardgoodShippingGroupCount}"/>
      </dsp:include>
          <dsp:include src="/panels/order/shipping/includes/shippingGroupMethodView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
            <dsp:param name="shippingGroupIndex" value="${hardgoodShippingGroupIndex}"/>
            <dsp:param name="order" value="${order}"/>
            <dsp:param name="shippingGroup" value="${hardgoodShippingGroup}"/>
          </dsp:include>
					<c:if test="${multipleGroups}">
						<%-- <dsp:include src="/panels/order/shipping/includes/itemTable.jsp"
							otherContext="${CSRConfigurator.truContextRoot}">
							<dsp:param name="shippingGroupIndex" value="${shipmentIndex}" />
						</dsp:include> --%>
						<dsp:include
							src="/panels/order/shipping/includes/itemTableForMultiBPP.jsp"
							otherContext="${CSRConfigurator.truContextRoot}">
							<dsp:param name="shippingGroupIndex" value="${shipmentIndex}" />
							<dsp:param name="shippingGroupId" value="${hardgoodShippingGroup.id}" />
							
						</dsp:include>
						<c:set var="shipmentIndex" value="${shipmentIndex + 1}" />
						</fieldset>
					</c:if>
					</c:if>
			</c:forEach>
		</c:when>
        <c:when test="${hardgoodShippingGroupCount > 1}">
          <%-- Please note: There are intentionally 2 counters in this loop:
          "status" gets updated for each iteration so that we correctly index
          into the shipping group array in the submitted form.
          "shipmentIndex" gets updated only for hardgood iterations with a physical address --%>
          <dsp:getvalueof var="order" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
          <c:set var="shipmentIndex" value="${0}"/>
          <c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="status">
          
            <%-- <c:forEach items="${shippingGroup.commerceItemRelationships}"
                      var="ciRelationship" varStatus="ciIndex"> --%>
    
    	<c:if test="${ciRelationship.commerceItem.commerceItemClassType != 'donationCommerceItem'}">
  
          
            <c:if test="${(shippingGroup.shippingGroupClassType == 'hardgoodShippingGroup') and (ciRelationship.commerceItem.commerceItemClassType ne 'giftWrapCommerceItem')}">
              <c:set var="hardgoodShippingGroup" value="${shippingGroup}"/>
            
              <fieldset>
                <legend>
                  <fmt:message key="shipping.chooseMethod.shippingGroupNumber">
                    <fmt:param value="${shipmentIndex + 1}"/>
                  </fmt:message>
                </legend>
								<dsp:include
									src="/panels/order/shipping/includes/shippingMethodPicker.jsp"
									otherContext="${CSRConfigurator.truContextRoot}">
									<dsp:param name="shippingGroupIndex"
										value="${status.index}" />
									<dsp:param name="order" value="${order}" />
									<dsp:param name="shippingGroup" value="${hardgoodShippingGroup}" />
									<dsp:param name="HGSGCount" value="${hardgoodShippingGroupCount}"/>
								</dsp:include>
								<dsp:include src="/panels/order/shipping/includes/shippingGroupMethodView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dsp:param name="shippingGroupIndex" value="${status.index}"/>
                  <dsp:param name="order" value="${order}"/>
                  <dsp:param name="shippingGroup" value="${hardgoodShippingGroup}"/>
                </dsp:include>
                <dsp:include src="/panels/order/shipping/includes/itemTableForMultiBPP.jsp" otherContext="${CSRConfigurator.truContextRoot}"> 
                  <dsp:param name="shippingGroupIndex" value="${shipmentIndex}"/>
                  <dsp:param name="shippingGroupId" value="${shippingGroup.id}"/>
                  <dsp:param name="catalogRefID" value="${ciRelationship.commerceItem.catalogRefId}"/>
                </dsp:include>
              </fieldset>
              <c:set var="shipmentIndex" value="${shipmentIndex + 1}"/>
            </c:if>
            </c:if>
          <%-- </c:forEach> --%>
			</c:forEach>		

				</c:when>
        <c:otherwise>
          <%-- No shipping groups --%>
        </c:otherwise>
      </c:choose>

      <dsp:include src="/panels/order/shipping/includes/promotionsListing.jsp" otherContext="${CSRConfigurator.contextRoot}">
        <dsp:param name="order" value="${order}"/>
      </dsp:include>
    <div class="atg_commerce_csr_shippingFooter">
       <a class="atg_commerce_csr_return" href="#" onclick="atgNavigate({
               panelStack : 'cmcShippingAddressPS',
               queryParams : { 'init': 'true' }});
               return false;">
       <fmt:message key='common.returnToShippingAddress' /> </a>
      <div class="atg_commerce_csr_tableControls">
        <input type="button" name="shippingMethodButton" class="atg_commerce_csr_activeButton"
               value="<fmt:message key="shipping.continueToBilling"/>"
        onclick="validateAndSubmit();return false;"/>
      </div>
    </div>

   </dsp:form>
  </div>


  <script type="text/javascript">
    atg.progress.update("cmcShippingMethodPS");

    dojo.addOnLoad(function() {
      var theForm = dojo.byId("csrSelectShippingMethods");
      if ((theForm != null) && (theForm.shippingMethodButton != null)) {
        theForm.shippingMethodButton.focus();
      }
    });
    function assignAction(defaultValue){
    	var hiddenShipInput = document.getElementById('hiddenShipInput');
    	var GWChecked=defaultValue;
    	hiddenShipInput.value = GWChecked;
    }
    function validateAndSubmit(){
    	if($("#pageName").length)
		{
			var pageName = $("#pageName").val();
			
			if(pageName == 'pickInStore')
				{
					if($(".pickupError").length)
						{
							$(".pickupError").remove();
						}
					$(".primary-pickup-person").find("input[type='text']").removeClass("error");
					var error = false;
					var emailError = false;
					$(".pickupPersonDetails").each(function(){
					var alternatePickup = $(this).find("#alternateInfoCheckBean").val();
					$(this).find(".primary-pickup-person").find("input[type='text']").filter(function(){
						if($(this).hasClass("error"))
							{
								$(this).removeClass("error");
							}
						if($.trim($(this).val()) == '')
							{
								error = true;
								$(this).addClass("error");
							}
						if($(this).hasClass("email"))
							{
								var email = $(this).val();
								if(!isValidEmail(email))
									{
										emailError = true;
										$(this).addClass("error");
									}
							}
					});
					if(alternatePickup == 'true')
					{
						$(this).find(".alternate-pickup-person-container").find("input[type='text']").filter(function(){
							if($(this).hasClass("error"))
								{
									$(this).removeClass("error");
								}
    						if($.trim($(this).val()) == '')
    							{
    								error = true;
    								$(this).addClass("error");
    							}
    						if($(this).hasClass("alternateemail"))
							{
								var email = $(this).val();
								if(!isValidEmail(email))
									{
										emailError = true;
										$(this).addClass("error");
									}
							}
    					});			
						}
					});
					if(error)
						{
							$(".error").closest("div").parent('div').parent("div").prepend("<span style='color:red' class='pickupError'>Please enter required fields</span>");
							return false;
						}
					else if(emailError)
						{
							$(".error").closest(".primary-pickup-person").parent("div").parent('div').prepend("<span style='color:red' class='pickupError'>Please enter valid email</span>");
							return false;
						}
					else{
						var form = document.getElementById("instorePickupForm1");
			        	var differred = atgSubmitAction({
			                form : form,
			     			  panel : ['cmcShippingMethodP'],
			     			  panelStack : 'cmcShippingMethodPS'
			     			  });
			        	differred.addCallback(function(results) { 
						assignAction('default');
						atg.commerce.csr.order.shipping.applySelectShippingMethods();
			        	});
					}
				}
			else{
				
				assignAction('default');
				atg.commerce.csr.order.shipping.applySelectShippingMethods();
	        	
			}
		} 
    	else{
    		assignAction('default');
    		atg.commerce.csr.order.shipping.applySelectShippingMethods();
    	}
    }
    function applyGiftMessage(shippingGroupId){
    				var message = $("#"+shippingGroupId+"_giftWrapMessage").val();
		       		var form=document.getElementById('applyGiftMessage');
		       		var shipGroupId = $("#"+shippingGroupId+"_giftShipGroupId").val();
		       		var giftReceipt = $("#"+shippingGroupId+"-gifting-items").find('input[name="giftReceipt"]').val();
		       		var giftOptionsInfoCount =  $('input[name="giftOptionsInfoCount"]').val();
		       		//alert(shipGroupId+" "+giftReceipt+" "+giftOptionsInfoCount);
		       		form.giftMessage.value=message;
		       		form.shippingGroupId.value=shipGroupId;
		       		form.giftReceipt.value=giftReceipt;
		       		form.giftOptionsInfoCount.value=giftOptionsInfoCount;
		       		if($("#pageName").length){
			        	var pageName = $("#pageName").val();
			        	if(pageName == 'pickInStore'){
			        		var differred =atgSubmitAction({
				       	        form : document.getElementById('instorePickupForm1'),
				       				  panel : ['cmcShippingMethodP'],
				       				  panelStack : 'cmcShippingMethodPS'
				       			 });
				       		differred.addCallback(function(results) {
				       			atgSubmitAction({
				                form : form,
				     			  panel : ['cmcShippingMethodP'],
				     			  panelStack : 'cmcShippingMethodPS'
				     			  });
				       		});
			        	}
			        	else{
			        		atgSubmitAction({
				       	        form : document.getElementById('applyGiftMessage'),
				       				  panel : ['cmcShippingMethodP'],
				       				  panelStack : 'cmcShippingMethodPS'
				       			 });
			        	}
		        	}
		       		else{
		       			atgSubmitAction({
			       	        form : document.getElementById('applyGiftMessage'),
			       				  panel : ['cmcShippingMethodP'],
			       				  panelStack : 'cmcShippingMethodPS'
			       			 });
		       		}
    }
    function isPhoneNumberFormat(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(charCode == 45)return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	}
    function handleRadioButtonClick(obj,shipGroupId, commerceItemRelationshipId, selectedId, commerceItemId, 
    		giftWrapProductId, giftWrapSkuId, defaultWrapProductId, defaultWrapSkuId, giftWrapSelected){
    	var prevSelectedSkuId = $(obj).parents(".gift-wrap-container-wrapper").find('.wrapSkuId.prevSelected').data('id') || '';
    	$("#shipGroupId").val(shipGroupId);
    	$("#commerceItemRelationshipId").val(commerceItemRelationshipId);
    	$("#selectedId").val(selectedId);
    	$("#commerceItemId").val(commerceItemId);
    	$("#giftWrapProductId").val(giftWrapProductId);
    	$("#giftWrapSkuId").val(giftWrapSkuId);
    	$("#previousGiftWrapSkuId").val(prevSelectedSkuId);
    	$("#giftWrapSelected").val(giftWrapSelected);
    	
    	if($("#pageName").length){
        	var pageName = $("#pageName").val();
        	if(pageName == 'pickInStore'){
        		var differred =atgSubmitAction({
        	      	  form : document.getElementById('applyGiftWrap')
            		  
            	 });
	       		differred.addCallback(function(results) {
	       			atgSubmitAction({
	                form : document.getElementById("instorePickupForm1"),
	     			  panel : ['cmcShippingMethodP'],
	     			  panelStack : 'cmcShippingMethodPS'
	     			  });
	       		});
        	}
        	else{
        		atgSubmitAction({
        	      	  form : document.getElementById('applyGiftWrap'),
        	    		  panel : ['cmcShippingMethodP'],
        	    		  panelStack : 'cmcShippingMethodPS'
        	    	 });
        	}
    	}
   		else{
   			atgSubmitAction({
   	      	  form : document.getElementById('applyGiftWrap'),
   	    		  panel : ['cmcShippingMethodP'],
   	    		  panelStack : 'cmcShippingMethodPS'
   	    	 });
   		}
    }
    /* $(document).ready(function(){debugger;	
    	var shippingGroupId,relationshipId,uniqueId,wrapCommerceItemId,shipMethodCode,shippingPrice,regionCode
 		$(".shipping-method-checkbox").each(function(){
 			if($(this).is(":checked"))
 				{
 					shippingGroupId = $(this).parent().find('#shippingGroupId').val();
 					relationshipId = $(this).parent().find('#relationshipId').val();
 					uniqueId = $(this).parent().find('#uniqueId').val();
 					wrapCommerceItemId = $(this).parent().find('#wrapCommerceItemId').val();
 					shipMethodCode = $(this).parent().find('#shipMethodCode').val();
 					shippingPrice = $(this).parent().find('#shippingPrice').val();
 					regionCode = $(this).parent().find('#regionCode').val();
 					updateShippingMethod($(this),shippingGroupId,relationshipId,uniqueId,wrapCommerceItemId,shipMethodCode,shippingPrice,regionCode);
 				}
 		})
 	}) */
  </script>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/shippingMethod.jsp#1 $$Change: 875535 $--%>
