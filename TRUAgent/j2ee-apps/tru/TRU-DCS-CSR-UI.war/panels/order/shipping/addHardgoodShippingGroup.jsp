<%--
This page provides the option to add hard good shipping group.
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/addHardgoodShippingGroup.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>

<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/CreateHardgoodShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean var="addHardgoodShippingGroup"
                  bean="/atg/commerce/custsvc/ui/fragments/order/AddHardgoodShippingGroup"/>
  <dsp:importbean var="hardgoodShippingGroupConfig"
                  bean="/atg/commerce/custsvc/ui/HardgoodShippingGroupConfiguration"/>                  
  <dsp:importbean var="addressForm" bean="/atg/svc/agent/ui/fragments/AddressForm"/>
  <dsp:importbean var="agentUIConfig" bean="/atg/svc/agent/ui/AgentUIConfiguration"/>
  
   <dsp:importbean  bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>
  

  <c:set var="formId" value="csrAddShippingAddress"/>
  <svc-ui:frameworkUrl var="successURL" panelStacks="cmcShippingAddressPS,globalPanels"/>
  <svc-ui:frameworkUrl var="errorURL" panelStacks="cmcShippingAddressPS"/>

  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">

<c:set var="pageName" value="addAddress"/>
  
  <%-- 	sss<dsp:valueof bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler.address"/> --%>
  
  
    <dsp:form id="${formId}" formid="${formId}">
    
    
    
    <%--  <dsp:input type="hidden" priority="-10" value=""
                 bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroup"/>

      <dsp:input type="hidden" value="${errorURL }" name="errorURL"
                 bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroupErrorURL"/>

      <dsp:input type="hidden" value="${successURL }" name="successURL"
                 bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroupSuccessURL"/> --%>
                 
    
           
                      <dsp:input type="hidden" priority="-10" value=""
                 bean="ShippingGroupFormHandler.addShippingAddress"/>

      <dsp:input type="hidden" value="${errorURL}" name="errorURL"
                 bean="ShippingGroupFormHandler.commonErrorURL"/>

      <dsp:input type="hidden" value="${successURL }" name="successURL"
                 bean="ShippingGroupFormHandler.commonSuccessURL"/>      

	 <dsp:input type="hidden" bean="ShippingGroupFormHandler.address.lastName" name="lastNameSG" id="lastNameSG" />  
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.newShipToAddressName" name="newShipToAddressNameSG" id="newShipToAddressNameSG" /> 
		
		 
		<dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToAddressName" name="shipToAddressNameSG" id="shipToAddressNameSG" /> 
		
		
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.firstName" name="firstNameSG" id="firstNameSG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.middleName" name="middleNameSG" id="middleNameSG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.country" name="countrySG" id="countrySG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address1" name="address1SG" id="address1SG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.address2" name="address2SG" id="address2SG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.city" name="citySG" id="citySG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.state" name="stateSG" id="stateSG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.postalCode" name="postalCodeSG" id="postalCodeSG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.address.phoneNumber" name="phoneNumberSG" id="phoneNumberSG" /> 
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.saveShippingAddress" name="saveShippingAddress" id="saveShippingAddress" /> 
      <%-- If the tab container is used by the shipping address page, then this snippet below selects the right
      tab on error. --%>
      <dsp:droplet name="Switch">
        <dsp:param bean="CreateHardgoodShippingGroupFormHandler.formError"
                   name="value"/>
        <dsp:oparam name="true">
          <script type="text/javascript">
            _container_.onLoadDeferred.addCallback(function () {
              var sgAddTabContainer = dijit.byId("shippingAddressAddContainer");
              if (sgAddTabContainer) sgAddTabContainer.selectChild("${hardgoodShippingGroupConfig.type}");
            });
          </script>
        </dsp:oparam>
      </dsp:droplet>

      <div class="atg-csc-base-table atg-base-table-customer-address-add-form"
          id="atg_commerce_csr_neworder_newShippingAddress">
        <dsp:include src="/include/addresses/addressFormSP.jsp" otherContext="${agentUIConfig.truContextRoot}">
          <dsp:param name="formId" value="${formId}"/>
          <dsp:param name="addressBean"
                     value="/atg/commerce/custsvc/order/CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress"/>
          <dsp:param name="submitButtonId" value="addAddressButton"/>
        </dsp:include>
        
      </div>

      <div class="atg_svc_saveProfile">
        <dsp:droplet name="/atg/dynamo/droplet/Switch">
          <dsp:param
              bean="/atg/userprofiling/ActiveCustomerProfile.transient"
              name="value"/>
          <dsp:oparam name="false">
            <div>
              <dsp:input type="checkbox" checked="true" iclass="default_save_Shipping" 
                        bean="ShippingGroupFormHandler.saveShippingAddress"  value="true"/>
              <fmt:message key="newOrderSingleShipping.addShippingAddress.field.saveToProfile"/>
            </div>
          </dsp:oparam>
          <dsp:oparam name="true">
            <dsp:input type="hidden" bean="ShippingGroupFormHandler.saveShippingAddress" value="false"/>
          </dsp:oparam>
        </dsp:droplet>
      </div>

      <div class="atg_svc_formActions">
        <div>
<%--           <input type="button" name="addAddressButton"
                    id="addAddressButton"
                    class="atg_commerce_csr_activeButton"
                    onclick="atg.commerce.csr.order.shipping.addShippingAddress();return false;"
                    value="<fmt:message key="newOrderSingleShipping.addShippingAddress.button.addAddress"/>"
                form="${formId}"
                dojoType="atg.widget.validation.SubmitButton"/> --%>
                
                 <input type="button" name="addAddressButton"
                    id="addAddressButton"
                    class="atg_commerce_csr_activeButton"
                    onclick="verifyAddress();"
                    value="<fmt:message key="newOrderSingleShipping.addShippingAddress.button.addAddress"/>"
                form="${formId}"
                dojoType="atg.widget.validation.SubmitButton"/> 
                
           
        </div>
      </div>
     <div id="hiddenFirstSuggestedAddress" style="display:none"></div>
    </dsp:form>
    
      <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
       
        </c:url> 
    
      <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/addressDoctor.jsp">
        </c:url>
    
     <script type="text/javascript">
  
        
        atg.commerce.csr.order.shipping.addShippingAddress = function (){
        	
        	
        	if($('#atg_commerce_csr_addNewShippingAddress').find('#error').length)
        		{
        			$('#error').remove();
        		}
        	//var selctedAddressRadio = $('input[name=valid-address]:checked');
        	var selctedAddressRadio = $('#csrAddressDoctorFloatingPane input[name=valid-address]:checked');
			var count = selctedAddressRadio.val();
			if(count > 0){
			var selectParent = $(selctedAddressRadio).parent();
			var address1 = selectParent.find('#suggestedAddress1-'+count).val();
			var address2 = selectParent.find('#suggestedAddress2-'+count).val();
			var city = selectParent.find('#suggestedCity-'+count).val();
			var state = selectParent.find('#suggestedState-'+count).val();
			var postalCode = selectParent.find('#suggestedPostalCode-'+count).val();
			var country = selectParent.find('#suggestedCountry-'+count).val();
			
			}
			else{
	          	var city=document.getElementById('city').value; 
	          	var state1=document.getElementById('state').value; 
	          	var country="US";
	          	var postalCode=document.getElementById('postalCode').value; 
	          	var address1=document.getElementById('address1').value;
	          	var address2=document.getElementById('address2').value;
	          	var state = $.trim(state1.split('-')[0]);
			}
			//var nickName=document.getElementById('nickName').value;
			var lastName=document.getElementById('lastName').value; 
	        var firstName=document.getElementById('firstName').value; 
	        var saveShipping = $(".default_save_Shipping").val();
	        
	       // alert(saveShipping);
	       // var middleName=document.getElementById('middleName').value; 
	        var phoneNumber=document.getElementById('phoneNumber').value; 
        	var form =  document.getElementById('csrAddShippingAddress'); 
        	form.lastNameSG.value=lastName;
        	form.firstNameSG.value=firstName;
        	form.saveShippingAddress.value=saveShipping;
        	//alert(saveShipping);
        	//form.middleNameSG.value=middleName;
        	form.citySG.value=city;
        	form.stateSG.value=state;
        	form.countrySG.value=country;
        	form.postalCodeSG.value=postalCode;
        	form.phoneNumberSG.value=phoneNumber;
        	form.address1SG.value=address1;
        	form.address2SG.value=address2; 
        	//form.newShipToAddressNameSG.value=nickName;  
        	//form.shipToAddressNameSG.value=nickName;
            atgSubmitAction({form:dojo.byId("csrAddShippingAddress"), sync: true});
            atgNavigate({panelStack : 'cmcShippingAddressPS', queryParams: { init : 'true' }});
            hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
          };
          
     
      
              function verifyAddress(){
            	  if($('#error').length)
            		{
            			$('#error').remove();
            		}
              	var city=document.getElementById('city').value; 
              	var state=document.getElementById('state').value; 
              	var country=document.getElementById('csrAddShippingAddress_country');
              	var postalCode=document.getElementById('postalCode').value; 
              	var state1 = $.trim(state.split('-')[0]);
              	var address1=document.getElementById('address1').value;
              	var address2=document.getElementById('address2').value;
    			//var nickName=document.getElementById('nickName').value;
    			var lastName=document.getElementById('lastName').value; 
                var firstName=document.getElementById('firstName').value; 
                //var middleName=document.getElementById('middleName').value; 
                var phoneNumber=document.getElementById('phoneNumber').value;
                var emptyInput = false;
                $("#atg_commerce_csr_neworder_newShippingAddress").find("input[type='text']").not("#middleName,#address2,#nickName").filter(function(){
                	var inputText = $.trim($(this).val());
                	if(inputText == '')
                	{
                		emptyInput = true;
                	}
                });
                if(emptyInput == true) return false;
          		if(postalCode.length < 5)
				{
		     		
					$('#postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
					return false;
				}
				else if(phoneNumber.length < 10)
				{
					$('#phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
					return false;
				}    		
//alert(resultsUrl);
			dojo.xhrPost({
						url : "/TRU-DCS-CSR/panels/order/shipping/includes/results.jsp?pageName=${pageName}&address1="+address1+"&address2="+address2+"&city="+city+"&state="+state1+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&windowid="+window.windowId,
						encoding : "utf-8",
						preventCache : true,
						handle : function(_362, _363) {
							var response = dojo.trim(_362);
							dojo.query("#hiddenFirstSuggestedAddress")[0].innerHTML = response;
							var derivedStatus = dojo.query("#hiddenFirstSuggestedAddress #derivedStatus")[0].innerHTML;
							var addressRecognized = dojo.query("#hiddenFirstSuggestedAddress #addressDoctorVO1")[0].innerHTML;
							if(derivedStatus == "NO" && addressRecognized == "true"){
								var data =  dojo.query("#hiddenFirstSuggestedAddress #firstSuggestedAddress")[0].innerHTML
								var form =  document.getElementById('csrAddShippingAddress'); 
					        	form.lastNameSG.value=lastName;
					        	form.firstNameSG.value=firstName;
					        	form.saveShippingAddress.value=$(".default_save_Shipping").val();
					        	//alert(saveShipping);
					        	//form.middleNameSG.value=middleName;
					        	form.citySG.value = dojo.query("#hiddenFirstSuggestedAddress #suggestedCity-1")[0].value;
					        	form.stateSG.value = dojo.query("#hiddenFirstSuggestedAddress #suggestedState-1")[0].value;
					        	form.countrySG.value = 'US';
					        	form.postalCodeSG.value = dojo.query("#hiddenFirstSuggestedAddress #suggestedPostalCode-1")[0].value;
					        	form.phoneNumberSG.value = phoneNumber;
					        	form.address1SG.value = dojo.query("#hiddenFirstSuggestedAddress #suggestedAddress1-1")[0].value;
					        	form.address2SG.value = dojo.query("#hiddenFirstSuggestedAddress #suggestedAddress2-1")[0].value; 
								//var suggestedAddress1 = form.suggestedAddress1-0; */
						/* 	atg.commerce.csr.common
									.showPopupWithReturn({
										popupPaneId : 'csrAddressDoctorFloatingPane',
										url : "${addressDoctorURL}?pageName=${pageName}&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state1+"&postalCode="+postalCode+"&windowId="+window.windowId,
										onClose : function(args) {
											if (args.result == 'ok') {
												atgSubmitAction({
													panelStack : [
															'cmcShippingAddressPS',
															'globalPanels' ],
													form : document
															.getElementById('transformForm')
												});
											}
										}
									}); */
									atgSubmitAction({form:dojo.byId("csrAddShippingAddress"), sync: true});
						            atgNavigate({panelStack : 'cmcShippingAddressPS', queryParams: { init : 'true' }});
									/* atgSubmitAction({
										panelStack : [
												'cmcShippingAddressPS',
												'globalPanels' ],
										form : document.getElementById('csrAddShippingAddress')
									}); */
							return false;
							}
							else if(derivedStatus == "YES"){
								atg.commerce.csr.common
								.showPopupWithReturn({
									popupPaneId : 'csrAddressDoctorFloatingPane',
									url : "${addressDoctorURL}?pageName=${pageName}&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state1+"&postalCode="+postalCode+"&windowId="+window.windowId,
									onClose : function(args) {
										if (args.result == 'ok') {
											atgSubmitAction({
												panelStack : [
														'cmcShippingAddressPS',
														'globalPanels' ],
												form : document
														.getElementById('transformForm')
											});
										}
									}
								});
							}
							else{
								atg.commerce.csr.order.shipping.addShippingAddress();
								return false;
							}
							if (document
									.getElementById("inStorePickupResultsss")) {
								document
										.getElementById("inStorePickupResultsss").innerHTML = _362;
							}

						},
						mimetype : "text/html"
					}); 
        	 }
        	
              $(document).on("click",".default_save_Shipping",function(){
              	if($(this).is(":checked"))
              		{
              			$(this).val("true");
              		}
              	else
              		{
              			$(this).val("false");
              		}
              })    
        </script>   
      
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/addHardgoodShippingGroup.jsp#1 $$Change: 875535 $--%>

