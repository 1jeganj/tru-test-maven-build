<%-- This page is used to edit hard good shipping group.
param - nickname
This parameter is used to initialize shipping group from the ShippingGroupMapContainer.

param - success
This parameter is used to close the popup panel and refresh the parent page. This parameter is
added to the request on edit form submission.

--%>
<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">

  <dsp:importbean var="updateShippingGroupFormHandler"
                  bean="/atg/commerce/custsvc/order/UpdateHardgoodShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
  <dsp:importbean var="sgConfig"
                  bean="/atg/commerce/custsvc/ui/HardgoodShippingGroupConfiguration"/>
  <dsp:importbean var="addressForm" bean="/atg/svc/agent/ui/fragments/AddressForm"/>

  <dsp:getvalueof var="nickname" param="nickname"/>
  <dsp:getvalueof var="success" param="success"/>
    <dsp:getvalueof var="shippingGoupp" param="shippingGoupp"/>

<c:set var="pageName" value="edit"/>
<c:set var="closeId" value="csrEditAddressFloatingPane"/>
 <dsp:getvalueof var="addrKey" param="addrKey"/>
       <dsp:getvalueof var="adre1" param="adre1"/>
        <dsp:getvalueof var="adre2" param="adre2"/>
         <dsp:getvalueof var="city" param="city"/>
          <dsp:getvalueof var="country" param="country"/>
         <dsp:getvalueof var="state" param="state"/>
          <dsp:getvalueof var="postalCode" param="postalCode"/>
           <dsp:getvalueof var="shipId" param="id"/>
            <dsp:getvalueof var="sgType" param="sgTypeValue"/>
             <dsp:getvalueof var="shipAddress" param="/atg/commerce/custsvc/order/UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress"/>

  <c:url var="successErrorURL" context="${CSRConfigurator.truContextRoot}"
         value="${sgConfig.editPageFragment.URL}">
    <c:param name="nickname" value="${nickname}"/>
    <c:param name="${stateHolder.windowIdParameterName}"
             value="${windowId}"/>
    <c:param name="success" value="true"/>
  </c:url>

  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
	<style>
	.hide{
		display:none;
	}
	</style>
    <div id="atg_commerce_csr_editShippingAddress"
         class="atg_commerce_csr_popupPanel atg_commerce_csr_addressFormPopup atg_svc_popupPanel">
      <dsp:layeredBundle basename="${sgConfig.resourceBundle}">
       <fmt:message var="editPageFragmentTitle" key="${sgConfig.editPageFragmentTitleKey}"/>
      </dsp:layeredBundle>
    <%--   <h2>
        <c:out value="${editPageFragmentTitle}"/>
      </h2> --%>
      <div class="atg_commerce_csr_popupPanelCloseButton"></div>
      <div>
        <%--When there is an error, display the error on the page. --%>
        <dsp:droplet name="Switch">
          <dsp:param bean="UpdateHardgoodShippingGroupFormHandler.formError"
                     name="value"/>
          <dsp:oparam name="true">
            &nbsp;<br/><br/>
            <span class="atg_commerce_csr_common_content_alert"><fmt:message key="common.error.header"/></span>
            <br>
          <span class="atg_commerce_csr_common_content_alert">
          <UL>
            <dsp:droplet name="ErrorMessageForEach">
              <dsp:param bean="UpdateHardgoodShippingGroupFormHandler.formExceptions"
                         name="exceptions"/>
              <dsp:oparam name="output">
                <LI>
                  <dsp:valueof param="message"/>
              </dsp:oparam>
            </dsp:droplet>
          </UL>
          </span>
          </dsp:oparam>
          <dsp:oparam name="false">
            <c:if test="${success}">
              <%--When there is no error on the page submission, close the popup page and refresh the parent page.
              the parent page only will refresh if the result parameter value is ok. --%>
              <script type="text/javascript">
                hidePopupWithResults('atg_commerce_csr_editShippingAddress', {result : 'ok'});
              </script>
            </c:if>
          </dsp:oparam>
        </dsp:droplet>
      </div>
      <c:set var="formId" value="csrEditShippingAddressForm"/>
      <dsp:form id="${formId}"
                formid="${formId}" iclass="abcd">

        <dsp:input type="hidden" priority="-10" value=""
                   bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroup"/>
                   
         <dsp:input type="hidden" 
                   bean="UpdateHardgoodShippingGroupFormHandler.updateProfile" id="updateProfile" name="updateProfile"/>

        <dsp:input type="hidden" value="${successErrorURL }"
                   bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroupErrorURL"/>

        <dsp:input type="hidden" value="${successErrorURL }"
                   bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroupSuccessURL"/>

        <dsp:input type="hidden" bean="UpdateHardgoodShippingGroupFormHandler.shippingGroupByNickname"
                   value="${fn:escapeXml(nickname) }" priority="5" id="csrEditShippingAddressForm_nickName"/>

        <c:if test="${empty updateShippingGroupFormHandler.hardgoodShippingGroup }">
          <dsp:setvalue bean="UpdateHardgoodShippingGroupFormHandler.shippingGroupByNickname"
                        value="${fn:escapeXml(nickname) }" />
        </c:if>

        <div class="atg_dataForm atg_commerce_csr_addressForm atg-csc-base-table atg-base-table-customer-address-add-form">
          <dsp:include src="/include/addresses/addressFormEdit.jsp" otherContext="/truagent">
            <dsp:param name="formId" value="${formId}"/>
            <dsp:param name="addressBean"
                       value="/atg/commerce/custsvc/order/UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress"/>
            <dsp:param name="submitButtonId" value="${formId}SaveButton"/>
             <dsp:param name="page" value="shippingEdit"/>
          </dsp:include>

          <div class="atg_svc_saveProfile atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <!--
   <span class="atg_commerce_csr_fieldTitle">
              &nbsp;
            </span>-->

            <dsp:droplet name="/atg/dynamo/droplet/Switch">
              <dsp:param bean="UpdateHardgoodShippingGroupFormHandler.shippingAddressExistsInProfile"
                         name="value"/>
              <dsp:oparam name="true">
                <dsp:droplet name="/atg/dynamo/droplet/Switch">
                  <dsp:param
                    bean="/atg/userprofiling/ActiveCustomerProfile.transient"
                    name="value"/>
                  <dsp:oparam name="false">
                    <dsp:input type="checkbox" checked="${true}" bean="UpdateHardgoodShippingGroupFormHandler.updateProfile" value="true" iclass="edit_default_save_Shipping"/>
                    <fmt:message key="newOrderSingleShipping.editShippingAddress.field.saveToProfile"/>
                  </dsp:oparam>
                  <dsp:oparam name="true">
                    <dsp:input type="hidden" bean="UpdateHardgoodShippingGroupFormHandler.updateProfile" value="false" iclass="edit_default_save_Shipping"/>
                  </dsp:oparam>
                </dsp:droplet>
              </dsp:oparam>
              <dsp:oparam name="false">
                <dsp:input type="hidden" bean="UpdateHardgoodShippingGroupFormHandler.updateProfile" value="false" iclass="edit_default_save_Shipping"/>
              </dsp:oparam>
            </dsp:droplet>
          </div>
          <div class="atg_svc_formActions atg-csc-base-table-cell">
            <div class="atg_commerce_csr_panelFooter">
         <input type="button"
                     name="${formId}SaveButton"
                     value="<fmt:message key='common.save' />"
                     onclick= "addressDoctorSelectBtnEdit('${nickname}');checkRadioBtnChecked();"
                     dojoType="atg.widget.validation.SubmitButton"/> 
                     
                     <!-- <input type="button"
                     name="${formId}SaveButton"
                     value="<fmt:message key='common.save' />"
                     onclick="atg.commerce.csr.order.shipping.editShippingAddress('${successErrorURL}');return false;"
                     dojoType="atg.widget.validation.SubmitButton"/> -->
                      
                     
                     
                     
                     
                     
              <%-- When the user clicks on the cancel button, just hide the popup panel. --%>
              <input type="button"
                     value="<fmt:message key='common.cancel'/>"
                     onclick="hidePopupWithResults( 'atg_commerce_csr_editShippingAddress', {result : 'cancel'});return false;"/>
            </div>
          </div>
        </div>
<div id="hiddenEditShippingSuggestedAddress" style="display:none"></div>
      </dsp:form>
      <%-- end of editShippingAddressForm --%>
    </div>
  </dsp:layeredBundle>
	<div class="hide" id="editAddressDoctor"> 
		<dsp:include src="/panels/order/shipping/includes/addressDoctor.jsp"
			 otherContext="${CSRConfigurator.truContextRoot}">
		</dsp:include>
	</div>


  <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
         <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
           <c:param name="addressBean"  value="${shipAddress}"/>
        </c:url> 

      <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
              value="/panels/order/shipping/includes/addressDoctor.jsp">
         <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
           <c:param name="addressBean"  value="${shipAddress}"/>
       </c:url>
  	<script type="text/javascript">
		function addressDoctorSelectBtnEdit(nickName) {
			
			if($('#atg_commerce_csr_editShippingAddress').find('#error').length)
    		{
    			$('#error').remove();
    		}
			
			var lastName=document.getElementById('csrEditShippingAddressForm_lastName').value; 
        	 var firstName=document.getElementById('csrEditShippingAddressForm_firstName').value; 
        	 //var middleName=document.getElementById('csrEditShippingAddressForm_middleName').value; 
        	var city=document.getElementById('csrEditShippingAddressForm_city').value; 
        	var state=document.getElementById('csrEditShippingAddressForm_state').value; 
        	var country='US'; 
        	var postalCode=document.getElementById('csrEditShippingAddressForm_postalCode').value; 
        	var phoneNumber=document.getElementById('csrEditShippingAddressForm_phoneNumber').value; 
        	var address1=document.getElementById('csrEditShippingAddressForm_address1').value;
        	//var address2=document.getElementById('csrEditShippingAddressForm_address2').value;
        	var nickName=document.getElementById('csrEditShippingAddressForm_nickName').value;
        	var updateToProfile = $('.edit_default_save_Shipping').val();
        	
         	if(postalCode.length < 5)
    		{
         		
    			$('#csrEditShippingAddressForm_postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
    			return false;
    		}
    	else if(phoneNumber.length < 10)
    		{
    			$('#csrEditShippingAddressForm_phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
    			return false;
    		}
        	
        	
        	state = state.split("-")[0]; 
        	
        	
			var form = dojo.byId("csrEditShippingAddressForm");
			form.updateProfile.value=updateToProfile;
			dojo.xhrPost({
						
						//atg.commerce.csr.common.submitPopup('${successErrorURL}', dojo.byId("csrEditShippingAddressForm"), dijit.byId("csrEditAddressFloatingPane"));
						url : '${resultsUrl}&phoneNo='+phoneNumber+'&nickName='+nickName+'&phoneNumber='+phoneNumber+'&firstName='+firstName+'&lastName='+lastName+'&adre1='+address1+'&city='+city+'&country='+country+'&state='+state+'&postalCode='+postalCode+'&updateToProfile='+updateToProfile+'&pageName=${pageName}&closeId=${closeId}',
						encoding : "utf-8",
						preventCache : true,
						
						
						handle : function(_362, _363) {
							
							var response = dojo.trim(_362);
							dojo.query("#hiddenEditShippingSuggestedAddress")[0].innerHTML = response;
							var derivedStatus = dojo.query("#hiddenEditShippingSuggestedAddress #shipEdit")[0].innerHTML;
							var addressRecognized = dojo.query("#hiddenEditShippingSuggestedAddress #shipEditVo")[0].innerHTML;
							
							
							if(derivedStatus == "NO" && addressRecognized == "true"){
								var data =  dojo.query("#hiddenEditShippingSuggestedAddress #firstSuggestedAddressEdit")[0].innerHTML
								var form =  document.getElementById('shipToAddressFromSuggested'); 
					        	form.lastNameAO.value=lastName;
					        	form.firstNameAO.value=firstName;
					        	form.phoneNumberAO.value = phoneNumber;
					        	var oo =dojo.query("#hiddenEditShippingSuggestedAddress #suggestedCity-1")[0].value;;
					        	//form.saveShippingAddress.value=$(".default_save_Shipping").val();
					        	form.cityAO.value = dojo.query("#hiddenEditShippingSuggestedAddress #suggestedCity-1")[0].value;
					        	form.stateAO.value = dojo.query("#hiddenEditShippingSuggestedAddress #suggestedState-1")[0].value;
					        	form.countryAO.value = 'US';
					        	form.postalCodeAO.value = dojo.query("#hiddenEditShippingSuggestedAddress #suggestedPostalCode-1")[0].value;
					        	form.address1AO.value = dojo.query("#hiddenEditShippingSuggestedAddress #suggestedAddress1-1")[0].value;
					        	form.shipToNewAddressAO.value=nickName;
					        	
					        	atgSubmitAction({form:dojo.byId(form)});
					        	
					        	//atgNavigate({panelStack : 'cmcShippingAddressPS'});
					        	hidePopupWithResults('csrEditAddressFloatingPane', {result : 'cancel'});
					            return false;
								
							}
							
							else if(derivedStatus == "YES"){
							atg.commerce.csr.common
									.showPopupWithReturn({
										popupPaneId : 'csrAddressDoctorFloatingPane',
										title : 'Edit',
										url : '${addressDoctorURL}&phoneNo='+phoneNumber+'&nickName='+nickName+'&phoneNumber='+phoneNumber+'&firstName='+firstName+'&lastName='+lastName+'&adre1='+address1+'&city='+city+'&country='+country+'&state='+state+'&postalCode='+postalCode+'&updateToProfile='+updateToProfile+'&pageName=${pageName}&closeId=${closeId}',
										onClose : function(args) {
											if (args.result == 'ok') {
												atgSubmitAction({
													panelStack : [
															'cmcShippingAddressPS',
															'globalPanels' ],
													form : document
															.getElementById('transformForm')
												});
											}
										}
									});
							hidePopupWithResults('csrEditAddressFloatingPane', {result : 'cancel'});
							return false;
							
							}
							else {
								
								atg.commerce.csr.order.shipping.editShippingAddress('/panels/order/shipping/editHardgoodShippingGroup.jsp');
								hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
								return false;
							}
							
							if (document
									.getElementById("inStorePickupResultsss")) {
								document
										.getElementById("inStorePickupResultsss").innerHTML = _362;
							}

						},
						mimetype : "text/html"
					});
				}
		
		  $(document).on("click",".edit_default_save_Shipping",function(){
            	if($(this).is(":checked"))
            		{
            			$(this).val("true");
            		}
            	else
            		{
            			$(this).val("false");
            		}
            })    
	</script> 
	
	<script type="text/javascript">
   		atg.commerce.csr.order.shipping.editShippingAddress = function(pURL){
      	atg.commerce.csr.common.submitPopup('${successErrorURL}', dojo.byId("csrEditShippingAddressForm"), dijit.byId("csrEditAddressFloatingPane"));
    };
	/* function editShippingAddress(){
		alert("yy");
		$("#editAddressDoctor").removeClass("hide");
		$("#csrEditShippingAddressForm").addClass("hide");
	}
	function closeModal(){
		hidePopupWithResults('csrEditAddressFloatingPane', {result : 'cancel'});
	}; */
 </script> 
  
  
</dsp:page>


<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/shipping/editHardgoodShippingGroup.jsp#1 $$Change: 875535 $--%>