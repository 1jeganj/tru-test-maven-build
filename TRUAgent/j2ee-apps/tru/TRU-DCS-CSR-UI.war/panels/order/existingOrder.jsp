<%--
 This page defines the existing order panel
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/existingOrder.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    
    <dsp:include src="/panels/order/finish/existingOrderView.jsp" otherContext="${CSRConfigurator.truContextRoot}">      
    </dsp:include>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/existingOrder.jsp#1 $$Change: 875535 $--%>
