<%@ include file="/include/top.jspf"%>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean bean="/com/tru/commerce/email/TRUEmailNotificationFormHandler"/>
<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:getvalueof param="notifyMeMailId" var="notifyMeMailId"></dsp:getvalueof>
<dsp:getvalueof param="itemProductId" var="itemProductId"></dsp:getvalueof>
<dsp:getvalueof param="catalogRefId" var="catalogRefId"></dsp:getvalueof>
<dsp:getvalueof param="currentSiteId" var="currentSiteId"></dsp:getvalueof>
<dsp:getvalueof param="onlinePID" var="onlinePID"></dsp:getvalueof>
<dsp:getvalueof param="productPageUrl" var="productPageUrl"></dsp:getvalueof>
<dsp:getvalueof param="sendSpecialofferToMail" var="sendSpecialofferToMail"></dsp:getvalueof>
<dsp:getvalueof param="dsiplayName" var="itemName"></dsp:getvalueof>
<dsp:getvalueof param="description" var="itemDescription"></dsp:getvalueof>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.helpURLName" paramvalue="itemURL" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.helpURLValue" paramvalue="itemURL" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.itemDescription" paramvalue="itemDescription" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.displayName" paramvalue="itemName" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.notifyMeMailId" paramvalue="notifyMeMailId" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.productId" paramvalue="itemProductId" />
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.skuId" paramvalue="catalogRefId" />
	    <dsp:setvalue bean="TRUEmailNotificationFormHandler.pdpPageURL" paramvalue="productPageUrl" /> 
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.onlinePID" paramvalue="onlinePID"/>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.sendSpecialofferToMail" paramvalue="sendSpecialofferToMail"/>
		<dsp:setvalue bean="TRUEmailNotificationFormHandler.notifyMe" value="submit" />
		
			<dsp:droplet name="Switch">
				<dsp:param bean="TRUEmailNotificationFormHandler.formError" name="value"/>
				<dsp:oparam name="true">
					 <dsp:droplet name="ErrorMessageForEach">
				        <dsp:param name="exceptions" bean="TRUEmailNotificationFormHandler.formExceptions"/>
						<dsp:oparam name="outputStart">
							Following are the form errors:
						</dsp:oparam>
						<dsp:oparam name="output">
						   <span><dsp:valueof param="message" /></span>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
				<dsp:oparam name="false">
				<div class="notify-me-title">Thank You...</div>
				<div style="font-size: 13px;">We will notify you when this item is back in stock.</div>
				</dsp:oparam>
			  </dsp:droplet>
	</dsp:layeredBundle>
</dsp:page>