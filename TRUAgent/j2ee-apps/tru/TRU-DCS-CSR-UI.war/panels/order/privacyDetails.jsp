<%@ include file="/include/top.jspf"%>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<div>
	<div class="title">Privacy Policy</div>
	<div class="tse-scroll-content tse-quickhelp-scroll-content" style="width: 100%;">
		<div class="quick-help-modal-content tse-content">
		<div class="modal-static-content">
		<ul>
			<li>Online at Toysrus.com, Babiesrus.com, Toyrusinc.com and any other websites operated by Toys"R"Us (the "Toys"R"Us Sites");</li>
			<li>When you call, e-mail or otherwise communicate with Toys"R"Us;</li>
			<li>Through or in connection with apps or software sponsored by Toys"R"Us;</li>
			<li>On portions of other websites hosted or sponsored by Toys"R"Us;</li>
			<li>When you or your child use the Tabeo tablet;</li>
			<li>In any games or promotions sponsored by Toys"R"Us; and</li>
			<li>At Toys"R"Us and Babies"R"Us retail stores in the United States (the "Retail Stores").</li>
		</ul>
		</div>
		</div>
	</div>
</div>
	</dsp:layeredBundle>
</dsp:page>