<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart"/>

<dsp:setvalue bean="GiftCardFormHandler.giftCardNumber" paramvalue="gcNum" />
<dsp:setvalue bean="GiftCardFormHandler.giftCardPin" paramvalue="gcPin" />
<dsp:setvalue bean="GiftCardFormHandler.fetchGiftCardBalance" value="submit" />

<dsp:droplet name="/atg/dynamo/droplet/Switch">
           <dsp:param bean="GiftCardFormHandler.formError" name="value" />
           <dsp:oparam name="true">
	           <dsp:droplet name="/atg/dynamo/droplet/ErrorMessageForEach">
	             <dsp:param name="exceptions" bean="GiftCardFormHandler.formExceptions" />
					<dsp:oparam name="outputStart">
	            		 Following are the form errors:
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:valueof param="message" valueishtml="true" />
					</dsp:oparam>
			</dsp:droplet>
           </dsp:oparam>
            <dsp:oparam name="false">
				<div class="gift-card-balance">&nbsp;<dsp:valueof bean="GiftCardFormHandler.giftCardBalance" converter="currency"/>&nbsp;</div>
				<div class="gift-card-balance-date">Card balance as of&nbsp;<dsp:valueof
							bean="/atg/dynamo/service/CurrentDate.timeAsDate" date="MM/dd/yyyy" /></div>
							<a class="checkAnotherCard" href="javascript:void(0);">check another card</a> 
            </dsp:oparam>
         </dsp:droplet>
</dsp:layeredBundle>
</dsp:page>