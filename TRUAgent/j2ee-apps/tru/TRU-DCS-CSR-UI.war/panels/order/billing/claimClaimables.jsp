<%-- FIXME: Add page comments

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/claimClaimables.jsp#1 $$Change: 875535 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/purchase/GiftCardFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/PaymentGroupFormHandler"/>
  <dsp:importbean var="urlDroplet" bean="/atg/svc/droplet/FrameworkUrlDroplet"/>
  <dsp:importbean bean="/atg/commerce/custsvc/promotion/CouponFormHandler"/>
  <dsp:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/purchase/BillingFormHandler" />
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart"/>
  <dsp:getvalueof var="order" param="order"/>
  <dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType"/>
  <%-- <dsp:valueof bean="ShoppingCart.current.priceInfo.total"/> --%>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <dsp:getvalueof bean="ActiveCustomerProfile.rewardNo" var="profileRewardNumber"/>
	    <c:if test="${not empty profileRewardNumber}">
		<dsp:setvalue bean="BillingFormHandler.initRewardNumberInOrder" value="submit" />
		</c:if>
    <div>
      <dsp:droplet name="FrameworkUrlDroplet">
        <dsp:param name="panelStacks" value="cmcBillingPS"/>
        <dsp:oparam name="output">
          <dsp:getvalueof var="successURL" bean="FrameworkUrlDroplet.url"/>
        </dsp:oparam>
      </dsp:droplet>
       <dsp:form method="post" id="giftCardApplicationForm" name="giftCardApplicationForm">
	    <dsp:input type="hidden" id="giftCardPin" name="giftCardPin" bean="GiftCardFormHandler.giftCardPin" />
	    <dsp:input type="hidden" id="giftCardNumber" name="giftCardNumber" bean="GiftCardFormHandler.giftCardNumber" />
	    <dsp:input type="hidden" id="giftCardEmail" name="giftCardEmail" bean="GiftCardFormHandler.giftCardEmail" />
	    <dsp:input type="hidden" id="CSCRequest" name="CSCRequest" bean="GiftCardFormHandler.CSCRequest" />
	    
	    
	    <%-- <dsp:input type="hidden" bean="GiftCardFormHandler.order" value="${order}"/> --%>  
	    <dsp:input type="hidden" value="dummy" priority="-100" bean="GiftCardFormHandler.applyGiftCard"/>
  	 </dsp:form>
  	 
  	 <dsp:form method="post" id="giftCardRemovalForm" name="giftCardRemovalForm">
	    <dsp:input type="hidden" bean="GiftCardFormHandler.cardId" id="giftCardId" />
	    <dsp:input type="hidden" id="giftCardEmail" name="giftCardEmail" bean="GiftCardFormHandler.giftCardEmail" />
	    <dsp:input type="hidden" id="CSCRequest" name="CSCRequest" bean="GiftCardFormHandler.CSCRequest" />
		<dsp:input type="hidden" bean="GiftCardFormHandler.removeGiftCard" value="submit" />
  	 </dsp:form>
	 <dsp:form method="post" id="applyCouponForm" name="applyCouponForm">
           <dsp:input type="hidden" id="couponClaimCode" name="couponClaimCode" bean="CouponFormHandler.couponClaimCode" />
           <dsp:input type="hidden" id="couponEmail" name="couponEmail" bean="CouponFormHandler.couponEmail" />
           <dsp:input type="hidden" value="dummy" priority="-100" bean="CouponFormHandler.claimCoupon"/>
        </dsp:form>
      <dsp:form method="post" id="removeCouponForm" name="removeCouponForm">
           <dsp:input type="hidden" id="removeCouponClaimCode" name="removeCouponClaimCode" bean="CouponFormHandler.couponClaimCode" />
           <dsp:input type="hidden" id="couponEmail" name="couponEmail" bean="CouponFormHandler.couponEmail" />
           <dsp:input type="hidden" value="dummy" priority="-100" bean="CouponFormHandler.removeCoupon"/>
        </dsp:form>
      
      <dsp:form id="csrBillingClaimableForm" onsubmit="return false" formid="csrBillingClaimableForm" autocomplete="off">
      <dsp:include src="/panels/order/billing/giftCard.jsp" otherContext="${CSRConfigurator.truContextRoot}">
      	<dsp:param name="order" value="${order}"/>
      	<dsp:param name="ProfileRewardNumber" value="${ProfileRewardNumber}"/>
      	<dsp:param name="selectedPaymentGroupType" value="${selectedPaymentGroupType}"/>
       </dsp:include>
         
        
        <!-- ATG Coupen/Promo Code Start -->
        <dsp:include src="/panels/order/billing/promoCode.jsp" otherContext="${CSRConfigurator.truContextRoot}">
      <dsp:param name="order" value="${order}"/>
      </dsp:include>
       <!-- ATG Coupen/Promo Code End -->
       
       <!-- Reward R Us(olson) Integration Start --> 
        <dsp:include src="/panels/order/billing/reward.jsp" otherContext="${CSRConfigurator.truContextRoot}">
        <dsp:param name="order" value="${order}"/>
        </dsp:include>
        
<!-- Reward R Us(olson) Integration End -->        
        <dsp:input bean="PaymentGroupFormHandler.claimItemSuccessURL" type="hidden" value="${successURL }"/>
        <dsp:input bean="PaymentGroupFormHandler.claimItemErrorURL" type="hidden" value="${successURL }"/>
        <dsp:input bean="PaymentGroupFormHandler.claimItem" type="hidden" value="" priority="-10"/>
		 </dsp:form>
		 
		 
        <script type="text/javascript">
        _container_.onLoadDeferred.addCallback(function() {
            atg.keyboard.registerDefaultEnterKey({form:"csrBillingClaimableForm", name:"billingClaimCode"},
              dijit.byNode(dojo.byId("csrBillingClaimableForm")["billingClaimSubmit"]), "buttonClick");
          });
          _container_.onUnloadDeferred.addCallback(function() {
            atg.keyboard.unRegisterDefaultEnterKey({form:"csrBillingClaimableForm",name:"billingClaimCode"});
          });
                         
          atg.commerce.csr.order.billing.giftcardpayment = function() {
        	  var form=dojo.byId("giftCardApplicationForm");
        	  form.giftCardPin.value=dojo.byId("gcpin").value;
        	  form.giftCardNumber.value=dojo.byId("gcNumber").value;
        	  form.giftCardEmail.value=dojo.byId("email").value;
        	  form.CSCRequest.value=true;
        	  dojo.xhrPost({form:form,url:"/TRU-DCS-CSR/panels/order/billing/giftCard.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
     			 if(!(_1f instanceof Error)){
	     			atgNavigate({panelStack:"cmcBillingPS", queryParams: { init : 'true' }});return false;
	        	 }
	          },mimetype:"text/html"}); 
     	}
       
         atg.commerce.csr.order.billing.removegiftcard = function(ele){
        	 var $this = $(ele);
       		 var form=dojo.byId("giftCardRemovalForm");
       		form.giftCardId.value=$this.attr('data-id');
       		form.giftCardEmail.value=dojo.byId("email").value;
      	  	form.CSCRequest.value=true;
       		dojo.xhrPost({form:form,url:"/TRU-DCS-CSR/panels/order/billing/giftCard.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
       			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcBillingPS", queryParams: { init : 'true' }});return false;
	        		 }
    			},mimetype:"text/html"}); 
       	 }
	
         
         
									atg.commerce.csr.order.billing.saveUserInputAndClaimClaimables = function() {
										var a = $('#coupon_Claim_Code').val();
										var form = dojo.byId("applyCouponForm");
										form.couponEmail.value = dojo.byId("email").value;
										form.couponClaimCode.value = dojo
												.byId("coupon_Claim_Code").value;
										atgSubmitAction({panels : [ 'cmcBillingP' ], panelStack : 'cmcBillingPS',form : document.getElementById('applyCouponForm')});
										<%--
										atgSubmitAction({
											form : document.getElementById('applyCouponForm')
										});
										atgNavigate({panelStack:"cmcBillingPS"});
										--%>
									}

									function removePromoCodePaymentPage(couponId) {
										var form = dojo.byId("removeCouponForm");
										form.couponEmail.value = dojo.byId("email").value;
										form.removeCouponClaimCode.value = couponId;
										atgSubmitAction({panels : [ 'cmcBillingP' ], panelStack : 'cmcBillingPS',form : document.getElementById('removeCouponForm')});
										<%--
										atgSubmitAction({
											form : document.getElementById('removeCouponForm')
										});
										atgNavigate({panelStack:"cmcBillingPS"});
										--%>
									}
								</script>
     
    </div>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/claimClaimables.jsp#1 $$Change: 875535 $--%>
