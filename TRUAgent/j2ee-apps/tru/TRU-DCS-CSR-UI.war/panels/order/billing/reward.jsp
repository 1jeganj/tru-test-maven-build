 <%@ include file="/include/top.jspf"%>
<dsp:page>
	<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:getvalueof var="order" param="order" />
		<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />
		<dsp:importbean bean="/atg/commerce/custsvc/order/purchase/BillingFormHandler" />
		<dsp:importbean bean="/atg/multisite/Site" />
	    <dsp:getvalueof param="order.rewardNumber" var="orderRewardNumber"/>
	   <ul class="atg_dataForm atg_commerce_csr_creditClaimForm">              
            <li class="atg_svc_billingClaimCode">
            <span align="left">
              <label>
              <fmt:message key="billing.reward.rs.label" bundle="${TRUCustomResources}"/>
              </label>
            </span>
              		        	
          </li> 
           <image align="right" src="/TRU-DCS-CSR/images/Payment_Rewards-R-Us-Card-Thumb.jpg"></image>  	                
          <br><br>             
          <li class="atg_svc_billingClaimCode">
            <span align="left">
              <label>
              <fmt:message key="billing.reward.message" bundle="${TRUCustomResources}"/>    
              </label>
            </span>  
	     <div class="member-number">
	         <div class="inline">
	             <p><fmt:message key="billing.reward.number.label" bundle="${TRUCustomResources}"/></p>
	             	<input type="text" id="enterMembershipIDInBilling" autocomplete="off" name="enterMembershipIDInBilling" value="${orderRewardNumber}" maxlength="13" onkeypress="return isNumberOnly(event)"/>
	         </div>
	         <%-- <div class="forgot-number-password pull-right">
	             <p>
				<a
				href="${olsonLookUpURL}"
				class="" target="_blank"><fmt:message key="billing.reward.forgot.label" bundle="${TRUCustomResources}"/></a>
	             </p>
	             <p><a class="notMemberpopover" href="javascript:void(0)"><fmt:message key="billing.reward.member.label" bundle="${TRUCustomResources}"/></a>
	             </p>
	         </div> --%>  
	     </div>
        </ul>
        <script type="text/javascript">
	        function isNumberOnly(evt)
		        {
		     	   evt = (evt) ? evt : window.event;
		            var charCode = (evt.which) ? evt.which : evt.keyCode;
		            if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
		     	    	 return true;
		     	    }
		     	    return false;
		        }
	       $(document).on("paste","#enterMembershipIDInBilling",function(e){
	    	   var clipboardData = e.clipboardData || window.clipboardData;
	    	  	if(clipboardData && clipboardData.getData && clipboardData.getData('Text')){
	    	  		var txt = clipboardData.getData('Text')
	    	  	}
	    	  	else{
	    	  		var txt = e.originalEvent.clipboardData.getData('text');
	    	  	}
	    	    if(isNaN(txt)){
	    	        e.preventDefault();
	    	    }
	    	    else
	    	    	{
	    		    	$("#rewardNumberUpdate").attr("disabled",false);
	    				$("#rewardNumberRemove").attr("disabled",false);
	    	    	}
	       });
        </script>
        </dsp:layeredBundle>
        </dsp:page>
        