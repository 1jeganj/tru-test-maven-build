<%-- FIXME: Add page comments

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/choosePaymentOptions.jsp#1 $$Change: 875535 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/PaymentGroupFormHandler"/>
  <dsp:importbean var="shippingGroupFormHandler" bean="/atg/commerce/custsvc/order/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/PaymentGroupDroplet"/>
  <dsp:importbean bean="/atg/svc/agent/ui/AgentUIConfiguration"/>
  <dsp:importbean var="CSRAgentTools" bean="/atg/commerce/custsvc/util/CSRAgentTools"/>
  <dsp:getvalueof var="paymentGroups" param="paymentGroups"/>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" var="CSRConfigurator"/>
  <dsp:getvalueof var="userOrder" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
<c:set var="isExistingOrder" value="false"/>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">

  <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
  <c:set var="order" value="${cart.current}" />
  
    <script type="text/javascript">
      dojo.require("dojox.Dialog");
      dojo.require("atg.widget.validation.CurrencyTextboxEx");
    </script>
    <script type="text/javascript">
    _container_.onLoadDeferred.addCallback(function() {
        dojo.subscribe("/atg/commerce/csr/order/PaymentBalance",
          function (event) {
            atg.commerce.csr.order.billing.paymentBalanceEventListener(event);
          });
      });
      atg.commerce.csr.order.billing.initializePaymentContainer("<c:out value='${userOrder.priceInfo.total}'/>", "<c:out value='${userOrder.priceInfo.currencyCode}'/>", { "locale" : "<dsp:valueof bean='AgentUIConfiguration.javaScriptFormattingLocale' />", "currencySymbol" : "<dsp:valueof bean='CSRAgentTools.currentOrderCurrencySymbolInFormattingLocale' />"});
      atg.commerce.csr.order.billing.initializeCreditCardTypeDataContainer();
    </script>

    <dsp:droplet name="ForEach">
      <dsp:param name="array" bean="/atg/commerce/payment/CreditCardTools.cardCodesMap"/>
      <dsp:oparam name="output">
        <dsp:getvalueof var="cardType" param="key"/>
        <dsp:getvalueof var="cardCode" param="element"/>
        <script type="text/javascript">
          atg.commerce.csr.order.billing.addCreditCardTypeData("<c:out value='${cardType}'/>", "<c:out value='${cardCode}'/>");
        </script>
      </dsp:oparam>
    </dsp:droplet>

    <svc-ui:frameworkUrl var="successURL" panelStacks="cmcCompleteOrderPS,globalPanels"/>
    <svc-ui:frameworkUrl var="errorURL"/>

    <c:set var="csrBillingFormName" value="csrBillingForm"/>

    <dsp:form id="${csrBillingFormName}" formid="csrBillingForm">
    <%--This sets the order level payment options. --%>
    <dsp:setvalue bean="PaymentGroupFormHandler.listId"
                  paramvalue="order.id"/>
    <dsp:input bean="PaymentGroupFormHandler.listId"
               beanvalue="PaymentGroupFormHandler.listId" priority="5"
               type="hidden"/>


        <dsp:include src="/panels/order/billing/displayPaymentOptions.jsp" otherContext="${CSRConfigurator.truContextRoot}">
        <dsp:param name="displayPaymentOptions" param="displayPaymentOptions"/>
        <dsp:param name="paymentGroups" param="paymentGroups"/>
        <dsp:param name="paymentGroupList" bean="PaymentGroupFormHandler.currentList" />
        </dsp:include>

      <dsp:input type="hidden" priority="-10" value="" name="csrHandleApplyPaymentGroups"
                 bean="PaymentGroupFormHandler.applyPaymentGroups"/>

      <dsp:input type="hidden" value="${errorURL}" name="errorURL"
                 bean="PaymentGroupFormHandler.applyPaymentGroupsErrorURL"/>

      <dsp:input type="hidden" value="${successURL}" name="successURL"
                 bean="PaymentGroupFormHandler.applyPaymentGroupsSuccessURL"/>

      <dsp:input type="hidden" priority="-10" value=""
                 name="csrPaymentGroupsPreserveUserInputOnServerSide"
                 bean="PaymentGroupFormHandler.preserveUserInputOnServerSide"/>
	
	  <dsp:input type="hidden" id="rewardNumberCheckOut" name="rewardNumberCheckOut" bean="PaymentGroupFormHandler.rewardNumber"/>
	  <dsp:input type="hidden" id="emailInBilling" name="emailInBilling" bean="PaymentGroupFormHandler.email"/>
      <dsp:input type="hidden" value="false" name="persistOrder" bean="PaymentGroupFormHandler.persistOrder"/>
      <div style="display:none">
        <span class="atg_messaging_requiredIndicator"
              id="orderTotalUIValidatorAlert">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <input type="hidden" name="orderTotalUIValidator"
               dojoType="dijit.form.ValidationTextBox"
               validIf="function () { return atg.commerce.csr.order.billing.isZeroBalance();}"
               inlineIndicator="orderTotalUIValidatorAlert"/>
      </div>
			<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.firstName" name="firstNameRA" id="firstNameRA" />
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.lastName" name="lastNameRA" id="lastNameRA" />
	 	<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.address1" name="address1RA" id="address1RA" />
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.address2" name="address2RA" id="address2RA" />
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.city" name="cityRA" id="cityRA"/>
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.state" name="stateRA" id="stateRA"/>
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.postalCode" name="postalCodeRA" id="postalCodeRA"/>
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.phoneNumber" name="phoneNumberRA" id="phoneNumberRA"/>
		<dsp:input type="hidden" bean="PaymentGroupFormHandler.addressInputFields.country" name="countryRA" id="countryRA"/>
    </dsp:form>
    
    <div class="atg_commerce_csr_billingSummary">
      <div class="atg_commerce_csr_orderModifications">
      <dsp:include src="/include/order/promotionsSummary.jsp" otherContext="${CSRConfigurator.contextRoot}">
        <dsp:param name="order" value="${userOrder}"/>
      </dsp:include>
      </div>
      <csr:displayOrderSummary
        order="${userOrder}" isExistingOrder="${isExistingOrder}"
        isShowHeader="false"
        isDisplayBalanceDue="${true}"
        />
      <div class="atg_commerce_csr_billingFooter">
        <fmt:message var="returnToShippingMethodLabel" key="common.returnToShippingMethod"/>
        <fmt:message var="returnToShippingAddressLabel" key="common.returnToShippingAddress"/>
        <fmt:message var="continueToOrderReviewLabel" key="newOrderBilling.continueToOrderReview"/>

        <c:if test="${shippingGroupFormHandler.nonGiftHardgoodShippingGroupCount > 0}">
          <c:set var="goBackStack" value="cmcShippingMethodPS"/>
          <c:set var="goBackLabel" value="${returnToShippingMethodLabel}"/>
        </c:if>
        <c:if test="${shippingGroupFormHandler.nonGiftHardgoodShippingGroupCount == 0}">
          <c:set var="goBackStack" value="cmcShippingAddressPS"/>
          <c:set var="goBackLabel" value="${returnToShippingAddressLabel}"/>
        </c:if>

        <dsp:include src="/include/order/checkoutFooter.jsp" otherContext="${CSRConfigurator.truContextRoot}">
          <dsp:param name="goBackLabel" value="${goBackLabel}"/>
          <dsp:param name="goBackStack" value="${goBackStack}"/>
          <dsp:param name="nextButtonOnClick"
                     value="atg.commerce.csr.order.billing.applyPaymentGroups({form:'csrBillingForm'});return false;"/>
          <dsp:param name="nextButtonLabel" value="${continueToOrderReviewLabel}"/>
          <dsp:param name="nextButtonFormId" value="${csrBillingFormName}"/>
        </dsp:include>
      </div>
    </div>

    <script type="text/javascript">
      _container_.onLoadDeferred.addCallback(function() {
    	atg.commerce.csr.order.billing.assignBalance();
        atg.commerce.csr.order.billing.firePaymentBalanceDojoEvent();
        atg.commerce.csr.order.billing.csrBillingFormValidate();
        atg.service.form.watchInputs('${csrBillingFormName}', atg.commerce.csr.order.billing.csrBillingFormValidate);

        var theButton = document.getElementById("checkoutFooterNextButton");
        if (theButton != null) {
          theButton.focus();
        }
      });
      _container_.onUnloadDeferred.addCallback(function () {
        atg.service.form.unWatchInputs('${csrBillingFormName}');
      });
    </script>
    <script type="text/javascript">
      if (!dijit.byId("editPaymentOptionFloatingPane")) {
        new dojox.Dialog({ id:"editPaymentOptionFloatingPane",
          cacheContent: "false",
          executeScripts:"true",
          scriptHasHooks:"true",
          duration: 100,
          "class":"atg_commerce_csr_popup"});
      }
      
     
    </script>
  </dsp:layeredBundle>
</dsp:page>
<%-- end of newOrderBilling.jsp --%>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/choosePaymentOptions.jsp#1 $$Change: 875535 $--%>
