<%@  include file="/include/top.jspf"%>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
	<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" />
	<dsp:importbean bean="/atg/commerce/custsvc/promotion/CouponFormHandler" />
	<dsp:getvalueof var="appliedPromoNames" bean="ShoppingCart.current.appliedPromoNames"/>
	<dsp:getvalueof var="notAppliedPromoNames" bean="ShoppingCart.current.notAppliedPromoNames"/>
	<dsp:getvalueof var="notAppliedCouponCodes" bean="ShoppingCart.current.notAppliedCouponCode"/>
	<dsp:getvalueof var="order" param="order" />
	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />
			<div class="promo-code-table">
		<table>
			<tbody>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="ActiveCustomerProfile.activePromotions" />
					<dsp:param name="elementName" value="promotionstatus" />
					<dsp:getvalueof var="promotionDetails"
						param="promotionstatus.promotion.promotionDetails" />
					<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName"/>
					<dsp:oparam name="output">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="promotionstatus.coupons" />
							<dsp:param name="elementName" value="coupon" />
							<dsp:oparam name="output">
					 <dsp:getvalueof var="couponID" param="coupon.id" />
					 <dsp:getvalueof var="appliedCoupons" bean="ShoppingCart.current.singleUseCoupon"/>		
								<c:if test="${appliedCoupons.containsValue(couponID) and appliedPromoNames.containsValue(promotionName)}">
								<tr>
					 				<td>${promotionName}</td>
									<td><fmt:message key="checkout.payment.applied" bundle="${TRUCustomResources}" /></td>
									<td>
										<a onclick="removePromoCodePaymentPage('${couponID}')" style="cursor:pointer;">
											<fmt:message key="checkout.payment.remove" bundle="${TRUCustomResources}"/>
										</a>
									</td>
								</tr>
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="ActiveCustomerProfile.activePromotions" />
					<dsp:param name="elementName" value="promotionstatus" />
					<dsp:getvalueof var="promotionName" param="promotionstatus.promotion.displayName"/>
					<dsp:oparam name="output">
					<dsp:droplet name="ForEach">
							<dsp:param name="array" param="promotionstatus.coupons" />
							<dsp:param name="elementName" value="coupon" />
							<dsp:oparam name="output">
							<dsp:getvalueof var="couponID" param="coupon.id" />
							<c:choose>
									<c:when test="${notAppliedCouponCodes.containsValue(couponID)}">
										<tr>
											<td><dsp:valueof value="${promotionName}"/></td>
											<td>Not Applied</td>
											<td>
												<a onclick="removePromoCodePaymentPage('${couponID}')" style="cursor:pointer;"> 
												<fmt:message key="checkout.payment.remove" bundle="${TRUCustomResources}"/>
												</a>
											</td>
										</tr>
									</c:when>
									<c:when test="${notAppliedPromoNames.containsValue(promotionName)}">
										<tr>
											<td><dsp:valueof value="${promotionName}"/></td>
											<td>Not Applied</td>
											<td>
												<a onclick="removePromoCodePaymentPage('${couponID}')" style="cursor:pointer;"> 
												<fmt:message key="checkout.payment.remove" bundle="${TRUCustomResources}"/>
												</a>
											</td>
										</tr>
									</c:when>
									</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</tbody>
		</table>
	</div>
	</dsp:layeredBundle>
</dsp:page>