<%-- This page is used to add credit card as payment option.

--%>

<%@ include file="/include/top.jspf"%>

<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="cart"/>
  <c:set var="currentOrder" value="${cart.current}"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CreateCreditCardFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/profile/AddressHolder"/>
  <dsp:importbean var="addCreditCard" bean="/atg/commerce/custsvc/ui/fragments/order/AddCreditCard"/>
  <dsp:importbean var="creditCardConfig"
                  bean="/atg/commerce/custsvc/ui/CreditCardConfiguration"/>                  
  <dsp:importbean var="creditCardForm" bean="/atg/commerce/custsvc/ui/fragments/CreditCardForm"/>
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  

  <svc-ui:frameworkUrl var="successURL" panelStacks="cmcBillingPS,globalPanels"/>
  <svc-ui:frameworkUrl var="errorURL" panelStacks="cmcBillingPS"/>
  <dsp:droplet name="Switch">
    <dsp:param bean="CreateCreditCardFormHandler.formError" name="value"/>
    <dsp:oparam name="false">
      <dsp:setvalue bean="CreateCreditCardFormHandler.clearCreditCard" value="true"/>
      <%-- This resets the addresses in the address holder. --%>
      <dsp:setvalue bean="AddressHolder.resetAddresses" value="true"/>
    </dsp:oparam>
  </dsp:droplet>
  <dsp:setvalue bean="CreateCreditCardFormHandler.autoSelectInitialAddress" value="true"/>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">

    <c:set var="formId" value="csrBillingAddCreditCard"/>
     <c:set var="pageName" value="billing"/>
    <dsp:form method="POST" id="${formId}" formid="${formId}">

<%--       <dsp:input type="hidden" priority="-10" value="" bean="CreateCreditCardFormHandler.newCreditCard"/> --%>
      <dsp:input type="hidden" value="${successURL}" bean="CreateCreditCardFormHandler.newCreditCardErrorURL"/>
      <dsp:input type="hidden" value="${errorURL}" bean="CreateCreditCardFormHandler.newCreditCardSuccessURL"/>

      <%-- If the tab container is used by the billing page, then this snippet below selects the right
      tab on error. --%>
      <dsp:droplet name="Switch">
        <dsp:param bean="CreateCreditCardFormHandler.formError"
                   name="value"/>
        <dsp:oparam name="true">
          <script type="text/javascript">
            _container_.onLoadDeferred.addCallback(function () {
              var pgAddTabContainer = dijit.byId("paymentOptionAddContainer");
              if (pgAddTabContainer) pgAddTabContainer.selectChild("${creditCardConfig.type}");
            });
          </script>
        </dsp:oparam>
      </dsp:droplet>

      <div class="atg_commerce_csr_add_credit_card_form">
        <dsp:include src="${creditCardForm.URL}" otherContext="${CSRConfigurator.truContextRoot}">
          <dsp:param name="formId" value="${formId}"/>
          <dsp:param name="creditCardBean"
                     value="/atg/commerce/custsvc/order/CreateCreditCardFormHandler.creditCard"/>
          <dsp:param name="creditCardAddressBean"
                     value="/atg/commerce/custsvc/order/CreateCreditCardFormHandler.creditCard.billingAddress"/>
          <dsp:param name="creditCardFormHandler" value="/atg/commerce/custsvc/order/CreateCreditCardFormHandler"/>
          <dsp:param name="submitButtonId" value="${formId}_billingAddCreditCardButton"/>
          <dsp:param name="isMaskCardNumber" value="${false}"/>
          <dsp:param name="isUseExistingAddress" value="${true}"/>
        </dsp:include>
              <div class="atg_commerce_csr_saveProfile">
                <dsp:droplet name="/atg/dynamo/droplet/Switch">
                  <dsp:param
                    bean="/atg/userprofiling/ActiveCustomerProfile.transient"
                    name="value"/>
                  <dsp:oparam name="false"> 
                    <dsp:input type="checkbox" checked="true" 
                    bean="CreateCreditCardFormHandler.copyAddress" iclass="defaultSave_checkBox" value="true"/>
                    <fmt:message key="newOrderBilling.addEditCreditCard.field.card.saveToProfile"/>
                    
                   <dsp:setvalue bean="CreateCreditCardFormHandler.copyAddress"  value="true"/>
                  </dsp:oparam>
                  <dsp:oparam name="true">
                    <dsp:input type="hidden" iclass="defaultSave_checkBox" bean="CreateCreditCardFormHandler.copyAddress" value="false"/>
                  <dsp:setvalue bean="CreateCreditCardFormHandler.copyAddress"  value="false"/>
                  </dsp:oparam>
                </dsp:droplet>
              </div>
              <div class="atg_commerce_csr_addControls">
                <fmt:message key="newOrderBilling.addCreditCard.header.addCreditCard" var="addCreditCardLabel"/>
       <%--         <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton"
                       value="${addCreditCardLabel}"
                       onclick="${formId}HideValidationPopups();atg.commerce.csr.order.billing.saveUserInputAndAddCreditCard();return false;"
                       dojoType="atg.widget.validation.SubmitButton"/> --%>
                   
                   
                   
                                       
                 <%--  <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton" value="${addCreditCardLabel}" dojoType="atg.widget.validation.SubmitButton"  onclick="${formId}HideValidationPopups();startTokenization();return false;"
           class="selectyesno"/>   --%>
                                      
                 <input type="button" id="${formId}_billingAddCreditCardButton" name="billingAddCreditCardButton" value="${addCreditCardLabel}" dojoType="atg.widget.validation.SubmitButton"  onclick="getOrderId();return false;"
           			class="selectyesno ${formId}"/> 
           
           
                 
              </div>
              
         <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
       
        </c:url>      
         <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/billingSuggestions.jsp">
          <c:param name="addrKey" value="${addressKey}"/>
             <c:param name="pageName" value="billing"/>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
            <c:param name="sp" value="${shippingGroup}"/>
        </c:url>     
              
       
      </div>
    </dsp:form>
     <div id="hiddenCreditSuggestedAddress" style="display:none"></div>
    <c:set var="formId" value="csrBillingAddCreditCard"/>
       <dsp:getvalueof var="formId" param="formId"/>
    <%-- Before adding the credit card, save the user input in the page such as amount.--%>
    <script type="text/javascript">
   
    function isValidForm(){
    	var existingAddress = $("#csrBillingAddCreditCard_existingBillingArea").find("input[type=radio]:checked").val();
    	var emptyInputError = false;
    	if(existingAddress == 'false')
    		{
		    	$("#csrBillingAddCreditCard_addressArea").find("input[type=text]").not(":hidden").not("#csrBillingAddCreditCard_address2").filter(function(){
		    		if($(this).val() == "")
		    			{
		    				emptyInputError = true;
		    			}
		    	});
    		}
    	if($("#csrBillingAddCreditCard_nameOnCreditCard").val() == "" || 
    		$("#csrBillingAddCreditCard_creditCardType").val() =="" ||
    		$("#csrBillingAddCreditCard_creditCardNumber").val() =="" ||
    		($("#csrBillingAddCreditCard_expirationMonth").val() =="" && $("#csrBillingAddCreditCard_creditCardType").val() != "R Us Private Label Credit Card") ||
    		($("#csrBillingAddCreditCard_expirationYear").val() =="Year" && $("#csrBillingAddCreditCard_creditCardType").val() != "R Us Private Label Credit Card") ||
    		$("#csrBillingAddCreditCard_cvv").val() =="" || emptyInputError)
    		{
    			$('#csrBillingAddCreditCard').find("#csrBillingAddCreditCard_billingAddCreditCardButton").attr("disabled",true);
    		}
    	else
    		{
    		$('#csrBillingAddCreditCard').find("#csrBillingAddCreditCard_billingAddCreditCardButton").attr("disabled",false);
    		}
    	}
    $(document).ready(function(){
    	setTimeout(function(){
    		isValidForm();
    	},300)
    	
    });
    $(document).on('keyup',"#csrBillingAddCreditCard input[type=text]",function(){
		isValidForm();
	});
    
   
    
      atg.commerce.csr.order.billing.saveUserInputAndAddCreditCard = function() {
        var returnValue = atg.commerce.csr.order.billing.saveUserInput();
        if (returnValue == true) {
          atg.commerce.csr.order.billing.addCreditCard();
        }
        ;
      }
      
      var ${formId}HideValidationPopups = function () {
          if (dojo.byId("dijit__MasterTooltip_0")) {
            dojo.byId("dijit__MasterTooltip_0").style.display="none";
          }
        };
        function getOrderId(){
        	if($('#error').length)
        		{
        			$('#error').remove();
        		}
        	if($('#existingAddress').length)
        		{
        			var existingAddress = $('#existingAddress').val();
        		}
        	else
        		{
        			existingAddress = 'false';
        		}
        	
       	 var cardType = $("#csrBillingAddCreditCard_creditCardType").val();
			 var cvv = $("#csrBillingAddCreditCard_cvv").val();
			 var cvvLength = cvv.length;
			 if((cardType != "American Express" && cvvLength < 3) || (cardType == "American Express" && cvvLength < 4) )
				{
					$('#csrBillingAddCreditCard_cvv').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid cvv</span></td>');
					return false;
				} 
        	 if(existingAddress=='false'){
        		var emptyInputError = false,emptyInputAddressError = false;
        		var noState = true;
        		$("#csrBillingAddCreditCard_addressArea").find("input[type='text']").not(":hidden").not("#csrBillingAddCreditCard_middleName").not("#csrBillingAddCreditCard_address2").filter(function(){
        			if($.trim($(this).val()) == '')
        				{
        					emptyInputAddressError = true;
        				}
        		});
        		$("#csrBillingAddCreditCard_creditCardForm").find("input[type='text'],select").not(":hidden").filter(function(){
        			if($.trim($(this).val()) == '')
        				{
        					emptyInputError = true;
        				}
        		});
        		if((cardType == "RUSPrivateLabelCard" || cardType == "R Us Private Label Credit Card" ) && $("#csrBillingAddCreditCard_expirationMonth").val() == "" && $("#csrBillingAddCreditCard_expirationYear").val() == "Year")
	       		 {
	       		 	emptyInputError = false;
	       		 }
        		if(emptyInputError || emptyInputAddressError)
        		{
        			return false;
        		}
        		
		 	 var country= $("#csrBillingAddCreditCard_country").val();
			 var address1 = $('#csrBillingAddCreditCard_address1').val();
			 var address2 = $('#csrBillingAddCreditCard_address2').val();
			 var city = $('#csrBillingAddCreditCard_city').val();
			 var state1 = $('#csrBillingAddCreditCard_state').val();
			 var postalCode = $('#csrBillingAddCreditCard_postalCode').val();
			 var phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
			 var address2 = $('#csrBillingAddCreditCard_address2').val(); 
			
			/*  var country= $("#inStorePayment_country").val();
			 var address1 = $('#inStorePayment_address1').val();
			 var address2 = $('#inStorePayment_address2').val();
			 var city = $('#inStorePayment_city').val();
			 var state1 = $('#inStorePayment_state').val();
			 var postalCode = $('#inStorePayment_postalCode').val();
			 var phoneNumber = $('#inStorePayment_phoneNumber').val();
			 var address2 = $('#inStorePayment_address2').val(); */
			 var pageName="billing";
			 
			if(country != 'United States')
				{
					if(state1 == 'NA')
						{
							var state = 'NA'
						}
				}
			else{
				
				state1 = state1.split("-")[0];
				var state = $.trim(state1);
			}
	     	if(postalCode.length < 5)
			{
	     		
				$('#csrBillingAddCreditCard_postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
				return false;
			}
			else if(phoneNumber.length < 10)
			{
				$('#csrBillingAddCreditCard_phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
				return false;
			}
	     	if(country != 'United States')
			{
	     		startTokenization();
			}
	     	else{
				dojo.xhrPost({
						url : "${resultsUrl}?address1="+address1+"&address2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&windowid="+window.windowId,
						encoding : "utf-8",
						preventCache : true,
						handle : function(_362, _363) {
							
							var response = dojo.trim(_362);
							dojo.query("#hiddenCreditSuggestedAddress")[0].innerHTML = response;
							var derivedStatus = dojo.query("#hiddenCreditSuggestedAddress #derivedStatus")[0].innerHTML;
							var addressRecognized = dojo.query("#hiddenCreditSuggestedAddress #addressDoctorVO1")[0].innerHTML;
							
							if(derivedStatus == "NO" && addressRecognized == "true"){
								var data =  dojo.query("#hiddenCreditSuggestedAddress #firstSuggestedAddress")[0].innerHTML
								var form =  document.getElementById('csrBillingAddCreditCard'); 
								var suggestedAddress ={};
								suggestedAddress.city = dojo.query("#hiddenCreditSuggestedAddress #suggestedCity-1")[0].value;
								suggestedAddress.state = dojo.query("#hiddenCreditSuggestedAddress #suggestedState-1")[0].value;
								//suggestedAddress.country = dojo.query("#hiddenCreditSuggestedAddress #suggestedCountry-1")[0].value;
								suggestedAddress.country = 'US';
								suggestedAddress.postalCode = dojo.query("#hiddenCreditSuggestedAddress #suggestedPostalCode-1")[0].value;
								suggestedAddress.phoneNumber = phoneNumber;
								suggestedAddress.address1 = dojo.query("#hiddenCreditSuggestedAddress #suggestedAddress1-1")[0].value;
								suggestedAddress.address2 = dojo.query("#hiddenCreditSuggestedAddress #suggestedAddress2-1")[0].value; 
					        	startTokenization(suggestedAddress,0);
					        	/* $("#country").val(country);
					        	$("#address1").val(address1);
					        	$("#address2").val(address2);
					        	$("#city").val(city);
					        	$("#state").val(state);
					        	$("#postalCode").val(postalCode); */
					        	
							return false;
							}
							
							else if(derivedStatus == "YES"){
						
								atg.commerce.csr.common
								.showPopupWithReturn({
									popupPaneId : 'csrAddressDoctorFloatingPane',
									url : "${addressDoctorURL}&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&windowId="+window.windowId,
									onClose : function(args) {
										if (args.result == 'ok') {
											atgSubmitAction({
												panelStack : [
														'cmcShippingAddressPS',
														'globalPanels' ],
												form : document
														.getElementById('transformForm')
											});
										}
									}
								});
								return false;
							}
							else{
								startTokenization("",0);
								return false;
							}
							
							
							if (document
									.getElementById("inStorePickupResultsss")) {
								document
										.getElementById("inStorePickupResultsss").innerHTML = _362;
							}

						},
						mimetype : "text/html"
					});
	     		}
        	 }
        	 else{
        		// var isDefaultSaveChecked = 4
        		 startTokenization();
        	 }
		}
        
        $(document).on("click",".defaultSave_checkBox",function(){
        	if($(this).is(":checked"))
        		{
        			$(this).val("true");
        		}
        	else
        		{
        			$(this).val("false");
        		}
        })
        $(document).on("keypress","#csrBillingAddCreditCard_cvv,#csrBillingEditCreditCard_cvv",function(evt){
              		 evt = (evt) ? evt : window.event;
             	      var charCode = (evt.which) ? evt.which : evt.keyCode;
             	      if(charCode == 45)return true;
             	      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
             	          return false;
             	      }
             	      return true;
              	});
    </script>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/addCreditCard.jsp#1 $$Change: 875535 $--%>
