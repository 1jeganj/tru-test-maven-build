<%@ include file="/include/top.jspf" %>
<dsp:page>


<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
<dsp:getvalueof var="order" param="order"/>


<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
</style>
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />


		<div class="giftCardbalanceCheck">
		Enter your card number and PIN, then click "Check Balance".
			<ul class="atg_dataForm atg_commerce_csr_creditClaimForm">
				<li class="atg_svc_billingClaimCode">
					<span align="left">
						<label> <fmt:message key="billing.giftcard.label" bundle="${TRUCustomResources}"/></label>
					</span>
				</li> 
				<br><br>
				<div class="inline">
					<p>
						<fmt:message key="billing.giftcard.cardnumber.label" bundle="${TRUCustomResources}"/>
					</p>
					<input type="number" id="gccNumber" name="gccNumber" class="chkNumbersOnly" value=""/> 
				</div>
				<div class="availableOptionsForm">
					<p>
					   <fmt:message key="billing.giftcard.pinnumber.label" bundle="${TRUCustomResources}" />
					</p>
					 <input type="password" id="gccpin" name="gccpin" class="chkNumbersOnly" value=""/>	 
		     				 <button type="button" id="co-payment-gift-card-check" 
							onclick="giftCardBalCheck();return false;"
								dojoType="atg.widget.validation.SubmitButton" value="Check Balance"> </button>
				</div>
		</ul>
	</div>
	<div id="giftCardBalance"></div>
</dsp:layeredBundle>
</dsp:page>