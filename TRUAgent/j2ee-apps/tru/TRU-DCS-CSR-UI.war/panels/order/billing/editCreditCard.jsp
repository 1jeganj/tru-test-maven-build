<%--
During the checkout flow or process, this page is used to edit credit cards.
The updated credit card information could be saved in the profile or may be only used in the current order.

Anonymous users will not see the option to save the credit card to the profile.

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/editCreditCard.jsp#1 $$Change: 875535 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean bean="/atg/userprofiling/ActiveCustomerProfile"/>
  <dsp:importbean var="CSRUpdateCreditCardFormHandler"
                  bean="/atg/commerce/custsvc/order/UpdateCreditCardFormHandler"/>
  <dsp:importbean var="urlDroplet"
                  bean="/atg/svc/droplet/FrameworkUrlDroplet"/>
  <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
  <dsp:importbean var="pageFragment"
                  bean="/atg/commerce/custsvc/ui/fragments/order/EditCreditCard"/>
  <dsp:importbean var="pgConfig"
                  bean="/atg/commerce/custsvc/ui/CreditCardConfiguration"/>
  <dsp:importbean var="creditCardForm" bean="/atg/commerce/custsvc/ui/fragments/CreditCardForm"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="cart"/>
    <dsp:importbean var="CSRConfigurator"
                  bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <c:set var="currentOrder" value="${cart.current}"/>

  <dsp:getvalueof var="nickname" param="nickname"/>
  <dsp:getvalueof var="success" param="success"/>
  <c:url var="successErrorURL" context="${CSRConfigurator.truContextRoot}"
         value="${pgConfig.editPageFragment.URL}">
    <c:param name="nickname" value="${nickname}"/>
    <c:param name="${stateHolder.windowIdParameterName}"
             value="${windowId}"/>
           
    <c:param name="success" value="true"/>
  </c:url>
<style>
.mine .atg-csc-base-table-cell{padding-top:1px!important;}
</style>
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">

    <c:set var="formId" value="csrBillingEditCreditCard"/>
    <dsp:form method="POST" id="${formId}" formid="${formId}">
      <dsp:input type="hidden" priority="-10" value=""
                 bean="UpdateCreditCardFormHandler.updateCreditCard"/>
      <dsp:input type="hidden" value="${successErrorURL }"
                 bean="UpdateCreditCardFormHandler.updateCreditCardErrorURL"/>
      <dsp:input type="hidden" value="${successErrorURL }"
                 bean="UpdateCreditCardFormHandler.updateCreditCardSuccessURL"/>
      <dsp:input type="hidden" value="${fn:escapeXml(nickname) }"
                 bean="UpdateCreditCardFormHandler.creditCardName"/>
      <dsp:input type="hidden" value="${fn:escapeXml(nickname) }"
                 bean="UpdateCreditCardFormHandler.creditCardByNickname" priority="5"/>
      <dsp:setvalue bean="UpdateCreditCardFormHandler.creditCardByNickname" value="${fn:escapeXml(nickname) }"/>

      <div id="editCreditCardPagePrompt" class="atg_commerce_csr_popupPanel">
      <dsp:layeredBundle basename="${pgConfig.resourceBundle}">
       <fmt:message var="editPageFragmentTitle" key="${pgConfig.editPageFragmentTitleKey}"/>
      </dsp:layeredBundle>
        <div>
          <dsp:droplet name="Switch">
            <dsp:param bean="UpdateCreditCardFormHandler.formError"
                       name="value"/>
            <dsp:oparam name="true">

              <span class="atg_commerce_csr_common_content_alert"><fmt:message key="common.error.header"/></span>
              <br>
            <span class="atg_commerce_csr_common_content_alert">
            <ul>
              <dsp:droplet name="ErrorMessageForEach">
                <dsp:param bean="UpdateCreditCardFormHandler.formExceptions"
                           name="exceptions"/>
                <dsp:oparam name="output">
                  <li>
                    <dsp:valueof param="message"/>
                  </li>
                </dsp:oparam>
              </dsp:droplet>
            </ul>
            </span>
            </dsp:oparam>
            <dsp:oparam name="false">
              <c:if test="${success}">
                <%--When there is no error on the page submission, close the popup page and refresh the parent page.
                the parent page only will refresh if the result parameter value is ok. --%>
                <script type="text/javascript">
                  hidePopupWithResults('editCreditCardPagePrompt', {result : 'ok'});
                </script>
              </c:if>
            </dsp:oparam>
          </dsp:droplet>
        </div>

						  <c:set var="isSubmitted" value="${false}"/>
						  <dsp:droplet name="/atg/commerce/custsvc/order/IsOrderSubmitted">
						    <dsp:param name="order" value="${currentOrder}"/>
						    <dsp:oparam name="true">
						      <c:set var="isSubmitted" value="${true}"/>
						    </dsp:oparam>
						  </dsp:droplet>

        <ul class="atg_dataForm atg_commerce_csr_paymentForm mine">
         <dsp:include src="${creditCardForm.URL}" otherContext="${CSRConfigurator.truContextRoot}">
            <dsp:param name="formId" value="${formId}"/>
            <dsp:param name="creditCardBean"
                       value="/atg/commerce/custsvc/order/UpdateCreditCardFormHandler.workingCreditCard"/>
            <dsp:param name="creditCardAddressBean"
                       value="/atg/commerce/custsvc/order/UpdateCreditCardFormHandler.newAddress"/>
            <dsp:param name="creditCardFormHandler" value="/atg/commerce/custsvc/order/UpdateCreditCardFormHandler"/>
            <dsp:param name="submitButtonId" value="billingEditCreditCardButton"/>
            <dsp:param name="isMaskCardNumber" value="${true}"/>
            <dsp:param name="isUseExistingAddress" value="${true}"/>
            <dsp:param name="disableCreditCardType" value="${isSubmitted}"/>
            <dsp:param name="disableCreditCardNumber" value="${isSubmitted}"/>
            <dsp:param name="isEditCreditCard" value="${true}"/>
          </dsp:include>
        </ul>
        <div class="atg_commerce_csr_panelFooter">
          <dsp:droplet name="/atg/dynamo/droplet/Switch">
            <dsp:param bean="UpdateCreditCardFormHandler.creditCardExistsInProfile"
                       name="value"/>
            <dsp:oparam name="true">
              <dsp:droplet name="/atg/dynamo/droplet/Switch">
                <dsp:param
                  bean="/atg/userprofiling/ActiveCustomerProfile.transient"
                  name="value"/>
                <dsp:oparam name="false">
                  <div style="float:left;margin-left:20px">
                    <dsp:input type="checkbox" checked="${true}"
                               bean="UpdateCreditCardFormHandler.updateProfile" name="updateToProfile"/>
                    <fmt:message key="newOrderBilling.addEditCreditCard.field.card.saveToProfile"/>
                  </div>  
                </dsp:oparam>
                <dsp:oparam name="true">
                  <dsp:input type="hidden" bean="UpdateCreditCardFormHandler.updateProfile" value="false"/>
                </dsp:oparam>
              </dsp:droplet>
            </dsp:oparam>
            <dsp:oparam name="false">
              <dsp:input type="hidden" bean="UpdateCreditCardFormHandler.updateProfile" value="false"/>
            </dsp:oparam>
          </dsp:droplet>
          <input type="hidden" name="_DARGS"
                 value="${CSRConfigurator.truContextRoot}/panels/order/billing/editCreditCard.jsp.csrBillingEditCreditCard"/>
  <%--         <input type="button" id="billingEditCreditCardButton"
                 value="<fmt:message key='common.save.title'/>"
                 onclick="editCreditCard();"
                 dojoType="atg.widget.validation.SubmitButton"/> --%>
           <input type="button" id="billingEditCreditCardButton"
                 value="<fmt:message key='common.save.title'/>"
                 onclick="getOrderId();"
                 dojoType="atg.widget.validation.SubmitButton"/>       
                 
          <input type="button" value="<fmt:message key='common.cancel.title'/>"
                 onclick="hidePopupWithResults( 'editCreditCardPagePrompt', {result : 'cancel'});return false;"/>
        </div>
        <div id="hiddenEditCreditSuggestedAddress" style="display:none"></div>
      </div>
    </dsp:form>
  </dsp:layeredBundle>
  
    	<c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
       
        </c:url>      
         <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/billingSuggestions.jsp">
          <c:param name="addrKey" value="${addressKey}"/>
             <c:param name="pageName" value="editBilling"/>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
            <c:param name="sp" value="${shippingGroup}"/>
        </c:url>  
  
  <script type="text/javascript">
  	function editCreditCard(suggestedAddressFromAJAX,createNewAddressFlag){
  		
  		var createNewAddress = $("#editCreditCardPagePrompt").find("input[type='radio']:checked").val();
  		var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
  		if((createNewAddress == 'false'&& addressFlag == '0')||(suggestedAddressFromAJAX == 'false'&& createNewAddressFlag == '0'))
  			{
  				var firstName = $("#csrBillingEditCreditCard_firstName").val();
  				var middleName = $("#csrBillingEditCreditCard_middleName").val();
  				var lastName = $("#csrBillingEditCreditCard_lastName").val();
  				var country = getCountry();
  				var address1 = $("#csrBillingEditCreditCard_address1").val();
  				var address2 = $("#csrBillingEditCreditCard_address2").val();
  				var city = $("#csrBillingEditCreditCard_city").val();
  				var state = $("#csrBillingEditCreditCard_state").val();
  				if(country != 'US')
				{
					if(state == 'NA')
						{
							var state1 = 'NA'
						}
				}
				else{
					var state1 = $.trim(state.split("-")[0]);
				}
  				var postalCode = $("#csrBillingEditCreditCard_postalCode").val();
  				var phoneNo = $("#csrBillingEditCreditCard_phoneNumber").val();
				$("#csrBillingEditCreditCard_firstName_existingAddress").val(firstName);
				$("#csrBillingEditCreditCard_middleName_existingAddress").val(middleName);
				$("#csrBillingEditCreditCard_lastName_existingAddress").val(lastName);
				$("#csrBillingEditCreditCard_country_existingAddress").val(country);
				$("#csrBillingEditCreditCard_address1_existingAddress").val(address1);
				$("#csrBillingEditCreditCard_address2_existingAddress").val(address2);
				$("#csrBillingEditCreditCard_city_existingAddress").val(city);
				$("#csrBillingEditCreditCard_state_existingAddress").val(state1);
				$("#csrBillingEditCreditCard_postalCode_existingAddress").val(postalCode);
				$("#csrBillingEditCreditCard_phoneNumber_existingAddress").val(phoneNo);
				atg.commerce.csr.order.billing.editCreditCard('${successErrorURL}');
				hidePopupWithResults( 'csrAddressDoctorFloatingPane', {result : 'cancel'});
  			}
  		else if(addressFlag != "0" && !suggestedAddressFromAJAX){
  				var country,address1,city,state,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
  		     		 country= $("#suggestedCountry-"+addressFlag).val();
  					 address1 = $('#suggestedAddress1-'+addressFlag).val();
  		   			 city = $('#suggestedCity-'+addressFlag).val();
  		   			 state = $('#suggestedState-'+addressFlag).val();
  		   			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
  		   			 address2 = $('#suggestedAddress2-'+addressFlag).val();
  		   			 phoneNumber = $('#${formId}_phoneNumber').val();
  		   			 firstName = $('#${formId}_firstName').val();
  		   			 middleName = $('#${formId}_middleName').val();
  		   			 lastName = $('#${formId}_lastName').val();
  		   			 state1 = $.trim(state.split("-")[0]);
  				
  				
  				
				$("#csrBillingEditCreditCard_firstName_existingAddress").val(firstName);
				$("#csrBillingEditCreditCard_middleName_existingAddress").val(middleName);
				$("#csrBillingEditCreditCard_lastName_existingAddress").val(lastName);
				$("#csrBillingEditCreditCard_country_existingAddress").val(country);
				$("#csrBillingEditCreditCard_address1_existingAddress").val(address1);
				$("#csrBillingEditCreditCard_address2_existingAddress").val(address2);
				$("#csrBillingEditCreditCard_city_existingAddress").val(city);
				$("#csrBillingEditCreditCard_state_existingAddress").val(state1);
				$("#csrBillingEditCreditCard_postalCode_existingAddress").val(postalCode);
				$("#csrBillingEditCreditCard_phoneNumber_existingAddress").val(phoneNumber);
				atg.commerce.csr.order.billing.editCreditCard('${successErrorURL}');
				hidePopupWithResults( 'csrAddressDoctorFloatingPane', {result : 'cancel'});
	  		}
  		else if(suggestedAddressFromAJAX){
  				var firstName = $("#csrBillingEditCreditCard_firstName").val();
				var middleName = $("#csrBillingEditCreditCard_middleName").val();
				var lastName = $("#csrBillingEditCreditCard_lastName").val();
				var phoneNumber = $("#csrBillingEditCreditCard_phoneNumber").val();
    		    $("#csrBillingEditCreditCard_firstName_existingAddress").val(firstName);
				$("#csrBillingEditCreditCard_middleName_existingAddress").val(middleName);
				$("#csrBillingEditCreditCard_lastName_existingAddress").val(lastName);
				$("#csrBillingEditCreditCard_country_existingAddress").val(suggestedAddressFromAJAX.country);
				$("#csrBillingEditCreditCard_address1_existingAddress").val(suggestedAddressFromAJAX.address1);
				$("#csrBillingEditCreditCard_address2_existingAddress").val(address2);
				$("#csrBillingEditCreditCard_city_existingAddress").val(suggestedAddressFromAJAX.city);
				$("#csrBillingEditCreditCard_state_existingAddress").val(suggestedAddressFromAJAX.state1);
				$("#csrBillingEditCreditCard_postalCode_existingAddress").val(suggestedAddressFromAJAX.postalCode);
				$("#csrBillingEditCreditCard_phoneNumber_existingAddress").val(phoneNumber);
				atg.commerce.csr.order.billing.editCreditCard('${successErrorURL}');
				hidePopupWithResults( 'csrAddressDoctorFloatingPane', {result : 'cancel'});
				
    	}
  		else
  			{
  				atg.commerce.csr.order.billing.editCreditCard('${successErrorURL}');
  				hidePopupWithResults( 'csrAddressDoctorFloatingPane', {result : 'cancel'});
  			}
  	}
  	
    function getOrderId(){
    		if($("#error").length){
    			$("#error").remove();
    		}
    		var firstName = $("#csrBillingEditCreditCard_firstName").val();
			var middleName = $("#csrBillingEditCreditCard_middleName").val();
			var lastName = $("#csrBillingEditCreditCard_lastName").val();
			var country = getCountry();
			var address1 = $("#csrBillingEditCreditCard_address1").val();
			var address2 = $("#csrBillingEditCreditCard_address2").val();
			var city = $("#csrBillingEditCreditCard_city").val();
			var state = $("#csrBillingEditCreditCard_state").val();
			var postalCode = $("#csrBillingEditCreditCard_postalCode").val();
			var phoneNo = $("#csrBillingEditCreditCard_phoneNumber").val();
			var pageName = 'editBilling';
			 var cardType = $("#csrBillingEditCreditCard_creditCardType").val();
			 var cvv = $("#csrBillingEditCreditCard_cvv").val();
			 var cvvLength = cvv.length;
			 if((cardType != "American Express" && cvvLength < 3) || (cardType == "American Express" && cvvLength < 4) )
				{
					$('#csrBillingEditCreditCard_cvv').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid cvv</span></td>');
					return false;
				}
			var existingAddress = $("#editCreditCardPagePrompt").find("input[type='radio']:checked").val();
			if(country != 'US')
			{
				if(state == 'NA')
					{
						var state1 = 'NA'
					}
			}
		else{
				var state1 = $.trim(state.split('-')[0]);
			}
			var inputError = false;
			$("#editCreditCardPagePrompt").find(".atg_commerce_csr_paymentForm").find(".atg-csc-base-table:first").find("input[type='text'],#csrBillingEditCreditCard_expirationMonth").not(":hidden").not("#csrBillingEditCreditCard_address2").filter(function(){
				var value = $(this).val();
				if ($.trim(value) == '')
					{
						inputError = true;
						
					}
			});
			if((cardType == "RUSPrivateLabelCard" || cardType == "R Us Private Label Credit Card" ) && $("#csrBillingEditCreditCard_expirationMonth").val() == "" && $("#csrBillingEditCreditCard_expirationYear").val() == "Year")
      		 {
				inputError = false;
      		 }
			if(inputError)
			{
				return false;
			}
			if(existingAddress == 'true')
			{
				atg.commerce.csr.order.billing.editCreditCard('${successErrorURL}');
			}
			
			else if(existingAddress == 'false')
				{
					$("#csrBillingEditCreditCard_addressArea").find("input[type='text']").not(":hidden").not("#csrBillingEditCreditCard_middleName").not("#csrBillingEditCreditCard_address2").filter(function(){
						var value = $(this).val();
						if($.trim(value) == '')
							{
								inputError = true;
							}
					});
					if(inputError)
					{
						return false;
					}
					if(postalCode.length < 5)
					{
			     		
						$('#csrBillingEditCreditCard_postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
						return false;
					}
					if(phoneNo.length < 10)
					{
						$('#csrBillingEditCreditCard_phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
						return false;
					}
					
					else{
						if(country != 'US')
						{
							editCreditCard();
						}
						else
							{
						dojo.xhrPost({
								url : "${resultsUrl}?pageName="+pageName+"&address1="+address1+"&address2="+address2+"&city="+city+"&state="+state1+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&windowid="+window.windowId,
								encoding : "utf-8",
								preventCache : true,
								handle : function(_362, _363) {
									var response = dojo.trim(_362);
									dojo.query("#hiddenCreditSuggestedAddress")[0].innerHTML = response;
									var derivedStatus = dojo.query("#hiddenCreditSuggestedAddress #derivedStatus")[0].innerHTML;
									var addressRecognized = dojo.query("#hiddenCreditSuggestedAddress #addressDoctorVO1")[0].innerHTML;
									
									
									if(derivedStatus == "NO" && addressRecognized == "true"){
										var data =  dojo.query("#hiddenEditCreditSuggestedAddress #firstSuggestedAddress")[0].innerHTML
										var suggestedAddress ={};
										suggestedAddress.city = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedCity-1")[0].value;
										suggestedAddress.state = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedState-1")[0].value;
									//	suggestedAddress.country = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedCountry-1")[0].value;
									    suggestedAddress.country = 'US';
										
									    suggestedAddress.postalCode = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedPostalCode-1")[0].value;
										suggestedAddress.phoneNumber = phoneNumber;
										suggestedAddress.address1 = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedAddress1-1")[0].value;
										suggestedAddress.address2 = dojo.query("#hiddenEditCreditSuggestedAddress #suggestedAddress2-1")[0].value; 
										editCreditCard(suggestedAddress);
							        	/* $("#country").val(country);
							        	$("#address1").val(address1);
							        	$("#address2").val(address2);
							        	$("#city").val(city);
							        	$("#state").val(state);
							        	$("#postalCode").val(postalCode); */
							        	
									return false;
									}
									else if(derivedStatus == "YES"){
										atg.commerce.csr.common
										.showPopupWithReturn({
											popupPaneId : 'csrAddressDoctorFloatingPane',
											url : "${addressDoctorURL}?pageName="+pageName+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state1+"&postalCode="+postalCode+"&windowId="+window.windowId,
											onClose : function(args) {
												if (args.result == 'ok') {
													atgSubmitAction({
														panelStack : [
																'cmcShippingAddressPS',
																'globalPanels' ],
														form : document
																.getElementById('transformForm')
													});
												}
											}
										});
										return false;
									}
									else{
										//atg.commerce.csr.common.submitPopup('${successErrorURL}', document.getElementById("csrBillingEditCreditCard"), dijit.byId("editPaymentOptionFloatingPane"));
										//hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
										editCreditCard('false',0);
  										hidePopupWithResults( 'csrAddressDoctorFloatingPane', {result : 'cancel'});
										return false;
									}
										/*  */
									if (document
											.getElementById("inStorePickupResultsss")) {
										document
												.getElementById("inStorePickupResultsss").innerHTML = _362;
									}

								},
								mimetype : "text/html"
							});
							}
							
						}
					}
			
		hidePopupWithResults( 'editCreditCardPagePrompt', {result : 'cancel'});
    	
	}
    function getCountry()
    {
    	var country = $("#csrBillingEditCreditCard_country").val();
    	if(country == 'United States')
    		{
    			var country1 = 'US';
    		}
    	else
    		{
    			var country1 = country;
    		}
    	return country1;
    }
    var ${formId}DisableSubmit = function () {
        var disable = false;
        if (!dijit.byId("${formId}_creditCardType")._isvalid) disable = true;
        if (dijit.byId("${formId}_creditCardNumber") &&
          !dijit.byId("${formId}_creditCardNumber")._isvalid) disable = true;
        if (dijit.byId("${formId}_maskedCreditCardNumber") &&
          !dijit.byId("${formId}_maskedCreditCardNumber")._isvalid) disable = true;
        if (!dijit.byId("${formId}_expirationMonth")._isvalid) disable = true;
        if (!dijit.byId("${formId}_expirationYear")._isvalid) disable = true;
        if (!dijit.byId("${formId}_cvv")._isvalid) disable = true;
        return disable;
      };
      var ${formId}IsValidCardType = function () {
        if (dijit.byId("${formId}_creditCardNumber")) dijit.byId("${formId}_creditCardNumber").validate(false);
        if (dijit.byId("${formId}_maskedCreditCardNumber")) dijit.byId("${formId}_maskedCreditCardNumber").validate(false);
        ${formId}Validate();
        return this._isvalid;
      };
      var ${formId}IsValidCardNum = function () {
        this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumber(
          dijit.byId("${formId}_creditCardType"),
          dijit.byId("${formId}_creditCardNumber"));
        return this._isvalid;
      };
      var ${formId}isValidSecurityNum = function () {
     		this._isvalid = atg.commerce.csr.order.billing.isValidCVV(
          dijit.byId("${formId}_cvv"),
          dijit.byId("${formId}_creditCardType"));
      	return this._isvalid;
        };
      var ${formId}IsValidMaskedCardNum = function () {
        this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumberInEditContext({
          creditCardType : dijit.byId('${formId}_creditCardType'),
          creditCardNumber: dijit.byId('${formId}_maskedCreditCardNumber'),
          originalMaskedCreditCardNumber: '${maskedCreditCardNumberVar}'
        });
        return this._isvalid;
      };
      var ${formId}IsValidMonthCombo = function () {
        this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardMonth(
          dijit.byId("${formId}_expirationMonth"), dijit.byId("${formId}_expirationYear"));
        dijit.byId("${formId}_expirationMonth").isValid = function () { return this._isvalid; }; // prevent recursion
        dijit.byId("${formId}_expirationYear").validate(false);
        ${formId}Validate();
        dijit.byId("${formId}_expirationMonth").isValid = ${formId}IsValidMonthCombo;
        return this._isvalid;
      };
      var ${formId}IsValidYearCombo = function () {
        this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardYear(
          dijit.byId("${formId}_expirationYear"));
        dijit.byId("${formId}_expirationYear").isValid = function () { return this._isvalid; }; // prevent recursion
        dijit.byId("${formId}_expirationMonth").validate(false);
        ${formId}Validate();
        dijit.byId("${formId}_expirationYear").isValid = ${formId}IsValidYearCombo;
        return this._isvalid;
      };
  </script>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/billing/editCreditCard.jsp#1 $$Change: 875535 $--%>
