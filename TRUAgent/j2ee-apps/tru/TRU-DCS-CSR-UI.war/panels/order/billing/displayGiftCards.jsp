<%@  include file="/include/top.jspf"%>
<dsp:page>
<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<c:set var="gcCount" value="0" />
	<div class="gift-card-table">
		<table>
			<tbody>
				<dsp:droplet name="ForEach">
				<dsp:getvalueof var="paygroup" bean="ShoppingCart.current.paymentGroups"/>
				<dsp:param name="array" bean="ShoppingCart.current.paymentGroups" />
				<dsp:param name="elementName" value="paymentGroup" />
				<dsp:oparam name="output">	
						<dsp:getvalueof var="pgType" param="paymentGroup.paymentGroupClassType" />	
							<c:if test="${not empty pgType && pgType eq 'giftCard'}">
								<c:set var="gcCount" value="${gcCount+1}"/>
								<dsp:getvalueof var="gcNumber" param="paymentGroup.giftCardNumber" />
								<dsp:getvalueof var="gcAmount" param="paymentGroup.amount" />
								<dsp:getvalueof var="cardId" param="paymentGroup.id" />
								<tr id="giftCardsList">
									<td><dsp:valueof param="paymentGroup.giftCardNumber" converter="creditcard" maskcharacter="x" /></td>
									<td>$<dsp:valueof param="paymentGroup.amount" number="#.00"/> &nbsp;Applied</td>
									<td><a id="giftCardRemove" class="pull-right" data-id="${cardId}" onclick="atg.commerce.csr.order.billing.removegiftcard(this);return false;" href="javascript:void(0);">remove</a></td>
								</tr>
							</c:if>
					</dsp:oparam>
				</dsp:droplet>	
			</tbody>
		</table>
		<input type="hidden" id="gcAppliedCount" name="gcAppliedCount" value="${gcCount}" />
	</div>					
</dsp:page>