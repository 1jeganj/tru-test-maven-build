<%--
This page defines the billing panel
@version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/panels/order/billing/billing.jsp#1 $
@updated $DateTime: 2015/01/26 17:26:27 $
--%>
<%@ include file="/include/top.jspf"%>
<dsp:page xml="true">
   
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="cart"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/PaymentGroupDroplet"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CreateCreditCardFormHandler"/>
  <dsp:importbean bean="/com/tru/commerce/order/droplet/PaymentGroupTypeDroplet" />
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
  <dsp:importbean bean="/com/tru/radial/common/TRURadialConfiguration" />
  <dsp:getvalueof var="enableVerboseDebug" bean="TRURadialConfiguration.enableVerboseDebug"/>
  <dsp:getvalueof var="radialPaymentJSPath" bean="TRURadialConfiguration.radialPaymentJSPath"/>
  <dsp:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />

  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />
  <c:set var="currentOrder" value="${cart.current}"/>
  <c:set var="showInstorePaymentOption" value="true"/>
     <c:set var="pageName" value="billing"/>
    <dsp:droplet name="PaymentGroupTypeDroplet">
	<dsp:param value="${currentOrder}" name="order" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="isPayInStoreEligible" param="isPayInStoreEligible" vartype="java.lang.boolean" />
		<dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType" />
	</dsp:oparam>
</dsp:droplet>
	
	<%--	Setting showInStorePaymentOption to restrict the In-Store-Payment option for Store Pickup items --%>
	
	<c:forEach items="${currentOrder.shippingGroups}" var="shippingGroup" varStatus="shippingGroupIndex">
        <c:if test="${shippingGroup.shippingGroupClassType eq 'inStorePickupShippingGroup'}">
        	<c:set var="showInstorePaymentOption" value="false"/>
        </c:if>
    </c:forEach> 
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:importbean var="AddClaimables" bean="/atg/commerce/custsvc/ui/fragments/order/AddClaimables"/>
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  	<dsp:importbean bean="/atg/commerce/custsvc/order/purchase/BillingFormHandler"/>
		<dsp:importbean bean="/atg/svc/agent/ui/AgentUIConfiguration" />

  <dsp:getvalueof var="returnRequest" bean="/atg/commerce/custsvc/order/ShoppingCart.returnRequest"/>
  <c:set var="displayPaymentOptions" value="true"/>

  <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnActive">
  <dsp:oparam name="true">
    <c:if test="${returnRequest.returnPaymentState eq 'None'}">
      <c:set var="displayPaymentOptions" value="false"/>
    </c:if>
  </dsp:oparam>
  </dsp:droplet>

  <c:if test="${displayPaymentOptions == false}">
    <div class="atg_svc_content atg_commerce_csr_content">
    <dsp:layeredBundle basename="atg.commerce.csr.returns.WebAppResources">
      <fmt:message key="exchange.noMoneyDue"/>
    </dsp:layeredBundle>
    </div>
  </c:if>
    <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
       
        </c:url>      
         <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/billingSuggestions.jsp">
          <c:param name="addrKey" value="${addressKey}"/>
           <%--   <c:param name="pageName" value="radialChange"/> --%>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
            <c:param name="sp" value="${shippingGroup}"/>
        </c:url>     
  <div id="hiddenBillingAddressSuggestedAddress" style="display:none;"></div>
  <script type="text/javascript">
 

  if (!dijit.byId("csrBillingFloatingPane")) {
	    new dojox.Dialog({ id: "csrBillingFloatingPane",
	      cacheContent: "false",
	      executeScripts: "true",
	      scriptHasHooks: "true",
	      duration: 100,
	      "class": "atg_commerce_csr_popup"});
  }
  
<%--  
  function inStorePaymentMethod(){
		 atgSubmitAction({form : document.getElementById('inStorePayment'),panels : ['cmcBillingP'], panelStack : 'cmcBillingPS'   			
		});  
	} 
--%>
  
  function inStorePaymentMethod(pageName){
	  
	  addAddressToPayment(pageName);
	  

	} 
  
  function addAddressToPayment(pageName){
  	
  	
  	if($('#error').length)
  		{
  			$('#error').remove();
  		}
  	if($('#existingAddress').length)
  		{
  			var existingAddress = $('#existingAddress').val();
  		}
  	else
  		{
  			existingAddress = 'false';
  		}
  	
  	 if(existingAddress=='false'){
  		var emptyInputError = false;
  		var noState = true;
  		$("#csrBillingAddCreditCard_addressArea").find("input[type='text']").not(":hidden").not("#csrBillingAddCreditCard_middleName").not("#csrBillingAddCreditCard_address2").filter(function(){
  			if($.trim($(this).val()) == '')
  				{
  					emptyInputError = true;
  				}
  		});
  		if(emptyInputError)
  		{
  			return false;
  		}
  		
		 var country= $("#csrBillingAddCreditCard_country").val();
		 var address1 = $('#csrBillingAddCreditCard_address1').val();
		 var address2 = $('#csrBillingAddCreditCard_address2').val();
		 var city = $('#csrBillingAddCreditCard_city').val();
		 var state1 = $('#csrBillingAddCreditCard_state').val();
		 var postalCode = $('#csrBillingAddCreditCard_postalCode').val();
		 var phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
		 var address2 = $('#csrBillingAddCreditCard_address2').val(); 
		 var firstName = $('#csrBillingAddCreditCard_firstName').val();
		 var lastName = $('#csrBillingAddCreditCard_lastName').val();
		if(country != 'United States')
			{
				if(state1 == 'NA')
					{
						var state = 'NA'
					}
			}
		else{
			
			state1 = state1.split("-")[0];
			var state = $.trim(state);
		}
   	if(postalCode.length < 5)
		{
   		
			$('#csrBillingAddCreditCard_postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
			return false;
		}
		else if(phoneNumber.length < 10)
		{
			$('#csrBillingAddCreditCard_phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
			return false;
		}
   	if(country != 'United States')
		{
   			if(pageName == 'paymentPage')
   				{
	   				 $("#csrBillingForm").find("#firstNameRA").val(firstName);
		   	    	 $("#csrBillingForm").find("#lastNameRA").val(lastName);
		   	    	 $("#csrBillingForm").find("#address1RA").val(address1);
		   	    	 $("#csrBillingForm").find("#address2RA").val(address2);
		   	    	 $("#csrBillingForm").find("#cityRA").val(city);
		   	    	 $("#csrBillingForm").find("#stateRA").val(state);
		   	    	 $("#csrBillingForm").find("#postalCodeRA").val(postalCode);
		   	    	 $("#csrBillingForm").find("#phoneNumberRA").val(phoneNumber);
		   	    	 $("#csrBillingForm").find("#countryRA").val(country);
   					 atg.commerce.csr.order.billing.applyPaymentGroups({form:'csrBillingForm'});return false
   				}
   			else
	   			{
   					submitPayInStore();
	   			}
		}
   	else{
			dojo.xhrPost({
					url : "${resultsUrl}?address1="+address1+"&address2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&windowid="+window.windowId,
					encoding : "utf-8",
					preventCache : true,
					handle : function(_362, _363) {
						var response = dojo.trim(_362);
						dojo.query("#hiddenBillingAddressSuggestedAddress")[0].innerHTML = response;
						var derivedStatus = dojo.query("#hiddenBillingAddressSuggestedAddress #derivedStatus")[0].innerHTML;
						var addressRecognized = dojo.query("#hiddenBillingAddressSuggestedAddress #addressDoctorVO1")[0].innerHTML;
						if(derivedStatus == "NO" && addressRecognized == "true"){
							var data =  dojo.query("#hiddenBillingAddressSuggestedAddress #firstSuggestedAddress")[0].innerHTML
							//var form =  document.getElementById('csrBillingForm'); 
				        	//form.lastNameRA.value=lastName;
				        	//form.firstNameRA.value=firstName;
				        	//form.saveShippingAddress.value=$(".default_save_Shipping").val();
				        	//alert(saveShipping);
				        	//form.middleNameSG.value=middleName;
				        	/* form.cityRA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedCity-1")[0].value;
				        	form.stateRA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedState-1")[0].value;
				        	form.countryRA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedCountry-1")[0].value;
				        	form.postalCodeRA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedPostalCode-1")[0].value;
				        	form.phoneNumberRA.value = phoneNumber;
				        	form.address1RA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedAddress1-1")[0].value;
				        	form.address2RA.value = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedAddress2-1")[0].value;  */
				        	
				        	var suggestedAddress ={};
							suggestedAddress.city = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedCity-1")[0].value;
							suggestedAddress.state = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedState-1")[0].value;
							//suggestedAddress.country = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedCountry-1")[0].value;
							suggestedAddress.country = 'US';
							
							suggestedAddress.postalCode = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedPostalCode-1")[0].value;
							suggestedAddress.address1 = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedAddress1-1")[0].value;
							suggestedAddress.address2 = dojo.query("#hiddenBillingAddressSuggestedAddress #suggestedAddress2-1")[0].value; 
							inStorePaymentPopupSubmitSuggest(suggestedAddress);
								/* atgSubmitAction({
									panelStack : [
											'cmcShippingAddressPS',
											'globalPanels' ],
									form : document.getElementById('csrAddShippingAddress')
								}); */
						return false;
						}
						else if(derivedStatus == "YES"){
							atg.commerce.csr.common
							.showPopupWithReturn({
								popupPaneId : 'csrAddressDoctorFloatingPane',
								url : "${addressDoctorURL}&pageName="+pageName+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&windowId="+window.windowId,
								onClose : function(args) {
									if (args.result == 'ok') {
										atgSubmitAction({
											panelStack : [
													'cmcShippingAddressPS',
													'globalPanels' ],
											form : document
													.getElementById('transformForm')
										});
									}
								}
							});
							return false; 
						}
						else{
							inStorePaymentPopupSubmitSuggest();
							return false;
						}
						
						if (document
								.getElementById("inStorePickupResultsss")) {
							document
									.getElementById("inStorePickupResultsss").innerHTML = _362;
						}

					},
					mimetype : "text/html"
				});
   		}
  	 }
  	 else if(existingAddress == 'true') {
  		var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName,state;
  		 
  		 //var formId= document.getElementById('csrBillingAddCreditCard');
	 		 country = $('#csrBillingAddCreditCard_country_existingAddress').val();
	   		 address1 = $('#csrBillingAddCreditCard_address1_existingAddress').val();
	   		 city = $('#csrBillingAddCreditCard_city_existingAddress').val();
	   		 state = $('#csrBillingAddCreditCard_state_existingAddress').val();
	   		 postalCode = $('#csrBillingAddCreditCard_postalCode_existingAddress').val();
	   		 address2 = $('#csrBillingAddCreditCard_address2_existingAddress').val();
		 	 phoneNumber = $('#csrBillingAddCreditCard_phoneNumber_existingAddress').val();
			 firstName = $('#csrBillingAddCreditCard_firstName_existingAddress').val();
			 middleName = $('#csrBillingAddCreditCard_middleName_existingAddress').val();
			 lastName = $('#csrBillingAddCreditCard_lastName_existingAddress').val();
			 state1 = state.split("-")[0];
	     	 state = $.trim(state);
	     	if(pageName == 'paymentPage')
				{
   				 $("#csrBillingForm").find("#firstNameRA").val(firstName);
	   	    	 $("#csrBillingForm").find("#lastNameRA").val(lastName);
	   	    	 $("#csrBillingForm").find("#address1RA").val(address1);
	   	    	 $("#csrBillingForm").find("#address2RA").val(address2);
	   	    	 $("#csrBillingForm").find("#cityRA").val(city);
	   	    	 $("#csrBillingForm").find("#stateRA").val(state);
	   	    	 $("#csrBillingForm").find("#postalCodeRA").val(postalCode);
	   	    	 $("#csrBillingForm").find("#phoneNumberRA").val(phoneNumber);
	   	    	 $("#csrBillingForm").find("#countryRA").val(country);
					 atg.commerce.csr.order.billing.applyPaymentGroups({form:'csrBillingForm'});return false
				}
			else
   			{
				 $("#inStorePayment").find("#firstNameRA").val(firstName);
		    	 $("#inStorePayment").find("#lastNameRA").val(lastName);
		    	 $("#inStorePayment").find("#address1RA").val(address1);
		    	 $("#inStorePayment").find("#address2RA").val(address2);
		    	 $("#inStorePayment").find("#cityRA").val(city);
		    	 $("#inStorePayment").find("#stateRA").val(state);
		    	 $("#inStorePayment").find("#postalCodeRA").val(postalCode);
		    	 $("#inStorePayment").find("#phoneNumberRA").val(phoneNumber);
		    	 $("#inStorePayment").find("#countryRA").val(country);
		    	 submitPayInStore();
   			}
	     	 
		 }
  	 }
	
  
  
  function submitPayInStore(){
	  var form= document.getElementById('inStorePayment');
	  if($('#email').length == 1){
		  var email=dojo.byId("email").value;
		   if(email == '')
			   {
			   $('.email').find("#emailErrMsg").remove();
			   	$('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Please enter your email address</span>");
			   	return false;
			   }
		   else
			   {
				 if (!isValidEmail(email)) {
					   $('.email').find("#emailErrMsg").remove();
					   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
					   return false;
				  }
			 }
		   form.emailInstorePayment.value=email; 
		  }
			 atgSubmitAction({form : form,panels : ['cmcCompleteOrderP'], panelStack : 'cmcCompleteOrderPS'   			
			});  
	  
  }

</script>
  

  <%-- The PaymentGroupDroplet is used to initialize desire payment groups. In CSC we are
  interested in only order level pricing and not on item level pricing. The total cost for the
  order is assigned to the available payment options. Using the current CSC implementation,
  you can't perform item to payment option pricing. In order order to accomplish this, you need
  to over-ride this page --%>
  
  <dsp:droplet name="PaymentGroupDroplet">
    <dsp:param name="paymentGroupTypes" bean="CSRConfigurator.paymentGroupTypesToBeInitialized"/>
    <dsp:param name="initOrderPayment" param="init"/>
    <dsp:param name="initPaymentGroups" param="init"/>
    <dsp:param name="initBasedOnOrder" param="init"/>
    <dsp:param name="createAllPaymentInfos" value="true"/>
    <dsp:oparam name="output">
      <dsp:getvalueof param="paymentGroups" var="paymentGroups"/>
      <dsp:getvalueof param="paymentInfos" var="paymentInfos"/>
      <dsp:getvalueof param="order" var="order"/>
    </dsp:oparam>
  </dsp:droplet>
  <%-- ${paymentInfos} --%>
 
  <c:if test="${empty paymentGroups}">
    <c:set var='addNewPaymentOptionSelected' value=' selected="true"' />
  </c:if>
  
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <%-- <fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/> --%>
    <div dojoType="dijit.layout.AccordionContainer" duration="200" sizeMin="20" sizeShare="38">
      <div dojoType="dijit.layout.AccordionPane" title="<fmt:message key='newOrderBilling.addNewPaymentOption.header.title' />" <c:out value="${addNewPaymentOptionSelected}" />>
      </br><c:choose>
        <c:when test="${profile.transient}">
        <div class="inline email">
	             email address<span class="requiredStar">*</span>
	             	<input type="text" id="email" name="email" maxlength="50" value="${order.email}"/>
	         </div>
        </c:when>
        <c:otherwise>
        <c:if test="${not empty profile.email}">
        <div class="inline">
	             Email Address:<span><input type="text" id="email" name="email" maxlength="50" value="${profile.email}" readonly/>
	            </span>
	         </div>
        </c:if>
        </c:otherwise>
        </c:choose>
        </br>
        <ul class="atg_dataForm atg_commerce_csr_creditClaimForm"/>
         <div class="atg-csc-instore-payment-div">
            <c:set var="paymentGroupsCount" value="${paymentGroups == null ? 0 : fn:length(paymentGroups)}"/>
            <div id="atg_commerce_csr_addNewCreditCardPane">
            <dsp:getvalueof var="pgTypeConfigs" bean="CSRConfigurator.paymentGroupTypeConfigurations"/>
            <%-- Determine how many add fragments are in the payment group type configs.--%>
            <c:set var="addPageFragmentCount" value="${0}"/>
            <c:forEach var="pgTypeConfig" items="${pgTypeConfigs}">
              <c:if test="${addPageFragmentCount < 2 && pgTypeConfig!= null && pgTypeConfig.addPageFragment != null }">
                <c:set var="addPageFragmentCount" value="${addPageFragmentCount + 1}"/>
              </c:if>
            </c:forEach>
      
            <%-- If there is only one addPageFragment, do not display the tab. --%>
            <c:if test="${addPageFragmentCount == 1}">
              <c:forEach var="pgTypeConfig" items="${pgTypeConfigs}">
                <c:if test="${pgTypeConfig!= null && pgTypeConfig.addPageFragment != null }">
                  <dsp:include src="${pgTypeConfig.addPageFragment.URL}"
                               otherContext="${CSRConfigurator.truContextRoot}"/>
                </c:if>
              </c:forEach>
            </c:if>

            <%-- If there is more than one addPageFragments, then display the tab. --%>
            <c:if test="${addPageFragmentCount > 1}">
              <div id="paymentOptionAddContainer" dojoType="dijit.layout.TabContainer"
                   doLayout="false">
      
                <%---The dijit.layout.Contentpane does not support scripts execution. Because of that
                we are using dojox.layout.ContentPane. This will help us to validate the
                form fields
                --%>
                <c:forEach var="pgTypeConfig" items="${pgTypeConfigs}">
                  <c:if test="${pgTypeConfig!= null && pgTypeConfig.addPageFragment != null }">
                   <dsp:layeredBundle basename="${pgTypeConfig.resourceBundle}">
                    <fmt:message var="addPageFragmentTitle" key="${pgTypeConfig.addPageFragmentTitleKey}"/>
                   </dsp:layeredBundle>
                    <div id="${pgTypeConfig.type}" dojoType="dojox.layout.ContentPane" executeScripts="true"
                         scriptHasHooks="true" title="${addPageFragmentTitle}">
                      <dsp:include src="${pgTypeConfig.addPageFragment.URL}"
                                   otherContext="${pgTypeConfig.addPageFragment.servletContext}">
                      </dsp:include>
                    </div>
                  </c:if>
                </c:forEach>
              </div>
              
            </c:if>

          </div>
          </div>
      </div>
      
   
    <%--   <div dojoType="dijit.layout.AccordionPane" title="<fmt:message key='newOrderBilling.availablePaymentOptions.title' />" selected="true">
 		  --%>
        <c:if test="${displayPaymentOptions == true}">
        <div class="atg_svc_content atg_commerce_csr_content">
       <!-- Page for pay in store start -->
     <%--   <dsp:include src="/include/order/radialBillingAddress.jsp" otherContext="${CSRConfigurator.truContextRoot}"/> --%>
		 <dsp:form method="post" id="inStorePayment" name="inStorePayment">
		  <dsp:input type="hidden" bean="BillingFormHandler.email" id="emailInstorePayment" name="emailInstorePayment"/>

		   
		   	<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.firstName" name="firstNameRA" id="firstNameRA" />
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.lastName" name="lastNameRA" id="lastNameRA" />
	 	<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.address1" name="address1RA" id="address1RA" />
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.address2" name="address2RA" id="address2RA" />
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.city" name="cityRA" id="cityRA"/>
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.state" name="stateRA" id="stateRA"/>
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.postalCode" name="postalCodeRA" id="postalCodeRA"/>
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.phoneNumber" name="phoneNumberRA" id="phoneNumberRA"/>
		
		<dsp:input type="hidden" bean="BillingFormHandler.addressInputFields.country" name="countryRA" id="countryRA"/>
        		   <dsp:input type="hidden" value="submit" bean="BillingFormHandler.ensurePaymentGroup" />
		 
		   <c:if test="${true}">
		  <c:set var="formId" value="csrBillingAddCreditCard"/>
		              <dsp:getvalueof var="addresses" value="${address}"></dsp:getvalueof>
		              <dsp:input bean="BillingFormHandler.addressInputFields.firstName" type="hidden" id="${formId}_firstName_existingAddress" value="${firstAddress.firstName}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.middleName" type="hidden" id="${formId}_middleName_existingAddress" value="${firstAddress.middleName}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.lastName" type="hidden" id="${formId}_lastName_existingAddress" value="${firstAddress.lastName}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.country" type="hidden" id="${formId}_country_existingAddress" value="${firstAddress.country}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.address1" type="hidden" id="${formId}_address1_existingAddress" value="${firstAddress.address1}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.address2" type="hidden" id="${formId}_address2_existingAddress" value="${firstAddress.address2}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.city" type="hidden" id="${formId}_city_existingAddress" value="${firstAddress.city}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.state" type="hidden" id="${formId}_state_existingAddress" value="${firstAddress.state}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.postalCode" type="hidden" id="${formId}_postalCode_existingAddress" value="${firstAddress.postalCode}"></dsp:input>
		              <dsp:input bean="BillingFormHandler.addressInputFields.phoneNumber" type="hidden" id="${formId}_phoneNumber_existingAddress" value="${firstAddress.phoneNumber}"></dsp:input>
		         </c:if> 
		 </dsp:form>
							<fmt:message key="billing.payinstore.label" bundle="${TRUCustomResources}"/>
							<c:choose>
								<c:when test="${isPayInStoreEligible}">
								<c:choose>
								<c:when test="${selectedPaymentGroupType eq 'inStorePayment'}"> 
					             <input type="button" value="submit" onclick= "javascript:inStorePaymentMethod('radialChange');" name="inStorePaymentButton" />
								</c:when>
								<c:otherwise>
								<input type="button" value="submit" onclick= "javascript:inStorePaymentMethod('radialChange');"
											name="inStorePaymentButton"/>
								</c:otherwise>
								</c:choose>
								</c:when>
								<c:otherwise>
								 <input type="button" value="submit" onclick= "javascript:inStorePaymentMethod('radialChange');" disabled
											name="inStorePaymentButton" />
		                         <a href="JavaScript:payInStoreDetailWindow('http://www.toysrus.com/helpdesk/index.jsp?display=payment&amp;index=0&amp;subdisplay=options#pin');">
       		                     <span><fmt:message key="checkout.payment.details" bundle="${TRUCustomResources}" /></span>
                                 </a><span>
         						 <fmt:message	key="checkout.payment.payInStore.disabledMessage" bundle="${TRUCustomResources}"/></span>
								</c:otherwise>
							</c:choose>
				
      <!-- Page for pay in store end -->
        <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
      
        <%-- We do not want to include the claimables in the tab and we want to provide more visibility for the claimables.
        Because of that it is not included in the tab and it is considered as separate item. --%>
        <dsp:include src="${AddClaimables.URL}" otherContext="${CSRConfigurator.truContextRoot}">
         <dsp:param name="order" value="${currentOrder}"/>
         <dsp:param name="selectedPaymentGroupType" value="${selectedPaymentGroupType}"/>
         </dsp:include>
       </dsp:layeredBundle>
       </c:if>
      
        <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
          <dsp:include src="/panels/order/billing/choosePaymentOptions.jsp" otherContext="${CSRConfigurator.truContextRoot}">
            <dsp:param name="displayPaymentOptions" value="${displayPaymentOptions}"/>
            <dsp:param name="paymentGroups" value="${paymentGroups}"/>
           <dsp:param name="order" value="${currentOrder}"/>
          </dsp:include>
        </dsp:layeredBundle>
        <!-- </div> -->
             <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
                value="/panels/order/shipping/includes/billingSuggestions.jsp">
          <c:param name="addrKey" value="${addressKey}"/>
           <c:param name="paymentGroups" value="${paymentGroups}"/>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
        </c:url>
        
        <script type="text/javascript" charset="UTF-8"	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script type="text/javascript" charset="UTF-8"	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript">
          atg.progress.update('cmcBillingPS');
          _container_.onUnloadDeferred.addCallback(function () {
            atgHideLoadingIcon();
          });          
 atg.commerce.csr.order.billing.applyPaymentGroups = function (pParams){
	  var form  = document.getElementById("csrBillingForm");
	   var rewardNumber=dojo.byId("enterMembershipIDInBilling").value;
	   if($('#email').length == 1){
	   var email=dojo.byId("email").value;
	   if(email == '')
		   {
		   $('.email').find("#emailErrMsg").remove();
		   	$('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Please enter your email address</span>");
		   	return false;
		   }
	   else
		   {
			   if (!isValidEmail(email)) {
				   $('.email').find("#emailErrMsg").remove();
				   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
				   return false;
			  }
		 }
	   form.emailInBilling.value=email;
	   }
	  form.rewardNumberCheckOut.value=rewardNumber; 
	  if (container && container.availablePaymentMethods) {
	    for (id in container.availablePaymentMethods) {
	      var paymentMethod = container.availablePaymentMethods[id];
	      if (paymentMethod.paymentGroupType == 'inStorePayment') {
	        var paymentInput = dijit.byId(paymentMethod.paymentGroupId);
	        var paymentCheckbox = dojo.byId(paymentMethod.paymentGroupId + "_checkbox");
	        var paymentRelationshipType = dojo.byId(paymentMethod.paymentGroupId + "_relationshipType");
	        if (paymentInput && paymentCheckbox && !paymentCheckbox.checked) {
	          paymentInput.setValue(0);
	        }
	        if (paymentInput && paymentRelationshipType && paymentCheckbox && !paymentCheckbox.checked) {
	          paymentRelationshipType.value = 'ORDERAMOUNT';
	        }
	      }
	    }
	  }
	  
	  atg.commerce.csr.common.enableDisable([{form: "csrBillingForm", name: "csrHandleApplyPaymentGroups"}],
	      [{form: "csrBillingForm", name: "csrPaymentGroupsPreserveUserInputOnServerSide"}]);
	  atg.commerce.csr.common.prepareFormToPersistOrder (pParams);
	  atgSubmitAction({form:form});
	}; 
	
	function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	}

	$(document).on('blur','#email',function(){
		$value = $(this).val();
		if (!isValidEmail($value) && $value!="") {
			   $('.email').find("#emailErrMsg").remove();
			   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
			   return false;
		  }
		else if($('.email').find("#emailErrMsg").length>0){
			$('.email').find("#emailErrMsg").remove();
		}
	});
	$(document).on('keyup','#email',function(){
		if ($(this).val()=="" && $('.email').find("#emailErrMsg").length>0) {
			$('.email').find("#emailErrMsg").remove();
		}
	})
	/**
	 *
	 * This method gets current values from the credit card type and credit card number widgets and
	 * using dojo.validate.isValidCreditCard() to validate the credit card.
	 *
	 */
	 
	 function payInStoreDetailWindow(url) {
			popupWindow = window.open(
			url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
		}
	
	atg.commerce.csr.order.billing.isValidCreditCardNumber = function (pCardTypeWidget, pCardNumberWidget) {
		
	  if (!pCardTypeWidget) {
	    return false;
	  }

	  if (!pCardNumberWidget) {
	    return false;
	  }
	  var ccType = pCardTypeWidget.getValue();
	  var ccNumber = pCardNumberWidget.getValue();

	  if (ccType === '') {
	    dojo.debug("Supplied credit card type is not valid.");
	    pCardTypeWidget.promptMessage="Please select a valid credit card type.";
	    return false;
	  }
	  if (ccNumber === '') {
	    dojo.debug("Supplied card number is not valid.");
	    pCardNumberWidget.invalidMessage=getResource('csc.billing.invalidCreditCardNumber');
	    return false;
	  }
	  var cardTypeData = creditCardTypeDataContainer.getCreditCardTypeDataByKey(ccType);
	  if (!cardTypeData) {
	    dojo.debug("Supplied credit card type is not valid.");
	    pCardTypeWidget.invalidMessage=getResource('csc.billing.invalidCreditCardType');
	    return false;
	  }

	  var code = cardTypeData.code;
	  if (!cardTypeData) {
	    dojo.debug("Supplied credit card type is not valid."+cardTypeData);
	    pCardTypeWidget.invalidMessage=getResource('csc.billing.invalidCreditCardType');
	    return false;
	  }
	  if(code == 'RUSCoBrandedMasterCard' || code == 'RUSPrivateLabelCard'){
		 if(isTRUCardValid(ccNumber, code)){
			 return true;
		 }else{
			 return false;
		 }
	  }else{ 
		 if(dojox.validate.isValidCreditCard(ccNumber, code)) {
		    dojo.debug("This is a valid credit card number.");
		    return true;
		  } else {
		    dojo.debug("Please provide a valid credit card number.");
		    pCardNumberWidget.invalidMessage=getResource('csc.billing.invalidCreditCardNumber');
		    return false;
		  }
 	   }
	};
	
	// function to validate the TRU cards 
	function isTRUCardValid(pCCNumber, pCode){
		 var value = pCCNumber;
		 var v = parseInt(value.substr(0, 6));
		 var code = pCode.toLowerCase();
		 if (code == 'rusprivatelabelcard' && v == 604586 && (value.length == 16)) { /*PLCC card*/
             return true;
         } else if (code == 'ruscobrandedmastercard' && v == 524363 && (value.length == 16)) { /*USCOBMasterCard card*/
             return true;
         } else if (code == 'ruscobrandedmastercard' && v == 523770 && (value.length == 16)) { /*PRCOBMasterCard card*/
             return true;
         } 
	}

	
	 if (!dijit.byId("csrEditAddressFloatingPane")) {
		new dojox.Dialog({
			id : "csrEditAddressFloatingPane",
			cacheContent : "false",
			executeScripts : "true",
			scriptHasHooks : "true",
			duration : 100,
			"class" : "atg_commerce_csr_popup"
		});
	}
	 
	 if (!dijit.byId("csrAddressDoctorFloatingPane")) {
			new dojox.Dialog({
				id : "csrAddressDoctorFloatingPane",
				cacheContent : "false",
				executeScripts : "true",
				scriptHasHooks : "true",
				duration : 100,
				"class" : "atg_commerce_csr_popup"
			});
		}
	
	 function inStorePaymentPopupSubmitSuggest(suggestedAddressFromBilling){
			var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
			//var addressFlag = $("input[name=valid-address][type=radio]:checked").val()?suggestedAddressFromBilling;
		   if(suggestedAddressFromBilling){
		  		 country= suggestedAddressFromBilling.country;
				 address1 = suggestedAddressFromBilling.address1;
				 city = suggestedAddressFromBilling.city;
				 state1 = suggestedAddressFromBilling.state;
				 postalCode = suggestedAddressFromBilling.postalCode;
				 //address2 = suggestedAddressFromBilling.address2;
				 phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
				 firstName = $('#csrBillingAddCreditCard_firstName').val();
				 middleName = $('#csrBillingAddCreditCard_middleName').val();
				 lastName = $('#csrBillingAddCreditCard_lastName').val();
		  	}
		  	else{
			
			     var firstName = $('#csrBillingAddCreditCard_firstName').val();
				 var middleName = $('#csrBillingAddCreditCard_middleName').val();
				 var lastName = $('#csrBillingAddCreditCard_lastName').val();
				 var country=dijit.byId("csrBillingAddCreditCard_country");
				 var address1 = $('#csrBillingAddCreditCard_address1').val();
				 var address2 = $('#csrBillingAddCreditCard_address2').val();
				 var city = $('#csrBillingAddCreditCard_city').val();
				 var state1 = $('#csrBillingAddCreditCard_state').val();
				 var postalCode = $('#csrBillingAddCreditCard_postalCode').val();
				 var phoneNumber = $('#csrBillingAddCreditCard_phoneNumber').val();
				 var address2 = $('#csrBillingAddCreditCard_address2').val();
				 
		  	}
				if(country != 'US')
				{
					if(state1 == 'NA')
						{
							var state = 'NA'
						}
				}
				else{
					state1 = state1.split("-")[0];
					var state = $.trim(state1);
				}
			  
			 var form= document.getElementById('inStorePayment');
		 	 $("#inStorePayment").find("#firstNameRA").val(firstName);
			 $("#inStorePayment").find("#lastNameRA").val(lastName);
			 $("#inStorePayment").find("#address1RA").val(address1);
			 $("#inStorePayment").find("#address2RA").val(address2);
			 $("#inStorePayment").find("#cityRA").val(city);
			 $("#inStorePayment").find("#stateRA").val(state);
			 $("#inStorePayment").find("#postalCodeRA").val(postalCode);
			 $("#inStorePayment").find("#phoneNumberRA").val(phoneNumber);
			 $("#inStorePayment").find("#countryRA").val(country);
			  
			 
			  if($('#email').length == 1){
			  var email=dojo.byId("email").value;
			   if(email == '')
				   {
				   $('.email').find("#emailErrMsg").remove();
				   	$('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Please enter your email address</span>");
				   	return false;
				   }
			   else
				   {
					 if (!isValidEmail(email)) {
						   $('.email').find("#emailErrMsg").remove();
						   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
						   return false;
					  }
				 }
			   form.emailInstorePayment.value=email; 
			  }
				 atgSubmitAction({form : form,panels : ['cmcCompleteOrderP'], panelStack : 'cmcCompleteOrderPS'   			
				});  
				 hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
			} 
		  (function(){
			   var head = document.getElementsByTagName('head')[0];
			   var radialScript = document.createElement('script');
			   radialScript.type= 'text/javascript';
			   radialScript.src= '${radialPaymentJSPath}';
			   head.appendChild(radialScript);
		  })();
        </script>

      </div>
      
    </div>
  </dsp:layeredBundle>


 	    <dsp:form id="billingForm" formid="billingForm">
<%--     <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCard.creditCardNumber" name="CardinalToken" id="CardinalToken" /> --%>
	    <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.nameOnCard" name="nameOnCard" id="nameOnCard" />
	    <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.creditCardToken" name="CardinalToken" id="CardinalToken" />
	    <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.creditCardNumber" name="accountNumber" id="accountNumber" />
	    <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.cardLength" name="cardLength" id="cardLength" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.expirationMonth" name="expirationMonth" id="expirationMonth" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.expirationYear" name="expirationYear" id="expirationYear" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardInfoMap.creditCardVerificationNumber" name="cardCode" id="cardCode" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.cardType" name="cardType" id="cardType" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.firstName" name="firstName" id="firstName" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.lastName" name="lastName" id="lastName" />
	 	<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.address1" name="address1" id="address1" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.address2" name="address2" id="address2" />
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.city" name="city" id="city"/>
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.state" name="state" id="state"/>
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.postalCode" name="postalCode" id="postalCode"/>
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.phoneNumber" name="phoneNumber" id="phoneNumber"/>
		<dsp:input type="hidden" bean="CreateCreditCardFormHandler.addressInputFields.country" name="country" id="country"/>
        <dsp:input type="hidden" bean="CreateCreditCardFormHandler.copyAddress" name="copyAddress" id="copyAddress" />
        <dsp:input type="hidden" bean="CreateCreditCardFormHandler.billingAddressNickName" name="billingAddressNickName" id="billingAddressNickName" />
        <dsp:input type="hidden" bean="CreateCreditCardFormHandler.creditCardEmail" name="creditCardEmail" id="creditCardEmail" /> 
        
         <dsp:input type="hidden" bean="CreateCreditCardFormHandler.newBillingAddress" name="newBillingAddress" id="newBillingAddress" value="true"/>
       
        <dsp:input type="hidden" bean="CreateCreditCardFormHandler.newCreditCard" value="submit" /> 
       
     </dsp:form> 

</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/panels/order/billing/billing.jsp#1 $$Change: 946917 $--%>