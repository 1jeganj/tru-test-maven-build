<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean bean="/com/tru/commerce/order/purchase/GiftCardFormHandler"/>
<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
<dsp:getvalueof var="order" param="order"/>
<dsp:getvalueof var="selectedPaymentGroupType" param="selectedPaymentGroupType"/>
<dsp:importbean bean="/com/tru/droplet/TenderBasedCheckDroplet" />	
<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
</style>
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
<dsp:droplet name="TenderBasedCheckDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		<dsp:getvalueof var="tenderBasedPromoExists" param="showGiftCardFlag"/>
		</dsp:oparam>
		</dsp:droplet>
<!-- Gift Card Start -->
<c:choose>
 <c:when test="${selectedPaymentGroupType ne 'inStorePayment'  and !tenderBasedPromoExists}"> 
		<ul class="atg_dataForm atg_commerce_csr_creditClaimForm">
			<li class="atg_svc_billingClaimCode">
				<span align="left">
					<label> <fmt:message key="billing.giftcard.label" bundle="${TRUCustomResources}"/></label>
				</span>
			</li>  <fmt:message key="billing.giftcard.message" bundle="${TRUCustomResources}"/>
			<br><br>
			<div class="inline">
				<p>
					<fmt:message key="billing.giftcard.cardnumber.label" bundle="${TRUCustomResources}"/>
				</p>
				<input type="text" id="gcNumber" name="gcNumber" class="chkNumbersOnly" value="" onkeypress="return isNumberOnly(event);"/> 
			</div>
			<div class="availableOptionsForm">
				<p>
				   <fmt:message key="billing.giftcard.pinnumber.label" bundle="${TRUCustomResources}" />
				</p>
				 <input type="password" id="gcpin" name="ean" class="chkNumbersOnly" value=""/>	 
				 <button type="button" id="co-payment-gift-card" 
						onclick="atg.commerce.csr.order.billing.giftcardpayment();return false;"
      								dojoType="atg.widget.validation.SubmitButton" value="Apply"> </button>
      								
      
      				 <button type="button" id="co-payment-gift-card-check" 
						onclick="giftCardBalCheckPopup();return false;"
							dojoType="atg.widget.validation.SubmitButton" value="Check Balance"> </button>
			</div>
	</ul>
	</c:when>
	<c:when test="${selectedPaymentGroupType eq 'inStorePayment'   or tenderBasedPromoExists}"> 
		<ul class="atg_dataForm atg_commerce_csr_creditClaimForm">
			<li class="atg_svc_billingClaimCode">
				<span align="left">
					<label> <fmt:message key="billing.giftcard.label" bundle="${TRUCustomResources}"/></label>
				</span>
			</li>  <fmt:message key="billing.giftcard.message" bundle="${TRUCustomResources}"/>
			<br><br>
			<div class="inline">
				<p>
					<fmt:message key="billing.giftcard.cardnumber.label" bundle="${TRUCustomResources}"/>
				</p>
				<input type="text" id="gcNumber" name="gcNumber" class="chkNumbersOnly" value="" disabled onkeypress="return isNumberOnly(event);"/> 
			</div>
			<div class="availableOptionsForm">
				<p>
				   <fmt:message key="billing.giftcard.pinnumber.label" bundle="${TRUCustomResources}" />
				</p>
				 <input type="password" id="gcpin" name="ean" class="chkNumbersOnly" value="" disabled/>
				 	 
				 <input type="button" id="co-payment-gift-card" 
						onclick="atg.commerce.csr.order.billing.giftcardpayment();return false;"
      								 value="Apply" disabled>
      			<input type="button" id="co-payment-gift-card-check" onclick="giftCardBalCheckPopup();return false;" value="Check Balance" disabled/>
			</div>
	</ul>
	</c:when>
</c:choose>
<div id="giftCardsAppliedDiv">
	</div>
      <!-- Gift Card End -->
      <%-- <div class="priceSummary">
			<dsp:include page="/checkout/common/priceSummary.jsp" />
	 </div> --%>
      <div id="giftCardList" class="giftCardList">
		<dsp:include src="/panels/order/billing/displayGiftCards.jsp" otherContext="${CSRConfigurator.truContextRoot}"/>
	  </div>
	</dsp:layeredBundle>
				
		<script>
		if (!dijit.byId("atg_commerce_csr_catalog_checkBalancePopup")) {

		    new dojox.Dialog({ id:"atg_commerce_csr_catalog_checkBalancePopup",

		                       cacheContent:"false",

		                       executeScripts:"true",

		                       scriptHasHooks:"true",

		                       duration: 100,

		                       "class":"atg_commerce_csr_popup"});
			
		  }
		function isNumberOnly(evt){
	    	evt = (evt) ? evt : window.event;
	    	var charCode = (evt.which) ? evt.which : evt.keyCode;
	    	 if (navigator.userAgent.indexOf("Firefox") > 0) {
	    	    	if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
	    	    		return true;
	    	        	}
	    	    		return false;
	    	    	}
	    	    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
	    	    	 return true;
	    	    }
	    	   
	    	    return false;
	    }
				function giftCardBalCheckPopup() {
					atg.commerce.csr.common.showPopupWithReturn({

                        popupPaneId: 'atg_commerce_csr_catalog_checkBalancePopup',
                        title: 'Check Gift Card balance',
                        url: "/TRU-DCS-CSR/panels/order/billing/giftCardCheckBal.jsp?",

                        onClose: function(args) {
                        }});
				}
				function giftCardBalCheck() {
						 var gcNum = $('#gccNumber').val();
						 var gcPin = $('#gccpin').val();
						 if($.trim(gcNum) == '' || $.trim(gcNum) == '')
							 {
							 	return false;
							 }
						 $("#giftCardCheckNum").val(gcNum);
						 $("#giftCardCheckPin").val(gcPin);
						 var form = document.getElementById('giftCardCheck');
						 dojo.xhrPost({form:form,url:"/TRU-DCS-CSR/panels/order/billing/ajaxIntermediateGiftCardCheckBalResponse.jsp?gcNum=" + gcNum + "&gcPin=" + gcPin,content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
			     			 if(!(_1f instanceof Error)){
			     				 $('.giftCardbalanceCheck').hide();
				     			document.getElementById("giftCardBalance").innerHTML = _1f;
				     			$('#giftCardBalance').show();
				        	 }
				          },mimetype:"text/html"}); 
					/* atgSubmitAction({form : document.getElementById('giftCardCheck')}) */
					}
				function clearPrePopulatedDataCSC() {
					if ($("#gcNumber,#gcpin").length > 0) {
						$("#gcNumber,#gcpin").val("");
					}
				}

				$(document).on("click",".checkAnotherCard",function(){
					$('.giftCardbalanceCheck').show();
					$("#giftCardBalance").hide();
				});
				
				$(document).ready(function(){
					setTimeout(function(){
						$('#gcpin').val('');
						$('#gcNumber').val('');
					},500);
					
					
					setTimeout(function () {
						clearPrePopulatedDataCSC();
					}, 1000); 
					
				});
				</script>
</dsp:page>