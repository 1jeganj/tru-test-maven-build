<%@ include file="/include/top.jspf"%>
<dsp:page>
	<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
		<dsp:importbean
			bean="/atg/commerce/custsvc/promotion/CouponFormHandler" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
		<dsp:getvalueof var="order" param="order" />
		<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />
		<!-- Promo coupon Start -->
		<style type="text/css">
			#termOfUSeDetails_content {
			position: absolute;
		    background: #fff;
		    width: 250px;
		    padding: 10px;
		    border: 1px solid rgba(0,0,0,0.2);
		    margin-top: 9px;
		    display: none;
		    left: 13px;
		    border-radius: 5px;
		    }
		    #termOfUSeDetails_content .arrow
		    {
		    position: absolute;
		    top: -12px;
		    left: 14px;
		    }
		</style>
		
		<ul class="atg_dataForm atg_commerce_csr_creditClaimForm">
			<li class="atg_svc_billingClaimCode"><span
				class="atg_commerce_csr_fieldTitle"> <label> <fmt:message
							key="billing.Coupen.label" bundle="${TRUCustomResources}" />
				</label> <br>
			</span> <fmt:message key="billing.Coupen.codenumber.label"
					bundle="${TRUCustomResources}" />
					 <input type="text" id="coupon_Claim_Code" name="coupon_Claim_Code" onkeyup="convertToUpperCase()"/>
				</div>
				<div>
					<button type="button" 
						onclick="atg.commerce.csr.order.billing.saveUserInputAndClaimClaimables();return false;"
						dojoType="atg.widget.validation.SubmitButton" value="Claim">
				</div> <br>
		</ul>
		<ul>
		</ul>
		<a class="termOfUSeDetails" href="javascript:void(0);" data-original-title="" title=""><fmt:message key="checkout.payment.details" bundle="${TRUCustomResources}" /></a>
		<div id="termOfUSeDetails_content">
		<img src="/TRU-DCS-CSR/images/up-arrow.png" class="arrow"/>
			<div class="saved-address-header">Promotional Codes</div>
			<p>Promotional codes need to be entered one at a time. Adding more than one promotional code can void promotions that have already been applied. Any promotional discounts will be reflected in the Order Details section on this page. You will need to re-enter your credit card information and Rewards"R"Us number after clicking the "Apply" button</p>
		</div>		
		<dsp:include page="/panels/order/billing/promoCodeSummary.jsp">
		 <dsp:param name="order" value="${order}"/>
		</dsp:include>
			<!-- Promo coupon End -->
		<%-- <dsp:include src="/panels/order/billing/displayGiftCards.jsp" otherContext="${CSRConfigurator.truContextRoot}"/> --%>
	</dsp:layeredBundle>
	<script type="text/javascript">
		function convertToUpperCase()
		{
				var value = document.getElementById("coupon_Claim_Code").value;
				document.getElementById("coupon_Claim_Code").value = value.toUpperCase();
		}
		$(document).on("mouseover",".termOfUSeDetails",function(){
			$("#termOfUSeDetails_content").fadeIn();
		})
		$(document).on("mouseout",".termOfUSeDetails",function(){
			$("#termOfUSeDetails_content").fadeOut();
		})
	</script>

</dsp:page>