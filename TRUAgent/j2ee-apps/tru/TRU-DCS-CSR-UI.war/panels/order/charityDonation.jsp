<%@ include file="/include/top.jspf"%>
<dsp:page>
    <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
    <dsp:importbean var="siteURL" bean="/atg/commerce/custsvc/util/CSRAgentConfiguration"/>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/com/tru/commerce/cart/droplet/DonationSKULookupDroplet"/>
	<dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" />
	
	<dsp:droplet name="DonationSKULookupDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var="donationSKUItem" param="donationSKUItem"/>
			<dsp:getvalueof var="donationProductItem" param="donationProductItem"/>
		</dsp:oparam>
	</dsp:droplet>
	
<style typr="text/css">
	.donationProduct .toys-for-tots .donate-input-group .input-dollar-sign
	{
		width: auto;
	    float: left;
	    position: relative;
	    margin-top: 9px;
	    left: 17px;
	    font-size: 24px;
	    color: #666;
	}
	.donationProduct .toys-for-tots .donate-input-group .donation-text
	{
		padding-left: 16px;
	    width: 130px;
	    height: 30px;
	    font-size: 24px;
	    float: left;
	}
	.donationProduct .toys-for-tots
	{
		margin: 10px;
 	    float: left;
	}
	.donationProduct .toys-for-tots-logo,
	.donationProduct .donate-input-group
	{
		float: left;
	} 
	.donationProduct .toys-for-tots-donate-text
	{
		float: left;
	    font-size: 28px;
	    margin-left: 10px;
	    margin-top: 9px;
	    color: red;
	}
	.donationProduct .toys-for-tots-subheader
	{
		float: left;
	    width: 100%;
	    margin-top: 10px;
	    margin-bottom: 10px;
	}
	.donationProduct .toys-for-tots-enter-amount
	{
		float: left;
	    display: block;
	    margin-right: 45px;
	    margin-top: 10px;
	}
	.donationProduct .donate-button
	{
		font-size: 19px;
	    color: #fff;
	    background-color: #009ddb;
	    border: 3px solid #009ddb;
	    margin-left: 15px;
	    height: 34px;
	    width: 104px;
	}
</style>
	<div class="donationProduct" style="float: left;width:100%;background-color: #f3f3f3;">
	<input id="donationProductId" type="hidden" value="${donationProductItem.id}" />
	<input id="donationSKUId" type="hidden" value="${donationSKUItem.id}" />
	<input id="domainUrl" type="hidden" value="${siteURL.serverName}"/>
	<svc-ui:frameworkUrl var="cartURL" panelStacks="cmcShoppingCartPS" tab="commerceTab"/>

	<dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" var="cartModifierFormHandler"/>
	
	<dsp:form id="addDonationItemToCart">
	<c:set var="donationProductId" value="${donationProductItem.id}" />
			 <dsp:input type="hidden" bean="CartModifierFormHandler.productId" value="${donationProductId}" />
			<c:set var="catalogRefIds" value="${donationSKUItem.id}" />
			<dsp:input type="hidden" bean="CartModifierFormHandler.catalogRefIds" value="${catalogRefIds}"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.quantity" value="1"/>
		 	<dsp:input type="hidden" bean="CartModifierFormHandler.donationAmount" id="donationAmount" name="donationAmount"/> 
			<dsp:input type="hidden" bean="CartModifierFormHandler.donationItem" value="true" />
			<dsp:input type="hidden" bean="CartModifierFormHandler.addDonationItemToOrder" value="submit" />
	  		<dsp:input type="hidden" name="successURL" value="${cartURL}"  bean="CartModifierFormHandler.addItemToOrderSuccessURL"/>
  		<dsp:input type="hidden" name="errorURL" value="${cartURL}"  bean="CartModifierFormHandler.addItemToOrderErrorURL"/>

	</dsp:form>
	
	
	
	<dsp:getvalueof value="${originatingRequest.contextPath}"
		var="contextPath" />
	<div class="donation-error-message" style="display: none;margin: 6px 0px;color: red;font-weight: bold;">
		<div class="shopping-cart-error-state">
		<div class="shopping-cart-error-state-header row">
			<div class="error-state-exclamation-lg img-responsive col-md-2"></div>
			<div class="shopping-cart-error-state-header-text col-md-10">
				<fmt:message key="shoppingcart.oops.issue" />
				<div class="shopping-cart-error-state-subheader">A minimum of $1.00 is required to make a donation
				</div>
			</div>
			</div>
		</div>
	</div>
	<dsp:droplet name="/atg/targeting/TargetingForEach">
		<dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/CharityDonationContentTargeter"/>
		<dsp:oparam name="output">
			<dsp:valueof param="element.data" valueishtml="true" />
		</dsp:oparam>
	</dsp:droplet>
	
	</div>
<script type="text/javascript">
	$(document).ready(function(){
			var finalImagePath,finalDomainUrl;
			var imagePath = $(".donationProduct").find(".toys-for-tots-logo img").attr("src");
			var domainUrl = $("#domainUrl").val();
			if(domainUrl.indexOf("http://") >= 0)
				{
					domainUrl = domainUrl.split("http://")[1];
					finalDomainUrl = "https://"+domainUrl;
				}
			else if(domainUrl.indexOf("https://") >= 0)
				{
					finalDomainUrl = domainUrl;
				}
			else if(domainUrl.indexOf("http://") < 0)
				{
					finalDomainUrl = "https://"+domainUrl;
				}
			if(imagePath.indexOf("../") >= 0)
				{
					imagePath = imagePath.split("../")[1];
					finalImagePath = finalDomainUrl+'/'+imagePath;
				}
			else if(imagePath.indexOf("http") < 0 && imagePath.indexOf(".com") < 0)
				{
					finalImagePath = finalDomainUrl+imagePath;
				}
			else if(imagePath.indexOf("https:") >= 0 && imagePath.indexOf(".com") >= 0)
				{
					finalImagePath = imagePath;
				}
			else if(imagePath.indexOf("http:") >= 0 && imagePath.indexOf(".com") >= 0)
				{
					imagePath = imagePath.split(".com/")[1];
					finalImagePath = finalDomainUrl+"/"+imagePath;
				}
			$(".donationProduct").find(".toys-for-tots-logo img").attr("src",finalImagePath);
	});
	$(".donate-button").on("click",function(event){
		event.preventDefault();
		event.stopPropagation();
		var donationAmount = $(".donation-text").val();
		if($('.error').length)
			{
				$('.error').remove();
			}
		if(isNaN(parseFloat(donationAmount))|| donationAmount < 1)
			{
				$(".toys-for-tots").prepend("<p class='error' style='color: red;line-height: 0;margin-top: 5px;margin-bottom: 20px;font-size: 13px;margin-left: 0;'>Donation amount must be at least $1.00. Please re-enter the amount you want to donate.</p>")
				return false;
			}
		var form = document.getElementById('addDonationItemToCart');
		var enteredAmount = $('.donation-text').val();
		form.donationAmount.value=enteredAmount;
		atgSubmitAction({
			form : form
			});
	}); 
		
function validateDonationAmount(evt) {
	$('.donation-text').attr('maxlength','7');
	var enteredAmount = $('.donation-text:last').val();
	var afterDot = enteredAmount.split('.')[1];
	var beforeDot = enteredAmount.split('.')[0];
	var charCode = (evt.which) ? evt.which : event.keyCode;
	
	var currentPosition = evt.currentTarget.selectionStart;
	var tmpDotIndex = enteredAmount.indexOf('.');
	
	if(charCode === 13){
		$('.donate-button').click();
	}
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	if ( (charCode === 46 && enteredAmount.indexOf('.') >= 0 ) || ( typeof afterDot !== "undefined" && currentPosition > tmpDotIndex && afterDot.length >= 2)) {
        return false;
    }
	/*if(enteredAmount.indexOf('.') != -1 && enteredAmount.substring(enteredAmount.indexOf('.'), enteredAmount.indexOf('.').length).length > 2){
		return false;
	}*/
	return true;

}
		
</script>	
</dsp:page>
