<%--
 This page defines the confirm order/return/exchange panel
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/confirm.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">

    <dsp:include src="/panels/order/confirm/confirmOrder.jsp" otherContext="${CSRConfigurator.truContextRoot}">
    </dsp:include>

  </dsp:layeredBundle>
  <%-- This snippet is added to highlight the Products in the progress bar.. --%>
  <script type="text/javascript">
    atg.progress.update('cmcCustomerSearchPS');
  </script>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/confirm.jsp#1 $$Change: 875535 $--%>
