
<%--
 This page defines the shopping cart panel

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/cart.jsp#3 $
@updated $DateTime: 2014/04/15 07:49:50 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf"%>

<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
      <dsp:importbean var="changeGiftLinkFragment" bean="/atg/commerce/custsvc/ui/fragments/gwp/ChangeGiftLinkPageFragment" />
      <dsp:importbean var="selectGiftLinkFragment" bean="/atg/commerce/custsvc/ui/fragments/gwp/SelectGiftLinkPageFragment" />
      <dsp:importbean bean="/atg/commerce/custsvc/promotion/CouponFormHandler" />
      <dsp:importbean bean="/atg/svc/droplet/FrameworkUrlDroplet" />
      <dsp:importbean bean="/atg/dynamo/droplet/Switch" />
      <dsp:importbean bean="/atg/dynamo/droplet/PossibleValues" />
      <dsp:importbean bean="/atg/svc/security/droplet/HasAccessRight" />
      <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
      <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" />
      <dsp:importbean var="mafh" bean="/atg/commerce/custsvc/order/ManualAdjustmentsFormHandler" />
      <dsp:importbean bean="/com/tru/commerce/order/purchase/GiftOptionsFormHandler"/>
      <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
      <dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem" />
      <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" var="CSRConfigurator" />
      <dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="agentTools" />
      <dsp:importbean bean="/atg/commerce/custsvc/order/GetTotalAppeasementsForOrderDroplet" />
      <dsp:importbean bean="/atg/dynamo/droplet/multisite/SharingSitesDroplet"/>
      <dsp:importbean bean="/atg/commerce/custsvc/ui/tables/promotion/AvailablePromotionsGrid" var="walletGridConfig"/>
      <dsp:importbean bean="/atg/commerce/locations/RQLStoreLookupDroplet"/>
      <dsp:importbean bean="/atg/commerce/custsvc/order/HardgoodShippingDisplayListDefinition" var="hardgoodShippingDisplayListDefinition"/>
      <dsp:importbean bean="/atg/commerce/custsvc/order/InstoreShippingDisplayListDefinition" var="instoreShippingDisplayListDefinition"/>
      <dsp:importbean bean="/atg/commerce/custsvc/order/ElectronicShippingDisplayListDefinition" var="electronicShippingDisplayListDefinition"/>
      <dsp:importbean bean="/atg/commerce/custsvc/environment/CSREnvironmentTools" var="envTools"/>
      <dsp:importbean bean="/atg/commerce/custsvc/multisite/IsSiteAccessibleDroplet"/>
      <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
      <dsp:getvalueof var="isActiveOrderOwnerChangeble" bean="/atg/commerce/custsvc/environment/CSREnvironmentTools.activeOrderOwnerChangeble" />
	  <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
	  <dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRValidatePreSellableDroplet"/>
	  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
	  <dsp:importbean bean="/atg/multisite/Site" />
      <dsp:getvalueof var="currentSiteId" bean="Site.id" />
      	<style type="text/css">
      		#cmcShoppingCartPContent
      		{
      			min-height:500px !important;
      		}
      		#column1 .panelContent{
      			overflow-x: visible !important;
      		}
		    .shopping-cart-surprise-image
		    {
		    	    width: 20px;
				    height: 20px;
				    background-image: url(/TRU-DCS-CSR/images/dont-spoil.png);
				    background-repeat: no-repeat;
				    float: left;
				    margin-top: -5px;
				    margin-right: 5px;
		    }
		    #cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable
		    {
			    display: block;
			    float: left;
			    position: relative;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead,
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody
			{
			    float: left;
			    width: 100%;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:first-child
			{
			        width: 5%;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:nth-child(2)
			{
			   width: 40.5%;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:nth-child(3)
			{
			   width: 11.5%;
			   text-align: left;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:nth-child(4)
			{
			   width: 16.3%;
			   text-align: left;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:nth-child(5)
			{
			   width: 13.6%;
			   text-align: left;
			   padding-left: 3px !important;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable thead th:nth-child(6)	
			{
			   width: 20%;
			   text-align: left;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr:first-child
			{
			    float: left !important;
			    height: 20px !important;
			    width: 100% !important;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr
			{
				float:left;
				padding-right: 10px;
				/* margin-top: 10px; */
				width: 100%;
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:first-child
			{
			    width: 5%;
			    margin: 0px;
			    /* padding: 0px !important; */
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:nth-child(2)
			{
			    width: 42%;
			    /* padding: 0px !important; */
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:nth-child(3)
			{
			    width: 11.5%;
			    /* padding-left: 12px !important; */
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:nth-child(4)
			{
			    width: 16.3%;
			    /* padding-left: 10px !important; */
			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:nth-child(5){
				width: 13.6%;
	    		text-align: left;
	    		/* padding-left: 15px !important; */
   			}
			#cmcShoppingCartPContent #itemsForm table.atg_commerce_csr_innerTable tbody tr td:nth-child(6){
			    width: 10%;
    			text-align: left; 
    			padding-left: 20px !important;
			}
			.atg_dataTable_altRow{
				width: 100%;
			}
			#cmcShoppingCartPContent td.atg_iconCell{
				padding-right: 4px !important;
			}
			
		</style>
		<fmt:message key="shoppingcart.error.outOfStockMsg" var="outOfStockMsg" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(outOfStockMsg,doubleQuote)}"> 
		<c:set var="outOfStockMsg" value="${fn:replace(outOfStockMsg,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.customerPurchaseLimit" var="customerPurchaseLimit" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(customerPurchaseLimit,doubleQuote)}"> 
		<c:set var="customerPurchaseLimit" value="${fn:replace(customerPurchaseLimit,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.is" var="is" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(is,doubleQuote)}"> 
		<c:set var="is" value="${fn:replace(is,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.plsSelectQty" var="plsSelectQty" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(plsSelectQty,doubleQuote)}"> 
		<c:set var="plsSelectQty" value="${fn:replace(plsSelectQty,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.selectSkuQty" var="selectSkuQty" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(selectSkuQty,doubleQuote)}"> 
		<c:set var="selectSkuQty" value="${fn:replace(selectSkuQty,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.qtyZeroMsg" var="qtyZeroMsg" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(qtyZeroMsg,doubleQuote)}"> 
		<c:set var="qtyZeroMsg" value="${fn:replace(qtyZeroMsg,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
		<fmt:message key="shoppingcart.error.quantityExceedMsg" var="qtyExceedMsg" bundle="${TRUCustomResources}"/>
		<c:if test="${fn:contains(qtyZeroMsg,doubleQuote)}"> 
		<c:set var="qtyExceedMsg" value="${fn:replace(qtyExceedMsg,doubleQuote,slashDoubleQuote)}"/> 
		</c:if>
      	<script type="text/javascript">
      		var outOfStockMsg = "${outOfStockMsg}",customerPurchaseLimit = "${customerPurchaseLimit}",is = "${is}",plsSelectQty = "${plsSelectQty}",selectSkuQty = "${selectSkuQty}",qtyZeroMsg = "${qtyZeroMsg}",qtyExceedMsg = "${qtyExceedMsg}";
      	</script>
      <%-- Select Customer Panel --%>
      <c:if test="${isActiveOrderOwnerChangeble}">
        <div class="atg_commerce_csr_cartSelectCustomerPanel" style="background-color:#F5F5F5;margin-bottom:4px;padding-bottom:0px;display:block;">
	        <dsp:include src="/panels/order/customer.jsp" otherContext="${CSRConfigurator.truContextRoot}">
	          <dsp:param name="order" value="${order}" />
	        </dsp:include>
        </div>
      </c:if>
      <c:set var="order" value="${cart.current}" />
      	<dsp:droplet name="Switch">
			<dsp:param name="value" bean="/atg/multisite/Site.enableCharityDonation"/>
			<dsp:oparam name="true">
				<dsp:include page="charityDonation.jsp"></dsp:include>
			</dsp:oparam>
		</dsp:droplet>
      <dsp:droplet name="HasAccessRight">
        <dsp:param name="accessRight" value="commerce-custsvc-adjust-price-privilege" />
        <dsp:oparam name="accessGranted">
          <c:set var="adjustPricePriv" value="true" />
        </dsp:oparam>
        <dsp:oparam name="accessDenied">
          <c:set var="adjustPricePriv" value="false" />
        </dsp:oparam>
      </dsp:droplet>
      <dsp:droplet name="FrameworkUrlDroplet">
        <dsp:param name="panelStacks" value="cmcShoppingCartPS" />
        <dsp:oparam name="output">
          <dsp:getvalueof var="thisURL" bean="FrameworkUrlDroplet.url" />
        </dsp:oparam>
      </dsp:droplet>
      <csr:getCurrencyCode order="${order}">
        <c:set var="currencyCode" value="${currencyCode}" scope="request" />
      </csr:getCurrencyCode>

      <c:choose>
        <c:when test="${empty order.commerceItems}">
           <div id="atg_commerce_csr_emptyCartMessage" style="position: relative;margin-left:9px;margin-top:10px;float: left;width: 100%;"><fmt:message key="cart.emptyCart" /><a href="#" onclick="atg.commerce.csr.openPanelStack('cmcCatalogPS');return false;"><fmt:message key="cart.emptyCartLink" /></a><br>
         <%-- <dsp:include src="${selectGiftLinkFragment.URL}" otherContext="${selectGiftLinkFragment.servletContext}">
          <dsp:param name="order" value="${order}" />
          </dsp:include> --%>
          </div> 
        </c:when>
        <c:otherwise>
          <script type="text/javascript">
        if (!dijit.byId("atg_commerce_csr_catalog_productQuickViewPopup")) {
          new dojox.Dialog({ id: "atg_commerce_csr_catalog_productQuickViewPopup",
                             cacheContent: "false",
                             executeScripts: "true",
                             scriptHasHooks: "true",
                             duration: 100,
                             "class": "atg_commerce_csr_popup"});
        }
        
        /* BPP code changes - start */
        function addUpdateBppItem(relationShipId, bppInfoId, bppSkuId, bppItemId,  pageName) {
      	   	$("#bppSkuId").val(bppSkuId);
      	  	$("#bppInfoId").val(bppInfoId);
      	  	$("#bppItemId").val(bppItemId);
      	  	$("#relationShipId").val(relationShipId);
      	  if(pageName == "cart"){
      	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
     			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
	        		 }
  			},mimetype:"text/html"});
      	  }
      	if(pageName == "Review"){
      	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
     			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
	        		 }
  			},mimetype:"text/html"});
      	  }
      	 }
        
        function removeBppItem(relationShipId, bppInfoId, pageName) {
      	   $("#relShipId").val(relationShipId);
      	  	$("#bppInfoId").val(bppInfoId);
      	  	$("#pageName").val(pageName);
      	  if(pageName == "cart"){
        	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
       			if(!(_1f instanceof Error)){
  	     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
  	        		 }
    			},mimetype:"text/html"});
        	  }
        	if(pageName == "Review"){
        	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
       			if(!(_1f instanceof Error)){
  	     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
  	        		 }
    			},mimetype:"text/html"});
        	  }
      	 }
        
        function selectProtectionPlan(e,obj){	  
      	   var e = e || window.event;
      	   var checkbox = $(obj);
      	   var pageName = $(obj).attr('data-pageName');
      	   if (checkbox.is(":checked")) {		  
      		  var relId = $(obj).attr('data-relId');
      		  var bppSkuId = $(obj).attr('data-bppSkuId');
      		  var bppItem = $(obj).attr('data-bppItemId');
      	  	  var bppItemCount = $(obj).attr('data-bppItemCount');
      	  	  var pageName=$(obj).attr('data-pageName');
      		  var bppItemId;		  
      		  $('.bppPlanMultiShippingPage').attr('disabled', false);
      		  if(!isEmpty(bppItemCount) && parseInt(bppItemCount) == 1) { 
      			  bppItemId = $(obj).attr('data-bppItemId'); 
      			  addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName, obj);
      		  }		  
      	  }else{
      		    var pageName=$(obj).attr('data-pageName');
      			var bppItemInfoId = $(obj).attr('data-bppInfoId');
      			var relId = $(obj).attr('data-relId');
      			if(!isEmpty(bppItemInfoId)) {
      				removeBppItem(relId, bppItemInfoId, pageName, obj);
      			}
      	  }
      	}
       
        $(document).on('change', '.protection-plan select', function(e){
        	e.stopImmediatePropagation(); 
             		var pageName = $(this).attr('data-pageName');
             		var bppItemInfoId = $(this).attr('data-bppInfoId');
             		var relId = $(this).attr('data-relId');
             		var bppSkuId = $(this).attr('data-bppSkuId');
             		var bppItemId = $(this).val();
             		if('select' == bppItemId) {
             			return false;
             		}
             		if(isEmpty(bppItemInfoId)) {
             			addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName);
             		} else {
             			addUpdateBppItem(relId, bppItemInfoId, bppSkuId, bppItemId, pageName);
             		}
             	}); 
	        $(document).on("mouseover",".learn-more-giftWrap",function(){
	        	$(".learn-more-giftWrap-tooltip").fadeIn('fast');
	        });
	        $(document).on("mouseout",".learn-more-giftWrap",function(){
	        	$(".learn-more-giftWrap-tooltip").fadeOut('fast');
	        });
       </script>
					<dsp:form id="addUpdateBppItemForm" formid="addUpdateBppItemForm">
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppSkuId"
							name="bppSkuId" id="bppSkuId" />
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppInfoId"
							name="bppInfoId" id="bppInfoId" />
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppItemId"
							name="bppItemId" id="bppItemId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.relationShipId"
							name="relationShipId" id="relationShipId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.addUpdateBppItem" value="true" />
						
					</dsp:form>

					<dsp:form id="removeBppItemForm" formid="removeBppItemForm">
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.relationShipId"
							name="relationShipId" id="relShipId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.removeBPPItemRelationship"
							value="true" />
					</dsp:form>
					<dsp:form method="post" id="itemsForm" formid="itemsForm">
					
			<c:forEach items="${order.commerceItems}" var="item" varStatus="vs">
				<c:choose>
					<c:when test="${item.commerceItemClassType == 'donationCommerceItem' && order.totalItemCount == 1}">
							<svc-ui:frameworkUrl var="mtpiSuccessURL" panelStacks="cmcBillingPS" init="true" />
			        </c:when>
					<c:otherwise>
           					<svc-ui:frameworkUrl var="mtpiSuccessURL" panelStacks="cmcShippingAddressPS" init="true" />
        			</c:otherwise>
				</c:choose>
			</c:forEach>				
          <%--   <svc-ui:frameworkUrl var="mtpiSuccessURL" panelStacks="cmcShippingAddressPS" init="true" /> --%>
            <dsp:input type="hidden" value="false" id="persistOrder" bean="CartModifierFormHandler.persistOrder" />
            <dsp:input type="hidden" value="${mtpiSuccessURL}" bean="CartModifierFormHandler.moveToPurchaseInfoSuccessURL" />
            <dsp:input type="hidden" value="${thisURL}" bean="CartModifierFormHandler.moveToPurchaseInfoErrorURL" />
            <dsp:input type="hidden" value="${mtpiSuccessURL}" bean="CartModifierFormHandler.moveToPurchaseInfoByRelIdSuccessURL" />
            <dsp:input type="hidden" value="${thisURL}" bean="CartModifierFormHandler.moveToPurchaseInfoByRelIdErrorURL" />
            <table class="atg_dataTable atg_commerce_csr_innerTable" border="0" cellspacing="0" cellpadding="0" summary="item details">
              <thead>
                <tr>
                  <%-- Site Icon Heading --%>
                  <c:if test="${isMultiSiteEnabled == true}">
                    <th class="atg_commerce_csr_siteIcon"></th>
                  </c:if>
                  <th><fmt:message key="cart.items.itemDescription" /></th>
                  <th><fmt:message key="cart.items.inventoryStatus" /></th>
                  <th><fmt:message key="cart.items.qty" /></th>
                  <th class="atg_numberValue"><fmt:message key="cart.items.priceEach" /></th>
                  <th class="atg_numberValue atg_commerce_csr_totalPrice"><fmt:message key="cart.items.totalPrice" /></th>
                  <!-- <th class="atg_numberValue atg_commerce_csr_totalPrice">Final Price</th> -->	
                  <th abbr="edit" scope="down"></th>
                  <th abbr="remove" scope="down"></th>
                </tr>
              </thead>
              <tbody>
                <c:set var="shippingGroupCount" value="0" />
                  <c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="shippingGroupIndex">
                    <c:set var="shippingGroupCount" value="${shippingGroupCount + 1}" />
                  </c:forEach>
                  <c:choose>
                  <c:when test="${shippingGroupCount > 1}">
                    <dsp:droplet name="/atg/commerce/locations/RQLStoreLookupDroplet">
                      <dsp:oparam name="output">
                        <dsp:getvalueof param="items" var="stores"/>
                      </dsp:oparam>
                    </dsp:droplet>
                    <dsp:droplet name="CartItemDetailsDroplet">
						<dsp:param name="order" bean="ShoppingCart.current" />
						<dsp:param name="isCSCRequest" value="true"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="items" param="items" />
						</dsp:oparam>
					</dsp:droplet>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" value="${items}" />
						<dsp:param name="elementName" value="item" />
						<dsp:oparam name="output">
						<dsp:getvalueof var="skuItem" param="item.skuItem"/>
						<dsp:getvalueof var="productItem" param="item.productItem"/>
						<dsp:getvalueof var="itemSiteId" param="item.siteId"/>
						<dsp:getvalueof var="itemId" param="item.id"/>
						<dsp:getvalueof var="itemProductId" param="item.productId"/>
						<dsp:getvalueof var="catalogRefId" param="item.skuId"/>
						<dsp:getvalueof var="itemOnSale" param="item.onSale"/>
						<dsp:getvalueof var="itemSalePrice" param="item.salePrice"/>
						<dsp:getvalueof var="itemListPrice" param="item.price"/>
						<dsp:getvalueof var="itemTotalAmount" param="item.totalAmount"/>
						<dsp:getvalueof var="itemRelQty" param="item.relItemQtyInCart"/>
						<dsp:getvalueof var="itemMetaQty" param="item.quantity"/>
						<dsp:getvalueof var="itemCommerceItemId" param="item.commerceItemId"/>
						<dsp:getvalueof var="itemShippingGroupId" param="item.shipGroupId"/>
						<dsp:getvalueof var="itemDisplayStrikeThrough" param="item.displayStrikeThrough"/>
						<dsp:getvalueof var="itemDisplayStrikeThroughPrice" param="item.displayStrikeThroughPrice"/>
						<dsp:getvalueof var="itemLocationId" param="item.locationId"/>
						<dsp:getvalueof var="wrapSkuId" param="item.wrapSkuId"/>
						<dsp:getvalueof var="wrapQuantity" param="item.wrapQuantity"/>
						<dsp:getvalueof var="donationAmount" param="item.donationAmount"/>
						<dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough" />
						<dsp:getvalueof var="savedAmount" param="item.savedAmount" />
						<dsp:getvalueof var="price" param="item.price" />
						<dsp:getvalueof var="salePrice" param="item.salePrice"/>
						<dsp:getvalueof var="description" param="item.skuDescription" />
						<dsp:getvalueof var="skuDisplayName" param="item.displayName" />
						<dsp:getvalueof var="gwpQtyToBeDisabled" param="item.gWPPromotionItem"/>
						<c:set var="locationIdSupplied" value="false"/>
						<dsp:getvalueof var="Name" param="item.skuItem.onlinePID" />
						
						
						
						<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
													<dsp:param name="productId" param="item.skuItem.onlinePID" />
													<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="productPageUrl"
															param="productPageUrl" />
													</dsp:oparam>
												</dsp:droplet>
												
					 <c:choose>
					  <c:when test="${not empty skuItem.nmwa && (skuItem.nmwa eq 'Y')}">
			         		<c:set var="nmwaEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="nmwaEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
		         							<input type="hidden" id="itemProductId_${catalogRefId}" value="${itemProductId}">
											<input type="hidden" id="catalogRefId_${catalogRefId}" value="${catalogRefId}">
											<input type="hidden" id="currentSiteId_${catalogRefId}" value="${currentSiteId}">
											<input type="hidden" id="productPageUrl_${catalogRefId}" value="${productPageUrl}">
											<input type="hidden" id="skuDisplayName_${catalogRefId}" value="${skuDisplayName}">
											<input type="hidden" id="description_${catalogRefId}" value="${description}">
											<input type="hidden" id="onlinePID_${catalogRefId}" value="${Name}">
						
                      <dsp:tomap var="sku" value="${skuItem}" />
                      <dsp:tomap var="product" value="${productItem}" />
                      <c:choose>
							 <c:when test="${not empty itemMetaQty}">
								<c:set var="itemQty" value="${itemMetaQty}"/>
							</c:when>
							<c:otherwise>
								<c:set var="itemQty" value="${itemRelQty}"/>
							</c:otherwise>
					  </c:choose>
                      <%-- <c:choose>
			         	<c:when test="${not empty product.itemInStorePickUp && (product.itemInStorePickUp eq 'Y' || product.itemInStorePickUp eq 'YES' || product.itemInStorePickUp eq 'Yes' || product.itemInStorePickUp eq 'yes') || sku.shipToStoreEligible eq true}">
			         		<c:set var="ispuEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="ispuEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
		         	<input type="hidden" id="isEligibleIspu_${catalogRefId}" value="${ispuEligible}"/> --%>
		         	
   					<c:if test="${donationAmount != null}">
		        		<tr>
		        		<td></td>
		        		<td><fmt:message key="cart.toysFor.tots" bundle="${TRUCustomResources}"/></td>
		        		<td> <fmt:message key="cart.thank.you" bundle="${TRUCustomResources}"/></td>
		        	   <td class="atg_commerce_csr_quatity"><input value="1" type="hidden" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}"  class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/></br>
                           </td> 
		        		<td></td>
		        		<td>$${donationAmount}</td>
		        		<td></td>
		        		<td class="atg_iconCell">
                                  <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="atg.commerce.csr.cart.deleteCartItem('${itemCommerceItemId}');return false;"> <fmt:message
                                    key="cart.items.delete" /></a>Remove
                                </td>
		        		</tr>
		              </c:if> 
		         	
		         	<c:if test="${itemListPrice != 0.0}">
                      <tr style="height:20px;">
                        <td colspan="8"></td>
                      </tr>
                      <tr class="atg_dataTable_altRow">
                        <c:if test="${isMultiSiteEnabled == true}">
                          <c:set var="siteId" value="${itemSiteId}" />
                          <td class="atg_commerce_csr_siteIcon"><csr:siteIcon siteId="${siteId}" /></td>
                        </c:if>

                        <%-- Item Description --%>
                        <td>
                          <c:set var="cartShareableSite" value="${true}" />
                          <dsp:droplet name="/atg/dynamo/droplet/multisite/SitesShareShareableDroplet">
                            <dsp:param name="siteId" value="${currentSiteId}" />
                            <dsp:param name="otherSiteId" value="${siteId}" />
                            <dsp:param name="shareableTypeId" value="${CSRConfigurator.cartShareableTypeId}" />
                            <dsp:oparam name="false">
                              <c:set var="cartShareableSite" value="${false}" />
                            </dsp:oparam>
                           </dsp:droplet>
                        
                        <ul class="atg_commerce_csr_itemDesc">
                          <li>
                          <%-- If the current site and item site do not share shopping cart, then display the item Description without a link --%>
                          <c:choose>
                            <c:when test="${cartShareableSite == false}">
                              ${fn:escapeXml(sku.displayName)}
                            </c:when>
                            <c:when test="${(isMultiSiteEnabled == true) && (isSiteDeleted != true)}">
                              <a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(itemProductId)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}');
                                    return false;" href="#">${fn:escapeXml(sku.displayName)}</a>
                            </c:when>
                            <c:otherwise>
                              <a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(itemProductId)}');
                                    return false;" href="#">${fn:escapeXml(sku.displayName)}</a>
                            </c:otherwise>
                          </c:choose>
                              
                          
                          	<c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="shippingGroupIndex">
		                        <c:forEach items="${shippingGroup.commerceItemRelationships}" var="ciRelationship">
		                          <c:choose>
		                            <c:when test="${sku.id == ciRelationship.commerceItem.auxiliaryData.catalogRef.repositoryId && ciRelationship.id == itemId}">
		                            	<c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup'}">
				                          <dsp:include src="${changeGiftLinkFragment.URL}" otherContext="${CSRConfigurator.truContextRoot}">
					                         <dsp:param name="item" param="item" />
					                          <dsp:param name="order" value="${order}" />
					                          <dsp:param name="multiShipping" value="true"/>
					                          <dsp:param name="giftWrapsku" value="${sku}" />
					                          <dsp:param name="itemShippingGroupId" value="${itemShippingGroupId}"/>
				                      	  </dsp:include>
				                      	 </c:if>
			                        </c:when>
			                      </c:choose>
			                    </c:forEach>
			                  </c:forEach>
                          </li>
                          <li>${fn:escapeXml(catalogRefId)}</li></br>
																<dsp:droplet name="Switch">
																	<dsp:param name="value"
																		param="item.skuItem.shipInOrigContainer" />
																	<dsp:oparam name="true">
																		<div class="shopping-cart-surprise-image inline"></div>
																		<fmt:message key="shoppingcart.spoil.surprise"
																			bundle="${TRUCustomResources}" />
																		<span>&#xb7;</span>
																		<a href="#" onclick="showSpoilDetails();"
																			class="learn-more-giftWrap"><fmt:message
																				key="shoppingcart.learn.more"
																				bundle="${TRUCustomResources}" /></a>
																	</dsp:oparam>
																</dsp:droplet>
															</ul>
                        </td>
                        <%-- Inventory status --%>
                        <td class="atg_commerce_csr_inventoryStatus">
                        <dsp:droplet name="TRUCSRValidatePreSellableDroplet">
							<dsp:param name="sku" value="${skuItem}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="preSellable" param="preSellable" />
								<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
						</dsp:droplet>
						<c:if test="${not empty itemLocationId}">
							<c:set var="locationIdSupplied" value="true"/>
						</c:if>
                        <csr:inventoryStatus itemId = "${itemCommerceItemId}" formattedDate="${formattedDate}" locationIdSupplied="${locationIdSupplied}" locationId="${itemLocationId}"  commerceItemId="${catalogRefId}" preSellable="${preSellable}"  preSellQtyUnits="${sku.presellQuantityUnits}" customerPurchaseLimit="${sku.customerPurchaseLimit}" nmwaEligible="${nmwaEligible}"/>
                        </td>
                        <%-- Quantity --%>
                        
                        <td class="atg_commerce_csr_quatity">${itemQty}</td>
                        <%-- Price Each --%>
                        <td class="atg_numberValue atg_commerce_csr_priceEach" style="padding-left: 13px !important;">
                         <c:choose>
																<c:when test="${displayStrikeThrough}">
																	<span
																		class="atg_commerce_csr_common_content_strikethrough">
																		<web-ui:formatNumber value="${price}" type="currency"
																			currencyCode="${currencyCode}" />
																	</span>
																	<span><web-ui:formatNumber value="${salePrice}"
																			type="currency" currencyCode="${currencyCode}" /></span>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${savedAmount > 0.0}">
																			<web-ui:formatNumber value="${salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:when>
																		<c:otherwise>
																			<web-ui:formatNumber value="${salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
                        
                        </td>
                        <%-- Total Price --%>
                        <td class="atg_numberValue atg_commerce_csr_totalPrice"><web-ui:formatNumber value="${itemTotalAmount}" type="currency" currencyCode="${currencyCode}" /></td>
                      
                        <%-- Edit line item --%>

                        <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                          <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                          <dsp:param name="order" value="${order}" />
                          <dsp:oparam name="empty">
                            <td class="atg_iconCell"><dsp:tomap var="product" value="${productItem}" /> <c:if test="${fn:length(product.childSKUs) > 1}">
                              <svc-ui:frameworkPopupUrl var="popupUrl" value="/include/order/editProductSKU.jsp" context="${CSRConfigurator.contextRoot}" windowId="${windowId}" mode="apply" commerceItemId="${itemId}" />
                              <a id="edit_${sku.id}" class="atg_tableIcon atg_propertyEdit" title="<fmt:message key='cart.items.edit'/>" href="#"
                                onclick="atg.commerce.csr.common.showPopupWithReturn({
                                  popupPaneId: 'atg_commerce_csr_catalog_productQuickViewPopup',
                                  title: '${fn:escapeXml(sku.displayName)}',
                                  url: '${popupUrl}',
                                  onClose: function( args ) {
                                    if ( args.result == 'ok' ) {
                                      atgSubmitAction({
                                        panels : ['cmcShoppingCartP'],
                                        panelStack : 'cmcShoppingCartPS',
                                        form : document.getElementById('transformForm')
                                      });
                                    }
                                  }});return false;">
                              <fmt:message key="cart.items.edit" /> </a>
                            </c:if></td>
                          </dsp:oparam>
                          <dsp:oparam name="output">
                            <td class="atg_iconCell"></td>
                          </dsp:oparam>
                        </dsp:droplet>
                        
                        <%-- Delete line item --%>
            
							<dsp:getvalueof var="fromMetaInfo" param="item.fromMetaInfo"></dsp:getvalueof>
                     
                     
                     
               
                     
                     	<dsp:droplet name="Switch">
					<dsp:param name="value" param="item.fromMetaInfo" />
					<dsp:oparam name="true">
								    <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('update','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
                        </td>
					</dsp:oparam>
					<dsp:oparam name="false">
						   <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('remove','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
					</dsp:oparam>
				</dsp:droplet>
                     
				
                      </tr>
                      <c:if test="${skuItem.bppEligible}"> 
							<tr>
								<td></td>
								<td colspan="3"	class="atg_commerce_csr_shoppingCart_items_underline">
									<%-- <dsp:droplet name="CartItemDetailsDroplet">
										<dsp:param name="order" bean="ShoppingCart.current" />
										<dsp:oparam name="output"> --%>
											<dsp:include src="/include/gwp/buyersProtectionPlan.jsp" otherContext="${CSRConfigurator.truContextRoot}">
											<dsp:param name="pageName" value="cart" />
											<dsp:param name="items" param="items" />
											<dsp:param name="multiShipping" value="true"/>
											<dsp:param name="currentItem" value="${itemCommerceItemId}"/>
											</dsp:include>
											<%-- </dsp:oparam>
									</dsp:droplet> --%>
								</td>
							</tr>
						</c:if>
                      </c:if>
                     <c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="shippingGroupIndex">
                        <c:forEach items="${shippingGroup.commerceItemRelationships}" var="ciRelationship">
                          <c:choose>
                            <c:when test="${sku.id == ciRelationship.commerceItem.auxiliaryData.catalogRef.repositoryId && ciRelationship.id == itemId}"> 
                              <tr>
                                <c:if test="${isMultiSiteEnabled == true}">
                                 <td></td>
                                 <td>
                                </c:if>
                                <c:if test="${isMultiSiteEnabled == false}">
                                  <td>
                                </c:if>
                                <c:choose>
                                  <c:when test="${shippingGroup.shippingGroupClassType == 'hardgoodShippingGroup'}">
                                    <span class="atg-csc-base-bold">
                                      <c:choose>
                                        <c:when test="${!empty hardgoodShippingDisplayListDefinition.intro}">
                                          <fmt:message key="${hardgoodShippingDisplayListDefinition.intro}" var="hardgoodShippingIntro"/>
                                        </c:when>
                                        <c:otherwise>
                                          <fmt:message key="inStorePickup.shippingGroup.deliver" var="hardgoodShippingIntro"/>
                                        </c:otherwise>
                                      </c:choose>
                                      ${hardgoodShippingIntro}
                                    </span>
                                    <c:if test="${!empty shippingGroup.shippingAddress}">
                                      <span id="address_${itemId}">
                                        <c:set var="showTooltip" value="${false}" />
                                        <c:forEach items="${hardgoodShippingDisplayListDefinition.items}" var="listItem" varStatus="status">
                                          <c:if test="${!empty shippingGroup.shippingAddress[listItem]}">
                                            <c:set var="showTooltip" value="${true}" />
                                            ${shippingGroup.shippingAddress[listItem]}
                                            <c:if test="${!status.last}">
                                            ,
                                            </c:if>
                                          </c:if>
                                        </c:forEach>
                                        <c:if test="${showTooltip}">
                                          <span class="atg-csc-in-store-more"></span>
                                        </c:if>
                                      </span>
                                      <c:if test="${showTooltip}">
                                        <div dojoType="dijit.Tooltip" connectId="address_${ciRelationship.id}">
                                          ${shippingGroup.shippingAddress.address1}
                                          <br />
                                          <c:if test="${!empty shippingGroup.shippingAddress.address2}">
                                            ${shippingGroup.shippingAddress.address2}
                                            <br />
                                          </c:if>
                                          ${shippingGroup.shippingAddress.city}
                                          <c:if test="${!empty shippingGroup.shippingAddress.state}">
                                            , ${shippingGroup.shippingAddress.state}
                                          </c:if>
                                          <br />
                                          ${shippingGroup.shippingAddress.country}
                                          <br />
                                          ${shippingGroup.shippingAddress.phoneNumber}
                                        </div> 
                                      </c:if>
                                    </c:if>
                                    <br />
                                  </c:when>
                                  <c:when test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
                                    <style>
                                      #edit_${sku.id} {
                                        display:none;
                                      }
                                    </style>
                                    <span class="atg-csc-base-bold">
                                      <c:choose>
                                        <c:when test="${!empty instoreShippingDisplayListDefinition.intro}">
                                          <fmt:message key="${instoreShippingDisplayListDefinition.intro}" var="instoreShippingIntro"/>
                                        </c:when>
                                        <c:otherwise>
                                          <fmt:message key="inStorePickup.shippingGroup.deliver" var="instoreShippingIntro"/>
                                        </c:otherwise>
                                      </c:choose>
                                      ${instoreShippingIntro}
                                    </span>
                                    <c:forEach items="${stores}" var="store">
                                      <dsp:tomap value="${store}" var="store"/>
                                      <c:if test="${shippingGroup.locationId == store.locationId}">
                                      	<c:set var="locationIdSupplied" value="true"/>
                                      	<c:set var="locationId" value="${store.locationId}"/>
                                        <span id="store_${ciRelationship.id}">
                                          <c:forEach items="${instoreShippingDisplayListDefinition.items}" var="listItem" varStatus="status">
                                            <c:if test="${!empty store[listItem]}">
                                              ${store[listItem]}
                                              <c:if test="${!status.last}">
                                              ,
                                              </c:if>
                                            </c:if>
                                          </c:forEach>
                                          <span class="atg-csc-in-store-more"></span>
                                        </span>
                                        <div dojoType="dijit.Tooltip" connectId="store_${ciRelationship.id}">
                                          <b>${store.name}</b>
                                          <br />
                                          ${store.address1}
                                          <br />
                                          ${store.city} ${store.stateAddress}, ${store.postalCode}
                                          <br />
                                          ${store.country}
                                          <br />
                                          ${store.phoneNumber}
                                        </div> 
                                      </c:if>
                                    </c:forEach>
                                    <br />
                                  </c:when>
                                  <c:when test="${shippingGroup.shippingGroupClassType == 'electronicShippingGroup'}">
                                    <span class="atg-csc-base-bold">
                                      <c:choose>
                                        <c:when test="${!empty electronicShippingDisplayListDefinition.intro}">
                                          <fmt:message key="${electronicShippingDisplayListDefinition.intro}" var="electronicShippingIntro"/>
                                        </c:when>
                                        <c:otherwise>
                                          <fmt:message key="inStorePickup.shippingGroup.deliver" var="electronicShippingIntro"/>
                                        </c:otherwise>
                                      </c:choose>
                                      ${electronicShippingIntro}
                                    </span>
                                    <c:forEach items="${electronicShippingDisplayListDefinition.items}" var="listItem" varStatus="status">
                                      <c:if test="${!empty shippingGroup[listItem]}">
                                        ${shippingGroup[listItem]}
                                        <c:if test="${!status.last}">
                                        ,
                                        </c:if>
                                      </c:if>
                                    </c:forEach>
                                    <br />
                                  </c:when>
                                </c:choose>
                                </td>
                                <td>
                                <csr:inventoryStatus commerceRelId = "${ciRelationship.id}" locationIdSupplied="${locationIdSupplied}" locationId="${locationId}" commerceItemId="${catalogRefId}" customerPurchaseLimit="${sku.customerPurchaseLimit}" /></td>
                                
                                <td class="atg_commerce_csr_quatity">
                                	<c:choose>
	                                	<c:when test="${gwpQtyToBeDisabled}">
	                                		<input type="hidden" name="${itemCommerceItemId}" value="${itemQty}"/>
	                                		<input value="${itemQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}"  id="${ciRelationship.id}"  class="ciRelationship updateCartItemQty" disabled="disabled" onkeypress="return isNumberOnly(event);"/><input type="hidden" id="itemSkuId" value="${fn:escapeXml(catalogRefId)}"/></br>
	                                	</c:when>
	                                	<c:otherwise>
	                                  		<input value="${itemQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}" id="${ciRelationship.id}" class="ciRelationship updateCartItemQty" onkeypress="return isNumberOnly(event);"/><input type="hidden" id="itemSkuId" value="${fn:escapeXml(catalogRefId)}"/></br>
	                                  	</c:otherwise>
                                  	</c:choose>
                                  <c:if test="${not empty sku.customerPurchaseLimit}">
        					 		limit ${sku.customerPurchaseLimit} items per customer
         					 	  </c:if>
                                  <input value="${itemQty}" type="hidden" id="${ciRelationship.id}_quantity" />
                                  <input type="hidden" id="itemSkuId" value="${fn:escapeXml(item.catalogRefId)}"/>
                                  </td>
                                  <td></td><td></td><td></td>
                   		<dsp:getvalueof var="fromMetaInfo" param="item.fromMetaInfo"></dsp:getvalueof>
                     	<dsp:droplet name="Switch">
					<dsp:param name="value" param="item.fromMetaInfo" />
					<dsp:oparam name="true">
								    <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('update','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
                        </td>
					</dsp:oparam>
					<dsp:oparam name="false">
						   <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('remove','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
					</dsp:oparam>
				</dsp:droplet>
                                
                              </tr>
                            </c:when>
                          </c:choose>
                        </c:forEach>
                      </c:forEach>
                      
                      <%-- Display breakdown if commerce item is associated with a giftlist --%>
                      <c:set var="customerTotalQuantity" value="${itemQty}" />
                      <c:set var="giftlistTotalQuantity" value="0" />
                      <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                        <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                        <dsp:param name="order" value="${order}" />
                        <dsp:oparam name="output">
                          <dsp:getvalueof var="key" param="key" />
                          <dsp:getvalueof var="value" param="element" />
                            <c:set var="customerTotalQuantity" value="${customerTotalQuantity - value }" />
                            <c:set var="giftlistTotalQuantity" value="${giftlistTotalQuantity + value }" />
                        </dsp:oparam>
                        <dsp:getvalueof var="numberOfGiftRecipients" param="size" />
                      </dsp:droplet><%-- End GiftlistShoppingCartQuantityDroplet --%>
                      <c:if test="${customerTotalQuantity > '0' && giftlistTotalQuantity > '0'}">
                        <tr>
                          <c:if test="${isMultiSiteEnabled == true}">
                           <td></td>
                           <td>
                          </c:if>
                          <c:if test="${isMultiSiteEnabled == false}">
                            <td>
                          </c:if>
                          <ul class="atg_commerce_csr_itemDesc">
                            <span class="atg-csc-in-store-sub-item"></span><li class="atg_commerce_csr_giftwishListName"><fmt:message key="cart.customer.currentCustomer.label" /><%-- Display Current Customer label --%></li>
                          </ul>
                          </td>
                          <td></td>
                          <td><c:out value="${customerTotalQuantity}" /><%-- Display Customer Quantity --%></td>
                        </tr>
                      </c:if>
                      <%-- Multiple Gift Recipients detected, So disable Quantity box for this particular SKU --%>
                      <c:if test="${numberOfGiftRecipients > 1}">
                      <input value="${itemQty}" type="hidden" size="5" maxlength="5" name="${itemCommerceItemId}" id="hidden_${itemCommerceItemId}" />
                        <script type="text/javascript">         
                         /*  dojo.byId("${item.id}").disabled = true; */
                        </script>
                      </c:if>
                      <%-- Gift Recipient along with current customer purchasing sku for themself, so disable Quantity box for this particular SKU --%>
                      <c:if test="${numberOfGiftRecipients > '0' && customerTotalQuantity > '0'}">
                      <input value="${itemQty}" type="hidden" size="5" maxlength="5" name="${itemCommerceItemId}" id="hidden_${itemCommerceItemId}" />
                        <script type="text/javascript">         
                         /*  dojo.byId("${item.id}").disabled = true; */
                        </script>
                      </c:if>
                      <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                        <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                        <dsp:param name="order" value="${order}" />
                        <dsp:oparam name="output">
                          <dsp:getvalueof var="key" param="key" />
                          <dsp:getvalueof var="value" param="element" />
                          <dsp:droplet name="/atg/commerce/gifts/GiftlistLookupDroplet">
                            <dsp:param name="id" param="key" />
                            <dsp:oparam name="output">
                              <dsp:setvalue paramvalue="element" param="giftlist" />
                              <tr>
                                <c:if test="${isMultiSiteEnabled == true}">
                                  <td></td>
                                  <td>
                                </c:if>
                                <c:if test="${isMultiSiteEnabled == false}">
                                  <td>
                                </c:if>
                                <ul class="atg_commerce_csr_itemDesc">
                                  <span class="atg-csc-in-store-sub-item"></span><li class="atg_commerce_csr_giftwishListName">
                                  <dsp:getvalueof var="eventName" vartype="java.lang.String" param="giftlist.eventName" />
                                  <a href="#" class="blueU" onclick="atg.commerce.csr.order.gift.giftlistBuyFrom('${key}');return false;">
                                  <dsp:valueof param="giftlist.owner.firstName" />&nbsp; 
                                  <dsp:valueof param="giftlist.owner.lastName" />, 
                                  <c:out value="${eventName}" /> 
                                  </a><%-- Display Gift Recipient Name and Event Name --%></li>
                                </ul>
                                </td>
                                <td></td>
                                <td><c:out value="${value}" /></td><%-- Display Gift Recipient Quantity --%>
                              </tr>
                            </dsp:oparam>
                          </dsp:droplet><%-- End GiftlistLookupDroplet --%>
                        </dsp:oparam>
                      </dsp:droplet><%-- End GiftlistShoppingCartQuantityDroplet --%>
                    <%-- </c:forEach> --%>
                    </dsp:oparam>
                    </dsp:droplet> 
                  </c:when>
                  <c:otherwise>
                    <%-- <c:forEach items="${order.commerceItems}" var="item" varStatus="vs"> --%>
                    				<dsp:droplet name="CartItemDetailsDroplet">
										<dsp:param name="order" bean="ShoppingCart.current" />
										<dsp:param name="isCSCRequest" value="true"/>
										<dsp:oparam name="output">
										<dsp:getvalueof var="items" param="items" />
											</dsp:oparam>
									</dsp:droplet>
										<dsp:droplet name="ForEach">
										<dsp:param name="array" value="${items}" />
										<dsp:getvalueof param="count" var="vs"></dsp:getvalueof>
										<dsp:param name="elementName" value="item" />
										<dsp:oparam name="output">
										<dsp:getvalueof var="skuItem" param="item.skuItem"/>
											<dsp:getvalueof var="productItem" param="item.productItem"/>
											<dsp:getvalueof var="itemSiteId" param="item.siteId"/>
											<dsp:getvalueof var="itemId" param="item.id"/>
											<dsp:getvalueof var="itemProductId" param="item.productId"/>
											<dsp:getvalueof var="catalogRefId" param="item.skuId"/>
											<dsp:getvalueof var="itemOnSale" param="item.onSale"/>
											<dsp:getvalueof var="itemSalePrice" param="item.salePrice"/>
											<dsp:getvalueof var="itemListPrice" param="item.price"/>
											<dsp:getvalueof var="itemTotalAmount" param="item.totalAmount"/>
											<dsp:getvalueof var="itemRelQty" param="item.relItemQtyInCart"/>
											<dsp:getvalueof var="itemMetaQty" param="item.quantity"/>
											<dsp:getvalueof var="itemCommerceItemId" param="item.commerceItemId"/>
											<dsp:getvalueof var="wrapItemCommerceItemId" param="item.wrapCommerceItemId"/>
											<dsp:getvalueof var="itemShippingGroupId" param="item.shipGroupId"/>
											<dsp:getvalueof var="itemDisplayStrikeThrough" param="item.displayStrikeThrough"/>
											<dsp:getvalueof var="itemDisplayStrikeThroughPrice" param="item.displayStrikeThroughPrice"/>
											<dsp:getvalueof var="itemLocationId" param="item.locationId"/>
											<dsp:getvalueof var="wrapQuantity" param="item.wrapQuantity"/>
											<dsp:getvalueof var="wrapSkuId" param="item.wrapSkuId"/>
											<dsp:getvalueof var="donationAmount" param="item.donationAmount"/>
											<dsp:getvalueof var="displayStrikeThrough" param="item.displayStrikeThrough" />
											<dsp:getvalueof var="savedAmount" param="item.savedAmount" />
											<dsp:getvalueof var="price" param="item.price" />
											<dsp:getvalueof var="salePrice" param="item.salePrice"/>
											<dsp:getvalueof var="description" param="item.skuDescription" />
											<dsp:getvalueof var="skuDisplayName" param="item.displayName" />
											<dsp:getvalueof var="Name" param="item.skuItem.onlinePID" />
											<dsp:getvalueof var="gwpQtyToBeDisabled" param="item.gWPPromotionItem"/>
																				
											<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
													<dsp:param name="productId" param="item.skuItem.onlinePID" />
													<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="productPageUrl"
															param="productPageUrl" />
													</dsp:oparam>
												</dsp:droplet>
											<input type="hidden" id="itemProductId_${catalogRefId}" value="${itemProductId}">
											<input type="hidden" id="catalogRefId_${catalogRefId}" value="${catalogRefId}">
											<input type="hidden" id="currentSiteId_${catalogRefId}" value="${currentSiteId}">
											<input type="hidden" id="productPageUrl_${catalogRefId}" value="${productPageUrl}">
											<input type="hidden" id="skuDisplayName_${catalogRefId}" value="${skuDisplayName}">
											<input type="hidden" id="description_${catalogRefId}" value="${description}">
											<input type="hidden" id="onlinePID_${catalogRefId}" value="${Name}">
							<c:choose>
							 <c:when test="${not empty itemMetaQty}">
								<c:set var="itemQty" value="${itemMetaQty}"/>
							</c:when>
							<c:otherwise>
								<c:set var="itemQty" value="${itemRelQty}"/>
							</c:otherwise>
					  </c:choose>
					  <c:choose>
					  <c:when test="${not empty skuItem.nmwa && (skuItem.nmwa eq 'Y')}">
			         		<c:set var="nmwaEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="nmwaEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
                    
                      <dsp:tomap var="sku" value="${skuItem}" />
                      <dsp:tomap var="product" value="${productItem}" />
                     <c:choose>
			         	<c:when test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes') || sku.shipToStoreEligible eq true}">
			         		<c:set var="ispuEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="ispuEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
		         	<input type="hidden" id="isEligibleIspu_${sku.id}" value="${ispuEligible}"/> 
		         	    <c:if test="${donationAmount != null}">
		        		<tr>
		        		<td></td>
		        		<td><fmt:message key="cart.toysFor.tots" bundle="${TRUCustomResources}"/></td>
		        		<td> <fmt:message key="cart.thank.you" bundle="${TRUCustomResources}"/></td>
		        	   <td class="atg_commerce_csr_quatity"><input value="1" type="hidden" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}"  class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/></br>
                           </td> 
		        		<td></td>
		        		<td>$<dsp:valueof param="item.donationAmount" number="#.00"/></td>
		        		<td></td>
		        		<td class="atg_iconCell">
                                  <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="atg.commerce.csr.cart.deleteCartItem('${itemCommerceItemId}');return false;"> <fmt:message
                                    key="cart.items.delete" /></a>Remove
                                </td>
		        		</tr>
		              </c:if> 
		         	
                     <%--  <c:choose>
                      <c:when test="${item.commerceItemClassType ne 'giftWrapCommerceItem'}"> --%>
                      <c:if test="${itemListPrice != 0.0}">
                      <tr class="atg_dataTable_altRow">
                        <c:if test="${isMultiSiteEnabled == true}">
                          <c:set var="siteId" value="${itemSiteId}" />
                          <td class="atg_commerce_csr_siteIcon"><csr:siteIcon siteId="${siteId}" /></td>
                        </c:if>


                        <%-- Item Description --%>
                        <td>
                          <c:set var="cartShareableSite" value="${true}" />
                          <dsp:droplet name="/atg/dynamo/droplet/multisite/SitesShareShareableDroplet">
                            <dsp:param name="siteId" value="${currentSiteId}" />
                            <dsp:param name="otherSiteId" value="${siteId}" />
                            <dsp:param name="shareableTypeId" value="${CSRConfigurator.cartShareableTypeId}" />
                            <dsp:oparam name="false">
                              <c:set var="cartShareableSite" value="${false}" />
                            </dsp:oparam>
                           </dsp:droplet>

							<ul class="atg_commerce_csr_itemDesc">
								<li>
									<%-- If the current site and item site do not share shopping cart, then display the item Description without a link --%>
									<c:choose>
										<c:when test="${cartShareableSite == false}">
					                      ${fn:escapeXml(sku.displayName)}
					                    </c:when>
										<c:when
											test="${(isMultiSiteEnabled == true) && (isSiteDeleted != true)}">
											<a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(itemProductId)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}');
                            return false;" href="#">${fn:escapeXml(sku.displayName)}</a>
										</c:when>
										<c:otherwise>
											<a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(itemProductId)}');
                            return false;" href="#">${fn:escapeXml(sku.displayName)}</a>
										</c:otherwise>
									</c:choose>
								</li>
								<c:choose>
									<c:when
										test="${envTools.siteAccessControlOn =='true' }">
										<c:set var="siteId" value="${itemSiteId}" />
										<dsp:droplet name="IsSiteAccessibleDroplet">
											<dsp:param name="siteId" value="${siteId}" />
											<dsp:oparam name="true">
												<li>${fn:escapeXml(catalogRefId)}</li>
												<li><dsp:include src="${changeGiftLinkFragment.URL}" otherContext="${CSRConfigurator.truContextRoot}">
														<dsp:param name="item" param="item" />
														<dsp:param name="order" value="${order}" />
														<dsp:param name="existingRelationShipId"
															param="existingRelationShipId" />
														<dsp:param name="giftWrapsku" value="${sku}" />
														<dsp:param name="currentItem" value="${item.id}" />
														<dsp:param name="multiShipping" value="false" />
													</dsp:include>
												</li>
											</dsp:oparam>
										</dsp:droplet>
									</c:when>
									<c:otherwise>
										<li><dsp:include
												src="${changeGiftLinkFragment.URL}"
												otherContext="${changeGiftLinkFragment.servletContext}">
												<dsp:param name="item" value="${item}" />
												<dsp:param name="order" value="${order}" />
												<dsp:param name="multiShipping" value="false" />
											</dsp:include></li>
									</c:otherwise>
								</c:choose>
								<li><dsp:droplet name="Switch">
										<dsp:param name="value"
											param="item.skuItem.shipInOrigContainer" />
										<dsp:oparam name="true">
											<div class="shopping-cart-surprise-image inline"></div>
											<fmt:message key="shoppingcart.spoil.surprise"
												bundle="${TRUCustomResources}" />
											<span>&#xb7;</span>
											<a href="#" onclick="showSpoilDetails();"
												class="learn-more-giftWrap"><fmt:message
													key="shoppingcart.learn.more"
													bundle="${TRUCustomResources}" /></a>
										</dsp:oparam>
									</dsp:droplet></li>
							</ul>
						</td>
                        <%-- Inventory status --%>
                        <td class="atg_commerce_csr_inventoryStatus">                    
                        <dsp:droplet name="TRUCSRValidatePreSellableDroplet">
							<dsp:param name="sku" value="${skuItem}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="preSellable" param="preSellable" />
								<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
						</dsp:droplet>
						<c:set var="locationIdSupplied" value="false"/>
						<c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="shippingGroupIndex">
							<c:if test="${shippingGroup.shippingGroupClassType eq 'inStorePickupShippingGroup'}">
								<c:set var="locationId" value="${shippingGroup.locationId}"/>
								<c:set var="locationIdSupplied" value="true"/>
							</c:if>
						</c:forEach>
                         <csr:inventoryStatus itemId = "${itemCommerceItemId}" formattedDate="${formattedDate}" commerceItemId="${catalogRefId}" locationIdSupplied="${locationIdSupplied}" locationId="${itemLocationId}" preSellable="${preSellable}"  preSellQtyUnits="${sku.presellQuantityUnits}" customerPurchaseLimit="${sku.customerPurchaseLimit}" nmwaEligible="${nmwaEligible}"/></td>
                        
                        
                        <%-- Quantity --%>
                      
                        <c:choose>
                        
                          <c:when test ="${envTools.siteAccessControlOn =='true' }">
                            <c:set var="siteId" value="${itemSiteId}"/>   
                            <dsp:droplet name="IsSiteAccessibleDroplet">
                              <dsp:param name="siteId" value="${siteId}"/>
                              <dsp:oparam name="true">
                                <td class="atg_commerce_csr_quatity">
                                <c:choose>
	                                <c:when test="${gwpQtyToBeDisabled}">
	                                	<input type="hidden" name="${itemCommerceItemId}" value="${itemQty}"/>
	                               		 <input value="${itemMetaQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}" class="updateCartItemQty" disabled="disabled" onkeypress="return isNumberOnly(event);"/></br>
	                                </c:when>
	                                <c:otherwise>
	                                	<input value="${itemMetaQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}" class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/></br>
	                                </c:otherwise>
                                </c:choose>
                                <c:if test="${not empty sku.customerPurchaseLimit}">
        					 limit ${sku.customerPurchaseLimit} items per customer
         					 </c:if><input type="hidden" id="itemSkuId" value="${fn:escapeXml(catalogRefId)}"/></td>
                                </dsp:oparam>
                              <dsp:oparam name="false">
                                <td class="atg_commerce_csr_quatity"/>
                              </dsp:oparam>
                            </dsp:droplet>
                          </c:when>
                          <c:otherwise>
                          		<c:choose>
	                                <c:when test="${gwpQtyToBeDisabled}">
                           				 <td class="atg_commerce_csr_quatity"><input value="${itemQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}"  class="updateCartItemQty" disabled="disabled" onkeypress="return isNumberOnly(event);"/></br>
                           			</c:when>
                           			<c:otherwise>
                           				<td class="atg_commerce_csr_quatity"><input value="${itemQty}" type="text" size="3" maxlength="3" name="${itemCommerceItemId}" id="${itemCommerceItemId}"  class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/></br>
                           			</c:otherwise>	
                           		</c:choose>
                            <c:if test="${not empty skuItem.customerPurchaseLimit}">
        					 limit ${sku.customerPurchaseLimit} items per customer
         					 </c:if><input type="hidden" id="itemSkuId" value="${fn:escapeXml(catalogRefId)}"/></td>
                          </c:otherwise>
                        </c:choose>
                        
                        
                        <%-- Price Each --%>
                        <td class="atg_numberValue atg_commerce_csr_priceEach">
                       								 <c:choose>
																<c:when test="${displayStrikeThrough}">
																	<span
																		class="atg_commerce_csr_common_content_strikethrough">
																		<web-ui:formatNumber value="${price}" type="currency"
																			currencyCode="${currencyCode}" />
																	</span>
																	<span><web-ui:formatNumber value="${salePrice}"
																			type="currency" currencyCode="${currencyCode}" /></span>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${savedAmount > 0.0}">
																			<web-ui:formatNumber value="${salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:when>
																		<c:otherwise>
																			<web-ui:formatNumber value="${salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
														</td>
                        
                      
                        
                        <%-- Total Price --%>
                        <td class="atg_numberValue atg_commerce_csr_totalPrice"><web-ui:formatNumber value="${itemTotalAmount}" type="currency" currencyCode="${currencyCode}" /></td>
                        <%-- Final Price (price override) --%>
            
                        
                        <%-- Edit line item --%>
                        <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                          <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                          <dsp:param name="order" value="${order}" />
                          <dsp:oparam name="empty">
                          <c:choose>
                          <c:when test ="${envTools.siteAccessControlOn =='true' }">
                            <c:set var="siteId" value="${itemSiteId}"/>   
                            <dsp:droplet name="IsSiteAccessibleDroplet">
                              <dsp:param name="siteId" value="${siteId}"/>
                              <dsp:oparam name="true">
                                <td class="atg_iconCell"><dsp:tomap var="product" value="${productItem}" /> <c:if test="${fn:length(product.childSKUs) > 1}">
                              <svc-ui:frameworkPopupUrl var="popupUrl" value="/include/order/editProductSKU.jsp" context="${CSRConfigurator.contextRoot}" windowId="${windowId}" mode="apply" commerceItemId="${itemCommerceItemId}" />
                              <a class="atg_tableIcon atg_propertyEdit" title="<fmt:message key='cart.items.edit'/>" href="#"
                                onclick="atg.commerce.csr.common.showPopupWithReturn({
                                  popupPaneId: 'atg_commerce_csr_catalog_productQuickViewPopup',
                                  title: '${fn:escapeXml(sku.displayName)}',
                                  url: '${popupUrl}',
                                  onClose: function( args ) {
                                    if ( args.result == 'ok' ) {
                                      atgSubmitAction({
                                        panels : ['cmcShoppingCartP'],
                                        panelStack : 'cmcShoppingCartPS',
                                        form : document.getElementById('transformForm')
                                      });
                                    }
                                  }});return false;">
                              <fmt:message key="cart.items.edit" /> </a>
                            </c:if></td>
                              </dsp:oparam>
                              <dsp:oparam name="false">
                                <td class="atg_iconCell"/>
                              </dsp:oparam>
                            </dsp:droplet>
                          </c:when>
                          <c:otherwise>
                            <td class="atg_iconCell"><dsp:tomap var="product" value="${productItem}" /> <c:if test="${fn:length(product.childSKUs) > 1}">
                              <svc-ui:frameworkPopupUrl var="popupUrl" value="/include/order/editProductSKU.jsp" context="${CSRConfigurator.contextRoot}" windowId="${windowId}" mode="apply" commerceItemId="${item.id}" />
                              <a class="atg_tableIcon atg_propertyEdit" title="<fmt:message key='cart.items.edit'/>" href="#"
                                onclick="atg.commerce.csr.common.showPopupWithReturn({
                                  popupPaneId: 'atg_commerce_csr_catalog_productQuickViewPopup',
                                  title: '${fn:escapeXml(sku.displayName)}',
                                  url: '${popupUrl}',
                                  onClose: function( args ) {
                                    if ( args.result == 'ok' ) {
                                      atgSubmitAction({
                                        panels : ['cmcShoppingCartP'],
                                        panelStack : 'cmcShoppingCartPS',
                                        form : document.getElementById('transformForm')
                                      });
                                    }
                                  }});return false;">
                              <fmt:message key="cart.items.edit" /> </a>
                            </c:if></td>
                          </c:otherwise>
                        </c:choose>
                        </dsp:oparam>
                          <dsp:oparam name="output">
                            <td class="atg_iconCell"></td>
                          </dsp:oparam>
                        </dsp:droplet>
		
		<dsp:getvalueof var="fromMetaInfo" param="item.fromMetaInfo"></dsp:getvalueof>
                     
                     
                     
           
                     
                     	<dsp:droplet name="Switch">
					<dsp:param name="value" param="item.fromMetaInfo" />
					<dsp:oparam name="true">
								    <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('update','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
                        </td>
					</dsp:oparam>
					<dsp:oparam name="false">
						   <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('remove','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>Remove
					</dsp:oparam>
				</dsp:droplet>
                     
                <%--   		<c:choose>
						<c:when test="${not empty wrapSkuId}">
							    <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('update','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>
                        </td>
						</c:when><c:otherwise>
						    <td class="atg_iconCell">
                          <a class="atg_tableIcon atg_propertyClear" title="<fmt:message key='cart.items.delete'/>" href="#" onclick="removeItemFormCart('remove','<dsp:valueof param="item.wrapSkuId"/>','<dsp:valueof param="item.id"/>','<dsp:valueof param="item.wrapQuantity"/>','<dsp:valueof param="item.quantity"/>');return false;"> <fmt:message
                          key="cart.items.delete" /></a>
                        </td>
						</c:otherwise>
					</c:choose> --%>
                      </tr>
                      </c:if>
                   
                      <%-- Display breakdown if commerce item is associated with a giftlist --%>
                      <c:set var="customerTotalQuantity" value="${itemQty}" />
                      <c:set var="giftlistTotalQuantity" value="0" />
                      <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                        <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                        <dsp:param name="order" value="${order}" />
                        <dsp:oparam name="output">
                          <dsp:getvalueof var="key" param="key" />
                          <dsp:getvalueof var="value" param="element" />
                            <c:set var="customerTotalQuantity" value="${customerTotalQuantity - value }" />
                            <c:set var="giftlistTotalQuantity" value="${giftlistTotalQuantity + value }" />
                        </dsp:oparam>
                        <dsp:getvalueof var="numberOfGiftRecipients" param="size" />
                      </dsp:droplet><%-- End GiftlistShoppingCartQuantityDroplet --%>
                      <c:if test="${customerTotalQuantity > '0' && giftlistTotalQuantity > '0'}">
                        <tr>
                          <c:if test="${isMultiSiteEnabled == true}">
                           <td></td>
                           <td>
                          </c:if>
                          <c:if test="${isMultiSiteEnabled == false}">
                            <td>
                          </c:if>
                          <ul class="atg_commerce_csr_itemDesc">
                            <span class="atg-csc-in-store-sub-item"></span><li class="atg_commerce_csr_giftwishListName"><fmt:message key="cart.customer.currentCustomer.label" /><%-- Display Current Customer label --%></li>
                          </ul>
                          </td>
                          <td></td>
                          <td><c:out value="${customerTotalQuantity}" /><%-- Display Customer Quantity --%></td>
                        </tr>
                      </c:if>
            
                      
                      <%-- Multiple Gift Recipients detected, So disable Quantity box for this particular SKU --%>
                      <c:if test="${numberOfGiftRecipients > 1}">
<%--  <input value="${itemQty}" type="hidden" size="5" maxlength="5" name="${itemCommerceItemId}" id="hidden_${itemCommerceItemId}" /> --%>
                       
                      </c:if>
                      <%-- Gift Recipient along with current customer purchasing sku for themself, so disable Quantity box for this particular SKU --%>
                      <c:if test="${numberOfGiftRecipients > 0 && customerTotalQuantity > 0}">
                    <%--  <input value="${itemQty}" type="hidden" size="5" maxlength="5" name="${itemCommerceItemId}" id="hidden_${itemCommerceItemId}" /> --%>
                        
                      </c:if>
                      <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
                        <dsp:param name="commerceItemId" value="${itemCommerceItemId}" />
                        <dsp:param name="order" value="${order}" />
                        <dsp:oparam name="output">
                          <dsp:getvalueof var="key" param="key" />
                          <dsp:getvalueof var="value" param="element" />
                          <dsp:droplet name="/atg/commerce/gifts/GiftlistLookupDroplet">
                            <dsp:param name="id" param="key" />
                            <dsp:oparam name="output">
                              <dsp:setvalue paramvalue="element" param="giftlist" />
                              <tr>
                                <c:if test="${isMultiSiteEnabled == true}">
                                  <td></td>
                                  <td>
                                </c:if>
                                <c:if test="${isMultiSiteEnabled == false}">
                                  <td>
                                </c:if>
                                <ul class="atg_commerce_csr_itemDesc">
                                  <span class="atg-csc-in-store-sub-item"></span><li class="atg_commerce_csr_giftwishListName">
                                  <dsp:getvalueof var="eventName" vartype="java.lang.String" param="giftlist.eventName" />
                                  <a href="#" class="blueU" onclick="atg.commerce.csr.order.gift.giftlistBuyFrom('${key}');return false;">
                                  <dsp:valueof param="giftlist.owner.firstName" />&nbsp; 
                                  <dsp:valueof param="giftlist.owner.lastName" />, 
                                  <c:out value="${eventName}" /> 
                                  </a><%-- Display Gift Recipient Name and Event Name --%></li>
                                </ul>
                                </td>
                                <td></td>
                                <td><c:out value="${value}" /></td><%-- Display Gift Recipient Quantity --%>
                              </tr>
                            </dsp:oparam>
                          </dsp:droplet><%-- End GiftlistLookupDroplet --%>
                        </dsp:oparam>
                      </dsp:droplet><%-- End GiftlistShoppingCartQuantityDroplet --%>
                      
                      <!-- Added for Protection Paln: START -->
						<c:if test="${skuItem.bppEligible}">  
							<tr>
								<td></td>
								<td colspan="3"	class="atg_commerce_csr_shoppingCart_items_underline">
								<%-- 	<dsp:droplet name="CartItemDetailsDroplet">
										<dsp:param name="order" bean="ShoppingCart.current" />
										<dsp:oparam name="output"> --%>
										
											<dsp:include src="/include/gwp/buyersProtectionPlan.jsp" otherContext="${CSRConfigurator.truContextRoot}">
											<dsp:param name="pageName" value="cart" />
											<dsp:param name="multiShipping" value="false"/>
											<dsp:param name="item" param="item" />
											<dsp:param name="currentItem" value="${item.id}"/>
											</dsp:include>
									<%-- 		</dsp:oparam>
									</dsp:droplet> --%>
								</td>
							</tr>
						</c:if> 
					<!-- Added for Protection Paln: END -->	
					<%-- 	</c:when>
						<c:otherwise>
						<input type="hidden" value="${item.quantity}" type="text" size="5" maxlength="5" name="${item.id}" id="${item.id}" class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/><input type="hidden" id="itemSkuId" value="${fn:escapeXml(item.catalogRefId)}"/>
						</c:otherwise>
						
						 </c:choose>
						 --%>
						 <c:if test="${not empty wrapItemCommerceItemId}">
						 <input type="hidden" value="${wrapQuantity}" type="text" size="5" maxlength="5" name="${wrapItemCommerceItemId}" id="${wrapItemCommerceItemId}" class="updateCartItemQty" onkeypress="return isNumberOnly(event);"/><input type="hidden" id="itemSkuId" value="${fn:escapeXml(wrapSkuId)}"/>
						</c:if>
						<c:set var="commerceItemIdArray" value="${fn:split(itemId, ' ')}" />
               
                 </dsp:oparam>
                 </dsp:droplet>
                 
             
								 
                  </c:otherwise>
                </c:choose>
           
				<tr><td></td>
                <td colspan="3" class="atg_commerce_csr_shoppingCart_items_underline">
                  <%-- <dsp:include src="${selectGiftLinkFragment.URL}" otherContext="${selectGiftLinkFragment.servletContext}">
                  <dsp:param name="order" value="${order}" />
                  </dsp:include>--%>
                </td> </tr>
              </tbody>
            </table>
	
            <%-- Order Summary and Buttons --%>
            <div class="atg_commerce_csr_shoppingCartSummary">
              <div class="atg_commerce_csr_shoppingCartControls">
                <dsp:input type="hidden" value="ignored" priority="-10" id="modifyOrderSubmitter" bean="CartModifierFormHandler.setOrderByCommerceId" />
                <c:choose>
                  <c:when test="${shippingGroupCount > 1}">
                    <input type="button" id="updatePriceButton" class="multiShipping" value="<fmt:message key='cart.items.updatePrice'/>" onclick="updateCartAndCheckout('multiShippingCartUpdate'); return false;" />
                    <dsp:input type="hidden" value="ignored" priority="-10" id="moveToPurchaseInfoSubmitter" bean="CartModifierFormHandler.moveToPurchaseInfoByRelId" />
                    <input type="button" class="atg_commerce_csr_activeButton atg_commerce_csr_shippingButton abc" dojoType="atg.widget.validation.SubmitButton" form="itemsForm" name="checkoutFooterNextButton" id="checkoutFooterNextButton"
                      value="<fmt:message key="cart.continueToShipping"/>" onclick="updateCartAndCheckout('multiShippingCheckout'); return false;" />
                  </c:when>
                  <c:otherwise>
                    <input type="button" id="updatePriceButton" class="singleShipping" value="<fmt:message key='cart.items.updatePrice'/>" onclick="updateCartAndCheckout('singleShippingCartUpdate'); return false;" />
                    <dsp:input type="hidden" value="ignored" priority="-10" id="moveToPurchaseInfoSubmitter" bean="CartModifierFormHandler.moveToPurchaseInfoByCommerceId" />
                    <input type="button" class="atg_commerce_csr_activeButton atg_commerce_csr_shippingButton" dojoType="atg.widget.validation.SubmitButton" form="itemsForm" name="checkoutFooterNextButton" id="checkoutFooterNextButton"
                      value="<fmt:message key="cart.continueToShipping"/>" onclick="updateCartAndCheckout('singleShippingCheckout'); return false;" />
                  </c:otherwise>
                </c:choose>
              </div>
              <div class="atg_commerce_csr_orderSummary"><csr:cartDisplayOrderSummary order="${order}" isShowHeader="${false}" /></div>

            <script type="text/javascript">
            _container_.onLoadDeferred.addCallback( function() {
              atg.keyboard.registerFormDefaultEnterKey("itemsForm", "updatePriceButton");
            });
            _container_.onUnloadDeferred.addCallback( function() {
              atg.keyboard.unRegisterFormDefaultEnterKey("itemsForm");
            });
            function updateCartAndCheckout(btnReference){
            	//debugger;
           	 	  $(".error").remove();
            	  $('.updateCartItemQty').removeAttr('style');
            	  var quantityExceedError = false;
            	  var customerPurchaseLimitError = false;
            	  var outOfStockError = false;
            	  var skuId ='';
            	  var purchaseLimit = '';
            	  var stocklevel = 0;
            	  var noQtySelected = true;
            	  var qtyZeroError = false;
            	  $(".updateCartItemQty").each(function(){
            		 var skuid=$(this).parent('td').find("#itemSkuId").val();
             		 if(btnReference == 'multiShippingCartUpdate' || btnReference == 'multiShippingCheckout'){
             			var commerceItemid = $(this).attr("name");
	              		 var qtyInputLevel = 0;
	              		 $("[name='"+commerceItemid+"']").not(":hidden").each(function(){
	              			var thisInput = parseInt($(this).val());
           			 	qtyInputLevel =  parseInt(qtyInputLevel+thisInput);
           			 	return qtyInputLevel;
	              		 });
             		 }
             		 else
             			 {
             				var qtyInputLevel = parseInt($(this).val());
             			 }
      				 var skuStockLevel = parseInt($("#stockLevel_"+skuid).val());
      				 var customerpurchaselimit = parseInt($("#customerPurchaseLimit_"+skuid).val());
      				 var stockStatus = $("#inventoryStatus_"+skuid).val();
      				 if($.trim($(this).val()) != '')
             			 {
             			 	noQtySelected = false;
             			 }
             		 if(parseInt($.trim($(this).val())) == 0 || $(this).val() == "")
	        			 {
	            			 qtyZeroError = true;
	            			 $(this).css({"border-color":"red"});
	        			 }
             		 else if(stockStatus == 'outofstock')
	             		 {
             				outOfStockError = true;
             				skuId = skuid;
     			 			stocklevel = skuStockLevel
     			 			$(this).css({"border-color":"red"});
	             		 }
             		else if(qtyInputLevel > skuStockLevel)
             			 {
     			 			quantityExceedError = true;
     			 			skuId = skuid;
     			 			stocklevel = skuStockLevel
     			 			$(this).css({"border-color":"red"});
             			 }
             		 else if(customerpurchaselimit > 0 && qtyInputLevel > customerpurchaselimit)
             			 {
             			 	customerPurchaseLimitError = true;
             			 	skuId = skuid;
             			 	purchaseLimit = customerpurchaselimit;
             			 	$(this).css({"border-color":"red"});
             			 }
             	  });
             	  if(outOfStockError || quantityExceedError || customerPurchaseLimitError || noQtySelected || qtyZeroError)
             		  {
	             		if(outOfStockError)
		      		  		{
		      		  			$("#itemsForm").find(".atg_commerce_csr_innerTable").prepend('<tr class="error"><td colspan="6"><span style="color:red">'+outOfStockMsg+'</td></tr>');
		      		  		}	
	             		else if(quantityExceedError)
             		  		{
             		  			$("#itemsForm").find(".atg_commerce_csr_innerTable").prepend('<tr class="error"><td colspan="6"><span style="color:red">'+qtyExceedMsg+'Stock Level For '+skuId+' is '+stocklevel+'</td></tr>');
             		  		}
             		  	else if(customerPurchaseLimitError)
             		  		{
             		  			$("#itemsForm").find(".atg_commerce_csr_innerTable").prepend('<tr class="error"><td colspan="6"><span style="color:red">'+customerPurchaseLimit+' <b style="color:black;">'+skuId+'</b> '+is+' <b style="color:black;">'+purchaseLimit+'</b> please select quantity accordingly</span></td></tr>');
             		  		}
             		  	else if(noQtySelected)
           		  		{
             		  			$("#itemsForm").find(".atg_commerce_csr_innerTable").prepend('<tr class="error"><td colspan="6"><span style="color:red">'+selectSkuQty+'</span></td></tr>');
           		  			
           		  		}
             		  	else if(qtyZeroError)
        		  		{
             		  		$("#itemsForm").find(".atg_commerce_csr_innerTable").prepend('<tr class="error"><td colspan="6"><span style="color:red">'+qtyZeroMsg+'</span></td></tr>');
        		  			
        		  		}
             		  	return false;
             		  }
             	  if(btnReference == 'multiShippingCartUpdate')
	        		  {
	              		atg.commerce.csr.cart.setOrderByRelationshipIdForm();
	
	        		  }
             	  if(btnReference == 'singleShippingCartUpdate')
             		  {
             			atg.commerce.csr.cart.updatePrice();
             		  }
             	  if(btnReference == 'singleShippingCheckout' || btnReference == 'multiShippingCheckout')
             		  {
             			atg.commerce.csr.cart.submitNextAction();
             		  }
           }
            function isNumberOnly(evt){
            	evt = (evt) ? evt : window.event;
            	var charCode = (evt.which) ? evt.which : evt.keyCode;
            	 if (navigator.userAgent.indexOf("Firefox") > 0) {
            	    	if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
            	    		return true;
            	        	}
            	    		return false;
            	    	}
            	    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
            	    	 return true;
            	    }
            	   
            	    return false;
            }
            
          </script>
          
                    <script type="text/javascript">
                    function removeItemFormCart(actions,giftWrapSkuId,relationShipId,wrapItemQuantity,relItemQtyInCart){
                    	
                    	if(actions=='remove'){
                    	var form = document.getElementById('removeItemFormCartForm');
                    	var relationShipIdArr = [];
                    	var arrayval = new Array(relationShipId);
                    	 form.removalRelationshipIds.value = new Array(relationShipId);
                    	form.relationShipId.value = relationShipId;
                    	form.giftWrapSkuId.value = giftWrapSkuId;
                    	form.desiredQTY.value = relItemQtyInCart;
                    	
                    	form.wrapItemQuantity.value = wrapItemQuantity;
                    	/* form.commerceItemId.value = commerceItemId; */
                    	atgSubmitAction({
                    		 form : form,
                    		 panels : ['cmcShoppingCartP'],
                             panelStack : 'cmcShoppingCartPS'
                             });
                    	}else if(actions=='update'){
                    		var form = document.getElementById('setRelationshipIdForm');
                    		form.relationShipId.value = relationShipId;
                        	form.giftWrapSkuId.value = giftWrapSkuId;
                        	form.wrapItemQuantity.value = wrapItemQuantity;
                        	form.desiredQTY.value = relItemQtyInCart;
                        	atgSubmitAction({
                        		
                       		 panels : ['cmcShoppingCartP'],
                                panelStack : 'cmcShoppingCartPS',
                                	form : form
                                });
                    	}
                    }
                    </script> 
          </dsp:form>
        </c:otherwise>
      </c:choose>

      <c:set var="displayOrderModDiv" value="${true}"/>
      <%-- if we are doing an exchange and there are no promos in the exchange order, don't display the div --%>
      <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnActive">
      <dsp:oparam name="true">
        <dsp:droplet name="/atg/commerce/custsvc/promotion/PromotionViewDroplet">
        <dsp:param name="byType" value="${false}"/>
        <dsp:param name="order" value="${cart.current}"/>
        <dsp:oparam name="empty">
            <c:set var="displayOrderModDiv" value="${false}"/>
        </dsp:oparam>      
        </dsp:droplet>
      </dsp:oparam>      
      </dsp:droplet>

      <c:if test="${displayOrderModDiv}">
      <%-- Order Modification Container --%>
      <div class="atg_commerce_csr_orderModifications">
      
      
      <%-- Promotions Container --%>
        
         <div class="atg_commerce_csr_promotionsListing">
        
        <%-- Promotions Title and Browse Link --%>
         <h4><fmt:message key="cart.promotions" />
       <%-- Show promotions browser if order not exchange/return, not submitted and agent has privilege --%> 
        <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnActive">
          <dsp:oparam name="false">
            <dsp:droplet name="/atg/commerce/custsvc/order/IsOrderSubmitted">
              <dsp:oparam name="false">
                <dsp:droplet name="HasAccessRight">
                  <dsp:param name="accessRight" value="commerce-custsvs-browse-promotions-privilege" />
                  <dsp:oparam name="accessGranted">
                    <c:set var="isShowPromotionsBrowser" value="${true}"/>
                  <%--   <a class="atg_commerce_csr_promoBrowser" href="#" onclick="atg.commerce.csr.promotion.openPromotionsBrowser(${walletGridConfig.gridWidgetId}_refreshSearchResults)"><fmt:message key="cart.promotions.browsePromotions" /></a> --%>
                  </dsp:oparam>
                </dsp:droplet>
              </dsp:oparam>
            </dsp:droplet>
          </dsp:oparam>
        </dsp:droplet></h4>



        <%-- List of Promotions --%>
        <div class="atg_commerce_csr_promotionsListingInternal"><dsp:include src="/include/order/promotionQualificationSummary.jsp" otherContext="${CSRConfigurator.truContextRoot}">
          <dsp:param name="order" value="${order}" />
        </dsp:include></div>
  
        <%-- Enter Coupon Code --%>
      <%--     <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnActive">
            <dsp:oparam name="false">
              <dsp:form method="post" id="couponForm" formid="couponForm">
                <ul class="atg_dataForm atg_commerce_csr_enterPromoForm">
                  <li><dsp:input type="hidden" value="${thisUrl}" bean="CouponFormHandler.claimCouponSuccessURL" /> <dsp:input type="hidden" value="${thisUrl}" bean="CouponFormHandler.claimCouponErrorURL" /> <fmt:message var="enterClaimCodeMsg"
                    key="cart.promotions.enterClaimCode" /> <dsp:input type="text" id="couponCode" size="26" value="${fn:escapeXml(enterClaimCodeMsg)}"
                    onclick="atg.commerce.csr.common.setIfValue(
                        'couponCode','${fn:escapeXml(enterClaimCodeMsg)}','')"
                    onblur="atg.commerce.csr.common.setIfValue(
                        'couponCode','','${fn:escapeXml(enterClaimCodeMsg)}')" bean="CouponFormHandler.couponClaimCode" /> <dsp:input type="hidden" value="dummy"
                    bean="CouponFormHandler.claimCoupon" /></li>
                  <li>
                    <input type="submit" value="<fmt:message key='cart.promotions.addCoupon'/>"/>
                   </li>
                </ul>
              </dsp:form>
              <script type="text/javascript">
              document.forms["couponForm"].onsubmit = function() {
                atgSubmitAction({
                        panels : ['cmcShoppingCartP'],
                        panelStack : 'cmcShoppingCartPS',
                        form : document.getElementById('couponForm')});
                return false;
              };
            </script>
            </dsp:oparam>
          </dsp:droplet>  --%>
  
        <%-- Closing DIV for Promotions Container --%>
        </div>
        
        <%-- Site Access Control to display or disable Appeasment section --%>
        
        
     

      <%-- Closing DIV for Order Modifications Container --%>
      </div>
      </c:if> <%-- end if promo container should be included --%>

    <div style="display: none;" id="atg_commerce_csr_catalog_addToCartContainer"></div>
    </dsp:layeredBundle>
    <%-- These variables are used in validation script --%>
    <dsp:getvalueof var="minAdjAmount" bean="ManualAdjustmentsFormHandler.minimumAdjustmentAmount" />
    <dsp:getvalueof var="maxAdjAmount" bean="ManualAdjustmentsFormHandler.maximumAdjustmentAmount" />
    <script type="text/javascript">
      atg.progress.update('cmcShoppingCartPS');
      atg.commerce.csr.cart.isInRange = function() {
        var maxl = parseInt(this.maxlength);
        var minl = parseInt(this.minlength);
        var len = this.getValue().length;
        var inrange = len >= (isNaN(minl) ? 0 : minl) && len <= (isNaN(maxl) ? Infinity : maxl);
        if (inrange == true) {
          dojo.byId("adjustmentSubmitButton").disabled = false;
        }
        else {
          dojo.byId("adjustmentSubmitButton").disabled = true;
        }
        return inrange;
      }
      var validateAdjustmentsForm = function () {
        var disable = false;
        var amount = dijit.byId("amountTxt").getValue();
        // amount must be number in range form minAdjAmount to maxAdjAmount
        if (isNaN(amount) || amount < ${minAdjAmount} || amount > ${maxAdjAmount})  disable = true;  
        dojo.byId("adjustmentsForm").adjustmentSubmitButton.disabled = disable;
      }
      _container_.onLoadDeferred.addCallback(function () {
        var comments = dijit.byId("commentsTxt");
        if (comments) {
          comments.isInRange = atg.commerce.csr.cart.isInRange;
        }

        var theButton = dojo.byId("checkoutFooterNextButton");
        if (theButton != null) {
          theButton.focus();
        }

        // wrap long lines in Adjustment Comments
        dojo.query("div.atg_commerce_csr_orderAdjustmentComment").forEach(function(commentEl){
            commentEl.innerHTML = commentEl.innerHTML.replace(/([^\s]{10})(?=[^\s]{2,})/g,"$1<wbr/>");
        });
        
        // validation of adjustmentsForm.
        validateAdjustmentsForm();
        atg.service.form.watchInputs('adjustmentsForm', validateAdjustmentsForm);
        
      });
      _container_.onUnloadDeferred.addCallback(function() {
        atg.service.form.unWatchInputs('adjustmentsForm');
      });
      
      <c:if test="${!empty isShowPromotionsBrowser && isShowPromotionsBrowser == true}"
        ><dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources"
          ><dsp:droplet name="SharingSitesDroplet">
          <dsp:oparam name="output"/
            ><dsp:getvalueof var="sites" param="sites"
          /></dsp:oparam
        ></dsp:droplet
        >atg.commerce.csr.promotion.sites = [];
        atg.commerce.csr.promotion.sites.push({value:'',name:'<fmt:message key="promotion.allSites"/>'});
        <c:forEach var="siteConfig" items="${sites}"
          ><dsp:tomap var="siteConfigMap" value="${siteConfig}"
          />atg.commerce.csr.promotion.sites.push({value:'${siteConfigMap.id}',name:'${siteConfigMap.name}'});
        </c:forEach
        ><c:remove var="isShowPromotionsBrowser"
      /></dsp:layeredBundle></c:if>
      
   /*    $(document).on('keyup','.updateCartItemQty',function(){
  		var $this = $(this);
  		var id = $this.attr('id');
  		var stockLimit = parseInt($(document).find("#stockLevel_"+id).val());
  		if(parseInt($this.val()) >  stockLimit){
  			$this.val(stockLimit);
  		}
  	});   */
  	
    </script>
     	<dsp:form formid="removeItemFormCartForm" id="removeItemFormCartForm">
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.relationShipId" name="relationShipId" id="relationShipId" value="" />
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.removalRelationshipIds" name="removalRelationshipIds" id="removalRelationshipIds" />
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.giftWrapSkuId" name ="giftWrapSkuId" id="giftWrapSkuId" value=""/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.desiredQTYByRelationshipId" name ="desiredQTY" id="desiredQTY" value=""/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.wrapItemQuantity" name ="wrapItemQuantity" id="wrapItemQuantity" value=""/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.removeItemFromOrderByRelationshipId" value="true"/>
                      </dsp:form> 
                      
                            	<dsp:form formid="setRelationshipIdForm" id="setRelationshipIdForm">
                            	<dsp:input type="hidden" bean="CartModifierFormHandler.relationShipId" name="relationShipId" id="relationShipId" value="" />
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.splitItemRemove" value="true" />	             
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.giftWrapSkuId" name ="giftWrapSkuId" id="giftWrapSkuId"/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.desiredQTYByRelationshipId" name ="desiredQTY" id="desiredQTY" value=""/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.wrapItemQuantity" name ="wrapItemQuantity" id="wrapItemQuantity"/>
	                      	<dsp:input type="hidden" bean="CartModifierFormHandler.setOrderByRelationshipId" value="true"/>
                      </dsp:form> 
    				<dsp:form id="giftItemForm" formid="giftItemForm">
                        <dsp:input type="hidden" bean="GiftOptionsFormHandler.shipGroupId" name="ShipGroupId" id="ShipGroupId" />
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.commerceItemRelationshipId" name="CommerceItemRelationshipId" id="CommerceItemRelationshipId"/>
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.commerceItemId" name="CommerceItemId" id="CommerceItemId" />
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.quantity" name="Quantity" id="Quantity"/>
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftWrapProductId" name="GiftWrapProductId" id="GiftWrapProductId"/>
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftWrapSkuId" name="GiftWrapSkuId" id="GiftWrapSkuId"/>
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.giftItem" name="GiftItem" id="GiftItem"/>
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.currentPage" value="cart" />
						<dsp:input type="hidden" bean="GiftOptionsFormHandler.updateGiftItem" value="true" />
			 </dsp:form>
  </dsp:page>
</c:catch>
                
<c:if test="${exception != null}">
  ${exception}
  <%
    Exception ee = (Exception) pageContext.getAttribute("exception");
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/cart.jsp#3 $$Change: 883241 $--%>