<%--
Display the buttons that are needed by finishOrder.jsp

Expected params
currentOrder : The order.
includeForm : A boolean flag.
              These buttons and forms are repeated in the same page and the form repetition is causing the bug 152772.
              In order to avoid this problem, the form repetition is avoided by passing the includeForm flag.

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/finishOrderButtons.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@  include file="/include/top.jspf"%>
<c:catch var="exception">

<dsp:page xml="true">
  
  <dsp:importbean var="urlDroplet" bean="/atg/svc/droplet/FrameworkUrlDroplet" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/CommitOrderFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CancelOrderFormHandler"/>
  <dsp:importbean var="CSRConfigurator" bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsOrderIncomplete"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsOrderSubmitted"/>
  <dsp:importbean bean="/atg/commerce/custsvc/returns/ReturnFormHandler"/>
  <dsp:getvalueof var="agentLogin" bean="/atg/agent/userprofiling/AgentProfileFormHandler.value.login"/>
  
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <dsp:getvalueof var="order" param="currentOrder"/>    
  <dsp:getvalueof var="includeForm" param="includeForm"/>    
   
  <c:set var="submitButtonName">
    <dsp:droplet name="IsOrderIncomplete">
      <dsp:oparam name="true">
        <fmt:message key='common.submit.order'/>
      </dsp:oparam>
      <dsp:oparam name="false">
        <fmt:message key='common.update'/>
      </dsp:oparam>
    </dsp:droplet>
  </c:set>
  <textarea cols="60" rows="20" id="field_d1" name="field_d1"  hidden="true"></textarea>
  
  <%-- Check if we are dealing with an Exchange--%>
    <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnActive">
     <dsp:oparam name="true">
      <dsp:getvalueof var="processName" param="returnRequest.processName"/>
        <c:choose>
          <%-- Dealing with an Exchange--%>
          <c:when test="${processName == 'Exchange'}">
            <%-- Create success and error urls for Submit Exchange --%>
            <svc-ui:frameworkUrl var="submitSuccessURL" panelStacks="cmcConfirmReturnPS"/>
            <svc-ui:frameworkUrl var="submitErrorURL"/>
            <dsp:layeredBundle basename="atg.commerce.csr.returns.WebAppResources">
              <fmt:message key='common.submitExchange' var="submitButtonName"/>
            </dsp:layeredBundle>
            <csr:displayCheckoutPanelFooter
            nextIconOnclickURL="atg.commerce.csr.order.finish.submitExchange(); return false;"            
            cancelActionErrorURL="${cancelErrorURL}"
            order="${order}"
            includeForm="${includeForm}"
            submitActionButtonName="${submitButtonName}"
           />
                       
            <%-- Exchange form --%>          
            <c:if test="${includeForm == true}">

            <dsp:form 
              id="atg_commerce_csr_submitExchangeForm"
              formid="atg_commerce_csr_submitExchangeForm">          
              
              <dsp:input type="hidden" priority="-10" value=""
                         name="handleSubmitExchange" 
                         bean="ReturnFormHandler.confirmReturn" />
              <dsp:input type="hidden" value="${submitErrorURL }"
                bean="ReturnFormHandler.confirmReturnErrorURL" />          
              <dsp:input type="hidden" value="${submitSuccessURL }"
                bean="ReturnFormHandler.confirmReturnSuccessURL" />                            
            </dsp:form>
            </c:if>            
          </c:when>
         </c:choose>
      </dsp:oparam>
      <dsp:oparam name="false">
        <%-- Not dealing with an Exchange. Must be a New or Existing order!--%>                 
          <svc-ui:frameworkUrl var="processTemplateSuccessURL" panelStacks="cmcScheduleCreatePS"/>
          <svc-ui:frameworkUrl var="processTemplateErrorURL"/>
          <svc-ui:frameworkUrl var="submitAndScheduleSuccessURL" panelStacks="cmcConfirmOrderWithSchedulePS"/>
          <svc-ui:frameworkUrl var="submitSuccessURL" panelStacks="cmcConfirmOrderPS"/>
          <svc-ui:frameworkUrl var="submitErrorURL"/>
          
          <%-- If the order is Incomplete then just submit it normally. --%>
          <dsp:droplet name="IsOrderIncomplete">
            <dsp:oparam name="true">      
              <csr:displayCheckoutPanelFooter
                nextIconOnclickURL="atg.commerce.csr.order.finish.submitOrder('aaatg_commerce_csr_finishOrderSubmitForm'); return false;"
                submitAndScheduleOnclickURL="atg.commerce.csr.order.finish.submitAndScehduleOrder('atg_commerce_csr_finishOrderSubmitAndScheduleForm'); return false;"
                scheduleOnclickURL="atg.commerce.csr.order.finish.scheduleOrder('atg_commerce_csr_scheduleOrderForm'); return false;"
                cancelActionErrorURL="${cancelErrorURL}"
                order="${order}"
                includeForm="${includeForm}"
                submitActionButtonName="${submitButtonName}"
               />       
                          

              <%-- Schedule Order Form --%>
              <c:if test="${includeForm == true}">
              
              <dsp:form style="display:none" id="atg_commerce_csr_scheduleOrderForm"
                formid="atg_commerce_csr_scheduleOrderForm">          
                
                <dsp:input type="hidden" priority="-10" value=""
                  bean="CommitOrderFormHandler.processTemplate" />

                <dsp:input type="hidden" value="${processTemplateErrorURL }"
                  bean="CommitOrderFormHandler.processTemplateErrorURL" />
            
                <dsp:input type="hidden" value="${processTemplateSuccessURL }"
                  bean="CommitOrderFormHandler.processTemplateSuccessURL" />      
              </dsp:form> <%-- End Schedule Order Form --%>          
              
              
              <%-- Finish Order Submit Form --%>
              <dsp:form style="display:none" id="atg_commerce_csr_finishOrderSubmitAndScheduleForm"
                formid="atg_commerce_csr_finishOrderSubmitAndScheduleForm">          
                
                <dsp:input type="hidden" priority="-10" value=""
                  bean="CommitOrderFormHandler.commitOrder" />
                  
                  <dsp:input  type="hidden" 
                 bean="CommitOrderFormHandler.email" value="${order.email}"/>
            	
                <dsp:input type="hidden" value="${order.id}" 
                  bean="CommitOrderFormHandler.orderId" />
                
                <dsp:input type="hidden" value="true" name="scheduleOrder" 
                  bean="CommitOrderFormHandler.createTemplateFromSubmittedOrder" />
                  
                <dsp:input type="hidden" value="NEW_ORDER" 
                  bean="CommitOrderFormHandler.templateToUse" />
            
                <dsp:input type="hidden" value="${submitErrorURL }"
                  bean="CommitOrderFormHandler.commitOrderErrorURL" />
            
                <dsp:input type="hidden" value="${submitAndScheduleSuccessURL }"
                  bean="CommitOrderFormHandler.commitOrderSuccessURL" />      
              </dsp:form> <%-- End Finish Order Submit Form --%>          
              <%-- Finish Order Submit Form --%>
			 </c:if>
              <dsp:form id="aaatg_commerce_csr_finishOrderSubmitForm"
                formid="aaatg_commerce_csr_finishOrderSubmitForm">  
                <!-- Start - Logic to set paymentGroupType --> 
               <c:forEach items="${order.paymentGroups}" var="paymentGroup" varStatus="paymentGroupIndex">
                     <c:set var="paymentGroupTypeWasSet" value="false"/>
                     <c:if test="${!paymentGroupTypeWasSet}">
                            <c:choose>
                              <c:when test="${paymentGroup.paymentGroupClassType eq 'inStorePayment'}">
                                         <dsp:input name="paymentGroupType" type="hidden" bean="CommitOrderFormHandler.paymentGroupType" value="${paymentGroup.paymentGroupClassType}"/>
                                         <c:set var="paymentGroupTypeWasSet" value="true"/>                              
                             </c:when>
                             <c:when test="${paymentGroup.paymentGroupClassType eq 'creditCard'}">
                             
	                                 <dsp:input name="paymentGroupType" type="hidden" bean="CommitOrderFormHandler.paymentGroupType" value="${paymentGroup.paymentGroupClassType}"/>
	                                 <c:set var="paymentGroupTypeWasSet" value="true"/>  
	                           <%--         <dsp:input type="hidden"  id="firstName_commit" name="firstName_commit"
							                 value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.firstName}"
							                  bean="CommitOrderFormHandler.addressInputFields.firstName" />                  
							                  
											  <dsp:input type="hidden"  id="lastName_commit" name="lastName_commit" 
											  value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.lastName}"
							                  
											  bean="CommitOrderFormHandler.addressInputFields.lastName" />
							                  <dsp:input type="hidden"  id="address1_commit" name="address1" 
											  value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.address1}"
											  
							                  bean="CommitOrderFormHandler.addressInputFields.address1" />
							                  <dsp:input type="hidden"  id="address2_commit" name="address2_commit" 
											  value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.address2}"
							                  bean="CommitOrderFormHandler.addressInputFields.address2" />
							                  				  
							                  <dsp:input type="hidden"  id="postalCode_commit" name="postalCode_commit" 
							                  value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.postalCode}"
							                  bean="CommitOrderFormHandler.addressInputFields.postalCode" />
											  
							                    <dsp:input type="hidden"  id="city_commit" name="city_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.city}"
							                  bean="CommitOrderFormHandler.addressInputFields.city" />
											  
							                    <dsp:input type="hidden"  id="state_commit" name="state_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.state}"
							                  bean="CommitOrderFormHandler.addressInputFields.state" />
											  
							                    <dsp:input type="hidden" id="phoneNumber_commit" name="phoneNumber_commit"  
							                    value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.phoneNumber}"
							                  bean="CommitOrderFormHandler.addressInputFields.phoneNumber" />
							                  
							                    <dsp:input type="hidden"  id="country_commit" name="country_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].billingAddress.country}"
							                  bean="CommitOrderFormHandler.addressInputFields.country" />
											  
							                  <dsp:input type="hidden" id="email_commit" name="email_commit" value="accept@email.com"
							                  bean="CommitOrderFormHandler.addressInputFields.email" />
							                  
							                   <dsp:input type="hidden"  id="nameOnCard_commit" name="nameOnCard_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].nameOnCard}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.nameOnCard" />
							                  
							                   <dsp:input type="hidden"  id="expirationMonth_commit" name="expirationMonth_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].expirationMonth}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.expirationMonth" />
							                   
							                   <dsp:input type="hidden"  id="expirationYear_commit" name="expirationYear_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].expirationYear}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.expirationYear" />
							                  
							                   <dsp:input type="hidden"  id="creditCardToken_commit" name="creditCardToken_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].creditCardNumber}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.creditCardToken" />
							                  
							                       <dsp:input type="hidden"  id="creditCardNumber_commit" name="creditCardNumber_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].creditCardNumber}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.creditCardNumber" />
							                  
							                  <dsp:input type="hidden"  id="creditCardType_commit" name="creditCardType_commit" 
												value="${order.paymentGroups[paymentGroupIndex.index].creditCardType}"
							                  bean="CommitOrderFormHandler.creditCardInfoMap.creditCardType" />  --%>                                             
                                       </c:when>
                           </c:choose> 
                    </c:if>
                </c:forEach>
                <!-- end - Logic to set paymentGroupType -->   
                        
                
                <dsp:input type="hidden" priority="-10" value=""
                  bean="CommitOrderFormHandler.commitOrder" />
                 
               
                 <dsp:input id="emailInReview" name="emailInReview" type="hidden" 
                 bean="CommitOrderFormHandler.email" value="${order.email}"/>                                                      
                   
                 <dsp:input id="rewardNumberReviewTest" name="rewardNumberReviewTest" type="hidden" 
                 bean="CommitOrderFormHandler.rewardNumber" value="${order.rewardNumber}"/> 
                 
                 <dsp:input id="deviceID" name="deviceID" type="hidden" 
                 bean="CommitOrderFormHandler.deviceID"/> 
                 
                 <dsp:input type="hidden" value="${agentLogin}"
                 bean="CommitOrderFormHandler.enteredBy" />
                 
                <dsp:input type="hidden" value="${order.id}" 
                  bean="CommitOrderFormHandler.orderId" />
                
                <dsp:input type="hidden" value="NEW_ORDER" 
                  bean="CommitOrderFormHandler.templateToUse" />
            
                <dsp:input type="hidden" value="${submitErrorURL }"
                  bean="CommitOrderFormHandler.commitOrderErrorURL" />
            
                <dsp:input type="hidden" value="${submitSuccessURL }"
                  bean="CommitOrderFormHandler.commitOrderSuccessURL" />      
              </dsp:form> <%-- End Finish Order Submit Form --%>     
                   
           
            </dsp:oparam>
            <dsp:oparam name="false">
              <%-- The order is not Incomplete it must be Submitted so 
              reconcile it. --%>        
              <csr:displayCheckoutPanelFooter
              nextIconOnclickURL="atg.commerce.csr.order.finish.submitOrder('atg_commerce_csr_existingOrderReconcileForm'); return false;"
              cancelActionErrorURL="${cancelErrorURL}"
              order="${order}"
              includeForm="${includeForm}"
              submitActionButtonName="${submitButtonName}"
             />
              
              <svc-ui:frameworkUrl var="concurrentUpdateErrorURL" panelStacks="cmcExistingOrderPS,globalPanels"/>
            <c:if test="${includeForm == true}">
              <%-- Existing Order Reconcile Form --%>
              <dsp:form style="display:none" id="atg_commerce_csr_existingOrderReconcileForm"
                formid="atg_commerce_csr_existingOrderReconcileForm">
                <dsp:input type="hidden" priority="-10" value="${concurrentUpdateErrorURL}" 
                  bean="CommitOrderFormHandler.concurrentUpdateErrorURL" />
                
                <dsp:input type="hidden" priority="-10" value=""
                  bean="CommitOrderFormHandler.commitOrderUpdates" />
            
                <dsp:input type="hidden" value="${order.id}" 
                  bean="CommitOrderFormHandler.orderId" />
                  
                <dsp:input type="hidden" value="ORDER_UPDATE" 
                  bean="CommitOrderFormHandler.templateToUse" />
            
                <dsp:input type="hidden" value="${submitErrorURL }"
                  bean="CommitOrderFormHandler.commitOrderUpdatesErrorURL" />
            
                <dsp:input type="hidden" value="${submitSuccessURL }"
                  bean="CommitOrderFormHandler.commitOrderUpdatesSuccessURL" />          
              </dsp:form>  <%-- End Existing Order Reconcile Form --%> 
              </c:if>                          
            </dsp:oparam>
          </dsp:droplet> <%-- End IsOrderIncomplete --%>   
      </dsp:oparam>
    </dsp:droplet>    
  </dsp:layeredBundle>
</dsp:page>


  <script type="text/javascript">
  atg.commerce.csr.order.finish.submitOrder = function (theForm){
		  var theForm = dojo.byId("aaatg_commerce_csr_finishOrderSubmitForm");
		  callingFraudJsCollector();
		  var deviceID = $("#field_d1").val();
		deviceID = deviceID.substring(0, 4000);
		  var rewardNumber=dojo.byId("enterMembershipIDInReview").value;
		  /* var firstName=dojo.byId("firstName_commit").value;
		  var lastName=dojo.byId("lastName_commit").value;
		  var address1=dojo.byId("address1_commit").value;
		  var address2=dojo.byId("address1_commit").value;
		  var postalCode=dojo.byId("postalCode_commit").value;
		  var city=dojo.byId("city_commit").value;
		  var state=dojo.byId("state_commit").value;
		  var phoneNumber=dojo.byId("phoneNumber_commit").value;
		  var country=dojo.byId("country_commit").value;
		  var email=dojo.byId("email_commit").value;

		  var nameOnCard=dojo.byId("nameOnCard_commit").value;
		  var expirationMonth=dojo.byId("expirationMonth_commit").value;
		  var expirationYear=dojo.byId("expirationYear_commit").value;
		  var creditCardToken=dojo.byId("creditCardToken_commit").value;
		  var creditCardNumber=dojo.byId("creditCardNumber_commit").value;
		  var creditCardType=dojo.byId("creditCardType_commit").value; */
		  
		 
		  theForm.rewardNumberReviewTest.value=rewardNumber;
		  theForm.deviceID.value=deviceID;
		  /* theForm.firstName_commit.value=firstName;
		  theForm.lastName_commit.value=lastName;
		  theForm.address1_commit.value=address1;
		  theForm.address2_commit.value=address2;
		  theForm.postalCode_commit.value=postalCode;
		  theForm.city_commit.value=city;
		  theForm.state_commit.value=state;
		  theForm.phoneNumber_commit.value=phoneNumber;
		  theForm.country_commit.value=country;
		  theForm.email_commit.value=email;
		  theForm.nameOnCard_commit.value=nameOnCard;
		  theForm.expirationMonth_commit.value=expirationMonth;
		  theForm.expirationYear_commit.value=expirationYear;
		  theForm.creditCardToken_commit.value=creditCardToken;
		  theForm.creditCardNumber_commit.value=creditCardNumber;
		  theForm.creditCardType_commit.value=creditCardType; */
		  atgSubmitAction({
		    form:theForm,
		    panelStack: ["globalPanels"]
		    });
		};
  </script>
  	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/info.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/line.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/script_util.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/user_data.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/util_form.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/details.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/plan_data.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/mode.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/user.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/service.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/form_functions.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/util.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/session_d.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/condition.js"></script>
	<script type="text/javascript" src="${CSRConfigurator.truContextRoot}/script/fraud-js-collector/scripts.js"></script>	
		
  <script>
  function callingFraudJsCollector() {
		switch(Math.floor(Math.random()*15 + 1))
		{
		case 1:
			utild.pdfunction('field_d1');
			break;
		case 2:
			form_v.load_d('field_d1');
			break;
		case 3:
			data1.val('field_d1');
			break;
		case 4:
			de1.extract('field_d1');
			break;
		case 5:
			udata.cut('field_d1');
			break;
		case 6:
			detspc.bundle('field_d1');
			break;
		case 7:
			plan.compile('field_d1');
			break;
		case 8:
			util.fill('field_d1');
			break;
		case 9:
			js.validate('field_d1');
			break;
		case 10:
			element.parse_data('field_d1');
			break;
		case 11:
			dt1.val('field_d1');
			break;
		case 12:
			forms.data_parse('field_d1');
			break;
		case 13:
			form_space.extractData('field_d1');
			break;
		case 14:
			javascript.verify('field_d1');
			break;
		case 15:
			value.payload('field_d1');
			break;
		}
		return false;
	}
  </script>

</c:catch>
<c:if test="${exception != null}">
  <c:out value="${exception}"/>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/finishOrderButtons.jsp#1 $$Change: 875535 $--%>
