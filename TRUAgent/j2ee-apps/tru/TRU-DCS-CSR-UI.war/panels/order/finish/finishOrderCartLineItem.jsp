<%--
Display the appropriate details for each commerce item in the cart.

Expected params
commerceItem : The commerce item.
commerceItemIndex : Used to render alternate rows in the table in different styles.
currencyCode : The order.priceInfo.currencyCode value.

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/finishOrderCartLineItem.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@  include file="/include/top.jspf"%>
<c:catch var="exception">
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <dsp:importbean bean="/com/tru/commerce/cart/droplet/CartItemDetailsDroplet" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" />
  <dsp:importbean bean="/atg/commerce/custsvc/util/AltColor"/>
  <dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsOrderIncomplete"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsHighlightedState"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CommerceItemStateDescriptions"/>
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
  <dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRValidatePreSellableDroplet"/>
  <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
  <dsp:getvalueof var="isExistingOrderView" param="isExistingOrderView"/>
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" var="CSRConfigurator"/>
  <dsp:getvalueof var="item" param="commerceItem"/>
  <dsp:getvalueof var="order" param="order"/>
	  <dsp:getvalueof var="currencyCode" param="currencyCode"/>
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
  <dsp:droplet name="AltColor">
    <dsp:param name="value" param="commerceItemIndex"/>
    <dsp:oparam name="odd">
      <tr class="atg_dataTable_altRow">
    </dsp:oparam>
    <dsp:oparam name="even">
      <tr>
    </dsp:oparam>
  </dsp:droplet>
  <style type="text/css">
  .shopping-cart-gift-image {
	width: 25px;
	height: 25px;
	background-image: url('/TRU-DCS-CSR/images/cart-sprite-2.png');
	background-position: 0px -180px;
	vertical-align: middle;
   }
   .protection-plan{position:relative;}
   .atg_dataTable .atg_numberValue{
    			text-align: left;
    		}
    		.atg table.atg_commerce_csr_innerTable th{
    			text-align: left;
    			padding-left: 8px;
    		}
  </style>
  <script type="text/javascript">
    if (!dijit.byId("productQuickViewPopup")) {
      new dojox.Dialog({ id: "productQuickViewPopup",
                         cacheContent: "false",
                         executeScripts: "true",
                         scriptHasHooks: "true",
                         duration: 100,
                         "class": "atg_commerce_csr_popup"});
    }
    /* BPP code changes - start */
    function addUpdateBppItem(relationShipId, bppInfoId, bppSkuId, bppItemId,  pageName) {
    	$("#bppSkuId").val(bppSkuId);
  	  	$("#bppInfoId").val(bppInfoId);
  	  	$("#bppItemId").val(bppItemId);
  	  	$("#relationShipId").val(relationShipId);
  	  	$("#pageName").val(pageName);
  	  if(pageName == "cart"){
  	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});
  	  }
  	if(pageName == "Review"){
  	  	dojo.xhrPost({form:document.getElementById('addUpdateBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});
  	  }
  	 	   }
    function removeBppItem(relationShipId, bppInfoId, pageName) {
  	   $("#relShipId").val(relationShipId);
  	  	$("#bppInfoId").val(bppInfoId);
  	  	$("#pName").val(pageName);
  	  if(pageName == "cart"){
    	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
   			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
	        		 }
			},mimetype:"text/html"});
    	  }
    	if(pageName == "Review"){
    	  	dojo.xhrPost({form:document.getElementById('removeBppItemForm'),url:"/TRU-DCS-CSR/include/gwp/buyersProtectionPlan.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
   			if(!(_1f instanceof Error)){
	     				atgNavigate({panelStack:"cmcCompleteOrderPS", queryParams: { init : 'true' }});return false;
	        		 }
			},mimetype:"text/html"});
    	  }
  	  	
  	    	 }
    function selectProtectionPlan(e,obj){	  
    	  
   	   var e = e || window.event;
   	   var checkbox = $(obj);
   	   var pageName = $(obj).attr('data-pageName');
   	   if (checkbox.is(":checked")) {		  
   		  var relId = $(obj).attr('data-relId');
   		  var bppSkuId = $(obj).attr('data-bppSkuId');
   		  var bppItem = $(obj).attr('data-bppItemId');
   	  	  var bppItemCount = $(obj).attr('data-bppItemCount');
   	  	  var pageName=$(obj).attr('data-pageName');
   		  var bppItemId;		  
   		  $('.bppPlanMultiShippingPage').attr('disabled', false);
   		  if(!isEmpty(bppItemCount) && parseInt(bppItemCount) == 1) { 
   			  bppItemId = $(obj).attr('data-bppItemId'); 
   			  addUpdateBppItem(relId, '', bppSkuId, bppItemId, pageName, obj);
   		  }		  
   	  }else{
   		    var pageName=$(obj).attr('data-pageName');
   			var bppItemInfoId = $(obj).attr('data-bppInfoId');
   			var relId = $(obj).attr('data-relId');
   			if(!isEmpty(bppItemInfoId)) {
   				removeBppItem(relId, bppItemInfoId, pageName, obj);
   			}
   	  }
   	}

  </script>
  
  
  			  

  
  
  <dsp:droplet name="CartItemDetailsDroplet">
				<dsp:param name="order" value="${order}" />
				<dsp:param name="isCSCRequest" value="true"/>
				<dsp:oparam name="output">
				
					<dsp:getvalueof var="items" param="items" />
				</dsp:oparam>
			</dsp:droplet>
	  <dsp:importbean var="container" bean="/atg/commerce/custsvc/order/ShippingGroupContainerService"/>		
  <dsp:getvalueof var="commerceItemShippingInfos" bean="ShippingGroupContainerService.allCommerceItemShippingInfos"/>
 
			<c:forEach items="${items}" var="cartItem">
			 <c:if test="${cartItem.donationAmount == null}">
			  <c:if test="${isMultiSiteEnabled == true}">
			    <c:set var="siteId" value="${cartItem.siteId}"/>
			    <td class="atg_commerce_csr_siteIcon">
			      <csr:siteIcon siteId="${siteId}" />
			    </td>
			  </c:if>
			  <td>  
		  <dsp:form id="addUpdateBppItemForm" formid="addUpdateBppItemForm">
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppSkuId"
							name="bppSkuId" id="bppSkuId" />
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppInfoId"
							name="bppInfoId" id="bppInfoId" />
						<dsp:input type="hidden" bean="CartModifierFormHandler.bppItemId"
							name="bppItemId" id="bppItemId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.relationShipId"
							name="relationShipId" id="relationShipId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.pageName"
							name="pageName" id="pageName"/>
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.addUpdateBppItem" value="true" />
					</dsp:form>

					<dsp:form id="removeBppItemForm" formid="removeBppItemForm">
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.relationShipId"
							name="relationShipId" id="relShipId" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.pageName"
							name="pName" id="pName" />
						<dsp:input type="hidden"
							bean="CartModifierFormHandler.removeBPPItemRelationship"
							value="true" />
					</dsp:form>

      <ul class="atg_commerce_csr_itemDesc">
        <li>
        
          <dsp:tomap var="sku" value="${cartItem.skuItem}"/>
          <dsp:getvalueof var="itemRelQty" value="${cartItem.relItemQtyInCart}"/>
		  <dsp:getvalueof var="itemMetaQty" value="${cartItem.quantity}"/>
          <c:choose>
            <c:when test="${(isMultiSiteEnabled == true) && (isSiteDeleted != true)}">
              <svc-ui:frameworkPopupUrl var="commerceItemPopup"
                value="/include/order/product/productReadOnly.jsp"
                siteId="${siteId}"
                context="${CSRConfigurator.contextRoot}"
                windowId="${windowId}"
                productId="${cartItem.productId}"/>
            <a title="<fmt:message key='cart.items.quickView'/>"
                href="#" onclick="atg.commerce.csr.common.showPopupWithReturn({
                  popupPaneId: 'productQuickViewPopup',
                  title: '<c:out value="${fn:escapeXml(sku.displayName)}"/>',
                  url: '${commerceItemPopup}',
                  onClose: function( args ) { }} );return false;">
                ${fn:escapeXml(sku.displayName)}
              </a>
            </c:when>
            <c:otherwise>
              <svc-ui:frameworkPopupUrl var="commerceItemPopup"
                value="/include/order/product/productReadOnly.jsp"
                siteId="${siteId}"
                context="${CSRConfigurator.contextRoot}"
                windowId="${windowId}"
                productId="${cartItem.productId}"/>
              <a title="<fmt:message key='cart.items.quickView'/>"
                href="#" onclick="atg.commerce.csr.common.showPopupWithReturn({
                  popupPaneId: 'productQuickViewPopup',
                  title: '<c:out value="${fn:escapeXml(sku.displayName)}"/>',
                  url: '${commerceItemPopup}',
                  onClose: function( args ) {  }} );return false;">
                ${fn:escapeXml(sku.displayName)}
              </a>
            </c:otherwise>
          </c:choose>  
        </li>
        <li>
          <c:out value="${cartItem.skuId}"/>
        </li>
        <li>
        	<c:if test="${not empty cartItem.wrapDisplayName}">
				<dsp:getvalueof var="giftWrapName" value="${cartItem.wrapDisplayName}"></dsp:getvalueof>
				<div class="shopping-cart-gift-image gift inline"></div>
				<fmt:message key="shoppingcart.gift.with.label" bundle="${TRUCustomResources}">  </fmt:message>
						With ${giftWrapName}
			</c:if>  
        </li>
        
        <c:if test="${sku.bppEligible}"> 
        <li class="buyersProtection-plan">
       
														<dsp:include src="/include/gwp/buyersProtectionPlan.jsp" otherContext="${CSRConfigurator.truContextRoot}">
														<dsp:param name="pageName" value="Review" />
														<dsp:param name="item" value="${cartItem}" />
														<dsp:param name="multiShipping" value="true"/>
														</dsp:include>
											</li>
											</c:if>
											
											<dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
													<dsp:param name="productId" value="${sku.onlinePID}" />
													<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="productPageUrl"
															param="productPageUrl" />
													</dsp:oparam>
												</dsp:droplet>



											<input type="hidden" id="itemProductId_${sku.id}" value="${cartItem.productId}">
											<input type="hidden" id="catalogRefId_${sku.id}" value="${sku.id}">
											<input type="hidden" id="currentSiteId_${sku.id}" value="${siteId}">
											<input type="hidden" id="productPageUrl_${sku.id}" value="${productPageUrl}">
											<input type="hidden" id="skuDisplayName_${sku.id}" value="${sku.displayName}">
											<input type="hidden" id="description_${sku.id}" value="${sku.description}">
											
											
											
											
      </ul>
    </td>
    <td>
      <dsp:droplet name="IsOrderIncomplete">
        <dsp:param name="order" value="${order}"/>
        <c:set var="locationIdSupplied" value="false"/>
        <dsp:oparam name="true">
        <c:if test="${not empty cartItem.locationId}">
             <c:set var="locationIdSupplied" value="true"/>
        </c:if>
        <dsp:droplet name="TRUCSRValidatePreSellableDroplet">
							<dsp:param name="sku" value="${cartItem.skuItem}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="preSellable" param="preSellable" />
								<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
						</dsp:droplet>
          <csr:inventoryStatus locationIdSupplied="${locationIdSupplied}" formattedDate="${formattedDate}" preSellable="${preSellable}" locationId="${cartItem.locationId}" commerceItemId="${cartItem.skuId}"/>
        </dsp:oparam>
        <dsp:oparam name="false">
          <dsp:droplet name="CommerceItemStateDescriptions">
            <dsp:param name="state" value="${item.stateAsString}"/>
            <dsp:param name="elementName" value="stateDescription"/>
            <dsp:oparam name="output">
              <dsp:droplet name="IsHighlightedState">
                <dsp:param name="obj" value="${item}"/>
                <dsp:oparam name="true">
                  <span class="atg_commerce_csr_dataHighlight"><dsp:valueof param="stateDescription"></dsp:valueof></span>
                </dsp:oparam>        
                <dsp:oparam name="false">
                  <dsp:valueof param="stateDescription"></dsp:valueof>
                </dsp:oparam>        
              </dsp:droplet>
            </dsp:oparam>
          </dsp:droplet>
        </dsp:oparam>
      </dsp:droplet>
    </td>
    <td class="atg_numberValue">  
      <%-- Always add the returnedQuantity. This will show the correct data
      for items with returns but won't affect new orders. --%>  
      <c:set var="itemQty" value="1"/>
      <c:choose>
			 <c:when test="${not empty itemMetaQty}">
				<c:set var="itemQty" value="${itemMetaQty}"/>
			</c:when>
			<c:otherwise>
				<c:set var="itemQty" value="${itemRelQty}"/>
			</c:otherwise>
	  </c:choose>
    	${itemQty}
    </td>
    <td class="atg_numberValue">
        <c:choose>
																<c:when test="${cartItem.displayStrikeThrough}">
																	<span
																		class="atg_commerce_csr_common_content_strikethrough">
																		<web-ui:formatNumber value="${cartItem.price}" type="currency"
																			currencyCode="${currencyCode}" />
																	</span>
																	<span><web-ui:formatNumber value="${cartItem.salePrice}"
																			type="currency" currencyCode="${currencyCode}" /></span>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${cartItem.savedAmount > 0.0}">
																			<web-ui:formatNumber value="${cartItem.salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:when>
																		<c:otherwise>
																			<web-ui:formatNumber value="${cartItem.salePrice}"
																				type="currency" currencyCode="${currencyCode}" />
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
    </td>
    <td class="atg_numberValue">
      <%-- <c:set var="pi" value="${item.priceInfo}"/>
      <c:if test="${pi.rawTotalPrice != item.totalAmount}">
        <span class="atg_commerce_csr_common_content_strikethrough">
         <web-ui:formatNumber value="${pi.rawTotalPrice}" type="currency" currencyCode="${currencyCode}"/>
        </span>
        &nbsp;
      </c:if> --%>
      <web-ui:formatNumber value="${cartItem.totalAmount}" type="currency" currencyCode="${currencyCode}"/>
    </td>
    <td class="atg_numberValue atg_commerce_csr_priceOveride">
      <c:choose>
        <c:when test="${item.priceInfo.amountIsFinal}" >
          <web-ui:formatNumber value="${item.totalAmount}" type="currency" currencyCode="${currencyCode}"/>
        </c:when>
        <c:otherwise>
          <c:out value="" />
        </c:otherwise>
      </c:choose>
    </td>
  </tr>
  <%
    /*
     * Display breakdown if commerce item is associated with a giftlist
     */
  %>
  <c:set var="customerTotalQuantity" value="${item.relItemQtyInCart}" />
  <c:set var="giftlistTotalQuantity" value="0" />
  <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
    <dsp:param name="commerceItemId" value="${cartItem.commerceItemId}" />
    <dsp:param name="order" value="${order}"/>
    <dsp:oparam name="output">
      <dsp:getvalueof var="key" param="key" />
      <dsp:getvalueof var="value" param="element" />
      <dsp:droplet name="/atg/commerce/gifts/GiftlistLookupDroplet">
        <dsp:param name="id" param="key" />
        <dsp:oparam name="output">
          <dsp:setvalue paramvalue="element" param="giftlist" />
          <c:set var="customerTotalQuantity" value="${customerTotalQuantity - value }" />
          <c:set var="giftlistTotalQuantity" value="${giftlistTotalQuantity + value }" />
        </dsp:oparam>
      </dsp:droplet><%-- End GiftlistLookupDroplet --%>
    </dsp:oparam>
  </dsp:droplet><%-- End GiftlistShoppingCartQuantityDroplet --%>
  <c:if test="${customerTotalQuantity > '0' && giftlistTotalQuantity > '0'}">
    <tr>
      <c:if test="${isMultiSiteEnabled == true}">
        <td></td>
        <td>
      </c:if>
      <c:if test="${isMultiSiteEnabled == false}">
        <td>
      </c:if>
        <ul class="atg_commerce_csr_itemDesc">
          <li class="atg_commerce_csr_giftwishListName">
            <fmt:message key="shoppingCartSummary.currentCustomer.label"/><%-- Display Current Customer label --%>
          </li>
        </ul>
      </td>
      <td></td>
      <td class="atg_numberValue"><c:out value="${customerTotalQuantity}" /><%-- Display Customer Quantity --%>
      </td>
    </tr>
  </c:if>
  <dsp:droplet name="/atg/commerce/gifts/GiftlistShoppingCartQuantityDroplet">
    <dsp:param name="commerceItemId" value="${cartItem.commerceItemId}" />
    <dsp:param name="order" value="${order}"/>
    <dsp:oparam name="output">
      <tr>
        <dsp:getvalueof var="key" param="key" />
        <dsp:getvalueof var="value" param="element" />
        <dsp:droplet name="/atg/commerce/gifts/GiftlistLookupDroplet">
          <dsp:param name="id" param="key" />
          <dsp:oparam name="output">
          <c:if test="${isMultiSiteEnabled == true}">
            <td></td>
            <td>
          </c:if>
          <c:if test="${isMultiSiteEnabled == false}">
            <td>
          </c:if>
              <ul class="atg_commerce_csr_itemDesc">
                <li class="atg_commerce_csr_giftwishListName">
                <dsp:setvalue paramvalue="element" param="giftlist" /> 
                <dsp:getvalueof var="eventName" vartype="java.lang.String" param="giftlist.eventName" />
                <dsp:valueof param="giftlist.owner.firstName" />&nbsp; 
                <dsp:valueof param="giftlist.owner.lastName" />, <c:out value="${eventName}" /><%-- Display Gift Recipient Name and Event Name --%>
                </li>
              </ul>
            </td>
            <td></td>
            <td class="atg_numberValue">
              <c:out value="${value}" /><%-- Display Gift Recipient Quantity --%>
            </td>
          </dsp:oparam>
        </dsp:droplet><%-- End GiftlistLookupDroplet --%>
      </tr>
    </dsp:oparam>
  </dsp:droplet>
  </c:if>
  
   <c:if test="${cartItem.donationAmount != null}">
			  <c:if test="${isMultiSiteEnabled == true}">
			    <c:set var="siteId" value="${cartItem.siteId}"/>
			    <td class="atg_commerce_csr_siteIcon">
			      <csr:siteIcon siteId="${siteId}" />
			    </td>
			  </c:if>
			  <td>  



      <ul class="atg_commerce_csr_itemDesc">
        <li>

		<fmt:message key="cart.toysFor.tots" bundle="${TRUCustomResources}"/>
           
        </li>
        <li>
                
        </li>
        <li>
		</li>
      </ul>
    </td>
    <td>
   <fmt:message key="cart.thank.you" bundle="${TRUCustomResources}"/>
    </td>
    <td class="atg_numberValue">  
    
    </td>
    <td class="atg_numberValue">

    </td>
    <td class="atg_numberValue">
    	<web-ui:formatNumber value="${cartItem.donationAmount}"	type="currency" currencyCode="${currencyCode}" />
      
    </td>
    <!-- <td class="atg_numberValue atg_commerce_csr_priceOveride">
  
    </td> -->
  </tr>
 

  
  </c:if>
  
  <%-- End GiftlistShoppingCartQuantityDroplet --%>
 </c:forEach>

 </dsp:layeredBundle>
</dsp:page>
</c:catch>
<c:if test="${exception != null}">
  <c:out value="${exception}"/>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/finishOrderCartLineItem.jsp#1 $$Change: 875535 $--%>