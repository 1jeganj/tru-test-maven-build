<%--
Display details of the shopping cart. Use the 
finishOrderCartLineItem.jsp to render each commerce item.

Expected params
currentOrder : The order that the shopping cart details are retrieved from.

@version $Id: 
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@  include file="/include/top.jspf"%>
<c:catch var="exception">
<dsp:page xml="true">

  <dsp:importbean bean="/atg/commerce/custsvc/order/GetTotalAppeasementsForOrderDroplet"/>

  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  
  <dsp:getvalueof var="order" param="currentOrder"/>

  <dsp:getvalueof var="isExistingOrderView" param="isExistingOrderView"/>
  
  <c:if test="${empty isExistingOrderView}">
    <c:set var="isExistingOrderView" value="false" />
  </c:if>

  <csr:renderer name="/atg/commerce/custsvc/ui/renderers/ItemDescription">
      <jsp:attribute name="setPageData">
      </jsp:attribute>
      <jsp:body>
        <dsp:include src="${renderInfo.url}" otherContext="${renderInfo.contextRoot}">
          <dsp:param name="currentOrder" value="${order}"/>
          <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
        </dsp:include>
      </jsp:body>
    </csr:renderer>
<div class="atg_commerce_csr_orderReview">
  <%-- Display Promotions --%> 
  
  <div class="atg_commerce_csr_orderModifications">
  <dsp:include src="/include/order/promotionsSummary.jsp" otherContext="${CSRConfigurator.contextRoot}">
    <dsp:param name="order" value="${order}" />
  </dsp:include>
  </div>
  
  <%-- Display Pricing Summary --%> 
  <csr:displayOrderSummary order="${order}" isExistingOrder="${isExistingOrderView}" isShowHeader="false"/>
  <csr:getCurrencyCode order="${order}">
   <c:set var="currencyCode" value="${currencyCode}" scope="request" />
  </csr:getCurrencyCode> 
  
    <dsp:droplet name="GetTotalAppeasementsForOrderDroplet">
      <dsp:param name="order" value="${order}"/>
      <dsp:param name="elementName" value="totalAppeasements"/>
      <dsp:oparam name="output">
        <dsp:getvalueof var="totalAppeasements" param="totalAppeasements"/>
        <div class="atg_commerce_csr_appeasementListing">
          <span class="atg_svc_fieldTitle"><strong><fmt:message key='view.order.appeasementTotal'/></strong></span> 
          <span class="plainText"><web-ui:formatNumber value="${totalAppeasements}" type="currency" currencyCode="${currencyCode}"/></span>
          
        </div>
      </dsp:oparam>
    </dsp:droplet>
  </div>
  
  </dsp:layeredBundle>
</dsp:page>

</c:catch>
<c:if test="${exception != null}">
  <c:out value="${exception}"/>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/shoppingCartSummary.jsp#1 $$Change: 875535 $--%>
