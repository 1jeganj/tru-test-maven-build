<%--
Display details of all the shipping groups in the order. Use the
finishOrderBillingLineItem.jsp to render each line item.

Expected params
currentOrder : The order that the payment group details are retrieved from.

@version $Id:
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsHighlightedState"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupStateDescriptions"/>

  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
    <dsp:getvalueof var="order" param="currentOrder"/>
    <dsp:getvalueof var="isExistingOrderView" param="isExistingOrderView"/>
	<c:set var="inStorePickupShippingGroupCount" value="${0}"/>
	<c:set var="hardgoodShippingGroupCount" value="${0}"/>
	
	
	<c:forEach items="${order.shippingGroups}"
                   var="shippingGroup" varStatus="shippingGroupIndex">
     <c:if test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
      <c:set var="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount + 1}"/>
    </c:if>
    <c:if test="${shippingGroup.shippingGroupClassType == 'hardgoodShippingGroup'}">
      <c:set var="hardgoodShippingGroupCount" value="${hardgoodShippingGroupCount + 1}"/>
    </c:if>
    </c:forEach>
    
    <c:set var="shippingGroupCount" value="${inStorePickupShippingGroupCount+hardgoodShippingGroupCount}"/>
    
   
    <c:if test="${empty isExistingOrderView}">
      <c:set var="isExistingOrderView" value="false"/>
    </c:if>
    <%-- Show Shipping Status if viewing an existing order --%>
    <c:choose>
      <c:when test="${order.shippingGroupCount == 1}">
        <c:forEach items="${order.shippingGroups}"
                   var="shippingGroup" varStatus="shippingGroupIndex">
					
					<dsp:include src="/include/order/shippingGroupReadView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
            <dsp:param name="shippingGroup" value="${shippingGroup}"/>
            <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
          </dsp:include>
				<c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup' && not empty shippingGroup.giftWrapMessage}">
			     	 <div style="width: 432px;word-break: break-word;float: left;font-family: inherit;">
  					 <span style="font-size: 12px;">Gift Message</span><br/><p style="margin:0;padding:0;word-wrap: break-word;">${shippingGroup.giftWrapMessage}</p>
  				 </div>
				</c:if>
        </c:forEach>
      </c:when>
      <c:when test="${order.shippingGroupCount > 1}">
        <c:forEach items="${order.shippingGroups}"
                   var="shippingGroup" varStatus="shippingGroupIndex">
       <c:choose>
       <c:when test="${hardgoodShippingGroupCount == 1 && inStorePickupShippingGroupCount != 0}">
       <c:forEach items="${shippingGroup.commerceItemRelationships}"
                      var="ciRelationship" varStatus="ciIndex">
        <c:if test="${ciRelationship.commerceItem.commerceItemClassType != 'donationCommerceItem'}">
          <fieldset>
            <legend>
              <fmt:message key='shippingSummary.shipment.header'/>

              <c:out value="${shippingGroupIndex.count}"/>
            </legend>
            <dsp:include src="/include/order/shippingGroupReadView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="shippingGroup" value="${shippingGroup}"/>
              <dsp:param name="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
              <c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup' && not empty shippingGroup.giftWrapMessage}">
  				 <div style="width: 432px;word-break: break-word;float: right;font-family: inherit;">
  					 <span style="font-size: 12px;">Gift Message</span><br/><p style="margin:0;padding:0;word-wrap: break-word;">${shippingGroup.giftWrapMessage}</p>
  				 </div>
   			</c:if>
            <dsp:include src="/include/order/displayCommerceItem.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="currentOrder" value="${order}"/>
              <dsp:param name="currentShippingGroup" value="${shippingGroup}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
            </br>
          
            
          </fieldset>
          </c:if>
          </c:forEach>
          </c:when>
          
        
          
                 <c:when test="${hardgoodShippingGroupCount > 1 && inStorePickupShippingGroupCount != 0}">
          <fieldset>
            <legend>
              <fmt:message key='shippingSummary.shipment.header'/>

              <c:out value="${shippingGroupIndex.count}"/>
            </legend>
            <dsp:include src="/include/order/shippingGroupReadView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="shippingGroup" value="${shippingGroup}"/>
              <dsp:param name="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
              <c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup' && not empty shippingGroup.giftWrapMessage}">
  				 <div style="width: 432px;word-break: break-word;float: right;font-family: inherit;">
  					 <span style="font-size: 12px;">Gift Message</span><br/><p style="margin:0;padding:0;word-wrap: break-word;">${shippingGroup.giftWrapMessage}</p>
  				 </div>
   			</c:if>
            <dsp:include src="/include/order/displayCommerceItem.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="currentOrder" value="${order}"/>
              <dsp:param name="currentShippingGroup" value="${shippingGroup}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
            </br>
          
            
          </fieldset>
         
          </c:when>
          
                    <c:when test="${hardgoodShippingGroupCount > 1}">
          <fieldset>
            <legend>
              <fmt:message key='shippingSummary.shipment.header'/>

              <c:out value="${shippingGroupIndex.count}"/>
            </legend>
            <dsp:include src="/include/order/shippingGroupReadView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="shippingGroup" value="${shippingGroup}"/>
              <dsp:param name="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
              <c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup' && not empty shippingGroup.giftWrapMessage}">
  				 <div style="width: 432px;word-break: break-word;float: right;font-family: inherit;">
  					 <span style="font-size: 12px;">Gift Message</span><br/><p style="margin:0;padding:0;word-wrap: break-word;">${shippingGroup.giftWrapMessage}</p>
  				 </div>
   			</c:if>
            <dsp:include src="/include/order/displayCommerceItem.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="currentOrder" value="${order}"/>
              <dsp:param name="currentShippingGroup" value="${shippingGroup}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
            </br>
          
            
          </fieldset>
         
          </c:when>
          
            
           <c:when test="${inStorePickupShippingGroupCount > 1}">
       <c:forEach items="${shippingGroup.commerceItemRelationships}"
                      var="ciRelationship" varStatus="ciIndex">
        <c:if test="${ciRelationship.commerceItem.commerceItemClassType != 'donationCommerceItem'}">
          <fieldset>
            <legend>
              <fmt:message key='shippingSummary.shipment.header'/>

              <c:out value="${shippingGroupIndex.count}"/>
            </legend>
            <dsp:include src="/include/order/shippingGroupReadView.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="shippingGroup" value="${shippingGroup}"/>
              <dsp:param name="inStorePickupShippingGroupCount" value="${inStorePickupShippingGroupCount}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
              <c:if test="${shippingGroup.shippingGroupClassType ne 'inStorePickupShippingGroup' && not empty shippingGroup.giftWrapMessage}">
  				 <div style="width: 432px;word-break: break-word;float: right;font-family: inherit;">
  					 <span style="font-size: 12px;">Gift Message</span><br/><p style="margin:0;padding:0;word-wrap: break-word;">${shippingGroup.giftWrapMessage}</p>
  				 </div>
   			</c:if>
            <dsp:include src="/include/order/displayCommerceItem.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="currentOrder" value="${order}"/>
              <dsp:param name="currentShippingGroup" value="${shippingGroup}"/>
              <dsp:param name="isExistingOrderView" value="${isExistingOrderView}"/>
            </dsp:include>
            </br>
          
            
          </fieldset>
          </c:if>
          </c:forEach>
          </c:when>
          
          
          </c:choose>
          </c:forEach>
      </c:when>
    </c:choose>
  </dsp:layeredBundle>

</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/order/finish/shippingSummary.jsp#1 $$Change: 875535 $--%>
