<%--
 In-Store Pickup Locations
 This popup displays a list of in-store pickup locations, based on a proximity search
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/pickupLocations.jsp#2 $$Change: 875744 $
 @updated $DateTime: 2014/03/17 09:46:19 $$Author: jsiddaga $
--%>

<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">

  <dsp:getvalueof var="productId" param="productId"/>
  <dsp:getvalueof var="skuId" param="skuId"/>
  <dsp:getvalueof var="quantity" param="quantity"/>
  <dsp:getvalueof var="state" param="state" />
  <dsp:getvalueof var="pinCode" param="pinCode" />
  <dsp:getvalueof var="city" param="city" />
  <dsp:getvalueof var="country" param="country" />
  
  <dsp:importbean bean="/atg/commerce/locations/RQLStoreLookupDroplet"/>
  <dsp:importbean bean="/atg/commerce/inventory/InventoryLookup"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler"/>
  
  <dsp:include src="/include/catalog/pickupLocations.jsp" otherContext="${CSRConfigurator.truContextRoot}">
    <dsp:param name="productId" value="${productId}" />
    <dsp:param name="skuId" value="${skuId}" />
    <dsp:param name="quantity" value="${quantity}" />
  </dsp:include>
  
  <svc-ui:frameworkUrl var="url" splitChar="|"  panelStacks="${renderInfo.pageOptions.successPanelStacks}" /> 
  <dsp:form method="post" id="inStorePickupForm" name="inStorePickupForm">
  <dsp:input type="hidden" value="pickupStore" bean="CartModifierFormHandler.pageName"/>
    <dsp:input type="hidden" value="${url}" bean="CartModifierFormHandler.addItemToOrderSuccessURL" />
    <dsp:input type="hidden" value="${url}" bean="CartModifierFormHandler.addItemToOrderErrorURL" />
    <dsp:input type="hidden" value="${productId}" name="productId" bean="CartModifierFormHandler.productId" />
    <dsp:input type="hidden" value="${skuId}" name="skuId" bean="CartModifierFormHandler.catalogRefIds" /> 
    <dsp:input type="hidden" value="" name="locationId" bean="CartModifierFormHandler.locationId"/>
    <dsp:input type="hidden" value="${quantity}" name="quantity" bean="CartModifierFormHandler.quantity"/>
    <dsp:input type="hidden" value="dummy" priority="-100" bean="CartModifierFormHandler.addItemToOrder"/>
    <input id="atg.successMessage" name="atg.successMessage" type="hidden" value=""/> 
  </dsp:form>
  <div id="inStorePickupResults">
  </div>

</dsp:layeredBundle>

</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/pickupLocations.jsp#2 $$Change: 875744 $--%>