<%--
 This page encodes the search results as JSON
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/addProductByIdOnProduct.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf"%>

<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"
		var="productLookup" />
	<dsp:importbean bean="/atg/commerce/catalog/SKULookup" />
	<dsp:importbean bean="/atg/dynamo/servlet/RequestLocale" />
	<dsp:importbean bean="/atg/commerce/inventory/InventoryLookup" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
	<dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRValidatePreSellableDroplet"/>
	 <dsp:importbean bean="/com/tru/commerce/droplet/TRUCSRPriceDroplet" />
	<dsp:importbean
		bean="/atg/commerce/custsvc/catalog/GetSKUParentProductDroplet" />
	<dsp:importbean var="agentTools"
		bean="/atg/commerce/custsvc/util/CSRAgentTools" />
	<dsp:importbean
		bean="/atg/dynamo/droplet/multisite/SharingSitesDroplet" />
	<dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem" />
	<dsp:importbean bean="/atg/dynamo/droplet/multisite/GetSiteDroplet" />
	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
	<dsp:importbean bean="/atg/multisite/SiteContext" />
	<dsp:getvalueof var="currentSite" bean="SiteContext.site" />
	<dsp:importbean var="site1" bean="/atg/multisite/Site" />
	<c:if test="${empty isMultisiteEnabled}">
	  <c:set var="isMultisiteEnabled" value="${agentTools.multiSiteEnabled}" />
	</c:if>
	<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
		<dsp:getvalueof param="productId" var="productId" />
		<dsp:getvalueof param="skuId" var="skuId" />

		<dsp:droplet name="SharingSitesDroplet">
			<dsp:oparam name="output">
				<dsp:getvalueof var="sites" param="sites" />
			</dsp:oparam>
		</dsp:droplet>

		<csr:getCurrencyCode>
			<c:set var="currencyCode" value="${currencyCode}" scope="request" />
		</csr:getCurrencyCode>

		<c:if test="${!empty productId}">
			<dsp:droplet name="ProductLookup">
				<dsp:param bean="RequestLocale.locale" name="repositoryKey" />
				<dsp:param name="id" param="productId" />
				<dsp:param name="sites" value="${sites}" />
				<dsp:param name="elementName" value="product" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="productItem" param="product" />
					<dsp:tomap var="product" param="product" />
					<dsp:getvalueof param="product.childSkus" var="productChildSkus" />
				</dsp:oparam>
			</dsp:droplet>
		</c:if>

		<c:if test="${!empty skuId}">
			<dsp:droplet name="SKULookup">
				<dsp:param bean="RequestLocale.locale" name="repositoryKey" />
				<dsp:param name="id" param="skuId" />
				<dsp:param name="sites" value="${sites}" />
				<dsp:param name="elementName" value="sku" />
				<dsp:oparam name="output">
					<dsp:getvalueof param="sku" var="skuItem" />
					<dsp:tomap var="sku" param="sku" />
					<dsp:droplet name="GetSKUParentProductDroplet">
						<dsp:param name="skuId" value="${sku.id}" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="productItem" param="product" />
							<dsp:getvalueof param="product" var="product" />
							<dsp:tomap var="product" param="product" />
							<dsp:getvalueof value="${product.childSkus}"
								var="productChildSkus" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
		<c:if test="${not empty product}">
			<dsp:getvalueof value="${fn:length(productChildSkus)}" var="skuCount" />
			<json:object prettyPrint="${UIConfig.prettyPrintResponses}">
				<json:property name="productId" value="${product.id}" />
				<c:choose>
					<c:when test="${!empty skuId}">
						<json:property name="skuCount" value="1" />
					</c:when>
					<c:otherwise>
						<json:property name="skuCount" value="${skuCount}" />
					</c:otherwise>
				</c:choose>
				<json:property name="productName" value="${product.displayName}" />
				<c:if test="${isMultisiteEnabled}">		
				  <dsp:droplet name="SiteIdForCatalogItem">
					<dsp:param name="item" value="${productItem}" />
					<dsp:param name="currentSiteFirst" value="true" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="productSiteId" param="siteId" />
						<dsp:droplet name="GetSiteDroplet">
							<dsp:param name="siteId" value="${productSiteId}" />
							<dsp:oparam name="output">
								<dsp:getvalueof param="site" var="site" />
								<dsp:tomap value="${site}" var="site" />
								<dsp:getvalueof var="siteIconURL" param="site.favicon" />
								<dsp:getvalueof var="siteIconHover" param="site.name" />
								<c:if test="${empty siteIconURL}">
									<c:set var="siteIconURL"
										value="${CSRConfigurator.defaultSiteIconURL}" />
								</c:if>
							</dsp:oparam>

							<dsp:oparam name="empty">
								<c:if test="${!empty currentSite}">
         						<dsp:tomap var="currentSiteMap" value="${currentSite}"/>
									<c:set var="siteIconURL" value="${currentSiteMap.favicon}" />
									<c:set var="siteIconHover" value="${currentSiteMap.name}" />
								</c:if>

								<c:if test="${empty siteIconURL}">
									<c:set var="siteIconURL"
										value="${CSRConfigurator.defaultSiteIconURL}" />
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
						<json:property name="productSiteId" value="${productSiteId}" />
					</dsp:oparam>
				  </dsp:droplet>
                </c:if>
				<c:choose>
					<c:when test="${!empty skuId}">
						<dsp:tomap var="sku" value="${skuItem}" />
						<json:array name="skus">
							<json:object>
								<json:property name="skuId" value="${skuId}" />
			    				<c:if test="${isMultisiteEnabled}">		
								  <json:property name="productSiteId" value="${productSiteId}" />
								  <json:property name="siteIconURL" value="${siteIconURL}" />
								  <json:property name="siteIconHover" value="${siteIconHover}" />
								</c:if>
						<json:property name="displayName"
									value="${sku.displayName}" >
									</json:property>

								<dsp:droplet name="TRUCSRPriceDroplet">
									<dsp:param name="skuId" value="${skuId}" />
									<dsp:param name="productId" value="${product.id}" />
									<dsp:param name="site" value="${site1}" />
									<dsp:oparam name="true">
									<dsp:getvalueof var="salePrice" param="salePrice" />
									<dsp:getvalueof var="listPrice" param="listPrice" />
									<json:property name="skuPriceEach" value="${listPrice}" />
									<json:property name="skuPriceEachFormatted">
									 <web-ui:formatNumber value="${listPrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
										<json:property name="skuDiscountedPrice" value="${salePrice}" />
										<json:property name="skuDiscountedPriceFormatted">
										<web-ui:formatNumber value="${salePrice}" type="currency"
												currencyCode="${currencyCode}" />
										</json:property>
								</dsp:oparam>
								<dsp:oparam name="false">
								<dsp:getvalueof var="salePrice" param="salePrice" />
									<dsp:getvalueof var="listPrice" param="listPrice" />
									<c:choose>
									<c:when test="${salePrice == 0}">
									<json:property name="skuPriceEach" value="${listPrice}" />
									<json:property name="skuPriceEachFormatted">
									<web-ui:formatNumber value="${listPrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
									</c:when>
									<c:otherwise>
									<json:property name="skuPriceEach" value="${salePrice}" />
									<json:property name="skuPriceEachFormatted">
									<web-ui:formatNumber value="${salePrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
									</c:otherwise>
									</c:choose>
								</dsp:oparam>
								<dsp:oparam name="empty">
										<json:property name="skuPriceEach"/>
										<json:property name="skuPriceEachFormatted">
										<fmt:message key="catalogBrowse.searchResults.noPrice"/>
										</json:property>
								</dsp:oparam>
								</dsp:droplet>

								<dsp:droplet name="TRUCSRValidatePreSellableDroplet">
									<dsp:param name="sku" value="${skuItem}" />
									<dsp:oparam name="output">
									<dsp:getvalueof var="preSellable" param="preSellable" />
									<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
								</dsp:droplet>
								<dsp:droplet name="TRUCSRInventoryLookupDroplet">
									<dsp:param name="skuId" value="${sku.id}" />
									<dsp:param name="locationIdSupplied" value="false"/>
									<dsp:param name="useCache" value="true" />
									<dsp:oparam name="output">
										<dsp:droplet name="Switch">
											<dsp:param name="value"
												param="availability" />
											<dsp:getvalueof param="stockLevel" var="stockLevel"/>
											<dsp:oparam name="1001">
												<json:property name="skuStatus">
												<fmt:message
														key="global.product.availabilityStatus.outOfStock" />,
												<c:set var="ispuEligible" value="false" />
													<c:set var="s2s" value="false" />
													<c:set var="ispu" value="false" />
													<c:set var="onlineEligible" value="false" />
													<c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
														<c:set var="s2s" value="true" />
													</c:if>
													<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
														<dsp:getvalueof var="ispu" value="true" />
													</c:if>
													<c:if test="${ispu eq 'true' || s2s eq 'true'}">
														<c:set var="ispuEligible" value="true" />
													</c:if>
													<c:if
														test="${empty sku.channelAvailability or sku.channelAvailability ne 'In Store Only'}">
														<c:set var="onlineEligible" value="true" />
													</c:if>
													
													<c:if test="${sku.shipInOrigContainer}">
															#S#Dont spoil the surprise item#E#
													</c:if>
													<c:if test="${ispuEligible && !onlineEligible}">
														Available in store only,
													</c:if>
												</json:property>
												<json:property name="skuStatusCssClass" value="notInStock" />
												<c:set var="inventoryStatus" value="outofstock" />
											</dsp:oparam>
											<dsp:oparam name="1002">
												<json:property name="skuStatus">
													<fmt:message
														key="global.product.availabilityStatus.preorder" />
												</json:property>
												<c:set var="inventoryStatus" value="preorder" />
											</dsp:oparam>
											<dsp:oparam name="1003">
												<json:property name="skuStatus">
													<fmt:message
														key="global.product.availabilityStatus.backorder" />
												</json:property>
												<c:set var="inventoryStatus" value="backorder" />
											</dsp:oparam>
											<dsp:oparam name="1000">
												<json:property name="skuStatus">
												<c:if test="${!preSellable}">
													<fmt:message
														key="global.product.availabilityStatus.inStock" /> (${stockLevel}),
														<c:if test="${not empty sku.customerPurchaseLimit}">
															limit [${sku.customerPurchaseLimit}] items per customer,    
														</c:if>
														</c:if>
														<c:if test="${preSellable}">
														<fmt:message
														key="global.product.availabilityStatus.inStock" /> (${stockLevel}),
														<c:if test="${not empty sku.customerPurchaseLimit}">
														limit [${sku.customerPurchaseLimit}] items per customer,      
														</c:if>
														pre-order now,	
														Estimated Shipping date:${formattedDate},
													</c:if>
														<dsp:droplet name="TRUCSRPriceDroplet">
															<dsp:param name="skuId" value="${skuId}" />
															<dsp:param name="productId" value="${product.id}" />
															<dsp:param name="site" value="${site1}" />
															<dsp:oparam name="true">
															<dsp:getvalueof var="salePrice" param="salePrice" />
															</dsp:oparam>
															<dsp:oparam name="false">
															<dsp:getvalueof var="salePrice" param="salePrice" />
															</dsp:oparam>
														</dsp:droplet>
														<c:set var="skuDisplayFlag" value="true"/>
														 <c:if test="${sku.unCartable || !sku.webDisplayFlag || sku.deleted || sku.type=='nonMerchSKU' || sku.supressInSearch || !sku.superDisplayFlag || empty salePrice || salePrice==0.0}">
														 <c:set var="skuDisplayFlag" value="false"/>
														 </c:if>

													<c:set var="ispuEligible" value="false" />
													<c:set var="s2s" value="false" />
													<c:set var="ispu" value="false" />
													<c:set var="onlineEligible" value="false" />
													<c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
														<c:set var="s2s" value="true" />
													</c:if>
													<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
														<dsp:getvalueof var="ispu" value="true" />
													</c:if>
													<c:if test="${ispu eq 'true' || s2s eq 'true'}">
														<c:set var="ispuEligible" value="true" />
													</c:if>
													<c:if
														test="${empty sku.channelAvailability or sku.channelAvailability ne 'In Store Only'}">
														<c:set var="onlineEligible" value="true" />
													</c:if>
													<c:if test="${ispuEligible && !onlineEligible}">
														Available in store only,
													</c:if>
													<c:if test="${sku.shipInOrigContainer}">
															#S#Dont spoil the surprise item#E#
													</c:if>
														 skuDisplayflag:${skuDisplayFlag}
													</json:property>
												<c:set var="inventoryStatus" value="instock" />
											</dsp:oparam>
											<dsp:oparam name="unset">
												<json:property name="skuStatus" value="" />
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet> 
							</json:object>
						</json:array>
					</c:when>
					<c:otherwise>
						<json:array name="skus" items="${productChildSkus}" var="skuItem">
							<dsp:tomap var="sku" value="${skuItem}" />
							<json:object>
								<json:property name="skuId" value="${sku.id}" />
			    				<c:if test="${isMultisiteEnabled}">
								  <json:property name="productSiteId" value="${productSiteId}" />
								  <json:property name="siteIconURL" value="${siteIconURL}" />
								  <json:property name="siteIconHover" value="${siteIconHover}" />
								</c:if>
								<json:property name="displayName"
									value="${sku.displayName}" />
								
								<dsp:droplet name="TRUCSRPriceDroplet">
									<dsp:param name="skuId" value="${sku.id}" />
									<dsp:param name="productId" value="${product.id}" />
									<dsp:param name="site" value="${site1}" />
									<dsp:oparam name="true">
									<dsp:getvalueof var="salePrice" param="salePrice" />
									<dsp:getvalueof var="listPrice" param="listPrice" />
									<json:property name="skuPriceEach" value="${listPrice}" />
									<json:property name="skuPriceEachFormatted">
									 <web-ui:formatNumber value="${listPrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
										<json:property name="skuDiscountedPrice" value="${salePrice}" />
										<json:property name="skuDiscountedPriceFormatted">
										<web-ui:formatNumber value="${salePrice}" type="currency"
												currencyCode="${currencyCode}" />
										</json:property>
								</dsp:oparam>
								<dsp:oparam name="false">
								<dsp:getvalueof var="salePrice" param="salePrice" />
									<dsp:getvalueof var="listPrice" param="listPrice" />
									<c:choose>
									<c:when test="${salePrice == 0}">
									<json:property name="skuPriceEach" value="${listPrice}" />
									<json:property name="skuPriceEachFormatted">
									<web-ui:formatNumber value="${listPrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
									</c:when>
									<c:otherwise>
									<json:property name="skuPriceEach" value="${salePrice}" />
									<json:property name="skuPriceEachFormatted">
									<web-ui:formatNumber value="${salePrice}" type="currency"
											currencyCode="${currencyCode}" />
									</json:property>
									</c:otherwise>
									</c:choose>
								</dsp:oparam>
								<dsp:oparam name="empty">
										<json:property name="skuPriceEach"/>
										<json:property name="skuPriceEachFormatted">
										<fmt:message key="catalogBrowse.searchResults.noPrice"/>
										</json:property>
								</dsp:oparam>
								</dsp:droplet>
												
								<dsp:droplet name="TRUCSRValidatePreSellableDroplet">
									<dsp:param name="sku" value="${skuItem}" />
									<dsp:oparam name="output">
									<dsp:getvalueof var="preSellable" param="preSellable" />
									<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
								</dsp:droplet>
								<dsp:droplet name="TRUCSRInventoryLookupDroplet">
									<dsp:param name="skuId" value="${sku.id}" />
									<dsp:param name="locationIdSupplied" value="false"/>
									<dsp:param name="useCache" value="true" />
									<dsp:oparam name="output">
										<dsp:droplet name="Switch">
											<dsp:param name="value"
												param="availability" />
											<dsp:getvalueof param="stockLevel" var="stockLevel"/>
											<dsp:oparam name="1001">
												<json:property name="skuStatus">
												<fmt:message
														key="global.product.availabilityStatus.outOfStock" />,
												<c:set var="ispuEligible" value="false" />
													<c:set var="s2s" value="false" />
													<c:set var="ispu" value="false" />
													<c:set var="onlineEligible" value="false" />
													<c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
														<c:set var="s2s" value="true" />
													</c:if>
													<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
														<dsp:getvalueof var="ispu" value="true" />
													</c:if>
													<c:if test="${ispu eq 'true' || s2s eq 'true'}">
														<c:set var="ispuEligible" value="true" />
													</c:if>
													<c:if
														test="${empty sku.channelAvailability or sku.channelAvailability ne 'In Store Only'}">
														<c:set var="onlineEligible" value="true" />
													</c:if>
													
													<c:if test="${sku.shipInOrigContainer}">
															#S#Dont spoil the surprise item#E#
													</c:if>
													<c:if test="${ispuEligible && !onlineEligible}">
														Available in store only,
													</c:if>
												</json:property>
												<json:property name="skuStatusCssClass" value="notInStock" />
												<c:set var="inventoryStatus" value="outofstock" />
											</dsp:oparam>
											<dsp:oparam name="1002">
												<json:property name="skuStatus">
													<fmt:message
														key="global.product.availabilityStatus.preorder" />
												</json:property>
												<c:set var="inventoryStatus" value="preorder" />
											</dsp:oparam>
											<dsp:oparam name="1003">
												<json:property name="skuStatus">
													<fmt:message
														key="global.product.availabilityStatus.backorder" />
												</json:property>
												<c:set var="inventoryStatus" value="backorder" />
											</dsp:oparam>
											<dsp:oparam name="1000">
												<json:property name="skuStatus">
												<c:if test="${!preSellable}">
													<fmt:message
														key="global.product.availabilityStatus.inStock" /> (${stockLevel}),
														<c:if test="${not empty sku.customerPurchaseLimit}">
															limit [${sku.customerPurchaseLimit}] items per customer,    
														</c:if>
														</c:if>
														<c:if test="${preSellable}">
														<fmt:message
														key="global.product.availabilityStatus.inStock" /> (${stockLevel}),
														<c:if test="${not empty sku.customerPurchaseLimit}">
														limit [${sku.customerPurchaseLimit}] items per customer,      
														</c:if>
														pre-order now,	
														Estimated Shipping date:${formattedDate},
													</c:if>
														<dsp:droplet name="TRUCSRPriceDroplet">
															<dsp:param name="skuId" value="${skuId}" />
															<dsp:param name="productId" value="${product.id}" />
															<dsp:param name="site" value="${site1}" />
															<dsp:oparam name="true">
															<dsp:getvalueof var="salePrice" param="salePrice" />
															</dsp:oparam>
															<dsp:oparam name="false">
															<dsp:getvalueof var="salePrice" param="salePrice" />
															</dsp:oparam>
														</dsp:droplet>
														<c:set var="skuDisplayFlag" value="true"/>
														 <c:if test="${sku.unCartable || !sku.webDisplayFlag || sku.deleted || sku.type=='nonMerchSKU' || sku.supressInSearch || !sku.superDisplayFlag || empty salePrice || salePrice==0.0}">
														 <c:set var="skuDisplayFlag" value="false"/>
														 </c:if>

													<c:set var="ispuEligible" value="false" />
													<c:set var="s2s" value="false" />
													<c:set var="ispu" value="false" />
													<c:set var="onlineEligible" value="false" />
													<c:if test="${not empty sku.shipToStoreEligible && sku.shipToStoreEligible eq true}">
														<c:set var="s2s" value="true" />
													</c:if>
													<c:if test="${not empty sku.itemInStorePickUp && (sku.itemInStorePickUp eq 'Y' || sku.itemInStorePickUp eq 'YES' || sku.itemInStorePickUp eq 'Yes' || sku.itemInStorePickUp eq 'yes')}">
														<dsp:getvalueof var="ispu" value="true" />
													</c:if>
													<c:if test="${ispu eq 'true' || s2s eq 'true'}">
														<c:set var="ispuEligible" value="true" />
													</c:if>
													<c:if
														test="${empty sku.channelAvailability or sku.channelAvailability ne 'In Store Only'}">
														<c:set var="onlineEligible" value="true" />
													</c:if>
													<c:if test="${ispuEligible && !onlineEligible}">
														Available in store only,
													</c:if>
													<c:if test="${sku.shipInOrigContainer}">
															#S#Dont spoil the surprise item#E#
													</c:if>
														 skuDisplayflag:${skuDisplayFlag}
													</json:property>
												<c:set var="inventoryStatus" value="instock" />
											</dsp:oparam>
											<dsp:oparam name="unset">
												<json:property name="skuStatus" value="" />
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet> 
															
							</json:object>
						</json:array>
					</c:otherwise>
				</c:choose>
			</json:object>
		</c:if>
	</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/addProductByIdOnProduct.jsp#1 $$Change: 875535 $--%>
