<%--
 This page defines the add product by ID panel
 @version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/panels/catalog/addProductById.jsp#2 $
 @updated $DateTime: 2015/07/10 11:58:13 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">

  <dsp:importbean bean="atg/commerce/custsvc/catalog/AddProductsByIdConfigurator" var="addProductsByIdConfigurator"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" var="cartModifierFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/OrderIsModifiable"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="shoppingCart"/>
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="csrAgentTools"/>

  <c:set var="order" value="${shoppingCart.current}"/>

  <script type="text/javascript">
    _container_.onLoadDeferred.addCallback( function() {
      atg.commerce.csr.catalog.restoreAddProductByIdData('${addProductsByIdConfigurator.addProductByIdProductsFromWindowScope}');
    });

    /* _container_.onUnloadDeferred.addCallback(function () {
      atg.commerce.csr.catalog.storeAddProductByIdData();
    }); */
  </script>
<style>
	.infoImage {
	    height: 16px;
	    background: url(/TRU-DCS-CSR/images/dont-spoil.png) no-repeat;
	    padding-left: 20px;
	    background-size: 16px 16px;
		padding-top: 1px;
	}
</style>
    <div class="atg_commerce_csr_corePanelData">
      <div class="atg_commerce_csr_coreProductAddByID">

        <input type="hidden" id="atg_commerce_csr_catalog_tmpProductId" value=""/>
        <input type="hidden" id="atg_commerce_csr_catalog_tmpSkuId" value=""/>
        <input type="hidden" id="atg_commerce_csr_catalog_productNotFoundError" value="<fmt:message key="catalogBrowse.addProductsById.productNotFoundError"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_skuNotFoundError" value="<fmt:message key="catalogBrowse.addProductsById.skuNotFoundError"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_addProducts" value="<fmt:message key="catalogBrowse.productViewPopup.addProducts"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_edit" value="<fmt:message key="catalogBrowse.addProductsById.edit"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_editTooltip" value="<fmt:message key="catalogBrowse.addProductsById.editTooltip"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_editLineItem" value="<fmt:message key="catalogBrowse.addProductsById.editLineItem"/>"/>
        
        <input type="hidden" id="atg_commerce_csr_catalog_delete" value="<fmt:message key="catalogBrowse.addProductsById.delete"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_deleteTooltip" value="<fmt:message key="catalogBrowse.addProductsById.deleteTooltip"/>"/>
        <input type="hidden" id="atg_commerce_csr_catalog_dontStore" value="0"/>
        <input type="hidden" id="atg_commerce_csr_catalog_products" value=""/>

        <input type="hidden" id="atg_commerce_csr_catalog_showProductEntryField" value="${csrAgentTools.CSRConfigurator.useProductId}"/>
        <input type="hidden" id="atg_commerce_csr_catalog_showSKUEntryField" value="${csrAgentTools.CSRConfigurator.useSKUId}"/>

        <input type="hidden" id="atg_commerce_csr_catalog_isMultiSiteEnabled" value="${isMultiSiteEnabled}"/>

        <input type="hidden" id="atg_commerce_csr_catalog_quantityInputTagMaxlength" value="${csrAgentTools.CSRConfigurator.quantityInputTagMaxlength}"/>
        <input type="hidden" id="atg_commerce_csr_catalog_quantityInputTagSize" value="${csrAgentTools.CSRConfigurator.quantityInputTagSize}"/>

        <%-- support for currency formatting in JavaScript --%>
        <input type="hidden" id="atg_commerce_csr_catalog_activeCurrencyCode" value='<dsp:valueof value="${csrAgentTools.activeCustomerCurrencyCode}" />'/>

        <%-- support for currency formatting in JavaScript --%>
        <input type="hidden" id="atg_commerce_csr_catalog_currentOrderCurrencySymbol" value='<dsp:valueof value="${csrAgentTools.currentOrderCurrencySymbolInFormattingLocale}" />'/>

        <%-- support for currency number of decimal places formatting in JavaScript --%>
        <dsp:droplet name="/atg/commerce/pricing/CurrencyDecimalPlacesDroplet">
          <dsp:param name="currencyCode" value="${csrAgentTools.activeCustomerCurrencyCode}"/>
            <dsp:oparam name="output">
              <dsp:getvalueof var="currencyDecimalPlaces" param="currencyDecimalPlaces"/>
            </dsp:oparam>
        </dsp:droplet>
        <input type="hidden" id="atg_commerce_csr_catalog_activeCurrencyCodeNumberOfDecimalPlaces" value='<dsp:valueof value="${currencyDecimalPlaces}" />'/>
        <c:set var="isOrderModifiable" value="false"/>
        <c:if test="${!empty shoppingCart.originalOrder}">
          <dsp:droplet name="OrderIsModifiable">
            <dsp:param name="order" value="${shoppingCart.originalOrder}"/>
            <dsp:oparam name="true">
              <c:set var="isOrderModifiable" value="true"/>
            </dsp:oparam>
            <dsp:oparam name="false">
              <c:set var="isOrderModifiable" value="false"/>
            </dsp:oparam>
          </dsp:droplet>
        </c:if>

<script type="text/javascript">
  if (!dijit.byId("atg_commerce_csr_catalog_atg_commerce_csr_catalog_productReadOnlyPopup")) {
    new dojox.Dialog({ id: "atg_commerce_csr_catalog_atg_commerce_csr_catalog_productReadOnlyPopup",
                       cacheContent: "false", 
                       executeScripts:"true",
                       scriptHasHooks: "true",
                       duration: 100,
                       "class":"atg_commerce_csr_popup"});
  }                     
</script>
        <c:url var="productReadOnlyURL" context="${CSRConfigurator.truContextRoot}" value="/include/order/product/productReadOnly.jsp">
           <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
           <c:param name="productId" value=""/>
        </c:url>
        <input type="hidden" id="atg_commerce_csr_catalog_productReadOnly" value="<c:out value='${productReadOnlyURL}'/>"/>

<script type="text/javascript">
  if (!dijit.byId("atg_commerce_csr_catalog_productQuickViewPopup")) {
    new dojox.Dialog({ id:"atg_commerce_csr_catalog_productQuickViewPopup",
                       cacheContent:"false", 
                       executeScripts:"true",
                       scriptHasHooks:"true",
                       duration: 100,
                       "class":"atg_commerce_csr_popup"});
  }
</script>
        <c:url var="productQuickViewURL" context="${CSRConfigurator.truContextRoot}" value="/include/order/product/productQuickView.jsp">
           <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
           <c:param name="productId" value=""/>
        </c:url>
        <input type="hidden" id="atg_commerce_csr_catalog_productQuickViewURL" value="<c:out value='${productQuickViewURL}'/>"/>

  <script type="text/javascript">
    if (!dijit.byId("editLineItemPopup")) {
      new dojox.Dialog({ id:"editLineItemPopup",
                       cacheContent:"false", 
                       executeScripts:"true",
                       scriptHasHooks:"true",
                       duration: 100,
                       "class": "atg_commerce_csr_popup"});
    }
    
    function resetOutOfStock(){
    	var $doc = $(document);
    	var items = $doc.find("#atg_commerce_csr_catalog_addProductsByIdTable").find("[id^=atg_commerce_csr_catalog_skuStatus]");
    	
    	items.each(function(index, obj){
    		var $this = $(this);
    		var skuStatus = $this.text();
    		var id = $this.attr('id').split('atg_commerce_csr_catalog_skuStatus')[1];
    		if(skuStatus.indexOf("Out of Stock") != -1){
    			$doc.find("#atg_commerce_csr_catalog_qty"+id).val(0);
    			$doc.find("#atg_commerce_csr_catalog_qty"+id).trigger('change');
    			//$doc.find("#atg_commerce_csr_catalog_qty"+id).attr('disabled',true);
    		}else{
    			$doc.find("#atg_commerce_csr_catalog_qty"+id).attr('disabled',false);
    		}	    		
    	});
    };
     
    // Validates the Out of Stock products and sets the quantity of those products to '0' before adding the items to cart.
    function addToCartBtnAddProduct(){
    	if($(".error").length)
    		{
    			$(".error").remove();
    		}
  	  $('.quantity-input').removeAttr('style');
  	  var quantityExceedError = false;
  	  var customerPurchaseLimitError = false;
  	  var outOfStockError = false;
  	  var skuId ='';
  	  var stockLevel = 0;
  	  var purchaseLimit = '';
  	  var noQtySelected = true;
  	  var qtyZeroError = false;
  	  var notEligibleForSTHError = false;
  	  var skus = [];
  	  var skuObeject = {};
  	  var skuArray = [];
  	  var skuOrProdExistError = false;
  	  $('[id^=atg_commerce_csr_catalog_qty]').filter(function(){
  		if(!$(this).hasClass("disabled"))
 		 {
	  		 var id = $(this).attr('id').split('atg_commerce_csr_catalog_qty')[1];
	  		 var skuid = $("#atg_commerce_csr_catalog_skuId"+id).val();
	  		 var prodId = $("#atg_commerce_csr_catalog_productId"+id);
	  		 var qtyInputLevel = parseInt($(this).val());
	  		 var stockStatusLabel = $("#atg_commerce_csr_catalog_skuStatus"+id).text();
	  		 if(stockStatusLabel != '' && stockStatusLabel != 'Out of Stock')
	  	  		{
	  			 	if(stockStatusLabel.indexOf('(') > 0)
	  			 	{
	  					var skuStockLevel = parseInt(stockStatusLabel.split('(')[1].split(')')[0]);
	  			 	}
	  				if(stockStatusLabel.indexOf('[') > 0){
	  					var customerpurchaselimit = parseInt(stockStatusLabel.split('[')[1].split(']')[0]);
	  				}
	  				var stockStatus = stockStatusLabel.split('(')[0];
	  	  		}
	  		 else if( stockStatusLabel == 'Out of Stock')
	  			 {
	  				outOfStockError = true;
	  			 }
	  		if(stockStatusLabel.indexOf('Available in store only') > -1)
				{
					notEligibleForSTHError = true;
				}
	  		 if($.trim($(this).val()) != '')
	  			 {
	  			 	noQtySelected = false;
	  			 }
	  		if ($.inArray(skuid, skus) > -1) {
	  			skuOrProdExistError = true;
	        } else {
	        	skus.push(skuid);
	        }
	  		if(parseInt($.trim($(this).val())) == 0)
		        {
			        qtyZeroError = true;
			        $(this).css({"border-color":"red"});
		        }
	  		 if(qtyInputLevel > skuStockLevel)
	  			 {
			 		quantityExceedError = true;
			 		skuId = skuid;
			 		stockLevel = skuStockLevel;
			 		$(this).css({"border-color":"red"});
	  			 	
	  			 }
	  		 else if(customerpurchaselimit > 0 && qtyInputLevel > customerpurchaselimit)
	  			 {
	  			 	customerPurchaseLimitError = true;
	  			 	skuId = skuid;
	  			 	purchaseLimit = customerpurchaselimit;
	  			 	$(this).css({"border-color":"red"});
	  			 }
	  		 
  	  	}
  	  });
  		
  		
  	  if(quantityExceedError || customerPurchaseLimitError || noQtySelected || qtyZeroError || skuOrProdExistError || outOfStockError || notEligibleForSTHError)
  		  {
  		  	if(quantityExceedError)
  		  		{
  		  			$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Quantity input exceeds stock level. Please enter valid quantity.Stock level for '+skuId+' is '+stockLevel+'</span>');
  		  		}
  		  	else if(customerPurchaseLimitError)
  		  		{
      		  		$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Customer purchase limit for <b style="color:black;">'+skuId+'</b> is <b style="color:black;">'+purchaseLimit+'</b> please select quantity accordingly</span>');
  		  			
  		  		}
  		  	else if(noQtySelected)
		  		{
  		  			$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Please enter any sku quantity </span>');
		  			
		  		}
  		   else if(qtyZeroError)
	          {
  					$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Quantity should not be zero </span>');
	          }
  		  else if(skuOrProdExistError)
	         {
					$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Same sku cannot add multiple times</span>');
	         }
  		 else if(outOfStockError)
       		 {
				$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">sku is out of stock</span>');
       		 }
  		 else if(notEligibleForSTHError)
  			 {
  				$(".atg_commerce_csr_coreProductAddByID").prepend('<span style="color:red" class="error">Not available for ship to home</span>');
  			 }
  		  	return false;
  		  }
  	  else
  		  {
		  		resetOutOfStock();       	
		    	atg.commerce.csr.catalog.addProductsByIdToShoppingCart();
  		  }
    }
    $(document).on('keypress','[id^=atg_commerce_csr_catalog_qty]', function(evt){
    	var length = parseInt($(this).val().length);
    	if(length > 2){return false;}
    	evt = (evt) ? evt : window.event;
    	var charCode = (evt.which) ? evt.which : evt.keyCode;
    	 if (navigator.userAgent.indexOf("Firefox") > 0) {
    	    	if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
    	    		return true;
    	        	}
    	    		return false;
    	    	}
    	    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
    	    	 return true;
    	    }
    	   
    	    return false;
    });
    $(document).on("click",".atg_propertyClear",function(){
    	if($(".error").length)
    		{
    			$(".error").remove();
    		}
    })
    
  </script>
        <c:url var="productEditLineItemURL" context="${CSRConfigurator.contextRoot}" value="/include/order/editProductSKU.jsp">
           <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
           <c:param name="mode" value="return"/>
           <c:param name="skuId" value="SKUIDPLACEHOLDER"/>
           <c:param name="productId" value=""/>
        </c:url>
        <input type="hidden" id="atg_commerce_csr_catalog_productEditLineItem" value="<c:out value='${productEditLineItemURL}'/>"/>
        <c:url var="readProductJsonURL" context="${CSRConfigurator.truContextRoot}" value="/panels/catalog/addProductByIdOnProduct.jsp">
           <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
        </c:url>
        <input type="hidden" id="atg_commerce_csr_catalog_readProductJsonURL" value="<c:out value='${readProductJsonURL}'/>"/>

        <dsp:form formid="addProductsByIdForm" style="display:block;" id="atg_commerce_csr_catalog_addProductsByIdForm">
          <table cellpadding="0" cellspacing="0" class="atg_dataTable" id="atg_commerce_csr_catalog_addProductsByIdTable">
            <thead>
              <tr id="atg_commerce_csr_catalog_addProductsByIdTr0">
                <c:if test="${csrAgentTools.CSRConfigurator.useProductId}">
                  <th scope="col" class="atg_numberValue">
                    <fmt:message key="catalogBrowse.addProductsById.id"/>
                  </th>
                </c:if>
                <c:if test="${csrAgentTools.CSRConfigurator.useSKUId}">
                  <th scope="col" class="atg_numberValue">
                    <fmt:message key="catalogBrowse.addProductsById.sku"/>
                  </th>
                </c:if>
                <th scope="col" class="atg_numberValue">
                  <fmt:message key="catalogBrowse.addProductsById.qty"/>
                </th>
                <th scope="col">
                  <fmt:message key="catalogBrowse.addProductsById.name"/>
                </th>
                <th scope="col" style="width: 165px;">
                  <fmt:message key="catalogBrowse.addProductsById.status"/>
                </th>
                <th scope="col" class="atg_numberValue">
                  <fmt:message key="catalogBrowse.addProductsById.priceEach"/>
                </th>
                <th scope="col" class="atg_numberValue">
                  <fmt:message key="catalogBrowse.addProductsById.totalPrice"/>
                </th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
              </tr>
            </thead>

            <svc-ui:frameworkUrl var="url"/>
            <dsp:input bean="CartModifierFormHandler.addItemCount" name="atg_commerce_csr_catalog_addItemCount" type="hidden" value="1"/>
            <dsp:input bean="CartModifierFormHandler.addMultipleItemsToOrderErrorURL" type="hidden" value="${url}" />
            <dsp:input bean="CartModifierFormHandler.addMultipleItemsToOrderSuccessURL" type="hidden" value="${url}" />
            <dsp:input bean="CartModifierFormHandler.addMulpitleItemsToOrder" type="hidden" value="" priority="-10"/>

          </table>
        </dsp:form>
        <dsp:setvalue bean="CartModifierFormHandler.addItemCount" value="1"/>
        <div style="display:none;" id="atg_commerce_csr_catalog_addToCartContainer"></div>
      </div>
      <div class="atg_commerce_csr_panelFooter">
        <input type="hidden" name="atg_commerce_csr_catalog_isOrderModifiableHiddenValue" id="atg_commerce_csr_catalog_isOrderModifiableHiddenValue" value="<c:out value='${isOrderModifiable}'/>"/>
        <input type="button" value="<fmt:message key='catalogBrowse.addProductsById.addToShoppingCart'/>" onclick="addToCartBtnAddProduct();" disabled="disabled" id="atg_commerce_csr_catalog_addToShoppingButton"/>
      </div>
    </div>    
    <script type="text/javascript">
      atg.progress.update('cmcCatalogPS');
      _container_.onLoadDeferred.addCallback( function() {
        atg.keyboard.registerFormDefaultEnterKey("atg_commerce_csr_catalog_addProductsByIdForm", "atg_commerce_csr_catalog_addToShoppingButton");      
      });
      _container_.onUnloadDeferred.addCallback( function() {
        atg.keyboard.unRegisterFormDefaultEnterKey("atg_commerce_csr_catalog_addProductsByIdForm");      
      });
      
      $(document).on('change','[id^=atg_commerce_csr_catalog_productId],[id^=atg_commerce_csr_catalog_skuId]', function(evt){
    	  $('.error').remove();
    	  var parentTr = $(this).closest("[id^=atg_commerce_csr_catalog_addProductsByIdTr]").attr("id");
    	  var rowId = parseInt(parentTr.split("atg_commerce_csr_catalog_addProductsByIdTr")[1]);
    	  var count = 0;
    	  var displayProduct = setInterval(function(){
    		  if($("#atg_commerce_csr_catalog_productQuickViewPopup").css('display') =='block')
				  {
	    			  clearInterval(displayProduct);
	    			  count++;    			  	
					  }
    		  else{
    			  displayRow(rowId);
				  count++;
				  clearInterval(displayProduct);
    			  
    		  }
    	  },200);
    	  if(count == 6)
    		  {
    		  	clearInterval(displayProduct);
    		  }
      });
      $(document).on("click","#skuPickOK,#okSkuPick",function(){
	  var rowId = 1;
	  var rowLength = $("[id^=atg_commerce_csr_catalog_addProductsByIdTr]").length;
	  for(var i = 1;i <= rowLength;i++)
		  {
		  	displayRow(rowId);
		  	rowId = rowId + 1;
		  }
  		});  
   function displayRow(rowId){
	   	  var status = $("#atg_commerce_csr_catalog_skuStatus"+rowId).html();
		  if(typeof status !== 'undefined' || $.trim(status) != '')
			  {
			  	var status1 = status.split("skuDisplayflag")[0];
			  	var discription = status1.split(",");
			  	var displaySku = discription.join("<br>");
			  	
			  	if(displaySku.indexOf('#S#') > -1){
			  		displaySku = displaySku.replace('#S#','<div class="infoImage">');
			  		displaySku = displaySku.replace('#E#','</div>');
			  		/* var tmpDiscription = discription.slice(0,-1);
				  	var tmpDiscriptionLast = discription.slice(-1);
			  		displaySku = tmpDiscription.join('<br>') + '<div class="infoImage">' + tmpDiscriptionLast.join('<br>') + '</div>'; */	
			  	}
			  	
			  	
			  	var skuDisplay = status.split("skuDisplayflag:")[1];
			  	if($.trim(skuDisplay) == 'false')
			  		{
			  			$("#atg_commerce_csr_catalog_qty"+rowId).val("").addClass('disabled').attr("disabled",true).css("background-color","#e6e6e6");
			  			$("#atg_commerce_csr_catalog_qty"+rowId).closest("#quantityCell"+rowId).find(".skuNotAvailable").remove();
			  			$("#atg_commerce_csr_catalog_qty"+rowId).closest("#quantityCell"+rowId).find("br").remove();
			  			$("#atg_commerce_csr_catalog_qty"+rowId).closest("#quantityCell"+rowId).append("</br><span class='skuNotAvailable'>Product not available for Purchase</span>");
			  			
			  		}
			  	//displaySku = displaySku.replace('<br>###','<br class="abc">');
			  	$("#atg_commerce_csr_catalog_skuStatus"+rowId).html(displaySku);
			  }
		  $('#atg_commerce_csr_catalog_productName'+rowId).find("img").remove();
		  var disabled = true;
		  $("[name^=atg_commerce_csr_catalog_qty]").filter(function(){
			 var thisClass = $(this).attr("class");
			 var qtyInput = $.trim($(this).val());
			 if(typeof thisClass == 'undefined' && (qtyInput != '' || qtyInput != 0))
				 {
					 disabled = false;
				 }
		  });
		  $("#atg_commerce_csr_catalog_addToShoppingButton").attr("disabled",disabled);
   }
    </script>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/panels/catalog/addProductById.jsp#2 $$Change: 1179550 $--%>
