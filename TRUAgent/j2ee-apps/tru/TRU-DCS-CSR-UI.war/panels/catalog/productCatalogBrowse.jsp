<%--
 This page defines the product catalog panel
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/productCatalogBrowse.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
  <dsp:importbean bean="/atg/userprofiling/Profile" var="profile"/>

    <dsp:tomap var="profile" value="${profile}"/>
    <c:set var="currentCatalog" value="${profile.catalog}"/>
    <div class="panelContent" id="___panelContent___">
      <div parseWidgets="false" class="atg_commerce_csr_browseTree">
        <dsp:include src="/include/catalog/navigationTree.jsp" otherContext="${CSRConfigurator.contextRoot}" />
      </div>

     <script type="text/javascript" charset="UTF-8"	src="/TRU-DCS-CSR/script/catalog.js"></script>
	   <div class="atg_commerce_csr_coreContent atg_commerce_csr_productBrowse" id="atg_commerce_csr_coreContent">
        <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
        <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" var="cartModifierFormHandler"/>
        <dsp:importbean bean="/atg/commerce/custsvc/catalog/CustomCatalogProductSearch" var="customCatalogProductSearch"/>
        <dsp:importbean bean="/atg/commerce/custsvc/catalog/ProductSearch" var="productSearch"/>
        <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="shoppingCart"/>
        <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
          
        <dsp:importbean var="defaultPageFragment" bean="/atg/commerce/custsvc/ui/fragments/catalog/ProductSearchDefault" /> 
        <dsp:importbean var="extendedPageFragment" bean="/atg/commerce/custsvc/ui/fragments/catalog/ProductSearchExtended" /> 
          <style type="text/css">
	    	#atg_commerce_csr_catalog_notifyMeEmailPopup {
			width: 850px !important;
			max-width: 850px !important;
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div
			{
			/* width: 23% !important; */
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails ul
			{
			float: right !important;
			width: 75% !important;
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div img#atg_commerce_csr_catalog_product_info_image
			{
			width: 80px !important;
			height: 80px !important;
		}
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div.shopping-cart-remove{
			width: 250px !important;
		}
		.shopping-cart-surprise-image
		    {
		    	    width: 20px;
				    height: 20px;
				    background-image: url(/TRU-DCS-CSR/images/dont-spoil.png);
				    background-repeat: no-repeat;
				    float: left;
				    margin-right: 5px;
				    margin-top:-3px;
		    }
		    div.atg_commerce_csr_productCatalogDetails ul li{
		    	 margin-top:1px;
		    }
		    div.atg_commerce_csr_productCatalogDetails ul li.spoilInfo {
		    	 margin-top: 5px;
		    }
		  </style>
        <c:set var="orderIsModifiable" value="false"/>
        <c:if test="${!empty shoppingCart.originalOrder}">
          <dsp:droplet name="/atg/commerce/custsvc/order/OrderIsModifiable">
            <dsp:param name="order" value="${shoppingCart.originalOrder}"/>
            <dsp:oparam name="true">
              <c:set var="orderIsModifiable" value="true"/>
            </dsp:oparam>
          </dsp:droplet>
        </c:if>
        <dsp:tomap bean="/atg/userprofiling/Profile" var="profile"/>
        
        <c:set var="useCustomCatalogs" value="${CSRConfigurator.customCatalogs}"/>

        <c:set var="productSearchBean" value="ProductSearch"/>
        <c:if test="${useCustomCatalogs}">
          <c:set var="productSearch" value="${customCatalogProductSearch}"/>
          <c:set var="productSearchBean" value="CustomCatalogProductSearch"/>
        </c:if>
   
        <script type="text/javascript">
          dojo.provide("atg.commerce.csr.catalog.productCatalog");
          atg.commerce.csr.catalog.productCatalogSearchSetFormSortProperty = function (columnIndex, sortDesc) {
            var theForm = dojo.byId(atg.commerce.csr.catalog.productCatalogPagedData.formId);
          }

          atg.commerce.csr.catalog.productCatalogSearchFields = [
            { name: '&nbsp;'},
            { name: '<fmt:message key="catalogBrowse.searchResults.productId"/>'},
            { name: '<fmt:message key="catalogBrowse.searchResults.SKU"/>'},
            { name: '<fmt:message key="catalogBrowse.searchResults.name"/>'},
            { name: '<fmt:message key="catalogBrowse.searchResults.priceRange"/>'},
            { name: '<fmt:message key="catalogBrowse.searchResults.qty"/>'},
            { name: '<fmt:message key="catalogBrowse.searchResults.actions"/>'}
          ];

          atg.commerce.csr.catalog.productCatalogSearchProperties = [
            { property: "image" },
            { property: "productId" },
            { property: "sku" },
            { property: "name" },
            { property: "priceRange" },
            { property: "qty" },
            { property: "actions" }
          ];
          atg.commerce.csr.catalog.productCatalog.searchView =
            { cells: [[
              { name: '<fmt:message key="catalogBrowse.searchResults.productInformation"/>',width:"65%"},
              { name: '<fmt:message key="catalogBrowse.searchResults.actions"/>',width:"35%"}
            ]]};


          atg.commerce.csr.catalog.productCatalog.searchLayout = [atg.commerce.csr.catalog.productCatalog.searchView];


            atg.commerce.csr.catalog.productCatalogPagedData =
              new atg.data.FormhandlerData(atg.commerce.csr.catalog.productCatalogSearchFields,"${CSRConfigurator.truContextRoot}/include/catalog/productCatalogSearchResults.jsp?orderIsModifiable=<c:out value='${orderIsModifiable}'/>");
            <dsp:getvalueof var="rowsPerPage" bean="${productSearchBean}.maxResultsPerPage"/>
            atg.commerce.csr.catalog.productCatalogPagedData.rowsPerPage = ${rowsPerPage};
            atg.commerce.csr.catalog.productCatalogPagedData.formId = 'selectTreeNode';
            atg.commerce.csr.catalog.productCatalogPagedData.setCurrentPageNumber = function(inRowIndex) {
              var currentPage = Math.floor(inRowIndex / this.rowsPerPage) + 1;
              var form = dojo.byId(this.formId);
              if (form) {
                if (form[this.formCurrentPageField]){
                  form[this.formCurrentPageField].value = currentPage;
                }
              }
            };
            atg.commerce.csr.catalog.productCatalogPagedData.rows = function(inRowIndex, inData) {
              for (var i=inRowIndex, l=inData.results.length; i<l; i++) {
                var newRow = [
                  '<div class="atg_commerce_csr_productCatalogDetails"><div>' + inData.results[i].image + '</div><ul><li>' + inData.results[i].name + '</li><li>' + inData.results[i].productId  + '</li><li>'  + inData.results[i].mfrSuggestedAge + '</li><li>' + inData.results[i].priceRange + '</li><li>'  + inData.results[i].highestRankPromotion + '</li><li class="spoilInfo">'  + inData.results[i].spoil + '</li></ul></div>',
                  inData.results[i].qty + inData.results[i].actions
                ];
                this.setRow(newRow, i);
              }
            };

            atg.commerce.csr.catalog.productCatalog.searchRefreshGrid = function () {
              dojo.byId("selectTreeNode").currentResultPageNum.value = 1;
              atg.commerce.csr.catalog.productCatalogPagedData.count = 0;
              dijit.byId("atg_commerce_csr_catalog_productCatalogTable").rowCount = 0;
              atg.commerce.csr.catalog.productCatalogPagedData.clearData();
              dijit.byId("atg_commerce_csr_catalog_productCatalogTable").updateRowCount(0);
              var form = document.getElementById("selectTreeNode");
              var selectedCategoryId = null;
              if (form) {
            	  selectedCategoryId = form.hierarchicalCategoryId.value;
              }
        /*       $.ajax({
                  type: "POST",
                  url: "/TRU-DCS-CSR/include/catalog/subCategoriesList.jsp?categoryId="+selectedCategoryId,
                  success: function (data) {
                	  //alert(data);
                	 $("#ddd").html(data);
                	 
                  }
              }); */
              atg.commerce.csr.catalog.productCatalogPagedData.fetchRowCount(
              {
                callback: function(inRowCount) {
                  if (inRowCount.resultLength > 0) {
                    dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "display", "");
                    dojo.style("atg_commerce_csr_catalog_noResults", "display", "none");
                    dojo.style("atg_commerce_csr_catalog_selectCategory", "display", "none");
                  } else {
                    if (selectedCategoryId) {
                        dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "display", "none");
                        dojo.style("atg_commerce_csr_catalog_selectCategory", "display", "none");
                        dojo.style("atg_commerce_csr_catalog_noResults", "display", "");
                    } else {
                        dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "display", "none");
                        dojo.style("atg_commerce_csr_catalog_selectCategory", "display", "");
                        dojo.style("atg_commerce_csr_catalog_noResults", "display", "none");
                    }
                  }
                  atg.commerce.csr.catalog.productCatalogPagedData.count = inRowCount.resultLength;
                  dijit.byId("atg_commerce_csr_catalog_productCatalogTable").rowCount = inRowCount.resultLength;
                  atg.commerce.csr.catalog.productCatalogPagedData.clearData();
                  dijit.byId("atg_commerce_csr_catalog_productCatalogTable").updateRowCount(inRowCount.resultLength);
                }
              });
            };

            atg.commerce.csr.catalog.productCatalogAddPagination = function() {
              dijit.byId("atg_commerce_csr_catalog_productCatalogTable").setStructure(atg.commerce.csr.catalog.productCatalog.searchLayout);
              //dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "visibility", "hidden");
            }

            atg.commerce.csr.catalog.productCatalogRemovePagination = function() {
              // clean up after ourselves
              atg.commerce.csr.catalog.productCatalogPagedData = undefined;
            }
            </script>
          <script type="text/javscript">
          _container_.onLoadDeferred.addCallback(function () {
              atg.commerce.csr.catalog.clearTreeStateIfCatalogChanged('<c:out value="${currentCatalog}"/>');
              atg.commerce.csr.catalog.productCatalogAddPagination();
              var form = dojo.byId("selectTreeNode");
              if (form && window.catalogInfo) {
                if (window.catalogInfo.allHierarchicalCategoryId) form.allHierarchicalCategoryId.value = window.catalogInfo.allHierarchicalCategoryId;
                if (window.catalogInfo.hierarchicalCategoryId) form.hierarchicalCategoryId.value = window.catalogInfo.hierarchicalCategoryId;
                <c:if test="${CSRConfigurator.useSKUId}">
                  if (window.catalogInfo.sku) form.sku.value = window.catalogInfo.sku;
                </c:if>
                <c:if test="${CSRConfigurator.useProductId}">
                  if (window.catalogInfo.productID) form.productID.value = window.catalogInfo.productID;
                </c:if>
                if (window.catalogInfo.priceRelation) form.priceRelation.value = window.catalogInfo.priceRelation;
                if (window.catalogInfo.itemPrice) form.itemPrice.value = window.catalogInfo.itemPrice;
                if (window.catalogInfo.searchInput) form.searchInput.value = window.catalogInfo.searchInput;
              }
              dojo.connect(this, "resize", dojo.hitch(dijit.byId("atg_commerce_csr_catalog_productCatalogTable"), "update"));
              
              atg.keyboard.registerFormDefaultEnterKey("selectTreeNode", "searchButton");
          });
          _container_.onUnloadDeferred.addCallback(function () {
              atg.keyboard.unRegisterFormDefaultEnterKey("selectTreeNode");
              atg.commerce.csr.catalog.productCatalogRemovePagination();
              dojo.disconnect(this, "resize", dojo.hitch(dijit.byId("atg_commerce_csr_catalog_productCatalogTable"), "update"));
          });


          </script>
          
          
          	<div id="atg_commerce_csr_catalog_subCategoriesListContainer">
               <c:if test="${!empty productSearch.hierarchicalCategoryId}">
                <c:import url="/include/catalog/subCategoriesList.jsp">
                <c:param name="_windowid" value="${param['_windowid']}"/>
                <c:param name="categoryId" value="${productSearch.hierarchicalCategoryId}"/>
                <c:param name="path" value="${productSearch.navigationPath}"/>
              </c:import> 
              </c:if>
           
          </div>
          <div id="atg_commerce_csr_catalog_selectCategory">
            <fmt:message key="catalogBrowse.browseResults.selectCategory"/>
          </div>
          <div id="atg_commerce_csr_catalog_noResults" style="display:none;">
            <fmt:message key="catalogBrowse.browseResults.noResults"/>
          </div>
          <div id="atg_commerce_csr_catalog_productCatalogTableContainer" style="height:350px;display:none">
            <div id="atg_commerce_csr_catalog_productCatalogTable"
             dojoType="dojox.Grid"
             model="atg.commerce.csr.catalog.productCatalogPagedData"
             autoHeight="true"
             onMouseOverRow="atg.noop()"
             onRowClick="atg.noop()"
             onCellClick="atg.noop()"
             onCellContextMenu="atg.noop()"
             onkeydown="atg.noop()">
            </div>
          </div>
      </div>
    </div>
  </dsp:layeredBundle>
  <script type="text/javascript">
    dojo.addOnLoad(function () {
      atg.progress.update('cmcCatalogPS');
    });
    

 function addToCartSearchResults(scProductId, sku, productId, addInfoMessage, siteId, customerPurchaseLimit){
		 $.ajax({
			  method: "POST",
			  url: "/TRU-DCS-CSR/include/catalog/inventoryLookupAjaxResponse.jsp",
			  data: {skuId : sku},
			  dataType :'json'
			})
		  .done(function( msg ) {
			  $('.searchQtyErrorMsg').remove();
			  var quantity = parseInt($(document).find("#itemQuantity"+scProductId).val());
			  var onlineEligible = $("#onlineEligible_"+sku).val();
			  if(typeof msg.response === "undefined" || ( typeof msg.response !== "undefined" &&  msg.response.skuStatus == "outofstock")){
				  if(msg.response.skuStatus=="outofstock")
					  {
					 	var nmwaEligible = $("#nmwaEligible_"+sku).val();
					  	$(document).find("#itemQuantity"+scProductId).val('');
					  	var outOfStockMsg = '<span class="outOfStockMsg" style="margin-right: 5px;">Out Of Stock</span>';
					  	if(nmwaEligible == 'true')
					  		{
					  			var nmwaMsg = '<a href="#" id="'+sku+'" class="out-of-stock-text">email me when available</a>';
					  		}
					  	else
					  		{
					  			var nmwaMsg = '';
					  		}
					  	var displayMsg = '<div style="display:block;margin-top:5px;">'+outOfStockMsg+nmwaMsg+'</div>';
					  	var qtyInputId = "itemQuantity"+scProductId;
					  	var qtyInputBtnId = qtyInputId+"_button";
					  	$("#"+qtyInputId).attr("disabled",true);
					  	$("#"+qtyInputBtnId).attr("disabled",true);
					  	$("#"+qtyInputId).parent("td").append($(displayMsg));
					  }
				  else
					  {
						  $(document).find("#itemQuantity"+scProductId).val(0);
					  }
				  return false;
			  }
			  var response = msg.response;
			  var noQtySelected = true;
        	  var qtyZeroError = false;
        	  var quantityExceedError = false;
        	  var customerPurchaseLimitError = false;
			 if($.trim($("#itemQuantity"+scProductId).val()) == '')
 			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg" style="color:red;">&nbsp;&nbsp;Please select any sku quantity </span>');
 			 }
			  else if(parseInt($.trim($("#itemQuantity"+scProductId).val())) == 0)
 			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">&nbsp;&nbsp;quantity should not be zero</span>');
 			 }
			 else  if(customerPurchaseLimit > 0 && quantity > customerPurchaseLimit)
			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">Customer purchase limit for sku is <b style="color:black;">'+customerPurchaseLimit+'</b></span>');
 			 } 
			 else if(response.skuStatus == "instock" && onlineEligible == 'false'){
				 $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">Not available for ship to home</span>');
			 }
			 else if(response.skuStatus=="instock" &&  quantity > response.stockLevel && onlineEligible == 'true'){
				  $(document).find("#itemQuantity"+scProductId).val(response.stockLevel);
				  atg.commerce.csr.catalog.addItemToOrder(scProductId, sku, productId, addInfoMessage, siteId);
			  }else if(response.skuStatus=="instock" && (quantity <= response.stockLevel) && onlineEligible == 'true'){
				  atg.commerce.csr.catalog.addItemToOrder(scProductId, sku, productId, addInfoMessage, siteId);
			  }
		  }).fail(function(){
			  //debugger;
			  $(document).find("#itemQuantity"+scProductId).val(0);
		  });
	}
	$(document).on("click",".out-of-stock-text",function(){
		var skuId = $(this).attr("id");
		loadNotifyMe(skuId)
	});
	if (!dijit.byId("atg_commerce_csr_catalog_notifyMeEmailPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_notifyMeEmailPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
    if (!dijit.byId("atg_commerce_csr_catalog_privacyDetailsPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_privacyDetailsPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
	function loadNotifyMe(skuId)
	{
		var displayName = $("#skuDisplayName_"+skuId).val();
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Notify Me',
            url: "/TRU-DCS-CSR/panels/order/notifyMePopUp.jsp?skuId="+skuId+"&skuDisplayName="+displayName
            });	
	}
	function showPrivacyDetails()
	{
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Privacy Policy',
            url: "/TRU-DCS-CSR/panels/order/privacyDetails.jsp",
            onClose: function(args) {
            	
            }});	
	}
	function submitNotifyMeDetails(skuId)
	{
		var emailId = dojo.byId("notifyMeMailId").value;
  	  	var itemProductId = $("#itemProductId_"+skuId).val();
  	    var catalogRefId= $("#catalogRefId_"+skuId).val();
  		var currentSiteId = $("#currentSiteId_"+skuId).val();
  		var productUrl=$("#productPageUrl_"+skuId).val();
  		var dsiplayName=$("#skuDisplayName_"+skuId).val();
  		var description=$("#description_"+skuId).val();
  		var sendSpecialofferToMail = $("#notify-checkbox").is(":checked");
  		var onlinePID=$("#onlinePID_"+skuId).val();
  		if(isValidEmail(emailId)){
    		dojo.xhrPost({url:"/TRU-DCS-CSR/panels/order/submitNotifyDetails.jsp?notifyMeMailId="+emailId+"&itemProductId="+itemProductId+"&catalogRefId="+catalogRefId+"&currentSiteId="+currentSiteId+"&productPageUrl="+productUrl+"&sendSpecialofferToMail="+sendSpecialofferToMail+"&skuDisplayName="+dsiplayName+"&description="+description+"&onlinePID="+onlinePID,content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(responce){
    			var msg = $.trim(responce);
    			if(msg.indexOf("Following are the form errors:") > -1)
  				{
  					var msg1 = msg.split(":")[1];
  					$("#successOrErrorMsg").html(msg1).css("color","red").show();
  				}
    			else{
	 				 	dojo.style(dojo.byId('notifyMe_content'), "display", "none");
	 				 	$("#successOrErrorMsg").html(msg).css({'color':"#000",'margin-top':"33px"}).show();
	 				
	    		 	}
  			},mimetype:"text/html"});
      	}
  	  	else{
  	  		$("#successOrErrorMsg").html("Please enter the email Id in the correct format.").css("color","red");
  	  	}
	}
	function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	} 
  </script>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/catalog/productCatalogBrowse.jsp#1 $$Change: 875535 $--%>