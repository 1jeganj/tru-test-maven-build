<%--
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/ordersummary/modify/cart.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<!-- begin ordersummary/modify/cart.jsp -->
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
<dt>
  <a href="#" id="keyboardShortcutShoppingCart" onclick="<dsp:include src="/include/order/currentOrderViewAction.jsp" otherContext="${CSRConfigurator.contextRoot}"/>event.cancelBubble=true;return false;"><fmt:message key="orderSummary.shoppingCart"/></a> 
</dt>
<dd>
  <span id="atg_csc_ordersummary_shoppingCartItems">
    <fmt:message key="orderSummary.numItems">
      <fmt:param>
        <web-ui:formatNumber value="${! empty order.commerceItems ? order.totalItemCount : 0}"/>
      </fmt:param>
    </fmt:message>
  </span>
</dd>
<dt>
  <em><fmt:message key="orderSummary.orderTotal"/></em>
</dt>
<dd>
  <csr:getCurrencyCode order="${order}">
			<c:set var="currencyCode" value="${currencyCode}" scope="request" />
		</csr:getCurrencyCode> 
		<span id="atg_csc_ordersummary_orderTotalAmount">
    <web-ui:formatNumber value="${order.priceInfo.total}" type="currency" currencyCode="${currencyCode}"/>
  </span>
</dd>
</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/ordersummary/modify/cart.jsp#1 $$Change: 875535 $--%>
