<%--
 This page defines the order summary panel
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/ordersummary/orderSummary.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<!-- begin orderSummary.jsp -->
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">

<dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" scope="request"/>
<dsp:importbean var="agentTools" bean="/atg/commerce/custsvc/util/CSRAgentTools" scope="request"/>
 <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
<c:set var="order" value="${cart.current}" scope="request"/>

<dsp:droplet name="/atg/commerce/custsvc/order/IsOrderSubmitted">
  <dsp:param name="order" value="${order}"/>
  <dsp:oparam name="true">
    <c:set var="orderIsSubmitted" value="true" scope="request"/>
  </dsp:oparam>
  <dsp:oparam name="false">
    <c:set var="orderIsSubmitted" value="false" scope="request"/>
  </dsp:oparam>
</dsp:droplet>

<dsp:droplet name="/atg/commerce/custsvc/order/OrderIsModifiable">
  <dsp:param name="order" value="${order}"/>
  <dsp:oparam name="true">
    <c:set var="orderIsModifiable" value="true" scope="request" />
  </dsp:oparam>
  <dsp:oparam name="false">
    <c:set var="orderIsModifiable" value="false" scope="request" />
  </dsp:oparam>
</dsp:droplet>

<dsp:droplet name="/atg/commerce/custsvc/order/OrderIsReturnable">
  <dsp:param name="order" value="${order}"/>
  <dsp:oparam name="true">
    <c:set var="orderIsReturnable" value="true" scope="request" />
  </dsp:oparam>
  <dsp:oparam name="false">
    <c:set var="orderIsReturnable" value="false" scope="request" />
  </dsp:oparam>
</dsp:droplet>

<c:set var="returnRequest" value="${cart.returnRequest}" scope="request" />

<dsp:droplet name="/atg/commerce/custsvc/order/IsOrderIncomplete">
  <dsp:param name="order" value="${order}"/>
  <dsp:oparam name="true">
    <c:set var="orderIsIncomplete" value="true" scope="request" />
  </dsp:oparam>
  <dsp:oparam name="false">
    <c:set var="orderIsIncomplete" value="false" scope="request" />
  </dsp:oparam>
</dsp:droplet>

<dsp:droplet name="/atg/dynamo/droplet/Switch">
  <dsp:param bean="/atg/commerce/custsvc/util/CSRConfigurator.usingScheduledOrders" name="value"/>
  <dsp:oparam name="true">
    <dsp:droplet name="/atg/commerce/custsvc/order/scheduled/IsScheduledOrderTemplate">
      <dsp:param name="order" value="${order}"/>
      <dsp:oparam name="true">
        <c:set var="orderIsTemplate" value="true" scope="request"/>
      </dsp:oparam>
      <dsp:oparam name="false">
        <c:set var="orderIsTemplate" value="false" scope="request"/>
      </dsp:oparam>
    </dsp:droplet>
  </dsp:oparam>
</dsp:droplet>


<dsp:importbean var="start" bean="/atg/commerce/custsvc/ordersummary/Start"/>

<dsp:include otherContext="${CSRConfigurator.truContextRoot}" page="${start.page}"/>

</dsp:page>
<!-- end orderSummary.jsp -->
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/ordersummary/orderSummary.jsp#1 $$Change: 875535 $--%>
