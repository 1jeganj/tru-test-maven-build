<%@ tag language="java" %>
<%@ attribute name="order" required="true" type="atg.commerce.order.Order"%>
<%@ attribute name="isShowHeader" required="false" %>
<%@ attribute name="isDisplayBalanceDue" required="false" %>

<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" %>
<%@ taglib prefix="web-ui" uri="http://www.atg.com/taglibs/web-ui_rt"%>
<dsp:page xml="true">
  <dsp:importbean var="agentTools" bean="/atg/commerce/custsvc/util/CSRAgentTools" />
  <dsp:importbean bean="/com/tru/commerce/cart/droplet/OrderSummaryDetailsDroplet" />	
  <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
	<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
    <c:set var="isDisplayReturnCredit" value="${false}"/>
    
    <dsp:droplet name="/atg/commerce/custsvc/returns/IsReturnExchange">
      <dsp:oparam name="true">
		<%-- if the replacement order is the same as the input order, display return credit values --%>
        <dsp:getvalueof var="processName" param="returnProcessName"/>
        <c:if test="${processName == 'Exchange'}">
	        <dsp:getvalueof var="returnObject" param="returnRequest"/>
	        <c:if test="${returnObject.replacementOrder.id == order.id}">
		        <c:set var="isDisplayReturnCredit" value="${true}"/>
	        </c:if>
        </c:if>
      </dsp:oparam>
    </dsp:droplet>
    
    <dsp:droplet name="OrderSummaryDetailsDroplet">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		<dsp:getvalueof var="commerceItemCount" param="itemCount" />
		<dsp:getvalueof var="showPaypalCheckOutFlag" param="showPaypalCheckOutFlag" />
		</dsp:oparam>
		<dsp:oparam name="empty">
		<dsp:getvalueof var="commerceItemCount" value="0" />
		</dsp:oparam>
		</dsp:droplet>
      <c:if test="${empty isShowHeader || isShowHeader == true}">
        <span class="atg_commerce_csr_orderSummaryHeader"
          id="atg_commerce_csr_neworder_orderSummaryHeader">
          <fmt:message  key="displayOrderSummary.header" />
        </span>
      </c:if>
      <c:if test="${ ! (empty order.priceInfo || empty order.priceInfo.currencyCode) }">
        <table class="atg_dataForm" id="atg_commerce_csr_neworder_orderSummaryData">
          <tr>
            <td>
              <fmt:message  key="displayOrderSummary.subTotal" />
              (<c:choose>
							<c:when test="${commerceItemCount > 1 }">
										<dsp:valueof value="${commerceItemCount}"></dsp:valueof>
							</c:when>
							<c:otherwise>
								<span> 
										<dsp:valueof value="${commerceItemCount}"></dsp:valueof>
								</span>
							</c:otherwise>
						</c:choose><fmt:message  key="items" bundle="${TRUCustomResources}"/>)
            </td>
            <td class="arg_commerce_csr_orderSummaryAmount">
              <web-ui:formatNumber value="${order.priceInfo.rawSubtotal}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
            </td>
         </tr>
         <c:if test="${order.priceInfo.discountAmount ne '0.0'}"> 
            <tr>
              <td>
                <fmt:message  key="displayOrderSummary.discount"   bundle="${TRUCustomResources}"/>
              </td>
              <td class="arg_commerce_csr_orderSummaryAmount">
                <web-ui:formatNumber value="${-order.priceInfo.discountAmount}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
              </td>
          </tr>
          </c:if>
          <c:if test="${order.shipToHomeItemsCount > 0}">
          <tr>
            <td>
            	 <fmt:message key="displayOrderSummary.estimated.shipping" bundle="${TRUCustomResources}"/>
           	</td>
           	 <td class="arg_commerce_csr_orderSummaryAmount">
            	<span> <dsp:getvalueof value="${order.priceInfo.shipping}" var="estimatedShipping"></dsp:getvalueof> 
            	<c:choose>
					<c:when test="${estimatedShipping == 0.0}">
							<c:choose>
                          		<c:when test="${order.shipToHomeShippingMethodsCount > 0}">
                          			FREE
                          		</c:when>
                          		<c:otherwise>
                          			-
                          		</c:otherwise>
                        	</c:choose>
						<%-- <dsp:valueof format="currency" value="0.0" locale="en_US" converter="currency"/>	 --%>
					</c:when>
					<c:otherwise>
						<dsp:valueof format="currency" value="${estimatedShipping}" locale="en_US" converter="currency"/>
					</c:otherwise>
				</c:choose>
				</span>
			</td>
          </tr>
          </c:if>
          <dsp:getvalueof var="totalShippingSurcharge" value="${order.priceInfo.totalShippingSurcharge}"/>
          <c:if test="${totalShippingSurcharge > 0.0 }">
          <tr>
            <td>
            	<fmt:message key="displayorderSummary.surcharge"  bundle="${TRUCustomResources}"/>
            </td>
            <td class="arg_commerce_csr_orderSummaryAmount">
            <web-ui:formatNumber value="${order.priceInfo.totalShippingSurcharge}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
            </td>
          </tr>
          </c:if>
          <tr>
            <td>
            	<fmt:message key="displayorderSummary.estimated.salestax" bundle="${TRUCustomResources}"/>
            </td>
            <td class="arg_commerce_csr_orderSummaryAmount">
              	<c:choose>
                      <c:when test="${not empty order.taxPriceInfo && not empty order.taxPriceInfo.estimatedSalesTax && order.taxPriceInfo.estimatedSalesTax gt 0.0}">
                      		<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedSalesTax}" locale="en_US" converter="currency"/>
                      </c:when>
                      <c:otherwise>
                      		<span class="sales-tax"> <fmt:message key="shoppingcart.calculated.checkout" bundle="${TRUCustomResources}"/></span>
                      </c:otherwise>
               	</c:choose>
            </td>
          </tr>
         <dsp:getvalueof var="estimatedLocalTax" value="${order.taxPriceInfo.estimatedLocalTax}"/>
				<c:if test="${estimatedLocalTax gt 0.0}">
					<tr>
						<td><fmt:message key="shoppingcart.estimated.local.tax" bundle="${TRUCustomResources}"/></td>
						<td class="arg_commerce_csr_orderSummaryAmount">
						<dsp:valueof format="currency" value="${estimatedLocalTax}" locale="en_US" converter="currency" /></td>
					</tr>
				</c:if>
				<dsp:getvalueof var="estimatedIslandTax" value="${order.taxPriceInfo.estimatedIslandTax}"/>
				<c:if test="${estimatedIslandTax gt 0.0}">
					<tr>
						<td><fmt:message key="shoppingcart.estimated.island.tax" bundle="${TRUCustomResources}"/></td>
						<td class="arg_commerce_csr_orderSummaryAmount">
						<dsp:valueof format="currency" value="${order.taxPriceInfo.estimatedIslandTax}" locale="en_US" converter="currency" /></td>
					</tr>
				</c:if>

		<dsp:getvalueof var="ewasteFees" value="${order.priceInfo.ewasteFees}"/>
		 <c:choose>
		 	<c:when test="${ewasteFees gt 0.0}">
			  <tr>
                <td>
                  ewaste:
                </td>
                <td class="arg_commerce_csr_orderSummaryAmount">
                  <web-ui:formatNumber value="${order.priceInfo.ewasteFees}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                </td>
              </tr>
			</c:when>
		 </c:choose>
	     <dsp:getvalueof var="hasGiftWrapItems" vartype="java.lang.boolean" value="${order.GWItemInOrder}"/>
                                <dsp:getvalueof var="giftWrapPrice" value="${order.priceInfo.giftWrapPrice}"/>
                                <c:if test="${hasGiftWrapItems}">
	                              			  <tr>
                							<td>
	                               					<fmt:message key="gift.wrap" bundle="${TRUCustomResources}"/>
	                               					</td>
	                               					<td class="arg_commerce_csr_orderSummaryAmount">
	                                    		<c:choose>
			                               		<c:when test="${giftWrapPrice <= 0.0}">
			                                    	free
			                               		</c:when>
			                               		<c:otherwise>
			                               			<dsp:valueof format="currency"  value="${giftWrapPrice}" locale="en_US" converter="currency"/>
			                               		</c:otherwise>
			                               	</c:choose>
			                               	</td>
	                                 </tr>
	                          
                                </c:if>

          <c:choose>
            <c:when test="${isDisplayReturnCredit}">

			  <%-- display order total, followed by return credit --%>
              <tr>
                <td>
                  <fmt:message key="displayOrderSummary.orderTotal" />
                </td>
                <td class="arg_commerce_csr_orderSummaryAmount">
                  <web-ui:formatNumber value="${order.priceInfo.total}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                </td>
              </tr>
              <tr>
                <td>
                  <fmt:message key="displayOrderSummary.returnCredit" />
                </td>
                <td class="arg_commerce_csr_orderSummaryAmount">
                  <web-ui:formatNumber value="${-returnObject.totalRefundAmount}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                </td>
              </tr>

              <c:set var="balance" value="${order.priceInfo.total - returnObject.totalRefundAmount}"/>
                <c:choose>

                <c:when test="${isDisplayBalanceDue == true}"> <%-- isDisplayReturnCredit is true, isDisplayBalanceDue is true --%> 
                  <c:choose>
                    <c:when test="${returnObject.returnPaymentState == 'Refund'}"> <%-- getting a refund --%> 
                    <tr>
                      <td>
                        <span class="atg_commerce_csr_orderSummaryTotal">
                        	<fmt:message key="displayOrderSummary.refundAmount" />
                       	</span>
                      </td>
                      <td class="arg_commerce_csr_orderSummaryAmount">
                        <span class="atg_commerce_csr_orderSummaryTotal atg_csc_negativeBalance" >
                        <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <fmt:message key="displayOrderSummary.balanceDue" />
                       </td>
                      <td class="arg_commerce_csr_orderSummaryAmount" id="displayCSRCustomerPaymentBalance">
                          <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                      </td>
                    </tr>

                  </c:when>
                  <c:otherwise> <%-- have to pay --%>
                    <tr>
                      <td>
                        <span class="atg_commerce_csr_orderSummaryTotal">
                        <fmt:message key="displayOrderSummary.paymentAmount" />
                        </span>
                      </td>
                      <td class="arg_commerce_csr_orderSummaryAmount">
                        <span class="atg_commerce_csr_orderSummaryTotal atg_csc_positiveBalance" >
                        <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <fmt:message key="displayOrderSummary.balanceDue" />
                      </td>
                      <td class="arg_commerce_csr_orderSummaryAmount" id="displayCSRCustomerPaymentBalance">
                          <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                      </td>
                    </tr>
                  </c:otherwise>
                  </c:choose>
                </c:when>


                <c:otherwise> <%-- isDisplayReturnCredit is true, isDisplayBalanceDue is false --%>
                  <c:choose>
                    <c:when test="${returnObject.returnPaymentState == 'Refund'}">
                    <tr>
                      <td>
                        <span class="atg_commerce_csr_orderSummaryTotal">
                          <fmt:message key="displayOrderSummary.refundAmount" />
                        </span>
                      </td>
                      <td class="arg_commerce_csr_orderSummaryAmount">
                        <span class="atg_commerce_csr_orderSummaryTotal atg_csc_negativeBalance">
                          <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                        </span>
                      </td>
                    </tr>
                  </c:when>
                  <c:otherwise>
                    <tr>
                      <td>
                        <span class="atg_commerce_csr_orderSummaryTotal">
                          <fmt:message key="displayOrderSummary.paymentAmount" />
                        </span>
                      </td>
                      <td class="arg_commerce_csr_orderSummaryAmount">
                        <span class="atg_commerce_csr_orderSummaryTotal atg_csc_positiveBalance">
                          <web-ui:formatNumber value="${balance}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                        </span>
                      </td>
                    </tr>
                  </c:otherwise>
                  </c:choose>
                </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise> <%-- isDisplayReturnCredit is false, isDisplayBalanceDue is true --%> 
              <c:choose> 
                <c:when test="${isDisplayBalanceDue == true}">
                  <tr>
                    <td>
                      <fmt:message key="displayOrderSummary.orderTotal" />
                    </td>
                    <td class="arg_commerce_csr_orderSummaryAmount">
                      <web-ui:formatNumber value="${order.priceInfo.total}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span class="atg_commerce_csr_orderSummaryTotal">
                        <fmt:message key="displayOrderSummary.balanceDue" />
                      </span>
                    </td>
                    <td class="arg_commerce_csr_orderSummaryAmount">
                      <span class="atg_commerce_csr_orderSummaryTotal" id="displayCSRCustomerPaymentBalance">
		              		<web-ui:formatNumber value="${order.priceInfo.total}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                      </span>
                    </td>
                  </tr>
                </c:when>
                <c:otherwise>
                  <tr>
                    <td>
                      <span class="atg_commerce_csr_orderSummaryTotal">
                        <fmt:message key="displayOrderSummary.orderTotal" />
                      </span>
                    </td>
                    <td class="arg_commerce_csr_orderSummaryAmount">
                      <span class="atg_commerce_csr_orderSummaryTotal atg_csc_positiveBalance">
                       <web-ui:formatNumber value="${order.priceInfo.total}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>
                      </span>
                    </td>
                  </tr>
                  <tr>
                  <td>
                  <c:if test="${order.priceInfo.totalSavings gt 0.0}">
								<fmt:message key="shoppingcart.saved" bundle="${TRUCustomResources}"/>&nbsp;
								<web-ui:formatNumber value="${order.priceInfo.totalSavings}" type="currency" currencyCode="${order.priceInfo.currencyCode}"/>&nbsp;
								<fmt:message key="shoppingcart.left.bracket" bundle="${TRUCustomResources}"/><web-ui:formatNumber value="${order.priceInfo.totalSavingsPercentage}" type="number" currencyCode="${order.priceInfo.currencyCode}"/><fmt:message key="shoppingcart.percentage" bundle="${TRUCustomResources}"/><fmt:message key="shoppingcart.right.bracket" bundle="${TRUCustomResources}"/>&nbsp;<fmt:message key="shoppingcart.on.this.order" bundle="${TRUCustomResources}"/>
				 </c:if>
                  </td>
                  </tr>
                </c:otherwise>
              </c:choose>
            </c:otherwise>
          </c:choose>
        </table>
      </c:if>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/WEB-INF/tags/displayOrderSummary.tag#1 $$Change: 875535 $--%>
