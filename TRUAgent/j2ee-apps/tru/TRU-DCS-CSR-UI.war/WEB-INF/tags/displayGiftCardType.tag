<%@ tag language="java"%>
<%@ attribute name="giftCard" required="true"  type="com.tru.commerce.order.TRUGiftCard"%> 

<%@ taglib prefix="dsp"
  uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%-- This tag will display the gift card number in a given format --%>
<dsp:page xml="true">
  <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
    	<fmt:message key="billing.giftcard.label" bundle="${TRUCustomResources}"/>&nbsp;
    	<fmt:message key="common.hyphen" />&nbsp;
  </dsp:layeredBundle>
    <c:if test="${!empty giftCard && !empty giftCard.giftCardNumber}">
      <c:set var="cardNumberIndex" value="${fn:length(giftCard.giftCardNumber) - 4}"/>
      <c:out value="${fn:substring(giftCard.giftCardNumber, cardNumberIndex, -1)}"/>
    </c:if>
</dsp:page>
