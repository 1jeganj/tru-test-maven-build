<%@ tag language="java" %>

<%@ attribute name="commerceItem" required="false" type="atg.adapter.gsa.GSAItem" %>
<%@ attribute name="commerceItemId" required="false" %>
<%@ attribute name="customerPurchaseLimit" required="false" %>
<%@ attribute name="preSellable" required="false" %>
<%@ attribute name="preSellQtyUnits" required="false" %>
<%@ attribute name="formattedDate" required="false" %>
<%@ attribute name="nmwaEligible" required="false" %>




<%@ variable name-given="fullInventoryStatus"
    variable-class="java.lang.String"
    scope="AT_BEGIN"
    description="A string description of the status" %>

<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_0" %>

<c:catch var="exception">
  <dsp:page>
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
    <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" /> 
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
     <c:if test="${empty commerceItemId}">
      <dsp:tomap var="ci" value="${commerceItem}"/>
      <c:set var="commerceItemId" value="${ci.id}"/>
    </c:if>
     <style type="text/css">
    	#atg_commerce_csr_catalog_notifyMeEmailPopup
    	{
    		width:850px !important;
    		max-width:850px !important;
    	}
    </style>
    <dsp:droplet name="TRUCSRInventoryLookupDroplet">
      <dsp:param name="skuId" value="${commerceItemId}"/>
      <dsp:param name="locationIdSupplied" value="false"/>
      <dsp:oparam name="output">
        <dsp:droplet name="Switch">
          <dsp:param name="value" param="availability"/>
          <dsp:oparam name="1001">
            <fmt:message key="global.product.availabilityStatus.outOfStock"/>
            <c:set var="inventoryStatus" value="outofstock"/>
            </br>
            <c:if test="${nmwaEligible}">
           		 <a href="#" id="out-of-stock-text" onclick="loadNotifyMe('${commerceItemId}');">email me when available</a>
            </c:if>
          </dsp:oparam>
          <dsp:oparam name="1002">
            <fmt:message key="global.product.availabilityStatus.preorder"/>
            <c:set var="inventoryStatus" value="preorder"/>
          </dsp:oparam>
          <dsp:oparam name="1003">
            <fmt:message key="global.product.availabilityStatus.backorder"/>
            <c:set var="inventoryStatus" value="backorder"/>
          </dsp:oparam> 
          <dsp:oparam name="1000">
          <c:if test="${!preSellable}"> 
            <b>
              <dsp:getvalueof var="stockLevel" param="stockLevel"/>
              ${stockLevel}
            </b>
            <fmt:message key="global.product.availabilityStatus.inStock"/>
            <c:set var="inventoryStatus" value="instock"/>
            </c:if>
            <c:if test="${preSellable}"> 
            <b>
              <dsp:getvalueof var="stockLevel" param="stockLevel"/>
              ${stockLevel}
            </b>
            <fmt:message key="global.product.availabilityStatus.inStock"/></br>
            <c:set var="inventoryStatus" value="instock"/>
             <fmt:message key="pre.order" bundle="${TRUCustomResources}" />
             </br><fmt:message key="estimated.delivery.date" bundle="${TRUCustomResources}" />${formattedDate}
            </c:if>
          </dsp:oparam>
        </dsp:droplet>
      </dsp:oparam>
    </dsp:droplet>
    
    <input type="hidden" id="stockLevel_${commerceItemId}" value="${stockLevel}"/> 
    <input type="hidden" id="inventoryStatus_${commerceItemId}" value="${inventoryStatus}"/>
    <input type="hidden" id="customerPurchaseLimit_${commerceItemId}" value="${customerPurchaseLimit}"/>
    </dsp:layeredBundle>
    <script type="text/javascript">
    if (!dijit.byId("atg_commerce_csr_catalog_notifyMeEmailPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_notifyMeEmailPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
    if (!dijit.byId("atg_commerce_csr_catalog_privacyDetailsPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_privacyDetailsPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
   	function loadNotifyMe(skuId)
	{
		var displayName = $("#skuDisplayName_"+skuId).val();
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Notify Me',
            url: "/TRU-DCS-CSR/panels/order/notifyMePopUp.jsp?skuId="+skuId+"&skuDisplayName="+displayName,
            });	
	}
   	function showPrivacyDetails()
	{
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Privacy Policy',
            url: "/TRU-DCS-CSR/panels/order/privacyDetails.jsp",
            onClose: function(args) {
            	
            }});	
	}
   	function submitNotifyMeDetails(skuId)
	{
		
		var emailId = dojo.byId("notifyMeMailId").value;
  	  	var itemProductId = $("#itemProductId_"+skuId).val();
  	    var catalogRefId= $("#catalogRefId_"+skuId).val();
  		var currentSiteId = $("#currentSiteId_"+skuId).val();
  		var productUrl=$("#productPageUrl_"+skuId).val();
  		var dsiplayName=$("#skuDisplayName_"+skuId).val();
  		var description=$("#description_"+skuId).val();
  		var onlinePID=$("#onlinePID_"+skuId).val();
  		
  		var sendSpecialofferToMail = $("#notify-checkbox").is(":checked");
  		if(isValidEmail(emailId)){
    		dojo.xhrPost({url:"/TRU-DCS-CSR/panels/order/submitNotifyDetails.jsp?notifyMeMailId="+emailId+"&itemProductId="+itemProductId+"&catalogRefId="+catalogRefId+"&currentSiteId="+currentSiteId+"&productPageUrl="+productUrl+"&sendSpecialofferToMail="+sendSpecialofferToMail+"&skuDisplayName="+dsiplayName+"&description="+description+"&onlinePID="+onlinePID,content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(responce){
    			var msg = $.trim(responce);
    			if(msg.indexOf("Following are the form errors:") > -1)
  				{
  					var msg1 = msg.split(":")[1];
  					$("#successOrErrorMsg").html(msg1).css("color","red").show();
  				}
    			else{
	 				 	dojo.style(dojo.byId('notifyMe_content'), "display", "none");
	 				 	$("#successOrErrorMsg").html(msg).css({'color':"#000",'margin-top':"33px"}).show();
	 				
	    		 	}
  			},mimetype:"text/html"});
      	}
  	  	else{
  	  		$("#successOrErrorMsg").html("Please enter the email Id in the correct format.").css("color","red");
  	  	}
	}
	function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	}
   </script> 
    
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <% 
    Exception ee = (Exception) jspContext.getAttribute("exception"); 
    ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/WEB-INF/tags/fullInventoryStatus.tag#1 $$Change: 875535 $--%>
