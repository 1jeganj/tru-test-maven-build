<%--

  Implements the checkbox to set a given address to the default of
  some type. The type is determined by the
  DefaultAddressReferenceManger's address.

@param options the defaultAddressEditorOptions map
@param formHandlerPath the component path of the AddressBookFormHandler
@param formHandler the form handler varible from the dspel:importbean tag

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/addresses/defaultFlagEditor.jsp#1 $$Change: 875535 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:getvalueof var="fhp" param="formHandlerPath"/>
  <dsp:getvalueof var="fh" param="formHandler"/>
  <dsp:getvalueof var="opts" param="options"/>
  <dsp:getvalueof var="optsKey" param="optionsKey"/>
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
		<c:choose>
			<c:when test="${opts.set eq true}">
				<c:set var="defaultValue" value="true"/>
			</c:when>
			<c:otherwise>
				<c:set var="defaultValue" value="false"/>
			</c:otherwise>
		</c:choose>
       <dsp:input type="checkbox" checked="${opts.set}"
        disabled="${opts['default'] or not opts.canSetDefault}"
        bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.default${optsKey}" iclass="defaultAdd_checkBox" value="${defaultValue}"/>
      <fmt:message key="${opts.resourceMessageKey}"/> 
      
  </dsp:layeredBundle>
</dsp:page>


<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/addresses/defaultFlagEditor.jsp#1 $$Change: 875535 $--%>
