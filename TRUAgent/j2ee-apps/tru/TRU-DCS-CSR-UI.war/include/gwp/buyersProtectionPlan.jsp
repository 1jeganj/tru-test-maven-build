<%--
This page produces the gwp link for an item in the order. 
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/gwp/buyersProtectionPlan.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<style type="text/css">
/***************Cart page square trade start ********/
.protection-plan {
    box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px 0px;
    box-sizing: border-box;
    height: 85px;
    width: 690px;
    background: rgb(243, 243, 243) none repeat scroll 0% 0% / auto padding-box border-box;
    font-family:'Avenir Light';
    font-size:14px;
    margin: 25px 0px;
    font-family : "Arial";
    
}

.inline-protection-plan {
    box-sizing: border-box;
    color: rgb(102, 102, 102);
    display: inline-block;
    height: 85px;
    width: 145px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(102, 102, 102);
}

.buyer-protection-plan-image-protection-plan {
    box-sizing: border-box;
    color: rgb(102, 102, 102);
    display: inline-block;
    height: 60px;
    left: 15px;
    width: 112px;
    background: rgb(255, 255, 255) url(/TRU-DCS-CSR/images/CSC_ST_logo.jpg) no-repeat scroll 0% 0% / cover padding-box border-box;
    font-size:14px;
    margin: 0px 10px 0px 0px;
}

.inline2-protection-plan {
    display: inline-block;
    letter-spacing: -0.351999998092651px;
	margin-left:15px;
    
}

.checkbox-sm-off {
font-size:14px;
}

.protection-plan .checkbox-group>div:first-child {
    margin-right: 5px;
}

.checkbox-sm-on {
    height: 16px;
    width: 16px;
    background-image: url(/TRU-DCS-CSR/images/checkbox-sm-on.jpg);
    cursor: pointer;
    vertical-align: middle;
    display: inline-block;
    background-repeat: no-repeat;
}

.checkbox-sm-off {
    height: 16px;
    width: 16px;
    background-image: url(/TRU-DCS-CSR/images/checkbox-sm-off.jpg);
    cursor: pointer;
    vertical-align: middle;
    display: inline-block;
    background-repeat: no-repeat;
}
.protection-plan p{
	font-size: 11px;
}

.checkbox-group{
font-size:14px;
}

.inline2-protection-plan a {
margin-left:15px;
}

.protection-plan-static-copy {
	font-weight:bold;
	padding-left:60px;
}
.buyer-protection-plan-image {
    width: 112px;
    height: 60px;
    background-image: url('/TRU-DCS-CSR/images/CSC_ST_logo.jpg');
    background-size: cover;
    background-repeat: no-repeat;
}
.protection-plan > div:first-child {
    background-color: #fff;
    height: 100%;
    width: 145px;
}
.inline {
    display: inline-block;
}

.protection-plan > div:last-child {
    width: 75%;
}
.protection-plan > div:last-child {
    margin-left: 5px;
    position: relative;
}
#cmcCompleteOrderPContent .atg_commerce_csr_content .atg_commerce_csr_itemDesc li.buyersProtection-plan:has( > div.protection-plan)
{
	height:125px;
}
  
/***************Cart page square trade end ********/

  </style>
 
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
	
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="multiShipping" param="multiShipping"/>
	<c:choose>
		<c:when test="${multiShipping}">
			<dsp:getvalueof var="relationshipItemId" param="item.id" />
			<dsp:getvalueof var="bppItemInfoId" param="item.bppItemInfoId" />
			<dsp:getvalueof var="bppSkuId" param="item.bppSkuId" />
			<dsp:getvalueof var="bppItemId" param="item.bppItemId" />
			<dsp:getvalueof var="itemQty" param="item.quantity"/>
			<dsp:getvalueof var="bppItemCount" param="item.bppItemCount" />	
			<dsp:droplet name="ForEach">
       			<dsp:param name="array" param="item.bppItemList"/>
       			<dsp:param name="elementName" value="bppItem" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
				<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
			</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="item.bppItemList" />
				<dsp:oparam name="false">
					<c:choose>
					<c:when test="${pageName ne 'Confirm'}">
					<div class="protection-plan">
							<div class="inline">
								<div class="buyer-protection-plan-image inline"></div>
							</div>
							<div class="inline">
							
								<div class="checkbox-group">
									<dsp:droplet name="IsEmpty">
										<dsp:param name="value" value="${bppItemInfoId}" />
										<dsp:oparam name="false">
											<input type="checkbox" class="buyer-protection-plan"  checked="checked" onchange= "javascript:selectProtectionPlan(event,this);" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}" data-pageName="${pageName}" />
										</dsp:oparam>
										<dsp:oparam name="true">									
											<input type="checkbox" class="buyer-protection-plan"  onchange= "javascript:selectProtectionPlan(event,this);" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}"  data-pageName="${pageName}"/>
										</dsp:oparam>
									</dsp:droplet>
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="item.bppItemCount" />
										<dsp:oparam name="1">
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="item.bppItemList" />
												<dsp:param name="elementName" value="bppItem" />
												<dsp:oparam name="outputStart">
													<dsp:getvalueof var="bppTerm"
														param="bppItem.bppTerm" />
													<fmt:message key="shoppingcart.protect.it.label" bundle="${TRUCustomResources}">
														<fmt:param value="${bppTerm}"></fmt:param>
													</fmt:message>
													<fmt:message key="shoppingcart.protect.plan.label" bundle="${TRUCustomResources}"/>
													<a target="popup" href="javascript:void(0);"
														onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
														<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
													</a>
												</dsp:oparam>
												<dsp:oparam name="output">
													<span class="protection-plan-static-copy"> <dsp:getvalueof
															var="bppRetail" param="bppItem.bppRetail" /> <fmt:setLocale
															value="en_US" /> <fmt:formatNumber
															value="${bppRetail*itemQty}" type="currency" />
													</span>
												</dsp:oparam>
											</dsp:droplet>
										</dsp:oparam>
										<dsp:oparam name="default">
										<fmt:message
												key="shoppingcart.bpp.multiple.option.text" bundle="${TRUCustomResources}"/>
											<a target="popup" href="javascript:void(0);"
												onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
												<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
											</a>
											<dsp:droplet name="IsEmpty">
												<dsp:param name="value" value="${bppItemInfoId}" />
												<dsp:oparam name="false">
												<select class="bppPlanMultiShippingPage"
														id="bppPlan_${relationshipItemId}"
														data-pageName="${pageName}"
														data-relId="${relationshipItemId}"
														data-bppInfoId="${bppItemInfoId}"
														data-bppSkuId="${bppSkuId}">
												</dsp:oparam>
												<dsp:oparam name="true">
												<select class="bppPlanMultiShippingPage"
														id="bppPlan_${relationshipItemId}"
														data-pageName="${pageName}"
														disabled="disabled"
														data-relId="${relationshipItemId}"
														data-bppInfoId="${bppItemInfoId}"
														data-bppSkuId="${bppSkuId}">
												</dsp:oparam>
											</dsp:droplet>
											<option value="select"><fmt:message key="checkout.review.selectPlan" bundle="${TRUCustomResources}"/></option>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="item.bppItemList" />
												<dsp:param name="elementName" value="bppItem" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="itemId"
														param="bppItem.bppItemId" />
													<dsp:droplet name="Switch">
														<dsp:param name="value"
															param="bppItem.preSelected" />
														<dsp:oparam name="true">
															<option value="${itemId}" selected="selected">
																<dsp:valueof param="bppItem.bppPlan" />
															</option>
														</dsp:oparam>
														<dsp:oparam name="false">
															<option value="${itemId}">
																<dsp:valueof param="bppItem.bppPlan" />
															</option>
														</dsp:oparam>
													</dsp:droplet>
												</dsp:oparam>
											</dsp:droplet>
											</select>
										</dsp:oparam>
									</dsp:droplet>
								</div>
								<p>
									<fmt:message key="shoppingcart.protect.plan.message" bundle="${TRUCustomResources}"/></
								</p>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<dsp:droplet name="IsEmpty">
							<dsp:param name="value" value="${bppItemInfoId}" />
							<dsp:oparam name="false">
								<div class="protection-plan">
									<div class="inline">
										<div class="buyer-protection-plan-image inline"></div>
									</div>
									<div class="inline">
										<div class="checkbox-group">
											<fmt:message
												key="shoppingcart.protect.it.label" bundle="${TRUCustomResources}"/>
											<span> <fmt:message key="checkout.square.trade.plan" bundle="${TRUCustomResources}"/>
											</span> <a target="popup" href="javascript:void(0);"
												onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
												<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
											</a>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="item.bppItemList" />
												<dsp:param name="elementName" value="bppItem" />
												<dsp:oparam name="output">
													<span class="protection-plan-static-copy"> <dsp:valueof
															param="bppItem.bppPlan" />
													</span>
												</dsp:oparam>
											</dsp:droplet>
										</div>
										<p>	<fmt:message 	key="shoppingcart.protect.plan.message" bundle="${TRUCustomResources}" /></p>
									</div>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</c:otherwise>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
			</c:when>
			<c:otherwise>
			<dsp:getvalueof var="item" param="item"/>
			<dsp:getvalueof var="relationshipItemId" param="item.id" />
			<dsp:getvalueof var="bppItemInfoId" param="item.bppItemInfoId" />
			<dsp:getvalueof var="bppSkuId" param="item.bppSkuId" />
			<dsp:getvalueof var="bppItemId" param="item.bppItemId" />
			<dsp:getvalueof var="bppItemCount" param="item.bppItemCount" />
			<dsp:getvalueof var="itemQty" param="item.quantity"/>
			<dsp:droplet name="ForEach">
       			<dsp:param name="array" param="item.bppItemList"/>
       			<dsp:param name="elementName" value="bppItem" />
				<dsp:oparam name="output">
				<dsp:getvalueof var="itemId" param="bppItem.bppItemId"/>
				<dsp:getvalueof var="bppSKUId" param="bppItem.bppSkuId"/>
			</dsp:oparam>
			</dsp:droplet>
				<dsp:droplet name="IsEmpty">
					<dsp:param name="value" param="item.bppItemList" />
					<dsp:oparam name="false">
						<c:choose>
						<c:when test="${pageName ne 'Confirm'}">
						<div class="protection-plan">
								<div class="inline">
									<div class="buyer-protection-plan-image inline"></div>
								</div>
								<div class="inline">
								
									<div class="checkbox-group">
										<dsp:droplet name="IsEmpty">
											<dsp:param name="value" value="${bppItemInfoId}" />
											<dsp:oparam name="false">
												<input type="checkbox" class="buyer-protection-plan"  checked="checked" onchange= "javascript:selectProtectionPlan(event,this);" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}" data-pageName="${pageName}" />
											</dsp:oparam>
											<dsp:oparam name="true">									
												<input type="checkbox" class="buyer-protection-plan"  onchange= "javascript:selectProtectionPlan(event,this);" data-relId="${relationshipItemId}" data-bppInfoId="${bppItemInfoId}" data-bppSkuId="${bppSKUId}" data-bppItemId="${bppItemId}" data-bppItemCount="${bppItemCount}"  data-pageName="${pageName}"/>
											</dsp:oparam>
										</dsp:droplet>
										<dsp:droplet name="Switch">
											<dsp:param name="value" param="item.bppItemCount" />
											<dsp:oparam name="1">
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="item.bppItemList" />
													<dsp:param name="elementName" value="bppItem" />
													<dsp:oparam name="outputStart">
														<dsp:getvalueof var="bppTerm"
															param="bppItem.bppTerm" />
														<fmt:message key="shoppingcart.protect.it.label" bundle="${TRUCustomResources}">
															<fmt:param value="${bppTerm}"></fmt:param>
														</fmt:message>
														<fmt:message key="shoppingcart.protect.plan.label" bundle="${TRUCustomResources}"/>
														<a target="popup" href="javascript:void(0);"
															onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
															<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
														</a>
													</dsp:oparam>
													<dsp:oparam name="output">
														<span class="protection-plan-static-copy"> <dsp:getvalueof
																var="bppRetail" param="bppItem.bppRetail" /> <fmt:setLocale
																value="en_US" /> <fmt:formatNumber
																value="${bppRetail*itemQty}" type="currency" />
														</span>
													</dsp:oparam>
												</dsp:droplet>
											</dsp:oparam>
											<dsp:oparam name="default">
											<fmt:message
													key="shoppingcart.bpp.multiple.option.text" bundle="${TRUCustomResources}"/>
												<a target="popup" href="javascript:void(0);"
													onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
													<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
												</a>
												<dsp:droplet name="IsEmpty">
													<dsp:param name="value" value="${bppItemInfoId}" />
													<dsp:oparam name="false">
													<select class="bppPlanMultiShippingPage"
															id="bppPlan_${relationshipItemId}"
															data-pageName="${pageName}"
															data-relId="${relationshipItemId}"
															data-bppInfoId="${bppItemInfoId}"
															data-bppSkuId="${bppSKUId}">
													</dsp:oparam>
													<dsp:oparam name="true">
													<select class="bppPlanMultiShippingPage"
															id="bppPlan_${relationshipItemId}"
															data-pageName="${pageName}"
															disabled="disabled"
															data-relId="${relationshipItemId}"
															data-bppInfoId="${bppItemInfoId}"
															data-bppSkuId="${bppSKUId}">
													</dsp:oparam>
												</dsp:droplet>
												<option value="select"><fmt:message key="checkout.review.selectPlan" bundle="${TRUCustomResources}"/></option>
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="item.bppItemList" />
													<dsp:param name="elementName" value="bppItem" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="itemId"
															param="bppItem.bppItemId" />
														<dsp:droplet name="Switch">
															<dsp:param name="value"
																param="bppItem.preSelected" />
															<dsp:oparam name="true">
																<option value="${itemId}" selected="selected">
																	<dsp:valueof param="bppItem.bppPlan" />
																</option>
															</dsp:oparam>
															<dsp:oparam name="false">
																<option value="${itemId}">
																	<dsp:valueof param="bppItem.bppPlan" />
																</option>
															</dsp:oparam>
														</dsp:droplet>
													</dsp:oparam>
												</dsp:droplet>
												</select>
											</dsp:oparam>
										</dsp:droplet>
									</div>
									<p>
										<fmt:message key="shoppingcart.protect.plan.message" bundle="${TRUCustomResources}"/></
									</p>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<dsp:droplet name="IsEmpty">
								<dsp:param name="value" value="${bppItemInfoId}" />
								<dsp:oparam name="false">
									<div class="protection-plan">
										<div class="inline">
											<div class="buyer-protection-plan-image inline"></div>
										</div>
										<div class="inline">
											<div class="checkbox-group">
												<fmt:message
													key="shoppingcart.protect.it.label" bundle="${TRUCustomResources}"/>
												<span> <fmt:message key="checkout.square.trade.plan" bundle="${TRUCustomResources}"/>
												</span> <a target="popup" href="javascript:void(0);"
													onclick="window.open('/TRU-DCS-CSR/include/gwp/bppLearnMore.jsp', 'popup','width=600,height=600')">
													<fmt:message key="shoppingcart.protect.learn.label" bundle="${TRUCustomResources}"/>
												</a>
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="item.bppItemList" />
													<dsp:param name="elementName" value="bppItem" />
													<dsp:oparam name="output">
														<span class="protection-plan-static-copy"> <dsp:valueof
																param="bppItem.bppPlan" />
														</span>
													</dsp:oparam>
												</dsp:droplet>
											</div>
											<p>	<fmt:message 	key="shoppingcart.protect.plan.message" bundle="${TRUCustomResources}" /></p>
										</div>
									</div>
								</dsp:oparam>
							</dsp:droplet>
						</c:otherwise>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>
			</c:otherwise>
			</c:choose>
</dsp:page> 