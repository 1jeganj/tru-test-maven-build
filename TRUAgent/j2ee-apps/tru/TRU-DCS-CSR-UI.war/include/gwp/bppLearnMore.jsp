 <%@ include file="/include/top.jspf"%>
<dsp:page xml="true">
	<dsp:importbean var="site" bean="/atg/multisite/Site" />
	<dsp:getvalueof var="currentSiteId" bean="Site.id" />
	<dsp:importbean var="locale" bean="/atg/userprofiling/ActiveCustomerProfile.locale" />
	<dsp:getvalueof var="atgTargeterpath" bean="/atg/registry/RepositoryTargeters/TRU/ShoppingCart/BPPLearnMoreLinkTargeter.AbsoluteName" />
	<dsp:getvalueof var="cacheKey" value="${atgTargeterpath}${siteId}${locale}" />
		<dsp:droplet name="/com/tru/cache/TRUBPPLearnMoreLinkTargeterCacheDroplet">
			<dsp:param name="key" value="${cacheKey}" />
			<dsp:oparam name="output">
				<dsp:droplet name="/atg/targeting/TargetingForEach">
					<dsp:param name="targeter" bean="${atgTargeterpath}" />
					<dsp:oparam name="output">
						<dsp:valueof param="element.data" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
</dsp:page> 