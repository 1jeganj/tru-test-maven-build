<%--
This page produces the gwp link for an item in the order. 
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/gwp/changeGiftLink.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>

<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<style type="text/css">

/***************Cart page gift box start ***************/

.gift.inline {
	display: inline-block;
}
.checkbox-sm-on {
height: 16px;
width: 16px;
background-image: url('/TRU-DCS-CSR/images/checkbox-sm-on.jpg');
cursor: pointer;
vertical-align: middle;
display: inline-block;
background-repeat: no-repeat;
}
.checkbox-sm-off {
	height: 16px;
	width: 16px;
	background-image: url('/TRU-DCS-CSR/images/checkbox-sm-off.jpg');
	cursor: pointer;
	vertical-align: middle;
	display: inline-block;
	background-repeat: no-repeat;
}
.shopping-cart-gift-image {
	width: 25px;
	height: 25px;
	background-image: url('/TRU-DCS-CSR/images/cart-sprite-2.png');
	background-position: 0px -180px;
	vertical-align: middle;
}

/***************Cart page gift box end ***************/

/***************Cart page square trade start ********/
.protection-plan {
    box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 3px 0px;
    box-sizing: border-box;
    height: 85px;
    width: 690px;
    background: rgb(243, 243, 243) none repeat scroll 0% 0% / auto padding-box border-box;
    font-family:'Avenir Light';
    font-size:14px;
    margin: 25px 0px;
    font-family : "Arial";
    
}

.inline-protection-plan {
    box-sizing: border-box;
    color: rgb(102, 102, 102);
    display: inline-block;
    height: 85px;
    width: 145px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(102, 102, 102);
}

.buyer-protection-plan-image-protection-plan {
    box-sizing: border-box;
    color: rgb(102, 102, 102);
    display: inline-block;
    height: 60px;
    left: 15px;
    width: 112px;
    background: rgb(255, 255, 255) url(/TRU-DCS-CSR/images/CSC_ST_logo.jpg) no-repeat scroll 0% 0% / cover padding-box border-box;
    font-size:14px;
    margin: 0px 10px 0px 0px;
}

.inline2-protection-plan {
    display: inline-block;
    letter-spacing: -0.351999998092651px;
	margin-left:15px;
    
}

.checkbox-sm-off {
font-size:14px;
}


.protection-plan p{
	font-size: 11px;
}

.checkbox-group{
font-size:14px;
}

.inline2-protection-plan a {
margin-left:15px;
}

.protection-plan-static-copy {
	font-weight:bold;
	padding-left:60px;
}

/***************Cart page square trade end ********/
  </style>
<script type="text/javascript">

function updateGiftItem(obj,shipGroupId, commerceItemRelationshipId, commerceItemId, quantity, giftWrapProductId, giftWrapSkuId){
		$(obj).toggleClass('checkbox-sm-off checkbox-sm-on');
		var giftItem = $(obj).hasClass('checkbox-sm-on');
		$("#ShipGroupId").val(shipGroupId);
		$("#CommerceItemRelationshipId").val(commerceItemRelationshipId);
		$("#CommerceItemId").val(commerceItemId);
		$("#Quantity").val(quantity);
		$("#GiftWrapProductId").val(giftWrapProductId);
		$("#GiftWrapSkuId").val(giftWrapSkuId);
		$("#GiftItem").val(giftItem);
		
		dojo.xhrPost({form:document.getElementById('giftItemForm'),url:"/TRU-DCS-CSR/include/gwp/changeGiftLink.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
 			if(!(_1f instanceof Error)){
     				atgNavigate({panelStack:"cmcShoppingCartPS", queryParams: { init : 'true' }});return false;
        		 }
			},mimetype:"text/html"});	
	} 

<%-- change gift popup --%>
dojo.addOnLoad(function () {
  if (!dijit.byId("selectGiftPopup")) {
    new dojox.Dialog({ id: "selectGiftPopup",
                       cacheContent: "false",
                       executeScripts: "true",
                       scriptHasHooks: "true",
                        duration: 100,
                       "class": "atg_commerce_csr_popup"});
  }
});

 



</script>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean var="changeGiftPopupPageFragment" bean="/atg/commerce/custsvc/ui/fragments/gwp/ChangeGiftPopupPageFragment" />
<dsp:getvalueof var="item" param="item"/>
<dsp:getvalueof var="order" param="order"/>
<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" />
<dsp:getvalueof var="pageName" param="pageName" />
<dsp:getvalueof var="currentItem" param="currentItem"/>
<dsp:getvalueof var="existingRelationShipId" param="existingRelationShipId" />
<dsp:getvalueof var="multiShipping" param="multiShipping"/>
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />

	<c:choose>
		<c:when test="${multiShipping}">
			<dsp:getvalueof var="itemShippingGroupId" param="itemShippingGroupId"/>
			<c:forEach items="${order.shippingGroups}" var="shippingGroup" varStatus="vs">
            	<c:if test="${itemShippingGroupId == shippingGroup.id}">
            	  <dsp:getvalueof var="shippingGroupObject" value="${shippingGroup}"/>
            	 </c:if>
            </c:forEach>
         <dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.color" />
		<dsp:oparam name="false">
				Color :
				<dsp:valueof param="item.color" /><br>
		</dsp:oparam>
	</dsp:droplet>
		 	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.size" />
		<dsp:oparam name="false">
				Size : <dsp:valueof param="item.size" />
		</dsp:oparam>
	</dsp:droplet>
			<dsp:getvalueof var="commerceItemRelationships" value="${shippingGroupObject.commerceItemRelationships}"/>
				<c:forEach items="${commerceItemRelationships}" var="ciRels">
						<dsp:getvalueof var="commerceItemRelationshipId" value="${ciRels.id}"/>
						<dsp:getvalueof var="isGiftItem" value="${ciRels.isGiftItem}"></dsp:getvalueof>
						<dsp:getvalueof var="commerceItemId" value="${ciRels.commerceItem.id}"></dsp:getvalueof>
						<c:if test="${commerceItemId eq item.commerceItemId}">
						<c:set var="class" value="gift-checkbox checkbox-sm-off"></c:set>
						<c:if test="${isGiftItem}">
								<c:set var="class" value="gift-checkbox checkbox-sm-on"></c:set>
						</c:if>
						
						<c:if test="${ciRel.commerceItem.commerceItemClassType ne 'giftWrapCommerceItem'}">
						<div class="shopping-cart-gift" id="co-shopping-cart-gift">
									<div class="${class}" id="${item.commerceItemId}" onclick="updateGiftItem(this,'${item.shipGroupId}', 
										'${item.id}', 
										'${item.commerceItemId}', '${item.quantity}', 
										'${item.wrapProductId}' ,
										'${item.wrapSkuId}');">
									</div>
									<div class="shopping-cart-gift-image gift inline"></div>
									<dsp:getvalueof var="giftWrapName" param="item.wrapDisplayName"></dsp:getvalueof>
									<div class="gift inline">
										<fmt:message key="shoppingcart.gift.with.label" bundle="${TRUCustomResources}">
												<fmt:param value="${giftWrapName}"></fmt:param>
										</fmt:message>
									</div>
						</div>
						</c:if>
 					</c:if>
 			</c:forEach>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.color" />
		<dsp:oparam name="false">
				Color :
				<dsp:valueof param="item.color" /><br>
		</dsp:oparam>
	</dsp:droplet>
		 	<dsp:droplet name="IsEmpty">
		<dsp:param name="value" param="item.size" />
		<dsp:oparam name="false">
				Size : <dsp:valueof param="item.size" /><br/>
		</dsp:oparam>
	</dsp:droplet>
		
				<dsp:getvalueof var="check" param="item.commerceItemId"></dsp:getvalueof>
		<dsp:getvalueof var="commerceItem" value="${item.wrapSkuId}"></dsp:getvalueof>
		<dsp:getvalueof var="qty" value="${item.quantity}" />
		<dsp:getvalueof var="check" value="${item.commerceItemId}"></dsp:getvalueof>
		<dsp:getvalueof var="currentItem" param="currentItem"/>
		<c:forEach items="${order.shippingGroups}" var="shippingGroupItem">
			<dsp:getvalueof var="commerceItemRels" value="${shippingGroupItem.commerceItemRelationships}"/>
			<c:forEach items="${commerceItemRels}" var="ciRel">
			<dsp:getvalueof var="commerceItemRelationshipId" value="${ciRel.id}"/>
						<dsp:getvalueof var="isGiftItem" value="${ciRel.isGiftItem}"></dsp:getvalueof>
						<dsp:getvalueof var="commerceItemId" value="${ciRel.commerceItem.id}"></dsp:getvalueof>
						 <c:if test="${commerceItemId eq item.commerceItemId}"> 
						<c:set var="class" value="gift-checkbox checkbox-sm-off"></c:set>
					 <c:if test="${isGiftItem}"> 
								<c:set var="class" value="gift-checkbox checkbox-sm-on"></c:set>
						 </c:if>
						 <c:if test="${ciRel.commerceItem.commerceItemClassType ne 'giftWrapCommerceItem'}">
						<div class="shopping-cart-gift" id="co-shopping-cart-gift">
									<div class="${class}" id="${item.id}" onclick="updateGiftItem(this,'<dsp:valueof value="${item.shipGroupId}" />', 
										'${item.id}', 
										'${item.commerceItemId}', '${qty}', 
										'${item.wrapProductId}' ,
										'${item.wrapSkuId}');">
									</div>
									<div class="shopping-cart-gift-image gift inline"></div>
									<dsp:getvalueof var="giftWrapName" param="item.wrapDisplayName"></dsp:getvalueof>
									<div class="gift inline">
										<fmt:message key="shoppingcart.gift.with.label" bundle="${TRUCustomResources}">
												<fmt:param value="${giftWrapName}"></fmt:param>
										</fmt:message>
									</div>
						</div>
						</c:if>
					</c:if>
				
			</c:forEach>
		</c:forEach>

		</c:otherwise>
	</c:choose>

<%-- 	</dsp:form>	 --%>
</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/gwp/changeGiftLink.jsp#1 $$Change: 875535 $--%>
