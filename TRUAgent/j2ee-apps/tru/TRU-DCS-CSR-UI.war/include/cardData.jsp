<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
      <dsp:importbean var="cctools" bean="/atg/commerce/payment/CreditCardTools" />
      <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
      
        {identifier:"abbreviation",
        items: [
          <c:forEach var="card" items="${cctools.cardTypesMap}" varStatus="status">
            <c:if test="${status.index > 0}">,</c:if>{name:"<fmt:message key="${fn:toLowerCase(card.key)}"  bundle="${TRUCustomResources}"/>",label:"<fmt:message key="${fn:toLowerCase(card.key)}"  bundle="${TRUCustomResources}"/>",abbreviation:"${card.key}"}
          </c:forEach>
        ]}
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <%
     Exception ee = (Exception) pageContext.getAttribute("exception");
     ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/cardData.jsp#1 $$Change: 875535 $--%>
