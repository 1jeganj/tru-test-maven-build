<%@ include file="/include/top.jspf" %>

<dsp:page xml="true">
<dsp:importbean bean="/atg/commerce/order/purchase/BillingFormHandler" />
	<dsp:setvalue bean="BillingFormHandler.getNONCE" value="true" />
	{
		"nonce": "<dsp:valueof bean="BillingFormHandler.nonceValue" />" ,
		"expires_in_seconds":"<dsp:valueof bean="BillingFormHandler.expiresInSecondsValue" />" ,
		"jwt":"<dsp:valueof bean="BillingFormHandler.jwtValue" />",
		"radialPaymentErrorKey":"<dsp:valueof bean="BillingFormHandler.radialPaymentErrorKey" />"
	}
	
</dsp:page>


