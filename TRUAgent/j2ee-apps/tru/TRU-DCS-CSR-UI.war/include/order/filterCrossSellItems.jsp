<%--
This JSP is used to filter out products that are not applicable to the current cart sharing group, contain only disabled sites and are added to the cart.
The above filtered out products should not be displayed in the cross sell items list.
 
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/filterCrossSellItems.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $
--%>

<%@ include file="/include/top.jspf"%>

<dsp:page xml="true">
 <dsp:importbean  bean="/atg/commerce/custsvc/collections/filter/droplet/CrossSellItemsFilterDroplet" />
 <dsp:importbean bean="/atg/commerce/catalog/SKULookup" />
 <dsp:importbean bean="/atg/dynamo/servlet/RequestLocale" />
 <dsp:getvalueof var="relatedProducts" param="relatedProducts" />
<%-- filter out items in the cart.  --%>
<%-- Filter out non-current cart sharing products and products with only disabled sites. --%>

  <c:set var="filteredCrossSellItems" scope="request" />
  <c:set var="filteredCrossSellItemsCount" scope="request" value="0" />
  
  <dsp:droplet name="SKULookup">
				<dsp:param bean="RequestLocale.locale" name="repositoryKey" />
				<dsp:param name="id" value="${relatedProducts}" />
				<dsp:param name="elementName" value="sku" />
				<dsp:oparam name="output">
					<dsp:getvalueof param="sku" var="filteredCollection" />
					
				<c:set var="filteredCrossSellItems" scope="request"	value="${filteredCollection}" />
			
				</dsp:oparam>
			</dsp:droplet>


</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/filterCrossSellItems.jsp#1 $$Change: 875535 $--%>