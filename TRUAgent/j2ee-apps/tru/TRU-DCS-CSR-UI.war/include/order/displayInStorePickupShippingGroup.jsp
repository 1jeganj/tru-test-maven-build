<%--
 In-Store Pickup Shipping Group
 This page displays the in-store pickup shipping group
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/displayInStorePickupShippingGroup.jsp#3 $$Change: 883241 $
 @updated $DateTime: 2014/04/15 07:49:50 $$Author: jsiddaga $
 
 Modified for Instore Pick Up for TRU CSC - by PA
--%>

<%@ include file="/include/top.jspf" %>
<style>
  	#atg_commerce_csr_neworder_ShippingAddressHome {
  		float: none;
  	}
  </style>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsHighlightedState"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShippingGroupStateDescriptions"/>
  <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/InstoreShippingDisplayListDefinition" var="instoreShippingDisplayListDefinition"/>
  <dsp:importbean var="shippingGroupFormHandler" bean="atg/commerce/custsvc/order/ShippingGroupFormHandler" />
 <dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
  <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
  <dsp:importbean bean="/com/tru/commerce/order/droplet/DisplayOrderDetailsDroplet" />
  <dsp:importbean bean="/atg/userprofiling/ActiveCustomerProfile" />
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:importbean bean="/atg/commerce/locations/StoreLookupDroplet" />
  
  <dsp:getvalueof var="shippingGroup" param="shippingGroup"/>
  <dsp:getvalueof var="propertyName" param="propertyName"/>
  <dsp:getvalueof var="displayValue" param="displayValue"/>
  <dsp:getvalueof var="displayHeading" param="displayHeading"/>
 <dsp:getvalueof var="shippingGroupIndex" param="shippingGroupIndex"/>
  <dsp:getvalueof var="displaySelectButton" param="displaySelectButton"/>
  <dsp:getvalueof var="displayAuthorizedForm" param="displayAuthorizedForm"/>
  <dsp:getvalueof var="shortDisplay" param="shortDisplay"/>
  <dsp:getvalueof var="displayAuthorizedReceiver" param="displayAuthorizedReceiver"/>
  <dsp:getvalueof var="displayStatus" param="displayStatus"/>
  
  <c:set var="order" value="${cart.current}" />
  
  <dsp:setLayeredBundle basename="atg.commerce.csr.order.WebAppResources"/>
  <dsp:droplet name="/atg/commerce/locations/RQLStoreLookupDroplet">
  
    <dsp:oparam name="output">
      <dsp:getvalueof param="items" var="stores"/>
    </dsp:oparam>
  </dsp:droplet>
  <c:forEach items="${stores}" var="store">
    <dsp:tomap value="${store}" var="store"/>
    <c:if test="${shippingGroup.locationId == store.locationId}">
      <c:set var="currentStore" value="${store}" />
    </c:if>
  </c:forEach>
  <c:set var="selectButtonOk" value="${true}" />
  <c:choose>
    <c:when test="${shortDisplay}">
    
      <fmt:message key="${instoreShippingDisplayListDefinition.intro}" />
      <c:forEach items="${instoreShippingDisplayListDefinition.items}" var="listItem" varStatus="status">
        <c:if test="${!empty currentStore[listItem]}">
          ${currentStore[listItem]}
          <c:if test="${!status.last}">
          ,
          </c:if>
        </c:if>
      </c:forEach>
    </c:when>
    <c:otherwise>
      <c:if test="${displayAuthorizedForm}">
          <dsp:droplet name="DisplayOrderDetailsDroplet">
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:param name="profile" bean="ActiveCustomerProfile" />
			<dsp:param name="page" value="store-pickup" />
			<dsp:param name="shippingDetails" value="false" />
			<dsp:param name="inStoreDetails" value="true" />
			<dsp:oparam name="output">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="shippingDestinationsVO" />
					<dsp:param name="elementName" value="shippingDestinationVO" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="shippingGroupType" param="shippingDestinationVO.shippingGroupType"/>
						<dsp:getvalueof var="shippingDestinationIndex" param="index"/>
						
						<dsp:setvalue bean="ShippingGroupFormHandler.shippingGroupsCount" paramvalue="size" />
						<dsp:input type="hidden" bean="ShippingGroupFormHandler.shippingGroupsCount" priority="1000" paramvalue="size" />
						<c:if test="${(shippingGroupType eq 'inStorePickupShippingGroup')}">
						<fieldset>
							 <legend>
			                  <fmt:message key="shipping.chooseMethod.shippingGroupNumber">
			                    <fmt:param value="${shippingDestinationIndex + 1}"/>
			                  </fmt:message>
			                </legend>
							<div>
					           <fmt:message key="inStorePickup.shippingMethod.authorizedRecipient" />
					        </div>
							<dsp:droplet name="ForEach">
								<dsp:param name="array"	param="shippingDestinationVO.shipmentVOMap" />
								<dsp:param name="elementName" value="shipmentVO" />
								<dsp:oparam name="output">
									<div>
										<dsp:include page="inStorePickUpForm.jsp">
											<dsp:param name="shipmentVO" param="shipmentVO" />
											<dsp:param name="index" value="${shippingDestinationIndex}" />
											<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
										</dsp:include>
									</div>
									<dsp:getvalueof var="locationId" param="shipmentVO.shippingGroup.locationId" />
									<fmt:message key="inStorePickup.shippingMethod.pickupLocation" /> 
									<dsp:droplet name="StoreLookupDroplet">
										<dsp:param name="id" value="${locationId}" />
										<dsp:param name="elementName" value="store" />
										<dsp:oparam name="output">
											 <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect"> 
									          <li class="atg-csc-base-bold">
									            <dsp:valueof param="store.name" />
									          </li>
									          <li>
									            <dsp:valueof param="store.address1" />
									          </li>
									          <c:if test="${!empty store.address2 }">
									            <li>
									               <dsp:valueof param="store.address2" />
									            </li>
									          </c:if>
									          <li>
									            <dsp:valueof param="store.city" /> <dsp:valueof param="store.stateAddress" />, <dsp:valueof param="store.postalCode" />
									          </li>
									          <li>
									            <dsp:valueof param="store.country" />
									          </li>
									          <li>
									            <dsp:valueof param="store.phoneNumber" />
									          </li>
									         </ul> 
										</dsp:oparam>
									</dsp:droplet>
									<div class="spacer"></div>
								</dsp:oparam>
							</dsp:droplet>
							<dsp:include src="/panels/order/shipping/includes/itemTable.jsp" otherContext="${CSRConfigurator.truContextRoot}"> 
		                 		<dsp:param name="shippingGroupIndex" value="${shippingDestinationIndex}"/>
		               		</dsp:include>
		               		</fieldset>
						</c:if>
				  
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet> 
      </c:if>
      <div class="atg_commerce_csr_addressView">
        <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
          <li class="atg-csc-base-bold">
            ${currentStore.name}
          </li>
          <li>
            ${currentStore.address1}
          </li>
          <c:if test="${!empty currentStore.address2 }">
            <li>
               ${currentStore.address2}
            </li>
          </c:if>
          <li>
            ${currentStore.city} ${currentStore.stateAddress}, ${currentStore.postalCode}
          </li>
          <li>
            ${currentStore.country}
          </li>
          <li>
            ${currentStore.phoneNumber}
          </li>
          <c:if test="${displaySelectButton != false}">
            <c:set var="canBePickupUpInStore" value="true" />
              <c:forEach items="${order.commerceItems}" var="item" varStatus="vs">
              <c:if test="${item.commerceItemClassType ne 'donationCommerceItem'}">
              <dsp:tomap var="sku" value="${item.auxiliaryData.catalogRef}" />
              <dsp:tomap var="product" value="${item.auxiliaryData.productRef}" />
              <c:set var="skuObj" value="${item.auxiliaryData.catalogRef}" />
              <c:set var="productObj" value="${item.auxiliaryData.productRef}" />
               <dsp:droplet name="TRUCSRInventoryLookupDroplet">
                <dsp:param name="skuId" value="${sku.id}"/>
                <dsp:param name="useCache" value="true" /> 
                <dsp:param name="locationId" value="${currentStore.locationId}"/>
                <dsp:param name="locationIdSupplied" value="true"/>
                <dsp:oparam name="output">
                  <dsp:getvalueof param="inventoryInfo" var="inventoryInfo"/>
                  <dsp:getvalueof param="stockLevel" var="stockLevel"/>
                  <dsp:droplet name="/atg/commerce/catalog/OnlineOnlyDroplet">
                    <dsp:param name="product" value="${productObj}"/>
                    <dsp:param name="sku" value="${skuObj}"/>
                    <dsp:oparam name="true">
                      <c:set var="canBePickupUpInStore" value="false" />
                    </dsp:oparam>
                  </dsp:droplet> 
                  <c:if test="${empty stockLevel || stockLevel <= 0}">
                    <c:set var="canBePickupUpInStore" value="false" />
                  </c:if>
                </dsp:oparam>
              </dsp:droplet> 
              </c:if>
            </c:forEach> 
            <c:if test="${canBePickupUpInStore}">  
              <li class="atg_commerce_csr_shippingControls">
                <fmt:message var="resourceSelect" key="newOrderSingleShipping.button.select"/>
                <input type="hidden" name="currentLocationId" id="currentLocationId" value="${currentStore.locationId}"/>
                <input type="button" value="${resourceSelect}" onclick="atg.commerce.csr.order.shipping.shipToAddress('${currentStore.locationId}');return false;">
              </li>
            </c:if> 
          </c:if>
        </ul>
      </div>
      <c:if test="${displayAuthorizedReceiver && (!empty shippingGroup.firstName || !empty shippingGroup.lastName)}">
        <br />
        <div class="atg_commerce_csr_addressView">
          <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
            <li style="margin-top: -27px;margin-bottom: 15px;">
              <fmt:message key="shippingSummary.inStorePickup.authorizedReceiver" />
            </li>
            <li>
               ${shippingGroup.firstName} ${shippingGroup.lastName}
            <c:if test="${not empty shippingGroup.altFirstName}"> 
              and/or ${shippingGroup.altFirstName} ${shippingGroup.altLastName} 
              </c:if>
            </li>
          </ul>
        </div>
      </c:if>
      <%-- <c:if test="${displayStatus}">
        <div class="atg_commerce_csr_addressView">
          <ul id="atg_commerce_csr_neworder_ShippingAddressHome" class="atg_svc_shipAddress addressSelect">
            <li style="margin-top: -27px;margin-bottom: 15px;">
              <fmt:message key="shippingSummary.inStorePickup.status" />
            </li>
            <li>
              <fmt:message key="shippingSummary.inStorePickup.goodsAreReady" />
            </li>
          </ul>
        </div>
      </c:if> --%>
    </c:otherwise>
  </c:choose>
  
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/displayInStorePickupShippingGroup.jsp#3 $$Change: 883241 $--%>