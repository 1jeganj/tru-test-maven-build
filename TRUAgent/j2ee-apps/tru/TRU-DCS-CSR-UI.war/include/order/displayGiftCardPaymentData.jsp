<%--
Display the appropriate details for the Gift card.

This page is shared across the entire CSC application. This page is used wherever a payment
group information is getting displayed.

This page is more specifically used in billing, returns, order review,
order view and email pages.

Expected params
paymentGroup : The payment group from which the information is going to be retrieved.

If you want to display any of the values below, pass in a parameters below.
propertyName : required -- This parameter is used to display a particular information about a property
displayValue : optional -- The parameter is used to display the value of the <code>propertyName</code>
displayHeading : optional -- The parameter is used to display the heading of the <code>propertyName</code>

@version $Id:
@updated $DateTime: 2014/03/14 15:50:19 $$Author: PA $
--%>
<%@  include file="/include/top.jspf"%>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/order/IsHighlightedState"/>
  <dsp:importbean
    bean="/atg/commerce/custsvc/order/PaymentGroupStateDescriptions"/>
 <dsp:getvalueof var="userOrder" bean="/atg/commerce/custsvc/order/ShoppingCart.current"/>
  <dsp:getvalueof var="paymentGroup" param="paymentGroup"/>
  <dsp:getvalueof var="propertyName" param="propertyName"/>
  <dsp:getvalueof var="displayValue" param="displayValue"/>
  <dsp:getvalueof var="displayHeading" param="displayHeading"/>

  <dsp:setLayeredBundle basename="atg.commerce.csr.order.WebAppResources"/>
  <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />

  <c:if test="${paymentGroup['class'].name == 'com.tru.commerce.order.TRUGiftCard'}">
    <c:if test="${propertyName == 'value1'}">
      <c:if test="${displayHeading == true}">
        <fmt:message key='billingSummary.commerceItem.header.type'/>
      </c:if>
      <c:if test="${displayValue == true}">
        <csr:displayGiftCardType giftCard="${paymentGroup}"/>
      </c:if>
    </c:if>

    <c:if test="${propertyName == 'value2'}">
      <c:if test="${displayHeading == true}">
        	<fmt:message key="billing.giftcard.amount" bundle="${TRUCustomResources}"/>
      </c:if>
      <c:if test="${displayValue == true}">
	        $<dsp:valueof param="paymentGroup.amount" number="#.00"/>
     </c:if>
     </c:if>

    <c:if test="${propertyName == 'status'}">
       <c:if test="${displayHeading == true}">
        <fmt:message key='billingSummary.commerceItem.header.state'/>
      </c:if>
      <c:if test="${displayValue == true}">
        <dsp:droplet name="PaymentGroupStateDescriptions">
          <dsp:param name="state" value="${paymentGroup.stateAsString}"/>
          <dsp:param name="elementName" value="stateDescription"/>
          <dsp:oparam name="output">
            <dsp:droplet name="IsHighlightedState">
              <dsp:param name="obj" value="${paymentGroup}"/>
              <dsp:oparam name="true">
		          <span class="atg_commerce_csr_dataHighlight"> <dsp:valueof
		            param="stateDescription"></dsp:valueof></span>
              </dsp:oparam>
              <dsp:oparam name="false">
                <dsp:valueof param="stateDescription"></dsp:valueof>
              </dsp:oparam>
            </dsp:droplet>
          </dsp:oparam>
        </dsp:droplet>
      </c:if>
    </c:if>

	<c:if test="${propertyName == 'value3'}">
		<c:if test="${displayHeading == true}">
       	Billing Address
     	</c:if>
		<c:if test="${displayValue == true}">
			${userOrder.billingAddress.firstName}
			${userOrder.billingAddress.lastName}
			<li>${userOrder.billingAddress.address1}</li>
			<li>${userOrder.billingAddress.address2}</li>
			<c:if test="${not empty userOrder.billingAddress.city}">
				${userOrder.billingAddress.city},
			</c:if>
			<c:if test="${not empty userOrder.billingAddress.state}">
				${userOrder.billingAddress.state},
			</c:if>
			${userOrder.billingAddress.postalCode}
			<li>${userOrder.billingAddress.country}</li>
			<li>${userOrder.billingAddress.phoneNumber}</li>
		</c:if>
	</c:if>
	</c:if>
</dsp:page>
