<%@ include file="/include/top.jspf" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean var="shippingGroupFormHandler" bean="atg/commerce/custsvc/order/ShippingGroupFormHandler" />

	<dsp:getvalueof var="inStorePickUpShippingGroup" param="shipmentVO.shippingGroup"/>
	<dsp:getvalueof var="storePickUpInfo" param="shipmentVO.storePickUpInfo"/>
	<dsp:getvalueof var="shippingGroupType" param="shippingGroupType"/>
	
	<c:if test="${shippingGroupType eq 'inStorePickupShippingGroup'}">
	<div class="row">
	<div class="col-xs-5">
	</div>
	</div>
		<div class="pickupPersonDetails">
			<dsp:include page="inStorePickUpPrimaryContact.jsp" >
				<dsp:param name="storePickUpInfo" value="${storePickUpInfo}"/>
				<dsp:param name="index" param="index" />
			</dsp:include>
			<!-- End .primary-pickup-person -->
			<dsp:include page="inStorePickUpAlternateContact.jsp" >
				<dsp:param name="storePickUpInfo" value="${storePickUpInfo}"/>
				<dsp:param name="index" param="index" />
			</dsp:include>
		</div>
	</c:if>
	<script>
		
			$(document).on('click', '.alternateInfoCheck', function(){
				if($(this).is(':checked')){
					$(this).parent().find('#alternateInfoCheckBean').val('true');
					$(this).closest('.pickupPersonDetails').find('.alternate-pickup-person-container').show();	
				}else{
					$(this).parent().find('#alternateInfoCheckBean').val('false');
					$(this).closest('.pickupPersonDetails').find('.alternate-pickup-person-container').hide();
					$(this).closest('.pickupPersonDetails').find('.alternate-pickup-person-container input').val('');
				}
								
			});
		
	</script>
</dsp:page>