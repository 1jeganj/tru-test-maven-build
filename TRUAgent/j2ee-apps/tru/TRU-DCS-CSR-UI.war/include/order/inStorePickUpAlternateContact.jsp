<%@ include file="/include/top.jspf" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<style>
		.alternate-pickup-person-container {
			display: none;
		}
	</style>
	<%-- <dsp:importbean bean="/com/tru/commerce/order/purchase/InStorePickUpFormHandler"/> --%>
	<dsp:importbean var="shippingGroupFormHandler" bean="atg/commerce/custsvc/order/ShippingGroupFormHandler" />
	
	<dsp:getvalueof var="storePickUpInfo" param="storePickUpInfo" />
	<dsp:getvalueof var="index" param="index" />
	<dsp:getvalueof var="alternateInfoRequired" value="${storePickUpInfo.alternateInfoRequired}"/>
	
	<div class="alternate-pickup-person-container" id="alternate-pickup-person ${storePickUpInfo.shippingGroupId}-altChk" <c:if test="${alternateInfoRequired eq true}">style="display:block;"</c:if>>
		<div>
			<span class="required-asterisk">*</span>
			<fmt:message key="checkout.pickup.firstName" />
			<dsp:input type="text" iclass="alternatefirstname" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateFirstName" value="${storePickUpInfo.alternateFirstName}"></dsp:input>
		</div>
		
		<div>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.lastName" />
				<dsp:input type="text" iclass="alternatelastname" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateLastName" value="${storePickUpInfo.alternateLastName}"></dsp:input>
		</div>
		
		<div>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.telephone" />
				<dsp:input type="text" iclass="alternatephonenumber splCharCheck" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternatePhoneNumber"
						value="${storePickUpInfo.alternatePhoneNumber}" onkeypress="return isPhoneNumberFormat(event)" maxlength="30">
				</dsp:input>
		</div>
		<div>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.email" />
				<dsp:input type="text" iclass="alternateemail" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateEmail" value="${storePickUpInfo.alternateEmail}">
				</dsp:input>
		</div>
		<br>
	</div>
</dsp:page>