<%--
 This page defines the credit card form using the following input parameters:

 formId - the DOM ID of the form node to submit to add or update credit cards
 creditCardBean - the object that has credit card type, number and expiration date
 creditCardAddressBean - the object that allows the option of adding a new address to the credit card
 creditCardFormHandler - the full path to the form handler to add or update the credit card
 submitButtonId - the DOM ID of the submit button so that the button can be enabled or
   disabled based on the validity of the form inputs
 isMaskCardNumber - edit credit card hides the number with a mask exposing only the last
   four digits
 isUseExistingAddress - Some of the form handlers have the opposite logic from each other:
   some use an isUseExistingAddress property whose state must be false to create new addresses,
   others use the opposite isCreateNewAddress property whose state must be true to create new addresses.
   Note: this is not a way to turn off the address or new address area of the credit card form.

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/creditCardForm.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>

<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean var="addressForm" bean="/atg/svc/agent/ui/fragments/AddressForm"/>
    <dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet"/>
    <dsp:getvalueof var="formId" param="formId"/>
    <dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
    <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
    <dsp:getvalueof var="creditCardBean" param="creditCardBean"/>
    <dsp:getvalueof var="creditCardAddressBean" param="creditCardAddressBean"/>
    <dsp:getvalueof var="creditCardFormHandler" param="creditCardFormHandler"/>
    <dsp:getvalueof var="submitButtonId" param="submitButtonId"/>
    <dsp:getvalueof var="isMaskCardNumber" param="isMaskCardNumber"/>
    <dsp:getvalueof var="isUseExistingAddress" param="isUseExistingAddress"/>
    <dsp:getvalueof var="disableCreditCardType" param="disableCreditCardType"/>
    <dsp:getvalueof var="disableCreditCardNumber" param="disableCreditCardNumber"/>
    <dsp:getvalueof var="PLCCExpMonth" bean="TRUConfiguration.expMonthAdjustmentForPLCC"/>
    <dsp:getvalueof var="PLCCExpYear" bean="TRUConfiguration.expYearAdjustmentForPLCC"/>
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />
      <fmt:message var="emptyAddressSelection" key="emptyAddressSelection" />
      <fmt:message var="invalidCreditCardType" key="invalidCreditCardType" />
      <fmt:message var="invalidCreditCardNumber" key="invalidCreditCardNumber" />
      <fmt:message var="creditCardNumberMissing" key="creditCardNumberMissing" />
      <fmt:message var="invalidCreditCardDate" key="invalidCreditCardDate" />
      <fmt:message var="invalidExistingAddress" key="invalidExistingAddress" />
      <fmt:message var="invalidCardName" key="invalidCardName" bundle="${TRUCustomResources}"/>
      <input type="hidden" id="PLCCExpMonth" value="${PLCCExpMonth}"/>
      <input type="hidden" id="PLCCExpYear" value="${PLCCExpYear}"/>
      <c:if test="${disableCreditCardType}">
        <script type="text/javascript">
          _container_.onLoadDeferred.addCallback(function(){
               var ccType = dijit.byId('${formId}_creditCardType');
               if (ccType) ccType.setDisabled(true);
          });
        </script>
      </c:if>

      <c:if test="${disableCreditCardNumber}">
        <script type="text/javascript">
          _container_.onLoadDeferred.addCallback(function(){
            atg.commerce.csr.common.disableTextboxWidget('${formId}_maskedCreditCardNumber');
            atg.commerce.csr.common.disableTextboxWidget('${formId}_creditCardNumber');
          });
        </script>
      </c:if>
      
      <script>

/* This function is used to validate credit card number based on mod 10 and Luhn algorithm. */
function validateCreditCardNumber(value) {
	// accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value))
		return false;
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n), nDigit = parseInt(cDigit, 10);
		if (bEven) {
			if ((nDigit *= 2) > 9)
				nDigit -= 9;
		}
		nCheck += nDigit;
		bEven = !bEven;
	}
	return (nCheck % 10) == 0;
}


function startTokenization(suggestedAddressFromAJAX) {
	var enableVerboseDebug=true;
	var accountNumberbefore = $('#${formId}_creditCardNumber').val();
	 var isValidCard = validateCreditCardNumber(accountNumberbefore);
	 var orderId = $('#${formId}_orderId').val();
	 var accountNumber = $('#${formId}_creditCardNumber').val();
	 var nameOnCard = $('#${formId}_nameOnCreditCard').val();
	 var cardType = getCreditCardType();
	 var tmpVal = $('#${formId}_expirationMonth').val();
	 /* var expirationMonth = (tmpVal.length <= 1) ? '0'+ tmpVal : tmpVal;
	 var expirationYear= $('#${formId}_expirationYear').val(); */
	 var expirationMonth,expirationYear;
	 if(cardType == 'RUSPrivateLabelCard')
		 {
			expirationMonth = $("#PLCCExpMonth").val();
		 	expirationYear= $("#PLCCExpYear").val();
		 }
	 else
		 {
			 expirationMonth = (tmpVal.length <= 1) ? '0'+ tmpVal : tmpVal;
			 expirationYear= $('#${formId}_expirationYear').val();
		 }
	 var truCurrencyCode = $('#${formId}_truCurrencyCode').val();
	 var cardCode = $('#${formId}_cvv').val();
	 var defaultCreditCard = $("#makeDefaultCreditCard").val();
	  
	  var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName,nickName;
	 var existingAddress = $('#editCreditCardForm').find("input[type='radio']:checked").val();
	
	 if(typeof(existingAddress) == 'undefined')
		 {
		 	existingAddress = 'true';
		 }
		  var nickName = $('#${formId}_nickName').val();
		 	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
     	if(suggestedAddressFromAJAX && ($('#${formId}_country').val() == 'US'||$('#${formId}_country').val().toLowerCase() == 'united states')){
     		
     		/*  country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
   			 city = $('#suggestedCity-'+addressFlag).val();
   			 state = $('#suggestedState-'+addressFlag).val();
   			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
   			 address2 = $('#suggestedAddress2-'+addressFlag).val(); */
   			 country= suggestedAddressFromAJAX.country;
			 address1 = suggestedAddressFromAJAX.address1;
  			 city = suggestedAddressFromAJAX.city;
  			 state = suggestedAddressFromAJAX.state;
  			 postalCode = suggestedAddressFromAJAX.postalCode;
  			 address2 = suggestedAddressFromAJAX.address2;
   			 phoneNumber = $('#${formId}_phoneNumber').val();
   			 firstName = $('#${formId}_firstName').val();
   			 middleName = $('#${formId}_middleName').val();
   			 lastName = $('#${formId}_lastName').val();
   			state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
	     	
     	}
     	else if(existingAddress != 'false' && typeof(addressFlag) !='undefined' && addressFlag != "0" && !suggestedAddressFromAJAX && ($('#${formId}_country').val() == 'US'||$('#${formId}_country').val().toLowerCase() == 'united states')){
     		
     		 country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
   			 city = $('#suggestedCity-'+addressFlag).val();
   			 state = $('#suggestedState-'+addressFlag).val();
   			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
   			 address2 = $('#suggestedAddress2-'+addressFlag).val(); 
   			 phoneNumber = $('#${formId}_phoneNumber').val();
   			 firstName = $('#${formId}_firstName').val();
   			 middleName = $('#${formId}_middleName').val();
   			 lastName = $('#${formId}_lastName').val();
   			state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
	     	
     	}
     	else if(existingAddress == 'true'){
		var nickName = $('#${formId}_nickName').val();

		 var firstName = $('#${formId}_firstName').val();

		 var middleName = $('#${formId}_middleName').val();
	
		 var lastName = $('#${formId}_lastName').val();

		 var country=dijit.byId("${formId}_country");

		 var address1 = $('#${formId}_address1').val();
		 var city = $('#${formId}_city').val();
		 var state1 = $('#${formId}_state').val();
		 var postalCode = $('#${formId}_postalCode').val();
		 var phoneNumber = $('#${formId}_phoneNumber').val();
		 var address2 = $('#${formId}_address2').val();
		if(country != 'US')
		{
			if(state1 == 'NA')
				{
					var state = 'NA'
				}
		}
		else{
			state1 = state1.split("-")[0];
			var state = $.trim(state1);
		}
		
     	}
     	else if(existingAddress == 'false')
		 {
		 
		 	var selectedAddress = $("#editCreditCardForm_existingAddressList option:selected").text();
		 	
		 	var addressDetails = selectedAddress.split(",");
		 	country= $.trim(addressDetails[3]);
			address1 = $.trim(addressDetails[7]);
 			city = $.trim(addressDetails[2]);
 			state = $.trim(addressDetails[4]);
 			postalCode = $.trim(addressDetails[5]);
 			address2 = $.trim(addressDetails[8]);
 			phoneNumber = $.trim(addressDetails[6]);
 			firstName = $.trim(addressDetails[0]);
 			middleName = "";
 			lastName = $.trim(addressDetails[1]);
 			var selectedCardBillingAddress = $("#editCreditCardForm_existingAddressList option:selected").attr('class');
 			nickName = $("#editCreditCardForm_existingAddressList option:selected").attr('class');
 			state1 = state.split("-")[0];
	     	var state = $.trim(state1);
	     
		 }
	
	  

   var d = new Date();
   var timeStamp = d.getTime(); 
    var currentMonth = d.getMonth() + 1;
   var currentYear = d.getFullYear();
	$("#nameOnCard").val(nameOnCard);
	$("#expirationMonth").val(expirationMonth);
	$("#expirationYear").val(expirationYear);
	$("#cardCode").val(cardCode);
	$('#accountNumber').val(accountNumber);
	$("#cardType").val(cardType);
	$('#cardLength').val(accountNumber.length);
	$("#nickName").val(nickName);
	$("#nickName1").val(selectedCardBillingAddress);
	$("#selectedCardBillingAddress").val(selectedCardBillingAddress);
	$("#accountBillingForm").find("#firstName").val(firstName);
	$("#middleName").val(middleName);
	$("#accountBillingForm").find("#lastName").val(lastName);
	$("#country").val(country);
	$("#address1").val(address1);
	$("#address2").val(address2);
	$("#city").val(city);
	$("#state").val(state);
	$("#postalCode").val(postalCode);
	$("#phoneNumber").val(phoneNumber);
	$("#defaultCreditCard").val(defaultCreditCard);
   if( parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear ){
		return false;
   }
	var isEditSavedCard = $('#isEditSavedCard').val();
	var jsonObject = 
    {

    		"cmpiRequestObject" : {
    			"OrderNumber" : orderId,
    			"CurrencyCode": truCurrencyCode,
    			"Amount": "",
    			"CardExpMonth": expirationMonth,
    			"CardExpYear" : expirationYear,
    			"CardNumber": accountNumber,
    			"EMail": "",
    			"BillingFirstName": firstName,
    			"BillingLastName":  lastName,
    			"BillingAddress1": address1,
    			"BillingAddress2": address2,
    			"BillingCity": city,
    			"BillingState": state,
    			"BillingCountryCode": country,
    			"BillingPostalCode": postalCode,
    			"BillingPhone": phoneNumber,
    			"IPAddress": "0:0:0:0:0:0:0:1",  // Clientâ€™s IP
    			"UserAgent": navigator.userAgent
    		}

    }
		 if(enableVerboseDebug) {
			 console.log("calling getNONCEResponce() for accountNumber:"+accountNumber);
		} 
		getNONCEResponce();
			//Radial.tokenize()
		if(enableVerboseDebug) {
			 console.log("calling tokenizeAndAuthorize() for accountNumber:"+accountNumber);
		}
		Radial.tokenize(accountNumber,tokenizeCallBackHandler);
		cardinalApi = "Tokenization_checkout";
		return false;
}

function getNONCEResponce(){
	var nonceRetryAttempts = 0;
	var maxNonceRetryAttempts = 0;
	var errorFlag = false;
	
	var enableVerboseDebug = false;
	if(enableVerboseDebug) {
		 console.log("Inside getNONCEResponce()");
	}
	$.ajax({
			url:  "/TRU-DCS-CSR/include/checkout/common/nonceAjax.jsp",
			type: "POST",
			async:false,
			dataType:"json",
			cache:false,
			success: function(nonceData){
     				if(typeof nonceData == 'string'){
     					nonceData = JSON.parse(nonceData);
     				}
				if(typeof nonceData == 'object' && typeof nonceData.nonce != 'undefined' && typeof nonceData.jwt != 'undefined' && nonceData.nonce != '' && nonceData.radialPaymentErrorKey != '50002'){
					 if(enableVerboseDebug) {
						 console.log("getNONCEResponce() Radial Nonce:"+nonceData.nonce);
						 console.log("getNONCEResponce() Radial jwt:"+nonceData.jwt);
						 console.log("getNONCEResponce() START Radial setup()");
					 }
					 Radial.setup(nonceData.nonce, nonceData.jwt);
					 if(enableVerboseDebug) {
						 console.log("getNONCEResponce() END calling Radial setup()");
					 }
				 	jwt = nonceData.jwt;
					nonceSetup = true;
					nonceRetryAttempts = 0;
					return false;
				 }
				nonceRetryAttempts++;
				if(enableVerboseDebug) {
					console.log("Error in getNONCEResponce() and showing the Error radialPaymentErrorKey:" + nonceData.radialPaymentErrorKey);
				}
				if(nonceRetryAttempts == maxNonceRetryAttempts) {
					if(enableVerboseDebug) {
						console.log("Error in getNONCEResponce() and showing the Error msg");
					}
					nonceRetryAttempts = 0;
					nonceSetup = false;
					var radailPaymentErrorCode = nonceData.radialPaymentErrorKey;
					var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
					var radailPaymentMsg= errorKeyJson[key];
					$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">'+radailPaymentMsg+'</span>');
					return false;
				}
				errorFlag = true;
     		
			},
			error:function(e){
	            console.log(e);
			}
	
}); 
		if(errorFlag) {
			getNONCEResponce();
		}
}

function tokenizeCallBackHandler(data) {
	
	var getTokenRetryAttempts = 0;
	var maxGetTokenRetryAttempts = 0
	var enableVerboseDebug = true;
	if(enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() response data token:"+data.account_token);
	}
	var radailPaymentErrorCode = data.ErrorNumber;
	if(enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() in Error Number:"+radailPaymentErrorCode);
	}
	//getting nonce again and re_try token
	if(radailPaymentErrorCode !=undefined && radailPaymentradailPaymentErrorCode=='50002' && getTokenRetryAttempts < maxGetTokenRetryAttempts){
		getTokenRetryAttempts++;
		startTokenization();
		return false;
	}else if(radailPaymentErrorCode !=undefined && radailPaymentErrorCode=='50001'|| radailPaymentErrorCode=='50003' && getTokenRetryAttempts < maxGetTokenRetryAttempts){
		getTokenRetryAttempts++;
		startTokenization();
		return false;
	}else if(radailPaymentErrorCode !=undefined && radailPaymentErrorCode=='40001'|| radailPaymentErrorCode=='40002'||radailPaymentErrorCode=='40003' ||radailPaymentErrorCode=='40004' ||radailPaymentErrorCode=='50001' ||radailPaymentErrorCode=='50002'||radailPaymentErrorCode=='50003'){
		getTokenRetryAttempts = 0;
		alert("Error occured while processing radial services with code "+radailPaymentErrorCode);
		return false;
	}else{
	var form = document.getElementById('accountBillingForm');
	var ccToken=data.account_token;
	$("#CardinalToken").val(ccToken);
	atgSubmitAction({panelStack : 'customerPanels',form : document.getElementById('accountBillingForm')});

	}
}

function getCreditCardType(currentEvent){
		/* if($("#editCreditCardForm_maskedCreditCardNumber").length)
			{
				var value = $("#editCreditCardForm_maskedCreditCardNumber").val();
				 var value = $("#editCreditCardForm_creditCardNumber").val();
				var value= "4005550000000019" 
			}
		else
			{
				var value = $("#editCreditCardForm_creditCardNumber").val();
			} */
				var value = $("#editCreditCardForm_creditCardNumber").val();
       var cardType = '';
       if (value.length > 5) {
           var v = parseInt(value.substr(0, 6));

           if ((v >= 400000 && v <= 499999) && (value.length > 5 || value.length == 13 || value.length == 16)) { /*visa card*/
               if ((v == 425329 || v == 425332 || v == 425333 || v == 425334) && (value.length > 5 || value.length == 13 || value.length == 16)) {
                   cardType = 'invalid';
               } else {
                   $(this).attr('maxlength', '16');
                   cardType = 'visa';
               }
           } else if ((((v >= 510000 && v <= 523769) || (v >= 523771 && v <= 524362) || (v >= 524364 && v <= 559999)) && (value.length > 5 || value.length == 16))) {
               $(this).attr('maxlength', '16');
               cardType = 'masterCard';
           } else if ((v >= 360000 && v <= 369999 && (value.length == 14 || value.length == 16))) {
               if (value.length == 14) {
                   cardType = 'masterCard';
               } else if (value.length == 16) {
                   $(this).attr('maxlength', '16');
                   cardType = 'discover';
               } else {
                   cardType = 'invalid';
               }
           } else if (((v >= 370000 && v <= 379999) || (v >= 340000 && v <= 349999)) && (value.length > 5 || value.length == 15)) { /*Amex card*/
               $(this).attr('maxlength', '15');
               cardType = 'americanExpress';
           } else if ((v >= 601100 && v <= 601199 || v >= 622126 && v <= 622925 || v >= 628200 && v <= 628899 || v >= 644000 && v <= 659999 || v >= 300000 && v <= 305999 || v >= 309500 && v <= 309599 || v >= 352800 && v <= 358999 || v >= 380000 && v <= 399999 || v >= 624000 && v <= 625999) && (value.length > 5 || value.length == 16)) { /*discover card*/
               if ((v == 601104 || (v >= 601110 && v < 601120) || (v >= 601150 && v < 601174) || (v >= 601175 && v < 601177) || (v >= 601180 && v < 601186)) && (value.length > 5 || value.length == 16)) {
                   cardType = 'invalid';
               } else {
                   $(this).attr('maxlength', '16');
                   cardType = 'discover';
               }
           } else if (v == 604586 && (value.length > 5 || value.length == 16)) { /*PLCC card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSPrivateLabelCard';
           } else if (v == 524363 && (value.length > 5 || value.length == 16)) { /*USCOBMasterCard card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSCoBrandedMasterCard';
           } else if (v == 523770 && (value.length > 5 || value.length == 16)) { /*PRCOBMasterCard card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSCoBrandedMasterCard';
           } else {
               $(this).removeAttr('maxlength');
               cardType = 'invalid';
           }
       } else {
           $(this).removeAttr('maxlength');
          cardType = 'invalid';
       }
       return cardType;
}

</script>

      <div dojoType="dojo.data.ItemFileReadStore" jsId="cardStore" url="${CSRConfigurator.contextRoot}/include/cardData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>
      <div dojoType="dojo.data.ItemFileReadStore" jsId="monthStore" url="${CSRConfigurator.truContextRoot}/include/monthData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>
      <div dojoType="dojo.data.ItemFileReadStore" jsId="yearStore" url="${CSRConfigurator.contextRoot}/include/yearData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>

      <div class="atg-csc-base-table">
      
      			<div class="atg_commerce_csr_cardType atg-csc-base-table-row">
					<span
						class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
						<label class="atg_messaging_requiredIndicator ">name on card
					</label> <span class="requiredStar">*</span>
					</span>

					<div class="atg-csc-base-table-cell">
							  <dsp:input id="${formId}_orderId" type="hidden" bean="ShoppingCart.current.id"/>
							   <dsp:input id="${formId}_truCurrencyCode" type="hidden" bean="TRUConfiguration.currencyCode"/> 
		         <dsp:input type="text" id="${formId}_nameOnCreditCard" iclass="atg-base-table-customer-credit-card-add-number" bean="${creditCardBean}.nameOnCard"
                  required="<%=true%>" size="25" maxlength="25" style="width:175px !important">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardNumber }"/>
                  <dsp:tagAttribute name="autocomplete" value="off" />
                </dsp:input>
					</div>
				</div>
      
      
        <div class="atg_commerce_csr_cardType atg-csc-base-table-row">
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label class="atg_messaging_requiredIndicator ">
              <fmt:message key="newOrderBilling.addEditCreditCard.header.card.title"/>
            </label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <dsp:input id="${formId}_creditCardType" iclass="" bean="${creditCardBean}.creditCardType"
              required="<%=true%>" style="width:175px !important" onchange="setCvvLength();">
              <dsp:tagAttribute name="dojoType" value="atg.widget.form.FilteringSelect" />
              <dsp:tagAttribute name="autoComplete" value="true" />
              <dsp:tagAttribute name="searchAttr" value="name" />
              <dsp:tagAttribute name="store" value="cardStore" />
              <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardType }"/>
            </dsp:input>
          </div>
        </div>

        <div class="atg_commerce_csr_cardNumber atg-csc-base-table-row" >
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label class="atg_messaging_requiredIndicator" >
              <fmt:message key="newOrderBilling.addEditCreditCard.field.cardNumber"/>
            </label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <c:choose>
              <c:when test="${isMaskCardNumber}">

                <%-- Bugs-fixed: 144174-1 The following snippet creates a parameter and the parameter
                     has the masked credit card number and the masked credit card string is passed to the
                     credit card validity routine. --%>


                <dsp:param name="maskedCreditCardNumber" bean="${creditCardBean}.creditCardNumber"
                  converter="creditCard" maskcharacter="*" numcharsunmasked="4"/>
                <dsp:getvalueof var="maskedCreditCardNumberVar" param="maskedCreditCardNumber"/>

                <dsp:input type="text" id="${formId}_maskedCreditCardNumber" iclass="atg-base-table-customer-credit-card-add-number"
                  bean="${creditCardBean}.creditCardNumber"
                  required="<%=true%>" size="25" maxlength="16"
                  converter="creditCard" maskcharacter="*" numcharsunmasked="4">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardNumber }"/>
                  <dsp:tagAttribute name="autocomplete" value="off" />
                </dsp:input>
                <dsp:droplet  name="/atg/commerce/custsvc/events/ViewCreditCardEventDroplet">
                  <dsp:param name="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
                  <dsp:param name="creditCardNumber" bean="${creditCardBean}.creditCardNumber" />
                </dsp:droplet>

              </c:when>
              <c:otherwise>
                <dsp:input type="text" id="${formId}_creditCardNumber" iclass="atg-base-table-customer-credit-card-add-number" bean="${creditCardBean}.creditCardNumber"
                  required="<%=true%>" size="25" maxlength="16">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardNumber }"/>
                  <dsp:tagAttribute name="autocomplete" value="off" />
                </dsp:input>
              </c:otherwise>
            </c:choose>
          </div>
        </div>
        
      <div class="atg_commerce_csr_expirationDate atg-csc-base-table-row">
        <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
          <label class="atg_messaging_requiredIndicator">
            <fmt:message key="newOrderBilling.addEditCreditCard.field.expDate"/>
          </label>
          <span class="requiredStar">*</span>
        </span>
        <dsp:getvalueof var="isEditCreditCard" param="isEditCreditCard"></dsp:getvalueof>
        <div class="atg-csc-base-table-cell">
                    <dsp:select id="${formId}_expirationMonth" iclass="" bean="${creditCardBean}.expirationMonth"
            required="<%=true%>" style="width:120px;">
             
        		

					<c:choose>
						<c:when test="${isEditCreditCard}">
						<dsp:getvalueof var="expirationMonth"  bean="${creditCardBean}.expirationMonth"  />
						<option value="${expirationMonth}">
							<dsp:valueof bean="${creditCardBean}.expirationMonth" />
						</option>
						</c:when>
						<c:otherwise>
							<option value="">
           						Month
           					</option>
           						</c:otherwise>
					</c:choose>
        		
					<dsp:droplet name="DisplayMonthAndYearDroplet">
													<dsp:param name="monthList" value="monthList"/>
													<dsp:oparam name="output">
														<dsp:droplet name="ForEach">
															<dsp:param name="array" param="monthList"/>
															<dsp:param name="elementName" value="month"/>
															<dsp:oparam name="output">
																<dsp:getvalueof var="month" param="month" />
																<option value='<dsp:valueof param="month" />'
						                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
						                                                      <dsp:param name="obj1" value="${month}"/>
						                                                      <dsp:param name="obj2" value="${expirationMonth}"/>
						                                                      <dsp:oparam name="equal">
						                                                          selected="selected"
						                                                      </dsp:oparam>
						                                              </dsp:droplet> >
						                                              <dsp:valueof param="month"/>
						                                         </option>
						                                         </dsp:oparam>
															</dsp:droplet>
														</dsp:oparam>
													</dsp:droplet>
          </dsp:select>
        </div>
        <div class="atg-csc-base-table-cell">
          <dsp:input id="${formId}_expirationYear" iclass="" bean="${creditCardBean}.expirationYear"
            required="<%=true%>" style="width:85px;">
            <dsp:tagAttribute name="dojoType" value="atg.widget.form.FilteringSelect" />
            <dsp:tagAttribute name="autoComplete" value="true" />
            <dsp:tagAttribute name="searchAttr" value="name" />
            <dsp:tagAttribute name="store" value="yearStore" />
            <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardDate }" />
          </dsp:input>
        </div>
      </div>

		    <div class="atg_commerce_csr_cardNumber atg-csc-base-table-row atg-base-table-customer-credit-card-line" >
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label" style="height:0px">
          <label class="atg_messaging_requiredIndicator">
            cvv
          </label>
          <span class="requiredStar">*</span>
        </span>
    
        <div class="atg-csc-base-table-cell">
         <%--  <dsp:input type="text" id="${formId}_cvv" bean="${creditCardBean}.cavv" required="<%=true%>" size="4" maxlength="4">
             <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
             <dsp:tagAttribute name="required" value="true" />
             <dsp:tagAttribute name="trim" value="true" />
             <dsp:tagAttribute name="promptMessage" value="${cvvNumberMissing}" />
             <dsp:tagAttribute name="invalidMessage" value="invalid cvv"/>
             <dsp:tagAttribute name="autocomplete" value="off" />
           </dsp:input> --%>
           <input type="text" id="${formId}_cvv" required="<%=true%>" size="4" maxlength="4"/>
           
        </div>
      </div>
	
      <div class="atg_commerce_csr_existingBilling atg-csc-base-table-row">
          <c:choose>
            <c:when test="${isUseExistingAddress}">
              <dsp:getvalueof var="existingAddressList" bean="${creditCardFormHandler}.existingAddresses" />
              <dsp:getvalueof var="useExistingAddress" bean="${creditCardFormHandler}.useExistingAddress" />
              <%-- When there is no existing addresses, do not display the radio buttons. Instead just
              display the address form. --%>

             <c:choose>
                <c:when test="${fn:length(existingAddressList) == 0}"> 
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label>
                  </span>
                  
                   <script type="text/javascript" charset="utf-8">
                    _container_.onLoadDeferred.addCallback(function () {
                    atg.commerce.csr.${formId}existingBillingAddressPicker();
                  });
                  </script>
                </c:when>
                <c:otherwise>
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label><span class="requiredStar">*</span>
                  </span>
					<dsp:setvalue param="existAddress" beanvalue="${creditCardFormHandler}.existingAddresses"/>existAddress::::${existAddress}
                 	<dsp:setvalue param="existFirstAddress" paramvalue="existAddress[0]"/>${existFirstAddress}
                 	<dsp:getvalueof var="firstAddress" param="existFirstAddress" />                  
                  <div class="atg-csc-base-table-cell">
                    
                        <dsp:input id="${formId}_existingAddress" type="radio" checked="${useExistingAddress ? true : false}"
                          bean="${creditCardFormHandler}.useExistingAddress"
                          value="true" onclick="${formId}HideAddressArea();"/>
                        <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.useExistingAddress"/></span>
                        </div>
                        <div class="atg-csc-base-table-cell">
                          <dsp:select id="${formId}_existingAddressList" iclass="" bean="${creditCardFormHandler}.addressIndex">
                            <dsp:droplet name="ForEach">
                              <dsp:param name="array" bean="${creditCardFormHandler}.existingAddresses" />
                              <dsp:oparam name="output">
                                <dsp:getvalueof var="address" param="element"/>
                                <dsp:getvalueof var="addressIndex" param="index"/>
                                <dsp:option value="${addressIndex}">
                                  ${fn:escapeXml(address.address1)}${!empty address.address2 ? ' ' : '' }${!empty address.address2 ? fn:escapeXml(address.address2) : '' }
                                </dsp:option>
                              </dsp:oparam>
                            </dsp:droplet>
                          </dsp:select>
                        </div>
                      
                  
                </c:otherwise>
              </c:choose>
              
              </div>
              
            
              <div class="atg-csc-base-table-row">
                <div class="atg-csc-base-table-cell"></div>
                <div class="atg_commerce_csr_createNewAddress atg-csc-base-table-cell">
                  <script type="text/javascript" charset="utf-8">
                      atg.commerce.csr.${formId}existingBillingAddressPicker = function(){
                        var existingAddressList = dojo.byId('${formId}_existingAddressList');
                        if(existingAddressList){
                          existingAddressList.disabled = true;
                        }
                        dojo.style(dojo.byId('${formId}_addressArea'),'display','block');
                        if(dojo.byId('editPaymentOptionFloatingPane').style.display == 'block'){
                          dijit.byId('editPaymentOptionFloatingPane').layout();
                        }
                      };
                  </script>
                  <div>
                    <c:choose>
                      <c:when test="${fn:length(existingAddressList) >0}">
                        <dsp:input type="radio" checked="${!useExistingAddress ? true : false}"
                          bean="${creditCardFormHandler}.useExistingAddress"
                          value="false" id="${formId}_existingAddress"
                          onclick="atg.commerce.csr.${formId}existingBillingAddressPicker();"/>
                        <fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.createNewAddress"/>
                      </c:when>
                      <c:otherwise>
                        <dsp:input type="hidden" bean="${creditCardFormHandler}.useExistingAddress"
                          value="false" id="${formId}_existingAddress"/>
                      </c:otherwise>
                    </c:choose>
                  </div>

                </div>
              </div>
                  
                  </div>
                  <div id="${formId}_addressArea" class="atg-base-table-customer-credit-card-address-form" style="display:none;">
                    <div class="atg-csc-base-table atg-base-table-customer-address-add-form">
                      <dsp:include src="${addressForm.URL}" otherContext="${agentUIConfig.truContextRoot}">
                        <dsp:param name="formId" value="${formId}"/>
                        <dsp:param name="addressBean" value="${creditCardAddressBean}"/>
                        <dsp:param name="submitButtonId" value="${submitButtonId}"/>
                        <dsp:param name="isDisableSubmit" value="${formId}DisableSubmit"/>
                        <c:choose>
                        <c:when test="${fn:length(existingAddressList)  == 0}">
                          <dsp:param name="validateIf" value="true"/>
                        </c:when>
                        <c:otherwise>
                          <dsp:param name="validateIf" value="dojo.byId('${formId}')['${creditCardFormHandler}.useExistingAddress'][0].checked == false"/>
                        </c:otherwise>
                        </c:choose>
                      </dsp:include>
                    </div>
                  </div>
            </c:when>
            <c:otherwise>
              <dsp:importbean var="ccfh" bean="${creditCardFormHandler}"/>
              <dsp:getvalueof var="addrId" bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.repositoryId"/>
              <dsp:input type="hidden" nullable="false" value="${addrId}"
                bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.REPOSITORYID"/>
              <c:set var="addressMetaInfos" value="${ccfh.addressBook.addressMetaInfos}"/>
              <c:set var="addressFound" value="${false}"/>

              <c:forEach var="ami" items="${ccfh.addressBook.addressMetaInfos}">
                  <c:if test="${ami.value.valid}">
                    <c:set var="addressFound" value="${true}"/>
                  </c:if>
              </c:forEach>
              <%-- When there is no existing addresses, do not display the radio buttons. Instead just
              display the address form. --%>

              <c:choose>
                <c:when test="${!addressFound}">
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label>
                  </span>

                  <script type="text/javascript" charset="utf-8">
                    _container_.onLoadDeferred.addCallback(function () {
                    atg.commerce.csr.${formId}existingBillingAddressPicker();
                  });
                  </script>
                </c:when>
                <c:otherwise>
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label><span class="requiredStar">*</span>
                  </span>

                  <div class="atg-csc-base-table-cell">
                    <dsp:getvalueof var="createNewAddress" bean="${creditCardFormHandler}.createNewAddress" />
                    <dsp:input type="radio" checked="true" iclass="atg_inlineLabel"
                      bean="CreditCardFormHandler.createNewAddress" value="false"
                      priority="999" id="${formId}_newAddress"
                      onclick="${formId}HideAddressArea();"/>
                    <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.useExistingAddress"/></span>
                  </div>
                  
                  <div class="atg-csc-base-table-cell">  
                    <dsp:select id="${formId}_existingAddressList" iclass=""
                      bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.repositoryId"
                      onchange="atg.commerce.csr.customer.existingCreditCardAddressChanged();">
                    
                      <c:forEach var="ami" items="${ccfh.addressBook.addressMetaInfos}">
                      
                        <c:if test="${ami.value.valid}">
                          <c:choose>
                          <c:when test="${!empty ami.value.params.defaultOptions.billingAddress}">
                           <c:forEach var="nickname" items="${ami.value.nicknames}">
                            <c:set var="temp" value="${nickname}"></c:set>
                            </c:forEach> 
                            <dsp:option value="${ami.key}"    iclass="${fn:escapeXml(temp)}" selected="true">
                        <%--  <c:forEach var="nickname" items="${ami.value.nicknames}">
                            
                             ${fn:escapeXml(nickname)},
                            </c:forEach> --%>
                          
                             ${fn:escapeXml(ami.value.address.firstName)},
                              ${fn:escapeXml(ami.value.address.lastName)},
                              ${fn:escapeXml(ami.value.address.city)},
                              ${fn:escapeXml(ami.value.address.country)},
                              ${fn:escapeXml(ami.value.address.state)},
                              ${fn:escapeXml(ami.value.address.postalCode)},
                              ${fn:escapeXml(ami.value.address.phoneNumber)},
                              ${fn:escapeXml(ami.value.address.address1)},
                              ${fn:escapeXml(ami.value.address.address2)},
                              ${fn:escapeXml(ami.value.address.address3)}
                            </dsp:option>
                          </c:when>
                          <c:otherwise>
                            <c:forEach var="nickname" items="${ami.value.nicknames}">
                            <c:set var="temp" value="${nickname}"></c:set>
                            </c:forEach> 
                            <dsp:option value="${ami.key}" iclass="${fn:escapeXml(temp)}">
                           
                             ${fn:escapeXml(ami.value.address.firstName)},
                              ${fn:escapeXml(ami.value.address.lastName)},
                              ${fn:escapeXml(ami.value.address.city)},
                              ${fn:escapeXml(ami.value.address.country)},
                              ${fn:escapeXml(ami.value.address.state)},
                              ${fn:escapeXml(ami.value.address.postalCode)},
                              ${fn:escapeXml(ami.value.address.phoneNumber)},
                              ${fn:escapeXml(ami.value.address.address1)},
                              ${fn:escapeXml(ami.value.address.address2)},
                              ${fn:escapeXml(ami.value.address.address3)}
                            </dsp:option>
                          </c:otherwise>
                          </c:choose>
                          <script type="text/javascript">
                            _container_.onLoadDeferred.addCallback( function() {
                               atg.commerce.csr.customer.addrList[ "${ami.key}" ] = {
                                    id: "${ami.key}",
                                 first: "${fn:escapeXml(ami.value.address.firstName)}",
                                middle: "${fn:escapeXml(ami.value.address.middleName)}",
                                  last: "${fn:escapeXml(ami.value.address.lastName)}" };
                             });
                          </script>
                        </c:if>
                      </c:forEach>
                    </dsp:select>
                  </div>
                </c:otherwise>
              </c:choose>
              </div>
              <div class="atg-csc-base-table-row">
                <div class="atg-csc-base-table-cell"></div>
                <div class="atg_commerce_csr_createNewAddress atg-csc-base-table-cell">
                  <script type="text/javascript" charset="utf-8">
                    atg.commerce.csr.${formId}existingBillingAddressPicker = function(){
                      var existingAddressList = dojo.byId('${formId}_existingAddressList');
                      if(existingAddressList){
                        existingAddressList.disabled = true;
                      }
                      dojo.style(dojo.byId('${formId}_addressArea'),'display','block');
                      if(dojo.byId('creditCardPopup').style.display == 'block'){
                        dijit.byId('creditCardPopup').layout();
                      }
                    }
                  </script>
                  <div>
                    <c:choose>
                     <c:when test="${addressFound}">
                      <dsp:input type="radio" checked="false" id="${formId}_newAddress"
                        iclass="atg_inlineLabel" priority="999"
                        bean="CreditCardFormHandler.createNewAddress" value="true"
                        onclick="atg.commerce.csr.${formId}existingBillingAddressPicker();"/>
                      <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.createNewAddress"/></span>
                      </c:when>
                      <c:otherwise>
                        <dsp:input type="hidden" checked="false" id="${formId}_newAddress"
                          iclass="atg_inlineLabel" priority="999"
                          bean="CreditCardFormHandler.createNewAddress" value="true"
                          />
                      </c:otherwise>
                    </c:choose>
                  </div>
                  
                </div>
                  </div>
                  
                  </div>
                  
                  <div id="${formId}_addressArea" class="atg-base-table-customer-credit-card-address-form" style="display:none;">
                      <div class="atg-csc-base-table atg-base-table-customer-address-add-form">
                        <dsp:include src="${addressForm.URL}" otherContext="/truagent">
                          <dsp:param name="formId" value="${formId}"/>
                          <dsp:param name="addressBean" value="${creditCardAddressBean}"/>
                          <dsp:param name="submitButtonId" value="${submitButtonId}"/>
                          <dsp:param name="isDisableSubmit" value="${formId}DisableSubmit"/>
                           <c:choose>
                           <c:when test="${addressFound}">
                            <dsp:param name="validateIf" value="dojo.byId('${formId}')['${creditCardFormHandler}.createNewAddress'][0].checked == false"/>
                           </c:when>
                           <c:otherwise>
                              <dsp:param name="validateIf" value="true"/>
                           </c:otherwise>
                           </c:choose>
                        </dsp:include>
                      </div>
                  </div>
            </c:otherwise>
          </c:choose>
      

      
      
      <script type="text/javascript">
        var ${formId}DisableSubmit = function () {
          var disable = false;
          if (!dijit.byId("${formId}_creditCardType")._isvalid) disable = true;
          if (dijit.byId("${formId}_creditCardNumber") &&
            !dijit.byId("${formId}_creditCardNumber")._isvalid) disable = true;
          if (dijit.byId("${formId}_maskedCreditCardNumber") &&
            !dijit.byId("${formId}_maskedCreditCardNumber")._isvalid) disable = true;
          if (!dijit.byId("${formId}_expirationMonth")._isvalid) disable = true;
          if (!dijit.byId("${formId}_expirationYear")._isvalid) disable = true;
          return disable;
        };
        var ${formId}IsValidCardType = function () {
          if (dijit.byId("${formId}_creditCardNumber")) dijit.byId("${formId}_creditCardNumber").validate(false);
          if (dijit.byId("${formId}_maskedCreditCardNumber")) dijit.byId("${formId}_maskedCreditCardNumber").validate(false);
          ${formId}Validate();
          return this._isvalid;
        };
        var ${formId}IsValidCardNum = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumber(
            dijit.byId("${formId}_creditCardType"),
            dijit.byId("${formId}_creditCardNumber"));
          return this._isvalid;
        };
        var ${formId}IsValidMaskedCardNum = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumberInEditContext({
            creditCardType : dijit.byId('${formId}_creditCardType'),
            creditCardNumber: dijit.byId('${formId}_maskedCreditCardNumber'),
            originalMaskedCreditCardNumber: '${maskedCreditCardNumberVar}'
          });
          return this._isvalid;
        };
        var ${formId}IsValidMonthCombo = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardMonth(
            dijit.byId("${formId}_expirationMonth"), dijit.byId("${formId}_expirationYear"));
          dijit.byId("${formId}_expirationMonth").isValid = function () { return this._isvalid; }; // prevent recursion
          dijit.byId("${formId}_expirationYear").validate(false);
          ${formId}Validate();
          dijit.byId("${formId}_expirationMonth").isValid = ${formId}IsValidMonthCombo;
          return this._isvalid;
        };
        var ${formId}IsValidYearCombo = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardYear(
            dijit.byId("${formId}_expirationYear"));
          dijit.byId("${formId}_expirationYear").isValid = function () { return this._isvalid; }; // prevent recursion
          dijit.byId("${formId}_expirationMonth").validate(false);
          ${formId}Validate();
          dijit.byId("${formId}_expirationYear").isValid = ${formId}IsValidYearCombo;
          return this._isvalid;
        };
        var ${formId}HideAddressArea = function () {
          dojo.style(dojo.byId('${formId}_addressArea'),'display','none'); 
          var existingAddressList = dojo.byId('${formId}_existingAddressList');
          if(existingAddressList){
            existingAddressList.disabled = false;
          }
        };
        _container_.onLoadDeferred.addCallback(function () {
          if (dijit.byId("${formId}_creditCardType")) {
            dijit.byId("${formId}_creditCardType").isValid = ${formId}IsValidCardType;
          }
          if (dijit.byId("${formId}_maskedCreditCardNumber")) {
            dijit.byId("${formId}_maskedCreditCardNumber").isValid = ${formId}IsValidMaskedCardNum;
          }
          if (dijit.byId("${formId}_creditCardNumber")) {
            dijit.byId("${formId}_creditCardNumber").isValid = ${formId}IsValidCardNum;
          }
          if (dijit.byId("${formId}_expirationMonth")) {
            dijit.byId("${formId}_expirationMonth").isValid = ${formId}IsValidMonthCombo;
          }
          if (dijit.byId("${formId}_expirationYear")) {
            dijit.byId("${formId}_expirationYear").isValid = ${formId}IsValidYearCombo;
          }
        });
        /* function setCvvLength()
        {
        	$(".error").remove();
        	document.getElementById("${formId}_cvv").value = "";
        	var creditCardType = document.getElementById("${formId}_creditCardType").value;
      	  	if(creditCardType == "American Express")
      	  		{
      	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","4");
      	  		}
      	  	else
      	  		{
      	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","3");
      	  		}
      	  if(creditCardType == 'R Us Private Label Credit Card')
	  		{
	  			document.getElementById("${formId}_expirationMonth").disabled = true;
	  			document.getElementById("${formId}_expirationYear").disabled = true;
	  			$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","none");
	  		}
	  	else
	  		{
	  			document.getElementById("${formId}_expirationMonth").disabled = false;
				document.getElementById("${formId}_expirationYear").disabled = false;
	  			$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","all");
	  		}
        }
        $(document).ready(function(){
        	setCvvLength();
        }) */
        
        
        
      </script>
   
    </dsp:layeredBundle>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <%
     Exception ee = (Exception) pageContext.getAttribute("exception");
     ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/order/creditCardForm.jsp#1 $$Change: 875535 $--%>
