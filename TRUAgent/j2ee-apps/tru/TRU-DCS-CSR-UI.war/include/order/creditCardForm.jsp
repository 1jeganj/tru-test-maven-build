<%--
 This page defines the credit card form using the following input parameters:

 formId - the DOM ID of the form node to submit to add or update credit cards
 creditCardBean - the object that has credit card type, number and expiration date
 creditCardAddressBean - the object that allows the option of adding a new address to the credit card
 creditCardFormHandler - the full path to the form handler to add or update the credit card
 submitButtonId - the DOM ID of the submit button so that the button can be enabled or
   disabled based on the validity of the form inputs
 isMaskCardNumber - edit credit card hides the number with a mask exposing only the last
   four digits
 isUseExistingAddress - Some of the form handlers have the opposite logic from each other:
   some use an isUseExistingAddress property whose state must be false to create new addresses,
   others use the opposite isCreateNewAddress property whose state must be true to create new addresses.
   Note: this is not a way to turn off the address or new address area of the credit card form.

@version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/include/order/creditCardForm.jsp#1 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>

<c:catch var="exception">
  <dsp:page xml="true">
	<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/svc/agent/ui/CSRConfigurator"/>
    <dsp:importbean var="agentUIConfig" bean="/atg/svc/agent/ui/AgentUIConfiguration"/>
    <dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
    <dsp:importbean var="cart" bean="/atg/commerce/custsvc/order/ShoppingCart" />
    <dsp:importbean bean="/com/tru/common/droplet/DisplayMonthAndYearDroplet"/>
    <dsp:importbean var="addressForm" bean="/atg/svc/agent/ui/fragments/AddressForm"/>
    <dsp:getvalueof var="formId" param="formId"/>
    <dsp:getvalueof var="creditCardBean" param="creditCardBean"/>
    <dsp:getvalueof var="creditCardAddressBean" param="creditCardAddressBean"/>
    <dsp:getvalueof var="creditCardFormHandler" param="creditCardFormHandler"/>
    <dsp:getvalueof var="submitButtonId" param="submitButtonId"/>
    <dsp:getvalueof var="isMaskCardNumber" param="isMaskCardNumber"/>
    <dsp:getvalueof var="isUseExistingAddress" param="isUseExistingAddress"/>
    <dsp:getvalueof var="disableCreditCardType" param="disableCreditCardType"/>
    <dsp:getvalueof var="disableCreditCardNumber" param="disableCreditCardNumber"/>
    <dsp:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode"/>
    <dsp:getvalueof var="PLCCExpMonth" bean="TRUConfiguration.expMonthAdjustmentForPLCC"/>
    <dsp:getvalueof var="PLCCExpYear" bean="TRUConfiguration.expYearAdjustmentForPLCC"/>
     <dsp:getvalueof var="isEditCreditCard" param="isEditCreditCard"/>
       <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources"
			var="TRUCustomResources" />  
    <dsp:layeredBundle basename="atg.commerce.csr.order.WebAppResources">
      <fmt:message var="emptyAddressSelection" key="emptyAddressSelection" />
      <fmt:message var="invalidCreditCardType" key="invalidCreditCardType" />
      <fmt:message var="invalidCreditCardNumber" key="invalidCreditCardNumber" />
      <fmt:message var="creditCardNumberMissing" key="creditCardNumberMissing" />
      <fmt:message var="invalidCreditCardDate" key="invalidCreditCardDate" />
      <fmt:message var="invalidExistingAddress" key="invalidExistingAddress" />
      <fmt:message var="invalidCardName" key="invalidCardName" bundle="${TRUCustomResources}"/>
      
      <dsp:getvalueof var="cvvNumberMissing" value="cvvNumberMissing" />
      <input type="hidden" id="PLCCExpMonth" value="${PLCCExpMonth}"/>
      <input type="hidden" id="PLCCExpYear" value="${PLCCExpYear}"/>
      <style>
.atg-csc-base-table-cell{padding-top:7px!important;}
.select-box{float:left;}
.select-box .select-box-input{    margin-top: -8px;
    float: left;
    position: absolute;}
	#csrBillingAddCreditCard_creditCardForm #csrBillingAddCreditCard_expirationMonth
	{
		-moz-appearance: none;
		-webkit-appearance: none;
		appearance: none;
		height:20px;
		border:1px solid #cccccc;
		background:transparent url(/agent/images/icons/icon_dropdown_large.png) no-repeat scroll right center;
		background-size:16px;
	}
</style>
      <c:if test="${disableCreditCardType}">
        <script type="text/javascript">
          _container_.onLoadDeferred.addCallback(function(){
               var ccType = dijit.byId('${formId}_creditCardType');
               if (ccType) ccType.setDisabled(true);
          });
        </script>
      </c:if>

      <c:if test="${disableCreditCardNumber}">
        <script type="text/javascript">
          _container_.onLoadDeferred.addCallback(function(){
            atg.commerce.csr.common.disableTextboxWidget('${formId}_maskedCreditCardNumber');
            atg.commerce.csr.common.disableTextboxWidget('${formId}_creditCardNumber');
          });
          
        </script>
      </c:if>
      <style>
      	#csrBillingAddCreditCard_addressArea .atg-csc-base-table
      	{
      		display: table-row;
      	}
      	#editCreditCardPagePrompt #csrBillingEditCreditCard_addressArea
      	{
      		    height: 265px !important;
      	}
      	.atg-csc-instore-payment-div
      	{
      		padding-top: 0px !important;
      	}
      	
      </style>
<script>
function getNONCEResponce(){
	var nonceRetryAttempts = 0;
	var maxNonceRetryAttempts = 0;
	var errorFlag = false;
	
	var enableVerboseDebug = false;
	if(enableVerboseDebug) {
		 console.log("Inside getNONCEResponce()");
	}
	$.ajax({
			url:  "/TRU-DCS-CSR/include/checkout/common/nonceAjax.jsp",
			type: "POST",
			async:false,
			dataType:"json",
			cache:false,
			success: function(nonceData){
     				if(typeof nonceData == 'string'){
     					nonceData = JSON.parse(nonceData);
     				}
				if(typeof nonceData == 'object' && typeof nonceData.nonce != 'undefined' && typeof nonceData.jwt != 'undefined' && nonceData.nonce != '' && nonceData.radialPaymentErrorKey != '50002'){
					 if(enableVerboseDebug) {
						 console.log("getNONCEResponce() Radial Nonce:"+nonceData.nonce);
						 console.log("getNONCEResponce() Radial jwt:"+nonceData.jwt);
						 console.log("getNONCEResponce() START Radial setup()");
					 }
					 Radial.setup(nonceData.nonce, nonceData.jwt);
					 if(enableVerboseDebug) {
						 console.log("getNONCEResponce() END calling Radial setup()");
					 }
				 	jwt = nonceData.jwt;
					nonceSetup = true;
					nonceRetryAttempts = 0;
					return false;
				 }
				nonceRetryAttempts++;
				if(enableVerboseDebug) {
					console.log("Error in getNONCEResponce() and showing the Error radialPaymentErrorKey:" + nonceData.radialPaymentErrorKey);
				}
				if(nonceRetryAttempts == maxNonceRetryAttempts) {
					if(enableVerboseDebug) {
						console.log("Error in getNONCEResponce() and showing the Error msg");
					}
					nonceRetryAttempts = 0;
					nonceSetup = false;
					var radailPaymentErrorCode = nonceData.radialPaymentErrorKey;
					var radailPaymentKey = errorKeyJson[radailPaymentErrorCode];
					var radailPaymentMsg= errorKeyJson[key];
					$('.checkout-payment-header').eq(0).before('<span class="errorDisplay">'+radailPaymentMsg+'</span>');
					return false;
				}
				errorFlag = true;
     		
			},
			error:function(e){
	            console.log(e);
			}
	
}); 
		if(errorFlag) {
			getNONCEResponce();
		}
}


function tokenizeCallBackHandler(data) {
	var getTokenRetryAttempts = 0;
	var maxGetTokenRetryAttempts = 0
	var enableVerboseDebug = true;
	if(enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() response data token:"+data.account_token);
	}
	var radailPaymentErrorCode = data.ErrorNumber;
	if(enableVerboseDebug) {
		console.log("tokenizeCallBackHandler() in Error Number:"+radailPaymentErrorCode);
	}
	//getting nonce again and re_try token
	if(radailPaymentErrorCode !=undefined && radailPaymentradailPaymentErrorCode=='50002' && getTokenRetryAttempts < maxGetTokenRetryAttempts){
		getTokenRetryAttempts++;
		startTokenization();
		return false;
	}else if(radailPaymentErrorCode !=undefined && radailPaymentErrorCode=='50001'|| radailPaymentErrorCode=='50003' && getTokenRetryAttempts < maxGetTokenRetryAttempts){
		getTokenRetryAttempts++;
		startTokenization();
		return false;
	}else if(radailPaymentErrorCode !=undefined && radailPaymentErrorCode=='40001'|| radailPaymentErrorCode=='40002'||radailPaymentErrorCode=='40003' ||radailPaymentErrorCode=='40004' ||radailPaymentErrorCode=='50001' ||radailPaymentErrorCode=='50002'||radailPaymentErrorCode=='50003'){
		getTokenRetryAttempts = 0;
		/* $('.checkout-payment-header').eq(0).before('<span class="errorDisplay">'+radailPaymentErrorCode+'</span>'); */
		alert("Error occured while processing radial services with code "+radailPaymentErrorCode);
		return false;
	}else{
	var form = document.getElementById('billingForm');
	var ccToken=data.account_token;
	$("#CardinalToken").val(ccToken);
	dojo.xhrPost({form:document.getElementById('billingForm'),url:"/TRU-DCS-CSR/panels/order/billing/billing.jsp?",content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(_1f,_2f){
			if(!(_1f instanceof Error)){
 				atgNavigate({panelStack:"cmcBillingPS", queryParams: { init : 'true' }});return false;
    		 }
		},mimetype:"text/html"});
	}
}



/* This function is used to validate credit card number based on mod 10 and Luhn algorithm. */
function validateCreditCardNumber(value) {
	// accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value))
		return false;
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n), nDigit = parseInt(cDigit, 10);
		if (bEven) {
			if ((nDigit *= 2) > 9)
				nDigit -= 9;
		}
		nCheck += nDigit;
		bEven = !bEven;
	}
	return (nCheck % 10) == 0;
}


function startTokenization(suggestedAddressFromAJAX) {
	var email=dojo.byId("email").value;
	if($('#atg_commerce_csr_addNewCreditCardPane').find('.error').length)
	{
		$('.error').remove();
	} 
	var cardType = getCreditCardType();
	var enableVerboseDebug = false;
	var accountNumberbefore = $('#${formId}_creditCardNumber').val();
	 var isValidCard = validateCreditCardNumber(accountNumberbefore);
	 if(!isValidCard) {
		$('#atg_commerce_csr_addNewCreditCardPane').prepend('<p class="error" style="color:red;font-weight:bold;">Please enter a valid card number</p>');
		return false;
		}
	 var emptyInputError = false;
	 $("#csrBillingAddCreditCard_creditCardForm").find("input[type='text'],select").not(":hidden").filter(function(){
			if($.trim($(this).val()) == '')
				{
					emptyInputError = true;
				}
		});
	 if(cardType == "RUSPrivateLabelCard" && $("#csrBillingAddCreditCard_expirationMonth").val() == "" && $("#csrBillingAddCreditCard_expirationYear").val() == "Year")
		 {
		 	emptyInputError = false;
		 }
		if(emptyInputError)
		{
			return false;
		}
	if($('#csrBillingAddCreditCard_addressArea').find('#error').length)
	{
		$('#error').remove();
	}
	
	 var orderId = $('#${formId}_orderId').val();
	 
	 var accountNumber = $('#${formId}_creditCardNumber').val();
	 var nameOnCard = $('#${formId}_nameOnCreditCard').val();
	 var tmpVal = $('#${formId}_expirationMonth').val();
	 var expirationMonth,expirationYear;
	 if(cardType == 'RUSPrivateLabelCard')
		 {
			expirationMonth = $("#PLCCExpMonth").val();
		 	expirationYear= $("#PLCCExpYear").val();
		 }
	 else
		 {
			 expirationMonth = (tmpVal.length <= 1) ? '0'+ tmpVal : tmpVal;
			 expirationYear= $('#${formId}_expirationYear').val();
		 }
	 var truCurrencyCode = $('#${formId}_truCurrencyCode').val();
	 var cardCode = $('#${formId}_cvv').val();
	 var defaultShipping = $(".defaultSave_checkBox").val();
	 var existingAddress = $('.atg_commerce_csr_add_credit_card_form').find("input[type='radio']:checked").val();
	 
	 if(existingAddress === undefined){
		 existingAddress = 'false';
	 }
	 if(existingAddress == 'false' &&  !suggestedAddressFromAJAX && ($("input[name=valid-address][type=radio]:checked").val() == "0" || $("input[name=valid-address]").length == '0')){
	 	 var firstName = $('#${formId}_firstName').val();
		 var middleName = $('#${formId}_middleName').val();
		 var lastName = $('#${formId}_lastName').val();
		 var country=dijit.byId("${formId}_country");
		 var address1 = $('#${formId}_address1').val();
		 var city = $('#${formId}_city').val();
		 var state1 = $('#${formId}_state').val();
		 var postalCode = $('#${formId}_postalCode').val();
		 var phoneNumber = $('#${formId}_phoneNumber').val();
		 var address2 = $('#${formId}_address2').val();
		if(country != 'US')
		{
			if(state1 == 'NA')
				{
					var state = 'NA'
				}
		}
		else{
			state1 = state1.split("-")[0];
			var state = $.trim(state1);
		}
	 }
	 
	 else if(existingAddress == 'true')
		 {
		 country = $('#${formId}_country_existingAddress').val();
	   		 address1 = $('#${formId}_address1_existingAddress').val();
	   		 city = $('#${formId}_city_existingAddress').val();
	   		 state = $('#${formId}_state_existingAddress').val();
	   		 postalCode = $('#${formId}_postalCode_existingAddress').val();
	   		 address2 = $('#${formId}_address2_existingAddress').val();
		 	 phoneNumber = $('#${formId}_phoneNumber_existingAddress').val();
			 firstName = $('#${formId}_firstName_existingAddress').val();
			 middleName = $('#${formId}_middleName_existingAddress').val();
			 lastName = $('#${formId}_lastName_existingAddress').val();
			 state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
	     	 
		 }
	 else{
		var country,address1,city,state1,postalCode,phoneNumber,address2,pageName,firstName,middleName,lastName;
     	var addressFlag = $("input[name=valid-address][type=radio]:checked").val();
     	if(addressFlag != "0" && !suggestedAddressFromAJAX){
   			 country= $("#suggestedCountry-"+addressFlag).val();
			 address1 = $('#suggestedAddress1-'+addressFlag).val();
   			 city = $('#suggestedCity-'+addressFlag).val();
   			 state = $('#suggestedState-'+addressFlag).val();
   			 postalCode = $('#suggestedPostalCode-'+addressFlag).val();
   			 address2 = $('#suggestedAddress2-'+addressFlag).val();
   			 phoneNumber = $('#${formId}_phoneNumber').val();
   			 firstName = $('#${formId}_firstName').val();
   			 middleName = $('#${formId}_middleName').val();
   			 lastName = $('#${formId}_lastName').val();
   			 state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
	     	if(country == 'United States' || country == 'UNITED STATES'){
	     		country='US';
	     	}
     	}
     	else if(suggestedAddressFromAJAX){
     		 country= suggestedAddressFromAJAX.country;//$("#suggestedCountry-"+addressFlag).val();
			 address1 = suggestedAddressFromAJAX.address1;//$('#suggestedAddress1-'+addressFlag).val();
   			 city = suggestedAddressFromAJAX.city;//$('#suggestedCity-'+addressFlag).val();
   			 state = suggestedAddressFromAJAX.state;//$('#suggestedState-'+addressFlag).val();
   			 postalCode = suggestedAddressFromAJAX.postalCode;//$('#suggestedPostalCode-'+addressFlag).val();
   			 address2 = suggestedAddressFromAJAX.address2;//$('#suggestedAddress2-'+addressFlag).val();
   			 phoneNumber = $('#${formId}_phoneNumber').val();
   			 firstName = $('#${formId}_firstName').val();
   			 middleName = $('#${formId}_middleName').val();
   			 lastName = $('#${formId}_lastName').val();
   			 state1 = state.split("-")[0];
	     	 var state = $.trim(state1);
     	}
 /*     	firstName = $('#${formId}_firstName_existingAddress').val();
  		middleName = $('#${formId}_middleName_existingAddress').val();
  		lastName = $('#${formId}_lastName_existingAddress').val();
     	phoneNumber = $('#csrBillingAddCreditCard_phoneNumber'+addressFlag).val();
     	state1 = state1.split("-")[0];
  		state = $.trim(state1); */
	 }
	  $('.errorDisplay').remove();		  
	 

    var d = new Date();
   var timeStamp = d.getTime(); 
    
  /*  if( country ne 'United States' or country ne 'US'){
	  state = "";
	   
   } */
   var currentMonth = d.getMonth() + 1;
   var currentYear = d.getFullYear();
	$("#nameOnCard").val(nameOnCard);
	$("#expirationMonth").val(expirationMonth);
	$("#expirationYear").val(expirationYear);
	$("#cardCode").val(cardCode);
	$('#accountNumber').val(accountNumber);
	$("#cardType").val(cardType);
	$('#cardLength').val(accountNumber.length);
	/* $("#accountNumber").val(accountNumber); */
	$("#firstName").val(firstName);
	$("#middleName").val(middleName);
	$("#lastName").val(lastName);
	$("#country").val(country);
	$("#address1").val(address1);
	$("#address2").val(address2);
	$("#city").val(city);
	$("#state").val(state);
	$("#postalCode").val(postalCode);
	$("#phoneNumber").val(phoneNumber);
	//$("#copyToProfile").val(defaultShipping);
	$("#copyAddress").val(defaultShipping);
	$("#creditCardEmail").val(email);
   if( parseInt(expirationMonth) < currentMonth && parseInt(expirationYear) == currentYear ){
   /* <span class="errorDisplay">Expiry date is not valid, please select valid date</span>    		 */
		return false;
   }
	var isEditSavedCard = $('#isEditSavedCard').val();
	/* if(isEditSavedCard == 'false'){	 */		
	/* try {
		Cardinal.start('cca', {
		   "OrderDetails":{  
			  "OrderNumber": orderId + timeStamp,
			  "Amount":"0000",
			  "CurrencyCode":truCurrencyCode
		   },
		   "Consumer":{  
			  "Account":{  
				 "AccountNumber":accountNumber,
				 "ExpirationMonth":expirationMonth,
				 "ExpirationYear":expirationYear,
				 "CardCode":cardCode,
				 "NameOnAccount":nameOnCard
			  }
		   },
		   "Options":{  
			  "EnableCCA":false
		   }
		});	
		

    			
	} catch (e) {
		
   	console.log( (window['Cardinal'] === undefined ? "Payment service did not load properly. " : "An error occurred during processing. ") + e );
		return false;
   } */
		//cardinalApi = "Tokenization";
	/* } else {
		onSubmitAddCreditCard();
	} */
	
	var jsonObject = 
    {

    		"cmpiRequestObject" : {
    			"OrderNumber" : orderId,
    			"CurrencyCode": truCurrencyCode,
    			"Amount": "0.00",
    			"CardExpMonth": expirationMonth,
    			"CardExpYear" : expirationYear,
    			"CardNumber": accountNumber,
    			"EMail": "",
    			"BillingFirstName": firstName,
    			"BillingLastName":  lastName,
    			"BillingAddress1": address1,
    			"BillingAddress2": address2,
    			"BillingCity": city,
    			"BillingState": state,
    			"BillingCountryCode": country,
    			"BillingPostalCode": postalCode,
    			"BillingPhone": phoneNumber,
    			"IPAddress": "0:0:0:0:0:0:0:1",  // Clientâ€™s IP
    			"UserAgent": navigator.userAgent
    		}

    }
		 if(enableVerboseDebug) {
			 console.log("calling getNONCEResponce() for accountNumber:"+accountNumber);
		} 
			getNONCEResponce();
			//Radial.tokenize()
		if(enableVerboseDebug) {
			 console.log("calling tokenizeAndAuthorize() for accountNumber:"+accountNumber);
		}
		Radial.tokenize(accountNumber,tokenizeCallBackHandler);
		cardinalApi = "Tokenization_checkout";
		return false;
		//tokenize(accountNumber, authorizeSavedTokenCallbackHandler);
		//cardinalApi = "TokenizationAndAuthentication";
		
}

function getCreditCardType(currentEvent){
       var value = $("#csrBillingAddCreditCard_creditCardNumber").val();
       var cardType = '';
       if (value.length > 5) {
           var v = parseInt(value.substr(0, 6));

           if ((v >= 400000 && v <= 499999) && (value.length > 5 || value.length == 13 || value.length == 16)) { /*visa card*/
               if ((v == 425329 || v == 425332 || v == 425333 || v == 425334) && (value.length > 5 || value.length == 13 || value.length == 16)) {
                   cardType = 'invalid';
               } else {
                   $(this).attr('maxlength', '16');
                   cardType = 'visa';
               }
           } else if ((((v >= 510000 && v <= 523769) || (v >= 523771 && v <= 524362) || (v >= 524364 && v <= 559999)) && (value.length > 5 || value.length == 16))) {
               $(this).attr('maxlength', '16');
               cardType = 'masterCard';
           } else if ((v >= 360000 && v <= 369999 && (value.length == 14 || value.length == 16))) {
               if (value.length == 14) {
                   cardType = 'masterCard';
               } else if (value.length == 16) {
                   $(this).attr('maxlength', '16');
                   cardType = 'discover';
               } else {
                   cardType = 'invalid';
               }
           } else if (((v >= 370000 && v <= 379999) || (v >= 340000 && v <= 349999)) && (value.length > 5 || value.length == 15)) { /*Amex card*/
               $(this).attr('maxlength', '15');
               cardType = 'americanExpress';
           } else if ((v >= 601100 && v <= 601199 || v >= 622126 && v <= 622925 || v >= 628200 && v <= 628899 || v >= 644000 && v <= 659999 || v >= 300000 && v <= 305999 || v >= 309500 && v <= 309599 || v >= 352800 && v <= 358999 || v >= 380000 && v <= 399999 || v >= 624000 && v <= 625999) && (value.length > 5 || value.length == 16)) { /*discover card*/
               if ((v == 601104 || (v >= 601110 && v < 601120) || (v >= 601150 && v < 601174) || (v >= 601175 && v < 601177) || (v >= 601180 && v < 601186)) && (value.length > 5 || value.length == 16)) {
                   cardType = 'invalid';
               } else {
                   $(this).attr('maxlength', '16');
                   cardType = 'discover';
               }
           } else if (v == 604586 && (value.length > 5 || value.length == 16)) { /*PLCC card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSPrivateLabelCard';
           } else if (v == 524363 && (value.length > 5 || value.length == 16)) { /*USCOBMasterCard card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSCoBrandedMasterCard';
           } else if (v == 523770 && (value.length > 5 || value.length == 16)) { /*PRCOBMasterCard card*/
               $(this).attr('maxlength', '16');
               cardType = 'RUSCoBrandedMasterCard';
           } else {
               $(this).removeAttr('maxlength');
               cardType = 'invalid';
           }
       } else {
           $(this).removeAttr('maxlength');
          cardType = 'invalid';
       }
       return cardType;
}

</script>
      <div dojoType="dojo.data.ItemFileReadStore" jsId="cardStore" url="${CSRConfigurator.contextRoot}/include/cardData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>
      <div dojoType="dojo.data.ItemFileReadStore" jsId="monthStore" url="${CSRConfigurator.truContextRoot}/include/monthData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>
      <div dojoType="dojo.data.ItemFileReadStore" jsId="yearStore" url="${CSRConfigurator.contextRoot}/include/yearData.jsp?${stateHolder.windowIdParameterName}=${windowId}"></div>
      

      <div class="atg-csc-base-table">
			<div id="${formId}_existingBillingArea" class="atg-csc-base-table-row" >
                  <div  class="atg-base-table-customer-credit-card-address-form" >
                    <div class="atg-csc-base-table atg-base-table-customer-address-add-form" style="display: table-row;">
	 

      <div class="atg_commerce_csr_existingBilling atg-csc-base-table-row">
          <c:choose>
            <c:when test="${isUseExistingAddress}">
              <dsp:getvalueof var="existingAddressList" bean="${creditCardFormHandler}.existingAddresses" />
              <dsp:getvalueof var="useExistingAddress" bean="${creditCardFormHandler}.useExistingAddress" />
              <%-- When there is no existing addresses, do not display the radio buttons. Instead just
              display the address form. --%>

              <c:choose>
                <c:when test="${fn:length(existingAddressList) == 0}">
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label>
                  </span>
                  
                   <script type="text/javascript" charset="utf-8">
                    _container_.onLoadDeferred.addCallback(function () {
                    atg.commerce.csr.${formId}existingBillingAddressPicker();
                  });
                  </script>
                </c:when>
                <c:otherwise>
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label><span class="requiredStar">*</span>
                  </span>
                
                 <dsp:setvalue param="existAddress" beanvalue="${creditCardFormHandler}.existingAddresses"/>
                 <dsp:setvalue param="existFirstAddress" paramvalue="existAddress[0]"/>
                 <dsp:getvalueof var="firstAddress" param="existFirstAddress" />
                 
                 <c:if test="${formId eq 'csrBillingAddCreditCard'}">
                 <dsp:getvalueof var="defaultBillingAddress" bean="${creditCardFormHandler}.defaultBillingAddress"></dsp:getvalueof>
                 <c:if test="${!empty defaultBillingAddress}">
                 <dsp:getvalueof var="firstAddress" value="${defaultBillingAddress}"></dsp:getvalueof>
                 </c:if>
                 </c:if>
                  <div class="atg-csc-base-table-cell">
                        <dsp:input id="${formId}_existingAddress" type="radio" checked="${useExistingAddress ? true : false}"
                          bean="${creditCardFormHandler}.useExistingAddress"
                          value="true" onclick="${formId}HideAddressArea();"/><input type="hidden" id="existingAddress" value="true"/>
                        <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.useExistingAddress"/></span>
                        </div>
                        <div class="atg-csc-base-table-cell select-box">             
                          <dsp:select id="${formId}_existingAddressList" iclass="select-box-input" bean="${creditCardFormHandler}.addressIndex">
                             <c:if test="${!empty defaultBillingAddress && formId eq 'csrBillingAddCreditCard'}">
                                <dsp:option value="${defaultBillingAddress}">
                                  ${fn:escapeXml(defaultBillingAddress.firstName)},
                                  ${fn:escapeXml(defaultBillingAddress.lastName)},
                                  ${fn:escapeXml(defaultBillingAddress.country)},
                                  ${fn:escapeXml(defaultBillingAddress.city)},
                                  ${fn:escapeXml(defaultBillingAddress.state)},
                                  ${fn:escapeXml(defaultBillingAddress.address1)},
                                  ${!empty defaultBillingAddress.address2 ? ' ' : '' }${!empty defaultBillingAddress.address2 ? fn:escapeXml(defaultBillingAddress.address2) : '' },
                                  ${fn:escapeXml(defaultBillingAddress.postalCode)},
                                  ${fn:escapeXml(defaultBillingAddress.phoneNumber)}
                                </dsp:option>
                                </c:if>
                            <dsp:droplet name="ForEach">
                              <dsp:param name="array" bean="${creditCardFormHandler}.existingAddresses" />
                              <dsp:oparam name="output">
                                <dsp:getvalueof var="address" param="element"/>
                                
                                <dsp:getvalueof var="addressIndex" param="index"/>
                                <c:choose>
                                <c:when test="${formId eq 'csrBillingAddCreditCard'}"><c:set var="addressValuePassed" value="${address},phoneNo=${address.phoneNumber},postalCode=${address.postalCode}"></c:set></c:when>
                                <c:otherwise><c:set var="addressValuePassed" value="${addressIndex}"></c:set></c:otherwise>
                                </c:choose>
                            
                                <dsp:option  value="${addressValuePassed}">
                                 ${fn:escapeXml(address.firstName)},
                                  ${fn:escapeXml(address.lastName)},
                                  ${fn:escapeXml(address.country)},
                                  ${fn:escapeXml(address.city)},
                                  ${fn:escapeXml(address.state)},
                                  ${fn:escapeXml(address.address1)},
                                  ${!empty address.address2 ? ' ' : '' }${!empty address.address2 ? fn:escapeXml(address.address2) : '' },
                                  ${fn:escapeXml(address.postalCode)},
                                  ${fn:escapeXml(address.phoneNumber)}
                                </dsp:option>
                              </dsp:oparam>
                            </dsp:droplet>
                            
                          </dsp:select>
                        </div>
                </c:otherwise>
              </c:choose>
                 <script type="text/javascript">
              	$("#csrBillingAddCreditCard_existingAddressList").on("change",function(){
              		var addressSelected = $(this).val();
              		var firstName = addressSelected.split("mFirstName=")[1].split(",")[0];
              		var middleName = addressSelected.split("mMiddleName=")[1].split(",")[0];
              		var lastName = addressSelected.split("mLastName=")[1].split(",")[0];
              		var address1 = addressSelected.split("mAddress1=")[1].split(",")[0];
              		var address2 = addressSelected.split("mAddress2=")[1].split(",")[0];
              		var city = addressSelected.split("mCity=")[1].split(",")[0];
              		var state = addressSelected.split("mState=")[1].split(",")[0];
              		var country = addressSelected.split("mCountry=")[1].split(",")[0];
              		var phoneNo = addressSelected.split("phoneNo=")[1].split(",")[0];
              		var postalCode = addressSelected.split("postalCode=")[1].split(",")[0];
              		$("#${formId}_firstName_existingAddress").val(firstName);
              		$("#${formId}_middleName_existingAddress").val(middleName);
              		$("#${formId}_lastName_existingAddress").val(lastName);
              		$("#${formId}_country_existingAddress").val(country);
              		$("#${formId}_address1_existingAddress").val(address1);
              		$("#${formId}_address2_existingAddress").val(address2);
              		$("#${formId}_city_existingAddress").val(city);
              		$("#${formId}_state_existingAddress").val(state);
              		$("#${formId}_postalCode_existingAddress").val(postalCode);
              		$("#${formId}_phoneNumber_existingAddress").val(phoneNo);
              		
              	});
              </script>
              </div>
           <c:if test="${isUseExistingAddress}">
              <dsp:getvalueof var="addresses" value="${address}"></dsp:getvalueof>
              <dsp:input bean="${creditCardAddressBean}.firstName" type="hidden" id="${formId}_firstName_existingAddress" value="${firstAddress.firstName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.middleName" type="hidden" id="${formId}_middleName_existingAddress" value="${firstAddress.middleName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.lastName" type="hidden" id="${formId}_lastName_existingAddress" value="${firstAddress.lastName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.country" type="hidden" id="${formId}_country_existingAddress" value="${firstAddress.country}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.address1" type="hidden" id="${formId}_address1_existingAddress" value="${firstAddress.address1}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.address2" type="hidden" id="${formId}_address2_existingAddress" value="${firstAddress.address2}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.city" type="hidden" id="${formId}_city_existingAddress" value="${firstAddress.city}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.state" type="hidden" id="${formId}_state_existingAddress" value="${firstAddress.state}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.postalCode" type="hidden" id="${formId}_postalCode_existingAddress" value="${firstAddress.postalCode}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.phoneNumber" type="hidden" id="${formId}_phoneNumber_existingAddress" value="${firstAddress.phoneNumber}"></dsp:input>
	          <c:if test="${isEditCreditCard eq 'false'}">
	
				<dsp:input type="hidden" bean="${creditCardAddressBean}.newBillingAddress" name="newBillingAddress" id="newBillingAddress" value="false"/>
	      	 </c:if>
         </c:if> 
<%--            <c:if test="${isUseExistingAddress && ! empty defaultBillingAddress}">
              <dsp:getvalueof var="addresses" value="${address}"></dsp:getvalueof>
              <dsp:input bean="${creditCardAddressBean}.firstName" type="hidden" id="${formId}_firstName_existingAddress" value="${defaultBillingAddress.firstName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.middleName" type="hidden" id="${formId}_middleName_existingAddress" value="${defaultBillingAddress.middleName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.lastName" type="hidden" id="${formId}_lastName_existingAddress" value="${defaultBillingAddress.lastName}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.country" type="hidden" id="${formId}_country_existingAddress" value="${defaultBillingAddress.country}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.address1" type="hidden" id="${formId}_address1_existingAddress" value="${defaultBillingAddress.address1}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.address2" type="hidden" id="${formId}_address2_existingAddress" value="${defaultBillingAddress.address2}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.city" type="hidden" id="${formId}_city_existingAddress" value="${defaultBillingAddress.city}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.state" type="hidden" id="${formId}_state_existingAddress" value="${defaultBillingAddress.state}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.postalCode" type="hidden" id="${formId}_postalCode_existingAddress" value="${defaultBillingAddress.postalCode}"></dsp:input>
              <dsp:input bean="${creditCardAddressBean}.phoneNumber" type="hidden" id="${formId}_phoneNumber_existingAddress" value="${defaultBillingAddress.phoneNumber}"></dsp:input>
         </c:if> --%>
              <div class="atg-csc-base-table-row">
                <div class="atg-csc-base-table-cell"></div>
                <div class="atg_commerce_csr_createNewAddress atg-csc-base-table-cell">
                  <script type="text/javascript" charset="utf-8">
                      atg.commerce.csr.${formId}existingBillingAddressPicker = function(){
                    	  $("#existingAddress").val(false);
                        var existingAddressList = dojo.byId('${formId}_existingAddressList');
                        if(existingAddressList){
                          existingAddressList.disabled = true;
                        }
                        dojo.style(dojo.byId('${formId}_addressArea'),'display','block');
                        if(dojo.byId('editPaymentOptionFloatingPane').style.display == 'block'){
                          dijit.byId('editPaymentOptionFloatingPane').layout();
                        }
                      };
                  </script>
                  <div>
                    <c:choose>
                      <c:when test="${fn:length(existingAddressList) >0}">
                        <dsp:input type="radio" checked="${!useExistingAddress ? true : false}"
                          bean="${creditCardFormHandler}.useExistingAddress"
                          value="false" id="${formId}_existingAddress"
                          onclick="atg.commerce.csr.${formId}existingBillingAddressPicker();"/>
                          
                        <fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.createNewAddress"/>
                      </c:when>
                      <c:otherwise>
                        <dsp:input type="hidden" bean="${creditCardFormHandler}.useExistingAddress"
                          value="false" id="${formId}_existingAddress"/>
                      </c:otherwise>
                    </c:choose>
                  </div>

                </div>
              </div></div></div></div>
                  <div id="${formId}_addressArea" class="atg-csc-base-table-row" style="float: left;height:350px;display:none;">
                  <div  class="atg-base-table-customer-credit-card-address-form" style="position:absolute;">
                    <div class="atg-csc-base-table atg-base-table-customer-address-add-form" style="display: table-row;">
                      <dsp:include src="${addressForm.URL}" otherContext="${agentUIConfig.truContextRoot}">
                        <dsp:param name="formId" value="${formId}"/>
                        <dsp:param name="addressBean" value="${creditCardAddressBean}"/>
                        <dsp:param name="submitButtonId" value="${submitButtonId}"/>
                        <dsp:param name="isDisableSubmit" value="${formId}DisableSubmit"/>
                        <c:choose>
                        <c:when test="${fn:length(existingAddressList)  == 0}">
                          <dsp:param name="validateIf" value="true"/>
                        </c:when>
                        <c:otherwise>
                          <dsp:param name="validateIf" value="dojo.byId('${formId}')['${creditCardFormHandler}.useExistingAddress'][0].checked == false"/>
                        </c:otherwise>
                        </c:choose>
                      </dsp:include>
                    </div>
                  </div>
                  </div>
                  <div class="atg-csc-base-table-row" style="height: 24px;"><div style="height: 10px;width: 98.2%;border: 1px solid #d1d1d1;position: absolute;background-color: rgb(232, 232, 232);margin-top: 5px;"></div></div>
                  	<div id="${formId}_creditCardForm" class="atg-csc-base-table-row" style="display: table-row;">
	                  <div  class="atg-base-table-customer-credit-card-form" >
	                    <div class="atg-csc-base-table atg-base-table-customer-creditcard-add-form" >
                  	<div class="atg_commerce_csr_cardType atg-csc-base-table-row">
					<span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
						<label class="atg_messaging_requiredIndicator ">name on card
					</label> <span class="requiredStar">*</span>
					</span>

					<div class="atg-csc-base-table-cell">
							  <dsp:input id="${formId}_orderId" type="hidden" bean="ShoppingCart.current.id"/>
							   <dsp:input id="${formId}_truCurrencyCode" type="hidden" bean="TRUConfiguration.currencyCode"/>
		         <dsp:input type="text" id="${formId}_nameOnCreditCard" iclass="atg-base-table-customer-credit-card-add-number" bean="${creditCardBean}.nameOnCard"
                  required="<%=true%>" size="25" maxlength="25" style="width:175px !important">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCardName}"/>
                  <dsp:tagAttribute name="autocomplete" value="off" />
                </dsp:input>
					</div>
				</div>
				<div class="atg_commerce_csr_cardType atg-csc-base-table-row">
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label class="atg_messaging_requiredIndicator ">
              <fmt:message key="newOrderBilling.addEditCreditCard.header.card.title"/>
            </label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <dsp:input id="${formId}_creditCardType" iclass="" bean="${creditCardBean}.creditCardType"
              required="<%=true%>" style="width:175px !important" onchange="setCvvLength();">
              <dsp:tagAttribute name="dojoType" value="atg.widget.form.FilteringSelect" />
              <dsp:tagAttribute name="autoComplete" value="true" />
              <dsp:tagAttribute name="searchAttr" value="name" />
              <dsp:tagAttribute name="store" value="cardStore" />
              <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardType }"/>
            </dsp:input>
          </div>
        </div>

        <div class="atg_commerce_csr_cardNumber atg-csc-base-table-row" >
        
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label class="atg_messaging_requiredIndicator" >
              <fmt:message key="newOrderBilling.addEditCreditCard.field.cardNumber"/>
            </label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <c:choose>
              <c:when test="${isMaskCardNumber}">

                <%-- Bugs-fixed: 144174-1 The following snippet creates a parameter and the parameter
                     has the masked credit card number and the masked credit card string is passed to the
                     credit card validity routine. --%>
                <dsp:param name="maskedCreditCardNumber" bean="${creditCardBean}.creditCardNumber"
                  converter="creditCard" maskcharacter="*" numcharsunmasked="4"/>
                <dsp:getvalueof var="maskedCreditCardNumberVar" param="maskedCreditCardNumber"/>

                <dsp:input type="text" id="${formId}_maskedCreditCardNumber" iclass="atg-base-table-customer-credit-card-add-number"
                  bean="${creditCardBean}.creditCardNumber"
                  required="<%=true%>" size="25" maxlength="16"
                  converter="creditCard" maskcharacter="*" numcharsunmasked="4" onchange="getCreditCardType(this);">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardNumber }"/>
                  <dsp:tagAttribute name="autocomplete" value="off"/>
                  <dsp:tagAttribute name="disabled" value="true"/>
                </dsp:input>
                <dsp:droplet  name="/atg/commerce/custsvc/events/ViewCreditCardEventDroplet">
                  <dsp:param name="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
                  <dsp:param name="creditCardNumber" bean="${creditCardBean}.creditCardNumber" />
                </dsp:droplet>

              </c:when>
              <c:otherwise>
                <dsp:input type="text" id="${formId}_creditCardNumber" iclass="atg-base-table-customer-credit-card-add-number" bean="${creditCardBean}.creditCardNumber"
                  required="<%=true%>" size="25" maxlength="16" onchange="getCreditCardType(this);">
                  <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
                  <dsp:tagAttribute name="required" value="true" />
                  <dsp:tagAttribute name="trim" value="true" />
                  <dsp:tagAttribute name="promptMessage" value="${creditCardNumberMissing }" />
                  <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardNumber }"/>
                  <dsp:tagAttribute name="autocomplete" value="off" />
                </dsp:input>
              </c:otherwise>
            </c:choose>
          </div>
        </div>
      <div class="atg_commerce_csr_expirationDate atg-csc-base-table-row">
        <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
          <label class="atg_messaging_requiredIndicator">
            <fmt:message key="newOrderBilling.addEditCreditCard.field.expDate"/>
          </label>
          <span class="requiredStar">*</span>
        </span>
        <div class="atg-csc-base-table-cell">
          <dsp:select id="${formId}_expirationMonth" iclass="" bean="${creditCardBean}.expirationMonth"
            required="<%=true%>" style="width:120px;">
             
        		

					<c:choose>
						<c:when test="${isEditCreditCard}">
						<dsp:getvalueof var="expirationMonth"  bean="${creditCardBean}.expirationMonth"  />
						<option value="${expirationMonth}">
							<dsp:valueof bean="${creditCardBean}.expirationMonth" />
						</option>
						</c:when>
						<c:otherwise>
							<option value="">
           						Month
           					</option>
           						</c:otherwise>
					</c:choose>
        		
					<dsp:droplet name="DisplayMonthAndYearDroplet">
													<dsp:param name="monthList" value="monthList"/>
													<dsp:oparam name="output">
														<dsp:droplet name="ForEach">
															<dsp:param name="array" param="monthList"/>
															<dsp:param name="elementName" value="month"/>
															<dsp:oparam name="output">
																<dsp:getvalueof var="month" param="month" />
																<option value='<dsp:valueof param="month" />'
						                                              <dsp:droplet name="/atg/dynamo/droplet/Compare">
						                                                      <dsp:param name="obj1" value="${month}"/>
						                                                      <dsp:param name="obj2" value="${expirationMonth}"/>
						                                                      <dsp:oparam name="equal">
						                                                          selected="selected"
						                                                      </dsp:oparam>
						                                              </dsp:droplet> >
						                                              <dsp:valueof param="month"/>
						                                         </option>
						                                         </dsp:oparam>
															</dsp:droplet>
														</dsp:oparam>
													</dsp:droplet>
          </dsp:select>
        </div>
        <div class="atg-csc-base-table-cell">
          <dsp:input id="${formId}_expirationYear" iclass="" bean="${creditCardBean}.expirationYear"
            required="<%=true%>" style="width:85px;">
            <dsp:tagAttribute name="dojoType" value="atg.widget.form.FilteringSelect" />
            <dsp:tagAttribute name="autoComplete" value="true" />
            <dsp:tagAttribute name="searchAttr" value="name" />
            <dsp:tagAttribute name="store" value="yearStore" />
            <dsp:tagAttribute name="invalidMessage" value="${invalidCreditCardDate }" />
          </dsp:input>
        </div>
      </div>
      
       <div class="atg_commerce_csr_cardNumber atg-csc-base-table-row" >
          <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
          <label class="atg_messaging_requiredIndicator">
            cvv
          </label>
          <span class="requiredStar">*</span>
        </span>
        <div class="atg-csc-base-table-cell">
       
          <dsp:input type="text" id="${formId}_cvv" bean="${creditCardBean}.cavv" required="<%=true%>" size="4" maxlength="4">
             <dsp:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox" />
             <dsp:tagAttribute name="required" value="true" />
             <dsp:tagAttribute name="trim" value="true" />
             <dsp:tagAttribute name="promptMessage" value="${cvvNumberMissing}" />
             <dsp:tagAttribute name="invalidMessage" value="invalid cvv"/>
             <dsp:tagAttribute name="autocomplete" value="off" />
           </dsp:input>
        </div>
      </div></div></div></div>
                  </div>
                 
            </c:when>
            <c:otherwise>
              <dsp:importbean var="ccfh" bean="${creditCardFormHandler}"/>
              <dsp:getvalueof var="addrId" bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.repositoryId"/>
              <dsp:input type="hidden" nullable="false" value="${addrId}"
                bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.REPOSITORYID"/>
              <c:set var="addressMetaInfos" value="${ccfh.addressBook.addressMetaInfos}"/>
              <c:set var="addressFound" value="${false}"/>

              <c:forEach var="ami" items="${ccfh.addressBook.addressMetaInfos}">
                  <c:if test="${ami.value.valid}">
                    <c:set var="addressFound" value="${true}"/>
                  </c:if>
              </c:forEach>
              <%-- When there is no existing addresses, do not display the radio buttons. Instead just
              display the address form. --%>

              <c:choose>
                <c:when test="${!addressFound}">
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label>
                  </span>

                  <script type="text/javascript" charset="utf-8">
                    _container_.onLoadDeferred.addCallback(function () {
                    atg.commerce.csr.${formId}existingBillingAddressPicker();
                  });
                  </script>
                </c:when>
                <c:otherwise>
                  <span class="atg_commerce_csr_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label atg-base-table-customer-credit-card-spacing-two-top">
                    <label class="atg_messaging_requiredIndicator atg_commerce_csr_billingAddress" >
                      <fmt:message key="newOrderBilling.addEditCreditCard.header.billingAddress.title"/>
                    </label><span class="requiredStar">*</span>
                  </span>

                  <div class="atg-csc-base-table-cell">
                    <dsp:getvalueof var="createNewAddress" bean="${creditCardFormHandler}.createNewAddress" />
                    <dsp:input type="radio" checked="true" iclass="atg_inlineLabel"
                      bean="CreditCardFormHandler.createNewAddress" value="false"
                      priority="999" id="${formId}_newAddress"
                      onclick="${formId}HideAddressArea();"/>
                    <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.useExistingAddress"/></span>
                  </div>
                  
                  <div class="atg-csc-base-table-cell">
                    <dsp:select id="${formId}_existingAddressList" iclass=""
                      bean="${creditCardFormHandler}.value.${ccfh.creditCardWallet.addressPropertyName}.repositoryId"
                      onchange="atg.commerce.csr.customer.existingCreditCardAddressChanged();">
                      <c:forEach var="ami" items="${ccfh.addressBook.addressMetaInfos}">
                        <c:if test="${ami.value.valid}">
                          <c:choose>
                          <c:when test="${!empty ami.value.params.defaultOptions.billingAddress}">
                            <dsp:option value="${ami.key}" selected="true">
                              ${fn:escapeXml(ami.value.address.address1)}
                       
                              ${fn:escapeXml(ami.value.address.address2)}
                              ${fn:escapeXml(ami.value.address.address3)}
                            </dsp:option>
                          </c:when>
                          <c:otherwise>
                            <dsp:option value="${ami.key}">
                              ${fn:escapeXml(ami.value.address.address1)}
                              ${fn:escapeXml(ami.value.address.address2)}
                              ${fn:escapeXml(ami.value.address.address3)}
                             
                            </dsp:option>
                          </c:otherwise>
                          </c:choose>
                          <script type="text/javascript">
                            _container_.onLoadDeferred.addCallback( function() {
                               atg.commerce.csr.customer.addrList[ "${ami.key}" ] = {
                                    id: "${ami.key}",
                                 first: "${fn:escapeXml(ami.value.address.firstName)}",
                                middle: "${fn:escapeXml(ami.value.address.middleName)}",
                                  last: "${fn:escapeXml(ami.value.address.lastName)}" };
                             });
                          </script>
                        </c:if>
                      </c:forEach>
                    </dsp:select>
                  </div>
                </c:otherwise>
              </c:choose>
              </div>
              <div class="atg-csc-base-table-row">
                <div class="atg-csc-base-table-cell"></div>
                <div class="atg_commerce_csr_createNewAddress atg-csc-base-table-cell">
                  <script type="text/javascript" charset="utf-8">
                    atg.commerce.csr.${formId}existingBillingAddressPicker = function(){
                      var existingAddressList = dojo.byId('${formId}_existingAddressList');
                      if(existingAddressList){
                        existingAddressList.disabled = true;
                      }
                      dojo.style(dojo.byId('${formId}_addressArea'),'display','block');
                      if(dojo.byId('creditCardPopup').style.display == 'block'){
                        dijit.byId('creditCardPopup').layout();
                      }
                    }
                  </script>
                  <div>
                    <c:choose>
                     <c:when test="${addressFound}">
                      <dsp:input type="radio" checked="false" id="${formId}_newAddress"
                        iclass="atg_inlineLabel" priority="999"
                        bean="CreditCardFormHandler.createNewAddress" value="true"
                        onclick="atg.commerce.csr.${formId}existingBillingAddressPicker();"/>
                      <span><fmt:message key="newOrderBilling.addEditCreditCard.field.billingAddress.createNewAddress"/></span>
                      </c:when>
                      <c:otherwise>
                        <dsp:input type="hidden" checked="false" id="${formId}_newAddress"
                          iclass="atg_inlineLabel" priority="999"
                          bean="CreditCardFormHandler.createNewAddress" value="true"
                          />
                      </c:otherwise>
                    </c:choose>
                  </div>
                  
                </div>
                  </div>
                  
                  </div>
                  <div id="${formId}_addressArea" class="atg-csc-base-table-row" style="display:none; float: left;height: 228px;">
                  <div  class="atg-base-table-customer-credit-card-address-form " style="position:absolute;">
                      <div class="atg-csc-base-table atg-base-table-customer-address-add-form" style="display: table-row;">
                        <dsp:include src="${addressForm.URL}" otherContext="${addressForm.servletContext}">
                          <dsp:param name="formId" value="${formId}"/>
                          <dsp:param name="addressBean" value="${creditCardAddressBean}"/>
                          <dsp:param name="submitButtonId" value="${submitButtonId}"/>
                          <dsp:param name="isDisableSubmit" value="${formId}DisableSubmit"/>
                           <c:choose>
                           <c:when test="${addressFound}">
                            <dsp:param name="validateIf" value="dojo.byId('${formId}')['${creditCardFormHandler}.createNewAddress'][0].checked == false"/>
                           </c:when>
                           <c:otherwise>
                              <dsp:param name="validateIf" value="true"/>
                           </c:otherwise>
                           </c:choose>
                        </dsp:include>
                      </div>
                  </div>
                  </div>
                  <div class="atg-csc-base-table-row" style="height: 24px;"><div style="height: 10px;width: 98.2%;border: 1px solid #d1d1d1;position: absolute;background-color: rgb(232, 232, 232);margin-top: 5px;"></div></div>
            </c:otherwise>
          </c:choose>
      

      
      <script type="text/javascript">
    	
        var ${formId}DisableSubmit = function () {
          var disable = false;
          if (!dijit.byId("${formId}_creditCardType")._isvalid) disable = true;
          if (dijit.byId("${formId}_creditCardNumber") &&
            !dijit.byId("${formId}_creditCardNumber")._isvalid) disable = true;
          if (dijit.byId("${formId}_maskedCreditCardNumber") &&
            !dijit.byId("${formId}_maskedCreditCardNumber")._isvalid) disable = true;
          if (!dijit.byId("${formId}_expirationMonth")._isvalid) disable = true;
          if (!dijit.byId("${formId}_expirationYear")._isvalid) disable = true;
          if (!dijit.byId("${formId}_cvv")._isvalid) disable = true;
          return disable;
        };
        var ${formId}IsValidCardType = function () {
          if (dijit.byId("${formId}_creditCardNumber")) dijit.byId("${formId}_creditCardNumber").validate(false);
          if (dijit.byId("${formId}_maskedCreditCardNumber")) dijit.byId("${formId}_maskedCreditCardNumber").validate(false);
          ${formId}Validate();
          return this._isvalid;
        };
        var ${formId}IsValidCardNum = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumber(
            dijit.byId("${formId}_creditCardType"),
            dijit.byId("${formId}_creditCardNumber"));
          return this._isvalid;
        };
        var ${formId}isValidSecurityNum = function () {
       		this._isvalid = atg.commerce.csr.order.billing.isValidCVV(
            dijit.byId("${formId}_cvv"),
            dijit.byId("${formId}_creditCardType"));
        	return this._isvalid;
          };
        var ${formId}IsValidMaskedCardNum = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardNumberInEditContext({
            creditCardType : dijit.byId('${formId}_creditCardType'),
            creditCardNumber: dijit.byId('${formId}_maskedCreditCardNumber'),
            originalMaskedCreditCardNumber: '${maskedCreditCardNumberVar}'
          });
          return this._isvalid;
        };
        var ${formId}IsValidMonthCombo = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardMonth(
            dijit.byId("${formId}_expirationMonth"), dijit.byId("${formId}_expirationYear"));
          dijit.byId("${formId}_expirationMonth").isValid = function () { return this._isvalid; }; // prevent recursion
          dijit.byId("${formId}_expirationYear").validate(false);
          ${formId}Validate();
          dijit.byId("${formId}_expirationMonth").isValid = ${formId}IsValidMonthCombo;
          return this._isvalid;
        };
        var ${formId}IsValidYearCombo = function () {
          this._isvalid = atg.commerce.csr.order.billing.isValidCreditCardYear(
            dijit.byId("${formId}_expirationYear"));
          dijit.byId("${formId}_expirationYear").isValid = function () { return this._isvalid; }; // prevent recursion
          dijit.byId("${formId}_expirationMonth").validate(false);
          ${formId}Validate();
          dijit.byId("${formId}_expirationYear").isValid = ${formId}IsValidYearCombo;
          return this._isvalid;
        };
        var ${formId}HideAddressArea = function () {
        	 $("#existingAddress").val(true);
          dojo.style(dojo.byId('${formId}_addressArea'),'display','none'); 
          var existingAddressList = dojo.byId('${formId}_existingAddressList');
          if(existingAddressList){
            existingAddressList.disabled = false;
          }
        };
        _container_.onLoadDeferred.addCallback(function () {
          if (dijit.byId("${formId}_creditCardType")) {
            dijit.byId("${formId}_creditCardType").isValid = ${formId}IsValidCardType;
          }
          if (dijit.byId("${formId}_maskedCreditCardNumber")) {
            dijit.byId("${formId}_maskedCreditCardNumber").isValid = ${formId}IsValidMaskedCardNum;
          }
          if (dijit.byId("${formId}_creditCardNumber")) {
            dijit.byId("${formId}_creditCardNumber").isValid = ${formId}IsValidCardNum;
          }
          if (dijit.byId("${formId}_expirationMonth")) {
            dijit.byId("${formId}_expirationMonth").isValid = ${formId}IsValidMonthCombo;
          }
          if (dijit.byId("${formId}_expirationYear")) {
            dijit.byId("${formId}_expirationYear").isValid = ${formId}IsValidYearCombo;
          }
          if (dijit.byId("${formId}_cvv")) {
              dijit.byId("${formId}_cvv").isValid = ${formId}isValidSecurityNum;
          }
        });
        function setCvvLength()
        {
        	$("#error").remove();
        	document.getElementById("${formId}_cvv").value = "";
        	var creditCardType = document.getElementById("${formId}_creditCardType").value;
      	  	if(creditCardType == "American Express" || creditCardType == "americanExpress")
      	  		{
      	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","4");
      	  		}
      	  	else
      	  		{
      	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","3");
      	  		}
      	  	if(creditCardType == 'R Us Private Label Credit Card' || creditCardType == "RUSPrivateLabelCard")
      	  		{
      	  			document.getElementById("${formId}_expirationMonth").disabled = true;
      	  			document.getElementById("${formId}_expirationYear").disabled = true;
	      	  		$("#${formId}_expirationMonth,#${formId}_expirationYear").css("background-color","#e9e9e9");
      	  			$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","none");
	      	  		var userAgent = window.navigator.userAgent;
	  	  			var isIeBrowser = userAgent.indexOf("MSIE");
	  	  			if(isIeBrowser > 0)
	  	  				{
	  	  					$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").hide();
	  	  				}
      	  		}
      	  	else
      	  		{
      	  			document.getElementById("${formId}_expirationMonth").disabled = false;
	  				document.getElementById("${formId}_expirationYear").disabled = false;
	  				$("#${formId}_expirationMonth,#${formId}_expirationYear").css("background-color","#fff");
		  			$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","all");
		  			var userAgent = window.navigator.userAgent;
	  	  			var isIeBrowser = userAgent.indexOf("MSIE");
	  	  			if(isIeBrowser > 0)
	  	  				{
	  	  					$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").show();
	  	  				}
      	  		}
        }
        $(document).ready(function(){
        	setCvvLength();
        });
      </script>
    </dsp:layeredBundle>

  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <%
     Exception ee = (Exception) pageContext.getAttribute("exception");
     ee.printStackTrace();
  %>
</c:if>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.2/src/web-apps/DCS-CSR-UI/include/order/creditCardForm.jsp#1 $$Change: 875535 $--%>
