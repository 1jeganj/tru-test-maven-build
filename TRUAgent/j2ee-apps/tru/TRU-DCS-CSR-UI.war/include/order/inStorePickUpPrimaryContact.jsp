<%@ include file="/include/top.jspf" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />

<dsp:page>
	<dsp:importbean var="shippingGroupFormHandler" bean="atg/commerce/custsvc/order/ShippingGroupFormHandler" />
	
	<dsp:getvalueof var="storePickUpInfo" param="storePickUpInfo" />
	<dsp:getvalueof var="index" param="index" />
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].shippingGroupId" value="${storePickUpInfo.shippingGroupId}" />
	<dsp:input type="hidden" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].shippingGroupType" value="${storePickUpInfo.shippingGroupType}" />
	<div class="primary-pickup-person">
		<div>
			<input type="hidden" class="instorePickupPage" id="pageName" value="pickInStore"/>
			<p>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.firstName" />
				<dsp:input type="text" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].firstName" value="${storePickUpInfo.firstName}" iclass="firstName" id="ispuPrimary_firstName[${index}]">
				</dsp:input>
			</p>
		</div>
		<div>
			<p>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.lastName" />
				<dsp:input type="text" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].lastName" value="${storePickUpInfo.lastName}" iclass="lastName" id="ispuPrimary_lastName[${index}]">
				</dsp:input>
			</p>
		</div>
		<div>
			<p>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.telephone" />
				<dsp:input type="text" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].phoneNumber" value="${storePickUpInfo.phoneNumber}"
								onkeypress="return isPhoneNumberFormat(event)" maxlength="30" iclass="phoneNumber" id="ispuPrimary_phoneNumber[${index}]">
			</dsp:input>
			</p>
		</div>
		
		<div>
			<p>
				<span class="required-asterisk">*</span>
				<fmt:message key="checkout.pickup.email" />
				<c:set var="emailVal" value=""/>
				<c:choose>
					<c:when test="${not empty storePickUpInfo.email}">
						<c:set var="emailVal" value="${storePickUpInfo.email}"/>
					</c:when>
					<c:otherwise>
						<c:if test="${not empty profile.email}">
							<c:set var="emailVal" value="${profile.email}"/>
						</c:if>
					</c:otherwise>
				</c:choose>
				<dsp:input type="text" maxlength="50" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].email" value="${emailVal}" iclass="email" id="ispuPrimary_email[${index}]">
				</dsp:input>
			</p>
		</div>
		<div>
			<p>
				<dsp:getvalueof var="alternateInfoRequired" value="${storePickUpInfo.alternateInfoRequired}" />
				<c:choose>
					<c:when test="${alternateInfoRequired eq true}">
						<dsp:input type="hidden" id="alternateInfoCheckBean" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateInfoRequired" value="${alternateInfoRequired}"/>
						<input type="checkbox" id="${storePickUpInfo.shippingGroupId}"
							class="alternateInfoCheck" value="${alternateInfoRequired}" data-status="" checked="checked" />
					</c:when>
					<c:otherwise>
						<dsp:input type="hidden" id="alternateInfoCheckBean" bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateInfoRequired"/>
						<input type="checkbox" class="alternateInfoCheck" id="${storePickUpInfo.shippingGroupId}" value="${alternateInfoRequired}" data-status=""/>
					</c:otherwise>
				</c:choose>
				
					<fmt:message key="checkout.pickup.alternatePickup" />
					<dsp:input type="hidden" id="${storePickUpInfo.shippingGroupId}-altChk" 
					bean="ShippingGroupFormHandler.storePickUpInfos[${index}].alternateInfoRequired" value="${storePickUpInfo.alternateInfoRequired}"/>
			</p>
		</div>
	</div>
</dsp:page>