<%@ include file="/include/top.jspf" %>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle" />
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="pageName" param="pageName" />
	<dsp:getvalueof var="shippingGroupType" param="shippingGroupType" />
	
	<dsp:droplet name="ForEach">
		<dsp:param name="array"	param="shipmentVO.cartItemDetailVOs" />
		<dsp:param name="elementName" value="cartItemDetailVO" />
		<dsp:oparam name="output">
			<c:choose>
				<c:when test="${pageName eq 'Review'}">
					<div class="checkout-review-product-block gift-wrapped">
						<dsp:include page="/checkout/common/displayItemDetail.jsp">
							<dsp:param name="cartItemDetailVO" param="cartItemDetailVO" />
							<dsp:param name="pageName" param="pageName" />
							<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
						</dsp:include>
					</div>
					<hr class="horizontal-divider">
	        	</c:when>
	        	<c:when test="${pageName eq 'Confirm'}">
					<div class="checkout-review-product-block gift-wrapped">
						<dsp:include page="/checkout/common/displayItemDetail.jsp">
							<dsp:param name="cartItemDetailVO" param="cartItemDetailVO" />
							<dsp:param name="pageName" param="pageName" />
							<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
						</dsp:include>
					</div>
					<hr class="horizontal-divider">
	        	</c:when>
	        	<c:when test="${pageName eq 'store-pickup'}">
	        		<div class="row">
						<div class="col-xs-12">
							<dsp:include page="displayItemDetail.jsp">
								<dsp:param name="cartItemDetailVO" param="cartItemDetailVO" />
								<dsp:param name="pageName" param="pageName" />
								<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
							</dsp:include>
						</div>
					</div>
	        	</c:when>
	        </c:choose>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="ForEach">
		<dsp:param name="array"	param="shipmentVO.cartOOSItemDetailVOs" />
		<dsp:param name="elementName" value="cartOOSItemDetailVO" />
		<dsp:oparam name="output">
			<c:choose>
				<c:when test="${pageName eq 'Review'}">
					<div class="checkout-review-product-block gift-wrapped">
						<dsp:include page="/checkout/common/displayItemDetail.jsp">
							<dsp:param name="cartOOSItemDetailVO" param="cartOOSItemDetailVO" />
							<dsp:param name="pageName" param="pageName" />
							<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
						</dsp:include>
					</div>
					<hr class="horizontal-divider">
		        </c:when>
		        <c:when test="${pageName eq 'Confirm'}">
					<div class="checkout-review-product-block gift-wrapped">
						<dsp:include page="/checkout/common/displayItemDetail.jsp">
							<dsp:param name="cartOOSItemDetailVO" param="cartOOSItemDetailVO" />
							<dsp:param name="pageName" param="pageName" />
							<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
						</dsp:include>
					</div>
					<hr class="horizontal-divider">
		        </c:when>
		        <c:when test="${pageName eq 'store-pickup'}">
	        		<div class="row">
						<div class="col-xs-12">
							<dsp:include page="/checkout/common/displayItemDetail.jsp">
								<dsp:param name="cartOOSItemDetailVO" param="cartOOSItemDetailVO" />
								<dsp:param name="pageName" param="pageName" />
								<dsp:param name="shippingGroupType" value="${shippingGroupType}" />
							</dsp:include>
						</div>
					</div>
	        	</c:when>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>