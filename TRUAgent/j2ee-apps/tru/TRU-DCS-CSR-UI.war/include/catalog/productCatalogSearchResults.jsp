<%--
 This page encodes the search results as JSON
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productCatalogSearchResults.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" var="CSRConfigurator"/>
<dsp:importbean bean="/atg/commerce/custsvc/catalog/CustomCatalogProductSearch" var="customCatalogProductSearch"/>
<dsp:importbean bean="/atg/commerce/custsvc/catalog/ProductSearch" var="productSearch"/>
<dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
<dsp:importbean bean="/atg/dynamo/servlet/RequestLocale"/>
<dsp:importbean bean="/atg/commerce/multisite/SiteIdForCatalogItem"/>
<dsp:importbean bean="/atg/dynamo/droplet/multisite/GetSiteDroplet"/>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="agentTools"/>
<dsp:importbean bean="/atg/dynamo/droplet/multisite/SharingSitesDroplet"/>
<dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
<dsp:importbean var="site1" bean="/atg/multisite/Site" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUCSRPriceDroplet" />
<dsp:importbean bean="/com/tru/commerce/droplet/TRUSkuPromotionDetailsDroplet"/>
<dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRSuggestedMfrAgeDroplet"/>
 <fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" />
 <dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRValidatePreSellableDroplet"/>
<dsp:importbean bean="/atg/multisite/Site"/> 
<dsp:getvalueof var="currentSiteId" bean="Site.id"/>

<dsp:getvalueof param="orderIsModifiable" var="orderIsModifiable"/>
<dsp:getvalueof param="isSearching" var="isSearching"/>

<c:choose>
  <c:when test="${orderIsModifiable}">
    <c:set var="orderIsModifiableDisableAttribute" value=""/>
  </c:when>
  <c:otherwise>
    <c:set var="orderIsModifiableDisableAttribute" value=" disabled='disabled' "/>
  </c:otherwise>
</c:choose>
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
  <c:set var="useCustomCatalogs" value="${CSRConfigurator.customCatalogs}"/>

  <csr:getCurrencyCode>
   <c:set var="currencyCode" value="${currencyCode}" scope="request" />
  </csr:getCurrencyCode> 

  <c:set var="productSearchBean" value="ProductSearch"/>
  <c:if test="${useCustomCatalogs}">
    <c:set var="productSearch" value="${customCatalogProductSearch}"/>
    <c:set var="productSearchBean" value="CustomCatalogProductSearch"/>
  </c:if>
  <dsp:getvalueof bean="${productSearchBean}.isCatalogBrowsing" var="isCatalogBrowsing"/>
  <c:choose>
    <c:when test="${isCatalogBrowsing}">
      <dsp:droplet name="SharingSitesDroplet">
        <dsp:oparam name="output">
          <dsp:getvalueof var="sites" param="sites"/>
        </dsp:oparam>
      </dsp:droplet>

      <dsp:droplet name="CategoryLookup">
        <dsp:param bean="RequestLocale.locale" name="repositoryKey"/>
        <dsp:param name="id" value="${productSearch.hierarchicalCategoryId}"/>
        <dsp:param name="sites" value="${sites}"/>
        <dsp:param name="elementName" value="category"/>
        <dsp:oparam name="output">
          <dsp:tomap param="category" var="category"/>
          <c:if test="${!empty category.childProducts}">
            <c:set var="searchResults" value="${category.childProducts}"/>
          </c:if>
        </dsp:oparam>
      </dsp:droplet>
      <c:set var="resultSetSize" value="${fn:length(searchResults)}"/>
    </c:when>
    <c:otherwise>
      <dsp:getvalueof bean="${productSearchBean}.resultSetSize" var="resultSetSize"/>
      <dsp:getvalueof bean="${productSearchBean}.searchResults" var="searchResults"/>
    </c:otherwise>
  </c:choose>
  <json:object prettyPrint="${UIConfig.prettyPrintResponses}" escapeXml="false">
    <c:choose>
      <c:when test="${!empty searchResults}">
        <json:property name="resultLength" value="${resultSetSize}"/>
        <json:array name="results" items="${searchResults}" var="element">
          <dsp:tomap value="${element}" var="product"/>
          <dsp:tomap value="${product.smallImage}" var="smallImage"/>
          <dsp:tomap value="${product.childSKUs}" var="childSKUs"/>
          <c:if test="${empty childSKUs.baseList[1]}">
            <dsp:tomap value="${childSKUs.baseList[0]}" var="singleSKU"/>
           
          </c:if>
          <json:object>
            <c:choose>
              <c:when test="${isSearching}">
                <json:property name="siteIcon">
                  <dsp:droplet name="SiteIdForCatalogItem">
                    <dsp:param name="item" value="${element}"/>
                    <dsp:getvalueof bean="${productSearchBean}.catalogIdentifierSiteIds" var="productSearchBeanSites"/>
                    <dsp:param name="siteId" value="${(!empty productSearchBeanSites) ? productSearchBeanSites[0] : null}"/>
                    <dsp:param name="currentSiteFirst" value="true"/>
                    <dsp:oparam name="output">
                    <dsp:getvalueof param="siteId" var="siteId"/>
                    <csr:siteIcon siteId="${siteId}" />
                    </dsp:oparam>
                  </dsp:droplet>
                </json:property>
              </c:when>
              <c:otherwise>
                <c:set var="siteId" value="" />
              </c:otherwise>
            </c:choose>
            <c:choose>
              <c:when test="${!empty smallImage.url}">
                <json:property name="image">
                  <a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(product.id)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}');return false;" href="#"><img src="${fn:escapeXml(smallImage.url)}" alt="${fn:escapeXml(product.displayName)}" border="0"  height="60"/></a>
                </json:property>
              </c:when>
              <c:otherwise>
                <c:url context='/agent' value='/images/icon_confirmationLarge.gif' var="defaultImageURL"/>
                <json:property name="image">
                  <a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(product.id)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}');return false;" href="#"><img src="${defaultImageURL}" alt="${fn:escapeXml(product.displayName)}" border="0"  height="60"/></a>
                </json:property>
              </c:otherwise>
            </c:choose>
              
            <json:property name="productId" value="${fn:escapeXml(product.id)}"/>

            <c:choose>
              <c:when test="${empty childSKUs.baseList[1]}">
                <json:property name="sku" value="${fn:escapeXml(singleSKU.id)}"/>
              </c:when>
              <c:otherwise>
                <json:property name="sku" value="&nbsp;"/>
              </c:otherwise>
            </c:choose>
                  <c:if test="${not empty product}">
             <dsp:tomap var="productMap" value="${product}"/>
              <dsp:tomap var="image" value="${productMap.smallImage}"/>
              	<dsp:droplet name="IsEmpty">
				<dsp:param name="value" value="${productMap.childSKUs[0].primaryImage}" />
				<dsp:oparam name="false">
						<json:property name="image">
						<dsp:getvalueof var="imageValue" value="${productMap.childSKUs[0].primaryImage}?fit=inside|80:80"/>
             <img src="${imageValue}" id="atg_commerce_csr_catalog_product_info_image"/>
            </json:property>
	            	
				</dsp:oparam>
				<dsp:oparam name="true">
					<json:property name="image">
             <img  src="/TRU-DCS-CSR/images/no-image500.gif" id="atg_commerce_csr_catalog_product_info_image" border="0" height="60" width="115px"/>
            </json:property>
					
				</dsp:oparam>
			</dsp:droplet>
            </c:if>
            <json:property name="name">
              <a onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(product.id)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}');return false;" href="#">${fn:escapeXml(product.displayName)}</a>
               <c:if test="${not empty childSKUs.baseList[0].promotionalStickerDisplay}">
              	<span id="promotionalStickercontent" style="color: #fff;font-weight: bold;background-color: #004ebc">${childSKUs.baseList[0].promotionalStickerDisplay}</span>
              </c:if>
              <dsp:droplet name="TRUCSRValidatePreSellableDroplet">
							<dsp:param name="sku" value="${childSKUs.baseList[0]}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="preSellable" param="preSellable" />
								<dsp:getvalueof var="formattedDate" param="formattedDate" />
								</dsp:oparam>
			</dsp:droplet>
			 <c:if test="${preSellable}">
				</br> <fmt:message key="pre.order" bundle="${TRUCustomResources}" />
			 </c:if>
            </json:property>
                       
            <c:choose>
              <c:when test="${1 == fn:length(product.childSKUs) || 0 == fn:length(product.childSKUs) }">
                  <dsp:droplet name="TRUCSRPriceDroplet">
		<dsp:param name="skuId" value="${singleSKU.id}"/>
		<dsp:param name="productId" value="${product.id}" />
		<dsp:param name="site" value="${site1}" />
		<dsp:oparam name="true">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		<json:property name="priceRange">
		<span class="atg_commerce_csr_common_content_strikethrough"><web-ui:formatNumber value="${listPrice}" type="currency" currencyCode="${currencyCode}"/></span>
		<web-ui:formatNumber value="${salePrice}" type="currency" currencyCode="${currencyCode}"/>
		</json:property>
		</dsp:oparam>
		<dsp:oparam name="false">
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
					<json:property name="priceRange">
					<web-ui:formatNumber value="${listPrice}" type="currency" currencyCode="${currencyCode}"/></json:property>
											</c:when>
					<c:otherwise>
					<json:property name="priceRange">
					<web-ui:formatNumber value="${salePrice}" type="currency" currencyCode="${currencyCode}"/>
					</json:property>
					</c:otherwise>
				</c:choose>
		</dsp:oparam>
		<dsp:oparam name="empty">
		<json:property name="priceRange">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		 <fmt:message key="catalogBrowse.searchResults.noPrice"/>
		</json:property>
		
		</dsp:oparam>
		 
	</dsp:droplet>
              </c:when>
              <c:otherwise>
                <csr:priceRange lowPrice="lowestPrice" highPrice="highestPrice" productId='${product.repositoryId}'/>
                <c:choose>
                  <c:when test="${lowestPrice eq highestPrice and empty lowestPrice}">
                    <json:property name="priceRange">
                      <fmt:message key="catalogBrowse.searchResults.noPrice"/>
                    </json:property>
                  </c:when>
                  <c:when test="${lowestPrice eq highestPrice and !empty lowestPrice}">
                    <json:property name="priceRange">
                      <web-ui:formatNumber value="${lowestPrice}" type="currency" currencyCode="${currencyCode}"/>
                    </json:property>
                  </c:when>
                  <c:otherwise>
                    <json:property name="priceRange">
                      <web-ui:formatNumber value="${lowestPrice}" type="currency" currencyCode="${currencyCode}"/>${empty rangeSeparator ? "-" : rangeSeparator}<web-ui:formatNumber value="${highestPrice}" type="currency" currencyCode="${currencyCode}"/>
                    </json:property>
                  </c:otherwise>
                </c:choose>
              </c:otherwise>
            </c:choose> 
               <dsp:droplet name="/com/tru/utils/TRUPdpURLDroplet">
					<dsp:param name="productId" value="${singleSKU.onlinePID}" />
					<dsp:param name="siteId" bean="/atg/multisite/Site.id" />
					<dsp:oparam name="output">
						<dsp:getvalueof var="productPageUrl"
							param="productPageUrl" />
					</dsp:oparam>
				</dsp:droplet>
            <c:choose>
         
              <c:when test="${empty childSKUs.baseList[1]}">
                <fmt:message key="catalogBrowse.searchResults.productAddedToOrder.js" var="addToOrderMsg"><fmt:param value="${fn:escapeXml(product.displayName)}"/></fmt:message>
                <c:set var="addToOrderMsg_search" value="&#039;"/>
                <c:set var="addToOrderMsg_replace" value="&#092;&#039;"/>
                <c:set var="addToOrderMsg" value="${fn:replace(addToOrderMsg,addToOrderMsg_search,addToOrderMsg_replace)}"/>
                <c:choose>
                 <c:when test="${singleSKU.unCartable || !singleSKU.webDisplayFlag || singleSKU.deleted || singleSKU.type=='nonMerchSKU' || singleSKU.supressInSearch || !singleSKU.superDisplayFlag || salePrice==0.0}">
                 <json:property name="qty">
                 	Product not available for Purchase
                 </json:property>
                 <json:property name="actions">
                 </json:property>
                </c:when>
                <c:otherwise>
                <json:property name="qty">
                	  <input type="hidden" id="itemProductId_${singleSKU.id}" value="${singleSKU.originalParentProduct}">
					  <input type="hidden" id="catalogRefId_${singleSKU.id}" value="${singleSKU.id}">
					  <input type="hidden" id="currentSiteId_${singleSKU.id}" value="${currentSiteId}">
					  <input type="hidden" id="productPageUrl_${singleSKU.id}" value="${productPageUrl}"> 
					  <input type="hidden" id="skuDisplayName_${singleSKU.id}" value="${singleSKU.displayName}">
					  <input type="hidden" id="description_${singleSKU.id}" value="${singleSKU.description}">
					  <input type="hidden" id="onlinePID_${singleSKU.id}" value="${singleSKU.onlinePID}">
					  <c:choose>
					  <c:when test="${not empty singleSKU.nmwa && (singleSKU.nmwa eq 'Y')}">
			         		<c:set var="nmwaEligible" value="true"/>
			         	</c:when>
			         	<c:otherwise>
			         		<c:set var="nmwaEligible" value="false"/>
			         	</c:otherwise>
		         	</c:choose>
		         	<c:set var="onlineEligible" value="false"/>
		         	<c:if test="${empty singleSKU.channelAvailability or singleSKU.channelAvailability ne 'In Store Only'}">
		         	<c:set var="onlineEligible" value="true"/>
		         	</c:if>
		         	<input type="hidden" id="onlineEligible_${singleSKU.id}" value="${onlineEligible}"> 
		         	<input type="hidden" id="nmwaEligible_${singleSKU.id}" value="${nmwaEligible}"> 
                  	<input maxlength="3" size="5" type="text" onkeypress="return isNumberOnly(event);" onkeyup="if (event.keyCode==13){dojo.byId('itemQuantity${status.count}${fn:escapeXml(product.id)}_button').click();}" id="itemQuantity${status.count}${fn:escapeXml(product.id)}" <c:out value="${orderIsModifiableDisableAttribute}"/>class="inputQuantity"/>
                  	<c:if test="${not empty singleSKU.customerPurchaseLimit}">
         				limit ${singleSKU.customerPurchaseLimit} items per customer
         			 </c:if></br>
                </json:property>
                <fmt:message key='catalogBrowse.searchResults.buy' var="buyButtonLabel"/>
                <json:property name="actions">
                  <input id="itemQuantity${status.count}${fn:escapeXml(product.id)}_button" type="button" name="submit" value="${fn:escapeXml(buyButtonLabel)}" onclick="addToCartSearchResults('${status.count}${fn:escapeXml(product.id)}', '${fn:escapeXml(singleSKU.id)}', '${fn:escapeXml(product.id)}', '${addToOrderMsg}', '${fn:escapeXml(siteId)}','${singleSKU.customerPurchaseLimit}');" <c:out value="${orderIsModifiableDisableAttribute}"/>/>
                </json:property>
                </c:otherwise>
                </c:choose>
                </c:when>
              <c:otherwise>
                <json:property name="qty" value="&nbsp;"/>
                <fmt:message key='catalogBrowse.searchResults.view' var="viewButtonLabel"/>
                <json:property name="actions">
                  <input type="button" name="submit" value="${fn:escapeXml(viewButtonLabel)}" onclick="atg.commerce.csr.catalog.showProductViewInSiteContext('${fn:escapeXml(product.id)}', '${fn:escapeXml(siteId)}', '${fn:escapeXml(currentSiteId)}')"/>
                  <fmt:message key="search.results.view.button" bundle="${TRUCustomResources}"/>
                 </json:property>
              </c:otherwise>
              
            </c:choose>
            <dsp:droplet name="TRUSkuPromotionDetailsDroplet">
			<dsp:param name="sku" value="${childSKUs.baseList[0].id}"/>
			<dsp:oparam name="output">
				<dsp:param name="sku" value="${skuPromoDetails}" />
					<dsp:param name="promoId" value="${pramoID}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="promotionInfos" param="promotionInfo"/>
					<c:forEach items="${promotionInfos}" var="promotionInfo">
						<c:if test="${promotionInfo.skuId eq childSKUs.baseList[0].id}"> 
							<c:if test="${promotionInfo.promotions.size() > 0 }">
								<dsp:getvalueof var="promotionToDisplay" value="${promotionInfo.promotions[0].details}"/>
							</c:if>
						</c:if>
					</c:forEach> 
					
				</dsp:oparam> 
			</dsp:oparam>
		 </dsp:droplet>
		 <json:property name="highestRankPromotion">
			 <c:choose>
                  <c:when test="${not empty promotionToDisplay}"> 
						${promotionToDisplay}&nbsp;. 
				  </c:when>
				  <c:otherwise></c:otherwise> 
			 </c:choose>
		</json:property>
		<dsp:droplet name="TRUCSRSuggestedMfrAgeDroplet">
			<dsp:param name="skuId" value="${childSKUs.baseList[0].id}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="mfrSuggestedAge" param="mfrSuggestedAge"/>
				</dsp:oparam>
		</dsp:droplet>
		 <json:property name="mfrSuggestedAge">
			 <c:choose>
                  <c:when test="${not empty mfrSuggestedAge}"> 
						Age: ${mfrSuggestedAge} 
				  </c:when>
				  <c:otherwise></c:otherwise> 
			 </c:choose>
		</json:property>
		<json:property name="spoil">
			<dsp:droplet name="Switch">
				<dsp:param name="value"
					value="${childSKUs.baseList[0].shipInOrigContainer}" />
				<dsp:oparam name="true">
						<div class="shopping-cart-surprise-image inline"></div>
						<fmt:message key="shoppingcart.spoil.surprise"
							bundle="${TRUCustomResources}" />
						<span>&#xb7;</span><a href="#" onclick="showSpoilDetails();"
							class="learn-more-giftWrap"><fmt:message
								key="shoppingcart.learn.more" bundle="${TRUCustomResources}" /></a><br>
					
				</dsp:oparam>
			</dsp:droplet>
			<c:set var="ispuEligible" value="false"/>
          	<c:set var="s2s" value="false"/>
          	<c:set var="ispu" value="false"/>
			<c:if test="${not empty childSKUs.baseList[0].shipToStoreEligible && childSKUs.baseList[0].shipToStoreEligible eq true}">
				<c:set var="s2s" value="true" />
			</c:if>
			<c:if test="${not empty childSKUs.baseList[0].itemInStorePickUp && (childSKUs.baseList[0].itemInStorePickUp eq 'Y' || childSKUs.baseList[0].itemInStorePickUp eq 'YES' || childSKUs.baseList[0].itemInStorePickUp eq 'Yes' || childSKUs.baseList[0].itemInStorePickUp eq 'yes')}">
				<dsp:getvalueof var="ispu" value="true" />
			</c:if>
			<c:if test="${ispu eq 'true' || s2s eq 'true'}">
			<c:set var="ispuEligible" value="true"/>
			</c:if>
			<c:set var="onlineEligible" value="false"/>
		    <c:if test="${empty childSKUs.baseList[0].channelAvailability or childSKUs.baseList[0].channelAvailability ne 'In Store Only'}">
		    	<c:set var="onlineEligible" value="true"/>
		    </c:if>
			
			<c:if test="${ispuEligible && !onlineEligible}">
				Available in store only
			</c:if>
		</json:property>
	</json:object>
        </json:array>
      </c:when>
      <c:otherwise>
        <json:property name="resultLength" value="${0}"/>
      </c:otherwise>
    </c:choose>
  </json:object>
</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productCatalogSearchResults.jsp#1 $$Change: 875535 $--%>