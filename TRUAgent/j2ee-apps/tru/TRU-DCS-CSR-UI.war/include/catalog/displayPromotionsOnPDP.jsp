 <%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/csr/droplet/TRUCSRItemLevelPromotionDroplet"/>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
 <dsp:droplet name="TRUCSRItemLevelPromotionDroplet">
				<dsp:param name="skuId" param="skuId"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="promotions" param="promotions"/>
				</dsp:oparam>
				</dsp:droplet>
				<dsp:droplet name="ForEach">
					<dsp:param name="array" value="${promotions}" />
					<dsp:param name="elementName" value="promotion" />
					<dsp:oparam name="output">
					<dsp:getvalueof var="promotionId" param="promotion.id" />
					<dsp:getvalueof var="promotionDetails" param="promotion.promotionDetails" />
						<li class="small-grey">
							<div class="inline blue-bullet"></div> 
							<dsp:getvalueof var="desc" param="promotion.description" />
								<c:choose>
									<c:when test="${not empty desc}">
										<dsp:valueof param="promotion.description" />&nbsp;.
									</c:when>
									<c:otherwise>
										<dsp:valueof param="promotion.displayName" />&nbsp;.
									</c:otherwise>
								</c:choose>
								<c:if test="${not empty promotionDetails}">
									<div class="small-blue inline">
										<c:choose>
											<c:when test="${fn:startsWith(promotionDetails, 'http')}">
												<a href="${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details"/></a>
											</c:when>
											<c:when test="${fn:startsWith(promotionDetails, 'www' )}">
												<a href="http://${promotionDetails}" target="_blank"><fmt:message key="tru.productdetail.label.details"/></a>
											</c:when>
											<c:otherwise>
												<input type="hidden" id="promotionDetails_${promotionId}" value="${fn:escapeXml(promotionDetails)}"/>
												<a href="#" onclick="showPromotionDetails('${promotionId}');"><fmt:message key="tru.productdetail.label.details"/></a>
											</c:otherwise>
										</c:choose>	
									</div>
							</c:if>
						</li>
					</dsp:oparam>
				</dsp:droplet> 
</dsp:page>