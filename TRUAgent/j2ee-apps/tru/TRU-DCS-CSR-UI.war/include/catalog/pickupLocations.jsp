<%--
 In-Store Pickup Locations
 This page displays the proximity search inputs
@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/pickupLocations.jsp#1 $$Change: 875535 $
 @updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
<dsp:importbean var="customerPanelConfig" bean="/atg/svc/agent/customer/CustomerPanelConfig" />
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
  <dsp:importbean bean="/atg/svc/agent/ui/AgentUIConfiguration" />
<dsp:importbean bean="/atg/commerce/locations/StoreLocatorFormHandler" />
<dsp:importbean var="inStorePickupDistanceList" bean="/atg/commerce/order/purchase/InStorePickupDistanceList" />
<dsp:importbean var="storeLocatorFormHandler" bean="/atg/commerce/locations/StoreLocatorFormHandler" />
<dsp:importbean var="distanceUnits" bean="/atg/commerce/units/DistanceUnits" />
<c:set var="distanceUnitsVar" value="${distanceUnits.baseUnit}" />
<dsp:layeredBundle basename="atg.commerce.units.UnitOfMeasureResources">
  <fmt:message key="${distanceUnitsVar}" var="distanceUnitsVarLabel"/>
</dsp:layeredBundle>

<dsp:getvalueof var="productId" param="productId"/>
<dsp:getvalueof var="skuId" param="skuId"/>
<dsp:getvalueof var="quantity" param="quantity"/>
<dsp:getvalueof var="postaladdress" param="pinCode"/>
<dsp:getvalueof var="state" param="state"/>
<dsp:getvalueof var="city" param="city"/>
<dsp:getvalueof var="country" param="country"/>
<dsp:getvalueof var="allItems" param="allItems"/>

<div style="width:100%;margin-top:10px">
<c:set var="milesToLocateStores" value="${CSRConfigurator.milesToLocateStores}"/>
  <c:choose>
    <c:when test="${!empty quantity}">
      <svc-ui:frameworkPopupUrl var="successURL"
        value="/include/catalog/pickupLocationsResults.jsp?productId=${productId}&skuId=${skuId}&quantity=${quantity}"
        context="${CSRConfigurator.truContextRoot}"
        success="true"
        windowId="${windowId}"/>
      <svc-ui:frameworkPopupUrl var="allItemsSuccessURL"
        value="/include/catalog/pickupLocationsResults.jsp?productId=${productId}&skuId=${skuId}&quantity=${quantity}&allItems=true"
        context="${CSRConfigurator.truContextRoot}"
        success="true"
        windowId="${windowId}"/>
    </c:when>
    <c:otherwise>
      <svc-ui:frameworkPopupUrl var="successURL"
        value="/include/catalog/shippingLocationsSearchResults.jsp?productId=${productId}&skuId=${skuId}&quantity=${quantity}"
        context="${CSRConfigurator.contextRoot}"
        success="true"
        windowId="${windowId}"/>
      <svc-ui:frameworkPopupUrl var="allItemsSuccessURL"
        value="/include/catalog/shippingLocationsSearchResults.jsp?productId=${productId}&skuId=${skuId}&quantity=${quantity}&allItems=${allItems}"
        context="${CSRConfigurator.contextRoot}"
        success="true"
        windowId="${windowId}"/>
    </c:otherwise>
  </c:choose>
 
  <dsp:form method="post" id="inStorePickupSearchForm" name="inStorePickupSearchForm">
    <c:choose>
      <c:when test="${!empty storeLocatorFormHandler.geoLocatorService.provider}">      
        <c:set var="tableStyle" value="" />
      </c:when>
      <c:otherwise>
        <c:choose>
          <c:when test="${storeLocatorFormHandler.distance > 0}">
            <input type="hidden" value="${storeLocatorFormHandler.distance}" name="geoLocatorServiceProviderEmpty" />
          </c:when>
          <c:otherwise>
            <input type="hidden" value="-1" name="geoLocatorServiceProviderEmpty" />
          </c:otherwise>
        </c:choose>
        <c:set var="tableStyle" value="style='display:none'" />
      </c:otherwise>
    </c:choose>
    <dsp:input type="hidden" value="${country}" name="countryCode" id="countryCode" bean="StoreLocatorFormHandler.countryCode" />
    <dsp:input type="hidden" name="state" id="state" bean="StoreLocatorFormHandler.state" />
    <dsp:input type="hidden" name="city" id="city" bean="StoreLocatorFormHandler.city"/>
	<dsp:input type="hidden" name="postalAddress" id="postalAddress" bean="StoreLocatorFormHandler.postalAddress"/>    
    <dsp:input type="hidden" value="${milesToLocateStores}" name="distance" bean="StoreLocatorFormHandler.distance" />
    <dsp:input type="hidden" value="${successURL}" name="successURL" bean="StoreLocatorFormHandler.successURL" />
    <input type="hidden" value="${successURL}" name="successURLHidden" />
    <input type="hidden" value="${allItemsSuccessURL}" name="allItemsSuccessURLHidden" />
    <dsp:input priority="-10" type="hidden" value="Locate Stores" bean="StoreLocatorFormHandler.locateItems" />    
  </dsp:form>
  
  <table border="0" cellpadding="0" cellspacing="3" id="inStorePickupSearchContainer" ${tableStyle}>
    <tr>
      <td colspan="4">
      	<%-- commenting the below lines to populate the default country as United States instead of taking from countryStore --%>
        <%-- <div dojoType="dojo.data.ItemFileReadStore" jsId="countryStore" url="${customerPanelConfig.countryDataUrl}?${stateHolder.windowIdParameterName}=${windowId}"></div>
        <c:choose>
          <c:when test="${storeLocatorFormHandler.geoLocatorService.allowCityAndState}">
            <input id="inStorePickupCountry" dojoType="atg.widget.form.FilteringSelect" validate="return true;" autoComplete="true" searchAttr="name" store="countryStore" name="countryField" style="width:100%" onChange="atg.commerce.csr.catalog.pickupInStoreCountryChange('${customerPanelConfig.stateDataUrl}?${stateHolder.windowIdParameterName}=${windowId}&isOrderSearch=true&countryCode=');" />
          </c:when>
          <c:otherwise>
            <input id="inStorePickupCountry" dojoType="atg.widget.form.FilteringSelect" validate="return true;" autoComplete="true" searchAttr="name" store="countryStore" name="countryField" style="width:100%" />
          </c:otherwise>
        </c:choose> --%>
        <input id="inStorePickupCountry" validate="return true;" autoComplete="true" searchAttr="name" value="United States" name="countryField" style="width:100%" readonly="true"/>
      </td>
    </tr>
    <tr>
      <td>
        Postal Code<fmt:message key="catalogBrowse.inStorePickup.search.postalCode" var="postalCodeLabel"/>
        <input type="text" id="inStorePickupPostalCode" class="gray" onclick="if (this.value == '${postalCodeLabel}') {this.value='';this.className='';}"></input>
      </td>
      <c:if test="${storeLocatorFormHandler.geoLocatorService.allowCityAndState}">
        	<td><fmt:message key="catalogBrowse.inStorePickup.search.or"/> State</td>
        <td>
          <div dojoType="dojo.data.ItemFileReadStore" jsId="stateStore" url="${customerPanelConfig.stateDataUrl}?${stateHolder.windowIdParameterName}=${windowId}&countryCode=${countryCode}&isOrderSearch=true"></div>
          <input id="inStorePickupState" dojoType="atg.widget.form.FilteringSelect" validate="return true;" autoComplete="true" searchAttr="name" store="stateStore" name="stateField" />
        </td>
        <td>
          City <fmt:message key="catalogBrowse.inStorePickup.search.city" var="cityLabel"/>
          <input type="text" id="inStorePickupPostalCity" class="gray" onclick="if (this.value == '${cityLabel}') {this.value='';this.className='';}"></input>
        </td>
      </c:if>
    </tr>
    <tr>
      <td>
        <select id="inStorePickupProximity">
          <c:forEach var="distance" items="${inStorePickupDistanceList.distances}">
             <option value="${distance}">< ${distance}${distanceUnitsVarLabel}</option>
          </c:forEach>
        </select>
      </td>
      <td colspan="3" style="text-align:right">
        <c:choose>
          <c:when test="${!empty quantity}">
            <input id="inStorePickupSearch" type="button" value="<fmt:message key='catalogBrowse.inStorePickup.search.search' />" onclick="atg.commerce.csr.catalog.pickupInStoreSearchStores('${productId}', '${skuId}', ${quantity})" style="text-align:right" />
          </c:when>
          <c:otherwise>
            <input id="inStorePickupSearch" type="button" value="<fmt:message key='catalogBrowse.inStorePickup.search.search' />" onclick="atg.commerce.csr.catalog.pickupInStoreSearchStores()" style="text-align:right" />
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </table>
  <c:choose>
    <c:when test="${!empty storeLocatorFormHandler.geoLocatorService.provider}">
    </c:when>
    <c:otherwise>
      <script defer="defer">
        <c:choose>
          <c:when test="${!empty quantity}">
            atg.commerce.csr.catalog.pickupInStoreSearchStores('${productId}', '${skuId}', ${quantity});
          </c:when>
          <c:otherwise>
            atg.commerce.csr.catalog.pickupInStoreSearchStores();
          </c:otherwise>
        </c:choose>
      </script>
    </c:otherwise>
  </c:choose>
  <div id="storesSearchResults">
  </div>
</div>
<script>

dojo.addOnLoad(function(){
	  var form=dojo.byId("inStorePickupSearchForm");
	  if(document.getElementById("postalAddress").value != ''){
		  var milesToLocateStores = "<c:out value="${milesToLocateStores}"/>";
		  $("#inStorePickupProximity").val(milesToLocateStores);
		  form.countryCode.value = document.getElementById("countryCode").value;
		  form.state.value = document.getElementById("state").value;
		  form.city.value = document.getElementById("city").value;
		  form.postalAddress.value = document.getElementById("postalAddress").value;
		  form.distance.value=milesToLocateStores;
		  dojo.xhrPost({form:form,url:"/TRU-DCS-CSR/include/catalog/pickupLocationsResults.jsp?_windowid="+window.windowId,encoding:"utf-8",preventCache:true,handle:function(_362,_363){
				if(document.getElementById("inStorePickupResults")){
					document.getElementById("inStorePickupResults").innerHTML=_362;
				}
		  },mimetype:"text/html"});
	  }else{
		  return;
	  }
});
 dojo.provide("atg.commerce.csr.cart");
//Modifying OOTB script for Instore Pick up functionality in PDP and Shipping pages
 atg.commerce.csr.catalog.pickupInStoreSearchStores=function(_358,_359,_35a,_35b){
	if(!_358&&!_359&&!_35a){
		var form=dojo.byId("shippingInStorePickupSearchForm");
		var _35d=dojo.byId("shippingInStorePickupSearchContainer");
		if(_35d){
			form.countryCode.value=dojo.byId("shippingInStorePickupCountry").value;
			var _postalCity=dojo.byId("shippingInStorePickupPostalCity");
			
			if(dojo.byId("shippingInStorePickupPostalCode").value != ""){
				form.postalAddress.value=dojo.byId("shippingInStorePickupPostalCode").value
			}else if(dojo.byId("shippingInStorePickupState").value != ""){
				form.postalAddress.value=dojo.byId("shippingInStorePickupState").value
			}else if(_postalCity&&_postalCity.value!=""&&_postalCity.value!="City"){
				form.postalAddress.value=_postalCity.value;
			}
			
			if(form["geoLocatorServiceProviderEmpty"]){
			if(form["geoLocatorServiceProviderEmpty"].value=="-1"){
				form.distance.value=-1;
			}else{
				form.distance.value=form["geoLocatorServiceProviderEmpty"].value;
			}
			}else{
				form.distance.value=dojo.byId("shippingInStorePickupProximity").value;
			}
		}
	}else{
		var form=dojo.byId("inStorePickupSearchForm");
		var _35d=dojo.byId("inStorePickupSearchContainer");
		if(_35d){
			form.countryCode.value=dojo.byId("inStorePickupCountry").value;
			form.postalAddress.value=dojo.byId("inStorePickupPostalCode").value;
			var _postalCity=dojo.byId("inStorePickupPostalCity");
			
			if(dojo.byId("inStorePickupPostalCode").value != ""){
				form.postalAddress.value=dojo.byId("inStorePickupPostalCode").value;
			}else if(dojo.byId("inStorePickupState").value != ""){
				form.postalAddress.value=dojo.byId("inStorePickupState").value;
			}else if( _postalCity && _postalCity.value!="" && _postalCity.value!="City" ){
				form.postalAddress.value=_postalCity.value;
			}
			
			if(form["geoLocatorServiceProviderEmpty"]){
				if(form["geoLocatorServiceProviderEmpty"].value=="-1"){
					form.distance.value=-1;
				}else{
					form.distance.value=form["geoLocatorServiceProviderEmpty"].value;
				}
			}else{
				form.distance.value=dojo.byId("inStorePickupProximity").value;
			}
		}
	}
	if(_35b){
		if(form["allItemsSuccessURLHidden"]){
			form["successURL"].value=form["allItemsSuccessURLHidden"].value;
		}
		}else{
			if(form["successURLHidden"]){
				form["successURL"].value=form["successURLHidden"].value;
			}
		}
		if(!_358&&!_359&&!_35a){
			dojo.xhrPost({form:form,url:atg.commerce.csr.getContextRoot()+"/include/catalog/shippingLocationsSearchResults.jsp?_windowid="+window.windowId,encoding:"utf-8",handle:function(_35f,_360){
			if(document.getElementById("storesSearchResults")){
				document.getElementById("storesSearchResults").innerHTML=_35f;
			}
		},mimetype:"text/html"});
		}else{
			dojo.xhrPost({form:form,url:"/TRU-DCS-CSR/include/catalog/pickupLocationsResults.jsp?_windowid="+window.windowId,queryParams:{"productId":_358,"skuId":_359,"quantity":_35a,"allItems":_35b},encoding:"utf-8",preventCache:true,handle:function(_361,_362){
			if(document.getElementById("inStorePickupResults")){
				document.getElementById("inStorePickupResults").innerHTML=_361;
			}
		},mimetype:"text/html"});
	}
};

</script>
</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/pickupLocations.jsp#1 $$Change: 875535 $--%>