

 <%@ include file="/include/top.jspf" %>
<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
<dsp:importbean bean="/com/tru/commerce/csr/catalog/SKUDetailDroplet" />
<dsp:getvalueof param="productInfo" var="productInfo" />
<dsp:getvalueof param="productInfo.defaultSKU.vendorsProductDemoUrl" var="vendorsProductDemoUrl" />		
<dsp:importbean bean="/atg/multisite/Site"/>
<dsp:getvalueof bean="Site.virtucommURL" var="virtucommURL"></dsp:getvalueof>
<dsp:importbean bean="/com/tru/common/TRUConfiguration"/>
<dsp:getvalueof param="productInfo.defaultSKU.displayName" var="displayName" /> 
<fmt:setBundle basename="com.tru.svc.agent.ui.TRUCustomResources" var="TRUCustomResources" /> 
<dsp:getvalueof param="item" var="item"/>
<div class="pdp-product-details-block">
        <div class="product-details">
        <dsp:droplet name="SKUDetailDroplet">
        	<dsp:param name="skuId" param="skuId"/>
        	<dsp:oparam name="output">
        		<dsp:getvalueof var="skuVo" param="skuVo"/>
        	</dsp:oparam>
        </dsp:droplet>
            <div>
            	
	                <dsp:include page="textZoneMFGSection.jsp">
	               		<dsp:param name="skuVo" value="${skuVo}"/>
	                </dsp:include>
				
                <div class="top-product-details-content" id="cms">
                
                    <div>
                    </div>
					 <c:if test="${not empty skuVo.longDescription}" >
                    <h2 class="productdesc-aboutthistoy"><fmt:message key="tru.productdetail.label.productdescription" /></h2>
                    <div class="content-productdesc-about-history">
                    <dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
                    	<dsp:param name="data" value="${skuVo.longDescription}"/>
                    	<dsp:oparam name="output">
                    		<dsp:valueof param="enocdeData" valueishtml="true" />
                       	</dsp:oparam>
                    </dsp:droplet>
                    <dsp:valueof param="skuVo.longDescription" valueishtml="true" />
                    </div>
					 <div class="clearfix"></div>
                    </c:if>
						
					 </div>
					 <c:choose>
						<c:when test="${not empty skuVo.longDescription}">
							<div class="product-details-content">
						</c:when>
						<c:otherwise>
							<div class="product-details-content-new">
						</c:otherwise>
					</c:choose>                
                     <dsp:include page="thingsToKnowSection.jsp">
                     	<dsp:param name="skuVo" value="${skuVo}"/>
                     </dsp:include>
                    <dsp:include page="readMoreContent.jsp">
                     <dsp:param name="manufacturerStyleNumber" param="manufacturerStyleNumber"/>
					 <dsp:param name="rusItemNumber" param="rusItemNumber"/>
                   	 <dsp:param name="skuVo" value="${skuVo}"/>
					</dsp:include>
                </div>
                </div>
                
            </div>
           
            <div class="modal fade" id="productVideoModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog warning-modal">
                    <div class="modal-content sharp-border">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="document.getElementById('ytplayer').stopVideo();">X</button>
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                         <object id="ytplayer" type="application/x-shockwave-flash" data="http://www.youtube.com/v/wL60hEXAL7s?version=3&amp;enablejsapi=1">
                                            <param name="allowScriptAccess" value="always">
                                            <embed id="ytplayer1" src="http://www.youtube.com/v/wL60hEXAL7s?version=3&amp;enablejsapi=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="590" height="390">
                                        </object>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
	 		        
<script type="text/javascript">
	$(".knowMoreDetails").on("click",function(){
		if($("#knowMoreGiftWrap_content").css("display") == 'none')
			{
				$("#knowMoreGiftWrap_content").show();
			}
		else
			{
				$("#knowMoreGiftWrap_content").hide();
			}
	});
</script>
</dsp:page> 