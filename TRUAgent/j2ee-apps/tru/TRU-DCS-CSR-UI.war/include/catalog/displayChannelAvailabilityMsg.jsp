<%@ include file="/include/top.jspf" %>
<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof var="colorSizeVariantsAvailableStatus" param="colorSizeVariantsAvail"/>
<dsp:getvalueof var="channelAvailability" param="channelAvailability"/>
<dsp:getvalueof var="presellable" param="presellable"/>
<c:if test="${colorSizeVariantsAvailableStatus eq 'noColorSizeAvailable' && !presellable}" ><%-- && ((inventoryStatus ne 'outOfStock') || (inventoryStatus eq 'outOfStock' && not empty locationIdFromCookie && not empty storeAvailMsg))}"> --%>
		<div class="available-online">
				<div class="green-list-dot inline"></div>
				<c:choose>
					<c:when test="${channelAvailability eq 'Online Only'}">
	                    <fmt:message key="tru.productdetail.label.availableOnlineOnly" /> <span class="dotClass"></span>
	                    <div class="small-blue inline">
							<%--<button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button>--%>	            
 							<a href="javascript:void(0);" class="learn-more" onclick="showDetailsPopup('channelAvailability','onlineOnly');"><fmt:message key="tru.productdetail.label.learnmore"/></a>
 						</div> 
				<!-- <div class="learn-more-tooltips hide">
					item available for online			         
			    </div>  -->
	                </c:when>
					<c:when test="${channelAvailability eq 'In Store Only' }">
	                	<fmt:message key="tru.productdetail.label.availableStoreOnly" /> <span class="dotClass"></span>
	                	<div class="small-blue inline">
	            	<%-- <button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button> --%>
	            	<a href="javascript:void(0);" class="learn-more" onclick="showDetailsPopup('channelAvailability','storeOnly');"><fmt:message key="tru.productdetail.label.learnmore"/></a>
	            </div> 
				<!-- <div class="learn-more-tooltips hide">
					these items is available for customers to go buy at stores only			       
			    </div>  -->
	                </c:when>
					<c:otherwise>
	                    <fmt:message key="tru.productdetail.label.availableonlineandStore" /><span class="dotClass"></span>
	                    <div class="small-blue inline">
	            	<%-- <button href="javascript:void(0);" class="learn-more learn-more-tooltiplink" data-toggle="popover"><fmt:message key="tru.productdetail.label.learnmore"/></button> --%>
	            	<a href="javascript:void(0);" class="learn-more" onclick="showDetailsPopup('channelAvailability','both');"><fmt:message key="tru.productdetail.label.learnmore"/></a>
	            </div> 
				<!--  <div class="learn-more-tooltips hide">
					item available for buying online and at stores			       
			    </div>  -->
	                </c:otherwise>
				</c:choose>
			 </div>
            </c:if>
</dsp:page>