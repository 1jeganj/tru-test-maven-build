<%@ include file="/include/top.jspf" %>
<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof  param="productInfo" var="productInfo"/>
<dsp:getvalueof  param="skuVo.upcNumbers" var="upcNumbers"/>
<dsp:getvalueof  param="productInfo.defaultSKU" var="defaultSKU"/>
<dsp:getvalueof  param="productInfo.manufacturerStyleNumber" var="manufacturerStyleNumber"/>
<dsp:getvalueof  param="productInfo.ewasteSurchargeSku" var="ewasteSurchargeSku"/>
<dsp:getvalueof  param="skuVo.productDimenssionLength" var="productDimenssionLength"/>
<dsp:getvalueof  param="skuVo.productDimenssionWidth" var="productDimenssionWidth"/>
<dsp:getvalueof  param="skuVo.productDimenssionHeight" var="productDimenssionHeight"/>
<dsp:getvalueof  param="skuVo.productItemWeight" var="productItemWeight"/>
<dsp:getvalueof  param="skuVo.assemblyDimenssionHeight" var="assemblyDimenssionHeight"/>
<dsp:getvalueof  param="skuVo.assemblyDimenssionLength" var="assemblyDimenssionLength"/>
<dsp:getvalueof  param="skuVo.assemblyDimenssionWidth" var="assemblyDimenssionWidth"/>
<dsp:getvalueof  param="skuVo.assembledWeight" var="assembledWeight"/>
<dsp:getvalueof  param="skuVo.assemblyRequired" var="assemblyRequired"/>
<dsp:getvalueof  param="productInfo.ewasteSurchargeState" var="ewasteSurchargeState"/>
<dsp:getvalueof var="manufacturerStyleNumber" param="manufacturerStyleNumber"/>
<dsp:getvalueof var="rusItemNumber" param="rusItemNumber"/>
<h2>additional info</h2>
		<ul class="additional-info">
			<c:if test="${not empty rusItemNumber}">
			<li>“R”Web#: <dsp:valueof value="${rusItemNumber}"></dsp:valueof>
			</li>
			</c:if>
			<li>SKU: <span class="additionalInfoUIDPlaceHolder"><dsp:valueof param="skuId"></dsp:valueof></span>
			</li>
			<%-- <c:if test="${not empty upcNumbers}">--%>			
			<dsp:getvalueof var="upcNumbersLength" value="${fn:length(upcNumbers)}"></dsp:getvalueof>
			<li>UPC/EAN/ISBN:
			<c:forEach items="${upcNumbers}" var="upcNumber" varStatus="loop">
			<dsp:getvalueof value="${loop.index}" var="index"></dsp:getvalueof>
			<c:choose>
			<c:when test="${upcNumbersLength ne  index+1}">
			<dsp:valueof value="${upcNumber}"></dsp:valueof>,
			</c:when>
			<c:otherwise>
			<dsp:valueof value="${upcNumber}"></dsp:valueof>
			</c:otherwise>
			</c:choose>
			</c:forEach>
			</li>
			<%--</c:if>--%>			
			<%--<c:if test="${not empty manufacturerStyleNumber}">--%>	
			<li>Manufacturer #: 
			<dsp:valueof value="${manufacturerStyleNumber}"></dsp:valueof>
			</li>
			<c:if test="${not empty assembledWeight}">
			<li>Shipping Weight: ${assembledWeight}&nbsp;<fmt:message key="tru.productdetail.label.shippingWeightPounds"/></li>
			</c:if>
			<c:if test="${not empty productItemWeight}">
			<li>Product Weight: ${productItemWeight}&nbsp;<fmt:message key="tru.productdetail.label.productWeightPounds"/></li>
			</c:if>
			<c:if test="${not empty productDimenssionLength && not empty productDimenssionWidth && not empty productDimenssionHeight }">
			<li>Product Dimensions (in inches):&nbsp;${productDimenssionLength} x ${productDimenssionWidth} x ${productDimenssionHeight}</li>
			</c:if>
			<c:if test="${not empty assemblyDimenssionLength && not empty assemblyDimenssionWidth && not empty assemblyDimenssionHeight }">
			<li>Assembled Dimensions (in inches):&nbsp;${assemblyDimenssionLength} x ${assemblyDimenssionWidth} x ${assemblyDimenssionHeight}</li>
			</c:if>
			<c:if test="${not empty assemblyRequired && assemblyRequired eq 'Y'}">
			<li>Assembly Required</li>
			</c:if>
			<c:if test="${not empty ewasteSurchargeState  && ewasteSurchargeState eq 'CA'}">
			<li>Ewaste Surcharge SKU:&nbsp;<fmt:message key="tru.productdetail.label.ewastesurchargemessage"/> </li>
			</c:if>
		</ul>
</dsp:page>