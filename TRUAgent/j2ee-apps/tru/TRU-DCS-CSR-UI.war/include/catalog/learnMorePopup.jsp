<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<p>spoiler alert! Important note for gift givers! One or
											more of the items you are adding to your cart may ship in the
											manufacturer's original packaging, which may reveal what's
											inside. To avoid spoiling the surprise, consider shipping
											this item to another location or selecting "Store Pick Up"
											during the checkout process.</p>
</dsp:page>