<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:getvalueof  param="esrbrating" var="esrbrating"/>
<dsp:getvalueof  param="videoGameEsrb" var="videoGameSKU"/>
<dsp:getvalueof  param="productInfo.productId" var="productId"/>
<dsp:getvalueof value="false" var="matureValue"></dsp:getvalueof>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<c:if test="${not empty videoGameSKU}">
<style type="text/css">
	.esrbImage {
    width: 33px;
    height: 45px;
    }
    .inline {
    display: inline-block;
    }
</style>
	<div class="logo">
	
		<c:choose>
			<c:when test="${videoGameSKU eq 'Everyone'}">
				<img src="/TRU-DCS-CSR/images/esrb/E.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Early Childhood'}">
				<img src="/TRU-DCS-CSR/images/esrb/eC.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Everyone 10+'}">
				<img src="/TRU-DCS-CSR/images/esrb/E10Plus.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Mature'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="/TRU-DCS-CSR/images/esrb/M.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Teen'}">
				<img src="/TRU-DCS-CSR/images/esrb/T.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Rating Pending'}">
				<img src="/TRU-DCS-CSR/images/esrb/RP.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Adults Only'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="/TRU-DCS-CSR/images/esrb/Ao.png" class="esrbImage">
			</c:when>
		</c:choose>

		<div class="limit-1 inline">
			<span class="rating"><fmt:message
					key="tru.productdetail.label.esrbRating" /></span> <br>
				<c:if test="${not empty esrbrating}">
					<fmt:message key="${esrbrating}" />
				</c:if>
		</div>
	</div>
	</c:if>
	 <input id="isAvailableOnlyForMature-${productId}" class="maturevalue" type="hidden" value="${matureValue}">  
 
</dsp:page>