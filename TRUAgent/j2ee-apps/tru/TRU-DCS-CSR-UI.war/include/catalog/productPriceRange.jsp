<%--
 A page fragment that displays the product price range or, when there's only
 one sku, the price of the sku. 

 @param productToPrice - The product item
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productPriceRange.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<dsp:getvalueof var="rangeProduct" param="product"/>
<dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="csrAgentTools"/>
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <dsp:tomap var="rangeProductMap" value="${rangeProduct}"/>
    <csr:priceRange lowPrice="lowestPrice" highPrice="highestPrice" productId="${rangeProductMap.id}"/>

    <csr:getCurrencyCode>
     <c:set var="currencyCode" value="${currencyCode}" scope="request" />
    </csr:getCurrencyCode> 
    
    <c:choose>
    <c:when test="${lowestPrice eq highestPrice and empty lowestPrice}">
      <fmt:message key="catalogBrowse.searchResults.noPrice"/>
    </c:when>
    <c:when test="${lowestPrice eq highestPrice and !empty lowestPrice}">
      <web-ui:formatNumber value="${lowestPrice}" type="currency" currencyCode="${currencyCode}"/>
    </c:when>
    <c:otherwise>
      <web-ui:formatNumber value="${lowestPrice}" type="currency" currencyCode="${currencyCode}"/>
      ${empty rangeSeparator ? "-" : rangeSeparator}
      <web-ui:formatNumber value="${highestPrice}" type="currency" currencyCode="${currencyCode}"/>
    </c:otherwise>
  </c:choose>
  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productPriceRange.jsp#1 $$Change: 875535 $--%>
