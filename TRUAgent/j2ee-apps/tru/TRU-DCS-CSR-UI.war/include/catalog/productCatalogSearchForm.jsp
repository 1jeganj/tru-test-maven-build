<%--
 This page shows the product catalog results
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productCatalogSearchForm.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
  <dsp:importbean bean="/atg/commerce/custsvc/order/CartModifierFormHandler" var="cartModifierFormHandler"/>
  <dsp:importbean bean="/atg/commerce/custsvc/catalog/CustomCatalogProductSearch" var="customCatalogProductSearch"/>
  <dsp:importbean bean="/atg/commerce/custsvc/catalog/ProductSearch" var="productSearch"/>
  <dsp:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="shoppingCart"/>
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRConfigurator" />
  <dsp:importbean var="defaultPageFragment" bean="/atg/commerce/custsvc/ui/fragments/catalog/ProductSearchDefault" /> 
  <dsp:importbean var="extendedPageFragment" bean="/atg/commerce/custsvc/ui/fragments/catalog/ProductSearchExtended" />
	<style type="text/css">
		#atg_commerce_csr_catalog_notifyMeEmailPopup {
			width: 850px !important;
			max-width: 850px !important;
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div
			{
			/* width: 23% !important; */
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails ul
			{
			float: right !important;
			width: 75% !important;
		}
		
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div img#atg_commerce_csr_catalog_product_info_image
			{
			width: 80px !important;
			height: 80px !important;
		}
		#atg_commerce_csr_catalog_productCatalogTableContainer .atg_commerce_csr_productCatalogDetails div.shopping-cart-remove{
			width: 250px !important;
		}
				
		.shopping-cart-surprise-image
		    {
		    	    width: 20px;
				    height: 20px;
				    background-image: url(/TRU-DCS-CSR/images/dont-spoil.png);
				    background-repeat: no-repeat;
				    float: left;
				    margin-right: 5px;
				    margin-top:-3px;
		    }
		    div.atg_commerce_csr_productCatalogDetails ul li{
		    	 margin-top:1px;
		    }
		    div.atg_commerce_csr_productCatalogDetails ul li.spoilInfo {
		    	 margin-top: 5px;
		    }
		 
		   
	</style>
  <c:if test="${!empty shoppingCart.originalOrder}">
    <dsp:droplet name="/atg/commerce/custsvc/order/OrderIsModifiable">
      <dsp:param name="order" value="${shoppingCart.originalOrder}"/>
      <dsp:oparam name="true">
        <c:set var="orderIsModifiable" value="true"/>
      </dsp:oparam>
    </dsp:droplet>
  </c:if>
  <dsp:tomap bean="/atg/userprofiling/Profile" var="profile"/>
  
  <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <fmt:message key="catalogBrowse.searchResults.noResults" var="noResultsMessage" />
    <fmt:message key="catalogBrowse.searchResults.productResults" var="productResultsNumberMessage" />
    <fmt:message key="catalogBrowse.searchResults.noSearchPerformed" var="noSearchPerformedMessage" />
    <fmt:message key="text.leftBracket" var="leftBracketMessage" />
    <fmt:message key="text.rightBracket" var="rightBracketMessage"/>

    <c:set var="useCustomCatalogs" value="${CSRConfigurator.customCatalogs}"/>

    <c:set var="productSearchBean" value="ProductSearch"/>
    <c:if test="${useCustomCatalogs}">
      <c:set var="productSearch" value="${customCatalogProductSearch}"/>
      <c:set var="productSearchBean" value="CustomCatalogProductSearch"/>
    </c:if>
    <script type="text/javascript">
      dojo.provide("atg.commerce.csr.catalog.productCatalog");

      atg.commerce.csr.catalog.productCatalogSearchFields = [
        <c:if test="${isMultiSiteEnabled}">
        { name: '<fmt:message key="catalogBrowse.searchResults.site"/>', canSort: false},
        </c:if>
        { name: '<fmt:message key="catalogBrowse.searchResults.productInformation"/>', canSort: true, property: "displayName"},
        { name: '<fmt:message key="catalogBrowse.searchResults.actions"/>', canSort: false}
      ];

      atg.commerce.csr.catalog.productCatalog.searchView =
        { cells: [[
          <c:if test="${isMultiSiteEnabled}">
          { name: '<fmt:message key="catalogBrowse.searchResults.site"/>',width:"10%"},
          </c:if>
          { name: '<fmt:message key="catalogBrowse.searchResults.productInformation"/>',width:"55%"},
          { name: '<fmt:message key="catalogBrowse.searchResults.actions"/>',width:"35%"}
        ]]};


atg.commerce.csr.catalog.productCatalog.searchLayout = [atg.commerce.csr.catalog.productCatalog.searchView];


  atg.commerce.csr.catalog.productCatalogPagedData =
    new atg.data.FormhandlerData(atg.commerce.csr.catalog.productCatalogSearchFields,"${CSRConfigurator.truContextRoot}/include/catalog/productCatalogSearchResults.jsp?orderIsModifiable=<c:out value='${orderIsModifiable}'/>&isSearching=true");
  <dsp:getvalueof var="rowsPerPage" bean="${productSearchBean}.maxResultsPerPage"/>
  atg.commerce.csr.catalog.productCatalogPagedData.rowsPerPage = ${rowsPerPage};
  atg.commerce.csr.catalog.productCatalogPagedData.formId = 'searchByProductForm';
  atg.commerce.csr.catalog.productCatalogPagedData.setCurrentPageNumber = function(inRowIndex) {
    var currentPage = Math.floor(inRowIndex / this.rowsPerPage) + 1;
    var form = dojo.byId(this.formId);
    if (form) {
      if (form[this.formCurrentPageField]){
        form[this.formCurrentPageField].value = currentPage;
      }
    }
  };
  atg.commerce.csr.catalog.productCatalogPagedData.rows = function(inRowIndex, inData) {
	  
    for (var i=0, l=inData.results.length; i<l; i++) {
      var newRow = [
        <c:if test="${isMultiSiteEnabled}">
        inData.results[i].siteIcon,
        </c:if>
        '<div class="atg_commerce_csr_productCatalogDetails"><div>' + inData.results[i].image + '</div><ul><li>' + inData.results[i].name + '</li><li>' + inData.results[i].productId  + '</li><li>'  + inData.results[i].mfrSuggestedAge + '</li><li>'  + inData.results[i].priceRange + '</li><li>'  + inData.results[i].highestRankPromotion + '</li><li class="spoilInfo">'  + inData.results[i].spoil + '</li></ul></div>',
        inData.results[i].qty + inData.results[i].actions
      ];
      this.setRow(newRow, inRowIndex + i);
    }
  };

  atg.commerce.csr.catalog.productCatalog.searchRefreshGrid = function () {
    dojo.byId("searchByProductForm").currentResultPageNum.value = 1;
    atg.commerce.csr.catalog.productCatalogPagedData.count = 0;
    dijit.byId("atg_commerce_csr_catalog_productCatalogTable").rowCount = 0;
    atg.commerce.csr.catalog.productCatalogPagedData.clearData();
    dijit.byId("atg_commerce_csr_catalog_productCatalogTable").updateRowCount(0);
    atg.commerce.csr.catalog.productCatalogPagedData.fetchRowCount(
    {
      callback: function(inRowCount) {
        if (inRowCount.resultLength > 0) {
          dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "display", "");
          //dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "visibility", "visible");
          //dojo.style("atg_commerce_csr_catalog_search_noResults", "display", "none");
          document.getElementById("atg_commerce_csr_catalog_search_noResults").innerHTML = "<c:out value='${productResultsNumberMessage}' /><c:out value='${leftBracketMessage}' />" + inRowCount.resultLength + "<c:out value='${rightBracketMessage}' />";
        } else {
          dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "display", "none");
          //dojo.style("atg_commerce_csr_catalog_search_noResults", "display", "");
          
          //CSC-169542
          if(window.catalogInfo){
        	 window.catalogInfo.isCatalogSearching = false ; 
          }
          document.getElementById("atg_commerce_csr_catalog_search_noResults").innerHTML = "<c:out value='${noResultsMessage}' />";
        }
        atg.commerce.csr.catalog.productCatalogPagedData.count = inRowCount.resultLength;
        dijit.byId("atg_commerce_csr_catalog_productCatalogTable").rowCount = inRowCount.resultLength;
        atg.commerce.csr.catalog.productCatalogPagedData.clearData();
        dijit.byId("atg_commerce_csr_catalog_productCatalogTable").updateRowCount(inRowCount.resultLength);
      }
    });
  };

  atg.commerce.csr.catalog.productCatalogAddPagination = function() {
    dijit.byId("atg_commerce_csr_catalog_productCatalogTable").setStructure(atg.commerce.csr.catalog.productCatalog.searchLayout);
    //dojo.style("atg_commerce_csr_catalog_productCatalogTableContainer", "visibility", "hidden");
  }

  atg.commerce.csr.catalog.productCatalogRemovePagination = function() {
    // clean up after ourselves
    atg.commerce.csr.catalog.productCatalogPagedData = undefined;
  }

	/* $(document).on('keyup','input[id^=itemQuantity]',function(){
		var $this = $(this);
		var stockLimit = parseInt($this.attr('data-stockLimit'));
		if(parseInt($this.val()) >  stockLimit){
			$this.val(stockLimit);
		}
	}); */
  
	function addToCartSearchResults(scProductId, sku, productId, addInfoMessage, siteId, customerPurchaseLimit){
		var quantityInput = $("#itemQuantity"+scProductId).val();
		if($(".searchQtyErrorMsg").length)
			{
				$(".searchQtyErrorMsg").remove();
			}
		if(quantityInput == "" || quantityInput == null)
			{
				$(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">Please enter quantity</span>');
				return false;
			}
		 $.ajax({
			  method: "POST",
			  url: "/TRU-DCS-CSR/include/catalog/inventoryLookupAjaxResponse.jsp",
			  data: {skuId : sku},
			  dataType :'json'
			})
		  .done(function( msg ) {
			  //debugger;
			  $('.searchQtyErrorMsg').remove();
			  var quantity = parseInt($(document).find("#itemQuantity"+scProductId).val());
			  var onlineEligible = $("#onlineEligible_"+sku).val();
			  $(document).find("#itemQuantity"+scProductId).val(quantity);
			  if(typeof msg.response === "undefined" || ( typeof msg.response !== "undefined" &&  msg.response.skuStatus == "outofstock")){
				  if(msg.response.skuStatus == "outofstock")
					  {
					  	var nmwaEligible = $("#nmwaEligible_"+sku).val();
					  	$(document).find("#itemQuantity"+scProductId).val('');
					  	var outOfStockMsg = '<span class="outOfStockMsg" style="margin-right: 5px;">Out Of Stock</span>';
					  	if(nmwaEligible == 'true')
					  		{
					  			var nmwaMsg = '<a href="#" id="'+sku+'" class="out-of-stock-text">email me when available</a>';
					  		}
					  	else
					  		{
					  			var nmwaMsg = '';
					  		}
					  	var displayMsg = '<div style="display:block;margin-top:5px;">'+outOfStockMsg+nmwaMsg+'</div>';
					  	var qtyInputId = "itemQuantity"+scProductId;
					  	var qtyInputBtnId = qtyInputId+"_button";
					  	$("#"+qtyInputId).attr("disabled",true);
					  	$("#"+qtyInputBtnId).attr("disabled",true);
					  	$("#"+qtyInputId).parent("td").append($(displayMsg));
					  }
				  else
					  {
						  $(document).find("#itemQuantity"+scProductId).val(0);
					  }
				  return false;
			  }
			  var response = msg.response;
			  var noQtySelected = true;
        	  var qtyZeroError = false;
        	  var quantityExceedError = false;
        	  var customerPurchaseLimitError = false;
			 if($.trim($("#itemQuantity"+scProductId).val()) == '')
 			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg" style="color:red;">&nbsp;&nbsp;Please select any sku quantity </span>');
 			 }
			  else if(parseInt($.trim($("#itemQuantity"+scProductId).val())) == 0)
 			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">&nbsp;&nbsp;quantity should not be zero</span>');
 			 }
			 else  if(customerPurchaseLimit > 0 && quantity > customerPurchaseLimit)
			 {
				  $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">Customer purchase limit for sku is <b style="color:black;">'+customerPurchaseLimit+'</b></span>');
 			 }
			 else if(response.skuStatus == "instock" && onlineEligible == 'false'){
				 $(document).find("#itemQuantity"+scProductId).closest('td').append('<span class="searchQtyErrorMsg"  style="color:red;">Not available for ship to home</span>');
			 }
			 else if(response.skuStatus=="instock" &&  quantity > response.stockLevel && onlineEligible == 'true'){
				  $(document).find("#itemQuantity"+scProductId).val(response.stockLevel);
				  atg.commerce.csr.catalog.addItemToOrder(scProductId, sku, productId, addInfoMessage, siteId);
			  }else if(response.skuStatus=="instock" && (quantity <= response.stockLevel) && onlineEligible == 'true'){
				  atg.commerce.csr.catalog.addItemToOrder(scProductId, sku, productId, addInfoMessage, siteId);
			  }
		  }).fail(function(){
			  $(document).find("#itemQuantity"+scProductId).val(0);
		  });
	}
	$(document).on("click",".out-of-stock-text",function(){
		var skuId = $(this).attr("id");
		loadNotifyMe(skuId)
	});
	function isNumberOnly(evt){
    	evt = (evt) ? evt : window.event;
    	var charCode = (evt.which) ? evt.which : evt.keyCode;
    	 if (navigator.userAgent.indexOf("Firefox") > 0) {
    	    	if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58) || evt.which == 0) {
    	    		return true;
    	        	}
    	    		return false;
    	    	}
    	    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
    	    	 return true;
    	    }
    	   
    	    return false;
    }
	
	atg.commerce.csr.catalog.onNewProductSearch = function () {
	    var sku = document.getElementById("sku");
	    if (sku) sku.value = "";
	    var productID = document.getElementById("productID");
	    if (productID) productID.value = "";
	    var mfr = document.getElementById("mfr");
	    if (mfr) mfr.value = "";
	    var web = document.getElementById("web");
	    if (web) web.value = "";
	    var displayName = document.getElementById("displayName");
	    if (displayName) displayName.value = "";
	    var itemPrice = document.getElementById("itemPrice");
	    if (itemPrice) itemPrice.value = "";
	    var searchInput = document.getElementById("searchInput");
	    if (searchInput) searchInput.value = "";
	    var priceRelation = document.getElementById("priceRelation");
	    if (priceRelation) priceRelation.selectedIndex = 0;
	    var siteSelect = document.getElementById("siteSelect");
	    if (siteSelect) siteSelect.selectedIndex = 0;
	    var categorySelect = document.getElementById("categorySelect");
	    if (categorySelect) {
	      categorySelect.selectedIndex = 0;
	      categorySelect.disabled = "disabled";
	    }
	    window.catalogInfo = null;
	  }
	if (!dijit.byId("atg_commerce_csr_catalog_notifyMeEmailPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_notifyMeEmailPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
    if (!dijit.byId("atg_commerce_csr_catalog_privacyDetailsPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_privacyDetailsPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
	function loadNotifyMe(skuId)
	{
		var displayName = $("#skuDisplayName_"+skuId).val();
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Notify Me',
            url: "/TRU-DCS-CSR/panels/order/notifyMePopUp.jsp?skuId="+skuId+"&skuDisplayName="+displayName,
            });	
	}
	function showPrivacyDetails()
	{
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_notifyMeEmailPopup',
            title:'Privacy Policy',
            url: "/TRU-DCS-CSR/panels/order/privacyDetails.jsp",
            onClose: function(args) {
            	
            }});	
	}
	function submitNotifyMeDetails(skuId)
	{
		var emailId = dojo.byId("notifyMeMailId").value;
  	  	var itemProductId = $("#itemProductId_"+skuId).val();
  	    var catalogRefId= $("#catalogRefId_"+skuId).val();
  		var currentSiteId = $("#currentSiteId_"+skuId).val();
  		var productUrl=$("#productPageUrl_"+skuId).val();
  		var dsiplayName=$("#skuDisplayName_"+skuId).val();
  		var description=$("#description_"+skuId).val();
  		var onlinePID=$("#onlinePID_"+skuId).val();
  		var sendSpecialofferToMail = $("#notify-checkbox").is(":checked");
  		if(isValidEmail(emailId)){
    		dojo.xhrPost({url:"/TRU-DCS-CSR/panels/order/submitNotifyDetails.jsp?notifyMeMailId="+emailId+"&itemProductId="+itemProductId+"&catalogRefId="+catalogRefId+"&currentSiteId="+currentSiteId+"&productPageUrl="+productUrl+"&sendSpecialofferToMail="+sendSpecialofferToMail+"&skuDisplayName="+dsiplayName+"&description="+description+"&onlinePID="+onlinePID,content:{_windowid:window.windowId},encoding:"utf-8",preventCache:true,handle:function(responce){
    			var msg = $.trim(responce);
    			if(msg.indexOf("Following are the form errors:") > -1)
  				{
  					var msg1 = msg.split(":")[1];
  					$("#successOrErrorMsg").html(msg1).css("color","red").show();
  				}
    			else{
	 				 	dojo.style(dojo.byId('notifyMe_content'), "display", "none");
	 				 	$("#successOrErrorMsg").html(msg).css({'color':"#000",'margin-top':"33px"}).show();
	 				
	    		 	}
  			},mimetype:"text/html"});
      	}
  	  	else{
  	  		$("#successOrErrorMsg").html("Please enter the email Id in the correct format.").css("color","red");
  	  	}
	}
	function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	}
	if (!dijit.byId("atg_commerce_csr_catalog_spoilDetailPopup")) {
	    new dojox.Dialog({ id:"atg_commerce_csr_catalog_spoilDetailPopup",
	                       cacheContent:"false", 
	                       executeScripts:"true",
	                       scriptHasHooks:"true",
	                       duration: 100,
	                       "class":"atg_commerce_csr_popup"});
	}
	function showSpoilDetails()
	{
		atg.commerce.csr.common.showPopupWithReturn({
            popupPaneId: 'atg_commerce_csr_catalog_spoilDetailPopup',
            title:'Dont spoil surprise Details',
            url: "/TRU-DCS-CSR/include/catalog/learnMorePopup.jsp",	
            dataType:"html",
            onClose: function(args) {
            	
            }});
		
	}
  </script>
    <script type="text/javscript">
    _container_.onLoadDeferred.addCallback(function () {
        atg.commerce.csr.catalog.productCatalogAddPagination();
        var form = dojo.byId("searchByProductForm");
        if (form && window.catalogInfo) {
          <c:if test="${CSRConfigurator.useSKUId}">
            if (window.catalogInfo.sku) form.sku.value = window.catalogInfo.sku;
          </c:if>
          <c:if test="${CSRConfigurator.useProductId}">
            if (window.catalogInfo.productID) form.productID.value = window.catalogInfo.productID;
          </c:if>
          if (window.catalogInfo.displayName) form.displayName.value=window.catalogInfo.displayName;
		  if (window.catalogInfo.manufacturerStyleNumber) form.mfr.value = window.catalogInfo.mfr;
          if (window.catalogInfo.rusItemNumber) form.web.value=window.catalogInfo.web;
          if (window.catalogInfo.siteSelect) form.siteSelect.selectedIndex = window.catalogInfo.siteSelect;
          if (window.catalogInfo.priceRelation) form.priceRelation.value = window.catalogInfo.priceRelation;
          if (window.catalogInfo.itemPrice) form.itemPrice.value = window.catalogInfo.itemPrice;
          if (window.catalogInfo.searchInput) form.searchInput.value = window.catalogInfo.searchInput;
          
          if (window.catalogInfo.categorySelect) {
            atg.commerce.csr.catalog.selectSite('<fmt:message key="catalogBrowse.findProducts.allCategories"/>', window.catalogInfo.categorySelect);
          } else {
            atg.commerce.csr.catalog.selectSite('<fmt:message key="catalogBrowse.findProducts.allCategories"/>');
          }
          if (window.catalogInfo.isCatalogSearching == true) {
            atg.commerce.csr.catalog.catalogSearch('');
          }
        }
        dojo.connect(_container_, "resize", dijit.byId("atg_commerce_csr_catalog_productCatalogTable"), "update");
        if(window.catalogInfoOnNewProductSearchConnect) {
        	// remove old connection
        	dojo.disconnect(window.catalogInfoOnNewProductSearchConnect);
        }
        if (dojo.byId("atg_svc_c2cAction")) {
          window.catalogInfoOnNewProductSearchConnect = dojo.connect(dojo.byId("atg_svc_c2cAction"), "onclick", atg.commerce.csr.catalog.onNewProductSearch);
        }else if(dojo.byId("globalContextStartCall")){
          window.catalogInfoOnNewProductSearchConnect = dojo.connect(dojo.byId("globalContextStartCall"), "onclick", atg.commerce.csr.catalog.onNewProductSearch);
        }else if(dojo.byId("globalContextEndAndStartCall")){
          window.catalogInfoOnNewProductSearchConnect = dojo.connect(dojo.byId("globalContextEndAndStartCall"), "onclick", atg.commerce.csr.catalog.onNewProductSearch);
        }
        atg.keyboard.registerFormDefaultEnterKey("searchByProductForm", "searchButton");
    });
    _container_.onUnloadDeferred.addCallback(function () {
        atg.keyboard.unRegisterFormDefaultEnterKey("searchByProductForm");
        atg.commerce.csr.catalog.productCatalogRemovePagination();
        dojo.disconnect(_container_, "resize", dijit.byId("atg_commerce_csr_catalog_productCatalogTable"), "update");
    });

      // autofocus attribute does not function correctly in IE and firefox
      // this polyfil can be removed when both bowsers fix this
      dojo.byId('searchButton').focus()



      
    </script>
    <div class="atg_commerce_csr_findProdForm">
    <dsp:form action="#" id="searchByProductForm" formid="searchByProductForm">
      <dsp:input bean="${productSearchBean}.isCatalogBrowsing" name="isCatalogBrowsing" value="false" type="hidden"/>
      <dsp:input bean="${productSearchBean}.navigationPath" name="path" type="hidden"/>
      <dsp:input bean="${productSearchBean}.sortProperty" name="sortProperty" type="hidden"/>
      <dsp:input bean="${productSearchBean}.sortDirection" name="sortDirection" type="hidden"/>
      <%-- the following input will trigger both setCurrentResultPageNum and handleCurrentResultPageNum --%>
      <dsp:input type="hidden" name="currentResultPageNum" priority="-10" bean="${productSearchBean}.currentResultPageNum" beanvalue="${productSearchBean}.currentResultPageNum" />
      <div class="atg-csc-base-table atg-csc-base-spacing-top">
        <c:if test="${not empty defaultPageFragment.URL}">
          <dsp:include src="${defaultPageFragment.URL}" otherContext="${defaultPageFragment.servletContext}" />
        </c:if>
  
        <c:if test="${not empty extendedPageFragment.URL}">
          <dsp:include src="${extendedPageFragment.URL}" otherContext="${extendedPageFragment.servletContext}" />
        </c:if>
        <div class="atg-csc-base-table-cell atg-base-table-product-catalog-search-label"></div>
        <div class="atg-csc-base-table-cell"></div>
        <div class="atg-csc-base-table-cell atg-base-table-product-catalog-search-label"></div>
        <div class="atg_commerce_csr_formButtons atg-csc-base-table-cell atg-base-table-product-catalog-search-button">
          <input type="button" class="atg_commerce_csr_searchFormButton" id="searchButton" value="<fmt:message key='catalogBrowse.findProducts.search'/>" onclick="atg.commerce.csr.catalog.catalogSearch('<dsp:valueof bean="${productSearchBean}.hierarchicalCategoryId"/>')" autofocus />
          <a href="#" onclick="atg.commerce.csr.catalog.onNewProductSearch();return false;"><fmt:message key="catalogBrowse.findProducts.newSearch"/></a>
        <script>
          // autofocus atttribute does not function correctly in IE and firefox
          // remove this shim when 
          dojo.byId('searchButton').focus()
        </script>
        </div>
      </div>
    </dsp:form>
    </div>
    <div id="atg_commerce_csr_catalog_search_noResults">
        <c:out value="${noSearchPerformedMessage}" />
    </div>
    <div id="atg_commerce_csr_catalog_productCatalogTableContainer" style="height: 300px;display:none;">
      <div id="atg_commerce_csr_catalog_productCatalogTable"
       dojoType="dojox.Grid"
      jbarraud-apps-x model="atg.commerce.csr.catalog.productCatalogPagedData"
       autoHeight="false"
       onMouseOverRow="atg.noop()"
       onRowClick="atg.noop()"
       onCellClick="atg.noop()"
       onCellContextMenu="atg.noop()"
       onkeydown="atg.noop()">
      </div>
    </div>

  </dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/productCatalogSearchForm.jsp#1 $$Change: 875535 $--%>
