<%@ include file="/include/top.jspf" %>
<dsp:page>

 	<div class="read-more-content">
		<dsp:include page="safetyWarningSection.jsp">
			<dsp:param name="skuVo" param="skuVo"/>
		</dsp:include>
		<dsp:include page="featuresSection.jsp">
			<dsp:param name="skuVo" param="skuVo"/>
		</dsp:include>
		<dsp:include page="additionalInfoSection.jsp">
			<dsp:param name="manufacturerStyleNumber" param="manufacturerStyleNumber"/>
			<dsp:param name="rusItemNumber" param="rusItemNumber"/>
			<dsp:param name="skuVO" param="skuVO"/>
		</dsp:include>
		<dsp:include page="howToGetItSection.jsp">
			<dsp:param name="skuVo" param="skuVo"/>
		</dsp:include>
	</div>
	
</dsp:page>