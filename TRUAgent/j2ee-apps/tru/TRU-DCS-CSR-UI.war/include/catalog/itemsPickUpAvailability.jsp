<%--
 In-Store Pickup Locations
 This page displays the proximity search inputs
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/shippingPickupLocations.jsp#1 $$Change: 875535 $
 @updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
<dsp:importbean bean="/com/tru/commerce/csr/droplet/ItemsPickupAvailabilityDroplet"/>
<dsp:getvalueof var="skuId" param="skuId"/>
<dsp:getvalueof var="orderId" param="orderId"/>
<dsp:droplet name="ItemsPickupAvailabilityDroplet">
				<dsp:param name="storeId" param="storeId"/>
				<dsp:param name="orderId" param="orderId"/>
				<dsp:param name="skuId" param="skuId"/>
				<dsp:param name="qty" param="qty"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="storeHoursMsgMap" param="storeHoursMsgMap"/>
					<c:choose>
						<c:when test="${not empty orderId}">
							<c:forEach items="${storeHoursMsgMap}" var="storeHours">
									${storeHours.key} | ${storeHours.value}
								</br>
							</c:forEach>
						</c:when>
						<c:when test="${not empty skuId}">
						
						<c:forEach items="${storeHoursMsgMap}" var="storeHours">
							<c:if test="${storeHours.value eq 'storelocator.readyInAnHour Not Found in Repository'}">
							Ready under an hour
							</c:if>
							</c:forEach>
						</c:when>
					</c:choose>
				</dsp:oparam>
				<dsp:oparam name="empty">
					No Channel Availability Information available
				</dsp:oparam>
				</dsp:droplet>
</dsp:page>