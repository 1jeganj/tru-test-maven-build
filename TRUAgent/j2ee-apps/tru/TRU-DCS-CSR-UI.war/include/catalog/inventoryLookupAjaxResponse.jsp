<%@ taglib uri="http://www.atg.com/taglibs/json" prefix="json" %>
<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/csr/inventory/TRUCSRInventoryLookupDroplet"/>
<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="TRUCSRInventoryLookupDroplet">
		<dsp:param name="skuId" param="skuId" />
		<dsp:param name="locationIdSupplied" value="false"/>
		<dsp:param name="useCache" value="true" />
		<dsp:oparam name="output">
			<dsp:droplet name="Switch">
				<dsp:param name="value"
					param="availability" />
				<dsp:getvalueof param="stockLevel" var="stockLevel"/>
				<c:set var="stockLevel" value="${stockLevel}"/>
				<dsp:oparam name="1001">
					<c:set var="inventoryStatus" value="outofstock" />
				</dsp:oparam>
				<dsp:oparam name="1002">
					<c:set var="inventoryStatus" value="preorder" />
				</dsp:oparam>
				<dsp:oparam name="1003">
					<c:set var="inventoryStatus" value="backorder" />
				</dsp:oparam>
				<dsp:oparam name="1000">
					<c:set var="inventoryStatus" value="instock" />
				</dsp:oparam>
				<dsp:oparam name="unset">
					<json:property name="skuStatus" value="" />
					<c:set var="inventoryStatus" value="unset" />
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet> 
	<json:object name="inventoryResp" escapeXml="false">
		
		<json:object name="response">
			<json:property name="skuStatus" value="${inventoryStatus}"/>
			<json:property name="stockLevel" value="${stockLevel}"/>
		</json:object>
	</json:object>
	
</dsp:page>