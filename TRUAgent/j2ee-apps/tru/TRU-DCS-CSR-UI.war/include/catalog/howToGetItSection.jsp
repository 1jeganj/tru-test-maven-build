<%@ include file="/include/top.jspf" %>
<dsp:page>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<dsp:getvalueof  param="skuVo.freightClass" var="freightClass"/>
<dsp:getvalueof  param="skuVo.freightClassMessage" var="freightClassMessage"/>
<dsp:getvalueof  param="skuVo.storeMessage" var="storeMessage"/>
<c:if test="${not empty freightClassMessage || not empty storeMessage}">
	<h2>how to get it</h2>
	<c:if test="${not empty freightClassMessage}">
			<dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
                 <dsp:param name="data" value="${freightClassMessage}"/>
                  <dsp:oparam name="output">
                    		<dsp:valueof param="enocdeData" valueishtml="true" />
                   </dsp:oparam>
            </dsp:droplet>
	<%-- <dsp:valueof value="${freightClassMessage}" valueishtml="true"></dsp:valueof> --%>
	</c:if>
	<c:if test="${not empty storeMessage}">
		<p><fmt:message key="tru.productdetail.label.storePickup"/>(<span class="things-to-know"><a data-target="#learnMorePopupHTGI" data-toggle="modal" href="javascript:void(0);"><fmt:message key="tru.productdetail.label.learnmore"/></a></span>):</p>
			<dsp:droplet name="/com/tru/commerce/droplet/TRUBigStringDecoderDroplet">
                 <dsp:param name="data" value="${storeMessage}"/>
                  <dsp:oparam name="output">
                    		<dsp:valueof param="enocdeData" valueishtml="true" />
                   </dsp:oparam>
            </dsp:droplet>
	<%-- <dsp:valueof value="${storeMessage}" valueishtml="true"></dsp:valueof> --%>
	</c:if>
	</c:if>
	
</dsp:page>