<%@ include file="/include/top.jspf" %>
<dsp:page>
	<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
	<dsp:getvalueof  param="sizeChart" var="sizeChartName"/>
	<dsp:getvalueof  param="channelAvailability" var="channelAvailability"/>
	<dsp:getvalueof  param="channelMode" var="channelMode"/>
	<dsp:importbean bean="/com/tru/common/TRUStoreConfiguration" />
	<dsp:importbean bean="/com/tru/commerce/csr/droplet/CSRSiteDroplet"/>
	<%--  <dsp:importbean bean="/atg/multisite/Site" />
      <dsp:getvalueof var="currentSiteId" bean="Site.id" /> --%>
   	<dsp:getvalueof var="siteId" param="siteId"/>
	<dsp:getvalueof var="pdpSizechartImageStaticPath" bean="TRUStoreConfiguration.pdpSizechartImageStaticPath" />
	<c:if test="${not empty channelAvailability && not empty channelMode}">
		<c:choose>
			<c:when test="${channelMode eq 'onlineOnly'}">
				<div class="modal-content sharp-border welcome-back-overlay">
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
					<fmt:message key="tru.productdetail.label.learnMorePopupOnline"></fmt:message>
					</p>
				</div>
			</c:when>
			<c:when test="${channelMode eq 'storeOnly'}">
				<div class="modal-content sharp-border welcome-back-overlay">
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.learnMorePopupStore"></fmt:message>
					</p>
				</div>
			</c:when>
			<c:otherwise>
				<div class="modal-content sharp-border welcome-back-overlay">
					<h2><fmt:message key="tru.productdetail.label.learnmore" /></h2>
					<p class="abandonCartReminder">
						<fmt:message key="tru.productdetail.label.learnMorePopupBoth"></fmt:message>
					</p>
				</div>	
			</c:otherwise>
		</c:choose>
	</c:if>		
	<c:if test="${not empty sizeChartName }">
		<div class="size-content size-chart-image">	
		<dsp:droplet name="CSRSiteDroplet">
		<dsp:param name="siteId" value="${siteId}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="siteURL" param="siteURL"/>
			</dsp:oparam>
		</dsp:droplet>	
			 <img src="${siteURL}${pdpSizechartImageStaticPath}${sizeChartName}" /> 
		</div>
	</c:if>
</dsp:page>