 <%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:importbean bean="/com/tru/commerce/csr/droplet/DisplayPromotionDetailsDroplet"/>
	<dsp:droplet name="DisplayPromotionDetailsDroplet">
		<dsp:param name="promotionId" param="promotionId"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="promotionDetails" param="promotionDetails"/>
			${promotionDetails}
		</dsp:oparam>
		<dsp:oparam name="empty">
			No Details for the promotion Id: <dsp:valueof param="promotionId" />
		</dsp:oparam>
	</dsp:droplet> 
</dsp:page> 