<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:getvalueof  param="esrbrating" var="esrbrating"/>
<dsp:getvalueof  param="displayEsrbrating" var="displayEsrbrating"/>
<dsp:getvalueof  param="videoGameEsrb" var="videoGameSKU"/>
<dsp:getvalueof  param="productInfo.productId" var="productId"/>
<dsp:getvalueof value="false" var="matureValue"></dsp:getvalueof>
<fmt:setBundle basename="com.tru.resourcebundle.fhl.I18NResourceBundle"/>
<c:if test="${not empty videoGameSKU}">
<style type="text/css">
	.esrbImage {
    width: 33px;
    height: 45px;
    }
    .inline {
    display: inline-block;
    }
</style>
	<div class="logo">
	
		<c:choose>
			<c:when test="${videoGameSKU eq 'Everyone'}">
				<img src="/TRU-DCS-CSR/images/esrb/E.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Early Childhood'}">
				<img src="/TRU-DCS-CSR/images/esrb/eC.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Everyone 10+'}">
				<img src="/TRU-DCS-CSR/images/esrb/E10Plus.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Mature'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="/TRU-DCS-CSR/images/esrb/M.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Teen'}">
				<img src="/TRU-DCS-CSR/images/esrb/T.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Rating Pending'}">
				<img src="/TRU-DCS-CSR/images/esrb/RP.png" class="esrbImage">
			</c:when>
			<c:when test="${videoGameSKU eq 'Adults Only'}">
				<dsp:getvalueof value="true" var="matureValue"></dsp:getvalueof>
				<img src="/TRU-DCS-CSR/images/esrb/Ao.png" class="esrbImage">
			</c:when>
		</c:choose>
		
		<div class="limit-1 inline">
			<span class="rating"><fmt:message
					key="tru.productdetail.label.esrbRating" /></span> <br>
				<c:if test="${not empty esrbrating}">
					<fmt:message key="${esrbrating}" />
				</c:if>
		</div>
	</div>
	</c:if>
 	<c:if test="${displayEsrbrating eq 'true'}">
 		<span class="esrbMatureError">Warning</span>
						<p><b>One or more of the items you are attempting to add to your cart is intended to mature audiences.</b> You must be 17 or older to purchase.<br>
						This item may contain intense violence, blood and gore, sexual content and/or strong language.</p>
		<p>By ordering this item you are certifing that you are at-least of 17 years of age.</p> 		
 		<button id="yes" value="yes" onclick="addproductToCart();" >Yes</button>
 		<button id="yes" value="No" onclick="hidePopupWithResults('atg_commerce_csr_catalog_productDetailPopup', {result:'cancel'});return false;" >No</button>
 		
 	</c:if>
 
</dsp:page>