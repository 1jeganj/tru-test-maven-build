<%@ include file="/include/top.jspf" %>
<dsp:page>
<dsp:getvalueof  param="skuVo.features" var="features"/>
<c:if test="${not empty features }">
<h2>features</h2>
		<ul>
		<c:forEach items="${features}" var="feature">
			<li>
			<%-- <dsp:valueof value="${feature.key}"></dsp:valueof> --%>
			<dsp:valueof value="${feature.value}"></dsp:valueof>
			</li>
			
			<!-- <li>Anna is a free-spirited daydreamer determined to save her
				kingdom</li>
			<li>Elaborate gown features a rich, multi-colored bodice
				decorated with vibrant pink swirl, sparkles and flowers</li>
			<li>Her bright blue skirt is accented with traditional Norwegian
				inspired design</li>
			<li>Girls will love reenacting their favorite scenes from the
				movie</li> -->
				</c:forEach>
		</ul>
		</c:if>
</dsp:page>