<%--
 A page fragment that displays the SKU price

 @param product - The product item
 @param sku - The SKU item belonging to the product
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/displaySkuPrice.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dsp:page xml="true">
 <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
  <dsp:importbean bean="/atg/commerce/custsvc/util/CSRAgentTools" var="csrAgentTools"/>
  <dsp:importbean bean="/com/tru/commerce/droplet/TRUCSRPriceDroplet" />
  <dsp:importbean var="site" bean="/atg/multisite/Site" />
  <dsp:getvalueof var="currentSiteId" param="currentSiteId"/>
  <dsp:getvalueof var="sku" param="sku"/>
  <dsp:getvalueof var="product" param="product"/>
  
 <%--  <csr:skuPriceDisplay salePrice="displaySalePrice" listPrice="displayListPrice" product="${product}" sku="${sku}"/> --%>
  <%-- <csr:getCurrencyCode>
   <c:set var="currencyCode" value="${currencyCode}" scope="request" />
  </csr:getCurrencyCode> 
  <c:if test="${not empty displaySalePrice && displaySalePrice ne displayListPrice}">
    <span class="atg_commerce_csr_common_content_strikethrough">
      <web-ui:formatNumber value="${displayListPrice}" type="currency" currencyCode="${currencyCode}"/>
    </span>
    &nbsp;<web-ui:formatNumber value="${displaySalePrice}" type="currency" currencyCode="${currencyCode}"/>
  </c:if>
  <c:if test="${empty displaySalePrice || (not empty displaySalePrice && displaySalePrice eq displayListPrice)}">
    <web-ui:formatNumber value="${displayListPrice}" type="currency" currencyCode="${currencyCode}"/>
  </c:if> --%>
  <csr:getCurrencyCode>
   <c:set var="currencyCode" value="${currencyCode}" scope="request" />
  </csr:getCurrencyCode>
  <dsp:droplet name="TRUCSRPriceDroplet">
		<dsp:param name="skuId" value="${sku.id}"/>
		<dsp:param name="productId" value="${product.id}" />
		<dsp:param name="site" value="${site}" />
		<dsp:param name="siteId" value="${currentSiteId}" />
		<dsp:oparam name="true">
		<dsp:getvalueof var="salePrice" param="salePrice" />
		<dsp:getvalueof var="listPrice" param="listPrice" />
		<span class="atg_commerce_csr_common_content_strikethrough"><web-ui:formatNumber value="${listPrice}" type="currency" currencyCode="${currencyCode}"/></span>
		<web-ui:formatNumber value="${salePrice}" type="currency" currencyCode="${currencyCode}"/>
		</dsp:oparam>
		<dsp:oparam name="false">
		<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="salePrice" param="salePrice" />
				<dsp:getvalueof var="listPrice" param="listPrice" />
				<c:choose>
					<c:when test="${salePrice == 0}">
						<span><web-ui:formatNumber value="${listPrice}" type="currency" currencyCode="${currencyCode}"/></span>
					</c:when>
					<c:otherwise>
						<span><web-ui:formatNumber value="${salePrice}" type="currency" currencyCode="${currencyCode}"/></span>
					</c:otherwise>
				</c:choose>
		</dsp:oparam>
		<dsp:oparam name="empty">
		 <fmt:message key="catalogBrowse.searchResults.noPrice"/>
		</dsp:oparam>
	</dsp:droplet>
	</dsp:layeredBundle>
</dsp:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/catalog/displaySkuPrice.jsp#1 $$Change: 875535 $--%>
