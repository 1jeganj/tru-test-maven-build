<%--
  Credit Card Editor 

@creditCardId - Optional, the ID of an credit card to edit. If not
  supplied, creates a new credit card.

@version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/creditcards/creditCardEditor.jsp#2 $$Change: 883241 $
@updated $DateTime: 2014/04/15 07:49:50 $$Author: jsiddaga $
--%>
<%@ include file="/include/top.jspf" %>
<c:catch var="exception">
  <dsp:page xml="true">
    <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean var="creditCardForm" bean="/atg/commerce/custsvc/ui/fragments/CreditCardForm"/>
<dsp:importbean var="CSRUpdateCreditCardFormHandler"
                  bean="/atg/commerce/custsvc/order/UpdateCreditCardFormHandler"/>
  <dsp:importbean var="urlDroplet"
                  bean="/atg/svc/droplet/FrameworkUrlDroplet"/>
  <dsp:importbean var="pageFragment"
                  bean="/atg/commerce/custsvc/ui/fragments/order/EditCreditCard"/>
  <dsp:importbean var="pgConfig"
                  bean="/atg/commerce/custsvc/ui/CreditCardConfiguration"/>
    <dsp:importbean var="CSRConfigurator"
                  bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
    <dsp:layeredBundle basename="atg.svc.commerce.WebAppResources">
    <c:url var="successErrorURL" context="${CSRConfigurator.truContextRoot}"
         value="${pgConfig.editPageFragment.URL}">
	    <c:param name="nickname" value="${nickname}"/>
	    <c:param name="${stateHolder.windowIdParameterName}"
	             value="${windowId}"/>
	           
	    <c:param name="success" value="true"/>
	  </c:url>
      <c:set var="creditCardFormHandlerPath"
        value="/atg/commerce/custsvc/repository/CreditCardFormHandler"/>
      <dsp:importbean var="ccfh" bean="${creditCardFormHandlerPath}"/>
       <c:if test="${not empty param.creditCardId}">
        <dsp:setvalue value="${param.creditCardId}"  bean="CreditCardFormHandler.repositoryId"/>
    <c:set var="ccItem" value="${ccfh.creditCardMetaInfo.creditCard}"/>
    <c:choose>
      <c:when test="${ccItem.repositoryId eq param.creditCardId}">
        <c:set var="isDeleted" value="false"/>
      </c:when>
      <c:otherwise>
        <c:set var="isDeleted" value="true"/>
      </c:otherwise>
    </c:choose>
     </c:if>
      <svc-ui:frameworkPopupUrl var="url"
        value="/include/creditcards/creditCardEditor.jsp"
        context="${CSRConfigurator.contextRoot}"
        creditCardId="${param.creditCardId}"
        windowId="${windowId}"/>
      <svc-ui:frameworkPopupUrl var="successUrl"
        value="/include/creditcards/creditCardEditor.jsp"
        context="${CSRConfigurator.contextRoot}"
        success="true"
        creditCardId="${param.creditCardId}"
        windowId="${windowId}"/>
      <svc-ui:frameworkPopupUrl var="errorUrl"
        value="/include/creditcards/creditCardEditor.jsp"
        context="${CSRConfigurator.contextRoot}"
        success="true"
        windowId="${windowId}"/>
      <script type="text/javascript">
        atg.commerce.csr.order.billing.initializeCreditCardTypeDataContainer();
        <dsp:droplet name="ForEach">
          <dsp:param name="array" bean="/atg/commerce/payment/CreditCardTools.cardCodesMap" />
          <dsp:oparam name="output">
            <dsp:getvalueof var="cardType" param="key"/>
            <dsp:getvalueof var="cardCode" param="element"/>
                atg.commerce.csr.order.billing.addCreditCardTypeData("<c:out value='${cardType}'/>", "<c:out value='${cardCode}'/>");
          </dsp:oparam>
        </dsp:droplet>
      </script>
      <div class="atg_commerce_csr_popupPanel">
        <c:choose>
          <c:when test="${param.success}">
            <script type="text/javascript">
              hidePopupWithResults( 'atg_commerce_csr_editCreditCard', {result : 'save'});
            </script>
          </c:when>
          <c:otherwise>
            <dsp:droplet name="/atg/targeting/TargetingForEach">
              <dsp:param name="targeter" bean="/atg/registry/Slots/UserMessagingSlot"/>
              <dsp:oparam name="outputStart"> 
                <span class="atg_commerce_csr_common_content_alert">
                  <ul>
              </dsp:oparam>
              <dsp:oparam name="output"> 
                <dsp:getvalueof var="details" param="element.messageDetails" />
                <li>${fn:escapeXml(details[0].description)}</li>
              </dsp:oparam>
              <dsp:oparam name="outputEnd"> 
                  </ul>
                </span>
              </dsp:oparam>
              <dsp:oparam name="empty">
              </dsp:oparam>
            </dsp:droplet>
          </c:otherwise>
        </c:choose>
        <c:set var="formId" value="editCreditCardForm"/>
        <dsp:form formid="${formId}" id="${formId}">
      <c:if test="${isDeleted == false or empty isDeleted}">
          <dsp:input type="hidden" value="${successUrl}"
            bean="CreditCardFormHandler.createSuccessURL"/>
          <dsp:input type="hidden" value="${url}"
            bean="CreditCardFormHandler.createErrorURL"/>
          <dsp:input type="hidden" value="${successUrl}"
            bean="CreditCardFormHandler.updateSuccessURL"/>
          <dsp:input type="hidden" value="${errorUrl}"
            bean="CreditCardFormHandler.updateErrorURL"/>
          <c:choose>
            <c:when test="${not empty param.creditCardId}">
              <dsp:setvalue value="${param.creditCardId}"
                 bean="CreditCardFormHandler.repositoryId"/>
              <dsp:input type="hidden" value="${param.creditCardId}"
                 priority="1000" bean="CreditCardFormHandler.repositoryId"/>
              <h2 id="atg_commerce_csr_editCreditCard">
                <fmt:message key="creditCard.editTitle"/>
              </h2>
            </c:when>
            <c:otherwise>
              <h2 id="atg_commerce_csr_editCreditCard">
                <fmt:message key="creditCard.addNewTitle"/>
              </h2>
            </c:otherwise>
          </c:choose>
    	  <dsp:getvalueof var="userCreditCardId" param="creditCardId"></dsp:getvalueof>
    	  <c:choose>
    	  	<c:when test="${not empty userCreditCardId}">
    	  		<c:set var="isEditCreditCard" value="${true}"/>
    	  	</c:when>
    	  	<c:otherwise>
    	  		<c:set var="isEditCreditCard" value="${false}"/>
    	  	</c:otherwise>
    	  </c:choose>	
          <ul class="atg_commerce_csr_paymentForm">
            <dsp:include src="/include/order/creditCardForm_account.jsp" otherContext="${CSRConfigurator.truContextRoot}">
              <dsp:param name="formId" value="${formId}"/>
              <dsp:param name="isEditCreditCard" value="${isEditCreditCard}"/>
              <dsp:param name="creditCardBean" value="/atg/commerce/custsvc/repository/CreditCardFormHandler.value"/>
              <dsp:param name="creditCardAddressBean" value="/atg/commerce/custsvc/repository/CreditCardFormHandler.newAddressMetaInfo.address"/>
              <dsp:param name="creditCardFormHandler" value="/atg/commerce/custsvc/repository/CreditCardFormHandler"/>
              <dsp:param name="submitButtonId" value="saveChoice"/>
              <dsp:param name="isMaskCardNumber" value="${!empty param.creditCardId ? true : false}"/>
              <dsp:param name="isUseExistingAddress" value="${false}"/>
            </dsp:include>
            <li>
           <c:forEach var="editor" items="${ccfh.creditCardMetaInfo.params.defaultCreditCardEditorOptions}">
              <c:if test="${not empty editor.value.customEditorPage}">
                <dsp:include src="${editor.value.customEditorPage}" otherContext="${CSRConfigurator.truContextRoot}">
                  <dsp:param name="options" value="${editor.value}"/>
                  <dsp:param name="formHandlerPath" value="${creditCardFormHandlerPath}"/>
                  <dsp:param name="formHandler" value="${ccfh}"/>
                </dsp:include>
              </c:if>
            </c:forEach>          
            </li>
          </ul>
          
          <%-- create action --%>
          <dsp:input type="hidden" value="--" priority="-100"
            name="createCreditCardAction" bean="CreditCardFormHandler.create"/>
          <%-- update action --%>
          <dsp:input type="hidden" value="--" priority="-100"
            name="updateCreditCardAction" bean="CreditCardFormHandler.update"/>
          <dsp:param name="maskedCreditCardNumber" bean="/atg/commerce/custsvc/repository/CreditCardFormHandler.value.creditCardNumber" 
            converter="creditCard" maskcharacter="*" numcharsunmasked="4"/>
          <dsp:getvalueof var="maskedCreditCardNumberVar" param="maskedCreditCardNumber"/>
          <div class="atg_commerce_csr_panelFooter">
            <input value="<fmt:message key='creditCard.save.label'/>" 
              type="button" id="saveChoice" name="saveChoice" 
              onClick="<c:choose><c:when test="${empty param.creditCardId ? 'true' : 'false'}">
               getOrderId('createCreditCard');return false;
                </c:when><c:otherwise>
                getOrderId('editCreditCard');return false;
                </c:otherwise></c:choose>"/>
            <input value="<fmt:message key='creditCard.cancel.label'/>" 
              type="button" name="cancelChoice"
              onClick="hidePopupWithResults('atg_commerce_csr_editCreditCard', {result:'cancel'}); 
              return false;"/>
          </div>
      </c:if>
      <c:if test="${isDeleted}">
        <div  style="font-size:1.1667em; font-weight:bold;">This credit card is already deleted!<br></div>
            <div class="atg_commerce_csr_panelFooter">
            <input value="Return" 
              type="button" name="saveChoice"
              onClick="hidePopupWithResults('creditCardPopup', {result:'save'}); 
              return false;"/>
            </div>        
      </c:if>
        </dsp:form>
      </div>
      <div style="display:none;" id="hiddenMyAccountAddCreditSuggestedAddress"></div>
    </dsp:layeredBundle>
    
         <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
             value="/panels/order/shipping/includes/results.jsp">
     
     </c:url>  
     <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
              value="/panels/order/shipping/includes/billingSuggestions.jsp">
         <c:param name="accId" value="${param.addressId}"/>
            <c:param name="pageName" value="accountCredit"/>
              <c:param name="paramId" value="${param.creditCardId}"/>
              <c:param name="maskedCreditCardNumberVar" value="${maskedCreditCardNumberVar}"/>
         <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
         <%--   <c:param name="sp" value="${shippingGroup}"/> --%>
       </c:url>
       
     <script type="text/javascript">
     $(document).on("keypress","#editCreditCardForm_cvv",function(evt){
  		 evt = (evt) ? evt : window.event;
 	      var charCode = (evt.which) ? evt.which : evt.keyCode;
 	      if(charCode == 45)return true;
 	      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
 	          return false;
 	      }
 	      return true;
  	});
    function getOrderId(eventType){
    	if($('#error').length)
    		{
    			$('#error').remove();
    		}
    	var cardType = $("#editCreditCardForm_creditCardType").val();
		 var cvv = $("#editCreditCardForm_cvv").val();
		 var cvvLength = cvv.length;
		 if((cardType != "American Express" && cvvLength < 3) || (cardType == "American Express" && cvvLength < 4) )
			{
				$('#editCreditCardForm_cvv').parent().append('<span class="error" style="color:red;position: absolute;margin-left: 8px;font-weight:bold;">Invalid cvv</span>');
				return false;
			}
			
    	var createNewAddress = $("#editCreditCardForm").find("input[type='radio']:checked").val();
    	if(typeof(createNewAddress) == 'undefined' || createNewAddress == '')
    		{
    			createNewAddress = 'true';
    		}
    	 if(createNewAddress == 'true'){
    		var emptyInputError = false;
    		var noState = true;
    		$("#editCreditCardForm_addressArea").find("input[type='text']").not(":hidden").not("#editCreditCardForm_middleName").not("#editCreditCardForm_address2").filter(function(){
    			if($.trim($(this).val()) == '')
    				{
    					emptyInputError = true;
    				}
    		});
    		if(emptyInputError)
    		{
    			return false;
    		}
    		
		 var country= $("#editCreditCardForm_country").val();
		 var address1 = $('#editCreditCardForm_address1').val();
		 var address2 = $('#editCreditCardForm_address2').val();
		 var city = $('#editCreditCardForm_city').val();
		 var state1 = $('#editCreditCardForm_state').val();
		 var postalCode = $('#editCreditCardForm_postalCode').val();
		 var phoneNumber = $('#editCreditCardForm_phoneNumber').val();
		 var address2 = $('#editCreditCardForm_address2').val();
		 var pageName="billing";
		 var paramId =$('#paramId').val();
		if(country != 'United States')
			{
				if(state1 == 'NA')
					{
						var state = 'NA'
					}
			}
		else{
			
			state1 = state1.split("-")[0];
			var state = $.trim(state1);
		}
     	if(postalCode.length < 5)
		{
     		
			$('#editCreditCardForm_postalCode').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
			return false;
		}
		else if(phoneNumber.length < 10)
		{
			$('#editCreditCardForm_phoneNumber').closest('tr').append('<td id="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
			return false;
		}
     	if(country != 'United States' && eventType == 'createCreditCard')
		{
     		startTokenization();
		}
     	else if(country != 'United States' && eventType == 'editCreditCard')
     		{
	     		document.getElementById('${formId}')['/atg/commerce/custsvc/repository/CreditCardFormHandler.value.creditCardNumber']['disabled'] = 
	                ('${maskedCreditCardNumberVar}' == dijit.byId('${formId}_maskedCreditCardNumber').getValue());
	              document.getElementById('${formId}').createCreditCardAction['disabled'] = true;
	              document.getElementById('${formId}').updateCreditCardAction['disabled'] = false;
		          atgSubmitPopup({url: '${url}', 
	              form: document.getElementById('${formId}'),
	              popup: getEnclosingPopup('atg_commerce_csr_editCreditCard')});
	            return false;
     		}
     	else{
			dojo.xhrPost({
					url : "${resultsUrl}?address1="+address1+"&address2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&windowid="+window.windowId,
					encoding : "utf-8",
					preventCache : true,
					handle : function(_362, _363) {
						
						
						var response = dojo.trim(_362);
						dojo.query("#hiddenMyAccountAddCreditSuggestedAddress")[0].innerHTML = response;
						var derivedStatus = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #derivedStatus")[0].innerHTML;
						var addressRecognized = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #addressDoctorVO1")[0].innerHTML;
						
						if(derivedStatus == "NO" && addressRecognized == "true"){
							var data =  dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #firstSuggestedAddress")[0].innerHTML
							var form =  document.getElementById('csrBillingAddCreditCard'); 
							var suggestedAddress ={};
							suggestedAddress.city = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedCity-1")[0].value;
							suggestedAddress.state = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedState-1")[0].value;
							//suggestedAddress.country = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedCountry-1")[0].value;
							suggestedAddress.country = 'US';
							suggestedAddress.postalCode = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedPostalCode-1")[0].value;
							suggestedAddress.phoneNumber = phoneNumber;
							suggestedAddress.address1 = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedAddress1-1")[0].value;
							suggestedAddress.address2 = dojo.query("#hiddenMyAccountAddCreditSuggestedAddress #suggestedAddress2-1")[0].value; 
				        	startTokenization(suggestedAddress,0);
				        	/* $("#country").val(country);
				        	$("#address1").val(address1);
				        	$("#address2").val(address2);
				        	$("#city").val(city);
				        	$("#state").val(state);
				        	$("#postalCode").val(postalCode); */
				        	
						return false;
						}
						
						else if(derivedStatus == "YES"){
						atg.commerce.csr.common
								.showPopupWithReturn({
									popupPaneId : 'csrAddressDoctorFloatingPane',
									url : "${addressDoctorURL}?paramId="+paramId+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&windowId="+window.windowId,
									onClose : function(args) {
										if (args.result == 'ok') {
											atgSubmitAction({
												panelStack : [
														'cmcShippingAddressPS',
														'globalPanels' ],
												form : document
														.getElementById('transformForm')
											});
										}
									}
								});
						}
						else{
							startTokenization("",0);
							return false;
						}
						if (document.getElementById("inStorePickupResultsss")) {
							document.getElementById("inStorePickupResultsss").innerHTML = _362;
						}

					},
					mimetype : "text/html"
				});
     		}
    	 }
    	 else{
    		 if(eventType == 'createCreditCard')
    			 {
    				 startTokenization();
    			 }
    		 else
    			 {
	    			 document.getElementById('${formId}')['/atg/commerce/custsvc/repository/CreditCardFormHandler.value.creditCardNumber']['disabled'] = 
	 	                ('${maskedCreditCardNumberVar}' == dijit.byId('${formId}_maskedCreditCardNumber').getValue());
	 	              document.getElementById('${formId}').createCreditCardAction['disabled'] = true;
	 	              document.getElementById('${formId}').updateCreditCardAction['disabled'] = false;
	 		          atgSubmitPopup({url: '${url}', 
	 	              form: document.getElementById('${formId}'),
	 	              popup: getEnclosingPopup('atg_commerce_csr_editCreditCard')});
	 	            return false;
    			 }
    	 }
    	 hidePopupWithResults( 'creditCardPopup', {result : 'cancel'});
	}
  	$(document).on("click","#makeDefaultCreditCard",function(){
  		if($(this).is(":checked"))
	  	{
	  		$(this).val("true");
	  	}
  		else{
  			$(this).val("false");
  		}
  	});
  	function setCvvLength()
    {
    	$(".error").remove();
    	document.getElementById("${formId}_cvv").value = "";
    	var creditCardType = document.getElementById("${formId}_creditCardType").value;
    	if(creditCardType == "American Express" || creditCardType == "americanExpress")
  	  		{
  	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","4");
  	  		}
  	  	else
  	  		{
  	  			document.getElementById("${formId}_cvv").setAttribute("maxlength","3");
  	  		}
	  	if(creditCardType == 'R Us Private Label Credit Card' || creditCardType == "RUSPrivateLabelCard")
			{
				document.getElementById("${formId}_expirationMonth").disabled = true;
				document.getElementById("${formId}_expirationYear").disabled = true;
				$("#editCreditCardForm_expirationMonth,#editCreditCardForm_expirationYear").css("background-color","#e9e9e9");
				$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","none");
				var userAgent = window.navigator.userAgent;
  	  			var isIeBrowser = userAgent.indexOf("MSIE");
  	  			if(isIeBrowser > 0)
  	  				{
  	  					$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").hide();
  	  				}
			}
		else
			{
				document.getElementById("${formId}_expirationMonth").disabled = false;
				document.getElementById("${formId}_expirationYear").disabled = false;
				$("#editCreditCardForm_expirationMonth,#editCreditCardForm_expirationYear").css("background-color","#fff");
				$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").css("pointer-events","all");
				var userAgent = window.navigator.userAgent;
  	  			var isIeBrowser = userAgent.indexOf("MSIE");
  	  			if(isIeBrowser > 0)
  	  				{
  	  					$("#${formId}_expirationYear").closest('tr').find(".dijitDownArrowButtonInner").show();
  	  				}
			}
    }
  	$(document).ready(function(){
  		setCvvLength();
  	})
  	 function isValidForm(){
    	var existingAddress = $("#creditCardPopup").find("input[type=radio]:checked").val();
    	var emptyInputError = false;
    	if(existingAddress == 'true')
    		{
		    	$("#editCreditCardForm_addressArea").find("input[type=text]").not(":hidden").not("#editCreditCardForm_address2").filter(function(){
		    		if($(this).val() == "")
		    			{
		    				emptyInputError = true;
		    			}
		    	});
    		}
    	if($("#editCreditCardForm_nameOnCreditCard").val() == "" || 
    		$("#editCreditCardForm_creditCardType").val() =="" ||
    		$("#editCreditCardForm_creditCardNumber").val() =="" ||
    		($("#editCreditCardForm_expirationMonth").val() =="" && $("#editCreditCardForm_creditCardType").val() != "R Us Private Label Credit Card") ||
    		($("#editCreditCardForm_expirationYear").val() =="Year" && $("#editCreditCardForm_creditCardType").val() != "R Us Private Label Credit Card") ||
    		$("#editCreditCardForm_cvv").val() =="" || emptyInputError)
    		{
    			$('#editCreditCardForm').find("#saveChoice").attr("disabled",true);
    		}
    	else
    		{
    			$('#editCreditCardForm').find("#saveChoice").attr("disabled",false);
    		}
    	}
    $(document).ready(function(){
    	setTimeout(function(){
    		isValidForm();
    	},300)
    	
    });
    $(document).on('keyup',"#editCreditCardForm	 input[type=text]",function(){
		isValidForm();
	});
    </script>
  </dsp:page>
</c:catch>
<c:if test="${exception != null}">
  ${exception}
  <%
     Exception ee = (Exception) pageContext.getAttribute("exception");
     ee.printStackTrace();
  %>
</c:if>

<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/include/creditcards/creditCardEditor.jsp#2 $$Change: 883241 $--%>