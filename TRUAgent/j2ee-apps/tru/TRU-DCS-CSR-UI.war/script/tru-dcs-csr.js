dojo.provide("atg.commerce.csr.cart");
//Modifying OOTB script for Instore Pick up functionality in PDP and Shipping pages
atg.commerce.csr.catalog.pickupInStoreSearchStores=function(_358,_359,_35a,_35b){
	if(!_358&&!_359&&!_35a){
		var form=dojo.byId("shippingInStorePickupSearchForm");
		var _35d=dojo.byId("shippingInStorePickupSearchContainer");
		if(_35d){
			form.countryCode.value=dojo.byId("shippingInStorePickupCountry").value;
			form.postalAddress.value=dojo.byId("shippingInStorePickupPostalCode").value;
			if(dojo.byId("shippingInStorePickupState")){
				form.state.value=dojo.byId("shippingInStorePickupState").value;
			}
			var _35e=dojo.byId("shippingInStorePickupPostalCity");
			if(_35e&&_35e.value!=""&&_35e.value!="City"){
				form.city.value=_35e.value;
			}
			if(form["geoLocatorServiceProviderEmpty"]){
			if(form["geoLocatorServiceProviderEmpty"].value=="-1"){
				form.distance.value=-1;
			}else{
				form.distance.value=form["geoLocatorServiceProviderEmpty"].value;
			}
			}else{
				form.distance.value=dojo.byId("shippingInStorePickupProximity").value;
			}
		}
	}else{
		var form=dojo.byId("inStorePickupSearchForm");
		var _35d=dojo.byId("inStorePickupSearchContainer");
		if(_35d){
			form.countryCode.value=dojo.byId("inStorePickupCountry").value;
			form.postalAddress.value=dojo.byId("inStorePickupPostalCode").value;
			if(dojo.byId("inStorePickupState")){
				form.state.value=dojo.byId("inStorePickupState").value;
			}
			var _35e=dojo.byId("inStorePickupPostalCity");
			if(_35e&&_35e.value!=""&&_35e.value!="City"){
				form.city.value=_35e.value;
			}
			if(form["geoLocatorServiceProviderEmpty"]){
				if(form["geoLocatorServiceProviderEmpty"].value=="-1"){
					form.distance.value=-1;
				}else{
					form.distance.value=form["geoLocatorServiceProviderEmpty"].value;
				}
			}else{
				form.distance.value=dojo.byId("inStorePickupProximity").value;
			}
		}
	}
	if(_35b){
		if(form["allItemsSuccessURLHidden"]){
			form["successURL"].value=form["allItemsSuccessURLHidden"].value;
		}
		}else{
			if(form["successURLHidden"]){
				form["successURL"].value=form["successURLHidden"].value;
			}
		}
		if(!_358&&!_359&&!_35a){
			dojo.xhrPost({form:form,url:atg.commerce.csr.getContextRoot()+"/include/catalog/shippingLocationsSearchResults.jsp?_windowid="+window.windowId,encoding:"utf-8",handle:function(_35f,_360){
			if(document.getElementById("storesSearchResults")){
				document.getElementById("storesSearchResults").innerHTML=_35f;
			}
		},mimetype:"text/html"});
		}else{
			dojo.xhrPost({form:form,url:atg.commerce.csr.getContextRoot()+"/include/catalog/pickupLocationsResults.jsp?_windowid="+window.windowId,queryParams:{"productId":_358,"skuId":_359,"quantity":_35a,"allItems":_35b},encoding:"utf-8",preventCache:true,handle:function(_361,_362){
			if(document.getElementById("inStorePickupResults")){
				document.getElementById("inStoesulrePickupRts").innerHTML=_361;
			}
		},mimetype:"text/html"});
	}
};
