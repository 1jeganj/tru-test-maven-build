<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--
  Login page for Agent application.

  @version $Id: /TRU/TRUAgent/j2ee-apps/tru/TRUServiceFramework.war/login.jsp#2 $$Change: 883241 $
  @updated $DateTime: 2016/10/25 07:49:50 $$Author: PA $
--%>
<%@ include file="/include/top.jspf"%>
<dspel:page xml="true">
  <dspel:importbean var="profileErrorMessageForEach" bean="/atg/dynamo/droplet/ErrorMessageForEach" />
  <dspel:importbean var="switch" bean="/atg/dynamo/droplet/Switch" />
  <dspel:importbean bean="/atg/svc/repository/service/StateHolderService" var="stateHolder" />

  <dspel:setvalue bean="AgentProfileFormHandler.value.password" value="" />
  <c:choose>
    <c:when test="${param.sessioninvalid and param.ppr}">
      <caf:outputStatus statusKey="sessioninvalid" redirectUrl="${UIConfig.contextRoot}/" />
    </c:when>
    <c:otherwise>
      <dspel:setLayeredBundle basename="atg.svc.agent.WebAppResources" />
      <input type="hidden" name="ssUserName" id="ssUserName" value="${param.userName }"/>
      <input type="hidden" name="isSSOLogin" id="isSSOLogin" value="${param.ssoLogin}"/>
      <html>
        <head>
          <link rel="icon" href="<c:url value='/image/favicon.ico'/>" type="image/x-icon" />
          <link rel="shortcut icon" href="<c:url value='/images/tru-logo-sm.png'/>" type="image/x-icon" />
          <link type="text/css" href="/csc/tru-csc-workspace.css" rel="stylesheet" />
          <title><fmt:message key="appName" /></title>
          <script type="text/javascript">
            window.history.forward(1);
            function setFocus() {
            	 var ssUserName = document.getElementById("ssUserName");
            	 var isSSLogin = document.getElementById("isSSOLogin");
            	 var loginForm = document.getElementById('loginForm');
            	 loginForm.username.value = ssUserName.value;
            	 loginForm.submit();           
            }
          </script>
          <style>
          	body{height:100%;}
          </style>
        </head>
        <body id="loginBody" onload="setFocus()">
          <div id="loginDiv" style="height:253px;margin-top:230px">
            
            <dspel:form action="${thisPage}" method="post" name="loginForm" id="loginForm" requiresSessionConfirmation="true">
              <input type="hidden" name="<c:out value='${stateHolder.windowIdParameterName}'/>" value="<c:out value='${windowId}'/>" />
              <dspel:input bean="AgentProfileFormHandler.loginErrorURL" type="hidden" value="/agent/login.jsp?${stateHolder.windowIdParameterName}=${windowId}" />
              <dspel:input bean="AgentProfileFormHandler.loginSuccessURL" type="hidden" value="/agent/main.jsp?${stateHolder.windowIdParameterName}=${windowId}" />
             
                <dspel:droplet name="Switch">
                  <dspel:param bean="AgentProfileFormHandler.formError" name="value" />
                  <dspel:oparam name="true">
                    <fmt:message var="invalidLogin" key="login.invalidLogin" />
                    <fmt:message var="invalidPassword" key="login.invalidPassword" />
                    <p id="loginError">
                      <dspel:droplet name="ErrorMessageForEach">
                        <dspel:param name="messageTable" value="invalidLogin=${invalidLogin},invalidPassword=${invalidPassword}"/>
                        <dspel:param bean="AgentProfileFormHandler.formExceptions"
                         name="exceptions" />
                        <dspel:oparam name="output">
                          <c:set var="displayMessage">
                            <dspel:valueof param="message" converter="valueishtml" />
                          </c:set>
                          ${displayMessage}
                        </dspel:oparam>
                      </dspel:droplet>
                    </p>
                  </dspel:oparam>
                </dspel:droplet>
                    <dspel:input bean="AgentProfileFormHandler.value.login" maxlength="40" id="username" type="hidden" autocomplete="off"/>
                    <dspel:input bean="AgentProfileFormHandler.value.password" maxlength="40" id="password" type="hidden"/>
                    <!-- <img src="/truagent/images/toysrus-logo-large.png" class="esrbImage" /> -->
                   <%-- <center>
                    <h3>
                    <div class="logoProduct">
             			 <span class="logoProductName"><img src="/truagent/images/tru-logo-sm.png"/></span> <span class="logoProductTitle"><fmt:message key="logo.product.title"/></span>
		            </div>
                    </h3>
                    <span style="color:blue;">Please wait a movement to be connected</span>
                    </center> --%>
                    <div style="width:100%;height:100%;background: url(/truagent/images/loading.gif) no-repeat center;"></div>
                  <dspel:input bean="AgentProfileFormHandler.login" type="hidden" value="${loginButtonLabel}" id="loginFormSubmit" iclass="buttonSmall go" />
            </dspel:form>
          </div>
        </body>
      </html>
    </c:otherwise>
  </c:choose>
</dspel:page>
<%-- @version $Id: /TRU/TRUAgent/j2ee-apps/tru/TRUServiceFramework.war/login.jsp#2 $$Change: 883241 $--%>
