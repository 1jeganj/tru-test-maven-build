<!-- ticketSearchCustomerResultsPanel.jsp -->
<%--
    @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/panels/customerSearchResultsPanel.jsp#1 $$Change: 875535 $
    @updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>
<%@  include file="/include/top.jspf" %>
<dspel:page xml="true">
<dspel:include src="/panels/customer/searchResults.jsp" otherContext="${UIConfig.truContextRoot}"/>
</dspel:page>
<!-- end ticketSearchCustomerResultsPanel.jsp -->
<%-- @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/panels/customerSearchResultsPanel.jsp#1 $$Change: 875535 $--%>
