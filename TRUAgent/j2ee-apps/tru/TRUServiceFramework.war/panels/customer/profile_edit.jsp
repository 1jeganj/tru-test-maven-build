<%--
 This page defines the Customer Information Panel
 @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/panels/customer/profile_edit.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>

<%@ include file="/include/top.jspf" %>
<dspel:page xml="true">
  <dspel:layeredBundle basename="atg.svc.agent.WebAppResources">
  <dspel:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dspel:importbean var="CustomerProfileFormHandler" bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler"/>
  <dspel:importbean var="agentUIConfig" bean="/atg/svc/agent/ui/AgentUIConfiguration"/>

  <dspel:importbean var="defaultPageFragment" bean="/atg/svc/agent/ui/fragments/customer/CustomerEditDefault" /> 
  <dspel:importbean var="extendedPageFragment" bean="/atg/svc/agent/ui/fragments/customer/CustomerEditExtended" /> 

  <fmt:message key="customer.emailAddress.invalid" var="emailAddressInvalid"/>
  <fmt:message key="customer.emailAddress.required" var="emailAddressRequired"/>
  <fmt:message key="customer.firstName.required" var="firstNameRequired"/>
  <fmt:message key="customer.lastName.required" var="lastNameRequired"/>
  <fmt:message key="customer.loginName.required" var="loginNameRequired"/>
  <fmt:message key="customer.birthDate.tooltip" var="birthDateTooltip"/>

  <dspel:form style="display:none" action="#" id="customerResetPasswordForm" formid="customerResetPasswordForm">
    <dspel:input type="hidden" priority="-10" value="" bean="CustomerProfileFormHandler.resetPassword"/>
  </dspel:form>
  
  <dspel:form action="post" id="rewardCreateForm" formid="rewardCreateForm">
  <dspel:input type="hidden" id="rewardNumber" name="rewardNumber" bean="CustomerProfileFormHandler.rewardNumber" />
  <dspel:input type="hidden" value="dummy" priority="-100" bean="CustomerProfileFormHandler.addRewardMember" />
  </dspel:form>

  <dspel:setvalue bean="CustomerProfileFormHandler.extractDefaultValuesFromProfile" value="true"/> 

  <dspel:form action="#" id="customerCreateForm" formid="customerCreateForm">
    <input id="atg.successMessage" name="atg.successMessage" type="hidden" value=""/>
    <input id="atg.failureMessage" name="atg.failureMessage" type="hidden" value=""/>
  
  <c:if test="${not empty defaultPageFragment.URL}">			  
    <dspel:include src="${defaultPageFragment.URL}" otherContext="${agentUIConfig.truContextRoot}" />
  </c:if>	
  
  <c:if test="${not empty extendedPageFragment.URL}">			  
    <dspel:include src="${extendedPageFragment.URL}" otherContext="${extendedPageFragment.servletContext}" />
  </c:if>	

  <div>
     <ul class="atg_svc_addressForm customerInfo">
       <li class="coreCustomerInfoDataAction">
          <dspel:input type="hidden" value="true" bean="CustomerProfileFormHandler.update"/>
     
          
           <dspel:input type="hidden"  value="true" bean="CustomerProfileFormHandler.emailUpdate"/> 
          <fmt:message key="customer.view.update.button" var="updateButton" />
          <fmt:message key="customer.update.success.message" var="updateSuccessMessageFormat" />
          <dspel:getvalueof var="customerFirstName" bean="CustomerProfileFormHandler.value.firstName"/>
          <dspel:getvalueof var="customerLastName" bean="CustomerProfileFormHandler.value.lastName"/>
          <fmt:message key="customer.update.failure.message" var="updateFailureMessage" >
            <fmt:param value="${fn:escapeXml(customerFirstName)}" />
            <fmt:param value="${fn:escapeXml(customerLastName)}" />
          </fmt:message>
          <input id="update" type="button" value="${updateButton}" dojoType="atg.widget.validation.SubmitButton" 
            onclick='editCustomer("${fn:escapeXml(updateSuccessMessageFormat)}", "${fn:escapeXml(updateFailureMessage)}");return false;'/>
       </li>
    </ul>
  </div>

  </dspel:form>
  
  
   <dspel:form action="post" id="removeRewardNumber" formid="removeRewardNumber">
   <dspel:input type="hidden" value="dummy" priority="-100" bean="CustomerProfileFormHandler.removeRewardMember" />
   </dspel:form>
   
   
   
  <dspel:form action="post" id="rewardCreateForm" formid="rewardCreateForm">
  <dspel:input type="hidden" id="rewardNumber" name="rewardNumber" bean="CustomerProfileFormHandler.rewardNumber" />
  <dspel:input type="hidden" value="dummy" priority="-100" bean="CustomerProfileFormHandler.addRewardMember" />
  
<hr></br>
        <div class="atg-csc-base-table-row">
      						<span
							class="atg_svc_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label"
							style="vertical-align: top;"> <label for="segments">Enter
								Membership Number:</label>
						</span>
						<c:choose>
							<c:when
								test="${not empty CustomerProfileFormHandler.value.rewardNo}">
								<div class="atg-csc-base-table-cell">
									<dspel:input id="rewardNum" name="rewardNo" type="text" size="25" maxlength="13" required="${true}" bean="CustomerProfileFormHandler.value.rewardNo" onkeypress="return isNumberOnly(event);"/>
										<input type="button" value="update" id="rewardNumberUpdate" onclick="validateDenaliAccountNum()"/>
										<input type="button" value="remove" id="rewardNumberRemove" onclick="removeRewardNumber()"/>
								</div>
							</c:when>
					     </c:choose>
					     
					     <c:choose>
							<c:when
								test="${empty CustomerProfileFormHandler.value.rewardNo}">
								<div class="atg-csc-base-table-cell">
									<input id="rewardNum"  name="enterRewardNo" type="text" size="24" maxlength="13" style="position:relative !important;" onkeypress="return isNumberOnly(event);"/>
                                   <input name="rewardVal" type="button" value="apply" onclick="validateDenaliAccountNum()"/>
								</div>
							</c:when>
					     </c:choose>  
		  </div>
		  <br/>
		  
		    
  </dspel:form>
  <dspel:form action="post" id="customerUpdateForm" formId="customerUpdateForm">
  	<dspel:input type="hidden" id="firstName" value="" bean='CustomerProfileFormHandler.value.firstName'/>
  	<dspel:input type="hidden" id="lastName" value="" bean="CustomerProfileFormHandler.value.lastName"/>
  	<dspel:input type="hidden" id="email" value="" bean="CustomerProfileFormHandler.value.email"/>
  	<dspel:input type="hidden" id="login" value="" bean="CustomerProfileFormHandler.value.login"/>
  	<input id="atg.successMessage" name="atg.successMessage" type="hidden" value=""/>
    <input id="atg.failureMessage" name="atg.failureMessage" type="hidden" value=""/>
  	<dspel:input type="hidden" value="submit" bean="CustomerProfileFormHandler.update"/>
  </dspel:form> 
  <script type="text/javascript">
    atg.progress.update('cmcCustomerSearchPS');
  </script>
  <script type="text/javascript">
  var customerCreateFormValidate = function() {
	  var disable = false;
	  
	  if (!dijit.byId("firstName").isValid()) disable = true;
	  if (!dijit.byId("email").isValid()) disable = true;
	  if (!dijit.byId("lastName").isValid()) disable = true;

	  dojo.byId("customerCreateForm").update.disabled = disable;
	}

_container_.onLoadDeferred.addCallback(function() {
  customerCreateFormValidate();
  atg.service.form.watchInputs('customerCreateForm', customerCreateFormValidate);
  
  // The dsp:input button tag doesn't seem to be applying the tabIndex correctly, so we will do it manually
  var _submitButton = dojo.byId("update");
  if (_submitButton != null) {
    _submitButton.tabIndex = "8";
  }
});
_container_.onUnloadDeferred.addCallback(function () {
    atg.service.form.unWatchInputs('customerCreateForm');
  });

function validateDenaliAccountNum()
{
	var accountNumber = $('#rewardNum').val();
	var form=dojo.byId("rewardCreateForm");
	form.rewardNumber.value=dojo.byId("rewardNum").value;
	atgSubmitAction({
		panels : ['CustomerInformationPanel'],
        panelStack : 'customerPanels',
	    form : document.getElementById('rewardCreateForm')});
}

function removeRewardNumber()
{
	atgSubmitAction({
		panels : ['CustomerInformationPanel'],
        panelStack : 'customerPanels',
	    form : document.getElementById('removeRewardNumber')});
}
function isNumberOnly(evt)
{
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode == 46 || charCode == 8 || (charCode > 47 && charCode < 58)) {
    	return true;
	   }
	return false;
}
$(document).on('keyup', '#rewardNum', function(evt){
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode == 8 || charCode == 46)
    	{
    		var value = $(this).val();
    		if(value == "")
    			{
    				$("#rewardNumberUpdate").attr("disabled",true);
    				$("#rewardNumberRemove").attr("disabled",true);
    			}
    		
    	}
    else{
		$("#rewardNumberUpdate").attr("disabled",false);
		$("#rewardNumberRemove").attr("disabled",false);
	}
});
$(document).on('paste', '#rewardNum', function(e){
  	var clipboardData = e.clipboardData || window.clipboardData;
  	if(clipboardData && clipboardData.getData && clipboardData.getData('Text')){
  		var txt = clipboardData.getData('Text')
  	}
  	else{
  		var txt = e.originalEvent.clipboardData.getData('text');
  	}
    if(isNaN(txt)){
        e.preventDefault();
    }
    else
    	{
	    	$("#rewardNumberUpdate").attr("disabled",false);
			$("#rewardNumberRemove").attr("disabled",false);
    	}
});
function editCustomer(successMessageFormat, failureMessage){
	  /* successMessageFormat uses customer info that input user,
	   * failureMessage uses customer profile info
	   */
	 var theForm = dojo.byId("customerCreateForm");
	 var firstName = theForm["firstName"].value;
	 var lastName = theForm["lastName"].value;
	 var email = theForm["email"].value;
	 var form = dojo.byId("customerUpdateForm");
	 form["atg.successMessage"].value = dojo.string.substitute(successMessageFormat, [firstName, lastName]);
	 form["atg.failureMessage"].value = failureMessage;
	 form.firstName.value = firstName;
	 form.lastName.value = lastName;
	 form.email.value = email;
	 form.login.value = email;
	 atgSubmitAction({
	   form: form,
	   panel:["customerInformationPanel"],
	   panelStack: ["customerPanels","globalPanels"]
	   });
	 }
</script>
  </dspel:layeredBundle>
</dspel:page>
<%-- @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/panels/customer/profile_edit.jsp#1 $$Change: 875535 $--%>

