<%--
 This page defines the Customer Credit Cards Panel
 @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/customer/creditCards.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dspel:page xml="true">
  <dspel:importbean bean="/com/tru/common/TRUConfiguration"/>
<dspel:importbean bean="/atg/userprofiling/Profile" />
 <dspel:importbean bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler"/>
 <dspel:importbean  var="CSRConfigurator" bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
<%-- <dspel:importbean bean="/com/tru/integrations/cardinal/CardinalJwtDroplet" />	 --%>
 <dspel:importbean bean="/com/tru/radial/common/TRURadialConfiguration" />
<dspel:getvalueof var="enableVerboseDebug" bean="TRURadialConfiguration.enableVerboseDebug"/>
<dspel:getvalueof var="radialPaymentJSPath" bean="TRURadialConfiguration.radialPaymentJSPath"/>
  <dspel:layeredBundle basename="atg.svc.commerce.WebAppResources">
  
    <dspel:getvalueof var="mode" param="mode"/>
    <dspel:importbean var="profile"
      bean="/atg/userprofiling/ServiceCustomerProfile"/>
    <dspel:importbean var="ccfh" 
      bean="/atg/commerce/custsvc/repository/CreditCardFormHandler"/>
       <dspel:importbean bean="/atg/commerce/custsvc/order/ShoppingCart" var="cart"/>
       
       	<dspel:getvalueof var="orderId" bean="ShoppingCart.current.id"/>
 	<dspel:getvalueof var="orderAmount" bean="ShoppingCart.current.priceInfo.total"/>	 
 	<dspel:getvalueof var="currencyCode" bean="TRUConfiguration.currencyCode"/>
 	<dspel:getvalueof var="profileId" bean="Profile.id"/>
 	
 		<%-- <dspel:droplet name="CardinalJwtDroplet">
		<dspel:oparam name="output">
        	<dspel:getvalueof var="jwt" param="jwt"/>
        </dspel:oparam>
	</dspel:droplet> 
	<input type="hidden" name="jwt" id="jwt" value="${jwt}"/> --%>
    <c:set var="wallet" value="${ccfh.creditCardWallet}"/>
<script type="text/javascript">
  if (!dijit.byId("creditCardPopup")) {
    new dojox.Dialog({ id: "creditCardPopup",
                       cacheContent: "false", 
                       executeScripts: "true",
                       scriptHasHooks: "true"});
  }
</script>

<script type="text/javascript" charset="UTF-8"	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" charset="UTF-8"	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div id="atg_commerce_csr_customerinfo_credit_cards_subPanel" class="atg_svc_subPanel">
            <script type="text/javascript">
        
            (function(){
 			   var head = document.getElementsByTagName('head')[0];
 			   var radialScript = document.createElement('script');
 			   radialScript.type= 'text/javascript';
 			   radialScript.src= '${radialPaymentJSPath}';
 			   head.appendChild(radialScript);
 		  })();

/*  atg.commerce.csr.order.billing.applyPaymentGroups = function (pParams){
	  var form  = document.getElementById("csrBillingForm");
	   var rewardNumber=dojo.byId("enterMembershipIDInBilling").value;
	   if($('#email').length == 1){
	   var email=dojo.byId("email").value;
	   if(email == '')
		   {
		   $('.email').find("#emailErrMsg").remove();
		   	$('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Please enter your email address</span>");
		   	return false;
		   }
	   else
		   {
			   if (!isValidEmail(email)) {
				   $('.email').find("#emailErrMsg").remove();
				   $('.email').append("<span id='emailErrMsg' style='color:red;font-weight:bold;margin-left:5px;'>Email format is not valid, please enter valid email address</span>");
				   return false;
			  }
		 }
	   form.emailInBilling.value=email;
	   }
	  form.rewardNumberCheckOut.value=rewardNumber; 
	  if (container && container.availablePaymentMethods) {
	    for (id in container.availablePaymentMethods) {
	      var paymentMethod = container.availablePaymentMethods[id];
	      if (paymentMethod.paymentGroupType == 'inStorePayment') {
	        var paymentInput = dijit.byId(paymentMethod.paymentGroupId);
	        var paymentCheckbox = dojo.byId(paymentMethod.paymentGroupId + "_checkbox");
	        var paymentRelationshipType = dojo.byId(paymentMethod.paymentGroupId + "_relationshipType");
	        if (paymentInput && paymentCheckbox && !paymentCheckbox.checked) {
	          paymentInput.setValue(0);
	        }
	        if (paymentInput && paymentRelationshipType && paymentCheckbox && !paymentCheckbox.checked) {
	          paymentRelationshipType.value = 'ORDERAMOUNT';
	        }
	      }
	    }
	  }
	  
	  atg.commerce.csr.common.enableDisable([{form: "csrBillingForm", name: "csrHandleApplyPaymentGroups"}],
	      [{form: "csrBillingForm", name: "csrPaymentGroupsPreserveUserInputOnServerSide"}]);
	  atg.commerce.csr.common.prepareFormToPersistOrder (pParams);
	  atgSubmitAction({form:form});
	}; */ 
	
	/* function isValidEmail(email) {
	    var patern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4})(\]?)$/;
	    return patern.test(email);
	} */
	/**
	 *
	 * This method gets current values from the credit card type and credit card number widgets and
	 * using dojo.validate.isValidCreditCard() to validate the credit card.
	 *
	 */
	
	atg.commerce.csr.order.billing.isValidCreditCardNumber = function (pCardTypeWidget, pCardNumberWidget) {
		
	  if (!pCardTypeWidget) {
	    return false;
	  }

	  if (!pCardNumberWidget) {
	    return false;
	  }
	  var ccType = pCardTypeWidget.getValue();
	  var ccNumber = pCardNumberWidget.getValue();

	  if (ccType === '') {
	    dojo.debug("Supplied credit card type is not valid.");
	    pCardTypeWidget.promptMessage="Please select a valid credit card type.";
	    return false;
	  }
	  if (ccNumber === '') {
	    dojo.debug("Supplied card number is not valid.");
	    pCardNumberWidget.invalidMessage=getResource('csc.billing.invalidCreditCardNumber');
	    return false;
	  }
	  var cardTypeData = creditCardTypeDataContainer.getCreditCardTypeDataByKey(ccType);
	  if (!cardTypeData) {
	    dojo.debug("Supplied credit card type is not valid.");
	    pCardTypeWidget.invalidMessage=getResource('csc.billing.invalidCreditCardType');
	    return false;
	  }

	  var code = cardTypeData.code;
	  if (!cardTypeData) {
	    dojo.debug("Supplied credit card type is not valid."+cardTypeData);
	    pCardTypeWidget.invalidMessage=getResource('csc.billing.invalidCreditCardType');
	    return false;
	  }
	  if(code == 'RUSCoBrandedMasterCard' || code == 'RUSPrivateLabelCard'){
		 if(isTRUCardValid(ccNumber, code)){
			 return true;
		 }else{
			 return false;
		 }
	  }else{ 
		 if(dojox.validate.isValidCreditCard(ccNumber, code)) {
		    dojo.debug("This is a valid credit card number.");
		    return true;
		  } else {
		    dojo.debug("Please provide a valid credit card number.");
		    pCardNumberWidget.invalidMessage=getResource('csc.billing.invalidCreditCardNumber');
		    return false;
		  }
 	   }
	};
	
	// function to validate the TRU cards 
	function isTRUCardValid(pCCNumber, pCode){
		 var value = pCCNumber;
		 var v = parseInt(value.substr(0, 6));
		 var code = pCode.toLowerCase();
		 if (code == 'rusprivatelabelcard' && v == 604586 && (value.length == 16)) { /*PLCC card*/
             return true;
         } else if (code == 'ruscobrandedmastercard' && v == 524363 && (value.length == 16)) { /*USCOBMasterCard card*/
             return true;
         } else if (code == 'ruscobrandedmastercard' && v == 523770 && (value.length == 16)) { /*PRCOBMasterCard card*/
             return true;
         } 
	}

	

		  
        </script>

  <div class="atg_svc_subPanelHeader" >   
      <ul class="atg_svc_panelToolBar">
        <li class="atg_svc_header">
          <h4 id="atg_commerce_csr_customerinfo_paymentMethods"><fmt:message key="customer.creditCards.title.label"/> </h4>
        </li>
        <dspel:getvalueof var="mode" param="mode"/>
        <c:if test="${mode == 'edit'}">   
          <li class="atg_svc_last">          
            <svc-ui:frameworkPopupUrl var="creditCardEdit"
              value="/include/creditcards/creditCardEditor.jsp"
              context="${CSRConfigurator.truContextRoot}"
              windowId="${windowId}"/>
            <a href="#"
              class="atg_svc_popupLink"
              onClick="atg.commerce.csr.common.showPopupWithReturn({
                popupPaneId: 'creditCardPopup',
                title: '<fmt:message key="customer.creditCards.addCreditCard.label"/>',
                url: '${creditCardEdit}',
                onClose: function( args ) {
                  if ( args.result == 'save' ) {
                    atgSubmitAction({
                      panels : ['customerInformationPanel'],
                      panelStack : ['customerPanels','globalPanels'],
                      form : document.getElementById('transformForm')
                    });
                  }
                }});
                return false;">
              <fmt:message key="customer.creditCards.addCreditCard.label"/>
            </a>
          </li>
        </c:if>
       </ul>
    </div>

    <c:choose>
      <c:when test="${not empty wallet.creditCardMetaInfos}">
        <div class="creditCards">
          <%-- default credit cards --%>
          <c:forEach var="ccm" items="${wallet.creditCardMetaInfos}">
            <c:if test="${not empty ccm.value.params.defaultOptions}">
              <div class="customerInfo_creditCards atg_svc_iconD" title="<fmt:message key='creditCard.defaultCreditCard'/>" style="height:220px!important;width: 300px !important">
                <dspel:include src="/panels/customer/creditCardDisplay.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dspel:param name="mode" param="mode"/>
                  <dspel:param name="ccm" value="${ccm.value}"/>
                </dspel:include>
              </div>
            </c:if>
          </c:forEach>
          <%-- non-default credit cards --%>
          <c:forEach var="ccm" items="${wallet.creditCardMetaInfos}">
            <c:if test="${empty ccm.value.params.defaultOptions}">
              <div class="customerInfo_creditCards atg_svc_iconD" style="height:220px!important;width: 300px !important;">
                <dspel:include src="/panels/customer/creditCardDisplay.jsp" otherContext="${CSRConfigurator.truContextRoot}">
                  <dspel:param name="mode" param="mode"/>
                  <dspel:param name="ccm" value="${ccm.value}"/>
                </dspel:include>
              </div>
            </c:if>
          </c:forEach>
        </div>
      </c:when>
      <c:otherwise>
        <div class="emptyLabel">
          <fmt:message key="customer.creditCards.noCreditCards.label"/>
        </div>
      </c:otherwise>
    </c:choose>  
    </div>
    
    

    
    
    
    
    
    
    
    
    
    
    
    
  </dspel:layeredBundle>
  
   	    <dspel:form id="accountBillingForm" formid="accountBillingForm">
<%--     <dspel:input type="hidden" bean="CreateCreditCardFormHandler.creditCard.creditCardNumber" name="CardinalToken" id="CardinalToken" /> --%>
	    <dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.nameOnCard" name="nameOnCard" id="nameOnCard" />
	    <%-- <dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.creditCardToken" name="CardinalToken" id="CardinalToken" /> --%>
	    <dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.creditCardNumber" name="accountNumber" id="accountNumber" />
	    <dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.cardLength" name="cardLength" id="cardLength" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.expirationMonth" name="expirationMonth" id="expirationMonth" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.expirationYear" name="expirationYear" id="expirationYear" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.creditCardVerificationNumber" name="cardCode" id="cardCode" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.creditCardInfoMap.creditCardType" name="cardType" id="cardType" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.billingAddressNickName" name="nickName" id="nickName"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.selectedCardBillingAddress" name="selectedCardBillingAddress" id="selectedCardBillingAddress"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.firstName" name="firstName" id="firstName" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.lastName" name="lastName" id="lastName" />
	 	<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.address1" name="address1" id="address1" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.address2" name="address2" id="address2" />
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.city" name="city" id="city"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.state" name="state" id="state"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.nickName" name="nickName1" id="nickName1"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.postalCode" name="postalCode" id="postalCode"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.phoneNumber" name="phoneNumber" id="phoneNumber"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.addressValue.country" name="country" id="country"/>
		<dspel:input type="hidden" bean="CustomerProfileFormHandler.defaultCreditCard" name="defaultCreditCard" id="defaultCreditCard" />
        <dspel:input type="hidden" bean="CustomerProfileFormHandler.addCreditCard" value="submit" /> 
     </dspel:form>
</dspel:page>
<%-- @version $Id: //application/DCS-CSR-UI/version/11.1/src/web-apps/DCS-CSR-UI/panels/customer/creditCards.jsp#1 $$Change: 875535 $--%>
