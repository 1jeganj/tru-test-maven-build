<%--
Address Editor 

@addressId - Optional, the ID of an address to edit. If not supplied,
  creates a new address.

@version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/include/addresses/addressEditor.jsp#1 $$Change: 875535 $
@updated $DateTime: 2014/03/14 15:50:19 $$Author: jsiddaga $
--%>

<%@ include file="/include/top.jspf" %>
<dspel:page xml="true">

  <dspel:layeredBundle basename="atg.svc.agent.WebAppResources">
   <dsp:importbean var="CSRConfigurator" bean="/atg/commerce/custsvc/util/CSRConfigurator"/>
    <dsp:importbean bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler"/>
      <dspel:getvalueof var="nickNameValue" param="nickNameValue"/>
    <c:set var="addressBookFormHandlerPath"
      value="/atg/svc/agent/profile/AddressBookFormHandler"/>
    <dspel:importbean var="abfh" bean="${addressBookFormHandlerPath}"/>
    
    <dspel:importbean var="addressForm" bean="/atg/svc/agent/ui/fragments/AddressForm"/>
    
    	
     <c:set var="accountId" value="${param.addressId}"/>
     <input type="hidden" value="${accountId}" id="accountId"/>
     
    <svc-ui:frameworkPopupUrl var="url"
      value="/include/addresses/addressEditor.jsp"
      context="${CSRConfigurator.truContextRoot}"
      addressId="${param.addressId}"
      windowId="${windowId}"/>
    <svc-ui:frameworkPopupUrl var="successUrl"
      value="/include/addresses/addressEditor.jsp"
      context="${CSRConfigurator.truContextRoot}"
      addressId="${param.addressId}"
      success="true"
      windowId="${windowId}"/>
    <div class="atg_svc_popupPanel">
      <c:set var="formId" value="profileAddressEditorForm"/>
      <dspel:form action="#" method="post" id="${formId}"
        formid="${formId}">
        <h3 id="atg_commerce_csr_customerinfo_popNewAddress">
          <c:choose>
            <c:when test="${not empty param.addressId}">
              <dspel:setvalue bean="AddressBookFormHandler.addressId"
                value="${param.addressId}"/>
              <dspel:input type="hidden" value="${param.addressId}"
                priority="1000"
                bean="AddressBookFormHandler.addressId"/>
            </c:when>
            <c:otherwise>
            </c:otherwise>
          </c:choose>
        </h3>
        <div class="atg_commerce_csr_popupPanelCloseButton">
        </div>
        <div class="atg-csc-base-table atg-base-table-customer-address-add-form">
          <dspel:include src="/include/addresses/addressFormAM.jsp" otherContext="/truagent">
            <dspel:param name="formId" value="${formId}"/>
             <dspel:param name="nickNameValue" value="${nickNameValue}"/>
            <dspel:param name="addressBean" value="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.addressValue"/>
               <dspel:param name="addressBook" value="/atg/svc/agent/profile/AddressBookFormHandler.addressMetaInfo.address"/>
            <dspel:param name="submitButtonId" value="saveChoice"/>
          </dspel:include>
            <c:choose>
              <%-- when the address meta info has no param.nickname ... --%>
              <c:when test="${empty abfh.addressMetaInfo.nickname}">
                <%-- arbitrarily pick first nickname --%>
                <c:forEach var="n" end="1" items="${abfh.addressMetaInfo.nicknames}">
                <c:set var="nickname" value="${n}"/>
                </c:forEach>
              </c:when>
              <c:otherwise>
               <c:set var="nickname" value="${abfh.addressMetaInfo.nickname}"/>
              </c:otherwise>
            </c:choose>
           <dspel:input id="profileAddressEditorForm_nickName" type="hidden" bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.addressValue.nickName" size="40" maxlength="40" value="${nickname}">
           </dspel:input>
          
        </div>

        <div class="atg_svc_saveProfile">
      <%--     <c:forEach var="custEditor" items="${abfh.addressMetaInfo.params.managerEditorOptions}">
            <c:if test="${not empty custEditor.value.customEditorPage">
            <div>
              <dspel:include otherContext="/TRU-DCS-CSR"
                src="${custEditor.value.customEditorPage}">
                <dspel:param name="optionsKey" value="${custEditor.key}"/>
                <dspel:param name="options" value="${custEditor.value}"/>
                <dspel:param name="formHandlerPath" value="${addressBookFormHandlerPath}"/>
                <dspel:param name="formHandler" value="${abfh}"/>
              
              </dspel:include>
            </div>
            </c:if>
          </c:forEach>  --%>
          
             <c:forEach var="custEditor" items="${abfh.addressMetaInfo.params.managerEditorOptions}">
            <c:if test="${not empty custEditor.value.customEditorPage}">
            <div>
              <dspel:include otherContext="/TRU-DCS-CSR"
                src="${custEditor.value.customEditorPage}">
                <dspel:param name="optionsKey" value="${custEditor.key}"/>
                <dspel:param name="options" value="${custEditor.value}"/>
                <dspel:param name="formHandlerPath" value="${addressBookFormHandlerPath}"/>
                <dspel:param name="formHandler" value="${abfh}"/>
              
              </dspel:include>
            </div>
            </c:if>
          </c:forEach>
        </div>
        <div class="atg_svc_formActions">
<%--           <dspel:input type="hidden" value="${successUrl}" 
            bean="AddressBookFormHandler.successURL"/>
          <dspel:input type="hidden" value="${url}" 
            bean="AddressBookFormHandler.errorURL"/>
          <dspel:input type="hidden" value="--" priority="-100"
            id="addAddressAction"
            bean="AddressBookFormHandler.addAddress"/>
          <dspel:input type="hidden" value="--" priority="-100"
            id="updateAddressAction"
            bean="AddressBookFormHandler.updateAddress"/> --%>
              <dspel:input type="hidden" value="${successUrl}" 
            bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.commonSuccessURL"/>
          <dspel:input type="hidden" value="${url}" 
            bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.commonErrorURL"/>
          <dspel:input type="hidden" value="--" priority="-100"
            id="addAddressAction"
            bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.addShippingAddress"/>
           <dspel:input type="hidden" value="--" priority="-100"
            id="updateAddressAction"
            bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler.editShippingAddress"/>
 
            
            
          <div>
          <%--   <input id="saveChoice" name="saveChoice" value="<fmt:message key='address.save.label'/>" type="button"
                   onclick="if ( ${empty param.addressId} ) {
                              dojo.byId( 'addAddressAction' )['disabled'] = false;
                              dojo.byId( 'updateAddressAction' )['disabled'] = true;
                            }
                            else {
                              dojo.byId( 'addAddressAction' )['disabled'] = true;
                              dojo.byId( 'updateAddressAction' )['disabled'] = false;
                            }
                            atgSubmitPopup({url: '${url}', 
                              form: dojo.byId('${formId}'),
                              popup: getEnclosingPopup('addrEditNickname')});
                            return false;"/> --%>
                            
                 <input id="saveChoice" name="saveChoice" value="<fmt:message key='address.save.label'/>" type="button"
                   onclick="getOrderId();return false;"  dojoType="atg.widget.validation.SubmitButton"/>
            <input value="<fmt:message key='address.cancel.label'/>" 
              type="button" id="cancelChoice"
              onClick="hidePopupWithResults('addressPopup', {result:'cancel'}); 
              return false;"/>
          </div>
        </div>
      </dspel:form>
    </div>
    <div style="display:none" id="hiddenMyAccountAddSuggestedAddress"></div>
      <c:url var="resultsUrl" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/results.jsp">
       
        </c:url>      
         <c:url var="addressDoctorURL" context="${CSRConfigurator.truContextRoot}"
               value="/panels/order/shipping/includes/billingSuggestions.jsp">
          <c:param name="accId" value="${param.addressId}"/>
             <c:param name="pageName" value="account"/>
          <c:param name="${stateHolder.windowIdParameterName}" value="${windowId}"/>
          <%--   <c:param name="sp" value="${shippingGroup}"/> --%>
        </c:url>   
    <c:if test="${param.success}">
      <script type="text/javascript">
        hidePopupWithResults( 'addressPopup', {result : 'save'});
      </script>
    </c:if>
    
    <script type="text/javascript">
    var ${formId}Validate = function () {
        var disable = false;
        <c:if test="${!empty isDisableSubmit}">disable = ${isDisableSubmit}();</c:if>
        <c:if test="${!empty validateIf}">if (${validateIf}) {</c:if>
         // if (!dijit.byId("${formId}_nickName").isValid()) disable = true;
          if (!dijit.byId("${formId}_firstName").isValid()) disable = true;
          if (!dijit.byId("${formId}_lastName").isValid()) disable = true;
          if (!dijit.byId("${formId}_address1").isValid()) disable = true;
          if (!dijit.byId("${formId}_city").isValid()) disable = true;
          if (!dijit.byId("${formId}_state").isValid()) disable = true;
          if (!dijit.byId("${formId}_postalCode").isValid()) disable = true;
          if (!dijit.byId("${formId}_country").isValid()) disable = true;
          if (!dijit.byId("${formId}_phoneNumber").isValid()) disable = true;
        <c:if test="${!empty validateIf}">}</c:if>
        dojo.byId("${formId}").dojo.byId("saveChoice").disabled = disable;
      };
      _container_.onLoadDeferred.addCallback(function () {
          ${formId}Validate();
          ${formId}ChangeCountry();
          atg.service.form.watchInputs("${formId}", ${formId}Validate);
        });
        _container_.onUnloadDeferred.addCallback(function () {
          atg.service.form.unWatchInputs('${formId}');
        });
      _container_.onLoadDeferred.addCallback(function () {
        document.getElementById("saveChoice").disabled=true;
      });
    </script>
    
        <script type="text/javascript">
        $(document).on('keypress','#profileAddressEditorForm_nickName',function(e){
		        var regex = new RegExp("^[a-zA-Z0-9]");
		        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		        if (regex.test(str)) {
		            return true;
		        }
		
		        e.preventDefault();
		        return false;
        });
        function getOrderId(){
        		if($('.error').length)
        			{
        				$('.error').remove();
        			}
			 var country = getCountry();
			 var address1 = $('#profileAddressEditorForm_address1').val();
			 var address2 = $('#profileAddressEditorForm_address2').val();
			 var city = $('#profileAddressEditorForm_city').val();
			 var state1 = $('#profileAddressEditorForm_state').val();
			 var postalCode = $('#profileAddressEditorForm_postalCode').val();
			 var phoneNumber = $('#profileAddressEditorForm_phoneNumber').val();
			 var address2 = $('#profileAddressEditorForm_address2').val();
			 var nickName = $('#profileAddressEditorForm_nickName').val();
			 var pageName="account";
			 var selectedAsDefaultShipping = $("#defaultShippingCheckBox_hidden").val();
			 var state = state1.split('-')[0];
			 var inputError = false;
			 $("#addressPopup").find('input[type="text"]').not(':hidden').not('#profileAddressEditorForm_address2,#profileAddressEditorForm_nickName').filter(function(){
				 var value = $(this).val();
				if($.trim(value) == '')
					{
						inputError = true;
					}
				 
			 });
			 if(inputError)
				 {
				 	return false;
				 }
			 if(postalCode.length < 5)
				{
		     		
					$('#profileAddressEditorForm_postalCode').closest('tr').append('<td class="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid postalcode</span></td>');
					return false;
				}
				else if(phoneNumber.length < 10)
				{
					$('#profileAddressEditorForm_phoneNumber').closest('tr').append('<td class="error"><span style="color:red;position: absolute;margin-top: -8px;font-weight:bold;">Invalid phone number</span></td>');
					return false;
				}
			 if(country == 'US'){
				 
			dojo.xhrPost({
						url : "${resultsUrl}?nickName="+nickName+"&address1="+address1+"&address2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&phoneNumber="+phoneNumber+"&defaultShippingAdd="+selectedAsDefaultShipping+"&windowid="+window.windowId,
						encoding : "utf-8",
						preventCache : true,
						handle : function(_362, _363) {
							var response = dojo.trim(_362);
							dojo.query("#hiddenMyAccountAddSuggestedAddress")[0].innerHTML = response;
							var derivedStatus = dojo.query("#hiddenMyAccountAddSuggestedAddress #derivedStatus")[0].innerHTML;
							var addressRecognized = dojo.query("#hiddenMyAccountAddSuggestedAddress #addressDoctorVO1")[0].innerHTML;
							if(derivedStatus == "NO" && addressRecognized == "true"){
								
								
								
								var data =  dojo.query("#hiddenMyAccountAddSuggestedAddress #firstSuggestedAddress")[0].innerHTML
								var form =  document.getElementById('profileAddressEditorForm'); 
						           
							 		$("#profileAddressEditorForm_country").val('US');
							 		$("#profileAddressEditorForm_address1").val(dojo.query("#hiddenMyAccountAddSuggestedAddress #suggestedAddress1-1")[0].value);
							 		$("#profileAddressEditorForm_address2").val(dojo.query("#hiddenMyAccountAddSuggestedAddress #suggestedAddress2-1")[0].value);
							 		$("#profileAddressEditorForm_city").val(dojo.query("#hiddenMyAccountAddSuggestedAddress #suggestedCity-1")[0].value);
							 		$("#profileAddressEditorForm_state").val(dojo.query("#hiddenMyAccountAddSuggestedAddress #suggestedState-1")[0].value);
							 		$("#profileAddressEditorForm_postalCode").val(dojo.query("#hiddenMyAccountAddSuggestedAddress #suggestedPostalCode-1")[0].value);
						            
							 			if (${empty param.addressId}) {
		                              dojo.byId( 'addAddressAction' )['disabled'] = false;
		                              dojo.byId( 'updateAddressAction' )['disabled'] = true;
		                            }
		                            else {
		                              dojo.byId( 'addAddressAction' )['disabled'] = true;
		                              dojo.byId( 'updateAddressAction' )['disabled'] = false;
		                            }
							 		
							 		atgSubmitPopup({url: '${url}',form: dojo.byId('profileAddressEditorForm'),
							 	        popup: getEnclosingPopup('profileAddressEditorForm_nickName')});
							 		hidePopupWithResults('csrAddressDoctorFloatingPane', {result : 'cancel'});
						            
							}
							
							else if(derivedStatus == "YES"){
							atg.commerce.csr.common.showPopupWithReturn({
										popupPaneId : 'csrAddressDoctorFloatingPane',
										url : "${addressDoctorURL}&nickName="+nickName+"&adre1="+address1+"&adre2="+address2+"&city="+city+"&state="+state+"&postalCode="+postalCode+"&defaultShippingAdd="+selectedAsDefaultShipping,
										onClose : function(args) {
											if (args.result == 'ok') {
												atgSubmitAction({
													panelStack : [
															'cmcShippingAddressPS',
															'globalPanels' ],
													form : document
															.getElementById('transformForm')
												});
											}
										}
									});
							hidePopupWithResults('addressPopup', {result : 'cancel'});
							}
							else{
							
								if (${empty param.addressId}) {
		                              dojo.byId( 'addAddressAction' )['disabled'] = false;
		                              dojo.byId( 'updateAddressAction' )['disabled'] = true;
		                            }
		                            else {
		                            
		                              dojo.byId( 'addAddressAction' )['disabled'] = true;
		                              dojo.byId( 'updateAddressAction' )['disabled'] = false;
		                            }
								atgSubmitPopup({url: '${url}', 
				                              form: dojo.byId('profileAddressEditorForm'),
				                              popup: getEnclosingPopup('profileAddressEditorForm_nickName')});
								hidePopupWithResults('addressPopup', {result : 'cancel'});
								return false;
							}
							if (document
									.getElementById("inStorePickupResultsss")) {
								document
										.getElementById("inStorePickupResultsss").innerHTML = _362;
							}

						},
						mimetype : "text/html"
					}); 
			 }else{
				 var accountId = document.getElementById('accountId').value;
				if(dojo.string.trim(accountId) == ''){
					dojo.byId( 'addAddressAction' )['disabled'] = false;
                dojo.byId( 'updateAddressAction' )['disabled'] = true;
              }
              else {
                dojo.byId( 'addAddressAction' )['disabled'] = true;
                dojo.byId( 'updateAddressAction' )['disabled'] = false;
              }
	
				 
				 atgSubmitPopup({url: '${url}', 
                     form: document.getElementById('profileAddressEditorForm'),
                     popup: getEnclosingPopup('profileAddressEditorForm_nickName')});
					 hidePopupWithResults('profileAddressEditorForm_nickName', {result : 'cancel'});
					 atgNavigate({panelStack:"customerPanels", queryParams: { init : 'true' }});return false;
             
				
				 
			 }
		}
        $(document).on("click",".defaultAdd_checkBox",function(){
        	if($(this).is(":checked"))
        		{
        			$(this).val("true");
        		}
        	else
        		{
        			$(this).val("false");
        		}
        })
        
        $(document).on("keypress","#profileAddressEditorForm_phoneNumber,#profileAddressEditorForm_phoneNumber",function(evt){
    	      evt = (evt) ? evt : window.event;
    	      var charCode = (evt.which) ? evt.which : evt.keyCode;
    	      if(charCode == 45)return true;
    	      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    	          return false;
    	      }
    	      return true;
          });
        $(document).on("click","#defaultShippingCheckBox",function(){
        	if($(this).is(':checked'))
        		{
        			$("#defaultShippingCheckBox_hidden").val('true');
        		}
        	else
        		{
        			$("#defaultShippingCheckBox_hidden").val('false');
        		}
        })
        function getCountry()
        {
        	var countrySelected = $("#profileAddressEditorForm_country").val();
        	var country;
        	if(countrySelected == 'United States')
        		{
        			country ='US';
        		}
        	else
        		{
        			country = countrySelected;
        		}
        	return country;
        }
        
    </script>
    
  </dspel:layeredBundle>
</dspel:page>
<%-- @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/include/addresses/addressEditor.jsp#1 $$Change: 875535 $--%>