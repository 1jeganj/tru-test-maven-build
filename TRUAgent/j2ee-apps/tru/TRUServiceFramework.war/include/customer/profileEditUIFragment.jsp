<%--
 This UI fragment defines the Edit Profile Information Panel that is shipped with the product, 
 but may be replaced by a customer at their discretion. Customers need simply point the configuration file
 to the new JSP snippet for this to take effect.
 @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/include/customer/profileEditUIFragment.jsp#1 $
 @updated $DateTime: 2014/03/14 15:50:19 $
--%>
<%@ include file="/include/top.jspf" %>
<dspel:page xml="true">
  <dspel:layeredBundle basename="atg.svc.agent.WebAppResources">
  <dspel:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <dspel:importbean var="CustomerProfileFormHandler" bean="/atg/svc/agent/ui/formhandlers/CustomerProfileFormHandler"/>
  <dspel:importbean var="agentUIConfig" bean="/atg/svc/agent/ui/AgentUIConfiguration"/>
  <dspel:importbean bean="/atg/core/i18n/LocaleTools"/>
  <dspel:importbean var="profile" bean="/atg/userprofiling/ActiveCustomerProfile" />
  <fmt:message key="customer.emailAddress.invalid" var="emailAddressInvalid"/>
  <fmt:message key="customer.emailAddress.required" var="emailAddressRequired"/>
  <fmt:message key="customer.firstName.required" var="firstNameRequired"/>
  <fmt:message key="customer.lastName.required" var="lastNameRequired"/>
  <fmt:message key="customer.loginName.required" var="loginNameRequired"/>
  <fmt:message key="customer.birthDate.tooltip" var="birthDateTooltip"/>
  <script type="text/javascript">
    dojo.require("dijit.form.DateTextBox");
  </script>
     <div class="atg_svc_coreCustomerInfo">
    <div class="coreCustomerInfo">

      <div class="atg-csc-base-table atg-base-table-customer-create-form">
        <div class="atg-csc-base-table-row">
          <span class="atg_svc_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label for="firstName" class="atg_messaging_requiredIndicator" id="firstNameAlert">
          <fmt:message key="customer.firstName.label"/></label>
          <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <dspel:input id="firstName" name="firstName" iclass="" type="text" size="25"  maxlength="40"
                        bean="CustomerProfileFormHandler.value.firstName">
              <dspel:tagAttribute name="tabindex" value="1"/>
              <dspel:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox"/>
              <dspel:tagAttribute name="required" value="true"/>
              <dspel:tagAttribute name="trim" value="true"/>
              <dspel:tagAttribute name="missingMessage" value="${firstNameRequired}"/>
              <dspel:tagAttribute name="inlineIndicator" value="firstNameAlert"/>
            </dspel:input>
          </div>
        </div>

        <div class="atg-csc-base-table-row">
          <span class="atg_svc_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label for="lastName" class="atg_messaging_requiredIndicator" id="lastNameAlert">
            <fmt:message key="customer.lastName.label"/></label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <dspel:input id="lastName" name="lastName" type="text" size="25" iclass="text" maxlength="40"
                                   bean="CustomerProfileFormHandler.value.lastName">
              <dspel:tagAttribute name="tabindex" value="2"/>
              <dspel:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox"/>
              <dspel:tagAttribute name="required" value="true"/>
              <dspel:tagAttribute name="trim" value="true"/>
              <dspel:tagAttribute name="missingMessage" value="${lastNameRequired}"/>
              <dspel:tagAttribute name="inlineIndicator" value="lastNameAlert"/>
            </dspel:input>
          </div>

        </div>
        <div class="atg-csc-base-table-row">
          <span class="atg_svc_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label for="email" class="atg_messaging_requiredIndicator" id="emailAlert">
            <fmt:message key="customer.email.label"/></label>
            <span class="requiredStar">*</span>
          </span>
          <div class="atg-csc-base-table-cell">
            <dspel:input id="email"  name="email" type="text" size="25" iclass="text" maxlength="50"
                                   bean="CustomerProfileFormHandler.value.email" onchange="changeEmail()";>
              <dspel:tagAttribute name="tabindex" value="3"/>
              <dspel:tagAttribute name="dojoType" value="atg.widget.form.ValidationTextBox"/>
              <dspel:tagAttribute name="validator" value="dojox.validate.isEmailAddress"/>
              <dspel:tagAttribute name="required" value="true"/>
              <dspel:tagAttribute name="trim" value="true"/>
              <dspel:tagAttribute name="missingMessage" value="${emailAddressRequired}"/>
              <dspel:tagAttribute name="invalidMessage" value="${emailAddressInvalid}"/>
              <dspel:tagAttribute name="inlineIndicator" value="emailAlert"/>
            </dspel:input>
            
              <dspel:input id="login"  name="login" type="hidden" iclass="text" maxlength="50"
                                    bean="CustomerProfileFormHandler.value.login">
            </dspel:input>
          </div>
        </div>
        <div class="atg-csc-base-table-row">
          <span class="atg_svc_fieldTitle atg-csc-base-table-cell atg-base-table-customer-create-first-label">
            <label for="password"><fmt:message key="customer.view.password.label"/></label>
          </span>
          <span class="plainText atg-csc-base-table-cell">
            <a href="#" onclick="emailNewPassword('Reset Password Email sent to Customer Email Address')"><fmt:message key="customer.view.password.link"/></a>
          </span>
        </div>
            
		  </div>
 
       </div>
      </div>
      </div>
    
  <script type="text/javascript">
  var customerCreateFormValidate = function () {
	    var disable = false;
	    
	    if (validateForCreate()) {
	      dijit.byId("firstName").required = true;
	      dijit.byId("email").required = true;
	      dijit.byId("lastName").required = true;
	      dijit.byId("login").required = true;
	  
	      dojo.style("firstNameRequiredStar", "visibility", "visible");
	      dojo.style("emailRequiredStar", "visibility", "visible");
	      dojo.style("lastNameRequiredStar", "visibility", "visible");
	      dojo.style("loginRequiredStar", "visibility", "visible");
	      
	      if (!dijit.byId("firstName").isValid()) disable = true;
	      if (!dijit.byId("email").isValid()) disable = true;
	      if (!dijit.byId("lastName").isValid()) disable = true;
	      if (!dijit.byId("login").isValid()) disable = true;
	    }else{
	      dijit.byId("firstName").required = false;
	      dijit.byId("email").required = false;
	      dijit.byId("lastName").required = false;
	      dijit.byId("login").required = false;
	      
	      dojo.style("firstNameRequiredStar", "visibility", "hidden");
	      dojo.style("emailRequiredStar", "visibility", "hidden");
	      dojo.style("lastNameRequiredStar", "visibility", "hidden");
	      dojo.style("loginRequiredStar", "visibility", "hidden");
	      
	      dijit.byId("firstName").validate();
	      dijit.byId("email").validate();
	      dijit.byId("lastName").validate();
	      dijit.byId("login").validate();
	    }
	  
	    dojo.byId("customerCreateForm").update.disabled = disable;
	  
	  }
   function changeEmail(){
	   var email = document.getElementById('email').value;
	   document.getElementById('email').value = email.toLowerCase();
	   document.getElementById('login').value = email.toLowerCase();
	   
   }  
</script> 
  </dspel:layeredBundle>
</dspel:page>
<%-- @version $Id: //application/service-UI/version/11.1/framework/Agent/src/web-apps/ServiceFramework/include/customer/profileEditUIFragment.jsp#1 $$Change: 875535 $--%>
