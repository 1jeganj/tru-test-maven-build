package atg.commerce.csr.order;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.CreditCard;
import atg.droplet.DropletFormException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUExtendableCreditCardTools;
import com.tru.commerce.csr.util.TRUCSCConstants;

/**
 * The Class TRUCSRUpdateCreditCardFormHandler extends CSRUpdateCreditCardFormHandler.
 */
public class TRUCSRUpdateCreditCardFormHandler extends CSRUpdateCreditCardFormHandler {
 
 /* 
  * validate Credit Cards
  * 
  *  * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @param pCreditCard
	 *            the creditCard
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
  */
	
 @Override
 
public void validateCreditCard(CreditCard pCreditCard,
		DynamoHttpServletRequest pRequest,
		DynamoHttpServletResponse pResponse) throws ServletException,
		IOException {
	  int statusCode = ((TRUExtendableCreditCardTools)getCreditCardTools()).verifyCreditCard(pCreditCard,pCreditCard.getCreditCardNumber(),pCreditCard.getCreditCardType());
	    if (statusCode == 0) {
	      return;
	    }
	    String msg = getCreditCardTools().getStatusCodeMessage(statusCode, getUserLocale(pRequest, pResponse));
	    String propertyPath = generatePropertyPath(TRUCSCConstants.VALIDATE_CREDIT_CARD);
	    addFormException(new DropletFormException(msg, propertyPath, new Integer(statusCode).toString()));
	
}
}
