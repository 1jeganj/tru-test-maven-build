package atg.commerce.csr.order.purchase;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.CommerceIdentifier;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupCommerceItemRelationship;
import atg.commerce.order.PaymentGroupOrderRelationship;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.PaymentGroupRelationshipContainer;
import atg.commerce.order.PaymentGroupShippingGroupRelationship;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.purchase.CommerceIdentifierPaymentInfo;
import atg.commerce.order.purchase.CommerceItemPaymentInfo;
import atg.commerce.order.purchase.OrderPaymentInfo;
import atg.commerce.order.purchase.PaymentGroupDroplet;
import atg.commerce.order.purchase.ShippingGroupPaymentInfo;
import atg.commerce.order.purchase.TaxPaymentInfo;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.csr.util.TRUCSCConstants;

public class TRUCSRPaymentGroupDroplet extends PaymentGroupDroplet {

	/**
	 * service method overridden to is used to set credit card payment method to ZERO <br>
	 */
	@Override
	protected void initializeOrderPayment(Order pOrder) {
		
		super.initializeOrderPayment(pOrder);
		List<CommerceIdentifierPaymentInfo> cipiList=getCommerceIdentifierPaymentInfoContainer().getCommerceIdentifierPaymentInfos(pOrder.getId());
		
		for (Iterator iterator = cipiList.iterator(); iterator.hasNext();) {
			CommerceIdentifierPaymentInfo commerceIdentifierPaymentInfo = (CommerceIdentifierPaymentInfo) iterator
					.next();
			String paymentMehtod=commerceIdentifierPaymentInfo.getPaymentMethod();
			/*if(paymentMehtod==null){*/
				commerceIdentifierPaymentInfo.setAmount(TRUCSCConstants.DOUBLE_ZERO);
		/*	}*/
			
		}
	}
	
	/**
	 * Method 'initializeCommerceIdentifierPaymentInfos' is used to set relationships <br>
	 * 
	 */
	public void initializeCommerceIdentifierPaymentInfos(PaymentGroupRelationshipContainer pContainer)
	  {
	    List paymentGroupRelationships = pContainer.getPaymentGroupRelationships();
	    if (paymentGroupRelationships != null) {
	      Iterator relIter = paymentGroupRelationships.iterator();

	      while (relIter.hasNext()) {
	        PaymentGroupRelationship rel = (PaymentGroupRelationship)relIter.next();

	        PaymentGroup paymentGroup = rel.getPaymentGroup();
	        if (getPaymentGroupManager().paymentGroupIsModifiable(paymentGroup))
	        {
	          CommerceIdentifierPaymentInfo cipi = null;
	          if (rel instanceof PaymentGroupOrderRelationship) {
	            int relationshipType = ((PaymentGroupOrderRelationship)rel).getRelationshipType();
	            if ((relationshipType == 403) || (relationshipType == 405))
	            {
	              cipi = new TaxPaymentInfo();
	            }
	            else cipi = new OrderPaymentInfo();
	          }
	          else if (rel instanceof PaymentGroupCommerceItemRelationship) {
	            cipi = new CommerceItemPaymentInfo();
	          } else if (rel instanceof PaymentGroupShippingGroupRelationship) {
	            cipi = new ShippingGroupPaymentInfo();
	          }
	          if (cipi == null) {
	            if (isLoggingError()) logError("Could not identify relationship type, so cipi is null.");
	          }
	          else {
	            cipi.setCommerceIdentifier((CommerceIdentifier)pContainer);
	            cipi.setRelationshipType(RelationshipTypes.typeToString(rel.getRelationshipType()));

	            String paymentGroupName = getPaymentGroupName(paymentGroup);
	            if (paymentGroupName == null) {
	              paymentGroupName = getNewPaymentGroupName(paymentGroup);
	            }
	            getPaymentGroupMapContainer().addPaymentGroup(paymentGroupName, paymentGroup);
	            cipi.setPaymentMethod(paymentGroupName);
	            cipi.setAmount(0.0d);
	            if (rel instanceof PaymentGroupCommerceItemRelationship) {
	              cipi.setQuantity(((PaymentGroupCommerceItemRelationship)rel).getQuantity());
	              cipi.setQuantityWithFraction(((PaymentGroupCommerceItemRelationship)rel).getQuantityWithFraction());
	            }
	            getCommerceIdentifierPaymentInfoContainer().addCommerceIdentifierPaymentInfo(((CommerceIdentifier)pContainer).getId(), cipi);
	          }
	        }
	      }
	    }
	  }
	
	
	/**
	 * service method overridden to is used to set credit card payment method to ZERO <br>
	 * 
	 * @param pRequest
	 *            - Holds the DynamoHttpServletRequest.
	 * @param pResponse
	 *            - Holds the DynamoHttpServletResponse.
	 * @throws ServletException
	 *             - ServletException if an error occurs.
	 * @throws IOException
	 *             - IOException if an error occurs.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
	throws ServletException, IOException 
	{
		// calling super method
		super.service(pRequest, pResponse);
		List<CommerceIdentifierPaymentInfo> cipiList=getCommerceIdentifierPaymentInfoContainer().getCommerceIdentifierPaymentInfos(getOrder().getId());
		for (Iterator iterator = cipiList.iterator(); iterator.hasNext();) {
			CommerceIdentifierPaymentInfo commerceIdentifierPaymentInfo = (CommerceIdentifierPaymentInfo) iterator.next();
				commerceIdentifierPaymentInfo.setAmount(TRUCSCConstants.DOUBLE_ZERO);
		}
	}
}
