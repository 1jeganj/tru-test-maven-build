package atg.commerce.csr.order;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.Relationship;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.CommerceIdentifierPaymentInfo;
import atg.commerce.order.purchase.CommerceIdentifierPaymentInfoContainer;
import atg.commerce.pricing.PricingConstants;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.order.TRUCSROrderManager;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.purchase.TRUBillingProcessHelper;
import com.tru.commerce.order.purchase.TRUPaymentGroupContainerService;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;

/**
 * This class extends ATG OOTB CSRPaymentGroupFormHandler and used
 * for managing the payment group related operation.
 *
 * @version 1.0
 * @author Professional Access
 *
 *
 */
public class TRUCSRPaymentGroupFormHandler extends CSRPaymentGroupFormHandler {

	/**
	 * property to hold OrderWithTotalZero flag.
	 */
	private boolean mOrderWithTotalZero;
	
	/** property: Reference to the BillingProcessHelper component. */
	private TRUBillingProcessHelper mBillingHelper;

	/**
	 *  Property to hold reward number of profile.
	 */
	private String mRewardNumber;

	/** The m email. */
	private String mEmail;

	/**
	 *  Property to hold ValidationException.
	 */
	private ValidationExceptions mVe = new ValidationExceptions();

	/**
	 *  Property to hold mErrorHandler.
	 */
	private IErrorHandler mErrorHandler;
	
	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();
	
	
	/** property to hold BillingAddressNickName.*/
	private String mBillingAddressNickName;
	
	/**
	 * Gets the billing address nick name.
	 *
	 * @return mBillingAddressNickName.
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}
	
	/**
	 * Sets the billing address nick name.
	 *
	 * @param pBillingAddressNickName parameter.
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}
	
	/**
	 * Gets the address input fields.
	 *
	 * @return the mAddressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * Sets the address input fields.
	 *
	 * @param pAddressInputFields the addressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		
		this.mAddressInputFields = pAddressInputFields;
	}

	/**
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler
	 *            the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		this.mErrorHandler = pErrorHandler;
	}
	
	
	
	/**
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * @param pBillingHelper
	 *            the billingHelper to set.
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}
	
	/**
	 * Gets the  email.
	 *
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * Sets the m email.
	 *
	 * @param pEmail
	 *            the email to set
	 */
	public void setEmail(String pEmail) {
		this.mEmail = pEmail;
	}

	/**
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}

	/**
	 * @param pRewardNumber
	 *            the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		this.mRewardNumber = pRewardNumber;
	}
	
	/**
	 * @return the mOrderWithTotalZero
	 */
	public boolean isOrderWithTotalZero() {
		return mOrderWithTotalZero;
	}

	/**
	 * @param pOrderWithTotalZero the mOrderWithTotalZero to set
	 */
	public void setOrderWithTotalZero(boolean pOrderWithTotalZero) {
		this.mOrderWithTotalZero = pOrderWithTotalZero;
	}
	
	/**
	 * Check for in store payment.
	 *
	 * @return true, if successful
	 */
	private boolean checkForInStorePayment(){
		if (isLoggingDebug()) {
        	logDebug("Entering to class:[TRUCSRPaymentGroupFormHandler] method:[checkForInStorePayment]");
		}
		final TRUOrderImpl currentOrder = ((TRUOrderImpl) getOrder());
	    try {
	    if (currentOrder != null) {
			List<PaymentGroup> paymentGroups = currentOrder.getPaymentGroups();
			List<ShippingGroup> shippingGroups = currentOrder.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if(shippingGroup instanceof TRUInStorePickupShippingGroup )
				{
						for (PaymentGroup paymentGroup : paymentGroups) {
							if(TRUCSCConstants.IN_STORE_PAYMENT.equalsIgnoreCase(paymentGroup.getPaymentMethod()))
							{
							getPaymentGroupManager().removePaymentGroupFromOrder(currentOrder, paymentGroup.getId());
							((TRUPaymentGroupContainerService)getPaymentGroupMapContainer()) .removePaymentGroup(paymentGroup.getId());
							((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(
			                           PricingConstants.OP_REPRICE_ORDER_TOTAL,
			                           getOrder(),
			                           getUserPricingModels(),
			                           getUserLocale(),
			                           getProfile(),
			                           createRepriceParameterMap(),
			                           this);
							((TRUCSROrderManager) getOrderManager()).updateOrder(currentOrder);
							addFormException(new DropletException(TRUCSCConstants.PAY_IN_STORE_ERROR_MSG));
							return false;
							}
						}
				}
			}
	    }
	    }catch (CommerceException e) {
	    	if (isLoggingError()) {
				logError("CommerceException in @Class::TRUCSRPaymentGroupFormHandler::@method::checkForInStorePayment()", e);
			}
		}
	    catch (RunProcessException rpE) {
            if (isLoggingError()) {
                 logError("RunProcessException occured during checkForInStorePayment for order Id : " + currentOrder.getId(), rpE);
          }
	    }
	    if (isLoggingDebug()) {
        	logDebug("Exit from class:[TRUCSRPaymentGroupFormHandler] method:[checkForInStorePayment]");
		}
		return true;
	}

	/**
	 * This method is for validating the payment group.
	 *
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @throws ServletException
	 *             if an error occurred while processing the servlet request
	 * @throws IOException
	 *             if an error occurred while reading or writing the servlet
	 *
	 */
	  public void preApplyPaymentGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			    throws ServletException, IOException {
		    if (isLoggingDebug()){
 			logDebug("Start: TRUCSRPaymentGroupFormHandler.preApplyPaymentGroups()");
	 		}
		    double cipiAmount=0.00d;
		    Map<String, CommerceIdentifierPaymentInfo> cipiContainer=getCommerceIdentifierPaymentInfoContainer().getCommerceIdentifierPaymentInfoMap();
			Set<String> keySet = cipiContainer.keySet();
			String keyValueRemove = null;
			List<CommerceIdentifierPaymentInfo> cipi=(List<CommerceIdentifierPaymentInfo>) cipiContainer.get(((TRUOrderImpl) getOrder()).getId());
			
			for (Iterator iterator = cipi.iterator(); iterator.hasNext();) {
				CommerceIdentifierPaymentInfo commerceIdentifierPaymentInfo = (CommerceIdentifierPaymentInfo) iterator
						.next();
				cipiAmount=cipiAmount+commerceIdentifierPaymentInfo.getAmount();
				
			}
		/*	boolean flag= false;*/
		/*	for (Iterator iterator = keySet.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				List<CommerceIdentifierPaymentInfo cipi = cipiContainer.get(string);
				cipiAmount=cipiAmount+cipi.getAmount();
			}*/
			double orderAmount = ((TRUOrderImpl) getOrder()).getPriceInfo().getTotal();
			DecimalFormat df = new DecimalFormat("#.##");      
			orderAmount = Double.valueOf(df.format(orderAmount));
			cipiAmount=Double.valueOf(df.format(cipiAmount));
			double diff=cipiAmount-orderAmount;
			logDebug("Value of cipiAmount---------------"+ cipiAmount);
			logDebug("Value of orderAmount---------------"+ orderAmount);
			logDebug("Difference of cipiAmount-orderAmount---------------"+ diff);
			if(cipiAmount-orderAmount!=0.00d){
				addFormException(new DropletException("Amount Entered does not match with order total. Please enter correct amount and use only one credit card for checkout"));
				return;
			}
		    if(checkForInStorePayment()){
		     if (StringUtils.isNotBlank(getRewardNumber())
		    		 &&  !validateDenaliAccountNum(getRewardNumber()) ){
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);   
				getErrorHandler().processException(mVe, this);
				return;
			}
		     ((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
		     ((TRUOrderImpl) getOrder()).setEmail(getEmail());
		     super.preApplyPaymentGroups(pRequest, pResponse);
		    }
				if (isLoggingDebug()){
		 			logDebug("End: TRUCSRPaymentGroupFormHandler.preApplyPaymentGroups()");
		 		} 
			}
	  
		/**
		 * This method is for validating the reward membershipCard number.
		 *
		 * @param pRewardNumber
		 *            rewardNumber
		 * @return boolean
		 *            
		 */
		public boolean validateDenaliAccountNum(String pRewardNumber)
	    {
		      if (isLoggingDebug()){
		    	  logDebug("Start: TRUCSRPaymentGroupFormHandler.validateDenaliAccountNum()");
	 		   }
	           boolean isValid=false;
	           int charPos = TRUConstants.ZERO;
	           int evensum = TRUConstants.ZERO;
	           int oddsum = TRUConstants.ZERO;
	           int csum = TRUConstants.ZERO;
	           int check = TRUConstants.ZERO;
	           String rewardNumberSubStr = TRUConstants.EMPTY;
	           
	           if(pRewardNumber.length()==TRUConstants.THIRTEEN
	        		   && pRewardNumber.substring(TRUConstants.ZERO,TRUConstants.TWO).equalsIgnoreCase(TRUConstants.TWENTY_ONE))
	           { 
	                  csum = Integer.parseInt(TRUConstants.DOUBLE_QUOTE + 
	                		  				  pRewardNumber.charAt(TRUConstants.TWELVE));
	                  if (pRewardNumber != null && pRewardNumber.length() == TRUConstants.THIRTEEN) {
	                	  rewardNumberSubStr = pRewardNumber.substring(TRUConstants.ZERO, TRUConstants.TWELVE);
	                  }
	                  if (rewardNumberSubStr == null || rewardNumberSubStr.length()!= TRUConstants.TWELVE) {
	                        return false;
	                  }
	                  
	                  for (int i = rewardNumberSubStr.length() -TRUConstants.SIZE_ONE; i >= TRUConstants.ZERO; i--) {
	                        charPos = ((rewardNumberSubStr.length() - i)-TRUConstants.SIZE_ONE);
	                        if ((rewardNumberSubStr.length() - i) % TRUConstants.TWO == TRUConstants.ZERO) {
	                               evensum = evensum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE+rewardNumberSubStr.charAt(charPos));
	                        } else {
	                               oddsum = oddsum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE+rewardNumberSubStr.charAt(charPos));
	                        }
	                  }
	                  check = TRUConstants.TEN - ((evensum * TRUConstants.THREE + oddsum) % TRUConstants.TEN);
	                  if (check >= TRUConstants.TEN)
	                        {
	                               check = TRUConstants.ZERO;
	                        }
	                        isValid = (csum == check);
	           }
	   		  if (isLoggingDebug()) {
	 			logDebug("End: TRUCSRPaymentGroupFormHandler.validateDenaliAccountNum()");
	 		  }
	           return isValid;
	    }

	/**
	 * Apply payment groups.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see atg.commerce.order.purchase.PaymentGroupFormHandler#applyPaymentGroups
	 * (atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	protected void applyPaymentGroups(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		PaymentGroupManager pgm = getPaymentGroupManager();
		Order order = getShoppingCart().getCurrent();
		List<PaymentGroup> pgs=order.getPaymentGroups();
		CommerceIdentifierPaymentInfoContainer container = getCommerceIdentifierPaymentInfoContainer();
		List commerceIdentifierPaymentInfos = container
				.getAllCommerceIdentifierPaymentInfos();
		try {
			List nonModifiablePaymentGroups = pgm
					.getNonModifiablePaymentGroups(order);
			
			pgm.removeAllPaymentGroupsFromOrder(order,
					nonModifiablePaymentGroups);
			
			TRUCSROrderManager orderManager = (TRUCSROrderManager) getCSRAgentTools() .getOrderManager();
			
			@SuppressWarnings("unused")
			String billingAddressNickName = getBillingAddressNickName();
			if(true) {
				ContactInfo billingAddress  = new ContactInfo();
				OrderTools.copyAddress(getAddressInputFields(), billingAddress);
				//Adding the billing address to the credit card. 
				((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
				String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
				((TRUOrderImpl)getOrder()).setBillingAddressNickName(generatedUniqueNickName);
				/*final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
				final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
				final Address address = hgsg.getShippingAddress();	    
				OrderTools.copyAddress(getAddressInputFields(), address);*/
				/*hgsg.setBillingAddress(Boolean.TRUE);
				hgsg.setNickName(generatedUniqueNickName);*/
				billingAddressNickName = generatedUniqueNickName;
				//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
				
				//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
				/*getShippingGroupMapContainer().addShippingGroup(generatedUniqueNickName, hgsg);*/
				setBillingAddressNickName(generatedUniqueNickName);
			} 
			
			
			Iterator iter = commerceIdentifierPaymentInfos.listIterator();
			if(!isOrderWithTotalZero()){
				while (iter.hasNext()) {
					CommerceIdentifierPaymentInfo cipi = (CommerceIdentifierPaymentInfo) iter
							.next();
					if (isAllowPaymentGroupsWithZeroAmount()) {
						applyCommerceIdentifierPaymentInfo(cipi, order);
					}
					int type = RelationshipTypes.stringToType(cipi
							.getRelationshipType());
					switch (type) {
					case TRUCSCConstants.PAYMENTAMOUNTREMAINING:
						applyCommerceIdentifierPaymentInfo(cipi, order);
						break;
					case TRUCSCConstants.SHIPPINGAMOUNTREMAINING:
						applyCommerceIdentifierPaymentInfo(cipi, order);
						break;
					case TRUCSCConstants.ORDERAMOUNTREMAINING:
						if(order.getPriceInfo() != null && order.getPriceInfo().getTotal() == TRUCSCConstants.DOUBLE_ZERO_FORMAT){
							setOrderWithTotalZero(true);
						}
						//if ((cipi != null) && (cipi.getAmount() > TRUCSCConstants.DOUBLE_ZERO_FORMAT || total == 0.0)) {
							applyCommerceIdentifierPaymentInfo(cipi, order);
						//}
						break;
					case TRUCSCConstants.TAXAMOUNTREMAINING:
						applyCommerceIdentifierPaymentInfo(cipi, order);
						break;
					default:
						if (cipi != null && cipi.getAmount() > TRUCSCConstants.DOUBLE_ZERO_FORMAT) {
							applyCommerceIdentifierPaymentInfo(cipi, order);
						}
					}
				}
			}
			if (isApplyDefaultPaymentGroup()) {
				applyDefaultPaymentGroup(order);
			}
			//Committing this change for testing the order total zero scenario while applying coupons to the order with 0 - Defect #64665
			if(order.getPriceInfo().getTotal() ==TRUCSCConstants.DOUBLE_ZERO_FORMAT){
				setOrderWithTotalZero(true);
				String name = getPaymentGroupMapContainer().getDefaultPaymentGroupName();
			    PaymentGroup paymentGroup = getPaymentGroup(name);
			    if(paymentGroup != null){
			    	applyDefaultPaymentGroup(order);
			    }
			}
			validateOrderRemainingAmount(pRequest,pResponse);
			if (getFormError()) {
				return;
			}
			pgm.recalculatePaymentGroupAmounts(order);
		} catch (CommerceException exc) {
			if (isLoggingError()){
				logError(exc);
			}
		}
	}

	/**
	 * Validate order remaining amount.
	 *
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public void validateOrderRemainingAmount(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			logDebug("START TRUCSRPaymentGroupFormHandler:: applyPaymentGroups method");
		}
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		if (listOfPaymentGroup == null || listOfPaymentGroup.size() ==TRUCSCConstants.NUMNER_ZERO) {
			mVe.addValidationError(TRUErrorKeys.ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED, null);
		} else {
			boolean validateOrderRemainingAmount =
					getBillingHelper().validateOrderRemainingAmount(order, TRUCheckoutConstants.COMMIT_ORDER);
			if (!validateOrderRemainingAmount) {
				mVe.addValidationError(TRUErrorKeys.GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT, null);
			}
		}
		getErrorHandler().processException(mVe, this);
		if (isLoggingDebug()) {
			logDebug("END TRUCSRPaymentGroupFormHandler:: applyPaymentGroups method");
		}
	}
	
	
	
	protected Map createRepriceParameterMap_tax() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRShippingGroupFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRShippingGroupFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
		/**
		 * This method will called after the handle method to apply card level promotion.
		 *
		 * @param pRequest
		 *            the servlet's request
		 * @param pResponse
		 *            the servlet's response
		 * @throws ServletException
		 *             if an error occurred while processing the servlet request
		 * @throws IOException
		 *             if an error occurred while reading or writing the servlet
		 *             
		 */

		public void postApplyPaymentGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	            throws ServletException, IOException {
	      if (isLoggingDebug()){
	               logDebug("Start: TRUCSRPaymentGroupFormHandler.postApplyPaymentGroups()");
	        }
	      super.postApplyPaymentGroups(pRequest, pResponse);
	      TRUOrderImpl order =(TRUOrderImpl) getShoppingCart().getCurrent();
	      PaymentGroup group = null;
	      TRUCreditCard creditCard = null;
	      List<PaymentGroup> paymentGroups =  ((TRUOrderImpl) getOrder()).getPaymentGroups();
	      List<TRUCreditCard> creditCards=new ArrayList<TRUCreditCard>();
	      List<String> removeList=new ArrayList<String>();
	      PaymentGroupManager pgm = ((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).getOrderManager().getPaymentGroupManager();
	      try{
	    	  
	    	  for (Iterator iterator = paymentGroups.iterator(); iterator.hasNext();) {
	    		  group = (PaymentGroup) iterator.next();
	    		  
	    		    if(group instanceof TRUCreditCard && group.getAmount()==0.0){
	    		    	removeList.add(group.getId());
	    	          }
				
			}
	    	  for (Iterator iterator = removeList.iterator(); iterator.hasNext();) {
				String id = (String) iterator.next();
				pgm.removePaymentGroupFromOrder(order, id);
			}
	    	  ((TRUCSROrderManager) getOrderManager()).updateOrder(order);
	    	
	        for (Iterator iter = paymentGroups.iterator(); iter.hasNext(); ) {
	                group = (PaymentGroup) iter.next();
	                if (group instanceof TRUCreditCard){
	                      creditCard = (TRUCreditCard) group;
	                      String ccType = creditCard.getCreditCardType();
	                      ((TRUOrderImpl) getOrder()).setCreditCardType(ccType);
	                      creditCards.add(creditCard);
	               }
	            	if(creditCards!=null && creditCards.size()>1){
	            		addFormException(new DropletException("Splitting amount to multiple cards is not allowed. Please use one card for checkout"));
	               		pgm.removeEmptyPaymentGroups(order);
	              		((TRUCSROrderManager) getOrderManager()).updateOrder(order);
	    				return; 
		     	       	}
	            	
	            	
	     	        if ((!isOrderWithTotalZero()  || paymentGroups.size() > TRUCSCConstants.NUMBER_ONE) && group.getAmount() == TRUCSCConstants.DOUBLE_ZERO) {
	        					pgm.removePaymentGroupFromOrder(order, group.getId());
	        					 ((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(
	        	                           PricingConstants.OP_REPRICE_ORDER_TOTAL,
	        	                           getOrder(),
	        	                           getUserPricingModels(),
	        	                           getUserLocale(),
	        	                           getProfile(),
	        	                           createRepriceParameterMap(),
	        	                           this);
	        		     	       	((TRUCSROrderManager) getOrderManager()).updateOrder(getOrder());
	        		     	   	return;
	        			}
	     	       List<Relationship> updateSGCIRelList = new ArrayList<Relationship>();
	     	       List<ShippingGroup> shippingGroups = getOrder().getShippingGroups();
	   			if(shippingGroups.size() > TRUCSCConstants.NUMBER_ONE){
	   				for (ShippingGroup sg : shippingGroups) {
	   						updateSGCIRelList.addAll(sg.getCommerceItemRelationships());
	   				}
	   			}
	   			for (Iterator iterator = updateSGCIRelList.iterator(); iterator
						.hasNext();) {
					TRUShippingGroupCommerceItemRelationship sGCIRel = (TRUShippingGroupCommerceItemRelationship) iterator
							.next();
					if(sGCIRel.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
						TRUHardgoodShippingGroup hSG=(TRUHardgoodShippingGroup) sGCIRel.getShippingGroup();
						hSG.setShippingAddress(getAddressInputFields());
					}
				}
	     	       ((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(
                           PricingConstants.OP_REPRICE_ORDER_TOTAL,
                           getOrder(),
                           getUserPricingModels(),
                           getUserLocale(),
                           getProfile(),
                           createRepriceParameterMap_tax(),
                           this);
	     	       
	     	      
	     	       	((TRUCSROrderManager) getOrderManager()).updateOrder(getOrder());
	     	    
	        }
	        
	        
	      }
	        catch (CommerceException e) {
				if(isLoggingError()){
			    	logError(" CommerceException occured at TRUCSRPaymentGroupFormHandler.postApplyPaymentGroups()",e);
			    }
			}catch (RunProcessException rpE) {
                 if (isLoggingError()) {
                      logError("RunProcessException occured during preCommitOrder for order Id : " + getOrder().getId(), rpE);
               }
        }
	     
	         
	         	if (isLoggingDebug()){
	         		logDebug("End: TRUCSRPaymentGroupFormHandler.postApplyPaymentGroups()");
	         	} 
		}
		
	}