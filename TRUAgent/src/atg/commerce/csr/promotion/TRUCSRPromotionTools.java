package atg.commerce.csr.promotion;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.commerce.promotion.TRUPromotionTools;

import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.pricing.TRUPricingModelProperties;



/**
 * This class has extended TRUPromotionTools to include the items related to CSC.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUCSRPromotionTools extends TRUPromotionTools {
	
	
	/**
	 * This will return the Promotion Ids set for the corresponding skuItem
	 * @param pCatalogProperties for CatalogProperties
	 * @param pSkuItem for SkuItem
	 * @return promotionsIds
	 */
	public Set<String> getPromotionIds(TRUCatalogProperties pCatalogProperties, RepositoryItem pSkuItem){
		if(isLoggingDebug()){
			logDebug("START - TRUCSRPromotionTools :: getPromotionIds method");
		}
		Set<String> promotionsIds = null;
		if(pSkuItem.getPropertyValue(pCatalogProperties.getSkuQualifiedForPromos()) != null){
			promotionsIds = new HashSet<String>((List<String>) pSkuItem.getPropertyValue(pCatalogProperties.getSkuQualifiedForPromos()));
		}
		if(isLoggingDebug()){
			logDebug("EXIT - TRUCSRPromotionTools :: getPromotionIds method");
		}
		return promotionsIds;
	}
	
	/**
	 * This will sort the promotion list
	 * @param pList for promotionList
	 */
	public void compareAndAdd(List<RepositoryItem> pList) {
		if(isLoggingDebug()){
			logDebug("START - TRUCSRPromotionTools :: compareAndAdd method");
		}
		Collections.sort(pList, new Comparator<RepositoryItem>(){
			public int compare(RepositoryItem pPriority1, RepositoryItem pPriority2) {
					Integer p01Item;
					Integer p02Item;
					if((Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
						p01Item=(Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName());
					}else{
						p01Item=(Integer) pPriority1.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getPriority());
					}
					if((Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName())!=null){
						p02Item=(Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getDisplayPriorityPropertyName());
					}else{
						p02Item=(Integer) pPriority2.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getPriority());
					}
					return (p01Item.compareTo(p02Item));
				}
			});
		if(isLoggingDebug()){
			logDebug("END - TRUCSRPromotionTools :: compareAndAdd method");
		}
	}
	
	/**
	 * This will return the promotion detail by promotionId
	 * @param pPromotionId for promotionId
	 * @return promoDetail
	 */
	public String getPromoDetailsByPromoId(String pPromotionId){
		if(isLoggingDebug()){
			logDebug("START - TRUCSRPromotionTools :: getPromoDetailsByPromoId method");
		}
		RepositoryItem promotionItem = null;
		String promotionDetails = null;
		try {
			promotionItem = getItemForId(((TRUPricingModelProperties) getPricingModelProperties()).getPromotionItemDescName(), pPromotionId);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("No Promotion Found for Promotion ID : {0}",pPromotionId);
			}
		}
		if(promotionItem != null){
			promotionDetails = (String) promotionItem.getPropertyValue(((TRUPricingModelProperties) getPricingModelProperties()).getPromotionDetailsPropertyName());
		}
		if(isLoggingDebug()){
			logDebug("END - TRUCSRPromotionTools :: getPromoDetailsByPromoId method");
		}
		return promotionDetails;
	}
}
