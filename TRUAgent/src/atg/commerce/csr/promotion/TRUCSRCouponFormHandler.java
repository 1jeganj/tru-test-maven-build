package atg.commerce.csr.promotion;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.order.CSROrderHolder;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.order.Order;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.PromotionException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.integrations.sucn.common.SUCNIntegrationException;
import com.tru.integrations.sucn.couponlookup.TRUSUCNLookupService;
import com.tru.integrations.sucn.couponlookup.response.CouponLookupResponse;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;


/**
 * This class extends ATG OOTB CSRCouponFormHandler and used
 * for managing the coupon related operation Extensions to CouponFormHandler.
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRCouponFormHandler extends CSRCouponFormHandler {
	
	/** The m coupon email. */
	private String mCouponEmail;

	/** Number regex. */
	public static final String NUMBER_REGEX = "^[0-9]+";
	
	/** Static constant for 20. */
	private  static final int INT_TWENTY = 20;
	
	/** ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();

	/**
	 * property to hold salePriceList.
	 */
	public static final String SALE_PRICE_LIST = "salePriceList";

	/**
	 * property to hold priceList.
	 */
	public static final String PRICE_LIST = "priceList";

	/**
	 * property to hold 20.
	 */
	public static final int _20 = 20;

	/**
	 * property to hold mTRUSUCNLookupService.
	 */
	private TRUSUCNLookupService mSucnLookupService;

	/**
	 * property to hold mOrderId
	 */
	private String mOrderId;

	/**
	 * This holds the value for single use coupon code entered by customer.
	 */
	private String mSingleUseCouponCode;
	
	/**
	 *  property to hold mSucnCode.
	 */
	private int  mSucnCode;

	/**
	 * property to hold mUserPricingModels.
	 */
	private PricingModelHolder mUserPricingModels;
	
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;


	/**
	 * property to hold mPurchaseProcessHelper.
	 */
	private TRUCSRPurchaseProcessHelper mPurchaseProcessHelper;

	/**
	 * property to hold RepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	/**
	 * property to hold SuccessErrorUrlMap.
	 */
	private Map<String, String> mSuccessErrorUrlMap;

	/**
	 * property to hold mCouponConfiguration.
	 */
	private TRUConfiguration mCouponConfiguration;

	/**
	 * property to hold ErrorHandler.
	 */
	private IErrorHandler mErrorHandler;

	/**
	 * property to hold OrderManager.
	 */
	private TRUOrderManager mOrderManager;

	/**
	 * property to hold mDisplayName.
	 */
	private String mDisplayName;
	
	/**
	 *  property to hold mSucnErrorMessage.
	 */
	private String mSucnErrorMessage;

	/**
	 * property to hold mErrorHandlerManager.
	 */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * property to hold mCartComponent.
	 */
	private CSROrderHolder mCartComponent;
	
	/**
	 * List to hold Error Keys.
	 */
	private List<String> mErrorKeyList;
	

	/**
	 * Gets the coupon email.
	 *
	 * @return the coupon email
	 */
	public String getCouponEmail() {
		return mCouponEmail;
	}

	/**
	 * Sets the coupon email.
	 *
	 * @param mCouponEmail the new coupon email
	 */
	public void setCouponEmail(String mCouponEmail) {
		this.mCouponEmail = mCouponEmail;
	}
	
	/**
	 * Gets the error key list.
	 * 
	 * @return the errorKeyList
	 */
	public List<String> getErrorKeyList() {
		return mErrorKeyList;
	}

	/**
	 * Sets the error key list.
	 * 
	 * @param pErrorKeyList
	 *            the errorKeyList to set
	 */
	public void setErrorKeyList(List<String> pErrorKeyList) {
		mErrorKeyList = pErrorKeyList;
	}

	/**
	 * @return the mCartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent
	 *            the cartComponent to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		mCartComponent = pCartComponent;
	}

	/**
	 * @return TransactionDemarcation object
	 */
	protected TransactionDemarcation getTransactionDemarcation() {
		return new TransactionDemarcation();
	}

	/**
	 * @return the SucnLookupService
	 */
	public TRUSUCNLookupService getSucnLookupService() {
		return mSucnLookupService;
	}

	/**
	 * @param pSucnLookupService
	 *            the SucnLookupService to set
	 */
	public void setSucnLookupService(TRUSUCNLookupService pSucnLookupService) {
		mSucnLookupService = pSucnLookupService;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId
	 *            the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * Returns the singleUseCouponCode
	 * 
	 * @return the singleUseCouponCode
	 */
	public String getSingleUseCouponCode() {
		return mSingleUseCouponCode;
	}

	/**
	 * Sets/updates the singleUseCouponCode
	 * 
	 * @param pSingleUseCouponCode
	 *            the singleUseCouponCode to set
	 */
	public void setSingleUseCouponCode(String pSingleUseCouponCode) {
		mSingleUseCouponCode = pSingleUseCouponCode;
	}

	/**
	 * @return the mUserPricingModels
	 */
	public PricingModelHolder getUserPricingModels() {
		return mUserPricingModels;
	}

	/**
	 * @param pUserPricingModels
	 *            the mUserPricingModels to set
	 */
	public void setUserPricingModels(PricingModelHolder pUserPricingModels) {
		mUserPricingModels = pUserPricingModels;
	}

	
	/**
	 * @return the mSuccessErrorUrlMap
	 */
	public Map<String, String> getSuccessErrorUrlMap() {
		return mSuccessErrorUrlMap;
	}

	/**
	 * @param pSuccessErrorUrlMap
	 *            the mSuccessErrorUrlMap to set
	 */
	public void setSuccessErrorUrlMap(Map<String, String> pSuccessErrorUrlMap) {
		mSuccessErrorUrlMap = pSuccessErrorUrlMap;
	}

	/**
	 * @return the mRepeatingRequestMonitor
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @param pRepeatingRequestMonitor
	 *            the mRepeatingRequestMonitor to set
	 */
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	/**
	 * @return the mTRUPurchaseProcessHelper
	 */
	public TRUCSRPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * This method will set mPurchaseProcessHelper with pPurchaseProcessHelper.
	 * @param pPurchaseProcessHelper the mPurchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(TRUCSRPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * @return the couponConfiguration
	 */
	public TRUConfiguration getCouponConfiguration() {
		return mCouponConfiguration;
	}

	/**
	 * @param pCouponConfiguration
	 *            the couponConfiguration to set
	 */
	public void setCouponConfiguration(TRUConfiguration pCouponConfiguration) {
		mCouponConfiguration = pCouponConfiguration;
	}

	/**
	 * This method will return ErrorHandler.
	 * 
	 * @return mErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * This method will set ErrorHandlers with pErrorHandler.
	 * 
	 * @param pErrorHandler
	 *            refers the IErrorHandler
	 */

	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * This Method will return the TRUOrderManager.
	 * 
	 * @return the mOrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * This method will set OrderManager to pOrderManager.
	 * 
	 * @param pOrderManager
	 *            the mOrderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * @return the mdisplayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * @param pDisplayNamep the displayNamep to set
	 */
	public void setDisplayName(String pDisplayNamep) {
		mDisplayName = pDisplayNamep;
	}

	/**
	 * @return the errorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager the errorHandlerManager to set
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	
	/**
	 * @return the integrationStatusUtil.
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set.
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}
	
	/**
	 * @return the mSucnCode.
	 */
	public int getSucnCode() {
		return mSucnCode;
	}

	/**
	 * @param pSucnCode the mSucnCode to set.
	 */
	public void setSucnCode(int pSucnCode) {
		this.mSucnCode = pSucnCode;
	}

	/**
	 * @return the mSucnErrorMessage.
	 */
	public String getSucnErrorMessage() {
		return mSucnErrorMessage;
	}

	/**
	 * @param pSucnErrorMessage the mSucnErrorMessage to set.
	 */
	public void setSucnErrorMessage(String pSucnErrorMessage) {
		this.mSucnErrorMessage = pSucnErrorMessage;
	}
	/**
	 * Claim the specified coupon, register a form exception if the coupon is invalid or an error occurs.
	 * 
	 * @param pRequest
	 *            - current HTTP servlet request
	 * @param pResponse
	 *            - current HTTP servlet response
	 * 
	 * @return true if coupon has been claimed; false otherwise
	 * 
	 * @throws ServletException
	 *             if an error occurred during claiming the coupon
	 * @throws IOException
	 *             if an error occurred during claiming the coupon
	 */
	@Override
	public boolean handleClaimCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRCouponFormHandler.handleClaimCoupon() ");
		}
		String couponCodeId = pRequest.getQueryParameter(TRUConstants.COUPON_ID);
		if(!StringUtils.isBlank(couponCodeId)){
			setCouponClaimCode(couponCodeId);
		}
		//Map<String, String> singleUseCoupon = null;
		String tenderdCouponCode =  getCouponClaimCode();
		CSRAgentTools csrAgentTools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		Profile profile = getProfile();
		final String myHandleMethod = TRUConstants.HANDLE_CLAIM_COUPON;
		boolean isSingleUseCoupon = Boolean.FALSE;
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			String tenderdCouponClaimCode = getCouponClaimCode();
		final boolean isCouponServiceEnabled =  getIntegrationStatusUtil().getCouponServiceIntStatus(pRequest);
		if (StringUtils.isEmpty(tenderdCouponClaimCode)) { 
			if (isLoggingError()) {
				logError("User enterd empty coupon code... Adding validation exception");
			}
			mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EMPTY, null);
			getErrorHandler().processException(mVe, this);
		}
		if (StringUtils.isNotBlank(tenderdCouponClaimCode) && tenderdCouponClaimCode.length() != INT_TWENTY ) {
			setCouponClaimCode(tenderdCouponCode);
		}
		boolean isSUCNEnabled = ((TRUClaimableTools)getClaimableTools()).checkSucnEnabled(tenderdCouponCode);
		if(tenderdCouponCode.length() != INT_TWENTY  && isSUCNEnabled){
			if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN)){
				addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN));
				}else{
					mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
					getErrorHandler().processException(mVe, this);
				}
			if (isLoggingError()) {
				logError("User enterd not valid coupon code... Adding validation exception");
			}
		}
		if (tenderdCouponCode.length() == INT_TWENTY && isCouponServiceEnabled && tenderdCouponCode.matches(NUMBER_REGEX)) {
			createSingleUseCoupon(tenderdCouponCode);
			isSingleUseCoupon = Boolean.TRUE;
		}else{
			setCouponClaimCode(tenderdCouponClaimCode);
		}
		boolean isCouponUsed = preClaimCoupon(isSingleUseCoupon,tenderdCouponCode);// calling this to validate user entered coupon
		if (!isCouponUsed && !getFormError()){
			final TRUOrderImpl currentOrder = (TRUOrderImpl) getCartComponent().getCurrent();
			final TRUOrderManager orderManager = getOrderManager();
			TRUPropertyManager propertyManager = orderManager.getPropertyManager();
			final TransactionDemarcation td = new TransactionDemarcation();
			final TransactionManager tm = getTransactionManager();
			boolean rollback = true;
			try {
				td.begin(tm, TransactionDemarcation.REQUIRED);
				//check if PG as giftCard then set creditCardType as null for not to apply promotion.
				if(StringUtils.isNotBlank(getCouponEmail()))
				{
					currentOrder.setEmail(getCouponEmail());
				}
				orderManager.setCCTypeNullIfPGAsGC(currentOrder);
				if(isSingleUseCoupon){
					profile.setPropertyValue(propertyManager.getSucnCodePropertyName(), tenderdCouponCode);
				}
				Map<String, String> singleUseCoupon = currentOrder.getSingleUseCoupon();
				singleUseCoupon.put(tenderdCouponCode,getCouponClaimCode());
				claimCoupon(pRequest, pResponse);
				final PricingModelHolder userPricingModels = csrenvtools.getCurrentOrderPricingModelHolder();
				userPricingModels.initializePricingModels();
				getPurchaseProcessHelper().runProcessRepriceOrder(
						getCouponConfiguration().getOrderRepricingOption(), currentOrder, userPricingModels,
						getUserLocale(pRequest), getProfile(), null, null);
				adjustOrderPaymentGroups(currentOrder);
				synchronized (currentOrder) {
					orderManager.updateOrder(currentOrder);
				}
				rollback = false;
			} catch (ClaimableException ce) {
				final Throwable sourceException = ce.getSourceException();
				if (sourceException instanceof PromotionException) {
					final PromotionException pe = (PromotionException) sourceException;
					if (isLoggingError()) {
						vlogError("PromotionException  while claiming coupon : {0}", pe);
					}
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED)){
						addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED));
					}else{
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
						getErrorHandler().processException(mVe, this);
					}
					return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
				} else {
					if (isLoggingError()) {
						vlogError("ClaimableException  while claiming coupon : {0}", ce);
					}
					
					if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_NOT_FOUND)){
						addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_NOT_FOUND));
					}else{
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_NOT_FOUND, null);
						getErrorHandler().processException(mVe, this);
					}					
					rollback = false;
				}
				return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
			} catch (CommerceException commerceException) {
				if (isLoggingError()) {
					vlogError("CommerceException  while claiming coupon : {0}", commerceException);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} catch (TransactionDemarcationException tde) {
				if (isLoggingError()) {
					vlogError("TransactionDemarcationException  while claiming coupon:{0} ", tde);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					vlogError("RunProcessException  while claiming coupon:{0} ", e);
				}
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
				getErrorHandler().processException(mVe, this);
			} finally {
				profile.setPropertyValue(propertyManager.getSucnCodePropertyName(), null);
				try {
					if (tm != null) {
						td.end(rollback);
					}
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						vlogError("TransactionDemarcationException in finally while claiming coupon:{0} ", e);
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("Exiting from class:[TRUCSRCouponFormHandler] method:[handleClaimCoupon]");
			}
		}
	}
		return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
	}

	
	
	
	
	/**
	 * Creates the single use coupon.
	 *
	 * @param pTenderedCouponCode the tendered coupon code
	 * @throws ServletException the servlet exception
	 */
	public void createSingleUseCoupon(String pTenderedCouponCode) throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRCouponFormHandler.createOrUpdateSingleUseCoupon() ");
		}
		String promoId = null;
		int code = 0;
		String sucnErrorMessage = null;
		CouponLookupResponse lookupCouponDetails = null;
		TRUClaimableTools claimableTools = (TRUClaimableTools) getClaimableTools();
		if (!StringUtils.isBlank(pTenderedCouponCode)) {
			try {
				lookupCouponDetails = getSucnLookupService().lookupCouponDetails(getCouponClaimCode(), getOrderId(),TRUConstants.EMPTY);
				if (lookupCouponDetails != null) {
					if (isLoggingDebug()) {
						vlogDebug("lookupCouponDetailsl:{0}", lookupCouponDetails.toString());
					}
					promoId = lookupCouponDetails.getPromoId();
					if (!(claimableTools.checkSucnEnabled(promoId))) {
						if (isLoggingError()) {
							logError("User enterd not valid coupon code... Adding validation exception");
						}
						mVe.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
						getErrorHandler().processException(mVe, this);
					}
					if (getFormError()) {
						return;
					}
					code = lookupCouponDetails.getCode();
					sucnErrorMessage = lookupCouponDetails.getText();
					setSucnCode(code);
					setSucnErrorMessage(sucnErrorMessage);
					if (isLoggingDebug()) {
						vlogDebug("sucnErrorMessage:{0} :: sucncode:{1}", sucnErrorMessage, code);
					}
					setCouponClaimCode(promoId);
				}

			} catch (SUCNIntegrationException e) {
				if (isLoggingError()) {
					logError("While getting the webservice Integration error ", e);
				}
			} catch (ServletException exc) {
				if (isLoggingError()) {
					logError("ServletException : While getting error message ", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUCSRCouponFormHandler.createOrUpdateSingleUseCoupon() ");
		}
	}


	/**
	 * This method is to adjust order payment groups.
	 * @param pOrder - Order
	 * @throws CommerceException - if any
	 */
	private void adjustOrderPaymentGroups(Order pOrder) throws CommerceException {
	    if(isLoggingDebug()){
 			logDebug("Start: TRUCSRCouponFormHandler.adjustOrderPaymentGroups()");
 		}
		PipelineResult result;
		result = getOrderManager().adjustPaymentGroups(pOrder);
		if (isLoggingDebug()) {
			logDebug("TRUCSRCouponFormHandler/adjustOrderPaymentGroups method "+result);
		}
		if(isLoggingDebug()){
 			logDebug("End: TRUCSRCouponFormHandler.adjustOrderPaymentGroups()");
 		} 
	}
	/**
	 * This method is used to check weather coupon is expired or not.
	 * 
	 * @param pCouponItem
	 *            - coupon code
	 * @return boolean
	 * @throws RepositoryException RepositoryException
	 */
	private boolean canClaimCoupon(RepositoryItem pCouponItem) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRCouponFormHandler.canClaimCoupon() ");
		}
		Boolean canClaim = false;
		if (pCouponItem == null) {
			canClaim = false;
		} else {
			return getClaimableManager().checkClaimableDates(pCouponItem);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRCouponFormHandler.canClaimCoupon() ");
		}
		return canClaim;
	}

	/**
	 * Register a form exception if the coupon is empty .
	 * 
	 * @param pIsSingleUseCoupon - boolean.
	 * @param pSUCNCoupon - SUCN coupon Code.
	 * 
	 * @return boolean value
	 * @throws ServletException
	 *             if an error occurred during validating the coupon.
	 * @throws IOException
	 *             if an error occurred during validating the coupon.
	 */

	public boolean preClaimCoupon(boolean pIsSingleUseCoupon, String pSUCNCoupon)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCouponFormHandler] method:[preClaimCoupon]");
		}
		final ValidationExceptions ve = new ValidationExceptions();
		RepositoryItem coupon = null;
		final String tenderdCouponCode = getCouponClaimCode();
		final String sucnErrorMessage = getSucnErrorMessage();
		final Profile profile = getProfile();
		boolean couponUsed = false;
		if (getSucnCode() != 0 && !StringUtils.isEmpty(sucnErrorMessage)) {//This fix provided as per defect 51850
			if ( getSucnCode() == TRUConstants.FOUR) {
				if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED)){
					addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED));
				}else{
					ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
					getErrorHandler().processException(ve, this);
				}
			} else {
				if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN)){
					addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_PLEASE_ENTER_VALID_SUCN));
				}else{
					ve.addValidationError(TRUErrorKeys.TRU_PLEASE_ENTER_VALID_SUCN, null);
					getErrorHandler().processException(ve, this);
				}
			}
	}
		boolean isValid = Boolean.TRUE;
		final TRUPricingTools pricingTools = (TRUPricingTools) getPromotionTools().getPricingTools();
		try {
			isValid = getOrderManager().getCouponCode(profile,tenderdCouponCode,pIsSingleUseCoupon,pSUCNCoupon);
			// If this coupon has already been claimed just return
			if (!StringUtils.isEmpty(tenderdCouponCode) && !isValid) {
				couponUsed = true;
				return couponUsed;
			}
			if (!StringUtils.isEmpty(tenderdCouponCode)) {
				coupon = getClaimableTools().getClaimableItem(tenderdCouponCode);
				if (coupon != null) {
					final String couponName = (String) pricingTools.getValueForProperty(getDisplayName(), coupon);
					if (!StringUtils.isEmpty(couponName) && !canClaimCoupon(coupon)) {
						if(getErrorKeyList().contains(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED)){
							addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_CHECKOUT_COUPON_EXPIRED));
						}else{
							ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EXPIRED, null);
							getErrorHandler().processException(ve, this);
						}
					}
				}
			}
			// Modified for defect - TUW-64607
			//final boolean isMaxLimitReached = getOrderManager().checkMaxCouponLimit(profile);
			final boolean isMaxLimitReached = getOrderManager().checkOrderMaxCouponLimit((TRUOrderImpl)getCartComponent().getCurrent());
			if(isMaxLimitReached ){
				if (isLoggingError()) {
					logError("Reached Max Limit for applying coupons... Adding validation exception");
				}
	  		    ve.addValidationError(TRUErrorKeys.TRU_ERROR_COUPON_MAX_LIMIT, null);
				getErrorHandler().processException(ve, this);
	  	  	}
		} catch (CommerceException exc) {
			if (isLoggingError()) {
				vlogError("CommerceException : while getting coupon from profile : {0} with exception : {1}", profile, exc);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RunProcessException  while claiming coupon:{0} ", e);
			}
			ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
			getErrorHandler().processException(ve, this);
		} 
		if (getFormError()) {
			return false;
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from class:[TRUCSRCouponFormHandler] method:[preClaimCoupon]");
		}
		return false;
	}

	/**
	 *  This method is used to remove the coupon which user applied.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @return
	 * 				- true if coupon has been removed; false otherwise
	 * @throws ServletException
	 *             - If any ServletException
	 * @throws IOException
	 *             - If any IOException
	 * @throws RunProcessException
	 * 				- If any RunProcessException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean handleRemoveCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, RunProcessException {

		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRCouponFormHandler.handleRemoveCoupon()");
		}
		final ValidationExceptions ve = new ValidationExceptions();
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String myHandleMethod = TRUConstants.HANDLE_REMOVE_COUPON;
		final Order currentOrder = getCartComponent().getCurrent();
		final TRUOrderManager orderManager = getOrderManager();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		String couponCodeId = pRequest.getQueryParameter(TRUConstants.COUPON_ID);
		if(!StringUtils.isBlank(couponCodeId)){
			setCouponClaimCode(couponCodeId);
		}
		CSRAgentTools csrAgentTools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			vlogDebug("Success URL value : {0} Error URL value : {1}", getClaimCouponSuccessURL(),
					getClaimCouponErrorURL());
			vlogDebug("Success URL value : {0} Error URL value : {1}", getClaimCouponSuccessURL(),
					getClaimCouponErrorURL());
			preRemoveCoupon(pRequest, pResponse);
			if(!getFormError()){
				try {
					if (tm != null) {
						td.begin(tm, TransactionDemarcation.REQUIRED);
						if(StringUtils.isNotBlank(getCouponEmail()))
						{
							((TRUOrderImpl)currentOrder).setEmail(getCouponEmail());
						}
						String tenderdCouponCode = getCouponClaimCode();
						getPurchaseProcessHelper().removeCouponItem(tenderdCouponCode, getProfile(),
								getUserLocale(pRequest));
						Map extraParameters = new HashMap();
						extraParameters.put(TRUConstants.REMOVED_COUPON_CODE, tenderdCouponCode);
						extraParameters.put(PRICE_LIST, csrenvtools.getCurrentPriceList());
						extraParameters.put(SALE_PRICE_LIST, csrenvtools.getCurrentSalePriceList());
						final PricingModelHolder userPricingModels = csrenvtools.getCurrentOrderPricingModelHolder();
						userPricingModels.initializePricingModels();
						getPurchaseProcessHelper().runProcessRepriceOrder(getCouponConfiguration().mOrderRepricingOption,
								currentOrder,
								userPricingModels,
								getUserLocale(pRequest),
								getProfile(),
								extraParameters,
								null);
						adjustOrderPaymentGroups(currentOrder);
						synchronized (currentOrder) {
							orderManager.updateOrder(currentOrder);
						}
					}
				} catch (TransactionDemarcationException trDem) {
					lRollbackFlag = true;
					if (isLoggingError()) {
						logError("TransactionDemarcationException occured while removing reward number with exception {0}",
								trDem);
					}
				} catch (CommerceException commerceException) {
					if (isLoggingError()) {
						vlogError("CommerceException  while claiming coupon : {0}", commerceException);
					}
					ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_PLEASE_TRY_AGAIN, null);
					getErrorHandler().processException(ve, this);
				} finally {
					if (tm != null) {
						try {
							td.end(lRollbackFlag);
						} catch (TransactionDemarcationException tD) {
							if (isLoggingError()) {
								logError("TransactionDemarcationException", tD);
							}
						}
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRUConstants.HANDLE_REMOVE_COUPON);
					}
				}
				if (isLoggingDebug()) {
					logDebug("End: TRUCSRCouponFormHandler.handleRemoveCoupon()");
				}
			}
		}
		return checkFormRedirect(getClaimCouponSuccessURL(), getClaimCouponErrorURL(), pRequest, pResponse);
	}


	/**
	 * Register a form exception if the coupon is empty or invalid.
	 * 
	 * @param pRequest
	 *            - current HTTP servlet request
	 * @param pResponse
	 *            - current HTTP servlet response
	 * 
	 * @throws ServletException
	 *             if an error occurred during validating the coupon
	 * @throws IOException
	 *             if an error occurred during validating the coupon
	 */
	public void preRemoveCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCouponFormHandler] method:[preRemoveCoupon]");
		}
		ValidationExceptions ve = new ValidationExceptions();
		RepositoryItem coupon = null;
		String tenderdCouponCode = getCouponClaimCode();
		if (StringUtils.isEmpty(tenderdCouponCode)) {
			if (isLoggingError()) {
				logError("User enterd empty coupon code... Adding validation exception");
			}
			ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_EMPTY, null);
			getErrorHandler().processException(ve, this);
		}
		if (!StringUtils.isEmpty(tenderdCouponCode)) {
			try {
				coupon = getClaimableTools().getClaimableItem(tenderdCouponCode);
				if (null == coupon) {
					if (isLoggingError()) {
						logError("User enterd invalid coupon code... Adding validation exception");
					}
					ve.addValidationError(TRUErrorKeys.TRU_ERROR_CART_COUPON_NOT_FOUND, null);
					getErrorHandler().processException(ve, this);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					vlogError("RunProcessException  while removing coupon:{0} ", re);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCouponFormHandler] method:[preRemoveCoupon]");
		}
	}

	/**
	 * This method is used to get the coupon code by calling SUCN Service.
	 *//*
	private void updateCouponCode() {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCouponFormHandler] method:[updateCouponCode]");
		}
		String lPromo_id = null;
		CouponLookupResponse lLookupCouponDetailsl = null;
		try {
			lLookupCouponDetailsl = getSucnLookupService().lookupCouponDetails(getCouponClaimCode(), getOrderId());
			if (lLookupCouponDetailsl != null) {
				lPromo_id = lLookupCouponDetailsl.getPromoId();
			}
			if (!StringUtils.isEmpty(lPromo_id)) {
				setCouponClaimCode(lPromo_id);
			}
		} catch (SUCNIntegrationException e) {
			if (isLoggingError()) {
				logError("While getting the webservice Integration error ", e);
			}
		}
		vlogDebug("Six digit response code : {0}", lPromo_id);
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCouponFormHandler] method:[updateCouponCode]");
		}
	}*/
	
	/**
	 * This method is used to get the error URL and Success URL from the map.
	 * 
	 * @param pValue
	 *            - success or error value.
	 * @return url - success or error url.
	 */
	public String getSuccessOrErrorURL(String pValue) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRCouponFormHandler.getSuccessOrErrorURL() ");
			vlogDebug("Success or Error value: {0}", pValue);
		}
		String url = null;
		if (pValue != null) {
			url = getSuccessErrorUrlMap().get(pValue);
		}
		if (url == null) {
			url = pValue;
		} else {
			vlogDebug("Success or Error URL value is null : {0}", pValue);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRCouponFormHandler.getSuccessOrErrorURL() ");
		}
		return url;
	}

}
