package atg.commerce.csr.environment;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.repository.RepositoryItem;


public class TRUCSREnvironmentMonitor extends CSREnvironmentMonitor {
	
	
	/**
	 * Reset order for owner change.
	 *
	 * @param pOrder the order
	 * @param pNewOwner the new owner
	 * @throws CommerceException the commerce exception
	 * @see atg.commerce.csr.environment.CSREnvironmentMonitor#resetOrderForOwnerChange(atg.commerce.order.Order, atg.repository.RepositoryItem)
	 */
	public void resetOrderForOwnerChange(Order pOrder, RepositoryItem pNewOwner)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("TRUCSREnvironmentMonitor.resetOrderForOwnerChange() method.STARTS");
		}
		ShippingGroupManager shippingGroupManager = getCSREnvironmentTools()
				.getCSRAgentTools().getOrderManager().getShippingGroupManager();
		PaymentGroupManager paymentGroupManager = getCSREnvironmentTools()
				.getCSRAgentTools().getOrderManager().getPaymentGroupManager();

		List exclusionPGs = new ArrayList();
		if (pOrder.getShippingGroups() != null) {
			for (Iterator iterator = pOrder.getShippingGroups().iterator(); iterator
					.hasNext();) {
				ShippingGroup sg = (ShippingGroup) iterator.next();
				exclusionPGs.add(sg);
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCSREnvironmentMonitor.resetOrderForOwnerChange() exclusion payment groups, exclusionPGs:{0} "
					+ exclusionPGs);
		}
		shippingGroupManager.removeAllShippingGroupsFromOrder(pOrder,
				exclusionPGs);
		paymentGroupManager.removeAllPaymentGroupsFromOrder(pOrder);
		CommerceItemManager commerceItemManager = getCSREnvironmentTools()
				.getCSRAgentTools().getCommerceItemManager();
		Map usedShippingGroups = new HashMap();
		Iterator commerceItems = pOrder.getCommerceItems().iterator();
		while (commerceItems.hasNext()) {
			CommerceItem item = (CommerceItem) commerceItems.next();
			String shippingGroupType = shippingGroupManager
					.getFirstShippingGroupType(null, null, item);
			if (isLoggingDebug()) {
				vlogDebug("TRUCSREnvironmentMonitor.resetOrderForOwnerChange() shippingGroupType:{0} "
						+ shippingGroupType);
			}
			ShippingGroup shippingGroup = null;
			if (usedShippingGroups.containsKey(shippingGroupType)) {
				shippingGroup = (ShippingGroup) usedShippingGroups
						.get(shippingGroupType);
			} else {
				shippingGroup = shippingGroupManager
						.createShippingGroup(shippingGroupType);
				shippingGroupManager.addShippingGroupToOrder(pOrder,
						shippingGroup);
			}
			if (isLoggingDebug()) {
				vlogDebug(
						"TRUCSREnvironmentMonitor.resetOrderForOwnerChange() CommerceItemId:{0}, shippingGroupId:{1}",
						item.getId(), shippingGroup.getId());
			}
			commerceItemManager.addItemQuantityToShippingGroup(pOrder,
					item.getId(), shippingGroup.getId(), item.getQuantity());
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUCSREnvironmentMonitor.resetOrderForOwnerChange() method.END");
		}
	}
}