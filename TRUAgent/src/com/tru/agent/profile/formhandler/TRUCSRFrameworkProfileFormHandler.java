/**
 * 
 */
package com.tru.agent.profile.formhandler;


import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;

import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.dynamo.LangLicense;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.svc.ui.formhandlers.FrameworkProfileFormHandler;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

import com.tru.commerce.csr.util.TRUCSCConstants;

/**
 *
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRFrameworkProfileFormHandler extends FrameworkProfileFormHandler{


private static ResourceBundle sResourceBundle = ResourceBundle.getBundle(TRUCSCConstants.ATG_SCENARIO_SCENARIO_RESOURCES, LangLicense.getLicensedDefault());
	

/**
 * To holds the String mSsoLogin 
 */
private String mSsoLoginURI;

/**
 * @return the mSsoLoginURI
 */
public String getSsoLoginURI() {
	return mSsoLoginURI;
}

/**
 * @param pSsoLoginURI the mSsoLoginURI to set
 */
public void setSsoLoginURI(String pSsoLoginURI) {
	this.mSsoLoginURI = pSsoLoginURI;
}

/**
 * 
 * This method is works as is OOTB behavior if the request URI non other than the agent/login.jsp.
 * if the request URI is turagent/login.jsp it will find the agent item with customized code.
 * @param pRequest DynamoHttpServletRequest
 * @param pResponse DynamoHttpServletResponse
 * @throws IOException Exception
 * @throws ServletException Exception
 * @return RepositoryItem RepositoryItem
 */
protected RepositoryItem findUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException
		{
			if(isLoggingDebug()){
				logDebug("Enter into the TRUCSRFrameworkProfileFormHandler -> findUser()");
			}
			String reuestUri = pRequest.getRequestURI();
			if(isLoggingDebug()){
				logDebug("reuestUri :"+reuestUri);
			}
			if(StringUtils.isNotBlank(getSsoLoginURI()) && StringUtils.isNotBlank(reuestUri) && reuestUri.equalsIgnoreCase(getSsoLoginURI())){
				return findUserAgentWithoutPassword(pRequest, pResponse);
			}else{
				return super.findUser(pRequest, pResponse);
		
			}
		}

/**
 * This method will get the called on the requestURI is truagent/login.jsp.
 * This method will get the user item(Agent item) without having the password property&value.
 * @param pRequest DynamoHttpServletRequest
 * @param pResponse DynamoHttpServletResponse
 * @return RepositoryItem RepositoryItem
 * @throws ServletException Exception
 * @throws IOException Exception
 */
protected RepositoryItem findUserAgentWithoutPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
    throws ServletException, IOException
  {
	if(isLoggingDebug()){
		logDebug("Enter into the TRUCSRFrameworkProfileFormHan -> findUserWithoutPassword()");
	}
    ProfileTools ptools = getProfileTools();
    PropertyManager pmgr = ptools.getPropertyManager();
    String loginPropertyName = pmgr.getLoginPropertyName();
    String login = getStringValueProperty(loginPropertyName);
    
    if ((login != null) && (isTrimProperty(loginPropertyName))) {
      login = login.trim();
    }
    if (isLoggingDebug()) {
      logDebug("findUser: login=" + login);
    }
    if(StringUtils.isBlank(login)){
      if (isLoggingDebug()){
        logDebug("findUser: missing login");}
      String msg = formatUserMessage(TRUCSCConstants.MISSING_LOGIN, pRequest);
      String propertyPath = generatePropertyPath(loginPropertyName);
      addFormException(new DropletFormException(msg, propertyPath, TRUCSCConstants.MISSING_LOGIN));
    }
  
    try
    {
      if (!(getFormError())) {
        if (isLoggingDebug()) {
          logDebug("findUser: no form errors, looking for user=" + login);
        }
        RepositoryItem item = findUserAgent(login, ptools.getProfileRepository(), pRequest, pResponse);

        if (item != null) {
          if (isLoggingDebug()){
            logDebug("findUser: user " + login + " found.");}
          return item;

        }

        if (!(ptools.locateUserFromLogin(login, new Profile(), getLoginProfileType()))) {
        	if (isLoggingDebug()){
        		logDebug("findUser: invalid User name");}
        	addFormException(new DropletException(formatUserMessage(TRUCSCConstants.INVALID_LOGIN, login, pRequest), TRUCSCConstants.INVALID_LOGIN));
        }
      }

    }
    catch (RepositoryException exc)
    {
      addFormException(new DropletException(formatUserMessage(TRUCSCConstants.INVALID_LOGIN, login, pRequest), exc, TRUCSCConstants.INVALID_LOGIN));

      if (isLoggingError()) {
        Object[] args = new Object[TRUCSCConstants.NUMBER_ONE];
        if (login != null){
          args[0] = login;}
        else{
          args[0] = TRUCSCConstants.NULL;}
        logError(ResourceUtils.getMsgResource(TRUCSCConstants.INVALID_LOGIN, "atg.userprofiling.ProfileResources", sResourceBundle, args), exc);

      }

    }

    return null;
  }
	

/**
 * This method will render the Agent repository Item if the valid user name or login id in the agent repository.
 *  
 * @param pLogin String
 * @param pProfileRepository Repository
 * @param pRequest DynamoHttpServeltRequest
 * @param pResponse DynamoHttpServletResponse
 * @return RepositoryItem RepositoryItem
 * @throws RepositoryException Exception
 * @throws ServletException Exception
 * @throws IOException Exception
 */
protected RepositoryItem findUserAgent(String pLogin, Repository pProfileRepository, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	    throws RepositoryException, ServletException, IOException
	    {
		
		if(isLoggingDebug()){
			logDebug("Enter into the TRUCSRFrameworkProfileFormHan -> findUserAgent()");
		}
		return getProfileTools().getItem(pLogin, null, getLoginProfileType());

	    }
	
}