package com.tru.atg.commerce.csr.repository.servlet;

import java.util.Iterator;
import java.util.Map;

import atg.commerce.csr.repository.servlet.CustomCatalogSearchFormHandler;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryView;

import com.tru.commerce.csr.util.TRUCSCConstants;


/**
 * The Class TRUCSRCustomCatalogSearchFormHandler.
 */
public class TRUCSRCustomCatalogSearchFormHandler extends CustomCatalogSearchFormHandler {
	
    /** The m rus item number. */
    private String mRusItemNumber;
    
    /** The m manufacturer style number. */
    private String mManufacturerStyleNumber;
	
	/** The m display name. */
	private String mDisplayName;   
    
	
	
	/**
	 * Gets the rus item number.
	 *
	 * @return the rus item number
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	
	/**
	 * Sets the rus item number.
	 *
	 * @param pRusItemNumber the new rus item number
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		this.mRusItemNumber = pRusItemNumber;
	}

	/**
	 * Gets the manufacturer style number.
	 *
	 * @return the manufacturer style number
	 */
	public String getManufacturerStyleNumber() {
		return mManufacturerStyleNumber;
	}

	
	/**
	 * Sets the manufacturer style number.
	 *
	 * @param pManufacturerStyleNumber the new manufacturer style number
	 */
	public void setManufacturerStyleNumber(String pManufacturerStyleNumber) {
		this.mManufacturerStyleNumber = pManufacturerStyleNumber;
	}

	
	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	
	/**
	 * Sets the display name.
	 *
	 * @param pDisplayName the new display name
	 */
	public void setDisplayName(String pDisplayName) {
		this.mDisplayName = pDisplayName;
	}

	
	/** The m rus item number property. */
	private String mRusItemNumberProperty;
	
	/** The m manufacturer style number property. */
	private String mManufacturerStyleNumberProperty;
	
	/** The m display name property. */
	private String mDisplayNameProperty;
	
	/**
	 * Gets the rus item number property.
	 *
	 * @return the rus item number property
	 */
	public String getRusItemNumberProperty() {
		return mRusItemNumberProperty;
	}

	/**
	 * Sets the rus item number property.
	 *
	 * @param pRusItemNumberProperty the new rus item number property
	 */
	public void setRusItemNumberProperty(String pRusItemNumberProperty) {
		this.mRusItemNumberProperty = pRusItemNumberProperty;
	}

	/**
	 * Gets the manufacturer style number property.
	 *
	 * @return the manufacturer style number property
	 */
	public String getManufacturerStyleNumberProperty() {
		return mManufacturerStyleNumberProperty;
	}

	/**
	 * Sets the manufacturer style number property.
	 *
	 * @param pManufacturerStyleNumberProperty the new manufacturer style number property
	 */
	public void setManufacturerStyleNumberProperty(
			String pManufacturerStyleNumberProperty) {
		this.mManufacturerStyleNumberProperty = pManufacturerStyleNumberProperty;
	}

	/**
	 * Gets the display name property.
	 *
	 * @return the display name property
	 */
	public String getDisplayNameProperty() {
		return mDisplayNameProperty;
	}

	/**
	 * Sets the display name property.
	 *
	 * @param pDisplayNameProperty the new display name property
	 */
	public void setDisplayNameProperty(String pDisplayNameProperty) {
		this.mDisplayNameProperty = pDisplayNameProperty;
	}

	
	
	/**
	 * Generate search query.
	 *
	 * @param pRepository the repository
	 * @param pItemType the item type
	 * @param pQueryBuilder the query builder
	 * @return the query
	 * @throws DropletFormException the droplet form exception
	 * @throws RepositoryException the repository exception
	 */
	protected Query generateSearchQuery(Repository pRepository, String pItemType, QueryBuilder pQueryBuilder)
	 throws DropletFormException, RepositoryException
	 {
		if (isLoggingDebug()) {
			logDebug("BEGIN:::Entering TRUCSRCustomCatalogSearchFormHandler -> generateSearchQuery()");
		}
		int iQueryCount = 0;
		Query oOTBQuery = null;
		Query oStyleNumberCustomQry = null;
  		Query oRusItemNumberCustomQry = null;
		Query oDisplayNameCustomQry = null;
		
		boolean bStyleNumber = Boolean.FALSE;
		boolean bRusNumber = Boolean.FALSE;
		boolean bDisplayName= Boolean.FALSE;
		
		boolean bOOTB = Boolean.TRUE;
		oOTBQuery = super.generateSearchQuery(pRepository, pItemType, pQueryBuilder);
		
		if(oOTBQuery == null ){
			bOOTB = Boolean.FALSE;
			if(StringUtils.isBlank(getManufacturerStyleNumber()) && StringUtils.isBlank(getRusItemNumber()) && StringUtils.isBlank(getDisplayName())){
				return null;
			}
		}
		if(bOOTB){
			++iQueryCount;
		}
		oStyleNumberCustomQry = generateCustomQueryForStyleNumber(pRepository, pItemType, pQueryBuilder);
		oRusItemNumberCustomQry = generateCustomQueryForRusItemNumber(pRepository, pItemType, pQueryBuilder);
		oDisplayNameCustomQry = generateCustomQueryForDisplayName(pRepository, pItemType, pQueryBuilder);
		
		if(oStyleNumberCustomQry !=null && !StringUtils.isBlank(getManufacturerStyleNumber())){
			bStyleNumber = Boolean.TRUE;
			++iQueryCount;
		}
		if(oRusItemNumberCustomQry !=null && !(StringUtils.isBlank(getRusItemNumber()))){
			bRusNumber = Boolean.TRUE;
			++iQueryCount;
		}
		if(oDisplayNameCustomQry !=null && !StringUtils.isBlank(getDisplayName())){
			bDisplayName = Boolean.TRUE;
			++iQueryCount;
		}
		
				
		Query[] oQueries = new Query[iQueryCount];
		iQueryCount = 0;
		if(bOOTB){
			oQueries[iQueryCount++] = oOTBQuery; 
		}
		if (bStyleNumber) {
			oQueries[iQueryCount++] = oStyleNumberCustomQry;
		}
		if (bRusNumber){
			oQueries[iQueryCount++] = oRusItemNumberCustomQry;
		}
		if(bDisplayName){
			oQueries[iQueryCount++] = oDisplayNameCustomQry;
		}
		
		if (isLoggingDebug()) {
			logDebug("END:::Entering TRUCSRCustomCatalogSearchFormHandler -> generateSearchQuery()");
		}
		return pQueryBuilder.createAndQuery(oQueries);
	 }

		
	
	/**
	 * Generate custom query for style number.
	 *
	 * @param pRepository the repository
	 * @param pItemType the item type
	 * @param pQueryBuilder the query builder
	 * @return the query
	 * @throws RepositoryException the repository exception
	 */
	protected Query generateCustomQueryForStyleNumber(Repository pRepository, String pItemType, 
				QueryBuilder pQueryBuilder) throws RepositoryException {
		    if (isLoggingDebug()) {
		    	logDebug("BEGIN:::Entering TRUCSRCustomCatalogSearchFormHandler -> generateCustomQueryForStyleNumber()");
			}
		    RepositoryView oRepositoryView = getRepositoryView(pRepository, pItemType);
		    QueryBuilder oQueryBuilder = oRepositoryView.getQueryBuilder();
		    QueryExpression oStyleNumber = oQueryBuilder.createPropertyQueryExpression(getManufacturerStyleNumberProperty());
		    QueryExpression oStyleNumberProperty = oQueryBuilder.createConstantQueryExpression(getManufacturerStyleNumber());
		    Query query = oQueryBuilder.createComparisonQuery(oStyleNumber, oStyleNumberProperty, QueryBuilder.EQUALS);
		   
		    Object catalog = getCurrentCatalog();
		    String[] catalogIds = getCatalogs();
		   
		    if (isLoggingDebug()) {
		    	logDebug("Current catalog: " + catalog);
		    }
		    
	        if ((catalog == null) && (((catalogIds == null) || (catalogIds.length == 0)))) {
	          if (isNullCatalogMeansNoResults()) {
	        	return null;
	         }
	          return query;
	        }
	        
	        //OOTB 
	        if ((catalogIds != null) && (catalogIds.length > 0)) {
	        	return generateCatalogsQueryClause(pQueryBuilder, query, catalogIds);
	        }
	        //OOTB
	        if (isQueryByCatalog()){
	        	return generateCatalogQueryClause(pQueryBuilder, query, catalog);
	        }
		    return query;
		}	
	

	/**
	 * Generate custom query for display name.
	 *
	 * @param pRepository the repository
	 * @param pItemType the item type
	 * @param pQueryBuilder the query builder
	 * @return the query
	 * @throws RepositoryException the repository exception
	 */
	protected Query generateCustomQueryForDisplayName(Repository pRepository, String pItemType, 
				QueryBuilder pQueryBuilder) throws RepositoryException {
		    if (isLoggingDebug()) {
		    	logDebug("BEGIN:::Entering TRUCSRCustomCatalogSearchFormHandler -> generateCustomQueryForDisplayName()");
			}
		    RepositoryView oRepositoryView = getRepositoryView(pRepository, pItemType);
		    QueryBuilder oQueryBuilder = oRepositoryView.getQueryBuilder();
		    QueryExpression oDisplayName = oQueryBuilder.createPropertyQueryExpression(getDisplayNameProperty());
		    QueryExpression oDisplayNameProperty = oQueryBuilder.createConstantQueryExpression(getDisplayName());
		    Query query = oQueryBuilder.createComparisonQuery(oDisplayName, oDisplayNameProperty, QueryBuilder.EQUALS);
		   
		    Object catalog = getCurrentCatalog();
		    String[] catalogIds = getCatalogs();
		   
		    if (isLoggingDebug()) {
		    	logDebug("Current catalog: " + catalog);
		    }
		    
	        if ((catalog == null) && (((catalogIds == null) || (catalogIds.length == 0)))) {
	          if (isNullCatalogMeansNoResults()) {
	        	return null;
	         }
	          return query;
	        }
	        
	        //OOTB 
	        if ((catalogIds != null) && (catalogIds.length > 0)) {
	        	return generateCatalogsQueryClause(pQueryBuilder, query, catalogIds);
	        }
	        //OOTB
	        if (isQueryByCatalog()){
	        	return generateCatalogQueryClause(pQueryBuilder, query, catalog);
	        }
		    return query;
		}	
	

	
	/**
	 * Generate custom query for rus item number.
	 *
	 * @param pRepository the repository
	 * @param pItemType the item type
	 * @param pQueryBuilder the query builder
	 * @return the query
	 * @throws RepositoryException the repository exception
	 */
	protected Query generateCustomQueryForRusItemNumber(Repository pRepository, String pItemType, 
				QueryBuilder pQueryBuilder) throws RepositoryException {
		    if (isLoggingDebug()) {
		    	logDebug("BEGIN:::Entering TRUCSRCustomCatalogSearchFormHandler -> generateCustomQueryForRusItemNumber()");
			}
		    RepositoryView oRepositoryView = getRepositoryView(pRepository, pItemType);
		    QueryBuilder oQueryBuilder = oRepositoryView.getQueryBuilder();
		    QueryExpression oRusNumber = oQueryBuilder.createPropertyQueryExpression(getRusItemNumberProperty());
		    QueryExpression oRusNumberProperty = oQueryBuilder.createConstantQueryExpression(getRusItemNumber());
		    Query query = oQueryBuilder.createComparisonQuery(oRusNumber, oRusNumberProperty, QueryBuilder.EQUALS);
		   
		    Object catalog = getCurrentCatalog();
		    String[] catalogIds = getCatalogs();
		   
		    if (isLoggingDebug()) {
		    	logDebug("Current catalog: " + catalog);
		    }
		    
	        if ((catalog == null) && (((catalogIds == null) || (catalogIds.length == 0)))) {
	          if (isNullCatalogMeansNoResults()) {
	        	return null;
	         }
	          return query;
	        }
	        
	        //OOTB 
	        if ((catalogIds != null) && (catalogIds.length > 0)) {
	        	return generateCatalogsQueryClause(pQueryBuilder, query, catalogIds);
	        }
	        //OOTB
	        if (isQueryByCatalog()){
	        	return generateCatalogQueryClause(pQueryBuilder, query, catalog);
	        }
		    return query;
		}	
	
		
	/**
	 * Checks if is advanced search property values empty.
	 *
	 * @return true, if is advanced search property values empty
	 */
	@Override
	  protected boolean isAdvancedSearchPropertyValuesEmpty()
	  {
	    Iterator r1 = getAdvancedSearchPropertyValues().values().iterator();
	    while (r1.hasNext())
	    {
	      Iterator r2 = ((Map)r1.next()).values().iterator();
	      while (r2.hasNext()) {
	        Object tmp = r2.next();
	        if (tmp instanceof String) {
	          if (!(StringUtils.isBlank((String)tmp))) {
	            return false;
	          }

	        }
	        else if (tmp != null) {
	          return false;
	        }
	      }
	    }
	    if(StringUtils.isNotBlank(getManufacturerStyleNumber()) || StringUtils.isNotBlank(getRusItemNumber()) || StringUtils.isNotBlank(getDisplayName())){
	    return false;
	    }
	    return true;
	  }
	
	/**
	 * Checks if is advanced search property ranges empty.
	 *
	 * @return true, if is advanced search property ranges empty
	 */
	@Override
	protected boolean isAdvancedSearchPropertyRangesEmpty()
	  {
	    Iterator r1 = getAdvancedSearchPropertyRanges().values().iterator();
	    while (r1.hasNext())
	    {
	      Iterator r2 = ((Map)r1.next()).values().iterator();
	      while (r2.hasNext()) {
	        Map tmp = (Map)r2.next();
	        if ((tmp.get(TRUCSCConstants.MAX) != null) || (tmp.get(TRUCSCConstants.MIN) != null)) {
	          return false;
	        }
	      }
	    }
	    if(StringUtils.isNotBlank(getManufacturerStyleNumber()) || StringUtils.isNotBlank(getRusItemNumber()) || StringUtils.isNotBlank(getDisplayName())){
		    return false;
		    }
	    return true;
	  }
	
/*

*//**
 * Perform search.
 *
 * @param pRequest the request
 * @param pResponse the response
 * @return true, if successful
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 *//*
@SuppressWarnings("unchecked")
@Override
protected boolean performSearch(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	    throws ServletException, IOException
	  {
	 List<RepositoryItem> subResultAfter = new ArrayList<RepositoryItem>();
	 List<RepositoryItem> subResultBefore = new ArrayList<RepositoryItem>();
	RepositoryItem product;
	List<TRUCSRProductItemDetailsVO> itemList = new ArrayList<TRUCSRProductItemDetailsVO>();
	    prepare(pRequest, pResponse);

	    HashMap target = getAdvancedSearchPropertyValues();
	    String[] types = getItemTypes();

	    Iterator valSet = getPropertyValues().entrySet().iterator();
	    while (valSet.hasNext()) {
	      Map.Entry val = (Map.Entry)valSet.next();
	      for (int i = 0; i < types.length; ++i) {
	        ((HashMap)target.get(types[i])).put(val.getKey(), val.getValue());
	      }
	    }
	    if ((!(getAllowEmptySearch())) && (areSearchValuesEmpty())) {
	      super.handleSearch(pRequest, pResponse);
	    }
	    if ((isEnableCountQuery()) && (getItemTypes().length > TRUCSCConstants.NUMBER_ONE)) {
	      return super.handleSearch(pRequest, pResponse);
	    }
	    this.mStartIndex = ((getCurrentResultPageNum() - TRUCSCConstants.NUMBER_ONE ) * getMaxResultsPerPage());
	    this.mEndIndex = (this.mStartIndex + getMaxResultsPerPage());
	    setResultSetSize(0);
	    this.mSearchResults.clear();
	    this.mSearchResultsByItemType.clear();
	     subResultBefore =  (List<RepositoryItem>) generateResultSet(getItemTypes()[0]);
	     for (int i=0; i<subResultBefore.size();i++) {
	        itemList = ((TRUCSRCatalogTools)getCatalogTools()).productItemDetails(subResultBefore.get(i));
	        product  = subResultBefore.get(i);
			if(!itemList.isEmpty()){
				for(TRUCSRProductItemDetailsVO itemVO : itemList){
					RepositoryItem sku =itemVO.getSkuItem();
					Boolean webDisplayFlag = ((TRUCSRCatalogTools)getCatalogTools()).getSkuItemWebDisplay(sku);
					Boolean supInSearch=((TRUCSRCatalogTools)getCatalogTools()).getSkuItemSuppressSearch(sku);
					//Boolean superDisFlag=getCatalogTools().getSkuItemSuperDisplay(sku);
					Boolean deleteFlag=((TRUCSRCatalogTools)getCatalogTools()).getSkuItemDeletedFlag(sku);
					//Boolean unCartableFlag=getCatalogTools().getSkuItemUncartableFlag(sku);
					String type=((TRUCSRCatalogTools)getCatalogTools()).getSkuItemTypeProperty(sku);
					
				    //if(deleteFlag!=null && !deleteFlag && webDisplayFlag!=null && webDisplayFlag && StringUtils.isNotBlank(type) && !type.equalsIgnoreCase(TRUCSCConstants.NON_MERCH_SKU) && supInSearch!=null && !supInSearch && superDisFlag!=null && !superDisFlag)
					if(deleteFlag!=null && !deleteFlag && webDisplayFlag!=null && webDisplayFlag && StringUtils.isNotBlank(type) && !type.equalsIgnoreCase(TRUCSCConstants.NON_MERCH_SKU) && supInSearch!=null && !supInSearch)
				    {
				     subResultAfter.add(product);
				    }
				}
	        }
	    }
	    if (subResultAfter != null) {
	      this.mSearchResults.addAll(subResultAfter);
	      setResultSetSize(subResultAfter.size());
	      this.mSearchResultsByItemType.put(getItemTypes()[0], subResultAfter);
	    }
	    if (isLoggingDebug()) {
	      int size = (subResultAfter != null) ? subResultAfter.size() : 0;
	      logDebug("resultSet for type: " + getItemTypes()[0] + ", size: " + size + ", results: " + subResultAfter);

	    }
	    this.mPreviouslySubmitted = true;
	    this.mEndIndex = (this.mStartIndex + getResultSetSize());
	   
	    return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
	}*/
}


