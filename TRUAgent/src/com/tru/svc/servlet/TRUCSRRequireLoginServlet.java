/**
 * 
 */
package com.tru.svc.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.nucleus.WindowScopeManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
import atg.svc.repository.service.StateHolderService;
import atg.svc.servlet.RequireLoginServlet;
import atg.svc.state.StateHolderAttributeMap;
import atg.userprofiling.Profile;

import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.svc.agent.ui.TRUCSRAgentUIConfiguration;

/**
 * This class extends ATG OOTB RequireLoginServlet and used maintain global
 * configuration values for the Commerce Service Center application. 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRRequireLoginServlet extends RequireLoginServlet{
	/** Constant to hold the STATE_HOLDER_ATTRIBUTE_MAP value. */
	private static final String STATE_HOLDER_ATTRIBUTE_MAP = "/atg/svc/state/StateHolderAttributeMap";
	/** The Constant MISSING_LOGIN. */
	private TRUCSRAgentUIConfiguration mUIConfiguration = null;
	
	 /**
	 * To Holds String Array object
	 */
	private String[] mRequestParameters = null;

	/**
	 * @param pRequestParameters To set the requestParameters pRequestParameters
	 */
	public void setRequestParameters(String[] pRequestParameters)
	  {
	    this.mRequestParameters = pRequestParameters;
	  }


	/** 
	 * To get the getRequestParameters
	 * @return String[] mRequestParameters()
	 * 
	 */
	public String[] getRequestParameters()
	  {
	    return this.mRequestParameters;
	  }
	  
	
	 /**
	 * To Holds String object
	 */
	private String mHdnUserName;
		
	 /**
	 * To Holds String object
	 */
	private String mHdnSecurityLevel;
	
	/**
	 * to Holds the string object
	 */
	private String mConfigSecurityVal;
	

	/**
	 * @return the configSecurityVal
	 */
	public String getConfigSecurityVal() {
		return mConfigSecurityVal;
	}


	/**
	 * @param pConfigSecurityVal the configSecurityVal to set
	 */
	public void setConfigSecurityVal(String pConfigSecurityVal) {
		if(StringUtils.isBlank(pConfigSecurityVal)){
			pConfigSecurityVal = TRUCSCConstants.NUMBER_FOUR;
		}
		this.mConfigSecurityVal = pConfigSecurityVal;
	}


	/**
	 * @return the hdnUserName
	 */
	public String getHdnUserName() {
		return mHdnUserName;
	}


	/**
	 * @param pHdnUserName the hdnUserName to set
	 */
	public void setHdnUserName(String pHdnUserName) {
		if(StringUtils.isBlank(pHdnUserName)){
			pHdnUserName = TRUCSCConstants.HDN_USERNAME;
		}
		this.mHdnUserName = pHdnUserName;
	}


	/**
	 * @return the hdnSecurityLevel
	 */
	public String getHdnSecurityLevel() {
		return mHdnSecurityLevel;
	}


	/**
	 * @param pHdnSecurityLevel the hdnSecurityLevel to set
	 */
	public void setHdnSecurityLevel(String pHdnSecurityLevel) {
		if(StringUtils.isBlank(pHdnSecurityLevel)){
			pHdnSecurityLevel =TRUCSCConstants.HDN_SECURITYLEVEl;
		}
		this.mHdnSecurityLevel = pHdnSecurityLevel;
	}


	/** this method is used to work as is but the added new logic which is getting update the requestURI.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws IOException IOException
	 * @throws ServletException ServletException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
    throws IOException, ServletException
		  {
			if(isLoggingDebug()){
				logDebug("Start the TRUCSRRequireLoginServlet -> service()");
			}
		    boolean isRedirect = false;

		    if ((pRequest == null) || (pRequest.getSession() == null)) {
		      passRequest(pRequest, pResponse);
		      return;
		    }

		    ServletContext servletContext = pRequest.getSession().getServletContext();
		    if (servletContext == null) {
		      passRequest(pRequest, pResponse);
		      return;
		    }

		    String isRequireLoginEnabled = servletContext.getInitParameter(TRUCSCConstants.REQUIRE_LOGIN_ENABLED);
		    if ((isRequireLoginEnabled != null) && (isRequireLoginEnabled.equals(Boolean.TRUE.toString()))) {
		      if (this.mProfileComponentName == null){
		        throw new ServletException(TRUCSCConstants.REQUIRE_LOGIN_PROFILE_RESTRICTION);}
		      if (this.mUIConfigurationComponentName == null){
		        throw new ServletException(TRUCSCConstants.REQUIRE_LOGIN_UI_RESTRICTION);}
		      this.mUIConfiguration = ((TRUCSRAgentUIConfiguration)Nucleus.getGlobalNucleus().resolveName(this.mUIConfigurationComponentName));
		      if (this.mUIConfiguration == null) {
		        throw new ServletException(new StringBuilder().append(TRUCSCConstants.UI_CONFIGURATION_START).append(this.mUIConfigurationComponentName).append(TRUCSCConstants.UI_CONFIGURATION_END).toString());
		      }

		      String windowId = WindowScopeManager.getCurrentWindowId();
		      if ((windowId != null) && (windowId.isEmpty())) {
		        windowId = null;

		      }

		      StateHolderAttributeMap attributeMap = (StateHolderAttributeMap)ServletUtil.getCurrentRequest().resolveName(STATE_HOLDER_ATTRIBUTE_MAP, true);

		      boolean isSessionInvalid = false;


		      if (windowId != null) {
		        isSessionInvalid = !(getWindowIdManager().isHasWindowId(windowId));

		      }

		      String requestUri = pRequest.getRequestURI();

		      boolean hasValidUriExtension = false;
		      for (int i = 0; (i < this.mValidUriExtensions.length) && (requestUri != null); ++i) {
		        if (requestUri.endsWith(this.mValidUriExtensions[i])) {
		          hasValidUriExtension = true;
		          break;
		        }
		      }

		      if ((hasValidUriExtension) && (!(ServletUtil.inInclude(pRequest)))) {
		        DynamoHttpServletRequest dynRequest = ServletUtil.getCurrentRequest();
		        Profile profile = (Profile)dynRequest.resolveName(this.mProfileComponentName);
		        String loginURI = this.mUIConfiguration.getLoginURI();
		        String hdnSecurityLevel = pRequest.getParameter(getHdnSecurityLevel());
		        String userName = pRequest.getParameter(getHdnUserName());
		        if(StringUtils.isNotBlank(userName)){
		        	loginURI = this.mUIConfiguration.getTRULoginURI();
		        }
		        Map additionalParametersMap = null;
		        if (this.mRequestParameters != null) {
		          for (int i = 0; (i < this.mRequestParameters.length) && (pRequest != null); ++i) {
		            String requestParamName = this.mRequestParameters[i];
		            if (pRequest.getParameter(requestParamName) != null) {
		              String paramValue = pRequest.getParameter(requestParamName);
		              if ((paramValue != null) && (additionalParametersMap == null)) {
		                  additionalParametersMap = new HashMap();
		                  additionalParametersMap.put(requestParamName, paramValue);
		              }
		            }
		          }

		        }

		        if (isLoggingDebug()) {
					logDebug(new StringBuilder()
							.append(" \n In handleDoFilter: ").append((profile == null) ? "Profile=null"
									: new StringBuilder().append("Profile.transient=").append(profile.isTransient()).toString())
							.append(" \n loginURI=").append(loginURI).append(" \n contextPath=").append(dynRequest.getContextPath())
							.append(" \n pathInfo=").append(dynRequest.getPathInfo()).append(" \n requestURI=")
							.append(dynRequest.getRequestURI()).append(" \n enabled=")
							.append(this.mEnabled).append(" \n loginRequired=").append(this.mUIConfiguration.isLoginRequired())
							.append(" \n isSuggestedLoginEnabled=").append(this.mUIConfiguration
									.isSuggestedLoginEnabled()).append(" \n suggestedLoginParam=")
							.append(this.mUIConfiguration.getSuggestedLoginParam())
							.append(" \n request suggestedLoginParam value=").append(dynRequest
									.getParameter(this.mUIConfiguration.getSuggestedLoginParam())).toString());

		        }

		        boolean shouldRedirectToLogin = false;
		        String url = null;

		        if (isLoggingDebug()) {
					logDebug(new StringBuilder().append("\n AAA shouldRedirectToLogin is initially ")
							.append(shouldRedirectToLogin).append("\n\n").toString());

		        }

		        if ((this.mUIConfiguration.isLoginRequired()) && (this.mEnabled))
		        {
		          if (isLoggingDebug()) {
		            logDebug("\n loginRequired is true and enabled is true\n");
		          }

		          shouldRedirectToLogin = (profile == null) || (profile.isTransient());
		          if (isLoggingDebug() && (profile != null)) {
		            logDebug(new StringBuilder().append("\n BBB \nprofile ").append(profile).append("\n").toString());
		            logDebug(new StringBuilder().append("\ntransient =").append(profile.isTransient()).append("\n").toString());
		            logDebug(new StringBuilder().append("\nID = ").append(profile.getRepositoryId()).append("\n").toString());
		            logDebug(new StringBuilder().append("\n shouldRedirectToLogin  = ").append(shouldRedirectToLogin).append("\n\n").toString());

		          }

		          if ((loginURI != null) && (shouldRedirectToLogin)) {
		            url = dynRequest.encodeURL(loginURI);
		          }

		          if ((shouldRedirectToLogin) && (!(this.mUIConfiguration.isEnableRequireLoginFilterForEmailTemplate()))) {
		            String[] templatePaths = this.mUIConfiguration.getEmailTemplatePaths();
		            String pathInfo = dynRequest.getPathInfo();
		            if (templatePaths != null) {
		              for (int i = 0; i < templatePaths.length; ++i) {
		                String templatePath = templatePaths[i];
		                if ((templatePath != null) && (((templatePath.equals(pathInfo)) || (templatePath.equals(requestUri))))) {
		                  shouldRedirectToLogin = false;
		                  break;
		                }
		              }
		            }
		          }
		        }

		        String isDPSLogout = pRequest.getParameter(TRUCSCConstants.DPSLOGOUT);
		        if ((isDPSLogout != null) && (isDPSLogout.equalsIgnoreCase(Boolean.TRUE.toString()))) {
		          shouldRedirectToLogin = false;
		        }

		        if ((shouldRedirectToLogin) && (url != null)) {
		          if (url.indexOf(TRUCSCConstants.QUESTIONMARK) > 0) {
		            url = new StringBuilder().append(url).append(TRUCSCConstants.STRING_AND).toString();
		          }
		          else {
		            url = new StringBuilder().append(url).append(TRUCSCConstants.QUESTIONMARK).toString();
		          }
		         
		          url = new StringBuilder().append(url).append(TRUCSCConstants.SESSION_INVALID).append(Boolean.toString(isSessionInvalid)).toString();

		        
		          if(StringUtils.isNotBlank(userName)){
		        	  url = new StringBuilder().append(url).append(TRUCSCConstants.AND_USER_NAME).append(userName).toString();
		        	  pRequest.setAttribute(TRUCSCConstants.USERNAME, userName);
		          }
		        
		          if(StringUtils.isNotBlank(hdnSecurityLevel) && hdnSecurityLevel.equalsIgnoreCase(TRUCSCConstants.CONSTANT_TRUE)){
		        	  url = new StringBuilder().append(url).append(TRUCSCConstants.AND_SSOLOGIN).append(hdnSecurityLevel).toString();
		        	  pRequest.setAttribute(TRUCSCConstants.SSOLOGIN, hdnSecurityLevel);
		          }
		          
		          String isAjax = pRequest.getParameter(TRUCSCConstants.ISPPR);
		          if ((isAjax != null) && (isAjax.equalsIgnoreCase(Boolean.TRUE.toString()))) {
		            url = new StringBuilder().append(url).append(TRUCSCConstants.AND_PPR).append(Boolean.TRUE.toString()).toString();
		          }

		          if ((windowId == null) || (isSessionInvalid))
		          {
		            windowId = createNewWindowId(pRequest);


		            attributeMap.setAttributes(windowId, new HashMap(TRUCSCConstants.SIX_FOUR));

		            StateHolderService.getInstance().setAttribute(TRUCSCConstants.IS_NEW_WINDOW_ID, Boolean.TRUE);
		            url = new StringBuilder().append(url).append(TRUCSCConstants.AND_WINDOW_ID).append(windowId).toString();



		            if (additionalParametersMap != null) {
		              Set paramValues = additionalParametersMap.entrySet();
		              Iterator additionalParamsIterator = paramValues.iterator();
		              while (additionalParamsIterator.hasNext()) {
		                Map.Entry anEntry = (Map.Entry)additionalParamsIterator.next();
		                String param = (String)anEntry.getKey();
		                String value = (String)anEntry.getValue();
		                url = new StringBuilder().append(url).append(TRUCSCConstants.STRING_AND).append(param).append(TRUCSCConstants.STRING_EQUAL).append(value).toString();
		              }

		            }

		            isRedirect = true;
		            if (isLoggingDebug()) {
		              logDebug(new StringBuilder().append(TRUCSCConstants.NON_LOCAL_LOGIN_START).append(url).append(TRUCSCConstants.NON_LOCAL_LOGIN_END).toString());
		            }
		            pResponse.sendRedirect(url);
		          }
		          else
		          {
		            StateHolderService.getInstance().setAttribute(TRUCSCConstants.IS_NEW_WINDOW_ID, Boolean.FALSE);
		          }
		        }
		      }
		    }

		    if (!(isRedirect)){
		      passRequest(pRequest, pResponse);}
		  }
	
}