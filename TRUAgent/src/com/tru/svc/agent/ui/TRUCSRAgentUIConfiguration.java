/**
 * 
 */
package com.tru.svc.agent.ui;

import atg.svc.agent.ui.AgentUIConfiguration;

/**
 * This class extends ATG OOTB AgentUIConfiguration and used maintain global
 * configuration values for the Commerce Service Center application. 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRAgentUIConfiguration extends AgentUIConfiguration{

	/**
	 * Property to hold the mTruContextRoot.
	 */
	private String mTruContextRoot;
	

	/**
	 * Property to hold the mTRULoginURL.
	 */
	private String mTRULoginURI;

	/**
	 * @return the mTRULoginURI
	 */
	public String getTRULoginURI() {
		return mTRULoginURI;
	}

	/**
	 * @param pTRULoginURI the mTRULoginURI to set
	 */
	public void setTRULoginURI(String pTRULoginURI) {
		this.mTRULoginURI = pTRULoginURI;
	}

	/**
	 * @return the mTruContextRoot
	 */
	public String getTruContextRoot() {

		return mTruContextRoot;
	}

	/**
	 * @param pTruContextRoot
	 *            the truContextRoot to set
	 */
	public void setTruContextRoot(String pTruContextRoot) {

		mTruContextRoot = pTruContextRoot;
	}
	
}