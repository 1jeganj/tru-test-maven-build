package com.tru.commerce.claimable;

import atg.commerce.csr.claimable.CSRClaimableManager;
/**
 * class 'TRUCSRClaimableManager' extends the TRU CSC Specific functionalities OOTB 'CSRClaimableManager'.
 * 
 * @author Professional Access
 * @version 1.0
 *
 *
 */
public class TRUCSRClaimableManager extends CSRClaimableManager{
	
	/**
	 * This method is overridden to apply single use coupon and update the max uses property of a coupon only 
	 * when the coupon is not single use coupon
	 *
	 * @param pProfileId the profile id
	 * @param pCouponClaimCode the coupon claim code
	 * @throws ClaimableException the claimable exception
	 */
	/*@Override
	public void claimCoupon(String pProfileId, String pCouponClaimCode) throws ClaimableException
 {
		if (isLoggingDebug()) {
		logDebug("Enter into [Class: TRUClaimableManager  method: claimCoupon]");
		vlogDebug("pProfileId:{0} :: pCouponClaimCode:{1}", pProfileId ,pCouponClaimCode);
		}
		TransactionDemarcation td = new TransactionDemarcation();
		try {
			td.begin(getTransactionManager(), TRUCommerceConstants.INT_THREE);
			if(isLoggingDebug()){
				vlogDebug(TRUConstants.PROF_ID, new Object[] { pProfileId });
			}
			RepositoryItem profile = getPromotionTools().getProfileRepository().getItemForUpdate(pProfileId,
					getPromotionTools().getProfileItemType());
			if (profile == null) {
				throw new ClaimableException(TRUConstants.NULL_PROFILE, ResourceUtils.getMsgResource(TRUConstants.NULL_PROFILE,
						TRUConstants.PROMOTION_RESOURCES, sPromotionResourceBundle));

			}
			RepositoryItem coupon = findAndClaimCoupon(pCouponClaimCode);
			Map couponPromos = new HashMap();
			if (checkMaxCouponLimit(couponPromos, profile)){
				handleMaxCouponLimitReached(couponPromos, (MutableRepositoryItem) profile);
			}
			else if (getMaxCouponsPerProfile() == 0) {
				throw new ClaimableException(TRUConstants.CLAIMING_COUPON_NOTALLOWED, ResourceUtils.getMsgResource(TRUConstants.CLAIMING_COUPON_NOTALLOWED,
						TRUConstants.PROMOTION_RESOURCES, sPromotionResourceBundle));
			}
			try {
				Integer value = (Integer) coupon.getPropertyValue(getClaimableTools().getUsesPropertyName());
				int uses = (value != null) ? value.intValue() : TRUConstants.ZERO;
				value = (Integer) coupon.getPropertyValue(getClaimableTools().getMaxUsesPropertyName());
				int maxUses = (value != null) ? value.intValue() : TRUConstants.INT_MINUS_ONE;
				if (maxUses > TRUConstants.INT_MINUS_ONE) {
					if (uses >= maxUses) {
						Object[] args = { pCouponClaimCode };
						ClaimableException e = new ClaimableException(TRUConstants.NO_COUPON_USES, ResourceUtils.getMsgResource(TRUConstants.NO_COUPON_USES,
								TRUConstants.PROMOTION_RESOURCES, sPromotionResourceBundle, args));
						e.setMessageArguments(args);
						throw e;
					}
					if (isLoggingDebug()) {
						vlogDebug("maxUses:{0} :: }", maxUses);
						}
					if (maxUses != TRUConstants.INTEGER_NUMBER_ONE) {
						DynamicBeans.setPropertyValue(coupon, getClaimableTools().getUsesPropertyName(), Integer.valueOf(uses +TRUConstants.INTEGER_NUMBER_ONE));
						((MutableRepository) getClaimableTools().getClaimableRepository()).updateItem((MutableRepositoryItem) coupon);
					}
				}
				if(isLoggingDebug()){
				vlogDebug(TRUConstants.PROMOTION_PROPERTY, new Object[] { getClaimableTools().getPromotionsPropertyName() });
				}
				PromotionTools promotionTools = getPromotionTools();
				Set<RepositoryItem> multiplePromotions = (Set) coupon.getPropertyValue(getClaimableTools().getPromotionsPropertyName());
				int grantPromotionExceptionCount;
				if (multiplePromotions != null) {
					vlogDebug(TRUConstants.MULTIPLE_PROMOTIONS, new Object[0]);
					grantPromotionExceptionCount = TRUConstants.ZERO;
					for (RepositoryItem promotion : multiplePromotions) {
						try {
							if(isLoggingDebug()){
							vlogDebug(TRUConstants.PROMOTION_DESCRIPTOR, new Object[] { promotion.getItemDescriptor() });
							vlogDebug(TRUConstants.PROMOTION_TO_BE_GRANTED, new Object[] { promotion.getRepositoryId() });
							}
							String currentSiteId = SiteContextManager.getCurrentSiteId();
							promotionTools.grantPromotion((MutableRepositoryItem) profile, promotion, null, currentSiteId, coupon);
						} catch (PromotionException ex) {
							++grantPromotionExceptionCount;

							if (grantPromotionExceptionCount == multiplePromotions.size()) {
								throw ex;
							}
						}
					}
				}
				if ((multiplePromotions == null) || (multiplePromotions.isEmpty())) {
					throw new ClaimableException(TRUConstants.NO_COUPON_PROMOTIONS, ResourceUtils.getMsgResource(TRUConstants.NO_COUPON_PROMOTIONS,
							TRUConstants.PROMOTION_RESOURCES, sPromotionResourceBundle));
				}
				promotionTools.initializePricingModels();
				
			} catch (PromotionException pe) {
				throw new ClaimableException(pe);
			}
		} catch (ClaimableException ce) {
			throw ce;
		} catch (Exception exc) {
			if (isLoggingError()) {
				logError(exc);
			}
			throw new ClaimableException(exc);
		} finally {
			try {
				td.end();
			} catch (TransactionDemarcationException tde) {
				if (isLoggingError()){
					logError(tde);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("exit of  [Class: TRUClaimableManager  method: claimCoupon]");
			}
	}*/

}
