package com.tru.commerce.profile;

import java.beans.IntrospectionException;
import java.util.Map;

import javax.jms.JMSException;

import atg.beans.PropertyNotFoundException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.tru.userprofiling.TRUPropertyManager;


/**
 * This class is a tools class for profile
 * in order to  fetch email value.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class TRUCSRProfileTools extends TRUProfileTools{
	
	/** This method fetches the email value . */
	
	private String mCreditCardNickName;
	
	/**
	 * Gets the credit card nick name.
	 *
	 * @return the credit card nick name
	 */
	public String getCreditCardNickName() {
		return mCreditCardNickName;
	}


	/**
	 * Sets the credit card nick name.
	 *
	 * @param pCreditCardNickName the new credit card nick name
	 */
	public void setCreditCardNickName(String pCreditCardNickName) {
		this.mCreditCardNickName = pCreditCardNickName;
	}


	/**
	 * Fetch email.
	 *
	 * @param pProfile the profile
	 * @return the string
	 */
	public String fetchEmail(Profile pProfile){
		  
		  if(isLoggingDebug()){
				logDebug("TRUCSRProfileTools (fetchEmail) : Starts");
			}
		String emailValue = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
		  if(isLoggingDebug()){
				logDebug("TRUCSRProfileTools (fetchEmail) : Ends");
			}
		return emailValue;
	}
	
	
	/**
	 * Update generated pwd property.
	 *
	 * @param pProfile the profile
	 */
	public void updateGeneratedPwdProperty(Profile pProfile)
	{
		  if(isLoggingDebug()){
				logDebug("TRUCSRProfileTools (updateGeneratedPwdProperty) : Starts");
			}
		MutableRepository profileRepository = (MutableRepository)pProfile.getRepository();
	      MutableRepositoryItem mutableProfileItem;
		try {
			mutableProfileItem = profileRepository.getItemForUpdate(pProfile.getRepositoryId(), pProfile.getItemDescriptor().getItemDescriptorName());
			if (mutableProfileItem != null) {
			mutableProfileItem.setPropertyValue(getPropertyManager().getGeneratedPasswordPropertyName(), Boolean.TRUE);
			profileRepository.updateItem(mutableProfileItem);
			}else {
				if (isLoggingDebug()) {
					vlogDebug(" There is no profile Item in Repository for the mentioned profileID ", pProfile.getRepositoryId());
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {	
				logError("RepositoryException", e);	
			}
		}
		  if(isLoggingDebug()){
				logDebug("TRUCSRProfileTools (updateGeneratedPwdProperty) : Ends");
			}
	}
	
	/**
	 * This method used to send the email to user by calling the EpslonAccountSource.
	 * 
	 * @param pEmail
	 *            - String email property
	 */
	public void sendEpslonEmailToUser(String pEmail) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEpslonEmailToUser) : STARTS");
		}
		vlogDebug("EpslonAccountSource()" + getEpslonAccountSource());
		if (getEpslonAccountSource() != null) {
			try {
				getEpslonAccountSource().sendNewAccountEmail(pEmail);
			} catch (JMSException jmsException) {
				if (isLoggingError()) {
					vlogError("JSONException occured while sending email to epsilon with exception : {0}", jmsException);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (sendEpslonEmailToUser) : ENDS");
		}
	}
	

	/**
	 * This method used to create the credit card with the new address entered by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pAddressValue
	 *            The map to hold the address details entered by the user
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pBillingAddressNickName
	 *            The billing address nick name to add address to secondary address
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws PropertyNotFoundException
	 *             PropertyNotFoundException
	 */
	public void addCardWithNewAddress(Profile pProfile, Map<String, String> pAddressValue,
			Map<String, String> pCreditCardInfoMap, String pBillingAddressNickName) throws RepositoryException,
			IntrospectionException, PropertyNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithNewAddress) : STARTS");
		}
		final String creditCardNickName = createProfileCreditCard(pProfile, pCreditCardInfoMap, null, pAddressValue, null);
		setCreditCardNickName(creditCardNickName);
		final RepositoryItem creditCard = getCreditCardByNickname(creditCardNickName, pProfile);
		if (creditCard != null) {
			final RepositoryItem billingAddressFromCard = (RepositoryItem) creditCard
					.getPropertyValue(((TRUPropertyManager) getPropertyManager()).getBillingAddressPropertyName());
			addProfileRepositoryAddress(pProfile, pBillingAddressNickName, billingAddressFromCard);
//			String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
		/*	if(pProfile.isTransient()){
				getSETALogger().logAddCreditCard(login);
				getSETALogger().logAddBillingAddress(login, (String)billingAddressFromCard.getRepositoryId());
			}*/
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithNewAddress) : ENDS");
		}
	}
	
	/**
	 * This method used to create the credit card with the address selected by the customer.
	 * 
	 * @param pProfile
	 *            The current user profile
	 * @param pSelectedAddress
	 *            The selected address to be set as billing address
	 * @param pCreditCardInfoMap
	 *            The map to hold the card details entered by the user
	 * @param pAddressValue
	 *            address value map to pass to the update second. address method
	 * @throws RepositoryException
	 *             RepositoryException
	 * @throws IntrospectionException
	 *             IntrospectionException
	 * @throws InstantiationException
	 *             InstantiationException
	 * @throws IllegalAccessException
	 *             IllegalAccessException
	 * @throws ClassNotFoundException
	 *             ClassNotFoundException
	 */
	public void addCardWithExistingAddress(Profile pProfile, String pSelectedAddress, Map<String, String> pCreditCardInfoMap,
			Map<String, String> pAddressValue) throws RepositoryException, IntrospectionException, 
			InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithExistingAddress) : STARTS");
		}
		updateProfileRepositoryAddress(pProfile, pAddressValue);
		final RepositoryItem billingAddress = getSecondaryAddressByNickName(pProfile, pSelectedAddress);
		if (isLoggingDebug()) {
			logDebug("Billing Address " + billingAddress);
		}
		try {
			final String creditCardNickName=createProfileCreditCard(pProfile, pCreditCardInfoMap, null, billingAddress, null);
			setCreditCardNickName(creditCardNickName);
//			String login = (String)pProfile.getPropertyValue(getPropertyManager().getLoginPropertyName());
		/*	if(pProfile.isTransient()){
				getSETALogger().logAddCreditCard(login);
				getSETALogger().logAddBillingAddress(login, (String)billingAddress.getRepositoryId());
			}*/
		} catch (PropertyNotFoundException e) {
			vlogError("PropertyNotFoundException occured while adding credit card with exception : {0}", e);
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (addCardWithExistingAddress) : ENDS");
		}
	}
}