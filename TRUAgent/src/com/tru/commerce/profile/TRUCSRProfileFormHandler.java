package com.tru.commerce.profile;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.apache.soap.encoding.soapenc.Base64;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.csr.order.CSROrderHolder;
import atg.commerce.csr.profile.CSRCustomerProfileFormHandler;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.PropertyManager;
import atg.userprofiling.address.AddressTools;

import com.globalsportsinc.crypt.CryptKeeper;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.csr.util.TRUCSRAgentConfiguration;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.payment.TRUPaymentConstants;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.messaging.TRUEpslonChangeEmailSource;
import com.tru.messaging.TRUEpslonPasswordResetSource;
import com.tru.messaging.oms.TRUCustomerMasterImportSource;
import com.tru.messaging.oms.TRUCustomerOrderUpdateSource;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;
import com.tru.validator.fhl.IValidator;

/**
 * This class extends ATG OOTB CustomerProfileFormHandler and used
 * for managing the account related operation.
 * 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRProfileFormHandler extends CSRCustomerProfileFormHandler{

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS = "TRUCSRProfileFormHandler.handleEditShippingAddress";

	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS = "TRUCSRProfileFormHandler.handleAddShippingAddress";
	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD = "TRUCSRProfileFormHandler.handleAddCreditCard";
	
	/** The Constant TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER. */
	private static final String TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER = "TRUProfileFormHandler.handleRemoveRewardMember";

	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();

	/**
	 * Property to hold the Site Context Manager.
	 */
	private SiteContextManager mSiteContextManager;
	
	
	/**
	 * @return the mSiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}
	
	/**
	 * @param pSiteContextManager the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/** The m page name. */
	private String mPageName;
	
	

	/**
	 * property to hold billing address nick name.
	 */
	private String mBillingAddressNickName;

	/**
	 * property to hold credit card CVV.
	 */
	private String mCreditCardCVV;


	/**
	 * map to hold credit card info.
	 */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();


	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;

	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;

	/**
	 * Property to hold the mPasswordResetSource.
	 */
	private TRUEpslonPasswordResetSource mPasswordResetSource;

	/**
	 * Property to hold the mTRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * Property to hold the OrderTools.
	 */
	private TRUOrderTools mOrderTools;

	/**
	 * mCustomerMasterImportSource.
	 */
	private TRUCustomerMasterImportSource mCustomerMasterImportSource;

	/**
	 * property to hold the mValidator.
	 */
	private IValidator mValidator;

	/**
	 * property to hold ValidationException.
	 */
	private ValidationExceptions mVE = new ValidationExceptions();

	/**
	 * Property to hold the mAgentConfiguration.
	 */
	private TRUCSRAgentConfiguration mAgentConfiguration;

	/**
	 *  property to hold reward number of profile.
	 */
	private String mRewardNumber;

	/**
	 * property to hold TRUPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * Property to holds mTRUProfileManager.
	 */
	private TRUProfileManager mProfileManager;

	/**
	 * property to hold the mErrorHandler.
	 */
	private IErrorHandler mErrorHandler;

	/**
	 * property to hold OrderManager.
	 */
	private TRUOrderManager mOrderManager;

	/**
	 *  property to hold RepeatingRequestMonitor.
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	/**
	 *  property to hold mCartComponent.
	 */
	private CSROrderHolder mCartComponent;

	/**
	 *  Property to hold mResetPwdUrl.
	 */
	private String mResetPwdUrl;

	/**
	 *  Property to hold mMyAccountUrl.
	 */
	private String mMyAccountUrl;

	/**
	 *  Property to hold mIntegrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/**
	 *  Property to hold emailUpdate.
	 */
	private boolean mEmailUpdate;

	/**
	 * Property to hold mUserOldEmailAddress.
	 */
	private String mUserOldEmailAddress;

	/**
	 * Property to hold mCsrProfileTools.
	 */
	private TRUCSRProfileTools mCsrProfileTools;
	
	
	/**
	 * property to hold selected billing address.
	 */
	private String mSelectedCardBillingAddress;
	
	/**
	 * property to hold nickNameCount.
	 */
	private static int numberCount=0;
	
	/**
	 * Gets the page name.
	 *
	 * @return the page name
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the mPageName to set
	 */
	public void setPageName(String pPageName) {
		this.mPageName = pPageName;
	}
	
	
	/**
	 * @return the mSelectedCardBillingAddress
	 */
	public String getSelectedCardBillingAddress() {
		return mSelectedCardBillingAddress;
	}
	
	/**
	 * @param pSelectedCardBillingAddress the mSelectedCardBillingAddress to set
	 */
	public void setSelectedCardBillingAddress(String pSelectedCardBillingAddress) {
		this.mSelectedCardBillingAddress = pSelectedCardBillingAddress;
	}

	/**
	 * Holds address values.
	 */
	private Map<String, String> mAddressValue = new HashMap<String, String>();
	/**
	 * list to hold error keys.
	 */
	private List<String> mErrorKeysList = new ArrayList<String>();

	/**
	 *  Property to hold mDefaultShippingAddress.
	 */
	private boolean mDefaultShippingAddress;
	
	/**
	 *  Property to hold mDefaultShippingAddress.
	 */
	private boolean mDefaultBillingAddress;

	/**
	 * @return the isDefaultShippingAddress
	 */

	public boolean isDefaultBillingAddress() {
		return mDefaultBillingAddress;
	}
	/**
	 * @param pDefaultBillingAddress the mDefaultBillingAddress to set
	 */
	public void setDefaultBillingAddress(boolean pDefaultBillingAddress) {
		mDefaultBillingAddress = pDefaultBillingAddress;
	}

	/**
	 *  Property to hold mDefaultShippingAddress.
	 */
	private boolean mDefaultCreditCard;
	/**
	 * @return the defaultCreditCard
	 */
	public boolean isDefaultCreditCard() {
		return mDefaultCreditCard;
	}

	/**
	 * @param pDefaultCreditCard the defaultCreditCard to set
	 */
	public void setDefaultCreditCard(boolean pDefaultCreditCard) {
		mDefaultCreditCard = pDefaultCreditCard;
	}
	
	/**
	 * Holds orderUpdateSource
	 */
	private TRUCustomerOrderUpdateSource mOrderUpdateSource;
	
	/**
	 * This is to get the value for orderUpdateSource
	 *
	 * @return the orderUpdateSource value
	 */
	public TRUCustomerOrderUpdateSource getOrderUpdateSource() {
		return mOrderUpdateSource;
	}

	/**
	 * This method to set the pOrderUpdateSource with orderUpdateSource
	 * 
	 * @param pOrderUpdateSource the orderUpdateSource to set
	 */
	public void setOrderUpdateSource(TRUCustomerOrderUpdateSource pOrderUpdateSource) {
		mOrderUpdateSource = pOrderUpdateSource;
	}

	/**
	 * @return the isDefaultShippingAddress
	 */

	public boolean isDefaultShippingAddress() {
		return mDefaultShippingAddress;
	}
	/**
	 * @param pDefaultShippingAddress the mDefaultShippingAddress to set
	 */
	public void setDefaultShippingAddress(boolean pDefaultShippingAddress) {
		mDefaultShippingAddress = pDefaultShippingAddress;
	}

	/**
	 * @return the errorKeysList
	 */
	public List<String> getErrorKeysList() {
		return mErrorKeysList;
	}

	/**
	 * @param pErrorKeysList the errorKeysList to set
	 */
	public void setErrorKeysList(List<String> pErrorKeysList) {
		mErrorKeysList = pErrorKeysList;
	}

	/**
	 * @return the mCsrProfileTools
	 */

	public TRUCSRProfileTools getCsrProfileTools() {
		return mCsrProfileTools;
	}
	/**
	 * @param pCsrProfileTools the user old email address
	 */
	public void setCsrProfileTools(TRUCSRProfileTools pCsrProfileTools) {
		mCsrProfileTools = pCsrProfileTools;
	}

	/**
	 * @return the userOldEmailAddress
	 */
	public String getUserOldEmailAddress() {
		return mUserOldEmailAddress;
	}

	/**
	 * @param pUserOldEmailAddress the user old email address
	 */
	public void setUserOldEmailAddress(String pUserOldEmailAddress) {
		mUserOldEmailAddress = pUserOldEmailAddress;
	}

	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}

	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * @return the ship to mResetPwdUrl.
	 */

	public String getResetPwdUrl() {
		return mResetPwdUrl;
	}

	/**
	 * @param pResetPwdUrl
	 *            - the ship to mResetPwdUrl
	 */
	public void setResetPwdUrl(String pResetPwdUrl) {
		mResetPwdUrl = pResetPwdUrl;
	}
	/**
	 * @return the ship to mMyAccountUrl.
	 */

	public String getMyAccountUrl() {
		return mMyAccountUrl;
	}

	/**
	 * @param pMyAccountUrl
	 *            - the ship to mMyAccountUrl.
	 */
	public void setMyAccountUrl(String pMyAccountUrl) {
		mMyAccountUrl = pMyAccountUrl;
	}


	/**
	 * @return TRUProfileManager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}

	/**
	 * @param pProfileManager
	 *            set property mProfileManager, used for business logic validation
	 *
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}

	/**
	 * @return the errorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler
	 *            the errorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager
	 *            the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}

	/**
	 * @param pRewardNumber
	 *            the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		mRewardNumber = pRewardNumber;
	}

	/**
	 * @return the mAgentConfiguration
	 */
	public TRUCSRAgentConfiguration getAgentConfiguration() {
		return mAgentConfiguration;
	}

	/**
	 * @param pAgentConfiguration
	 *            the agentConfiguration to set
	 */
	public void setAgentConfiguration(TRUCSRAgentConfiguration pAgentConfiguration) {
		mAgentConfiguration = pAgentConfiguration;
	}

	/**
	 * @return the mPasswordResetSource
	 */
	public TRUEpslonPasswordResetSource getPasswordResetSource() {
		return mPasswordResetSource;
	}

	/**
	 * @param pPasswordResetSource
	 *            the passwordResetSource to set
	 */
	public void setPasswordResetSource(TRUEpslonPasswordResetSource pPasswordResetSource) {
		mPasswordResetSource = pPasswordResetSource;
	}

	/**
	 * @return IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * @param pValidator
	 *            the mValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		mValidator = pValidator;
	}

	/**
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * @param pCommonErrorURL
	 *            the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * @return the mTRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration
	 *            the TRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * This method returns the customerMasterImportSource value.
	 *
	 * @return the customerMasterImportSource
	 */
	public TRUCustomerMasterImportSource getCustomerMasterImportSource() {
		return mCustomerMasterImportSource;
	}

	/**
	 * @return the mRepeatingRequestMonitor
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * @param pOrderTools
	 *            the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}


	/**
	 * This Method will return the TRUOrderManager.
	 *
	 * @return the mOrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * This method will set OrderManager to pOrderManager.
	 *
	 * @param pOrderManager
	 *            the mOrderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * @return the mCartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent
	 *            the CSROrderHolder to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		mCartComponent = pCartComponent;
	}

	/**
	 *  property to hold changeEmailSource.
	 */
	private TRUEpslonChangeEmailSource mChangeEmailSource;

	/**
	 * @return the changeEmailSource
	 */
	public TRUEpslonChangeEmailSource getChangeEmailSource() {
		return mChangeEmailSource;
	}

	/**
	 * @param pChangeEmailSource the changeEmailSource to set
	 */
	public void setChangeEmailSource(TRUEpslonChangeEmailSource pChangeEmailSource) {
		mChangeEmailSource = pChangeEmailSource;
	}

	/**
	 * @return the mEmailUpdate
	 */
	public boolean isEmailUpdate() {
		return mEmailUpdate;
	}

	/**
	 * @param pEmailUpdate the mEmailUpdate to set
	 */
	public void setEmailUpdate(boolean pEmailUpdate) {
		mEmailUpdate = pEmailUpdate;
	}

	/**
	 * Sets property AddressValue map.
	 * 
	 * @param pAddressValue
	 *            AddressValue
	 **/
	public void setAddressValue(Map<String, String> pAddressValue) {
		mAddressValue = pAddressValue;
	}

	/**
	 * Gets the address value.
	 * 
	 * @return This is a map that stores the value of address properties.
	 */
	public Map<String, String> getAddressValue() {
		return mAddressValue;
	}

	/**
	 * @return the addressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * @param pAddressInputFields the addressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		mAddressInputFields = pAddressInputFields;
	}

	/**
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}


	/**
	 * @return the billingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * @param pBillingAddressNickName the billingAddressNickName to set
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}


	/**
	 * @param pCreditCardInfoMap the creditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}


	/**
	 * Gets the name on card.
	 * 
	 * @return the nameOnCard
	 */
	public String getNameOnCard() {
		return getCreditCardInfoMap().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getNameOnCardPropertyName());
	}
	/**
	 * Gets the credit card number.
	 * 
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return getCreditCardInfoMap().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardNumberPropertyName());
	}

	/**
	 * Gets the expiration month.
	 * 
	 * @return the expirationMonth
	 */
	public String getExpirationMonth() {
		return getCreditCardInfoMap().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardExpirationMonthPropertyName());
	}

	/**
	 * Gets the expiration year.
	 * 
	 * @return the expirationYear
	 */
	public String getExpirationYear() {
		return getCreditCardInfoMap().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getCreditCardExpirationYearPropertyName());
	}

	/**
	 * Gets the address first name.
	 * 
	 * @return the Address - FirstName
	 */
	public String getAddressFirstName() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getFirstNamePropertyName());
	}

	/**
	 * Gets the address last name.
	 * 
	 * @return the Address - LastName
	 */
	public String getAddressLastName() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getLastNamePropertyName());
	}

	/**
	 * Gets the address address one.
	 * 
	 * @return the address1
	 */
	public String getAddressAddressOne() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressLineOnePropertyName());
	}

	/**
	 * Gets the address city.
	 * 
	 * @return the city
	 */
	public String getAddressCity() {
		return getAddressValue()
				.get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager().getAddressCityPropertyName());
	}

	/**
	 * Gets the address state.
	 * 
	 * @return the state
	 */
	public String getAddressState() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressStatePropertyName());
	}

	/**
	 * Gets the address postal code.
	 * 
	 * @return the PostalCodeCode
	 */
	public String getAddressPostalCode() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressPostalCodePropertyName());
	}

	/**
	 * Gets the address country.
	 * 
	 * @return the country
	 */
	public String getAddressCountry() {
		return getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressCountryPropertyName());
	}


	/**
	 * Gets the address phone number.
	 * 
	 * @return the PhoneNumber
	 */
	public String getAddressPhoneNumber() {
		String addressPhoneNumber = getAddressValue().get(((TRUProfileTools) getProfileTools()).getTRUPropertyManager()
				.getAddressPhoneNumberPropertyName());
		return addressPhoneNumber;
	}

	/**
	 * @return the creditCardCVV
	 */
	public String getCreditCardCVV() {
		return mCreditCardCVV;
	}

	/**
	 * @param pCreditCardCVV the creditCardCVV to set
	 */
	public void setCreditCardCVV(String pCreditCardCVV) {
		mCreditCardCVV = pCreditCardCVV;
	}

	/**
	 * property to hold mPurchaseProcessHelper.
	 */
	private TRUCSRPurchaseProcessHelper mPurchaseProcessHelper;
	
	/**
	 * @return the purchaseProcessHelper
	 */
	public TRUCSRPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUCSRPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}
	
	/**
	 * This method is override for resetting the password - in case of forgot
	 * password.
	 *
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @return true if successful
	 * @throws ServletException
	 *             if an error occurred while processing the servlet request
	 * @throws IOException
	 *             if an error occurred while reading or writing the servlet
	 * 
	 */
	@Override
	public boolean handleResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleResetPassword) : Starts");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();

		String myHandleMethod = TRUCSCConstants.HANDLERESET;
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = getTransactionDemarcation();

		if (rrm == null ||
				rrm.isUniqueRequestEntry(myHandleMethod)) {
			try
			{
				if (tm != null) {
					td.begin(tm, TRUCSCConstants.NUMBER_THREE);
				}
				RepositoryItem ticket = getEnvironmentTools().getActiveTicket();
				if (ticket == null)
				{
					addFormException(new DropletException(TRUCSCConstants.CURRENT_TICKET_HOLDER_IS_NULL));
				}
				getCsrProfileTools().updateGeneratedPwdProperty(getProfile());
				String email = getCsrProfileTools().fetchEmail(getProfile());
				String resetPwdURLWithoutParam = generateURL(pRequest, getResetPwdUrl());
				String lEncryptedEmail = Base64.encode(email.getBytes());
				String serverTimeStamp = ((TRUProfileTools) getProfileTools()).getServerTimeStamp();
				String encodedTimeStamp = Base64.encode(serverTimeStamp.getBytes());
				String lResetPasswordURL = resetPwdURLWithoutParam +
						TRUConstants.QUESTION_MARK_STRING +
						TRUConstants.EMAIL_PARAM_NAME +
						TRUConstants.EQUALS_SYMBOL_STRING + lEncryptedEmail
						+ TRUConstants.AMPERSAND_STRING + TRUConstants.TIMESTAMP_PARAM_NAME + TRUConstants.EQUALS_SYMBOL_STRING + encodedTimeStamp;


				String lMyAccountURL = generateURL(pRequest, getMyAccountUrl());

				if (getTRUConfiguration() != null && getTRUConfiguration().isEnableEpslon()) {
					if (!StringUtils.isBlank(email) && getPasswordResetSource() != null) {
						try {
							getPasswordResetSource().sendPasswordResetEmail(email, lMyAccountURL, lResetPasswordURL );
						} catch (JMSException pJMSException) {
							if (isLoggingError()) {
								logError("JMS EXCEPTION", pJMSException);
							}
						}
					}else{
						if (isLoggingDebug()) {
							logDebug("PasswordResetSource is NULL or Email Address is Empty");
						}
					}
				}else{
					if (isLoggingDebug()) {
						logDebug("TRUConfiguration is NULL or enableEpslon is disabled");
					}
				}

			}
			catch (TransactionDemarcationException pTransactionDemarcationException)
			{
				if (isLoggingError()) {
					logError(" TransactionDemarcationException :{0}", pTransactionDemarcationException);
				}
				addFormException(new DropletException(TRUCSCConstants.UNABLE_TO_RESET_PASSWORD, pTransactionDemarcationException));
			}

			finally
			{
				try
				{
					if (tm != null) {
						td.end();
					}
				}
				catch (TransactionDemarcationException pTransactionDemarcationException)
				{
					if (isLoggingError()) {
						logError(" TransactionDemarcationException :{0}", pTransactionDemarcationException);
					}
					addFormException(new DropletException(TRUCSCConstants.UNABLE_TO_RESET_PASSWORD, pTransactionDemarcationException));

				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleResetPassword) : Ends");
		}
		return checkFormRedirect(getSuccessUrl(), getErrorUrl(), pRequest, pResponse);
	}

	/**
	 * This method is used to generate the url.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pRedirectPage
	 *            redirection page url
	 * @return String
	 *            returns to final url for redirection on click
	 */
	public String generateURL(DynamoHttpServletRequest pRequest, String pRedirectPage) {
		if (isLoggingDebug()){
			logDebug("TRUCSRProfileFormHandler (generateURL) : Starts");
		}

    	String lServerName = getAgentConfiguration().getServerName();
    	String lURL =null;
    	if(!StringUtils.isBlank(lServerName)){
    	  lURL= lServerName + pRedirectPage;
    	}
    	if (isLoggingDebug()) {
		logDebug("TRUCSRProfileFormHandler (generateURL) : Ends");
	}
		return lURL;
	}

	/**
	 * This method is for performing validations before adding reward number pertaining to user.
	 *
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddRewardMember(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preAddRewardMember()");
		}

		if (StringUtils.isBlank(getRewardNumber())) {
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REWARD_NUMBER_MISSING_INFO, null);
			getErrorHandler().processException(mVE, this);
		}
		else if (StringUtils.isNotBlank(getRewardNumber())
				&& !getProfileManager().validateDenaliAccountNum(getRewardNumber())) {
			mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_REWARD_NUMBER_INVALID, null);
			getErrorHandler().processException(mVE, this);
			return;
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preAddRewardMember()");
		}
	}

	/**
	 * This method is for the required tasks to be done after adding reward number to user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postAddRewardMember(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postAddRewardMember()");
		}
		TRUOrderImpl currentOrder = null ;
		currentOrder = (TRUOrderImpl) getCartComponent().getCurrent();
		TRUOrderManager orderManager = getOrderManager();
		if (currentOrder != null) {
			synchronized (currentOrder) {
				try {
						TRUProfileTools ptools = (TRUProfileTools) getProfileTools();
						final TRUPropertyManager propertyManager = (TRUPropertyManager) ptools.getPropertyManager();
						ptools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
						currentOrder.setRewardNumber((String)getProfile().getPropertyValue(getPropertyManager().getRewardNumberPropertyName()));
						orderManager.updateOrder(currentOrder);
				}catch (RepositoryException repoExc) {
					vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
				}
				catch (CommerceException comEx) {
					if (isLoggingError()) {
						logError("CommerceException occured while adding reward number with exception {0}", comEx);
					}
				}
			}
		}
		if (getCustomerMasterImportSource() != null) {
			getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), null);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.postAddRewardMember()");
		}
	}

	/**
	 * This method is for adding the reward number for the user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @return success/fail
	 */
	public boolean handleAddRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.handleAddRewardMember()");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUProfileFormHandler.handleAddRewardMember";
		boolean lRollbackFlag = false;
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = getTransactionDemarcation();
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preAddRewardMember(pRequest,pResponse);
					if (!getFormError()) {
						getProfileTools().updateProperty(getPropertyManager().getRewardNumberPropertyName(), getRewardNumber(), getProfileItem());
						getProfileTools().updateProperty(getPropertyManager().getloyaltyCustomer(), Boolean.TRUE, getProfileItem());
						postAddRewardMember(pRequest,pResponse);
					}

				}
			} catch (RepositoryException repoEx) {
				lRollbackFlag = true;
				if (isLoggingError()) {
					logError("RepositoryException", repoEx);
				}
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				if (isLoggingError()) {
					logError("TransactionDemarcationException occured while adding rewardnumber with exception {0}", trDem);
				}
			}finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						if (isLoggingError()) {
							logError("TransactionDemarcationException", tD);
						}
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("End: TRUCSRProfileFormHandler.handleAddRewardMember()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is invoked when user confirms the request to delete the rewards number from profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleRemoveRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.handleRemoveRewardMember()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean lRollbackFlag = false;
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER))) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					getProfileTools().updateProperty(getPropertyManager().getRewardNumberPropertyName(), TRUConstants.EMPTY,
							getProfileItem());
					getProfileTools().updateProperty(getPropertyManager().getloyaltyCustomer(), Boolean.FALSE, getProfileItem());
					postRemoveRewardMember(pRequest, pResponse);
				}
			} catch (RepositoryException rExc) {
				vlogError("Repository Exception occured while removing the rewatd card number from profile with exception {0}",
						rExc);
			} catch (TransactionDemarcationException trDem) {
				lRollbackFlag = true;
				vlogError("TransactionDemarcationException occured while removing reward number with exception {0}", trDem);
			} finally {
				if (tm != null) {
					try {
						td.end(lRollbackFlag);
					} catch (TransactionDemarcationException tD) {
						vlogError("TransactionDemarcationException", tD);
					}
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_REMOVE_REWARD_MEMBER);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.handleRemoveRewardMember()");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	
	
	/**
	 * post method for handleRemoveRewardMember.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postRemoveRewardMember(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postRemoveRewardMember()");
		}
		/**
		 * remove the rewards number from order also if the user removes it from profile
		 */
		TRUOrderImpl currentOrder = null;
		if (null != ((TRUOrderImpl) getCartComponent().getCurrent())) {
			currentOrder = ((TRUOrderImpl) getCartComponent().getCurrent());
		}
		if (currentOrder != null) {
			synchronized (currentOrder) {
				currentOrder.setRewardNumber((String) getProfile().getPropertyValue(
						getPropertyManager().getRewardNumberPropertyName()));
				try {
					getOrderManager().updateOrder(currentOrder);
				} catch (CommerceException comEx) {
					vlogError("CommerceException occured while removing reward number with exception {0}", comEx);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.postRemoveRewardMember()");
		}
	}
	
	/**
	 * This method is for getting the current email from profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws IOException - IOException
	 * @throws ServletException - ServletException
	 */
	@Override
	protected void preUpdateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preUpdateUser()");
		}
		super.preUpdateUser(pRequest, pResponse);
		// String email =  (String)((TRUCSRProfileTools)getProfileTools()).fetchEmail(getProfile());
		  PropertyManager propmanager = getProfileTools().getPropertyManager();
		String email = getCsrProfileTools().fetchEmail(getProfile());
		
		String anonymousUser =	(String)getValue().get(propmanager.getEmailAddressPropertyName());
		
		if(!StringUtils.isBlank(email)){
		setUserOldEmailAddress(email.toLowerCase());
		}
		try {
			
			if(email!=null && !email.isEmpty()){
			if(!StringUtils.isBlank(anonymousUser)&& !email.equalsIgnoreCase(anonymousUser)){

				if (userAlreadyExists(anonymousUser.toLowerCase(), getProfileTools().getProfileRepository(), pRequest, pResponse)) {

					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_ALREADY_EXISTS, null);
					getErrorHandler().processException(mVE, this);

				}
			} 
			}
			else{
			if(!StringUtils.isBlank(anonymousUser)){

				if (userAlreadyExists(anonymousUser.toLowerCase(), getProfileTools().getProfileRepository(), pRequest, pResponse)) {

					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_EMAIL_ALREADY_EXISTS, null);
					getErrorHandler().processException(mVE, this);

				}
			}
			}
		}

		catch (RepositoryException exception) {

			if (isLoggingError()) {
				logError("Repository Exception", exception);
			}
		}

	
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preUpdateUser()");
		}
	}

	/**
	 * Post create user.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * 
	 */
	@Override
	protected void postCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException
		  {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postCreateUser()");
		}
			super.postCreateUser(pRequest, pResponse);
			
			try {
				TRUProfileTools ptools = (TRUProfileTools) getProfileTools();
				final TRUPropertyManager propertyManager = (TRUPropertyManager) ptools.getPropertyManager();
				ptools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
				//final String password = getStringValueProperty(propertyManager.getPasswordPropertyName()).trim();
				//ptools.updateProperty(propertyManager.getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password), getProfile());
			} catch (RepositoryException repoExc) {
				vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
			}
			
			if (isLoggingDebug()) {
				logDebug("End: TRUCSRProfileFormHandler.postCreateUser()");
			}
		  }
	
	
	/**
	 * This method is for sending email change confirmation email to user.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws IOException - IOException
	 * @throws ServletException - ServletException
	 * 
	 */
	@Override
	protected void postUpdateUser(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postUpdateUser()");
		}
		TRUOrderImpl lastOrder = null ;
		lastOrder = (TRUOrderImpl) getCartComponent().getLast();
		super.postUpdateUser(pRequest, pResponse);

		boolean isEpslonEnabled = Boolean.FALSE;
		
		
		TRUProfileTools ptools = (TRUProfileTools) getProfileTools();
		final TRUPropertyManager propertyManager = (TRUPropertyManager) ptools.getPropertyManager();
		isEpslonEnabled = getIntegrationStatusUtil().getEpslonIntStatus(pRequest);
		if (!getFormError() && isEmailUpdate() && getTRUConfiguration() != null && isEpslonEnabled ) {
			String emailAddressPropertyName = propertyManager.getEmailAddressPropertyName();
			String emailAddress = getStringValueProperty(emailAddressPropertyName);
			String lMyAccountURL = generateMyAccountURL(pRequest, getMyAccountUrl());
			if (!StringUtils.isBlank(emailAddress)) {
				if (getChangeEmailSource() == null) {
					vlogDebug("ChangeEmailSource Component is :",getChangeEmailSource());
				}else{
					try {

						String oldEmail= getUserOldEmailAddress();
						String updatedEmail= emailAddress;
						getChangeEmailSource().sendEmailOnEmailChange(oldEmail,updatedEmail,lMyAccountURL);
					} catch (JMSException jmsExp) {
						if (isLoggingError()) {
							logError("JMS EXCEPTION", jmsExp);
						}
					}
				}
			}
		}

		String emailId = getStringValueProperty( propertyManager.getEmailAddressPropertyName());
		if (getTRUConfiguration() != null && isEpslonEnabled && !isEmailUpdate()) {
			getCsrProfileTools().sendEpslonEmailToUser(emailId);
		}
		
		if (getCustomerMasterImportSource() != null) {
			getCustomerMasterImportSource().sendCustomerMasterImport(getProfile(), null);
		}
		
		try {
			final String password = getStringValueProperty(propertyManager.getPasswordPropertyName());
			if(StringUtils.isNotBlank(password)){
				ptools.updateProperty(propertyManager.getRadialPasswordPropertyName(),CryptKeeper.encryptAndEncode(password.trim()), getProfile());
				ptools.updateProperty(((TRUPropertyManager)ptools.getPropertyManager()).getLastProfileUpdatePropertyName(), new Date(), getProfile());
			}
			ptools.updateProperty(propertyManager.getLastProfileUpdatePropertyName(), new Date(), getProfile());
			if(getPageName()!= null && getPageName().equals(TRUCSCConstants.ORDER_CONFIRM) && getOrderUpdateSource() != null && lastOrder != null){
				getOrderUpdateSource().sendCustomerOrderUpdate(getProfile(), lastOrder);
			}
		} catch (RepositoryException repoExc) {
			vlogError("RepositoryException occured while setting the last updated dated to profile with exception : {0}", repoExc);
		}
		

/*		String comments = (String)getEditValue().get(TRUCSCConstants.COMMENTS);
		if (!StringUtils.isBlank(comments)) {

			try {
				getAgentTools().createProfileComment(getRepositoryId(), getAgentProfile().getDataSource(), comments);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError("RepositoryException", e);
				}
			}

		}*/
		
		
		

		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.postUpdateUser()");
		}
	}

	/**
	 * This method is used to generate the url.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pRedirectPage
	 *            redirection page url
	 * @return String - returns to final url for redirection on click
	 */
	public String generateMyAccountURL(DynamoHttpServletRequest pRequest, String pRedirectPage) {
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (generateURL) : Starts");
		}
		final String lHost = pRequest.getHeader(TRUConstants.HOST);
		final String lContextPath = pRequest.getContextPath();
		final String lURL = lHost + lContextPath + pRedirectPage;
		if (isLoggingDebug()) {
			logDebug("TRUProfileTools (generateURL) : Ends");
		}
		return lURL;
	}

	/**

	 * This method is for adding the shipping address to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleAddShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleAddShippingAddress): START");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS)) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					preAddShippingAddress(pRequest, pResponse);
					if (!getFormError()) {
						final Address addressObject = AddressTools.createAddressFromMap(getAddressValue(),TRUConstants.ADDRESS_CLASS_NAME);
						String nickName = getAddressValue().get(TRUConstants.ADDRESS_NICK_NAME);
						String firstName = getAddressValue().get(TRUCSCConstants.FIRST_NAME);
						String lstName =	getAddressValue().get(TRUCSCConstants.LAST_NAME);
						if(StringUtils.isBlank(nickName)){
							if(numberCount>=TRUCSCConstants.NUMBER_ONE){
								nickName =   firstName+lstName+numberCount;
								}
								else{
									nickName =   firstName+lstName;
								}
							numberCount = numberCount+TRUCSCConstants.NUMBER_ONE;
						}
						
						final String newAddressId = ((TRUProfileTools) getProfileTools()).createProfileRepositorySecondaryAddress(getProfile(),
								nickName, addressObject);
						vlogDebug("New Addess Id in TRUProfileFormHandler.handleAddShippingAddress: " + newAddressId);
						//((TRUProfileTools) getProfileTools()).setProfileDefaultAddress(getProfileItem());
						if(isDefaultShippingAddress()|| isDefaultBillingAddress()){
							setAddressAsDefault(nickName);
					}
					}
				}
				
			} catch (TransactionDemarcationException e) {
				lRollbackFlag = true;
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				throw new ServletException(e);
			} catch (InstantiationException iEx) {
				lRollbackFlag = true;
				vlogError(
						"InstantiationException occured in TRUProfileFormHandler.handleEditShippingAddress  with exception : {0}",
						iEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			} catch (IllegalAccessException illEx) {
				lRollbackFlag = true;
				vlogError(
						"IllegalAccessException occured in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						illEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			} catch (ClassNotFoundException cEx) {
				lRollbackFlag = true;
				vlogError(
						"ClassNotFoundException occured while in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						cEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			} catch (RepositoryException repoEx) {
				lRollbackFlag = true;
				vlogError(
						"RepositoryException occured while in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						repoEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			} catch (IntrospectionException introEx) {
				lRollbackFlag = true;
				vlogError(
						"IntrospectionException occured in TRUProfileFormHandler.handleEditShippingAddress with exception : {0}",
						introEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				try {
					if (tm != null) {
						td.end(lRollbackFlag);
					}
				} catch (TransactionDemarcationException e) {
					vlogError("TransactionDemarcationException in TRUProfileFormHandler.handleEditShippingAddress", e);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_SHIPPING_ADDRESS);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleAddShippingAddress): END");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}

	/**
	 * This method is for doing the basic validations before adding the shipping address to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preAddShippingAddress()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		try {
			getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_ADDRESS_FORM, this);
			if (!getAddressValue().isEmpty()) {
				profileTools.trimMapValues(getAddressValue());
				profileTools.changeCountryValue(getAddressValue());
			}
			String nickName = (getAddressValue()).get(profileTools.getTRUPropertyManager().getNicknamePropertyName());
			final String country = getAddressValue().get(profileTools.getTRUPropertyManager().getAddressCountryPropertyName());
	                 String firstName = getAddressValue().get(profileTools.getTRUPropertyManager().getFirstNamePropertyName());
			String lstName =	getAddressValue().get(profileTools.getTRUPropertyManager().getLastNamePropertyName());
			
			
				if(StringUtils.isBlank(nickName)){
					//numberCount =0;
				if(numberCount>=TRUCSCConstants.NUMBER_ONE){
					nickName =   firstName+lstName+numberCount;
					}
					else{
						nickName =   firstName+lstName;
					}
				//numberCount=numberCount+1;
			}
			if (getProfileManager().validateDuplicateAddressNickname(getProfile(), nickName)) {
				if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)){
					addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS));
				}else{
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME, null);
					getErrorHandler().processException(mVE, this);
				}
			}
			
			if(!TRUCSCConstants.COUNTRY.equalsIgnoreCase(country)){
				getAddressValue().put(profileTools.getTRUPropertyManager().getAddressStatePropertyName(), TRUCSCConstants.STATE_NA);
				if(isDefaultShippingAddress()){
					addFormException(new DropletException(TRUCSCConstants.CANT_SHIPTO_INTERNATIONAL_ADDRESS));
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preAddShippingAddress()");
		}
	}


	/**
	 * This method is used for doing back-end validation on new credit card before adding.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preAddCreditCard()");
		}
		try {
			String ccType = null;
			String billingAddressNickName=null;
			if(StringUtils.isBlank(getBillingAddressNickName())){
				if(numberCount==0){
				billingAddressNickName=getAddressFirstName()+getAddressLastName();
				}else{
					billingAddressNickName=getAddressFirstName()+getAddressLastName()+numberCount;
				}
				setBillingAddressNickName(billingAddressNickName);
				numberCount++;
			}
			final String creditCardNumber = getCreditCardInfoMap().get(getPropertyManager().getCreditCardNumberPropertyName());
			final String cardLength = getCreditCardInfoMap().get(TRUConstants.CARD_LENGTH);
			TRUPropertyManager propertyManager = getPropertyManager();
			if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()).equals(
					getTRUConfiguration().getCardNumberPLCC())) {
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationMonthPropertyName(),
						getTRUConfiguration().getExpMonthAdjustmentForPLCC());
				getCreditCardInfoMap().put(propertyManager.getCreditCardExpirationYearPropertyName(),
						getTRUConfiguration().getExpYearAdjustmentForPLCC());
			}

			/*	if (getAddressValue().get(TRUConstants.BILLING_ADDRESS_VISIBLE).equalsIgnoreCase(TRUConstants.TRUE)) {*/
			getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITH_ADDR_FORM, this);
			if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getCreditCardNumber())
					|| StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
					|| StringUtils.isBlank(getBillingAddressNickName()) || StringUtils.isBlank(getAddressFirstName())
					|| StringUtils.isBlank(getAddressLastName()) || StringUtils.isBlank(getAddressAddressOne())
					|| StringUtils.isBlank(getAddressCity()) || StringUtils.isBlank(getAddressState())
					|| StringUtils.isBlank(getAddressPostalCode()) || StringUtils.isBlank(getAddressCountry())
					|| StringUtils.isBlank(getAddressPhoneNumber()) || StringUtils.isBlank(getCreditCardInfoMap().get(getPropertyManager().getCreditCardVerificationNumberPropertyName()))) {
				mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
				getErrorHandler().processException(mVE, this);
			} else {
				ccType = ((TRUProfileTools) getProfileTools()).getCreditCardTypeFromRepository(creditCardNumber, cardLength);
				final String nickName = getBillingAddressNickName();
				final boolean isCardExpired = getProfileManager().validateCardExpiration(
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_MONTH),
						getCreditCardInfoMap().get(TRUConstants.EXPIRATION_YEAR));
				final boolean isDuplicateNickName = getProfileManager().validateDuplicateAddressNickname(getProfile(),
						nickName);
				if (StringUtils.isBlank(ccType)) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_INVALID, null);
					getErrorHandler().processException(mVE, this);
				} else if (isCardExpired) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
					getErrorHandler().processException(mVE, this);
				} else if (StringUtils.isBlank(getSelectedCardBillingAddress()) && isDuplicateNickName) {
					if(getErrorKeysList().contains(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS)){
						addFormException(new DropletException(TRUErrorKeys.TRU_ERROR_ACCOUNT_NICKNAME_EXISTS));
					}else{
						mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_DUPLICATE_ADDR_NICKNAME, null);
						getErrorHandler().processException(mVE, this);
					}
				}
				if (StringUtils.isNotBlank(ccType)) {
					getCreditCardInfoMap().put(getPropertyManager().getCreditCardTypePropertyName(), ccType);
				}
			}
			/*}*//* else {
				getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
				if (StringUtils.isBlank(getNameOnCard()) || StringUtils.isBlank(getCreditCardNumber())
						|| StringUtils.isBlank(getExpirationMonth()) || StringUtils.isBlank(getExpirationYear())
						|| StringUtils.isBlank(getCreditCardCVV())) {
					mVE.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
					getErrorHandler().processException(mVE, this);
				}
			}*/
			// Change Country from UNITED STATES to US.
			if (!getAddressValue().isEmpty()) {
				((TRUProfileTools) getProfileTools()).trimMapValues(getAddressValue());
				((TRUProfileTools) getProfileTools()).trimMapValues(getCreditCardInfoMap());
				((TRUProfileTools) getProfileTools()).changeCountryValue(getAddressValue());
			}

			/*		// Calling the Zero authorization for card verification.
			if (!getFormError() && getOrderTools().isPayeezyEnabled()) {
				ContactInfo contactInfo = new ContactInfo();
				contactInfo = getProfileManager().populateContactInfo(contactInfo, getAddressValue());
				contactInfo.setEmail((String) getProfile().getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				TRUCreditCardStatus creditCardStatus = ((TRUPurchaseProcessHelper) getShippingHelper().getPurchaseProcessHelper()).verifyZeroAuthorization(
						TRUPaymentConstants.DOUBLE_INITIAL_VALUE, getSiteContextManager().getCurrentSite().getId(),
						getCreditCardInfoMap(), contactInfo, getProfile().getRepositoryId());
				// For any error adding it to the form errors.
				if (creditCardStatus != null && !creditCardStatus.getTransactionSuccess()) {
					mVE.addValidationError(creditCardStatus.getErrorMessage(), null);
					getErrorHandler().processException(mVE, this);
				}
			}*/
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preAddCreditCard()");
		}
	}

	/**
	 * This method is for adding new credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.handleAddCreditCard()");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD)) {
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
			}
			preAddCreditCard(pRequest, pResponse);
			callZeroAuthForCardVerification(true);
			if (!getFormError() && tm != null) {
				try {
					td.begin(tm, TransactionDemarcation.REQUIRED);
			/*		if (getCreditCardInfoMap() != null
							&& !StringUtils.isBlank(getCreditCardInfoMap().get(TRUConstants.CREDIT_CARD_TOKEN))) {
						getCreditCardInfoMap().put(TRUConstants.CREDIT_CARD_NUMBER,
								getCreditCardInfoMap().get(TRUConstants.CREDIT_CARD_TOKEN));
					}*/
						if (StringUtils.isBlank(getSelectedCardBillingAddress())) {
					((TRUProfileTools) getProfileTools()).addCardWithNewAddress(getProfile(), getAddressValue(),
							getCreditCardInfoMap(), getBillingAddressNickName(), null);
					}else {
						vlogDebug("Adding  as billing address for credit card :{0}", getSelectedCardBillingAddress());
							((TRUProfileTools) getProfileTools()).addCardWithExistingAddress(getProfile(),
									getSelectedCardBillingAddress(), getCreditCardInfoMap(), getAddressValue(), null);
						
					}
					((TRUProfileTools) getProfileTools()).setProfileDefaultAddress(getProfileItem());
					postAddCreditCard(pRequest, pResponse);
				} catch (RepositoryException repExc) {
					lRollbackFlag = true;
					vlogError("RepositoryException occured while adding credit card with exception : {0}", repExc);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (IntrospectionException introExc) {
					lRollbackFlag = true;
					vlogError("IntrospectionException occured while adding credit card with exception : {0}", introExc);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (PropertyNotFoundException e) {
					lRollbackFlag = true;
					vlogError("PropertyNotFoundException occured while adding credit card with exception : {0}", e);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (TransactionDemarcationException tdExe) {
					lRollbackFlag = true;
					vlogError("TransactionDemarcationException occured while adding credit card with exception : {0}", tdExe);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (InstantiationException iEx) {
					lRollbackFlag = true;
					vlogError(
							"InstantiationException occured in TRUCSRProfileFormHandler.handleAddCreditCard  with exception : {0}",
							iEx);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (IllegalAccessException illEx) {
					lRollbackFlag = true;
					vlogError("IllegalAccessException occured in TRUCSRProfileFormHandler.handleAddCreditCard with exception : {0}",
							illEx);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} catch (ClassNotFoundException cEx) {
					lRollbackFlag = true;
					vlogError(
							"ClassNotFoundException occured while in TRUCSRProfileFormHandler.handleAddCreditCard with exception : {0}",
							cEx);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
				} finally {
					if (tm != null) {
						try {
							td.end(lRollbackFlag);
						} catch (TransactionDemarcationException tD) {
							vlogError("TransactionDemarcationException occured while adding credit card with exception ", tD);
							PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
						}
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
					}
					if (PerformanceMonitor.isEnabled()) {
						PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_ADD_CREDIT_CARD);
					}
				}
				if (isLoggingDebug()) {
					logDebug("End: TRUCSRProfileFormHandler.handleAddCreditCard()");
				}
			}
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}



	/**
	 * This method is for the actions to be performed after adding credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 */
	protected void postAddCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postAddCreditCard()");
		}
		try {
			if(isDefaultCreditCard()){
				String creditCardNickName=((TRUCSRProfileTools)getProfileTools()).getCreditCardNickName();
			((CommerceProfileTools) getProfileTools()).setDefaultCreditCard(getProfileItem(), creditCardNickName);
			getProfileTools().updateProperty(TRUConstants.SELECTED_CC_NICKNAME, creditCardNickName, getProfile());
			}
		} catch (RepositoryException e) {
			vlogError("Repository exception occured in TRUCSRProfileFormHandler.postAddCreditCard", e);
		}

		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.postAddCreditCard()");
		}
	}
	
	/**
	 * This method is used Call the Zero authorization for card verification.
	 * 
	 * @param pAddressValidated
	 *            boolean value
	 * @throws ServletException
	 *             ServletException
	 */
	private void callZeroAuthForCardVerification(boolean pAddressValidated) throws ServletException {
		
		// Calling the Zero authorization for card verification.
		if (!getFormError() && pAddressValidated && ((TRUProfileTools)getProfileTools()).isEnableValidateCard()) {
			ContactInfo contactInfo = new ContactInfo();
			contactInfo = getProfileManager().populateContactInfo(contactInfo, getAddressValue());
			contactInfo.setEmail((String) getProfile().getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
			if (StringUtils.isBlank(getCreditCardInfoMap().get(getPropertyManager().getCreditCardTokenPropertyName()))) {
				getCreditCardInfoMap().put(getPropertyManager().getCreditCardTokenPropertyName(),
						getCreditCardInfoMap().get(getPropertyManager().getCreditCardNumberPropertyName()));
			}
			TRUCreditCardStatus creditCardStatus = ((TRUCSRPurchaseProcessHelper)getPurchaseProcessHelper()).
					verifyZeroAuthorization(TRUPaymentConstants.DOUBLE_INITIAL_VALUE, getSiteContextManager().getCurrentSite().getId(), 
							getCreditCardInfoMap(), contactInfo, getProfile().getRepositoryId());
			// For any error adding it to the form errors.
			if (creditCardStatus != null && !creditCardStatus.getTransactionSuccess()) {
				mVE.addValidationError(creditCardStatus.getErrorMessage(), null);
				getErrorHandler().processException(mVE, this);
			}
		}
		
	}

	/**
	 * This method is for editing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean success/fail
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleEditShippingAddress): START");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final TransactionManager tm = getTransactionManager();
		final TransactionDemarcation td = getTransactionDemarcation();
		boolean lRollbackFlag = false;
		if (rrm == null || rrm.isUniqueRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS)) {
			preEditShippingAddress(pRequest, pResponse);
			if (!getFormError() && tm != null) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
					td.begin(tm, TransactionDemarcation.REQUIRED);
						((TRUProfileTools) getProfileTools()).updateProfileRepositoryAddress(getProfile(),
								getAddressValue());
				if(isDefaultShippingAddress()|| isDefaultBillingAddress()){
					setAddressAsDefault(getAddressValue()
							.get(TRUConstants.ADDRESS_NICK_NAME));
				}
			}
			
		
			catch (TransactionDemarcationException e) {
				lRollbackFlag = true;
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				throw new ServletException(e);
			} catch (InstantiationException iEx) {
				lRollbackFlag = true;
				vlogError(
						"InstantiationException occured  in TRUCSRProfileFormHandler.handleEditShippingAddress with exception : {0}",
						iEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			} catch (IllegalAccessException illEx) {
				lRollbackFlag = true;
				vlogError(
						"IllegalAccessException occured in TRUCSRProfileFormHandler.handleEditShippingAddress with exception : {0}",
						illEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			} catch (ClassNotFoundException clsEx) {
				lRollbackFlag = true;
				vlogError(
						"ClassNotFoundException occured in TRUCSRProfileFormHandler.handleEditShippingAddress with exception : {0}",
						clsEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			} catch (RepositoryException repoEx) {
				lRollbackFlag = true;
				vlogError("RepositoryException occured in TRUCSRProfileFormHandler.handleEditShippingAddress with exception : {0}",
						repoEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			} catch (IntrospectionException introEx) {
				lRollbackFlag = true;
				vlogError(
						"IntrospectionException occured in TRUCSRProfileFormHandler.handleEditShippingAddress with exception : {0}",
						introEx);
				PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
			} finally {
				if (isLoggingDebug()) {
					logDebug("<-- Remove Request Entry of RepeatingRequestMonitor from TRUProfileFormHandler.handleEditShippingAddress -->");
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				try {
					if (tm != null) {
						td.end(lRollbackFlag);
					}
				} catch (TransactionDemarcationException e) {
					vlogError("TransactionDemarcationException in TRUCSRProfileFormHandler.handleEditShippingAddress", e);
					PerformanceMonitor.cancelOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRU_PROFILE_FORM_HANDLER_HANDLE_EDIT_SHIPPING_ADDRESS);
				}
			}
		}
		}
		if (isLoggingDebug()) {
			logDebug("TRUCSRProfileFormHandler (handleEditShippingAddress): END");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
	}
	/**
	 * This method is for the required tasks to be done before editing the shipping address pertaining to the user account.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preEditShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preEditShippingAddress()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
		try {
			getValidator().validate(TRUConstants.MY_ACCOUNT_EDIT_ADDRESS_FORM, this);
			if (!getAddressValue().isEmpty()) {
				((TRUProfileTools) getProfileTools()).trimMapValues(getAddressValue());
				((TRUProfileTools) getProfileTools()).changeCountryValue(getAddressValue());
			}
			final String country = getAddressValue().get(profileTools.getTRUPropertyManager().getAddressCountryPropertyName());
			if(!TRUCSCConstants.COUNTRY.equalsIgnoreCase(country)){
				getAddressValue().put(profileTools.getTRUPropertyManager().getAddressStatePropertyName(), TRUCSCConstants.STATE_NA);
				if(isDefaultShippingAddress()){
					addFormException(new DropletException(TRUCSCConstants.CANT_SHIPTO_INTERNATIONAL_ADDRESS));
				}
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			vlogError("Validation exception occured: {0}", ve);
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			vlogError("System exception occured: {0}", se);
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preEditShippingAddress()");
		}
	}




	/**
	 * This method is invoked when user clicks on set as default for a particular address. The address selected is set as default
	 * in the profile and previous default address if any is no more a default address.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public void setAddressAsDefault(String nickName)
			throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.handleSetAddrAsDefault()");
		}
		final TRUProfileTools profileTools = (TRUProfileTools) getProfileTools();
					//final String nickName = getAddressValue().get(profileTools.getTRUPropertyManager().getNicknamePropertyName());

					final String lAddressNickName = nickName;
					vlogDebug("value of nick name selected for setting as default {0} ", lAddressNickName);
					final RepositoryItem newDefaultShippingAddress = profileTools.getProfileAddress(getProfile(), lAddressNickName);
					if(isDefaultShippingAddress()){
					
						getProfileTools().updateProperty(getPropertyManager().getShippingAddressPropertyName(),
								newDefaultShippingAddress, getProfile());
						/*final RepositoryItem prevShippingAddress = (RepositoryItem) getProfile().getPropertyValue(
								getPropertyManager().getPreviousShippingAddressPropertyName());
						if (null == prevShippingAddress) {
							// First time Update current address to prev address
							getProfileTools().updateProperty(getPropertyManager().getPreviousShippingAddressPropertyName(),
									newDefaultShippingAddress, getProfile());
							getProfileTools().updateProperty(getPropertyManager().getPrevShippingAddressNickNamePropertyName(),
									lAddressNickName, getProfile());
						}*/
					}
					if(isDefaultBillingAddress()){
						
						//final RepositoryItem newDefaultShippingAddress1 = profileTools.getProfileAddress(getProfile(), newDefaultShippingAddress);
						
						getProfileTools().updateProperty(getPropertyManager().getBillingAddressPropertyName(),
								newDefaultShippingAddress, getProfile());
					}

		if (isLoggingDebug()) {
			logDebug("End : TRUCSRProfileFormHandler.handleSetAddrAsDefault()");
		}
	}

	/**
	 * This method is for the required tasks to be done before setting the address as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void preSetAddrAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.preSetAddrAsDefault()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.preSetAddrAsDefault()");
		}
	}

	/**
	 * This method is for the required tasks to be done after setting the address as default.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	protected void postSetAddrAsDefault(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Start: TRUCSRProfileFormHandler.postSetAddrAsDefault()");
		}
		if (isLoggingDebug()) {
			logDebug("End: TRUCSRProfileFormHandler.postSetAddrAsDefault()");
		}
	}
	
	
	
}