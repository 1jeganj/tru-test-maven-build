package com.tru.commerce.pricing;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.pricing.priceLists.PriceListException;
import atg.multisite.Site;
import atg.repository.RepositoryItem;

import com.tru.common.TRUConstants;

/**
 * The Class TRUCSRPricingTools.
 */
public class TRUCSRPricingTools extends TRUPricingTools {
	
	/** This holds the Default PriceList Property Name. **/
	private String mDefaultPriceListPropertyName;

	/** This holds the Default Sale PriceList Property Name. **/
	private String mDefaultSalePriceListPropertyName;
	
	/** This holds the reference for TRUPriceProperties. **/
	private TRUPriceProperties mPriceProperties;
	
	/**
	 * This method will return mPriceProperties.
	 *
	 * @return the mPriceProperties.
	 */
	public TRUPriceProperties getPriceProperties() {
		return mPriceProperties;
	}
	/**
	 * This method will set mPriceProperties with pPriceProperties.
	 * @param pPriceProperties the mPriceProperties to set.
	 */
	public void setPriceProperties(TRUPriceProperties pPriceProperties) {
		mPriceProperties = pPriceProperties;
	}
	/**
	 * This method will return mDefaultPriceListPropertyName.
	 *
	 * @return the mDefaultPriceListPropertyName.
	 */
	public String getDefaultPriceListPropertyName() {
		return mDefaultPriceListPropertyName;
	}
	/**
	 * This method will set mDefaultPriceListPropertyName with pDefaultPriceListPropertyName.
	 * @param pDefaultPriceListPropertyName the mDefaultPriceListPropertyName to set.
	 */

	public void setDefaultPriceListPropertyName(String pDefaultPriceListPropertyName) {
		mDefaultPriceListPropertyName = pDefaultPriceListPropertyName;
	}
	/**
	 * This method will return mDefaultSalePriceListPropertyName.
	 *
	 * @return the mDefaultSalePriceListPropertyName.
	 */
	public String getDefaultSalePriceListPropertyName() {
		return mDefaultSalePriceListPropertyName;
	}
	/**
	 * This method will set mDefaultSalePriceListPropertyName with pDefaultSalePriceListPropertyName.
	 * @param pDefaultSalePriceListPropertyName the mDefaultSalePriceListPropertyName to set.
	 */

	public void setDefaultSalePriceListPropertyName(
			String pDefaultSalePriceListPropertyName) {
		mDefaultSalePriceListPropertyName = pDefaultSalePriceListPropertyName;
	}
	
	/** This holds the Agent Tools. **/	
	private CSRAgentTools mCSRAgentTools;
	
	/**
	 * This method will set mCSRAgentTools with pCSRAgentTools.
	 * @param pCSRAgentTools the mCSRAgentTools to set.
	 */
	 public void setCSRAgentTools(CSRAgentTools pCSRAgentTools)
	  {
	    this.mCSRAgentTools = pCSRAgentTools;
	  }

	/**
	 * This method will return mCSRAgentTools.
	 * 
	 * @return the mCSRAgentTools.
	 */
	  public CSRAgentTools getCSRAgentTools()
	  {
	    return this.mCSRAgentTools;
	  }

	/**
	 * This method will return the list for the SKU.
	 * @param pSkuId - SKU ID to get Price.
	 * @param pProductId - Product ID to get Price.
	 * @return listPrice - List price item.
	 */
	public double getListPriceForSKU(String pSkuId, String pProductId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRPricingTools  method: getListPriceForSKU]");
			vlogDebug("SKU Id : {0} ", pSkuId);
		}
		RepositoryItem priceItem = null;
		RepositoryItem priceList = null;
		double listPrice = TRUConstants.DOUBLE_ZERO;
		CSRAgentTools csrAgentTools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
		priceList =csrenvtools.getCurrentPriceList();
		try {
			if(priceList != null && pSkuId != null){
				priceItem = getPriceListManager().getPrice(priceList, pProductId, pSkuId);
			}else{
				vlogDebug("Price list : {0} pProductId  : {1} pSkuId : {2}", priceList, pProductId, pSkuId);
			}
			if(priceItem != null){
				listPrice =(double) getValueForProperty(priceItem);
			}
		} catch (PriceListException exc) {
			if (isLoggingError()) {
				vlogError(
						"PriceListException: while getting price for SKU : {0} Product : {1} with exception : {2}",
						pSkuId,
						pProductId,
						exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPricingTools  method: getListPriceForSKU]");
		}
		return listPrice;
	}

	/**
	 * This method will return the sale for the SKU.
	 * @param pSkuId - SKU ID to get Price.
	 * @param pProductId - Product ID to get Price.
	 * @return listPrice - Sale price item.
	 */
	public double getSalePriceForSKU(String pSkuId, String pProductId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRPricingTools  method: getSalePriceForSKU]");
			vlogDebug("SKU Id : {0} ", pSkuId);
		}
		RepositoryItem priceItem = null;
		RepositoryItem priceList = null;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		CSRAgentTools csrAgentTools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
		priceList=csrenvtools.getCurrentSalePriceList();
				try {
			if(priceList != null && pSkuId != null){
				priceItem = getPriceListManager().getPrice(priceList, pProductId, pSkuId);
			}else{
				vlogDebug("Price list : {0} pProductId  : {1} pSkuId : {2}", priceList, pProductId, pSkuId);
			}
			
			if(priceItem != null){
				salePrice =(double) getValueForProperty(priceItem);
			}
		} catch (PriceListException exc) {
			if (isLoggingError()) {
				vlogError(
						"PriceListException: while getting price for SKU : {0} Product : {1} with exception : {2}",
						pSkuId,
						pProductId,
						exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPricingTools  method: getSalePriceForSKU]");
		}
		return salePrice;
	}
	
	
	
		
	
	/**
	 * Gets the value for list price property.
	 *
	 * @param pItem the item
	 * @return the value for list price property
	 */
	public Object getValueForProperty(RepositoryItem pItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRPricingTools  method: getValueForProperty]");
			vlogDebug("pPropertyName : {0} and Item is : {1}", getPriceProperties().getListPricePropertyName(), pItem);
		}
		Object value = null;

		if (pItem != null) {
			value = pItem.getPropertyValue(getPriceProperties().getListPricePropertyName());
		}
		vlogDebug("Value for the pPropertyName : {0} is : {1} Item is : {2}", getPriceProperties().getListPricePropertyName(), value, pItem);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPricingTools  method: getValueForProperty]");
		}
		return value;
	}
	
	/**
	 * This method is to validate the threshold prices. Which is used to display the strike through prices.
	 * 
	 * @param pSite
	 *            - Site.
	 * @param pListPrice - list price.
	 * @param pSalePrice - sale prices.
	 * @return boolean - true or false base on thresholdAmount and thresholdPersentage .
	 */
	public boolean validateThresholdPrices(Site pSite,
			double pListPrice, double pSalePrice) {
		if (isLoggingDebug()) {
			logDebug("Entered into [Class: TRUCSRPricingTools  method: validateThresholdPrices]");
			vlogDebug("Site : {0}  pListPrice : {1} pSalePrice : {2}", pSite, pListPrice, pSalePrice);
		}
		TRUPriceProperties priceProperties = getPriceProperties();
		double thresholdPricePercentage = TRUConstants.DOUBLE_ZERO;
		double thresholdPriceAmount = TRUConstants.DOUBLE_ZERO;
		if (pSite != null) {
			if(pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName()) == null &&
					pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName()) == null){
				return Boolean.FALSE;
			}
			if (pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName()) != null) {
				thresholdPricePercentage =
						(double) pSite.getPropertyValue(priceProperties.getThresholdPricePercentagePropertyName());
			}
			if (pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName()) != null) {
				thresholdPriceAmount =
						(double) pSite.getPropertyValue(priceProperties.getThresholdPriceAmountPropertyName());
			}
			vlogDebug(
					"Site level Threshold Price Percentage : {0} Threshold Price Amount : {1}",
					thresholdPricePercentage,
					thresholdPriceAmount);
		}
		if (pListPrice != TRUConstants.DOUBLE_ZERO && pSalePrice != TRUConstants.DOUBLE_ZERO) {
			double diffPercentage = round((pListPrice - pSalePrice) / pListPrice * TRUConstants.HUNDERED);
			double diffAmount = round(pListPrice - pSalePrice);
			if (diffPercentage >= thresholdPricePercentage || diffAmount > thresholdPriceAmount) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPricingTools  method: validateThresholdPrices]");
		}
		return Boolean.FALSE;
	}
	
	/**
	 * This method is to calculate the saved amount and save percentages in case of any difference in the list and sale.
	 * prices.
	 * 
	 * @param pListPrice - list price.
	 * @param pSalePrice - sale prices.
	 *  @return Map - savings.
	 */
	public Map<String, Double> calculateSavings(double pListPrice, double pSalePrice) {
		if (isLoggingDebug()) {
			logDebug("Entered into [Class: TRUCSRPricingTools  method: calculateSavings]");
			vlogDebug("pListPrice : {0} pSalePrice : {1}", pListPrice, pSalePrice);
		}
		Map<String, Double> savingsMap = new HashMap<String, Double>();
		double savedAmount = pListPrice - pSalePrice;
		double savedPercentage=TRUConstants.DOUBLE_ZERO;
		if(pListPrice!= TRUConstants.DOUBLE_ZERO){
			savedPercentage = (pListPrice - pSalePrice) / pListPrice * TRUConstants.HUNDERED;
		}
		vlogDebug("Saved amount : {0} Saved Perecentage : {1}", savedAmount, savedPercentage);
		savingsMap.put(TRUConstants.SAVED_AMOUNT, round(savedAmount));
		savingsMap.put(TRUConstants.SAVED_PERCENTAGE, round(savedPercentage));
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPricingTools  method: calculateSavings]");
		}
		return savingsMap;
	}
	
}
