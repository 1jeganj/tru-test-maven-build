/**
 * TRUCSRPriceDroplet.java
 */
package com.tru.commerce.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.pricing.priceLists.PriceDroplet;
import atg.multisite.Site;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;



import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.pricing.TRUCSRPricingTools;
import com.tru.common.TRUConstants;


/**
 * The Class TRUCSRPriceDroplet.
 */
public class TRUCSRPriceDroplet extends PriceDroplet {

	
	/** The m pricing tools. */
	private TRUCSRPricingTools mPricingTools;

	/**
	 * This method populates the prices to bean.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest.
	 * @param pResponse
	 *            - DynamoHttpServletResponse.
	 * @throws ServletException -ServletException.
	 * @throws IOException - IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRPriceDroplet  method: service]");
		}
		double listPrice=TRUConstants.DOUBLE_ZERO;
		double salePrice=TRUConstants.DOUBLE_ZERO;
		Site site=(Site)pRequest.getLocalParameter(TRUCSCConstants.SITE);
		//Need to get from jsp
		String skuId=(String)pRequest.getLocalParameter(TRUConstants.SKU_ID);
		//Need to get from jsp
		String productId=(String)pRequest.getLocalParameter(TRUConstants.PRODUCT_ID);
		
		//String siteId=(String)pRequest.getLocalParameter("siteId");
		// Get prices for SKU		
		listPrice = getPricingTools().getListPriceForSKU(skuId, productId);
		salePrice = getPricingTools().getSalePriceForSKU(skuId, productId);
		vlogDebug("List Price for SKU : {0} is : {1}", skuId, listPrice);
		vlogDebug("Sale Price for SKU : {0} is : {1}", skuId, salePrice);
		if(listPrice != TRUConstants.DOUBLE_ZERO && salePrice != TRUConstants.DOUBLE_ZERO){
			// This method will validates whether strike through price need to be displayed or not.
			boolean calculateSavings = getPricingTools().validateThresholdPrices(site, listPrice, salePrice);
			// This method will calculate the Savings and updates the values in bean.
			if (calculateSavings) {
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				pRequest.setParameter(TRUConstants.SAVED_AMOUNT, savingsMap.get(TRUConstants.SAVED_AMOUNT));
				pRequest.setParameter(TRUConstants.SAVED_PERCENTAGE, savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
				pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
				pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_TRUE, pRequest, pResponse);
			} else {
				pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
				pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM_FALSE, pRequest, pResponse);
			}
		}else{
			pRequest.setParameter(TRUConstants.LIST_PRICE, listPrice);
			pRequest.setParameter(TRUConstants.SALE_PRICE, salePrice);
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRPriceDroplet  method: service]");
		}
	}

	
	/**
	 * Gets the pricing tools.
	 *
	 * @return the pricing tools
	 */
	public TRUCSRPricingTools getPricingTools() {
		return mPricingTools;
	}

	
	/**
	 * Sets the pricing tools.
	 *
	 * @param pPricingTools the new pricing tools
	 */
	public void setPricingTools(TRUCSRPricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

}
