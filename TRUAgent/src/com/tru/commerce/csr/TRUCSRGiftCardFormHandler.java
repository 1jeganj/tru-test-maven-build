/**
 * 
 */
package com.tru.commerce.csr;

import java.util.HashMap;
import java.util.Map;

import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.profile.CommercePropertyManager;

import com.tru.commerce.order.purchase.TRUGiftCardFormHandler;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;

/**
 * The Class TRUCSRGiftCardFormHandler.
 *
 * @author
 */
public class TRUCSRGiftCardFormHandler extends TRUGiftCardFormHandler {
	
	
	
	/** This holds the Agent Tools. **/	
	private CSRAgentTools mCSRAgentTools;
	
	/**
	 * This method will set mCSRAgentTools with pCSRAgentTools.
	 * @param pCSRAgentTools the mCSRAgentTools to set.
	 */
	 public void setCSRAgentTools(CSRAgentTools pCSRAgentTools)
	  {
	    this.mCSRAgentTools = pCSRAgentTools;
	  }

	/**
	 * This method will return mCSRAgentTools.
	 * 
	 * @return the mCSRAgentTools.
	 */
	  public CSRAgentTools getCSRAgentTools()
	  {
	    return this.mCSRAgentTools;
	  }

	
	@SuppressWarnings("rawtypes")
	@Override
	protected Map createRepriceParameterMap()
	  {
		CSRAgentTools csrAgentTools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();

	    HashMap parameters = new HashMap();

	    CommercePropertyManager props = getCommercePropertyManager();
	    if (props != null)
	    {
	      String priceListPropertyName = getCommercePropertyManager().getPriceListPropertyName();
	      String priceSalePropertyName = getCommercePropertyManager().getSalePriceListPropertyName();
	      parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
	      parameters.put(priceListPropertyName, csrenvtools.getCurrentPriceList());
	      parameters.put(priceSalePropertyName, csrenvtools.getCurrentSalePriceList());
	    }
	    return parameters;
	  }
	
	
}
