package com.tru.commerce.csr.locations;

import java.util.Map;

import atg.repository.RepositoryItem;

import com.tru.commerce.locations.TRUStoreLocatorTools;




/**
 * @author PA
 * Class to get Store closing hours.
 *
 */
public class TRUCSRStoreLocatorTools extends TRUStoreLocatorTools {
	
	/**
	 * @param pStoreItem to set storeItem
	 * @return closingTimeMsg
	 */
	public String getStoreClosingHoursTime(RepositoryItem pStoreItem){
		if(isLoggingDebug()){
			logDebug("Entering to TRUCSRStoreLocatorTools.getStoreClosingHoursTime method");
		}
		final Map<String,String> storeHoursMap = getStoreWorkingHours(pStoreItem);
		final String closingTimeMsg = getStoreClosingTime(storeHoursMap);
		if(isLoggingDebug()){
			logDebug("Exiting from TRUCSRStoreLocatorTools.getStoreClosingHoursTime method");
		}
		return closingTimeMsg;
	}
}
