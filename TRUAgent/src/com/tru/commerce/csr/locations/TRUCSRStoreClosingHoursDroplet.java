package com.tru.commerce.csr.locations;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.common.TRUConstants;

/**
 * This droplet will populate store closing hours data
 * @author PA
 *
 */
public class TRUCSRStoreClosingHoursDroplet extends DynamoServlet {
	
	/** Holds the mLocationTools. */
	private TRUCSRStoreLocatorTools mLocationTools;

	
	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools
	 */
	public TRUCSRStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set
	 */
	public void setLocationTools(TRUCSRStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}


	/**
	 * Service method.
	 *
	 * @param pRequest
	 *             the DynamoHttpServletRequest
	 *  pRequest
	 *             the DynamoHttpServletRequest
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
		final RepositoryItem storeItem = (RepositoryItem) pRequest.getLocalParameter(TRUCSCConstants.STORE_ITEM);
		String storeWorkingHours = getLocationTools().getStoreClosingHoursTime(storeItem);
		if(storeWorkingHours != null){
			pRequest.setParameter(TRUCSCConstants.STORE_HOURS, storeWorkingHours);
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
	}
}
