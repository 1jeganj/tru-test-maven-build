/*
 *
 * Copyright (C) 2015, Professional Access (A division of Zensar
 * Technologies Limited).  All Rights Reserved. No use, copying or
 * distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included
 * on all copies, modifications and derivatives of this work.
 */
package com.tru.commerce.csr.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.commerce.csr.promotion.TRUCSRPromotionTools;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.csr.catalog.TRUCSRCatalogTools;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;


/**
 * This TRUCSRItemLevelPromotionDroplet.
 *
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUCSRItemLevelPromotionDroplet extends DynamoServlet {
	
	private CatalogProperties mCatalogProperties;

	/**
	 * Holds reference for TRUConfiguration.
	 */
	private TRUConfiguration mTruConfiguration;
	
	/**property to hold promotionTools.
	 */
	private TRUCSRPromotionTools mPromotionTools;
	
	/** The m catalog properties. */
	private TRUCSRCatalogTools mCatalogTools;
	
	/**
	 * @param pCatalogProperties to set mCatalogProperties
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties){
	    this.mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * @return mCatalogTools
	 */
	public CatalogProperties getCatalogProperties(){
	    return this.mCatalogProperties;
	}
	
	/**
	 * @return mCatalogTools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools to set mCatalogTools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * Returns the truConfiguration.
	 * 
	 * @return the truConfiguration.
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets/updates the truConfiguration.
	 * 
	 * @param pTruConfiguration
	 *            the truConfiguration to set.
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}
	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools.
	 */
	public TRUCSRPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set.
	 */
	public void setPromotionTools(TRUCSRPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
	/**
	 * Service.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUItemLevelPromotionDroplet  method: service]");
		}
		
		Set<String> promotionsIds = null;
		String skuId = (String) pRequest.getLocalParameter(TRUCSCConstants.SKU_ID);
		RepositoryItem skuItem = null;
		int promoCount = TRUConstants.SIZE_ONE;
		String promotionCountinPDP = getTruConfiguration().getPromotionCountinPDP();
		
		if(!StringUtils.isBlank(promotionCountinPDP)){
			try{
				promoCount = Integer.parseInt(promotionCountinPDP);
			}catch(NumberFormatException nfe){
				if(isLoggingError()){
					vlogError("Provided Promotion Count {0} is not a Integer Value {1}: ", promotionCountinPDP,nfe);
				}
			}
		}
		if(skuId != null){
			try {
				skuItem = getCatalogTools().findSKU(skuId);
			} catch (RepositoryException e) {
				if(isLoggingError()){
					logError("Exception while finding sku from CatalogTools for the skuId - "+skuId, e);
				}
 			}
 			if (skuItem != null) {
 				TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
 				promotionsIds = getPromotionTools().getPromotionIds(catalogProperties, skuItem);
			}
		}
		
		if (promotionsIds != null && !promotionsIds.isEmpty()) {
			List<RepositoryItem> sortedPromotionsList = getSortedPromotionDetails(promotionsIds);
			if (sortedPromotionsList != null && !sortedPromotionsList.isEmpty() && sortedPromotionsList.size() > promoCount) {
				pRequest.setParameter(TRUConstants.RANK_ONE_PROMOTION, sortedPromotionsList.get(TRUConstants.ZERO));
				if(isLoggingDebug()){
					vlogDebug("Rank One promotion is  : {0}", sortedPromotionsList.get(TRUConstants.ZERO));
				}
				sortedPromotionsList = sortedPromotionsList.subList(TRUConstants.INTEGER_NUMBER_ONE, promoCount);
				pRequest.setParameter(TRUConstants.PROMOTION_COUNT, sortedPromotionsList.size());
				pRequest.setParameter(TRUConstants.PROMOTIONS, sortedPromotionsList);
				if(isLoggingDebug()){
					vlogDebug("Special offers to be displayed in PDP is : {0}", sortedPromotionsList);
				}
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			} else if (sortedPromotionsList != null && !sortedPromotionsList.isEmpty()){
				pRequest.setParameter(TRUConstants.RANK_ONE_PROMOTION, sortedPromotionsList.get(TRUConstants.ZERO));
				if(isLoggingDebug()){
					vlogDebug("Rank One promotion is  : {0}", sortedPromotionsList.get(TRUConstants.ZERO));
				}
				pRequest.setParameter(TRUConstants.PROMOTION_COUNT, sortedPromotionsList.size());
				pRequest.setParameter(TRUConstants.PROMOTIONS, sortedPromotionsList);
				if(isLoggingDebug()){
					vlogDebug("Special offers to be displayed in PDP is : {0}", sortedPromotionsList);
				}
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			}
		} else {
			pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			if(isLoggingDebug()){
				logDebug("Promotion list is emplty");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemLevelPromotionDroplet  method: service]");
		}
	}
	

	/**
	 * This will return the sorted Promotion details according to the priority.
	 * @param pPromotionsIds for promotionIds
	 * @return List<RepositoryItem>
	 */
	private List<RepositoryItem> getSortedPromotionDetails(Set<String> pPromotionsIds){
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUItemLevelPromotionDroplet  method: getSortedPromotionDetails]");
		}
		List<RepositoryItem> promoListExp = new ArrayList<RepositoryItem>();
		List<RepositoryItem> promoList = new ArrayList<RepositoryItem>();
		RepositoryItem promotionItem = null;
		TRUPromotionTools promotionTools = getPromotionTools();
		Repository promoRepository = promotionTools.getPromotions();
		 TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools().getPricingModelProperties();
		if(promoRepository != null){
			 Date now = getPromotionTools().getCurrentDate().getTimeAsDate();
			for(String lPromoId : pPromotionsIds){
				try {
					int indexOf = lPromoId.indexOf(TRUCommerceConstants.PIPELINE);
					String promoId = lPromoId.substring(0,indexOf);
					promotionItem = promotionTools.getItemForId(pricingModelProperties.getPromotionItemDescName(), promoId);
					
					//checking promotion is expired or not
					if(promotionItem != null && !promotionTools.checkPromotionExpiration(promotionItem, now)){
							promoListExp.add(promotionItem);
						if(isLoggingDebug()){
							vlogDebug("Pormotion Item : {0} is valid", promotionItem);
						}
					}
				} catch (RepositoryException exc) {
					if(isLoggingError()){
						vlogError("No Promotion Found for Promotion ID : {0}",lPromoId);
					}
				}
			}
			for(RepositoryItem lPromoItem : promoListExp){
					//checking promotion is started or not
					if(lPromoItem != null && getPromotionTools().checkPromotionStartDate(lPromoItem, now)){
						promoList.add(lPromoItem);
						if(isLoggingDebug()){
							vlogDebug("Pormotion Item : {0} is valid", lPromoItem);
						}
					}
			}
		}
		List<RepositoryItem> list = new ArrayList<RepositoryItem>(promoList);
		getPromotionTools().compareAndAdd(list);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUItemLevelPromotionDroplet  method: getSortedPromotionDetails]");
		}
		return list;
	}
}
