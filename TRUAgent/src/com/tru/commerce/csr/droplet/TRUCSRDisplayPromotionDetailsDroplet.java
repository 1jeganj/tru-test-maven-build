/**
 * 
 */
package com.tru.commerce.csr.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.csr.promotion.TRUCSRPromotionTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.common.TRUConstants;

/**
 * @author PA
 * Droplet to display the promotion details
 *
 */
public class TRUCSRDisplayPromotionDetailsDroplet extends DynamoServlet {
	
	/**property to hold promotionTools.
	 */
	private TRUCSRPromotionTools mPromotionTools;
	
	/**
	 * Gets the promotion tools.
	 *
	 * @return the promotionTools.
	 */
	public TRUCSRPromotionTools getPromotionTools() {
		return mPromotionTools;
	}
	/**
	 * Sets the promotion tools.
	 *
	 * @param pPromotionTools the promotionTools to set.
	 */
	public void setPromotionTools(TRUCSRPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}
	
	/**
	 * Service.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRDisplayPromotionDetailsDroplet  method: service]");
		}
		String promotionDetails = null;
		TRUCSRPromotionTools promotionTools = getPromotionTools();
		String promotionId = (String) pRequest.getLocalParameter(TRUCSCConstants.PROMOTION_ID);
		if (promotionId != null) {
			promotionDetails = promotionTools.getPromoDetailsByPromoId(promotionId);
			if(promotionDetails != null){
				promotionDetails = promotionDetails.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
				pRequest.setParameter(TRUCSCConstants.PROMOTION_DETAILS, promotionDetails);
				pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			}else{
				pRequest.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pRequest, pResponse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRDisplayPromotionDetailsDroplet  method: service]");
		}
	}

}
