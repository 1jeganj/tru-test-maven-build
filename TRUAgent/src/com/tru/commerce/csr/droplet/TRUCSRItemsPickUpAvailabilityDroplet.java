/*
 *
 * Copyright (C) 2015, Professional Access (A division of Zensar
 * Technologies Limited).  All Rights Reserved. No use, copying or
 * distribution of this work may be made except in accordance with a
 * valid license agreement from PA, India. This notice must be included
 * on all copies, modifications and derivatives of this work.
 */
package com.tru.commerce.csr.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.csr.order.CSROrderHolder;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.csr.catalog.TRUCSRCatalogTools;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUStoreItemVO;



/**
 * This TRUCSRItemLevelPromotionDroplet.
 *
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUCSRItemsPickUpAvailabilityDroplet extends DynamoServlet {
	
	/** Property To Hold commerce property manager. */
	private TRUCommercePropertyManager mCommercePropertyManager;
	
	/* This property used to hold orderManager */
	private TRUOrderManager mOrderManager;

	/** Holds the mLocationTools. */
	private TRUStoreLocatorTools mLocationTools;
	
	/**
	 * property to hold PropertyManager.
	 */
 	private CSROrderHolder mCartComponent;
 	
 	/** The m catalog properties. */
	private TRUCSRCatalogTools mCatalogTools;
	
	
	/**
	 * Inventory manager.
	 */
	private TRUCoherenceInventoryManager mInventoryManager;
	
	/**
	 * Gets the commerce property manager.
	 *
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets the commerce property manager.
	 *
	 * @param pCommercePropertyManager the new commerce property manager
	 */
	public void setCommercePropertyManager(
			TRUCommercePropertyManager pCommercePropertyManager) {
		this.mCommercePropertyManager = pCommercePropertyManager;
	}
	
	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		this.mOrderManager = pOrderManager;
	}
	
	/**
	 * Gets the inventory manager.
	 *
	 * @return the inventoryManager.
	 */
	public TRUCoherenceInventoryManager getInventoryManager() {
		return mInventoryManager;
	}

	/**
	 * Sets the inventory manager.
	 *
	 * @param pInventoryManager - the inventoryManager to set.
	 */
	public void setInventoryManager(TRUCoherenceInventoryManager pInventoryManager) {
		mInventoryManager = pInventoryManager;
	}

	
	/**
	 * @return mCatalogTools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}


	/**
	 * @param pCatalogTools to set mCatalogTools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the mCartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent the mCartComponent to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		mCartComponent = pCartComponent;
	}
	
	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools
	 */
	public TRUStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set
	 */
	public void setLocationTools(TRUStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}

	/**
	 * service method.
	 *
	 * @param pReq the request to set
	 * @param pRes the response to set
	 * @throws ServletException throws servletException
	 * @throws IOException throws IOException
	 */
 	public void service(DynamoHttpServletRequest pReq,
 			DynamoHttpServletResponse pRes) throws ServletException,
 			IOException {
 		
 		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRItemsPickUpAvailabilityDroplet  method: service]");
		}
 		final String storeId = (String) pReq.getLocalParameter(TRUCSCConstants.STORE_ID);
 		final String orderId = (String) pReq.getLocalParameter(TRUCSCConstants.ORDER_ID);
 		final String skuId = (String) pReq.getLocalParameter(TRUCSCConstants.SKU_ID);
 		final String quantity = (String) pReq.getLocalParameter(TRUCSCConstants.QUANTITY);
 		long qty = TRUCSCConstants.ZERO_LONG;
 		if(quantity != null){
 			qty = Long.parseLong(quantity);
 		}
 		
 		
		final TRUStoreItemVO storeItemVO = new TRUStoreItemVO();
 		TRUOrderImpl currentOrder = null;
 		if(orderId != null){
			try {
				currentOrder = (TRUOrderImpl) getOrderManager().loadOrder(orderId);
			} catch (CommerceException e1) {
				logError("Exception while loading the order for order number : "+orderId, e1);
			}
 		}
		if(currentOrder != null){
			if(isLoggingDebug()){
				logDebug("This will return the channel availability messages in order level for id: "+orderId);
			}
			List<CommerceItem> ciItemList = currentOrder.getCommerceItems();
	 		Map<String, String> storeHoursMsgMap = new HashMap<String,String>();
	 		String s2s = null;
		 		for(CommerceItem ciItem : ciItemList){
		 			String catalogRefId = ciItem.getCatalogRefId();
		 			try {
						RepositoryItem skuItem = getCatalogTools().findSKU(catalogRefId);
						Boolean s2sValue=(Boolean) skuItem.getPropertyValue(getCommercePropertyManager().getShipToStoreEligible());
						String skuName =(String)skuItem.getPropertyValue(getCommercePropertyManager().getDisplayName());
						//check for ShipToStoreEligible flag from SKU
						if(s2sValue!=null && s2sValue.booleanValue())
						{
							s2s=TRUCommerceConstants.YES;
						}else{
							s2s=TRUCommerceConstants.NO;
						}
						
						//check for inStorePickUp flag from SKU
						String ispu=(String) skuItem
								.getPropertyValue(getCommercePropertyManager().getItemInStorePickUp());
						if(TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
							ispu=TRUCommerceConstants.YES;
						}else{
							ispu=TRUCommerceConstants.NO;
						}
						
						if((s2s.equals(TRUCommerceConstants.YES) ||ispu.equals(TRUCommerceConstants.YES))){
							long itemQuantity = ciItem.getQuantity();	
							getStoreHoursMap(true, storeItemVO, catalogRefId, skuName, storeId, Long.toString(itemQuantity), storeHoursMsgMap);
						} 
		 		}catch (RepositoryException e) {
		 			if(isLoggingError()){
		 				logError("Repository Exception from TRUCSRItemsPickupAvailabilityDroplet ", e);
		 			}
				} 
	 		}
			if(!storeHoursMsgMap.isEmpty()){
		 		pReq.setParameter(TRUCSCConstants.STORE_HOURS_MSG_MAP, storeHoursMsgMap);
		 		pReq.serviceLocalParameter(TRUConstants.OUTPUT, pReq, pRes);
			}else{
				pReq.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pReq, pRes);
				if(isLoggingDebug()){
					logDebug("Store hours map is empty");
				}
			}
		}else if (skuId != null){
			if(isLoggingDebug()){
				logDebug("This will return the channel availability messages in sku level for id: "+skuId);
			}
			Map<String, String> storeHoursMsgMap = new HashMap<String,String>();
			getStoreHoursMap(false, storeItemVO, skuId, null, storeId, quantity, storeHoursMsgMap);
			if(!storeHoursMsgMap.isEmpty()){
		 		pReq.setParameter(TRUCSCConstants.STORE_HOURS_MSG_MAP, storeHoursMsgMap);
		 		pReq.serviceLocalParameter(TRUConstants.OUTPUT, pReq, pRes);
			}else{
				pReq.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pReq, pRes);
				if(isLoggingDebug()){
					logDebug("Store hours map is empty");
				}
			}
		}else{
			pReq.serviceLocalParameter(TRUConstants.EMPTY_OPARAM, pReq, pRes);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRItemsPickupAvailabilityDroplet  method: service]");
		}
 	}
 	
 	/**
 	 * This method will populate getStoreHoursMap map
 	 * @param pOrderData stores boolean value
 	 * @param pStoreItemVO holds storeItemVO
 	 * @param pSkuId holds skuId
 	 * @param pSkuName holds skuName
 	 * @param pStoreId holds storeId
 	 * @param pQuantity holds qty value
 	 * @param pStoreHoursMsgMap holds pStoreHoursMsgMap
 	 */
 	private void getStoreHoursMap(boolean pOrderData, TRUStoreItemVO pStoreItemVO, String pSkuId, String pSkuName, String pStoreId, String pQuantity, Map<String, String> pStoreHoursMsgMap){
 		if(isLoggingDebug()){
 			logDebug("entering to getStoreHoursMap method");
 		}
 		int availability;
		long availableStock;
		double availableStockAtStore = TRUConstants.DOUBLE_ZERO;
		TRUCoherenceInventoryManager invManager = getInventoryManager();
		TRUStoreLocatorTools locatorTools = (TRUStoreLocatorTools)getLocationTools();
		pStoreItemVO.setSkuId(pSkuId);
		try{
			availability = invManager.queryAvailabilityStatus(pSkuId,  pStoreId);
			if(availability == TRUCommerceConstants.INT_IN_STOCK){
				pStoreItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
			}
			RepositoryItem storeItem = locatorTools.getStoreItem(pStoreId);
			availableStock = invManager.queryStockLevel(pSkuId, pStoreId);
			availableStockAtStore = availableStock;

			if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK && pStoreId != null){
				storeItem = locatorTools.getStoreItem(pStoreId);
				if(storeItem != null){
					Double storeItemWareHouseLocationId = (Double)storeItem.getPropertyValue(
							locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
					availability=invManager.queryAvailabilityStatus(pSkuId,Long.toString(Math.round(storeItemWareHouseLocationId)));
					availableStock=invManager.queryStockLevel(pSkuId,Long.toString(Math.round(storeItemWareHouseLocationId)));
					if(availability == TRUCommerceConstants.INT_IN_STOCK){
						pStoreItemVO.setWareHouseInventoryFlag(true);
						pStoreItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
					}else{
						pStoreItemVO.setInventoryItemStatus(TRUCommerceConstants.OUT_OF_STOCK);
					}
				}
			}
			
			locatorTools.setStocklevel(availableStockAtStore);
			locatorTools.setItemQuantity(pQuantity);
			String storeHoursMsg = locatorTools.getStoreHoursMsg(storeItem, pStoreItemVO);
			if(!pOrderData){
				pStoreHoursMsgMap.put(pSkuId+TRUCSCConstants.HIPHON+pStoreId, storeHoursMsg);
			}else{			
				pStoreHoursMsgMap.put(pSkuId+TRUCSCConstants.HIPHON+pSkuName, storeHoursMsg);
			}

			if(isLoggingDebug()){
	 			logDebug("exit from getStoreHoursMap method");
	 		}
	 	}catch (InventoryException ie) {
			if(isLoggingError()){
				logError("Inventory Exception from TRUCSRItemsPickupAvailabilityDroplet ", ie);
			}
		}
 	}
}
