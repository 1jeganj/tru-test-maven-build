/**
 * 
 */
package com.tru.commerce.csr.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUSiteVO;
import com.tru.utils.TRUGetSiteTypeUtil;



/**
 * @author PA
 *
 */
public class TRUCSRSiteDroplet extends DynamoServlet {
	
	/**
	 *  The m tru get site type mTRUGetSiteTypeUtil. 
	 *  
	 */
	private TRUGetSiteTypeUtil mTRUGetSiteTypeUtil;
	
	/** Property Holds mSiteContextManager. */
	private SiteContextManager mSiteContextManager;

	/**
	 * Gets the TRU get site type util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUGetSiteTypeUtil getTRUGetSiteTypeUtil() {
		return mTRUGetSiteTypeUtil;
	}



	/**
	 * Sets the TRU get site type util.
	 *
	 * @param pTRUGetSiteTypeUtil the new TRU get site type util
	 */
	public void setTRUGetSiteTypeUtil(TRUGetSiteTypeUtil pTRUGetSiteTypeUtil) {
		this.mTRUGetSiteTypeUtil = pTRUGetSiteTypeUtil;
	}
	
	/**
	 * Returns Site Context Manager.
	 *
	 * @return the SiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * sets Site Context Manager.
	 *
	 * @param pSiteContextManager the SiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}
	
	/**
	 * service method.
	 *
	 * @param pRequest the request to set
	 * @param pResponse the response to set
	 * @throws ServletException throws servletException
	 * @throws IOException throws IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		String siteProductionUrl = null;
		StringBuffer productUrl = new StringBuffer();
		Site currentSite = null;
		String siteId = null;
				
		siteId = (String)pRequest.getLocalParameter(TRUCSCConstants.SITE_ID);
		try {
			currentSite = getSiteContextManager().getSite(siteId);
		} catch (SiteContextException e) {
			if(isLoggingError()){
				logError("Exception while getting the site object for site id: "+siteId, e);
			}
		}
			
		if(currentSite != null) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			TRUSiteVO truSiteVO = getTRUGetSiteTypeUtil().getSiteInfo(request, currentSite);
			if(truSiteVO != null ) {
				siteProductionUrl = truSiteVO.getSiteURL();
			}
	
			if(!StringUtils.isEmpty(siteProductionUrl) && 
					!siteProductionUrl.equals(TRUCommerceConstants.SINGLE_FORWARD_SLASH)) {
					productUrl.append(TRUCommerceConstants.HTTP_SCHEME);
					productUrl.append(TRUCommerceConstants.COLON_SYMBOL);
					productUrl.append(TRUCommerceConstants.FORWARD_SLASH);
					productUrl.append(siteProductionUrl);
			}
			pRequest.setParameter(TRUCSCConstants.SITE_URL, productUrl.toString());
			pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
			
			if (isLoggingDebug()) {
				logDebug("END :: Inside TRUProductPageUtil.constructPDPURLWithOnlinePID() method ");
			}
		}
	}
}
