package com.tru.commerce.csr.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.catalog.custom.CatalogProperties;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.csr.catalog.TRUCSRCatalogTools;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.common.TRUConstants;

/**
 * @author PA
 *
 */
public class TRUCSRSuggestedMfrAgeDroplet extends DynamoServlet{
	
	/** The m catalog tools. */
	private TRUCSRCatalogTools mCatalogTools;
	
	/** The m catalog properties. */
	private CatalogProperties mCatalogProperties;
	
	/**
	 * @return mCatalogTools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools to set mCatalogTools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @param pCatalogProperties to set mCatalogProperties
	 */
	public void setCatalogProperties(CatalogProperties pCatalogProperties){
	    this.mCatalogProperties = pCatalogProperties;
	}
	
	/**
	 * @return mCatalogTools
	 */
	public CatalogProperties getCatalogProperties(){
	    return this.mCatalogProperties;
	}
	
	/**
	 * Service.
	 *
	 * @param pRequest DynamoHttpServletRequest.
	 * @param pResponse DynamoHttpServletResponse.
	 * @throws ServletException ServletException.
	 * @throws IOException IOException.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCSRSuggestedMfrAgeDroplet  method: service]");
		}
		
		String skuId = (String) pRequest.getLocalParameter(TRUCSCConstants.SKU_ID);
		RepositoryItem skuItem = null;
		if(skuId != null){
			try {
				skuItem = getCatalogTools().findSKU(skuId);
			} catch (RepositoryException e) {
				pRequest.serviceLocalParameter(TRUConstants.EMPTY, pRequest, pResponse);
				if(isLoggingError()){
					logError("Exception while finding sku from CatalogTools for the skuId - "+skuId, e);
				}
 			}
 			if (skuItem != null) {
 				String mfgAgeMessage = getCatalogTools().getMinAndMaxMFGAgeMessageBySku(skuItem);
 				if(mfgAgeMessage != null){
 					if(isLoggingDebug()){
 						logDebug("Suggested Mfg Age : "+mfgAgeMessage);
 					}
 					pRequest.setParameter(TRUCSCConstants.MFR_SUGGESTED_AGE, mfgAgeMessage);
 					pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
 				}else{
 					pRequest.serviceLocalParameter(TRUConstants.EMPTY, pRequest, pResponse);
 				}
			}else{
				if(isLoggingDebug()){
					logDebug("No SKU item with id: "+skuId);
				}
				pRequest.serviceLocalParameter(TRUConstants.EMPTY, pRequest, pResponse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCSRSuggestedMfrAgeDroplet  method: service]");
		}
	}
}
