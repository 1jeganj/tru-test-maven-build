package com.tru.commerce.csr.droplet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.csr.catalog.TRUCSRCatalogTools;
import com.tru.commerce.csr.util.TRUCSCConstants;

/**
 * The Class TRUCSRValidatePreSellableDroplet.
 */
public class TRUCSRValidatePreSellableDroplet extends DynamoServlet{
	
	
	
	/** The m catalog tools. */
	private TRUCSRCatalogTools mCatalogTools;
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the catalog tools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}

	
		/**
		 * This method is used to get the current order as input and iterates all the commerce items present in the current
		 * order and send the items in list of shopping cart vo's.
		 * 
		 * @param pRequest
		 *            the request
		 * @param pResponse
		 *            the response
		 * @throws ServletException
		 *             the servlet exception
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into method TRUCSRValidatePreSellableDroplet.service :: START");
		}
		Object sku = pRequest.getObjectParameter(TRUCSCConstants.SKU);
		if (null != sku) {
			Date streetDate = getCatalogTools().getSkuItem(sku);
			boolean preSellable = Boolean.FALSE;
			SimpleDateFormat sdf = new SimpleDateFormat(
					TRUCommerceConstants.DATE_FORMAT);
			Date currentDate = new Date();
			String formattedDate = null;
			if (streetDate != null && currentDate.before(streetDate)) {
				formattedDate = sdf.format(streetDate);
				preSellable = Boolean.TRUE;
			}
			pRequest.setParameter(TRUCSCConstants.PRE_SELLABLE, preSellable);
			pRequest.setParameter(TRUCSCConstants.FORMATED_DATE, formattedDate);
			pRequest.serviceLocalParameter(TRUCSCConstants.OUTPUT_OPARAM,
					pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting  method TRUCSRValidatePreSellableDroplet.service :: END");
		}
	}
		

}
