package com.tru.commerce.csr.inventory;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.inventory.InventoryException;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUStoreItemVO;


/**
 * This class extends ATG OOTB DynamoServlet and used
 * for managing the Inventory related operation.
 *
 * @version 1.0
 * @author Professional Access
 *
 *
 */
public class TRUCSRInventoryLookupDroplet  extends DynamoServlet {

/** The input parameter name for the product and skuId to check. */
	public static final ParameterName LOCATION_ID = ParameterName.getParameterName("locationId");
	public static final ParameterName LOCATION_ID_SUPPLIED = ParameterName.getParameterName("locationIdSupplied");

	/**
	 * Sku id parameter name.
	 */
	public static final ParameterName SKU_ID = ParameterName.getParameterName("skuId");

	/** The output parameter name for the availabilityDate to set. */
	public static final String AVAILABILITY_DATE = "availabilityDate";

	/** The oparam name rendered once if the item is a preorderable item. */
	public static final String OPARAM_OUTPUT_PREORDERABLE = "preorderable";

	/** The oparam name rendered once if the item is not preorderable and is in stock. */
	public static final String OPARAM_OUTPUT_AVAILABLE = "available";

	/** The oparam name rendered once if the item is not preorderable, is not in stock  and is backorderable. */
	public static final String OPARAM_OUTPUT_BACKORDERABLE = "backorderable";

	/** The oparam name rendered once if the item is none of the above. */
	public static final String OPARAM_OUTPUT_UNAVAILABLE = "unavailable";

	public static final String OPARAM_OUTPUT = "output";

	/** The oparam name rendered once if the provided skuId can not be looked up in the inventory repository. */
	public static final String OPARAM_OUTPUT_ERROR = "error";

	/** The oparam name rendered once if none of the above open parameters exists. */
	public static final String OPARAM_OUTPUT_DEFAULT = "default";

	/**
	 * Inventory manager.
	 */
	private TRUCoherenceInventoryManager mInventoryManager;

	/** Property to hold storeAvailableMessage. */
	private String mStoreAvailableMessage;

	/** Property to hold storeNotAvailableMessage. */
	private String mStoreNotAvailableMessage;

	/** currentDate. */
	private CurrentDate mCurrentDate;

	/** Holds the mLocationTools. */
	private TRUStoreLocatorTools mLocationTools;

	/**
	 * Gets the store not available message.
	 *
	 * @return the storeNotAvailableMessage
	 */
	public String getStoreNotAvailableMessage() {
		return mStoreNotAvailableMessage;
	}

	/**
	 * Sets the store not available message.
	 *
	 * @param pStoreNotAvailableMessage the storeNotAvailableMessage to set
	 */
	public void setStoreNotAvailableMessage(String pStoreNotAvailableMessage) {
		mStoreNotAvailableMessage = pStoreNotAvailableMessage;
	}

	/**
	 * Gets the store available message.
	 *
	 * @return the storeAvailableMessage
	 */
	public String getStoreAvailableMessage() {
		return mStoreAvailableMessage;
	}

	/**
	 * Sets the store available message.
	 *
	 * @param pStoreAvailableMessage the storeAvailableMessage to set
	 */
	public void setStoreAvailableMessage(String pStoreAvailableMessage) {
		mStoreAvailableMessage = pStoreAvailableMessage;
	}

	/**
	 * Gets the inventory manager.
	 *
	 * @return the inventoryManager.
	 */
	public TRUCoherenceInventoryManager getInventoryManager() {
		return mInventoryManager;
	}

	/**
	 * Sets the inventory manager.
	 *
	 * @param pInventoryManager - the inventoryManager to set.
	 */
	public void setInventoryManager(TRUCoherenceInventoryManager pInventoryManager) {
		mInventoryManager = pInventoryManager;
	}

	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools
	 */
	public TRUStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set
	 */
	public void setLocationTools(TRUStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}


	/**
	 * Sets the current date.
	 *
	 * @param pCurrentDate Sets the CurrentDate component.
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * Gets the current date.
	 *
	 * @return currentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}


	/**
	 * Determines if the item is preorderable.
	 * @param pRequest - http request
	 * @param pResponse - http response
	 * @throws ServletException if an error occurs
	 * @throws IOException if an error occurs
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		Object locationIdParam = pRequest.getLocalParameter(LOCATION_ID);
		Object skuIdParam = pRequest.getLocalParameter(SKU_ID);
		Object locationIdSupplied = pRequest.getLocalParameter(LOCATION_ID_SUPPLIED);
		Object quantityRequested = pRequest.getLocalParameter(TRUCSCConstants.QUANTITY);
		
		// Check for valid input
		if (locationIdSupplied != null
				&& TRUCommerceConstants.TRUE.equals(locationIdSupplied.toString())
				&& locationIdParam == null) {
			if (isLoggingDebug()) {
				logDebug("MISSING PARAM: no location id  supplied");
			}
			return;
		} else if (locationIdSupplied != null 
				   && TRUCommerceConstants.TRUE.equals(locationIdSupplied.toString())
				   && !(locationIdParam instanceof String)) {
			if (isLoggingDebug()) {
				logDebug("INCORRECT PARAM: location id argument not a string");
			}
			return;
		}

		if (skuIdParam == null) {
			if (isLoggingDebug()) {
				logDebug("MISSING PARAM: no sku id supplied");
			}
			return;
		} else if (!(skuIdParam instanceof String)) {
			if (isLoggingDebug()) {
				logDebug("INCORRECT PARAM: sku id argument is not a string");
			}
			return;
		}

		TRUCoherenceInventoryManager invManager = getInventoryManager();
		final TRUStoreItemVO storeItemVO = new TRUStoreItemVO();
		TRUStoreLocatorTools locatorTools = (TRUStoreLocatorTools)getLocationTools();
		RepositoryItem storeItem = null;

		// Call InventoryManager to do all the work and convert results into correctly rendered output
		try {

			int availability;
			long availableStock;
			double availableStockAtStore = TRUConstants.DOUBLE_ZERO;
			storeItemVO.setSkuId((String)skuIdParam);
			if(locationIdSupplied != null && TRUCSCConstants.CONSTANT_TRUE.equals(locationIdSupplied.toString())){
				availability = invManager.queryAvailabilityStatus((String) skuIdParam, (String) locationIdParam);
				if(availability == TRUCommerceConstants.INT_IN_STOCK){
					storeItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
				}
			}else{
				availability = invManager.queryAvailabilityStatus((String) skuIdParam);
			}
			if(locationIdSupplied != null && TRUCSCConstants.CONSTANT_TRUE.equals(locationIdSupplied.toString())){
				storeItem = locatorTools.getStoreItem(locationIdParam.toString());
				availableStock = invManager.queryStockLevel(skuIdParam.toString(), locationIdParam.toString());
				availableStockAtStore = availableStock;
			}else{
				availableStock = invManager.queryStockLevel(skuIdParam.toString());
			}

			if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK && locationIdSupplied != null && 
					TRUCSCConstants.CONSTANT_TRUE.equals(locationIdSupplied.toString())){
				if(locationIdParam != null){
					storeItem = locatorTools.getStoreItem(locationIdParam.toString());
					if(storeItem != null){
						Double storeItemWareHouseLocationId = (Double)storeItem.getPropertyValue(
								locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
						availability=invManager.queryAvailabilityStatus(skuIdParam.toString(),Long.toString(Math.round(storeItemWareHouseLocationId)));
						availableStock=invManager.queryStockLevel(skuIdParam.toString(),Long.toString(Math.round(storeItemWareHouseLocationId)));
						if(availability == TRUCommerceConstants.INT_IN_STOCK){
							storeItemVO.setWareHouseInventoryFlag(true);
							 storeItemVO.setInventoryItemStatus(TRUCommerceConstants.IN_STOCK);
						}else{
							storeItemVO.setInventoryItemStatus(TRUCommerceConstants.OUT_OF_STOCK);
						}
					}
				}
			}
			if(locationIdSupplied != null && 
					TRUCSCConstants.CONSTANT_TRUE.equals(locationIdSupplied.toString()) && locationIdParam != null){
				if(quantityRequested != null){
					locatorTools.setStocklevel(availableStockAtStore);
					locatorTools.setItemQuantity(quantityRequested.toString());
				}
				String storeHoursMsg = locatorTools.getStoreHoursMsg(storeItem, storeItemVO);
				pRequest.setParameter(TRUCSCConstants.STORE_HOURS_MSG, storeHoursMsg);
			}
			
			pRequest.setParameter(TRUCSCConstants.STOCK_LEVEL, availableStock);
			pRequest.setParameter(TRUCSCConstants.AVAILABILITY, availability);

			pRequest.serviceLocalParameter(OPARAM_OUTPUT, pRequest, pResponse);
		} catch (InventoryException iexc) {
			if (isLoggingError()) {
				logError("InventoryException in TRUCSRInventoryLookupDroplet.service", iexc);
			}
			pRequest.serviceLocalParameter(OPARAM_OUTPUT_ERROR, pRequest, pResponse);
		}
	}
	
	
	
	/**

	   * This method is used to add the unAvailMsg to the Request object.
	   * @param pLocationId - The storeId.
	   * @param pRequest - The input request object.
	   */
	  public void setUnAvailMsg(String pLocationId,DynamoHttpServletRequest pRequest){
		  if(isLoggingDebug()) {
			logDebug("Start : TRUCSRInventoryLookupDroplet :setUnAvailMsg(String pLocationId,DynamoHttpServletRequest pRequest)");
		  }
		  final TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getLocationTools());
		  if(!StringUtils.isBlank(pLocationId)){
	      	  
			//Get the store item
			  final RepositoryItem storeRepoItem = locatorTools.getStoreItem(pLocationId);
		      final String address1 = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
		    		  getAddress1PropertyName());
		      	
		      final String chainCode = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
		    		  getChainCodePropertyName());
		      if(StringUtils.isNotBlank(chainCode)){
		    	  final String chianName = locatorTools.getToysMap().get(chainCode.trim());
			      final StringBuilder builder = new StringBuilder();
			      builder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
		       	//Prepare the store availability message
	      	if(!StringUtils.isBlank(address1)){
	      		final String storeAvailMsg = StringUtils.replace(getStoreNotAvailableMessage(), TRUCheckoutConstants.ZERO_BRACES, 
	      				builder.toString());
	      		pRequest.setParameter(TRUCheckoutConstants.STORE_AVAI_MSG, storeAvailMsg);
	      		pRequest.setParameter( TRUCheckoutConstants.ADDRESS_WITHCHAINNAME, builder);
	      	}
	      }
		  }
		  if(isLoggingDebug()) {
			logDebug("End : TRUCSRInventoryLookupDroplet :setUnAvailMsg(String pLocationId,DynamoHttpServletRequest pRequest)");
		  }
	  }
	  
	  /**
	   * Sets the avail msg.
	   *
	   * @param pLocationId - The storeId.
	   * @param pRequest - The input request object.
	   */
	  public void setAvailMsg(String pLocationId,DynamoHttpServletRequest pRequest){
		  if(isLoggingDebug()) {
			logDebug("Start : TRUStoreAvailabilityDroplet :setAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
		  }
		  final TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getLocationTools());
		  
		  if(!StringUtils.isBlank(pLocationId)){
	      	//Get the store item
	      	final RepositoryItem storeRepoItem = locatorTools.getStoreItem(pLocationId);
	      	final String address1 = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
	      			getAddress1PropertyName());
	      	
	      	final String chainCode = (String)locatorTools.getPropertyValue(storeRepoItem,locatorTools.getLocationPropertyManager().
	      			getChainCodePropertyName());
	      	 if(StringUtils.isNotBlank(chainCode)){
	      	final String chianName = locatorTools.getToysMap().get(chainCode.trim());
	      	final StringBuilder builder = new StringBuilder();
	      	builder.append(chianName).append(TRUCheckoutConstants.COMMA_SEPERATOR).append(address1);
	      	
	      	//Prepare the store availability message
	      	if(!StringUtils.isBlank(address1)){
	      		final String storeAvailMsg = StringUtils.replace(getStoreAvailableMessage(), TRUCheckoutConstants.ZERO_BRACES, builder.toString());
	      		pRequest.setParameter(TRUCheckoutConstants.STORE_AVAI_MSG, storeAvailMsg);
	      		pRequest.setParameter(TRUCheckoutConstants.ADDRESS_WITHCHAINNAME, builder);
	      	}
	      }
		  }
		  if(isLoggingDebug()) {
			logDebug("End : TRUStoreAvailabilityDroplet :setAvailMsg(String pIsReqFromAjax,String pLocationId,DynamoHttpServletRequest pRequest)");
		  }
	  }

}
