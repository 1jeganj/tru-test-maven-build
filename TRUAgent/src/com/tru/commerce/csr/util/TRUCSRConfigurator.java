package com.tru.commerce.csr.util;

import atg.commerce.csr.util.CSRConfigurator;

/**
 * This class extends ATG OOTB CSRConfigurator and used maintain global
 * configuration values for the Commerce Service Center application. 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRConfigurator extends CSRConfigurator{
	
	private String mMilesToLocateStores;
	 
	/**
	 * Property to hold the mTruContextRoot
	 */
	private String mTruContextRoot;

	/**
	 * Property to hold the mTruContextRoot
	 */
	private String mTruShippingMethodAvailable;
	

	/**
	 * @return the truShippingMethodAvailable
	 */
	public String getTruShippingMethodAvailable() {
		return mTruShippingMethodAvailable;
	}

	/**
	 * @param pTruShippingMethodAvailable the truShippingMethodAvailable to set
	 */
	public void setTruShippingMethodAvailable(String pTruShippingMethodAvailable) {
		this.mTruShippingMethodAvailable = pTruShippingMethodAvailable;
	}

	/**
	 * @return the mTruContextRoot
	 */
	public String getTruContextRoot() {

		return mTruContextRoot;
	}

	/**
	 * @param pTruContextRoot
	 *            the truContextRoot to set
	 */
	public void setTruContextRoot(String pTruContextRoot) {

		mTruContextRoot = pTruContextRoot;
	}

	/**
	 * @return mMilesToLocateStores
	 */
	public String getMilesToLocateStores() {
		return mMilesToLocateStores;
	}


	/**
	 * @param pMilesToLocateStores
	 *            the milesToLocateStores to set
	 */
	public void setMilesToLocateStores(String pMilesToLocateStores) {
		this.mMilesToLocateStores = pMilesToLocateStores;
	}
}