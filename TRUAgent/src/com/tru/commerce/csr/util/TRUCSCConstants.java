package com.tru.commerce.csr.util;

import atg.nucleus.naming.ParameterName;


/**
 * This class hold all csc related constants.
 * @author Professional Access
 * @version 1.0
 */

public class TRUCSCConstants {
	
	/** Holds the pipe delimiter. */
	public static final String PIPE_DELIMETRE = "\\|";
	
	/**
	 * Constant for NICK_NAME
	 */
	public static final String NICK_NAME = "nickNameProperty";
	
	/** The Constant SKU_ID. */
	public static final String SKU_ID = "skuId";
	
	 /** The Constant PICK_UP_STORE. */
 	public static final String PICK_UP_STORE = "pickupStore";
	/** The Constant CATALOG_REF_ID. */
	public static final String CATALOG_REF_ID="catalogRefId";
	
	/** The Constant QTY. */
	public static final String QTY="quantity";
	
	/** The Constant ADD_ITEM_COUNT. */
	public static final String ADD_ITEM_COUNT="addItemCount";
	
	
	/** The Constant PDP. */
	public static final String PDP="pdp";
	/**
	 * Constant for IS_GIFT_RECEIPT
	 */
	public static final String IS_GIFT_RECEIPT = "isGiftReceipt";
	
	/**
	 * Constant for IS_GIFT_RECEIPT
	 */
	public static final String GIFT_WRAP_MESSAGE = "GiftWrapMessage";
	
	/**
	 * Constant for Email for AddressInputFields while adding CC
	 */
	public static final String ADDRESS_INPUT_EMAIL = "abc@gmail.com";
	
	
	/** The Constant NUMBER_TEN. */
	
	public static final int NUMBER_TEN = 10;
	
	
	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT="dd/MM/yyyy";
	
	/** The Constant FORMATED_DATE. */
	public static final String FORMATED_DATE="formattedDate";
	
	/** The Constant SKU. */
	public static final String SKU="sku";
	
	/** Constant to hold output. */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	
	/** The Constant PRE_SELLABLE. */
	public static final String PRE_SELLABLE="preSellable";

	/**
	 * Constant for DOUBLE_ZERO_FORMAT.
	 */
	public static final double DOUBLE_ZERO_FORMAT = 0.0D;
	
	/**
	 * Constant for DOUBLE_ZERO.
	 */
	public static final double DOUBLE_ZERO = 0.0;
	
	/**
	 * property to hold PAYMENT_RELATIONTYPE_PAYMENTAMOUNTREMAINING property.
	 */ 
	public static final int PAYMENTAMOUNTREMAINING = 203;
	
	/**
	 * property to hold PAYMENT_RELATIONTYPE_SHIPPINGAMOUNTREMAINING property.
	 */ 
	public static final int SHIPPINGAMOUNTREMAINING = 302;
	
	/**
	 * property to hold PAYMENT_RELATIONTYPE_ORDERAMOUNTREMAINING property.
	 */ 
	public static final int ORDERAMOUNTREMAINING = 404;
	
	/**
	 * property to hold PAYMENT_RELATIONTYPE_TAXAMOUNTREMAINING property.
	 */ 
	public static final int TAXAMOUNTREMAINING = 405;
	
	/**
	 * property to hold STRING_Y property.
	 */ 
	public static final String STRING_Y = "Y";
	
	/**
	 * property to hold STRING_Y property.
	 */ 
	public static final String STRING_YES = "Yes";
	
	/**
	 * property to hold NUMBER_THREE property.
	 */ 
	public static final int NUMBER_THREE = 3;
	
	/**
	 * property to hold NUMNER_ZERO property.
	 */ 
	public static final int NUMNER_ZERO = 0;
	
	/**
	 * property to hold NUMBER_ONE property.
	 */ 
	public static final int NUMBER_ONE = 1;
	
	/**
	 * property to hold NUMBER_ONE property.
	 */ 
	public static final String NUMBER_FOUR = "4";
	
	/**
	 * property to hold CURRENT_TICKET_HOLDER_IS_NULL property.
	 */ 
	public static final String CURRENT_TICKET_HOLDER_IS_NULL = "currentTicket in TicketHolder is null";
	
	/**
	 * property to hold UNABLE_TO_RESET_PASSWORD property.
	 */ 
	public static final String UNABLE_TO_RESET_PASSWORD = "unable to reset password";

	/**
	 * property to hold PENDING_MERCHANT_ACTION property.
	 */ 
	public static final String PENDING_MERCHANT_ACTION = "pending_merchant_action";
	
	/**
	 * property to hold PROCESSING property.
	 */ 
	public static final String PROCESSING = "processing";
	
	/**
	 * property to hold AUTHORIZE_FAILED property.
	 */ 
	public static final String AUTHORIZE_FAILED = "authorize_failed";
	
	/**
	 * property to hold SETTLE_FAILED property.
	 */ 
	public static final String SETTLE_FAILED = "settle_failed";
	
	/**
	 * property to hold INITIAL property.
	 */ 
	public static final String INITIAL = "initial";
	
	/**
	 * property to hold IN_STORE_PAYMENT property.
	 */ 
	public static final String IN_STORE_PAYMENT = "inStorePayment";
	
	/**
	 * property to hold COMMIT_ORDER property.
	 */ 
	public static final String COMMIT_ORDER = "commitOrder";
	
	/**
	 * property to hold STRING_UNDERSCORE property.
	 */ 
	public static final String STRING_UNDERSCORE = "-";
	
	/**
	 * property to hold NOT_FOUND_STRING property.
	 */ 
	public static final int NOT_FOUND_NUMBER = 404;
	
	/**
	 * property to hold ERROR_UPDATING_ORDER_AFTER_ADDING_CREDIT_CARD property.
	 */ 
	public static final String ERROR_UPDATING_ORDER_AFTER_ADDING_CREDIT_CARD = "errorUpdatingOrderAfterAddingCreditCard";
	
	/**
	 * property to hold success.
	 */
	public static final String SUCCESS = "success";

	public static final String HANDLERESET = "TRUCSRProfileFormHandler.handleResetPassword";
	
	public static final String HANDLECONFIRMATIONMAIL = "TRUCSRCommitOrderFormHandler.handleSendConfirmationMessage";

	/**
	 * property to hold hardgoodShippingGroup.
	 */
	public static final String HARDGOOD_SHIPPING_GROUP = "hardgoodShippingGroup";


	/**
	 * property to hold errorValidateShippingGroup.
	 */
	public static final String ERROR_VALIDATE_SHIPPING_GROUP = "errorValidateShippingGroup";

	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String COULD_NOT_FIND_SHIPPING_GROUP_OR_ADDRESS2 = "couldNotFindShippingGroupOrAddress";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String PAYMENT_TAB = "payment-tab";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String SHIPPING_PRICE = "shippingPrice";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String REGION_CODE = "regionCode";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String SHIP_METHOD_CODE = "shipMethodCode";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String ERROR_APPLYING_SHIPPING_GROUP = "errorApplyingShippingGroup";
	
	/**
	 * property to hold couldNotFindShippingGroupOrAddress.
	 */
	public static final String ERROR_WHILE_PERSISTING_ORDER2 = "errorWhilePersistingOrder";
	/**
	 * property to hold TWO_NOT_THREE property.
	 */ 
	public static final int TWO_NOT_THREE = 203;
	/**
	 * property to hold THREE_NOT_TWO property.
	 */ 
	public static final int THREE_NOT_TWO = 302;
	/**
	 * property to hold FOUR_NOT_FIVE property.
	 */ 
	public static final int FOUR_NOT_FIVE = 405;
	/**
	 * property to hold TWO property.
	 */ 
	public static final int TWO = 2;

	/**
	 * property to hold DEFAULT.
	 */
	public static final String DEFAULT = "default";
	/**
	 * CONSTANT TO HOLD STOCK_LEVEL.
	 */
	public static final String STOCK_LEVEL = "stockLevel";
	
	/**
	 * CONSTANT TO HOLD AVAILABILITY.
	 */
	public static final String AVAILABILITY = "availability";
	
	/** The Constant PLEASE_TRY_AGAIN. */
	public static final String PLEASE_TRY_AGAIN = "Please Try again";
	
	/** The Constant NOT_ELIGIBLE_FOR_INSPU. */
	public static final String NOT_ELIGIBLE_FOR_INSPU = "Some items in the cart are not eligible for store pickup. Please proceed with multiple shipping";
	
	/** The Constant ITEM_NOT_ELIGIBLE_FOR_INSPU. */
	public static final String ITEM_NOT_ELIGIBLE_FOR_INSPU = "Some items in the cart are not eligible for store pickup. Please proceed with multiple shipping";
	
	/** The Constant ITEM_STRING. */
	public static final String ITEM_STRING = "The item ";
	
	/** The Constant ITEM_NOT_ELIGIBLEFOR_ISPU_CHOOSE_DIFF_ADDR. */
	public static final String ITEM_NOT_ELIGIBLEFOR_ISPU_CHOOSE_DIFF_ADDR = " is not eligible for store pickup. Please choose different address. ";
	
	/** The Constant ITEM_INVENTORY_NOT_AVAIL_IN_STORE. */
	public static final String ITEM_INVENTORY_NOT_AVAIL_IN_STORE = "Inventory not available for the item ";
	
	/** The Constant AT_SELECTED_LOC. */
	public static final String AT_SELECTED_LOC = " at the selected location : ";
	
	/** The Constant AT_SELECTED_LOC. */
	public static final long LONG_MINUS_ONE = -1;
	
	/** The Constant CONSTANT_TRUE. */
	public static final String CONSTANT_TRUE = "true";
	
	/** The Constant TAXWARE_ADDRESS_INVALID_ERROR_MESSAGE. */
	public static final String TAXWARE_ADDRESS_INVALID_ERROR_MESSAGE = "Oops! Seems there is an issue with address. Please correct.";
	
	/** The Constant DONATION_COMMERCE_ITEM. */
	public static final String DONATION_COMMERCE_ITEM = "donationCommerceItem";
	
	/** The Constant GIFTWRAP_COMMERCE_ITEM. */
	public static final String GIFTWRAP_COMMERCE_ITEM ="giftWrapCommerceItem";
	
	/** The Constant MAX. */
	public static final String MAX = "max";
	
	/** The Constant MIN. */
	public static final String MIN = "min";
	
	/** The Constant comments. */
	public static final String COMMENTS = "comments";
	
	/** The Constant firstName. */
	public static final String FIRST_NAME = "firstName";
	
	/** The Constant lastName. */
	public static final String LAST_NAME = "lastName";
	
	/** The Constant PRODUCT_ID. */
	public static final String PRODUCT_ID = "productId";
	
	/** The Constant HAS_COLOR_VARIANT. */
	public static final String HAS_COLOR_VARIANT = "hasColorVariant";
	
	/** The Constant HAS_COLOR_VARIANT. */
	public static final String HAS_SIZE_VARIANT = "hasSizeVariant";
	
	/** The Constant CHILD_SKUS. */
	public static final String CHILD_SKUS = "childSkus";
	
	/** The Constant NON_MERCH_SKU. */
	public static final String NON_MERCH_SKU = "nonMerchSKU";
	
	/** The Constant PRODUCT_INFO. */
	public static final String PRODUCT_INFO = "productInfo";
	
	/** The Constant SKU_VO. */
	public static final String SKU_VO = "skuVo";
	
	/** The Constant SITE. */
	public static final String SITE = "site";
	/** The Constant SHIPPING_ERROR. */
	public static final String SHIPPING_ERROR = "Due to local ordinances,We are unable to complete current order.";
			
	/** The Constant SHIPPING_RESTRICTON. */
	public static final String SHIPPING_RESTRICTON = "Please remove the restricted item(s) from your cart to proceed with checkout process.";
			
	/** The Constant CART_LINK. */
	public static final String CART_LINK = "Click on shoppingCart link return to your cart.";
	
	/** The Constant SHIPMENT. */
	public static final String SHIPMENT = "shipment to";
	
	/** The Constant MFR_SUGGESTED_AGE. */
	public static final String MFR_SUGGESTED_AGE = "mfrSuggestedAge";
	
	/** The Constant PROMOTION_ID. */
	public static final String PROMOTION_ID = "promotionId";
	
	/** The Constant PROMOTION_DETAILS. */
	public static final String PROMOTION_DETAILS = "promotionDetails";
	/** The Constant GWP_ERROR_QUALIFY. */
	public static final String GWP_ERROR_QUALIFY = "gwpErrorQualifyingGift";
	/** The Constant ATG_PROMO_RESOURCE. */
	public static final String ATG_PROMO_RESOURCE = "atg.commerce.promotion.PromotionResources";
	/** The Constant DISABLE_GWP. */
	public static final String DISABLE_GWP = "disableAutoGWP";
	/** The Constant GWP_PROMO_ID. */
	public static final String GWP_PROMO_ID = "gwpPromotionId";
	/** The Constant PRICING_CONTEXT. */
	public static final String PRICING_CONTEXT = "pricingContext";
	/** The Constant GIFT_DETAIL. */
	public static final String GIFT_DETAIL = "giftDetail";
	/** The Constant GIFT_TYPE. */
	public static final String GIFT_TYPE = "giftType";
	/** The Constant AUTO_REMOVE. */
	public static final String AUTO_REMOVE = "autoRemove";
	/** The Constant ZERO_LONG. */
	public static final long ZERO_LONG = 0L;
	/** The Constant SITE_ID. */
	public static final String SITE_ID = "siteId";
	/** The Constant SITE_URL. */
	public static final String SITE_URL = "siteURL";
	/** The Constant STORE_HOURS. */
	public static final String STORE_HOURS = "storeHours";
	/** The Constant STORE_ITEM. */
	public static final String STORE_ITEM = "storeItem";
	/** The Constant STORE_ITEM. */
	public static final String STORE_HOURS_MSG = "storeHoursMsg";
	/** The Constant STORE_HOURS_MSG_MAP. */
	public static final String STORE_HOURS_MSG_MAP = "storeHoursMsgMap";
	/** The Constant REPLACE_HTML_STRING. */
	public static final String REPLACE_HTML_STRING = "\\<.*?\\>";
	/** The Constant QUANTITY. */
	public static final String QUANTITY = "quantity";
	
	/** The Constant ERROR_MESSAGE. */
	public static final String ERROR_MESSAGE ="errorMessage_";
	
	/** The Constant ITEM_OUT_OF_STOCK. */
	public static final String ITEM_OUT_OF_STOCK = "The following item went out of stock :";
	/** The Constant ALL_ITEMS_OUT_OF_STOCK. */
	public static final String ALL_ITEMS_OUT_OF_STOCK = "All item(s) have gone Out of Stock. Please add item(s) to cart";
	
	/** The Constant FEW_ITEMS_OUT_OF_STOCK_MSG. */
	public static final String FEW_ITEMS_OUT_OF_STOCK_REDIRECT_MSG = "One or more item(s) have gone Out of Stock. You can click on submit order to complete order. Out of Stock Items has been removed from the order";
	
	/** The Constant FEW_ITEMS_OUT_OF_STOCK_CART_MSG. */
	public static final String FEW_ITEMS_OUT_OF_STOCK_REDIRECT_CART_MSG = "One or more item(s) have gone Out of Stock. You can continue checkout. Out of Stock Items will be removed from the order";
	
	/** The Constant FEW_ITEMS_OUT_OF_STOCK_MSG. */
	public static final String ALL_OUT_OF_STOCK_REDIRECT_MSG = "Please Click on Shopping Cart link to continue adding items";
	
	/** The Constant ALL_OUT_OF_STOCK_REDIRECT_CART_MSG. */
	public static final String ALL_OUT_OF_STOCK_REDIRECT_CART_MSG = "Please add One or more item(s) to cart to proceed further";
	
	/** The Constant SOME_INFO_MISSING. */
	public static final String SOME_INFO_MISSING = "Oops. There was an issue. Oops! Some information is missing. You must fill in the required fields";
	
	/** The Constant SOME_INFO_MISSING_MULTI. */
	public static final String SOME_INFO_MISSING_MULTI = "Oops. There was an issue. Please change or add new address to see available ship methods.";
	
	/** The Constant CANT_SHIPTO_INTERNATIONAL_ADDRESS. */
	public static final String CANT_SHIPTO_INTERNATIONAL_ADDRESS = "Shipping to International Address is not Allowed";
	
	/** The Constant COUNTRY. */
	public static final String COUNTRY = "US";

	/** The Constant FOR_ITEM. */
	public static final String FOR_ITEM = " for item ";
	/** The Constant STATE_NA. */
	public static final String STATE_NA = "NA";
	
	/** The Constant PAY_IN_STORE_ERROR_MSG. */
	public static final String PAY_IN_STORE_ERROR_MSG ="Payment In Store is not eligible with In Store Pickup items in cart. Please add another payment to proceed further.";

	/** The Constant NULL. */
	public static final String NULL = "null";
	/** The Constant INVALID_LOGIN. */
	public static final String INVALID_LOGIN = "invalidLogin";
	/** The Constant MISSING_LOGIN. */
	public static final String MISSING_LOGIN = "missingLogin";
	/** The Constant ATG_SCENARIO_SCENARIO_RESOURCES. */
	public static final String ATG_SCENARIO_SCENARIO_RESOURCES = "atg.scenario.ScenarioResources";
	/** The Constant MISSING_LOGIN. */
	public static final String IS_TURE = "true";
	/** The Constant MISSING_LOGIN. */
	public static final String DPSLOGOUT = "DPSLogout";
	/** The Constant MISSING_LOGIN. */
	public static final String SSOLOGIN = "ssoLogin";
	/** The Constant MISSING_LOGIN. */
	public static final String USERNAME = "userName";
	/**
	 * Constant for REQUIRE_LOGIN_ENABLED
	 */
	public static final String REQUIRE_LOGIN_ENABLED = "require-login-enabled";
	/**
	 * Constant for REQUIRE_LOGIN_PROFILE_RESTRICTION
	 */
	public static final String REQUIRE_LOGIN_PROFILE_RESTRICTION = "A valid value must be specified for the profileComponentName init-param of the RequireLoginServlet.";
	/**
	 * Constant for REQUIRE_LOGIN_UI_RESTRICTION
	 */
	public static final String REQUIRE_LOGIN_UI_RESTRICTION = "A value must be specified for the UIConfigurationComponentName init-param of the RequireLoginServlet.";
	/**
	 * Constant for UI_CONFIGURATION_START
	 */
	public static final String UI_CONFIGURATION_START="The UIConfiguration instance specified by the UIConfigurationComponentName: \"";
	/**
	 * Constant for UI_CONFIGURATION_END
	 */
	public static final String UI_CONFIGURATION_END="\" could not be found.";
	/** The Constant QUESTIONMARK. */
	public static final String QUESTIONMARK = "?";
	/** The Constant STRING_AND. */
	public static final String STRING_AND = "&";
	/** The Constant SESSION_INVALID. */
	public static final String SESSION_INVALID = "sessioninvalid=";
	/** The Constant AND_USER_NAME. */
	public static final String AND_USER_NAME = "&userName=";
	/** The Constant AND_SSOLOGIN. */
	public static final String AND_SSOLOGIN = "&ssoLogin=";
	/** The Constant ISPPR. */
	public static final String ISPPR = "_isppr";
	/** The Constant AND_PPR. */
	public static final String AND_PPR = "&ppr=";
	/**
	 * property to hold SIX_FOUR property.
	 */ 
	public static final int SIX_FOUR = 64;
	/** The Constant IS_NEW_WINDOW_ID. */
	public static final String IS_NEW_WINDOW_ID = "isNewWindowIdFlag";
	/** The Constant AND_WINDOW_ID. */
	public static final String AND_WINDOW_ID = "&_windowid=";
	/** The Constant STRING_EQUAL. */
	public static final String STRING_EQUAL = "=";
	/** The Constant NON_LOCAL_LOGIN_START. */
	public static final String NON_LOCAL_LOGIN_START = "\nRedirecting to non-local login URI ";
	/** The Constant NON_LOCAL_LOGIN_END. */
	public static final String NON_LOCAL_LOGIN_END = "\n\n\n___________________________";
	/** The Constant AT_LOCATION. */
	public static final String AT_LOCATION = " at location ";
		/** The Constant LOCATION_ID. */
	public static final String LOCATION_ID = "locationId";
	/** The Constant ORDER_ID. */
	public static final String ORDER_ID = "orderId";
	/** The Constant STORE_ID. */
	public static final String STORE_ID = "storeId";
	/** The Constant HIPHON. */
	public static final String HIPHON = " - ";
	/** The Constant PLEASE_SELECT_OTHER_LOCATION. */
	public static final String PLEASE_SELECT_OTHER_LOCATION = " Please select other location";
	
	/** The Constant INVERTED_COMMA. */
	public static final String INVERTED_COMMA ="";
	
	
	/** The Constant HDN_USERNAME. */
	public static final String HDN_USERNAME ="hdnUserName";
	
	/** The Constant HDN_SECURITYLEVEl. */
	public static final String HDN_SECURITYLEVEl ="hdnSecurityLevel";
	
	/** The Constant ORDER_CONFIRM. */
	public static final String ORDER_CONFIRM ="orderConfirm";
	
	/** The Constant UNITED_STATES. */
	public static final String UNITED_STATES ="UNITED STATES";
	
	/** The Constant VALIDATE_CREDIT_CARD. */
	public static final String VALIDATE_CREDIT_CARD ="validateCreditCard()";
	
	/** The Constant ISLAND_TAX. */
	public static final String ISLAND_TAX ="islandTax";
	
	/** The Constant LOCAL_TAX. */
	public static final String LOCAL_TAX ="localTax";
	
	/** The Constant REVIEW. */
	public static final String REVIEW ="Review";
	
}
