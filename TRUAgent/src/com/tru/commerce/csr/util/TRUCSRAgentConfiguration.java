package com.tru.commerce.csr.util;

import atg.nucleus.GenericService;

/**
 * This class extends ATG OOTB GenericService and used maintain global
 * configuration values for the Commerce Service Center application.
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRAgentConfiguration extends GenericService{
	 	
	/**
	 * Property to hold the mContextPath.
	 */
	private String mContextPath;
	
	/**
	 * Property to hold the mServerName.
	 */
	private String mServerName;
	
	/**
	 * Property to hold the mServerPort.
	 */
	private int mServerPort;
	
	/**
	 * Property to hold the mHTTP.
	 */
	private String mHTTP;	

	/**
	 * @return the mContextPath
	 */
	public String getContextPath() {
		return mContextPath;
	}

	/**
	 * @param pContextPath
	 *            the contextPath to set
	 */
	public void setContextPath(String pContextPath) {
		mContextPath = pContextPath;
	}

	/**
	 * @return the mServerName
	 */
	public String getServerName() {
		return mServerName;
	}

	/**
	 * @param pServerName
	 *            the serverName to set
	 */
	public void setServerName(String pServerName) {
		mServerName = pServerName;
	}

	/**
	 * @return the mServerPort
	 */
	public int getServerPort() {
		return mServerPort;
	}
	
	/**
	 * @param pServerPort
	 *            the serverPort to set
	 */
	public void setServerPort(int pServerPort) {
		mServerPort = pServerPort;
	}

	/**
	 * @return the mHTTP
	 */
	public String getHTTP() {
		return mHTTP;
	}

	/**
	 * @param pHTTP
	 *            the HTTP to set
	 */
	public void setHTTP(String pHTTP) {
		mHTTP = pHTTP;
	}  	
}