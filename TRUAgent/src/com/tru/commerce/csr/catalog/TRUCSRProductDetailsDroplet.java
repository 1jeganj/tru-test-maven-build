package com.tru.commerce.csr.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.csr.util.TRUCSCConstants;


/**
 * @author PA
 * This will return the SKU details associated to a product.
 *
 */
public class TRUCSRProductDetailsDroplet extends DynamoServlet{
	
	/** The m catalog properties. */
	private TRUCSRCatalogTools mCatalogTools;
	
	
	/**
	 * @return mCatalogTools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}


	/**
	 * @param pCatalogTools to set mCatalogTools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}


	/**
	 * This will sets the details of child skus associated to a product.
	 * @param pReq - http request
	 * @param pRes - http response
	 * @throws ServletException if an error occurs
	 * @throws IOException if an error occurs
	 */
	@Override
	public void service(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
		boolean hasColorVariant = false;
		boolean hasSizeVariant = false;
		ProductInfoVO productInfo = new ProductInfoVO();
		final String productId = (String) pReq.getLocalParameter(TRUCSCConstants.PRODUCT_ID);
		RepositoryItem productItem = null;
		List<SKUInfoVO> childSkusList = new ArrayList<SKUInfoVO>();
		try {
			productItem = (RepositoryItem)getCatalogTools().findProduct(productId);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("ProductItem object is null for the given product id - " + productId, e);
			}
		}
		
		if(productItem != null){
			productInfo = getCatalogTools().createProductInfo(productItem, false, null);
			childSkusList = productInfo.getChildSKUsList();
			if(childSkusList != null && !childSkusList.isEmpty()){
				for(SKUInfoVO itemVO : childSkusList){
					boolean colorCheck = itemVO.isColorVariantExists();
					boolean sizeCheck = itemVO.isSizeVariantExists();
					if(colorCheck){
						hasColorVariant = true;
					}
					if(sizeCheck){
						hasSizeVariant = true;
					}
				}
			}
			pReq.setParameter(TRUCSCConstants.HAS_COLOR_VARIANT, hasColorVariant);
			pReq.setParameter(TRUCSCConstants.HAS_SIZE_VARIANT, hasSizeVariant);
			pReq.setParameter(TRUCSCConstants.PRODUCT_INFO, productInfo);
			pReq.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pReq, pRes);
		}else{
			pReq.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pReq, pRes);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
	}
}
