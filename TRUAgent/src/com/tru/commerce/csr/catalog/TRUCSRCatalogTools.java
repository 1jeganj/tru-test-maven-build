package com.tru.commerce.csr.catalog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.csr.catalog.vo.TRUCSRProductItemDetailsVO;

/**
 * This class extends TRUCatalogTools.
 * This class contains series of helper methods for accessing CustomCatalog.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUCSRCatalogTools extends TRUCatalogTools{
	
	/**
	 * Gets the sku item.
	 *
	 * @param pSkuItem the sku item
	 * @return the sku item
	 */
	public Date getSkuItem(Object pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItem method..");
		}
		RepositoryItem sku = (RepositoryItem) pSkuItem;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Date streetDate = (Date) sku.getPropertyValue(catalogProperties
				.getStreetDate());
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools.getSkuItem method..");
		}
		return streetDate;
	}
	
	/**
	 * Gets the sku item web display.
	 *
	 * @param pSkuItem the sku item
	 * @return the sku item web display
	 */
	public Boolean getSkuItemWebDisplay(Object pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItemWebDisplay method..");
		}
		RepositoryItem sku = (RepositoryItem) pSkuItem;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Boolean webdisplay = (Boolean) sku.getPropertyValue(catalogProperties
				.getWebDisplayFlag());
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools.getSkuItemWebDisplay method..");
		}
		return webdisplay;
	}
	
	
	/**
	 * Gets the sku item suppress search.
	 *
	 * @param pSkuItem the sku item
	 * @return the sku item suppress search
	 */
	public Boolean getSkuItemSuppressSearch(Object pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItemSuppressSearch method..");
		}
		RepositoryItem sku = (RepositoryItem) pSkuItem;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Boolean suppressSearch = (Boolean) sku.getPropertyValue(catalogProperties.getSupressInSearch());
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools.getSkuItemSuppressSearch method..");
		}
		return suppressSearch;
	}
	
		
	/**
	 * Gets the sku item deleted flag.
	 *
	 * @param pSkuItem the sku item
	 * @return the sku item deleted flag
	 */
	public Boolean getSkuItemDeletedFlag(Object pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItemDeletedFlag method..");
		}
		RepositoryItem sku = (RepositoryItem) pSkuItem;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		Boolean isDeleted = (Boolean) sku.getPropertyValue(catalogProperties.getDeletedPropertyName());
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools.getSkuItemDeletedFlag method..");
		}
		return isDeleted;
	}
	
		
	/**
	 * Gets the sku item type property.
	 *
	 * @param pSkuItem the sku item
	 * @return the sku item type property
	 */
	public String getSkuItemTypeProperty(Object pSkuItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItemTypeProperty method..");
		}
		RepositoryItem sku = (RepositoryItem) pSkuItem;
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String typeProperty = (String) sku.getPropertyValue(catalogProperties.getTypePropertyName());
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.getSkuItemTypeProperty method..");
		}
		return typeProperty;
	}

	/**
	 * This method used to create product info object which will be used to
	 * display the details in PDP page.
	 * 
	 * @param pProductItem
	 *            - Product item
	 * @param pChannelType - ChannelType
	 * @param pCatalogRefIds - CatalogRefIds
	 * @return ProductInfoVO - product info object
	 */
	@SuppressWarnings("unchecked")
	public ProductInfoVO createProductInfo(RepositoryItem pProductItem, boolean pChannelType, List<String> pCatalogRefIds) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUCSRCatalogTools.createProductInfo method..");
		}
		ProductInfoVO productInfo = new ProductInfoVO();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		if (pProductItem != null) {
			populateProductBasicInfo(pProductItem, productInfo);
			if (isLoggingDebug()) {
				vlogDebug(
						"Product Id : {0}  Type : {1}  isActive : {2}  isNavigable : {3}",
						productInfo.getProductId(),
						productInfo.getProductType(), productInfo.isActive());
			}
			List<RepositoryItem> childSKUs = (List<RepositoryItem>) pProductItem
					.getPropertyValue(catalogProperties
							.getChildSkusPropertyName());
			if (childSKUs != null && !childSKUs.isEmpty()) {
				SKUInfoVO skuVo = null;
				List<SKUInfoVO> childSkuList = new ArrayList<SKUInfoVO>();
				List<SKUInfoVO> inStockChildSkuList = new ArrayList<SKUInfoVO>();
				List<SKUInfoVO> outOfStockChildSkuList = new ArrayList<SKUInfoVO>();
				for (RepositoryItem childSKU : childSKUs) {
					skuVo = createSKUInfo(childSKU, pProductItem, pChannelType);
					skuVo.setSkuItem(childSKU);
					/*if (!TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE
							.equalsIgnoreCase(productInfo.getProductType())) {*/
						// This method will populate all the variants related
						// information
						populateVariantsInformation(skuVo,childSKU, pProductItem, childSkuList);
						
					//}
					String inventoryStatus = skuVo.getInventoryStatus();
					if (TRUCommerceConstants.IN_STOCK
							.equalsIgnoreCase(inventoryStatus)) {
						inStockChildSkuList.add(skuVo);
						productInfo
								.setProductStatus(TRUCommerceConstants.IN_STOCK);
					} else if (TRUCommerceConstants.OUT_OF_STOCK
							.equalsIgnoreCase(inventoryStatus)) {
						outOfStockChildSkuList.add(skuVo);
						productInfo
								.setProductStatus(TRUCommerceConstants.OUT_OF_STOCK);
					}
				}
				productInfo.setChildSKUsList(childSkuList);
				productInfo.setActiveSKUsList(inStockChildSkuList);
				productInfo.setInActiveSKUsList(outOfStockChildSkuList);
				
				productInfo.setActive(Boolean.TRUE);
				
			} else {
				// If SKU is not active system should not display PDP page
				productInfo.setActive(Boolean.FALSE);
			}
		} else {
			// If SKU is not active system should not display PDP page
			productInfo.setActive(Boolean.FALSE);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUCSRCatalogTools.createProductInfo method.. , productInfo::"
					+ productInfo);
		}
		return productInfo;
	}
	/**
	 * This method will populate all the variants related information.
	 * 
	 * 
	 * @param pSkuVo
	 *            - pSkuVo
	 * @param pChildSku
	 *            - pChildSku
	 * @param pChildSkuList
	 *            - pChildSkuList
	 * @param pProductItem
	 *            - ProductItem
	 */
	public void populateVariantsInformation(SKUInfoVO pSkuVo, RepositoryItem pChildSku, RepositoryItem pProductItem, List<SKUInfoVO> pChildSkuList) {
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String colorCode = null;
		String colorSequence=null;
		String sizeSequence=null;
		String juvenileSize = null;
		String juvenileSizeDesc = null;
		Integer colorSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer sizeSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer count = TRUCommerceConstants.INT_FIVE_THOUSAND;
		RepositoryItem color=(RepositoryItem) pChildSku.getPropertyValue(catalogProperties.getSkuColorCode());
		if(color!=null){
			colorCode=(String) color.getPropertyValue(catalogProperties.getColorName());
			colorSequence=(String) color.getPropertyValue(catalogProperties.getColorDisplaySequence());
		}
		
		RepositoryItem size = (RepositoryItem)pChildSku.getPropertyValue(catalogProperties.getSkuJuvenileSize());
		if(size!=null){
			juvenileSize=(String) size.getPropertyValue(catalogProperties.getSizeDescription());
			sizeSequence=(String) size.getPropertyValue(catalogProperties.getSizeDisplaySequence());
			if(size.getPropertyValue(catalogProperties.getDescription())!=null){
				juvenileSizeDesc=(String) size.getPropertyValue(catalogProperties.getDescription());
			}
		}
		if (colorCode != null) {
			colorCode = colorCode.toLowerCase();
			pSkuVo.setColorCode(colorCode);
			if(colorSequence!=null){
				colorSequenceNumber=Integer.parseInt(colorSequence);
			}else{
				colorSequenceNumber=count;
				count++;
			}
			pSkuVo.setColorSequenceNumber(colorSequenceNumber);
		}

		if (juvenileSizeDesc != null) {
			juvenileSizeDesc = juvenileSizeDesc.toUpperCase();
			pSkuVo.setJuninileSizeDesc(juvenileSizeDesc);
			if (sizeSequence!=null) {
				sizeSequenceNumber=Integer.parseInt(sizeSequence);
			} else {
				sizeSequenceNumber=count;
				count++;
			}
		}else if(juvenileSize!=null){
			juvenileSize = juvenileSize.toUpperCase();
			pSkuVo.setJuninileSizeDesc(juvenileSize);
			if (sizeSequence!=null) {
				sizeSequenceNumber=Integer.parseInt(sizeSequence);
			} else {
				sizeSequenceNumber=count;
				count++;
			}
		}
		pSkuVo.setSizeSequenceNumber(sizeSequenceNumber);
		if(!pChildSkuList.isEmpty()){
			addItemBySortingVariants(pChildSkuList, pSkuVo); 
		}else{
			if(pSkuVo.getColorSequenceNumber() != 0){
				pSkuVo.setColorVariantExists(true);
			}
			if(pSkuVo.getSizeSequenceNumber() != 0){
				pSkuVo.setSizeVariantExists(true);
			}
			pChildSkuList.add(pSkuVo);
		}
	}
	
	/**
	 * This method will sort the list and add the current item.
	 * @param pSortedChildSkuList for sortedChildSkuList
	 * @param pItemVO for itemVO
	 * @return List<TRUCSRProductItemDetailsVO>
	 */
	private List<SKUInfoVO> addItemBySortingVariants(List<SKUInfoVO> pSortedChildSkuList, SKUInfoVO pItemVO){
		
		if(isLoggingDebug()){
			logDebug("START :: TRUCSRCatalogTools class : addItemBySortingVariants method ");
		}
		
		int currentItemColorSeq = pItemVO.getColorSequenceNumber();
		int currentItemSizeSeq = pItemVO.getSizeSequenceNumber();
		int counter = 0;
		boolean addedFlag = false;
		for(SKUInfoVO itemDetail : pSortedChildSkuList){
			int colorSequence = itemDetail.getColorSequenceNumber();
			int sizeSequence = itemDetail.getSizeSequenceNumber();
			// This will check if the current color sequence is lesser than the list from sortedList
			if(currentItemColorSeq != 0){
				pItemVO.setColorVariantExists(true);
				if(currentItemColorSeq < colorSequence){
					pSortedChildSkuList.add(counter, pItemVO);
					addedFlag = true;
					break;
				}else if((currentItemColorSeq != 0 && currentItemColorSeq == colorSequence)){
					pItemVO.setSizeVariantExists(true);
					if(currentItemSizeSeq < sizeSequence){
						pSortedChildSkuList.add(counter, pItemVO);
					addedFlag = true;
					break;
					}
				}//This will check if the current color sequence is equal to the list from sortedList. If yes, then this will check for size and sort accordingly.
			}else if(currentItemSizeSeq != 0){
				pItemVO.setSizeVariantExists(true);
				if(currentItemSizeSeq < sizeSequence){
					pSortedChildSkuList.add(counter, pItemVO);
				addedFlag = true;
				break;
				}
			}
			counter++;
		}
		if(!addedFlag){
			if(currentItemColorSeq != 0){
				pItemVO.setColorVariantExists(true);
			} if(currentItemSizeSeq != 0){
				pItemVO.setSizeVariantExists(true);
			}
			pSortedChildSkuList.add(pItemVO);
		}
		
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools class : addItemBySortingVariants method ");
		}
		return pSortedChildSkuList;
	}

	
	/**
	 * Product item details.
	 *
	 * @param pProductItem the product item
	 * @return the list
	 */
	public List<TRUCSRProductItemDetailsVO> productItemDetails(RepositoryItem pProductItem){
		
		if(isLoggingDebug()){
			logDebug("Start :: TRUCSRCatalogTools class : populateProductSkuInformation method ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		List<TRUCSRProductItemDetailsVO> itemList = new ArrayList<TRUCSRProductItemDetailsVO>();
		List<RepositoryItem> childSkus= (List<RepositoryItem>) pProductItem.getPropertyValue(catalogProperties.getChildSKUsPropertyName());
		for (RepositoryItem childSKU : childSkus) {
			TRUCSRProductItemDetailsVO itemDetailVO = new TRUCSRProductItemDetailsVO();
			itemDetailVO.setSkuItem(childSKU);
			itemList.add(itemDetailVO);
			break;
			}
		if(isLoggingDebug()){
			logDebug("END :: TRUCSRCatalogTools class : populateProductSkuInformation method ");
		}
		return itemList;
	}
	
	/**
	 * This will return the min and maz suggested manufacturer age.
	 *
	 * @param pSkuItem the sku item
	 * @return the suggested age
	 */
	public String getMinAndMaxMFGAgeMessageBySku(RepositoryItem pSkuItem){
		if(isLoggingDebug()){
			logDebug("Start :: TRUCSRCatalogTools class : getMinAndMaxMFGAgeMessageBySku method ");
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogProperties();
		String mfgMinAge = (String) pSkuItem
				.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMin());
		String mfgMaxAge = (String) pSkuItem
				.getPropertyValue(catalogProperties.getProductMfrSuggestedAgeMax());
		String mfgAgeMessage = getMinAndMaxMFGAgeMessage(mfgMinAge, mfgMaxAge);
		
		if(isLoggingDebug()){
			logDebug("End :: TRUCSRCatalogTools class : getMinAndMaxMFGAgeMessageBySku method ");
		}
		return mfgAgeMessage;
	}
}
