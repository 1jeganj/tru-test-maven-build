package com.tru.commerce.csr.catalog;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.csr.util.TRUCSCConstants;


/**
 * @author PA
 * This will return the SKU details associated to a product.
 *
 */
public class TRUCSRSKUDetailDroplet extends DynamoServlet{
	
	/** The m catalog properties. */
	private TRUCSRCatalogTools mCatalogTools;
	
	
	/**
	 * @return mCatalogTools
	 */
	public TRUCSRCatalogTools getCatalogTools() {
		return mCatalogTools;
	}


	/**
	 * @param pCatalogTools to set mCatalogTools
	 */
	public void setCatalogTools(TRUCSRCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}


	/**
	 * This will sets the details of child skus associated to a product.
	 * @param pReq - http request
	 * @param pRes - http response
	 * @throws ServletException if an error occurs
	 * @throws IOException if an error occurs
	 */
	@Override
	public void service(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
		RepositoryItem skuItem = null;
		final String skuId = (String) pReq.getLocalParameter(TRUCSCConstants.SKU_ID);
		if(!skuId.isEmpty() &&  skuId != null){
		try {
			skuItem = getCatalogTools().findSKU(skuId);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("RepositoryException while finding the SKU item with id : "+skuId, e);
			}
		}
		SKUInfoVO skuVo = getCatalogTools().createSKUInfo(skuItem, null, false);
			pReq.setParameter(TRUCSCConstants.SKU_VO, skuVo);
			pReq.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pReq, pRes);
		}else{
			pReq.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pReq, pRes);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from class:[TRUCSRProductDetailsDroplet] method:[service]");
		}
	}
}
