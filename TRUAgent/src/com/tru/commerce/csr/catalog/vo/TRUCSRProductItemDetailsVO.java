package com.tru.commerce.csr.catalog.vo;

import atg.repository.RepositoryItem;

/**
 * This class is used to create VO object which contains ProductItemDetails.
 * @version 1.0
 * @author Professional Access
 *
 */
public class TRUCSRProductItemDetailsVO {
	
	/** The sku item. */
	private RepositoryItem mSkuItem;
	
	/** The color code. */
	private String mColorCode;
	
	/** The color sequence number. */
	private int mColorSequenceNumber;
	
	/** The juvinile sizedesc. */
	private String mJuninileSizeDesc;
	
	/** The size sequence number. */
	private int mSizeSequenceNumber;

	/** The boolean flag color variant exists. */
	private boolean mColorVariantExists;

	/** The boolean flag size variant exists. */
	private boolean mSizeVariantExists;
	
	/**
	 * @return mColorVariantExists
	 */
	public boolean isColorVariantExists() {
		return mColorVariantExists;
	}

	/**
	 * @param pColorVariantExists to set mColorVariantExists
	 */
	public void setColorVariantExists(boolean pColorVariantExists) {
		mColorVariantExists = pColorVariantExists;
	}

	/**
	 * @return mSizeVariantExists
	 */
	public boolean isSizeVariantExists() {
		return mSizeVariantExists;
	}

	/**
	 * @param pSizeVariantExists to set mSizeVariantExists
	 */
	public void setSizeVariantExists(boolean pSizeVariantExists) {
		mSizeVariantExists = pSizeVariantExists;
	}

	
	/**
	 * @return the mSizeSequenceNumber
	 */
	public int getSizeSequenceNumber() {
		return mSizeSequenceNumber;
	}

	/**
	 * @param pSizeSequenceNumber the mSizeSequenceNumber to set
	 */
	public void setSizeSequenceNumber(int pSizeSequenceNumber) {
		mSizeSequenceNumber = pSizeSequenceNumber;
	}

	/**
	 * @return mJuninileSizeDesc
	 */
	public String getJuninileSizeDesc() {
		return mJuninileSizeDesc;
	}

	
	/**
	 * @param pJuninileSizeDesc to set mJuninileSizeDesc
	 */
	public void setJuninileSizeDesc(String pJuninileSizeDesc) {
		mJuninileSizeDesc = pJuninileSizeDesc;
	}

	/**
	 * @return mColorSequenceNumber
	 */
	public int getColorSequenceNumber() {
		return mColorSequenceNumber;
	}

	/**
	 * @param pColorSequenceNumber to set mColorSequenceNumber
	 */
	public void setColorSequenceNumber(int pColorSequenceNumber) {
		mColorSequenceNumber = pColorSequenceNumber;
	}

	/**
	 * @return mColorCode
	 */
	public String getColorCode() {
		return mColorCode;
	}

	/**
	 * @param pColorCode to set mColorCode
	 */
	public void setColorCode(String pColorCode) {
		mColorCode = pColorCode;
	}

	/**
	 * @return mSkuItem
	 */
	public RepositoryItem getSkuItem() {
		return mSkuItem;
	}

	
	/**
	 * @param pSkuItem to set mSkuItem
	 */
	public void setSkuItem(RepositoryItem pSkuItem) {
		mSkuItem = pSkuItem;
	}
	
}
