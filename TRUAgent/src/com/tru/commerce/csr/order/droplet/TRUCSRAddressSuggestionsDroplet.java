package com.tru.commerce.csr.order.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.csr.order.CSROrderHolder;
import atg.core.util.ContactInfo;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.csr.order.TRUCSRAddressDoctor;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRUIntegrationConfiguration;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.utils.TRUIntegrationStatusUtil;

/**
 * class 'TRUCSRAddressSuggestionsDroplet' extends DynamoServlet functionalities OOTB .
 * 
 * @author Professional Access
 * @version 1.0
 *
 *
 */
public class TRUCSRAddressSuggestionsDroplet extends DynamoServlet{
	
	/** Property to hold mIntegrationStatusUtil. */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;

	/** Property to hold ProfileTools. */
	private TRUCSRAddressDoctor mAddressDoctor;
	
	/** Property to hold address doctor response status. */
	private String mAddressDoctorProcessStatus;
	

	/** Property to hold ProfileTools. */
	private TRUProfileTools mProfileTools;
	
	/** Property to hold ProfileTools. */
	private TRUCSRPurchaseProcessHelper mTruCSRPurchaseProcessHelper;
	/** The mStoreIntegrationConfiguration. */
	private TRUIntegrationConfiguration mStoreIntegrationConfiguration;
	
	/**
	 * property: shipping address.
	 */
	private ContactInfo mAddress = new ContactInfo();
	
	/**
	 * property to hold mCartComponent.
	 */
	private CSROrderHolder mCartComponent;
	
	/**
	 * @return the mCartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent
	 *            the cartComponent to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		mCartComponent = pCartComponent;
	}
	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}
	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}
	
	
	/**
	 * Gets the address doctor.
	 *
	 * @return the address doctor
	 */
	public TRUCSRAddressDoctor getAddressDoctor() {
		return mAddressDoctor;
	}

	/**
	 * Sets the address doctor.
	 *
	 * @param pAddressDoctor the new address doctor
	 */
	public void setAddressDoctor(TRUCSRAddressDoctor pAddressDoctor) {
		mAddressDoctor = pAddressDoctor;
	}
	
	/**
	 * Gets the address doctor process status.
	 *
	 * @return mAddressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * Sets the address doctor process status.
	 *
	 * @param pAddressDoctorProcessStatus the mAddressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}
	
	
	/**
	 * Gets the tru csr purchase process helper.
	 *
	 * @return the truCSRPurchaseProcessHelper
	 */
	public TRUCSRPurchaseProcessHelper getTruCSRPurchaseProcessHelper() {
		return mTruCSRPurchaseProcessHelper;
	}

	/**
	 * Sets the tru csr purchase process helper.
	 *
	 * @param pTruCSRPurchaseProcessHelper the truCSRPurchaseProcessHelper to set
	 */
	public void setTruCSRPurchaseProcessHelper(
			TRUCSRPurchaseProcessHelper pTruCSRPurchaseProcessHelper) {
		this.mTruCSRPurchaseProcessHelper = pTruCSRPurchaseProcessHelper;
	}

	/**
	 * Gets the profile tools.
	 *
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	/**
	 * Gets the address.
	 *
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * Sets the address.
	 *
	 * @param pAddress            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}
	/**
	 * @return the storeIntegrationConfiguration
	 */
	public TRUIntegrationConfiguration getStoreIntegrationConfiguration() {
		return mStoreIntegrationConfiguration;
	}

	/**
	 * @param pStoreIntegrationConfiguration the storeIntegrationConfiguration to set
	 */
	public void setStoreIntegrationConfiguration(TRUIntegrationConfiguration pStoreIntegrationConfiguration) {
		mStoreIntegrationConfiguration = pStoreIntegrationConfiguration;
	}
	
	/**
	 * This method will takes address as input and gets the suggestions.
	 * 
	 * @param pReq
	 *            the servlet's request
	 * @param pRes
	 *            the servlet's response
	 * @throws ServletException
	 *             if an error occurred while processing the servlet request
	 * @throws IOException
	 *             if an error occurred while reading or writing the servlet
	 *             
	 */
	
	public void service(DynamoHttpServletRequest pReq,
			DynamoHttpServletResponse pRes) throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRAddressSuggestionsDroplet] method:[service]");
		}
		getAddressDoctor().setTaxwareError(false);
		String address1 =(String) pReq.getObjectParameter(TRUConstants.ADDRESS_1);
		String address2 =(String) pReq.getObjectParameter(TRUConstants.ADDRESS_2);
		String city =(String) pReq.getObjectParameter(TRUConstants.CITY);
		String state =(String) pReq.getObjectParameter(TRUConstants.STATE);
		String postalCode =(String) pReq.getObjectParameter(TRUConstants.POSTAL_CODE_FOR_ADD);

		boolean isEnableAddressDoctor = false;
		
		isEnableAddressDoctor = getStoreIntegrationConfiguration().isEnableAddressDoctor();

		boolean isAddressValidated = false;
		if (!isAddressValidated    && isEnableAddressDoctor) {
			
			final String addressStatus = validAddress(address1,address2, city, state, postalCode);
			setAddressDoctorProcessStatus(addressStatus);
			TRUProfileTools  profileTools=(TRUProfileTools) getTruCSRPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
			TRUAddressDoctorResponseVO addressResponseVo = profileTools.getAddressDoctorResponseVO();
			getAddressDoctor().setAddressDoctorResponseVO(profileTools.getAddressDoctorResponseVO());
			
			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
				isAddressValidated = Boolean.TRUE;
			}
			else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
				
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				addressDoctorResponseVO.setDerivedStatus(addressResponseVo.getDerivedStatus());
				getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
			}
			else if (addressStatus != null && TRUConstants.CONNECTION_ERROR.equalsIgnoreCase(addressStatus)) {
				setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_CONNECTION_ERROR);
				TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
				addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
				addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
				getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
			}
		}
		
		else if (!isEnableAddressDoctor && !isAddressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			addressDoctorResponseVO.setDerivedStatus(TRUConstants.YES);
			getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRAddressSuggestionsDroplet] method:[service]");
		}

	}
		
	       
	/**
	 * This method will takes address as input and gets the suggestions.
	 * 
	 * @param pAddress1
	 *            the String.
	 * @param pAddress2
	 *            the String.
	 * @param pCity
	 *             the String.
	 * @param pState 
	 * 				the String.
	 * @param pPostalCode
	 *             the String.
	 * @return  addressStatus String.       
	 */ 
	
	public String validAddress(String pAddress1,String pAddress2, String pCity, String pState, String pPostalCode) {

        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[validAddress]");
       }
		vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ",
				pAddress2);
		String addressStatus = null;
		if (mAddress != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1,pAddress1);
			addressValue.put(TRUConstants.ADDRESS_2,pAddress2);
			addressValue.put(TRUConstants.CITY, pCity);
			addressValue.put(TRUConstants.STATE, pState);
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pPostalCode);
			addressStatus = getProfileTools().validateAddress(addressValue);
		}
		vlogDebug("Returns addressStatus: {0} ",addressStatus);
		if (addressStatus == null) {
			addressStatus = TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR;
		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[validAddress]");
       }
		return addressStatus;
	}

}
