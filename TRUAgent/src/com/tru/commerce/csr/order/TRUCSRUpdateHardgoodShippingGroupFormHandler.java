package com.tru.commerce.csr.order;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.csr.order.CSRUpdateHardgoodShippingGroupFormHandler;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;

/**
 * class 'TRUCSRUpdateHardgoodShippingGroupFormHandler' extends the TRU CSC Specific functionalities OOTB 'CSRUpdateHardgoodShippingGroupFormHandler'.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCSRUpdateHardgoodShippingGroupFormHandler extends CSRUpdateHardgoodShippingGroupFormHandler{
	
	
	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * property to hold mAddressDoctor.
	 */
	private TRUCSRAddressDoctor mAddressDoctor;

	/**
	 * property to hold mProfileTools.
	 */
	private TRUProfileTools mProfileTools;

	/**
	 * property to hold mTRUConfiguration
	 */
	private TRUConfiguration mTRUConfiguration;
	
	/**
	 * boolean property to hold if address is validated by address doctor ot not
	 */
	private String mAddressValidated;
	
	/**
	 * property: shipping address
	 */
	private ContactInfo mAddress = new ContactInfo();
	
	/**
	 * Property to hold address doctor response status
	 */
	private String mAddressDoctorProcessStatus;
	
	/**
	 * Property to hold Address doctor response VO
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
	
	/**
	 * property to hold mCommonSuccessURL
	 */
	private String mCommonSuccessURL;
	
	/**
	 * property to hold mCommonErrorURL
	 */
	private String mCommonErrorURL;
	
	/**
	 *  property to hold integrationStatusUtil.
	 */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	
	/**
	 * Gets the profile tools.
	 * 
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 * 
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * @return the mAddressDoctor
	 */
	public TRUCSRAddressDoctor getAddressDoctor() {
		return mAddressDoctor;
	}

	/**
	 * @param pAddressDoctor
	 *            the TRUCSRAddressDoctor to set
	 */
	public void setAddressDoctor(TRUCSRAddressDoctor pAddressDoctor) {
		this.mAddressDoctor = pAddressDoctor;
	}

	/**
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * mAddressValidated
	 * 
	 * @return addressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * mAddressValidated
	 * 
	 * @param pAddressValidated
	 *            the addressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		this.mAddressValidated = pAddressValidated;
	}


	/**
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * @param pAddress
	 *            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}

	/**
	 * 
	 * @return addressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * 
	 * @param pAddressDoctorProcessStatus
	 *            the addressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		this.mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * @return the TRUAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * 
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		this.mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * @param pCommonErrorURL
	 *            the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}
	
	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}
	
	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}
	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}
		
	
	/**
	 * preUpdateHardgoodShippingGroup method is overridden validate address.
	 * 
	 * @param pRequest		
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	public void preUpdateHardgoodShippingGroup(	DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
	throws ServletException, IOException 
	{
    	if(isLoggingDebug()){
			logDebug("TRUCSRUpdateHardgoodShippingGroupFormHandler (preUpdateHardgoodShippingGroup) : Starts");
		}
		/*boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
		boolean isEnableAddressDoctor = false;
		getAddressDoctor().setTaxwareError(false);
		
	    HardgoodShippingGroup shippingGroup = getWorkingHardgoodShippingGroup();
	    
	    // Validate address with AddressDoctor Service
		
		if(SiteContextManager.getCurrentSite() != null){
			Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
			if(null!=value){
				isEnableAddressDoctor = (boolean) value;	
			}						
		}
	    
		if (false	&& isEnableAddressDoctor) {
			
			String addressStatus = validateAddress(shippingGroup.getShippingAddress());
			setAddressDoctorProcessStatus(addressStatus);
			getAddressDoctor().setAddressDoctorResponseVO(getProfileTools().getAddressDoctorResponseVO());
			
			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED
					.equalsIgnoreCase(addressStatus)) {
				isAddressValidated = Boolean.TRUE;
			}
			 else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
					setAddressDoctorProcessStatus(addressStatus);
					TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
					addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
					getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
					//isAddressValidated = Boolean.TRUE;
				}
		} else if (false && !isAddressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
		}*/
		
    	if(isLoggingDebug()){
			logDebug("TRUCSRUpdateHardgoodShippingGroupFormHandler (preUpdateHardgoodShippingGroup) : Ends");
		}
	}
	
	/**
	 * validates the address with address doctor.
	 * 
	 * @param pAddress
	 *            address object in which the user entered address is populated
	 * @return the status address doctor returned
	 */
	public String validateAddress(Address pAddress) {
    	if(isLoggingDebug()){
			logDebug("TRUCSRUpdateHardgoodShippingGroupFormHandler (validateAddress) : Starts");
		}
		vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ", pAddress);
		String addressStatus = null;
		if (pAddress != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1, pAddress.getAddress1());
			addressValue.put(TRUConstants.ADDRESS_2, pAddress.getAddress2());
			addressValue.put(TRUConstants.CITY, pAddress.getCity());
			addressValue.put(TRUConstants.STATE, pAddress.getState());
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pAddress.getPostalCode());
			addressStatus = (getProfileTools()).validateAddress(addressValue);
		}
		vlogDebug("returns addressStatus: {0} ", addressStatus);
		if (addressStatus == null) {
			addressStatus = TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR;
		}
    	if(isLoggingDebug()){
			logDebug("TRUCSRUpdateHardgoodShippingGroupFormHandler (validateAddress) : Ends");
		}
		return addressStatus;
	}
}