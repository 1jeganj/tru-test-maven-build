/**
 * 
 */
package com.tru.commerce.csr.order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import atg.commerce.CommerceException;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class extends ATG OOTB TRUOrderManager and used
 * for managing the order related operation.
 * 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSROrderManager extends TRUOrderManager {

	/**
	 * Ensures in-store payment group in order.
	 * 
	 * @param pOrder
	 *            the Order Object
	 * @return PaymentGroup
	 *            the PaymentGroup
	 * @throws CommerceException
	 *            the CommerceException
	 */
	public PaymentGroup ensureInStoreCSCPayment(Order pOrder)
			throws CommerceException
	{
		if(isLoggingDebug()){
			logDebug("TRUCSROrderManager (ensureInStoreCSCPayment) : Starts");
		}
		PaymentGroup inStorePayment = null;
		List<PaymentGroup> exclusionPaymentGroups = new ArrayList<PaymentGroup>();

		boolean ensureInStorePayment = Boolean.FALSE;
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof InStorePayment) {
				ensureInStorePayment = true;
				exclusionPaymentGroups.add(paymentGroup);
			}
		}
		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, exclusionPaymentGroups);

		if (!ensureInStorePayment) {
			inStorePayment = getPaymentGroupManager().createPaymentGroup(TRUCheckoutConstants.IN_STORE_PAYMENT);
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder,	inStorePayment);
			addRemainingOrderAmountToPaymentGroup(pOrder, inStorePayment.getId());

			return inStorePayment;

		}
		if(isLoggingDebug()){
			logDebug("TRUCSROrderManager (ensureInStoreCSCPayment) : Ends");
		}
		return null;
	}

	/**
	 * Instore Payment group in CSC order with the given payment group type.
	 * 
	 * @param pOrder
	 *            the Order Object
	 * @param pPaymentGroupType
	 *            the String
	 * @return PaymentGroup
	 *            the PaymentGroup
	 * @throws CommerceException
	 *            the CommerceException
	 */
	public PaymentGroup ensureInstorePaymentGroup(Order pOrder, String pPaymentGroupType) throws CommerceException {
		if(isLoggingDebug()){
			logDebug("TRUCSROrderManager (ensureInstorePaymentGroup) : Starts");
		}
		TRUOrderManager orderManager = (TRUCSROrderManager) getOrderManager();
		if(TRUCheckoutConstants.IN_STORE_PAYMENT.equalsIgnoreCase(pPaymentGroupType)) {
			return ((TRUCSROrderManager) orderManager).ensureInStoreCSCPayment(pOrder);
		}
		if(isLoggingDebug()){
			logDebug("TRUCSROrderManager (ensureInstorePaymentGroup) : Ends");
		}
		return null;
	}

	/**
	 * Checks if is any address field empty.
	 * 
	 * @param pAddress
	 *            address
	 * @return boolean
	 */
	@Override
	public boolean isAnyAddressFieldEmpty(ContactInfo pAddress) {
 		if (isLoggingDebug()) {
		logDebug("Entering into class:[TRUOrderManager] method:[isAnyAddressFieldEmpty]");
	}
		String firstName = null;
		String lastName = null;
		String country = null;
		String address1 = null;
		String city = null;
		String state = null;
		String postalCode = null;
		String phoneNumber = null;
		if (pAddress != null) {
			firstName = pAddress.getFirstName();
			lastName = pAddress.getLastName();
			country = pAddress.getCountry();
			address1 = pAddress.getAddress1();
			city = pAddress.getCity();
			state = pAddress.getState();
			postalCode = pAddress.getPostalCode();
			phoneNumber = pAddress.getPhoneNumber();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUOrderManager] method:[isAnyAddressFieldEmpty]");
		}
		return StringUtils.isBlank(firstName) ||
			   StringUtils.isBlank(lastName) ||
			   StringUtils.isBlank(country) ||
			   StringUtils.isBlank(address1) ||
			   StringUtils.isBlank(city) ||
			   StringUtils.isBlank(state) ||
			   StringUtils.isBlank(postalCode) ||
			   StringUtils.isBlank(phoneNumber);
	}

	/**
	 * Checks if is any credit card field empty.
	 * 
	 * @param pCreditCardMap
	 *            - creditCardMap
	 * @return boolean
	 */
	@SuppressWarnings("unused")
	@Override
	public boolean isAnyCreditCardFieldEmpty(Map<String, String> pCreditCardMap) {
		if (isLoggingDebug()) {
		logDebug("Entering into class:[TRUOrderManager] method:[isAnyCreditCardFieldEmpty]");
	}
		TRUPropertyManager cpmgr = ((TRUOrderManager) getOrderManager()).getPropertyManager();
		String ccNum = null;
		String ccExpMonth = null;
		String ccExpYear = null;
		String ccNameOnCard = null;
		String ccCVV = null;
		String ccCardinalToken = null;
		String creditCardType = null;
		String cardLength = null;
		if (pCreditCardMap != null) {
			if (pCreditCardMap.get(cpmgr.getCreditCardNumberPropertyName()) != null) {
				ccNum = pCreditCardMap.get(cpmgr.getCreditCardNumberPropertyName());
			} /*else {
				ccNum = "5424180279791732";
				}*/
			// COMMENTTED FOR DISABLING CARDINAL AND ENABLE RADIAL
			// THIS WAS CHANGED FROM FALSE TO TRUE
			
			/*ccCardinalToken = pCreditCardMap.get(cpmgr.getCreditCardTokenPropertyName());*/
			ccExpMonth = pCreditCardMap.get(cpmgr.getCreditCardExpirationMonthPropertyName());
			ccExpYear = pCreditCardMap.get(cpmgr.getCreditCardExpirationYearPropertyName());
			ccNameOnCard = pCreditCardMap.get(cpmgr.getNameOnCardPropertyName());
			if (pCreditCardMap.get(cpmgr.getCreditCardVerificationNumberPropertyName()) != null) {
				ccCVV = pCreditCardMap.get(cpmgr.getCreditCardVerificationNumberPropertyName());
			}
			cardLength = pCreditCardMap.get(TRUConstants.CARD_LENGTH);
		}
		// add credit card
		if (ccNum != null/*&& !ccNum.trim().equals(ccCardinalToken.trim())*/) {
			creditCardType =
					((TRUProfileTools) getOrderTools().getProfileTools()).getCreditCardTypeFromRepository(ccNum,cardLength);
			pCreditCardMap.put(cpmgr.getCreditCardTypePropertyName(), creditCardType);
		} else {
			// commit order when card is saved
			creditCardType = pCreditCardMap.get(cpmgr.getCreditCardTypePropertyName());
			pCreditCardMap.put(cpmgr.getCreditCardTypePropertyName(), creditCardType);
		}
		vlogDebug("@Class::TRUOrderManager::@method::isAnyCreditCardFieldEmpty() : END");
		if (StringUtils.isNotBlank(creditCardType)
				&& creditCardType.equals(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
			return StringUtils.isBlank(ccNum) ||
				   StringUtils.isBlank(ccNameOnCard) ||
				   StringUtils.isBlank(ccCVV)/* ||
				   StringUtils.isBlank(ccCardinalToken)*/; 
			// COMMENTTED FOR DISABLING CARDINAL AND ENABLE RADIAL
		} else {
			return StringUtils.isBlank(ccNum) || 
				   StringUtils.isBlank(ccExpMonth) ||
				   StringUtils.isBlank(ccExpYear) ||
				   StringUtils.isBlank(ccNameOnCard) ||
				   StringUtils.isBlank(ccCVV) /*||
				   StringUtils.isBlank(ccCardinalToken)*/;
			// COMMENTTED FOR DISABLING CARDINAL AND ENABLE RADIAL
		}
	}
	
	/**
	 * Update order with fraud parameters.
	 *
	 * @param pRequest the request
	 * @param pOrder the order
	 * @param pProfile the profile
	 * @param pDeviceId the device id
	 * @param pUserSessionStartTime the user session start time
	 */
	public void updateOrderWithFraudParameters(DynamoHttpServletRequest pRequest, TRUOrderImpl pOrder, 
			RepositoryItem pProfile, String pDeviceId, Date pUserSessionStartTime){
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::updateOrderWithFraudParameters() : START");
		}
		if(StringUtils.isNotBlank(pDeviceId)) {
			int deviceIdLenth = pDeviceId.length();
			if(deviceIdLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setDeviceID(pDeviceId.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setDeviceID(pDeviceId);
			}
		}
		pOrder.setBrowserID(pRequest.getHeader(TRUCheckoutConstants.USER_AGENT));
		pOrder.setBrowserSessionId(pRequest.getCookieParameter(TRUCheckoutConstants.TRUSESSIONID));
		pOrder.setBrowserConnection(pRequest.getHeader(TRUCheckoutConstants.CONNECTION));
		pOrder.setBrowserAccept(pRequest.getHeader(TRUCheckoutConstants.ACCEPT));
		if (isLoggingDebug()) {
			vlogDebug("pRequest.getHeader(Accept-Encoding) :{0}", pRequest.getHeader(TRUCheckoutConstants.ACCEPT_ENCODING));
		}
		pOrder.setBrowserAcceptEncoding(pRequest.getHeader(TRUCheckoutConstants.ACCEPT_ENCODING));
		pOrder.setBrowserAcceptCharset(pRequest.getHeader(TRUCheckoutConstants.CONTENT_TYPE));
		pOrder.setBrowserIdLanguageCode(pRequest.getHeader(TRUCheckoutConstants.ACCEPT_LANGUAGE));
		String cookie = pRequest.getHeader(TRUCheckoutConstants.COOKIE);
		if(StringUtils.isNotBlank(cookie)) {
			int cookieLenth = cookie.length();
			if(cookieLenth > TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO) {
				pOrder.setBrowserCookie(cookie.substring(TRUCheckoutConstants.INT_ZERO, TRUCheckoutConstants.INT_FOUR_ZERO_ZERO_ZERO));
			} else {
				pOrder.setBrowserCookie(cookie);
			}
		}
		pOrder.setBrowserReferer(pRequest.getHeader(TRUCheckoutConstants.REFERER));
			pOrder.setHostName(pRequest.getServerName());

		String clientIp = pRequest.getHeader(TRUCheckoutConstants.TRUE_CLIENT_IP);
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader(True-Client-IP) :{0}", clientIp);
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getRemoteAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getRemoteAddr():{0}", clientIp);
			}	
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getLocalAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getLocalAddr():{0}", clientIp);
			}
		}
		pOrder.setCustomerIPAddress(clientIp);
		try {
			Date currentDate = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(TRUCheckoutConstants.HH_MM_SS);
//			Date startTime = dateFormat.parse(dateFormat.format(pUserSessionStartTime));
			Date currentTime = dateFormat.parse(dateFormat.format(currentDate));
			long duration = 0;
			pOrder.setTimeSpentOnSite(formatDuration(duration));
		} catch (ParseException e) {
			if(isLoggingError()){
				vlogError("ParseException in handleAddUpdateBppItem : {0}", e);
			}
		}
		if(isLoggingDebug()){
			logDebug("@Class::TRUOrderManager::@method::updateOrderWithFraudParameters() : END");
		}
	}
	
	/**
	 * Format duration.
	 *
	 * @param pDuration the duration
	 * @return the string
	 */
	private static String formatDuration(long pDuration) {
	    long hours = TimeUnit.MILLISECONDS.toHours(pDuration);
	    long minutes = TimeUnit.MILLISECONDS.toMinutes(pDuration) % TRUCheckoutConstants.INT_SIX_ZERO;
	    long seconds = TimeUnit.MILLISECONDS.toSeconds(pDuration) % TRUCheckoutConstants.INT_SIX_ZERO;
	    return String.format(TRUCheckoutConstants.TIME_SPENT_ONSITE_FORMAT, hours, minutes, seconds);
	}
	

}