package com.tru.commerce.csr.order;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.purchase.CreateHardgoodShippingGroupFormHandler;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.droplet.DropletFormException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;

import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUAddressDoctorResponseVO;

/**
 * This class extends ATG OOTB CreateHardgoodShippingGroupFormHandler and used.
 * to manage the TRU Specific Address Doctor Integration
 *
 * @version 1.0
 * @author Professional Access
 *
 *
 */
public class TRUCSRCreateHardgoodShippingGroupFormHandler extends CreateHardgoodShippingGroupFormHandler {

	/**
     * property to hold the mAddressDoctor.
     */
	private TRUCSRAddressDoctor mAddressDoctor;

    /**
     * property to hold the mProfileTools.
     */

	private TRUProfileTools mProfileTools;

	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;
	
	/**
	 * boolean property to hold if address is validated by
	 * address doctor or not.
	 */
	private String mAddressValidated;
	
	/**
	 * property: shipping address.
	 */
	private ContactInfo mAddress = new ContactInfo();
	
	/**
	 * Property to hold address doctor response status.
	 */
	private String mAddressDoctorProcessStatus;
	
	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;
	
	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	
	/**
	 * Gets the profile tools.
	 *
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 * 
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	/**
	 * @return the mAddressDoctor
	 */
	public TRUCSRAddressDoctor getAddressDoctor() {
		return mAddressDoctor;
	}

	/**
	 * @param pAddressDoctor
	 *            the TRUCSRAddressDoctor to set
	 */
	public void setAddressDoctor(TRUCSRAddressDoctor pAddressDoctor) {
		this.mAddressDoctor = pAddressDoctor;
	}

	/**
	 * @return the tRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration
	 *            the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * mAddressValidated.
	 * @return addressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * mAddressValidated.
	 *
	 * @param pAddressValidated
	 *            the addressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		this.mAddressValidated = pAddressValidated;
	}

	/**
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * @param pAddress
	 *            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}

	/**
	 *
	 * @return addressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 *
	 * @param pAddressDoctorProcessStatus
	 *            the addressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		this.mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * @return the TRUAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 *
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		this.mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * @param pCommonErrorURL
	 *            the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		this.mCommonErrorURL = pCommonErrorURL;
	}
	
	/**
	 * preCreateHardgoodShippingGroup method is overridden validate address.
	 *
	 * @param pRequest
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	public void preCreateHardgoodShippingGroup(	DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
	throws ServletException, IOException
	{
	 	if (isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (preCreateHardgoodShippingGroup) : Starts");
		}

		if (isValidateAddress()) {
			validateShippingGroup();
			if (getFormError()){
				return;
			}
		}
	 	if (isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (preCreateHardgoodShippingGroup) : Ends");
		}
	}

	
	/**
	 *  Method to validate Address.
	 */
	public void validateShippingGroup() 
	{
	 	if (isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (validateShippingGroup) : Starts");
		}
		HardgoodShippingGroup shippingGroup = getHardgoodShippingGroup();
		DynamoHttpServletRequest req = ServletUtil.getCurrentRequest();
		DynamoHttpServletResponse res = ServletUtil.getCurrentResponse();

		if ((shippingGroup == null) || 
			(shippingGroup.getShippingAddress() == null)) {
			if (isLoggingDebug()){
				logDebug("Shipping Group or Shipping Address can't be NULL.");
			}
			try {
				String msg = formatUserMessage(	TRUCSCConstants.COULD_NOT_FIND_SHIPPING_GROUP_OR_ADDRESS2, req, res);
				String propertyPath = generatePropertyPath(TRUCSCConstants.HARDGOOD_SHIPPING_GROUP);
				addFormException(new DropletFormException(msg, propertyPath));
			} catch (IOException ioe) {
				if (isLoggingError()){
					logError(ioe);
				}
			} catch (ServletException se) {
				if (isLoggingError()){
					logError(se);
				}
			}
			return;
		}
		
		boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
		// Validate address with AddressDoctor Service
		if (!isAddressValidated	&& getTRUConfiguration().isEnableAddressDoctor()) {
			String addressStatus = validAddress(getHardgoodShippingGroup().getShippingAddress());
			setAddressDoctorProcessStatus(addressStatus);
			setAddressDoctorResponseVO(((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools().getAddressDoctorResponseVO());
			getAddressDoctor().setAddressDoctorResponseVO(((TRUPurchaseProcessHelper) getPurchaseProcessHelper()).getProfileTools()
									.getAddressDoctorResponseVO());

			if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
				isAddressValidated = Boolean.TRUE;
			}
		} else if (!getTRUConfiguration().isEnableAddressDoctor() && !isAddressValidated) {
			setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
			TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
			addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
			setAddressDoctorResponseVO(addressDoctorResponseVO);
		}
	 	if(isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (validateShippingGroup) : Ends");
		}
	}

	
	/**
	 * Method to validAddress.
	 *
	 * @param pShippingAddress
	 *            the ShippingAddress
	 * @return String
	 */
	public String validAddress(Address pShippingAddress) {
	 	if(isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (validAddress) : Starts");
		}
		vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ",
				pShippingAddress);
		String addressStatus = null;
		if (pShippingAddress != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1,pShippingAddress.getAddress1());
			addressValue.put(TRUConstants.ADDRESS_2,pShippingAddress.getAddress1());
			addressValue.put(TRUConstants.CITY, pShippingAddress.getCity());
			addressValue.put(TRUConstants.STATE, pShippingAddress.getState());
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pShippingAddress.getPostalCode());
			addressStatus = (getProfileTools()).validateAddress(addressValue);
		}
		vlogDebug("returns addressStatus: {0} ",addressStatus);
	 	if(isLoggingDebug()){
			logDebug("TRUCSRCreateHardgoodShippingGroupFormHandler (validAddress) : Ends");
		}
		return addressStatus;

	}
}
