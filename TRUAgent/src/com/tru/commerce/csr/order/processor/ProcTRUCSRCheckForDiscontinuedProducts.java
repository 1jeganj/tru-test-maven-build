package com.tru.commerce.csr.order.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.common.TRUUserSession;

/**
 * Implemented for Intraday items check.
 * The Class ProcTRUCSRCheckForDiscontinuedProducts.
 *  @author Professional access
 *  @version 1.0
 */
public class ProcTRUCSRCheckForDiscontinuedProducts extends ApplicationLoggingImpl
implements PipelineProcessor {
	/**
	 * Resource bundle name.
	 */
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	/** The Constant MSG_INVALID_ORDER_PARAMETER. 
	 */
	public static final String MSG_INVALID_ORDER_PARAMETER = "InvalidOrderParameter";
	/** The Constant SUCCESS. 
	 * 
	 */
	private static final int SUCCESS = 1;
	/** 
	 * Resource Bundle. 
	 */
	private static java.util.ResourceBundle sResourceBundle = 
			LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
	/** The Purchase process helper. */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	
	/**
	 * Gets the purchase process helper.
	 *
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * Sets the purchase process helper.
	 *
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * Returns the valid return codes:
	 * 1 - The processor completed.
	 * @return an integer array of the valid return codes.
	 */
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };

		return ret;
	}
	
	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;
	
	/**
	 * Gets the shopping cart utils.
	 * 
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 * 
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	
	/**
	 * Run process.
	 *
	 * @param pParamObject the param object
	 * @param pParamPipelineResult the param pipeline result
	 * @return the int
	 * @throws Exception the exception
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(java.lang.Object, atg.service.pipeline.PipelineResult)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParamObject,
			PipelineResult pParamPipelineResult) throws Exception {
		if(isLoggingDebug()){
			vlogDebug("ProcTRUCSRCheckForDiscontinuedProducts.runProcess()methods.STARTS");
		}
		Map map = (HashMap) pParamObject;
		Order order = (Order) map.get(PipelineConstants.ORDER);
		TRUUserSession session = (TRUUserSession) map.get(TRUPipeLineConstants.SESSION_COMPONENT);
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(MSG_INVALID_ORDER_PARAMETER, 
					MY_RESOURCE_NAME, sResourceBundle));
		}
		//Custom validation
		getPurchaseProcessHelper().checkForDiscontinuedProducts(order, session);
		
		if (null != order && null != session && null != session.getDeletedSkuList()) {
			final Set<String> deletedSkuList = (Set<String>) session.getDeletedSkuList();
			int count=0;
					List<String> removeItemList =getShoppingCartUtils().getTRUCartModifierHelper()
					.getRemovedItemMessages(deletedSkuList);
					for(String item:removeItemList){
						String infoMsg=item.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
						pParamPipelineResult.addError(TRUCSCConstants.ERROR_MESSAGE+count, infoMsg);
						count++;
					}
					return TRUCheckoutConstants.INT_ZERO;
			}
		if (isLoggingDebug()) {
			vlogDebug("ProcTRUCSRCheckForDiscontinuedProducts.runProcess()methods.ENDS Orderid : {0}",order.getId());
		}
		return TRUCheckoutConstants.INT_ONE ;
	}
}
