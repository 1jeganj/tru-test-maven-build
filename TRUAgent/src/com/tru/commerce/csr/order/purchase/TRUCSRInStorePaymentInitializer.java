package com.tru.commerce.csr.order.purchase;


import atg.commerce.order.Order;
import atg.commerce.order.purchase.InStorePaymentInitializer;
import atg.commerce.order.purchase.PaymentGroupInitializationException;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;

/**
 * Overriding InStorePaymentInitializer to restrict the creation of InStorePayment if there are any InStorePickUpShippingGroup
 *
 */
public class TRUCSRInStorePaymentInitializer extends InStorePaymentInitializer{

	/**
	 * Overriding this method to restrict the creation of InStorePayment if there are any InStorePickUpShippingGroup.
	 * 
	 * @param pProfile for profile
	 * @param pPaymentGroupMapContainer for pPaymentGroupMapContainer
	 * @param pRequest for DynamoHttpServletRequest
	 *
	 * @throws PaymentGroupInitializationException for  PaymentGroupInitializationException
	 *
	 */
	public void initializePaymentGroups(Profile pProfile, PaymentGroupMapContainer pPaymentGroupMapContainer, DynamoHttpServletRequest pRequest)
			    throws PaymentGroupInitializationException
 {
		Order order = getConfiguration().getShoppingCart().getCurrent();
		if (order == null) {
			return;
		}
	}
}
