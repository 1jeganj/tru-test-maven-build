/**
 * 
 */
package com.tru.commerce.csr.order.purchase;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;





import atg.commerce.CommerceException;
import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.order.CSROrderHolder;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.CreatePaymentGroupFormHandler;
import atg.commerce.order.purchase.PaymentGroupFormHandler;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.csr.order.TRUCSROrderManager;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;

/**
 * This class extends ATG OOTB PaymentGroupFormHandler and used
 * to perform all the operations on payment page in checkout flow.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUCSCBillingInfoFormHandler extends PaymentGroupFormHandler implements CreatePaymentGroupFormHandler {

	/**
 	 *  Property to hold Address mCSRAgentTools
 	 */
 	private CSRAgentTools mCSRAgentTools;

	/**
	 * property to hold PropertyManager.
	 */
 	private CSROrderHolder mCartComponent;
 		
	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;
	
	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	
	/**
	 * property to hold mPaymentGroupType.
	 */
	private String mPaymentGroupType;
	
 	/**
     * Reference to the Transaction Manager
     */
    private TransactionManager mTransactionManager;
    
	/**
	 * property to hold mPaymentInfoSuccessURL.
	 */
	private String mPaymentInfoSuccessURL;
	
	/**
	 * property to hold mPaymentInfoErrorURL.
	 */
	private String mPaymentInfoErrorURL;
	
	/** The m email. */
	private String mEmail;
	
	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();
	
	/** The Saved address. */
	private boolean mNewBillingAddress;
	
	/** The Edit billing address. */
	private boolean mEditBillingAddress;
	
	/** The Edit credit card. */
	private boolean mEditCreditCard;
	
	/** The Saved address. */
	private boolean mNewCreditCard;
	
	/** property to hold BillingAddressNickName.*/
	private String mBillingAddressNickName;
	
	/**
	 * Gets the billing address nick name.
	 *
	 * @return mBillingAddressNickName.
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}
	
	/**
	 * Sets the billing address nick name.
	 *
	 * @param pBillingAddressNickName parameter.
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}
	/**
	 * @return the editCreditCard
	 */
	public boolean isEditCreditCard() {
		return mEditCreditCard;
	}

	/**
	 * @param pEditCreditCard the editCreditCard to set
	 */
	public void setEditCreditCard(boolean pEditCreditCard) {
		mEditCreditCard = pEditCreditCard;
	}

	/**
	 * @return the newCreditCard
	 */
	public boolean isNewCreditCard() {
		return mNewCreditCard;
	}

	/**
	 * @param pNewCreditCard the newCreditCard to set
	 */
	public void setNewCreditCard(boolean pNewCreditCard) {
		mNewCreditCard = pNewCreditCard;
	}

	/**
	 * @return the newBillingAddress
	 */
	public boolean isNewBillingAddress() {
		return mNewBillingAddress;
	}

	/**
	 * @param pNewBillingAddress the newBillingAddress to set
	 */
	public void setNewBillingAddress(boolean pNewBillingAddress) {
		mNewBillingAddress = pNewBillingAddress;
	}

	/**
	 * @return the editBillingAddress
	 */
	public boolean isEditBillingAddress() {
		return mEditBillingAddress;
	}

	/**
	 * @param pEditBillingAddress the editBillingAddress to set
	 */
	public void setEditBillingAddress(boolean pEditBillingAddress) {
		mEditBillingAddress = pEditBillingAddress;
	}

	/** property to Hold financeAgreed. */
	private boolean mNoFinanceAgreed;
	
	/**
	 * @return the addressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * @param pAddressInputFields the addressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		mAddressInputFields = pAddressInputFields;
	}
	
	/**
	 * Gets the m email.
	 *
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * Sets the m email.
	 *
	 * @param pEmail
	 *            the email to set
	 */
	public void setEmail(String pEmail) {
		this.mEmail = pEmail;
	}
    
    /**
	 * @return the paymentInfoSuccessURL
	 */
	public String getPaymentInfoSuccessURL() {
		return mPaymentInfoSuccessURL;
	}
	
	/**
	 * @return the paymentInfoErrorURL
	 */
	public String getPaymentInfoErrorURL() {
		return mPaymentInfoErrorURL;
	}
    
	/**
	 * 
	 * @param pPaymentInfoErrorURL
	 *            the paymentInfoErrorURL to set
	 **/
	public void setPaymentInfoErrorURL(String pPaymentInfoErrorURL) {
		mPaymentInfoErrorURL = pPaymentInfoErrorURL;
	}
	
	/**
	 * 
	 * @param pPaymentInfoSuccessURL
	 *            the paymentInfoSuccessURL to set
	 **/
	public void setPaymentInfoSuccessURL(String pPaymentInfoSuccessURL) {
		mPaymentInfoSuccessURL = pPaymentInfoSuccessURL;
	}

    /**
     * @return the transactionManager
     */
    public TransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    /**
     * @param pTransactionManager the transactionManager to set
     */
    public void setTransactionManager(TransactionManager pTransactionManager) {
        mTransactionManager = pTransactionManager;
    }
    
    /**
	 * @return the mCartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent
	 *            the CSROrderHolder to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		this.mCartComponent = pCartComponent;
	}

	/**
	 * @return the mCSRAgentTools
	 */
	public CSRAgentTools getCSRAgentTools() {
		return mCSRAgentTools;
	}
	
	/**
	 * @param pCSRAgentTools
	 *            the CSRAgentTools to set
	 */
	public void setCSRAgentTools(CSRAgentTools pCSRAgentTools) {
		this.mCSRAgentTools =pCSRAgentTools ;
	}

	/**
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		this.mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}
	
	/**
	 * @return the paymentGroupType
	 */
	public String getPaymentGroupType() {
		return mPaymentGroupType;
	}

	/**
	 * @param pPaymentGroupType
	 *            the paymentGroupType to set
	 */
	public void setPaymentGroupType(String pPaymentGroupType) {
		mPaymentGroupType = pPaymentGroupType;
	}
	
	/**
	 * This method is used to handle the initialization of payment group card
	 * based on the given payment group type.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return true
	 * @throws ServletException
	 *             servletException
	 * @throws IOException
	 *             iOException
	 */
	@SuppressWarnings("unchecked")
	public boolean handleEnsurePaymentGroup(DynamoHttpServletRequest pRequest,	DynamoHttpServletResponse pResponse) 
	throws ServletException, IOException {
		
		if (isLoggingDebug()) {
			logDebug("Start of TRUCSCBillingInfoFormHandler.handleEnsurePaymentGroup method");
		}

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		TRUOrderImpl order = (TRUOrderImpl) getCartComponent().getCurrent();
		getAddressInputFields().getFirstName();
		
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUConstants.HANDLE_ENSURE_PAYMENT_GROUP))) {
			resetFormExceptions();
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				
				
				
				synchronized (order) {
					
					TRUCSROrderManager orderManager = (TRUCSROrderManager) getCSRAgentTools() .getOrderManager();
					PaymentGroup payGroup = orderManager.ensureInStoreCSCPayment(order);
					((TRUOrderImpl) getOrder()).setEmail(getEmail());
					//if(((TRUOrderImpl) getOrder()).getBillingAddressMap()==null|| ((TRUOrderImpl) getOrder()).getBillingAddressMap().isEmpty()){
					/*((TRUOrderImpl) getOrder()).getBillingAddressMap().put("address1",  getAddressInputFields().getAddress1());
					((TRUOrderImpl) getOrder()).getBillingAddressMap().put("address2",  getAddressInputFields().getAddress2());
					((TRUOrderImpl) getOrder()).getBillingAddressMap().put("city",  getAddressInputFields().getCity());
					((TRUOrderImpl) getOrder()).getBillingAddressMap().put("state",  getAddressInputFields().getState());
					((TRUOrderImpl) getOrder()).getBillingAddressMap().put("postalCode",  getAddressInputFields().getPostalCode());
					((TRUOrderImpl) getOrder()).getBillingAddressMap().put("country",  getAddressInputFields().getCountry());*/
					//}
					
					
					
					@SuppressWarnings("unused")
					String billingAddressNickName = getBillingAddressNickName();
					if(true) {
						ContactInfo billingAddress  = new ContactInfo();
						OrderTools.copyAddress(getAddressInputFields(), billingAddress);
						//Adding the billing address to the credit card. 
						((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
						String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
						((TRUOrderImpl)getOrder()).setBillingAddressNickName(generatedUniqueNickName);
						final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
						final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
						final Address address = hgsg.getShippingAddress();	    
						OrderTools.copyAddress(getAddressInputFields(), address);
						hgsg.setBillingAddress(Boolean.TRUE);
						hgsg.setNickName(generatedUniqueNickName);
						billingAddressNickName = generatedUniqueNickName;
						//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
						
						//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
						getShippingGroupMapContainer().addShippingGroup(generatedUniqueNickName, hgsg);
						setBillingAddressNickName(generatedUniqueNickName);
					} 
					
					
						
						((TRUOrderImpl)getOrder()).setCreditCardType(null);
						if (isLoggingDebug()) {
							logDebug("handleensurebefore:-> " + order.getId());
						}
                      	if (isLoggingDebug()) {
							logDebug("handleensureafter:-> " + payGroup);
						}
						
						@SuppressWarnings("rawtypes")
						Map extraParameters = new HashMap();
						CSRAgentTools csrAgentTools = getCSRAgentTools();
						CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
						extraParameters.put(TRUConstants.PRICE_LIST, csrenvtools.getCurrentPriceList());
						extraParameters.put(TRUConstants.SALE_PRICE_LIST, csrenvtools.getCurrentSalePriceList());
						PricingModelHolder userPricingModels = csrenvtools.getCurrentOrderPricingModelHolder();
						userPricingModels.initializePricingModels();
						getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
								getOrder(),
								userPricingModels,
								getUserLocale(),
								getProfile(),
								extraParameters,
								null); 
						if (payGroup != null) {
							payGroup.setAmount(order.getPriceInfo().getTotal());
						}
						getOrderManager().updateOrder(order);

						if (isLoggingDebug()) {
							logDebug("handleensureafter:-> " + order.getId());
							logDebug("handleensureafter:-> "+ order.getRelationships());
							logDebug("handleensureafter:-> "+ order.getPaymentGroups());
						}
					
				}
			} catch (CommerceException pComExc) {
				if (isLoggingError()) {
					logError("Exception in @Class::TRUCSCBillingInfoFormHandler::@method::handleEnsurePaymentGroup()", pComExc);
				}
			} catch (TransactionDemarcationException transDemEx) {
				throw new ServletException(transDemEx);
			} catch (RunProcessException e) {
				vlogError(e,"RunProcessException in TRUCSRCreateFormHandler.handleNewCreditCard");
			}

			finally {
				try {
					if (tm != null) {
						td.end();
					}
				} catch (TransactionDemarcationException transDemEx) {
					if (isLoggingError()) {
						vlogError(transDemEx,"TransactionDemarcationException occured,OrderId:{0} ProfileId:{1}",order.getId(), order.getProfileId());
					}
				}

				if (rrm != null) {
					rrm.removeRequestEntry(TRUConstants.HANDLE_ENSURE_PAYMENT_GROUP);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUCSCBillingInfoFormHandler.handleEnsurePaymentGroup method");
		}
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),pRequest, pResponse);
	}
	
	/**
	 * This method is used to initialize the rewards number in order from profile.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @return boolean
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 */
	public boolean handleInitRewardNumberInOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUCSCBillingInfoFormHandler.handleInitRewardNumberInOrder method");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		TRUOrderImpl order = (TRUOrderImpl) getCartComponent().getCurrent();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUConstants.HANDLE_REWARD_NO))) {
			Transaction tr = null;
			resetFormExceptions();
			try {
				tr = ensureTransaction();
				synchronized (order) {
					if (!getFormError()) {
						// get the rewards number from profile if any and update to order 
						String rewardNumber = (String) getProfile().getPropertyValue(TRUConstants.REWARD_NO);
						if(StringUtils.isNotBlank(rewardNumber) && StringUtils.isBlank(((TRUOrderImpl)getOrder()).getRewardNumber())){
							((TRUOrderImpl)getOrder()).setRewardNumber(rewardNumber);
						}
						//call the post methodgetOrder()
						getOrderManager().updateOrder(getOrder());
					}
				}
		  } catch (CommerceException comExc) {
			  if(isLoggingError())
			  {
			  logError(
						"Exception in @Class::TRUCSCBillingInfoFormHandler::@method::handleInitRewardNumberInOrder()",
						comExc);
			  }
		} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(TRUConstants.HANDLE_REWARD_NO);
				}
			}
		  }
		if (isLoggingDebug()) {
			logDebug("End of TRUCSCBillingInfoFormHandler.handleInitRewardNumberInOrder method");
		}
		return checkFormRedirect(getPaymentInfoSuccessURL(), getPaymentInfoErrorURL(), pRequest, pResponse);
	}
	
}