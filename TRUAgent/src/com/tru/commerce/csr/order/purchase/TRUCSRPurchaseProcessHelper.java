package com.tru.commerce.csr.order.purchase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.adapter.gsa.ChangeAwareSet;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.order.CSRPurchaseProcessHelper;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.states.OrderStates;
import atg.commerce.states.PaymentGroupStates;
import atg.commerce.states.ShippingGroupStates;
import atg.commerce.util.PipelineErrorHandler;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardInfo;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.purchase.TRUAddCommerceItemInfo;
import com.tru.commerce.payment.processor.TRUCreditCardProcessor;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class extends ATG OOTB CSRPurchaseProcessHelper and used
 * for managing the coupon and updating the order status related operation.
 * 
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRPurchaseProcessHelper extends CSRPurchaseProcessHelper{
	
	  		/**
			 * property to hold salePriceList.
			 */
			public static final String SALE_PRICE_LIST = "salePriceList";

			/**
			 * property to hold priceList.
			 */
			public static final String PRICE_LIST = "priceList";


	/**
	 * This property holds the mCsrAgentTools value.
	 */
	private CSRAgentTools mCsrAgentTools;

	/**
	 * property to hold the mProfileManager.
	 */
	private TRUProfileManager mProfileManager;
	

	/**
	 * property to hold the mClaimableManager.
	 */
	private ClaimableManager mClaimableManager;

	/**
	 * Property to hold PromotionTools.
	 */
	private TRUPromotionTools mPromotionTools;

	/** Propery to hold the TestCardinal *. */
	private boolean mTestCardinal;
	
	/** Propery to hold the CardinalToken number *. */
	private String mCardinalTokenNumber;
	
	/**
	 * @return the csrAgentTools
	 */
	public CSRAgentTools getCsrAgentTools() {
		return mCsrAgentTools;
	}

	/**
	 * @param pCsrAgentTools
	 *            the mCsrAgentTools to set
	 */
	public void setCsrAgentTools(CSRAgentTools pCsrAgentTools) {
		mCsrAgentTools = pCsrAgentTools;
	}
	
	/**
	 * @return the profileManager
	 */
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}

	/**
	 * @param pProfileManager the profileManager to set
	 */
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}

	/**
	 * Checks if is test cardinal.
	 *
	 * @return the testCardinal
	 */
	public boolean isTestCardinal() {
		return mTestCardinal;
	}

	/**
	 * Sets the test cardinal.
	 *
	 * @param pTestCardinal
	 *            the testCardinal to set
	 */
	public void setTestCardinal(boolean pTestCardinal) {
		mTestCardinal = pTestCardinal;
	}

	/**
	 * Gets the cardinal token number.
	 *
	 * @return the cardinalTokenNumber
	 */
	public String getCardinalTokenNumber() {
		return mCardinalTokenNumber;
	}

	/**
	 * Sets the cardinal token number.
	 *
	 * @param pCardinalTokenNumber
	 *            the cardinalTokenNumber to set
	 */
	public void setCardinalTokenNumber(String pCardinalTokenNumber) {
		mCardinalTokenNumber = pCardinalTokenNumber;
	}

	/**
	 * Gets the claimable manager.
	 *
	 * @return the mClaimableManager
	 */
	public ClaimableManager getClaimableManager() {
		return mClaimableManager;
	}

	/**
	 * Sets the claimable manager.
	 *
	 * @param pClaimableManager
	 *            the new claimable manager
	 */
	public void setClaimableManager(ClaimableManager pClaimableManager) {
		mClaimableManager = pClaimableManager;
	}

	/**
	 * This method will set PromotionTools with pPromotionTools.
	 *
	 * @param pPromotionTools
	 *            -Promotion Tools.
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	/**
	 * This property contains PromotionTools object.
	 *
	 * @return PromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;
	
	/**
	 * This property contains TRUConfiguration object.
	 *
	 * @return TRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * This method will set TRUConfiguration with pTRUConfiguration.
	 *
	 * @param pTRUConfiguration
	 *            -TRUConfiguration.
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * This method is for Checking to see if the order, any shipping groups,
	 * or any payment groups are in a "PENDING_MERCHANT_ACTION" state.
	 *
	 * @param pOrder
	 *            the Order
	 *
	 */
	@Override
	public void updateStates(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("TRUCSRPurchaseProcessHelper (updateStates) : Starts");
		}
		OrderStates orderStates = getOrderStates();
		if (pOrder.getState() == orderStates.getStateValue(TRUCSCConstants.PENDING_MERCHANT_ACTION)) {
			getOrderFulfillmentTools().setOrderState(
										pOrder, 
										orderStates.getStateValue(TRUCSCConstants.PROCESSING),
										null,
										new ArrayList());

		}

		if (isLoggingDebug()) {
			logDebug("Resubmit payment groups in PEND");
		}
		PaymentGroupStates paymentGroupStates = getPaymentGroupStates();
		List paymentGroups = pOrder.getPaymentGroups();
		for (Iterator iter = paymentGroups.iterator(); iter.hasNext(); ) {
			PaymentGroup group = (PaymentGroup) iter.next();
			if (group.getState() == paymentGroupStates.getStateValue(TRUCSCConstants.AUTHORIZE_FAILED) ||
					group.getState() == paymentGroupStates.getStateValue(TRUCSCConstants.SETTLE_FAILED)) {
				group.setState(paymentGroupStates.getStateValue(TRUCSCConstants.INITIAL));
			}

		}

		if (isLoggingDebug()) {
			logDebug("SHIPPING GROUPS");
		}
		ShippingGroupStates shippingGroupStates = getShippingGroupStates();
		List shippingGroups = pOrder.getShippingGroups();
		for (Iterator iter = shippingGroups.iterator(); iter.hasNext(); ) {
			ShippingGroup group = (ShippingGroup) iter.next();
			if (group.getState() == shippingGroupStates.getStateValue(TRUCSCConstants.PENDING_MERCHANT_ACTION)) {
				group.setState(shippingGroupStates.getStateValue(TRUCSCConstants.INITIAL));
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUCSRPurchaseProcessHelper (updateStates) : Ends");
		}
	}

	/**
	 * This method for adding the credit card.
	 *
	 *
	 * @param pProfile
	 *            the RepositoryItem
	 * @param pNewCreditCard
	 *            the Map
	 * @param pBillingAddressNickName
	 *            the String
	 * @param pBillingAddress
	 *            the Address
	 * @param pPaymentGroupMapContainer
	 *            the PaymentGroupMapContainer
	 * @param pCopyAddress
	 *            the copyAddress
	 * @throws CommerceException
	 *             the CommerceException
	 * 
	 */
	public void addCreditCard(RepositoryItem pProfile, Map<String,String> pNewCreditCard,
			String pBillingAddressNickName, Address pBillingAddress, 
			PaymentGroupMapContainer pPaymentGroupMapContainer, boolean pCopyAddress) throws CommerceException {
		vlogDebug("START of TRUCSRPurchaseProcessHelper.addCreditCard method");
		PaymentGroupManager pgm = getOrderManager().getPaymentGroupManager();
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();

		
		// Create a new payment group and update properties.
		TRUCreditCard creditCard = (TRUCreditCard) pgm.createPaymentGroup();
		
		try {
			profileTools.copyShallowCreditCardProperties(pNewCreditCard, creditCard);
			creditCard.setCreditCardNumber(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TOKEN));
			creditCard.setCreditCardType(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TYPE));
		} catch (PropertyNotFoundException pnfe) {
			if (isLoggingError()) {
				logError(pnfe);
			}
		}
		creditCard.setBillingAddressNickName(pBillingAddressNickName);
		creditCard.setBillingAddress(pBillingAddress);
		creditCard.setAmount(TRUCSCConstants.DOUBLE_ZERO_FORMAT);
		// get unique nickname
		String paymentGroupName = getNewPaymentGroupName(creditCard, pPaymentGroupMapContainer);
		if (pPaymentGroupMapContainer.getPaymentGroupMap().isEmpty()) {
			pPaymentGroupMapContainer.setDefaultPaymentGroupName(paymentGroupName);
		}else if (paymentGroupName != null && StringUtils.isEmpty(pPaymentGroupMapContainer.getDefaultPaymentGroupName())) {
			pPaymentGroupMapContainer.setDefaultPaymentGroupName(paymentGroupName);
		}
		// Put the new payment group in the container.
		pPaymentGroupMapContainer.addPaymentGroup(paymentGroupName, creditCard);
		boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
		if (!isAnonymousUser && pCopyAddress) {
			profileTools.createProfileCreditCard(pProfile, pNewCreditCard, pBillingAddressNickName);
		}
		vlogDebug("END of TRUCSRPurchaseProcessHelper.addCreditCard method");
	}


	/**
	 * returns unique payment group name
	 *
	 * @param pPaymentGroup paymentGroup
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @return uniqueCreditCardNickname
	 */
	@SuppressWarnings("rawtypes")
	private String getNewPaymentGroupName(CreditCard pPaymentGroup,
			PaymentGroupMapContainer pPaymentGroupMapContainer) {
		vlogDebug("START of TRUCSRPurchaseProcessHelper.getNewPaymentGroupName method");
		if (!(pPaymentGroup instanceof CreditCard)){
			return null;
		}
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();

		Map paymentGroupMap = pPaymentGroupMapContainer.getPaymentGroupMap();
		Collection paymentGroupNicknames = null;
		if (paymentGroupMap != null) {
			paymentGroupNicknames = paymentGroupMap.keySet();
		}
		vlogDebug("END of TRUCSRPurchaseProcessHelper.getNewPaymentGroupName method");
		return profileTools.getUniqueCreditCardNickname(pPaymentGroup, paymentGroupNicknames, null);
	}

	/**
	 * This method is for removing the coupon item.
	 *
	 * @param pCurrentCouponCode
	 *            the current coupon code
	 * @param pProfile
	 *            the profile
	 * @param pUserLocale
	 *            the user locale
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void removeCouponItem(String pCurrentCouponCode, RepositoryItem pProfile, Locale pUserLocale) throws CommerceException {
		if(isLoggingDebug()){
			logDebug("TRUCSRPurchaseProcessHelper (removeCouponItem) : Starts");
		}
		if (pProfile != null) {
			MutableRepositoryItem profile = null;
			TRUPromotionTools promotionTools = getPromotionTools();
			MutableRepository profileMutableRepItem = (MutableRepository) pProfile.getRepository();
			String profileRepId = pProfile.getRepositoryId();
			ClaimableManager claimableManager = getClaimableManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean shouldRollback = true;
			try {
				td.begin(getTransactionManager());

				RepositoryItem currentCoupon = null;
				// Get and remove all coupon's promotions from order
				if (!StringUtils.isEmpty(pCurrentCouponCode)) {
					currentCoupon = claimableManager.claimItem(pCurrentCouponCode);
				}
				ChangeAwareSet promotions = null;
				// get all promotions
				if (currentCoupon != null) {
					promotions = (ChangeAwareSet) currentCoupon.getPropertyValue(claimableManager.getClaimableTools()
							.getPromotionsPropertyName());
				}

				// Ensure profile to be a MutableRepositoryItem
				if (pProfile != null && !(pProfile instanceof MutableRepositoryItem)) {
					profile = profileMutableRepItem.getItemForUpdate(profileRepId, pProfile.getItemDescriptor()
							.getItemDescriptorName());
				}
				if (promotions != null && !promotions.isEmpty()) {
					if (isLoggingDebug()) {
						vlogDebug("Before removing promotions size :{0}, Profile : {1} ", promotions.size(), profileRepId);
					}
					for (Object promotion : promotions) {
						if (profile != null) {
							promotionTools.removeCoupon(profile, (RepositoryItem) promotion, false,currentCoupon);
						} else {
							promotionTools.removeCoupon((MutableRepositoryItem) pProfile, (RepositoryItem) promotion, false,currentCoupon);
						}
					}
					if (isLoggingDebug()) {
						vlogDebug("After removing promotions size :{0} ", promotions.size());
					}
				}

				shouldRollback = false;
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					vlogError("TransactionDemarcationException while removing coupon :{0} ", e);
				}
				throw new CommerceException(e);
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					vlogError("RepositoryException while removing coupon :{0} ", e);
				}
				throw new CommerceException(e);
			} finally {
				// Commit or rollback transaction
				try {
					td.end(shouldRollback);
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						vlogError("TransactionDemarcationException while removing coupon in finally block :{0} ", e);
					}
					throw new CommerceException(e);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUCSRPurchaseProcessHelper (removeCouponItem) : Ends");
		}
	}

	/**
	 * Run process tru validate for checkout chain.
	 *
	 * @param pOrder
	 *            the order
	 * @param pPricingModels
	 *            the pricing models
	 * @param pLocale
	 *            the locale
	 * @param pProfile
	 *            the profile
	 * @param pExtraParameters
	 *            the extra parameters
	 * @throws RunProcessException
	 *             the run process exception
	 * @param pParameters
	 *            - parameters
	 * @param pErrorHandler
	 *            - errorHandler
	 */
	@SuppressWarnings ({"rawtypes"})
	public void runProcessTRUValidateForCheckoutChain(Order pOrder, PricingModelHolder pPricingModels,
			Locale pLocale, RepositoryItem pProfile, Map pParameters, Map pExtraParameters,
			PipelineErrorHandler pErrorHandler)
					throws RunProcessException {
		PipelineResult result =
				runProcess(
						TRUCheckoutConstants.TRU_VALIDATE_FOR_CHECKOUT_CHAIN,
						pOrder,
						pPricingModels,
						pLocale,
						pProfile,
						pParameters,
						pExtraParameters);
		processPipelineErrors(result, pErrorHandler);
	}

	/**
	 * Run process to Reprice the Order.
	 * @param pPricingOperation the pricing operation
	 * @param pOrder the order
	 * @param pPricingModels the pricing models
	 * @param pLocale the locale
	 * @param pProfile the profile
	 * @param pExtraParameters the extra parameters
	 * @param pErrorHandler - errorHandler
	 * @throws RunProcessException the run process exception
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void runProcessRepriceOrder(String pPricingOperation, Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters,
			PipelineErrorHandler pErrorHandler) throws RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRPurchaseProcessHelper::@method::runProcessRepriceOrder() : BEGIN ");
		}
		CSRAgentTools csrAgentTools = getCsrAgentTools();
		CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
		if (pExtraParameters == null) {
			Map extraParameters = new HashMap();
			extraParameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE, Boolean.TRUE);
			extraParameters.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
			extraParameters.put(PRICE_LIST, csrenvtools.getCurrentPriceList());
			extraParameters.put(SALE_PRICE_LIST, csrenvtools.getCurrentSalePriceList());
			super.runProcessRepriceOrder(pPricingOperation, pOrder, pPricingModels, pLocale, pProfile, extraParameters,
					pErrorHandler);
		}
		else{
			pExtraParameters.put(TRUCommerceConstants.UPDATE_SHIP_METHOD_PRICE, Boolean.TRUE);
			pExtraParameters.put(TRUTaxwareConstant.CALCULATE_TAX, Boolean.TRUE);
			pExtraParameters.put(PRICE_LIST, csrenvtools.getCurrentPriceList());
			pExtraParameters.put(SALE_PRICE_LIST, csrenvtools.getCurrentSalePriceList());
			super.runProcessRepriceOrder(pPricingOperation, pOrder, pPricingModels, pLocale, pProfile, pExtraParameters,
					pErrorHandler);
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRPurchaseProcessHelper::@method::runProcessRepriceOrder() : END ");
		}
	}
	
	/**
	 * createCommerceItem.
	 * 
	 * @param pItemInfo
	 *            the item info
	 * @param pCatalogKey
	 *            the catalog key
	 * @param pOrder
	 *            the order
	 * @return the commerce item
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@Override
	protected CommerceItem createCommerceItem(AddCommerceItemInfo pItemInfo, String pCatalogKey, Order pOrder)
			throws CommerceException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createCommerceItem() : BEGIN ");
		}
		CommerceItemManager cimgr = getOrderManager().getCommerceItemManager();
		String siteId = pItemInfo.getSiteId();
		if (StringUtils.isBlank(siteId)) {
			siteId = null;
		}
		CommerceItem ci = cimgr.createCommerceItem(pItemInfo.getCommerceItemType(), pItemInfo.getCatalogRefId(), null,
				pItemInfo.getProductId(), null, pItemInfo.getQuantity(), pCatalogKey, null, siteId, null);
		TRUAddCommerceItemInfo addCommerceItemInfo = null;
		if (pItemInfo instanceof TRUAddCommerceItemInfo) {
			addCommerceItemInfo = (TRUAddCommerceItemInfo) pItemInfo;
			if (ci instanceof TRUGiftWrapCommerceItem) {
				return ci;
			} else if (ci instanceof TRUDonationCommerceItem && addCommerceItemInfo.getDonationAmount() != null) {
				((TRUDonationCommerceItem) ci).setDonationAmount(addCommerceItemInfo.getDonationAmount());
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("Item created with ID ", ci.getId());
		}
		CommerceItem ciFinal = cimgr.addItemToOrder(pOrder, ci);
		if (isLoggingDebug()) {
			vlogDebug("New item :: {0} :: merged into existing item :: {1}", ci.getId(), ciFinal.getId());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUPurchaseProcessHelper::@method::createCommerceItem() : END ");
		}
		return ciFinal;
	}

	/**
	 * Run process tru validate for checkout chain.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pPricingModels
	 *            the pricing models
	 * @param pLocale
	 *            the locale
	 * @param pProfile
	 *            the profile
	 * @param pExtraParameters
	 *            the extra parameters
	 * @throws RunProcessException
	 *             the run process exception
	 * @param pParameters
	 *            - parameters
	 * @param pErrorHandler
	 *            - errorHandler
	 */
	@SuppressWarnings({ "rawtypes" })
	public void runProcessTRUValidationCheckoutChain(Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pParameters, Map pExtraParameters,
			PipelineErrorHandler pErrorHandler) throws RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRPurchaseProcessHelper::@method::runProcessTRUValidateForCheckoutChain() : BEGIN");
		}
		PipelineResult result = runProcess(
				TRUCheckoutConstants.TRU_VALIDATE_FOR_CHECKOUT_CHAIN, pOrder,
				pPricingModels, pLocale, pProfile, pParameters,
				pExtraParameters);
		processPipelineErrors(result, pErrorHandler);
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRPurchaseProcessHelper::@method::runProcessTRUValidateForCheckoutChain() : END");
		}
	}
	
	/**
	 * This method invokes the Zero Authorization for the credit card
	 * verification.
	 * 
	 * @param pAmount
	 *            amount
	 * @param pSiteId
	 *            siteId
	 * @param pCreditCardMap
	 *            creditCardMap
	 * @param pAddressFields
	 *            addressFields
	 * @return TRUCreditCardStatus - TRUCreditCardStatus
	 * @param pMerchantRefNumber
	 *            - merchantRefNumber
	 */
	public TRUCreditCardStatus verifyZeroAuthorization(Double pAmount, String pSiteId, Map<String, String> pCreditCardMap,
			ContactInfo pAddressFields, String pMerchantRefNumber) {
		if (isLoggingDebug()) {
			vlogDebug("Start of TRUPurchaseProcessHelper/verifyZeroAuthorization method");
		}

		if (pSiteId != null && pCreditCardMap != null && pAddressFields != null) {

			TRUCreditCardInfo cardInfo = new TRUCreditCardInfo();

			cardInfo.setMerchantRefNumber(pMerchantRefNumber);

			//cardInfo.setCurrencyCode(getConfiguration().getCurrencyCodesString().get(pSiteId));

			TRUPropertyManager propertyManager = ((TRUOrderManager) getOrderManager()).getPropertyManager();

			cardInfo.setCardHolderName(pCreditCardMap.get(propertyManager.getNameOnCardPropertyName()));

			cardInfo.setCreditCardNumber(pCreditCardMap.get(propertyManager.getCreditCardTokenPropertyName()));

			cardInfo.setTokenType(getTRUConfiguration().getTokenType().get(pSiteId));
			
			cardInfo.setSiteId(pSiteId);

			String creditCardType = null;

			if (!StringUtils.isBlank(pCreditCardMap.get(propertyManager.getCreditCardTypePropertyName()))) {
				creditCardType = pCreditCardMap.get(propertyManager.getCreditCardTypePropertyName());
			} else {
				creditCardType = ((TRUProfileTools) ((TRUOrderTools) ((TRUOrderManager) getOrderManager()).getOrderTools())
						.getProfileTools()).getCreditCardTypeFromRepository(
						pCreditCardMap.get(propertyManager.getCreditCardNumberPropertyName()),
						pCreditCardMap.get(TRUConstants.CARD_LENGTH));
			}

			cardInfo.setCreditCardType(getTRUConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType));

			cardInfo.setExpirationYear(pCreditCardMap.get(propertyManager.getCreditCardExpirationYearPropertyName()));
			
			cardInfo.setExpirationMonth(pCreditCardMap.get(propertyManager.getCreditCardExpirationMonthPropertyName()));

			cardInfo.setCvv(pCreditCardMap.get(propertyManager.getCreditCardVerificationNumberPropertyName()));

			cardInfo.setBillingAddress(pAddressFields);

			/*if (getConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType)
					.equalsIgnoreCase(TRUCheckoutConstants.RUS_COB_MASTER_CARD)
					|| getConfiguration().getCreditCardTypeNamesForIntegration().get(creditCardType)
							.equalsIgnoreCase(TRUCheckoutConstants.RUS_PRIVATE_LABEL_CARD)) {
				cardInfo.setAmount(TRUConstants._1);
			} else {
				cardInfo.setAmount(TRUConstants._0);
			}*/

			return ((TRUCreditCardProcessor) ((TRUOrderManager) getOrderManager()).getPaymentManager().getCreditCardProcessor())
					.zeroAuthorization(cardInfo);
		}
		if (isLoggingDebug()) {
			vlogDebug("End of TRUPurchaseProcessHelper/verifyZeroAuthorization method");
		}
		return null;
	}
}