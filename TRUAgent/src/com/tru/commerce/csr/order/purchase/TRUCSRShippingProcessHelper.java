/**
 * 
 */
package com.tru.commerce.csr.order.purchase;

import java.sql.Timestamp;
import java.util.Date;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.core.util.Address;
import atg.repository.RepositoryItem;

import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.storelocator.tol.PARALStoreLocatorTools;

/**
 * Shipping Process Helper to add CSC specific functionalities.
 * @author PA
 *
 */
public class TRUCSRShippingProcessHelper extends TRUShippingProcessHelper{
		
	/** Holds the mLocationTools. */
	private PARALStoreLocatorTools mLocationTools;
	
	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools
	 */
	public PARALStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set
	 */
	public void setLocationTools(PARALStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}
	
	
	/**
	 * @param pLocationId for location id
	 * @return warehouse locationid
	 */
	public Double getStoreItemWareHouseLocationId(String pLocationId){
		if(isLoggingDebug()){
			logDebug("Start : TRUCSRShippingProcessHelper Class - getStoreItemWareHouseLocationId method to "
					+ "get the warehouse location id for the location : "+pLocationId);
		}
		TRUStoreLocatorTools locatorTools = (TRUStoreLocatorTools)getLocationTools();
		RepositoryItem storeItem=locatorTools.getStoreItem(pLocationId);
		Double storeItemWareHouseLocationId = (Double)storeItem.getPropertyValue(
				locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
		
		if(isLoggingDebug()){
			logDebug("End : TRUCSRShippingProcessHelper Class - getStoreItemWareHouseLocationId method to "
					+ "get the warehouse location id for the location : "+pLocationId);
		}
		return storeItemWareHouseLocationId;
	}
	
	/**
	 * Saves address to address book.
	 * 
	 * @param pAddress
	 *            - address Address to save.
	 * @param pNickName
	 *            - nickname - Nickname for the address being saved.
	 * @param pProfile
	 *            - the profile.
	 * 
	 * @throws CommerceException
	 *             indicates that a severe error occurred while performing a
	 *             commerce operation.
	 */
	public void saveAddressToAddressBook(Address pAddress, String pNickName, RepositoryItem pProfile) throws CommerceException {
		getPurchaseProcessHelper().getOrderTools().saveAddressToAddressBook(pProfile, pAddress, pNickName);
	}
	
	
	/**
	 * <p>
	 * Creates a new shipping group and adds it to the shipping group map
	 * container.
	 * </p>
	 * <p>
	 * Optionally saves the shipping group address to the profile based on the
	 * saveShippingAddress option.
	 * </p>
	 *
	 * @param pProfile - shopper profile.
	 * @param pSaveShippingAddress - save shipping address
	 * @param pNewShipToAddressName - Address nickname to use for the address being added.
	 * @param pAddress - Address to add.
	 * @param pShippingGroupMapContainer - map of all shipping groups for the profile.
	 * @return the TRUHardgoodShippingGroup - hgsg 
	 * @throws CommerceException indicates that a severe error occurred while performing a commerce operation.
	 */
	public TRUHardgoodShippingGroup addShippingAddress(RepositoryItem pProfile,
			String pNewShipToAddressName, Address pAddress,
			ShippingGroupMapContainer pShippingGroupMapContainer,boolean pSaveShippingAddress) throws CommerceException {
		final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
		final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) pShippingGroupMapContainer;
		
	    // Create a new shipping group with the new address.
		final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
		final Address address = hgsg.getShippingAddress();	    
	    OrderTools.copyAddress(pAddress, address);
	    
	    hgsg.setNickName(pNewShipToAddressName);
	    hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
	    // Put the new shipping group in the container.
	    pShippingGroupMapContainer.addShippingGroup(pNewShipToAddressName, hgsg);
	    
	    //Set Default when first address added
	    if (shippingGroupMapContainer.getHardGoodShipGroupCount()==TRUConstants.ONE) {
	    	shippingGroupMapContainer.setDefaultShippingGroupName(pNewShipToAddressName);
	    }
	    
	    final TRUProfileTools profileTools = (TRUProfileTools) getPurchaseProcessHelper().getOrderTools().getProfileTools();
	    final boolean isAnonymousUser = profileTools.isAnonymousUser(pProfile);
	    if (!isAnonymousUser && pSaveShippingAddress) {
	    	saveAddressToAddressBook(pAddress, pNewShipToAddressName, pProfile);	    	 	
	    }
	    return hgsg;
	}
		 
}
