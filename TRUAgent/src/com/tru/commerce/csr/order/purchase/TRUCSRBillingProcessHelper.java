/**
 * 
 */
package com.tru.commerce.csr.order.purchase;



import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.CreditCard;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;


/**
 * This custom class extended the TRUCSRPurchaseProcessHelper to implement the TRU business requirement.
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUCSRBillingProcessHelper extends TRUCSRPurchaseProcessHelper {

	/** property to hold api key. */
	private String mApiKey;

	/** property to hold ProfileManager. */
	private TRUProfileManager mProfileManager;

	/** Property to hold commerce property manager. */
	private TRUPropertyManager mPropertyManager;

	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return mProfileManager profile manager
	 */
	@Override
	public TRUProfileManager getProfileManager() {
		return mProfileManager;
	}
	/**
	 * @param pProfileManager parameter
	 */
	@Override
	public void setProfileManager(TRUProfileManager pProfileManager) {
		mProfileManager = pProfileManager;
	}

	/**
	 * <p>adds new credit card to the container and also to the profile for logged in user.</p>
	 * @param pProfile profile
	 * @param pNewCreditCard newCreditCard
	 * @param pBillingAddressNickName billingAddressNickName
	 * @param pBillingAddress billingAddress
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @throws CommerceException CommerceException
	 */
	@Override
	public void addCreditCard(RepositoryItem pProfile, Map<String,String> pNewCreditCard,
			String pBillingAddressNickName, Address pBillingAddress, PaymentGroupMapContainer pPaymentGroupMapContainer,boolean pCopyToProfile) throws CommerceException {
		vlogDebug("START of TRUBillingProcessHelper.addCreditCard method");
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		// Create a new payment group and update properties.
		TRUCreditCard creditCard = (TRUCreditCard) getOrderManager().getPaymentGroupManager().createPaymentGroup();
		try {
			profileTools.copyShallowCreditCardProperties(pNewCreditCard, creditCard);
			creditCard.setCreditCardNumber(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TOKEN));
			creditCard.setCreditCardType(pNewCreditCard.get(TRUConstants.CREDIT_CARD_TYPE));
		} catch (PropertyNotFoundException pnfe) {
			if (isLoggingError()) {
				logError(pnfe);
			}
		}
		creditCard.setBillingAddressNickName(pBillingAddressNickName);
		creditCard.setBillingAddress(pBillingAddress);
		// get unique nickname
		String paymentGroupName = getNewPaymentGroupName(creditCard, pPaymentGroupMapContainer);
		if (pPaymentGroupMapContainer.getPaymentGroupMap().isEmpty()) {
			pPaymentGroupMapContainer.setDefaultPaymentGroupName(paymentGroupName);
		}else if(paymentGroupName != null && StringUtils.isEmpty(pPaymentGroupMapContainer.getDefaultPaymentGroupName())){
			pPaymentGroupMapContainer.setDefaultPaymentGroupName(paymentGroupName);
		}
		// Put the new payment group in the container.
		pPaymentGroupMapContainer.addPaymentGroup(paymentGroupName, creditCard);
		if (!profileTools.isAnonymousUser(pProfile)/* && pCopyToProfile*/) {
			profileTools.createProfileCreditCard(pProfile, pNewCreditCard, pBillingAddressNickName);
		}
		vlogDebug("END of TRUBillingProcessHelper.addCreditCard method");
	}

	/**
	 * <p>edit credit card to the container and also to the profile for logged in user.</p>
	 * @param pProfile profile
	 * @param pEditCreditCard editCreditCard
	 * @param pCreditCardName creditCardName
	 * @param pBillingAddressNickName billingAddressNickName
	 * @param pBillingAddress billingAddress
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @throws CommerceException CommerceException
	 */
	public void editCreditCard(RepositoryItem pProfile, Map<String,String> pEditCreditCard, String pCreditCardName,
			String pBillingAddressNickName, Address pBillingAddress, PaymentGroupMapContainer pPaymentGroupMapContainer)
					throws CommerceException {
		vlogDebug("START of TRUBillingProcessHelper.editCreditCard method");
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		if (StringUtils.isNotBlank(pCreditCardName)) {
			TRUCreditCard creditCard = (TRUCreditCard) pPaymentGroupMapContainer.getPaymentGroup(pCreditCardName);
			if (creditCard != null) {
				creditCard.setBillingAddressNickName(pBillingAddressNickName);
				creditCard.setBillingAddress(pBillingAddress);

				pEditCreditCard.put(getPropertyManager().getCreditCardTypePropertyName(), creditCard.getCreditCardType());
				try {
					profileTools.copyShallowCreditCardProperties(pEditCreditCard, creditCard);
				} catch (PropertyNotFoundException pnfe) {
					if (isLoggingError()) {
						logError(pnfe);
					}
				}
			}
		}
		if (!profileTools.isAnonymousUser(pProfile)) {
			profileTools.updateProfileCreditCard(pProfile, pCreditCardName, pEditCreditCard, pBillingAddressNickName);
		}
		vlogDebug("END of TRUBillingProcessHelper.editCreditCard method");
	}

	/**
	 * This method is used to get the unique payment group name.
	 * @param pPaymentGroup paymentGroup
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @return uniqueCreditCardNickname
	 */
	@SuppressWarnings("rawtypes")
	private String getNewPaymentGroupName(CreditCard pPaymentGroup,
			PaymentGroupMapContainer pPaymentGroupMapContainer) {
		vlogDebug("START of TRUBillingProcessHelper.getNewPaymentGroupName method");
		if (!(pPaymentGroup instanceof CreditCard)){
			return null;
		}
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		Map paymentGroupMap = pPaymentGroupMapContainer.getPaymentGroupMap();
		Collection paymentGroupNicknames = null;
		if (paymentGroupMap != null) {
			paymentGroupNicknames = paymentGroupMap.keySet();
		}
		return profileTools.getUniqueCreditCardNickname(pPaymentGroup, paymentGroupNicknames, null);
	}

	/**
	 * <p>removes credit card from the container and also from the profile for logged in user.</p>
	 * @param pProfile profile
	 * @param pCreditCardName creditCardName
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @throws RepositoryException RepositoryException
	 */
	public void setCreditCardAsDefault(RepositoryItem pProfile, String pCreditCardName,
			PaymentGroupMapContainer pPaymentGroupMapContainer) throws RepositoryException {
		vlogDebug("START of TRUBillingProcessHelper.setCreditCardAsDefault method");
		pPaymentGroupMapContainer.setDefaultPaymentGroupName(pCreditCardName);

		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		// set default card to profile
		if (!profileTools.isAnonymousUser(pProfile)) {
			//profileTools.setDefaultCreditCard(pProfile, pCreditCardName);
		}
		vlogDebug("END of TRUBillingProcessHelper.setCreditCardAsDefault method");
	}

	/**
	 * <p>removes credit card from the container and also from the profile for logged in user.</p>
	 * @param pProfile profile
	 * @param pRemoveCreditCardName removeCreditCardName
	 * @param pPaymentGroupMapContainer paymentGroupMapContainer
	 * @throws RepositoryException RepositoryException
	 */
	public void removeCreditCard(RepositoryItem pProfile, String pRemoveCreditCardName,
			PaymentGroupMapContainer pPaymentGroupMapContainer) throws RepositoryException {
		vlogDebug("START of TRUBillingProcessHelper.removeCreditCard method");
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();

		// remove from container
		if ((CreditCard) pPaymentGroupMapContainer.getPaymentGroup(pRemoveCreditCardName) != null) {
			if (pRemoveCreditCardName.equalsIgnoreCase(pPaymentGroupMapContainer.getDefaultPaymentGroupName())) {
				pPaymentGroupMapContainer.setDefaultPaymentGroupName(null);
			}
			pPaymentGroupMapContainer.removePaymentGroup(pRemoveCreditCardName);
		}

		// remove from profile
		if (!profileTools.isAnonymousUser(pProfile)) {
			profileTools.removeProfileCreditCard(pProfile, pRemoveCreditCardName, null);
		}
		vlogDebug("END of TRUBillingProcessHelper.removeCreditCard method");
	}

	/**
	 * Updates the order credit card with the input map values and billing address ContactInfo object.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pCreditCardInputMap
	 *            the credit card input map
	 * @param pBillingAddress
	 *            the billing address
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void updateCreditCard(Order pOrder, Map<String, String> pCreditCardInputMap, ContactInfo pBillingAddress)
			throws CommerceException {
		vlogDebug("START of TRUCSRBillingProcessHelper.updateCreditCard method");
		vlogDebug("INPUT PARAMS OF updateOrderWithPayment() pOrder: {0} pCreditCardInputMap: {1} pBillingAddress: {2}", pOrder,
				pCreditCardInputMap, pBillingAddress);
		TRUCreditCard creditCard = (TRUCreditCard)((TRUOrderManager) getOrderManager()).getCreditCard(pOrder);

		if (creditCard != null) {
			vlogDebug("Updating the creditCard details for order");

			// TODO Start : This line needs to be change after cardinal commerce starts working.
			if (!StringUtils.isBlank(pCreditCardInputMap.get(getPropertyManager().getCreditCardTokenPropertyName()))) {
				creditCard.setCreditCardNumber(pCreditCardInputMap.get(getPropertyManager().getCreditCardTokenPropertyName()));
			} else {
				if (isTestCardinal()) {
					creditCard.setCreditCardNumber(getCardinalTokenNumber());
				} else {
					creditCard.setCreditCardNumber(pCreditCardInputMap.get(getPropertyManager().getCreditCardNumberPropertyName()));
				}
			}
			// TODO Start : This line needs to be remove after cardinal commerce starts working.

			creditCard.setCreditCardType(pCreditCardInputMap.get(getPropertyManager().getCreditCardTypePropertyName()));
			creditCard.setExpirationMonth(pCreditCardInputMap.get(getPropertyManager().getCreditCardExpirationMonthPropertyName()));
			creditCard.setExpirationYear(pCreditCardInputMap.get(getPropertyManager().getCreditCardExpirationYearPropertyName()));
			creditCard.setCardVerificationNumber(pCreditCardInputMap.get(getPropertyManager().getCreditCardVerificationNumberPropertyName()));
			creditCard.setNameOnCard(pCreditCardInputMap.get(getPropertyManager().getNameOnCardPropertyName()));
			// Updating the credit card addresses.
			((TRUOrderTools)((TRUOrderManager) getOrderManager()).getOrderTools()).updateCreditCardAddress(pBillingAddress, creditCard);

			((TRUOrderManager) getOrderManager()).updateCardDataInOrder(pOrder, creditCard.getCreditCardType());
			vlogDebug("TRUBillingProcessHelper.updateCreditCard :: " + 
						"credit card from the order updated with the input map and billing address provided.");
		} else {
			if(isLoggingError()) {
				logError("Exception in @Class::TRUBillingProcessHelper::@method::updateCreditCard(): Credit Card is NULL");
			}
		}

		if (isLoggingDebug()) {
			logDebug("End of updateCreditCard method");
		}
	}

	/**
	 * Updates the order paymentGroup credit card object with the given credit card.
	 * @param pOrder the order
	 * @param pCreditCard the credit card
	 * @return orderCreditCard the CreditCard
	 * @throws CommerceException the commerce exception
	 */
	public CreditCard updateOrderWithCreditCard(Order pOrder, CreditCard pCreditCard) throws CommerceException {
		vlogDebug("START of TRUCSRBillingProcessHelper.updateOrderWithCreditCard method");
		vlogDebug("INPUT PARAMS OF updateOrderWithCreditCard() pOrder: {0} pCreditCard: {1}",
				pOrder, pCreditCard);
		CreditCard orderCreditCard = null;
		if(pOrder != null && pCreditCard != null) {
			orderCreditCard = ((TRUOrderManager) getOrderManager()).getCreditCard(pOrder);
			if (orderCreditCard != null) {
				copyCardInfo(pCreditCard, orderCreditCard);
				//Update card type in profile
				((TRUOrderManager) getOrderManager()).updateCardDataInOrder(pOrder, orderCreditCard.getCreditCardType());
				vlogDebug("TRUBillingProcessHelper.updateOrderWithCreditCard : End");
			} else {
				if(isLoggingError()) {
					logError("Exception in @Class::TRUBillingProcessHelper::@method::updateOrderWithCreditCard(): Credit Card is NULL");
				}
			}
		}
		else{
			vlogDebug("TRUBillingProcessHelper.updateOrderWithCreditCard : CreditCard found empty");
		}

		return orderCreditCard;
	}

	/**
	 * Copies the property values of the RepositoryItem of type creditCard to CreditCard object.
	 * @param pSourceCreditCard the credit card item
	 * @param pTargetCreditCard the credit card object
	 * @return CreditCard
	 * @throws CommerceException
	 *             the CommerceException
	 */
	public CreditCard copyCardInfo(CreditCard pSourceCreditCard, CreditCard pTargetCreditCard) throws CommerceException {
		vlogDebug("START of TRUCSRBillingProcessHelper.copyCardInfo method");
		vlogDebug("INPUT PARAMS OF copyCardInfo() pSourceCreditCard: {0} pTargetCreditCard: {1}",
				pSourceCreditCard, pTargetCreditCard);
		if(pSourceCreditCard != null && pTargetCreditCard != null) {
			TRUCreditCard sourceCreditCard = (TRUCreditCard) pSourceCreditCard;
			TRUCreditCard targetCreditCard = (TRUCreditCard) pTargetCreditCard;

			targetCreditCard.setCreditCardNumber(sourceCreditCard.getCreditCardNumber());
			targetCreditCard.setCreditCardType(sourceCreditCard.getCreditCardType());
			targetCreditCard.setExpirationMonth(sourceCreditCard.getExpirationMonth());
			targetCreditCard.setExpirationYear(sourceCreditCard.getExpirationYear());
			//targetCreditCard.setCardVerficationNumber(sourceCreditCard.getCardVerficationNumber());
			targetCreditCard.setNameOnCard(sourceCreditCard.getNameOnCard());
			targetCreditCard.setBillingAddressNickName(sourceCreditCard.getBillingAddressNickName());

			if (targetCreditCard.getBillingAddress() != null &&
					sourceCreditCard.getBillingAddress() != null) {
				OrderTools.copyAddress(sourceCreditCard.getBillingAddress(), targetCreditCard.getBillingAddress());
			} else {
				targetCreditCard.setBillingAddress(sourceCreditCard.getBillingAddress());
			}

			vlogDebug("END of TRUCSRBillingProcessHelper.copyCardInfo : CreditCard set with the values");
		}
		vlogDebug("End of copyCardInfo method and returns pCreditCardObject: {0} ", pTargetCreditCard);
		return pTargetCreditCard;
	}

	/**
	 * Ensures payment group in order with the given payment group type.
	 * @param pOrder the order
	 * @param pPaymentGroupType the payment group type
	 * @throws CommerceException
	 *             the CommerceException
	 */
	public void ensurePaymentGroup(Order pOrder, String pPaymentGroupType) throws CommerceException {
		vlogDebug("START of TRUCSRBillingProcessHelper.ensurePaymentGroup method");
		TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();

		if(TRUCheckoutConstants.IN_STORE_PAYMENT.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensureInStorePayment(pOrder);
			((TRUOrderImpl)pOrder).setCreditCardType(null);
		} else if(TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensureCreditCard(pOrder);
		} else if(TRUCheckoutConstants.PAYPAL_CARD.equalsIgnoreCase(pPaymentGroupType)) {
			orderManager.ensurePayPal(pOrder);
		}
		vlogDebug("END of TRUCSRBillingProcessHelper.ensurePaymentGroup method");
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return mApiKey;
	}

	/**
	 * @param pApiKey the apiKey to set
	 */
	public void setApiKey(String pApiKey) {
		mApiKey = pApiKey;
	}

	/**
	 * Validate order remaining amount.
	 * @param pOrder the order
	 * @param pActionType the action type
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public boolean validateOrderRemainingAmount(Order pOrder, String pActionType) throws ServletException,
	IOException {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			logDebug("START TRUBillingProcessHelper:: validateOrderRemainingAmount method");
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		boolean isCreditCardExist = false;
		boolean isPayPalExist = false;
		boolean isPayInStoreExist = false;
		TRUCreditCard creditCardPG = null;
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		PaymentGroupManager pgm = getOrderManager().getPaymentGroupManager();
		for (PaymentGroup pg : listOfPaymentGroup) {
			if(pg instanceof TRUCreditCard){
				creditCardPG = (TRUCreditCard)pg;
				isCreditCardExist = true;
				break;
			}
			if(pg instanceof TRUPayPal){
				isPayPalExist = true;
				break;
			}
			if(pg instanceof InStorePayment){
				isPayInStoreExist = true;
				break;
			}
		}
		double orderRemaningAmount = ((TRUOrderManager)getOrderManager()).getOrderRemaningAmount(order);
		if (!(isPayPalExist || isCreditCardExist || isPayInStoreExist)) {
			if (orderRemaningAmount >  TRUConstants.DOUBLE_ZERO) {
				return false;
			} else if(TRUCheckoutConstants.COMMIT_ORDER.equalsIgnoreCase(pActionType) && isCreditCardExist) {
				try {
					//If the order remaining amount is not left and amount sufficient with another pg then removing the CreditCard pg.
					pgm.removePaymentGroupFromOrder(order, creditCardPG.getId());
				} catch (CommerceException comExp) {
					if(isLoggingError()){
						vlogError(" CommerceException occured at KLSCommitOrderFormHandler.preCommitOrder(),OrderId:{0} profileId:{1} CommerceException:{2}", order.getId(),order.getProfileId(),comExp);
					}
				}
			}
		}
		if (orderRemaningAmount ==  TRUConstants.DOUBLE_ZERO && TRUCSCConstants.COMMIT_ORDER.equalsIgnoreCase(pActionType) && isCreditCardExist) {
			try {
				//If the order remaining amount is not left and amount sufficient with another pg then removing the CreditCard pg.
				pgm.removePaymentGroupFromOrder(order, creditCardPG.getId());
			} catch (CommerceException comExp) {
				if(isLoggingError()){
					vlogError(" CommerceException occured at KLSCommitOrderFormHandler.preCommitOrder(),OrderId:{0} profileId:{1} CommerceException:{2}", order.getId(),order.getProfileId(),comExp);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END TRUBillingProcessHelper:: validateOrderRemainingAmount method");
		}
		return true;
	}
}
