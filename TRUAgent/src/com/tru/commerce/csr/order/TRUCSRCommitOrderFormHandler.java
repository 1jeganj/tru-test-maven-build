/*
 * 
 */

package com.tru.commerce.csr.order;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.csr.order.CSRCommitOrderFormHandler;
import atg.commerce.csr.order.CSROrderHolder;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemRelationship;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.promotion.TRUPromotionTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUCSROrderTools;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.purchase.TRUBillingProcessHelper;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.common.cml.ValidationExceptions;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.integrations.common.TRUEpslonBppInfoVO;
import com.tru.messaging.TRUEpslonOrderAcknowledgeSource;
import com.tru.regex.EmailPasswordValidator;
import com.tru.userprofiling.TRUProfileManager;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.validator.fhl.IValidator;


/**
 * This class extends ATG OOTB CSRCommitOrderFormHandler and used
 * for managing the TRU Specific check out functionalities.
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
	
public class TRUCSRCommitOrderFormHandler extends CSRCommitOrderFormHandler {
	
	/** The entered by. */
	private String mEnteredBy; 
	
	
	/** The Constant TEST_EMAIL. */
	public static final String TEST_EMAIL = " ";

	/**
	 * property to holds reference for mEpslonOrderAcknowledgeSource.
	 */
	private TRUEpslonOrderAcknowledgeSource mEpslonOrderAcknowledgeSource;

	/**
	 * property to holds reference for mEmail.
	 */
	private String mEmail;
	
	/** The Device id. */
	public String mDeviceID;

	/** property to hold reward number of profile. */
	private String mRewardNumber;

	/**
	 * property to holds reference for mTRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * property to hold TRUPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/** property to hold ValidationException. */
	private ValidationExceptions mVe = new ValidationExceptions();

	/** property to hold the mErrorHandler. */
	private IErrorHandler mErrorHandler;

	private CSROrderHolder mCartComponent;

	/** property to hold Reward Number Valid flag. */
	private String mRewardNumberValid;

	/** The Payment group type. */
	private String mPaymentGroupType;

	/** property to hold OrderSetWithDefault. */
	private boolean mOrderSetWithDefault;

	/** property to hold the mValidator. */
	private IValidator mValidator;

	/** map to hold credit card info. */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();

	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();

	/** Property to hold if RUS radio is selected. */
	private boolean mRUSCard;

	/** property to hold BillingAddressNickName. */
	private String mBillingAddressNickName;

	/** property: Reference to the BillingProcessHelper component. */
	private TRUBillingProcessHelper mBillingHelper;

	/** Property to hold mMoveToConfirmationChainId. */
	private String mMoveToConfirmationChainId;

	private String mValidateShippingGroupsChainId;

	/** property: Reference to the ShippingProcessHelper component . */
	private TRUShippingProcessHelper mShippingHelper;

	/** property to hold mCreditCardNickName. */
	private String mCreditCardNickName;

	/** Property to hold Selected Billing Address. */
	private String mSelectedCardBillingAddress;

	private ContactInfo mAddress = new ContactInfo();
	
	/** Property to holds mEmailPasswordValidator.*/
	private EmailPasswordValidator mEmailPasswordValidator;
	
	/** property to hold user session component. */
	private TRUUserSession mUserSession;
	
	/** Holds reference for PromotionTools. */
	private TRUPromotionTools mPromotionTools;
	/** 
	 * @return the emailPasswordValidator */
	public EmailPasswordValidator getEmailPasswordValidator() {
		return mEmailPasswordValidator;
	}
	
	/**
	 * @param pEmailPasswordValidator the emailPasswordValidator to set. */
	public void setEmailPasswordValidator(EmailPasswordValidator pEmailPasswordValidator) {
		this.mEmailPasswordValidator = pEmailPasswordValidator;
	}

	/**
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * @param pAddress
	 *            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}

	/**
	 * @return mCreditCardNickName credit card nick name
	 */
	public String getCreditCardNickName() {
		return mCreditCardNickName;
	}

	/**
	 * @param pCreditCardNickName
	 *            credit card nick name
	 */
	public void setCreditCardNickName(String pCreditCardNickName) {
		mCreditCardNickName = pCreditCardNickName;
	}

	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set.
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * @return the validateShippingGroupsChainId.
	 */
	public String getValidateShippingGroupsChainId() {
		return mValidateShippingGroupsChainId;
	}

	/**
	 * @param pValidateShippingGroupsChainId
	 *            the validateShippingGroupsChainId to set.
	 */
	public void setValidateShippingGroupsChainId(
			String pValidateShippingGroupsChainId) {
		mValidateShippingGroupsChainId = pValidateShippingGroupsChainId;
	}

	/**
	 * This method gets moveToConfirmationChainId.
	 * 
	 * @return the moveToConfirmationChainId
	 */
	public String getMoveToConfirmationChainId() {
		return mMoveToConfirmationChainId;
	}

	/**
	 * This method sets moveToConfirmationChainId.
	 * 
	 * @param pMoveToConfirmationChainId
	 *            the moveToConfirmationChainId to set
	 */
	public void setMoveToConfirmationChainId(String pMoveToConfirmationChainId) {
		mMoveToConfirmationChainId = pMoveToConfirmationChainId;
	}

	/**
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * @param pBillingHelper
	 *            the billingHelper to set.
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}

	/**
	 * @return mBillingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * @return the selectedCardBillingAddress
	 */
	public String getSelectedCardBillingAddress() {
		return mSelectedCardBillingAddress;
	}

	/**
	 * @param pSelectedCardBillingAddress
	 *            the selectedCardBillingAddress to set
	 */
	public void setSelectedCardBillingAddress(String pSelectedCardBillingAddress) {
		mSelectedCardBillingAddress = pSelectedCardBillingAddress;
	}

	/**
	 * @param pBillingAddressNickName
	 *            parameter
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}

	/**
	 * @return the mRUSCard
	 */
	public boolean isRUSCard() {
		return mRUSCard;
	}

	/**
	 * @param pRUSCard
	 *            the mRUSCard to set.
	 */
	public void setRUSCard(boolean pRUSCard) {
		mRUSCard = pRUSCard;
	}

	/**
	 * Sets property AddressValue map.
	 * 
	 * @param pAddressValue
	 *            addressValue
	 **/
	public void setAddressInputFields(ContactInfo pAddressValue) {
		mAddressInputFields = pAddressValue;
	}

	/**
	 * @return This is a map that stores the value of address properties.
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * @param pCreditCardInfoMap
	 *            the creditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * @return the IValidator
	 */
	public IValidator getValidator() {
		return mValidator;
	}

	/**
	 * @param pValidator
	 *            the IValidator to set
	 */
	public void setValidator(IValidator pValidator) {
		mValidator = pValidator;
	}

	/**
	 * @return mOrderSetWithDefault order set with default property
	 */
	public boolean isOrderSetWithDefault() {
		return mOrderSetWithDefault;
	}

	/**
	 * @param pOrderSetWithDefault
	 *            parameter
	 */
	public void setOrderSetWithDefault(boolean pOrderSetWithDefault) {
		mOrderSetWithDefault = pOrderSetWithDefault;
	}

	/**
	 * @return the paymentGroupType
	 */
	public String getPaymentGroupType() {
		return mPaymentGroupType;
	}

	/**
	 * @param pPaymentGroupType
	 *            the paymentGroupType to set.
	 */
	public void setPaymentGroupType(String pPaymentGroupType) {
		mPaymentGroupType = pPaymentGroupType;
	}

	/**
	 * @return the mRewardNumberValid
	 */
	public String getRewardNumberValid() {
		return mRewardNumberValid;
	}

	/**
	 * @param pRewardNumberValid
	 *            the rewardNumberValid to set.
	 */
	public void setRewardNumberValid(String pRewardNumberValid) {
		mRewardNumberValid = pRewardNumberValid;
	}

	/**
	 * @return the cartComponent
	 */
	public CSROrderHolder getCartComponent() {
		return mCartComponent;
	}

	/**
	 * @param pCartComponent
	 *            the cartComponent to set
	 */
	public void setCartComponent(CSROrderHolder pCartComponent) {
		mCartComponent = pCartComponent;
	}

	/**
	 * @return the IErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * @param pErrorHandler
	 *            the IErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * @return the mRewardNumber
	 */
	public String getRewardNumber() {
		return mRewardNumber;
	}

	/**
	 * @param pRewardNumber
	 *            the mRewardNumber to set
	 */
	public void setRewardNumber(String pRewardNumber) {
		mRewardNumber = pRewardNumber;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager
	 *            the mPropertyManager to set.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mEmail
	 */

	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param pEmail
	 *            the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * This method is used to get epslonOrderAcknowledgeSource.
	 * 
	 * @return epslonOrderAcknowledgeSource TRUEpslonOrderAcknowledgeSource
	 */
	public TRUEpslonOrderAcknowledgeSource getEpslonOrderAcknowledgeSource() {
		return mEpslonOrderAcknowledgeSource;
	}

	/**
	 * This method is used to set epslonOrderAcknowledgeSource.
	 *
	 * @param pEpslonOrderAcknowledgeSource
	 *            TRUEpslonOrderAcknowledgeSource
	 */
	public void setEpslonOrderAcknowledgeSource(
			TRUEpslonOrderAcknowledgeSource pEpslonOrderAcknowledgeSource) {
		mEpslonOrderAcknowledgeSource = pEpslonOrderAcknowledgeSource;
	}

	/**
	 * This method is used to get tRUConfiguration.
	 * 
	 * @return tRUConfiguration TRUConfiguration
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * This method is used to set tRUConfiguration.
	 *
	 * @param pTRUConfiguration
	 *            TRUConfiguration
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}


	/**
	 * @return the mEnteredBy
	 */
	public String getEnteredBy() {
		return mEnteredBy;
	}

	/**
	 * @param pEnteredBy the mEnteredBy to set
	 */
	public void setEnteredBy(String pEnteredBy) {
		this.mEnteredBy = pEnteredBy;
	}
	
	


	/**
	 * @return the deviceID
	 */
	public String getDeviceID() {
		return mDeviceID;
	}

	/**
	 * @param pDeviceID the deviceID to set
	 */
	public void setDeviceID(String pDeviceID) {
		mDeviceID = pDeviceID;
	}

	//START:Change for Epsilon
	
	/**
	 * Property to hold mEwasteSurchargeSkuPropertyName.
	 */
	private String mEwasteSurchargeSkuPropertyName;
	
	/**
	 * Gets the ewaste surcharge sku property name.
	 * 
	 * @return the ewasteSurchargeSkuPropertyName
	 */
	public String getEwasteSurchargeSkuPropertyName() {
		return mEwasteSurchargeSkuPropertyName;
	}

	/**
	 * Sets the ewaste surcharge sku property name.
	 * 
	 * @param pEwasteSurchargeSkuPropertyName
	 *            the ewasteSurchargeSkuPropertyName to set
	 */
	public void setEwasteSurchargeSkuPropertyName(
			String pEwasteSurchargeSkuPropertyName) {
		mEwasteSurchargeSkuPropertyName = pEwasteSurchargeSkuPropertyName;
	}
	
	//END:Change for Epsilon
	
	
	/**
	 * This method is overridden to update the transactionalCurrencyCode
	 * for the currentOrder same as last order.
	 * 
	 * @param pRequest
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void preCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug())
		{
			logDebug("TRUCSRCommitOrderFormHandler (preCommitOrder) : Starts");
		}

		TRUCSROrderManager orderManager = (TRUCSROrderManager) getOrderManager();
		
		TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		try {

			transactionDemarcation.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
			synchronized (getOrder()) {
				if (getFormError()) {
					return;
				}

				// Added for reward RS
				if (StringUtils.isNotBlank(getRewardNumber()) && !validateDenaliAccountNum(getRewardNumber())) {
					mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
					getErrorHandler().processException(mVe, this);
					return;
				}
				if(StringUtils.isNotBlank(getRewardNumber())){
				((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
				}
				((TRUOrderImpl) getOrder()).setEnteredBy(getEnteredBy());
				/*if (TRUCheckoutConstants.CREDITCARD.equalsIgnoreCase(getPaymentGroupType())) {
					if (isLoggingDebug()) {
						logDebug("Credit Card Payment method");
					}
					preCommitOrderFromOrderReview(pRequest, pResponse);

					if (getFormError()) {
						return;
					}

				}*/
				// Setting source of Order required for OMS service
				((TRUOrderImpl) getOrder()).setSourceOfOrder(getTRUConfiguration().getCsrSourceOrderType());
				// Validate order remaining amount
				if(((TRUOrderImpl) getOrder()).getPriceInfo() != null && ((TRUOrderImpl) getOrder()).getPriceInfo().getAmount() > TRUCSCConstants.DOUBLE_ZERO_FORMAT){
					validateOrderRemainingAmount(pRequest, pResponse);
				}
				if (getFormError()) {
					return;
				}
				
				ensureEmailAddress();
				// Custom TRU validation
				Map parameters = new HashMap();
				int countBeforeRemovalOfOOSItems = 0;
				
				((TRUOrderImpl) getOrder()).setOutOfStockItems(null);
				List sgCiRels = getShippingGroupCommerceItemRelationships(getOrder());
				if ((sgCiRels != null && !sgCiRels.isEmpty())) {
					final int sgCiRelsCount = sgCiRels.size();
					for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
						final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
						final TRUCommerceItemImpl item = (TRUCommerceItemImpl) sgCiRel.getCommerceItem();
						if (item instanceof TRUGiftWrapCommerceItem) {
							continue;
						}else{
							countBeforeRemovalOfOOSItems++;
						}
					}
				}
				parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.PROCESS_ORDER);
				
				TRUUserSession sessionComponent = getUserSession();
                parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);

				((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(
						getOrder(),
						getUserPricingModels(),
						getUserLocale(),
						getProfile(),
						parameters,
						null,
						this);

				
				
				boolean allOutOfStock = false;
				boolean showRedirectMessage = false;
				// Calling the reprice order and adjusting the PGs when the order gets modified
				if (((TRUOrderImpl) getOrder()).isValidationFailed()) {
					boolean hasLocationId = false;
					if(!((TRUOrderImpl)getOrder()).getOutOfStockItems().isEmpty()){
						List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) getOrder()).getOutOfStockItems();
						for(TRUOutOfStockItem itemVO : outOfStockItems){
							String locationId = (String)itemVO.getPropertyValue(TRUCSCConstants.LOCATION_ID);
							if(locationId != null){
								hasLocationId = true;
							}
						}
						if(!hasLocationId && outOfStockItems != null && outOfStockItems.size() == countBeforeRemovalOfOOSItems){
							String infoMessage = TRUCSCConstants.ALL_ITEMS_OUT_OF_STOCK;
							allOutOfStock = true;
							addFormException(new DropletException(infoMessage));
						}else{
							for(TRUOutOfStockItem itemVO : outOfStockItems){
								String itemName = (String)itemVO.getPropertyValue(TRUCSCConstants.CATALOG_REF_ID);
								String locationId = (String)itemVO.getPropertyValue(TRUCSCConstants.LOCATION_ID);
								String infoMessage = TRUCSCConstants.INVERTED_COMMA;
								if(locationId != null){
									infoMessage = TRUCSCConstants.ITEM_OUT_OF_STOCK+itemName+TRUCSCConstants.AT_LOCATION+locationId+TRUCSCConstants.PLEASE_SELECT_OTHER_LOCATION;
								}else{
									infoMessage = TRUCSCConstants.ITEM_OUT_OF_STOCK+itemName;
								}
								
								if(infoMessage != null ){
									infoMessage = infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
									showRedirectMessage = true;
									addFormException(new DropletException(infoMessage));
								}
							}
						}
					}
					if(allOutOfStock  && !hasLocationId){
						addFormException(new DropletException(TRUCSCConstants.ALL_OUT_OF_STOCK_REDIRECT_MSG));
					}else if(!allOutOfStock && showRedirectMessage  && !hasLocationId){
						addFormException(new DropletException(TRUCSCConstants.FEW_ITEMS_OUT_OF_STOCK_REDIRECT_MSG));
					}
					((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(
							PricingConstants.OP_REPRICE_ORDER_TOTAL,
							getOrder(),
							getUserPricingModels(),
							getUserLocale(),
							getProfile(),
							createRepriceParameterMaps(),
							this);
					((TRUCSROrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
					orderManager.updateOrder(getOrder());
					return;
				}
				((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessRepriceOrder(
						PricingConstants.OP_REPRICE_ORDER_TOTAL,
						getOrder(),
						getUserPricingModels(),
						getUserLocale(),
						getProfile(),
						createRepriceParameterMaps(),
						this);
				orderManager.updateOrder(getOrder());
				TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
				if (getProfile().isTransient()) {
					((TRUOrderImpl) getOrder()).setEmail(getEmail());
				} else {
					((TRUOrderImpl) getOrder()).setEmail((String) getProfile().getPropertyValue(
							profileTools.getPropertyManager().getEmailAddressPropertyName()));
				}
				if (isLoggingDebug())
				{
					logDebug("Before Calling Order Manager....");
				}
				
				
				orderManager.updateOrder(getOrder());

			}
			// calling OOTB method
			super.preCommitOrder(pRequest, pResponse);

		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("CommerceException occured during preCommitOrder for order Id : " + getOrder().getId(), e);
			}
		} catch (RunProcessException rpE) {
			if (isLoggingError()) {
				logError("RunProcessException occured during preCommitOrder for order Id : " + getOrder().getId(), rpE);
			}
		} catch (TransactionDemarcationException trE) {
			if (isLoggingError()) {
				logError("TransactionDemarcationException occured during preCommitOrder for order Id : "+ getOrder().getId(), trE);
			}
		} finally {
			if (transactionDemarcation != null) {
				try {
					transactionDemarcation.end();
				} catch (TransactionDemarcationException demarcationException) {
					// addFormException(new DropletException(payPalErrMessage));
					if (isLoggingError()) {
						logError("TransactionDemarcationException occurs ", demarcationException);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("TRUCSRCommitOrderFormHandler.preCommitOrder : Ends");
		}
	}


	/**
	 * Commit orader.
	 * 
	 * @param pOrder
	 *            the order
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void commitOrder(Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("START TRUCommitOrderFormHandler:: commitOrder method");
		}
		if (getFormError()) {
			checkFormRedirect(null, getCommitOrderErrorURL(), pRequest, pResponse);
			return;
		}
		((TRUCSROrderManager)getOrderManager()).updateOrderWithFraudParameters(pRequest, (TRUOrderImpl)getOrder(), getProfile(), getDeviceID(), getUserSession().getUserSessionStartTime());
		super.commitOrder(pOrder, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("END TRUCommitOrderFormHandler:: commitOrder method");
		}
	}

	/**
	 * This method is used to add calculate tax parameter.
	 * @return Map
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	protected Map createRepriceParameterMaps() {
		if (isLoggingDebug()) {
			logDebug("TRUCSRCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CAL_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			logDebug("TRUCSRCommitOrderFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
	
	


	/**
	 * Commit order from payment.
	 *
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             the commerce exception
	 */
	public void commitOrderFromPayment(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("START TRUCommitOrderFormHandler:: commitOrderFromPayment method");
		}
		// Getting Credit card
		TRUCreditCard creditCard = (TRUCreditCard) ((TRUCSROrderManager) getOrderManager()).getCreditCard(getOrder());
		if (creditCard == null) {
			if (isLoggingError()) {
				logError("Exception in @Class::TRUCommitOrderFormHandler::@method::commitOrderFromPayment(): Credit Card is NULL");
			}
			return;
		}
		/*if (!isOrderSetWithDefault()) {
			// Adding credit card to container and profile
			if (!isRUSCard()) {
				addCreditCard(pRequest, pResponse);
			}
			// updating order credit card
			getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
			creditCard.setBillingAddressNickName(getBillingAddressNickName());
		}*/
		// set cardinal properties
		// getBillingHelper().updateCardinalProperties(getCreditCardInfoMap().get(TRUConstants.CARDINAL_JWT),
		// creditCard);
		((TRUOrderImpl) getOrder()).setRewardNumber(getRewardNumber());
		if (isLoggingDebug()) {
			logDebug("END TRUCommitOrderFormHandler:: commitOrderFromPayment method");
		}
	}

	/**
	 * Add credit card.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @param pResponse
	 *            DynamoHttpServletResponse
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @throws CommerceException
	 *             CommerceException
	 */
	protected void addCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		vlogDebug("START of TRUBillingInfoFormHandler.addCreditCard method");
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();

		String billingAddressNickName = getBillingAddressNickName();
		if (getBillingAddressNickName() == null) {
			billingAddressNickName =
					getBillingHelper().getPurchaseProcessHelper().generateUniqueNickname(
							getAddressInputFields().getFirstName(),
							getAddressInputFields().getLastName());
		} else {
			billingAddressNickName = getBillingAddressNickName();
		}
		if (getShippingGroupMapContainer().getShippingGroup(billingAddressNickName) != null) {
			// updating address in shipping group map container and also in profile for logged in user
			getShippingHelper().modifyShippingAddress(getProfile(), billingAddressNickName,
					getAddressInputFields(), getShippingGroupMapContainer(), getOrder());
		} else {
			// adding address in shipping group map container and also in profile for logged in user
			getShippingHelper().addShippingAddress(getProfile(), billingAddressNickName,
					getAddressInputFields(), getShippingGroupMapContainer());
		}
		String creditCardType = null;
		// updating credit card type
		if (getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()) != null) {
			creditCardType =
					((TRUProfileTools) ((TRUOrderTools) getOrderManager().getOrderTools()).getProfileTools())
					.getCreditCardTypeFromRepository(
							getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()),
							getCreditCardInfoMap().get(TRUConstants.CARD_LENGTH));
		}
		// if the number is set as token then get from credit card info map.
		if (creditCardType == null) {
			vlogDebug("creditCardType is null : -- >" + creditCardType);
			creditCardType = getCreditCardInfoMap().get(propertyManager.getCreditCardTypePropertyName());
		}

		vlogDebug("creditCardType: {0} ", creditCardType);
		getCreditCardInfoMap().put(propertyManager.getCreditCardTypePropertyName(), creditCardType);

		getBillingHelper().addCreditCard(getOrder(), getProfile(), getCreditCardInfoMap(), billingAddressNickName,
				getAddressInputFields(), getPaymentGroupMapContainer(), true);

		vlogDebug("END of TRUBillingInfoFormHandler.addCreditCard method");
	}


	/**
	 * Checks for credit card expiration in order. Adds form exception if credit card expired.
	 * 
	 * @throws ServletException
	 *             returns if Servlet exception
	 * @throws IOException
	 *             returns if I/O exception
	 * @return true - if credit card is expired.
	 */
	public boolean isOrderCreditCardExpired() throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("START :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
		}
		TRUCSROrderManager orderManager = (TRUCSROrderManager) getOrderManager();
		CreditCard creditCard = orderManager.getCreditCard(getOrder());
		if (creditCard != null &&
				StringUtils.isNotBlank(creditCard.getCreditCardNumber())) {
			TRUProfileManager profileManager = getBillingHelper().getProfileManager();
			boolean isCardExpired = profileManager
					.validateCardExpiration(creditCard.getExpirationMonth(), creditCard.getExpirationYear());
			if (isCardExpired) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ORDER_CREDIT_CARD_EXPIRED, null);
				getErrorHandler().processException(mVe, this);
				if (isLoggingDebug()) {
					logDebug("END :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
				}
				return Boolean.TRUE;
			}
		}
		if (isLoggingDebug()) {
			logDebug("END :: TRUCommitOrderFormHandler.isOrderCreditCardExpired method");
		}
		return Boolean.FALSE;
	}


	/**
	 * Pre commit order from order review.
	 *
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws RunProcessException
	 *             the run process exception
	 * @throws ServletException
	 *             the servlet exception
	 * @throws CommerceException
	 *             the commerce exception
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void preCommitOrderFromOrderReview(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RunProcessException, ServletException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("START TRUCommitOrderFormHandler:: preCommitOrderFromOrderReview method");
		}
		TRUCSROrderManager orderManager = (TRUCSROrderManager) getOrderManager();

		// Getting Credit card
		// set cardinal properties
		// getBillingHelper().updateCardinalProperties(getCreditCardInfoMap().get(TRUConstants.CARDINAL_JWT),
		// creditCard);
		Map parameters = new HashMap();
		parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.PROCESS_ORDER);
		TRUUserSession sessionComponent = getUserSession();
        parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
        
		((TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper()).runProcessTRUValidateForCheckoutChain(getOrder(),
				getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);

		getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(),
				getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMaps(), this);
		
		((TRUOrderImpl) getOrder()).setEmail(getEmail());
		orderManager.updateOrder(getOrder());

		if (isLoggingDebug()) {
			logDebug("END TRUCommitOrderFormHandler:: preCommitOrderFromOrderReview method");
		}
	}

	/**
	 * Validate order remaining amount.
	 *
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	public void validateOrderRemainingAmount(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		// validate the order for order amount.
		if (isLoggingDebug()) {
			logDebug("START TRUCommitOrderFormHandler:: validateCommitOrder method");
		}
		TRUOrderImpl order = (TRUOrderImpl) getOrder();
		List<PaymentGroup> listOfPaymentGroup = order.getPaymentGroups();
		if (listOfPaymentGroup == null || listOfPaymentGroup.size() ==TRUCSCConstants.NUMNER_ZERO) {
			mVe.addValidationError(TRUErrorKeys.ERR_OTHER_PAYMENT_OPTION_IS_REQUIRED, null);
		} else {
			boolean validateOrderRemainingAmount =
					getBillingHelper().validateOrderRemainingAmount(order, TRUCheckoutConstants.COMMIT_ORDER);
			if (!validateOrderRemainingAmount) {
				mVe.addValidationError(TRUErrorKeys.GIFT_CARDS_BALANCE_IS_LESS_THAN_ORDER_AMOUNT, null);
			}
		}
		getErrorHandler().processException(mVe, this);
		if (isLoggingDebug()) {
			logDebug("END TRUCommitOrderFormHandler:: validateCommitOrder method");
		}
	}


	/**
	 * In store payment to commit order.
	 *
	 * @throws ServletException
	 *             ServletException
	 * @throws IOException
	 *             IOException
	 * @throws CommerceException
	 *             the commerce exception
	 */

	@SuppressWarnings("unused")
	private void validateCommitOrderFromPayment()throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("Start of TRUBillingInfoFormHandler/validateCommitOrderFromPayment method");
		}
		try {
			if (isLoggingDebug()) {
				logDebug("TRUBillingInfoFormHandler.validateCommitOrderFromPayment :: addCreditCardWithoutAddress");
			}
			if (!isOrderSetWithDefault()) {
				if (getProfile().isTransient()) {
					/*To validate all mandatory fields including email when user is not logged in along with email format*/
					if (((TRUCSROrderManager) getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap()) ||
							((TRUCSROrderManager) getOrderManager()).isAnyAddressFieldEmpty(getAddressInputFields())) {
						if (isLoggingDebug()) {
							logDebug("TRUBillingInfoFormHandler.validateCommitOrderFromPayment is having errors");
						}
						getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
						getErrorHandler().processException(mVe, this);
					}
				} else {
					/*To validate all mandatory fields when user is logged in*/
					if (((TRUCSROrderManager) getOrderManager()).isAnyCreditCardFieldEmpty(getCreditCardInfoMap()) ||
							((TRUCSROrderManager) getOrderManager()).isAnyAddressFieldEmpty(getAddressInputFields())) {
						if (isLoggingDebug()) {
							logDebug("TRUBillingInfoFormHandler.validateCommitOrderFromPayment is having errors");
						}
						getValidator().validate(TRUConstants.MY_ACCOUNT_ADD_CARD_WITHOUT_ADDR_FORM, this);
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
						getErrorHandler().processException(mVe, this);
					}
				}
				Map creditCards =
						(Map) getProfile().getPropertyValue(
								((TRUCSROrderManager) getOrderManager())
								.getPropertyManager()
								.getCreditCardPropertyName());
				CreditCard orderCreditCard = ((TRUCSROrderManager) getOrderManager())
						.getCreditCard(getOrder());
				if (orderCreditCard != null) {
					if (!getFormError() && creditCards.isEmpty() && StringUtils.isBlank(getEmail())	&& 
							StringUtils.isBlank(orderCreditCard.getBillingAddress().getAddress1())) {
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_ADD_CARD_MISS_INFO, null);
						getErrorHandler().processException(mVe, this);
					}
				} else {
					if (isLoggingError()) {
						logError("Exception in @Class::TRUCommitOrderFormHandler::@method::validateCommitOrderFromPayment(): Credit Card is NULL");
					}
				}
			} else {
				// validates order credit card
				isOrderCreditCardExpired();
			}
			if (!getFormError() && StringUtils.isNotBlank(getRewardNumber()) && 
					getRewardNumberValid().equalsIgnoreCase(TRUConstants.FALSE)) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_REWARD_NUMBER_INVALID, null);
				getErrorHandler().processException(mVe, this);
			}
		} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured: {0}", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured: {0}", se);
			}
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUBillingInfoFormHandler/validateCommitOrderFromPayment method");
		}

	}

	/**
	 * This method is overridden to update the transactionalCurrencyCode for the currentOrder same as last order.
	 * 
	 * @param pRequest
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void postCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if(isLoggingDebug()){
			logDebug("TRUCSRCommitOrderFormHandler (postCommitOrder) : Starts");
		}
		double eWastefee = 0.0;
		Address billingAddress = null;
		// calling OOTB method
		
		super.postCommitOrder(pRequest, pResponse);
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		try {
			if(((TRUOrderImpl) getCartComponent().getLast()).getRewardNumber() != null){
				String rewardNumber = ((TRUOrderImpl)getCartComponent().getLast()).getRewardNumber();
				profileTools.updateProperty(((TRUPropertyManager)profileTools.getPropertyManager()).getRewardNumberPropertyName(), rewardNumber, getProfile());
			}
			// Added code for tender based promotions(budegetLimit)
			if((TRUOrderImpl) getCartComponent().getLast() != null){
			final List<PricingAdjustment> tenderTypeAdjustmentPromotions = getPromotionTools().updateTenderTypeAdjustment((TRUOrderImpl) getCartComponent().getLast());
			// update or create the tenderTypePromotion item
				if (tenderTypeAdjustmentPromotions != null) {
					getPromotionTools().createOrUpdateTenderTypePromo(tenderTypeAdjustmentPromotions);
				}
			}
			// Added for epslon order acknowledgment mail.
			if (getTRUConfiguration().isEnableEpslon() && getShoppingCart() != null) {
				TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();	
				Map<String,TRUEpslonBppInfoVO> epslonBppItemMap = new HashMap<String, TRUEpslonBppInfoVO>();
				Map<String,String> shipDeliveryTimeMap = new HashMap<String, String>();
				Map<String,Double> itemEwasteFeeMap = new HashMap<String,Double>();
				if (getShoppingCart().getLast() != null) {
					String orderID = getShoppingCart().getLast().getId();
					String email= ((TRUOrderImpl)getShoppingCart().getLast()).getEmail();
					Order order = null;
					try {
						if(StringUtils.isBlank(email)){
							email = TEST_EMAIL ;
							if (isLoggingDebug()) {
								logDebug("Email in order is Empty ::: ");
							}
						}
						
						try {
							order =  getOrderManager().loadOrder(orderID);
							billingAddress  = ((TRUOrderImpl)order).getBillingAddress();
							if(billingAddress != null){
								shipDeliveryTimeMap.put(TRUCommerceConstants.BILLINGFIRSTNAME,billingAddress.getFirstName());
								shipDeliveryTimeMap.put(TRUCommerceConstants.BILLINGLASTNAME, billingAddress.getLastName());
							}
						} catch (CommerceException e) {
							if (isLoggingError()) {
								logError("CommerceException in TRUEpslonOrderAcknowledgeService generateOrderAckRequest() : ", e);
							}
						}						
						TRUShippingGroupCommerceItemRelationship cItemRel=null;
						ShippingGroup shippingGroup = null;
						TRUBppItemInfo bppItemInfo = null;
						String relationShipId = TRUConstants.EMPTY;
						String state = TRUConstants.ABCD;
						CommerceItem commItem = null;
						String bppSkuID  = TRUConstants.EMPTY;
						List<ShippingGroup> shippingGroups = order.getShippingGroups();
						Iterator<ShippingGroup> it = shippingGroups.iterator();
						 while (it.hasNext()) {
					        	shippingGroup = it.next();
					        	String shippingGroupID = shippingGroup.getId();
					        	String deliveryTimeLine = TRUConstants.EMPTY;
					        	if(shippingGroup instanceof HardgoodShippingGroup ){
					        	TRUHardgoodShippingGroup hardGoodSG = (TRUHardgoodShippingGroup) shippingGroup;
					        	if(hardGoodSG.getDeliveryTimeFrame() != null){
					        	  deliveryTimeLine = hardGoodSG.getDeliveryTimeFrame();
					        	}
					        	}
					        	shipDeliveryTimeMap.put(shippingGroupID, deliveryTimeLine);
								List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
								Iterator<CommerceItemRelationship> itr = itemList.iterator();
								while (itr.hasNext()) {
								cItemRel  = (TRUShippingGroupCommerceItemRelationship) itr.next();
								if(cItemRel != null){
									relationShipId = cItemRel.getId();
									bppItemInfo = cItemRel.getBppItemInfo();
									bppSkuID  = ((TRUOrderTools) getOrderManager().getOrderTools()).getBppSkuId(cItemRel);
									commItem = cItemRel.getCommerceItem();
									if(commItem != null){
										TRUItemPriceInfo itemPriceEwasteInfo = ((TRUItemPriceInfo)commItem.getPriceInfo());
										if(null != itemPriceEwasteInfo){
											RepositoryItem productItem=(RepositoryItem)cItemRel.getCommerceItem().getAuxiliaryData().getProductRef();									
											if(productItem.getPropertyValue(getEwasteSurchargeSkuPropertyName())!=null && !((String)productItem.getPropertyValue(getEwasteSurchargeSkuPropertyName())).equalsIgnoreCase(state))
											{
												eWastefee = ((TRUCSROrderTools)getPurchaseProcessHelper().getOrderManager().getOrderTools()).getPricingTools().getEWastePrice(productItem);
												//eWastefee = ((TRUPricingTools)((TRUPurchaseProcessHelper)getPurchaseProcessHelper()).getProfileTools().getPricingTools()).getEWastePrice(productItem); 
											}
											if (isLoggingDebug()) {
												logDebug("Item eWasteFee value for Item::: " + commItem.getCatalogRefId());
												logDebug("Item eWasteFee value ::: " + eWastefee);
											}
											itemEwasteFeeMap.put(commItem.getCatalogRefId(), eWastefee);
										}
									}
								RepositoryItem bppItem = (RepositoryItem) cItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
								if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId()))
								{
									TRUEpslonBppInfoVO epslonBppInfoVO = new TRUEpslonBppInfoVO();
									if(StringUtils.isBlank(bppSkuID)){
										bppSkuID = bppItemInfo.getId();
									}
									String bppItemName = bppItemInfo.getRepositoryItem().getItemDisplayName();
									double bppItemPrice = bppItemInfo.getBppPrice();
									epslonBppInfoVO.setBppSkuID(bppSkuID);
									epslonBppInfoVO.setBppItemName(bppItemName);
									epslonBppInfoVO.setBppItemPrice(bppItemPrice);
									epslonBppItemMap.put(relationShipId, epslonBppInfoVO);
							   }
							 }
							}
						 }
						 CommerceItem commerceItem = null;
						 List<CommerceItem> commerceItems = order.getCommerceItems();
						 Iterator<CommerceItem> itci = commerceItems.iterator();
						 while (itci.hasNext()) {
						 commerceItem =  itci.next();
						  String commerceItemId = commerceItem.getId();
							TRUItemPriceInfo itemPriceInfo = ((TRUItemPriceInfo)commerceItem.getPriceInfo());
							if(null != itemPriceInfo){							
								Map<String, String> truItemDiscountShare = itemPriceInfo.getTruItemDiscountShare();
								if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty()){
									Iterator<String> itemDiscountShareIterator = truItemDiscountShare.keySet().iterator();
									double itemDiscShare = TRUConstants.DOUBLE_ZERO;
									while(itemDiscountShareIterator.hasNext()){
										String key = (String) itemDiscountShareIterator.next();
										String value = truItemDiscountShare.get(key);
										if(!StringUtils.isBlank(value)){
											String[] valueSplit = value.split(TRUCSCConstants.PIPE_DELIMETRE);
											String discShare  = valueSplit[0];
											 itemDiscShare = itemDiscShare + Double.parseDouble(discShare);										
										}
									}
									itemDiscShare = roundTwoPlaceDecimal(itemDiscShare, TRUConstants.TWO);
									itemEwasteFeeMap.put(commerceItemId,itemDiscShare);
								}
							}
						 }
						 boolean isAnonymous = getProfile().isTransient();	
						 TRUTaxPriceInfo tPriceInfo = (TRUTaxPriceInfo) order.getTaxPriceInfo();
						 itemEwasteFeeMap.put(TRUCSCConstants.ISLAND_TAX, tPriceInfo.getEstimatedIslandTax());
						 itemEwasteFeeMap.put(TRUCSCConstants.LOCAL_TAX, tPriceInfo.getEstimatedLocalTax());
						getEpslonOrderAcknowledgeSource().sendOrderAcknowledgementEmail(orderID,email,epslonBppItemMap,isAnonymous,shipDeliveryTimeMap,itemEwasteFeeMap);
					} catch (JMSException e) {
						if (isLoggingError()) {
							logError("JMSException in TRUCSRCommitOrderFormHandler.postCommitOrder() : ",e);
						}
					}
				} else {
					if (isLoggingDebug()) {
						logDebug("Order is Empty");
					}
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("Epslon Is Disabled or Order is Empty");
				}
			}
			// update the single use coupon uses
			List<RepositoryItem> singleUseCoupons = ((TRUClaimableTools) getClaimableManager().getClaimableTools()).getSingleUsedCoupon(getOrder());
			if(singleUseCoupons != null && !singleUseCoupons.isEmpty()){
				getPromotionTools().revokeSingleUseCouponsfromUser(singleUseCoupons, getProfile());
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUCommitOrderFormHandler.postCommitOrder() : ", e);
			}
		}

		if(isLoggingDebug()){
			logDebug("TRUCSRCommitOrderFormHandler (postCommitOrder) : Ends");
		}
	}
	
	/**
	 * this method is used to round off the decimal value with two place.
	 * 
	 * @param pNumber
	 *            the number
	 * @param pRoundingDecimalPlaces
	 *            the roundingDecimalPlaces
	 * @return BigDecimal Object
	 */
	private double roundTwoPlaceDecimal(double pNumber, int pRoundingDecimalPlaces) {
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRCommitOrderFormHandler:: (roundTwoPlaceDecimal): pNumber:{0}", pNumber);
		}
		BigDecimal bd = new BigDecimal(Double.toString(pNumber));
		bd = bd.setScale(pRoundingDecimalPlaces, TRUTaxwareConstant.NUMERIC_FOUR);
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRCommitOrderFormHandler: (roundTwoPlaceDecimal): bid=g decimal:{0}", bd);
		}
		return bd.doubleValue();
	}

	/**
	 * @This method validates the reward membershipCard number.
	 * @param pRewardNumber rewardNumber
	 * @return boolean
	 */
	public boolean validateDenaliAccountNum(String pRewardNumber)
	{
		boolean isValid=false;
		int charPos=TRUConstants.ZERO;
		int evensum = TRUConstants.ZERO;
		int oddsum = TRUConstants.ZERO;
		int csum=TRUConstants.ZERO;
		int check=TRUConstants.ZERO;
		String rewardNumberSubStr=TRUConstants.EMPTY;

		if(pRewardNumber.length()==TRUConstants.THIRTEEN && pRewardNumber.substring(TRUConstants.ZERO,TRUConstants.TWO).equalsIgnoreCase(TRUConstants.TWENTY_ONE))
		{
			csum = Integer.parseInt(TRUConstants.DOUBLE_QUOTE+pRewardNumber.charAt(TRUConstants.TWELVE));
			if (pRewardNumber != null && pRewardNumber.length() == TRUConstants.THIRTEEN) {
				rewardNumberSubStr = pRewardNumber.substring(TRUConstants.ZERO, TRUConstants.TWELVE);
			}
			if (rewardNumberSubStr == null || rewardNumberSubStr.length()!= TRUConstants.TWELVE) {
				return false;
			}

			for (int i = rewardNumberSubStr.length() -TRUConstants.SIZE_ONE; i >= TRUConstants.ZERO; i--) {
				charPos = rewardNumberSubStr.length() - i-TRUConstants.SIZE_ONE;
				if ((rewardNumberSubStr.length() - i) % TRUConstants.TWO == TRUConstants.ZERO) {
					evensum = evensum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE+rewardNumberSubStr.charAt(charPos));
				} else {
					oddsum = oddsum + Integer.parseInt(TRUConstants.DOUBLE_QUOTE+rewardNumberSubStr.charAt(charPos));
				}
			}
			check = TRUConstants.TEN - (evensum * TRUConstants.THREE + oddsum) % TRUConstants.TEN;
			if (check >= TRUConstants.TEN)
			{
				check = TRUConstants.ZERO;
			}
			isValid = csum == check;
		}
		return isValid;
	}
	
	/**
	 * This method handles sending email on order confirmation.
	 * 
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 * @throws ServletException
	 *             returns if Servlet exception
	 * @throws IOException
	 *             returns if I/O exception
	 * @return true or false
	 * 
	 */
	
	  public boolean handleSendConfirmationMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			    throws ServletException, IOException
			    {
		  RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		  String myHandleMethod = TRUCSCConstants.HANDLECONFIRMATIONMAIL;
		  if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod)))

		  {
			  Transaction tr = null;
			  try {
				  tr = ensureTransaction();

				  if (getUserLocale() == null) 
				  {
					  setUserLocale(getUserLocale(pRequest, pResponse));
				  }
				  Order order = getShoppingCart().getCurrent();
				  synchronized (order)
				  {
					  preSendConfirmationMessage(pRequest, pResponse);
					  
					  if (getFormError()) {
							return checkFormRedirect(getCommitOrderUpdatesSuccessURL(), getCommitOrderUpdatesErrorURL(), pRequest, pResponse);
				        }

					  sendConfirmationMessage(pRequest, pResponse);

					  postSendConfirmationMessage(pRequest, pResponse);

				  }

			  }      finally      {

		           if (tr != null) {
		        	   commitTransaction(tr);     
		           }
		            if (rrm != null)    {
		            	 rrm.removeRequestEntry(myHandleMethod);  
		            }
		             
		        }
		     
			  }
		  return false;
	}
	  
	 /**
	 * @This method invokes epslon.
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 */
	  
	  public void sendConfirmationMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	  {
		  
		  if(isLoggingDebug()){
				logDebug("TRUCSRCommitOrderFormHandler (sendConfirmationMessage) : Starts");
			}
				// Added for epslon order acknowledgment mail.
				if (getTRUConfiguration().isEnableEpslon() && getShoppingCart() != null) {
					TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();	
					Map<String,TRUEpslonBppInfoVO> epslonBppItemMap = new HashMap<String, TRUEpslonBppInfoVO>();
					Map<String,String> shipDeliveryTimeMap = new HashMap<String, String>();
					Map<String,Double> itemEwasteFeeMap = new HashMap<String,Double>();
					if (getShoppingCart().getLast() != null) {
						String orderID = getShoppingCart().getLast().getId();
						String email= getConfirmationInfo().getToEmailAddress();
						Order order = null;
						try {
							if(StringUtils.isBlank(email)){
								email = TEST_EMAIL;
								if (isLoggingDebug()) {
									logDebug("Email in order is Empty ::: ");
								}
							}
							try {
								order =  getOrderManager().loadOrder(orderID);
							} catch (CommerceException e) {
								if (isLoggingError()) {
									logError("CommerceException in TRUEpslonOrderAcknowledgeService generateOrderAckRequest() : ", e);
								}
							}
							ShippingGroup shippingGroup = null;
							TRUBppItemInfo bppItemInfo = null;
							String relationShipId = TRUConstants.EMPTY;
							List<ShippingGroup> shippingGroups = order.getShippingGroups();
							Iterator<ShippingGroup> it = shippingGroups.iterator();
							 while (it.hasNext()) {
					        	shippingGroup = it.next();
					        	
					        	String shippingGroupID = shippingGroup.getId();
					        	String deliveryTimeLine = TRUConstants.EMPTY;
					        	
					        	if(shippingGroup instanceof HardgoodShippingGroup ){
					        	TRUHardgoodShippingGroup hardGoodSG = (TRUHardgoodShippingGroup) shippingGroup;
					        	if(hardGoodSG.getDeliveryTimeFrame() != null){
					        	  deliveryTimeLine = hardGoodSG.getDeliveryTimeFrame();
					        	}
					        	}
					        	shipDeliveryTimeMap.put(shippingGroupID, deliveryTimeLine);
								
								List<CommerceItemRelationship> itemList = shippingGroup.getCommerceItemRelationships();
								Iterator<CommerceItemRelationship> itr = itemList.iterator();
								while (itr.hasNext()) {
								TRUShippingGroupCommerceItemRelationship cItemRel  = (TRUShippingGroupCommerceItemRelationship) itr.next();
								
								if(cItemRel != null){
									relationShipId = cItemRel.getId();
									bppItemInfo = cItemRel.getBppItemInfo();
								}
								RepositoryItem bppItem = (RepositoryItem) cItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
								if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId())){
									TRUEpslonBppInfoVO epslonBppInfoVO = new TRUEpslonBppInfoVO();
									String bppSkuID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
									if(StringUtils.isBlank(bppSkuID)){
										bppSkuID = bppItemInfo.getId();
										}
									String bppItemName = bppItemInfo.getRepositoryItem().getItemDisplayName();
									double bppItemPrice = bppItemInfo.getBppPrice();
									epslonBppInfoVO.setBppSkuID(bppSkuID);
									epslonBppInfoVO.setBppItemName(bppItemName);
									epslonBppInfoVO.setBppItemPrice(bppItemPrice);
									epslonBppItemMap.put(relationShipId, epslonBppInfoVO);
								   }
								  }
								}
							 
							 CommerceItem commerceItem = null;
							 List<CommerceItem> commerceItems = order.getCommerceItems();
							 Iterator<CommerceItem> itci = commerceItems.iterator();
							 while (itci.hasNext()) {
								commerceItem =  itci.next();
								String commerceItemId = commerceItem.getId();
								TRUItemPriceInfo itemPriceInfo = ((TRUItemPriceInfo)commerceItem.getPriceInfo());
								if(null != itemPriceInfo){
									double eWastefee = itemPriceInfo.getEwasteFees();
									itemEwasteFeeMap.put(commerceItemId, eWastefee);
								}
							 }
							 boolean isAnonymous = getProfile().isTransient();
							 TRUTaxPriceInfo tPriceInfo = (TRUTaxPriceInfo) order.getTaxPriceInfo();
							 itemEwasteFeeMap.put(TRUCSCConstants.ISLAND_TAX, tPriceInfo.getEstimatedIslandTax());
							 itemEwasteFeeMap.put(TRUCSCConstants.LOCAL_TAX, tPriceInfo.getEstimatedLocalTax());
							getEpslonOrderAcknowledgeSource().sendOrderAcknowledgementEmail(orderID,email,epslonBppItemMap,isAnonymous,shipDeliveryTimeMap,itemEwasteFeeMap);
						} catch (JMSException e) {
							if (isLoggingError()) {
								logError("JMSException in TRUCSRCommitOrderFormHandler.postCommitOrder() : ",e);
							}
						}
					} else {
						if (isLoggingDebug()) {
							logDebug("Order is Empty");
						}
					}
				} else {
					if (isLoggingDebug()) {
						logDebug("Epslon Is Disabled or Order is Empty");
					}
				}

			if(isLoggingDebug()){
				logDebug("TRUCSRCommitOrderFormHandler (sendConfirmationMessage) : Ends");
			}
	  }
	  
  	/**
	 * @This method validates email address is empty or not.
	 * @param pRequest
	 *            DynamoHttpServletRequest.
	 * @param pResponse
	 *            DynamoHttpServletResponse.
	 */
	  public void preSendConfirmationMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	  {
		  if(isLoggingDebug()){
				logDebug("TRUCSRCommitOrderFormHandler (preSendConfirmationMessage) : Starts");
			}
	  
	  if (StringUtils.isEmpty(getConfirmationInfo().getToEmailAddress())) {
	      if (isLoggingDebug()) {
	        logDebug("No To Address was specified for the confirmation message. Email is not being sent.");
	      }
	      return;

	    }
	  if(isLoggingDebug()){
			logDebug("TRUCSRCommitOrderFormHandler (preSendConfirmationMessage) : Ends");
		}
	  }
	  
	  /**
		 * @This post method.
		 * @param pRequest
		 *            DynamoHttpServletRequest.
		 * @param pResponse
		 *            DynamoHttpServletResponse.
		 */
	  
	  public void postSendConfirmationMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
		  
		  if(isLoggingDebug()){
				logDebug("TRUCSRCommitOrderFormHandler (postSendConfirmationMessage) : Starts");
			}
		  if(isLoggingDebug()){
				logDebug("TRUCSRCommitOrderFormHandler (postSendConfirmationMessage) : Ends");
			}
	  }
	  
	  /**
		 * Ensure email address.
		 *
		 * @throws ServletException the servlet exception
		 */
		private void ensureEmailAddress() throws ServletException {
			if (isLoggingDebug()) {
				vlogDebug("START TRUCommitOrderFormHandler:: ensureEmailAddress method");
			}
				if(getProfile().isTransient()){
					/*To validate email when user is not logged in along with email format*/
					if (StringUtils.isBlank(getEmail())) {
						if (isLoggingDebug()) {
							vlogDebug("TRUBillingInfoFormHandler.ensureEmailAddress is having errors");
						}
						
					} else if(!getFormError() && StringUtils.isNotBlank(getEmail()) && !getEmailPasswordValidator().validateEmail(getEmail())){
						mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECKOUT_INVALID_EMAIL, null);   
						getErrorHandler().processException(mVe, this);
					}
					if (getFormError()) {
						return;
					}
					((TRUOrderImpl)getOrder()).setEmail(getEmail());
				} else {
					((TRUOrderImpl)getOrder()).setEmail((String) getProfile().getPropertyValue(getCommercePropertyManager().getEmailAddressPropertyName()));
				}
		 
		 		
			if (isLoggingDebug()) {
				vlogDebug("END TRUCommitOrderFormHandler:: ensureEmailAddress method");
			}
		}
		
		/**
		 * Gets the shipping group commerce item relationships.
		 * 
		 * @param pOrder
		 *            the order.
		 * @return the shipping group commerce item relationships.
		 * @throws CommerceException
		 *             the commerce exception.
		 */
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public List getShippingGroupCommerceItemRelationships(Order pOrder) throws CommerceException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCSRCommitOrderFormHandler::@method::getShippingGroupCommerceItemRelationships() : START : Order --> "	+ pOrder);
			}
			List<TRUShippingGroupCommerceItemRelationship> rels=new ArrayList<TRUShippingGroupCommerceItemRelationship>();
			List<CommerceItem> cItems=pOrder.getCommerceItems();
			List<CommerceItem> tmpList = new ArrayList<CommerceItem>(cItems); 
		    Collections.reverse(tmpList);
			for(CommerceItem cItem:tmpList){
				rels.addAll(cItem.getShippingGroupRelationships());
			}
			if (isLoggingDebug()) {
				vlogDebug("@Class::TRUCSRCommitOrderFormHandler::@method::getShippingGroupCommerceItemRelationships() : END");
			}
			return rels;
		}

	/**
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @param pUserSession the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}
	}