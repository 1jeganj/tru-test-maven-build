package com.tru.commerce.csr.order;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.order.CSRShippingGroupFormHandler;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.order.purchase.CommerceItemShippingInfo;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tangosol.net.RequestTimeoutException;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.order.purchase.TRUCSRShippingProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUCSROrderTools;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingGroupManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.purchase.TRUGiftingPurchaseProcessHelper;
import com.tru.commerce.order.purchase.TRUPaymentGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.vo.TRUShipmentVO;
import com.tru.commerce.order.vo.TRUStorePickUpInfo;
import com.tru.commerce.pricing.TRUShipMethodVO;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUCSRConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.ValidationExceptions;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.errorhandler.fhl.IErrorHandler;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.radial.integration.taxware.TRUTaxwareConstant;
import com.tru.storelocator.tol.PARALStoreLocatorTools;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;

/**
 * class 'TRUCSRShippingGroupFormHandler' extends the TRU CSC Specific functionalities OOTB 'CSRShippingGroupFormHandler'.
 * 
 * @author Professional Access
 * @version 1.0
 *
 *
 */
public class TRUCSRShippingGroupFormHandler extends CSRShippingGroupFormHandler{
	
	/** Holds the mLocationTools. */
	private PARALStoreLocatorTools mLocationTools;
	
	  /**
	   * Inventory manager.
	   */
	  private TRUCoherenceInventoryManager mInventoryManager;
	  
	/** The m relation ships to copy. */
	private Map<String, Map<String,Object>> mRelationShipsToCopy = new HashMap<String, Map<String,Object>>();

	/** property: Reference to the ShippingProcessHelper component. */
	private TRUCSRShippingProcessHelper mShippingHelper;

	/** Property to hold mBppItemInfoPropertyName. */
	private String mBppItemInfoPropertyName;

	/** property to Hold shopping cart utils. */
	private TRUShoppingCartUtils mShoppingCartUtils;

	/** Property to hold mQuantity. */
	long mQuantity;

	/** Property to hold mLocationId. */
	private String mLocationId;

	/**Common log-1.*/
	private static final String COMMON_LOG_1 ="Redirecting due to form error in runProcessValidateShippingGroups";

	/** The Constant TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD. */
	private static final String TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD = "TRUCSRShippingGroupFormHandler.handleUpdateShipMethod";
    /** The m ship method code. */
	private String mShipMethodCode;

	/** The mShippingPrice. */
	private Double mShippingPrice;

	/** The m region code. */
	private String mRegionCode;

	/** holds Array of store pick up info vo. **/
	private TRUStorePickUpInfo[] mStorePickUpInfos;
	
	/** holds shipping group count. **/
	private int mShippingGroupsCount;

	/** Property to hold ProfileTools. */
	private TRUCSRAddressDoctor mAddressDoctor;

	/** Property to hold ProfileTools. */
	private TRUCSRPurchaseProcessHelper mTruCSRPurchaseProcessHelper;

	/** property: Nickname for new shipping address. */
	private String mNewShipToAddressName;

	/** mVe. */
	private ValidationExceptions mVe = new ValidationExceptions();

	/** mShipMethodVO. */
	private TRUShipMethodVO mShipMethodVO = new TRUShipMethodVO();

	/** property: flag indicating to check for ship to address name. */
	private String mShipToAddressName;

	/** Property to hold ProfileTools. */
	private TRUProfileTools mProfileTools;

	/** Property to hold Address doctor response VO. */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;

	/** property to hold tRUConfiguration. */
	private TRUCSRConfiguration mTRUCSRConfiguration;

	/** property: shipping address. */
	private ContactInfo mAddress = new ContactInfo();

	/** mErrorHandler. */
	private IErrorHandler mErrorHandler;

	/** property: mtruHardgoodShippingGroup for new shipping address. */

	private TRUHardgoodShippingGroup mTruHardgoodShippingGroup;

	/** property to hold mCommonSuccessURL. */
	private String mCommonSuccessURL;

	/** property to hold mCommonErrorURL. */
	private String mCommonErrorURL;

	/** boolean property to hold if address is validated by address doctor ot not. */
	private String mAddressValidated;

	/** Property to hold address doctor response status. */
	private String mAddressDoctorProcessStatus;

	/** The m hard good shipping group. */
	private HardgoodShippingGroup mHardGoodShippingGroup;

	/** The m action. */
	private String mAction;
	
	/** property to hold mCommerceItemRelationshipId  */
	private String mCommerceItemRelationshipId;
	
	/** property to hold user session component  */
	private TRUUserSession mUserSession;
	
	/** property to hold Navigate To Multi Ship.  */
	private boolean mNavigateToMultiShip;

	/**
	 * property: flag indicating whether billing address will be same as
	 * shipping address.
	 */
	private boolean mBillingAddressSameAsShippingAddress;
	
	/** property to hold mShipToNewAddressErrorURL */
	private String mShipToNewAddressErrorURL;
			
	/** The Unique id. */
	private String mUniqueId;
	
	/** property to hold mIncludeGifts */
	private boolean  mIncludeGifts;
	
	/** property to hold Is Empty Address.  */
	private boolean mEmptyShippingAddress;

	/** Property to hold mIntegrationStatusUtil. */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	
	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * booean to hold orderReconciliationNeeded
	 */
	private boolean mOrderReconciliationNeeded;
	
	/**
	 * holds relation ship id.
	 */
	private String mRelationShipId;
	
	
	/** The m gifting process helper. */
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;
	
	/**
	 * property to hold nickNameCount.
	 */
	private static int numberCount=0;
	
	/**
	 * holds SaveShippingAddress.
	 */
	private boolean mSaveShippingAddress;
	
	/** The Constant PLEASE_TRY_AGAIN. */
	private static final String PLEASE_TRY_AGAIN = "Please Try again";
	
	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * getter for errorHandlerManager.
	 *
	 * @return mErrorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}
	
	/**
	 * Sets the errorHandlerManager.
	 *
	 * @param pErrorHandlerManager the mErrorHandlerManager to set
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		this.mErrorHandlerManager = pErrorHandlerManager;
	}
	
	/**
	 * @return the mSaveShippingAddress
	 */
	public boolean isSaveShippingAddress() {
		return mSaveShippingAddress;
	}
	/**
	 * @param pSaveShippingAddress the mSaveShippingAddress to set
	 */
	public void setSaveShippingAddress(boolean pSaveShippingAddress) {
		mSaveShippingAddress = pSaveShippingAddress;
	}

	/**
	 * Gets the gifting process helper.
	 *
	 * @return the giftingProcessHelper.
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}
	
	/**
	 * Sets the gifting process helper.
	 *
	 * @param pGiftingProcessHelper the giftingProcessHelper to set.
	 */
	public void setGiftingProcessHelper(
			TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}
	
	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}
	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}

	/**
	 * Get the property to hold the action to be
	 * performed by the formhandler.
	 * @return the mAction
	 */
	public String getAction() {
		return mAction;
	}

	/**
	 * Set the property to hold the action to be
	 * performed by the formhandler.
	 * @param pAction - String
	 */
	public void setAction(String pAction) {
		mAction = pAction.trim();
	}

	/**
	 * Gets the shipping price.
	 *
	 * @return the mShippingPrice
	 */
	public  Double getShippingPrice() {
		return mShippingPrice;
	}

	/**
	 * Sets the shipping price.
	 *
	 * @param pShippingPrice the mShippingPrice
	 */
	public void setShippingPrice(Double pShippingPrice) {
		this.mShippingPrice = pShippingPrice;
	}

	/**
	 * Gets the region code.
	 *
	 * @return the mRegionCode
	 */
	public String getRegionCode() {
		return mRegionCode;
	}

	/**
	 * Sets the region code.
	 *
	 * @param pRegionCode the mRegionCode
	 */
	public void setRegionCode(String pRegionCode) {
		this.mRegionCode = pRegionCode;
	}


	/**
	 * Gets the ship method code.
	 *
	 * @return the mShipMethodCode
	 */
	public String getShipMethodCode() {
		return mShipMethodCode;
	}

	/**
	 * Sets the ship method code.
	 *
	 * @param pShipMethodCode the mShipMethodCode
	 */
	public void setShipMethodCode(String pShipMethodCode) {
		this.mShipMethodCode = pShipMethodCode;
	}
	/** Gets the ship method vo.
	 *
	 * @return the mShipMethodVO
	 */
	public TRUShipMethodVO getShipMethodVO() {
		return mShipMethodVO;
	}

	/**
	 * Sets the ship method vo.
	 *
	 * @param pShipMethodVO the mShipMethodVO to set
	 */
	public void setShipMethodVO(TRUShipMethodVO pShipMethodVO) {
		this.mShipMethodVO = pShipMethodVO;
	}

	/**
	 * Gets the tru csr purchase process helper.
	 *
	 * @return the truCSRPurchaseProcessHelper
	 */
	public TRUCSRPurchaseProcessHelper getTruCSRPurchaseProcessHelper() {
		return mTruCSRPurchaseProcessHelper;
	}

	/**
	 * Sets the tru csr purchase process helper.
	 *
	 * @param pTruCSRPurchaseProcessHelper the truCSRPurchaseProcessHelper to set
	 */
	public void setTruCSRPurchaseProcessHelper(
			TRUCSRPurchaseProcessHelper pTruCSRPurchaseProcessHelper) {
		this.mTruCSRPurchaseProcessHelper = pTruCSRPurchaseProcessHelper;
	}

	/**
	 * Gets the profile tools.
	 *
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * Gets the ship to address name.
	 *
	 * @return the shipToAddressName
	 */
	public String getShipToAddressName() {
		return mShipToAddressName;
	}

	/**
	 * Sets the ship to address name.
	 *
	 * @param pShipToAddressName            the shipToAddressName to set
	 */
	public void setShipToAddressName(String pShipToAddressName) {
		mShipToAddressName = pShipToAddressName;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * Sets the address.
	 *
	 * @param pAddress            - the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}


	/**
	 * Gets the new ship to address name.
	 *
	 * @return new shipping address.
	 */
	public String getNewShipToAddressName() {
		return mNewShipToAddressName;
	}

	/**
	 * Sets the new ship to address name.
	 *
	 * @param pNewShipToAddressName            - mNewShipToAddressName.
	 */
	public void setNewShipToAddressName(String pNewShipToAddressName) {
		mNewShipToAddressName = pNewShipToAddressName;

		if (mNewShipToAddressName != null) {
			mNewShipToAddressName = mNewShipToAddressName.trim();
		}
	}


	/**
	 * Gets the tru hardgood shipping group.
	 *
	 * @return mtruHardgoodShippingGroup.
	 */


	public TRUHardgoodShippingGroup getTruHardgoodShippingGroup() {
		return mTruHardgoodShippingGroup;
	}

	/**
	 * Sets the tru hardgood shipping group.
	 *
	 * @param pTruHardgoodShippingGroup  - mtruHardgoodShippingGroup
	 */

	public void setTruHardgoodShippingGroup(
			TRUHardgoodShippingGroup pTruHardgoodShippingGroup) {
		mTruHardgoodShippingGroup = pTruHardgoodShippingGroup;
	}


	/**
	 * Gets the error handler.
	 *
	 * @return the mErrorHandler
	 */
	public IErrorHandler getErrorHandler() {
		return mErrorHandler;
	}

	/**
	 * Sets the error handler.
	 *
	 * @param pErrorHandler the mErrorHandler to set
	 */
	public void setErrorHandler(IErrorHandler pErrorHandler) {
		mErrorHandler = pErrorHandler;
	}

	/**
	 * Gets the common success url.
	 *
	 * @return the mCommonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * Sets the common success url.
	 *
	 * @param pCommonSuccessURL            the mCommonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * Gets the common error url.
	 *
	 * @return the mCommonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * Sets the common error url.
	 *
	 * @param pCommonErrorURL   the mCommonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * mAddressValidated.
	 *
	 * @return mAddressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * mAddressValidated.
	 *
	 * @param pAddressValidated the mAddressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		mAddressValidated = pAddressValidated;
	}


	/**
	 * Gets the address doctor process status.
	 *
	 * @return mAddressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * Sets the address doctor process status.
	 *
	 * @param pAddressDoctorProcessStatus the mAddressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}


	/**
	 * Gets the TRU configuration.
	 *
	 * @return the mTRUConfiguration
	 */
	public TRUCSRConfiguration getTRUConfiguration() {
		return mTRUCSRConfiguration;
	}

	/**
	 * Sets the TRU configuration.
	 *
	 * @param pTRUCSRConfiguration the mTRUCSRConfiguration to set
	 */
	public void setTRUConfiguration(TRUCSRConfiguration pTRUCSRConfiguration) {
		mTRUCSRConfiguration = pTRUCSRConfiguration;
	}

	/**
	 * Gets the address doctor response vo.
	 *
	 * @return the mAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * Sets the address doctor response vo.
	 *
	 * @param pAddressDoctorResponseVO the mAddressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUCSRShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper            the shippingHelper to set
	 */
	public void setShippingHelper(TRUCSRShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * Gets the hard good shipping group.
	 *
	 * @return the hard good shipping group
	 */
	public HardgoodShippingGroup getHardGoodShippingGroup() {
		return mHardGoodShippingGroup;
	}

	/**
	 * Sets the hard good shipping group.
	 *
	 * @param pHardGoodShippingGroup the new hard good shipping group
	 */
	public void setHardGoodShippingGroup(
			HardgoodShippingGroup pHardGoodShippingGroup) {
		mHardGoodShippingGroup = pHardGoodShippingGroup;
	}

	/**
	 * Gets the address doctor.
	 *
	 * @return the address doctor
	 */
	public TRUCSRAddressDoctor getAddressDoctor() {
		return mAddressDoctor;
	}

	/**
	 * Sets the address doctor.
	 *
	 * @param pAddressDoctor the new address doctor
	 */
	public void setAddressDoctor(TRUCSRAddressDoctor pAddressDoctor) {
		mAddressDoctor = pAddressDoctor;
	}

	/**
	 * @return the shippingGroupsCount
	 */

	public int getShippingGroupsCount() {
		return mShippingGroupsCount;
	}

	/**
	 * @return the emptyShippingAddress.
	 */
	public boolean isEmptyShippingAddress() {
		return mEmptyShippingAddress;
	}

	/**
	 * @param pEmptyShippingAddress the emptyShippingAddress to set.
	 */
	public void setEmptyShippingAddress(boolean pEmptyShippingAddress) {
		mEmptyShippingAddress = pEmptyShippingAddress;
	}
	/**
	 * @return the mIncludeGifts.
	 */
	public boolean isIncludeGifts() {
		return mIncludeGifts;
	}
	/**
	 * @param pIncludeGifts the mIncludeGifts to set.
	 */
	public void setIncludeGifts(boolean pIncludeGifts) {
		this.mIncludeGifts = pIncludeGifts;
	}
	/**
	 * @return the mUniqueId.
	 */
	public String getUniqueId() {
		return mUniqueId;
	}
	/**
	 * @param pUniqueId the uniqueId to set.
	 */
	public void setUniqueId(String pUniqueId) {
		mUniqueId = pUniqueId;
	}	/**
		 * @return the ship to new address error URL.
		 */
		public String getShipToNewAddressErrorURL() {
			return mShipToNewAddressErrorURL;
		}

		/**
		 * @param pShipToNewAddressErrorURL
		 *            - the ship to new address error URL.
		 */
		public void setShipToNewAddressErrorURL(String pShipToNewAddressErrorURL) {
			mShipToNewAddressErrorURL = pShipToNewAddressErrorURL;
		}

	/**
	 * @return the billingAddressSameAsShippingAddress.
	 */
	public boolean isBillingAddressSameAsShippingAddress() {
		return mBillingAddressSameAsShippingAddress;
	}

	/**
	 * @param pBillingAddressSameAsShippingAddress
	 *            the billingAddressSameAsShippingAddress to set.
	 */
	public void setBillingAddressSameAsShippingAddress(
			boolean pBillingAddressSameAsShippingAddress) {
		mBillingAddressSameAsShippingAddress = pBillingAddressSameAsShippingAddress;
	}
	
	/**
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * @param pUserSession
	 *            the userSession to set.
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * @return the relationShipId
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}

	/**
	 * @param pRelationShipId the relationShipId to set.
	 */
	public void setRelationShipId(String pRelationShipId) {
		mRelationShipId = pRelationShipId;
	}

	/**
	 * @return the shippingManager.
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}
	/**
	 * @param pShippingManager
	 *            the shippingManager to set.
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		this.mShippingManager = pShippingManager;
	}

	/**
	 * mShippingManager.
	 */
	private TRUShippingManager mShippingManager;

	/** Property To Hold Wrap Commerce Item Id. */
	private String mWrapCommerceItemId;

	/**
	 * @return the wrapCommerceItemId.
	 */
	public String getWrapCommerceItemId() {
		return mWrapCommerceItemId;
	}
	/**
	 * @param pWrapCommerceItemId the wrapCommerceItemId to set.
	 */
	public void setWrapCommerceItemId(String pWrapCommerceItemId) {
		mWrapCommerceItemId = pWrapCommerceItemId;
	}
	/**
	 *
	 * @return orderReconciliationNeeded
	 */
	public boolean isOrderReconciliationNeeded() {
		return mOrderReconciliationNeeded;
	}
	/**
	 *
	 * @param pOrderReconciliationNeeded OrderReconciliationNeeded
	 */
	public void setOrderReconciliationNeeded(boolean pOrderReconciliationNeeded) {
		this.mOrderReconciliationNeeded = pOrderReconciliationNeeded;
	}

	/**
	 * property: flag indicating whether gift options page will be displayed.
	 */
	private boolean mShowMeGiftingOptions = false;

	/**
	 * @return the showMeGiftingOptions.
	 */
	public boolean isShowMeGiftingOptions() {
		return mShowMeGiftingOptions;
	}

	/**
	 * @param pShowMeGiftingOptions
	 *            the showMeGiftingOptions to set.
	 */
	public void setShowMeGiftingOptions(boolean pShowMeGiftingOptions) {
		mShowMeGiftingOptions = pShowMeGiftingOptions;
	}

	/**
	 * @return the mPropertyManager.
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set.
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}

	/**
	 * @return commerceItemRelationshipId
	 */
	public String getCommerceItemRelationshipId() {
		return mCommerceItemRelationshipId;
	}

	/**
	 * @param pCommerceItemRelationshipId the commerceItemRelationshipId to set
	 */
	public void setCommerceItemRelationshipId(String pCommerceItemRelationshipId) {
		mCommerceItemRelationshipId = pCommerceItemRelationshipId;
	}

	/**
	 * @return the mNavigateToMultiShip.
	 */
	public boolean isNavigateToMultiShip() {
		return mNavigateToMultiShip;
	}

	/**
	 * @param pNavigateToMultiShip the mNavigateToMultiShip to set.
	 */
	public void setNavigateToMultiShip(boolean pNavigateToMultiShip) {
		this.mNavigateToMultiShip = pNavigateToMultiShip;
	}
	

	/**
	 * Gets the inventory manager.
	 *
	 * @return the inventoryManager.
	 */
  public TRUCoherenceInventoryManager getInventoryManager() {
    return mInventoryManager;
  }

  /**
   * Sets the inventory manager.
   *
   * @param pInventoryManager - the inventoryManager to set.
   */
  public void setInventoryManager(TRUCoherenceInventoryManager pInventoryManager) {
    mInventoryManager = pInventoryManager;
  }
  

	/**
	 * @param pShippingGroupsCount the shippingGroupsCount to set
	 */
	public void setShippingGroupsCount(int pShippingGroupsCount) {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[setShippingGroupsCount]");
       }
		mShippingGroupsCount = pShippingGroupsCount;
		if (mShippingGroupsCount <= 0) {
			this.mShippingGroupsCount = 0;
			this.mStorePickUpInfos = null;
		} else {
			this.mShippingGroupsCount = pShippingGroupsCount;
			this.mStorePickUpInfos = new TRUStorePickUpInfo[this.mShippingGroupsCount];
			try {
				for (int index = 0; index < mShippingGroupsCount; ++index) {
					this.mStorePickUpInfos[index] = ((TRUStorePickUpInfo) Class
							.forName(TRUCommerceConstants.STORE_PICKUP_INFO_CLASS_NAME).newInstance());
				}
			} catch (InstantiationException insExe) {
				this.mStorePickUpInfos = null;
				vlogError("InstantiationException occurred in setShippingGroupsCount {0}", insExe);
			} catch (IllegalAccessException illExe) {
				this.mStorePickUpInfos = null;
				vlogError("IllegalAccessException occurred in setShippingGroupsCount {0}", illExe);
			} catch (ClassNotFoundException clsExe) {
				this.mStorePickUpInfos = null;
				vlogError("ClassNotFoundException occurred in setShippingGroupsCount {0}", clsExe);
			}
		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[setShippingGroupsCount]");
       }
	}
	
	/**
	 * @return the storePickUpInfos
	 */
	public TRUStorePickUpInfo[] getStorePickUpInfos() {
		return mStorePickUpInfos;
	}

	/**
	 * @param pStorePickUpInfos the storePickUpInfos to set
	 */
	public void setStorePickUpInfos(TRUStorePickUpInfo[] pStorePickUpInfos) {
		mStorePickUpInfos = pStorePickUpInfos;
	}

	/**
	 * @return the mLocationId
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * @param pLocationId the mLocationId to set
	 */
	public void setLocationId(String pLocationId) {
		this.mLocationId = pLocationId;
	}

	/**
	 * @return the mQuantity
	 */
	public long getQuantity() {
		return mQuantity;
	}

	/**
	 * @param pQuantity the mQuantity to set
	 */
	public void setQuantity(long pQuantity) {
		this.mQuantity = pQuantity;
	}

	/**
	 * Gets the shopping cart utils.
	 *
	 * @return the shopping cart utils
	 */
	public TRUShoppingCartUtils getShoppingCartUtils() {
		return mShoppingCartUtils;
	}

	/**
	 * Sets the shopping cart utils.
	 *
	 * @param pShoppingCartUtils
	 *            the new shopping cart utils
	 */
	public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
		this.mShoppingCartUtils = pShoppingCartUtils;
	}

	/**
	 * @return the bppItemInfoPropertyName
	 */
	public String getBppItemInfoPropertyName() {
		return mBppItemInfoPropertyName;
	}

	/**
	 * @param pBppItemInfoPropertyName the bppItemInfoPropertyName to set
	 */
	public void setBppItemInfoPropertyName(String pBppItemInfoPropertyName) {
		mBppItemInfoPropertyName = pBppItemInfoPropertyName;
	}
	
	/**
	 * @return the mRelationShipsToCopy
	 */
	public Map<String, Map<String, Object>> getRelationShipsToCopy() {
		return mRelationShipsToCopy;
	}
	/**
	 * @param pRelationShipsToCopy the mRelationShipsToCopy to set
	 */
	public void setRelationShipsToCopy(
			Map<String, Map<String, Object>> pRelationShipsToCopy) {
		this.mRelationShipsToCopy = pRelationShipsToCopy;
	}
	
	/**
	 * Gets the location tools.
	 *
	 * @return the locationTools
	 */
	public PARALStoreLocatorTools getLocationTools() {
		return mLocationTools;
	}

	/**
	 * Sets the location tools.
	 *
	 * @param pLocationTools            the locationTools to set
	 */
	public void setLocationTools(PARALStoreLocatorTools pLocationTools) {
		mLocationTools = pLocationTools;
	}

    /**
     *  This method is overridden for address doctor validation.
     *
     *
     * @param pRequest
     *            the servlet's request
     * @param pResponse
     *            the servlet's response
     * @throws ServletException
     *             if an error occurred while processing the servlet request
     * @throws IOException
     *             if an error occurred while reading or writing the servlet
     *
     */
    public void preApplyShippingGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preApplyShippingGroups]");
       }
     super.preApplyShippingGroups(pRequest, pResponse);
     
     try {
			TRUHardgoodShippingGroup  oldShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
			final List<TRUShippingGroupCommerceItemRelationship> sgCiRels = ((TRUShippingGroupManager)getShippingGroupManager()).getOnlyShipToHomeItemRelationShips(getOrder());
			if (sgCiRels != null && sgCiRels.size() > TRUConstants._1 && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) sgCiRels.get(i).getCommerceItem();
					final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
					if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
						continue;
					}
					/*if (null !=  getShippingManager().isItemQualifyForShipMethod(commerceItem, oldShippingGroup, getOrder())) {
						String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHODS_MULTISHIP);
						addFormException(new DropletException(errorMessage, commerceItem.getId()));
						if (isLoggingInfo()) {
							vlogInfo("{0} is not eligible for commerceItem {1} ,So order reconciliation required .",
									oldShippingGroup.getShippingMethod(), commerceItem.getId());
						}
						errorMessage = null;
						break;
					}	*/					
				}
			}
		} catch (CommerceException e) {			
			addFormException(new DropletException(PLEASE_TRY_AGAIN));
			vlogError("Exception Occured in @Class:::TRUShippingGroupFormHandler::@method::isOrderReconciliationRequired()", e);
		}
            if (isLoggingDebug()) {
                logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[preApplyShippingGroups]");
             }
}

	/**
	 * Valid address.
	 *
	 * @param pOrderShippingGroup
	 *            a TRUHardgoodShippingGroup value.
	 * @return addressStatus
	 *
	 */

	public String validAddress(TRUHardgoodShippingGroup pOrderShippingGroup) {

        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[validAddress]");
       }
		vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ",
				pOrderShippingGroup);
		String addressStatus = null;
		if (pOrderShippingGroup != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1,pOrderShippingGroup.getShippingAddress().getAddress1());
			addressValue.put(TRUConstants.ADDRESS_2,pOrderShippingGroup.getShippingAddress().getAddress2());
			addressValue.put(TRUConstants.CITY, pOrderShippingGroup.getShippingAddress().getCity());
			addressValue.put(TRUConstants.STATE, pOrderShippingGroup.getShippingAddress().getState());
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pOrderShippingGroup.getShippingAddress().getPostalCode());
			addressStatus = getProfileTools().validateAddress(addressValue);
		}
		vlogDebug("Returns addressStatus: {0} ",addressStatus);
		if (addressStatus == null) {
			addressStatus = TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR;
		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[validAddress]");
       }
		return addressStatus;

	}

    /**
    * Post apply shipping groups.
    *
    *
    * @param pRequest
    *            the servlet's request
    * @param pResponse
    *            the servlet's response
    * @throws ServletException
    *             if an error occurred while processing the servlet request
    * @throws IOException
    *             if an error occurred while reading or writing the servlet
    *             
    */
	public void postApplyShippingGroups(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException
	{
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[postApplyShippingGroups]");
       }
		super.postApplyShippingGroups(pRequest, pResponse);
		if (getFormError()) {
			return;
		}

		Order order = getOrder();

		CSRAgentTools agenttools = getCSRAgentTools();
		CSREnvironmentTools csrenvtools = agenttools.getCSREnvironmentTools();
		TRUCSRPurchaseProcessHelper helper = getTruCSRPurchaseProcessHelper();
		try
		{
			Locale agentLocale = csrenvtools.getEnvironmentTools().getUserLocale();

			agenttools.recreateInitialDetailsForItemsMarkedAsFinal(order, agentLocale, getProfile());

			agenttools.repriceOrder(getPostApplyShippingGroupsPricingOp(),
									order,
									getUserPricingModels(),
									agentLocale,
									getProfile(),
									csrenvtools.getEnvironmentTools().getAgentProfile(), 
									null,
									this);
			helper.updateStates(order);
		}
		catch (CommerceException e) {
			processException(e, TRUCSCConstants.ERROR_APPLYING_SHIPPING_GROUP, pRequest, pResponse);
		}

		if (getFormError()) {
			return;
		}

		if (!isPersistOrder()) {
			return;
		}
		try {
			getCSRAgentTools().persistCurrentOrder();
		}
		catch (CommerceException ce) {
			processException(ce, TRUCSCConstants.ERROR_WHILE_PERSISTING_ORDER2, pRequest, pResponse);
		}
        if (isLoggingDebug()) {
        	logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[postApplyShippingGroups]");
       }
	}

	/**
	 * to validate all the required field to change ship method.
	 *
	 * @param pRequest - request
	 * @param pResponse -response
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void preChangeShipMethod(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preChangeShipMethod]");
       }
		
		if (getShipMethodVO() != null) {
			Double shipPrice = getShipMethodVO().getShippingPrice();
			if(StringUtils.isBlank(getShipMethodVO().getShipMethodCode()) ||
			   StringUtils.isBlank(getShipMethodVO().getRegionCode()) ||
			   shipPrice == null) {
				mVe.addValidationError(
						TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			}
			getErrorHandler().processException(mVe, this);
		}
        if (isLoggingDebug()) {
        	logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[preChangeShipMethod]");
       }
	}

    /**
    * Handle change shipping method.
    *
    *
    * @param pRequest
    *            the servlet's request
    * @param pResponse
    *            the servlet's response
    *
    * @return true, if successful
    *
    * @throws ServletException
    *             if an error occurred while processing the servlet request
    * @throws IOException
    *             if an error occurred while reading or writing the servlet
    *
    */
	public boolean handleChangeShippingMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleChangeShippingMethod]");
       }
		String myHandleMethod = "TRUShippingGroupFormHandler.handleChangeShipMethod";
		double selectedShippingPrice = 0;
		boolean returnVal = true;
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (isLoggingDebug()) {
					logDebug("handleChangeShipMethod()");
				}

				if (!StringUtils.isBlank(getAction())
						&& getAction().contains(TRUCSCConstants.DEFAULT)) {
					if (isLoggingDebug()) {
						vlogDebug("***** handleApplyShippingMethods due to empty action *****");
					}
					//manage gifting options
					setAction(getAction().replace(TRUCSCConstants.DEFAULT, TRUConstants.EMPTY_STRING));
					returnVal = handleApplyShippingMethods(pRequest,pResponse);
				}
				else{
					if (getShippingPrice()!=null) {
						selectedShippingPrice = Double.valueOf(getShippingPrice());
					}
					try{
						synchronized (getOrder()) {
							TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
							if (hardgoodShippingGroup != null) {
								hardgoodShippingGroup.setShippingPrice(selectedShippingPrice);
								hardgoodShippingGroup.setShippingMethod(getShipMethodCode());
								hardgoodShippingGroup.setRegionCode(getRegionCode());
								runProcessRepriceOrder(getOrder(),
													getUserPricingModels(),
													getLocale(),
													getProfile(),
													createRepriceParameterMap());
								getOrderManager().updateOrder(getOrder());
							}
						}
					}catch (CommerceException pCommerceException) {
						if (isLoggingError()) {
							logError("CommerceException in TRUShippingGroupFormHandler.handleShipToNewAddress", pCommerceException);
						}
					}
					catch (RunProcessException pRunProcessException) {
						if (isLoggingError()) {
							logError("RunProcessException in TRUShippingGroupFormHandler.handleShipToNewAddress", pRunProcessException);
						}
					}
					returnVal= checkFormRedirect(getCommonSuccessURL(),	getCommonErrorURL(), pRequest, pResponse);
				}
			}   finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
        if (isLoggingDebug()) {
        	logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[handleChangeShippingMethod]");
       }
		return returnVal;

	}



	/**
	    * PreMutlipleShippingGroupCheckout.
	    *
	    *
	    * @param pRequest
	    *            the servlet's request
	    * @param pResponse
	    *            the servlet's response
	    *
	    * @throws ServletException
	    *             if an error occurred while processing the servlet request
	    * @throws IOException
	    *             if an error occurred while reading or writing the servlet
	    *
	    */
	public void preMultipleShippingGroupCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException,IOException {
		 if (isLoggingDebug()) {
	        	logDebug("Entering to class:[TRUCSRShippingGroupFormHandler] method:[preMultipleShippingGroupCheckout]");
	     }
		if(checkForInternationalAddress() && checkForEligibilityOfItems()){	 		 
			Order order = getShoppingCart().getCurrent();
			if (null != order && order.getCommerceItems() != null){
				List<CommerceItem> commerceItems = order.getCommerceItems();
				try {
					for (CommerceItem commerceItem : commerceItems) {
						if(!(commerceItem.getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.GIFTWRAP_COMMERCE_ITEM))){
						List<TRUShippingGroupCommerceItemRelationship> relationShipObjList = commerceItem.getShippingGroupRelationships();
						if(relationShipObjList != null){
							for (TRUShippingGroupCommerceItemRelationship relationShipObj : relationShipObjList) {
								if(!(relationShipObj.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.GIFTWRAP_COMMERCE_ITEM))){
								 //relationShipObj.getGiftItemInfo();
								String relationShipId = relationShipObj.getId();
								Map<String, Object> copyRelValues = new HashMap<String, Object>();
								TRUShippingGroupCommerceItemRelationship selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) order.getRelationship(relationShipId);
								//selectedShippingGroupRel.getGiftItemInfo();
								if((null != selectedShippingGroupRel.getBppItemInfo()   && StringUtils.isNotBlank(selectedShippingGroupRel.getBppItemInfo().getId())&& StringUtils.isNotBlank(selectedShippingGroupRel.getBppItemInfo().getBppItemId())) || relationShipObj.getGiftItemInfo() != null){
									List<String> copyProperties = getTRUConfiguration().getCopyShipItemRelProperties();
									synchronized (this) {	
										for (String copyProperty : copyProperties) {
											Object value = DynamicBeans.getPropertyValue(selectedShippingGroupRel, copyProperty);
											if (value != null) {
												copyRelValues.put(copyProperty, value);
											}
										}
									}
								}
								//empty check for copyRelValues
								if(copyRelValues.size() > 0){
									getRelationShipsToCopy().put(commerceItem.getId(), copyRelValues);
								}
							}
							}
						}
					}
					}
				}catch (PropertyNotFoundException e) {
					if (isLoggingError()) {
						vlogError("PropertyNotFoundException occured in TRUOrderTools.copyShippingRelationShip()",e);
						}
						if (isLoggingDebug()) {
							vlogDebug("@Class::TRUOrderTools::@method::copyShippingRelationShip() : END");
						}
				}catch (CommerceException e) {
					if (isLoggingError()) {
						logError("CommerceException in @Class::TRUCSRShippingGroupFormHandler::@method::changeShippingGroup()", e);
					}
				} 
			}
					
		}
		 if (isLoggingDebug()) {
	        	logDebug("Exiting from class:[TRUCSRShippingGroupFormHandler] method:[preMultipleShippingGroupCheckout]");
	     }
	}
	
	/**
	 * This will check to restrict the international shipping in case of multi shipping.
	 * @return checkForInternationalAddress
	 */
	private boolean checkForInternationalAddress(){
		if (isLoggingDebug()) {
        	logDebug("Entering to class:[TRUCSRShippingGroupFormHandler] method:[checkForInternationalAddress]");
		}
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				getShippingGroupMapContainer();
		List commerceItemShippingInfos = shippingGroupMapContainer.getAllCommerceItemShippingInfos();
		Iterator iter = commerceItemShippingInfos.listIterator();
	    while (iter.hasNext())
	    {
	      CommerceItemShippingInfo cisi = (CommerceItemShippingInfo)iter.next();
	      ShippingGroup shippingGroup = getShippingGroupByAddressKey(cisi.getShippingGroupName(), shippingGroupMapContainer);
	      if(shippingGroup instanceof TRUHardgoodShippingGroup){
	    	  TRUHardgoodShippingGroup hgShippingGroup = (TRUHardgoodShippingGroup)shippingGroup;
	    	  
	    	  if(!hgShippingGroup.getShippingAddress().getCountry().equalsIgnoreCase(TRUCSCConstants.COUNTRY)&& !hgShippingGroup.getShippingAddress().getCountry().equalsIgnoreCase(TRUCSCConstants.UNITED_STATES)){
					addFormException(new DropletException(TRUCSCConstants.CANT_SHIPTO_INTERNATIONAL_ADDRESS));
				 }
	    	  
	      }
	    }
	    if(getFormError()){
	    	return false;
	    }
	    if (isLoggingDebug()) {
        	logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[checkForInternationalAddress]");
		}
	    return true;
	}
	
	/**
	 * This method checks for the eligibility of the items before proceeding with multishipment.
	 * @return checkForEligibilityOfItems
	 */
	private boolean checkForEligibilityOfItems(){
		if (isLoggingDebug()) {
        	logDebug("Entering to class:[TRUCSRShippingGroupFormHandler] method:[checkForEligibilityOfItems]");
		}
		 final TRUCoherenceInventoryManager invManager = getInventoryManager();
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				getShippingGroupMapContainer();
		List commerceItemShippingInfos = shippingGroupMapContainer.getAllCommerceItemShippingInfos();
		Iterator iter = commerceItemShippingInfos.listIterator();
	    while (iter.hasNext())
	    {
	     long availableStockAtLocation=0;
	      CommerceItemShippingInfo cisi = (CommerceItemShippingInfo)iter.next();
	      if(!cisi.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
	      ShippingGroup shippingGroup = getShippingGroupByAddressKey(cisi.getShippingGroupName(), shippingGroupMapContainer);
	      if(shippingGroup instanceof TRUInStorePickupShippingGroup){
	    	 boolean eligibleForISPU = getShippingHelper().isProductEligibleForISPUByCISI(cisi);
			try {
				int availability = invManager.queryAvailabilityStatus(cisi.getCommerceItem().getCatalogRefId(),  cisi.getShippingGroupName());
				availableStockAtLocation = invManager.queryStockLevel(cisi.getCommerceItem().getCatalogRefId(), cisi.getShippingGroupName());
				if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK && cisi.getShippingGroupName() != null){
					Double storeItemWareHouseLocationId = getShippingHelper().getStoreItemWareHouseLocationId(cisi.getShippingGroupName());
					availableStockAtLocation=invManager.queryStockLevel(cisi.getCommerceItem().getCatalogRefId(),Long.toString(Math.round(storeItemWareHouseLocationId)));
				}
			} catch (InventoryException e) {
				availableStockAtLocation = TRUCSCConstants.LONG_MINUS_ONE;
				if (isLoggingError()) {
					logError("InventoryException while getting the inventory for an item : "+cisi.getCommerceItem().getCatalogRefId()+" at location : "+cisi.getShippingGroupName(),e);
				}
				} catch (RequestTimeoutException rte) {
				availableStockAtLocation = TRUCSCConstants.LONG_MINUS_ONE;
				if (isLoggingError()) {
					logError("RequestTimeoUt Exception: "+cisi.getCommerceItem().getCatalogRefId()+" at location : "+cisi.getShippingGroupName(),rte);
				}
			}
			if(!eligibleForISPU){
				addFormException(new DropletException(TRUCSCConstants.ITEM_STRING+cisi.getCommerceItem().getCatalogRefId()+TRUCSCConstants.ITEM_NOT_ELIGIBLEFOR_ISPU_CHOOSE_DIFF_ADDR));
				return false;
			}else if(eligibleForISPU && cisi.getQuantity() > availableStockAtLocation){
				addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+cisi.getCommerceItem().getCatalogRefId()+TRUCSCConstants.AT_SELECTED_LOC+cisi.getShippingGroupName()));
				return false;
			}
	      }else if(shippingGroup instanceof TRUHardgoodShippingGroup && !cisi.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.GIFTWRAP_COMMERCE_ITEM)){
	    		  try {
	    			  availableStockAtLocation = invManager.queryStockLevel(cisi.getCommerceItem().getCatalogRefId());
	    		  } catch (InventoryException e) {
	    			  availableStockAtLocation = TRUCSCConstants.LONG_MINUS_ONE;
	    			  if (isLoggingError()) {
	    			  logError("InventoryException while getting the inventory for an item : "+cisi.getCommerceItem().getCatalogRefId()+" at location : "+cisi.getShippingGroupName(),e);
	    			  }
	    		  }
	    		  if(cisi.getCommerceItem().getQuantity() > availableStockAtLocation){
	    			  addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+cisi.getCommerceItem().getCatalogRefId()));
	    			  return false;
	    	  }
	      }
	      }
	    }
	    if (isLoggingDebug()) {
        	logDebug("Exiting from class:[TRUCSRShippingGroupFormHandler] method:[checkForEligibilityOfItems]");
		}
		return true;
	}
	
	
	/**
	    * postMutlipleShippingGroupCheckout.
	    *
	    *
	    * @param pRequest
	    *            the servlet's request
	    * @param pResponse
	    *            the servlet's response
	    *
	    * @throws ServletException
	    *             if an error occurred while processing the servlet request
	    * @throws IOException
	    *             if an error occurred while reading or writing the servlet
	    *
	    */
	public void postMultipleShippingGroupCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException{
		
		 if (isLoggingDebug()) {
	        	logDebug("Entering to class:[TRUCSRShippingGroupFormHandler] method:[postMultipleShippingGroupCheckout]");
	     }
		Order order = getShoppingCart().getCurrent();
		for (String commerceItemId : getRelationShipsToCopy().keySet()) {
			CommerceItem commerceItem;
			try {
				commerceItem = order.getCommerceItem(commerceItemId);
				List<TRUShippingGroupCommerceItemRelationship> relationShipObjList = commerceItem.getShippingGroupRelationships();
				for (TRUShippingGroupCommerceItemRelationship relationShipObj : relationShipObjList) {
					Map<String,Object> propertiesToCopy = getRelationShipsToCopy().get(commerceItemId);
					Iterator entries = propertiesToCopy.entrySet().iterator();
					while(entries.hasNext()){
						Map.Entry entry = (Map.Entry) entries.next();
						String key = (String)entry.getKey();
						Object value = entry.getValue();
						DynamicBeans.setPropertyValue(relationShipObj, key, value);
					}
					
					// calling copyShippingBPPRelationShip() to get BPP Info
					((TRUCSROrderTools)getOrderManager().getOrderTools()).copyShippingBPPRelationShip(relationShipObj);
					
					logDebug("Ship Method before runProcessRepriceOrder () .... : "+relationShipObj.getShippingGroup().getShippingMethod());
					if(relationShipObj.getShippingGroup() instanceof HardgoodShippingGroup){
						getShippingHelper().setShipMethodAndPrice(getOrder(), commerceItem, (TRUHardgoodShippingGroup)relationShipObj.getShippingGroup(),
								((HardgoodShippingGroup)relationShipObj.getShippingGroup()).getShippingMethod());
						}
					
					
				}
				runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(),	getProfile(), createRepriceParameterMap());
				((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
				getOrderManager().updateOrder(getOrder());
			} catch (CommerceItemNotFoundException e) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException occured in TRUCSRShippingGroupFormHandler.postMultipleShippingGroupCheckout()",e);
					}
			}
			 catch (InvalidParameterException e) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException occured in TRUCSRShippingGroupFormHandler.postMultipleShippingGroupCheckout()",e);
					}
			} catch (PropertyNotFoundException e) {
				if (isLoggingError()) {
					vlogError("PropertyNotFoundException occured in TRUCSRShippingGroupFormHandler.postMultipleShippingGroupCheckout()",e);
					}
			} catch (RunProcessException e) {
				if (isLoggingError()) {
					vlogError("RunProcessException occured in TRUCSRShippingGroupFormHandler.postMultipleShippingGroupCheckout()",e);
					}
			} catch (CommerceException e) {
				if (isLoggingError()) {
					vlogError("CommerceException occured in TRUCSRShippingGroupFormHandler.postMultipleShippingGroupCheckout()",e);
					}
			}
		}
		
			 if (isLoggingDebug()) {
	        	logDebug("Exiting from class:[TRUCSRShippingGroupFormHandler] method:[postMultipleShippingGroupCheckout]");
	     }
	}
	
	
	/**
	 * Handle multiple shipping group checkout.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see atg.commerce.csr.order.CSRShippingGroupFormHandler#handleMultipleShippingGroupCheckout(
	 * atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	public boolean handleMultipleShippingGroupCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException,IOException {
        if (isLoggingDebug()) {
        	logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleMultipleShippingGroupCheckout]");
       }
		String myHandleMethod = "TRUShippingGroupFormHandler.handleMultipleShippingGroupCheckout";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (isLoggingDebug()) {
					logDebug("Start :: TRUCSRShippingGroupFormHandler.handleMultipleShippingGroupCheckout");
				}
				((TRUOrderImpl) getOrder()).setCurrentTabInCSC(TRUCSCConstants.PAYMENT_TAB);
				
				 super.handleMultipleShippingGroupCheckout(pRequest, pResponse);
				int count=0;
			
				List<ShippingGroup> sgList=getOrder().getShippingGroups();
		
					for(ShippingGroup sg :sgList){
						if(sg instanceof TRUHardgoodShippingGroup){
						((TRUHardgoodShippingGroup) sg).setNickName(TRUCSCConstants.NICK_NAME+count);
							count++;
						}
					}
			} 
			finally {
				if(getFormError()){
					
					Vector<DropletException> validationErrors = getFormExceptions();
					boolean isTaxwareError = Boolean.FALSE;
					boolean isZipCodeError = Boolean.FALSE;
					if(validationErrors != null && !validationErrors.isEmpty()){
						for(DropletException error: validationErrors){
							String zipcodeError = error.getMessage();
							if(zipcodeError != null && zipcodeError.contains(TRUTaxwareConstant.TRU_TAXWARE_GENERAL_DATA_ERROR)) {
								isTaxwareError = Boolean.TRUE;
								resetFormExceptions();
								break;
							}
							if(zipcodeError != null && zipcodeError.contains(TRUCSCConstants.SHIPMENT)){
								isZipCodeError = Boolean.TRUE;
								resetFormExceptions();
								break;
							}
						}
					}
					if(isTaxwareError) {
						addFormException(new DropletException(TRUCSCConstants.TAXWARE_ADDRESS_INVALID_ERROR_MESSAGE));
					}
					if(isZipCodeError) {
						addFormException(new DropletException(TRUCSCConstants.SHIPPING_ERROR+TRUCSCConstants.SHIPPING_RESTRICTON+TRUCSCConstants.CART_LINK));
					}
						
				}
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
        if (isLoggingDebug()) {
         logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[handleMultipleShippingGroupCheckout]");
        }
		return  false;

	}
	
	
	/**
	 * Overriding findShippingAddressDestinationURL for not redirect to payment page from shipping address page as per OOTB.
	 *
	 * @return destinationURL
	 */
	@Override
	protected String findShippingAddressDestinationURL(){
		 if (isLoggingDebug()) {
	         logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[findShippingAddressDestinationURL]");
	        }
	   String destinationURL = null;
	      destinationURL = getShippingMethodURL();
	      if (isLoggingDebug()) {
	          logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[findShippingAddressDestinationURL]");
	         }
	    return destinationURL;
	  }


	
	/**
	 * Handle 'Add Address in Profile' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	
	public boolean handleAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleAddShippingAddress]");
       }
		String myHandleMethod = "TRUShippingGroupFormHandler.handleAddShippingAddress";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		
		
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in addShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					preAddShippingAddress(pRequest, pResponse);
					
				
	
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in preAddShippingAddress.");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
				
					addShippingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in addShippingAddress");
						}
						return checkFormRedirect(getCommonSuccessURL(),
								getCommonErrorURL(), pRequest, pResponse);
					}
					postAddShippingAddress(pRequest, pResponse);
					
					getOrderManager().updateOrder(getOrder());
					
					
				}
				}catch (CommerceException e) {
				if (isLoggingError()) {
				vlogError("CommerceException in TRUShippingGroupFormHandler.handleAddShippingAddress", e);
				}
			} 
			
			finally {
				if (tr != null)
				{
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
			
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[handleAddShippingAddress]");
       }
		return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(),
				pRequest, pResponse);
		
	}
	
	/**
	 * addShippingAddress 
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	
	public void addShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[addShippingAddress]");
       }
		try {
			
			String nickName;
			if(!StringUtils.isBlank(getNewShipToAddressName())){
				
				nickName = getNewShipToAddressName();
			}
			else{
				//nickName = getProfileTools().generateUniqueNickname(getAddress().getFirstName()+ TRUConstants.WHITE_SPACE + getAddress().getLastName());
				if(numberCount>=TRUCSCConstants.NUMBER_ONE){
					nickName =   getAddress().getFirstName()+getAddress().getLastName()+numberCount;
					}
					else{
						nickName =   getAddress().getFirstName()+getAddress().getLastName();
					}
				numberCount = numberCount+TRUCSCConstants.NUMBER_ONE;
			}
			
			TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
			
			getShippingHelper().addShippingAddress(getProfile(),nickName, getAddress(),shippingGroupMapContainer,isSaveShippingAddress());
			
			// update nick name to shipping group
			ShippingGroup shippingGroup = shippingGroupMapContainer.getShippingGroup(nickName);
			if (shippingGroup instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
				shippingGroupMapContainer.setSelectedAddressNickName(nickName);
				hardgoodShippingGroup.setNickName(nickName);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError("CommerceException in TRUShippingGroupFormHandler.addShippingAddress", e);
			}
		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[addShippingAddress]");
       }}
	
	/**
	 * postAddShippingAddress 
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	
	public void postAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("START of TRUShippingGroupFormHandler.postAddShippingAddress method");
		}
		if(isLoggingDebug()){
			logDebug("END of TRUShippingGroupFormHandler.postAddShippingAddress method");
		}
	}
	
	/**
	 * preAddShippingAddress 
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	public void preAddShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preAddShippingAddress]");
       }
		/*if (!getFormError()) {
			mVe.addValidationError(
					TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
			getErrorHandler().processException(mVe, this);
		}*/
		String nickName;
		if(!StringUtils.isBlank(getNewShipToAddressName())){
			
			nickName = getNewShipToAddressName();
		}
		else{
			//nickName = getProfileTools().generateUniqueNickname(getAddress().getFirstName()+ TRUConstants.WHITE_SPACE + getAddress().getLastName()+numberCount);
			
				if(numberCount>=TRUCSCConstants.NUMBER_ONE){
					nickName =   getAddress().getFirstName()+getAddress().getLastName()+numberCount;
					}
					else{
						nickName =   getAddress().getFirstName()+getAddress().getLastName();
					}
				//numberCount = numberCount+1;
		}
		if (!getFormError()) {
  			validateDuplicateAddressNickname(nickName, pRequest, pResponse);
  		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[preAddShippingAddress]");
       }
    }
	
	/**
	 * Handle 'Ship to existing address' case.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * 
	 * @return redirection result.
	 * 
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.

	 */
	public boolean handleShipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleShipToExistingAddress]");
       }
		String myHandleMethod = "TRUShippingGroupFormHandler.handleShipToExistingAddress";
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		boolean hasException = false;
		getAddressDoctor().setTaxwareError(false);
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					preShipToExistingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in field validation() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					((TRUOrderImpl) getOrder()).setCurrentTabInCSC(TRUCSCConstants.PAYMENT_TAB);
					shipToExistingAddress(pRequest, pResponse);
					if (getFormError()) {
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in handleShipToExistingAddress() method");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					runProcessValidationShippingInfos(pRequest, pResponse);
					if (getFormError()) {
						Vector<DropletException> validationErrors = getFormExceptions();
						boolean isZipcodeError = Boolean.FALSE;
						if(validationErrors != null && !validationErrors.isEmpty()){
							for(DropletException error: validationErrors){
								String zipcodeError = error.getMessage();
								if(zipcodeError.contains(TRUCSCConstants.SHIPMENT)){
									isZipcodeError = Boolean.TRUE;
									resetFormExceptions();
									break;
								}
							}
						}
						if(isZipcodeError) {
							addFormException(new DropletException(TRUCSCConstants.SHIPPING_ERROR+TRUCSCConstants.SHIPPING_RESTRICTON+TRUCSCConstants.CART_LINK));
						}
						
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in runProcessValidateShippingGroups");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
					
					//setOrderReconciliationNeeded(isOrderReconciliationRequired());
					if (isLoggingDebug()) {
						vlogDebug("isOrderReconciliationNeeded() ? {0}", isOrderReconciliationNeeded());
					}
					postShipToExisitngAddress(pRequest, pResponse);
					getShippingGroupManager().removeEmptyShippingGroups(getOrder());
					runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap_tax());
					((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
					getOrderManager().updateOrder(getOrder());
					if (getFormError()) {
						getAddressDoctor().setTaxwareError(true);
						if (isLoggingDebug()) {
							logDebug("Redirecting due to form error in runProcessValidateShippingGroups");
						}
						return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
					}
				}
			} catch (RunProcessException e) {
				 hasException = true;
				addFormException(new DropletException(TRUCSCConstants.PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("RunProcessException in TRUShippingGroupFormHandler.handleShipToExistingAddress", e);
				}
			}  catch (CommerceException e) {
				 hasException = true;
				addFormException(new DropletException(TRUCSCConstants.PLEASE_TRY_AGAIN));
				if (isLoggingError()) {
					logError("CommerceException in TRUShippingGroupFormHandler.handleShipToExistingAddress", e);
				}
			} finally {
				checkForTaxwareError();
				if (tr != null) {
					if (hasException) {
						try {
							setTransactionToRollbackOnly();
						} catch (SystemException e) {
							if (isLoggingError()) {
								logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
							}
						}
					}
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[handleShipToExistingAddress]");
       }
		return checkFormRedirect(getCommonSuccessURL(),getCommonErrorURL(), pRequest, pResponse);
	}
	
	
	
	protected Map createRepriceParameterMap_tax() {
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRShippingGroupFormHandler (createRepriceParameterMapToSkipTaxCal) : BEGIN");
		}
		Map parameters;
		parameters = super.createRepriceParameterMap();
		if (parameters == null) {
			parameters = new HashMap();
		}
		parameters.put(TRUTaxwareConstant.CALC_TAX, Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("TRUCSRShippingGroupFormHandler (createRepriceParameterMapToSkipTaxCal) : END");
		}
		return parameters;
	}
	
	
	/**
	 * This method will check the taxware generator error in the form exceptions and will add the user informatic error message.
	 */
	private void checkForTaxwareError(){
		if(getFormError()){
			Vector<DropletException> validationErrors = getFormExceptions();
			boolean isTaxwareError = Boolean.FALSE;
			if(validationErrors != null && !validationErrors.isEmpty()){
				for(DropletException error: validationErrors){
					String taxwareError = error.getMessage();
					if(taxwareError != null && taxwareError.contains(TRUTaxwareConstant.TRU_TAXWARE_GENERAL_DATA_ERROR)) {
						isTaxwareError = Boolean.TRUE;
						resetFormExceptions();
						break;
					}
				}
			}
			if(isTaxwareError) {
				addFormException(new DropletException(TRUCSCConstants.TAXWARE_ADDRESS_INVALID_ERROR_MESSAGE));
			}
		}
	}
	
	/**
	 * <p>
	 * This method will check whether all the cart items or eligible for selected shipping method or not.
	 * </p>.
	 *
	 * @return boolean
	 */
	private boolean isOrderReconciliationRequired() {
		boolean isOrderReconciliationRequired = false;
		//TRUHardgoodShippingGroup  oldShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
		final List<TRUShippingGroupCommerceItemRelationship> sgCiRels = ((TRUShippingGroupManager)getShippingGroupManager()).getOnlyShipToHomeItemRelationShips(getOrder());
		if (sgCiRels != null && sgCiRels.size() > TRUConstants._1 && !sgCiRels.isEmpty()) {
			final int sgCiRelsCount = sgCiRels.size();
			for (int i = 0; i < sgCiRelsCount; ++i) {
				final TRUCommerceItemImpl commerceItem = (TRUCommerceItemImpl) sgCiRels.get(i).getCommerceItem();
				final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
				if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
					continue;
				}
				TRUHardgoodShippingGroup  oldShippingGroup=(TRUHardgoodShippingGroup)sgCiRels.get(i).getShippingGroup();
				if (null !=  getShippingManager().isItemQualifyForShipMethod(commerceItem, oldShippingGroup, getOrder())) {
					String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_PREFERRED_SHIP_METHOD);
					addFormException(new DropletException(errorMessage, commerceItem.getId()));
					if (isLoggingInfo()) {
						vlogInfo("{0} is not eligible for commerceItem {1} ,So order reconciliation required .",
								oldShippingGroup.getShippingMethod(), commerceItem.getId());
					}
					isOrderReconciliationRequired = true;
					errorMessage = null;
					break;
				}						
			}
		}
		return isOrderReconciliationRequired;
	}
	
	/**
	 * Performs input data validations for existing shipping address specified
	 * by shopper
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 */
	@SuppressWarnings("unchecked")
	protected void preShipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preShipToExistingAddress]");
       }
		Iterator i$;
	    Object mapEntry;
	    Map.Entry sgMapEntry;
	    TRUOrderImpl order = (TRUOrderImpl) getShoppingCart().getCurrent();
	    ShippingGroupMapContainer sgc = getShippingGroupMapContainer();
	    order.getShippingGroups();
	    for (Iterator iterator = order.getShippingGroups().iterator(); iterator.hasNext();) {
			ShippingGroup sg = (ShippingGroup) iterator.next();
			sgc.addShippingGroup(sg.getId(), sg);
			
		}
	    Map<String, String> map = new HashMap<String, String>();
	    map = sgc.getShippingGroupMap();
	    /* This method will validates the shipping group in the order and 
	    	changes the shipping group of the commerce item if required. */
	    checkForShippingGroup(order, pRequest, pResponse);
	   
	    List<ShippingGroup> shippingGroups = order.getShippingGroups();
		boolean isEnableAddressDoctor = false;
		
		ShippingGroup shippingGroupToCheckAdd = sgc.getShippingGroup(getShipToAddressNickname());
		if(shippingGroupToCheckAdd != null && !(shippingGroupToCheckAdd instanceof TRUInStorePickupShippingGroup)){
			TRUHardgoodShippingGroup containerHGShippingGroup = (TRUHardgoodShippingGroup) sgc.getShippingGroup(getShipToAddressNickname());
			if(!containerHGShippingGroup.getShippingAddress().getCountry().equalsIgnoreCase(TRUCSCConstants.COUNTRY)&& !containerHGShippingGroup.getShippingAddress().getCountry().equalsIgnoreCase(TRUCSCConstants.UNITED_STATES)){
				addFormException(new DropletException(TRUCSCConstants.CANT_SHIPTO_INTERNATIONAL_ADDRESS));
			 }
		}
		
		
		
		if(shippingGroups!=null && !shippingGroups.isEmpty()) {                    
             for(ShippingGroup shippingGroup : shippingGroups) {
            	 if(shippingGroup instanceof TRUHardgoodShippingGroup && shippingGroup.getCommerceItemRelationshipCount() > 0) {
            		 TRUHardgoodShippingGroup shippingGroupHG = (TRUHardgoodShippingGroup) shippingGroup;
		     		/*if(SiteContextManager.getCurrentSite() != null){
						Object value=SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getEnableAddressDoctorPropertyName());
						Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
						if(value!=null){
							isEnableAddressDoctor = (boolean)value;
						}						
					}*/
		     		
		     		

                    boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
                    if (!isAddressValidated    && isEnableAddressDoctor) {
                    	
                		for (Map.Entry<String, String> entry : map.entrySet()) {
	                		if(entry.getKey().equalsIgnoreCase(getShipToAddressNickname()) && sgc.getShippingGroup(entry.getKey()) instanceof TRUHardgoodShippingGroup){	
		                		shippingGroupHG=(TRUHardgoodShippingGroup) sgc.getShippingGroup(entry.getKey());
		                    	String addressStatus = validAddress(shippingGroupHG);
		                        setAddressDoctorProcessStatus(addressStatus);
		                        TRUProfileTools  profileTools=(TRUProfileTools) getTruCSRPurchaseProcessHelper().getOrderManager().getOrderTools().getProfileTools();
		                        getAddressDoctor().setAddressDoctorResponseVO(profileTools.getAddressDoctorResponseVO());
		
		                        if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
		                        	isAddressValidated = Boolean.TRUE;
		                        }
		                        
		                        else if (TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR.equalsIgnoreCase(addressStatus)) {
									setAddressDoctorProcessStatus(addressStatus);
									TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
									addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
									getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
									//isAddressValidated = Boolean.TRUE;
								}
	                		}
                		}
                		
                    }else if (!isEnableAddressDoctor && !isAddressValidated) {
	                   setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
	                   TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
	
	                   addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
	                   
	                   getAddressDoctor().setAddressDoctorResponseVO(addressDoctorResponseVO);
                    }
                    
                    
                  /*  if (shippingGroup != null && sgc.getShippingGroupMap() != null) {
	                   for (i$ = sgc.getShippingGroupMap().entrySet().iterator(); i$.hasNext(); ) { mapEntry = i$.next();
		                   sgMapEntry = (Map.Entry)mapEntry;
		                   if (sgMapEntry != null && sgMapEntry.getValue() != null)
		                   {
	                          ShippingGroup msg = (ShippingGroup)sgMapEntry.getValue();
	                          if (msg != null && msg.getId().equals(shippingGroup.getId())) {
	                                 sgc.getShippingGroupMap().put(sgMapEntry.getKey(), shippingGroup);
	                                 break;
	                          }
		                   }
	                   }
                    }*/
            	 }
             }
	       }
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[preShipToExistingAddress]");
       }
	}

	

	/**
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException 
	 */
	protected void postShipToExisitngAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[postShipToExisitngAddress]");
       }
		
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[postShipToExisitngAddress]");
       }
	}
	
	/**
	 * Setup single shipping details for shipping to a existing address.
	 * 
	 * @param pRequest
	 *            a <code>DynamoHttpServletRequest</code> value.
	 * @param pResponse
	 *            a <code>DynamoHttpServletResponse</code> value.
	 * @exception ServletException
	 *                if an error occurs.
	 * @exception IOException
	 *                if an error occurs.
	 * @throws CommerceException 
	 */
	protected void shipToExistingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException, CommerceException {
		
        if (isLoggingDebug()) {
        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[shipToExistingAddress]");
       }
		
        
		if (!checkFormRedirect(null, getShipToNewAddressErrorURL(), pRequest,
				pResponse)) {
			return;
		}
		final List<Relationship> updateSGCIRelList = new ArrayList<Relationship>();
		TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
		TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
				getShippingGroupMapContainer();		
		shippingGroupMapContainer.getShippingGroupMapForDisplay();
		shippingGroupMapContainer.getAllCommerceItemShippingInfos();
		shippingGroupMapContainer.getShippingAddress();
		
		
			  
		if (StringUtils.isNotEmpty(getShipToAddressNickname())) {
			// getting container shipping group
			ShippingGroup containerShippingGroup = shippingGroupMapContainer.getShippingGroup(getShipToAddressNickname());
			if(containerShippingGroup instanceof TRUHardgoodShippingGroup){
				TRUHardgoodShippingGroup containerHGShippingGroup = (TRUHardgoodShippingGroup) shippingGroupMapContainer.getShippingGroup(getShipToAddressNickname());
			  
			// Get the shippingGroup if already available
			TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
			
			if (hardgoodShippingGroup != null && containerHGShippingGroup != null) {
				// updating address
				
				OrderTools.copyAddress(containerHGShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());
				if (containerHGShippingGroup.getShippingAddress() instanceof ContactInfo) {
					setAddress((ContactInfo) containerHGShippingGroup.getShippingAddress());
				}
				
				// if different nick name Then update the shipping group with giftwrap
                if (!getShipToAddressNickname().equalsIgnoreCase(hardgoodShippingGroup.getNickName())) {                                
                    hardgoodShippingGroup.setNickName(getShipToAddressNickname());                            
             }      
						
			}
			if (isLoggingDebug()) {
				logDebug("Shipping Group Id: " + hardgoodShippingGroup.getId());
				logDebug("Shipping Price: " + hardgoodShippingGroup.getShippingPrice());
				logDebug("Shipping Method: " + hardgoodShippingGroup.getShippingMethod());
				logDebug("Nick Name: " + hardgoodShippingGroup.getNickName());
				logDebug("Shipping Address: " + hardgoodShippingGroup.getShippingAddress());
			}			
			//getShippingHelper().updateShippingMethodAndPrice(getOrder(), hardgoodShippingGroup);			
			// setting address as selected address nick name
			shippingGroupMapContainer.setSelectedAddressNickName(getShipToAddressNickname());
			shippingGroupMapContainer.setDefaultShippingGroupName(getShipToAddressNickname());
			// Get all the commerceItems Relationships by each SG.
			
			hardgoodShippingGroup.setOrderShippingAddress(true);
			containerHGShippingGroup.setOrderShippingAddress(true);
			
			getShippingHelper().updateShippingMethodAndPrice(getOrder(), hardgoodShippingGroup,shippingGroupMapContainer);		
			// setting address as selected address nick name
			shippingGroupMapContainer.setDefaultShippingGroupName(getShipToAddressNickname());
			
        if (isLoggingDebug()) {
        logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[shipToExistingAddress]");
         }
		}
			final List<ShippingGroup> shippingGroups = getOrder().getShippingGroups();
			if(shippingGroups.size() > TRUCSCConstants.NUMBER_ONE){
				for (ShippingGroup sg : shippingGroups) {
						updateSGCIRelList.addAll(sg.getCommerceItemRelationships());
				}
			}
			TRUShippingGroupCommerceItemRelationship  sGCIR= null;

			if (!updateSGCIRelList.isEmpty()) {
				for (Object sgCIRel : updateSGCIRelList) {
					final TRUShippingGroupCommerceItemRelationship truSGCIRel = (TRUShippingGroupCommerceItemRelationship) sgCIRel;
					final CommerceItem commerceItem = truSGCIRel.getCommerceItem();
					final long quantity = truSGCIRel.getQuantity();
					final boolean isGiftItem = truSGCIRel.getIsGiftItem();
					final List<RepositoryItem> oldGiftItemInfo = truSGCIRel.getGiftItemInfo();
					final TRUBppItemInfo oldBPPItemInfo = truSGCIRel.getBppItemInfo();
					getCommerceItemShippingInfoContainer();
					if(StringUtils.isNotEmpty(getLocationId()) && 
							!commerceItem.getCommerceItemClassType().
							equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
					getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
							commerceItem.getId(), truSGCIRel.getShippingGroup().getId(), quantity);
					
					getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
							commerceItem.getId(), containerShippingGroup.getId(), quantity);
					sGCIR = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
							.getShippingGroupCommerceItemRelationship(getOrder(), commerceItem.getId(),
									containerShippingGroup.getId());
					
					final List<RepositoryItem> newGiftItemInfo = sGCIR.getGiftItemInfo();
					if (oldGiftItemInfo != null && !oldGiftItemInfo.isEmpty()
							&& (newGiftItemInfo == null || newGiftItemInfo.isEmpty())) {
						sGCIR.setGiftItemInfo(oldGiftItemInfo);
					} /*else if (oldGiftItemInfo != null && !oldGiftItemInfo.isEmpty() && newGiftItemInfo != null
							&& !newGiftItemInfo.isEmpty()) {
						// Merge giftwarp old GiftItemInfo into new GiftItemInfo
						// Associate the updated GiftItemInfo to current SGRel
						//getGiftingProcessHelper().mergeGiftItemInfo(order, oldGiftItemInfo, sGCIR);
					}*/

					if (oldBPPItemInfo != null &&  StringUtils.isNotBlank(oldBPPItemInfo.getId()) && StringUtils.isNotBlank(oldBPPItemInfo.getBppItemId())){
						
						if(StringUtils.isBlank(sGCIR.getBppItemInfo().getId())){
							sGCIR.setBppItemInfo(oldBPPItemInfo);
						}
					}/* else if (oldGiftItemInfo != null && !oldGiftItemInfo.isEmpty() && newGiftItemInfo != null
							&& !newGiftItemInfo.isEmpty()) {
						// Merge giftwarp old GiftItemInfo into new GiftItemInfo
						// Associate the updated GiftItemInfo to current SGRel
						//getGiftingProcessHelper().mergeGiftItemInfo(order, oldGiftItemInfo, sGCIR);
					}*/
					
					if (isGiftItem) {
						sGCIR.setIsGiftItem(true);
					}
				}
					
				/*	if(truSGCIRel.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
					sGCIR.setCommerceItem(truSGCIRel.getCommerceItem());
				}*/
					
				}
			}
			//getShippingHelper().updateShippingMethodAndPrice(getOrder(), containerShippingGroup);
		    }
		shippingGroupManager.removeEmptyShippingGroups(getOrder());		
		
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws RunProcessException RunProcessException
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	private void runProcessValidationShippingInfos(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RunProcessException,
			ServletException, IOException {
		Map<String, Object> extraParams=new HashMap<String, Object>();						
		extraParams.put(TRUConstants.TRU_SHIPPING_MANAGER, getShippingManager());
		extraParams.put(TRUConstants.ORDER,getOrder());
		extraParams.put(TRUConstants.TRU_SHIPPING_TOOLS,getShippingManager().getShippingTools());
		runProcessValidateShippingGroups(getOrder(),
				getUserPricingModels(),
				getUserLocale(pRequest, pResponse),
				getProfile(), extraParams);
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */  
	 protected void preShipToNewAddress(DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws ServletException,
				IOException {
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preShipToNewAddress]");
	           }

			if (StringUtils.isBlank(getAddress().getFirstName())
					|| StringUtils.isBlank(getAddress().getLastName())
					|| StringUtils.isBlank(getAddress().getAddress1())
					|| StringUtils.isBlank(getAddress().getCity())
					|| StringUtils.isBlank(getAddress().getPostalCode())
					|| StringUtils.isBlank(getAddress().getState())
					|| StringUtils.isBlank(getAddress().getPhoneNumber())
					|| StringUtils.isBlank(getShippingMethod())
					) {
				
				setEmptyShippingAddress(Boolean.TRUE);
				
				mVe.addValidationError(
						TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDRESS_MISSING_INFO, null);
				getErrorHandler().processException(mVe, this);
				return;
			}
	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[preShipToNewAddress]");
	           }
		}
		
		/**
		 * Handle 'Ship to new address' case.
		 * 
		 * @param pRequest
		 *            a <code>DynamoHttpServletRequest</code> value.
		 * @param pResponse
		 *            a <code>DynamoHttpServletResponse</code> value.
		 * 
		 * @return redirection result.
		 * 
		 * @exception ServletException
		 *                if an error occurs.
		 * @exception IOException
		 *                if an error occurs.
		 */
		public boolean handleShipToNewAddress(DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws ServletException, IOException {
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleShipToNewAddress]");
	           }
			String myHandleMethod = "TRUShippingGroupFormHandler.handleShipToNewAddress";
			RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			boolean hasException=false;
			if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
				Transaction tr = null;
				boolean isEnableAddressDoctor = false;
				try {
					tr = ensureTransaction();
					synchronized (getOrder()) {
						//preShipToNewAddress(pRequest, pResponse);
						if (getFormError()) {
							if (isLoggingDebug()) {
								vlogDebug("Redirecting due to form error in field validation() method");
							}
							return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
						}

						if(SiteContextManager.getCurrentSite() != null){
							/*Object value=SiteContextManager.getCurrentSite().getPropertyValue(getPropertyManager().getEnableAddressDoctorPropertyName());*/
							Object value = getIntegrationStatusUtil().getAddressDoctorIntStatus(pRequest);
							if(value!=null){
								isEnableAddressDoctor = (boolean)value;
							}						
						}

						// flag to check is address already validated
						boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());
						// Validate address with AddressDoctor Service
						Order order = getShoppingCart().getCurrent();
						List<ShippingGroup> shippingGroups=   order.getShippingGroups();

						if(shippingGroups!=null && !shippingGroups.isEmpty()) {                    
							for(ShippingGroup shippingGroup : shippingGroups) {
								if(shippingGroup instanceof TRUHardgoodShippingGroup) {

									TRUHardgoodShippingGroup shippingGroupHG = (TRUHardgoodShippingGroup) shippingGroup;

									if (!isAddressValidated && isEnableAddressDoctor) {
										String addressStatus = validAddress(shippingGroupHG);
										setAddressDoctorProcessStatus(addressStatus);
										setAddressDoctorResponseVO(getProfileTools().getAddressDoctorResponseVO());
										if (TRUConstants.ADDRESS_DOCTOR_STATUS_VERIFIED.equalsIgnoreCase(addressStatus)) {
											isAddressValidated = Boolean.TRUE;
										}
									} else if (!isEnableAddressDoctor && !isAddressValidated) {
										setAddressDoctorProcessStatus(TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR);
										TRUAddressDoctorResponseVO addressDoctorResponseVO = new TRUAddressDoctorResponseVO();
										addressDoctorResponseVO.setAddressRecognized(Boolean.FALSE);
										setAddressDoctorResponseVO(addressDoctorResponseVO);
									}
									if(StringUtils.isBlank(getNewShipToAddressName())){
										setNewShipToAddressName(getAddress().getFirstName() + TRUConstants.WHITE_SPACE + getAddress().getLastName());
									}
									shipToNewAddress(pRequest, pResponse);
									if (getFormError()) {
										if (isLoggingDebug()) {
											vlogDebug("Redirecting due to form error in shipToNewAddress() method");
										}
										return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
									}

									runProcessValidationShippingInfos(pRequest, pResponse);
									if (getFormError()) {
										if (isLoggingDebug()) {
											vlogDebug(COMMON_LOG_1);
										}
										return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
									}

									// Check for order reconciliation
									setOrderReconciliationNeeded(isOrderReconciliationRequired());
									if (isLoggingDebug()) {
										vlogDebug("isOrderReconciliationNeeded() ? {0}", isOrderReconciliationNeeded());
									}
									((TRUPaymentGroupContainerService)getPaymentGroupMapContainer()).setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());
									postShipToNewAddress(pRequest, pResponse);
									runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(), createRepriceParameterMap());
									// Added for updating special financing related flags in
									// order if order is eligible
									((TRUOrderTools) getOrderManager().getOrderTools()).updateOrderIfEligibleForFinancing(getOrder());
									((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
									getOrderManager().updateOrder(getOrder());
									// getShippingHelper().getProfileTools().setProfileDefaultAddress(getProfile());
								}
							} 
						}                   
					}

				}catch (RunProcessException e) {
					hasException=true;
					addFormException(new DropletException(TRUCSCConstants.PLEASE_TRY_AGAIN));
					if (isLoggingError()) {
						logError("RunProcessException in TRUShippingGroupFormHandler.handleShipToNewAddress", e);
					}
				} catch (CommerceException e) {
					hasException=true;
					addFormException(new DropletException(TRUCSCConstants.PLEASE_TRY_AGAIN));
					if (isLoggingError()) {
						logError("CommerceException in TRUShippingGroupFormHandler.handleShipToNewAddress", e);
					}
				}finally {
					if (tr != null) {
						if (hasException) {
							try {
								setTransactionToRollbackOnly();
							} catch (SystemException e) {
								if (isLoggingError()) {
									logError("SystemException in TRUInStorePickUpFormHandler.handleInitRegistryShippingGroups", e);
								}
							}
						}
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				}
			}
	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[handleShipToNewAddress]");
	           }
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		}
		
		/**
		 * Setup single shipping details for shipping to a new address.
		 * 
		 * @param pRequest
		 *            a <code>DynamoHttpServletRequest</code> value.
		 * @param pResponse
		 *            a <code>DynamoHttpServletResponse</code> value.
		 * @exception ServletException
		 *                if an error occurs.
		 * @exception IOException
		 *                if an error occurs.
		 * @throws CommerceException CommerceException
		 */
		protected void shipToNewAddress(DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws ServletException,
				IOException, CommerceException {
			
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[shipToNewAddress]");
	           }		
			if (!checkFormRedirect(null, getShipToNewAddressErrorURL(), pRequest,
					pResponse)) {
				return;
			}		
			TRUShippingGroupManager shippingGroupManager = (TRUShippingGroupManager) getShippingGroupManager();
			TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) 
					getShippingGroupMapContainer();
			
			if (StringUtils.isNotEmpty(getNewShipToAddressName())) {
				// Create a new shipping group with the new address and put the new shipping group in the container.
				getShippingHelper().findOrAddShippingGroupByNickname(getProfile(), getNewShipToAddressName(), getAddress(),
							shippingGroupMapContainer);
				
				// get shipping group based on nick name
				TRUHardgoodShippingGroup containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().
						getShippingGroup(getNewShipToAddressName());
				
				// order first shipping group
				TRUHardgoodShippingGroup hardgoodShippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
				hardgoodShippingGroup.getShippingMethod();
				if (hardgoodShippingGroup != null) {
					// updating address
					OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), hardgoodShippingGroup.getShippingAddress());				
					// updating shipping price
					hardgoodShippingGroup.setNickName(getNewShipToAddressName());
					//hardgoodShippingGroup.setShippingPrice(getShippingPrice());
					hardgoodShippingGroup.setShippingMethod(hardgoodShippingGroup.getShippingMethod());
				}		
				// setting address as selected address nick name
				shippingGroupMapContainer.setSelectedAddressNickName(getNewShipToAddressName());
				shippingGroupMapContainer.setDefaultShippingGroupName(getShipToAddressName());
			}
			
			shippingGroupManager.removeEmptyShippingGroups(getOrder());

	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[shipToNewAddress]");
	           }
		}
		/**
		 * This method initializes the billing address from the shipping address if
		 * the user selected that option. Saves addresses in the profile, if the
		 * user selected that option.
		 * 
		 * @param pRequest
		 *            a <code>DynamoHttpServletRequest</code> value.
		 * @param pResponse
		 *            a <code>DynamoHttpServletResponse</code> value.
		 * @exception ServletException
		 *                if an error occurs.
		 * @exception IOException
		 *                if an error occurs.
		 * @throws CommerceException
		 * 					if an error occurs.
		 */
		protected void postShipToNewAddress(DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws ServletException,
				IOException, CommerceException {
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[postShipToNewAddress]");
	           }
			
			TRUOrderImpl order = (TRUOrderImpl) getOrder();
			order.setBillingAddressSameAsShippingAddress(isBillingAddressSameAsShippingAddress());
			
	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[postShipToNewAddress]");
	           }
		}
		
		/**
		 * <p>
		 * 	This method will be invoked when "ship to multiple address" CTA clicked on single shipping page.
		 * </p>
		 * @throws CommerceException commerce exception
		 */
		@SuppressWarnings({ "unchecked", "unused" })
		private void applyMultiShipping() throws CommerceException {
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[applyMultiShipping]");
	           }
				TRUShippingManager shippingManager = getShippingHelper().getPurchaseProcessHelper().getShippingManager();
				List<TRUShipMethodVO> eligibleShipMethodVOs = null;
				List<Relationship> sgCiRels = getShippingHelper().getPurchaseProcessHelper().getShippingGroupManager().getAllShippingGroupRelationships(getOrder());
				if (sgCiRels != null && !sgCiRels.isEmpty()) {
					int sgCiRelsCount = sgCiRels.size();
					for (int i = 0; i < sgCiRelsCount; ++i) {
						TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
						CommerceItem commerceItem = sgCiRel.getCommerceItem();					

						if(commerceItem instanceof TRUGiftWrapCommerceItem || commerceItem instanceof TRUDonationCommerceItem){
							continue;
						}
						if (sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup) {
							TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();
							// Get eligible ship methods for specific item based on
							// the address and item's freightClass property
							String nickName = null;
							// get shipping group based on nick name
							TRUHardgoodShippingGroup containerShippingGroup = null;
							if (!StringUtils.isBlank(getNewShipToAddressName())) {
								nickName = getNewShipToAddressName();
							} else if (!StringUtils.isBlank(getShipToAddressName())){
								nickName = getShipToAddressName();
							}/*else{
								//This logic is for handling ship to multi address CTA clicked on add  new shipping page.
								nickName =commerceItem.getCatalogRefId() +TRUConstants.IFAN+sgCiRel.getId();														 
							}
							
							containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroup(nickName);
							if(containerShippingGroup==null){
								 // Put the new shipping group in the container.
								 getShippingGroupMapContainer().addShippingGroup(nickName, oldShippingGroup);
							}*/
							eligibleShipMethodVOs = shippingManager.getEligibleShipMethodsByItem(commerceItem, oldShippingGroup.getShippingAddress(),
									getOrder());
							
							if (!shippingManager.isPreferredShippingExistis(eligibleShipMethodVOs,oldShippingGroup.getShippingMethod())) {
								String defaultLeastShipMethod = TRUConstants.EMPTY;
								double defaultLeastShipPrice = TRUConstants.DOUBLE_ZERO;
								if(eligibleShipMethodVOs != null && !eligibleShipMethodVOs.isEmpty() && eligibleShipMethodVOs.get(0) != null){
									 defaultLeastShipMethod = eligibleShipMethodVOs.get(0).getShipMethodCode();
									 defaultLeastShipPrice = eligibleShipMethodVOs.get(0).getShippingPrice();
								}
									// Adding new shipping group
									TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager().createShippingGroup();
									getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);							
									// Get the shippingGroup from the container for
									// nickName
									containerShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupMapContainer().getShippingGroup(nickName);
									if (newShippingGroup != null) {
										if (containerShippingGroup != null) {
											OrderTools.copyAddress(containerShippingGroup.getShippingAddress(), newShippingGroup.getShippingAddress());
										}
										newShippingGroup.setNickName(nickName);
										newShippingGroup.setShippingMethod(defaultLeastShipMethod);
										newShippingGroup.setShippingPrice(defaultLeastShipPrice);
										getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(), commerceItem.getId(), oldShippingGroup.getId(),
												sgCiRel.getQuantity());
										getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(), commerceItem.getId(), newShippingGroup.getId(),
												sgCiRel.getQuantity());
									}
								//Adding empty form error to highlight the div box if the preferred ship method is not eligible for any item 
								addFormException(new DropletException(TRUConstants.EMPTY, newShippingGroup.getId()));								
							}/*else{
								oldShippingGroup.setNickName(nickName);
							}*/
						}
					}
				}
				getShippingGroupManager().removeEmptyShippingGroups(getOrder());
		        if (isLoggingDebug()) {
		            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[applyMultiShipping]");
		           }
		}
		
		/**
		 * This method will validates the commerce items and its associated shipping groups. 
		 * This will change ISPU to HG shipping group and viceversa if required. 
		 * 
		 * @param pOrder
		 *            the order object
		 * @param pRequest
		 *            the servlet's request
		 * @param pResponse
		 *            the servlet's response
		 * @throws ServletException
		 *             if an error occurred while processing the servlet request
		 * @throws IOException
		 *             if an error occurred while reading or writing the servlet
		 *             
		 */
		@SuppressWarnings("unchecked")
		protected void checkForShippingGroup(TRUOrderImpl pOrder, DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws ServletException,
				IOException{
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[checkForShippingGroup]");
	           }
			TRUShippingGroupCommerceItemRelationship selectedShippingGroupRel = null;
			if (isLoggingDebug()) {
				vlogDebug(
						"@Class::TRUCSRShippingGroupFormHandler::@method::changeShippingGroup() : Relationship Id : {0} : Location Id : {1}",
						getRelationShipId(), getLocationId());
			}
			if (null != pOrder && pOrder.getCommerceItems() != null){
				List<CommerceItem> commerceItems = pOrder.getCommerceItems();
				try {
					for (CommerceItem commerceItem : commerceItems) {
						final TRUCoherenceInventoryManager invManager = getInventoryManager();
						 long availableStockAtLocation = 0;
						 if((!commerceItem.getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)) || (!commerceItem.getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.GIFTWRAP_COMMERCE_ITEM))){
						
							if(getLocationId()==null || StringUtils.isBlank(getLocationId())){
								availableStockAtLocation = invManager.queryStockLevel(commerceItem.getCatalogRefId());
								if(commerceItem.getQuantity() > availableStockAtLocation){
									addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+commerceItem.getCatalogRefId()));
									return;
									 }
							}else{
								int availability = invManager.queryAvailabilityStatus(commerceItem.getCatalogRefId(),  getLocationId());
								availableStockAtLocation = invManager.queryStockLevel(commerceItem.getCatalogRefId(),getLocationId());
								if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK && getLocationId() != null){
									Double storeItemWareHouseLocationId = getShippingHelper().getStoreItemWareHouseLocationId(getLocationId());
									availableStockAtLocation=invManager.queryStockLevel(commerceItem.getCatalogRefId(),Long.toString(Math.round(storeItemWareHouseLocationId)));
								}
							 if(commerceItem.getQuantity() > availableStockAtLocation){
								addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+commerceItem.getCatalogRefId()));
								return;
							 }
							}
						 }
						if(availableStockAtLocation>commerceItem.getQuantity() || commerceItem.getCommerceItemClassType().
								equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
						List<TRUShippingGroupCommerceItemRelationship> relationShipObjList = commerceItem.getShippingGroupRelationships();
						for (TRUShippingGroupCommerceItemRelationship relationShipObj : relationShipObjList) {
							String relationShipId = relationShipObj.getId();
							selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) pOrder.getRelationship(relationShipId);
							if (null != selectedShippingGroupRel && !(commerceItem instanceof TRUGiftWrapCommerceItem) && 
									selectedShippingGroupRel.getShippingGroup() != null ) {
								if (StringUtils.isNotBlank(getLocationId()) && StringUtils.isNotEmpty(getLocationId())
										&& ((selectedShippingGroupRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup && 
												((TRUInStorePickupShippingGroup) selectedShippingGroupRel
												.getShippingGroup()).getLocationId().equalsIgnoreCase(getLocationId())) && 
												!commerceItem.getCommerceItemClassType().
												equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM))) {
									if (isLoggingDebug()) {
										vlogDebug("@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : selectedShippingGroupRel Location Id and Location Id in request are equal");
									}
									continue;
								}else if (StringUtils.isBlank(getLocationId()) && StringUtils.isEmpty(getLocationId())
										&& ((selectedShippingGroupRel.getShippingGroup() instanceof TRUHardgoodShippingGroup))) {
									if (isLoggingDebug()) {
										vlogDebug("@Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange() : selectedShippingGroupRel Location Id and Location Id in request are equal");
									}
									continue;
								}
								setQuantity(selectedShippingGroupRel.getQuantity());
								setRelationShipId(relationShipId);
								shippingGroupChange(pOrder, selectedShippingGroupRel);
								try {
									runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(),	getProfile(), createRepriceParameterMap());
								} catch (RunProcessException e) {
									if (isLoggingError()) {
										logError(
												"RunProcessException in @Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange()",
												e);
									}
								}
								getOrderManager().updateOrder(pOrder);
							}
						}
						}
					}
					getShippingGroupManager().removeEmptyShippingGroups(getOrder());
				} catch (CommerceException e) {
					if (isLoggingError()) {
						logError("CommerceException in @Class::TRUCSRShippingGroupFormHandler::@method::changeShippingGroup()", e);
					}
				} catch (RepositoryException re) {
					if (isLoggingError()) {
						logError("RepositoryException in @Class::TRUCartModifierFormHandler::@method::handleShippingGroupChange()",
								re);
					}
				} 
			}
	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[checkForShippingGroup]");
	        }
		}
		
		/**
		 * This method is used to change the shipping option (ship to home/store pickup). If the commerce item shipping group
		 * relationship already exits with in the same commerce item, the quantity will added/merged with that relationship. If the
		 * commerce item shipping group relationship does not exits with in the same commerce item, it will create new commerce item
		 * shipping group relationship.
		 * 
		 * @param pOrder
		 *            the order
		 * @param pSelectedShippingGroupRel
		 *            the selected shipping group rel
		 * @throws CommerceException
		 *             the commerce exception
		 * @throws RepositoryException
		 *             the repository exception
		 */
		@SuppressWarnings("unchecked")
		private void shippingGroupChange(Order pOrder, TRUShippingGroupCommerceItemRelationship pSelectedShippingGroupRel)
				throws CommerceException, RepositoryException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChange() : BEGIN");
			}
			
			List<ShippingGroupCommerceItemRelationship> sgCiRels = null;
			sgCiRels = (List<ShippingGroupCommerceItemRelationship>) pOrder.getCommerceItem(
					pSelectedShippingGroupRel.getCommerceItem().getId()).getShippingGroupRelationships();
			if (StringUtils.isBlank(getLocationId()) || pSelectedShippingGroupRel.getCommerceItem().getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)) {
				shippingGroupChangeFromISPUToHG(pOrder, pSelectedShippingGroupRel, sgCiRels);
			} else if (!StringUtils.isBlank(getLocationId())
					&& !getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(getLocationId())) {
				shippingGroupChangeFromHGToISPU(pOrder, pSelectedShippingGroupRel, sgCiRels);
			}
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChange() : END");
			}
		}
		
		/**
		 * Shipping group change from Hardgood to InstorePickup.
		 *
		 * @param pOrder the order
		 * @param pSelectedShippingGroupRel the selected shipping group rel
		 * @param pSgCiRels the sg ci rels
		 * @throws CommerceException the commerce exception
		 */
		private void shippingGroupChangeFromHGToISPU(Order pOrder, TRUShippingGroupCommerceItemRelationship pSelectedShippingGroupRel,
				List<ShippingGroupCommerceItemRelationship> pSgCiRels) throws CommerceException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : BEGIN");
			}
			
			boolean eligibleForISPU = getShippingHelper().isProductEligibleForISPU(pOrder, pSelectedShippingGroupRel);
			
			final TRUCoherenceInventoryManager invManager = getInventoryManager();
			 long availableStockAtLocation;
			if(eligibleForISPU){
				 List<CommerceItem> commerceItems = pOrder.getCommerceItems();
				for (CommerceItem commerceItem : commerceItems) {
					 if(!commerceItem.getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
						if(getLocationId() != null || !StringUtils.isEmpty(getLocationId())){
							int availability = invManager.queryAvailabilityStatus(commerceItem.getCatalogRefId(),  getLocationId());
							availableStockAtLocation = invManager.queryStockLevel(commerceItem.getCatalogRefId(),getLocationId());
							if (availability == TRUCommerceConstants.INT_OUT_OF_STOCK && getLocationId() != null){
								Double storeItemWareHouseLocationId = getShippingHelper().getStoreItemWareHouseLocationId(getLocationId());
								availableStockAtLocation=invManager.queryStockLevel(commerceItem.getCatalogRefId(),Long.toString(Math.round(storeItemWareHouseLocationId)));
							}
							if(commerceItem.getQuantity() > availableStockAtLocation){
								addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+commerceItem.getCatalogRefId()));
								return;
							}
						}
					 }
				   }
				
				List<RepositoryItem> getGiftItemInfo = pSelectedShippingGroupRel.getGiftItemInfo();
				for (RepositoryItem giftItem : getGiftItemInfo) {
					if(giftItem.getPropertyValue(getPropertyManager().getGiftWrapCommItemId()) != null) {
						CommerceItem commerceItem = getOrder().getCommerceItem(
								(String) giftItem.getPropertyValue(getPropertyManager().getGiftWrapCommItemId()));
						if(commerceItem instanceof TRUGiftWrapCommerceItem){
							  getCommerceItemManager().removeAllRelationshipsFromCommerceItem(getOrder(), 
									  														  commerceItem.getId());
				              getCommerceItemManager().removeItemFromOrder(getOrder(), commerceItem.getId());
						}
					}
				}
				ShippingGroupCommerceItemRelationship selectedShippingGroupRelToMerge = null;
				ShippingGroup shippingGroup = null;
				long qtyToMerge = getQuantity();
				if (isLoggingDebug()) {
					vlogDebug(
							"@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : location id : {0} : qtyToMerge : {1}",
							getLocationId(), qtyToMerge);
				}
				for (ShippingGroupCommerceItemRelationship sgRel : pSgCiRels) {
					if (!getRelationShipId().equalsIgnoreCase(sgRel.getId())
							&& sgRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup
							&& ((TRUInStorePickupShippingGroup) sgRel.getShippingGroup()).getLocationId().equalsIgnoreCase(
									getLocationId())
							&& getOrderManager().getOrderTools().getDefaultShippingGroupType()
							.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType())) {
						selectedShippingGroupRelToMerge = sgRel;
						if (isLoggingDebug()) {
							vlogDebug(
									"TRUInStorePickupShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
									selectedShippingGroupRelToMerge.getId());
						}
						break;
					}
				}
				if (selectedShippingGroupRelToMerge == null && !pOrder.getShippingGroups().isEmpty()) {
					for (ShippingGroup sg : (List<ShippingGroup>) pOrder.getShippingGroups()) {
						if (shippingGroup == null
								&& sg instanceof TRUInStorePickupShippingGroup
								&& ((TRUInStorePickupShippingGroup) sg).getLocationId().equalsIgnoreCase(getLocationId())
								&& (getOrderManager().getOrderTools().getDefaultShippingGroupType().equalsIgnoreCase(
										pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType()) ||
										TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType()))) {
							shippingGroup = sg;
							if (isLoggingDebug()) {
								vlogDebug("TRUInStorePickupShippingGroup is already exists to merge :: shippingGroup id :: {0}",
										sg.getId());
							}
							break;
						}
					}
					if (shippingGroup == null) {
						shippingGroup = createMissedIspuSGsInOrderBasedOnSG(
								pOrder, pSelectedShippingGroupRel, getLocationId());
					}
				}
				addUpdateShippingGroupRelationaShip(pOrder, pSelectedShippingGroupRel, qtyToMerge, shippingGroup,
						selectedShippingGroupRelToMerge);
				if (isLoggingDebug()) {
					logDebug("@Class::TRUCartModifierFormHandler::@method::shippingGroupChangeFromHGToISPU() : END");
				}
			}else{
				addFormException(new DropletException(TRUCSCConstants.NOT_ELIGIBLE_FOR_INSPU));
			}
		}
		
		/**
		 * Creates the missed ispu sgs in order based on sg.
		 * 
		 * @param pOrder
		 *            the order
		 * @param pSelShippingGroup
		 *            the sel shipping group
		 * @param pLocationId
		 *            the location id
		 * @return the shipping group
		 * @throws CommerceException
		 *             the commerce exception
		 */
		public ShippingGroup createMissedIspuSGsInOrderBasedOnSG(Order pOrder, ShippingGroupCommerceItemRelationship pSelShippingGroup, String pLocationId)
				throws CommerceException {
	        if (isLoggingDebug()) {
	            logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[createMissedIspuSGsInOrderBasedOnSG]");
	           }

			ShippingGroup shippingGroup = null;
			try {
				if (pSelShippingGroup.getShippingGroup() instanceof TRUHardgoodShippingGroup
						|| pSelShippingGroup.getShippingGroup() instanceof TRUInStorePickupShippingGroup) {
					if (isLoggingDebug()) {
						vlogDebug("There is no TRUPurchaseProcessHelper is exists to merge :: hence creating a new shipping group");
					}
					
					shippingGroup = getShippingGroupManager().createShippingGroup(
							((TRUOrderTools) getOrderManager().getOrderTools()).getInStorePickupShippingGroupType());
					
					if (shippingGroup instanceof TRUInStorePickupShippingGroup) {
						TRUInStorePickupShippingGroup ispsg = (TRUInStorePickupShippingGroup) shippingGroup;
						ispsg.setLocationId(pLocationId);
						ispsg.setWarhouseLocationCode(
								Long.toString(getShoppingCartUtils().getTRUCartModifierHelper().getWareHouseLocationId(pLocationId)
								.longValue()));
					}
					getShippingGroupManager().addShippingGroupToOrder(pOrder, shippingGroup);
					return shippingGroup;
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError(
							"RepositoryException while setting warehouse location code " +
							 "@Class::TRUPurchaseProcessHelper::@method::createMissedIspuSGsInOrderBasedOnSG() ",
							repositoryException);
				}
			}
	        if (isLoggingDebug()) {
	            logDebug("Exit from class:[TRUCSRShippingGroupFormHandler] method:[createMissedIspuSGsInOrderBasedOnSG]");
	           }

			return shippingGroup;
		}

		/**
		 * Shipping group change from InstorePickup to Hardgood.
		 *
		 * @param pOrder the order
		 * @param pSelectedShippingGroupRel the selected shipping group rel
		 * @param pSgCiRels the sg ci rels
		 * @throws CommerceException the commerce exception
		 */
		private void shippingGroupChangeFromISPUToHG(Order pOrder, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel,
				List<ShippingGroupCommerceItemRelationship> pSgCiRels) throws CommerceException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::shippingGroupChangeFromISPUToHG() : START");
			}
			final TRUCoherenceInventoryManager invManager = getInventoryManager();
			 long availableStockAtLocation;
			 
			 
			 
			 List<CommerceItem> commerceItems = pOrder.getCommerceItems();
					for (CommerceItem commerceItem : commerceItems) {
						if(!commerceItem.getCommerceItemClassType().equalsIgnoreCase(TRUCSCConstants.DONATION_COMMERCE_ITEM)){
							if(getLocationId()==null || StringUtils.isEmpty(getLocationId())){
								availableStockAtLocation = invManager.queryStockLevel(commerceItem.getCatalogRefId());
								if(commerceItem.getQuantity() > availableStockAtLocation){
									addFormException(new DropletException(TRUCSCConstants.ITEM_INVENTORY_NOT_AVAIL_IN_STORE+commerceItem.getCatalogRefId()));
									return;
								}
							}
						}
					}
			
			ShippingGroupCommerceItemRelationship selectedShippingGroupRelToMerge = null;
			ShippingGroup shippingGroup = null;
			long qtyToMerge = getQuantity();
			if (isLoggingDebug()) {
				vlogDebug(
						"@Class::TRUCSRShippingGroupFormHandler::@method::shippingGroupChangeFromISPUToHG() : location id : {0} : qtyToMerge : {1}",
						getLocationId(), qtyToMerge);
			}
			for (ShippingGroupCommerceItemRelationship sgRel : pSgCiRels) {
				if (!getRelationShipId().equalsIgnoreCase(sgRel.getId())
						&& sgRel.getShippingGroup() instanceof TRUHardgoodShippingGroup
						|| (TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(
								pSelectedShippingGroupRel.getShippingGroup().getShippingGroupClassType()))) {
					selectedShippingGroupRelToMerge = sgRel;
					vlogDebug("TRUHardgoodShippingGroup, shippingGroupRel is already exists to merge :: ralationship id :: {0}",
							selectedShippingGroupRelToMerge.getId());
					break;
				}
			}
			if (selectedShippingGroupRelToMerge == null && !pOrder.getShippingGroups().isEmpty()) {
				for (ShippingGroup sg : (List<ShippingGroup>) pOrder.getShippingGroups()) {
					if (shippingGroup == null
							&& (TRUCommerceConstants.INSTORE_PICKUP_SG.equalsIgnoreCase(pSelectedShippingGroupRel
									.getShippingGroup().getShippingGroupClassType()) || sg.getShippingGroupClassType().equalsIgnoreCase("hardGoodShippingGroup"))) {
						shippingGroup = getShippingHelper().getPurchaseProcessHelper().getFirstHardgoodShippingGroup(getOrder());
						if (isLoggingDebug()) {
							vlogDebug("TRUHardgoodShippingGroup is already exists to merge :: shippingGroup id :: {0}",
									sg.getId());
						}
						break;
					}
				}
			}
			addUpdateShippingGroupRelationaShip(pOrder, pSelectedShippingGroupRel, qtyToMerge, shippingGroup,
					selectedShippingGroupRelToMerge);
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::shippingGroupChangeFromISPUToHG() : END");
			}
		}
		
		
		/**
		 * Update shipping group relationa ship.
		 *
		 * @param pOrder
		 *            the order
		 * @param pSelectedShippingGroupRel
		 *            the selected shipping group rel
		 * @param pQtyToMerge
		 *            the qty to merge
		 * @param pShippingGroup
		 *            the shipping group
		 * @param pSelectedShippingGroupRelToMerge
		 *            the selected shipping group rel to merge
		 * @throws CommerceException
		 *             the commerce exception
		 */
		protected void addUpdateShippingGroupRelationaShip(Order pOrder,
				ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel, long pQtyToMerge, ShippingGroup pShippingGroup,
				ShippingGroupCommerceItemRelationship pSelectedShippingGroupRelToMerge) throws CommerceException {
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::addUpdateShippingGroupRelationaShip() : BEGIN");
			}
			if (null != pOrder && null != pSelectedShippingGroupRel) {
				if (isLoggingDebug()) {
					vlogDebug(
							"pOrder id : {0} : pSelectedShippingGroupRel id : {1} : pSelectedShippingGroupRel qty : {2} : pQtyToMerge : {3}",
							pOrder.getId(), 
							pSelectedShippingGroupRel.getId(), 
							pSelectedShippingGroupRel.getQuantity(),
							pQtyToMerge);
				}
				getCommerceItemManager().removeItemQuantityFromShippingGroup(pOrder,
						pSelectedShippingGroupRel.getCommerceItem().getId(),
						pSelectedShippingGroupRel.getShippingGroup().getId(),
						pSelectedShippingGroupRel.getQuantity());
				if (null != pSelectedShippingGroupRelToMerge) {
					vlogDebug("pSelectedShippingGroupRelToMerge id : {0}", pSelectedShippingGroupRelToMerge.getId());
					getCommerceItemManager().addItemQuantityToShippingGroup(pOrder,
							pSelectedShippingGroupRelToMerge.getCommerceItem().getId(),
							pSelectedShippingGroupRelToMerge.getShippingGroup().getId(), pQtyToMerge);
				} else if (null != pShippingGroup) {
					vlogDebug("pShippingGroup id : {0}", pShippingGroup.getId());
					ShippingGroupCommerceItemRelationship newShpGrpCIRel = null;
					getCommerceItemManager().addItemQuantityToShippingGroup(pOrder,
							pSelectedShippingGroupRel.getCommerceItem().getId(), pShippingGroup.getId(), pQtyToMerge);
					newShpGrpCIRel = (ShippingGroupCommerceItemRelationship) getShippingGroupManager().
							getShippingGroupCommerceItemRelationship(getOrder(),
										pSelectedShippingGroupRel.getCommerceItem().getId(),
										pShippingGroup.getId());
					((TRUOrderTools) getOrderManager().getOrderTools()).copyShippingRelationShip(
							pSelectedShippingGroupRel, newShpGrpCIRel);
				}
			}
			if (isLoggingDebug()) {
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::addUpdateShippingGroupRelationaShip() : END");
			}
		}
		
		/**
		 * This method will return the shipping group corresponding to the address key from the shipping group container service.
		 * @param pAddressKey
		 * 			Address Key
		 * @param pSgc
		 * 			ShippinGroupContainer Service
		 * @return shippingGroup
		 */
		private ShippingGroup getShippingGroupByAddressKey(String pAddressKey, TRUShippingGroupContainerService pSgc){
			if(isLoggingDebug()){
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::getShippingGroupByAddressKey() : START");
			}
			final ShippingGroup shippingGroup = ((Map<String, ShippingGroup>) pSgc.getShippingGroupMap())
					.get(pAddressKey);
			
			if(isLoggingDebug()){
				logDebug("@Class::TRUCSRShippingGroupFormHandler::@method::getShippingGroupByAddressKey() : END");
			}
			return shippingGroup;
		}
		
		/**
		 * validateShippingAddress.
		 *
		 * @param pNickName nickname
		 * @param pRequest a <code>DynamoHttpServletRequest</code> value.
		 * @param pResponse a <code>DynamoHttpServletResponse</code> value.
		 * @throws IOException if an error occurs.
		 * @throws ServletException if an error occurs.
		 */
		protected void validateDuplicateAddressNickname(String pNickName, DynamoHttpServletRequest pRequest,
				DynamoHttpServletResponse pResponse) throws IOException, ServletException {
			if (isLoggingDebug()) {
				vlogDebug("Start: TRUShippingGroupFormHandler.validateDuplicateAddressNickname()");
			}

			if (getShippingGroupMapContainer().getShippingGroup(pNickName) != null) {
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_DUPLICATE_ADDR_NICKNAME, null);
				getErrorHandler().processException(mVe, this);
			}

			if (isLoggingDebug()) {
				vlogDebug("End: TRUShippingGroupFormHandler.validateDuplicateAddressNickname()");
			}
		}
		
		/**
		 * <p>
		 * 	This method will be invoked on change of ship method in single ship page.
		 * </p>
		 * @param pRequest DynamoHttpServletRequest
		 * @param pResponse DynamoHttpServletResponse
		 * @return boolean
		 * @throws ServletException ServletException
		 * @throws IOException IOException
		 */
		public boolean handleUpdateShipMethod(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
				throws ServletException, IOException {
			if (isLoggingDebug()) {
				vlogDebug("START of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
			}
			final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
			if ((rrm == null) || (rrm.isUniqueRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD))) {
				Transaction tr = null;
				try {
					tr = ensureTransaction();
					final TRUOrderTools orderTools = (TRUOrderTools) getOrderManager().getOrderTools();
					final String selectedShipMethodCode = getShipMethodVO().getShipMethodCode();
					final String selectedregionCode = getShipMethodVO().getRegionCode();
					final double selectedShippingPrice = getShipMethodVO().getShippingPrice();
					TRUShippingGroupCommerceItemRelationship newShpGrpCIRel = null;
					synchronized (getOrder()) {
						CommerceItem giftWrapCommerceItem = null;
						try {
							if (StringUtils.isNotBlank(getWrapCommerceItemId())) {
								giftWrapCommerceItem = getOrder().getCommerceItem(getWrapCommerceItemId());
							}
						} catch (CommerceItemNotFoundException ceE) {
							if (isLoggingError()) {
								logError("CommerceItemNotFoundException in TRUShippingGroupFormHandler.selectExistingAddress", ceE);
							}
						}
						if (StringUtils.isNotBlank(getRelationShipId())) {
							final Relationship relationship = getOrder().getRelationship(getRelationShipId());
							if (relationship instanceof TRUShippingGroupCommerceItemRelationship) {
								final TRUShippingGroupCommerceItemRelationship oldShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) relationship;
								final TRUHardgoodShippingGroup oldShippingGroup = (TRUHardgoodShippingGroup) getOrder().getShippingGroup(
										getShippingGroupId());
								if (oldShippingGroup != null) {
									if (oldShpGrpCIRel.getQuantity() >= TRUTaxwareConstant.NUMBER_ONE) {
										getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(getOrder(),
												oldShpGrpCIRel.getCommerceItem().getId(), oldShippingGroup.getId(),
												TRUTaxwareConstant.NUMBER_ONE);
										if (giftWrapCommerceItem != null) {
											getOrderManager().getCommerceItemManager().removeItemQuantityFromShippingGroup(
													getOrder(), giftWrapCommerceItem.getId(), oldShippingGroup.getId(),
													TRUTaxwareConstant.NUMBER_ONE);
											getGiftingProcessHelper().removeGiftItemQuantity(getOrder(), oldShpGrpCIRel,
													giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
										}
									}

									final TRUHardgoodShippingGroup existingShippingGroup = ((TRUOrderManager) getOrderManager())
											.getShipGroupByNickNameAndShipMethod(getOrder(), oldShippingGroup.getNickName(),
													selectedShipMethodCode);
									if (existingShippingGroup != null) {
										getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
												oldShpGrpCIRel.getCommerceItem().getId(), existingShippingGroup.getId(),
												TRUTaxwareConstant.NUMBER_ONE);
										newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
												.getShippingGroupCommerceItemRelationship(getOrder(),
														oldShpGrpCIRel.getCommerceItem().getId(), existingShippingGroup.getId());
										// assign the gw item to existing SG
										if (giftWrapCommerceItem != null) {
											getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
													giftWrapCommerceItem.getId(), existingShippingGroup.getId(),
													TRUTaxwareConstant.NUMBER_ONE);
											getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel,
													giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
										}
									} else {
										// Creating new shipping group and increase qty
										final TRUHardgoodShippingGroup newShippingGroup = (TRUHardgoodShippingGroup) getShippingGroupManager()
												.createShippingGroup(oldShippingGroup.getShippingGroupClassType());
										if (newShippingGroup != null && oldShippingGroup != null) {
											OrderTools.copyAddress(oldShippingGroup.getShippingAddress(),
													newShippingGroup.getShippingAddress());
										}
										if (newShippingGroup != null) {
											if (oldShippingGroup instanceof TRUChannelHardgoodShippingGroup
													&& newShippingGroup instanceof TRUChannelHardgoodShippingGroup) {											
												copyChannelShippingInfo(oldShippingGroup, newShippingGroup);
											}
											getShippingGroupManager().addShippingGroupToOrder(getOrder(), newShippingGroup);
											newShippingGroup.setNickName(oldShippingGroup.getNickName());
											newShippingGroup.setShippingMethod(selectedShipMethodCode);
											newShippingGroup.setShippingPrice(selectedShippingPrice);
											newShippingGroup.setRegionCode(selectedregionCode);
											newShippingGroup.setGiftWrapMessage(oldShippingGroup.getGiftWrapMessage());
											// assign the item to new SG
											getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
													oldShpGrpCIRel.getCommerceItem().getId(), newShippingGroup.getId(),
													TRUTaxwareConstant.NUMBER_ONE);

											newShpGrpCIRel = (TRUShippingGroupCommerceItemRelationship) getShippingGroupManager()
													.getShippingGroupCommerceItemRelationship(getOrder(),
															oldShpGrpCIRel.getCommerceItem().getId(), newShippingGroup.getId());
											// assign the gw item to new SG
											if (giftWrapCommerceItem != null) {
												getOrderManager().getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(),
														giftWrapCommerceItem.getId(), newShippingGroup.getId(),
														TRUTaxwareConstant.NUMBER_ONE);
												getGiftingProcessHelper().addGiftItemQuantity(getOrder(), newShpGrpCIRel,
														giftWrapCommerceItem, TRUTaxwareConstant.NUMBER_ONE);
											}
										}
									}
									orderTools.copyShippingRelationShip(oldShpGrpCIRel, newShpGrpCIRel);
								}
							}
						}
						getShippingGroupManager().removeEmptyShippingGroups(getOrder());
						runProcessRepriceOrder(getOrder(), getUserPricingModels(), getLocale(), getProfile(),
								createRepriceParameterMap());
						// updating CartItemDetailVO for the presentation layer
						if (newShpGrpCIRel != null) {
							updateCartItemDetailVO(newShpGrpCIRel);
						}

						postUpdateShipMethod(newShpGrpCIRel);
						getOrderManager().updateOrder(getOrder());
					}
				} catch (RunProcessException e) {
					if (isLoggingError()) {
						logError("RunProcessException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
					}
				} catch (CommerceException e) {
					if (isLoggingError()) {
						logError("CommerceException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError("RepositoryException in TRUShippingGroupFormHandler.handleUpdateShipMethod", e);
					}
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRU_SHIPPING_GROUP_FORM_HANDLER_HANDLE_SELECT_SHIP_METHOD);
					}
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("END of TRUShippingGroupFormHandler.handleUpdateShipMethod method");
			}
			return checkFormRedirect(getCommonSuccessURL(), getCommonErrorURL(), pRequest, pResponse);
		}
		/**
		 * to validate all the required field to change ship method.
		 *
		 * @param pNewShpGrpCIRel - TRUShippingGroupCommerceItemRelationship
		 * @throws ServletException - ServletException
		 * @throws IOException - IOException
		 */
		protected void postUpdateShipMethod(
				TRUShippingGroupCommerceItemRelationship pNewShpGrpCIRel) throws ServletException,
				IOException {
			if(isLoggingDebug()){
				vlogDebug("START of TRUShippingGroupFormHandler.postUpdateShipMethod method ");
			}		
			
			final String previousTab=((TRUOrderImpl)getOrder()).getPreviousTab();
			if(TRUCheckoutConstants.REVIEW_TAB.equals(previousTab)){
				getShippingHelper().updateEstimateDeliveryDate(pNewShpGrpCIRel);
				if(isLoggingDebug()){
					logDebug("EDD Updated");
				}
			}
			
			if (isLoggingDebug()) {
				vlogDebug("END of TRUShippingGroupFormHandler.postUpdateShipMethod method");
			}
			
		}
		
		/**
		 * Update cart item detail vo.
		 *
		 * @param pNewShpGrpCIRel the new shp group ci rel
		 */
		private void updateCartItemDetailVO(TRUShippingGroupCommerceItemRelationship pNewShpGrpCIRel){
			if (isLoggingDebug()) {
				vlogDebug("START of TRUShippingGroupFormHandler.updateCartItemDetailVO()");
			}
			TRUHardgoodShippingGroup newShippGroup = null;
			final TRUShipmentVO shipmentVO = new TRUShipmentVO();
			newShippGroup = (TRUHardgoodShippingGroup) pNewShpGrpCIRel.getShippingGroup();
			final TRUCartItemDetailVO truCartItemDetailVO = ((TRUOrderImpl) getOrder()).getCartItemDetailVOMap().get(getUniqueId());
			truCartItemDetailVO.setId(pNewShpGrpCIRel.getId());
			truCartItemDetailVO.setShipGroupId(newShippGroup.getId());
			truCartItemDetailVO.setShippingGroupName(newShippGroup.getNickName());
			truCartItemDetailVO.setSelectedShippingMethod(newShippGroup.getShippingMethod());
			truCartItemDetailVO.setParentQuantity(pNewShpGrpCIRel.getQuantity());
			final TRUShippingPriceInfo priceInfo = (TRUShippingPriceInfo) newShippGroup.getPriceInfo();
			if (priceInfo != null) {
				truCartItemDetailVO.setSelectedShippingMethodPrice(priceInfo.getAmount());
			} else {
				truCartItemDetailVO.setSelectedShippingMethodPrice(newShippGroup.getShippingPrice());
			}
			
			final String freightClass = getShippingManager().getFreightClassFromCI(pNewShpGrpCIRel.getCommerceItem());
			final String region = getShippingManager().getShippingRegion(newShippGroup.getShippingAddress());			
			final List<TRUShipMethodVO> shipMethodVOs = getShippingManager().getShipMethods (
					freightClass, region, getOrder(), true);			
			
			shipmentVO.setShipMethodVOs(shipMethodVOs);
			shipmentVO.setShippingGroup(newShippGroup);
			shipmentVO.setSelectedShippingMethod(newShippGroup.getShippingMethod());
			shipmentVO.setEstimateShipWindowMin(pNewShpGrpCIRel.getEstimateShipWindowMin());
			shipmentVO.setEstimateShipWindowMax(pNewShpGrpCIRel.getEstimateShipWindowMax());
			//shipmentVO.setEstimateMinDate(getRelativeDate(pNewShpGrpCIRel.getEstimateShipWindowMin()));
			//shipmentVO.setEstimateMaxDate(getRelativeDate(pNewShpGrpCIRel.getEstimateShipWindowMax()));
			double shippingPrice = TRUCommerceConstants.DOUBLE_ZERO;
			if (newShippGroup.getPriceInfo() != null) {
				shippingPrice = newShippGroup.getPriceInfo().getAmount();
			}
			shipmentVO.setSelectedShippingPrice(shippingPrice);
			((TRUOrderImpl) getOrder()).setShipmentVO(shipmentVO);
			if (isLoggingDebug()) {
				vlogDebug("END of TRUShippingGroupFormHandler.updateCartItemDetailVO()");
			}
		}
		
		/**
		 * returns date in fomat DD/MM/YY *.
		 *
		 * @param pDays the days
		 * @return the relative date
		 */
		private String getRelativeDate(int pDays) {
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DATE, pDays);
			DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
			return df.format(now.getTime());
		}
		
		/**
		 * Copy channel shipping info.
		 *
		 * @param pOldShippingGroup the old shipping group
		 * @param pNewShippingGroup the new shipping group
		 */
		private void copyChannelShippingInfo(final TRUHardgoodShippingGroup pOldShippingGroup, TRUHardgoodShippingGroup pNewShippingGroup) {
			TRUChannelHardgoodShippingGroup channelNewShippingGroup = (TRUChannelHardgoodShippingGroup) pNewShippingGroup;
			TRUChannelHardgoodShippingGroup channelOldShippingGroup = (TRUChannelHardgoodShippingGroup) pOldShippingGroup;
			channelNewShippingGroup.setChannelId(channelOldShippingGroup.getChannelId());
			channelNewShippingGroup.setChannelType(channelOldShippingGroup.getChannelType());
			channelNewShippingGroup.setChannelName(channelOldShippingGroup.getChannelName());
			channelNewShippingGroup.setChannelUserName(channelOldShippingGroup.getChannelUserName());
			channelNewShippingGroup.setChannelCoUserName(channelOldShippingGroup.getChannelCoUserName());
			channelNewShippingGroup.setRegistrantId(channelOldShippingGroup.getRegistrantId());
			channelNewShippingGroup.setRegistryAddress(channelOldShippingGroup.isRegistryAddress());
			channelNewShippingGroup.setSourceChannel(channelOldShippingGroup.getSourceChannel());
		}
		
		/**
		 * This method will update the instore pickup person details
		 * @param pRequest -DynamoHttpServletRequest
		 * @param pResponse - DynamoHttpServletResponse
		 * @throws ServletException - ServletException
		 * @throws IOException -IOException
		 * @return boolean
		 */
		public boolean handleUpdateInStorePickUpData(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
				throws ServletException, IOException {
	        if (isLoggingDebug()) {
	        logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[handleUpdateInStorePickUpData]");
	       }
	        
	        TRUOrderManager orderManager = (TRUOrderManager) getOrderManager();
			
			TRUStorePickUpInfo[] storePickUpInfos = getStorePickUpInfos();
			if (storePickUpInfos != null) {
				for (TRUStorePickUpInfo storePickUpInfo : storePickUpInfos) {
					if (storePickUpInfo != null &&
							StringUtils.isNotBlank(storePickUpInfo.getShippingGroupId())) {
						try {
							ShippingGroup shippingGroupRel = getOrder().getShippingGroup(storePickUpInfo.getShippingGroupId());
							if (shippingGroupRel instanceof TRUInStorePickupShippingGroup) {
								TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) shippingGroupRel;
								if (inStorePickupShippingGroup != null) {
									orderManager.updateInStoreData(getOrder(), inStorePickupShippingGroup, storePickUpInfo);
								}
							}
						} catch (ShippingGroupNotFoundException  e) {
							if (isLoggingError()) {
								logError("ShippingGroupNotFoundException in TRUCSRShippingGroupFormHandler.handleUpdateInStorePickUpData", e);
							}
						}catch (CommerceException  e) {
							if (isLoggingError()) {
								logError("CommerceException in TRUCSRShippingGroupFormHandler.handleUpdateInStorePickUpData", e);
							}
						}
					}
				}
			}
			
			if (isLoggingDebug()) {
				logDebug("End of TRUCSRShippingGroupFormHandler.handleUpdateInStorePickUpData method");
			}
	        return checkFormRedirect(getApplyShippingMethodsSuccessURL(), getApplyShippingMethodsErrorURL(), pRequest, pResponse);
		}
		
		 /**
	     *  This method is overridden for shipping restriction.
	     *
	     *
	     * @param pRequest
	     *            the servlet's request
	     * @param pResponse
	     *            the servlet's response
	     * @throws ServletException
	     *             if an error occurred while processing the servlet request
	     * @throws IOException
	     *             if an error occurred while reading or writing the servlet
	     *
	     */
		
	public void preApplyShippingMethods(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preApplyShippingMethods]");
		}
		super.preApplyShippingMethods(pRequest, pResponse);
		try {
			final List sgCiRels = getShippingGroupManager().getAllShippingGroupRelationships(getOrder());
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels
							.get(i);
					if (sgCiRelsCount > TRUCSCConstants.NUMBER_ONE) {
						final CommerceItem commerceItem = sgCiRel.getCommerceItem();
						final TRUShippingGroupManager groupManager = (TRUShippingGroupManager) getShippingGroupManager();
						if (groupManager.isNotInstanceOfTRUCommerceItem(commerceItem)) {
							continue;
						}
						if (sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup) {
							final TRUHardgoodShippingGroup sg = (TRUHardgoodShippingGroup) sgCiRel.getShippingGroup();
							final Address address = sg.getShippingAddress();
							if (isLoggingDebug()) {
								vlogDebug(
										"Shipping address.getFirstName() ={0} ,  address.getLastName()={1}, and ShippingMethod={2}",
										address.getFirstName(), address.getLastName(), sgCiRel.getShippingGroup().getShippingMethod());
							}
							if (address == null || StringUtils.isBlank(address.getFirstName()) ||
									StringUtils.isBlank(address.getLastName()) || StringUtils.isBlank(address.getAddress1())) {
								String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_ADDRESS_MISSING_INFO);
								addFormException(new DropletException(errorMessage, commerceItem.getCatalogRefId() + TRUConstants.IFAN));
								errorMessage = null;
								return;
							} else if (StringUtils.isBlank(sgCiRel.getShippingGroup().getShippingMethod())) {
								String errorMessage = getErrorMessage(TRUErrorKeys.TRU_ERROR_SHIPPING_NO_SHIP_METHODS_MULTISHIP);
								addFormException(new DropletException(errorMessage, sg.getId()));
								errorMessage = null;
								return;
							}
							setOrderReconciliationNeeded(isOrderReconciliationRequired());
						}

				}
		
			else if (StringUtils.isBlank(sgCiRel.getShippingGroup().getShippingMethod())) {

						if (sgCiRel.getShippingGroup() instanceof HardgoodShippingGroup){
							if ((sgCiRel.getShippingGroup() == null) || sgCiRel.getShippingGroup().getShippingMethod() == null||sgCiRel.getShippingGroup().getShippingMethod().isEmpty()) {
								{
									addFormException(new DropletException(
											TRUCSCConstants.SOME_INFO_MISSING));

						}
					}

						}
						setOrderReconciliationNeeded(isOrderReconciliationRequired());
					}
				}
			}
			//runProcessValidationShippingInfos(pRequest, pResponse);
		}
		catch (CommerceException e) {
			addFormException(new DropletException(PLEASE_TRY_AGAIN));
			if (isLoggingError()) {
				logError("CommerceException in TRUShippingGroupFormHandler.preShipToMultiAddress", e);
			}
		}

		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRShippingGroupFormHandler] method:[preApplyShippingMethods]");
		}
	}
	
 	/**
 	 * <p>
 	 * This method will get the error message for givem error key.
 	 * </p>
 	 * @param pErrorKey - pErrorKey
 	 * @return errorMessage - errorMessage
 	 */
 	private String getErrorMessage(String pErrorKey){
 		String errorMessage=pErrorKey;
 		try {
			 errorMessage = getErrorHandlerManager().getErrorMessage(pErrorKey);
		} catch (com.tru.common.cml.SystemException e) {
			if(isLoggingError()){
				logError("com.tru.common.cml.SystemException @Class:::TRUShippingGroupFormHandler::@method::getErrorMessage()\t:"+e,e);
			}
		}
 		return errorMessage;
 	}
		
 
}