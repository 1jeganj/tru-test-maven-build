package com.tru.commerce.csr.order;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.agent.events.ItemUpdateAgentEvent;
import atg.commerce.CommerceException;
import atg.commerce.csr.environment.CSREnvironmentTools;
import atg.commerce.csr.order.CSRCreateCreditCardFormHandler;
import atg.commerce.csr.util.CSRAgentTools;
import atg.commerce.order.CreditCard;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.purchase.CommerceIdentifierPaymentInfo;
import atg.commerce.order.purchase.CommerceIdentifierPaymentInfoContainer;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.states.OrderStates;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.PropertyUpdate;

import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.purchase.TRUBillingProcessHelper;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.vo.TRUAddressDoctorResponseVO;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUIntegrationStatusUtil;


/**
 * This class extends ATG OOTB CSRCreateCreditCardFormHandler and used
 * for managing the credit card related operation.
 * @author Professional Access
 * @version 1.0
 * 
 * 
 */
public class TRUCSRCreateCreditCardFormHandler extends CSRCreateCreditCardFormHandler{

	/**
	 * property to hold mOrderTools.
	 */
	private TRUOrderTools mOrderTools;

	/** property: Reference to the ShippingProcessHelper component . */
	private TRUShippingProcessHelper mShippingHelper;

	/**
	 * property to hold mOrderStates.
	 */
	private OrderStates mOrderStates;

	/**
	 * property to hold PropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;

	/**
	 * Property to hold mAddressDoctor.
	 */
	private TRUCSRAddressDoctor mAddressDoctor;
	
	/**
	 * @return the mDefaultBillingAddress
	 */
	public RepositoryItem getDefaultBillingAddress() {
		return ((CommerceProfileTools)getProfileTools()).getDefaultBillingAddress(getProfile());
	}




	/**
	 * Property to hold mProfileTools.
	 */
	private TRUProfileTools mProfileTools;

	/**
	 * property to hold tRUConfiguration.
	 */
	private TRUConfiguration mTRUConfiguration;

	/**
	 * boolean property to hold if address is validated by address doctor or not.
	 */
	private String mAddressValidated;
	
	/** The m card type. */
	private String mCardType;
	
	/** The m email. */
	private String mCreditCardEmail;


	/**
	 *  Property to hold mCopyAddress.
	 */
	private boolean mCopyAddress;
	
	/**
	 * @return the isCopyAddress
	 */
	public boolean isCopyAddress() {
		return mCopyAddress;
	}
	/**
	 * @param pCopyAddress the mCopyAddress to set
	 */
	public void setCopyAddress(boolean pCopyAddress) {
		mCopyAddress = pCopyAddress;
	}
	
	/**
	 * property to hold nickNameCount.
	 */
	private static int nickNameCount=TRUCSCConstants.NUMBER_ONE ;
	
	/**
	 * Gets the email.
	 *
	 * @return the mCreditCardEmail
	 */
	public String getCreditCardEmail() {
		return mCreditCardEmail;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param pCreditCardEmail the new email
	 */
	public void setCreditCardEmail(String pCreditCardEmail) {
		this.mCreditCardEmail = pCreditCardEmail;
	}

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	public String getCardType() {
		return mCardType;
	}

	/**
	 * Sets the card type.
	 *
	 * @param pCardType the new card type
	 */
	public void setCardType(String pCardType) {
		this.mCardType = pCardType;
	}

	/**
	 * property: shipping address.
	 */
	private ContactInfo mAddress = new ContactInfo();

	/** Holds address values. */
	private ContactInfo mAddressInputFields = new ContactInfo();

	/**
	 * Property to hold address doctor response status.
	 */
	private String mAddressDoctorProcessStatus;

	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;

	/**
	 * property to hold mCommonSuccessURL.
	 */
	private String mCommonSuccessURL;

	/**
	 * property to hold mCommonErrorURL.
	 */
	private String mCommonErrorURL;
	
	/** Property to hold mIntegrationStatusUtil. */
	private TRUIntegrationStatusUtil mIntegrationStatusUtil;
	

	/** property: Reference to the BillingProcessHelper component .*/
	private TRUBillingProcessHelper mBillingHelper;
	
	/**
	 * @return the billingHelper
	 */
	public TRUBillingProcessHelper getBillingHelper() {
		return mBillingHelper;
	}

	/**
	 * @param pBillingHelper the billingHelper to set
	 */
	public void setBillingHelper(TRUBillingProcessHelper pBillingHelper) {
		mBillingHelper = pBillingHelper;
	}

	/**
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * @param pShippingHelper
	 *            the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}

	/**
	 * @return the orderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * @param pOrderTools the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/** map to hold credit card info */
	private Map<String, String> mCreditCardInfoMap = new HashMap<String, String>();
	/**
	 * @return the creditCardInfoMap
	 */
	public Map<String, String> getCreditCardInfoMap() {
		return mCreditCardInfoMap;
	}

	/**
	 * @param pCreditCardInfoMap the creditCardInfoMap to set
	 */
	public void setCreditCardInfoMap(Map<String, String> pCreditCardInfoMap) {
		mCreditCardInfoMap = pCreditCardInfoMap;
	}

	/**
	 * @return the billingAddressNickName
	 */
	public String getBillingAddressNickName() {
		return mBillingAddressNickName;
	}

	/**
	 * @param pBillingAddressNickName the billingAddressNickName to set
	 */
	public void setBillingAddressNickName(String pBillingAddressNickName) {
		mBillingAddressNickName = pBillingAddressNickName;
	}

	/** property to hold BillingAddressNickName */
	private String mBillingAddressNickName;


	/**
	 * @return the addressInputFields
	 */
	public ContactInfo getAddressInputFields() {
		return mAddressInputFields;
	}

	/**
	 * @param pAddressInputFields the addressInputFields to set
	 */
	public void setAddressInputFields(ContactInfo pAddressInputFields) {
		mAddressInputFields = pAddressInputFields;
	}


	/**
	 * @param pOrderStates
	 *            the mOrderStates to set
	 */

	public void setOrderStates(OrderStates pOrderStates)
	{
		mOrderStates = pOrderStates;
	}

	/**
	 * Gets the mOrderStates .
	 * 
	 * @return the mOrderStates
	 */

	public OrderStates getOrderStates()
	{
		return mOrderStates;
	}

	/**
	 * Gets the profile tools.
	 * 
	 * @return the mProfileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 * 
	 * @param pProfileTools
	 *            the mProfileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * @return the mAddressDoctor.
	 */
	public TRUCSRAddressDoctor getAddressDoctor() {
		return mAddressDoctor;
	}

	/**
	 * @param pAddressDoctor the TRUCSRAddressDoctor to set
	 */
	public void setAddressDoctor(TRUCSRAddressDoctor pAddressDoctor) {
		mAddressDoctor = pAddressDoctor;
	}

	/**
	 * @return the tRUConfiguration.
	 */
	public TRUConfiguration getTRUConfiguration() {
		return mTRUConfiguration;
	}

	/**
	 * @param pTRUConfiguration the tRUConfiguration to set
	 */
	public void setTRUConfiguration(TRUConfiguration pTRUConfiguration) {
		mTRUConfiguration = pTRUConfiguration;
	}

	/**
	 * mAddressValidated.
	 * 
	 * @return addressValidated
	 */
	public String getAddressValidated() {
		return mAddressValidated;
	}

	/**
	 * @param pAddressValidated the addressValidated to set
	 */
	public void setAddressValidated(String pAddressValidated) {
		mAddressValidated = pAddressValidated;
	}

	/**
	 * @return the address.
	 */
	public ContactInfo getAddress() {
		return mAddress;
	}

	/**
	 * @param pAddress the address to set.
	 */
	public void setAddress(ContactInfo pAddress) {
		mAddress = pAddress;
	}

	/**
	 * @return addressDoctorProcessStatus
	 */
	public String getAddressDoctorProcessStatus() {
		return mAddressDoctorProcessStatus;
	}

	/**
	 * 
	 * @param pAddressDoctorProcessStatus
	 *            the addressDoctorProcessStatus to set
	 */
	public void setAddressDoctorProcessStatus(String pAddressDoctorProcessStatus) {
		mAddressDoctorProcessStatus = pAddressDoctorProcessStatus;
	}

	/**
	 * @return the TRUAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}

	/**
	 * @return the commonSuccessURL
	 */
	public String getCommonSuccessURL() {
		return mCommonSuccessURL;
	}

	/**
	 * @param pCommonSuccessURL
	 *            the commonSuccessURL to set
	 */
	public void setCommonSuccessURL(String pCommonSuccessURL) {
		mCommonSuccessURL = pCommonSuccessURL;
	}

	/**
	 * @return the commonErrorURL
	 */
	public String getCommonErrorURL() {
		return mCommonErrorURL;
	}

	/**
	 * @param pCommonErrorURL
	 *            the commonErrorURL to set
	 */
	public void setCommonErrorURL(String pCommonErrorURL) {
		mCommonErrorURL = pCommonErrorURL;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * @return the integrationStatusUtil
	 */
	public TRUIntegrationStatusUtil getIntegrationStatusUtil() {
		return mIntegrationStatusUtil;
	}
	/**
	 * @param pIntegrationStatusUtil the integrationStatusUtil to set
	 */
	public void setIntegrationStatusUtil(TRUIntegrationStatusUtil pIntegrationStatusUtil) {
		mIntegrationStatusUtil = pIntegrationStatusUtil;
	}


	/**
	 * preCreateCreditCard method is overridden validate address.
	 * 
	 * @param pRequest
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void preCreateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException
	{
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCreateCreditCardFormHandler] method:[preCreateCreditCard]");
		}
//		boolean isEnableAddressDoctor = false;
//		boolean isAddressValidated = Boolean.parseBoolean(getAddressValidated());

		if (isLoggingDebug()) {
			logDebug("Start of TRUBillingInfoFormHandler/preAddCreditCard method");
		}

		if (isLoggingDebug()) {
			logDebug("TRUBillingInfoFormHandler.preAddCreditCard :: addCreditCardWithoutAddress");
		}
		
		if(!getFormError()){
			/**
			 * Validates the credit card fields.
			 */
			validateCreditCard();
		}
		if(!getFormError() && getOrderTools().isPayeezyEnabled()){
			if(!StringUtils.isBlank(TRUCSCConstants.ADDRESS_INPUT_EMAIL)){
				getAddressInputFields().setEmail(TRUCSCConstants.ADDRESS_INPUT_EMAIL);
			}else{
				getAddressInputFields().setEmail(((TRUOrderImpl)getOrder()).getEmail());
			}

			//Calling the Zero authorization for card verification.
			/*		TRUCreditCardStatus creditCardStatus = ((TRUPurchaseProcessHelper)getPurchaseProcessHelper()).verifyZeroAuthorization(
															TRUConstants.DOUBLE_ZERO,
																getOrder().getSiteId(),getCreditCardInfoMap(),getAddressInputFields(),getOrder().getId());
				//For any error adding it to the form errors.
				if(creditCardStatus != null && !creditCardStatus.getTransactionSuccess()){
					mVe.addValidationError(creditCardStatus.getErrorMessage(), null);
					getErrorHandler().processException(mVe, this);
				}*/
		}
		/*	} catch (ValidationExceptions ve) {
			getErrorHandler().processException(ve, this);
			if (isLoggingError()) {
				logError("Validation exception occured: {0}", ve);
			}
		} catch (SystemException se) {
			getErrorHandler().processException(se, this);
			if (isLoggingError()) {
				logError("System exception occured: {0}", se);
			}
		}*/

		// Validate address with AddressDoctor Service
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCreateCreditCardFormHandler] method:[preCreateCreditCard]");
		}
	}

	/**
	 * This method is for validating the credit card.
	 * 
	 * @throws ServletException
	 *             the ServletException
	 *             
	 */
	private void validateCreditCard() throws ServletException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCreateCreditCardFormHandler] method:[validateCreditCard]");
		}
		/*if (validationResult != 0) {
			switch (validationResult) {
			case TRUCommerceConstants.CARD_EXPIRED:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_ACCOUNT_CARD_EXPIRED, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_HAS_INVALID_CHARS:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_HAS_INVALID_CHARS,null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_DOESNT_MATCH_TYPE:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_DOESNT_MATCH_TYPE,null);
				break;
			case TRUCommerceConstants.CARD_LENGTH_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_LENGTH_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_NUMBER_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_NUMBER_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_INFO_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_INFO_NOT_VALID, null);
				break;
			case TRUCommerceConstants.CARD_EXP_DATE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_EXP_DATE_NOT_VALID,null);
				break;
			case TRUCommerceConstants.CARD_TYPE_NOT_VALID:
				mVe.addValidationError(TRUErrorKeys.TRU_ERROR_CHECK_CARD_TYPE_NOT_VALID, null);
				break;
			}
			getErrorHandler().processException(mVe, this);
		}*/
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCreateCreditCardFormHandler] method:[validateCreditCard]");
		}
	}



	/**
	 * handles the new Credit card flow.
	 * 
	 * @param pRequest - the request object
	 * @param pResponse - the response object
	 * @return boolean - form redirect success or Error url
	 * @throws ServletException - if any
	 * @throws IOException - if any
	 */
	@SuppressWarnings({ "unchecked", "unchecked" })
	@Override
	public boolean handleNewCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCreateCreditCardFormHandler] method:[handleNewCreditCard]");
		}	
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "TRUCSRCreateCreditCardFormHandler.handleNewCreditCard";
		Transaction tr = null;
        //boolean isEnableAddressDoctor = false;
		if (rrm == null || 
				rrm.isUniqueRequestEntry(myHandleMethod)) {
			/*boolean isEnableAddressDoctor = false;*/
			try {
				tr = ensureTransaction();

				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(myHandleMethod);
				}
				if (getFormError()) {
					if (isLoggingDebug()) {
						logDebug("Redirecting due to form error in addCreditCard");
					}
					return checkFormRedirect(getNewCreditCardSuccessURL(), 
											 getNewCreditCardErrorURL(), pRequest, pResponse);
				}
				preCreateCreditCard(pRequest,pResponse);

				if (!getFormError()) {
					
					tr = ensureTransaction();
					synchronized (getOrder()) {
							if(StringUtils.isNotBlank(getCreditCardEmail())){		
								((TRUOrderImpl) getOrder()).setEmail(getCreditCardEmail());
							}
				/*		String billingAddressNickName = getBillingAddressNickName();
						if(false) {
							TRUCSROrderManager orderManager = (TRUCSROrderManager) getCSRAgentTools() .getOrderManager();
							ContactInfo billingAddress  = new ContactInfo();
							OrderTools.copyAddress(getAddressInputFields(), billingAddress);
							//Adding the billing address to the credit card. 
							((TRUOrderImpl)getOrder()).setBillingAddress(billingAddress);
							
							String generatedUniqueNickName = orderManager.generateUniqueNickNameForAddress(getAddressInputFields(), getShippingGroupMapContainer().getShippingGroupMap());
							((TRUOrderImpl)getOrder()).setBillingAddressNickName(generatedUniqueNickName);
							final ShippingGroupManager sgm = getPurchaseProcessHelper().getShippingGroupManager();
							final TRUHardgoodShippingGroup hgsg = (TRUHardgoodShippingGroup) sgm.createShippingGroup();	   
							final Address address = hgsg.getShippingAddress();	    
							OrderTools.copyAddress(getAddressInputFields(), address);
							hgsg.setBillingAddress(Boolean.TRUE);
							hgsg.setNickName(generatedUniqueNickName);
							billingAddressNickName = generatedUniqueNickName;
							//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
							
							//hgsg.setLastActivity(new Timestamp((new Date()).getTime()));
							getShippingGroupMapContainer().addShippingGroup(generatedUniqueNickName, hgsg);
							setBillingAddressNickName(generatedUniqueNickName);
						}*/
					
						addCreditCard(pRequest, pResponse);
					
					
					TRUCreditCard creditCard = (TRUCreditCard)getCreditCard();
					
					
					
					getBillingHelper().updateCreditCard(getOrder(), getCreditCardInfoMap(), getAddressInputFields());
					creditCard.setBillingAddressNickName(((TRUOrderImpl)getOrder()).getBillingAddressNickName());
					
						if(StringUtils.isNotBlank(getCardType()))
							
						{
							((TRUOrderImpl) getOrder()).setCreditCardType(getCardType());
								Map extraParameters = new HashMap();
								CSRAgentTools csrAgentTools = getCSRAgentTools();
								CSREnvironmentTools csrenvtools = csrAgentTools.getCSREnvironmentTools();
								extraParameters.put(TRUConstants.PRICE_LIST, csrenvtools.getCurrentPriceList());
								extraParameters.put(TRUConstants.SALE_PRICE_LIST, csrenvtools.getCurrentSalePriceList());
								PricingModelHolder userPricingModels = csrenvtools.getCurrentOrderPricingModelHolder();
								userPricingModels.initializePricingModels();
								getPurchaseProcessHelper().runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL,
										getOrder(),
										userPricingModels,
										getUserLocale(),
										getProfile(),
										extraParameters,
										null);
							
						}
						
						
						final TRUShippingGroupContainerService shippingGroupMapContainer = (TRUShippingGroupContainerService) getShippingGroupMapContainer();
						
						//final TRUHardgoodShippingGroup hgsg1 = (TRUHardgoodShippingGroup) shippingGroupMapContainer.getShippingGroup("ll ll");
						
							if (getBillingAddressNickName().equalsIgnoreCase(shippingGroupMapContainer.getDefaultShippingGroupName())) {
								shippingGroupMapContainer.setDefaultShippingGroupName(null);
							}
							shippingGroupMapContainer.removeShippingGroup(getBillingAddressNickName());
						
						  final TRUProfileTools profileTools = getProfileTools();
							if (!profileTools.isAnonymousUser(getProfile())) {
								//processRemoveProfile(getBillingAddressNickName(), getProfile(), profileTools, profileTools.isAnonymousUser(getProfile()));
								
								try {
									getProfileTools().removeProfileRepositoryAddress(getProfile(), getBillingAddressNickName(), true);
									getProfileTools().setProfileDefaultAddress(getProfile());
								} catch (RepositoryException e) {
									vlogError("RepositoryException in TRUCSRCreateFormHandler.handleNewCreditCard", e);
								}
							}
					
						getOrderManager().updateOrder(getOrder());
					}
				}
			} catch (CommerceException cEx) {
				vlogError("CommerceException handleNewCreditCard method with exception : {0}", cEx);
				PerformanceMonitor.cancelOperation(myHandleMethod);
			} catch (RunProcessException e) {
					vlogError("RunProcessException in TRUCSRCreateFormHandler.handleNewCreditCard", e);
			} finally{
				if (tr != null) {
					commitTransaction(tr);
				}
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(myHandleMethod);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCreateCreditCardFormHandler] method:[handleNewCreditCard]");
		}
		return checkFormRedirect(getNewCreditCardSuccessURL(), getNewCreditCardErrorURL(), pRequest, pResponse);}



	/**
	 * Added for CSC functionalities.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 * @throws CommerceException
	 *             - if any
	 */
	
	protected void addCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		if (isLoggingDebug()) {
			logDebug("Entering into class:[TRUCSRCreateCreditCardFormHandler] method:[addCreditCard]");
		}	
		String billingAddressNickName=null;
		TRUProfileTools profileTools = (TRUProfileTools) getOrderManager().getOrderTools().getProfileTools();
		TRUPropertyManager propertyManager = (TRUPropertyManager) profileTools.getPropertyManager();

		billingAddressNickName =getAddressInputFields().getFirstName()+
				getAddressInputFields().getLastName()+nickNameCount;
		nickNameCount=nickNameCount+TRUCSCConstants.NUMBER_ONE;
				
		setBillingAddressNickName(billingAddressNickName);
		
		
	/*	if(isCopyAddress()){*/
			if (getShippingGroupMapContainer().getShippingGroup(billingAddressNickName) != null) {
				// updating address in shipping group map container and also in profile for logged in user
				getShippingHelper().modifyShippingAddress(getProfile(), billingAddressNickName,
						getAddressInputFields(), getShippingGroupMapContainer(),getOrder());
			} else {
				// adding address in shipping group map container and also in profile for logged in user
				getShippingHelper().addShippingAddress(getProfile(), billingAddressNickName,
						getAddressInputFields(), getShippingGroupMapContainer());
			}
		/*}*/

		String creditCardType = profileTools.getCreditCardTypeFromRepository
				(getCreditCardInfoMap().get(propertyManager.getCreditCardNumberPropertyName()),getCreditCardInfoMap().get(TRUConstants.CARD_LENGTH));

		vlogDebug("value of credit card type calculated from getOrderTools().getCreditCardType() creditCardType: {0} ", creditCardType);
		getCreditCardInfoMap().put(propertyManager.getCreditCardTypePropertyName(), creditCardType);


		((TRUCSRPurchaseProcessHelper)getPurchaseProcessHelper()).addCreditCard(getProfile(), getCreditCardInfoMap(), billingAddressNickName,
				getAddressInputFields(), getPaymentGroupMapContainer(),isCopyAddress());
		if (isLoggingDebug()) {
			logDebug("Exit from class:[TRUCSRCreateCreditCardFormHandler] method:[addCreditCard]");
		}
	}



	/**
	 * postCreateCreditCard method is overridden validate address.
	 * 
	 * @param pRequest
	 *            - the request object
	 * @param pResponse
	 *            - the response object
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */

	@Override
	public void postCreateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException
	{
		if(isLoggingDebug()){
			logDebug("TRUCSRCreateCreditCardFormHandler (postCreateCreditCard) : Starts");
		}
		CreditCard cc = getCreditCard();

		if (getRelationshipType() != null)
		{
			int type = RelationshipTypes.stringToType(getRelationshipType());
			CommerceIdentifierPaymentInfoContainer container = getCommerceIdentifierPaymentInfoContainer();
			List commerceIdentifierPaymentInfos = container.getAllCommerceIdentifierPaymentInfos();

			if (type == TRUCSCConstants.NOT_FOUND_NUMBER)
			{
				Iterator cipiIter = commerceIdentifierPaymentInfos.iterator();
				while (cipiIter.hasNext()) {
					CommerceIdentifierPaymentInfo cipi = (CommerceIdentifierPaymentInfo)cipiIter.next();
					if (cipi.getRelationshipType().equals(getRelationshipType())) {
						cipi.setPaymentMethod(getCreditCardName());
					}
				}
			}
			else
			{
				CommerceIdentifierPaymentInfo cipi = (CommerceIdentifierPaymentInfo)commerceIdentifierPaymentInfos.get(0);
				int existingType = RelationshipTypes.stringToType(cipi.getRelationshipType());
				boolean remainingType = false;
				if (existingType == TRUCSCConstants.TWO_NOT_THREE || 
					existingType == TRUCSCConstants.THREE_NOT_TWO || 
					existingType == TRUCSCConstants.NOT_FOUND_NUMBER || 
					existingType == TRUCSCConstants.FOUR_NOT_FIVE) {
					remainingType = true;
				}
				if (remainingType) {
					CommerceIdentifierPaymentInfo newCipi = createSpecificPaymentInfo(cipi);
					newCipi.setCommerceIdentifier(cipi.getCommerceIdentifier());
					newCipi.setRelationshipType(cipi.getAmountType());
					newCipi.setPaymentMethod(getCreditCardName());
					newCipi.setAmount(getAmount());
					container.addCommerceIdentifierPaymentInfo(cipi.getCommerceIdentifier().getId(), newCipi);
				}
				else {
					cipi.setSplitPaymentMethod(getCreditCardName());
					splitCommerceIdentifierPaymentInfoByAmount(cipi, getAmount());
				}
			}
		}

		Order order = getOrder();
		getAgentMessagingTools().sendAddPaymentGroupEvent(getProfile().getRepositoryId(), order.getId(), cc.getId(), getTicketId());
		TRUCSRPurchaseProcessHelper helper = (TRUCSRPurchaseProcessHelper) getPurchaseProcessHelper();

		helper.updateStates(order);
		try
		{
			getOrderManager().updateOrder(order);
		}
		catch (CommerceException ce) {
			processException(ce, TRUCSCConstants.ERROR_UPDATING_ORDER_AFTER_ADDING_CREDIT_CARD, pRequest, pResponse);
		}

		List updates = new ArrayList();

		if (getCreditCard() == null || 
			getProfile() == null || 
			!isCopyToProfile()) {
			return;
		}
		CommercePropertyManager cmgr = (CommercePropertyManager)getCommerceProfileTools().getPropertyManager();
		RepositoryItem profilecc = getOrderManager().getOrderTools().getProfileTools().getCreditCardByNickname(getCreditCardName(), getProfile());

		if (profilecc != null) {
			PropertyUpdate update = new PropertyUpdate(
					TRUCSCConstants.TWO, 
					getCommerceProfileTools().formatMultiValueUpdateString(
					cmgr.getCreditCardPropertyName(), getCreditCardName(), profilecc),
					TRUConstants.EMPTY_STRING, profilecc.toString());
			updates.add(update);
			ItemUpdateAgentEvent event = getAgentMessagingTools().createItemUpdateAgentEvent(
					getItemUpdateActivityType(), 
					getProfile(), 
					getProfile().getRepositoryId(), 
					updates, getTicketId());

			getAgentMessagingTools().sendItemUpdateAgentEvent(event, getAgentMessagingTools().getUpdateProfileJMSType());
			createCreateCreditCardTicketActivity(getTicket(), updates);
		}
		if(isLoggingDebug()){
			logDebug("TRUCSRCreateCreditCardFormHandler (postCreateCreditCard) : Ends");
		}
	}

	
	
	/**
	 * validates the address with address doctor.
	 * 
	 * @param pAddress
	 *            address object in which the user entered address is populated
	 * @return the status address doctor returned
	 */
	public String validateAddress(Address pAddress) {
		if(isLoggingDebug()){
			logDebug("TRUCSRCreateCreditCardFormHandler (validateAddress) : Starts");
		}
		vlogDebug("INPUT PARAMS OF validateAddress method pAddress: {0} ", pAddress);
		String addressStatus = null;
		if (pAddress != null) {
			Map<String, String> addressValue = new HashMap<String, String>();
			addressValue.put(TRUConstants.ADDRESS_1, pAddress.getAddress1());
			addressValue.put(TRUConstants.ADDRESS_2, pAddress.getAddress2());
			addressValue.put(TRUConstants.CITY, pAddress.getCity());
			addressValue.put(TRUConstants.STATE, pAddress.getState());
			addressValue.put(TRUConstants.POSTAL_CODE_FOR_ADD, pAddress.getPostalCode());
			addressStatus = getProfileTools().validateAddress(addressValue);
		}
		vlogDebug(" returns addressStatus: {0} ", addressStatus);
		
		if (addressStatus == null) {
			addressStatus = TRUConstants.ADDRESS_DOCTOR_STATUS_VALIDATION_ERROR;
		}
		if(isLoggingDebug()){
			logDebug("TRUCSRCreateCreditCardFormHandler (validateAddress) : Ends");
		}
		return addressStatus;
	}

}