package com.tru.commerce.csr.order;

import com.tru.common.vo.TRUAddressDoctorResponseVO;

/**
 * Class 'TRUCSRAddressDoctor' used to manage the TRU Specific Address Doctor.
 * Integration functionalities.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUCSRAddressDoctor {

	/**
	 * Property to hold Address doctor response VO.
	 */
	private TRUAddressDoctorResponseVO mAddressDoctorResponseVO;

	/**
	 * @return the TRUAddressDoctorResponseVO
	 */
	public TRUAddressDoctorResponseVO getAddressDoctorResponseVO() {
		return mAddressDoctorResponseVO;
	}

	/**
	 * 
	 * @param pAddressDoctorResponseVO
	 *            the addressDoctorResponseVO to set
	 */
	public void setAddressDoctorResponseVO(
			TRUAddressDoctorResponseVO pAddressDoctorResponseVO) {
		this.mAddressDoctorResponseVO = pAddressDoctorResponseVO;
	}
	/**
	 * Property to hold mTaxwareError.
	 */
	
	private Boolean mTaxwareError;
	
	/**
	 * @return the mTaxwareError
	 */
	
	public Boolean getTaxwareError() {
		return mTaxwareError;
	}
	/**
	 * 
	 * @param pTaxwareError
	 *            the mTaxwareError to set
	 */
	public void setTaxwareError(Boolean pTaxwareError) {
		this.mTaxwareError = pTaxwareError;
	}
	
}
