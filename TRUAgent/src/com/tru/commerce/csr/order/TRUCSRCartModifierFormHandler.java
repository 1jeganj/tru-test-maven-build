package com.tru.commerce.csr.order;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.CommerceException;
import atg.commerce.csr.order.CSRCartModifierFormHandler;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.RelationshipNotFoundException;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.processor.BillingAddrValidator;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.utils.TRUShoppingCartUtils;
import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.csr.order.purchase.TRUCSRPurchaseProcessHelper;
import com.tru.commerce.csr.util.TRUCSCConstants;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUOutOfStockItem;
import com.tru.commerce.order.TRUPipeLineConstants;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.purchase.TRUAddCommerceItemInfo;
import com.tru.commerce.order.purchase.TRUGiftingPurchaseProcessHelper;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.common.TRUStoreConfiguration;
import com.tru.common.TRUUserSession;
import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;

/**
 * This class extends ATG OOTB CSRCartModifierFormHandler and used
 * to manage the order.
 * @version 1.0
 * @author Professional Access
 * 
 * 
 */
public class TRUCSRCartModifierFormHandler extends CSRCartModifierFormHandler {
	
	/** The Item inline messages. */
	private Map<String, String> mItemInlineMessages;
	
	/** The Cart info message. */
	private boolean mAddUpdateItem;
	
	/** Property To hold Bpp sku Id. */
	private String mBppSkuId;
	
	/** The Relation ship id. */
	private String mRelationShipId;
	
	/** Property To hold Bpp item Id. */
	private String mBppItemId;
	
	/** The Constant SUCCESS_URL. */
	private static final String SUCCESS_URL = "/framework.jsp?_windowid=3&t=commerceTab&ps=cmcShoppingCartPS";
	
	/** The Constant ERROR_URL. */
	private static final String ERROR_URL = "/framework.jsp?_windowid=3&t=commerceTab&ps=cmcShoppingCartPS";
	
	/** The Constant HANDLE_ADD_DONATION_ITEM. */
	private static final String HANDLE_ADD_DONATION_ITEM = "CartModifierOrderFormHandler.handleAddDonationItemToOrder";
	
	/** property to Hold mPageName. */
    private String mPageName;
    
    /** The Desired qty by relationship id. */
    private String mDesiredQTYByRelationshipId;
    
    /** property to Hold shopping cart utils. */
    private TRUShoppingCartUtils mShoppingCartUtils;
    
	/** property to hold user session component. */
	private TRUUserSession mUserSession;

	/** property to hold Gift Wrap Sku Id. */
	private String mGiftWrapSkuId;

	/** property to hold Wrap Item Quantity. */
	private String mWrapItemQuantity;

	/** property to hold Wrap Item Quantity. */
	private String mPrevWrapItemQuantity;

	/** The Donation amount. */
	private double mDonationAmount;

	/** The Is donation item. */
	private boolean mDonationItem;

	/** The Is TRUGiftingPurchaseProcessHelper. */
	private TRUGiftingPurchaseProcessHelper mGiftingProcessHelper;
	
	/** The Is saleTaxCall. */
	private boolean mSaleTaxCall = Boolean.TRUE;
	
	/** Static property to HANDLE_GIFT_WRAP_METHOD NAME. */
	/**	private final static String HANDLE_GIFT_WRAP_METHOD = "CartModifierOrderFormHandler.handleGiftWrap" */

	/** Static property to HANDLE_ADD_UPDATE_BPP_ITEM. */
	private static final String HANDLE_ADD_UPDATE_BPP_ITEM = "CartModifierOrderFormHandler.handleAddUpdateBppItem";
	
	/** Holds String PayerID. */
	private String mPayerId;
	
	/** Holds String PayPal Token. */
	private String mPayPalToken;
	
	/** property to hold mBillingAddressValidator.*/
	private BillingAddrValidator mBillingAddressValidator;
	
	/** property to hold mPaypalRedirectURL.*/
	private String mPaypalRedirectURL;
	
	/** The Store configuration. */
	private TRUStoreConfiguration mStoreConfiguration;
	    
	/** Property to Hold is remove item. */
	private boolean mSplitItemRemove;
	
	/** property to hold mAddSuccessURL. */
	private String mAddSuccessURL;
	
	/** property to hold mAddErrorURL.*/
	private String mAddErrorURL;
	
	/** This property hold reference for TRUConfiguration.*/
	private TRUConfiguration mTruConfiguration;

	/** Property to hold moveToConfirmationChainId. */
	private String mMoveToConfirmationChainId;
	
	/**
	 * Gets the item inline messages.
	 * @return the item inline messages
	 */
	public Map<String, String> getItemInlineMessages() {
		if(null == mItemInlineMessages) {
			mItemInlineMessages = new HashMap<String, String>();
		}
		return mItemInlineMessages;
	}

	/**
	 * Sets the item inline messages.
	 *
	 * @param pItemInlineMessages the item inline messages
	 */
	public void setItemInlineMessages(Map<String, String> pItemInlineMessages) {
		mItemInlineMessages = pItemInlineMessages;
	}
	
	/**
     * Gets the DesiredQTYByRelationshipId.
     *
     * @return the DesiredQTYByRelationshipId
     */
	public String getDesiredQTYByRelationshipId() {
		return mDesiredQTYByRelationshipId;
	}

	 /**
     * Sets the DesiredQTYByRelationshipId.
     *
     * @param pDesiredQTYByRelationshipId the DesiredQTYByRelationshipId
     */
	public void setDesiredQTYByRelationshipId(String pDesiredQTYByRelationshipId) {
		this.mDesiredQTYByRelationshipId = pDesiredQTYByRelationshipId;
	}

	/**
     * Gets the page name.
     *
     * @return the page name
     */
    public String getPageName() {
          return mPageName;
    }
    
    /**
     * Sets the page name.
     *
     * @param pPageName the new page name
     */
    public void setPageName(String pPageName) {
          this.mPageName = pPageName;
    }
    
    /**
    * Gets the shopping cart utils.
    * 
     * @return the shopping cart utils
    */
    public TRUShoppingCartUtils getShoppingCartUtils() {
          return mShoppingCartUtils;
    }

    /**
    * Sets the shopping cart utils.
    * 
     * @param pShoppingCartUtils
    *            the new shopping cart utils
    */
    public void setShoppingCartUtils(TRUShoppingCartUtils pShoppingCartUtils) {
          this.mShoppingCartUtils = pShoppingCartUtils;
    }
	
	/**
	 * Gets the mAddSuccessURL.
	 *
	 * @return the mAddSuccessURL.
	 */
	
    public String getAddSuccessURL() {
		return mAddSuccessURL;
	}
    /**
	 * Sets the mAddSuccessURL.
	 *
	 * @param pAddSuccessURL the new mAddSuccessURL
	 */

	public void setAddSuccessURL(String pAddSuccessURL) {
		this.mAddSuccessURL = pAddSuccessURL;
	}
	
	/**
	 * Gets the mAddErrorURL.
	 *
	 * @return the mAddErrorURL.
	 */

	public String getAddErrorURL() {
		return mAddErrorURL;
	}

	/**
	 * Sets the mAddErrorURL.
	 *
	 * @param pAddErrorURL the new pAddErrorURL
	 */
	
	public void setAddErrorURL(String pAddErrorURL) {
		this.mAddErrorURL = pAddErrorURL;
	}


	/**
	 * Checks if is split item remove.
	 *
	 * @return the splitItemRemove
	 */
	public boolean isSplitItemRemove() {
		return mSplitItemRemove;
	}


	/**
	 * Sets the split item remove.
	 *
	 * @param pSplitItemRemove the splitItemRemove to set
	 */
	public void setSplitItemRemove(boolean pSplitItemRemove) {
		mSplitItemRemove = pSplitItemRemove;
	}
	
	/**
	 * Gets the store configuration.
	 *
	 * @return the store configuration
	 */
	public TRUStoreConfiguration getStoreConfiguration() {
		return mStoreConfiguration;
	}

	/**
	 * Sets the store configuration.
	 *
	 * @param pStoreConfiguration the new store configuration
	 */
	public void setStoreConfiguration(TRUStoreConfiguration pStoreConfiguration) {
		mStoreConfiguration = pStoreConfiguration;
	}
	
	/**
	 * Gets the paypal redirect url.
	 *
	 * @return the mPaypalRedirectURL
	 */
	public String getPaypalRedirectURL() {
		return mPaypalRedirectURL;
	}

	/**
	 * Sets the paypal redirect url.
	 *
	 * @param pPaypalRedirectURL the pPaypalRedirectURL to set
	 */
	public void setPaypalRedirectURL(String pPaypalRedirectURL) {
		this.mPaypalRedirectURL = pPaypalRedirectURL;
	}
	
	/**
	 * Gets the billing address validator.
	 *
	 * @return the billing address validator
	 */	
	public BillingAddrValidator getBillingAddressValidator() {
		return mBillingAddressValidator;
	}

	/**
	 * Sets the billing address validator.
	 *
	 * @param pBillingAddressValidator the pBillingAddressValidator to set
	 */	
	public void setBillingAddressValidator(BillingAddrValidator pBillingAddressValidator) {
		this.mBillingAddressValidator = pBillingAddressValidator;
	}
	
	/**
	 * Gets the pay pal token.
	 *
	 * @return the mPayPalToken
	 */
	public String getPayPalToken() {
		return mPayPalToken;
	}
	
	/**
	 * Sets the pay pal token.
	 *
	 * @param pPayPalToken the pPayPalToken to set
	 */
	public void setPayPalToken(String pPayPalToken) {
		this.mPayPalToken = pPayPalToken;
	}
	
	/**
	 * Gets the payer id.
	 *
	 * @return the mPayerId
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * Sets the payer id.
	 *
	 * @param pPayerId the pPayerId to set
	 */
	public void setPayerId(String pPayerId) {
		this.mPayerId = pPayerId;
	}
	

	/**
	 * Gets the gifting process helper.
	 * 
	 * @return the giftingProcessHelper
	 */
	public TRUGiftingPurchaseProcessHelper getGiftingProcessHelper() {
		return mGiftingProcessHelper;
	}

	/**
	 * Sets the gifting process helper.
	 * 
	 * @param pGiftingProcessHelper
	 *            the giftingProcessHelper to set
	 */
	public void setGiftingProcessHelper(TRUGiftingPurchaseProcessHelper pGiftingProcessHelper) {
		mGiftingProcessHelper = pGiftingProcessHelper;
	}

	/**
	 * Checks if is donation item.
	 * 
	 * @return true, if is donation item
	 */
	public boolean isDonationItem() {
		return mDonationItem;
	}

	/**
	 * Sets the donation item.
	 * 
	 * @param pDonationItem
	 *            the new donation item
	 */
	public void setDonationItem(boolean pDonationItem) {
		mDonationItem = pDonationItem;
	}

	/**
	 * Gets the donation amount.
	 * 
	 * @return the donation amount
	 */
	public double getDonationAmount() {
		return mDonationAmount;
	}

	/**
	 * Sets the donation amount.
	 * 
	 * @param pDonationAmount
	 *            the new donation amount
	 */
	public void setDonationAmount(double pDonationAmount) {
		mDonationAmount = pDonationAmount;
	}

	/**
	 * This method gets moveToConfirmationChainId.
	 * 
	 * @return the moveToConfirmationChainId
	 */
	public String getMoveToConfirmationChainId() {
		return mMoveToConfirmationChainId;
	}

	/**
	 * This method sets moveToConfirmationChainId.
	 * 
	 * @param pMoveToConfirmationChainId
	 *            the moveToConfirmationChainId to set
	 */
	public void setMoveToConfirmationChainId(String pMoveToConfirmationChainId) {
		mMoveToConfirmationChainId = pMoveToConfirmationChainId;
	}

	/**
	 * Returns the this property hold reference for TRUConfiguration.
	 * 
	 * @return the mTruConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * sets commerce TRUConfiguration.
	 * 
	 * @param pTruConfiguration
	 *            the TRUConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		this.mTruConfiguration = pTruConfiguration;
	}

	/**
	 * Gets the prev wrap item quantity.
	 * 
	 * @return the prevWrapItemQuantity
	 */
	public String getPrevWrapItemQuantity() {
		return mPrevWrapItemQuantity;
	}

	/**
	 * Sets the prev wrap item quantity.
	 * 
	 * @param pPrevWrapItemQuantity
	 *            the prevWrapItemQuantity to set
	 */
	public void setPrevWrapItemQuantity(String pPrevWrapItemQuantity) {
		mPrevWrapItemQuantity = pPrevWrapItemQuantity;
	}

	/**
	 * Gets the wrap item quantity.
	 * 
	 * @return the wrapItemQuantity
	 */
	public String getWrapItemQuantity() {
		return mWrapItemQuantity;
	}

	/**
	 * Sets the wrap item quantity.
	 * 
	 * @param pWrapItemQuantity
	 *            the wrapItemQuantity to set
	 */
	public void setWrapItemQuantity(String pWrapItemQuantity) {
		mWrapItemQuantity = pWrapItemQuantity;
	}

	/**
	 * Gets the gift wrap sku id.
	 * 
	 * @return the giftWrapSkuId
	 */
	public String getGiftWrapSkuId() {
		return mGiftWrapSkuId;
	}

	/**
	 * Sets the gift wrap sku id.
	 * 
	 * @param pGiftWrapSkuId
	 *            the giftWrapSkuId to set
	 */
	public void setGiftWrapSkuId(String pGiftWrapSkuId) {
		mGiftWrapSkuId = pGiftWrapSkuId;
	}

	/**
	 * Gets the user session.
	 * 
	 * @return the userSession
	 */
	public TRUUserSession getUserSession() {
		return mUserSession;
	}

	/**
	 * Sets the user session.
	 * 
	 * @param pUserSession
	 *            the userSession to set
	 */
	public void setUserSession(TRUUserSession pUserSession) {
		mUserSession = pUserSession;
	}

	/**
	 * Checks if is sale tax call.
	 *
	 * @return the saleTaxCall
	 */
	public boolean isSaleTaxCall() {
		return mSaleTaxCall;
	}

	/**
	 * Sets the sale tax call.
	 *
	 * @param pSaleTaxCall the saleTaxCall to set
	 */
	public void setSaleTaxCall(boolean pSaleTaxCall) {
		mSaleTaxCall = pSaleTaxCall;
	}
	
	/**
	 * Gets the bpp sku id.
	 * 
	 * @return the bpp sku id
	 */
	public String getBppSkuId() {
		return mBppSkuId;
	}
	
	/**
	 * Sets the BPP info id.
	 * 
	 * @param pBppSkuId
	 *            the new BPP info id
	 */
	public void setBppSkuId(String pBppSkuId) {
		mBppSkuId = pBppSkuId;
	}
	/**
	 * property to hold mRestService.
	 */
	private boolean mRestService;
	

	/** Property to Hold Error handler manager. */
	private DefaultErrorHandlerManager mErrorHandlerManager;
	
	/**
	 * Checks if is rest service.
	 *
	 * @return the mRestService
	 */
	public boolean isRestService() {
		return mRestService;
	}

	/**
	 * Sets the rest service.
	 *
	 * @param pRestService the pRestService to set
	 */
	public void setRestService(boolean pRestService) {
		this.mRestService = pRestService;
	}
	
	/** The BPP info id. */
	private String mBPPInfoId;
	
	/**
	 * Gets the BPP info id.
	 * 
	 * @return the BPP info id
	 */
	public String getBPPInfoId() {
		return mBPPInfoId;
	}
	/**
	 * Gets the error handler manager.
	 * 
	 * @return the error handler manager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}
	
	/**
	 * Sets the error handler manager.
	 * 
	 * @param pErrorHandlerManager
	 *            the new error handler manager
	 */
	public void setErrorHandlerManager(DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * Sets the BPP info id.
	 * 
	 * @param pBPPInfoId
	 *            the new BPP info id
	 */
	public void setBPPInfoId(String pBPPInfoId) {
		mBPPInfoId = pBPPInfoId;
	}
	
	/**
	 * Gets the bpp item id.
	 * 
	 * @return the bpp item id
	 */
	public String getBppItemId() {
		return mBppItemId;
	}

	/**
	 * Sets the BPP info id.
	 * 
	 * @param pBppItemId
	 *            the new BPP info id
	 */
	public void setBppItemId(String pBppItemId) {
		mBppItemId = pBppItemId;
	}

	/**
	 * Gets the relation ship id.
	 * 
	 * @return the relation ship id
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}
	
	/**
	 * Sets the relation ship id.
	 * 
	 * @param pRelationShipId
	 *            the new relation ship id
	 */
	public void setRelationShipId(String pRelationShipId) {
		mRelationShipId = pRelationShipId;
	}

	/**
	 * Checks if is adds the update item.
	 * 
	 * @return true, if is adds the update item
	 */
	public boolean isAddUpdateItem() {
		return mAddUpdateItem;
	}

	/**
	 * Sets the adds the update item.
	 * 
	 * @param pAddUpdateItem
	 *            the new adds the update item
	 */
	public void setAddUpdateItem(boolean pAddUpdateItem) {
		mAddUpdateItem = pAddUpdateItem;
	}
	
	
	/**
	 * This  OOTB method  is overridden to do the merge for donation item.
	 *
	 * @param pRequest                             - DynamoHttpServletRequest
	 * @param pResponse                            - DynamoHttpServletResponse
	 * @throws ServletException                    - ServletException if any
	 * @throws IOException                         - IOException if any
	 */
	protected void addItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException
		  {
		    boolean valid = mergeItemInputForAdd(pRequest, pResponse, getCommerceItemType(), false);
		    
		    if (isDonationItem()) {
				mergeItemCustomInputForAdd();
			}
		    if (valid){
		      doAddItemsToOrder(pRequest, pResponse);
		  }
		}
	
	/**
	 * Merge item custom input for add.
	 */
	private void mergeItemCustomInputForAdd() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::mergeItemCustomInputForAdd() : BEGIN ");
			vlogDebug(
					"channel Id : {0} : channelType : {1} : channelUserName : {2} : channelCoUserName : {3} : channelName : {4} :" + 
							" CatalogRefIds : {5} : isDonationItem : {6}",
					getCatalogRefIds(), isDonationItem());
		}
		for (int index = 0; index < getCatalogRefIds().length; ++index) {
			final AddCommerceItemInfo addCommerceItemInfo = getItems()[index];
			
			if (isDonationItem()) {
				final String classType = ((TRUOrderTools) getOrderManager().getOrderTools()).getDonationCommerceItemClassType();
				if (isLoggingDebug()) {
					vlogDebug("classType : {0} : DonationAmount : {1}", classType, getDonationAmount());
				}
				if (!StringUtils.isBlank(classType)) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setCommerceItemType(classType);
				}
				if (getDonationAmount() != 0) {
					((TRUAddCommerceItemInfo) addCommerceItemInfo).setDonationAmount(getDonationAmount());
					//updateOrder(getOrder(), null, pRequest, pResponse);
					try {
						getOrderManager().updateOrder(getOrder());
					} catch (CommerceException e) {
						if (isLoggingError()) {
							logError("CommerceException in @Class::TRUCSRCartModifierFormHandler::@method::mergeItemCustomInputForAdd()", e);
						}
					
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::mergeItemCustomInputForAdd() : END ");
		}
	}
	
	
	/**
     * This is OOTB method which is overridden to do the validation before
     * adding an item to the cart.
     * 
     * @param pRequest                             - DynamoHttpServletRequest
     * @param pResponse                            - DynamoHttpServletResponse
     * @throws ServletException                    - ServletException if any
     * @throws IOException                         - IOException if any
     * 
      */
	public void preAddItemToOrder(DynamoHttpServletRequest pRequest,
                  DynamoHttpServletResponse pResponse) throws ServletException,
                  IOException {
           if (isLoggingDebug()) {
                  logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddItemToOrder() : BEGIN");
           }
           final AddCommerceItemInfo[] items = this.getItems();
           String locationId = null;
           String addItemCount = (String) pRequest.getLocalParameter(TRUCSCConstants.ADD_ITEM_COUNT);
           
           if (!StringUtils.isBlank(getLocationId())) {
                  locationId = getLocationId();
           } else {
                  locationId = getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome();
           }
           if(TRUCSCConstants.PICK_UP_STORE.equalsIgnoreCase(getPageName()))
         {
        	   String skuId = (String) pRequest.getLocalParameter(TRUCSCConstants.SKU_ID);
               String qty=(String) pRequest.getLocalParameter(TRUCSCConstants.QTY);
               long quantity= Long.parseLong(qty);
               performQtyValidations(skuId, quantity, locationId);
        }else if((items!=null &&items.length >= TRUCSCConstants.NUMBER_ONE) || TRUCSCConstants.PDP.equalsIgnoreCase(getPageName()) || (addItemCount!=null)){
        		Map<String, Long> currentItems = new HashMap<String, Long>();
           if (null != items) {
                  vlogDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddItemToOrder() : Items size : {0}", items.length);
           }
           
           if(items != null){
                  for(AddCommerceItemInfo commerceItem : items){
                         String currentCatalogRef = commerceItem.getCatalogRefId();
                         Long currentQty = commerceItem.getQuantity();
                         if(currentQty >0){
                         currentItems.put(currentCatalogRef, currentQty);
                         }
                         
                  }
           }
                  Iterator<Map.Entry<String, Long>> entries = currentItems.entrySet().iterator();
                  while (entries.hasNext()) {
                      Map.Entry<String, Long> entry = entries.next();
                         if (isLoggingDebug()) {
                               vlogDebug(
                                             "@Class::TRUCSRCartModifierFormHandler::@method::preAddItemToOrder() : skuId : {0} : quantity : {1} : location id : {2}",
                                             entry.getKey(),  entry.getValue(), locationId);
                         }
                         performQtyValidations(entry.getKey(), entry.getValue(), locationId);
                  }
           }
        else if(isDonationItem()){
        	
        	  if (isLoggingDebug()) {
                  logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddItemToOrder() : donation item");
           }
        }
        else{
                  String skuId = (String) pRequest.getLocalParameter(TRUCSCConstants.CATALOG_REF_ID);
                  String qty=(String) pRequest.getLocalParameter(TRUCSCConstants.QTY);
                  long quantity= Long.parseLong(qty);
                  performQtyValidations(skuId, quantity, locationId);
           }
           if (isLoggingDebug()) {
                  logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddItemToOrder() : END");
           }
     }
     
 	/**
 	 * Check the sku price and inventory.
 	 * 
 	 * @param pSkuId
 	 *            the sku id
 	 * @param pQuantity
 	 *            the quantity
 	 * @param pLocationId
 	 *            the location id
 	 */
 	protected void performQtyValidations(String pSkuId, long pQuantity, String pLocationId) {
 		if (isLoggingDebug()) {
 			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidations() : BEGIN");
 			vlogDebug("pSkuId : {0} : pQuantity : {1} : pLocationId : {2}", pSkuId, pQuantity, pLocationId);
 		}
 		try {
 			String inlineInfoMessage = null;
 			final String displayName = getShoppingCartUtils().getDisplayNameForSku(pSkuId);
 			int invenotryStatus = TRUCommerceConstants.INT_IN_STOCK;
 			RepositoryItem skuItem = getCatalogTools().findSKU(pSkuId);
 			Date currentDate = new Date();
 			if (skuItem != null) {
 				Date streetDate = (Date) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
 						.getStreetDate());
 				String backOrderStatus = (String) skuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
 						.getBackOrderStatus());
 				if (streetDate != null && currentDate.before(streetDate)) {
 					if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
 						invenotryStatus = TRUCommerceConstants.INT_PRE_ORDER;
 					} else if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.N)) {
 						invenotryStatus = TRUCommerceConstants.INT_OUT_OF_STOCK;
 					} else {
 						invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
 					}
 				} else {
 					invenotryStatus = getShoppingCartUtils().getInventoryStatus(pSkuId, pLocationId);
 				}
 			}
 			inlineInfoMessage = getInlineInfoMessage(pSkuId, pQuantity, pLocationId,
 					  displayName, invenotryStatus);
 			if(StringUtils.isNotBlank(inlineInfoMessage)) {
 				getItemInlineMessages().put(pSkuId + TRUCommerceConstants.UNDER_SCORE + pLocationId, inlineInfoMessage);
 			}
 		} catch (RepositoryException | SystemException e) {
 			if (isLoggingError()) {
 				logError(" RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preAddItemToOrder() :", e);
 			}
 		}
 		if (isLoggingDebug()) {
 			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::performQtyValidations() : END");
 		}
 	}
 	
 	/**
 	 * This method will  get inline message.
 	 * @param pSkuId - sku id
 	 * @param pQuantity - quantity
 	 * @param pLocationId - location id
 	 * @param pDisplayName - display name
 	 * @param pInvenotryStatus - inventory status
 	 * @return String - string
 	 * @throws RepositoryException - Repository Exception
 	 * @throws SystemException - System Exception
 	 */
 		private String getInlineInfoMessage(String pSkuId, long pQuantity, String pLocationId,
 				final String pDisplayName,
 				final int pInvenotryStatus) throws RepositoryException,
 				SystemException {
 			String infoMessage;
 			String inlineInfoMessage = null;
 			long inventoryQty = 0;
 			long itemQtyInCart = 0;
 			long itemRelQtyInCart = 0;
 			long customerLimit = 0;
 			if (pInvenotryStatus == TRUCommerceConstants.INT_IN_STOCK || pInvenotryStatus == TRUCommerceConstants.INT_LIMITED_STOCK
 					|| pInvenotryStatus == TRUCommerceConstants.INT_PRE_ORDER) {
 				RepositoryItem pSkuItem = getCatalogTools().findSKU(pSkuId);
 				Date currentDate = new Date();
 				if (pSkuItem != null) {
 					Date streetDate = (Date) pSkuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
 							.getStreetDate());
 					String backOrderStatus = (String) pSkuItem.getPropertyValue(getShoppingCartUtils().getPropertyManager()
 							.getBackOrderStatus());
 					if (streetDate != null && currentDate.before(streetDate)) {
 						if (backOrderStatus != null && backOrderStatus.equalsIgnoreCase(TRUCommerceConstants.R)) {
 							inventoryQty = TRUCommerceConstants.TEST_MODE_STOCK_LEVEL;
 						} else {
 							inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, pInvenotryStatus);
 						}
 					} else {
 						inventoryQty = getShoppingCartUtils().getItemInventory(pSkuId, pLocationId, pInvenotryStatus);
 					}
 					itemRelQtyInCart = getShoppingCartUtils().getItemQtyInCartByRelShip(pSkuId, pLocationId,
 							getOrder());
 				}
 				customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(pSkuId);
 				itemQtyInCart = getShoppingCartUtils().getItemQtyInCart(pSkuId, getOrder());

 				final long purchaseLimit = customerLimit - itemQtyInCart;
 				final long inventoryLimit = inventoryQty - itemRelQtyInCart;
 				if (isLoggingDebug()) {
 					vlogDebug(
 							"invenotryStatus : {0} : inventoryQty : {1} : itemRelQtyInCart : {2} : customerLimit : {3} : itemQtyInCart : {4}",
 							pInvenotryStatus, inventoryQty, itemRelQtyInCart, customerLimit, itemQtyInCart);
 				}
 				if (purchaseLimit > inventoryLimit) {
 					if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
 						infoMessage = getErrorHandlerManager().getErrorMessage(
 								TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
 						infoMessage = MessageFormat.format(infoMessage, pDisplayName, inventoryQty);
 						infoMessage=infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
 						addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
 						inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
 								TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
 					} else if (inventoryLimit < pQuantity) {
 						if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome()
 								.equalsIgnoreCase(pLocationId)) {
 							infoMessage = getErrorHandlerManager().getErrorMessage(
 									TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
 						} else {
 							infoMessage = getErrorHandlerManager().getErrorMessage(
 									TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
 						}
 						infoMessage = MessageFormat.format(infoMessage, pDisplayName);
 						infoMessage=infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
 						addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
 						inlineInfoMessage = getErrorHandlerManager().getErrorMessage(
 								TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
 					}
 				} else {
 					if (purchaseLimit == TRUCommerceConstants.INT_ZERO) {
 						infoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
 						infoMessage = MessageFormat.format(infoMessage, pDisplayName);
 						infoMessage=infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
 						addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
 						inlineInfoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
 					} else if (purchaseLimit < pQuantity) {
 						infoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY);
 						infoMessage = MessageFormat.format(infoMessage, pDisplayName);
 						infoMessage=infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
 						addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
 						inlineInfoMessage = getErrorHandlerManager().getErrorMessage(TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY);
 					}
 				}
 			} else if (pInvenotryStatus == TRUCommerceConstants.INT_OUT_OF_STOCK
 					|| pInvenotryStatus == TRUCommerceConstants.NUM_ZERO) {
 				if (isLoggingDebug()) {
 					vlogDebug("invenotryStatus : {0} : inventoryQty", pInvenotryStatus);
 				}
 				if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
 					infoMessage = getErrorHandlerManager().getErrorMessage(
 							TRUCommerceConstants.TRU_SHOPPINGCART_NO_ONLINE_INVENTORY_INFO_MESSAGE_KEY);
 				} else {
 					infoMessage = getErrorHandlerManager().getErrorMessage(
 							TRUCommerceConstants.TRU_SHOPPINGCART_NO_INVENTORY_AT_SELECTED_STORE_INFO_MESSAGE_KEY);
 				}
 				infoMessage = MessageFormat.format(infoMessage, pDisplayName);
 				infoMessage=infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
 				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
 			}
 			return inlineInfoMessage;
 		}
	
	
	/**
	 * Pre add update bpp item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	public void preAddUpdateBppItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddUpdateBppItem() : BEGIN : getRelationShipId() : {0}",
					getRelationShipId());
		}
		if (StringUtils.isBlank(getRelationShipId()) || StringUtils.isBlank(getBppSkuId()) || StringUtils.isBlank(getBppItemId())) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException se) {
				vlogError(
						"SystemException in @Class::TRUCartModifierFormHandler::@method::preAddUpdateBppItem()",
						se);
			}
		} 
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preAddUpdateBppItem() : END");
		}
	}
	/**
	 * Handle add update bpp item.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleAddUpdateBppItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::handleAddUpdateBppItem() : BEGIN");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null != getOrder()) {
			if (isLoggingDebug()) {
			vlogDebug("order id : {0} : BPPInfoId : {1} : RelationShipId : {2} : BppSkuId : {3} : BppItemId : {4}",
					getOrder().getId(), getBPPInfoId(), getRelationShipId(), getBppSkuId(), getBppItemId());
			}
			if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_ADD_UPDATE_BPP_ITEM))) {
				Transaction tr = null;
				try {
					tr = ensureTransaction();
					synchronized (getOrder()) {
						preAddUpdateBppItem(pRequest, pResponse);
						if(!getFormError()){
						if (StringUtils.isBlank(getBPPInfoId())) {
							((TRUOrderTools) getOrderManager().getOrderTools()).addBppItemToOrder(getOrder(), getRelationShipId(),
									getBppSkuId(), getBppItemId());
						} else if (StringUtils.isNotBlank(getBPPInfoId())) {
							((TRUOrderTools) getOrderManager().getOrderTools()).updateBppItemInOrder(getOrder(), getRelationShipId(),
									getBppSkuId(), getBppItemId());
						}
						getPurchaseProcessHelper().runProcessRepriceOrder(
								getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
								getUserLocale(), getProfile(), createRepriceParameterMap(), this);
						if(StringUtils.isNotBlank(getPageName()) && getPageName().equalsIgnoreCase(TRUCSCConstants.REVIEW))
						{
						super.runProcessRecalcPaymentGroupAmounts(getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
						}
						updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
				      }
					}
				} catch (RelationshipNotFoundException rnfe) {
					vlogError("RelationshipNotFoundException in handleAddUpdateBppItem : ", rnfe);
				} catch (InvalidParameterException ipe) {
					vlogError("InvalidParameterException in handleAddUpdateBppItem : ", ipe);
				} catch (RepositoryException re) {
					vlogError("RepositoryException in handleAddUpdateBppItem : ", re);
				} catch (RunProcessException rpe) {
					vlogError("RunProcessException in handleAddUpdateBppItem : ", rpe);
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(HANDLE_ADD_UPDATE_BPP_ITEM);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::handleAddUpdateBppItem() : END");
		}
		//if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(), getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Pre remove bpp item relationship.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	public void preRemoveBPPItemRelationship(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUCSRCartModifierFormHandler::@method::preRemoveBPPItemRelationship() : BEGIN : getRelationShipId() : {0}",
					getRelationShipId());
		}
		
		if (StringUtils.isBlank(getRelationShipId())) {
			try {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			} catch (SystemException se) {
				vlogError(
						"SystemException in @Class::TRUCartModifierFormHandler::@method::preRemoveBPPItemRelationship()",
						se);
			}
		} 
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preRemoveBPPItemRelationship() : END");
		}
	}
	
	/**
	 * Handle remove bpp item relationship.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleRemoveBPPItemRelationship(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::handleRemoveBPPItemRelationship() : BEGIN");
		}
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null != getOrder()) {
			vlogDebug("order id : {0} : RelationShipId : {1}", getOrder().getId(), getRelationShipId());
			if ((rrm == null) || (rrm.isUniqueRequestEntry(TRUCommerceConstants.HANDLE_REMOVE_BPP_ITEM_METHOD))) {
				Transaction tr = null;
				try {
					tr = ensureTransaction();
					preRemoveBPPItemRelationship(pRequest, pResponse);
					if(!getFormError()){
					synchronized (getOrder()) {
						((TRUOrderTools) getOrderManager().getOrderTools()).removeBPPItemInfo(getOrder(), getRelationShipId());
						getPurchaseProcessHelper().runProcessRepriceOrder(
								getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
								getUserLocale(), getProfile(), createRepriceParameterMap(), this);
						if(StringUtils.isNotBlank(getPageName()) && getPageName().equalsIgnoreCase(TRUCSCConstants.REVIEW))
						{
						super.runProcessRecalcPaymentGroupAmounts(getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
						}
						getOrderManager().updateOrder(getOrder());
					}
				  }
				} catch (RelationshipNotFoundException rnfe) {
					vlogError(rnfe, "RelationshipNotFoundException in @method: handleRemoveBPPItemRelationship");
				} catch (InvalidParameterException ipe) {
					vlogError(ipe, "InvalidParameterException in @method: handleRemoveBPPItemRelationship");
				} catch (RepositoryException repositoryException) {
					vlogError(repositoryException, "RepositoryException in @method: handleRemoveBPPItemRelationship");
				} catch (CommerceException commerceException) {
					vlogError(commerceException, "CommerceException in @method: handleRemoveBPPItemRelationship");
				} catch (RunProcessException runProcessException) {
					vlogError(runProcessException, "RunProcessException in @method: handleRemoveBPPItemRelationship");
				} finally {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (rrm != null) {
						rrm.removeRequestEntry(TRUCommerceConstants.HANDLE_REMOVE_BPP_ITEM_METHOD);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::handleRemoveBPPItemRelationship() : END");
		}
		// if the request is from rest service
		if (isRestService()) {
			return checkFormRedirect(getAddItemToOrderSuccessURL(),getAddItemToOrderErrorURL(), pRequest, pResponse);
		}
		return false;
	}
	

	/**
	 * Handle add donation item to order.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @return true, if successful
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean handleAddDonationItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddDonationItemToOrder() : BEGIN");
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(HANDLE_ADD_DONATION_ITEM))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					final String donationCommerceId = getDonationCommerceId();
					if (isLoggingDebug()) {
						vlogDebug("donationCommerceId : {0} : DonationAmount : {1}", donationCommerceId, getDonationAmount());
					}
					if (donationCommerceId != null) {
						final CommerceItem commerceItem = getOrder().getCommerceItem(donationCommerceId);
						if (commerceItem != null) {
							commerceItem.getQuantity();
							getOrder().getRelationships();
						final Double totalDonationAmount = ((TRUDonationCommerceItem) commerceItem).getDonationAmount() + getDonationAmount();
							((TRUDonationCommerceItem) commerceItem).setDonationAmount(totalDonationAmount);
							getPurchaseProcessHelper().runProcessRepriceOrder(
									getPurchaseProcessHelper().getAddItemToOrderPricingOp(), getOrder(), getUserPricingModels(),
									getUserLocale(), getProfile(), createRepriceParameterMap(), this);
							updateOrder(getOrder(), TRUCommerceConstants.ERROR_UPDATING_ORDER, pRequest, pResponse);
							return checkFormRedirect(SUCCESS_URL,ERROR_URL, pRequest, pResponse);
							
						}
					} else {
						setSiteId(getStoreConfiguration().getToysRUsSiteId());
						super.handleAddItemToOrder(pRequest, pResponse);
						return checkFormRedirect(SUCCESS_URL,ERROR_URL, pRequest, pResponse);
					}
				}
			} catch (CommerceItemNotFoundException cinfe) {
				if (isLoggingError()) {
					logError("CommerceItemNotFoundException in handleAddDonationItemToOrder : ", cinfe);
				}
			} catch (InvalidParameterException ipe) {
				if (isLoggingError()) {
					logError("InvalidParameterException in handleAddDonationItemToOrder : ", ipe);
				}
			} catch (RunProcessException runProcessException) {
				if (isLoggingError()) {
					logError("RunProcessException in handleAddDonationItemToOrder : ", runProcessException);
				}
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(HANDLE_ADD_DONATION_ITEM);
				}
				
			}

		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::handleAddDonationItemToOrder() : END");
		}
		
		return false;
	}
	
	/**
	 * Gets the donation commerce id.
	 * 
	 * @return the donation commerce id
	 */
	private String getDonationCommerceId() {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getDonationCommerceId() : BEGIN");
		}
		if (null != getOrder()) {
			@SuppressWarnings("unchecked")
			final List<CommerceItem> commerceItems = getOrder().getCommerceItems();
			if (!commerceItems.isEmpty()) {
				for (CommerceItem commerceItem : commerceItems) {
					if (commerceItem instanceof TRUDonationCommerceItem) {
						return commerceItem.getId();
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCartModifierFormHandler::@method::getDonationCommerceId() : END");
		}
		return null;
	}
	
			
	
	/**
	 * This method will be called after all the processing is done in removeItemFromOrder method, updateRemoveCartCookies() method
	 * will called to update/remove cart cookie accordingly.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 */
	@Override
	public void preRemoveItemFromOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		// Updating Giftwrap Item
		String infoMessage = null;
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId() : BEGIN : GiftWrapSkuId : {0}",
					getGiftWrapSkuId());
		}
		try {
			
			if (StringUtils.isNotBlank(getGiftWrapSkuId())) {
				final long giftWrapQty = Long.parseLong(getWrapItemQuantity());
				getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(), giftWrapQty, false);
			}
			if (StringUtils.isBlank(getRelationShipId())) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_EMPTY_INFO_MESSAGE_KEY);
				setAddUpdateItem(Boolean.FALSE);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
			}

		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(
						"CommerceException @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						ce);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(
						"RepositoryException @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						re);
			}
		} catch (SystemException se) {
			if (isLoggingError()) {
				logError(
						"SystemException in @Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId()",
						se);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preRemoveItemFromOrderByRelationshipId() : END");
		}
	}

	/**
	 * Pre set order by relationship id.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	public void preSetOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug(
					"@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId() : BEGIN : isSplitItemRemove : {0}",
					isSplitItemRemove());
		}
		try {
			if (isSplitItemRemove()) {
				preSetOrderByRelationshipIdForSplitItemRemove(pRequest, pResponse);
			} else {
				setAddUpdateItem(Boolean.TRUE);
			}
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError("CommerceException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId()", ce);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError("RepositoryException in @Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId()", re);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * Pre set order by relationship id for remove.
	 * 
	 * @param pRequest
	 *            the request
	 * @param pResponse
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CommerceException
	 *             - thows this
	 * @throws RepositoryException
	 *             - if this catch
	 */
	protected void preSetOrderByRelationshipIdForSplitItemRemove(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForSplitItemRemove() : BEGIN");
		}
		long desiredQty = 0;
		ShippingGroupCommerceItemRelationship selectedShippingGroupRel = null;
		long relQtyInCart = 0;
		setAddUpdateItem(Boolean.TRUE);
		if (isLoggingDebug()) {
			vlogDebug("Order : {0} : isSplitItemRemove() : {1} : getGiftWrapSkuId() : {2}", getOrder(), isSplitItemRemove(),
					getGiftWrapSkuId());
		}
		try {
			if (null != getOrder()) {
				if (!StringUtils.isBlank(getDesiredQTYByRelationshipId())) {
					desiredQty = Long.parseLong(getDesiredQTYByRelationshipId());
				}
				
				selectedShippingGroupRel = (TRUShippingGroupCommerceItemRelationship) getOrder().getRelationship(
						getRelationShipId());
				relQtyInCart = selectedShippingGroupRel.getQuantity();
				if (isLoggingDebug()) {
					vlogDebug("desiredQty : {0} : relQtyInCart : {1}", desiredQty, relQtyInCart);
				}
				if (isSplitItemRemove() && !StringUtils.isBlank(getRelationShipId())) {
					// Gift Wrap changes
					if (StringUtils.isNotBlank(getGiftWrapSkuId())) {
						final long giftWrapQty = Long.parseLong(getWrapItemQuantity());
						getGiftingProcessHelper().updateGiftWrap(getOrder(), getRelationShipId(), getGiftWrapSkuId(),
								giftWrapQty, false);
						if (desiredQty == 0) {
							desiredQty = giftWrapQty;
						}
					}
					if (desiredQty == relQtyInCart) {
						final String[] removalId = new String[TRUConstants.SIZE_ONE];
						removalId[TRUConstants.ZERO] = getRelationShipId();
						setRemovalRelationshipIds(removalId);
						return;
					} else {
						pRequest.setParameter(getRelationShipId(), relQtyInCart - desiredQty);
						return;
					}
				}
			}
		} catch (RelationshipNotFoundException e) {
			if (isLoggingError()) {
				logError(" RelationshipNotFoundException in @Class::TRUBaseCartModifierFormHandler::@method::" + 
						"preSetOrderByRelationshipIdForSplitItemRemove() :", e);
			}
		} catch (InvalidParameterException e) {
			if (isLoggingError()) {
				logError(" InvalidParameterException in @Class::TRUBaseCartModifierFormHandler::@method::" + 
								"preSetOrderByRelationshipIdForSplitItemRemove() :", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::preSetOrderByRelationshipIdForSplitItemRemove() : END");
		}
	}
	
	
	/**
	 * Pre set order by relationship id for update.
	 *
	 * @param pRequest            the request
	 * @param pDesiredQty            the desired qty
	 * @param pLocationId            the location id
	 * @param pInventoryStatus            the inventory status
	 * @param pSelectedShippingGroupRel the selected shipping group rel
	 * @throws RepositoryException             the repository exception
	 * @throws SystemException             the system exception
	 */

	protected void setOrderByRelationshipIdForUpdate(DynamoHttpServletRequest pRequest, long pDesiredQty, String pLocationId,
			int pInventoryStatus, ShippingGroupCommerceItemRelationship pSelectedShippingGroupRel) throws RepositoryException,
			SystemException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForUpdate() : BEGIN");
		}
		long inventoryQty = 0;
		long customerLimit = 0;
		String infoMessage = null;
		long relQuantityPerItemInCart = 0;

		String skuId = pSelectedShippingGroupRel.getCommerceItem().getCatalogRefId();
		long relQuantity = pSelectedShippingGroupRel.getQuantity();
		long commerceItemQtyInCart = pSelectedShippingGroupRel.getCommerceItem().getQuantity();
		String displayName = getShoppingCartUtils().getDisplayNameForSku(skuId);

		customerLimit = getShoppingCartUtils().getCustomerPurchaseLimit(skuId);
		inventoryQty = getShoppingCartUtils().getItemInventory(skuId, pLocationId, pInventoryStatus);
		relQuantityPerItemInCart = (getShoppingCartUtils().getItemQtyInCartByRelShip(skuId, pLocationId, getOrder())) - relQuantity;
		final long purchaseLimit = customerLimit - (commerceItemQtyInCart - relQuantity);
		final long inventoryLimit = inventoryQty - relQuantityPerItemInCart;

		if (isLoggingDebug()) {
			vlogDebug("desiredQty : {0} relQuantity : {1} skuId : {2} : locationId : {3} displayName : {4} inventoryQty : {5} : customerLimit : {6}",
					pDesiredQty, relQuantity, skuId, pLocationId, displayName, inventoryQty, customerLimit);
			vlogDebug("purchaseLimit : {0} inventoryLimit : {1}", purchaseLimit, inventoryLimit);
		}

		if (purchaseLimit > inventoryLimit) {
			if (inventoryLimit == TRUCommerceConstants.INT_ZERO) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				setAddUpdateItem(Boolean.FALSE);
			} else if (inventoryLimit < pDesiredQty) {
				if (getShoppingCartUtils().getPropertyManager().getLocationIdForShipToHome().equalsIgnoreCase(pLocationId)) {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_ENFORCEMENT_MESSAGE_KEY);
				} else {
					infoMessage = getErrorHandlerManager().getErrorMessage(
							TRUCommerceConstants.TRU_CART_ITEM_INVENTORY_AT_SELECTED_STORE_ENFORCEMENT_INFO_MSG_KEY);
				}
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				pRequest.setParameter(getRelationShipId(), inventoryLimit);
			} else if (inventoryLimit == pDesiredQty) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_INVENTORY_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName, inventoryQty);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_ADJUSTMENT_INFO_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			}
		} else {
			if (purchaseLimit == TRUCommerceConstants.INT_ZERO) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				setAddUpdateItem(Boolean.FALSE);
			} else if (purchaseLimit < pDesiredQty) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.TRU_SHOPPINGCART_ITEM_LIMIT_ENFORCEMENT_INLINE_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
				pRequest.setParameter(getRelationShipId(), purchaseLimit);
			} else if (purchaseLimit == pDesiredQty) {
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INFO_MESSAGE_KEY);
				infoMessage = MessageFormat.format(infoMessage, displayName);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_TOP_MSG));
				infoMessage = getErrorHandlerManager().getErrorMessage(
						TRUCommerceConstants.SHOPPINGCART_QTY_LIMIT_REACHED_INLINE_INFO_MESSAGE_KEY);
				addFormException(new DropletException(infoMessage, TRUCommerceConstants.CART_INLINE_MSG));
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::setOrderByRelationshipIdForUpdate() : END");
		}
	}
	
	
	/**
	 * Pre move to purchase info.
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void preMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preMoveToPurchaseInfo() : START");
		}
		try {
			TRUOrderImpl orderImpl=(TRUOrderImpl) getOrder();		
			synchronized (orderImpl) {
				orderImpl.setValidationFailed(Boolean.FALSE);	
				int countBeforeRemovalOfOOSItems = 0;
				((TRUOrderImpl) getOrder()).setOutOfStockItems(null);
				List sgCiRels = getShippingGroupCommerceItemRelationships(getOrder());
				if ((sgCiRels != null && !sgCiRels.isEmpty())) {
					final int sgCiRelsCount = sgCiRels.size();
					for (int i = TRUCommerceConstants.INT_ZERO; i < sgCiRelsCount; ++i) {
						final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
						final TRUCommerceItemImpl item = (TRUCommerceItemImpl) sgCiRel.getCommerceItem();
						if (item instanceof TRUGiftWrapCommerceItem) {
							continue;
						}else{
							countBeforeRemovalOfOOSItems++;
						}
					}
				}
				Map parameters = new HashMap();
				TRUUserSession sessionComponent = getUserSession();
				parameters.put(TRUPipeLineConstants.SESSION_COMPONENT, sessionComponent);
				parameters.put(TRUPipeLineConstants.CHAIN_ID, TRUPipeLineConstants.MOVE_TO_PURCHASE_INFO);
				 ((TRUCSRPurchaseProcessHelper)getPurchaseProcessHelper()).runProcessTRUValidationCheckoutChain(getOrder(),
						getUserPricingModels(), getUserLocale(), getProfile(), parameters, null, this);
					
					// Calling the reprice order and adjusting the PGs when the order gets modified
					if (((TRUOrderImpl) getOrder()).isValidationFailed()) {
						resetFormExceptions();
						boolean allOutOfStock = false;
						boolean showRedirectMessage = false;
						boolean hasLocationId = false;
						if(!((TRUOrderImpl)getOrder()).getOutOfStockItems().isEmpty()){
							List<TRUOutOfStockItem> outOfStockItems = ((TRUOrderImpl) getOrder()).getOutOfStockItems();
							for(TRUOutOfStockItem itemVO : outOfStockItems){
								String locationId = (String)itemVO.getPropertyValue(TRUCSCConstants.LOCATION_ID);
								if(locationId != null){
									hasLocationId = true;
								}
							}
							if(!hasLocationId && outOfStockItems != null && outOfStockItems.size() == countBeforeRemovalOfOOSItems){
								String infoMessage = TRUCSCConstants.ALL_ITEMS_OUT_OF_STOCK;
								allOutOfStock = true;
								addFormException(new DropletException(infoMessage));
							}else{
								for(TRUOutOfStockItem itemVO : outOfStockItems){
									String itemName = (String)itemVO.getPropertyValue(TRUCSCConstants.CATALOG_REF_ID);
									String locationId = (String)itemVO.getPropertyValue(TRUCSCConstants.LOCATION_ID);
									String infoMessage = TRUCSCConstants.INVERTED_COMMA;
									if(locationId != null){
										infoMessage = TRUCSCConstants.ITEM_OUT_OF_STOCK+itemName+TRUCSCConstants.AT_LOCATION+locationId+TRUCSCConstants.PLEASE_SELECT_OTHER_LOCATION;
									}else{
										infoMessage = TRUCSCConstants.ITEM_OUT_OF_STOCK+itemName;
									}
									if(infoMessage != null){
										infoMessage = infoMessage.replaceAll(TRUCSCConstants.REPLACE_HTML_STRING, TRUCheckoutConstants.EMPTY_STRING);
										showRedirectMessage = true;
										addFormException(new DropletException(infoMessage));
									}
								}
							}
						}
						if(!allOutOfStock && showRedirectMessage && !hasLocationId){
							addFormException(new DropletException(TRUCSCConstants.FEW_ITEMS_OUT_OF_STOCK_REDIRECT_CART_MSG));
						}
						runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
						//((TRUOrderManager) getOrderManager()).adjustPaymentGroups(getOrder());
						getOrderManager().updateOrder(getOrder());
						return;
					}
					runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, getOrder(), getUserPricingModels(), getUserLocale(), getProfile(), createRepriceParameterMap());
					getOrderManager().updateOrder(getOrder());
			}
		}catch (RunProcessException rpE) {
			if (isLoggingError()) {
				logError("RunProcessException occurred TRUCSRCartModifierFormHandler::@method::preMoveToPurchaseInfo()", rpE);
			}
			
		}catch (CommerceException rpE) {
			if (isLoggingError()) {
				logError("CommerceException occurred TRUCSRCartModifierFormHandler::@method::preMoveToPurchaseInfo()", rpE);
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::preMoveToPurchaseInfo() : END");
		}
	}
	
	/**
	 * Modify Order By Realtionship ID
	 *
	 * @param pRequest            the request
	 * @param pResponse            the response
	 * @throws ServletException             the servlet exception
	 * @throws CommerceException             the commerce exception
	 * @throws RunProcessException             the RunProcessException exception
	 * @throws IOException             Signals that an I/O exception has occurred.
	 */
	protected void modifyOrderByRelationshipId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException, RunProcessException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUBaseCartModifierFormHandler::@method::modifyOrderByRelationshipId() : BEGIN : isAddUpdateItem : {0}",
					isAddUpdateItem());
		}
		if (isAddUpdateItem()) {
			super.modifyOrderByRelationshipId(pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUBaseCartModifierFormHandler::@method::modifyOrderByRelationshipId() : END");
		}
	}
	
	/**
	 * Gets the shipping group commerce item relationships.
	 * 
	 * @param pOrder
	 *            the order.
	 * @return the shipping group commerce item relationships.
	 * @throws CommerceException
	 *             the commerce exception.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List getShippingGroupCommerceItemRelationships(Order pOrder) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSRCartModifierFormHandler::@method::getShippingGroupCommerceItemRelationships() : START : Order --> "	+ pOrder);
		}
		List<TRUShippingGroupCommerceItemRelationship> rels=new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		List<CommerceItem> cItems=pOrder.getCommerceItems();
		List<CommerceItem> tmpList = new ArrayList<CommerceItem>(cItems); 
		Collections.reverse(tmpList);
		for(CommerceItem cItem:tmpList){
			rels.addAll(cItem.getShippingGroupRelationships());
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRUCSRCartModifierFormHandler::@method::getShippingGroupCommerceItemRelationships() : END");
		}
		return rels;
		}
	
	}

