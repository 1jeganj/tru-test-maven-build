package com.tru.commerce.order;

import atg.commerce.CommerceException;
import atg.commerce.order.ChangedProperties;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

/**
 * The Class TRUCSROrderTools.
 * @author Professional Access
 * @version 1.0
 */

public class TRUCSROrderTools extends TRUOrderTools{
	
	/**
	 * @param pSrcSGCIRel - SrcSGCIRel
	 * @param pDestSGCIRel - DestSGCIRel
	 * @throws CommerceException - CommerceException
	 */
	public void copyShippingRelationShip(ShippingGroupCommerceItemRelationship pSrcSGCIRel, 
			ShippingGroupCommerceItemRelationship pDestSGCIRel) throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUCSROrderTools::@method::copyShippingRelationShip() : BEGIN");
			}
		try {
			//pDestSGCIRel.setCommerceItem(pSrcSGCIRel.getCommerceItem());
			if (pSrcSGCIRel != null && pDestSGCIRel != null) {
				TRUBppItemInfo srcBppItemInfo = ((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).getBppItemInfo();
				TRUBppItemInfo destBppItemInfo = ((TRUShippingGroupCommerceItemRelationship) pDestSGCIRel).getBppItemInfo();
				//If src null and dest is not null==>Copy the dest value into SRC
				if(srcBppItemInfo.getId()==null && destBppItemInfo.getId()!=null){
					MutableRepository orderRepository = (MutableRepository) getOrderRepository();
					MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());					
					destBppItemInfo = new TRUBppItemInfo();					
					destBppItemInfo.setId(bppItemInfoForAdd.getRepositoryId());														
					if (destBppItemInfo instanceof ChangedProperties) {
						((ChangedProperties) destBppItemInfo).setSaveAllProperties(Boolean.TRUE);
						((ChangedProperties) destBppItemInfo).setRepositoryItem(bppItemInfoForAdd);
					}					
					destBppItemInfo.setBppItemId(destBppItemInfo.getBppItemId());
					destBppItemInfo.setBppSkuId(destBppItemInfo.getBppSkuId());
					destBppItemInfo.setBppPrice(destBppItemInfo.getBppPrice());	
					((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).setBppItemInfo(destBppItemInfo);
				}else if(srcBppItemInfo.getId()!=null && destBppItemInfo.getId()==null){					
					MutableRepository orderRepository = (MutableRepository) getOrderRepository();
					MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());					
					TRUBppItemInfo destBppItemInfo1 = new TRUBppItemInfo();					
					destBppItemInfo1.setId(bppItemInfoForAdd.getRepositoryId());														
					if (destBppItemInfo instanceof ChangedProperties) {
						((ChangedProperties) destBppItemInfo1).setSaveAllProperties(Boolean.TRUE);
						((ChangedProperties) destBppItemInfo1).setRepositoryItem(bppItemInfoForAdd);
					}					
					destBppItemInfo1.setBppItemId(srcBppItemInfo.getBppItemId());
					destBppItemInfo1.setBppSkuId(srcBppItemInfo.getBppSkuId());
					destBppItemInfo1.setBppPrice(srcBppItemInfo.getBppPrice());	
					((TRUShippingGroupCommerceItemRelationship) pDestSGCIRel).setBppItemInfo(destBppItemInfo1);									
				}else if(srcBppItemInfo.getId()!=null && destBppItemInfo.getId()!=null){
					((TRUShippingGroupCommerceItemRelationship) pSrcSGCIRel).setBppItemInfo(destBppItemInfo);
				}
			}		
			
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError("RepositoryException occured in TRUCSROrderTools.copyShippingRelationShip()", e);
			}
			throw new CommerceException(e);
		} 
	}
	
	
	
	/**
	 * copyShippingBPPRelationShip shipping BBP info.
	 *
	 * @param pSGCIRel the old shipping group
	 * @throws CommerceException - CommerceException
	 */
	public void copyShippingBPPRelationShip(TRUShippingGroupCommerceItemRelationship pSGCIRel)
	throws CommerceException {
		if (isLoggingDebug()) {
			logDebug("TRUCSROrderTools::@method::copyShippingBPPRelationShip() : BEGIN");
		}
		try {
			if (pSGCIRel.getBppItemInfo().getId() != null) {
				vlogDebug("pSGCIRel.getBppItemInfo().getId()----:"+pSGCIRel.getBppItemInfo().getId());
				MutableRepository orderRepository = (MutableRepository) getOrderRepository();
				MutableRepositoryItem bppItemInfoForAdd = orderRepository.createItem(getBppItemInfoDescriptorName());
				TRUBppItemInfo destBppItemInfo1 = new TRUBppItemInfo();
				destBppItemInfo1.setId(bppItemInfoForAdd.getRepositoryId());
				if (pSGCIRel.getBppItemInfo() instanceof ChangedProperties) {
					((ChangedProperties) destBppItemInfo1).setSaveAllProperties(Boolean.TRUE);
					((ChangedProperties) destBppItemInfo1).setRepositoryItem(bppItemInfoForAdd);
					vlogDebug("bppItemInfoForAdd-----: "+bppItemInfoForAdd);
					
				}
			}
		} catch (RepositoryException pRepositoryException) {
			if (isLoggingError()) {
				vlogError("RepositoryException occured in TRUCSROrderTools.copyShippingRelationShip()",pRepositoryException);
			}

			throw new CommerceException(pRepositoryException);
		}
		if (isLoggingDebug()) {
			logDebug("TRUCSROrderTools::@method::copyShippingBPPRelationShip() : exit");
		}
	}
}
