package com.tru.commerce.promotion;


import java.util.List;
import java.util.Map;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.pricing.GWPInfo;
import atg.commerce.pricing.PricingContext;
import atg.commerce.pricing.PricingException;
import atg.commerce.promotion.GWPMarkerManager;
import atg.commerce.promotion.GWPStatusHolder;
import atg.commerce.promotion.GiftWithPurchaseSelection;
import atg.core.util.ResourceUtils;
import atg.markers.MarkerException;
import atg.repository.RepositoryItem;

import com.tru.commerce.csr.util.TRUCSCConstants;

/**
 * This class is extended for TRU business extension.
 * 
 * @author PA
 * @version 1.0.
 */

public class TRUCSRGWPManager extends TRUGWPManager{
	
	/**
	 * This method is used to check whetehr the item qualifies for gift.
	 * @param pGWPInfo - GWPInfo
	 * @param pNewItemInfos -List
	 * @param pNewItems - List
	 * @param pPricingContext - PricingContext
	 * @param pStatusHolder - GWPStatusHolder
	 * @param pExtraParameters -Map
	 * @return restartPricing - boolean
	 * @throws PricingException - exception
	 */
	
	  @SuppressWarnings({ "null", "unchecked" })
	public boolean qualifyGift(GWPInfo pGWPInfo, List<AddCommerceItemInfo> pNewItemInfos, List<CommerceItem> pNewItems, PricingContext pPricingContext, GWPStatusHolder pStatusHolder, Map pExtraParameters)
			    throws PricingException
			  {
			    if (isLoggingDebug()) {
			      logDebug("Entered qualifyGift()");
			      logDebug(new StringBuilder().append("pGWPInfo:").append(pGWPInfo).toString());
			      logDebug(new StringBuilder().append("pNewItemInfos:").append(pNewItemInfos).toString());
			      logDebug(new StringBuilder().append("pNewItems:").append(pNewItems).toString());
			      logDebug(new StringBuilder().append("pPricingContext:").append(pPricingContext).toString());
			      logDebug(new StringBuilder().append("pStatusHolder:").append(pStatusHolder).toString());
			      logDebug(new StringBuilder().append("pExtraParameters:").append(pExtraParameters).toString());
			    }

			    boolean restartPricing = false;

			    if (pGWPInfo == null) {
			      if (isLoggingDebug()) {
			        logDebug("GWPInfo is null so leaving qualifyGift() with false.");
			      }
			      return false;
			    }

			    Map orderDetailsMap = pGWPInfo.getOrderDetailsMap();
			    if (orderDetailsMap == null) {
			      String msg = ResourceUtils.getMsgResource(TRUCSCConstants.GWP_ERROR_QUALIFY, TRUCSCConstants.ATG_PROMO_RESOURCE, sResourceBundle);

			      if (isLoggingError()) {
			        logError(msg);
			        if (pGWPInfo != null){ logError(pGWPInfo.toString());}
			      }
			      throw new PricingException(msg);
			    }

			    boolean autoRemove = true;
			    Boolean autoRemoveBool = (Boolean)orderDetailsMap.get(TRUCSCConstants.AUTO_REMOVE);
			    if (autoRemoveBool != null) {
			      autoRemove = autoRemoveBool.booleanValue();
			    }

			    String giftType = (String)orderDetailsMap.get(TRUCSCConstants.GIFT_TYPE);
			    if (giftType == null) {
			      String msg = ResourceUtils.getMsgResource(TRUCSCConstants.GWP_ERROR_QUALIFY,TRUCSCConstants.ATG_PROMO_RESOURCE, sResourceBundle);

			      if (isLoggingError()) {
			        logError(msg);
			        if (pGWPInfo != null){ logError(pGWPInfo.toString());}
			      }
			      throw new PricingException(ResourceUtils.getMsgResource(TRUCSCConstants.GWP_ERROR_QUALIFY,TRUCSCConstants.ATG_PROMO_RESOURCE, sResourceBundle));
			    }

			    String giftDetail = (String)orderDetailsMap.get(TRUCSCConstants.GIFT_DETAIL);
			    if (giftDetail == null) {
			      String msg = ResourceUtils.getMsgResource(TRUCSCConstants.GWP_ERROR_QUALIFY, TRUCSCConstants.ATG_PROMO_RESOURCE, sResourceBundle);

			      if (isLoggingError()) {
			        logError(msg);
			        if (pGWPInfo != null){
			        	logError(pGWPInfo.toString());
			        }
			      }
			      throw new PricingException(msg);
			    }

			    GWPMarkerManager manager = getGwpMarkerManager();

			    long targetedQuantity = pGWPInfo.getTargetedQuantity();
			    long quantity = pGWPInfo.getQuantity();

			    RepositoryItem orderMarker = pGWPInfo.getMarker();
			    if (orderMarker == null) {
			      try
			      {
			        pStatusHolder.addQualifiedPromotion(extractPromotionId(pGWPInfo.getPromotionId()));
			        orderMarker = manager.addOrderMarker(pPricingContext.getOrder(), pGWPInfo.getPromotionId(), pGWPInfo.getGiftHashCode(), quantity, autoRemove, giftType, giftDetail, targetedQuantity);

			      }
			      catch (MarkerException e)
			      {
			        String msg = ResourceUtils.getMsgResource(TRUCSCConstants.GWP_ERROR_QUALIFY, TRUCSCConstants.ATG_PROMO_RESOURCE, sResourceBundle);

			        if (isLoggingError()) {
			          logError(msg, e);
			        
			        }
			      }
			    }

			    if ((pExtraParameters != null) && (pExtraParameters.containsKey(TRUCSCConstants.DISABLE_GWP)))
			    {
			      if (isLoggingDebug()) {
			        logDebug("GWP auto add and remove is disabled so leaving qualifyGift() with false.");
			      }
			      return false;
			    }

			    GiftWithPurchaseSelection selection = manager.getOrderMarkerSelection(orderMarker);
			    long quantityAvailableToAutoAdd = selection.getQuantityAvailableForSelection();

			    if (quantityAvailableToAutoAdd <= TRUCSCConstants.ZERO_LONG)
			    {
			      if (isLoggingDebug()) {
			        logDebug("All available quantity already targeted so leaving qualifyGift() with false");
			      }
			      return false;
			    }
			    pExtraParameters.put(TRUCSCConstants.GWP_PROMO_ID, manager.getPromotionId(orderMarker));
			    pExtraParameters.put(TRUCSCConstants.PRICING_CONTEXT, pPricingContext);

			    processAutoAdd(orderMarker, quantityAvailableToAutoAdd, pNewItemInfos, pNewItems, pPricingContext, pExtraParameters);
			    
			    if(pNewItems!=null && !pNewItems.isEmpty()){
			    	restartPricing= true;
			    }
			    
			    if (isLoggingDebug()) {
			      logDebug(new StringBuilder().append("Leaving qualifyGift() with ").append(restartPricing).toString());
			    }
			    return restartPricing;
			  }
}
