package com.tru.common;

import java.util.List;

/**
 * The Class TRUCSRConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCSRConfiguration extends TRUConfiguration {
	
	/**
	 * hold boolean property enableTaxware.
	 */
	private boolean mEnableTaxware;
	
	/** The Copy ship item rel properties. */
	private List<String> mCopyShipItemRelProperties;

	/**
	 * @return the mEnableTaxware
	 */
	public boolean isEnableTaxware() {
		return mEnableTaxware;
	}

	/**
	 * @param pEnableTaxware the pEnableTaxware to set
	 */
	public void setEnableTaxware(boolean pEnableTaxware) {
		this.mEnableTaxware = pEnableTaxware;
	}
	
	/**
	 * Gets the copy ship item rel properties.
	 *
	 * @return the copy ship item rel properties.
	 */
	public List<String> getCopyShipItemRelProperties() {
		return mCopyShipItemRelProperties;
	}
	
	/**
	 * Sets the copy ship item rel properties.
	 *
	 * @param pCopyShipItemRelProperties the copyShipItemRelProperties to set.
	 */
	public void setCopyShipItemRelProperties(List<String> pCopyShipItemRelProperties) {
		mCopyShipItemRelProperties = pCopyShipItemRelProperties;
	}

}