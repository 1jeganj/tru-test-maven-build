package com.tru.logging;
/**
 * 
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSETAConstants {

	/**
	 * Constant to hold EVENTTYPE_LOGINITIALIZED.
	 */
	public static final String EVENTTYPE_LOGINITIALIZED = "SETALogInitialized";
	/**
	 * Constant to hold EVENTTYPE_LOGSTOPPED.
	 */
	public static final String EVENTTYPE_LOGSTOPPED = "SETALogStopped";
	/**
	 * Constant to hold EVENTTYPE_CUSTLOGINSUCCESS.
	 */
	public static final String EVENTTYPE_CUSTLOGINSUCCESS = "custLoginSuccess";
	/**
	 * Constant to hold EVENTTYPE_CUSTLOGINFAIL.
	 */
	public static final String EVENTTYPE_CUSTLOGINFAIL = "custLoginFailure";
	
	/**
	 * Constant to hold EVENTTYPE_CUSTUPDATECREDITCARD.
	 */
	public static final String EVENTTYPE_CUSTUPDATECREDITCARD="custUpdateCC";
	/**
	 * Constant to hold EVENTTYPE_CUSTADDCREDITCARD.
	 */
	public static final String EVENTTYPE_CUSTADDCREDITCARD="custAddCC";
	/**
	 * Constant to hold EVENTTYPE_CUSTUPDATEBILLINGADDRESS.
	 */
	public static final String EVENTTYPE_CUSTUPDATEBILLINGADDRESS="custUpdBA";
	/**
	 * Constant to hold EVENTTYPE_CUSTADDBILLINGADDRESS.
	 */
	public static final String EVENTTYPE_CUSTADDBILLINGADDRESS="custAddBA";
	
	/**
	 * Constant to hold EVENTTYPE_CUSTCREATEACCOUNT.
	 */
	public static final String EVENTTYPE_CUSTCREATEACCOUNT="custCreateAccount";
	
	/**
	 * Constant to hold USER.
	 */
	public static final String USER="suser";
	
	/**
	 * Constant to hold SRC.
	 */
	public static final String SRC="src";
	/**
	 * Constant to hold ADD_ID.
	 */
	public static final String ADD_ID="add_id";
	/**
	 * Constant to hold CC_ID.
	 */
	public static final String CC_ID="cc_id";
	/**
	 * Constant to hold EQUAL_SYMBOL.
	 */
	public static final String EQUAL_SYMBOL = "=";
	/**
	 * Constant to hold TRIPLE_HASH_SYMBOL.
	 */
	public static final String TRIPLE_HASH_SYMBOL = "---";
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String EMPTY_STRING= " ";
	
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String PATTERN_COMPILE= "\\$\\{(.+?)\\}";
	/**
	 * Constant to hold EMPTY_STRING.
	 */
	public static final String DATE_TIME= "dateTime";
	/**
	 * Constant to hold HOST.
	 */		
	public static final String HOST="host";
	/**
	 * Constant to hold CEFTAG.
	 */
	public static final String CEFTAG="cefTag";
	/**
	 * Constant to hold CEFVERSION.
	 */
	public static final String CEFVERSION="cefVersion";
	/**
	 * Constant to hold DEVICE_VENDOR.
	 */
	public static final String DEVICE_VENDOR="deviceVendor";
	/**
	 * Constant to hold DEVICE_PRODUCT.
	 */
	public static final String DEVICE_PRODUCT="deviceProduct";
	/**
	 * Constant to hold DEVICE_VERSION.
	 */
	public static final String DEVICE_VERSION="deviceVersion";
	/**
	 * Constant to hold EVENT_ID.
	 */
	public static final String EVENT_ID="eventId";
	/**
	 * Constant to hold EVENT_NAME.
	 */
	public static final String EVENT_NAME="eventName";
	/**
	 * Constant to hold SEVERITY.
	 */
	public static final String SEVERITY="severity";
	
	/**
	 * Constant to hold NULL.
	 */
	public static final String NULL="null";
	
	/**
	 * Constant to hold SETALOG.
	 */
	public static final String SETALOG="**** SETALog";
	/**
	 * Constant to hold NUMERIC_ONE.
	 */
	public static final int NUMERIC_ONE = 1;
	
	/** The Constant EVENTTYPE_CUSTREMOVECREDITCARD. */
	public static final String EVENTTYPE_CUSTREMOVECREDITCARD="custRemoveCC";
	
	/** The Constant EVENTTYPE_CUSTADDGIFTCARD. */
	public static final String EVENTTYPE_CUSTADDGIFTCARD="custAddGC";
	
	/** The Constant EVENTTYPE_CUSTREMOVEGIFTCARD. */
	public static final String EVENTTYPE_CUSTREMOVEGIFTCARD="custRemoveGC";
}
