package com.tru.logging;

import java.util.Calendar;
import java.util.Map;

import atg.nucleus.logging.LogEvent;
/**
 * 
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSETAEvent extends LogEvent {

	/**
	 * property to hold time stamp.
	 */
	private long mTimestamp;
	/**
	 * property to hold event Type.
	 */
	private String mEventType;
	/**
	 * property to hold event Name.
	 */
	private String mEventName;
	/**
	 * property to hold severity.
	 */
	private String mSeverity;
	/**
	 * property to hold extensionMap.
	 */
	private Map<String, String> mExtensionMap;
	/**
	 * Gets the Time stamp
	 * 
	 * @return the mTimestamp
	 */
	public long getTimeStamp(){
		return this.mTimestamp;
	}
	/**
	 * Gets the EventType
	 * 
	 * @return the mEventType
	 */
	public String getEventType(){
		return this.mEventType;
	}
	/**
	 * Gets the EventName
	 * 
	 * @return the mEventName
	 */
	public String getEventName(){
		return this.mEventName;
	}
	/**
	 * Gets the Severity
	 * 
	 * @return the mSeverity
	 */
	public String getSeverity(){
		return this.mSeverity;
	}
	/**
	 * Gets the ExtensionMap
	 * 
	 * @return the mExtensionMap
	 */
	public Map<String, String> getExtensionMap(){
		return this.mExtensionMap;
	}

	/**
	 * 
	 * @param pEventType EventType
	 * @param pEventName EventName
	 * @param pSeverity Severity
	 * @param pExtensionMap ExtensionMap
	 */
	public TRUSETAEvent(String pEventType,String pEventName,String pSeverity,Map<String,String> pExtensionMap) {
		super(pEventType); //Need to come back to this.
		mTimestamp = Calendar.getInstance().getTimeInMillis();
		this.mEventType = pEventType;
		this.mEventName = pEventName;
		this.mSeverity = pSeverity;
		this.mExtensionMap = pExtensionMap;
	}

	/**
	 * @return SETAlog
	 */
	public String getIdentifier()
	{
		return TRUSETAConstants.SETALOG;
	}

}
