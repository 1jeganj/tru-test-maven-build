package com.tru.logging;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.nucleus.logging.LogEvent;
import atg.nucleus.logging.RotatingFileLogger;
/**
 * Extends the RotatingFileLogger to write out TRUSETAEvent objects to the CEF Format
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSETARotatingFileLogger extends RotatingFileLogger {
	
	/*
	 * The Host name to use in the CEF Header
	 */
	private String mHostName;
	/*
	 * CEF tag, usually "CEF" in the header
	 */
	private String mCefTag;
	/*
	 * CEF Version, set to 0 from documents, but could be 1.
	 */
	private String mCefVersion;
	/*
	 * Date format for the headrer time stamp. Set to MMM d HH:mm:ss per the CEF standards
	 */
	private String mDateFormat;
	/*
	 * String defining the template of the CEF header.
	 */
	private String mCefHeaderFormat;
	/*
	 * Vendor string for the header
	 */
	private String mDeviceVendor;
	/*
	 * Product String for the Header
	 */
	private String mDeviceProduct;
	/*
	 * Version String for the header
	 */
	private String mDeviceVersion;
	/**
	 * Gets the deviceVendor.
	 * 
	 * @return the deviceVendor
	 */
	public String getDeviceVendor() {
		return mDeviceVendor;
	}
	/**
	 * Sets the deviceVendor.
	 * 
	 * @param pDeviceVendor the deviceVendor to set
	 */
    
	public void setDeviceVendor(String pDeviceVendor) {
		this.mDeviceVendor = pDeviceVendor;
	}

	/**
	 * Gets the deviceProduct.
	 * 
	 * @return the deviceProduct
	 */
	public String getDeviceProduct() {
		return mDeviceProduct;
	}
	/**
	 * Sets the deviceProduct.
	 * 
	 * @param pDeviceProduct the deviceProduct to set
	 */

	public void setDeviceProduct(String pDeviceProduct) {
		this.mDeviceProduct = pDeviceProduct;
	}

	/**
	 * Gets the deviceVersion.
	 * 
	 * @return the deviceVersion
	 */
	public String getDeviceVersion() {
		return mDeviceVersion;
	}

	/**
	 * Sets the deviceVersion.
	 * 
	 * @param pDeviceVersion the deviceVersion to set
	 */
	public void setDeviceVersion(String pDeviceVersion) {
		this.mDeviceVersion = pDeviceVersion;
	}
    
	/**
	 * Gets the cefHeaderFormat.
	 * 
	 * @return the cefHeaderFormat
	 */
	public String getCefHeaderFormat() {
		return mCefHeaderFormat;
	}
	/**
	 * Sets the cefHeaderFormat.
	 * 
	 * @param pCefHeaderFormat the cefHeaderFormat to set
	 */
    
	public void setCefHeaderFormat(String pCefHeaderFormat) {
		this.mCefHeaderFormat = pCefHeaderFormat;
		
	}


	private SimpleDateFormat mDateFormatter;
	/**
	 * Gets the dateFormat.
	 * 
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return mDateFormat;
	}

	/**
	 * Sets the dateFormat.
	 * 
	 * @param pDateFormat the dateFormat to set
	 */
	public void setDateFormat(String pDateFormat) {
		this.mDateFormat = pDateFormat;
		this.mDateFormatter = new SimpleDateFormat(pDateFormat);
	}
	
	
	/**
	 * Gets the cefTag.
	 * 
	 * @return the cefTag
	 */

	public String getCefTag() {
		return mCefTag;
	}

	/**
	 * Sets the cefTag.
	 * 
	 * @param pCefTag the cefTag to set
	 */
	public void setCefTag(String pCefTag) {
		this.mCefTag = pCefTag;
	}
	/**
	 * Gets the cefVersion.
	 * 
	 * @return the cefVersion
	 */
	public String getCefVersion() {
		return mCefVersion;
	}
	/**
	 * Sets the cefVersion.
	 * 
	 * @param pCefVersion the cefVersion to set
	 */

	public void setCefVersion(String pCefVersion) {
		this.mCefVersion = pCefVersion;
	}
	/**
	 * Gets the hostName.
	 * 
	 * @return the hostName
	 */  

	public String getHostName() {
		return mHostName;
	}

	/**
	 * Sets the hostName.
	 * 
	 * @param pHostName the hostName to set
	 */
	public void setHostName(String pHostName) {
		this.mHostName = pHostName;
	}
	/**
	 * @param pLogEvent :LogEvent
	 */
	@Override
	public synchronized void writeLogEvent(LogEvent pLogEvent) {
		TRUSETAEvent event = null;
		
		if(!(pLogEvent instanceof TRUSETAEvent)){
			if(isLoggingError()){
				logError("TRU SETALogger was passed a non-seta event.");
			}
			return;
		}else{
			event = (TRUSETAEvent)pLogEvent;
		}
	      
		String cefHeader = generateCEFHeader(event);
		String cefExtenstions = generateCEFExtenstions(event);
		getLogStream().print(cefHeader+cefExtenstions);
		getLogStream().println();

	}
	
	/**
	 * 
	 * @param pLogEvent :SETAEvent
	 * @return buffer the String
	 */
	private String generateCEFExtenstions(TRUSETAEvent pLogEvent){

		StringBuffer buffer = new StringBuffer();
		for(String key : pLogEvent.getExtensionMap().keySet()){
			buffer.append(TRUSETAConstants.EMPTY_STRING);
			buffer.append(key);
			buffer.append(TRUSETAConstants.EQUAL_SYMBOL);
			Object v = pLogEvent.getExtensionMap().get(key);
			String vs = TRUSETAConstants.TRIPLE_HASH_SYMBOL;
			if(v!=null){
				vs=v.toString();
			}
			buffer.append(vs);
		}
		return buffer.toString();
		
	}
	/**
	 * 
	 * @param pLogEvent :SETAEvent
	 * @return buffer the String
	 */
	private String generateCEFHeader(TRUSETAEvent pLogEvent){
		Pattern cefHeaderPattern=Pattern.compile(TRUSETAConstants.PATTERN_COMPILE);
		Matcher cefHeaderMatcher = cefHeaderPattern.matcher(mCefHeaderFormat);

		StringBuffer buffer = new StringBuffer();
		HashMap<String,String> templateValues = new HashMap<String, String>();
		templateValues.put(TRUSETAConstants.DATE_TIME, mDateFormatter.format(new Date(pLogEvent.getTimeStamp())));
		templateValues.put(TRUSETAConstants.HOST, getHostName());
		templateValues.put(TRUSETAConstants.CEFTAG, getCefTag());
		templateValues.put(TRUSETAConstants.CEFVERSION,getCefVersion());
		templateValues.put(TRUSETAConstants.DEVICE_VENDOR, getDeviceVendor());
		templateValues.put(TRUSETAConstants.DEVICE_PRODUCT, getDeviceProduct());
		templateValues.put(TRUSETAConstants.DEVICE_VERSION, getDeviceVersion());
		templateValues.put(TRUSETAConstants.EVENT_ID, pLogEvent.getEventType());
		templateValues.put(TRUSETAConstants.EVENT_NAME, pLogEvent.getEventName());
		templateValues.put(TRUSETAConstants.SEVERITY,pLogEvent.getSeverity());
		
		while(cefHeaderMatcher.find()){
			if(templateValues.containsKey(cefHeaderMatcher.group(TRUSETAConstants.NUMERIC_ONE))){
				String sub = templateValues.get(cefHeaderMatcher.group(TRUSETAConstants.NUMERIC_ONE));
				cefHeaderMatcher.appendReplacement(buffer, sub!=null ? Matcher.quoteReplacement(sub) : TRUSETAConstants.NULL);
			}
		}
		cefHeaderMatcher.appendTail(buffer);
		return buffer.toString();
	}
	
}
