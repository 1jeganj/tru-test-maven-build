package com.tru.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

/**
 * This class is used to log CEF logging.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSETALogger extends GenericService{

	/** Constant for SPLIT_SYMBOL_COMA. */
	public static final String SPLIT_SYMBOL_COMA = ",";
	
	/** The Constant for string DEFAULT_IP . */
	public static final String DEFAULT_IP = "208.247.73.130";
	
	/** The Constant for string HEADER_PARAM_IPADDRESS . */
	public static final String HEADER_PARAM_IPADDRESS ="X-FORWARDED-FOR";

	
	/** Flag if logging info messages */
	  private boolean mLoggingSETA = true;
	
	/**
	 * Property to hold nameMap
	 */
	private Properties mSeverityMap;
	
	/**
	 * Property to hold nameMap
	 */
	private Properties mNameMap;
	
	/**
	 * @return the nameMap
	 */
	public Properties getNameMap() {
		return mNameMap;
	}
	
	/**
	 * @param pNameMap the nameMap to set
	 */
	public void setNameMap(Properties pNameMap) {
		mNameMap = pNameMap;
	}

	/**
	 * @return the severityMap
	 */
	public Properties getSeverityMap() {
		return mSeverityMap;
	}

	/**
	 * Sets the severityMap
	 * 
	 * @param pSeverityMap the severityMap to set
	 */
	public void setSeverityMap(Properties pSeverityMap) {
		mSeverityMap = pSeverityMap;
	}

	@Override
	public void doStartService() throws ServiceException {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/doStartService");
		}
		//logLogInitalizedEvent();
		super.doStartService();
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/doStartService");
		}
	}
	
	@Override
	public void doStopService() throws ServiceException {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/doStopService");
		}
		//logLogStoppedEvent();
		super.doStopService();
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/doStopService");
		}
	}
	
	/**
	 * Log initialized event.
	 */
	public void logLogInitalizedEvent(){
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logLogInitalizedEvent");
		}
		TRUSETAEvent event = new TRUSETAEvent(TRUSETAConstants.EVENTTYPE_LOGINITIALIZED,getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_LOGINITIALIZED),getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_LOGINITIALIZED),new HashMap());
		sendLogEvent(event);
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logLogInitalizedEvent");
		}
	}
	
	/**
	 * Log a stopped event.
	 */
	public void logLogStoppedEvent(){
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logLogStoppedEvent");
		}
		TRUSETAEvent event = new TRUSETAEvent(TRUSETAConstants.EVENTTYPE_LOGSTOPPED, getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_LOGSTOPPED),getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_LOGSTOPPED),new HashMap());
		sendLogEvent(event);
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logLogStoppedEvent");
		}
	}
	
	
	/**
	 * Log a successful customer login.
	 * @param pUserId The profile id of the logged in user.
	 */
	public void logLoginSuccess(String pUserId){
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logLoginSuccess");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTLOGINSUCCESS;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTLOGINSUCCESS);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTLOGINSUCCESS);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logLoginSuccess");
		}
	}
	
	/**
	 * Log a failed customer login.
	 * @param pUserId The login name of the user that tried to login
	 */
	public void logLoginFailure(String pUserId){
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logLoginFailure");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTLOGINFAIL;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTLOGINFAIL);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTLOGINFAIL);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logLoginFailure");
		}
	}
	
	/**
	 * Log a customer account created.
	 * @param pUserId  The profile id of the created user
	 */
	public void logCreateAccount(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logCreateAccount");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTCREATEACCOUNT;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTCREATEACCOUNT);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTCREATEACCOUNT);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logCreateAccount");
		}
	}
	
	/**
	 * Log a customer credit card .
	 * @param pUserId  The profile id of the created user
	 */
	public void logAddCreditCard(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logAddCreditCard");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTADDCREDITCARD;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDCREDITCARD);
			//extenstions.put(TRUSETAConstants.CC_ID, pCreditCardId);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDCREDITCARD);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logAddCreditCard");
		}
	}

	/**
	 * Log a customer credit card .
	 * @param pUserId  The profile id of the created user
	 */
	public void logUpdateCreditCard(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logUpdateCreditCard");
		}
		
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTUPDATECREDITCARD;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTUPDATECREDITCARD);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTUPDATECREDITCARD);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logUpdateCreditCard");
		}

	}

	
	
	/**
	 * Log a customer Billing Address updated .
	 * @param pUserId  The profile id of the created user
	 * @param pContactInfoId - Billing Address Info repository id
	 */
	public void logUpdateBillingAddress(String pUserId, String pContactInfoId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logUpdateBillingAddress");
		}
		
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTUPDATEBILLINGADDRESS;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTUPDATEBILLINGADDRESS);
			extenstions.put(TRUSETAConstants.ADD_ID, pContactInfoId);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTUPDATEBILLINGADDRESS);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logUpdateBillingAddress");
		}
	}
	
	
	/**
	 * Log a customer Billing Address .
	 * @param pUserId  The profile id of the created user
	 * @param pContactInfoId - Billing Address Info repository id
	 */
	public void logAddBillingAddress(String pUserId, String pContactInfoId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logAddBillingAddress");
		}
		
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTADDBILLINGADDRESS;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDBILLINGADDRESS);
			extenstions.put(TRUSETAConstants.ADD_ID, pContactInfoId);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDBILLINGADDRESS);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logAddBillingAddress");
		}
		
	}
	
	
	
	/**
	 * 
	 * @param pUserId userdid
	 * @param pEventType eventtype
	 * @param pEventName eventname
	 * @param pSeverity severity
	 * @param pExtenstions extensions
	 */
	private void logSETAEvent(String pUserId, String pEventType, String pEventName, String pSeverity, Map<String,String> pExtenstions   ) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logSETAEvent");
		}
		pExtenstions.put(TRUSETAConstants.USER, pUserId);
		pExtenstions.put(TRUSETAConstants.SRC, getTrueClientIpAddress(ServletUtil.getCurrentRequest()));
		TRUSETAEvent event = new TRUSETAEvent(pEventType,pEventName,pSeverity,pExtenstions);
		sendLogEvent(event);
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logSETAEvent");
		}
	}
	
	
	
	/**
	 * This method gets mLoggingSETA value
	 *
	 * @return the mLoggingSETA value
	 */
	public boolean isLoggingSETA() {
		return mLoggingSETA;
	}
	
	/**
	 * This method sets the mLoggingSETA with mLoggingSETA value
	 *
	 * @param pLoggingSETA the pLoggingSETA to set
	 */
	public void setLoggingSETA(boolean pLoggingSETA) {
		this.mLoggingSETA = pLoggingSETA;
	}
	
	
	/**
	 * Gets the request client ip address.
	 *
	 * @param pRequest the request
	 * @return the request client ip address
	 */
	public String getTrueClientIpAddress(DynamoHttpServletRequest pRequest) {
		
		if (isLoggingDebug()) {
			logDebug("START:: TRUSETALogger.getRequestClientIpAddress method..");
		}
		String clientIp = pRequest.getHeader(HEADER_PARAM_IPADDRESS);
		if (isLoggingDebug()) {
			vlogDebug("Request.getHeader():: {0} ::{1}", HEADER_PARAM_IPADDRESS, clientIp);
		}
		if(StringUtils.isNotBlank(clientIp)) {
			StringTokenizer tokenizer = new StringTokenizer(clientIp, SPLIT_SYMBOL_COMA);
			while (tokenizer.hasMoreTokens()) {
				clientIp = tokenizer.nextToken().trim();
				break;
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(" True Client IP :{0}", clientIp);
		}
		if (StringUtils.isBlank(clientIp)) {
			if (isLoggingDebug()) {
				vlogDebug(
						"The Client ip :{0} Header is null so setting Remote Addr as client IP",clientIp);
			}
			clientIp = pRequest.getRemoteAddr();
			if (isLoggingDebug()) {
				vlogDebug("True Client IP after setting RemoteAddress:{0}",clientIp);
			}
		}

		if (StringUtils.isBlank(clientIp)) {
			clientIp = pRequest.getLocalAddr();
			if (isLoggingDebug()) {
				vlogDebug("Request.getLocalAddr():{0}", clientIp);
			}
		}
		
		if (StringUtils.isBlank(clientIp)) {
			clientIp = DEFAULT_IP;
		}
		
		if (isLoggingDebug()) {
			vlogDebug(" True Client IP :{0}", clientIp);
			logDebug("END:: TRUSETALogger.getRequestClientIpAddress method..");
		}

		return clientIp;
	}

	
	/**
	 * Log remove credit card.
	 *
	 * @param pUserId the user id
	 */
	public void logRemoveCreditCard(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logRemoveCreditCard");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTREMOVECREDITCARD;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTREMOVECREDITCARD);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTREMOVECREDITCARD);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logRemoveCreditCard");
		}
	}
	
	/**
	 * Log add gift card.
	 *
	 * @param pUserId the user id
	 */
	public void logAddGiftCard(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logAddGiftCard");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTADDGIFTCARD;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDGIFTCARD);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTADDGIFTCARD);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logAddGiftCard");
		}
	}
	
	/**
	 * Log remove gift card.
	 *
	 * @param pUserId the user id
	 */
	public void logRemoveGiftCard(String pUserId) {
		if(isLoggingDebug()){
			logDebug("Start :: TRUSETALogger/logRemoveGiftCard");
		}
		if (isLoggingSETA()) {
			HashMap<String,String> extenstions = new HashMap<String,String>();
			String eventtype = TRUSETAConstants.EVENTTYPE_CUSTREMOVEGIFTCARD;
			String eventName = getNameMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTREMOVEGIFTCARD);
			String serverity = getSeverityMap().getProperty(TRUSETAConstants.EVENTTYPE_CUSTREMOVEGIFTCARD);
			logSETAEvent(pUserId, eventtype, eventName,serverity,extenstions);
		}
		if(isLoggingDebug()){
			logDebug("End :: TRUSETALogger/logRemoveGiftCard");
		}
	}
}
