-- Start - Added for Enable charity donation - 08st Dec 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ENABLE_CHARITY_DONATION NUMBER(1) DEFAULT 1;

-- End - Added for Enable charity donation - 08st Dec 2015

-- Start - Added for Enable ABANDON CART - 08st Dec 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD ABANDON_CART_THRESHOLD NUMBER(1) DEFAULT 1;

-- End - Added for Enable ABANDON CART - 08st Dec 2015