drop table tru_size;

drop table tru_color;

CREATE TABLE tru_color (
	color_id 		varchar2(254)	NOT NULL,
	color_name 		varchar2(254)	NULL,
	color_Display_Sequence 	varchar2(254)	NULL,
	PRIMARY KEY(color_id)
);

CREATE TABLE tru_size (
	size_id 		varchar2(254)	NOT NULL,
	size_Description 	varchar2(254)	NULL,
	size_Display_Sequence 	varchar2(254)	NULL,
	PRIMARY KEY(size_id)
);
alter table tru_sku	add INLINE_FILES VARCHAR2(500)	NULL;
commit;