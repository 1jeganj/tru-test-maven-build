-- Start : 11-12-15 added for paypal payment group

ALTER TABLE tru_paypal_pament_group 
	ADD (	payer_first_name varchar2(254) NULL,
			payer_middle_name varchar2(254) NULL,
			payer_last_name varchar2(254) NULL,
			payer_phone_number varchar2(254) NULL,
			payer_email varchar2(254) NULL
		);

ALTER TABLE tru_payPal_payment_status
	ADD (	error_code varchar2(254) NULL,
			payment_pending_reason varchar2(254) NULL,
			reason_code varchar2(254) NULL
		);

-- End : 11-12-15 added for paypal payment group