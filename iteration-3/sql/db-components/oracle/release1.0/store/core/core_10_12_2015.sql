drop table tru_giftfinder;

drop table tru_giftFinderStackItems;

drop table tru_giftFinderStackItemList;


GRANT SELECT,INSERT,DELETE,UPDATE,INDEX,REFERENCES ON tru_giftFinderStackItemList TO TRUPUB_QC;
GRANT SELECT,INSERT,DELETE,UPDATE,INDEX,REFERENCES ON tru_giftFinderStackItemList TO TRUCATB_QC;
GRANT SELECT,INSERT,DELETE,UPDATE,INDEX,REFERENCES ON tru_giftFinderStackItemList TO TRUCATA_QC;
GRANT SELECT,INSERT,DELETE,UPDATE,INDEX,REFERENCES ON tru_giftFinderStackItemList TO TRUCATA_QC_STAGING;
GRANT SELECT,INSERT,DELETE,UPDATE,INDEX,REFERENCES ON tru_giftFinderStackItemList TO TRUCATB_QC_STAGING;

CREATE TABLE tru_giftFinderStackItemList (
id varchar2(254)    NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE tru_giftfinder (
id         varchar2(254)    NOT NULL,
name       varchar2(254)    NULL,
type       INTEGER          NULL,
nvalue     varchar2(254)    NULL,
imageURL   varchar2(254)    NULL,
PRIMARY KEY(id)
);

CREATE TABLE tru_giftFinderStackItems (
id  varchar2(254)    NOT NULL REFERENCES tru_giftFinderStackItemList(id),
sequence_num         INTEGER    NOT NULL,
gift_finder_stack_item      varchar2(254)    NULL REFERENCES tru_giftfinder(id),
PRIMARY KEY(id, sequence_num)
);

CREATE INDEX tru_giftFinderStackItems_idx ON tru_giftFinderStackItems(id, sequence_num);
