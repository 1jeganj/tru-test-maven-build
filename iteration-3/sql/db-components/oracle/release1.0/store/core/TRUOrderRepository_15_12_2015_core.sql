-- drop table tru_gift_card;
-- drop table tru_pay_status;

CREATE TABLE tru_gift_card (
          payment_group_id         varchar2(254)      NOT NULL REFERENCES dcspp_pay_group(payment_group_id),
          card_holder_name         varchar2(254)      NULL,
          gift_card_number           varchar2(254)      NULL,
          gift_card_pin                  varchar2(254)      NULL,
          amount_applied             number(28, 20)    NULL,
          PRIMARY KEY(payment_group_id)
);

CREATE INDEX tru_gift_card_idx ON tru_gift_card(payment_group_id);

CREATE TABLE tru_pay_status (
          status_id              varchar2(254)      NOT NULL REFERENCES dcspp_pay_status(status_id),
          bank_resp_code            varchar2(254)      NULL,
          bank_message              varchar2(254)      NULL,
          correlationId                   varchar2(254)      NULL,
          currency               varchar2(254)      NULL,
          gatewayResponseCode          varchar2(254)      NULL,
          gatewayMessage                    varchar2(254)      NULL,
          method                           varchar2(254)      NULL,
          transactionTag              varchar2(254)      NULL,
          transactionType   varchar2(254)      NULL,
          validationStatus   varchar2(254)      NULL,
          validationType               varchar2(254)      NULL,
          action_code                   varchar2(254)      NULL,
          error_code           varchar2(254)      NULL,
          fraud_transaction_id      varchar2(254)      NULL,
          rules_tipped                   varchar2(254)      NULL,
          recommendation_code varchar2(254)      NULL,
          PRIMARY KEY(status_id)
);

CREATE INDEX tru_pay_status_idx ON tru_pay_status(status_id);
