drop table tru_gift_card;

CREATE TABLE tru_gift_card (
	payment_group_id 	varchar2(254)	NOT NULL REFERENCES dcspp_pay_group(payment_group_id),
	gift_card_number 	varchar2(254)	NULL,
	card_holder_name 	varchar2(254)	NULL,
	gift_card_pin 		varchar2(254)	NULL,
	current_balance 	number(28, 20)	NULL,
	remaining_balance 	number(28, 20)	NULL,
	PRIMARY KEY(payment_group_id)
);

CREATE INDEX tru_gift_card_idx ON tru_gift_card(payment_group_id);

delete from tru_bpp_item_rel;

delete from tru_bpp_item;

alter table tru_bpp_item MODIFY (bpp_price number(19,7));