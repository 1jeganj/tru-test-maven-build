-- START changes on 14/12/2015, added RUS Card special finacing terms related columns --
ALTER TABLE TRU_ORDER ADD FINANCE_AGREED NUMBER(1) DEFAULT 0 NOT NULL CHECK(FINANCE_AGREED IN(0,1));
ALTER TABLE TRU_ORDER ADD FINANCING_TERMS_AVAILED NUMBER(2)	DEFAULT 0;
-- END changes on 14/12/2015, added RUS Card special finacing terms related columns --