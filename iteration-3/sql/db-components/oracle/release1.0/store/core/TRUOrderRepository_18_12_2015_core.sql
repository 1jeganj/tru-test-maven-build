-- drop table tru_bpp_item;
-- drop table tru_bpp_item_rel;

CREATE TABLE tru_bpp_item (
	bpp_item_info_id 	varchar2(254)	NOT NULL,
	bpp_item_id 		varchar2(254)	NULL,
	bpp_sku_id 		varchar2(254)	NULL,
	bpp_price 		number(28, 20)	NULL,
	PRIMARY KEY(bpp_item_info_id)
);

CREATE TABLE tru_bpp_item_rel (
	relationship_id 	varchar2(254)	NOT NULL REFERENCES dcspp_relationship(relationship_id),
	bpp_item_info_id 	varchar2(254)	NULL REFERENCES tru_bpp_item(bpp_item_info_id),
	PRIMARY KEY(relationship_id)
);

CREATE INDEX tru_bpp_item_rel_relationship_idx ON tru_bpp_item_rel(relationship_id);

alter table tru_shipitem_rel add estimate_ship_window_min INTEGER null;
alter table tru_shipitem_rel add estimate_ship_window_max INTEGER null;