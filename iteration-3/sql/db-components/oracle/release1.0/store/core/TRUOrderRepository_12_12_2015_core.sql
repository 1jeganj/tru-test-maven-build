-- Start : 12-12-15 added for paypal payment group

ALTER TABLE tru_paypal_pament_group RENAME TO tru_paypal_payment_group;
ALTER TABLE tru_paypal_payment_status DROP COLUMN currency;
ALTER TABLE tru_paypal_payment_status DROP COLUMN method;

-- End : 12-12-15 added for paypal payment group