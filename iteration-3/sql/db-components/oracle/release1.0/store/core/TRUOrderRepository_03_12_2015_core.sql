-- drop table tru_paypal_bill_address;

CREATE TABLE tru_paypal_bill_address (
	payment_group_id 	varchar2(254)	NOT NULL REFERENCES dcspp_pay_group(payment_group_id),
	prefix 			varchar2(254)	NULL,
	first_name 		varchar2(254)	NULL,
	middle_name 		varchar2(254)	NULL,
	last_name 		varchar2(254)	NULL,
	suffix 			varchar2(254)	NULL,
	job_title 		varchar2(254)	NULL,
	company_name 		varchar2(254)	NULL,
	address_1 		varchar2(254)	NULL,
	address_2 		varchar2(254)	NULL,
	address_3 		varchar2(254)	NULL,
	city 			varchar2(254)	NULL,
	county 			varchar2(254)	NULL,
	state 			varchar2(254)	NULL,
	postal_code 		varchar2(254)	NULL,
	country 		varchar2(254)	NULL,
	phone_number 		varchar2(254)	NULL,
	fax_number 		varchar2(254)	NULL,
	email 			varchar2(254)	NULL,
	PRIMARY KEY(payment_group_id)
);

CREATE INDEX tru_paypal_bill_address_payment_group_idx ON tru_paypal_bill_address(payment_group_id);