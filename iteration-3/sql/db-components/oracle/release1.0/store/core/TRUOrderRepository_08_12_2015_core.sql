-- Start : 08-12-15 added for donation Item

CREATE TABLE TRU_DONATION_COMMERCE_ITEM (
    commerce_item_id        varchar2(254)    NOT NULL REFERENCES dcspp_item(commerce_item_id),
    donation_amount           number(19,7) NULL,
    PRIMARY KEY(commerce_item_id)
);

-- End : 08-12-15 added for donation Item