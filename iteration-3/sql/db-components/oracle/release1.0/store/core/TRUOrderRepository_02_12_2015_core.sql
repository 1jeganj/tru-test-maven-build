-- drop table tru_paypal_pament_group;
-- drop table tru_payPal_payment_status;

CREATE TABLE tru_paypal_pament_group (
	payment_group_id 	varchar2(254)	NOT NULL REFERENCES dcspp_pay_group(payment_group_id),
	payer_id 		varchar2(254)	NULL,
	token_id 		varchar2(254)	NULL,
	paypal_correlation_id 	varchar2(254)	NULL,
	paypal_transaction_id 	varchar2(254)	NULL,
	transaction_timestamp 	DATE	NULL,
	msg_sub_id 		varchar2(254)	NULL,
	payer_status 		varchar2(254)	NULL,
	checkout_status 	varchar2(254)	NULL,
	PRIMARY KEY(payment_group_id)
);

CREATE INDEX tru_paypal_pament_group_payment_group_idx ON tru_paypal_pament_group(payment_group_id);

CREATE TABLE tru_payPal_payment_status (
	status_id 		varchar2(254)	NOT NULL REFERENCES dcspp_pay_status(status_id),
	correlation_id 		varchar2(254)	NULL,
	payment_status 		varchar2(254)	NULL,
	currency 		varchar2(254)	NULL,
	method 			varchar2(254)	NULL,
	PRIMARY KEY(status_id)
);

CREATE INDEX tru_payPal_payment_status_status_idx ON tru_payPal_payment_status(status_id);