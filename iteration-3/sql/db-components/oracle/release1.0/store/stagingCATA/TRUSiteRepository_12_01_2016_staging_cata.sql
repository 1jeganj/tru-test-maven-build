-- Start - persist order days for persisting the transient profile order - 12th Jan 2016
ALTER TABLE TRU_SITE_CONFIGURATION ADD persist_order_days INTEGER DEFAULT 2592000;
-- End  - persist order days for persisting the transient profile order - 12th Jan 2016

