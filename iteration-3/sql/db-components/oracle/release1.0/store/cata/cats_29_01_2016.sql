drop TABLE tru_sku_online_collec_name;

CREATE TABLE tru_sku_online_collec_name (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	collection_id 		varchar2(255)	NOT NULL,
	collection_name 	varchar2(255)	NULL,
	PRIMARY KEY(sku_id, collection_id)
);