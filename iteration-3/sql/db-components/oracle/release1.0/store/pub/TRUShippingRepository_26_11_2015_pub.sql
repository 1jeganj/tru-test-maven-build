-- drop table TRU_SHIP_HOLIDAY_INFO;

-- drop table TRU_SHIP_METHOD_NOT_ALLOWED;

-- drop table TRU_SHIP_METHOD_REGIONS;

-- drop table TRU_ELIGIBLE_FREIGHT_CLASSES;

-- drop table TRU_SHIP_METHOD;

-- drop table TRU_SHIP_REGIONS;

-- drop table TRU_ZIPCODE_RESTRICTIONS;

CREATE TABLE TRU_ZIPCODE_RESTRICTIONS (
	ASSET_VERSION	number(19)	not null,
	WORKSPACE_ID	varchar2(40)	not null,
	BRANCH_ID	varchar2(40)	not null,
	IS_HEAD	number(1,0)	not null,
	VERSION_DELETED	number(1,0)	not null,
	VERSION_EDITABLE number(1,0)	not null,
	PRED_VERSION	number(19,0)	,
	CHECKIN_DATE timestamp (6), 
	ID 			varchar2(254)	NOT NULL,
	SKU_ID 			varchar2(254)	NOT NULL,
	ZIP_CODE 		varchar2(254)	NOT NULL,
	ERROR_MESSAGE 		CLOB	NOT NULL,
	PRIMARY KEY(ID,ASSET_VERSION)
);

CREATE TABLE TRU_SHIP_REGIONS (
	ASSET_VERSION	number(19)	not null,
	WORKSPACE_ID	varchar2(40)	not null,
	BRANCH_ID	varchar2(40)	not null,
	IS_HEAD	number(1,0)	not null,
	VERSION_DELETED	number(1,0)	not null,
	VERSION_EDITABLE number(1,0)	not null,
	PRED_VERSION	number(19,0)	,
	CHECKIN_DATE timestamp (6), 
	
	SHIP_REGION_ID 		varchar2(254)	NOT NULL,
	SHIP_REGION_NAME 	INTEGER	NULL,
	ORDER_CUT_OFF_PRICE 	number(28, 20)	NULL,
	SHIP_MIN_PRICE 		number(28, 20)	NULL,
	SHIP_MAX_PRICE 		number(28, 20)	NULL,
	DELIVERY_TIME_MIN_DAYS 	INTEGER	NULL,
	DELIVERY_TIME_MAX_DAYS 	INTEGER	NULL,
	PRIMARY KEY(SHIP_REGION_ID,ASSET_VERSION)
);

CREATE TABLE TRU_SHIP_METHOD (
	ASSET_VERSION	number(19)	not null,
	WORKSPACE_ID	varchar2(40)	not null,
	BRANCH_ID	varchar2(40)	not null,
	IS_HEAD	number(1,0)	not null,
	VERSION_DELETED	number(1,0)	not null,
	VERSION_EDITABLE number(1,0)	not null,
	PRED_VERSION	number(19,0)	,
	CHECKIN_DATE timestamp (6), 
	SHIP_METHOD_ID 		varchar2(254)	NOT NULL,
	SHIP_METHOD_NAME 	varchar2(254)	NOT NULL ,
	SHIP_METHOD_CODE 	varchar2(254)	NOT NULL ,
	SHIP_METH_DETAILS 	CLOB	NULL,
	PRIMARY KEY(SHIP_METHOD_ID,ASSET_VERSION)
);

CREATE TABLE TRU_ELIGIBLE_FREIGHT_CLASSES (
	ASSET_VERSION	number(19)	not null,
	SHIP_METHOD_ID 		varchar2(254)	NOT NULL,
	FREIGHT_CLASS 		varchar2(254)	NOT NULL,
	PRIMARY KEY(SHIP_METHOD_ID, FREIGHT_CLASS,ASSET_VERSION)
);


CREATE TABLE TRU_SHIP_METHOD_REGIONS (
	ASSET_VERSION	number(19)	not null,
	SHIP_METHOD_ID 		varchar2(254)	NOT NULL ,
	SHIP_REGION_ID 		varchar2(254)	NOT NULL ,
	PRIMARY KEY(SHIP_METHOD_ID, SHIP_REGION_ID,ASSET_VERSION)
);


CREATE TABLE TRU_SHIP_METHOD_NOT_ALLOWED (
	ASSET_VERSION	number(19)	not null,
	SHIP_METHOD_ID 		varchar2(254)	,
	STATE_CODE 		varchar2(254)	NOT NULL,
	PRIMARY KEY(SHIP_METHOD_ID, STATE_CODE,ASSET_VERSION)
);


CREATE TABLE TRU_SHIP_HOLIDAY_INFO (
	ASSET_VERSION	number(19)	not null,	
	WORKSPACE_ID	varchar2(40)	not null,
	BRANCH_ID	varchar2(40)	not null,
	IS_HEAD	number(1,0)	not null,
	VERSION_DELETED	number(1,0)	not null,
	VERSION_EDITABLE number(1,0)	not null,
	PRED_VERSION	number(19,0)	,
	CHECKIN_DATE timestamp (6), 
	
	ID 			varchar2(254)	NOT NULL,
	HOLIDAY_NAME 		varchar2(254)	NULL,
	HOLIDAY_DATE 		date	NULL,
	IS_ACTIVE 		number(1)	NULL,
	CHECK (IS_ACTIVE IN (0, 1)),
	PRIMARY KEY(ID,ASSET_VERSION)
);

