drop TABLE tru_sku_online_collec_name;

CREATE TABLE tru_sku_online_collec_name (
	sku_id 			varchar2(40)	NOT NULL,
	asset_version 		INTEGER	NOT NULL,
	collection_id 		varchar2(255)	NOT NULL,
	collection_name 	varchar2(255)	NULL,
	PRIMARY KEY(sku_id, asset_version, collection_id)
);