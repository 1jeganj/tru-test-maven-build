-- Start - Added Reminder Count for Abandon Cart Messaging - 21st Dec 2015 

ALTER TABLE TRU_SITE_CONFIGURATION ADD reminder_count NUMBER(1) DEFAULT 1;

-- End - Added Reminder Count for Abandon Cart Messaging - 21st Dec 2015 