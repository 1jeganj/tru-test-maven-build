-- Start - Added display finance promo banner boolean flag for showing or hiding the banner- 30th Dec 2015
ALTER TABLE TRU_SITE_CONFIGURATION ADD DISP_FINANCE_PROMO_BANNER NUMBER(1) DEFAULT 1 NOT NULL CHECK(DISP_FINANCE_PROMO_BANNER IN(0,1));
-- End - Added display finance promo banner boolean flag for showing or hiding the banner- 30th Dec 2015

-- Start - Added column to hold threshold amount for availing six month special fianancing- 30th Dec 2015 
ALTER TABLE TRU_SITE_CONFIGURATION ADD THRESH_SIX_MONTH NUMBER(28, 20) NULL;
-- End - Added column to hold threshold amount for availing six month special fianancing- 30th Dec 2015

-- Start - Added column to hold threshold amount for availing twelve month special fianancing- 30th Dec 2015 
ALTER TABLE TRU_SITE_CONFIGURATION ADD THRESH_TWELVE_MONTH NUMBER(28, 20) NULL;
-- End - Added column to hold threshold amount for availing twelve month special fianancing- 30th Dec 2015