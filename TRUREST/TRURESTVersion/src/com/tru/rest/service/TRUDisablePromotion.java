package com.tru.rest.service;

import java.util.Calendar;

import atg.epub.project.Process;
import atg.nucleus.GenericService;
import atg.process.action.ActionException;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;

import com.tru.merchandising.constants.TRUMerchConstants;
import com.tru.merchutil.tol.workflow.TRUWorkFlowTools;
import com.tru.promotion.repository.TRUPromotionRepositoryTools;



/**
 * The Class TRUDisablePromotion.
 * This is used to disable the promotionId that is sent to disablePromotion() method in BCC
 *    
 * @author PA
 * @version 1.1
 * 
 */
public class TRUDisablePromotion extends GenericService {

	/** The m work flow tools. */
	private TRUWorkFlowTools mWorkFlowTools;


	/** The m promotion tools. */

	private TRUPromotionRepositoryTools mPromotionRepositoryTools;
	/**
	 * Gets the work flow tools.
	 *
	 * @return the work flow tools
	 */
	public TRUWorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 *
	 * @param pWorkFlowTools the new work flow tools
	 */
	public void setWorkFlowTools(TRUWorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionRepositoryTools getPromotionRepositoryTools() {
		return mPromotionRepositoryTools;
	}

	/**
	 * @param pPromotionRepositoryTools the promotionTools to set
	 */
	public void setPromotionRepositoryTools(TRUPromotionRepositoryTools pPromotionRepositoryTools) {
		mPromotionRepositoryTools = pPromotionRepositoryTools;
	}



	/**
	 * This method will disable the promotionId in BCC and deploys the project.
	 * 
	 * @param pPromotionId
	 *            the Promotion ID
	 */
	public void disablePromotion(String[] pPromotionId){
		if (isLoggingDebug()) {
			logDebug("Start TRUDisablePromotion.disablePromotion method..");
		}
		if(pPromotionId != null && pPromotionId.length > 0){
			String workflowName = null;
			StringBuffer projectName = new StringBuffer();
			Calendar calendar = Calendar.getInstance();
			projectName.append(TRUMerchConstants.PROMOTION_DISABLE_PROJECT_NAME);
			projectName.append(TRUMerchConstants.UNDER_SCORE);
			projectName.append(calendar.getTime());
			workflowName=getWorkFlowTools().getAutoWorkflow();
			Process process = getWorkFlowTools().initiateWorkflow(projectName.toString(), workflowName);
			for (int i = 0; i < pPromotionId.length; i++) {
				getPromotionRepositoryTools().disablePromotionItem(pPromotionId[i]);
			}
			try {
				getWorkFlowTools().advanceWorkflow(process,
						getWorkFlowTools().getTaskOutcomeId());
				getWorkFlowTools().finishTask();

				if (isLoggingDebug()) {
					logDebug("End TRUDisablePromotion.disablePromotion method..");
				}


			} catch (MissingWorkflowDescriptionException pExe) {
				if (isLoggingError()) {
					vlogError("MissingWorkflowDescriptionException: While creating workflow to disable promotion with exception : {0}", pExe);
				}
			} catch (ActorAccessException pExe) {
				if (isLoggingError()) {
					vlogError("ActorAccessException: While accessing rest service to disable promotion with exception : {0}", pExe);
				}
			} catch (ActionException pExe) {
				if (isLoggingError()) {
					vlogError("ActorAccessException: disabling promotion with exception : {0}", pExe);
				}

			}
		}
	}

}
