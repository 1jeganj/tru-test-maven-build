package com.tru.rest.service;

import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.tru.merchutil.tol.workflow.WorkFlowTools;

import atg.core.util.StringUtils;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.epub.project.ProjectHome;
import atg.nucleus.GenericService;



/**
 * The Class TRUGetProjectNameWithId.
 * This is used to get the deployed project name with project id.
 *    
 * @author PA
 * @version 1.1
 * 
 */
public class TRUGetProjectNameWithId extends GenericService {

	/** The WorkFlow Tools. */
	private WorkFlowTools mWorkFlowTools;
	
	private String mUserName;
	
	/**
	 * This method will give the deployed project name with project id.
	 * 
	 * @param pPromotionId
	 *            the Promotion ID
	 */
	public String projectNameFromID(String pProjectId){
		if (isLoggingDebug()) {
			logDebug("Start TRUGetProjectNameWithId.projectNameFromID method..");
		}
		ProjectHome projectHome = null;
		String projectName = null;
		if(!StringUtils.isEmpty(pProjectId)){
			if (isLoggingDebug()) {
				vlogDebug("TRUGetProjectNameWithId.projectNameFromID() pProjectId : {0}" , pProjectId);
			}
			getWorkFlowTools().assumeUserIdentity(getUserName());
			projectHome = ProjectConstants.getPersistentHomes().getProjectHome();
			if (projectHome != null) {
				try {
					Project project = projectHome.findById(pProjectId);
					if (project != null) {
						projectName = project.getDisplayName();
						if (isLoggingDebug()) {
							vlogDebug("TRUGetProjectNameWithId.projectNameFromID() projectName : {0}" , projectName);
						}
					}
				} catch (EJBException e) {
					if (isLoggingError()) {
						vlogError("EJB Exception while accessing item ", e);
					}
				} catch (FinderException e) {
					if (isLoggingError()) {
						vlogError("Finder Exception while accessing item ", e);
					}
				}
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("End of TRUGetProjectNameWithId.projectNameFromID() method");
		}
		return projectName;
	}

	/**
	 * Gets the work flow tools.
	 * 
	 * @return the workFlowTools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 * 
	 * @param pWorkFlowTools
	 *            the workFlowTools to set
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}
	
	/**
	 * Gets the UserName.
	 * 
	 * @return the UserName
	 */
	public String getUserName() {
		return mUserName;
	}

	/**
	 * Sets the UserName.
	 * 
	 * @param pUserName
	 *            the UserName to set
	 */
	public void setUserName(String pUserName) {
		this.mUserName = pUserName;
	}
}
