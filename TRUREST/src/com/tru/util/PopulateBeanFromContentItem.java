package com.tru.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.SearchAdjustments;
import com.endeca.infront.cartridge.model.AdjustedSearch;
import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.model.SortOptionLabel;
import com.endeca.infront.cartridge.model.SuggestedSearch;
import com.endeca.infront.cartridge.model.UrlAction;
import com.endeca.infront.content.support.XmlContentItem;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.common.AdjustedTerms;
import com.tru.common.Ancestors;
import com.tru.common.Classification;
import com.tru.common.DidYouMean;
import com.tru.common.DimCacheObjectVO;
import com.tru.common.EndecaRecords;
import com.tru.common.Navigation;
import com.tru.common.PagingActionTemplate;
import com.tru.common.PriceDetails;
import com.tru.common.Product;
import com.tru.common.ProductList;
import com.tru.common.Redirect;
import com.tru.common.RefinementCrumbs;
import com.tru.common.Refinements;
import com.tru.common.SKUDetail;
import com.tru.common.SKURecords;
import com.tru.common.SearchAdjustment;
import com.tru.common.SortOptions;
import com.tru.common.SuggestedSearches;
import com.tru.common.SwatchColors;
import com.tru.common.TRURestConfiguration;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.vo.BreadCrumbsVO;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUEndecaPriceUtils;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;
/**
 * This class is used to populate beans from contentItem object return by assembler. 
 * @version 1.0
 * @author PA
 * @param <E> the type of elements in this collection
 */
public class PopulateBeanFromContentItem<E> extends GenericService{
	
	/** Property to hold mRestConfiguration. */
	private TRURestConfiguration mRestConfiguration;
	
	/** Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;
	
	/** Property to hold mDimensionValueCache. */
	private TRUDimensionValueCache mDimensionValueCache;
	
	/** Property to hold mGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mGetSiteTypeUtil;
	
	/** Property to hold mProductEndecaUtil. */
	private TRUProductEndecaUtil<E> mProductEndecaUtil;
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	/** Property to hold mEndecaPriceUtils. */
	private TRUEndecaPriceUtils mEndecaPriceUtils;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * @return the restConfiguration
	 */
	public TRURestConfiguration getRestConfiguration() {
		return mRestConfiguration;
	}
	/**
	 * @param pRestConfiguration the restConfiguration to set
	 */
	public void setRestConfiguration(TRURestConfiguration pRestConfiguration) {
		mRestConfiguration = pRestConfiguration;
	}
	/**
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}
	/**
	 * @param pDimensionValueCache the dimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}
	/**
	 * This method used to populate bean from content item.
	 * @param pResponseContentItem responseContentItem
	 * @param pRefineId String
	 * @param pCallType String
	 * @param pStoreId - StoreId
	 * @param pPriceEnableFlag - PriceEnableFlag
	 * @return output
	 */
	
	@SuppressWarnings({ "unchecked" })
	public ProductList populateBean(ContentItem pResponseContentItem, String pRefineId, String pCallType, String pStoreId, boolean pPriceEnableFlag) {
		ProductList output= new ProductList();
		BasicContentItem redirectResponse;
		UrlAction urlAction;
		Redirect redirect;
		redirectResponse=(BasicContentItem)pResponseContentItem.get(TRURestConstants.REDIRECT);
		if(redirectResponse!=null&&redirectResponse.get(getRestConfiguration().getAtType()).equals(TRURestConstants.REDIRECT_TYPE)){
			urlAction=(UrlAction) redirectResponse.get(TRURestConstants.LINK);
			redirect=new Redirect();
			redirect.setLinkUrl(urlAction.getUrl());
			output.setRedirect(redirect);
			output.setSearchType(TRURestConstants.REDIRECT_TYPE);
			return output;
		}
		List<E> contentItem = (ArrayList<E>) pResponseContentItem.get(getRestConfiguration().getContents());
		XmlContentItem allContents = null;
		List<E> mainHeader = null;
		List<E> mainProduct = null;
		List<E> mainContent = null;
		if(contentItem!=null && !contentItem.isEmpty()){
			allContents = (XmlContentItem) contentItem.get(TRURestConstants.INT_ZERO);
			mainHeader = (ArrayList<E>) allContents.get(getRestConfiguration().getMainHeader());
			mainProduct = (ArrayList<E>) allContents.get(getRestConfiguration().getMainProduct());
			mainContent = (ArrayList<E>) allContents.get(getRestConfiguration().getMainContent());
		}
		Breadcrumbs breadcrumbs;
		BasicContentItem classificationHierarchy;
		SearchAdjustments searchAdjustments;
		//Iterate header content section to get breadcrumb, classification hierarchy and search adjustment cartridges response
		int arraySize=TRURestConstants.INT_ZERO;
		if(mainHeader!=null){
			arraySize=mainHeader.size();	
		}
		for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
			if(mainHeader.get(iterate) instanceof Breadcrumbs && ((String)((Breadcrumbs)mainHeader.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruBreadcrumbs())){
				breadcrumbs = (Breadcrumbs) mainHeader.get(iterate);
				populateBreadcrumbsBean(breadcrumbs,output,pRefineId);
			} else if(mainHeader.get(iterate) instanceof BasicContentItem && ((String)((BasicContentItem)mainHeader.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruSubCatPageClassificationHierarchy())) {
				classificationHierarchy = (BasicContentItem) mainHeader.get(iterate);
				populateClassificationHierarchy(classificationHierarchy,output);
			} else if(mainHeader.get(iterate) instanceof SearchAdjustments && ((String)((SearchAdjustments)mainHeader.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getSearchAdjustments())) {
				searchAdjustments = (SearchAdjustments) mainHeader.get(iterate);
				populateSearchAdjustments(searchAdjustments,output);
			}
		}
		//Iterate main content section to get breadcrumbs cartridge response
		arraySize=TRURestConstants.INT_ZERO;
		if(mainContent!=null){
			arraySize=mainContent.size();
		}
		for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
			if(mainContent.get(iterate) instanceof Breadcrumbs && ((String)((Breadcrumbs)mainContent.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruBreadcrumbs())){
				breadcrumbs = (Breadcrumbs) mainContent.get(iterate);
				populateBreadcrumbsBean(breadcrumbs,output,pRefineId);
			} else if(mainContent.get(iterate) instanceof BasicContentItem && ((String)((BasicContentItem)mainContent.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruCatPageClassificationHierarchy())) {
				classificationHierarchy = (BasicContentItem) mainContent.get(iterate);
				populateClassificationHierarchy(classificationHierarchy,output);
			}	
		}
		XmlContentItem navigation;
		XmlContentItem newXmlMain;
		ResultsList resultsList = null;
		ResultsList skuIdArray = null;
		arraySize = TRURestConstants.INT_ZERO;
		Map<String, TRUInventoryInfo> inventoryInfo = null;
		Map<String, Map<String, Object>> skuPriceMap = null;
		// added for main product content slot
		if (mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem
				&& ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(getRestConfiguration().getAtType()).toString().equals(getRestConfiguration().getTRUMainProductContentSlot())) {
			mainProduct = ((List<E>) ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(TRURestConstants.CONTENTS));
			if(mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem){
				newXmlMain = (XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO);
				mainProduct = (List<E>) newXmlMain.get(getRestConfiguration().getMainProduct());
			}
		}
		if(mainProduct!=null)
		{
			arraySize=mainProduct.size();
		}
		for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++) {
			if(mainProduct.get(iterate) instanceof ResultsList && ((String)((ResultsList)mainProduct.get(iterate)).get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruResultsList())){
				skuIdArray = (ResultsList)mainProduct.get(iterate);
				if (skuIdArray != null && !skuIdArray.isEmpty() && skuIdArray.get(TRURestConstants.SKU_IDS) != null) {
					output.setSkuIds(skuIdArray.get(TRURestConstants.SKU_IDS).toString());
				}
				if (skuIdArray != null && !skuIdArray.isEmpty() && skuIdArray.get(TRURestConstants.SKU_PRICE_ARRAY) != null) {
					String skuProdString = skuIdArray.get(TRURestConstants.SKU_PRICE_ARRAY).toString();
					if(StringUtils.isNotBlank(skuProdString)) {
						String[] skuProdArray = skuProdString.split(TRURestConstants.SPLIT_PIPE);
						output.setSkuPriceArray(skuProdArray);
						break;
					}
				}
			} 
		}
		if (output.getSkuIds() != null && StringUtils.isNotBlank(pCallType) && !pCallType.equalsIgnoreCase(TRURestConstants.PROMO_OUTPUT)) {
			inventoryInfo = getProductEndecaUtil().populateOnlineAndStoreInventory(output.getSkuIds(), pStoreId);
		}
		if (output.getSkuPriceArray() != null && pPriceEnableFlag) {
			skuPriceMap = getEndecaPriceUtils().generatePriceMapForEndeca(output.getSkuPriceArray());
		}
		//Iterate main product section to get refinements, selected refinements and results list cartridge response
		for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
			if(mainProduct.get(iterate) instanceof XmlContentItem && ((String)((XmlContentItem)mainProduct.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruGuidedNavigation())){
				navigation = (XmlContentItem) mainProduct.get(iterate);
				populateNavigationBean(navigation,output);
			} else if(mainProduct.get(iterate) instanceof ResultsList && ((String)((ResultsList)mainProduct.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruResultsList())){
				resultsList = (ResultsList)mainProduct.get(iterate);
				populateResultsListBean(resultsList, output, pCallType ,inventoryInfo, pStoreId, skuPriceMap);
			} else if(mainProduct.get(iterate) instanceof Breadcrumbs && ((String)((Breadcrumbs)mainProduct.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruCrumbFacets())){
				breadcrumbs = (Breadcrumbs)mainProduct.get(iterate);
				populateBreadcrumbsBean(breadcrumbs,output,pRefineId);
			}
		}
		
		return output;
	}
	/**
	 * Populate.
	 * @param pSearchAdjustments searchAdjustments
	 * @param pOutput output
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populateSearchAdjustments(SearchAdjustments pSearchAdjustments, ProductList pOutput) {
		SearchAdjustment searchAdjustment=new SearchAdjustment();
		Iterator<?> entries;
		if(pSearchAdjustments.getOriginalTerms()!=null){
			pOutput.setOriginalTerms(pSearchAdjustments.getOriginalTerms());
		}	
		//Populate suggested search terms
		int iterate;
		int size;
		Map<String,List<SuggestedSearch>> endecaSearchSuggests = (HashMap<String,List<SuggestedSearch>>) pSearchAdjustments.getSuggestedSearches();
		if(endecaSearchSuggests!=null){	
			List<SuggestedSearches> newSuggestedSearchesArray=new ArrayList<SuggestedSearches>();
			Map<String,List<SuggestedSearches>> suggestedSearches=new HashMap<String,List<SuggestedSearches>>();
			DidYouMean didYouMean= new DidYouMean();
			entries=endecaSearchSuggests.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();
			    String key = (String)entry.getKey();
				List<SuggestedSearch> value = (ArrayList<SuggestedSearch>)entry.getValue();
				for(iterate=TRURestConstants.INT_ZERO,size=value.size();iterate<size;iterate++){
					SuggestedSearch endecaSuggestedSearch=value.get(iterate);
					SuggestedSearches newSuggestedSearches=new SuggestedSearches();
					newSuggestedSearches.setLabel(endecaSuggestedSearch.getLabel());
					newSuggestedSearches.setNavigationState(endecaSuggestedSearch.getNavigationState());
					newSuggestedSearchesArray.add(newSuggestedSearches);
				}
				suggestedSearches.put(key, newSuggestedSearchesArray);
			}
			didYouMean.setSuggestedSearches(suggestedSearches);
			pOutput.setSearchType(TRURestConstants.DID_YOU_MEAN);
			pOutput.setDidYouMean(didYouMean);
		}	
		//Populate adjusted search terms
		Map<String, List<AdjustedSearch>> endecaAdjustedSearchMap=(HashMap<String,List<AdjustedSearch>>)pSearchAdjustments.getAdjustedSearches();
		if(endecaAdjustedSearchMap!=null){
			List<AdjustedTerms> newAdjustedTermsArray=new ArrayList<AdjustedTerms>();
			Map<String,List<AdjustedTerms>> adjustedSearches=new HashMap<String,List<AdjustedTerms>>();
			entries=endecaAdjustedSearchMap.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();
			    String key = (String)entry.getKey();
				List<AdjustedSearch> value = (ArrayList<AdjustedSearch>)entry.getValue();
				for(iterate=TRURestConstants.INT_ZERO,size=value.size();iterate<size;iterate++){
					AdjustedSearch endecaAdjustedSearch=value.get(iterate);
					AdjustedTerms newAdjustedTerms=new AdjustedTerms();
					newAdjustedTerms.setAdjustedTerms(endecaAdjustedSearch.getAdjustedTerms());
					newAdjustedTermsArray.add(newAdjustedTerms);
				}
				adjustedSearches.put(key, newAdjustedTermsArray);
			}
			searchAdjustment.setAdjustedSearches(adjustedSearches);
			pOutput.setSearchType(TRURestConstants.SEARCH_ADJUSTMENTS);
			pOutput.setSearchAdjustments(searchAdjustment);
		}	
	}
	/**
	 * Populate classification hierarchy.
	 * @param pClassificationHierarchy classificationHierarchy
	 * @param pOutput output
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populateClassificationHierarchy(BasicContentItem pClassificationHierarchy, ProductList pOutput) {
		Map<?, ?> breadcrumbsVOmap = null;
		if(pClassificationHierarchy.get(TRURestConstants.TWO_LEVEL_CAT_MAP)!=null){
			breadcrumbsVOmap= (HashMap<?, ?>) (pClassificationHierarchy.get(TRURestConstants.TWO_LEVEL_CAT_MAP));
		}
		int size;
		int count;
		if(breadcrumbsVOmap!=null){
			Map<String,List<Classification>> newClassificationMap= new HashMap<String,List<Classification>>();
			Iterator<?> entries = breadcrumbsVOmap.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry entry = (Map.Entry) entries.next();
			    String key = (String)entry.getKey();
				List<BreadCrumbsVO> value = (ArrayList<BreadCrumbsVO>)entry.getValue();
				List<Classification> classificationList = new ArrayList<Classification>();
			    for(size=value.size(),count=TRURestConstants.INT_ZERO;count<size;count++){
			    	Classification classification = new Classification();
			    	classification.setCategoryId(value.get(count).getCategoryId());
			    	classification.setCategoryImage(value.get(count).getCategoryImage());
			    	classification.setCategoryName(value.get(count).getCategoryName());
			    	classification.setCategoryURL(value.get(count).getCategoryURL());
			    	classificationList.add(classification);
			    }	
			    newClassificationMap.put(key, classificationList);
			}
			pOutput.setTwoLevelCategoryTreeMap(newClassificationMap);
		}
		if(pClassificationHierarchy.get(TRURestConstants.CURRENT_CATEGORY_ID)!=null){
			pOutput.setCurrentCategoryId((String) pClassificationHierarchy.get(TRURestConstants.CURRENT_CATEGORY_ID));
		}
		if(pClassificationHierarchy.get(TRURestConstants.CURRENT_CATEGORY_NAME)!=null){
			pOutput.setCurrentCategoryName((String) pClassificationHierarchy.get(TRURestConstants.CURRENT_CATEGORY_NAME));
		}
	}

	/**
	 * Populate bean from breadcrumb cartridge response.
	 * @param pBreadcrumbs breadcrumb
	 * @param pOutput output
	 * @param pRefineId String
	 */
	private void populateBreadcrumbsBean(Breadcrumbs pBreadcrumbs, ProductList pOutput, String pRefineId) {
		//Populate refinementcrumb
		List<Ancestor> ancestors;
		ListIterator<Ancestor> iterateAncestor;
		RefinementBreadcrumb refinementBreadcrumb;
		RefinementCrumbs newRefinementCrumbs;
		List<RefinementCrumbs> newRefinementCrumbsArray = new ArrayList<RefinementCrumbs>();
		Ancestor ancestor;
		Ancestors newAncestor;
		List<Ancestors> newAncestorArray;
		List<RefinementBreadcrumb> refinementCrumbs = (ArrayList<RefinementBreadcrumb>) pBreadcrumbs.getRefinementCrumbs();
		ListIterator<RefinementBreadcrumb> iterate = refinementCrumbs.listIterator();
		while(iterate.hasNext()){
			refinementBreadcrumb = iterate.next();
			if(StringUtils.isNotBlank(pRefineId) && StringUtils.isNotBlank(refinementBreadcrumb.getProperties().get(getRestConfiguration().getCategoryRepositoryId())) && refinementBreadcrumb.getProperties().get(getRestConfiguration().getCategoryRepositoryId()).equals(pRefineId)){
				continue;
			}
			newAncestorArray = new ArrayList<Ancestors>();
			ancestors = (ArrayList<Ancestor>) refinementBreadcrumb.getAncestors();
			iterateAncestor = ancestors.listIterator();
			while(iterateAncestor.hasNext()){
				newAncestor = new Ancestors();
				ancestor = iterateAncestor.next();
				newAncestor.setLabel(ancestor.getLabel());
				if(StringUtils.isNotBlank(ancestor.getProperties().get(TRURestConstants.CATEGORY_REPOSITORY_ID))){
					newAncestor.setNavigationState(getAncesterCategoryURL(ancestor));
				}
				newAncestorArray.add(newAncestor);				
			}
			newRefinementCrumbs = new RefinementCrumbs();
			pOutput.setComparable(Boolean.parseBoolean(refinementBreadcrumb.getProperties().get(getRestConfiguration().getIsCompareProduct())));
			newRefinementCrumbs.setLabel(refinementBreadcrumb.getLabel());
			if (refinementBreadcrumb.getDimensionName().equals(getRestConfiguration().getProductCategory()) &&
					!refinementBreadcrumb.getRemoveAction().getNavigationState().contains(TRURestConstants.PARAM_SEARCH)) {
				String tempNavState = refinementBreadcrumb.getRemoveAction().getNavigationState().substring(TRURestConstants.INT_ONE);
				String categoryDimId = tempNavState.substring(tempNavState.lastIndexOf(TRURestConstants.EQUALS)+TRURestConstants.INT_ONE, tempNavState.length());
				if(StringUtils.isBlank(pRefineId)){
					newRefinementCrumbs.setNavigationState(TRURestConstants.EMPTY_SPACE);
				}else if(tempNavState.contains(TRURestConstants.NAVIGATION_EQUAL)){
					newRefinementCrumbs.setNavigationState(TRURestConstants.QUERY+TRURestConstants.NAVIGATION_EQUAL+categoryDimId+TRURestConstants.PLUS+tempNavState.substring(TRURestConstants.INT_TWO));
				}else{
					newRefinementCrumbs.setNavigationState(TRURestConstants.QUERY+TRURestConstants.NAVIGATION_EQUAL+categoryDimId+TRURestConstants.AND+tempNavState);
				}
			}else{
				newRefinementCrumbs.setNavigationState(refinementBreadcrumb.getRemoveAction().getNavigationState());
			}
			newRefinementCrumbs.setAncestors(newAncestorArray);
			newRefinementCrumbs.setCategoryId(refinementBreadcrumb.getProperties().get(getRestConfiguration().getCategoryRepositoryId()));
			newRefinementCrumbsArray.add(newRefinementCrumbs);
		}
		pOutput.setRefinementCrumbs(newRefinementCrumbsArray);
	}
	
	/**
	 * Populate ancestor category URL.
	 * @param pAncestor ancestor
	 * @return tempCatURL String
	 */
	@SuppressWarnings("static-access")
	public String getAncesterCategoryURL(Ancestor pAncestor){
		StringBuffer tempCatURL = new StringBuffer(getDimensionValueCache().getCacheObject(pAncestor.getProperties().get(TRURestConstants.CATEGORY_REPOSITORY_ID)).getUrl());
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if(StringUtils.isNotBlank(request.getHeader(TRURestConstants.API_CHANNEL))&&!request.getHeader(TRURestConstants.API_CHANNEL).equals(TRURestConstants.MOBILE)){
			StringBuffer categoryURL = new StringBuffer();
			categoryURL.append(getSchemeDetectionUtil().getScheme());
			Site currentSite = getGetSiteTypeUtil().getSiteContextManager().getCurrentSite();
			TRUSiteVO siteVO = getGetSiteTypeUtil().getSiteInfo(request, currentSite);
			categoryURL.append(siteVO.getSiteURL());
			tempCatURL.insert(TRURestConstants.INT_ZERO, categoryURL);
		}
		return tempCatURL.toString();
	}
	
	/**
	 * Populate bean from results list cartridge response.
	 * @param pResultsList resultsList
	 * @param pCallType String
	 * @param pInventoryInfo - InventoryInfo
	 * @param pStoreId - StoreId
	 * @param pOutput output
	 * @param pSkuPriceMap - SkuPriceMap
	 */
	@SuppressWarnings("unchecked")
	private void populateResultsListBean(ResultsList pResultsList, ProductList pOutput, String pCallType ,Map<String, TRUInventoryInfo> pInventoryInfo, String pStoreId,Map<String, Map<String, Object>> pSkuPriceMap){
		if(StringUtils.isNotBlank(pCallType) && pCallType.equalsIgnoreCase(TRURestConstants.SEARCH_OUTPUT) && pResultsList.getRecords() != null && !pResultsList.getRecords().isEmpty() && (pResultsList.getRecords().size() == TRUEndecaConstants.INT_ONE)){	
			Record record = pResultsList.getRecords().get(TRUEndecaConstants.INT_ZERO);
			List<String> siteIds = record.getAttributes().get(TRUEndecaConstants.SKU_SITEID);
			if (siteIds != null && !siteIds.isEmpty()) {
				String currentSiteId = SiteContextManager.getCurrentSiteId();
				if (!siteIds.contains(currentSiteId)) {
					pOutput.setSearchType(TRURestConstants.ERR_ENTER_VALID_SITE);
					return;
				}
			}
		}		
		pOutput.setFirstRecNum((Long)pResultsList.getFirstRecNum());
		pOutput.setLastRecNum((Long)pResultsList.getLastRecNum());
		pOutput.setTotalNumOfRec((Long)pResultsList.getTotalNumRecs());
		if(pOutput.getTotalNumOfRec()==TRURestConstants.INT_ZERO){
			if(pOutput.getSearchType()==null){
				pOutput.setSearchType(TRURestConstants.NO_RESULTS);
			}
			return;
		}else{
			if(pOutput.getSearchType()==null){
				pOutput.setSearchType(TRURestConstants.SEARCH_RESULTS);
			}
		}
		PagingActionTemplate pagingActionTemplate=new PagingActionTemplate();
		NavigationAction navAction=pResultsList.getPagingActionTemplate();
		StringBuffer navigationState=new StringBuffer();
		String[] breakNS=navAction.getNavigationState().substring(TRURestConstants.INT_ONE).split(TRURestConstants.AND);
		int itr=TRURestConstants.INT_ZERO;
		navigationState.append(TRURestConstants.QUERY);
		for(int size=breakNS.length;itr<size;itr++){
			if(!breakNS[itr].contains(TRURestConstants.NO_PARAM)&&!breakNS[itr].contains(TRURestConstants.NRPP_PARAM)){
				navigationState.append(breakNS[itr]+TRURestConstants.AND);
			}
		}
		pagingActionTemplate.setNavigationState(navigationState.deleteCharAt(navigationState.length()-TRURestConstants.INT_ONE).toString());
		pOutput.setPagingActionTemplate(pagingActionTemplate);
		// start populating products
		List<Record> products = (ArrayList<Record>)pResultsList.getRecords();
		Iterator<Record> iterate = products.iterator();
		List<EndecaRecords> newRecordsArray = new ArrayList<EndecaRecords>();
		EndecaRecords newRecords;
		while(iterate.hasNext()){	
			newRecords = populateProduct(iterate.next() ,pInventoryInfo, pStoreId, pCallType, pSkuPriceMap);
			newRecordsArray.add(newRecords);
		}
		pOutput.setRecords(newRecordsArray);
		List<SortOptionLabel> sortOptions = new ArrayList<SortOptionLabel>();
		SortOptionLabel endecaSort;
		SortOptions newSort;
		List<SortOptions> newSortArray = new ArrayList<SortOptions>();
		sortOptions = (ArrayList<SortOptionLabel>)pResultsList.getSortOptions();
		ListIterator<SortOptionLabel> listSort = sortOptions.listIterator();
		while(listSort.hasNext()){
			newSort = new SortOptions();
			endecaSort = listSort.next();
			newSort.setNavigationState(endecaSort.getNavigationState());
			newSort.setLabel(endecaSort.getLabel());
			newSort.setSelected(endecaSort.isSelected());
			newSortArray.add(newSort);
		}
		pOutput.setSortOptions(newSortArray);
		pOutput.setCMSSPOT((List<String>) pResultsList.get(getRestConfiguration().getCMSSPOTPropertyName()));
		if(pOutput.getCMSSPOT()!=null){
			pOutput.setRecsPerPage((Long)pResultsList.getRecsPerPage()+pOutput.getCMSSPOT().size());
		}else{
			pOutput.setRecsPerPage((Long)pResultsList.getRecsPerPage());
		}
	}
	
	/**
	 * Populate product information in beans.
	 * @param pEndecaRecord endeca record
	 * @param pInventoryInfo - InventoryInfo
	 * @param pStoreId - StoreId
	 * @param pCallType - CallType
	 * @param pSkuPriceMap - SkuPriceMap
	 * @return EndecaRecords newRecords
	 */
	@SuppressWarnings({ "rawtypes" })
	private EndecaRecords populateProduct(Record pEndecaRecord ,Map<String, TRUInventoryInfo> pInventoryInfo, String pStoreId,String pCallType, Map<String, Map<String, Object>> pSkuPriceMap){
		EndecaRecords newRecords;
		Map<String,Attribute> productAttributes = new HashMap<String,Attribute>();
		productAttributes = (HashMap<String,Attribute>) pEndecaRecord.getAttributes();		
		Product newProduct = new Product(); 
		List<Record> skus;
		List<SKUDetail> newSkuArray;
		SKURecords skuRecords;
		Iterator<Record> iterateSKUs;
		int  collectionskusize=0;
		boolean isCollectionProduct = TRURestConstants.BOOLEAN_FALSE;
		if (productAttributes.get(getRestConfiguration().getSkuListSize())!=null) {
			collectionskusize	= Integer.parseInt(productAttributes.get(getRestConfiguration().getSkuListSize()).toString());	
		}

		if(productAttributes.get(getRestConfiguration().getProductDisplayName())!=null){
			newProduct.setDisplayName(productAttributes.get(getRestConfiguration().getProductDisplayName()).toString());
		}
		if(productAttributes.get(getRestConfiguration().getProductSiteId())!=null){
			if(productAttributes.get(getRestConfiguration().getProductSiteId()).contains(SiteContextManager.getCurrentSiteId())){
				newProduct.setSiteId(SiteContextManager.getCurrentSiteId());
			}else{
				newProduct.setSiteId(productAttributes.get(getRestConfiguration().getProductSiteId()).toString());
			}
		}
		if(productAttributes.get(getRestConfiguration().getProductRepositoryId())!=null){
			newProduct.setRepositoryId(productAttributes.get(getRestConfiguration().getProductRepositoryId()).toString());
		}
		if(pEndecaRecord.getDetailsAction()!=null){
			newProduct.setUrl(getSchemeDetectionUtil().getScheme()+pEndecaRecord.getDetailsAction().getRecordState());
		}
		if(productAttributes.get(getRestConfiguration().getProductLanguage())!=null){
			newProduct.setLanguage(productAttributes.get(getRestConfiguration().getProductLanguage()).toString());
		}
		if(productAttributes.get(getRestConfiguration().getProductCategory())!=null){
			newProduct.setCategory(productAttributes.get(getRestConfiguration().getProductCategory()).toString());
		}
		if(productAttributes.get(getRestConfiguration().getProductType())!=null && 
				productAttributes.get(getRestConfiguration().getProductType()).toString().equals(TRURestConstants.COLLECTION_TYPE) && collectionskusize >TRUEndecaConstants.INT_ONE){
			newProduct.setIsCollectionProduct(TRURestConstants.BOOLEAN_TRUE);
			isCollectionProduct = TRURestConstants.BOOLEAN_TRUE;
		}
		newRecords = new EndecaRecords();
		//iterating SKU's
		skus=new ArrayList<Record>();
		newSkuArray = new ArrayList<SKUDetail>();
		skuRecords = new SKURecords();
		skus = (ArrayList<Record>)pEndecaRecord.getRecords();
		iterateSKUs = skus.iterator();
		SKUDetail newSku;
		while(iterateSKUs.hasNext()){
			newSku=populateSKUDetail(iterateSKUs.next(), pEndecaRecord.getNumRecords(), isCollectionProduct ,pInventoryInfo, pStoreId, pCallType, pSkuPriceMap);
			newSkuArray.add(newSku);			
		}
		setProductDetail(pEndecaRecord,newProduct);
		skuRecords.setSku(newSkuArray);
		newRecords.setRecords(skuRecords);
		newRecords.setProduct(newProduct);
		return newRecords;
	}
	
	/**
	 * Populate product level properties those common for all SKU's.
	 * @param pEndecaRecord endeca record
	 * @param pNewProduct product bean
	 */
	private void setProductDetail(Record pEndecaRecord, Product pNewProduct){
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
				getRestConfiguration().getPromotionalStickerDisplay())!=null){
					pNewProduct.setPromotionalStickerDisplay(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
							getRestConfiguration().getPromotionalStickerDisplay()).toString());
		}
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getRestConfiguration().getSkuBrand())!=null){
			pNewProduct.setBrand(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
					getRestConfiguration().getSkuBrand()).toString());
		}
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getRestConfiguration().getProductLongDescription())!=null){
			pNewProduct.setShortDescription(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
					getRestConfiguration().getProductLongDescription()).toString());
		}
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getRestConfiguration().getSkuLongDescription())!=null){
			pNewProduct.setLongDescription(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
					getRestConfiguration().getSkuLongDescription()).toString());
		}
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getRestConfiguration().getProductCategory())!=null){
			pNewProduct.setCategory(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
					getRestConfiguration().getProductCategory()).toString());
		}
		
		if(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getRestConfiguration().getOnlinePID())!=null){
			pNewProduct.setOnlinePID(pEndecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(
					getRestConfiguration().getOnlinePID()).toString());
		}
	}
	
	/**
	 * Populate SKU level properties.
	 * @param pEndecaChildRecord endeca child record
	 * @param pNumRecords 
	 * @return SKUDetail SKU detail
	 * @param pInventoryInfo - InventoryInfo
	 * @param pStoreId - StoreId
	 * @param pCallType - CallType
	 * @param pIsCollectionProduct to identify if product type is collection
	 * @param pSkuPriceMap - SkuPriceMap
	 */
	@SuppressWarnings({ "rawtypes" })
	private SKUDetail populateSKUDetail(Record pEndecaChildRecord, long pNumRecords, boolean pIsCollectionProduct, Map<String, TRUInventoryInfo> pInventoryInfo, String pStoreId,String pCallType, Map<String, Map<String, Object>> pSkuPriceMap){
		String s2s=null;
		String ispuString = null;
		String shipToStore = null;
		TRUInventoryInfo currSKUInvInfo = null;
		Map<String, Object> priceMap = null;
		Map<String,Attribute> skuAttributes = (HashMap<String,Attribute>) pEndecaChildRecord.getAttributes();
		SKUDetail newSku = new SKUDetail();
		if(pSkuPriceMap!= null && !pSkuPriceMap.isEmpty() && skuAttributes.get(getRestConfiguration().getSkuRepositoryId())!=null){
			priceMap =	pSkuPriceMap.get(skuAttributes.get(getRestConfiguration().getSkuRepositoryId()).toString());
		}
		setSwatchPromoDetail(skuAttributes,newSku,pNumRecords);
		setSKUBasicDetail(skuAttributes,newSku,pIsCollectionProduct);
		setSKUImageDetail(skuAttributes,newSku,pIsCollectionProduct);
		setSKUPriceDetail(skuAttributes,newSku,priceMap);
		if(skuAttributes.get(getRestConfiguration().getChildWeightMin())!=null){
			newSku.setChildWeightMin(Integer.parseInt(skuAttributes.get(getRestConfiguration().getChildWeightMin()).toString()));
		}
		if(skuAttributes.get(getRestConfiguration().getChildWeightMax())!=null){
			newSku.setChildWeightMax(Integer.parseInt(skuAttributes.get(getRestConfiguration().getChildWeightMax()).toString()));
		}
		if(skuAttributes.get(getRestConfiguration().getStreetDate())!=null){
			newSku.setStreetDate(skuAttributes.get(getRestConfiguration().getStreetDate()).toString());
		}
		if(skuAttributes.get(getRestConfiguration().getRegistryEligibility())!=null){
			newSku.setRegistryEligibility(skuAttributes.get(getRestConfiguration().getRegistryEligibility()).toString().equals(TRURestConstants.ONE_STR));
		}
		if(skuAttributes.get(getRestConfiguration().getVideoGameEsrb())!=null){
			newSku.setVideoGameEsrb(skuAttributes.get(getRestConfiguration().getVideoGameEsrb()).toString());
		}
		if(skuAttributes.get(getRestConfiguration().getVideoGameGenre())!=null){
			newSku.setVideoGameGenre(skuAttributes.get(getRestConfiguration().getVideoGameGenre()).toString());
		}
		if(skuAttributes.get(getRestConfiguration().getCustomerPurchaseLimit())!=null){
			newSku.setCustomerPurchaseLimit(skuAttributes.get(getRestConfiguration().getCustomerPurchaseLimit()).toString());
		}
		if(skuAttributes.get(getRestConfiguration().getSkuChannelAvailability()) != null){
			newSku.setChannelAvailability(skuAttributes.get(getRestConfiguration().getSkuChannelAvailability()).toString());
		}
		if(skuAttributes.get(getRestConfiguration().getIsStylized())!=null) {
            newSku.setIsStylized(Boolean.parseBoolean(skuAttributes.get(getRestConfiguration().getIsStylized()).toString()));
		}
		if(skuAttributes.get(getRestConfiguration().getOriginalParentProduct())!=null){
			newSku.setOriginalParentProduct(skuAttributes.get(getRestConfiguration().getOriginalParentProduct()).toString());
		}
		
		if(skuAttributes.get(getRestConfiguration().getRegistryWarningIndicator())!=null) {
            newSku.setRegistryWarningIndicator(skuAttributes.get(getRestConfiguration().getRegistryWarningIndicator()).toString());
		}
		if (skuAttributes.get(getRestConfiguration().getItemInStorePickUp()) != null) {
			String ispu = skuAttributes.get(getRestConfiguration().getItemInStorePickUp()).toString();
			if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)) {
				ispuString = TRUCommerceConstants.YES;
			} else {
				ispuString = TRUCommerceConstants.NO;
			}
			newSku.setIspu(ispuString);
		} else {
			newSku.setIspu(TRUCommerceConstants.NO);
		}
		if(skuAttributes.get(getRestConfiguration().getShipToStore()) != null) {
			shipToStore = null;
			s2s = skuAttributes.get(getRestConfiguration().getShipToStore()).toString();
			if(StringUtils.isNotEmpty(s2s) && s2s.equals(TRUCommerceConstants.STRING_ONE)) {
				shipToStore = TRUCommerceConstants.YES;
			} else {
				shipToStore = TRUCommerceConstants.NO;
			}
			newSku.setS2s(shipToStore);
		}
		if(StringUtils.isNotBlank(pCallType) && !pCallType.equalsIgnoreCase(TRURestConstants.PROMO_OUTPUT)) {
			if (pInventoryInfo != null && !pInventoryInfo.isEmpty()) {
				currSKUInvInfo = (TRUInventoryInfo) pInventoryInfo.get(newSku.getRepositoryId());
			}
			if (currSKUInvInfo == null || currSKUInvInfo.getAtcStatus() == TRURestConstants.COHERENCE_OUT_OF_STOCK) {
				newSku.setInventoryStatus(TRUCommerceConstants.OUT_OF_STOCK); 
			} else if (currSKUInvInfo != null) {
				newSku.setInventoryStatus(TRUCommerceConstants.IN_STOCK);
				if (StringUtils.isNotBlank(pStoreId) && StringUtils.isNotBlank(currSKUInvInfo.getLocationId()) && pStoreId.equalsIgnoreCase(currSKUInvInfo.getLocationId()) && ((StringUtils.isNotEmpty(ispuString) && ispuString.equalsIgnoreCase(TRUCommerceConstants.YES)) || (StringUtils.isNotEmpty(shipToStore) && shipToStore.equalsIgnoreCase(TRUCommerceConstants.YES)))) {
					newSku.setStoreInventory(true);
				}
			}
		}
		return newSku;
	}
	
	/**
	 * Populate SKU level properties.
	 * @param pNumRecords 
	 * @param pSkuAttributes endeca child record attributes
	 * @param pNewSku SKU bean
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setSwatchPromoDetail(Map<String,Attribute> pSkuAttributes, SKUDetail pNewSku, long pNumRecords){
		SwatchColors swatchColors;
		List<SwatchColors> swatchColorsList = new ArrayList<SwatchColors>();
		if(pNumRecords>TRURestConstants.INT_ONE && pSkuAttributes.get(getRestConfiguration().getSwatchImage())!=null){
			for(String swatchPromo:(List<String>)pSkuAttributes.get(getRestConfiguration().getSwatchImage())){
				swatchColors = new SwatchColors();
				swatchColors.setImageURL(swatchPromo.substring(TRURestConstants.INT_ZERO,swatchPromo.lastIndexOf(TRURestConstants.SEMICOLON)));
				swatchColorsList.add(swatchColors);
			}
			pNewSku.setSwatchImage(swatchColorsList);
		} 
	}
	
	/**
	 * Populate SKU level properties.
	 * @param pSkuAttributes endeca child record attributes
	 * @param pNewSku SKU bean
	 * @param pIsCollectionProduct to identify if product type is collection
	 */
	@SuppressWarnings("rawtypes")
	private void setSKUBasicDetail(Map<String,Attribute> pSkuAttributes, SKUDetail pNewSku, boolean pIsCollectionProduct){
		if(pIsCollectionProduct && pSkuAttributes.get(getRestConfiguration().getCollectionName())!=null) {
			pNewSku.setDisplayName(pSkuAttributes.get(getRestConfiguration().getCollectionName()).toString());
		} else {
			if(pSkuAttributes.get(getRestConfiguration().getSkuDisplayName())!=null){
				pNewSku.setDisplayName(pSkuAttributes.get(getRestConfiguration().getSkuDisplayName()).toString());
			}
		}	
		if(pSkuAttributes.get(getRestConfiguration().getSkuCreationDate())!=null){
			pNewSku.setCreationDate(pSkuAttributes.get(getRestConfiguration().getSkuCreationDate()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getSkuRepositoryId())!=null){
			pNewSku.setRepositoryId(pSkuAttributes.get(getRestConfiguration().getSkuRepositoryId()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getSkuOnSale())!=null){
			pNewSku.setOnSale(pSkuAttributes.get(getRestConfiguration().getSkuOnSale()).toString().equals(TRURestConstants.ONE_STR));
		}
		if(pSkuAttributes.get(getRestConfiguration().getSkuSiteId())!=null){
			if(pSkuAttributes.get(getRestConfiguration().getSkuSiteId()).contains(SiteContextManager.getCurrentSiteId())){
				pNewSku.setSiteId(SiteContextManager.getCurrentSiteId());
			}else{
				pNewSku.setSiteId(pSkuAttributes.get(getRestConfiguration().getSkuSiteId()).toString());
			}
		}
		if(pSkuAttributes.get(getRestConfiguration().getSkuReviewRating())!=null){
			pNewSku.setReviewRating(pSkuAttributes.get(getRestConfiguration().getSkuReviewRating()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getSkuReviewCount())!=null){
			pNewSku.setReviewCount(pSkuAttributes.get(getRestConfiguration().getSkuReviewCount()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getMfrSuggestedAgeMin())!=null){
			pNewSku.setMfrSuggestedAgeMin(pSkuAttributes.get(getRestConfiguration().getMfrSuggestedAgeMin()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getMfrSuggestedAgeMax())!=null){
			pNewSku.setMfrSuggestedAgeMax(pSkuAttributes.get(getRestConfiguration().getMfrSuggestedAgeMax()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getPriceDisplay())!=null){
			pNewSku.setPriceDisplay(pSkuAttributes.get(getRestConfiguration().getPriceDisplay()).toString());
		}
	}
	
	/**
	 * Populate SKU level properties.
	 * @param pSkuAttributes endeca child record attributes
	 * @param pNewSku SKU bean
	 * @param pIsCollectionProduct to identify if product type is collection
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setSKUImageDetail(Map<String,Attribute> pSkuAttributes, SKUDetail pNewSku,  boolean pIsCollectionProduct){
		if(pIsCollectionProduct && pSkuAttributes.get(getRestConfiguration().getCollectionImageUrl())!=null) {
				pNewSku.setPrimaryImage(pSkuAttributes.get(getRestConfiguration().getCollectionImageUrl()).toString());
		} else {
			if(pSkuAttributes.get(getRestConfiguration().getSearchImageUrl())!=null){
				pNewSku.setPrimaryImage(pSkuAttributes.get(getRestConfiguration().getSearchImageUrl()).toString());
			}
		}
		if(pSkuAttributes.get(getRestConfiguration().getSecondaryImage())!=null){
			pNewSku.setSecondaryImage(pSkuAttributes.get(getRestConfiguration().getSecondaryImage()).toString());
		}
		if(pSkuAttributes.get(getRestConfiguration().getAlternateImage())!=null){
			pNewSku.setAlternateImage(pSkuAttributes.get(getRestConfiguration().getAlternateImage()));
		}
		if(pSkuAttributes.get(getRestConfiguration().getShipInOrigContainer())!=null){
			pNewSku.setShipInOrigContainer(pSkuAttributes.get(
					getRestConfiguration().getShipInOrigContainer()).toString().equals(TRURestConstants.ONE_STR));
		}
		if(pSkuAttributes.get(getRestConfiguration().getUnCartable())!=null){
			pNewSku.setUnCartable(pSkuAttributes.get(getRestConfiguration().getUnCartable()).toString().equals(TRURestConstants.ONE_STR));
		}
	}
	
	/**
	 * Populate SKU level properties.
	 * @param pSkuAttributes endeca child record attributes
	 * @param pNewSku SKU bean
	 * @param pSkuPriceMap - SkuPriceMap
	 */
	@SuppressWarnings("rawtypes")
	private void setSKUPriceDetail(Map<String,Attribute> pSkuAttributes, SKUDetail pNewSku, Map<String, Object> pSkuPriceMap){
		if(pSkuPriceMap != null && !pSkuPriceMap.isEmpty()) {
			if(pSkuPriceMap.get(getRestConfiguration().getIsStrikeThrough())!=null){
				pNewSku.setIsStrikeThrough(Boolean.parseBoolean(pSkuPriceMap.get(getRestConfiguration().getIsStrikeThrough()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getHasPriceRange())!=null){
				pNewSku.setHasPriceRange(Boolean.parseBoolean(pSkuPriceMap.get(getRestConfiguration().getHasPriceRange()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getSkuSalePrice())!=null){
				pNewSku.setSalePrice(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getSkuSalePrice()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getSkuListPrice())!=null){
				pNewSku.setListPrice(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getSkuListPrice()).toString()));
			}
			// populate price details
			PriceDetails priceDetails=new PriceDetails();
			if(pSkuPriceMap.get(getRestConfiguration().getMaximumPrice())!=null){
				priceDetails.setMaximumPrice(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getMaximumPrice()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getMinimumPrice())!=null){
				priceDetails.setMinimumPrice(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getMinimumPrice()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getSavedAmount())!=null){
				priceDetails.setSavedAmount(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getSavedAmount()).toString()));
			}
			if(pSkuPriceMap.get(getRestConfiguration().getSavedPercentage())!=null){
				priceDetails.setSavedPercentage(Double.parseDouble(pSkuPriceMap.get(getRestConfiguration().getSavedPercentage()).toString()));
			}
			pNewSku.setPriceDetails(priceDetails);
		} else {
			if(pSkuAttributes.get(getRestConfiguration().getIsStrikeThrough())!=null){
				pNewSku.setIsStrikeThrough(Boolean.parseBoolean(pSkuAttributes.get(getRestConfiguration().getIsStrikeThrough()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getHasPriceRange())!=null){
				pNewSku.setHasPriceRange(Boolean.parseBoolean(pSkuAttributes.get(getRestConfiguration().getHasPriceRange()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getSkuSalePrice())!=null){
				pNewSku.setSalePrice(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getSkuSalePrice()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getSkuListPrice())!=null){
				pNewSku.setListPrice(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getSkuListPrice()).toString()));
			}
			// populate price details
			PriceDetails priceDetails=new PriceDetails();
			if(pSkuAttributes.get(getRestConfiguration().getMaximumPrice())!=null){
				priceDetails.setMaximumPrice(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getMaximumPrice()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getMinimumPrice())!=null){
				priceDetails.setMinimumPrice(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getMinimumPrice()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getSavedAmount())!=null){
				priceDetails.setSavedAmount(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getSavedAmount()).toString()));
			}
			if(pSkuAttributes.get(getRestConfiguration().getSavedPercentage())!=null){
				priceDetails.setSavedPercentage(Double.parseDouble(pSkuAttributes.get(getRestConfiguration().getSavedPercentage()).toString()));
			}
			pNewSku.setPriceDetails(priceDetails);
		}	
	}
	/**
	 * Populate bean from navigation cartridge response.
	 * @param pNavigation navigation
	 * @param pOutput output
	 */
	@SuppressWarnings("unchecked")
	private void populateNavigationBean(XmlContentItem pNavigation, ProductList pOutput){
		List<E> refinements = (ArrayList<E>) pNavigation.get(getRestConfiguration().getNavigation());
		RefinementMenu refinementMenu = null;
		RefinementMenu refinementMenuName = null;
		List<Refinement> refinementArray;
		Refinement refinement;
		ListIterator<Refinement> iterateRefinement = null;
		Refinements newRefinements;
		List<Refinements> refinementsArray = null;
		Navigation newNavigation;
		List<Navigation> navigationArray = new ArrayList<Navigation>();
		int count;
		int size;
		for(count=TRURestConstants.INT_ZERO,size=refinements.size();count<size;count++){
			if(refinements.get(count) instanceof RefinementMenu){
				refinementMenu = (RefinementMenu) refinements.get(count);
				refinementMenuName = (RefinementMenu) refinements.get(count);
				refinementArray = (List<Refinement>) refinementMenu.getRefinements();
				iterateRefinement = refinementArray.listIterator();
				refinementsArray = new ArrayList<Refinements>();
				while(iterateRefinement.hasNext()){
					refinement = iterateRefinement.next();
					newRefinements = new Refinements();
					newRefinements.setNavigationState(refinement.getNavigationState());
					newRefinements.setLabel(refinement.getLabel());
					newRefinements.setCount(refinement.getCount());
					newRefinements.setMultiselect(refinement.isMultiSelect());
					refinementsArray.add(newRefinements);
				}
				newNavigation = new Navigation();
				if(refinementMenu.get(getRestConfiguration().getDisplayable())!=null){
					newNavigation.setDisplayable((boolean) refinementMenu.get(getRestConfiguration().getDisplayable()));
				}
				if(refinementMenuName.get(getRestConfiguration().getFacetName())!=null){
					newNavigation.setName((String) refinementMenuName.get(getRestConfiguration().getFacetName()));
				}	
				newNavigation.setRefinements(refinementsArray);
				navigationArray.add(newNavigation);
			}
		}
		pOutput.setNavigation(navigationArray);
	}
	
	/**
	 * get navigation URL from category id or category dimension id.
	 * @param pCategoryId category id
	 * @return String navigation URL 
	 */
	public DimCacheObjectVO getNavigationURLfromCategory(String pCategoryId){
		DimensionValueCacheObject dimCacheObject = null;
		DimCacheObjectVO dimCacheObjectVO = new DimCacheObjectVO();
		dimCacheObject = getDimensionValueCache().getCachedObjectForDimval(pCategoryId);//Passed id is dimension id
		if (isLoggingDebug()) {
			logDebug("category/subcategory/family service fatched page name from dimension id."+dimCacheObject);
		}
		if (dimCacheObject == null) {
			dimCacheObject = getDimensionValueCache().getCacheWithMultipleAncestors(pCategoryId);
			if (isLoggingDebug()) {
				logDebug("category/subcategory/family service fetched page name from category id."+dimCacheObject);
			}
			if(dimCacheObject==null){
				// Passed Id is may be registry id
				dimCacheObject = getNavigationURLFromRegistry(pCategoryId);
				dimCacheObjectVO.setRegistryClassification(true);
			}
		} 
		dimCacheObjectVO.setDimValueCacheObject(dimCacheObject);
		return dimCacheObjectVO;
	}
	
	/**
	 * get navigation URL from category id or category dimension id.
	 * @param pCategoryId category id
	 * @return String navigation URL 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DimensionValueCacheObject getNavigationURLFromRegistry(String pCategoryId){
		if (isLoggingDebug()) {
			logDebug("category/subcategory/family service fatching page name from registry category id.");
		}
		DimensionValueCacheObject result = null;
		RepositoryItem categoryItem = null;
		String newCategoryId=null;
		try {
			categoryItem = getCatalogTools().findCategory(pCategoryId);
			if(categoryItem!=null && 
					categoryItem.getItemDescriptor().getItemDescriptorName().equals(getRestConfiguration().getRegistryClassificationItemName())){
				List<RepositoryItem> crossReferenceList = 
						(List<RepositoryItem>) categoryItem.getPropertyValue(getRestConfiguration().getCrossReferencePropertyName());
				if(crossReferenceList!=null && !crossReferenceList.isEmpty()){
					String currentSiteId = SiteContextManager.getCurrentSiteId();
					Set siteIdsSet=null;
					for(RepositoryItem classificationItem:crossReferenceList){
						siteIdsSet = (Set) classificationItem.getPropertyValue(getRestConfiguration().getSiteIdsPropertyName());
						if(StringUtils.isNotBlank(currentSiteId) && siteIdsSet!=null && siteIdsSet.contains(currentSiteId)){
							newCategoryId = classificationItem.getRepositoryId();
							break;
						}
					}
					if(StringUtils.isNotBlank(newCategoryId)){
						result = getDimensionValueCache().getCacheWithMultipleAncestors(newCategoryId);
						if(result!=null){
							return result;
						}
					}
					
				}
			}
		} catch (RepositoryException e) {
			vlogError("Getting repository exception while fatching item for passed incorrect category id.",e);
		}
		return result;
	}
	/**
	 * @return the getSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getGetSiteTypeUtil() {
		return mGetSiteTypeUtil;
	}
	/**
	 * @param pGetSiteTypeUtil the getSiteTypeUtil to set
	 */
	public void setGetSiteTypeUtil(TRUGetSiteTypeUtil pGetSiteTypeUtil) {
		mGetSiteTypeUtil = pGetSiteTypeUtil;
	}
	/**
	 * @return the mProductEndecaUtil
	 */
	public TRUProductEndecaUtil<E> getProductEndecaUtil() {
		return mProductEndecaUtil;
	}
	/**
	 * @param pProductEndecaUtil the mProductEndecaUtil to set
	 */
	public void setProductEndecaUtil(TRUProductEndecaUtil<E> pProductEndecaUtil) {
		this.mProductEndecaUtil = pProductEndecaUtil;
	}
/**
 * Get the skuIds
 * @param pSkuIds : SkuIds
 * @return productIdList :  productIdList
 */
	public List<String> getSkuIds(String pSkuIds) {
		if (isLoggingDebug()) {
			logDebug("BEGIN: @method getSkuIds() @class: PopulateBeanFromContentItem" +pSkuIds);
		}
		List<String> productIdList = new ArrayList<String>();
		String[] productIdArray;
		productIdArray = pSkuIds.split(TRUCommerceConstants.SPLIT_PIPE);
		productIdList = Arrays.asList(productIdArray);
		if (isLoggingDebug()) {
			logDebug("END: @method getSkuIds() @class: PopulateBeanFromContentItem");
		}
		return productIdList;
	}

/**
 * @return the endecaPriceUtils
 */
public TRUEndecaPriceUtils getEndecaPriceUtils() {
	return mEndecaPriceUtils;
}

/**
 * @param pEndecaPriceUtils the endecaPriceUtils to set
 */
public void setEndecaPriceUtils(TRUEndecaPriceUtils pEndecaPriceUtils) {
	mEndecaPriceUtils = pEndecaPriceUtils;
}
	
}
