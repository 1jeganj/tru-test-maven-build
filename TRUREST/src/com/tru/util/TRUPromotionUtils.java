package com.tru.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.tru.commerce.claimable.TRUClaimableTools;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.common.promotion.Coupon;
import com.tru.common.promotion.PromotionStatus;
import com.tru.rest.constants.TRURestConstants;

/**
 * This  class is utility class for promotions.  
 * @version 1.0
 * @author Professional Access
 * 
 */
public class TRUPromotionUtils extends GenericService{

	/**property to hold promotionTools */
	private TRUPromotionTools mPromotionTools;

	/**property to hold mClaimableTools */
	private TRUClaimableTools mClaimableTools;
	
	/**property to hold mCouponCode */
	private boolean mCouponCode;
	
	  /**
	 * @return the mCouponCode
	 */
	public boolean isCouponCode() {
		return mCouponCode;
	}

	/**
	 * @param pCouponCode the mCouponCode to set
	 */
	public void setCouponCode(boolean pCouponCode) {
		mCouponCode = pCouponCode;
	}

	/**
	 * @return the promotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the promotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}
	
	
	/**
	 * @return the mClaimableTools
	 */
	public TRUClaimableTools getClaimableTools() {
		return mClaimableTools;
	}

	/**
	 * @param pClaimableTools the mClaimableTools to set
	 */
	public void setClaimableTools(TRUClaimableTools pClaimableTools) {
		this.mClaimableTools = pClaimableTools;
	}

	/**
	 * This method is used to get the Coupon repository items associated to the Promotion.
	 * 
	 * @param pProfile : Profile 
	 * @param pOrder : Order
	 * @param pIsAppliedCoupon  - AppliedCoupen
	 * @return List<PromotionStatus> : List of PromotionStatus items
	 */
@SuppressWarnings("unchecked")
public List<PromotionStatus> getAppliedPromotions(Profile pProfile,TRUOrderImpl pOrder, boolean pIsAppliedCoupon){
	if (isLoggingDebug()) {
		logDebug("Entering into the TRUPromotionUtils.getAppliedPromotions method");
	}
	List<RepositoryItem> activePromotions = (List<RepositoryItem>) pProfile.getPropertyValue(getPromotionTools().getActivePromotionsProperty());
	List<PromotionStatus> filteredActivePromotions=new ArrayList<PromotionStatus>();
	Set<String> validCouponSet=new HashSet<String>();
	Map<String, String> promotionDetailsMap= null;
	Map<String, String> couponMap=null;
	if(pIsAppliedCoupon){
		couponMap=pOrder.getSingleUseCoupon();
		promotionDetailsMap=pOrder.getAppliedPromoNames();
	}else{
		couponMap=pOrder.getNotAppliedCouponCode();
		promotionDetailsMap=pOrder.getNotAppliedPromoNames();
	}
	if(couponMap!=null && !couponMap.isEmpty()){
		for(Map.Entry<String,String> entry : couponMap.entrySet()){
			validCouponSet.add(entry.getValue());
		}
	}		
// Take its active promotions and iterate over them.
	if (activePromotions != null && !activePromotions.isEmpty()) {
		List<RepositoryItem> coupons = null;
		List<Coupon> filteredCoupons = null;
		PromotionStatus filteredPromotionStatus=null;
		RepositoryItem promotion=null;
		TRUPricingModelProperties pricingModelProperties= (TRUPricingModelProperties)getPromotionTools().getPricingModelProperties();
		for (RepositoryItem promotionStatus : activePromotions) {
			// Each promotionStatus contains a list of coupons that
			// activated a promotion. Inspect these coupons.
			filteredPromotionStatus =new PromotionStatus();
			String description=null;
			String promotionName = null;
			if (promotionStatus != null) {
				coupons = (List<RepositoryItem>) promotionStatus.getPropertyValue(getPromotionTools().getPromoStatusCouponsPropertyName());
				promotion = (RepositoryItem)promotionStatus.getPropertyValue(pricingModelProperties.getPromotionPropertyName());
					if (promotion != null) {
					description = (String)promotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
					promotionName = (String)promotion.getPropertyValue(pricingModelProperties.getDisplayName());
				}
				if(StringUtils.isNotBlank(description)){
					filteredPromotionStatus.setDescription(description);
				}
				if(StringUtils.isNotBlank(promotionName)){
					filteredPromotionStatus.setPromotionName(promotionName);
				}
			}
			if (coupons != null && !coupons.isEmpty()) {
				filteredCoupons= new ArrayList<Coupon>();
				Coupon filteredCoupon=null;
				for (RepositoryItem coupon : coupons) {
					if(coupon!=null){
						String couponId = (String) coupon.getPropertyValue(getClaimableTools().getIdPropertyName());
						if(pIsAppliedCoupon && StringUtils.isNotBlank(couponId) && validCouponSet.contains(couponId) && StringUtils.isNotBlank(promotionName)&& promotionDetailsMap.containsKey(couponId) &&
								promotionDetailsMap.containsValue(promotionName)){
							String couponDisplayName = (String) coupon.getPropertyValue(getClaimableTools().getDerivedDisplayNamePropertyName());
							filteredCoupon=new Coupon();
							filteredCoupon.setCouponCode(couponId);
							filteredCoupon.setCouponName(couponDisplayName);
							//filteredActivePromotions.add(promotionStatus);
							filteredCoupons.add(filteredCoupon);
							
						} if(!pIsAppliedCoupon && StringUtils.isNotBlank(couponId) && validCouponSet.contains(couponId)){
							String couponDisplayName = (String) coupon.getPropertyValue(getClaimableTools().getDerivedDisplayNamePropertyName());
							filteredCoupon=new Coupon();
							filteredCoupon.setCouponCode(couponId);
							filteredCoupon.setCouponName(couponDisplayName);
							//filteredActivePromotions.add(promotionStatus);
							filteredCoupons.add(filteredCoupon);
						}
					}
				}
				filteredPromotionStatus.setCoupons(filteredCoupons);
			}
			if(!pIsAppliedCoupon && StringUtils.isNotBlank(promotionName)&& promotionDetailsMap!=null && promotionDetailsMap.containsValue(promotionName)){
                for(Map.Entry entry:promotionDetailsMap.entrySet()){
                       if(promotionName.equals((String)entry.getValue())){
                    	   filteredCoupons= new ArrayList<Coupon>();
                           Coupon filteredCoupon=null;
                           filteredCoupon=new Coupon();
                          filteredCoupon.setCouponCode((String)entry.getKey());
                          filteredCoupon.setCouponName(promotionName);
                          //filteredActivePromotions.add(promotionStatus);
                          filteredCoupons.add(filteredCoupon);
                          filteredPromotionStatus.setCoupons(filteredCoupons);
                       }
                }
          }
			filteredActivePromotions.add(filteredPromotionStatus);
		}
	}
	if (isLoggingDebug()) {
		logDebug("Exit from the TRUPromotionUtils.getAppliedPromotions method");
	}
	return filteredActivePromotions;
	
}
/**
 *  This method fetches item level,order promotions for order confirm page.
 * @param pOrder : Order
 * @return List of Promotions.
 */
 public List<PromotionStatus> fetchLastOrderPromotions(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUPromotionUtils.fetchLastOrderPromotions method");
		}
        List<PromotionStatus> orderPromoList = new ArrayList<PromotionStatus>();
        List<PromotionStatus> itemPromoList = new ArrayList<PromotionStatus>();
        List<PromotionStatus> shippingPromoList = new ArrayList<PromotionStatus>();
        List<PromotionStatus> allAppliedPromoList = new ArrayList<PromotionStatus>();
		if (pOrder != null) {
			orderPromoList = getOrderLevelPromo(pOrder);
			if (orderPromoList != null && !orderPromoList.isEmpty()) {
				allAppliedPromoList.addAll(orderPromoList);
			}
			itemPromoList = getItemLevelPromo(pOrder);
			if (itemPromoList != null && !itemPromoList.isEmpty() && allAppliedPromoList != null) {
				allAppliedPromoList.addAll(itemPromoList);
			}
			shippingPromoList = getShippingGroupPromo(pOrder);
			if (shippingPromoList != null && !shippingPromoList.isEmpty() && allAppliedPromoList != null) {
				allAppliedPromoList.addAll(shippingPromoList);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from the TRUPromotionUtils.fetchLastOrderPromotions method");
		}
		return allAppliedPromoList;
	}
 
 /**
  * This method return all the applied orderPromotion  
  * @param pOrder : Order
  * @return appliedPromoList the list of orderPromotion list
  */
 @SuppressWarnings("unchecked")
public List<PromotionStatus> getOrderLevelPromo(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUPromotionUtils.getOrderLevelPromo method");
		}
		List<PricingAdjustment> orderAdjustments = null;
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) order.getPriceInfo();
		if (null != orderPriceInfo) {
			orderAdjustments = orderPriceInfo.getAdjustments();	
		}
		List<PromotionStatus> appliedPromoList = new ArrayList<PromotionStatus>();
		Set<String> validCouponSet = new HashSet<String>();
		Map<String, String> promotionDetailsMap = null;
		Map<String, String> couponMap = null;
		couponMap = order.getSingleUseCoupon();
		promotionDetailsMap = order.getAppliedPromoNames();
		if (couponMap != null && !couponMap.isEmpty()) {
			for (Map.Entry<String, String> entry : couponMap.entrySet()) {
				validCouponSet.add(entry.getValue());
			}
		}
     if(orderAdjustments != null && !orderAdjustments.isEmpty()) {
            RepositoryItem orderPromotion = null;
        	RepositoryItem coupon = null;
            for(PricingAdjustment orderAdjustment : orderAdjustments) {
                  orderPromotion = orderAdjustment.getPricingModel();
                  if(orderPromotion == null){
                         continue;
                  }
                  PromotionStatus appliedOrderPromotionStatus = new PromotionStatus();
                  TRUPricingModelProperties pricingModelProperties= (TRUPricingModelProperties)getPromotionTools().getPricingModelProperties();
                  String description = null;
                  String displayName = null;
				if (orderPromotion != null) {
					description = (String) orderPromotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
					displayName = (String) orderPromotion.getPropertyValue(pricingModelProperties.getDisplayName());
					appliedOrderPromotionStatus.setCouponType(TRURestConstants.ORDER_DISCOUNT);
				}
				if (StringUtils.isNotBlank(description)) {
					appliedOrderPromotionStatus.setDescription(description);
				}
				if (StringUtils.isNotBlank(displayName)) {
					appliedOrderPromotionStatus.setPromotionName(displayName);
				}
				if (null != orderAdjustment.getCoupon()) {
					coupon = orderAdjustment.getCoupon();
				}
				if (coupon != null) {
							String couponId = (String) coupon.getPropertyValue(getClaimableTools().getIdPropertyName());
							if(StringUtils.isNotBlank(couponId) && validCouponSet.contains(couponId) && StringUtils.isNotBlank(displayName)&& promotionDetailsMap.containsKey(couponId) &&
									promotionDetailsMap.containsValue(displayName)) {
								appliedOrderPromotionStatus.setAppliedCoupon(couponId);
							}
				}
				appliedPromoList.add(appliedOrderPromotionStatus);
			}
		}
     if (isLoggingDebug()) {
			logDebug("Exit from the TRUPromotionUtils.getOrderLevelPromo method");
		}
	return appliedPromoList;
 }
 
/**
 * This method return all the applied itemPromotion  
 * @param pOrder : Order
 * @return appliedPromoList the list of itemLevel promotions
 */
 @SuppressWarnings("unchecked")
public List<PromotionStatus> getItemLevelPromo(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUPromotionUtils.getItemLevelPromo method");
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		final List<CommerceItem> commerceItems = order.getCommerceItems();
		if (commerceItems == null || commerceItems.isEmpty()) {
			return null;
		}
		TRUItemPriceInfo itemPriceInfo = null;
		List<PricingAdjustment> adjustments = null;
		List<PromotionStatus> appliedPromoList = new ArrayList<PromotionStatus>();
		Set<String> validCouponSet = new HashSet<String>();
		Map<String, String> promotionDetailsMap = null;
		Map<String, String> couponMap = null;
		couponMap = order.getSingleUseCoupon();
		promotionDetailsMap = order.getAppliedPromoNames();
		if (couponMap != null && !couponMap.isEmpty()) {
			for (Map.Entry<String, String> entry : couponMap.entrySet()) {
				validCouponSet.add(entry.getValue());
			}
		}
		for (CommerceItem cItem : commerceItems) {
			if (cItem.getPriceInfo() != null) {
				itemPriceInfo = (TRUItemPriceInfo) cItem.getPriceInfo();
				if (itemPriceInfo != null) {
					adjustments = itemPriceInfo.getAdjustments();
				}
				if (adjustments == null || adjustments.isEmpty()) {
					continue;
				}
			RepositoryItem promotion = null;
			RepositoryItem coupon = null;
			for (PricingAdjustment priceAdj : adjustments) {
				promotion = priceAdj.getPricingModel();
				if (promotion == null) {
					continue;
				}
				PromotionStatus appliedItemPromotion = new PromotionStatus();
				TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools().getPricingModelProperties();
				String description = null;
				String displayName = null;
				if (promotion != null) {
					description = (String) promotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
					displayName = (String) promotion.getPropertyValue(pricingModelProperties.getDisplayName());
					appliedItemPromotion.setCouponType(TRURestConstants.ITEM_DISCOUNT);
				}
				if (null != priceAdj.getCoupon()) {
					coupon = priceAdj.getCoupon();
				}
				if (StringUtils.isNotBlank(description)) {
					appliedItemPromotion.setDescription(description);
				}
				if (StringUtils.isNotBlank(displayName)) {
					appliedItemPromotion.setPromotionName(displayName);
				}
				if (coupon != null) {
							String couponId = (String) coupon.getPropertyValue(getClaimableTools().getIdPropertyName());
							if (StringUtils.isNotBlank(couponId) && validCouponSet.contains(couponId) && StringUtils.isNotBlank(displayName) && promotionDetailsMap.containsKey(couponId) && promotionDetailsMap.containsValue(displayName)) {
								appliedItemPromotion.setAppliedCoupon(couponId);
							}
					}
					appliedPromoList.add(appliedItemPromotion);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from the TRUPromotionUtils.getItemLevelPromo method");
		}
		return appliedPromoList;
	}

 /**
  * This method return all the applied shippingGroup  
  * @param pOrder : Order
  * @return appliedPromoList the list of itemLevel promotions
  */
  @SuppressWarnings("unchecked")
 public List<PromotionStatus> getShippingGroupPromo(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUPromotionUtils.getShippingGroupPromo method");
		}
 		TRUOrderImpl order = (TRUOrderImpl) pOrder;
 		final List<ShippingGroup> shippigGroupList = order.getShippingGroups();
		if (shippigGroupList == null || shippigGroupList.isEmpty()) {
			return null;
		}
 		TRUShippingPriceInfo itemPriceInfo = null;
 		List<PromotionStatus> appliedPromoList = new ArrayList<PromotionStatus>();
 		Set<String> validCouponSet = new HashSet<String>();
 		Map<String, String> promotionDetailsMap = null;
 		Map<String, String> couponMap = null;
 		couponMap = order.getSingleUseCoupon();
 		promotionDetailsMap = order.getAppliedPromoNames();
 		if (couponMap != null && !couponMap.isEmpty()) {
 			for (Map.Entry<String, String> entry : couponMap.entrySet()) {
 				validCouponSet.add(entry.getValue());
 			}
 		}
 		for (ShippingGroup shippingGroup : shippigGroupList) {
 			itemPriceInfo = (TRUShippingPriceInfo) shippingGroup.getPriceInfo();
 			List<PricingAdjustment> adjustments = itemPriceInfo.getAdjustments();
 			if (adjustments == null || adjustments.isEmpty()) {
 				continue;
 			}
 			RepositoryItem promotion = null;
 			RepositoryItem coupon = null;
 			for (PricingAdjustment priceAdj : adjustments) {
 				promotion = priceAdj.getPricingModel();
 				if (promotion == null) {
 					continue;
 				}
 				PromotionStatus appliedItemPromotion = new PromotionStatus();
 				TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getPromotionTools().getPricingModelProperties();
 				String description = null;
 				String displayName = null;
 				if (promotion != null) {
 					description = (String) promotion.getPropertyValue(pricingModelProperties.getPromotionDetailsPropertyName());
 					displayName = (String) promotion.getPropertyValue(pricingModelProperties.getDisplayName());
 					appliedItemPromotion.setCouponType(TRURestConstants.SHIPPING_DISCOUNT);
 				}
 				if (null != priceAdj.getCoupon()) {
 					coupon = priceAdj.getCoupon();
 				}
 				if (StringUtils.isNotBlank(description)) {
 					appliedItemPromotion.setDescription(description);
 				}
 				if (StringUtils.isNotBlank(displayName)) {
 					appliedItemPromotion.setPromotionName(displayName);
 				}
 				if (coupon != null) {
 							String couponId = (String) coupon.getPropertyValue(getClaimableTools().getIdPropertyName());
 							if (StringUtils.isNotBlank(couponId) && validCouponSet.contains(couponId) && StringUtils.isNotBlank(displayName) && promotionDetailsMap.containsKey(couponId) && promotionDetailsMap.containsValue(displayName)) {
 								appliedItemPromotion.setAppliedCoupon(couponId);
 							}
 				}
 				appliedPromoList.add(appliedItemPromotion);
 			}
 		}
 		if (isLoggingDebug()) {
			logDebug("Exist from the TRUPromotionUtils.getShippingGroupPromo method");
		}
		return appliedPromoList;
	}

}
