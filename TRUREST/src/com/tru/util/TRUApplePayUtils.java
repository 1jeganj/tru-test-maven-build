package com.tru.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.CommerceException;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.purchase.TRUShippingProcessHelper;
import com.tru.common.ApplePayOrderInfo;
import com.tru.common.Items;
import com.tru.common.TRUConstants;
import com.tru.rest.constants.TRURestConstants;



/**
 * This  class is utility class for apple pay.  
 * @version 1.0
 * @author Professional Access
 * 
 */
public class TRUApplePayUtils extends GenericService{
	
	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mCartModifierHelper;
	/** property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	/**
	 * Holds the mCatalogTools
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * property to hold mShippingManager.
	 */
	private TRUShippingManager mShippingManager;
	
	private TRUCommercePropertyManager mPropertyManager;

	/**
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}

	/**
	 * Gets the cart modifier helper.
	 * 
	 * @return the cart modifier helper
	 */
	public TRUCartModifierHelper getCartModifierHelper() {
		return mCartModifierHelper;
	}

	/**
	 * Sets the cart modifier helper.
	 * 
	 * @param pCartModifierHelper
	 *            the new cart modifier helper
	 */
	public void setCartModifierHelper(TRUCartModifierHelper pCartModifierHelper) {
		this.mCartModifierHelper = pCartModifierHelper;
	}
	/**
	 * @return the mCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * @param pCatalogTools
	 *            the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * The method will return whether the sku is applePayEligible or not.
	 * @param pSkuId - skuId
	 * @return applePayEligible - return boolean property whether sku is applePayEligible or not.
	 */
	public boolean getApplePayEligible(String pSkuId) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUApplePayUtils.getApplePayEligible method");
		}
		boolean applePayEligible = Boolean.FALSE;
		RepositoryItem skuItem = null;
		if (StringUtils.isNotBlank(pSkuId)) {
			skuItem = fetchSkuItem(pSkuId);
		}
		if (skuItem != null) {
			int inventoryStatusOnlineInt = TRUCommerceConstants.INT_ZERO;
			String inventoryStatusOnline = null;
			//fetch inventory status for s2h items
			try {
				inventoryStatusOnlineInt = getCatalogTools().getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId);
				if(inventoryStatusOnlineInt == TRUCommerceConstants.INT_IN_STOCK || inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
						inventoryStatusOnline = TRUCommerceConstants.IN_STOCK;
				} else if(inventoryStatusOnlineInt == TRUCommerceConstants.INT_OUT_OF_STOCK) {
						inventoryStatusOnline = TRUCommerceConstants.OUT_OF_STOCK;
				}
			} catch (InventoryException inventoryException) {
				if (isLoggingError()) {
					logError(
							"InventoryException in TRUCheckProductEligibilityDroplet::getInventoryStatus :",inventoryException);
				}
			}
			//if item is out of stock, return applePayEligible as false.
			if(inventoryStatusOnline.equalsIgnoreCase(TRUCommerceConstants.OUT_OF_STOCK)){
				return applePayEligible;
			}else if(inventoryStatusOnline.equalsIgnoreCase(TRUCommerceConstants.IN_STOCK)){
				applePayEligible = Boolean.TRUE;
			}	
		} 
		if (isLoggingDebug()) {
			logDebug("Exiting from  TRUApplePayUtils.getApplePayEligible method");
		}
	 return applePayEligible;
	}
	/**
	 * The method will return whether the sku is eligible for registry or not.
	 * @param pOrder - Order
	 * @return isRegistryEligibility - return boolean property the sku is eligible for registry or not.
	 */
	public boolean isRegistryEligibility(Order pOrder) {
		boolean isRegistryEligibility = Boolean.FALSE;
		if (pOrder.getShippingGroups()!= null && !pOrder.getShippingGroups().isEmpty()) {
			@SuppressWarnings("unchecked")
			List<ShippingGroup> shippingGroups = (List<ShippingGroup>) pOrder.getShippingGroups();
			for (ShippingGroup orderSG : shippingGroups) {
				if (orderSG instanceof TRUChannelHardgoodShippingGroup) {	
					TRUChannelHardgoodShippingGroup hgSg = (TRUChannelHardgoodShippingGroup) orderSG;
						if(StringUtils.isNotBlank(hgSg.getChannelUserName()) && StringUtils.isNotBlank(hgSg.getChannelId())) {
							isRegistryEligibility = Boolean.TRUE;
							break;
						}
				}
			}
		}
		return isRegistryEligibility;
	}
	/**
	 * The method will return whether the Order is eligible for ApplePay or not.
	 * @param pOrder - current Order
	 * @return applePayEligible - return boolean property whether the Order is eligible for ApplePay or not.
	 */
	@SuppressWarnings("unchecked")
	public boolean getApplePayCartEligible(Order pOrder) {
		boolean applePayEligible = Boolean.FALSE;
			if(null != pOrder && !isRegistryEligibility(pOrder) && !isGiftCardPayment(pOrder)) {
				final List<CommerceItem> commerceItems = pOrder.getCommerceItems();
				if (!commerceItems.isEmpty()) {
					for (CommerceItem commerceItem : commerceItems) {
						if (commerceItem instanceof TRUGiftWrapCommerceItem || commerceItem instanceof TRUDonationCommerceItem) {
							applePayEligible = Boolean.FALSE;
							break;
						}
						applePayEligible = getApplePayEligible(commerceItem.getCatalogRefId());
						if(applePayEligible && !isCollectionProduct(commerceItem.getCatalogRefId())) {
							continue;
						}else {
								applePayEligible = Boolean.FALSE;
								break;
						}
					}
				}
				if(applePayEligible && isCommonShippingMethods(pOrder)) {
					applePayEligible = Boolean.TRUE;
				}
			}
		return applePayEligible;
	}
	
	/**
	 * The method will return whether it is CollectionProduct or not.
	 * @param pSkuId - sku repository item
	 * @return isCollectionProduct - return boolean property whether it is CollectionProduct or not.
	 */
	@SuppressWarnings("unchecked")
	public boolean isCollectionProduct(String pSkuId) {
		boolean isCollectionProduct = Boolean.FALSE;
		RepositoryItem skuItem = null;
		Map<String,String> collectionNames=null;
		skuItem = fetchSkuItem(pSkuId);
		if(skuItem!= null && skuItem.getPropertyValue(getCatalogProperties().getOnlineCollectionName()) != null) {
			collectionNames = (Map<String,String>)skuItem.getPropertyValue(getCatalogProperties().getOnlineCollectionName());
				if(!collectionNames.isEmpty()) {
					isCollectionProduct = Boolean.TRUE;
			}
		}
		return isCollectionProduct;
	}
	
	/**
	 * The method will return if Order has common shipping methods or not.
	 * @param pOrder - Order
	 * @return isCommonShippingMethods - return boolean property if Order has common shipping methods or not.
	 */
	public boolean isCommonShippingMethods(Order pOrder) {
		boolean isCommonShippingMethods = Boolean.FALSE;
		Set<String> freightClasseSet = getShippingManager().getUniqueFreightClasses(pOrder);
		if(freightClasseSet==null || freightClasseSet.isEmpty()){
			if(isLoggingError()){
				vlogError("pFreightClassesSet in null for the order id {0}",pOrder.getId());
			}
			return isCommonShippingMethods;
		}
		if(freightClasseSet.size() == TRUConstants.ONE) {
			isCommonShippingMethods = Boolean.TRUE;
		} else {
			RepositoryItem[] commonShipMethods = getShippingManager().getShippingTools().getShipMethodsByFreights(freightClasseSet);
			if(commonShipMethods == null || commonShipMethods.length == TRUConstants.ZERO) {

				if(isLoggingError()){
					vlogError("commonShipMethods in null for the order id {0}",pOrder.getId());
				}
				return isCommonShippingMethods;
			} else if(commonShipMethods.length > TRUConstants.ZERO) {
				isCommonShippingMethods = Boolean.TRUE;
			}
		}
		return isCommonShippingMethods;	
	}
	
	/**
	 * The method will return if Order has Gift Card as payment or not.
	 * @param pOrder - Order
	 * @return isGiftCardPayment - return boolean property if Order has Gift Card as payment or not.
	 */
	public boolean isGiftCardPayment(Order pOrder) {
	 boolean isGiftCardPayment = Boolean.FALSE;
		if (pOrder.getPaymentGroups()!= null && !pOrder.getPaymentGroups().isEmpty()) {
			@SuppressWarnings("unchecked")
			List<PaymentGroup> paymentGroups = (List<PaymentGroup>)pOrder.getPaymentGroups();
			for (PaymentGroup orderPG:paymentGroups) {
				if(orderPG instanceof TRUGiftCard) {
					isGiftCardPayment = Boolean.TRUE;
					break;
				}
			}
		}
	 return isGiftCardPayment; 	
	}
	
	/**
	 * Gets the restriction error.
	 * @param pRestrictionsMap the restriction map
	 * @param pSkuId skuId
	 * @param pZipCode zipCode
	 */
	public void getRestrictionError(Map<String, String> pRestrictionsMap, String pSkuId, String pZipCode) {
		String errorMsg=getShippingManager().getShippingTools().checkSKUZipCodeRestriction(pSkuId, pZipCode);
		if(StringUtils.isBlank(errorMsg)) {
			return;
		}
		RepositoryItem skuItem = null;
		String displayName=TRUConstants.EMPTY;
		skuItem = fetchSkuItem(pSkuId);
			if(skuItem!= null) {
				displayName = (String)skuItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
			}
		if(StringUtils.isNotBlank(errorMsg)){
			pRestrictionsMap.put(pSkuId,displayName+TRUConstants.WHITE_SPACE+errorMsg);
		}
	}
	/**
	 *	This method checks the item level shipping restrictions to which zipcode is not allowed.
	 * @param pOrder order
	 * @param pZipCode zipCode
	 * @param pRestrictionsMap pRestrictionsMap
	 */
	@SuppressWarnings("unchecked")
	public void validateZipCodeRestriction(Order pOrder, String pZipCode, Map<String, String> pRestrictionsMap){		
		final List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if (!commerceItems.isEmpty()) {
			for (CommerceItem commerceItem : commerceItems) {
				getRestrictionError(pRestrictionsMap, commerceItem.getCatalogRefId(), pZipCode);
			}
		}
	}
	/**
	 *	This method will return skuItem.
	 * @param pSkuId skuId
	 * @return the repository item.
	 */
	private RepositoryItem fetchSkuItem(String pSkuId) {
		RepositoryItem skuItem = null;
		try {
			skuItem = getCartModifierHelper().findSkuItem(pSkuId);
			
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		return skuItem;
	}
		
	/**
	 * property: Reference to the ShippingProcessHelper component.
	 */
	private TRUShippingProcessHelper mShippingHelper;
	/**
	 * Gets the shipping helper.
	 *
	 * @return the shippingHelper
	 */
	public TRUShippingProcessHelper getShippingHelper() {
		return mShippingHelper;
	}

	/**
	 * Sets the shipping helper.
	 *
	 * @param pShippingHelper the shippingHelper to set
	 */
	public void setShippingHelper(TRUShippingProcessHelper pShippingHelper) {
		mShippingHelper = pShippingHelper;
	}
	
	
	/**
	 * Populate apple pay order info vo.
	 *
	 * @param pOrder the order
	 * @param pApplePayOrderInfo the apple pay order info
	 * @return the apple pay order info
	 */
	public ApplePayOrderInfo populateApplePayOrderInfoVO(TRUOrderImpl pOrder, ApplePayOrderInfo pApplePayOrderInfo) {

		if (isLoggingDebug()) {
			logDebug("START :: TRUApplePayUtils ::  populateApplePayOrderInfoVO");
		}
		TRUCommercePropertyManager  propertyManager = getPropertyManager();
		Map<String, String> couponCodes = null;
		List<String> couponCodesList = new ArrayList<String>();
		if(pOrder.getSingleUseCoupon() !=null){
			couponCodes = pOrder.getSingleUseCoupon();
			if (null != couponCodes && !couponCodes.isEmpty()) {
				for (Map.Entry<String, String> entrySet : couponCodes.entrySet()) {
					if (StringUtils.isNotBlank(entrySet.getKey())) {
						couponCodesList.add(entrySet.getKey());
					}
				}
			}
		}
		pApplePayOrderInfo.setCouponCodes(couponCodesList);
		pApplePayOrderInfo.setId(pOrder.getId());
		pApplePayOrderInfo.setShippingTotal(pOrder.getPriceInfo().getShipping());
		pApplePayOrderInfo.setTaxTotal(pOrder.getPriceInfo().getTax());
		pApplePayOrderInfo.setCustomerBillingCity(pOrder.getBillingAddress().getCity());
		pApplePayOrderInfo.setCustomerBillingPostalCode(pOrder.getBillingAddress().getPostalCode());
		pApplePayOrderInfo.setDiscountAmount(pOrder.getPriceInfo().getDiscountAmount());
		pApplePayOrderInfo.setCustomerBillingState(pOrder.getBillingAddress().getState());
		pApplePayOrderInfo.setPaymentType(TRURestConstants.APPLE_PAY);
		pApplePayOrderInfo.setSubTotal(pOrder.getPriceInfo().getRawSubtotal());
		pApplePayOrderInfo.setTotal(pOrder.getPriceInfo().getTotal());
		pApplePayOrderInfo.setCurrencyCode(pOrder.getPriceInfo().getCurrencyCode());
		
		
		List<Items> itemsVo = new ArrayList<Items>();
		try {
			List sgCiRels = getShippingHelper().getPurchaseProcessHelper().getShippingGroupManager().getAllShippingGroupRelationships(pOrder);
			if (sgCiRels != null && !sgCiRels.isEmpty()) {
				final int sgCiRelsCount = sgCiRels.size();
				for (int i = 0; i < sgCiRelsCount; ++i) {
					final TRUShippingGroupCommerceItemRelationship sgCiRel = (TRUShippingGroupCommerceItemRelationship) sgCiRels.get(i);
					final CommerceItem commerceItem = sgCiRel.getCommerceItem();
					Items items = new Items();
					RepositoryItem skuItem = getSkuItem(propertyManager, commerceItem);
					items.setProductId(((TRUCommerceItemImpl) commerceItem).getAuxiliaryData().getProductId());
					items.setPrice(((TRUCommerceItemImpl) commerceItem).getPriceInfo().getListPrice());
					items.setProductName((String)skuItem.getPropertyValue(propertyManager.getDisplayName()) );
					items.setQuantity(((TRUCommerceItemImpl) commerceItem).getQuantity());
					items.setSalePrice(((TRUCommerceItemImpl) commerceItem).getPriceInfo().getSalePrice());
					items.setSavedAmount(((TRUCommerceItemImpl) commerceItem).getPriceInfo().getOrderDiscountShare());
					itemsVo.add(items);
				}
			}
		} catch (CommerceException commerceException) {
			vlogError(commerceException, "CommerceException in @method: populateVO");
		} catch (RepositoryException repositoryException) {
			
			vlogError(repositoryException, "repositoryException in @method: populateVO");
		}
		pApplePayOrderInfo.setItems(itemsVo);

		if (isLoggingDebug()) {
			logDebug("END :: TRUApplePayUtils ::  populateApplePayOrderInfoVO");
		}
		return pApplePayOrderInfo;

	}
	

	/**
	 * Gets the sku item.
	 *
	 * @param pPropertyManager the property manager
	 * @param pCommerceItem the commerce item
	 * @return the sku item
	 * @throws RepositoryException the repository exception
	 */
	private RepositoryItem getSkuItem(TRUCommercePropertyManager pPropertyManager, final CommerceItem pCommerceItem) throws RepositoryException {
		RepositoryItem skuItem = null;
		String skuId = (String) ((CommerceItemImpl) pCommerceItem).getPropertyValue(pPropertyManager.getCatalogRefIdPropertyName());
		if (!StringUtils.isBlank(skuId)) {
			skuItem = getCartModifierHelper().findSkuItem(skuId);
		}
		return skuItem;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager the mPropertyManager to set
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		this.mPropertyManager = pPropertyManager;
	}
	
	
	
	
		
}
