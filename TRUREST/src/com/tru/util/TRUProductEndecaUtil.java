package com.tru.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimValIdList;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.ERecSearch;
import com.endeca.navigation.ERecSearchList;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.PropertyMap;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.commerce.locations.TRUStoreLocatorTools;
import com.tru.common.RegistryProductInfoVO;
import com.tru.common.RegistrySKUInfoVO;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.assembler.TRUIndexingConfiguration;
import com.tru.endeca.filter.TRUPriceExistsFilterBuilder;
import com.tru.rest.constants.TRURestConstants;
import com.tru.rest.constants.TRUSearchMode;
import com.tru.storelocator.tol.PARALStoreLocatorTools;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This class extends the GenericService class. 
 * This class contains series of helper methods.
 * 
 * @author Professional Access
 * @version 1.0
 * @param <E> the type of elements in this collection
 * 
 */
public class TRUProductEndecaUtil<E> extends GenericService {
	
	/**
	 * This property hold reference for mRestConfiguration.
	 */
	private TRURestConfiguration mRestConfiguration;
	
	/**
	 * This property hold reference for mCoherenceInventoryManager.
	 */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	
	/**
	 * Holds the mSiteUtil
	 */
	private TRUGetSiteTypeUtil mSiteUtil;
	
	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;
	
	/** Holds mdexRequest property of MdexRequest. */
	private MdexRequest mMdexRequest;
	
	/** Holds property of mRecordSize. */
	private int mRecordSize;
	
	/** Holds mPriceExistsFilterBuilder property of mPriceExistsFilterBuilder. */
	private TRUPriceExistsFilterBuilder mPriceExistsFilterBuilder;
	
	/** The Property manager. */
	private TRUPropertyManager mPropertyManager;
	
	/** The Property mPriceFilter. */
	private String mPriceFilter;
	
	/** The Property mEnablePriceFilter. */
	private boolean mEnablePriceFilter;
	
	/**
	 * Gets the property manager.
	 * 
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 *  Holds the mStoreLocatorTools.
	 */
	private PARALStoreLocatorTools mStoreLocatorTools;

	/**
	 * This method should set inventory for all the skuCode.
	 * 
	 * @param pCatalogRefIds
	 *            CatalogRefIds
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 * @return inventoryInfoMap inventoryInfoMap
	 */
	     public Map<String, TRUInventoryInfo> prepareInventory(List<String> pCatalogRefIds) throws SiteContextException {
	            if (isLoggingDebug()) {
	                   logDebug("BEGIN:: TRUProductEndecaUtil.prepareInventory method.. ");
	            }
	            if (PerformanceMonitor.isEnabled()) {
	                   PerformanceMonitor.startOperation(TRURestConstants.START_INVENTORY_CALL);
	            }
	            Map<String,TRUInventoryInfo> inventoryInfoMap = getCoherenceInventoryManager().queryBulkS2HStockLevelMap(pCatalogRefIds); 
	            if (isLoggingDebug()) {
	                   logDebug("END:: TRUProductEndecaUtil.:::::prepareInventory method");
	            }
	            if (PerformanceMonitor.isEnabled()) {
	                   PerformanceMonitor.startOperation(TRURestConstants.END_INVENTORY_CALL);
	            }
	            return inventoryInfoMap;
	     }

	     /**
	 * This method should set inventory for all the skuCode.
	 * 
	 * @param pCatalogRefIds
	 *            CatalogRefIds
	 * @param pLocationId
	 *            -LocationId
	 * @param pWareHouseId
	 *            - WareHouseId
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 * @return inventoryInfoMap inventoryInfoMap
	 */
	 	     public Map<String, TRUInventoryInfo> prepareStoreInventory(List<String> pCatalogRefIds,String pLocationId,String pWareHouseId) throws SiteContextException {
	 	            if (isLoggingDebug()) {
	 	                   logDebug("BEGIN:: TRUProductEndecaUtil.prepareInventoryStore method.. ");
	 	            }
	 	            if (PerformanceMonitor.isEnabled()) {
	 	                   PerformanceMonitor.startOperation(TRURestConstants.START_INVENTORY_CALL);
	 	            }
	 	            Map<String,TRUInventoryInfo> storeInventoryInfoMap = getCoherenceInventoryManager().queryBulkStoreStockLevelMap(pCatalogRefIds, pLocationId, pWareHouseId); 
	 	            if (isLoggingDebug()) {
	 	                   logDebug("END:: TRUProductEndecaUtil.:::::prepareInventoryStore method");
	 	            }
	 	            if (PerformanceMonitor.isEnabled()) {
	 	                   PerformanceMonitor.startOperation(TRURestConstants.END_INVENTORY_CALL);
	 	            }
	 	            return storeInventoryInfoMap;
	 	     }
/**
 * This method will return both inventory status and online/store
 * @param pSkuId - SkuId
 * @param pStoreId - StoreId
 * @return storeInvetoryMap - storeInvetoryMap
 */
	public Map<String, TRUInventoryInfo> populateOnlineAndStoreInventory(String pSkuId, String pStoreId) {
		if (isLoggingDebug()) {
             logDebug("BEGIN:: TRUProductEndecaUtil.populateOnlineAndStoreInventory method.. ");
      }
		List<String> skuIdList = null;
		Map<String, TRUInventoryInfo> onlineInvetoryMap = null;
		Map<String, TRUInventoryInfo> storeInvetoryMap = null;
		Map<String, TRUInventoryInfo> onlineAndStoreInvetoryMap = new HashMap<String, TRUInventoryInfo>();
		List<String> queryStoreInvetory = null;
		TRUInventoryInfo inventory = null;
		String wareHouseId = null;
		if (pSkuId != null && !pSkuId.isEmpty()) {
			skuIdList = getSkuIds(pSkuId);	
		}
		try {
			if (skuIdList != null && !skuIdList.isEmpty()) {
				if (isLoggingDebug()) {
					vlogDebug("List of SkuIds to query onlineInventory:::::{0}", skuIdList);
				}
				queryStoreInvetory = new ArrayList<String>(skuIdList) ;
				onlineInvetoryMap = prepareInventory(skuIdList);
				if (onlineInvetoryMap != null && !onlineInvetoryMap.isEmpty()) {
					for (Map.Entry<String, TRUInventoryInfo> entry : onlineInvetoryMap.entrySet()) {
						 inventory = entry.getValue();
						 if (inventory.getAtcStatus() !=TRURestConstants.COHERENCE_OUT_OF_STOCK) { 
							queryStoreInvetory.remove(inventory.getCatalogRefId());	 
						 }
					}
				}
			}
			if (queryStoreInvetory != null && !queryStoreInvetory.isEmpty() && StringUtils.isNotEmpty(pStoreId)) {
				wareHouseId = getWareHouseId(pStoreId);
				if (isLoggingDebug()) {
					vlogDebug("List of SkuIds to query storeInvenory:::::{0}", queryStoreInvetory);
					vlogDebug("Selected storeId:::::{0}", pStoreId);
					vlogDebug("WareHouseId for the selected storeId:::::{0}", wareHouseId);
				}
				if (StringUtils.isNotEmpty(wareHouseId)) {
					storeInvetoryMap = prepareStoreInventory(queryStoreInvetory, pStoreId, wareHouseId);
				}
				if (isLoggingDebug()) {
					vlogDebug("SkuIds OutOfStock for the selected store:::::{0}", queryStoreInvetory);
				}
			}
		} catch (NullPointerException e) {
			if (isLoggingError()) {
				logError("NullPointerException throws while fetching the inventory", e);
			}
		} catch (UnsupportedOperationException e) {
			if (isLoggingError()) {
				logError("UnsupportedOperationException throws while fetching the inventory", e);
			}
		} catch (SiteContextException e) {
			if (isLoggingError()) {
				logError("SiteContextException throws while fetching the inventory", e);
			}
		}
		if (onlineInvetoryMap != null && !onlineInvetoryMap.isEmpty()) {
			onlineAndStoreInvetoryMap.putAll(onlineInvetoryMap);
		}
		if (storeInvetoryMap != null && !storeInvetoryMap.isEmpty()) {
			onlineAndStoreInvetoryMap.putAll(storeInvetoryMap);
		}
		if (isLoggingDebug()) {
             logDebug("END:: TRUProductEndecaUtil.:::::populateOnlineAndStoreInventory method");
      }
		return onlineAndStoreInvetoryMap;
	}
	
	/**
	 * 
	 * @param pRecList
	 *            - RecList
	 * @param pSkuIds
	 *            - SkuIds
	 * @param pSkuDetail
	 *            - SkuDetail
	 * @param pIsInventoryEnabled
	 *            - IsInventoryEnabled
	 * @return registryProductInfoVOList - list of productItems
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 */
	public List<RegistryProductInfoVO> populateSearchResult(ERecList pRecList, List<String> pSkuIds, String pSkuDetail, boolean pIsInventoryEnabled) throws SiteContextException{
		PropertyMap propertyMap = null;
		String productId = null;
		String siteId = null;
		AssocDimLocationsList dimValues = null;
		AssocDimLocations siteIdDimLocation = null;
		RegistryProductInfoVO registryProductInfoVO = null;
		Map<String, TRUInventoryInfo> inventoryInfoMap = null;
		List<RegistryProductInfoVO> registryProductInfoVOList = new ArrayList<RegistryProductInfoVO>();
		Map<String, RegistryProductInfoVO> prodInvoMap = new HashMap<String, RegistryProductInfoVO>();
		boolean skuDetails =Boolean.parseBoolean(pSkuDetail);
			if (pIsInventoryEnabled && pSkuIds != null && !pSkuIds.isEmpty() && pRecList != null && !pRecList.isEmpty()) {
				inventoryInfoMap = prepareInventory(pSkuIds);
				setRecordSize(pRecList.size());
			}
		for (Object result : pRecList) {
			 final ERec eRec = (ERec) result;
			 dimValues = eRec.getDimValues();
			 siteIdDimLocation = dimValues.getAssocDimLocations(getRestConfiguration().getSkuSiteId());
			 String currentSiteId = SiteContextManager.getCurrentSiteId();
			 propertyMap = eRec.getProperties();
				siteIdDimLocation = dimValues.getAssocDimLocations(getRestConfiguration().getSkuSiteId());
				 currentSiteId = SiteContextManager.getCurrentSiteId();
				if (siteIdDimLocation != null && !siteIdDimLocation.isEmpty() && siteIdDimLocation.size() > TRURestConstants.INT_ZERO) {
					@SuppressWarnings("rawtypes")
					Iterator iterator = siteIdDimLocation.listIterator();
						while (iterator.hasNext()) {
						DimLocation dimLoc = ((DimLocation)iterator.next());
						String dimLocSiteId = dimLoc.getDimValue().getName();
						if(StringUtils.isNotBlank(dimLocSiteId) && dimLocSiteId.contains(currentSiteId)) {
							siteId = currentSiteId;
							break;
							}
						}
						if (StringUtils.isBlank(siteId)) {
							DimLocation dimLoc = ((DimLocation) (siteIdDimLocation.get(TRURestConstants.INT_ZERO)));
							siteId = dimLoc.getDimValue().getName();
						}
					}
				productId = (String) propertyMap.get(getRestConfiguration().getProductRepositoryId().toString());
				if (prodInvoMap.containsKey(productId)) {
					registryProductInfoVO = prodInvoMap.get(productId);
					enrichSKU(false, inventoryInfoMap, registryProductInfoVO, propertyMap, siteId);
					continue;
				}
				registryProductInfoVO = new RegistryProductInfoVO();
				if (propertyMap.get(getRestConfiguration().getSkuDisplayName()) != null) {
					registryProductInfoVO.setDisplayName(propertyMap.get(getRestConfiguration().getSkuDisplayName()).toString());
				}
				if (propertyMap.get(getRestConfiguration().getProductRepositoryId()) != null) {
					productId = (String) propertyMap.get(getRestConfiguration().getProductRepositoryId().toString());
					registryProductInfoVO.setRusItemNumber(productId);
					prodInvoMap.put(productId, registryProductInfoVO);
				}
				if (skuDetails) {
					enrichSKU(true, inventoryInfoMap, registryProductInfoVO, propertyMap, siteId);		
					}
			registryProductInfoVOList.add(registryProductInfoVO);
		}
		return registryProductInfoVOList;
	}

	
/**
 * This method to create required skuProperties.
 *
 * @param pPropertyMap - PropertyMap
 * @return skuInfo return the skuVO Object
 */
	public RegistrySKUInfoVO createSKUInfo(PropertyMap pPropertyMap ) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.createSKUInfo method.. ");
		}
		RegistrySKUInfoVO skuInfo = null;
		Set<String> upcNumberSet = new HashSet<String>();
		String sticker = null;
		String ispu = null;
		String ispuString = null;
		PriceInfoVO priceDetails = null;
		if (pPropertyMap != null && !pPropertyMap.isEmpty()) {
			skuInfo = new RegistrySKUInfoVO();
			if(pPropertyMap.get(getRestConfiguration().getSkuRepositoryId())!=null) {
				skuInfo.setId(pPropertyMap.get(getRestConfiguration().getSkuRepositoryId()).toString());	
				}
			if (pPropertyMap.get(getRestConfiguration().getUnCartable()) != null) {
				skuInfo.setUnCartable(pPropertyMap.get(getRestConfiguration().getUnCartable()).toString().equals(TRURestConstants.ONE_STR));
			}
			if (pPropertyMap.get(getRestConfiguration().getRegistryEligibility()) != null) {
				skuInfo.setRegistryEligibility(pPropertyMap.get(getRestConfiguration().getRegistryEligibility()).toString().equals(TRURestConstants.ONE_STR));
			}
			if (pPropertyMap.get(getRestConfiguration().getPromotionalStickerDisplay()) != null) {
				sticker = (String) pPropertyMap.get(getRestConfiguration().getPromotionalStickerDisplay());
				skuInfo.setPromotionalStickerDisplay(sticker);
			}
			if (pPropertyMap.get(getRestConfiguration().getSkuReviewRating()) != null) {
				String reviewRating = (String) pPropertyMap.get(getRestConfiguration().getSkuReviewRating());
				if(StringUtils.isNotBlank(reviewRating)) {
					float rating = Float.parseFloat(reviewRating);
					int ratingInteger = (int) rating;
					skuInfo.setReviewRating(ratingInteger);
				}	
			}
			if (pPropertyMap.get(getRestConfiguration().getUpcNumbers()) != null) {
				upcNumberSet.add(pPropertyMap.get(getRestConfiguration().getUpcNumbers()).toString());
				skuInfo.setUpcNumbers(upcNumberSet);	
			}
			if(pPropertyMap.get(getRestConfiguration().getOriginalParentProduct())!=null) {
				skuInfo.setOriginalParentProduct(pPropertyMap.get(getRestConfiguration().getOriginalParentProduct()).toString());
			}
			priceDetails = new PriceInfoVO();
			if (pPropertyMap.get(getRestConfiguration().getSkuSalePrice()) != null) {
				priceDetails.setSalePrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getSkuSalePrice()).toString()));
			}
			if (pPropertyMap.get(getRestConfiguration().getSkuListPrice()) != null) {
				priceDetails.setListPrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getSkuListPrice()).toString()));
			}
			if (priceDetails.getSalePrice() == TRURestConstants.INT_ZERO && pPropertyMap.get(getRestConfiguration().getMinimumPrice()) != null) {
				priceDetails.setSalePrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getMinimumPrice()).toString()));
			}
			if (priceDetails.getListPrice() == TRURestConstants.INT_ZERO && pPropertyMap.get(getRestConfiguration().getMaximumPrice()) != null) {
				priceDetails.setListPrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getMaximumPrice()).toString()));
			}
			if (priceDetails.getSalePrice() == TRURestConstants.INT_ZERO && pPropertyMap.get(getRestConfiguration().getPriceAscendingSort()) != null) {
				priceDetails.setSalePrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getPriceAscendingSort()).toString()));	
			}
			if (priceDetails.getListPrice() == TRURestConstants.INT_ZERO && pPropertyMap.get(getRestConfiguration().getPriceDescendingSort()) != null) {
				priceDetails.setListPrice(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getPriceDescendingSort()).toString()));	
			}
			if (pPropertyMap.get(getRestConfiguration().getSavedAmount()) != null) {
				priceDetails.setSavedAmount(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getSavedAmount()).toString()));
			}
			if (pPropertyMap.get(getRestConfiguration().getSavedPercentage()) != null) {
				priceDetails.setSavedPercentage(Double.parseDouble(pPropertyMap.get(getRestConfiguration().getSavedPercentage()).toString()));
			}
			
			if (priceDetails != null) {
				skuInfo.setPriceInfo(priceDetails);
			}
			if (pPropertyMap.get(getRestConfiguration().getOnlinePID()) != null) {
				skuInfo.setOnlinePID((String)pPropertyMap.get(getRestConfiguration().getOnlinePID()).toString());
			}
			if (pPropertyMap.get(getRestConfiguration().getItemInStorePickUp()) != null) {
				 ispu = pPropertyMap.get(getRestConfiguration().getItemInStorePickUp()).toString();
				 ispuString = null;
				if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)) {
					ispuString = TRUCommerceConstants.YES;
				} else {
					ispuString = TRUCommerceConstants.NO;
				}
				skuInfo.setIspu(ispuString);
			}
			if (pPropertyMap.get(getRestConfiguration().getIspu()) != null && skuInfo.getIspu() == null) {
				 ispu = pPropertyMap.get(getRestConfiguration().getIspu()).toString();
				 ispuString = null;
				if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)) {
					ispuString = TRUCommerceConstants.YES;
				} else {
					ispuString = TRUCommerceConstants.NO;
				}
				skuInfo.setIspu(ispuString);	
			}
			else {
				skuInfo.setIspu(TRUCommerceConstants.NO);
			}
			if (pPropertyMap.get(getRestConfiguration().getRegistryWarningIndicator()) != null) {
				skuInfo.setRegistryWarningIndicator((String)pPropertyMap.get(getRestConfiguration().getRegistryWarningIndicator()).toString());
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductEndecaUtil.:::::createSKUInfo method");
		}
		return skuInfo;

	}

/**
 * This method should set inventory for all the skuCode.
 *
 * @param pStyleCheck - StyleCheck
 * @param pInventoryInfoMap - InventoryInfoMap
 * @param pProductInfoVO - ProductInfoVO
 * @param pPropertyMap - PropertyMap
 * @param pSiteId - SiteId
 * @throws SiteContextException  If any SiteContextException occurs
 */
	public void enrichSKU(boolean pStyleCheck, Map<String,TRUInventoryInfo> pInventoryInfoMap, RegistryProductInfoVO pProductInfoVO, PropertyMap pPropertyMap, String pSiteId) throws SiteContextException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.enrichSKU method.. ");
		}
		RegistrySKUInfoVO skuInfo = null;
		TRUInventoryInfo currSKUInvInfo = null;
		String skuId = null;
		Set<RegistrySKUInfoVO> childSkuList = pProductInfoVO.getChildSKUsList();
		skuInfo = createSKUInfo(pPropertyMap);
		if(pPropertyMap.get(getRestConfiguration().getSkuRepositoryId())!=null) {
			skuId = pPropertyMap.get(getRestConfiguration().getSkuRepositoryId()).toString();	
			}
		if (skuInfo != null) {
			if (pInventoryInfoMap != null && !pInventoryInfoMap.isEmpty()) {
				currSKUInvInfo = (TRUInventoryInfo) pInventoryInfoMap.get(skuInfo.getId());
			}
			if (pProductInfoVO != null && childSkuList == null) {
				childSkuList = new HashSet<RegistrySKUInfoVO>();
			}
			if (currSKUInvInfo == null || currSKUInvInfo.getAtcStatus() == TRURestConstants.COHERENCE_OUT_OF_STOCK) {
				skuInfo.setInventoryStatus(TRUCommerceConstants.OUT_OF_STOCK); 
				pProductInfoVO.setProductStatus(TRUCommerceConstants.OUT_OF_STOCK);
			} else if (currSKUInvInfo != null) {
				skuInfo.setInventoryStatus(TRUCommerceConstants.IN_STOCK);
				pProductInfoVO.setProductStatus(TRUCommerceConstants.IN_STOCK);
			}
			if (StringUtils.isNotBlank(skuInfo.getOnlinePID())) {
				pProductInfoVO.setPdpURL(getSiteUtil().getProductPageUtil().constructPDPURLWithOnlinePID(skuInfo.getOnlinePID(), pSiteId));
			}
			if (StringUtils.isNotBlank(pProductInfoVO.getParentCategoryId())) {
				skuInfo.setParentCategoryId(pProductInfoVO.getParentCategoryId());
			}
			if(StringUtils.isNotBlank(pSiteId)){
				skuInfo.setSiteId(pSiteId);
			}
			if (!childSkuList.contains(skuId)) {
				childSkuList.add(skuInfo);	
			}
			if (pStyleCheck) {
				pProductInfoVO.setChildSKUsList(childSkuList);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductEndecaUtil.:::::enrichSKU method");
		}
	}

	/**
	 * 
	 * @param pItemQuery
	 *            - pItemQuery
	 * @param pSerachProperty
	 *            - SerachProperty
	 * @return eRecList - the endeca eRecList
	 * @throws ENEQueryException
	 *             If any ENEQueryException occurs
	 */
	@SuppressWarnings("unchecked")
	public  ERecList createSearchQuery(String pItemQuery, String pSerachProperty) throws ENEQueryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.:::::createSearchQuery method");
		}
		final ENEQuery query = new ENEQuery();
		ERecList eRecList = null;
		final DimValIdList dimValIdList = new DimValIdList(TRURestConstants.ZERO);
		final ERecSearchList searches = new ERecSearchList();
		final ERecSearch eRecSearch = new ERecSearch(pSerachProperty, pItemQuery, TRUSearchMode.ANY.getMode());
		query.setNavDescriptors(dimValIdList);
		searches.add(eRecSearch);
		query.setAggrERecRollupKey(TRURestConstants.ONLINE_PID);
		if (isEnablePriceFilter()) {
			query.setNavRecordFilter(getPriceFilter());	
		} else {
			query.setNavRecordFilter(getPriceExistsFilterBuilder().buildRecordFilter());	
		}
		query.setNavAllRefinements(false);
		query.setNavERecSearchDidYouMean(false);
		query.setNavERecSearches(searches);
		query.setDimSearchCompound(false);
		query.setNavNumERecs(TRURestConstants.RECORDS_PER_PAGE);
		final ENEQueryResults results = createConnection().query(query);
		if (results != null) {
			eRecList = results.getNavigation().getERecs();
			if (isLoggingDebug()) {
				vlogDebug("TRUProductEndecaUtil.:::::createSearchQuery method", eRecList.size());
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductEndecaUtil.:::::createSearchQuery method");
		}
		return eRecList;
	}
	
	  /**
     * @return eneConnection -return the ENEConnections
     */
    private  ENEConnection createConnection() {
        return new HttpENEConnection(getIndexingConfiguration().getDefaultMdexHostName(), getIndexingConfiguration().getDefaultMdexPort());
    }
	/**
	 * @return the mRestConfiguration
	 */
	public TRURestConfiguration getRestConfiguration() {
		return mRestConfiguration;
	}
	/**
	 * @param pRestConfiguration the mRestConfiguration to set
	 */
	public void setRestConfiguration(TRURestConfiguration pRestConfiguration) {
		this.mRestConfiguration = pRestConfiguration;
	}
	/**
	 * @return the mCoherenceInventoryManager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}
	/**
	 * @param pCoherenceInventoryManager the mCoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		this.mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 * @return the mSiteUtil
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}

	/**
	 * @param pSiteUtil the mSiteUtil to set
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		this.mSiteUtil = pSiteUtil;
	}

	/**
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * @return the mMdexRequest
	 */
	public MdexRequest getMdexRequest() {
		return mMdexRequest;
	}

	/**
	 * @param pMdexRequest the mMdexRequest to set
	 */
	public void setMdexRequest(MdexRequest pMdexRequest) {
		this.mMdexRequest = pMdexRequest;
	}
	
	
	 /**
		 * This property hold reference for mIndexingConfiguration.
		 */
		private TRUIndexingConfiguration mIndexingConfiguration;
		
		/**
		 * @return the mIndexingConfiguration
		 */
		public TRUIndexingConfiguration getIndexingConfiguration() {
			return mIndexingConfiguration;
		}
		/**
		 * @param pRestConfiguration the mIndexingConfiguration to set
		 */
		public void setIndexingConfiguration(TRUIndexingConfiguration pRestConfiguration) {
			this.mIndexingConfiguration = pRestConfiguration;
		}
		
		/**
		 * This property hold reference for CatalogTools.
		 */	
		private TRUCatalogTools mCatalogTools;
		
		/**
		 * @return the catalogTools
		 */
		public TRUCatalogTools getCatalogTools() {
			return mCatalogTools;
		}

		/**
		 * @param pCatalogTools the catalogTools to set
		 */
		public void setCatalogTools(TRUCatalogTools pCatalogTools) {
			mCatalogTools = pCatalogTools;
		}
		

		/** Property Holds CatalogProperties. */
		private TRUCatalogProperties mCatalogProperties;
		/**
		 * Gets the catalog properties.
		 *
		 * @return mCatalogProperties
		 */
		public TRUCatalogProperties getCatalogProperties() {
			return mCatalogProperties;
		}

		/**
		 * Sets the catalog properties.
		 *
		 * @param pCatalogProperties the CatalogProperties to set
		 */
		public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
			mCatalogProperties = pCatalogProperties;
		}
		
		/**
		 * This method will return the CategorySiteList
		 * 
		 * @param pCategoryId
		 *            - CategoryId
		 * @return siteIdList categorySiteIdList
		 */
		@SuppressWarnings("unchecked")
		public Set<String> getCategorySiteList(String pCategoryId) {
			if (isLoggingDebug()) {
				logDebug("BEGIN:: TRUProductEndecaUtil.getCategorySiteList method.. ");
			}
			Set<String> siteIdList = null;
			try {
				RepositoryItem categoryItem = getCatalogTools().findCategory(pCategoryId);
				if (categoryItem != null) {
					siteIdList = (Set<String>)categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
				}	
			} 
			catch (RepositoryException repositoryException) {
				if(isLoggingError()){
					logError("RepositoryException occured in TRUProductEndecaUtil.getCategorySiteList method while fetching categoryItem", repositoryException);	
				}
			}
			if (isLoggingDebug()) {
				logDebug("END:: TRUProductEndecaUtil.:::::getCategorySiteList method");
			}
			return siteIdList;
		}
		
/**
 * This method will return the warehouseId for the selected storeId 
 * @param pStoreId - StoreId;
 * @return wareHouseId - the wareHouseId
 */
	public String getWareHouseId(String pStoreId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.getWareHouseId method.. ");
		}
		TRUStoreLocatorTools locatorTools = ((TRUStoreLocatorTools)getStoreLocatorTools());
		String wId = null;
  	   	RepositoryItem storeItem = locatorTools.getStoreItem(pStoreId);
  	   	Double storeItemWareHouseLocationId = (Double)locatorTools.getPropertyValue(storeItem, locatorTools.getLocationPropertyManager().getWarehouseLocationCodePropertyName());
		if (!storeItemWareHouseLocationId.isNaN()) {
			Integer wareHouseId = storeItemWareHouseLocationId.intValue();
			wId = wareHouseId.toString();
		}
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.getWareHouseId method.. ");
		}
  	   	return wId;
	}
	
	/**
	 * Get the skuIds
	 * @param pSkuIds : SkuIds
	 * @return productIdList :  productIdList
	 */
		public List<String> getSkuIds(String pSkuIds) {
			if (isLoggingDebug()) {
				logDebug("BEGIN: @method getSkuIds() @class: PopulateBeanFromContentItem" +pSkuIds);
			}
			List<String> productIdList = new ArrayList<String>();
			String[] productIdArray;
			productIdArray = pSkuIds.split(TRUCommerceConstants.SPLIT_PIPE);
			for (String skuVal : productIdArray) {
				productIdList.add(skuVal);
			}
			//productIdList = Arrays.asList(productIdArray);
			if (isLoggingDebug()) {
				logDebug("END: @method getSkuIds() @class: PopulateBeanFromContentItem");
			}
			return productIdList;
		}

	/**
	 * This method will return all the skuIds from Endeca records
	 * @param pRecList - RecList
	 * @return skuIds - List of skuIds
	 */
	public List<String> getSkuIdsFromRecord(ERecList pRecList) {
		List<String> skuIds = new ArrayList<String>();
		String skuId = null;
		for (Object result : pRecList) {
			final ERec eRec = (ERec) result;
			if (eRec.getProperties().get(getRestConfiguration().getSkuRepositoryId()) != null) {
				skuId = eRec.getProperties().get(getRestConfiguration().getSkuRepositoryId()).toString();
				skuIds.add(skuId);
			}
		}
		return skuIds;
	}
		/**
		 * @return the mPriceExistsFilterBuilder
		 */
		public TRUPriceExistsFilterBuilder getPriceExistsFilterBuilder() {
			return mPriceExistsFilterBuilder;
		}

		/**
		 * @param pPriceExistsFilterBuilder the mPriceExistsFilterBuilder to set
		 */
		public void setPriceExistsFilterBuilder(
				TRUPriceExistsFilterBuilder pPriceExistsFilterBuilder) {
			this.mPriceExistsFilterBuilder = pPriceExistsFilterBuilder;
		}

		/**
		 * @return the mStoreLocatorTools
		 */
		public PARALStoreLocatorTools getStoreLocatorTools() {
			return mStoreLocatorTools;
		}

		/**
		 * @param pStoreLocatorTools the mStoreLocatorTools to set
		 */
		public void setStoreLocatorTools(PARALStoreLocatorTools pStoreLocatorTools) {
			this.mStoreLocatorTools = pStoreLocatorTools;
		}

		/**
		 * @return the mRecordSize
		 */
		public int getRecordSize() {
			return mRecordSize;
		}

		/**
		 * @param pRecordSize the mRecordSize to set
		 */
		public void setRecordSize(int pRecordSize) {
			mRecordSize = pRecordSize;
		}

		/**
		 * @return the mPriceFilter
		 */
		public String getPriceFilter() {
			return mPriceFilter;
		}

		/**
		 * @param pPriceFilter the mPriceFilter to set
		 */
		public void setPriceFilter(String pPriceFilter) {
			mPriceFilter = pPriceFilter;
		}

		/**
		 * @return the mEnablePriceFilter
		 */
		public boolean isEnablePriceFilter() {
			return mEnablePriceFilter;
		}

		/**
		 * @param pEnablePriceFilter the mEnablePriceFilter to set
		 */
		public void setEnablePriceFilter(boolean pEnablePriceFilter) {
			mEnablePriceFilter = pEnablePriceFilter;
		}
}
