package com.tru.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.model.Ancestor;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.content.support.XmlContentItem;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.Navigation;
import com.tru.common.ParametricFeedCategoryProfileVO;
import com.tru.common.Refinements;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaUtils;
import com.tru.endeca.vo.ParametricProfileVO;
import com.tru.rest.constants.TRUParametricFeedConstants;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;
/**
 * This class is used to populate beans from contentItem object return by assembler. 
 * @version 1.0
 * @author PA
 * @param <E> the type of elements in this collection
 */
public class TRUParametricFeedContentItemUtil<E> extends GenericService{

	/** Property to hold mTRUEndecaUtils. */
	private TRUEndecaUtils mTRUEndecaUtils;

	/** Property to hold mGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mGetSiteTypeUtil;

	/**  Holds the mCatalogProperties */
	private TRUCatalogProperties mCatalogProperties;
	/** Property to hold mRestConfiguration. */
	private TRURestConfiguration mRestConfiguration;

	/** Property to hold mCatalogTools. */
	private TRUCatalogTools mCatalogTools;

	/** Property to hold mDimensionValueCache. */
	private TRUDimensionValueCache mDimensionValueCache;

	/**
	 * @return the restConfiguration
	 */
	public TRURestConfiguration getRestConfiguration() {
		return mRestConfiguration;
	}
	/**
	 * @param pRestConfiguration the restConfiguration to set
	 */
	public void setRestConfiguration(TRURestConfiguration pRestConfiguration) {
		mRestConfiguration = pRestConfiguration;
	}
	/**
	 * @return the truCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the CatalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}
	/**
	 * @param pDimensionValueCache the dimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}
	/**
	 * This method used to populate bean from content item.
	 * @param pResponseContentItem responseContentItem
	 * @param pProfileId -- the profileId
	 * @param pParamProfileXML -- the profileXMl string
	 * @param pCategoryId -- the category Id
	 * @return output -- the ParametricFeedCategoryProfileVO
	 */
	@SuppressWarnings("unchecked")
	public ParametricFeedCategoryProfileVO constructCategoryProfileData(ContentItem pResponseContentItem, String pProfileId, StringBuffer pParamProfileXML, String pCategoryId){
		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.constructCategoryProfileData() method : category/subcategory/family service fetching page name from registry category id.");
		}
		ParametricFeedCategoryProfileVO output= new ParametricFeedCategoryProfileVO();
		List<E> contentItem = (ArrayList<E>) pResponseContentItem.get(getRestConfiguration().getContents());
		XmlContentItem allContents = null;
		List<E> mainProduct = null;
		if(contentItem!=null && !contentItem.isEmpty()){
			allContents = (XmlContentItem) contentItem.get(TRURestConstants.INT_ZERO);
			mainProduct = (ArrayList<E>) allContents.get(getRestConfiguration().getMainProduct());
		}
		XmlContentItem navigation;
		XmlContentItem newXmlMain;
		int arraySize=TRURestConstants.INT_ZERO;
		// added for main product content slot
		if (mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem
				&& ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(getRestConfiguration().getAtType()).toString().equals(getRestConfiguration().getTRUMainProductContentSlot())) {
		
			mainProduct = ((List<E>) ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(TRURestConstants.CONTENTS));
			if(mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem){
				newXmlMain = (XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO);
				mainProduct = (List<E>) newXmlMain.get(getRestConfiguration().getMainProduct());
			}
		}
		if(mainProduct!=null) {
			arraySize=mainProduct.size();
		}
		//Iterate main product section to get refinements, selected refinements and results list cartridge response
		for(int iterate=TRURestConstants.INT_ZERO; iterate<arraySize; iterate++) {
			if(mainProduct.get(iterate) instanceof XmlContentItem && ((String)((XmlContentItem)mainProduct.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruGuidedNavigation())){
				
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILESTART);
				navigation = (XmlContentItem) mainProduct.get(iterate);
				String profileName = (String)navigation.get(TRUParametricFeedConstants.NAME);
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILEIDSTART);
				pParamProfileXML.append(pProfileId);
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILEIDEND);
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILENAMESTART);
				pParamProfileXML.append(TRUParametricFeedConstants.CDATASTART+profileName+TRUParametricFeedConstants.CDATAEND);
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILENAMEEND);
				pParamProfileXML.append(TRUParametricFeedConstants.ASSOCATIDSTART);
				pParamProfileXML.append(pCategoryId);
				pParamProfileXML.append(TRUParametricFeedConstants.ASSOCATIDEND);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBUTESSTART);

				constructCategoryNavigation(navigation,output,pParamProfileXML);

				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBUTESEND);
				pParamProfileXML.append(TRUParametricFeedConstants.PROFILEEND);
			}
		}
		output.setXmlProfile(pParamProfileXML.toString());
		if (isLoggingDebug()) {
			vlogDebug("END :: TRUParametricFeedContentItemUtil.constructCategoryProfileData() method : ProfileVO :: {0}",output);
		}
		return output;
	}

	/**
	 * Populate bean from navigation cartridge response.
	 * @param pNavigation navigation
	 * @param pParamProfileXML -- the profileXml String
	 * @param pOutput output
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void constructCategoryNavigation(XmlContentItem pNavigation, ParametricFeedCategoryProfileVO pOutput,StringBuffer pParamProfileXML){
		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.constructCategoryNavigation() method : category/subcategory/family service fatching page name from registry category id.");
		}
		List<E> refinements = (ArrayList<E>) pNavigation.get(getRestConfiguration().getNavigation());
		RefinementMenu refinementMenu = null;
		List<Refinements> refinementsArray = null;
		Navigation newNavigation;
		List<Navigation> navigationArray = new ArrayList<Navigation>();
		int count;
		int size;
		if (isLoggingDebug()) {
			vlogDebug("TRUParametricFeedContentItemUtil.constructCategoryNavigation() refinements :: {0}", refinements);
		}
		for(count=TRURestConstants.INT_ZERO,size=refinements.size();count<size;count++){
			if(refinements.get(count) instanceof RefinementMenu){
				refinementMenu = (RefinementMenu) refinements.get(count);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBSTART);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBDISPSEQSTART);
				pParamProfileXML.append(count);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBDISPSEQEND);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBNAMESTART);

				if(refinementMenu.get(getRestConfiguration().getFacetName())!=null) {
					pParamProfileXML.append(TRUParametricFeedConstants.CDATASTART+refinementMenu.get(getRestConfiguration().getFacetName())+TRUParametricFeedConstants.CDATAEND);
				}
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBNAMEEND);
				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYNAMESTART);
				if(refinementMenu.get(getRestConfiguration().getFacetName())!=null) {
					pParamProfileXML.append(TRUParametricFeedConstants.CDATASTART+refinementMenu.get(getRestConfiguration().getFacetName())+TRUParametricFeedConstants.CDATAEND);
				}
				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYNAMEEND);
				pParamProfileXML.append(TRUParametricFeedConstants.FACETRANGESTART);

				refinementsArray = populateRefinementArray(refinementMenu);
				if (isLoggingDebug()) {
					vlogDebug("TRUParametricFeedContentItemUtil.constructCategoryNavigation() : refinementsArray :: {0}",refinementsArray);
				}
				for (Iterator iterator = refinementsArray.iterator(); iterator.hasNext();) {
					Refinements currRefinement = (Refinements) iterator.next();
					pParamProfileXML.append(TRUParametricFeedConstants.FACETSTART);
					pParamProfileXML.append(TRUParametricFeedConstants.FACETDISPLAYNAMESTART);
					pParamProfileXML.append(TRUParametricFeedConstants.CDATASTART+currRefinement.getLabel()+TRUParametricFeedConstants.CDATAEND);
					pParamProfileXML.append(TRUParametricFeedConstants.FACETDISPLAYNAMEEND);
					pParamProfileXML.append(TRUParametricFeedConstants.COUNTSTART);
					pParamProfileXML.append(currRefinement.getCount());
					pParamProfileXML.append(TRUParametricFeedConstants.COUNTEND);
					pParamProfileXML.append(TRUParametricFeedConstants.FACETEND);

				}
				pParamProfileXML.append(TRUParametricFeedConstants.FACETRANGEEND);


				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYMORECOUNTSTART);
				pParamProfileXML.append(TRUParametricFeedConstants.ZEROLABEL);
				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYMORECOUNTEND);
				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYMORESTART);
				pParamProfileXML.append(TRUParametricFeedConstants.CDATASTART+TRUParametricFeedConstants.NALABEL+TRUParametricFeedConstants.CDATAEND);
				pParamProfileXML.append(TRUParametricFeedConstants.DISPLAYMOREEND);

				pParamProfileXML.append(TRUParametricFeedConstants.MULTISELECTSTART);
				if(refinementMenu.isMultiSelect()){
					pParamProfileXML.append(TRUParametricFeedConstants.TRUE);
				}
				else {
					pParamProfileXML.append(TRUParametricFeedConstants.FALSE);
				}
				pParamProfileXML.append(TRUParametricFeedConstants.MULTISELECTEND);
				pParamProfileXML.append(TRUParametricFeedConstants.ATTRIBEND);
				newNavigation = new Navigation();
				if(refinementMenu.get(getRestConfiguration().getDisplayable())!=null) {
					newNavigation.setDisplayable((boolean) refinementMenu.get(getRestConfiguration().getDisplayable()));
				}
				if(refinementMenu.get(getRestConfiguration().getFacetName())!=null) {

					newNavigation.setName((String) refinementMenu.get(getRestConfiguration().getFacetName()));
				}	
				newNavigation.setRefinements(refinementsArray);
				navigationArray.add(newNavigation);
			}
		}
		pOutput.setNavigation(navigationArray);
		if (isLoggingDebug()) {
			logDebug("END: TRUParametricFeedContentItemUtil.constructCategoryNavigation() method : category/subcategory/family service fatching page name from registry category id.");
		}
	}

	/**
	 * This method populates the refinements.
	 * @param pRefinementMenu -- the refinement menu
	 * @return refinementsArray -- the list of refinements
	 */
	public List<Refinements> populateRefinementArray(RefinementMenu pRefinementMenu )
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParametricFeedContentItemUtil.populateRefinementArray() method");
		}
		List<Refinement> refinementArray = null;
		ListIterator<Refinement> iterateRefinement = null;
		Refinements newRefinements = null;
		Refinement refinement;
		List<Refinements> refinementsArray = null;
		refinementArray = (List<Refinement>) pRefinementMenu.getRefinements();
		iterateRefinement = refinementArray.listIterator();
		refinementsArray = new ArrayList<Refinements>();
		while(iterateRefinement.hasNext()){
			refinement = iterateRefinement.next();
			newRefinements = new Refinements();
			newRefinements.setNavigationState(refinement.getNavigationState());
			newRefinements.setLabel(refinement.getLabel());
			newRefinements.setCount(refinement.getCount());
			newRefinements.setMultiselect(refinement.isMultiSelect());
			refinementsArray.add(newRefinements);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParametricFeedContentItemUtil.populateRefinementArray() method");
		}
		return refinementsArray;
	}

	/**
	 * get navigation URL from category id or category dimension id.
	 * @param pCategoryId category id
	 * @return String navigation URL 
	 */
	public DimensionValueCacheObject getNavigationURLfromCategory(String pCategoryId){
		DimensionValueCacheObject dimCacheObject = null;
		dimCacheObject = getDimensionValueCache().getCachedObjectForDimval(pCategoryId);//Passed id is dimension id
		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.getNavigationURLfromCategory() . category/subcategory/family service fatched page name from dimension id."+dimCacheObject);
		}
		if (dimCacheObject == null) {
			dimCacheObject = getDimensionValueCache().getCacheWithMultipleAncestors(pCategoryId);
			if (isLoggingDebug()) {
				logDebug("TRUParametricFeedContentItemUtil.getNavigationURLfromCategory() - category/subcategory/family service fetched page name from category id."+dimCacheObject);
			}
			if(dimCacheObject==null){
				// Passed Id is may be registry id
				dimCacheObject = getNavigationURLFromRegistry(pCategoryId);
			}
		} 
		if (isLoggingDebug()) {
			logDebug("END: TRUParametricFeedContentItemUtil.getNavigationURLfromCategory() . category/subcategory/family service fatched page name from dimension id."+dimCacheObject);
		}
		return dimCacheObject;
	}

	/**
	 * get navigation URL from category id or category dimension id.
	 * @param pCategoryId category id
	 * @return String navigation URL 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DimensionValueCacheObject getNavigationURLFromRegistry(String pCategoryId){
		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.getNavigationURLFromRegistry() method : category/subcategory/family service fatching page name from registry category id.");
		}
		DimensionValueCacheObject result = null;
		RepositoryItem categoryItem = null;
		String newCategoryId=null;
		try {
			categoryItem = getCatalogTools().findCategory(pCategoryId);
			if(categoryItem!=null && 
					categoryItem.getItemDescriptor().getItemDescriptorName().equals(getRestConfiguration().getRegistryClassificationItemName())){
				List<RepositoryItem> crossReferenceList = 
						(List<RepositoryItem>) categoryItem.getPropertyValue(getRestConfiguration().getCrossReferencePropertyName());
				if(crossReferenceList!=null && !crossReferenceList.isEmpty()){
					String currentSiteId = SiteContextManager.getCurrentSiteId();
					Set siteIdsSet=null;
					for(RepositoryItem classificationItem:crossReferenceList){
						siteIdsSet = (Set) classificationItem.getPropertyValue(getRestConfiguration().getSiteIdsPropertyName());
						if(StringUtils.isNotBlank(currentSiteId) && siteIdsSet!=null && siteIdsSet.contains(currentSiteId)){
							newCategoryId = classificationItem.getRepositoryId();
							break;
						}
					}
					if(StringUtils.isNotBlank(newCategoryId)){
						result = getDimensionValueCache().getCacheWithMultipleAncestors(newCategoryId);
						if(result!=null){
							return result;
						}
					}

				}
			}
		} catch (RepositoryException e) {
			vlogError("TRUParametricFeedContentItemUtil.getNavigationURLFromRegistry() method : Getting repository exception while fatching item for passed incorrect category id.",e);
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUParametricFeedContentItemUtil.getNavigationURLFromRegistry() method : category/subcategory/family service fatching page name from registry category id.");
		}
		return result;
	}

	/**
	 * This method will return the CategorySiteList
	 * 
	 * @param pCategoryId
	 *            - CategoryId
	 * @return siteIdList categorySiteIdList
	 */
	@SuppressWarnings("unchecked")
	public boolean getCategorySiteList(String pCategoryId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductEndecaUtil.getCategorySiteList method.. ");
		}
		boolean isValidCategorySite = false;
		RepositoryItem categoryItem = null;
		try {
			categoryItem = getCatalogTools().findCategory(pCategoryId);
		} catch (RepositoryException repositoryException) {
			if(isLoggingError()){
				vlogError(repositoryException, "RepositoryException occured in TRUProductEndecaUtil.getCategorySiteList method while fetching categoryItem");	
			}
		}

		if (categoryItem != null) {
			Set<String> siteIdList = (Set<String>)categoryItem.getPropertyValue(getCatalogProperties().getSiteIDPropertyName());
			if(siteIdList != null && !siteIdList.isEmpty()) {
				String currentSiteId = SiteContextManager.getCurrentSiteId();
				if (StringUtils.isNotBlank(currentSiteId) && siteIdList.contains(currentSiteId)) {
					isValidCategorySite = true;
				}
			}
		}	
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductEndecaUtil.:::::getCategorySiteList method");
		}
		return isValidCategorySite;
	}

	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}

	/**
	 * This method populates the Parametric CSV data.
	 * @param pResponseContentItem -- the response content
	 * @param pCategoryId -- the Category Id
	 * @return ParametricFeedCategoryProfileVO -- the ParametricFeedCategoryProfileVO
	 */
	@SuppressWarnings("unchecked")
	public ParametricFeedCategoryProfileVO constructCategoryCsvData(ContentItem pResponseContentItem, String pCategoryId) {

		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.constructCategoryCsvData() method :");
		}
		ParametricFeedCategoryProfileVO output = new ParametricFeedCategoryProfileVO();
		ParametricProfileVO parametricProfileVO = new ParametricProfileVO();
		parametricProfileVO.setCatalogId(pCategoryId);
		List<E> contentItem = (ArrayList<E>) pResponseContentItem.get(getRestConfiguration().getContents());
		XmlContentItem allContents = null;
		List<E> mainHeader = null;
		List<E> mainProduct = null;
		List<E> mainContent = null;
		if(contentItem != null && !contentItem.isEmpty()){
			allContents = (XmlContentItem) contentItem.get(TRURestConstants.INT_ZERO);
			parametricProfileVO.setPageNameDisplayTxt((String)allContents.get(TRUParametricFeedConstants.TYPE));
			mainHeader = (ArrayList<E>) allContents.get(getRestConfiguration().getMainHeader());
			mainProduct = (ArrayList<E>) allContents.get(getRestConfiguration().getMainProduct());
			mainContent = (ArrayList<E>) allContents.get(getRestConfiguration().getMainContent());
		}
		Breadcrumbs breadcrumbs;
		String primaryCategoryBreadCrumb = null;
		if(getTRUEndecaUtils().isChekPrimaryCategory()) {
			String displayStatus = null;
			List<String> primaryCategoryIdList = getTRUEndecaUtils().populatePrimaryBreadcrumbPathList(pCategoryId);
			if(primaryCategoryIdList != null && !primaryCategoryIdList.isEmpty()) {
				primaryCategoryBreadCrumb = getTRUEndecaUtils().generateCategoryPrimaryPathBreadcrumb(primaryCategoryIdList);
				parametricProfileVO.setBreadCrumb(primaryCategoryBreadCrumb);
			}
			try {
				displayStatus = getCatalogTools().findCategoryDisplayStatus(pCategoryId);
			} catch (RepositoryException e) {

				if (isLoggingError()) {
					vlogError(e, "Class TRUParametricFeedContentItemUtil :An exception occured during getting category displayStatus.");
				}
			}

			generateCategoryDisplayStatus(displayStatus,parametricProfileVO);
		}

		//Iterate header content section to get breadcrumb, classification hierarchy and search adjustment cartridges response
		if(StringUtils.isEmpty(primaryCategoryBreadCrumb)){
			int arraySize = TRURestConstants.INT_ZERO;
			if(mainHeader != null) {
				arraySize = mainHeader.size();	
			}

			for(int iterate = TRURestConstants.INT_ZERO; iterate<arraySize; iterate++) {
				if(mainHeader.get(iterate) instanceof Breadcrumbs && ((String)((Breadcrumbs)mainHeader.get(iterate)).
						get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruBreadcrumbs())) {
					breadcrumbs = (Breadcrumbs) mainHeader.get(iterate);
					parametricProfileVO = populateBreadcrumbsBean(breadcrumbs,parametricProfileVO);

				} 
			}
			//Iterate main content section to get breadcrumbs cartridge response
			arraySize=TRURestConstants.INT_ZERO;
			if(mainContent != null) {
				arraySize = mainContent.size();
			}
			for(int iterate=TRURestConstants.INT_ZERO; iterate<arraySize; iterate++) {
				if(mainContent.get(iterate) instanceof Breadcrumbs && ((String)((Breadcrumbs)mainContent.get(iterate)).
						get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruBreadcrumbs())) {
					breadcrumbs = (Breadcrumbs) mainContent.get(iterate);
					parametricProfileVO =	populateBreadcrumbsBean(breadcrumbs, parametricProfileVO);
				} 
			}
		}
		int arraySize=TRURestConstants.INT_ZERO;
		// added for main product content slot
		if (mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem
				&& ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(getRestConfiguration().getAtType()).toString().equals(getRestConfiguration().getTRUMainProductContentSlot())) {
			mainProduct = ((List<E>) ((XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO)).get(TRURestConstants.CONTENTS));
			if(mainProduct != null	&& !mainProduct.isEmpty() && mainProduct.get(TRURestConstants.INT_ZERO) instanceof XmlContentItem){
				XmlContentItem newXmlMain = (XmlContentItem) mainProduct.get(TRURestConstants.INT_ZERO);
				mainProduct = (List<E>) newXmlMain.get(getRestConfiguration().getMainProduct());
			}
		}
		if(mainProduct!=null)
		{
			arraySize=mainProduct.size();
		}
		//Iterate main product section to get refinements, selected refinements and results list cartridge response
		for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
			if(mainProduct.get(iterate) instanceof XmlContentItem && ((String)((XmlContentItem)mainProduct.get(iterate)).
					get(getRestConfiguration().getAtType())).equals(getRestConfiguration().getTruGuidedNavigation())){
				XmlContentItem navigation = (XmlContentItem) mainProduct.get(iterate);
				String navText = (String)navigation.get(TRUParametricFeedConstants.NAME);
				if(StringUtils.isNotBlank(navText) && !TRUEndecaConstants.NULL.equals(navText))	{
					parametricProfileVO.setParametricProfileNavTxt(navText);
				}
				if (isLoggingDebug()) {
					vlogDebug("TRUParametricFeedContentItemUtil.constructCategoryCsvData() method :parametricProfileVO {0}", parametricProfileVO);
				}
			}
		}
		output.setParametricProfileVO(parametricProfileVO);
		if (isLoggingDebug()) {
			logDebug("END: TRUParametricFeedContentItemUtil.constructCategoryCsvData() method :");
		}
		return output;
	}

	/**
	 * Populates ParametricProfileVO CSV data from breadcrumb cartridge response.
	 * @param pBreadcrumbs pBreadcrumbs -- the Breadcrumb
	 * @param pParametricProfileVO -- the  pParametricProfileVO
	 * @return pParametricProfileVO -- the ParametricProfileVO
	 */
	private ParametricProfileVO populateBreadcrumbsBean(Breadcrumbs pBreadcrumbs,ParametricProfileVO pParametricProfileVO) {

		if (isLoggingDebug()) {
			logDebug("BEGIN: TRUParametricFeedContentItemUtil.populateBreadcrumbsBean() method :");
		}
		StringBuffer breadCrumb = new StringBuffer();
		if (breadCrumb != null && breadCrumb.length() > TRUEndecaConstants.INT_ZERO) {
			breadCrumb.append(TRUEndecaConstants.GREATERTHEN);
		}
		String displayStatus = null;
		List<RefinementBreadcrumb> refinementCrumbs = (ArrayList<RefinementBreadcrumb>) pBreadcrumbs.getRefinementCrumbs();

		ListIterator<RefinementBreadcrumb> iterate = refinementCrumbs.listIterator();
		while(iterate.hasNext()) {
			RefinementBreadcrumb refinementBreadcrumb = iterate.next();
			displayStatus = refinementBreadcrumb.getProperties().get(TRUEndecaConstants.DISPLAY_STATUS);
			List<Ancestor> ancestors = (ArrayList<Ancestor>) refinementBreadcrumb.getAncestors();
			ListIterator<Ancestor> iterateAncestor = ancestors.listIterator();
			while(iterateAncestor.hasNext()){
				Ancestor ancestor = iterateAncestor.next();
				if(StringUtils.isNotBlank(ancestor.getLabel())) {
					breadCrumb.append(ancestor.getLabel());
					breadCrumb.append(TRUEndecaConstants.GREATERTHEN);
				}
			}
			if(StringUtils.isNotBlank(refinementBreadcrumb.getLabel())) {
				breadCrumb.append(refinementBreadcrumb.getLabel());
			}
		}
		generateCategoryDisplayStatus(displayStatus,pParametricProfileVO);
		pParametricProfileVO.setBreadCrumb(breadCrumb.toString());
		if (isLoggingDebug()) {
			vlogDebug("END: TRUParametricFeedContentItemUtil.populateBreadcrumbsBean() method :breadCrumb {0}", breadCrumb.toString());
		}
		return pParametricProfileVO;
	}

	/**
	 * Sets the category status for  category.
	 * @param pDisplayStatus -- the display status
	 * @param pParametricVo -- the parametric vo
	 */
	private void generateCategoryDisplayStatus(String pDisplayStatus, ParametricProfileVO pParametricVo){
		if (isLoggingDebug()) {
			vlogDebug("BEGIN: TRUParametricFeedContentItemUtil  :: generateCategoryDisplayStatus() method ");
		}
		if(StringUtils.isEmpty(pDisplayStatus)){
			pParametricVo.setCatalogStatusCD(pDisplayStatus);
			return;
		}
		if(pDisplayStatus.equals(TRUEndecaConstants.ACTIVE)){
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_A);
		}else if(pDisplayStatus.equals(TRUEndecaConstants.NO_DISPLAY)){
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_ND);
		}else if(pDisplayStatus.equals(TRUEndecaConstants.HIDDEN)){
			pParametricVo.setCatalogStatusCD(TRUEndecaConstants.STRING_H);
		}else{
			pParametricVo.setCatalogStatusCD(pDisplayStatus);
		}
		if (isLoggingDebug()) {
			vlogDebug("END: TRUParametricFeedContentItemUtil  :: generateCategoryDisplayStatus() method ");
		}
	}

	/**
	 * @return the getSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getGetSiteTypeUtil() {
		return mGetSiteTypeUtil;
	}
	/**
	 * @param pGetSiteTypeUtil the getSiteTypeUtil to set
	 */
	public void setGetSiteTypeUtil(TRUGetSiteTypeUtil pGetSiteTypeUtil) {
		mGetSiteTypeUtil = pGetSiteTypeUtil;
	}

	/**
	 * Sets the TRU Endeca util.
	 *
	 * @param pTRUEndecaUtils - the new TRU get site type util
	 */
	public void setTRUEndecaUtils(TRUEndecaUtils pTRUEndecaUtils) {
		this.mTRUEndecaUtils = pTRUEndecaUtils;
	}	

	/**
	 * Gets the TRU get Endeca util.
	 *
	 * @return the TRU get site type util
	 */
	public TRUEndecaUtils getTRUEndecaUtils() {
		return mTRUEndecaUtils;
	}

}
