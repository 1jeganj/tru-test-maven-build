
package com.tru.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RecordDetails;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.content.support.XmlContentItem;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.BreadcrumbInfoVO;
import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.catalog.vo.ProductPromoInfoVO;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.RegistryProductInfoVO;
import com.tru.common.RegistrySKUInfoVO;
import com.tru.common.TRUConstants;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This class extends the GenericService class. 
 * This class contains series of helper methods.
 * 
 * @author Professional Access
 * @version 1.0
 * @param <E> the type of elements in this collection
 * 
 */
public class TRUProductInfoUtil<E> extends GenericService {
	
	/**
	 * This property hold reference for CatalogManager.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 * This property hold reference for mPriceListManager.
	 */
	private PriceListManager mPriceListManager;
	/**
	 *  Holds the mPricingTools.
	 */
	
	private TRUPricingTools mPricingTools;
	
	/** This holds the Default PriceList Property Name. **/
	private String mDefaultPriceListPropertyName;
	/**
	 * Holds the mSiteUtil
	 */
	private TRUGetSiteTypeUtil mSiteUtil;
	
	/**
	 *  Holds the mAssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;
	
	/**
	 * Holds the mTRURestConfiguration
	 */
	private TRURestConfiguration mRestConfiguration;
	
	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	/** property to hold promotionTools. */
	private TRUPromotionTools mPromotionTools;
	
	/** property to hold mCatalogProperties. */
	private TRUCatalogProperties mCatalogProperties;
	
	/** property to hold pProductIds. */
	private List<String> mProductIds;

	/** Property to hold mLoadSkus */
	private String mLoadSkus;

	/** This holds the mDefaultSalePriceListPropertyName **/
	private String mDefaultSalePriceListPropertyName;
	
	/** This holds the  mPriceListRepository**/
	private MutableRepository mPriceListRepository;
	
	/** This holds the  mLoadPrice**/
	private boolean mLoadPrice;
	
	/** Holds property of mEnablePDPURL. */
	private boolean mEnablePromotion;
	
	/** This holds the  mTestInventoryInfoMap**/
	public Map<String, Long> mTestInventoryInfoMap = new HashMap<String, Long>();
	
	/** This holds the  mTestPriceInfoMap**/
	public Map<String, String> mTestPriceInfoMap = new HashMap<String, String>();
	/**
	 * @return the mPriceListRepository
	 */
	public MutableRepository getPriceListRepository() {
		return mPriceListRepository;
	}

	/**
	 * @param pPriceListRepository the mPriceListRepository to set
	 */
	public void setPriceListRepository(MutableRepository pPriceListRepository) {
		this.mPriceListRepository = pPriceListRepository;
	}

	/**
	 * @return the mLoadSkus
	 */
	public String getLoadSkus() {
		return mLoadSkus;
	}
	/**
	 * @param pLoadSkus the mLoadSkus to set
	 */
	public void setLoadSkus(String pLoadSkus) {
		this.mLoadSkus = pLoadSkus;
	}


	/**
	 * @return the mProductIds
	 */
	public List<String> getProductIds() {
		return mProductIds;
	}

	/**
	 * @param pProductIds the mProductIds to set
	 */
	public void setProductIds(List<String> pProductIds) {
		this.mProductIds = pProductIds;
	}
	/**
	 * @return the mDefaultPriceListPropertyName
	 */
	public String getDefaultPriceListPropertyName() {
		return mDefaultPriceListPropertyName;
	}

	/**
	 * @param pDefaultPriceListPropertyName the mDefaultPriceListPropertyName to set
	 */
	public void setDefaultPriceListPropertyName(String pDefaultPriceListPropertyName) {
		this.mDefaultPriceListPropertyName = pDefaultPriceListPropertyName;
	}

	/**
	 * @return the mTestInventoryInfoMap
	 */
	public Map<String, Long> getTestInventoryInfoMap() {
		return mTestInventoryInfoMap;
	}

	/**
	 * @param pTestInventoryInfoMap the mTestInventoryInfoMap to set
	 */
	public void setTestInventoryInfoMap(Map<String, Long> pTestInventoryInfoMap) {
		this.mTestInventoryInfoMap = pTestInventoryInfoMap;
	}

	
	/**
	 * @return the mPriceListManager
	 */
	public PriceListManager getPriceListManager() {
		return mPriceListManager;
	}

	/**
	 * @param pPriceListManager the mPriceListManager to set
	 */
	public void setPriceListManager(PriceListManager pPriceListManager) {
		this.mPriceListManager = pPriceListManager;
	}

	/**
	 * This method will set the price on skuLevel for PDP page.
	 * 
	 * @param pProductId
	 *            - ProductId
	 * 
	 * @param pSkuId
	 *            - SkuId
	 * @param pSite
	 *            Site
	 * @return priceinfo priceinfo
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 */
		public PriceInfoVO setItemPrice(String pProductId, String pSkuId, Site pSite) throws SiteContextException {
			if (isLoggingDebug()) {
				logDebug("BEGIN:: TRUProductInfoUtil.setItemPrice method.. ");
			}
			double listPrice = TRUConstants.DOUBLE_ZERO;
			double salePrice = TRUConstants.DOUBLE_ZERO;
			PriceInfoVO priceinfo = null;
			if (StringUtils.isNotEmpty(pProductId) && StringUtils.isNotEmpty(pSkuId)) {
				listPrice = getPricingTools().getListPriceForSKU(pSkuId,pProductId, pSite);
				salePrice = getPricingTools().getSalePriceForSKU(pSkuId,pProductId, pSite);
				boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				priceinfo = new PriceInfoVO();
				if (calculateSavings) {
					priceinfo.setSavedAmount(savingsMap.get(TRUConstants.SAVED_AMOUNT));
					priceinfo.setSavedPercentage(savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);
				} else {
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);
				}
			}
			if (isLoggingDebug()) {
				logDebug("END:: TRUProductInfoUtil.:::::setItemPrice method");
			}
			return priceinfo;
		}
		
	/**
	 * This method will populate PDP page navigation Breadcrumb state.
	 * 
	 * @param pAncestorsList
	 *            AncestorsList
	 * @param pReq
	 *            Req
	 * @return parentCategoryMap parentCategoryMap
	 */
		@SuppressWarnings("unchecked")
		public Map<String,String> populateBreadcrumbDetails(List<BreadcrumbInfoVO> pAncestorsList, DynamoHttpServletRequest pReq) {
			if (isLoggingDebug()) {
				logDebug("BEGIN:: TRUProductInfoUtil.populateBreadcrumbDetails method.. ");
			}
			if (PerformanceMonitor.isEnabled()) {
				PerformanceMonitor.startOperation(TRURestConstants.START_ENDECA_METHOD);
			}
			XmlContentItem allContents = null;
			List<E> mainConetnts = null;
			List<E> categoryVOList = null;
			ContentItem contentItem=null;
			Map<String,String> parentCatMap=new HashMap<String,String>();
			CategoryVO lastCategoryVO=null;
			String parentCategoryId=null;
			String parentCatName=null;
			int arraySize = TRURestConstants.INT_ZERO;
			RecordDetails recordDetails;
			contentItem = createContentInclude(pReq, getRestConfiguration().getPagesProductDetailPage());
			try {
				contentItem = getAssemblerTools().invokeAssembler(contentItem);
			} catch (AssemblerException assemblerException) {
				if (isLoggingError()) {
					logError("AssemblerException in TRUProductInfoUtil.::@method::populateBreadcrumbDetails:",assemblerException);
				}
			}
			List<E> modifiedContentItem = (ArrayList<E>) contentItem.get(TRURestConstants.CONTENTS);
			if (modifiedContentItem != null && !modifiedContentItem.isEmpty()) {
			allContents = (XmlContentItem) modifiedContentItem.get(TRURestConstants.INT_ZERO);
			mainConetnts = (ArrayList<E>) allContents.get(TRURestConstants.MAIN_CONTENT);
			if (mainConetnts != null) {
				arraySize = mainConetnts.size();
				for (int iterate = TRURestConstants.INT_ZERO; iterate < arraySize; iterate++) {
					if (mainConetnts.get(iterate) instanceof RecordDetails && ((String) ((RecordDetails) mainConetnts.get(iterate)).get(TRURestConstants.TYPE)).equals(TRURestConstants.TRU_PDP_BREADCRUMBS)) {
						recordDetails = (RecordDetails) mainConetnts.get(iterate);
						categoryVOList = (ArrayList<E>) recordDetails.get(TRURestConstants.PDP_BREADCRUMB_LIST);
							if (categoryVOList != null && !categoryVOList.isEmpty()) {
							lastCategoryVO=(CategoryVO) categoryVOList.get(categoryVOList.size()-TRURestConstants.INT_ONE);
							parentCategoryId=lastCategoryVO.getCategoryId();
							parentCatName=lastCategoryVO.getCategoryName();
							parentCatMap.put(TRURestConstants.PARENT_CATEGORY_ID, parentCategoryId);
							parentCatMap.put(TRURestConstants.PARENT_CATEGORY_NAME, parentCatName);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.:::::populateBreadcrumbDetails method");
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(TRURestConstants.END_ENDECA_METHOD);
		}
		return parentCatMap;
	}

	/**
	 * This method is for populating registry product properties
	 * 
	 * @param pInputMap
	 *            - InputMap
	 * @param pIsInventoryEnabled
	 *            - IsInventoryEnabled
	 * @return prodcutInfoList
	 * @throws RepositoryException
	 *             If any RepositoryException occurs
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 * @throws PriceListException
	 *             If any PriceListException occurs
	 */
	public List<RegistryProductInfoVO> populateProductInfo(Map<String, String> pInputMap, boolean pIsInventoryEnabled) throws RepositoryException, SiteContextException, PriceListException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductInfoUtil.:::::populateProductInfo method");
		}
		List<RegistryProductInfoVO> prodcutInfoList = new ArrayList<RegistryProductInfoVO>();
		Set<RepositoryItem> productItems = null;
		Map<RepositoryItem, RepositoryItem> itemMap = null;
		boolean allSkus = false;
		boolean skuDetails = false;
		List<String> skuIdList =null;
		String[] skuCodeArray = null;
		List<String> upcList = null;
		String[] onlinePIDArray = null;
		List<String> onlinePIDList = null;
		Map<String,TRUInventoryInfo> inventoryInfoMap =null;
		String[] upcArray = null;
		String skuCode = (String) pInputMap.get(TRURestConstants.SKU_CODES);
		String allSku = (String) pInputMap.get(TRURestConstants.ALL_SKU_IDS);
		String skuDetail = (String) pInputMap.get(TRURestConstants.SKU_DETAIL);
		String upcNumber = (String) pInputMap.get(TRURestConstants.UPC_NUMBERS);
		String onlinePID = (String)pInputMap.get(TRURestConstants.ONLINE_PIDS);
		allSkus = Boolean.parseBoolean(allSku);
		skuDetails = Boolean.parseBoolean(skuDetail);
		if (pInputMap != null && !pInputMap.isEmpty() && StringUtils.isNotEmpty(skuCode)) {
			skuIdList = getCatalogManager().getCatalogTools().spliteProductId(skuCode);
			skuCodeArray = spliteProductId(skuCode);
			itemMap = getSkuItemsFromSkuIds(skuCodeArray);
		}
		if(pInputMap != null && !pInputMap.isEmpty() && StringUtils.isNotEmpty(upcNumber)) {
			upcList = new ArrayList<String>();
			upcList = getCatalogManager().getCatalogTools().spliteProductId(upcNumber);
			upcArray = upcList.toArray(new String[upcList.size()]);
			if (itemMap == null) {
				itemMap = new HashMap<RepositoryItem, RepositoryItem>();
			}
			if (skuIdList == null) {
				skuIdList = new ArrayList<String>();
			}
			skuIdList = getProductIdFromUPCNumber(upcArray, skuIdList, itemMap, allSkus);
		}
		if(pInputMap != null && !pInputMap.isEmpty() && StringUtils.isNotEmpty(onlinePID)) {
			onlinePIDList = new ArrayList<String>();
			onlinePIDList = getCatalogManager().getCatalogTools().spliteProductId(onlinePID);
			onlinePIDArray = onlinePIDList.toArray(new String[onlinePIDList.size()]);
			if (productItems == null) {
				productItems = new HashSet<RepositoryItem>();
			}
			if (skuIdList == null) {
				skuIdList = new ArrayList<String>();
			}
			skuIdList = getProductIdsFromOnlinePid(onlinePIDArray, skuIdList, productItems, allSkus);
		}
		if (pIsInventoryEnabled && skuIdList != null && !skuIdList.isEmpty()) {
			inventoryInfoMap = prepareInventory(skuIdList);
		}
		if (itemMap != null && !itemMap.isEmpty()) {
			prodcutInfoList = populateProductInfo(itemMap, skuDetails, allSkus, inventoryInfoMap);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.:::::populateProductInfo method");
		}
		return prodcutInfoList;
	}

	/**
	 * This method create productInfo
	 * 
	 * @param pItemMap
	 *            - ItemMap
	 * @param pSkuDetails
	 *            - SkuDetails
	 * @param pAllSku
	 *            - AllSku
	 * @param pInventoryInfoMap
	 *            - InventoryInfoMap
	 * @return prodcutInfoList list of productObject
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 */
	@SuppressWarnings("unchecked")
	public List<RegistryProductInfoVO> populateProductInfo(Map<RepositoryItem, RepositoryItem> pItemMap, boolean pSkuDetails, boolean pAllSku, Map<String,TRUInventoryInfo> pInventoryInfoMap) throws SiteContextException {
		RepositoryItem childSku = null;
		RepositoryItem productItem = null;
		RegistryProductInfoVO productInfoVO = null;
		String productId=null;
		List<RegistryProductInfoVO> prodcutInfoList = new ArrayList<RegistryProductInfoVO>();
		Map<String, RegistryProductInfoVO> prodInvoMap = new HashMap<String, RegistryProductInfoVO>();
		if (pItemMap != null && !pItemMap.isEmpty()) {
			for (Map.Entry<RepositoryItem, RepositoryItem> childSkuItem : pItemMap.entrySet()) {
					childSku = childSkuItem.getKey();
					productItem = childSkuItem.getValue();
					if(productItem != null ){
					productId = productItem.getRepositoryId();
						if (prodInvoMap.containsKey(productId)) {
							productInfoVO = prodInvoMap.get(productId);
							enrichSKU(false, pInventoryInfoMap, childSku,productInfoVO, productItem);
							continue;
						}
						productInfoVO = new RegistryProductInfoVO();
						if (productItem.getPropertyValue(getCatalogProperties().getProductRusItemNumber()) != null) {
							productInfoVO.setRusItemNumber(String.valueOf(productItem.getPropertyValue(getCatalogProperties().getProductRusItemNumber())));
						}
						String displayName = (String) productItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
						productInfoVO.setDisplayName(displayName);
						if (pSkuDetails && childSku != null && !pAllSku) {
								enrichSKU(true, pInventoryInfoMap, childSku, productInfoVO, productItem);
							List<String> styleDimensions = (List<String>) productItem.getPropertyValue(getCatalogProperties().getProductStyleDimension());
							if (styleDimensions != null && !styleDimensions.isEmpty()) {
								populateVariantsInformation(productInfoVO, childSku);
							}
						}
						if (productInfoVO != null) {
							prodcutInfoList.add(productInfoVO);
						prodInvoMap.put(productId, productInfoVO);
					}
				}
			}
		}
		return prodcutInfoList;
	}
	/**
	 * This method should set inventory for all the skuCode.
	 * 
	 * @param pStyleCheck
	 *            - StyleCheck
	 * 
	 * @param pInventoryInfoMap
	 *            - InventoryInfoMap
	 * 
	 * @param pChildSku
	 *            - ChildSku
	 * 
	 * @param pProductInfoVO
	 *            - ProductInfoVO
	 * 
	 * @param pProductItem
	 *            - ProductItem
	 * 
	 *  @throws SiteContextException
	 *             If any SiteContextException occurs
	 */
     @SuppressWarnings("unchecked")
	public void enrichSKU(boolean pStyleCheck, Map<String,TRUInventoryInfo> pInventoryInfoMap , RepositoryItem pChildSku, RegistryProductInfoVO pProductInfoVO, RepositoryItem pProductItem) throws SiteContextException {
            if (isLoggingDebug()) {
                   logDebug("BEGIN:: TRUProductInfoUtil.enrichSKU method.. ");
            }
		Site site = SiteContextManager.getCurrentSite();
		RegistrySKUInfoVO skuInfo = null;
		TRUInventoryInfo currSKUInvInfo = null;
		PriceInfoVO priceInfo = null;
		Set<RegistrySKUInfoVO> childSkuList = pProductInfoVO.getChildSKUsList();
		skuInfo = createSKUInfo(pChildSku);
		if(skuInfo!=null){
			priceInfo = setItemPrice(pProductItem.getRepositoryId(), pChildSku.getRepositoryId(), site);
			skuInfo.setPriceInfo(priceInfo);
			if (pInventoryInfoMap != null && !pInventoryInfoMap.isEmpty()) {
				currSKUInvInfo = (TRUInventoryInfo) pInventoryInfoMap.get(pChildSku.getRepositoryId());
			}
			if (pProductInfoVO != null && childSkuList == null) {
				childSkuList = new HashSet<RegistrySKUInfoVO>();
			}
			if (currSKUInvInfo != null && currSKUInvInfo.getQuantity() >= TRURestConstants.INT_ONE) {
				skuInfo.setInventoryStatus(TRUCommerceConstants.IN_STOCK);
				pProductInfoVO.setProductStatus(TRUCommerceConstants.IN_STOCK);
			} else {
				skuInfo.setInventoryStatus(TRUCommerceConstants.OUT_OF_STOCK);
				pProductInfoVO.setProductStatus(TRUCommerceConstants.OUT_OF_STOCK);
			}
			if (StringUtils.isNotBlank(skuInfo.getOnlinePID())) {
				pProductInfoVO.setPdpURL(getSiteUtil().getProductPageUtil().constructPDPURLWithOnlinePID(skuInfo.getOnlinePID(), site.getId()));
			}
			childSkuList.add(skuInfo);
			if(pStyleCheck){
				pProductInfoVO.setChildSKUsList(childSkuList);
				List<String> styleDimensions = (List<String>) pProductItem.getPropertyValue(getCatalogProperties().getProductStyleDimension());
				if (styleDimensions != null && !styleDimensions.isEmpty()) {
					populateVariantsInformation(pProductInfoVO, pChildSku);
				}
			}	
		}
	  if (isLoggingDebug()) {
             logDebug("END:: TRUProductInfoUtil.enrichSKU method.. ");
      } 		
	}

     /**
     * This method should set inventory for all the skuCode.
     * 
     * @param pCatalogRefIds
     *            CatalogRefIds
     * @throws SiteContextException
     *             If any SiteContextException occurs
     *  @return inventoryInfoMap inventoryInfoMap           
     */
     public Map<String, TRUInventoryInfo> prepareInventory(List<String> pCatalogRefIds) throws SiteContextException {
            if (isLoggingDebug()) {
                   logDebug("BEGIN:: TRUProductInfoUtil.prepareInventory method.. ");
            }
            if (PerformanceMonitor.isEnabled()) {
                   PerformanceMonitor.startOperation(TRURestConstants.START_INVENTORY_CALL);
            }
            Map<String,TRUInventoryInfo> inventoryInfoMap = getCatalogManager().getCatalogTools().getCoherenceInventoryManager().queryBulkS2HStockLevelMap(pCatalogRefIds); 
            if (isLoggingDebug()) {
                   logDebug("END:: TRUProductInfoUtil.:::::prepareInventory method");
            }
            if (PerformanceMonitor.isEnabled()) {
                   PerformanceMonitor.startOperation(TRURestConstants.END_INVENTORY_CALL);
            }
            return inventoryInfoMap;
     }

     /**
 	 * This method will split the skuCode or upc or productId from the request.
 	 * @param pSpliteIds SpliteIds
 	 * @return productIdList productIdList
 	 */
 		public String[] spliteProductId(String pSpliteIds) {
 			if (isLoggingDebug()) {
 				logDebug("BEGIN: @method spliteProductId() @class: TRUCatalogTools" +pSpliteIds);
 			}
 			String sliteId = null;
 			String[] productIdArray=null;
 			if(StringUtils.isNotBlank(pSpliteIds)){
 				sliteId = pSpliteIds.replaceAll(TRUCommerceConstants.OPEN, TRUCommerceConstants.EMPTY_SPACE).replaceAll(TRUCommerceConstants.CLOSE, TRUCommerceConstants.EMPTY_SPACE);
 	 			productIdArray = sliteId.split(TRUCommerceConstants.SPLIT_PIPE);
 			}
 			if (isLoggingDebug()) {
 				logDebug("END: @method spliteProductId() @class: TRUCatalogTools");
 			}
 			return productIdArray;
 		}	
	/**
	 * This method used for creating the registry SKUVoInfos
	 * 
	 * @param pSKUItem
	 *            SKUItem
	 * @return skuInfo  return the skuInfo
	 */
	@SuppressWarnings("unchecked")
	public RegistrySKUInfoVO createSKUInfo(RepositoryItem pSKUItem) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductInfoUtil.:::::createSKUInfo method");
		}
		RegistrySKUInfoVO skuInfo = null;
		boolean unCartable = Boolean.FALSE;
		boolean registryEligibility = Boolean.FALSE;
		List<String> promos = null;
		if (pSKUItem != null) {
			skuInfo=new RegistrySKUInfoVO();
			skuInfo.setId(pSKUItem.getRepositoryId());
			if(isEnablePromotion() && pSKUItem.getPropertyValue(getCatalogProperties().getSkuQualifiedForPromos()) != null) {
				promos = (List<String>) pSKUItem.getPropertyValue(getCatalogProperties().getSkuQualifiedForPromos());
				List<ProductPromoInfoVO> productPromoInfoList = getPromotionTools().getSkuPromotionDetails(promos);
				if(productPromoInfoList!=null && !productPromoInfoList.isEmpty()) {
					skuInfo.setPromos(productPromoInfoList);
				}	
			}
			skuInfo.setNewItem(getCatalogManager().getCatalogTools().isNewlyAddedInventory(pSKUItem));
			if (pSKUItem.getPropertyValue(getCatalogProperties().getUnCartable()) != null) {
				unCartable = (boolean) pSKUItem
						.getPropertyValue(getCatalogProperties().getUnCartable());
			}
			skuInfo.setUnCartable(unCartable);
			if (pSKUItem.getPropertyValue(getCatalogProperties()
					.getSkuRegistryEligibility()) != null) {
				registryEligibility = (boolean) pSKUItem.getPropertyValue(getCatalogProperties().getSkuRegistryEligibility());
			}
			skuInfo.setRegistryEligibility(registryEligibility);
			if (pSKUItem.getPropertyValue(getCatalogProperties()
					.getSkuPromotionalStickerDisplay()) != null) {
				String sticker = (String) pSKUItem
						.getPropertyValue(getCatalogProperties()
								.getSkuPromotionalStickerDisplay());
				skuInfo.setPromotionalStickerDisplay(sticker);
			}
			if (pSKUItem.getPropertyValue(getCatalogProperties().getSkuReviewRating()) != null) {
				Float reviewRating = (Float) pSKUItem
						.getPropertyValue(getCatalogProperties().getSkuReviewRating());
				if(reviewRating != null) {
					float rating=reviewRating.floatValue();
					int ratingInteger=(int) rating;
					skuInfo.setReviewRating(ratingInteger);
				}
			}
			if(pSKUItem.getPropertyValue(getCatalogProperties().getUpcNumbers())!=null) {
				skuInfo.setUpcNumbers((Set<String>)pSKUItem.getPropertyValue(getCatalogProperties().getUpcNumbers()));
			}
			if(pSKUItem.getPropertyValue(getCatalogProperties().getOriginalParentProduct()) != null){
				skuInfo.setOriginalParentProduct((String)pSKUItem.getPropertyValue(getCatalogProperties().getOriginalParentProduct()));
			}
			if (pSKUItem.getPropertyValue(getCatalogProperties().getOnlinePID()) != null) {
				skuInfo.setOnlinePID((String) pSKUItem
						.getPropertyValue(getCatalogProperties().getOnlinePID()));
			}
			String ispu= (String) pSKUItem.getPropertyValue(getCatalogProperties().getIspu());
			String ispuString=null;
		if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)){
				ispuString=TRUCommerceConstants.YES;
			}else{
				ispuString=TRUCommerceConstants.NO;
			}
		skuInfo.setIspu(ispuString);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.:::::createSKUInfo method");
		}
		return skuInfo;
		
	}
	/**
	 * This method will populate all the variants related information.
	 * 
	 * @param pProductInfo
	 *            - ProductInfo
	 * @param pSkuItems
	 *            - SkuItems
	 */
	public void populateVariantsInformation(RegistryProductInfoVO pProductInfo,
			RepositoryItem pSkuItems) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProductInfoUtil.populateVariantsInformation method..::");
		}
		Set<String> allColorSwatchImageList = new HashSet<String>();
		Map<String, String> colorCodeToSwatchImageMap = new HashMap<String, String>();
		Set<Integer> sizeSequenceList = new HashSet<Integer>();
		Set<Integer> colorSequenceList = new HashSet<Integer>();
		Set<String> sizeSet = new HashSet<String>();
		Map<Integer, String> sequenceTosizeMap = new HashMap<Integer, String>();
		Map<Integer, String> sequenceToColorMap = new HashMap<Integer, String>();
		String colorCode = null;
		String colorSwatchImage = null;
		String colorSequence=null;
		String sizeSequence=null;
		String juvenileSize = null;
		String juvenileSizeDesc = null;
		Integer colorSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer sizeSequenceNumber=TRUCommerceConstants.INT_ZERO;
		Integer count = TRUCommerceConstants.INT_FIVE_THOUSAND;
			RepositoryItem color=(RepositoryItem) pSkuItems.getPropertyValue(getCatalogProperties().getSkuColorCode());
			if(color!=null){
			colorCode=(String) color.getPropertyValue(getCatalogProperties().getColorName());
			colorSequence=(String) color.getPropertyValue(getCatalogProperties().getColorDisplaySequence());
			}
			colorSwatchImage = (String) pSkuItems
					.getPropertyValue(getCatalogProperties().getSkuSwatchImage());
			RepositoryItem size = (RepositoryItem)pSkuItems.getPropertyValue(getCatalogProperties().getSkuJuvenileSize());
			if(size!=null){
				juvenileSize=(String) size.getPropertyValue(getCatalogProperties().getSizeDescription());
				sizeSequence=(String) size.getPropertyValue(getCatalogProperties().getSizeDisplaySequence());
				if(size.getPropertyValue(getCatalogProperties().getDescription())!=null){
				juvenileSizeDesc=(String) size.getPropertyValue(getCatalogProperties().getDescription());
				}
			}
			if (colorSwatchImage != null) {
				allColorSwatchImageList.add(colorSwatchImage);
			}
			if (colorCode != null) {
				colorCode = colorCode.toLowerCase();
				colorCodeToSwatchImageMap.put(colorCode, colorSwatchImage);
				if(colorSequence!=null){
					colorSequenceNumber=Integer.parseInt(colorSequence);
				}else{
					colorSequenceNumber=count;
					count++;
				}
				colorSequenceList.add(colorSequenceNumber);
				sequenceToColorMap.put(colorSequenceNumber, colorCode);
			}
				if (juvenileSizeDesc != null) {
					juvenileSizeDesc = juvenileSizeDesc.toUpperCase();
					if (sizeSequence!=null) {
						sizeSequenceNumber=Integer.parseInt(sizeSequence);
					} else {
						sizeSequenceNumber=count;
						count++;
					}
					sizeSequenceList.add(sizeSequenceNumber);
					sequenceTosizeMap.put(sizeSequenceNumber, juvenileSizeDesc);
				}else if(juvenileSize!=null){
					juvenileSize = juvenileSize.toUpperCase();
					if (sizeSequence!=null) {
						sizeSequenceNumber=Integer.parseInt(sizeSequence);
					} else {
						sizeSequenceNumber=count;
						count++;
					}
					sizeSequenceList.add(sizeSequenceNumber);
					sequenceTosizeMap.put(sizeSequenceNumber, juvenileSize);
				}
		List<String> sortedSizeList = getCatalogManager().getCatalogTools().makeSortedListFromCollection(
				sizeSequenceList, sequenceTosizeMap, sizeSet);
		if (isLoggingDebug()) {
			logDebug(" TRUProductInfoUtil.populateVariantsInformation method..::Sorted size list:::"
					+ sortedSizeList);
		}
		pProductInfo.setColorCodeToSwatchImageMap(colorCodeToSwatchImageMap);
		pProductInfo.setJuvenileSizesList(sortedSizeList);
		
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.populateVariantsInformation method..::");
		}
	}

	/**
	 * This method will query list of SkuItems for the skuIds
	 * 
	 * @param pSkuIds
	 *            - SkuIds
	 * @return itemMap
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 * @throws PriceListException
	 *             If any PriceListException occurs
	 */
	public Map<RepositoryItem, RepositoryItem> getSkuItemsFromSkuIds(
			String[] pSkuIds) throws PriceListException, SiteContextException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUProductInfoUtil.getSkuItemsFromSkuIds");
		}
		RepositoryItem[] skuItems = null;
		Map<RepositoryItem, RepositoryItem> itemMap = new HashMap<RepositoryItem, RepositoryItem>();
		try {
			if (pSkuIds != null && pSkuIds.length > 0) {
				CatalogTools catalogTools = getCatalogManager().getCatalogTools();
				String skuItemName = getCatalogProperties().getRegularSKUItemDescPropertyName();
				RepositoryView view = catalogTools.getCatalog().getView(skuItemName);
				QueryBuilder queryBuilder = view.getQueryBuilder();
				QueryExpression skuPropertyExpression = queryBuilder.createPropertyQueryExpression(TRURestConstants.SKU_ITEM_ID);
				QueryExpression skuConstantExpression = queryBuilder.createConstantQueryExpression(pSkuIds);
				Query skuQuery = queryBuilder.createIncludesQuery(skuConstantExpression, skuPropertyExpression);
				skuItems = view.executeQuery(skuQuery);
				if (skuItems != null) {
					itemMap = loadSkuPropertiesMap(skuItems);
				}
				if (isLoadPrice()) {
					loadPrice(pSkuIds);
				}
			}
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException occured", repositoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.getSkuItemsFromSkuIds method..::");
		}
		return itemMap;
	}
	/**
	 * This method will query list of SkuItems for the onlinePID
	 * @param pOnlinePID - OnlinePID
	 * @return products
	 */
		public RepositoryItem[] getProductItemFromOnlinePID(String[] pOnlinePID) {
			if (isLoggingDebug()) {
				logDebug("BEGIN::TRUProductInfoUtil.getProductIdFromSkuId");
			}
			RepositoryItem[] products = null;
			try {
		      CatalogTools catalogTools = getCatalogManager().getCatalogTools();
		      String skuItemName = getCatalogProperties().getRegularSKUItemDescPropertyName();
		      RepositoryView view = catalogTools.getCatalog().getView(skuItemName);
		      QueryBuilder queryBuilder = view.getQueryBuilder();
		      QueryExpression skuPropertyExpression = queryBuilder.createPropertyQueryExpression(TRURestConstants.ONLINE_ID);
		      QueryExpression skuConstantExpression = queryBuilder.createConstantQueryExpression(pOnlinePID);
		      Query skuQuery = queryBuilder.createIncludesQuery(skuConstantExpression, skuPropertyExpression);
			  products = view.executeQuery(skuQuery);
				if (products != null) {
					return products;
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException occured", repositoryException);
				}
			}
			if (isLoggingDebug()) {
				logDebug("END:: TRUProductInfoUtil.getProductIdFromSkuId method..::");
			}
			return products;
		}
	/**
	 * This method is used to get the ProductItem based on the UPC number.
	 * @param pUPCNumber - UPCNumber
	 * @param pSkuIdList - SkuIdList
	 * @param pItemMap - ItemMap
	 * @param pAllskus - Allskus
	 * @return skuId
	 */
	@SuppressWarnings("unchecked")
	public List<String> getProductIdFromUPCNumber(String[] pUPCNumber, List<String> pSkuIdList, Map<RepositoryItem, RepositoryItem> pItemMap, boolean pAllskus) {
		RepositoryView skuView = null;
		RepositoryItem[] productItem = null;
		List<String> skuId = new ArrayList<String>();
		skuId = new ArrayList<String>(pSkuIdList);
		try {
			CatalogTools catalogTools = getCatalogManager().getCatalogTools();
			 skuView = catalogTools.getCatalog().getView(getCatalogProperties().getRegularSKUItemDescPropertyName());
			 QueryBuilder queryBuilder = skuView.getQueryBuilder();
			 QueryExpression propertyExpression = queryBuilder.createPropertyQueryExpression(getCatalogProperties().getUpcNumbers());
			 QueryExpression UpcExpression = queryBuilder.createConstantQueryExpression(pUPCNumber);
			 Query includesQuery = queryBuilder.createIncludesAnyQuery(propertyExpression, UpcExpression);
			 productItem = skuView.executeQuery(includesQuery);
			 for (RepositoryItem skuItem : productItem) {
					Set<RepositoryItem> parentParent = (Set<RepositoryItem>) skuItem.getPropertyValue(getCatalogProperties().getParentProductsPropertyName());
					if (parentParent != null && !parentParent.isEmpty() && !pItemMap.containsKey(skuItem) && !pItemMap.containsValue((RepositoryItem) parentParent.toArray()[TRURestConstants.INT_ZERO])) {
						pItemMap.put(skuItem, (RepositoryItem) parentParent.toArray()[TRURestConstants.INT_ZERO]);
						skuId.add(skuItem.getRepositoryId());
					}
				}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException occured", e);
			}
		}
		return skuId;
	}
	
	/**
	 * To get the ProductItem from list of onliePID.
	 * @param pOnlinePIds OnlinePIds
	 * @param pSkuIdList SkuIdList
	 * @param pProductItemSet ProductItemSet
	 * @param pAllskus Allskus
	 * @return skuId
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getProductIdsFromOnlinePid(String[] pOnlinePIds, List<String> pSkuIdList, Set<RepositoryItem> pProductItemSet, boolean pAllskus) {
		List<String> skuId = new ArrayList<String>();
		skuId = new ArrayList<String>(pSkuIdList);
			RepositoryItem[] skuList = getProductItemFromOnlinePID(pOnlinePIds);
			if(skuList == null || skuList.length == TRUCommerceConstants.INT_ZERO || skuList[TRUCommerceConstants.INT_ZERO] == null) {
				return pSkuIdList;
			}
		for (RepositoryItem skuItem : skuList) {
			Set<RepositoryItem> parentProducts =  (Set<RepositoryItem>) skuItem.getPropertyValue(getCatalogProperties().getParentProductsPropertyName());
			if(parentProducts != null && !parentProducts.isEmpty()) {
				Iterator iter = parentProducts.iterator();
				RepositoryItem productItem = (RepositoryItem) iter.next();
					pProductItemSet.add(productItem);
				List<RepositoryItem> childSKUs = (List<RepositoryItem>) productItem.getPropertyValue(getCatalogProperties().getChildSkusPropertyName());
					if (!childSKUs.isEmpty() && !pAllskus) {
					for (RepositoryItem sku : childSKUs) {
							if (sku.getRepositoryId().equals(skuItem.getRepositoryId()) && !skuId.contains(sku.getRepositoryId())) {
								skuId.add(sku.getRepositoryId());
							}
						}
					}
				}
			}
		return skuId;
	}
	
	/**
	 * Used to redirect the request.
	 * 
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath
	 *            - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
		protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
		{
			return new RedirectAwareContentInclude(pResourcePath);
		}
	/**
	 * @return the mCatalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}
	/**
	 * @param pCatalogManager the mCatalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		this.mCatalogManager = pCatalogManager;
	}
	/**
	 * @return the mPricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}
	/**
	 * @param pPricingTools the mPricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
		
		
	}
	/**
	 * @return the mSiteUtil
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}
	/**
	 * @param pSiteUtil the mSiteUtil to set
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		this.mSiteUtil = pSiteUtil;
	}

	/**
	 * @return the mAssemblerTools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the mAssemblerTools to set
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		this.mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the mRestConfiguration
	 */
	public TRURestConfiguration getRestConfiguration() {
		return mRestConfiguration;
	}

	/**
	 * @param pRestConfiguration the mRestConfiguration to set
	 */
	public void setRestConfiguration(TRURestConfiguration pRestConfiguration) {
		this.mRestConfiguration = pRestConfiguration;
	}

	/**
	 * @return the mProductPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * @param pProductPageUtil the mProductPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}

	/**
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		this.mPromotionTools = pPromotionTools;
	}

	/**
	 * @return the mCatalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the mCatalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}
	
	
	/**
	 * @return the mDefaultSalePriceListPropertyName
	 */
	public String getDefaultSalePriceListPropertyName() {
		return mDefaultSalePriceListPropertyName;
	}

	/**
	 * @param pDefaultSalePriceListPropertyName the mDefaultSalePriceListPropertyName to set
	 */
	public void setDefaultSalePriceListPropertyName(String pDefaultSalePriceListPropertyName) {
		this.mDefaultSalePriceListPropertyName = pDefaultSalePriceListPropertyName;
	}

/**
	 * @return the mLoadPrice
	 */
	public boolean isLoadPrice() {
		return mLoadPrice;
	}

	/**
	 * @param pLoadPrice the mLoadPrice to set
	 */
	public void setLoadPrice(boolean pLoadPrice) {
		this.mLoadPrice = pLoadPrice;
	}

/**
	 * @return the mEnablePromotion
	 */
	public boolean isEnablePromotion() {
		return mEnablePromotion;
	}

	/**
	 * @param pEnablePromotion the mEnablePromotion to set
	 */
	public void setEnablePromotion(boolean pEnablePromotion) {
		this.mEnablePromotion = pEnablePromotion;
	}

/**
 * This method loads productLevel properties
 * @param pProdList - ProdList
 */
	public void loadProductsProperties(List<RepositoryItem> pProdList) {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUProductInfoUtil.loadProductsProperties");
		}
		for (RepositoryItem prodtem : pProdList) {
			prodtem.getPropertyValue(getCatalogProperties().getProductRusItemNumber());
			prodtem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
			prodtem.getPropertyValue(getCatalogProperties().getProductStyleDimension());
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.loadProductsProperties method..::");
		}
	}	

	/**
	 * This method loads the skuLeavel properties
	 * 
	 * @param pSkuItems
	 *            - SkuItems
	 * @return itemMap return the skuItem itemMap
	 */
		@SuppressWarnings("unchecked")
		public Map<RepositoryItem,RepositoryItem>  loadSkuPropertiesMap(RepositoryItem[] pSkuItems) {
			if (isLoggingDebug()) {
				logDebug("BEGIN::TRUProductInfoUtil.loadSkuPropertiesMap");
			}
		Map<RepositoryItem, RepositoryItem> itemMap = new HashMap<RepositoryItem, RepositoryItem>();
		List<RepositoryItem> productItemList = null;
		if (pSkuItems != null) {
			productItemList = new ArrayList<RepositoryItem>();
			for (RepositoryItem skuItem : pSkuItems) {
				Set<RepositoryItem> parentParent = (Set<RepositoryItem>) skuItem.getPropertyValue(getCatalogProperties().getParentProductsPropertyName());
				if (parentParent != null && !parentParent.isEmpty()) {
					itemMap.put(skuItem, (RepositoryItem) parentParent.toArray()[TRURestConstants.INT_ZERO]);
					productItemList.add((RepositoryItem) parentParent.toArray()[TRURestConstants.INT_ZERO]);
				}
				skuItem.getPropertyValue(getCatalogProperties().getUpcNumbers());
				skuItem.getPropertyValue(getCatalogProperties().getUnCartable());
				skuItem.getPropertyValue(getCatalogProperties().getSkuPromotionalStickerDisplay());
				skuItem.getPropertyValue(getCatalogProperties().getSkuReviewRating());
				skuItem.getPropertyValue(getCatalogProperties().getInventoryFirstAvailableDate());
				skuItem.getPropertyValue(getCatalogProperties().getOnlinePID());
				skuItem.getPropertyValue(getCatalogProperties().getSkuJuvenileSize());
				skuItem.getPropertyValue(getCatalogProperties().getSkuColorCode());
				skuItem.getPropertyValue(getCatalogProperties().getSkuSwatchImage());
				skuItem.getPropertyValue(getCatalogProperties().getSkuQualifiedForPromos());
				skuItem.getPropertyValue(getCatalogProperties().getIspu());

			}
			if (productItemList != null) {
				loadProductsProperties(productItemList);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.loadSkuPropertiesMap method..::");
		}
		return itemMap;
	}
	/**
	 * This method will query list of skuId from coheranceInventory system
	 * 
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 */
	public void queryBulkS2HStockLevel() throws SiteContextException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUProductInfoUtil.loadSKUInventory");
		}
		Map<String, TRUInventoryInfo> inventoryInfoMap = null;
		Map<String, Long> inventoryInfo = new HashMap<String, Long>();
		List<String> catalogRefIds = null;
		if (getLoadSkus() != null && !getLoadSkus().isEmpty()) {
			catalogRefIds = getCatalogManager().getCatalogTools().spliteProductId(getLoadSkus());
			inventoryInfoMap = prepareInventory(catalogRefIds);
			if (inventoryInfoMap != null && !inventoryInfoMap.isEmpty()) {
				for (Map.Entry<String, TRUInventoryInfo> childSkuItem : inventoryInfoMap.entrySet()) {
					inventoryInfo.put(childSkuItem.getKey(), childSkuItem.getValue().getQuantity());
				}
				setTestInventoryInfoMap(inventoryInfo);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.loadSKUInventory method..::");
		}
	}	
	/**
	 * This method load the sales and list priceItem
	 * 
	 * @param pSkuIds
	 *            - SkuIds
	 * @throws PriceListException
	 *             - If any PriceListException occurs
	 * @throws SiteContextException
	 *             - If any SiteContextException occurs
	 * @throws RepositoryException
	 *             - If any RepositoryException occurs
	 */
	public void loadPrice(String[] pSkuIds) throws PriceListException, SiteContextException, RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUProductInfoUtil.loadPrice");
		}
		List<RepositoryItem> listPriceItem = null;
		List<RepositoryItem> salePriceItem = null;
		RepositoryItem priceList = null;
		RepositoryItem salePriceList = null;
		Site site = SiteContextManager.getCurrentSite();
		if (site != null) {
			salePriceList = (RepositoryItem) site.getPropertyValue(getDefaultSalePriceListPropertyName());
			priceList = (RepositoryItem) site.getPropertyValue(getDefaultPriceListPropertyName());
			salePriceItem = getProductPrices(salePriceList, pSkuIds);
			listPriceItem = getProductPrices(priceList, pSkuIds);
			if (salePriceItem != null && listPriceItem != null) {
				Iterator<RepositoryItem> salesPriceItems = salePriceItem.iterator();
				Iterator<RepositoryItem> listPriceItems = listPriceItem.iterator();
				while (salesPriceItems.hasNext() && listPriceItems.hasNext()) {
					salePriceList = salesPriceItems.next();
					priceList = listPriceItems.next();
					salePriceList.getPropertyValue(getPricingTools().getPriceProperties().getListPricePropertyName());
					priceList.getPropertyValue(getPricingTools().getPriceProperties().getListPricePropertyName());
				}
			}
			site.getPropertyValue(getPricingTools().getPriceProperties().getThresholdPricePercentagePropertyName());
			site.getPropertyValue(getPricingTools().getPriceProperties().getThresholdPriceAmountPropertyName());
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.loadPrice method..::");
		}
	}

	/**
	 * Query the sales and list price items by skuIds
	 * 
	 * @param pPriceList
	 *            - PriceList
	 * @param pSkuId
	 *            - SkuId
	 * @return priceItemList return the priceItemList
	 * @throws RepositoryException
	 *             - If any RepositoryException occurs
	 */
	public List<RepositoryItem> getProductPrices(RepositoryItem pPriceList, String[] pSkuId) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("BEGIN::TRUProductInfoUtil.getProductPrices");
		}
		List<RepositoryItem> priceItemList = null;
		Query priceListMatch = null;
		QueryExpression priceList = null;
		RepositoryItem currPriceList = null;
		Query[] queries = null;
		Query findPrice = null;
		RepositoryItem[] prices = null;
		Repository priceListRep = getPriceListRepository();
		RepositoryView priceView = priceListRep.getView(TRURestConstants.PRICE);
		QueryBuilder qb = priceView.getQueryBuilder();
		QueryExpression priceListProperty = qb.createPropertyQueryExpression(TRURestConstants.PRICE_LIST);
		priceList = qb.createConstantQueryExpression(pPriceList);
		priceListMatch = qb.createComparisonQuery(priceListProperty, priceList,TRURestConstants.INT_FOUR);
		QueryExpression pricePropertyExpression = qb.createPropertyQueryExpression(TRURestConstants.SKUID);
		QueryExpression priceConstantExpression = qb.createConstantQueryExpression(pSkuId);
		Query skuQuery = qb.createIncludesQuery(priceConstantExpression,pricePropertyExpression);
		queries = new Query[TRURestConstants.INT_TWO];
		queries[TRURestConstants.INT_ZERO] = skuQuery;
		queries[TRURestConstants.INT_ONE] = priceListMatch;
		findPrice = qb.createAndQuery(queries);
		prices = priceView.executeQuery(findPrice);
		if (prices == null) {
			currPriceList = null;
			RepositoryItem basePriceList = (RepositoryItem) pPriceList.getPropertyValue(getPricingTools().getPriceProperties().getBasePriceList());
			currPriceList = (RepositoryItem) basePriceList.getPropertyValue(getPricingTools().getPriceProperties().getBasePriceList());
			priceList = qb.createConstantQueryExpression(currPriceList);
			priceListMatch = qb.createComparisonQuery(priceListProperty, priceList,TRURestConstants.INT_FOUR);
			queries = new Query[TRURestConstants.INT_TWO];
			queries[TRURestConstants.INT_ZERO] = skuQuery;
			queries[TRURestConstants.INT_ONE] = priceListMatch;
			findPrice = qb.createAndQuery(queries);
			prices = priceView.executeQuery(findPrice);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUProductInfoUtil.getProductPrices method..::");
		}
		if (prices != null) {
			priceItemList = new ArrayList<RepositoryItem>(Arrays.asList(prices));	
		}
		return priceItemList;
	}
}
