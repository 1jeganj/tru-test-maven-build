package com.tru.rest.constants;

import atg.nucleus.naming.ParameterName;

/**
 *  This class hold all Mobile Rest WebService Constant.
 *  @author Professional Access
 * 	@version 1.0
 */


public class TRURestConstants {

	/** Constant for API_KEY. */
	public static final String API_KEY = "X-APP-API_KEY";
	
	/**
	 * Constant for ERROR_MSG.
	 */
	public static final String ERROR_MSG = "errorMsg";
	
	/**
	 * Constant for ERR_INVALID_REQ.
	 */
	public static final String ERR_INVALID_REQ = "Invalid Request";
	/**
	 * Constant for ERR_INVALID_SITE.
	 */
	public static final String ERR_INVALID_SITE = "Invalid Site";
	/** 
	 * Constant for API_CHANNEL.
	 **/
	public static final String API_CHANNEL = "X-APP-API_CHANNEL";
	
	/**
	 * Constant for registry_id.
	 **/
	public static final String REGISTRY_ID = "registry_id";
	/**
	 * Constant for REGISTRY_USER.
	 */
	public static final String REGISTRY_USER = "REGISTRY_USER";

	/**
	 * Constant for OUT PARAM.
	 */
	public static final String OUTPUT_PARAM = "output";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String EDQ = "nonReg";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String CATEGORY_OUTPUT = "catOutput";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String SUB_CATEGORY_OUTPUT = "subCatOutput";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String SEARCH_OUTPUT = "searchOutput";
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String FILTER_STRING = "filterString";
	
	/**
	 * Constant for OUT PARAM.
	 */
	public static final String PRODUCT_LIST_OUTPUT = "productListOutput";
	
	/**
	 * Constant for Access-Control-Allow-Origin.
	 */
	public static final String ACCESS_CONTROL = "Access-Control-Allow-Origin";
	
	/**
	 * Constant for *.
	 */
	public static final String ACCESS_CONTROL_ALLOW = "*";
	
	/**
	 * Constant for Access-Control-Allow-Methods.
	 */
	public static final String ACCESS_CONTROL_METHODS = "Access-Control-Allow-Methods";
	
	/**
	 * Constant for "POST, GET, OPTIONS, DELETE". 
	 */
	
	public static final String ACCESS_METHODS = "POST, GET, OPTIONS, DELETE";
	/**
	 * Constant for Access-Control-Max-Age.
	 */
	public static final String ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
	
	/**
	 * Constant for Access-Max-Age.
	 */
	
	public static final String ACCESS_CONTROL_AGE="3600";
	/**
	 * Constant for Access-Control-Allow-Headers.
	 */
	public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	
	/**
	 * Constant for Origin.
	 */
	public static final String ACCEPT_ORIGIN = "X-Requested-With,accept,content-type,authorization,X-APP-API_KEY,X-APP-API_CHANNEL";

	/**
	 * Constant for Credential.
	 */
	
	public static final String ACCESS_CONTROL_ALLOW_CREDENTIAL = "Access-Control-Allow-Credentials";
	/**
	 * Constant for Access origin.
	 */
	
	public static final String ALLOW_ACCESS = "true";
	/**
	 * Constant for Control expose header.
	 */

	public static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
	
	/**
	 * Constant for API KEY.
	 */

	public static final String API_KEY_CHANNEL = "X-APP-API_KEY,X-APP-API_CHANNEL";
	
	/**
	 * Constant for Allow option.
	 */

	public static final String ALLOW_OPTION = "OPTIONS";
	
	/**
	 * Constant for Allow STATUS_CODE.
	 */

	public static final Integer STATUS_CODE = 200;

	/**
	 * Constant for Allow CONTENT_ITEM.
	 */

	public static final String CONTENT_ITEM = "contentItem";

	/**
	 * Constant for Allow EMPTY_PARAM.
	 */

	public static final String EMPTY_PARAM = "empty";

	/**
	 * Constant for Allow ERROR_INFO.
	 */

	public static final String ERROR_INFO = "errorInfo";

	/**
	 * Constant for Allow TRY_LATER.
	 */

	public static final String TRY_LATER = "System error";

	/**
	 * Constant for Allow INPUT_VALUES.
	 */

	public static final String INPUT_VALUES = "Please provide valid input values";

	/**
	 * Constant for Allow UTF_EIGHT.
	 */

	public static final String UTF_EIGHT = "UTF-8";

	/**
	 * Constant for Allow AND.
	 */

	public static final String AND = "&";

	/**
	 * Constant for Allow EQUALS.
	 */

	public static final String EQUALS = "=";
	
	/**
	 * Constant for Allow EQUALS.
	 */

	public static final String QUERY = "?";

	/**
	 * Constant for Allow INT_ZERO.
	 */

	public static final int INT_ZERO = 0;

	/**
	 * Constant for Allow INT_ONE.
	 */

	public static final int INT_ONE = 1;
	/**
	 * Constant for Allow INT_TWO.
	 */
	public static final int INT_TWO = 2;
	/**
	 * Constant for Allow INT_THREE.
	 */
	public static final int INT_THREE = 3;
	/**
	 * Constant for Allow INT_FOUR.
	 */
	public static final int INT_FOUR = 4;
	
	/**
	 * Constant for Allow INT_FIVE.
	 */
	public static final int INT_FIVE = 5;
	
	/**
	 * Constant for Allow INT_TEN.
	 */
	public static final int INT_TWELVE = 12;
	
	/**
	 * Constant for Allow INT_TEN.
	 */
	public static final int INT_EIGHT = 8;

	/**
	 * Constant for Allow NINE.
	 */
	
	public static final int INT_NINE = 9;
	/**
	 * Constant for Allow QS_NTT.
	 */
	public static final String SEARCH_KEY = "keyword";

	/**
	 * Constant for Allow LABEL.
	 */

	public static final String LABEL = "label";

	/**
	 * Constant for Allow PARAM_SEARCH.
	 */

	public static final String PARAM_SEARCH = "keyword=";

	/**
	 * Constant for Allow MOBILE.
	 */

	public static final String MOBILE = "mobile";

	/**
	 * Constant for Allow REGISTRY.
	 */

	public static final String REGISTRY = "registry";

	/**
	 * Constant for Allow WISHLIST.
	 */

	public static final String WISHLIST = "wishlist";
	
	/**
	 * Constant for Allow NAVIGATIONSTATE.
	 */

	public static final String NAVIGATIONSTATE = "NavigationState";
	
	/**
	 * Constant for Allow CATEGORYID.
	 */

	public static final String CATEGORYID = "categoryId";
	
	/**
	 * Constant for productId.
	 */
	public static final ParameterName PRODUCT_ID = ParameterName.getParameterName("productIds");
	/**
	 * Constant for allSkus.
	 */
	public static final ParameterName ALL_SKUS = ParameterName.getParameterName("allSkus");
	/**
	 * Constant for skuDetails.
	 */
	public static final ParameterName SKU_DETAILS = ParameterName.getParameterName("skuDetail");
	/**
	 * Constant for productId.
	 */
	public static final ParameterName ALL_INVENTORY = ParameterName.getParameterName("allInventory");
	
	/** Constant to hold productInfo. */
	public static final String PRODUCT_INFO = "productInfo";
	
	/**
	 * Constant for skuId.
	 */
	public static final ParameterName SHIP_GROUPID = ParameterName.getParameterName("shippingGroupId");
	
	/**
	 * Constant for skuId.
	 */
	public static final ParameterName GIFT_MESSAGE = ParameterName.getParameterName("giftWrapMessage");
	
	/**
	 * Constant for skuId.
	 */
	public static final ParameterName SKU_CODE = ParameterName.getParameterName("skuCode");
	
	/**
	 * property to hold the rest context path.
	 */
	public static final String REST_CONTEXT_PATH = "/rest";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String CONTENT_PATH = "contentPath";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String AJAX_PAGE = "/ajaxpage";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String AJAX_RESULTLIST = "/ajaxResultlist";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String TWO_LEVEL_CAT_MAP = "twoLevelCategoryTreeMap";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String CURRENT_CATEGORY_ID = "CurrentCategoryId";
	
	/**
	 * property to hold the rest content path.
	 */
	public static final String CURRENT_CATEGORY_NAME = "CurrentCategoryName";
	
	/**
	 * Constant for Allow PDP.
	 */
	public static final String PDP = "/pdp";
	
	/**
	 * property to hold contents.
	 */
	public static final String CONTENTS = "contents";

	/**
	 * property to hold MainContent.
	 */
	public static final String MAIN_CONTENT = "MainContent";

	/**
	 * property to hold  @type value.
	 */	
	public static final String TYPE = "@type";

	/**
	 * property to hold TRUPDPBreadcrumbs.
	 */
	public static final String TRU_PDP_BREADCRUMBS = "TRUPDPBreadcrumbs";
	
	/**
	 * property to hold PDPBreadCrumbList.
	 */
	public static final String PDP_BREADCRUMB_LIST = "PDPBreadCrumbList";
	/**
	 * property to hold productPromotions.
	 */
	
	public static final String PRODUCT_PROMOTION = "productPromotions";
	
	/**
	 * property to hold promotionDetails.
	 */
	
	public static final String PROMOTION_DETAILS = "promotionDetails";
	
	/**
	 * property to hold description.
	 */
	
	public static final String PROMO_DESCRIPTION = "description";
	
	/**
	 * property to hold SORT_REQ.
	 */
	
	public static final String SORT_REQ = "Ns=";
	/**
	 * property to hold TRUTypeAheadResultList.
	 */
	public static final String TYPEAHEAD_RESULTLIST = "TRUTypeAheadResultList";
	
	/**
	 * property to hold TRUDimensionSearchAutoSuggestItem.
	 */
	public static final String DIMENSION_SEARCH_AUTOSUGGEST = "TRUDimensionSearchAutoSuggestItem";
	
	/**
	 * property to hold dimensionSearchGroups.
	 */
	public static final String DIMENSION_SEARCH_GROUPS = "dimensionSearchGroups";
	
	/**
	 * property to hold autoSuggest.
	 */
	public static final String TYPEAHEAD_NAME = "autoSuggest";
	/**
	 * property to hold pageOutput.
	 */
	public static final String PAGE_OUTPUT = "pageOutput";
	
	/**
	 * Constant for Allow UNAUTHORIZED_STATUS_CODE.
	 */

	public static final Integer UNAUTHORIZED_STATUS_CODE = 401;
	
	/**
	 * property to hold S.
	 */

	public static final String S = "s";
	
	/**
	 * property to hold ZERO.
	 */

	public static final String ZERO = "0";
	
	/**
	 * property to hold application/json.
	 */

	public static final String CONTENT_TYPE = "application/json";
	
	/**
	 * property to hold REST_CONTEXT.
	 */
	public static final String REST_CONTEXT = "/rest";
	
	/**
	 * property to hold REC_LIMIT.
	 */
	public static final String REC_LIMIT = "Nrpp";
	
	/**
	 * property to hold NRPP_PARAM.
	 */
	public static final String NRPP_PARAM = "Nrpp=";
	
	/**
	 * property to hold OFFSET.
	 */
	public static final String OFFSET = "No";
	
	/**
	 * property to hold SHOP_BY.
	 */
	public static final String SHOP_BY = "shopBy";
	/**
	 * property to hold SHOP_BY.
	 */
	public static final String APPLIED_COUPON = "appliedCoupon";
	
	/**
	 * property to hold PAGE_NAME_CONFIRM.
	 */
	public static final String PAGE_NAME_CONFIRM = "orderConfirm";
	
	/**
	 * property to hold NO_PARAM.
	 */
	public static final String NO_PARAM = "No=";
	
	/**
	 * property to hold REPLACE_NRPP.
	 */
	public static final String REPLACE_NRPP = "%7BrecordsPerPage%7D";
	
	/**
	 * property to hold REPLACE_NO.
	 */
	public static final String REPLACE_NO = "%7Boffset%7D";
	/**
	 * property to hold INVALID_PRODUCT_ID.
	 */
	public static final String INVALID_PRODUCT_ID = "Invalid product id";
	/**
	 * property to hold REDIRECT.
	 */
	public static final String REDIRECT="endeca:redirect";
	/**
	 * property to hold REDIRECT_TYPE.
	 */
	public static final String REDIRECT_TYPE="Redirect";
	/**
	 * property to hold LINK.
	 */
	public static final String LINK="link";
	/**
	 * property to hold DidYouMean.
	 */
	public static final String DID_YOU_MEAN="DidYouMean";
	/**
	 * property to hold searchAdjustments.
	 */
	public static final String SEARCH_ADJUSTMENTS="searchAdjustments";
	/**
	 * property to hold NoResults.
	 */
	public static final String NO_RESULTS="NoResults";
	/**
	 * property to hold SearchResults.
	 */
	public static final String SEARCH_RESULTS="SearchResults";
	/**
	 * property to hold collectionId.
	 */
	public static final String COLLECTION_ID = "collectionId";
	
	/**
	 * property to hold collectionProductInfo.
	 */
	public static final String COLLECTION_PRODUCT_INFO = "collectionProductInfo";
	
	/**
	 * property to hold UPC_NUMBER.
	 */
	public static final String UPC_NUMBER = "upc";
	
	/**
	 * property to hold SPLITE_PRODUCTIDS.
	 */
	public static final String SPLIT_PIPE = "\\|";
	
	/**
	 * property to hold SPLITE_PRODUCTIDS.
	 */
	public static final String SPLIT_DOUBLE_PIPE = "||";
	
	/**
	 * property to hold PRODUCT_TYPE.
	 */
	public static final String PRODUCT = "product";
	
	/**
	 * property to hold EMPTY_SPACE.
	 */
	public static final String EMPTY_SPACE = "";
	
	/** The Constant OPEN.
	 */
	public static final String OPEN="\\[";
	
	/** The Constant CLOSE. */
	public static final String CLOSE="\\]";
	
	/** Holds string Constant TYPE. */
	public static final String PRODUCT_TYPE = "type";

	/** Holds string Constant COLLECTION_TYPE. */

	public static final String COLLECTION_TYPE = "collectionProduct";

	/** Holds string Constant PRODUCT_COMPARE_ID. */

	public static final String PRODUCT_COMPARE_ID = "compareProds";

	/** Holds string Constant PRODUCT_LIST. */

	public static final String PRODUCT_LIST = "productList";
	
	/** Holds string  Constant COLLECTION_TYPE. */
	
	public static final String SPLIT_COMMA = "\\,";
	
	/** Holds string  Constant CHANNEL_AVAILABILITY_STATUS. */
	
	public static final String CHANNEL_AVAILABILITY_STATUS = "NA";
	
	/** Constant for RESPONSE_STATUS. */
	public static final String RESPONSE_STATUS = "responseStatus";
	
	/** Constant for LAST_ACCESS_REGISTRY_ID. */
	public static final String LAST_ACCESS_REGISTRY_ID = "lastAccessedRegistryID";
	
	/** Constant for LAST_ACCESS_WISHLIST_ID. */
	public static final String LAST_ACCESS_WISHLIST_ID = "lastAccessedWishlistID";
	
	
	/** Constant for LAST_ACCESS_WISHLIST_NAME. */
	public static final String LAST_ACCESS_WISHLIST_NAME = "lastAccessedWishlistName";
	

	/** Constant for LAST_ACCESS_REGISTRY_NAME. */
	public static final String LAST_ACCESS_REGISTRY_NAME = "lastAccessedRegistryName";
	/** Constant for MY_WISHLIST_ID. */
	public static final String MY_WISHLIST_ID = "myWishlistID";
	
	/**
	 * Constant for WISHLIST_ID.
	 **/
	public static final String WISHLIST_ID = "wishlist_id";
	
	/** Constant for RETRIEVE_REGISTRY_COOKIE. */
	public static final String RETRIEVE_REGISTRY_COOKIE = "retrieveRegistryCookie";
	
	/** The Constant TRUE. */
	public static final String TRUE="true";
	
	/** The Constant FALSE. */
	public static final String FALSE = "false";
	
	/** Holds string  Constant FREE_STORE_PICK_UP_NOT_AVAILABLE. */
	public static final String FREE_STORE_PICK_UP_NOT_AVAILABLE = "N";
	
	/** Holds string  Constant FREE_STORE_PICK_UP_NOT_AVAILABLE. */
	public static final boolean BOOLEAN_TRUE = true;
	
	/** Holds string  Constant FREE_STORE_PICK_UP_NOT_AVAILABLE. */
	public static final boolean BOOLEAN_FALSE = false;
	
	/** Constant for NICKNAME. */
	public static final String NICKNAME = "nickName";
	
	/** Constant for ADDRESSMAP. */
	public static final String ADDRESSMAP = "addressMap";
	
	/** Constant for ADDRESS. */
	public static final String ADDRESS = "address";
	
	/** Constant for GIFT_OPTIONS_INFO_COUNT. */
	public static final String GIFT_OPTIONS_INFO_COUNT = "giftOptionsInfoCount";
	
	/** Constant for SPLIT_SYMBOL_COMA. */
	public static final String SPLIT_SYMBOL_COMA = ",";
	
	/** Constant for GIFT_OPTION_INFO_COLLECTION. */
	public static final String GIFT_OPTION_INFO_COLLECTION = "giftOptionsInfoCollection";
	
	/** Constant for GIFT_OPTIONS_LIST. */
	public static final String GIFT_OPTIONS_LIST = "giftOptionsList";
	
	/** Constant for COLLECTION_INPU.T. */
	public static final String COLLECTION_INPUT = "collection";
	
	/** Constant for ITEM_ARRAY. */
	public static final String ITEM_ARRAY = "itemArray";
	
	/**
	 * Constant for Allow UNSUPPORTED_COUNTRY.
	 */

	public static final String UNSUPPORTED_COUNTRY = "Unsupported country code";
	/**
	 * Constant for Allow US_COUNTRY_CODE.
	 */

	public static final String US_COUNTRY_CODE = "US";
	
	/** Constant for INSTORE_PICUP_INFO_ARRAY. */
	public static final String INSTORE_PICUP_INFO_ARRAY = "inStorePickupInfoArray";

	/** Constant for ARRAY_SIZE. */
	public static final String ARRAY_SIZE = "arraySize";
	
	/** Constant for ONLINE_PID. */
	public static final String ONLINE_PID = "onlinePId";

	/** Constant for OUTPUT_PARAM_REGISTRY. */
	public static final String OUTPUT_PARAM_REGISTRY = "registryoutput";
	
	/** Constant for PDP_ONLINE_ID_QUERY. */
	public static final String PDP_ONLINE_ID_QUERY = "A=";
	
	/** Constant for PARENT_CATEGORY_ID. */
	public static final String PARENT_CATEGORY_ID = "catId";
	
	/** Constant for PARENT_CATEGORY_NAME. */
	public static final String PARENT_CATEGORY_NAME = "catName";
	
	/** Constant for REQ_HEADER_ORIGIN. */
	public static final String REQ_HEADER_ORIGIN = "ORIGIN";
	
	/** Constant for ALL_CHANNEL_ORIGINS. */
	public static final String ALL_CHANNEL_ORIGINS = "allChannelOrigins";
	
	/** Constant for CATEGORY_REPOSITORY_ID. */
	public static final String CATEGORY_REPOSITORY_ID = "category.repositoryId";
	
	/** Constant for ACTOR_ERROR. */
	public static final String ACTOR_ERROR = "error";
	
	/** Constant for ACTOR_SUCCESS. */
	public static final String ACTOR_SUCCESS = "success";
	
	/** Constant for ACTOR_AUTH_ACTOR. */
	public static final String ACTOR_AUTH_ACTOR = "securitystatusactor";
	
	/** Constant for ACTOR_AUTH_ACTOR. */
	public static final String KEYS = "keys";
	
	/** Constant for ACTOR_AUTH_ACTOR. */
	public static final String MESSAGES = "messages";
	
	/** Constant for defaultShipping. */
	public static final String DEFAULT_SHIPPING = "defaultShipping";
	
	/** Constant for defaultShipping. */
	public static final String SHIPPING_TAB = "shipping-tab";
	
	/** Constant for defaultShipping. */
	public static final String GIFTING_TAB = "gifting-tab";
	
	/** Constant for defaultShipping. */
	public static final String PICKUP_TAB = "pickup-tab";
	
	/** Constant for defaultShipping. */
	public static final String PAYMENT_TAB = "payment-tab";
	
	/** Constant for defaultShipping. */
	public static final String REVIEW_TAB = "review-tab";
	
	/** Constant for defaultShipping. */
	public static final String PAGE_NAME = "pageName";
	
	/** Constant for defaultShipping. */
	public static final String ORDER = "order";
	
	/** Constant for PDP_URL. */
	public static final String PDP_URL = "pdpurl";
	
	/** Constant for GET_CART. */
	public static final String GET_CART = "shoppingcartactor/getcart";
	
	/** Constant for CART_LOAD. */
	public static final String CART_LOAD = "cartLoad";
	
	/** Constant for CONTENT_TYPE_URL. */
	public static final String CONTENT_TYPE_URL = "application/x-www-form-urlencoded";
	
	/** Constant for EMPERSAND. */
	public static final String EMPERSAND = "&amp;";
	
	/** Constant for CATEGORY_ID. */
	public static final String AND_CATEGORY_EQUAL= "&categoryid=";
	
	/** Constant for CATEGORY_ID. */
	public static final String CATEGORY_EQUAL= "categoryid=";
	
	/** Constant for N_VALUE. */
	public static final String N_VALUE= "?N";
	
	/** Constant for HTTP. */
	public static final String HTTP = "http://";
	
	/** Constant for NAVIGATION_EQUAL. */
	public static final String NAVIGATION_EQUAL = "N=";
	
	/** Constant for RELATIONSHIP_ID. */
	public static final String RELATIONSHIP_ID = "relationShipId";
	
	/** Constant for activePromotions. */
	public static final String ACTIVE_PROMOTIONS = "activePromotions";

	/** Constant for productId=. */
	public static final String PRODUCTID_EQUAL = "productId=";
	
	/** Constant for ONE_STR. */
	public static final String ONE_STR = "1";
	
	/** Constant for YES. */
	public static final String YES = "Y";
	
	/** Constant for action. */
	public static final String ACTION = "action";
	
	/** Constant for shopByBrand. */
	public static final String SHOP_BY_BRAND = "shopByBrand";
	
	/** Constant for shopByCharacter. */
	public static final String SHOP_BY_CHARACTER = "shopByCharacter";
	
	/** Constant for confirm. */
	public static final String CONFIRM = "confirm";
	
	/** Constant for MainHeader. */
	public static final String MAIN_HEADER = "MainHeader";
	
	/** Constant for ShopByDimension. */
	public static final String SHOP_BY_DIMENSION = "ShopByDimension";
	
	/** Constant for dimensionsList. */
	public static final String DIMENSION_LIST = "dimensionsList";
	
	/** Constant for SEMICOLON. */
	public static final String SEMICOLON = ";";
	
	/** Constant for RECORD_SPEC. */
	public static final String RECORD_SPEC = "recordspec";
	
	/** Constant for ENCODED_AMPERSAND. */
	public static final String ENCODED_AMPERSAND = "%26";
	
	/** Constant for START_SERVICE. */
	public static final String START_SERVICE = "START TRUMobileProductInfoLookupDroplet service method";
	
	/** Constant for END_SERVICE. */
	public static final String END_SERVICE = "END TRUMobileProductInfoLookupDroplet service method";
	
	/** Constant for REGISTRY_START_SERVICE. */
	public static final String REGISTRY_START_SERVICE = "START TRURegistryProductInfoLookupDroplet service method";
	
	/** Constant for REGISTRY_END_SERVICE. */
	public static final String REGISTRY_END_SERVICE = "END TRURegistryProductInfoLookupDroplet service method";
	
	/** Constant for START_PRICE_METHOD. */
	public static final String START_PRICE_METHOD = "START TRUProductInfoUtil setItemPrice method";
	
	/** Constant for END_PRICE_METHOD. */
	public static final String END_PRICE_METHOD = "END TRUProductInfoUtil setItemPrice method";
	
	/** Constant for START_ENDECA_METHOD. */
	public static final String START_ENDECA_METHOD = "START TRUProductInfoUtil populateBreadcrumbDetails method";
	
	/** Constant for END_ENDECA_METHOD. */
	public static final String END_ENDECA_METHOD = "END TRUProductInfoUtil populateBreadcrumbDetails method";
	
	/** Constant for START_ENDECA_METHOD. */
	public static final String START_INVENTORY_CALL = "START TRUProductInfoUtil prepareInventory method";
	
	/** Constant for END_ENDECA_METHOD. */
	public static final String END_INVENTORY_CALL = "END TRUProductInfoUtil prepareInventory method";
	
	/** Constant for SOS. */
	public static final String SOS = "sos";
	
	/** Constant for IS_SOS_REQUEST. */
	public static final String IS_SOS_REQUEST = "isSOSRequest";
	
	/** Constant for STORE_ID. */
	public static final String STORE_ID = "locationId";
	
	/** Constant for PLUS. */
	public static final String PLUS = "+";
	
	/** constants for KEYWORDS. */
	public static final String KEYWORDS = "keywords";
	
	/** constants for PRODUCT_SEO_DESCRIPTION. */
	public static final String PRODUCT_SEO_DESCRIPTION = "productSeoDescription";
	
	/** constants for PRODUCT_SEO_H1_TEXT. */
	public static final String PRODUCT_SEO_H1_TEXT = "productSeoH1Text";
	
	/** constants for PRODUCT_SEO_TITLE. */
	public static final String PRODUCT_SEO_TITLE = "productSeoTitle";
	
	/** constants for PDP_PAGE. */
	public static final String PDP_PAGE = "pdp";
	
	/** constants for CAT_SEO_TITLE. */
	public static final String CAT_SEO_TITLE = "catSeoTitle";
	
	/** constants for CAT_SEO_H1_TEXT. */	
	public static final String CAT_SEO_H1_TEXT = "catSeoH1Text";
	
	/** constants for CAT_SEO_DESCRIPTION. */
	public static final String CAT_SEO_DESCRIPTION = "catSeoDescription";
	
	/** constants for CHILD_SKUS. */
	public static final String CHILD_SKUS = "childSKUs";

	/** constants for ALL_SKU_IDS. */
	public static final String ALL_SKU_IDS = "allSkus";

	/** constants for SKU_DETAIL. */
	public static final String SKU_DETAIL = "skuDetail";

	/** constants for SKU_CODES. */
	public static final String SKU_CODES = "skuCode";

	/** constants for UPC_NUMBERS. */
	public static final String UPC_NUMBERS = "upcNumber";

	/** constants for ONLINE_PIDS. */
	public static final String ONLINE_PIDS = "onlinePIds";
	
	/** constants for ONLINE_ID. */
	public static final String ONLINE_ID = "onlinePID";
	
	/** constants for SKU_ITEM_ID. */
	public static final String SKU_ITEM_ID = "id";
	
	/** constants for PRODUCT_ITEM_ID. */
	public static final String PRODUCT_ITEM_ID = "id";
	
	/** constants for SKU_ID. */
	public static final String SKU_ID = "id";
	
	/** constants for RECORDS_PER_PAGE. */
	public static final int RECORDS_PER_PAGE = 500;
	
	/** constants for RECORDS_PER_PAGE. */
	public static final String RECORDS_ID = "record.id";
	
	/** constants for PRICE. */
	public static final String PRICE = "price";
	
	/** constants for PRICE_LIST. */
	public static final String PRICE_LIST = "priceList";
	
	/** constants for SKUID. */
	public static final String SKUID = "skuId";
	
	/** constants for BASE_PRICE_LIST. */
	public static final String BASE_PRICE_LIST = "basePriceList";
	
	/** constants for SINGLE_PIPE. */
	public static final String SINGLE_PIPE = "|";
	

	/** constants for NON_CMSSPOT. */
	public static final String NON_CMSSPOT = "nonCMSSPOT";
	/**
	 * Constant for ERR_ENTER_VALID_SITE.
	 */
	public static final String ERR_ENTER_VALID_SITE = "Please use alternate site";
	
	/** Constant for PROMO_ID. */
	public static final String AND_PROMOID_EQUAL= "&promoid=";
	
	/** Constant for PROMO_ID. */
	public static final String PROMOID_EQUAL= "promoid=";
	/**
	 * Constant for Allow PROMOID_PARAM.
	 */

	public static final String PROMOID_PARAM = "promoid";
	/**
	 * Constant for Allow GIFTITEM.
	 */

	public static final String GIFTITEM = "giftItem";

	/** Constant for GWPPROMO_ID. */
	public static final String AND_GWPPROMOID_EQUAL= "&gwppromoid=";
	
	/** Constant for GWPPROMO_ID. */
	public static final String GWPPROMOID_EQUAL= "gwppromoid=";
	/**
	 * Constant for Allow PROMOID.
	 */
	public static final String PROMOID = "promoId";
	/**
	 * Constant for visitor_id.
	 **/
	public static final String VISITOR_ID = "visitorID";
	/**
	 * Constant for browser_id.
	 **/
	public static final String BROWSER_ID = "browserID";
	/**
	 * Constant for SESSION_ID.
	 **/
	public static final String SESSION_ID = "sessionID";
	/**
	 * Constant for CUSTOMER_ID.
	 */
	public static final String CUSTOMER_ID = "customerID";
	/**
	 * Constant for REGEX_DOT_STRING.
	 */
	public static final String REGEX_DOT_STRING = "\\.";
	/** 
	 *  Constant to hold IP_ADDRESS. 
	 */
	public static final String IP_ADDRESS = "X-FORWARDED-FOR";
	/**
     * Constant for IS_COLLECTION_PRODUCT.
     **/
     public static final String IS_COLLECTION_PRODUCT = "Nr=AND(isCollectionProduct:false)";  
     /**
 	 * Constant for Allow FilterCollection.
 	 */

 	public static final String FILTERCOLLECTION = "filterCollection";
 	/**
	 * Constant for Allow PRODUCTID.
	 */
	public static final String PRODUCTID = "productId";
	/**
	 * Constant for Allow QUANTITY.
	 */
	public static final String QUANTITY = "quantity";
	/**
	 * Constant for shippingGroupContainer.
	 **/
	public static final String SHIPPING_GROUP_CONTAINER = "shippingGroupContainer";
	/**
	 * Constant for BILLING_ADDR_MAP.
	 */
	public static final String BILLING_ADDR_MAP = "billingAddressMap";
	/**
	 * Constant for BILLING_ADDR_NKNAME.
	 */
	public static final String BILLING_ADDR_NKNAME = "defaultBillingAddressNickName";
	
	/**
	 * Constant for BILLING_ADDR_NKNAME.
	 */
	public static final String ONLY_ORDER_BILLING_ADDRESS = "onlyOrderBillingAddress";
	
	/**
	 * Constant for SHIP_TO_HOME - ONLINE_ONLY.
	 */
	public static final String ONLINE_ONLY = "Online Only";
	/**
	 * Constant for SHIP_TO_STORE - STORE_ONLY.
	 */
	public static final String STORE_ONLY = "In Store Only";
	/**
	 * Constant for CART_ELIGIBLE.
	 */
	public static final String CART_ELIGIBLE = "cartEligibility";
	
	/**
	 * Constant for APPLE_PAY_ELIGIBLE.
	 */
	public static final String APPLE_PAY_ELIGIBLE = "applePayEligible";	
	/**
	 * Constant for SKU_IDS.
	 */
	public static final String SKU_IDS ="skuIdArray";  
	/**
	 * Constant for APPLE_PAY_BUY_NOW.
	 */
	public static final String APPLE_PAY_BUY_NOW = "applePayBuyNow";
	/**
	 * Constant for ZIP_CODE.
	 */
	public static final String ZIP_CODE = "zipCode";	
	/**
	 * Constant for HAS_SHIPPING_RESTRICTIONS.
	 */
	public static final String HAS_SHIPPING_RESTRICTIONS = "hasShippingRestrictions";	
	/**
	 * Constant for RESTRICTIONS_MAP.
	 */
	public static final String RESTRICTIONS_MAP = "shippingRestrictionsMap";	
	/**
	 * Constant for APPLE_PAY.
	 */
	public static final String APPLE_PAY = "applePay";
	/**
	 * Constant for PUSH_SITE.
	 */
	public static final String PUSH_SITE = "pushSite";
	/**
	 * Constant for DEFAULT_SITE.
	 */
	public static final String DEFAULT_SITE = "ToysRUs";
	/** The Constant TOYSRUS_DOMAIN. */
	public static final String TOYSRUS_DOMAIN = "toysrus";
	
	/**
	 * Constant forPROMO_OUTPUT.
	 */
	public static final String PROMO_OUTPUT = "promoOutput";
	/** constants for CANONICALURL. */
	public static final String CANONICALURL = "canonicalUrl";
	/** constants for AltURL. */
	public static final String ALTURL = "altUrl"; 
	
	/**
	 * Constant for RESPONSE_DOCS.
	 */
	public static final String RESPONSE_DOCS = "docs";
	
	/**
	 * Constant for RESPONSE_SKUID.
	 */
	public static final String RESPONSE_SKUID = "sku_id";
	
	/**
	 * Constant for BM_RESPONSE_NODE.
	 */
	public static final String BM_RESPONSE_NODE = "response";
	
	/**
	 * Constant for RESPONSE_NUMFOUND.
	 */
	public static final String RESPONSE_NUMFOUND = "numFound";
	/**
	 * Constant for SKU_PRICE_ARRAY.
	 */
	public static final String SKU_PRICE_ARRAY ="SKU_PRICE_ARRAY";  
	/**
	 * Constant for PRICE_ENABLE.
	 */
	public static final String PRICE_ENABLE ="priceEnableFlag"; 
	
	/**
	 * Constant for CUSTOM_FILTER.
	 */
	
	public static final String CUSTOM_FILTER = "AND(AND(isDeleted:0),AND(webDisplayFlag:1),AND(isCollectionProduct:false))";
	
	/**
	 * Constant for CUSTOM_ROLLUP_KEY.
	 */
	public static final String CUSTOM_ROLLUP_KEY = "customrollupKey";
	
	/**
	 * Constant for SKU_REPOSITORYID.
	 */
	public static final String SKU_REPOSITORYID = "sku.repositoryId";
	
	/**
	 * Constant for SKU_UPCNUMBERS.
	 */
	public static final String SKU_UPCNUMBERS = "sku.upcNumbers";
	
	/**
	 * Constant for MODE_MATCHALL.
	 */
	public static final String MODE_MATCHALL = "mode matchall";
	
	/**
	 * Constant for MODE_MATCHANY.
	 */
	public static final String MODE_MATCHANY = "mode matchany";
	
	/**
	 * Constant for MODE_MATCHPARTIAL.
	 */
	public static final String MODE_MATCHPARTIAL = "mode matchpartial";
	
	/**
	 * Constant for MODE_MATCHALLPARTIAL.
	 */
	public static final String MODE_MATCHALLPARTIAL = "mode matchallpartial";
	
	/** Constant to hold COHERENCE_IN_STOCK. */
	public static final long COHERENCE_IN_STOCK = 16771;
	
	/** Constant to hold COHERENCE_OUT_OF_STOCK. */
	public static final long COHERENCE_OUT_OF_STOCK = 16770;
	
	/** Constant to hold LIMITED_STOCK. */
	public static final long LIMITED_STOCK = 16769;
	/** Constant to hold shippingDestinationsVO. */
	public static final String SHIPPING_DESTINATIONS = "shippingDestinations";
	/** Constant to hold shippingDestinationsVO. */
	public static final String CARTITEM_DETAILS = "cartItemDetails";
	/** Constant to hold shippingDestinationsVO. */
	public static final String CART_PAGE = "cartPage";

	/** Constant to hold ITEM_DISCOUNT. */
	
	public static final String ITEM_DISCOUNT = "Item Level";
	/** Constant to hold SHIPPING_DISCOUNT. */
	
	public static final String SHIPPING_DISCOUNT = "Shipping Level";
	/** Constant to hold ORDER_DISCOUNT. */
	
	public static final String ORDER_DISCOUNT = "Order Level";

}
