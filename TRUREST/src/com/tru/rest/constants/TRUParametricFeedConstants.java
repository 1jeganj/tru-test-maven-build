package com.tru.rest.constants;


/**
 *  This class hold all Mobile Rest WebService Constant.
 *  @author Professional Access
 * 	@version 1.0
 */


public class TRUParametricFeedConstants {
	
	/** The Constant GET_METHOD. */
	public static final String GET_METHOD = "GET";
	
	/** The Constant CONTENTS. */
	public static final String CONTENTS = "contents";
	/** The Constant FORMATSTRING. */
	public static final String FORMATSTRING = "&format=json";
	/** The Constant ASSOCATIDSTART. */
	public static final String ASSOCATIDSTART = "<ASSOC_CATEGORY_ID>";
	/** The Constant MAINPRODUCT. */
	public static final String MAINPRODUCT = "MainProduct";
	/** The Constant TRUPRODUCTDETAILPAGE. */
	public static final String TRUPRODUCTDETAILPAGE = "TRUProductDetailPage";
	/** The Constant DIMNAME. */
	public static final String DIMNAME = "dimensionName";
	/** The Constant REFINEMENT. */
	public static final String REFINEMENT = "refinements";
	/** The Constant NUMREFINEMENTS. */
	public static final String NUMREFINEMENTS = "numRefinements";
	/** The Constant LABEL. */
	public static final String LABEL = "label";
	/** The Constant COUNT. */
	public static final String COUNT = "count";
	/** The Constant NAVIGATION. */
	public static final String NAVIGATION = "navigation";
	/** The Constant NAME. */
	public static final String NAME = "name";
	/** The Constant MULTISELECT. */
	public static final String MULTISELECT = "multiSelect";
	/** The Constant DISPLAYMORELABEL. */
	public static final String DISPLAYMORELABEL = "moreLinkText";
	/** The Constant PROFILESTART. */
	public static final String PROFILESTART = "<PROFILE>";
	/** The Constant PROFILEEND. */
	public static final String PROFILEEND = "</PROFILE>";
	/** The Constant PARAMETRICPROFILESTART. */
	public static final String PARAMETRICPROFILESTART = "<PARAMETRIC_PROFILE>";
	/** The Constant PARAMETRICPROFILEEND. */
	public static final String PARAMETRICPROFILEEND = "</PARAMETRIC_PROFILE>";
	/** The Constant PROFILEIDSTART. */
	public static final String PROFILEIDSTART = "<PROFILE_ID>";
	/** The Constant PROFILEIDEND. */
	public static final String PROFILEIDEND = "</PROFILE_ID>";
	/** The Constant PROFILENAMESTART. */
	public static final String PROFILENAMESTART = "<PROFILE_NAME>";
	/** The Constant PROFILENAMEEND. */
	public static final String PROFILENAMEEND = "</PROFILE_NAME>";
	/** The Constant CDATASTART. */
	public static final String CDATASTART = "<![CDATA[";
	/** The Constant CDATAEND. */
	public static final String CDATAEND = "]]>";
	/** The Constant ASSOCATIDEND. */
	public static final String ASSOCATIDEND = "</ASSOC_CATEGORY_ID>";
	/** The Constant ATTRIBUTESSTART. */
	public static final String ATTRIBUTESSTART = "<ATTRIBUTES>";
	/** The Constant ATTRIBUTESEND. */
	public static final String ATTRIBUTESEND = "</ATTRIBUTES>";
	/** The Constant ATTRIBSTART. */
	public static final String ATTRIBSTART = "<ATTRIBUTE>";
	/** The Constant ATTRIBEND. */
	public static final String ATTRIBEND = "</ATTRIBUTE>";
	/** The Constant ATTRIBDISPSEQSTART. */
	public static final String ATTRIBDISPSEQSTART = "<ATTRIB_DISP_SEQ>";
	/** The Constant ATTRIBDISPSEQEND. */
	public static final String ATTRIBDISPSEQEND = "</ATTRIB_DISP_SEQ>";
	/** The Constant ATTRIBNAMESTART. */
	public static final String ATTRIBNAMESTART = "<ATTRIB_NAME>";
	/** The Constant ATTRIBNAMEEND. */
	public static final String ATTRIBNAMEEND = "</ATTRIB_NAME>";
	/** The Constant DISPLAYNAMESTART. */
	public static final String DISPLAYNAMESTART = "<DISPLAY_NAME>";
	/** The Constant DISPLAYNAMEEND. */
	public static final String DISPLAYNAMEEND = "</DISPLAY_NAME>";
	/** The Constant FACETRANGESTART. */
	public static final String FACETRANGESTART = "<FACET_RANGE>";
	/** The Constant FACETRANGEEND. */
	public static final String FACETRANGEEND = "</FACET_RANGE>";
	/** The Constant FACETSTART. */
	public static final String FACETSTART = "<FACET>";
	/** The Constant FACETEND. */
	public static final String FACETEND = "</FACET>";
	/** The Constant FACETDISPLAYNAMESTART. */
	public static final String FACETDISPLAYNAMESTART = "<FACET_DISPLAY_NAME>";
	/** The Constant FACETDISPLAYNAMEEND. */
	public static final String FACETDISPLAYNAMEEND = "</FACET_DISPLAY_NAME>";
	/** The Constant COUNTSTART. */
	public static final String COUNTSTART = "<COUNT>";
	/** The Constant COUNTEND. */
	public static final String COUNTEND = "</COUNT>";
	/** The Constant DISPLAYMORECOUNTSTART. */
	public static final String DISPLAYMORECOUNTSTART = "<DISPLAY_MORE_THRESHOLD_COUNT>";
	/** The Constant DISPLAYMORECOUNTEND. */
	public static final String DISPLAYMORECOUNTEND = "</DISPLAY_MORE_THRESHOLD_COUNT>";
	/** The Constant DISPLAYMORESTART. */
	public static final String DISPLAYMORESTART = "<DISPLAY_MORE_LABEL>";
	/** The Constant DISPLAYMOREEND. */
	public static final String DISPLAYMOREEND = "</DISPLAY_MORE_LABEL>";
	/** The Constant MULTISELECTSTART. */
	public static final String MULTISELECTSTART = "<MULTI_SELECT>";
	/** The Constant MULTISELECTEND. */
	public static final String MULTISELECTEND = "</MULTI_SELECT>";
	/** The Constant NALABEL. */
	public static final String NALABEL = "NA";
	/** The Constant ZEROLABEL. */
	public static final int ZEROLABEL = 0;
	/** The Constant TYPE. */
	public static final String TYPE = "@type";
	/** The Constant TRUE. */
	public static final String TRUE = "true";
	/** The Constant FALSE. */
	public static final String FALSE = "false";
	/** This holds the reference for FAILED_HTTP_ERROR_CODE *. */
	public static final String FAILED_HTTP_ERROR_CODE = "Failed : HTTP error code : ";
	/** This holds the reference for PROFILE_ID *. */
	public static final String PROFILE_ID = "profileId";
	/** This holds the reference for REPORT_TYPE *. */
	public static final String REPORT_TYPE = "reportType";
	/** This holds the reference for XML *. */
	public static final String XML = "xml";
	/** This holds the reference for CSV *. */
	public static final String CSV = "csv";
	
}
