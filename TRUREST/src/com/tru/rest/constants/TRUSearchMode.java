package com.tru.rest.constants;

/**
 *  This class hold enum properties for ENEQuery search.
 *  @author Professional Access
 * 	@version 1.0
 */

public enum TRUSearchMode {

    ALL(TRURestConstants.MODE_MATCHALL),

    ANY(TRURestConstants.MODE_MATCHANY),

    PARTIAL(TRURestConstants.MODE_MATCHPARTIAL),

    ALLPARTIAL(TRURestConstants.MODE_MATCHALLPARTIAL);

    private String mSearchMode;

    /**
	 * @return the mSearchMode
	 */
    public String getMode() {
        return mSearchMode;
    }
    /**
	 * @param pSearchMode the mSearchMode to set
	 */
    private TRUSearchMode(final String pSearchMode) {
        this.mSearchMode = pSearchMode;
    }

}