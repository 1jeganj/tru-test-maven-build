package com.tru.common.promotion;

/**
 * This class is Coupon.
 * @author PA
 * @version 1.0
 */
public class Coupon {
	/**
	 * This property hold reference for couponCode.
	 */
	private String mCouponCode;
	/**
	 * This property hold reference for couponName.
	 */
	private String mCouponName;
	/**
	 * @return the couponCode
	 */
	public String getCouponCode() {
		return mCouponCode;
	}
	/**
	 * @param pCouponCode the couponCode to set
	 */
	public void setCouponCode(String pCouponCode) {
		mCouponCode = pCouponCode;
	}
	/**
	 * @return the couponName
	 */
	public String getCouponName() {
		return mCouponName;
	}
	/**
	 * @param pCouponName the couponName to set
	 */
	public void setCouponName(String pCouponName) {
		mCouponName = pCouponName;
	}
	
	
	
}
