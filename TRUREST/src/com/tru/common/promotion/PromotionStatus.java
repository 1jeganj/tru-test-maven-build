package com.tru.common.promotion;

import java.util.List;

/**
 * This class is PromotionStatus.
 * @author PA
 * @version 1.0
 */
public class PromotionStatus {
	
	/**
	 * Property to hold details.
	 */
	private String mDescription;
	/**
	 * Property to hold PromotionName.
	 */
	private String mPromotionName;
	
	/**
	 * Property to hold mCouponType.
	 */
	private String mCouponType;
	
	/**
	 * Property to hold mAppliedCoupon.
	 */
	private String mAppliedCoupon;
	/**
	 * @return the promotionName
	 */
	public String getPromotionName() {
		return mPromotionName;
	}

	/**
	 * @param pPromotionName the promotionName to set
	 */
	public void setPromotionName(String pPromotionName) {
		mPromotionName = pPromotionName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}

	/**
	 * Property to hold mCoupons.
	 */
	private List<Coupon> mCoupons;

	/**
	 * @return the coupons
	 */
	public List<Coupon> getCoupons() {
		return mCoupons;
	}

	/**
	 * @param pCoupons the coupons to set
	 */
	public void setCoupons(List<Coupon> pCoupons) {
		mCoupons = pCoupons;
	}

	/**
	 * @return the mCouponType
	 */
	public String getCouponType() {
		return mCouponType;
	}

	/**
	 * @param pCouponType the mCouponType to set
	 */
	public void setCouponType(String pCouponType) {
		mCouponType = pCouponType;
	}

	/**
	 * @return the mAppliedCoupon
	 */
	public String getAppliedCoupon() {
		return mAppliedCoupon;
	}

	/**
	 * @param pAppliedCoupon the mAppliedCoupon to set
	 */
	public void setAppliedCoupon(String pAppliedCoupon) {
		mAppliedCoupon = pAppliedCoupon;
	}
	
}