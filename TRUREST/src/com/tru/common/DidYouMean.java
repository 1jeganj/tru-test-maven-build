package com.tru.common;

import java.util.List;
import java.util.Map;

/**
 * This class is DidYouMean.
 * @author PA
 * @version 1.0
 */
public class DidYouMean {
	/**
	 * This property hold reference for mSuggestedSearches.
	 */
	private Map<String,List<SuggestedSearches>> mSuggestedSearches;
	
	/**
	 * @return the suggestedSearches
	 */
	public Map<String, List<SuggestedSearches>> getSuggestedSearches() {
		return mSuggestedSearches;
	}
	/**
	 * @param pSuggestedSearches the suggestedSearches to set
	 */
	public void setSuggestedSearches(Map<String, List<SuggestedSearches>> pSuggestedSearches) {
		mSuggestedSearches = pSuggestedSearches;
	}
}
