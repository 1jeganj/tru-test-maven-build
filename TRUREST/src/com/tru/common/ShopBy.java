package com.tru.common;

import java.util.List;

import com.tru.endeca.vo.ShopByDimensionVO;

/**
 * This class is ShopBy.
 * @author PA
 * @version 1.0
 */
public class ShopBy {
	/**
	 * This property hold reference for mDimensionList.
	 */
	List<ShopByDimensionVO> mDimensionList;

	/**
	 * @return the dimensionList
	 */
	public List<ShopByDimensionVO> getDimensionList() {
		return mDimensionList;
	}

	/**
	 * @param pDimensionList the dimensionList to set
	 */
	public void setDimensionList(List<ShopByDimensionVO> pDimensionList) {
		mDimensionList = pDimensionList;
	}
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	
}
