package com.tru.common.searchsuggestions;

import java.util.List;

/**
 * This class is Dimension.
 * @author PA
 * @version 1.0
 */
public class Dimension {
	/**
	 * This property hold reference for DimVal.
	 */
	private List<DimensionVal> mDimensionSearchValues;

	/**
	 * @return the dimVal
	 */
	public List<DimensionVal> getDimensionSearchValues() {
		return mDimensionSearchValues;
	}

	/**
	 * @param pDimensionSearchValues the dimVal to set
	 */
	public void setDimensionSearchValues(List<DimensionVal> pDimensionSearchValues) {
		mDimensionSearchValues = pDimensionSearchValues;
	}
}
