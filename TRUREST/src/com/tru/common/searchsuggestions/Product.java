package com.tru.common.searchsuggestions;

/**
 * This class is Product.
 * @author PA
 * @version 1.0
 */
public class Product {
	/**
	 * This property hold reference for DisplayName.
	 */
	private String mDisplayName;
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * This property hold reference for ProductId.
	 */
	private String mProductId;
	/**
	 * This property hold reference for OnlinePID.
	 */
	private String mOnlinePID;
	/**
	 * This property hold reference for Url.
	 */
	private String mUrl;
	/**
	 * @return the onlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}
	/**
	 * @param pOnlinePID the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return mProductId;
	}
	/**
	 * @param pProductId the productId to set
	 */
	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}
	/**
	 * @param pDisplayName the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return mUrl;
	}
	/**
	 * @param pUrl the url to set
	 */
	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}
	
}
