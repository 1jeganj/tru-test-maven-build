package com.tru.common.searchsuggestions;

/**
 * This class is DimensionVal.
 * @author PA
 * @version 1.0
 */
public class DimensionVal {
	
	/**
	 * This property hold reference for DisplayName.
	 */
	private String mLabel;
	
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mNavigationState;
	
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mCategoryId;
	
	/**
	 * @return the label
	 */
	public String getLabel() {
		return mLabel;
	}
	/**
	 * @param pLabel the label to set
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}
	/**
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}
	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}
	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
}
