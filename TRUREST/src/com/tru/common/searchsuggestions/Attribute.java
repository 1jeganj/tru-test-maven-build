package com.tru.common.searchsuggestions;

/**
 * This class is Attribute.
 * @author PA
 * @version 1.0
 */
public class Attribute {
	/**
	 * This property hold reference for mProduct.
	 */
	private Product mProduct;

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return mProduct;
	}

	/**
	 * @param pProduct the product to set
	 */
	public void setProduct(Product pProduct) {
		mProduct = pProduct;
	}
}
