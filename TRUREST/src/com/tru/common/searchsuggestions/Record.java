package com.tru.common.searchsuggestions;

/**
 * This class is Record.
 * @author PA
 * @version 1.0
 */
public class Record {
	/**
	 * This property hold reference for Attributes.
	 */
	private Attribute mAttributes;

	/**
	 * @return the attributes
	 */
	public Attribute getAttributes() {
		return mAttributes;
	}

	/**
	 * @param pAttributes the attributes to set
	 */
	public void setAttributes(Attribute pAttributes) {
		mAttributes = pAttributes;
	}
}
