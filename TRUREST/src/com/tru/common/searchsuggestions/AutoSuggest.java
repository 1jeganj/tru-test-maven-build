package com.tru.common.searchsuggestions;

import java.util.List;

/**
 * This class is AutoSuggest to hold bean properties.
 * @author PA
 * @version 1.0
 */
public class AutoSuggest {
	/**
	 * This property hold reference for Records.
	 */
	private List<Record> mRecords;
	/**
	 * This property hold reference for TotalNumRecs.
	 */
	private long mTotalNumRecs;
	/**
	 * This property hold reference for DimensionSearchGroups.
	 */
	private List<Dimension> mDimensionSearchGroups;
	/**
	 * @return the records
	 */
	public List<Record> getRecords() {
		return mRecords;
	}
	/**
	 * @param pRecords the records to set
	 */
	public void setRecords(List<Record> pRecords) {
		mRecords = pRecords;
	}
	/**
	 * @return the totalNumRecs
	 */
	public long getTotalNumRecs() {
		return mTotalNumRecs;
	}
	/**
	 * @param pTotalNumRecs the totalNumRecs to set
	 */
	public void setTotalNumRecs(long pTotalNumRecs) {
		mTotalNumRecs = pTotalNumRecs;
	}
	/**
	 * @return the dimensions
	 */
	public List<Dimension> getDimensionSearchGroups() {
		return mDimensionSearchGroups;
	}
	/**
	 * @param pDimensionSearchGroups the dimensions to set
	 */
	public void setDimensionSearchGroups(List<Dimension> pDimensionSearchGroups) {
		mDimensionSearchGroups = pDimensionSearchGroups;
	}
}
