package com.tru.common;

import java.util.List;
import java.util.Map;

import com.tru.rest.constants.TRURestConstants;

/**
 * This class is ProductList.
 * 
 */
public class ProductList {
	/**
	 * This property hold reference for mRemoveAllAction.
	 */
	private RemoveAllAction mRemoveAllAction;
	/**
	 * This property hold reference for mPagingActionTemplate.
	 */
	private PagingActionTemplate mPagingActionTemplate;
	/**
	 * This property hold reference for mSearchType.
	 */
	private String mSearchType;
	/**
	 * This property hold reference for mOriginalTerms.
	 */
	private List<String> mOriginalTerms;
	/**
	 * This property hold reference for mSearchAdjustments.
	 */
	private SearchAdjustment mSearchAdjustments;
	/**
	 * This property hold reference for mDidYouMean.
	 */
	private DidYouMean mDidYouMean;
	/**
	 * This property hold reference for mRedirect.
	 */
	private Redirect mRedirect;
	/**
	 * This property hold reference for mSortOptions.
	 */
	private List<SortOptions> mSortOptions;
	/**
	 * This property hold reference for mFirstRecNum.
	 */
	private Long mFirstRecNum;
	/**
	 * This property hold reference for mLastRecNum.
	 */
	private Long mLastRecNum;
	/**
	 * This property hold reference for mRecsPerPage.
	 */
	private Long mRecsPerPage;
	/**
	 * This property hold reference for mTotalNumOfRec.
	 */
	private Long mTotalNumOfRec;
	/**
	 * This property hold reference for mRecords.
	 */
	private List<EndecaRecords> mRecords;
	/**
	 * This property hold reference for mRefinementCrumbs.
	 */
	private List<RefinementCrumbs> mRefinementCrumbs;
	/**
	 * This property hold reference for mNavigation.
	 */
	private List<Navigation> mNavigation;
	/**
	 * This property hold reference for mCurrentCategoryId.
	 */
	private String mCurrentCategoryId;
	/**
	 * This property hold reference for mCurrentCategoryName.
	 */
	private String mCurrentCategoryName;
	/**
	 * This property hold reference for mTwoLevelCategoryTreeMap.
	 */
	private Map<String,List<Classification>> mTwoLevelCategoryTreeMap;
	/**
	 * This property hold reference for mComparable.
	 */
	private boolean mComparable=TRURestConstants.BOOLEAN_FALSE;	
	/**
	 * This property hold reference for mCategorySeoTitle.
	 */
	private String mCategorySeoTitle;
	/**
	 * This property hold reference for mCategorySeoDisplayName.
	 */
	private String mCategorySeoDisplayName;
	/**
	 * This property hold reference for mCategorySeoDescription.
	 */
	private String mCategorySeoDescription;
	/**
	 * This property hold reference for mCategorySeoKeywords.
	 */
	private List<String> mCategorySeoKeywords;
	/**
	 * This property hold reference for mNavigation.
	 */
	private List<String> mCMSSPOT;
	
	/**
	 * This property hold reference for mSkuIdArray.
	 */
	private String mSkuIds;
	/**
     * This property hold reference for mCategorySeoCanonicalUrl.
     */
    private String mCategorySeoCanonicalUrl;
    /**
     * This property hold reference for mCategorySeoAltUrl.
     */
    private String mCategorySeoAlternateUrl; 
    /**
	 * This property hold reference for mSkuPriceArray.
	 */
	private String [] mSkuPriceArray;
	/**
	 * @return the cMSSPOT
	 */
	public List<String> getCMSSPOT() {
		return mCMSSPOT;
	}
	/**
	 * @param pCMSSPOT the cMSSPOT to set
	 */
	public void setCMSSPOT(List<String> pCMSSPOT) {
		mCMSSPOT = pCMSSPOT;
	}
	/**
	 * @return the categorySeoTitle
	 */
	public String getCategorySeoTitle() {
		return mCategorySeoTitle;
	}
	/**
	 * @param pCategorySeoTitle the categorySeoTitle to set
	 */
	public void setCategorySeoTitle(String pCategorySeoTitle) {
		mCategorySeoTitle = pCategorySeoTitle;
	}
	/**
	 * @return the categorySeoDescription
	 */
	public String getCategorySeoDescription() {
		return mCategorySeoDescription;
	}
	/**
	 * @param pCategorySeoDescription the categorySeoDescription to set
	 */
	public void setCategorySeoDescription(String pCategorySeoDescription) {
		mCategorySeoDescription = pCategorySeoDescription;
	}
	/**
	 * @return the categorySeoKeywords
	 */
	public List<String> getCategorySeoKeywords() {
		return mCategorySeoKeywords;
	}
	/**
	 * @param pCategorySeoKeywords the categorySeoKeywords to set
	 */
	public void setCategorySeoKeywords(List<String> pCategorySeoKeywords) {
		mCategorySeoKeywords = pCategorySeoKeywords;
	}
	/**
	 * @return the comparable
	 */
	public boolean isComparable() {
		return mComparable;
	}
	/**
	 * @param pComparable the comparable to set
	 */
	public void setComparable(boolean pComparable) {
		mComparable = pComparable;
	}
	/**
	 * @return the currentCategoryId
	 */
	public String getCurrentCategoryId() {
		return mCurrentCategoryId;
	}
	/**
	 * @param pCurrentCategoryId the currentCategoryId to set
	 */
	public void setCurrentCategoryId(String pCurrentCategoryId) {
		mCurrentCategoryId = pCurrentCategoryId;
	}
	/**
	 * @return the twoLevelCategoryTreeMap
	 */
	public Map<String,List<Classification>> getTwoLevelCategoryTreeMap() {
		return mTwoLevelCategoryTreeMap;
	}
	/**
	 * @param pTwoLevelCategoryTreeMap the twoLevelCategoryTreeMap to set
	 */
	public void setTwoLevelCategoryTreeMap(Map<String,List<Classification>> pTwoLevelCategoryTreeMap) {
		mTwoLevelCategoryTreeMap = pTwoLevelCategoryTreeMap;
	}
	/**
	 * @return the sortOptions
	 */
	public List<SortOptions> getSortOptions() {
		return mSortOptions;
	}
	/**
	 * @param pSortOptions the sortOptions to set
	 */
	public void setSortOptions(List<SortOptions> pSortOptions) {
		mSortOptions = pSortOptions;
	}
	/**
	 * @return the firstRecNum
	 */
	public Long getFirstRecNum() {
		return mFirstRecNum;
	}
	/**
	 * @param pFirstRecNum the firstRecNum to set
	 */
	public void setFirstRecNum(Long pFirstRecNum) {
		mFirstRecNum = pFirstRecNum;
	}
	/**
	 * @return the lastRecNum
	 */
	public Long getLastRecNum() {
		return mLastRecNum;
	}
	/**
	 * @param pLastRecNum the lastRecNum to set
	 */
	public void setLastRecNum(Long pLastRecNum) {
		mLastRecNum = pLastRecNum;
	}
	/**
	 * @return the recsPerPage
	 */
	public Long getRecsPerPage() {
		return mRecsPerPage;
	}
	/**
	 * @param pRecsPerPage the recsPerPage to set
	 */
	public void setRecsPerPage(Long pRecsPerPage) {
		mRecsPerPage = pRecsPerPage;
	}
	/**
	 * @return the records
	 */
	public List<EndecaRecords> getRecords() {
		return mRecords;
	}
	/**
	 * @param pRecords the records to set
	 */
	public void setRecords(List<EndecaRecords> pRecords) {
		mRecords = pRecords;
	}
	/**
	 * @return the refinementCrumbs
	 */
	public List<RefinementCrumbs> getRefinementCrumbs() {
		return mRefinementCrumbs;
	}
	/**
	 * @param pRefinementCrumbs the refinementCrumbs to set
	 */
	public void setRefinementCrumbs(List<RefinementCrumbs> pRefinementCrumbs) {
		mRefinementCrumbs = pRefinementCrumbs;
	}
	/**
	 * @return the navigation
	 */
	public List<Navigation> getNavigation() {
		return mNavigation;
	}
	/**
	 * @param pNavigation the navigation to set
	 */
	public void setNavigation(List<Navigation> pNavigation) {
		mNavigation = pNavigation;
	}
	
	/**
	 * @return the totalNumOfRec
	 */
	public Long getTotalNumOfRec() {
		return mTotalNumOfRec;
	}
	
	/**
	 * @param pTotalNumOfRec the totalNumOfRec to set
	 */
	public void setTotalNumOfRec(Long pTotalNumOfRec) {
		mTotalNumOfRec = pTotalNumOfRec;
	}
	
	/**
	 * @return the currentCategoryName
	 */
	public String getCurrentCategoryName() {
		return mCurrentCategoryName;
	}
	
	/**
	 * @param pCurrentCategoryName the currentCategoryName to set
	 */
	public void setCurrentCategoryName(String pCurrentCategoryName) {
		mCurrentCategoryName = pCurrentCategoryName;
	}	
	/**
	 * @return the pagingActionTemplate
	 */
	public PagingActionTemplate getPagingActionTemplate() {
		return mPagingActionTemplate;
	}
	/**
	 * @param pPagingActionTemplate the pagingActionTemplate to set
	 */
	public void setPagingActionTemplate(PagingActionTemplate pPagingActionTemplate) {
		mPagingActionTemplate = pPagingActionTemplate;
	}
	/**
	 * @return the removeAllAction
	 */
	public RemoveAllAction getRemoveAllAction() {
		return mRemoveAllAction;
	}
	/**
	 * @param pRemoveAllAction the removeAllAction to set
	 */
	public void setRemoveAllAction(RemoveAllAction pRemoveAllAction) {
		mRemoveAllAction = pRemoveAllAction;
	}
	
	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return mSearchType;
	}
	/**
	 * @param pSearchType the searchType to set
	 */
	public void setSearchType(String pSearchType) {
		mSearchType = pSearchType;
	}
	/**
	 * @return the searchAdjustments
	 */
	public SearchAdjustment getSearchAdjustments() {
		return mSearchAdjustments;
	}
	/**
	 * @param pSearchAdjustments the searchAdjustments to set
	 */
	public void setSearchAdjustments(SearchAdjustment pSearchAdjustments) {
		mSearchAdjustments = pSearchAdjustments;
	}
	/**
	 * @return the didYouMean
	 */
	public DidYouMean getDidYouMean() {
		return mDidYouMean;
	}
	/**
	 * @param pDidYouMean the didYouMean to set
	 */
	public void setDidYouMean(DidYouMean pDidYouMean) {
		mDidYouMean = pDidYouMean;
	}
	/**
	 * @return the redirect
	 */
	public Redirect getRedirect() {
		return mRedirect;
	}
	/**
	 * @param pRedirect the redirect to set
	 */
	public void setRedirect(Redirect pRedirect) {
		mRedirect = pRedirect;
	}
	
	/**
	 * @return the originalTerms
	 */
	public List<String> getOriginalTerms() {
		return mOriginalTerms;
	}
	/**
	 * @param pOriginalTerms the originalTerms to set
	 */
	public void setOriginalTerms(List<String> pOriginalTerms) {
		mOriginalTerms = pOriginalTerms;
	}
	/**
	 * @return the categorySeoDisplayName
	 */
	public String getCategorySeoDisplayName() {
		return mCategorySeoDisplayName;
	}
	/**
	 * @param pCategorySeoDisplayName the categorySeoDisplayName to set
	 */
	public void setCategorySeoDisplayName(String pCategorySeoDisplayName) {
		mCategorySeoDisplayName = pCategorySeoDisplayName;
	}
	/**
	 * @return the mSkuIds
	 */
	public String getSkuIds() {
		return mSkuIds;
	}
	/**
	 * @param pSkuIds the mSkuIds to set
	 */
	public void setSkuIds(String pSkuIds) {
		this.mSkuIds = pSkuIds;
	}
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}  
    /**
     * @return the categorySeoCanonicalUrl
     */
    public String getCategorySeoCanonicalUrl() {
            return mCategorySeoCanonicalUrl;
    }
    /**
     * @param pCategorySeoCanonicalUrl the categorySeoCanonicalUrl to set
     */
    public void setCategorySeoCanonicalUrl(String pCategorySeoCanonicalUrl) {
            mCategorySeoCanonicalUrl = pCategorySeoCanonicalUrl;
    }
    /**
     * @return the categorySeoAlternateUrl
     */
    public String getCategorySeoAlternateUrl() {
            return mCategorySeoAlternateUrl;
    }
    /**
     * @param pCategorySeoAlternateUrl the categorySeoAlternateUrl to set
     */
    public void setCategorySeoAlternateUrl(String pCategorySeoAlternateUrl) {
            mCategorySeoAlternateUrl = pCategorySeoAlternateUrl;
    }
	/**
	 * @return the skuPriceArray
	 */
	public String[] getSkuPriceArray() {
		return mSkuPriceArray;
	}
	/**
	 * @param pSkuPriceArray the skuPriceArray to set
	 */
	public void setSkuPriceArray(String[] pSkuPriceArray) {
		mSkuPriceArray = pSkuPriceArray;
	}

}
