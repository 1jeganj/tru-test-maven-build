package com.tru.common;

import java.util.List;


/**
 * This class is ApplePayOrderInfo.
 * @author PA
 * @version 1.0
 */
public class ApplePayOrderInfo {
	
	
	/** The m id. */
	private String mId;

	
	/** The m coupon codes. */
	private List<String> mCouponCodes;
	
	/** The m currency code. */
	private String mCurrencyCode;
	
	/** The m customer billing city. */
	private String mCustomerBillingCity;
	
	/** The m customer billing state. */
	private String mCustomerBillingState;
	
	/** The m customer billing postal code. */
	private String mCustomerBillingPostalCode;
	
	/** The m discount amount. */
	private Double mDiscountAmount;
	
	/** The m payment type. */
	private String mPaymentType;
	
	/** The m shipping total. */
	private Double mShippingTotal;
	
	/** The m sub total. */
	private Double mSubTotal;
	
	/** The m tax total. */
	private Double mTaxTotal;
	
	/** The m total. */
	private Double mTotal;
	
	/** The m items. */
	private List<Items> mItems;
	
	/**
	 * Gets the id.
	 *
	 * @return the mId
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 *
	 * @param pId the new id
	 */
	public void setId(String pId) {
		this.mId = pId;
	}

	/**
	 * Gets the items.
	 *
	 * @return the mItems
	 */
	public List<Items> getItems() {
		return mItems;
	}

	/**
	 * Sets the items.
	 *
	 * @param pItems the new items
	 */
	public void setItems(List<Items> pItems) {
		this.mItems = pItems;
	}



	/**
	 * Gets the coupon codes.
	 *
	 * @return the coupon codes
	 */
	public List<String> getCouponCodes() {
		return mCouponCodes;
	}

	/**
	 * Sets the coupon codes.
	 *
	 * @param pCouponCodes the new coupon codes
	 */
	public void setCouponCodes(List<String> pCouponCodes) {
		this.mCouponCodes = pCouponCodes;
	}

	/**
	 * Gets the currency code.
	 *
	 * @return the currency code
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}

	/**
	 * Sets the currency code.
	 *
	 * @param pCurrencyCode the new currency code
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		this.mCurrencyCode = pCurrencyCode;
	}

	/**
	 * Gets the customer billing city.
	 *
	 * @return the customer billing city
	 */
	public String getCustomerBillingCity() {
		return mCustomerBillingCity;
	}

	/**
	 * Sets the customer billing city.
	 *
	 * @param pCustomerBillingCity the new customer billing city
	 */
	public void setCustomerBillingCity(String pCustomerBillingCity) {
		this.mCustomerBillingCity = pCustomerBillingCity;
	}

	/**
	 * Gets the customer billing state.
	 *
	 * @return the customer billing state
	 */
	public String getCustomerBillingState() {
		return mCustomerBillingState;
	}

	/**
	 * Sets the customer billing state.
	 *
	 * @param pCustomerBillingState the new customer billing state
	 */
	public void setCustomerBillingState(String pCustomerBillingState) {
		this.mCustomerBillingState = pCustomerBillingState;
	}

	/**
	 * Gets the customer billing postal code.
	 *
	 * @return the customer billing postal code
	 */
	public String getCustomerBillingPostalCode() {
		return mCustomerBillingPostalCode;
	}

	/**
	 * Sets the customer billing postal code.
	 *
	 * @param pCustomerBillingPostalCode the new customer billing postal code
	 */
	public void setCustomerBillingPostalCode(String pCustomerBillingPostalCode) {
		this.mCustomerBillingPostalCode = pCustomerBillingPostalCode;
	}

	/**
	 * Gets the discount amount.
	 *
	 * @return the discount amount
	 */
	public Double getDiscountAmount() {
		return mDiscountAmount;
	}

	/**
	 * Sets the discount amount.
	 *
	 * @param pDiscountAmount the new discount amount
	 */
	public void setDiscountAmount(Double pDiscountAmount) {
		this.mDiscountAmount = pDiscountAmount;
	}

	/**
	 * Gets the payment type.
	 *
	 * @return the payment type
	 */
	public String getPaymentType() {
		return mPaymentType;
	}

	/**
	 * Sets the payment type.
	 *
	 * @param pPaymentType the new payment type
	 */
	public void setPaymentType(String pPaymentType) {
		this.mPaymentType = pPaymentType;
	}

	/**
	 * Gets the shipping total.
	 *
	 * @return the shipping total
	 */
	public Double getShippingTotal() {
		return mShippingTotal;
	}

	/**
	 * Sets the shipping total.
	 *
	 * @param pShippingTotal the new shipping total
	 */
	public void setShippingTotal(Double pShippingTotal) {
		this.mShippingTotal = pShippingTotal;
	}

	/**
	 * Gets the sub total.
	 *
	 * @return the sub total
	 */
	public Double getSubTotal() {
		return mSubTotal;
	}

	/**
	 * Sets the sub total.
	 *
	 * @param pSubTotal the new sub total
	 */
	public void setSubTotal(Double pSubTotal) {
		this.mSubTotal = pSubTotal;
	}

	/**
	 * Gets the tax total.
	 *
	 * @return the tax total
	 */
	public Double getTaxTotal() {
		return mTaxTotal;
	}

	/**
	 * Sets the tax total.
	 *
	 * @param pTaxTotal the new tax total
	 */
	public void setTaxTotal(Double pTaxTotal) {
		this.mTaxTotal = pTaxTotal;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public Double getTotal() {
		return mTotal;
	}

	/**
	 * Sets the total.
	 *
	 * @param pTotal the new total
	 */
	public void setTotal(Double pTotal) {
		this.mTotal = pTotal;
	}


	
	
}
