package com.tru.common;

import java.util.List;
import java.util.Map;

/**
 * This class is SearchAdjustment.
 * @author PA
 * @version 1.0
 */
public class SearchAdjustment {
	/**
	 * This property hold reference for AdjustedSearches.
	 */
	private Map<String,List<AdjustedTerms>> mAdjustedSearches;
	
	/**
	 * @return the adjustedSearches
	 */
	public Map<String, List<AdjustedTerms>> getAdjustedSearches() {
		return mAdjustedSearches;
	}
	/**
	 * @param pAdjustedSearches the adjustedSearches to set
	 */
	public void setAdjustedSearches(Map<String, List<AdjustedTerms>> pAdjustedSearches) {
		mAdjustedSearches = pAdjustedSearches;
	}
}
