package com.tru.common;

import java.util.List;
import java.util.Set;

import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.catalog.vo.ProductPromoInfoVO;

/**
 * This class holds SKU related information for registry.
 * 
 * @author PA
 * @version 1.0
 */
public class RegistrySKUInfoVO {
	/**
	 * Property to hold mInventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**
	 * Property to hold mRegistryEligibility.
	 */
	
	private boolean mRegistryEligibility;
	
	/**
	 * Property to hold SKU id.
	 */
	private String mId;
	
	/**
	 * Property to hold mNewItem.
	 */
	private boolean mNewItem;
	
	/**
	 * Property to hold  mPriceInfoVO.
	 */
	private PriceInfoVO mPriceInfo;
	
	/**
	 * Property to mUpcNumbers.
	 */
	private Set<String> mUpcNumbers;
	
	/**
	 * Property to hold skuReviewRating.
	 */
	private int mReviewRating;
	
	/**
	 * Property to hold mSkuPromos.
	 */
	private List<ProductPromoInfoVO> mPromos;
	
	/**
	 * Property to hold mUnCartable.
	 */
	private boolean mUnCartable;
	
	/**
	 * Property to hold mPromotionalStickerDisplay.
	 */
	private String mPromotionalStickerDisplay;
	
	/**
	 * Property to hold  mOnlinePID.
	 */
	private String mOnlinePID;
	
	/**
	 * Property to hold  mParentCategoryId.
	 */
	private String mParentCategoryId;
	
	/**
	 * Property to hold mIspu.
	 */
	private String mIspu;
	
	/**
	 * Property to hold OriginalParentProduct.
	 */
	private String mOriginalParentProduct;
	/**
	 * Property to hold RegistryWarningIndicator.
	 */
	private String mRegistryWarningIndicator;
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * @return the originalParentProduct
	 */
	public String getOriginalParentProduct() {
		return mOriginalParentProduct;
	}

	/**
	 * @param pOriginalParentProduct the originalParentProduct to set
	 */
	public void setOriginalParentProduct(String pOriginalParentProduct) {
		mOriginalParentProduct = pOriginalParentProduct;
	}

	/**
	 * @return the mInventoryStatus
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * @param pInventoryStatus the mInventoryStatus to set
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		this.mInventoryStatus = pInventoryStatus;
	}

	/**
	 * @return the mRegistryEligibility
	 */
	public boolean isRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * @param pRegistryEligibility the mRegistryEligibility to set
	 */
	public void setRegistryEligibility(boolean pRegistryEligibility) {
		this.mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * @return the mId
	 */
	public String getId() {
		return mId;
	}

	/**
	 * @param pId the mId to set
	 */
	public void setId(String pId) {
		this.mId = pId;
	}

	/**
	 * @return the mNewItem
	 */
	public boolean isNewItem() {
		return mNewItem;
	}

	/**
	 * @param pNewItem the mNewItem to set
	 */
	public void setNewItem(boolean pNewItem) {
		this.mNewItem = pNewItem;
	}

	/**
	 * @return the mPriceInfo
	 */
	public PriceInfoVO getPriceInfo() {
		return mPriceInfo;
	}

	/**
	 * @param pPriceInfo the mPriceInfo to set
	 */
	public void setPriceInfo(PriceInfoVO pPriceInfo) {
		this.mPriceInfo = pPriceInfo;
	}

	/**
	 * @return the mUpcNumbers
	 */
	public Set<String> getUpcNumbers() {
		return mUpcNumbers;
	}

	/**
	 * @param pUpcNumbers the mUpcNumbers to set
	 */
	public void setUpcNumbers(Set<String> pUpcNumbers) {
		this.mUpcNumbers = pUpcNumbers;
	}

	/**
	 * @return the mReviewRating
	 */
	public int getReviewRating() {
		return mReviewRating;
	}

	/**
	 * @param pReviewRating the mReviewRating to set
	 */
	public void setReviewRating(int pReviewRating) {
		this.mReviewRating = pReviewRating;
	}

	/**
	 * @return the mPromos
	 */
	public List<ProductPromoInfoVO> getPromos() {
		return mPromos;
	}

	/**
	 * @param pPromos the mPromos to set
	 */
	public void setPromos(List<ProductPromoInfoVO> pPromos) {
		this.mPromos = pPromos;
	}

	/**
	 * @return the mUnCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}

	/**
	 * @param pUnCartable the mUnCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		this.mUnCartable = pUnCartable;
	}

	/**
	 * @return the mPromotionalStickerDisplay
	 */
	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}

	/**
	 * @param pPromotionalStickerDisplay the mPromotionalStickerDisplay to set
	 */
	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		this.mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}

	/**
	 * @return the mOnlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * @param pOnlinePID the mOnlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		this.mOnlinePID = pOnlinePID;
	}

	/**
	 * @return the mParentCategoryId
	 */
	public String getParentCategoryId() {
		return mParentCategoryId;
	}

	/**
	 * @param pParentCategoryId the mParentCategoryId to set
	 */
	public void setParentCategoryId(String pParentCategoryId) {
		this.mParentCategoryId = pParentCategoryId;
	}

	/**
	 * @return the mIspu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the mIspu to set
	 */
	public void setIspu(String pIspu) {
		this.mIspu = pIspu;
	}
	/**
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * @param pRegistryWarningIndicator the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId
	 *            the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
}
