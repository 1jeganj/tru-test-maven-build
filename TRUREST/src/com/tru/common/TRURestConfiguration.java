package com.tru.common;

import java.util.List;
import java.util.Map;

/**
 * Class TRURestConfiguration extends TRUConfiguration and will be holding REST API Services Related Configurations .
 * @author PA
 * @version 1.0
 * 
 */
public class TRURestConfiguration extends TRUConfiguration{
	
	/** property to hold restInvokeCookieURL. */
	private String mRestInvokeCookieURL;
	
	/** enable or disable the rest API key security**/
	private boolean mApiKeySecurityEnable;
	
	/** Holds mApiKey value. */
	private String mApiKey;
	
	/** Holds mApiChannelTypes value. */
	private List<String> mApiChannelTypes;
	
	/** Property to hold keyword search pages. */
	private String mPagesKeywordSearch;
	
	/** Property to hold type ahead pages. */
	private String mPagesTypeAhead;
	
	/** Property to hold sub category pages. */
	private String mPagesSubCategory;
	
	/** Property to hold product detail pages. */
	private String mPagesProductDetailPage;
	
	/** Property to hold no search results pages. */
	private String mPagesNoSearchResults;
	
	/** Property to hold family pages. */
	private String mPagesFamily;
	
	/** Property to hold collection pages. */
	private String mPagesCollection;
	
	/** Property to hold category pages. */
	private String mPagesCategory;

	/** This property hold chennelMap. */
	private Map<String,String> mChannelMap;
	
	/** property to hold contents property. */
	private String mContents;
	
	/** property to hold main header section name. */
	private String mMainHeader;
	
	/** property to hold main section name. */
	private String mMainProduct;
	
	/** property to hold cartridge typne property. */
	private String mAtType;
	
	/** property to hold breadcrumb cartridge name. */
	private String mTruBreadcrumbs;
	
	/** property to hold guided navigation cartridge. */
	private String mTruGuidedNavigation;
	
	/** property to hold result list cartridge name. */
	private String mTruResultsList;
	
	/** property to hold result list cartridge name. */
	private String mTruCrumbFacets;
	
	/** property to hold product display name property. */
	private String mProductDisplayName;
	
	/** property to hold product repository id property. */
	private String mProductRepositoryId;
	
	/** property to hold product long description property. */
	private String mProductLongDescription;
	
	/** property to hold product url property. */
	private String mProductUrl;
	
	/** property to hold product language property. */
	private String mProductLanguage;
	
	/** property to hold sku display name property. */
	private String mSkuDisplayName;
	
	/** property to hold sku creation date property. */
	private String mSkuCreationDate;
	
	/** property to hold sku repository id property. */
	private String mSkuRepositoryId;
	
	/** property to hold sku on sale property. */
	private String mSkuOnSale;
	
	/** property to hold sku site id property. */
	private String mSkuSiteId;
	
	/** property to hold sku review rating property. */
	private String mSkuReviewRating;
	
	/** property to hold sku review count property. */
	private String mSkuReviewCount;
	
	/** property to hold sku max age property. */
	private String mMaxAge;
	
	/** property to hold sku min age property. */
	private String mMinAge;
	
	/** property to hold swatch image property. */
	private String mSwatchImage;
	
	/** property to hold sku sale price property. */
	private String mSkuSalePrice;
	
	/** property to hold navigation property. */
	private String mNavigation;
	
	/** property to hold pagesAjaxpage property. */
	private String mPagesAjaxpage;
	
	/** property to hold pagesAjaxResultlist property. */
	private String mPagesAjaxResultlist;
	
	/** property to hold mainContent property. */
	private String mMainContent;
	
	/** property to hold truSubCatPageClassificationHierarchy property. */
	private String mTruSubCatPageClassificationHierarchy;
	
	/** property to hold searchAdjustments property. */
	private String mSearchAdjustments;
	
	/** property to hold truCatPageClassificationHierarchy property. */
	private String mTruCatPageClassificationHierarchy;
	
	/** property to hold childWeightMax property. */
	private String mChildWeightMax;
	
	/** property to hold childWeightMin property. */
	private String mChildWeightMin;
	
	/** property to hold alternateImage property. */
	private String mAlternateImage;
	
	/** property to hold searchImageUrl property. */
	private String mSearchImageUrl;
	
	/** property to hold secondaryImage property. */
	private String mSecondaryImage;
	
	/** property to hold promotionalStickerDisplay property. */
	private String mPromotionalStickerDisplay;
	
	/** property to hold productSiteId property. */
	private String mProductSiteId;
	
	/** property to hold skuBrand property. */
	private String mSkuBrand;
	
	/** property to hold skuListPrice property. */
	private String mSkuListPrice;
	
	/** property to hold productCategory property. */
	private String mProductCategory;
	
	
	/** property to hold skuChannelAvailability property. */
	private String mSkuChannelAvailability;
	
	/** property to hold mfrSuggestedAgeMax property. */
	private String mMfrSuggestedAgeMax;
	
	/** property to hold mfrSuggestedAgeMin property. */
	private String mMfrSuggestedAgeMin;
	
	/** property to hold streetDate property. */
	private String mStreetDate;
	
	/** property to hold registryEligibility property. */
	private String mRegistryEligibility;
	
	/** property to hold videoGameEsrb property. */
	private String mVideoGameEsrb;
	
	/** property to hold customerPurchaseLimit property. */
	private String mCustomerPurchaseLimit;
	
	/** property to hold videoGameGenre property. */
	private String mVideoGameGenre;
	
	/** property to hold maximumPrice property. */
	private String mMaximumPrice;
	
	/** property to hold minimumPrice property. */
	private String mMinimumPrice;
	
	/** property to hold savedPercentage property. */
	private String mSavedPercentage;
	
	/** property to hold savedAmount property. */
	private String mSavedAmount;
	
	/** property to hold isCompareProduct property. */
	private String mIsCompareProduct;
	
	/** property to hold displayable property. */
	private String mDisplayable;
	
	/** property to hold categoryRepositoryId property. */
	private String mCategoryRepositoryId;
	
	/** property to hold onlinePID property. */
	private String mOnlinePID;
	
	/** property to hold priceDisplay property. */
	private String mPriceDisplay;
	
	/** property to hold mRegistryClassificationItemName property. */
	private String mRegistryClassificationItemName;
	
	/** property to hold mCrossReferencePropertyName property. */
	private String mCrossReferencePropertyName;
	
	/** property to hold ProductType property. */
	private String mProductType;
	
	/** property to hold isStrikeThrough property. */
	private String mIsStrikeThrough;
	
	/** property to hold categoryPageURL property. */
	private String mCategoryPageURL;
	
	/** property to hold subCategoryPageURL property. */
	private String mSubCategoryPageURL;
	
	/** property to hold familyPageURL property. */
	private String mFamilyPageURL;
	
	/** property to hold CMSSPOTPropertyName property. */
	private String mCMSSPOTPropertyName;
	
	/** property to hold mShipInOrigContainer. */
	private String mShipInOrigContainer;
	
	/** property to hold mUnCartable. */
	private String mUnCartable;
	
	/** property to hold mPagesShopByBrand. */
	private String mPagesShopByBrand;
	
	/** property to hold mPagesShopByCharacter. */
	private String mPagesShopByCharacter;
	
	/** property to hold mPagesShopBy. */
	private String mPagesShopBy;
	
	/** property to hold mSkuQualifiedForPromos. */
	private String mSkuQualifiedForPromos;
	
	/** property to hold mTRUMainProductContentSlot. */
	private String mTRUMainProductContentSlot;
	
	/** property to hold mRegistryProductURL. */
	private String mRegistryProductURL;
	
	/** property to hold mSkuCode. */
	private String mSkuCode;
	
	/** property to hold mFacetName. */
	private String mFacetName;
	
	/** property to hold mCharacter. */
	private String mCharacter;
	
	/** property to hold mBrand. */
	private String mBrand;
	
	/** property to hold mShopbyPageURL. */
	private String mShopbyPageURL;
	
	/** property to hold mSearchPageURL. */
	private String mSearchPageURL;
	
	/** property to hold mSkuLongDescription. */
	private String mSkuLongDescription;
	
	/** property to hold mItemInStorePickUp. */
	private String mItemInStorePickUp;
	
	/** property to hold mRusItemNumber. */
	private String mRusItemNumber;
	
	/** property to hold mUpcNumbers. */
	private String mUpcNumbers;
	
	/** property to hold isNew. */
	private String mIsNew;
	
	/** property to hold mContentAjaxknockoutCollection. */
	private String mContentAjaxknockoutCollection;
	
	/** property to hold mshipToStoreEligible. */
	private String mShipToStoreEligible;
	/** property to hold mApplePayTestURL. */
	private String mApplePayTestURL;
	/** enable or disable the registryCatId**/
	private boolean mRegistryCatId;
	/** property to hold the mIspu**/
	private String mIspu;
	
	/** property to hold the mPriceAscendingSort**/
	private String mPriceAscendingSort;
	
	/** property to hold the mPriceDescendingSort**/
	private String mPriceDescendingSort;
	
	/**
	 * @return the registryCatId
	 */
	public boolean isRegistryCatId() {
		return mRegistryCatId;
	}

	/**
	 * @param pRegistryCatId the registryCatId to set
	 */
	public void setRegistryCatId(boolean pRegistryCatId) {
		mRegistryCatId = pRegistryCatId;
	}

	/**
	 * @return the skuQualifiedForPromos
	 */
	public String getSkuQualifiedForPromos() {
		return mSkuQualifiedForPromos;
	}

	/** property to hold mPagesPromotion. */
	private String mPagesPromotion;
	
	/**
	 * property to hold mOriginalParentProduct.
	 */
	private String mOriginalParentProduct;
	
	/** property to hold searchImageUrl property. */
	private String mCollectionImageUrl;
	
	/** property to hold searchImageUrl property. */
	private String mSkuListSize;
	
	/**
	 * property to hold mCollectionName.
	 */
	private String mCollectionName;
	/**
	 * property to hold mRestDataType.
	 */
	private String mRestDataType;
	
	/**
	 * property to hold mShipToStore.
	 */
	private String mShipToStore;
	/**
     * Property to Hold mCMSSpotsDisabledChannelList.
     */
	/**
	 * Gets the rest data type.
	 *
	 * @return the RestDataType
	 */
	public String getRestDataType() {
		return mRestDataType;
	}

	/**
	 * Sets the rest data type.
	 *
	 * @param pRestDataType the restDataType to set
	 */
	public void setRestDataType(String pRestDataType) {
		mRestDataType = pRestDataType;
	}

	/**
	 * @return the CollectionName
	 */
	public String getCollectionName() {
		return mCollectionName;
	}
	/**
	 * @param pCollectionName the collectionName to set
	 */
	public void setCollectionName(String pCollectionName) {
		mCollectionName = pCollectionName;
	}

	/**
	 * @return the originalParentProduct
	 */
	public String getOriginalParentProduct() {
		return mOriginalParentProduct;
	}

	/**
	 * @param pOriginalParentProduct the originalParentProduct to set
	 */
	public void setOriginalParentProduct(String pOriginalParentProduct) {
		mOriginalParentProduct = pOriginalParentProduct;
	}

	/**
	 * @param pSkuQualifiedForPromos the skuQualifiedForPromos to set
	 */
	public void setSkuQualifiedForPromos(String pSkuQualifiedForPromos) {
		mSkuQualifiedForPromos = pSkuQualifiedForPromos;
	}

	/**
	 * @return the unCartable
	 */
	public String getUnCartable() {
		return mUnCartable;
	}

	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(String pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * @return the shipInOrigContainer
	 */
	public String getShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * @param pShipInOrigContainer the shipInOrigContainer to set
	 */
	public void setShipInOrigContainer(String pShipInOrigContainer) {
		mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * @return the cMSSPOTPropertyName
	 */
	public String getCMSSPOTPropertyName() {
		return mCMSSPOTPropertyName;
	}

	/**
	 * @param pCMSSPOTPropertyName the cMSSPOTPropertyName to set
	 */
	public void setCMSSPOTPropertyName(String pCMSSPOTPropertyName) {
		mCMSSPOTPropertyName = pCMSSPOTPropertyName;
	}

	/**
	 * @return the categoryPageURL
	 */
	public String getCategoryPageURL() {
		return mCategoryPageURL;
	}

	/**
	 * @param pCategoryPageURL the categoryPageURL to set
	 */
	public void setCategoryPageURL(String pCategoryPageURL) {
		mCategoryPageURL = pCategoryPageURL;
	}

	/**
	 * @return the subCategoryPageURL
	 */
	public String getSubCategoryPageURL() {
		return mSubCategoryPageURL;
	}

	/**
	 * @param pSubCategoryPageURL the subCategoryPageURL to set
	 */
	public void setSubCategoryPageURL(String pSubCategoryPageURL) {
		mSubCategoryPageURL = pSubCategoryPageURL;
	}

	/**
	 * @return the familyPageURL
	 */
	public String getFamilyPageURL() {
		return mFamilyPageURL;
	}

	/**
	 * @param pFamilyPageURL the familyPageURL to set
	 */
	public void setFamilyPageURL(String pFamilyPageURL) {
		mFamilyPageURL = pFamilyPageURL;
	}

	/**
	 * @return the isStrikeThrough
	 */
	public String getIsStrikeThrough() {
		return mIsStrikeThrough;
	}

	/**
	 * @param pIsStrikeThrough the isStrikeThrough to set
	 */
	public void setIsStrikeThrough(String pIsStrikeThrough) {
		mIsStrikeThrough = pIsStrikeThrough;
	}

	/**
	 * @return the hasPriceRange
	 */
	public String getHasPriceRange() {
		return mHasPriceRange;
	}

	/**
	 * @param pHasPriceRange the hasPriceRange to set
	 */
	public void setHasPriceRange(String pHasPriceRange) {
		mHasPriceRange = pHasPriceRange;
	}

	/** property to hold hasPriceRange property. */
	private String mHasPriceRange;
	
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return mProductType;
	}

	/**
	 * @param pProductType the productType to set
	 */
	public void setProductType(String pProductType) {
		mProductType = pProductType;
	}

	/** property to hold mSiteIdsPropertyName property. */
	private String mSiteIdsPropertyName;

	/**
	 * @return the registryClassificationItemName
	 */
	public String getRegistryClassificationItemName() {
		return mRegistryClassificationItemName;
	}

	/**
	 * @param pRegistryClassificationItemName the registryClassificationItemName to set
	 */
	public void setRegistryClassificationItemName(
			String pRegistryClassificationItemName) {
		mRegistryClassificationItemName = pRegistryClassificationItemName;
	}

	/**
	 * @return the crossReferencePropertyName
	 */
	public String getCrossReferencePropertyName() {
		return mCrossReferencePropertyName;
	}

	/**
	 * @param pCrossReferencePropertyName the crossReferencePropertyName to set
	 */
	public void setCrossReferencePropertyName(String pCrossReferencePropertyName) {
		mCrossReferencePropertyName = pCrossReferencePropertyName;
	}

	/**
	 * @return the maximumPrice
	 */
	public String getMaximumPrice() {
		return mMaximumPrice;
	}

	/**
	 * @param pMaximumPrice the maximumPrice to set
	 */
	public void setMaximumPrice(String pMaximumPrice) {
		mMaximumPrice = pMaximumPrice;
	}

	/**
	 * @return the minimumPrice
	 */
	public String getMinimumPrice() {
		return mMinimumPrice;
	}

	/**
	 * @param pMinimumPrice the minimumPrice to set
	 */
	public void setMinimumPrice(String pMinimumPrice) {
		mMinimumPrice = pMinimumPrice;
	}

	
	/**
	 * @return the onlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * @param pOnlinePID the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}

	/**
	 * @return the mCategoryRepositoryId
	 */
	public String getCategoryRepositoryId() {
		return mCategoryRepositoryId;
	}

	/**
	 * @param pCategoryRepositoryId the CategoryRepositoryId to set
	 */
	public void setCategoryRepositoryId(String pCategoryRepositoryId) {
		mCategoryRepositoryId = pCategoryRepositoryId;
	}

	/**
	 * @return the isCompareProduct
	 */
	public String getIsCompareProduct() {
		return mIsCompareProduct;
	}

	/**
	 * @param pIsCompareProduct the isCompareProduct to set
	 */
	public void setIsCompareProduct(String pIsCompareProduct) {
		mIsCompareProduct = pIsCompareProduct;
	}

	/**
	 * @return the displayable
	 */
	public String getDisplayable() {
		return mDisplayable;
	}

	/**
	 * @param pDisplayable the displayable to set
	 */
	public void setDisplayable(String pDisplayable) {
		mDisplayable = pDisplayable;
	}

	

	/**
	 * @return the savedPercentage
	 */
	public String getSavedPercentage() {
		return mSavedPercentage;
	}

	/**
	 * @param pSavedPercentage the savedPercentage to set
	 */
	public void setSavedPercentage(String pSavedPercentage) {
		mSavedPercentage = pSavedPercentage;
	}

	/**
	 * @return the savedAmount
	 */
	public String getSavedAmount() {
		return mSavedAmount;
	}

	/**
	 * @param pSavedAmount the savedAmount to set
	 */
	public void setSavedAmount(String pSavedAmount) {
		mSavedAmount = pSavedAmount;
	}

	/**

	 * @return the mSkuListPrice
	 */
	public String getSkuListPrice() {
		return mSkuListPrice;
	}

	/**
	 * @param pSkuListPrice the mSkuListPrice to set
	 */
	public void setSkuListPrice(String pSkuListPrice) {
		this.mSkuListPrice = pSkuListPrice;
	}

	/**

	 * @return the productCategory
	 */
	public String getProductCategory() {
		return mProductCategory;
	}

	/**
	 * @param pProductCategory the productCategory to set
	 */
	public void setProductCategory(String pProductCategory) {
		mProductCategory = pProductCategory;
	}


	/**
	 * @return the mfrSuggestedAgeMax
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * @param pMfrSuggestedAgeMax the mfrSuggestedAgeMax to set
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}

	/**
	 * @return the mfrSuggestedAgeMin
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * @param pMfrSuggestedAgeMin the mfrSuggestedAgeMin to set
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * @param pStreetDate the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @return the registryEligibility
	 */
	public String getRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * @param pRegistryEligibility the registryEligibility to set
	 */
	public void setRegistryEligibility(String pRegistryEligibility) {
		mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * @return the videoGameEsrb
	 */
	public String getVideoGameEsrb() {
		return mVideoGameEsrb;
	}

	/**
	 * @param pVideoGameEsrb the videoGameEsrb to set
	 */
	public void setVideoGameEsrb(String pVideoGameEsrb) {
		mVideoGameEsrb = pVideoGameEsrb;
	}

	/**
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * @param pCustomerPurchaseLimit the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * @return the videoGameGenre
	 */
	public String getVideoGameGenre() {
		return mVideoGameGenre;
	}

	/**
	 * @param pVideoGameGenre the videoGameGenre to set
	 */
	public void setVideoGameGenre(String pVideoGameGenre) {
		mVideoGameGenre = pVideoGameGenre;
	}

	/**

	 * @return the skuBrand
	 */
	public String getSkuBrand() {
		return mSkuBrand;
	}

	/**
	 * @param pSkuBrand the skuBrand to set
	 */
	public void setSkuBrand(String pSkuBrand) {
		mSkuBrand = pSkuBrand;
	}

	/**
	 * @return the productSiteId
	 */
	public String getProductSiteId() {
		return mProductSiteId;
	}

	/**
	 * @param pProductSiteId the productSiteId to set
	 */
	public void setProductSiteId(String pProductSiteId) {
		mProductSiteId = pProductSiteId;
	}

	/**
	 * @return the promotionalStickerDisplay
	 */
	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}

	/**
	 * @param pPromotionalStickerDisplay the promotionalStickerDisplay to set
	 */
	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}

	/**
	 * @return the secondaryImage
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}

	/**
	 * @param pSecondaryImage the secondaryImage to set
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}

	/**
	 * @return the childWeightMax
	 */
	public String getChildWeightMax() {
		return mChildWeightMax;
	}

	/**
	 * @param pChildWeightMax the childWeightMax to set
	 */
	public void setChildWeightMax(String pChildWeightMax) {
		mChildWeightMax = pChildWeightMax;
	}

	/**
	 * @return the childWeightMin
	 */
	public String getChildWeightMin() {
		return mChildWeightMin;
	}

	/**
	 * @param pChildWeightMin the childWeightMin to set
	 */
	public void setChildWeightMin(String pChildWeightMin) {
		mChildWeightMin = pChildWeightMin;
	}

	/**
	 * @return the searchImageUrl
	 */
	public String getSearchImageUrl() {
		return mSearchImageUrl;
	}

	/**
	 * @param pSearchImageUrl the searchImageUrl to set
	 */
	public void setSearchImageUrl(String pSearchImageUrl) {
		mSearchImageUrl = pSearchImageUrl;
	}

	/**
	 * @return the truSubCatPageClassificationHierarchy
	 */
	public String getTruSubCatPageClassificationHierarchy() {
		return mTruSubCatPageClassificationHierarchy;
	}

	/**
	 * @param pTruSubCatPageClassificationHierarchy the truSubCatPageClassificationHierarchy to set
	 */
	public void setTruSubCatPageClassificationHierarchy(
			String pTruSubCatPageClassificationHierarchy) {
		mTruSubCatPageClassificationHierarchy = pTruSubCatPageClassificationHierarchy;
	}

	/**
	 * @return the searchAdjustments
	 */
	public String getSearchAdjustments() {
		return mSearchAdjustments;
	}

	/**
	 * @param pSearchAdjustments the searchAdjustments to set
	 */
	public void setSearchAdjustments(String pSearchAdjustments) {
		mSearchAdjustments = pSearchAdjustments;
	}

	/**
	 * @return the truCatPageClassificationHierarchy
	 */
	public String getTruCatPageClassificationHierarchy() {
		return mTruCatPageClassificationHierarchy;
	}

	/**
	 * @param pTruCatPageClassificationHierarchy the truCatPageClassificationHierarchy to set
	 */
	public void setTruCatPageClassificationHierarchy(
			String pTruCatPageClassificationHierarchy) {
		mTruCatPageClassificationHierarchy = pTruCatPageClassificationHierarchy;
	}

	/**
	 * @return the mainContent
	 */
	public String getMainContent() {
		return mMainContent;
	}

	/**
	 * @param pMainContent the mainContent to set
	 */
	public void setMainContent(String pMainContent) {
		mMainContent = pMainContent;
	}

	/**
	 * @return the pagesAjaxpage
	 */
	public String getPagesAjaxpage() {
		return mPagesAjaxpage;
	}

	/**
	 * @param pPagesAjaxpage the pagesAjaxpage to set
	 */
	public void setPagesAjaxpage(String pPagesAjaxpage) {
		mPagesAjaxpage = pPagesAjaxpage;
	}

	/**
	 * @return the pagesAjaxResultlist
	 */
	public String getPagesAjaxResultlist() {
		return mPagesAjaxResultlist;
	}

	/**
	 * @param pPagesAjaxResultlist the pagesAjaxResultlist to set
	 */
	public void setPagesAjaxResultlist(String pPagesAjaxResultlist) {
		mPagesAjaxResultlist = pPagesAjaxResultlist;
	}

	/**
	 * @return the contents
	 */
	public String getContents() {
		return mContents;
	}

	/**
	 * @param pContents the contents to set
	 */
	public void setContents(String pContents) {
		mContents = pContents;
	}

	/**
	 * @return the mainHeader
	 */
	public String getMainHeader() {
		return mMainHeader;
	}

	/**
	 * @param pMainHeader the mainHeader to set
	 */
	public void setMainHeader(String pMainHeader) {
		mMainHeader = pMainHeader;
	}

	/**
	 * @return the mainProduct
	 */
	public String getMainProduct() {
		return mMainProduct;
	}

	/**
	 * @param pMainProduct the mainProduct to set
	 */
	public void setMainProduct(String pMainProduct) {
		mMainProduct = pMainProduct;
	}

	/**
	 * @return the atType
	 */
	public String getAtType() {
		return mAtType;
	}

	/**
	 * @param pAtType the atType to set
	 */
	public void setAtType(String pAtType) {
		mAtType = pAtType;
	}

	/**
	 * @return the truBreadcrumbs
	 */
	public String getTruBreadcrumbs() {
		return mTruBreadcrumbs;
	}

	/**
	 * @param pTruBreadcrumbs the truBreadcrumbs to set
	 */
	public void setTruBreadcrumbs(String pTruBreadcrumbs) {
		mTruBreadcrumbs = pTruBreadcrumbs;
	}

	/**
	 * @return the truGuidedNavigation
	 */
	public String getTruGuidedNavigation() {
		return mTruGuidedNavigation;
	}

	/**
	 * @param pTruGuidedNavigation the truGuidedNavigation to set
	 */
	public void setTruGuidedNavigation(String pTruGuidedNavigation) {
		mTruGuidedNavigation = pTruGuidedNavigation;
	}

	/**
	 * @return the truResultsList
	 */
	public String getTruResultsList() {
		return mTruResultsList;
	}

	/**
	 * @param pTruResultsList the truResultsList to set
	 */
	public void setTruResultsList(String pTruResultsList) {
		mTruResultsList = pTruResultsList;
	}

	/**
	 * @return the productDisplayName
	 */
	public String getProductDisplayName() {
		return mProductDisplayName;
	}

	/**
	 * @param pProductDisplayName the productDisplayName to set
	 */
	public void setProductDisplayName(String pProductDisplayName) {
		mProductDisplayName = pProductDisplayName;
	}

	/**
	 * @return the productRepositoryId
	 */
	public String getProductRepositoryId() {
		return mProductRepositoryId;
	}

	/**
	 * @param pProductRepositoryId the productRepositoryId to set
	 */
	public void setProductRepositoryId(String pProductRepositoryId) {
		mProductRepositoryId = pProductRepositoryId;
	}

	/**
	 * @return the productLongDescription
	 */
	public String getProductLongDescription() {
		return mProductLongDescription;
	}

	/**
	 * @param pProductLongDescription the productLongDescription to set
	 */
	public void setProductLongDescription(String pProductLongDescription) {
		mProductLongDescription = pProductLongDescription;
	}

	/**
	 * @return the productUrl
	 */
	public String getProductUrl() {
		return mProductUrl;
	}

	/**
	 * @param pProductUrl the productUrl to set
	 */
	public void setProductUrl(String pProductUrl) {
		mProductUrl = pProductUrl;
	}

	/**
	 * @return the productLanguage
	 */
	public String getProductLanguage() {
		return mProductLanguage;
	}

	/**
	 * @param pProductLanguage the productLanguage to set
	 */
	public void setProductLanguage(String pProductLanguage) {
		mProductLanguage = pProductLanguage;
	}

	/**
	 * @return the skuDisplayName
	 */
	public String getSkuDisplayName() {
		return mSkuDisplayName;
	}

	/**
	 * @param pSkuDisplayName the skuDisplayName to set
	 */
	public void setSkuDisplayName(String pSkuDisplayName) {
		mSkuDisplayName = pSkuDisplayName;
	}

	/**
	 * @return the skuCreationDate
	 */
	public String getSkuCreationDate() {
		return mSkuCreationDate;
	}

	/**
	 * @param pSkuCreationDate the skuCreationDate to set
	 */
	public void setSkuCreationDate(String pSkuCreationDate) {
		mSkuCreationDate = pSkuCreationDate;
	}

	/**
	 * @return the skuRepositoryId
	 */
	public String getSkuRepositoryId() {
		return mSkuRepositoryId;
	}

	/**
	 * @param pSkuRepositoryId the skuRepositoryId to set
	 */
	public void setSkuRepositoryId(String pSkuRepositoryId) {
		mSkuRepositoryId = pSkuRepositoryId;
	}

	/**
	 * @return the skuOnSale
	 */
	public String getSkuOnSale() {
		return mSkuOnSale;
	}

	/**
	 * @param pSkuOnSale the skuOnSale to set
	 */
	public void setSkuOnSale(String pSkuOnSale) {
		mSkuOnSale = pSkuOnSale;
	}

	/**
	 * @return the skuSiteId
	 */
	public String getSkuSiteId() {
		return mSkuSiteId;
	}

	/**
	 * @param pSkuSiteId the skuSiteId to set
	 */
	public void setSkuSiteId(String pSkuSiteId) {
		mSkuSiteId = pSkuSiteId;
	}

	/**
	 * @return the skuReviewRating
	 */
	public String getSkuReviewRating() {
		return mSkuReviewRating;
	}

	/**
	 * @param pSkuReviewRating the skuReviewRating to set
	 */
	public void setSkuReviewRating(String pSkuReviewRating) {
		mSkuReviewRating = pSkuReviewRating;
	}

	/**
	 * @return the skuReviewCount
	 */
	public String getSkuReviewCount() {
		return mSkuReviewCount;
	}

	/**
	 * @param pSkuReviewCount the skuReviewCount to set
	 */
	public void setSkuReviewCount(String pSkuReviewCount) {
		mSkuReviewCount = pSkuReviewCount;
	}

	/**
	 * @return the maxAge
	 */
	public String getMaxAge() {
		return mMaxAge;
	}

	/**
	 * @param pMaxAge the maxAge to set
	 */
	public void setMaxAge(String pMaxAge) {
		mMaxAge = pMaxAge;
	}

	/**
	 * @return the minAge
	 */
	public String getMinAge() {
		return mMinAge;
	}

	/**
	 * @param pMinAge the minAge to set
	 */
	public void setMinAge(String pMinAge) {
		mMinAge = pMinAge;
	}

	/**
	 * @return the swatchImage
	 */
	public String getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * @param pSwatchImage the swatchImage to set
	 */
	public void setSwatchImage(String pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}

	/**
	 * @return the skuSalePrice
	 */
	public String getSkuSalePrice() {
		return mSkuSalePrice;
	}

	/**
	 * @param pSkuSalePrice the skuSalePrice to set
	 */
	public void setSkuSalePrice(String pSkuSalePrice) {
		mSkuSalePrice = pSkuSalePrice;
	}

	/**
	 * @return the navigation
	 */
	public String getNavigation() {
		return mNavigation;
	}

	/**
	 * @param pNavigation the navigation to set
	 */
	public void setNavigation(String pNavigation) {
		mNavigation = pNavigation;
	}

	/**
	 * Gets the rest invoke cookie url.
	 *
	 * @return the restInvokeCookieURL
	 */
	public String getRestInvokeCookieURL() {
		return mRestInvokeCookieURL;
	}

	/**
	 * Sets the rest invoke cookie url.
	 *
	 * @param pRestInvokeCookieURL the restInvokeCookieURL to set
	 */
	public void setRestInvokeCookieURL(String pRestInvokeCookieURL) {
		mRestInvokeCookieURL = pRestInvokeCookieURL;
	}

	/**
	 * @return the apiKeySecurityEnable
	 */
	public boolean isApiKeySecurityEnable() {
		return mApiKeySecurityEnable;
	}

	/**
	 * @param pApiKeySecurityEnable the apiKeySecurityEnable to set
	 */
	public void setApiKeySecurityEnable(boolean pApiKeySecurityEnable) {
		mApiKeySecurityEnable = pApiKeySecurityEnable;
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return mApiKey;
	}

	/**
	 * @param pApiKey the apiKey to set
	 */
	public void setApiKey(String pApiKey) {
		mApiKey = pApiKey;
	}

	/**
	 * @return the mApiChannelTypes
	 */
	public List<String> getApiChannelTypes() {
		return mApiChannelTypes;
	}

	/**
	 * @param pApiChannelTypes the mApiChannelTypes to set
	 */
	public void setApiChannelTypes(List<String> pApiChannelTypes) {
		this.mApiChannelTypes = pApiChannelTypes;
	}
	
	/**
	 * @return the pagesSubCategory
	 */
	public String getPagesSubCategory() {
		return mPagesSubCategory;
	}

	/**
	 * @param pPagesSubCategory the pagesSubCategory to set
	 */
	public void setPagesSubCategory(String pPagesSubCategory) {
		mPagesSubCategory = pPagesSubCategory;
	}

	/**
	 * @return the pagesProductDetailPage
	 */
	public String getPagesProductDetailPage() {
		return mPagesProductDetailPage;
	}

	/**
	 * @param pPagesProductDetailPage the pagesProductDetailPage to set
	 */
	public void setPagesProductDetailPage(String pPagesProductDetailPage) {
		mPagesProductDetailPage = pPagesProductDetailPage;
	}

	/**
	 * @return the pagesNoSearchResults
	 */
	public String getPagesNoSearchResults() {
		return mPagesNoSearchResults;
	}

	/**
	 * @param pPagesNoSearchResults the pagesNoSearchResults to set
	 */
	public void setPagesNoSearchResults(String pPagesNoSearchResults) {
		mPagesNoSearchResults = pPagesNoSearchResults;
	}

	/**
	 * @return the pagesFamily
	 */
	public String getPagesFamily() {
		return mPagesFamily;
	}

	/**
	 * @param pPagesFamily the pagesFamily to set
	 */
	public void setPagesFamily(String pPagesFamily) {
		mPagesFamily = pPagesFamily;
	}

	/**
	 * @return the pagesCollection
	 */
	public String getPagesCollection() {
		return mPagesCollection;
	}

	/**
	 * @param pPagesCollection the pagesCollection to set
	 */
	public void setPagesCollection(String pPagesCollection) {
		mPagesCollection = pPagesCollection;
	}

	/**
	 * @return the pagesCategory
	 */
	public String getPagesCategory() {
		return mPagesCategory;
	}

	/**
	 * @param pPagesCategory the pagesCategory to set
	 */
	public void setPagesCategory(String pPagesCategory) {
		mPagesCategory = pPagesCategory;
	}

	/**
	 * @return the pagesSearch
	 */
	public String getPagesKeywordSearch() {
		return mPagesKeywordSearch;
	}

	/**
	 * @param pPagesSearch the pagesSearch to set
	 */
	public void setPagesKeywordSearch(String pPagesSearch) {
		mPagesKeywordSearch = pPagesSearch;
	}

	/**
	 * @return the pagesTypeAhead
	 */
	public String getPagesTypeAhead() {
		return mPagesTypeAhead;
	}

	/**
	 * @param pPagesTypeAhead the pagesTypeAhead to set
	 */
	public void setPagesTypeAhead(String pPagesTypeAhead) {
		mPagesTypeAhead = pPagesTypeAhead;
	}

	/**
	 * @return the mChannelMap
	 */
	public Map<String, String> getChannelMap() {
		return mChannelMap;
	}

	/**
	 * @param pChannelMap to set
	 */
	public void setChannelMap(Map<String, String> pChannelMap) {
		this.mChannelMap = pChannelMap;
	}	
	
	/**
	 * @return the truCrumbFacets
	 */
	public String getTruCrumbFacets() {
		return mTruCrumbFacets;
	}

	/**
	 * @param pTruCrumbFacets the truCrumbFacets to set
	 */
	public void setTruCrumbFacets(String pTruCrumbFacets) {
		mTruCrumbFacets = pTruCrumbFacets;
	}
	
	/**
	 * @return the alternateImage
	 */
	public String getAlternateImage() {
		return mAlternateImage;
	}

	/**
	 * @param pAlternateImage the alternateImage to set
	 */
	public void setAlternateImage(String pAlternateImage) {
		mAlternateImage = pAlternateImage;
	}

	/**
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}

	/**
	 * @param pPriceDisplay the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * @return the siteIdsPropertyName
	 */
	public String getSiteIdsPropertyName() {
		return mSiteIdsPropertyName;
	}

	/**
	 * @param pSiteIdsPropertyName the siteIdsPropertyName to set
	 */
	public void setSiteIdsPropertyName(String pSiteIdsPropertyName) {
		mSiteIdsPropertyName = pSiteIdsPropertyName;
	}

	/**
	 * @return the pagesShopByBrand
	 */
	public String getPagesShopByBrand() {
		return mPagesShopByBrand;
	}

	/**
	 * @param pPagesShopByBrand the pagesShopByBrand to set
	 */
	public void setPagesShopByBrand(String pPagesShopByBrand) {
		mPagesShopByBrand = pPagesShopByBrand;
	}

	/**
	 * @return the pagesShopByCharacter
	 */
	public String getPagesShopByCharacter() {
		return mPagesShopByCharacter;
	}

	/**
	 * @param pPagesShopByCharacter the pagesShopByCharacter to set
	 */
	public void setPagesShopByCharacter(String pPagesShopByCharacter) {
		mPagesShopByCharacter = pPagesShopByCharacter;
	}

	/**
	 * @return the pagesShopBy
	 */
	public String getPagesShopBy() {
		return mPagesShopBy;
	}

	/**
	 * @param pPagesShopBy the pagesShopBy to set
	 */
	public void setPagesShopBy(String pPagesShopBy) {
		mPagesShopBy = pPagesShopBy;
	}

	/**
	 * @return the mTRUMainProductContentSlot
	 */
	public String getTRUMainProductContentSlot() {
		return mTRUMainProductContentSlot;
	}

	/**
	 * @param pTRUMainProductContentSlot the TRUMainProductContentSlot to set
	 */
	public void setTRUMainProductContentSlot(String pTRUMainProductContentSlot) {
		mTRUMainProductContentSlot = pTRUMainProductContentSlot;
	}

	/**
	 * @return the mRegistryProductURL
	 */
	public String getRegistryProductURL() {
		return mRegistryProductURL;
	}

	/**
	 * @param pRegistryProductURL the mRegistryProductURL to set
	 */
	public void setRegistryProductURL(String pRegistryProductURL) {
		this.mRegistryProductURL = pRegistryProductURL;
	}

	/**
	 * @return the mSkuCode
	 */
	public String getSkuCode() {
		return mSkuCode;
	}

	/**
	 * @param pSkuCode the mSkuCode to set
	 */
	public void setSkuCode(String pSkuCode) {
		this.mSkuCode = pSkuCode;
	}

	/**
	 * @return the facetName
	 */
	public String getFacetName() {
		return mFacetName;
	}

	/**
	 * @param pFacetName the facetName to set
	 */
	public void setFacetName(String pFacetName) {
		mFacetName = pFacetName;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return mBrand;
	}

	/**
	 * @param pBrand the brand to set
	 */
	public void setBrand(String pBrand) {
		mBrand = pBrand;
	}

	/**
	 * @return the character
	 */
	public String getCharacter() {
		return mCharacter;
	}

	/**
	 * @param pCharacter the character to set
	 */
	public void setCharacter(String pCharacter) {
		mCharacter = pCharacter;
	}

	/**
	 * @return the shopbyPageURL
	 */
	public String getShopbyPageURL() {
		return mShopbyPageURL;
	}

	/**
	 * @param pShopbyPageURL the shopbyPageURL to set
	 */
	public void setShopbyPageURL(String pShopbyPageURL) {
		mShopbyPageURL = pShopbyPageURL;
	}

	/**
	 * @return the searchPageURL
	 */
	public String getSearchPageURL() {
		return mSearchPageURL;
	}

	/**
	 * @param pSearchPageURL the searchPageURL to set
	 */
	public void setSearchPageURL(String pSearchPageURL) {
		mSearchPageURL = pSearchPageURL;
	}

	/**
	 * @return the skuLongDescription
	 */
	public String getSkuLongDescription() {
		return mSkuLongDescription;
	}

	/**
	 * @param pSkuLongDescription the skuLongDescription to set
	 */
	public void setSkuLongDescription(String pSkuLongDescription) {
		mSkuLongDescription = pSkuLongDescription;
	}

	/**
	 * @return the mItemInStorePickUp
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * @param pItemInStorePickUp the mItemInStorePickUp to set
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		this.mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * @return the mUpcNumbers
	 */
	public String getUpcNumbers() {
		return mUpcNumbers;
	}

	/**
	 * @param pUpcNumbers the mUpcNumbers to set
	 */
	public void setUpcNumbers(String pUpcNumbers) {
		this.mUpcNumbers = pUpcNumbers;
	}

	/**
	 * @return the isNew
	 */
	public String getIsNew() {
		return mIsNew;
	}

	/**
	 * @param pIsNew the isNew to set
	 */
	public void setIsNew(String pIsNew) {
		this.mIsNew = pIsNew;
	}

	/**
	 * @return the mRusItemNumber
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	/**
	 * @param pRusItemNumber the mRusItemNumber to set
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		this.mRusItemNumber = pRusItemNumber;
	}

	/**
	 * @return the skuChannelAvailability
	 */
	public String getSkuChannelAvailability() {
		return mSkuChannelAvailability;
	}

	/**
	 * @param pSkuChannelAvailability the skuChannelAvailability to set
	 */
	public void setSkuChannelAvailability(String pSkuChannelAvailability) {
		mSkuChannelAvailability = pSkuChannelAvailability;
	}

	/**
	 * @return the contentAjaxknockoutCollection
	 */
	public String getContentAjaxknockoutCollection() {
		return mContentAjaxknockoutCollection;
	}

	/**
	 * @param pContentAjaxknockoutCollection the contentAjaxknockoutCollection to set
	 */
	public void setContentAjaxknockoutCollection(
			String pContentAjaxknockoutCollection) {
		mContentAjaxknockoutCollection = pContentAjaxknockoutCollection;
	}
	
	/**
	 * @return the ShipToStoreEligible
	 */
	public String getShipToStoreEligible() {
		return mShipToStoreEligible;
	}

	/**
	 * @param pShipToStoreEligible the ShipToStoreEligible to set
	 */
	public void setShipToStoreEligible(
			String pShipToStoreEligible) {
		mShipToStoreEligible = pShipToStoreEligible;
	}
	/**
	 * @return the pagesPromotion
	 */
	public String getPagesPromotion() {
		return mPagesPromotion;
	}

	/**
	 * @param pPagesPromotion the pagesPromotion to set
	 */
	public void setPagesPromotion(String pPagesPromotion) {
		mPagesPromotion = pPagesPromotion;
	}

	/** property to hold isStylized property. */
	private String mIsStylized;
	
	/**
	 * @return the isStylized
	 */
	public String getIsStylized() {
		return mIsStylized;
	}

	/**
	 * @param pIsStylized the isStylized to set
	 */
	public void setIsStylized(String pIsStylized) {
		mIsStylized = pIsStylized;
	}
	
	/** property to hold registryWarningIndicator property. */
	private String mRegistryWarningIndicator;
	
	/**
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * @param pRegistryWarningIndicator the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}
	/**
	 * @return the collectionImageUrl
	 */
	public String getCollectionImageUrl() {
		return mCollectionImageUrl;
	}

	/**
	 * @param pCollectionImageUrl the collectionImageUrl to set
	 */
	public void setCollectionImageUrl(String pCollectionImageUrl) {
		mCollectionImageUrl = pCollectionImageUrl;
	}
	/**
	 * @return the skuListSize
	 */
	public String getSkuListSize() {
		return mSkuListSize;
	}

	/**
	 * @param pSkuListSize the skuListSize to set
	 */
	public void setSkuListSize(String pSkuListSize) {
		mSkuListSize = pSkuListSize;
	}
	
     private List<String> mCollectionProductsChannelList;
     /**
     * @return the CollectionProductsChannelList
     */
     public List<String> getCollectionProductsChannelList() {
            return mCollectionProductsChannelList;
     }

     /**
     * @param pCollectionProductsChannelList the CollectionProductsChannelList to set
     */
     public void setCollectionProductsChannelList(
                   List<String> pCollectionProductsChannelList) {
            mCollectionProductsChannelList = pCollectionProductsChannelList;
     }

	/**
	 * @return the mShipToStore
	 */
	public String getShipToStore() {
		return mShipToStore;
	}

	/**
	 * @param pShipToStore the mShipToStore to set
	 */
	public void setShipToStore(String pShipToStore) {
		this.mShipToStore = pShipToStore;
	}

	/**
	 * @return the mApplePayTestURL
	 */
	public String getApplePayTestURL() {
		return mApplePayTestURL;
	}

	/**
	 * @param pApplePayTestURL the mApplePayTestURL to set
	 */
	public void setApplePayTestURL(String pApplePayTestURL) {
		this.mApplePayTestURL = pApplePayTestURL;
	}

	/**
	 * @return the mIspu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the mIspu to set
	 */
	public void setIspu(String pIspu) {
		mIspu = pIspu;
	}

	/**
	 * @return the mPriceAscendingSort
	 */
	public String getPriceAscendingSort() {
		return mPriceAscendingSort;
	}

	/**
	 * @param pPriceAscendingSort the mPriceAscendingSort to set
	 */
	public void setPriceAscendingSort(String pPriceAscendingSort) {
		mPriceAscendingSort = pPriceAscendingSort;
	}

	/**
	 * @return the mPriceDescendingSort
	 */
	public String getPriceDescendingSort() {
		return mPriceDescendingSort;
	}

	/**
	 * @param pPriceDescendingSort the mPriceDescendingSort to set
	 */
	public void setPriceDescendingSort(String pPriceDescendingSort) {
		mPriceDescendingSort = pPriceDescendingSort;
	}
     
}
