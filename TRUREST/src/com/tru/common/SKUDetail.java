package com.tru.common;

import java.util.List;

import com.tru.rest.constants.TRURestConstants;

/**
 * This class is SKU.
 * @author PA
 * @version 1.0
 */
public class SKUDetail {
	/**
	 * This property hold reference for DisplayName.
	 */
	private String mDisplayName;
	/**
	 * This property hold reference for CreationDate.
	 */
	private String mCreationDate;
	/**
	 * This property hold reference for RepositoryId.
	 */
	private String mRepositoryId;
	/**
	 * This property hold reference for OnSale.
	 */
	private boolean mOnSale;
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * This property hold reference for ReviewRating.
	 */
	private String mReviewRating;
	/**
	 * This property hold reference for ReviewCount.
	 */
	private String mReviewCount;
	/**
	 * This property hold reference for FrSuggestedAgeMin.
	 */
	private String mFrSuggestedAgeMin;
	/**
	 * This property hold reference for FrSuggestedAgeMax.
	 */
	private String mFrSuggestedAgeMax;
	/**
	 * This property hold reference for PrimaryImage.
	 */
	private String mPrimaryImage;
	/**
	 * This property hold reference for SecondaryImage.
	 */
	private String mSecondaryImage;
	/**
	 * This property hold reference for SalePrice.
	 */
	private Double mSalePrice;
	/**
	 * This property hold reference for ListPrice.
	 */
	private Double mListPrice;
	/**
	 * This property hold reference for ChildWeightMin.
	 */
	private Integer mChildWeightMin;
	/**
	 * This property hold reference for ChildWeightMax.
	 */
	private Integer mChildWeightMax;
	/**
	 * This property hold reference for AlternateImage.
	 */
	private List<String> mAlternateImage;
	/**
	 * This property hold reference for StreetDate.
	 */
	private String mStreetDate;
	/**
	 * This property hold reference for RegistryEligibility.
	 */
	private boolean mRegistryEligibility;
	/**
	 * This property hold reference for VideoGameEsrb.
	 */
	private String mVideoGameEsrb;
	/**
	 * This property hold reference for VideoGameGenre.
	 */
	private String mVideoGameGenre;
	/**
	 * This property hold reference for PriceDetails.
	 */
	private PriceDetails mPriceDetails;
	/**
	 * This property hold reference for CustomerPurchaseLimit.
	 */
	private String mCustomerPurchaseLimit;
	/**
	 * This property hold reference for PriceDisplay.
	 */
	private String mPriceDisplay;
	
	/**
	 * This property hold reference for mInventoryStatus.
	 */
	private String mInventoryStatus;
	
	/**
	 * This property hold reference for mStoreId.
	 */
	private boolean mStoreInventory;
	
	/**
	 * This property hold reference for PriceDisplay.
	 */
	private boolean mIsStrikeThrough;
	
	/**
	 * This property hold reference for PriceDisplay.
	 */
	private boolean mHasPriceRange;
	
	/**
	 * This property hold reference for PriceDisplay.
	 */
	private boolean mShipInOrigContainer;
	
	/**
	 * This property hold reference for PriceDisplay.
	 */
	private boolean mUnCartable;
	/**
	 * This property hold reference for mPromos.
	 */
	private List<Promo> mPromos;
	
	/** 
	 * property to hold isStylized property.
	 */
	private boolean mIsStylized;
	
	/**
	 * property to hold OriginalParentProduct
	 */
	private String mOriginalParentProduct;
	
	/**
	 * Property to hold RegistryWarningIndicator.
	 */
	private String mRegistryWarningIndicator;
	
	/**
	 * property to hold mCollectionName.
	 */
	private String mCollectionName;
	
	/**
	 * property to hold mS2s.
	 */
	private String mS2s;
	
	/**
	 * property to hold mIspu.
	 */
	private String mIspu;
	
	/**
	 * @return the originalParentProduct
	 */
	public String getOriginalParentProduct() {
		return mOriginalParentProduct;
	}

	/**
	 * @param pOriginalParentProduct the originalParentProduct to set
	 */
	public void setOriginalParentProduct(String pOriginalParentProduct) {
		mOriginalParentProduct = pOriginalParentProduct;
	}

	/**
	 * @return the promos
	 */
	public List<Promo> getPromos() {
		return mPromos;
	}

	/**
	 * @param pPromos the promos to set
	 */
	public void setPromos(List<Promo> pPromos) {
		mPromos = pPromos;
	}

	/**
	 * This property hold reference for SwatchImage.
	 */
	private List<SwatchColors> mSwatchImage;
	/**
	 * @return the unCartable
	 */
	public boolean isUnCartable() {
		return mUnCartable;
	}

	/**
	 * @param pUnCartable the unCartable to set
	 */
	public void setUnCartable(boolean pUnCartable) {
		mUnCartable = pUnCartable;
	}

	/**
	 * @return the shipInOrigContainer
	 */
	public boolean isShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * @param pShipInOrigContainer the shipInOrigContainer to set
	 */
	public void setShipInOrigContainer(boolean pShipInOrigContainer) {
		mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * @return the isStrikeThrough
	 */
	public boolean isIsStrikeThrough() {
		return mIsStrikeThrough;
	}

	/**
	 * @param pIsStrikeThrough the isStrikeThrough to set
	 */
	public void setIsStrikeThrough(boolean pIsStrikeThrough) {
		mIsStrikeThrough = pIsStrikeThrough;
	}

	/**
	 * @return the hasPriceRange
	 */
	public boolean isHasPriceRange() {
		return mHasPriceRange;
	}

	/**
	 * @param pHasPriceRange the hasPriceRange to set
	 */
	public void setHasPriceRange(boolean pHasPriceRange) {
		mHasPriceRange = pHasPriceRange;
	}

	/**
	 * @return the PriceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}

	/**
	 * @param pPriceDisplay the PriceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * @param pCustomerPurchaseLimit
	 *            the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * @return the priceDetails
	 */
	public PriceDetails getPriceDetails() {
		return mPriceDetails;
	}

	/**
	 * @param pPriceDetails
	 *            the priceDetails to set
	 */
	public void setPriceDetails(PriceDetails pPriceDetails) {
		mPriceDetails = pPriceDetails;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * @param pDisplayName
	 *            the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return mCreationDate;
	}

	/**
	 * @param pCreationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(String pCreationDate) {
		mCreationDate = pCreationDate;
	}

	/**
	 * @return the repositoryId
	 */
	public String getRepositoryId() {
		return mRepositoryId;
	}

	/**
	 * @param pRepositoryId
	 *            the repositoryId to set
	 */
	public void setRepositoryId(String pRepositoryId) {
		mRepositoryId = pRepositoryId;
	}

	/**
	 * @return the onSale
	 */
	public boolean isOnSale() {
		return mOnSale;
	}

	/**
	 * @param pOnSale
	 *            the onSale to set
	 */
	public void setOnSale(boolean pOnSale) {
		mOnSale = pOnSale;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * @param pSiteId
	 *            the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	/**
	 * @return the reviewRating
	 */
	public String getReviewRating() {
		return mReviewRating;
	}

	/**
	 * @param pReviewRating
	 *            the reviewRating to set
	 */
	public void setReviewRating(String pReviewRating) {
		mReviewRating = pReviewRating;
	}

	/**
	 * @return the reviewCount
	 */
	public String getReviewCount() {
		return mReviewCount;
	}

	/**
	 * @param pReviewCount
	 *            the reviewCount to set
	 */
	public void setReviewCount(String pReviewCount) {
		mReviewCount = pReviewCount;
	}

	/**
	 * @return the mfrSuggestedAgeMin
	 */
	public String getMfrSuggestedAgeMin() {
		return mFrSuggestedAgeMin;
	}

	/**
	 * @param pMfrSuggestedAgeMin
	 *            the mfrSuggestedAgeMin to set
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		mFrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * @return the mfrSuggestedAgeMax
	 */
	public String getMfrSuggestedAgeMax() {
		return mFrSuggestedAgeMax;
	}

	/**
	 * @param pMfrSuggestedAgeMax
	 *            the mfrSuggestedAgeMax to set
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mFrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}

	/**
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * @param pPrimaryImage
	 *            the primaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}

	/**
	 * @return the secondaryImage
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}

	/**
	 * @param pSecondaryImage
	 *            the secondaryImage to set
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}

	/**
	 * @return the alternateImage
	 */
	public List<String> getAlternateImage() {
		return mAlternateImage;
	}

	/**
	 * @param pAlternateImage
	 *            the alternateImage to set
	 */
	public void setAlternateImage(List<String> pAlternateImage) {
		mAlternateImage = pAlternateImage;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return mSalePrice;
	}

	/**
	 * @param pSalePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * @param pStreetDate
	 *            the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * @return the registryEligibility
	 */
	public boolean isRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * @param pRegistryEligibility
	 *            the registryEligibility to set
	 */
	public void setRegistryEligibility(boolean pRegistryEligibility) {
		mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * @return the videoGameEsrb
	 */
	public String getVideoGameEsrb() {
		return mVideoGameEsrb;
	}

	/**
	 * @param pVideoGameEsrb
	 *            the videoGameEsrb to set
	 */
	public void setVideoGameEsrb(String pVideoGameEsrb) {
		mVideoGameEsrb = pVideoGameEsrb;
	}

	/**
	 * @return the videoGameGenre
	 */
	public String getVideoGameGenre() {
		return mVideoGameGenre;
	}

	/**
	 * @param pVideoGameGenre
	 *            the videoGameGenre to set
	 */
	public void setVideoGameGenre(String pVideoGameGenre) {
		mVideoGameGenre = pVideoGameGenre;
	}

	/**
	 * @return the childWeightMin
	 */
	public Integer getChildWeightMin() {
		return mChildWeightMin;
	}

	/**
	 * @param pChildWeightMin
	 *            the childWeightMin to set
	 */
	public void setChildWeightMin(Integer pChildWeightMin) {
		mChildWeightMin = pChildWeightMin;
	}

	/**
	 * @return the childWeightMax
	 */
	public Integer getChildWeightMax() {
		return mChildWeightMax;
	}

	/**
	 * @param pChildWeightMax
	 *            the childWeightMax to set
	 */
	public void setChildWeightMax(Integer pChildWeightMax) {
		mChildWeightMax = pChildWeightMax;
	}

	/**
	 * @return the listPrice
	 */
	public Double getListPrice() {
		return mListPrice;
	}

	/**
	 * @param pListPrice
	 *            the listPrice to set
	 */
	public void setListPrice(Double pListPrice) {
		mListPrice = pListPrice;
	}
	
	/**
	 * @return the swatchImage
	 */
	public List<SwatchColors> getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * @param pSwatchImage
	 *            the swatchImage to set
	 */
	public void setSwatchImage(List<SwatchColors> pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}
	/**
	 * This property hold reference for ChannelAvailability.
	 */
	private String mChannelAvailability=TRURestConstants.CHANNEL_AVAILABILITY_STATUS;
	
	
	/**
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}
	/**
	 * @param pChannelAvailability the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}
	
	
	/**
	 * @return the isStylized
	 */
	public boolean getIsStylized() {
		return mIsStylized;
	}

	/**
	 * @param pIsStylized the isStylized to set
	 */
	public void setIsStylized(boolean pIsStylized) {
		mIsStylized = pIsStylized;
	}
	
	/**
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * @param pRegistryWarningIndicator the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}
	/**
	 * @return the CollectionName
	 */
	public String getCollectionName() {
		return mCollectionName;
	}
	/**
	 * @param pCollectionName the collectionName to set
	 */
	public void setCollectionName(String pCollectionName) {
		mCollectionName = pCollectionName;
	}

	/**
	 * @return the mS2s
	 */
	public String getS2s() {
		return mS2s;
	}

	/**
	 * @param pS2s the mS2s to set
	 */
	public void setS2s(String pS2s) {
		this.mS2s = pS2s;
	}

	/**
	 * @return the mIspu
	 */
	public String getIspu() {
		return mIspu;
	}

	/**
	 * @param pIspu the mIspu to set
	 */
	public void setIspu(String pIspu) {
		this.mIspu = pIspu;
	}

	/**
	 * @return the mInventoryStatus
	 */
	public String getInventoryStatus() {
		return mInventoryStatus;
	}

	/**
	 * @param pInventoryStatus the mInventoryStatus to set
	 */
	public void setInventoryStatus(String pInventoryStatus) {
		this.mInventoryStatus = pInventoryStatus;
	}

	/**
	 * @return the mStoreInventory
	 */
	public boolean isStoreInventory() {
		return mStoreInventory;
	}

	/**
	 * @param pStoreInventory the mStoreInventory to set
	 */
	public void setStoreInventory(boolean pStoreInventory) {
		this.mStoreInventory = pStoreInventory;
	}

	
}
