package com.tru.common;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class holds product related information for registry.
 * 
 * @author PA
 * @version 1.0
 */
public class RegistryProductInfoVO {

	/**
	 * Property to hold mPdpURL.
	 */
	private String mPdpURL;
	
	/**
	 * Property to hold mProductStatus.
	 */
	private String mProductStatus;
	
	/**
	 * Property to hold mJuvenileSizesList.
	 */
	private List<String> mJuvenileSizesList;
	
	/**
	 * Property to hold mRusItemNumber.
	 */
	private String mRusItemNumber;
	
	/**
	 * Property to hold mColorCodeToSwatchImageMap.
	 */
	private Map<String, String> mColorCodeToSwatchImageMap;
	
	/**
	 * Property to hold mDisplayName.
	 */
	private String mDisplayName;
	
	/**
	 * Property to hold mChildSKUsList.
	 */
	private  Set<RegistrySKUInfoVO> mChildSKUsList;
	
	/**
	 * Property to hold  mParentCategoryId.
	 */
	private String mParentCategoryId;

	/**
	 * @return the mPdpURL
	 */
	public String getPdpURL() {
		return mPdpURL;
	}

	/**
	 * @param pPdpURL the mPdpURL to set
	 */
	public void setPdpURL(String pPdpURL) {
		this.mPdpURL = pPdpURL;
	}

	/**
	 * @return the mProductStatus
	 */
	public String getProductStatus() {
		return mProductStatus;
	}

	/**
	 * @param pProductStatus the mProductStatus to set
	 */
	public void setProductStatus(String pProductStatus) {
		this.mProductStatus = pProductStatus;
	}

	/**
	 * @return the mJuvenileSizesList
	 */
	public List<String> getJuvenileSizesList() {
		return mJuvenileSizesList;
	}

	/**
	 * @param pJuvenileSizesList the mJuvenileSizesList to set
	 */
	public void setJuvenileSizesList(List<String> pJuvenileSizesList) {
		this.mJuvenileSizesList = pJuvenileSizesList;
	}

	/**
	 * @return the mRusItemNumber
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	/**
	 * @param pRusItemNumber the mRusItemNumber to set
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		this.mRusItemNumber = pRusItemNumber;
	}

	/**
	 * @return the mColorCodeToSwatchImageMap
	 */
	public Map<String, String> getColorCodeToSwatchImageMap() {
		return mColorCodeToSwatchImageMap;
	}

	/**
	 * @param pColorCodeToSwatchImageMap the mColorCodeToSwatchImageMap to set
	 */
	public void setColorCodeToSwatchImageMap(
			Map<String, String> pColorCodeToSwatchImageMap) {
		this.mColorCodeToSwatchImageMap = pColorCodeToSwatchImageMap;
	}

	/**
	 * @return the mDisplayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * @param pDisplayName the mDisplayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		this.mDisplayName = pDisplayName;
	}

	/**
	 * @return the mChildSKUsList
	 */
	public Set<RegistrySKUInfoVO> getChildSKUsList() {
		return mChildSKUsList;
	}

	/**
	 * @param pChildSKUsList the mChildSKUsList to set
	 */
	public void setChildSKUsList(Set<RegistrySKUInfoVO> pChildSKUsList) {
		this.mChildSKUsList = pChildSKUsList;
	}

	/**
	 * @return the mParentCategoryId
	 */
	public String getParentCategoryId() {
		return mParentCategoryId;
	}

	/**
	 * @param pParentCategoryId the mParentCategoryId to set
	 */
	public void setParentCategoryId(String pParentCategoryId) {
		this.mParentCategoryId = pParentCategoryId;
	}
	
	
}
