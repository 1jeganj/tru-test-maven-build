package com.tru.common;

import java.util.ArrayList;
import java.util.Map;

/**
 * This class is TwoLevelCategoryTreeMap.
 * @author PA
 * @version 1.0
 */
public class TwoLevelCategoryTreeMap {
	/**
	 * This property hold reference for ClassificationMap.
	 */
	private Map<String,ArrayList<Classification>> mClassificationMap;
	
	
	/**
	 * @return the classificationMap
	 */
	public Map<String, ArrayList<Classification>> getClassificationMap() {
		return mClassificationMap;
	}

	/**
	 * @param pClassificationMap the classificationMap to set
	 */
	public void setClassificationMap(
			Map<String, ArrayList<Classification>> pClassificationMap) {
		mClassificationMap = pClassificationMap;
	} 
}
