
package com.tru.common;

import atg.commerce.endeca.cache.DimensionValueCacheObject;

/**
 * This class is DimCacheObj.
 * @author PA
 * @version 1.0
 */
public class DimCacheObjectVO {
	/**
	 * This property hold reference for DimensionValueCacheObject.
	 */
	private DimensionValueCacheObject mDimValueCacheObject;
	/**
	 * This property hold reference for RegistryClassification.
	 */
	private boolean mRegistryClassification;
	/**
	 * @return the dimValueCacheObject
	 */
	public DimensionValueCacheObject getDimValueCacheObject() {
		return mDimValueCacheObject;
	}

	/**
	 * @param pDimValueCacheObject the dimValueCacheObject to set
	 */
	public void setDimValueCacheObject(
			DimensionValueCacheObject pDimValueCacheObject) {
		mDimValueCacheObject = pDimValueCacheObject;
	}

	/**
	 * @return the registryClassification
	 */
	public boolean isRegistryClassification() {
		return mRegistryClassification;
	}

	/**
	 * @param pRegistryClassification the registryClassification to set
	 */
	public void setRegistryClassification(boolean pRegistryClassification) {
		mRegistryClassification = pRegistryClassification;
	}
}
