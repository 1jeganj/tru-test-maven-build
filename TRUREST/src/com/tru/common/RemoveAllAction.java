package com.tru.common;

/**
 * This class is RemoveAllAction.
 * @author PA
 * @version 1.0
 */
public class RemoveAllAction {
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mNavigationState;

	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}

	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
}
