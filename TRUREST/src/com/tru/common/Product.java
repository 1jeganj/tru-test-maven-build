package com.tru.common;

import com.tru.rest.constants.TRURestConstants;

/**
 * This class is Product.
 * @author PA
 * @version 1.0
 */
public class Product {
	/**
	 * This property hold reference for DisplayName.
	 */
	private String mDisplayName;
	/**
	 * This property hold reference for SiteId.
	 */
	private String mSiteId;
	/**
	 * This property hold reference for RepositoryId.
	 */
	private String mRepositoryId;
	/**
	 * This property hold reference for longDescription.
	 */
	private String mLongDescription;
	/**
	 * This property hold reference for Url.
	 */
	private String mUrl;
	/**
	 * This property hold reference for Category.
	 */
	private String mCategory;
	/**
	 * This property hold reference for ShortDescription.
	 */
	private String mShortDescription;
	/**
	 * This property hold reference for Language.
	 */
	private String mLanguage;

	/**
	 * This property hold reference for PromotionalStickerDisplay.
	 */
	private String mPromotionalStickerDisplay;
	/**
	 * This property hold reference for Brand.
	 */
	private String mBrand;
	/**
	 * This property hold reference for OnlinePID.
	 */
	private String mOnlinePID;
	
	/**
	 * This property hold reference for OnlinePID.
	 */
	private boolean mIsCollectionProduct = TRURestConstants.BOOLEAN_FALSE;
	/**
	 * @return the isCollectionProduct
	 */
	public boolean isIsCollectionProduct() {
		return mIsCollectionProduct;
	}
	/**
	 * @param pIsCollectionProduct the isCollectionProduct to set
	 */
	public void setIsCollectionProduct(boolean pIsCollectionProduct) {
		mIsCollectionProduct = pIsCollectionProduct;
	}
	/**
	 * @return the mOnlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}
	/**
	 * @param pOnlinePID the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return mDisplayName;
	}
	/**
	 * @param pDisplayName the displayName to set
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * @return the repositoryId
	 */
	public String getRepositoryId() {
		return mRepositoryId;
	}
	/**
	 * @param pRepositoryId the repositoryId to set
	 */
	public void setRepositoryId(String pRepositoryId) {
		mRepositoryId = pRepositoryId;
	}
	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return mLongDescription;
	}
	/**
	 * @param pLongDescription the longDescription to set
	 */
	public void setLongDescription(String pLongDescription) {
		mLongDescription = pLongDescription;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return mUrl;
	}
	/**
	 * @param pUrl the url to set
	 */
	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return mCategory;
	}
	/**
	 * @param pCategory the category to set
	 */
	public void setCategory(String pCategory) {
		mCategory = pCategory;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return mShortDescription;
	}
	/**
	 * @param pShortDescription the shortDescription to set
	 */
	public void setShortDescription(String pShortDescription) {
		mShortDescription = pShortDescription;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return mLanguage;
	}
	/**
	 * @param pLanguage the language to set
	 */
	public void setLanguage(String pLanguage) {
		mLanguage = pLanguage;
	}
	
	/**
	 * @return the promotionalStickerDisplay
	 */
	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}
	/**
	 * @param pPromotionalStickerDisplay the promotionalStickerDisplay to set
	 */
	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return mBrand;
	}
	/**
	 * @param pBrand the brand to set
	 */
	public void setBrand(String pBrand) {
		mBrand = pBrand;
	}
}
