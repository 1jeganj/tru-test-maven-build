package com.tru.common;


/**
 * This class is Items.
 * @author PA
 * @version 1.0
 */
public class Items {
	
	/** The  product id. */
	private String mProductId;
	
	/** The m product name. */
	private String mProductName;
	
	/** The m quantity. */
	private Double mQuantity;
	
	/** The m price. */
	private Double mPrice;
	
	/** The m saved amount. */
	private Double mSavedAmount;
	
	/** The m sale price. */
	private Double mSalePrice;
	/**
	 * @return the mProductId
	 */
	public String getProductId() {
		return mProductId;
	}
	/**
	 * @param pProductId the mProductId to set
	 */
	public void setProductId(String pProductId) {
		this.mProductId = pProductId;
	}
	/**
	 * @return the mProductName
	 */
	public String getProductName() {
		return mProductName;
	}
	/**
	 * @param pProductName the mProductName to set
	 */
	public void setProductName(String pProductName) {
		this.mProductName = pProductName;
	}
	/**
	 * @return the mQuantity
	 */
	public Double getQuantity() {
		return mQuantity;
	}
	/**
	 * @param pQuantity the mQuantity to set
	 */
	public void setQuantity(double pQuantity) {
		this.mQuantity = pQuantity;
	}
	/**
	 * @return the mPrice
	 */
	public Double getPrice() {
		return mPrice;
	}
	/**
	 * @param pPrice the mPrice to set
	 */
	public void setPrice(double pPrice) {
		this.mPrice = pPrice;
	}
	/**
	 * @return the mSavedAmount
	 */
	public Double getSavedAmount() {
		return mSavedAmount;
	}
	/**
	 * @param pSavedAmount the mSavedAmount to set
	 */
	public void setSavedAmount(double pSavedAmount) {
		this.mSavedAmount = pSavedAmount;
	}
	/**
	 * @return the mSalePrice
	 */
	public Double getSalePrice() {
		return mSalePrice;
	}
	/**
	 * @param pSalePrice the mSalePrice to set
	 */
	public void setSalePrice(double pSalePrice) {
		this.mSalePrice = pSalePrice;
	}
	
	

}
