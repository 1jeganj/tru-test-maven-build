package com.tru.common;

/**
 * This class is Redirect.
 * @author PA
 * @version 1.0
 */
public class Redirect {
	/**
	 * This property hold reference for LinkUrl.
	 */
	private String mLinkUrl;

	/**
	 * @return the linkUrl
	 */
	public String getLinkUrl() {
		return mLinkUrl;
	}

	/**
	 * @param pLinkUrl the linkUrl to set
	 */
	public void setLinkUrl(String pLinkUrl) {
		mLinkUrl = pLinkUrl;
	} 
}
