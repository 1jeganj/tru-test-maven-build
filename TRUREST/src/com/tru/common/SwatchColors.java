package com.tru.common;

/**
 * This class is SwatchColors.
 * @author PA
 * @version 1.0
 */
public class SwatchColors {
	/**
	 * This property hold reference for mImageURL.
	 */
	private String mImageURL;
	
	/**
	 * @return the mImageURL
	 */
	public String getImageURL() {
		return mImageURL;
	}
	/**
	 * @param pImageURL the mImageURL to set
	 */
	public void setImageURL(String pImageURL) {
		mImageURL = pImageURL;
	}
}
