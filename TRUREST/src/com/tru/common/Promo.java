package com.tru.common;

/**
 * This class is Promo.
 * @author PA
 * @version 1.0
 */
public class Promo {
	/**
	 * This property hold reference for mPromoId.
	 */
	private String mPromoId;
	/**
	 * This property hold reference for mDescription.
	 */
	private String mDescription;
	
	/**
	 * This property hold reference for mStartDate.
	 */
	private String mStartDate;
	
	/**
	 * This property hold reference for mEndDate.
	 */
	private String mEndDate;
	
	/**
	 * This property hold reference for mRank.
	 */
	private String mRank;

	/**
	 * @return the promoId
	 */
	public String getPromoId() {
		return mPromoId;
	}

	/**
	 * @param pPromoId the promoId to set
	 */
	public void setPromoId(String pPromoId) {
		mPromoId = pPromoId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
	 * @param pDescription the description to set
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return mStartDate;
	}

	/**
	 * @param pStartDate the startDate to set
	 */
	public void setStartDate(String pStartDate) {
		mStartDate = pStartDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return mEndDate;
	}

	/**
	 * @param pEndDate the endDate to set
	 */
	public void setEndDate(String pEndDate) {
		mEndDate = pEndDate;
	}

	/**
	 * @return the rank
	 */
	public String getRank() {
		return mRank;
	}

	/**
	 * @param pRank the rank to set
	 */
	public void setRank(String pRank) {
		mRank = pRank;
	}
	
}
