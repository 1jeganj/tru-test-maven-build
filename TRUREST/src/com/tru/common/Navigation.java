package com.tru.common;

import java.util.List;

import com.tru.rest.constants.TRURestConstants;

/**
 * This class is Navigation.
 * @author PA
 * @version 1.1
 */
public class Navigation {
	/** property holds Name. */
	private String mName;
	/** property holds Refinements. */
	private List<Refinements> mRefinements;	
	/** property holds Displayable. */
	private boolean mDisplayable=TRURestConstants.BOOLEAN_TRUE;
	/**
	 * @return the displayable
	 */
	public boolean isDisplayable() {
		return mDisplayable;
	}

	/**
	 * @param pDisplayable the displayable to set
	 */
	public void setDisplayable(boolean pDisplayable) {
		mDisplayable = pDisplayable;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param pName the name to set
	 */
	public void setName(String pName) {
		mName = pName;
	}

	/**
	 * @return the refinements
	 */
	public List<Refinements> getRefinements() {
		return mRefinements;
	}

	/**
	 * @param pRefinements the refinements to set
	 */
	public void setRefinements(List<Refinements> pRefinements) {
		mRefinements = pRefinements;
	}

	
	 	
}
