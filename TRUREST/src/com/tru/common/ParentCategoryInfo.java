
package com.tru.common;

import java.util.Map;

/**
 * This class is ParentCategoryInfo.
 * @author PA
 * @version 1.0
 */
public class ParentCategoryInfo {
	private Map<String, String> mParentCategoryIdMap;

	/**
	 * @return the mParentCategoryIdMap
	 */
	public Map<String, String> getParentCategoryIdMap() {
		return mParentCategoryIdMap;
	}

	/**
	 * @param pParentCategoryIdMap the mParentCategoryIdMap to set
	 */
	public void setParentCategoryIdMap(Map<String, String> pParentCategoryIdMap) {
		this.mParentCategoryIdMap = pParentCategoryIdMap;
	}
	

}
