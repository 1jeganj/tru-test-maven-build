package com.tru.common;

/**
 * This class is AdjustedTerms.
 * @author PA
 * @version 1.0
 */
public class AdjustedTerms {
	/**
	 * This property hold reference for mAdjustedTerms.
	 */
	private String mAdjustedTerms;
	
	/**
	 * @return the adjustedTerms
	 */
	public String getAdjustedTerms() {
		return mAdjustedTerms;
	}

	/**
	 * @param pAdjustedTerms the adjustedTerms to set
	 */
	public void setAdjustedTerms(String pAdjustedTerms) {
		mAdjustedTerms = pAdjustedTerms;
	}

}
