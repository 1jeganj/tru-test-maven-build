package com.tru.common;

/**
 * This class is Records.
 * @author PA
 * @version 1.0
 * 
 */
public class EndecaRecords {
	
	//Long numRecords;
	private Product mProduct;	
	private SKURecords mRecords;
	
	/**
	 * @return the pRecords
	 *//*
	public Long getNumRecords() {
		return numRecords;
	}
	*//**
	 * @param pPRecords the pRecords to set
	 *//*
	public void setNumRecords(Long pPRecords) {
		numRecords = pPRecords;
	}*/
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return mProduct;
	}
	/**
	 * @param pProduct the product to set
	 */
	public void setProduct(Product pProduct) {
		mProduct = pProduct;
	}
	/**
	 * @return the records
	 */
	public SKURecords getRecords() {
		return mRecords;
	}
	/**
	 * @param pRecords the records to set
	 */
	public void setRecords(SKURecords pRecords) {
		mRecords = pRecords;
	}
}
