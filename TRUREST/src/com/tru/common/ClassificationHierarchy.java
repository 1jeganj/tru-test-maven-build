package com.tru.common;

import java.util.ArrayList;
import java.util.Map;

/**
 * This class is ClassificationHierarchy.
 * @author PA
 * @version 1.0
 */
public class ClassificationHierarchy {
	/**
	 * This property hold reference for CurrentCategoryId.
	 */
	private String mCurrentCategoryId;
	/**
	 * This property hold reference for TwoLevelCategoryTreeMap.
	 */
	private Map<String,ArrayList<Classification>> mTwoLevelCategoryTreeMap;
	/**
	 * @return the currentCategoryId
	 */
	public String getCurrentCategoryId() {
		return mCurrentCategoryId;
	}
	/**
	 * @param pCurrentCategoryId the currentCategoryId to set
	 */
	public void setCurrentCategoryId(String pCurrentCategoryId) {
		mCurrentCategoryId = pCurrentCategoryId;
	}
	/**
	 * @return the twoLevelCategoryTreeMap
	 */
	public Map<String,ArrayList<Classification>> getTwoLevelCategoryTreeMap() {
		return mTwoLevelCategoryTreeMap;
	}
	/**
	 * @param pTwoLevelCategoryTreeMap the twoLevelCategoryTreeMap to set
	 */
	public void setTwoLevelCategoryTreeMap(Map<String,ArrayList<Classification>> pTwoLevelCategoryTreeMap) {
		mTwoLevelCategoryTreeMap = pTwoLevelCategoryTreeMap;
	}
}
