package com.tru.common;

/**
 * This class is Refinements.
 * @author PA
 * @version 1.0
 */
public class Refinements {
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mNavigationState;
	/**
	 * This property hold reference for Label.
	 */
	private String mLabel;
	/**
	 * This property hold reference for Count.
	 */
	private Integer mCount;
	/**
	 * This property hold reference for Multiselect.
	 */
	boolean mMultiselect;
	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}
	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return mLabel;
	}
	/**
	 * @param pLabel the label to set
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}
	/**
	 * @return the count
	 */
	public Integer getCount() {
		return mCount;
	}
	/**
	 * @param pCount the count to set
	 */
	public void setCount(Integer pCount) {
		mCount = pCount;
	}
	/**
	 * @return the multiselect
	 */
	public boolean isMultiselect() {
		return mMultiselect;
	}
	/**
	 * @param pMultiselect the multiselect to set
	 */
	public void setMultiselect(boolean pMultiselect) {
		mMultiselect = pMultiselect;
	}
}
