package com.tru.common;

/**
 * This class is PriceDetails.
 * @author PA
 * @version 1.0
 */
public class PriceDetails {
	/**
	 * This property hold reference for MaximumPrice.
	 */
	private	Double mMaximumPrice;
	/**
	 * This property hold reference for MinimumPrice.
	 */
	private	Double mMinimumPrice;
	/**
	 * This property hold reference for SavedAmount.
	 */
	private	Double mSavedAmount;
	/**
	 * This property hold reference for SavedPercentage.
	 */
	private	Double mSavedPercentage;
			
	/**
	 * @return the maximumPrice
	 */
	public Double getMaximumPrice() {
		return mMaximumPrice;
	}
	/**
	 * @param pMaximumPrice the maximumPrice to set
	 */
	public void setMaximumPrice(Double pMaximumPrice) {
		mMaximumPrice = pMaximumPrice;
	}
	/**
	 * @return the minimumPrice
	 */
	public Double getMinimumPrice() {
		return mMinimumPrice;
	}
	/**
	 * @param pMinimumPrice the minimumPrice to set
	 */
	public void setMinimumPrice(Double pMinimumPrice) {
		mMinimumPrice = pMinimumPrice;
	}
	/**
	 * @return the savedAmount
	 */
	public Double getSavedAmount() {
		return mSavedAmount;
	}
	/**
	 * @param pSavedAmount the savedAmount to set
	 */
	public void setSavedAmount(Double pSavedAmount) {
		mSavedAmount = pSavedAmount;
	}
	/**
	 * @return the savedPercentage
	 */
	public Double getSavedPercentage() {
		return mSavedPercentage;
	}
	/**
	 * @param pSavedPercentage the savedPercentage to set
	 */
	public void setSavedPercentage(Double pSavedPercentage) {
		mSavedPercentage = pSavedPercentage;
	}
}
