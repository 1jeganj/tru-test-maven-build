package com.tru.common;

/**
 * This class is Classification.
 * @author PA
 * @version 1.0
 */
public class Classification {
	/**
	 * This property hold reference for mCategoryId.
	 */
	private String mCategoryId;
	/**
	 * This property hold reference for mCategoryImage.
	 */
	private String mCategoryImage;
	/**
	 * This property hold reference for mCategoryName.
	 */
	private String mCategoryName;
	/**
	 * This property hold reference for mCategoryURL.
	 */
	private String mCategoryURL;
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}
	/**
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}
	/**
	 * @return the categoryImage
	 */
	public String getCategoryImage() {
		return mCategoryImage;
	}
	/**
	 * @param pCategoryImage the categoryImage to set
	 */
	public void setCategoryImage(String pCategoryImage) {
		mCategoryImage = pCategoryImage;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}
	/**
	 * @param pCategoryName the categoryName to set
	 */
	public void setCategoryName(String pCategoryName) {
		mCategoryName = pCategoryName;
	}
	/**
	 * @return the categoryURL
	 */
	public String getCategoryURL() {
		return mCategoryURL;
	}
	/**
	 * @param pCategoryURL the categoryURL to set
	 */
	public void setCategoryURL(String pCategoryURL) {
		mCategoryURL = pCategoryURL;
	}
}
