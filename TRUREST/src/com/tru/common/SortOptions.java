package com.tru.common;

/**
 * This class is SortOptions.
 * @author PA
 * @version 1.0
 */
public class SortOptions {
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mNavigationState;
	/**
	 * This property hold reference for Label.
	 */
	private String mLabel;
	/**
	 * This property hold reference for Selected.
	 */
	private Boolean mSelected;
	/**
	 * @return the selected
	 */
	public Boolean getSelected() {
		return mSelected;
	}
	/**
	 * @param pSelected the selected to set
	 */
	public void setSelected(Boolean pSelected) {
		mSelected = pSelected;
	}
	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}
	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return mLabel;
	}
	/**
	 * @param pLabel the label to set
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}
}
