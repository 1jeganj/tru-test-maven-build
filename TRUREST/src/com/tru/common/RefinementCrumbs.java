package com.tru.common;

import java.util.List;

/**
 * This class is RefinementCrumbs.
 * @author PA
 * @version 1.0
 */
public class RefinementCrumbs {
	/**
	 * This property hold reference for Ancestors.
	 */
	private List<Ancestors> mAncestors;
	/**
	 * This property hold reference for NavigationState.
	 */
	private String mNavigationState;
	/**
	 * This property hold reference for Label.
	 */
	private String mLabel;
	/**
	 * This property hold reference for CategoryId.
	 */
	private String mCategoryId;

	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return mCategoryId;
	}
	/**
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}
	/**
	 * @return the navigationState
	 */
	public String getNavigationState() {
		return mNavigationState;
	}
	/**
	 * @param pNavigationState the navigationState to set
	 */
	public void setNavigationState(String pNavigationState) {
		mNavigationState = pNavigationState;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return mLabel;
	}
	/**
	 * @param pLabel the label to set
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}

	/**
	 * @return the ancestors
	 */
	public List<Ancestors> getAncestors() {
		return mAncestors;
	}

	/**
	 * @param pAncestors the ancestors to set
	 */
	public void setAncestors(List<Ancestors> pAncestors) {
		mAncestors = pAncestors;
	}

	
	 	
}
