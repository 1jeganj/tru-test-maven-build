package com.tru.common;

import java.util.List;

/**
 * This class is SKURecords.
 * @author PA
 * @version 1.0
 */
public class SKURecords {
	/**
	 * This property hold reference for list of SKUDetail.
	 */
	private List<SKUDetail> mSku;

	/**
	 * @return the sku
	 */
	public List<SKUDetail> getSku() {
		return mSku;
	}

	/**
	 * @param pSku the sku to set
	 */
	public void setSku(List<SKUDetail> pSku) {
		mSku = pSku;
	}
	
}
