package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.tru.common.ParametricFeedCategoryProfileVO;
import com.tru.common.TRURestConfiguration;
import com.tru.rest.constants.TRUParametricFeedConstants;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUParametricFeedContentItemUtil;

/**
 * This class is TRUCategoryEndecaResponseDroplet which will get used from REST Actor.
 * This droplet will fetch the ENdeca response will be provided for Reports
 * @author PA
 * @version 1.0
 * @param <E>
 * 
 */
public class TRUCategoryEndecaResponseDroplet<E> extends DynamoServlet {

	/**
	 * This property hold reference for TRUEndecaParametricFeedContentItemUtil.
	 */
	private TRUParametricFeedContentItemUtil<E> mTRUParametricFeedContentItemUtil;
	/**
	 * This property hold reference for AssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;

	/**
	 * This property hold reference for TRURestConfiguration.
	 */

	private TRURestConfiguration mTRURestConfiguration;

	/**
	 * @return the assemblerTools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the assemblerTools to set
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the TRURestConfiguration
	 */
	public TRURestConfiguration getTRURestConfiguration() {
		return mTRURestConfiguration;
	}

	/**
	 * @param pTRURestConfiguration the TRURestConfiguration to set
	 */
	public void setTRURestConfiguration(TRURestConfiguration pTRURestConfiguration) {
		mTRURestConfiguration = pTRURestConfiguration;
	}

	/**
	 * Used to Render the Endeca Content Item based on input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUCategoryEndecaResponseDroplet.service method");
		}
		StringBuffer parametricXml = new StringBuffer();
		ContentItem contentItem = null;
		String pageName = null;
		ContentItem responseContentItem = null;
		ParametricFeedCategoryProfileVO modifiedContentItem = null;
		StringBuffer queryStringParam = new StringBuffer();

		String reportType = (String) pRequest.getLocalParameter(TRUParametricFeedConstants.REPORT_TYPE);
		if (isLoggingDebug()) {
			vlogDebug(" This is either category, subcategory or family request.  reportType :: {0}", reportType);
		}

		String categoryId = (String) pRequest.getLocalParameter(TRURestConstants.CATEGORYID);
		String profileId = (String) pRequest.getLocalParameter(TRUParametricFeedConstants.PROFILE_ID);

		if(StringUtils.isNotBlank(categoryId)) {
			if (isLoggingDebug()) {
				vlogDebug(" This is either category, subcategory or family request.  CategoryId is :: {0}", categoryId);
			}
			boolean isValidCategorySite = getTRUParametricFeedContentItemUtil().getCategorySiteList(categoryId);
			if (!isValidCategorySite) {
				if (isLoggingDebug()) {
					logDebug("Resulting into Error Message As data not available for the entered site id");
				}
				pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.ERR_ENTER_VALID_SITE);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			DimensionValueCacheObject dimCacheObject = getTRUParametricFeedContentItemUtil().getNavigationURLfromCategory(categoryId);
			if(dimCacheObject == null ) {
				if (isLoggingDebug()) {
					logDebug("Resulting into Error Message As incorrect parameter passed");
				}
				pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			String dimValId = dimCacheObject.getDimvalId();
			String tempURL = dimCacheObject.getUrl();
			if(StringUtils.isNotBlank(tempURL)) {
				pageName = tempURL.substring(TRURestConstants.INT_ZERO, tempURL.indexOf(TRURestConstants.QUERY));
			}
			queryStringParam.append(TRURestConstants.NAVIGATION_EQUAL).append(dimValId).append(TRURestConstants.AND_CATEGORY_EQUAL).append(dimValId);
			pRequest.setQueryString(queryStringParam.toString());
			if(StringUtils.isNotBlank(pageName)) {
				if(isLoggingDebug()) {
					vlogDebug("PageName is: {0}", pageName);
				}
				if(pageName.equals(getTRURestConfiguration().getCategoryPageURL())) {
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesCategory());
				} else if(pageName.equals(getTRURestConfiguration().getSubCategoryPageURL())) {
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesSubCategory());
				} else if(pageName.equals(getTRURestConfiguration().getFamilyPageURL())) {
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesFamily());
				}
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Resulting into Error Message As No required parameter passed");
			}
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		try {
			responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
		}
		catch (AssemblerException e) {
			vlogError(e, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] { contentItem });
		}

		if(pRequest.getParameter(TRURestConstants.ERROR_INFO) != null) {
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		
		if(responseContentItem != null) {
			if(StringUtils.isNotBlank(reportType)) {
				if(reportType.equalsIgnoreCase(TRUParametricFeedConstants.CSV)) {
					modifiedContentItem = getTRUParametricFeedContentItemUtil().constructCategoryCsvData(responseContentItem, categoryId);
				} else if(reportType.equalsIgnoreCase(TRUParametricFeedConstants.XML)) {
					modifiedContentItem = getTRUParametricFeedContentItemUtil().constructCategoryProfileData(responseContentItem, profileId, parametricXml, categoryId);
				}

				if (isLoggingDebug()) {
					vlogDebug(" This is either category, subcategory or family request.  modifiedContentItem :: {0}", modifiedContentItem);
				}
			}	
		} else {
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		
		if(modifiedContentItem == null) {
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		} 
		
		if(StringUtils.isNotEmpty(reportType) && reportType.equalsIgnoreCase(TRUParametricFeedConstants.CSV)) {
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem.getParametricProfileVO());
		} else if(StringUtils.isNotEmpty(reportType) && reportType.equalsIgnoreCase(TRUParametricFeedConstants.XML)) {
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem.getXmlProfile());
		}

		pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUCategoryEndecaResponseDroplet.service method");
		}
	}

	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}

	/**
	 * @return the tRUParametricFeedContentItemUtil
	 */
	public TRUParametricFeedContentItemUtil<E> getTRUParametricFeedContentItemUtil() {
		return mTRUParametricFeedContentItemUtil;
	}

	/**
	 * @param pTRUParametricFeedContentItemUtil the tRUParametricFeedContentItemUtil to set
	 */
	public void setTRUParametricFeedContentItemUtil(TRUParametricFeedContentItemUtil<E> pTRUParametricFeedContentItemUtil) {
		mTRUParametricFeedContentItemUtil = pTRUParametricFeedContentItemUtil;
	}

}
