package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.RegistryProductInfoVO;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUProductInfoUtil;

/**
 * This Class is TRURegistryProductInfoLookupDroplet which will be used for registry/wishList productInfo API.
 * @author PA
 * @version 1.0
 * @param <E> the type of elements in this collection
 * 
 */
public class TRURegistryProductInfoLookupDroplet<E> extends DynamoServlet {

	
	/**
	 * Holds the mCatalogTools
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Holds the pProductInfoUtil
	 */
	private TRUProductInfoUtil<E> mProductInfoUtil;
	
	
	/**
	 * Holds the mInventoryEnabled
	 */
	private boolean mInventoryEnabled;

	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRURegistryProductInfoLookupDroplet.service method.. ");
		}
		Map<String, String> inputMap = new HashMap<String, String>();
		List<RegistryProductInfoVO> prodcutInfoList = new ArrayList<RegistryProductInfoVO>();
		String allSku = (String) pRequest.getLocalParameter(TRURestConstants.ALL_SKUS);
		String skuDetail = (String) pRequest.getLocalParameter(TRURestConstants.SKU_DETAILS);
		String skuCode = (String) pRequest.getLocalParameter(TRURestConstants.SKU_CODE);
		String upcNumber=(String)pRequest.getLocalParameter(TRURestConstants.UPC_NUMBER);
		String onlinePIds = (String)pRequest.getLocalParameter(TRURestConstants.ONLINE_PID);
		if (StringUtils.isNotEmpty(allSku)) {
			inputMap.put(TRURestConstants.ALL_SKU_IDS, allSku);
		}
		if (StringUtils.isNotEmpty(skuDetail)) {
			inputMap.put(TRURestConstants.SKU_DETAIL, skuDetail);
		}
		if (StringUtils.isNotEmpty(skuCode)) {
			inputMap.put(TRURestConstants.SKU_CODES, skuCode);
		}
		if (StringUtils.isNotEmpty(upcNumber)) {
			inputMap.put(TRURestConstants.UPC_NUMBERS, upcNumber);
		}
		if(StringUtils.isNotEmpty(onlinePIds)){
			inputMap.put(TRURestConstants.ONLINE_PIDS, onlinePIds);
		}
		if (inputMap!=null && !inputMap.isEmpty()) {
			try {
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.startOperation(TRURestConstants.REGISTRY_START_SERVICE);
				}
				prodcutInfoList = getProductInfoUtil().populateProductInfo(inputMap, isInventoryEnabled());
				if (PerformanceMonitor.isEnabled()) {
					PerformanceMonitor.endOperation(TRURestConstants.REGISTRY_END_SERVICE);
				}
			} catch (RepositoryException repositoryException) {
					if (isLoggingError()) {
						logError("RepositoryException in TRURegistryProductInfoLookupDroplet.::@method::service:",repositoryException);
					}
			} catch (SiteContextException siteContextException) {
					if (isLoggingError()) {
						logError("SiteContextException in TRURegistryProductInfoLookupDroplet.::@method::service",siteContextException);
					}
			} catch (PriceListException priceListException) {
				if (isLoggingError()) {
					logError("PriceListException in TRURegistryProductInfoLookupDroplet.::@method::service",priceListException);	
				}	
				}
			}
		if (prodcutInfoList != null && !prodcutInfoList.isEmpty()) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, prodcutInfoList);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM_REGISTRY, pRequest, pResponse);	
		} else {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);	
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRURegistryProductInfoLookupDroplet.");
		}
	}

	
	/**
	 * @return the mCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	/**
	 * @return the mProductInfoUtil
	 */
	public TRUProductInfoUtil<E> getProductInfoUtil() {
		return mProductInfoUtil;
	}

	/**
	 * @param pProductInfoUtil the mProductInfoUtil to set
	 */
	public void setProductInfoUtil(TRUProductInfoUtil<E> pProductInfoUtil) {
		this.mProductInfoUtil = pProductInfoUtil;
	}

	/**
	 * @return the mInventoryEnabled
	 */
	public boolean isInventoryEnabled() {
		return mInventoryEnabled;
	}

	/**
	 * @param pInventoryEnabled the mInventoryEnabled to set
	 */
	public void setInventoryEnabled(boolean pInventoryEnabled) {
		this.mInventoryEnabled = pInventoryEnabled;
	}

	
}
