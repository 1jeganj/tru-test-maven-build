package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.catalog.vo.BreadcrumbInfoVO;
import com.tru.common.ParentCategoryInfo;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUProductInfoUtil;

/**
 * This Class is TRUParentCategoryLookupDroplet which will be used for registry/wishList parentCategoryId API.
 * @author PA
 * @version 1.0
 * @param <E> the type of elements in this collection
 * 
 */
public class TRUParentCategoryLookupDroplet<E> extends DynamoServlet {
	/**
	 * Holds the pProductInfoUtil
	 */
	private TRUProductInfoUtil<E> mProductInfoUtil;

	/**
	 * Get bulk parentCategoryId
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUParentCategoryLookupDroplet.service method.. ");
		}
		List<String> skuIdList = null;
		Map<String, String> skuAndOnlineIdMap = null;
		String[] skuCodeArray = null;
		List<BreadcrumbInfoVO> ancestorsList = new ArrayList<BreadcrumbInfoVO>();
		Map<String, String> breadcrumbMap = new HashMap<String, String>();
		Map<String, String> categoryIdMap = new HashMap<String, String>();
		ParentCategoryInfo categoryInfo = new ParentCategoryInfo();
		String skuCode = (String) pRequest.getLocalParameter(TRURestConstants.SKU_CODE);
		if (StringUtils.isNotEmpty(skuCode)) {
			skuIdList = getProductInfoUtil().getCatalogManager().getCatalogTools().spliteProductId(skuCode);
			 skuCodeArray = skuIdList.toArray(new String[skuIdList.size()]);
			try {
				skuAndOnlineIdMap = getProductInfoUtil().getCatalogManager().getCatalogTools().findSkus(skuCodeArray);
				if (skuAndOnlineIdMap != null && !skuAndOnlineIdMap.isEmpty()) {
					for (Entry<String, String> entry : skuAndOnlineIdMap.entrySet()) {
						pRequest.setQueryString(TRURestConstants.PRODUCTID_EQUAL + entry.getValue());
						pRequest.setParameter(TRURestConstants.RECORD_SPEC,getProductInfoUtil().getCatalogManager().getCatalogTools().createPDPUrlFromOnlinePid(entry.getValue()));
						breadcrumbMap = getProductInfoUtil().populateBreadcrumbDetails(ancestorsList, pRequest);
						if (breadcrumbMap != null && !breadcrumbMap.isEmpty()) {
							categoryIdMap.put(entry.getKey(), breadcrumbMap.get(TRURestConstants.PARENT_CATEGORY_ID));
						}
					}
					categoryInfo.setParentCategoryIdMap(categoryIdMap);
				}
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUParentCategoryLookupDroplet.service method ", repositoryException);
				}
			}
			if (categoryInfo != null) {
				pRequest.setParameter(TRURestConstants.PRODUCT_INFO, categoryInfo);
				pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);	
			}
		}
		if (categoryInfo == null || StringUtils.isEmpty(skuCode)) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);	
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUParentCategoryLookupDroplet:::service method");
		}
	}

	/**
	 * @return the mProductInfoUtil
	 */
	public TRUProductInfoUtil<E> getProductInfoUtil() {
		return mProductInfoUtil;
	}

	/**
	 * @param pProductInfoUtil
	 *            the mProductInfoUtil to set
	 */
	public void setProductInfoUtil(TRUProductInfoUtil<E> pProductInfoUtil) {
		this.mProductInfoUtil = pProductInfoUtil;
	}

}
