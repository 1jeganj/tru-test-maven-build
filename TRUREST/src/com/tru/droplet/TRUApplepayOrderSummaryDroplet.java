package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.CommerceItemManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.ApplePayOrderInfo;
import com.tru.util.TRUApplePayUtils;

/**
 * This droplet is used to display Applepay Order Summary .
 *
 * @author PA
 * @version 1.0
 */
public class TRUApplepayOrderSummaryDroplet extends DynamoServlet {

	/** Property To Hold commerce item manager. */
	private CommerceItemManager mCommerceItemManager;
	/**
	 * Holds the mApplePayUtils
	 */
	private TRUApplePayUtils mApplePayUtils;
	/**
	 * @return the mApplePayUtils
	 */
	public TRUApplePayUtils getApplePayUtils() {
		return mApplePayUtils;
	}

	/**
	 * @param pApplePayUtils
	 *            the ApplePayUtils to set
	 */
	public void setApplePayUtils(TRUApplePayUtils pApplePayUtils) {
		mApplePayUtils = pApplePayUtils;
	}
	
	
	
	
	/**
	 * Sets the commerce item manager.
	 * 
	 * @param pCommerceItemManager
	 *            the new commerce item manager.
	 */
	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

	/**
	 * Gets the commerce item manager.
	 * 
	 * @return the commerce item manager.
	 */
	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	
	
	/**
	 * Service.
	 *
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUApplepayOrderSummaryDroplet.service :: START");
		}
		TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);
		ApplePayOrderInfo applePayOrderInfo=new ApplePayOrderInfo();
		applePayOrderInfo=	getApplePayUtils().populateApplePayOrderInfoVO(order, applePayOrderInfo);
		pRequest.setParameter(TRUCheckoutConstants.APPLE_PAY_ORDER_INFO, applePayOrderInfo);
		pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("TRUApplepayOrderSummaryDroplet.service :: END");
		}
	}
}