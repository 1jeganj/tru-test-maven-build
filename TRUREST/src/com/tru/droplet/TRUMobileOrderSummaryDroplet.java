package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.common.Product;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This Class is TRUMobileOrderSummaryDroplet which will get order summary.
 * @author PA
 * @version 1.0
 */
public class TRUMobileOrderSummaryDroplet extends DynamoServlet {

	/**
	 * Holds the mSiteUtil
	 */
	private TRUGetSiteTypeUtil mSiteUtil;
	/**
	 * Holds the mCatalogTool
	 */
	private TRUCatalogTools mCatalogTool;
	
	/** The m product page util. */
	private TRUProductPageUtil mProductPageUtil;
	/**
	 * Used to Render the JSON Response for mobile.
	 * 
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *            - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileOrderSummaryDroplet.service method.. ");
		}
		Order order = (Order) pRequest.getObjectParameter(TRURestConstants.ORDER);
		if (order == null) {
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		} else {
			TRUCommerceItemImpl commerceItemImpl;
			String productId = null;
			String onlinePID = null;
			String skuId = null;
			List<Product> itemURL = new ArrayList<Product>();
			List<TRUCommerceItemImpl> commerceItems = order.getCommerceItems();
			int commerceItemsSize = TRURestConstants.INT_ZERO;
			if (commerceItems != null && !commerceItems.isEmpty()) {
				commerceItemsSize = commerceItems.size();
				for (int count = TRURestConstants.INT_ZERO; count < commerceItemsSize; count++) {
					Product product = new Product();
					commerceItemImpl = (TRUCommerceItemImpl) commerceItems.get(count);
					productId = commerceItemImpl.getAuxiliaryData().getProductId();
					skuId=commerceItemImpl.getCatalogRefId();
					onlinePID = getCatalogTool().getOnlinePIDFromSKUId(skuId);
					Site site = SiteContextManager.getCurrentSite();
					String pdpUrl = getPDPURL(onlinePID, site);
					if (StringUtils.isNotBlank(pdpUrl)) {
						product.setUrl(pdpUrl);
					}
					product.setOnlinePID(productId);
					itemURL.add(product);
				}
				pRequest.setParameter(TRURestConstants.ORDER, order);
				pRequest.setParameter(TRURestConstants.PDP_URL, itemURL);
				pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
			} else {
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileOrderSummaryDroplet.");
		}
	}
	/**
	 * This method will return the PDP page URL based on the productId and siteId.
	 * @param pProductId productId
	 * @param pSiteId siteId
	 * @return string
	 */
	protected String getPDPURL(String pProductId, Site pSiteId) {
		if (isLoggingDebug()) {
			logDebug("@Class::TRUMobileOrderSummaryDroplet::@method::getPDPURL() : START");
		}
		String productPageUrl = null;
		if (!StringUtils.isEmpty(pProductId) && pSiteId != null) {
			if (StringUtils.isNotEmpty(pSiteId.toString())) {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId, pSiteId.toString());
				return productPageUrl;
			} else {
				productPageUrl = getProductPageUtil().getProductPageURL(pProductId);
				return productPageUrl;
			}
		}
		if (isLoggingDebug()) {
			logDebug("@Class::TRUMobileOrderSummaryDroplet::@method::getPDPURL() : END");
		}
		return productPageUrl;
	}
	/**
	 * @return the mSiteUtil
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}

	/**
	 * @param pSiteUtil the mSiteUtil to set
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		this.mSiteUtil = pSiteUtil;
	}

	/**
	 * @return the mCatalogTool
	 */
	public TRUCatalogTools getCatalogTool() {
		return mCatalogTool;
	}

	/**
	 * @param pCatalogTool the mCatalogTool to set
	 */
	public void setCatalogTool(TRUCatalogTools pCatalogTool) {
		this.mCatalogTool = pCatalogTool;
	}
	/**
	 * @return the mProductPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}
	/**
	 * @param pProductPageUtil the mProductPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}
	
}
