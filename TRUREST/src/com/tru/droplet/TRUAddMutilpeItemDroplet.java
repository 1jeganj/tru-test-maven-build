package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.rest.constants.TRURestConstants;

/**
 * This TRUAddMutilpeItemDroplet set multiple prodcutId and and skuId in addItem.
 * @author Professional Access
 * @version  : 1.0
 * 
 */
public class TRUAddMutilpeItemDroplet extends DynamoServlet{
	
	/**
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUAddMutilpeItemDroplet.service method");	
		}
		AddCommerceItemInfo items = null;
		AddCommerceItemInfo [] itemArray = null;
		List<AddCommerceItemInfo> itemsList = null;
		List<TRUCartItemDetailVO> collectionItemInfoList = (List<TRUCartItemDetailVO>) pRequest.getObjectParameter(TRURestConstants.COLLECTION_INPUT);
		if (!collectionItemInfoList.isEmpty()) {
			if (isLoggingDebug()) {
				logDebug("CollectionItemInfoList is not empty");	
			}
			itemsList = new ArrayList<AddCommerceItemInfo>();
			for (TRUCartItemDetailVO itemVO : collectionItemInfoList) {
				items = new AddCommerceItemInfo();
				items.setCatalogRefId(itemVO.getSkuId());
				items.setProductId(itemVO.getProductId());
				items.setQuantity(itemVO.getQuantity());
				items.setLocationId(itemVO.getLocationId());
				itemsList.add(items);
			}
		}else{
			if (isLoggingDebug()) {
				logDebug("Input parameter is not passed generating Empty");	
			}
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);
			return;
		}
		if (itemsList!=null && !itemsList.isEmpty()) {
			itemArray = itemsList.toArray(new AddCommerceItemInfo[itemsList.size()]);
			pRequest.setParameter(TRURestConstants.ITEM_ARRAY, itemArray);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}else{
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("End:: into the TRUAddMutilpeItemDroplet.service method");
		}
	}
}
