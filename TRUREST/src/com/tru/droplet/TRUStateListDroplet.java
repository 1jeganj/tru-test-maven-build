package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.util.StateListDroplet;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.tru.rest.constants.TRURestConstants;

/**
 * TRUStateListDroplet extends StateListDroplet and performs 
 * country  code validation and then invoke super class to 
 * get state info for valid country code passed as input
 * parameter.  
 * 
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUStateListDroplet extends StateListDroplet{
	
	
	
	/**
	 * Used to get country  code validation and then invoke super class to get the state info for valid country code passed as input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUStateListDroplet.service method..");
		}
		String countryCode = (String)pRequest.getObjectParameter(COUNTRY_CODE);
		if (StringUtils.isBlank(countryCode)) {
			if (isLoggingDebug()) {
				logDebug("Country Code is been passed as empty");
			}
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}else{
			if (isLoggingDebug()) {
				logDebug("Country Code is not empty");
			}
			if(TRURestConstants.US_COUNTRY_CODE.equalsIgnoreCase(countryCode)){
				if (isLoggingDebug()) {
					logDebug("Country Code is US invoking StateListDroplet.service ");
				}
				super.service(pRequest, pResponse);
			}else{
				if (isLoggingDebug()) {
					logDebug("Un Supported Country Code");
				}
				pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.UNSUPPORTED_COUNTRY);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUStateListDroplet.service method..");
		}

	}
	

	
}
