package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.rest.constants.TRURestConstants;

/**
 * This Class is TRUMobileRemoveItemDroplet which will remove the shopping cart item.
 * @author PA
 * @version 1.0
 * 
 */
public class TRUMobileRemoveItemDroplet extends DynamoServlet {
	
	 private String mRelationShipId;
	  /**
	 * This method is used to set next tab in order for rest call.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUMobileDetermineRemoveItemDroplet.service method");
		}
		String removeId = (String) pRequest.getLocalParameter(TRURestConstants.RELATIONSHIP_ID);
		if (StringUtils.isNotBlank(removeId)) {
			if (removeId.startsWith(getRelationShipId())) {
				pRequest.serviceLocalParameter(TRURestConstants.TRUE, pRequest, pResponse);		
			} else {
				pRequest.serviceLocalParameter(TRURestConstants.FALSE, pRequest, pResponse);
			}
		} else {
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUMobileDetermineRemoveItemDroplet.service method");
		}
	}
	/**
	 * @return the mRelationShipId
	 */
	public String getRelationShipId() {
		return mRelationShipId;
	}
	/**
	 * @param pRelationShipId the RelationShipId to set
	 */
	public void setRelationShipId(String pRelationShipId) {
		this.mRelationShipId = pRelationShipId;
	}

}
