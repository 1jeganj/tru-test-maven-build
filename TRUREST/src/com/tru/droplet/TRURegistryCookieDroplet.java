package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is intended for creating the Registry cookie. parameters to be
 * passed by user, 'registry_id' is a required parameter.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>registry_id</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>REGISTRY_USER</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/droplet/RegistryCookie"&gt;<br>
 *          &lt;dspel:param name="registry_id" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 */
public class TRURegistryCookieDroplet extends DynamoServlet {

	/**
	 * Constant to hold HYPHEN_SYMBOL.
	 */
	public static final String PATH = "/";

	/**
	 * This method is overridden to check if cookie has no registry_id then add
	 * registry_id to the cookie for REST Web Services.
	 * 
	 * @param pReq
	 *            - DynamoHttpServletRequest
	 * @param pRes
	 *            - DynamoHttpServletResponse
	 * @throws IOException
	 *             -
	 * @throws ServletException
	 *             -
	 */
	public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes) throws ServletException,	IOException {
		boolean retrieveCookies = false;
		String registryId = (String) pReq.getLocalParameter(TRURestConstants.REGISTRY_ID);
		String responseStatus = (String) pReq.getLocalParameter(TRURestConstants.RESPONSE_STATUS);
		String lastAccessedRegistryID = (String) pReq.getLocalParameter(TRURestConstants.LAST_ACCESS_REGISTRY_ID);
		String lastAccessedRegistryName = (String) pReq.getLocalParameter(TRURestConstants.LAST_ACCESS_REGISTRY_NAME);
		String myWishlistID = (String) pReq.getLocalParameter(TRURestConstants.MY_WISHLIST_ID);
		String lastAccessedWishlistID = (String) pReq.getLocalParameter(TRURestConstants.LAST_ACCESS_WISHLIST_ID);
		String lastAccessedWishlistName = (String) pReq.getLocalParameter(TRURestConstants.LAST_ACCESS_WISHLIST_NAME);
		String retrieveRegistryCookie=(String)pReq.getLocalParameter(TRURestConstants.RETRIEVE_REGISTRY_COOKIE);
		if (StringUtils.isNotBlank(retrieveRegistryCookie) && retrieveRegistryCookie.equals(TRURestConstants.TRUE)) {
			retrieveCookies = true;
		}
		String domainName = (String)pReq.getServerName();
		String domainNamePrefix=null;
		if (StringUtils.isNotBlank(domainName))
		{
         domainNamePrefix = domainName.substring(domainName.indexOf(TRUConstants.DOT), domainName.length());
		}
		Cookie newcookie;
		if (isLoggingDebug()) {
			logDebug("BEGIN:: RegistryCookie.service method..");
		}
		if(pReq.getCookies() == null){
			newcookie = new Cookie(TRURestConstants.REGISTRY_USER,registryId);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.RESPONSE_STATUS,responseStatus);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_REGISTRY_ID,lastAccessedRegistryID);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_REGISTRY_NAME,lastAccessedRegistryName);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.MY_WISHLIST_ID,myWishlistID);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_WISHLIST_ID,lastAccessedWishlistID);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_WISHLIST_NAME,lastAccessedWishlistName);
			newcookie.setPath(PATH);
			newcookie.setDomain(domainNamePrefix);
			pRes.addCookie(newcookie);
		}else{
			setRegistryDetails(pReq, pRes, registryId,lastAccessedRegistryName,responseStatus,lastAccessedRegistryID,myWishlistID,lastAccessedWishlistID,lastAccessedWishlistName,domainNamePrefix);
		}
		if (retrieveCookies && pReq.getCookies() != null) {
			for (Cookie cookie : pReq.getCookies()) {
				if (cookie.getName().equals(TRURestConstants.REGISTRY_USER)) {
					String registryIdFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.REGISTRY_ID, registryIdFromCookie);
				} else if (cookie.getName().equals(TRURestConstants.RESPONSE_STATUS)) {
					String responseStatusFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.RESPONSE_STATUS, responseStatusFromCookie);
				} else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_REGISTRY_ID)) {
					String lastAccessedRegistryIDFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.LAST_ACCESS_REGISTRY_ID,lastAccessedRegistryIDFromCookie);
				}
				else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_REGISTRY_NAME)) {
					String lastAccessedRegistryNameFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.LAST_ACCESS_REGISTRY_NAME,lastAccessedRegistryNameFromCookie);
				}
				else if (cookie.getName().equals(TRURestConstants.MY_WISHLIST_ID)) {
					String myWishListIDFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.WISHLIST_ID,myWishListIDFromCookie);
				}
				else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_WISHLIST_ID)) {
					String lastAccessedWishListIDFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.LAST_ACCESS_WISHLIST_ID,lastAccessedWishListIDFromCookie);
				}
				else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_WISHLIST_NAME)) {
					String lastAccessedWishListNameFromCookie = (String) cookie.getValue();
					pReq.setParameter(TRURestConstants.LAST_ACCESS_WISHLIST_NAME,lastAccessedWishListNameFromCookie);
				}
			  }
		}
		pReq.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pReq, pRes);
		if (isLoggingDebug()) {
			logDebug("END:: RegistryCookie.service method..");
		}
	}
	/**
	 * This method will set Registry  details.
	 * 
	 * 
	 * @param pReq -  Request
	 * @param pRes - Response
	 * @param pRegistryId - RegistryId 
	 * @param pResponseStatus - ResponseStatus
	 * @param pLastAccessedRegistryID - LastAccessedRegistryID
	 * @param pMyWishlistID - MyWishlistID     
	 * @param pLastAccessedWishlistID - LastAccessedWishlistID
	 * @param pLastAccessedRegistryName - LastAccessedRegistryName
	 * @param pLastAccessedWishlistName - LastAccessedWishlistName
	 * @param pDomainNamePrefix - DomainNamePrefix
	 */
	private void setRegistryDetails(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes, String pRegistryId,String pLastAccessedRegistryName,String pResponseStatus, String pLastAccessedRegistryID,String pMyWishlistID, String pLastAccessedWishlistID,String pLastAccessedWishlistName,String pDomainNamePrefix) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: RegistryCookie.setRegistryDetails method..");
		}
		boolean isCreateRegistryUserCookie=true;
		boolean isCreateResponseStatusCookie=true;
		boolean isCreateLastAccessRegistryIdCookie=true;
		boolean isCreateWishlistUserCookie=true;
		boolean isCreateLastAccessWishlistIdCookie=true;
		boolean isCreateLastAccessRegistryNameCookie=true;
		boolean isCreateLastAccessWishlistNameCookie=true;
		Cookie newcookie;
		for (Cookie cookie : pReq.getCookies()) {
			if (cookie.getName().equals(TRURestConstants.REGISTRY_USER)	&& StringUtils.isNotBlank(pRegistryId)) {
				cookie.setValue(pRegistryId);
				cookie.setPath(PATH);
				cookie.setDomain(pDomainNamePrefix);
				pRes.addCookie(cookie);
				pReq.setParameter(TRURestConstants.REGISTRY_ID, pRegistryId);
				isCreateRegistryUserCookie=false;
			} else if (cookie.getName().equals(TRURestConstants.RESPONSE_STATUS) && StringUtils.isNotBlank(pResponseStatus)) {
					cookie.setValue(pResponseStatus);
					pRes.addCookie(cookie);
					cookie.setPath(PATH);
					cookie.setDomain(pDomainNamePrefix);
					pReq.setParameter(TRURestConstants.RESPONSE_STATUS, pResponseStatus);
					isCreateResponseStatusCookie=false;					
			} else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_REGISTRY_ID) && StringUtils.isNotBlank(pLastAccessedRegistryID)) {
					cookie.setValue(pLastAccessedRegistryID);
					pRes.addCookie(cookie);
					cookie.setPath(PATH);
					cookie.setDomain(pDomainNamePrefix);
					pReq.setParameter(TRURestConstants.LAST_ACCESS_REGISTRY_ID,pLastAccessedRegistryID);
					isCreateLastAccessRegistryIdCookie=false;
			} else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_REGISTRY_NAME) && StringUtils.isNotBlank(pLastAccessedRegistryName)) {
					cookie.setValue(pLastAccessedRegistryName);
					pRes.addCookie(cookie);
					cookie.setPath(PATH);
					cookie.setDomain(pDomainNamePrefix);
					pReq.setParameter(TRURestConstants.LAST_ACCESS_REGISTRY_NAME,pLastAccessedRegistryName);
					isCreateLastAccessRegistryNameCookie=false;
			}
			else if (cookie.getName().equals(TRURestConstants.MY_WISHLIST_ID) && StringUtils.isNotBlank(pMyWishlistID)) {
				cookie.setValue(pMyWishlistID);
				pRes.addCookie(cookie);
				cookie.setPath(PATH);
				cookie.setDomain(pDomainNamePrefix);
				pReq.setParameter(TRURestConstants.MY_WISHLIST_ID,pMyWishlistID);
				isCreateWishlistUserCookie=false;
			}
			else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_WISHLIST_ID) && StringUtils.isNotBlank(pLastAccessedWishlistID)) {
				cookie.setValue(pLastAccessedWishlistID);
				pRes.addCookie(cookie);
				cookie.setPath(PATH);
				cookie.setDomain(pDomainNamePrefix);
				pReq.setParameter(TRURestConstants.LAST_ACCESS_WISHLIST_ID,pLastAccessedWishlistID);
				isCreateLastAccessWishlistIdCookie=false;
			}
			else if (cookie.getName().equals(TRURestConstants.LAST_ACCESS_WISHLIST_NAME) && StringUtils.isNotBlank(pLastAccessedWishlistName)) {
				cookie.setValue(pLastAccessedWishlistName);
				pRes.addCookie(cookie);
				cookie.setPath(PATH);
				cookie.setDomain(pDomainNamePrefix);
				pReq.setParameter(TRURestConstants.LAST_ACCESS_WISHLIST_NAME,pLastAccessedWishlistName);
				isCreateLastAccessWishlistNameCookie=false;
			}
		}
		if(isCreateRegistryUserCookie && StringUtils.isNotBlank(pRegistryId)){
			newcookie = new Cookie(TRURestConstants.REGISTRY_USER,pRegistryId);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateResponseStatusCookie && StringUtils.isNotBlank(pResponseStatus)){
			newcookie = new Cookie(TRURestConstants.RESPONSE_STATUS,pResponseStatus);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateLastAccessRegistryIdCookie && StringUtils.isNotBlank(pLastAccessedRegistryID)){
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_REGISTRY_ID,pLastAccessedRegistryID);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateLastAccessRegistryNameCookie && StringUtils.isNotBlank(pLastAccessedRegistryName)){
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_REGISTRY_NAME,pLastAccessedRegistryName);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateWishlistUserCookie && StringUtils.isNotBlank(pMyWishlistID)){
			newcookie = new Cookie(TRURestConstants.MY_WISHLIST_ID,pMyWishlistID);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateLastAccessWishlistIdCookie && StringUtils.isNotBlank(pLastAccessedWishlistID)){
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_WISHLIST_ID,pLastAccessedWishlistID);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateLastAccessWishlistNameCookie && StringUtils.isNotBlank(pLastAccessedWishlistName)){
			newcookie = new Cookie(TRURestConstants.LAST_ACCESS_WISHLIST_NAME,pLastAccessedWishlistName);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if (isLoggingDebug()) {
			logDebug("END:: RegistryCookie.setRegistryDetails method..");
		}
	}

}
