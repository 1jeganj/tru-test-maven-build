package com.tru.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;
import com.tru.common.promotion.PromotionStatus;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUPromotionUtils;

/**
 * This Class is TRUMobileFilterSucnCouponDroplet which will filter sucn coupons.
 * @author PA
 * @version 1.0
 * 
 */
public class TRUMobileFilterSucnCouponDroplet extends DynamoServlet {
	
	/**property to hold mPromotionUtils */
	private TRUPromotionUtils mPromotionUtils;
	
	
	/**
	 * @return the promotionUtils
	 */
	public TRUPromotionUtils getPromotionUtils() {
		return mPromotionUtils;
	}


	/**
	 * @param pPromotionUtils the promotionUtils to set
	 */
	public void setPromotionUtils(TRUPromotionUtils pPromotionUtils) {
		mPromotionUtils = pPromotionUtils;
	}


	/**
	 * This method is used to set next tab in order for rest call.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUMobileFilterSucnCoupenDroplet.service method");
		}
		TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(TRURestConstants.ORDER);
		Profile profile =(Profile) pRequest.getObjectParameter(TRUConstants.PROFILE);
		boolean isAppliedCoupon =  Boolean.parseBoolean((String) pRequest.getLocalParameter(TRURestConstants.APPLIED_COUPON));
		boolean isOrderConfirm =  Boolean.parseBoolean((String) pRequest.getLocalParameter(TRURestConstants.PAGE_NAME_CONFIRM));
		if (order == null) {
			if (isLoggingDebug()) {
				logDebug("Profile is:"+profile+", order is:"+order);
			}	
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		if (isOrderConfirm) {
			List<PromotionStatus> filteredActivePromotions=getPromotionUtils().fetchLastOrderPromotions(order);
			pRequest.setParameter(TRURestConstants.ACTIVE_PROMOTIONS, filteredActivePromotions);	
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
			return;

		} else {
			List<PromotionStatus> filteredActivePromotions=getPromotionUtils().getAppliedPromotions(profile, order,isAppliedCoupon);
			pRequest.setParameter(TRURestConstants.ACTIVE_PROMOTIONS, filteredActivePromotions);	
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUMobileFilterSucnCoupenDroplet.service method");
		}	
	}

}
