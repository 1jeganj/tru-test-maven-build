package com.tru.droplet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.order.ShippingGroup;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.purchase.TRUShippingGroupContainerService;
import com.tru.rest.constants.TRURestConstants;

/**
 * This credit card fetches all available credit card map from the profile and also the default credit card.
 * @author PA
 * @version 1.0
 */
public class TRUAvailableBillingAddressesDroplet extends DynamoServlet {
	
	/**
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUAvailableBillingAddressesDroplet.service :: START");
		}
		TRUShippingGroupContainerService shippingGroupContainer = (TRUShippingGroupContainerService) pRequest.getObjectParameter(TRURestConstants.SHIPPING_GROUP_CONTAINER);
		TRUOrderImpl order = (TRUOrderImpl) pRequest.getObjectParameter(TRUCheckoutConstants.ORDER);
		boolean  onlyOrderBillingAddress = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRURestConstants.ONLY_ORDER_BILLING_ADDRESS));
		if (order == null) {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
			return;
		}
		Map<String, Address> billingAddressMap = null;
		if(onlyOrderBillingAddress){
			if(order.getBillingAddress()!=null && StringUtils.isNotBlank(order.getBillingAddressNickName())){
				pRequest.setParameter(TRURestConstants.BILLING_ADDR_MAP, order.getBillingAddress());
				pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
			}else{
				pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
			}
			return;
		}else{
			if (shippingGroupContainer == null) {
				pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
				return;
			}
			final Map shippingGroupMapForDisplay = shippingGroupContainer.getShippingGroupMapForDisplay();
			if(shippingGroupMapForDisplay==null){
				pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
				return;
			}
			final Set<String> shippingGroupKeys = shippingGroupMapForDisplay.keySet();
			billingAddressMap=new LinkedHashMap<String, Address>();
			for (String shippingGroupKey : shippingGroupKeys) {
				final ShippingGroup shippingGroup = (ShippingGroup) shippingGroupMapForDisplay.get(shippingGroupKey);
				if (shippingGroup instanceof TRUHardgoodShippingGroup) {
					final TRUHardgoodShippingGroup hardgoodShippingGroup = (TRUHardgoodShippingGroup) shippingGroup;
					if(hardgoodShippingGroup!=null){
					if(StringUtils.isNotEmpty(shippingGroupKey) && shippingGroupKey.equalsIgnoreCase(order.getBillingAddressNickName())) {
							billingAddressMap.put(shippingGroupKey,hardgoodShippingGroup.getShippingAddress());
						}else{
							boolean billing=(Boolean)hardgoodShippingGroup.isBillingAddress();
								if(billing){
									billingAddressMap.put(shippingGroupKey,hardgoodShippingGroup.getShippingAddress());
								}
								
							}
					}
				}
			}
		if(StringUtils.isNotBlank(order.getBillingAddressNickName()) && order.getBillingAddress()!=null && billingAddressMap.get(order.getBillingAddressNickName())==null ){
				billingAddressMap.put(order.getBillingAddressNickName(),order.getBillingAddress());
			}
			pRequest.setParameter(TRURestConstants.BILLING_ADDR_NKNAME, order.getBillingAddressNickName());
		}
		pRequest.setParameter(TRURestConstants.BILLING_ADDR_MAP, billingAddressMap);
		if (billingAddressMap.isEmpty()) {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_EMPTY, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCheckoutConstants.OPARAM_OUTPUT, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUAvailableBillingAddressesDroplet.service :: END");
		}
	}
}
