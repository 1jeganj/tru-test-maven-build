package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.cart.vo.TRUCartItemDetailVO;
import com.tru.commerce.catalog.vo.PrimaryPathCategoryVO;
import com.tru.commerce.order.vo.TRUShipmentVO;
import com.tru.commerce.order.vo.TRUShippingDestinationVO;
import com.tru.endeca.category.primarypath.TRUCategoryPrimaryPathManager;
import com.tru.rest.constants.TRURestConstants;


/**
 * This droplet is used to render Category details for Sku.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUGetCategoryDetailsDroplet extends DynamoServlet {

	/**
	 * Property to hold TRUCatalogManager.
	 */
	private TRUCategoryPrimaryPathManager mPrimaryPathManager;


	/**
	 * This method will get the skuId from the JSP as a parameter and it will retun the Category details for the perticular skus.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUGetCategoryDetailsDroplet.service method..");
		}
		boolean cartPage = Boolean.parseBoolean((String) pRequest.getLocalParameter(TRURestConstants.CART_PAGE));
		if(cartPage){
			List<TRUCartItemDetailVO> cartItem= (List<TRUCartItemDetailVO>)pRequest.getObjectParameter(TRURestConstants.CARTITEM_DETAILS);
			if(cartItem==null || cartItem.isEmpty()){
				pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse); 
				return;	
			}
			setCategoryDetails(cartItem);
			pRequest.setParameter(TRURestConstants.CARTITEM_DETAILS, cartItem);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		Map<String, TRUShippingDestinationVO> shippingDestinationVOs=null;
		shippingDestinationVOs= (Map<String, TRUShippingDestinationVO>) pRequest.getObjectParameter(TRURestConstants.SHIPPING_DESTINATIONS);
		if(shippingDestinationVOs==null){
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse); 
			return;	
		}
		//iterate over map 
		for (TRUShippingDestinationVO shippingDestinationVO :shippingDestinationVOs.values() ) {
			if(shippingDestinationVO.getShipmentVOMap()==null){
				continue;
			}
			for (TRUShipmentVO shipmentVO :shippingDestinationVO.getShipmentVOMap().values() ) {
				if(shipmentVO==null ||  shipmentVO.getCartItemDetailVOs()==null){
					continue;
				}
				
				setCategoryDetails(shipmentVO.getCartItemDetailVOs());
				
				
			}
		}
		if(shippingDestinationVOs != null && !shippingDestinationVOs.isEmpty()){
			pRequest.setParameter(TRURestConstants.SHIPPING_DESTINATIONS, shippingDestinationVOs);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}

		if (isLoggingDebug()) {
			logDebug("END:: TRUGetCategoryDetailsDroplet.service method.. ");
		}
	}
	/**
	 * This method will get the skuId from the JSP as a parameter and it will retun the Category details for the perticular skus.
	 * 
	 * @param pCartItemDetailVOs - pCartItemDetailVOs.
	 */

	private void setCategoryDetails(List<TRUCartItemDetailVO> pCartItemDetailVOs) {
		List<PrimaryPathCategoryVO> primaryPathCategoryVOList = null;
		
		for (TRUCartItemDetailVO cartItemDetailVO :pCartItemDetailVOs ) {
			if(cartItemDetailVO == null || StringUtils.isBlank(cartItemDetailVO.getSkuId())){
				continue;
			}
			primaryPathCategoryVOList= getPrimaryPathManager().populatePrimaryPathVOList(cartItemDetailVO.getSkuId());
			String productCategoryName =  TRUCommerceConstants.EMPTY_SPACE;
			List subcatList=new ArrayList<String>();
			if(primaryPathCategoryVOList!=null && ! primaryPathCategoryVOList.isEmpty()){
				productCategoryName=primaryPathCategoryVOList.get(0).getCategoryName();
				for (int j = TRUCommerceConstants.INT_ONE; j < primaryPathCategoryVOList.size()-1; ++j) {
					if(StringUtils.isNotBlank(primaryPathCategoryVOList.get(j).getCategoryName())){
						subcatList.add(primaryPathCategoryVOList.get(j).getCategoryName());
					}
				}
			}
			if (StringUtils.isNotBlank(productCategoryName)) {
				cartItemDetailVO.setCategoryName(productCategoryName);
			}
			if (!subcatList.isEmpty()) {
				cartItemDetailVO.setSubCategoryNames(subcatList);
			}
			
		}
		
	}


	/**
	 * @return mPrimaryPathManager
	 */
	public TRUCategoryPrimaryPathManager getPrimaryPathManager() {
		return mPrimaryPathManager;
	}
	/**
	 * @param pPrimaryPathManager -- the PrimaryPathManager
	 */
	public void setPrimaryPathManager(TRUCategoryPrimaryPathManager pPrimaryPathManager) {
		mPrimaryPathManager = pPrimaryPathManager;
	}


}
