package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.common.TRUErrorKeys;
import com.tru.rest.constants.TRURestConstants;

/**
 * Class TRURestParseGiftMessageDroplet extends DynamoServlet .
 * Used to get giftitemList.
 * @author PA
 * @version 1.0
 */
public class TRURestParseGiftMessageDroplet extends DynamoServlet {

	/**
	 * Used to get giftitemList.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:TRURestParseGiftMessageDroplet.service method");
		}
		String shipGroupId = (String) pRequest.getLocalParameter(TRURestConstants.SHIP_GROUPID);
		String giftMessage = (String) pRequest.getLocalParameter(TRURestConstants.GIFT_MESSAGE);
		boolean isMissingInfo = false;
		List<TRUGiftOptionsInfo> giftOptionsList = new ArrayList<TRUGiftOptionsInfo>();
		if (StringUtils.isBlank(shipGroupId) || StringUtils.isBlank(giftMessage)) {
			isMissingInfo = true;
		}
		if (isMissingInfo) {
		pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_GIFT_OPTIONS_MISSING_INFO);
		pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		return; 
		} else {
		TRUGiftOptionsInfo giftOptionVO = new TRUGiftOptionsInfo();
		giftOptionVO.setGiftWrapMessage(giftMessage);
		giftOptionVO.setShippingGroupId(shipGroupId);
		giftOptionVO.setGiftReceipt(true);
		giftOptionsList.add(giftOptionVO);
		TRUGiftOptionsInfo[] giftOptionsArray= giftOptionsList.toArray(new TRUGiftOptionsInfo[giftOptionsList.size()]);
		pRequest.setParameter(TRURestConstants.GIFT_OPTIONS_LIST, giftOptionsArray);
		pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRURestParseGiftMessageDroplet.service method");
		}
	}
}
