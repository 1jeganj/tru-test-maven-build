package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.rest.constants.TRURestConstants;

/**
 * The Class TRUMobileGiftWrapFilterDroplet.
 */
public class TRUMobileGiftWrapFilterDroplet extends DynamoServlet {

	/**
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *            - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUMobileGiftWrapFilterDroplet.service method..");
		}
		List<RepositoryItem> filtereditemsList = new ArrayList<RepositoryItem>();
		List<RepositoryItem> childSkuList = (List<RepositoryItem>) pRequest.getObjectParameter(TRUCommerceConstants.CHILD_SKU_LIST);
		if (childSkuList != null) {
			for (RepositoryItem childSku : childSkuList) {
				if (childSku.getPropertyValue(TRUCommerceConstants.TYPE).equals(TRUCommerceConstants.NON_MERCH_SKU)
						&& (childSku.getPropertyValue(TRUCommerceConstants.SUB_TYPE).equals(TRUCommerceConstants.TRU_GIFT_WRAP) || childSku
								.getPropertyValue(TRUCommerceConstants.SUB_TYPE).equals(TRUCommerceConstants.BRU_GIFT_WRAP))) {
					filtereditemsList.add(childSku);
				}
			}
			pRequest.setParameter(TRUCommerceConstants.FILTERED_ITEM_LIST, filtereditemsList);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}else{
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileGiftWrapFilterDroplet.service method.. ");
		}
	}
}
