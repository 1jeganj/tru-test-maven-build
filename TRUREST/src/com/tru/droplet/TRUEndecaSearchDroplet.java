package com.tru.droplet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentSlotConfig;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.DimCacheObjectVO;
import com.tru.common.ProductList;
import com.tru.common.RemoveAllAction;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.PopulateBeanFromContentItem;
import com.tru.util.TRUProductEndecaUtil;

/**
 * This class is TRUEndecaSearchDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0
 * @param <E>
 * 
 */
public class TRUEndecaSearchDroplet<E> extends DynamoServlet {
	
	/**
	 * This property hold reference for AssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;
	
	/**
	 * This property hold reference for TRURestConfiguration.
	 */
	
	private TRURestConfiguration mTRURestConfiguration;
	
	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;
	
	/**
	 * This property hold reference mTRUSEOTools.
	 */
	private TRUSEOTools mTRUSEOTools;
	
	/**
	 * This property hold reference populateBeanFromContentItem.
	 */
	@SuppressWarnings("rawtypes")
	private PopulateBeanFromContentItem mPopulateBeanFromContentItem;

	/**
	 * @return the assemblerTools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the assemblerTools to set
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the TRURestConfiguration
	 */
	public TRURestConfiguration getTRURestConfiguration() {
		return mTRURestConfiguration;
	}

	/**
	 * @param pTRURestConfiguration the TRURestConfiguration to set
	 */
	public void setTRURestConfiguration(TRURestConfiguration pTRURestConfiguration) {
		mTRURestConfiguration = pTRURestConfiguration;
	}
	
	/**
	 * Gets the dimensionValueCache
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets the dimensionValueCache
	 * @param pDimensionValueCache the dimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}
	
	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}
	/**
	 * sets Endeca Configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}
		
	/** Property Holds productEndecaUtil */
	private  TRUProductEndecaUtil<E>  mProductEndecaUtil;

	/**
	 * @return the mProductEndecaUtil
	 */
	public TRUProductEndecaUtil<E> getProductEndecaUtil() {
		return mProductEndecaUtil;
	}
	/**
	 * @param pProductEndecaUtil the mProductEndecaUtil to set
	 */
	public void setProductEndecaUtil(TRUProductEndecaUtil<E> pProductEndecaUtil) {
		this.mProductEndecaUtil = pProductEndecaUtil;
	}
	/**
	 * Used to Render the Endeca Content Item based on input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUEndecaSearchDroplet.service method");
		}
		ContentItem contentItem = null;
		ContentItem responseContentItem = null;
		String pageName = null;
		ProductList modifiedContentItem = null;
		StringBuffer addNrppParam = new StringBuffer();
		RemoveAllAction removeAllAction=new RemoveAllAction();
		boolean shopBy = false;
		String navigationState = (String) pRequest.getLocalParameter(TRURestConstants.NAVIGATIONSTATE);
		String categoryId = (String) pRequest.getLocalParameter(TRURestConstants.CATEGORYID);
		String searchKey = (String) pRequest.getLocalParameter(TRURestConstants.SEARCH_KEY);
		String pageOutput = (String) pRequest.getLocalParameter(TRURestConstants.PAGE_OUTPUT);
		String recordsPerPage = (String) pRequest.getLocalParameter(TRURestConstants.REC_LIMIT);
		String offset = (String) pRequest.getLocalParameter(TRURestConstants.OFFSET);
		String storeId = (String) pRequest.getLocalParameter(TRURestConstants.STORE_ID);
		shopBy = Boolean.parseBoolean((String) pRequest.getLocalParameter(TRURestConstants.SHOP_BY));
		String channelType = (String) pRequest.getHeader(TRURestConstants.API_CHANNEL);
		boolean isFilterCollection = false;
		isFilterCollection = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRURestConstants.FILTERCOLLECTION));
		boolean priceEnableFlag = false;
		priceEnableFlag =((Boolean)pRequest.getLocalParameter(TRURestConstants.PRICE_ENABLE));
		
		String tempNavigation;
		String refineId = null;
		String callType  = null;
		Map<String,Object> seoTagMap = null;
		if (isLoggingDebug()) {
			logDebug("navigationState"+navigationState+"\n categoryId"+categoryId+"\n searchKey"+searchKey);
		}
		if(StringUtils.isNotBlank(pRequest.getContentType())&&pRequest.getContentType().contains(TRURestConstants.CONTENT_TYPE_URL)&&StringUtils.isNotBlank(navigationState)){
			tempNavigation = navigationState.replaceAll(TRURestConstants.EMPERSAND, TRURestConstants.AND);
			navigationState = tempNavigation;
		}
		if(shopBy){
			if (isLoggingDebug()) {
				logDebug(" This is either brand or character request.  Id is"+ categoryId);
			}
			pRequest.setQueryString(TRURestConstants.NAVIGATION_EQUAL+categoryId);
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesShopBy());
		}else if(StringUtils.isNotBlank(categoryId)){
			if (isLoggingDebug()) {
				logDebug(" This is either category, subcategory or family request.  CategoryId is"+ categoryId);
			}
			callType = TRURestConstants.CATEGORY_OUTPUT;
			Set<String> siteIdList  = getProductEndecaUtil().getCategorySiteList(categoryId);
			if(siteIdList != null && !siteIdList.isEmpty() ){
				String currentSiteId = SiteContextManager.getCurrentSiteId();
				if (StringUtils.isNotBlank(currentSiteId) && ! siteIdList.contains(currentSiteId)) {
					if (isLoggingDebug()) {
						logDebug("Resulting into Error Message As data not available for the entered site id");
					}
					pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.ERR_ENTER_VALID_SITE);
					pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
					return;
				}
			}
			String dimValId = null;
			DimensionValueCacheObject dimCacheObject = null;
			DimCacheObjectVO dimCacheObjectVO = null;
			dimCacheObjectVO = getPopulateBeanFromContentItem().getNavigationURLfromCategory(categoryId);
			dimCacheObject = dimCacheObjectVO.getDimValueCacheObject();
			if(dimCacheObject==null ){
				if (isLoggingDebug()) {
					logDebug("Resulting into Error Message As incorrect parameter passed");
				}
				pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			boolean isRegClassification=false;
			isRegClassification = dimCacheObjectVO.isRegistryClassification();
			dimValId = dimCacheObject.getDimvalId();
			String tempURL = dimCacheObject.getUrl();
			if(StringUtils.isNotBlank(tempURL)){
				pageName = tempURL.substring(TRURestConstants.INT_ZERO, tempURL.indexOf(TRURestConstants.QUERY));
			}
			addNrppParam.append(TRURestConstants.NAVIGATION_EQUAL).append(dimValId).append(TRURestConstants.AND_CATEGORY_EQUAL).append(dimValId);
			if(StringUtils.isNotBlank(recordsPerPage)){
				addNrppParam.append(TRURestConstants.AND).append(TRURestConstants.NRPP_PARAM).append(recordsPerPage);
			}	
			removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+dimValId+TRURestConstants.AND_CATEGORY_EQUAL+dimValId);
			if(isFilterCollection && StringUtils.isNotEmpty(channelType) && getTRURestConfiguration().getCollectionProductsChannelList().contains(channelType)) {
				addNrppParam.append(TRURestConstants.AND).append(TRURestConstants.IS_COLLECTION_PRODUCT);
				removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+dimValId+TRURestConstants.AND_CATEGORY_EQUAL+dimValId+TRURestConstants.AND+TRURestConstants.IS_COLLECTION_PRODUCT);
			}
			pRequest.setQueryString(addNrppParam.toString());
			if (StringUtils.isNotBlank(pageName)) {
				if(isLoggingDebug()){
					logDebug("PageName is:"+pageName);
				}
				if (isRegClassification) {
						contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesFamily());	
				}
				else if(pageName.equals(getTRURestConfiguration().getCategoryPageURL())){
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesCategory());
				}
				else if(pageName.equals(getTRURestConfiguration().getSubCategoryPageURL())){
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesSubCategory());
				}
				else if(pageName.equals(getTRURestConfiguration().getFamilyPageURL())){
					contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesFamily());
				}
			}
			seoTagMap = getTRUSEOTools().getCategorySeoOverride(dimCacheObject.getRepositoryId());	
		}else if(StringUtils.isNotBlank(searchKey)){
			if (isLoggingDebug()) {
				logDebug("This is search request. SearchKey is"+ searchKey);
			}
			callType = TRURestConstants.SEARCH_OUTPUT;
			//This is search request
			addNrppParam.append(TRURestConstants.PARAM_SEARCH+searchKey);
			
			if(StringUtils.isNotBlank(recordsPerPage)){
				addNrppParam.append(TRURestConstants.AND).append(TRURestConstants.NRPP_PARAM).append(recordsPerPage);
			}
			removeAllAction.setNavigationState(TRURestConstants.QUERY+TRURestConstants.PARAM_SEARCH+searchKey);
			pRequest.setQueryString(addNrppParam.toString());
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesKeywordSearch());
		}else if(StringUtils.isNotBlank(navigationState)){
			if(isLoggingDebug()){
				logDebug("NavigationState is not Empty. This might be filter, clearFilter, clearAllFilter or load more product request.");
			}
			callType = TRURestConstants.FILTER_STRING;
			tempNavigation=URLDecoder.decode(navigationState, TRURestConstants.UTF_EIGHT);
			String[] breakNS=tempNavigation.substring(TRURestConstants.INT_ONE).split(TRURestConstants.AND);
			String[] params;
			int iterate=TRURestConstants.INT_ZERO;
			for(int size=breakNS.length;iterate<size;iterate++){
				if(iterate+TRURestConstants.INT_ONE<size && !breakNS[iterate+TRURestConstants.INT_ONE].contains(TRURestConstants.EQUALS)) {
					StringBuffer breakNSBuffer = new StringBuffer();
					breakNSBuffer.append(breakNS[iterate]).append(TRURestConstants.ENCODED_AMPERSAND).append(breakNS[iterate+TRURestConstants.INT_ONE]);
					breakNS[iterate] = breakNSBuffer.toString();
				}
				if(breakNS[iterate].contains(TRURestConstants.EQUALS)){
					params=breakNS[iterate].split(TRURestConstants.EQUALS);
					if(!params[TRURestConstants.INT_ZERO].equals(TRURestConstants.REC_LIMIT)&&!params[TRURestConstants.INT_ZERO].equals(TRURestConstants.OFFSET)){
						pRequest.setParameter(params[TRURestConstants.INT_ZERO], params[TRURestConstants.INT_ONE]);
						addNrppParam.append(breakNS[iterate]).append(TRURestConstants.AND);
					}
				}
				if(breakNS[iterate].contains(TRURestConstants.CATEGORY_EQUAL)){
					params=breakNS[iterate].split(TRURestConstants.EQUALS);
					refineId = params[TRURestConstants.INT_ONE];
					removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+refineId+TRURestConstants.AND+breakNS[iterate]);
					if(getDimensionValueCache().getCachedObjectForDimval(refineId)!=null){
						refineId=getDimensionValueCache().getCachedObjectForDimval(refineId).getRepositoryId();
					}
				} else if(breakNS[iterate].contains(TRURestConstants.PARAM_SEARCH)){
					removeAllAction.setNavigationState(TRURestConstants.QUERY+breakNS[iterate]);
				} 
			}
			if(isFilterCollection && StringUtils.isNotEmpty(channelType) && getTRURestConfiguration().getCollectionProductsChannelList().contains(channelType)) {
				String filterString = removeAllAction.getNavigationState();
				removeAllAction.setNavigationState(filterString+TRURestConstants.AND+TRURestConstants.IS_COLLECTION_PRODUCT);
			}
			if(StringUtils.isNotBlank(recordsPerPage)){
				addNrppParam.append(TRURestConstants.NRPP_PARAM+recordsPerPage+TRURestConstants.AND);
				pRequest.setParameter(TRURestConstants.REC_LIMIT, recordsPerPage);
			}
			if(StringUtils.isNotBlank(offset)){
				addNrppParam.append(TRURestConstants.NO_PARAM+offset+TRURestConstants.AND);
				pRequest.setParameter(TRURestConstants.OFFSET, offset);
			}
			pRequest.setQueryString(addNrppParam.deleteCharAt(addNrppParam.length()-TRURestConstants.INT_ONE).toString());
			contentItem = createContentSlotConfig(pRequest, getTRURestConfiguration().getContentAjaxknockoutCollection());
		}else{
			if (isLoggingDebug()) {
				logDebug("Resulting into Error Message As No required parameter passed");
			}
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		try {
			responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
			if(responseContentItem!=null){
				modifiedContentItem = getPopulateBeanFromContentItem().populateBean(responseContentItem,refineId,callType,storeId,priceEnableFlag);
				if(StringUtils.isNotBlank(modifiedContentItem.getSearchType()) && modifiedContentItem.getSearchType().equalsIgnoreCase(TRURestConstants.ERR_ENTER_VALID_SITE)) {
					pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.ERR_ENTER_VALID_SITE);
					pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
					return;
				}
				modifiedContentItem.setRemoveAllAction(removeAllAction);
				if(seoTagMap!=null){
					modifiedContentItem.setCategorySeoTitle((String) seoTagMap.get(TRURestConstants.CAT_SEO_TITLE));
					modifiedContentItem.setCategorySeoDisplayName((String) seoTagMap.get(TRURestConstants.CAT_SEO_H1_TEXT));
					modifiedContentItem.setCategorySeoDescription((String) seoTagMap.get(TRURestConstants.CAT_SEO_DESCRIPTION));
					if(StringUtils.isNotBlank((String) seoTagMap.get(TRURestConstants.KEYWORDS))){
						modifiedContentItem.setCategorySeoKeywords(Arrays.asList(seoTagMap.get(TRURestConstants.KEYWORDS).toString().split(TRURestConstants.SPLIT_SYMBOL_COMA)));
					}
					//setting the newly added properties
                    modifiedContentItem.setCategorySeoCanonicalUrl((String) seoTagMap.get(TRURestConstants.CANONICALURL));
                    modifiedContentItem.setCategorySeoAlternateUrl((String) seoTagMap.get(TRURestConstants.ALTURL));               
				}
				String currentSiteId = SiteContextManager.getCurrentSiteId();
				if(StringUtils.isNotBlank(currentSiteId)) {
					modifiedContentItem.setSiteId(currentSiteId);
				} 
			} else {
				pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
		}
		catch (AssemblerException e) {
			vlogError(e, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] { contentItem });
		}
		if(modifiedContentItem==null){
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		else{
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem);
			if(getEndecaConfigurations().isDisableCMSSpots() && StringUtils.isNotEmpty(channelType) && getEndecaConfigurations().getCMSSpotsDisabledChannelList().contains(channelType)){
				pageOutput = TRURestConstants.NON_CMSSPOT.concat(pageOutput);
			}
			pRequest.serviceLocalParameter(pageOutput, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUEndecaSearchDroplet.service method");
		}
	}
	
	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}
	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pContent - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentSlotConfig createContentSlotConfig(DynamoHttpServletRequest pRequest, String pContent)
	{
		return new ContentSlotConfig(pContent,TRURestConstants.INT_ONE);
	}

	/**
	 * @return the populateBeanFromContentItem
	 */
	@SuppressWarnings("rawtypes")
	public PopulateBeanFromContentItem getPopulateBeanFromContentItem() {
		return mPopulateBeanFromContentItem;
	}

	/**
	 * @param pPopulateBeanFromContentItem the populateBeanFromContentItem to set
	 */
	@SuppressWarnings("rawtypes")
	public void setPopulateBeanFromContentItem(PopulateBeanFromContentItem pPopulateBeanFromContentItem) {
		mPopulateBeanFromContentItem = pPopulateBeanFromContentItem;
	}

	/**
	 * @return the tRUSEOTools
	 */
	public TRUSEOTools getTRUSEOTools() {
		return mTRUSEOTools;
	}

	/**
	 * @param pTRUSEOTools the tRUSEOTools to set
	 */
	public void setTRUSEOTools(TRUSEOTools pTRUSEOTools) {
		mTRUSEOTools = pTRUSEOTools;
	}

}
