package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class used to render product related information in PDP page.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUMobileDisplayProductInfoDroplet extends DynamoServlet {
	
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 *  Holds the mPricingTools
	 */

	private TRUPricingTools mPricingTools;
	
	/**
	 *  Holds the mProductPageUtil
	 */
	private TRUProductPageUtil mProductPageUtil;
	/**
	 * This method will get the product related information required to be populated into PDP page.
	 * 
	 * @param pRequest - reference to DynamoHttpServletRequest object.
	 * @param pResponse - reference to DynamoHttpServletResponse object.
	 * @throws ServletException - if any exception occurs in servicing the request.
	 * @throws IOException - if any exception occurs while writing the response to output stream.
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.service method.. ");
		}
		Object productIdObj = pRequest.getLocalParameter(TRUCommerceConstants.PRODUCT_ID);
		String productId =  null;
		ProductInfoVO productInfo = null;
		String pdpURL = null;
		Site site = SiteContextManager.getCurrentSite();
		if(productIdObj!=null){
			productId =productIdObj.toString();
			try {
				productInfo = (getCatalogManager().createProductInfo(productId, false, null,TRUCommerceConstants.TYPE_PRODUCT,null));
				if (productInfo != null) {
					SKUInfoVO defaultSku= getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(null, productInfo);
					productInfo.setDefaultSKU(defaultSku);
					if(productInfo.getDefaultSKU()!=null){
						pdpURL=getProductPageUtil().getProductPageURL(productInfo.getDefaultSKU().getOnlinePID(), site.toString());
						productInfo.setPdpURL(pdpURL);
					}
					setItemPrice(productInfo,site);	
				}
				
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUMobileProductInfoLookupDroplet.service method ", repositoryException);
				}
			}
			if (productInfo != null) {
				pRequest.setParameter(TRURestConstants.PRODUCT_INFO, productInfo);
				pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
			}else{
				pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);	
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileDisplayProductInfoDroplet.");
		}
	}
	/**
	 * This method will set the price on skuLevel for PDP page.
	 * @param pProduct hold the reference of productVO
	 * @param pSite hold the reference of site
	 */
	protected void setItemPrice(ProductInfoVO pProduct, Site pSite) {
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		PriceInfoVO priceinfo = new PriceInfoVO();
		List<SKUInfoVO> childSku = new ArrayList<SKUInfoVO>();
		if (pProduct.getChildSKUsList() != null
				&& pProduct.getChildSKUsList().size() > 0) {
			for (SKUInfoVO childSkuinfo : pProduct.getChildSKUsList()) {
				listPrice = getPricingTools().getListPriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
				salePrice = getPricingTools().getSalePriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
				boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				if (calculateSavings) {
					priceinfo.setSavedAmount(savingsMap.get(TRUConstants.SAVED_AMOUNT));
					priceinfo.setSavedPercentage(savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);
				} else {
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);
				}
				childSkuinfo.setPriceInfo(priceinfo);
				childSku.add(childSkuinfo);
				pProduct.setChildSKUsList(childSku);
			}
		}

	}
	/**
	 * @return the mCatalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the CatalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		this.mCatalogManager = pCatalogManager;
	}
	/**
	 * @return the mPricingTools
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * @param pPricingTools the PricingTools to set
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}	
	
	/**
	 * @return the mProductPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * @param pProductPageUtil the ProductPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}
}
