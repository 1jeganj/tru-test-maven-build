package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.vo.TRUGiftOptionsInfo;
import com.tru.common.TRUConstants;
import com.tru.common.TRUErrorKeys;
import com.tru.rest.constants.TRURestConstants;

/**
 * Class TRURestParseGiftInformationDroplet extends DynamoServlet .
 * Used to get giftitemList.
 * @author PA
 * @version 1.0
 */
public class TRURestParseGiftInformationDroplet extends DynamoServlet {

	/**
	 * Used to get giftitemList.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:TRURestParseGiftInformationDroplet.service method");
		}
		String giftString = (String) pRequest.getLocalParameter(TRURestConstants.GIFT_OPTION_INFO_COLLECTION);
		String giftWrapCount = (String) pRequest.getLocalParameter(TRURestConstants.GIFT_OPTIONS_INFO_COUNT);
		List<TRUGiftOptionsInfo> giftOptionsList=new ArrayList<TRUGiftOptionsInfo>();
		boolean isMissingInfo=Boolean.FALSE;
		boolean handled=Boolean.FALSE;
		if (StringUtils.isBlank(giftString) || StringUtils.isBlank(giftWrapCount)) {
			pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_GIFT_OPTIONS_MISSING_INFO);
			handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		if (StringUtils.isNotBlank(giftString) && StringUtils.isNotBlank(giftWrapCount) && (Integer.parseInt(giftWrapCount) <= 0)) {
			
			pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_GIFT_OPTIONS_MISSING_INFO);
			handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
			String[] giftWrapInfos = giftString.split(TRURestConstants.SPLIT_SYMBOL_COMA);
			if (giftWrapInfos != null && giftWrapInfos.length > TRURestConstants.INT_ZERO) {
				for (String giftWrapfoItem : giftWrapInfos) {
					if (StringUtils.isNotBlank(giftWrapfoItem)) {
						String[] giftWrapProperties = giftWrapfoItem.split(TRURestConstants.SPLIT_PIPE);
						if (giftWrapProperties != null && giftWrapProperties.length >= TRUConstants.THREE) {
							TRUGiftOptionsInfo giftOptionVO = new TRUGiftOptionsInfo();
							boolean errorInInput=populateGiftOptionVO(giftWrapProperties,giftOptionVO);
							if(errorInInput){
								isMissingInfo=Boolean.TRUE;
								break;
							}
							if(isLoggingDebug()){
								logDebug("TRURestParseGiftInformationDroplet::@method::Service giftOptionVO is: " + giftOptionVO);
							}
							giftOptionsList.add(giftOptionVO);
						}
						else{
							isMissingInfo=Boolean.TRUE;
							break;
						}
					}else{
						isMissingInfo=Boolean.TRUE;
						break;
					}
				}
				if (isLoggingDebug()) {
					logDebug("TRURestParseGiftInformationDroplet.service method, giftOptionsList size"+giftOptionsList.size());
				}
			}else{
				isMissingInfo=Boolean.TRUE;
			}
			
	if(handled || isMissingInfo ){
		pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_GIFT_OPTIONS_MISSING_INFO);
		handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
	}else{
		TRUGiftOptionsInfo[] giftOptionsArray= giftOptionsList.toArray(new TRUGiftOptionsInfo[giftOptionsList.size()]);
		pRequest.setParameter(TRURestConstants.GIFT_OPTIONS_LIST, giftOptionsArray);
		handled=pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
	}
	
	if (isLoggingDebug()) {
		logDebug("END:TRURestParseGiftInformationDroplet.service method");
	}	
	}

	/**
	 * To populate gift option VO.
	 * @param pGiftWrapProperties pGiftWrapProperties
	 * @param pGiftOptionVO pGiftOptionVO
	 * @return valid input - returns true/false
	 */
	private boolean populateGiftOptionVO(String[] pGiftWrapProperties,
			TRUGiftOptionsInfo pGiftOptionVO) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:TRURestParseGiftInformationDroplet.populateGiftOptionVO method");
		}
		
		if(StringUtils.isBlank(pGiftWrapProperties[TRUConstants.ONE])){
			return true;
		}
		if(StringUtils.isBlank(pGiftWrapProperties[TRUConstants.TWO])){
			return true;
		}
		if(StringUtils.isBlank(pGiftWrapProperties[TRUConstants.THREE])){
			return true;
		}
		pGiftOptionVO.setGiftWrapMessage((pGiftWrapProperties[TRUConstants.ZERO]));
		if(StringUtils.isNotBlank(pGiftOptionVO.getGiftWrapMessage())){
			pGiftOptionVO.setGiftReceipt(true);
		}else{
			pGiftOptionVO.setGiftReceipt(Boolean.parseBoolean((pGiftWrapProperties[TRUConstants.ONE])));
		}
		pGiftOptionVO.setGiftOptionId((pGiftWrapProperties[TRUConstants.TWO]));
		pGiftOptionVO.setShippingAdressNickName(pGiftWrapProperties[TRUConstants.THREE]);
		if (isLoggingDebug()) {
			logDebug("END:TRURestParseGiftInformationDroplet.populateGiftOptionVO method");
		}
		return false;
	}
	
}
