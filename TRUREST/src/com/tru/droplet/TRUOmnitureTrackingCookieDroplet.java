package com.tru.droplet;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.constants.TRUCheckoutConstants;
import com.tru.common.TRUConstants;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is intended for creating the Omniture cookie. parameters to be
 * passed by user, 'visitor_id' is a required parameter.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>visitor_id</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>OMNITURE_USER</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/droplet/OmnitureTrackingCookieDroplet"&gt;<br>
 *          &lt;dspel:param name="visitor_id" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 */
public class TRUOmnitureTrackingCookieDroplet extends DynamoServlet {

	/**
	 * Constant to hold HYPHEN_SYMBOL.
	 */
	public static final String PATH = "/";

	/**
	 * This method is overridden to check if cookie has no omniture_id then add
	 * omniture_id to the cookie for REST Web Services.
	 * 
	 * @param pReq
	 *            - DynamoHttpServletRequest
	 * @param pRes
	 *            - DynamoHttpServletResponse
	 * @throws IOException
	 *             -
	 * @throws ServletException
	 *             -
	 */
	public void service(DynamoHttpServletRequest pReq, DynamoHttpServletResponse pRes) throws ServletException,	IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: OmnitureCookie.service method..");
		}
		String visitorId = (String) pReq.getLocalParameter(TRUConstants.PROFILE_ID);
		String customerStatus = TRUConstants.EMPTY_STRING;
		if(pReq.getLocalParameter(TRUConstants.LOGIN_STATUS) != null){		
			customerStatus = pReq.getLocalParameter(TRUConstants.LOGIN_STATUS).toString();
		}	
		String customerId = visitorId;
		String ipAddress = getIpAddress(pReq);
		String browserId = null;
		String sessionId = null;
		if (StringUtils.isNotBlank(ipAddress)){
			if (isLoggingDebug()) {
				logDebug("IP Address of the user is:= "+ ipAddress);
			}
			browserId = ipAddress.trim().replaceAll(TRURestConstants.REGEX_DOT_STRING, TRUConstants.EMPTY);
		}
	/*	if(pReq.getSession() != null) {
			sessionId = (String) pReq.getSession().getId();
		}*/
		if(pReq.getCookieParameter(TRUCheckoutConstants.TRUSESSIONID) != null){		
			sessionId = pReq.getCookieParameter(TRUCheckoutConstants.TRUSESSIONID).toString();
		}	
		String domainName = (String)pReq.getServerName();
		String domainNamePrefix=null;
		if (StringUtils.isNotBlank(domainName)){
			domainNamePrefix = domainName.substring(domainName.indexOf(TRUConstants.DOT), domainName.length());
		}
		if(domainNamePrefix.contains(TRURestConstants.TOYSRUS_DOMAIN)) {
			Cookie newcookie;
			if(pReq.getCookies() == null){
				newcookie = new Cookie(TRURestConstants.VISITOR_ID,visitorId);
				newcookie.setPath(PATH);
				newcookie.setDomain(domainNamePrefix);
				pRes.addCookie(newcookie);
				newcookie = new Cookie(TRURestConstants.CUSTOMER_ID,customerId);
				newcookie.setPath(PATH);
				newcookie.setDomain(domainNamePrefix);
				pRes.addCookie(newcookie);
				newcookie = new Cookie(TRURestConstants.BROWSER_ID,browserId);
				newcookie.setPath(PATH);
				newcookie.setDomain(domainNamePrefix);
				pRes.addCookie(newcookie);
				newcookie = new Cookie(TRURestConstants.SESSION_ID,sessionId);
				newcookie.setPath(PATH);
				newcookie.setDomain(domainNamePrefix);
				pRes.addCookie(newcookie);
				newcookie = new Cookie(TRUConstants.CUST_STATUS_COOKIE,customerStatus);
				newcookie.setPath(PATH);
				newcookie.setDomain(domainNamePrefix);
				pRes.addCookie(newcookie);
				
			}else{
				setOmnitureDetails(pReq, pRes, visitorId, customerId,browserId,sessionId,domainNamePrefix,customerStatus);
			}
			if (pReq.getCookies() != null) {
				for (Cookie cookie : pReq.getCookies()) {
					if (cookie.getName().equals(TRURestConstants.VISITOR_ID)) {
						String visitorIdFromCookie = (String) cookie.getValue();
						pReq.setParameter(TRURestConstants.VISITOR_ID, visitorIdFromCookie);
					} else if (cookie.getName().equals(TRURestConstants.CUSTOMER_ID)) {
						String profileIdFromCookie = (String) cookie.getValue();
						pReq.setParameter(TRURestConstants.CUSTOMER_ID, profileIdFromCookie);
					} else if (cookie.getName().equals(TRURestConstants.BROWSER_ID)) {
						String browserIdFromCookie = (String) cookie.getValue();
						pReq.setParameter(TRURestConstants.BROWSER_ID,browserIdFromCookie);
					}
					else if (cookie.getName().equals(TRURestConstants.SESSION_ID)) {
						String sessionIdFromCookie = (String) cookie.getValue();
						pReq.setParameter(TRURestConstants.SESSION_ID,sessionIdFromCookie);
					}
				  }
			}
			pReq.setParameter(TRUConstants.SESSION_ID, sessionId);
			pReq.setParameter(TRUConstants.VISITOR_ID, visitorId);
		}
		pReq.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pReq, pRes);
		if (isLoggingDebug()) {
			logDebug("END:: OmnitureCookie.service method..");
		}
	}
	/**
	 * This method will set Omniture  details.
	 * 
	 * 
	 * @param pReq -  Request
	 * @param pRes - Response
	 * @param pVisitorId - VisitorId 
	 * @param pCustomerId - CustomerId
	 * @param pBrowserId - BrowserId
	 * @param pSessionId - SessionId
	 * @param pCustStatus - pCustStatus
	 * @param pDomainNamePrefix - DomainNamePrefix
	 */
	private void setOmnitureDetails(DynamoHttpServletRequest pReq,DynamoHttpServletResponse pRes, String pVisitorId,String pCustomerId, String pBrowserId,
								     String pSessionId,String pDomainNamePrefix,String pCustStatus) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: OmnitureCookie.setOmnitureDetails method..");
		}
		boolean isCreateOmnitureUserCookie=true;
		boolean isCreateBrowserCookie=true;
		boolean isCreateSessionCookie=true;
		Cookie newcookie;
		for (Cookie cookie : pReq.getCookies()) {
			if (cookie.getName().equals(TRURestConstants.VISITOR_ID)) {
				isCreateOmnitureUserCookie=false;
			} else if (cookie.getName().equals(TRURestConstants.BROWSER_ID)) {
				isCreateBrowserCookie=false;
			}else if (cookie.getName().equals(TRURestConstants.SESSION_ID)) {
				isCreateSessionCookie=false;
			}
		}
		if(isCreateOmnitureUserCookie && StringUtils.isNotBlank(pVisitorId)){
			newcookie = new Cookie(TRURestConstants.VISITOR_ID,pVisitorId);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(StringUtils.isNotBlank(pCustomerId)){
			newcookie = new Cookie(TRURestConstants.CUSTOMER_ID,pCustomerId);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateBrowserCookie && StringUtils.isNotBlank(pBrowserId)){
			newcookie = new Cookie(TRURestConstants.BROWSER_ID,pBrowserId);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(isCreateSessionCookie && StringUtils.isNotBlank(pSessionId)){
			newcookie = new Cookie(TRURestConstants.SESSION_ID,pSessionId);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if(StringUtils.isNotBlank(pCustStatus)){
			newcookie = new Cookie(TRUConstants.CUST_STATUS_COOKIE,pCustStatus);
			newcookie.setPath(PATH);
			newcookie.setDomain(pDomainNamePrefix);
			pRes.addCookie(newcookie);
		}
		if (isLoggingDebug()) {
			logDebug("END:: OmnitureCookie.setOmnitureDetails method..");
		}
	}

	/**
	 * Method gets the ip address from the request.
	 * 
	 * @param pDynamoRequest
	 *            - DynamoHttpServletRequest
	 * @return String - IpAddress of the user.
	 */
	public String getIpAddress(DynamoHttpServletRequest pDynamoRequest) {
		if (isLoggingDebug()) {
			logDebug("START:: OmnitureCookie.getIpAddress method..");
			vlogDebug("DynamoRequest :{0}", pDynamoRequest);
		}
		String clientIp = TRURestConstants.EMPTY_SPACE;
		if (pDynamoRequest != null) {
			clientIp = pDynamoRequest.getHeader(TRURestConstants.IP_ADDRESS);
			if(StringUtils.isNotBlank(clientIp)) {
				StringTokenizer tokenizer = new StringTokenizer(clientIp,TRURestConstants.SPLIT_SYMBOL_COMA);
				 while (tokenizer.hasMoreTokens()) {
	                	clientIp = tokenizer.nextToken().trim();
	                	break;
				 }
			}
			if (isLoggingDebug()) {
				vlogDebug(" True Client IP :{0}", clientIp);
			}
			if (StringUtils.isBlank(clientIp)) {
				if (isLoggingDebug()) {
					vlogDebug(
							"The Client ip :{0} Header is null so setting Remote Addr as client IP",clientIp);
				}
				clientIp = pDynamoRequest.getRemoteAddr();
				if (isLoggingDebug()) {
					vlogDebug("True Client IP after setting RemoteAddress:{0}",clientIp);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug(" True Client IP :{0}", clientIp);
			logDebug("END:: OmnitureCookie.getIpAddress method..");
		}
		return clientIp;
	}

}
