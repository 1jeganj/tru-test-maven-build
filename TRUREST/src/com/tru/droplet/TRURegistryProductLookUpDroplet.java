package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ERecList;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.common.RegistryProductInfoVO;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUProductEndecaUtil;

/**
 * This Class is TRURegistryProductLookUpDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0
 * @param <E> the type of elements in this collection
 * 
 */
public class TRURegistryProductLookUpDroplet<E> extends DynamoServlet {
	
	/** Holds mdexRequest property of mProductEndecaUtil. */
	private  TRUProductEndecaUtil<E>  mProductEndecaUtil;
	
	/** Holds mdexRequest property of mInventoryEnabled. */
	private boolean mInventoryEnabled;
	
	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRURegistryLoadProductDroplet.service method.. ");
		}
		ERecList eRecList = null;
		List<String> skuIdList = null;
		String skuIdString = null;
		String upcIdString = null;
		String onlinePID = null;
		Set<RegistryProductInfoVO> productSet = new HashSet<RegistryProductInfoVO>();
		List<RegistryProductInfoVO> skuInfoList = null;
		String skuCode = (String) pRequest.getLocalParameter(TRURestConstants.SKU_CODE);
		String upcNumber = (String) pRequest.getLocalParameter(TRURestConstants.UPC_NUMBER);
		String onlinePIds = (String)pRequest.getLocalParameter(TRURestConstants.ONLINE_PID);
		String skuDetail = (String) pRequest.getLocalParameter(TRURestConstants.SKU_DETAILS);
		try {
			if (StringUtils.isNotEmpty(skuCode)) {
				skuIdList = spliteSkuId(skuCode);
				skuIdString = skuCode.replace(TRURestConstants.SINGLE_PIPE, TRURestConstants.PLUS);
				eRecList = getProductEndecaUtil().createSearchQuery(skuIdString, TRURestConstants.SKU_REPOSITORYID);
				skuInfoList = getProductEndecaUtil().populateSearchResult(eRecList, skuIdList, skuDetail, isInventoryEnabled());
				productSet.addAll(skuInfoList);
			} else if (StringUtils.isNotEmpty(upcNumber)) {
				upcIdString = upcNumber.replace(TRURestConstants.SINGLE_PIPE, TRURestConstants.PLUS);
				eRecList = getProductEndecaUtil().createSearchQuery(upcIdString, TRURestConstants.SKU_UPCNUMBERS);
				skuIdList = getProductEndecaUtil().getSkuIdsFromRecord(eRecList);
				skuInfoList = getProductEndecaUtil().populateSearchResult(eRecList, skuIdList, skuDetail, isInventoryEnabled());
				productSet.addAll(skuInfoList);
			} else if (StringUtils.isNotEmpty(onlinePIds)) {
				onlinePID = onlinePIds.replace(TRURestConstants.SINGLE_PIPE, TRURestConstants.PLUS);
				eRecList = getProductEndecaUtil().createSearchQuery(onlinePID, TRURestConstants.ONLINE_ID);
				skuIdList = getProductEndecaUtil().getSkuIdsFromRecord(eRecList);
				skuInfoList = getProductEndecaUtil().populateSearchResult(eRecList, skuIdList, skuDetail, isInventoryEnabled());
				productSet.addAll(skuInfoList);
			}
		} catch (ENEQueryException eQueryException) {
			if (isLoggingError()) {
				logError("ENEQueryException in TRURegistryLoadProductDroplet.::@method::service :",eQueryException);
			}
		} catch (SiteContextException siteContextException) {
			if (isLoggingError()) {
				logError("SiteContextException in TRURegistryLoadProductDroplet.::@method::service :",siteContextException);
			}
		}
		if(StringUtils.isBlank(skuCode) && StringUtils.isBlank(upcNumber) && StringUtils.isBlank(onlinePIds)) {
			if (isLoggingDebug()) {
				logDebug("Resulting into Error Message As No required parameter passed");
			}
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		if (productSet != null && !productSet.isEmpty()) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, productSet);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRURegistryLoadProductDroplet.");
		}			
	}
	/**
	 * This method will split the skuCode or upc or productId from the request.
	 * @param pSpliteIds SpliteIds
	 * @return productIdList productIdList
	 */
		public List<String> spliteSkuId(String pSpliteIds) {
			if (isLoggingDebug()) {
				logDebug("BEGIN: @method spliteProductId() @class: TRURegistryProductLookUpDroplet" +pSpliteIds);
			}
			String skuIdInput = null;
			String[] productIdArray;
			List<String> productIdList = new ArrayList<String>();
			skuIdInput = pSpliteIds.replaceAll(TRUCommerceConstants.OPEN, TRUCommerceConstants.EMPTY_SPACE).replaceAll(TRUCommerceConstants.CLOSE, TRUCommerceConstants.EMPTY_SPACE);
			productIdArray = skuIdInput.split(TRUCommerceConstants.SPLIT_PIPE);
			for(String skuVal : productIdArray) {
				productIdList.add(skuVal);
			}
			if (isLoggingDebug()) {
				logDebug("END: @method spliteProductId() @class: TRURegistryProductLookUpDroplet");
			}
			return productIdList;
		}
	/**
	 * @return the mProductEndecaUtil
	 */
	public TRUProductEndecaUtil<E> getProductEndecaUtil() {
		return mProductEndecaUtil;
	}
	/**
	 * @param pProductEndecaUtil the mProductEndecaUtil to set
	 */
	public void setProductEndecaUtil(TRUProductEndecaUtil<E> pProductEndecaUtil) {
		this.mProductEndecaUtil = pProductEndecaUtil;
	}
	/**
	 * @return the mInventoryEnabled
	 */
	public boolean isInventoryEnabled() {
		return mInventoryEnabled;
	}
	/**
	 * @param pInventoryEnabled the mInventoryEnabled to set
	 */
	public void setInventoryEnabled(boolean pInventoryEnabled) {
		this.mInventoryEnabled = pInventoryEnabled;
	}
}
