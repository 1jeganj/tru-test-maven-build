package com.tru.droplet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.common.vo.ResourceBundleVO;
import com.tru.rest.constants.TRURestConstants;
/**
 * This TRUResourceBundleDroplet fetches values for the resource bundle keys.
 * @author Professional Access
 * @version  : 1.0
 * 
 */
public class TRUResourceBundleDroplet extends DynamoServlet {
	/**  Holds CatalogTools. */
	private TRUCatalogTools mCatalogTools;

	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	/**
	 * This method is used to fetch values for the resource bundle keys.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUEndecaSearchDroplet.service method");
		}
		List<ResourceBundleVO> keyMessageMap = null;
		String keys=(String) pRequest.getLocalParameter(TRURestConstants.KEYS);
		if(StringUtils.isNotBlank(keys)){
			keyMessageMap = getCatalogTools().getResourceBundleMessage(Arrays.asList(keys.split(TRURestConstants.SPLIT_PIPE)));
		}
		if(StringUtils.isNotBlank(keys)&&keyMessageMap!=null){
			pRequest.setParameter(TRURestConstants.MESSAGES, keyMessageMap);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUEndecaSearchDroplet.service method");
		}
	}
}
