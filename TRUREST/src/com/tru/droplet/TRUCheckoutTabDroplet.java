package com.tru.droplet;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.purchase.TRUPurchaseProcessHelper;
import com.tru.rest.constants.TRURestConstants;
/**
 * This class is used to set next tab in checkout page 
 * @version 1.0
 * @author Professional Access
 */
public class TRUCheckoutTabDroplet extends DynamoServlet{
	
	/* This property used to hold map of checkout tabs */
	private Map<String,String> mCheckoutTabMap;
	
	/* This property used to hold orderManager */
	private TRUOrderManager mOrderManager;
	
	/* This property used to hold orderTools */
	private TRUOrderTools mOrderTools;
	
	/* This property used to hold orderTools */
	private TRUPurchaseProcessHelper mPurchaseProcessHelper;

	/**
	 * @return the purchaseProcessHelper
	 */
	public TRUPurchaseProcessHelper getPurchaseProcessHelper() {
		return mPurchaseProcessHelper;
	}

	/**
	 * @param pPurchaseProcessHelper the purchaseProcessHelper to set
	 */
	public void setPurchaseProcessHelper(
			TRUPurchaseProcessHelper pPurchaseProcessHelper) {
		mPurchaseProcessHelper = pPurchaseProcessHelper;
	}

	/**
	 * @return the mOrderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * @return the mOrderTools
	 */
	public TRUOrderTools getOrderTools() {
		return mOrderTools;
	}

	/**
	 * @param pOrderTools the orderTools to set
	 */
	public void setOrderTools(TRUOrderTools pOrderTools) {
		mOrderTools = pOrderTools;
	}

	/**
	 * @return the checkoutTabMap
	 */
	public Map<String, String> getCheckoutTabMap() {
		return mCheckoutTabMap;
	}

	/**
	 * @param pCheckoutTabMap the checkoutTabMap to set
	 */
	public void setCheckoutTabMap(Map<String, String> pCheckoutTabMap) {
		mCheckoutTabMap = pCheckoutTabMap;
	}
	
	/**
	 * This method is used to set next tab in order for rest call.
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUCheckoutTabDroplet.service method");
		}
		String previousTab=TRURestConstants.EMPTY_SPACE;
		String currentTab=TRURestConstants.EMPTY_SPACE;
		String nextTab=TRURestConstants.EMPTY_SPACE;
		
		TRUOrderImpl order = (TRUOrderImpl) pRequest.getLocalParameter(TRURestConstants.ORDER);
		if(order==null){return;}
		Map<String,Boolean> activeTabs = order.getActiveTabs();
		currentTab = (String) pRequest.getLocalParameter(TRURestConstants.PAGE_NAME);
		
		if(activeTabs.isEmpty()){
			previousTab=TRURestConstants.EMPTY_SPACE;
		} else {
			previousTab = order.getCurrentTab();
		}
		
		Integer previousTabValue = TRURestConstants.INT_ZERO;
		Integer currentTabValue = TRURestConstants.INT_ONE;
		boolean flag=false;
		
		if(getCheckoutTabMap().containsKey(currentTab)&&getCheckoutTabMap().containsKey(previousTab)){
			previousTabValue=Integer.parseInt(getCheckoutTabMap().get(previousTab).toString());
			currentTabValue=Integer.parseInt(getCheckoutTabMap().get(currentTab).toString());
		}
		
		if(previousTabValue>currentTabValue){
			//reverse flow case 1
			if(previousTabValue==TRURestConstants.INT_FIVE){
				nextTab=TRURestConstants.REVIEW_TAB;
			}else if(previousTabValue==TRURestConstants.INT_FOUR){
				nextTab=TRURestConstants.PAYMENT_TAB;
			}else{
				//set next active tab reverse flow case 2
				for (Entry<String, Boolean> entry : activeTabs.entrySet()) {
					if(flag){
						nextTab = entry.getKey();
					}
		            if (entry.getKey().equals(currentTab)) {
		            	flag=true;
		            }
		        }
			}
		}else{
			//simple forward flow case 3
			List<TRUInStorePickupShippingGroup> instoreList= ((TRUOrderTools) getOrderManager().getOrderTools()).getAllInStorePickupShippingGroups(order);
			if(currentTab.equals(TRURestConstants.SHIPPING_TAB)){
				if(order.isShowMeGiftingOptions()){
					nextTab=TRURestConstants.GIFTING_TAB;
				} else if(instoreList != null && !instoreList.isEmpty()){
					nextTab=TRURestConstants.PICKUP_TAB;
				} else {
					nextTab=TRURestConstants.PAYMENT_TAB;
				}
			} else if(currentTab.equals(TRURestConstants.GIFTING_TAB)){
				if(instoreList != null && !instoreList.isEmpty()){
					nextTab=TRURestConstants.PICKUP_TAB;
				} else {
					nextTab=TRURestConstants.PAYMENT_TAB;
				}
			} else if(currentTab.equals(TRURestConstants.PICKUP_TAB)){
				nextTab=TRURestConstants.PAYMENT_TAB;
			} else {
				nextTab=TRURestConstants.REVIEW_TAB;
			}
		}
		
		if(currentTab.equals(TRURestConstants.REVIEW_TAB)){
			nextTab=TRURestConstants.EMPTY_SPACE;
		}
				
		order.setCurrentTab(currentTab);
		order.setPreviousTab(previousTab);
		order.setNextTab(nextTab);
				
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUCheckoutTabDroplet.service method");
		}
	}
}
