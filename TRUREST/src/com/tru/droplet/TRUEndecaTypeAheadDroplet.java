package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.Site;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.content.support.XmlContentItem;
import com.tru.common.TRURestConfiguration;
import com.tru.common.searchsuggestions.AutoSuggest;
import com.tru.common.searchsuggestions.Dimension;
import com.tru.common.searchsuggestions.DimensionVal;
import com.tru.common.searchsuggestions.Product;
import com.tru.common.vo.TRUSiteVO;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * This class is TRUEndecaTypeAheadDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0 
 * @param <E> the type of elements in this collection
 */
public class TRUEndecaTypeAheadDroplet<E> extends DynamoServlet {
	
	/**
	 * This property hold reference for AssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;
	
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;

	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}
	
	/**
	 * This property hold reference for TRURestConfiguration.
	 */
	private TRURestConfiguration mTRURestConfiguration;
	
	/** Property to hold mGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mGetSiteTypeUtil;
	
	/**
	 * @return the assemblerTools.
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the assemblerTools to set.
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the tRURestConfiguration.
	 */
	public TRURestConfiguration getTRURestConfiguration() {
		return mTRURestConfiguration;
	}

	/**
	 * @param pTRURestConfiguration the tRURestConfiguration to set.
	 */
	public void setTRURestConfiguration(TRURestConfiguration pTRURestConfiguration) {
		mTRURestConfiguration = pTRURestConfiguration;
	}
	
	/**
	 * Used to Render the Endeca Content Item based on input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUEndecaSearchDroplet.service method");
		}
		ContentItem contentItem = null;
		ContentItem responseContentItem = null;
		String queryString = null;
		AutoSuggest modifiedContentItem = null;
		String searchKey = (String) pRequest.getLocalParameter(TRURestConstants.SEARCH_KEY);
		if(StringUtils.isBlank(searchKey)){
			if (isLoggingDebug()) {
				logDebug("Resulting into Error Message As No required parameter passed");
			}
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		else{
			queryString = TRURestConstants.PARAM_SEARCH+searchKey;
			pRequest.setQueryString(queryString);
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesTypeAhead());
		}
		try {
			responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
			if(responseContentItem!=null){
				modifiedContentItem = populateBean(responseContentItem);
			} else {
				pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
		}
		catch (AssemblerException e) {
			vlogError(e, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] { contentItem });
		}
		if((contentItem==null) || (modifiedContentItem==null) ){
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		else{
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUEndecaSearchDroplet.service method");
		}
	}
	
	/**
	 * Used to render typeAhead searchSuggestions for mobile.
	 * @param pResponseContentItem - holds the reference for ResponseContentItem
	 * @return TypeAheadResult - returns a TypeAheadResult
	 */
	@SuppressWarnings({ "unchecked" })
	private AutoSuggest populateBean(ContentItem pResponseContentItem) {
		List<E> contentItem = (ArrayList<E>) pResponseContentItem.get(getTRURestConfiguration().getContents());
		XmlContentItem allContents = null;
		if(contentItem!=null && !contentItem.isEmpty()){
			allContents = (XmlContentItem) contentItem.get(TRURestConstants.INT_ZERO);
			if(allContents.get(TRURestConstants.TYPEAHEAD_NAME)!=null){
				AutoSuggest autoSuggest = new AutoSuggest();
				contentItem = (ArrayList<E>) allContents.get(TRURestConstants.TYPEAHEAD_NAME);
				int arraySize = TRURestConstants.INT_ZERO;
				if(contentItem!=null){
					arraySize=contentItem.size();
				}
				for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
					if(contentItem.get(iterate) instanceof ResultsList && ((String)((ResultsList)contentItem.get(iterate)).get(getTRURestConfiguration().getAtType())).equals(TRURestConstants.TYPEAHEAD_RESULTLIST)){
						ResultsList resultsList = (ResultsList)contentItem.get(iterate);
						populateSuggestedProducts(resultsList, autoSuggest);
					}
					if(contentItem.get(iterate) instanceof DimensionSearchResults && ((String)((DimensionSearchResults)contentItem.get(iterate)).get(getTRURestConfiguration().getAtType())).equals(TRURestConstants.DIMENSION_SEARCH_AUTOSUGGEST)){
						DimensionSearchResults dimensionSearchResults = (DimensionSearchResults)contentItem.get(iterate);
						populateDimensions(dimensionSearchResults, autoSuggest);
					}
				}
				return autoSuggest;
			}
		}
		return null;
	}
	
	/**
	 * Used to populate product suggestions for mobile.
	 * @param pResultsList - holds the reference for ResultsList cartridge response
	 * @param pAutoSuggest - new bean for filter endeca response
	 */
	@SuppressWarnings("rawtypes")
	private void populateSuggestedProducts(ResultsList pResultsList, AutoSuggest pAutoSuggest){
		Record endecaRecord;
		Product newProduct;
		HashMap<String,Attribute> productAttributes;
		List<com.tru.common.searchsuggestions.Record> reocrds = new ArrayList<com.tru.common.searchsuggestions.Record>();
		com.tru.common.searchsuggestions.Record record;
		com.tru.common.searchsuggestions.Attribute attribute;
		List<Record> products = (ArrayList<Record>)pResultsList.getRecords();
		ListIterator<Record> iterateProduct = (ListIterator<Record>) products.listIterator();
		while(iterateProduct.hasNext()){
			record = new com.tru.common.searchsuggestions.Record();
			endecaRecord = iterateProduct.next();
			productAttributes = (HashMap<String,Attribute>) endecaRecord.getAttributes();
			newProduct = new Product(); 
			if(productAttributes.get(getTRURestConfiguration().getProductDisplayName())!=null){
				newProduct.setDisplayName(productAttributes.get(getTRURestConfiguration().getProductDisplayName()).toString());
			}
			if(productAttributes.get(getTRURestConfiguration().getProductRepositoryId())!=null){
				newProduct.setProductId(productAttributes.get(getTRURestConfiguration().getProductRepositoryId()).toString());
			}
			if(productAttributes.get(getTRURestConfiguration().getProductSiteId())!=null){
				newProduct.setSiteId(productAttributes.get(getTRURestConfiguration().getProductSiteId()).toString());
			}
			if(endecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getTRURestConfiguration().getOnlinePID())!=null){
				newProduct.setOnlinePID(endecaRecord.getRecords().get(TRURestConstants.INT_ZERO).getAttributes().get(getTRURestConfiguration().getOnlinePID()).toString());
			}
			if(endecaRecord.getDetailsAction().getRecordState()!=null){
				newProduct.setUrl(getSchemeDetectionUtil().getScheme()+endecaRecord.getDetailsAction().getRecordState());
			}
			attribute = new com.tru.common.searchsuggestions.Attribute();
			attribute.setProduct(newProduct);
			record.setAttributes(attribute);
			reocrds.add(record);
		}
		pAutoSuggest.setTotalNumRecs(pResultsList.getTotalNumRecs());
		pAutoSuggest.setRecords(reocrds);
	}
	
	/**
	 * Used to populate dimension suggestions for mobile.
	 * @param pDimensionSearchResults - holds the reference for DimensionSearchResults cartridge response
	 * @param pAutoSuggest - new bean for filter endeca response
	 */
	@SuppressWarnings("unchecked")
	private void populateDimensions(DimensionSearchResults pDimensionSearchResults, AutoSuggest pAutoSuggest){
		Dimension newDimension;
		List<DimensionVal> newDimensionValList;
		DimensionVal newDimensionVal;
		DimensionSearchValue dimensionSearchValue;
		List<DimensionSearchValue> DimensionSearchValue;
		DimensionSearchGroup dimensionSearchGroup;
		ListIterator<DimensionSearchValue> dimensionSearchValueIterate;
		List<DimensionSearchGroup> dimensionSearchGroups = (ArrayList<DimensionSearchGroup>) pDimensionSearchResults.get(TRURestConstants.DIMENSION_SEARCH_GROUPS);
		ListIterator<DimensionSearchGroup> dimensionSearchGroupsIterate = dimensionSearchGroups.listIterator();
		List<Dimension> newDimensionList = new ArrayList<Dimension>();
		while(dimensionSearchGroupsIterate.hasNext()){		
			newDimension = new Dimension();
			newDimensionValList = new ArrayList<DimensionVal>();
			dimensionSearchGroup = dimensionSearchGroupsIterate.next();
			DimensionSearchValue = dimensionSearchGroup.getDimensionSearchValues();
			dimensionSearchValueIterate = DimensionSearchValue.listIterator();
			while(dimensionSearchValueIterate.hasNext()){
				newDimensionVal = new DimensionVal();
				dimensionSearchValue = dimensionSearchValueIterate.next();
				newDimensionVal.setLabel(dimensionSearchValue.getLabel());
				newDimensionVal.setNavigationState(getDimensionURLWithDomain(dimensionSearchValue, dimensionSearchGroup.getDimensionName()));
				newDimensionVal.setCategoryId(dimensionSearchValue.getProperties().get(getTRURestConfiguration().getCategoryRepositoryId()));
				newDimensionValList.add(newDimensionVal);
			}
			newDimension.setDimensionSearchValues(newDimensionValList);
			newDimensionList.add(newDimension);
		}
		pAutoSuggest.setDimensionSearchGroups(newDimensionList);
	}

	/**
	 * Populate dimension URL.
	 * 
	 * @param pDimValue
	 *            dimension search value
	 * @param pDimnsionName
	 *            dimension name
	 * @return dimsionURL String
	 */
	private String getDimensionURLWithDomain(DimensionSearchValue pDimValue, String pDimnsionName) {
		StringBuffer dimsionURL = new StringBuffer();
		if (pDimnsionName.equals(TRUEndecaConstants.PRODUCT_CATEGORY_DIMENSION_NAME)) {
			dimsionURL.append(pDimValue.getNavigationState());
		} else {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			dimsionURL.append(getSchemeDetectionUtil().getScheme());
			Site currentSite = getGetSiteTypeUtil().getSiteContextManager().getCurrentSite();
			TRUSiteVO siteVO = getGetSiteTypeUtil().getSiteInfo(request, currentSite);
			dimsionURL.append(siteVO.getSiteURL());
			dimsionURL.append(getTRURestConfiguration().getSearchPageURL());
			dimsionURL.append(TRURestConstants.QUERY);
			dimsionURL.append(TRURestConstants.PARAM_SEARCH);
			dimsionURL.append(pDimValue.getLabel());
		}
		return dimsionURL.toString();
	}

	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}

	/**
	 * @return the getSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getGetSiteTypeUtil() {
		return mGetSiteTypeUtil;
	}

	/**
	 * @param pGetSiteTypeUtil the getSiteTypeUtil to set
	 */
	public void setGetSiteTypeUtil(TRUGetSiteTypeUtil pGetSiteTypeUtil) {
		mGetSiteTypeUtil = pGetSiteTypeUtil;
	}

}
