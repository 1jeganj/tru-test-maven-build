package com.tru.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConstants;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUApplePayUtils;


/**
 * This droplet is used to validate shipping restrictions for applePay.
 * @author PA
 * @version 1.0
 */
public class TRUShippingRestrictionsDroplet extends DynamoServlet {
	/**
	 * Holds the mApplePayUtils
	 */
	private TRUApplePayUtils mApplePayUtils;
	/**
	 * @return the mApplePayUtils
	 */
	public TRUApplePayUtils getApplePayUtils() {
		return mApplePayUtils;
	}

	/**
	 * @param pApplePayUtils
	 *            the ApplePayUtils to set
	 */
	public void setApplePayUtils(TRUApplePayUtils pApplePayUtils) {
		mApplePayUtils = pApplePayUtils;
	}
	
	
	/**
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			vlogDebug("TRUShippingRestrictionsDroplet.service :: START");
		}
		boolean applePayBuyNow = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRURestConstants.APPLE_PAY_BUY_NOW));
		String zipCode = (String) pRequest.getLocalParameter(TRURestConstants.ZIP_CODE);
		String skuId = (String) pRequest.getLocalParameter(TRURestConstants.SKUID);
		boolean hasShippingRestrictions = Boolean.FALSE;
		Order order=(Order) pRequest.getObjectParameter(TRUConstants.CURRENT_ORDER);
		final Map<String, String> restrictionsMap=new HashMap<String, String>();
		if(applePayBuyNow) {
			if(StringUtils.isBlank(skuId) || StringUtils.isBlank(zipCode)) {
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			getApplePayUtils().getRestrictionError(restrictionsMap, skuId, zipCode);
			
		} else {
			if(order == null || order.getCommerceItems().isEmpty() || StringUtils.isBlank(zipCode)) {
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			getApplePayUtils().validateZipCodeRestriction(order, zipCode, restrictionsMap);
		}
		if(!restrictionsMap.isEmpty()) {
			hasShippingRestrictions = Boolean.TRUE;
		}	
		pRequest.setParameter(TRURestConstants.HAS_SHIPPING_RESTRICTIONS, hasShippingRestrictions);
		pRequest.setParameter(TRURestConstants.RESTRICTIONS_MAP, restrictionsMap);
		pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			vlogDebug("TRUShippingRestrictionsDroplet.service :: END");
		}
	}
}
