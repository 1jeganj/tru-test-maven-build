package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.comparison.TRUProductComparisonList;
import com.tru.commerce.catalog.vo.CompareProductInfoVO;
import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is TRUMobileCompareProductInfoDroplet  which will get used from REST Actor.
 * @author PA
 * @version 1.0
 * 
 */
public class TRUMobileCompareProductInfoDroplet extends DynamoServlet{
	
	/**
	 * Property to hold mCatalogTools.
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 *  Holds the mPricingTools.
	 */

	private TRUPricingTools mPricingTools;
	
	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUMobileCompareProductInfoDroplet.service method.. ");
		}
		String[] onlinePID = null;
		String productIdFromOnlinePID = null;
		List<String> productIds = new ArrayList<String>();
		Map<String,String> productIdSkuIdMap = new HashMap<String,String>();
		List<RepositoryItem> activeProductList = null;
		CompareProductInfoVO compareProductInfoVO = null;
		List<ProductInfoVO> productInfoList=new ArrayList<ProductInfoVO>();
		Map<String, String> productIdSkuItemMap = new HashMap<String, String>();
		Site site = SiteContextManager.getCurrentSite();
		boolean isValidProduct = false;
		String productCompareIds = (String) pRequest.getLocalParameter(TRURestConstants.PRODUCT_COMPARE_ID);
		String categoryId =(String) pRequest.getLocalParameter(TRURestConstants.CATEGORYID);
		TRUProductComparisonList productComparisonList =(TRUProductComparisonList) pRequest.getObjectParameter(TRURestConstants.PRODUCT_LIST);
		if (StringUtils.isNotBlank(productCompareIds)) {	
			onlinePID = productCompareIds.split(TRURestConstants.SPLIT_PIPE);
			for (int index = 0; index < onlinePID.length; index++) {
					productIdFromOnlinePID=getCatalogTools().getProductIdFromOnlinePID(onlinePID[index]);
					productIds.add(productIdFromOnlinePID);
					String addedSkuId=getCatalogTools().getSkuIdFromOnlinePID(onlinePID[index]);
					productIdSkuIdMap.put(productIdFromOnlinePID+TRUCommerceConstants.UNDER_SCORE+index, addedSkuId);
			}
		}
		 List compareAtrributeNameList= null;
		if ((productIds != null && categoryId != null) && (isProductCompareListValid(productIds, categoryId))) {
				productComparisonList.clear();
				int productCount = productIds.size();
				for (int count = 0; count < productCount; count++) {
				String indexKey = productIds.get(count) + TRUCommerceConstants.UNDER_SCORE + count;
				     String addedIndxSkuId=productIdSkuIdMap.get(indexKey);
				addProductsToCompareList(productIds.get(count), productComparisonList, categoryId, addedIndxSkuId);
				}
				// getting ACTIVE product items from comparison list
				if (productComparisonList!=null) {
				List comparisonList = (List) productComparisonList.getItems();
				activeProductList = getCatalogTools().getProductItems(comparisonList, productIdSkuItemMap, categoryId);
				if (activeProductList != null && activeProductList.size() >= TRURestConstants.INT_TWO) {
						compareProductInfoVO = getCatalogTools().createCompareProductInfo(activeProductList,Boolean.TRUE);
						if (compareProductInfoVO != null && !compareProductInfoVO.getProductInfoList().isEmpty()) {
							isValidProduct = true;
							compareAtrributeNameList= populateCompareAttributes(activeProductList, compareProductInfoVO,productInfoList, productIdSkuItemMap, site,categoryId);
					}
				}
			}
		}
		if (isValidProduct) {
			if (productInfoList != null && !productInfoList.isEmpty()) {
				compareProductInfoVO.setProductInfoList(productInfoList);
			}
			pRequest.setParameter(TRUCommerceConstants.COMPARE_PRODUCT_INFO, compareProductInfoVO);
			if (compareAtrributeNameList != null) {
				pRequest.setParameter(TRUCommerceConstants.SET_OF_ATTRIBUTENAME, compareAtrributeNameList);
				pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
			}
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}else{
			pRequest.serviceLocalParameter(TRUCommerceConstants.ERROR_OPARAM, pRequest, pResponse);
			return;	
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUMobileCompareProductInfoDroplet.service method.. ");
		}	
	}

	/**
	 * Populates Compare Attributes.
	 * 
	 * @param pActiveProductList -  ActiveProductList
	 * @param pCompareProductInfoVO - CompareProductInfoVO
	 * @param pProductInfoList -  ProductInfoList
	 * @param pProductIdSkuItemMap - ProductIdSkuItemMap
	 * @param pSite - Site
	 * @param pCategoryId - CategoryId
	 * @return List  - CompareAtrributeNameList
	 */
	private List  populateCompareAttributes(List<RepositoryItem> pActiveProductList,CompareProductInfoVO pCompareProductInfoVO,List<ProductInfoVO> pProductInfoList,Map<String, String> pProductIdSkuItemMap, Site pSite,
			String pCategoryId) {
		if (isLoggingDebug()) {
			vlogDebug("BEGIN:: TRUMobileCompareProductInfoDroplet.populateCompareAttributes method.. ");
		}
		int compareSize = pCompareProductInfoVO.getProductInfoList().size();
		 List compareAtrributeNameList=  new ArrayList<Object>();
		for (int i = TRUCommerceConstants.INT_ZERO; i < compareSize; i++){
			SKUInfoVO defaultSku = getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(
						null, pCompareProductInfoVO.getProductInfoList().get(i));
			String key = pCompareProductInfoVO.getProductInfoList().get(i).getProductId() + TRUCommerceConstants.UNDER_SCORE + i;
			String addedSkuId = pProductIdSkuItemMap.get(key);
			if (StringUtils.isNotBlank(addedSkuId) && TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE.equals(pCompareProductInfoVO.getProductInfoList().get(i).getColorSizeVariantsAvailableStatus())) {
					defaultSku = getCatalogTools().findDefaultSkuFormSkuId(pCompareProductInfoVO.getProductInfoList().get(i), addedSkuId);
			}
		     pCompareProductInfoVO.getProductInfoList().get(i).setDefaultSKU(defaultSku);
		     setItemPrice(pCompareProductInfoVO.getProductInfoList().get(i),pSite);
			 pProductInfoList.add(pCompareProductInfoVO.getProductInfoList().get(i));
			 List classificationAttributeNamesList = getCatalogTools().getAllClassificationAttributeNamesList(pCategoryId);
			 RepositoryItem activeprodItem=null;
			 for (RepositoryItem activeProduct : pActiveProductList) {
				 if (activeProduct!=null && activeProduct.getRepositoryId().equals(pCompareProductInfoVO.getProductInfoList().get(i).getProductId())) {
					activeprodItem = activeProduct;
							break;
				 }
			 }
			 Map valueMap= getCatalogTools().getCompareableProperties(activeprodItem,defaultSku,classificationAttributeNamesList);
			if (valueMap != null && !valueMap.isEmpty() && (compareAtrributeNameList == null || compareAtrributeNameList.isEmpty())) {
				compareAtrributeNameList= (List) valueMap.get(TRUCommerceConstants.SET_OF_ATTRIBUTENAME);
			}
			pCompareProductInfoVO.getProductInfoList().get(i).setComparableMap((Map) valueMap.get(TRUCommerceConstants.PRODUCT_ATTRIBUTE_NAME_VALUE_MAP));
		}	
		if (isLoggingDebug()) {
			vlogDebug("END:: TRUMobileCompareProductInfoDroplet.populateCompareAttributes method.. ");
		}
		return compareAtrributeNameList;
	}
	
	/**
	 * This method will add the given product id to the CompareList. Also it sets the categoryId to the productList's currentCategoryId.
	 * @param pProductId - Hold's the Product Id which are being added to CompareList.
	 * @param pProductComparisonList - Contains the products which are comparable.
	 * @param pCategoryID - Contains the category id.
	 * @param pSkuID - Contains the sku id.
	 */
	private void addProductsToCompareList(String pProductId,TRUProductComparisonList pProductComparisonList,
			String pCategoryID,String pSkuID) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileCompareProductInfoDroplet.addProductsToCompareList method..");
		}
		// used to set current category to comparison list
		if (StringUtils.isNotBlank(pCategoryID)) {
			pProductComparisonList.setCurrentCategoryId(pCategoryID);
		}
			if (isLoggingDebug()) {
				vlogDebug("ProductId : {0}", pProductId);
			}
			try {	
			
				pProductComparisonList.add(pProductId, pCategoryID, pSkuID, null);	
			
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("Error while adding selected product to the CompareList ", repositoryException);
				}
			}

		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileCompareProductInfoDroplet.addProductsToCompareList method..");
		}
	}
	
	/**
	 * This method used to set price for product compare.
	 * @param pProduct Product
	 * @param pSite Site
	 */
	protected void setItemPrice(ProductInfoVO pProduct, Site pSite) {
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		PriceInfoVO priceinfo = new PriceInfoVO();
		List<SKUInfoVO> childSku = new ArrayList<SKUInfoVO>();
		if (pProduct.getChildSKUsList() != null && ! pProduct.getChildSKUsList().isEmpty()) {
			for (SKUInfoVO childSkuinfo : pProduct.getChildSKUsList()) {

				listPrice = getPricingTools().getListPriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
				salePrice = getPricingTools().getSalePriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
				boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
				Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
				if (calculateSavings) {
					priceinfo.setSavedAmount(savingsMap.get(TRUConstants.SAVED_AMOUNT));
					priceinfo.setSavedPercentage(savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);
				} else {
					priceinfo.setListPrice(listPrice);
					priceinfo.setSalePrice(salePrice);

				}
				childSkuinfo.setPriceInfo(priceinfo);
				childSku.add(childSkuinfo);
				pProduct.setChildSKUsList(childSku);
			}
		}
	}
/**
 * This method should validate the product compare input.
 * @param pProductId ProductId
 * @param pCategoryId CategoryId
 * @return compareList compareList
 */
	public boolean isProductCompareListValid(List<String> pProductId,String pCategoryId) {
		boolean compareList = false;
		if (pProductId != null && pCategoryId != null) {
			if (pProductId.size() >= TRURestConstants.INT_TWO && pCategoryId != null) {
				compareList = true;
			}
			if (pProductId.size() > TRURestConstants.INT_FOUR ) {
				compareList = false;
			}
		}
		return compareList;
	}
/**
 * This method used to set the comparableList.
 * @param pCompareProductInfoVO CompareProductInfoVO
 * @param pAttributeAndValueMap AttributeAndValueMap
 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setComparableProperties(CompareProductInfoVO pCompareProductInfoVO, Map pAttributeAndValueMap) {
		List<Map<String, String>> compareList = null;
		Map<String, String> attriButeValues = null;
		if (pCompareProductInfoVO != null && pAttributeAndValueMap != null) {
			compareList = (List<Map<String, String>>) pAttributeAndValueMap.get(TRUCommerceConstants.PRODUCT_ATTRIBUTE_VALUES);
			int productInfoCount = pCompareProductInfoVO.getProductInfoList().size();
			for (int productCount = 0; productCount < productInfoCount; productCount++) {
				attriButeValues = compareList.get(productCount);
				pCompareProductInfoVO.getProductInfoList().get(productCount).setComparableMap(attriButeValues);
			}
		}
		
	}
	
	
	/**
	 * @return the mCatalogTools.
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools the mCatalogTools to set.
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}
	/**
	 * @return the mPricingTools.
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}
	/**
	 * @param pPricingTools the mPricingTools to set.
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}
	
}
