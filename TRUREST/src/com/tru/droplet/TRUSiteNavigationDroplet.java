package com.tru.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.SiteContext;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;

/**
 * This Class is intended for get the current site CatalogId based on TRU/BRU from query string.
 * 
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUSiteNavigationDroplet extends DynamoServlet{
	
	/**
	 * Property to hold TRU siteMap.
	 */
	private Map<String,String> mTruSiteMap;
	
	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 * This property hold reference for mSiteContextManager.
	 */
	private SiteContextManager mSiteContextManager;
	
	/**
	 * Used to get Site & catalog related information.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUSiteNavigationDroplet.service method..");
		}
		String siteId = null;
		String catalogId = null;
		String siteName = (String) pRequest.getLocalParameter(TRUCommerceConstants.SITE_NAME);
		if (StringUtils.isBlank(siteName)) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
			return;
		}
		else{
			siteId = getTruSiteMap().get(siteName);
		}
		if (StringUtils.isNotBlank(siteId)) {
			try {
				SiteContext siteContext = getSiteContextManager().getSiteContext(siteId);
				if(siteContext!=null){
					getSiteContextManager().pushSiteContext(siteContext);
				}
		    }
		    catch (SiteContextException e) {
		        if (isLoggingDebug()) {
		        	vlogDebug(e, "Error getting site context for site {0}.", new Object[] { siteId });
		        }
		        if (isLoggingError()) {
					logError("RepositoryException in TRUDisplayMegaMenuDroplet.service method ",e);
				}
		    }
			catalogId = getCatalogManager().getCatalogTools().getCatalogId(siteId);
		}
		if (StringUtils.isBlank(catalogId)) {
			pRequest.serviceLocalParameter(TRUCommerceConstants.EMPTY_OPARAM, pRequest, pResponse);
		}else{
			pRequest.setParameter(TRUCommerceConstants.SITE, siteId);
			pRequest.setParameter(TRUCommerceConstants.CATALOG_ID, catalogId);
			pRequest.serviceLocalParameter(TRUCommerceConstants.OUTPUT_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUSiteNavigationDroplet.service method..");
		}

	}
	
	/**
	 * @return the mTruSiteMap
	 */
	public Map<String,String> getTruSiteMap() {
		return mTruSiteMap;
	}

	/**
	 * @param pTruSiteMap the mTruSiteMap to set
	 */
	public void setTruSiteMap(Map<String,String> pTruSiteMap) {
		this.mTruSiteMap = pTruSiteMap;
	}

	/**
	 * @return the mCatalogManager
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the mCatalogManager to set
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		this.mCatalogManager = pCatalogManager;
	}
	
	/**
	 * @return the siteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}
	
}
