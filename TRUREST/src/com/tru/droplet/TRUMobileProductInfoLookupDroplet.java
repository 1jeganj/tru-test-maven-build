package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.inventory.InventoryException;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.shaded.org.apache.commons.lang.StringEscapeUtils;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.catalog.TRUCatalogManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.catalog.vo.BreadcrumbInfoVO;
import com.tru.commerce.catalog.vo.CollectionProductInfoVO;
import com.tru.commerce.catalog.vo.PriceInfoVO;
import com.tru.commerce.catalog.vo.PrimaryPathCategoryVO;
import com.tru.commerce.catalog.vo.ProductInfoVO;
import com.tru.commerce.catalog.vo.ProductPromoInfoVO;
import com.tru.commerce.catalog.vo.SKUInfoVO;
import com.tru.commerce.collection.TRUCollectionProductUtils;
import com.tru.commerce.order.TRUCommerceItemManager;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.common.TRUConstants;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.category.primarypath.TRUCategoryPrimaryPathManager;
import com.tru.endeca.urlformatter.TRUSeoUrlFormatter;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;

/**
 * This Class is TRUMobileProductInfoLookupDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0
 * 
 * 
 */
public class TRUMobileProductInfoLookupDroplet extends DynamoServlet {

	/**
	 * This property hold reference for TRUCatalogTools.
	 */
	private TRUCatalogManager mCatalogManager;
	
	/**
	 *  Holds the mPricingTools.
	 */

	private TRUPricingTools mPricingTools;
	
	/**
	 *  Holds the mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	/**
	 *  Holds the mCommerceItemManager.
	 */
	private TRUCommerceItemManager mCommerceItemManager;
	
	
	/**
	 *  Holds the mSeoUrlFormatter.
	 */
	 private TRUCatalogProperties mCatalogProperties;
	
	/**
	 * Holds the mTRURestConfiguration
	 */
	private TRURestConfiguration mRestConfiguration;
	
	/**
	 * Holds the mSiteUtil
	 */
	private TRUGetSiteTypeUtil mSiteUtil;
	
	/**
	 * Holds the mCatalogTools
	 */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * Holds the mSeoUrlFormatter
	 */
	private TRUSeoUrlFormatter mSeoUrlFormatter;
	
	/**
	 * Property to hold mCollectionUtils
	 */
	private TRUCollectionProductUtils mCollectionUtils;
	
	/**
	 * Holds the mBreadCrumbs
	 */
	private boolean mBreadCrumbsEnabled;

	/** property to hold TRUSEOManager instance. */
	private TRUSEOManager mTruSeoManager;
	
	/**
	 * Property to hold mPrimaryPathManager.
	 */
	private TRUCategoryPrimaryPathManager mPrimaryPathManager;
	
	/**
	 * Property to hold mPromotionTools.
	 */
	private TRUPromotionTools mPromotionTools;

	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.service method.. ");
		}
		boolean allSkus = false;
		boolean skuDetails = false;
		boolean isPDP = false;
		boolean isCollection = false;
		boolean isOnlinePIDRequest = false;
		String productId = null;
		boolean channelRequestType = false;
		boolean isPIDNotFoundInCurrentSite = false;
		String productItemId = null;
		String channelType = null;
		List<String> productIdList = null;
		List<String> skuIds= new ArrayList<String>();
		Map<String,List<String>> upcMap= new HashMap<String,List<String>>();
		List<String> onlinePidList = new ArrayList<String>();
		Set<String> skuSet= new HashSet<String>();
		List<ProductInfoVO> prodcutInfoList = new ArrayList<ProductInfoVO>();
		String productTypeFromInput = TRUCommerceConstants.NORMAL_PRODUCT;
		String allSku = (String) pRequest.getLocalParameter(TRURestConstants.ALL_SKUS);
		String skuDetail = (String) pRequest.getLocalParameter(TRURestConstants.SKU_DETAILS);
		String skuCode = (String) pRequest.getLocalParameter(TRURestConstants.SKU_CODE);
		String upcNumber=(String)pRequest.getLocalParameter(TRURestConstants.UPC_NUMBER);
		String onlinePIds = (String)pRequest.getLocalParameter(TRURestConstants.ONLINE_PID);
		channelType = (String) pRequest.getHeader(TRURestConstants.API_CHANNEL);
		String storeId = (String) pRequest.getLocalParameter(TRURestConstants.STORE_ID);
		CollectionProductInfoVO collectionProductInfoVO = null;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(TRURestConstants.START_SERVICE);
		}
		if (StringUtils.isNotBlank(onlinePIds) && channelType.equalsIgnoreCase(TRURestConstants.MOBILE)) {
			onlinePidList = getCatalogManager().getCatalogTools().spliteProductId(onlinePIds);
			isPIDNotFoundInCurrentSite = getCatalogManager().getCatalogTools().checkSiteValidationForPID(onlinePIds);
			if (isPIDNotFoundInCurrentSite) {
				pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.ERR_ENTER_VALID_SITE);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);	
				return;
			}
			productTypeFromInput = getCatalogManager().getCatalogTools().toFindTheProductTypeFromInput(onlinePidList);
		}
		if (StringUtils.isNotBlank(channelType) && !channelType.equalsIgnoreCase(TRURestConstants.MOBILE)) {
			channelRequestType = true;
		}
		Set<String> productSet= new HashSet<String>();
		if (TRUCommerceConstants.NORMAL_PRODUCT.equalsIgnoreCase(productTypeFromInput) && channelRequestType) {
			allSkus = Boolean.parseBoolean(allSku);
			skuDetails = Boolean.parseBoolean(skuDetail);
			if (StringUtils.isNotBlank(upcNumber)) {
				productIdList = getCatalogManager().getCatalogTools().spliteProductId(upcNumber);
				Set<String> upcSet = new HashSet<String>();
				for (String upcNo : productIdList) {
					upcMap = getCatalogManager().getCatalogTools().getProductIdFromUPCNumber(upcNo);
					Set<String> keySet=upcMap.keySet();
					for (String key : keySet) {
						List<String> skuIdSet=(List<String>) upcMap.get(key);
						if(!skuIdSet.isEmpty()){
							List<String> skuIdSetLocal=new ArrayList<String>();
							skuIdSetLocal.addAll(skuIdSet);
							skuIdSetLocal.addAll(skuIds);
							skuIds=skuIdSetLocal;
						}
					}
					upcSet.addAll(keySet);
					productSet.addAll(keySet);
				}
				productIdList = new ArrayList<String>(upcSet);
			}
			if (StringUtils.isNotBlank(skuCode)) {
				productIdList = getCatalogManager().getCatalogTools().spliteProductId(skuCode);
				Set<String> skuIdSet = new HashSet<String>();
				for (String skucodeId : productIdList) {
					productId = getCommerceItemManager().getProductIdFromSkuId(skucodeId);
					if (StringUtils.isNotBlank(productId)) {
						skuIdSet.add(productId);
						productSet.add(productId);
					}
				}
				productIdList = new ArrayList<String>(skuIdSet);
			}
			if (StringUtils.isNotBlank(upcNumber) && StringUtils.isNotBlank(skuCode) && StringUtils.isEmpty(onlinePIds)) {
				skuSet=new HashSet<String>(skuIds);
				productIdList=new ArrayList<String>(productSet);
			}
			if (StringUtils.isNotBlank(onlinePIds)) {
				isOnlinePIDRequest = true;
				productIdList = getCatalogManager().getCatalogTools().getProductIdsFromOnlinePid(onlinePIds);
			}
			if (productIdList != null && !productIdList.isEmpty()) {
				if (isLoggingDebug()) {
					logDebug("START:::Print multilple productIds:::::::::::::::::::::::::::"+ productIdList);
				}
				ProductInfoVO productInfo = null;
				for (String itemId : productIdList) {
					productItemId = itemId;
					List<String> filterSkus=null;
					productInfo = populateNormalProduct(productItemId, pRequest, channelRequestType, storeId);
					if (productInfo != null) {
						if (StringUtils.isNotBlank(skuCode) && StringUtils.isBlank(upcNumber)) {
							filterSkus=getCatalogManager().getCatalogTools().spliteProductId(skuCode);
						}
						if (StringUtils.isNotBlank(upcNumber) && StringUtils.isBlank(skuCode)) {
							filterSkus=skuIds;
						}
						if (StringUtils.isNotBlank(upcNumber) && StringUtils.isNotBlank(skuCode) && StringUtils.isEmpty(onlinePIds)) {
							filterSkus=new ArrayList<String>(skuSet);
						}
						if (productInfo.getChildSKUsList() != null && !productInfo.getChildSKUsList().isEmpty() && !isOnlinePIDRequest) {
								filterSku(productInfo, allSkus, skuDetails, filterSkus);	
							prodcutInfoList.add(productInfo);
							isPDP = true;
						} else if (productInfo.getChildSKUsList() != null && !productInfo.getChildSKUsList().isEmpty()) {
							prodcutInfoList.add(productInfo);
							isPDP = true;
						}
					}
				}
			}
		}
		if (TRUCommerceConstants.NORMAL_PRODUCT.equalsIgnoreCase(productTypeFromInput) && !channelRequestType && StringUtils.isNotBlank(onlinePIds) && !isPIDNotFoundInCurrentSite) {
			ProductInfoVO productInfo = null;
			productId = getCatalogManager().getCatalogTools().getProductIdFromOnlinePID(onlinePIds);
			if (StringUtils.isNotBlank(productId)) {
				productInfo = populateNormalProduct(productId, pRequest, channelRequestType, storeId);
			}
			if (productInfo != null && productInfo.getChildSKUsList() != null && !productInfo.getChildSKUsList().isEmpty()) {
				prodcutInfoList.add(productInfo);
				isPDP = true;
			}
		}
		if (TRUCommerceConstants.COLLECTION_PRODUCT.equalsIgnoreCase(productTypeFromInput)) {
			collectionProductInfoVO = populateCollectionProduct(onlinePIds, pRequest);
			if (collectionProductInfoVO != null) {
				isCollection = true;
			}	
		}
		if (isPDP && !channelRequestType && prodcutInfoList != null && !prodcutInfoList.isEmpty() && !isPIDNotFoundInCurrentSite) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, prodcutInfoList);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}
		if(isCollection && !channelRequestType && collectionProductInfoVO != null && !isPIDNotFoundInCurrentSite) {
			pRequest.setParameter(TRURestConstants.COLLECTION_PRODUCT_INFO, collectionProductInfoVO);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM,pRequest, pResponse);
		}
		if (isPDP && channelRequestType && prodcutInfoList != null && !prodcutInfoList.isEmpty()) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, prodcutInfoList);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM_REGISTRY, pRequest, pResponse);	
		}
		if(isCollection && channelRequestType && collectionProductInfoVO != null) {
			pRequest.setParameter(TRURestConstants.COLLECTION_PRODUCT_INFO, collectionProductInfoVO);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM_REGISTRY,pRequest, pResponse);
		}
		if (!isPDP && !isCollection) {
			pRequest.setParameter(TRURestConstants.PRODUCT_INFO, TRURestConstants.INVALID_PRODUCT_ID);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM,pRequest, pResponse);	
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(TRURestConstants.END_SERVICE);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.");
		}
	}

	/**
	 * This method will filter the PDP page response based on the flag value from the request.
	 * @param pProduct Product
	 * @param pAllSkus AllSkus
	 * @param pSkuDetails SkuDetails
	 * @param pSkuCodes SkuCodes
	 */
	protected void filterSku(ProductInfoVO pProduct, boolean pAllSkus, boolean pSkuDetails, List<String> pSkuCodes) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.filterSku method.. " +pProduct.getChildSKUsList());
		}
		SKUInfoVO skuinfo = null;
		List<SKUInfoVO> childSku = new ArrayList<SKUInfoVO>();
		if (!pSkuDetails) {
			pProduct.setChildSKUsList(null);
			pProduct.setDefaultSKU(null);
			return;
		}
		if ((pSkuDetails && !pAllSkus) && pSkuCodes != null && !pSkuCodes.isEmpty() && pProduct.getChildSKUsList() != null) {
			for (SKUInfoVO childSkuinfo : pProduct.getChildSKUsList()) {
				for (String skuCode : pSkuCodes) {	
					if (skuCode.equalsIgnoreCase(childSkuinfo.getId())) {
						skuinfo = childSkuinfo;
						pProduct.setProductStatus(skuinfo.getInventoryStatus());
						childSku.add(skuinfo);
					}
				}	
			}
			pProduct.setChildSKUsList(childSku);
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::filterSku method");
		}
	}

/**
 * This method will set the price on skuLevel for PDP page.
 * @param pProduct Product
 * @param pSite   Site
 * @throws SiteContextException 
 */
	protected void setItemPrice(ProductInfoVO pProduct, Site pSite) throws SiteContextException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.setItemPrice method.. ");
		}
		double listPrice = TRUConstants.DOUBLE_ZERO;
		double salePrice = TRUConstants.DOUBLE_ZERO;
		int relatedProductSize = TRURestConstants.INT_ZERO;
		PriceInfoVO priceinfo = null;
		String inventoryStatus = null;
		List<SKUInfoVO> childSku = new ArrayList<SKUInfoVO>();
		if (pProduct.getChildSKUsList() != null && !pProduct.getChildSKUsList().isEmpty()) {
		for (SKUInfoVO childSkuinfo : pProduct.getChildSKUsList()) {
			listPrice = getPricingTools().getListPriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
			salePrice = getPricingTools().getSalePriceForSKU(childSkuinfo.getId(), pProduct.getProductId(), pSite);
			boolean calculateSavings = getPricingTools().validateThresholdPrices(pSite, listPrice, salePrice);
			Map<String, Double> savingsMap = getPricingTools().calculateSavings(listPrice, salePrice);
			priceinfo = new PriceInfoVO();
			if (calculateSavings) {
				priceinfo.setSavedAmount(savingsMap.get(TRUConstants.SAVED_AMOUNT));
				priceinfo.setSavedPercentage(savingsMap.get(TRUConstants.SAVED_PERCENTAGE));
				priceinfo.setListPrice(listPrice);
				priceinfo.setSalePrice(salePrice);
			} else {
				priceinfo.setListPrice(listPrice);
				priceinfo.setSalePrice(salePrice);
			}
			childSkuinfo.setPriceInfo(priceinfo);
			childSku.add(childSkuinfo);	
			if (childSkuinfo.getRelatedProducts() != null && !childSkuinfo.getRelatedProducts().isEmpty()) {
				listPrice = TRUConstants.DOUBLE_ZERO;
				salePrice = TRUConstants.DOUBLE_ZERO;
				int relatedProductCount = childSkuinfo.getRelatedProducts().size();
				for (int count = TRURestConstants.INT_ZERO; count < relatedProductCount; count++) {
					listPrice = getPricingTools().getListPriceForSKU(childSkuinfo.getRelatedProducts().get(count).getSkuId(),childSkuinfo.getRelatedProducts().get(count).getProductId(), pSite);
					salePrice = getPricingTools().getSalePriceForSKU(childSkuinfo.getRelatedProducts().get(count).getSkuId(), childSkuinfo.getRelatedProducts().get(count).getProductId(), pSite);
					childSkuinfo.getRelatedProducts().get(count).setListPrice(listPrice);
					childSkuinfo.getRelatedProducts().get(count).setSalePrice(salePrice);
						inventoryStatus = getInventoryStatus(childSkuinfo.getRelatedProducts().get(count).getSkuId());
						childSkuinfo.getRelatedProducts().get(count).setProductStatus(inventoryStatus);
						if (childSkuinfo.getRelatedProducts().get(count).getChannelAvailability() == null) {
							childSkuinfo.getRelatedProducts().get(count).setChannelAvailability(TRURestConstants.CHANNEL_AVAILABILITY_STATUS);
						}
						if (childSkuinfo.getRelatedProducts().get(count).getIspu() == null) {
							childSkuinfo.getRelatedProducts().get(count).setIspu(TRURestConstants.FREE_STORE_PICK_UP_NOT_AVAILABLE);	
						}
					}
				}
				if (childSkuinfo.getBatteryProducts() != null && !childSkuinfo.getBatteryProducts().isEmpty()) {
					listPrice = TRUConstants.DOUBLE_ZERO;
					salePrice = TRUConstants.DOUBLE_ZERO;
					int batteryProductCount = childSkuinfo.getBatteryProducts().size();
					for (int count = TRURestConstants.INT_ZERO; count < batteryProductCount; count++) {
						listPrice = getPricingTools().getListPriceForSKU(childSkuinfo.getBatteryProducts().get(count).getSkuId(),childSkuinfo.getBatteryProducts().get(count).getProductId(), pSite);	
						salePrice = getPricingTools().getSalePriceForSKU(childSkuinfo.getBatteryProducts().get(count).getSkuId(),childSkuinfo.getBatteryProducts().get(count).getProductId(), pSite);
						childSkuinfo.getBatteryProducts().get(count).setListPrice(listPrice);
						childSkuinfo.getBatteryProducts().get(count).setSalePrice(salePrice);
						inventoryStatus = getInventoryStatus(childSkuinfo.getBatteryProducts().get(count).getSkuId());
						childSkuinfo.getBatteryProducts().get(count).setProductStatus(inventoryStatus);
						if (childSkuinfo.getBatteryProducts().get(count).getChannelAvailability() == null) {
							childSkuinfo.getBatteryProducts().get(count).setChannelAvailability(TRURestConstants.CHANNEL_AVAILABILITY_STATUS);
						}
						if (childSkuinfo.getBatteryProducts().get(count).getIspu() == null) {
							childSkuinfo.getBatteryProducts().get(count).setIspu(TRURestConstants.FREE_STORE_PICK_UP_NOT_AVAILABLE);	
						}	
					}
				}
				if (childSkuinfo.getChannelAvailability() == null) {
					childSkuinfo.setChannelAvailability(TRURestConstants.CHANNEL_AVAILABILITY_STATUS);
				}
				if (childSkuinfo.getIspu() == null) {
					childSkuinfo.setIspu(TRURestConstants.FREE_STORE_PICK_UP_NOT_AVAILABLE);	
				}
				if (childSkuinfo.getPromoString() != null && !childSkuinfo.getPromoString().isEmpty()) {
					getPromotionTools().setSkuPromotions(childSkuinfo, childSkuinfo.getPromoString());	
				}
				if (childSkuinfo.getRelatedProducts() != null && !childSkuinfo.getRelatedProducts().isEmpty()) {
					relatedProductSize = childSkuinfo.getRelatedProducts().size();
					for (int relCount = TRURestConstants.INT_ZERO; relCount < relatedProductSize; relCount++) {
						if(childSkuinfo.getRelatedProducts().get(relCount).getPromoString()!=null && !childSkuinfo.getRelatedProducts().get(relCount).getPromoString().isEmpty()) {
							List<ProductPromoInfoVO> promotionsList= getPromotionTools().getSkuPromotionDetails(childSkuinfo.getRelatedProducts().get(relCount).getPromoString());
							if(promotionsList!=null&& !promotionsList.isEmpty()){
								childSkuinfo.getRelatedProducts().get(relCount).setPromos(promotionsList);
							}
						}
					}
				}
			}
			pProduct.setChildSKUsList(childSku);
		}
		if (pProduct != null && pSite!=null) {
			try {
				getCatalogManager().getCatalogTools().populateThingsToKnow(pProduct, pSite.toString());
				
			} catch (RepositoryException repositoryException) {
				if (isLoggingError()) {
					logError("RepositoryException in TRUMobileProductInfoLookupDroplet.::@method::setItemPrice :",repositoryException);
				}
			}
		}	
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::setItemPrice method");
		}
	}
	
	/**
	 * This method should populate the normalProductVO
	 * @param pProductItemId ProductItemId
	 * @param pRequest Request
	 * @param pStoreId StoreId
	 * @param pChannelRequestType ChannelRequestType
	 * @return productInfoVo ProductInfoVO object
	 */
	protected ProductInfoVO populateNormalProduct(String pProductItemId, DynamoHttpServletRequest pRequest, boolean pChannelRequestType, String pStoreId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.populateNormalProduct method.. ");
		}
		Map<String, String> categoryMap = new HashMap<String, String>();
		List<BreadcrumbInfoVO> ancestorsList = new ArrayList<BreadcrumbInfoVO>();
		Site site = SiteContextManager.getCurrentSite();
		ProductInfoVO productInfoVo = null;
		String escapeHtmlFromLongDesc = null;
		try {
			productInfoVo = (getCatalogManager().createProductInfo(pProductItemId, pChannelRequestType, null,TRUCommerceConstants.TYPE_PRODUCT,null));
			if (productInfoVo == null) {
				return productInfoVo;
			}
			if (productInfoVo.getInActiveSKUsList() != null && !productInfoVo.getInActiveSKUsList().isEmpty()) {
				productInfoVo.setDefaultSKU(productInfoVo.getInActiveSKUsList().get(TRUConstants.ZERO));
			}
			SKUInfoVO defaultSku = getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(pStoreId, productInfoVo);
			if (defaultSku != null) {
				escapeHtmlFromLongDesc = StringEscapeUtils.unescapeHtml(defaultSku.getLongDescription());
				escapeHtmlFromLongDesc = escapeHtmlFromLongDesc.replaceAll(TRUCommerceConstants.GT, TRUCommerceConstants.GT_SYMBOL);
				escapeHtmlFromLongDesc = escapeHtmlFromLongDesc.replaceAll(TRUCommerceConstants.LT, TRUCommerceConstants.LT_SYMBOL);
				defaultSku.setLongDescription(escapeHtmlFromLongDesc);
				populateSEOProperties(productInfoVo,defaultSku.getOnlinePID(),productInfoVo.getSiteId()); 
				productInfoVo.setDefaultSKU(defaultSku);
				productInfoVo.setOnlinePid(productInfoVo.getDefaultSKU().getOnlinePID());
				String siteId = getProductPageUtil().getProductSite(productInfoVo.getProductId(), site.toString());
				if (siteId != null) {
					site = getSiteUtil().getSite(siteId);
				}
				productInfoVo.setProductStatus(defaultSku.getInventoryStatus());
				productInfoVo.setSiteId(site.toString());
				if (productInfoVo.getDefaultSKU().getOnlinePID() != null) {
					String pdpURL=getSiteUtil().getPDPURL(productInfoVo.getDefaultSKU().getOnlinePID());
					productInfoVo.setPdpURL(pdpURL);	
				}
				productInfoVo.setDisplayable(productInfoVo.getDefaultSKU().isDisplayable());
				productInfoVo.setSearchable(productInfoVo.getDefaultSKU().isSearchable());
			}
			if(productInfoVo != null && productInfoVo.getChildSKUsList()!=null && !productInfoVo.getChildSKUsList().isEmpty()) {
				setItemPrice(productInfoVo, site);	
			}
			if (isBreadCrumbsEnabled()
					&& productInfoVo.getDefaultSKU() != null && StringUtils.isNotBlank(productInfoVo.getDefaultSKU().getId())) {
				categoryMap = populateBreadCrumb(productInfoVo.getDefaultSKU().getId(), ancestorsList);
			}
			productInfoVo.setBreadcrumb(ancestorsList);
			if (categoryMap != null && !categoryMap.isEmpty() && productInfoVo.getDefaultSKU() != null) {
				productInfoVo.getDefaultSKU().setParentCategoryDescription(categoryMap.get(TRURestConstants.PARENT_CATEGORY_NAME));
				productInfoVo.getDefaultSKU().setParentCategoryId(categoryMap.get(TRURestConstants.PARENT_CATEGORY_ID));
			}
		} catch (RepositoryException  repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUMobileProductInfoLookupDroplet.service method ", repositoryException);
			}
			}
			catch (SiteContextException siteContextException) {
			if (isLoggingError()) {
				logError("SiteContextException in TRUMobileProductInfoLookupDroplet.service method ", siteContextException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::populateNormalProduct method");
		}
		if (!pChannelRequestType && productInfoVo != null) {
			replaceImageProperties(productInfoVo);
		}
		return productInfoVo;
	}

	/**
	 * This method should populate the collectionProductVO
	 * @param pOnlinePIds OnlinePIds 
	 * @param pRequest Request
	 * @return CollectionProductInfoVO CollectionProductInfoVO object
	 */
	protected CollectionProductInfoVO populateCollectionProduct(String pOnlinePIds, DynamoHttpServletRequest pRequest) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.populateCollectionProduct method.. ");
		}
		CollectionProductInfoVO collectionProductInfoVO = new CollectionProductInfoVO();
		List<BreadcrumbInfoVO> ancestorsList = new ArrayList<BreadcrumbInfoVO>();
		Site site = SiteContextManager.getCurrentSite();
		ProductInfoVO productInfo = new ProductInfoVO();
		int collectionCount = TRURestConstants.INT_ZERO;
		List<ProductInfoVO> prodcutInfoList = new ArrayList<ProductInfoVO>();
		try {
			collectionProductInfoVO = (getCatalogManager().createCollectionProductInfo(pOnlinePIds, true));
		if (collectionProductInfoVO != null && collectionProductInfoVO.getActiveProductInfoList() != null && !collectionProductInfoVO.getActiveProductInfoList().isEmpty()) {
			collectionCount = collectionProductInfoVO.getActiveProductInfoList().size();	
		}
		if(collectionProductInfoVO == null || collectionCount <=TRURestConstants.INT_ZERO){
			return null;
		}
		String siteId  = getProductPageUtil().getProductSite(collectionProductInfoVO.getCollectionId(), site.toString());
		Map<String,String> categoryMap=new HashMap<String,String>();
			if (isBreadCrumbsEnabled() && StringUtils.isNotBlank(collectionProductInfoVO.getChildSkuId())) {
				categoryMap = populateBreadCrumb(collectionProductInfoVO.getChildSkuId(), ancestorsList);
		}
		for (int count = TRURestConstants.INT_ZERO; count < collectionCount; count++) {
			productInfo = collectionProductInfoVO.getActiveProductInfoList().get(count);
			if (productInfo.getDefaultSKU() != null && !productInfo.getChildSKUsList().isEmpty()) {
				List<SKUInfoVO> childSkus= productInfo.getChildSKUsList();
				String productType = productInfo.getColorSizeVariantsAvailableStatus();
				if (siteId != null) {
					site = getSiteUtil().getSite(siteId);
				}
				if (childSkus != null && !childSkus.isEmpty() && childSkus.size() > TRURestConstants.INT_ONE
						&& productType.equals(TRUCommerceConstants.NO_COLOR_SIZE_AVAILABLE)) {
				for (SKUInfoVO childSku : childSkus) {
					ProductInfoVO productVo = getCollectionUtils().populateAssortementProductInfo(productInfo);
					productVo.setDefaultSKU(childSku);
					setItemPrice(productVo, site);
					productVo.setDisplayable(childSku.isDisplayable());
					productVo.setSearchable(childSku.isSearchable());
					productVo.setOnlinePid(childSku.getOnlinePID());
					if (productVo.getDefaultSKU().getOnlinePID() != null) {
						String pdpURL=getSiteUtil().getPDPURL(productVo.getDefaultSKU().getOnlinePID());
						productVo.setPdpURL(pdpURL);			
					}
							if (productVo.getDefaultSKU() != null) {
						populateSEOProperties(productVo,childSku.getOnlinePID(),collectionProductInfoVO.getSiteId());
						prodcutInfoList.add(productVo);	
							}
						}
					} else {
					SKUInfoVO defaultSku= getCatalogManager().getCatalogTools().getDefaultSkuInfoAndPopulateInvetoryInformation(null, productInfo);
					if (defaultSku != null) {
						productInfo.setDefaultSKU(defaultSku);
						setItemPrice(productInfo, site);
						productInfo.setProductStatus(defaultSku.getInventoryStatus());
						productInfo.setDisplayable(defaultSku.isDisplayable());
						productInfo.setSearchable(defaultSku.isSearchable());
						productInfo.setOnlinePid(defaultSku.getOnlinePID());
						if (productInfo.getDefaultSKU().getOnlinePID() != null) {
							String pdpURL=getSiteUtil().getPDPURL(productInfo.getDefaultSKU().getOnlinePID());
							productInfo.setPdpURL(pdpURL);			
						}
						populateSEOProperties(productInfo,defaultSku.getOnlinePID(),collectionProductInfoVO.getSiteId());
							if (productInfo.getDefaultSKU() != null) {
								replaceImageProperties(productInfo);
								prodcutInfoList.add(productInfo);
							}
						}
					}
				}
			}
		collectionProductInfoVO.setSiteId(site.toString());
		collectionProductInfoVO.setBreadcrumb(ancestorsList);
		collectionProductInfoVO.setProductInfoList(prodcutInfoList);
		for (int count = TRURestConstants.INT_ZERO; count < prodcutInfoList.size(); count++) {
			if (categoryMap != null && !categoryMap.isEmpty() && collectionProductInfoVO.getProductInfoList().get(count).getDefaultSKU() != null) {
				collectionProductInfoVO.getProductInfoList().get(count).getDefaultSKU().setParentCategoryDescription(categoryMap.get(TRURestConstants.PARENT_CATEGORY_NAME));
				collectionProductInfoVO.getProductInfoList().get(count).getDefaultSKU().setParentCategoryId(categoryMap.get(TRURestConstants.PARENT_CATEGORY_ID));
			}	
		}	
		} catch (RepositoryException repositoryException) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUMobileProductInfoLookupDroplet.populateCollectionProduct method ",repositoryException);
			}
		}
		 catch (SiteContextException siteContextException) {
				if (isLoggingError()) {
					logError("SiteContextException in TRUMobileProductInfoLookupDroplet.populateCollectionProduct method ",siteContextException);
				}	
			}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::populateCollectionProduct method");
		}
		return collectionProductInfoVO;
	}

	/**
	 * This method will populate PDP page navigation Breadcrumb state.
	 * @param pSkuId SkuId
	 * @param pAncestorsList AncestorsList
	 * @return parentCategoryMap parentCategoryMap
	 */
	protected Map<String,String> populateBreadCrumb(String pSkuId, List<BreadcrumbInfoVO> pAncestorsList)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.populateBreadCrumb method.. ");
		}
		Map<String, String> parentCatMap = null;
		if(StringUtils.isNotEmpty(pSkuId)) {
			List<PrimaryPathCategoryVO> primaryPathCategoryVOList = null;
			PrimaryPathCategoryVO categoryVO; 
			PrimaryPathCategoryVO lastCategoryVO = null; 
			String parentCategoryId = null;
			String parentCatName = null;
			parentCatMap = new HashMap<String, String>();
			primaryPathCategoryVOList = getPrimaryPathManager().populatePrimaryPathVOList(pSkuId);
			if (primaryPathCategoryVOList != null && !primaryPathCategoryVOList.isEmpty()) {
				int categoryVOSize = primaryPathCategoryVOList.size();
				for (int count = TRURestConstants.INT_ZERO; count < categoryVOSize; count++) {
					BreadcrumbInfoVO ancestors = new BreadcrumbInfoVO();
					categoryVO = (PrimaryPathCategoryVO) primaryPathCategoryVOList.get(count);
					ancestors.setNavigationState(categoryVO.getCategoryURL());
					ancestors.setLabel(categoryVO.getCategoryName());
					pAncestorsList.add(ancestors);
							}
				lastCategoryVO = (PrimaryPathCategoryVO) primaryPathCategoryVOList.get(primaryPathCategoryVOList.size()-TRURestConstants.INT_ONE);
				parentCategoryId = lastCategoryVO.getCategoryId();
				parentCatName = lastCategoryVO.getCategoryName();
				parentCatMap.put(TRURestConstants.PARENT_CATEGORY_ID, parentCategoryId);
				parentCatMap.put(TRURestConstants.PARENT_CATEGORY_NAME, parentCatName);
			}	
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::populateBreadCrumb method");
		}
		return parentCatMap;
	}
	/**
	 * Used to populate seo related properties for pdp.
	 * @param pProductInfo - holds the reference for product info vo
	 * @param pSKUId - holds the reference for sku id
	 * @param pSiteId - holds the reference for site id
	 */
	protected void populateSEOProperties(ProductInfoVO pProductInfo, String pSKUId, String pSiteId)
	{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.populateSEOProperties method.. ");
		}
		Map<String,Object> seoTagMap = null;
		seoTagMap = getTruSeoManager().fetchSEOTags(TRURestConstants.PDP_PAGE, pSKUId, pSiteId);
		if(seoTagMap!=null){
			pProductInfo.setSeoTitle((String) seoTagMap.get(TRURestConstants.PRODUCT_SEO_TITLE));
			pProductInfo.setSeoDisplayName((String) seoTagMap.get(TRURestConstants.PRODUCT_SEO_H1_TEXT));
			pProductInfo.setSeoDescription((String) seoTagMap.get(TRURestConstants.PRODUCT_SEO_DESCRIPTION));
			pProductInfo.setSeoKeywords((String) seoTagMap.get(TRURestConstants.KEYWORDS));
			pProductInfo.setSeoCanonicalURL((String) seoTagMap.get(TRURestConstants.CANONICALURL));
            pProductInfo.setSeoAlternateURL((String) seoTagMap.get(TRURestConstants.ALTURL));
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::populateSEOProperties method");
		}
	}

/**
 * This method replace the canicalImages from the catalogImage properties 
 * @param pProductInfoVO - ProductInfoVO
 */
	protected void replaceImageProperties(ProductInfoVO pProductInfoVO) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.replaceImageProperties method.. ");
		}
		int skuItemSize = TRURestConstants.INT_ZERO;
		int relatedProductSize = TRURestConstants.INT_ZERO;
		if (pProductInfoVO.getChildSKUsList() != null) {
			skuItemSize = pProductInfoVO.getChildSKUsList().size();
		}
		for (int skuCount = TRURestConstants.INT_ZERO; skuCount < skuItemSize; skuCount++) {
			if (pProductInfoVO.getChildSKUsList().get(skuCount).getPrimaryCanonicalImage() != null) {
				pProductInfoVO.getChildSKUsList().get(skuCount).setPrimaryImage(pProductInfoVO.getChildSKUsList().get(skuCount).getPrimaryCanonicalImage());	
			}
			if (pProductInfoVO.getChildSKUsList().get(skuCount).getSecondaryCanonicalImage() != null) {
				pProductInfoVO.getChildSKUsList().get(skuCount).setSecondaryImage(pProductInfoVO.getChildSKUsList().get(skuCount).getSecondaryCanonicalImage());
			}
			if (pProductInfoVO.getDefaultSKU() != null && pProductInfoVO.getDefaultSKU().getPrimaryCanonicalImage() != null) {
				pProductInfoVO.getDefaultSKU().setPrimaryImage(pProductInfoVO.getDefaultSKU().getPrimaryCanonicalImage());
			}
			if(pProductInfoVO.getDefaultSKU() != null && pProductInfoVO.getDefaultSKU().getSecondaryCanonicalImage() != null) {
				pProductInfoVO.getDefaultSKU().setSecondaryImage(pProductInfoVO.getDefaultSKU().getSecondaryCanonicalImage());
			}
			if (pProductInfoVO.getChildSKUsList().get(skuCount).getPromoString() != null && !pProductInfoVO.getChildSKUsList().get(skuCount).getPromoString().isEmpty()) {
				getPromotionTools().setSkuPromotions(pProductInfoVO.getChildSKUsList().get(skuCount), pProductInfoVO.getChildSKUsList().get(skuCount).getPromoString());	
			}
			if (pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts() != null && !pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().isEmpty()) {
				relatedProductSize = pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().size();
				for (int relCount = TRURestConstants.INT_ZERO; relCount < relatedProductSize; relCount++) {
					if(pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().get(relCount).getPromoString()!=null && !pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().get(relCount).getPromoString().isEmpty()) {
						List<ProductPromoInfoVO> promotionsList= getPromotionTools().getSkuPromotionDetails(pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().get(relCount).getPromoString());
						if(promotionsList!=null&& !promotionsList.isEmpty()){
							pProductInfoVO.getChildSKUsList().get(skuCount).getRelatedProducts().get(relCount).setPromos(promotionsList);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::replaceImageProperties method");
		}
	}
	
	/**
	 * Get the inventoryStatus for related and crossSellBattery items
	 * 
	 * @param pSkuId
	 *            - SkuId
	 * @return inventoryStatusOnline
	 */
	protected String getInventoryStatus(String pSkuId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUMobileProductInfoLookupDroplet.getInventoryStatus method.. ");
		}
		int inventoryStatusOnlineInt= TRUCommerceConstants.INT_ZERO;
		String inventoryStatusOnline = null;
		try {
			inventoryStatusOnlineInt = getCatalogTools().getCoherenceInventoryManager().queryAvailabilityStatus(pSkuId);
			if(inventoryStatusOnlineInt == TRUCommerceConstants.INT_IN_STOCK ||
					inventoryStatusOnlineInt == TRUCommerceConstants.INT_PRE_ORDER ||
					inventoryStatusOnlineInt == TRUCommerceConstants.INT_LIMITED_STOCK) {
				inventoryStatusOnline = TRUCommerceConstants.IN_STOCK;
			}else if (inventoryStatusOnlineInt == TRUCommerceConstants.INT_OUT_OF_STOCK) {			
				inventoryStatusOnline = TRUCommerceConstants.OUT_OF_STOCK;
			}
		} catch (InventoryException inventoryException) {
			if (isLoggingError()) {
			logError("InventoryException in TRUMobileProductInfoLookupDroplet.getInventoryStatus method ",inventoryException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: TRUMobileProductInfoLookupDroplet.:::::getInventoryStatus method");
		}
		return inventoryStatusOnline;
	}
	/**
	 * @return the mCatalogManager.
	 */
	public TRUCatalogManager getCatalogManager() {
		return mCatalogManager;
	}

	/**
	 * @param pCatalogManager the CatalogManager to set.
	 */
	public void setCatalogManager(TRUCatalogManager pCatalogManager) {
		this.mCatalogManager = pCatalogManager;
	}

	/**
	 * @return the mPricingTools.
	 */
	public TRUPricingTools getPricingTools() {
		return mPricingTools;
	}

	/**
	 * @param pPricingTools the PricingTools to set.
	 */
	public void setPricingTools(TRUPricingTools pPricingTools) {
		this.mPricingTools = pPricingTools;
	}

	/**
	 * @return the mProductPageUtil.
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * @param pProductPageUtil the ProductPageUtil to set.
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		this.mProductPageUtil = pProductPageUtil;
	}

	/**
	 * @return the mCommerceItemManager.
	 */
	public TRUCommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	/**
	 * @param pCommerceItemManager the CommerceItemManager to set.
	 */
	public void setCommerceItemManager(TRUCommerceItemManager pCommerceItemManager) {
		this.mCommerceItemManager = pCommerceItemManager;
	}
	
	/**
	 * @return the mCatalogProperties.
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}

	/**
	 * @param pCatalogProperties the mCatalogProperties to set.
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		this.mCatalogProperties = pCatalogProperties;
	}
	/**
	 * @return the mRestConfiguration.
	 */
	public TRURestConfiguration getRestConfiguration() {
		return mRestConfiguration;
	}

	/**
	 * @param pRestConfiguration the mRestConfiguration to set.
	 */
	public void setRestConfiguration(TRURestConfiguration pRestConfiguration) {
		this.mRestConfiguration = pRestConfiguration;
	}

	/**
	 * @return the mSiteUtil
	 */
	public TRUGetSiteTypeUtil getSiteUtil() {
		return mSiteUtil;
	}

	/**
	 * @param pSiteUtil the mSiteUtil to set
	 */
	public void setSiteUtil(TRUGetSiteTypeUtil pSiteUtil) {
		this.mSiteUtil = pSiteUtil;
	}

	/**
	 * @return the mCatalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * @return the mSeoUrlFormatter
	 */
	public TRUSeoUrlFormatter getSeoUrlFormatter() {
		return mSeoUrlFormatter;
	}

	/**
	 * @param pSeoUrlFormatter the seoUrlFormatter to set
	 */
	public void setSeoUrlFormatter(TRUSeoUrlFormatter pSeoUrlFormatter) {
		mSeoUrlFormatter = pSeoUrlFormatter;
	}

	/**
	 * @return the mBreadCrumbsEnabled
	 */
	public boolean isBreadCrumbsEnabled() {
		return mBreadCrumbsEnabled;
	}

	/**
	 * @param pBreadCrumbsEnabled the mBreadCrumbsEnabled to set
	 */
	public void setBreadCrumbsEnabled(boolean pBreadCrumbsEnabled) {
		this.mBreadCrumbsEnabled = pBreadCrumbsEnabled;
	}

	/**
	 * @return the mCollectionUtils
	 */
	public TRUCollectionProductUtils getCollectionUtils() {
		return mCollectionUtils;
	}

	/**
	 * @param pCollectionUtils the mCollectionUtils to set
	 */
	public void setCollectionUtils(TRUCollectionProductUtils pCollectionUtils) {
		this.mCollectionUtils = pCollectionUtils;
	}

	/**
	 * @return the truSeoManager
	 */
	public TRUSEOManager getTruSeoManager() {
		return mTruSeoManager;
	}

	/**
	 * @param pTruSeoManager the truSeoManager to set
	 */
	public void setTruSeoManager(TRUSEOManager pTruSeoManager) {
		mTruSeoManager = pTruSeoManager;
	}

	/**
	 * @return the primaryPathManager
	 */
	public TRUCategoryPrimaryPathManager getPrimaryPathManager() {
		return mPrimaryPathManager;
	}

	/**
	 * @param pPrimaryPathManager the mPrimaryPathManager to set
	 */
	public void setPrimaryPathManager(
			TRUCategoryPrimaryPathManager pPrimaryPathManager) {
		mPrimaryPathManager = pPrimaryPathManager;
	}

	/**
	 * @return the mPromotionTools
	 */
	public TRUPromotionTools getPromotionTools() {
		return mPromotionTools;
	}

	/**
	 * @param pPromotionTools the mPromotionTools to set
	 */
	public void setPromotionTools(TRUPromotionTools pPromotionTools) {
		mPromotionTools = pPromotionTools;
	}

	
}
