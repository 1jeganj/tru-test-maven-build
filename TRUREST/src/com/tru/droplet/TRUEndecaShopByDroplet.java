package com.tru.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.endeca.infront.content.support.XmlContentItem;
import com.tru.common.ShopBy;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.vo.ShopByDimensionVO;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is TRUEndecaShopByDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0 
 * 
 */
public class TRUEndecaShopByDroplet extends DynamoServlet {
	
	/**
	 * This property hold reference for AssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;
	
	/**
	 * This property hold reference for TRURestConfiguration.
	 */
	private TRURestConfiguration mTRURestConfiguration;
	
	/**
	 * @return the assemblerTools.
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the assemblerTools to set.
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the tRURestConfiguration.
	 */
	public TRURestConfiguration getTRURestConfiguration() {
		return mTRURestConfiguration;
	}

	/**
	 * @param pTRURestConfiguration the tRURestConfiguration to set.
	 */
	public void setTRURestConfiguration(TRURestConfiguration pTRURestConfiguration) {
		mTRURestConfiguration = pTRURestConfiguration;
	}
	
	/**
	 * Used to Render the Endeca Content Item based on input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUEndecaSearchDroplet.service method");
		}
		ContentItem contentItem = null;
		ContentItem responseContentItem = null;
		ShopBy modifiedContentItem = null;
		String action = (String) pRequest.getLocalParameter(TRURestConstants.ACTION);
		if(action.equals(TRURestConstants.SHOP_BY_BRAND)){
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesShopByBrand());
		}else if(action.equals(TRURestConstants.SHOP_BY_CHARACTER)){
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesShopByCharacter());
		}
		try {
			responseContentItem = getAssemblerTools().invokeAssembler(contentItem);
			if(responseContentItem!=null){
				modifiedContentItem = populateBean(responseContentItem);
			} else {
				pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
		}
		catch (AssemblerException e) {
			vlogError(e, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] { contentItem });
		}
		if((contentItem==null) || (modifiedContentItem==null) ){
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		else{
			String currentSiteId = SiteContextManager.getCurrentSiteId();
			if(StringUtils.isNotBlank(currentSiteId)) {
				modifiedContentItem.setSiteId(currentSiteId);
			} 
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem);
			pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUEndecaSearchDroplet.service method");
		}
	}
	
	/**
	 * Used to render typeAhead searchSuggestions for mobile.
	 *  @param <E> -  the type of elements in this collection
	 * @param pResponseContentItem - holds the reference for ResponseContentItem
	 * @return ShopBy - returns a shopBy
	 */
	@SuppressWarnings({ "unchecked" })
	private <E> ShopBy populateBean(ContentItem pResponseContentItem) {
		List<E> contentItem = (ArrayList<E>) pResponseContentItem.get(getTRURestConfiguration().getContents());
		XmlContentItem allContents = null;
		if(contentItem!=null && !contentItem.isEmpty()){
			allContents = (XmlContentItem) contentItem.get(TRURestConstants.INT_ZERO);
			if(allContents.get(TRURestConstants.MAIN_HEADER)!=null){
				ShopBy shopBy = new ShopBy();
				contentItem = (ArrayList<E>) allContents.get(TRURestConstants.MAIN_HEADER);
				int arraySize = TRURestConstants.INT_ZERO;
				if(contentItem!=null){
					arraySize=contentItem.size();
				}
				for(int iterate=TRURestConstants.INT_ZERO;iterate<arraySize;iterate++){
					if(contentItem.get(iterate) instanceof BasicContentItem && ((String)((BasicContentItem)contentItem.get(iterate)).get(getTRURestConfiguration().getAtType())).equals(TRURestConstants.SHOP_BY_DIMENSION)){
						BasicContentItem shopByDimension = (BasicContentItem)contentItem.get(iterate);
						shopBy.setDimensionList((List<ShopByDimensionVO>) shopByDimension.get(TRURestConstants.DIMENSION_LIST));
					}
				}
				return shopBy;
			}
		}
		return null;
	}

	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}

}
