package com.tru.droplet;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.ContentSlotConfig;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import com.tru.commerce.endeca.cache.TRUDimensionValueCache;
import com.tru.common.ProductList;
import com.tru.common.RemoveAllAction;
import com.tru.common.TRURestConfiguration;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.endeca.utils.TRUEndecaConfigurations;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.PopulateBeanFromContentItem;

/**
 * This class is TRUGetEligibleItemsDroplet which will get used from REST Actor.
 * @author PA
 * @version 1.0
 * 
 */
public class TRUGetEligibleItemsDroplet extends DynamoServlet {
	
	/**
	 * This property hold reference for AssemblerTools.
	 */
	private AssemblerTools mAssemblerTools;
	
	/**
	 * This property hold reference for TRURestConfiguration.
	 */
	
	private TRURestConfiguration mTRURestConfiguration;
	
	/**
	 * This property hold reference dimensionValueCache.
	 */
	private TRUDimensionValueCache mDimensionValueCache;
		
	/**
	 * This property hold reference populateBeanFromContentItem.
	 */
	@SuppressWarnings("rawtypes")
	private PopulateBeanFromContentItem mPopulateBeanFromContentItem;

	/**
	 * @return the assemblerTools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param pAssemblerTools the assemblerTools to set
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		mAssemblerTools = pAssemblerTools;
	}

	/**
	 * @return the TRURestConfiguration
	 */
	public TRURestConfiguration getTRURestConfiguration() {
		return mTRURestConfiguration;
	}

	/**
	 * @param pTRURestConfiguration the TRURestConfiguration to set
	 */
	public void setTRURestConfiguration(TRURestConfiguration pTRURestConfiguration) {
		mTRURestConfiguration = pTRURestConfiguration;
	}
	
	/**
	 * Gets the dimensionValueCache
	 * @return the dimensionValueCache
	 */
	public TRUDimensionValueCache getDimensionValueCache() {
		return mDimensionValueCache;
	}

	/**
	 * Sets the dimensionValueCache
	 * @param pDimensionValueCache the dimensionValueCache to set
	 */
	public void setDimensionValueCache(TRUDimensionValueCache pDimensionValueCache) {
		mDimensionValueCache = pDimensionValueCache;
	}
	
	/** Property Holds endecaConfigurations. */
	private TRUEndecaConfigurations mEndecaConfigurations;

	/**
	 * Returns Endeca Configurations.
	 *
	 * @return the EndecaConfigurations
	 */
	public TRUEndecaConfigurations getEndecaConfigurations() {
		return mEndecaConfigurations;
	}
	/**
	 * sets Endeca Configurations.
	 *
	 * @param pEndecaConfigurations the EndecaConfigurations to set
	 */
	public void setEndecaConfigurations(TRUEndecaConfigurations pEndecaConfigurations) {
		mEndecaConfigurations = pEndecaConfigurations;
	}
	/**
	 * Used to Render the Endeca Content Item based on input parameter.
	 * 
	 * @param pRequest
	 *         - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *         - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *         - ServletException
	 * @throws IOException
	 *         - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into the TRUGetEligibleItemsDroplet.service method");
		}
		ContentItem contentItem = null;
		ContentItem promoResponseContentItem = null;
		ProductList modifiedContentItem = null;
		StringBuffer addNrppParam = new StringBuffer();
		String navigationState = (String) pRequest.getLocalParameter(TRURestConstants.NAVIGATIONSTATE);
		String recordsPerPage = (String) pRequest.getLocalParameter(TRURestConstants.REC_LIMIT);
		String offset = (String) pRequest.getLocalParameter(TRURestConstants.OFFSET);
		String tempNavigation;
		RemoveAllAction removeAllAction=new RemoveAllAction();
		String refineId = null;
		boolean isGiftItem = false;
		String promoId = (String)pRequest.getLocalParameter(TRURestConstants.PROMOID);
		isGiftItem =Boolean.parseBoolean((String)pRequest.getLocalParameter(TRURestConstants.GIFTITEM));
		String pageOutput = (String) pRequest.getLocalParameter(TRURestConstants.PAGE_OUTPUT);
		String channelType = (String) pRequest.getHeader(TRURestConstants.API_CHANNEL);
		String storeId = (String) pRequest.getLocalParameter(TRURestConstants.STORE_ID);
		boolean priceEnableFlag = false;
		priceEnableFlag =((Boolean)pRequest.getLocalParameter(TRURestConstants.PRICE_ENABLE));
		String promoType = null;
		if(StringUtils.isNotBlank(pRequest.getContentType())&&pRequest.getContentType().contains(TRURestConstants.CONTENT_TYPE_URL)&&StringUtils.isNotBlank(navigationState)){
			tempNavigation = navigationState.replaceAll(TRURestConstants.EMPERSAND, TRURestConstants.AND);
			navigationState = tempNavigation;
		}
		if(StringUtils.isNotBlank(promoId)) {
			if (isLoggingDebug()) {
				logDebug("Promotion/GWP Id as passed by the user:" + promoId);
			}
			DimensionValueCacheObject dimCacheObjectforPromo=null;
			if(isGiftItem) {
				dimCacheObjectforPromo = getPromoDimCacheObject(TRUEndecaConstants.GWP_PROMOTION_UNDERSCORE,promoId);
			} else {
				dimCacheObjectforPromo = getPromoDimCacheObject(TRUEndecaConstants.PROMOTION_UNDERSCORE,promoId);
			}
			if(dimCacheObjectforPromo==null){
				pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			String dimValId = dimCacheObjectforPromo.getDimvalId();
			if(isGiftItem) {
				addNrppParam.append(TRURestConstants.NAVIGATION_EQUAL).append(dimValId).append(TRURestConstants.AND_GWPPROMOID_EQUAL).append(promoId);
				removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+dimValId+TRURestConstants.AND_GWPPROMOID_EQUAL+promoId);
			} else {
				addNrppParam.append(TRURestConstants.NAVIGATION_EQUAL).append(dimValId).append(TRURestConstants.AND_PROMOID_EQUAL).append(promoId);
				removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+dimValId+TRURestConstants.AND_PROMOID_EQUAL+promoId);
			}
			pRequest.setQueryString(addNrppParam.toString());
			contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesPromotion());
		}else if(StringUtils.isNotBlank(navigationState)){
			if(isLoggingDebug()){
				logDebug("NavigationState is not Empty. This might be filter, clearFilter, clearAllFilter or load more product request." + navigationState);
			}
			tempNavigation=URLDecoder.decode(navigationState, TRURestConstants.UTF_EIGHT);
			String[] breakNS=tempNavigation.substring(TRURestConstants.INT_ONE).split(TRURestConstants.AND);
			String[] params;
			int iterate=TRURestConstants.INT_ZERO;
			for(int size=breakNS.length;iterate<size;iterate++){
				if(iterate+TRURestConstants.INT_ONE<size && !breakNS[iterate+TRURestConstants.INT_ONE].contains(TRURestConstants.EQUALS)) {
					StringBuffer breakNSBuffer = new StringBuffer();
					breakNSBuffer.append(breakNS[iterate]).append(TRURestConstants.ENCODED_AMPERSAND).append(breakNS[iterate+TRURestConstants.INT_ONE]);
					breakNS[iterate] = breakNSBuffer.toString();
				}
				if(breakNS[iterate].contains(TRURestConstants.EQUALS)){
					params=breakNS[iterate].split(TRURestConstants.EQUALS);
					if(!params[TRURestConstants.INT_ZERO].equals(TRURestConstants.REC_LIMIT)&&!params[TRURestConstants.INT_ZERO].equals(TRURestConstants.OFFSET)){
						pRequest.setParameter(params[TRURestConstants.INT_ZERO], params[TRURestConstants.INT_ONE]);
						addNrppParam.append(breakNS[iterate]).append(TRURestConstants.AND);
					}
				}
				if(breakNS[iterate].contains(TRURestConstants.PROMOID_EQUAL) || breakNS[iterate].contains(TRURestConstants.GWPPROMOID_EQUAL)){
					params=breakNS[iterate].split(TRURestConstants.EQUALS);
					String promo = params[TRURestConstants.INT_ZERO];
					String promotionId = params[TRURestConstants.INT_ONE];
					if(promo.equalsIgnoreCase(TRURestConstants.PROMOID_PARAM)) {
						promoType = TRUEndecaConstants.PROMOTION_UNDERSCORE;
					} else {
						promoType = TRUEndecaConstants.GWP_PROMOTION_UNDERSCORE;
					}
					DimensionValueCacheObject dimCacheObjPromo=getPromoDimCacheObject(promoType,promotionId);
					if(dimCacheObjPromo==null){
						pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
						pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
						return;
					}
					refineId = getPromoDimCacheObject(promoType,promotionId).getDimvalId();

					removeAllAction.setNavigationState(TRURestConstants.N_VALUE+TRURestConstants.EQUALS+refineId+TRURestConstants.AND+breakNS[iterate]);
					if(getDimensionValueCache().getCachedObjectForDimval(refineId)!=null){
						refineId=getDimensionValueCache().getCachedObjectForDimval(refineId).getRepositoryId();
					}
				}	
			}
			if(StringUtils.isNotBlank(recordsPerPage)){
				addNrppParam.append(TRURestConstants.NRPP_PARAM).append(recordsPerPage).append(TRURestConstants.AND);
				pRequest.setParameter(TRURestConstants.REC_LIMIT, recordsPerPage);
			}
			if(StringUtils.isNotBlank(offset)){
				addNrppParam.append(TRURestConstants.NO_PARAM).append(offset).append(TRURestConstants.AND);
				pRequest.setParameter(TRURestConstants.OFFSET, offset);
			}
			pRequest.setQueryString(addNrppParam.deleteCharAt(addNrppParam.length()-TRURestConstants.INT_ONE).toString());
			if(StringUtils.isNotBlank(recordsPerPage)&&StringUtils.isNotBlank(offset)){
				contentItem = createContentInclude(pRequest, getTRURestConfiguration().getPagesAjaxResultlist());
			}else{
				contentItem = createContentSlotConfig(pRequest, getTRURestConfiguration().getContentAjaxknockoutCollection());
			}
		}else{
			if (isLoggingDebug()) {
				logDebug("Resulting into Error Message As No required parameter passed");
			}
			pRequest.setParameter(TRURestConstants.ERROR_INFO, TRURestConstants.INPUT_VALUES);
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		try {
			promoResponseContentItem = getAssemblerTools().invokeAssembler(contentItem);
			if(promoResponseContentItem!=null){
				modifiedContentItem = getPopulateBeanFromContentItem().populateBean(promoResponseContentItem, refineId, TRURestConstants.PROMO_OUTPUT, storeId, priceEnableFlag);
				modifiedContentItem.setRemoveAllAction(removeAllAction);
			} else {
				pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
		}
		catch (AssemblerException e) {
			vlogError(e, "An exception occurred invoking the assembler with ContentItem {0}.", new Object[] { contentItem });
		}
		if(modifiedContentItem==null){
			pRequest.setParameter(TRURestConstants.ERROR_INFO,TRURestConstants.TRY_LATER );
			pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		else{
			pRequest.setParameter(TRURestConstants.CONTENT_ITEM, modifiedContentItem);
			if(getEndecaConfigurations().isDisableCMSSpots() && StringUtils.isNotEmpty(channelType) && getEndecaConfigurations().getCMSSpotsDisabledChannelList().contains(channelType)){
				pageOutput = TRURestConstants.NON_CMSSPOT.concat(pageOutput);
			}
			pRequest.serviceLocalParameter(pageOutput, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUGetEligibleItemsDroplet.service method");
		}
	}
	
	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResourcePath - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath)
	{
		return new RedirectAwareContentInclude(pResourcePath);
	}
	/**
	 * Used to redirect the request.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pContent - holds the reference for pResourcePath
	 * @return ContentItem - returns a ContentItem
	 */
	protected ContentSlotConfig createContentSlotConfig(DynamoHttpServletRequest pRequest, String pContent)
	{
		return new ContentSlotConfig(pContent,TRURestConstants.INT_ONE);
	}
	/**
	 * @return the populateBeanFromContentItem
	 */
	@SuppressWarnings("rawtypes")
	public PopulateBeanFromContentItem getPopulateBeanFromContentItem() {
		return mPopulateBeanFromContentItem;
	}
	/**
	 * @param pPopulateBeanFromContentItem the populateBeanFromContentItem to set
	 */
	@SuppressWarnings("rawtypes")
	public void setPopulateBeanFromContentItem(PopulateBeanFromContentItem pPopulateBeanFromContentItem) {
		mPopulateBeanFromContentItem = pPopulateBeanFromContentItem;
	}
	/**
	 * get DimensionValueCacheObject from promotion id and promotion type.
	 * @param pPromoId - promotion id
	 * @param pPromoType - promotion type
	 * @return DimensionValueCacheObject - dimCacheObjectforPromo 
	 */
	public DimensionValueCacheObject getPromoDimCacheObject(String pPromoType, String pPromoId) {
		TRUDimensionValueCache dimCacheTools = getDimensionValueCache();
		DimensionValueCacheObject dimCacheObjectforPromo = dimCacheTools.getCacheObject(pPromoType+pPromoId);
		return dimCacheObjectforPromo;
	}
	

}
