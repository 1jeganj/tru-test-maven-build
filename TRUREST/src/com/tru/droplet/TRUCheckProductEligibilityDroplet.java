package com.tru.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.cart.helper.TRUCartModifierHelper;
import com.tru.common.TRUConstants;
import com.tru.common.TRURestConfiguration;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUApplePayUtils;

/**
 * This class is TRUCheckProductEligibilityDroplet which will get used from REST
 * Actor.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRUCheckProductEligibilityDroplet extends DynamoServlet {

	/** The TRU cart modifier helper. */
	private TRUCartModifierHelper mCartModifierHelper;

	/** Property To property manager. */
	private TRUCommercePropertyManager mPropertyManager;
	/**
	 * Holds the mApplePayUtils
	 */
	private TRUApplePayUtils mApplePayUtils;
	/**
	 * This property hold reference for TRURestConfiguration.
	 */
	
	private TRURestConfiguration mConfiguration;


	/**
	 * Gets the cart modifier helper.
	 * 
	 * @return the cart modifier helper
	 */
	public TRUCartModifierHelper getCartModifierHelper() {
		return mCartModifierHelper;
	}

	/**
	 * Sets the cart modifier helper.
	 * 
	 * @param pCartModifierHelper
	 *            the new cart modifier helper
	 */
	public void setCartModifierHelper(TRUCartModifierHelper pCartModifierHelper) {
		this.mCartModifierHelper = pCartModifierHelper;
	}

	/**
	 * Gets the property manager.
	 * 
	 * @return the property manager
	 */
	public TRUCommercePropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * Sets the property manager.
	 * 
	 * @param pPropertyManager
	 *            the new property manager
	 */
	public void setPropertyManager(TRUCommercePropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mApplePayUtils
	 */
	public TRUApplePayUtils getApplePayUtils() {
		return mApplePayUtils;
	}

	/**
	 * @param pApplePayUtils
	 *            the ApplePayUtils to set
	 */
	public void setApplePayUtils(TRUApplePayUtils pApplePayUtils) {
		mApplePayUtils = pApplePayUtils;
	}

	
	/**
	 * @return the configuration
	 */
	public TRURestConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRURestConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * Used to Render the Cart/Product Eligibility for ApplePay.
	 * 
	 * @param pRequest
	 *            - holds the reference for DynamoHttpServletRequest
	 * @param pResponse
	 *            - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException
	 *             - ServletException
	 * @throws IOException
	 *             - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
	if (isLoggingDebug()) {
			logDebug("Entering into the TRUCheckProductEligibilityDroplet.service method");
	}
		boolean cartEligibility = Boolean.parseBoolean((String)pRequest.getLocalParameter(TRURestConstants.CART_ELIGIBLE));
		String productId = (String) pRequest.getLocalParameter(TRURestConstants.PRODUCTID);
		String skuId = (String) pRequest.getLocalParameter(TRURestConstants.SKUID);
		Order order=(Order) pRequest.getObjectParameter(TRUConstants.CURRENT_ORDER);
		boolean applePayEligible = false;
		if (isLoggingDebug()) {
			logDebug("Input values for droplet skuId is : "+skuId+"and product Id is: " +productId);
		}
		if(cartEligibility) {
			if(order == null || order.getCommerceItems().isEmpty()) {
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			applePayEligible = getApplePayUtils().getApplePayCartEligible(order);
		} else {
			if(StringUtils.isBlank(skuId) || StringUtils.isBlank(productId)) {
				pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
				return;
			}
			if(!getApplePayUtils().isCollectionProduct(skuId)) {
				applePayEligible = getApplePayUtils().getApplePayEligible(skuId);
			}	
		}
		pRequest.setParameter(TRURestConstants.APPLE_PAY_ELIGIBLE, applePayEligible);
		pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exiting from the TRUCheckProductEligibilityDroplet.service method");
		}
	}
}
