package com.tru.droplet;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUErrorKeys;
import com.tru.rest.constants.TRURestConstants;

/**
 * Class TRUGetEditShipAddressDroplet.
 * @author Professional Access
 * @version  : 1.0
 * 
 */
public class TRUGetEditShipAddressDroplet extends DynamoServlet {
	
	/**
	 * Used to get shipping address using nick name.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for pResourcePath
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:TRUGetEditShipAddressDroplet.service method");
		}
		String nickName = (String) pRequest.getLocalParameter(TRURestConstants.NICKNAME);
		Map addressMap=(Map) pRequest.getLocalParameter(TRURestConstants.ADDRESSMAP);
		
		Object address=null;
		boolean handled=Boolean.FALSE;
		boolean addressFound=Boolean.FALSE;
		if(StringUtils.isEmpty(nickName)){
			pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_EMPTY_NICKNAME);
			handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		if(addressMap==null || addressMap.isEmpty()){
			pRequest.setParameter(TRURestConstants.ERROR_MSG,TRUErrorKeys.TRU_ERROR_ACCOUNT_NO_AVAILABLE_ADDR);
			handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		Set<String> keySet=addressMap.keySet();
		if(keySet!=null && !keySet.isEmpty()){
		for (String key : keySet) {
			if(key.equals(nickName)){
				addressFound=Boolean.TRUE;
				address=addressMap.get(key);
				break;
			}
		}
		}
		if(addressFound && !handled){
			pRequest.setParameter(TRURestConstants.ADDRESS, address);
			handled=pRequest.serviceLocalParameter(TRURestConstants.OUTPUT_PARAM, pRequest, pResponse);
		}else{
			pRequest.setParameter(TRURestConstants.ERROR_MSG, TRUErrorKeys.TRU_ERROR_ACCOUNT_ADDR_INVALID_NICKNAME);
			handled=pRequest.serviceLocalParameter(TRURestConstants.EMPTY_PARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("END:TRUGetEditShipAddressDroplet.service method");
		}
	}
}
