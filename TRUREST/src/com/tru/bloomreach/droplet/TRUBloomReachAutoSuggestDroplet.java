package com.tru.bloomreach.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tru.bloomreach.constant.TRUBloomReachConstants;
import com.tru.bloomreach.service.TRUBloomReachUtil;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is intended for creating the BloomReach Serch response.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>visitor_id</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>OMNITURE_USER</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/bloomreach/droplet/BloomReachAutoSuggestDroplet"&gt;<br>
 *          &lt;dspel:param name="visitor_id" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 * @param <E>
 */
public class TRUBloomReachAutoSuggestDroplet<E> extends DynamoServlet{
	/**
	 * This property hold reference for BloomReachUtil.
	 */
	private TRUBloomReachUtil<E> mBloomReachUtil;
	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@SuppressWarnings("static-access")
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBloomReachAutoSuggestDroplet;  method: service]");
			vlogDebug("Request : {0},response : {1}", pRequest, pResponse);
		}
		String brUID = (String) pRequest.getLocalParameter(TRUBloomReachConstants._BR_UID_2);
		String query = (String) pRequest.getLocalParameter(TRUBloomReachConstants.CUSTOMER_QUERY);
		String requestOrginURL = (String) pRequest.getLocalParameter(TRUBloomReachConstants.REQUEST_ORGIN_URL);
		String queryURL = null;
		String autoSuggestJSON = null;
		if (StringUtils.isBlank(query) || StringUtils.isBlank(requestOrginURL)) {
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, TRUBloomReachConstants.CHECK_INPUT_PARAM);
			pRequest.serviceLocalParameter(TRUBloomReachConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		Map<String, Object> formatedObj = null;
		List<Object> properyList = new ArrayList<Object>();
		queryURL = getBloomReachUtil().constructTypeAheadQuery(query, brUID ,requestOrginURL);
		autoSuggestJSON = getBloomReachUtil().getResponse(queryURL);
		if (StringUtils.isNotBlank(autoSuggestJSON)) {
			JsonParser parser = new JsonParser();
			JsonElement parse = parser.parse(autoSuggestJSON);
			JsonObject asJsonObject = parse.getAsJsonObject();
			formatedObj = getBloomReachUtil().toMap(asJsonObject);
			if(formatedObj!=null && !formatedObj.isEmpty()){
				properyList.add(formatedObj);	
			}
		}
		if (properyList != null && properyList.size() > TRURestConstants.INT_ZERO) {
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, properyList.get(TRURestConstants.INT_ZERO));
			pRequest.serviceLocalParameter(TRUBloomReachConstants.OUTPUT, pRequest, pResponse);		
		} else {
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, TRUBloomReachConstants.TRY_LATER);
			pRequest.serviceLocalParameter(TRUBloomReachConstants.EMPTY_PARAM, pRequest, pResponse);	
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUBloomReachAutoSuggestDroplet  method: service]");
		}
	}

	/**
	 * @return the mBloomReachUtil
	 */
	public TRUBloomReachUtil<E> getBloomReachUtil() {
		return mBloomReachUtil;
	}

	/**
	 * @param pBloomReachUtil the mBloomReachUtil to set
	 */
	public void setBloomReachUtil(TRUBloomReachUtil<E> pBloomReachUtil) {
		this.mBloomReachUtil = pBloomReachUtil;
	}

}
