package com.tru.bloomreach.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tru.bloomreach.bean.SearchResultBean;
import com.tru.bloomreach.constant.TRUBloomReachConstants;
import com.tru.bloomreach.service.TRUBloomReachUtil;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is intended for creating the BloomReach Serch response.
 * 
 * 
 * @author PA
 * @version 1.0 <br>
 * <br>
 *          <b>Input Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>visitor_id</code> - String<br>
 *          <b>Output Parameters</b><br>
 *          &nbsp;&nbsp;&nbsp;<code>OMNITURE_USER</code>
 * 
 *          &nbsp;&nbsp;&nbsp;<b>output - .</b> <br>
 *          <b>Usage:</b><br>
 *          &lt;dspel:droplet name="/com/tru/bloomreach/droplet/TRUBloomReachSearchDroplet"&gt;<br>
 *          &lt;dspel:param name="visitor_id" &gt;<br>
 *          &lt;dspel:oparam name="output"&gt;<br>
 *          &lt;/dspel:oparam&gt;<br>
 *          &lt;/dspel:droplet&gt;
 * @param <E>
 */
public class TRUBloomReachSearchDroplet<E> extends DynamoServlet {

	/**
	 * This property hold reference for BloomReachUtil.
	 */
	private TRUBloomReachUtil<E> mBloomReachUtil;
	
	/**
	 * This property hold reference for FetchPriceAndInventory.
	 */
	private boolean mFetchPriceAndInventory;
	
	/**
	 * Used to Render the JSON Response for mobile & registry.
	 * @param pRequest - holds the reference for DynamoHttpServletRequest
	 * @param pResponse - holds the reference for DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException
	 */
	@SuppressWarnings({ "unused", "static-access", "unchecked" })
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBloomReachSearchDroplet;  method: service]");
			vlogDebug("Request : {0},response : {1}", pRequest, pResponse);
		}
		String queryURL = null;
		String searchResponse = null;
		SearchResultBean searchResult = new SearchResultBean();
		Map<String, Object> formatedObj = null;
		List<Object> properyList = new ArrayList<Object>();
		String query = (String) pRequest.getLocalParameter(TRUBloomReachConstants.CUSTOMER_QUERY);
		String requestOrginURL = (String) pRequest.getLocalParameter(TRUBloomReachConstants.REQUEST_ORGIN_URL);
		String pageNumbers = (String) pRequest.getLocalParameter(TRUBloomReachConstants.PAGE_START);
		String totalRows = (String) pRequest.getLocalParameter(TRUBloomReachConstants.ROW_NUMBERS);
		String brUID = (String) pRequest.getLocalParameter(TRUBloomReachConstants._BR_UID_2);
		String filterQuery = (String) pRequest.getLocalParameter(TRUBloomReachConstants.SEARCH_FILTER_QUERY);
		String complexQuery = (String) pRequest.getLocalParameter(TRUBloomReachConstants.COMPLEX_SEARCH_FILTER);
		String sortOption = (String) pRequest.getLocalParameter(TRUBloomReachConstants.SORT_OPTIONS);
		String storeId = (String) pRequest.getLocalParameter(TRURestConstants.STORE_ID);
		if(StringUtils.isBlank(query) || StringUtils.isBlank(pageNumbers) || StringUtils.isBlank(totalRows) || StringUtils.isBlank(brUID) || StringUtils.isBlank(requestOrginURL)) {
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, TRUBloomReachConstants.CHECK_INPUT_PARAM);
			pRequest.serviceLocalParameter(TRUBloomReachConstants.EMPTY_PARAM, pRequest, pResponse);
			return;
		}
		queryURL = getBloomReachUtil().constructSearchQuery(query, pageNumbers, totalRows, brUID, filterQuery, sortOption, complexQuery, requestOrginURL);
		if (StringUtils.isNotEmpty(queryURL)) {
			searchResponse = getBloomReachUtil().getResponse(queryURL);	
			if (StringUtils.isNotEmpty(searchResponse)) {
				JsonParser parser = new JsonParser();
				JsonElement parse = parser.parse(searchResponse);
				JsonObject asJsonObject = parse.getAsJsonObject();
				formatedObj = getBloomReachUtil().toMap(asJsonObject);
				Map<Object,Object> responseObj = (Map<Object, Object>) formatedObj.get(TRURestConstants.BM_RESPONSE_NODE);
				formatedObj.remove(TRURestConstants.BM_RESPONSE_NODE);
				properyList.add(formatedObj);
				if (isFetchPriceAndInventory() && formatedObj != null && !formatedObj.isEmpty()) {
					if (properyList != null && properyList.size() > TRURestConstants.INT_ZERO){
						searchResult.setNavigationInfo(properyList.get(TRURestConstants.INT_ZERO));	
					}
					getBloomReachUtil().populateProduct(searchResult, storeId, responseObj);
					searchResult.getResponse().setRows(Integer.parseInt(totalRows));
					searchResult.getResponse().setStart(Integer.parseInt(pageNumbers));
				}
			}
		}	
		if (searchResult != null) {	
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, searchResult);
			pRequest.serviceLocalParameter(TRUBloomReachConstants.OUTPUT, pRequest, pResponse);		
		}else{
			pRequest.setParameter(TRUBloomReachConstants.SEARCH_INFO, TRUBloomReachConstants.TRY_LATER);
			pRequest.serviceLocalParameter(TRUBloomReachConstants.EMPTY_PARAM, pRequest, pResponse);	
		}
		if (isLoggingDebug()) {
			logDebug("Exit into [Class: TRUBloomReachSearchDroplet  method: service]");
		}
	}
		
	/**
	 * @return the mBloomReachUtil
	 */
	public TRUBloomReachUtil<E> getBloomReachUtil() {
		return mBloomReachUtil;
	}

	/**
	 * @param pBloomReachUtil the mBloomReachUtil to set
	 */
	public void setBloomReachUtil(TRUBloomReachUtil<E> pBloomReachUtil) {
		this.mBloomReachUtil = pBloomReachUtil;
	}

	/**
	 * @return the fetchPriceAndInventory
	 */
	public boolean isFetchPriceAndInventory() {
		return mFetchPriceAndInventory;
	}

	/**
	 * @param pFetchPriceAndInventory the fetchPriceAndInventory to set
	 */
	public void setFetchPriceAndInventory(boolean pFetchPriceAndInventory) {
		mFetchPriceAndInventory = pFetchPriceAndInventory;
	}	
}
