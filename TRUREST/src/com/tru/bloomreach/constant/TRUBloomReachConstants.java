package com.tru.bloomreach.constant;

import atg.nucleus.naming.ParameterName;

/**
 *  This class hold all BloomReach Constant
 *  @author Professional Access
 * 	@version 1.0
 */
public class TRUBloomReachConstants {
	
	/**
	 * Constant for CUSTOMER_QUERY.
	 */
	public static final ParameterName CUSTOMER_QUERY = ParameterName.getParameterName("q");
	
	/**
	 * Constant for ROW_NUMBERS.
	 */
	public static final ParameterName ROW_NUMBERS = ParameterName.getParameterName("rows");
	
	/**
	 * Constant for PAGE_START.
	 */
	public static final ParameterName PAGE_START = ParameterName.getParameterName("start");
	
	/**
	 * Constant for _BR_UID_2.
	 */
	public static final ParameterName _BR_UID_2 = ParameterName.getParameterName("bruid");
	
	/**
	 * Constant for SEARCH_FILTER_QUERY.
	 */
	public static final ParameterName SEARCH_FILTER_QUERY = ParameterName.getParameterName("fq");
	
	/**
	 * Constant for COMPLEX_SEARCH_FILTER.
	 */
	public static final ParameterName COMPLEX_SEARCH_FILTER = ParameterName.getParameterName("efq");
	
	
	
	/**
	 * Constant for SORT_OPTIONS.
	 */
	public static final ParameterName SORT_OPTIONS = ParameterName.getParameterName("sort");
	
	/**
	 * Constant for SORT_OPTIONS.
	 */
	public static final ParameterName FACET_QUERY = ParameterName.getParameterName("facetRange");
	
	/**
	 * Constant for SORT_OPTIONS.
	 */
	public static final ParameterName REQUEST_ORGIN_URL = ParameterName.getParameterName("url");
	
	
	/**
	 * Constant for REQUEST_TYPE.
	 */
	public static final String REQUEST_TYPE = "&request_type=";
	
	/**
	 * Constant for SEARCH_TYPE.
	 */
	public static final String SEARCH_TYPE = "&search_type=";
	
	/**
	 * Constant for REALM.
	 */
	public static final String REALM = "&realm=";
	
	/**
	 * Constant for BR_ORIGN.
	 */
	public static final String BR_ORIGN = "&br_origin=";
	
	/**
	 * Constant for OUT_PUT_PROPERTIESc.
	 */
	public static final String OUT_PUT_PROPERTIES = "&fl=";
	
	/**
	 * Constant for INPUT_QUERY.
	 */
	public static final String INPUT_QUERY = "&q=";
	
	/**
	 * Constant for URL.
	 */
	public static final String URL = "&url=";
	
	/**
	 * Constant for START_PAGE.
	 */
	public static final String START_PAGE = "&start=";
	
	/**
	 * Constant for ROWS.
	 */
	public static final String ROWS = "&rows=";
	
	/**
	 * Constant for RB_UID.
	 */
	public static final String RB_UID = "&_br_uid_2=";
	
	/**
	 * Constant for SORT_RESULT.
	 */
	public static final String SORT_RESULT = "&sort=";
	
	/**
	 * Constant for Allow EMPTY_PARAM.
	 */

	public static final String EMPTY_PARAM = "empty";

	/**
	 * Constant for OUTPUT.
	 */
	public static final String OUTPUT = "output";
	
	/** Constant to hold SEARCH_INFO. */
	public static final String SEARCH_INFO = "productInfo";
	
	/**
	 * property to hold TRY_LATER.
	 */
	public static final String TRY_LATER = "System Error";
	
	/**
	 * property to hold SPLIT_COMMA.
	 */
	public static final String SPLIT_COMMA = "\\,";

	/**
	 * property to hold SEARCH_FILTER_QUERY_PARAM.
	 */
	public static final String SEARCH_FILTER_QUERY_PARAM = "&fq=";
	
	/**
	 * property to hold ACCOUNT_ID.
	 */
	public static final String ACCOUNT_ID = "account_id=";
	
	/**
	 * property to hold AUTH_KEY.
	 */
	public static final String AUTH_KEY = "&auth_key=";
	
	/**
	 * property to hold DOMAIN_KEY.
	 */
	public static final String DOMAIN_KEY = "&domain_key=";
	
	/**
	 * property to hold REQUEST_ID.
	 */
	public static final String REQUEST_ID = "&request_id=";
	/**
	 * property to hold COMPLEX_FILTER.
	 */
	public static final String COMPLEX_FILTER = "&efq=";
	
	/**
	 * property to hold FACET_FILTER_RANGE.
	 */
	public static final String FACET_FILTER_RANGE = "&facet.range=";
	
	/**
	 * property to hold REF_URL.
	 */
	public static final String REF_URL = "&ref_url=";
	
	/**
	 * property to hold RSP_FMT.
	 */
	public static final String RSP_FMT = "&rsp_fmt=";
	
	/**
	 * Constant for CHECK_INPUT_PARAM.
	 */
	
	public static final String CHECK_INPUT_PARAM = "Please provide the proper input value";
}
