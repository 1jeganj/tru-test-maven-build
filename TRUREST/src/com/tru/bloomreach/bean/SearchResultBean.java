package com.tru.bloomreach.bean;

/**
 * This class SearchResultBean to hold all the BM response. 
 * 
 * 
 * @author Professional Access
 * @version 1.0
 * 
 * 
 */
public class SearchResultBean {
	
	/**
	 * This property hold reference for mResponse.
	 */
	private Response mResponse = new Response();
	
	/**
	 * This property hold reference for mPropertyList.
	 */
	private Object mNavigationInfo = new Object();
	/**
	 * @return the mResponse
	 */
	public Response getResponse() {
		return mResponse;
	}
	/**
	 * @param pResponse the mResponse to set
	 */
	public void setResponse(Response pResponse) {
		mResponse = pResponse;
	}
	/**
	 * @return the mNavigationInfo
	 */
	public Object getNavigationInfo() {
		return mNavigationInfo;
	}
	/**
	 * @param pNavigationInfo the mNavigationInfo to set
	 */
	public void setNavigationInfo(Object pNavigationInfo) {
		mNavigationInfo = pNavigationInfo;
	}
	
}
