package com.tru.bloomreach.bean;

import java.util.Set;

import com.tru.common.EndecaRecords;

/**
 * This class describes Response Status in BloomReach.
 * @author PA
 * @version 1.0
 */

public class Response {
	
	/**
	 * This property hold reference for mNumFound.
	 */
	private int mNumFound;
	/**
	 * This property hold reference for mStart.
	 */
	private int mStart;
	
	/**
	 * This property hold reference for mRecords.
	 */
	private Set<EndecaRecords> mRecords;
	
	/**
	 * This property hold reference for mRows.
	 */
	private int mRows;

	/**
	 * @return the mNumFound
	 */
	public int getNumFound() {
		return mNumFound;
	}

	/**
	 * @param pNumFound the mNumFound to set
	 */
	public void setNumFound(int pNumFound) {
		mNumFound = pNumFound;
	}

	/**
	 * @return the mStart
	 */
	public int getStart() {
		return mStart;
	}

	/**
	 * @param pStart the mStart to set
	 */
	public void setStart(int pStart) {
		mStart = pStart;
	}

	/**
	 * @return the mRecords
	 */
	public Set<EndecaRecords> getRecords() {
		return mRecords;
	}

	/**
	 * @param pRecords the mRecords to set
	 */
	public void setRecords(Set<EndecaRecords> pRecords) {
		mRecords = pRecords;
	}

	/**
	 * @return the mRows
	 */
	public int getRows() {
		return mRows;
	}

	/**
	 * @param pRows the mRows to set
	 */
	public void setRows(int pRows) {
		mRows = pRows;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Response [mNumFound=" + mNumFound + ", mStart=" + mStart
				+ ", mRecords=" + mRecords + ", mRows=" + mRows + "]";
	}
	
}
