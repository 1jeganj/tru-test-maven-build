package com.tru.bloomreach.service;

import java.util.List;

import atg.nucleus.GenericService;
/**
 * This class describes Response Status in Registry.
 * @author PA
 * @version 1.0
 */
public class TRUBloomReachConfiguration extends GenericService{

	/**
	 * This property hold reference for mDomainURL.
	 */
	private String mSearchURL;
	/**
	 * This property hold reference for mAccountId.
	 */
	private String mAccountId;
	/**
	 * This property hold reference for mAuthKey.
	 */
	private String mAuthKey;
	/**
	 * This property hold reference for mDomainKey.
	 */
	private String mDomainKey;
	/**
	 * This property hold reference for mRequestId.
	 */
	private String mRequestId;
	/**
	 * This property hold reference for mRealm.
	 */
	private String mRealm;
	/**
	 * This property hold reference for mBrOrigin.
	 */
	private String mBrOrigin;
	/**
	 * This property hold reference for mRequestType.
	 */
	private String mRequestType;
	/**
	 * This property hold reference for mSearchType.
	 */
	private String mSearchType;
	/**
	 * This property hold reference for mSearchFields.
	 */
	private String mSearchFields;
	
	/**
	 * This property hold reference for mTypeAheadURL.
	 */
	private String mTypeAheadURL;
	
	/**
	 * This property hold reference for mTypeAheadAccountId.
	 */
	private String mTypeAheadAccountId;
	
	/**
	 * This property hold reference for mTypeAheadAuthKey.
	 */
	private String mTypeAheadAuthKey;
	
	/**
	 * This property hold reference for mTypeAheadDomainKey.
	 */
	private String mTypeAheadDomainKey;
	
	/**
	 * This property hold reference for mTypeAheadRequestId.
	 */
	private String mTypeAheadRequestId;
	
	/**
	 * This property hold reference for mTypeAheadrequestType.
	 */
	private String mTypeAheadrequestType;
	
	/**
	 * This property hold reference for mTypeAheadRefUrl.
	 */
	private String mTypeAheadRefUrl;
	
	/**
	 * This property hold reference for typeAheadrspfmt.
	 */
	private String mTypeAheadrspfmt;
	
	
	/**
	 * This property hold reference for mProxyHost.
	 */
	private String mProxyHost;
	
	/**
	 * This property hold reference for mProxyPort.
	 */
	private int mProxyPort;
	
	/**
	 * This property hold reference for mTimeOut.
	 */
	private int mTimeOut; 
	
	/**
	 * This property hold reference for mFacetQueryRange.
	 */
	private List<String> mFacetQueryRange;
	
	
	/**
	 * Holds the property value of mProxyRequired.
	 */
	private boolean mProxyEnabled;
	
	
	/**
	 * @return the mSearchURL
	 */
	public String getSearchURL() {
		return mSearchURL;
	}
	/**
	 * @param pSearchURL the mSearchURL to set
	 */
	public void setSearchURL(String pSearchURL) {
		this.mSearchURL = pSearchURL;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return mAccountId;
	}
	/**
	 * @param pAccountId the accountId to set
	 */
	public void setAccountId(String pAccountId) {
		mAccountId = pAccountId;
	}
	/**
	 * @return the authKey
	 */
	public String getAuthKey() {
		return mAuthKey;
	}
	/**
	 * @param pAuthKey the authKey to set
	 */
	public void setAuthKey(String pAuthKey) {
		mAuthKey = pAuthKey;
	}
	/**
	 * @return the domainKey
	 */
	public String getDomainKey() {
		return mDomainKey;
	}
	/**
	 * @param pDomainKey the domainKey to set
	 */
	public void setDomainKey(String pDomainKey) {
		mDomainKey = pDomainKey;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return mRequestId;
	}
	/**
	 * @param pRequestId the requestId to set
	 */
	public void setRequestId(String pRequestId) {
		mRequestId = pRequestId;
	}
	/**
	 * @return the realm
	 */
	public String getRealm() {
		return mRealm;
	}
	/**
	 * @param pRealm the realm to set
	 */
	public void setRealm(String pRealm) {
		mRealm = pRealm;
	}
	/**
	 * @return the brOrigin
	 */
	public String getBrOrigin() {
		return mBrOrigin;
	}
	/**
	 * @param pBrOrigin the brOrigin to set
	 */
	public void setBrOrigin(String pBrOrigin) {
		mBrOrigin = pBrOrigin;
	}
	
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return mRequestType;
	}
	/**
	 * @param pRequestType the requestType to set
	 */
	public void setRequestType(String pRequestType) {
		mRequestType = pRequestType;
	}
	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return mSearchType;
	}
	/**
	 * @param pSearchType the searchType to set
	 */
	public void setSearchType(String pSearchType) {
		mSearchType = pSearchType;
	}
	/**
	 * @return the searchFields
	 */
	public String getSearchFields() {
		return mSearchFields;
	}
	/**
	 * @param pSearchFields the searchFields to set
	 */
	public void setSearchFields(String pSearchFields) {
		mSearchFields = pSearchFields;
	}
	
	/**
	 * @return the mTypeAheadURL
	 */
	public String getTypeAheadURL() {
		return mTypeAheadURL;
	}
	/**
	 * @param pTypeAheadURL the mTypeAheadURL to set
	 */
	public void setTypeAheadURL(String pTypeAheadURL) {
		this.mTypeAheadURL = pTypeAheadURL;
	}
	/**
	 * @return the mTypeAheadAccountId
	 */
	public String getTypeAheadAccountId() {
		return mTypeAheadAccountId;
	}
	/**
	 * @param pTypeAheadAccountId the mTypeAheadAccountId to set
	 */
	public void setTypeAheadAccountId(String pTypeAheadAccountId) {
		this.mTypeAheadAccountId = pTypeAheadAccountId;
	}
	/**
	 * @return the mTypeAheadAuthKey
	 */
	public String getTypeAheadAuthKey() {
		return mTypeAheadAuthKey;
	}
	/**
	 * @param pTypeAheadAuthKey the mTypeAheadAuthKey to set
	 */
	public void setTypeAheadAuthKey(String pTypeAheadAuthKey) {
		this.mTypeAheadAuthKey = pTypeAheadAuthKey;
	}
	/**
	 * @return the mTypeAheadDomainKey
	 */
	public String getTypeAheadDomainKey() {
		return mTypeAheadDomainKey;
	}
	/**
	 * @param pTypeAheadDomainKey the mTypeAheadDomainKey to set
	 */
	public void setTypeAheadDomainKey(String pTypeAheadDomainKey) {
		this.mTypeAheadDomainKey = pTypeAheadDomainKey;
	}
	/**
	 * @return the mTypeAheadRequestId
	 */
	public String getTypeAheadRequestId() {
		return mTypeAheadRequestId;
	}
	/**
	 * @param pTypeAheadRequestId the mTypeAheadRequestId to set
	 */
	public void setTypeAheadRequestId(String pTypeAheadRequestId) {
		this.mTypeAheadRequestId = pTypeAheadRequestId;
	}
	/**
	 * @return the mTypeAheadrequestType
	 */
	public String getTypeAheadrequestType() {
		return mTypeAheadrequestType;
	}
	/**
	 * @param pTypeAheadrequestType the mTypeAheadrequestType to set
	 */
	public void setTypeAheadrequestType(String pTypeAheadrequestType) {
		this.mTypeAheadrequestType = pTypeAheadrequestType;
	}
	/**
	 * @return the mTypeAheadRefUrl
	 */
	public String getTypeAheadRefUrl() {
		return mTypeAheadRefUrl;
	}
	/**
	 * @param pTypeAheadRefUrl the mTypeAheadRefUrl to set
	 */
	public void setTypeAheadRefUrl(String pTypeAheadRefUrl) {
		this.mTypeAheadRefUrl = pTypeAheadRefUrl;
	}
	/**
	 * @return the mTypeAheadrspfmt
	 */
	public String getTypeAheadrspfmt() {
		return mTypeAheadrspfmt;
	}
	/**
	 * @param pTypeAheadrspfmt the mTypeAheadrspfmt to set
	 */
	public void setTypeAheadrspfmt(String pTypeAheadrspfmt) {
		this.mTypeAheadrspfmt = pTypeAheadrspfmt;
	}
	
	/**
	 * @return the mProxyHost
	 */
	public String getProxyHost() {
		return mProxyHost;
	}
	/**
	 * @param pProxyHost the mProxyHost to set
	 */
	public void setProxyHost(String pProxyHost) {
		this.mProxyHost = pProxyHost;
	}
	/**
	 * @return the mProxyPort
	 */
	public int getProxyPort() {
		return mProxyPort;
	}
	/**
	 * @param pProxyPort the mProxyPort to set
	 */
	public void setProxyPort(int pProxyPort) {
		this.mProxyPort = pProxyPort;
	}
	/**
	 * @return the mTimeOut
	 */
	public int getTimeOut() {
		return mTimeOut;
	}
	/**
	 * @param pTimeOut the mTimeOut to set
	 */
	public void setTimeOut(int pTimeOut) {
		this.mTimeOut = pTimeOut;
	}
	/**
	 * @return the mFacetQueryRange
	 */
	public List<String> getFacetQueryRange() {
		return mFacetQueryRange;
	}
	/**
	 * @param pFacetQueryRange the mFacetQueryRange to set
	 */
	public void setFacetQueryRange(List<String> pFacetQueryRange) {
		this.mFacetQueryRange = pFacetQueryRange;
	}
	/**
	 * @return the mProxyEnabled
	 */
	public boolean isProxyEnabled() {
		return mProxyEnabled;
	}
	/**
	 * @param pProxyEnabled the mProxyEnabled to set
	 */
	public void setProxyEnabled(boolean pProxyEnabled) {
		mProxyEnabled = pProxyEnabled;
	}
	
}
