package com.tru.bloomreach.service;

import atg.nucleus.GenericService;

/**
 * This class describes Response Status in Registry.
 * @author PA
 * @version 1.0
 */
public class TRUBloomReachProxyConfiguration extends GenericService{
	private String mProxyName;
	private int mProxyPort;
	private int mTimeOut; 
	/**
	 * @return the proxyName
	 */
	public String getProxyName() {
		return mProxyName;
	}
	/**
	 * @param pProxyName the proxyName to set
	 */
	public void setProxyName(String pProxyName) {
		mProxyName = pProxyName;
	}
	/**
	 * @return the proxyPort
	 */
	public int getProxyPort() {
		return mProxyPort;
	}
	/**
	 * @param pProxyPort the proxyPort to set
	 */
	public void setProxyPort(int pProxyPort) {
		mProxyPort = pProxyPort;
	}
	/**
	 * @return the timeOut
	 */
	public int getTimeOut() {
		return mTimeOut;
	}
	/**
	 * @param pTimeOut the timeOut to set
	 */
	public void setTimeOut(int pTimeOut) {
		mTimeOut = pTimeOut;
	}
}
