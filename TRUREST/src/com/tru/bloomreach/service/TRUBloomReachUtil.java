package com.tru.bloomreach.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.service.perfmonitor.PerformanceMonitor;

import com.endeca.navigation.AssocDimLocations;
import com.endeca.navigation.AssocDimLocationsList;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.PropertyMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tru.bloomreach.bean.SearchResultBean;
import com.tru.bloomreach.constant.TRUBloomReachConstants;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.common.EndecaRecords;
import com.tru.common.PriceDetails;
import com.tru.common.Product;
import com.tru.common.SKUDetail;
import com.tru.common.SKURecords;
import com.tru.endeca.constants.TRUEndecaConstants;
import com.tru.integrations.constants.TRUConstants;
import com.tru.rest.constants.TRURestConstants;
import com.tru.util.TRUProductEndecaUtil;

/**
 * This class extends the GenericService class. 
 * This class contains series of helper methods.
 * 
 * @author Professional Access
 * @version 1.0
 * @param <E>
 * 
 */
public class TRUBloomReachUtil<E> extends GenericService{
		
	/**
	 * This property hold reference for mSearchConfiguration.
	 */
	private TRUBloomReachConfiguration mSearchConfiguration;
	
	/**
	 * This property hold reference for mProductEndecaUtil.
	 */
	private TRUProductEndecaUtil<E> mProductEndecaUtil;
	
	/**
	 * This property hold reference for mSkuIds.
	 */
	private String mSkuIds;
	
	/**
	 * This property hold reference for mInventorySkuIds.
	 */
	private List<String> mInventorySkuIds;
	
	
	/**
	 * This property hold reference for mEnbleMockData.
	 */
	private boolean mEnbleMockData;
	
	/**
	 * This property hold reference for mEnbleMockData.
	 */
	private String mSearchURL;
	
	/**
	 * This property hold reference for mEnbleMockData.
	 */
	private String mSearchSKUIds;
	
	/**
	 * This property hold reference for mTotalRecordSize.
	 */
	private long mTotalRecordSize;
	
	/**
	 * This method will populate the BloomReach API response and add price/inventory
	 * @param pQueryURL - QueryURL
	 * @return response
	 */
	public String getResponse(String pQueryURL) {
		if (isLoggingDebug()) {
            logDebug("BEGIN:: BloomReachUtil.getResponse method.. ");
		 }
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;
		String response = null;	
		try {
			httpClient = HttpClients.createDefault();
			HttpHost proxy = null;
			if(isLoggingDebug()){
			vlogDebug("Search API URL {0}", pQueryURL);	
			}
			setSearchURL(pQueryURL);
			HttpPost postResponse = new HttpPost(pQueryURL);
			RequestConfig config = RequestConfig.custom().build();
			proxy = new HttpHost(getSearchConfiguration().getProxyHost(), getSearchConfiguration().getProxyPort());
			config = RequestConfig.copy(config).setProxy(proxy).build();
			config = RequestConfig.copy(config).setAuthenticationEnabled(false).build();
			if (getSearchConfiguration().getTimeOut() != TRUConstants.NUMERIC_ZERO) {
				config = RequestConfig.copy(config).setConnectionRequestTimeout(getSearchConfiguration().getTimeOut()).build();
				config = RequestConfig.copy(config).setConnectTimeout(getSearchConfiguration().getTimeOut()).build();
				postResponse.setConfig(config);
				httpResponse = httpClient.execute(postResponse);
				if (httpResponse != null && httpResponse.getStatusLine().getStatusCode() == TRUConstants.SUCCESS) {
					response = EntityUtils.toString(httpResponse.getEntity(), TRUConstants.CHAR_ENCODE_VALUE);
				}
			}
		} catch (ClientProtocolException cpe) {
			if (isLoggingError()) {
				logError("ClientProtocolException Exception while processing Request", cpe);
			}
		} catch (IOException ie) {
			if (isLoggingError()) {
				logError("IOException Exception while processing Request", ie);
			}
		} finally {
			try {
				if (httpClient != null) {
					httpClient.close();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException ie) {
				if (isLoggingError()) {
					logError("IO Exception while closing HttpClient or Http Response.", ie);
				}
			}
		}
		if (isLoggingDebug()) {
            logDebug("END:: BloomReachUtil.getResponse method.. ");
		 }
		return response;
	}
	
	/**
	 *This method will construct the search query with domain 
	 * @param pQuery - Query
	 * @param pPageNumbers - PageNumbers
	 * @param pTotalRows - TotalRows
	 * @param pBrUID - BrUID
	 * @param pFilterQuery - FilterQuery
	 * @param pSortOption - SortOption
	 * @param pComplexQuery - ComplexQuery
	 * @param pRequestOrginURL - RequestOrginURL
	 * @return appentInput
	 * @throws UnsupportedEncodingException 
	 */
	public String constructSearchQuery(String pQuery, String pPageNumbers, String pTotalRows, String pBrUID, String pFilterQuery, String pSortOption, String pComplexQuery, String pRequestOrginURL) throws UnsupportedEncodingException {
		if (isLoggingDebug()) {
            logDebug("BEGIN:: BloomReachUtil.constructDomainURL method.. ");
		 }
		StringBuffer appentInput = new StringBuffer();
		if (StringUtils.isNotEmpty(getSearchConfiguration().getSearchURL())) {
			appentInput.append(getSearchConfiguration().getSearchURL());
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getAccountId())) {
			appentInput.append(TRUBloomReachConstants.ACCOUNT_ID).append(getSearchConfiguration().getAccountId());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getAuthKey())){
			appentInput.append(TRUBloomReachConstants.AUTH_KEY).append(getSearchConfiguration().getAuthKey());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getDomainKey())) {
			appentInput.append(TRUBloomReachConstants.DOMAIN_KEY).append(getSearchConfiguration().getDomainKey());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getRequestId())) {
			appentInput.append(TRUBloomReachConstants.REQUEST_ID).append(getSearchConfiguration().getRequestId());	
		}
		if (StringUtils.isNotEmpty(pBrUID)) {
			appentInput.append(TRUBloomReachConstants.RB_UID).append(encodeURLParam(pBrUID));	
		}
		if (StringUtils.isNotEmpty(pRequestOrginURL)) {
			appentInput.append(TRUBloomReachConstants.URL).append(encodeURLParam(pRequestOrginURL));	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getRequestType())) {
			appentInput.append(TRUBloomReachConstants.REQUEST_TYPE).append(getSearchConfiguration().getRequestType());	
		}
		if(StringUtils.isNotEmpty(pTotalRows)){
			appentInput.append(TRUBloomReachConstants.ROWS).append(pTotalRows);	
		}
		if (StringUtils.isNotEmpty(pPageNumbers)) {
			appentInput.append(TRUBloomReachConstants.START_PAGE).append(pPageNumbers);
		}
		if (StringUtils.isNotEmpty(getSearchConfiguration().getSearchFields())) {
			appentInput.append(TRUBloomReachConstants.OUT_PUT_PROPERTIES).append(encodeURLParam(getSearchConfiguration().getSearchFields()));	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getRealm())){
			appentInput.append(TRUBloomReachConstants.REALM).append(getSearchConfiguration().getRealm());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getBrOrigin())) {
			appentInput.append(TRUBloomReachConstants.BR_ORIGN).append(getSearchConfiguration().getBrOrigin());	
		}
		if (StringUtils.isNotEmpty(pQuery)) {
			appentInput.append(TRUBloomReachConstants.INPUT_QUERY).append(encodeURLParam(pQuery));
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getSearchType())) {
			appentInput.append(TRUBloomReachConstants.SEARCH_TYPE).append(getSearchConfiguration().getSearchType());	
		}
		if (StringUtils.isNotEmpty(pSortOption)) {
			appentInput.append(TRUBloomReachConstants.SORT_RESULT).append(encodeURLParam(pSortOption));
		}
		if (StringUtils.isNotEmpty(pFilterQuery)) {
			appentInput.append(splitSerachFilterQuery(pFilterQuery));
		}
		if (StringUtils.isNotEmpty(pComplexQuery)) {
			appentInput.append(TRUBloomReachConstants.COMPLEX_FILTER).append(encodeURLParam(pComplexQuery));	
		}
		if (getSearchConfiguration().getFacetQueryRange() != null && !getSearchConfiguration().getFacetQueryRange().isEmpty()) {
			appentInput.append(splitFacetFange(getSearchConfiguration().getFacetQueryRange()));	
		}
		if (isLoggingDebug()) {
            vlogDebug("END:: BloomReachUtil.constructDomainURL method.. ",appentInput.toString());
		 }
		return appentInput.toString();
	}
	
/**
 * This method to construct the TypeAheadURL
 * @param pSuggestQuery - SuggestQuery
 * @param pBrUID - BrUID
 * @param pRequestOrginURL - RequestOrginURL
 * @return appentInput
 * @throws UnsupportedEncodingException 
 */
	public String constructTypeAheadQuery(String pSuggestQuery, String pBrUID, String pRequestOrginURL) throws UnsupportedEncodingException {
		if (isLoggingDebug()) {
            vlogDebug("BEGIN:: BloomReachUtil.constructTypeAheadQuery method.. ");
		 }
		StringBuffer appentInput = new StringBuffer();
		if (StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadURL())) {
			appentInput.append(getSearchConfiguration().getTypeAheadURL());
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadAccountId())) {
			appentInput.append(TRUBloomReachConstants.ACCOUNT_ID).append(getSearchConfiguration().getTypeAheadAccountId());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadAuthKey())){
			appentInput.append(TRUBloomReachConstants.AUTH_KEY).append(getSearchConfiguration().getTypeAheadAuthKey());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadDomainKey())) {
			appentInput.append(TRUBloomReachConstants.DOMAIN_KEY).append(getSearchConfiguration().getTypeAheadDomainKey());	
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadRequestId())) {
			appentInput.append(TRUBloomReachConstants.REQUEST_ID).append(getSearchConfiguration().getTypeAheadRequestId());	
		}
		if (StringUtils.isNotEmpty(pBrUID)) {
			appentInput.append(TRUBloomReachConstants.RB_UID).append(pBrUID);	
		}
		if (StringUtils.isNotEmpty(pRequestOrginURL)) {
			appentInput.append(TRUBloomReachConstants.URL).append(encodeURLParam(pRequestOrginURL));	
		}
		appentInput.append(TRUBloomReachConstants.REF_URL).append(getSearchConfiguration().getTypeAheadRefUrl());
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadrspfmt())){
			appentInput.append(TRUBloomReachConstants.RSP_FMT).append(getSearchConfiguration().getTypeAheadrspfmt());	
		}
		if (StringUtils.isNotEmpty(pSuggestQuery)) {
			appentInput.append(TRUBloomReachConstants.INPUT_QUERY).append(encodeURLParam(pSuggestQuery));
		}
		if(StringUtils.isNotEmpty(getSearchConfiguration().getTypeAheadrequestType())){
			appentInput.append(TRUBloomReachConstants.REQUEST_TYPE).append(getSearchConfiguration().getTypeAheadrequestType());	
		}
		if (isLoggingDebug()) {
            vlogDebug("END:: BloomReachUtil.constructTypeAheadQuery method.. ");
		 }
		return appentInput.toString();
	}
	
	/**
	 * This method is to convert json object to map
	 *
	 * @param pObject - json object
	 * @return map - map
	 */
	public static Map<String, Object> toMap(JsonObject pObject) {
		Map<String, Object> map = new HashMap<String, Object>();
		for (Entry<String, JsonElement> pair : pObject.entrySet()) {
            map.put(pair.getKey(), toValue(pair.getValue()));
        }
		return map;
	}
	
	/**
	 * This method is to convert to value
	 *
	 * @param pValue - value
	 * @return object - Object
	 */
	private static Object toValue(JsonElement pValue) {
        if (pValue.isJsonNull()) {
            return null;
        } else if (pValue.isJsonArray()) {
            return toList((JsonArray) pValue);
        } else if (pValue.isJsonObject()) {
            return toMap((JsonObject) pValue);
        } else if (pValue.isJsonPrimitive()) {
            return toPrimitive((JsonPrimitive) pValue);
        }
        return null;
	}
	
	/**
	 * This method is to convert json array to list
	 *
	 * @param pArray - json array
	 * @return list - list
	 */
	private static List<Object> toList(JsonArray pArray) {
        List<Object> list = new ArrayList<Object>();
        for (JsonElement element : pArray) {
            list.add(toValue(element));
        }
        return list;
	}
	
	  /**
     * This method is to convert primitive
     *
     * @param pValue - value
     * @return string - string
     */
    private static Object toPrimitive(JsonPrimitive pValue) {
        if (pValue.isBoolean()) {
            return pValue.getAsBoolean();
        } else if (pValue.isString()) {
            return pValue.getAsString();
        } else if (pValue.isNumber()){
            return pValue.getAsNumber();
        }
        return null;
    }

	/**
	 * This method will construct all the searchFilterQuery params
	 * 
	 * @param pSearchFilterQuery
	 *            - SearchFilterQuery
	 * @return appentInput
	 * @throws UnsupportedEncodingException UnsupportedEncodingException
	 */
	public String splitSerachFilterQuery(String pSearchFilterQuery) throws UnsupportedEncodingException {
		 if (isLoggingDebug()) {
             logDebug("BEGIN:: BloomReachUtil.splitSerachFilterQuery method.. ");
		 }
		StringBuffer appentInput = new StringBuffer();
		String[] filterQuery;
		filterQuery = pSearchFilterQuery.split(TRUBloomReachConstants.SPLIT_COMMA);
		for (String searchFilter : filterQuery) {
			appentInput.append(TRUBloomReachConstants.SEARCH_FILTER_QUERY_PARAM).append(encodeURLParam(searchFilter));
		}
		if (isLoggingDebug()) {
            logDebug("END:: BloomReachUtil.:::::splitSerachFilterQuery method");
     }
		return appentInput.toString();
	}
	
	/**
	 * This method will construct all the searchFilterQuery params
	 * @param pFacetRange - FacetRange
	 * @return appentInput
	 * @throws UnsupportedEncodingException 
	 */
		public String splitFacetFange(List<String> pFacetRange) throws UnsupportedEncodingException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.splitFacetFange method.. ");
		}
		StringBuffer appentInput = new StringBuffer();
		for (String facets : pFacetRange) {
			appentInput.append(TRUBloomReachConstants.FACET_FILTER_RANGE).append(encodeURLParam(facets));
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.:::::splitFacetFange method");
		}
		return appentInput.toString();
	}
	/**
	 * This method should set inventory for all the skuCode.
	 * 
	 * @param pCatalogRefIds
	 *            CatalogRefIds
	 * @throws SiteContextException
	 *             If any SiteContextException occurs
	 * @return inventoryInfoMap - inventoryInfoMap
	 */
	     public Map<String, TRUInventoryInfo> prepareInventory(List<String> pCatalogRefIds) throws SiteContextException {
	            if (isLoggingDebug()) {
	                   logDebug("BEGIN:: BloomReachUtil.prepareInventory method.. ");
	            }
	            if (PerformanceMonitor.isEnabled()) {
	                   PerformanceMonitor.startOperation(TRURestConstants.START_INVENTORY_CALL);
	            }
	            Map<String,TRUInventoryInfo> inventoryInfoMap = getProductEndecaUtil().prepareInventory(pCatalogRefIds); 
	            if (isLoggingDebug()) {
	                   logDebug("END:: BloomReachUtil.:::::prepareInventory method");
	            }
	            if (PerformanceMonitor.isEnabled()) {
	                   PerformanceMonitor.startOperation(TRURestConstants.END_INVENTORY_CALL);
	            }
	            return inventoryInfoMap;
	     }

	/**
	 * This method will populate the inventory for all the items
	 * @param pSearchResponse - pSearchResponse
	 * @param pResponseObj - ResponseObj
	 * @param pStoreId - StoreId
	 * @throws  
	 */
	@SuppressWarnings("unchecked")
	public void populateProduct(SearchResultBean pSearchResponse, String pStoreId, Map<Object, Object> pResponseObj) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.populateProduct method.. ");
		}
		String prefix = TRURestConstants.EMPTY_SPACE;
		StringBuilder endecaQuerySkuIds = new StringBuilder();
		AssocDimLocationsList dimValues = null;
		List<String> skuIds = new ArrayList<String>();
		ERecList recList = null;
		String skuIdString = null;
		Object totalRecord = null;
		PropertyMap propertyMap = null;
		int numFound = 0;
		Map<String, TRUInventoryInfo> inventoryInfoMap = null;
		EndecaRecords newRecords;
		if (pResponseObj != null && !pResponseObj.isEmpty()) {
			List<Object> docObject = (List<Object>) pResponseObj.get(TRURestConstants.RESPONSE_DOCS);
			 totalRecord = (Object) pResponseObj.get(TRURestConstants.RESPONSE_NUMFOUND);
			if (totalRecord != null) {
				numFound = Integer.parseInt(totalRecord.toString());
			}
			if (docObject != null && !docObject.isEmpty()) {
				for (Object element : docObject) {
					Map<Object, Object> skuIdList = (Map<Object, Object>) element;
					List<Object> skuId = (List<Object>) skuIdList.get(TRURestConstants.RESPONSE_SKUID);
					skuIds.add(skuId.get(TRURestConstants.INT_ZERO).toString());
					endecaQuerySkuIds.append(prefix);
					prefix = TRURestConstants.PLUS;
					endecaQuerySkuIds.append(skuId.get(TRURestConstants.INT_ZERO));
				}
			}	
		}
		if (endecaQuerySkuIds != null) {
			skuIdString = endecaQuerySkuIds.toString().trim();
			setSearchSKUIds(skuIdString);
			try {
			Set<EndecaRecords> recordSet = new HashSet<EndecaRecords>();
			recList = getProductEndecaUtil().createSearchQuery(skuIdString, TRURestConstants.SKU_REPOSITORYID);
			if (recList != null && !recList.isEmpty()) {
				setTotalRecordSize(recList.size());
				inventoryInfoMap = getProductEndecaUtil().prepareInventory(skuIds);
				for (Object result : recList) {
					 final ERec eRec = (ERec) result;
					 propertyMap = eRec.getProperties();
					 dimValues = eRec.getDimValues();
					 newRecords = populateEndecaProduct(propertyMap, inventoryInfoMap, dimValues, pStoreId);
					 recordSet.add(newRecords);
					}
				}
				if (pSearchResponse.getResponse() != null) {
					pSearchResponse.getResponse().setRecords(recordSet);
					pSearchResponse.getResponse().setNumFound(numFound);
				}
			} catch (ENEQueryException eneQuery) {
				vlogError("Getting ENEQueryException exception from ENEQUERY", eneQuery);
			} catch (SiteContextException e) {
				vlogError("Getting SiteContextException exception while getting inventoryInfo.", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.:::::populateProduct method");
		}
	}
	
/**
 * This method to populateEndecaProduct
 * @param pPropertyMap - PropertyMap
 * @param pInventoryInfoMap - InventoryInfoMap
 * @param pDimValues - DimValues
 * @param pStoreId - StoreId
 * @return newRecords
 */
	private EndecaRecords populateEndecaProduct(PropertyMap pPropertyMap,Map<String, TRUInventoryInfo> pInventoryInfoMap, AssocDimLocationsList pDimValues,String pStoreId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.populateEndecaProduct method.. ");
		}
		int collectionskusize = 0;
		boolean isCollectionProduct = TRURestConstants.BOOLEAN_FALSE;
		AssocDimLocations siteIdDimLocation = null;
		EndecaRecords newRecords = null;
		DimLocation dimLoc = null;
		SKUDetail newSku = null;
		String dimVal = null;
		List<SKUDetail> newSkuArray=null;
		SKURecords skuRecords = new SKURecords();
		Product newProduct = new Product();
		siteIdDimLocation = pDimValues.getAssocDimLocations(getProductEndecaUtil().getRestConfiguration().getSkuSiteId());
		if (siteIdDimLocation != null && !siteIdDimLocation.isEmpty() && siteIdDimLocation.size() > TRURestConstants.INT_ZERO) {
			dimLoc = ((DimLocation) (siteIdDimLocation.get(TRURestConstants.INT_ZERO)));		
			dimVal = dimLoc.getDimValue().getName();
		}
		if (dimVal != null) {
			if(dimVal.contains(SiteContextManager.getCurrentSiteId())) {
				newProduct.setSiteId(SiteContextManager.getCurrentSiteId());
			}else{
				newProduct.setSiteId(dimVal);
			}
		}
		siteIdDimLocation = pDimValues.getAssocDimLocations(getProductEndecaUtil().getRestConfiguration().getProductCategory());
		if(siteIdDimLocation!=null && !siteIdDimLocation.isEmpty() && siteIdDimLocation.size() > TRURestConstants.INT_ZERO) {
			dimLoc = ((DimLocation) (siteIdDimLocation.get(TRURestConstants.INT_ZERO)));		
			dimVal = dimLoc.getDimValue().getName();
			if(dimVal!=null){
				newProduct.setCategory(dimVal);
			}
		}
		siteIdDimLocation = pDimValues.getAssocDimLocations(getProductEndecaUtil().getRestConfiguration().getProductLanguage());
		if(siteIdDimLocation!=null && !siteIdDimLocation.isEmpty() && siteIdDimLocation.size() > TRURestConstants.INT_ZERO) {
			dimLoc = ((DimLocation) (siteIdDimLocation.get(TRURestConstants.INT_ZERO)));		
			dimVal = dimLoc.getDimValue().getName();
			if(dimVal!=null) {
				newProduct.setLanguage(dimVal);
			}
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuListSize()) != null) {
			collectionskusize	= Integer.parseInt(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuListSize()).toString());	
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductLanguage())!=null) {
			newProduct.setLanguage(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductLanguage()).toString());
		}		
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductType())!=null && 
				pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductType()).toString().equals(TRURestConstants.COLLECTION_TYPE) && collectionskusize >TRUEndecaConstants.INT_ONE){
			newProduct.setIsCollectionProduct(TRURestConstants.BOOLEAN_TRUE);
			isCollectionProduct = TRURestConstants.BOOLEAN_TRUE;
		}
		setProduct(pPropertyMap,newProduct);
		newRecords = new EndecaRecords();
		newSkuArray=new ArrayList<SKUDetail>();
		newSku = populateSKUDetail(pPropertyMap, isCollectionProduct, pInventoryInfoMap, pStoreId);
		newSkuArray.add(newSku);
		skuRecords.setSku(newSkuArray);
		newRecords.setProduct(newProduct);
		newRecords.setRecords(skuRecords);
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.populateEndecaProduct method.. ");
		}
		return newRecords;
	}
	
	/**
	 * Set the Sku level properties for BloomReach response from Endeca record
	 * @param pPropertyMap - PropertyMap
	 * @param pIsCollectionProduct - IsCollectionProduct
	 * @param pInventoryInfo - InventoryInfo
	 * @param pStoreId - StoreId
	 * @return newSku - skuItem details
	 */
	private SKUDetail populateSKUDetail(PropertyMap pPropertyMap, boolean pIsCollectionProduct, Map<String, TRUInventoryInfo> pInventoryInfo, String pStoreId) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.populateSKUDetail method.. ");
		}
		String s2s=null;
		String ispuString = null;
		String shipToStore = null;
		TRUInventoryInfo currSKUInvInfo = null;
		SKUDetail newSku = new SKUDetail();
		setSKUBasicDetail(pPropertyMap,newSku,pIsCollectionProduct);
		setSKUImageDetail(pPropertyMap,newSku,pIsCollectionProduct);
		setSKUPriceDetail(pPropertyMap,newSku);
		if (pInventoryInfo != null && !pInventoryInfo.isEmpty()) {
			currSKUInvInfo = (TRUInventoryInfo) pInventoryInfo.get(newSku.getRepositoryId());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getChildWeightMin())!=null){
			newSku.setChildWeightMin(Integer.parseInt(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getChildWeightMin()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getChildWeightMax())!=null){
			newSku.setChildWeightMax(Integer.parseInt(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getChildWeightMax()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getStreetDate())!=null){
			newSku.setStreetDate(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getStreetDate()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getRegistryEligibility())!=null){
			newSku.setRegistryEligibility(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getRegistryEligibility()).toString().equals(TRURestConstants.ONE_STR));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getVideoGameEsrb())!=null){
			newSku.setVideoGameEsrb(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getVideoGameEsrb()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getVideoGameGenre())!=null){
			newSku.setVideoGameGenre(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getVideoGameGenre()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCustomerPurchaseLimit())!=null){
			newSku.setCustomerPurchaseLimit(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCustomerPurchaseLimit()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuChannelAvailability()) != null){
			newSku.setChannelAvailability(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuChannelAvailability()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getIsStylized())!=null) {
            newSku.setIsStylized(Boolean.parseBoolean(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getIsStylized()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getOriginalParentProduct())!=null){
			newSku.setOriginalParentProduct(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getOriginalParentProduct()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getRegistryWarningIndicator())!=null) {
            newSku.setRegistryWarningIndicator(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getRegistryWarningIndicator()).toString());
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getItemInStorePickUp()) != null) {
			String ispu = pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getItemInStorePickUp()).toString();
			if (TRUCommerceConstants.YES.equalsIgnoreCase(ispu) || TRUCommerceConstants.YES_STRING.equalsIgnoreCase(ispu) || TRUCommerceConstants.TRUE_FLAG.equalsIgnoreCase(ispu)) {
				ispuString = TRUCommerceConstants.YES;
			} else {
				ispuString = TRUCommerceConstants.NO;
			}
			newSku.setIspu(ispuString);
		} else {
			newSku.setIspu(TRUCommerceConstants.NO);
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getShipToStore()) != null){
			shipToStore = null;
			s2s = pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getShipToStore()).toString();
			if(StringUtils.isNotEmpty(s2s) && s2s.equals(TRUCommerceConstants.STRING_ONE)){
				shipToStore = TRUCommerceConstants.YES;	
			}else{
				shipToStore = TRUCommerceConstants.NO;
			}
			newSku.setS2s(shipToStore);
		}
		if (currSKUInvInfo == null || currSKUInvInfo.getAtcStatus() == TRURestConstants.COHERENCE_OUT_OF_STOCK) {
			newSku.setInventoryStatus(TRUCommerceConstants.OUT_OF_STOCK); 
		} else if (currSKUInvInfo != null) {
			newSku.setInventoryStatus(TRUCommerceConstants.IN_STOCK);
			if (StringUtils.isNotBlank(pStoreId) && StringUtils.isNotBlank(currSKUInvInfo.getLocationId()) && pStoreId.equalsIgnoreCase(currSKUInvInfo.getLocationId()) && ((StringUtils.isNotEmpty(ispuString) && ispuString.equalsIgnoreCase(TRUCommerceConstants.YES)) || (StringUtils.isNotEmpty(shipToStore) && shipToStore.equalsIgnoreCase(TRUCommerceConstants.YES)))) {
				newSku.setStoreInventory(true);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.populateSKUDetail method.. ");
		}
		return newSku;
	}

	/**
	 * Populate SKU level properties.
	 * @param pNewSku - NewSku
	 * @param pPropertyMap - PropertyMap
	 */
	private void setSKUPriceDetail(PropertyMap pPropertyMap, SKUDetail pNewSku){
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.setSKUPriceDetail method.. ");
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getIsStrikeThrough())!=null){
			pNewSku.setIsStrikeThrough(Boolean.parseBoolean(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getIsStrikeThrough()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getHasPriceRange())!=null){
			pNewSku.setHasPriceRange(Boolean.parseBoolean(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getHasPriceRange()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getPriceDisplay())!=null){
			pNewSku.setPriceDisplay(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getPriceDisplay()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuSalePrice())!=null){
			pNewSku.setSalePrice(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuSalePrice()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuListPrice())!=null){
			pNewSku.setListPrice(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuListPrice()).toString()));
		}
		PriceDetails priceDetails=new PriceDetails();
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMaximumPrice())!=null){
			priceDetails.setMaximumPrice(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMaximumPrice()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMinimumPrice())!=null){
			priceDetails.setMinimumPrice(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMinimumPrice()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSavedAmount())!=null){
			priceDetails.setSavedAmount(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSavedAmount()).toString()));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSavedPercentage())!=null){
			priceDetails.setSavedPercentage(Double.parseDouble(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSavedPercentage()).toString()));
		}
		pNewSku.setPriceDetails(priceDetails);
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.setSKUPriceDetail method.. ");
		}
	}
	
/**
 * This method to set the SKUImageDetail
 * @param pPropertyMap - PropertyMap
 * @param pNewSku - NewSku
 * @param pIsCollectionProduct - IsCollectionProduct
 */
	private void setSKUImageDetail(PropertyMap pPropertyMap, SKUDetail pNewSku, boolean pIsCollectionProduct) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.setSKUImageDetail method.. ");
		}
		if(pIsCollectionProduct && pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCollectionImageUrl())!=null) {
			pNewSku.setPrimaryImage(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCollectionImageUrl()).toString());
		} else {
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSearchImageUrl())!=null){
			pNewSku.setPrimaryImage(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSearchImageUrl()).toString());
			}
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSecondaryImage())!=null){
		pNewSku.setSecondaryImage(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSecondaryImage()).toString());
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getShipInOrigContainer())!=null){
		pNewSku.setShipInOrigContainer(pPropertyMap.get(
				getProductEndecaUtil().getRestConfiguration().getShipInOrigContainer()).toString().equals(TRURestConstants.ONE_STR));
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getUnCartable())!=null){
		pNewSku.setUnCartable(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getUnCartable()).toString().equals(TRURestConstants.ONE_STR));
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.setSKUImageDetail method.. ");
		}
	}
/**
 * Set the SKUBasicDetail
 * @param pPropertyMap - PropertyMap
 * @param pNewSku - NewSku
 * @param pIsCollectionProduct - IsCollectionProduct
 */
	private void setSKUBasicDetail(PropertyMap pPropertyMap, SKUDetail pNewSku, boolean pIsCollectionProduct) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.setSKUBasicDetail method.. ");
		}
		String siteId = null;
		if (pIsCollectionProduct && pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCollectionName())!=null) {
			pNewSku.setDisplayName(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getCollectionName()).toString());
		} else {
			if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuDisplayName())!=null){
				pNewSku.setDisplayName(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuDisplayName()).toString());
			}
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuCreationDate())!=null){
			pNewSku.setCreationDate(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuCreationDate()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuRepositoryId())!=null){
			pNewSku.setRepositoryId(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuRepositoryId()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuOnSale())!=null){
			pNewSku.setOnSale(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuOnSale()).toString().equals(TRURestConstants.ONE_STR));
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuSiteId())!=null) {
			siteId=getProductEndecaUtil().getRestConfiguration().getSkuSiteId().toString();
			if(siteId.contains(SiteContextManager.getCurrentSiteId())){
				pNewSku.setSiteId(SiteContextManager.getCurrentSiteId());
			}else{
				pNewSku.setSiteId(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuSiteId()).toString());
			}
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuReviewRating())!=null){
			pNewSku.setReviewRating(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuReviewRating()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuReviewCount())!=null){
			pNewSku.setReviewCount(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuReviewCount()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMfrSuggestedAgeMin())!=null){
			pNewSku.setMfrSuggestedAgeMin(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMfrSuggestedAgeMin()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMfrSuggestedAgeMax())!=null){
			pNewSku.setMfrSuggestedAgeMax(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getMfrSuggestedAgeMax()).toString());
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.setSKUBasicDetail method.. ");
		}
	}
/**
 * Set the product level properties for BloomReach response from Endeca record
 * @param pPropertyMap - PropertyMap
 * @param pNewProduct - pNewProduct
 */
	private void setProduct(PropertyMap pPropertyMap, Product pNewProduct) {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.setProduct method.. ");
		}
		String pdpURL = null;
		String onlinePID = null;
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductDisplayName()) != null) {
			pNewProduct.setDisplayName(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductDisplayName()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductRepositoryId()) != null) {
			pNewProduct.setRepositoryId(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductRepositoryId()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getOnlinePID()) != null) {
			onlinePID = pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getOnlinePID()).toString();
			pNewProduct.setOnlinePID(onlinePID);
		}
		if (pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductCategory()) != null) {
			pNewProduct.setCategory(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductCategory()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuLongDescription()) != null){
			pNewProduct.setLongDescription(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuLongDescription()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getPromotionalStickerDisplay()) != null) {
			pNewProduct.setPromotionalStickerDisplay(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getPromotionalStickerDisplay()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuBrand()) != null){
			pNewProduct.setBrand(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getSkuBrand()).toString());
		}
		if(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductLongDescription()) != null){
			pNewProduct.setShortDescription(pPropertyMap.get(getProductEndecaUtil().getRestConfiguration().getProductLongDescription()).toString());
		}
		try {
			pdpURL = getProductEndecaUtil().getSiteUtil().getPDPURL(onlinePID);
			pNewProduct.setUrl(pdpURL);
		} catch (SiteContextException e) {
			vlogError("Getting SiteContextException exception while getting PDP URL.",e);
		}
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.setProduct method.. ");
		}
	}
	/**
	 * This method will encode the input params
	 * @param pQueryParam - QueryParam
	 * @throws UnsupportedEncodingException - if throws the UnsupportedEncodingException
	 * @return encodedParam
	 *
	 */
	public String encodeURLParam(String pQueryParam) throws UnsupportedEncodingException {
		if (isLoggingDebug()) {
			logDebug("BEGIN:: BloomReachUtil.encodeURLParam method.. ");
		}
		String encodedQuery = null;
		encodedQuery = URLEncoder.encode(pQueryParam, TRUCommerceConstants.UTF_ENCODE);
		if (isLoggingDebug()) {
			logDebug("END:: BloomReachUtil.encodeURLParam method.. ");
		}
		return encodedQuery;
	}

	/**
	 * @return the searchConfiguration
	 */
	public TRUBloomReachConfiguration getSearchConfiguration() {
		return mSearchConfiguration;
	}

	/**
	 * @param pSearchConfiguration the searchConfiguration to set
	 */
	public void setSearchConfiguration(TRUBloomReachConfiguration pSearchConfiguration) {
		mSearchConfiguration = pSearchConfiguration;
	}

	/**
	 * @return the mProductEndecaUtil
	 */
	public TRUProductEndecaUtil<E> getProductEndecaUtil() {
		return mProductEndecaUtil;
	}

	/**
	 * @param pProductEndecaUtil the mProductEndecaUtil to set
	 */
	public void setProductEndecaUtil(TRUProductEndecaUtil<E> pProductEndecaUtil) {
		this.mProductEndecaUtil = pProductEndecaUtil;
	}

	/**
	 * @return the mSkuIds
	 */
	public String getSkuIds() {
		return mSkuIds;
	}

	/**
	 * @param pSkuIds the mSkuIds to set
	 */
	public void setSkuIds(String pSkuIds) {
		this.mSkuIds = pSkuIds;
	}

	
	/**
	 * @return the mEnbleMockData
	 */
	public boolean isEnbleMockData() {
		return mEnbleMockData;
	}

	/**
	 * @param pEnbleMockData the mEnbleMockData to set
	 */
	public void setEnbleMockData(boolean pEnbleMockData) {
		this.mEnbleMockData = pEnbleMockData;
	}

	/**
	 * @return the mInventorySkuIds
	 */
	public List<String> getInventorySkuIds() {
		return mInventorySkuIds;
	}

	/**
	 * @param pInventorySkuIds the mInventorySkuIds to set
	 */
	public void setInventorySkuIds(List<String> pInventorySkuIds) {
		this.mInventorySkuIds = pInventorySkuIds;
	}

	/**
	 * @return the mSearchURL
	 */
	public String getSearchURL() {
		return mSearchURL;
	}

	/**
	 * @param pSearchURL the mSearchURL to set
	 */
	public void setSearchURL(String pSearchURL) {
		mSearchURL = pSearchURL;
	}

	/**
	 * @return the mSearchSKUIds
	 */
	public String getSearchSKUIds() {
		return mSearchSKUIds;
	}

	/**
	 * @param pSearchSKUIds the mSearchSKUIds to set
	 */
	public void setSearchSKUIds(String pSearchSKUIds) {
		mSearchSKUIds = pSearchSKUIds;
	}

	/**
	 * @return the mTotalRecordSize
	 */
	public long getTotalRecordSize() {
		return mTotalRecordSize;
	}

	/**
	 * @param pTotalRecordSize the mTotalRecordSize to set
	 */
	public void setTotalRecordSize(long pTotalRecordSize) {
		mTotalRecordSize = pTotalRecordSize;
	}
	
}
