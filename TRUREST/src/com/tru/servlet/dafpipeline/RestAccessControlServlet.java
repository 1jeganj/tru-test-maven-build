package com.tru.servlet.dafpipeline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.tru.common.TRUConstants;
import com.tru.common.TRURestConfiguration;
import com.tru.rest.constants.TRURestConstants;

/**
 * This class is extended to allow the cross domain testing for REST Web Services.
 * @author Professional Access
 * @version  : 1.0
 * 
 */


public class RestAccessControlServlet extends InsertableServletImpl {

	/**
	 * This property hold chennelMap
	 */
	private TRURestConfiguration mConfiguration;
	
	/** 
	 * This method is overridden to allow the cross domain testing for REST Web Services.
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws IOException - IOException
	 * @throws ServletException -  ServletException 
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws IOException, ServletException {
		
		if(isLoggingDebug()){
    		logDebug("ENTERING into ::::: RestAccessControlServlet (service())");
    	}
		String contextName= pRequest.getContextPath();
		
		if (!StringUtils.isBlank(contextName)&& contextName.equals(TRURestConstants.REST_CONTEXT)&& !TRURestConstants.ALLOW_OPTION.equalsIgnoreCase(pRequest.getMethod())&& !isHeaderParameterValidated(pRequest, pResponse)) {
			return;
		}
		  
		setAllowOriginForChannelType(pRequest,pResponse);
		
		pResponse.addHeader(TRURestConstants.ACCESS_CONTROL_ALLOW_CREDENTIAL,TRURestConstants.ALLOW_ACCESS);
		pResponse.addHeader(TRURestConstants.ACCESS_CONTROL_METHODS,TRURestConstants.ACCESS_METHODS);
		pResponse.addHeader(TRURestConstants.ACCESS_CONTROL_MAX_AGE,TRURestConstants.ACCESS_CONTROL_AGE);
		pResponse.addHeader(TRURestConstants.ACCESS_CONTROL_ALLOW_HEADERS,TRURestConstants.ACCEPT_ORIGIN);
		 if (TRURestConstants.ALLOW_OPTION.equalsIgnoreCase(pRequest.getMethod())) {
             pResponse.setStatus(TRURestConstants.STATUS_CODE);
             pResponse.addHeader(TRURestConstants.ACCESS_CONTROL_EXPOSE_HEADERS,TRURestConstants.API_KEY_CHANNEL);
             return;
      }
		if(isLoggingDebug()){
    		logDebug("Exit from ::::: RestAccessControlServlet (service())");
    	}
		
		passRequest(pRequest, pResponse);
		
	}

	
	
	/**
	 * This method will set Access-Control-Allow-Origin header.
	 * 
	 * 
	 * @param pRequest dynamoHttpServletRequest
	 * @param pResponse dynamoHttpServletResponse
	 */
	@SuppressWarnings("rawtypes")
	public void setAllowOriginForChannelType(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		List allowedChannelOrigins =null;
		String requestOrigin = pRequest.getHeader(TRURestConstants.REQ_HEADER_ORIGIN);
		String channelType = pRequest.getHeader(TRURestConstants.API_CHANNEL);

		if (StringUtils.isNotBlank(channelType)) {
			allowedChannelOrigins =(List)getAllowedOriginsMap().get(channelType);
		}
		else if(TRURestConstants.ALLOW_OPTION.equalsIgnoreCase(pRequest.getMethod())){
			allowedChannelOrigins=(List) getAllowedOriginsMap().get(TRURestConstants.ALL_CHANNEL_ORIGINS);
		}
		
		if(allowedChannelOrigins!=null && !allowedChannelOrigins.isEmpty() && allowedChannelOrigins.contains(requestOrigin) ){
			pResponse.addHeader(TRURestConstants.ACCESS_CONTROL,requestOrigin);
		}
		
	}

	
	
	/**
	 * This method will determine channel type from pRequest header and set the.
	 * allow origin url accordingly.
	 * 
	 * @return allowedChannelTypeMap - returns allowedChannelTypeMap
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map getAllowedOriginsMap() {
		Map<String, List> allowedChannelTypeMap = new HashMap<String,List>();
		List<String> myList = new ArrayList<String>();
		List<String> allOriginList = new ArrayList();
		Iterator<?> entries;
		entries=getConfiguration().getChannelMap().entrySet().iterator();
		String allowedOrigins= new String();
		
		while (entries.hasNext()) {
			Map.Entry entry = (Map.Entry) entries.next();
			allowedOrigins= (String)entry.getValue();
			if(!StringUtils.isBlank(allowedOrigins)){
				myList = new ArrayList<String>(Arrays.asList(allowedOrigins.split(TRUConstants.COMMA )));
				allowedChannelTypeMap.put((String)entry.getKey(), myList);
				allOriginList.addAll(myList);
			}
		}
		if(allOriginList!=null && !allOriginList.isEmpty()){
			allowedChannelTypeMap.put(TRURestConstants.ALL_CHANNEL_ORIGINS, allOriginList);
		}
		return allowedChannelTypeMap;	
	}
	
	/**
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return boolean true/false
	 */
	public boolean isHeaderParameterValidated(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse ){
		if(isLoggingDebug()){
    		logDebug("ENTERING into ::::: RestAccessControlServlet.isHeaderParameterValidated");
    		logDebug("Request Method is ::"+pRequest.getMethod()+"Request URL is ::::"+pRequest.getRequestURL()+"current site is :::"+SiteContextManager.getCurrentSite());
    	}
		
		boolean flag= true;
		boolean noSite = false;
		
		String currReqURL= pRequest.getRequestURL().toString();
		if(StringUtils.isNotBlank(currReqURL)){
			currReqURL = currReqURL.toLowerCase();
		}
		if (StringUtils.isNotBlank(currReqURL) && SiteContextManager.getCurrentSite() == null && !currReqURL.contains(TRURestConstants.ACTOR_ERROR) && !currReqURL.contains(TRURestConstants.ACTOR_SUCCESS) && !currReqURL.contains(TRURestConstants.ACTOR_AUTH_ACTOR) ) {
			noSite=true;
		}
		
		
		if (getConfiguration().isApiKeySecurityEnable()) {
			String apiKey = pRequest.getHeader(TRURestConstants.API_KEY);
			String apiChannel = pRequest.getHeader(TRURestConstants.API_CHANNEL);
			
			if (noSite|| (StringUtils.isBlank(apiKey) || StringUtils.isBlank(apiChannel))|| !apiKey.equals(getConfiguration().getApiKey())|| !isChannelExist(apiChannel, getConfiguration().getApiChannelTypes())) {
				
				flag= false;
				  JSONObject json = new JSONObject();
				    try {
				    	if(noSite){
				    		json.put(TRURestConstants.ERROR_MSG, TRURestConstants.ERR_INVALID_SITE);
				    	}
				    	else{
				    		json.put(TRURestConstants.ERROR_MSG, TRURestConstants.ERR_INVALID_REQ);	
				    	}
						
					} catch (JSONException e) {
						if (isLoggingError()) {
							vlogError("JSONException occured", e);
						}
					}
				pResponse.setContentType(TRURestConstants.CONTENT_TYPE);
				pResponse.setStatus(TRURestConstants.UNAUTHORIZED_STATUS_CODE);
				PrintWriter writer;
				try {
					writer = pResponse.getWriter();
					writer.print(json.toString());
			        writer.flush();
				} catch (IOException e) {
					if (isLoggingError()) {
						vlogError("IOException occured", e);
					}
				}   
			}
			else if (currReqURL.contains(TRURestConstants.GET_CART)) {
				pRequest.setParameter(TRURestConstants.CART_LOAD, TRURestConstants.BOOLEAN_TRUE);
			}
			if (pRequest.getCookieParameterValues(TRURestConstants.SOS) != null
					&& pRequest.getCookieParameterValues(TRURestConstants.SOS)[TRURestConstants.INT_ZERO].equals(TRURestConstants.TRUE)) {
				pRequest.setParameter(TRURestConstants.IS_SOS_REQUEST, TRURestConstants.BOOLEAN_TRUE);
			}else{
				pRequest.setParameter(TRURestConstants.IS_SOS_REQUEST, TRURestConstants.BOOLEAN_FALSE);
			}

			if(isLoggingDebug()){
	    		logDebug("Exit from ::::: RestAccessControlServlet.isHeaderParameterValidated");
	    	}
		}
		return flag;
		
	}
	
	
	/**
	 * This method validate the Channel from request header.
	 * @param pApiChannel - pApiChannel
	 * @param pApiChannelList - pApiChannelList
	 * @return boolean true/false
	 */
	public boolean isChannelExist(String pApiChannel, List<String> pApiChannelList) {
		return pApiChannelList.contains(pApiChannel);

	}
	
	
	/**
	 * @return the mConfiguration
	 */
	public TRURestConfiguration getConfiguration() {
		return mConfiguration;
	}
	/**
	 * @param pConfiguration the mConfiguration to set
	 */
	public void setConfiguration(TRURestConfiguration pConfiguration) {
		this.mConfiguration = pConfiguration;
	}
	
}
