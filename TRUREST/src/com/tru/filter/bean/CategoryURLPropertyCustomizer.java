package com.tru.filter.bean;

import java.util.Map;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.service.filter.bean.BeanFilterException;
import atg.service.filter.bean.PropertyCustomizer;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.tru.commerce.catalog.vo.CategoryVO;
import com.tru.common.vo.TRUSiteVO;
import com.tru.rest.constants.TRURestConstants;
import com.tru.utils.TRUGetSiteTypeUtil;
import com.tru.utils.TRUSchemeDetectionUtil;

/**
 * This class is CategoryURLPropertyCustomizer which will append the domain with category URL for registry.
 * @author PA
 * @version 1.0
 * 
 */
public class CategoryURLPropertyCustomizer extends GenericService implements PropertyCustomizer{
	
	/** Property to hold mGetSiteTypeUtil. */
	private TRUGetSiteTypeUtil mGetSiteTypeUtil;
	/** The scheme detection util. */
	private TRUSchemeDetectionUtil mSchemeDetectionUtil;
	
	/**
	 * Implement method of PropertyCustomizer to get output property object.
	 * 
	 * @param pParamObject  - holds the reference for Object
	 * @param pParamString  - holds the reference for String
	 * @param pParamMap    - Map<String, String>
	 * @throws BeanFilterException - BeanFilterException
	 * @return Object - return modified category URL
	 */
	@Override
	public Object getPropertyValue(Object pParamObject, String pParamString,
			Map<String, String> pParamMap) throws BeanFilterException {
		if (pParamObject == null){
		    return null;
		}
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		if(StringUtils.isNotBlank(request.getHeader(TRURestConstants.API_CHANNEL))&&request.getHeader(TRURestConstants.API_CHANNEL).equals(TRURestConstants.MOBILE)){
			return ((CategoryVO)pParamObject).getCategoryURL();
		}
		StringBuffer categoryURL = new StringBuffer();
		Site currentSite = SiteContextManager.getCurrentSite();
		TRUSiteVO siteVO = getGetSiteTypeUtil().getSiteInfo(request, currentSite);
		categoryURL.append(getSchemeDetectionUtil().getScheme());
		categoryURL.append(siteVO.getSiteURL());
		categoryURL.append(((CategoryVO)pParamObject).getCategoryURL());
		return categoryURL.toString();
	}

	/**
	 * @return the getSiteTypeUtil
	 */
	public TRUGetSiteTypeUtil getGetSiteTypeUtil() {
		return mGetSiteTypeUtil;
	}

	/**
	 * @param pGetSiteTypeUtil the getSiteTypeUtil to set
	 */
	public void setGetSiteTypeUtil(TRUGetSiteTypeUtil pGetSiteTypeUtil) {
		mGetSiteTypeUtil = pGetSiteTypeUtil;
	}
	
	/**
	 * Gets the scheme detection util.
	 * 
	 * @return the scheme detection util
	 */
	public TRUSchemeDetectionUtil getSchemeDetectionUtil() {
		return mSchemeDetectionUtil;
	}

	/**
	 * Sets the scheme detection util.
	 * 
	 * @param pSchemeDetectionUtil
	 *            the new scheme detection util
	 */
	public void setSchemeDetectionUtil(TRUSchemeDetectionUtil pSchemeDetectionUtil) {
		this.mSchemeDetectionUtil = pSchemeDetectionUtil;
	}

}
