package com.tru.feedprocessor.tol.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.catalog.loader.TRUXMLFeedLoader;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassificationCrossReference;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class TRUTaxonomyHandler. This class is a generic class that used to process the taxonomy feed files.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUTaxonomyHandler extends DefaultHandler {
	/** The property to hold logging. */
	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRUXMLFeedLoader.class);

	/** The property to hold ClassificationMap*. */
	private Map<String, TRUClassification> mClassificationMap = new HashMap<String, TRUClassification>();
	/** The property to hold mCategoryMap*. */
	private Map<String, TRUCategory> mCategoryMap = new HashMap<String, TRUCategory>();

	/** The property to hold TruClassificationVOs*. */
	private List<TRUClassification> mTruClassificationVOs = new ArrayList<TRUClassification>();
	/** The property to hold mEntityKeySet*. */
	private List<String> mEntityKeySet = new ArrayList<String>();
	/** The property to hold Name*. */
	private boolean mName;

	/** The property to hold MetaData *. */
	private boolean mMetaData;

	/** The property to hold Value *. */
	private boolean mValue;

	/** The property to hold CharactersBuffer*. */
	private StringBuffer mCharactersBuffer = new StringBuffer();

	/** The property to hold TAXONOMYPARENT*. */
	private boolean mTAXONOMYPARENT;

	/** The property to hold TAXONOMYCATEGORY*. */
	private boolean mTAXONOMYCATEGORY;

	/** The property to hold SITETAXONOMYROOT*. */
	private boolean mSITETAXONOMYROOT;

	/** The property to hold TAXONOMYCATEGORYCOUNT*. */
	private int mTAXONOMYCATEGORYCOUNT;

	/** The property to hold TAXONOMYNODE*. */
	private boolean mTAXONOMYNODE;

	/** The property to hold ClassificationCrossReference*. */
	private boolean mClassificationCrossReference;

	/** The property to hold TAXONOMYCOLLECTION*. */
	private boolean mTAXONOMYCOLLECTION;

	/** The property to hold ClassificationID*. */
	private String mClassificationID;

	/** The property to hold ParentClassificationID*. */
	private String mParentClassificationID;

	/** The property to hold ParentClassificationIDs*. */
	private List<String> mParentClassificationIDs = new ArrayList<String>();

	/** The property to hold CrossMap*. */
	private Map<String, TRUClassificationCrossReference> mCrossMap = new HashMap<String, TRUClassificationCrossReference>();

	/** The property to hold ClassificationCrossReferenceID*. */
	private String mClassificationCrossReferenceID;

	/** The property to hold AttributeID*. */
	private String mAttributeID;

	/** The property to hold ParentCategoryID*. */
	private String mParentCategoryID;

	/** The property to hold Category*. */
	private TRUCategory mCategory;

	/** The property to hold Categories*. */
	private List<TRUCategory> mCategories = new ArrayList<TRUCategory>();

	/** The property to hold ClassifiationList*. */
	private List<TRUClassification> mClassifiationList = new ArrayList<TRUClassification>();

	/** property to hold TRUSKUFeedVO List. */
	private Map<String, Set<String>> mClassificationIdAndRootCategoryMap;
	/** property to hold mCollectionProdcutsMap List. */
	private Map<String, TRUCollectionProductVO> mCollectionProdcutsMap;
	/** property to hold collectionProductID . */
	private String mCollectionProductID;
	/** The property to hold TruClassificationVOs*. */
	private List<TRUCollectionProductVO> mTRUCollectionProductVOs;
	/** The property to hold mCollectionProductKeys*. */
	private Set<String> mCollectionProductKeys;

	/** The TAXONOMYFILTEREDLANDINGPAGE. */
	private boolean mTAXONOMYFILTEREDLANDINGPAGE;
	
	/** The property to hold mCrossReferenceList*. */
	List<String> mCrossReferenceList = null;
	
	/** The property to hold mCrossReferenceMap*. */
	private Map<String, List<String>> mCrossReferenceMap;
	
	/** property to hold FileName . */
	private String mFileName;
	
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the cross reference map.
	 *
	 * @return the crossReferenceMap
	 */
	public Map<String, List<String>> getCrossReferenceMap() {
		if(mCrossReferenceMap == null){
			mCrossReferenceMap = new HashMap<String, List<String>>();
		}
		return mCrossReferenceMap;
	}

	/**
	 * Sets the cross reference map.
	 *
	 * @param pCrossReferenceMap the crossReferenceMap to set
	 */
	public void setCrossReferenceMap(Map<String, List<String>> pCrossReferenceMap) {
		mCrossReferenceMap = pCrossReferenceMap;
	}

	/**
	 * Gets the collection product keys.
	 * 
	 * @return the collection product keys
	 */
	public Set<String> getCollectionProductKeys() {
		if (mCollectionProductKeys == null) {
			mCollectionProductKeys = new HashSet<String>();
		}
		return mCollectionProductKeys;
	}

	/**
	 * Sets the collection product keys.
	 * 
	 * @param pCollectionProductKeys
	 *            the new collection product keys
	 */
	public void setCollectionProductKeys(Set<String> pCollectionProductKeys) {
		this.mCollectionProductKeys = pCollectionProductKeys;
	}

	/**
	 * Gets the TRU collection product v os.
	 * 
	 * @return the TRU collection product v os
	 */
	public List<TRUCollectionProductVO> getTRUCollectionProductVOs() {
		if (mTRUCollectionProductVOs == null) {
			mTRUCollectionProductVOs = new ArrayList<TRUCollectionProductVO>();
		}
		return mTRUCollectionProductVOs;
	}

	/**
	 * Sets the TRU collection product v os.
	 * 
	 * @param pTRUCollectionProductVOs
	 *            the new TRU collection product v os
	 */
	public void setTRUCollectionProductVOs(List<TRUCollectionProductVO> pTRUCollectionProductVOs) {
		this.mTRUCollectionProductVOs = pTRUCollectionProductVOs;
	}

	/**
	 * Gets the collection prodcuts map.
	 * 
	 * @return the collection prodcuts map
	 */
	public Map<String, TRUCollectionProductVO> getCollectionProdcutsMap() {
		if (mCollectionProdcutsMap == null) {
			mCollectionProdcutsMap = new HashMap<String, TRUCollectionProductVO>();
		}
		return mCollectionProdcutsMap;
	}

	/**
	 * Sets the collection prodcuts map.
	 * 
	 * @param pCollectionProdcutsMap
	 *            the collection prodcuts map
	 */
	public void setCollectionProdcutsMap(Map<String, TRUCollectionProductVO> pCollectionProdcutsMap) {
		this.mCollectionProdcutsMap = pCollectionProdcutsMap;
	}

	/**
	 * Gets the TRUClassificationIdAndRootCategoryMap.
	 * 
	 * @return the TRUClassificationIdAndRootCategoryMap
	 */
	public Map<String, Set<String>> getClassificationIdAndRootCategoryMap() {
		if (mClassificationIdAndRootCategoryMap == null) {
			mClassificationIdAndRootCategoryMap = new HashMap<String, Set<String>>();
		}
		return mClassificationIdAndRootCategoryMap;
	}

	/**
	 * Sets the classification id and root category map.
	 * 
	 * @param pClassificationIdAndRootCategoryMap
	 *            the classification id and root category map
	 */
	public void setClassificationIdAndRootCategoryMap(Map<String, Set<String>> pClassificationIdAndRootCategoryMap) {
		this.mClassificationIdAndRootCategoryMap = pClassificationIdAndRootCategoryMap;
	}

	/**
	 * Gets the Categories.
	 * 
	 * @return mCategories
	 */
	public List<TRUCategory> getCategories() {
		return mCategories;
	}

	/**
	 * Sets the Categories.
	 * 
	 * @param pCategories
	 *            the new categories
	 */
	public void setCategories(List<TRUCategory> pCategories) {
		mCategories = pCategories;
	}

	/**
	 * Gets the EntityKeySet.
	 * 
	 * @return mEntityKeySet
	 */
	public List<String> getEntityKeySet() {
		return mEntityKeySet;
	}

	/**
	 * Sets the entity key set.
	 * 
	 * @param pEntityKeySet
	 *            the new entity key set
	 */
	public void setEntityKeySet(List<String> pEntityKeySet) {
		this.mEntityKeySet = pEntityKeySet;
	}

	/**
	 * Gets the List of TruClassificaion VO.
	 * 
	 * @return mTruClassificationVOs
	 */
	public List<TRUClassification> getTruClassificationVOs() {
		return mTruClassificationVOs;
	}

	/**
	 * Sets the TruClassification VO.
	 * 
	 * @param pTruClassificationVOs
	 *            the new tru classification v os
	 */
	public void setTruClassificationVOs(List<TRUClassification> pTruClassificationVOs) {
		mTruClassificationVOs = pTruClassificationVOs;
	}

	/**
	 * The Method startElement starts processing the tags.
	 * 
	 * @param pUri
	 *            the uri
	 * @param pLocalName
	 *            the local name
	 * @param pQName
	 *            the q name
	 * @param pAttributes
	 *            the attributes
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void startElement(String pUri, String pLocalName, String pQName, Attributes pAttributes) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.startElement BEGIN ");
		}
		if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.SITETAXONOMYROOT)) {
			mSITETAXONOMYROOT = Boolean.TRUE;
			mClassificationID = pAttributes.getValue(TRUFeedConstants.ID);
			TRUClassification classificationVO = new TRUClassification();
			classificationVO.setEntityName(TRUFeedConstants.CLASSIFICATION_ENTITY);
			classificationVO.setID(mClassificationID);
			classificationVO.setUserTypeID(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID));
			classificationVO.setFileName(getFileName());
			mClassificationMap.put(pAttributes.getValue(TRUFeedConstants.ID), classificationVO);

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) &&
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.TAXONOMYPARENT)) {
			mTAXONOMYPARENT = Boolean.TRUE;
			mClassificationID = pAttributes.getValue(TRUFeedConstants.ID);
			mCategory = new TRUCategory();
			mCategoryMap.put(mClassificationID, mCategory);
			mCategory.setID(mClassificationID);
			mParentCategoryID = pAttributes.getValue(TRUFeedConstants.ID);
			if (mClassificationID != null) {
				mParentClassificationID = mClassificationID;
			}

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) && 
				(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.TAXONOMYCATEGORY) || pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.TAXONOMYSUBCATEGORY))) {
			mTAXONOMYCATEGORYCOUNT++;
			mTAXONOMYCATEGORY = Boolean.TRUE;
			startElementForClassification(pAttributes);
			mCrossReferenceList = new ArrayList<String>();

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.TAXONOMYCOLLECTION)) {
			mTAXONOMYCOLLECTION = Boolean.TRUE;
			TRUCollectionProductVO collectionProductVO = new TRUCollectionProductVO();
			mCollectionProductID = pAttributes.getValue(TRUFeedConstants.ID);
			collectionProductVO.setId(mCollectionProductID);
			collectionProductVO.getParentClassifications().add(mClassificationID);
			collectionProductVO.setFileName(getFileName());
			getCollectionProdcutsMap().put(mCollectionProductID, collectionProductVO);
			Set<String> rootParentCat = new HashSet<String>();
			rootParentCat.add(mParentCategoryID);
			getClassificationIdAndRootCategoryMap().put(mCollectionProductID, rootParentCat);
			if (mParentCategoryID.equals(TRUFeedConstants.NAVIGATION_TRU)) {

				TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_TRU);
				collectionProductVO.getRootParentCategories().add(category.getID());
			}
			if (mParentCategoryID.equals(TRUFeedConstants.NAVIGATION_BRU)) {
				TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_BRU);
				collectionProductVO.getRootParentCategories().add(category.getID());
			}
			if (mParentCategoryID.equals(TRUFeedConstants.NAVIGATION_FAO)) {
				TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_FAO);
				collectionProductVO.getRootParentCategories().add(category.getID());
			}

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(TRUFeedConstants.TAXONOMYNODE)) {
			mTAXONOMYNODE = Boolean.TRUE;
			startElementForClassification(pAttributes);

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATION) &&
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equalsIgnoreCase(
						TRUFeedConstants.TAXONOMYFILTEREDLANDINGPAGE)) {
			mTAXONOMYFILTEREDLANDINGPAGE = Boolean.TRUE;
			startElementForClassification(pAttributes);

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.NAME)) {
			mName = Boolean.TRUE;

		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.CLASSIFICATIONCROSSREFERENCE)) {
			mClassificationCrossReference = Boolean.TRUE;
			mClassificationCrossReferenceID = pAttributes.getValue(TRUFeedConstants.CLASSIFICATION_ID);
			mCrossMap.put(mClassificationCrossReferenceID, new TRUClassificationCrossReference());
			TRUClassification crossClassificationVO = mClassificationMap.get(mClassificationCrossReferenceID);
			if(crossClassificationVO == null){
				crossClassificationVO = new TRUClassification();
				crossClassificationVO.setID(mClassificationCrossReferenceID);
				crossClassificationVO.setFileName(getFileName());
				mClassificationMap.put(mClassificationCrossReferenceID, crossClassificationVO);
			}
			TRUClassificationCrossReference crossClassVO = mCrossMap.get(mClassificationCrossReferenceID);
			crossClassVO.setId(mClassificationCrossReferenceID);
			crossClassVO.getParentCategory().add(mClassificationID);
			TRUClassification currentVO = mClassificationMap.get(mClassificationID);
			TRUClassification currentCrossVO = mClassificationMap.get(mClassificationCrossReferenceID);
			if (currentCrossVO != null) {
				if(mClassificationID.equals(mParentCategoryID)){
					mCategory.getFixedChildCategories().add(mClassificationCrossReferenceID);
				} else if (!currentCrossVO.getChildCategories().contains(mClassificationID)) {
					currentVO.getChildCategories().add(mClassificationCrossReferenceID);
				}
			} else {
				currentVO.getChildCategories().add(mClassificationCrossReferenceID);
			}
			if(getCrossReferenceMap().get(mClassificationID) != null){
				((List<String>)getCrossReferenceMap().get(mClassificationID)).add(mClassificationCrossReferenceID);
			}else{
				if(mCrossReferenceList == null){
					List<String> crossRefList = new ArrayList<String>();
					crossRefList.add(mClassificationCrossReferenceID);
					getCrossReferenceMap().put(mClassificationID,crossRefList);
				} else {
					mCrossReferenceList.add(mClassificationCrossReferenceID);
					getCrossReferenceMap().put(mClassificationID,mCrossReferenceList);
				}
			}
		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.METADATA)) {
			mMetaData = Boolean.TRUE;
		} else if (pQName.equalsIgnoreCase(TRUFeedConstants.VALUE)) {
			mValue = Boolean.TRUE;
			mAttributeID = pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID);

		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.startElement END ");
		}
	}

	/**
	 * The method startElementForClassification used to add parent categories destination directory.
	 * 
	 * @param pAttributes
	 *            the attributes
	 */
	private void startElementForClassification(Attributes pAttributes) {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.loadData BEGIN ");
		}
		if (mClassificationID != null) {
			if (mParentClassificationID != null) {
				mParentClassificationIDs.add(mParentClassificationID);
				mParentClassificationID = mClassificationID;
			} else {
				mParentClassificationID = mClassificationID;
			}
		}
		mClassificationID = pAttributes.getValue(TRUFeedConstants.ID);
		
		Set<String> rootParentCat = new HashSet<String>();
		rootParentCat.add(mParentCategoryID);
		List<String> crossParent = new ArrayList<String>();
		
		TRUClassification classificationVO = mClassificationMap.get(mClassificationID);
		
		if(classificationVO == null){
			classificationVO = new TRUClassification();
			classificationVO.setFileName(getFileName());
		}
		classificationVO.setPrimaryParentCategory(mParentClassificationID);
		
		classificationVO.setEntityName(TRUFeedConstants.CLASSIFICATION_ENTITY);
		if (mTAXONOMYCATEGORYCOUNT == TRUFeedConstants.ONE) {
			classificationVO.setParentCategory(mParentCategoryID);
		}
		TRUClassificationCrossReference crossClassMap = mCrossMap.get(mClassificationID);
		if (crossClassMap != null) {
			crossParent = crossClassMap.getParentCategory();
			crossParent.add(mParentClassificationID);
		} else if (mParentClassificationID != null && mTAXONOMYCATEGORYCOUNT != 0) {
			crossParent.add(mParentClassificationID);
		}
		if (mTAXONOMYCATEGORYCOUNT != TRUFeedConstants.ONE) {
			classificationVO.getParentClasssifications().addAll(crossParent);
		}
		classificationVO.setID(mClassificationID);
		classificationVO.setUserTypeID(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID));
		classificationVO.setDeleted(Boolean.FALSE);
		getClassificationIdAndRootCategoryMap().put(mClassificationID, rootParentCat);
		mClassificationMap.put(pAttributes.getValue(TRUFeedConstants.ID), classificationVO);
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.loadData BEGIN ");
		}

	}

	/**
	 * The method characters for reading the itenms from the tags.
	 * 
	 * @param pChar
	 *            the char
	 * @param pStart
	 *            the start
	 * @param pLength
	 *            the length
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void characters(char[] pChar, int pStart, int pLength) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.characters BEGIN ");
		}
		mCharactersBuffer = mCharactersBuffer.append(new String(pChar, pStart, pLength));
		String characters = mCharactersBuffer.toString().trim();
		if (mName && mTAXONOMYPARENT && mTAXONOMYCATEGORYCOUNT == 0) {
			mCategory.setName(characters);
		} else if (mName && mTAXONOMYCOLLECTION && (!characters.equals(TRUFeedConstants.CLASSIFICATION_1_ROOT))) {
			TRUCollectionProductVO collectionProductMapVO = getCollectionProdcutsMap().get(mCollectionProductID);
			collectionProductMapVO.setOnlineTitle(characters);
		} else if (mName && mClassificationID != null && (!characters.equals(TRUFeedConstants.CLASSIFICATION_1_ROOT))) {
			mClassificationMap.get(mClassificationID).setName(characters);
		} else if (mValue) {
			TRUClassification currentVO = mClassificationMap.get(mClassificationID);
			TRUClassification crossClassificationVO = mClassificationMap.get(mClassificationCrossReferenceID);
			TRUCollectionProductVO collectionProductMapVO = getCollectionProdcutsMap().get(mCollectionProductID);
			if (mTAXONOMYCOLLECTION && mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_TWO)) {
				collectionProductMapVO.setOnlineLongDesc(characters);
			} else if (mTAXONOMYCOLLECTION && mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_FIVE)) {
				collectionProductMapVO.setCollectionImage(characters);
			} else if (mTAXONOMYNODE && mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_NINE)) {
				currentVO.setDisplayStatus(characters);
			} else if (mTAXONOMYCOLLECTION && mAttributeID.equals(TRUFeedConstants.EIGHT_ZERO_ZERO_ZERO) && mMetaData) {
				collectionProductMapVO.setDisplayStatus(characters);
			} else if ((mTAXONOMYCATEGORY || mTAXONOMYFILTEREDLANDINGPAGE)
					&& mAttributeID.equals(TRUFeedConstants.EIGHT_ZERO_ZERO_ZERO)) {
				currentVO.setDisplayStatus(characters);
			} else if ((((mTAXONOMYNODE || mTAXONOMYFILTEREDLANDINGPAGE) || mTAXONOMYCATEGORY) && !mTAXONOMYCOLLECTION)
					&& mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_EIGHT) && !mClassificationCrossReference) {
				currentVO.setDisplayOrder(characters);
			} else if (((mTAXONOMYNODE || mTAXONOMYFILTEREDLANDINGPAGE) || mTAXONOMYCATEGORY)
					&& mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_TWO)) {
				currentVO.setLongDescription(characters);
			} else if (mClassificationCrossReference && mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_FOUR)) {
				//crossClassVO.setDisplayStatus(characters);
				crossClassificationVO.getCrossRefDisplayNameMap().put(mClassificationID, characters);
			} else if (mClassificationCrossReference && mAttributeID.equals(TRUFeedConstants.SEVEN_NINE_NINE_EIGHT)) {
				//crossClassVO.setDisplayOrder(characters);
				crossClassificationVO.getCrossRefDisplayOrderMap().put(mClassificationID, characters);
			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.characters END ");
		}
	}

	/**
	 * The method endElement() will add the child categories.
	 * 
	 * @param pUri
	 *            the uri
	 * @param pLocalName
	 *            the local name
	 * @param pQName
	 *            the q name
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void endElement(String pUri, String pLocalName, String pQName) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.endElement BEGIN ");
		}
		if (mName) {
			mName = Boolean.FALSE;
		} else if (mValue) {
			mValue = Boolean.FALSE;
		} else if (mMetaData) {
			mMetaData = Boolean.FALSE;
		} else if (mClassificationCrossReference) {
			mClassificationCrossReference = Boolean.FALSE;
		} else if (mTAXONOMYCOLLECTION) {
			mTAXONOMYCOLLECTION = Boolean.FALSE;
		} else if (mTAXONOMYNODE) {
			mTAXONOMYNODE = Boolean.FALSE;
			endElementForClassification();
		} else if (mTAXONOMYFILTEREDLANDINGPAGE) {
			mTAXONOMYFILTEREDLANDINGPAGE = Boolean.FALSE;
			endElementForClassification();
		} else if (mTAXONOMYCATEGORY) {
			TRUClassification currentVO = mClassificationMap.get(mClassificationID);
			if (!StringUtils.isBlank(currentVO.getParentCategory())) {
				if (currentVO.getParentCategory().equals(TRUFeedConstants.NAVIGATION_TRU)) {
					TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_TRU);
					category.getFixedChildCategories().add(currentVO.getID());
				} else if (currentVO.getParentCategory().equals(TRUFeedConstants.NAVIGATION_BRU)) {
					TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_BRU);
					category.getFixedChildCategories().add(currentVO.getID());
				} else if (currentVO.getParentCategory().equals(TRUFeedConstants.NAVIGATION_FAO)) {
					TRUCategory category = mCategoryMap.get(TRUFeedConstants.NAVIGATION_FAO);
					category.getFixedChildCategories().add(currentVO.getID());
				}
			}
			endElementForClassification();
			mTAXONOMYCATEGORYCOUNT--;
			if (mTAXONOMYCATEGORYCOUNT <= 0) {
				mTAXONOMYCATEGORY = Boolean.FALSE;
			}
		} else if (mTAXONOMYPARENT) {
			mTAXONOMYPARENT = Boolean.FALSE;
			if (!mParentClassificationIDs.isEmpty()) {
				int lastIndex = mParentClassificationIDs.size() - TRUFeedConstants.ONE;
				mParentClassificationID = mParentClassificationIDs.get(lastIndex);
				mParentClassificationIDs.remove(lastIndex);
			}
			mCategories.add(mCategory);
		} else if (mSITETAXONOMYROOT) {
			classificationSet();
			checkCrossReference();
			mSITETAXONOMYROOT = Boolean.FALSE;
		}
		mCharactersBuffer.setLength(0);
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.endElement END ");
		}
	}
	
	/**
	 * Check cross reference.
	 */
	private void checkCrossReference(){
		for (Map.Entry<String, List<String>> entry : mCrossReferenceMap.entrySet()) {
		    String key = entry.getKey();
		    if(getClassificationIdAndRootCategoryMap().get(key) != null ){
		    	Set keyRootCategory = getClassificationIdAndRootCategoryMap().get(key);
		    	for(String str: entry.getValue()){
		    		Set valRootCategory = getClassificationIdAndRootCategoryMap().get(str);
		    		if(valRootCategory != null && !keyRootCategory.containsAll(valRootCategory)){
		    			valRootCategory.addAll(keyRootCategory);
		    			childClassificationsRootParentCategoryUpdate(keyRootCategory, str);
		    		}
			    }
		    }
		}
	}

	/**
	 * Child classifications root parent category update.
	 *
	 * @param pKeyRootCategory the key root category
	 * @param pStr the str
	 */
	private void childClassificationsRootParentCategoryUpdate(Set pKeyRootCategory, String pStr) {
		TRUClassification classification = mClassificationMap.get(pStr);
		List<String> childCategories = classification.getChildCategories();
		for(String childId: childCategories){
			Set childRootCategory = getClassificationIdAndRootCategoryMap().get(childId);
			if(childRootCategory != null){
				childRootCategory.addAll(pKeyRootCategory);
				childClassificationsRootParentCategoryUpdate(pKeyRootCategory, childId);
			}
		}
	}

	/**
	 * The Method endElementForClassification will process all the child categories.
	 */
	private void endElementForClassification() {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.endElementForClassification BEGIN ");
		}
		TRUClassification parentVO = mClassificationMap.get(mParentClassificationID);
		if (mTAXONOMYCATEGORYCOUNT != 0 && parentVO != null) {
			parentVO.getChildCategories().add(mClassificationID);
		}
		mClassificationID = mParentClassificationID;
		if (!mParentClassificationIDs.isEmpty()) {
			int lastIndex = mParentClassificationIDs.size() - TRUFeedConstants.ONE;
			mParentClassificationID = mParentClassificationIDs.get(lastIndex);
			mParentClassificationIDs.remove(lastIndex);
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.endElementForClassification END ");
		}
	}

	/**
	 * The method classificationSet will add all the objects to a list.
	 * 
	 */
	private void classificationSet() {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.classificationSet BEGIN ");
		}
		for (String collectionMapKey : getCollectionProdcutsMap().keySet()) {
			if (mCrossMap.keySet().contains(collectionMapKey)) {
				getCollectionProdcutsMap().get(collectionMapKey).getParentClassifications()
						.addAll(mCrossMap.get(collectionMapKey).getParentCategory());
			}
		}
		Iterator<String> itrCollProd = getCollectionProdcutsMap().keySet().iterator();
		while (itrCollProd.hasNext()) {
			TRUCollectionProductVO listOfVO = getCollectionProdcutsMap().get(itrCollProd.next());
			getTRUCollectionProductVOs().add(listOfVO);
		}

		getCollectionProductKeys().addAll(getCollectionProdcutsMap().keySet());
		Iterator<String> itr = mClassificationMap.keySet().iterator();
		while (itr.hasNext()) {
			TRUClassification listOfVO = mClassificationMap.get(itr.next());
			if (!listOfVO.getID().equalsIgnoreCase(TRUFeedConstants.WEBSITE_NAVIGATION_HIERARCHY)) {
				listOfVO.getCollectionProductKeySet().addAll(getCollectionProdcutsMap().keySet());
				mClassifiationList.add(listOfVO);
			}
		}
		Set<String> entityKeys = mClassificationMap.keySet();
		entityKeys.remove(TRUFeedConstants.WEBSITE_NAVIGATION_HIERARCHY);
		getTruClassificationVOs().addAll(mClassifiationList);
		getEntityKeySet().addAll(entityKeys);
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUTaxonomyHandler.classificationSet END ");
		}
	}

}
