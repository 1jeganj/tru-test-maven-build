/*
 * 
 */
package com.tru.feedprocessor.tol.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRURegistryClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRURegistrySubClassfication;
import com.tru.feedprocessor.tol.catalog.vo.TRURootClassification;

/**
 * The Class TRURegsitryHandler.
 * This class is used to parse the registry xml and load all the data into the respective VO's 
 * @author Professional Access
 * @version 1.0
 */
public class TRURegsitryHandler extends DefaultHandler {

	
	/** Property to hold  logging. */
	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRURegsitryHandler.class);
	
	/** Property to hold  tru classification vo's. */
	private List<TRUClassification> mTruClassificationVOs = new ArrayList<TRUClassification>();

	/** Property to hold  tru mTRUCategories. */
	private List<TRUCategory> mTRUCategories = new ArrayList<TRUCategory>();

	/** Property to hold  tru root classification. */
	private TRURootClassification mTRURootClassification;

	/** Property to hold  tru registry classification. */
	private TRURegistryClassification mTRURegistryClassification;

	/** Property to hold  tru registry classifications. */
	private List<TRURegistryClassification> mTRURegistryClassifications;

	/** Property to hold  tru registry sub classfication. */
	private TRURegistrySubClassfication mTRURegistrySubClassfication;

	/** Property to hold  tru registry sub classfications. */
	private List<TRURegistrySubClassfication> mTRURegistrySubClassfications;

	/** Property to hold registry cat. */
	private boolean mIsRegistryCat = Boolean.FALSE;

	/** Property to hold classification. */
	private boolean mIsClassification = Boolean.FALSE;

	/** Property to hold sub classification. */
	private boolean mIsSubClassification = Boolean.FALSE;

	/** Property to hold classification name. */
	private boolean mIsClassificationName = Boolean.FALSE;

	/** Property to hold sub classification name. */
	private boolean mIsSubClassificationName = Boolean.FALSE;

	/** Property to hold classification display order. */
	private boolean mIsClassificationDisplayOrder = Boolean.FALSE;

	/** Property to hold classification image. */
	private boolean mIsClassificationImage = Boolean.FALSE;

	/** Property to hold must or nice. */
	private boolean mIsMustOrNice = Boolean.FALSE;

	/** Property to hold sub classification display order. */
	private boolean mIsSubClassificationDisplayOrder = Boolean.FALSE;

	/** Property to hold suggested qty. */
	private boolean mIsSuggestedQty = Boolean.FALSE;

	/** Property to hold registry cat name. */
	private boolean mIsRegistryCatName = Boolean.FALSE;
	/** The property to hold mEntityKeySet*. */
	private List<String> mEntityKeySet = new ArrayList<String>();

	/** property to hold the mCharacters. */
	private StringBuffer mCharactersBuffer = new StringBuffer();

	/** Property to hold FeedLogger. */
	private static FeedLogger mLogger;
	
	/** property to hold FileName . */
	private String mFileName;
	
	/** The Is classification cross reference. */
	private Boolean mIsClassificationCrossReference = Boolean.FALSE;
	
	/** The Is dummy cross ref. */
	private Boolean mIsDummyCrossRef = Boolean.FALSE;
	
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * This method is overridden to check the xml and set the classification properties to 
	 * the into the respective VO's.
	 *
	 * @param pUri the uri
	 * @param pLocalName the local name
	 * @param pQName the q name
	 * @param pAttributes the attributes
	 * @throws SAXException the SAX exception
	 */
	@Override
	public void startElement(String pUri, String pLocalName, String pQName, Attributes pAttributes) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.startElement BEGIN ");
		}
		if (pQName.equals(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equals(TRUFeedConstants.US_REGISTRY_CAT)) {
			mTRURootClassification = new TRURootClassification();
			mTRURootClassification.setID(pAttributes.getValue(TRUFeedConstants.ID));
			mTRURootClassification.setUserTypeID(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID));
			mIsRegistryCat = Boolean.TRUE;
			mTRURegistryClassifications = new ArrayList<TRURegistryClassification>();
		} else if (pQName.equals(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equals(TRUFeedConstants.PRIMARY_CATEGORY) && !mIsClassificationCrossReference) {
			mTRURegistryClassification = new TRURegistryClassification();
			mTRURegistryClassification.setID(pAttributes.getValue(TRUFeedConstants.ID));
			mTRURegistryClassification.setUserTypeID(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID));
			mTRURegistrySubClassfications = new ArrayList<TRURegistrySubClassfication>();
			mIsClassification = Boolean.TRUE;
		}

		else if (pQName.equals(TRUFeedConstants.CLASSIFICATION) && 
				pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID).equals(TRUFeedConstants.SUB_CATEGORY)  && !mIsClassificationCrossReference) {
			mTRURegistrySubClassfication = new TRURegistrySubClassfication();
			mTRURegistrySubClassfication.setID(pAttributes.getValue(TRUFeedConstants.ID));
			mTRURegistrySubClassfication.setUserTypeID(pAttributes.getValue(TRUFeedConstants.USER_TYPE_ID));
			mIsSubClassification = Boolean.TRUE;
		} else if (pQName.equals(TRUFeedConstants.NAME)  && !mIsClassificationCrossReference) {
			if (mIsSubClassification) {
				mIsSubClassificationName = Boolean.TRUE;
			} else if (mIsClassification) {
				mIsClassificationName = Boolean.TRUE;
			} else if (mIsRegistryCat) {
				mIsRegistryCatName = Boolean.TRUE;
			}
		} else if (pQName.equals(TRUFeedConstants.VALUE)  && !mIsClassificationCrossReference) {
			if (mIsSubClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.DISPLAY_ORDER)) {
				mIsSubClassificationDisplayOrder = Boolean.TRUE;
			} else if (mIsSubClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.SUGGESTED_QTY)) {
				mIsSuggestedQty = Boolean.TRUE;
			} else if (mIsSubClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.MUST_OR_NICE)) {
				mIsMustOrNice = Boolean.TRUE;
			} else if (mIsClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.IMAGE_PATH)) {
				mIsClassificationImage = Boolean.TRUE;
			} else if (mIsClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.DISPLAY_ORDER)) {
				mIsClassificationDisplayOrder = Boolean.TRUE;
			} else if (mIsSubClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.DISPLAY_STATUS) && 
					pAttributes.getValue(TRUFeedConstants.ID).equals(TRUFeedConstants.YES)) {
				mTRURegistrySubClassfication.setDisplayStatus(TRUFeedConstants.ACTIVE);
			} else if (mIsSubClassification && 
					pAttributes.getValue(TRUFeedConstants.ATTRIBUTE_ID).equals(TRUFeedConstants.DISPLAY_STATUS) && 
					pAttributes.getValue(TRUFeedConstants.ID).equals(TRUFeedConstants.NO)) {
				mTRURegistrySubClassfication.setDisplayStatus(TRUFeedConstants.HIDDEN);
			}

		} else if (mIsSubClassification && pQName.equals(TRUFeedConstants.CROSS_REFERRENCE)){  
			if(pAttributes.getValue(TRUFeedConstants.TYPE).equals(TRUFeedConstants.RCGSN)) {
				mIsClassificationCrossReference = Boolean.TRUE;
				String crossrefId = pAttributes.getValue(TRUFeedConstants.CLASSIFICATION_ID);
				if (crossrefId.indexOf(TRUFeedConstants.UNDER_SCORE) > 0) {
					crossrefId = crossrefId.substring(0, crossrefId.indexOf(TRUFeedConstants.UNDER_SCORE));
				}
				mTRURegistrySubClassfication.getCrossReferenceId().add(crossrefId);
			} else {
				mIsDummyCrossRef = Boolean.TRUE;
			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.startElement BEGIN ");
		}
	}

	/**
	 * This method is overridden to check the xml and set the classification properties to 
	 * the into the respective VO's.
	 *
	 * @param pChar the char
	 * @param pStart the start
	 * @param pLength the length
	 * @throws SAXException the SAX exception
	 */
	@Override
	public void characters(char[] pChar, int pStart, int pLength) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.characters BEGIN ");
		}

		mCharactersBuffer = mCharactersBuffer.append(pChar, pStart, pLength);
		String characters = mCharactersBuffer.toString().trim();
		if (mIsSubClassificationName) {
			mTRURegistrySubClassfication.setName(characters);
		} else if (mIsClassificationName) {
			mTRURegistryClassification.setName(characters);
		} else if (mIsClassificationDisplayOrder) {
			mTRURegistryClassification.setDisplayOrder(characters);
		} else if (mIsClassificationImage) {
			mTRURegistryClassification.setImagePath(characters);
		} else if (mIsMustOrNice) {
			mTRURegistrySubClassfication.setMustOrNiceToHave(characters);
		} else if (mIsSuggestedQty) {
			mTRURegistrySubClassfication.setSuggestedQty(characters);
		} else if (mIsSubClassificationDisplayOrder) {
			mTRURegistrySubClassfication.setDisplayOrder(characters);
		} else if (mIsRegistryCatName) {
			mTRURootClassification.setName(characters);
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.characters END ");
		}
	}

	/**
	 * This method is overridden to check the xml and set the classification properties to 
	 * the into the respective VO's.
	 *
	 * @param pUri the uri
	 * @param pLocalName the local name
	 * @param pQName the q name
	 * @throws SAXException the SAX exception
	 */
	@Override
	public void endElement(String pUri, String pLocalName, String pQName) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.endElement BEGIN ");
		}
		if (pQName.equals(TRUFeedConstants.CLASSIFICATION)) {
			if (!mIsClassificationCrossReference && mIsSubClassification) {
				mIsSubClassification = Boolean.FALSE;
				mIsSubClassificationName = Boolean.FALSE;
				if(null!=mTRURegistrySubClassfications){
				mTRURegistrySubClassfications.add(mTRURegistrySubClassfication);
				}
			} else if (!mIsClassificationCrossReference && mIsClassification) {
				mIsClassification = Boolean.FALSE;
				mIsClassificationName = Boolean.FALSE;
				mTRURegistryClassification.setTRURegistrySubClassfication(mTRURegistrySubClassfications);
				if(null!=mTRURegistryClassifications){
				mTRURegistryClassifications.add(mTRURegistryClassification);
				}
			} else if (!mIsClassificationCrossReference && mIsRegistryCat) {
				mIsRegistryCat = Boolean.FALSE;
				mTRURootClassification.setTRURegistryClassification(mTRURegistryClassifications);
				setTRUClassificationList();
			}
		}

		else if (pQName.equals(TRUFeedConstants.NAME)) {
			if (mIsSubClassificationName) {
				mIsSubClassificationName = Boolean.FALSE;
			} else if (mIsClassificationName) {
				mIsClassificationName = Boolean.FALSE;
			} else if (mIsRegistryCatName) {
				mIsRegistryCatName = Boolean.FALSE;
			}
		} else if (pQName.equals(TRUFeedConstants.VALUE)) {
			if (mIsClassificationDisplayOrder) {
				mIsClassificationDisplayOrder = Boolean.FALSE;
			} else if (mIsClassificationImage) {
				mIsClassificationImage = Boolean.FALSE;
			} else if (mIsMustOrNice) {
				mIsMustOrNice = Boolean.FALSE;
			} else if (mIsSuggestedQty) {
				mIsSuggestedQty = Boolean.FALSE;
			} else if (mIsSubClassificationDisplayOrder) {
				mIsSubClassificationDisplayOrder = Boolean.FALSE;
			}
		}
		if (pQName.equals(TRUFeedConstants.CROSS_REFERRENCE)) {
			if(mIsDummyCrossRef){
				mIsDummyCrossRef = Boolean.FALSE;
			} else {
				mIsClassificationCrossReference = Boolean.FALSE;
			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.endElement END ");
		}
		mCharactersBuffer.setLength(0);
	}

	/**
	 * Sets the tru classification list. This method is used to iterate all the catalog object and build the categories and
	 * list of classification
	 */
	private void setTRUClassificationList() {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.setTRUClassificationList BEGIN ");
		}
		TRUClassification parentClassVO = null;
		TRUClassification childClassVo = null;
		List<String> childClassIds = null;
		List<String> parentClassIds = new ArrayList<String>();
		TRUCategory rootCategory = new TRUCategory();
		String classificationType = null;
		Map<String, TRUClassification> classificationVO=new HashMap<String, TRUClassification>();
		rootCategory.setUserTypeID(mTRURootClassification.getUserTypeID());
		rootCategory.setName(mTRURootClassification.getName());
		rootCategory.setRegistry(Boolean.TRUE);
		rootCategory.setID(mTRURootClassification.getID());
		if (mTRURootClassification.getUserTypeID().equals(TRUFeedConstants.US_REGISTRY_CAT)) {
			classificationType = TRUFeedConstants.REGISTRY;
		}
		for (TRURegistryClassification regClassfication : mTRURootClassification.getTRURegistryClassification()) {
			parentClassVO = new TRUClassification();
			parentClassVO.setEntityName(TRUFeedConstants.REGISTRY_CLASSIFICATION_ENTITY);
			parentClassVO.setID(regClassfication.getID());
			parentClassVO.setName(regClassfication.getName());
			parentClassVO.setDisplayOrder(regClassfication.getDisplayOrder());
			parentClassVO.setImagePath(regClassfication.getImagePath());
			parentClassVO.setUserTypeID(regClassfication.getUserTypeID());
			parentClassVO.setClassificationType(classificationType);
			parentClassVO.setParentCategory(mTRURootClassification.getID());
			parentClassVO.setRegistry(Boolean.TRUE);
			parentClassVO.setDeleted(Boolean.FALSE);
			parentClassVO.setRootParentCategory(mTRURootClassification.getID());
			parentClassVO.setFileName(getFileName());
			parentClassIds.add(regClassfication.getID());
			childClassIds = new ArrayList<String>();
			for (TRURegistrySubClassfication subClassification : regClassfication.getTRURegistrySubClassfication()) {
				childClassIds.add(subClassification.getID());
				childClassVo = new TRUClassification();
				childClassVo.setEntityName(TRUFeedConstants.REGISTRY_CLASSIFICATION_ENTITY);
				childClassVo.setID(subClassification.getID());
				childClassVo.setUserTypeID(subClassification.getUserTypeID());
				childClassVo.setName(subClassification.getName());
				childClassVo.setMustOrNiceToHave(subClassification.getMustOrNiceToHave());
				childClassVo.setSuggestedQty(subClassification.getSuggestedQty());
				childClassVo.setRegistryCrossReferenceIds(subClassification.getCrossReferenceId());
				childClassVo.setDisplayOrder(subClassification.getDisplayOrder());
				childClassVo.setDisplayStatus(subClassification.getDisplayStatus());
				childClassVo.setParentClassification(regClassfication.getID());
				childClassVo.setRootParentCategory(mTRURootClassification.getID());
				childClassVo.setClassificationType(classificationType);
				childClassVo.setRegistry(Boolean.TRUE);
				childClassVo.setDeleted(Boolean.FALSE);
				childClassVo.setFileName(getFileName());
				classificationVO.put(childClassVo.getID(), childClassVo);
				parentClassVO.setTRURegistrySubClassfication(childClassIds);
			}
			classificationVO.put(parentClassVO.getID(), parentClassVO);
		}
		rootCategory.getFixedChildCategories().addAll(parentClassIds);
		Iterator<String> itr = classificationVO.keySet().iterator();
		getEntityKeySet().addAll(classificationVO.keySet());
		while (itr.hasNext()) {
			TRUClassification listOfVO = classificationVO.get(itr.next());
			getTruClassificationVOs().add(listOfVO);
		}
		getTRUCategories().add(rootCategory);
		
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRURegsitryHandler.setTRUClassificationList END ");
		}
	}

	/**
	 * Gets the tru classification v os.
	 * 
	 * @return the tru classification v os
	 */
	public List<TRUClassification> getTruClassificationVOs() {
		return mTruClassificationVOs;
	}

	/**
	 * Gets the TRU categories.
	 * 
	 * @return the TRU categories
	 */
	public List<TRUCategory> getTRUCategories() {
		return mTRUCategories;
	}

	/**
	 * Gets the Logger .
	 * 
	 * @return mLogger
	 */
	public static FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * sets the logger property.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/**
	 * Gets the entity key set.
	 *
	 * @return the entity key set
	 */
	public List<String> getEntityKeySet() {
		return mEntityKeySet;
	}

	/**
	 * Sets the entity key set.
	 *
	 * @param pEntityKeySet the new entity key set
	 */
	public void setEntityKeySet(List<String> pEntityKeySet) {
		mEntityKeySet = pEntityKeySet;
	}
}
