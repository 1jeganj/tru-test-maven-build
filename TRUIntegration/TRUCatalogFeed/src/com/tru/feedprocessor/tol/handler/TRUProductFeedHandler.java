package com.tru.feedprocessor.tol.handler;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUBeanUtilsBean;
import com.tru.feedprocessor.tol.catalog.vo.TRUBatteryQuantitynTypeVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUBookCdDvDSKUVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCarSeatSKUVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCribSKUVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCrossSellsnUidBatteriesVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUNonMerchSKUVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductsVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUStrollerSKUVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUVideoGameSKUVO;

/**
 * The Class TRUProductFeedHandler. This class is a generic class that used to process the product feed files.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUProductFeedHandler extends DefaultHandler {

	/** property to hold TRUProductsVO. */
	private TRUProductsVO mTRUProductsVO;

	/** property to hold TRUProductFeedVO. */
	private TRUProductFeedVO mTRUProductFeedVO;

	/** property to hold TRUSKUFeedVO. */
	private TRUSkuFeedVO mTRUSKUFeedVO;

	/** property to hold TRUStrollerSKUVO. */
	private TRUStrollerSKUVO mTRUStrollerSKUVO;

	/** property to hold TRUCribSKUVO. */
	private TRUCribSKUVO mTRUCribSKUVO;

	/** property to hold TRUCarSeatSKUVO. */
	private TRUCarSeatSKUVO mTRUCarSeatSKUVO;

	/** property to hold TRUBookCdDvDSKUVO. */
	private TRUBookCdDvDSKUVO mTRUBookCdDvDSKUVO;

	/** property to hold TRUVideoGameSKUVO. */
	private TRUVideoGameSKUVO mTRUVideoGameSKUVO;

	/** property to hold TRUNonMerchSKUVO. */
	private TRUNonMerchSKUVO mTRUNonMerchSKUVO;

	/** property to hold CharactersBuffer. */
	private StringBuffer mCharactersBuffer = new StringBuffer();

	/** property to hold Characters. */
	private String mCharacters;

	/** property to hold IsProductFeedVO. */
	private Boolean mIsProductFeedVO = Boolean.FALSE;

	/** property to hold IsSKUFeedVO. */
	private Boolean mIsSKUFeedVO = Boolean.FALSE;

	/** property to hold IsStyle. */
	private Boolean mIsStyle = Boolean.FALSE;

	/** property to hold SkuClassificationIds. */
	private List<String> mSkuClassificationIds;

	/** property to hold IsDigitalAssetImage. */
	private Boolean mIsDigitalAssetImage = Boolean.FALSE;

	/** property to hold IsPrimaryImage. */
	private Boolean mIsPrimaryImage = Boolean.FALSE;

	/** property to hold IsSecondaryImage. */
	private Boolean mIsSecondaryImage = Boolean.FALSE;

	/** property to hold IsSecondaryImage. */
	private Boolean mIsAlternateImage = Boolean.FALSE;

	/** property to hold InternetImageURL. */
	private Boolean mInternetImageURL = Boolean.FALSE;
	
	/** property to hold CanonicalImageURL. */
	private Boolean mCanonicalImageURL = Boolean.FALSE;

	/** property to hold Image. */
	private Boolean mImage = Boolean.FALSE;

	/** property to hold ProductCrossReference. */
	private Boolean mProductCrossReference = Boolean.FALSE;

	/** property to hold CrossSell. */
	private Boolean mCrossSell = Boolean.FALSE;

	/** property to hold Batteries. */
	private Boolean mBatteries = Boolean.FALSE;

	/** property to hold Metadata. */
	private Boolean mMetadata = Boolean.FALSE;

	/** property to hold UPCNUMBER. */
	private Boolean mUPCNUMBER = Boolean.FALSE;

	/** property to hold BRANDNAMESECONDARY. */
	private Boolean mBRANDNAMESECONDARY = Boolean.FALSE;

	/** property to hold BRANDNAMETERTIARYCHARACTERTHEME. */
	private Boolean mBRANDNAMETERTIARYCHARACTERTHEME = Boolean.FALSE;

	/** property to hold STRLBESTUSES. */
	private Boolean mSTRLBESTUSES = Boolean.FALSE;

	/** property to hold STRLEXTRAS. */
	private Boolean mSTRLEXTRAS = Boolean.FALSE;

	/** property to hold STRLTYPE. */
	private Boolean mSTRLTYPE = Boolean.FALSE;

	/** property to hold STRLWHATISIMP. */
	private Boolean mSTRLWHATISIMP = Boolean.FALSE;

	/** property to hold CribMaterialTypes. */
	private Boolean mCribMaterialTypes = Boolean.FALSE;

	/** property to hold CribStyles. */
	private Boolean mCribStyles = Boolean.FALSE;

	/** property to hold cribTypes. */
	private Boolean mCribTypes = Boolean.FALSE;

	/** property to hold cribWhatIsImp. */
	private Boolean mCribWhatIsImp = Boolean.FALSE;

	/** property to hold carSeatFeatures. */
	private Boolean mCarSeatFeatures = Boolean.FALSE;

	/** property to hold carSeatTypes. */
	private Boolean mCarSeatTypes = Boolean.FALSE;

	/** property to hold carSeatWhatIsImp. */
	private Boolean mCarSeatWhatIsImp = Boolean.FALSE;

	/** property to hold PRODUCTSAFETYWARNING. */
	private Boolean mPRODUCTSAFETYWARNING = Boolean.FALSE;

	/** property to hold ItemAttribute. */
	private Boolean mItemAttribute = Boolean.FALSE;
	
	/** property to hold Interest. */
	private Boolean mInterest = Boolean.FALSE;
	
	/** property to hold FinderEligible. */
	private Boolean mFinderEligible = Boolean.FALSE;
	
	/** property to hold SpecialFeatures. */
	private Boolean mSpecialFeatures  = Boolean.FALSE;

	/** property to hold ProductId. */
	private String mProductId;

	/** property to hold AttributeId. */
	private String mAttributeId;

	/** property to hold Value. */
	private String mValue;

	/** property to hold SKUTYPE. */
	private String mSKUType;
	
	/** property to hold ISNonMerchFlag. */
	private String mISNonMerchFlag;

	/** property to hold CrossSellsnUidBatteriesVO. */
	private TRUCrossSellsnUidBatteriesVO mCrossSellsnUidBatteriesVO;

	/** property to hold BatteryQuantitynTypeVO. */
	private TRUBatteryQuantitynTypeVO mBatteryQuantitynTypeVO;

	/** property to hold mStyleAttributeId . */
	private String mStyleAttributeId;

	/** property to hold TRUStyleProductFeedVO. */
	private TRUProductFeedVO mTRUStyleProductFeedVO;

	/** property to hold Feed. */
	private String mFeed;

	/** property to hold DeltaFeedSKUInSideMap. */
	private Boolean mDeltaFeedSKUInSideMap = Boolean.FALSE;

	/** property to hold DeltaFeedProductInSideMap. */
	private Boolean mDeltaFeedProductInSideMap = Boolean.FALSE;

	/** property to hold ChildProducts. */
	private Map<String, TRUCollectionProductVO> mCollectionProdcutsMap;

	/** The mInventoryFlag. */
	private Boolean mInventoryFlag;
	
	/** The Feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** property to hold mSequenceNumber. */
	private String mSequenceNumber = null;
	
	/** The Classification id. */
	private String mClassificationId = null;
	
	/** property to hold FileName . */
	private String mFileName;
	
	/** The Empty sku properties flag. */
	private Boolean mEmptySkuPropertiesFlag;
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}
	
	/** property to hold Logging. */
	public static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(
			TRUProductFeedHandler.class);
	
	/** property to hold ClassificationIdAndRootCategoryMap. */
	private Map<String, Set<String>> mClassificationIdAndRootCategoryMap;
	
	/**
	 * Checks if is inventory flag.
	 * 
	 * @return the inventoryFlag
	 */
	public Boolean isInventoryFlag() {
		return mInventoryFlag;
	}

	/**
	 * Sets the inventory flag.
	 * 
	 * @param pInventoryFlag
	 *            the inventoryFlag to set
	 */
	public void setInventoryFlag(Boolean pInventoryFlag) {
		mInventoryFlag = pInventoryFlag;
	}

	/**
	 * Gets the collection prodcuts map.
	 * 
	 * @return the collection prodcuts map
	 */
	public Map<String, TRUCollectionProductVO> getCollectionProdcutsMap() {
		if (mCollectionProdcutsMap == null) {
			mCollectionProdcutsMap = new HashMap<String, TRUCollectionProductVO>();
		}
		return mCollectionProdcutsMap;
	}

	/**
	 * Sets the collection prodcuts map.
	 * 
	 * @param pCollectionProdcutsMap
	 *            the collection prodcuts map
	 */
	public void setCollectionProdcutsMap(Map<String, TRUCollectionProductVO> pCollectionProdcutsMap) {
		this.mCollectionProdcutsMap = pCollectionProdcutsMap;
	}

	/**
	 * Gets the feed.
	 * 
	 * @return the feed
	 */
	public String getFeed() {
		return mFeed;
	}

	/**
	 * Sets the feed.
	 * 
	 * @param pFeed
	 *            the feed to set
	 */
	public void setFeed(String pFeed) {
		mFeed = pFeed;
	}
	
	/**
	 * Gets the TRUClassificationIdAndRootCategoryMap.
	 * 
	 * @return the TRUClassificationIdAndRootCategoryMap
	 */
	public Map<String, Set<String>> getClassificationIdAndRootCategoryMap() {
		if (mClassificationIdAndRootCategoryMap == null) {
			mClassificationIdAndRootCategoryMap = new HashMap<String, Set<String>>();
		}
		return mClassificationIdAndRootCategoryMap;
	}

	/**
	 * Sets the classification id and root category map.
	 * 
	 * @param pClassificationIdAndRootCategoryMap
	 *            the classification id and root category map
	 */
	public void setClassificationIdAndRootCategoryMap(Map<String, Set<String>> pClassificationIdAndRootCategoryMap) {
		this.mClassificationIdAndRootCategoryMap = pClassificationIdAndRootCategoryMap;
	}

	/** property to hold mTRUCategory. */
	private TRUCategory mTRUCategory;

	/**  property to hold  Classification reference. */
	private Boolean mClassificationReference = Boolean.FALSE;

	/** property to hold  classification ref meta data. */
	private Boolean mClassRefMetaData = Boolean.FALSE;

	/** The Color family. */
	private Boolean mColorFamily = Boolean.FALSE;

	/** The Size family. */
	private Boolean mSizeFamily = Boolean.FALSE;

	/** The Character theme. */
	private Boolean mCharacterTheme = Boolean.FALSE;

	/** The Collection theme. */
	private Boolean mCollectionTheme = Boolean.FALSE;

	/**
	 * Gets the TRU category.
	 * 
	 * @return the TRUCategory
	 */
	public TRUCategory getTRUCategory() {
		if (mTRUCategory == null) {
			mTRUCategory = new TRUCategory();
		}
		return mTRUCategory;
	}

	/**
	 * Sets the TRU category.
	 * 
	 * @param pTRUCategory
	 *            the TRUCategory to set
	 */
	public void setTRUCategory(TRUCategory pTRUCategory) {
		mTRUCategory = pTRUCategory;
	}

	/**
	 * Gets the TRU products vo.
	 * 
	 * @return the TRUProductsVO
	 */
	public TRUProductsVO getTRUProductsVO() {
		if (mTRUProductsVO == null) {
			mTRUProductsVO = new TRUProductsVO();
		}
		return mTRUProductsVO;
	}

	/**
	 * Sets the TRU products vo.
	 * 
	 * @param pTRUProductsVO
	 *            the tRUProductsVO to set
	 */
	public void setTRUProductsVO(TRUProductsVO pTRUProductsVO) {
		mTRUProductsVO = pTRUProductsVO;
	}

	/**
	 * This method invokes the start tag.
	 * 
	 * @param pUri
	 *            the uri
	 * @param pLocalName
	 *            the local name
	 * @param pElementName
	 *            the element name
	 * @param pAttributes
	 *            the attributes
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void startElement(String pUri, String pLocalName, String pElementName, Attributes pAttributes)
			throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("Start @Class: TRUProductFeedHandler, @method: startElement()");
		}

		if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT)) {
			mTRUProductFeedVO = new TRUProductFeedVO();
			mTRUProductFeedVO.setSequenceNumber(getSequenceNumber());
			mTRUProductFeedVO.setFileName(getFileName());
			mIsProductFeedVO = Boolean.TRUE;
			mTRUProductFeedVO.setFeed(getFeed());
			if ((mTRUProductFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) || mTRUProductFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELETE)) && StringUtils.isBlank(mTRUProductFeedVO.getFeedActionCode())) {
				mTRUProductFeedVO.setFeedActionCode(pAttributes.getValue(TRUProductFeedConstants.ACTION_CODE));
			}
			mMetadata = Boolean.FALSE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SKU)) {
			if (getFeed().equalsIgnoreCase(TRUProductFeedConstants.FULL) ||
					getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELETE) ||
					(getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) &&
							getTRUProductsVO().getTRUSKUFeedVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null &&
							getTRUProductsVO().getTRUStrollerSKUMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null &&
							getTRUProductsVO().getTRUCribSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null &&
							getTRUProductsVO().getTRUCarSeatSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null &&
							getTRUProductsVO().getTRUBookCdDvDSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null &&
							getTRUProductsVO().getTRUVideoGameSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) == null)) {
				mTRUSKUFeedVO = new TRUSkuFeedVO();
				mTRUSKUFeedVO.setSequenceNumber(getSequenceNumber());
				mTRUSKUFeedVO.setFileName(getFileName());
				mTRUSKUFeedVO.setInventoryFlag(isInventoryFlag());
			} else {
				if (getTRUProductsVO().getTRUSKUFeedVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUSKUFeedVOMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				} else if (getTRUProductsVO().getTRUStrollerSKUMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUStrollerSKUMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				} else if (getTRUProductsVO().getTRUCribSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUCribSKUVOMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				} else if (getTRUProductsVO().getTRUCarSeatSKUVOMap().get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUCarSeatSKUVOMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				} else if (getTRUProductsVO().getTRUBookCdDvDSKUVOMap()
						.get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUBookCdDvDSKUVOMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				} else if (getTRUProductsVO().getTRUVideoGameSKUVOMap()
						.get(pAttributes.getValue(TRUProductFeedConstants.ID)) != null) {
					mTRUSKUFeedVO = getTRUProductsVO().getTRUVideoGameSKUVOMap().get(
							pAttributes.getValue(TRUProductFeedConstants.ID));
				}
			}

			mTRUSKUFeedVO.setId(pAttributes.getValue(TRUProductFeedConstants.ID));
			mTRUSKUFeedVO.setRepositoryId(pAttributes.getValue(TRUProductFeedConstants.ID));
			mIsSKUFeedVO = Boolean.TRUE;
			mTRUSKUFeedVO.setIsDeleted(TRUProductFeedConstants.N);
			mTRUSKUFeedVO.setFeed(getFeed());
			mSkuClassificationIds = new ArrayList<String>();
			if ((mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) || mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELETE)) && StringUtils.isBlank(mTRUSKUFeedVO.getFeedActionCode())) {
				mTRUSKUFeedVO.setFeedActionCode(pAttributes.getValue(TRUProductFeedConstants.ACTION_CODE));
			}
			mMetadata = Boolean.FALSE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.REFERENCE) && mIsSKUFeedVO){
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.PARENT_CLASSIFICATIONS);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CLASSIFICATION_REFERENCE) && mIsSKUFeedVO) {
			String classRefType = (String) pAttributes.getValue(TRUProductFeedConstants.TYPE);
			if (mIsSKUFeedVO
					&& (classRefType.equalsIgnoreCase(TRUProductFeedConstants.NAVIGATION_CATEGORY) || classRefType
							.equalsIgnoreCase(TRUProductFeedConstants.FILTERED_LANDING_PAGE))) {
				mClassificationReference = Boolean.TRUE;
				Set<String> rootParentCategory = null;
				String[] classifacation_array = ((String) pAttributes.getValue(TRUProductFeedConstants.CLASSIFICATION_ID))
						.split(TRUProductFeedConstants.UNDER_SCORE);
				if (classifacation_array.length >= FeedConstants.NUMBER_ONE && 
						!StringUtils.isBlank(classifacation_array[TRUProductFeedConstants.NUMBER_ZERO])) {
					mClassificationId = classifacation_array[TRUProductFeedConstants.NUMBER_ZERO];
					if (getClassificationIdAndRootCategoryMap().get(mClassificationId) != null) {
						rootParentCategory = getClassificationIdAndRootCategoryMap().get(mClassificationId);
					}
				}
				if(!mSkuClassificationIds.contains(mClassificationId)){
					mSkuClassificationIds.add(mClassificationId);
				}
				
				if(!mTRUProductFeedVO.getParentClassifications().contains(mClassificationId)){
					mTRUProductFeedVO.getParentClassifications().add(mClassificationId);
				}
				if (rootParentCategory != null) {
					mTRUSKUFeedVO.getRootParentCategories().addAll(rootParentCategory);
					mTRUProductFeedVO.getRootParentCategories().addAll(rootParentCategory);
				}
				mTRUProductFeedVO.setClassificationIdAndRootCategoryMap(getClassificationIdAndRootCategoryMap());
			} else if (mIsSKUFeedVO && 
					((String) pAttributes.getValue(TRUProductFeedConstants.TYPE))
							.equalsIgnoreCase(TRUProductFeedConstants.COLLECTIONSPAGE)) {
				mClassificationReference = Boolean.TRUE;
				Set<String> rootParentCategory = null;
				String classificationId = null;
				String[] classifacation_array = ((String) pAttributes.getValue(TRUProductFeedConstants.CLASSIFICATION_ID))
						.split(TRUProductFeedConstants.UNDER_SCORE);
				if (classifacation_array.length >= FeedConstants.NUMBER_ONE && 
						!StringUtils.isBlank(classifacation_array[TRUProductFeedConstants.NUMBER_ZERO])) {
					classificationId = classifacation_array[TRUProductFeedConstants.NUMBER_ZERO];
					if (getCollectionProdcutsMap().get(classificationId) != null) {
						getCollectionProdcutsMap().get(classificationId).getChildSKUS().add(mTRUSKUFeedVO.getId());
						rootParentCategory = getClassificationIdAndRootCategoryMap().get(classificationId);
						if (rootParentCategory != null){
							mTRUProductFeedVO.getRootParentCategories().addAll(rootParentCategory);
						}
					}
				}
				if(getCollectionProdcutsMap().get(classificationId) != null) {
					mTRUSKUFeedVO.getOnlineCollectionName().put(classificationId, getCollectionProdcutsMap().get(classificationId).getOnlineTitle());
				}
				
			} else if (mIsProductFeedVO && !mIsSKUFeedVO) {
				mClassificationReference = Boolean.TRUE;
				if (pAttributes.getValue(TRUProductFeedConstants.TYPE).equalsIgnoreCase(
						TRUProductFeedConstants.US_REGISTRY_CAT)) {
					mTRUProductFeedVO.setRegistryClassification((String) pAttributes
							.getValue(TRUProductFeedConstants.CLASSIFICATION_ID));
				} else if (pAttributes.getValue(TRUProductFeedConstants.TYPE).equalsIgnoreCase(
						TRUProductFeedConstants.VENDOR)) {
					mTRUProductFeedVO.setVendorClassification((String) pAttributes
							.getValue(TRUProductFeedConstants.CLASSIFICATION_ID));
				}
			}

		} else if(pElementName.equalsIgnoreCase(TRUProductFeedConstants.METADATA) && mClassificationReference){
			mClassRefMetaData = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_CROSS_REFERENCE)) {
			mProductCrossReference = Boolean.TRUE;
			if (mIsSKUFeedVO && pAttributes.getValue(TRUProductFeedConstants.TYPE).equalsIgnoreCase(TRUProductFeedConstants.CROSSSELL)) {
				mProductId = pAttributes.getValue(TRUProductFeedConstants.PRODUCT_ID);
				mCrossSell = Boolean.TRUE;
			} else if(!mIsSKUFeedVO && pAttributes.getValue(TRUProductFeedConstants.TYPE).equalsIgnoreCase(TRUProductFeedConstants.BATTERIES)){
				mProductId = pAttributes.getValue(TRUProductFeedConstants.PRODUCT_ID);
				mBatteries = Boolean.TRUE;
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.METADATA) && mProductCrossReference) {
			mMetadata = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VALUE) && !mIsStyle) {
			if (mProductCrossReference && mCrossSell && mMetadata) {
				mAttributeId = pAttributes.getValue(TRUProductFeedConstants.ATTRIBUTE_ID);
			} else if (mProductCrossReference && mBatteries && mMetadata) {
				mAttributeId = pAttributes.getValue(TRUProductFeedConstants.ATTRIBUTE_ID);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.IMAGE) && mIsSKUFeedVO) {
			mImage = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DIGITAL_ASSET_IMAGE) && mIsSKUFeedVO) {
			mIsDigitalAssetImage = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRIMARY_IMAGE) && mIsSKUFeedVO) {
			mIsPrimaryImage = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SECONDARY_IMAGE) && mIsSKUFeedVO) {
			mIsSecondaryImage = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ALTERNATE_IMAGE) && mIsSKUFeedVO) {
			mIsAlternateImage = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.ALTERNATE_IMAGE);
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CANONICAL_IMAGE_URL);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTERNET_IMAGE_URL)
				&& (mIsDigitalAssetImage || mIsPrimaryImage || mIsSecondaryImage || mIsAlternateImage) && mIsSKUFeedVO) {
			mInternetImageURL = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CANONICAL_IMAGE_URL)
				&& (mIsDigitalAssetImage || mIsPrimaryImage || mIsSecondaryImage || mIsAlternateImage) && mIsSKUFeedVO) {
			mCanonicalImageURL = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BRAND_NAME_SECONDARY) && mIsSKUFeedVO) {
			mBRANDNAMESECONDARY = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.BRAND_NAME_SECONDARY);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BRANDNAMETERTIARY_CHARACTER_THEME) && mIsSKUFeedVO) {
			mBRANDNAMETERTIARYCHARACTERTHEME = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.BRANDNAMETERTIARY_CHARACTER_THEME);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.UPC_NUMBER) && mIsSKUFeedVO) {
			mUPCNUMBER = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.UPC_NUMBER);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_SAFETY_WARNING) && mIsSKUFeedVO) {
			mPRODUCTSAFETYWARNING = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.PRODUCT_SAFETY_WARNING);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_BEST_USES) && mIsSKUFeedVO) {
			mSTRLBESTUSES = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.STRL_BEST_USES);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_EXTRAS) && mIsSKUFeedVO) {
			mSTRLEXTRAS = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.STRL_EXTRAS);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_TYPE) && mIsSKUFeedVO) {
			mSTRLTYPE = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.STRL_TYPE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_WHAT_IS_IMP) && mIsSKUFeedVO) {
			mSTRLWHATISIMP = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.STRL_WHAT_IS_IMP);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_MATERIAL_TYPE) && mIsSKUFeedVO) {
			mCribMaterialTypes = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CRIB_MATERIAL_TYPE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_STYLE) && mIsSKUFeedVO) {
			mCribStyles = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CRIB_STYLE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_TYPE) && mIsSKUFeedVO) {
			mCribTypes = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CRIB_TYPE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_WHAT_IS_IMP) && mIsSKUFeedVO) {
			mCribWhatIsImp = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CRIB_WHAT_IS_IMP);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_FEATURES) && mIsSKUFeedVO) {
			mCarSeatFeatures = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CAR_SEAT_FEATURES);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_TYPE) && mIsSKUFeedVO) {
			mCarSeatTypes = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CAR_SEAT_TYPE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_WHAT_IS_IMP) && mIsSKUFeedVO) {
			mCarSeatWhatIsImp = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CAR_SEAT_WHAT_IS_IMP);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTEREST) && mIsSKUFeedVO) {
			mInterest = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.INTEREST);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FINDER_ELIGIBLE)) {
			mFinderEligible = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.FINDER_ELIGIBLE);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SPECIAL_FEATURES)) {
			mSpecialFeatures = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.SPECIAL_FEATURES);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLOR_FAMILY)) {
			mColorFamily = Boolean.TRUE;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SIZE_FAMILY)) {
			mSizeFamily = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.SIZE_FAMILY);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHARACTER_THEME)) {
			mCharacterTheme = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.CHARACTER_THEME);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLLECTION_THEME)) {
			mCollectionTheme = Boolean.TRUE;
			mTRUSKUFeedVO.getUpdatedListProps().add(TRUProductFeedConstants.COLLECTION_THEME);
		}

		// NonMerchSKU changes

		else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_ATTRIBUTE) && !mIsSKUFeedVO) {
			mItemAttribute = Boolean.TRUE;
		}

		// Delta Feed changes

		else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STYLE)) {
			mIsStyle = Boolean.TRUE;
			if (getTRUProductsVO().getTRUProductFeedVOMap().get(
					pAttributes.getValue(TRUProductFeedConstants.ID.toUpperCase())) != null) {
				mTRUStyleProductFeedVO = getTRUProductsVO().getTRUProductFeedVOMap().get(
						pAttributes.getValue(TRUProductFeedConstants.ID.toUpperCase()));
			} else {
				mTRUStyleProductFeedVO = new TRUProductFeedVO();
				mTRUStyleProductFeedVO.setSequenceNumber(getSequenceNumber());
				mTRUStyleProductFeedVO.setFileName(getFileName());
				mTRUStyleProductFeedVO.setId(pAttributes.getValue(TRUProductFeedConstants.ID.toUpperCase()));
				mTRUStyleProductFeedVO.setRepositoryId(pAttributes.getValue(TRUProductFeedConstants.ID.toUpperCase()));
			}
			mTRUStyleProductFeedVO.setFeed(getFeed());
			if (mTRUStyleProductFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && StringUtils.isBlank(mTRUStyleProductFeedVO.getFeedActionCode())) {
				mTRUStyleProductFeedVO.setFeedActionCode(pAttributes.getValue(TRUProductFeedConstants.ACTIONCODE));
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VALUE) && mIsStyle) {
			mStyleAttributeId = pAttributes.getValue(TRUProductFeedConstants.ATTRIBUTEID);
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DIMENSION) &&
				mIsStyle && !mTRUStyleProductFeedVO.getStyleDimensions() .contains(pAttributes.getValue(TRUProductFeedConstants.LABEL))) {
			mTRUStyleProductFeedVO.getStyleDimensions().add(pAttributes.getValue(TRUProductFeedConstants.LABEL));
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STYLEUID) && mIsStyle) {
			mTRUStyleProductFeedVO.getStyleUIDs().put(pAttributes.getValue(TRUProductFeedConstants.PRODUCTID),
					pAttributes.getValue(TRUProductFeedConstants.ACTIONCODE));
		}

		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("End @Class: TRUProductFeedHandler, @method: startElement()");
		}

	}

	/**
	 * This method collects the data between tags.
	 * 
	 * @param pCharacters
	 *            the characters
	 * @param pStart
	 *            the start
	 * @param pLength
	 *            the length
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void characters(char[] pCharacters, int pStart, int pLength) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("Start @Class: TRUProductFeedHandler, @method: characters()");
		}

		mCharactersBuffer = mCharactersBuffer.append(pCharacters, pStart, pLength);
		mCharacters = mCharactersBuffer.toString().trim();
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("End @Class: TRUProductFeedHandler, @method: characters()");
		}
	}

	/**
	 * This method invokes the end tag.
	 * 
	 * @param pUri
	 *            the uri
	 * @param pLocalName
	 *            the local name
	 * @param pElementName
	 *            the element name
	 * @throws SAXException
	 *             the SAX exception
	 */
	@Override
	public void endElement(String pUri, String pLocalName, String pElementName) throws SAXException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("Start @Class: TRUProductFeedHandler, @method: endElement()");
		}
		if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCTS)) {
			getTRUProductsVO().setTRUCategory(getTRUCategory());
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT)) {
			if (!mDeltaFeedProductInSideMap) {
				getTRUProductsVO().getTRUProductFeedVOList().add(mTRUProductFeedVO);
				getTRUProductsVO().getTRUProductFeedVOMap().put(mTRUProductFeedVO.getId(), mTRUProductFeedVO);
			}
			mIsProductFeedVO = Boolean.FALSE;
			mDeltaFeedProductInSideMap = Boolean.FALSE;
			mISNonMerchFlag = null;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SKU)) {
			if (mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && 
					getTRUProductsVO().getTRUSKUFeedVOMap().get(mTRUSKUFeedVO.getId()) != null) {
				mDeltaFeedSKUInSideMap = Boolean.TRUE;
			}
			mTRUSKUFeedVO.setParentClassifications(mSkuClassificationIds);
			if (!mTRUProductFeedVO.getChildSKUIds().contains(mTRUSKUFeedVO.getId()) && 
					(mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.FULL) || 
							(mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && !StringUtils.isBlank(mTRUSKUFeedVO.getFeedActionCode()) && 
									mTRUSKUFeedVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.A)))) {
				mTRUProductFeedVO.getChildSKUIds().add(mTRUSKUFeedVO.getId());
			}
			mTRUSKUFeedVO.setOrignalParentProduct(mTRUProductFeedVO);
			if (!StringUtils.isBlank(mTRUSKUFeedVO.getFeed()) && 
					(mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) || mTRUSKUFeedVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELETE))&& 
					!StringUtils.isBlank(mTRUSKUFeedVO.getFeedActionCode()) &&
					mTRUSKUFeedVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.D)) {
				mTRUSKUFeedVO.setIsDeleted(TRUProductFeedConstants.Y);
				mTRUSKUFeedVO.setWebDisplayFlag(TRUProductFeedConstants.N);
				mTRUSKUFeedVO.setSupressInSearch(TRUProductFeedConstants.Y);
				mTRUSKUFeedVO.setEmptySkuPropertiesFlag(getEmptySkuPropertiesFlag());
			}
			
			if(!StringUtils.isBlank(mTRUProductFeedVO.getSource()) && !mTRUProductFeedVO.getSource().equalsIgnoreCase(TRUProductFeedConstants.RMS)){
				mTRUSKUFeedVO.setDivisionCode(mTRUProductFeedVO.getDivisionCode());
				mTRUSKUFeedVO.setDepartmentCode(mTRUProductFeedVO.getDepartmentCode());
				mTRUSKUFeedVO.setClassCode(mTRUProductFeedVO.getClassCode());
				mTRUSKUFeedVO.setSubclassCode(mTRUProductFeedVO.getSubclassCode());
			}
			if(mISNonMerchFlag != null && mISNonMerchFlag.equalsIgnoreCase(TRUProductFeedConstants.NONMERCHSKU)){
				mSKUType = mISNonMerchFlag;
			}
			ConvertUtils.register(new DateConverter(null), Date.class);
			try {
				if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.STROLLER_SKU)) {
					if (getTRUProductsVO().getTRUStrollerSKUMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUStrollerSKUVO = new TRUStrollerSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.STROLLER_SKU);
						BeanUtils.copyProperties(mTRUStrollerSKUVO, mTRUSKUFeedVO);
						mTRUStrollerSKUVO.setEntityName(TRUProductFeedConstants.STROLLER_SKU);
						getTRUProductsVO().getTRUStrollerSKUList().add(mTRUStrollerSKUVO);
						getTRUProductsVO().getTRUStrollerSKUMap().put(mTRUStrollerSKUVO.getId(), mTRUStrollerSKUVO);
						mTRUProductFeedVO.getChildSKUsList().add(mTRUStrollerSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.STROLLER_SKU);
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
					}
				} else if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.CRIB_SKU)) {
					if (getTRUProductsVO().getTRUCribSKUVOMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUCribSKUVO = new TRUCribSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.CRIB_SKU);
						BeanUtils.copyProperties(mTRUCribSKUVO, mTRUSKUFeedVO);
						mTRUCribSKUVO.setEntityName(TRUProductFeedConstants.CRIB_SKU);
						getTRUProductsVO().getTRUCribSKUVOList().add(mTRUCribSKUVO);
						getTRUProductsVO().getTRUCribSKUVOMap().put(mTRUCribSKUVO.getId(), mTRUCribSKUVO);
						mTRUProductFeedVO.getChildSKUsList().add(mTRUCribSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.CRIB_SKU);
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
					}
				} else if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_SKU)) {
					if (getTRUProductsVO().getTRUCarSeatSKUVOMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUCarSeatSKUVO = new TRUCarSeatSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.CAR_SEAT_SKU);
						BeanUtils.copyProperties(mTRUCarSeatSKUVO, mTRUSKUFeedVO);
						mTRUCarSeatSKUVO.setEntityName(TRUProductFeedConstants.CAR_SEAT_SKU);
						getTRUProductsVO().getTRUCarSeatSKUVOList().add(mTRUCarSeatSKUVO);
						getTRUProductsVO().getTRUCarSeatSKUVOMap().put(mTRUCarSeatSKUVO.getId(), mTRUCarSeatSKUVO);
						mTRUProductFeedVO.getChildSKUsList().add(mTRUCarSeatSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.CAR_SEAT_SKU);
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
					}
				} else if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.BOOKCDDVD_SKU)) {
					if (getTRUProductsVO().getTRUBookCdDvDSKUVOMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUBookCdDvDSKUVO = new TRUBookCdDvDSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.BOOKCDDVD_SKU);
						BeanUtils.copyProperties(mTRUBookCdDvDSKUVO, mTRUSKUFeedVO);
						mTRUBookCdDvDSKUVO.setEntityName(TRUProductFeedConstants.BOOKCDDVD_SKU);
						getTRUProductsVO().getTRUBookCdDvDSKUVOList().add(mTRUBookCdDvDSKUVO);
						getTRUProductsVO().getTRUBookCdDvDSKUVOMap().put(mTRUBookCdDvDSKUVO.getId(), mTRUBookCdDvDSKUVO);
						mTRUProductFeedVO.getChildSKUsList().add(mTRUBookCdDvDSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.BOOKCDDVD_SKU);
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
					}
				} else if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.VIDEOGAME_SKU)) {
					if (getTRUProductsVO().getTRUVideoGameSKUVOMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUVideoGameSKUVO = new TRUVideoGameSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.VIDEOGAME_SKU);
						BeanUtils.copyProperties(mTRUVideoGameSKUVO, mTRUSKUFeedVO);
						mTRUVideoGameSKUVO.setEntityName(TRUProductFeedConstants.VIDEOGAME_SKU);
						getTRUProductsVO().getTRUVideoGameSKUVOList().add(mTRUVideoGameSKUVO);
						getTRUProductsVO().getTRUVideoGameSKUVOMap().put(mTRUVideoGameSKUVO.getId(), mTRUVideoGameSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.VIDEOGAME_SKU);
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mTRUProductFeedVO.getChildSKUsList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
						mTRUProductFeedVO.getChildSKUsList().add(mTRUVideoGameSKUVO);
					}
				} else if (null != mSKUType && mSKUType.equalsIgnoreCase(TRUProductFeedConstants.NONMERCHSKU)) {
					if (getTRUProductsVO().getTRUNonMerchSKUVOMap().get(mTRUSKUFeedVO.getId()) == null || mDeltaFeedSKUInSideMap) {
						mTRUSKUFeedVO.setSubType(TRUProductFeedConstants.UNKNOWN);
						//mTRUSKUFeedVO.setOnlineTitle(TRUProductFeedConstants.NONMERCHSKU);
						mTRUNonMerchSKUVO = new TRUNonMerchSKUVO();
						mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.NONMERCHSKU);
						BeanUtils.copyProperties(mTRUNonMerchSKUVO, mTRUSKUFeedVO);
						mTRUNonMerchSKUVO.setEntityName(TRUProductFeedConstants.NONMERCHSKU);
						getTRUProductsVO().getTRUNonMerchSKUVOList().add(mTRUNonMerchSKUVO);
						getTRUProductsVO().getTRUNonMerchSKUVOMap().put(mTRUNonMerchSKUVO.getId(), mTRUNonMerchSKUVO);
						mTRUProductFeedVO.setType(TRUProductFeedConstants.NONMERCHSKU);
						mTRUProductFeedVO.getRootParentCategories().add(TRUProductFeedConstants.NAVIGATION_TRU);
						mTRUProductFeedVO.getRootParentCategories().add(TRUProductFeedConstants.NAVIGATION_BRU);
						/*if (!getTRUCategory().getTRUFixedChildProducts().contains(mTRUProductFeedVO.getId())) {
							getTRUCategory().getTRUFixedChildProducts().add(mTRUProductFeedVO.getId());
						}
						if (!getTRUCategory().getBRUFixedChildProducts().contains(mTRUProductFeedVO.getId())) {
							getTRUCategory().getBRUFixedChildProducts().add(mTRUProductFeedVO.getId());
						}*/
						if (mDeltaFeedSKUInSideMap) {
							getTRUProductsVO().getTRUSKUFeedVOMap().remove(mTRUSKUFeedVO.getId());
							getTRUProductsVO().getTRUSKUFeedVOList().remove(mTRUSKUFeedVO);
							mDeltaFeedSKUInSideMap = Boolean.FALSE;
						}
						mTRUProductFeedVO.getChildSKUsList().add(mTRUNonMerchSKUVO);
					}
				} else if (StringUtils.isBlank(mTRUSKUFeedVO.getSKUType()) || 
						(getTRUProductsVO().getTRUSKUFeedVOMap().get(mTRUSKUFeedVO.getId()) == null)) {
					mTRUSKUFeedVO.setSKUType(TRUProductFeedConstants.REGULAR_SKU);
					mTRUProductFeedVO.setType(TRUProductFeedConstants.REGULAR_SKU);
					mTRUProductFeedVO.getChildSKUsList().add(mTRUSKUFeedVO);
					getTRUProductsVO().getTRUSKUFeedVOList().add(mTRUSKUFeedVO);
					getTRUProductsVO().getTRUSKUFeedVOMap().put(mTRUSKUFeedVO.getId(), mTRUSKUFeedVO);
					mDeltaFeedSKUInSideMap = Boolean.FALSE;
				}
			} catch (IllegalAccessException ile) {
				mLogging.logError("IllegalAccessException in product feed handler", ile);
			} catch (InvocationTargetException ine) {
				mLogging.logError("InvocationTargetException in product feed handler", ine);
			}
			mIsSKUFeedVO = Boolean.FALSE;
			mSKUType = null;
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ONLINE_TITLE)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setOnlineTitle(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setOnlineTitle(mCharacters);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ONLINE_LONG_DESC)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setOnlineLongDesc(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setOnlineLongDesc(mCharacters);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DIVISION_CODE)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setDivisionCode(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setDivisionCode(mCharacters);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DEPARTMENT_CODE)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setDepartmentCode(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setDepartmentCode(mCharacters);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CLASS_CODE)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setClassCode(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setClassCode(mCharacters);
			}
		} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SUBCLASS_CODE)) {
			if (mIsSKUFeedVO) {
				mTRUSKUFeedVO.setSubclassCode(mCharacters);
			} else if (mIsProductFeedVO) {
				mTRUProductFeedVO.setSubclassCode(mCharacters);
			}
		} else if (mIsSKUFeedVO) {

			if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VALUE)) {
				if (mProductCrossReference && mCrossSell && mMetadata && 
						mAttributeId.equalsIgnoreCase(TRUProductFeedConstants.CROSS_SELLS_BATTERIES_ID) && !StringUtils.isBlank(mCharacters)) {
					mValue = mCharacters;
					mAttributeId = null;
				} else if (mBRANDNAMESECONDARY && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getBrandNameSecondary().add(mCharacters);
				} else if (mBRANDNAMETERTIARYCHARACTERTHEME && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getBrandnametertiaryCharacterTheme().add(mCharacters);
				} else if (mUPCNUMBER && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getUpcNumbers().add(mCharacters);
				} else if (mPRODUCTSAFETYWARNING && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getProductSafetyWarnings().add(mCharacters);
				} else if (mSTRLBESTUSES && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getStrlBestUses().add(mCharacters);
				} else if (mSTRLEXTRAS && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getStrlExtras().add(mCharacters);
				} else if (mSTRLTYPE && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getStrlTypes().add(mCharacters);
				} else if (mSTRLWHATISIMP && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getStrlWhatIsImp().add(mCharacters);
				} else if (mCribMaterialTypes && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCribMaterialTypes().add(mCharacters);
				} else if (mCribStyles && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCribStyles().add(mCharacters);
				} else if (mCribTypes && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCribTypes().add(mCharacters);
				} else if (mCribWhatIsImp && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCribWhatIsImp().add(mCharacters);
				} else if (mCarSeatFeatures && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCarSeatFeatures().add(mCharacters);
				} else if (mCarSeatTypes && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCarSeatTypes().add(mCharacters);
				} else if (mCarSeatWhatIsImp && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCarSeatWhatIsImp().add(mCharacters);
				} else if (mInterest && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getInterest().add(mCharacters);
				} else if (mFinderEligible && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getFinderEligible().add(mCharacters);
				} else if (mSpecialFeatures && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getSpecialFeatures().add(mCharacters);
				} else if(mClassRefMetaData && mClassificationReference && StringUtils.isNotBlank(mCharacters) && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.Y)){
					mTRUSKUFeedVO.setPrimaryPathCategory(mClassificationId);
				} else if (mSizeFamily && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getSizeFamily().add(mCharacters);
				} else if (mCharacterTheme && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCharacterTheme().add(mCharacters);
				} else if (mCollectionTheme && !StringUtils.isBlank(mCharacters)) {
					mTRUSKUFeedVO.getCollectionTheme().add(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_CROSS_REFERENCE) && mCrossSell) {
				if (mMetadata) {
					mCrossSellsnUidBatteriesVO = new TRUCrossSellsnUidBatteriesVO();
					mCrossSellsnUidBatteriesVO.setCrossSellsBatteries(mValue);
					mCrossSellsnUidBatteriesVO.setUidBatteryCrossSell(mProductId);
					mTRUSKUFeedVO.getCrossSellsnUidBatteriesList().add(mCrossSellsnUidBatteriesVO);
				} else if (!mMetadata) {
					mTRUSKUFeedVO.getCrossSellSkuId().add(mProductId);
				}
				mProductCrossReference = Boolean.FALSE;
				mCrossSell = Boolean.FALSE;
				mBatteries = Boolean.FALSE;
				mMetadata = Boolean.FALSE;
				mProductId = null;
				mValue = null;
				mAttributeId = null;
			} else if(pElementName.equalsIgnoreCase(TRUProductFeedConstants.METADATA) && mClassificationReference){
				mClassRefMetaData = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BRAND_NAME_PRIMARY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getBrandNamePrimary());
				} else {
					mTRUSKUFeedVO.setBrandNamePrimary(mCharacters);
				}
			}  else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHANNEL_AVAILABILITY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getChannelAvailability());
				} else {
					mTRUSKUFeedVO.setChannelAvailability(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_IN_STORE_PICK_UP)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getItemInStorePickUp());
				} else {
					mTRUSKUFeedVO.setItemInStorePickUp(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTEREST)) {
				mInterest = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SKILLS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getSkills());
				} else {
					mTRUSKUFeedVO.setSkills(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRIMARY_CATEGORY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getPrimaryCategory());
				} else {
					mTRUSKUFeedVO.setPrimaryCategory(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PLAY_TYPE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getPlayType());
				} else {
					mTRUSKUFeedVO.setPlayType(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.WHAT_IS_IMPORTANT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getWhatIsImportant());
				} else {
					mTRUSKUFeedVO.setWhatIsImportant(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AK_HI_2_DAY_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAkhiMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AKHI2DAYFSDOLLAR);
				} else {
					mTRUSKUFeedVO.getAkhiMap().put(TRUProductFeedConstants.AKHI2DAYFSDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AK_HI_FIXED_SURCHARGE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAkhiMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AKHIFIXEDSURCHARGE);
				} else {
					mTRUSKUFeedVO.getAkhiMap().put(TRUProductFeedConstants.AKHIFIXEDSURCHARGE, mCharacters);
				}
				
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AK_HI_OVERNIGHT_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAkhiMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AKHIOVERNIGHTFSDOLLAR);
				} else {
					mTRUSKUFeedVO.getAkhiMap().put(TRUProductFeedConstants.AKHIOVERNIGHTFSDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AK_HI_STD_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAkhiMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AKHISTDFSDOLLARDOLLAR);
				} else {
					mTRUSKUFeedVO.getAkhiMap().put(TRUProductFeedConstants.AKHISTDFSDOLLARDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ASSEMBLED_DEPTH)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAssemblyMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ASSEMBLEDDEPTH);
				} else {
					mTRUSKUFeedVO.getAssemblyMap().put(TRUProductFeedConstants.ASSEMBLEDDEPTH, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ASSEMBLED_HEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAssemblyMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ASSEMBLEDHEIGHT);
				} else {
					mTRUSKUFeedVO.getAssemblyMap().put(TRUProductFeedConstants.ASSEMBLEDHEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ASSEMBLED_WEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAssemblyMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ASSEMBLEDWEIGHT);
				} else {
					mTRUSKUFeedVO.getAssemblyMap().put(TRUProductFeedConstants.ASSEMBLEDWEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ASSEMBLED_WIDTH)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAssemblyMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ASSEMBLEDWIDTH);
				} else {
					mTRUSKUFeedVO.getAssemblyMap().put(TRUProductFeedConstants.ASSEMBLEDWIDTH, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ASSEMBLY_REQUIRED)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getAssemblyMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ASSEMBLYREQUIRED);
				} else {
					mTRUSKUFeedVO.getAssemblyMap().put(TRUProductFeedConstants.ASSEMBLYREQUIRED, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BACKORDER_STATUS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getBackOrderStatus());
				} else {
					mTRUSKUFeedVO.setBackorderStatus(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BRAND_NAME_SECONDARY)) {
				mBRANDNAMESECONDARY = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BRANDNAMETERTIARY_CHARACTER_THEME)) {
				mBRANDNAMETERTIARYCHARACTERTHEME = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BROWSE_HIDDEN_KEYWORD)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getBrowseHiddenKeyword());
				} else {
					mTRUSKUFeedVO.setBrowseHiddenKeyword(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAN_BE_GIFT_WRAPPED)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getCanBeGiftWrapped());
				} else {
					mTRUSKUFeedVO.setCanBeGiftWrapped(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHILD_WEIGHT_MAX)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getChildWeightMax());
				} else {
					mTRUSKUFeedVO.setChildWeightMax(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHILD_WEIGHT_MIN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getChildWeightMin());
				} else {
					mTRUSKUFeedVO.setChildWeightMin(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLOR_CODE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getColorCode());
				} else {
					mTRUSKUFeedVO.setColorCode(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLOR_UPC_LEVEL)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getColorUpcLevel());
				} else {
					mTRUSKUFeedVO.setColorUpcLevel(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CUSTOMER_PURCHASE_LIMIT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getCustomerPurchaseLimit());
				} else {
					mTRUSKUFeedVO.setCustomerPurchaseLimit(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DROPSHIP_FLAG)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getDropShipFlag());
				} else {
					mTRUSKUFeedVO.setDropshipFlag(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FLEXIBLE_SHIPPING_PLAN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getFlexibleShippingPlan());
				} else {
					mTRUSKUFeedVO.setFlexibleShippingPlan(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FREIGHT_CLASS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getFreightClass());
				} else {
					mTRUSKUFeedVO.setFreightClass(mCharacters);
				}
			} else if(pElementName.equalsIgnoreCase(TRUProductFeedConstants.FURNITURE_FINISH)){
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getFurnitureFinish());
				} else {
					mTRUSKUFeedVO.setFurnitureFinish(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INCREMENTAL_SAFETY_STOCK_UNITS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getIncrementalSafetyStockUnits());
				} else {
					mTRUSKUFeedVO.setIncrementalSafetyStockUnits(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_DEPTH)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getItemDimensionMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ITEMDEPTH);
				} else {
					mTRUSKUFeedVO.getItemDimensionMap().put(TRUProductFeedConstants.ITEMDEPTH, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_HEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getItemDimensionMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ITEMHEIGHT);
				} else {
					mTRUSKUFeedVO.getItemDimensionMap().put(TRUProductFeedConstants.ITEMHEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_WEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getItemDimensionMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ITEMWEIGHT);
				} else {
					mTRUSKUFeedVO.getItemDimensionMap().put(TRUProductFeedConstants.ITEMWEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_WIDTH)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getItemDimensionMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ITEMWIDTH);
				} else {
					mTRUSKUFeedVO.getItemDimensionMap().put(TRUProductFeedConstants.ITEMWIDTH, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.JUVENILE_PRODUCT_SIZE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getJuvenileProductSize());
				} else {
					mTRUSKUFeedVO.setJuvenileProductSize(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LOWER48_2_DAY_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getLower48Map()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LOWER482DAYFSDOLLAR);
				} else {
					mTRUSKUFeedVO.getLower48Map().put(TRUProductFeedConstants.LOWER482DAYFSDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LOWER48_FIXED_SURCHARGE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getLower48Map()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LOWER48FIXEDSURCHARGE);
				} else {
					mTRUSKUFeedVO.getLower48Map().put(TRUProductFeedConstants.LOWER48FIXEDSURCHARGE, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LOWER48_OVERNIGHT_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getLower48Map()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LOWER48OVERNIGHTFSDOLLAR);
				} else {
					mTRUSKUFeedVO.getLower48Map().put(TRUProductFeedConstants.LOWER48OVERNIGHTFSDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LOWER48_STANDARD_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getLower48Map()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LOWER48STANDARDFSDOLLAR);
				} else {
					mTRUSKUFeedVO.getLower48Map().put(TRUProductFeedConstants.LOWER48STANDARDFSDOLLAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MAX_TARGET_AGE_TRU_WEBSITES)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getMaxTargetAgeTruWebsites());
				} else {
					mTRUSKUFeedVO.setMaxTargetAgeTruWebsites(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MFR_SUGGESTED_AGE_MAX)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getMfrSuggestedAgeMax());
				} else {
					mTRUSKUFeedVO.setMfrSuggestedAgeMax(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MFR_SUGGESTED_AGE_MIN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getMfrSuggestedAgeMin());
				} else {
					mTRUSKUFeedVO.setMfrSuggestedAgeMin(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MIN_TARGET_AGE_TRU_WEBSITES)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getMinTargetAgeTruWebsites());
				} else {
					mTRUSKUFeedVO.setMinTargetAgeTruWebsites(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NMWA_PERCENTAGE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getNmwaPercentage());
				} else if (mCharacters.contains(TRUProductFeedConstants.PERCENTAGE)) {
						String parts[] = mCharacters.split(TRUProductFeedConstants.PERCENTAGE);
						mCharacters = parts[0];
						mTRUSKUFeedVO.setNmwaPercentage(mCharacters);
				} else {
					mTRUSKUFeedVO.setNmwaPercentage(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_WINDOW_MAX)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.setShipWindowMin(TRUProductFeedConstants.NUMBER_FOURTY_EIGHT_STRING);
				} else {
					mTRUSKUFeedVO.setShipWindowMax(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_WINDOW_MIN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.setShipWindowMin(TRUProductFeedConstants.NUMBER_TWENTY_FOUR_STRING);
				} else {
					mTRUSKUFeedVO.setShipWindowMin(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ONLINE_PID)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getOnlinePID());
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getOriginalPID());
				} else {
					mTRUSKUFeedVO.setOnlinePID(mCharacters);
					mTRUSKUFeedVO.setOriginalPID(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ONLINE_PRODUCT_STREET_DATE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getStreetDate());
				} else {
					mTRUSKUFeedVO.setStreetDate(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INVENTORY_RECEIVED_DATE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getInventoryReceivedDate());
				} else {
					mTRUSKUFeedVO.setInventoryReceivedDate(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.RMS_SIZE_CODE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getRmsSizeCode());
				} else {
					mTRUSKUFeedVO.setRmsSizeCode(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.RMS_COLOR_CODE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getRmsColorCode());
				} else {
					mTRUSKUFeedVO.setRmsColorCode(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.OTHER_FIXED_SURCHARGE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getOtherFixedSurcharge());
				} else {
					mTRUSKUFeedVO.setOtherFixedSurcharge(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.OTHER_STD_F_S_DOLLAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getOtherStdFSDollar());
				} else {
					mTRUSKUFeedVO.setOtherStdFSDollar(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.OUTLET)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getOutlet());
				} else {
					mTRUSKUFeedVO.setOutlet(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRESELL_QUANTITY_UNITS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getPresellQuantityUnits());
				} else {
					mTRUSKUFeedVO.setPresellQuantityUnits(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRICE_DISPLAY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getPriceDisplay());
				} else {
					mTRUSKUFeedVO.setPriceDisplay(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_AWARDS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getProductAwards());
				} else {
					mTRUSKUFeedVO.setProductAwards(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_FEATURE1)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getProductFeature()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PRODUCTFEATURE1);
				} else {
					mTRUSKUFeedVO.getProductFeature().put(TRUProductFeedConstants.PRODUCTFEATURE1, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_FEATURE2)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getProductFeature()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PRODUCTFEATURE2);
				} else {
					mTRUSKUFeedVO.getProductFeature().put(TRUProductFeedConstants.PRODUCTFEATURE2, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_FEATURE3)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getProductFeature()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PRODUCTFEATURE3);
				} else {
					mTRUSKUFeedVO.getProductFeature().put(TRUProductFeedConstants.PRODUCTFEATURE3, mCharacters);
				}
				} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_FEATURE4)) {
					if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
						mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getProductFeature()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PRODUCTFEATURE4);
					} else {
						mTRUSKUFeedVO.getProductFeature().put(TRUProductFeedConstants.PRODUCTFEATURE4, mCharacters);
					}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_FEATURE5)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getProductFeature()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PRODUCTFEATURE5);
				} else {
					mTRUSKUFeedVO.getProductFeature().put(TRUProductFeedConstants.PRODUCTFEATURE5, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_SAFETY_WARNING)) {
				mPRODUCTSAFETYWARNING = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PROMOTIONAL_STICKER_DISPLAY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getPromotionalStickerDisplay());
				} else {
					mTRUSKUFeedVO.setPromotionalStickerDisplay(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.REGISTRY_WARNING_INDICATOR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getRegistryWarningIndicator());
				} else {
					mTRUSKUFeedVO.setRegistryWarningIndicator(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.REGISTRY_WISH_LIST_ELIGIBILITY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getRegistryEligibility());
				} else {
					mTRUSKUFeedVO.setRegistryEligibility(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SAFETY_STOCK)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getSafetyStock());
				} else {
					mTRUSKUFeedVO.setSafetyStock(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_IN_ORIG_CONTAINER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getShipInOrigContainer());
				} else {
					mTRUSKUFeedVO.setShipInOrigContainer(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SUPRESS_IN_SEARCH)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getSupressInSearch());
				} else {
					mTRUSKUFeedVO.setSupressInSearch(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SYSTEM_REQUIREMENTS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getSystemRequirements());
				} else {
					mTRUSKUFeedVO.setSystemRequirements(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.UPC_NUMBER)) {
				mUPCNUMBER = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NMWA)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getNmwa());
				} else {
					mTRUSKUFeedVO.setNmwa(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.WEB_DISPLAY_FLAG)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getWebDisplayFlag());
				} else {
					mTRUSKUFeedVO.setWebDisplayFlag(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BPP_ELIGIBLE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.setBppEligible(false);
				} else {
					if(mCharacters.equalsIgnoreCase(TRUProductFeedConstants.YES)){
						mTRUSKUFeedVO.setBppEligible(true);
					} else {
						mTRUSKUFeedVO.setBppEligible(false);
					}
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BPP_NAME)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getBppName());
				} else {
					mTRUSKUFeedVO.setBppName(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LEGAL_NOTICE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getLegalNotice());
				} else {
					mTRUSKUFeedVO.setLegalNotice(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SLAPPER_ITEM)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getSlapperItem());
				} else {
					mTRUSKUFeedVO.setSlapperItem(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_TO_STORE_ELIGIBLE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getShipToStoreEeligible());
				} else {
					mTRUSKUFeedVO.setShipToStoreEeligible(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_FROM_STORE_ELIGIBLE_LOV)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getShipFromStoreEligibleLov());
				} else {
					mTRUSKUFeedVO.setShipFromStoreEligibleLov(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VENDORS_PRODUCT_DEMO_URL)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getVendorsProductDemoUrl());
				} else {
					mTRUSKUFeedVO.setVendorsProductDemoUrl(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTERNET_IMAGE_URL) && 
					mInternetImageURL && mIsDigitalAssetImage) {
				mTRUSKUFeedVO.setSwatchImage(mCharacters);
				mInternetImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTERNET_IMAGE_URL) && 
					mInternetImageURL && mIsPrimaryImage) {
				mTRUSKUFeedVO.setPrimaryImage(mCharacters);
				mInternetImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTERNET_IMAGE_URL) && 
					mInternetImageURL && mIsSecondaryImage) {
				mTRUSKUFeedVO.setSecondaryImage(mCharacters);
				mInternetImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.INTERNET_IMAGE_URL) && 
					mInternetImageURL && mIsAlternateImage) {
				mInternetImageURL = Boolean.FALSE;
				if(!StringUtils.isBlank(mCharacters)){
					mTRUSKUFeedVO.getAlternateImage().add(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CANONICAL_IMAGE_URL) && 
					mCanonicalImageURL && mIsDigitalAssetImage) {
				mTRUSKUFeedVO.setSwatchCanonicalImage(mCharacters);
				mCanonicalImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CANONICAL_IMAGE_URL) && 
					mCanonicalImageURL && mIsPrimaryImage) {
				mTRUSKUFeedVO.setPrimaryCanonicalImage(mCharacters);
				mCanonicalImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CANONICAL_IMAGE_URL) && 
					mCanonicalImageURL && mIsSecondaryImage) {
				mTRUSKUFeedVO.setSecondaryCanonicalImage(mCharacters);
				mCanonicalImageURL = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CANONICAL_IMAGE_URL) && 
					mCanonicalImageURL && mIsAlternateImage) {
				mCanonicalImageURL = Boolean.FALSE;
				if(!StringUtils.isBlank(mCharacters)){
					mTRUSKUFeedVO.getAlternateCanonicalImage().add(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DIGITAL_ASSET_IMAGE) && mIsDigitalAssetImage && mIsSKUFeedVO) {
				mIsDigitalAssetImage = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRIMARY_IMAGE) && mIsPrimaryImage && mIsSKUFeedVO) {
				mIsPrimaryImage = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SECONDARY_IMAGE) && mIsSecondaryImage && mIsSKUFeedVO) {
				mIsSecondaryImage = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ALTERNATE_IMAGE) && mIsAlternateImage && mIsSKUFeedVO) {
				mIsAlternateImage = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.IMAGE) && mImage) {
				mImage = Boolean.FALSE;
			}

			/**
			 * BookCdDvdSKU Map properties START
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AUTHOR_ARTIST)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AUTHOR);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.AUTHOR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DATE_PUBLISHED)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.DATEPUBLISHED);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.DATEPUBLISHED, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DIRECTOR)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.DIRECTOR);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.DIRECTOR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.DVD_REGION)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.DVDREGION);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.DVDREGION, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FORMAT)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FORMAT);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.FORMAT, mCharacters);
				}
				
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MPAA_RATING)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MPAARATING);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.MPAARATING, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NO_OF_DISKS)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.NOOFDISKS);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.NOOFDISKS, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NO_OF_PAGES)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.NOOFPAGES);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.NOOFPAGES, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PARENTAL_ADVISORY)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PARENTALADVISORY);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.PARENTALADVISORY, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PUBLISHER)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PUBLISHER);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.PUBLISHER, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.RUN_TIME)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.RUNTIME);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.RUNTIME, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STUDIO)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.BOOKCDDVD_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getBookCdDvdPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STUDIO);
				} else {
					mTRUSKUFeedVO.getBookCdDvdPropertiesMap().put(TRUProductFeedConstants.STUDIO, mCharacters);
				}
				
			}

			/**
			 * Car Seat properties START
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_FEATURES)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				mCarSeatFeatures = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_TYPE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				mCarSeatTypes = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_WHAT_IS_IMP)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				mCarSeatWhatIsImp = Boolean.FALSE;
			}

			/**
			 * Car Seat Map properties START.
			 * 
			 */

			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_EASE_OF_USE_RATING)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CARSEATEASEOFUSERATING);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CARSEATEASEOFUSERATING, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_PATTERN)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CARSEATPATTERN);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CARSEATPATTERN, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_SIDE_IMP_PROTECT)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CARSEATSIDEIMPPROTECT);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CARSEATSIDEIMPPROTECT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FIVE_POINT_HARNESS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FIVEPOINTHARNESS);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.FIVEPOINTHARNESS, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_COLOR_THEME)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CARSEATCOLORTHEME);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CARSEATCOLORTHEME, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FULL_COVERAGE_CANOPY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLCOVERAGECANOPY);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.FULLCOVERAGECANOPY, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LATCH_INSTALLATION)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LATCHINSTALLATION);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.LATCHINSTALLATION, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SIDE_IMPACT_PROTECTION)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CAR_SEAT_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.SIDEIMPACTPROTECTION);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.SIDEIMPACTPROTECTION, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FULL_RECLINING_SEAT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLRECLININGSEAT);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.FULLRECLININGSEAT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHILD_GRAB_BAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CHILDGRABBAR);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CHILDGRABBAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ADJUSTABLE_HEADREST)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADJUSTABLEHEADREST);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.ADJUSTABLEHEADREST, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CAR_SEAT_WEIGHT_WITHOUT_BASE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CARSEATWEIGHTWITHOUTBASE);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CARSEATWEIGHTWITHOUTBASE, mCharacters);
				}
			} else if (pElementName
					.equalsIgnoreCase(TRUProductFeedConstants.CONVERTS_FROM_5PT_HARNESS_TO_BELT_POSITIONING_BOOSTER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CONVERTSFROM5PTHARNESSTOBELTPOSITIONINGBOOSTER);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CONVERTSFROM5PTHARNESSTOBELTPOSITIONINGBOOSTER, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CONVERTS_FROM_CONVERTIBLE_TO_BOOSTER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CONVERTSFROMCONVERTIBLETOBOOSTER);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CONVERTSFROMCONVERTIBLETOBOOSTER, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STEEL_REINFORCED_FRAME)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STEELREINFORCEDFRAME);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.STEELREINFORCEDFRAME, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COMPATIBLE_STROLLER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.COMPATIBLESTROLLER);
				} else {
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.COMPATIBLESTROLLER, mCharacters);
				}
			}

			/**
			 * Stroller properties START.
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_BEST_USES)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				mSTRLBESTUSES = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_EXTRAS)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				mSTRLEXTRAS = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_TYPE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				mSTRLTYPE = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_WHAT_IS_IMP)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				mSTRLWHATISIMP = Boolean.FALSE;
			}

			/**
			 * Stroller Map properties START.
			 * 
			 */

			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_CAR_SEAT_COMPATIBLE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STRLCARSEATCOMPATIBLE);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STRLCARSEATCOMPATIBLE, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_CHILDREN_CAPACITY)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STRLCHILDRENCAPACITY);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STRLCHILDRENCAPACITY, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STRL_PATTERN)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.STROLLER_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STRLPATTERN);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STRLPATTERN, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ALL_TERRAIN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ALLTERRAIN);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.ALLTERRAIN, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FULL_COVERAGE_CANOPY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLCOVERAGECANOPY);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.FULLCOVERAGECANOPY, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PARENT_CUP_HOLDER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.PARENTCUPHOLDER);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.PARENTCUPHOLDER, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FULL_RECLINING_SEAT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLRECLININGSEAT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.FULLRECLININGSEAT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AIR_FILL_TIRES)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AIRFILLTIRES);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.AIRFILLTIRES, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SUSPENSION)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.SUSPENSION);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.SUSPENSION, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COMPACT_FOLD)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.COMPACTFOLD);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.COMPACTFOLD, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHILD_GRAB_BAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CHILDGRABBAR);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.CHILDGRABBAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ADJUSTABLE_HEADREST)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADJUSTABLEHEADREST);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.ADJUSTABLEHEADREST, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STROLLER_WEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STROLLERWEIGHT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STROLLERWEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LOCKING_WHEELS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LOCKINGWHEELS);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.LOCKINGWHEELS, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STEEL_REINFORCED_FRAME)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STEELREINFORCEDFRAME);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STEELREINFORCEDFRAME, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ADJUSTABLE_HARNESS)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADJUSTABLEHARNESS);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.ADJUSTABLEHARNESS, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STORAGE_BASKET)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STORAGEBASKET);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STORAGEBASKET, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MATCHING_CAR_SEAT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MATCHINGCARSEAT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.MATCHINGCARSEAT, mCharacters);
				}
			}

			/**
			 * Common properties For CarSeatSKU, StrollerSKU START.
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CUP_OR_BOTTLE_HOLDER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CUPORBOTTLEHOLDER);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CUPORBOTTLEHOLDER);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.CUPORBOTTLEHOLDER, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.CUPORBOTTLEHOLDER, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ADJUSTABLE_HANDLEBAR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADJUSTABLEHANDLEBAR);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADJUSTABLEHANDLEBAR);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.ADJUSTABLEHANDLEBAR, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.ADJUSTABLEHANDLEBAR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.LIGHT_WEIGHT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LIGHTWEIGHT);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.LIGHTWEIGHT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.LIGHTWEIGHT, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.LIGHTWEIGHT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.AIR_VENT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AIRVENT);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.AIRVENT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.AIRVENT, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.AIRVENT, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ADDITIONAL_HEAD_PROTECTION)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADDITIONALHEADPROTECTION);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.ADDITIONALHEADPROTECTION);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.ADDITIONALHEADPROTECTION, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.ADDITIONALHEADPROTECTION, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MODULARITY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MODULARITY);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MODULARITY);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.MODULARITY, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.MODULARITY, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.HIGH_PERFORMANCE_FABRIC)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.HIGHPERFORMANCEFABRIC);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.HIGHPERFORMANCEFABRIC);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.HIGHPERFORMANCEFABRIC, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.HIGHPERFORMANCEFABRIC, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MACHINE_WASHABLE)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MACHINEWASHABLE);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.MACHINEWASHABLE);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.MACHINEWASHABLE, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.MACHINEWASHABLE, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.RECLINE_INDICATOR)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.RECLINEINDICATOR);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.RECLINEINDICATOR);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.RECLINEINDICATOR, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.RECLINEINDICATOR, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FULL_CANOPY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLCANOPY);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCarSeatPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.FULLCANOPY);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.FULLCANOPY, mCharacters);
					mTRUSKUFeedVO.getCarSeatPropertiesMap().put(TRUProductFeedConstants.FULLCANOPY, mCharacters);
				}
			}

			/**
			 * Common properties For CribSKU, StrollerSKU START.
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STORAGE_COMPARTMENT)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getStrollerPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STORAGECOMPARTMENT);
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.STORAGECOMPARTMENT);
				} else {
					mTRUSKUFeedVO.getStrollerPropertiesMap().put(TRUProductFeedConstants.STORAGECOMPARTMENT, mCharacters);
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.STORAGECOMPARTMENT, mCharacters);
				}
			}

			/**
			 * Crib properties START.
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_MATERIAL_TYPE)) {
				mCribMaterialTypes = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_STYLE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				mCribStyles = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_TYPE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				mCribTypes = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_WHAT_IS_IMP)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				mCribWhatIsImp = Boolean.FALSE;
			}

			/**
			 * Crib Map properties START.
			 * 
			 */

			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_BEDRAILS)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CRIBBEDRAILS);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.CRIBBEDRAILS, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_CONVERSION_KIT_INCLUDED)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CRIBCONVERSIONKITINCLUDED);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.CRIBCONVERSIONKITINCLUDED, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_HARDWARE_VISIBLE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CRIBHARDWAREVISIBLE);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.CRIBHARDWAREVISIBLE, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_STORAGE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.CRIB_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CRIBSTORAGE);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.CRIBSTORAGE, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CRIB_DESIGN)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.CRIBDESIGN);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.CRIBDESIGN, mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SAFETY_TETHER)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullSubProperties().add(getFeedCatalogProperty().getCribPropertiesMap()+TRUProductFeedConstants.DOT+TRUProductFeedConstants.SAFETYTETHER);
				} else {
					mTRUSKUFeedVO.getCribPropertiesMap().put(TRUProductFeedConstants.SAFETYTETHER, mCharacters);
				}
			
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLOR_FAMILY)) {
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getColorFamily());
				} else {
					mTRUSKUFeedVO.setColorFamily(mCharacters);
				}
				mColorFamily = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SIZE_FAMILY)) {
				mSizeFamily = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.CHARACTER_THEME)) {
				mCharacterTheme = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COLLECTION_THEME)) {
				mCollectionTheme = Boolean.FALSE;
			} 

			/**
			 * Common properties For CribSKU, CarSeatSKU, StrollerSKU removing from map.
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.FINDER_ELIGIBLE)) {
				mFinderEligible = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SPECIAL_FEATURES)) {
				mSpecialFeatures = Boolean.FALSE;
			}

			/**
			 * VideoGamesSKU Map properties START
			 * 
			 */
			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VIDEO_GAME_ESRB)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.VIDEOGAME_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getVideoGameEsrb());
				} else {
					mTRUSKUFeedVO.setVideoGameEsrb(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VIDEO_GAME_GENRE)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.VIDEOGAME_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getVideoGameGenre());
				} else {
					mTRUSKUFeedVO.setVideoGameGenre(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VIDEO_GAME_PLATFORM)) {
				if (null == mSKUType || StringUtils.isBlank(mSKUType)) {
					mSKUType = TRUProductFeedConstants.VIDEOGAME_SKU;
				}
				if(StringUtils.isBlank(mCharacters) || (mCharacters != null && mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NULL))){
					mTRUSKUFeedVO.getEmptyNullProperties().add(getFeedCatalogProperty().getVideoGamePlatform());
				} else {
					mTRUSKUFeedVO.setVideoGamePlatform(mCharacters);
				}
			}
			
			if(pElementName.equalsIgnoreCase(TRUProductFeedConstants.CLASSIFICATION_REFERENCE)){
				mClassificationReference = Boolean.FALSE;
				mClassificationId = null;
			}

		} else if (mIsProductFeedVO && !mIsSKUFeedVO) {
			if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BATTERY_INCLUDED)) {
				mTRUProductFeedVO.setBatteryIncluded(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.BATTERY_REQUIRED)) {
				mTRUProductFeedVO.setBatteryRequired(mCharacters);
			}else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.EWASTE_SURCHARGE_SKU)) {
				mTRUProductFeedVO.setEwasteSurchargeSku(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.EWASTE_SURCHARGE_STATE)) {
				mTRUProductFeedVO.setEwasteSurchargeState(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.GENDER)) {
				mTRUProductFeedVO.setGender(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.RUS_ITEM_NUMBER)) {
				String prodId = mCharacters;
				if (getTRUProductsVO().getTRUProductFeedVOMap().get(prodId) != null) {
					TRUProductFeedVO tRUProductFeedVO = getTRUProductsVO().getTRUProductFeedVOMap().get(prodId);
					BeanUtilsBean bean = new TRUBeanUtilsBean();
					String actionCode = tRUProductFeedVO.getFeedActionCode();
					try {
						bean.copyProperties(tRUProductFeedVO, mTRUProductFeedVO);
						if(StringUtils.isNotBlank(actionCode)){
                            tRUProductFeedVO.setFeedActionCode(actionCode);
						}
					} catch (IllegalAccessException ile) {
						mLogging.logError("IllegalAccessException in product feed handler", ile);
					} catch (InvocationTargetException ine) {
						mLogging.logError("InvocationTargetException in product feed handler", ine);
					}
					mTRUProductFeedVO = tRUProductFeedVO;
					mDeltaFeedProductInSideMap = Boolean.TRUE;
				}
				mTRUProductFeedVO.setRusItemNumber(prodId);
				mTRUProductFeedVO.setId(prodId);
				mTRUProductFeedVO.setRepositoryId(prodId);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SHIP_FROM_STORE_ELIGIBLE)) {
				mTRUProductFeedVO.setShipFromStoreEligible(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SPECIAL_ASSEMBLY_INSTRUCTIONS)) {
				mTRUProductFeedVO.setSpecialAssemblyInstructions(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.TAX_CODE)) {
					
				    if(mCharacters.length() > TRUProductFeedConstants.NUMBER_FIVE) {
						mCharacters = mCharacters.substring(mCharacters.length() - TRUProductFeedConstants.NUMBER_FIVE, mCharacters.length());
					}
				
				mTRUProductFeedVO.setTaxCode(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.TAX_PRODUCT_VALUE_CODE)) {
				mTRUProductFeedVO.setTaxProductValueCode(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VENDORS_PRODUCT_DEMO_URL)) {
				mTRUProductFeedVO.setVendorsProductDemoUrl(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.MANUFACTURER_STYLE_NUMBER)) {
				mTRUProductFeedVO.setManufacturerStyleNumber(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NON_MERCHANDISE_FLAG)) {
				mTRUProductFeedVO.setNonMerchandiseFlag(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.COUNTRY_ELIGIBILITY)) {
				mTRUProductFeedVO.setCountryEligibility(mCharacters);
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.SOURCE)) {
				mTRUProductFeedVO.setSource(mCharacters);
			}
			
			// NonMerchSKU changes

			else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.ITEM_ATTRIBUTE)) {
				mItemAttribute = Boolean.FALSE;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.EDQ_ITEM_TYPE) && mItemAttribute && 
					(mCharacters.equalsIgnoreCase(TRUProductFeedConstants.NON_MERCHANDISE) && (null == mISNonMerchFlag || StringUtils.isBlank(mISNonMerchFlag)))) {
				mISNonMerchFlag = TRUProductFeedConstants.NONMERCHSKU;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VALUE) && mProductCrossReference && mBatteries && mMetadata && mAttributeId != null &&
							mAttributeId.equalsIgnoreCase(TRUProductFeedConstants.BATTERY_QUANTITY_ID) && !StringUtils.isBlank(mCharacters)) {
				mValue = mCharacters;
				mAttributeId = null;
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.PRODUCT_CROSS_REFERENCE) && mMetadata && !mCrossSell && mBatteries) {
				mBatteryQuantitynTypeVO = new TRUBatteryQuantitynTypeVO();
				mBatteryQuantitynTypeVO.setBatteryQuantity(mValue);
				mBatteryQuantitynTypeVO.setBatteryType(mProductId);
				mTRUProductFeedVO.getBatteryQuantitynTypeList().add(mBatteryQuantitynTypeVO);
				mProductCrossReference = Boolean.FALSE;
				mCrossSell = Boolean.FALSE;
				mBatteries = Boolean.FALSE;
				mMetadata = Boolean.FALSE;
				mProductId = null;
				mValue = null;
				mAttributeId = null;
			}
			
		}

		// Delta Feed changes

		else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.STYLE)) {
			if (getTRUProductsVO().getTRUProductFeedVOMap().get(mTRUStyleProductFeedVO.getId()) == null) {
				mTRUStyleProductFeedVO.setType(TRUProductFeedConstants.STYLE);
				getTRUProductsVO().getTRUProductFeedVOList().add(mTRUStyleProductFeedVO);
				getTRUProductsVO().getTRUProductFeedVOMap().put(mTRUStyleProductFeedVO.getId(), mTRUStyleProductFeedVO);
				mTRUStyleProductFeedVO.setClassificationIdAndRootCategoryMap(getClassificationIdAndRootCategoryMap());
			}
			mIsStyle = Boolean.FALSE;
		} else if (mIsStyle) {
			if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.VALUE)) {
				if (StringUtils.isNotBlank(mStyleAttributeId) && mStyleAttributeId.equalsIgnoreCase(TRUProductFeedConstants.STYLE_VALUE_ID)) {
					mTRUStyleProductFeedVO.setSizeChartName(mCharacters);
				}
			} else if (pElementName.equalsIgnoreCase(TRUProductFeedConstants.NAME)) {
				mTRUStyleProductFeedVO.setOnlineTitle(mCharacters);
			}
		}
		mCharactersBuffer.setLength(0);
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("End @Class: TRUProductFeedHandler, @method: endElement()");
		}

	}

	/**
	 * Gets the feed catalog property.
	 *
	 * @return the feed catalog property
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 *
	 * @param pFeedCatalogProperty the new feed catalog property
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the sequence number.
	 * 
	 * @return the mSequenceNumber
	 */
	public String getSequenceNumber() {
		return mSequenceNumber;
	}

	/**
	 * Sets the sequence number.
	 * 
	 * @param pSequenceNumber
	 *            the new sequence number
	 */
	public void setSequenceNumber(String pSequenceNumber) {
		this.mSequenceNumber = pSequenceNumber;
	}

	/**
	 * Gets the empty sku properties flag.
	 *
	 * @return the emptySkuPropertiesFlag
	 */
	public Boolean getEmptySkuPropertiesFlag() {
		return mEmptySkuPropertiesFlag;
	}

	/**
	 * Sets the empty sku properties flag.
	 *
	 * @param pEmptySkuPropertiesFlag the emptySkuPropertiesFlag to set
	 */
	public void setEmptySkuPropertiesFlag(Boolean pEmptySkuPropertiesFlag) {
		mEmptySkuPropertiesFlag = pEmptySkuPropertiesFlag;
	}

}