package com.tru.feedprocessor.tol.catalog.tools;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;

import org.apache.commons.beanutils.BeanUtilsBean;

/**
 * The Class TRUBeanUtilsBean.
 */
public class TRUBeanUtilsBean extends BeanUtilsBean {
	@Override
    public void copyProperty(Object pDest, String pName, Object pValue)
            throws IllegalAccessException, InvocationTargetException {
		if (pValue == null) {
        	return;
        }
        if(pValue!=null &&( pValue instanceof ArrayList<?> || pValue instanceof HashSet<?> ||  pValue instanceof LinkedHashMap<?, ?>) ){
        	return;
        }
        super.copyProperty(pDest, pName, pValue);
    }
}
