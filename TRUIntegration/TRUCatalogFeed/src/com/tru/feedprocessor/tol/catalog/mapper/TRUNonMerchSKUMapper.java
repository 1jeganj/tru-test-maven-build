package com.tru.feedprocessor.tol.catalog.mapper;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUSKUVOToRepository;
import com.tru.feedprocessor.tol.catalog.vo.TRUNonMerchSKUVO;

/**
 * The Class TRUNonMerchSKUMapper. This class maps the data stored in entities to repository to push the data of properties and
 * NonMerchSKU properties it is used to write the VO properties to the ProductCatalog repository
 * @author Professional Access
 * @version 1.0
 */
public class TRUNonMerchSKUMapper extends AbstractFeedItemMapper<TRUNonMerchSKUVO> {
	
	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/**
	 * The variable to hold "mSKUVOToRepository" property name.
	 */
	private TRUSKUVOToRepository mSKUVOToRepository;
	
	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	
	/**
	 * Gets the SKUVO to repository.
	 * 
	 * @return the sKUVOToRepository
	 */
	public TRUSKUVOToRepository getSKUVOToRepository() {
		return mSKUVOToRepository;
	}

	/**
	 * Sets the SKUVO to repository.
	 * 
	 * @param pSKUVOToRepository
	 *            the sKUVOToRepository to set
	 */
	public void setSKUVOToRepository(TRUSKUVOToRepository pSKUVOToRepository) {
		mSKUVOToRepository = pSKUVOToRepository;
	}

	/**
	 * Setting Non Merch SKU properties it is used to write the VO properties to the ProductCatalog repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUNonMerchSKUVO pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Start @Class: TRUNonMerchSKUMapper, @method: map()");
		
		MutableRepositoryItem skuRepoItem = getSKUVOToRepository().settingCommonSKUProperties(pVOItem, pRepoItem);
		
		getLogger().vlogDebug("End @Class: TRUNonMerchSKUMapper, @method: map()");

		return skuRepoItem;
	}
}
