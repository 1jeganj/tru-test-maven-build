package com.tru.feedprocessor.tol.catalog.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUStyleErrorVO;

/**
 * The Class TRUDeltaRepositoryToProductVO. This class populates the product properties for the Style product
 * @author Professional Access
 * @version 1.0
 */
public class TRUDeltaRepositoryToProductVO {
	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * The variable to hold "FeedRepository" property name.
	 */
	private MutableRepository mFeedRepository;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the feed repository.
	 * 
	 * @return the feedRepository
	 */
	public MutableRepository getFeedRepository() {
		return mFeedRepository;
	}

	/**
	 * Sets the feed repository.
	 * 
	 * @param pFeedRepository
	 *            the feedRepository to set
	 */
	public void setFeedRepository(MutableRepository pFeedRepository) {
		mFeedRepository = pFeedRepository;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method populates Style product properties from SKU's Original parent product.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pProductRepoItem
	 *            the product repo item
	 * @param pErrorList
	 * 			the Error List           
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void repositoryToProductVO(TRUProductFeedVO pVOItem, MutableRepositoryItem pProductRepoItem, List<TRUStyleErrorVO> pErrorList)
			throws FeedSkippableException {
		getLogger().vlogDebug("Start @Class: TRUDeltaRepositoryToProductVO, @method: RepositoryToProductVO()");
		String method = TRUProductFeedConstants.REPOSITORY_TO_PRODUCT_VO;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}

		String firstSkuId = null;
		List<TRUStyleErrorVO> styleErrorList = pErrorList;
		TRUStyleErrorVO erroVO = new TRUStyleErrorVO();
		erroVO.setId(pVOItem.getId());
		erroVO.setVersion(pVOItem.getSequenceNumber());
		erroVO.setFileName(pVOItem.getFileName());
		Iterator<Map.Entry<String, String>> entries = pVOItem.getStyleUIDs().entrySet().iterator();
		while (entries.hasNext()) {
			String skuId = null;
			String actionCode = null;
			MutableRepositoryItem skuRepoItem = null;
			MutableRepositoryItem productRepoItem = null;
			List<RepositoryItem> originalProdChildSkus = null;
			Map.Entry<String, String> entry = entries.next();
			List<String> originalProdChildSkuIDs = new ArrayList<String>();
			skuId = entry.getKey();
			actionCode = entry.getValue();
			skuRepoItem = getSkuItem(skuId,pVOItem);
			if (skuRepoItem != null && 
					!StringUtils.isBlank((String) skuRepoItem.getPropertyValue(getFeedCatalogProperty()
							.getOrignalParentProduct()))) {
				productRepoItem = getProductItem((String) skuRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getOrignalParentProduct()),pVOItem);
				if (productRepoItem != null) {
					originalProdChildSkus = (List<RepositoryItem>) productRepoItem
							.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
					
					for (RepositoryItem prodChldSku : originalProdChildSkus) {
						originalProdChildSkuIDs.add(prodChldSku.getRepositoryId());
					}
					
					RepositoryItem removeitem = null;
					if (actionCode.equalsIgnoreCase(TRUProductFeedConstants.A)) {
						// SKU ID with Action code A SKU ID appending to Style Product ChildSKU Ids
						pVOItem.getChildSKUIds().add(skuId);
						// SKU ID with Action code A SKU ID removing from it's OriginalProduct ChildSKU Ids
						if (originalProdChildSkus != null && !originalProdChildSkus.isEmpty()) {
							for (RepositoryItem repoItem : originalProdChildSkus) {
								if (repoItem.getRepositoryId().equals(skuId)) {
									removeitem = repoItem;
									break;
								}
							}
							if (removeitem != null) {
								originalProdChildSkus.remove(removeitem);
							}
							// Updating Original Parent Product's childsku's
							productRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(),
									originalProdChildSkus);
							// Updating Original Parent Product's parentclassifications
							updateOriginalProductParentClassifications(originalProdChildSkus, productRepoItem);
						}
						// SKU ID with Action code A Setting it's OnlinePID to it's StyleProductID
						//Commenting the below part for CR-107
						//SKURepositoryItem.setPropertyValue(getFeedCatalogProperty().getOnlinePID(), pVOItem.getId());
						if (firstSkuId == null &&
								((List<String>) pProductRepoItem.getPropertyValue(getFeedCatalogProperty()
										.getChildSKUsName())).isEmpty()) {
							firstSkuId = skuId;
							// Populating Style Product properties by Original Product properties
							settingProductVOFromProductRepositoryItem(productRepoItem, pVOItem);
							settingProductVOToOriginalProductRepositoryItem(pVOItem, pProductRepoItem);
						}
						try {
							getFeedRepository().updateItem(skuRepoItem);
							getFeedRepository().updateItem(productRepoItem);
						} catch (RepositoryException re) {
							getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
							pVOItem.setErrorMessage(re.getMessage());
							throw new FeedSkippableException(null, null,
									TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
						}
						
					} else if (actionCode.equalsIgnoreCase(TRUProductFeedConstants.D)) {
						// Removing SKU ID with Action code D from Sytle Product ChildSKU Ids
						List<RepositoryItem> styleProductChildSkuIds = (List<RepositoryItem>) pProductRepoItem
								.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
						for (RepositoryItem repoItem : styleProductChildSkuIds) {
							if (repoItem.getRepositoryId().equals(skuId)) {
								removeitem = repoItem;
								break;
							}
						}
						if (removeitem != null) {
							styleProductChildSkuIds.remove(removeitem);
						}
						pProductRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(),
								styleProductChildSkuIds);
						// SKU ID with Action code D Setting it's OnlinePID to it's OriginalProductID
						//Commenting the below part for CR-107
						/*SKURepositoryItem.setPropertyValue(getFeedCatalogProperty().getOnlinePID(),
								SKURepositoryItem.getPropertyValue(getFeedCatalogProperty().getOriginalPID()));*/
						try {
							getFeedRepository().updateItem(skuRepoItem);
						} catch (RepositoryException re) {
							getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
							pVOItem.setErrorMessage(re.getMessage());
							throw new FeedSkippableException(null, null,
									TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
						}
						// SKU ID with Action code D SKU ID appending to it's OriginalProduct ChildSKU Ids
						if(!originalProdChildSkuIDs.contains(skuRepoItem.getRepositoryId())){
							originalProdChildSkus.add(skuRepoItem);
						}
						
						// Updating Original Parent Product's childsku's
						productRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(),
								originalProdChildSkus);
						// Updating Original Parent Product's parentclassifications
						updateOriginalProductParentClassifications(originalProdChildSkus, productRepoItem);
						try {
							getFeedRepository().updateItem(productRepoItem);
						} catch (RepositoryException re) {
							getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
							pVOItem.setErrorMessage(re.getMessage());
							throw new FeedSkippableException(null, null,
									TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
						}
					}
				} else {
					erroVO.getSkusNotAssociated().add(skuId);
				}

			} else {
				erroVO.getSkusNotAvailable().add(skuId);
			}

		}
		
		if(!erroVO.getSkusNotAvailable().isEmpty() || !erroVO.getSkusNotAssociated().isEmpty()){
			styleErrorList.add(erroVO);
		}
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		getLogger().vlogDebug("End @Class: TRUDeltaRepositoryToProductVO, @method: RepositoryToProductVO()");
	}

	/**
	 * This method gets the SKU repository item.
	 *
	 * @param pSkuId            the sku id
	 * @param pVOItem the VO item
	 * @return the sku item
	 * @throws FeedSkippableException the feed skippable exception
	 */
	private MutableRepositoryItem getSkuItem(String pSkuId, TRUProductFeedVO pVOItem) throws FeedSkippableException {
		MutableRepositoryItem RepositoryItem = null;
		try {
			if(getFeedRepository().getItem(pSkuId, getFeedCatalogProperty().getSKUName()) != null){
				RepositoryItem = (MutableRepositoryItem) getFeedRepository().getItem(pSkuId, getFeedCatalogProperty().getSKUName());
			}
		} catch (RepositoryException re) {
			getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
			pVOItem.setErrorMessage(re.getMessage());
			throw new FeedSkippableException(null, null,
					TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
		}
		return RepositoryItem;
	}

	/**
	 * This method gets the Product repository item.
	 *
	 * @param pProductId            the product id
	 * @param pVOItem the VO item
	 * @return the product item
	 * @throws FeedSkippableException the feed skippable exception
	 */
	private MutableRepositoryItem getProductItem(String pProductId, TRUProductFeedVO pVOItem) throws FeedSkippableException {
		MutableRepositoryItem RepositoryItem = null;
		try {
			if(getFeedRepository().getItem(pProductId, getFeedCatalogProperty().getProductName()) != null){
				RepositoryItem = (MutableRepositoryItem) getFeedRepository().getItem(pProductId, getFeedCatalogProperty().getProductName());
			}
		} catch (RepositoryException re) {
			getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
			pVOItem.setErrorMessage(re.getMessage());
			throw new FeedSkippableException(null, null,
					TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
		}	
		return RepositoryItem;
	}

	/**
	 * This method updates OriginalProduct ParentClassifications.
	 * 
	 * @param pOriginalProductChildSkuIds
	 *            the original product child sku ids
	 * @param pProductRepositoryItem
	 *            the product repository item
	 */
	private void updateOriginalProductParentClassifications(List<RepositoryItem> pOriginalProductChildSkuIds,
			MutableRepositoryItem pProductRepositoryItem) {
		List<String> parentClassifications = new ArrayList<String>();
		for (RepositoryItem skuitem : pOriginalProductChildSkuIds) {
			List<String> RepositorySKUParentClassifications = (List<String>) skuitem
					.getPropertyValue(getFeedCatalogProperty().getParentClassifications());
			if (null != RepositorySKUParentClassifications && !RepositorySKUParentClassifications.isEmpty()) {
				parentClassifications.addAll(RepositorySKUParentClassifications);
			}
		}
		pProductRepositoryItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(), parentClassifications);

	}
		
	/**
	 * Check for sku delete.
	 *
	 * @param pVOItem the VO item
	 * @param pRepoItem the repo item
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public void checkForSKUDelete(TRUProductFeedVO pVOItem, MutableRepositoryItem pRepoItem) throws FeedSkippableException{
		List<TRUSkuFeedVO> skuFeedVOList = pVOItem.getChildSKUsList();
		if(!skuFeedVOList.isEmpty()){
			for(TRUSkuFeedVO skuFeedVO: skuFeedVOList){
				if(!StringUtils.isBlank(skuFeedVO.getFeedActionCode()) && skuFeedVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.D)){
					MutableRepositoryItem SKURepositoryItem = getSkuItem(skuFeedVO.getId(),pVOItem);
					Set<RepositoryItem> parentProducts = (Set<RepositoryItem>) SKURepositoryItem.getPropertyValue(getFeedCatalogProperty().getParentProducts());
					for(RepositoryItem parentRepositoryItem : parentProducts){
						if(parentRepositoryItem.getRepositoryId().equalsIgnoreCase(pRepoItem.getRepositoryId())){
							List<RepositoryItem> productChildSkuIds = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
							RepositoryItem removeitem = null;
							for (RepositoryItem repoItem : productChildSkuIds) { 
								if (repoItem.getRepositoryId().equals(skuFeedVO.getId())) {
									removeitem = repoItem;
									break;
								}
							}
							if (removeitem != null) {
								productChildSkuIds.remove(removeitem);
							}
							pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(), productChildSkuIds);
							updateOriginalProductParentClassifications(productChildSkuIds, pRepoItem);
						}else{
							MutableRepositoryItem updatedParentRepositoryItem = (MutableRepositoryItem) parentRepositoryItem;
							List<RepositoryItem> productChildSkuIds = (List<RepositoryItem>) updatedParentRepositoryItem.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
							RepositoryItem removeitem = null;
							for (RepositoryItem repoItem : productChildSkuIds) {
								if (repoItem.getRepositoryId().equals(skuFeedVO.getId())) {
									removeitem = repoItem;
									break;
								}
							}
							if (removeitem != null) {
								productChildSkuIds.remove(removeitem);
							}
							updatedParentRepositoryItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(), productChildSkuIds);
							updateOriginalProductParentClassifications(productChildSkuIds, updatedParentRepositoryItem);
							try {
								getFeedRepository().updateItem(updatedParentRepositoryItem);
							} catch (RepositoryException re) {
								getLogger().vlogError(re, TRUProductFeedConstants.REPOSITORY_EXCEPTION);
								pVOItem.setErrorMessage(re.getMessage());
								throw new FeedSkippableException(null, null,
										TRUProductFeedConstants.REPOSITORY_EXCEPTION, pVOItem, re);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Update merchandise flags.
	 *
	 * @param pProductVO the product vo
	 * @param pRepoItem the repo item
	 */
	public void updateMerchandiseFlags(TRUProductFeedVO pProductVO, RepositoryItem pRepoItem){
		
		List<RepositoryItem> RepositoryProductChildSKUs = (List<RepositoryItem>) pRepoItem
				.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
		String voDivsionCode = pProductVO.getDivisionCode();
		String voDepartmentCode = pProductVO.getDepartmentCode();
		String voClassCode = pProductVO.getClassCode();
		String voSubClassCode = pProductVO.getSubclassCode();
		if (null != RepositoryProductChildSKUs && !RepositoryProductChildSKUs.isEmpty()) {
			for (RepositoryItem ProductChildSKUsItem : RepositoryProductChildSKUs) {
				MutableRepositoryItem repositoryItem = (MutableRepositoryItem) ProductChildSKUsItem;
				if(!StringUtils.isBlank(voDivsionCode)){
					repositoryItem.setPropertyValue(getFeedCatalogProperty().getDivisionCode(), voDivsionCode);
				}
				if(!StringUtils.isBlank(voDepartmentCode)){
					repositoryItem.setPropertyValue(getFeedCatalogProperty().getDepartmentCode(), voDepartmentCode);
				}
				if(!StringUtils.isBlank(voClassCode)){
					repositoryItem.setPropertyValue(getFeedCatalogProperty().getClassCode(), voClassCode);
				}
				if(!StringUtils.isBlank(voSubClassCode)){
					repositoryItem.setPropertyValue(getFeedCatalogProperty().getSubclassCode(), voSubClassCode);
				}
			}
		}
		
	}

	/**
	 * This method sets to productvo from product repository item.
	 * 
	 * @param pRepositoryItem
	 *            the repository item
	 * @param pProdFeedVO
	 *            the prod feed vo
	 */
	private void settingProductVOFromProductRepositoryItem(MutableRepositoryItem pRepositoryItem,
			TRUProductFeedVO pProdFeedVO) {

		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getProductDisplayName()) != null) {
			pProdFeedVO.setOnlineTitle((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getProductDisplayName()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getLongDescriptionName()) != null) {
			pProdFeedVO.setOnlineLongDesc((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getLongDescriptionName()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBatteryIncluded()) != null) {
			pProdFeedVO.setBatteryIncluded((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getBatteryIncluded()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBatteryRequired()) != null) {
			pProdFeedVO.setBatteryRequired((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getBatteryRequired()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCost()) != null) {
			pProdFeedVO.setCost((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCost()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getEwasteSurchargeSku()) != null) {
			pProdFeedVO.setEwasteSurchargeSku((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getEwasteSurchargeSku()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getEwasteSurchargeState()) != null) {
			pProdFeedVO.setEwasteSurchargeState((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getEwasteSurchargeState()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getGender()) != null) {
			pProdFeedVO.setGender((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getGender()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRusItemNumber()) != null) {
			pProdFeedVO.setRusItemNumber(((Long) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getRusItemNumber())).toString());
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipFromStoreEligible()) != null) {
			pProdFeedVO.setShipFromStoreEligible((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getShipFromStoreEligible()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSpecialAssemblyInstructions()) != null) {
			pProdFeedVO.setSpecialAssemblyInstructions((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getSpecialAssemblyInstructions()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getTaxCode()) != null) {
			pProdFeedVO.setTaxCode((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getTaxCode()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getTaxProductValueCode()) != null) {
			pProdFeedVO.setTaxProductValueCode((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getTaxProductValueCode()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl()) != null) {
			pProdFeedVO.setVendorsProductDemoUrl((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getVendorsProductDemoUrl()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getManufacturerStyleNumber()) != null) {
			pProdFeedVO.setManufacturerStyleNumber((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getManufacturerStyleNumber()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getNonMerchandiseFlag()) != null) {
			pProdFeedVO.setNonMerchandiseFlag((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getNonMerchandiseFlag()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCountryEligibility()) != null) {
			pProdFeedVO.setCountryEligibility((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getCountryEligibility()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVendorClassification()) != null) {
			pProdFeedVO.setVendorClassification((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getVendorClassification()));
		}
		if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRegistryClassification()) != null) {
			pProdFeedVO.setRegistryClassification((String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
					.getRegistryClassification()));
		}

	}

	/**
	 * This method sets the product vo to original product repository item.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 */
	private void settingProductVOToOriginalProductRepositoryItem(TRUProductFeedVO pVOItem, MutableRepositoryItem pRepoItem) {
		if (!StringUtils.isBlank(pVOItem.getId())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getId());
		}

		if (!StringUtils.isBlank(pVOItem.getOnlineTitle())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductDisplayName(), pVOItem.getOnlineTitle());
		}

		if (!StringUtils.isBlank(pVOItem.getOnlineLongDesc())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), pVOItem.getOnlineLongDesc());
		}

		if (!StringUtils.isBlank(pVOItem.getBatteryIncluded())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryIncluded(), pVOItem.getBatteryIncluded());
		}

		if (!StringUtils.isBlank(pVOItem.getBatteryRequired())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryRequired(), pVOItem.getBatteryRequired());
		}

		if (!StringUtils.isBlank(pVOItem.getCost())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCost(), pVOItem.getCost());
		}

		if (!StringUtils.isBlank(pVOItem.getEwasteSurchargeSku())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeSku(), pVOItem.getEwasteSurchargeSku());
		}

		if (!StringUtils.isBlank(pVOItem.getEwasteSurchargeState())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeState(), pVOItem.getEwasteSurchargeState());
		}

		if (!StringUtils.isBlank(pVOItem.getGender())) {
			if (pVOItem.getGender().contains(TRUProductFeedConstants.BOY)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.BOY);
			} else if (pVOItem.getGender().contains(TRUProductFeedConstants.GIRL)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.GIRL);
			} else if (pVOItem.getGender().contains(TRUProductFeedConstants.BOTH)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.BOTH);
			}
		}

		if (!StringUtils.isBlank(pVOItem.getRusItemNumber())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRusItemNumber(),
					Long.parseLong(pVOItem.getRusItemNumber()));
		}

		if (!StringUtils.isBlank(pVOItem.getShipFromStoreEligible())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipFromStoreEligible(),
					pVOItem.getShipFromStoreEligible());
		}

		if (!StringUtils.isBlank(pVOItem.getSpecialAssemblyInstructions())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSpecialAssemblyInstructions(),
					pVOItem.getSpecialAssemblyInstructions());
		}

		if (!StringUtils.isBlank(pVOItem.getTaxCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxCode(), pVOItem.getTaxCode());
		}

		if (!StringUtils.isBlank(pVOItem.getTaxProductValueCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxProductValueCode(), pVOItem.getTaxProductValueCode());
		}

		if (!StringUtils.isBlank(pVOItem.getVendorsProductDemoUrl())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl(),
					pVOItem.getVendorsProductDemoUrl());
		}

		if (!StringUtils.isBlank(pVOItem.getManufacturerStyleNumber())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getManufacturerStyleNumber(),
					pVOItem.getManufacturerStyleNumber());
		}

		if (!StringUtils.isBlank(pVOItem.getNonMerchandiseFlag())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getNonMerchandiseFlag(), pVOItem.getNonMerchandiseFlag());
		}

		if (!StringUtils.isBlank(pVOItem.getCountryEligibility())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCountryEligibility(), pVOItem.getCountryEligibility());
		}

		if (!StringUtils.isBlank(pVOItem.getVendorClassification())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getVendorClassification(), pVOItem.getVendorClassification());
		}

		// Properties included from Product relationship mapper
		if (!StringUtils.isBlank(pVOItem.getRegistryClassification())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryClassification(),
					pVOItem.getRegistryClassification());
		}

		// Delta Feed changes

		if (!StringUtils.isBlank(pVOItem.getSizeChartName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSizeChartName(), pVOItem.getSizeChartName());
		}

		if (!pVOItem.getStyleDimensions().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStyleDimensions(), pVOItem.getStyleDimensions());
		}
	}

}
