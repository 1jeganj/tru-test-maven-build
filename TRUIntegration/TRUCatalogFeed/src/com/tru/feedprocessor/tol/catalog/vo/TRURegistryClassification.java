/*
 * 
 */
package com.tru.feedprocessor.tol.catalog.vo;

import java.util.List;

/**
 * The Class TRURegistryClassification.
 * It will hold all the properties required for the Classification of primary category
 * @author Professional Access
 * @version 1.0
 */
public class TRURegistryClassification {

	/** Property to hold  id. */
	private String mID;

	/** Property to hold  user type id. */
	private String mUserTypeID;

	/** Property to hold  name. */
	private String mName;

	/** Property to hold  tru registry sub classfication. */
	private List<TRURegistrySubClassfication> mTRURegistrySubClassfication;

	/** Property to hold  image path. */
	private String mImagePath;

	/** Property to hold  display order. */
	private String mDisplayOrder;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getID() {
		return mID;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pID
	 *            the new id
	 */
	public void setID(String pID) {
		this.mID = pID;
	}

	/**
	 * Gets the user type id.
	 * 
	 * @return the user type id
	 */
	public String getUserTypeID() {
		return mUserTypeID;
	}

	/**
	 * Sets the user type id.
	 * 
	 * @param pUserTypeID
	 *            the new user type id
	 */
	public void setUserTypeID(String pUserTypeID) {
		this.mUserTypeID = pUserTypeID;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 * 
	 * @param pName
	 *            the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the TRU registry sub classfication.
	 * 
	 * @return the TRU registry sub classfication
	 */
	public List<TRURegistrySubClassfication> getTRURegistrySubClassfication() {
		return mTRURegistrySubClassfication;
	}

	/**
	 * Sets the TRU registry sub classfication.
	 * 
	 * @param pTRURegistrySubClassfication
	 *            the new TRU registry sub classfication
	 */
	public void setTRURegistrySubClassfication(List<TRURegistrySubClassfication> pTRURegistrySubClassfication) {
		this.mTRURegistrySubClassfication = pTRURegistrySubClassfication;
	}

	/**
	 * Gets the image path.
	 * 
	 * @return the image path
	 */
	public String getImagePath() {
		return mImagePath;
	}

	/**
	 * Sets the image path.
	 * 
	 * @param pImagePath
	 *            the new image path
	 */
	public void setImagePath(String pImagePath) {
		this.mImagePath = pImagePath;
	}

	/**
	 * Gets the display order.
	 * 
	 * @return the display order
	 */
	public String getDisplayOrder() {
		return mDisplayOrder;
	}

	/**
	 * Sets the display order.
	 * 
	 * @param pDisplayOrder
	 *            the new display order
	 */
	public void setDisplayOrder(String pDisplayOrder) {
		this.mDisplayOrder = pDisplayOrder;
	}
}
