package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUCategory will have all the properties related to the category item.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUCategory extends BaseFeedProcessVO {

	/** The property to hold id. */
	private String mID;

	/** The property to hold user type id. */
	private String mUserTypeID;

	/** The property to hold name. */
	private String mName;
	/** The property to hold registry. */
	private boolean mRegistry;
	/** The property to hold fixedChildCategories. */
	private List<String> mFixedChildCategories;
	/** The property to hold fixedChildCategories. */
	private List<String> mTRUFixedChildCollectionProducts;
	
	/** The property to hold fixedChildCategories. */
	private List<String> mBRUFixedChildCollectionProducts;

	/** The property to hold fixedChildCategories. */
	private List<String> mFAOFixedChildCollectionProducts;

	/** The TRU fixed child products. */
	private Set<String> mTRUFixedChildProducts;

	/** The BRU fixed child products. */
	private Set<String> mBRUFixedChildProducts;

	/** The FAO fixed child products. */
	private Set<String> mFAOFixedChildProducts;

	/**
	 * Instantiates a new TRU category.
	 */
	public TRUCategory() {
		setEntityName(TRUFeedConstants.CATEGORY_NAME);
	}

	/**
	 * Gets the BRU fixed child collection products.
	 *
	 * @return the BRU fixed child collection products
	 */
	public List<String> getBRUFixedChildCollectionProducts() {
		if (mBRUFixedChildCollectionProducts == null) {
			mBRUFixedChildCollectionProducts = new ArrayList<String>();
		}
		return mBRUFixedChildCollectionProducts;
	}
	/**
	 * Gets the BRU fixed child products.
	 * 
	 * @return the bRUFixedChildProducts
	 */
	public Set<String> getBRUFixedChildProducts() {
		if (mBRUFixedChildProducts == null) {
			mBRUFixedChildProducts = new java.util.HashSet<String>();
		}
		return mBRUFixedChildProducts;
	}

	/**
	 * Gets the FAO fixed child collection products.
	 *
	 * @return the FAO fixed child collection products
	 */
	public List<String> getFAOFixedChildCollectionProducts() {
		if (mFAOFixedChildCollectionProducts == null) {
			mFAOFixedChildCollectionProducts = new ArrayList<String>();
		}
		return mFAOFixedChildCollectionProducts;
	}

	/**
	 * Gets the FAO fixed child products.
	 * 
	 * @return the fAOFixedChildProducts
	 */
	public Set<String> getFAOFixedChildProducts() {
		if (mFAOFixedChildProducts == null) {
			mFAOFixedChildProducts = new java.util.HashSet<String>();
		}
		return mFAOFixedChildProducts;
	}

	/**
	 * Gets the fixedChildCategories.
	 * 
	 * @return fixedChildCategories
	 */
	public List<String> getFixedChildCategories() {
		if (mFixedChildCategories == null) {
			mFixedChildCategories = new ArrayList<String>();
		}
		return mFixedChildCategories;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getID() {
		return mID;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Gets the TRU fixed child collection products.
	 *
	 * @return the TRU fixed child collection products
	 */
	public List<String> getTRUFixedChildCollectionProducts() {
		if (mTRUFixedChildCollectionProducts == null) {
			mTRUFixedChildCollectionProducts = new ArrayList<String>();
		}
		return mTRUFixedChildCollectionProducts;
	}

	/**
	 * Gets the TRU fixed child products.
	 * 
	 * @return the tRUFixedChildProducts
	 */
	public Set<String> getTRUFixedChildProducts() {
		if (mTRUFixedChildProducts == null) {
			mTRUFixedChildProducts = new java.util.HashSet<String>();
		}
		return mTRUFixedChildProducts;
	}

	/**
	 * Gets the user type id.
	 * 
	 * @return the user type id
	 */
	public String getUserTypeID() {
		return mUserTypeID;
	}

	/**
	 * Gets the Registry.
	 * 
	 * @return true, if is registry
	 */
	public boolean isRegistry() {
		return mRegistry;
	}

	/**
	 * Sets the BRU fixed child collection products.
	 *
	 * @param pBRUFixedChildCollectionProducts the new BRU fixed child collection products
	 */
	public void setBRUFixedChildCollectionProducts(List<String> pBRUFixedChildCollectionProducts) {
		mBRUFixedChildCollectionProducts = pBRUFixedChildCollectionProducts;
	}

	/**
	 * Sets the BRU fixed child products.
	 * 
	 * @param pBRUFixedChildProducts
	 *            the bRUFixedChildProducts to set
	 */
	public void setBRUFixedChildProducts(Set<String> pBRUFixedChildProducts) {
		mBRUFixedChildProducts = pBRUFixedChildProducts;
	}

	/**
	 * Sets the FAO fixed child collection products.
	 *
	 * @param pFAOFixedChildCollectionProducts the new FAO fixed child collection products
	 */
	public void setFAOFixedChildCollectionProducts(List<String> pFAOFixedChildCollectionProducts) {
		mFAOFixedChildCollectionProducts = pFAOFixedChildCollectionProducts;
	}

	/**
	 * Sets the FAO fixed child products.
	 * 
	 * @param pFAOFixedChildProducts
	 *            the fAOFixedChildProducts to set
	 */
	public void setFAOFixedChildProducts(Set<String> pFAOFixedChildProducts) {
		mFAOFixedChildProducts = pFAOFixedChildProducts;
	}

	/**
	 * sets the fixedChildCategories.
	 * 
	 * @param pFixedChildCategories
	 *            the new fixed child categories
	 */
	public void setFixedChildCategories(List<String> pFixedChildCategories) {
		this.mFixedChildCategories = pFixedChildCategories;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pID
	 *            the new id
	 */
	public void setID(String pID) {
		this.mID = pID;
		setRepositoryId(pID);
	}

	/**
	 * Sets the name.
	 * 
	 * @param pName
	 *            the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * sets the Registry.
	 * 
	 * @param pRegistry
	 *            the new registry
	 */
	public void setRegistry(boolean pRegistry) {
		this.mRegistry = pRegistry;
	}

	/**
	 * Sets the TRU fixed child collection products.
	 *
	 * @param pTRUFixedChildCollectionProducts the new TRU fixed child collection products
	 */
	public void setTRUFixedChildCollectionProducts(List<String> pTRUFixedChildCollectionProducts) {
		mTRUFixedChildCollectionProducts = pTRUFixedChildCollectionProducts;
	}

	/**
	 * Sets the TRU fixed child products.
	 * 
	 * @param pTRUFixedChildProducts
	 *            the tRUFixedChildProducts to set
	 */
	public void setTRUFixedChildProducts(Set<String> pTRUFixedChildProducts) {
		mTRUFixedChildProducts = pTRUFixedChildProducts;
	}

	/**
	 * Sets the user type id.
	 * 
	 * @param pUserTypeID
	 *            the new user type id
	 */
	public void setUserTypeID(String pUserTypeID) {
		this.mUserTypeID = pUserTypeID;
	}

}
