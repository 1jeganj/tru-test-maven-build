package com.tru.feedprocessor.tol.catalog.processor;

import java.util.Map;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValueChangedInspector;
import com.tru.feedprocessor.tol.catalog.vo.TRUBookCdDvDSKUVO;

/**
 * The Class TRUBookCdDvDSKUItemValueChangedInspector. This class inspects each and every property holds in entites to
 * repository of SKU properties and BookCdDvDSKU properties has any changes. If any change occurs in one property entire
 * entity will be updated.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBookCdDvDSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold CommonSKUItemValueChangedInspector holder. */
	private TRUCommonSKUItemValueChangedInspector mCommonSKUItemValueChangedInspector;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the common sku item value changed inspector.
	 * 
	 * @return the commonSKUItemValueChangedInspector
	 */
	public TRUCommonSKUItemValueChangedInspector getCommonSKUItemValueChangedInspector() {
		return mCommonSKUItemValueChangedInspector;
	}

	/**
	 * Sets the common sku item value changed inspector.
	 * 
	 * @param pCommonSKUItemValueChangedInspector
	 *            the commonSKUItemValueChangedInspector to set
	 */
	public void setCommonSKUItemValueChangedInspector(
			TRUCommonSKUItemValueChangedInspector pCommonSKUItemValueChangedInspector) {
		mCommonSKUItemValueChangedInspector = pCommonSKUItemValueChangedInspector;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
	
	/**
	 * This method is used to inspect the SKU properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUBookCdDvDSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUBookCdDvDSKUVO) {
			TRUBookCdDvDSKUVO skuVO = (TRUBookCdDvDSKUVO) pBaseFeedProcessVO;
			Map<String, String> bookCdDvdPropertiesMap = (Map<String, String>) pRepositoryItem.
					getPropertyValue(getFeedCatalogProperty().getBookCdDvdPropertiesMap());
			if (getCommonSKUItemValueChangedInspector().isUpdated(skuVO, pRepositoryItem)) {
				return retVal = Boolean.TRUE;
			}
			if (!skuVO.getBookCdDvdPropertiesMap().isEmpty() && 
					!getCommonSKUItemValueChangedInspector().mapsAreEqual(skuVO.getBookCdDvdPropertiesMap(),
							bookCdDvdPropertiesMap)) {
				return retVal = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUBookCdDvDSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	
}
