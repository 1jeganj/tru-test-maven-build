package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;

/**
 * The Class CategoryItemValueChangedInspector. This class inspects each and every property holds in entites to 
 * repository of Classification has any changes.
 * @version 1.0
 * @author Professional Access
 */
public class TRUClassificationItemValueChangedInspector implements IItemValueChangeInspector {

	/** The Feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * The method is overridden to check whether properties are modified or not of the current vo to the repository item .
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUClassificationItemValueChangedInspector, @method: isUpdated()");
		
		boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUClassification) {
			TRUClassification classificationVO = (TRUClassification) pBaseFeedProcessVO;
			if(checkPropertyForUpdate(classificationVO.getUserTypeID(),
					(String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getUserTypeIdPropertyName()))){
				return retVal = Boolean.TRUE;
			}
			if(checkPropertyForUpdate(classificationVO.getName(),
					(String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))){
				return retVal = Boolean.TRUE;
			}
			if(checkPropertyForUpdate(classificationVO.getLongDescription(),
					(String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getLongDescriptionPropertyName()))){
				return retVal = Boolean.TRUE;
			}
			if(checkPropertyForUpdate(classificationVO.getDisplayStatus(),
					(String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getDisplayStatusPropertyName()))){
				return retVal = Boolean.TRUE;
			}
			if(checkDisplayOrderProperty(classificationVO.getDisplayOrder(),
					(Integer) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getDisplayOrderPropertyName()))){
				return retVal = Boolean.TRUE;
			}
			if(!classificationVO.isDeleted().equals(
					(Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName()))){
				return retVal = Boolean.TRUE;
			}
			
			if (!classificationVO.getCrossRefDisplayNameMap().equals(
					pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getCrossRefDisplayNamePropertyName()))) {
				return retVal = Boolean.TRUE;
			}
			
			if (!classificationVO.getCrossRefDisplayOrderMap().equals(
					pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getCrossRefDisplayOrderPropertyName()))) {
				return retVal = Boolean.TRUE;
			}
			
			if(checkPropertyForUpdate(classificationVO.getPrimaryParentCategory(),
					(String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPrimaryParentCategory()))){
				return retVal = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUClassificationItemValueChangedInspector, @method: isUpdated()" + retVal);

		return retVal;
	}
	/**
	 * Check property for update. It will check the whether the property in the VO is equals to the property of the
	 * repository.
	 * 
	 * @param pVoProperty
	 *            the vo property
	 * @param pItemProperty
	 *            the item property
	 * @return true, if successful
	 */
	private boolean checkPropertyForUpdate(String pVoProperty, String pItemProperty) {
		getLogger().vlogDebug("Start @Class: TRUClassificationItemValueChangedInspector, @method: checkPropertyForUpdate()");
		if (!StringUtils.isBlank(pVoProperty) && !pVoProperty.equals(pItemProperty)) {
			return true;
		}
		getLogger().vlogDebug("End @Class: TRUClassificationItemValueChangedInspector, @method: checkPropertyForUpdate()");
		return false;
	}

	/**
	 * Check display order property. It will check the whether the property in the VO is equals to the property of the
	 * repository.
	 * 
	 * @param pVoProperty
	 *            the vo property
	 * @param pItemProperty
	 *            the item property
	 * @return true, if successful
	 */
	private boolean checkDisplayOrderProperty(String pVoProperty, Integer pItemProperty) {
		getLogger().vlogDebug(
				"Start @Class: TRUClassificationItemValueChangedInspector, @method: checkDisplayOrderProperty()");
		if (pVoProperty == null && pItemProperty == null) {
			return false;
		} else if ((pVoProperty != null && pItemProperty != null && Integer.parseInt(pVoProperty) != pItemProperty)
				|| (pVoProperty != null && pItemProperty == null)) {
			return true;
		}
		getLogger()
				.vlogDebug("End @Class: TRUClassificationItemValueChangedInspector, @method: checkDisplayOrderProperty()");
		return false;
	}
}
