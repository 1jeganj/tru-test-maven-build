package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUNonMerchSKUVO. This class extends TRUSkuFeedVO to set the entity name to NONMERCHSKU.
 * @author Professional Access
 * @version 1.0
 */
public class TRUNonMerchSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU NonMerch skuvo.
	 */
	public TRUNonMerchSKUVO() {
		setEntityName(TRUProductFeedConstants.NONMERCHSKU);
	}

}
