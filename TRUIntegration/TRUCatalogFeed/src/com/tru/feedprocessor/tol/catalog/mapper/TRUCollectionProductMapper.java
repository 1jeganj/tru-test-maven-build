package com.tru.feedprocessor.tol.catalog.mapper;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class TRUCollectionProductMapper. This class maps the data stored in entities to repository to push the data
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCollectionProductMapper extends AbstractFeedItemMapper<TRUCollectionProductVO> {

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;
	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	

	/**
	 * Maps the Values
	 * 
	 * This Method map() will set the repository properties with the values from the feed
	 * 
	 * This Method sets the property value of the collection product item in the catalog.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUCollectionProductVO pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Start @Class: TRUCollectionProductMapper, @method: map()");
		
		String method = TRUProductFeedConstants.COLLECTION_PRD_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.CPMCOUNT);
		count++;
		getLogger().vlogDebug("collprodMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.CPMCOUNT,count);
		/*if (!StringUtils.isBlank(pVOItem.getId())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getId());
		}*/

		if (!StringUtils.isBlank(pVOItem.getOnlineTitle())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductDisplayName(), pVOItem.getOnlineTitle());
		} else if (pVOItem.getChildSKUsList() != null
				&& !StringUtils.isBlank(pVOItem.getChildSKUsList().get(0).getOnlineTitle())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getChildSKUsList().get(0)
					.getOnlineTitle());
		}

		if (!StringUtils.isBlank(pVOItem.getOnlineLongDesc())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), pVOItem.getOnlineLongDesc());
		}
		
		if (!StringUtils.isBlank(pVOItem.getCollectionImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCollectionImage(), pVOItem.getCollectionImage());
		}
		
		if (!StringUtils.isBlank(pVOItem.getDisplayStatus())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayStatusPropertyName(), pVOItem.getDisplayStatus());
		}
		getLogger().vlogDebug("End @Class: TRUCollectionProductMapper, @method: map()");
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		return pRepoItem;
	}

}
