package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassificationCrossReference;

/**
 * The Class CategoryRelationshipMapper. This class maps the Classification relationships data stored in entities.
 * @version 1.0
 * @author Professional Access
 * 
 */
public class TRUClassificationRelationshipMapper extends AbstractFeedItemMapper<TRUClassification> {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	

	/**
	 * This Method map() will maps the relation among the classifications and sets the parentCategory,RootParentCategory
	 * properties with the respective values.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MutableRepositoryItem map(TRUClassification pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUClassificationRelationshipMapper, @method: map()");
		String method = TRUProductFeedConstants.CLASSIFICATION_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.CRMCOUNT);
		count++;
		getLogger().vlogDebug("classRelMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.CRMCOUNT,count);
		
		EntityDataProvider entityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = (List<BaseFeedProcessVO>) entityDataProvider.
				getEntityList(TRUFeedConstants.CLASSIFICATION_ENTITY);
		
		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);
		
		Set<String> taxonomyChildClassificationSet = null;
		RepositoryItem[] lCategoryRepositoryItems = null;
		if (!StringUtils.isBlank(pVOItem.getID())) {
			taxonomyChildClassificationSet = getTaxonomyChildClassificationArray(pVOItem.getID(), lBaseFeedProcessVOList,
					pVOItem);
		}
		for(String collectionId: pVOItem.getCollectionProductKeySet()){
			if(taxonomyChildClassificationSet.contains(collectionId)){
				taxonomyChildClassificationSet.remove(collectionId);
			}
		}
		if (taxonomyChildClassificationSet != null) {
			lCategoryRepositoryItems = productCatalogRepository.getItems(taxonomyChildClassificationSet.
					toArray(new String[taxonomyChildClassificationSet.size()]),
					getFeedCatalogProperty().getClassificationPropertyName());
		}
		if (lCategoryRepositoryItems != null && lCategoryRepositoryItems.length > 0) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName(),
					Arrays.asList(lCategoryRepositoryItems));
		}

		getLogger().vlogDebug("End @Class: TRUClassificationRelationshipMapper, @method: map()");
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		return pRepoItem;
	}

	/**
	 * Gets the taxonomy child classification array.
	 * 
	 * @param pCategoryId
	 *            the category id
	 * @param pBaseFeedProcessVOList
	 *            the base feed process vo list
	 * @param pVOItem
	 *            the VO item
	 * @return the taxonomy child classification array
	 */
	private Set<String> getTaxonomyChildClassificationArray(String pCategoryId, List<BaseFeedProcessVO> pBaseFeedProcessVOList,
			TRUClassification pVOItem) {
		getLogger().vlogDebug("Begin @Class: TRUClassificationRelationshipMapper, @method: getTaxonomyChildClassificationArray()");
		Set<String> childCategory = new HashSet<String>();
		for (BaseFeedProcessVO lBaseFeedProcessVO : pBaseFeedProcessVOList) {
			TRUClassification classification = (TRUClassification) lBaseFeedProcessVO;
			if (classification.getID().equals(pCategoryId)) {
				childCategory.addAll(classification.getChildCategories());
			}
		}
		List<TRUClassificationCrossReference> crossVo = pVOItem.getClassificationCrossReference();
		if (crossVo != null) {
			for (TRUClassificationCrossReference crossObj : crossVo) {
				if (!childCategory.contains(crossObj.getId())) {
					childCategory.add(crossObj.getId());
				}
			}
		}
		getLogger().vlogDebug("End @Class: TRUClassificationRelationshipMapper, @method: getTaxonomyChildClassificationArray()");
		return childCategory;

	}

}
