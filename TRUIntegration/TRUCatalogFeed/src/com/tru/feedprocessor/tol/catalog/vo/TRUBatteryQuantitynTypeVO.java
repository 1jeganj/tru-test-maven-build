package com.tru.feedprocessor.tol.catalog.vo;

/**
 * The Class TRUBatteryQuantitynTypeVO. This class holds batteries properties.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBatteryQuantitynTypeVO {

	/** property to hold battery type holder. */
	private String mBatteryType;

	/** property to hold battery quantity holder. */
	private String mBatteryQuantity;

	/**
	 * Gets the battery type.
	 * 
	 * @return the batteryType
	 */
	public String getBatteryType() {
		return mBatteryType;
	}

	/**
	 * Sets the battery type.
	 * 
	 * @param pBatteryType
	 *            the batteryType to set
	 */
	public void setBatteryType(String pBatteryType) {
		mBatteryType = pBatteryType;
	}

	/**
	 * Gets the battery quantity.
	 * 
	 * @return the batteryQuantity
	 */
	public String getBatteryQuantity() {
		return mBatteryQuantity;
	}

	/**
	 * Sets the battery quantity.
	 * 
	 * @param pBatteryQuantity
	 *            the batteryQuantity to set
	 */
	public void setBatteryQuantity(String pBatteryQuantity) {
		mBatteryQuantity = pBatteryQuantity;
	}

}
