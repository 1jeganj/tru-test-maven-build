package com.tru.feedprocessor.tol.catalog.mapper;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUSKUVOToRepository;
import com.tru.feedprocessor.tol.catalog.vo.TRUVideoGameSKUVO;

/**
 * The Class TRUVideoGameSKUMapper. This class maps the data stored in entities to repository to push the data of properties
 * and VideoGameSKU properties it is used to write the VO properties to the ProductCatalog repository
 * @author Professional Access
 * @version 1.0
 */
public class TRUVideoGameSKUMapper extends AbstractFeedItemMapper<TRUVideoGameSKUVO> {

	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * The variable to hold "mSKUVOToRepository" property name.
	 */
	private TRUSKUVOToRepository mSKUVOToRepository;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * Gets the SKUVO to repository.
	 * 
	 * @return the sKUVOToRepository
	 */
	public TRUSKUVOToRepository getSKUVOToRepository() {
		return mSKUVOToRepository;
	}

	/**
	 * Sets the SKUVO to repository.
	 * 
	 * @param pSKUVOToRepository
	 *            the sKUVOToRepository to set
	 */
	public void setSKUVOToRepository(TRUSKUVOToRepository pSKUVOToRepository) {
		mSKUVOToRepository = pSKUVOToRepository;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Setting common sku properties and VideoGameSKU properties it is used to write the VO properties to the ProductCatalog
	 * repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUVideoGameSKUVO pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUVideoGameSKUMapper, @method: map()");
		
		String method = TRUProductFeedConstants.VDGAME_SKU_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		MutableRepositoryItem videoGameRepoItem = getSKUVOToRepository().settingCommonSKUProperties(pVOItem, pRepoItem);

		getLogger().vlogDebug("End @Class: TRUVideoGameSKUMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}

		return videoGameRepoItem;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

}