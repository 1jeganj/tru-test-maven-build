package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class CollectionProductItemValidator. This class validates the mandatory properties of CollectionProducts.
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUCollectionProductItemValidator implements IFeedItemValidator {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Validate. It validates whether the required properties of the collection product are there or not. It also checks whether
	 * the properties are in correct format w.r.t repository If the required property is not there it will throw the
	 * exception.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCollectionProductItemValidator, @method: validate()");

		TRUCollectionProductVO collectionProd = (TRUCollectionProductVO) pBaseFeedProcessVO;
		if (StringUtils.isBlank(collectionProd.getId())) {
			collectionProd.setErrorMessage(TRUFeedConstants.ERROR_CLASSIFICATION_ID);
			throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_TAXONOMY, collectionProd, null);
		}
		if (StringUtils.isBlank(collectionProd.getOnlineTitle())) {
			collectionProd.setErrorMessage(TRUFeedConstants.ERROR_CLASSIFICATION_NAME);
			throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_TAXONOMY, collectionProd, null);
		}

		getLogger().vlogDebug("End @Class: TRUCollectionProductItemValidator, @method: validate()");

	}
}
