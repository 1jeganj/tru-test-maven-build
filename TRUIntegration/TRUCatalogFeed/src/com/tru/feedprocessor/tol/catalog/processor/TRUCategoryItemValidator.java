package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;

/**
 * The Class CategoryItemValidator. This class validates the mandatory properties of Categories.
 *  
 * @version 1.0
 * @author Professional Access
 */
public class TRUCategoryItemValidator implements IFeedItemValidator {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Validate. It validates whether the required properties of the category are there or not. If the required property is.
	 * not there it will throw the exception.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCategoryItemValidator, @method: validate()");

		TRUCategory categoryItem = (TRUCategory) pBaseFeedProcessVO;
		if (StringUtils.isBlank(categoryItem.getName())) {
			if (categoryItem.isRegistry()) {
				throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_REGISTRY, categoryItem, null);
			} else {
				throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_TAXONOMY, categoryItem, null);
			}
		}

		getLogger().vlogDebug("End @Class: TRUCategoryItemValidator, @method: validate()");

	}
}
