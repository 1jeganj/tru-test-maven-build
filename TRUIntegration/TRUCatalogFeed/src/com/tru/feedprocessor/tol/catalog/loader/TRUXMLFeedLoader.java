package com.tru.feedprocessor.tol.catalog.loader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.loader.IFeedDataLoader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCatalog;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.handler.TRUProductFeedHandler;
import com.tru.feedprocessor.tol.handler.TRURegsitryHandler;
import com.tru.feedprocessor.tol.handler.TRUTaxonomyHandler;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUXMLFeedLoader. This class calls SAXParser to parse the Feed file and store the data in respective entity
 * classes.
 * 
 * @author Professional Access
 * @version 1.0
 * 
 */
public class TRUXMLFeedLoader implements IFeedDataLoader {
	/**
	 * Property to hold EntityVOMap.
	 */
	private Map<String, List<? extends BaseFeedProcessVO>> mEntityVOMap;

	/** Property to hold logging. */
	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRUXMLFeedLoader.class);

	/**
	 * loadData. This method is overridden to parse all the feed files and load the data into respective entities
	 * 
	 * @param pCurExecCntx
	 *            the cur exec cntx
	 * @return the map< string, list<? extends base feed process v o>>
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(FeedExecutionContextVO pCurExecCntx) throws Exception {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.loadData BEGIN ");
		}
		FeedExecutionContextVO curExecCntx = pCurExecCntx;
		TRUCatalog catalog = new TRUCatalog();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		TRUProductFeedHandler productFeedHandler = new TRUProductFeedHandler();
		productFeedHandler.setInventoryFlag((Boolean) pCurExecCntx.getConfigValue(TRUProductFeedConstants.INVENTORY_FLAG));
		productFeedHandler.setEmptySkuPropertiesFlag((Boolean) pCurExecCntx.getConfigValue(TRUProductFeedConstants.EMPTY_SKU_PROPERTIES_FLAG));
		
		TRUFeedCatalogProperty feedCatalogProperty = (TRUFeedCatalogProperty) pCurExecCntx.getConfigValue(TRUProductFeedConstants.FEED_CATALOG_PROPERTY);
		
		productFeedHandler.setFeedCatalogProperty(feedCatalogProperty);
		settingContextVariables(curExecCntx);
		
		if (!curExecCntx.getFileName().endsWith(FeedConstants.TABLE)) {
			String fileName = curExecCntx.getFileName();
			String[] lFileList = fileName.split(TRUFeedConstants.DOLLAR);
			for (String file : lFileList) {
				parseFeedFiles(pCurExecCntx,
						catalog, saxParser,
						productFeedHandler, file);
			}
			settingAllProductSkuVOs(curExecCntx, catalog, productFeedHandler);
			List<TRUCategory> truCategoryList = catalog.getTRUCategory();
			
			curExecCntx.setConfigValue(TRUProductFeedConstants.TRUCATEGORYLIST, truCategoryList);
			mEntityVOMap = processEntityMapVO(catalog);

		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.loadData END::");
		}
		return mEntityVOMap;
	}

	/**
	 * Sets the ting context variables.
	 *
	 * @param pCurExecCntx the new ting context variables
	 */
	private void settingContextVariables(FeedExecutionContextVO pCurExecCntx) {
		int taxonomyVOCount;
		int registryVOCount;
		int productVOCount;
		int skuVOCount;
		int prodMapperCount;
		int collProdMapperCount;
		int skuMapperCount;
		int classfMapperCount;
		int RegistryMapperCount;
		int prodRelMapperCount;
		int collProdRelMapperCount;
		int classfRelMapperCount;
		int RegistryRelMapperCount;
		taxonomyVOCount = registryVOCount = productVOCount = skuVOCount =  prodMapperCount = collProdMapperCount = skuMapperCount = classfMapperCount = RegistryMapperCount = prodRelMapperCount = collProdRelMapperCount = classfRelMapperCount = RegistryRelMapperCount = 0;
		if(pCurExecCntx.getConfigValue(TRUProductFeedConstants.TCOUNT) == null){
			pCurExecCntx.setConfigValue(TRUProductFeedConstants.TCOUNT, taxonomyVOCount);
			pCurExecCntx.setConfigValue(TRUProductFeedConstants.RCOUNT, registryVOCount);
			pCurExecCntx.setConfigValue(TRUProductFeedConstants.PCOUNT, productVOCount);
			pCurExecCntx.setConfigValue(TRUProductFeedConstants.SCOUNT, skuVOCount);
			pCurExecCntx.setConfigValue(TRUProductFeedConstants.PMCOUNT, prodMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.CPMCOUNT, collProdMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.SMCOUNT, skuMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.CMCOUNT, classfMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.RMCOUNT, RegistryMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.PRMCOUNT, prodRelMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.CPRMCOUNT, collProdRelMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.CRMCOUNT, classfRelMapperCount);
		    pCurExecCntx.setConfigValue(TRUProductFeedConstants.RRMCOUNT, RegistryRelMapperCount);
		    
		}
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 * @param pAlertLogger the alert logger
	 */
	private void logSuccessMessage(String pStartDate, String pMessage, String pFileName, TRUAlertLogger pAlertLogger) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		pAlertLogger.logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_PARSING, pFileName, extenstions);
	}

	
	/**
	 * Parses the feed files.
	 *
	 * @param pCurExecCntx the cur exec cntx
	 * @param pCatalog the catalog
	 * @param pSaxParser the sax parser
	 * @param pProductFeedHandler the product feed handler
	 * @param pFile the file
	 * @throws FileNotFoundException the file not found exception
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws FeedSkippableException the feed skippable exception
	 */
	private void parseFeedFiles(FeedExecutionContextVO pCurExecCntx,
			TRUCatalog pCatalog, SAXParser pSaxParser, TRUProductFeedHandler pProductFeedHandler,
			String pFile) throws FileNotFoundException, SAXException,
			IOException, FeedSkippableException {
		InputStream lFileInputStream;
		int taxonomyVOCount;
		int registryVOCount;
		TRUAlertLogger alertLogger = (TRUAlertLogger) pCurExecCntx.getConfigValue(TRUProductFeedConstants.ALERT_LOGGER);
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		if (pFile.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
			// Loads the data from the web site taxonomy and check the threshold
			logSuccessMessage(startDate, TRUProductFeedConstants.PARSE_TAX, pFile, alertLogger);
			lFileInputStream = new FileInputStream(pCurExecCntx.getFilePath() + pFile);
			TRUTaxonomyHandler truTaxonomyHandler = new TRUTaxonomyHandler();
			truTaxonomyHandler.setFileName(pFile);
			pSaxParser.parse(lFileInputStream, truTaxonomyHandler);
			pCurExecCntx.setConfigValue(TRUFeedConstants.ENTITY_KEYS, truTaxonomyHandler.getEntityKeySet());
			isThresholdExceeded(pCurExecCntx, truTaxonomyHandler
					.getTruClassificationVOs().size() + truTaxonomyHandler.getCategories().size() + truTaxonomyHandler.getTRUCollectionProductVOs().size(),
					TRUFeedConstants.WEB_SITE_TAXONOMY);
				pCurExecCntx.setConfigValue(TRUFeedConstants.TAXONOMY_RECORD_COUNT, truTaxonomyHandler
						.getTruClassificationVOs().size() + truTaxonomyHandler.getCategories().size());
				pCatalog.getTRUClassification().addAll(truTaxonomyHandler.getTruClassificationVOs());
				pCatalog.getTRUCategory().addAll(truTaxonomyHandler.getCategories());
				pCatalog.getCollectionProductVO().addAll(truTaxonomyHandler.getTRUCollectionProductVOs());
				pCatalog.getClassificationIdAndRootCategoryMap().putAll(
						truTaxonomyHandler.getClassificationIdAndRootCategoryMap());
				pCatalog.setCollectionProdcutsMap(truTaxonomyHandler.getCollectionProdcutsMap());
				pProductFeedHandler.setClassificationIdAndRootCategoryMap(pCatalog
						.getClassificationIdAndRootCategoryMap());
				pProductFeedHandler.setCollectionProdcutsMap(truTaxonomyHandler.getCollectionProdcutsMap());
				taxonomyVOCount = truTaxonomyHandler.getTruClassificationVOs().size() + truTaxonomyHandler.getTRUCollectionProductVOs().size();
				if (mLogging.isLoggingDebug()) {
					mLogging.logDebug("Taxonomy VO Count: "+taxonomyVOCount);
				}

		} else if (pFile.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
			// Loads the data from the registry taxonomy and check the threshold
			logSuccessMessage(startDate, TRUProductFeedConstants.PARSE_REG, pFile, alertLogger);
			lFileInputStream = new FileInputStream(pCurExecCntx.getFilePath() + pFile);
			TRURegsitryHandler tRURegsitryHandler = new TRURegsitryHandler();
			tRURegsitryHandler.setFileName(pFile);
			pSaxParser.parse(lFileInputStream, tRURegsitryHandler);
			pCurExecCntx.setConfigValue(TRUFeedConstants.ENTITY_KEYS_REG, tRURegsitryHandler.getEntityKeySet());
			isThresholdExceeded(pCurExecCntx, tRURegsitryHandler
					.getTruClassificationVOs().size() + tRURegsitryHandler.getTRUCategories().size(),
					TRUFeedConstants.REGISTRY_CATEGORIZATION);
				pCurExecCntx.setConfigValue(TRUFeedConstants.REGISTRY_RECORD_COUNT, tRURegsitryHandler
						.getTruClassificationVOs().size() + tRURegsitryHandler.getTRUCategories().size());
				pCatalog.getTRURegistryClassification().addAll(tRURegsitryHandler.getTruClassificationVOs());
				pCatalog.getTRUCategory().addAll(tRURegsitryHandler.getTRUCategories());
				registryVOCount = tRURegsitryHandler.getTruClassificationVOs().size();
				if (mLogging.isLoggingDebug()) {
					mLogging.logDebug("Registry VO Count: "+registryVOCount);
				}
		} else if (pFile.contains(TRUFeedConstants.ATG_PRODUCT)) {
			// Loads the data from the product taxonomy and check the threshold
			logSuccessMessage(startDate, TRUProductFeedConstants.PARSE_PROD, pFile, alertLogger);
			lFileInputStream = new FileInputStream(pCurExecCntx.getFilePath() + pFile);
			if (pFile.contains(TRUProductFeedConstants.CANONICAL_FULL)) {
				pProductFeedHandler.setFeed(TRUProductFeedConstants.FULL);
				pProductFeedHandler.setFileName(pFile);
			} else if(pFile.contains(TRUProductFeedConstants.CANONICAL_DELETE)) {
				pProductFeedHandler.setFeed(TRUProductFeedConstants.DELETE);
				pProductFeedHandler.setFileName(pFile);
			} else {
				pProductFeedHandler.setFeed(TRUProductFeedConstants.DELTA);
				String[] feedFileNames = pFile.split(TRUFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
				String seqNumber = feedFileNames[TRUFeedConstants.NUMBER_SIX];
				String sequence = seqNumber.substring(TRUFeedConstants.NUMBER_ZERO, seqNumber.length() - TRUFeedConstants.NUMBER_FOUR);
				pProductFeedHandler.setSequenceNumber(sequence);
				pProductFeedHandler.setFileName(pFile);
				
			}
			pSaxParser.parse(lFileInputStream, pProductFeedHandler);
			pProductFeedHandler.setSequenceNumber(null);
		}
	}

	/**
	 * Setting all product sku v os.
	 *
	 * @param pCurExecCntx the cur exec cntx
	 * @param pCatalog the catalog
	 * @param pProductFeedHandler the product feed handler
	 */
	private void settingAllProductSkuVOs(FeedExecutionContextVO pCurExecCntx,
			TRUCatalog pCatalog, TRUProductFeedHandler pProductFeedHandler) {
		int productVOCount;
		int skuVOCount;
		pCatalog.getTRUProductFeedVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUProductFeedVOList());
		pCatalog.getTRUSKUFeedVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUSKUFeedVOList());
		pCatalog.getTRUStrollerSKUList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUStrollerSKUList());
		pCatalog.getTRUCribSKUVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUCribSKUVOList());
		pCatalog.getTRUCarSeatSKUVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUCarSeatSKUVOList());
		pCatalog.getTRUBookCdDvDSKUVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUBookCdDvDSKUVOList());
		pCatalog.getTRUVideoGameSKUVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUVideoGameSKUVOList());
		pCatalog.getTRUNonMerchSKUVOList().addAll(pProductFeedHandler.getTRUProductsVO().getTRUNonMerchSKUVOList());
		int totalProductCanonicalCount = pProductFeedHandler.getTRUProductsVO().getTRUProductFeedVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUSKUFeedVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUStrollerSKUList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUCribSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUCarSeatSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUBookCdDvDSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUVideoGameSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUNonMerchSKUVOList().size();
		pCurExecCntx.setConfigValue(
				TRUProductFeedConstants.PRODUCT_CANONICAL_TOTAL_COUNT, totalProductCanonicalCount);
		productVOCount = pProductFeedHandler.getTRUProductsVO().getTRUProductFeedVOList().size();
		skuVOCount = pProductFeedHandler.getTRUProductsVO().getTRUSKUFeedVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUStrollerSKUList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUCribSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUCarSeatSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUBookCdDvDSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUVideoGameSKUVOList().size()
				+ pProductFeedHandler.getTRUProductsVO().getTRUNonMerchSKUVOList().size();
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("productVO Count: "+productVOCount+" skuVO Count :"+skuVOCount);
		}
	}

	/**
	 * Checks if is threshold exceeded.
	 *
	 * @param pCurExecCntx            the cur exec cntx
	 * @param pCurrentFeedRecordCount            the current feed record count
	 * @param pFeedType            the feed type
	 * @return true, if is threshold exceeded
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	private boolean isThresholdExceeded(FeedExecutionContextVO pCurExecCntx, int pCurrentFeedRecordCount, String pFeedType)
			throws IOException, FeedSkippableException {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.loadData isThresholdExceeded::");
		}
		if (pCurExecCntx != null &&
				pCurExecCntx.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_PATH) != null &&
				pCurExecCntx.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_NAME) != null) {
			String catalogPreviousDayFilePath = (String) pCurExecCntx.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_PATH);
			String catalogPreviousDayFileName = ((Map<String, String>) pCurExecCntx.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_NAME)).get(pFeedType);
			checkForFile(catalogPreviousDayFilePath);
			File countFile = new File(catalogPreviousDayFilePath + catalogPreviousDayFileName);
			if (countFile == null ||!countFile.exists() ||!countFile.isFile() ||!countFile.canRead()) {
				return captureRecordCount(pCurrentFeedRecordCount, countFile);
			} else {
				double minPriorityThresholdP3 = (double) pCurExecCntx.getConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P3);
				double maxPriorityThresholdP3 = (double) pCurExecCntx.getConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P3);
				Writer writer = null;
				BufferedWriter bw = null;
				Reader reader = null;
				BufferedReader br = null;
				try {
					reader = new FileReader(countFile);
					br = new BufferedReader(reader);
					int count = Integer.parseInt(br.readLine());
					if (pCurrentFeedRecordCount < count) {
						Double percentage = calculatePercentage(count, pCurrentFeedRecordCount);
						if (percentage >= minPriorityThresholdP3 &&
								percentage <= maxPriorityThresholdP3) {
							// percentage is between the priority three threshold
							if (pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
								pCurExecCntx.setConfigValue(TRUFeedConstants.REGISTRY_ALERT, TRUFeedConstants.REGISTRY_P_THREE_ALERT);
							} else if (pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
								pCurExecCntx.setConfigValue(TRUFeedConstants.TAXONOMY_ALERT, TRUFeedConstants.TAXONOMY_P_THREE_ALERT);
							}
							writer = new FileWriter(countFile);
							bw = new BufferedWriter(writer);
							bw.flush();
							bw.write(Integer.toString(pCurrentFeedRecordCount));
							return false;
						} else if (percentage > maxPriorityThresholdP3) {
							// percentage is between the priority two threshold
							if (pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
								pCurExecCntx.setConfigValue(TRUFeedConstants.REGISTRY_ALERT, TRUFeedConstants.REGISTRY_P_TWO_ALERT);
								throw new FeedSkippableException(null, null, TRUFeedConstants.TAXONOMY_P_TWO_ALERT, null, null);
							} else if (pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
								pCurExecCntx.setConfigValue(TRUFeedConstants.TAXONOMY_ALERT, TRUFeedConstants.TAXONOMY_P_TWO_ALERT);
								throw new FeedSkippableException(null, null, TRUFeedConstants.TAXONOMY_P_TWO_ALERT, null, null);
							}
						} else {
							writer = new FileWriter(countFile);
							bw = new BufferedWriter(writer);
							bw.flush();
							bw.write(Integer.toString(pCurrentFeedRecordCount));
							return false;
						}
					} else {
						writer = new FileWriter(countFile);
						bw = new BufferedWriter(writer);
						bw.flush();
						bw.write(Integer.toString(pCurrentFeedRecordCount));
						return false;
					}
				} catch (FileNotFoundException e) {
					if (mLogging.isLoggingError()) {
						mLogging.logError("FileNotFoundException : getObject=", e);
					}
				} catch (NumberFormatException e) {
					if (mLogging.isLoggingError()) {
						mLogging.logError("NumberFormatException : getObject=", e);
					}
				} catch (IOException e) {
					if (mLogging.isLoggingError()) {
						mLogging.logError("IOException : getObject=", e);
					}
				} finally {
					executeFinallyBlock(writer, bw, reader, br);
				}

			}
		}
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.loadData isThresholdExceeded::");
		}
		return false;
	}

	/**
	 * Check for file.
	 *
	 * @param pCatalogPreviousDayFilePath the catalog previous day file path
	 */
	private void checkForFile(String pCatalogPreviousDayFilePath) {
		File sourceFolder = new File(pCatalogPreviousDayFilePath);
		if (!sourceFolder.exists() &&
				!sourceFolder.isDirectory()) {
			sourceFolder.mkdirs();
		}
	}

	/**
	 * Execute finally block.
	 *
	 * @param pWriter the writer
	 * @param pBuffWriter the buff writer
	 * @param pReader the reader
	 * @param pBuffReader the buff reader
	 */
	private void executeFinallyBlock(Writer pWriter,
			BufferedWriter pBuffWriter, Reader pReader,
			BufferedReader pBuffReader) {
		try {
			if (null != pBuffReader) {
				pBuffReader.close();
			}
			if (null != pReader) {
				pReader.close();
			}
			if (null != pBuffWriter) {
				pBuffWriter.close();
			}
			if (null != pWriter) {
				pWriter.close();
			}
		} catch (IOException e) {
			if (mLogging.isLoggingError()) {
				mLogging.logError("IOException : getObject=", e);
			}
		}
	}

	/**
	 * Capture record count.
	 *
	 * @param pCurrentFeedRecordCount the current feed record count
	 * @param pCountFile the count file
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private boolean captureRecordCount(int pCurrentFeedRecordCount,
			File pCountFile) throws IOException {
		Writer writer;
		BufferedWriter bw;
		// if the file doesnt exist write the todays record count
		writer = new FileWriter(pCountFile);
		bw = new BufferedWriter(writer);
		try {
			bw.write(Integer.toString(pCurrentFeedRecordCount));
		} catch (IOException e) {
			if (mLogging.isLoggingError()) {
				mLogging.logError("IOException : getObject=", e);
			}
		} finally {
			try {
				if (null != bw) {
					bw.close();
				}
				if (null != writer) {
					writer.close();
				}
			} catch (IOException e) {
				if (mLogging.isLoggingError()) {
					mLogging.logError("IOException : getObject=", e);
				}
			}
		}
		return false;
	}

	/**
	 * Calculate percentage.
	 * 
	 * @param pCount
	 *            the count
	 * @param pCurrentFeedRecordCount
	 *            the current feed record count
	 * @return the double
	 */
	private Double calculatePercentage(int pCount, int pCurrentFeedRecordCount) {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.calculatePercentage BEGIN::");
		}
		BigDecimal todaysCount = BigDecimal.valueOf(pCurrentFeedRecordCount);
		BigDecimal previuosCount = BigDecimal.valueOf(pCount);
		BigDecimal percentage = previuosCount.subtract(todaysCount)
				.multiply(BigDecimal.valueOf(TRUFeedConstants.NUMBER_HUNDRED))
				.divide(previuosCount, FeedConstants.NUMBER_TWO, BigDecimal.ROUND_HALF_UP);
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.calculatePercentage END::");
		}
		return percentage.doubleValue();
	}

	/**
	 * Process entity map vo.
	 * 
	 * @param pObj
	 *            the obj
	 * @return the map< string, list<? extends base feed process v o>>
	 */
	public Map<String, List<? extends BaseFeedProcessVO>> processEntityMapVO(Object pObj) {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.processEntityMapVO BEGIN::");
		}
		TRUCatalog lCatalog = (TRUCatalog) pObj;

		Map<String, List<? extends BaseFeedProcessVO>> lBaseFeedVOMap = new HashMap<String, List<? extends BaseFeedProcessVO>>();

		lBaseFeedVOMap.put(FeedConstants.CATEGORY_ITEMDESCRIPTOR, lCatalog.getTRUCategory());
		lBaseFeedVOMap.put(TRUFeedConstants.CLASSIFICATION_ENTITY, lCatalog.getTRUClassification());
		lBaseFeedVOMap.put(TRUFeedConstants.REGISTRY_CLASSIFICATION_ENTITY, lCatalog.getTRURegistryClassification());
		lBaseFeedVOMap.put(FeedConstants.PRODUCT_ITEMDESCRIPTOR, lCatalog.getTRUProductFeedVOList());
		lBaseFeedVOMap.put(TRUFeedConstants.COLLECTION_PRODUCT, lCatalog.getCollectionProductVO());
		lBaseFeedVOMap.put(TRUProductFeedConstants.REGULAR_SKU, lCatalog.getTRUSKUFeedVOList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.STROLLER_SKU, lCatalog.getTRUStrollerSKUList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.CRIB_SKU, lCatalog.getTRUCribSKUVOList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.CAR_SEAT_SKU, lCatalog.getTRUCarSeatSKUVOList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.BOOKCDDVD_SKU, lCatalog.getTRUBookCdDvDSKUVOList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.VIDEOGAME_SKU, lCatalog.getTRUVideoGameSKUVOList());
		lBaseFeedVOMap.put(TRUProductFeedConstants.NONMERCHSKU, lCatalog.getTRUNonMerchSKUVOList());
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("TRUXMLFeedLoader.processEntityMapVO END::");
		}
		return lBaseFeedVOMap;
	}
}
