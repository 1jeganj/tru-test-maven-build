package com.tru.feedprocessor.tol.catalog.mapper;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;

/**
 * The Class CategoryMapper. This class maps the data stored in entities to repository to push the data.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCategoryMapper extends AbstractFeedItemMapper<TRUCategory> {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Maps the Values.
	 * 
	 * This Method map() will set the repository properties with the values from the feed.
	 * 
	 * This Method sets the property value of the product item in the catalog.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 */
	@Override
	public MutableRepositoryItem map(TRUCategory pVOItem, MutableRepositoryItem pRepoItem) {

		getLogger().vlogDebug("Start @Class: TRUCategoryMapper, @method: map()");
		
		String method = TRUProductFeedConstants.CATEGORY_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}

		if (!StringUtils.isBlank(pVOItem.getName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductDisplayName(), pVOItem.getName());
		}
		if (!StringUtils.isBlank(pVOItem.getID())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getID());
		}
		if (!StringUtils.isBlank(pVOItem.getName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getName());
		}

		getLogger().vlogDebug("End @Class: TRUCategoryMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		return pRepoItem;

	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty.
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set.
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
