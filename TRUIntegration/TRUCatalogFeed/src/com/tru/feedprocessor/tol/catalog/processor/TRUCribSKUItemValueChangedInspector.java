package com.tru.feedprocessor.tol.catalog.processor;

import java.util.List;
import java.util.Map;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValueChangedInspector;
import com.tru.feedprocessor.tol.catalog.vo.TRUCribSKUVO;

/**
 * The Class TRUCribSKUItemValueChangedInspector. This class inspects each and every property holds in entites to repository
 * of SKU properties and CribSKU properties has any changes. If any change occurs in one property entire entity will be
 * updated.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCribSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/** property to hold CommonSKUItemValueChangedInspector holder. */
	private TRUCommonSKUItemValueChangedInspector mCommonSKUItemValueChangedInspector;

	/**
	 * Gets the common sku item value changed inspector.
	 * 
	 * @return the commonSKUItemValueChangedInspector
	 */
	public TRUCommonSKUItemValueChangedInspector getCommonSKUItemValueChangedInspector() {
		return mCommonSKUItemValueChangedInspector;
	}

	/**
	 * Sets the common sku item value changed inspector.
	 * 
	 * @param pCommonSKUItemValueChangedInspector
	 *            the commonSKUItemValueChangedInspector to set
	 */
	public void setCommonSKUItemValueChangedInspector(
			TRUCommonSKUItemValueChangedInspector pCommonSKUItemValueChangedInspector) {
		mCommonSKUItemValueChangedInspector = pCommonSKUItemValueChangedInspector;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the SKU properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCribSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUCribSKUVO) {
			TRUCribSKUVO skuVO = (TRUCribSKUVO) pBaseFeedProcessVO;
			Map<String, String> cribPropertiesMap = (Map<String, String>) pRepositoryItem
					.getPropertyValue(getFeedCatalogProperty().getCribPropertiesMap());
			if (getCommonSKUItemValueChangedInspector().isUpdated(skuVO, pRepositoryItem)) {
				return retVal = Boolean.TRUE;
			}
			
			
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_MATERIAL_TYPE)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribMaterialTypes()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String cribMaterialTypes : skuVO.getCribMaterialTypes()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribMaterialTypes()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribMaterialTypes()))
										.contains(cribMaterialTypes)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}

			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_STYLE)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribStyles()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String cribStyles : skuVO.getCribStyles()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribStyles()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribStyles())).contains(cribStyles)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_TYPE)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribTypes()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String cribTypes : skuVO.getCribTypes()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribTypes()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribTypes())).contains(cribTypes)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_WHAT_IS_IMP)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribWhatIsImp()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String cribWhatisImp : skuVO.getCribWhatIsImp()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribWhatIsImp()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCribWhatIsImp())).contains(cribWhatisImp)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			
			if (!skuVO.getCribPropertiesMap().isEmpty()
					&& !getCommonSKUItemValueChangedInspector()
							.mapsAreEqual(skuVO.getCribPropertiesMap(), cribPropertiesMap)) {
				return retVal = Boolean.TRUE;
			}

		}

		getLogger().vlogDebug("End @Class: TRUCribSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
}
