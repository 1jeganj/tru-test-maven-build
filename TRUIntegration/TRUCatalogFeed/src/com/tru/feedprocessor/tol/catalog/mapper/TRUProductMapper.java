package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUBatteryQuantitynTypeVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;

/**
 * The Class TRUProductMapper. This class maps the data stored in entities to repository to push the data of properties and
 * it is used to write the VO properties to the ProductCatalog repository.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUProductMapper extends AbstractFeedItemMapper<TRUProductFeedVO> {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;
	
	/**
	 * The variable to hold "FeedRepository" property name.
	 */
	private MutableRepository mFeedRepository;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	

	/**
	 * Setting common Product properties it is used to write the VO properties to the ProductCatalog repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUProductFeedVO pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUProductMapper, @method: map()");
		
		String method = TRUProductFeedConstants.PRODUCT_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}

		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.PMCOUNT);
		count++;
		getLogger().vlogDebug("prodMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.PMCOUNT,count);
		
		if (!StringUtils.isBlank(pVOItem.getFeed())
				&& pVOItem.getFeed().equalsIgnoreCase(
						TRUProductFeedConstants.DELETE)
				&& !StringUtils.isBlank(pVOItem.getFeedActionCode())
				&& pVOItem.getFeedActionCode().equalsIgnoreCase(
						TRUProductFeedConstants.D)) {
			
			deleteProductProperties(pRepoItem);
			
			return pRepoItem;
		}

		if (!StringUtils.isBlank(pVOItem.getId())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getId());
		}

		if (!StringUtils.isBlank(pVOItem.getOnlineTitle())) {   
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductDisplayName(), pVOItem.getOnlineTitle());
		} else if (pVOItem.getChildSKUsList().size() > TRUProductFeedConstants.NUMBER_ZERO 	&&
				!StringUtils.isBlank(pVOItem.getChildSKUsList().get(0).getOnlineTitle()) && StringUtils.isEmpty((String) pRepoItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getChildSKUsList().get(0)
					.getOnlineTitle());
		}

		if (!StringUtils.isBlank(pVOItem.getOnlineLongDesc())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), pVOItem.getOnlineLongDesc());
		}

		if (!StringUtils.isBlank(pVOItem.getBatteryIncluded())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryIncluded(), pVOItem.getBatteryIncluded());
		}

		if (!StringUtils.isBlank(pVOItem.getBatteryRequired())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryRequired(), pVOItem.getBatteryRequired());
		}

		if (!StringUtils.isBlank(pVOItem.getCost())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCost(), pVOItem.getCost());
		}

		if (!StringUtils.isBlank(pVOItem.getEwasteSurchargeSku())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeSku(), pVOItem.getEwasteSurchargeSku());
		}

		if (!StringUtils.isBlank(pVOItem.getEwasteSurchargeState())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeState(), pVOItem.getEwasteSurchargeState());
		}

		if (!StringUtils.isBlank(pVOItem.getGender())) {
			if (pVOItem.getGender().contains(TRUProductFeedConstants.BOY)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.BOY);
			} else if (pVOItem.getGender().contains(TRUProductFeedConstants.GIRL)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.GIRL);
			} else if (pVOItem.getGender().contains(TRUProductFeedConstants.BOTH)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), TRUProductFeedConstants.BOTH);
			}
		}

		if (!StringUtils.isBlank(pVOItem.getRusItemNumber())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRusItemNumber(),
					Long.parseLong(pVOItem.getRusItemNumber()));
		}

		if (!StringUtils.isBlank(pVOItem.getShipFromStoreEligible())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipFromStoreEligible(),
					pVOItem.getShipFromStoreEligible());
		}

		if (!StringUtils.isBlank(pVOItem.getSpecialAssemblyInstructions())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSpecialAssemblyInstructions(),
					pVOItem.getSpecialAssemblyInstructions());
		}

		if (!StringUtils.isBlank(pVOItem.getTaxCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxCode(), pVOItem.getTaxCode());
		}

		if (!StringUtils.isBlank(pVOItem.getTaxProductValueCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxProductValueCode(), pVOItem.getTaxProductValueCode());
		}

		if (!StringUtils.isBlank(pVOItem.getVendorsProductDemoUrl())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl(),
					pVOItem.getVendorsProductDemoUrl());
		}

		if (!StringUtils.isBlank(pVOItem.getManufacturerStyleNumber())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getManufacturerStyleNumber(),
					pVOItem.getManufacturerStyleNumber());
		}

		if (!StringUtils.isBlank(pVOItem.getNonMerchandiseFlag())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getNonMerchandiseFlag(), pVOItem.getNonMerchandiseFlag());
		}

		if (!StringUtils.isBlank(pVOItem.getCountryEligibility())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCountryEligibility(), pVOItem.getCountryEligibility());
		}

		if (!StringUtils.isBlank(pVOItem.getVendorClassification())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getVendorClassification(), pVOItem.getVendorClassification());
		}
		if (!pVOItem.getBatteryQuantitynTypeList().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteries(),
					createBatteryQuantitynType(pVOItem.getBatteryQuantitynTypeList(),pRepoItem));
		}
		
		// Properties included from Product relationship  mapper
		if (!StringUtils.isBlank(pVOItem.getRegistryClassification())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryClassification(), pVOItem.getRegistryClassification());
		}
		
		// Delta Feed changes

		if (!StringUtils.isBlank(pVOItem.getSizeChartName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSizeChartName(), pVOItem.getSizeChartName());
		}

		if (!pVOItem.getStyleDimensions().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStyleDimensions(), pVOItem.getStyleDimensions());
		}

		getLogger().vlogDebug("End @Class: TRUProductMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}


		return pRepoItem;
	}
	
	/**
	 * Delete product properties.
	 *
	 * @param pRepoItem the repo item
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	protected void deleteProductProperties(MutableRepositoryItem pRepoItem) throws RepositoryException,
	FeedSkippableException {
		getLogger().vlogDebug("Start @Class: TRUProductMapper, @method: deleteProductProperties()");
		//setting all properties to null for annual item delete process
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryIncluded(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteryRequired(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCost(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeSku(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getEwasteSurchargeState(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getGender(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRusItemNumber(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipFromStoreEligible(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSpecialAssemblyInstructions(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getTaxProductValueCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getManufacturerStyleNumber(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getNonMerchandiseFlag(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCountryEligibility(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorClassification(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteries(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryClassification(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSizeChartName(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrand(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getWhyWeLoveIt(), null);
		
		getLogger().vlogDebug("End @Class: TRUProductMapper, @method: deleteProductProperties()");
	}

	/**
	 * Creates the battery quantityn type.
	 *
	 * @param pTRUBatteryQuantitynTypeVO            the TRU battery quantityn type vo
	 * @param pRepoItem the repo item
	 * @return the list
	 * @throws RepositoryException             the repository exception
	 */
	protected List<MutableRepositoryItem> createBatteryQuantitynType(
			List<TRUBatteryQuantitynTypeVO> pTRUBatteryQuantitynTypeVO, MutableRepositoryItem pRepoItem) throws RepositoryException {

		getLogger().vlogDebug("Start @Class: TRUProductMapper, @method: createBatteryQuantitynType()");
		List<MutableRepositoryItem> feedRepositoryItems = new ArrayList<MutableRepositoryItem>();
		List<RepositoryItem> batteries = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getBatteries());
		if(!batteries.isEmpty()){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBatteries(), feedRepositoryItems);
			for(RepositoryItem repoItem : batteries){
				getFeedRepository().removeItem(repoItem.getRepositoryId(), getFeedCatalogProperty().getBattery());
			}
		}
		for (TRUBatteryQuantitynTypeVO battery : pTRUBatteryQuantitynTypeVO) {
			MutableRepositoryItem FeedRepositoryItem = getFeedRepository().createItem(getFeedCatalogProperty().getBattery());
			FeedRepositoryItem.setPropertyValue(getFeedCatalogProperty().getBatteryType(), battery.getBatteryType());
			if(battery.getBatteryQuantity() != null){
				FeedRepositoryItem.setPropertyValue(getFeedCatalogProperty().getBatteryQuantity(),
						Integer.parseInt(battery.getBatteryQuantity()));
			}
			
			getFeedRepository().addItem(FeedRepositoryItem);
			feedRepositoryItems.add(FeedRepositoryItem);
		}

		getLogger().vlogDebug("End @Class: TRUProductMapper, @method: createBatteryQuantitynType()");

		return feedRepositoryItems;
	}
	
	

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}
	
	/**
	 * Gets the feed repository.
	 * 
	 * @return the feedRepository
	 */
	public MutableRepository getFeedRepository() {
		return mFeedRepository;
	}

	/**
	 * Sets the feed repository.
	 * 
	 * @param pFeedRepository
	 *            the feedRepository to set
	 */
	public void setFeedRepository(MutableRepository pFeedRepository) {
		mFeedRepository = pFeedRepository;
	}

}
