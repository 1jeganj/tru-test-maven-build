package com.tru.feedprocessor.tol.catalog.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUCommonSKUItemValidator. This class validates the mandatory properties of SKU
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCommonSKUItemValidator {

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Validate sku properties whether the mandatory values are there in sku or not and any parse exception.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void validateSKUProperties(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCommonSKUItemValidator, @method: validateSKUProperties()");

		if (pBaseFeedProcessVO instanceof TRUSkuFeedVO) {
			TRUSkuFeedVO skuVO = (TRUSkuFeedVO) pBaseFeedProcessVO;
			if(!StringUtils.isBlank(skuVO.getFeed()) && skuVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && StringUtils.isBlank(skuVO.getFeedActionCode())){
				skuVO.setErrorMessage(TRUProductFeedConstants.ACTION_CODE_EMPTY);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_EMPTY, skuVO,
						null);
			}
			if ((!StringUtils.isBlank(skuVO.getShipFromStoreEligibleLov()) && !(skuVO.getShipFromStoreEligibleLov()
							.equalsIgnoreCase(TRUProductFeedConstants.Y) || skuVO.getShipFromStoreEligibleLov()
							.equalsIgnoreCase(TRUProductFeedConstants.N)))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.SHIP_FROM_STORE_ELIGIBLE_LOV_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getDropshipFlag()) && !(skuVO.getDropshipFlag().equalsIgnoreCase(
					TRUProductFeedConstants.Y) || skuVO.getDropshipFlag().equalsIgnoreCase(TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.DROPSHIP_FLAG_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getOutlet()) && !(skuVO.getOutlet().equalsIgnoreCase(
					TRUProductFeedConstants.Y) || skuVO.getOutlet().equalsIgnoreCase(TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.OUTLET_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getShipInOrigContainer()) && !(skuVO.getShipInOrigContainer()
					.equalsIgnoreCase(TRUProductFeedConstants.Y) || skuVO.getShipInOrigContainer().equalsIgnoreCase(
					TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.SHIP_IN_ORIG_CONTAINER_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getSupressInSearch()) && !(skuVO.getSupressInSearch().equalsIgnoreCase(
					TRUProductFeedConstants.Y) || skuVO.getSupressInSearch().equalsIgnoreCase(
					TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.SUPRESS_IN_SEARCH_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getWebDisplayFlag()) && !(skuVO.getWebDisplayFlag().equalsIgnoreCase(
					TRUProductFeedConstants.A) || skuVO.getWebDisplayFlag().equalsIgnoreCase(
					TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.WEB_DISPLAY_FLAG_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getSlapperItem()) && !(skuVO.getSlapperItem().equalsIgnoreCase(
					TRUProductFeedConstants.Y) || skuVO.getSlapperItem().equalsIgnoreCase(TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.SLAPPER_ITEM_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if((!StringUtils.isBlank(skuVO.getRegistryEligibility()) && !(skuVO.getRegistryEligibility()
					.equalsIgnoreCase(TRUProductFeedConstants.Y) || skuVO.getRegistryEligibility().equalsIgnoreCase(
					TRUProductFeedConstants.N)))){
				skuVO.setErrorMessage(TRUProductFeedConstants.REGISTRY_WISH_LIST_ELIGIBILITY_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BOOLEAN, skuVO,
						null);
			}
			if (!StringUtils.isBlank(skuVO.getNmwaPercentage())) {
				try {
					Float.parseFloat(skuVO.getNmwaPercentage());
				} catch (NumberFormatException ex) {
					getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					skuVO.setErrorMessage(TRUProductFeedConstants.NMWA_PERCENTAGE_INVALID);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NMWA, skuVO,
							ex);
				}
			}
			if (!StringUtils.isBlank(skuVO.getReviewCountDecimal())) {
				try {
					Float.parseFloat(skuVO.getReviewCountDecimal());
				} catch (NumberFormatException ex) {
					getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					skuVO.setErrorMessage(TRUProductFeedConstants.REVIEWCOUNTDECIMAL_INVALID);
					throw new FeedSkippableException(null, null,
							TRUProductFeedConstants.INVALID_RECORD_PRODUCT_REVIEWCOUNTDECIMAL, skuVO, ex);
				}
			}
			if (!StringUtils.isBlank(skuVO.getSalesVolume())) {
				try {
					Long.parseLong(skuVO.getSalesVolume());
				} catch (NumberFormatException ex) {
					getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					skuVO.setErrorMessage(TRUProductFeedConstants.SALESVOLUME_INVALID);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_SALESVOLUME,
							skuVO, ex);
				}
			}
			if (!StringUtils.isBlank(skuVO.getStreetDate())) {
				try {
					SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
					formatter.parse(skuVO.getStreetDate());
				} catch (ParseException ex) {
					getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					skuVO.setErrorMessage(TRUProductFeedConstants.STREET_DATE_INVALID);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_STREET_DATE,
							skuVO, ex);
				}
			}
			if (!StringUtils.isBlank(skuVO.getInventoryReceivedDate())) {
				try {
					SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
					formatter.parse(skuVO.getInventoryReceivedDate());
				} catch (ParseException ex) {
					getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					skuVO.setErrorMessage(TRUProductFeedConstants.INVENTORY_RECEIVED_DATE_INVALID);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_INVENTORY_RECEIVED_DATE,
							skuVO, ex);
				}
			}
			if (StringUtils.isBlank(skuVO.getOnlineTitle())
					&& (!StringUtils.isBlank(skuVO.getFeed()) && (skuVO.getFeed().equalsIgnoreCase(
							TRUProductFeedConstants.FULL) || (skuVO.getFeed()
							.equalsIgnoreCase(TRUProductFeedConstants.DELTA) && skuVO.getFeedActionCode().equalsIgnoreCase(
							TRUProductFeedConstants.A))))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.SKU_DISPLAY_NAME_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_DISPLAY_NAME,
						skuVO, null);
			}
			if (StringUtils.isBlank(skuVO.getId())) {
				skuVO.setErrorMessage(TRUProductFeedConstants.SKU_ID_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID, skuVO, null);
			}
			if (!StringUtils.isBlank(skuVO.getBackorderStatus())
					&& !(skuVO.getBackorderStatus().equalsIgnoreCase(TRUProductFeedConstants.N)
							|| skuVO.getBackorderStatus().equalsIgnoreCase(TRUProductFeedConstants.B) || skuVO
							.getBackorderStatus().equalsIgnoreCase(TRUProductFeedConstants.R))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.BACK_ORDER_STATUS_INVALID);
				throw new FeedSkippableException(null, null,
						TRUProductFeedConstants.INVALID_RECORD_PRODUCT_BACK_ORDER_STATUS, skuVO, null);
			}
			if (!StringUtils.isBlank(skuVO.getPriceDisplay())
					&& !(skuVO.getPriceDisplay().equalsIgnoreCase(TRUProductFeedConstants.STANDARD)
							|| skuVO.getPriceDisplay().equalsIgnoreCase(TRUProductFeedConstants.PRICE_ON_PRODUCT_PAGE) || skuVO
							.getPriceDisplay().equalsIgnoreCase(TRUProductFeedConstants.PRICE_ON_CART_PAGE))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.PRICE_DISPLAY_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_PRICE_DISPLAY,
						skuVO, null);
			}
			if (!StringUtils.isBlank(skuVO.getNmwa())
					&& !(skuVO.getNmwa().equalsIgnoreCase(TRUProductFeedConstants.Y) || skuVO.getNmwa().equalsIgnoreCase(
							TRUProductFeedConstants.N))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.NMWA_INVALID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NMWA_VALUE,
						skuVO, null);
			}
			if(skuVO.getUpcNumbers() != null && !skuVO.getUpcNumbers().isEmpty()){
				Boolean check = Boolean.FALSE;
				for(String upcNumber:skuVO.getUpcNumbers()){
					if(upcNumber.length()>TRUProductFeedConstants.NUMBER_THIRTEEN){
						check = Boolean.TRUE;
						break;
					}
				}
				if(check){
					skuVO.setErrorMessage(TRUProductFeedConstants.UPC_NUMBER_INVALID);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_UPC_NUMBER,
							skuVO, null);
				}
			}
			if (!StringUtils.isBlank(skuVO.getChannelAvailability())
					&& !(skuVO.getChannelAvailability().equalsIgnoreCase(TRUProductFeedConstants.IN_STORE_ONLY)
							|| skuVO.getChannelAvailability().equalsIgnoreCase(TRUProductFeedConstants.ONLINE_ONLY) || skuVO.getChannelAvailability()
							.equalsIgnoreCase(TRUProductFeedConstants.N_A))) {
				skuVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_CHANNEL_AVAILABILITY);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_CHANNEL_AVAILABILITY, skuVO, null);
			}
		}

		getLogger().vlogDebug("End @Class: TRUCommonSKUItemValidator, @method: validateSKUProperties()");
	}
}
