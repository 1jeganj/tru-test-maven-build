package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUNonMerchSKUVO;

/**
 * The Class TRUNonMerchSKUItemValueChangedInspector. This class inspects each and every property holds in entites to
 * repository of NonMerchSKU properties has any changes. If any change occurs in one property entire entity will be updated.
 * @author Professional Access.
 * @version 1.0.
 */
public class TRUNonMerchSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** The mLogger. */
	private FeedLogger mLogger;

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * This method is used to inspect the NonMerchSKU properties of entites with repository items for any changes. If any
	 * change occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUNonMerchSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

			if (pBaseFeedProcessVO instanceof TRUNonMerchSKUVO && !StringUtils.isBlank(((TRUNonMerchSKUVO) pBaseFeedProcessVO).getSubType()) && 
					!((TRUNonMerchSKUVO) pBaseFeedProcessVO).getSubType().equals(
							pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSubType()))) {
				retVal = Boolean.TRUE;
				return retVal ;
			}

		getLogger().vlogDebug("End @Class: TRUNonMerchSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}
}
