package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUProductFeedVO. VO class which will store all the product item properties and sku ids.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUProductFeedVO extends BaseFeedProcessVO {

	/** property to hold mId holder. */
	private String mId;

	/** property to hold OnlineTitle. */
	private String mOnlineTitle;

	/** property to hold OnlineLongDesc. */
	private String mOnlineLongDesc;

	/** property to hold BatteryIncluded. */
	private String mBatteryIncluded;

	/** property to hold BatteryRequired. */
	private String mBatteryRequired;

	/** property to hold Cost. */
	private String mCost;

	/** property to hold EwasteSurchargeSku. */
	private String mEwasteSurchargeSku;

	/** property to hold EwasteSurchargeState. */
	private String mEwasteSurchargeState;

	/** property to hold Gender. */
	private String mGender;

	/** property to hold RusItemNumber. */
	private String mRusItemNumber;

	/** property to hold ShipFromStoreEligible. */
	private String mShipFromStoreEligible;

	/** property to hold SpecialAssemblyInstructions. */
	private String mSpecialAssemblyInstructions;

	/** property to hold TaxCode. */
	private String mTaxCode;

	/** property to hold TaxProductValueCode. */
	private String mTaxProductValueCode;

	/** property to hold ManufacturerStyleNumber. */
	private String mManufacturerStyleNumber;

	/** property to hold VendorsProductDemoUrl. */
	private String mVendorsProductDemoUrl;

	/** property to hold NonMerchandiseFlag. */
	private String mNonMerchandiseFlag;

	/** property to hold CountryEligibility. */
	private String mCountryEligibility;

	/** property to hold RootParentCategories. */
	private Set<String> mRootParentCategories;

	/** property to hold ParentClassifications. */
	private List<String> mParentClassifications;

	/** property to hold RegistryClassification. */
	private String mRegistryClassification;

	/** property to hold VendorClassification. */
	private String mVendorClassification;

	/** property to hold SizeChartName. */
	private String mSizeChartName;

	/** property to hold ChildSKUsList. */
	private List<TRUSkuFeedVO> mChildSKUsList;

	/** property to hold ChildSKUIds. */
	private List<String> mChildSKUIds;

	/** property to hold StyleDimensions. */
	private List<String> mStyleDimensions;

	/** property to hold StyleUIDs. */
	private Map<String, String> mStyleUIDs;

	/** property to hold Feed. */
	private String mFeed;

	/** property to hold FeedActionCode. */
	private String mFeedActionCode;

	/** property to hold Type. */
	private String mType;
	
	/** property to hold ClassificationIdAndRootCategoryMap. */
	private Map<String,Set<String>> mClassificationIdAndRootCategoryMap;
	
	/** property to DivisionCode Type. */
	private String mDivisionCode;
	
	/** property to DepartmentCode Type. */
	private String mDepartmentCode;
	
	/** property to ClassCode Type. */
	private String mClassCode;
	
	/** property to SubclassCode Type. */
	private String mSubclassCode;
	
	/** property to Source Type. */
	private String mSource;
	
	/** property to ErrorMessage Type. */
	private String mErrorMessage;
	
	/** property to hold mSequenceNumber. */
	private String mSequenceNumber;
	
	/** property to hold BatteryQuantitynTypeList. */
	private List<TRUBatteryQuantitynTypeVO> mBatteryQuantitynTypeList;
	
	/** property to hold FileName . */
	private String mFileName;
	
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}
	
	/**
	 * Instantiates a new TRU product feed vo.
	 */
	public TRUProductFeedVO() {
		setEntityName(TRUProductFeedConstants.PRODUCT);
	}
	
	/**
	 * Gets the battery quantityn type list.
	 * 
	 * @return the batteryQuantitynTypeList
	 */
	public List<TRUBatteryQuantitynTypeVO> getBatteryQuantitynTypeList() {
		if (mBatteryQuantitynTypeList == null) {
			mBatteryQuantitynTypeList = new ArrayList<TRUBatteryQuantitynTypeVO>();
		}
		return mBatteryQuantitynTypeList;
	}

	/**
	 * Sets the battery quantityn type list.
	 * 
	 * @param pBatteryQuantitynTypeList
	 *            the batteryQuantitynTypeList to set
	 */
	public void setBatteryQuantitynTypeList(List<TRUBatteryQuantitynTypeVO> pBatteryQuantitynTypeList) {
		mBatteryQuantitynTypeList = pBatteryQuantitynTypeList;
	}
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the errorMessage to set
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource() {
		return mSource;
	}

	/**
	 * Sets the source.
	 *
	 * @param pSource the source to set
	 */
	public void setSource(String pSource) {
		mSource = pSource;
	}

	/**
	 * Gets the division code.
	 *
	 * @return the divisionCode
	 */
	public String getDivisionCode() {
		return mDivisionCode;
	}

	/**
	 * Sets the division code.
	 *
	 * @param pDivisionCode the divisionCode to set
	 */
	public void setDivisionCode(String pDivisionCode) {
		mDivisionCode = pDivisionCode;
	}

	/**
	 * Gets the department code.
	 *
	 * @return the departmentCode
	 */
	public String getDepartmentCode() {
		return mDepartmentCode;
	}

	/**
	 * Sets the department code.
	 *
	 * @param pDepartmentCode the departmentCode to set
	 */
	public void setDepartmentCode(String pDepartmentCode) {
		mDepartmentCode = pDepartmentCode;
	}

	/**
	 * Gets the class code.
	 *
	 * @return the classCode
	 */
	public String getClassCode() {
		return mClassCode;
	}

	/**
	 * Sets the class code.
	 *
	 * @param pClassCode the classCode to set
	 */
	public void setClassCode(String pClassCode) {
		mClassCode = pClassCode;
	}

	/**
	 * Gets the subclass code.
	 *
	 * @return the subclassCode
	 */
	public String getSubclassCode() {
		return mSubclassCode;
	}

	/**
	 * Sets the subclass code.
	 *
	 * @param pSubclassCode the subclassCode to set
	 */
	public void setSubclassCode(String pSubclassCode) {
		mSubclassCode = pSubclassCode;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return mType;
	}

	/**
	 * Sets the type.
	 * 
	 * @param pType
	 *            the type to set
	 */
	public void setType(String pType) {
		mType = pType;
	}

	/**
	 * Gets the feed action code.
	 * 
	 * @return the feedActionCode
	 */
	public String getFeedActionCode() {
		return mFeedActionCode;
	}

	/**
	 * Sets the feed action code.
	 * 
	 * @param pFeedActionCode
	 *            the feedActionCode to set
	 */
	public void setFeedActionCode(String pFeedActionCode) {
		mFeedActionCode = pFeedActionCode;
	}

	/**
	 * Gets the feed.
	 * 
	 * @return the feed
	 */
	public String getFeed() {
		return mFeed;
	}

	/**
	 * Sets the feed.
	 * 
	 * @param pFeed
	 *            the feed to set
	 */
	public void setFeed(String pFeed) {
		mFeed = pFeed;
	}

	/**
	 * Gets the style ui ds.
	 * 
	 * @return the styleUIDs
	 */
	public Map<String, String> getStyleUIDs() {
		if (mStyleUIDs == null) {
			mStyleUIDs = new LinkedHashMap<String, String>();
		}
		return mStyleUIDs;
	}

	/**
	 * Sets the style ui ds.
	 * 
	 * @param pStyleUIDs
	 *            the styleUIDs to set
	 */
	public void setStyleUIDs(Map<String, String> pStyleUIDs) {
		mStyleUIDs = pStyleUIDs;
	}

	/**
	 * Gets the style dimensions.
	 * 
	 * @return the styleDimensions
	 */
	public List<String> getStyleDimensions() {
		if (mStyleDimensions == null) {
			mStyleDimensions = new ArrayList<String>();
		}
		return mStyleDimensions;
	}

	/**
	 * Sets the style dimensions.
	 * 
	 * @param pStyleDimensions
	 *            the styleDimensions to set
	 */
	public void setStyleDimensions(List<String> pStyleDimensions) {
		mStyleDimensions = pStyleDimensions;
	}

	/**
	 * Gets the child sku ids.
	 * 
	 * @return the childSKUIds
	 */
	public List<String> getChildSKUIds() {
		if (mChildSKUIds == null) {
			mChildSKUIds = new ArrayList<String>();
		}
		return mChildSKUIds;
	}

	/**
	 * Sets the child sku ids.
	 * 
	 * @param pChildSKUIds
	 *            the childSKUIds to set
	 */
	public void setChildSKUIds(List<String> pChildSKUIds) {
		mChildSKUIds = pChildSKUIds;
	}

	/**
	 * Gets the size chart name.
	 * 
	 * @return the sizeChartName
	 */
	public String getSizeChartName() {
		return mSizeChartName;
	}

	/**
	 * Sets the size chart name.
	 * 
	 * @param pSizeChartName
	 *            the sizeChartName to set
	 */
	public void setSizeChartName(String pSizeChartName) {
		mSizeChartName = pSizeChartName;
	}

	/**
	 * Gets the child SKUs list.
	 * 
	 * @return the childSKUsList
	 */
	public List<TRUSkuFeedVO> getChildSKUsList() {
		if (mChildSKUsList == null) {
			mChildSKUsList = new ArrayList<TRUSkuFeedVO>();
		}
		return mChildSKUsList;
	}

	/**
	 * Sets the child SKUs list.
	 * 
	 * @param pChildSKUsList
	 *            the childSKUsList to set
	 */
	public void setChildSKUsList(List<TRUSkuFeedVO> pChildSKUsList) {
		mChildSKUsList = pChildSKUsList;
	}

	/**
	 * Gets the root parent categories.
	 * 
	 * @return the rootParentCategories
	 */
	public Set<String> getRootParentCategories() {
		if (mRootParentCategories == null) {
			mRootParentCategories = new HashSet<String>();
		}
		return mRootParentCategories;
	}

	/**
	 * Sets the root parent categories.
	 * 
	 * @param pRootParentCategories
	 *            the rootParentCategories to set
	 */
	public void setRootParentCategories(Set<String> pRootParentCategories) {
		mRootParentCategories = pRootParentCategories;
	}

	/**
	 * Gets the parent classifications.
	 * 
	 * @return the parentClassifications
	 */
	public List<String> getParentClassifications() {
		if (mParentClassifications == null) {
			mParentClassifications = new ArrayList<String>();
		}
		return mParentClassifications;
	}

	/**
	 * Sets the parent classifications.
	 * 
	 * @param pParentClassifications
	 *            the parentClassifications to set
	 */
	public void setParentClassifications(List<String> pParentClassifications) {
		mParentClassifications = pParentClassifications;
	}

	/**
	 * Gets the registry classification.
	 * 
	 * @return the registryClassification
	 */
	public String getRegistryClassification() {
		return mRegistryClassification;
	}

	/**
	 * Sets the registry classification.
	 * 
	 * @param pRegistryClassification
	 *            the registryClassification to set
	 */
	public void setRegistryClassification(String pRegistryClassification) {
		mRegistryClassification = pRegistryClassification;
	}

	/**
	 * Gets the vendor classification.
	 * 
	 * @return the vendorClassification
	 */
	public String getVendorClassification() {
		return mVendorClassification;
	}

	/**
	 * Sets the vendor classification.
	 * 
	 * @param pVendorClassification
	 *            the vendorClassification to set
	 */
	public void setVendorClassification(String pVendorClassification) {
		mVendorClassification = pVendorClassification;
	}

	/**
	 * Gets the online long description.
	 * 
	 * @return the onlineLongDesc
	 */
	public String getOnlineLongDesc() {
		return mOnlineLongDesc;
	}

	/**
	 * Sets the online long description.
	 * 
	 * @param pOnlineLongDesc
	 *            the onlineLongDesc to set
	 */
	public void setOnlineLongDesc(String pOnlineLongDesc) {
		mOnlineLongDesc = pOnlineLongDesc;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            the id to set
	 */
	public void setId(String pId) {
		mId = pId;
		setRepositoryId(pId);
	}

	/**
	 * Gets the online title.
	 * 
	 * @return the onlineTitle
	 */
	public String getOnlineTitle() {
		return mOnlineTitle;
	}

	/**
	 * Sets the online title.
	 * 
	 * @param pOnlineTitle
	 *            the onlineTitle to set
	 */
	public void setOnlineTitle(String pOnlineTitle) {
		mOnlineTitle = pOnlineTitle;
	}

	/**
	 * Gets the battery included.
	 * 
	 * @return the batteryIncluded
	 */
	public String getBatteryIncluded() {
		return mBatteryIncluded;
	}

	/**
	 * Sets the battery included.
	 * 
	 * @param pBatteryIncluded
	 *            the batteryIncluded to set
	 */
	public void setBatteryIncluded(String pBatteryIncluded) {
		mBatteryIncluded = pBatteryIncluded;
	}

	/**
	 * Gets the battery required.
	 * 
	 * @return the batteryRequired
	 */
	public String getBatteryRequired() {
		return mBatteryRequired;
	}

	/**
	 * Sets the battery required.
	 * 
	 * @param pBatteryRequired
	 *            the batteryRequired to set
	 */
	public void setBatteryRequired(String pBatteryRequired) {
		mBatteryRequired = pBatteryRequired;
	}

	/**
	 * Gets the gender.
	 * 
	 * @return the gender
	 */
	public String getGender() {
		return mGender;
	}

	/**
	 * Sets the gender.
	 * 
	 * @param pGender
	 *            the gender to set
	 */
	public void setGender(String pGender) {
		mGender = pGender;
	}

	/**
	 * Gets the RUS item number.
	 * 
	 * @return the rusItemNumber
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	/**
	 * Sets the RUS item number.
	 * 
	 * @param pRusItemNumber
	 *            the rusItemNumber to set
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		mRusItemNumber = pRusItemNumber;
	}

	/**
	 * Gets the ship from store eligible.
	 * 
	 * @return the shipFromStoreEligible
	 */
	public String getShipFromStoreEligible() {
		return mShipFromStoreEligible;
	}

	/**
	 * Sets the ship from store eligible.
	 * 
	 * @param pShipFromStoreEligible
	 *            the shipFromStoreEligible to set
	 */
	public void setShipFromStoreEligible(String pShipFromStoreEligible) {
		mShipFromStoreEligible = pShipFromStoreEligible;
	}

	/**
	 * Gets the tax code.
	 * 
	 * @return the taxCode
	 */
	public String getTaxCode() {
		return mTaxCode;
	}

	/**
	 * Sets the tax code.
	 * 
	 * @param pTaxCode
	 *            the taxCode to set
	 */
	public void setTaxCode(String pTaxCode) {
		mTaxCode = pTaxCode;
	}

	/**
	 * Gets the manufacturer style number.
	 * 
	 * @return the manufacturerStyleNumber
	 */
	public String getManufacturerStyleNumber() {
		return mManufacturerStyleNumber;
	}

	/**
	 * Sets the manufacturer style number.
	 * 
	 * @param pManufacturerStyleNumber
	 *            the manufacturerStyleNumber to set
	 */
	public void setManufacturerStyleNumber(String pManufacturerStyleNumber) {
		mManufacturerStyleNumber = pManufacturerStyleNumber;
	}

	/**
	 * Gets the cost.
	 * 
	 * @return the cost
	 */
	public String getCost() {
		return mCost;
	}

	/**
	 * Sets the cost.
	 * 
	 * @param pCost
	 *            the cost to set
	 */
	public void setCost(String pCost) {
		mCost = pCost;
	}

	/**
	 * Gets the ewaste surcharge sku.
	 * 
	 * @return the ewasteSurchargeSku
	 */
	public String getEwasteSurchargeSku() {
		return mEwasteSurchargeSku;
	}

	/**
	 * Sets the ewaste surcharge sku.
	 * 
	 * @param pEwasteSurchargeSku
	 *            the ewasteSurchargeSku to set
	 */
	public void setEwasteSurchargeSku(String pEwasteSurchargeSku) {
		mEwasteSurchargeSku = pEwasteSurchargeSku;
	}

	/**
	 * Gets the ewaste surcharge state.
	 * 
	 * @return the ewasteSurchargeState
	 */
	public String getEwasteSurchargeState() {
		return mEwasteSurchargeState;
	}

	/**
	 * Sets the ewaste surcharge state.
	 * 
	 * @param pEwasteSurchargeState
	 *            the ewasteSurchargeState to set
	 */
	public void setEwasteSurchargeState(String pEwasteSurchargeState) {
		mEwasteSurchargeState = pEwasteSurchargeState;
	}

	/**
	 * Gets the special assembly instructions.
	 * 
	 * @return the specialAssemblyInstructions
	 */
	public String getSpecialAssemblyInstructions() {
		return mSpecialAssemblyInstructions;
	}

	/**
	 * Sets the special assembly instructions.
	 * 
	 * @param pSpecialAssemblyInstructions
	 *            the specialAssemblyInstructions to set
	 */
	public void setSpecialAssemblyInstructions(String pSpecialAssemblyInstructions) {
		mSpecialAssemblyInstructions = pSpecialAssemblyInstructions;
	}

	/**
	 * Gets the tax product value code.
	 * 
	 * @return the taxProductValueCode
	 */
	public String getTaxProductValueCode() {
		return mTaxProductValueCode;
	}

	/**
	 * Sets the tax product value code.
	 * 
	 * @param pTaxProductValueCode
	 *            the taxProductValueCode to set
	 */
	public void setTaxProductValueCode(String pTaxProductValueCode) {
		mTaxProductValueCode = pTaxProductValueCode;
	}

	/**
	 * Gets the vendors product demo url.
	 * 
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * Sets the vendors product demo url.
	 * 
	 * @param pVendorsProductDemoUrl
	 *            the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}

	/**
	 * Gets the non merchandise flag.
	 * 
	 * @return the nonMerchandiseFlag
	 */
	public String getNonMerchandiseFlag() {
		return mNonMerchandiseFlag;
	}

	/**
	 * Sets the non merchandise flag.
	 * 
	 * @param pNonMerchandiseFlag
	 *            the nonMerchandiseFlag to set
	 */
	public void setNonMerchandiseFlag(String pNonMerchandiseFlag) {
		mNonMerchandiseFlag = pNonMerchandiseFlag;
	}

	/**
	 * Gets the country eligibility.
	 * 
	 * @return the countryEligibility
	 */
	public String getCountryEligibility() {
		return mCountryEligibility;
	}

	/**
	 * Sets the country eligibility.
	 * 
	 * @param pCountryEligibility
	 *            the countryEligibility to set
	 */
	public void setCountryEligibility(String pCountryEligibility) {
		mCountryEligibility = pCountryEligibility;
	}
	
	/**
	 * Gets the  TRUClassificationIdAndRootCategoryMap.
	 * 
	 * @return the  TRUClassificationIdAndRootCategoryMap
	 */
	public Map<String, Set<String>> getClassificationIdAndRootCategoryMap() {
		if (mClassificationIdAndRootCategoryMap == null) {
			mClassificationIdAndRootCategoryMap = new HashMap<String,Set<String>>();
		}
		return mClassificationIdAndRootCategoryMap;
	}

	/**
	 * Sets the classification id and root category map.
	 *
	 * @param pClassificationIdAndRootCategoryMap the classification id and root category map
	 */
	public void setClassificationIdAndRootCategoryMap(
			Map<String, Set<String>> pClassificationIdAndRootCategoryMap) {
		this.mClassificationIdAndRootCategoryMap = pClassificationIdAndRootCategoryMap;
	}

	/**
	 * Gets the sequence number.
	 * 
	 * @return the sequenceNumber
	 */
	public String getSequenceNumber() {
		return mSequenceNumber;
	}

	/**
	 * Sets the sequence number.
	 * 
	 * @param pSequenceNumber
	 *            the sequenceNumber to set
	 */
	public void setSequenceNumber(String pSequenceNumber) {
		mSequenceNumber = pSequenceNumber;
	}
	
	/**
	 * Gets the repository id.
	 *
	 * @return string
	 */
	@Override
	public String getRepositoryId() {
		return mId;
	}

}
