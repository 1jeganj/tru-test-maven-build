package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Class TRUCatalog. This class holds the Catalog related properties.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUCatalog {

	/** property to hold TRUCategory List. */
	private List<TRUCategory> mTRUCategory;

	/** property to hold TRUClassification List. */
	private List<TRUClassification> mTRUClassification;
	/** property to hold TRUClassification List. */
	private List<TRUClassification> mTRURegistryClassification;
	/** property to hold TRUProductFeedVO List. */
	private List<TRUProductFeedVO> mTRUProductFeedVOList;

	/** property to hold TRUStrollerSKUVO List. */
	private List<TRUStrollerSKUVO> mTRUStrollerSKUList;

	/** property to hold TRUCribSKUVO List. */
	private List<TRUCribSKUVO> mTRUCribSKUVOList;

	/** property to hold TRUCarSeatSKUVO List. */
	private List<TRUCarSeatSKUVO> mTRUCarSeatSKUVOList;

	/** property to hold TRUBookCdDvDSKUVO List. */
	private List<TRUBookCdDvDSKUVO> mTRUBookCdDvDSKUVOList;

	/** property to hold TRUVideoGameSKUVO List. */
	private List<TRUVideoGameSKUVO> mTRUVideoGameSKUVOList;

	/** property to hold TRUSKUFeedVO List. */
	private List<TRUSkuFeedVO> mTRUSKUFeedVOList;

	/** property to hold TRUNonMerchSKUVO List. */
	private List<TRUNonMerchSKUVO> mTRUNonMerchSKUVOList;
	
	/** property to hold ClassificationIdAndRootCategoryMap List. */
	private Map<String,Set<String>> mClassificationIdAndRootCategoryMap;

	/** property to hold ChildProducts. */
	private Map<String,TRUCollectionProductVO> mCollectionProdcutsMap ;
	
	/** property to hold ClassificationIdAndRootCategoryMap List. */
	private List<TRUCollectionProductVO> mCollectionProductVO;

	/**
	 * Instantiates a new TRU catalog.
	 */
	public TRUCatalog() {
		setTRUCategory(new ArrayList<TRUCategory>());
		setTRUClassification(new ArrayList<TRUClassification>());
		setTRURegistryClassification(new ArrayList<TRUClassification>());
	}


	/**
	 * Gets the  TRUClassificationIdAndRootCategoryMap.
	 * 
	 * @return the  TRUClassificationIdAndRootCategoryMap
	 */
	public Map<String, Set<String>> getClassificationIdAndRootCategoryMap() {
		if (mClassificationIdAndRootCategoryMap == null) {
			mClassificationIdAndRootCategoryMap = new HashMap<String,Set<String>>();
		}
		return mClassificationIdAndRootCategoryMap;
	}

	/**
	 * Gets the collection prodcuts map.
	 *
	 * @return the collection prodcuts map
	 */
	public Map<String, TRUCollectionProductVO> getCollectionProdcutsMap() {
		if(mCollectionProdcutsMap==null){
			mCollectionProdcutsMap=new HashMap<String,TRUCollectionProductVO>();
		}
		return mCollectionProdcutsMap;
	}

	/**
	 * Gets the  TRUCollectionProductVO.
	 * 
	 * @return the  mCollectionProductVO
	 */
	public List<TRUCollectionProductVO> getCollectionProductVO() {
		if(mCollectionProductVO==null){
			mCollectionProductVO=new ArrayList<TRUCollectionProductVO>();
		}
		return mCollectionProductVO;
	}
	/**
	 * Gets the TRU book cd dv dskuvo list.
	 * 
	 * @return the tRUBookCdDvDSKUVOList
	 */
	public List<TRUBookCdDvDSKUVO> getTRUBookCdDvDSKUVOList() {
		if (mTRUBookCdDvDSKUVOList == null) {
			mTRUBookCdDvDSKUVOList = new ArrayList<TRUBookCdDvDSKUVO>();
		}
		return mTRUBookCdDvDSKUVOList;
	}

	/**
	 * Gets the TRU car seat skuvo list.
	 * 
	 * @return the tRUCarSeatSKUVOList
	 */
	public List<TRUCarSeatSKUVO> getTRUCarSeatSKUVOList() {
		if (mTRUCarSeatSKUVOList == null) {
			mTRUCarSeatSKUVOList = new ArrayList<TRUCarSeatSKUVO>();
		}
		return mTRUCarSeatSKUVOList;
	}

	/**
	 * Gets the TRU category.
	 * 
	 * @return the TRU category
	 */
	public List<TRUCategory> getTRUCategory() {
		return mTRUCategory;
	}

	/**
	 * Gets the TRU classification.
	 * 
	 * @return the TRU classification
	 */
	public List<TRUClassification> getTRUClassification() {
		return mTRUClassification;
	}

	/**
	 * Gets the TRU crib skuvo list.
	 * 
	 * @return the tRUCribSKUVOList
	 */
	public List<TRUCribSKUVO> getTRUCribSKUVOList() {
		if (mTRUCribSKUVOList == null) {
			mTRUCribSKUVOList = new ArrayList<TRUCribSKUVO>();
		}
		return mTRUCribSKUVOList;
	}

	/**
	 * Gets the TRU non merch skuvo list.
	 * 
	 * @return the tRUNonMerchSKUVOList
	 */
	public List<TRUNonMerchSKUVO> getTRUNonMerchSKUVOList() {
		if (mTRUNonMerchSKUVOList == null) {
			mTRUNonMerchSKUVOList = new ArrayList<TRUNonMerchSKUVO>();
		}
		return mTRUNonMerchSKUVOList;
	}

	/**
	 * Gets the TRU product feed vo list.
	 * 
	 * @return the tRUProductFeedVOList
	 */
	public List<TRUProductFeedVO> getTRUProductFeedVOList() {
		if (mTRUProductFeedVOList == null) {
			mTRUProductFeedVOList = new ArrayList<TRUProductFeedVO>();
		}
		return mTRUProductFeedVOList;
	}

	/**
	 * Gets the TRU classification.
	 * 
	 * @return the TRU classification
	 */
	public List<TRUClassification> getTRURegistryClassification() {
		return mTRURegistryClassification;
	}

	/**
	 * Gets the TRUSKU feed vo list.
	 * 
	 * @return the tRUSKUFeedVOList
	 */
	public List<TRUSkuFeedVO> getTRUSKUFeedVOList() {

		if (mTRUSKUFeedVOList == null) {
			mTRUSKUFeedVOList = new ArrayList<TRUSkuFeedVO>();
		}
		return mTRUSKUFeedVOList;
	}

	/**
	 * Gets the TRU stroller sku list.
	 * 
	 * @return the tRUStrollerSKUList
	 */
	public List<TRUStrollerSKUVO> getTRUStrollerSKUList() {
		if (mTRUStrollerSKUList == null) {
			mTRUStrollerSKUList = new ArrayList<TRUStrollerSKUVO>();
		}
		return mTRUStrollerSKUList;
	}

	/**
	 * Gets the TRU video game skuvo list.
	 * 
	 * @return the tRUVideoGameSKUVOList
	 */
	public List<TRUVideoGameSKUVO> getTRUVideoGameSKUVOList() {
		if (mTRUVideoGameSKUVOList == null) {
			mTRUVideoGameSKUVOList = new ArrayList<TRUVideoGameSKUVO>();
		}
		return mTRUVideoGameSKUVOList;
	}

	/**
	 * Sets the classification id and root category map.
	 *
	 * @param pClassificationIdAndRootCategoryMap the classification id and root category map
	 */
	public void setClassificationIdAndRootCategoryMap(
			Map<String, Set<String>> pClassificationIdAndRootCategoryMap) {
		this.mClassificationIdAndRootCategoryMap = pClassificationIdAndRootCategoryMap;
	}

	/**
	 * Sets the collection prodcuts map.
	 *
	 * @param pCollectionProdcutsMap the collection prodcuts map
	 */
	public void setCollectionProdcutsMap(
			Map<String, TRUCollectionProductVO> pCollectionProdcutsMap) {
		this.mCollectionProdcutsMap = pCollectionProdcutsMap;
	}

	/**
	 * Sets the mCollectionProductVO.
	 *
	 * @param pCollectionProductVO the collection of vo
	 */
	public void setCollectionProductVO(
			List<TRUCollectionProductVO> pCollectionProductVO) {
		this.mCollectionProductVO = pCollectionProductVO;
	}

	/**
	 * Sets the TRU book cd dv dskuvo list.
	 * 
	 * @param pTRUBookCdDvDSKUVOList
	 *            the tRUBookCdDvDSKUVOList to set
	 */
	public void setTRUBookCdDvDSKUVOList(List<TRUBookCdDvDSKUVO> pTRUBookCdDvDSKUVOList) {
		mTRUBookCdDvDSKUVOList = pTRUBookCdDvDSKUVOList;
	}

	/**
	 * Sets the TRU car seat skuvo list.
	 * 
	 * @param pTRUCarSeatSKUVOList
	 *            the tRUCarSeatSKUVOList to set
	 */
	public void setTRUCarSeatSKUVOList(List<TRUCarSeatSKUVO> pTRUCarSeatSKUVOList) {
		mTRUCarSeatSKUVOList = pTRUCarSeatSKUVOList;
	}

	/**
	 * Sets the TRU category.
	 * 
	 * @param pTRUCategory
	 *            the new TRU category
	 */
	public void setTRUCategory(List<TRUCategory> pTRUCategory) {
		this.mTRUCategory = pTRUCategory;
	}

	/**
	 * Sets the TRU classification.
	 * 
	 * @param pTRUClassification
	 *            the new TRU classification
	 */
	public void setTRUClassification(List<TRUClassification> pTRUClassification) {
		this.mTRUClassification = pTRUClassification;
	}

	/**
	 * Sets the TRU crib skuvo list.
	 * 
	 * @param pTRUCribSKUVOList
	 *            the tRUCribSKUVOList to set
	 */
	public void setTRUCribSKUVOList(List<TRUCribSKUVO> pTRUCribSKUVOList) {
		mTRUCribSKUVOList = pTRUCribSKUVOList;
	}

	/**
	 * Sets the TRU non merch skuvo list.
	 * 
	 * @param pTRUNonMerchSKUVOList
	 *            the tRUNonMerchSKUVOList to set
	 */
	public void setTRUNonMerchSKUVOList(List<TRUNonMerchSKUVO> pTRUNonMerchSKUVOList) {
		mTRUNonMerchSKUVOList = pTRUNonMerchSKUVOList;
	}

	/**
	 * Sets the TRU product feed vo list.
	 * 
	 * @param pTRUProductFeedVOList
	 *            the tRUProductFeedVOList to set
	 */
	public void setTRUProductFeedVOList(List<TRUProductFeedVO> pTRUProductFeedVOList) {
		mTRUProductFeedVOList = pTRUProductFeedVOList;
	}

	/**
	 * Sets the TRU classification.
	 * 
	 * @param pTRURegistryClassification
	 *            the new TRU registry classification
	 */
	public void setTRURegistryClassification(List<TRUClassification> pTRURegistryClassification) {
		this.mTRURegistryClassification = pTRURegistryClassification;
	}

	/**
	 * Sets the TRU stroller sku list.
	 * 
	 * @param pTRUStrollerSKUList
	 *            the tRUStrollerSKUList to set
	 */
	public void setTRUStrollerSKUList(List<TRUStrollerSKUVO> pTRUStrollerSKUList) {
		mTRUStrollerSKUList = pTRUStrollerSKUList;
	}

	/**
	 * Sets the TRU video game skuvo list.
	 * 
	 * @param pTRUVideoGameSKUVOList
	 *            the tRUVideoGameSKUVOList to set
	 */
	public void setTRUVideoGameSKUVOList(List<TRUVideoGameSKUVO> pTRUVideoGameSKUVOList) {
		mTRUVideoGameSKUVOList = pTRUVideoGameSKUVOList;
	}

}
