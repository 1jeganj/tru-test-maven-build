package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class TRUCollectionProductItemValueChangedInspector. This class inspects each and every property holds in entites to repository
 * of collection Product relations ships properties has any changes. If any change occurs in one property entire entity will be updated.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCollectionProductItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the Collection Product properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pRepoItem
	 *            the repo item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pFeedVO, RepositoryItem pRepoItem) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCollectionProductItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pFeedVO instanceof TRUCollectionProductVO) {
			TRUCollectionProductVO collectionProduct = (TRUCollectionProductVO) pFeedVO;
			

			if (!StringUtils.isBlank(collectionProduct.getOnlineTitle()) &&
					 !collectionProduct.getOnlineTitle().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(collectionProduct.getOnlineLongDesc()) &&
					 !collectionProduct.getOnlineLongDesc().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getLongDescriptionName()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(collectionProduct.getCollectionImage()) &&
					 !collectionProduct.getCollectionImage().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getCollectionImage()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(collectionProduct.getDisplayStatus()) &&
					 !collectionProduct.getDisplayStatus().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getDisplayStatusPropertyName()))) {
				return retVal = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUCollectionProductItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
