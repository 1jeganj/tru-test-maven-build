package com.tru.feedprocessor.tol.catalog.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUProductRelationshipItemValueChangedInspector. This class inspects each and every property holds in entites to repository
 * of Product relations ships properties has any changes. If any change occurs in one property entire entity will be updated.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUProductRelationshipItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the Product properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pRepoItem
	 *            the repo item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pFeedVO, RepositoryItem pRepoItem) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pFeedVO instanceof TRUProductFeedVO) {
			TRUProductFeedVO product = (TRUProductFeedVO) pFeedVO;
			
			if (!StringUtils.isBlank(product.getFeed())
					&& product.getFeed().equalsIgnoreCase(
							TRUProductFeedConstants.DELETE)
					&& !StringUtils.isBlank(product.getFeedActionCode())
					&& product.getFeedActionCode().equalsIgnoreCase(
							TRUProductFeedConstants.D)) {
				return Boolean.TRUE;
			}
			
			if(StringUtils.isBlank(product.getErrorMessage()) && product.getStyleUIDs().isEmpty() && product.getStyleDimensions().isEmpty()){
				if(!product.getChildSKUIds().isEmpty() && checkChildSkus(product.getChildSKUIds(), product, pRepoItem)){
					retVal = Boolean.TRUE;
				}
				else if(checkParentClassifications(product.getParentClassifications(), pRepoItem)){
					retVal = Boolean.TRUE;
				}
				else if(checkRootParentCategories(product.getRootParentCategories(), pRepoItem)){
					retVal = Boolean.TRUE;
				}
				else if(!product.getChildSKUsList().isEmpty() && checkForSKUDelete(product)){
					retVal = Boolean.TRUE;
				}
				else if((!StringUtils.isBlank(product.getDivisionCode()) || !StringUtils.isBlank(product.getDepartmentCode()) || !StringUtils.isBlank(product.getClassCode()) || !StringUtils.isBlank(product.getSubclassCode())) && checkMerchandiseFlags(product, pRepoItem)){
					retVal = Boolean.TRUE;
				}
			} else if(StringUtils.isBlank(product.getErrorMessage()) && !product.getStyleUIDs().isEmpty()){
				List<String> dBStyleDimensions = (List<String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getStyleDimensions());
				if(!product.getStyleDimensions().equals(dBStyleDimensions)){
					retVal = Boolean.TRUE;
				}
				if(!product.getStyleUIDs().isEmpty()){
					Set<String> skuIds = product.getStyleUIDs().keySet();
					List<String> listSkuIds = new ArrayList<String>();
					listSkuIds.addAll(skuIds);
					if(checkChildSkus(listSkuIds, product, pRepoItem)){
						retVal = Boolean.TRUE;
					}
				}
			}
			
			List<TRUSkuFeedVO> childSKUsList = product.getChildSKUsList();
			for (TRUSkuFeedVO skuFeedVO : childSKUsList) {
				if(skuFeedVO.getUpdatedListProps().contains(TRUProductFeedConstants.PARENT_CLASSIFICATIONS)){
					return Boolean.TRUE;
				}
			}
		}

		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}
	
	/**
	 * Check for sku delete.
	 *
	 * @param pVOItem the VO item
	 * @return the boolean
	 */
	private Boolean checkForSKUDelete(TRUProductFeedVO pVOItem){
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkForSKUDelete()");
		
		Boolean retVal = Boolean.FALSE;
		
		List<TRUSkuFeedVO> skuFeedVOList = pVOItem.getChildSKUsList();
		if(!skuFeedVOList.isEmpty()){
			for(TRUSkuFeedVO skuFeedVO: skuFeedVOList){
				if(!StringUtils.isBlank(skuFeedVO.getFeedActionCode()) && skuFeedVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.D)){
					retVal = Boolean.TRUE;
					break;
				}
			}
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkForSKUDelete()");
		
		return retVal;
	}
	
	/**
	 * Check child skus.
	 *
	 * @param pToCompareWithDBList the to compare with db list
	 * @param pProductVO the product vo
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	private Boolean checkChildSkus(List<String> pToCompareWithDBList, TRUProductFeedVO pProductVO, RepositoryItem pRepoItem){
		
		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkChildSkus()");
		
		Boolean retVal = Boolean.FALSE;
		List<RepositoryItem> RepositoryProductChildSKUs = (List<RepositoryItem>) pRepoItem
				.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
		List<String> dbChildSkus = new ArrayList<String>();
		if (null != RepositoryProductChildSKUs && !RepositoryProductChildSKUs.isEmpty()) {
			for (RepositoryItem ProductChildSKUsItem : RepositoryProductChildSKUs) {
				String skuId = (String) ProductChildSKUsItem.getPropertyValue(getFeedCatalogProperty().getId());
				dbChildSkus.add(skuId);
			}
			for(String skuId: pToCompareWithDBList){
				if (!dbChildSkus.contains(skuId)) {
					retVal = Boolean.TRUE;
					break;
				}else if(pProductVO.getStyleUIDs().get(skuId) != null && pProductVO.getStyleUIDs().get(skuId).equalsIgnoreCase(TRUProductFeedConstants.D)){
					retVal = Boolean.TRUE;
					break;
				}
			}
		} else if(!pToCompareWithDBList.isEmpty() && (null != RepositoryProductChildSKUs && RepositoryProductChildSKUs.isEmpty())){
			retVal = Boolean.TRUE;
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkChildSkus()");
		
		return retVal;
	}
	
	
	/**
	 * Check merchandise flags.
	 *
	 * @param pProductVO the product vo
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	private Boolean checkMerchandiseFlags(TRUProductFeedVO pProductVO, RepositoryItem pRepoItem){
		
		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkMerchandiseFlags()");
		
		Boolean retVal = Boolean.FALSE;
		List<RepositoryItem> RepositoryProductChildSKUs = (List<RepositoryItem>) pRepoItem
				.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
		String voDivsionCode = pProductVO.getDivisionCode();
		String voDepartmentCode = pProductVO.getDepartmentCode();
		String voClassCode = pProductVO.getClassCode();
		String voSubClassCode = pProductVO.getSubclassCode();
		if (null != RepositoryProductChildSKUs && !RepositoryProductChildSKUs.isEmpty()) {
			for (RepositoryItem ProductChildSKUsItem : RepositoryProductChildSKUs) {
				String divsionCode = (String) ProductChildSKUsItem.getPropertyValue(getFeedCatalogProperty().getDivisionCode());
				String departmentCode = (String) ProductChildSKUsItem.getPropertyValue(getFeedCatalogProperty().getDepartmentCode());
				String classCode = (String) ProductChildSKUsItem.getPropertyValue(getFeedCatalogProperty().getClassCode());
				String subClassCode = (String) ProductChildSKUsItem.getPropertyValue(getFeedCatalogProperty().getSubclassCode());
				if((!StringUtils.isBlank(voDivsionCode) && StringUtils.isBlank(divsionCode)) || (!StringUtils.isBlank(voDivsionCode) && !StringUtils.isBlank(divsionCode) && !voDivsionCode.equalsIgnoreCase(divsionCode))){
					retVal = Boolean.TRUE;
					break;
				}
				if((!StringUtils.isBlank(voDepartmentCode) && StringUtils.isBlank(departmentCode)) || (!StringUtils.isBlank(voDepartmentCode) && !StringUtils.isBlank(departmentCode) && !voDepartmentCode.equalsIgnoreCase(departmentCode))){
					retVal = Boolean.TRUE;
					break;
				}
				if((!StringUtils.isBlank(voClassCode) && StringUtils.isBlank(classCode)) || (!StringUtils.isBlank(voClassCode) && !StringUtils.isBlank(classCode) && !voClassCode.equalsIgnoreCase(classCode))){
					retVal = Boolean.TRUE;
					break;
				}
				if((!StringUtils.isBlank(voSubClassCode) && StringUtils.isBlank(subClassCode)) || (!StringUtils.isBlank(voSubClassCode) && !StringUtils.isBlank(subClassCode) && !voSubClassCode.equalsIgnoreCase(subClassCode))){
					retVal = Boolean.TRUE;
					break;
				}
			}
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkMerchandiseFlags()");
		
		return retVal;
	}
	
	/**
	 * Check parent classifications.
	 *
	 * @param pToCompareWithDBList the to compare with db list
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	private Boolean checkParentClassifications(List<String> pToCompareWithDBList, RepositoryItem pRepoItem){
		
		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkParentClassifications()");
		
		Boolean retVal = Boolean.FALSE;
		List<String> dBParentClassificaiton = (List<String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentClassifications());
		if (!dBParentClassificaiton.containsAll(pToCompareWithDBList)
				|| (dBParentClassificaiton != null && pToCompareWithDBList != null && dBParentClassificaiton.size() != pToCompareWithDBList.size())) {
			retVal = Boolean.TRUE;
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkParentClassifications()");
		
		return retVal;
	}
	
	/**
	 * Check root parent categories.
	 *
	 * @param pToCompareWithDBSet the to compare with db set
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	private Boolean checkRootParentCategories(Set<String> pToCompareWithDBSet, RepositoryItem pRepoItem){
		
		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkRootParentCategories()");
		
		Boolean retVal = Boolean.FALSE;
		Set<RepositoryItem> parentCats = (Set<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentCategories());
		Set<String> parentCatIDs = new HashSet<String>();
		for (RepositoryItem repositoryItem : parentCats) {
			parentCatIDs.add(repositoryItem.getRepositoryId());
		}
		if (!parentCatIDs.containsAll(pToCompareWithDBSet)
				|| (parentCatIDs != null && pToCompareWithDBSet != null && parentCatIDs.size() != pToCompareWithDBSet.size())) {
			retVal = Boolean.TRUE;
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkRootParentCategories()");
		
		return retVal;
	}


	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
