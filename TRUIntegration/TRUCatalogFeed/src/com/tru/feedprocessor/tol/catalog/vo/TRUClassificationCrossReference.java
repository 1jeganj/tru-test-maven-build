package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.List;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUClassificationCrossReference.
 * It will hold all the properties required for the Classification Cross Reference
 * @author Professional Access
 * @version 1.0
 */
public class TRUClassificationCrossReference extends BaseFeedProcessVO {

	/** Property to hold  id. */
	private String mId;

	/** Property to hold  long description. */
	private String mLongDescription;

	/** Property to hold  display status. */
	private String mDisplayStatus;

	/** Property to hold  display order. */
	private String mDisplayOrder;

	/** Property to hold  parent category. */
	private List<String> mParentCategory = new ArrayList<String>();

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            the new id
	 */
	public void setId(String pId) {
		this.mId = pId;
	}

	/**
	 * Gets the long description.
	 * 
	 * @return the long description
	 */
	public String getLongDescription() {
		return mLongDescription;
	}

	/**
	 * Sets the long description.
	 * 
	 * @param pLongDescription
	 *            the new long description
	 */
	public void setLongDescription(String pLongDescription) {
		this.mLongDescription = pLongDescription;
	}

	/**
	 * Gets the display status.
	 * 
	 * @return the display status
	 */
	public String getDisplayStatus() {
		return mDisplayStatus;
	}

	/**
	 * Sets the display status.
	 * 
	 * @param pDisplayStatus
	 *            the new display status
	 */
	public void setDisplayStatus(String pDisplayStatus) {
		this.mDisplayStatus = pDisplayStatus;
	}

	/**
	 * Gets the display order.
	 * 
	 * @return the display order
	 */
	public String getDisplayOrder() {
		return mDisplayOrder;
	}

	/**
	 * Sets the display order.
	 * 
	 * @param pDisplayOrder
	 *            the new display order
	 */
	public void setDisplayOrder(String pDisplayOrder) {
		this.mDisplayOrder = pDisplayOrder;
	}

	/**
	 * Gets the parent category.
	 * 
	 * @return the parent category
	 */
	public List<String> getParentCategory() {

		return mParentCategory;
	}

	/**
	 * Sets the parent category.
	 * 
	 * @param pParentCategory
	 *            the new parent category
	 */
	public void setParentCategory(List<String> pParentCategory) {
		this.mParentCategory = pParentCategory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO#getRepositoryId()
	 */
	@Override
	public String getRepositoryId() {
		return mId;
	}
}
