package com.tru.feedprocessor.tol.catalog.vo;

/**
 * The Class TRUCrossSellsnUidBatteriesVO. VO class which will store Cross Sell Batteries properties.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCrossSellsnUidBatteriesVO {

	/**
	 * The variable to hold "CrossSellsBatteries" property name.
	 */
	private String mCrossSellsBatteries;

	/**
	 * The variable to hold "UidBatteryCrossSell" property name.
	 */
	private String mUidBatteryCrossSell;

	/**
	 * Gets the cross sells batteries.
	 * 
	 * @return the crossSellsBatteries
	 */
	public String getCrossSellsBatteries() {
		return mCrossSellsBatteries;
	}

	/**
	 * Sets the cross sells batteries.
	 * 
	 * @param pCrossSellsBatteries
	 *            the crossSellsBatteries to set
	 */
	public void setCrossSellsBatteries(String pCrossSellsBatteries) {
		mCrossSellsBatteries = pCrossSellsBatteries;
	}

	/**
	 * Gets the uid battery cross sell.
	 * 
	 * @return the uidBatteryCrossSell
	 */
	public String getUidBatteryCrossSell() {
		return mUidBatteryCrossSell;
	}

	/**
	 * Sets the uid battery cross sell.
	 * 
	 * @param pUidBatteryCrossSell
	 *            the uidBatteryCrossSell to set
	 */
	public void setUidBatteryCrossSell(String pUidBatteryCrossSell) {
		mUidBatteryCrossSell = pUidBatteryCrossSell;
	}

}
