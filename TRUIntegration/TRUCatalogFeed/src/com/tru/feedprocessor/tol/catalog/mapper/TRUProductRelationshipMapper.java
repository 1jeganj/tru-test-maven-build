package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUDeltaRepositoryToProductVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUStyleErrorVO;

/**
 * The Class TRUProductRelationshipMapper. This class maps the relationships data stored in entities to repository to push
 * the data of properties and it is used to write the VO properties to the ProductCatalog repository
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUProductRelationshipMapper extends AbstractFeedItemMapper<TRUProductFeedVO> {

	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/** property to hold m delta Repository To Product VO holder. */
	private TRUDeltaRepositoryToProductVO mDeltaRepositoryToProductVO;

	/**
	 * Gets the delta repository to product vo.
	 * 
	 * @return the deltaRepositoryToProductVO
	 */
	public TRUDeltaRepositoryToProductVO getDeltaRepositoryToProductVO() {
		return mDeltaRepositoryToProductVO;
	}

	/**
	 * Sets the delta repository to product vo.
	 * 
	 * @param pDeltaRepositoryToProductVO
	 *            the deltaRepositoryToProductVO to set
	 */
	public void setDeltaRepositoryToProductVO(TRUDeltaRepositoryToProductVO pDeltaRepositoryToProductVO) {
		mDeltaRepositoryToProductVO = pDeltaRepositoryToProductVO;
	}
	

	/**
	 * Setting common Product Relationship properties it is used to write the VO properties to the ProductCatalog repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUProductFeedVO pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUProductRelationshipMapper, @method: map()" + pVOItem.getId());
		String method = TRUProductFeedConstants.PRODUCT_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.PRMCOUNT);
		count++;
		getLogger().vlogDebug("ProdRelMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.PRMCOUNT,count);

		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);
		
		if (!StringUtils.isBlank(pVOItem.getFeed())
				&& pVOItem.getFeed().equalsIgnoreCase(
						TRUProductFeedConstants.DELETE)
				&& !StringUtils.isBlank(pVOItem.getFeedActionCode())
				&& pVOItem.getFeedActionCode().equalsIgnoreCase(
						TRUProductFeedConstants.D)) {
			
			deleteProductRelationshipProperties(pVOItem, pRepoItem);
			
			return pRepoItem;
		}

		String feed = pVOItem.getFeed();
		if (!StringUtils.isBlank(feed) && feed.equalsIgnoreCase(TRUProductFeedConstants.DELTA)
				&& pVOItem.getStyleUIDs() != null && !pVOItem.getStyleUIDs().isEmpty()) {
			List<TRUStyleErrorVO> errorList = (List<TRUStyleErrorVO>) getConfigValue(TRUProductFeedConstants.STYLE_ERROR);
			getDeltaRepositoryToProductVO().repositoryToProductVO(pVOItem, pRepoItem, errorList);
		}

		if (feed.equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
			List<RepositoryItem> repositoryProdChildSKUs = (List<RepositoryItem>) pRepoItem
					.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
			if (null != repositoryProdChildSKUs && !repositoryProdChildSKUs.isEmpty()) {
				for (RepositoryItem prodItemChildSKU : repositoryProdChildSKUs) {
					if (!pVOItem.getChildSKUIds().contains(
							(String) prodItemChildSKU.getPropertyValue(getFeedCatalogProperty().getId()))) {
						pVOItem.getChildSKUIds().add(
								(String) prodItemChildSKU.getPropertyValue(getFeedCatalogProperty().getId()));
					}
				}
			}
		}
		if (!pVOItem.getChildSKUIds().isEmpty()) {
			String[] childSKUsArray = pVOItem.getChildSKUIds().toArray(new String[pVOItem.getChildSKUIds().size()]);
			RepositoryItem[] skuRepositoryItems = productCatalogRepository.getItems(childSKUsArray,
					TRUProductFeedConstants.SKU);
			List<RepositoryItem> newChildSkusList = new ArrayList<RepositoryItem>(); 
			newChildSkusList.addAll(Arrays.asList(skuRepositoryItems));
			List<RepositoryItem> removeList = new ArrayList<RepositoryItem>();
			for (int i = 0; i < skuRepositoryItems.length; i++) {
				Set<RepositoryItem> parentProds = (Set<RepositoryItem>) skuRepositoryItems[i].getPropertyValue(getFeedCatalogProperty().getParentProducts());
				for (RepositoryItem parentProd : parentProds) {
					List<String> styleDims = (List<String>) parentProd.getPropertyValue(getFeedCatalogProperty().getStyleDimensions());
					if(styleDims != null && styleDims.size() > TRUFeedConstants.NUMBER_ZERO){
						removeList.add(skuRepositoryItems[i]);
					}
				}
			}
			for (RepositoryItem removeSku : removeList) {
				newChildSkusList.remove(removeSku);
			}
			if (null != skuRepositoryItems && skuRepositoryItems.length > TRUProductFeedConstants.NUMBER_ZERO) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(), newChildSkusList);

			}
			
			Set<RepositoryItem> parentCats = (Set<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentCategories());
			List<String> parentCatIDs = new ArrayList<String>();
			for (RepositoryItem repositoryItem : parentCats) {
				parentCatIDs.add(repositoryItem.getRepositoryId());
			}
			
			if (!StringUtils.isBlank(pVOItem.getType()) && !pVOItem.getType().equalsIgnoreCase(TRUProductFeedConstants.NONMERCHSKU)) {
				Set<String> parentClassifications = new HashSet<String>();
				for (RepositoryItem skuitem : newChildSkusList) {
					List<String> repoSkuParentClfs = (List<String>) skuitem
							.getPropertyValue(getFeedCatalogProperty().getParentClassifications());
					if (null != repoSkuParentClfs && !repoSkuParentClfs.isEmpty()) {
						parentClassifications.addAll(repoSkuParentClfs);
					}
				}
				List<String> newClfs = new ArrayList<String>();
				newClfs.addAll(parentClassifications);
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(), newClfs);
				
				Map<String, Set<String>> rootCategoryMap = pVOItem.getClassificationIdAndRootCategoryMap();

				Set<RepositoryItem> rootParentCategories = new HashSet<RepositoryItem>();
				Set<String> rootCategoriesSet = new HashSet<String>();
				if (!StringUtils.isBlank(feed) && feed.equalsIgnoreCase(TRUProductFeedConstants.DELTA) && pVOItem.getStyleUIDs() != null
						&& !pVOItem.getStyleUIDs().isEmpty()) {
					for (String parentClassification : parentClassifications) {
						Set<String> rootCat = rootCategoryMap.get(parentClassification);
						if (rootCat != null) {
							rootCategoriesSet.addAll(rootCat);
						}
					}
				} else {
					rootCategoriesSet = pVOItem.getRootParentCategories();
				}
				
				for (String rootParentCategory : rootCategoriesSet) {
					if (!StringUtils.isBlank(rootParentCategory)) {
						RepositoryItem catItem = pRepoItem.getRepository().getItem(rootParentCategory, TRUProductFeedConstants.CATEGORY);
						if (catItem != null) {
							rootParentCategories.add(catItem);
						}
					}
				}
				if (rootParentCategories != null && !rootParentCategories.isEmpty()) {
					pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentCategories(), rootParentCategories);
				}
			} else if(!StringUtils.isBlank(pVOItem.getType()) && pVOItem.getType().equalsIgnoreCase(TRUProductFeedConstants.NONMERCHSKU)){
				
				List<TRUCategory> truCategoryList = (List<TRUCategory>) getCurrentFeedExecutionContext().getConfigValue(
						TRUProductFeedConstants.TRUCATEGORYLIST);
				
				Set<String> nonMerchRootCategoriesSet = pVOItem.getRootParentCategories();
				
				
				for (String rootParentCategory : nonMerchRootCategoriesSet) {
					if (!StringUtils.isBlank(rootParentCategory)) {

						for (Iterator<TRUCategory> iterator = truCategoryList.iterator(); iterator.hasNext();) {
							TRUCategory truCategory =  iterator.next();
							if (!StringUtils.isBlank(truCategory.getRepositoryId()) &&
									!StringUtils.isBlank(rootParentCategory) && !parentCatIDs.contains(rootParentCategory)) {
								if (truCategory.getRepositoryId().equals(TRUProductFeedConstants.NAVIGATION_TRU) && 
										rootParentCategory.equals(TRUProductFeedConstants.NAVIGATION_TRU)) {
									truCategory.getTRUFixedChildProducts().add(pVOItem.getId());
								} else if (truCategory.getRepositoryId().equals(TRUProductFeedConstants.NAVIGATION_BRU) && 
										rootParentCategory.equals(TRUProductFeedConstants.NAVIGATION_BRU)) {
									truCategory.getBRUFixedChildProducts().add(pVOItem.getId());
								} else if (truCategory.getRepositoryId().equals(TRUProductFeedConstants.NAVIGATION_FAO) && 
										rootParentCategory.equals(TRUProductFeedConstants.NAVIGATION_FAO)) {
									truCategory.getFAOFixedChildProducts().add(pVOItem.getId());
								}
							}
						}
					}
				}
				
				
			}
		}
		if (!StringUtils.isBlank(feed) && feed.equalsIgnoreCase(TRUProductFeedConstants.DELTA) && !StringUtils.isBlank(pVOItem.getType()) &&
				!pVOItem.getType().equalsIgnoreCase(TRUProductFeedConstants.NONMERCHSKU)) {
			getDeltaRepositoryToProductVO().checkForSKUDelete(pVOItem, pRepoItem);
		}
		getDeltaRepositoryToProductVO().updateMerchandiseFlags(pVOItem, pRepoItem);
		getLogger().vlogDebug("End @Class: TRUProductRelationshipMapper, @method: map()");
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		return pRepoItem;
	}

	/**
	 * Delete product relationship properties.
	 *
	 * @param pVOItem the VO item
	 * @param pRepoItem the repo item
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	protected void deleteProductRelationshipProperties(
			TRUProductFeedVO pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException, FeedSkippableException {
		
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentCategories(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildSKUsName(), null);
	}
}
