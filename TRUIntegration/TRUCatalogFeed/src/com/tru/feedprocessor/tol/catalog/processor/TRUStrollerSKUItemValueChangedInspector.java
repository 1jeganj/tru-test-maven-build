package com.tru.feedprocessor.tol.catalog.processor;

import java.util.List;
import java.util.Map;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValueChangedInspector;
import com.tru.feedprocessor.tol.catalog.vo.TRUStrollerSKUVO;

/**
 * The Class TRUStrollerSKUItemValueChangedInspector. This class inspects each and every property holds in entites to
 * repository of SKU properties and StrollerSKU properties has any changes. If any change occurs in one property entire
 * entity will be updated.
 * @author Professional Access
 * @version 1.0
 */
public class TRUStrollerSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold CommonSKUItemValueChangedInspector holder. */
	private TRUCommonSKUItemValueChangedInspector mCommonSKUItemValueChangedInspector;
	

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the common sku item value changed inspector.
	 * 
	 * @return the commonSKUItemValueChangedInspector
	 */
	public TRUCommonSKUItemValueChangedInspector getCommonSKUItemValueChangedInspector() {
		return mCommonSKUItemValueChangedInspector;
	}

	/**
	 * Sets the common sku item value changed inspector.
	 * 
	 * @param pCommonSKUItemValueChangedInspector
	 *            the commonSKUItemValueChangedInspector to set
	 */
	public void setCommonSKUItemValueChangedInspector(
			TRUCommonSKUItemValueChangedInspector pCommonSKUItemValueChangedInspector) {
		mCommonSKUItemValueChangedInspector = pCommonSKUItemValueChangedInspector;
	}


	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the SKU properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUStrollerSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUStrollerSKUVO) {
			TRUStrollerSKUVO skuVO = (TRUStrollerSKUVO) pBaseFeedProcessVO;
			Map<String, String> strollerPropertiesMap = (Map<String, String>) pRepositoryItem
					.getPropertyValue(getFeedCatalogProperty().getStrollerPropertiesMap());
			if (getCommonSKUItemValueChangedInspector().isUpdated(skuVO, pRepositoryItem)) {
				return retVal = Boolean.TRUE;
			}
			
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_BEST_USES)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlBestUses()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String strlBestUses : skuVO.getStrlBestUses()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlBestUses()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlBestUses())).contains(strlBestUses)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}

			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_EXTRAS)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlExtras()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String strlExtras : skuVO.getStrlExtras()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlExtras()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlExtras())).contains(strlExtras)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_TYPE)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlTypes()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String strlTypes : skuVO.getStrlTypes()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlTypes()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlTypes())).contains(strlTypes)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_WHAT_IS_IMP)) {
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlWhatIsImp()) == null) {
					return retVal = Boolean.TRUE;
				} else {
					for (String strlWhatisImp : skuVO.getStrlWhatIsImp()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlWhatIsImp()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStrlWhatIsImp())).contains(strlWhatisImp)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if (!skuVO.getStrollerPropertiesMap().isEmpty()
					&& !getCommonSKUItemValueChangedInspector().mapsAreEqual(skuVO.getStrollerPropertiesMap(),
							strollerPropertiesMap)) {
				return retVal = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUStrollerSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
}
