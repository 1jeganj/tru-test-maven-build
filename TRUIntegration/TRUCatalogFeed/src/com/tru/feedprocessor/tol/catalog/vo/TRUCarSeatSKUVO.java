package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUCarSeatSKUVO. This class extends TRUSkuFeedVO to set the entity name to CarSeatSKU.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCarSeatSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU car seat skuvo.
	 */
	public TRUCarSeatSKUVO() {
		setEntityName(TRUProductFeedConstants.CAR_SEAT_SKU);
	}

}