package com.tru.feedprocessor.tol.catalog.processor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassificationCrossReference;

/**
 * The Class TRUClassificationRelationshipItemValueChangedInspector. This class inspects each and every property holds in entites to repository of
 * Classification has any changes
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUClassificationRelationshipItemValueChangedInspector implements IItemValueChangeInspector {

	/** The Feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * The method is overridden to check whether properties are modified or not of the current vo to the repository item .
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUClassificationRelationshipItemValueChangedInspector, @method: isUpdated()");
		
		boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUClassification) {
			TRUClassification classificationVO = (TRUClassification) pBaseFeedProcessVO;
			if(StringUtils.isBlank(classificationVO.getErrorMessage())){
				Set<String> taxonomyChildClassificationSet = getTaxonomyChildClassificationArray(classificationVO);
				if(!taxonomyChildClassificationSet.isEmpty() && getFeedCatalogProperty().getFixedChildCategoriesName() != null){
					Set<String> repoIds = new HashSet<String>();
					List<RepositoryItem> DBFixedChildCategories = (List<RepositoryItem>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName());
					for(RepositoryItem repo: DBFixedChildCategories){
						repoIds.add(repo.getRepositoryId());
					}
					if(!(taxonomyChildClassificationSet.size() == repoIds.size() && repoIds.containsAll(taxonomyChildClassificationSet))){
						retVal = Boolean.TRUE;
					}
				}
			}
		}

		getLogger().vlogDebug("End @Class: TRUClassificationRelationshipItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}
	
	/**
	 * This method is used to get the childClassification.
	 * 
	 * @param pVOItem -classification
	 * @return childCategory
	 */
	private Set<String> getTaxonomyChildClassificationArray(TRUClassification pVOItem){
		getLogger().vlogDebug("Begin @Class: TRUClassificationRelationshipItemValueChangedInspector, @method: getTaxonomyChildClassificationArray()");
		Set<String> childCategory = new HashSet<String>();
		childCategory.addAll(pVOItem.getChildCategories());
		List<TRUClassificationCrossReference> crossVo = pVOItem.getClassificationCrossReference();
		if (crossVo != null) {
			for (TRUClassificationCrossReference crossObj : crossVo) {
				if (!childCategory.contains(crossObj.getId())) {
					childCategory.add(crossObj.getId());
				}
			}
		}
		getLogger().vlogDebug("End @Class: TRUClassificationRelationshipItemValueChangedInspector, @method: getTaxonomyChildClassificationArray()");
		return childCategory;
	}

}
