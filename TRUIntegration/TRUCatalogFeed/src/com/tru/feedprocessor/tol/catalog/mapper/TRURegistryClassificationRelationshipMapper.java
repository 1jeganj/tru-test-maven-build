package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;

/**
 * The Class CategoryRelationshipMapper. This class maps the Classification relationships data stored in entities to
 * repository to push the data
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRURegistryClassificationRelationshipMapper extends AbstractFeedItemMapper<TRUClassification> {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	

	/**
	 * This Method map() will maps the relation among the classifications and sets the parentCategory,RootParentCategory
	 * properties with the respective values.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MutableRepositoryItem map(TRUClassification pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRURegistryClassificationRelationshipMapper, @method: map()");
		
		String method = TRUProductFeedConstants.REGISTRY_CLASS_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.RRMCOUNT);
		count++;
		getLogger().vlogDebug("registryRelMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.RRMCOUNT,count);
		
		EntityDataProvider entityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = (List<BaseFeedProcessVO>) entityDataProvider
				.getEntityList(TRUFeedConstants.REGISTRY_CLASSIFICATION_ENTITY);

		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);

		if (!StringUtils.isBlank(pVOItem.getParentCategory())) {
			String[] childClassificationArray = getChildClassificationArray(pVOItem.getID(), lBaseFeedProcessVOList);

			RepositoryItem[] lCategoryRepositoryItems = productCatalogRepository.getItems(childClassificationArray,
					TRUFeedConstants.REGISTRY_CLASSIFICATION_ENTITY);

			if (lCategoryRepositoryItems != null) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName(),
						Arrays.asList(lCategoryRepositoryItems));
			}
		} else if (StringUtils.isBlank(pVOItem.getParentCategory())) {
			String[] crossClassificationArray = null;
			if (pVOItem.getRegistryCrossReferenceIds() != null && !pVOItem.getRegistryCrossReferenceIds().isEmpty()) {
				crossClassificationArray = pVOItem.getRegistryCrossReferenceIds().toArray(
						new String[pVOItem.getRegistryCrossReferenceIds().size()]);
				RepositoryItem[] crossClassificationItems = productCatalogRepository.getItems(crossClassificationArray,
						TRUFeedConstants.CLASSIFICATION_ENTITY);
				if (crossClassificationItems != null && crossClassificationItems.length > 0) {
					pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrossReferencePropertyName(),
							Arrays.asList(crossClassificationItems));
				}
			}
		}
		getLogger().vlogDebug("End @Class: TRURegistryClassificationRelationshipMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}

		return pRepoItem;
	}

	/**
	 * This method Gets the child classification array.
	 * 
	 * @param pCategoryId
	 *            - pCategoryId
	 * @param pBaseFeedProcessVOList
	 *            - pBaseFeedProcessVOList
	 * @return String[]
	 */
	private String[] getChildClassificationArray(String pCategoryId, List<BaseFeedProcessVO> pBaseFeedProcessVOList) {
		getLogger().vlogDebug(
				"Begin @Class: TRURegistryClassificationRelationshipMapper, @method: getChildClassificationArray()");
		Set<String> childCategory = new HashSet<String>();
		for (BaseFeedProcessVO lBaseFeedProcessVO : pBaseFeedProcessVOList) {
			TRUClassification classification = (TRUClassification) lBaseFeedProcessVO;
			if (classification.getID() != null && classification.getID().equals(pCategoryId) && 
					classification.getUserTypeID() != null 	&&
					classification.getUserTypeID().equals(TRUFeedConstants.PRIMARY_CATEGORY)) {
				childCategory.addAll(classification.getTRURegistrySubClassfication());
			}
		}
		getLogger().vlogDebug(
				"End @Class: TRURegistyClassificationRelationshipMapper, @method: getChildClassificationArray()");
		return childCategory.toArray(new String[childCategory.size()]);
	}

}
