package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUBatteryQuantitynTypeVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;

/**
 * The Class TRUProductItemValidator. This class validates the mandatory
 * properties of Product
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUProductItemValidator implements IFeedItemValidator {

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to validate the mandatory properties of product.
	 * 
	 * and for any exception
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUProductItemValidator, @method: validate()");

		if (pBaseFeedProcessVO instanceof TRUProductFeedVO) {
			TRUProductFeedVO productVO = (TRUProductFeedVO) pBaseFeedProcessVO;
			
			if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELETE)) {
				if(StringUtils.isBlank(productVO.getFeedActionCode())){
					productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_EMPTY);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_EMPTY, productVO, null);
				} else if(!productVO.getFeedActionCode().equals(TRUProductFeedConstants.D)){
					productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_NOT_D);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_NOT_D, productVO, null);
				}
				return;
			}
			
			if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)
					&& StringUtils.isBlank(productVO.getFeedActionCode())) {
				productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_EMPTY);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_ACTION_CODE_EMPTY, productVO, null);
			}
			if (StringUtils.isBlank(productVO.getId())) {
				productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID, productVO, null);
			}
			// In Full feed Proudct not having onlinetitle and there is no skus
			// associated for that product
			if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.FULL)
					&& StringUtils.isBlank(productVO.getOnlineTitle()) && productVO.getChildSKUsList().isEmpty()) {
				productVO
						.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME, productVO, null);
			}
			// In Full feed Proudct not having onlinetitle and is skus also not
			// having onlinetitle
			else if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.FULL)
					&& StringUtils.isBlank(productVO.getOnlineTitle()) && !productVO.getChildSKUsList().isEmpty()
					&& StringUtils.isBlank(productVO.getChildSKUsList().get(0).getOnlineTitle())) {
				productVO
						.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME, productVO, null);
			}
			// In Delta feed Product with Actioncode A and there is no skus
			// associated for that product
			else if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)
					&& !StringUtils.isBlank(productVO.getFeedActionCode()) && productVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.A)
					&& StringUtils.isBlank(productVO.getOnlineTitle()) && productVO.getChildSKUsList().isEmpty() && productVO.getStyleUIDs().isEmpty()) {
				productVO
						.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME, productVO, null);
			}
			// In Delta feed Product with Actioncode A and is skus also not
			// having onlinetitle
			else if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)
					&& !StringUtils.isBlank(productVO.getFeedActionCode()) && productVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.A)
					&& StringUtils.isBlank(productVO.getOnlineTitle()) && !productVO.getChildSKUsList().isEmpty() && StringUtils.isBlank(productVO.getChildSKUsList().get(0).getOnlineTitle())
					&& productVO.getStyleUIDs().isEmpty()) {
				productVO
						.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME, productVO, null);
			}
			// In Delta feed Style product without onlinetitle
			else if (!StringUtils.isBlank(productVO.getFeed()) && productVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)
					&& !productVO.getStyleUIDs().isEmpty() && StringUtils.isBlank(productVO.getOnlineTitle())) {
				productVO
						.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NAME, productVO, null);
			}
			if (!StringUtils.isBlank(productVO.getRusItemNumber())) {
				try {
					Long.parseLong(productVO.getRusItemNumber());
				} catch (NumberFormatException ne) {
					getLogger().vlogError(ne, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_RUSITEM_NUMBER);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_RUSITEM_NUMBER, productVO, ne);
				}
			}
			if (!StringUtils.isBlank(productVO.getGender())
					&& !(productVO.getGender().contains(TRUProductFeedConstants.BOY) || productVO.getGender().contains(TRUProductFeedConstants.GIRL) || productVO
							.getGender().contains(TRUProductFeedConstants.BOTH))) {
				productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_GENDER);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_GENDER, productVO, null);
			}
			if (!StringUtils.isBlank(productVO.getTaxCode())) {
				try {
					Integer.parseInt(productVO.getTaxCode());
				} catch (NumberFormatException ne) {
					getLogger().vlogError(ne, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
					productVO.setErrorMessage(TRUProductFeedConstants.INVALID_RECORD_PRODUCT_TAX_CODE);
					throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_TAX_CODE, productVO, ne);
				}
			}
			if (productVO.getBatteryQuantitynTypeList() != null && !productVO.getBatteryQuantitynTypeList().isEmpty()) {
				for (TRUBatteryQuantitynTypeVO batteryQuantitynType : productVO.getBatteryQuantitynTypeList()) {
					try {
						if(batteryQuantitynType.getBatteryQuantity() != null){
							Integer.parseInt(batteryQuantitynType.getBatteryQuantity());
						}
					} catch (NumberFormatException ex) {
						getLogger().vlogError(ex, TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
						productVO.setErrorMessage(TRUProductFeedConstants.BATTERY_QUANTITY_ID+TRUProductFeedConstants.EMPTY_STRING+TRUProductFeedConstants.INVALID_RECORD_PRODUCT);
						throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_NMWA,
								productVO, ex);
					}
				}
			}
		}

		getLogger().vlogDebug("End @Class: TRUProductItemValidator, @method: validate()");

	}

}
