package com.tru.feedprocessor.tol.catalog.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCrossSellsnUidBatteriesVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUSKUVOToRepository. This class maps the data stored in SKU entities to SKU repository item to push the data
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUSKUVOToRepository {

	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * The variable to hold "FeedRepository" property name.
	 */
	private MutableRepository mFeedRepository;

	/**
	 * The variable to hold "InventoryRepository" property name.
	 */
	private MutableRepository mInventoryRepository;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Setting common sku properties.
	 * 
	 * and it is used to write the VO properties to the ProductCatalog repository
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public MutableRepositoryItem settingCommonSKUProperties(TRUSkuFeedVO pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException {

		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: settingCommonSKUProperties()");
		
		if (pVOItem.getEmptySkuPropertiesFlag() && !StringUtils.isBlank(pVOItem.getFeed())
				&& pVOItem.getFeed().equalsIgnoreCase(
						TRUProductFeedConstants.DELETE)
				&& !StringUtils.isBlank(pVOItem.getFeedActionCode())
				&& pVOItem.getFeedActionCode().equalsIgnoreCase(
						TRUProductFeedConstants.D)) {
			
			deleteSKUProperties(pRepoItem);
			
			return pRepoItem;
		}
		
		if (!StringUtils.isBlank(pVOItem.getId())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getId());
		}
		if (!StringUtils.isBlank(pVOItem.getOnlineTitle())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getOnlineTitle());
		}
		if (!StringUtils.isBlank(pVOItem.getOnlineLongDesc())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), pVOItem.getOnlineLongDesc());
		}
		
		if (pVOItem.getOnlineCollectionName() != null && !pVOItem.getOnlineCollectionName().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> dbOnlineCollectionName = new HashMap<String, String>();
				dbOnlineCollectionName = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getOnlineCollectionName());
				deltaMapCheck(dbOnlineCollectionName, pVOItem.getOnlineCollectionName());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getOnlineCollectionName(), pVOItem.getOnlineCollectionName());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getOnlineCollectionName(), pVOItem.getOnlineCollectionName());
			}
		}
		
		if (pVOItem.getAkhiMap() != null && !pVOItem.getAkhiMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBAkhiMap = new HashMap<String, String>();
				DBAkhiMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getAkhiMap());
				deltaMapCheck(DBAkhiMap, pVOItem.getAkhiMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getAkhiMap(), pVOItem.getAkhiMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getAkhiMap(), pVOItem.getAkhiMap());
			}
		}
		if (pVOItem.getAssemblyMap() != null && !pVOItem.getAssemblyMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBAssemblyMap = new HashMap<String, String>();
				DBAssemblyMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getAssemblyMap());
				deltaMapCheck(DBAssemblyMap, pVOItem.getAssemblyMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getAssemblyMap(), pVOItem.getAssemblyMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getAssemblyMap(), pVOItem.getAssemblyMap());
			}
		}
		if (!StringUtils.isBlank(pVOItem.getBackorderStatus())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBackOrderStatus(), pVOItem.getBackorderStatus());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.BRAND_NAME_SECONDARY)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNameSecondary(), pVOItem.getBrandNameSecondary());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.BRANDNAMETERTIARY_CHARACTER_THEME)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNameTertiaryCharTheme(),
					pVOItem.getBrandnametertiaryCharacterTheme());
		}
		if (!StringUtils.isBlank(pVOItem.getBrowseHiddenKeyword())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrowseHiddenKeyword(), pVOItem.getBrowseHiddenKeyword());
		}
		if (!StringUtils.isBlank(pVOItem.getCanBeGiftWrapped())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCanBeGiftWrapped(), pVOItem.getCanBeGiftWrapped());
		}
		if (!StringUtils.isBlank(pVOItem.getChildWeightMax())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildWeightMax(), pVOItem.getChildWeightMax());
		}
		if (!StringUtils.isBlank(pVOItem.getChildWeightMin())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildWeightMin(), pVOItem.getChildWeightMin());
		}
		if (!StringUtils.isBlank(pVOItem.getColorCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorCode(), pVOItem.getColorCode());
		}
		if (!StringUtils.isBlank(pVOItem.getColorUpcLevel())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorUpcLevel(), getColorId(pVOItem.getColorUpcLevel()));
		}
		if (!StringUtils.isBlank(pVOItem.getCustomerPurchaseLimit())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCustomerPurchaseLimit(),
					pVOItem.getCustomerPurchaseLimit());
		}
		if (!StringUtils.isBlank(pVOItem.getDropshipFlag())) {
			if (pVOItem.getDropshipFlag().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getDropShipFlag(), Boolean.TRUE);
			} else if (pVOItem.getDropshipFlag().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getDropShipFlag(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getFlexibleShippingPlan())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getFlexibleShippingPlan(), pVOItem.getFlexibleShippingPlan());
		}
		if (!StringUtils.isBlank(pVOItem.getFreightClass())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getFreightClass(), pVOItem.getFreightClass());
		}
		if (!StringUtils.isBlank(pVOItem.getFurnitureFinish())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getFurnitureFinish(), pVOItem.getFurnitureFinish());
		}
		if (!StringUtils.isBlank(pVOItem.getIncrementalSafetyStockUnits())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getIncrementalSafetyStockUnits(),
					pVOItem.getIncrementalSafetyStockUnits());
		}
		if (!pVOItem.getItemDimensionMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBItemDimensionMap = new HashMap<String, String>();
				DBItemDimensionMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getItemDimensionMap());
				deltaMapCheck(DBItemDimensionMap, pVOItem.getItemDimensionMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getItemDimensionMap(), pVOItem.getItemDimensionMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getItemDimensionMap(), pVOItem.getItemDimensionMap());
			}
		}
		if (!StringUtils.isBlank(pVOItem.getJuvenileProductSize())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getJuvenileProductSize(),
					getSizeId(pVOItem.getJuvenileProductSize()));
		}
		if (!pVOItem.getLower48Map().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBLower48Map = new HashMap<String, String>();
				DBLower48Map = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getLower48Map());
				deltaMapCheck(DBLower48Map, pVOItem.getLower48Map());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getLower48Map(), pVOItem.getLower48Map());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getLower48Map(), pVOItem.getLower48Map());
			}
		}
		if (!StringUtils.isBlank(pVOItem.getMaxTargetAgeTruWebsites())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getMaxTargetAgeTruWebsites(),
					pVOItem.getMaxTargetAgeTruWebsites());
		}
		if (!StringUtils.isBlank(pVOItem.getMfrSuggestedAgeMax())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMax(), pVOItem.getMfrSuggestedAgeMax());
		}
		if (!StringUtils.isBlank(pVOItem.getMfrSuggestedAgeMin())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMin(), pVOItem.getMfrSuggestedAgeMin());
		}
		if (!StringUtils.isBlank(pVOItem.getMinTargetAgeTruWebsites())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getMinTargetAgeTruWebsites(),
					pVOItem.getMinTargetAgeTruWebsites());
		}
		if (!StringUtils.isBlank(pVOItem.getNmwaPercentage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getNmwaPercentage(),
					Float.parseFloat(pVOItem.getNmwaPercentage()));
		}
		if (!StringUtils.isBlank(pVOItem.getShipWindowMax())) {
			Integer value = Integer.parseInt(pVOItem.getShipWindowMax()) / TRUProductFeedConstants.NUMBER_TWENTY_FOUR;
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMax(), value.toString());
		}
		if (!StringUtils.isBlank(pVOItem.getShipWindowMin())) {
			Integer value = Integer.parseInt(pVOItem.getShipWindowMin()) / TRUProductFeedConstants.NUMBER_TWENTY_FOUR;
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMin(), value.toString());
		}
		if (!StringUtils.isBlank(pVOItem.getOnlinePID())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getOnlinePID(), pVOItem.getOnlinePID());
		}
		if (!StringUtils.isBlank(pVOItem.getOriginalPID())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getOriginalPID(), pVOItem.getOriginalPID());
		}
		if (!StringUtils.isBlank(pVOItem.getStreetDate())) {
			SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
			Date date = null;
			try {
				date = formatter.parse(pVOItem.getStreetDate());
			} catch (ParseException e) {
				if (isLoggingError()) {
					logError(TRUProductFeedConstants.PARSE_EXCEPTION, e);
				}
			}
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStreetDate(), date);
		}
		if (!StringUtils.isBlank(pVOItem.getInventoryReceivedDate())) {
			SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
			Date date = null;
			try {
				date = formatter.parse(pVOItem.getInventoryReceivedDate());
			} catch (ParseException e) {
				if (isLoggingError()) {
					logError(TRUProductFeedConstants.PARSE_EXCEPTION, e);
				}
			}
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getInventoryReceivedDate(), date);
		}
		if (!StringUtils.isBlank(pVOItem.getRmsSizeCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRmsSizeCode(), pVOItem.getRmsSizeCode());
		}
		if (!StringUtils.isBlank(pVOItem.getRmsColorCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRmsColorCode(), pVOItem.getRmsColorCode());
		}
		if (!StringUtils.isBlank(pVOItem.getOtherFixedSurcharge())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getOtherFixedSurcharge(), pVOItem.getOtherFixedSurcharge());
		}
		if (!StringUtils.isBlank(pVOItem.getOtherStdFSDollar())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getOtherStdFSDollar(), pVOItem.getOtherStdFSDollar());
		}
		if (!StringUtils.isBlank(pVOItem.getOutlet())) {
			if (pVOItem.getOutlet().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getOutlet(), Boolean.TRUE);
			} else if (pVOItem.getOutlet().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getOutlet(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getPresellQuantityUnits())) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getPresellQuantityUnits(), pVOItem.getPresellQuantityUnits());
		}
		if (!StringUtils.isBlank(pVOItem.getPriceDisplay())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPriceDisplay(), pVOItem.getPriceDisplay());
		}
		if (!StringUtils.isBlank(pVOItem.getProductAwards())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductAwards(), pVOItem.getProductAwards());
		}
		if (!pVOItem.getProductFeature().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductFeature(), pVOItem.getProductFeature());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.PRODUCT_SAFETY_WARNING)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductSafetyWarnings(),
					pVOItem.getProductSafetyWarnings());
		}
		if (!StringUtils.isBlank(pVOItem.getPromotionalStickerDisplay())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPromotionalStickerDisplay(),
					pVOItem.getPromotionalStickerDisplay());
		}
		if (!StringUtils.isBlank(pVOItem.getRegistryWarningIndicator())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryWarningIndicator(),
					pVOItem.getRegistryWarningIndicator());
		}
		if (!StringUtils.isBlank(pVOItem.getRegistryEligibility())) {
			if (pVOItem.getRegistryEligibility().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryEligibility(), Boolean.TRUE);
			} else if (pVOItem.getRegistryEligibility().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryEligibility(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getSafetyStock())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSafetyStock(), pVOItem.getSafetyStock());
		}
		if (!StringUtils.isBlank(pVOItem.getShipInOrigContainer())) {
			if (pVOItem.getShipInOrigContainer().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipInOrigContainer(), Boolean.TRUE);
			} else if (pVOItem.getShipInOrigContainer().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipInOrigContainer(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getSupressInSearch())) {
			if (pVOItem.getSupressInSearch().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getSupressInSearch(), Boolean.TRUE);
			} else if (pVOItem.getSupressInSearch().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getSupressInSearch(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getSystemRequirements())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSystemRequirements(), pVOItem.getSystemRequirements());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.UPC_NUMBER)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getUpcNumbers(), pVOItem.getUpcNumbers());
		}
		if (!StringUtils.isBlank(pVOItem.getNmwa())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getNmwa(), pVOItem.getNmwa());
		}
		if (!StringUtils.isBlank(pVOItem.getWebDisplayFlag())) {
			if (pVOItem.getWebDisplayFlag().equalsIgnoreCase(TRUProductFeedConstants.A)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getWebDisplayFlag(), Boolean.TRUE);
			} else if (pVOItem.getWebDisplayFlag().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getWebDisplayFlag(), Boolean.FALSE);
			}
		}
		if (pVOItem.getBppEligible() != null) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBppEligible(), pVOItem.getBppEligible());
		}
		if (!StringUtils.isBlank(pVOItem.getBppName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBppName(), pVOItem.getBppName());
		}
		if (!StringUtils.isBlank(pVOItem.getLegalNotice())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLegalNotice(), pVOItem.getLegalNotice());
		}
		if (!StringUtils.isBlank(pVOItem.getSwatchImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSwatchImage(), pVOItem.getSwatchImage());
		}
		if (!StringUtils.isBlank(pVOItem.getPrimaryImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryImage(), pVOItem.getPrimaryImage());
		}
		if (!StringUtils.isBlank(pVOItem.getSecondaryImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSecondaryImage(), pVOItem.getSecondaryImage());
		}
		if (!StringUtils.isBlank(pVOItem.getSwatchCanonicalImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSwatchCanonicalImage(), pVOItem.getSwatchCanonicalImage());
		}
		if (!StringUtils.isBlank(pVOItem.getPrimaryCanonicalImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryCanonicalImage(), pVOItem.getPrimaryCanonicalImage());
		}
		if (!StringUtils.isBlank(pVOItem.getSecondaryCanonicalImage())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSecondaryCanonicalImage(), pVOItem.getSecondaryCanonicalImage());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.INTEREST)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getInterest(), pVOItem.getInterest());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.FINDER_ELIGIBLE)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getFinderEligible(), pVOItem.getFinderEligible());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.SPECIAL_FEATURES)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSpecialFeatures(), pVOItem.getSpecialFeatures());
		}
		if (!StringUtils.isBlank(pVOItem.getColorFamily())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorFamily(),
					pVOItem.getColorFamily());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.SIZE_FAMILY)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSizeFamily(), pVOItem.getSizeFamily());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CHARACTER_THEME)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCharacterTheme(), pVOItem.getCharacterTheme());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.COLLECTION_THEME)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCollectionTheme(), pVOItem.getCollectionTheme());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.ALTERNATE_IMAGE)) {
			List<String> alternateImage = new ArrayList<String>();
			alternateImage.addAll(pVOItem.getAlternateImage());
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getAlternateImage(), alternateImage);
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CANONICAL_IMAGE_URL)) {
			List<String> alternateCanonicalImage = new ArrayList<String>();
			alternateCanonicalImage.addAll(pVOItem.getAlternateCanonicalImage());
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getAlternateCanonicalImage(), alternateCanonicalImage);
		}
		if (!StringUtils.isBlank(pVOItem.getSlapperItem())) {
			if (pVOItem.getSlapperItem().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getSlapperItem(), Boolean.TRUE);
			} else if (pVOItem.getSlapperItem().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getSlapperItem(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getIsDeleted())) {
			if (pVOItem.getIsDeleted().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName(), Boolean.TRUE);
			} else if (pVOItem.getIsDeleted().equalsIgnoreCase(TRUProductFeedConstants.N)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName(), Boolean.FALSE);
			}
		}
		if (!pVOItem.getCrossSellSkuId().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrossSellSkuId(), pVOItem.getCrossSellSkuId());
		}
		if (!pVOItem.getCrossSellsnUidBatteriesList().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrosssellBatteries(),
					createCrossSellsnUidBatteries(pVOItem.getCrossSellsnUidBatteriesList(),pRepoItem));

		}
		if (!StringUtils.isBlank(pVOItem.getShipFromStoreEligibleLov())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipFromStoreEligibleLov(),
					pVOItem.getShipFromStoreEligibleLov());
		}
		if (!StringUtils.isBlank(pVOItem.getVendorsProductDemoUrl())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl(),
					pVOItem.getVendorsProductDemoUrl());
		}
		if (!StringUtils.isBlank(pVOItem.getShipToStoreEeligible())) {
			if (pVOItem.getShipToStoreEeligible().equalsIgnoreCase(TRUProductFeedConstants.YES)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible(), Boolean.TRUE);
			} else if (pVOItem.getShipToStoreEeligible().equalsIgnoreCase(TRUProductFeedConstants.NO)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible(), Boolean.FALSE);
			}
		}
		if (!StringUtils.isBlank(pVOItem.getDivisionCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDivisionCode(),
					pVOItem.getDivisionCode());
		}
		if (!StringUtils.isBlank(pVOItem.getDepartmentCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDepartmentCode(),
					pVOItem.getDepartmentCode());
		}
		if (!StringUtils.isBlank(pVOItem.getClassCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getClassCode(),
					pVOItem.getClassCode());
		}
		if (!StringUtils.isBlank(pVOItem.getSubclassCode())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSubclassCode(),
					pVOItem.getSubclassCode());
		}
		if (!StringUtils.isBlank(pVOItem.getSkills())){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSkills(),pVOItem.getSkills());
		}
		if (!StringUtils.isBlank(pVOItem.getPrimaryCategory())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryCategory(),pVOItem.getPrimaryCategory());
		}
		if (!StringUtils.isBlank(pVOItem.getPlayType())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPlayType(),pVOItem.getPlayType());
		}
		if (!StringUtils.isBlank(pVOItem.getWhatIsImportant())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getWhatIsImportant(),pVOItem.getWhatIsImportant());
		}
		if (!StringUtils.isBlank(pVOItem.getBrandNamePrimary())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNamePrimary(), pVOItem.getBrandNamePrimary());
		}
		if (!StringUtils.isBlank(pVOItem.getChannelAvailability())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getChannelAvailability(), pVOItem.getChannelAvailability());
		}
		if (!StringUtils.isBlank(pVOItem.getItemInStorePickUp())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getItemInStorePickUp(), pVOItem.getItemInStorePickUp());
		}
		
		if (!StringUtils.isBlank(pVOItem.getPrimaryPathCategory())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryPathCategory(),pVOItem.getPrimaryPathCategory());
		}
		
		updateProductHTGITMsgPropertyInRepositoryItem(pRepoItem);
		
		// Properties included from sku relationship mapper
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.PARENT_CLASSIFICATIONS)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(),
					pVOItem.getParentClassifications());
		}
		if (!StringUtils.isBlank(pVOItem.getOrignalParentProduct().getId())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getOrignalParentProduct(), pVOItem.getOrignalParentProduct()
					.getId());
		}

		// Inventory changes
		if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.FULL) && !StringUtils.isBlank(pVOItem.getStreetDate()) && 
				!StringUtils.isBlank(pVOItem.getPresellQuantityUnits()) && 
				pVOItem.getInventoryFlag()) {
			Date current = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
			Date streetDate = null;
			try {
				streetDate = formatter.parse(pVOItem.getStreetDate());
			} catch (ParseException parseException) {
				if (isLoggingError()) {
					logError(TRUProductFeedConstants.PARSE_EXCEPTION, parseException);
				}
			}
			if (streetDate.after(current)) {
				createInventory(pVOItem);
			}
		} else if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) &&
				!StringUtils.isBlank(pVOItem.getPresellQuantityUnits()) && pVOItem.getInventoryFlag()) {
			createInventory(pVOItem);
		}
		
		//stroller properties
		
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_BEST_USES)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlBestUses(), pVOItem.getStrlBestUses());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_EXTRAS)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlExtras(), pVOItem.getStrlExtras());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_TYPE)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlTypes(), pVOItem.getStrlTypes());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.STRL_WHAT_IS_IMP)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlWhatIsImp(), pVOItem.getStrlWhatIsImp());
		}
		if (!pVOItem.getStrollerPropertiesMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBStrollerPropertiesMap = new HashMap<String, String>();
				DBStrollerPropertiesMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getStrollerPropertiesMap());
				deltaMapCheck(DBStrollerPropertiesMap, pVOItem.getStrollerPropertiesMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrollerPropertiesMap(),
						pVOItem.getStrollerPropertiesMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrollerPropertiesMap(),
						pVOItem.getStrollerPropertiesMap());
			}
		}
		
		//crib properties
		
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_MATERIAL_TYPE)) {
			pRepoItem
					.setPropertyValue(getFeedCatalogProperty().getCribMaterialTypes(), pVOItem.getCribMaterialTypes());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_STYLE)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribStyles(), pVOItem.getCribStyles());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_TYPE)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribTypes(), pVOItem.getCribTypes());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CRIB_WHAT_IS_IMP)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribWhatIsImp(), pVOItem.getCribWhatIsImp());
		}
		if (pVOItem.getCribPropertiesMap() != null && !pVOItem.getCribPropertiesMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBCribPropertiesMap = new HashMap<String, String>();
				DBCribPropertiesMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getCribPropertiesMap());
				deltaMapCheck(DBCribPropertiesMap, pVOItem.getCribPropertiesMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribPropertiesMap(),
						pVOItem.getCribPropertiesMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribPropertiesMap(),
						pVOItem.getCribPropertiesMap());
			}
		}
		
		//car seat properties
		
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_FEATURES)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatFeatures(), pVOItem.getCarSeatFeatures());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_TYPE)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatTypes(), pVOItem.getCarSeatTypes());
		}
		if (pVOItem.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_WHAT_IS_IMP)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatWhatIsImp(),
					pVOItem.getCarSeatWhatIsImp());
		}
		if (!pVOItem.getCarSeatPropertiesMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBCarSeatPropertiesMap = new HashMap<String, String>();
				DBCarSeatPropertiesMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getCarSeatPropertiesMap());
				deltaMapCheck(DBCarSeatPropertiesMap, pVOItem.getCarSeatPropertiesMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatPropertiesMap(),
						pVOItem.getCarSeatPropertiesMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatPropertiesMap(),
						pVOItem.getCarSeatPropertiesMap());
			}
		}
		
		//bookCdDvd properties
		
		if (!pVOItem.getBookCdDvdPropertiesMap().isEmpty()) {
			if (pVOItem.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA)) {
				Map<String, String> DBBookCdDvdPropertiesMap = new HashMap<String, String>();
				DBBookCdDvdPropertiesMap = (Map<String, String>) pRepoItem.getPropertyValue(getFeedCatalogProperty()
						.getBookCdDvdPropertiesMap());
				deltaMapCheck(DBBookCdDvdPropertiesMap, pVOItem.getBookCdDvdPropertiesMap());
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getBookCdDvdPropertiesMap(),
						pVOItem.getBookCdDvdPropertiesMap());
			} else {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getBookCdDvdPropertiesMap(),
						pVOItem.getBookCdDvdPropertiesMap());
			}
		}
		
		//video game properties
		if (!StringUtils.isBlank(pVOItem.getVideoGameEsrb())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGameEsrb(), pVOItem.getVideoGameEsrb());
		}
		if (!StringUtils.isBlank(pVOItem.getVideoGameGenre())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGameGenre(), pVOItem.getVideoGameGenre());
		}
		if (!StringUtils.isBlank(pVOItem.getVideoGamePlatform())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGamePlatform(),
					pVOItem.getVideoGamePlatform());
		}
		
		//non-merch sku properties
		if(!StringUtils.isBlank(pVOItem.getSubType())){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSubType(), pVOItem.getSubType());
		}
		
		try {
			
			List<String> emptyNullProperties = pVOItem.getEmptyNullProperties();
			for (String emptyNullProp : emptyNullProperties) {
				DynamicBeans.setPropertyValue(pRepoItem, emptyNullProp, null);
			}
			
			List<String> emptyNullSubProperties = pVOItem.getEmptyNullSubProperties();
			for (String emptyNullSubProp : emptyNullSubProperties) {
				
				String[] split = emptyNullSubProp.split(TRUProductFeedConstants.DOT_SPLIT);
				Object propertyValue = pRepoItem.getPropertyValue(split[TRUProductFeedConstants.NUMBER_ZERO]);
				if (propertyValue instanceof Map) {
					Map mapProp = (Map) propertyValue;
					mapProp.remove(split[TRUFeedConstants._1]);
				}
				//DynamicBeans.setSubPropertyValue(pRepoItem, emptyNullSubProp, null);
			}
		} catch (PropertyNotFoundException e) {
			throw new RepositoryException(e);
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: settingCommonSKUProperties()");

		return pRepoItem;
	}
	
	/**
	 * This method updates OriginalProduct ProductHTGITMsg Property.
	 * 
	 * @param pRepoItem
	 *            the repo item
	 */
	private void updateProductHTGITMsgPropertyInRepositoryItem(MutableRepositoryItem pRepoItem) {
		String iSPU = TRUProductFeedConstants.NO;
		String s2S = TRUProductFeedConstants.NO;
		String channelAvailability = (String) pRepoItem.getPropertyValue(getFeedCatalogProperty().getChannelAvailability());
		String itemInStorePickUp = (String) pRepoItem.getPropertyValue(getFeedCatalogProperty().getItemInStorePickUp());
		if (itemInStorePickUp != null && itemInStorePickUp.equalsIgnoreCase(TRUProductFeedConstants.Y)) {
			iSPU = TRUProductFeedConstants.YES;
		}
		if (pRepoItem.getPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible()) != null && 
				(Boolean) pRepoItem.getPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible())) {
			s2S = TRUProductFeedConstants.YES;
		}
		if (!StringUtils.isBlank(channelAvailability)	&&
				channelAvailability.equalsIgnoreCase(TRUProductFeedConstants.ONLINE_ONLY)) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE1);
		} else if (!StringUtils.isBlank(channelAvailability) &&
				channelAvailability.equalsIgnoreCase(TRUProductFeedConstants.IN_STORE_ONLY)) {
			if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.NO) && s2S.equalsIgnoreCase(TRUProductFeedConstants.NO)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE2);
			} else if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.YES) &&
					s2S.equalsIgnoreCase(TRUProductFeedConstants.NO)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE6);
			} else if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.NO) && 
					s2S.equalsIgnoreCase(TRUProductFeedConstants.YES)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE7);
			} else if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.YES) &&
					s2S.equalsIgnoreCase(TRUProductFeedConstants.YES)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE8);
			}
		} else if (StringUtils.isBlank(channelAvailability) ||
				channelAvailability.equalsIgnoreCase(TRUProductFeedConstants.N_A)) {
			if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.YES) && s2S.equalsIgnoreCase(TRUProductFeedConstants.NO)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE3);
			} else if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.NO) && 
					s2S.equalsIgnoreCase(TRUProductFeedConstants.YES)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE4);
			} else if (iSPU.equalsIgnoreCase(TRUProductFeedConstants.YES) && 
					s2S.equalsIgnoreCase(TRUProductFeedConstants.YES)) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), TRUProductFeedConstants.MESSAGE5);
			}
		}
	}

	/**
	 * Creates the cross sellsn uid batteries.
	 *
	 * @param pTRUCrossSellsnUidBatteriesVO            the TRU cross sellsn uid batteries vo
	 * @param pRepoItem the repo item
	 * @return the list
	 * @throws RepositoryException             the repository exception
	 */
	protected List<MutableRepositoryItem> createCrossSellsnUidBatteries(
			List<TRUCrossSellsnUidBatteriesVO> pTRUCrossSellsnUidBatteriesVO , MutableRepositoryItem pRepoItem) throws RepositoryException {

		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: createCrossSellsnUidBatteries()");

		List<MutableRepositoryItem> feedRepositoryItems = new ArrayList<MutableRepositoryItem>();
		List<RepositoryItem> batteries = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getCrosssellBatteries());
		if(!batteries.isEmpty()){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrosssellBatteries(), feedRepositoryItems);
			for(RepositoryItem repoItem : batteries){
				getFeedRepository().removeItem(repoItem.getRepositoryId(), getFeedCatalogProperty().getCrosssellBattery());
			}
		}
		for (TRUCrossSellsnUidBatteriesVO crossSell : pTRUCrossSellsnUidBatteriesVO) {
			MutableRepositoryItem FeedRepositoryItem = getFeedRepository().createItem(
					getFeedCatalogProperty().getCrosssellBattery());
			FeedRepositoryItem.setPropertyValue(getFeedCatalogProperty().getCrossSellsBatteries(),
					crossSell.getCrossSellsBatteries());
			FeedRepositoryItem.setPropertyValue(getFeedCatalogProperty().getUidBatteryCrossSell(),
					crossSell.getUidBatteryCrossSell());
			getFeedRepository().addItem(FeedRepositoryItem);
			feedRepositoryItems.add(FeedRepositoryItem);
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: createCrossSellsnUidBatteries()");

		return feedRepositoryItems;
	}


	/**
	 * Delta map check.
	 * 
	 * @param pDBMap
	 *            the DB map
	 * @param pHandlerMap
	 *            the handler map
	 */
	protected void deltaMapCheck(Map<String, String> pDBMap, Map<String, String> pHandlerMap) {
		for (Map.Entry<String, String> entry : pDBMap.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if (pDBMap.get(key) != null && pHandlerMap.get(key) == null) {
				pHandlerMap.put(key, value);
			}
		}
	}

	/**
	 * Creates the Inventory Item if exists it updates.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	protected void createInventory(TRUSkuFeedVO pVOItem) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: createInventory()");

		RepositoryView InventoryRepositoryView = getInventoryRepository().getView(getFeedCatalogProperty().getInventory());
		QueryBuilder InventoryQueryBuilder = InventoryRepositoryView.getQueryBuilder();
		QueryExpression catalogRefId = InventoryQueryBuilder.createPropertyQueryExpression(getFeedCatalogProperty()
				.getCatalogRefId());
		QueryExpression skuid = InventoryQueryBuilder.createConstantQueryExpression(pVOItem.getId());
		Query skuidQuery = InventoryQueryBuilder.createComparisonQuery(catalogRefId, skuid, QueryBuilder.EQUALS);
		QueryExpression locationId = InventoryQueryBuilder.createPropertyQueryExpression(getFeedCatalogProperty()
				.getLocationId());
		QueryExpression locationIdconstant = InventoryQueryBuilder
				.createConstantQueryExpression(TRUProductFeedConstants.ONLINE);
		Query locationIdQuery = InventoryQueryBuilder.createComparisonQuery(locationId, locationIdconstant,
				QueryBuilder.EQUALS);
		Query[] andQueries = { skuidQuery, locationIdQuery };
		Query InventoryQuery = InventoryQueryBuilder.createAndQuery(andQueries);
		RepositoryItem[] items = InventoryRepositoryView.executeQuery(InventoryQuery);
		if (items != null) {
			MutableRepositoryItem InventoryRepositoryItem = (MutableRepositoryItem) items[0];
			InventoryRepositoryItem.setPropertyValue(getFeedCatalogProperty().getPreorderLevel(),
					Long.parseLong(pVOItem.getPresellQuantityUnits()));
			getInventoryRepository().updateItem(InventoryRepositoryItem);
		} else {
			MutableRepositoryItem InventoryRepositoryItem = getInventoryRepository().createItem(
					TRUProductFeedConstants.INV + pVOItem.getId() + TRUProductFeedConstants.UNDER_SCORE +
					TRUProductFeedConstants.ONLINE, getFeedCatalogProperty().getInventory());
			InventoryRepositoryItem.setPropertyValue(getFeedCatalogProperty().getCatalogRefId(), pVOItem.getId());
			InventoryRepositoryItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getId());
			InventoryRepositoryItem.setPropertyValue(getFeedCatalogProperty().getLocationId(),
					TRUProductFeedConstants.ONLINE);
			InventoryRepositoryItem.setPropertyValue(getFeedCatalogProperty().getPreorderLevel(),
					Long.parseLong(pVOItem.getPresellQuantityUnits()));
			getInventoryRepository().addItem(InventoryRepositoryItem);
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: createInventory()");
	}

	/**
	 * Gets the color id.
	 * 
	 * @param pColorCode
	 *            the color code
	 * @return the color id
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public String getColorId(String pColorCode) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: getColorId()");

		String colorId = null;
		RepositoryView FeedRepositoryView = getFeedRepository().getView(TRUProductFeedConstants.COLOR);
		QueryBuilder FeedQueryBuilder = FeedRepositoryView.getQueryBuilder();
		QueryExpression colorName = FeedQueryBuilder.createPropertyQueryExpression(TRUProductFeedConstants.COLOR_NAME);
		QueryExpression colorColumn = FeedQueryBuilder.createConstantQueryExpression(pColorCode);
		Query colorQuery = FeedQueryBuilder.createComparisonQuery(colorName, colorColumn, QueryBuilder.EQUALS);
		RepositoryItem[] items = FeedRepositoryView.executeQuery(colorQuery);
		if (items != null) {
			RepositoryItem ColorRepositoryItem = (RepositoryItem) items[0];
			colorId = ColorRepositoryItem.getRepositoryId();
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: getColorId()");

		return colorId;

	}

	/**
	 * Gets the size id.
	 * 
	 * @param pSize
	 *            the size
	 * @return the size id
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public String getSizeId(String pSize) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: getSizeId()");

		String sizeId = null;
		RepositoryView FeedRepositoryView = getFeedRepository().getView(TRUProductFeedConstants.SIZE);
		QueryBuilder FeedQueryBuilder = FeedRepositoryView.getQueryBuilder();
		QueryExpression size = FeedQueryBuilder.createPropertyQueryExpression(TRUProductFeedConstants.SIZE_DESCRIPTION);
		QueryExpression sizeColumn = FeedQueryBuilder.createConstantQueryExpression(pSize.toUpperCase());
		Query colorQuery = FeedQueryBuilder.createComparisonQuery(size, sizeColumn, QueryBuilder.EQUALS);
		RepositoryItem[] items = FeedRepositoryView.executeQuery(colorQuery);
		if (items != null) {
			RepositoryItem ColorRepositoryItem = (RepositoryItem) items[0];
			sizeId = ColorRepositoryItem.getRepositoryId();
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: getSizeId()");

		return sizeId;

	}

	/**
	 * Gets the color name.
	 * 
	 * @param pColorId
	 *            the color id
	 * @return the color name
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public String getColorName(String pColorId) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method:  getColorName()");

		String colorCode = null;
		RepositoryItem FeedRepositoryItem = (RepositoryItem) getFeedRepository().getItem(pColorId,
				TRUProductFeedConstants.COLOR);
		if (FeedRepositoryItem != null) {
			colorCode = (String) FeedRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorNamePropertyName());
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method:  getColorName()");

		return colorCode;
	}

	/**
	 * Gets the size description.
	 * 
	 * @param pSizeId
	 *            the size id
	 * @return the size description
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public String getSizeDescription(String pSizeId) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method:  getSizeDescription()");

		String size = null;
		RepositoryItem FeedRepositoryItem = (RepositoryItem) getFeedRepository().getItem(pSizeId,
				TRUProductFeedConstants.SIZE);
		if (FeedRepositoryItem != null) {
			size = (String) FeedRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSizeDescriptionPropertyName());
		}

		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method:  getSizeDescription()");

		return size;
	}
	
	/**
	 * Delete sku properties.
	 *
	 * @param pRepoItem the repo item
	 * @throws RepositoryException the repository exception
	 */
	protected void deleteSKUProperties(MutableRepositoryItem pRepoItem) throws RepositoryException {
		getLogger().vlogDebug("Start @Class: TRUSKUVOToRepository, @method: deleteSKUProperties()");
		
		//setting all properties to null for annual item delete process
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSubType(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGamePlatform(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGameGenre(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVideoGameEsrb(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBookCdDvdPropertiesMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatPropertiesMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatWhatIsImp(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatTypes(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCarSeatFeatures(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribWhatIsImp(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribTypes(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribStyles(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCribMaterialTypes(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrollerPropertiesMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlWhatIsImp(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlTypes(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlExtras(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStrlBestUses(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOrignalParentProduct(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductHTGITMsg(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryPathCategory(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getItemInStorePickUp(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getChannelAvailability(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNamePrimary(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getWhatIsImportant(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPlayType(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryCategory(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSkills(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSubclassCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getClassCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getDepartmentCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getDivisionCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible(), Boolean.FALSE);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipFromStoreEligibleLov(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrosssellBatteries(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrossSellSkuId(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName(), Boolean.TRUE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSlapperItem(), Boolean.FALSE);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getAlternateCanonicalImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getAlternateImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCollectionTheme(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCharacterTheme(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSizeFamily(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorFamily(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSpecialFeatures(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getFinderEligible(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getInterest(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSecondaryCanonicalImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryCanonicalImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSwatchCanonicalImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSecondaryImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSwatchImage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getLegalNotice(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBppName(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBppEligible(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getWebDisplayFlag(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getNmwa(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getUpcNumbers(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSystemRequirements(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSupressInSearch(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipInOrigContainer(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getSafetyStock(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryEligibility(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRegistryWarningIndicator(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPromotionalStickerDisplay(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductSafetyWarnings(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductFeature(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getProductAwards(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPriceDisplay(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getPresellQuantityUnits(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOutlet(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOtherStdFSDollar(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOtherFixedSurcharge(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRmsColorCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getRmsSizeCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getInventoryReceivedDate(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getStreetDate(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOriginalPID(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOnlinePID(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMin(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMax(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getNmwaPercentage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMinTargetAgeTruWebsites(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMin(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMax(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMaxTargetAgeTruWebsites(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMin(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getShipWindowMax(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getNmwaPercentage(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMinTargetAgeTruWebsites(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMin(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMax(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getMaxTargetAgeTruWebsites(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getLower48Map(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getJuvenileProductSize(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getItemDimensionMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getIncrementalSafetyStockUnits(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getFurnitureFinish(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getFreightClass(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getFlexibleShippingPlan(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getDropShipFlag(), Boolean.FALSE); 
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCustomerPurchaseLimit(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorUpcLevel(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getColorCode(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildWeightMin(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildWeightMax(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getCanBeGiftWrapped(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrowseHiddenKeyword(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNameTertiaryCharTheme(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBrandNameSecondary(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getBackOrderStatus(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getAssemblyMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getAkhiMap(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getOnlineCollectionName(), null);
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionName(), null);
		
		getLogger().vlogDebug("End @Class: TRUSKUVOToRepository, @method: deleteSKUProperties()");
	}


	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the feed repository.
	 * 
	 * @return the feedRepository
	 */
	public MutableRepository getFeedRepository() {
		return mFeedRepository;
	}

	/**
	 * Sets the feed repository.
	 * 
	 * @param pFeedRepository
	 *            the feedRepository to set
	 */
	public void setFeedRepository(MutableRepository pFeedRepository) {
		mFeedRepository = pFeedRepository;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the inventory repository.
	 * 
	 * @return the inventoryRepository
	 */
	public MutableRepository getInventoryRepository() {
		return mInventoryRepository;
	}

	/**
	 * Sets the inventory repository.
	 * 
	 * @param pInventoryRepository
	 *            the inventoryRepository to set
	 */
	public void setInventoryRepository(MutableRepository pInventoryRepository) {
		mInventoryRepository = pInventoryRepository;
	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
      * Checks if is logging error.
      *
      * @return true/false
      */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
