package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.Arrays;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUSKURelationshipMapper. This class maps the relationships data stored in entities to repository to push the
 * data of properties and it is used to write the VO properties to the ProductCatalog repository.
 * @author Professional Access
 * @version 1.0
 */
public class TRUSKURelationshipMapper extends AbstractFeedItemMapper<TRUSkuFeedVO> {

	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Setting SKU relationship properties it is used to write the VO properties to the ProductCatalog repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUSkuFeedVO pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUSKURelationshipMapper, @method: map()");
		
		String method = TRUProductFeedConstants.SKU_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}

		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);

		if (pVOItem.getParentClassifications() != null && !pVOItem.getParentClassifications().isEmpty()) {
			String[] parentClassificationsArray = pVOItem.getParentClassifications().toArray(
					new String[pVOItem.getParentClassifications().size()]);
			RepositoryItem[] parentClassificationRepositoryItems = productCatalogRepository.getItems(
					parentClassificationsArray, getFeedCatalogProperty().getClassificationPropertyName());
			if (parentClassificationRepositoryItems != null &&
					parentClassificationRepositoryItems.length > TRUProductFeedConstants.NUMBER_ZERO) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(),
						Arrays.asList(parentClassificationRepositoryItems));
			}
		}

		if (!StringUtils.isBlank(pVOItem.getOrignalParentProduct().getId())) {
			RepositoryItem originalProductRepositoryItem = productCatalogRepository.getItem(pVOItem
					.getOrignalParentProduct().getId(), getFeedCatalogProperty().getProductName());
			if (originalProductRepositoryItem != null) {
				pRepoItem
						.setPropertyValue(getFeedCatalogProperty().getOrignalParentProduct(), originalProductRepositoryItem);
			}
		}

		getLogger().vlogDebug("End @Class: TRUSKURelationshipMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}

		return pRepoItem;
	}

}
