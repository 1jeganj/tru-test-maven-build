package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUCribSKUVO. This class extends TRUSkuFeedVO to set the entity name to CribSku.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCribSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU crib skuvo.
	 */
	public TRUCribSKUVO() {
		setEntityName(TRUProductFeedConstants.CRIB_SKU);
	}

}