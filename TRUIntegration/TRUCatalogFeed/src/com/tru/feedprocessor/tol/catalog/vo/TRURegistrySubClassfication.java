/*
 * 
 */
package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class TRURegistrySubClassfication.
 * It will hold all the properties required for the Classification of subcategory
 * @author Professional Access
 * @version 1.0
 */
public class TRURegistrySubClassfication {

	/** Property to hold  id. */
	private String mID;

	/** Property to hold  user type id. */
	private String mUserTypeID;

	/** Property to hold  name. */
	private String mName;

	/** Property to hold  display order. */
	private String mDisplayOrder;

	/** Property to hold  must or nice to have. */
	private String mMustOrNiceToHave;

	/** Property to hold  suggested quantity. */
	private String mSuggestedQty;

	/** Property to hold  cross reference id. */
	private List<String> mCrossReferenceId;

	/** Property to hold  display status. */
	private String mDisplayStatus;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getID() {
		return mID;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pID
	 *            the new id
	 */
	public void setID(String pID) {
		this.mID = pID;
	}

	/**
	 * Gets the user type id.
	 * 
	 * @return the user type id
	 */
	public String getUserTypeID() {
		return mUserTypeID;
	}

	/**
	 * Sets the user type id.
	 * 
	 * @param pUserTypeID
	 *            the new user type id
	 */
	public void setUserTypeID(String pUserTypeID) {
		this.mUserTypeID = pUserTypeID;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 * 
	 * @param pName
	 *            the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the display order.
	 * 
	 * @return the display order
	 */
	public String getDisplayOrder() {
		return mDisplayOrder;
	}

	/**
	 * Sets the display order.
	 * 
	 * @param pDisplayOrder
	 *            the new display order
	 */
	public void setDisplayOrder(String pDisplayOrder) {
		this.mDisplayOrder = pDisplayOrder;
	}

	/**
	 * Gets the must or nice to have.
	 * 
	 * @return the must or nice to have
	 */
	public String getMustOrNiceToHave() {
		return mMustOrNiceToHave;
	}

	/**
	 * Sets the must or nice to have.
	 * 
	 * @param pMustOrNiceToHave
	 *            the new must or nice to have
	 */
	public void setMustOrNiceToHave(String pMustOrNiceToHave) {
		this.mMustOrNiceToHave = pMustOrNiceToHave;
	}

	/**
	 * Gets the suggested quantity.
	 * 
	 * @return the suggested quantity
	 */
	public String getSuggestedQty() {
		return mSuggestedQty;
	}

	/**
	 * Sets the suggested quantity.
	 * 
	 * @param pSuggestedQty
	 *            the new suggested quantity
	 */
	public void setSuggestedQty(String pSuggestedQty) {
		this.mSuggestedQty = pSuggestedQty;
	}

	/**
	 * Gets the cross reference id.
	 * 
	 * @return the cross reference id
	 */
	public List<String> getCrossReferenceId() {
		if (mCrossReferenceId == null) {
			mCrossReferenceId = new ArrayList<String>();
		}
		return mCrossReferenceId;
	}

	/**
	 * Sets the cross reference id.
	 * 
	 * @param pCrossReferenceId
	 *            the new cross reference id
	 */
	public void setCrossReferenceId(List<String> pCrossReferenceId) {
		this.mCrossReferenceId = pCrossReferenceId;
	}

	/**
	 * Gets the display status.
	 * 
	 * @return the display status
	 */
	public String getDisplayStatus() {
		return mDisplayStatus;
	}

	/**
	 * Sets the display status.
	 * 
	 * @param pDisplayStatus
	 *            the new display status
	 */
	public void setDisplayStatus(String pDisplayStatus) {
		this.mDisplayStatus = pDisplayStatus;
	}

}
