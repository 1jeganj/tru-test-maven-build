package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.Arrays;
import java.util.List;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;

/**
 * The Class TRUProductRelationshipMapper.
 * This class maps the relationships data stored in entities to repository to push.
 * the data of properties and it is used to write the VO properties to the ProductCatalog repository.
 * 
 * @version 1.0.
 * @author Professional Access.
 */
public class TRUCategoryRelationshipMapper extends AbstractFeedItemMapper<TRUCategory> {

	/**
	 * The variable to hold "FeedCatalogProperty" property name.
	 */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the feedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            the feedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Setting common Product Relationship properties it is used to write the VO properties to the ProductCatalog repository.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MutableRepositoryItem map(TRUCategory pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		// DO Customization for Product Relationship mapping here...

		getLogger().vlogDebug("Start @Class: TRUCategoryRelationshipMapper, @method: map()");
		
		String method = TRUProductFeedConstants.CATEGORY_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		getLogger().vlogDebug("Updating relationships for Category : " + pVOItem.getID());

		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);

		String[] childTRUCategoriesArray = pVOItem.getFixedChildCategories().toArray(
				new String[pVOItem.getFixedChildCategories().size()]);
		String[] childTRUProductsArray = pVOItem.getTRUFixedChildProducts().toArray(
				new String[pVOItem.getTRUFixedChildProducts().size()]);
		String[] childBRUProductsArray = pVOItem.getBRUFixedChildProducts().toArray(
				new String[pVOItem.getBRUFixedChildProducts().size()]);

		RepositoryItem[] childCategoryRepositoryItems = productCatalogRepository.getItems(childTRUCategoriesArray,
				getFeedCatalogProperty().getClassificationPropertyName());
		RepositoryItem[] truproductRepositoryItems = productCatalogRepository.getItems(childTRUProductsArray,
				TRUProductFeedConstants.PRODUCT);
		RepositoryItem[] bruproductRepositoryItems = productCatalogRepository.getItems(childBRUProductsArray,
				TRUProductFeedConstants.PRODUCT);

		if (childCategoryRepositoryItems != null && childCategoryRepositoryItems.length > 0) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName(),
					Arrays.asList(childCategoryRepositoryItems));
		}
		
		
		
		if (truproductRepositoryItems != null && truproductRepositoryItems.length > 0 && pRepoItem.getRepositoryId().
				equals(TRUFeedConstants.NAVIGATION_TRU) && pRepoItem.getPropertyValue(getFeedCatalogProperty().getFixedChildProductsName()) != null) {
				List<RepositoryItem> productItemList = (List<RepositoryItem>) pRepoItem.
						getPropertyValue(getFeedCatalogProperty().getFixedChildProductsName());
				for (RepositoryItem repositoryItem : truproductRepositoryItems) {
						productItemList.add(repositoryItem);
				}
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getFixedChildProductsName(), productItemList);
		}
		if (bruproductRepositoryItems != null && bruproductRepositoryItems.length > 0 && pRepoItem.getRepositoryId().
				equals(TRUFeedConstants.NAVIGATION_BRU) && pRepoItem.getPropertyValue(getFeedCatalogProperty().getFixedChildProductsName()) != null) {
				List<RepositoryItem> productItemList = (List<RepositoryItem>) pRepoItem.
						getPropertyValue(getFeedCatalogProperty().getFixedChildProductsName());
				for (RepositoryItem repositoryItem : bruproductRepositoryItems) {
						productItemList.add(repositoryItem);
				}
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getFixedChildProductsName(), productItemList);
		}


		getLogger().vlogDebug("End @Class: TRUCategoryRelationshipMapper, @method: map()");
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}

		return pRepoItem;
	}

}
