package com.tru.feedprocessor.tol.catalog.processor;

import java.util.List;
import java.util.Map;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValueChangedInspector;
import com.tru.feedprocessor.tol.catalog.vo.TRUCarSeatSKUVO;

/**
 * The Class TRUCarSeatSKUItemValueChangedInspector. This class inspects each and every property holds in entites to
 * repository of SKU properties and CarSeatSKU properties has any changes. If any change occurs in one property entire entity
 * will be updated.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCarSeatSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold CommonSKUItemValueChangedInspector holder. */
	private TRUCommonSKUItemValueChangedInspector mCommonSKUItemValueChangedInspector;

	/**
	 * Gets the common sku item value changed inspector.
	 * 
	 * @return the commonSKUItemValueChangedInspector
	 */
	public TRUCommonSKUItemValueChangedInspector getCommonSKUItemValueChangedInspector() {
		return mCommonSKUItemValueChangedInspector;
	}

	/**
	 * Sets the common sku item value changed inspector.
	 * 
	 * @param pCommonSKUItemValueChangedInspector
	 *            the commonSKUItemValueChangedInspector to set
	 */
	public void setCommonSKUItemValueChangedInspector(
			TRUCommonSKUItemValueChangedInspector pCommonSKUItemValueChangedInspector) {
		mCommonSKUItemValueChangedInspector = pCommonSKUItemValueChangedInspector;
	}

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the SKU properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUBookCdDvDSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUCarSeatSKUVO) {
			TRUCarSeatSKUVO skuVO = (TRUCarSeatSKUVO) pBaseFeedProcessVO;
			Map<String, String> carSeatPropertiesMap = (Map<String, String>) pRepositoryItem
					.getPropertyValue(getFeedCatalogProperty().getCarSeatPropertiesMap());
			if (getCommonSKUItemValueChangedInspector().isUpdated(skuVO, pRepositoryItem)) {
				return retVal = Boolean.TRUE;
			}
			
			if(skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_FEATURES)){
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatFeatures()) == null) {
				return retVal = Boolean.TRUE;
				} else {
					for (String carSeatFeatures : skuVO.getCarSeatFeatures()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatFeatures()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatFeatures())).contains(carSeatFeatures)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if(skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_TYPE)){
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatTypes()) == null) {
				return retVal = Boolean.TRUE;
				} else {
					for (String carSeatTypes : skuVO.getCarSeatTypes()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatTypes()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatTypes())).contains(carSeatTypes)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			if(skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CAR_SEAT_WHAT_IS_IMP)){
				if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatWhatIsImp()) == null) {
				return retVal = Boolean.TRUE;
				} else {
					for (String carSeatWhatisImp : skuVO.getCarSeatWhatIsImp()) {
						if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatWhatIsImp()) != null
								&& !((List<String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCarSeatWhatIsImp()))
										.contains(carSeatWhatisImp)) {
							return retVal = Boolean.TRUE;
						}
					}
				}
			}
			
			if (!skuVO.getCarSeatPropertiesMap().isEmpty()
					&& !getCommonSKUItemValueChangedInspector().mapsAreEqual(skuVO.getCarSeatPropertiesMap(),
							carSeatPropertiesMap)) {
				return retVal = Boolean.TRUE;
			}

		}

		getLogger().vlogDebug("End @Class: TRUBookCdDvDSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
}
