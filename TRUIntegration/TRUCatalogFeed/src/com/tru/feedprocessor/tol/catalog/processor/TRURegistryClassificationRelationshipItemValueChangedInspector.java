package com.tru.feedprocessor.tol.catalog.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;

/**
 * The Class TRURegistryClassificationRelationshipItemValueChangedInspector. This class inspects each and every property holds in entites to repository of
 * Classification has any changes
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRURegistryClassificationRelationshipItemValueChangedInspector implements IItemValueChangeInspector {

	/** The Feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * The method is overridden to check whether properties are modified or not of the current vo to the repository item .
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRURegistryClassificationRelationshipItemValueChangedInspector, @method: isUpdated()");
		
		boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUClassification) {
			TRUClassification classificationVO = (TRUClassification) pBaseFeedProcessVO;
			if(StringUtils.isBlank(classificationVO.getErrorMessage())){
				Set<String> taxonomyChildClassificationSet = getTaxonomyChildClassificationArray(classificationVO);
				if(!taxonomyChildClassificationSet.isEmpty() && pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName()) != null){
					Set<String> repoIds = new HashSet<String>();
					List<RepositoryItem> DBFixedChildCategories = (List<RepositoryItem>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName());
					for(RepositoryItem repo: DBFixedChildCategories){ 
						repoIds.add(repo.getRepositoryId());
					} 
					if (!repoIds.containsAll(taxonomyChildClassificationSet)
							|| (repoIds != null && taxonomyChildClassificationSet != null && repoIds.size() != taxonomyChildClassificationSet.size())) {
						retVal = Boolean.TRUE;
					} 
				} 
				retVal = checkCrossReferences(pRepositoryItem, retVal, classificationVO);
			}
		}
  
		getLogger().vlogDebug("End @Class: TRU RegistryClassificationRelationshipItemValueChangedInspector, @method: isUpdated()");
		
		return retVal;


	}

	/**
	 * Check cross references.
	 *
	 * @param pRepositoryItem the repository item
	 * @param pRetVal the ret val
	 * @param pClassificationVO the classification vo
	 * @return true, if successful
	 */
	private boolean checkCrossReferences(RepositoryItem pRepositoryItem, boolean pRetVal, TRUClassification pClassificationVO) {
		boolean retVal = pRetVal;
		List<RepositoryItem> crossRefsDB = (List<RepositoryItem>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
				.getCrossReferencePropertyName());
		List<String> crossRefIdFromDB = new ArrayList<String>();
		for(RepositoryItem repo: crossRefsDB){ 
			crossRefIdFromDB.add(repo.getRepositoryId()); 
		} 
		 
		List<String> crossRefVOs = pClassificationVO.getRegistryCrossReferenceIds();
		if (!crossRefIdFromDB.equals(crossRefVOs)) {
			retVal = Boolean.TRUE;
		}

		return retVal;
	}
	
	
	/**
	 * This method is used to get the child classification.
	 * 
	 * @param pVOItem - VO Item
	 * @return childCategory - child Category
	 */
	private Set<String> getTaxonomyChildClassificationArray(TRUClassification pVOItem){
		getLogger().vlogDebug("Begin @Class: TRURegistryClassificationRelationshipItemValueChangedInspector, @method: getTaxonomyChildClassificationArray()");
		Set<String> childCategory = new HashSet<String>();
		if(pVOItem != null && pVOItem.getTRURegistrySubClassfication() != null) {
			childCategory.addAll(pVOItem.getTRURegistrySubClassfication());
		}
		getLogger().vlogDebug("End @Class: TRURegistryClassificationRelationshipItemValueChangedInspector, @method: getTaxonomyChildClassificationArray()");
		return childCategory;
	}

}
