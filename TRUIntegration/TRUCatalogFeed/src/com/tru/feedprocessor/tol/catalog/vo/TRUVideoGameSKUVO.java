package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUVideoGameSKUVO.
 * 
 * This class extends TRUSkuFeedVO to set the entity name to VideoGameSKU.
 * @author Professional Access
 * @version 1.0
 */
public class TRUVideoGameSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU video game skuvo.
	 */
	public TRUVideoGameSKUVO() {
		setEntityName(TRUProductFeedConstants.VIDEOGAME_SKU);
	}

}