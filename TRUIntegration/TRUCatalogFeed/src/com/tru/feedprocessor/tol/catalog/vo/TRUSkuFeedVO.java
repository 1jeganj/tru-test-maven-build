package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUSkuFeedVO.
 * 
 * VO class which will store all the properties related to regularSKU item
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUSkuFeedVO extends BaseFeedProcessVO {

	/** property to hold Id. */
	private String mId;

	/** property to hold SKUType. */
	private String mSKUType;

	/** property to hold OnlineTitle. */
	private String mOnlineTitle;

	/** property to hold OnlineLongDesc. */
	private String mOnlineLongDesc;

	/** property to hold AkhiMap. */
	private Map<String, String> mAkhiMap;

	/** property to hold AssemblyMap. */
	private Map<String, String> mAssemblyMap;

	/** property to hold BackorderStatus. */
	private String mBackorderStatus;

	/** property to hold BrandNameSecondary. */
	private List<String> mBrandNameSecondary;

	/** property to hold BrandnametertiaryCharacterTheme. */
	private List<String> mBrandnametertiaryCharacterTheme;

	/** property to hold BrowseHiddenKeyword. */
	private String mBrowseHiddenKeyword;

	/** property to hold CanBeGiftWrapped. */
	private String mCanBeGiftWrapped;

	/** property to hold CarSeatFeatures. */
	private List<String> mCarSeatFeatures;

	/** property to hold CarSeatTypes. */
	private List<String> mCarSeatTypes;

	/** property to hold CarSeatWhatIsImp. */
	private List<String> mCarSeatWhatIsImp;

	/** property to hold ChildWeightMax. */
	private String mChildWeightMax;

	/** property to hold ChildWeightMin. */
	private String mChildWeightMin;

	/** property to hold ColorCode. */
	private String mColorCode;

	/** property to hold ColorUpcLevel. */
	private String mColorUpcLevel;

	/** property to hold CribMaterialTypes. */
	private List<String> mCribMaterialTypes;

	/** property to hold CribStyles. */
	private List<String> mCribStyles;

	/** property to hold CribTypes. */
	private List<String> mCribTypes;

	/** property to hold CribWhatIsImp. */
	private List<String> mCribWhatIsImp;

	/** property to hold CustomerPurchaseLimit. */
	private String mCustomerPurchaseLimit;

	/** property to hold BookCdDvdPropertiesMap. */
	private Map<String, String> mBookCdDvdPropertiesMap;

	/** property to hold DropshipFlag. */
	private String mDropshipFlag;

	/** property to hold FlexibleShippingPlan. */
	private String mFlexibleShippingPlan;

	/** property to hold FreightClass. */
	private String mFreightClass;

	/** property to hold FurnitureFinish. */
	private String mFurnitureFinish;

	/** property to hold IncrementalSafetyStockUnits. */
	private String mIncrementalSafetyStockUnits;

	/** property to hold ItemDimensionMap. */
	private Map<String, String> mItemDimensionMap;

	/** property to hold JuvenileProductSize. */
	private String mJuvenileProductSize;

	/** property to hold Lower48Map. */
	private Map<String, String> mLower48Map;

	/** property to hold MaxTargetAgeTruWebsites. */
	private String mMaxTargetAgeTruWebsites;

	/** property to hold MfrSuggestedAgeMax. */
	private String mMfrSuggestedAgeMax;

	/** property to hold MfrSuggestedAgeMin. */
	private String mMfrSuggestedAgeMin;

	/** property to hold MinTargetAgeTruWebsites. */
	private String mMinTargetAgeTruWebsites;

	/** property to hold NmwaPercentage. */
	private String mNmwaPercentage;

	/** property to hold OnlineCollectionName. */
	private Map<String, String> mOnlineCollectionName;

	/** property to hold ShipWindowMax. */
	private String mShipWindowMax;

	/** property to hold ShipWindowMin. */
	private String mShipWindowMin;

	/** property to hold OnlinePID. */
	private String mOnlinePID;

	/** property to hold OriginalPID. */
	private String mOriginalPID;

	/** property to hold StreetDate. */
	private String mStreetDate;

	/** property to hold OtherFixedSurcharge. */
	private String mOtherFixedSurcharge;

	/** property to hold OtherStdFSDollar. */
	private String mOtherStdFSDollar;

	/** property to hold Outlet. */
	private String mOutlet;

	/** property to hold PresellQuantityUnits. */
	private String mPresellQuantityUnits;

	/** property to hold PriceDisplay. */
	private String mPriceDisplay;

	/** property to hold ProductAwards. */
	private String mProductAwards;

	/** property to hold ProductFeature. */
	private Map<String, String> mProductFeature;

	/** property to hold ProductSafetyWarnings. */
	private List<String> mProductSafetyWarnings;

	/** property to hold PromotionalStickerDisplay. */
	private String mPromotionalStickerDisplay;

	/** property to hold RegistryWarningIndicator. */
	private String mRegistryWarningIndicator;

	/** property to hold RegistryEligibility. */
	private String mRegistryEligibility;

	/** property to hold SafetyStock. */
	private String mSafetyStock;

	/** property to hold ShipInOrigContainer. */
	private String mShipInOrigContainer;

	/** property to hold StrlBestUses. */
	private List<String> mStrlBestUses;

	/** property to hold StrlExtras. */
	private List<String> mStrlExtras;

	/** property to hold StrlTypes. */
	private List<String> mStrlTypes;

	/** property to hold StrlWhatIsImp. */
	private List<String> mStrlWhatIsImp;

	/** property to hold SupressInSearch. */
	private String mSupressInSearch;

	/** property to hold SystemRequirements. */
	private String mSystemRequirements;

	/** property to hold UpcNumber. */
	private Set<String> mUpcNumbers;

	/** property to hold VideoGameEsrb. */
	private String mVideoGameEsrb;

	/** property to hold VideoGameGenre. */
	private String mVideoGameGenre;

	/** property to hold VideoGamePlatform. */
	private String mVideoGamePlatform;

	/** property to hold Nmwa. */
	private String mNmwa;

	/** property to hold WebDisplayFlag. */
	private String mWebDisplayFlag;

	/** property to hold Interest. */
	private List<String> mInterest;

	/** property to hold BppEligible;. */
	private Boolean mBppEligible;

	/** property to hold BppName. */
	private String mBppName;

	/** property to hold LegalNotice. */
	private String mLegalNotice;

	/** property to hold SwatchImage. */
	private String mSwatchImage;

	/** property to hold PrimaryImage. */
	private String mPrimaryImage;

	/** property to hold SecondaryImage. */
	private String mSecondaryImage;

	/** property to hold AlternateImage. */
	private Set<String> mAlternateImage;
	
	/** property to hold SwatchCanonicalImage. */
	private String mSwatchCanonicalImage;

	/** property to hold PrimaryCanonicalImage. */
	private String mPrimaryCanonicalImage;

	/** property to hold SecondaryCanonicalImage. */
	private String mSecondaryCanonicalImage;

	/** property to hold AlternateCanonicalImage. */
	private Set<String> mAlternateCanonicalImage;

	/** property to hold CrossSellsnUidBatteriesList. */
	private List<TRUCrossSellsnUidBatteriesVO> mCrossSellsnUidBatteriesList;

	/** property to hold CrossSellSkuId. */
	private List<String> mCrossSellSkuId;

	/** property to hold SlapperItem. */
	private String mSlapperItem;

	/** property to hold ShipToStoreEeligible. */
	private String mShipToStoreEeligible;

	/** property to hold SuperDisplayFlag. */
	private String mSuperDisplayFlag;

	/** property to hold ReviewRating. */
	private String mReviewRating;

	/** property to hold ReviewCount. */
	private String mReviewCount;

	/** property to hold ReviewCountDecimal. */
	private String mReviewCountDecimal;

	/** property to hold SalesVolume. */
	private String mSalesVolume;

	/** property to hold RootParentCategories. */
	private Set<String> mRootParentCategories;

	/** property to hold ParentClassifications. */
	private List<String> mParentClassifications;

	/** property to hold CarSeatPropertiesMap. */
	private Map<String, String> mCarSeatPropertiesMap;

	/** property to hold StrollerPropertiesMap. */
	private Map<String, String> mStrollerPropertiesMap;

	/** property to hold CribPropertiesMap. */
	private Map<String, String> mCribPropertiesMap;

	/** property to hold OrignalParentProduct. */
	private TRUProductFeedVO mOrignalParentProduct;

	/** property to hold ShipFromStoreEligibleLov. */
	private String mShipFromStoreEligibleLov;

	/** property to hold Feed. */
	private String mFeed;

	/** property to hold FeedActionCode. */
	private String mFeedActionCode;

	/** property to hold IsDeleted. */
	private String mIsDeleted;

	/** property to hold SubType. */
	private String mSubType;

	/** The mInventoryFlag. */
	private Boolean mInventoryFlag;
	
	/** The empty & null properties. */
	private List<String> mEmptyNullProperties;
	
	/** The Empty sku properties flag. */
	private Boolean mEmptySkuPropertiesFlag = Boolean.FALSE;
	
	/** The empty & null properties. */
	private List<String> mEmptyNullSubProperties;
	
	/** property to hold VendorsProductDemoUrl. */
	private String mVendorsProductDemoUrl;
	
	/** property to DivisionCode Type. */
	private String mDivisionCode;
	
	/** property to DepartmentCode Type. */
	private String mDepartmentCode;
	
	/** property to ClassCode Type. */
	private String mClassCode;
	
	/** property to SubclassCode Type. */
	private String mSubclassCode;
	
	/** property to Source Type. */
	private String mSource;
	
	/** property to ErrorMessage Type. */
	private String mErrorMessage;
	
	/** property to Updated List Properties. */
	private List<String> mUpdatedListProps;
	
	/** property to hold FinderEligible. */
	private List<String> mFinderEligible;
	
	/** property to hold SpecialFeatures. */
	private List<String> mSpecialFeatures;
	
	/** property to hold mSequenceNumber. */
	private String mSequenceNumber;

	/** The Inventory received date. */
	private String mInventoryReceivedDate;
	
	/** The variable holds RmsSizeCode property name. */
	private String mRmsSizeCode;
	
	/** The variable holds RmsColorCode property name. */
	private String mRmsColorCode;
	
	/** The variable holds Skills property name. */
	private String mSkills;
	
	/** The variable holds PrimaryCategory property name. */
	private String mPrimaryCategory;
	
	/** The variable holds PlayType property name. */
	private String mPlayType;
	
	/** The variable holds WhatIsImportant property name. */
	private String mWhatIsImportant;
	
	/** property to hold BrandNamePrimary. */
	private String mBrandNamePrimary;
	
	/** property to hold ChannelAvailability. */
	private String mChannelAvailability;
	
	/** property to hold PrimaryPathCategory. */
	private String mPrimaryPathCategory;
	
	/** property to hold FileName . */
	private String mFileName;
	
	/** The color family. */
	private String mColorFamily;
	
	/** The size family. */
	private List<String> mSizeFamily;
	
	/** The character theme. */
	private List<String> mCharacterTheme;
	
	/** The collection theme. */
	private List<String> mCollectionTheme;
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}
	
	/**
	 * Gets the item in store pick up.
	 * 
	 * @return the itemInStorePickUp
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 * 
	 * @param pItemInStorePickUp
	 *            the itemInStorePickUp to set
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		mItemInStorePickUp = pItemInStorePickUp;
	}
	
	/** property to hold ItemInStorePickUp. */
	private String mItemInStorePickUp;
	
	/**
	 * Gets the channel availability.
	 * 
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * Sets the channel availability.
	 * 
	 * @param pChannelAvailability
	 *            the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}
	
	/**
	 * Gets the brand name primary.
	 * 
	 * @return the brandNamePrimary
	 */
	public String getBrandNamePrimary() {
		return mBrandNamePrimary;
	}

	/**
	 * Sets the brand name primary.
	 * 
	 * @param pBrandNamePrimary
	 *            the brandNamePrimary to set
	 */
	public void setBrandNamePrimary(String pBrandNamePrimary) {
		mBrandNamePrimary = pBrandNamePrimary;
	}
	
	
	/**
	 * Gets the skills.
	 *
	 * @return the skills
	 */
	public String getSkills() {
		return mSkills;
	}

	/**
	 * Sets the skills.
	 *
	 * @param pSkills the skills to set
	 */
	public void setSkills(String pSkills) {
		mSkills = pSkills;
	}

	/**
	 * Gets the primary category.
	 *
	 * @return the primaryCategory
	 */
	public String getPrimaryCategory() {
		return mPrimaryCategory;
	}

	/**
	 * Sets the primary category.
	 *
	 * @param pPrimaryCategory the primaryCategory to set
	 */
	public void setPrimaryCategory(String pPrimaryCategory) {
		mPrimaryCategory = pPrimaryCategory;
	}

	/**
	 * Gets the play type.
	 *
	 * @return the playType
	 */
	public String getPlayType() {
		return mPlayType;
	}

	/**
	 * Sets the play type.
	 *
	 * @param pPlayType the playType to set
	 */
	public void setPlayType(String pPlayType) {
		mPlayType = pPlayType;
	}

	/**
	 * Gets the what is important.
	 *
	 * @return the whatIsImportant
	 */
	public String getWhatIsImportant() {
		return mWhatIsImportant;
	}

	/**
	 * Sets the what is important.
	 *
	 * @param pWhatIsImportant the whatIsImportant to set
	 */
	public void setWhatIsImportant(String pWhatIsImportant) {
		mWhatIsImportant = pWhatIsImportant;
	}

	/**
	 * Gets the swatch canonical image.
	 *
	 * @return the swatchCanonicalImage
	 */
	public String getSwatchCanonicalImage() {
		return mSwatchCanonicalImage;
	}

	/**
	 * Sets the swatch canonical image.
	 *
	 * @param pSwatchCanonicalImage the swatchCanonicalImage to set
	 */
	public void setSwatchCanonicalImage(String pSwatchCanonicalImage) {
		mSwatchCanonicalImage = pSwatchCanonicalImage;
	}

	/**
	 * Gets the primary canonical image.
	 *
	 * @return the primaryCanonicalImage
	 */
	public String getPrimaryCanonicalImage() {
		return mPrimaryCanonicalImage;
	}

	/**
	 * Sets the primary canonical image.
	 *
	 * @param pPrimaryCanonicalImage the primaryCanonicalImage to set
	 */
	public void setPrimaryCanonicalImage(String pPrimaryCanonicalImage) {
		mPrimaryCanonicalImage = pPrimaryCanonicalImage;
	}

	/**
	 * Gets the secondary canonical image.
	 *
	 * @return the secondaryCanonicalImage
	 */
	public String getSecondaryCanonicalImage() {
		return mSecondaryCanonicalImage;
	}

	/**
	 * Sets the secondary canonical image.
	 *
	 * @param pSecondaryCanonicalImage the secondaryCanonicalImage to set
	 */
	public void setSecondaryCanonicalImage(String pSecondaryCanonicalImage) {
		mSecondaryCanonicalImage = pSecondaryCanonicalImage;
	}

	/**
	 * Gets the alternate canonical image.
	 *
	 * @return the alternateCanonicalImage
	 */
	public Set<String> getAlternateCanonicalImage() {
		if (mAlternateCanonicalImage == null) {
			mAlternateCanonicalImage = new HashSet<String>();
		}
		return mAlternateCanonicalImage;
	}

	/**
	 * Sets the alternate canonical image.
	 *
	 * @param pAlternateCanonicalImage the alternateCanonicalImage to set
	 */
	public void setAlternateCanonicalImage(Set<String> pAlternateCanonicalImage) {
		mAlternateCanonicalImage = pAlternateCanonicalImage;
	}

	/**
	 * Gets the rms size code.
	 *
	 * @return the rmsSizeCode
	 */
	public String getRmsSizeCode() {
		return mRmsSizeCode;
	}

	/**
	 * Sets the rms size code.
	 *
	 * @param pRmsSizeCode the rmsSizeCode to set
	 */
	public void setRmsSizeCode(String pRmsSizeCode) {
		mRmsSizeCode = pRmsSizeCode;
	}

	/**
	 * Gets the rms color code.
	 *
	 * @return the rmsColorCode
	 */
	public String getRmsColorCode() {
		return mRmsColorCode;
	}

	/**
	 * Sets the rms color code.
	 *
	 * @param pRmsColorCode the rmsColorCode to set
	 */
	public void setRmsColorCode(String pRmsColorCode) {
		mRmsColorCode = pRmsColorCode;
	}
	

	/**
	 * Instantiates a new TRU sku feed vo.
	 */
	public TRUSkuFeedVO() {
		setEntityName(TRUProductFeedConstants.REGULAR_SKU);
	}
	
	/**
	 * Gets the finder eligible.
	 *
	 * @return the finderEligible
	 */
	public List<String> getFinderEligible() {
		if(mFinderEligible == null){
			mFinderEligible = new ArrayList<String>();
		}
		return mFinderEligible;
	}

	/**
	 * Sets the finder eligible.
	 *
	 * @param pFinderEligible the finderEligible to set
	 */
	public void setFinderEligible(List<String> pFinderEligible) {
		mFinderEligible = pFinderEligible;
	}

	/**
	 * Gets the special features.
	 *
	 * @return the specialFeatures
	 */
	public List<String> getSpecialFeatures() {
		if(mSpecialFeatures == null){
			mSpecialFeatures = new ArrayList<String>();
		}
		return mSpecialFeatures;
	}

	/**
	 * Sets the special features.
	 *
	 * @param pSpecialFeatures the specialFeatures to set
	 */
	public void setSpecialFeatures(List<String> pSpecialFeatures) {
		mSpecialFeatures = pSpecialFeatures;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the errorMessage to set
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}
	
	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource() {
		return mSource;
	}

	/**
	 * Sets the source.
	 *
	 * @param pSource the source to set
	 */
	public void setSource(String pSource) {
		mSource = pSource;
	}
	
	/**
	 * Gets the division code.
	 *
	 * @return the divisionCode
	 */
	public String getDivisionCode() {
		return mDivisionCode;
	}

	/**
	 * Sets the division code.
	 *
	 * @param pDivisionCode the divisionCode to set
	 */
	public void setDivisionCode(String pDivisionCode) {
		mDivisionCode = pDivisionCode;
	}

	/**
	 * Gets the department code.
	 *
	 * @return the departmentCode
	 */
	public String getDepartmentCode() {
		return mDepartmentCode;
	}

	/**
	 * Sets the department code.
	 *
	 * @param pDepartmentCode the departmentCode to set
	 */
	public void setDepartmentCode(String pDepartmentCode) {
		mDepartmentCode = pDepartmentCode;
	}

	/**
	 * Gets the class code.
	 *
	 * @return the classCode
	 */
	public String getClassCode() {
		return mClassCode;
	}

	/**
	 * Sets the class code.
	 *
	 * @param pClassCode the classCode to set
	 */
	public void setClassCode(String pClassCode) {
		mClassCode = pClassCode;
	}

	/**
	 * Gets the subclass code.
	 *
	 * @return the subclassCode
	 */
	public String getSubclassCode() {
		return mSubclassCode;
	}

	/**
	 * Sets the subclass code.
	 *
	 * @param pSubclassCode the subclassCode to set
	 */
	public void setSubclassCode(String pSubclassCode) {
		mSubclassCode = pSubclassCode;
	}

	/**
	 * Gets the inventory flag.
	 * 
	 * @return the inventoryFlag
	 */
	public Boolean getInventoryFlag() {
		return mInventoryFlag;
	}

	/**
	 * Sets the inventory flag.
	 * 
	 * @param pInventoryFlag
	 *            the inventoryFlag to set
	 */
	public void setInventoryFlag(Boolean pInventoryFlag) {
		mInventoryFlag = pInventoryFlag;
	}

	/**
	 * Gets the sub type.
	 * 
	 * @return the subType
	 */
	public String getSubType() {
		return mSubType;
	}

	/**
	 * Sets the sub type.
	 * 
	 * @param pSubType
	 *            the subType to set
	 */
	public void setSubType(String pSubType) {
		mSubType = pSubType;
	}

	/**
	 * Gets the checks if is deleted.
	 * 
	 * @return the isDeleted
	 */
	public String getIsDeleted() {
		return mIsDeleted;
	}

	/**
	 * Sets the checks if is deleted.
	 * 
	 * @param pIsDeleted
	 *            the isDeleted to set
	 */
	public void setIsDeleted(String pIsDeleted) {
		mIsDeleted = pIsDeleted;
	}

	/**
	 * Gets the feed action code.
	 * 
	 * @return the feedActionCode
	 */
	public String getFeedActionCode() {
		return mFeedActionCode;
	}

	/**
	 * Sets the feed action code.
	 * 
	 * @param pFeedActionCode
	 *            the feedActionCode to set
	 */
	public void setFeedActionCode(String pFeedActionCode) {
		mFeedActionCode = pFeedActionCode;
	}

	/**
	 * Gets the feed.
	 * 
	 * @return the feed
	 */
	public String getFeed() {
		return mFeed;
	}

	/**
	 * Sets the feed.
	 * 
	 * @param pFeed
	 *            the feed to set
	 */
	public void setFeed(String pFeed) {
		mFeed = pFeed;
	}

	/**
	 * Gets the review count decimal.
	 * 
	 * @return the reviewCountDecimal
	 */
	public String getReviewCountDecimal() {
		return mReviewCountDecimal;
	}

	/**
	 * Sets the review count decimal.
	 * 
	 * @param pReviewCountDecimal
	 *            the reviewCountDecimal to set
	 */
	public void setReviewCountDecimal(String pReviewCountDecimal) {
		mReviewCountDecimal = pReviewCountDecimal;
	}

	/**
	 * Gets the sales volume.
	 * 
	 * @return the salesVolume
	 */
	public String getSalesVolume() {
		return mSalesVolume;
	}

	/**
	 * Sets the sales volume.
	 * 
	 * @param pSalesVolume
	 *            the salesVolume to set
	 */
	public void setSalesVolume(String pSalesVolume) {
		mSalesVolume = pSalesVolume;
	}

	/**
	 * Gets the SKU type.
	 * 
	 * @return the sKUType
	 */
	public String getSKUType() {
		return mSKUType;
	}

	/**
	 * Sets the SKU type.
	 * 
	 * @param pSKUType
	 *            the sKUType to set
	 */
	public void setSKUType(String pSKUType) {
		mSKUType = pSKUType;
	}

	/**
	 * Gets the online collection name.
	 * 
	 * @return the onlineCollectionName
	 */
	public Map<String, String> getOnlineCollectionName() {
		if(mOnlineCollectionName == null){
			mOnlineCollectionName = new HashMap<String, String>();
		}
		return mOnlineCollectionName;
	}

	/**
	 * Sets the online collection name.
	 * 
	 * @param pOnlineCollectionName
	 *            the onlineCollectionName to set
	 */
	public void setOnlineCollectionName(Map<String, String> pOnlineCollectionName) {
		mOnlineCollectionName = pOnlineCollectionName;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * Gets the online title.
	 * 
	 * @return the onlineTitle
	 */
	public String getOnlineTitle() {
		return mOnlineTitle;
	}

	/**
	 * Sets the online title.
	 * 
	 * @param pOnlineTitle
	 *            the onlineTitle to set
	 */
	public void setOnlineTitle(String pOnlineTitle) {
		mOnlineTitle = pOnlineTitle;
	}

	/**
	 * Gets the online long desc.
	 * 
	 * @return the onlineLongDesc
	 */
	public String getOnlineLongDesc() {
		return mOnlineLongDesc;
	}

	/**
	 * Sets the online long desc.
	 * 
	 * @param pOnlineLongDesc
	 *            the onlineLongDesc to set
	 */
	public void setOnlineLongDesc(String pOnlineLongDesc) {
		mOnlineLongDesc = pOnlineLongDesc;
	}

	/**
	 * Gets the akhi map.
	 * 
	 * @return the akhiMap
	 */
	public Map<String, String> getAkhiMap() {
		if (mAkhiMap == null) {
			mAkhiMap = new HashMap<String, String>();
		}
		return mAkhiMap;
	}

	/**
	 * Sets the akhi map.
	 * 
	 * @param pAkhiMap
	 *            the akhiMap to set
	 */
	public void setAkhiMap(Map<String, String> pAkhiMap) {
		mAkhiMap = pAkhiMap;
	}

	/**
	 * Gets the assembly map.
	 * 
	 * @return the assemblyMap
	 */
	public Map<String, String> getAssemblyMap() {
		if (mAssemblyMap == null) {
			mAssemblyMap = new HashMap<String, String>();
		}
		return mAssemblyMap;
	}

	/**
	 * Sets the assembly map.
	 * 
	 * @param pAssemblyMap
	 *            the assemblyMap to set
	 */
	public void setAssemblyMap(Map<String, String> pAssemblyMap) {
		mAssemblyMap = pAssemblyMap;
	}

	/**
	 * Gets the backorder status.
	 * 
	 * @return the backorderStatus
	 */
	public String getBackorderStatus() {
		return mBackorderStatus;
	}

	/**
	 * Sets the backorder status.
	 * 
	 * @param pBackorderStatus
	 *            the backorderStatus to set
	 */
	public void setBackorderStatus(String pBackorderStatus) {
		mBackorderStatus = pBackorderStatus;
	}

	/**
	 * Gets the brand name secondary.
	 * 
	 * @return the brandNameSecondary
	 */
	public List<String> getBrandNameSecondary() {
		if (mBrandNameSecondary == null) {
			mBrandNameSecondary = new ArrayList<String>();
		}
		return mBrandNameSecondary;
	}

	/**
	 * Sets the brand name secondary.
	 * 
	 * @param pBrandNameSecondary
	 *            the brandNameSecondary to set
	 */
	public void setBrandNameSecondary(List<String> pBrandNameSecondary) {
		mBrandNameSecondary = pBrandNameSecondary;
	}

	/**
	 * Gets the brandnametertiary character theme.
	 * 
	 * @return the brandnametertiaryCharacterTheme
	 */
	public List<String> getBrandnametertiaryCharacterTheme() {
		if (mBrandnametertiaryCharacterTheme == null) {
			mBrandnametertiaryCharacterTheme = new ArrayList<String>();
		}
		return mBrandnametertiaryCharacterTheme;
	}

	/**
	 * Sets the brandnametertiary character theme.
	 * 
	 * @param pBrandnametertiaryCharacterTheme
	 *            the brandnametertiaryCharacterTheme to set
	 */
	public void setBrandnametertiaryCharacterTheme(List<String> pBrandnametertiaryCharacterTheme) {
		mBrandnametertiaryCharacterTheme = pBrandnametertiaryCharacterTheme;
	}

	/**
	 * Gets the browse hidden keyword.
	 * 
	 * @return the browseHiddenKeyword
	 */
	public String getBrowseHiddenKeyword() {
		return mBrowseHiddenKeyword;
	}

	/**
	 * Gets the interest.
	 * 
	 * @return the interest
	 */
	public List<String> getInterest() {
		if (mInterest == null) {
			mInterest = new ArrayList<String>();
		}
		return mInterest;
	}

	/**
	 * Sets the interest.
	 * 
	 * @param pInterest
	 *            the interest to set
	 */
	public void setInterest(List<String> pInterest) {
		mInterest = pInterest;
	}

	/**
	 * Sets the browse hidden keyword.
	 * 
	 * @param pBrowseHiddenKeyword
	 *            the browseHiddenKeyword to set
	 */
	public void setBrowseHiddenKeyword(String pBrowseHiddenKeyword) {
		mBrowseHiddenKeyword = pBrowseHiddenKeyword;
	}

	/**
	 * Gets the can be gift wrapped.
	 * 
	 * @return the canBeGiftWrapped
	 */
	public String getCanBeGiftWrapped() {
		return mCanBeGiftWrapped;
	}

	/**
	 * Sets the can be gift wrapped.
	 * 
	 * @param pCanBeGiftWrapped
	 *            the canBeGiftWrapped to set
	 */
	public void setCanBeGiftWrapped(String pCanBeGiftWrapped) {
		mCanBeGiftWrapped = pCanBeGiftWrapped;
	}

	/**
	 * Gets the car seat features.
	 * 
	 * @return the carSeatFeatures
	 */
	public List<String> getCarSeatFeatures() {
		if (mCarSeatFeatures == null) {
			mCarSeatFeatures = new ArrayList<String>();
		}
		return mCarSeatFeatures;
	}

	/**
	 * Sets the car seat features.
	 * 
	 * @param pCarSeatFeatures
	 *            the carSeatFeatures to set
	 */
	public void setCarSeatFeatures(List<String> pCarSeatFeatures) {
		mCarSeatFeatures = pCarSeatFeatures;
	}

	/**
	 * Gets the car seat types.
	 * 
	 * @return the carSeatTypes
	 */
	public List<String> getCarSeatTypes() {
		if (mCarSeatTypes == null) {
			mCarSeatTypes = new ArrayList<String>();
		}
		return mCarSeatTypes;
	}

	/**
	 * Sets the car seat types.
	 * 
	 * @param pCarSeatTypes
	 *            the carSeatTypes to set
	 */
	public void setCarSeatTypes(List<String> pCarSeatTypes) {
		mCarSeatTypes = pCarSeatTypes;
	}

	/**
	 * Gets the car seat what is imp.
	 * 
	 * @return the carSeatWhatIsImp
	 */
	public List<String> getCarSeatWhatIsImp() {
		if (mCarSeatWhatIsImp == null) {
			mCarSeatWhatIsImp = new ArrayList<String>();
		}
		return mCarSeatWhatIsImp;
	}

	/**
	 * Sets the car seat what is imp.
	 * 
	 * @param pCarSeatWhatIsImp
	 *            the carSeatWhatIsImp to set
	 */
	public void setCarSeatWhatIsImp(List<String> pCarSeatWhatIsImp) {
		mCarSeatWhatIsImp = pCarSeatWhatIsImp;
	}

	/**
	 * Gets the child weight max.
	 * 
	 * @return the childWeightMax
	 */
	public String getChildWeightMax() {
		return mChildWeightMax;
	}

	/**
	 * Sets the child weight max.
	 * 
	 * @param pChildWeightMax
	 *            the childWeightMax to set
	 */
	public void setChildWeightMax(String pChildWeightMax) {
		mChildWeightMax = pChildWeightMax;
	}

	/**
	 * Gets the child weight min.
	 * 
	 * @return the childWeightMin
	 */
	public String getChildWeightMin() {
		return mChildWeightMin;
	}

	/**
	 * Sets the child weight min.
	 * 
	 * @param pChildWeightMin
	 *            the childWeightMin to set
	 */
	public void setChildWeightMin(String pChildWeightMin) {
		mChildWeightMin = pChildWeightMin;
	}

	/**
	 * Gets the color code.
	 * 
	 * @return the colorCode
	 */
	public String getColorCode() {
		return mColorCode;
	}

	/**
	 * Sets the color code.
	 * 
	 * @param pColorCode
	 *            the colorCode to set
	 */
	public void setColorCode(String pColorCode) {
		mColorCode = pColorCode;
	}

	/**
	 * Gets the color upc level.
	 * 
	 * @return the colorUpcLevel
	 */
	public String getColorUpcLevel() {
		return mColorUpcLevel;
	}

	/**
	 * Sets the color upc level.
	 * 
	 * @param pColorUpcLevel
	 *            the colorUpcLevel to set
	 */
	public void setColorUpcLevel(String pColorUpcLevel) {
		mColorUpcLevel = pColorUpcLevel;
	}

	/**
	 * Gets the crib material types.
	 * 
	 * @return the cribMaterialTypes
	 */
	public List<String> getCribMaterialTypes() {
		if (mCribMaterialTypes == null) {
			mCribMaterialTypes = new ArrayList<String>();
		}
		return mCribMaterialTypes;
	}

	/**
	 * Sets the crib material types.
	 * 
	 * @param pCribMaterialTypes
	 *            the cribMaterialTypes to set
	 */
	public void setCribMaterialTypes(List<String> pCribMaterialTypes) {
		mCribMaterialTypes = pCribMaterialTypes;
	}

	/**
	 * Gets the crib styles.
	 * 
	 * @return the cribStyles
	 */
	public List<String> getCribStyles() {
		if (mCribStyles == null) {
			mCribStyles = new ArrayList<String>();
		}
		return mCribStyles;
	}

	/**
	 * Sets the crib styles.
	 * 
	 * @param pCribStyles
	 *            the cribStyles to set
	 */
	public void setCribStyles(List<String> pCribStyles) {
		mCribStyles = pCribStyles;
	}

	/**
	 * Gets the crib types.
	 * 
	 * @return the cribTypes
	 */
	public List<String> getCribTypes() {
		if (mCribTypes == null) {
			mCribTypes = new ArrayList<String>();
		}
		return mCribTypes;
	}

	/**
	 * Sets the crib types.
	 * 
	 * @param pCribTypes
	 *            the cribTypes to set
	 */
	public void setCribTypes(List<String> pCribTypes) {
		mCribTypes = pCribTypes;
	}

	/**
	 * Gets the crib what is imp.
	 * 
	 * @return the cribWhatIsImp
	 */
	public List<String> getCribWhatIsImp() {
		if (mCribWhatIsImp == null) {
			mCribWhatIsImp = new ArrayList<String>();
		}
		return mCribWhatIsImp;
	}

	/**
	 * Sets the crib what is imp.
	 * 
	 * @param pCribWhatIsImp
	 *            the cribWhatIsImp to set
	 */
	public void setCribWhatIsImp(List<String> pCribWhatIsImp) {
		mCribWhatIsImp = pCribWhatIsImp;
	}

	/**
	 * Gets the customer purchase limit.
	 * 
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Sets the customer purchase limit.
	 * 
	 * @param pCustomerPurchaseLimit
	 *            the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * Gets the book cd dvd properties map.
	 * 
	 * @return the bookCdDvdPropertiesMap
	 */
	public Map<String, String> getBookCdDvdPropertiesMap() {
		if (mBookCdDvdPropertiesMap == null) {
			mBookCdDvdPropertiesMap = new HashMap<String, String>();
		}
		return mBookCdDvdPropertiesMap;
	}

	/**
	 * Sets the book cd dvd properties map.
	 * 
	 * @param pBookCdDvdPropertiesMap
	 *            the bookCdDvdPropertiesMap to set
	 */
	public void setBookCdDvdPropertiesMap(Map<String, String> pBookCdDvdPropertiesMap) {
		mBookCdDvdPropertiesMap = pBookCdDvdPropertiesMap;
	}

	/**
	 * Gets the dropship flag.
	 * 
	 * @return the dropshipFlag
	 */
	public String getDropshipFlag() {
		return mDropshipFlag;
	}

	/**
	 * Sets the dropship flag.
	 * 
	 * @param pDropshipFlag
	 *            the dropshipFlag to set
	 */
	public void setDropshipFlag(String pDropshipFlag) {
		mDropshipFlag = pDropshipFlag;
	}

	/**
	 * Gets the flexible shipping plan.
	 * 
	 * @return the flexibleShippingPlan
	 */
	public String getFlexibleShippingPlan() {
		return mFlexibleShippingPlan;
	}

	/**
	 * Sets the flexible shipping plan.
	 * 
	 * @param pFlexibleShippingPlan
	 *            the flexibleShippingPlan to set
	 */
	public void setFlexibleShippingPlan(String pFlexibleShippingPlan) {
		mFlexibleShippingPlan = pFlexibleShippingPlan;
	}

	/**
	 * Gets the freight class.
	 * 
	 * @return the freightClass
	 */
	public String getFreightClass() {
		return mFreightClass;
	}

	/**
	 * Sets the freight class.
	 * 
	 * @param pFreightClass
	 *            the freightClass to set
	 */
	public void setFreightClass(String pFreightClass) {
		mFreightClass = pFreightClass;
	}

	/**
	 * Gets the furniture finish.
	 * 
	 * @return the furnitureFinish
	 */
	public String getFurnitureFinish() {
		return mFurnitureFinish;
	}

	/**
	 * Sets the furniture finish.
	 * 
	 * @param pFurnitureFinish
	 *            the furnitureFinish to set
	 */
	public void setFurnitureFinish(String pFurnitureFinish) {
		mFurnitureFinish = pFurnitureFinish;
	}

	/**
	 * Gets the incremental safety stock units.
	 * 
	 * @return the incrementalSafetyStockUnits
	 */
	public String getIncrementalSafetyStockUnits() {
		return mIncrementalSafetyStockUnits;
	}

	/**
	 * Sets the incremental safety stock units.
	 * 
	 * @param pIncrementalSafetyStockUnits
	 *            the incrementalSafetyStockUnits to set
	 */
	public void setIncrementalSafetyStockUnits(String pIncrementalSafetyStockUnits) {
		mIncrementalSafetyStockUnits = pIncrementalSafetyStockUnits;
	}

	/**
	 * Gets the item dimension map.
	 * 
	 * @return the itemDimensionMap
	 */
	public Map<String, String> getItemDimensionMap() {
		if (mItemDimensionMap == null) {
			mItemDimensionMap = new HashMap<String, String>();
		}
		return mItemDimensionMap;
	}

	/**
	 * Sets the item dimension map.
	 * 
	 * @param pItemDimensionMap
	 *            the itemDimensionMap to set
	 */
	public void setItemDimensionMap(Map<String, String> pItemDimensionMap) {
		mItemDimensionMap = pItemDimensionMap;
	}

	/**
	 * Gets the juvenile product size.
	 * 
	 * @return the juvenileProductSize
	 */
	public String getJuvenileProductSize() {
		return mJuvenileProductSize;
	}

	/**
	 * Sets the juvenile product size.
	 * 
	 * @param pJuvenileProductSize
	 *            the juvenileProductSize to set
	 */
	public void setJuvenileProductSize(String pJuvenileProductSize) {
		mJuvenileProductSize = pJuvenileProductSize;
	}

	/**
	 * Gets the lower48 map.
	 * 
	 * @return the lower48Map
	 */
	public Map<String, String> getLower48Map() {
		if (mLower48Map == null) {
			mLower48Map = new HashMap<String, String>();
		}
		return mLower48Map;
	}

	/**
	 * Sets the lower48 map.
	 * 
	 * @param pLower48Map
	 *            the lower48Map to set
	 */
	public void setLower48Map(Map<String, String> pLower48Map) {
		mLower48Map = pLower48Map;
	}

	/**
	 * Gets the max target age tru websites.
	 * 
	 * @return the maxTargetAgeTruWebsites
	 */
	public String getMaxTargetAgeTruWebsites() {
		return mMaxTargetAgeTruWebsites;
	}

	/**
	 * Sets the max target age tru websites.
	 * 
	 * @param pMaxTargetAgeTruWebsites
	 *            the maxTargetAgeTruWebsites to set
	 */
	public void setMaxTargetAgeTruWebsites(String pMaxTargetAgeTruWebsites) {
		mMaxTargetAgeTruWebsites = pMaxTargetAgeTruWebsites;
	}

	/**
	 * Gets the mfr suggested age max.
	 * 
	 * @return the mfrSuggestedAgeMax
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * Sets the mfr suggested age max.
	 * 
	 * @param pMfrSuggestedAgeMax
	 *            the mfrSuggestedAgeMax to set
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}

	/**
	 * Gets the mfr suggested age min.
	 * 
	 * @return the mfrSuggestedAgeMin
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * Sets the mfr suggested age min.
	 * 
	 * @param pMfrSuggestedAgeMin
	 *            the mfrSuggestedAgeMin to set
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * Gets the min target age tru websites.
	 * 
	 * @return the minTargetAgeTruWebsites
	 */
	public String getMinTargetAgeTruWebsites() {
		return mMinTargetAgeTruWebsites;
	}

	/**
	 * Sets the min target age tru websites.
	 * 
	 * @param pMinTargetAgeTruWebsites
	 *            the minTargetAgeTruWebsites to set
	 */
	public void setMinTargetAgeTruWebsites(String pMinTargetAgeTruWebsites) {
		mMinTargetAgeTruWebsites = pMinTargetAgeTruWebsites;
	}

	/**
	 * Gets the nmwa percentage.
	 * 
	 * @return the nmwaPercentage
	 */
	public String getNmwaPercentage() {
		return mNmwaPercentage;
	}

	/**
	 * Sets the nmwa percentage.
	 * 
	 * @param pNmwaPercentage
	 *            the nmwaPercentage to set
	 */
	public void setNmwaPercentage(String pNmwaPercentage) {
		mNmwaPercentage = pNmwaPercentage;
	}

	/**
	 * Gets the ship window max.
	 * 
	 * @return the shipWindowMax
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * Sets the ship window max.
	 * 
	 * @param pShipWindowMax
	 *            the shipWindowMax to set
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * Gets the ship window min.
	 * 
	 * @return the shipWindowMin
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * Sets the ship window min.
	 * 
	 * @param pShipWindowMin
	 *            the shipWindowMin to set
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * Gets the online pid.
	 * 
	 * @return the onlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * Sets the online pid.
	 * 
	 * @param pOnlinePID
	 *            the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}

	/**
	 * Gets the original pid.
	 * 
	 * @return the originalPID
	 */
	public String getOriginalPID() {
		return mOriginalPID;
	}

	/**
	 * Sets the original pid.
	 * 
	 * @param pOriginalPID
	 *            the originalPID to set
	 */
	public void setOriginalPID(String pOriginalPID) {
		mOriginalPID = pOriginalPID;
	}

	/**
	 * Gets the street date.
	 * 
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 * 
	 * @param pStreetDate
	 *            the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}
	
	/**
	 * Gets the inventory received date.
	 *
	 * @return the inventory received date
	 */
	public String getInventoryReceivedDate() {
		return mInventoryReceivedDate;
	}

	/**
	 * Sets the inventory received date.
	 *
	 * @param pInventoryReceivedDate the new inventory Received date
	 */
	public void setInventoryReceivedDate(String pInventoryReceivedDate) {
		mInventoryReceivedDate = pInventoryReceivedDate;
	}

	/**
	 * Gets the other fixed surcharge.
	 * 
	 * @return the otherFixedSurcharge
	 */
	public String getOtherFixedSurcharge() {
		return mOtherFixedSurcharge;
	}

	/**
	 * Sets the other fixed surcharge.
	 * 
	 * @param pOtherFixedSurcharge
	 *            the otherFixedSurcharge to set
	 */
	public void setOtherFixedSurcharge(String pOtherFixedSurcharge) {
		mOtherFixedSurcharge = pOtherFixedSurcharge;
	}

	/**
	 * Gets the other std fs dollar.
	 * 
	 * @return the otherStdFSDollar
	 */
	public String getOtherStdFSDollar() {
		return mOtherStdFSDollar;
	}

	/**
	 * Sets the other std fs dollar.
	 * 
	 * @param pOtherStdFSDollar
	 *            the otherStdFSDollar to set
	 */
	public void setOtherStdFSDollar(String pOtherStdFSDollar) {
		mOtherStdFSDollar = pOtherStdFSDollar;
	}

	/**
	 * Gets the outlet.
	 * 
	 * @return the outlet
	 */
	public String getOutlet() {
		return mOutlet;
	}

	/**
	 * Sets the outlet.
	 * 
	 * @param pOutlet
	 *            the outlet to set
	 */
	public void setOutlet(String pOutlet) {
		mOutlet = pOutlet;
	}

	/**
	 * Gets the presell quantity units.
	 * 
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}

	/**
	 * Sets the presell quantity units.
	 * 
	 * @param pPresellQuantityUnits
	 *            the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}

	/**
	 * Gets the price display.
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}

	/**
	 * Sets the price display.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * Gets the product awards.
	 * 
	 * @return the productAwards
	 */
	public String getProductAwards() {
		return mProductAwards;
	}

	/**
	 * Sets the product awards.
	 * 
	 * @param pProductAwards
	 *            the productAwards to set
	 */
	public void setProductAwards(String pProductAwards) {
		mProductAwards = pProductAwards;
	}

	/**
	 * Gets the product safety warnings.
	 * 
	 * @return the productSafetyWarnings
	 */
	public List<String> getProductSafetyWarnings() {
		if (mProductSafetyWarnings == null) {
			mProductSafetyWarnings = new ArrayList<String>();
		}
		return mProductSafetyWarnings;
	}

	/**
	 * Sets the product safety warnings.
	 * 
	 * @param pProductSafetyWarnings
	 *            the productSafetyWarnings to set
	 */
	public void setProductSafetyWarnings(List<String> pProductSafetyWarnings) {
		mProductSafetyWarnings = pProductSafetyWarnings;
	}

	/**
	 * Gets the promotional sticker display.
	 * 
	 * @return the promotionalStickerDisplay
	 */
	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}

	/**
	 * Sets the promotional sticker display.
	 * 
	 * @param pPromotionalStickerDisplay
	 *            the promotionalStickerDisplay to set
	 */
	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}

	/**
	 * Gets the registry warning indicator.
	 * 
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * Sets the registry warning indicator.
	 * 
	 * @param pRegistryWarningIndicator
	 *            the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}

	/**
	 * Gets the registry eligibility.
	 * 
	 * @return the registryEligibility
	 */
	public String getRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * Sets the registry eligibility.
	 * 
	 * @param pRegistryEligibility
	 *            the registryEligibility to set
	 */
	public void setRegistryEligibility(String pRegistryEligibility) {
		mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * Gets the safety stock.
	 * 
	 * @return the safetyStock
	 */
	public String getSafetyStock() {
		return mSafetyStock;
	}

	/**
	 * Sets the safety stock.
	 * 
	 * @param pSafetyStock
	 *            the safetyStock to set
	 */
	public void setSafetyStock(String pSafetyStock) {
		mSafetyStock = pSafetyStock;
	}

	/**
	 * Gets the ship in orig container.
	 * 
	 * @return the shipInOrigContainer
	 */
	public String getShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * Sets the ship in orig container.
	 * 
	 * @param pShipInOrigContainer
	 *            the shipInOrigContainer to set
	 */
	public void setShipInOrigContainer(String pShipInOrigContainer) {
		mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * Gets the strl best uses.
	 * 
	 * @return the strlBestUses
	 */
	public List<String> getStrlBestUses() {
		if (mStrlBestUses == null) {
			mStrlBestUses = new ArrayList<String>();
		}
		return mStrlBestUses;
	}

	/**
	 * Sets the strl best uses.
	 * 
	 * @param pStrlBestUses
	 *            the strlBestUses to set
	 */
	public void setStrlBestUses(List<String> pStrlBestUses) {
		mStrlBestUses = pStrlBestUses;
	}

	/**
	 * Gets the strl extras.
	 * 
	 * @return the strlExtras
	 */
	public List<String> getStrlExtras() {
		if (mStrlExtras == null) {
			mStrlExtras = new ArrayList<String>();
		}
		return mStrlExtras;
	}

	/**
	 * Sets the strl extras.
	 * 
	 * @param pStrlExtras
	 *            the strlExtras to set
	 */
	public void setStrlExtras(List<String> pStrlExtras) {
		mStrlExtras = pStrlExtras;
	}

	/**
	 * Gets the strl types.
	 * 
	 * @return the strlTypes
	 */
	public List<String> getStrlTypes() {
		if (mStrlTypes == null) {
			mStrlTypes = new ArrayList<String>();
		}
		return mStrlTypes;
	}

	/**
	 * Sets the strl types.
	 * 
	 * @param pStrlTypes
	 *            the strlTypes to set
	 */
	public void setStrlTypes(List<String> pStrlTypes) {
		mStrlTypes = pStrlTypes;
	}

	/**
	 * Gets the strl what is imp.
	 * 
	 * @return the strlWhatIsImp
	 */
	public List<String> getStrlWhatIsImp() {
		if (mStrlWhatIsImp == null) {
			mStrlWhatIsImp = new ArrayList<String>();
		}
		return mStrlWhatIsImp;
	}

	/**
	 * Sets the strl what is imp.
	 * 
	 * @param pStrlWhatIsImp
	 *            the strlWhatIsImp to set
	 */
	public void setStrlWhatIsImp(List<String> pStrlWhatIsImp) {
		mStrlWhatIsImp = pStrlWhatIsImp;
	}

	/**
	 * Gets the supress in search.
	 * 
	 * @return the supressInSearch
	 */
	public String getSupressInSearch() {
		return mSupressInSearch;
	}

	/**
	 * Sets the supress in search.
	 * 
	 * @param pSupressInSearch
	 *            the supressInSearch to set
	 */
	public void setSupressInSearch(String pSupressInSearch) {
		mSupressInSearch = pSupressInSearch;
	}

	/**
	 * Gets the system requirements.
	 * 
	 * @return the systemRequirements
	 */
	public String getSystemRequirements() {
		return mSystemRequirements;
	}

	/**
	 * Sets the system requirements.
	 * 
	 * @param pSystemRequirements
	 *            the systemRequirements to set
	 */
	public void setSystemRequirements(String pSystemRequirements) {
		mSystemRequirements = pSystemRequirements;
	}

	/**
	 * Gets the upc numbers.
	 * 
	 * @return the upcNumber
	 */
	public Set<String> getUpcNumbers() {
		if (mUpcNumbers == null) {
			mUpcNumbers = new HashSet<String>();
		}
		return mUpcNumbers;
	}

	/**
	 * Sets the upc numbers.
	 * 
	 * @param pUpcNumbers
	 *            the new upc numbers
	 */
	public void setUpcNumbers(Set<String> pUpcNumbers) {
		mUpcNumbers = pUpcNumbers;
	}

	/**
	 * Gets the video game esrb.
	 * 
	 * @return the videoGameEsrb
	 */
	public String getVideoGameEsrb() {
		return mVideoGameEsrb;
	}

	/**
	 * Sets the video game esrb.
	 * 
	 * @param pVideoGameEsrb
	 *            the videoGameEsrb to set
	 */
	public void setVideoGameEsrb(String pVideoGameEsrb) {
		mVideoGameEsrb = pVideoGameEsrb;
	}

	/**
	 * Gets the video game genre.
	 * 
	 * @return the videoGameGenre
	 */
	public String getVideoGameGenre() {
		return mVideoGameGenre;
	}

	/**
	 * Sets the video game genre.
	 * 
	 * @param pVideoGameGenre
	 *            the videoGameGenre to set
	 */
	public void setVideoGameGenre(String pVideoGameGenre) {
		mVideoGameGenre = pVideoGameGenre;
	}

	/**
	 * Gets the video game platform.
	 * 
	 * @return the videoGamePlatform
	 */
	public String getVideoGamePlatform() {
		return mVideoGamePlatform;
	}

	/**
	 * Sets the video game platform.
	 * 
	 * @param pVideoGamePlatform
	 *            the videoGamePlatform to set
	 */
	public void setVideoGamePlatform(String pVideoGamePlatform) {
		mVideoGamePlatform = pVideoGamePlatform;
	}

	/**
	 * Gets the nmwa.
	 * 
	 * @return the nmwa
	 */
	public String getNmwa() {
		return mNmwa;
	}

	/**
	 * Sets the nmwa.
	 * 
	 * @param pNmwa
	 *            the nmwa to set
	 */
	public void setNmwa(String pNmwa) {
		mNmwa = pNmwa;
	}

	/**
	 * Gets the web display flag.
	 * 
	 * @return the webDisplayFlag
	 */
	public String getWebDisplayFlag() {
		return mWebDisplayFlag;
	}

	/**
	 * Sets the web display flag.
	 * 
	 * @param pWebDisplayFlag
	 *            the webDisplayFlag to set
	 */
	public void setWebDisplayFlag(String pWebDisplayFlag) {
		mWebDisplayFlag = pWebDisplayFlag;
	}

	/**
	 * Gets the bpp eligible.
	 * 
	 * @return the bppEligible
	 */
	public Boolean getBppEligible() {
		return mBppEligible;
	}

	/**
	 * Sets the bpp eligible.
	 * 
	 * @param pBppEligible
	 *            the bppEligible to set
	 */
	public void setBppEligible(Boolean pBppEligible) {
		mBppEligible = pBppEligible;
	}

	/**
	 * Gets the bpp name.
	 * 
	 * @return the bppName
	 */
	public String getBppName() {
		return mBppName;
	}

	/**
	 * Sets the bpp name.
	 * 
	 * @param pBppName
	 *            the bppName to set
	 */
	public void setBppName(String pBppName) {
		mBppName = pBppName;
	}

	/**
	 * Gets the legal notice.
	 * 
	 * @return the legalNotice
	 */
	public String getLegalNotice() {
		return mLegalNotice;
	}

	/**
	 * Sets the legal notice.
	 * 
	 * @param pLegalNotice
	 *            the legalNotice to set
	 */
	public void setLegalNotice(String pLegalNotice) {
		mLegalNotice = pLegalNotice;
	}

	/**
	 * Gets the swatch image.
	 * 
	 * @return the swatchImage
	 */
	public String getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * Sets the swatch image.
	 * 
	 * @param pSwatchImage
	 *            the swatchImage to set
	 */
	public void setSwatchImage(String pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}

	/**
	 * Gets the primary image.
	 * 
	 * @return the primaryImageCount
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * Sets the primary image.
	 * 
	 * @param pPrimaryImage
	 *            the new primary image
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}

	/**
	 * Gets the secondary image.
	 * 
	 * @return the secondaryImageCount
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}

	/**
	 * Sets the secondary image.
	 * 
	 * @param pSecondaryImage
	 *            the new secondary image
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}

	/**
	 * Gets the alternate image.
	 * 
	 * @return the alternateImageCount
	 */
	public Set<String> getAlternateImage() {
		if (mAlternateImage == null) {
			mAlternateImage = new HashSet<String>();
		}
		return mAlternateImage;
	}

	/**
	 * Sets the alternate image.
	 * 
	 * @param pAlternateImage
	 *            the new alternate image
	 */
	public void setAlternateImage(Set<String> pAlternateImage) {
		mAlternateImage = pAlternateImage;
	}

	/**
	 * Gets the cross sell sku id.
	 * 
	 * @return the crossSellSkuId
	 */
	public List<String> getCrossSellSkuId() {

		if (mCrossSellSkuId == null) {
			mCrossSellSkuId = new ArrayList<String>();
		}

		return mCrossSellSkuId;
	}

	/**
	 * Sets the cross sell sku id.
	 * 
	 * @param pCrossSellSkuId
	 *            the crossSellSkuId to set
	 */
	public void setCrossSellSkuId(List<String> pCrossSellSkuId) {
		mCrossSellSkuId = pCrossSellSkuId;
	}

	/**
	 * Gets the slapper item.
	 * 
	 * @return the slapperItem
	 */
	public String getSlapperItem() {
		return mSlapperItem;
	}

	/**
	 * Sets the slapper item.
	 * 
	 * @param pSlapperItem
	 *            the slapperItem to set
	 */
	public void setSlapperItem(String pSlapperItem) {
		mSlapperItem = pSlapperItem;
	}

	/**
	 * Gets the ship to store eeligible.
	 * 
	 * @return the shipToStoreEeligible
	 */
	public String getShipToStoreEeligible() {
		return mShipToStoreEeligible;
	}

	/**
	 * Sets the ship to store eeligible.
	 * 
	 * @param pShipToStoreEeligible
	 *            the shipToStoreEeligible to set
	 */
	public void setShipToStoreEeligible(String pShipToStoreEeligible) {
		mShipToStoreEeligible = pShipToStoreEeligible;
	}

	/**
	 * Gets the super display flag.
	 * 
	 * @return the superDisplayFlag
	 */
	public String getSuperDisplayFlag() {
		return mSuperDisplayFlag;
	}

	/**
	 * Sets the super display flag.
	 * 
	 * @param pSuperDisplayFlag
	 *            the superDisplayFlag to set
	 */
	public void setSuperDisplayFlag(String pSuperDisplayFlag) {
		mSuperDisplayFlag = pSuperDisplayFlag;
	}

	/**
	 * Gets the review rating.
	 * 
	 * @return the reviewRating
	 */
	public String getReviewRating() {
		return mReviewRating;
	}

	/**
	 * Sets the review rating.
	 * 
	 * @param pReviewRating
	 *            the reviewRating to set
	 */
	public void setReviewRating(String pReviewRating) {
		mReviewRating = pReviewRating;
	}

	/**
	 * Gets the review count.
	 * 
	 * @return the reviewCount
	 */
	public String getReviewCount() {
		return mReviewCount;
	}

	/**
	 * Sets the review count.
	 * 
	 * @param pReviewCount
	 *            the reviewCount to set
	 */
	public void setReviewCount(String pReviewCount) {
		mReviewCount = pReviewCount;
	}

	/**
	 * Gets the root parent categories.
	 * 
	 * @return the rootParentCategories
	 */
	public Set<String> getRootParentCategories() {
		if (mRootParentCategories == null) {
			mRootParentCategories = new HashSet<String>();
		}
		return mRootParentCategories;
	}

	/**
	 * Sets the root parent categories.
	 * 
	 * @param pRootParentCategories
	 *            the rootParentCategories to set
	 */
	public void setRootParentCategories(Set<String> pRootParentCategories) {
		mRootParentCategories = pRootParentCategories;
	}

	/**
	 * Gets the parent classifications.
	 * 
	 * @return the parentClassifications
	 */
	public List<String> getParentClassifications() {
		if(mParentClassifications == null){
			mParentClassifications = new ArrayList<String>();
		}
		return mParentClassifications;
	}

	/**
	 * Sets the parent classifications.
	 * 
	 * @param pParentClassifications
	 *            the parentClassifications to set
	 */
	public void setParentClassifications(List<String> pParentClassifications) {
		mParentClassifications = pParentClassifications;
	}

	/**
	 * Gets the car seat properties map.
	 * 
	 * @return the carSeatPropertiesMap
	 */
	public Map<String, String> getCarSeatPropertiesMap() {
		if (mCarSeatPropertiesMap == null) {
			mCarSeatPropertiesMap = new HashMap<String, String>();
		}
		return mCarSeatPropertiesMap;
	}

	/**
	 * Sets the car seat properties map.
	 * 
	 * @param pCarSeatPropertiesMap
	 *            the carSeatPropertiesMap to set
	 */
	public void setCarSeatPropertiesMap(Map<String, String> pCarSeatPropertiesMap) {
		mCarSeatPropertiesMap = pCarSeatPropertiesMap;
	}

	/**
	 * Gets the stroller properties map.
	 * 
	 * @return the strollerPropertiesMap
	 */
	public Map<String, String> getStrollerPropertiesMap() {
		if (mStrollerPropertiesMap == null) {
			mStrollerPropertiesMap = new HashMap<String, String>();
		}
		return mStrollerPropertiesMap;
	}

	/**
	 * Sets the stroller properties map.
	 * 
	 * @param pStrollerPropertiesMap
	 *            the strollerPropertiesMap to set
	 */
	public void setStrollerPropertiesMap(Map<String, String> pStrollerPropertiesMap) {
		mStrollerPropertiesMap = pStrollerPropertiesMap;
	}

	/**
	 * Gets the crib properties map.
	 * 
	 * @return the cribPropertiesMap
	 */
	public Map<String, String> getCribPropertiesMap() {
		if (mCribPropertiesMap == null) {
			mCribPropertiesMap = new HashMap<String, String>();
		}
		return mCribPropertiesMap;
	}

	/**
	 * Sets the crib properties map.
	 * 
	 * @param pCribPropertiesMap
	 *            the cribPropertiesMap to set
	 */
	public void setCribPropertiesMap(Map<String, String> pCribPropertiesMap) {
		mCribPropertiesMap = pCribPropertiesMap;
	}

	/**
	 * Gets the orignal parent product.
	 * 
	 * @return the orignalParentProduct
	 */
	public TRUProductFeedVO getOrignalParentProduct() {
		return mOrignalParentProduct;
	}

	/**
	 * Sets the orignal parent product.
	 * 
	 * @param pOrignalParentProduct
	 *            the orignalParentProduct to set
	 */
	public void setOrignalParentProduct(TRUProductFeedVO pOrignalParentProduct) {
		mOrignalParentProduct = pOrignalParentProduct;
	}

	/**
	 * Gets the product feature.
	 * 
	 * @return the productFeature
	 */
	public Map<String, String> getProductFeature() {
		if (mProductFeature == null) {
			mProductFeature = new HashMap<String, String>();
		}
		return mProductFeature;
	}

	/**
	 * Sets the product feature.
	 * 
	 * @param pProductFeature
	 *            the productFeature to set
	 */
	public void setProductFeature(Map<String, String> pProductFeature) {
		mProductFeature = pProductFeature;
	}

	/**
	 * Gets the cross sellsn uid batteries list.
	 * 
	 * @return the crossSellsnUidBatteriesList
	 */
	public List<TRUCrossSellsnUidBatteriesVO> getCrossSellsnUidBatteriesList() {
		if (mCrossSellsnUidBatteriesList == null) {
			mCrossSellsnUidBatteriesList = new ArrayList<TRUCrossSellsnUidBatteriesVO>();
		}
		return mCrossSellsnUidBatteriesList;
	}

	/**
	 * Sets the cross sellsn uid batteries list.
	 * 
	 * @param pCrossSellsnUidBatteriesList
	 *            the crossSellsnUidBatteriesList to set
	 */
	public void setCrossSellsnUidBatteriesList(List<TRUCrossSellsnUidBatteriesVO> pCrossSellsnUidBatteriesList) {
		mCrossSellsnUidBatteriesList = pCrossSellsnUidBatteriesList;
	}


	/**
	 * Gets the ship from store eligible lov.
	 * 
	 * @return the ship from store eligible lov
	 */
	public String getShipFromStoreEligibleLov() {
		return mShipFromStoreEligibleLov;
	}

	/**
	 * Sets the ship from store eligible lov.
	 * 
	 * @param pShipFromStoreEligibleLov
	 *            the new ship from store eligible lov
	 */
	public void setShipFromStoreEligibleLov(String pShipFromStoreEligibleLov) {
		mShipFromStoreEligibleLov = pShipFromStoreEligibleLov;
	}

	/**
	 * Gets the m empty null properties.
	 *
	 * @return the m empty null properties
	 */
	public List<String> getEmptyNullProperties() {
		if(mEmptyNullProperties == null){
			mEmptyNullProperties = new ArrayList<String>();
		}
		return mEmptyNullProperties;
	}

	/**
	 * Sets the m empty null properties.
	 *
	 * @param pEmptyNullProperties the new m empty null properties
	 */
	public void setEmptyNullProperties(List<String> pEmptyNullProperties) {
		this.mEmptyNullProperties = pEmptyNullProperties;
	}

	/**
	 * Gets the empty null sub properties.
	 *
	 * @return the empty null sub properties
	 */
	public List<String> getEmptyNullSubProperties() {
		if(mEmptyNullSubProperties == null){
			mEmptyNullSubProperties = new ArrayList<String>();
		}
		return mEmptyNullSubProperties;
	}

	/**
	 * Sets the empty null sub properties.
	 *
	 * @param pEmptyNullSubProperties the new empty null sub properties
	 */
	public void setEmptyNullSubProperties(List<String> pEmptyNullSubProperties) {
		mEmptyNullSubProperties = pEmptyNullSubProperties;
	}
	
	/**
	 * Gets the vendors product demo url.
	 * 
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * Sets the vendors product demo url.
	 * 
	 * @param pVendorsProductDemoUrl
	 *            the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}

	/**
	 * Gets the updated list props.
	 *
	 * @return the updated list props
	 */
	public List<String> getUpdatedListProps() {
		if(mUpdatedListProps == null){
			mUpdatedListProps = new ArrayList<String>();
		}
		return mUpdatedListProps;
	}

	/**
	 * Sets the updated list props.
	 *
	 * @param pUpdatedListProps the new updated list props
	 */
	public void setUpdatedListProps(List<String> pUpdatedListProps) {
		mUpdatedListProps = pUpdatedListProps;
	}

	/**
	 * Gets the sequence number.
	 * 
	 * @return the sequenceNumber
	 */
	public String getSequenceNumber() {
		return mSequenceNumber;
	}

	/**
	 * Sets the sequence number.
	 * 
	 * @param pSequenceNumber
	 *            the sequenceNumber to set
	 */
	public void setSequenceNumber(String pSequenceNumber) {
		mSequenceNumber = pSequenceNumber;
	}

	/**
	 * Gets the primary path category.
	 *
	 * @return the primary path category
	 */
	public String getPrimaryPathCategory() {
		return mPrimaryPathCategory;
	}

	/**
	 * Sets the primary path category.
	 *
	 * @param pPrimaryPathCategory the new primary path category
	 */
	public void setPrimaryPathCategory(String pPrimaryPathCategory) {
		mPrimaryPathCategory = pPrimaryPathCategory;
	}

	/**
	 * Gets the color family.
	 *
	 * @return the colorFamily
	 */
	public String getColorFamily() {
		return mColorFamily;
	}

	/**
	 * Sets the color family.
	 *
	 * @param pColorFamily the colorFamily to set
	 */
	public void setColorFamily(String pColorFamily) {
		mColorFamily = pColorFamily;
	}

	/**
	 * Gets the size family.
	 *
	 * @return the sizeFamily
	 */
	public List<String> getSizeFamily() {
		if(mSizeFamily == null){
			mSizeFamily = new ArrayList<String>();
		}
		return mSizeFamily;
	}

	/**
	 * Sets the size family.
	 *
	 * @param pSizeFamily the sizeFamily to set
	 */
	public void setSizeFamily(List<String> pSizeFamily) {
		mSizeFamily = pSizeFamily;
	}

	/**
	 * Gets the character theme.
	 *
	 * @return the characterTheme
	 */
	public List<String> getCharacterTheme() {
		if(mCharacterTheme == null){
			mCharacterTheme = new ArrayList<String>();
		}
		return mCharacterTheme;
	}

	/**
	 * Sets the character theme.
	 *
	 * @param pCharacterTheme the characterTheme to set
	 */
	public void setCharacterTheme(List<String> pCharacterTheme) {
		mCharacterTheme = pCharacterTheme;
	}

	/**
	 * Gets the collection theme.
	 *
	 * @return the collectionTheme
	 */
	public List<String> getCollectionTheme() {
		if(mCollectionTheme == null){
			mCollectionTheme = new ArrayList<String>();
		}
		return mCollectionTheme;
	}

	/**
	 * Sets the collection theme.
	 *
	 * @param pCollectionTheme the collectionTheme to set
	 */
	public void setCollectionTheme(List<String> pCollectionTheme) {
		mCollectionTheme = pCollectionTheme;
	}
	
	/**
	 * Gets the empty sku properties flag.
	 *
	 * @return the empty sku properties flag
	 */
	public Boolean getEmptySkuPropertiesFlag() {
		return mEmptySkuPropertiesFlag;
	}

	/**
	 * Sets the empty sku properties flag.
	 *
	 * @param pEmptySkuPropertiesFlag the new empty sku properties flag
	 */
	public void setEmptySkuPropertiesFlag(Boolean pEmptySkuPropertiesFlag) {
		mEmptySkuPropertiesFlag = pEmptySkuPropertiesFlag;
	}

}
