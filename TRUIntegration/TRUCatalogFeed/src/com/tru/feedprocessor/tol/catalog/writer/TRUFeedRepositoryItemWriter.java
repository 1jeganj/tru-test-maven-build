package com.tru.feedprocessor.tol.catalog.writer;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.versionmanager.VersionManager;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.FeedRepositoryItemWriter;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUFeedRepositoryItemWriter.
 * @version 1.0
 * @author Professional Access
 */
public class TRUFeedRepositoryItemWriter extends FeedRepositoryItemWriter {

	/** The Version manager. */
	private VersionManager mVersionManager;

	/**
	 * Gets the version manager.
	 * 
	 * @return the versionManager
	 */
	public VersionManager getVersionManager() {
		return mVersionManager;
	}

	/**
	 * Sets the version manager.
	 * 
	 * @param pVersionManager
	 *            the versionManager to set
	 */
	public void setVersionManager(VersionManager pVersionManager) {
		mVersionManager = pVersionManager;
	}

	/**
	 * method updateItem().
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		MutableRepositoryItem lCurrentItem = null;

		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		if(pFeedVO instanceof TRUSkuFeedVO){
			lItemDiscriptorName = TRUFeedConstants.REGULAR_SKU_TYPE;
		}
		//Map<String, Integer> assetVersionMap = new HashMap<String, Integer>();

		if (lItemDiscriptorName != null) {

			try {
				IFeedItemMapper lFeedItemMapper = (IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
				lCurrentItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(
						mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()), lItemDiscriptorName);
				if(lCurrentItem == null){
					MessageFormat fmt = new MessageFormat(TRUFeedConstants.INVALID_ITEM_TYPE);
					
					Object[] args = {pFeedVO.getRepositoryId(), pFeedVO.getEntityName()};
					throw new FeedSkippableException(null, null,fmt.format(args), pFeedVO, null);
				}
				if (lFeedItemMapper != null) {
					lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
					getRepository(lRepositoryName).updateItem(lCurrentItem);
					afterUpdateItem(pFeedVO, lCurrentItem);
				}
			} catch (Exception exception) {
				getLogger().vlogError(TRUFeedConstants.EXCEPTION, exception);
				String message = exception.getMessage();
				if(StringUtils.isBlank(message) && exception.getCause()!=null){
					message=exception.getCause().getMessage();
				} 
				if(pFeedVO instanceof TRUSkuFeedVO){
					((TRUSkuFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.REPOSITORY_EXCEPTION+TRUProductFeedConstants.SEPARATOR+message);
				} else if(pFeedVO instanceof TRUProductFeedVO){
					((TRUProductFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.REPOSITORY_EXCEPTION+TRUProductFeedConstants.SEPARATOR+message);
				}
				logFailedMessage(startDate, TRUFeedConstants.INVALID_RECORD_MAX_SIZE);
				throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_MAX_SIZE, null, exception);
			}
			
		}
	}
	

	/**
	 * method addItem().
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void addItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		MutableRepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		//Map<String, Integer> assetVersionMap = new HashMap<String, Integer>();

		if (lItemDiscriptorName != null) {
			try {
				IFeedItemMapper lFeedItemMapper = (IFeedItemMapper) getMapperMap().get(pFeedVO.getEntityName());
				lCurrentItem = getRepository(lRepositoryName).createItem(pFeedVO.getRepositoryId(), lItemDiscriptorName);
				beforeAddItem(pFeedVO, lCurrentItem);
				if (lFeedItemMapper != null) {
					lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
					getRepository(lRepositoryName).addItem(lCurrentItem);
					afterAddItem(pFeedVO, lCurrentItem);
				}
			} catch (Exception exception) {
				getLogger().vlogError(TRUFeedConstants.EXCEPTION, exception);
				String message = exception.getMessage();
				if(StringUtils.isBlank(message) && exception.getCause()!=null){
					message=exception.getCause().getMessage();
				} 
				if(pFeedVO instanceof TRUSkuFeedVO){
					((TRUSkuFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.REPOSITORY_EXCEPTION+TRUProductFeedConstants.EMPTY_STRING+exception.getMessage());
				} else if(pFeedVO instanceof TRUProductFeedVO){
					((TRUProductFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.REPOSITORY_EXCEPTION+TRUProductFeedConstants.EMPTY_STRING+exception.getMessage());
				}
				logFailedMessage(startDate, TRUFeedConstants.INVALID_RECORD_MAX_SIZE);
				throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_MAX_SIZE, null, exception);
			}
			addInsertItemToList(pFeedVO);
		}
	}
	
	/**
	 * (non-Javadoc).
	 *
	 * @param pVOItems the vO items
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 * @see com.tru.feedprocessor.tol.AbstractFeedItemWriter.
	 * AbstractBvFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException,FeedSkippableException{
		
		if(getConfigValue(TRUProductFeedConstants.FILES_WRITING) == null){
			setConfigValue(TRUProductFeedConstants.FILES_WRITING, TRUProductFeedConstants.FILES_WRITING);
			String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
			logSuccessMessage(startDate, TRUProductFeedConstants.FILES_WRITING);
		}
		
		for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
			//condition avoid the concurrent updates
			if(!(getUniqueInsertItemList().contains(lBaseFeedProcessVO) || getUniqueUpdateItemList().contains(lBaseFeedProcessVO))){
				
				String lItemDiscriptorName = getItemDescriptorName(lBaseFeedProcessVO.getEntityName());

				if(mEntityIdMap!=null && mEntityIdMap.get(lItemDiscriptorName).get(lBaseFeedProcessVO.getRepositoryId())!=null){
					addUpdateItemToList(lBaseFeedProcessVO);
					updateItem(lBaseFeedProcessVO);
				}else{
					addInsertItemToList(lBaseFeedProcessVO);
					addItem(lBaseFeedProcessVO);

				}
			}
		}

	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FEED_PROCESS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FEED_PROCESS, null, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	
}
