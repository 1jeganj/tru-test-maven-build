package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUStrollerSKUVO.
 * 
 * This class extends TRUSkuFeedVO to set the entity name to StrollerSKU.
 * @author Professional Access
 * @version 1.0
 */
public class TRUStrollerSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU stroller skuvo.
	 */
	public TRUStrollerSKUVO() {
		setEntityName(TRUProductFeedConstants.STROLLER_SKU);
	}

}