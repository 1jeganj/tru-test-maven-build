package com.tru.feedprocessor.tol.catalog.processor;

import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;

/**
 * The Class CategoryItemValueChangedInspector. This class inspects each and every property holds in entites to repository of
 * Category has any changes.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCategoryItemValueChangedInspector implements IItemValueChangeInspector {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * The method is overridden to check whether properties are modified or not of the current vo to the repository item .
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCategoryItemValueChangedInspector, @method: isUpdated()");

/*		Boolean retVal = Boolean.FALSE;
		if (pBaseFeedProcessVO instanceof TRUCategory) {
			TRUCategory categoryVO = (TRUCategory) pBaseFeedProcessVO;
			if (!StringUtils.isBlank(categoryVO.getName())
					&& !categoryVO.getName().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getProductDisplayName()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(categoryVO.getID()) && !categoryVO.getID().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getId()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(categoryVO.getName())
					&& !categoryVO.getName().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))) {
				return retVal = Boolean.TRUE;
			}

		}*/
		getLogger().vlogDebug("End @Class: TRUCategoryItemValueChangedInspector, @method: isUpdated()");

		return Boolean.TRUE;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
