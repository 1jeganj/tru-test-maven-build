package com.tru.feedprocessor.tol.catalog.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class TRUCollectionProductRelationshipItemValueChangedInspector. This class inspects each and every property holds in entities to repository
 * of collection Product relations ships properties has any changes. If any change occurs in one property entire entity will be updated.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCollectionProductRelationshipItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the Collection Product properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pRepoItem
	 *            the repo item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pFeedVO, RepositoryItem pRepoItem) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCollectionProductRelationshipItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pFeedVO instanceof TRUCollectionProductVO) {
			TRUCollectionProductVO product = (TRUCollectionProductVO) pFeedVO;
			
			if(StringUtils.isBlank(product.getErrorMessage())){
				
				List<String> repoIds = new ArrayList<String>();
				List<RepositoryItem> dBChildSKUS = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getChildSKUsName());
				
				for(RepositoryItem repo: dBChildSKUS){
					repoIds.add(repo.getRepositoryId());
				}
				if(!repoIds.containsAll(product.getChildSKUS())){
					retVal = Boolean.TRUE;
				}
				if(!product.getRootParentCategories().isEmpty() && checkRootParentCategories(product.getRootParentCategories(), pRepoItem)){
					retVal = Boolean.TRUE;
				}
				
				if(checkParentClassifications(product.getParentClassifications(), pRepoItem)){
					retVal = Boolean.TRUE;
				}
			}
		}

		getLogger().vlogDebug("End @Class: TRUCollectionProductRelationshipItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}
	
	/**
	 * Check root parent categories.
	 *
	 * @param pToCompareWithDBSet the to compare with db set
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	@SuppressWarnings("unchecked")
	private Boolean checkRootParentCategories(Set<String> pToCompareWithDBSet, RepositoryItem pRepoItem){
		Boolean retVal = Boolean.FALSE;
		Set<RepositoryItem> parentCats = (Set<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentCategories());
		Set<String> parentCatIDs = new HashSet<String>();
		for (RepositoryItem repositoryItem : parentCats) {
			parentCatIDs.add(repositoryItem.getRepositoryId());
		}
		if(!parentCatIDs.containsAll(pToCompareWithDBSet)){
			retVal = Boolean.TRUE;
		}
		return retVal;
	}
	
	/**
	 * Check parent classifications.
	 *
	 * @param pToCompareWithDBList the to compare with db list
	 * @param pRepoItem the repo item
	 * @return the boolean
	 */
	@SuppressWarnings("unchecked")
	private Boolean checkParentClassifications(List<String> pToCompareWithDBList, RepositoryItem pRepoItem){
		
		getLogger().vlogDebug("Start @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkParentClassifications()");
		
		Boolean retVal = Boolean.FALSE;
		List<String> dBParentClassificaiton = (List<String>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentClassifications());
		if (!dBParentClassificaiton.containsAll(pToCompareWithDBList)
				|| (dBParentClassificaiton != null && pToCompareWithDBList != null && dBParentClassificaiton.size() != pToCompareWithDBList.size())) {
			retVal = Boolean.TRUE;
		}
		
		getLogger().vlogDebug("End @Class: TRUProductRelationshipItemValueChangedInspector, @method: checkParentClassifications()");
		
		return retVal;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
