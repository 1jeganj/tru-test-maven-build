package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUNonMerchSKUVO;

/**
 * The Class TRUNonMerchSKUItemValidator. This class validates the mandatory properties of NonMerchSKU.
 * @author Professional Access.
 * @version 1.0.
 */
public class TRUNonMerchSKUItemValidator implements IFeedItemValidator {

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to validate the mandatory properties of NonMerchSKU.
	 * 
	 * and for any exception
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUNonMerchSKUItemValidator, @method: validate()");
		
		TRUNonMerchSKUVO nonMerchSKUVO = (TRUNonMerchSKUVO) pBaseFeedProcessVO;

		if (StringUtils.isBlank(nonMerchSKUVO.getOnlineTitle()) && (!StringUtils.isBlank(nonMerchSKUVO.getFeed()) && (nonMerchSKUVO.getFeed().equalsIgnoreCase(
				TRUProductFeedConstants.FULL) || (nonMerchSKUVO.getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && nonMerchSKUVO.getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.A))))) {
			nonMerchSKUVO.setErrorMessage(TRUProductFeedConstants.SKU_DISPLAY_NAME_INVALID);
			throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_DISPLAY_NAME,
					pBaseFeedProcessVO, null);
		}
		if (StringUtils.isBlank(nonMerchSKUVO.getId())) {
			nonMerchSKUVO.setErrorMessage(TRUProductFeedConstants.SKU_ID_INVALID);
			throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID,
					pBaseFeedProcessVO, null);
		}

		getLogger().vlogDebug("End @Class: TRUNonMerchSKUItemValidator, @method: validate()");

	}

}
