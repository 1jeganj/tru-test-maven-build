package com.tru.feedprocessor.tol.catalog.mapper;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;

/**
 * The Class CategoryMapper. This class maps the data stored in entities to repository to push the data
 * @version 1.0
 * @author Professional Access
 */
public class TRUClassificationMapper extends AbstractFeedItemMapper<TRUClassification> {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}
	

	/**
	 * Maps the Values
	 * 
	 * This Method map() will set the repository properties with the values from the feed
	 * 
	 * This Method sets the property value of the product item in the catalog.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUClassification pVOItem, MutableRepositoryItem pRepoItem) throws RepositoryException,
			FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUClassificationMapper, @method: map()");
		
		String method = TRUProductFeedConstants.CLASSIFICATION_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		int count = (int) getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.CPMCOUNT);
		count++;  
		getLogger().vlogDebug("classMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.CPMCOUNT,count);
		
		if (!StringUtils.isBlank(pVOItem.getID())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getId(), pVOItem.getID());
		}
		if (!StringUtils.isBlank(pVOItem.getUserTypeID())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getUserTypeIdPropertyName(), pVOItem.getUserTypeID());
		}
		if (!StringUtils.isBlank(pVOItem.getName())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayNameDefault(), pVOItem.getName());
		}
		if (!StringUtils.isBlank(pVOItem.getLongDescription())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getLongDescriptionPropertyName(),
					pVOItem.getLongDescription());
		}
		if (!StringUtils.isBlank(pVOItem.getDisplayOrder())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayOrderPropertyName(),
					Integer.parseInt(pVOItem.getDisplayOrder()));
		}
		if (!StringUtils.isBlank(pVOItem.getDisplayStatus())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getDisplayStatusPropertyName(), pVOItem.getDisplayStatus());
		}
		if (!StringUtils.isBlank(pVOItem.getMustOrNiceToHave())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getHaveValuePropertyName(), pVOItem.getMustOrNiceToHave());
		}
		if (!StringUtils.isBlank(pVOItem.getSuggestedQty())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getSuggestedQuantityPropertyName(),
					pVOItem.getSuggestedQty());
		}
		
		if(pVOItem.getCrossRefDisplayNameMap() != null && !pVOItem.getCrossRefDisplayNameMap().isEmpty()){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrossRefDisplayNamePropertyName(),
					pVOItem.getCrossRefDisplayNameMap());
		}
		
		if(pVOItem.getCrossRefDisplayOrderMap() != null && !pVOItem.getCrossRefDisplayOrderMap().isEmpty()){
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getCrossRefDisplayOrderPropertyName(),
					pVOItem.getCrossRefDisplayOrderMap());
		}
		
		if (StringUtils.isNotBlank(pVOItem.getPrimaryParentCategory())) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getPrimaryParentCategory(), pVOItem.getPrimaryParentCategory());
		}
		
		pRepoItem.setPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName(), Boolean.FALSE);
		getLogger().vlogDebug("End @Class: TRUClassificationMapper, @method: map()");

		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		
		return pRepoItem;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

}
