/*
 * 
 */
package com.tru.feedprocessor.tol.catalog.vo;

import java.util.List;

/**
 * The Class TRURootClassification will have the root classification details and properties.
 * @author Professional Access
 * @version 1.0
 */
public class TRURootClassification {

	/**The Property to hold  id. */
	private String mID;

	/**The Property to hold  user type id. */
	private String mUserTypeID;

	/**The Property to hold  name. */
	private String mName;

	/**The Property to hold  tru registry classification. */
	private List<TRURegistryClassification> mTRURegistryClassification;

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getID() {
		return mID;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pID
	 *            the new id
	 */
	public void setID(String pID) {
		this.mID = pID;
	}

	/**
	 * Gets the user type id.
	 * 
	 * @return the user type id
	 */
	public String getUserTypeID() {
		return mUserTypeID;
	}

	/**
	 * Sets the user type id.
	 * 
	 * @param pUserTypeID
	 *            the new user type id
	 */
	public void setUserTypeID(String pUserTypeID) {
		this.mUserTypeID = pUserTypeID;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 * 
	 * @param pName
	 *            the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the TRU registry classification.
	 * 
	 * @return the TRU registry classification
	 */
	public List<TRURegistryClassification> getTRURegistryClassification() {
		return mTRURegistryClassification;
	}

	/**
	 * Sets the TRU registry classification.
	 * 
	 * @param pTRURegistryClassification
	 *            the new TRU registry classification
	 */
	public void setTRURegistryClassification(List<TRURegistryClassification> pTRURegistryClassification) {
		this.mTRURegistryClassification = pTRURegistryClassification;
	}

}
