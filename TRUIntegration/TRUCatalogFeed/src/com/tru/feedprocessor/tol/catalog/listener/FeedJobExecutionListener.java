/**
 * 
 */
package com.tru.feedprocessor.tol.catalog.listener;

import java.io.File;

import javax.transaction.TransactionManager;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import atg.core.util.StringUtils;
import atg.epub.project.Process;
import atg.process.action.ActionConstants;
import atg.process.action.ActionException;
import atg.security.ThreadSecurityManager;
import atg.workflow.ActorAccessException;
import atg.workflow.MissingWorkflowDescriptionException;
import atg.workflow.WorkflowConstants;
import atg.workflow.WorkflowManager;
import atg.workflow.WorkflowView;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.tasklet.TRUFileCreateExecutionContext;
import com.tru.merchutil.tol.workflow.WorkFlowTools;

/**
 * The listener interface for receiving feedJobExecution events. The class that is interested in processing a
 * feedJobExecution event implements this interface, and the object created with that class is registered with a component
 * using the component's addFeedJobExecutionListener method. When the feedJobExecution event occurs, that object's
 * appropriate method is invoked.
 * 
 * @author Professional Access
 * @version 1.0
 * @see FeedJobExecutionEvent
 */
public class FeedJobExecutionListener implements JobExecutionListener {

	/**
	 * the mFeedLogger.
	 */
	private FeedLogger mLogger;

	/** The m feed context. */
	private TRUFileCreateExecutionContext mFeedContext;
	/*
	 * property to hold delete task id
	 */
	/** The m delete task id. */
	private String mDeleteTaskId;
	/**
	 * Property to hold Work flow Manager.
	 */
	private WorkflowManager mWorkflowManager;

	/** The m work flow tools. */
	private WorkFlowTools mWorkFlowTools;

	/** The m user name. */
	private String mUserName;

	/** The m processing dir. */
	private String mProcessingDir;

	/** The m bad dir. */
	private String mBadDir;

	/**
	 * Gets the processing dir.
	 * 
	 * @return the processing dir
	 */
	public String getProcessingDir() {
		return mProcessingDir;
	}

	/**
	 * Sets the processing dir.
	 * 
	 * @param pProcessingDir
	 *            the new processing dir
	 */
	public void setProcessingDir(String pProcessingDir) {
		mProcessingDir = pProcessingDir;
	}

	/**
	 * Gets the bad dir.
	 * 
	 * @return the bad dir
	 */
	public String getBadDir() {
		return mBadDir;
	}

	/**
	 * Sets the bad dir.
	 * 
	 * @param pBadDir
	 *            the new bad dir
	 */
	public void setBadDir(String pBadDir) {
		mBadDir = pBadDir;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	public String getUserName() {
		return mUserName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param pUserName
	 *            the new user name
	 */
	public void setUserName(String pUserName) {
		this.mUserName = pUserName;
	}

	/**
	 * Gets the work flow tools.
	 * 
	 * @return the work flow tools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 * 
	 * @param pWorkFlowTools
	 *            the new work flow tools
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		this.mWorkFlowTools = pWorkFlowTools;
	}

	/**
	 * Gets the workflow manager.
	 * 
	 * @return the mWorkflowManager
	 */
	public WorkflowManager getWorkflowManager() {
		return mWorkflowManager;
	}

	/**
	 * Sets the workflow manager.
	 * 
	 * @param pWorkflowManager
	 *            the mWorkflowManager to set
	 */
	public void setWorkflowManager(WorkflowManager pWorkflowManager) {
		this.mWorkflowManager = pWorkflowManager;
	}

	/**
	 * Gets the delete task id.
	 * 
	 * @return the mDeleteTaskId
	 */
	public String getDeleteTaskId() {
		return mDeleteTaskId;
	}

	/**
	 * Sets the delete task id.
	 * 
	 * @param pDeleteTaskId
	 *            the mDeleteTaskId to set
	 */
	public void setDeleteTaskId(String pDeleteTaskId) {
		this.mDeleteTaskId = pDeleteTaskId;
	}

	/**
	 * Gets the feed context.
	 * 
	 * @return the feed context
	 */
	public TRUFileCreateExecutionContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 * 
	 * @param pFeedContext
	 *            the new feed context
	 */
	public void setFeedContext(TRUFileCreateExecutionContext pFeedContext) {
		this.mFeedContext = pFeedContext;
	}

	/** the mTransactionManager. */
	private TransactionManager mTransactionManager;

	/**
	 * Gets the transaction manager.
	 * 
	 * @return mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * Sets the transaction manager.
	 * 
	 * @param pTransactionManager
	 *            - pTransactionManager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	
	
	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the mLogger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		this.mLogger = pLogger;
	}




	
	/**
	 * @param pJobExecution - JobExecution
	 */
	public void beforeJob(JobExecution pJobExecution) {
		// TODO Auto-generated method stub

	}

	/**
	 * This method performs the after job executes.
	 * 
	 * @param pJobExecution
	 *            the job execution
	 */
	@Override
	public void afterJob(JobExecution pJobExecution) {
		getLogger()
				.vlogDebug("FeedJobExecutionListener.afterJob(JobExecution pJobExecution pProcess) begins", pJobExecution);
		final BatchStatus lBatchStatus = pJobExecution.getStatus();
		final File processingDir = new File(getProcessingDir());
		final File badFolder = new File(getBadDir());
		if (lBatchStatus.isUnsuccessful() && processingDir.exists()	&& processingDir.isDirectory() && processingDir.canRead()) {

			if (!(badFolder.exists()) && !(badFolder.isDirectory())) {
				badFolder.mkdirs();
			}
			final File[] processFiles = processingDir.listFiles();
			if (processFiles == null || processFiles.length == 0) {
				return;
			}
			for (File file : processFiles) {
				if (file.isFile()) {
					file.renameTo(new File(badFolder, file.getName()));
				}
			}

			deleteBCCProject((Process) getFeedContext().getConfigValue(
					FeedConstants.PROCESS));
		}
		getLogger().vlogDebug("FeedJobExecutionListener.afterJob(JobExecution pJobExecution pProcess) Ends", pJobExecution);
	}

	/**
	 * This method deletes the process / bcc project.
	 * 
	 * @param pProcess
	 *            - process instance
	 */
	public void deleteBCCProject(Process pProcess) {

		getLogger().vlogDebug("FeedJobExecutionListener.deleteBCCProject(Process pProcess) begins", pProcess);

		if (pProcess != null && !StringUtils.isBlank(getDeleteTaskId())) {

			advanceWorkflow(pProcess, getDeleteTaskId());

		}
		getLogger().vlogDebug("FeedJobExecutionListener.deleteBCCProject(Process pProcess) ends");
	}

	/**
	 * This method advances the workflow to the next state.
	 * 
	 * @param pProcess
	 *            - process
	 * @param pTaskOutcomeId
	 *            - task id
	 */
	public void advanceWorkflow(Process pProcess, String pTaskOutcomeId) {

		if (getWorkflowManager() != null && pProcess != null && !StringUtils.isBlank(pTaskOutcomeId)) {
			// advance work flow
			getWorkFlowTools().assumeUserIdentity(getUserName());
			final WorkflowView workflowView = getWorkflowManager().getWorkflowView(ThreadSecurityManager.currentUser());
			if (workflowView != null) {
				final String processName = pProcess.getProject().getWorkflow().getPropertyValue(TRUFeedConstants.PROCESS_NAME)
						.toString();
				final String processId = pProcess.getId();
				try {
					workflowView.fireTaskOutcome(processName, WorkflowConstants.DEFAULT_WORKFLOW_SEGMENT, processId,
							pTaskOutcomeId, ActionConstants.ERROR_RESPONSE_DEFAULT);
				} catch (MissingWorkflowDescriptionException e) {
					if (isLoggingError()) {
						logError("Missing workflow description exception : {0}", e);
					}
				} catch (ActorAccessException e) {
					if (isLoggingError()) {
						logError("Actor access exception : {0}", e);
					}
				} catch (ActionException e) {
					if (isLoggingError()) {
						logError("Action exception : {0}", e);
					}
				}
			}
		}

	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}

	
