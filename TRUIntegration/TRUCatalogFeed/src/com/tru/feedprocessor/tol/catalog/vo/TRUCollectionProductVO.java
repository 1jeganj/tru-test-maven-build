package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.List;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;

/**
 * The Class TRUCollectionProductVO. VO class which will store all the collection product item properties and 
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUCollectionProductVO extends TRUProductFeedVO {

	
	/** property to mCollectionImage . */
	private String mCollectionImage;
	/** The property to hold display status. */
	private String mDisplayStatus;
	/**
	 * Instantiates a new TRU collectionProduct.
	 */
	public TRUCollectionProductVO() {
		setEntityName(TRUFeedConstants.COLLECTION_PRODUCT);
	}

	
	/**
	 * Gets the display status.
	 *
	 * @return the displayStatus
	 */
	public String getDisplayStatus() {
		return mDisplayStatus;
	}


	/**
	 * Sets the display status.
	 *
	 * @param pDisplayStatus the displayStatus to set
	 */
	public void setDisplayStatus(String pDisplayStatus) {
		mDisplayStatus = pDisplayStatus;
	}


	/**
	 * Gets the CollectionImage.
	 * 
	 * @return the mCollectionImage
	 */
	public String getCollectionImage() {
		return mCollectionImage;
	}

	/**
	 * Sets the CollectionImage.
	 * 
	 * @param pCollectionImage
	 *            the CollectionImage to set
	 */
	public void setCollectionImage(String pCollectionImage) {
		this.mCollectionImage = pCollectionImage;
	}

	/** The m child products. */
	private List<String> mChildSKUS;


	/**
	 * Gets the child skus.
	 *
	 * @return the childSKUS
	 */
	public List<String> getChildSKUS() {
		if (mChildSKUS == null) {
			mChildSKUS = new ArrayList<String>();
		}
		return mChildSKUS;
	}


	/**
	 * Sets the child skus.
	 *
	 * @param pChildSKUS the childSKUS to set
	 */
	public void setChildSKUS(List<String> pChildSKUS) {
		mChildSKUS = pChildSKUS;
	}
}
