package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValueChangedInspector;
import com.tru.feedprocessor.tol.catalog.vo.TRUVideoGameSKUVO;

/**
 * The Class TRUVideoGameSKUItemValueChangedInspector. 
 * This class inspects each and every property holds in entites to repository of SKU and VideoGameSKU properties has any changes. 
 * If any change occurs in one property entire entity will be updated.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUVideoGameSKUItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;


	/** property to hold CommonSKUItemValueChangedInspector holder. */
	private TRUCommonSKUItemValueChangedInspector mCommonSKUItemValueChangedInspector;

	/**
	 * Gets the common sku item value changed inspector.
	 * 
	 * @return the commonSKUItemValueChangedInspector
	 */
	public TRUCommonSKUItemValueChangedInspector getCommonSKUItemValueChangedInspector() {
		return mCommonSKUItemValueChangedInspector;
	}

	/**
	 * Sets the common sku item value changed inspector.
	 * 
	 * @param pCommonSKUItemValueChangedInspector
	 *            the commonSKUItemValueChangedInspector to set
	 */
	public void setCommonSKUItemValueChangedInspector(
			TRUCommonSKUItemValueChangedInspector pCommonSKUItemValueChangedInspector) {
		mCommonSKUItemValueChangedInspector = pCommonSKUItemValueChangedInspector;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the SKU properties of entites with repository items for any changes. 
	 * If any change occurs in one property entire entity will be updated.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {
		
		getLogger().vlogDebug("Start @Class: TRUVideoGameSKUItemValueChangedInspector, @method: isUpdated()");
		boolean isUpdatedValue = Boolean.FALSE;
		if (pBaseFeedProcessVO instanceof TRUVideoGameSKUVO) {
			TRUVideoGameSKUVO videoGameSKUVO = (TRUVideoGameSKUVO) pBaseFeedProcessVO;
			if (getCommonSKUItemValueChangedInspector().isUpdated(videoGameSKUVO, pRepositoryItem)) {
				isUpdatedValue = Boolean.TRUE;
			}
			else if(!StringUtils.isBlank(videoGameSKUVO.getVideoGameEsrb())	&&
					!videoGameSKUVO.getVideoGameEsrb().equals(
							pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVideoGameEsrb()))) {
				isUpdatedValue = Boolean.TRUE;
			}
			else if(!StringUtils.isBlank(videoGameSKUVO.getVideoGameGenre()) &&
					!videoGameSKUVO.getVideoGameGenre().equals(
							pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVideoGameGenre()))) {
				isUpdatedValue = Boolean.TRUE;
			}
			else if(!StringUtils.isBlank(videoGameSKUVO.getVideoGamePlatform())	&&
					!videoGameSKUVO.getVideoGamePlatform().equals(
							pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVideoGamePlatform()))) {
				isUpdatedValue = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUVideoGameSKUItemValueChangedInspector, @method: isUpdated()");

		return isUpdatedValue;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
}
