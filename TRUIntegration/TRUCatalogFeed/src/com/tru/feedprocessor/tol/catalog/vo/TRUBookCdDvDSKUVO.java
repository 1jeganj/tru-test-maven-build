package com.tru.feedprocessor.tol.catalog.vo;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUBookCdDvDSKUVO. This class extends TRUSkuFeedVO to set the entity name to BookCdDvDSKU.
 * @version 1.0
 * @author Professional Access
 */
public class TRUBookCdDvDSKUVO extends TRUSkuFeedVO {

	/**
	 * Instantiates a new TRU book cd dv dskuvo.
	 */
	public TRUBookCdDvDSKUVO() {
		setEntityName(TRUProductFeedConstants.BOOKCDDVD_SKU);
	}

}