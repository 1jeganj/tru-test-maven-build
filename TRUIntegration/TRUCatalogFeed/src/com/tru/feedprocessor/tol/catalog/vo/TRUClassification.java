package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUClassification will hold all the properties related to the classification item.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUClassification extends BaseFeedProcessVO {

	/** The property to hold id. */
	private String mID;

	/** The property to hold user type id. */
	private String mUserTypeID;

	/** The property to hold name. */
	private String mName;

	/** The property to hold display order. */
	private String mDisplayOrder;

	/** The property to hold long description. */
	private String mLongDescription;

	/** The property to hold display status. */
	private String mDisplayStatus;

	/** The property to hold parent category. */
	private String mParentCategory;

	/** The property to hold parent categories. */
	private List<String> mParentCategories = new ArrayList<String>();

	/** The property to hold parent classsifications. */
	private List<String> mParentClasssifications = new ArrayList<String>();

	/** The property to hold tru registry sub classfication. */
	private List<String> mTRURegistrySubClassfication;

	/** The property to hold image path. */
	private String mImagePath;

	/** The property to hold must or nice to have. */
	private String mMustOrNiceToHave;

	/** The property to hold suggested qty. */
	private String mSuggestedQty;

	/** The property to hold registry cross reference ids. */
	private List<String> mRegistryCrossReferenceIds;

	/** The property to hold parent classification. */
	private String mParentClassification;

	/** The property to hold root parent category. */
	private String mRootParentCategory;

	/** The property to hold classification type. */
	private String mClassificationType;

	/** The property to hold child categories. */
	private List<String> mChildCategories = new ArrayList<String>();

	/** The property to hold classification cross reference. */
	private List<TRUClassificationCrossReference> mClassificationCrossReference = new ArrayList<TRUClassificationCrossReference>();

	/** The property to hold registry. */
	private boolean mRegistry;
	/** property to hold mCollectionProductKeySet List. */
	private List<String> mCollectionProductKeySet;
	
	/** The m deleted. */
	private Boolean mDeleted;
	
	/** The property to hold error message. */
	private String mErrorMessage;
	
	/** The cross ref display name map. */
	private Map<String, String> mCrossRefDisplayNameMap;
	
	/** The cross ref display order map. */
	private Map<String, String> mCrossRefDisplayOrderMap;
	
	/** property to hold FileName . */
	private String mFileName;
	
	/** The Primary parent category. */
	private String mPrimaryParentCategory;
	
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}
	
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the errorMessage to set
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}

	/**
	 * Checks if is deleted.
	 *
	 * @return the boolean
	 */
	public Boolean isDeleted() {
		return mDeleted;
	}
	
	/**
	 * Sets the deleted.
	 *
	 * @param pDeleted the new deleted
	 */
	public void setDeleted(Boolean pDeleted) {
		mDeleted = pDeleted;
	}
	/**
	 * Gets the  mCollectionProductKeySet.
	 * 
	 * @return the  mCollectionProductKeySet
	 */
	public List<String> getCollectionProductKeySet() {
		if(mCollectionProductKeySet==null){
			mCollectionProductKeySet=new ArrayList<String>();
		}
		return mCollectionProductKeySet;
	}
	
	/**
	 * Sets the mCollectionProductKeySet  .
	 *
	 * @param pCollectionProductKeySet the collection of vo keys
	 */
	public void setCollectionProductKeySet(List<String> pCollectionProductKeySet) {
		this.mCollectionProductKeySet = pCollectionProductKeySet;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getID() {
		return mID;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pID
	 *            the new id
	 */
	public void setID(String pID) {
		this.mID = pID;
		setRepositoryId(pID);
	}

	/**
	 * Gets the user type id.
	 * 
	 * @return the user type id
	 */
	public String getUserTypeID() {
		return mUserTypeID;
	}

	/**
	 * Sets the user type id.
	 * 
	 * @param pUserTypeID
	 *            the new user type id
	 */
	public void setUserTypeID(String pUserTypeID) {
		this.mUserTypeID = pUserTypeID;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 * 
	 * @param pName
	 *            the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the TRU registry sub classfication.
	 * 
	 * @return the TRU registry sub classfication
	 */
	public List<String> getTRURegistrySubClassfication() {
		return mTRURegistrySubClassfication;
	}

	/**
	 * Sets the TRU registry sub classfication.
	 * 
	 * @param pTRURegistrySubClassfication
	 *            the new TRU registry sub classfication
	 */
	public void setTRURegistrySubClassfication(List<String> pTRURegistrySubClassfication) {
		this.mTRURegistrySubClassfication = pTRURegistrySubClassfication;
	}

	/**
	 * Gets the image path.
	 * 
	 * @return the image path
	 */
	public String getImagePath() {
		return mImagePath;
	}

	/**
	 * Sets the image path.
	 * 
	 * @param pImagePath
	 *            the new image path
	 */
	public void setImagePath(String pImagePath) {
		this.mImagePath = pImagePath;
	}

	/**
	 * Gets the display order.
	 * 
	 * @return the display order
	 */
	public String getDisplayOrder() {
		return mDisplayOrder;
	}

	/**
	 * Sets the display order.
	 * 
	 * @param pDisplayOrder
	 *            the new display order
	 */
	public void setDisplayOrder(String pDisplayOrder) {
		this.mDisplayOrder = pDisplayOrder;
	}

	/**
	 * Gets the must or nice to have.
	 * 
	 * @return the must or nice to have
	 */
	public String getMustOrNiceToHave() {
		return mMustOrNiceToHave;
	}

	/**
	 * Sets the must or nice to have.
	 * 
	 * @param pMustOrNiceToHave
	 *            the new must or nice to have
	 */
	public void setMustOrNiceToHave(String pMustOrNiceToHave) {
		this.mMustOrNiceToHave = pMustOrNiceToHave;
	}

	/**
	 * Gets the suggested qty.
	 * 
	 * @return the suggested qty
	 */
	public String getSuggestedQty() {
		return mSuggestedQty;
	}

	/**
	 * Sets the suggested qty.
	 * 
	 * @param pSuggestedQty
	 *            the new suggested qty
	 */
	public void setSuggestedQty(String pSuggestedQty) {
		this.mSuggestedQty = pSuggestedQty;
	}

	/**
	 * Gets the parent classification.
	 * 
	 * @return the parent classification
	 */
	public String getParentClassification() {
		return mParentClassification;
	}

	/**
	 * Sets the parent classification.
	 * 
	 * @param pParentClassification
	 *            the new parent classification
	 */
	public void setParentClassification(String pParentClassification) {
		this.mParentClassification = pParentClassification;
	}

	/**
	 * Gets the root parent category.
	 * 
	 * @return the root parent category
	 */
	public String getRootParentCategory() {
		return mRootParentCategory;
	}

	/**
	 * Sets the root parent category.
	 * 
	 * @param pRootParentCategory
	 *            the new root parent category
	 */
	public void setRootParentCategory(String pRootParentCategory) {
		this.mRootParentCategory = pRootParentCategory;
	}

	/**
	 * Gets the long description.
	 * 
	 * @return the long description
	 */
	public String getLongDescription() {
		return mLongDescription;
	}

	/**
	 * Sets the long description.
	 * 
	 * @param pLongDescription
	 *            the new long description
	 */
	public void setLongDescription(String pLongDescription) {
		mLongDescription = pLongDescription;
	}

	/**
	 * Gets the display status.
	 * 
	 * @return the display status
	 */
	public String getDisplayStatus() {
		return mDisplayStatus;
	}

	/**
	 * Gets the registry cross reference ids.
	 * 
	 * @return the registry cross reference ids
	 */
	public List<String> getRegistryCrossReferenceIds() {
		if (mRegistryCrossReferenceIds == null) {
			mRegistryCrossReferenceIds = new ArrayList<String>();
		}
		return mRegistryCrossReferenceIds;
	}

	/**
	 * Sets the display status.
	 * 
	 * @param pDisplayStatus
	 *            the new display status
	 */
	public void setDisplayStatus(String pDisplayStatus) {
		mDisplayStatus = pDisplayStatus;
	}

	/**
	 * Sets the registry cross reference ids.
	 * 
	 * @param pRegistryCrossReferenceIds
	 *            the new registry cross reference ids
	 */
	public void setRegistryCrossReferenceIds(List<String> pRegistryCrossReferenceIds) {
		this.mRegistryCrossReferenceIds = pRegistryCrossReferenceIds;
	}

	/**
	 * Gets the parent categories.
	 * 
	 * @return the parent categories
	 */
	public List<String> getParentCategories() {
		return mParentCategories;
	}

	/**
	 * Sets the parent categories.
	 * 
	 * @param pParentCategories
	 *            the new parent categories
	 */
	public void setParentCategories(List<String> pParentCategories) {
		mParentCategories = pParentCategories;
	}

	/**
	 * Gets the classification cross reference.
	 * 
	 * @return the classification cross reference
	 */
	public List<TRUClassificationCrossReference> getClassificationCrossReference() {
		return mClassificationCrossReference;
	}

	/**
	 * Sets the classification cross reference.
	 * 
	 * @param pClassificationCrossReference
	 *            the new classification cross reference
	 */
	public void setClassificationCrossReference(List<TRUClassificationCrossReference> pClassificationCrossReference) {
		mClassificationCrossReference = pClassificationCrossReference;
	}

	/**
	 * Gets the child categories.
	 * 
	 * @return the child categories
	 */
	public List<String> getChildCategories() {
		return mChildCategories;
	}

	/**
	 * Sets the child categories.
	 * 
	 * @param pChildCategories
	 *            the new child categories
	 */
	public void setChildCategories(List<String> pChildCategories) {
		mChildCategories = pChildCategories;
	}

	/**
	 * Gets the repository id.
	 * 
	 * @return the repository id
	 */
	@Override
	public String getRepositoryId() {
		return mID;
	}

	/**
	 * Gets the parent category.
	 * 
	 * @return the parent category
	 */
	public String getParentCategory() {
		return mParentCategory;
	}

	/**
	 * Sets the parent category.
	 * 
	 * @param pParentCategory
	 *            the new parent category
	 */
	public void setParentCategory(String pParentCategory) {
		this.mParentCategory = pParentCategory;
	}

	/**
	 * Gets the classification type.
	 * 
	 * @return the classification type
	 */
	public String getClassificationType() {
		return mClassificationType;
	}

	/**
	 * Sets the classification type.
	 * 
	 * @param pClassificationType
	 *            the new classification type
	 */
	public void setClassificationType(String pClassificationType) {
		this.mClassificationType = pClassificationType;
	}

	/**
	 * Checks if is registry.
	 * 
	 * @return true, if is registry
	 */
	public boolean isRegistry() {
		return mRegistry;
	}

	/**
	 * Sets the registry.
	 * 
	 * @param pRegistry
	 *            the new registry
	 */
	public void setRegistry(boolean pRegistry) {
		this.mRegistry = pRegistry;
	}

	/**
	 * Gets the parent classsifications.
	 * 
	 * @return the parent classsifications
	 */
	public List<String> getParentClasssifications() {
		return mParentClasssifications;
	}

	/**
	 * Sets the parent classsifications.
	 * 
	 * @param pParentClasssifications
	 *            the new parent classsifications
	 */
	public void setParentClasssifications(List<String> pParentClasssifications) {
		this.mParentClasssifications = pParentClasssifications;
	}

	/**
	 * Gets the cross ref display name map.
	 *
	 * @return the cross ref display name map
	 */
	public Map<String, String> getCrossRefDisplayNameMap() {
		if(mCrossRefDisplayNameMap == null){
			mCrossRefDisplayNameMap = new HashMap<String, String>();
		}
		return mCrossRefDisplayNameMap;
	}

	/**
	 * Sets the cross ref display name map.
	 *
	 * @param pCrossRefDisplayNameMap the cross ref display name map
	 */
	public void setCrossRefDisplayNameMap(
			Map<String, String> pCrossRefDisplayNameMap) {
		mCrossRefDisplayNameMap = pCrossRefDisplayNameMap;
	}

	/**
	 * Gets the cross ref display order map.
	 *
	 * @return the cross ref display order map
	 */
	public Map<String, String> getCrossRefDisplayOrderMap() {
		if(mCrossRefDisplayOrderMap == null){
			mCrossRefDisplayOrderMap = new HashMap<String, String>();
		}
		return mCrossRefDisplayOrderMap;
	}

	/**
	 * Sets the cross ref display order map.
	 *
	 * @param pCrossRefDisplayOrderMap the cross ref display order map
	 */
	public void setCrossRefDisplayOrderMap(
			Map<String, String> pCrossRefDisplayOrderMap) {
		mCrossRefDisplayOrderMap = pCrossRefDisplayOrderMap;
	}

	/**
	 * Gets the primary parent category.
	 *
	 * @return the primaryParentCategory
	 */
	public String getPrimaryParentCategory() {
		return mPrimaryParentCategory;
	}

	/**
	 * Sets the primary parent category.
	 *
	 * @param pPrimaryParentCategory the primaryParentCategory to set
	 */
	public void setPrimaryParentCategory(String pPrimaryParentCategory) {
		mPrimaryParentCategory = pPrimaryParentCategory;
	}

}
