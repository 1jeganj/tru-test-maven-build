package com.tru.feedprocessor.tol.catalog.writer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.batch.item.ItemStreamException;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUStyleErrorVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUCatFeedRelShipWriter.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCatFeedRelShipWriter extends CatFeedItemRelShipWriter {

	/** The entitykeys. */
	private List<String> mEntitykeys = new ArrayList<String>();


	/** The Identifier id store key. */
	private String mIdentifierIdStoreKey;
	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifier id store key
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the new identifier id store key
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		this.mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * This method is overrides the doOpen method of CatFeedItemRelShipWriter.
	 * @throws ItemStreamException - ItemStreamException.
	 */
	@Override
	public void doOpen() throws ItemStreamException {
		MutableRepositoryItem lCurrentItem = null;
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, Map<String, String>> entityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
		List<String> entitiesList = new ArrayList<String>();
		Map<String, String> entityMap = entityIdMap.get(TRUFeedConstants.CLASSIFICATION_ENTITY);
		if (!entityMap.isEmpty()) {
			Iterator<String> itr = entityMap.keySet().iterator();
			while (itr.hasNext()) {
				entitiesList.add(itr.next());
			}
			FeedExecutionContextVO feedExecutionContextVO = getCurrentFeedExecutionContext();
			mEntitykeys = (List<String>) feedExecutionContextVO.getConfigValue(TRUFeedConstants.ENTITY_KEYS);
			if(mEntitykeys != null) {
				mEntitykeys.addAll((List<String>) feedExecutionContextVO.getConfigValue(TRUFeedConstants.ENTITY_KEYS_REG));
				for (String entity : entitiesList) {
					boolean entityExists = mEntitykeys.contains(entity);
					if (!entityExists) {
						try {
							lCurrentItem = (MutableRepositoryItem) getRepository(TRUFeedConstants.CATALOG_REPOSITORY).getItem(
									entity, TRUFeedConstants.CLASSIFICATION_ENTITY);
							if (lCurrentItem != null && lCurrentItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName()) != null
									&& !(Boolean) lCurrentItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName())) {
								lCurrentItem.setPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName(), Boolean.TRUE);
								List<RepositoryItem> childCats = (List<RepositoryItem>) lCurrentItem.getPropertyValue(getFeedCatalogProperty().
										getFixedChildCategoriesName());
								childCats.clear();

								lCurrentItem.setPropertyValue(getFeedCatalogProperty().getFixedChildCategoriesName(), childCats);
								getRepository(TRUFeedConstants.CATALOG_REPOSITORY).updateItem(lCurrentItem);
							}
						} catch (RepositoryException e) {
							if (isLoggingError()) {
								logError(TRUFeedConstants.REPOSITORY_EXCEPTION, e);
							}
							logFailedMessage(startDate, e.getMessage());
						}
					}
				}
			}
		}
		super.doOpen();
	}
	
	/**
	 * This method is overrides the doWrite method of CatFeedItemRelShipWriter.
	 * @param pVOItems - VOItems.
	 * @throws RepositoryException - RepositoryException.
	 * @throws FeedSkippableException - FeedSkippableException.
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems)
			throws RepositoryException, FeedSkippableException {
		
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		if(getConfigValue(TRUProductFeedConstants.FILES_WRITING) == null){
			setConfigValue(TRUProductFeedConstants.FILES_WRITING, TRUProductFeedConstants.FILES_WRITING);
			logSuccessMessage(startDate, TRUProductFeedConstants.FILES_WRITING);
		}
		
		try{
			for(BaseFeedProcessVO lBaseFeedProcessVO:pVOItems){
				if(lBaseFeedProcessVO instanceof TRUProductFeedVO && !((TRUProductFeedVO)lBaseFeedProcessVO).getStyleUIDs().isEmpty() && 
						getConfigValue(TRUProductFeedConstants.STYLE_ERROR) == null){
					List<TRUStyleErrorVO> errorList = new ArrayList<TRUStyleErrorVO>();
					setConfigValue(TRUProductFeedConstants.STYLE_ERROR, errorList);
				}
				updateItem(lBaseFeedProcessVO);	 
			}
		}catch(FeedSkippableException exception){
			String message = exception.getMessage();
			if(StringUtils.isBlank(message) && exception.getCause()!=null){
				message=exception.getCause().getMessage();
			} else {
				message = TRUFeedConstants.EXCEPTION;
			}
			logFailedMessage(startDate, message);
			throw new FeedSkippableException(exception.getPhaseName(),exception.getStepName(),exception.getMessage(),exception.getFeedItemVO(),exception);
		}catch(Exception exception){
			String message = exception.getMessage();
			if(StringUtils.isBlank(message) && exception.getCause()!=null){
				message=exception.getCause().getMessage();
			} else {
				message = TRUFeedConstants.REPOSITORY_EXCEPTION;
			}
			logFailedMessage(startDate, message);
			throw new FeedSkippableException(null,null,TRUFeedConstants.REPOSITORY_EXCEPTION,null ,exception);
		}
	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILES_WRITING, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILES_WRITING, null, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
