package com.tru.feedprocessor.tol.catalog.processor;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.tools.TRUCommonSKUItemValidator;

/**
 * The Class TRUVideoGameSKUItemValidator. This class validates the mandatory properties of VideoGameSKU
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUVideoGameSKUItemValidator implements IFeedItemValidator {

	/**
	 * The variable to hold "mCommonSKUItemValidator" property name.
	 */
	private TRUCommonSKUItemValidator mCommonSKUItemValidator;
	
	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the common sku item validator.
	 * 
	 * @return the commonSKUItemValidator
	 */
	public TRUCommonSKUItemValidator getCommonSKUItemValidator() {
		return mCommonSKUItemValidator;
	}

	/**
	 * Sets the common sku item validator.
	 * 
	 * @param pCommonSKUItemValidator
	 *            the commonSKUItemValidator to set
	 */
	public void setCommonSKUItemValidator(TRUCommonSKUItemValidator pCommonSKUItemValidator) {
		mCommonSKUItemValidator = pCommonSKUItemValidator;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to validate the mandatory properties of video game sku.
	 * 
	 * and for any exception
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUVideoGameSKUItemValidator, @method: validate()");

		getCommonSKUItemValidator().validateSKUProperties(pBaseFeedProcessVO);

		getLogger().vlogDebug("End @Class: TRUVideoGameSKUItemValidator, @method: validate()");

	}

}
