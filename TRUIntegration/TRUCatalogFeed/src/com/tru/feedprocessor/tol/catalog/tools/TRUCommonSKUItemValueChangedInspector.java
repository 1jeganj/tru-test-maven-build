package com.tru.feedprocessor.tol.catalog.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCrossSellsnUidBatteriesVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUCommonSKUItemValueChangedInspector. This class inspects each and every property holds in entites to
 * repository of SKU has any changes
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUCommonSKUItemValueChangedInspector implements IItemValueChangeInspector {
	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * The variable to hold "mSKUVOToRepository" property name.
	 */
	private TRUSKUVOToRepository mSKUVOToRepository;

	/**
	 * Gets the SKUVO to repository.
	 * 
	 * @return the sKUVOToRepository
	 */
	public TRUSKUVOToRepository getSKUVOToRepository() {
		return mSKUVOToRepository;
	}

	/**
	 * Sets the SKUVO to repository.
	 * 
	 * @param pSKUVOToRepository
	 *            the sKUVOToRepository to set
	 */
	public void setSKUVOToRepository(TRUSKUVOToRepository pSKUVOToRepository) {
		mSKUVOToRepository = pSKUVOToRepository;
	}

	/**
	 * This method is used to check any updates between VO properties and repository properties
	 * 
	 * and returns SKU need to be updated or not.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem)
			throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUCommonSKUItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pBaseFeedProcessVO instanceof TRUSkuFeedVO) {
			TRUSkuFeedVO skuVO = (TRUSkuFeedVO) pBaseFeedProcessVO;
			try {
				
				Map<String, String> akhiMap = (Map<String, String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
						.getAkhiMap());
				Map<String, String> onlineCollectionName = (Map<String, String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
						.getOnlineCollectionName());
				Map<String, String> assemblyMap = (Map<String, String>) pRepositoryItem
						.getPropertyValue(getFeedCatalogProperty().getAssemblyMap());
				Map<String, String> itemDimensionMap = (Map<String, String>) pRepositoryItem
						.getPropertyValue(getFeedCatalogProperty().getItemDimensionMap());
				Map<String, String> lower48Map = (Map<String, String>) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
						.getLower48Map());
				Map<String, String> productFeatureMap = (Map<String, String>) pRepositoryItem
						.getPropertyValue(getFeedCatalogProperty().getProductFeature());
				List<RepositoryItem> crosssellBatteries = (List<RepositoryItem>) pRepositoryItem
						.getPropertyValue(getFeedCatalogProperty().getCrosssellBatteries());

				if (!StringUtils.isBlank(skuVO.getOnlineTitle())&& 
						!skuVO.getOnlineTitle().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getOnlineLongDesc())	&& 
						!skuVO.getOnlineLongDesc().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getLongDescriptionName()))) {
					return retVal = Boolean.TRUE;
				}
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.INTEREST)) {
					if(!skuVO.getInterest().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getInterest()))){
						return retVal = Boolean.TRUE;
					}
				}
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.FINDER_ELIGIBLE)) {
					if(!skuVO.getFinderEligible().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFinderEligible()))){
						return retVal = Boolean.TRUE;
					}
				}
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.SPECIAL_FEATURES)) {
					if(!skuVO.getSpecialFeatures().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSpecialFeatures()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getColorFamily()) 	&&
						!skuVO.getColorFamily().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorFamily()))) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.SIZE_FAMILY)) {
					if(!skuVO.getSizeFamily().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSizeFamily()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CHARACTER_THEME)) {
					if(!skuVO.getCharacterTheme().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCharacterTheme()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.COLLECTION_THEME)) {
					if(!skuVO.getCollectionTheme().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCollectionTheme()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!skuVO.getOnlineCollectionName().isEmpty() && !mapsAreEqual(skuVO.getOnlineCollectionName(), onlineCollectionName)) {
					return retVal = Boolean.TRUE;
				}
				if (!skuVO.getAkhiMap().isEmpty() && !mapsAreEqual(skuVO.getAkhiMap(), akhiMap)) {
					return retVal = Boolean.TRUE;
				}
				if (!skuVO.getAssemblyMap().isEmpty() && !mapsAreEqual(skuVO.getAssemblyMap(), assemblyMap)) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getBackorderStatus())&& 
						!skuVO.getBackorderStatus().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBackOrderStatus()))) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.BRAND_NAME_SECONDARY)) {
					if(!skuVO.getBrandNameSecondary().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBrandNameSecondary()))){
						return retVal = Boolean.TRUE;
					}
				}
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.BRANDNAMETERTIARY_CHARACTER_THEME)) {
					if(!skuVO.getBrandnametertiaryCharacterTheme().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBrandNameTertiaryCharTheme()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getBrowseHiddenKeyword())&& 
						!skuVO.getBrowseHiddenKeyword().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBrowseHiddenKeyword()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getCanBeGiftWrapped())&& 
						!skuVO.getCanBeGiftWrapped().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCanBeGiftWrapped()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getChildWeightMax())&&
						!skuVO.getChildWeightMax().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getChildWeightMax()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getChildWeightMin())	&&
						!skuVO.getChildWeightMin().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getChildWeightMin()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getColorCode())	&&
						!skuVO.getColorCode().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getColorUpcLevel())&& 
						pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorUpcLevel()) != null) {
					String colorDBValue = (String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorUpcLevel());
					try {
						colorDBValue = getSKUVOToRepository().getColorName(colorDBValue);
					} catch (RepositoryException re) {
						if(isLoggingError()){
							logError("RepositoryException in TRUCommonSKUItemValueChangedInspector", re);
						}
					}
					if (!skuVO.getColorUpcLevel().equalsIgnoreCase(colorDBValue)) {
						return retVal = Boolean.TRUE;
					}
				} else if (!StringUtils.isBlank(skuVO.getColorUpcLevel())&& 
						pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getColorUpcLevel()) == null) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getCustomerPurchaseLimit())&& 
						!skuVO.getCustomerPurchaseLimit().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCustomerPurchaseLimit()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getDropshipFlag())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDropShipFlag()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDropShipFlag()) == Boolean.TRUE && 
							skuVO.getDropshipFlag().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDropShipFlag()) == Boolean.FALSE	&& 
							skuVO.getDropshipFlag().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getFlexibleShippingPlan())&& 
						!skuVO.getFlexibleShippingPlan().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFlexibleShippingPlan()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getFreightClass())	&& 
						!skuVO.getFreightClass().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFreightClass()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getFurnitureFinish())&&
						!skuVO.getFurnitureFinish().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getFurnitureFinish()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getIncrementalSafetyStockUnits())&& 
						!skuVO.getIncrementalSafetyStockUnits().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getIncrementalSafetyStockUnits()))) {
					return retVal = Boolean.TRUE;
				}
				if (!skuVO.getItemDimensionMap().isEmpty() && !mapsAreEqual(skuVO.getItemDimensionMap(), itemDimensionMap)) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getJuvenileProductSize())&&
						pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getJuvenileProductSize()) != null) {
					String sizeDBValue = (String) pRepositoryItem.getPropertyValue(getFeedCatalogProperty()
							.getJuvenileProductSize());
					try {
						sizeDBValue = getSKUVOToRepository().getSizeDescription(sizeDBValue);
					} catch (RepositoryException re) {
						if(isLoggingError()){
							logError("RepositoryException in TRUCommonSKUItemValueChangedInspector", re);
						}
					}
					if (!skuVO.getJuvenileProductSize().equalsIgnoreCase(sizeDBValue)) {
						return retVal = Boolean.TRUE;
					}
				} else if (!StringUtils.isBlank(skuVO.getJuvenileProductSize())	&& 
						pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getJuvenileProductSize()) == null) {
					return retVal = Boolean.TRUE;
				}
				if (!skuVO.getLower48Map().isEmpty() && !mapsAreEqual(skuVO.getLower48Map(), lower48Map)) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getMaxTargetAgeTruWebsites())&& 
						!skuVO.getMaxTargetAgeTruWebsites().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getMaxTargetAgeTruWebsites()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getMfrSuggestedAgeMax())	&& 
						!skuVO.getMfrSuggestedAgeMax().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMax()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getMfrSuggestedAgeMin())	&& 
						!skuVO.getMfrSuggestedAgeMin().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getMfrSuggestedAgeMin()))) {
					return retVal = Boolean.TRUE;
				}
				if (!checkDisplayOrderProperty(skuVO.getNmwaPercentage(),
						(Float) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getNmwaPercentage()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getShipWindowMax())) {
					Integer value = Integer.parseInt(skuVO.getShipWindowMax()) / TRUProductFeedConstants.NUMBER_TWENTY_FOUR;
					if (!value.toString().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipWindowMax()))) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getShipWindowMin())) {
					Integer value = Integer.parseInt(skuVO.getShipWindowMin()) / TRUProductFeedConstants.NUMBER_TWENTY_FOUR;
					if (!value.toString().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipWindowMin()))) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getOnlinePID())	&&
						!skuVO.getOnlinePID().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOnlinePID()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getOriginalPID())&& 
						!skuVO.getOriginalPID().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOriginalPID()))) {
					return retVal = Boolean.TRUE;
				}

				if (skuVO.getStreetDate() != null) {
					SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
					Date date = null;
					try {
						date = formatter.parse(skuVO.getStreetDate());
					} catch (ParseException e) {
						if(isLoggingError()){
							logError(TRUProductFeedConstants.PARSE_EXCEPTION, e);
						}
					}
					if (!date.equals((Date) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getStreetDate()))) {
						return retVal = Boolean.TRUE;
					}
				}
				
				if (skuVO.getInventoryReceivedDate() != null) {
					SimpleDateFormat formatter = new SimpleDateFormat(TRUProductFeedConstants.DATE_FORMAT, Locale.getDefault());
					Date date = null;
					try {
						date = formatter.parse(skuVO.getInventoryReceivedDate());
					} catch (ParseException e) {
						if(isLoggingError()){
							logError(TRUProductFeedConstants.PARSE_EXCEPTION, e);
						}
					}
					if (!date.equals((Date) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getInventoryReceivedDate()))) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getRmsSizeCode())&&
						!skuVO.getRmsSizeCode().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRmsSizeCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getRmsColorCode())&&
						!skuVO.getRmsColorCode().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRmsColorCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getOtherFixedSurcharge())&&
						!skuVO.getOtherFixedSurcharge().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOtherFixedSurcharge()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getOtherStdFSDollar())&& 
						!skuVO.getOtherStdFSDollar().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOtherStdFSDollar()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getOutlet())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOutlet()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOutlet()) == Boolean.TRUE	&& 
							skuVO.getOutlet().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOutlet()) == Boolean.FALSE && 
							skuVO.getOutlet().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getPresellQuantityUnits())&& 
						!skuVO.getPresellQuantityUnits().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPresellQuantityUnits()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getPriceDisplay())&& 
						!skuVO.getPriceDisplay().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPriceDisplay()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getProductAwards())&& 
						!skuVO.getProductAwards().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getProductAwards()))) {
					return retVal = Boolean.TRUE;
				}
				if (!skuVO.getProductFeature().isEmpty() && !mapsAreEqual(skuVO.getProductFeature(), productFeatureMap)) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.PRODUCT_SAFETY_WARNING)) {
					if(!skuVO.getProductSafetyWarnings().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getProductSafetyWarnings()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getPromotionalStickerDisplay())&& 
						!skuVO.getPromotionalStickerDisplay().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPromotionalStickerDisplay()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getRegistryWarningIndicator())&&
						!skuVO.getRegistryWarningIndicator().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRegistryWarningIndicator()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getRegistryEligibility())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRegistryEligibility()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRegistryEligibility()) == Boolean.TRUE &&
							skuVO.getRegistryEligibility().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getRegistryEligibility()) == Boolean.FALSE && 
							skuVO.getRegistryEligibility().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getSafetyStock()) && 
						!skuVO.getSafetyStock().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSafetyStock()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getShipInOrigContainer())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipInOrigContainer()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipInOrigContainer()) == Boolean.TRUE && 
							skuVO.getShipInOrigContainer().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipInOrigContainer()) == Boolean.FALSE && 
							skuVO.getShipInOrigContainer().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getSupressInSearch())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSupressInSearch()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSupressInSearch()) == Boolean.TRUE && 
							skuVO.getSupressInSearch().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSupressInSearch()) == Boolean.FALSE &&
							skuVO.getSupressInSearch().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getIsDeleted())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName()) == Boolean.TRUE && 
							skuVO.getIsDeleted().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getIsDeletedPropertyName()) == Boolean.FALSE &&
							skuVO.getIsDeleted().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getSystemRequirements())	&&
						!skuVO.getSystemRequirements().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSystemRequirements()))) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.UPC_NUMBER)) {
					if(!skuVO.getUpcNumbers().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getUpcNumbers()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getNmwa()) && 
						!skuVO.getNmwa().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getNmwa()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getWebDisplayFlag())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getWebDisplayFlag()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getWebDisplayFlag()) == Boolean.TRUE && 
							skuVO.getWebDisplayFlag().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getWebDisplayFlag()) == Boolean.FALSE &&
							skuVO.getWebDisplayFlag().equalsIgnoreCase(TRUProductFeedConstants.A)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getBppName()) && 
						!skuVO.getBppName().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBppName()))) {
					return retVal = Boolean.TRUE;
				}
				if (skuVO.getBppEligible() != null) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBppEligible()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBppEligible()) == Boolean.TRUE && 
							!skuVO.getBppEligible()) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBppEligible()) == Boolean.FALSE && 
							skuVO.getBppEligible()) {
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getLegalNotice()) && 
						!skuVO.getLegalNotice().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getLegalNotice()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getSwatchImage()) && 
						!skuVO.getSwatchImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSwatchImage()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getPrimaryImage()) && 
						!skuVO.getPrimaryImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPrimaryImage()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getSecondaryImage()) &&
						!skuVO.getSecondaryImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSecondaryImage()))) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.ALTERNATE_IMAGE)) {
					if(!skuVO.getAlternateImage().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getAlternateImage()))){
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getSwatchCanonicalImage()) && 
						!skuVO.getSwatchCanonicalImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSwatchCanonicalImage()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getPrimaryCanonicalImage()) && 
						!skuVO.getPrimaryCanonicalImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPrimaryCanonicalImage()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getSecondaryCanonicalImage()) &&
						!skuVO.getSecondaryCanonicalImage().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSecondaryCanonicalImage()))) {
					return retVal = Boolean.TRUE;
				}
				
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.CANONICAL_IMAGE_URL)) {
					if(!skuVO.getAlternateCanonicalImage().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getAlternateCanonicalImage()))){
						return retVal = Boolean.TRUE;
					}
				}
				
				if (!StringUtils.isBlank(skuVO.getSlapperItem())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSlapperItem()) == null) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSlapperItem()) == Boolean.TRUE && 
							skuVO.getSlapperItem().equalsIgnoreCase(TRUProductFeedConstants.N)) {
						return retVal = Boolean.TRUE;
					} else if ((Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSlapperItem()) == Boolean.FALSE &&
							skuVO.getSlapperItem().equalsIgnoreCase(TRUProductFeedConstants.Y)) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!skuVO.getCrossSellSkuId().isEmpty() && 
						!skuVO.getCrossSellSkuId().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getCrossSellSkuId()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getShipFromStoreEligibleLov()) && 
						!skuVO.getShipFromStoreEligibleLov().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipFromStoreEligibleLov()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getVendorsProductDemoUrl()) 	&&
						!skuVO.getVendorsProductDemoUrl().equals(
								pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getShipToStoreEeligible())) {
					if (pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible()) == null) {
						return retVal = Boolean.TRUE;
					} else if (skuVO.getShipToStoreEeligible().equalsIgnoreCase(TRUProductFeedConstants.YES)
							&& (Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible()) == Boolean.FALSE) {
						return retVal = Boolean.TRUE;
					} else if (skuVO.getShipToStoreEeligible().equalsIgnoreCase(TRUProductFeedConstants.NO)
							&& (Boolean) pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getShipToStoreEeligible()) == Boolean.TRUE) {
						return retVal = Boolean.TRUE;
					}
				}
				if (!StringUtils.isBlank(skuVO.getOrignalParentProduct().getId()) && 
						!skuVO.getOrignalParentProduct().getId()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getOrignalParentProduct()))) {
					return retVal = Boolean.TRUE;
				}
				if (skuVO.getUpdatedListProps().contains(TRUProductFeedConstants.PARENT_CLASSIFICATIONS)
						&& !skuVO.getParentClassifications().equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getParentClassifications()))) {
					return retVal = Boolean.TRUE;
				}
				if (skuVO.getCrossSellsnUidBatteriesList() != null && !skuVO.getCrossSellsnUidBatteriesList().isEmpty()
						&& checkCrosssellBatteriesItemsForUpdate(skuVO.getCrossSellsnUidBatteriesList(), crosssellBatteries)) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getDivisionCode()) && 
						!skuVO.getDivisionCode()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDivisionCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getDepartmentCode()) && 
						!skuVO.getDepartmentCode()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getDepartmentCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getClassCode()) && 
						!skuVO.getClassCode()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getClassCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getSubclassCode()) && 
						!skuVO.getSubclassCode()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSubclassCode()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getSkills()) && 
						!skuVO.getSkills()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getSkills()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getPrimaryCategory()) && 
						!skuVO.getPrimaryCategory()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPrimaryCategory()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getPlayType()) && 
						!skuVO.getPlayType()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPlayType()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getWhatIsImportant()) && 
						!skuVO.getWhatIsImportant()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getWhatIsImportant()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getBrandNamePrimary()) && 
						!skuVO.getBrandNamePrimary()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getBrandNamePrimary()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getChannelAvailability()) &&
						 !skuVO.getChannelAvailability().equals(
								 pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getChannelAvailability()))) {
					return retVal = Boolean.TRUE;
				}
				if (!StringUtils.isBlank(skuVO.getItemInStorePickUp()) &&
						 !skuVO.getItemInStorePickUp().equals(
								 pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getItemInStorePickUp()))) {
					return retVal = Boolean.TRUE;
				}
				
				if(!skuVO.getEmptyNullProperties().isEmpty() || !skuVO.getEmptyNullSubProperties().isEmpty()){
					return retVal = Boolean.TRUE;
				}
				
				if (!StringUtils.isBlank(skuVO.getPrimaryPathCategory()) && 
						!skuVO.getPrimaryPathCategory()
								.equals(pRepositoryItem.getPropertyValue(getFeedCatalogProperty().getPrimaryPathCategory()))) {
					return retVal = Boolean.TRUE;
				}
				
			} catch (Exception exception) {
				getLogger().vlogDebug(TRUFeedConstants.EXCEPTION + skuVO.getRepositoryId());
				getLogger().vlogError(TRUFeedConstants.EXCEPTION, exception);
				String message = exception.getMessage();
				if(StringUtils.isBlank(message) && exception.getCause()!=null){
					message=exception.getCause().getMessage();
				}
				skuVO.setErrorMessage(TRUProductFeedConstants.REPOSITORY_EXCEPTION+TRUProductFeedConstants.SEPARATOR+message);
				throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_MAX_SIZE, null, exception);
			}
		}

		getLogger().vlogDebug("End @Class: TRUCommonSKUItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}

	/**
	 * Check crosssell batteries items for update.
	 * 
	 * @param pVOItems
	 *            the VO items
	 * @param pRepoItems
	 *            the repo items
	 * @return true, if successful
	 */
	private boolean checkCrosssellBatteriesItemsForUpdate(List<TRUCrossSellsnUidBatteriesVO> pVOItems,
			List<RepositoryItem> pRepoItems) {

		getLogger().vlogDebug(
				"Start @Class: TRUCommonSKUItemValueChangedInspector, @method: checkCrosssellBatteriesItemsForUpdate()");

		boolean isUpdate = Boolean.FALSE;
		if (pVOItems != null && !pVOItems.isEmpty() && pRepoItems != null && !pRepoItems.isEmpty()) {
			if (pVOItems.size() == pRepoItems.size()) {
				List<TRUCrossSellsnUidBatteriesVO> repositoryItems = new ArrayList<TRUCrossSellsnUidBatteriesVO>();
				for (RepositoryItem item : pRepoItems) {
					TRUCrossSellsnUidBatteriesVO crossSellsnUidBatteriesVO = new TRUCrossSellsnUidBatteriesVO();
					if(item.getPropertyValue(getFeedCatalogProperty().getCrossSellsBatteries()) != null){
						crossSellsnUidBatteriesVO.setCrossSellsBatteries((String) item.getPropertyValue(getFeedCatalogProperty()
								.getCrossSellsBatteries()));
					}
					if(item.getPropertyValue(getFeedCatalogProperty().getUidBatteryCrossSell()) != null){
						crossSellsnUidBatteriesVO.setUidBatteryCrossSell((String) item.getPropertyValue(getFeedCatalogProperty()
								.getUidBatteryCrossSell()));
					}
					repositoryItems.add(crossSellsnUidBatteriesVO);
				}
				int size = pVOItems.size();
				for (int i = TRUProductFeedConstants.NUMBER_ZERO; i < size; i++) {
					TRUCrossSellsnUidBatteriesVO entity = pVOItems.get(i);
					TRUCrossSellsnUidBatteriesVO repository = repositoryItems.get(i);
					if (entity != null
							&& repository != null
							&& (entity.getCrossSellsBatteries() != null && !entity.getCrossSellsBatteries().equalsIgnoreCase(
									repository.getCrossSellsBatteries()))
							|| (entity.getUidBatteryCrossSell() != null && !entity.getUidBatteryCrossSell().equalsIgnoreCase(
									repository.getUidBatteryCrossSell()))) {
						isUpdate = Boolean.TRUE;
						break;
					} else {
						isUpdate = Boolean.FALSE;
					}
				}

			} else {
				isUpdate = Boolean.TRUE;
			}
		} else if(pVOItems != null && pRepoItems != null && !pVOItems.isEmpty() && pRepoItems.isEmpty()){
			isUpdate = Boolean.TRUE;
		}
		getLogger().vlogDebug(
				"End @Class: TRUCommonSKUItemValueChangedInspector, @method: checkCrosssellBatteriesItemsForUpdate()");

		return isUpdate;

	}

	/**
	 * Check display order property.
	 * 
	 * @param pVoProperty
	 *            the vo property
	 * @param pItemProperty
	 *            the item property
	 * @return true, if successful
	 */
	private boolean checkDisplayOrderProperty(String pVoProperty, Float pItemProperty) {
		if (pVoProperty == null && pItemProperty == null) {
			return true;
		} else if ((pVoProperty != null && pItemProperty != null && Float.parseFloat(pVoProperty) != pItemProperty)
				|| (pVoProperty != null && pItemProperty == null)) {
			return false;
		}

		return true;
	}

	/**
	 * This method is used to check the maps are equal or not.
	 * 
	 * @param pVOMap
	 *            the VO map
	 * @param pRepoMap
	 *            the repo map
	 * @return true, if successful
	 */
	public boolean mapsAreEqual(Map<String, String> pVOMap, Map<String, String> pRepoMap) {

		getLogger().vlogDebug("Start @Class: TRUCommonSKUItemValueChangedInspector, @method: mapsAreEqual()");

		if (pRepoMap != null && !pRepoMap.isEmpty()) {
			Set<String> repoMapKeySet = pRepoMap.keySet();
			Set<String> voMapKeySet = pVOMap.keySet();
			Collection<String> voMapValues = pVOMap.values();
			Collection<String> repoMapValues2 = pRepoMap.values();
			return voMapKeySet.containsAll(repoMapKeySet) && voMapValues.containsAll(repoMapValues2);
		} else if (!pVOMap.isEmpty() && pRepoMap.isEmpty()) {
			return Boolean.FALSE;
		}

		getLogger().vlogDebug("End @Class: TRUCommonSKUItemValueChangedInspector, @method: mapsAreEqual()");

		return Boolean.TRUE;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
      * Checks if is logging error.
      *
      * @return true/false
      */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
