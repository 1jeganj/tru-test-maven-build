package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class TRUStyleErrorVO.
 */
public class TRUStyleErrorVO {
	
	/** The Id. */
	private String mId;
	
	/** The Skus not available. */
	private List<String> mSkusNotAvailable;
	
	/** The Skus not associated. */
	private List<String> mSkusNotAssociated;
	
	/** property to hold FileName . */
	private String mFileName;
	
	/** The Version. */
	private String mVersion;
	

	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * Sets the version.
	 *
	 * @param pVersion the version to set
	 */
	public void setVersion(String pVersion) {
		mVersion = pVersion;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 *
	 * @param pId the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}

	/**
	 * Gets the skus not available.
	 *
	 * @return the skusNotAvailable
	 */
	public List<String> getSkusNotAvailable() {
		if(mSkusNotAvailable == null){
			mSkusNotAvailable = new ArrayList<String>();
		}
		return mSkusNotAvailable;
	}

	/**
	 * Sets the skus not available.
	 *
	 * @param pSkusNotAvailable the skusNotAvailable to set
	 */
	public void setSkusNotAvailable(List<String> pSkusNotAvailable) {
		mSkusNotAvailable = pSkusNotAvailable;
	}

	/**
	 * Gets the skus not associated.
	 *
	 * @return the skusNotAssociated
	 */
	public List<String> getSkusNotAssociated() {
		if(mSkusNotAssociated == null){
			mSkusNotAssociated = new ArrayList<String>();
		}
		return mSkusNotAssociated;
	}

	/**
	 * Sets the skus not associated.
	 *
	 * @param pSkusNotAssociated the skusNotAssociated to set
	 */
	public void setSkusNotAssociated(List<String> pSkusNotAssociated) {
		mSkusNotAssociated = pSkusNotAssociated;
	}
	
	
}
