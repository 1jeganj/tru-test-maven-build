package com.tru.feedprocessor.tol.catalog;

/**
 * This class is a property manager. It extends the atg.commerce.catalog.custom.CatalogProperties to add the Feed properties.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedCatalogProperty {

	/**
	 * The variable to hold "ID" property name.
	 */
	private String mId;

	/**
	 * The variable to hold "SKU" property name.
	 */
	private String mSKUName;

	/**
	 * The variable to hold "PriceList" property name.
	 */
	private String mPriceListName;

	/**
	 * The variable to hold "childSKUs" property name.
	 */
	private String mChildSKUsName;

	/**
	 * The variable to hold "fixedChildCategories" property name.
	 */
	private String mFixedChildCategoriesName;
	/**
	 * The variable to hold "isLookCategory" property name.
	 */
	private String mIsLookCategoryPropertyName;

	/**
	 * The variable to hold "isLookCategory" property name.
	 */
	private String mCompleteTheLookPropertyName;

	/**
	 * The variable to hold "template" property name.
	 */
	private String mProductTemplateProperty;

	/**
	 * The variable to hold "URL" property name.
	 */
	private String mProductTemplateURLProperty;

	/**
	 * The variable to hold "CreationDatePropertyName" property name.
	 */
	private String mCreationDatePropertyName;

	/**
	 * The variable to hold "productDisplayName" property name.
	 */
	private String mProductDisplayName;

	/**
	 * The variable to hold "brand" item descriptor name.
	 */
	private String mBrandItemDescriptorName;

	/**
	 * The variable to hold "promotionalContent" item descriptor name.
	 */
	private String mPromotionalContentItemDescriptorName;
	/**
	 * The variable to hold "designer" item descriptor name.
	 */
	private String mDesignerItemDescriptorName;

	/**
	 * The variable to hold "itemID" property name.
	 */
	private String mItemIdPropertyName;

	/**
	 * The variable to hold "associatedCategoryPropertyName" property name.
	 */
	private String mAssociatedCategoryPropertyName;
	/**
	 * The variable to hold "mimicSkuID" property name.
	 */
	private String mMimicSkuIdPropertyName;
	/**
	 * The variable to hold "enabled" property name.
	 */
	private String mEnabledPropertyName;
	/**
	 * The variable to hold "changed" property name.
	 */
	private String mChangedPropertyName;

	/**
	 * The variable to hold "collection" property name.
	 */
	private String mCollectionPropertyName;

	/** The Interest. */
	private String mInterest;

	/**
	 * The variable holds lastModifiedDate property name.
	 */
	private String mLastModifiedDatePropertyName;

	/**
	 * The variable holds mProfileItemPropertyName property name.
	 */
	private String mProfileItemPropertyName;

	/**
	 * The variable holds mEstiloClassificationNamePropertyName property name.
	 */
	private String mEstiloClassificationNamePropertyName;

	/**
	 * The variable holds mClassificationNamePropertyName property name.
	 */
	private String mClassificationNamePropertyName;

	/**
	 * The variable holds mTitleImage property name.
	 */
	private String mTitleImage;

	/**
	 * The variable holds mDisplayNameDefault property name.
	 */
	private String mDisplayNameDefault;

	/**
	 * The variable holds mStoreIdPropertyName property name.
	 */
	private String mStoreIdPropertyName;

	/**
	 * The variable holds mPropertyName property name.
	 */
	private String mPropertyName;

	/**
	 * The variable holds mIsNewSkuPropertyName property name.
	 */
	private String mIsNewSkuPropertyName;

	/**
	 * The variable holds mIsOnsaleSkuPropertyName property name.
	 */
	private String mIsOnsaleSkuPropertyName;

	/** holds the reference of mListPricePropertyName. */
	private String mListPricePropertyName;

	/** holds the reference of mSalePricePropertyName. */
	private String mSalePricePropertyName;

	/** holds the reference of mCategoryName. */
	private String mCategoryName;

	// Properties for Product Feed
	/** The m battery included. */
	private String mBatteryIncluded;

	/** The m battery required. */
	private String mBatteryRequired;

	/** The m brand name primary. */
	private String mBrandNamePrimary;

	/** The m channel availability. */
	private String mChannelAvailability;

	/** The m cost. */
	private String mCost;

	/** The m ewaste surcharge sku. */
	private String mEwasteSurchargeSku;

	/** The m ewaste surcharge state. */
	private String mEwasteSurchargeState;

	/** The m gender. */
	private String mGender;

	/** The m item in store pick up. */
	private String mItemInStorePickUp;

	/** The m rus item number. */
	private String mRusItemNumber;

	/** The m ship from store eligible. */
	private String mShipFromStoreEligible;

	/** The m special assembly instructions. */
	private String mSpecialAssemblyInstructions;

	/** The m tax code. */
	private String mTaxCode;

	/** The m tax product value code. */
	private String mTaxProductValueCode;

	/** The m manufacturer style number. */
	private String mManufacturerStyleNumber;

	/** The m vendors product demo url. */
	private String mVendorsProductDemoUrl;

	/** The m non merchandise flag. */
	private String mNonMerchandiseFlag;

	/** The m country eligibility. */
	private String mCountryEligibility;

	/** The m registry classification. */
	private String mRegistryClassification;

	/** The m vendor classification. */
	private String mVendorClassification;

	// properties for sku
	/** The m akhi map. */
	private String mAkhiMap;
	
	/** The Online collection name. */
	private String mOnlineCollectionName;

	/** The m assembly map. */
	private String mAssemblyMap;

	/** The m back order status. */
	private String mBackOrderStatus;

	/** The m brand name secondary. */
	private String mBrandNameSecondary;

	/** The m brand name tertiary char theme. */
	private String mBrandNameTertiaryCharTheme;

	/** The m browse hidden keyword. */
	private String mBrowseHiddenKeyword;

	/** The m can be gift wrapped. */
	private String mCanBeGiftWrapped;

	/** The m car seat features. */
	private String mCarSeatFeatures;

	/** The m car seat types. */
	private String mCarSeatTypes;

	/** The m car seat what is imp. */
	private String mCarSeatWhatIsImp;

	/** The m child weight max. */
	private String mChildWeightMax;

	/** The m child weight min. */
	private String mChildWeightMin;

	/** The m color code. */
	private String mColorCode;

	/** The m color upc level. */
	private String mColorUpcLevel;

	/** The m crib material types. */
	private String mCribMaterialTypes;

	/** The m crib styles. */
	private String mCribStyles;

	/** The m crib types. */
	private String mCribTypes;

	/** The m crib what is imp. */
	private String mCribWhatIsImp;

	/** The m customer purchase limit. */
	private String mCustomerPurchaseLimit;

	/** The m book cd dvd properties map. */
	private String mBookCdDvdPropertiesMap;

	/** The m dropship flag. */
	private String mDropShipFlag;

	/** The m flexible shipping plan. */
	private String mFlexibleShippingPlan;

	/** The m freight class. */
	private String mFreightClass;

	/** The m furniture finish. */
	private String mFurnitureFinish;

	/** The m incremental safety stock units. */
	private String mIncrementalSafetyStockUnits;

	/** The m item dimension map. */
	private String mItemDimensionMap;

	/** The m juvenile product size. */
	private String mJuvenileProductSize;

	/** The m lower48 map. */
	private String mLower48Map;

	/** The m max target age tru websites. */
	private String mMaxTargetAgeTruWebsites;

	/** The m mfr suggested age max. */
	private String mMfrSuggestedAgeMax;

	/** The m mfr suggested age min. */
	private String mMfrSuggestedAgeMin;

	/** The m min target age tru websites. */
	private String mMinTargetAgeTruWebsites;

	/** The m nmwa percentage. */
	private String mNmwaPercentage;

	/** The m ship window max. */
	private String mShipWindowMax;

	/** The m ship window min. */
	private String mShipWindowMin;

	/** The m online pid. */
	private String mOnlinePID;

	/** The m original pid. */
	private String mOriginalPID;

	/** The street date. */
	private String mStreetDate;
	
	/** The Inventory received date. */
	private String mInventoryReceivedDate;

	/** The m other fixed surcharge. */
	private String mOtherFixedSurcharge;

	/** The m other std fs dollar. */
	private String mOtherStdFSDollar;

	/** The m outlet. */
	private String mOutlet;

	/** The m presell quantity units. */
	private String mPresellQuantityUnits;

	/** The m price display. */
	private String mPriceDisplay;

	/** The m product awards. */
	private String mProductAwards;

	/** The m product feature. */
	private String mProductFeature;

	/** The m product safety warnings. */
	private String mProductSafetyWarnings;

	/** The m promotional sticker display. */
	private String mPromotionalStickerDisplay;

	/** The m registry warning indicator. */
	private String mRegistryWarningIndicator;

	/** The m registry eligibility. */
	private String mRegistryEligibility;

	/** The m safety stock. */
	private String mSafetyStock;

	/** The m ship in orig container. */
	private String mShipInOrigContainer;

	/** The m strl best uses. */
	private String mStrlBestUses;

	/** The m strl extras. */
	private String mStrlExtras;

	/** The m strl types. */
	private String mStrlTypes;

	/** The m strl what is imp. */
	private String mStrlWhatIsImp;

	/** The m supress in search. */
	private String mSupressInSearch;

	/** The m system requirements. */
	private String mSystemRequirements;

	/** The m upc number. */
	private String mUpcNumbers;

	/** The m video game esrb. */
	private String mVideoGameEsrb;

	/** The m video game genre. */
	private String mVideoGameGenre;

	/** The m video game platform. */
	private String mVideoGamePlatform;

	/** The m nmwa. */
	private String mNmwa;

	/** The m web display flag. */
	private String mWebDisplayFlag;

	/** The m bpp eligible. */
	private String mBppEligible;

	/** The m bpp name. */
	private String mBppName;

	/** The m legal notice. */
	private String mLegalNotice;

	/** The m swatch image. */
	private String mSwatchImage;

	/** The m primary image. */
	private String mPrimaryImage;

	/** The m secondary image. */
	private String mSecondaryImage;

	/** The m alternate image. */
	private String mAlternateImage;
	
	/** property to hold SwatchCanonicalImage. */
	private String mSwatchCanonicalImage;

	/** property to hold PrimaryCanonicalImage. */
	private String mPrimaryCanonicalImage;

	/** property to hold SecondaryCanonicalImage. */
	private String mSecondaryCanonicalImage;

	/** property to hold AlternateCanonicalImage. */
	private String mAlternateCanonicalImage;

	/** The Crosssell battery. */
	private String mCrosssellBattery;

	/** The crosssell batteries. */
	private String mCrosssellBatteries;

	/** The Battery. */
	private String mBattery;

	/** The batteries. */
	private String mBatteries;

	/** The Cross sells batteries. */
	private String mCrossSellsBatteries;

	/** The Uid battery cross sell. */
	private String mUidBatteryCrossSell;

	/** The Battery type. */
	private String mBatteryType;

	/** The Battery quantity. */
	private String mBatteryQuantity;

	/** The m cross sell sku id. */
	private String mCrossSellSkuId;

	/** The m slapper item. */
	private String mSlapperItem;
	
	/** property to hold ShipFromStoreEligibleLov. */
	private String mShipFromStoreEligibleLov;

	/** The m ship to store eeligible. */
	private String mShipToStoreEeligible;

	/** The m super display flag. */
	private String mSuperDisplayFlag;

	/** The m review rating. */
	private String mReviewRating;

	/** The m review count. */
	private String mReviewCount;

	/** The m root parent categories. */
	private String mRootParentCategories;

	/** The m parent classifications. */
	private String mParentClassifications;

	/** The m car seat properties map. */
	private String mCarSeatPropertiesMap;

	/** The m stroller properties map. */
	private String mStrollerPropertiesMap;

	/** The m crib properties map. */
	private String mCribPropertiesMap;

	/** The m orignal parent product. */
	private String mOrignalParentProduct;
	
	/** property to DivisionCode Type. */
	private String mDivisionCode;
	
	/** property to DepartmentCode Type. */
	private String mDepartmentCode;
	
	/** property to ClassCode Type. */
	private String mClassCode;
	
	/** property to SubclassCode Type. */
	private String mSubclassCode;
	
	// properties for Classifications
	/**
	 * The variable holds mClassificationId property name.
	 */
	private String mClassificationIdPropertyName;
	/**
	 * The variable holds mNameProperty property name.
	 */
	private String mNamePropertyName;
	/**
	 * The variable holds userTypeId property name.
	 */
	private String mUserTypeIdPropertyName;
	/**
	 * The variable holds displayOrder property name.
	 */
	private String mDisplayOrderPropertyName;
	/**
	 * The variable holds haveValue property name.
	 */
	private String mHaveValuePropertyName;
	/**
	 * The variable holds suggestedQuantity property name.
	 */
	private String mSuggestedQuantityPropertyName;
	/**
	 * The variable holds classificationType property name.
	 */
	private String mClassificationTypePropertyName;
	/**
	 * The variable holds parentCategory property name.
	 */
	private String mParentCategoryPropertyName;
	/**
	 * The variable holds rootParentCategory property name.
	 */
	private String mRootParentCategoryPropertyName;
	/**
	 * The variable holds childClassifications property name.
	 */
	private String mChildClassificationsPropertyName;
	/**
	 * The variable holds classification property name.
	 */
	private String mClassificationPropertyName;
	/**
	 * The variable holds crossReference property name.
	 */
	private String mCrossReferencePropertyName;
	/**
	 * The variable holds parentClassifications property name.
	 */
	private String mParentClassificationsPropertyName;
	/**
	 * The variable holds longDescription property name.
	 */
	private String mLongDescriptionPropertyName;
	/**
	 * The variable holds displayStatus property name.
	 */
	private String mDisplayStatusPropertyName;

	/**
	 * The variable holds Parent Categories property name.
	 */
	private String mParentCategories;

	/** The variable holds the delete property. */
	private String mIsDeletedPropertyName;
	/**
	 * The variable holds Size Chart Name property name.
	 */
	private String mSizeChartName;

	/**
	 * The variable holds Style Dimensions property name.
	 */
	private String mStyleDimensions;

	/**
	 * The variable holds subType property name.
	 */
	private String mSubType;

	/**
	 * The variable holds catalogRefId property name.
	 */
	private String mCatalogRefId;

	/**
	 * The variable holds preorderLevel property name.
	 */
	private String mPreorderLevel;

	/**
	 * The variable holds inventory property name.
	 */
	private String mInventory;

	/**
	 * The variable holds locationId property name.
	 */
	private String mLocationId;
	
	/**
	 * The variable holds productHTGITMsg property name.
	 */
	private String mProductHTGITMsg;
	
	/**
	 * The variable holds childProducts property name.
	 */
	private String mChildProducts;
	
	/**
	 * The variable holds CollectionImage property name.
	 */
	private String mCollectionImage;
	
	/**
	 * The variable holds parentProducts property name.
	 */
	private String mParentProducts;
	
	/**
	 * The variable holds finderEligible property name.
	 */
	private String mFinderEligible;
	
	/**
	 * The variable holds specialFeatures property name.
	 */
	private String mSpecialFeatures;
	
	/**
	 * The variable holds mColorNamePropertyName property name.
	 */
	private String mColorNamePropertyName;
	
	/**
	 * The variable holds mSizeDescriptionPropertyName property name.
	 */
	private String mSizeDescriptionPropertyName;
	
	/**
	 * The variable holds RmsSizeCode property name.
	 */
	private String mRmsSizeCode;
	
	/**
	 * The variable holds RmsColorCode property name.
	 */
	private String mRmsColorCode;
	
	/** The variable holds Skills property name. */
	private String mSkills;
	
	/** The variable holds PrimaryCategory property name. */
	private String mPrimaryCategory;
	
	/** The variable holds PlayType property name. */
	private String mPlayType;
	
	/** The variable holds WhatIsImportant property name. */
	private String mWhatIsImportant;
	
	/** The Cross ref display order property name. */
	private String mCrossRefDisplayOrderPropertyName;
	
	/** The Cross ref display name property name. */
	private String mCrossRefDisplayNamePropertyName;
	
	/** The variable holds Brand property name. */
	private String mBrand;
	
	/** The variable holds WhyWeLoveIt property name. */
	private String mWhyWeLoveIt;
	
	/** The variable holds PrimaryParentCategory property name. */
	private String mPrimaryParentCategory;
	
	/** The variable holds ColorFamily property name. */
	private String mColorFamily;
	
	/** The variable holds SizeFamily property name. */
	private String mSizeFamily;
	
	/** The variable holds CharacterTheme property name. */
	private String mCharacterTheme;
	
	/** The variable holds CollectionTheme property name. */
	private String mCollectionTheme;
	
	
	/**
	 * Gets the skills.
	 *
	 * @return the skills
	 */
	public String getSkills() {
		return mSkills;
	}

	/**
	 * Sets the skills.
	 *
	 * @param pSkills the skills to set
	 */
	public void setSkills(String pSkills) {
		mSkills = pSkills;
	}

	/**
	 * Gets the primary category.
	 *
	 * @return the primaryCategory
	 */
	public String getPrimaryCategory() {
		return mPrimaryCategory;
	}

	/**
	 * Sets the primary category.
	 *
	 * @param pPrimaryCategory the primaryCategory to set
	 */
	public void setPrimaryCategory(String pPrimaryCategory) {
		mPrimaryCategory = pPrimaryCategory;
	}

	/**
	 * Gets the play type.
	 *
	 * @return the playType
	 */
	public String getPlayType() {
		return mPlayType;
	}

	/**
	 * Sets the play type.
	 *
	 * @param pPlayType the playType to set
	 */
	public void setPlayType(String pPlayType) {
		mPlayType = pPlayType;
	}

	/**
	 * Gets the what is important.
	 *
	 * @return the whatIsImportant
	 */
	public String getWhatIsImportant() {
		return mWhatIsImportant;
	}

	/**
	 * Sets the what is important.
	 *
	 * @param pWhatIsImportant the whatIsImportant to set
	 */
	public void setWhatIsImportant(String pWhatIsImportant) {
		mWhatIsImportant = pWhatIsImportant;
	}
	
	
	

	/**
	 * Gets the swatch canonical image.
	 *
	 * @return the swatchCanonicalImage
	 */
	public String getSwatchCanonicalImage() {
		return mSwatchCanonicalImage;
	}

	/**
	 * Sets the swatch canonical image.
	 *
	 * @param pSwatchCanonicalImage the swatchCanonicalImage to set
	 */
	public void setSwatchCanonicalImage(String pSwatchCanonicalImage) {
		mSwatchCanonicalImage = pSwatchCanonicalImage;
	}

	/**
	 * Gets the primary canonical image.
	 *
	 * @return the primaryCanonicalImage
	 */
	public String getPrimaryCanonicalImage() {
		return mPrimaryCanonicalImage;
	}

	/**
	 * Sets the primary canonical image.
	 *
	 * @param pPrimaryCanonicalImage the primaryCanonicalImage to set
	 */
	public void setPrimaryCanonicalImage(String pPrimaryCanonicalImage) {
		mPrimaryCanonicalImage = pPrimaryCanonicalImage;
	}

	/**
	 * Gets the secondary canonical image.
	 *
	 * @return the secondaryCanonicalImage
	 */
	public String getSecondaryCanonicalImage() {
		return mSecondaryCanonicalImage;
	}

	/**
	 * Sets the secondary canonical image.
	 *
	 * @param pSecondaryCanonicalImage the secondaryCanonicalImage to set
	 */
	public void setSecondaryCanonicalImage(String pSecondaryCanonicalImage) {
		mSecondaryCanonicalImage = pSecondaryCanonicalImage;
	}

	/**
	 * Gets the alternate canonical image.
	 *
	 * @return the alternateCanonicalImage
	 */
	public String getAlternateCanonicalImage() {
		return mAlternateCanonicalImage;
	}

	/**
	 * Sets the alternate canonical image.
	 *
	 * @param pAlternateCanonicalImage the alternateCanonicalImage to set
	 */
	public void setAlternateCanonicalImage(String pAlternateCanonicalImage) {
		mAlternateCanonicalImage = pAlternateCanonicalImage;
	}

	/**
	 * Gets the division code.
	 *
	 * @return the divisionCode
	 */
	public String getDivisionCode() {
		return mDivisionCode;
	}

	/**
	 * Sets the division code.
	 *
	 * @param pDivisionCode the divisionCode to set
	 */
	public void setDivisionCode(String pDivisionCode) {
		mDivisionCode = pDivisionCode;
	}

	/**
	 * Gets the department code.
	 *
	 * @return the departmentCode
	 */
	public String getDepartmentCode() {
		return mDepartmentCode;
	}

	/**
	 * Sets the department code.
	 *
	 * @param pDepartmentCode the departmentCode to set
	 */
	public void setDepartmentCode(String pDepartmentCode) {
		mDepartmentCode = pDepartmentCode;
	}

	/**
	 * Gets the class code.
	 *
	 * @return the classCode
	 */
	public String getClassCode() {
		return mClassCode;
	}

	/**
	 * Sets the class code.
	 *
	 * @param pClassCode the classCode to set
	 */
	public void setClassCode(String pClassCode) {
		mClassCode = pClassCode;
	}

	/**
	 * Gets the subclass code.
	 *
	 * @return the subclassCode
	 */
	public String getSubclassCode() {
		return mSubclassCode;
	}

	/**
	 * Sets the subclass code.
	 *
	 * @param pSubclassCode the subclassCode to set
	 */
	public void setSubclassCode(String pSubclassCode) {
		mSubclassCode = pSubclassCode;
	}

	/**
	 * Gets the rms size code.
	 *
	 * @return the rmsSizeCode
	 */
	public String getRmsSizeCode() {
		return mRmsSizeCode;
	}

	/**
	 * Sets the rms size code.
	 *
	 * @param pRmsSizeCode the rmsSizeCode to set
	 */
	public void setRmsSizeCode(String pRmsSizeCode) {
		mRmsSizeCode = pRmsSizeCode;
	}

	/**
	 * Gets the rms color code.
	 *
	 * @return the rmsColorCode
	 */
	public String getRmsColorCode() {
		return mRmsColorCode;
	}

	/**
	 * Sets the rms color code.
	 *
	 * @param pRmsColorCode the rmsColorCode to set
	 */
	public void setRmsColorCode(String pRmsColorCode) {
		mRmsColorCode = pRmsColorCode;
	}

	/**
	 * Gets the color name property name.
	 *
	 * @return the colorNamePropertyName
	 */
	public String getColorNamePropertyName() {
		return mColorNamePropertyName;
	}

	/**
	 * Sets the color name property name.
	 *
	 * @param pColorNamePropertyName the colorNamePropertyName to set
	 */
	public void setColorNamePropertyName(String pColorNamePropertyName) {
		mColorNamePropertyName = pColorNamePropertyName;
	}

	/**
	 * Gets the size description property name.
	 *
	 * @return the sizeDescriptionPropertyName
	 */
	public String getSizeDescriptionPropertyName() {
		return mSizeDescriptionPropertyName;
	}

	/**
	 * Sets the size description property name.
	 *
	 * @param pSizeDescriptionPropertyName the sizeDescriptionPropertyName to set
	 */
	public void setSizeDescriptionPropertyName(String pSizeDescriptionPropertyName) {
		mSizeDescriptionPropertyName = pSizeDescriptionPropertyName;
	}

	/**
	 * Gets the finder eligible.
	 *
	 * @return the finderEligible
	 */
	public String getFinderEligible() {
		return mFinderEligible;
	}

	/**
	 * Sets the finder eligible.
	 *
	 * @param pFinderEligible the finderEligible to set
	 */
	public void setFinderEligible(String pFinderEligible) {
		mFinderEligible = pFinderEligible;
	}

	/**
	 * Gets the special features.
	 *
	 * @return the specialFeatures
	 */
	public String getSpecialFeatures() {
		return mSpecialFeatures;
	}

	/**
	 * Sets the special features.
	 *
	 * @param pSpecialFeatures the specialFeatures to set
	 */
	public void setSpecialFeatures(String pSpecialFeatures) {
		mSpecialFeatures = pSpecialFeatures;
	}

	/**
	 * Gets the parent products.
	 *
	 * @return the parentProducts
	 */
	public String getParentProducts() {
		return mParentProducts;
	}
	
	/**
	 * Sets the parent products.
	 *
	 * @param pParentProducts the parentProducts to set
	 */
	public void setParentProducts(String pParentProducts) {
		mParentProducts = pParentProducts;
	}
	
	/**
	 * Gets the CollectionImage.
	 *
	 * @return the CollectionImage
	 */
	public String getCollectionImage() {
		return mCollectionImage;
	}
	/**
	 * Sets the CollectionImage.
	 *
	 * @param pCollectionImage the CollectionImage
	 */
	public void setCollectionImage(String pCollectionImage) {
		this.mCollectionImage = pCollectionImage;
	}

		/**

	 * Gets the child products.
	 *
	 * @return the child products
	 */
	public String getChildProducts() {
		return mChildProducts;
	}

	/**
	 * Sets the child products.
	 *
	 * @param pChildProducts the new child products
	 */
	public void setChildProducts(String pChildProducts) {
		this.mChildProducts = pChildProducts;
	}


	/**
	 * Gets the product htgit msg.
	 *
	 * @return the productHTGITMsg
	 */
	public String getProductHTGITMsg() {
		return mProductHTGITMsg;
	}

	/**
	 * Sets the product htgit msg.
	 *
	 * @param pProductHTGITMsg the productHTGITMsg to set
	 */
	public void setProductHTGITMsg(String pProductHTGITMsg) {
		mProductHTGITMsg = pProductHTGITMsg;
	}

	/**
	 * Gets the location id.
	 * 
	 * @return the locationId
	 */
	public String getLocationId() {
		return mLocationId;
	}

	/**
	 * Sets the location id.
	 * 
	 * @param pLocationId
	 *            the locationId to set
	 */
	public void setLocationId(String pLocationId) {
		mLocationId = pLocationId;
	}

	/**
	 * Gets the inventory.
	 * 
	 * @return the inventory
	 */
	public String getInventory() {
		return mInventory;
	}

	/**
	 * Sets the inventory.
	 * 
	 * @param pInventory
	 *            the inventory to set
	 */
	public void setInventory(String pInventory) {
		mInventory = pInventory;
	}

	/**
	 * Gets the catalog ref id.
	 * 
	 * @return the catalogRefId
	 */
	public String getCatalogRefId() {
		return mCatalogRefId;
	}

	/**
	 * Sets the catalog ref id.
	 * 
	 * @param pCatalogRefId
	 *            the catalogRefId to set
	 */
	public void setCatalogRefId(String pCatalogRefId) {
		mCatalogRefId = pCatalogRefId;
	}

	/**
	 * Gets the preorder level.
	 * 
	 * @return the preorderLevel
	 */
	public String getPreorderLevel() {
		return mPreorderLevel;
	}

	/**
	 * Sets the preorder level.
	 * 
	 * @param pPreorderLevel
	 *            the preorderLevel to set
	 */
	public void setPreorderLevel(String pPreorderLevel) {
		mPreorderLevel = pPreorderLevel;
	}

	/**
	 * Gets the sub type.
	 * 
	 * @return the subType
	 */
	public String getSubType() {
		return mSubType;
	}

	/**
	 * Sets the sub type.
	 * 
	 * @param pSubType
	 *            the subType to set
	 */
	public void setSubType(String pSubType) {
		mSubType = pSubType;
	}

	/**
	 * Gets the size chart name.
	 * 
	 * @return the sizeChartName
	 */
	public String getSizeChartName() {
		return mSizeChartName;
	}

	/**
	 * Sets the size chart name.
	 * 
	 * @param pSizeChartName
	 *            the sizeChartName to set
	 */
	public void setSizeChartName(String pSizeChartName) {
		mSizeChartName = pSizeChartName;
	}

	/**
	 * Gets the style dimensions.
	 * 
	 * @return the styleDimensions
	 */
	public String getStyleDimensions() {
		return mStyleDimensions;
	}

	/**
	 * Sets the style dimensions.
	 * 
	 * @param pStyleDimensions
	 *            the styleDimensions to set
	 */
	public void setStyleDimensions(String pStyleDimensions) {
		mStyleDimensions = pStyleDimensions;
	}

	/**
	 * Gets the IsDeleted required.
	 * 
	 * @return the IsDeleted
	 */
	public String getIsDeletedPropertyName() {
		return mIsDeletedPropertyName;
	}

	/**
	 * Sets the IsDeleted .
	 * 
	 * @param pIsDeletedPropertyName
	 *            the new checks if is deleted property name
	 */
	public void setIsDeletedPropertyName(String pIsDeletedPropertyName) {
		this.mIsDeletedPropertyName = pIsDeletedPropertyName;
	}

	/**
	 * Gets the battery included.
	 * 
	 * @return the batteryIncluded
	 */
	public String getBatteryIncluded() {
		return mBatteryIncluded;
	}

	/**
	 * Sets the battery included.
	 * 
	 * @param pBatteryIncluded
	 *            the batteryIncluded to set
	 */
	public void setBatteryIncluded(String pBatteryIncluded) {
		mBatteryIncluded = pBatteryIncluded;
	}

	/**
	 * Gets the battery required.
	 * 
	 * @return the batteryRequired
	 */
	public String getBatteryRequired() {
		return mBatteryRequired;
	}

	/**
	 * Sets the battery required.
	 * 
	 * @param pBatteryRequired
	 *            the batteryRequired to set
	 */
	public void setBatteryRequired(String pBatteryRequired) {
		mBatteryRequired = pBatteryRequired;
	}

	/**
	 * Gets the brand name primary.
	 * 
	 * @return the brandNamePrimary
	 */
	public String getBrandNamePrimary() {
		return mBrandNamePrimary;
	}

	/**
	 * Sets the brand name primary.
	 * 
	 * @param pBrandNamePrimary
	 *            the brandNamePrimary to set
	 */
	public void setBrandNamePrimary(String pBrandNamePrimary) {
		mBrandNamePrimary = pBrandNamePrimary;
	}

	/**
	 * Gets the channel availability.
	 * 
	 * @return the channelAvailability
	 */
	public String getChannelAvailability() {
		return mChannelAvailability;
	}

	/**
	 * Sets the channel availability.
	 * 
	 * @param pChannelAvailability
	 *            the channelAvailability to set
	 */
	public void setChannelAvailability(String pChannelAvailability) {
		mChannelAvailability = pChannelAvailability;
	}

	/**
	 * Gets the cost.
	 * 
	 * @return the cost
	 */
	public String getCost() {
		return mCost;
	}

	/**
	 * Sets the cost.
	 * 
	 * @param pCost
	 *            the cost to set
	 */
	public void setCost(String pCost) {
		mCost = pCost;
	}

	/**
	 * Gets the ewaste surcharge sku.
	 * 
	 * @return the ewasteSurchargeSku
	 */
	public String getEwasteSurchargeSku() {
		return mEwasteSurchargeSku;
	}

	/**
	 * Sets the ewaste surcharge sku.
	 * 
	 * @param pEwasteSurchargeSku
	 *            the ewasteSurchargeSku to set
	 */
	public void setEwasteSurchargeSku(String pEwasteSurchargeSku) {
		mEwasteSurchargeSku = pEwasteSurchargeSku;
	}

	/**
	 * Gets the ewaste surcharge state.
	 * 
	 * @return the ewasteSurchargeState
	 */
	public String getEwasteSurchargeState() {
		return mEwasteSurchargeState;
	}

	/**
	 * Sets the ewaste surcharge state.
	 * 
	 * @param pEwasteSurchargeState
	 *            the ewasteSurchargeState to set
	 */
	public void setEwasteSurchargeState(String pEwasteSurchargeState) {
		mEwasteSurchargeState = pEwasteSurchargeState;
	}

	/**
	 * Gets the gender.
	 * 
	 * @return the gender
	 */
	public String getGender() {
		return mGender;
	}

	/**
	 * Sets the gender.
	 * 
	 * @param pGender
	 *            the gender to set
	 */
	public void setGender(String pGender) {
		mGender = pGender;
	}

	/**
	 * Gets the item in store pick up.
	 * 
	 * @return the itemInStorePickUp
	 */
	public String getItemInStorePickUp() {
		return mItemInStorePickUp;
	}

	/**
	 * Sets the item in store pick up.
	 * 
	 * @param pItemInStorePickUp
	 *            the itemInStorePickUp to set
	 */
	public void setItemInStorePickUp(String pItemInStorePickUp) {
		mItemInStorePickUp = pItemInStorePickUp;
	}

	/**
	 * Gets the rus item number.
	 * 
	 * @return the rusItemNumber
	 */
	public String getRusItemNumber() {
		return mRusItemNumber;
	}

	/**
	 * Sets the rus item number.
	 * 
	 * @param pRusItemNumber
	 *            the rusItemNumber to set
	 */
	public void setRusItemNumber(String pRusItemNumber) {
		mRusItemNumber = pRusItemNumber;
	}

	/**
	 * Gets the ship from store eligible.
	 * 
	 * @return the shipFromStoreEligible
	 */
	public String getShipFromStoreEligible() {
		return mShipFromStoreEligible;
	}

	/**
	 * Sets the ship from store eligible.
	 * 
	 * @param pShipFromStoreEligible
	 *            the shipFromStoreEligible to set
	 */
	public void setShipFromStoreEligible(String pShipFromStoreEligible) {
		mShipFromStoreEligible = pShipFromStoreEligible;
	}

	/**
	 * Gets the special assembly instructions.
	 * 
	 * @return the specialAssemblyInstructions
	 */
	public String getSpecialAssemblyInstructions() {
		return mSpecialAssemblyInstructions;
	}

	/**
	 * Sets the special assembly instructions.
	 * 
	 * @param pSpecialAssemblyInstructions
	 *            the specialAssemblyInstructions to set
	 */
	public void setSpecialAssemblyInstructions(String pSpecialAssemblyInstructions) {
		mSpecialAssemblyInstructions = pSpecialAssemblyInstructions;
	}

	/**
	 * Gets the tax code.
	 * 
	 * @return the taxCode
	 */
	public String getTaxCode() {
		return mTaxCode;
	}

	/**
	 * Sets the tax code.
	 * 
	 * @param pTaxCode
	 *            the taxCode to set
	 */
	public void setTaxCode(String pTaxCode) {
		mTaxCode = pTaxCode;
	}

	/**
	 * Gets the tax product value code.
	 * 
	 * @return the taxProductValueCode
	 */
	public String getTaxProductValueCode() {
		return mTaxProductValueCode;
	}

	/**
	 * Sets the tax product value code.
	 * 
	 * @param pTaxProductValueCode
	 *            the taxProductValueCode to set
	 */
	public void setTaxProductValueCode(String pTaxProductValueCode) {
		mTaxProductValueCode = pTaxProductValueCode;
	}

	/**
	 * Gets the manufacturer style number.
	 * 
	 * @return the manufacturerStyleNumber
	 */
	public String getManufacturerStyleNumber() {
		return mManufacturerStyleNumber;
	}

	/**
	 * Sets the manufacturer style number.
	 * 
	 * @param pManufacturerStyleNumber
	 *            the manufacturerStyleNumber to set
	 */
	public void setManufacturerStyleNumber(String pManufacturerStyleNumber) {
		mManufacturerStyleNumber = pManufacturerStyleNumber;
	}

	/**
	 * Gets the vendors product demo url.
	 * 
	 * @return the vendorsProductDemoUrl
	 */
	public String getVendorsProductDemoUrl() {
		return mVendorsProductDemoUrl;
	}

	/**
	 * Sets the vendors product demo url.
	 * 
	 * @param pVendorsProductDemoUrl
	 *            the vendorsProductDemoUrl to set
	 */
	public void setVendorsProductDemoUrl(String pVendorsProductDemoUrl) {
		mVendorsProductDemoUrl = pVendorsProductDemoUrl;
	}

	/**
	 * Gets the non merchandise flag.
	 * 
	 * @return the nonMerchandiseFlag
	 */
	public String getNonMerchandiseFlag() {
		return mNonMerchandiseFlag;
	}

	/**
	 * Sets the non merchandise flag.
	 * 
	 * @param pNonMerchandiseFlag
	 *            the nonMerchandiseFlag to set
	 */
	public void setNonMerchandiseFlag(String pNonMerchandiseFlag) {
		mNonMerchandiseFlag = pNonMerchandiseFlag;
	}

	/**
	 * Gets the country eligibility.
	 * 
	 * @return the countryEligibility
	 */
	public String getCountryEligibility() {
		return mCountryEligibility;
	}

	/**
	 * Sets the country eligibility.
	 * 
	 * @param pCountryEligibility
	 *            the countryEligibility to set
	 */
	public void setCountryEligibility(String pCountryEligibility) {
		mCountryEligibility = pCountryEligibility;
	}

	/**
	 * Gets the registry classification.
	 * 
	 * @return the registryClassification
	 */
	public String getRegistryClassification() {
		return mRegistryClassification;
	}

	/**
	 * Sets the registry classification.
	 * 
	 * @param pRegistryClassification
	 *            the registryClassification to set
	 */
	public void setRegistryClassification(String pRegistryClassification) {
		mRegistryClassification = pRegistryClassification;
	}

	/**
	 * Gets the vendor classification.
	 * 
	 * @return the vendorClassification
	 */
	public String getVendorClassification() {
		return mVendorClassification;
	}

	/**
	 * Sets the vendor classification.
	 * 
	 * @param pVendorClassification
	 *            the vendorClassification to set
	 */
	public void setVendorClassification(String pVendorClassification) {
		mVendorClassification = pVendorClassification;
	}

	/**
	 * Gets the price list name.
	 * 
	 * @return the priceListName
	 */
	public String getPriceListName() {
		return mPriceListName;
	}

	/**
	 * Sets the price list name.
	 * 
	 * @param pPriceListName
	 *            the priceListName to set
	 */
	public void setPriceListName(String pPriceListName) {
		mPriceListName = pPriceListName;
	}

	/**
	 * Gets the sKU name.
	 * 
	 * @return the sKUName
	 */
	public String getSKUName() {
		return mSKUName;
	}

	/**
	 * Sets the sKU name.
	 * 
	 * @param pSKUName
	 *            the sKUName to set
	 */
	public void setSKUName(String pSKUName) {
		mSKUName = pSKUName;
	}

	/**
	 * Gets the child sk us name.
	 * 
	 * @return the childSKUsName
	 */
	public String getChildSKUsName() {
		return mChildSKUsName;
	}

	/**
	 * Sets the child sk us name.
	 * 
	 * @param pChildSKUsName
	 *            the childSKUsName to set
	 */
	public void setChildSKUsName(String pChildSKUsName) {
		mChildSKUsName = pChildSKUsName;
	}

	/**
	 * The variable to hold "longDescription" property name.
	 */
	private String mLongDescriptionName;

	/**
	 * Gets the long description name.
	 * 
	 * @return the longDescriptionName
	 */
	public String getLongDescriptionName() {
		return mLongDescriptionName;
	}

	/**
	 * Sets the long description name.
	 * 
	 * @param pLongDescriptionName
	 *            the longDescriptionName to set
	 */
	public void setLongDescriptionName(String pLongDescriptionName) {
		mLongDescriptionName = pLongDescriptionName;
	}

	/**
	 * The variable to hold "Description" property name.
	 */
	private String mDescriptionName;

	/**
	 * Gets the description name.
	 * 
	 * @return the descriptionName
	 */
	public String getDescriptionName() {
		return mDescriptionName;
	}

	/**
	 * Sets the description name.
	 * 
	 * @param pDescriptionName
	 *            the descriptionName to set
	 */
	public void setDescriptionName(String pDescriptionName) {
		mDescriptionName = pDescriptionName;
	}

	/**
	 * The variable to hold "fixedChildProducts" property name.
	 */
	private String mFixedChildProductsName;

	/**
	 * Gets the fixed child products name.
	 * 
	 * @return the fixedChildProductsName
	 */
	public String getFixedChildProductsName() {
		return mFixedChildProductsName;
	}

	/**
	 * Sets the fixed child products name.
	 * 
	 * @param pFixedChildProductsName
	 *            the fixedChildProductsName to set
	 */
	public void setFixedChildProductsName(String pFixedChildProductsName) {
		mFixedChildProductsName = pFixedChildProductsName;
	}

	/**
	 * Gets the fixed child categories name.
	 * 
	 * @return the fixedChildCategoriesName
	 */
	public String getFixedChildCategoriesName() {
		return mFixedChildCategoriesName;
	}

	/**
	 * Sets the fixed child categories name.
	 * 
	 * @param pFixedChildCategoriesName
	 *            the fixedChildCategoriesName to set
	 */
	public void setFixedChildCategoriesName(String pFixedChildCategoriesName) {
		mFixedChildCategoriesName = pFixedChildCategoriesName;
	}

	/**
	 * Gets the property name.
	 * 
	 * @return mPropertyName
	 */
	public String getPropertyName() {
		return mPropertyName;
	}

	/**
	 * Sets the property name.
	 * 
	 * @param pPropertyName
	 *            PropertyName
	 */
	public void setPropertyName(String pPropertyName) {
		this.mPropertyName = pPropertyName;
	}

	/**
	 * Gets the store id property name.
	 * 
	 * @return mStoreIdPropertyName
	 */
	public String getStoreIdPropertyName() {
		return mStoreIdPropertyName;
	}

	/**
	 * Sets the store id property name.
	 * 
	 * @param pStoreIdPropertyName
	 *            storeId
	 */
	public void setStoreIdPropertyName(String pStoreIdPropertyName) {
		this.mStoreIdPropertyName = pStoreIdPropertyName;
	}

	/**
	 * Gets the display name default.
	 * 
	 * @return the mDisplayNameDefault
	 */
	public String getDisplayNameDefault() {
		return mDisplayNameDefault;
	}

	/**
	 * Sets the display name default.
	 * 
	 * @param pDisplayNameDefault
	 *            the mDisplayNameDefault to set
	 */
	public void setDisplayNameDefault(String pDisplayNameDefault) {
		this.mDisplayNameDefault = pDisplayNameDefault;
	}

	/**
	 * Gets the title image.
	 * 
	 * @return the mTitleImage
	 */
	public String getTitleImage() {
		return mTitleImage;
	}

	/**
	 * Sets the title image.
	 * 
	 * @param pTitleImage
	 *            the mTitleImage to set
	 */
	public void setTitleImage(String pTitleImage) {
		this.mTitleImage = pTitleImage;
	}

	/**
	 * Gets estiloClassificationNamePropertyName.
	 * 
	 * @return the estiloClassificationNamePropertyName
	 */
	public String getEstiloClassificationNamePropertyName() {
		return mEstiloClassificationNamePropertyName;
	}

	/**
	 * Sets String.
	 * 
	 * @param pEstiloClassificationNamePropertyName
	 *            the estiloClassificationNamePropertyName to set
	 */
	public void setEstiloClassificationNamePropertyName(String pEstiloClassificationNamePropertyName) {
		mEstiloClassificationNamePropertyName = pEstiloClassificationNamePropertyName;
	}

	/**
	 * Gets classificationNamePropertyName.
	 * 
	 * @return the classificationNamePropertyName
	 */
	public String getClassificationNamePropertyName() {
		return mClassificationNamePropertyName;
	}

	/**
	 * Sets String.
	 * 
	 * @param pClassificationNamePropertyName
	 *            the classificationNamePropertyName to set
	 */
	public void setClassificationNamePropertyName(String pClassificationNamePropertyName) {
		mClassificationNamePropertyName = pClassificationNamePropertyName;
	}

	/**
	 * Gets the mProfileItemPropertyName.
	 * 
	 * @return mProfileItemPropertyName
	 */
	public String getProfileItemPropertyName() {
		return mProfileItemPropertyName;
	}

	/**
	 * Sets the mProfileItemPropertyName.
	 * 
	 * @param pProfileItemPropertyName
	 *            of type String
	 */
	public void setProfileItemPropertyName(String pProfileItemPropertyName) {
		this.mProfileItemPropertyName = pProfileItemPropertyName;
	}

	/**
	 * Gets the mLastModifiedDatePropertyName.
	 * 
	 * @return mLastModifiedDatePropertyName
	 */
	public String getLastModifiedDatePropertyName() {
		return mLastModifiedDatePropertyName;
	}

	/**
	 * Sets the mLastModifiedDatePropertyName.
	 * 
	 * @param pLastModifiedDatePropertyName
	 *            of type String.
	 */
	public void setLastModifiedDatePropertyName(String pLastModifiedDatePropertyName) {
		this.mLastModifiedDatePropertyName = pLastModifiedDatePropertyName;
	}

	/**
	 * Gets the mCollectionPropertyName.
	 * 
	 * @return mCollectionPropertyName
	 */
	public String getCollectionPropertyName() {
		return mCollectionPropertyName;
	}

	/**
	 * Sets the mCollectionPropertyName.
	 * 
	 * @param pCollectionPropertyName
	 *            to set pCollectionPropertyName
	 */
	public void setCollectionPropertyName(String pCollectionPropertyName) {
		this.mCollectionPropertyName = pCollectionPropertyName;
	}

	/**
	 * Gets the mProductDisplayName.
	 * 
	 * @return the mProductDisplayName
	 */

	public String getProductDisplayName() {
		return mProductDisplayName;
	}

	/**
	 * Sets the mProductDisplayName.
	 * 
	 * @param pProductDisplayName
	 *            the mProductDisplayName to set
	 */

	public void setProductDisplayName(String pProductDisplayName) {
		mProductDisplayName = pProductDisplayName;
	}

	/**
	 * Gets the productTemplateProperty.
	 * 
	 * @return the productTemplateProperty
	 */

	public String getProductTemplateProperty() {
		return mProductTemplateProperty;
	}

	/**
	 * Sets the productTemplateProperty.
	 * 
	 * @param pProductTemplateProperty
	 *            the productTemplateProperty to set
	 */

	public void setProductTemplateProperty(String pProductTemplateProperty) {
		mProductTemplateProperty = pProductTemplateProperty;
	}

	/**
	 * Gets the productTemplateURLProperty.
	 * 
	 * @return the productTemplateURLProperty
	 */

	public String getProductTemplateURLProperty() {
		return mProductTemplateURLProperty;
	}

	/**
	 * Sets the productTemplateURLProperty.
	 * 
	 * @param pProductTemplateURLProperty
	 *            the productTemplateURLProperty to set
	 */

	public void setProductTemplateURLProperty(String pProductTemplateURLProperty) {
		mProductTemplateURLProperty = pProductTemplateURLProperty;
	}

	/**
	 * Gets the checks if is look category property name.
	 * 
	 * @return the "completeTheLook" property name.
	 */
	public String getIsLookCategoryPropertyName() {
		return mIsLookCategoryPropertyName;
	}

	/**
	 * Sets the checks if is look category property name.
	 * 
	 * @param pIsLookCategoryPropertyName
	 *            the "isLookCategory" property name.
	 */
	public void setIsLookCategoryPropertyName(final String pIsLookCategoryPropertyName) {
		this.mIsLookCategoryPropertyName = pIsLookCategoryPropertyName;
	}

	/**
	 * Gets the complete the look property name.
	 * 
	 * @return the "completeTheLook" property name.
	 */
	public String getCompleteTheLookPropertyName() {
		return mCompleteTheLookPropertyName;
	}

	/**
	 * Sets the complete the look property name.
	 * 
	 * @param pCompleteTheLookPropertyName
	 *            - the "completeTheLook" property name.
	 */
	public void setCompleteTheLookPropertyName(final String pCompleteTheLookPropertyName) {
		this.mCompleteTheLookPropertyName = pCompleteTheLookPropertyName;
	}

	/**
	 * Gets the creation date property name.
	 * 
	 * @return the creationDatePropertyName
	 */
	public String getCreationDatePropertyName() {
		return mCreationDatePropertyName;
	}

	/**
	 * Sets the creation date property name.
	 * 
	 * @param pCreationDatePropertyName
	 *            the creationDatePropertyName to set
	 */
	public void setCreationDatePropertyName(String pCreationDatePropertyName) {
		mCreationDatePropertyName = pCreationDatePropertyName;
	}

	/**
	 * Gets brandItemDescriptorName.
	 * 
	 * @return the brandItemDescriptorName
	 */
	public String getBrandItemDescriptorName() {
		return mBrandItemDescriptorName;
	}

	/**
	 * Sets brandItemDescriptorName .
	 * 
	 * @param pBrandItemDescriptorName
	 *            the brandItemDescriptorName to set
	 */
	public void setBrandItemDescriptorName(String pBrandItemDescriptorName) {
		mBrandItemDescriptorName = pBrandItemDescriptorName;
	}

	/**
	 * Gets designerItemDescriptorName.
	 * 
	 * @return the designerItemDescriptorName
	 */
	public String getDesignerItemDescriptorName() {
		return mDesignerItemDescriptorName;
	}

	/**
	 * Sets designerItemDescriptorName .
	 * 
	 * @param pDesignerItemDescriptorName
	 *            the designerItemDescriptorName to set
	 */
	public void setDesignerItemDescriptorName(String pDesignerItemDescriptorName) {
		mDesignerItemDescriptorName = pDesignerItemDescriptorName;
	}

	/**
	 * Gets itemIdPropertyName.
	 * 
	 * @return the itemIdPropertyName
	 */
	public String getItemIdPropertyName() {
		return mItemIdPropertyName;
	}

	/**
	 * Sets itemIdPropertyName .
	 * 
	 * @param pItemIdPropertyName
	 *            the itemIdPropertyName to set
	 */
	public void setItemIdPropertyName(String pItemIdPropertyName) {
		mItemIdPropertyName = pItemIdPropertyName;
	}

	/**
	 * Gets mimicSkuIdPropertyName.
	 * 
	 * @return the mimicSkuIdPropertyName
	 */
	public String getMimicSkuIdPropertyName() {
		return mMimicSkuIdPropertyName;
	}

	/**
	 * Sets mimicSkuIdPropertyName .
	 * 
	 * @param pMimicSkuIdPropertyName
	 *            the mimicSkuIdPropertyName to set
	 */
	public void setMimicSkuIdPropertyName(String pMimicSkuIdPropertyName) {
		mMimicSkuIdPropertyName = pMimicSkuIdPropertyName;
	}

	/**
	 * Gets enabledPropertyName.
	 * 
	 * @return the enabledPropertyName
	 */
	public String getEnabledPropertyName() {
		return mEnabledPropertyName;
	}

	/**
	 * Sets enabledPropertyName .
	 * 
	 * @param pEnabledPropertyName
	 *            the enabledPropertyName to set
	 */
	public void setEnabledPropertyName(String pEnabledPropertyName) {
		mEnabledPropertyName = pEnabledPropertyName;
	}

	/**
	 * Gets changedPropertyName.
	 * 
	 * @return the changedPropertyName
	 */
	public String getChangedPropertyName() {
		return mChangedPropertyName;
	}

	/**
	 * Sets changedPropertyName .
	 * 
	 * @param pChangedPropertyName
	 *            the changedPropertyName to set
	 */
	public void setChangedPropertyName(String pChangedPropertyName) {
		mChangedPropertyName = pChangedPropertyName;
	}

	/**
	 * Gets the checks if is new sku property name.
	 * 
	 * @return the isNewSkuPropertyName
	 */
	public String getIsNewSkuPropertyName() {
		return mIsNewSkuPropertyName;
	}

	/**
	 * Sets the checks if is new sku property name.
	 * 
	 * @param pIsNewSkuPropertyName
	 *            the isNewSkuPropertyName to set
	 */
	public void setIsNewSkuPropertyName(String pIsNewSkuPropertyName) {
		mIsNewSkuPropertyName = pIsNewSkuPropertyName;
	}

	/**
	 * Gets the checks if is onsale sku property name.
	 * 
	 * @return the isOnsaleSkuPropertyName
	 */
	public String getIsOnsaleSkuPropertyName() {
		return mIsOnsaleSkuPropertyName;
	}

	/**
	 * Sets the checks if is onsale sku property name.
	 * 
	 * @param pIsOnsaleSkuPropertyName
	 *            the isOnsaleSkuPropertyName to set
	 */
	public void setIsOnsaleSkuPropertyName(String pIsOnsaleSkuPropertyName) {
		mIsOnsaleSkuPropertyName = pIsOnsaleSkuPropertyName;
	}

	/**
	 * Gets the promotional content item descriptor name.
	 * 
	 * @return the promotionalContentItemDescriptorName
	 */
	public String getPromotionalContentItemDescriptorName() {
		return mPromotionalContentItemDescriptorName;
	}

	/**
	 * Sets the promotional content item descriptor name.
	 * 
	 * @param pPromotionalContentItemDescriptorName
	 *            the promotionalContentItemDescriptorName to set
	 */
	public void setPromotionalContentItemDescriptorName(String pPromotionalContentItemDescriptorName) {
		mPromotionalContentItemDescriptorName = pPromotionalContentItemDescriptorName;
	}

	/**
	 * Gets associatedCategoryPropertyName.
	 * 
	 * @return the associatedCategoryPropertyName
	 */
	public String getAssociatedCategoryPropertyName() {
		return mAssociatedCategoryPropertyName;
	}

	/**
	 * Sets associatedCategoryPropertyName .
	 * 
	 * @param pAssociatedCategoryPropertyName
	 *            the associatedCategoryPropertyName to set
	 */

	public void setAssociatedCategoryPropertyName(String pAssociatedCategoryPropertyName) {
		mAssociatedCategoryPropertyName = pAssociatedCategoryPropertyName;
	}

	/**
	 * Gets the list price property name.
	 * 
	 * @return mListPricePropertyName
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}

	/**
	 * Sets listPricePropertyName.
	 * 
	 * @param pListPricePropertyName
	 *            the listPricePropertyName to set
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		this.mListPricePropertyName = pListPricePropertyName;
	}

	/**
	 * Gets the sale price property name.
	 * 
	 * @return mSalePricePropertyName
	 */

	public String getSalePricePropertyName() {
		return mSalePricePropertyName;
	}

	/**
	 * Sets salePricePropertyName.
	 * 
	 * @param pSalePricePropertyName
	 *            the salePricePropertyName to set
	 */
	public void setSalePricePropertyName(String pSalePricePropertyName) {
		this.mSalePricePropertyName = pSalePricePropertyName;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the mId
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            - pId
	 */
	public void setId(String pId) {
		this.mId = pId;
	}

	/** The m product name. */
	private String mProductName;

	/**
	 * Gets the product name.
	 * 
	 * @return the productName
	 */
	public String getProductName() {
		return mProductName;
	}

	/**
	 * Sets the product name.
	 * 
	 * @param pProductName
	 *            the productName to set
	 */
	public void setProductName(String pProductName) {
		this.mProductName = pProductName;
	}

	/** The m child products name. */
	private String mChildProductsName;

	/**
	 * Gets the child products name.
	 * 
	 * @return the childProductsName
	 */
	public String getChildProductsName() {
		return mChildProductsName;
	}

	/**
	 * Sets the child products name.
	 * 
	 * @param pChildProductsName
	 *            the childProductsName to set
	 */
	public void setChildProductsName(String pChildProductsName) {
		this.mChildProductsName = pChildProductsName;
	}

	/**
	 * Gets the category name.
	 * 
	 * @return the pCategoryName
	 */
	public String getCategoryName() {
		return mCategoryName;
	}

	/**
	 * Sets the category name.
	 * 
	 * @param pCategoryName
	 *            - pCategoryName
	 */
	public void setCategoryName(String pCategoryName) {
		this.mCategoryName = pCategoryName;
	}

	/**
	 * Gets the akhi map.
	 * 
	 * @return the akhiMap
	 */
	public String getAkhiMap() {
		return mAkhiMap;
	}

	/**
	 * Sets the akhi map.
	 * 
	 * @param pAkhiMap
	 *            the akhiMap to set
	 */
	public void setAkhiMap(String pAkhiMap) {
		mAkhiMap = pAkhiMap;
	}

	/**
	 * Gets the assembly map.
	 * 
	 * @return the assemblyMap
	 */
	public String getAssemblyMap() {
		return mAssemblyMap;
	}

	/**
	 * Sets the assembly map.
	 * 
	 * @param pAssemblyMap
	 *            the assemblyMap to set
	 */
	public void setAssemblyMap(String pAssemblyMap) {
		mAssemblyMap = pAssemblyMap;
	}

	/**
	 * Gets the back order status.
	 * 
	 * @return the backOrderStatus
	 */
	public String getBackOrderStatus() {
		return mBackOrderStatus;
	}

	/**
	 * Sets the back order status.
	 * 
	 * @param pBackOrderStatus
	 *            the backOrderStatus to set
	 */
	public void setBackOrderStatus(String pBackOrderStatus) {
		mBackOrderStatus = pBackOrderStatus;
	}

	/**
	 * Gets the brand name secondary.
	 * 
	 * @return the brandNameSecondary
	 */
	public String getBrandNameSecondary() {
		return mBrandNameSecondary;
	}

	/**
	 * Sets the brand name secondary.
	 * 
	 * @param pBrandNameSecondary
	 *            the brandNameSecondary to set
	 */
	public void setBrandNameSecondary(String pBrandNameSecondary) {
		mBrandNameSecondary = pBrandNameSecondary;
	}

	/**
	 * Gets the brand name tertiary char theme.
	 * 
	 * @return the brandNameTertiaryCharTheme
	 */
	public String getBrandNameTertiaryCharTheme() {
		return mBrandNameTertiaryCharTheme;
	}

	/**
	 * Sets the brand name tertiary char theme.
	 * 
	 * @param pBrandNameTertiaryCharTheme
	 *            the brandNameTertiaryCharTheme to set
	 */
	public void setBrandNameTertiaryCharTheme(String pBrandNameTertiaryCharTheme) {
		mBrandNameTertiaryCharTheme = pBrandNameTertiaryCharTheme;
	}

	/**
	 * Gets the browse hidden keyword.
	 * 
	 * @return the browseHiddenKeyword
	 */
	public String getBrowseHiddenKeyword() {
		return mBrowseHiddenKeyword;
	}

	/**
	 * Sets the browse hidden keyword.
	 * 
	 * @param pBrowseHiddenKeyword
	 *            the browseHiddenKeyword to set
	 */
	public void setBrowseHiddenKeyword(String pBrowseHiddenKeyword) {
		mBrowseHiddenKeyword = pBrowseHiddenKeyword;
	}

	/**
	 * Gets the can be gift wrapped.
	 * 
	 * @return the canBeGiftWrapped
	 */
	public String getCanBeGiftWrapped() {
		return mCanBeGiftWrapped;
	}

	/**
	 * Sets the can be gift wrapped.
	 * 
	 * @param pCanBeGiftWrapped
	 *            the canBeGiftWrapped to set
	 */
	public void setCanBeGiftWrapped(String pCanBeGiftWrapped) {
		mCanBeGiftWrapped = pCanBeGiftWrapped;
	}

	/**
	 * Gets the car seat features.
	 * 
	 * @return the carSeatFeatures
	 */
	public String getCarSeatFeatures() {
		return mCarSeatFeatures;
	}

	/**
	 * Sets the car seat features.
	 * 
	 * @param pCarSeatFeatures
	 *            the carSeatFeatures to set
	 */
	public void setCarSeatFeatures(String pCarSeatFeatures) {
		mCarSeatFeatures = pCarSeatFeatures;
	}

	/**
	 * Gets the car seat types.
	 * 
	 * @return the carSeatTypes
	 */
	public String getCarSeatTypes() {
		return mCarSeatTypes;
	}

	/**
	 * Sets the car seat types.
	 * 
	 * @param pCarSeatTypes
	 *            the carSeatTypes to set
	 */
	public void setCarSeatTypes(String pCarSeatTypes) {
		mCarSeatTypes = pCarSeatTypes;
	}

	/**
	 * Gets the car seat what is imp.
	 * 
	 * @return the carSeatWhatIsImp
	 */
	public String getCarSeatWhatIsImp() {
		return mCarSeatWhatIsImp;
	}

	/**
	 * Sets the car seat what is imp.
	 * 
	 * @param pCarSeatWhatIsImp
	 *            the carSeatWhatIsImp to set
	 */
	public void setCarSeatWhatIsImp(String pCarSeatWhatIsImp) {
		mCarSeatWhatIsImp = pCarSeatWhatIsImp;
	}

	/**
	 * Gets the child weight max.
	 * 
	 * @return the childWeightMax
	 */
	public String getChildWeightMax() {
		return mChildWeightMax;
	}

	/**
	 * Sets the child weight max.
	 * 
	 * @param pChildWeightMax
	 *            the childWeightMax to set
	 */
	public void setChildWeightMax(String pChildWeightMax) {
		mChildWeightMax = pChildWeightMax;
	}

	/**
	 * Gets the child weight min.
	 * 
	 * @return the childWeightMin
	 */
	public String getChildWeightMin() {
		return mChildWeightMin;
	}

	/**
	 * Sets the child weight min.
	 * 
	 * @param pChildWeightMin
	 *            the childWeightMin to set
	 */
	public void setChildWeightMin(String pChildWeightMin) {
		mChildWeightMin = pChildWeightMin;
	}

	/**
	 * Gets the color code.
	 * 
	 * @return the colorCode
	 */
	public String getColorCode() {
		return mColorCode;
	}

	/**
	 * Sets the color code.
	 * 
	 * @param pColorCode
	 *            the colorCode to set
	 */
	public void setColorCode(String pColorCode) {
		mColorCode = pColorCode;
	}

	/**
	 * Gets the color upc level.
	 * 
	 * @return the colorUpcLevel
	 */
	public String getColorUpcLevel() {
		return mColorUpcLevel;
	}

	/**
	 * Sets the color upc level.
	 * 
	 * @param pColorUpcLevel
	 *            the colorUpcLevel to set
	 */
	public void setColorUpcLevel(String pColorUpcLevel) {
		mColorUpcLevel = pColorUpcLevel;
	}

	/**
	 * Gets the crib material types.
	 * 
	 * @return the cribMaterialTypes
	 */
	public String getCribMaterialTypes() {
		return mCribMaterialTypes;
	}

	/**
	 * Sets the crib material types.
	 * 
	 * @param pCribMaterialTypes
	 *            the cribMaterialTypes to set
	 */
	public void setCribMaterialTypes(String pCribMaterialTypes) {
		mCribMaterialTypes = pCribMaterialTypes;
	}

	/**
	 * Gets the crib styles.
	 * 
	 * @return the cribStyles
	 */
	public String getCribStyles() {
		return mCribStyles;
	}

	/**
	 * Sets the crib styles.
	 * 
	 * @param pCribStyles
	 *            the cribStyles to set
	 */
	public void setCribStyles(String pCribStyles) {
		mCribStyles = pCribStyles;
	}

	/**
	 * Gets the crib types.
	 * 
	 * @return the cribTypes
	 */
	public String getCribTypes() {
		return mCribTypes;
	}

	/**
	 * Sets the crib types.
	 * 
	 * @param pCribTypes
	 *            the cribTypes to set
	 */
	public void setCribTypes(String pCribTypes) {
		mCribTypes = pCribTypes;
	}

	/**
	 * Gets the crib what is imp.
	 * 
	 * @return the cribWhatIsImp
	 */
	public String getCribWhatIsImp() {
		return mCribWhatIsImp;
	}

	/**
	 * Sets the crib what is imp.
	 * 
	 * @param pCribWhatIsImp
	 *            the cribWhatIsImp to set
	 */
	public void setCribWhatIsImp(String pCribWhatIsImp) {
		mCribWhatIsImp = pCribWhatIsImp;
	}

	/**
	 * Gets the customer purchase limit.
	 * 
	 * @return the customerPurchaseLimit
	 */
	public String getCustomerPurchaseLimit() {
		return mCustomerPurchaseLimit;
	}

	/**
	 * Sets the customer purchase limit.
	 * 
	 * @param pCustomerPurchaseLimit
	 *            the customerPurchaseLimit to set
	 */
	public void setCustomerPurchaseLimit(String pCustomerPurchaseLimit) {
		mCustomerPurchaseLimit = pCustomerPurchaseLimit;
	}

	/**
	 * Gets the book cd dvd properties map.
	 * 
	 * @return the bookCdDvdPropertiesMap
	 */
	public String getBookCdDvdPropertiesMap() {
		return mBookCdDvdPropertiesMap;
	}

	/**
	 * Gets the interest.
	 * 
	 * @return the interest
	 */
	public String getInterest() {
		return mInterest;
	}

	/**
	 * Sets the interest.
	 * 
	 * @param pInterest
	 *            the interest to set
	 */
	public void setInterest(String pInterest) {
		mInterest = pInterest;
	}

	/**
	 * Sets the book cd dvd properties map.
	 * 
	 * @param pBookCdDvdPropertiesMap
	 *            the bookCdDvdPropertiesMap to set
	 */
	public void setBookCdDvdPropertiesMap(String pBookCdDvdPropertiesMap) {
		mBookCdDvdPropertiesMap = pBookCdDvdPropertiesMap;
	}

	/**
	 * Gets the dropShip flag.
	 * 
	 * @return the dropshipFlag
	 */
	public String getDropShipFlag() {
		return mDropShipFlag;
	}

	/**
	 * Sets the dropShip flag.
	 * 
	 * @param pDropShipFlag
	 *            the dropShipFlag to set
	 */
	public void setDropShipFlag(String pDropShipFlag) {
		mDropShipFlag = pDropShipFlag;
	}

	/**
	 * Gets the flexible shipping plan.
	 * 
	 * @return the flexibleShippingPlan
	 */
	public String getFlexibleShippingPlan() {
		return mFlexibleShippingPlan;
	}

	/**
	 * Sets the flexible shipping plan.
	 * 
	 * @param pFlexibleShippingPlan
	 *            the flexibleShippingPlan to set
	 */
	public void setFlexibleShippingPlan(String pFlexibleShippingPlan) {
		mFlexibleShippingPlan = pFlexibleShippingPlan;
	}

	/**
	 * Gets the freight class.
	 * 
	 * @return the freightClass
	 */
	public String getFreightClass() {
		return mFreightClass;
	}

	/**
	 * Sets the freight class.
	 * 
	 * @param pFreightClass
	 *            the freightClass to set
	 */
	public void setFreightClass(String pFreightClass) {
		mFreightClass = pFreightClass;
	}

	/**
	 * Gets the furniture finish.
	 * 
	 * @return the furnitureFinish
	 */
	public String getFurnitureFinish() {
		return mFurnitureFinish;
	}

	/**
	 * Sets the furniture finish.
	 * 
	 * @param pFurnitureFinish
	 *            the furnitureFinish to set
	 */
	public void setFurnitureFinish(String pFurnitureFinish) {
		mFurnitureFinish = pFurnitureFinish;
	}

	/**
	 * Gets the incremental safety stock units.
	 * 
	 * @return the incrementalSafetyStockUnits
	 */
	public String getIncrementalSafetyStockUnits() {
		return mIncrementalSafetyStockUnits;
	}

	/**
	 * Sets the incremental safety stock units.
	 * 
	 * @param pIncrementalSafetyStockUnits
	 *            the incrementalSafetyStockUnits to set
	 */
	public void setIncrementalSafetyStockUnits(String pIncrementalSafetyStockUnits) {
		mIncrementalSafetyStockUnits = pIncrementalSafetyStockUnits;
	}

	/**
	 * Gets the item dimension map.
	 * 
	 * @return the itemDimensionMap
	 */
	public String getItemDimensionMap() {
		return mItemDimensionMap;
	}

	/**
	 * Sets the item dimension map.
	 * 
	 * @param pItemDimensionMap
	 *            the itemDimensionMap to set
	 */
	public void setItemDimensionMap(String pItemDimensionMap) {
		mItemDimensionMap = pItemDimensionMap;
	}

	/**
	 * Gets the juvenile product size.
	 * 
	 * @return the juvenileProductSize
	 */
	public String getJuvenileProductSize() {
		return mJuvenileProductSize;
	}

	/**
	 * Sets the juvenile product size.
	 * 
	 * @param pJuvenileProductSize
	 *            the juvenileProductSize to set
	 */
	public void setJuvenileProductSize(String pJuvenileProductSize) {
		mJuvenileProductSize = pJuvenileProductSize;
	}

	/**
	 * Gets the lower48 map.
	 * 
	 * @return the lower48Map
	 */
	public String getLower48Map() {
		return mLower48Map;
	}

	/**
	 * Sets the lower48 map.
	 * 
	 * @param pLower48Map
	 *            the lower48Map to set
	 */
	public void setLower48Map(String pLower48Map) {
		mLower48Map = pLower48Map;
	}

	/**
	 * Gets the max target age tru websites.
	 * 
	 * @return the maxTargetAgeTruWebsites
	 */
	public String getMaxTargetAgeTruWebsites() {
		return mMaxTargetAgeTruWebsites;
	}

	/**
	 * Sets the max target age tru websites.
	 * 
	 * @param pMaxTargetAgeTruWebsites
	 *            the maxTargetAgeTruWebsites to set
	 */
	public void setMaxTargetAgeTruWebsites(String pMaxTargetAgeTruWebsites) {
		mMaxTargetAgeTruWebsites = pMaxTargetAgeTruWebsites;
	}

	/**
	 * Gets the mfr suggested age max.
	 * 
	 * @return the mfrSuggestedAgeMax
	 */
	public String getMfrSuggestedAgeMax() {
		return mMfrSuggestedAgeMax;
	}

	/**
	 * Sets the mfr suggested age max.
	 * 
	 * @param pMfrSuggestedAgeMax
	 *            the mfrSuggestedAgeMax to set
	 */
	public void setMfrSuggestedAgeMax(String pMfrSuggestedAgeMax) {
		mMfrSuggestedAgeMax = pMfrSuggestedAgeMax;
	}

	/**
	 * Gets the mfr suggested age min.
	 * 
	 * @return the mfrSuggestedAgeMin
	 */
	public String getMfrSuggestedAgeMin() {
		return mMfrSuggestedAgeMin;
	}

	/**
	 * Sets the mfr suggested age min.
	 * 
	 * @param pMfrSuggestedAgeMin
	 *            the mfrSuggestedAgeMin to set
	 */
	public void setMfrSuggestedAgeMin(String pMfrSuggestedAgeMin) {
		mMfrSuggestedAgeMin = pMfrSuggestedAgeMin;
	}

	/**
	 * Gets the min target age tru websites.
	 * 
	 * @return the minTargetAgeTruWebsites
	 */
	public String getMinTargetAgeTruWebsites() {
		return mMinTargetAgeTruWebsites;
	}

	/**
	 * Sets the min target age tru websites.
	 * 
	 * @param pMinTargetAgeTruWebsites
	 *            the minTargetAgeTruWebsites to set
	 */
	public void setMinTargetAgeTruWebsites(String pMinTargetAgeTruWebsites) {
		mMinTargetAgeTruWebsites = pMinTargetAgeTruWebsites;
	}

	/**
	 * Gets the nmwa percentage.
	 * 
	 * @return the nmwaPercentage
	 */
	public String getNmwaPercentage() {
		return mNmwaPercentage;
	}

	/**
	 * Sets the nmwa percentage.
	 * 
	 * @param pNmwaPercentage
	 *            the nmwaPercentage to set
	 */
	public void setNmwaPercentage(String pNmwaPercentage) {
		mNmwaPercentage = pNmwaPercentage;
	}

	/**
	 * Gets the ship window max.
	 * 
	 * @return the shipWindowMax
	 */
	public String getShipWindowMax() {
		return mShipWindowMax;
	}

	/**
	 * Sets the ship window max.
	 * 
	 * @param pShipWindowMax
	 *            the shipWindowMax to set
	 */
	public void setShipWindowMax(String pShipWindowMax) {
		mShipWindowMax = pShipWindowMax;
	}

	/**
	 * Gets the ship window min.
	 * 
	 * @return the shipWindowMin
	 */
	public String getShipWindowMin() {
		return mShipWindowMin;
	}

	/**
	 * Sets the ship window min.
	 * 
	 * @param pShipWindowMin
	 *            the shipWindowMin to set
	 */
	public void setShipWindowMin(String pShipWindowMin) {
		mShipWindowMin = pShipWindowMin;
	}

	/**
	 * Gets the online pid.
	 * 
	 * @return the onlinePID
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * Sets the online pid.
	 * 
	 * @param pOnlinePID
	 *            the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}

	/**
	 * Gets the original pid.
	 * 
	 * @return the originalPID
	 */
	public String getOriginalPID() {
		return mOriginalPID;
	}

	/**
	 * Sets the original pid.
	 * 
	 * @param pOriginalPID
	 *            the originalPID to set
	 */
	public void setOriginalPID(String pOriginalPID) {
		mOriginalPID = pOriginalPID;
	}

	/**
	 * Gets the street date.
	 * 
	 * @return the streetDate
	 */
	public String getStreetDate() {
		return mStreetDate;
	}

	/**
	 * Sets the street date.
	 * 
	 * @param pStreetDate
	 *            the streetDate to set
	 */
	public void setStreetDate(String pStreetDate) {
		mStreetDate = pStreetDate;
	}

	/**
	 * Gets the other fixed surcharge.
	 * 
	 * @return the otherFixedSurcharge
	 */
	public String getOtherFixedSurcharge() {
		return mOtherFixedSurcharge;
	}

	/**
	 * Sets the other fixed surcharge.
	 * 
	 * @param pOtherFixedSurcharge
	 *            the otherFixedSurcharge to set
	 */
	public void setOtherFixedSurcharge(String pOtherFixedSurcharge) {
		mOtherFixedSurcharge = pOtherFixedSurcharge;
	}

	/**
	 * Gets the other std fs dollar.
	 * 
	 * @return the otherStdFSDollar
	 */
	public String getOtherStdFSDollar() {
		return mOtherStdFSDollar;
	}

	/**
	 * Sets the other std fs dollar.
	 * 
	 * @param pOtherStdFSDollar
	 *            the otherStdFSDollar to set
	 */
	public void setOtherStdFSDollar(String pOtherStdFSDollar) {
		mOtherStdFSDollar = pOtherStdFSDollar;
	}

	/**
	 * Gets the outlet.
	 * 
	 * @return the outlet
	 */
	public String getOutlet() {
		return mOutlet;
	}

	/**
	 * Sets the outlet.
	 * 
	 * @param pOutlet
	 *            the outlet to set
	 */
	public void setOutlet(String pOutlet) {
		mOutlet = pOutlet;
	}

	/**
	 * Gets the presell quantity units.
	 * 
	 * @return the presellQuantityUnits
	 */
	public String getPresellQuantityUnits() {
		return mPresellQuantityUnits;
	}

	/**
	 * Sets the presell quantity units.
	 * 
	 * @param pPresellQuantityUnits
	 *            the presellQuantityUnits to set
	 */
	public void setPresellQuantityUnits(String pPresellQuantityUnits) {
		mPresellQuantityUnits = pPresellQuantityUnits;
	}

	/**
	 * Gets the price display.
	 * 
	 * @return the priceDisplay
	 */
	public String getPriceDisplay() {
		return mPriceDisplay;
	}

	/**
	 * Sets the price display.
	 * 
	 * @param pPriceDisplay
	 *            the priceDisplay to set
	 */
	public void setPriceDisplay(String pPriceDisplay) {
		mPriceDisplay = pPriceDisplay;
	}

	/**
	 * Gets the product awards.
	 * 
	 * @return the productAwards
	 */
	public String getProductAwards() {
		return mProductAwards;
	}

	/**
	 * Sets the product awards.
	 * 
	 * @param pProductAwards
	 *            the productAwards to set
	 */
	public void setProductAwards(String pProductAwards) {
		mProductAwards = pProductAwards;
	}

	/**
	 * Gets the product feature.
	 * 
	 * @return the productFeature
	 */
	public String getProductFeature() {
		return mProductFeature;
	}

	/**
	 * Sets the product feature.
	 * 
	 * @param pProductFeature
	 *            the productFeature to set
	 */
	public void setProductFeature(String pProductFeature) {
		mProductFeature = pProductFeature;
	}

	/**
	 * Gets the product safety warnings.
	 * 
	 * @return the productSafetyWarnings
	 */
	public String getProductSafetyWarnings() {
		return mProductSafetyWarnings;
	}

	/**
	 * Sets the product safety warnings.
	 * 
	 * @param pProductSafetyWarnings
	 *            the productSafetyWarnings to set
	 */
	public void setProductSafetyWarnings(String pProductSafetyWarnings) {
		mProductSafetyWarnings = pProductSafetyWarnings;
	}

	/**
	 * Gets the promotional sticker display.
	 * 
	 * @return the promotionalStickerDisplay
	 */
	public String getPromotionalStickerDisplay() {
		return mPromotionalStickerDisplay;
	}

	/**
	 * Sets the promotional sticker display.
	 * 
	 * @param pPromotionalStickerDisplay
	 *            the promotionalStickerDisplay to set
	 */
	public void setPromotionalStickerDisplay(String pPromotionalStickerDisplay) {
		mPromotionalStickerDisplay = pPromotionalStickerDisplay;
	}

	/**
	 * Gets the registry warning indicator.
	 * 
	 * @return the registryWarningIndicator
	 */
	public String getRegistryWarningIndicator() {
		return mRegistryWarningIndicator;
	}

	/**
	 * Sets the registry warning indicator.
	 * 
	 * @param pRegistryWarningIndicator
	 *            the registryWarningIndicator to set
	 */
	public void setRegistryWarningIndicator(String pRegistryWarningIndicator) {
		mRegistryWarningIndicator = pRegistryWarningIndicator;
	}

	/**
	 * Gets the registry eligibility.
	 * 
	 * @return the registryEligibility
	 */
	public String getRegistryEligibility() {
		return mRegistryEligibility;
	}

	/**
	 * Sets the registry eligibility.
	 * 
	 * @param pRegistryEligibility
	 *            the registryEligibility to set
	 */
	public void setRegistryEligibility(String pRegistryEligibility) {
		mRegistryEligibility = pRegistryEligibility;
	}

	/**
	 * Gets the safety stock.
	 * 
	 * @return the safetyStock
	 */
	public String getSafetyStock() {
		return mSafetyStock;
	}

	/**
	 * Sets the safety stock.
	 * 
	 * @param pSafetyStock
	 *            the safetyStock to set
	 */
	public void setSafetyStock(String pSafetyStock) {
		mSafetyStock = pSafetyStock;
	}

	/**
	 * Gets the ship in orig container.
	 * 
	 * @return the shipInOrigContainer
	 */
	public String getShipInOrigContainer() {
		return mShipInOrigContainer;
	}

	/**
	 * Sets the ship in orig container.
	 * 
	 * @param pShipInOrigContainer
	 *            the shipInOrigContainer to set
	 */
	public void setShipInOrigContainer(String pShipInOrigContainer) {
		mShipInOrigContainer = pShipInOrigContainer;
	}

	/**
	 * Gets the strl best uses.
	 * 
	 * @return the strlBestUses
	 */
	public String getStrlBestUses() {
		return mStrlBestUses;
	}

	/**
	 * Sets the strl best uses.
	 * 
	 * @param pStrlBestUses
	 *            the strlBestUses to set
	 */
	public void setStrlBestUses(String pStrlBestUses) {
		mStrlBestUses = pStrlBestUses;
	}

	/**
	 * Gets the strl extras.
	 * 
	 * @return the strlExtras
	 */
	public String getStrlExtras() {
		return mStrlExtras;
	}

	/**
	 * Sets the strl extras.
	 * 
	 * @param pStrlExtras
	 *            the strlExtras to set
	 */
	public void setStrlExtras(String pStrlExtras) {
		mStrlExtras = pStrlExtras;
	}

	/**
	 * Gets the strl types.
	 * 
	 * @return the strlTypes
	 */
	public String getStrlTypes() {
		return mStrlTypes;
	}

	/**
	 * Sets the strl types.
	 * 
	 * @param pStrlTypes
	 *            the strlTypes to set
	 */
	public void setStrlTypes(String pStrlTypes) {
		mStrlTypes = pStrlTypes;
	}

	/**
	 * Gets the strl what is imp.
	 * 
	 * @return the strlWhatIsImp
	 */
	public String getStrlWhatIsImp() {
		return mStrlWhatIsImp;
	}

	/**
	 * Sets the strl what is imp.
	 * 
	 * @param pStrlWhatIsImp
	 *            the strlWhatIsImp to set
	 */
	public void setStrlWhatIsImp(String pStrlWhatIsImp) {
		mStrlWhatIsImp = pStrlWhatIsImp;
	}

	/**
	 * Gets the supress in search.
	 * 
	 * @return the supressInSearch
	 */
	public String getSupressInSearch() {
		return mSupressInSearch;
	}

	/**
	 * Sets the supress in search.
	 * 
	 * @param pSupressInSearch
	 *            the supressInSearch to set
	 */
	public void setSupressInSearch(String pSupressInSearch) {
		mSupressInSearch = pSupressInSearch;
	}

	/**
	 * Gets the system requirements.
	 * 
	 * @return the systemRequirements
	 */
	public String getSystemRequirements() {
		return mSystemRequirements;
	}

	/**
	 * Sets the system requirements.
	 * 
	 * @param pSystemRequirements
	 *            the systemRequirements to set
	 */
	public void setSystemRequirements(String pSystemRequirements) {
		mSystemRequirements = pSystemRequirements;
	}

	/**
	 * Gets the upc numbers.
	 * 
	 * @return the upcNumbers
	 */
	public String getUpcNumbers() {
		return mUpcNumbers;
	}

	/**
	 * Sets the upc numbers.
	 * 
	 * @param pUpcNumbers
	 *            the upcNumbers to set
	 */
	public void setUpcNumbers(String pUpcNumbers) {
		mUpcNumbers = pUpcNumbers;
	}

	/**
	 * Gets the video game esrb.
	 * 
	 * @return the videoGameEsrb
	 */
	public String getVideoGameEsrb() {
		return mVideoGameEsrb;
	}

	/**
	 * Sets the video game esrb.
	 * 
	 * @param pVideoGameEsrb
	 *            the videoGameEsrb to set
	 */
	public void setVideoGameEsrb(String pVideoGameEsrb) {
		mVideoGameEsrb = pVideoGameEsrb;
	}

	/**
	 * Gets the video game genre.
	 * 
	 * @return the videoGameGenre
	 */
	public String getVideoGameGenre() {
		return mVideoGameGenre;
	}

	/**
	 * Sets the video game genre.
	 * 
	 * @param pVideoGameGenre
	 *            the videoGameGenre to set
	 */
	public void setVideoGameGenre(String pVideoGameGenre) {
		mVideoGameGenre = pVideoGameGenre;
	}

	/**
	 * Gets the video game platform.
	 * 
	 * @return the videoGamePlatform
	 */
	public String getVideoGamePlatform() {
		return mVideoGamePlatform;
	}

	/**
	 * Sets the video game platform.
	 * 
	 * @param pVideoGamePlatform
	 *            the videoGamePlatform to set
	 */
	public void setVideoGamePlatform(String pVideoGamePlatform) {
		mVideoGamePlatform = pVideoGamePlatform;
	}

	/**
	 * Gets the nmwa.
	 * 
	 * @return the nmwa
	 */
	public String getNmwa() {
		return mNmwa;
	}

	/**
	 * Sets the nmwa.
	 * 
	 * @param pNmwa
	 *            the nmwa to set
	 */
	public void setNmwa(String pNmwa) {
		mNmwa = pNmwa;
	}

	/**
	 * Gets the web display flag.
	 * 
	 * @return the webDisplayFlag
	 */
	public String getWebDisplayFlag() {
		return mWebDisplayFlag;
	}

	/**
	 * Sets the web display flag.
	 * 
	 * @param pWebDisplayFlag
	 *            the webDisplayFlag to set
	 */
	public void setWebDisplayFlag(String pWebDisplayFlag) {
		mWebDisplayFlag = pWebDisplayFlag;
	}

	/**
	 * Gets the bpp eligible.
	 * 
	 * @return the bppEligible
	 */
	public String getBppEligible() {
		return mBppEligible;
	}

	/**
	 * Sets the bpp eligible.
	 * 
	 * @param pBppEligible
	 *            the bppEligible to set
	 */
	public void setBppEligible(String pBppEligible) {
		mBppEligible = pBppEligible;
	}

	/**
	 * Gets the bpp name.
	 * 
	 * @return the bppName
	 */
	public String getBppName() {
		return mBppName;
	}

	/**
	 * Sets the bpp name.
	 * 
	 * @param pBppName
	 *            the bppName to set
	 */
	public void setBppName(String pBppName) {
		mBppName = pBppName;
	}

	/**
	 * Gets the legal notice.
	 * 
	 * @return the legalNotice
	 */
	public String getLegalNotice() {
		return mLegalNotice;
	}

	/**
	 * Sets the legal notice.
	 * 
	 * @param pLegalNotice
	 *            the legalNotice to set
	 */
	public void setLegalNotice(String pLegalNotice) {
		mLegalNotice = pLegalNotice;
	}

	/**
	 * Gets the swatch image.
	 * 
	 * @return the swatchImage
	 */
	public String getSwatchImage() {
		return mSwatchImage;
	}

	/**
	 * Sets the swatch image.
	 * 
	 * @param pSwatchImage
	 *            the swatchImage to set
	 */
	public void setSwatchImage(String pSwatchImage) {
		mSwatchImage = pSwatchImage;
	}

	/**
	 * Gets the primary image.
	 * 
	 * @return the primaryImage
	 */
	public String getPrimaryImage() {
		return mPrimaryImage;
	}

	/**
	 * Sets the primary image.
	 * 
	 * @param pPrimaryImage
	 *            the primaryImage to set
	 */
	public void setPrimaryImage(String pPrimaryImage) {
		mPrimaryImage = pPrimaryImage;
	}

	/**
	 * Gets the secondary image.
	 * 
	 * @return the secondaryImage
	 */
	public String getSecondaryImage() {
		return mSecondaryImage;
	}

	/**
	 * Sets the secondary image.
	 * 
	 * @param pSecondaryImage
	 *            the secondaryImage to set
	 */
	public void setSecondaryImage(String pSecondaryImage) {
		mSecondaryImage = pSecondaryImage;
	}

	/**
	 * Gets the alternate image.
	 * 
	 * @return the alternateImage
	 */
	public String getAlternateImage() {
		return mAlternateImage;
	}

	/**
	 * Sets the alternate image.
	 * 
	 * @param pAlternateImage
	 *            the alternateImage to set
	 */
	public void setAlternateImage(String pAlternateImage) {
		mAlternateImage = pAlternateImage;
	}

	/**
	 * Gets the crosssell battery.
	 * 
	 * @return the crosssellBattery
	 */
	public String getCrosssellBattery() {
		return mCrosssellBattery;
	}

	/**
	 * Sets the crosssell battery.
	 * 
	 * @param pCrosssellBattery
	 *            the crosssellBattery to set
	 */
	public void setCrosssellBattery(String pCrosssellBattery) {
		mCrosssellBattery = pCrosssellBattery;
	}

	/**
	 * Gets the crosssell batteries.
	 * 
	 * @return the crosssellBatteries
	 */
	public String getCrosssellBatteries() {
		return mCrosssellBatteries;
	}

	/**
	 * Sets the crosssell batteries.
	 * 
	 * @param pCrosssellBatteries
	 *            the crosssellBatteries to set
	 */
	public void setCrosssellBatteries(String pCrosssellBatteries) {
		mCrosssellBatteries = pCrosssellBatteries;
	}

	/**
	 * Gets the battery.
	 * 
	 * @return the battery
	 */
	public String getBattery() {
		return mBattery;
	}

	/**
	 * Sets the battery.
	 * 
	 * @param pBattery
	 *            the battery to set
	 */
	public void setBattery(String pBattery) {
		mBattery = pBattery;
	}

	/**
	 * Gets the batteries.
	 * 
	 * @return the batteries
	 */
	public String getBatteries() {
		return mBatteries;
	}

	/**
	 * Sets the batteries.
	 * 
	 * @param pBatteries
	 *            the batteries to set
	 */
	public void setBatteries(String pBatteries) {
		mBatteries = pBatteries;
	}

	/**
	 * Gets the cross sells batteries.
	 * 
	 * @return the crossSellsBatteries
	 */
	public String getCrossSellsBatteries() {
		return mCrossSellsBatteries;
	}

	/**
	 * Sets the cross sells batteries.
	 * 
	 * @param pCrossSellsBatteries
	 *            the crossSellsBatteries to set
	 */
	public void setCrossSellsBatteries(String pCrossSellsBatteries) {
		mCrossSellsBatteries = pCrossSellsBatteries;
	}

	/**
	 * Gets the uid battery cross sell.
	 * 
	 * @return the uidBatteryCrossSell
	 */
	public String getUidBatteryCrossSell() {
		return mUidBatteryCrossSell;
	}

	/**
	 * Sets the uid battery cross sell.
	 * 
	 * @param pUidBatteryCrossSell
	 *            the uidBatteryCrossSell to set
	 */
	public void setUidBatteryCrossSell(String pUidBatteryCrossSell) {
		mUidBatteryCrossSell = pUidBatteryCrossSell;
	}

	/**
	 * Gets the battery type.
	 * 
	 * @return the batteryType
	 */
	public String getBatteryType() {
		return mBatteryType;
	}

	/**
	 * Sets the battery type.
	 * 
	 * @param pBatteryType
	 *            the batteryType to set
	 */
	public void setBatteryType(String pBatteryType) {
		mBatteryType = pBatteryType;
	}

	/**
	 * Gets the battery quantity.
	 * 
	 * @return the batteryQuantity
	 */
	public String getBatteryQuantity() {
		return mBatteryQuantity;
	}

	/**
	 * Sets the battery quantity.
	 * 
	 * @param pBatteryQuantity
	 *            the batteryQuantity to set
	 */
	public void setBatteryQuantity(String pBatteryQuantity) {
		mBatteryQuantity = pBatteryQuantity;
	}

	/**
	 * Gets the cross sell sku id.
	 * 
	 * @return the crossSellSkuId
	 */
	public String getCrossSellSkuId() {
		return mCrossSellSkuId;
	}

	/**
	 * Sets the cross sell sku id.
	 * 
	 * @param pCrossSellSkuId
	 *            the crossSellSkuId to set
	 */
	public void setCrossSellSkuId(String pCrossSellSkuId) {
		mCrossSellSkuId = pCrossSellSkuId;
	}

	/**
	 * Gets the slapper item.
	 * 
	 * @return the slapperItem
	 */
	public String getSlapperItem() {
		return mSlapperItem;
	}

	/**
	 * Sets the slapper item.
	 * 
	 * @param pSlapperItem
	 *            the slapperItem to set
	 */
	public void setSlapperItem(String pSlapperItem) {
		mSlapperItem = pSlapperItem;
	}

	/**
	 * Gets the ship from store eligible lov.
	 *
	 * @return the ship from store eligible lov
	 */
	public String getShipFromStoreEligibleLov() {
		return mShipFromStoreEligibleLov;
	}
	
	/**
	 * Sets the ship from store eligible lov.
	 *
	 * @param pShipFromStoreEligibleLov the new ship from store eligible lov
	 */
	public void setShipFromStoreEligibleLov(String pShipFromStoreEligibleLov) {
		mShipFromStoreEligibleLov = pShipFromStoreEligibleLov;
	}
	/**
	 * Gets the ship to store eeligible.
	 * 
	 * @return the shipToStoreEeligible
	 */
	public String getShipToStoreEeligible() {
		return mShipToStoreEeligible;
	}

	/**
	 * Sets the ship to store eeligible.
	 * 
	 * @param pShipToStoreEeligible
	 *            the shipToStoreEeligible to set
	 */
	public void setShipToStoreEeligible(String pShipToStoreEeligible) {
		mShipToStoreEeligible = pShipToStoreEeligible;
	}

	/**
	 * Gets the super display flag.
	 * 
	 * @return the superDisplayFlag
	 */
	public String getSuperDisplayFlag() {
		return mSuperDisplayFlag;
	}

	/**
	 * Sets the super display flag.
	 * 
	 * @param pSuperDisplayFlag
	 *            the superDisplayFlag to set
	 */
	public void setSuperDisplayFlag(String pSuperDisplayFlag) {
		mSuperDisplayFlag = pSuperDisplayFlag;
	}

	/**
	 * Gets the review rating.
	 * 
	 * @return the reviewRating
	 */
	public String getReviewRating() {
		return mReviewRating;
	}

	/**
	 * Sets the review rating.
	 * 
	 * @param pReviewRating
	 *            the reviewRating to set
	 */
	public void setReviewRating(String pReviewRating) {
		mReviewRating = pReviewRating;
	}

	/**
	 * Gets the review count.
	 * 
	 * @return the reviewCount
	 */
	public String getReviewCount() {
		return mReviewCount;
	}

	/**
	 * Sets the review count.
	 * 
	 * @param pReviewCount
	 *            the reviewCount to set
	 */
	public void setReviewCount(String pReviewCount) {
		mReviewCount = pReviewCount;
	}

	/**
	 * Gets the root parent categories.
	 * 
	 * @return the rootParentCategories
	 */
	public String getRootParentCategories() {
		return mRootParentCategories;
	}

	/**
	 * Sets the root parent categories.
	 * 
	 * @param pRootParentCategories
	 *            the rootParentCategories to set
	 */
	public void setRootParentCategories(String pRootParentCategories) {
		mRootParentCategories = pRootParentCategories;
	}

	/**
	 * Gets the parent classifications.
	 * 
	 * @return the parentClassifications
	 */
	public String getParentClassifications() {
		return mParentClassifications;
	}

	/**
	 * Sets the parent classifications.
	 * 
	 * @param pParentClassifications
	 *            the parentClassifications to set
	 */
	public void setParentClassifications(String pParentClassifications) {
		mParentClassifications = pParentClassifications;
	}

	/**
	 * Gets the car seat properties map.
	 * 
	 * @return the carSeatPropertiesMap
	 */
	public String getCarSeatPropertiesMap() {
		return mCarSeatPropertiesMap;
	}

	/**
	 * Sets the car seat properties map.
	 * 
	 * @param pCarSeatPropertiesMap
	 *            the carSeatPropertiesMap to set
	 */
	public void setCarSeatPropertiesMap(String pCarSeatPropertiesMap) {
		mCarSeatPropertiesMap = pCarSeatPropertiesMap;
	}

	/**
	 * Gets the stroller properties map.
	 * 
	 * @return the strollerPropertiesMap
	 */
	public String getStrollerPropertiesMap() {
		return mStrollerPropertiesMap;
	}

	/**
	 * Sets the stroller properties map.
	 * 
	 * @param pStrollerPropertiesMap
	 *            the strollerPropertiesMap to set
	 */
	public void setStrollerPropertiesMap(String pStrollerPropertiesMap) {
		mStrollerPropertiesMap = pStrollerPropertiesMap;
	}

	/**
	 * Gets the crib properties map.
	 * 
	 * @return the cribPropertiesMap
	 */
	public String getCribPropertiesMap() {
		return mCribPropertiesMap;
	}

	/**
	 * Sets the crib properties map.
	 * 
	 * @param pCribPropertiesMap
	 *            the cribPropertiesMap to set
	 */
	public void setCribPropertiesMap(String pCribPropertiesMap) {
		mCribPropertiesMap = pCribPropertiesMap;
	}

	/**
	 * Gets the orignal parent product.
	 * 
	 * @return the orignalParentProduct
	 */
	public String getOrignalParentProduct() {
		return mOrignalParentProduct;
	}

	/**
	 * Sets the orignal parent product.
	 * 
	 * @param pOrignalParentProduct
	 *            the orignalParentProduct to set
	 */
	public void setOrignalParentProduct(String pOrignalParentProduct) {
		mOrignalParentProduct = pOrignalParentProduct;
	}

	/**
	 * Gets the classification id property name.
	 * 
	 * @return the classification id property name
	 */
	public String getClassificationIdPropertyName() {
		return mClassificationIdPropertyName;
	}

	/**
	 * Sets the classification id property name.
	 * 
	 * @param pClassificationIdPropertyName
	 *            the new classification id property name
	 */
	public void setClassificationIdPropertyName(String pClassificationIdPropertyName) {
		this.mClassificationIdPropertyName = pClassificationIdPropertyName;
	}

	/**
	 * Gets the name property name.
	 * 
	 * @return the name property name
	 */
	public String getNamePropertyName() {
		return mNamePropertyName;
	}

	/**
	 * Sets the name property name.
	 * 
	 * @param pNamePropertyName
	 *            the new name property name
	 */
	public void setNamePropertyName(String pNamePropertyName) {
		this.mNamePropertyName = pNamePropertyName;
	}

	/**
	 * Gets the user type id property name.
	 * 
	 * @return the user type id property name
	 */
	public String getUserTypeIdPropertyName() {
		return mUserTypeIdPropertyName;
	}

	/**
	 * Sets the user type id property name.
	 * 
	 * @param pUserTypeIdPropertyName
	 *            the new user type id property name
	 */
	public void setUserTypeIdPropertyName(String pUserTypeIdPropertyName) {
		this.mUserTypeIdPropertyName = pUserTypeIdPropertyName;
	}

	/**
	 * Gets the display order property name.
	 * 
	 * @return the display order property name
	 */
	public String getDisplayOrderPropertyName() {
		return mDisplayOrderPropertyName;
	}

	/**
	 * Sets the display order property name.
	 * 
	 * @param pDisplayOrderPropertyName
	 *            the new display order property name
	 */
	public void setDisplayOrderPropertyName(String pDisplayOrderPropertyName) {
		this.mDisplayOrderPropertyName = pDisplayOrderPropertyName;
	}

	/**
	 * Gets the have value property name.
	 * 
	 * @return the have value property name
	 */
	public String getHaveValuePropertyName() {
		return mHaveValuePropertyName;
	}

	/**
	 * Sets the have value property name.
	 * 
	 * @param pHaveValuePropertyName
	 *            the new have value property name
	 */
	public void setHaveValuePropertyName(String pHaveValuePropertyName) {
		this.mHaveValuePropertyName = pHaveValuePropertyName;
	}

	/**
	 * Gets the suggested quantity property name.
	 * 
	 * @return the suggested quantity property name
	 */
	public String getSuggestedQuantityPropertyName() {
		return mSuggestedQuantityPropertyName;
	}

	/**
	 * Sets the suggested quantity property name.
	 * 
	 * @param pSuggestedQuantityPropertyName
	 *            the new suggested quantity property name
	 */
	public void setSuggestedQuantityPropertyName(String pSuggestedQuantityPropertyName) {
		this.mSuggestedQuantityPropertyName = pSuggestedQuantityPropertyName;
	}

	/**
	 * Gets the classification type property name.
	 * 
	 * @return the classification type property name
	 */
	public String getClassificationTypePropertyName() {
		return mClassificationTypePropertyName;
	}

	/**
	 * Sets the classification type property name.
	 * 
	 * @param pClassificationTypePropertyName
	 *            the new classification type property name
	 */
	public void setClassificationTypePropertyName(String pClassificationTypePropertyName) {
		this.mClassificationTypePropertyName = pClassificationTypePropertyName;
	}

	/**
	 * Gets the parent category property name.
	 * 
	 * @return the parent category property name
	 */
	public String getParentCategoryPropertyName() {
		return mParentCategoryPropertyName;
	}

	/**
	 * Sets the parent category property name.
	 * 
	 * @param pParentCategoryPropertyName
	 *            the new parent category property name
	 */
	public void setParentCategoryPropertyName(String pParentCategoryPropertyName) {
		this.mParentCategoryPropertyName = pParentCategoryPropertyName;
	}

	/**
	 * Gets the root parent category property name.
	 * 
	 * @return the root parent category property name
	 */
	public String getRootParentCategoryPropertyName() {
		return mRootParentCategoryPropertyName;
	}

	/**
	 * Sets the root parent category property name.
	 * 
	 * @param pRootParentCategoryPropertyName
	 *            the new root parent category property name
	 */
	public void setRootParentCategoryPropertyName(String pRootParentCategoryPropertyName) {
		this.mRootParentCategoryPropertyName = pRootParentCategoryPropertyName;
	}

	/**
	 * Gets the child classifications property name.
	 * 
	 * @return the child classifications property name
	 */
	public String getChildClassificationsPropertyName() {
		return mChildClassificationsPropertyName;
	}

	/**
	 * Sets the child classifications property name.
	 * 
	 * @param pChildClassificationsPropertyName
	 *            the new child classifications property name
	 */
	public void setChildClassificationsPropertyName(String pChildClassificationsPropertyName) {
		this.mChildClassificationsPropertyName = pChildClassificationsPropertyName;
	}

	/**
	 * Gets the classification property name.
	 * 
	 * @return the classification property name
	 */
	public String getClassificationPropertyName() {
		return mClassificationPropertyName;
	}

	/**
	 * Sets the classification property name.
	 * 
	 * @param pClassificationPropertyName
	 *            the new classification property name
	 */
	public void setClassificationPropertyName(String pClassificationPropertyName) {
		this.mClassificationPropertyName = pClassificationPropertyName;
	}

	/**
	 * Gets the cross reference property name.
	 * 
	 * @return the cross reference property name
	 */
	public String getCrossReferencePropertyName() {
		return mCrossReferencePropertyName;
	}

	/**
	 * Sets the cross reference property name.
	 * 
	 * @param pCrossReferencePropertyName
	 *            the new cross reference property name
	 */
	public void setCrossReferencePropertyName(String pCrossReferencePropertyName) {
		this.mCrossReferencePropertyName = pCrossReferencePropertyName;
	}

	/**
	 * Gets the parent classifications property name.
	 * 
	 * @return the parent classifications property name
	 */
	public String getParentClassificationsPropertyName() {
		return mParentClassificationsPropertyName;
	}

	/**
	 * Sets the parent classifications property name.
	 * 
	 * @param pParentClassificationsPropertyName
	 *            the new parent classifications property name
	 */
	public void setParentClassificationsPropertyName(String pParentClassificationsPropertyName) {
		this.mParentClassificationsPropertyName = pParentClassificationsPropertyName;
	}

	/**
	 * Gets the long description property name.
	 * 
	 * @return the long description property name
	 */
	public String getLongDescriptionPropertyName() {
		return mLongDescriptionPropertyName;
	}

	/**
	 * Sets the long description property name.
	 * 
	 * @param pLongDescriptionPropertyName
	 *            the new long description property name
	 */
	public void setLongDescriptionPropertyName(String pLongDescriptionPropertyName) {
		this.mLongDescriptionPropertyName = pLongDescriptionPropertyName;
	}

	/**
	 * Gets the display status property name.
	 * 
	 * @return the display status property name
	 */
	public String getDisplayStatusPropertyName() {
		return mDisplayStatusPropertyName;
	}

	/**
	 * Sets the display status property name.
	 * 
	 * @param pDisplayStatusPropertyName
	 *            the new display status property name
	 */
	public void setDisplayStatusPropertyName(String pDisplayStatusPropertyName) {
		this.mDisplayStatusPropertyName = pDisplayStatusPropertyName;
	}

	/**
	 * Gets the parent categories.
	 * 
	 * @return the parent categories
	 */
	public String getParentCategories() {
		return mParentCategories;
	}

	/**
	 * Sets the parent categories.
	 * 
	 * @param pParentCategories
	 *            the new parent categories
	 */
	public void setParentCategories(String pParentCategories) {
		mParentCategories = pParentCategories;
	}

	/**
	 * Gets the online collection name.
	 *
	 * @return the online collection name
	 */
	public String getOnlineCollectionName() {
		return mOnlineCollectionName;
	}

	/**
	 * Sets the online collection name.
	 *
	 * @param pOnlineCollectionName the new online collection name
	 */
	public void setOnlineCollectionName(String pOnlineCollectionName) {
		mOnlineCollectionName = pOnlineCollectionName;
	}

	/**
	 * Gets the inventory received date.
	 *
	 * @return the inventory received date
	 */
	public String getInventoryReceivedDate() {
		return mInventoryReceivedDate;
	}

	/**
	 * Sets the inventory received date.
	 *
	 * @param pInventoryReceivedDate the new inventory first available date
	 */
	public void setInventoryReceivedDate(String pInventoryReceivedDate) {
		mInventoryReceivedDate = pInventoryReceivedDate;
	}
	
	/** The Collection skus. */
	private String mCollectionSkus;

	/** The Primary path category. */
	private String mPrimaryPathCategory;


	/**
	 * Gets the collection skus.
	 *
	 * @return the collection skus
	 */
	public String getCollectionSkus() {
		return mCollectionSkus;
	}

	/**
	 * Sets the collection skus.
	 *
	 * @param pCollectionSkus the new collection skus
	 */
	public void setCollectionSkus(String pCollectionSkus) {
		mCollectionSkus = pCollectionSkus;
	}

	/**
	 * Gets the primary path category.
	 *
	 * @return the primary path category
	 */
	public String getPrimaryPathCategory() {
		return mPrimaryPathCategory;
	}

	/**
	 * Sets the primary path category.
	 *
	 * @param pPrimaryPathCategory the new primary path category
	 */
	public void setPrimaryPathCategory(String pPrimaryPathCategory) {
		mPrimaryPathCategory = pPrimaryPathCategory;
	}

	/**
	 * Gets the cross ref display order property name.
	 *
	 * @return the cross ref display order property name
	 */
	public String getCrossRefDisplayOrderPropertyName() {
		return mCrossRefDisplayOrderPropertyName;
	}

	/**
	 * Sets the cross ref display order property name.
	 *
	 * @param pCrossRefDisplayOrderPropertyName the new cross ref display order property name
	 */
	public void setCrossRefDisplayOrderPropertyName(
			String pCrossRefDisplayOrderPropertyName) {
		mCrossRefDisplayOrderPropertyName = pCrossRefDisplayOrderPropertyName;
	}

	/**
	 * Gets the cross ref display name property name.
	 *
	 * @return the cross ref display name property name
	 */
	public String getCrossRefDisplayNamePropertyName() {
		return mCrossRefDisplayNamePropertyName;
	}

	/**
	 * Sets the cross ref display name property name.
	 *
	 * @param pCrossRefDisplayNamePropertyName the new cross ref display name property name
	 */
	public void setCrossRefDisplayNamePropertyName(
			String pCrossRefDisplayNamePropertyName) {
		mCrossRefDisplayNamePropertyName = pCrossRefDisplayNamePropertyName;
	}

	/**
	 * Gets the brand.
	 *
	 * @return the brand
	 */
	public String getBrand() {
		return mBrand;
	}

	/**
	 * Sets the brand.
	 *
	 * @param pBrand the new brand
	 */
	public void setBrand(String pBrand) {
		mBrand = pBrand;
	}

	/**
	 * Gets the why we love it.
	 *
	 * @return the why we love it
	 */
	public String getWhyWeLoveIt() {
		return mWhyWeLoveIt;
	}

	/**
	 * Sets the why we love it.
	 *
	 * @param pWhyWeLoveIt the new why we love it
	 */
	public void setWhyWeLoveIt(String pWhyWeLoveIt) {
		mWhyWeLoveIt = pWhyWeLoveIt;
	}

	/**
	 * Gets the primary parent category.
	 *
	 * @return the primaryParentCategory
	 */
	public String getPrimaryParentCategory() {
		return mPrimaryParentCategory;
	}

	/**
	 * Sets the primary parent category.
	 *
	 * @param pPrimaryParentCategory the primaryParentCategory to set
	 */
	public void setPrimaryParentCategory(String pPrimaryParentCategory) {
		mPrimaryParentCategory = pPrimaryParentCategory;
	}

	/**
	 * Gets the color family.
	 *
	 * @return the colorFamily
	 */
	public String getColorFamily() {
		return mColorFamily;
	}

	/**
	 * Sets the color family.
	 *
	 * @param pColorFamily the colorFamily to set
	 */
	public void setColorFamily(String pColorFamily) {
		mColorFamily = pColorFamily;
	}

	/**
	 * Gets the size family.
	 *
	 * @return the sizeFamily
	 */
	public String getSizeFamily() {
		return mSizeFamily;
	}

	/**
	 * Sets the size family.
	 *
	 * @param pSizeFamily the sizeFamily to set
	 */
	public void setSizeFamily(String pSizeFamily) {
		mSizeFamily = pSizeFamily;
	}

	/**
	 * Gets the character theme.
	 *
	 * @return the characterTheme
	 */
	public String getCharacterTheme() {
		return mCharacterTheme;
	}

	/**
	 * Sets the character theme.
	 *
	 * @param pCharacterTheme the characterTheme to set
	 */
	public void setCharacterTheme(String pCharacterTheme) {
		mCharacterTheme = pCharacterTheme;
	}

	/**
	 * Gets the collection theme.
	 *
	 * @return the collectionTheme
	 */
	public String getCollectionTheme() {
		return mCollectionTheme;
	}

	/**
	 * Sets the collection theme.
	 *
	 * @param pCollectionTheme the collectionTheme to set
	 */
	public void setCollectionTheme(String pCollectionTheme) {
		mCollectionTheme = pCollectionTheme;
	}

}
