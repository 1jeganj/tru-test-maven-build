package com.tru.feedprocessor.tol.catalog.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class TRUProductsVO.
 * 
 * VO class which will store all product VOs, regularSKU VOs, StrollerSKU VOs, CribSKU VOs, CarSeatSKU VOs, BookCdDvDSKU VOs,
 * NonMerchSKU VOs and VideoGameSKU VOs
 * @version 1.0
 * @author Professional Access
 */
public class TRUProductsVO {

	/** The TRU product feed vo list. */
	private List<TRUProductFeedVO> mTRUProductFeedVOList;

	/** property to hold TRUSKUFeedVO List. */
	private List<TRUSkuFeedVO> mTRUSKUFeedVOList;

	/** property to hold TRUStrollerSKU List. */
	private List<TRUStrollerSKUVO> mTRUStrollerSKUList;

	/** property to hold TRUCribSKUVO List. */
	private List<TRUCribSKUVO> mTRUCribSKUVOList;

	/** property to hold TRUCarSeatSKUVO List. */
	private List<TRUCarSeatSKUVO> mTRUCarSeatSKUVOList;

	/** property to hold TRUBookCdDvDSKUVO List. */
	private List<TRUBookCdDvDSKUVO> mTRUBookCdDvDSKUVOList;

	/** property to hold TRUVideoGameSKUVO List. */
	private List<TRUVideoGameSKUVO> mTRUVideoGameSKUVOList;

	/** property to hold TRUNonMerchSKUVO List. */
	private List<TRUNonMerchSKUVO> mTRUNonMerchSKUVOList;

	/** property to hold TRUProductFeedVO Map. */
	private Map<String, TRUProductFeedVO> mTRUProductFeedVOMap;

	/** property to hold TRUSKUFeedVO Map. */
	private Map<String, TRUSkuFeedVO> mTRUSKUFeedVOMap;

	/** property to hold TRUStrollerSKU Map. */
	private Map<String, TRUStrollerSKUVO> mTRUStrollerSKUMap;

	/** property to hold TRUCribSKUVO Map. */
	private Map<String, TRUCribSKUVO> mTRUCribSKUVOMap;

	/** property to hold TRUCarSeatSKUVO Map. */
	private Map<String, TRUCarSeatSKUVO> mTRUCarSeatSKUVOMap;

	/** property to hold TRUBookCdDvDSKUVO Map. */
	private Map<String, TRUBookCdDvDSKUVO> mTRUBookCdDvDSKUVOMap;

	/** property to hold TRUVideoGameSKUVO Map. */
	private Map<String, TRUVideoGameSKUVO> mTRUVideoGameSKUVOMap;

	/** property to hold TRUNonMerchSKUVO Map. */
	private Map<String, TRUNonMerchSKUVO> mTRUNonMerchSKUVOMap;

	/**
	 * Gets the TRUSKU feed vo map.
	 * 
	 * @return the tRUSKUFeedVOMap
	 */
	public Map<String, TRUSkuFeedVO> getTRUSKUFeedVOMap() {
		if (mTRUSKUFeedVOMap == null) {
			mTRUSKUFeedVOMap = new HashMap<String, TRUSkuFeedVO>();
		}
		return mTRUSKUFeedVOMap;
	}

	/**
	 * Sets the trusku feed vo map.
	 * 
	 * @param pTRUSKUFeedVOMap
	 *            the tRUSKUFeedVOMap to set
	 */
	public void setTRUSKUFeedVOMap(Map<String, TRUSkuFeedVO> pTRUSKUFeedVOMap) {
		mTRUSKUFeedVOMap = pTRUSKUFeedVOMap;
	}

	/**
	 * Gets the TRU stroller sku map.
	 * 
	 * @return the tRUStrollerSKUMap
	 */
	public Map<String, TRUStrollerSKUVO> getTRUStrollerSKUMap() {
		if (mTRUStrollerSKUMap == null) {
			mTRUStrollerSKUMap = new HashMap<String, TRUStrollerSKUVO>();
		}
		return mTRUStrollerSKUMap;
	}

	/**
	 * Sets the tru stroller sku map.
	 * 
	 * @param pTRUStrollerSKUMap
	 *            the tRUStrollerSKUMap to set
	 */
	public void setTRUStrollerSKUMap(Map<String, TRUStrollerSKUVO> pTRUStrollerSKUMap) {
		mTRUStrollerSKUMap = pTRUStrollerSKUMap;
	}

	/**
	 * Gets the TRU crib skuvo map.
	 * 
	 * @return the tRUCribSKUVOMap
	 */
	public Map<String, TRUCribSKUVO> getTRUCribSKUVOMap() {
		if (mTRUCribSKUVOMap == null) {
			mTRUCribSKUVOMap = new HashMap<String, TRUCribSKUVO>();
		}
		return mTRUCribSKUVOMap;
	}

	/**
	 * Sets the tru crib skuvo map.
	 * 
	 * @param pTRUCribSKUVOMap
	 *            the tRUCribSKUVOMap to set
	 */
	public void setTRUCribSKUVOMap(Map<String, TRUCribSKUVO> pTRUCribSKUVOMap) {
		mTRUCribSKUVOMap = pTRUCribSKUVOMap;
	}

	/**
	 * Gets the TRU car seat skuvo map.
	 * 
	 * @return the tRUCarSeatSKUVOMap
	 */
	public Map<String, TRUCarSeatSKUVO> getTRUCarSeatSKUVOMap() {
		if (mTRUCarSeatSKUVOMap == null) {
			mTRUCarSeatSKUVOMap = new HashMap<String, TRUCarSeatSKUVO>();
		}
		return mTRUCarSeatSKUVOMap;
	}

	/**
	 * Sets the tru car seat skuvo map.
	 * 
	 * @param pTRUCarSeatSKUVOMap
	 *            the tRUCarSeatSKUVOMap to set
	 */
	public void setTRUCarSeatSKUVOMap(Map<String, TRUCarSeatSKUVO> pTRUCarSeatSKUVOMap) {
		mTRUCarSeatSKUVOMap = pTRUCarSeatSKUVOMap;
	}

	/**
	 * Gets the TRU book cd dv dskuvo map.
	 * 
	 * @return the tRUBookCdDvDSKUVOMap
	 */
	public Map<String, TRUBookCdDvDSKUVO> getTRUBookCdDvDSKUVOMap() {
		if (mTRUBookCdDvDSKUVOMap == null) {
			mTRUBookCdDvDSKUVOMap = new HashMap<String, TRUBookCdDvDSKUVO>();
		}
		return mTRUBookCdDvDSKUVOMap;
	}

	/**
	 * Sets the tru book cd dv dskuvo map.
	 * 
	 * @param pTRUBookCdDvDSKUVOMap
	 *            the tRUBookCdDvDSKUVOMap to set
	 */
	public void setTRUBookCdDvDSKUVOMap(Map<String, TRUBookCdDvDSKUVO> pTRUBookCdDvDSKUVOMap) {
		mTRUBookCdDvDSKUVOMap = pTRUBookCdDvDSKUVOMap;
	}

	/**
	 * Gets the TRU video game skuvo map.
	 * 
	 * @return the tRUVideoGameSKUVOMap
	 */
	public Map<String, TRUVideoGameSKUVO> getTRUVideoGameSKUVOMap() {
		if (mTRUVideoGameSKUVOMap == null) {
			mTRUVideoGameSKUVOMap = new HashMap<String, TRUVideoGameSKUVO>();
		}
		return mTRUVideoGameSKUVOMap;
	}

	/**
	 * Sets the tru video game skuvo map.
	 * 
	 * @param pTRUVideoGameSKUVOMap
	 *            the tRUVideoGameSKUVOMap to set
	 */
	public void setTRUVideoGameSKUVOMap(Map<String, TRUVideoGameSKUVO> pTRUVideoGameSKUVOMap) {
		mTRUVideoGameSKUVOMap = pTRUVideoGameSKUVOMap;
	}

	/**
	 * Gets the TRU product feed vo map.
	 * 
	 * @return the tRUProductFeedVOMap
	 */
	public Map<String, TRUProductFeedVO> getTRUProductFeedVOMap() {
		if (mTRUProductFeedVOMap == null) {
			mTRUProductFeedVOMap = new HashMap<String, TRUProductFeedVO>();
		}
		return mTRUProductFeedVOMap;
	}

	/**
	 * Sets the tru product feed vo map.
	 * 
	 * @param pTRUProductFeedVOMap
	 *            the tRUProductFeedVOMap to set
	 */
	public void setTRUProductFeedVOMap(Map<String, TRUProductFeedVO> pTRUProductFeedVOMap) {
		mTRUProductFeedVOMap = pTRUProductFeedVOMap;
	}

	/**
	 * Gets the TRU product feed vo list.
	 * 
	 * @return the tRUProductFeedVOList
	 */
	public List<TRUProductFeedVO> getTRUProductFeedVOList() {
		if (mTRUProductFeedVOList == null) {
			mTRUProductFeedVOList = new ArrayList<TRUProductFeedVO>();
		}
		return mTRUProductFeedVOList;
	}

	/**
	 * Sets the TRU product feed vo list.
	 * 
	 * @param pTRUProductFeedVOList
	 *            the tRUProductFeedVOList to set
	 */
	public void setTRUProductFeedVOList(List<TRUProductFeedVO> pTRUProductFeedVOList) {
		mTRUProductFeedVOList = pTRUProductFeedVOList;
	}

	/**
	 * Gets the TRUSKU feed vo list.
	 * 
	 * @return the tRUSKUFeedVOList
	 */
	public List<TRUSkuFeedVO> getTRUSKUFeedVOList() {

		if (mTRUSKUFeedVOList == null) {
			mTRUSKUFeedVOList = new ArrayList<TRUSkuFeedVO>();
		}
		return mTRUSKUFeedVOList;
	}

	/**
	 * Sets the TRUSKU feed vo list.
	 * 
	 * @param pTRUSKUFeedVOList
	 *            the tRUSKUFeedVOList to set
	 */
	public void setTRUSKUFeedVOList(List<TRUSkuFeedVO> pTRUSKUFeedVOList) {
		mTRUSKUFeedVOList = pTRUSKUFeedVOList;
	}

	/**
	 * Gets the TRU stroller sku list.
	 * 
	 * @return the tRUStrollerSKUList
	 */
	public List<TRUStrollerSKUVO> getTRUStrollerSKUList() {
		if (mTRUStrollerSKUList == null) {
			mTRUStrollerSKUList = new ArrayList<TRUStrollerSKUVO>();
		}
		return mTRUStrollerSKUList;
	}

	/**
	 * Sets the TRU stroller sku list.
	 * 
	 * @param pTRUStrollerSKUList
	 *            the tRUStrollerSKUList to set
	 */
	public void setTRUStrollerSKUList(List<TRUStrollerSKUVO> pTRUStrollerSKUList) {
		mTRUStrollerSKUList = pTRUStrollerSKUList;
	}

	/**
	 * Gets the TRU crib skuvo list.
	 * 
	 * @return the tRUCribSKUVOList
	 */
	public List<TRUCribSKUVO> getTRUCribSKUVOList() {
		if (mTRUCribSKUVOList == null) {
			mTRUCribSKUVOList = new ArrayList<TRUCribSKUVO>();
		}
		return mTRUCribSKUVOList;
	}

	/**
	 * Sets the TRU crib skuvo list.
	 * 
	 * @param pTRUCribSKUVOList
	 *            the tRUCribSKUVOList to set
	 */
	public void setTRUCribSKUVOList(List<TRUCribSKUVO> pTRUCribSKUVOList) {
		mTRUCribSKUVOList = pTRUCribSKUVOList;
	}

	/**
	 * Gets the TRU car seat skuvo list.
	 * 
	 * @return the tRUCarSeatSKUVOList
	 */
	public List<TRUCarSeatSKUVO> getTRUCarSeatSKUVOList() {
		if (mTRUCarSeatSKUVOList == null) {
			mTRUCarSeatSKUVOList = new ArrayList<TRUCarSeatSKUVO>();
		}
		return mTRUCarSeatSKUVOList;
	}

	/**
	 * Sets the TRU car seat skuvo list.
	 * 
	 * @param pTRUCarSeatSKUVOList
	 *            the tRUCarSeatSKUVOList to set
	 */
	public void setTRUCarSeatSKUVOList(List<TRUCarSeatSKUVO> pTRUCarSeatSKUVOList) {
		mTRUCarSeatSKUVOList = pTRUCarSeatSKUVOList;
	}

	/**
	 * Gets the TRU book cd dv dskuvo list.
	 * 
	 * @return the tRUBookCdDvDSKUVOList
	 */
	public List<TRUBookCdDvDSKUVO> getTRUBookCdDvDSKUVOList() {
		if (mTRUBookCdDvDSKUVOList == null) {
			mTRUBookCdDvDSKUVOList = new ArrayList<TRUBookCdDvDSKUVO>();
		}
		return mTRUBookCdDvDSKUVOList;
	}

	/**
	 * Sets the TRU book cd dv dskuvo list.
	 * 
	 * @param pTRUBookCdDvDSKUVOList
	 *            the tRUBookCdDvDSKUVOList to set
	 */
	public void setTRUBookCdDvDSKUVOList(List<TRUBookCdDvDSKUVO> pTRUBookCdDvDSKUVOList) {
		mTRUBookCdDvDSKUVOList = pTRUBookCdDvDSKUVOList;
	}

	/**
	 * Gets the TRU video game skuvo list.
	 * 
	 * @return the tRUVideoGameSKUVOList
	 */
	public List<TRUVideoGameSKUVO> getTRUVideoGameSKUVOList() {
		if (mTRUVideoGameSKUVOList == null) {
			mTRUVideoGameSKUVOList = new ArrayList<TRUVideoGameSKUVO>();
		}
		return mTRUVideoGameSKUVOList;
	}

	/**
	 * Sets the TRU video game skuvo list.
	 * 
	 * @param pTRUVideoGameSKUVOList
	 *            the tRUVideoGameSKUVOList to set
	 */
	public void setTRUVideoGameSKUVOList(List<TRUVideoGameSKUVO> pTRUVideoGameSKUVOList) {
		mTRUVideoGameSKUVOList = pTRUVideoGameSKUVOList;
	}

	/**
	 * Gets the TRU non merch skuvo list.
	 * 
	 * @return the tRUNonMerchSKUVOList
	 */
	public List<TRUNonMerchSKUVO> getTRUNonMerchSKUVOList() {
		if (mTRUNonMerchSKUVOList == null) {
			mTRUNonMerchSKUVOList = new ArrayList<TRUNonMerchSKUVO>();
		}
		return mTRUNonMerchSKUVOList;
	}

	/**
	 * Sets the TRU non merch skuvo list.
	 * 
	 * @param pTRUNonMerchSKUVOList
	 *            the tRUNonMerchSKUVOList to set
	 */
	public void setTRUNonMerchSKUVOList(List<TRUNonMerchSKUVO> pTRUNonMerchSKUVOList) {
		mTRUNonMerchSKUVOList = pTRUNonMerchSKUVOList;
	}

	/**
	 * Gets the TRU non merch skuvo map.
	 * 
	 * @return the tRUNonMerchSKUVOMap
	 */
	public Map<String, TRUNonMerchSKUVO> getTRUNonMerchSKUVOMap() {
		if (mTRUNonMerchSKUVOMap == null) {
			mTRUNonMerchSKUVOMap = new HashMap<String, TRUNonMerchSKUVO>();
		}
		return mTRUNonMerchSKUVOMap;
	}

	/**
	 * Sets the tru non merch skuvo map.
	 * 
	 * @param pTRUNonMerchSKUVOMap
	 *            the tRUNonMerchSKUVOMap to set
	 */
	public void setTRUNonMerchSKUVOMap(Map<String, TRUNonMerchSKUVO> pTRUNonMerchSKUVOMap) {
		mTRUNonMerchSKUVOMap = pTRUNonMerchSKUVOMap;
	}

	/** property to hold mTRUCategory. */
	private TRUCategory mTRUCategory;

	/**
	 * Gets the TRU category.
	 * 
	 * @return the TRUCategory
	 */
	public TRUCategory getTRUCategory() {
		return mTRUCategory;
	}

	/**
	 * Sets the TRU category.
	 * 
	 * @param pTRUCategory
	 *            the TRUCategory to set
	 */
	public void setTRUCategory(TRUCategory pTRUCategory) {
		mTRUCategory = pTRUCategory;
	}

}
