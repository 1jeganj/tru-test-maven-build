package com.tru.feedprocessor.tol.catalog.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;

/**
 * The Class CategoryItemValidator. This class validates the mandatory properties of Classifications.
 * @author Professional Access
 * @version 1.0
 * 
 * 
 */
public class TRURegistryClassificationItemValidator implements IFeedItemValidator {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;
	
	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Validate. It validates whether the required properties of the classification are there or not. It also checks whether
	 * the properties are in correct format w.r.t repository If the required property is not there it will throw the
	 * exception.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUClassificationItemValidator, @method: validate()");

		TRUClassification classification = (TRUClassification) pBaseFeedProcessVO;
		if (StringUtils.isBlank(classification.getRepositoryId())) {
			classification.setErrorMessage(TRUFeedConstants.ERROR_CLASSIFICATION_ID);
			throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_REGISTRY, classification, null);

		}
		if (StringUtils.isBlank(classification.getName())) {
			classification.setErrorMessage(TRUFeedConstants.ERROR_CLASSIFICATION_NAME);
			throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_REGISTRY, classification, null);
		}
		if (!isNumber(classification.getDisplayOrder())) {
			classification.setErrorMessage(TRUFeedConstants.ERROR_CLASSIFICATION_DO);
			throw new FeedSkippableException(null, null, TRUFeedConstants.INVALID_RECORD_REGISTRY, classification, null);

		}

		getLogger().vlogDebug("End @Class: TRUClassificationItemValidator, @method: validate()");

	}

	/**
	 * Checks if the given parameter is number or not.
	 * 
	 * @param pDisplayOrder
	 *            the display order
	 * @return true, if is number
	 */
	private boolean isNumber(String pDisplayOrder) {

		getLogger().vlogDebug("Start @Class: TRUClassificationItemValidator, @method: isNumber()");

		if (!StringUtils.isBlank(pDisplayOrder)) {
			try {
				Integer.parseInt(pDisplayOrder);
			} catch (NumberFormatException pNumberFormatException) {
				if (isLoggingError()) {
					logError(TRUFeedConstants.ERROR_MESSAGE, pNumberFormatException);
				}
				return false;
			}

		}

		getLogger().vlogDebug("End @Class: TRUClassificationItemValidator, @method: isNumber()");

		return true;
	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
