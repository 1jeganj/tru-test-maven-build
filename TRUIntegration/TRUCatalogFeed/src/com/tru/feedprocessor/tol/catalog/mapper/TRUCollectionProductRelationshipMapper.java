package com.tru.feedprocessor.tol.catalog.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;

/**
 * The Class TRUCollectionProductRelationshipMapper. This class maps the relationships data stored in entities to repository
 * to push the data of properties and it is used to write the VO properties to the ProductCatalog repository
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCollectionProductRelationshipMapper extends AbstractFeedItemMapper<TRUCollectionProductVO> {

	/** The m feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
	

	/**
	 * This Method map() will maps the relation among the Collection Product and sets the child products property with the
	 * respective values.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MutableRepositoryItem map(TRUCollectionProductVO pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Start @Class: TRUCollectionProductRelationshipMapper, @method: map()");
		
		String method = TRUProductFeedConstants.COLLECTION_PRD_REL_MAPPER_METHOD;
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.startOperation(method);
		}
		
		int count = (int)getCurrentFeedExecutionContext().getConfigValue(TRUProductFeedConstants.CPRMCOUNT);
		count++;
		getLogger().vlogDebug("collProdRelMappercount : "+count);
		getCurrentFeedExecutionContext().setConfigValue(TRUProductFeedConstants.CPRMCOUNT,count);
		
		Repository productCatalogRepository = (Repository) getRepository(FeedConstants.CATALOG_REPOSITORY);
		if (!pVOItem.getChildSKUS().isEmpty()) {
			RepositoryItem[] lCollectionProductChildSKUS = null;
			Set<RepositoryItem> collectionProductChildProductsSet = new HashSet<RepositoryItem>();
			//Getting all db sku ids
			List<RepositoryItem> dBChildSKUS = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getCollectionSkus());
			Set<String> skuIds = new HashSet<String>();
			for(RepositoryItem repo: dBChildSKUS){
				skuIds.add(repo.getRepositoryId());
			}
			//Adding vo skuids
			skuIds.addAll(pVOItem.getChildSKUS());
			//Getting sku repository item
			lCollectionProductChildSKUS = productCatalogRepository.getItems(
					skuIds.toArray(new String[skuIds.size()]),
					getFeedCatalogProperty().getSKUName());
			for(RepositoryItem repoItem : lCollectionProductChildSKUS){
				//Getting sku parent product
				Set<RepositoryItem> parentProduct = (Set<RepositoryItem>) repoItem.getPropertyValue(getFeedCatalogProperty().getParentProducts());
				if(parentProduct != null){
					//adding parent product to set
					collectionProductChildProductsSet.addAll(parentProduct);
				}
			}
			if (!collectionProductChildProductsSet.isEmpty()) {
				pRepoItem.setPropertyValue(getFeedCatalogProperty().getChildProducts(),
						new ArrayList<RepositoryItem>(collectionProductChildProductsSet));
			}
		}
		Set<RepositoryItem> parentCats = (Set<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentCategories());
		List<String> parentCatIDs = new ArrayList<String>();
		for (RepositoryItem repositoryItem : parentCats) {
			parentCatIDs.add(repositoryItem.getRepositoryId());
		}
				
		Set<String> rootParentCategories = pVOItem.getRootParentCategories();
		Set<RepositoryItem> rootParentCats = new HashSet<RepositoryItem>();
		for (String rootParentCategory : rootParentCategories) {
			if (!StringUtils.isBlank(rootParentCategory)) {
				RepositoryItem catItem = pRepoItem.getRepository().getItem(rootParentCategory, TRUProductFeedConstants.CATEGORY);
				if (catItem != null) {
					rootParentCats.add(catItem);
				}
			}
		}
		if (rootParentCats != null && !rootParentCats.isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentCategories(), rootParentCats);
		}
		
		if (PerformanceMonitor.isEnabled()) {
			PerformanceMonitor.endOperation(method);
		}
		
		if (!pVOItem.getParentClassifications().isEmpty()) {
			pRepoItem.setPropertyValue(getFeedCatalogProperty().getParentClassifications(),
					pVOItem.getParentClassifications());
		}
		
		getLogger().vlogDebug("End @Class: TRUCollectionProductRelationshipMapper, @method: map()");
		return pRepoItem;
	}

}
