package com.tru.feedprocessor.tol.catalog.processor;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;
import com.tru.feedprocessor.tol.catalog.vo.TRUBatteryQuantitynTypeVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;

/**
 * The Class TRUProductItemValueChangedInspector. This class inspects each and every property holds in entites to repository
 * of Product relations ships properties has any changes. If any change occurs in one property entire entity will be updated.
 * @version 1.0
 * @author Professional Access
 */
public class TRUProductItemValueChangedInspector implements IItemValueChangeInspector {

	/** property to hold m feed catalog property holder. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/** property to hold mLogger holder. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * This method is used to inspect the Product properties of entites with repository items for any changes. If any change
	 * occurs in one property entire entity will be updated.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pRepoItem
	 *            the repo item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pFeedVO, RepositoryItem pRepoItem) throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUProductItemValueChangedInspector, @method: isUpdated()");

		Boolean retVal = Boolean.FALSE;

		if (pFeedVO instanceof TRUProductFeedVO) {
			TRUProductFeedVO product = (TRUProductFeedVO) pFeedVO;
			
			if (!StringUtils.isBlank(product.getFeed())
					&& product.getFeed().equalsIgnoreCase(
							TRUProductFeedConstants.DELETE)
					&& !StringUtils.isBlank(product.getFeedActionCode())
					&& product.getFeedActionCode().equalsIgnoreCase(
							TRUProductFeedConstants.D)) {
				return Boolean.TRUE;
			}

			List<RepositoryItem> batteries = (List<RepositoryItem>) pRepoItem.getPropertyValue(getFeedCatalogProperty().getBatteries());

			if (!StringUtils.isBlank(product.getOnlineTitle()) &&
					 !product.getOnlineTitle().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getDisplayNameDefault()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getOnlineLongDesc()) &&
					 !product.getOnlineLongDesc().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getLongDescriptionName()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getBatteryIncluded()) &&
					 !product.getBatteryIncluded().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getBatteryIncluded()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getBatteryRequired()) &&
					 !product.getBatteryRequired().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getBatteryRequired()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getCost()) &&
					 !product.getCost().equals(pRepoItem.getPropertyValue(getFeedCatalogProperty().getCost()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getEwasteSurchargeSku()) &&
					 !product.getEwasteSurchargeSku().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getEwasteSurchargeSku()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getEwasteSurchargeState()) &&
					 !product.getEwasteSurchargeState().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getEwasteSurchargeState()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getGender()) && ((pRepoItem.getPropertyValue(getFeedCatalogProperty().getGender()) == null)||(!product.getGender().contains((String)pRepoItem.getPropertyValue(getFeedCatalogProperty().getGender()))))) {
					return retVal = Boolean.TRUE;
			}
			if (!checkDisplayOrderProperty(product.getRusItemNumber(),
					(Long) pRepoItem.getPropertyValue(getFeedCatalogProperty().getRusItemNumber()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getShipFromStoreEligible()) &&
					 !product.getShipFromStoreEligible().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getShipFromStoreEligible()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getSpecialAssemblyInstructions()) &&
					 !product.getSpecialAssemblyInstructions().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getSpecialAssemblyInstructions()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getTaxCode()) &&
					 !product.getTaxCode().equals(pRepoItem.getPropertyValue(getFeedCatalogProperty().getTaxCode()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getTaxProductValueCode()) &&
					 !product.getTaxProductValueCode().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getTaxProductValueCode()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getVendorsProductDemoUrl()) &&
					 !product.getVendorsProductDemoUrl().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getVendorsProductDemoUrl()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getManufacturerStyleNumber()) &&
					 !product.getManufacturerStyleNumber().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getManufacturerStyleNumber()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getNonMerchandiseFlag()) &&
					 !product.getNonMerchandiseFlag().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getNonMerchandiseFlag()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getCountryEligibility()) &&
					 !product.getCountryEligibility().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getCountryEligibility()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getVendorClassification()) &&
					 !product.getVendorClassification().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getVendorClassification()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getRegistryClassification()) &&
					 !product.getRegistryClassification().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getRegistryClassification()))) {
				return retVal = Boolean.TRUE;
			}
			if (!product.getParentClassifications().isEmpty() &&
					 !product.getParentClassifications().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getParentClassifications()))) {
				return retVal = Boolean.TRUE;
			}
			if (!StringUtils.isBlank(product.getSizeChartName()) &&
					 !product.getSizeChartName().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getSizeChartName()))) {
				return retVal = Boolean.TRUE;
			}
			if (!product.getStyleDimensions().isEmpty() &&
					 !product.getStyleDimensions().equals(
							pRepoItem.getPropertyValue(getFeedCatalogProperty().getStyleDimensions()))) {
				return retVal = Boolean.TRUE;
			}
			if (product.getBatteryQuantitynTypeList() != null && !product.getBatteryQuantitynTypeList().isEmpty()
					&& checkBatteryItemsForUpdate(product.getBatteryQuantitynTypeList(), batteries)) {
				return retVal = Boolean.TRUE;
			}
		}

		getLogger().vlogDebug("End @Class: TRUProductItemValueChangedInspector, @method: isUpdated()");

		return retVal;
	}
	
	/**
	 * Check battery items for update.
	 * 
	 * @param pVOItems
	 *            the VO items
	 * @param pRepoItems
	 *            the repo items
	 * @return true, if successful
	 */
	private boolean checkBatteryItemsForUpdate(List<TRUBatteryQuantitynTypeVO> pVOItems, List<RepositoryItem> pRepoItems) {

		getLogger().vlogDebug("Start @Class: TRUProductItemValueChangedInspector, @method: checkBatteryItemsForUpdate()");

		boolean isUpdate = Boolean.FALSE;
		if (pVOItems != null && !pVOItems.isEmpty() && pRepoItems != null && !pRepoItems.isEmpty()) {
			if (pVOItems.size() == pRepoItems.size()) {
				List<TRUBatteryQuantitynTypeVO> repositoryItems = new ArrayList<TRUBatteryQuantitynTypeVO>();
				for (RepositoryItem item : pRepoItems) {
					TRUBatteryQuantitynTypeVO batteryQuantitynTypeVO = new TRUBatteryQuantitynTypeVO();
					if(item.getPropertyValue(getFeedCatalogProperty().getBatteryType()) != null){
						batteryQuantitynTypeVO.setBatteryType((String) item.getPropertyValue(getFeedCatalogProperty()
								.getBatteryType()));
					}
					if(item.getPropertyValue(getFeedCatalogProperty().getBatteryQuantity()) != null){
						batteryQuantitynTypeVO.setBatteryQuantity(((Integer) item.getPropertyValue(getFeedCatalogProperty()
								.getBatteryQuantity())).toString());
					}
					repositoryItems.add(batteryQuantitynTypeVO);
				}
				int size = pVOItems.size();
				for (int i = TRUProductFeedConstants.NUMBER_ZERO; i < size; i++) {
					TRUBatteryQuantitynTypeVO entity = pVOItems.get(i);
					TRUBatteryQuantitynTypeVO repository = repositoryItems.get(i);
					if (entity != null && repository != null
							&& (entity.getBatteryType() != null && !entity.getBatteryType().equalsIgnoreCase(repository.getBatteryType()))
							|| (entity.getBatteryQuantity() != null && !entity.getBatteryQuantity().equalsIgnoreCase(repository.getBatteryQuantity()))) {
						isUpdate = Boolean.TRUE;
						break;
					} else {
						isUpdate = Boolean.FALSE;
					}
				}

			} else {
				isUpdate = Boolean.TRUE;
			}
		} else if(pVOItems != null && pRepoItems != null && !pVOItems.isEmpty() && pRepoItems.isEmpty()){
			isUpdate = Boolean.TRUE;
		}
		getLogger().vlogDebug("End @Class: TRUProductItemValueChangedInspector, @method: checkBatteryItemsForUpdate()");

		return isUpdate;

	}

	/**
	 * Check display order property.
	 * 
	 * @param pVoProperty
	 *            the vo property
	 * @param pItemProperty
	 *            the item property
	 * @return true, if successful
	 */
	private boolean checkDisplayOrderProperty(String pVoProperty, Long pItemProperty) {
		if (pVoProperty == null && pItemProperty == null) {
			return true;
		} else if ((pVoProperty != null && pItemProperty != null && Long.parseLong(pVoProperty) != pItemProperty) ||
				 (pVoProperty != null && pItemProperty == null)) {
			return false;
		}

		return true;
	}

	/**
	 * Gets the feed catalog property.
	 * 
	 * @return the mFeedCatalogProperty
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 * 
	 * @param pFeedCatalogProperty
	 *            - pFeedCatalogProperty to set
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}

}
