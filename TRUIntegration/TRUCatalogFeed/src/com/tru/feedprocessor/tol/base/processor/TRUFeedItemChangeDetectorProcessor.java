package com.tru.feedprocessor.tol.base.processor;

import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;

/**
 * The Class TRUFeedItemChangeDetectorProcessor.
 * @author PA.
 * @version 1.0.
 */
public class TRUFeedItemChangeDetectorProcessor extends FeedItemChangeDetectorProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.processor.FeedItemChangeDetectorProcessor#doProcess(com.tru.feedprocessor.tol
	 * .base.vo.BaseFeedProcessVO)
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException {
		if (pFeedVO != null) {

			getLogger().vlogDebug("Id : {0}" , pFeedVO.getRepositoryId());

			Map<String, Map<String, String>> lEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
			String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());

			if (lEntityIdMap != null && lEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()) == null && 
					pFeedVO instanceof TRUProductFeedVO && !(pFeedVO instanceof TRUCollectionProductVO) && 
					((TRUProductFeedVO) pFeedVO).getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && 
					!StringUtils.isBlank(((TRUProductFeedVO) pFeedVO).getFeedActionCode()) && 
					(((TRUProductFeedVO) pFeedVO).getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.C) ||
					((TRUProductFeedVO) pFeedVO).getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.D)) ) {
				((TRUProductFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.PCPIF);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID, pFeedVO,
						null);
			} else if (lEntityIdMap != null && lEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()) == null && 
					pFeedVO instanceof TRUSkuFeedVO && 
					((TRUSkuFeedVO) pFeedVO).getFeed().equalsIgnoreCase(TRUProductFeedConstants.DELTA) && 
					!StringUtils.isBlank(((TRUSkuFeedVO) pFeedVO).getFeedActionCode()) && 
					(((TRUSkuFeedVO) pFeedVO).getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.C) ||
					((TRUSkuFeedVO) pFeedVO).getFeedActionCode().equalsIgnoreCase(TRUProductFeedConstants.D)) ) {
				((TRUSkuFeedVO) pFeedVO).setErrorMessage(TRUProductFeedConstants.PCSIF);
				throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID, pFeedVO,
						null);
			} else {
				return super.doProcess(pFeedVO);
			}
		} else {
			throw new FeedSkippableException(null, null, TRUProductFeedConstants.INVALID_RECORD_PRODUCT_ID, pFeedVO, null);
		}
	}

}
