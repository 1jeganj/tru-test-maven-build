package com.tru.feedprocessor.tol.base.constants;

/**
 * The Class TRUProductFeedConstants. This class is specific to hold all Product Feed related constants.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUProductFeedConstants {
	// product feed constants
	/** The Constant PRODUCTS. */
	public static final String PRODUCTS = "Products";

	/** The Constant PRODUCT. */
	public static final String PRODUCT = "product";

	/** The Constant ID. */
	public static final String ID = "id";

	/** The Constant ONLINE_TITLE. */
	public static final String ONLINE_TITLE = "online-title";

	/** The Constant ONLINE_LONG_DESC. */
	public static final String ONLINE_LONG_DESC = "online-long-desc";

	/** The Constant SKU. */
	public static final String SKU = "sku";

	/** The Constant BATTERY_INCLUDED. */
	public static final String BATTERY_INCLUDED = "battery-included";

	/** The Constant BATTERY_REQUIRED. */
	public static final String BATTERY_REQUIRED = "battery-required";

	/** The Constant BRAND_NAME_PRIMARY. */
	public static final String BRAND_NAME_PRIMARY = "brand-name-primary";

	/** The Constant GENDER. */
	public static final String GENDER = "gender";

	/** The Constant RUS_ITEM_NUMBER. */
	public static final String RUS_ITEM_NUMBER = "rus-item-number";

	/** The Constant SHIP_FROM_STORE_ELIGIBLE. */
	public static final String SHIP_FROM_STORE_ELIGIBLE = "ship-from-store-eligible";

	/** The Constant TAX_CODE. */
	public static final String TAX_CODE = "tax-code";

	/** The Constant MANUFACTURER_STYLE_NUMBER. */
	public static final String MANUFACTURER_STYLE_NUMBER = "manufacturer-style-number";

	/** The Constant BUBBLE_CONTROL. */
	public static final String BUBBLE_CONTROL = "bubble-control";

	/** The Constant SITE_ELIGIBILITY. */
	public static final String SITE_ELIGIBILITY = "site-eligibility";

	/** The Constant CHANNEL_AVAILABILITY. */
	public static final String CHANNEL_AVAILABILITY = "channel-availability";

	/** The Constant EWASTE_SURCHARGE_SKU. */
	public static final String EWASTE_SURCHARGE_SKU = "ewaste-surcharge-sku";

	/** The Constant EWASTE_SURCHARGE_STATE. */
	public static final String EWASTE_SURCHARGE_STATE = "ewaste-surcharge-state";

	/** The Constant ITEM_IN_STORE_PICK_UP. */
	public static final String ITEM_IN_STORE_PICK_UP = "item-in-store-pick-up";

	/** The Constant SPECIAL_ASSEMBLY_INSTRUCTIONS. */
	public static final String SPECIAL_ASSEMBLY_INSTRUCTIONS = "special-assembly-instructions";

	/** The Constant TAX_PRODUCT_VALUE_CODE. */
	public static final String TAX_PRODUCT_VALUE_CODE = "tax-product-value-code";

	/** The Constant VENDORS_PRODUCT_DEMO_URL. */
	public static final String VENDORS_PRODUCT_DEMO_URL = "vendors-product-demo-url";

	/** The Constant NON_MERCHANDISE_FLAG. */
	public static final String NON_MERCHANDISE_FLAG = "non-merchandise-flag";

	/** The Constant COUNTRY_ELIGIBILITY. */
	public static final String COUNTRY_ELIGIBILITY = "country-eligibility";

	/** The Constant CLASSIFICATION_REFERENCE. */
	public static final String CLASSIFICATION_REFERENCE = "classification-reference";
	
	/** The Constant REFERENCE. */
	public static final String REFERENCE = "reference";

	/** The Constant TYPE. */
	public static final String TYPE = "type";

	/** The Constant US_Registry_Cat. */
	public static final String US_REGISTRY_CAT = "US_Registry_Cat";

	/** The Constant Vendor. */
	public static final String VENDOR = "Vendor";

	/** The Constant Classification_Id. */
	public static final String CLASSIFICATION_ID = "classification-id";

	/** The Constant UserTypeID. */
	public static final String USERTYPEID = "userTypeID";

	/** The Constant UID. */
	public static final String UID = "UID";

	/** The Constant COST. */
	public static final String COST = "cost";

	/** The Constant PRODUCT_CROSS_REFERENCE. */
	public static final String PRODUCT_CROSS_REFERENCE = "product-cross-reference";

	/** The Constant CROSSSELL. */
	public static final String CROSSSELL = "CrossSell";

	/** The Constant BATTERIES. */
	public static final String BATTERIES = "Batteries";

	/** The Constant PRODUCT_ID. */
	public static final String PRODUCT_ID = "product-id";

	/** The Constant VALUE. */
	public static final String VALUE = "value";

	/** The Constant METADATA. */
	public static final String METADATA = "metadata";

	/** The Constant ATTRIBUTE_ID. */
	public static final String ATTRIBUTE_ID = "attribute-id";

	/** The Constant CROSS_SELLS_BATTERIES_ID. */
	public static final String CROSS_SELLS_BATTERIES_ID = "7183";

	/** The Constant BATTERY_QUANTITY_ID. */
	public static final String BATTERY_QUANTITY_ID = "100139";

	// sku feed constants

	/** The Constant INTEREST. */
	public static final String INTEREST = "interest";

	// akhiMap
	/** The Constant AK_HI_2_DAY_F_S_DOLLAR. */
	public static final String AK_HI_2_DAY_F_S_DOLLAR = "ak-hi-2-day-f-s-dollar";

	/** The Constant AKHI2DAYFSDOLLAR. */
	public static final String AKHI2DAYFSDOLLAR = "akHi2DayFSDollar";

	/** The Constant AK_HI_FIXED_SURCHARGE. */
	public static final String AK_HI_FIXED_SURCHARGE = "ak-hi-fixed-surcharge";

	/** The Constant AKHIFIXEDSURCHARGE. */
	public static final String AKHIFIXEDSURCHARGE = "akHiFixedSurcharge";

	/** The Constant AK_HI_OVERNIGHT_F_S_DOLLAR. */
	public static final String AK_HI_OVERNIGHT_F_S_DOLLAR = "ak-hi-overnight-f-s-dollar";

	/** The Constant AKHIOVERNIGHTFSDOLLAR. */
	public static final String AKHIOVERNIGHTFSDOLLAR = "akHiOvernightFSDollar";

	/** The Constant AK_HI_STD_F_S_DOLLAR. */
	public static final String AK_HI_STD_F_S_DOLLAR = "ak-hi-std-f-s-dollar";

	/** The Constant AKHISTDFSDOLLARDOLLAR. */
	public static final String AKHISTDFSDOLLARDOLLAR = "akHiStdFSDollarDollar";

	// assemblyMap
	/** The Constant ASSEMBLED_DEPTH. */
	public static final String ASSEMBLED_DEPTH = "assembled-depth";

	/** The Constant ASSEMBLEDDEPTH. */
	public static final String ASSEMBLEDDEPTH = "assembledDepth";

	/** The Constant ASSEMBLED_WIDTH. */
	public static final String ASSEMBLED_WIDTH = "assembled-width";

	/** The Constant ASSEMBLEDWIDTH. */
	public static final String ASSEMBLEDWIDTH = "assembledWidth";

	/** The Constant ASSEMBLY_REQUIRED. */
	public static final String ASSEMBLY_REQUIRED = "assembly-required";

	/** The Constant ASSEMBLYREQUIRED. */
	public static final String ASSEMBLYREQUIRED = "assemblyRequired";

	/** The Constant ASSEMBLED_HEIGHT. */
	public static final String ASSEMBLED_HEIGHT = "assembled-height";

	/** The Constant ASSEMBLEDHEIGHT. */
	public static final String ASSEMBLEDHEIGHT = "assembledHeight";

	/** The Constant ASSEMBLED_WEIGHT. */
	public static final String ASSEMBLED_WEIGHT = "assembled-weight";

	/** The Constant ASSEMBLEDWEIGHT. */
	public static final String ASSEMBLEDWEIGHT = "assembledWeight";

	/** The Constant BACKORDER_STATUS. */
	public static final String BACKORDER_STATUS = "backorder-status";

	/** The Constant BRAND_NAME_SECONDARY. */
	public static final String BRAND_NAME_SECONDARY = "brand-name-secondary";

	/** The Constant BRANDNAMETERTIARY_CHARACTER_THEME. */
	public static final String BRANDNAMETERTIARY_CHARACTER_THEME = "brandnametertiary-character-theme";

	/** The Constant BROWSE_HIDDEN_KEYWORD. */
	public static final String BROWSE_HIDDEN_KEYWORD = "browse-hidden-keyword";

	/** The Constant CAN_BE_GIFT_WRAPPED. */
	public static final String CAN_BE_GIFT_WRAPPED = "can-be-gift-wrapped";

	/** The Constant CAR_SEAT_FEATURES. */
	public static final String CAR_SEAT_FEATURES = "car-seat-features";

	/** The Constant CAR_SEAT_TYPE. */
	public static final String CAR_SEAT_TYPE = "car-seat-type";

	/** The Constant CAR_SEAT_WHAT_IS_IMP. */
	public static final String CAR_SEAT_WHAT_IS_IMP = "car-seat-what-is-imp";

	/** The Constant CHILD_WEIGHT_MAX. */
	public static final String CHILD_WEIGHT_MAX = "child-weight-max";

	/** The Constant CHILD_WEIGHT_MIN. */
	public static final String CHILD_WEIGHT_MIN = "child-weight-min";

	/** The Constant COLOR_CODE. */
	public static final String COLOR_CODE = "color-code";

	/** The Constant COLOR_UPC_LEVEL. */
	public static final String COLOR_UPC_LEVEL = "color-upc-level";

	/** The Constant CRIB_MATERIAL_TYPE. */
	public static final String CRIB_MATERIAL_TYPE = "crib-material-type";

	/** The Constant CRIB_STYLE. */
	public static final String CRIB_STYLE = "crib-style";

	/** The Constant CRIB_TYPE. */
	public static final String CRIB_TYPE = "crib-type";

	/** The Constant CRIB_WHAT_IS_IMP. */
	public static final String CRIB_WHAT_IS_IMP = "crib-what-is-imp";

	/** The Constant CUSTOMER_PURCHASE_LIMIT. */
	public static final String CUSTOMER_PURCHASE_LIMIT = "customer-purchase-limit";

	/** The Constant DROPSHIP_FLAG. */
	public static final String DROPSHIP_FLAG = "dropship-flag";

	/** The Constant FLEXIBLE_SHIPPING_PLAN. */
	public static final String FLEXIBLE_SHIPPING_PLAN = "flexible-shipping-plan";

	/** The Constant FREIGHT_CLASS. */
	public static final String FREIGHT_CLASS = "freight-class";

	/** The Constant FURNITURE_FINISH. */
	public static final String FURNITURE_FINISH = "furniture-finish";

	/** The Constant INCREMENTAL_SAFETY_STOCK_UNITS. */
	public static final String INCREMENTAL_SAFETY_STOCK_UNITS = "incremental-safety-stock-units";

	/** The Constant JUVENILE_PRODUCT_SIZE. */
	public static final String JUVENILE_PRODUCT_SIZE = "juvenile-product-size";

	/** The Constant MAX_TARGET_AGE_TRU_WEBSITES. */
	public static final String MAX_TARGET_AGE_TRU_WEBSITES = "max-target-age-tru-websites";

	/** The Constant MIN_TARGET_AGE_TRU_WEBSITES. */
	public static final String MIN_TARGET_AGE_TRU_WEBSITES = "min-target-age-tru-websites";

	/** The Constant MFR_SUGGESTED_AGE_MAX. */
	public static final String MFR_SUGGESTED_AGE_MAX = "mfr-suggested-age-max";

	/** The Constant MFR_SUGGESTED_AGE_MIN. */
	public static final String MFR_SUGGESTED_AGE_MIN = "mfr-suggested-age-min";

	/** The Constant NMWA_PERCENTAGE. */
	public static final String NMWA_PERCENTAGE = "nmwa-percentage";

	/** The Constant SHIP_WINDOW_MIN. */
	public static final String SHIP_WINDOW_MIN = "ship-window-min";

	/** The Constant SHIP_WINDOW_MAX. */
	public static final String SHIP_WINDOW_MAX = "ship-window-max";

	/** The Constant ONLINE_PID. */
	public static final String ONLINE_PID = "online-pid";

	/** The Constant ONLINE_PRODUCT_STREET_DATE. */
	public static final String ONLINE_PRODUCT_STREET_DATE = "online-product-street-date";

	/** The Constant OTHER_FIXED_SURCHARGE. */
	public static final String OTHER_FIXED_SURCHARGE = "other-fixed-surcharge";

	/** The Constant OTHER_STD_F_S_DOLLAR. */
	public static final String OTHER_STD_F_S_DOLLAR = "other-std-f-s-dollar";

	/** The Constant OUTLET. */
	public static final String OUTLET = "outlet";

	/** The Constant PRODUCT_AWARDS. */
	public static final String PRODUCT_AWARDS = "product-awards";

	/** The Constant PRESELL_QUANTITY_UNITS. */
	public static final String PRESELL_QUANTITY_UNITS = "presell-quantity-units";

	/** The Constant PRICE_DISPLAY. */
	public static final String PRICE_DISPLAY = "price-display";

	/** The Constant PRODUCT_SAFETY_WARNING. */
	public static final String PRODUCT_SAFETY_WARNING = "product-safety-warning";

	/** The Constant PROMOTIONAL_STICKER_DISPLAY. */
	public static final String PROMOTIONAL_STICKER_DISPLAY = "promotional-sticker-display";

	/** The Constant REGISTRY_WARNING_INDICATOR. */
	public static final String REGISTRY_WARNING_INDICATOR = "registry-warning-indicator";

	/** The Constant REGISTRY_WISH_LIST_ELIGIBILITY. */
	public static final String REGISTRY_WISH_LIST_ELIGIBILITY = "registry-wish-list-eligibility";

	/** The Constant SAFETY_STOCK. */
	public static final String SAFETY_STOCK = "safety-stock";

	/** The Constant SHIP_IN_ORIG_CONTAINER. */
	public static final String SHIP_IN_ORIG_CONTAINER = "ship-in-orig-container";

	/** The Constant STRL_BEST_USES. */
	public static final String STRL_BEST_USES = "strl-best-uses";

	/** The Constant STRL_EXTRAS. */
	public static final String STRL_EXTRAS = "strl-extras";

	/** The Constant STRL_TYPE. */
	public static final String STRL_TYPE = "strl-type";

	/** The Constant STRL_WHAT_IS_IMP. */
	public static final String STRL_WHAT_IS_IMP = "strl-what-is-imp";

	/** The Constant SUPRESS_IN_SEARCH. */
	public static final String SUPRESS_IN_SEARCH = "supress-in-search";

	/** The Constant UPC_NUMBER. */
	public static final String UPC_NUMBER = "upc-number";

	/** The Constant SYSTEM_REQUIREMENTS. */
	public static final String SYSTEM_REQUIREMENTS = "system-requirements";

	/** The Constant VIDEO_GAME_ESRB. */
	public static final String VIDEO_GAME_ESRB = "video-game-esrb";

	/** The Constant VIDEO_GAME_GENRE. */
	public static final String VIDEO_GAME_GENRE = "video-game-genre";

	/** The Constant VIDEO_GAME_PLATFORM. */
	public static final String VIDEO_GAME_PLATFORM = "video-game-platform";

	/** The Constant NMWA. */
	public static final String NMWA = "nmwa";

	/** The Constant WEB_DISPLAY_FLAG. */
	public static final String WEB_DISPLAY_FLAG = "web-display-flag";

	/** The Constant LEGAL_NOTICE. */
	public static final String LEGAL_NOTICE = "legal-notice";

	/** The Constant SLAPPER_ITEM. */
	public static final String SLAPPER_ITEM = "slapper-item";

	/** The Constant SHIP_FROM_STORE_ELIGIBLE_LOV. */
	public static final String SHIP_FROM_STORE_ELIGIBLE_LOV = "ship-from-store-eligible-lov";

	// cribPropertiesMap
	/** The Constant CRIB_BEDRAILS. */
	public static final String CRIB_BEDRAILS = "crib-bedrails";

	/** The Constant CRIBBEDRAILS. */
	public static final String CRIBBEDRAILS = "cribBedrails";

	/** The Constant CRIB_CONVERSION_KIT_INCLUDED. */
	public static final String CRIB_CONVERSION_KIT_INCLUDED = "crib-conversion-kit-included";

	/** The Constant CRIBCONVERSIONKITINCLUDED. */
	public static final String CRIBCONVERSIONKITINCLUDED = "cribConversionKitIncluded";

	/** The Constant CRIB_HARDWARE_VISIBLE. */
	public static final String CRIB_HARDWARE_VISIBLE = "crib-hardware-visible";

	/** The Constant CRIBHARDWAREVISIBLE. */
	public static final String CRIBHARDWAREVISIBLE = "cribHardwareVisible";

	/** The Constant CRIB_STORAGE. */
	public static final String CRIB_STORAGE = "crib-storage";

	/** The Constant CRIBSTORAGE. */
	public static final String CRIBSTORAGE = "cribStorage";

	/** The Constant CRIB_DESIGN. */
	public static final String CRIB_DESIGN = "crib-design";

	/** The Constant CRIBDESIGN. */
	public static final String CRIBDESIGN = "cribDesign";

	/** The Constant SAFETY_TETHER. */
	public static final String SAFETY_TETHER = "safety-tether";

	/** The Constant SAFETYTETHER. */
	public static final String SAFETYTETHER = "safetyTether";

	// strollerPropertiesMap
	/** The Constant STRL_CAR_SEAT_COMPATIBLE. */
	public static final String STRL_CAR_SEAT_COMPATIBLE = "strl-car-seat-compatible";

	/** The Constant STRLCARSEATCOMPATIBLE. */
	public static final String STRLCARSEATCOMPATIBLE = "strlCarSeatCompatible";

	/** The Constant STRL_CHILDREN_CAPACITY. */
	public static final String STRL_CHILDREN_CAPACITY = "strl-children-capacity";

	/** The Constant STRLCHILDRENCAPACITY. */
	public static final String STRLCHILDRENCAPACITY = "strlChildrenCapacity";

	/** The Constant STRL_PATTERN. */
	public static final String STRL_PATTERN = "strl-pattern";

	/** The Constant STRLPATTERN. */
	public static final String STRLPATTERN = "strlPattern";

	/** The Constant ALL_TERRAIN. */
	public static final String ALL_TERRAIN = "all-terrain";

	/** The Constant ALLTERRAIN. */
	public static final String ALLTERRAIN = "allTerrain";

	/** The Constant STORAGE_COMPARTMENT. */
	public static final String STORAGE_COMPARTMENT = "storage-compartment";

	/** The Constant STORAGECOMPARTMENT. */
	public static final String STORAGECOMPARTMENT = "storageCompartment";

	/** The Constant PARENT_CUP_HOLDER. */
	public static final String PARENT_CUP_HOLDER = "parent-cup-holder";

	/** The Constant PARENTCUPHOLDER. */
	public static final String PARENTCUPHOLDER = "parentCupHolder";

	/** The Constant AIR_FILL_TIRES. */
	public static final String AIR_FILL_TIRES = "air-fill-tires";

	/** The Constant AIRFILLTIRES. */
	public static final String AIRFILLTIRES = "airFillTires";

	/** The Constant SUSPENSION. */
	public static final String SUSPENSION = "suspension";

	/** The Constant COMPACT_FOLD. */
	public static final String COMPACT_FOLD = "compact-fold";

	/** The Constant COMPACTFOLD. */
	public static final String COMPACTFOLD = "compactFold";

	/** The Constant STROLLER_WEIGHT. */
	public static final String STROLLER_WEIGHT = "stroller-weight";

	/** The Constant STROLLERWEIGHT. */
	public static final String STROLLERWEIGHT = "strollerWeight";

	/** The Constant LOCKING_WHEELS. */
	public static final String LOCKING_WHEELS = "locking-wheels";

	/** The Constant LOCKINGWHEELS. */
	public static final String LOCKINGWHEELS = "lockingWheels";

	/** The Constant ADJUSTABLE_HARNESS. */
	public static final String ADJUSTABLE_HARNESS = "adjustable-harness";

	/** The Constant ADJUSTABLEHARNESS. */
	public static final String ADJUSTABLEHARNESS = "adjustableHarness";

	/** The Constant STORAGE_BASKET. */
	public static final String STORAGE_BASKET = "storage-basket";

	/** The Constant STORAGEBASKET. */
	public static final String STORAGEBASKET = "storageBasket";

	/** The Constant MATCHING_CAR_SEAT. */
	public static final String MATCHING_CAR_SEAT = "matching-car-seat";

	/** The Constant MATCHINGCARSEAT. */
	public static final String MATCHINGCARSEAT = "matchingCarSeat";

	// carSeatPropertiesMap
	/** The Constant CAR_SEAT_EASE_OF_USE_RATING. */
	public static final String CAR_SEAT_EASE_OF_USE_RATING = "car-seat-ease-of-use-rating";

	/** The Constant CARSEATEASEOFUSERATING. */
	public static final String CARSEATEASEOFUSERATING = "carSeatEaseOfUseRating";

	/** The Constant CAR_SEAT_PATTERN. */
	public static final String CAR_SEAT_PATTERN = "car-seat-pattern";

	/** The Constant CARSEATPATTERN. */
	public static final String CARSEATPATTERN = "carSeatPattern";

	/** The Constant CAR_SEAT_SIDE_IMP_PROTECT. */
	public static final String CAR_SEAT_SIDE_IMP_PROTECT = "car-seat-side-imp-protect";

	/** The Constant CARSEATSIDEIMPPROTECT. */
	public static final String CARSEATSIDEIMPPROTECT = "carSeatSideImpProtect";

	/** The Constant FIVE_POINT_HARNESS. */
	public static final String FIVE_POINT_HARNESS = "five-point-harness";

	/** The Constant FIVEPOINTHARNESS. */
	public static final String FIVEPOINTHARNESS = "fivePointHarness";

	/** The Constant CAR_SEAT_COLOR_THEME. */
	public static final String CAR_SEAT_COLOR_THEME = "car-seat-color-theme";

	/** The Constant CARSEATCOLORTHEME. */
	public static final String CARSEATCOLORTHEME = "carSeatColorTheme";

	/** The Constant FINDER_ELIGIBLE. */
	public static final String FINDER_ELIGIBLE = "finder-eligible";

	/** The Constant FINDERELIGIBLE. */
	public static final String FINDERELIGIBLE = "finderEligible";

	/** The Constant FULL_COVERAGE_CANOPY. */
	public static final String FULL_COVERAGE_CANOPY = "full-coverage-canopy";

	/** The Constant FULLCOVERAGECANOPY. */
	public static final String FULLCOVERAGECANOPY = "fullCoverageCanopy";

	/** The Constant LATCH_INSTALLATION. */
	public static final String LATCH_INSTALLATION = "latch-installation";

	/** The Constant LATCHINSTALLATION. */
	public static final String LATCHINSTALLATION = "latchInstallation";

	/** The Constant SIDE_IMPACT_PROTECTION. */
	public static final String SIDE_IMPACT_PROTECTION = "side-impact-protection";

	/** The Constant SIDEIMPACTPROTECTION. */
	public static final String SIDEIMPACTPROTECTION = "sideImpactProtection";

	/** The Constant FULL_RECLINING_SEAT. */
	public static final String FULL_RECLINING_SEAT = "full-reclining-seat";

	/** The Constant FULLRECLININGSEAT. */
	public static final String FULLRECLININGSEAT = "fullRecliningSeat";

	/** The Constant CHILD_GRAB_BAR. */
	public static final String CHILD_GRAB_BAR = "child-grab-bar";

	/** The Constant CHILDGRABBAR. */
	public static final String CHILDGRABBAR = "childGrabBar";

	/** The Constant ADJUSTABLE_HEADREST. */
	public static final String ADJUSTABLE_HEADREST = "adjustable-headrest";

	/** The Constant ADJUSTABLEHEADREST. */
	public static final String ADJUSTABLEHEADREST = "adjustableHeadrest";

	/** The Constant CAR_SEAT_WEIGHT_WITHOUT_BASE. */
	public static final String CAR_SEAT_WEIGHT_WITHOUT_BASE = "car-seat-weight-without-base";

	/** The Constant CARSEATWEIGHTWITHOUTBASE. */
	public static final String CARSEATWEIGHTWITHOUTBASE = "carSeatWeightWithoutBase";

	/** The Constant FULL_CANOPY. */
	public static final String FULL_CANOPY = "full-canopy";

	/** The Constant FULLCANOPY. */
	public static final String FULLCANOPY = "fullCanopy";

	/** The Constant RECLINE_INDICATOR. */
	public static final String RECLINE_INDICATOR = "recline-indicator";

	/** The Constant RECLINEINDICATOR. */
	public static final String RECLINEINDICATOR = "reclineIndicator";

	/** The Constant MACHINE_WASHABLE. */
	public static final String MACHINE_WASHABLE = "machine-washable";

	/** The Constant MACHINEWASHABLE. */
	public static final String MACHINEWASHABLE = "machineWashable";

	/** The Constant CONVERTS_FROM_5PT_HARNESS_TO_BELT_POSITIONING_BOOSTER. */
	public static final String CONVERTS_FROM_5PT_HARNESS_TO_BELT_POSITIONING_BOOSTER = "converts-from-5pt-harness-to-belt-positioning-booster";

	/** The Constant CONVERTSFROM5PTHARNESSTOBELTPOSITIONINGBOOSTER. */
	public static final String CONVERTSFROM5PTHARNESSTOBELTPOSITIONINGBOOSTER = "convertsFrom5ptHarnessToBeltPositioningBooster";

	/** The Constant HIGH_PERFORMANCE_FABRIC. */
	public static final String HIGH_PERFORMANCE_FABRIC = "high-performance-fabric";

	/** The Constant HIGHPERFORMANCEFABRIC. */
	public static final String HIGHPERFORMANCEFABRIC = "highPerformanceFabric";

	/** The Constant MODULARITY. */
	public static final String MODULARITY = "modularity";

	/** The Constant CONVERTS_FROM_CONVERTIBLE_TO_BOOSTER. */
	public static final String CONVERTS_FROM_CONVERTIBLE_TO_BOOSTER = "converts-from-convertible-to-booster";

	/** The Constant CONVERTSFROMCONVERTIBLETOBOOSTER. */
	public static final String CONVERTSFROMCONVERTIBLETOBOOSTER = "convertsFromConvertibleToBooster";

	/** The Constant ADDITIONAL_HEAD_PROTECTION. */
	public static final String ADDITIONAL_HEAD_PROTECTION = "additional-head-protection";

	/** The Constant ADDITIONALHEADPROTECTION. */
	public static final String ADDITIONALHEADPROTECTION = "additionalHeadProtection";

	/** The Constant STEEL_REINFORCED_FRAME. */
	public static final String STEEL_REINFORCED_FRAME = "steel-reinforced-frame";

	/** The Constant STEELREINFORCEDFRAME. */
	public static final String STEELREINFORCEDFRAME = "steelReinforcedFrame";

	/** The Constant AIR_VENT. */
	public static final String AIR_VENT = "air-vent";

	/** The Constant AIRVENT. */
	public static final String AIRVENT = "airVent";

	/** The Constant COMPATIBLE_STROLLER. */
	public static final String COMPATIBLE_STROLLER = "compatible-stroller";

	/** The Constant COMPATIBLESTROLLER. */
	public static final String COMPATIBLESTROLLER = "compatibleStroller";

	/** The Constant LIGHT_WEIGHT. */
	public static final String LIGHT_WEIGHT = "light-weight";

	/** The Constant LIGHTWEIGHT. */
	public static final String LIGHTWEIGHT = "lightWeight";

	/** The Constant ADJUSTABLE_HANDLEBAR. */
	public static final String ADJUSTABLE_HANDLEBAR = "adjustable-handlebar";

	/** The Constant ADJUSTABLEHANDLEBAR. */
	public static final String ADJUSTABLEHANDLEBAR = "adjustableHandlebar";

	/** The Constant SPECIAL_FEATURES. */
	public static final String SPECIAL_FEATURES = "special-features";

	/** The Constant SPECIALFEATURES. */
	public static final String SPECIALFEATURES = "specialFeatures";

	/** The Constant CUP_OR_BOTTLE_HOLDER. */
	public static final String CUP_OR_BOTTLE_HOLDER = "cup-or-bottle-holder";

	/** The Constant CUPORBOTTLEHOLDER. */
	public static final String CUPORBOTTLEHOLDER = "cupOrBottleHolder";

	// productFeature
	/** The Constant PRODUCT_FEATURE1. */
	public static final String PRODUCT_FEATURE1 = "product-feature1";

	/** The Constant PRODUCTFEATURE1. */
	public static final String PRODUCTFEATURE1 = "productFeature1";

	/** The Constant PRODUCT_FEATURE2. */
	public static final String PRODUCT_FEATURE2 = "product-feature2";

	/** The Constant PRODUCTFEATURE2. */
	public static final String PRODUCTFEATURE2 = "productFeature2";

	/** The Constant PRODUCT_FEATURE3. */
	public static final String PRODUCT_FEATURE3 = "product-feature3";

	/** The Constant PRODUCTFEATURE3. */
	public static final String PRODUCTFEATURE3 = "productFeature3";

	/** The Constant PRODUCT_FEATURE4. */
	public static final String PRODUCT_FEATURE4 = "product-feature4";

	/** The Constant PRODUCTFEATURE4. */
	public static final String PRODUCTFEATURE4 = "productFeature4";

	/** The Constant PRODUCT_FEATURE5. */
	public static final String PRODUCT_FEATURE5 = "product-feature5";

	/** The Constant PRODUCTFEATURE5. */
	public static final String PRODUCTFEATURE5 = "productFeature5";

	// lower48Map
	/** The Constant LOWER48_2_DAY_F_S_DOLLAR. */
	public static final String LOWER48_2_DAY_F_S_DOLLAR = "lower48-2-day-f-s-dollar";

	/** The Constant LOWER482DAYFSDOLLAR. */
	public static final String LOWER482DAYFSDOLLAR = "lower482DayFSDollar";

	/** The Constant LOWER48_FIXED_SURCHARGE. */
	public static final String LOWER48_FIXED_SURCHARGE = "lower48-fixed-surcharge";

	/** The Constant LOWER48FIXEDSURCHARGE. */
	public static final String LOWER48FIXEDSURCHARGE = "lower48FixedSurcharge";

	/** The Constant LOWER48_OVERNIGHT_F_S_DOLLAR. */
	public static final String LOWER48_OVERNIGHT_F_S_DOLLAR = "lower48-overnight-f-s-dollar";

	/** The Constant LOWER48OVERNIGHTFSDOLLAR. */
	public static final String LOWER48OVERNIGHTFSDOLLAR = "lower48OvernightFSDollar";

	/** The Constant LOWER48_STANDARD_F_S_DOLLAR. */
	public static final String LOWER48_STANDARD_F_S_DOLLAR = "lower48-standard-f-s-dollar";

	/** The Constant LOWER48STANDARDFSDOLLAR. */
	public static final String LOWER48STANDARDFSDOLLAR = "lower48StandardFSDollar";

	// itemDimensionMap
	/** The Constant ITEM_DEPTH. */
	public static final String ITEM_DEPTH = "item-depth";

	/** The Constant ITEMDEPTH. */
	public static final String ITEMDEPTH = "itemDepth";

	/** The Constant ITEM_HEIGHT. */
	public static final String ITEM_HEIGHT = "item-height";

	/** The Constant ITEMHEIGHT. */
	public static final String ITEMHEIGHT = "itemHeight";

	/** The Constant ITEM_WEIGHT. */
	public static final String ITEM_WEIGHT = "item-weight";

	/** The Constant ITEMWEIGHT. */
	public static final String ITEMWEIGHT = "itemWeight";

	/** The Constant ITEM_WIDTH. */
	public static final String ITEM_WIDTH = "item-width";

	/** The Constant ITEMWIDTH. */
	public static final String ITEMWIDTH = "itemWidth";

	// bookcddvd properties
	/** The Constant AUTHOR_ARTIST. */
	public static final String AUTHOR_ARTIST = "author-artist";

	/** The Constant AUTHOR. */
	public static final String AUTHOR = "author";

	/** The Constant DATE_PUBLISHED. */
	public static final String DATE_PUBLISHED = "date-published";

	/** The Constant DATEPUBLISHED. */
	public static final String DATEPUBLISHED = "datePublished";

	/** The Constant DIRECTOR. */
	public static final String DIRECTOR = "director";

	/** The Constant DVD_REGION. */
	public static final String DVD_REGION = "dvd-region";

	/** The Constant DVDREGION. */
	public static final String DVDREGION = "dvdRegion";

	/** The Constant FORMAT. */
	public static final String FORMAT = "format";

	/** The Constant MPAA_RATING. */
	public static final String MPAA_RATING = "mpaa-rating";

	/** The Constant MPAARATING. */
	public static final String MPAARATING = "mpaaRating";

	/** The Constant NO_OF_DISKS. */
	public static final String NO_OF_DISKS = "no-of-disks";

	/** The Constant NOOFDISKS. */
	public static final String NOOFDISKS = "noOfDisks";

	/** The Constant NO_OF_PAGES. */
	public static final String NO_OF_PAGES = "no-of-pages";

	/** The Constant NOOFPAGES. */
	public static final String NOOFPAGES = "noOfPages";

	/** The Constant PARENTAL_ADVISORY. */
	public static final String PARENTAL_ADVISORY = "parental-advisory";

	/** The Constant PARENTALADVISORY. */
	public static final String PARENTALADVISORY = "parentalAdvisory";

	/** The Constant PUBLISHER. */
	public static final String PUBLISHER = "publisher";

	/** The Constant RUN_TIME. */
	public static final String RUN_TIME = "run-time";

	/** The Constant RUNTIME. */
	public static final String RUNTIME = "runTime";

	/** The Constant STUDIO. */
	public static final String STUDIO = "studio";

	/** The Constant IMAGE. */
	public static final String IMAGE = "image";

	/** The Constant IMAGE_ID. */
	public static final String IMAGE_ID = "ID";

	/** The Constant PRIMARY_IMAGE. */
	public static final String PRIMARY_IMAGE = "primary-image";

	/** The Constant SECONDARY_IMAGE. */
	public static final String SECONDARY_IMAGE = "packaged-image";

	/** The Constant ALTERNATE_IMAGE. */
	public static final String ALTERNATE_IMAGE = "alternate-image";

	/** The Constant DIGITAL_ASSET_IMAGE. */
	public static final String DIGITAL_ASSET_IMAGE = "digital-asset-image";

	/** The Constant FULL_SIZE_URL. */
	public static final String FULL_SIZE_URL = "full-size-url";

	/** The Constant INTERNET_IMAGE_URL. */
	public static final String INTERNET_IMAGE_URL = "internet-image-url";
	
	/** The Constant CANONICAL_IMAGE_URL. */
	public static final String CANONICAL_IMAGE_URL = "canonical-image-url";

	/** The Constant CAR_SEAT_SKU. */
	public static final String CAR_SEAT_SKU = "carSeatSKU";

	/** The Constant REGULAR_SKU. */
	public static final String REGULAR_SKU = "regularSKU";

	/** The Constant STROLLER_SKU. */
	public static final String STROLLER_SKU = "strollerSKU";

	/** The Constant CRIB_SKU. */
	public static final String CRIB_SKU = "cribSKU";

	/** The Constant BPP_SKU. */
	public static final String BPP_SKU = "bppSKU";

	/** The Constant BOOKCDDVD_SKU. */
	public static final String BOOKCDDVD_SKU = "bookCdDvDSKU";

	/** The Constant VIDEOGAME_SKU. */
	public static final String VIDEOGAME_SKU = "videoGameSKU";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = " ";

	/** The Constant BOY. */
	public static final String BOY = "Boy";

	/** The Constant GIRL. */
	public static final String GIRL = "Girl";

	/** The Constant BOTH. */
	public static final String BOTH = "Both";

	/** The Constant UNDER_SCORE. */
	public static final String UNDER_SCORE = "_";

	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;

	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;

	/** The Constant NUMBER_TEN. */
	public static final int NUMBER_TEN = 10;
	
	/** The Constant NUMBER_THIRTEEN. */
	public static final int NUMBER_THIRTEEN = 13;

	/** The Constant Y. */
	public static final String Y = "Y";

	/** The Constant N. */
	public static final String N = "N";

	/** The Constant A. */
	public static final String A = "A";

	/** The Constant A. */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** The Constant PROCESSING_DIRECTORY. */
	public static final String PROCESSING_DIRECTORY = "processingDirectory";
	
	/** The Constant DELETE_PROCESSING_DIRECTORY. */
	public static final String DELETE_PROCESSING_DIRECTORY = "deleteProcessingDirectory";
	
	/** The Constant DELETE_PROCESSING_DIRECTORY. */
	public static final String DELETE_SOURCE_DIRECTORY = "deleteSourceDirectory";

	/** The Constant BAD_DIRECTORY. */
	public static final String BAD_DIRECTORY = "badDirectory";

	/** The Constant INVALID_RECORD. */
	public static final String INVALID_RECORD_PRODUCT_ID = "Invalid Record: Product Feed -- Id is empty";

	/** The Constant INVALID_RECORD_PRODUCT_NAME. */
	public static final String INVALID_RECORD_PRODUCT_NAME = "Invalid Record: Product Feed -- Product Name is empty";

	/** The Constant INVALID_RECORD_PRODUCT_RUSITEM_NUMBER. */
	public static final String INVALID_RECORD_PRODUCT_RUSITEM_NUMBER = "Invalid Record: Product Feed -- Product RusItem_Number is not valid";

	/** The Constant INVALID_RECORD_PRODUCT_TAX_CODE. */
	public static final String INVALID_RECORD_PRODUCT_TAX_CODE = "Invalid Record: Product Feed -- Product Tax Code is not valid";

	/** The Constant INVALID_RECORD_PRODUCT_GENDER. */
	public static final String INVALID_RECORD_PRODUCT_GENDER = "Invalid Record: Product Feed -- Product Gender is not valid";
	
	/** The Constant INVALID_RECORD_PRODUCT_GENDER. */
	public static final String INVALID_RECORD_PRODUCT_CHANNEL_AVAILABILITY = "Invalid Record: Product Feed -- SKU Channel Availability is not valid";

	/** The Constant INVALID_RECORD_PRODUCT_BOOLEAN. */
	public static final String INVALID_RECORD_PRODUCT_BOOLEAN = "Invalid Record: Product Feed -- Boolean property is mismatched";

	/** The Constant INVALID_RECORD_PRODUCT_NMWA. */
	public static final String INVALID_RECORD_PRODUCT_NMWA = "Invalid Record: Product Feed -- NmwaPercentage is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_REVIEWCOUNTDECIMAL. */
	public static final String INVALID_RECORD_PRODUCT_REVIEWCOUNTDECIMAL = "Invalid Record: Product Feed -- ReviewCountDecimal is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_SALESVOLUME. */
	public static final String INVALID_RECORD_PRODUCT_SALESVOLUME = "Invalid Record: Product Feed -- SalesVolume is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_STREET_DATE. */
	public static final String INVALID_RECORD_PRODUCT_STREET_DATE = "Invalid Record: Product Feed -- Street Date is Invalid";
	
	/** The Constant INVALID_RECORD_PRODUCT_STREET_DATE. */
	public static final String INVALID_RECORD_INVENTORY_RECEIVED_DATE = "Invalid Record: Product Feed -- Inventory Received Date is Invalid";


	/** The Constant INVALID_RECORD_PRODUCT_DISPLAY_NAME. */
	public static final String INVALID_RECORD_PRODUCT_DISPLAY_NAME = "Invalid Record: Product Feed -- SKU Online Title is empty";

	/** The Constant INVALID_RECORD_PRODUCT_BACK_ORDER_STATUS. */
	public static final String INVALID_RECORD_PRODUCT_BACK_ORDER_STATUS = "Invalid Record: Product Feed -- SKU BACK ORDER STATUS is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_VIDEO_GAME_ESRB. */
	public static final String INVALID_RECORD_PRODUCT_VIDEO_GAME_ESRB = "Invalid Record: Product Feed -- SKU videoGameEsrb STATUS is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_FREIGHT_CLASS. */
	public static final String INVALID_RECORD_PRODUCT_FREIGHT_CLASS = "Invalid Record: Product Feed -- SKU freightClass is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_PRICE_DISPLAY. */
	public static final String INVALID_RECORD_PRODUCT_PRICE_DISPLAY = "Invalid Record: Product Feed -- SKU priceDisplay is invalid";

	/** The Constant INVALID_RECORD_PRODUCT_NMWA_VALUE. */
	public static final String INVALID_RECORD_PRODUCT_NMWA_VALUE = "Invalid Record: Product Feed -- SKU NMWA is invalid";
	
	/** The Constant INVALID_RECORD_PRODUCT_UPC_NUMBER. */
	public static final String INVALID_RECORD_PRODUCT_UPC_NUMBER = "Invalid Record: Product Feed -- SKU upc-number length is more than 13";

	/** The Constant INVALID_RECORD_PRODUCT_SKU_COLORUPCLEVEL_EXCEPTION . */
	public static final String INVALID_RECORD_PRODUCT_SKU_COLORUPCLEVEL_EXCEPTION = "Invalid Record: Product Feed -- SKU COLORUPCLEVEL Exception in Inspector";

	/** The Constant INVALID_RECORD_PRODUCT_SKU_JUVENILE_PRODUCT_SIZE_EXCEPTION . */
	public static final String INVALID_RECORD_PRODUCT_SKU_JUVENILE_PRODUCT_SIZE_EXCEPTION = "Invalid Record: Product Feed -- SKU JUVENILEPRODUCTSIZE Exception in Inspector";

	/** The Constant INVALID_RECORD_PRODUCT. */
	public static final String INVALID_RECORD_PRODUCT = "Invalid Record: Product Feed";

	/** The Constant SUCCESS_DIRECTORY. */
	public static final String SUCCESS_DIRECTORY = "successDirectory";

	/** The Constant ERROR_DIRECTORY. */
	public static final String ERROR_DIRECTORY = "errorDirectory";

	/** The Constant PRICE_FEED_DATA_VALIDATION_STEP. */
	public static final String CAT_FEED_DATA_VALIDATION_STEP = "catFeedDataValidationStep";

	/** The Constant PRICE_FEED_DATA_EXISTS_STEP. */
	public static final String CAT_FEED_DATA_EXISTS_STEP = "catFeedDataExistsStep";

	/** The Constant XLS. */
	public static final String XLS = "xls";

	/** The Constant SHEET. */
	public static final String SHEET = "sheet";

	/** The Constant NAVIGATION_FAO. */
	public static final String NAVIGATION_FAO = "Navigation_FAO";

	/** The Constant FAO. */
	public static final String FAO = "FAO";

	/** The Constant NAVIGATION_BRU. */
	public static final String NAVIGATION_BRU = "Navigation_BRU";

	/** The Constant BRU. */
	public static final String BRU = "BRU";

	/** The Constant NAVIGATION_TRU. */
	public static final String NAVIGATION_TRU = "Navigation_TRU";

	/** The Constant TRU. */
	public static final String TRU = "TRU";

	/** The Constant STYLE. */
	public static final String STYLE = "Style";

	/** The Constant ACTIONCODE. */
	public static final String ACTIONCODE = "ActionCode";

	/** The Constant ACTIONCODE. */
	public static final String ACTION_CODE = "action-code";

	/** The Constant NAME. */
	public static final String NAME = "Name";

	/** The Constant DIMENSION. */
	public static final String DIMENSION = "Dimension";

	/** The Constant LABEL. */
	public static final String LABEL = "Label";

	/** The Constant ATTRIBUTEID. */
	public static final String ATTRIBUTEID = "AttributeID";

	/** The Constant STYLEUID. */
	public static final String STYLEUID = "StyleUID";

	/** The Constant PRODUCTID. */
	public static final String PRODUCTID = "ProductID";

	/** The Constant STYLEVALUEID. */
	public static final String STYLE_VALUE_ID = "15821";

	/** The Constant FEED. */
	public static final String FEED = "FEED";

	/** The Constant FULL. */
	public static final String FULL = "FULL";
	
	/** The Constant DELETE. */
	public static final String DELETE = "Delete";

	/** The Constant DELTA. */
	public static final String DELTA = "DELTA";

	/** The Constant D. */
	public static final String D = "D";

	/** The Constant C. */
	public static final String C = "C";

	/** The Constant TILDE. */
	public static final String TILDE = "~";

	/** The Constant CANONICAL_FULL. */
	public static final String CANONICAL_FULL = "Canonical_Full";
	
	/** The Constant CANONICAL_DELETE. */
	public static final String CANONICAL_DELETE = "Canonical_Delete";

	/** The Constant ITEM_ATTRIBUTE. */
	public static final String ITEM_ATTRIBUTE = "item-attribute";

	/** The Constant EDQ_ITEM_TYPE. */
	public static final String EDQ_ITEM_TYPE = "edq-item-type";

	/** The Constant NON_MERCHANDISE. */
	public static final String NON_MERCHANDISE = "Non-Merchandise";

	/** The Constant NONMERCHSKU. */
	public static final String NONMERCHSKU = "nonMerchSKU";

	/** The Constant UNKNOWN. */
	public static final String UNKNOWN = "Unknown";

	/** The Constant INVENTORY. */
	public static final String INVENTORY = "inventory";

	/** The Constant ONLINE. */
	public static final String ONLINE = "online";

	/** The Constant INV. */
	public static final String INV = "inv";

	/** The Constant SHIP TO STORE ELIGIBLE. */
	public static final String SHIP_TO_STORE_ELIGIBLE = "ship-to-store-eligible";

	/** The Constant YES. */
	public static final String YES = "Yes";

	/** The Constant NO. */
	public static final String NO = "No";

	/** The Constant IN_STORE_ONLY. */
	public static final String IN_STORE_ONLY = "In Store Only";

	/** The Constant ONLINE_ONLY. */
	public static final String ONLINE_ONLY = "Online Only";

	/** The Constant IN_STORE_ONLY. */
	public static final String N_A = "N/A";

	/** The Constant MESSAGE1. */
	public static final String MESSAGE1 = "message1";

	/** The Constant MESSAGE2. */
	public static final String MESSAGE2 = "message2";

	/** The Constant MESSAGE3. */
	public static final String MESSAGE3 = "message3";

	/** The Constant MESSAGE4. */
	public static final String MESSAGE4 = "message4";

	/** The Constant MESSAGE5. */
	public static final String MESSAGE5 = "message5";

	/** The Constant MESSAGE6. */
	public static final String MESSAGE6 = "message6";

	/** The Constant MESSAGE7. */
	public static final String MESSAGE7 = "message7";

	/** The Constant MESSAGE8. */
	public static final String MESSAGE8 = "message8";

	/** The Constant MESSAGE9. */
	public static final String MESSAGE9 = "message9";

	/** The Constant MESSAGE10. */
	public static final String MESSAGE10 = "message10";

	/** The Constant MESSAGE11. */
	public static final String MESSAGE11 = "message11";

	/** The Constant MESSAGE12. */
	public static final String MESSAGE12 = "message12";

	/** The Constant COLLECTIONSPAGE. */
	public static final String COLLECTIONSPAGE = "CollectionsPage";

	/** The Constant AKAMAI_IMAGE_URL. */
	public static final String AKAMAI_IMAGE_URL = "akamaiImageURL";

	/** The Constant EARLY_CHILDHOOD. */
	public static final String EARLY_CHILDHOOD = "Early Childhood";

	/** The Constant EVERYONE. */
	public static final String EVERYONE = "Everyone";

	/** The Constant EVERYONE_10. */
	public static final String EVERYONE_10 = "Everyone 10+";

	/** The Constant MATURE. */
	public static final String MATURE = "Mature";

	/** The Constant RATING_PENDING. */
	public static final String RATING_PENDING = "Rating Pending";

	/** The Constant TEEN. */
	public static final String TEEN = "Teen";
	
	/** The Constant NA. */
	public static final String NA = "N/A";

	/** The Constant B. */
	public static final String B = "B";

	/** The Constant R. */
	public static final String R = "R";

	/** The Constant R. */
	public static final String SHPTRUCK = "SHPTRUCK";

	/** The Constant TRU_SHPG. */
	public static final String TRU_SHPG = "TRU_SHPG";

	/** The Constant TRU_SHPA. */
	public static final String TRU_SHPA = "TRU_SHPA";

	/** The Constant SHPA. */
	public static final String SHPA = "SHPA";

	/** The Constant ANY. */
	public static final String ANY = "ANY";

	/** The Constant STANDARD. */
	public static final String STANDARD = "Standard";

	/** The Constant PRICE_ON_PRODUCT_PAGE. */
	public static final String PRICE_ON_PRODUCT_PAGE = "Price on Product Page";

	/** The Constant PRICE_ON_CART_PAGE. */
	public static final String PRICE_ON_CART_PAGE = "Price on Cart Page";

	/** The Constant COLLECTIONPRODUCT. */
	public static final String COLLECTIONPRODUCT = "collectionProduct";

	/** The Constant TRUCATEGORYLIST. */
	public static final String TRUCATEGORYLIST = "truCategoryList";

	/** The Constant NUMBER_TWENTY_FOUR. */
	public static final int NUMBER_TWENTY_FOUR = 24;
	
	/** The Constant NUMBER_TWENTY_FOUR. */
	public static final String NUMBER_TWENTY_FOUR_STRING = "24";
	
	/** The Constant NUMBER_FOURTY_EIGHT. */
	public static final String NUMBER_FOURTY_EIGHT_STRING = "48";

	/** The Constant NUMBER_FIVE. */
	public static final int NUMBER_FIVE = 5;

	/** The Constant INVENTORY_FLAG . */
	public static final String INVENTORY_FLAG = "inventoryFlag";
	
	/** The Constant FEED_CATALOG_PROPERTY . */
	public static final String FEED_CATALOG_PROPERTY = "FeedCatalogProperty";
	

	/** The Constant COLOR_NAME. */
	public static final String COLOR_NAME = "colorName";

	/** The Constant SIZE_DESCRIPTION. */
	public static final String SIZE_DESCRIPTION = "sizeDescription";

	/** The Constant COLOR. */
	public static final String COLOR = "color";

	/** The Constant SIZE. */
	public static final String SIZE = "size";

	/** The Constant PERCENTAGE. */
	public static final String PERCENTAGE = "%";
	
	/** The Constant NULL. */
	public static final String NULL = "null";
	
	/** The Constant SOURCE. */
	public static final String SOURCE = "source";
	
	/** The Constant DIVISION_CODE. */
	public static final String DIVISION_CODE = "division-code";
	
	/** The Constant DEPARTMENT_CODE. */
	public static final String DEPARTMENT_CODE = "department-code";
	
	/** The Constant CLASS_CODE. */
	public static final String CLASS_CODE = "class-code";
	
	/** The Constant SUBCLASS_CODE. */
	public static final String SUBCLASS_CODE = "subclass-code";
	
	/** The Constant RMS. */
	public static final String RMS = "RMS";
	
	/** The Constant SEPARATOR. */
	public static final String SEPARATOR = " --> ";
	
	/** The Constant PCSIF. */
	public static final String PCSIF = "Please Create SKU in Full Feed";
	
	/** The Constant PCPIF. */
	public static final String PCPIF = "Please Create Product in Full Feed";

	/** The Constant BPP_ELIGIBLE. */
	public static final String BPP_ELIGIBLE = "bpp-eligible";

	/** The Constant BPP_NAME. */
	public static final String BPP_NAME = "bpp-name";
	
	/** The Constant RMS. */
	public static final String REPOSITORY_EXCEPTION = "Repository Exception";
	
	/** The Constant DOT_SPLIT. */
	public static final String DOT_SPLIT = "\\.";
	
	/** The Constant DOT. */
	public static final String DOT = ".";
	
	/** The Constant PRODUCT_CANONICAL_TOTAL_COUNT. */
	public static final String  PRODUCT_CANONICAL_TOTAL_COUNT = "Product Canonical Total Count";
	
	/** The Constant INVALID_RECORD_ACTION_CODE_EMPTY. */
	public static final String  INVALID_RECORD_ACTION_CODE_EMPTY = "Invalid Record: Product Feed -- Action Code is Empty";
	
	/** The Constant INVALID_RECORD_ACTION_CODE_NOT_D. */
	public static final String  INVALID_RECORD_ACTION_CODE_NOT_D = "Invalid Record: Product Feed -- Action Code is not correct. Products in delete file should have Action Code D";
	
	/** The Constant STYLE_ERROR. */
	public static final String STYLE_ERROR = "STYLE_ERROR";
	
	/** The Constant CAMMA. */
	public static final String CAMMA = ",";
	
	/** The Constant STYLE_ERROR_MSG_NOT_FOUND. */
	public static final String STYLE_ERROR_MSG_NOT_FOUND = "These SKUs are not there in Data Base. Please Create them";
	
	/** The Constant STYLE_ERROR_MSG_NOT_ASS. */
	public static final String STYLE_ERROR_MSG_NOT_ASS = "These SKUs are not associated with any Product. Please Check";
	
	/** The Constant SPACE. */
	public static final String SPACE = " ";

	/** The Constant INVENTORY_RECEIVED_DATE. */
	public static final String INVENTORY_RECEIVED_DATE = "inventory-received-date";
	
	/** The Constant RMS_SIZE_CODE. */
	public static final String RMS_SIZE_CODE = "rms-size-code";
	
	/** The Constant RMS_COLOR_CODE. */
	public static final String RMS_COLOR_CODE = "rms-color-code";
	
	/** The Constant TCOUNT. */
	public static final String TCOUNT = "TCOUNT";
	
	/** The Constant RCOUNT. */
	public static final String RCOUNT = "RCOUNT";
	
	/** The ConstantPCOUNT. */
	public static final String PCOUNT = "PCOUNT";
	
	/** The Constant SCOUNT. */
	public static final String SCOUNT = "SCOUNT";
	
	/** The Constant PMCOUNT. */
	public static final String PMCOUNT = "PMCOUNT";
	
	/** The Constant CPMCOUNT. */
	public static final String CPMCOUNT = "CPMCOUNT";
	
	/** The Constant SMCOUNT. */
	public static final String SMCOUNT = "SMCOUNT";
	
	/** The Constant CMCOUNT. */
	public static final String CMCOUNT = "CMCOUNT";
	
	/** The Constant RMCOUNT. */
	public static final String RMCOUNT = "RMCOUNT";
	
	/** The Constant PRMCOUNT. */
	public static final String PRMCOUNT = "PRMCOUNT";
	
	/** The Constant CPRMCOUNT. */
	public static final String CPRMCOUNT = "CPRMCOUNT";
	
	/** The Constant CRMCOUNT. */
	public static final String CRMCOUNT = "CRMCOUNT";
	
	/** The Constant RRMCOUNT. */
	public static final String RRMCOUNT = "RRMCOUNT";
	
	/** The Constant CATEGORY. */
	public static final String CATEGORY = "category";
	
	/** The Constant PARENT_CATEGORIES. */
	public static final String PARENT_CATEGORIES = "parentCategories";

	/** The Constant BOOK_SKU_MAPPER_METHOD. */
	public static final String BOOK_SKU_MAPPER_METHOD = "TRUBookCdDvDSKUMapper.map() : ";

	/** The Constant CAR_SEAT_SKU_MAPPER_METHOD. */
	public static final String CAR_SEAT_SKU_MAPPER_METHOD = "TRUCarSeatSKUMapper.map() : ";

	/** The Constant CATEGORY_MAPPER_METHOD. */
	public static final String CATEGORY_MAPPER_METHOD = "TRUCategoryMapper.map() : ";

	/** The Constant CATEGORY_REL_MAPPER_METHOD. */
	public static final String CATEGORY_REL_MAPPER_METHOD = "TRUCategoryRelationshipMapper.map() : ";

	/** The Constant CLASSIFICATION_MAPPER_METHOD. */
	public static final String CLASSIFICATION_MAPPER_METHOD = "TRUClassificationMapper.map() : ";

	/** The Constant CLASSIFICATION_REL_MAPPER_METHOD. */
	public static final String CLASSIFICATION_REL_MAPPER_METHOD = "TRUClassificationRelationshipMapper.map() : ";

	/** The Constant COLLECTION_PRD_MAPPER_METHOD. */
	public static final String COLLECTION_PRD_MAPPER_METHOD = "TRUCollectionProductMapper.map() : ";

	/** The Constant COLLECTION_PRD_REL_MAPPER_METHOD. */
	public static final String COLLECTION_PRD_REL_MAPPER_METHOD = "TRUCollectionProductRelationshipMapper.map() : ";

	/** The Constant CRIB_SKU_MAPPER_METHOD. */
	public static final String CRIB_SKU_MAPPER_METHOD = "TRUCribSKUMapper.map() : ";

	/** The Constant PRODUCT_MAPPER_METHOD. */
	public static final String PRODUCT_MAPPER_METHOD = "TRUProductMapper.map() : ";

	/** The Constant PRODUCT_REL_MAPPER_METHOD. */
	public static final String PRODUCT_REL_MAPPER_METHOD = "TRUProductRelationshipMapper.map() : ";

	/** The Constant REGISTRY_CLASS_MAPPER_METHOD. */
	public static final String REGISTRY_CLASS_MAPPER_METHOD = "TRURegistryClassificationMapper.map() : ";

	/** The Constant REGISTRY_CLASS_REL_MAPPER_METHOD. */
	public static final String REGISTRY_CLASS_REL_MAPPER_METHOD = "TRURegistryClassificationRelationshipMapper.map() : ";

	/** The Constant SKU_MAPPER_METHOD. */
	public static final String SKU_MAPPER_METHOD = "TRUSKUMapper.map() : ";

	/** The Constant SKU_REL_MAPPER_METHOD. */
	public static final String SKU_REL_MAPPER_METHOD = "TRUSKURelationshipMapper.map() : ";

	/** The Constant STROLLER_SKU_MAPPER_METHOD. */
	public static final String STROLLER_SKU_MAPPER_METHOD = "TRUStrollerSKUMapper.map() : ";

	/** The Constant VDGAME_SKU_MAPPER_METHOD. */
	public static final String VDGAME_SKU_MAPPER_METHOD = "TRUVideoGameSKUMapper.map() : ";
	
	/** The Constant SKILLS. */
	public static final String SKILLS = "skills";
	
	/** The Constant PRIMARY_CATEGORY. */
	public static final String PRIMARY_CATEGORY = "primary-category";
	
	/** The Constant PLAY_TYPE. */
	public static final String PLAY_TYPE = "play-type";
	
	/** The Constant WHAT_IS_IMPORTANT. */
	public static final String WHAT_IS_IMPORTANT = "what-is-important";
	
	/** The Constant HYPEN. */
	public static final String HYPEN = "-";
	
	/** The Constant PARSE_EXCEPTION. */
	public static final String PARSE_EXCEPTION = "ParseException in product feed handler";
	
	/** The Constant PRODUCT_FEED. */
	public static final String PRODUCT_FEED = "productfeed";
	
	/** The Constant FEED_FILE_SEQUENCE. */
	public static final String FEED_FILE_SEQUENCE = "feedfilesequence";
	
	/** The Constant SEQUENCE_PROCESSED. */
	public static final String SEQUENCE_PROCESSED = "sequenceprocessed";
	
	/** The Constant DOUBLE_NUMBER_ZERO. */
	public static final double DOUBLE_NUMBER_ZERO = 0.0;
	
	/** The Constant UPC_NUMBER_INVALID. */
	public static final String UPC_NUMBER_INVALID = "upc-number length is more than 13";
	
	/** The Constant NMWA_INVALID. */
	public static final String NMWA_INVALID = "nmwa value should be one of Y/N";

	/** The Constant PRICE_DISPLAY_INVALID. */
	public static final String PRICE_DISPLAY_INVALID = "price-display value should be one of Price on Cart Page / Price on Product Page / Standard";

	/** The Constant BACK_ORDER_STATUS_INVALID. */
	public static final String BACK_ORDER_STATUS_INVALID = "backorder-status value should be one of N/B/R ";

	/** The Constant SKU_ID_INVALID. */
	public static final String SKU_ID_INVALID = "SKU id is empty";

	/** The Constant SKU_DISPLAY_NAME_INVALID. */
	public static final String SKU_DISPLAY_NAME_INVALID = "online-title is empty";

	/** The Constant INVENTORY_RECEIVED_DATE_INVALID. */
	public static final String INVENTORY_RECEIVED_DATE_INVALID = "inventory-received-date value is not in proper date format";

	/** The Constant STREET_DATE_INVALID. */
	public static final String STREET_DATE_INVALID = "online-product-street-date value is not in proper date format";

	/** The Constant SALESVOLUME_INVALID. */
	public static final String SALESVOLUME_INVALID = "sales-volume is not a integer value";

	/** The Constant REVIEWCOUNTDECIMAL_INVALID. */
	public static final String REVIEWCOUNTDECIMAL_INVALID = "review-count-decimal is not a decimal value";

	/** The Constant NMWA_PERCENTAGE_INVALID. */
	public static final String NMWA_PERCENTAGE_INVALID = "nmwa-percentage is not a decimal value";

	/** The Constant REGISTRY_WISH_LIST_ELIGIBILITY_INVALID. */
	public static final String REGISTRY_WISH_LIST_ELIGIBILITY_INVALID = "registry-wish-list-eligibility value should be one of Y/N";

	/** The Constant SLAPPER_ITEM_INVALID. */
	public static final String SLAPPER_ITEM_INVALID = "slapper-item value should be one of Y/N";

	/** The Constant WEB_DISPLAY_FLAG_INVALID. */
	public static final String WEB_DISPLAY_FLAG_INVALID = "web-display-flag value should be one of A/N";

	/** The Constant SUPRESS_IN_SEARCH_INVALID. */
	public static final String SUPRESS_IN_SEARCH_INVALID = "supress-in-search value should be one of Y/N";

	/** The Constant SHIP_IN_ORIG_CONTAINER_INVALID. */
	public static final String SHIP_IN_ORIG_CONTAINER_INVALID = "ship-in-orig-container value should be one of Y/N";

	/** The Constant OUTLET_INVALID. */
	public static final String OUTLET_INVALID = "outlet value should be one of Y/N";

	/** The Constant DROPSHIP_FLAG_INVALID. */
	public static final String DROPSHIP_FLAG_INVALID = "dropship-flag value should be one of Y/N";

	/** The Constant SHIP_FROM_STORE_ELIGIBLE_LOV_INVALID. */
	public static final String SHIP_FROM_STORE_ELIGIBLE_LOV_INVALID = "ship-from-store-eligible-lov value should be one of Y/N";

	/** The Constant ACTION_CODE_EMPTY. */
	public static final String ACTION_CODE_EMPTY = "action-code is empty for SKU";
	
	/** property to hold WEB_SITE_TAXONOMY_ERROR_FILENAME. */
	public static final String WEB_SITE_TAXONOMY_ERROR_FILENAME = "Errors_WebSiteTaxonomyFeed";

	/** property to hold REGISTRY_CATEGORIZATION_ERROR_FILENAME. */
	public static final String REGISTRY_CATEGORIZATION_ERROR_FILENAME = "Errors_RegistryCategorizationFeed";

	/** property to hold PRODUCT_CANONICAL_ERROR_FILENAME. */
	public static final String PRODUCT_CANONICAL_ERROR_FILENAME = "Errors_ProductCanonicalFeed";
	
	/** The Constant PROCESS_ON. */
	public static final String PROCESS_ON= "_Processedon_";
	
	/** The Constant THRESHOLD_MESSAGE. */
	public static final String THRESHOLD_MESSAGE= " Job Failed as More than 30% Invalid Records Found in ";
	
	/** The Constant THRESHOLD_FAILED_PREVIOUS_MESSAGE. */
	public static final String THRESHOLD_FAILED_PREVIOUS_MESSAGE= " Job Failed as threshold exceeded more than 20% with previous processed feed count in ";
	
	/** The Constant PROCESS_FILE_SUCCESS. */
	public static final String PROCESS_FILE_SUCCESS= "File has been successfully processed";
	
	/** The Constant EMAIL_FAIL_MESSAGE1. */
	public static final String EMAIL_FAIL_MESSAGE1= "TRU ATG Web Store - USA - ";
	
	/** The Constant EMAIL_FAIL_MESSAGE2. */
	public static final String EMAIL_FAIL_MESSAGE2= " - Catalog - ";
	
	/** The Constant EMAIL_FAIL_MESSAGE3. */
	public static final String EMAIL_FAIL_MESSAGE3= " Processed with Errors - ";
	
	/** The Constant EMAIL_FAIL_MESSAGE4. */
	public static final String EMAIL_FAIL_MESSAGE4= " Processing Failed - ";
	
	/** The Constant EMAIL_FAIL_MESSAGE5. */
	public static final String EMAIL_FAIL_MESSAGE5= "MM-dd-yyyy'-'hh:mm:ss";
	
	/** The Constant EMAIL_FAIL_MESSAGE6. */
	public static final String EMAIL_FAIL_MESSAGE6= " Processed Successfully - ";
	
	/** The Constant SEQ_MISS. */
	public static final String SEQ_MISS= "sequenceMiss";
	
	/** The Constant JOB_FAILED_MISSING_SEQ. */
	public static final String JOB_FAILED_MISSING_SEQ= "Job Failed - File with a missing sequence number";
	
	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_MISSING_FILE= "Job Failed - Missing File";
	
	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_INVALID_FILE= "Job Failed - XML - XSD Validation Errors";
	
	/** The Constant JOB_SUCCESS. */
	public static final String JOB_SUCCESS= "Job processed successfully";
	
	/** The Constant FILE_INVALID. */
	public static final String FILE_INVALID= "fileInvalid";
	
	/** The Constant FILE_TYPE. */
	public static final String FILE_TYPE = "fileType";
	
	/** The Constant EQUALS. */
	public static final String EQUALS = "=";
	
	/** The Constant REGULAR_FEED. */
	public static final String REGULAR_FEED = "RegularFeed";
	
	/** The Constant BUT_WITH_ERRORS. */
	public static final String BUT_WITH_ERRORS= " but with some errors. Please find mail attachment for the errors.";
	
	/** The Constant THRESHOLD_WEBTAX_DETAILEDEXP. */
	public static final String THRESHOLD_WEBTAX_DETAILEDEXP = "WebSiteTaxonomyFeed file processing failed due to more than 30% invalid error records found. Please find mail attachment for the errors.";
	
	/** The Constant THRESHOLD_REG_DETAILEDEXP. */
	public static final String THRESHOLD_REG_DETAILEDEXP = "RegistryCategorizationFeed file processing failed due to more than 30% invalid error records found. Please find mail attachment for the errors.";
	
	/** The Constant THRESHOLD_PRODCAN_DETAILEDEXP. */
	public static final String THRESHOLD_PRODCAN_DETAILEDEXP = "ProductCanonicalFeed file processing failed due to more than 30% invalid error records found. Please find mail attachment for the errors.";
	
	/** The Constant INVALID_WEBTAX_DETAILEDEXP. */
	public static final String INVALID_WEBTAX_DETAILEDEXP = "WebSiteTaxonomyFeed XML file failed to validate with XSD.";
	
	/** The Constant INVALID_REG_DETAILEDEXP. */
	public static final String INVALID_REG_DETAILEDEXP = "RegistryCategorizationFeed XML file failed to validate with XSD";
	
	/** The Constant INVALID_PRODCAN_DETAILEDEXP. */
	public static final String INVALID_PRODCAN_DETAILEDEXP = "ProductCanonicalFeed XML file failed to validate with XSD";
	
	/** The Constant NOTAVIL_WEBTAX_DETAILEDEXP. */
	public static final String NOTAVIL_WEBTAX_DETAILEDEXP = "Feed processing failed due to WebSiteTaxonomyFeed file is not available at ";
	
	/** The Constant NOTAVILD_REG_DETAILEDEXP. */
	public static final String NOTAVIL_REG_DETAILEDEXP = "Feed processing failed due to RegistryCategorizationFeed file is not available at ";
	
	/** The Constant NOTAVIL_PRODCAN_DETAILEDEXP. */
	public static final String NOTAVIL_PRODCAN_DETAILEDEXP = "Feed processing failed due to ProductCanonicalFeed file is not available at ";
	
	/** The Constant JOB_FAILED. */
	public static final String JOB_FAILED = "Job Failed - ";
	
	/** The Constant NOT_AVAIL. */
	public static final String NOT_AVAIL = "  is not available @";
	
	/** The Constant AT. */
	public static final String AT = " at ";
	
	/** The Constant SEQ_DETAILEXP. */
	public static final String SEQ_DETAILEXP = " Delta ProductCanonicalFeed file processed might not be in the sequence with last processed feed file or Sequence of the files placed might not be in ordered set ";
	
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "MMddyyyy'-'HH:mm:ss";
	
	/** The Constant PERMISSION_ISSUE. */
	public static final String PERMISSION_ISSUE = "Permission issue";
	
	/** The Constant FOLDER_NOT_EXISTS. */
	public static final String FOLDER_NOT_EXISTS = "Folder not exists";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_1. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_1= "Job Failed - Folder ";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_2. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_2= " permissions issues";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_1. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_1= "Job Failed - Directory/folder ";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_2. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_2= " structure not available";
	
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT_1 = "MMddyyyy'_'HH-mm-ss";
	
	/** The Constant SUCCESS_COUNT. */
	public static final String SUCCESS_COUNT = "successcount";
	
	/** The Constant ERROR_COUNT. */
	public static final String ERROR_COUNT = "errorcount";
	
	/** The Constant PROD_THRESHOLD_PERCENTAGE. */
	public static final String PROD_THRESHOLD_PERCENTAGE = "productThresholdPercentage";
	
	/** The Constant THRESHOLD_WEBTAX_PREV_20_DETAILEDEXP. */
	public static final String THRESHOLD_WEBTAX_PREV_20_DETAILEDEXP = "WebSiteTaxonomyFeed file processing failed due to threshold exceeded more than 20% with previous processed feed count. Please check the file with previous processed file.";
	
	/** The Constant THRESHOLD_REG_PREV_20_DETAILEDEXP. */
	public static final String THRESHOLD_REG_PREV_20_DETAILEDEXP = "RegistryCategorizationFeed file processing failed due to threshold exceeded more than 20% with previous processed feed count. Please check the file with previous processed file.";
	
	/** The Constant THRESHOLD_WEBTAX_PREV_15_DETAILEDEXP. */
	public static final String THRESHOLD_WEBTAX_PREV_15_DETAILEDEXP = "WebSiteTaxonomyFeed file processing but the threshold is between 15 % - 20% when compared with previous processed feed count. Please check the file with previous processed file.";
	
	/** The Constant THRESHOLD_REG_PREV_15_DETAILEDEXP. */
	public static final String THRESHOLD_REG_PREV_15_DETAILEDEXP = "RegistryCategorizationFeed file processing but the threshold is between 15 % - 20% when compared with previous processed feed count. Please check the file with previous processed file.";
	
	/** The Constant THRESHOLD_FAILED_PREVIOUS_15_MESSAGE. */
	public static final String THRESHOLD_FAILED_PREVIOUS_15_MESSAGE= " Job processing but threshold is between 15% - 20% with previous processed feed count in ";
	
	/** The Constant PREV_ALERT. */
	public static final String PREV_ALERT = "prevAlert";

	/** The Constant REPOSITORY_TO_PRODUCT_VO. */
	public static final String REPOSITORY_TO_PRODUCT_VO = "TRUDeltaRepositoryToProductVO.repositoryToProductVO()";
	
	/** The Constant CATALOG_FEED. */
	public static final String CATALOG_FEED = "CatalogFeed";
	
	/** The Constant FILE_DOWNLOAD. */
	public static final String FILE_DOWNLOAD = "File Download";
	
	/** The Constant FILE_PARSING. */
	public static final String FILE_PARSING = "File Parsing";
	
	/** The Constant FILE_VALIDATION. */
	public static final String FILE_VALIDATION = "File Validation";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant TIME_TAKEN. */
	public static final String TIME_TAKEN = "Time Taken";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant START_DATE. */
	public static final String START_DATE = "Start Date";

	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant FILES_MOVED_SUCCESS. */
	public static final String FILES_MOVED_SUCCESS = "Files Moved successfully";
	
	/** The Constant NO_MATCH_FIELS. */
	public static final String NO_MATCH_FIELS = "No Match Files";
	
	/** The Constant FILES_VALID. */
	public static final String FILES_VALID = "Files are valid";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_COMPLETED = "Feed completed";
	
	/** The Constant FILES_WRITING. */
	public static final String FILES_WRITING = "Files Writing";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "Feed Status";
	
	/** The Constant FEED_FAILED. */
	public static final String FEED_FAILED = "Feed Failed";
	
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROCESS = "Feed Process";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";
	
	/** The Constant PARSE_TAX. */
	public static final String PARSE_TAX = "Parsing WebsiteTaxonomy File";
	
	/** The Constant PARSE_REG. */
	public static final String PARSE_REG = "Parsing RegistryCategorization File";
	
	/** The Constant PARSE_PROD. */
	public static final String PARSE_PROD = "Parsing ProductCanonical File";
	
	/** The Constant FILE_NO_SEQ. */
	public static final String FILE_NO_SEQ = "File is not in sequence with last processed";

	/** The Constant NAVIGATION_CATEGORY. */
	public static final String NAVIGATION_CATEGORY = "NavigationCategory";

	/** The Constant FILTERED_LANDING_PAGE. */
	public static final String FILTERED_LANDING_PAGE = "FilteredLandingPage";
	
	/** The Constant PARENT_CLASSIFICATIONS. */
	public static final String PARENT_CLASSIFICATIONS = "parentClassifications";
	
	/** The Constant COLOR_FAMILY. */
	public static final String COLOR_FAMILY = "color-family";
	
	/** The Constant SIZE_FAMILY. */
	public static final String SIZE_FAMILY = "size-family";
	
	/** The Constant CHARACTER_THEME. */
	public static final String CHARACTER_THEME = "character-theme";
	
	/** The Constant COLLECTION_THEME. */
	public static final String COLLECTION_THEME = "collection-theme";
	
	
	public static final String EMPTY_SKU_PROPERTIES_FLAG = "emptySkuPropertiesFlag";
	
	
	
}
