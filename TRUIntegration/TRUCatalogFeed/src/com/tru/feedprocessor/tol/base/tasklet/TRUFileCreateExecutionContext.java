package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.catalog.TRUFeedCatalogProperty;

/**
 * The Class TRUFileCreateExecutionContext. This class creates ExecutionContext for processing all the feed files in the
 * single project and sets the required properties for the execution.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUFileCreateExecutionContext extends SingleFileCreateExecutionContext {
	
	
	/** The feed catalog property. */
	private TRUFeedCatalogProperty mFeedCatalogProperty;

	/**
	 * This method is used to createExecutionContext for processing all the feed files in the single project and sets the
	 * required properties for the execution.
	 * 
	 * @return the queue
	 */
	@Override
	protected Queue<FeedExecutionContextVO> createExecutionContext() {

		getLogger().vlogDebug("Start @Class: TRUFileCreateExecutionContext, @method: createExecutionContext()");

		String fileNames = (String) retriveData(TRUFeedConstants.FEEDFILENAMES);
		Queue executionContextQueue = new LinkedBlockingQueue<FeedExecutionContextVO>();

		FeedExecutionContextVO feedExecutionContextVO = new FeedExecutionContextVO();

		feedExecutionContextVO.setFileName(fileNames);
		
		String fileType = (String) getJobParameter(TRUProductFeedConstants.FILE_TYPE);
		if(StringUtils.isNotBlank(fileType) && fileType.equals(TRUProductFeedConstants.DELETE)){
			
			feedExecutionContextVO.setFilePath((String) getConfigValue(TRUProductFeedConstants.DELETE_PROCESSING_DIRECTORY).toString());
		} else {
			feedExecutionContextVO.setFilePath((String) getConfigValue(FeedConstants.PROCESSING_DIRECTORY));
		}
		
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_NAME, getFeedHelper()
				.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_NAME));
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_PATH, getFeedHelper()
				.getConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_PATH));
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P3,
				getFeedHelper().getConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P3));
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P3,
				getFeedHelper().getConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P3));
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P2,
				getFeedHelper().getConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P2));
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P2,
				getFeedHelper().getConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P2));
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.INVENTORY_FLAG,
				getFeedHelper().getConfigValue(TRUProductFeedConstants.INVENTORY_FLAG));
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.EMPTY_SKU_PROPERTIES_FLAG,
				getFeedHelper().getConfigValue(TRUProductFeedConstants.EMPTY_SKU_PROPERTIES_FLAG));
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.FEED_CATALOG_PROPERTY,
				getFeedCatalogProperty());
		executionContextQueue.add(feedExecutionContextVO);

		getLogger().vlogDebug("End @Class: TRUFileCreateExecutionContext, @method: createExecutionContext()");

		return executionContextQueue;
	}

	/**
	 * Gets the feed catalog property.
	 *
	 * @return the feed catalog property
	 */
	public TRUFeedCatalogProperty getFeedCatalogProperty() {
		return mFeedCatalogProperty;
	}

	/**
	 * Sets the feed catalog property.
	 *
	 * @param pFeedCatalogProperty the new feed catalog property
	 */
	public void setFeedCatalogProperty(TRUFeedCatalogProperty pFeedCatalogProperty) {
		this.mFeedCatalogProperty = pFeedCatalogProperty;
	}
}
