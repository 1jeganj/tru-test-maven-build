package com.tru.feedprocessor.tol.base.tasklet;

import atg.epub.project.Process;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.merchutil.tol.workflow.WorkFlowTools;

/**
 * The Class TRUCreateProjectTask.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCreateProjectTask extends CreateProjectTask {

	/** The m work flow tools. */
	private WorkFlowTools mWorkFlowTools;

	/** The m feed context. */
	private TRUFileCreateExecutionContext mFeedContext;
	/**
	 * @return the feedContext
	 */
	public TRUFileCreateExecutionContext getFeedContext() {
		return mFeedContext;
	}
	/**
	 * @param pFeedContext the feedContext to set
	 */
	/**
	 * Sets the feed context.
	 * 
	 * @param pFeedContext
	 *            the new feed context
	 */
	public void setFeedContext(TRUFileCreateExecutionContext pFeedContext) {
		this.mFeedContext = pFeedContext;
	}

	/**
	 * Gets the work flow tools.
	 * 
	 * @return the work flow tools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 * 
	 * @param pWorkFlowTools
	 *            the new work flow tools
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		mWorkFlowTools = pWorkFlowTools;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.tasklet.CreateProjectTask#createProject()
	 */
	@Override
	protected void createProject() {

		String workflowName = null;

		saveProjectName(getProjectNameGenerator().getProjectName(getCurrentFeedExecutionContext()));

		if ((Boolean) getConfigValue(FeedConstants.USEAUTODEPLOY_WORKFLOW)) {
			workflowName = (String) getConfigValue(FeedConstants.AUTODEPLOY_WORKFLOW);
		} else {
			workflowName = (String) getConfigValue(FeedConstants.MANUAL_WORKFLOW);
		}
		Process process = mWorkFlowTools.initiateWorkflow((String) retriveData(FeedConstants.PROJECT_NAME), workflowName);

		saveData(FeedConstants.PROCESS, process);
		getFeedContext().setConfigValue(FeedConstants.PROCESS, process);
	}

}
