/*
 * 
 */
package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.filefilter.RegexFileFilter;

import atg.apache.taglibs.standard.lang.jstl.parser.ParseException;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedErrorConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUFileDownloadTask will move the files from source to destination.
 *
 * @author Professional Access
 * @version 1.1
 */

public class TRUFileDownloadTask extends FileDownloadTask {

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;
	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * Gets the file sequence repository.
	 *
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 *
	 * @param pFileSequenceRepository the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * This method is used for moving the file from source directory to destination directory.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Override
	protected void doExecute() throws Exception {
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: doExecute()");
		// mSourceDir will gets the source directory from the storeFeedConfig
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		String sourceDir = nullIfEmpty(getConfigValue(TRUFeedConstants.SOURCE_DIR).toString());
		String processingDir = nullIfEmpty(getConfigValue(TRUProductFeedConstants.PROCESSING_DIRECTORY).toString());
		
		String fileType = (String) getJobParameter(TRUProductFeedConstants.FILE_TYPE);
		if(StringUtils.isNotBlank(fileType) && fileType.equals(TRUProductFeedConstants.DELETE)){
			
			sourceDir = nullIfEmpty(getConfigValue(TRUProductFeedConstants.DELETE_SOURCE_DIRECTORY).toString());
			processingDir = nullIfEmpty(getConfigValue(TRUProductFeedConstants.DELETE_PROCESSING_DIRECTORY).toString());
		}
		
		String fileExtension = nullIfEmpty(getConfigValue(TRUFeedConstants.FILETYPE).toString());
		String filePattern = nullIfEmpty(getConfigValue(TRUFeedConstants.FILENAME).toString());
		File sharedFolderFile = new File(sourceDir);
		if (sharedFolderFile.exists() && sharedFolderFile.isDirectory() && sharedFolderFile.canRead()) {
			try {
				moveSrcToProcessingDir(sourceDir, processingDir, fileExtension, filePattern, startDate);
			} catch (java.text.ParseException parseEx) {
				if(isLoggingError()){
					logError(TRUFeedErrorConstants.PARSE_EXCEPTION, parseEx);
				}
				if(parseEx.getMessage() != null){
					logFailedMessage(startDate, parseEx.getMessage());
				}else {
					logFailedMessage(startDate, TRUFeedErrorConstants.PARSE_EXCEPTION);
				}
				
			} catch (ParseException parseEx) {
				if(isLoggingError()){
					logError(TRUFeedErrorConstants.PARSE_EXCEPTION, parseEx);
				}
				if(parseEx.getMessage() != null){
					logFailedMessage(startDate, parseEx.getMessage());
				}else {
					logFailedMessage(startDate, TRUFeedErrorConstants.PARSE_EXCEPTION);
				}
			} catch (IllegalArgumentException illegarEx) {
				if(isLoggingError()){
					logError(TRUFeedErrorConstants.ILLEGALARGUMENT_EXCEPTION, illegarEx);
				}
				if(illegarEx.getMessage() != null){
					logFailedMessage(startDate, illegarEx.getMessage());
				}else {
					logFailedMessage(startDate, TRUFeedErrorConstants.ILLEGALARGUMENT_EXCEPTION);
				}
			}
		} else if(!sharedFolderFile.exists() || !sharedFolderFile.isDirectory()){
			String messageSubject = TRUProductFeedConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(sourceDir, messageSubject);
			logFailedMessage(startDate, TRUProductFeedConstants.FOLDER_NOT_EXISTS);
			throw new FeedSkippableException(TRUProductFeedConstants.FOLDER_NOT_EXISTS);
		} else if(!sharedFolderFile.canRead()){
			String messageSubject = TRUProductFeedConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(sourceDir, messageSubject);
			logFailedMessage(startDate, TRUProductFeedConstants.PERMISSION_ISSUE);
			throw new FeedSkippableException(TRUProductFeedConstants.PERMISSION_ISSUE);
		}
		getLogger().vlogDebug("End @Class: TRUFileDownloadTask, @method: doExecute()");

	}

	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_DOWNLOAD, null, extenstions);
	}
	

	
	/**
	 * Move src to processing dir.
	 *
	 * @param pSourceDir the source dir
	 * @param pProcessingDir the processing dir
	 * @param pFiletype the filetype
	 * @param pFilePattern the file pattern
	 * @param pStartDate the start date
	 * @throws Exception the exception
	 */
	public void moveSrcToProcessingDir(String pSourceDir, String pProcessingDir, String pFiletype, String pFilePattern, String pStartDate)
			throws Exception {
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: moveSrcToProcessingDir()");
		File sourceFolder = new File(pSourceDir);
		File processingFolder = new File(pProcessingDir);
		StringBuffer filesList = new StringBuffer();
		if (sourceFolder != null && sourceFolder.exists() && sourceFolder.canRead()) {
			checkForFiles(pSourceDir, pFilePattern, filesList, pStartDate);

		}
		movefiles(processingFolder, sourceFolder, filesList, pSourceDir, pStartDate);
	}

	
	
	
	/**
	 * Check for files.
	 *
	 * @param pSourceDir the source dir
	 * @param pFilePattern the file pattern
	 * @param pFilesList the files list
	 * @param pStartDate the start date
	 * @throws java.text.ParseException the parse exception
	 * @throws Exception the exception
	 */
	private void checkForFiles(String pSourceDir, String pFilePattern,
			StringBuffer pFilesList,String pStartDate)
			throws java.text.ParseException, Exception {
		pFilesList.setLength(0);
		String taxName = null;
		String regName = null;
		String prodName = null;
		int taxCount = 0;
		int regCount = 0;
		int prodCount = 0;
		boolean taxFilesExists = Boolean.FALSE;
		boolean regFilesExists = Boolean.FALSE;
		boolean prodFilesExists = Boolean.FALSE;
		boolean deleteprodFileExists = Boolean.FALSE;
		List<Date> taxDateList = new ArrayList<Date>();
		List<Date> regDateList = new ArrayList<Date>();
		List<Date> prodDateList = new ArrayList<Date>();
		List<Integer> fileSequenceList = new ArrayList<Integer>();
		List<File> fileLists;
		String[] feedFileNames;
		String timestamp;
		String name;
		String seqNumber;
		Date date;
		fileLists = fileLists(pSourceDir, pFilePattern);
		Map<Integer,String> hashMap = new HashMap<Integer,String>();
		for (File srcFile : fileLists) {
			name = srcFile.getName();
			feedFileNames = name.split(TRUFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
			timestamp = feedFileNames[TRUFeedConstants.ONE];
			Format feedFormatter = new SimpleDateFormat(TRUFeedConstants.FEED_DATE_FORMAT, Locale.getDefault());
			Format actuallDateFormat = new SimpleDateFormat(TRUFeedConstants.DATE_FORMAT,  Locale.getDefault());
			date = (Date) ((DateFormat) feedFormatter).parse(timestamp);
			String feedName = actuallDateFormat.format(date);
			String todayDate = actuallDateFormat.format(new Date());
			if (name.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)
					&& todayDate.equalsIgnoreCase(feedName)
					&& (taxCount == 0 || (taxCount > 0 && date
							.after(taxDateList.get(taxDateList.size()
									- TRUFeedConstants.ONE))))) {
				taxFilesExists = Boolean.TRUE;
				taxDateList.add(date);
				taxName = name + TRUFeedConstants.DOLLAR_ONLY;
				taxCount++;
			}else if (name.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)
					&& todayDate.equalsIgnoreCase(feedName)
					&& (regCount == 0 || (regCount > 0 && date
							.after(regDateList.get(regDateList.size()
									- TRUFeedConstants.ONE))))) {
				regFilesExists = Boolean.TRUE;
				regDateList.add(date);
				regName = name + TRUFeedConstants.DOLLAR_ONLY;
				regCount++;
			}else if (name.contains(TRUFeedConstants.ATG_PRODUCT)) {
				if (name.contains(TRUProductFeedConstants.CANONICAL_FULL) && todayDate.equalsIgnoreCase(feedName)) {
					if (prodCount == 0 || (prodCount > 0 && date.after(prodDateList.get(prodDateList.size() - TRUFeedConstants.ONE)))) {
						prodName = name + TRUFeedConstants.DOLLAR_ONLY;
					}
					prodDateList.add(date);
					prodCount++;
					prodFilesExists = Boolean.TRUE;
				}else if(name.contains(TRUProductFeedConstants.CANONICAL_DELETE)){
					if (prodCount == 0 || (prodCount > 0 && date.after(prodDateList.get(prodDateList.size() - TRUFeedConstants.ONE)))) {
						prodName = name + TRUFeedConstants.DOLLAR_ONLY;
					}
					prodDateList.add(date);
					prodCount++;
					prodFilesExists = Boolean.TRUE;
					deleteprodFileExists = Boolean.TRUE;
				}else {
					feedFileNames = name.split(TRUFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
					seqNumber = feedFileNames[TRUFeedConstants.NUMBER_SIX];
					String sequence = seqNumber.substring(TRUFeedConstants.NUMBER_ZERO, seqNumber.length() - TRUFeedConstants.NUMBER_FOUR);
					hashMap.put(Integer.valueOf(sequence), name);
					fileSequenceList.add(Integer.valueOf(sequence));
				}
			}
		}
		taxDateList.clear();
		regDateList.clear();
		prodDateList.clear();
		if(checkFileSequence(fileSequenceList,pSourceDir,pStartDate)){
			prodFilesExists = Boolean.TRUE;
		}
		
		if(!deleteprodFileExists && !taxFilesExists && !regFilesExists && !prodFilesExists){
			fileNotExists(pSourceDir,pStartDate);
		}else if (deleteprodFileExists || (taxFilesExists && regFilesExists && prodFilesExists)) {
			checkForFileExists(pFilesList, taxName, regName, prodName,
					hashMap);
		} else if(!taxFilesExists){
			taxFileNotExists(pSourceDir,pStartDate);
		} else if(!regFilesExists){
			regFileNotExists(pSourceDir,pStartDate);
		} else if(!prodFilesExists){
			prodFileNotExists(pSourceDir,pStartDate);
		}
	}


	/**
	 * File not exists.
	 *
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @throws Exception the exception
	 */
	private void fileNotExists(String pSourceDir, String pStartDate) throws Exception {
		String messageSubject = getFeedEnvConfiguration().getCatNoFeedFileSubject();
		notifyFileDoseNotExits(pSourceDir, messageSubject);
		getLogger().logError(TRUFeedConstants.SPACE + TRUFeedConstants.NO_FILES_TO_DOWNLOAD);
		logFailedMessage(pStartDate, getFeedEnvConfiguration().getCatNoFeedFileSubject());
		throw new FeedSkippableException(TRUFeedConstants.FILE_DOWNLOAD_FAILED);
	}

	
	/**
	 * Prod file not exists.
	 *
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @throws Exception the exception
	 */
	private void prodFileNotExists(String pSourceDir, String pStartDate) throws Exception {
		String messageSubject = TRUProductFeedConstants.NOTAVIL_PRODCAN_DETAILEDEXP;
		notifyFileDoseNotExits(pSourceDir, messageSubject);
		getLogger().logError(TRUFeedConstants.SPACE + TRUFeedConstants.NO_FILES_TO_DOWNLOAD);
		logFailedMessage(pStartDate, TRUProductFeedConstants.NOTAVIL_PRODCAN_DETAILEDEXP+pSourceDir);
		throw new Exception(TRUFeedConstants.FILE_DOWNLOAD_FAILED);
	}

	
	/**
	 * Reg file not exists.
	 *
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @throws Exception the exception
	 */
	private void regFileNotExists(String pSourceDir, String pStartDate) throws Exception {
		String messageSubject = TRUProductFeedConstants.NOTAVIL_REG_DETAILEDEXP;
		notifyFileDoseNotExits(pSourceDir, messageSubject);
		getLogger().logError(TRUFeedConstants.SPACE + TRUFeedConstants.NO_FILES_TO_DOWNLOAD);
		logFailedMessage(pStartDate, TRUProductFeedConstants.NOTAVIL_REG_DETAILEDEXP+pSourceDir);
		throw new Exception(TRUFeedConstants.FILE_DOWNLOAD_FAILED);
	}

	
	/**
	 * Tax file not exists.
	 *
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @throws Exception the exception
	 */
	private void taxFileNotExists(String pSourceDir, String pStartDate) throws Exception {
		String messageSubject = TRUProductFeedConstants.NOTAVIL_WEBTAX_DETAILEDEXP;
		notifyFileDoseNotExits(pSourceDir, messageSubject);
		getLogger().logError(TRUFeedConstants.SPACE + TRUFeedConstants.NO_FILES_TO_DOWNLOAD);
		logFailedMessage( pStartDate, TRUProductFeedConstants.NOTAVIL_WEBTAX_DETAILEDEXP+pSourceDir);
		throw new Exception(TRUFeedConstants.FILE_DOWNLOAD_FAILED);
	}

	/**
	 * Check for file exists.
	 *
	 * @param pFilesList the files list
	 * @param pTaxName the tax name
	 * @param pRegName the reg name
	 * @param pProdName the prod name
	 * @param pHashMap the hash map
	 */
	private void checkForFileExists(StringBuffer pFilesList, String pTaxName,
			String pRegName, String pProdName, Map<Integer, String> pHashMap) {
		
		String prodName = pProdName;
		prodName = populateProductDeltaFeedName(prodName, pHashMap);
		if(StringUtils.isNotBlank(pTaxName)){
			pFilesList.append(pTaxName);
		}
		if(StringUtils.isNotBlank(pRegName)){
			pFilesList.append(pRegName);
		}
		if(StringUtils.isNotBlank(prodName)){
			pFilesList.append(prodName);
		}
		getLogger().logDebug("Final File name sequence processed : " + pFilesList);
		getLogger().logInfo("Final File name sequence processed : " + pFilesList);
		saveData(TRUFeedConstants.FEEDFILENAMES, pFilesList.toString());
	}
	
	
	/**
	 * Check file sequence.
	 *
	 * @param pFileSequenceList the file sequence list
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @return the boolean
	 * @throws Exception the exception
	 */
	private Boolean checkFileSequence(List<Integer> pFileSequenceList, String pSourceDir, String pStartDate) throws Exception{
		
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: checkFileSequence()");
		
		Boolean retVal = Boolean.FALSE;
		List<Integer> fileSequenceList = pFileSequenceList;
		if(!fileSequenceList.isEmpty()){
			Collections.sort(fileSequenceList);
			if(!checkMissingSequence(pFileSequenceList).isEmpty()){
				String messageSubject = getFeedEnvConfiguration().getCatFeedFilesNoSequence();
				notifyFileDoseNotExits(pSourceDir, messageSubject);
				getLogger().logError(TRUFeedConstants.SPACE + getFeedEnvConfiguration().getCatFeedFilesNoSequence());
				logFailedMessage(pStartDate, TRUProductFeedConstants.FILE_NO_SEQ);
				throw new FeedSkippableException(getFeedEnvConfiguration().getCatFeedFilesNoSequence());
			} else{
				RepositoryItem repoItem = getFileSequenceRepository().getItem(TRUProductFeedConstants.PRODUCT_FEED, TRUProductFeedConstants.FEED_FILE_SEQUENCE);
				if(repoItem != null){
					Integer i = (Integer) repoItem.getPropertyValue(getFeedEnvConfiguration().getSequenceProcessed());
					if(i + TRUProductFeedConstants.NUMBER_ONE != fileSequenceList.get(TRUProductFeedConstants.NUMBER_ZERO)){
						String messageSubject = getFeedEnvConfiguration().getCatFeedFilesSequenceNoMatch();
						notifyFileDoseNotExits(pSourceDir, messageSubject);
						getLogger().logError(TRUFeedConstants.SPACE + getFeedEnvConfiguration().getCatFeedFilesSequenceNoMatch());
						logFailedMessage(pStartDate, TRUProductFeedConstants.FILE_NO_SEQ);
						throw new FeedSkippableException(getFeedEnvConfiguration().getCatFeedFilesSequenceNoMatch());
					}
				}
			}
			retVal = Boolean.TRUE;
		}
		
		getLogger().vlogDebug("End @Class: TRUFileDownloadTask, @method: checkFileSequence()");
		
		return retVal;
	}
	
	/**
	 * Check missing sequence.
	 *
	 * @param pFileSequenceList the file sequence list
	 * @return the list
	 */
	private List<Integer> checkMissingSequence(List<Integer> pFileSequenceList) {
		
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: checkMissingSequence()");
		
		int j = pFileSequenceList.get(0);
		List<Integer> missing = new ArrayList<Integer>();
		int size = pFileSequenceList.size();
		for (int i=0;i < size;i++)
		{
		    if (j==pFileSequenceList.get(i))
		    {
		        j++;
		        continue;
		    }
		    else
		    {
		    	missing.add(j);
		        i--;
		    j++;
		    }
		}
		
		getLogger().vlogDebug("End @Class: TRUFileDownloadTask, @method: checkMissingSequence()");
		
		return missing;
	}

	/**
	 * Populate product delta feed name.
	 * 
	 * @param pProdName ProdName
	 * @param pMap HashMap
	 * @return the string
	 */
	private String populateProductDeltaFeedName(String pProdName, Map<Integer, String> pMap) {
		String name = pProdName;
		if (StringUtils.isBlank(name)) {
			Map<Integer, String> treeMap = new TreeMap<Integer, String>(pMap);
			if (treeMap != null) {
				for (Entry<Integer, String> entry : treeMap.entrySet()) {
					//Integer key = entry.getKey();
					String value = entry.getValue();
					if (StringUtils.isBlank(name)) {
						name = value + TRUFeedConstants.DOLLAR_ONLY;
					} else {
						name = name + value + TRUFeedConstants.DOLLAR_ONLY;
					}
				}
			}
		}
		return name;
	}

	
	/**
	 * Movefiles.
	 *
	 * @param pProcessingFolder the processing folder
	 * @param pSourceFolder the source folder
	 * @param pFilesList the files list
	 * @param pSourceDir the source dir
	 * @param pStartDate the start date
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	private boolean movefiles(File pProcessingFolder, File pSourceFolder, StringBuffer pFilesList, String pSourceDir, String pStartDate)
			throws Exception {
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: movefiles()");
		boolean feedfilesExists = Boolean.FALSE;
		if (pProcessingFolder != null && pProcessingFolder.exists() && pProcessingFolder.isDirectory() && pProcessingFolder.canWrite()) {
			try {
				feedfilesExists = moveAllFiles(pSourceFolder, pProcessingFolder.getCanonicalPath(), pFilesList);
				pFilesList.setLength(0);
				if (feedfilesExists) {
					getLogger().logInfo(TRUFeedConstants.SPACE + TRUFeedConstants.DOWNLOAD_SUCCESS);
				} else {
					fileNotExists(pSourceDir, pStartDate);
				}
			} catch (FileNotFoundException e) {
				if (isLoggingError()) {
					logError(TRUFeedErrorConstants.FILE_NOT_FOUND_EXCEPTION, e);
				}
				logFailedMessage(pStartDate, e.getMessage());
			} catch (IOException e) {
				if (isLoggingError()) {
					logError(TRUFeedErrorConstants.IOEXCEPTION, e);
				}
				logFailedMessage( pStartDate, e.getMessage());
			}
		} else if(!pProcessingFolder.exists() || !pProcessingFolder.isDirectory()){
			String messageSubject = TRUProductFeedConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(pProcessingFolder.getPath(), messageSubject);
			logFailedMessage(pStartDate, TRUProductFeedConstants.FOLDER_NOT_EXISTS);
			throw new FeedSkippableException(TRUProductFeedConstants.FOLDER_NOT_EXISTS);
		} else if(!pProcessingFolder.canRead()){
			String messageSubject = TRUProductFeedConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(pProcessingFolder.getPath(), messageSubject);
			logFailedMessage(pStartDate, TRUProductFeedConstants.PERMISSION_ISSUE);
			throw new FeedSkippableException(TRUProductFeedConstants.PERMISSION_ISSUE);
		}
		getLogger().vlogDebug("End @Class: TRUFileDownloadTask, @method: movefiles()");
		return feedfilesExists;

	}

	/**
	 * File lists.
	 * 
	 * @param pPath
	 *            - pPath
	 * @param pPattern
	 *            - pPattern
	 * @return the patternMatchfiles
	 */
	private List<File> fileLists(String pPath, String pPattern) {
		List<File> patternMatchingfiles = new ArrayList<File>();
		File dir = new File(pPath);
		getLogger().vlogDebug(pPattern);
		String filepattern[] = pPattern.split(TRUProductFeedConstants.TILDE);
		for (String pattern : filepattern) {
			getLogger().vlogDebug(pattern);
			FileFilter filter = new RegexFileFilter(pattern);
			File[] patternMatchfiles = dir.listFiles(filter);
			if (patternMatchfiles != null) {
				for (File files : patternMatchfiles) {
					getLogger().vlogDebug(files.getName());
					patternMatchingfiles.add(files);
				}
			}
		}
		return patternMatchingfiles;
	}

	/**
	 * Move all files.
	 * 
	 * @param pSharedDir
	 *            the shared dir
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pFilesList
	 *            the files list
	 * @return true, if successful
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean moveAllFiles(File pSharedDir, String pProcessingDir, StringBuffer pFilesList)
			throws FileNotFoundException, IOException {
		String fileNames = null;
		boolean fileMove = Boolean.FALSE;
		if (pSharedDir.exists() && pSharedDir.isDirectory()) {
			fileNames = pFilesList.toString();
		}
		String[] feedNames = fileNames.split(TRUFeedConstants.DOLLAR);
		if (feedNames.length > 0) {
			for (String feedsName : feedNames) {
				// move files to the folder
				if (!StringUtils.isBlank(feedsName)) {
					fileMove = moveFileToFolder(pSharedDir + TRUFeedConstants.SLASH + feedsName, pProcessingDir);
				}
			}
		}
		return fileMove;
	}

	/**
	 * This method is used for moving the file from source directory to destination directory.
	 * 
	 * @param pFromFile
	 *            - String
	 * @param pToFolder
	 *            - String
	 * @return boolean - value
	 * @throws FileNotFoundException
	 *             - Throws FileNotFoundException
	 * @throws IOException
	 *             - Throws IOException
	 */
	public static boolean moveFileToFolder(String pFromFile, String pToFolder) throws FileNotFoundException, IOException {
		File fromFile = new File(pFromFile);
		File toFolder = new File(pToFolder);
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		boolean value = fromFile.renameTo(new File(toFolder, fromFile.getName()));
		return value;
	}

	/**
	 * Notify file dose not exits.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 * @param pMessageSubject
	 *            the message subject
	 */
	public void notifyFileDoseNotExits(String pSourceDir, String pMessageSubject) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");
		
		String priority = null;
		String content = null;
		String detailException = null;
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		if(pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getCatFeedFilesNoSequence()) || pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getCatFeedFilesSequenceNoMatch())){
			content = TRUProductFeedConstants.JOB_FAILED_MISSING_SEQ;
			detailException = TRUProductFeedConstants.SEQ_DETAILEXP;
		}else if(pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getCatNoFeedFileSubject())){
			content = TRUProductFeedConstants.JOB_FAILED_MISSING_FILE;
			detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		}else if(pMessageSubject.contains(TRUProductFeedConstants.NOTAVIL_WEBTAX_DETAILEDEXP)){
			content = TRUProductFeedConstants.JOB_FAILED + getFeedEnvConfiguration().getWebsiteFeedName() +TRUProductFeedConstants.NOT_AVAIL+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()))+ TRUProductFeedConstants.AT +pSourceDir;
			detailException = TRUProductFeedConstants.NOTAVIL_WEBTAX_DETAILEDEXP + pSourceDir;
		}else if(pMessageSubject.contains(TRUProductFeedConstants.NOTAVIL_REG_DETAILEDEXP)){
			content = TRUProductFeedConstants.JOB_FAILED + getFeedEnvConfiguration().getRegistryFeedName() +TRUProductFeedConstants.NOT_AVAIL+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()))+ TRUProductFeedConstants.AT +pSourceDir;
			detailException = TRUProductFeedConstants.NOTAVIL_REG_DETAILEDEXP + pSourceDir;
		}else if(pMessageSubject.contains(TRUProductFeedConstants.NOTAVIL_PRODCAN_DETAILEDEXP)){
			content = TRUProductFeedConstants.JOB_FAILED + getFeedEnvConfiguration().getCatProdFeedName() +TRUProductFeedConstants.NOT_AVAIL+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()))+ TRUProductFeedConstants.AT +pSourceDir;
			detailException = TRUProductFeedConstants.NOTAVIL_PRODCAN_DETAILEDEXP + pSourceDir;
		}else if(pMessageSubject.contains(TRUProductFeedConstants.FOLDER_NOT_EXISTS)){
			content = TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_1 + pSourceDir + TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
			detailException = pSourceDir + TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
		}else if(pMessageSubject.contains(TRUProductFeedConstants.PERMISSION_ISSUE)){
			content = TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_1 + pSourceDir + TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_2;
			detailException = pSourceDir + TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_2;
		}
		
		priority = TRUProductFeedConstants.SEQ_MISS;
		String message = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, message);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.CAT_JOB_NAME, templateParameters);

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");
	}

}
