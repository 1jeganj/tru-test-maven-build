/**
 * 
 */
package com.tru.feedprocessor.tol.base.tasklet.support;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class TRUProjectNameGenerator.
 * This class is used to generate the project name specific to catalog feed
 *	@version 1.0
 *  @author Professional Access
 */
public class TRUCatProjectNameGenerator extends ProjectNameGenerator {
	
	/** The feed name. */
	private String mFeedName;

	/**
	 * This method is used to generate the project name specific to catalog feed.
	 * 
	 * @param pFeedExecCntx
	 *            the feed exec cntx
	 * @return the project name
	 */
	@Override
	public String getProjectName(FeedExecutionContextVO pFeedExecCntx) {

		Calendar lCal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat(TRUFeedConstants.TIMESTAMP_FORMAT_FOR_IMPORT, Locale.getDefault());
		StringBuffer projectName = new StringBuffer();
		projectName.append(getFeedName()).append(FeedConstants.SPACE).append(dateFormat.format(lCal.getTime()).toString());
		return projectName.toString();

	}

	/**
	 * Gets the feed name.
	 * 
	 * @return the mFeedName
	 */
	public String getFeedName() {
		return mFeedName;
	}

	/**
	 * Sets the feed name.
	 * 
	 * @param pFeedName
	 *            the new feed name
	 */
	public void setFeedName(String pFeedName) {
		this.mFeedName = pFeedName;
	}

}
