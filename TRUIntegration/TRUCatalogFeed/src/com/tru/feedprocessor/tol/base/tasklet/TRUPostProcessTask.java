package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;

import atg.core.util.StringUtils;
import atg.repository.RepositoryImpl;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * class is used to move the unprocessed files from source to unprocessed directory The Class TRUPostProcessTask.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUPostProcessTask extends PostProcessTask {

	/**
	 * method is used to move the unprocessed files from source to unprocessed directory .
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {

		RepositoryImpl catalogRepository = (RepositoryImpl) getRepository(TRUFeedConstants.CATALOG_REPOSITORY);
		catalogRepository.invalidateCaches();
		RepositoryImpl catalogRepositoryVer = (RepositoryImpl) getRepository(TRUFeedConstants.CATALOG_REPOSITORY_VER);
		catalogRepositoryVer.invalidateCaches();

		File sourceDir = new File((String) (getConfigValue(TRUFeedConstants.SOURCE_DIR).toString()));
		String fileType = (String) getJobParameter(TRUProductFeedConstants.FILE_TYPE);
		if(StringUtils.isNotBlank(fileType) && fileType.equals(TRUProductFeedConstants.DELETE)){
			
			sourceDir = new File((String)(getConfigValue(TRUProductFeedConstants.DELETE_SOURCE_DIRECTORY).toString()));
		}
		File toFolder = new File((String) (getConfigValue(TRUFeedConstants.UNPROCCESSED_DIRECTORY).toString()));
		if (sourceDir.exists() && sourceDir.isDirectory() && sourceDir.canRead()) {
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}
			File[] processFiles = sourceDir.listFiles();
			if (processFiles != null && processFiles.length > 0) {
				for (File file : processFiles) {
					if (file.isFile()) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
			}
		}
	}
}
