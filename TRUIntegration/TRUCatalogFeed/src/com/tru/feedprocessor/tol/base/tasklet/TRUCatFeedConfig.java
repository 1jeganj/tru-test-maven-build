/*
 * 
 */
package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedErrorConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.catalog.CatFeedConfig;

/**
 * The Class TRUCatFeedConfig. It Will contain all the properties required for the catalog feed processing.
 * 
 * @author Professional Access.
 * @version 1.0
 */
public class TRUCatFeedConfig extends CatFeedConfig {

	/** Property to hold Error folder. */
	private String mErrorFolder;
	/** Property to hold Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;

	/** Property to hold catalog previous day file path. */
	private String mCatalogPreviousDayFilePath;

	/** Property to hold catalog previous day file name. */
	private Map<String, String> mCatalogPreviousDayFileName;

	/** Property to hold Unproccessed directory. */
	private String mUnproccessedDirectory;

	/** The Bad directory. */
	private String mBadDirectory;

	/** The Processing directory. */
	private String mProcessingDirectory;

	/** The Success directory. */
	private String mSuccessDirectory;

	/** The Error directory. */
	private String mErrorDirectory;

	/** The mInventoryFlag. */
	private Boolean mInventoryFlag;
	
	/** The Delete source directory. */
	private String mDeleteSourceDirectory;
	
	/** The Delete processing directory. */
	private String mDeleteProcessingDirectory;
	
	/** The Empty sku properties flag. */
	private Boolean mEmptySkuPropertiesFlag = Boolean.FALSE;

	/**
	 * Checks if is inventory flag.
	 * 
	 * @return the inventoryFlag
	 */
	public Boolean getInventoryFlag() {
		return mInventoryFlag;
	}

	/**
	 * Sets the inventory flag.
	 * 
	 * @param pInventoryFlag
	 *            the inventoryFlag to set
	 */
	public void setInventoryFlag(Boolean pInventoryFlag) {
		mInventoryFlag = pInventoryFlag;
		setConfigValue(TRUProductFeedConstants.INVENTORY_FLAG, pInventoryFlag);
	}

	/**
	 * Gets the bad directory.
	 * 
	 * @return the badDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}

	/**
	 * Gets the processing directory.
	 * 
	 * @return the processingDirectory
	 */
	public String getProcessingDirectory() {
		return mProcessingDirectory;
	}

	/**
	 * Gets the success directory.
	 * 
	 * @return the successDirectory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}

	/**
	 * Gets the error directory.
	 * 
	 * @return the errorDirectory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}

	/**
	 * Gets the error file path.
	 * 
	 * @return the ErrorFolder
	 */
	public String getErrorFolder() {
		return mErrorFolder;
	}

	/**
	 * Sets the error file path.
	 * 
	 * @param pErrorFolder
	 *            the new file path
	 */
	public void setErrorFolder(String pErrorFolder) {
		mErrorFolder = pErrorFolder;
		setConfigValue(TRUFeedConstants.ERROR_FOLDER, pErrorFolder);
	}

	/**
	 * Sets the processing directory.
	 * 
	 * @param pProcessingDirectory
	 *            the new processing directory
	 */
	public void setProcessingDirectory(String pProcessingDirectory) {
		mProcessingDirectory = pProcessingDirectory;
		setConfigValue(TRUProductFeedConstants.PROCESSING_DIRECTORY, pProcessingDirectory);
	}

	/**
	 * Sets the bad directory.
	 * 
	 * @param pBadDirectory
	 *            the new bad directory
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
		setConfigValue(TRUProductFeedConstants.BAD_DIRECTORY, pBadDirectory);
	}

	/**
	 * Sets the success directory.
	 * 
	 * @param pSuccessDirectory
	 *            the new success directory
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
		setConfigValue(TRUProductFeedConstants.SUCCESS_DIRECTORY, pSuccessDirectory);
	}

	/**
	 * Sets the error directory.
	 * 
	 * @param pErrorDirectory
	 *            the new error directory
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
		setConfigValue(TRUProductFeedConstants.ERROR_DIRECTORY, pErrorDirectory);
	}

	/**
	 * This method will check whether all the required properties are there or not. if the required property is not there
	 * then it will throw an exception.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {

		try {
			if (StringUtils.isBlank(getConfigValue(FeedConstants.FILENAME).toString())) {
				getLogger().logError(TRUFeedErrorConstants.FILE_NAME_NOT_NULL);
			}
			if (StringUtils.isBlank(getConfigValue(FeedConstants.FILEPATH).toString())) {
				getLogger().logError(TRUFeedErrorConstants.FILE_PATH_NOT_NULL);
			}
			if (StringUtils.isBlank(getConfigValue(FeedConstants.ENTITY_LIST).toString()) && StringUtils.isBlank(getConfigValue(FeedConstants.STEP_ENTITY_LIST).toString())) {
				getLogger().logError(TRUFeedErrorConstants.ENTITY_LIST_NOT_NULL);
			}
			if (StringUtils.isBlank(getConfigValue(FeedConstants.ENTITY_SQL).toString())) {
				getLogger().logError(TRUFeedErrorConstants.ENTIY_SQL_NOT_NULL);
			}
			if (StringUtils.isBlank(getConfigValue(FeedConstants.ENTITYTOITEMDESCRIPTONNAMEMAP).toString())) {
				getLogger().logError(TRUFeedErrorConstants.ENTITY_ITEMDESCRIPTOR_NOT_NULL);
			}
			if (StringUtils.isBlank(getConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP).toString())) {
				getLogger().logError(TRUFeedErrorConstants.ENTITY_TO_REPOSITORY_NAME_MAP);
			}
			String voName = (String) getConfigValue(FeedConstants.DATA_LOADER_CLASS_FQCN);
			if (StringUtils.isBlank(voName)) {
				getLogger().logError(TRUFeedErrorConstants.FILE_LOADER_FQCN);
			}
			try {
				Class.forName(voName);
			} catch (ClassNotFoundException cnfe) {
				if (isLoggingError()) {
					logError(TRUFeedErrorConstants.FILE_PARSE_FQCN, cnfe);
				}
			}

			for (String key : ((List<String>) getConfigValue(FeedConstants.ENTITY_LIST))) {
				if (!((Map<String, String>) getConfigValue(FeedConstants.ENTITY_SQL)).containsKey(key)) {
					getLogger().logError(
							TRUFeedErrorConstants.SQL_MANADATORY + key + TRUFeedErrorConstants.ENTITY_LIST_CAT_FEED);
				}

			}

			if (!StringUtils.isBlank(getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).toString()) && getConfigValue(FeedConstants.FILE_DOWNLOADER_FLAG).equals(Boolean.toString(FeedConstants.TRUE))) {

				if (StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_HOST).toString())) {
					getLogger().logError(TRUFeedErrorConstants.HOSTNAME_FTP_SFTP);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.FTP_SFTP_PORT).toString())) {
					getLogger().logError(TRUFeedErrorConstants.PORT_NO_FTP_SFTP);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_USER).toString())) {
					getLogger().logError(TRUFeedErrorConstants.SERVER_IP_FTP_SFTP);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_PASSWORD).toString())) {
					getLogger().logError(TRUFeedErrorConstants.SERVER_IP_FTP_SFTP);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString())) {
					getLogger().logError(TRUFeedErrorConstants.SERVER_IP_FTP_SFTP);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.FILEPATH).toString())) {
					getLogger().logError(TRUFeedErrorConstants.FILE_PATH_NOT_NULL);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.PROTOCOL_TYPE).toString())) {
					getLogger().logError(TRUFeedErrorConstants.PROTOCOL_FTP_SFTP_NOTNULL);
				}
				if (StringUtils.isBlank(getConfigValue(FeedConstants.DOWNLOAD_FILETYPE).toString())) {
					getLogger().logError(TRUFeedErrorConstants.DOWNLOAD_FILE_TYPE_NOT_NULL);
				}
			}

		} catch (Exception e) {
			if (isLoggingError()) {
				logError(TRUFeedErrorConstants.CAT_FEED_CONFIG, e);
			}
		}

	}

	/**
	 * Gets the catalog previous day file path.
	 * 
	 * @return the catalog previous day file path
	 */
	public String getCatalogPreviousDayFilePath() {
		return mCatalogPreviousDayFilePath;
	}

	/**
	 * Sets the catalog previous day file path.
	 * 
	 * @param pCatalogPreviousDayFilePath
	 *            the new catalog previous day file path
	 */
	public void setCatalogPreviousDayFilePath(String pCatalogPreviousDayFilePath) {
		this.mCatalogPreviousDayFilePath = pCatalogPreviousDayFilePath;
		setConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_PATH, pCatalogPreviousDayFilePath);
	}

	/**
	 * Gets the catalog previous day file name.
	 * 
	 * @return the catalog previous day file name
	 */
	public Map<String, String> getCatalogPreviousDayFileName() {
		return mCatalogPreviousDayFileName;
	}

	/**
	 * Sets the catalog previous day file name.
	 * 
	 * @param pCatalogPreviousDayFileName
	 *            the catalog previous day file name
	 */
	public void setCatalogPreviousDayFileName(Map<String, String> pCatalogPreviousDayFileName) {
		mCatalogPreviousDayFileName = pCatalogPreviousDayFileName;
		setConfigValue(TRUFeedConstants.CATALOG_PREVIOUS_DAY_FILE_NAME, pCatalogPreviousDayFileName);
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
		if (null != getFeedEnvConfiguration()) {
			setConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P3, getFeedEnvConfiguration()
					.getLowerCutoffAlertPriority3());
			setConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P3, getFeedEnvConfiguration()
					.getUpperCutoffAlertPriority3());
			setConfigValue(TRUFeedConstants.MIN_PRIORITY_THRESHOLD_P2, getFeedEnvConfiguration()
					.getLowerCutoffAlertPriority2());
			setConfigValue(TRUFeedConstants.MAX_PRIORITY_THRESHOLD_P2, getFeedEnvConfiguration()
					.getUpperCutoffAlertPriority2());
		}
	}

	/**
	 * Gets the unproccessed directory.
	 * 
	 * @return the mUnproccessedDirectory
	 */
	public String getUnproccessedDirectory() {
		return mUnproccessedDirectory;
	}

	/**
	 * Sets the unproccessed directory.
	 * 
	 * @param pUnproccessedDirectory
	 *            the new unproccessed directory
	 */
	public void setUnproccessedDirectory(String pUnproccessedDirectory) {
		this.mUnproccessedDirectory = pUnproccessedDirectory;
		setConfigValue(TRUFeedConstants.UNPROCCESSED_DIRECTORY, pUnproccessedDirectory);
	}

	/**
	 * Gets the delete source directory.
	 *
	 * @return the delete source directory
	 */
	public String getDeleteSourceDirectory() {
		return mDeleteSourceDirectory;
	}

	/**
	 * Sets the delete source directory.
	 *
	 * @param pDeleteSourceDirectory the new delete source directory
	 */
	public void setDeleteSourceDirectory(String pDeleteSourceDirectory) {
		mDeleteSourceDirectory = pDeleteSourceDirectory;
		setConfigValue(TRUProductFeedConstants.DELETE_SOURCE_DIRECTORY, pDeleteSourceDirectory);
	}

	/**
	 * Gets the delete processing directory.
	 *
	 * @return the delete processing directory
	 */
	public String getDeleteProcessingDirectory() {
		return mDeleteProcessingDirectory;
	}

	/**
	 * Sets the delete processing directory.
	 *
	 * @param pDeleteProcessingDirectory the new delete processing directory
	 */
	public void setDeleteProcessingDirectory(String pDeleteProcessingDirectory) {
		mDeleteProcessingDirectory = pDeleteProcessingDirectory;
		setConfigValue(TRUProductFeedConstants.DELETE_PROCESSING_DIRECTORY, pDeleteProcessingDirectory);
	}
	
	/**
	 * Gets the empty sku properties flag.
	 *
	 * @return the empty sku properties flag
	 */
	public Boolean getEmptySkuPropertiesFlag() {
		return mEmptySkuPropertiesFlag;
	}

	/**
	 * Sets the empty sku properties flag.
	 *
	 * @param pEmptySkuPropertiesFlag the new empty sku properties flag
	 */
	public void setEmptySkuPropertiesFlag(Boolean pEmptySkuPropertiesFlag) {
		mEmptySkuPropertiesFlag = pEmptySkuPropertiesFlag;
		setConfigValue(TRUProductFeedConstants.EMPTY_SKU_PROPERTIES_FLAG, pEmptySkuPropertiesFlag);
	}

}
