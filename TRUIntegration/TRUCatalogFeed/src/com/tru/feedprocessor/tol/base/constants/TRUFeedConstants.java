package com.tru.feedprocessor.tol.base.constants;

/**
 * The Class TRUFeedConstants. Class contains the constants related to Catalog feed.
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedConstants {
	/** The Constant SOURCE_DIR. */
	public static final String SOURCE_DIR = "sourceDirectory";
	/** 
	 * The Constant BAD_DIRECTORY. 
	 */
	public final static String BAD_DIRECTORY="badDirectory";
	/** The Constantn FILEPATH. */
	public static final String FILEPATH = "filePath";
	/** The Constantn FILEPATH. */
	public static final String ERROR_FOLDER = "errorFolder";
	/** The Constantn FILETYPE. */
	public static final String FILETYPE = "fileType";
	/**
	 * The Constant SLASH.
	 */
	public static final String SLASH = "/";
	/**
	 * The Constant DOUBLE_SLASH.
	 */
	public static final String DOUBLE_SLASH_DOT = "\\.";

	/** The constant doubleslash. */
	public static final String DOUBLE_SLASH = "\\";
	/** The Constantn MOVEMENT_TO_FOLDER_FAILED. */
	public static final String MOVEMENT_TO_FOLDER_FAILED = "movement to folder failed";
	/** The Constant for Space. */
	public static final String SPACE = " ";

	/** The Constantn FORWARD_SLASH. */
	public static final String FORWARD_SLASH = "/";
	/** The Constant DOWNLOAD_SUCCESS. */
	public final static String DOWNLOAD_SUCCESS = "FILES DOWNLOAD_SUCCESS ";

	/** The Constant DOUBLE_BACKWARDSLASH_DOT. */
	public static final String DOUBLE_BACKWARDSLASH_DOT = "\\.";
	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;

	/** The Constant FILE_DOWNLOAD_FAILED. */

	/** The Constant FILE_DOWNLOAD_FAILED. */
	public static final String FILE_DOWNLOAD_FAILED = "File Download Failed...";
	/** The Constant NO_FILES_TO_DOWNLOAD. */
	public static final String NO_FILES_TO_DOWNLOAD = "NO_FILES_TO_DOWNLOAD";
	/** The Constant CATEGORY_NAME. */
	public static final String CATEGORY_NAME = "category";

	/** property to hold highPriorityEmail string. */
	public static final String HIGH_PRIORITY_EMAIL = "highPriorityEmail";

	/** property to hold boolean true value. */
	public static final String TRUE = "true";

	/** property to hold constant errorMessage. */
	public static final String ERROR_MESSAGE = "errorMessage";

	/** property to hold constant backwardslash. */
	public static final String FOUR_BACKWARDSLASH = "\\\\";
	/**
	 * property to hold dateFormat.
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** property to show the error if email fails. */
	public static final String EMAIL_ERROR = "Exception occured while sending a success email : ";

	/** property to hold ACCESS_DENIED. */
	public static final String ACCESS_DENIED = "Access Denied";

	/** property to hold UTF_16. */
	public static final String UTF_16 = "UTF-16";

	/** property to hold FOLDER_NAME. */
	public static final String FOLDER_NAME = "folderName";

	/** property to hold CLASSIFICATION. */
	public static final String CLASSIFICATION = "Classification";

	/** property to hold FOLDER_NAME. */
	public static final String USER_TYPE_ID = "UserTypeID";

	/** property to hold US_REGISTRY_CAT. */
	public static final String US_REGISTRY_CAT = "US_Registry_Cat";

	/** property to hold PRIMARY_CATEGORY. */
	public static final String PRIMARY_CATEGORY = "Primary Category";

	/** property to hold SUB_CATEGORY. */
	public static final String SUB_CATEGORY = "Sub Category";

	/** property to hold ID. */
	public static final String ID = "ID";

	/** property to hold NAME. */
	public static final String NAME = "Name";

	/** property to hold ATTRIBUTE_ID. */
	public static final String ATTRIBUTE_ID = "AttributeID";

	/** property to hold CROSS_REFERRENCE. */
	public static final String CROSS_REFERRENCE = "ClassificationCrossReference";

	/** property to hold TYPE. */
	public static final String TYPE = "Type";

	/** property to hold RCGSN. */
	public static final String RCGSN = "RCGSN";

	/** property to hold CLASSIFICATION_ID. */
	public static final String CLASSIFICATION_ID = "ClassificationID";

	/** property to hold UNDER_SCORE. */
	public static final String UNDER_SCORE = "_";

	/** property to hold VALUE. */
	public static final String VALUE = "Value";

	/** property to hold VALUE. */
	public static final String DISPLAY_ORDER = "150016";

	/** property to hold VALUE. */
	public static final String SUGGESTED_QTY = "150018";

	/** property to hold VALUE. */
	public static final String MUST_OR_NICE = "150015";

	/** property to hold VALUE. */
	public static final String IMAGE_PATH = "150020";

	/** property to hold VALUE. */
	public static final String REGISTRY = "Registry";

	/** property to hold DOLLAR. */
	public static final String DOLLAR = "\\$";

	/** property to hold WEB_SITE_TAXONOMY. */
	public static final String WEB_SITE_TAXONOMY = "WebSiteTaxonomy";

	/** property to hold REGISTRY_CATEGORIZATION. */
	public static final String REGISTRY_CATEGORIZATION = "RegistryCategorization";

	/** property to hold ATG_PRODUCT_FULL. */
	public static final String ATG_PRODUCT = "ATG_Product";

	/** property to hold CLASSIFICATION_ENTITY. */
	public static final String CLASSIFICATION_ENTITY = "classification";
	
	/** property to hold CLASSIFICATION_ENTITY. */
	public static final String REGISTRY_CLASSIFICATION_ENTITY = "registryClassification";

	/** property to hold DISPLAY_STATUS. */
	public static final String DISPLAY_STATUS = "150021";

	/** property to hold YES. */
	public static final String YES = "Y";

	/** property to hold NO. */
	public static final String NO = "N";

	/** property to hold ACTIVE. */
	public static final String ACTIVE = "ACTIVE";

	/** property to hold HIDDEN. */
	public static final String HIDDEN = "HIDDEN";

	/** The Constant DOUBLE_BACKWARDSLASH. */
	public static final String DOUBLE_BACKWARDSLASH = "\\";

	/** property to hold SITETAXONOMYROOT. */
	public static final String SITETAXONOMYROOT = "SITETAXONOMYROOT";

	/** property to hold TAXONOMYPARENT. */
	public static final String TAXONOMYPARENT = "TAXONOMYPARENT";

	/** property to hold TAXONOMYCATEGORY. */
	public static final String TAXONOMYCATEGORY = "TAXONOMYCATEGORY";
	
	/** property to hold TAXONOMYSUBCATEGORY. */
	public static final String TAXONOMYSUBCATEGORY = "TAXONOMYSUBCATEGORY";

	/** property to hold TAXONOMYCOLLECTION. */
	public static final String TAXONOMYCOLLECTION = "TAXONOMYCOLLECTION";

	/** property to hold TAXONOMYNODE. */
	public static final String TAXONOMYNODE = "TAXONOMYNODE";

	/** property to hold CLASSIFICATIONCROSSREFERENCE. */
	public static final String CLASSIFICATIONCROSSREFERENCE = "ClassificationCrossReference";

	/** property to hold METADATA. */
	public static final String METADATA = "MetaData";

	/** property to hold DERIVED. */
	public static final String DERIVED = "Derived";

	/** property to hold NAVIGATION_TRU. */
	public static final String NAVIGATION_TRU = "Navigation_TRU";

	/** property to hold NAVIGATION_BRU. */
	public static final String NAVIGATION_BRU = "Navigation_BRU";

	/** property to hold NAVIGATION_FAO. */
	public static final String NAVIGATION_FAO = "Navigation_FAO";

	/** property to hold CLASSIFICATION_1_ROOT. */
	public static final String CLASSIFICATION_1_ROOT = "Classification 1 root";

	/** property to hold SEVEN_NINE_NINE_TWO. */
	public static final String SEVEN_NINE_NINE_TWO = "7992";

	/** property to hold SEVEN_NINE_NINE_NINE. */
	public static final String SEVEN_NINE_NINE_NINE = "7999";

	/** property to hold EIGHT_ZERO_ZERO_ZERO. */
	public static final String EIGHT_ZERO_ZERO_ZERO = "8000";

	/** property to hold SEVEN_NINE_NINE_EIGHT. */
	public static final String SEVEN_NINE_NINE_EIGHT = "7998";
	
	/** property to hold SEVEN_NINE_NINE_FOUR. */
	public static final String SEVEN_NINE_NINE_FOUR = "7994";

	/** property to hold WEBSITE_NAVIGATION_HIERARCHY. */
	public static final String WEBSITE_NAVIGATION_HIERARCHY = "Website_Navigation_Hierarchy";

	/** property to hold ONE. */
	public static final int ONE = 1;

	/** property to hold INVALID_TAXONOMY. */
	public static final String INVALID_RECORD_TAXONOMY = "Invalid Taxonomy Record:";

	/** property to hold INVALID_RECORD_REGISTRY. */
	public static final String INVALID_RECORD_REGISTRY = "Invalid Registry Record:";
	/**
	 * property to hold DOUBLE_BACKWARDSLASH_UNDERSCORE.
	 */
	public static final String DOUBLE_BACKWARDSLASH_UNDERSCORE = "\\_";
	/**
	 *  property to hold FEED_DATE_FORMAT.
	 */
	public static final String FEED_DATE_FORMAT = "yyyy-MM-ddhhmmss";
	/**
	 * property to hold DOLLAR_ONLY.
	 */
	public static final String DOLLAR_ONLY = "$";
	/**
	 * property to hold FEEDFILENAMES.
	 */
	public static final String FEEDFILENAMES = "filesNames";
	/** The Constant CATALOG_PREVIOUS_DAY_FILE_NAME. */
	public static final String CATALOG_PREVIOUS_DAY_FILE_NAME = "CatalogPreviousDayFileName";
	
	/** The Constant CATALOG_PREVIOUS_DAY_FILE_PATH. */
	public static final String CATALOG_PREVIOUS_DAY_FILE_PATH = "CatalogPreviousDayFilePath";

	/** The Constant P_THREE_ALERT. */
	public static final String REGISTRY_P_THREE_ALERT = "Priority3";
	
	/** The Constant P_TWO_ALERT. */
	public static final String REGISTRY_P_TWO_ALERT = "Priority2";
	
	/** The Constant ALERT. */
	public static final String REGISTRY_ALERT="RegistryALert";
	
	/** The Constant P_THREE_ALERT. */
	public static final String TAXONOMY_P_THREE_ALERT = "Priority3";
	
	/** The Constant P_TWO_ALERT. */
	public static final String TAXONOMY_P_TWO_ALERT = "Priority2";
	
	/** The Constant ALERT. */
	public static final String TAXONOMY_ALERT="TaxonomyAlert";
	
	/** The Constant REGISTRY_THRESHOLD_LIMIT_EXCEDED. */
	public static final String REGISTRY_THRESHOLD_LIMIT_EXCEDED = "Registry Threshold Limit Exceded";
	
	/** The Constant TAXONOMY_THRESHOLD_LIMIT_EXCEDED. */
	public static final String TAXONOMY_THRESHOLD_LIMIT_EXCEDED = "Taxonomy Threshold Limit Exceded";

	/** The Constant CATALOG_JOB_NAME. */
	public static final String CATALOG_JOB_NAME = "catalogFeed";

	/** The Constant REGISTRY_RECORD_COUNT. */
	public static final String REGISTRY_RECORD_COUNT = "registryRecordCount";

	/** The Constant TAXONOMY_RECORD_COUNT. */
	public static final String TAXONOMY_RECORD_COUNT = "taxonomyRecordCount";
	
	/** The Constant MIN_PRIORITY_THRESHOLD_P3. */
	public static final String MIN_PRIORITY_THRESHOLD_P3 = "minPriorityThresholdP3";
	
	/** The Constant MAX_PRIORITY_THRESHOLD_P3. */
	public static final String MAX_PRIORITY_THRESHOLD_P3 = "maxPriorityThresholdP3";
	
	/** The Constant MIN_PRIORITY_THRESHOLD_P2. */
	public static final String MIN_PRIORITY_THRESHOLD_P2 = "minPriorityThresholdP2";
	
	/** The Constant MAX_PRIORITY_THRESHOLD_P2. */
	public static final String MAX_PRIORITY_THRESHOLD_P2 = "maxPriorityThresholdP2";
	
	/** The Constant TAXONOMY_P_ONE_ALERT. */
	public static final String TAXONOMY_P_ONE_ALERT = "Priority1";
	
	/** The Constant REGISTRY_P_ONE_ALERT. */
	public static final String REGISTRY_P_ONE_ALERT = "Priority1";

	/** property to hold NUMBER_HUNDRED. */
	public static final long NUMBER_HUNDRED = 100;
	/** The Constant MIN_PRIORITY_THRESHOLD_P3. */
	public static final String MIN_EXCPETION_PRIORITY_THRESHOLD_P3 = "minExcpetionPriorityThresholdP3";
	
	/** The Constant PRODUCT_CANONICAL_FILE. */
	public static final String PRODUCT_CANONICAL_FILE = " -- Product Canonical File";

	/** The Constant REGISTRY_FILE. */
	public static final String REGISTRY_FILE = " -- Registry File";

	/** The Constant TAXANOMY_FILE. */
	public static final String TAXANOMY_FILE = " -- Taxanomy File";
	/** The Constant MAX_PRIORITY_THRESHOLD_P3. */
	public static final String MAX_EXCPETION_PRIORITY_THRESHOLD_P3 = "maxExcpetionPriorityThresholdP3";
	
	/** The Constant MIN_PRIORITY_THRESHOLD_P2. */
	public static final String MIN_EXCPETION_PRIORITY_THRESHOLD_P2 = "minExcpetionPriorityThresholdP2";
	
	/** The Constant MAX_PRIORITY_THRESHOLD_P2. */
	public static final String MAX_EXCPETION_PRIORITY_THRESHOLD_P2 = "maxExcpetionPriorityThresholdP2";

	/** The Constant REGULAR_ALERT. */
	public static final String REGULAR_ALERT="Regular";
	
	/** The Constant CAT_FEED_LOAD_DATA_STEP. */
	public static final String CAT_FEED_LOAD_DATA_STEP="catFeedLoadDataStep";
	
	/** The Constant CAT_FEEDADD_UPDATE_ENTITY_STEP. */
	public static final String CAT_FEEDADD_UPDATE_ENTITY_STEP="catFeedAddUpdateEntityStep";
	/** The Constant UNPROCCESSED_DIRECTORY. */
	public static final String UNPROCCESSED_DIRECTORY="unproccessedDirectory";
	/** The Constant FILE_DOES_NOT_EXISTS. */
	public static final String FILE_DOES_NOT_EXISTS = "file does not exists";
	/** The Constant EXCEPTION. */
	public static final String EXCEPTION = "Exception";
	/** The Constant FILENAME. */
	public static final String FILENAME = "fileName";
	/** The Constant FILE_DOES_NOT_EXISTS_TAXONOMY. */
	public static final String FILE_DOES_NOT_EXISTS_TAXONOMY = "Taxnomy File does not exists";
	/** The Constant FILE_DOES_NOT_EXISTS_REGISTRY. */
	public static final String FILE_DOES_NOT_EXISTS_REGISTRY = "Registry File does not exists";
	/** The Constant FILE_DOES_NOT_EXISTS_PRODUCT. */
	public static final String FILE_DOES_NOT_EXISTS_PRODUCT = "Product File does not exists";
	
	/** The Constant INVALID_FILE_TAXANOMY. */
	public static final String INVALID_FILE_TAXANOMY = "Invalid Website Taxanomy Feed";

	/** The Constant INVALID_FILE_REGISTRY. */
	public static final String INVALID_FILE_REGISTRY = "Invalid Registry Feed";

	/** The Constant INVALID_FILE_PRODUCT. */
	public static final String INVALID_FILE_PRODUCT = "Invalid Product Canonical Full Feed";
	/** The Constant ENTITY_KEYS. */
	public static final String ENTITY_KEYS = "EntityKeys";
	/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_IMPORT = "yyyyMMddHHmmss";
	/**
	 * The Constant CATALOG_REPOSITORY.
	 */
	public static final String CATALOG_REPOSITORY = "catalogRepository";
	/**
	 * The constant  SERVICE_NOT_AVAILABLE.
	 */
	public static final String SERVICE_NOT_AVAILABLE = "catalog Feed Import Service is not available for the moment.Please contact the administator.";
	/**
	 * The constant REPOSITORY_EXCEPTION.
	 */
	public static final String REPOSITORY_EXCEPTION = "Repository Exception";
	/** The Constant INVALID_RECORD_MAX_SIZE. */
	public static final String INVALID_RECORD_MAX_SIZE = "Invalid Record : Repository Exception";
	
	/** The Constant INVALID_ITEM_TYPE. */
	public static final String INVALID_ITEM_TYPE = "Invalid Record: Product Feed -- No Record found for item with ID: {0}, for item type : {1}.";
	
	/** The Constant DEFAULT_XML_VERSION. */
	public static final String DEFAULT_XML_VERSION = "1.0";
	/** The Constant VERSION. */
	public static final String VERSION = "version";
	/**
	 *  The Constant TEST_FLAG.
	 */
	public static final String TEST_FLAG = "testFlag";
	/**
	 *  The Constant PA.
	 */
	public static final String PA = "PA";
	/**
	 *  The Constant CATALOG_REPOSITORY_VER.
	 */
	public static final String CATALOG_REPOSITORY_VER = "catalogRepositoryVer";
	/**
	 *  The Constant CAT_FEED_PROCESS_RELATIONSHIP_ENTITY_STEP.
	 */
	public static final String CAT_FEED_PROCESS_RELATIONSHIP_ENTITY_STEP = "catFeedProcessRelationshipsEntityStep";
	/** 
	 * The Constant PROCESSING_DIRECTORY. 
	 */
	public static final String PROCESSING_DIRECTORY="processingDirectory";
	
	/** 
	 * The Constant AKAMAI_IMAGE_URL. 
	 */
	public static final String AKAMAI_IMAGE_URL = "akamaiImageURL";
	/** 
	 * The Constant COLLECTION_PRODUCT. 
	 */
	public static final String COLLECTION_PRODUCT = "collectionProduct";
	/** 
	 * The Constant CLASSIFICATION_COLLECTION. 
	 */
	public static final String CLASSIFICATION_COLLECTION = "TAXONOMYCOLLECTION";
	/** 
	 * The Constant SEVEN_NINE_NINE_FIVE. 
	 */
	public static final String SEVEN_NINE_NINE_FIVE = "7995";
	/** 
	 * The Constant PROJECT_CREATION_STEP. 
	 */
	public static final String PROJECT_CREATION_STEP = "catFeedProjectCreationStep";
	/** 
	 * The Constant COMPLETED. 
	 */
	public static final String COMPLETED = "COMPLETED";
	/** 
	 * The Constant PROCESS_NAME. 
	 */
	public static final String PROCESS_NAME = "processName";
	/** 
	 * The Constant ENTITY_KEYS_REG. 
	 */
	public static final String ENTITY_KEYS_REG = "EntityKeysRegistry";
	
	/** The Constant REGULAR_SKU_TYPE. */
	public static final String REGULAR_SKU_TYPE = "regularSKU";
	
	/** The Constant TAXONOMYFILTEREDLANDINGPAGE. */
	public static final String TAXONOMYFILTEREDLANDINGPAGE = "TAXONOMYFILTEREDLANDINGPAGE";
	
	/** The Constant NUMBER_FOUR. */
	public static final int NUMBER_FOUR = 4;
	
	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;
	
	/** The Constant NUMBER_SIX. */
	public static final int NUMBER_SIX = 6;
	
	/** The Constant _1. */
	public static final int _1 = 1;
	
	/** The Constant COMMA_SEPERATED_STRING. */
	public static final String COMMA_SEPERATED_STRING = ", ";
	
	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";
	
	/** The Constant STRING_FULL. */
	public static final String STRING_FULL = "Full";
	
	/** The Constant ERROR_CLASSIFICATION_ID. */
	public static final String ERROR_CLASSIFICATION_ID = "Id is not specified for the Classification";
	
	/** The Constant ERROR_CLASSIFICATION_ID. */
	public static final String ERROR_CLASSIFICATION_USER_ID = "UserTypeID is not specified for the Classification";
	
	/** The Constant ERROR_CLASSIFICATION_ID. */
	public static final String ERROR_CLASSIFICATION_NAME= "Name is not specified for the Classification";
	
	/** The Constant ERROR_CLASSIFICATION_ID. */
	public static final String ERROR_CLASSIFICATION_DO = "DisplayOrder is not specified for the Classification";
	
	
}
