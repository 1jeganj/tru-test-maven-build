package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import atg.core.util.StringUtils;
import atg.epub.project.Process;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;
import com.tru.feedprocessor.tol.catalog.listener.FeedJobExecutionListener;
import com.tru.feedprocessor.tol.catalog.vo.TRUCategory;
import com.tru.feedprocessor.tol.catalog.vo.TRUClassification;
import com.tru.feedprocessor.tol.catalog.vo.TRUCollectionProductVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUProductFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUSkuFeedVO;
import com.tru.feedprocessor.tol.catalog.vo.TRUStyleErrorVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUCatFeedExecutionDataCollectorTasklet. 
 * This class is specific to capture the Failed Records and Email triggering.
 * @author Professional Access
 * @version
 */
public class TRUCatFeedExecutionDataCollectorTasklet extends FeedExecutionDataCollectorTasklet {

	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	
	/** The Feed job listener. */
	private FeedJobExecutionListener mFeedJobListener;
	
	/** The m feed context. */
	private TRUFileCreateExecutionContext mFeedContext;
	
	/**
	 * Gets the feed context.
	 * 
	 * @return the feed context
	 */
	public TRUFileCreateExecutionContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 * 
	 * @param pFeedContext
	 *            the new feed context
	 */
	public void setFeedContext(TRUFileCreateExecutionContext pFeedContext) {
		this.mFeedContext = pFeedContext;
	}

	/**
	 * Gets the feed job listener.
	 *
	 * @return the feedJobListener
	 */
	public FeedJobExecutionListener getFeedJobListener() {
		return mFeedJobListener;
	}

	/**
	 * Sets the feed job listener.
	 *
	 * @param pFeedJobListener the feedJobListener to set
	 */
	public void setFeedJobListener(FeedJobExecutionListener pFeedJobListener) {
		mFeedJobListener = pFeedJobListener;
	}

	/**
	 * Gets the file sequence repository.
	 *
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 *
	 * @param pFileSequenceRepository the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}

	/**
	 * execute.
	 *
	 * @param pStepContribution - pStepContribution.
	 * @param pChunkContext - pChunkContext.
	 * @return RepeatStatus - RepeatStatus
	 * @throws Exception - Exception.
	 * @see com.tru.feedprocessor.tol.base.tasklet.FeedExecutionDataCollectorTasklet#execute(org.springframework.batch.core
	 * .StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution pStepContribution, ChunkContext pChunkContext) throws Exception {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: execute()");

		super.execute(pStepContribution, pChunkContext);
		boolean isException = Boolean.FALSE;
		boolean isFileInvalid = Boolean.FALSE;
		boolean isFileNotExits = Boolean.FALSE;
		StringBuffer sb = new StringBuffer(getFeedEnvConfiguration().getCatInvalidSubject());
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		String messageSubject = null;
		long totalRecordCount = 0;
		FeedExecutionContextVO feedExecutionContextVO = getCurrentFeedExecutionContext();
		String registryAlert = (String) feedExecutionContextVO.getConfigValue(TRUFeedConstants.REGISTRY_ALERT);
		String taxonomyAlert = (String) feedExecutionContextVO.getConfigValue(TRUFeedConstants.TAXONOMY_ALERT);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.TCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.RCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.PCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.SCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.PMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.CPMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.SMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.CMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.RMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.PRMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.CPRMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.CRMCOUNT, 0);
		feedExecutionContextVO.setConfigValue(TRUProductFeedConstants.RRMCOUNT, 0);
		setConfigValue(TRUProductFeedConstants.FILES_WRITING, null);
		
		if (!StringUtils.isBlank(registryAlert) && registryAlert.equals(TRUFeedConstants.REGISTRY_P_THREE_ALERT)) {
			sendPreviousThresholdAlertMail(TRUFeedConstants.REGISTRY_CATEGORIZATION, TRUFeedConstants.TAXONOMY_P_THREE_ALERT);
		}
		if (!StringUtils.isBlank(taxonomyAlert) && taxonomyAlert.equals(TRUFeedConstants.TAXONOMY_P_THREE_ALERT)) {
			sendPreviousThresholdAlertMail(TRUFeedConstants.WEB_SITE_TAXONOMY, TRUFeedConstants.TAXONOMY_P_THREE_ALERT);
		}
		List<StepInfoVO> stepInfoVOList = feedExecutionContextVO.getStepInfos();
		if (null != stepInfoVOList && !stepInfoVOList.isEmpty()) {
			for (StepInfoVO stepInfoVO : stepInfoVOList) {
				if (null != stepInfoVO && null != stepInfoVO.getFeedFailureProcessException()) {
					if (!StringUtils.isBlank(stepInfoVO.getStepName()) && stepInfoVO.getStepName().equalsIgnoreCase(TRUProductFeedConstants.CAT_FEED_DATA_VALIDATION_STEP)) {
						if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.INVALID_FILE_TAXANOMY)) {
							isFileInvalid = Boolean.TRUE;
							messageSubject = sb.append(TRUFeedConstants.INVALID_FILE_TAXANOMY).toString();
						} else if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.INVALID_FILE_REGISTRY)) {
							isFileInvalid = Boolean.TRUE;
							messageSubject = sb.append(TRUFeedConstants.INVALID_FILE_REGISTRY).toString();
						} else if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.INVALID_FILE_PRODUCT)) {
							isFileInvalid = Boolean.TRUE;
							messageSubject = sb.append(TRUFeedConstants.INVALID_FILE_PRODUCT).toString();
						}
					} else if (!StringUtils.isBlank(stepInfoVO.getStepName()) && stepInfoVO.getStepName().equalsIgnoreCase(TRUProductFeedConstants.CAT_FEED_DATA_EXISTS_STEP) && stepInfoVO.getFeedFailureProcessException().getMessage().contains(FeedConstants.FILE_DOES_NOT_EXISTS)) {
						if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
							messageSubject = sb.append(TRUFeedConstants.TAXANOMY_FILE).toString();
						} else if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
							messageSubject = sb.append(TRUFeedConstants.REGISTRY_FILE).toString();
						} else if (stepInfoVO.getFeedFailureProcessException().getMessage()
								.contains(TRUFeedConstants.ATG_PRODUCT)) {
							messageSubject = sb.append(TRUFeedConstants.PRODUCT_CANONICAL_FILE).toString();
						} else {
							messageSubject = getFeedEnvConfiguration().getCatNoFeedFileSubject();
						}
						isFileNotExits = Boolean.TRUE;
					} else if (!StringUtils.isBlank(stepInfoVO.getStepName()) && stepInfoVO.getStepName().equalsIgnoreCase(TRUFeedConstants.CAT_FEED_LOAD_DATA_STEP) && (stepInfoVO.getFeedFailureProcessException().getMessage().contains(TRUFeedConstants.TAXONOMY_P_TWO_ALERT))) {
						if (!StringUtils.isBlank(registryAlert) && registryAlert.equals(TRUFeedConstants.REGISTRY_P_TWO_ALERT)) {
							sendPreviousThresholdAlertMail(TRUFeedConstants.REGISTRY_CATEGORIZATION, TRUFeedConstants.REGISTRY_P_TWO_ALERT);
						}
						if (!StringUtils.isBlank(taxonomyAlert) && taxonomyAlert.equals(TRUFeedConstants.TAXONOMY_P_TWO_ALERT)) {
							sendPreviousThresholdAlertMail(TRUFeedConstants.WEB_SITE_TAXONOMY, TRUFeedConstants.TAXONOMY_P_TWO_ALERT);
						}
						String processingDir = getConfigValue(TRUProductFeedConstants.PROCESSING_DIRECTORY).toString();
						String badDir = getConfigValue(TRUFeedConstants.BAD_DIRECTORY).toString();
						moveInvalidFileToBadDir(feedExecutionContextVO.getFileName(), processingDir, badDir);

					}
					isException = Boolean.TRUE;
				}
				if (null != stepInfoVO && !StringUtils.isBlank(stepInfoVO.getStepName()) && stepInfoVO.getStepName().equalsIgnoreCase(TRUFeedConstants.CAT_FEEDADD_UPDATE_ENTITY_STEP)) {
					totalRecordCount = stepInfoVO.getReadCount();
				}
			}
		}
		moveRecordsToDirectory(isException, isFileInvalid, isFileNotExits, feedExecutionContextVO, messageSubject,
				totalRecordCount);
		
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.REGISTRY_ALERT, null);
		feedExecutionContextVO.setConfigValue(TRUFeedConstants.TAXONOMY_ALERT, null);
		
		if(isFileInvalid){
			logFailedMessage(startDate, TRUProductFeedConstants.FEED_FAILED);
		}else if(isFileNotExits){
			logFailedMessage(startDate, TRUProductFeedConstants.FEED_FAILED);
		}else if(isException){
			logFailedMessage(startDate, TRUProductFeedConstants.FEED_FAILED);
		}else {
			logSuccessMessage(startDate, TRUProductFeedConstants.FEED_COMPLETED);
		}

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: execute()");

		return RepeatStatus.FINISHED;
	}

	/**
	 * Move records to directory.
	 * 
	 * @param pIsException
	 *            the is exception
	 * @param pIsFileInvalid
	 *            the is file invalid
	 * @param pIsFileNotExits
	 *            the is file not exits
	 * @param pFeedExecutionContextVO
	 *            the feed execution context vo
	 * @param pMessageSubject
	 *            the message subject
	 * @param pTotalRecordCount
	 *            the total record count
	 */
	@SuppressWarnings("unchecked")
	private void moveRecordsToDirectory(boolean pIsException, boolean pIsFileInvalid, boolean pIsFileNotExits,
			FeedExecutionContextVO pFeedExecutionContextVO, String pMessageSubject, long pTotalRecordCount) {
		boolean moveSuccessRecords = pIsException;
		String processingDir = getConfigValue(TRUProductFeedConstants.PROCESSING_DIRECTORY).toString();
		String badDir = getConfigValue(TRUProductFeedConstants.BAD_DIRECTORY).toString();
		String successDir = getConfigValue(TRUProductFeedConstants.SUCCESS_DIRECTORY).toString();
		String errorDir = getConfigValue(TRUProductFeedConstants.ERROR_DIRECTORY).toString();
		String sourceDir = getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString();
		String fileType = (String) getJobParameter(TRUProductFeedConstants.FILE_TYPE);
		if(StringUtils.isNotBlank(fileType) && fileType.equals(TRUProductFeedConstants.DELETE)){
			sourceDir = getConfigValue(TRUProductFeedConstants.DELETE_SOURCE_DIRECTORY).toString();
			processingDir = getConfigValue(TRUProductFeedConstants.DELETE_PROCESSING_DIRECTORY).toString();
		}
		if (pIsFileInvalid) {
			triggerInvalidFeedEmail(badDir, pMessageSubject);
			moveInvalidFileToBadDir(pFeedExecutionContextVO.getFileName(), processingDir, badDir);
			return;
		}
		if (pIsFileNotExits) {
			notifyFileDoseNotExits(sourceDir, pMessageSubject);
			return;
		}
		long taxonomyCount = 0;
		long registryCount = 0;
		long productCount = 0;
		if (pFeedExecutionContextVO.getConfigValue(TRUFeedConstants.TAXONOMY_RECORD_COUNT) != null) {

			taxonomyCount = (int) pFeedExecutionContextVO.getConfigValue(TRUFeedConstants.TAXONOMY_RECORD_COUNT);
		}
		if (pFeedExecutionContextVO.getConfigValue(TRUFeedConstants.REGISTRY_RECORD_COUNT) != null) {
			registryCount = (int) pFeedExecutionContextVO.getConfigValue(TRUFeedConstants.REGISTRY_RECORD_COUNT);
		}
		if (pTotalRecordCount - taxonomyCount - registryCount > FeedConstants.NUMBER_ZERO) {
			productCount = (int) pFeedExecutionContextVO.getConfigValue(TRUProductFeedConstants.PRODUCT_CANONICAL_TOTAL_COUNT);
		}
		List<FeedSkippableException> invalidRecords = null;
		List<FeedSkippableException> invalidTaxRecords = null;
		List<FeedSkippableException> invalidRegRecords = null;
		boolean fileNameProd = Boolean.FALSE;
		boolean fileNameReg = Boolean.FALSE;
		boolean fileNameTax = Boolean.FALSE;
		long taxonomySuccessCount = taxonomyCount;
		long registrySuccessCount = registryCount;
		long productSuccessCount = productCount;
		double productThresholdPercentage = 0;
		double websiteTaxonomyPercentage = 0;
		double registryTaxonomyPercentage = 0;
		String[] fileNames = null;
		if (!pIsException) {
			Map<String, List<FeedSkippableException>> allFeedSkippedException = pFeedExecutionContextVO
					.getAllFeedSkippedException();
			invalidRecords = new ArrayList<FeedSkippableException>();
			invalidRegRecords = new ArrayList<FeedSkippableException>();
			invalidTaxRecords = new ArrayList<FeedSkippableException>();
			if(!allFeedSkippedException.isEmpty()){
				moveSuccessRecords = Boolean.TRUE;
				for (Entry<String, List<FeedSkippableException>> entry : allFeedSkippedException.entrySet()) {
					List<FeedSkippableException> lFeedSkippableException = entry.getValue();
					for (FeedSkippableException lSkippedException : lFeedSkippableException) {
						if (null != lSkippedException && lSkippedException.getMessage().contains(TRUProductFeedConstants.INVALID_RECORD_PRODUCT)) {
							fileNameProd = Boolean.TRUE;
							invalidRecords.add(lSkippedException);
						} else if (null != lSkippedException && lSkippedException.getMessage().contains(TRUFeedConstants.INVALID_RECORD_REGISTRY)) {
							fileNameReg = Boolean.TRUE;
							invalidRegRecords.add(lSkippedException);
						} else if (null != lSkippedException
								&& lSkippedException.getMessage().contains(TRUFeedConstants.INVALID_RECORD_TAXONOMY)) {
							fileNameTax = Boolean.TRUE;
							invalidTaxRecords.add(lSkippedException);
						} else if (null != lSkippedException
								&& lSkippedException.getMessage().contains(TRUFeedConstants.INVALID_RECORD_MAX_SIZE)) {
							fileNameProd = Boolean.TRUE;
							invalidRecords.add(lSkippedException);
						}
					}
				}
				if (invalidTaxRecords != null && !invalidTaxRecords.isEmpty() && taxonomySuccessCount > FeedConstants.NUMBER_ZERO) {
					taxonomySuccessCount = taxonomySuccessCount - invalidTaxRecords.size();
				}
				if (invalidRegRecords != null && !invalidRegRecords.isEmpty() && registrySuccessCount > FeedConstants.NUMBER_ZERO) {
					registrySuccessCount = registrySuccessCount - invalidRegRecords.size();
				}
				if (invalidRecords != null && !invalidRecords.isEmpty() && productSuccessCount > FeedConstants.NUMBER_ZERO) {
					productSuccessCount = productSuccessCount - invalidRecords.size();
				}
				List<TRUStyleErrorVO> styleErrorList = (List<TRUStyleErrorVO>) getConfigValue(TRUProductFeedConstants.STYLE_ERROR);
	            if(null != styleErrorList && !styleErrorList.isEmpty()){
	                  fileNameProd = Boolean.TRUE;
	                  productSuccessCount = productSuccessCount - styleErrorList.size();
	            }
				String taxFileName = null;
				String regFileName = null;
				StringBuffer prodFileName = new StringBuffer();
				StringBuffer sequenceNumber = new StringBuffer();
				String seperator = TRUFeedConstants.EMPTY_STRING;
				boolean processAction = Boolean.TRUE;
				if (pFeedExecutionContextVO.getFileName() != null) {
					fileNames = pFeedExecutionContextVO.getFileName().split(TRUFeedConstants.DOLLAR);
					for (String name : fileNames) {
						if (name.contains(TRUFeedConstants.WEB_SITE_TAXONOMY) && fileNameTax) {
							taxFileName = name;
						} else if (name.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION) && fileNameReg) {
							regFileName = name;
						} else if (name.contains(TRUFeedConstants.ATG_PRODUCT) && fileNameProd) {
							prodFileName.append(seperator);
							prodFileName.append(name);
							seperator = TRUFeedConstants.COMMA_SEPERATED_STRING;
						}
					}
					sequenceNumber = frameSequenceNumber(fileNames);
				}
				if ((null != styleErrorList && !styleErrorList.isEmpty())) {
					productThresholdPercentage = calculatePercentage(productCount, invalidRecords.size()+styleErrorList.size());
				} else{
					productThresholdPercentage = calculatePercentage(productCount, invalidRecords.size());
				}
				registryTaxonomyPercentage = calculatePercentage(registryCount, invalidRegRecords.size());
				websiteTaxonomyPercentage = calculatePercentage(taxonomyCount, invalidTaxRecords.size());
				if(productThresholdPercentage > getFeedEnvConfiguration().getLowerCutoffAlertPriority2()){
					String alert = TRUFeedConstants.REGISTRY_P_TWO_ALERT;
					moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), processingDir, errorDir);
					getFeedJobListener().deleteBCCProject((Process) getFeedContext().getConfigValue(
							FeedConstants.PROCESS));
					triggerFeedExceededThresholdEmail(invalidRecords, prodFileName.toString(), errorDir, TRUFeedConstants.ATG_PRODUCT, alert, styleErrorList);
					processAction = Boolean.FALSE;
				} else if(registryTaxonomyPercentage > getFeedEnvConfiguration().getCutoffExceptionAlertPriority2()){
					String alert = TRUFeedConstants.REGISTRY_P_TWO_ALERT;
					moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), processingDir, errorDir);
					getFeedJobListener().deleteBCCProject((Process) getFeedContext().getConfigValue(
							FeedConstants.PROCESS));
					triggerFeedExceededThresholdEmail(invalidRegRecords, regFileName, errorDir, TRUFeedConstants.REGISTRY_CATEGORIZATION, alert, null);
					processAction = Boolean.FALSE;
				} else if(websiteTaxonomyPercentage > getFeedEnvConfiguration().getCutoffExceptionAlertPriority2()){
					String alert = TRUFeedConstants.TAXONOMY_P_TWO_ALERT;
					getFeedJobListener().deleteBCCProject((Process) getFeedContext().getConfigValue(
							FeedConstants.PROCESS));
					moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), processingDir, errorDir);
					triggerFeedExceededThresholdEmail(invalidTaxRecords, taxFileName, errorDir, TRUFeedConstants.WEB_SITE_TAXONOMY, alert, null);
					processAction = Boolean.FALSE;
				}
				if (((null != invalidRecords && !invalidRecords.isEmpty()) || (null != styleErrorList && !styleErrorList.isEmpty())) && processAction) {
					Map<String,Object> counts = new HashMap<String, Object>();
					counts.put(TRUProductFeedConstants.SUCCESS_COUNT, productSuccessCount);
					counts.put(TRUProductFeedConstants.ERROR_COUNT, productCount - productSuccessCount);
					counts.put(TRUProductFeedConstants.PROD_THRESHOLD_PERCENTAGE, productThresholdPercentage);
					processInvalidRecords(pFeedExecutionContextVO, invalidRecords, prodFileName.toString(), sequenceNumber.toString(), styleErrorList, fileNames, counts);
				}else if(processAction){
					storeSequenceNumber(fileNames);
					triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.ATG_PRODUCT, productSuccessCount, productCount - productSuccessCount, sequenceNumber.toString());
				}
				if (null != invalidRegRecords && !invalidRegRecords.isEmpty() && processAction) {
					processInvalidRegistryRecords(pFeedExecutionContextVO, processingDir, successDir,
							errorDir, registryTaxonomyPercentage,
							invalidRegRecords, regFileName, registrySuccessCount, registryCount - registrySuccessCount);
				}else if(processAction){
					triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.REGISTRY_CATEGORIZATION, registrySuccessCount, registryCount - registrySuccessCount, TRUFeedConstants.STRING_FULL);
				}
				if (null != invalidTaxRecords && !invalidTaxRecords.isEmpty() && processAction) {
					processInvalidTaxonomyRecords(
							pFeedExecutionContextVO, processingDir, successDir,
							errorDir, websiteTaxonomyPercentage,
							invalidTaxRecords, taxFileName, taxonomySuccessCount, taxonomyCount - taxonomySuccessCount);
				}else if(processAction){
					triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.WEB_SITE_TAXONOMY, taxonomySuccessCount, taxonomyCount - taxonomySuccessCount, TRUFeedConstants.STRING_FULL);
				}
			}
			setConfigValue(TRUProductFeedConstants.STYLE_ERROR, null);
		}
		if (!moveSuccessRecords) {
			fileNames = pFeedExecutionContextVO.getFileName().split(TRUFeedConstants.DOLLAR);
			storeSequenceNumber(fileNames);
			triggerSuccessEmails(pFeedExecutionContextVO, processingDir,
					successDir, taxonomyCount, registryCount, productCount,
					taxonomySuccessCount, registrySuccessCount,
					productSuccessCount);
		}
	}

	/**
	 * Frame sequence number.
	 * 
	 * @param pFileNames
	 *            the file names
	 * @return the string
	 */
	protected StringBuffer frameSequenceNumber(String[] pFileNames) {
		StringBuffer sequenceNumber = new StringBuffer();
		String seperator = TRUFeedConstants.EMPTY_STRING;
		if (pFileNames != null) {
			for (String name : pFileNames) {
				if (name.contains(TRUFeedConstants.ATG_PRODUCT)) {
					String[] feedFileNames = name.split(TRUFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
					String seqNumber = feedFileNames[TRUFeedConstants.NUMBER_SIX];
					String sequence = seqNumber.substring(TRUFeedConstants.NUMBER_ZERO, seqNumber.length() - TRUFeedConstants.NUMBER_FOUR);
					sequenceNumber.append(seperator);
					sequenceNumber.append(sequence);
					seperator = TRUFeedConstants.COMMA_SEPERATED_STRING;
				}
			}
		}
		return sequenceNumber;
	}
	
	/**
	 * Store sequence number.
	 *
	 * @param pFileNames the file names
	 */
	private void storeSequenceNumber(String[] pFileNames){
		List<Integer> fileSequenceList = new ArrayList<Integer>();
		if (pFileNames != null) {
			for (String name : pFileNames) {
				if (name.contains(TRUFeedConstants.ATG_PRODUCT)) {
					String[] feedFileNames = name.split(TRUFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
					String seqNumber = feedFileNames[TRUFeedConstants.NUMBER_SIX];
					String sequence = seqNumber.substring(TRUFeedConstants.NUMBER_ZERO, seqNumber.length() - TRUFeedConstants.NUMBER_FOUR);
					if(!(sequence.equalsIgnoreCase(TRUProductFeedConstants.FULL) || (sequence.equalsIgnoreCase(TRUProductFeedConstants.DELETE)))){
						fileSequenceList.add(Integer.valueOf(sequence));
					}
				}
			}
			if(!fileSequenceList.isEmpty()){
				Collections.sort(fileSequenceList);
				RepositoryItem repoItem;
				try {
					repoItem = getFileSequenceRepository().getItem(TRUProductFeedConstants.PRODUCT_FEED, TRUProductFeedConstants.FEED_FILE_SEQUENCE);
					if(repoItem != null){
						MutableRepositoryItem mutItem =  (MutableRepositoryItem) repoItem;
						mutItem.setPropertyValue(getFeedEnvConfiguration().getSequenceProcessed(), fileSequenceList.get(fileSequenceList.size() - TRUProductFeedConstants.NUMBER_ONE));
						getFileSequenceRepository().updateItem(mutItem);
					} else{
						MutableRepositoryItem mutItem = getFileSequenceRepository().createItem(TRUProductFeedConstants.PRODUCT_FEED, TRUProductFeedConstants.FEED_FILE_SEQUENCE);
						mutItem.setPropertyValue(getFeedEnvConfiguration().getSequenceProcessed(), fileSequenceList.get(fileSequenceList.size() - TRUProductFeedConstants.NUMBER_ONE));
						getFileSequenceRepository().addItem(mutItem);
					}
				} catch (RepositoryException re) {
					if (isLoggingError()) {
						logError("Error in : @class TRUCatFeedExecutionDataCollectorTasklet method frameSequenceNumber()",
							re);
					}
				}
			}
		}
	}

	
	
	/**
	 * Process invalid records.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pInvalidRecords the invalid records
	 * @param pProdFileName the prod file name
	 * @param pSequenceNumber the sequence number
	 * @param pStyleErrorList the style error list
	 * @param pFileNames the file names
	 * @param pCounts the counts
	 */
	protected void processInvalidRecords(
			FeedExecutionContextVO pFeedExecutionContextVO,
			List<FeedSkippableException> pInvalidRecords, String pProdFileName, String pSequenceNumber, List<TRUStyleErrorVO> pStyleErrorList, String[] pFileNames, Map<String, Object> pCounts) {
		String alert = null;
		long successCount = 0;
		long errorCount = 0;
		double productThresholdPercentage = 0;
		String processingDir = getConfigValue(TRUProductFeedConstants.PROCESSING_DIRECTORY).toString();
		String successDir = getConfigValue(TRUProductFeedConstants.SUCCESS_DIRECTORY).toString();
		String errorDir = getConfigValue(TRUProductFeedConstants.ERROR_DIRECTORY).toString();
		if(pCounts != null){
			successCount = (long) pCounts.get(TRUProductFeedConstants.SUCCESS_COUNT);
			errorCount = (long) pCounts.get(TRUProductFeedConstants.ERROR_COUNT);
			productThresholdPercentage = (double) pCounts.get(TRUProductFeedConstants.PROD_THRESHOLD_PERCENTAGE);
		}
		if (productThresholdPercentage > getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriorityRegular()
				&& productThresholdPercentage <= getFeedEnvConfiguration().getLowerCutoffAlertPriority2()) {
			alert = TRUFeedConstants.REGISTRY_P_THREE_ALERT;
			
		}
		moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), processingDir, successDir);
		//moveInvalidRecordsToErrorDir(pInvalidRecords, pProdFileName, pErrorDir, TRUFeedConstants.ATG_PRODUCT,alert, pStyleErrorList, pSequenceNumber);
		triggerFeedProcessedWithErrorsEmail(pInvalidRecords, pProdFileName, errorDir, TRUFeedConstants.ATG_PRODUCT, alert, pStyleErrorList, pSequenceNumber, successCount, errorCount);
		storeSequenceNumber(pFileNames);
	}
	
	
	/**
	 * Trigger feed exceeded threshold email.
	 *
	 * @param pInvalidRecords the invalid records
	 * @param pFileName the file name
	 * @param pErrorDir the error dir
	 * @param pFeedType the feed type
	 * @param pAlert the alert
	 * @param pStyleErrorList the style error list
	 */
	private void triggerFeedExceededThresholdEmail(List<FeedSkippableException> pInvalidRecords, String pFileName,
			String pErrorDir, String pFeedType, String pAlert, List<TRUStyleErrorVO> pStyleErrorList){
		getLogger().vlogDebug( 
				"Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedExceededThresholdEmail()");
		String subject = null;
		String content = null;
		String detailException = null;
		
		File[] attachmentFile = null;

		if (pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			content = getFeedEnvConfiguration().getWebsiteFeedName();
			detailException = TRUProductFeedConstants.THRESHOLD_WEBTAX_DETAILEDEXP;
		} else if (pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			content = getFeedEnvConfiguration().getRegistryFeedName();
			detailException = TRUProductFeedConstants.THRESHOLD_REG_DETAILEDEXP;
		} else if (pFeedType.equals(TRUFeedConstants.ATG_PRODUCT)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			content = getFeedEnvConfiguration().getCatProdFeedName();
			detailException = TRUProductFeedConstants.THRESHOLD_PRODCAN_DETAILEDEXP;
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		attachmentFile = new File[FeedConstants.NUMBER_ONE];
		attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pInvalidRecords, pFileName , pStyleErrorList, pErrorDir);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, TRUProductFeedConstants.THRESHOLD_MESSAGE+content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pAlert);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.CAT_JOB_NAME, templateParameters,
				attachmentFile);


		getLogger().vlogDebug(
				"End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedExceededThresholdEmail()");
	}

	
	/**
	 * Process invalid registry records.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pProcessingDir the processing dir
	 * @param pSuccessDir the success dir
	 * @param pErrorDir the error dir
	 * @param pRegistryTaxonomyPercentage the registry taxonomy percentage
	 * @param pInvalidRegRecords the invalid reg records
	 * @param pRegFileName the reg file name
	 * @param pSuccessRecordCount the success record count
	 * @param pErrorRecordCount the error record count
	 */
	protected void processInvalidRegistryRecords(
			FeedExecutionContextVO pFeedExecutionContextVO,
			String pProcessingDir, String pSuccessDir, String pErrorDir, Double pRegistryTaxonomyPercentage,
			List<FeedSkippableException> pInvalidRegRecords, String pRegFileName, long pSuccessRecordCount, long pErrorRecordCount) {
		
		String alert = null;
		if (pRegistryTaxonomyPercentage > getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriority3() && pRegistryTaxonomyPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriority3()) {
			alert = TRUFeedConstants.REGISTRY_P_THREE_ALERT;
		}
		//moveInvalidRecordsToErrorDir(pInvalidRegRecords, pRegFileName, pErrorDir,TRUFeedConstants.REGISTRY_CATEGORIZATION, alert , null , TRUProductFeedConstants.FULL);
		moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), pProcessingDir, pSuccessDir);
		triggerFeedProcessedWithErrorsEmail(pInvalidRegRecords, pRegFileName, pErrorDir, TRUFeedConstants.REGISTRY_CATEGORIZATION, alert, null, TRUProductFeedConstants.FULL, pSuccessRecordCount, pErrorRecordCount);

	}

	
	/**
	 * Process invalid taxonomy records.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pProcessingDir the processing dir
	 * @param pSuccessDir the success dir
	 * @param pErrorDir the error dir
	 * @param pWebsiteTaxonomyPercentage the website taxonomy percentage
	 * @param pInvalidTaxRecords the invalid tax records
	 * @param pTaxFileName the tax file name
	 * @param pSuccessRecordCount the success record count
	 * @param pErrorRecordCount the error record count
	 */
	protected void processInvalidTaxonomyRecords(
			FeedExecutionContextVO pFeedExecutionContextVO,
			String pProcessingDir, String pSuccessDir, String pErrorDir,
			Double pWebsiteTaxonomyPercentage,
			List<FeedSkippableException> pInvalidTaxRecords,String pTaxFileName, long pSuccessRecordCount, long pErrorRecordCount) {
		String alert = null;
		if (pWebsiteTaxonomyPercentage > getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriority3() && pWebsiteTaxonomyPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriority3()) {
			alert = TRUFeedConstants.TAXONOMY_P_THREE_ALERT;
		}
		//moveInvalidRecordsToErrorDir(pInvalidTaxRecords, pTaxFileName, pErrorDir, TRUFeedConstants.WEB_SITE_TAXONOMY,alert , null , TRUProductFeedConstants.FULL);
		moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), pProcessingDir, pSuccessDir);
		triggerFeedProcessedWithErrorsEmail(pInvalidTaxRecords, pTaxFileName, pErrorDir, TRUFeedConstants.WEB_SITE_TAXONOMY, alert, null, TRUProductFeedConstants.FULL, pSuccessRecordCount, pErrorRecordCount);
	}

	/**
	 * Trigger success emails.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pProcessingDir the processing dir
	 * @param pSuccessDir the success dir
	 * @param pTaxonomyCount the taxonomy count
	 * @param pRegistryCount the registry count
	 * @param pProductCount the product count
	 * @param pTaxonomySuccessCount the taxonomy success count
	 * @param pRegistrySuccessCount the registry success count
	 * @param pProductSuccessCount the product success count
	 */
	protected void triggerSuccessEmails(
			FeedExecutionContextVO pFeedExecutionContextVO,
			String pProcessingDir, String pSuccessDir, long pTaxonomyCount,
			long pRegistryCount, long pProductCount, long pTaxonomySuccessCount,
			long pRegistrySuccessCount, long pProductSuccessCount) {
		moveSuccessFileToSuccessDir(pFeedExecutionContextVO.getFileName(), pProcessingDir, pSuccessDir);
		String[] lFileList = pFeedExecutionContextVO.getFileName().split(TRUFeedConstants.DOLLAR);
		boolean isProductMailSent = false;
		StringBuffer sequenceNumber = frameSequenceNumber(lFileList);
		for (String fileName : lFileList) {
			if (fileName.contains(TRUFeedConstants.WEB_SITE_TAXONOMY) && pTaxonomySuccessCount > FeedConstants.NUMBER_ZERO) {
				triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.WEB_SITE_TAXONOMY, pTaxonomySuccessCount,
						pTaxonomyCount - pTaxonomySuccessCount, TRUFeedConstants.STRING_FULL);
			} else if (fileName.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION) && pRegistrySuccessCount > FeedConstants.NUMBER_ZERO) {
				triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.REGISTRY_CATEGORIZATION, pRegistrySuccessCount,
						pRegistryCount - pRegistrySuccessCount, TRUFeedConstants.STRING_FULL);
			} else if (fileName.contains(TRUFeedConstants.ATG_PRODUCT) && !isProductMailSent && pProductSuccessCount > FeedConstants.NUMBER_ZERO) {
				triggerFeedProcessedSuccessfullyEmail(TRUFeedConstants.ATG_PRODUCT, pProductSuccessCount, pProductCount - pProductSuccessCount,
						sequenceNumber.toString());
				isProductMailSent = true;
			}
		}
	}

	/**
	 * Notify file dose not exits.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 * @param pMessageSubject
	 *            the message subject
	 */
	public void notifyFileDoseNotExits(String pSourceDir, String pMessageSubject) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");

		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String subject = null;
		String content = null;
		String priority = null;
		String detailException = null;
		if (pMessageSubject.contains(TRUFeedConstants.TAXANOMY_FILE)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		} else if (pMessageSubject.contains(TRUFeedConstants.REGISTRY_FILE)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		} else if (pMessageSubject.contains(TRUFeedConstants.PRODUCT_CANONICAL_FILE)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		} else if(pMessageSubject.contains(TRUProductFeedConstants.FOLDER_NOT_EXISTS)){
			content = TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_1 + pSourceDir + TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
			detailException = pSourceDir + TRUProductFeedConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
		} else if(pMessageSubject.contains(TRUProductFeedConstants.PERMISSION_ISSUE)){
			content = TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_1 + pSourceDir + TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_2;
			detailException = pSourceDir + TRUProductFeedConstants.JOB_FAILED_PERMISSION_ISSUE_2;
		}
		if(detailException == null){
			detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		}
		priority = TRUProductFeedConstants.SEQ_MISS;
		content = TRUProductFeedConstants.JOB_FAILED_MISSING_FILE;
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.CAT_JOB_NAME, templateParameters);

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");
	}

	/**
	 * Send previous threshold alert mail.
	 *
	 * @param pFeedType the feed type
	 * @param pPriority the priority
	 */
	private void sendPreviousThresholdAlertMail(String pFeedType, String pPriority) {
		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: sendThresholdAlertMail()");

		String subject = null;
		String content = null;
		String detailException = null;
		File[] attachmentFile = null;
		String priority = null;
		if (pFeedType!=null && pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			if(pPriority != null && pPriority.equals(TRUFeedConstants.TAXONOMY_P_TWO_ALERT)){
				content = TRUProductFeedConstants.THRESHOLD_FAILED_PREVIOUS_MESSAGE + getFeedEnvConfiguration().getWebsiteFeedName();
				detailException = TRUProductFeedConstants.THRESHOLD_WEBTAX_PREV_20_DETAILEDEXP;
				priority = pPriority;
			}else if(pPriority != null && pPriority.equals(TRUFeedConstants.TAXONOMY_P_THREE_ALERT)){
				content = TRUProductFeedConstants.THRESHOLD_FAILED_PREVIOUS_15_MESSAGE + getFeedEnvConfiguration().getWebsiteFeedName();
				detailException = TRUProductFeedConstants.THRESHOLD_WEBTAX_PREV_15_DETAILEDEXP;
				priority = TRUProductFeedConstants.PREV_ALERT;
			}
		} else if (pFeedType!=null && pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			if(pPriority != null && pPriority.equals(TRUFeedConstants.TAXONOMY_P_TWO_ALERT)){
				content = TRUProductFeedConstants.THRESHOLD_FAILED_PREVIOUS_MESSAGE + getFeedEnvConfiguration().getRegistryFeedName();
				detailException = TRUProductFeedConstants.THRESHOLD_REG_PREV_20_DETAILEDEXP;
				priority = pPriority;
			}else if(pPriority != null && pPriority.equals(TRUFeedConstants.TAXONOMY_P_THREE_ALERT)){
				content = TRUProductFeedConstants.THRESHOLD_FAILED_PREVIOUS_15_MESSAGE + getFeedEnvConfiguration().getRegistryFeedName();
				detailException = TRUProductFeedConstants.THRESHOLD_REG_PREV_15_DETAILEDEXP;
				priority = TRUProductFeedConstants.PREV_ALERT;
			}
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.CAT_JOB_NAME, templateParameters,
				attachmentFile);

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: sendThresholdAlertMail()");

	}

	/**
	 * Move invalid file to bad dir.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pBadDir
	 *            the bad dir
	 */
	public void moveInvalidFileToBadDir(String pFileName, String pProcessingDir, String pBadDir) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: moveInvalidFileToBadDir()");

		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pBadDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					String[] lFileList = pFileName.split(TRUFeedConstants.DOLLAR);
					for (String lfileName : lFileList) {
						if (file.isFile() && file.getName().contains(lfileName)) {
							file.renameTo(new File(toFolder, file.getName()));
						}
					}
				}
			}
		} else if(sourceFolder.exists() || sourceFolder.isDirectory()){
			String messageSubject = TRUProductFeedConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(sourceFolder.getAbsolutePath(), messageSubject);
		} else if(sourceFolder.canRead()){
			String messageSubject = TRUProductFeedConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(sourceFolder.getAbsolutePath(), messageSubject);
		}

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: moveInvalidFileToBadDir()");

	}

	/**
	 * Trigger invalid feed email.
	 * 
	 * @param pBadDir
	 *            the bad dir
	 * @param pMessageSubject
	 *            the message subject
	 */
	public void triggerInvalidFeedEmail(String pBadDir, String pMessageSubject) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerInvalidFeedEmail()");

		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String subject = null;
		String priority = null;
		String content = null;
		String detailException = null;
		if(pMessageSubject.contains(TRUFeedConstants.INVALID_FILE_TAXANOMY)){
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			detailException = TRUProductFeedConstants.INVALID_WEBTAX_DETAILEDEXP;
		} else if(pMessageSubject.contains(TRUFeedConstants.INVALID_FILE_REGISTRY)){
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			detailException = TRUProductFeedConstants.INVALID_REG_DETAILEDEXP;
		} else if(pMessageSubject.contains(TRUFeedConstants.INVALID_FILE_PRODUCT)){
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			detailException = TRUProductFeedConstants.INVALID_PRODCAN_DETAILEDEXP;
		}
		priority = TRUProductFeedConstants.FILE_INVALID;
		content = TRUProductFeedConstants.JOB_FAILED_INVALID_FILE;
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.CAT_JOB_NAME, templateParameters);

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerInvalidFeedEmail()");

	}
	
	
		

	/**
	 * Trigger feed processed with errors email.
	 *
	 * @param pInvalidRecords            the invalid records
	 * @param pFileName            the file name
	 * @param pErrorDir            the error dir
	 * @param pFeedType            the feed type
	 * @param pAlert            the alert
	 * @param pStyleErrorList - StyleErrorList
	 * @param pSequenceNumber the sequence number
	 * @param pSuccessRecordCount the success record count
	 * @param pErrorRecordCount the error record count
	 */
	public void triggerFeedProcessedWithErrorsEmail(List<FeedSkippableException> pInvalidRecords, String pFileName,
			String pErrorDir, String pFeedType, String pAlert, List<TRUStyleErrorVO> pStyleErrorList , String pSequenceNumber, long pSuccessRecordCount, long pErrorRecordCount) {

		getLogger().vlogDebug(
				"Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedProcessedWithErrorsEmail()");
		String subject= null;
		File[] attachmentFile = null;
		String detailException = null;

		if (pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE3+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			detailException = getFeedEnvConfiguration().getWebsiteFeedName() +TRUProductFeedConstants.SPACE+ TRUProductFeedConstants.PROCESS_FILE_SUCCESS + TRUProductFeedConstants.BUT_WITH_ERRORS;
		} else if (pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE3+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			detailException = getFeedEnvConfiguration().getRegistryFeedName() +TRUProductFeedConstants.SPACE+ TRUProductFeedConstants.PROCESS_FILE_SUCCESS + TRUProductFeedConstants.BUT_WITH_ERRORS;
		} else if (pFeedType.equals(TRUFeedConstants.ATG_PRODUCT)) {
			if(pAlert != null && pAlert.equalsIgnoreCase(TRUFeedConstants.TAXONOMY_P_TWO_ALERT)){
				subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE4+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			} else{
				subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE3+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
			}
			detailException = getFeedEnvConfiguration().getCatProdFeedName() +TRUProductFeedConstants.SPACE+ TRUProductFeedConstants.PROCESS_FILE_SUCCESS + TRUProductFeedConstants.BUT_WITH_ERRORS;
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		attachmentFile = new File[FeedConstants.NUMBER_ONE];
		attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pInvalidRecords, pFileName , pStyleErrorList, pErrorDir);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pAlert);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pSuccessRecordCount + pErrorRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pErrorRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.CAT_JOB_NAME, templateParameters,
				attachmentFile);

		getLogger().vlogDebug(
				"End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedProcessedWithErrorsEmail()");

	}

	/**
	 * Creates the attachment file.
	 *
	 * @param pInvalidRecords            the invalid records
	 * @param pFileName            the file name
	 * @param pStyleErrorList - StyleErrorList
	 * @param pErrorDir the error dir
	 * @return the output stream
	 */
	public File createAttachmentFile(List<FeedSkippableException> pInvalidRecords, String pFileName , List<TRUStyleErrorVO> pStyleErrorList, String pErrorDir) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: createAttachmentFile()");

		File errorFolder = new File(pErrorDir);
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}
		File attachement = new File(errorFolder, attachementFileName(pFileName));
		if(pFileName.contains(TRUFeedConstants.WEB_SITE_TAXONOMY) || pFileName.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)){
			try {
				webSiteTaxonomyRegistryClassificationAttachment(pInvalidRecords, attachement);
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class TRUCatFeedExecutionDataCollectorTasklet method createAttachmentFile()",
						ioe);
		         }
			}
				
		}else if(pFileName.contains(TRUFeedConstants.ATG_PRODUCT)){
			try {
				productCanonicalAttachment(pInvalidRecords, pStyleErrorList,attachement);
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class TRUCatFeedExecutionDataCollectorTasklet method createAttachmentFile()",
						ioe);
		         }
			}
		}

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: createAttachmentFile()");

		return attachement;
	}

	/**
	 * Product canonical attachment.
	 *
	 * @param pInvalidRecords the invalid records
	 * @param pStyleErrorList the style error list
	 * @param pAttachement the attachement
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void productCanonicalAttachment(
			List<FeedSkippableException> pInvalidRecords,
			List<TRUStyleErrorVO> pStyleErrorList, File pAttachement)
			throws FileNotFoundException, IOException {
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		outputStream = new FileOutputStream(pAttachement);
		workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(TRUProductFeedConstants.SHEET);
		int rowCount = FeedConstants.NUMBER_ZERO;
		HSSFRow rowhead = sheet.createRow(rowCount);
		HSSFCellStyle rowCellStyle = workbook.createCellStyle();
		rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		productCanonicalAttachHeader(rowhead, rowCellStyle);
		
		rowCount = FeedConstants.NUMBER_ONE;
		HSSFCellStyle dataCellStyle = workbook.createCellStyle();
		dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		rowCount = productCanonicalAttachData(pInvalidRecords, sheet, rowCount,
				dataCellStyle);
		
		if(null != pStyleErrorList && !pStyleErrorList.isEmpty()){
			for(TRUStyleErrorVO styleErrorVO : pStyleErrorList){
				rowCount = productCanonicalAttachStyleData(sheet, rowCount,
						dataCellStyle, styleErrorVO);
			}
		}
		
		
		if (null != workbook) {
			workbook.write(outputStream);
		}
	}

	/**
	 * Product canonical attach style data.
	 *
	 * @param pSheet the sheet
	 * @param pRowCount the row count
	 * @param pDataCellStyle the data cell style
	 * @param pStyleErrorVO the style error vo
	 * @return the int
	 */
	private int productCanonicalAttachStyleData(HSSFSheet pSheet, int pRowCount,
			HSSFCellStyle pDataCellStyle, TRUStyleErrorVO pStyleErrorVO) {
		
		int rowCount = pRowCount;
		
		if(!pStyleErrorVO.getSkusNotAvailable().isEmpty()){
			HSSFRow dataRow = pSheet.createRow(rowCount);
			StringBuffer str = new StringBuffer();
			for (int i = 0; i < pStyleErrorVO.getSkusNotAvailable().size(); i++) {
				str.append(pStyleErrorVO.getSkusNotAvailable().get(i));
				if(i < pStyleErrorVO.getSkusNotAvailable().size()-TRUProductFeedConstants.NUMBER_ONE){
					str.append(TRUProductFeedConstants.CAMMA);
				}
			}
			
			dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
					new HSSFRichTextString(pStyleErrorVO.getFileName()));
			dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
					new HSSFRichTextString(Integer.toString(rowCount)));
			dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
					new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
			dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(pStyleErrorVO.getId()));
			dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(str.toString()));
			dataRow.createCell(FeedConstants.NUMBER_FIVE).setCellValue(new HSSFRichTextString(TRUFeedConstants.EMPTY_STRING));
			dataRow.createCell(FeedConstants.NUMBER_SIX).setCellValue(new HSSFRichTextString(TRUProductFeedConstants.STYLE_ERROR_MSG_NOT_FOUND));
			
			
			dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_FIVE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_SIX).setCellStyle(pDataCellStyle);
			
			rowCount++;
			
		}
		if(!pStyleErrorVO.getSkusNotAssociated().isEmpty()){
			HSSFRow dataRow = pSheet.createRow(rowCount);
			StringBuffer str = new StringBuffer();
			for (int i = 0; i < pStyleErrorVO.getSkusNotAssociated().size(); i++) {
				str.append(pStyleErrorVO.getSkusNotAssociated().get(i));
				if(i < pStyleErrorVO.getSkusNotAssociated().size()-TRUProductFeedConstants.NUMBER_ONE){
					str.append(TRUProductFeedConstants.CAMMA);
				}
			}

			dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
					new HSSFRichTextString(pStyleErrorVO.getFileName()));
			dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
					new HSSFRichTextString(Integer.toString(rowCount)));
			dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
					new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
			dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(pStyleErrorVO.getId()));
			dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(str.toString()));
			dataRow.createCell(FeedConstants.NUMBER_FIVE).setCellValue(new HSSFRichTextString(TRUFeedConstants.EMPTY_STRING));
			dataRow.createCell(FeedConstants.NUMBER_SIX).setCellValue(new HSSFRichTextString(TRUProductFeedConstants.STYLE_ERROR_MSG_NOT_ASS));
			
			
			dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_FIVE).setCellStyle(pDataCellStyle);
			dataRow.getCell(FeedConstants.NUMBER_SIX).setCellStyle(pDataCellStyle);
			
			rowCount++;
			
		}
		return rowCount;
	}

	/**
	 * Product canonical attach data.
	 *
	 * @param pInvalidRecords the invalid records
	 * @param pSheet the sheet
	 * @param pRowCount the row count
	 * @param pDataCellStyle the data cell style
	 * @return the int
	 */
	private int productCanonicalAttachData(
			List<FeedSkippableException> pInvalidRecords, HSSFSheet pSheet,
			int pRowCount, HSSFCellStyle pDataCellStyle) {
		
		int rowCount = pRowCount;
		
		for (FeedSkippableException fse : pInvalidRecords) {
			HSSFRow dataRow = pSheet.createRow(rowCount);
			if (fse.getFeedItemVO() instanceof TRUProductFeedVO && !(fse.getFeedItemVO() instanceof TRUCollectionProductVO)) {
				
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
						new HSSFRichTextString(((TRUProductFeedVO) fse.getFeedItemVO()).getFileName()));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
						new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(((TRUProductFeedVO) fse.getFeedItemVO()).getRepositoryId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(TRUFeedConstants.EMPTY_STRING));
				dataRow.createCell(FeedConstants.NUMBER_FIVE).setCellValue(new HSSFRichTextString(TRUFeedConstants.EMPTY_STRING));
				dataRow.createCell(FeedConstants.NUMBER_SIX).setCellValue(new HSSFRichTextString(((TRUProductFeedVO) fse.getFeedItemVO()).getErrorMessage()));
				
				
				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FIVE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_SIX).setCellStyle(pDataCellStyle);
				
				rowCount++;
			} else if (fse.getFeedItemVO() instanceof TRUSkuFeedVO) {
				
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
						new HSSFRichTextString(((TRUSkuFeedVO) fse.getFeedItemVO()).getFileName()));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
						new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(TRUFeedConstants.EMPTY_STRING));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(((TRUSkuFeedVO) fse.getFeedItemVO()).getRepositoryId()));
				dataRow.createCell(FeedConstants.NUMBER_FIVE).setCellValue(new HSSFRichTextString(((TRUSkuFeedVO) fse.getFeedItemVO()).getOnlinePID()));
				dataRow.createCell(FeedConstants.NUMBER_SIX).setCellValue(new HSSFRichTextString(((TRUSkuFeedVO) fse.getFeedItemVO()).getErrorMessage()));
				
				
				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FIVE).setCellStyle(pDataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_SIX).setCellStyle(pDataCellStyle);
				
				rowCount++;
			}
			
		}
		return rowCount;
	}

	/**
	 * Product canonical attach header.
	 *
	 * @param pRowhead the rowhead
	 * @param pRowCellStyle the row cell style
	 */
	private void productCanonicalAttachHeader(HSSFRow pRowhead,
			HSSFCellStyle pRowCellStyle) {
		pRowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getFeedEnvConfiguration().getFileName());
		pRowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getFeedEnvConfiguration().getRecordNumber());
		pRowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getFeedEnvConfiguration().getTimeStamp());
		pRowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getFeedEnvConfiguration().getProduct());
		pRowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(getFeedEnvConfiguration().getSku());
		pRowhead.createCell(FeedConstants.NUMBER_FIVE).setCellValue(getFeedEnvConfiguration().getPid());
		pRowhead.createCell(FeedConstants.NUMBER_SIX).setCellValue(
				getFeedEnvConfiguration().getAttachmentExceptionReasonLabel());
		
		pRowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_FIVE).setCellStyle(pRowCellStyle);
		pRowhead.getCell(FeedConstants.NUMBER_SIX).setCellStyle(pRowCellStyle);
	}

	/**
	 * Web site taxonomy registry classification attachment.
	 *
	 * @param pInvalidRecords the invalid records
	 * @param pAttachement the attachement
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void webSiteTaxonomyRegistryClassificationAttachment(
			List<FeedSkippableException> pInvalidRecords, File pAttachement)
			throws FileNotFoundException, IOException {
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		outputStream = new FileOutputStream(pAttachement);
		workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(TRUProductFeedConstants.SHEET);
		int rowCount = FeedConstants.NUMBER_ZERO;
		webSiteTaxRegistryClassAttachHeader(workbook, sheet, rowCount);
		
		rowCount = FeedConstants.NUMBER_ONE;
		webSiteRegistryClassAttachData(pInvalidRecords, workbook, sheet,
				rowCount);	
		if (null != workbook) {
			workbook.write(outputStream);
		}
	}

	/**
	 * Web site registry class attach data.
	 *
	 * @param pInvalidRecords the invalid records
	 * @param pWorkbook the workbook
	 * @param pSheet the sheet
	 * @param pRowCount the row count
	 */
	private void webSiteRegistryClassAttachData(
			List<FeedSkippableException> pInvalidRecords,
			HSSFWorkbook pWorkbook, HSSFSheet pSheet, int pRowCount) {
		
		int rowCount = pRowCount;
		
		HSSFCellStyle dataCellStyle = pWorkbook.createCellStyle();
		dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		for (FeedSkippableException fse : pInvalidRecords) {
			HSSFRow dataRow = pSheet.createRow(rowCount);
			if (fse.getFeedItemVO() instanceof TRUCategory) {
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
						new HSSFRichTextString(TRUProductFeedConstants.EMPTY_STRING));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
						new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(((TRUCategory) fse.getFeedItemVO()).getRepositoryId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(fse.getMessage()));
				
				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				
				rowCount++;
				
			} else if (fse.getFeedItemVO() instanceof TRUClassification) {
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
						new HSSFRichTextString(((TRUClassification) fse.getFeedItemVO()).getFileName()));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
						new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(((TRUClassification) fse.getFeedItemVO()).getRepositoryId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(((TRUClassification) fse.getFeedItemVO()).getErrorMessage()));
				
				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				
				rowCount++;
			} else if(fse.getFeedItemVO() instanceof TRUCollectionProductVO){
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(
						new HSSFRichTextString(((TRUCollectionProductVO) fse.getFeedItemVO()).getFileName()));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(
						new HSSFRichTextString(Integer.toString(pRowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(((TRUCollectionProductVO) fse.getFeedItemVO()).getRepositoryId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(((TRUCollectionProductVO) fse.getFeedItemVO()).getErrorMessage()));
				
				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				
				rowCount++;
			}
			
		}
	}

	/**
	 * Web site tax registry class attach header.
	 *
	 * @param pWorkbook the workbook
	 * @param pSheet the sheet
	 * @param pRowCount the row count
	 */
	private void webSiteTaxRegistryClassAttachHeader(HSSFWorkbook pWorkbook,
			HSSFSheet pSheet, int pRowCount) {
		HSSFRow rowhead = pSheet.createRow(pRowCount);
		HSSFCellStyle rowCellStyle = pWorkbook.createCellStyle();
		rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		rowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getFeedEnvConfiguration().getFileName());
		rowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getFeedEnvConfiguration().getRecordNumber());
		rowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getFeedEnvConfiguration().getTimeStamp());
		rowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getFeedEnvConfiguration().getClassification());
		rowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(
				getFeedEnvConfiguration().getAttachmentExceptionReasonLabel());
		
		rowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(rowCellStyle);
		rowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(rowCellStyle);
		rowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(rowCellStyle);
		rowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(rowCellStyle);
		rowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(rowCellStyle);
	}

	/**
	 * Attachement file name.
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachementFileName(String pFileName) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: attachementFileName()");

		StringBuffer stringBuffer = new StringBuffer();
		
		if(pFileName.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)){
			stringBuffer = stringBuffer.append(TRUProductFeedConstants.REGISTRY_CATEGORIZATION_ERROR_FILENAME).append(TRUProductFeedConstants.PROCESS_ON).append(((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT_1)).format(new Date()))).append(FeedConstants.DOT).append(TRUProductFeedConstants.XLS);
		} else if(pFileName.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)){
			stringBuffer = stringBuffer.append(TRUProductFeedConstants.WEB_SITE_TAXONOMY_ERROR_FILENAME).append(TRUProductFeedConstants.PROCESS_ON).append(((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT_1)).format(new Date()))).append(FeedConstants.DOT).append(TRUProductFeedConstants.XLS);
		} else if(pFileName.contains(TRUFeedConstants.ATG_PRODUCT)){
			stringBuffer = stringBuffer.append(TRUProductFeedConstants.PRODUCT_CANONICAL_ERROR_FILENAME).append(TRUProductFeedConstants.PROCESS_ON).append(((new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT_1)).format(new Date()))).append(FeedConstants.DOT).append(TRUProductFeedConstants.XLS);
		}

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: attachementFileName()");

		return stringBuffer.toString();
	}

	/**
	 * Move success file to success dir.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pSuccessDir
	 *            the success dir
	 */
	public void moveSuccessFileToSuccessDir(String pFileName, String pProcessingDir, String pSuccessDir) {

		getLogger().vlogDebug(
				"Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: moveSuccessFileToSuccessDir()");

		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pSuccessDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && pFileName != null) {
						String[] lFileList = pFileName.split(TRUFeedConstants.DOLLAR);
						for (String fileName : lFileList) {
							if (file.getName().contains(fileName)) {
								file.renameTo(new File(toFolder, file.getName()));
							}
						}
					}
				}
			}
		} else if(sourceFolder.exists() || sourceFolder.isDirectory()){
			String messageSubject = TRUProductFeedConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(pFileName, messageSubject);
		} else if(sourceFolder.canRead()){
			String messageSubject = TRUProductFeedConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(pFileName, messageSubject);
		}

		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: moveSuccessFileToSuccessDir()");

	}

	/**
	 * Trigger feed processed successfully email.
	 *
	 * @param pFeedType            the feed type
	 * @param pSuccessRecordCount            the success record count
	 * @param pErrorRecordCount            the error record count
	 * @param pSequenceNumber the sequence number
	 */
	public void triggerFeedProcessedSuccessfullyEmail(String pFeedType, long pSuccessRecordCount, long pErrorRecordCount, String pSequenceNumber) {

		getLogger().vlogDebug( 
				"Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedProcessedSuccessfullyEmail()");
		String subject = null;
		if (pFeedType.equals(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
			
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getWebsiteFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE6+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		} else if (pFeedType.equals(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getRegistryFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE6+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));
		} else if (pFeedType.equals(TRUFeedConstants.ATG_PRODUCT)) {
			subject = TRUProductFeedConstants.EMAIL_FAIL_MESSAGE1+getFeedEnvConfiguration().getEnv()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE2+getFeedEnvConfiguration().getCatProdFeedName()+TRUProductFeedConstants.EMAIL_FAIL_MESSAGE6+((new SimpleDateFormat(TRUProductFeedConstants.EMAIL_FAIL_MESSAGE5)).format(new Date()));

		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, subject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, TRUProductFeedConstants.JOB_SUCCESS);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pSuccessRecordCount + pErrorRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pErrorRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		getFeedEmailService().sendFeedSuccessEmail(FeedConstants.CAT_JOB_NAME, templateParameters);

		getLogger().vlogDebug(
				"End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: triggerFeedProcessedSuccessfullyEmail()");

	}

	/**
	 * Calculate percentage.
	 * 
	 * @param pTotalRecordCount
	 *            the total record count
	 * @param pErrorRecordCount
	 *            the error record count
	 * @return the double
	 */
	private Double calculatePercentage(long pTotalRecordCount, long pErrorRecordCount) {
		BigDecimal percentage = new BigDecimal(TRUProductFeedConstants.DOUBLE_NUMBER_ZERO);
		BigDecimal totalRecordCount = BigDecimal.valueOf(pTotalRecordCount);
		BigDecimal errorRecordCount = BigDecimal.valueOf(pErrorRecordCount);
		if (totalRecordCount != null && totalRecordCount.doubleValue() > TRUProductFeedConstants.NUMBER_ZERO) {
			percentage = errorRecordCount.multiply(BigDecimal.valueOf(TRUFeedConstants.NUMBER_HUNDRED)).divide(
					totalRecordCount, FeedConstants.NUMBER_TWO, BigDecimal.ROUND_HALF_UP);
		}
		return percentage.doubleValue();
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FEED_STATUS, null, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}