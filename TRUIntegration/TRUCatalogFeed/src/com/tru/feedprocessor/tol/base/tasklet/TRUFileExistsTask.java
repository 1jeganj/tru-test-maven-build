package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class FileExistsTask.
 * 
 * File exist task for validate the path and file in respective path
 * 
 * @author Professional Access
 * @version 1.1
 */
public class TRUFileExistsTask extends DataExistsTask {

	/**
	 * method to check for file existence.
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws Exception
	 *             the exception
	 *             
	 *             
	 */
	@Override
	protected void doExecute() throws FeedSkippableException, Exception {

		getLogger().vlogDebug("Start @Class: TRUFileExistsTask, @method: doExecute()");

		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		if (curExecCntx != null && curExecCntx.getFileName() != null &&
				!curExecCntx.getFileName().endsWith(FeedConstants.TABLE)) {
			String fileName = curExecCntx.getFileName();
			String[] lFileList = fileName.split(TRUFeedConstants.DOLLAR);
			if (lFileList != null && lFileList.length > TRUProductFeedConstants.NUMBER_ZERO) {
				for (String file : lFileList) {
					File lFile = new File(curExecCntx.getFilePath() + file);
					if (lFile == null || !lFile.exists() || !lFile.isFile() || !lFile.canRead()) {
						if (file.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
							logFailedMessage(startDate, TRUFeedConstants.FILE_DOES_NOT_EXISTS_TAXONOMY);
							throw new FeedSkippableException(getPhaseName(), getStepName(),
									TRUFeedConstants.FILE_DOES_NOT_EXISTS_TAXONOMY, null, null);
						} else if (file.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
							logFailedMessage(startDate, TRUFeedConstants.FILE_DOES_NOT_EXISTS_REGISTRY);
							throw new FeedSkippableException(getPhaseName(), getStepName(),
									TRUFeedConstants.FILE_DOES_NOT_EXISTS_REGISTRY, null, null); 
						} else if (file.contains(TRUFeedConstants.PRODUCT_CANONICAL_FILE)) {
							logFailedMessage(startDate, TRUFeedConstants.FILE_DOES_NOT_EXISTS_PRODUCT);
							throw new FeedSkippableException(getPhaseName(), getStepName(),
									TRUFeedConstants.FILE_DOES_NOT_EXISTS_PRODUCT, null, null);
						}
					}
				}
			} else {
				logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
				throw new Exception(FeedConstants.FILE_DOES_NOT_EXISTS);
			}
			logSuccessMessage(startDate, TRUProductFeedConstants.FILES_MOVED_SUCCESS);
		} else {
			logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
			throw new Exception(FeedConstants.FILE_DOES_NOT_EXISTS);
		}

		getLogger().vlogDebug("End @Class: TRUFileExistsTask, @method: doExecute()");

	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
