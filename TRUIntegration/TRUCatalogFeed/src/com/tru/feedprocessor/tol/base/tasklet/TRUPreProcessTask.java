package com.tru.feedprocessor.tol.base.tasklet;

import atg.repository.RepositoryImpl;

import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;

/**
 * The Class TRUPreProcessTask.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUPreProcessTask extends PreProcessTask {

	/**
	 * method doExecute() .
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	protected void doExecute() throws FeedSkippableException {

		RepositoryImpl rep = (RepositoryImpl) getRepository(TRUFeedConstants.CATALOG_REPOSITORY);
		rep.invalidateCaches();
		RepositoryImpl rep1 = (RepositoryImpl) getRepository(TRUFeedConstants.CATALOG_REPOSITORY_VER);
		rep1.invalidateCaches();

	}
}
