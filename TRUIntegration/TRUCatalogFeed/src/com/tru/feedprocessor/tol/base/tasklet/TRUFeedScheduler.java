package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.BatchInvoker;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;

/**
 * The Class TRUFeedScheduler. This class is used to run feed schedule tasks.
 * @version 1.0
 * @author Professional Access
 */
public class TRUFeedScheduler extends SingletonSchedulableService {

	/** property to hold feed job holder. */
	private BatchInvoker mFeedJob;
	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** property to hold Enabled flag. */
	private boolean mEnabled;

	/**
	 * Gets the feed job.
	 * 
	 * @return the feed job
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 * 
	 * @param pFeedJob
	 *            the new feed job
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		this.mFeedJob = pFeedJob;
	}

	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param pEnabled
	 *            the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * This method invokes to run the scheduledTask.
	 * 
	 * @param pScheduler
	 *            the scheduler
	 * @param pJob
	 *            the job
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {

		vlogDebug("Start @Class: TRUFeedScheduler, @method: doScheduledTask()");

		if (isEnabled()) {
			processCatalogFeed();
		} else {
			String messageSubject = TRUFeedConstants.SERVICE_NOT_AVAILABLE;
			Map<String, Object> templateParameters = new HashMap<String, Object>();
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, messageSubject);
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SITE_ID, getFeedEnvConfiguration().getSiteId());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, getFeedEnvConfiguration()
					.getFailureMailPriority3());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_QUEUE, getFeedEnvConfiguration().getQueue());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_NOTES, getFeedEnvConfiguration().getNotes());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SOFTWARE, getFeedEnvConfiguration()
					.getSoftware() + FeedConstants.SPACE + getFeedEnvConfiguration().getCatFeedName());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_NAME, getFeedEnvConfiguration().getOSName());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_JAVA_VERSION, getFeedEnvConfiguration()
					.getJavaVersion());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_USER_NAME, getFeedEnvConfiguration().getUser());
			templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
					.getOSHostName());
			getFeedEmailService().sendFeedFailureEmail(FeedConstants.CAT_JOB_NAME, templateParameters);
		}

		vlogDebug("End @Class: TRUFeedScheduler, @method: doScheduledTask()");
	}

	/**
	 * Process catalog feed.
	 */
	public void processCatalogFeed() {

		vlogDebug("Start @Class: TRUFeedScheduler, @method: processCatalogFeed()");

		BatchInvoker lBatchInvoker = new BatchInvoker();
		lBatchInvoker.loadContext(new String[] { FeedConstants.JOB_CONFIG_PATH, FeedConstants.CAT_FEED_CONFIG_PATH });
		Calendar lCDateTime = Calendar.getInstance();
		String[] input = {
				FeedConstants.CAT_JOB_NAME,
				TRUProductFeedConstants.FILE_TYPE
						+ TRUProductFeedConstants.EQUALS
						+ TRUProductFeedConstants.REGULAR_FEED,
				FeedConstants.CAT_JOB_NAME + TRUProductFeedConstants.EQUALS
						+ lCDateTime.getTimeInMillis() };

		lBatchInvoker.startJob(input);

		vlogDebug("End @Class: TRUFeedScheduler, @method: processCatalogFeed()");

	}

}
