package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedConstants;
import com.tru.feedprocessor.tol.base.constants.TRUFeedErrorConstants;
import com.tru.feedprocessor.tol.base.constants.TRUProductFeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class ValidateFileTask. This class is used for validating the input feed file. 
 * Project team has to override doExecute() method and do the file validation. 
 * If the file validation fails then they should throw FeedSkippableException.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUValidateFileTask extends ValidateDataTask {
	/* The mXmlSchemaNsURI */
	/** The m xml schema ns uri. */
	private String mXmlSchemaNsURI;

	/** The Schema xsd uri. */
	private Map<String, String> mSchemaXsdUri;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	

	/**
	 * Gets the xml schema ns uri.
	 * 
	 * @return the xmlSchemaNsURI
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}

	/**
	 * Sets the xml schema ns uri.
	 * 
	 * @param pXmlSchemaNsURI
	 *            the xmlSchemaNsURI to set
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		mXmlSchemaNsURI = pXmlSchemaNsURI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() This is the blank method where project team has to put
	 * the code which will be performed during validation of the file
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {

		getLogger().vlogDebug("Start @Class: TRUValidateFileTask, @method: doExecute()");

		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		String fileName = curExecCntx.getFileName();
		String fileType = null;
		String[] lFileList = fileName.split(TRUFeedConstants.DOLLAR);
		String startDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		for (String lfileName : lFileList) {
			File file = new File(curExecCntx.getFilePath() + lfileName);
			if (lfileName.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
				fileType = TRUFeedConstants.WEB_SITE_TAXONOMY;
			} else if (lfileName.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
				fileType = TRUFeedConstants.REGISTRY_CATEGORIZATION;
			} else if (lfileName.contains(TRUFeedConstants.ATG_PRODUCT)) {
				fileType = TRUFeedConstants.ATG_PRODUCT;
			}
			boolean flag = validateFile(file, fileType);

			if (!flag) {
				if (lfileName.contains(TRUFeedConstants.WEB_SITE_TAXONOMY)) {
					logFailedMessage(startDate, TRUFeedConstants.INVALID_FILE_TAXANOMY, lfileName);
					throw new FeedSkippableException(getPhaseName(), getStepName(), TRUFeedConstants.INVALID_FILE_TAXANOMY,
							null, null);
				} else if (lfileName.contains(TRUFeedConstants.REGISTRY_CATEGORIZATION)) {
					logFailedMessage(startDate, TRUFeedConstants.INVALID_FILE_REGISTRY, lfileName);
					throw new FeedSkippableException(getPhaseName(), getStepName(), TRUFeedConstants.INVALID_FILE_REGISTRY,
							null, null);
				} else if (lfileName.contains(TRUFeedConstants.ATG_PRODUCT)) {
					logFailedMessage(startDate, TRUFeedConstants.INVALID_FILE_PRODUCT, lfileName);
					throw new FeedSkippableException(getPhaseName(), getStepName(), TRUFeedConstants.INVALID_FILE_PRODUCT,
							null, null);
				}
			}
		}
		logSuccessMessage( startDate, TRUProductFeedConstants.FILES_VALID, fileName);
		getLogger().vlogDebug("End @Class: TRUValidateFileTask, @method: doExecute()");

	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logFailedMessage(String pStartDate, String pMessage, String pFileName) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_VALIDATION, pFileName, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage ,String pFileName) {
		String endDate = new SimpleDateFormat(TRUProductFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductFeedConstants.CATALOG_FEED, TRUProductFeedConstants.FILE_VALIDATION, pFileName, extenstions);
	}

	/**
	 * This method will validate the xml with xsd of the respective files.
	 * 
	 * @param pFile
	 *            - File
	 * @param pFileType
	 *            the file type
	 * @return boolean
	 */
	public boolean validateFile(File pFile, String pFileType) {

		getLogger().vlogDebug("Start @Class: TRUValidateFileTask, @method: validateFile()");

		boolean isValidFeed = true;

		if (FilenameUtils.getExtension(pFile.toString()).equals(FeedConstants.XML)) {

			SchemaFactory schemaFactory = null;
			Schema schema = null;
			Source xmlFile = new StreamSource(pFile);
			String lSchemaFileXsdUri = getSchemaXsdUri().get(pFileType);
			String lXmlSchemaNsURI = getXmlSchemaNsURI();

			try {
				if (StringUtils.isEmpty(lXmlSchemaNsURI)) {
					getLogger().logWarning(TRUFeedErrorConstants.XML_SCHEMA_NS_URI);
				} else {
					schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
				}

				if (StringUtils.isEmpty(lSchemaFileXsdUri)) {
					getLogger().logError(TRUFeedErrorConstants.XSD_URI_NULL);
				} else {
						//schema = schemaFactory.newSchema(new File(getSchemaFileXsdUri().get(pFileType)));
						
						schema = schemaFactory.newSchema(getClass().getClassLoader().getResource(lSchemaFileXsdUri));
				}

				Validator validator = schema.newValidator();

				validator.validate(xmlFile);
			} catch (SAXException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + TRUFeedErrorConstants.FILE_NOT_VALID , ex);
				}
			} catch (IOException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + TRUFeedErrorConstants.FILE_NOT_VALID , ex);
				}
			}
		}

		getLogger().vlogDebug("End @Class: TRUValidateFileTask, @method: validateFile()");

		return isValidFeed;
	}

	/**
	 * Gets the schema xsd uri.
	 *
	 * @return the schema xsd uri
	 */
	public Map<String, String> getSchemaXsdUri() {
		return mSchemaXsdUri;
	}

	/**
	 * Sets the schema xsd uri.
	 *
	 * @param pSchemaXsdUri the schema xsd uri
	 */
	public void setSchemaXsdUri(Map<String, String> pSchemaXsdUri) {
		mSchemaXsdUri = pSchemaXsdUri;
	}
}
