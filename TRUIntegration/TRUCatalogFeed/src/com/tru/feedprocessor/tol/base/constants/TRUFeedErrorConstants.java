package com.tru.feedprocessor.tol.base.constants;

/**
 * The Class TRUFeedErrorConstants will hold all error constants for the whole feed module.
 * @author Professional Access
 * @version 1.0
 */
public class TRUFeedErrorConstants {

	/**
	 * The Property to hold FILE_NOT_FOUND_EXCEPTION.
	 */
	public static final String FILE_NOT_FOUND_EXCEPTION = "FileNotFoundException ";

	/** The property to hold IOEXCEPTION. */
	public static final String IOEXCEPTION = "IOException";

	/** The property to hold FILE_NAME_NOT_NULL. */
	public static final String FILE_NAME_NOT_NULL = "File name can not be null in catalog feed configuration";

	/** The property to hold FILE_PATH_NOT_NULL. */
	public static final String FILE_PATH_NOT_NULL = "File path can not be null in catalog feed configuration";

	/** The property to hold ENTITY_LIST_NOT_NULL. */
	public static final String ENTITY_LIST_NOT_NULL = "Entity list or Step entity list can not be null in catalog feed configuration";

	/** The property to hold ENTIY_SQL_NOT_NULL. */
	public static final String ENTIY_SQL_NOT_NULL = "Entity sql can not be null in catalog feed configuration";

	/** The property to hold ENTITY_ITEMDESCRIPTOR_NOT_NULL. */
	public static final String ENTITY_ITEMDESCRIPTOR_NOT_NULL = "entityToItemDescriptorNameMap can not be null in catalog feed configuration";

	/** The property to hold ENTITY_TO_REPOSITORY_NAME_MAP. */
	public static final String ENTITY_TO_REPOSITORY_NAME_MAP = "entityToRepositoryNameMap can not be null in catalog feed configuration";

	/** The property to hold FILE_LOADER_FQCN. */
	public static final String FILE_LOADER_FQCN = "File Loader class FQCN can not be null in Catalog feed configuration";

	/** The property to hold FILE_PARSE_FQCN. */
	public static final Object FILE_PARSE_FQCN = "File Parse class FQCN must be a valid entry";

	/** The property to hold SQL_MANADATORY. */
	public static final String SQL_MANADATORY = "SQL is mandatory for ";

	/** The property to hold ENTITY_LIST_CAT_FEED. */
	public static final String ENTITY_LIST_CAT_FEED = " entity provided in entity list in Catalog feed configuration";

	/** The property to hold HOSTNAME_FTP_SFTP. */
	public static final String HOSTNAME_FTP_SFTP = "Host Name for FTP and SFTP is mandatory for file downloading in catalog feed configuration";

	/** The property to hold PORT_NO_FTP_SFTP. */
	public static final String PORT_NO_FTP_SFTP = "Port NO for FTP and SFTP is mandatory for file downloading in catalog feed configuration";

	/** The property to hold SERVER_IP_FTP_SFTP. */
	public static final String SERVER_IP_FTP_SFTP = "Server IP for FTP and SFTP is mandatory for file downloading in catalog feed configuration";

	/** The property to hold PROTOCOL_FTP_SFTP_NOTNULL. */
	public static final String PROTOCOL_FTP_SFTP_NOTNULL = "Protocol type for FTP or SFTP can not be null in catalog feed configuration";

	/** The property to hold DOWNLOAD_FILE_TYPE_NOT_NULL. */
	public static final String DOWNLOAD_FILE_TYPE_NOT_NULL = "Download file type can not be null in catalog feed configuration";

	/** The property to hold CAT_FEED_CONFIG. */
	public static final Object CAT_FEED_CONFIG = "Please provide proper configuration for catalog feed configuration";

	/** The property to hold XML_SCHEMA_NS_URI Uri is null or Empty in Validate File Task. */
	public static final String XML_SCHEMA_NS_URI = "XML_SCHEMA_NS_URI Uri is null or Empty in Validate File Task";

	/** The property to hold XSD_URI_NULL. */
	public static final String XSD_URI_NULL = "XSD Uri is null or Empty in Validate File Task";

	/** The property to hold FILE_NOT_VALID. */
	public static final String FILE_NOT_VALID = "        File is NOT valid     Reson is  --> ";
	/** The property to hold PARSE_EXCEPTION. */
	public static final String PARSE_EXCEPTION = "parsing exception :";
	/** The property to hold ILLEGALARGUMENT_EXCEPTION. */
	public static final String ILLEGALARGUMENT_EXCEPTION = "illegar argument exception";

}
