alter table tru_classification drop column COLLECTION_IMAGE;
drop table tru_collection_image;
CREATE TABLE tru_collection_image (
	product_id 		varchar2(254)	NOT NULL REFERENCES dcs_product(product_id),
	collection_image 	varchar2(255)	NULL,
	PRIMARY KEY(product_id)
);