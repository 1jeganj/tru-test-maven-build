ALTER TABLE tru_product DROP COLUMN REGISTRY_CLASSIFICATION_ID;

ALTER TABLE tru_product add  REGISTRY_CLASSIFICATION_ID varchar2(40) null;

drop table tru_prd_parent_clfs;

CREATE TABLE tru_prd_parent_clfs (
	product_id 		varchar2(40)	NOT NULL REFERENCES dcs_product(product_id),
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL,
	PRIMARY KEY(product_id, sequence_num)
);

drop table tru_sku_parent_clfs; 

CREATE TABLE tru_sku_parent_clfs (
	sku_id 			varchar2(40)	NOT NULL REFERENCES dcs_sku(sku_id),
	sequence_num 		INTEGER	NOT NULL,
	parent_classification 	varchar2(40)	NULL,
	PRIMARY KEY(sku_id, sequence_num)
);