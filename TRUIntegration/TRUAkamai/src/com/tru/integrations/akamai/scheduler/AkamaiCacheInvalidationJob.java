package com.tru.integrations.akamai.scheduler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.nucleus.GenericService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tru.integrations.akamai.beans.PurgeRequest;
import com.tru.integrations.akamai.beans.PurgeResponse;
import com.tru.integrations.akamai.beans.PurgeStatusResponse;
import com.tru.integrations.akamai.constants.AkamaiConstants;
import com.tru.integrations.akamai.email.AkamaiEmailSender;
import com.tru.integrations.akamai.service.AkamaiServiceImpl;
import com.tru.integrations.akamai.util.AkamaiPRConfiguration;
import com.tru.integrations.exception.TRUIntegrationException;

/**
 * This is a Job class for Akamai Cache Invalidation.
 * @version 1.0
 * @author Professional Access
 */
public class AkamaiCacheInvalidationJob extends GenericService {
	
	/**
	 * Holds the AkamaiServiceImpl.
	 */
	private AkamaiServiceImpl mAkamaiService;
	
	/**
	 * Holds the AkamaiPRConfiguration.
	 */
	private AkamaiPRConfiguration mAkamaiPRConfiguration;
	
	/**
	 * Holds the AkamaiEmailSender.
	 */
	private AkamaiEmailSender mAkamaiEmailSender;
	
	/**
	 * Holds the time format for purge email
	 */
	private String mEmailTimeFormat;
	
		
	/**
	 * This method used for cache invalidation.
	 * @throws TRUIntegrationException integration exception
	 */
	public void cacheInvalidation() throws TRUIntegrationException {
		if(isLoggingDebug()){
			logDebug("AkamaiCacheInvalidationJob :: cacheInvalidation() method :: STARTS ");
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(mEmailTimeFormat);
		Calendar calendar = Calendar.getInstance(Locale.US);
		
		try {
						
			Map<String,Object> mapObject=new HashMap<String,Object>();
			mapObject.put(AkamaiConstants.DOMAIN, mAkamaiPRConfiguration.getDomain());
			
			mapObject.put(AkamaiConstants.REQUEST_SUBMISSION_TIME, dateFormat.format(calendar.getTime()));
			//System.out.println(mapObject.get("requestSubmissionTime"));
			String purgeRequest = createRequest(mapObject);
			String stringPurgeResponse = mAkamaiService.purgeRequest(purgeRequest);
			
			PurgeResponse purgeResponse = parsePurgeResponse(stringPurgeResponse);
			
			if(purgeResponse.getHttpStatus() == AkamaiConstants.HTTP_STATUS_201) {
				String progressURI = purgeResponse.getProgressUri();
				mapObject.put(AkamaiConstants.PROGESS_URI, progressURI);
				mapObject.put(AkamaiConstants.PURGE_ID, purgeResponse.getPurgeId());
				//	int waitingSeconds = Integer.parseInt(String.valueOf(purgeResponse.getEstimatedSeconds()));
				int waitingSeconds = purgeResponse.getEstimatedSeconds() != null ?	purgeResponse.getEstimatedSeconds() : 0 ;
				
				String purgeStatus = AkamaiConstants.IN_PROGRESS;
				
				if (progressURI != null) {
					while(purgeStatus.equalsIgnoreCase(AkamaiConstants.IN_PROGRESS)) {
						int milliSeconds = waitingSeconds * AkamaiConstants.INTEGER_NUMBER_THOUSAND;
						Thread.sleep(milliSeconds);
						String stringPurgeStatusResponse = mAkamaiService.purgeStatus(progressURI);
						PurgeStatusResponse purgeStatusResponse = parsePurgeStatusResponse(stringPurgeStatusResponse);
						purgeStatus = purgeStatusResponse.getPurgeStatus();
						if (purgeStatusResponse.getPingAfterSeconds() != null) {
							waitingSeconds = purgeStatusResponse.getPingAfterSeconds();
						}
					}
					if (purgeStatus.equalsIgnoreCase(AkamaiConstants.DONE)) {
						mapObject.put(AkamaiConstants.REQUEST_COMPLETION_TIME, dateFormat.format(calendar.getTime()));
						//System.out.println(mapObject.get("requestCompletionTime"));
						mAkamaiEmailSender.sendAkamaiCacheInvalidationSuccessEmail(mapObject);
					}
				}	
			} else {
				mAkamaiEmailSender.sendAkamaiCacheInvalidationFailureEmail();
			}
		} catch (InterruptedException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			mAkamaiEmailSender.sendAkamaiCacheInvalidationFailureEmail();
			throw new TRUIntegrationException(e);
		}
		if(isLoggingDebug()){
			logDebug("AkamaiCacheInvalidationJob :: cacheInvalidation() method :: ENDS ");
		}
	}
	
	/**
	 * This method used to create request.
	 * @param pMapObject map of akamai purging configurations
	 * @return request purge request as string
	 * @throws TRUIntegrationException integration exception
	 */
	private String createRequest(Map<String, Object> pMapObject) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("AkamaiCacheInvalidationJob :: createRequest() :: START");
		}
		PurgeRequest purgeRequest = new PurgeRequest(); 
		purgeRequest.setType(mAkamaiPRConfiguration.getType());
		purgeRequest.setAction(mAkamaiPRConfiguration.getAction());
		purgeRequest.setDomain(mAkamaiPRConfiguration.getDomain());
		List<String> cpCodesList = mAkamaiPRConfiguration.getCodesList();
		purgeRequest.setObjects(cpCodesList);
		Integer cpCodeCount = 0;
		
		if(cpCodesList !=null){
			cpCodeCount = cpCodesList.size();
		}
		pMapObject.put(AkamaiConstants.CODE_LIST, cpCodesList);
		pMapObject.put(AkamaiConstants.CODE_COUNT, cpCodeCount);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String request;
		try {
			request = objectMapper.writeValueAsString(purgeRequest);
			if (isLoggingDebug()) {
				logDebug("AkamaiCacheInvalidationJob :: createRequest() :: END");
				vlogDebug("Request: \n {0}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(purgeRequest));
			}
		} catch (JsonProcessingException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e);
		}
		return request;
	}
	
	/**
	 * This method used to parse purge response.
	 * @param pPurgeResponse purge response
	 * @return purgeResponse parsed purge response
	 * @throws TRUIntegrationException integration exception
	 */
	private PurgeResponse parsePurgeResponse(String pPurgeResponse) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("AkamaiCacheInvalidationJob :: parsePurgeResponse() :: START");
		}
		ObjectMapper objectMapper = new ObjectMapper();
		PurgeResponse purgeResponse;
		try {
			purgeResponse = objectMapper.readValue(pPurgeResponse, PurgeResponse.class);
			if (isLoggingDebug()) {
				vlogDebug("Response: \n {0}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(purgeResponse));
				logDebug("AkamaiCacheInvalidationJob :: parsePurgeResponse() :: END");
			}
		} catch (IOException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e.getMessage(), e);
		}
		return purgeResponse;
	}
	
	/**
	 * This method used to parse purge status response.
	 * @param pPurgeStatusResponse purge response status
	 * @return purgeStatusResponse parsed purge response status
	 * @throws TRUIntegrationException integration exception
	 */
	private PurgeStatusResponse parsePurgeStatusResponse(String pPurgeStatusResponse) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("AkamaiCacheInvalidationJob :: parsePurgeStatusResponse() :: START");
		}
		ObjectMapper objectMapper = new ObjectMapper();
		PurgeStatusResponse purgeStatusResponse;
		try {
			purgeStatusResponse = objectMapper.readValue(pPurgeStatusResponse, PurgeStatusResponse.class);
			if (isLoggingDebug()) {
				vlogDebug("Response: \n {0}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(purgeStatusResponse));
				logDebug("AkamaiCacheInvalidationJob :: parsePurgeStatusResponse() :: END");
			}
		} catch (IOException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e.getMessage(), e);
		}
		return purgeStatusResponse;
	}

	/**
	 * @return mAkamaiService the akamaiService
	 */
	public AkamaiServiceImpl getAkamaiService() {
		return mAkamaiService;
	}

	/**
	 * @param pAkamaiService the akamaiService to set
	 */
	public void setAkamaiService(AkamaiServiceImpl pAkamaiService) {
		mAkamaiService = pAkamaiService;
	}

	/**
	 * @return the akamaiPRConfiguration
	 */
	public AkamaiPRConfiguration getAkamaiPRConfiguration() {
		return mAkamaiPRConfiguration;
	}

	/**
	 * @param pAkamaiPRConfiguration the akamaiPRConfiguration to set
	 */
	public void setAkamaiPRConfiguration(AkamaiPRConfiguration pAkamaiPRConfiguration) {
		mAkamaiPRConfiguration = pAkamaiPRConfiguration;
	}

	/**
	 * @return the akamaiEmailSender
	 */
	public AkamaiEmailSender getAkamaiEmailSender() {
		return mAkamaiEmailSender;
	}

	/**
	 * @param pAkamaiEmailSender the akamaiEmailSender to set
	 */
	public void setAkamaiEmailSender(AkamaiEmailSender pAkamaiEmailSender) {
		mAkamaiEmailSender = pAkamaiEmailSender;
	}
	
	/**
	 * @return the EmailTimeFormat
	 */
	public String getEmailTimeFormat() {
		return mEmailTimeFormat;
	}

	/**
	 * @param pEmailTimeFormat the emailTimeFormat to set
	 */
	public void setEmailTimeFormat(String pEmailTimeFormat) {
		this.mEmailTimeFormat = pEmailTimeFormat;
	}
	
}
