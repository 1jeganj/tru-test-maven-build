package com.tru.integrations.akamai.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.integrations.exception.TRUIntegrationException;

/**
 * This class is used to Invalidate the Akamai Cache on Schedule time
 * 
 * @author P.A.
 */
public class AkamaiCacheInvalidationScheduler extends SingletonSchedulableService {
	
	private boolean mEnable;
	
	private AkamaiCacheInvalidationJob mAkamaiCacheInvalidationJob;

	@Override
	public void doScheduledTask(Scheduler pArg0, ScheduledJob pArg1) {
		if(isLoggingDebug()){
			logDebug("AkamaiCacheInvalidationScheduler :: doScheduledTask() method :: STARTS ");
		}
		if (isEnable()) {
			try {
				mAkamaiCacheInvalidationJob.cacheInvalidation();
			} catch (TRUIntegrationException e) {
				if(isLoggingError()) {
					logError(e.getMessage(), e);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("AkamaiCacheInvalidationScheduler :: doScheduledTask() method :: ENDS ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the akamaiCacheInvalidationJob
	 */
	public AkamaiCacheInvalidationJob getAkamaiCacheInvalidationJob() {
		return mAkamaiCacheInvalidationJob;
	}

	/**
	 * @param pAkamaiCacheInvalidationJob the akamaiCacheInvalidationJob to set
	 */
	public void setAkamaiCacheInvalidationJob(
			AkamaiCacheInvalidationJob pAkamaiCacheInvalidationJob) {
		mAkamaiCacheInvalidationJob = pAkamaiCacheInvalidationJob;
	}
	
}
