package com.tru.integrations.akamai.util;

import atg.nucleus.GenericService;

/**
 * This class is used to have Akamai Configuration.
 * 
 * @author Sudheer Guduru
 *
 */
public class AkamaiConfiguration extends GenericService {
	
	private String mAkamaiPurgeRequestURL;
	
	private String mAkamaiPurgeStatusURL;
	
	private String mAkamaiUser;
	
	private boolean mProxyRequired;
	
	private int mRequestTimeout;
	
	private int mReadTimeout;
	
	/**
	 * @return the akamaiPurgeRequestURL
	 */
	public String getAkamaiPurgeRequestURL() {
		return mAkamaiPurgeRequestURL;
	}

	/**
	 * @param pAkamaiPurgeRequestURL the akamaiPurgeRequestURL to set
	 */
	public void setAkamaiPurgeRequestURL(String pAkamaiPurgeRequestURL) {
		mAkamaiPurgeRequestURL = pAkamaiPurgeRequestURL;
	}

	/**
	 * @return the akamaiPurgeStatusURL
	 */
	public String getAkamaiPurgeStatusURL() {
		return mAkamaiPurgeStatusURL;
	}

	/**
	 * @param pAkamaiPurgeStatusURL the akamaiPurgeStatusURL to set
	 */
	public void setAkamaiPurgeStatusURL(String pAkamaiPurgeStatusURL) {
		mAkamaiPurgeStatusURL = pAkamaiPurgeStatusURL;
	}
	
	/**
	 * @return the akamaiUser
	 */
	public String getAkamaiUser() {
		return mAkamaiUser;
	}

	/**
	 * @param pAkamaiUser the akamaiUser to set
	 */
	public void setAkamaiUser(String pAkamaiUser) {
		mAkamaiUser = pAkamaiUser;
	}

	/**
	 * @return the proxyRequired
	 */
	public boolean isProxyRequired() {
		return mProxyRequired;
	}

	/**
	 * @param pProxyRequired the proxyRequired to set
	 */
	public void setProxyRequired(boolean pProxyRequired) {
		mProxyRequired = pProxyRequired;
	}

	/**
	 * @return the requestTimeout
	 */
	public int getRequestTimeout() {
		return mRequestTimeout;
	}

	/**
	 * @param pRequestTimeout the requestTimeout to set
	 */
	public void setRequestTimeout(int pRequestTimeout) {
		mRequestTimeout = pRequestTimeout;
	}

	/**
	 * @return the readTimeout
	 */
	public int getReadTimeout() {
		return mReadTimeout;
	}

	/**
	 * @param pReadTimeout the readTimeout to set
	 */
	public void setReadTimeout(int pReadTimeout) {
		mReadTimeout = pReadTimeout;
	}
	
}
