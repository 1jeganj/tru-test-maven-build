package com.tru.integrations.akamai.util;

import java.util.List;

/**
 * This class configures Akamai request for Browse and Shop 
 * @author Sandeep Vishwakarma
 *
 */
public class AkamaiPRConfiguration {
	private String mType;

	private String mAction;

	private String mDomain;

	private List<String> mCodesList;

	/**
	 * @return the type
	 */
	public String getType() {
		return mType;
	}

	/**
	 * @param pType
	 *            the type to set
	 */
	public void setType(String pType) {
		mType = pType;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return mAction;
	}

	/**
	 * @param pAction
	 *            the action to set
	 */
	public void setAction(String pAction) {
		mAction = pAction;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return mDomain;
	}

	/**
	 * @param pDomain
	 *            the domain to set
	 */
	public void setDomain(String pDomain) {
		mDomain = pDomain;
	}

	/**
	 * @return the codesList
	 */
	public List<String> getCodesList() {
		return mCodesList;
	}

	/**
	 * @param pCodesList
	 *            the codesList to set
	 */
	public void setCodesList(List<String> pCodesList) {
		mCodesList = pCodesList;
	}

}
