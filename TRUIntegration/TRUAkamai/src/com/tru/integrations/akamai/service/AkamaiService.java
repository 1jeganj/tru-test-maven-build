package com.tru.integrations.akamai.service;

import com.tru.integrations.exception.TRUIntegrationException;

/**
 * This interface is used to define akamai purge.
 * @version 1.0
 * @author Professional Access
 *
 */
public interface AkamaiService {

	/**
	 * This method used to purge request.
	 * @param pRequest Request
	 * @return request after purge
	 * @throws TRUIntegrationException integration exception
	 */
	String purgeRequest(String pRequest) throws TRUIntegrationException;

	/**
	 * This method used purge status.
	 * @param pProgressURI ProgressURI
	 * @return purge status
	 * @throws TRUIntegrationException integration exception
	 */
	String purgeStatus(String pProgressURI) throws TRUIntegrationException;

	/**
	 * This method used get queue length.
	 * @return queue length
	 * @throws TRUIntegrationException integration exception
	 */
	String queueLength() throws TRUIntegrationException;

}
