package com.tru.integrations.akamai.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import atg.nucleus.GenericService;

import com.tru.integrations.akamai.constants.AkamaiConstants;
import com.tru.integrations.akamai.util.AkamaiConfiguration;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.http.client.TRUHTTPClient;

/**
 * This class is used to implement akamai purge.
 * @version 1.0
 * @author Professional Access
 *
 */
public class AkamaiServiceImpl extends GenericService implements AkamaiService {
	
	/**
	 * Holds the TRUHTTPClient.
	 */
	private TRUHTTPClient mHttpClient;
	
	/**
	 * Holds the AkamaiConfiguration.
	 */
	private AkamaiConfiguration mAkamaiConfiguration;
	
	/**
	 * This method used purge request.
	 * @param pRequest Request
	 * @return purgeResponse purge response
	 * @throws TRUIntegrationException integration exception
	 */
	@Override
	public String purgeRequest(String pRequest) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("AkamaiServiceImpl :: purgeRequest() :: START");
			vlogDebug("Request: \n {0}", pRequest);
		}
		Map<String, String> headerMap = createHeaderMap();
		String purgeResponse = mHttpClient.executeHttpPostRequest(mAkamaiConfiguration.getAkamaiPurgeRequestURL(),	pRequest, headerMap, false, mAkamaiConfiguration.getReadTimeout(), mAkamaiConfiguration.getRequestTimeout(), mAkamaiConfiguration.isProxyRequired());
		if (isLoggingDebug()) {
			vlogDebug("Response: \n {0}", purgeResponse);
			logDebug("AkamaiServiceImpl :: purgeRequest() :: END");
		}
		return purgeResponse;
	}

	/**
	 * This method used get purge status.
	 * @param pProgressURI progress URI
	 * @return purgeStatusResponse purge status response
	 * @throws TRUIntegrationException integration exception
	 */
	@Override
	public String purgeStatus(String pProgressURI) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("AkamaiServiceImpl :: purgeStatus() :: START");
			vlogDebug("ProgressURI: {0}", pProgressURI);
		}
		String purgeStatusURL = mAkamaiConfiguration.getAkamaiPurgeStatusURL()+pProgressURI;
		Map<String, String> headerMap = createHeaderMap();
		String purgeStatusResponse = mHttpClient.executeHttpGetRequest(purgeStatusURL, headerMap, false, mAkamaiConfiguration.getReadTimeout(), mAkamaiConfiguration.getRequestTimeout(), mAkamaiConfiguration.isProxyRequired());
		if (isLoggingDebug()) {
			vlogDebug("Response: {0}", purgeStatusResponse);
			logDebug("AkamaiServiceImpl :: purgeStatus() :: END");
		}
		return purgeStatusResponse;
	}
	
	/**
	 * This method used to get queue length.
	 * @return null as queue length
	 * @throws TRUIntegrationException integration exception
	 */
	@Override
	public String queueLength() throws TRUIntegrationException {
		return null;
	}
	
	/**
	 * This method used to create authorization.
	 * @return encodedStr encoded string
	 */
	private String createAuthorization() {
		if(isLoggingDebug()){
			logDebug("AkamaiServiceImpl :: createAuthorization() method :: STARTS ");
		}
		String user = mAkamaiConfiguration.getAkamaiUser();
		byte[] encoding = Base64.encodeBase64(user.getBytes());
		String encodedStr = new String(encoding);
		if(isLoggingDebug()){
			logDebug("AkamaiServiceImpl :: createAuthorization() method :: ENDS ");
		}
		return encodedStr;
	}

	/**
	 * This method used to create header map.
	 * @return headerMap map of headers
	 */
	private Map<String, String> createHeaderMap() {
		if(isLoggingDebug()){
			logDebug("AkamaiServiceImpl :: createHeaderMap() method :: STARTS ");
		}
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put(AkamaiConstants.AUTHORIZATION , AkamaiConstants.BASIC + AkamaiConstants.SPACE + createAuthorization());
		headerMap.put(AkamaiConstants.CONTENT_TYPE , AkamaiConstants.APPLICATION_JSON);
		if(isLoggingDebug()){
			logDebug("AkamaiServiceImpl :: createHeaderMap() method :: ENDS ");
		}
		return headerMap;
	}

	/**
	 * @return the httpClient
	 */
	public TRUHTTPClient getHttpClient() {
		return mHttpClient;
	}

	/**
	 * @param pHttpClient the httpClient to set
	 */
	public void setHttpClient(TRUHTTPClient pHttpClient) {
		mHttpClient = pHttpClient;
	}

	/**
	 * @return the akamaiConfiguration
	 */
	public AkamaiConfiguration getAkamaiConfiguration() {
		return mAkamaiConfiguration;
	}

	/**
	 * @param pAkamaiConfiguration the akamaiConfiguration to set
	 */
	public void setAkamaiConfiguration(AkamaiConfiguration pAkamaiConfiguration) {
		mAkamaiConfiguration = pAkamaiConfiguration;
	}

}
