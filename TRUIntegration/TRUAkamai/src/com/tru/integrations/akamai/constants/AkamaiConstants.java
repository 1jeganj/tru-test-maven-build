package com.tru.integrations.akamai.constants;

import com.tru.integrations.constants.TRUConstants;

/**
 * This class is used to store the Constants
 * 
 * @author Sudheer Guduru
 * @version 1.0  
 */
public class AkamaiConstants extends TRUConstants {
	/**
	 * Constant for IN_PROGRESS
	 */
	public static final String IN_PROGRESS = "In-Progress";
	/**
	 * Constant for DONE
	 */
	public static final String DONE = "Done";
	/**
	 *  Constant for AUTHORIZATION
	 */
	public static final String AUTHORIZATION = "Authorization";
	/**
	 * Constant for BASIC
	 */
	public static final String BASIC = "Basic";
	/**
	 * Constant for CONTENT_TYPE
	 */
	public static final String CONTENT_TYPE = "Content-Type";
	/**
	 * Constant for APPLICATION_JSON
	 */
	public static final String APPLICATION_JSON = "application/json";
	
	/** Constant for HTTP STATUS 201. */
	public static final int HTTP_STATUS_201 = 201;
	
	/** Constant for INTEGER NUMBER THOUSAND. */
	public static final int INTEGER_NUMBER_THOUSAND = 1000;
	/**
	 * Constant to hold domain
	 */
	public static final String DOMAIN = "domain";
	/**
	 * Constant to hold progress URI
	 */
	public static final String PROGESS_URI = "progressURI";
	/**
	 * Constant to hold PURGE ID
	 */
	public static final String PURGE_ID = "purgeId";
	/**
	 * Constant to hold CODE_LIST
	 */
	public static final String CODE_LIST = "cpCodesList";
	/**
	 * Constant to hold CODE_COUNT
	 */
	public static final String CODE_COUNT = "cpCodeCount";
	/**
	 * Constant to hold AKAMAI_CONTENT_REMOVAL_REQUEST
	 */
	public static final String AKAMAI_CONTENT_REMOVAL_REQUEST ="Your Akamai content removal request (";
	/**
	 * constants for BACK_PARENTHESES .
	 */
	public static final String BACK_PARENTHESES  = ")";
	
	/**
	 * Constant to hold start time for requestSubmissionTime
	 */
	public static final String REQUEST_SUBMISSION_TIME = "requestSubmissionTime";
	
	/**
	 * Constant to hold completion time for requestCompletionTime
	 */
	public static final String REQUEST_COMPLETION_TIME = "requestCompletionTime";
	
}
