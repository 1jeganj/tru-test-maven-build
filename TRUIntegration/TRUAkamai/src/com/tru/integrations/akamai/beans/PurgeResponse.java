package com.tru.integrations.akamai.beans;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * This is a bean class to hold purge response.
 * @author PA
 * @version 1.0 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "httpStatus", "detail", "estimatedSeconds", "purgeId",
		"progressUri", "pingAfterSeconds", "supportId" })
public class PurgeResponse {

	@JsonProperty("httpStatus")
	private Integer mHttpStatus;
	@JsonProperty("detail")
	private String mDetail;
	@JsonProperty("estimatedSeconds")
	private Integer mEstimatedSeconds;
	@JsonProperty("purgeId")
	private String mPurgeId;
	@JsonProperty("progressUri")
	private String mProgressUri;
	@JsonProperty("pingAfterSeconds")
	private Integer mPingAfterSeconds;
	@JsonProperty("supportId")
	private String mSupportId;
	@JsonIgnore
	private Map<String, Object> mAdditionalProperties = new HashMap<String, Object>();

	/**
	 * @return the httpStatus
	 */
	@JsonProperty("httpStatus")
	public Integer getHttpStatus() {
		return mHttpStatus;
	}

	/**
	 * @param pHttpStatus
	 *            the httpStatus to set
	 */
	@JsonProperty("httpStatus")
	public void setHttpStatus(Integer pHttpStatus) {
		mHttpStatus = pHttpStatus;
	}

	/**
	 * @return the detail
	 */
	@JsonProperty("detail")
	public String getDetail() {
		return mDetail;
	}

	/**
	 * @param pDetail
	 *            the detail to set
	 */
	@JsonProperty("detail")
	public void setDetail(String pDetail) {
		mDetail = pDetail;
	}

	/**
	 * @return the estimatedSeconds
	 */
	@JsonProperty("estimatedSeconds")
	public Integer getEstimatedSeconds() {
		return mEstimatedSeconds;
	}

	/**
	 * @param pEstimatedSeconds
	 *            the estimatedSeconds to set
	 */
	@JsonProperty("estimatedSeconds")
	public void setEstimatedSeconds(Integer pEstimatedSeconds) {
		mEstimatedSeconds = pEstimatedSeconds;
	}

	/**
	 * @return the purgeId
	 */
	@JsonProperty("purgeId")
	public String getPurgeId() {
		return mPurgeId;
	}

	/**
	 * @param pPurgeId
	 *            the purgeId to set
	 */
	@JsonProperty("purgeId")
	public void setPurgeId(String pPurgeId) {
		mPurgeId = pPurgeId;
	}

	/**
	 * @return the progressUri
	 */
	@JsonProperty("progressUri")
	public String getProgressUri() {
		return mProgressUri;
	}

	/**
	 * @param pProgressUri
	 *            the progressUri to set
	 */
	@JsonProperty("progressUri")
	public void setProgressUri(String pProgressUri) {
		mProgressUri = pProgressUri;
	}

	/**
	 * @return the pingAfterSeconds
	 */
	@JsonProperty("pingAfterSeconds")
	public Integer getPingAfterSeconds() {
		return mPingAfterSeconds;
	}

	/**
	 * @param pPingAfterSeconds
	 *            the pingAfterSeconds to set
	 */
	@JsonProperty("pingAfterSeconds")
	public void setPingAfterSeconds(Integer pPingAfterSeconds) {
		mPingAfterSeconds = pPingAfterSeconds;
	}

	/**
	 * @return the supportId
	 */
	@JsonProperty("supportId")
	public String getSupportId() {
		return mSupportId;
	}

	/**
	 * @param pSupportId
	 *            the supportId to set
	 */
	@JsonProperty("supportId")
	public void setSupportId(String pSupportId) {
		mSupportId = pSupportId;
	}

	/**
	 * @return the additionalProperties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return mAdditionalProperties;
	}

	/**
	 * @param pName name to set
	 * @param pValue value to set
	 */
	@JsonAnySetter
	public void setAdditionalProperties(String pName, Object pValue) {
		mAdditionalProperties.put(pName, pValue);
	}

}