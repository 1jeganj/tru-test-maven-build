package com.tru.integrations.akamai.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/**
 * This is a bean class to hold purge request object state.
 * @author PA
 * @version 1.0 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "action", "domain", "objects" })
public class PurgeRequest {

	@JsonProperty("type")
	private String mType;
	@JsonProperty("action")
	private String mAction;
	@JsonProperty("domain")
	private String mDomain;
	@JsonProperty("objects")
	private List<String> mObjects = new ArrayList<String>();
	@JsonIgnore
	private Map<String, Object> mAdditionalProperties = new HashMap<String, Object>();

	/**
	 * @return the type
	 */
	@JsonProperty("type")
	public String getType() {
		return mType;
	}

	/**
	 * @param pType
	 *            the type to set
	 */
	@JsonProperty("type")
	public void setType(String pType) {
		mType = pType;
	}

	/**
	 * @return the action
	 */
	@JsonProperty("action")
	public String getAction() {
		return mAction;
	}

	/**
	 * @param pAction
	 *            the action to set
	 */
	@JsonProperty("action")
	public void setAction(String pAction) {
		mAction = pAction;
	}

	/**
	 * @return the domain
	 */
	@JsonProperty("domain")
	public String getDomain() {
		return mDomain;
	}

	/**
	 * @param pDomain
	 *            the domain to set
	 */
	@JsonProperty("domain")
	public void setDomain(String pDomain) {
		mDomain = pDomain;
	}

	/**
	 * @return the objects
	 */
	@JsonProperty("objects")
	public List<String> getObjects() {
		return mObjects;
	}

	/**
	 * @param pObjects
	 *            the objects to set
	 */
	@JsonProperty("objects")
	public void setObjects(List<String> pObjects) {
		mObjects = pObjects;
	}

	/**
	 * @return the additionalProperties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return mAdditionalProperties;
	}

	/**
	 * @param pName
	 *          -pName
	 *@param pValue
	 *          -pValue
	 */
	@JsonAnySetter
	public void setAdditionalProperties(String pName, Object pValue) {
		mAdditionalProperties.put(pName, pValue);
	}

}