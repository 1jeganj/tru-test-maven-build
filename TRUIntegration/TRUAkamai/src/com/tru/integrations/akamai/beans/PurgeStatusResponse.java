package com.tru.integrations.akamai.beans;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * This ia a bean class to hold purge status response.
 * @author PA
 * @version 1.0 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "originalEstimatedSeconds", "progressUri",
		"originalQueueLength", "purgeId", "supportId", "httpStatus",
		"completionTime", "submittedBy", "purgeStatus", "submissionTime",
		"pingAfterSeconds" })
public class PurgeStatusResponse {

	@JsonProperty("originalEstimatedSeconds")
	private Integer mOriginalEstimatedSeconds;
	@JsonProperty("progressUri")
	private String mProgressUri;
	@JsonProperty("originalQueueLength")
	private Integer mOriginalQueueLength;
	@JsonProperty("purgeId")
	private String mPurgeId;
	@JsonProperty("supportId")
	private String mSupportId;
	@JsonProperty("httpStatus")
	private Integer mHttpStatus;
	@JsonProperty("completionTime")
	private Object mCompletionTime;
	@JsonProperty("submittedBy")
	private String mSubmittedBy;
	@JsonProperty("purgeStatus")
	private String mPurgeStatus;
	@JsonProperty("submissionTime")
	private String mSubmissionTime;
	@JsonProperty("pingAfterSeconds")
	private Integer mPingAfterSeconds;
	@JsonIgnore
	private Map<String, Object> mAdditionalProperties = new HashMap<String, Object>();

	/**
	 * @return the originalEstimatedSeconds
	 */
	@JsonProperty("originalEstimatedSeconds")
	public Integer getOriginalEstimatedSeconds() {
		return mOriginalEstimatedSeconds;
	}

	/**
	 * @param pOriginalEstimatedSeconds
	 *            the originalEstimatedSeconds to set
	 */
	@JsonProperty("originalEstimatedSeconds")
	public void setOriginalEstimatedSeconds(Integer pOriginalEstimatedSeconds) {
		mOriginalEstimatedSeconds = pOriginalEstimatedSeconds;
	}

	/**
	 * @return the progressUri
	 */
	@JsonProperty("progressUri")
	public String getProgressUri() {
		return mProgressUri;
	}

	/**
	 * @param pProgressUri
	 *            the progressUri to set
	 */
	@JsonProperty("progressUri")
	public void setProgressUri(String pProgressUri) {
		mProgressUri = pProgressUri;
	}

	/**
	 * @return the originalQueueLength
	 */
	@JsonProperty("originalQueueLength")
	public Integer getOriginalQueueLength() {
		return mOriginalQueueLength;
	}

	/**
	 * @param pOriginalQueueLength
	 *            the originalQueueLength to set
	 */
	@JsonProperty("originalQueueLength")
	public void setOriginalQueueLength(Integer pOriginalQueueLength) {
		mOriginalQueueLength = pOriginalQueueLength;
	}

	/**
	 * @return the purgeId
	 */
	@JsonProperty("purgeId")
	public String getPurgeId() {
		return mPurgeId;
	}

	/**
	 * @param pPurgeId
	 *            the purgeId to set
	 */
	@JsonProperty("purgeId")
	public void setPurgeId(String pPurgeId) {
		mPurgeId = pPurgeId;
	}

	/**
	 * @return the supportId
	 */
	@JsonProperty("supportId")
	public String getSupportId() {
		return mSupportId;
	}

	/**
	 * @param pSupportId
	 *            the supportId to set
	 */
	@JsonProperty("supportId")
	public void setSupportId(String pSupportId) {
		mSupportId = pSupportId;
	}

	/**
	 * @return the httpStatus
	 */
	@JsonProperty("httpStatus")
	public Integer getHttpStatus() {
		return mHttpStatus;
	}

	/**
	 * @param pHttpStatus
	 *            the httpStatus to set
	 */
	@JsonProperty("httpStatus")
	public void setHttpStatus(Integer pHttpStatus) {
		mHttpStatus = pHttpStatus;
	}

	/**
	 * @return the completionTime
	 */
	@JsonProperty("completionTime")
	public Object getCompletionTime() {
		return mCompletionTime;
	}

	/**
	 * @param pCompletionTime
	 *            the completionTime to set
	 */
	@JsonProperty("completionTime")
	public void setCompletionTime(Object pCompletionTime) {
		mCompletionTime = pCompletionTime;
	}

	/**
	 * @return the submittedBy
	 */
	@JsonProperty("submittedBy")
	public String getSubmittedBy() {
		return mSubmittedBy;
	}

	/**
	 * @param pSubmittedBy
	 *            the submittedBy to set
	 */
	@JsonProperty("submittedBy")
	public void setSubmittedBy(String pSubmittedBy) {
		mSubmittedBy = pSubmittedBy;
	}

	/**
	 * @return the purgeStatus
	 */
	@JsonProperty("purgeStatus")
	public String getPurgeStatus() {
		return mPurgeStatus;
	}

	/**
	 * @param pPurgeStatus
	 *            the purgeStatus to set
	 */
	@JsonProperty("purgeStatus")
	public void setPurgeStatus(String pPurgeStatus) {
		mPurgeStatus = pPurgeStatus;
	}

	/**
	 * @return the submissionTime
	 */
	@JsonProperty("submissionTime")
	public String getSubmissionTime() {
		return mSubmissionTime;
	}

	/**
	 * @param pSubmissionTime
	 *            the submissionTime to set
	 */
	@JsonProperty("submissionTime")
	public void setSubmissionTime(String pSubmissionTime) {
		mSubmissionTime = pSubmissionTime;
	}

	/**
	 * @return the pingAfterSeconds
	 */
	@JsonProperty("pingAfterSeconds")
	public Integer getPingAfterSeconds() {
		return mPingAfterSeconds;
	}

	/**
	 * @param pPingAfterSeconds
	 *            the pingAfterSeconds to set
	 */
	@JsonProperty("pingAfterSeconds")
	public void setPingAfterSeconds(Integer pPingAfterSeconds) {
		mPingAfterSeconds = pPingAfterSeconds;
	}

	/**
	 * @return the additionalProperties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return mAdditionalProperties;
	}

	/**
	 * @param pName name to set
	 * @param pValue value to set
	 */
	@JsonAnySetter
	public void setAdditionalProperties(String pName, Object pValue) {
		mAdditionalProperties.put(pName, pValue);
	}

}
