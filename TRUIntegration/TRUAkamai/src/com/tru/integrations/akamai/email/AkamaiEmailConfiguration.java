package com.tru.integrations.akamai.email;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class AkamaiEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class AkamaiEmailConfiguration extends GenericService {

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mAkamaiSuccessEmailInfo;

	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mAkamaiFailureEmailInfo;

	/** Property to hold Recipients. */
	private List<String> mRecipients;

	/**
	 * @return the akamaiSuccessEmailInfo
	 */
	public TemplateEmailInfoImpl getAkamaiSuccessEmailInfo() {
		return mAkamaiSuccessEmailInfo;
	}

	/**
	 * @param pAkamaiSuccessEmailInfo the akamaiSuccessEmailInfo to set
	 */
	public void setAkamaiSuccessEmailInfo(
			TemplateEmailInfoImpl pAkamaiSuccessEmailInfo) {
		mAkamaiSuccessEmailInfo = pAkamaiSuccessEmailInfo;
	}

	/**
	 * @return the akamaiFailureEmailInfo
	 */
	public TemplateEmailInfoImpl getAkamaiFailureEmailInfo() {
		return mAkamaiFailureEmailInfo;
	}

	/**
	 * @param pAkamaiFailureEmailInfo the akamaiFailureEmailInfo to set
	 */
	public void setAkamaiFailureEmailInfo(
			TemplateEmailInfoImpl pAkamaiFailureEmailInfo) {
		mAkamaiFailureEmailInfo = pAkamaiFailureEmailInfo;
	}
	
	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	
}
