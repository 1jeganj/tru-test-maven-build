package com.tru.integrations.akamai.email;

import java.util.Map;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.integrations.akamai.constants.AkamaiConstants;
import com.tru.integrations.exception.TRUIntegrationException;

/**
 * This class is used to send an Email on Akamai invalidation Status to Users listed.
 * @version 1.0
 * @author Professional Access
 */
public class AkamaiEmailSender extends GenericService {
	
	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;
	
	/** Property to hold mAkamaiEmailConfiguration. */
	private AkamaiEmailConfiguration mAkamaiEmailConfiguration;
	
	/**
	 * This method used to send success email on akamai cache invalidation
	 * @param pMapObject akamai configuration map
	 * @throws TRUIntegrationException the TRUIntegrationException
	 */
	public void sendAkamaiCacheInvalidationSuccessEmail(Map<String, Object> pMapObject) throws TRUIntegrationException {
		if(isLoggingDebug()){
			logDebug("AkamaiEmailSender :: sendAkamaiCacheInvalidationSuccessEmail() :: START");
		}
		
		TemplateEmailInfoImpl emailInfo = mAkamaiEmailConfiguration.getAkamaiSuccessEmailInfo();
		
		clearEmailInfo(emailInfo);
		
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(pMapObject);
		} else {
			emailInfo.setTemplateParameters(pMapObject);
		}
		try {
			emailInfo.setMessageSubject(AkamaiConstants.AKAMAI_CONTENT_REMOVAL_REQUEST+pMapObject.get(AkamaiConstants.PURGE_ID)+AkamaiConstants.BACK_PARENTHESES);
			emailInfo.setFillFromTemplate(true);
				 getTemplateEmailSender().sendEmailMessage(emailInfo, mAkamaiEmailConfiguration.getRecipients());
			  
		} catch (TemplateEmailException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e);
		} 
		if(isLoggingDebug()){
			logDebug("AkamaiEmailSender :: sendAkamaiCacheInvalidationSuccessEmail() :: END");
		}
		 
	}

	/**
	 * This method used to clear email information
	 * @param pEmailInfo email template information
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: AkamaiEmailSender : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: AkamaiEmailSender : @Method: clearEmailInfo()");
	}
	
	/**
	 * This method used to send failure email 
	 * @throws TRUIntegrationException integration exception
	 */
	public void sendAkamaiCacheInvalidationFailureEmail() throws TRUIntegrationException {
		if(isLoggingDebug()){
			logDebug("AkamaiEmailSender :: sendAkamaiCacheInvalidationFailureEmail() :: START");
		}
		TemplateEmailInfoImpl emailInfo = mAkamaiEmailConfiguration.getAkamaiFailureEmailInfo();
		try {
			getTemplateEmailSender().sendEmailMessage(emailInfo, mAkamaiEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new TRUIntegrationException(e);
		}
		if(isLoggingDebug()){
			logDebug("AkamaiEmailSender :: sendAkamaiCacheInvalidationFailureEmail() :: END");
		}
	}

	/**
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * @param pTemplateEmailSender the templateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
	
	
	/**
	 * @return the akamaiEmailConfiguration
	 */
	public AkamaiEmailConfiguration getAkamaiEmailConfiguration() {
		return mAkamaiEmailConfiguration;
	}

	/**
	 * @param pAkamaiEmailConfiguration the akamaiEmailConfiguration to set
	 */
	public void setAkamaiEmailConfiguration(
			AkamaiEmailConfiguration pAkamaiEmailConfiguration) {
		mAkamaiEmailConfiguration = pAkamaiEmailConfiguration;
	}

	
}
