package com.tru.rest.service.oms;

import atg.nucleus.GenericService;

import com.tru.integration.oms.TRUIntegrationManager;

/**
 * This class is to generate the JSON using REST service.
 * @author PA.
 * @version 1.0.
 */
public class TRUGetOrderForOM extends GenericService {
	
	/**
	 * Holds reference for TRUIntegrationManager
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	/**
	 * @param pOrderId - order id
	 */
	public void getOrderForOMS(String pOrderId){
		if(isLoggingDebug()){
			logDebug("Entered into Class : TRUGetOrderForOM method : getOrderForOMS");
		}
		if(pOrderId == null){
			return;
		}
		String customerOrderJSON = getIntegrationManager().generateCORequest(pOrderId);
		if(isLoggingDebug()){
			vlogDebug("customerOrderJSON : {0}", customerOrderJSON);
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUGetOrderForOM method : getOrderForOMS");
		}
	}

	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

}
