package com.tru.commerce.fulfillment;

import atg.commerce.fulfillment.SubmitOrder;

/**
 * Submit order message extended to add new properties submit order message.
 * @author Professional Access.
 * @version 1.0.
 */
public class TRUSubmitOrder extends SubmitOrder {

	/**
	 * Serial Version Id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Customer Order import request string.
	 */
	private String mCustomerOrderImportRequest;

	/**
	 * This method returns the customerOrderImportRequest value.
	 *
	 * @return the customerOrderImportRequest
	 */
	public String getCustomerOrderImportRequest() {
		return mCustomerOrderImportRequest;
	}

	/**
	 * This method sets the customerOrderImportRequest with parameter value pCustomerOrderImportRequest.
	 *
	 * @param pCustomerOrderImportRequest the customerOrderImportRequest to set
	 */
	public void setCustomerOrderImportRequest(String pCustomerOrderImportRequest) {
		mCustomerOrderImportRequest = pCustomerOrderImportRequest;
	}
}
