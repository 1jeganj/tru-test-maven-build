package com.tru.commerce.order.processor;

import java.io.Serializable;
import java.util.HashMap;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.adapter.gsa.GSARepository;
import atg.commerce.CommerceException;
import atg.commerce.fulfillment.SubmitOrder;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.service.pipeline.PipelineResult;

import com.tru.commerce.fulfillment.TRUSubmitOrder;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSConfiguration;
import com.tru.integration.oms.TRUOMSErrorTools;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.messaging.oms.TRUCustomerOrderUpdateMessage;

/**
 * This class overridden to generate the customer order import request in sync
 * with order placement.
 * @author Professional Access
 * @version 1.0
 */
public class TRUProcSendFulfillmentMessage extends TRUSendFulfillmentMessage {
	
	/**
	 * This holds reference for Integration manager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	
	/** Holds reference for mOmsErrorRepository. */
	private GSARepository mOmsErrorRepository;
	
	
	/** Holds reference for mOmsErrorTools. */
	private TRUOMSErrorTools mOmsErrorTools;
	/** Holds reference for mEnabled. */
	private boolean mEnabled;
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public int runProcess(Object pParam, PipelineResult pResult) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProcSendFulfillmentMessage  method: runProcess]");
		}
		if(!isEnabled()){
			return TRUOMSConstant.INT_ONE;
		}
		try {
			Serializable eventToSend = createEventToSend(pParam, pResult);

			if (eventToSend == null) {
				return TRUOMSConstant.INT_TWO;
			}
			sendObjectMessage(eventToSend, getEventType(eventToSend),
					getPortName());
		} catch (Exception exception) {
			//This has to be uncommented once Oracle SR for queue connection has been resolved.
			HashMap map = (HashMap) pParam;
			Order order = (Order) map.get(TRUOMSConstant.ORDER);
			if (order.getId() != null) {
				getOmsErrorTools().updateOmsErrorItemForOrder(order.getId(), TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE);
			} 
			if (isLoggingError()) {
				vlogError(exception,
						"Exception in method TRUProcSendFulfillmentMessage.runProcess()");
			}
			return TRUOMSConstant.INT_ONE;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from[Class: TRUProcSendFulfillmentMessage  method: runProcess]");
		}
		return TRUOMSConstant.INT_ONE;
	}
	
	/**
	 * This method will send XML message from source
	 * @param pObjectMessage - ObjectMessage
	 * @param pType - JMS type
	 * @param pPortName - PortName
	 * @throws JMSException- JMS Exception
	 * 
	 */
	@Override
	public void sendObjectMessage(Serializable pObjectMessage, String pType,
			String pPortName) throws JMSException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProcSendFulfillmentMessage  method: sendObjectMessage]");
		}
		String customerOrderXML = null;
		TRULayawayOrderImpl layawayOrderImpl = null;
		Order order = null;
		SubmitOrder submitOrder = null;
		TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
		String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
		if(pObjectMessage instanceof SubmitOrder){
			submitOrder = (SubmitOrder) pObjectMessage;
			if(submitOrder != null){
				order = submitOrder.getOrder();
			}
		}
		if (order != null && isAllowMessageSending() && configuration.isSendXMLMessageFromSource())
		{
			if(order instanceof TRULayawayOrderImpl){
				layawayOrderImpl = (TRULayawayOrderImpl) order;
				customerOrderXML = getIntegrationManager().generateXMLLayawayOrderRequest(layawayOrderImpl);
				if(isLoggingDebug()){
					vlogDebug("customerOrderXML : {0} ", customerOrderXML); 
				}
			}else if(order instanceof TRUOrderImpl){
				customerOrderXML = getIntegrationManager().generateCOXMLRequest(order.getId());
			}
			ObjectMessage om = getMessageSourceContext().createObjectMessage(pPortName);
			om.setJMSCorrelationID(customerOrderImportTransPrefix+order.getId());
			om.setObject(customerOrderXML);
			om.setJMSType(pType);
			//Send message
			getMessageSourceContext().sendMessage(pPortName, om);
			if(isLoggingDebug()){
				vlogDebug("Customer order import xml sent to  queue : {0}",  customerOrderXML);
			}
			//Process the generated CO request
			getIntegrationManager().processCOImportXMLRequest(customerOrderXML, order.getId());
			if(isLoggingDebug()){
				vlogDebug("Customer order import xml   :   {0} has been audited.",  customerOrderXML);
			}
		} else{
			super.sendObjectMessage(pObjectMessage, pType, pPortName);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUProcSendFulfillmentMessage  method: sendObjectMessage]");
		}
	}
	
	/**
	 * This method will create event to send the order import message.
	 * 
	 * @param pParam
	 *            - Map contains Objects
	 * @param pResult
	 * 			  - Pipeline Result
	 * @throws Exception
	 * 			-Generic Exception          
	 * @return Serializable : order message
	 */
	@Override
	public Serializable createEventToSend(Object pParam, PipelineResult pResult) throws Exception{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProcSendFulfillmentMessage  method: createEventToSend]");
			vlogDebug("Port name: {0} Message : {1} ", pParam, pResult);
		}
		TRUSubmitOrder soMessage = new TRUSubmitOrder();
		SubmitOrder submitOrder = null;
		Order order = null;
		Serializable soMessage1;
		String orderId = null;
			soMessage1 = (SubmitOrder) super.createEventToSend(pParam, pResult);
		if(soMessage1 instanceof SubmitOrder){
			submitOrder = (SubmitOrder) soMessage1;
		}
		if(submitOrder != null){
		    soMessage.setOrder(submitOrder.getOrder());
		    soMessage.setSource(getMessageSourceName());
		    soMessage.setId(getNextMessageId());
		    soMessage.setOriginalSource(getMessageSourceName());
		    soMessage.setOriginalId(submitOrder.getId());
		    soMessage.setProfile(submitOrder.getProfile());
		    soMessage.setSiteId(getSiteId(pParam));
		}
		TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
		String customerOrderJSON = null;
		if(soMessage != null && soMessage.getOrder() != null){
			TRULayawayOrderImpl layawayOrderImpl = null;
			order = soMessage.getOrder();
			if(order instanceof TRULayawayOrderImpl && configuration.isLayawayOrderRequest()){
				layawayOrderImpl = (TRULayawayOrderImpl) order;
				orderId = layawayOrderImpl.getId();
				customerOrderJSON = getIntegrationManager().generateLayawayOrderRequest(layawayOrderImpl,order);
			}
			if(configuration.isGenerateCOImportRequestInSync() && order instanceof TRUOrderImpl){
				orderId = order.getId();
				customerOrderJSON = getIntegrationManager().generateCORequest(orderId);
			}
			if(!StringUtils.isBlank(customerOrderJSON)){
				soMessage.setCustomerOrderImportRequest(customerOrderJSON);
			}
			if (isLoggingDebug()) {
				vlogDebug("orderId : {0}", orderId);
				vlogDebug("customerOrderJSON : {0}", customerOrderJSON);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUProcSendFulfillmentMessage  method: createEventToSend]");
		}
		return soMessage;
	}
	
	/**
	 * This method is to send the order update message to sink
	 * @param pCustomerOrderUpdateMessage - update message
	 */
	public void sendOrderUpdateMessage(TRUCustomerOrderUpdateMessage pCustomerOrderUpdateMessage) {
		if (isLoggingDebug()) {
			logDebug("Enter into  [Class: TRUProcSendFulfillmentMessage  method: sendOrderUpdateMessage]");
		}
		if(pCustomerOrderUpdateMessage == null){
			return;
		}
		ObjectMessage objectMessage = null;
		TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
		String customerOrderUpdateTransPrefix = configuration.getCustomerOrderUpdateTransPrefix();
		String orderId = pCustomerOrderUpdateMessage.getExternalOrderNumber();
		try{
			//Create object message
			objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
			objectMessage.setJMSCorrelationID(customerOrderUpdateTransPrefix+orderId);
		//Set Object message
		if(configuration.isSendXMLMessageFromSource()){
			String customerOrderUpdateXML = getIntegrationManager().getOmsUtils().populateCustomerOrderXMLForUpdate(pCustomerOrderUpdateMessage);
			objectMessage.setObject(customerOrderUpdateXML);
			//Send Customer Master Import Message
			getMessageSourceContext().sendMessage(getPortName(), objectMessage);
			if(isLoggingDebug()){
				vlogDebug("Customer order update xml sent to queue : {0}",  customerOrderUpdateXML);
			}
			//Process the generated CO request
			getIntegrationManager().processCOUpdateXMLRequest(customerOrderUpdateXML, orderId);
			if(isLoggingDebug()){
				vlogDebug("Customer order update xml   :   {0} has been audited.",  customerOrderUpdateXML);
			}
			
		} else {
			objectMessage.setObject(pCustomerOrderUpdateMessage);
			//Send Customer Master Import Message
			getMessageSourceContext().sendMessage(getPortName(), objectMessage);
		}
		}catch (JMSException exc) {
			//This has to be uncommented once Oracle SR for queue connection has been resolved.
			if (orderId != null) {
				getOmsErrorTools().updateOmsErrorItemForOrder(orderId,TRUOMSConstant.ORDER_UPDATE_SERVICE);
			}
			if(isLoggingError()){
				vlogError(exc, "JMSException in method sendOrderUpdateMessage");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("objectMessage in sendOrderUpdateMessage() Method...." , objectMessage);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUProcSendFulfillmentMessage  method: sendOrderUpdateMessage]");
		}
	}
	
	/**
	 * This method is to send failed orders
	 * @param pOrderId - Order id
	 */
	public void sendFailedOrders(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProcSendFulfillmentMessage  method: sendFailedOrders]");
		}
		if(pOrderId == null){
			return;
		}
		String customerOrderXML = null;
		TRULayawayOrderImpl layawayOrderImpl = null;
		Order order = null;
		TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
		String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
		try {
			order = getOmsErrorTools().getIntegrationManager().getOrderManager().loadOrder(pOrderId);
			if (order != null && isAllowMessageSending() && configuration.isSendXMLMessageFromSource())
			{
				if(order instanceof TRULayawayOrderImpl){
					layawayOrderImpl = (TRULayawayOrderImpl) order;
					customerOrderXML = getIntegrationManager().generateXMLLayawayOrderRequest(layawayOrderImpl);
					if(isLoggingDebug()){
						vlogDebug("customerOrderXML : {0} ", customerOrderXML); 
					}
				}else if(order instanceof TRUOrderImpl){
					customerOrderXML = getIntegrationManager().generateCOXMLRequest(order.getId());
				}
				ObjectMessage om = getMessageSourceContext().createObjectMessage(getPortName());
				om.setJMSCorrelationID(customerOrderImportTransPrefix+order.getId());
				om.setObject(customerOrderXML);
				om.setJMSType(null);
				//Send message
				getMessageSourceContext().sendMessage(getPortName(), om);
				if(isLoggingDebug()){
					vlogDebug("Customer order import xml sent to  queue : {0}",  customerOrderXML);
				}
			}
		} catch (CommerceException exc) {
			if(isLoggingError()){
				vlogError("CommerceException : while loading order with order id : {0] and exception is : {1}",pOrderId, exc);
			}
		} catch (JMSException exc) {
			if(isLoggingError()){
				vlogError("JMSException : while delivering failed orders to queue. order id : {0] and exception is : {1}",pOrderId, exc);
			}
		}
		//Process the generated CO request
		getIntegrationManager().processCOImportXMLRequest(customerOrderXML, order.getId());
		if(isLoggingDebug()){
			vlogDebug("Customer order import xml   :   {0} has been audited.",  customerOrderXML);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUProcSendFulfillmentMessage  method: sendFailedOrders]");
		}
	}
	
	/**
	 * This method is to re deliver the create/update xmls to queue in case of any fail overs.
	 * @param pTransId - Transaction Id
	 * @param pOrderMessageXML - create/update xml for  redelivery
	 */
	public void redeliverOrderMessages(String pTransId, String pOrderMessageXML) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUProcSendFulfillmentMessage  method: redeliverOrderMessages]");
			vlogDebug("Trans Id : {0} order message xml : {1}", pTransId, pOrderMessageXML);
		}
		if(pTransId == null && pOrderMessageXML == null){
			return;
		}
		try{
			ObjectMessage om = getMessageSourceContext().createObjectMessage(getPortName());
			om.setJMSCorrelationID(pTransId);
			om.setObject(pOrderMessageXML);
			om.setJMSType(null);
			//Send message
			getMessageSourceContext().sendMessage(getPortName(), om);
		} catch (JMSException exc) {
			if(isLoggingError()){
				vlogError("JMSException : while re delivering failed xml messages  to queue. trans  id : {0] and exception is : {1}",pTransId, exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUProcSendFulfillmentMessage  method: redeliverOrderMessages]");
		}
	}
	
	/**
	 * Gets the oms error repository.
	 *
	 * @return the omsErrorRepository
	 */
	public GSARepository getOmsErrorRepository() {
		return mOmsErrorRepository;
	}

	/**
	 * Sets the oms error repository.
	 *
	 * @param pOmsErrorRepository the omsErrorRepository to set
	 */
	public void setOmsErrorRepository(GSARepository pOmsErrorRepository) {
		mOmsErrorRepository = pOmsErrorRepository;
	}
	
	/**
	 * Gets the oms error tools.
	 *
	 * @return the omsErrorTools
	 */
	public TRUOMSErrorTools getOmsErrorTools() {
		return mOmsErrorTools;
	}

	/**
	 * Sets the oms error tools.
	 *
	 * @param pOmsErrorTools the omsErrorTools to set
	 */
	public void setOmsErrorTools(TRUOMSErrorTools pOmsErrorTools) {
		mOmsErrorTools = pOmsErrorTools;
	}

	/**
	 * This method returns the integrationManager value
	 *
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * This method sets the integrationManager with parameter value pIntegrationManager
	 *
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * This method returns the enabled value
	 *
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * This method sets the enabled with parameter value pEnabled
	 *
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
}
