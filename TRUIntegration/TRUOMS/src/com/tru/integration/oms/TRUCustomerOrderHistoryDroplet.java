package com.tru.integration.oms;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 * This droplet is to Call the order list service.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCustomerOrderHistoryDroplet extends DynamoServlet {

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName CUSTOMER_ID = ParameterName
			.getParameterName("customerId");

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName PAGE_NUMBER = ParameterName
			.getParameterName("pageNumber");

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName NO_OF_RECORDS_PER_PAGE = ParameterName
			.getParameterName("noOfRecordsPerPage");
	
	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName LAYAWAY_PAGE = ParameterName.getParameterName("layawayPage");


	/**
	 * Holds constant RESPONSE.
	 */
	public static final String RESPONSE = "response";

	/**
	 * Holds OmsUtils.
	 */
	private TRUOMSUtils mOmsUtils;

	/**
	 * Holds OmsAuditTools.
	 */
	private TRUOMSAuditTools mOmsAuditTools;

	/**
	 * Holds OmsEnable.
	 */
	private boolean mOmsTestModeEnable;

	/**
	 * Holds DummyOrderResponse.
	 */
	private String mDummyOrderResponse;
	
	/** This holds the reference for tru configuration */
	private TRUConfiguration mConfiguration;

	/**
	 * This method returns the omsUtils value.
	 * 
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 * 
	 * @param pOmsUtils
	 *            the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This method returns the omsAuditTools value.
	 * 
	 * @return the omsAuditTools
	 */
	public TRUOMSAuditTools getOmsAuditTools() {
		return mOmsAuditTools;
	}

	/**
	 * This method sets the omsAuditTools with parameter value pOmsAuditTools.
	 * 
	 * @param pOmsAuditTools
	 *            the omsAuditTools to set
	 */
	public void setOmsAuditTools(TRUOMSAuditTools pOmsAuditTools) {
		mOmsAuditTools = pOmsAuditTools;
	}

	/**
	 * This method returns the OmsTestModeEnable value.
	 * 
	 * @return the OmsTestModeEnable
	 */
	public boolean isOmsTestModeEnable() {
		return mOmsTestModeEnable;
	}

	/**
	 * @param pOmsTestModeEnable
	 *            the mOmsTestModeEnable to set
	 */
	public void setOmsTestModeEnable(boolean pOmsTestModeEnable) {
		this.mOmsTestModeEnable = pOmsTestModeEnable;
	}

	/**
	 * @return the mDummyOrderResponse
	 */
	public String getDummyOrderResponse() {
		return mDummyOrderResponse;
	}

	/**
	 * @param pDummyOrderResponse
	 *            the mDummyOrderResponse to set
	 */
	public void setDummyOrderResponse(String pDummyOrderResponse) {
		this.mDummyOrderResponse = pDummyOrderResponse;
	}
	
	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration
	 *            the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * This service method is to call the customer order list service if
	 * customer views orders from my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderHistoryDroplet method : service");
		}
		String customerOrderListJSONResponse = null;
		String customerId = (String) pRequest.getLocalParameter(CUSTOMER_ID);
		String pageNumber = (String) pRequest.getLocalParameter(PAGE_NUMBER);
		String noOfRecordsPerPage = (String) pRequest.
				getLocalParameter(NO_OF_RECORDS_PER_PAGE);
		String layawayPage = (String) pRequest.getLocalParameter(LAYAWAY_PAGE);
		long recordsPerPage = Long.parseLong(noOfRecordsPerPage);
		boolean orderHistoryPage = Boolean.TRUE;
		if(isLoggingDebug()){
			vlogDebug("Customer Order details for customer Id : {0} pageNumber : {1} recordsPerPage : {2} layawayPage : {3}",
					customerId,pageNumber,recordsPerPage,layawayPage);
		}
		String customerOrderlistJSONRequest = getOmsUtils().populateCustomerOrderListRequest(pageNumber, customerId,
						recordsPerPage,layawayPage);
		if (isLoggingDebug()) {
			vlogDebug("Customer Order list JSON : {0}", customerOrderlistJSONRequest);
		}
		if (isOmsTestModeEnable()) {
			// dummy json response
			if(isLoggingDebug()){
				logDebug("Test mode is enable");
			}
			customerOrderListJSONResponse = getDummyOrderResponse();
			if(isLoggingDebug()){
				vlogDebug("Dummy Response for order history : {0}", customerOrderListJSONResponse);
			}
		} else {
			// real call to OMS to get json response
			customerOrderListJSONResponse = getOmsUtils().omsOrderListServiceCall(TRUOMSConstant.EMPTY_STRING,
					TRUOMSConstant.EMPTY_STRING, getConfiguration().getOmsOrderListServiceURL(), customerOrderlistJSONRequest);
			if(isLoggingDebug()){
				vlogDebug("Response form OMS  : {0}", customerOrderListJSONResponse);
			}
		}
		if (!StringUtils.isEmpty(customerOrderListJSONResponse)) {
			if (!getOmsUtils().checkErrorMessageTag(customerOrderListJSONResponse,orderHistoryPage)) {
				customerOrderListJSONResponse = getOmsUtils().updateJsonObject(customerOrderListJSONResponse,layawayPage);
			}
			if(isLoggingDebug()){
				vlogDebug("Formated JSON resonse for order status display and format of date  : {0}", customerOrderListJSONResponse);
			}
		}
		pRequest.setParameter(RESPONSE, customerOrderListJSONResponse);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUCustomerOrderHistoryDroplet method : service");
		}
	}

}
