package com.tru.integration.oms;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 *This droplet is to Call the order details service.
 * @version 1.0
 * @author Professional Access
 */
public class TRUCustomerOrderDetailsDroplet extends DynamoServlet {

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName ORDER_ID = ParameterName
			.getParameterName("orderId");


	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName LAYAWAY_PAGE = ParameterName
			.getParameterName("layawaypage");


	/**
	 * Holds constant RESPONSE.
	 */
	public static final String RESPONSE = "response";

	/**
	 * Holds msUtils.
	 */
	private TRUOMSUtils mOmsUtils;

	/**
	 * Holds OmsAuditTools.
	 */
	private TRUOMSAuditTools mOmsAuditTools;

	/**
	 * Holds OmsEnable.
	 */
	private boolean mOmsTestModeEnable;

	/**
	 * Holds DummyOrderResponse.
	 */
	private String mDummyOrderResponse;

	/** This holds the reference for tru configuration. */
	private TRUConfiguration mConfiguration;

	/**
	 * Holds LayawayDummyOrderResponse.
	 */
	private String mLayawayDummyOrderResponse;

	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This method returns the omsAuditTools value.
	 *
	 * @return the omsAuditTools
	 */
	public TRUOMSAuditTools getOmsAuditTools() {
		return mOmsAuditTools;
	}

	/**
	 * This method sets the omsAuditTools with parameter value pOmsAuditTools.
	 *
	 * @param pOmsAuditTools the omsAuditTools to set
	 */
	public void setOmsAuditTools(TRUOMSAuditTools pOmsAuditTools) {
		mOmsAuditTools = pOmsAuditTools;
	}

	/**
	 * This method returns the omsEnable value.
	 * 
	 * @return the omsEnable
	 */
	public boolean isOmsTestModeEnable() {
		return mOmsTestModeEnable;
	}

	/**
	 * @param pOmsTestModeEnable
	 *            the mOmsTestModeEnable to set
	 */
	public void setOmsTestModeEnable(boolean pOmsTestModeEnable) {
		this.mOmsTestModeEnable = pOmsTestModeEnable;
	}

	/**
	 * @return the mDummyOrderResponse
	 */
	public String getDummyOrderResponse() {
		return mDummyOrderResponse;
	}

	/**
	 * @param pDummyOrderResponse the mDummyOrderResponse to set
	 */
	public void setDummyOrderResponse(String pDummyOrderResponse) {
		this.mDummyOrderResponse = pDummyOrderResponse;
	}

	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration
	 *            the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}


	/**
	 * @return the mLayawayDummyOrderResponse
	 */
	public String getLayawayDummyOrderResponse() {
		return mLayawayDummyOrderResponse;
	}

	/**
	 * @param pLayawayDummyOrderResponse the mLayawayDummyOrderResponse to set
	 */
	public void setLayawayDummyOrderResponse(String pLayawayDummyOrderResponse) {
		this.mLayawayDummyOrderResponse = pLayawayDummyOrderResponse;
	}


	/**
	 * This service method is to call the customer order details service if
	 * customer views order from my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderDetailsDroplet method : service");
		}
		String orderId = (String) pRequest.getLocalParameter(ORDER_ID);
		String layawaypage = (String) pRequest.getLocalParameter(LAYAWAY_PAGE);
		if (layawaypage == null && StringUtils.isEmpty(layawaypage)) {
			layawaypage = TRUConstants.FALSE;
		}
		String customerOrderDetailJSONResponse = null;
		if(isLoggingDebug()){
			vlogDebug("Customer Order details for order Id : {0} layawayPage : {1}", orderId,layawaypage);
		}
		if (isOmsTestModeEnable()) {
			// dummy json response
			if(isLoggingDebug()){
				logDebug("Test mode is enable");
			}
			if(layawaypage.equalsIgnoreCase(TRUConstants.TRUE)) {
				customerOrderDetailJSONResponse = getLayawayDummyOrderResponse();
				if(isLoggingDebug()){
					logDebug("Dummy Responce for layaway" + customerOrderDetailJSONResponse);
				}
				if (!getOmsUtils().checkErrorMessageTag(customerOrderDetailJSONResponse,false)) {
					customerOrderDetailJSONResponse = getOmsUtils().getPaymentDetailsForLayawayOrder(customerOrderDetailJSONResponse);
				}
			} else {
				customerOrderDetailJSONResponse = getDummyOrderResponse();
				if(isLoggingDebug()){
					logDebug("Dummy Responce for normal order" + customerOrderDetailJSONResponse);
				}
				if (!getOmsUtils().checkErrorMessageTag(customerOrderDetailJSONResponse,false)) {
				customerOrderDetailJSONResponse = getOmsUtils().rearrangeOrderDetailsJSON(customerOrderDetailJSONResponse);
				}
			}
			if(isLoggingDebug()){
				logDebug("Formated JSON responce " + customerOrderDetailJSONResponse);
			}
		} else {
			// real call to OMS to get json response
			customerOrderDetailJSONResponse = getOmsUtils().omsOrderDetailsServiceCall(TRUOMSConstant.EMPTY_STRING, 
					TRUOMSConstant.EMPTY_STRING, getConfiguration().getOmsOrderDetailServiceURL(), orderId);
			if (!StringUtils.isEmpty(customerOrderDetailJSONResponse)) {
				if(isLoggingDebug()){
					logDebug("Responce from OMS " + customerOrderDetailJSONResponse);
				}
				if (layawaypage.equalsIgnoreCase(TRUConstants.TRUE)) {
					if (!getOmsUtils().checkErrorMessageTag(customerOrderDetailJSONResponse,false)) {
						customerOrderDetailJSONResponse = getOmsUtils().getPaymentDetailsForLayawayOrder(customerOrderDetailJSONResponse);
					}
				} else {
					if (!getOmsUtils().checkErrorMessageTag(customerOrderDetailJSONResponse,false)) {
						customerOrderDetailJSONResponse = getOmsUtils().rearrangeOrderDetailsJSON(customerOrderDetailJSONResponse);
					}
				}
				if(isLoggingDebug()){
					logDebug("Formated JSON responce for payment dates " + customerOrderDetailJSONResponse);
				}
			}
		}
		//Call order details Service
		pRequest.setParameter(RESPONSE, customerOrderDetailJSONResponse);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderDetailsDroplet method : service");
		}
	}

	
}
