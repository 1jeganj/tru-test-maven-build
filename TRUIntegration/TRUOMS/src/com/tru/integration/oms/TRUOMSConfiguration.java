package com.tru.integration.oms;

import java.util.List;
import java.util.Map;

/**
 * This class holds property names of audit repository.
 * @author PA.
 * @version 1.0.
 */
public class TRUOMSConfiguration extends TRUOMSBaseConfiguration {

	/**
	 * Holds transactionIdPopertyName.
	 */
	private String mTransactionIdPopertyName;

	/**
	 * Holds requestPopertyName.
	 */
	private String mRequestPopertyName;

	/**
	 * Holds responsePopertyName.
	 */
	private String mResponsePopertyName;

	/**
	 * Holds typePopertyName.
	 */
	private String mTypePopertyName;

	/**
	 * Holds statusPopertyName.
	 */
	private String mStatusPopertyName;

	/**
	 * Holds transactionTimePopertyName.
	 */
	private String mTransactionTimePopertyName;

	/**
	 * Holds omsTransRecordsItemDescriptorName.
	 */
	private String mOmsTransRecordsItemDescriptorName;

	/**
	 * Holds mTcCompanyIdValue.
	 */
	private String mTcCompanyIdValue;

	/**
	 * Holds mOrderTypeValueForStore.
	 */
	private String mOrderTypeValueForStore;

	/**
	 * Holds mOrderTypeValueForCSC.
	 */
	private String mOrderTypeValueForCSC;

	/**
	 * Holds mOrderTypeValueForSOS.
	 */
	private String mOrderTypeValueForSOS;

	/**
	 * Holds OrderedQtyUOM.
	 */
	private String mOrderedQtyUOM;

	/**
	 * Holds mDeliveryOptionCustomerPickup.
	 */
	private String mDeliveryOptionCustomerPickup;

	/**
	 * Holds mPaymentGroupTypePropertyName.
	 */
	private String mPaymentGroupTypePropertyName;

	/**
	 * Holds mPaymentEntryType.
	 */
	private String mPaymentEntryType;
	/**
	 * Holds mCoupon.
	 */
	private String mCoupon;
	/**
	 * Holds mVendorFunded.
	 */
	private String mVendorFunded;
	/**
	 * Holds mRetailFunded.
	 */
	private String mRetailFunded;
	/**
	 * Holds mDiscountDetailItem.
	 */
	private String mDiscountDetailItem;
	/**
	 * Holds mGiftMessageNoteType.
	 */
	private String mGiftMessageNoteType;
	/**
	 * Holds mGiftMessageNoteType.
	 */
	private String mGiftMessageGWNoteCode;
	/**
	 * Holds mGiftMessageNoteType.
	 */
	private String mGiftMessageVSNoteType;
	/**
	 * Holds mGiftMessageGiftWrapNoteText.
	 */
	private String mGiftMessageGiftWrapNoteText;
	/**
	 * Holds mChargeCategoryShipping.
	 */
	private String mChargeCategoryShipping;
	/**
	 * Holds mChargeNameShipping.
	 */
	private String mChargeNameShipping;
	/**
	 * Holds mChargeCategoryVAS.
	 */
	private String mChargeCategoryVAS;
	/**
	 * Holds mChargeNameBPP.
	 */
	private String mChargeNameBPP;
	/**
	 * Holds mChanrgeNameShipSurcharge.
	 */
	private String mChargeNameShipSurcharge;
	/**
	 * Holds mChargeCategoryShipSurcharge.
	 */
	private String mChargeCategoryShipSurcharge;
	/**
	 * Holds mChangeNameE911.
	 */
	private String mChargeNameE911;
	/**
	 * Holds mChargeCategoryE911.
	 */
	private String mChargeCategoryE911;
	/**
	 * Holds mSlapper.
	 */
	private String mSlapper;
	/**
	 * Holds mNonSlapper.
	 */
	private String mNonSlapper;
	/**
	 * Holds mTruSiteId.
	 */
	private String mTruSiteId;
	/**
	 * Holds mBruSiteId.
	 */
	private String mBruSiteId;
	/**
	 * Holds mTruSiteParameter.
	 */
	private String mTruSiteParameter;
	/**
	 * Holds mBruSiteParameter.
	 */
	private String mBruSiteParameter;
	/**
	 * Holds mWishListRefField5.
	 */
	private String mWishListRefField5;
	/**
	 * Holds mRegistryRefField5.
	 */
	private String mRegistryRefField5;
	/**
	 * Holds mOrderEntryTypeWeb.
	 */
	private String mOrderEntryTypeWeb;
	/**
	 * Holds mOrderPaymentStatus.
	 */
	private String mOrderPaymentStatus;
	/**
	 * Holds mOrderRefField4ForTRU.
	 */
	private String mOrderRefField4ForTRU;
	/**
	 * Holds mOrderRefField4ForBRU.
	 */
	private String mOrderRefField4ForBRU;
	/**
	 * Holds mOrderTypeMap.
	 */
	private Map<String, String> mOrderTypeMap;

	/**
	 * mCancelReasonCode.
	 */
	private String mCancelReasonCode;
	/**
	 * mSaveOmsTransactionDetails.
	 */
	private boolean mSaveOmsTransactionDetails;

	/**
	 * mCustomerOrderUpdateTransPrefix.
	 */
	private String mCustomerOrderUpdateTransPrefix;

	/**
	 * mCustomerOrderCancelTransPrefix.
	 */
	private String mCustomerOrderCancelTransPrefix;

	/**
	 * mCustomerOrderImportTransPrefix.
	 */
	private String mCustomerOrderImportTransPrefix;

	/**
	 * mCustomerMasterImportTransPrefix.
	 */
	private String mCustomerMasterImportTransPrefix;
	/**
	 * mOrderListEntityType.
	 */
	private String mOrderListEntityType;
	
	/** mOrderListSortDirection. */
	private String mOrderListSortDirection;
	
	/** mOrderListSortField. */
	private String mOrderListSortField;
	/**
	 * mOrderListNoOfRecPerPage.
	 */
	private long mOrderListNoOfRecPerPage;
	
	/** mDiscountDetailOrder. */
	private String mDiscountDetailOrder;
	/**
	 * mDiscountDetailShipping.
	 */
	private String mDiscountDetailShipping;
	/**
	 * mAuthorizationTransactionType.
	 */
	private String mAuthorizationTransactionType;
	/**
	 * mSettlementTransactionType.
	 */
	private String mSettlementTransactionType;
	/**
	 * mPaymentMethodMap.
	 */
	private Map<String, String> mPaymentMethodMap;
	/**
	 * mChargeNameGiftWrap.
	 */
	private String mChargeNameGiftWrap;
	/**
	 * mCreditCardMethod.
	 */
	private String mCreditCardMethod;
	/**
	 * mPayPalMethod.
	 */
	private String mPayPalMethod;
	/**
	 * mProxyFirstName.
	 */
	private String mProxyFirstName;
	/**
	 * mProxyLastName.
	 */
	private String mProxyLastName;
	/**
	 * mProxyPhone.
	 */
	private String mProxyPhone;
	/**
	 * mProxyEmail.
	 */
	private String mProxyEmail;
	/**
	 * Holds mOrderStatusOHMap.
	 */
	private Map<String, String> mOrderStatusOHMap;

	/**
	 * Holds mOrderStatusOLMap.
	 */
	private Map<String, String> mOrderStatusOLMap;
	/**
	 * Holds mLayawayOrderStatusOHMap.
	 */
	private Map<String, String> mLayawayOrderStatusOHMap;
	/**
	 * Holds mRegistryInfoNoteType.
	 */
	private String mRegistryInfoNoteType;

	/**
	 * Holds mOrderDetailsCustomerOrderJsonElement.
	 */
	private String mOrderDetailsCustomerOrderJsonElement;
	/**
	 * Holds mOrderDetailsOrderLinesJsonElement.
	 */
	private String mOrderDetailsOrderLinesJsonElement;
	/**
	 * Holds mOrderDetailsCustomerOrderLineJsonElement.
	 */
	private String mOrderDetailsCustomerOrderLineJsonElement;
	/**
	 * Holds mOrderDetailsShipmentDetailsJsonElement.
	 */
	private String mOrderDetailsShipmentDetailsJsonElement;
	/**
	 * Holds mOrderDetailsShipmentDetailJsonElement.
	 */
	private String mOrderDetailsShipmentDetailJsonElement;
	/**
	 * Holds mOrderDetailsCartonsJsonElement.
	 */
	private String mOrderDetailsCartonsJsonElement;
	/**
	 * Holds mOrderDetailsCartonJsonElement.
	 */
	private String mOrderDetailsCartonJsonElement;
	/**
	 * Holds mOrderDetailsCartonDoNbrJsonElement.
	 */
	private String mOrderDetailsCartonDoNbrJsonElement;
	/**
	 * Holds mOrderDetailsDoDetailsJsonElement.
	 */
	private String mOrderDetailsDoDetailsJsonElement;
	
	/** Holds mOrderDetailsDoDetailJsonElement. */
	private String mOrderDetailsDoDetailJsonElement;
	/**
	 * Holds mOrderDetailsdoDetailsDoNbrJsonElement.
	 */
	private String mOrderDetailsdoDetailsDoNbrJsonElement;
	/**
	 * Holds mOrderDetailsDoLineDetailJsonElement.
	 */
	private String mOrderDetailsDoLineDetailJsonElement;
	/**
	 * Holds mOrderDetailsCoLineNbrJsonElement.
	 */
	private String mOrderDetailsCoLineNbrJsonElement;
	/**
	 * Holds mOrderDetailsShippingInfoJsonElement.
	 */
	private String mOrderDetailsShippingInfoJsonElement;
	/**
	 * Holds mOrderDetailsOrderLineNumberJsonElement.
	 */
	private String mOrderDetailsOrderLineNumberJsonElement;
	/**
	 * Holds mOrderDetailsChargeDetailsJsonElement.
	 */
	private String mOrderDetailsChargeDetailsJsonElement;
	/**
	 * Holds mOrderDetailsChargeDetailJsonElement.
	 */
	private String mOrderDetailsChargeDetailJsonElement;
	/**
	 * Holds mOrderDetailsChargeCategoryJsonElement.
	 */
	private String mOrderDetailsChargeCategoryJsonElement;
	/**
	 * Holds mOrderDetailsChargeNameJsonElement.
	 */
	private String mOrderDetailsChargeNameJsonElement;
	/**
	 * Holds mOrderDetailsChargeCategoryShipping.
	 */
	private String mOrderDetailsChargeCategoryShipping;
	/**
	 * Holds mOrderDetailsChargeNameShipping.
	 */
	private String mOrderDetailsChargeNameShipping;
	/**
	 * Holds mOrderDetailsChargeNameShippingSurcharge.
	 */
	private String mOrderDetailsChargeNameShippingSurcharge;
	/**
	 * Holds mOrderDetailsChargeCategoryVS.
	 */
	private String mOrderDetailsChargeCategoryVS;
	/**
	 * Holds mOrderDetailsChargeCategoryMisc.
	 */
	private String mOrderDetailsChargeCategoryMisc;
	/**
	 * Holds mOrderDetailsChargeNameGiftwrap.
	 */
	private String mOrderDetailsChargeNameGiftwrap;
	/**
	 * Holds mOrderDetailsChargeNameEwasteFee.
	 */
	private String mOrderDetailsChargeNameEwasteFee;
	/**
	 * Holds mOrderDetailsChargeAmountJsonElement.
	 */
	private String mOrderDetailsChargeAmountJsonElement;
	/**
	 * Holds mOrderDetailsSurchargeJsonElement.
	 */
	private String mOrderDetailsSurchargeJsonElement;
	/**
	 * Holds mOrderDetailsOrderTotalsJsonElement.
	 */
	private String mOrderDetailsOrderTotalsJsonElement;
	/**
	 * Holds mOrderDetailsCustomerOrderTotalDetailJsonElement.
	 */
	private String mOrderDetailsCustomerOrderTotalDetailJsonElement;
	/**
	 * Holds mOrderDetailsShippingJsonElement.
	 */
	private String mOrderDetailsShippingJsonElement;
	/**
	 * Holds mOrderDetailsGiftwrapJsonElement.
	 */
	private String mOrderDetailsGiftwrapJsonElement;
	/**
	 * Holds mOrderDetailsEwasteJsonElement.
	 */
	private String mOrderDetailsEwasteJsonElement;
	/**
	 * Holds mCustomerTransactionListResponseJsonElement.
	 */
	private String mCustomerTransactionListResponseJsonElement;

	/**
	 * Holds mCustomerOrdersJsonElement.
	 */
	private String mCustomerOrdersJsonElement;

	/**
	 * Holds mCustomerOrderJsonElement.
	 */
	private String mCustomerOrderJsonElement;

	/**
	 * Holds mOrderStatusJsonElement.
	 */
	private String mOrderStatusJsonElement;
	/**
	 * Holds mTaxRequestType.
	 */
	private String mTaxRequestType;
	/**
	 * Holds mTaxCategory.
	 */
	private String mTaxCategory;
	
	/** Holds mTaxStateLocationName. */
	private String mTaxStateLocationName;
	/**
	 * Holds mTaxCityLocationName.
	 */
	private String mTaxCityLocationName;
	/**
	 * Holds mTaxCountyLocationName.
	 */
	private String mTaxCountyLocationName;
	/**
	 * Holds mTaxSecStateLocationName.
	 */
	private String mTaxSecStateLocationName;
	/**
	 * Holds mTaxSecCityLocationName.
	 */
	private String mTaxSecCityLocationName;
	/**
	 * Holds mTaxSecCountyLocationName.
	 */
	private String mTaxSecCountyLocationName;

	/**
	 * Holds mOrderDetailsItemCountJsonElement.
	 */
	private String mOrderDetailsItemCountJsonElement;

	/**
	 * Holds orderResponseCustomerBasicInfoJsonElement.
	 */
	private String mOrderResponseCustomerBasicInfoJsonElement;
	/**
	 * Holds orderResponseMessagesJsonElement.
	 */
	private String mOrderResponseMessagesJsonElement;
	/**
	 * Holds orderResponseMessageJsonElement.
	 */
	private String mOrderResponseMessageJsonElement;
	/**
	 * Holds orderResponseSeverityJsonElement.
	 */
	private String mOrderResponseSeverityJsonElement;
	/**
	 * Holds mOrderResponseDescriptionJsonElement.
	 */
	private String mOrderResponseDescriptionJsonElement;

	/**
	 * Holds mOrderDetailsOrderCreatedDateJsonElement.
	 */
	private String mOrderDetailsOrderCreatedDateJsonElement;

	/** The m online pid. */
	private String mOnlinePID;
	/**
	 * Holds mOrderDetailsCustomerOrderNotesJsonElement.
	 */
	private String mOrderDetailsCustomerOrderNotesJsonElement;
	/**
	 * Holds mOrderDetailsCustomerOrderNoteJsonElement.
	 */
	private String mOrderDetailsCustomerOrderNoteJsonElement;
	/**
	 * Holds mOrderDetailsItemIdJsonElement.
	 */
	private String mOrderDetailsItemIdJsonElement;
	/**
	 * Holds mOrderDetailsProductPageUrlJsonElement.
	 */
	private String mOrderDetailsProductPageUrlJsonElement;
	/**
	 * Holds mOrderDetailsNoteTypeJsonElement.
	 */
	private String mOrderDetailsNoteTypeJsonElement;
	/**
	 * Holds mOrderDetailsNoteTextJsonElement.
	 */
	private String mOrderDetailsNoteTextJsonElement;
	/**
	 * Holds mOrderDetailsTransactionDateJsonElement.
	 */
	private String mOrderDetailsTransactionDateJsonElement;
	/**
	 * Holds mOrderDetailsTransactionTotalAmountElement.
	 */
	private String mOrderDetailsTransactionTotalAmountElement;
	/**
	 * Holds mOrderDetailsNextPaymentAmountJsonElement.
	 */
	private String mOrderDetailsNextPaymentAmountJsonElement;
	/**
	 * Holds mOrderDetailsRemainingAmountJsonElement.
	 */
	private String mOrderDetailsRemainingAmountJsonElement;
	/**
	 * Holds mOrderDetailsNextPaymentDueJsonElement.
	 */
	private String mOrderDetailsNextPaymentDueJsonElement;
	/**
	 * Holds mOrderDetailsIsTranctionAvailableJsonElement.
	 */
	private String mOrderDetailsIsTranctionAvailableJsonElement;
	/**
	 * Holds mOrderDetailsNIsRemainingAmountJsonElement.
	 */
	private String mOrderDetailsIsRemainingAmountJsonElement;
	/**
	 * Holds mOrderDetailsSiteNameForGWJsonElement.
	 */
	private String mOrderDetailsSiteNameForGWJsonElement;
	/**
	 * Holds mOrderDetailsVasJsonElement.
	 */
	private String mOrderDetailsVasJsonElement;
	/**
	 * Holds mOrderDetailsBppHypenJsonElement.
	 */
	private String mOrderDetailsBppHypenJsonElement;
	/**
	 * Holds mOrderDetailsBppTermJsonElement.
	 */
	private String mOrderDetailsBppTermJsonElement;
	/**
	 * Holds mOrderDetailsBppChargeAmountJsonElement.
	 */
	private String mOrderDetailsBppChargeAmountJsonElement;
	/**
	 * Holds mOrderDetailsTermPropertyName.
	 */
	private String mOrderDetailsTermPropertyName;
	/**
	 * Holds mOrderDetailsDisplayNamePropertyName.
	 */
	private String mOrderDetailsDisplayNamePropertyName;
	/**
	 * Holds mOrderDetailsPaymentDetailsJsonElement.
	 */
	private String mOrderDetailsPaymentDetailsJsonElement;
	/**
	 * Holds mOrderDetailsPaymentDetailJsonElement.
	 */
	private String mOrderDetailsPaymentDetailJsonElement;
	/**
	 * Holds mOrderDetailsResponseStatusJsonElement.
	 */
	private String mOrderDetailsResponseStatusJsonElement;
	/**
	 * Holds mOrderDetailsShippingInfoArrayJsonElement.
	 */
	private String mOrderDetailsShippingInfoArrayJsonElement;
	/**
	 * Holds mOrderDetailsIscartonAvailableJsonElement.
	 */
	private String mOrderDetailsIscartonAvailableJsonElement;
	/**
	 * Holds mOrderDetailsDisplayOrderJsonElement.
	 */
	private String mOrderDetailsDisplayOrderJsonElement;
	/**
	 * Holds mShippingMethodMap.
	 */
	private Map<String, String> mShippingMethodMap;
	/**
	 * Holds mCustomerFullDetailsJsonElement.
	 */
	private String mCustomerFullDetailsJsonElement;
	/**
	 * Holds mLayawayOrderHistoryReserveProperty.
	 */
	private String mLayawayOrderHistoryReserveProperty;
	/**
	 * Holds mGenerateCOImportRequestInSync.
	 */
	private boolean mGenerateCOImportRequestInSync;
	/**
	 * Holds mOrderDetailsErrorMessageString.
	 */
	private String mOrderDetailsErrorMessageString;
	/**
	 * Holds mOrderHistoryErrorMessageString.
	 */
	private String mOrderHistoryErrorMessageString;
	/**
	 * Holds mCustomerMasterErrorMessageString.
	 */
	private String mCustomerMasterErrorMessageString;
	/**
	 * Holds mCustomerOrderErrorMessageString.
	 */
	private String mCustomerOrderErrorMessageString;
	/**
	 * Holds mCodeJsonElement.
	 */
	private String mCodeJsonElement;
	/**
	 * Holds mDescriptionJsonElement.
	 */
	private String mDescriptionJsonElement;
	/**
	 * Holds mCoRequestRestURL.
	 */
	private String mCoRequestRestURL;
	
	/**
	 * Holds mDeliveryOptionSTS.
	 */
	private String mDeliveryOptionSTS;
	/**
	 * Holds mDeliveryOptionSTA.
	 */
	private String mDeliveryOptionSTA;
	/**
	 * Holds mTransactionDecisionSuccess.
	 */
	private String mTransactionDecisionSuccess;
	/**
	 * Holds mGiftMessageBPPNoteCode.
	 */
	private String mGiftMessageBPPNoteCode;
	/**
	 * Holds mUidPipeDelimeter.
	 */
	private String mUidPipeDelimeter;
	/**
	 * Holds mPaymentTransactionRecordStatus.
	 */
	private String mPaymentTransactionRecordStatus;
	/**
	 * Holds mPrimaryFirstName.
	 */
	private String mPrimaryFirstName;
	/**
	 * Holds mPaymentTransactionRecordStatus.
	 */
	private String mPrimaryLastName;
	/**
	 * Holds mPaymentTransactionRecordStatus.
	 */
	private String mPrimaryPhone;
	/**
	 * Holds mPaymentTransactionRecordStatus.
	 */
	private String mPrimaryEmail;
	
	/**
	 * Holds mOrderHistoryTotalNoOfRecordsJsonElement.
	 */
	private String mOrderHistoryTotalNoOfRecordsJsonElement;
	/**
	 * Holds mTransactionToRollback.
	 */
	private boolean mTransactionToRollback;
	
	/**
	 * Holds mTransactionToRollback.
	 */
	private String mOrderDetailsOrderCapturedDateJsonElement;
	
	/**
	 * Holds mOrderDetailsOrderedQuantityPropertyName.
	 */
	private String mOrderDetailsOrderedQuantityPropertyName;
	
	/**
	 * Holds mOrderDetailsTotalQuantityPropertyName.
	 */
	private String mOrderDetailsTotalQuantityPropertyName;
	/**
	 * Holds mOrderConfirmedJsonElement.
	 */
	private String mOrderConfirmedJsonElement;
	/**
	 * Holds mOnHoldJsonElement.
	 */
	private String mOnHoldJsonElement;
	/**
	 * Holds mMinimizeShipmentsJsonElement.
	 */
	private String mMinimizeShipmentsJsonElement;
	/**
	 * Holds mOrderSubtotalJsonElement.
	 */
	private String mOrderSubtotalJsonElement;
	/**
	 * Holds mOrderTotalJsonElement.
	 */
	private String mOrderTotalJsonElement;
	/**
	 * Holds mOrderCancelledJsonElement.
	 */
	private String mOrderCancelledJsonElement;
	/**
	 * Holds mNoteTypeShipping.
	 */
	private String mNoteTypeShipping;
	/**
	 * Holds mNoteTypeMisc.
	 */
	private String mNoteTypeMisc;
	/**
	 * Holds mIncludeCR82Changes.
	 */
	private boolean mIncludeCR82Changes;
	/**
	 * Holds mOrderDetailsIsGiftJsonElement.
	 */
	private String mOrderDetailsIsGiftJsonElement;
	/**
	 * Holds mOrderDetailsGiftMessageJsonElement.
	 */
	private String mOrderDetailsGiftMessageJsonElement;
	/**
	 * Holds mOrderDetailsTotalShippingSurchargeJsonElement.
	 */
	private String mOrderDetailsTotalShippingSurchargeJsonElement;
	/**
	 * Holds mOrderDetailsFormattedShippingJsonElement.
	 */
	private String mOrderDetailsFormattedShippingJsonElement;
	/**
	 * Holds mOrderDetailsFormattedGiftwrapJsonElement.
	 */
	private String mOrderDetailsFormattedGiftwrapJsonElement;
	/**
	 * Holds mOrderDetailsFormattedEwasteJsonElement.
	 */
	private String mOrderDetailsFormattedEwasteJsonElement;
	/**
	 * Holds mOrderDetailsFormattedTotalShippingSurchargeJsonElement.
	 */
	private String mOrderDetailsFormattedTotalShippingSurchargeJsonElement;
	/**
	 * Holds mOrderDetailsHelpAndReturnsURLJsonElement.
	 */
	private String mOrderDetailsHelpAndReturnsURLJsonElement;
	/**
	 * Holds mOrderDetailsHelpAndReturnsJsonElement.
	 */
	private String mOrderDetailsHelpAndReturnsJsonElement;
	
	/**
	 * Holds mOrderDetailsChargeNameValue.
	 */
	private String mOrderDetailsChargeNameValue;
	
	/**
	 * Holds mOrderDetailsChargeNameValue.
	 */
	private String mOrderDetailsCartonCountJsonElement;
	
	/**
	 * Holds mOrderDetailsShipmentItemCountJsonElement.
	 */
	private String mOrderDetailsShipmentItemCountJsonElement;
	
	/**
	 * Holds mOrderDetailstotalshippingSurchargeJsonElement.
	 */
	private String mOrderDetailstotalshippingSurchargeJsonElement;
	
	/**
	 * Holds mOrderDetailsNotesJsonElement.
	 */
	private String mOrderDetailsNotesJsonElement;
	
	/**
	 * Holds mOrderDetailsCartonDetailsJsonElement.
	 */
	private String mOrderDetailsCartonDetailsJsonElement;
	
	/**
	 * Holds mOrderDetailsCartonDetailJsonElement.
	 */
	private String mOrderDetailsCartonDetailJsonElement;
	
	/**
	 * Holds mOrderDetailsDoLineNbrJsonElement.
	 */
	private String mOrderDetailsDoLineNbrJsonElement;
	
	/**
	 * Holds mOrderDetailsDoLineItemsJsonElement.
	 */
	private String mOrderDetailsDoLineItemsJsonElement;
	
	/**
	 * Holds mOrderDetailsNoteJsonElement.
	 */
	private String mOrderDetailsDoLineItemJsonElement;
	
	/**
	 * Holds mOrderDetailsNoteJsonElement.
	 */
	private String mOrderDetailsNoteJsonElement;
	
	/** The  layaway order linedelivery option. */
	private String mLayawayOrderLinedeliveryOption;
	
	/** The  layaway gift card and pay instore check. */
	private Boolean mLayawayGiftCardAndPayInstoreCheck;
	
	/** The  payment detailsbill to detail json element. */
	private String mPaymentDetailsbillToDetailJsonElement;
	
	/** The m payment detailsbill to detail first name json element. */
	private String mPaymentDetailsbillToDetailFirstNameJsonElement;

	/** The  payment detailsbill to detail last name json element. */
	private String mPaymentDetailsbillToDetailLastNameJsonElement;
	
	/** The  customer account display name. */
	private String mCustomerAccountDisplayName;
	
	/** The  payment details payment detail account display number. */
	private String mPaymentDetailsPaymentDetailAccountDisplayNumber;
	
	/** The  order details shipping info ship via json element. */
	private String mOrderDetailsShippingInfoShipViaJsonElement;
	
	/** The  order details shipping info ship via display json element. */
	private String mOrderDetailsShippingInfoShipViaDisplayJsonElement;
	

	/** The  order details reference fields. */
	private String mOrderDetailsReferenceFields;
	
	/** The  order details reference fields reference field3. */
	private String mOrderDetailsReferenceFieldsReferenceField3;
	
	/** The  order details reference fields reference field4. */
	private String mOrderDetailsReferenceFieldsReferenceField4;

	/** The  order details start date. */
	private String mOrderDetailsStartDate;

	/** The  order details end date. */
	private String mOrderDetailsEndDate;
	
	/** The  shipping info item count json element. */
	private String mShippingInfoItemCountJsonElement;
	
	/** The m shipping info item total count json element. */
	private String mShippingInfoItemTotalCountJsonElement;
	
	/** The  shipping info shippment array. */
	private String mShippingInfoShippmentArray;
	
	/** The  order details shipping info shipping address. */
	private String mOrderDetailsShippingInfoShippingAddress;
	
	/** The  order lines note note type freight. */
	private String mOrderLinesNoteNoteTypeFreight;
	
	/** The  order details carton line item count json element. */
	private String mOrderDetailsCartonLineItemCountJsonElement;
	
	/** mOrderDetailsCartonCheckJsonElement. */
	private String mOrderDetailsCartonCheckJsonElement;
	
	/** mOrderDetailsIsCartonCheckJsonElement. */
	private String mOrderDetailsIsCartonCheckJsonElement;
	
	/** mOrderDetailsCustomAttributeListJsonElement. */
	private String mOrderDetailsCustomAttributeListJsonElement;
	
	/** mOrderDetailsNameJsonElement. */
	private String mOrderDetailsNameJsonElement;
	
	/** mOrderDetailsValueJsonElement. */
	private String mOrderDetailsValueJsonElement;
	
	/** mOrderDetailsPrimaryNamesJsonElement. */
	private String mOrderDetailsPrimaryNamesJsonElement;
	
	/** mOrderDetailsProxyNamesJsonElement. */
	private String mOrderDetailsProxyNamesJsonElement;
	
	/** mOrderDetailsShipToFacilityJsonElement. */
	private String mOrderDetailsShipToFacilityJsonElement;
	
	/** mShipToFacilityForDonation. */
	private String mShipToFacilityForDonation;
	
	/** mOrderHistoryOrderTypePropertyName. */
	private String mOrderHistoryOrderTypePropertyName;	
	
	/** mImageFitRatio. */
	private String mImageFitRatio;
	/**
	 * Gets the order details imageFitRatio json element.
	 * 
	 * @return the order details imageFitRatio json element
	 */
	/**
	 * Gets the image fit ratio.
	 *
	 * @return the image fit ratio
	 */
	public String getImageFitRatio() {
		return mImageFitRatio;
	}
	/**
	 * Sets the order details imageFitRatio json element.
	 * 
	 * @param pImageFitRatio order details json element
	 */
	/**
	 * Sets the image fit ratio.
	 *
	 * @param pImageFitRatio the new image fit ratio
	 */
	public void setImageFitRatio(String pImageFitRatio) {
		this.mImageFitRatio = pImageFitRatio;
	}

	/** The NoteType TPR for setting the DealId.  */
	
	private String mTprElement;
	
	/**
	 * Gets the NoteType TPR.
	 * @return the NoteType as TPR.
	 */
	public String getTprElement() {
		return mTprElement;
	}
	
	/**
	 * Used to set the NoteType TPR.
	 * @param pTprElement  to set the TPR Element.
	 */
	public void setTprElement(String pTprElement) {
		this.mTprElement = pTprElement;
	}

	/**
	 * Gets the order details carton line item count json element.
	 *
	 * @return the order details carton line item count json element
	 */
	public String getOrderDetailsCartonLineItemCountJsonElement() {
		return mOrderDetailsCartonLineItemCountJsonElement;
	}

	/**
	 * Sets the order details carton line item count json element.
	 *
	 * @param pOrderDetailsCartonLineItemCountJsonElement the new order details carton line item count json element
	 */
	public void setOrderDetailsCartonLineItemCountJsonElement(
			String pOrderDetailsCartonLineItemCountJsonElement) {
		 mOrderDetailsCartonLineItemCountJsonElement = pOrderDetailsCartonLineItemCountJsonElement;
	}

	/**
	 * Gets the order lines note note type freight.
	 *
	 * @return the order lines note note type freight
	 */
	public String getOrderLinesNoteNoteTypeFreight() {
		return mOrderLinesNoteNoteTypeFreight;
	}

	/**
	 * Sets the order lines note note type freight.
	 *
	 * @param pOrderLinesNoteNoteTypeFreight the new order lines note note type freight
	 */
	public void setOrderLinesNoteNoteTypeFreight(
			String pOrderLinesNoteNoteTypeFreight) {
		  mOrderLinesNoteNoteTypeFreight = pOrderLinesNoteNoteTypeFreight;
	}

	/**
	 * Gets the order details shipping info shipping address.
	 *
	 * @return the order details shipping info shipping address
	 */
	public String getOrderDetailsShippingInfoShippingAddress() {
		return mOrderDetailsShippingInfoShippingAddress;
	}

	/**
	 * Sets the order details shipping info shipping address.
	 *
	 * @param pOrderDetailsShippingInfoShippingAddress the new order details shipping info shipping address
	 */
	public void setOrderDetailsShippingInfoShippingAddress(
			String pOrderDetailsShippingInfoShippingAddress) {
		  mOrderDetailsShippingInfoShippingAddress = pOrderDetailsShippingInfoShippingAddress;
	}

	/**
	 * Gets the shipping info shippment array.
	 *
	 * @return the shipping info shippment array
	 */
	public String getShippingInfoShippmentArray() {
		return mShippingInfoShippmentArray;
	}

	/**
	 * Sets the shipping info shippment array.
	 *
	 * @param pShippingInfoShippmentArray the new shipping info shippment array
	 */
	public void setShippingInfoShippmentArray(String pShippingInfoShippmentArray) {
		  mShippingInfoShippmentArray = pShippingInfoShippmentArray;
	}

	/**
	 * Gets the shipping info item total count json element.
	 *
	 * @return the shipping info item total count json element
	 */
	public String getShippingInfoItemTotalCountJsonElement() {
		return mShippingInfoItemTotalCountJsonElement;
	}

	/**
	 * Sets the shipping info item total count json element.
	 *
	 * @param pShippingInfoItemTotalCountJsonElement the new shipping info item total count json element
	 */
	public void setShippingInfoItemTotalCountJsonElement(
			String pShippingInfoItemTotalCountJsonElement) {
		 mShippingInfoItemTotalCountJsonElement = pShippingInfoItemTotalCountJsonElement;
	}

	/**
	 * Gets the shipping info item count json element.
	 *
	 * @return the shipping info item count json element
	 */
	public String getShippingInfoItemCountJsonElement() {
		return mShippingInfoItemCountJsonElement;
	}

	/**
	 * Sets the shipping info item count json element.
	 *
	 * @param pShippingInfoItemCountJsonElement the new shipping info item count json element
	 */
	public void setShippingInfoItemCountJsonElement(
			String pShippingInfoItemCountJsonElement) {
		 mShippingInfoItemCountJsonElement = pShippingInfoItemCountJsonElement;
	}

	/**
	 * Gets the order details end date.
	 *
	 * @return the order details end date
	 */
	public String getOrderDetailsEndDate() {
		return mOrderDetailsEndDate;
	}
	
	/**
	 * Sets the order details end date.
	 *
	 * @param pOrderDetailsEndDate the new order details end date
	 */
	public void setOrderDetailsEndDate(String pOrderDetailsEndDate) {
		this.mOrderDetailsEndDate = pOrderDetailsEndDate;
	}
	
	
	/**
	 * Gets the order details start date.
	 *
	 * @return the order details start date
	 */
	public String getOrderDetailsStartDate() {
		return mOrderDetailsStartDate;
	}

	/**
	 * Sets the order details start date.
	 *
	 * @param pOrderDetailsStartDate the new order details start date
	 */
	public void setOrderDetailsStartDate(String pOrderDetailsStartDate) {
		this.mOrderDetailsStartDate = pOrderDetailsStartDate;
	}

	/**
	 * Gets the m order details reference fields.
	 *
	 * @return the m order details reference fields
	 */
	public String getOrderDetailsReferenceFields() {
		return mOrderDetailsReferenceFields;
	}

	/**
	 * Sets the m order details reference fields.
	 *
	 * @param pOrderDetailsReferenceFields the new order details reference fields
	 */
	public void setOrderDetailsReferenceFields(String pOrderDetailsReferenceFields) {
		mOrderDetailsReferenceFields = pOrderDetailsReferenceFields;
	}

	/**
	 * Gets the m order details reference fields reference field3.
	 *
	 * @return the m order details reference fields reference field3
	 */
	public String getOrderDetailsReferenceFieldsReferenceField3() {
		return mOrderDetailsReferenceFieldsReferenceField3;
	}

	/**
	 * Sets the m order details reference fields reference field3.
	 *
	 * @param pOrderDetailsReferenceFieldsReferenceField3 the new order details reference fields reference field3
	 */
	public void setOrderDetailsReferenceFieldsReferenceField3(
			String pOrderDetailsReferenceFieldsReferenceField3) {
		 mOrderDetailsReferenceFieldsReferenceField3 = pOrderDetailsReferenceFieldsReferenceField3;
	}

	/**
	 * Gets the m order details reference fields reference field4.
	 *
	 * @return the m order details reference fields reference field4
	 */
	public String getOrderDetailsReferenceFieldsReferenceField4() {
		return mOrderDetailsReferenceFieldsReferenceField4;
	}

	/**
	 * Sets the m order details reference fields reference field4.
	 *
	 * @param pOrderDetailsReferenceFieldsReferenceField4 the new order details reference fields reference field4
	 */
	public void setOrderDetailsReferenceFieldsReferenceField4(
			String pOrderDetailsReferenceFieldsReferenceField4) {
		mOrderDetailsReferenceFieldsReferenceField4 = pOrderDetailsReferenceFieldsReferenceField4;
	}

	
	/**
	 * Gets the order details shipping info ship via display json element.
	 *
	 * @return the order details shipping info ship via display json element
	 */
	public String getOrderDetailsShippingInfoShipViaDisplayJsonElement() {
		return mOrderDetailsShippingInfoShipViaDisplayJsonElement;
	}

	/**
	 * Sets the order details shipping info ship via display json element.
	 *
	 * @param pOrderDetailsShippingInfoShipViaDisplayJsonElement the new order details shipping info ship via display json element
	 */
	public void setOrderDetailsShippingInfoShipViaDisplayJsonElement(
			String pOrderDetailsShippingInfoShipViaDisplayJsonElement) {
		mOrderDetailsShippingInfoShipViaDisplayJsonElement = pOrderDetailsShippingInfoShipViaDisplayJsonElement;
	}

	/**
	 * Gets the order details shipping info ship via json element.
	 *
	 * @return the order details shipping info ship via json element
	 */
	public String getOrderDetailsShippingInfoShipViaJsonElement() {
		return mOrderDetailsShippingInfoShipViaJsonElement;
	}

	/**
	 * Sets the order details shipping info ship via json element.
	 *
	 * @param pOrderDetailsShippingInfoShipViaJsonElement the new order details shipping info ship via json element
	 */
	public void setOrderDetailsShippingInfoShipViaJsonElement(
			String pOrderDetailsShippingInfoShipViaJsonElement) {
		mOrderDetailsShippingInfoShipViaJsonElement = pOrderDetailsShippingInfoShipViaJsonElement;
	}

	/**
	 * Gets the payment details payment detail account display number.
	 *
	 * @return the payment details payment detail account display number
	 */
	public String getPaymentDetailsPaymentDetailAccountDisplayNumber() {
		return mPaymentDetailsPaymentDetailAccountDisplayNumber;
	}

	/**
	 * Sets the payment details payment detail account display number.
	 *
	 * @param pPaymentDetailsPaymentDetailAccountDisplayNumber the new payment details payment detail account display number
	 */
	public void setPaymentDetailsPaymentDetailAccountDisplayNumber(
			String pPaymentDetailsPaymentDetailAccountDisplayNumber) {
		 mPaymentDetailsPaymentDetailAccountDisplayNumber = pPaymentDetailsPaymentDetailAccountDisplayNumber;
	}

	/**
	 * Gets the customer account display name.
	 *
	 * @return the customer account display name
	 */
	public String getCustomerAccountDisplayName() {
		return mCustomerAccountDisplayName;
	}

	/**
	 * Sets the customer account display name.
	 *
	 * @param pCustomerAccountDisplayName the new customer account display name
	 */
	public void setCustomerAccountDisplayName(String pCustomerAccountDisplayName) {
		this.mCustomerAccountDisplayName = pCustomerAccountDisplayName;
	}

	/** The  display name flag. */
	private String mDisplayNameFlag;
	
	
	/**
	 * Gets the display name flag.
	 *
	 * @return the display name flag
	 */
	public String getDisplayNameFlag() {
		return mDisplayNameFlag;
	}

	/**
	 * Sets the display name flag.
	 *
	 * @param pDisplayNameFlag the new display name flag
	 */
	public void setDisplayNameFlag(String pDisplayNameFlag) {
		 mDisplayNameFlag = pDisplayNameFlag;
	}

	/**
	 * Gets the payment detailsbill to detail first name json element.
	 *
	 * @return the payment detailsbill to detail first name json element
	 */
	public String getPaymentDetailsbillToDetailFirstNameJsonElement() {
		return mPaymentDetailsbillToDetailFirstNameJsonElement;
	}

	/**
	 * Sets the payment detailsbill to detail first name json element.
	 *
	 * @param pPaymentDetailsbillToDetailFirstNameJsonElement the new payment detailsbill to detail first name json element
	 */
	public void setPaymentDetailsbillToDetailFirstNameJsonElement(
			String pPaymentDetailsbillToDetailFirstNameJsonElement) {
		mPaymentDetailsbillToDetailFirstNameJsonElement = pPaymentDetailsbillToDetailFirstNameJsonElement;
	}
	
	/**
	 * Gets the payment detailsbill to detail last name json element.
	 *
	 * @return the payment detailsbill to detail last name json element
	 */
	public String getPaymentDetailsbillToDetailLastNameJsonElement() {
		return mPaymentDetailsbillToDetailLastNameJsonElement;
	}

	/**
	 * Sets the payment detailsbill to detail last name json element.
	 *
	 * @param pPaymentDetailsbillToDetailLastNameJsonElement the new payment detailsbill to detail last name json element
	 */
	public void setPaymentDetailsbillToDetailLastNameJsonElement(
			String pPaymentDetailsbillToDetailLastNameJsonElement) {
		mPaymentDetailsbillToDetailLastNameJsonElement = pPaymentDetailsbillToDetailLastNameJsonElement;
	}

	/**
	 * Gets the payment detailsbill to detail json element.
	 *
	 * @return the payment detailsbill to detail json element
	 */
	public String getPaymentDetailsbillToDetailJsonElement() {
		return mPaymentDetailsbillToDetailJsonElement;
	}

	/**
	 * Sets the payment detailsbill to detail json element.
	 *
	 * @param pPaymentDetailsbillToDetailJsonElement the new payment detailsbill to detail json element
	 */
	public void setPaymentDetailsbillToDetailJsonElement(
			String pPaymentDetailsbillToDetailJsonElement) {
		  mPaymentDetailsbillToDetailJsonElement = pPaymentDetailsbillToDetailJsonElement;
	}

	/**
	 * Gets the layaway gift card and pay instore check.
	 *
	 * @return the layaway gift card and pay instore check
	 */
	public Boolean getLayawayGiftCardAndPayInstoreCheck() {
		return mLayawayGiftCardAndPayInstoreCheck;
	}

	/**
	 * Sets the layaway gift card and pay instore check.
	 *
	 * @param pLayawayGiftCardAndPayInstoreCheck the new layaway gift card and pay instore check
	 */
	public void setLayawayGiftCardAndPayInstoreCheck(
			Boolean pLayawayGiftCardAndPayInstoreCheck) {
		 mLayawayGiftCardAndPayInstoreCheck = pLayawayGiftCardAndPayInstoreCheck;
	}

	/**
	 * Gets the layaway order linedelivery option.
	 *
	 * @return the layaway order linedelivery option
	 */
	public String getLayawayOrderLinedeliveryOption() {
		return mLayawayOrderLinedeliveryOption;
	}

	/**
	 * Sets the layaway order linedelivery option.
	 *
	 * @param pLayawayOrderLinedeliveryOption the new layaway order linedelivery option
	 */
	public void setLayawayOrderLinedeliveryOption(
			String pLayawayOrderLinedeliveryOption) {
		mLayawayOrderLinedeliveryOption = pLayawayOrderLinedeliveryOption;
	}

	/** The m order details note sequence json element. */
	private String mOrderDetailsNoteSequenceJsonElement;
	
	/**
	 * Gets the order details note sequence json element.
	 *
	 * @return the order details note sequence json element
	 */
	public String getOrderDetailsNoteSequenceJsonElement() {
		return mOrderDetailsNoteSequenceJsonElement;
	}

	/**
	 * Sets the order details note sequence json element.
	 *
	 * @param pOrderDetailsNoteSequenceJsonElement the new order details note sequence json element
	 */
	public void setOrderDetailsNoteSequenceJsonElement(
			String pOrderDetailsNoteSequenceJsonElement) {
		this.mOrderDetailsNoteSequenceJsonElement = pOrderDetailsNoteSequenceJsonElement;
	}

	/**
	 * Gets the order detailstotalshipping surcharge json element.
	 *
	 * @return the orderDetailstotalshippingSurchargeJsonElement.
	 */
	public String getOrderDetailstotalshippingSurchargeJsonElement() {
		return mOrderDetailstotalshippingSurchargeJsonElement;
	}

	/**
	 * Sets the order detailstotalshipping surcharge json element.
	 *
	 * @param pOrderDetailstotalshippingSurchargeJsonElement the orderDetailstotalshippingSurchargeJsonElement to set.
	 */
	public void setOrderDetailstotalshippingSurchargeJsonElement(
			String pOrderDetailstotalshippingSurchargeJsonElement) {
		mOrderDetailstotalshippingSurchargeJsonElement = pOrderDetailstotalshippingSurchargeJsonElement;
	}

	/**
	 * Gets the transaction id poperty name.
	 *
	 * @return the transactionIdPopertyName
	 */
	public String getTransactionIdPopertyName() {
		return mTransactionIdPopertyName;
	}

	/**
	 * Sets the transaction id poperty name.
	 *
	 * @param pTransactionIdPopertyName            the transactionIdPopertyName to set
	 */
	public void setTransactionIdPopertyName(String pTransactionIdPopertyName) {
		mTransactionIdPopertyName = pTransactionIdPopertyName;
	}

	/**
	 * Gets the request poperty name.
	 *
	 * @return the requestPopertyName
	 */
	public String getRequestPopertyName() {
		return mRequestPopertyName;
	}

	/**
	 * Sets the request poperty name.
	 *
	 * @param pRequestPopertyName            the requestPopertyName to set
	 */
	public void setRequestPopertyName(String pRequestPopertyName) {
		mRequestPopertyName = pRequestPopertyName;
	}

	/**
	 * Gets the response poperty name.
	 *
	 * @return the responsePopertyName
	 */
	public String getResponsePopertyName() {
		return mResponsePopertyName;
	}

	/**
	 * Sets the response poperty name.
	 *
	 * @param pResponsePopertyName            the responsePopertyName to set
	 */
	public void setResponsePopertyName(String pResponsePopertyName) {
		mResponsePopertyName = pResponsePopertyName;
	}

	/**
	 * Gets the type poperty name.
	 *
	 * @return the typePopertyName
	 */
	public String getTypePopertyName() {
		return mTypePopertyName;
	}

	/**
	 * Sets the type poperty name.
	 *
	 * @param pTypePopertyName            the typePopertyName to set
	 */
	public void setTypePopertyName(String pTypePopertyName) {
		mTypePopertyName = pTypePopertyName;
	}

	/**
	 * Gets the status poperty name.
	 *
	 * @return the statusPopertyName
	 */
	public String getStatusPopertyName() {
		return mStatusPopertyName;
	}

	/**
	 * Sets the status poperty name.
	 *
	 * @param pStatusPopertyName            the statusPopertyName to set
	 */
	public void setStatusPopertyName(String pStatusPopertyName) {
		mStatusPopertyName = pStatusPopertyName;
	}

	/**
	 * Gets the transaction time poperty name.
	 *
	 * @return the transactionTimePopertyName
	 */
	public String getTransactionTimePopertyName() {
		return mTransactionTimePopertyName;
	}

	/**
	 * Sets the transaction time poperty name.
	 *
	 * @param pTransactionTimePopertyName            the transactionTimePopertyName to set
	 */
	public void setTransactionTimePopertyName(String pTransactionTimePopertyName) {
		mTransactionTimePopertyName = pTransactionTimePopertyName;
	}

	/**
	 * Gets the oms trans records item descriptor name.
	 *
	 * @return the omsTransRecordsItemDescriptorName
	 */
	public String getOmsTransRecordsItemDescriptorName() {
		return mOmsTransRecordsItemDescriptorName;
	}

	/**
	 * Sets the oms trans records item descriptor name.
	 *
	 * @param pOmsTransRecordsItemDescriptorName            the omsTransRecordsItemDescriptorName to set
	 */
	public void setOmsTransRecordsItemDescriptorName(
			String pOmsTransRecordsItemDescriptorName) {
		mOmsTransRecordsItemDescriptorName = pOmsTransRecordsItemDescriptorName;
	}

	/**
	 * This method returns the tcCompanyIdValue value.
	 *
	 * @return the tcCompanyIdValue
	 */
	public String getTcCompanyIdValue() {
		return mTcCompanyIdValue;
	}

	/**
	 * This method sets the tcCompanyIdValue with parameter value
	 * pTcCompanyIdValue.
	 *
	 * @param pTcCompanyIdValue            the tcCompanyIdValue to set
	 */
	public void setTcCompanyIdValue(String pTcCompanyIdValue) {
		mTcCompanyIdValue = pTcCompanyIdValue;
	}

	/**
	 * This method returns the orderTypeValueForStore value.
	 *
	 * @return the orderTypeValueForStore
	 */
	public String getOrderTypeValueForStore() {
		return mOrderTypeValueForStore;
	}

	/**
	 * This method sets the orderTypeValueForStore with parameter value
	 * pOrderTypeValueForStore.
	 *
	 * @param pOrderTypeValueForStore            the orderTypeValueForStore to set
	 */
	public void setOrderTypeValueForStore(String pOrderTypeValueForStore) {
		mOrderTypeValueForStore = pOrderTypeValueForStore;
	}

	/**
	 * This method returns the orderTypeValueForCSC value.
	 *
	 * @return the orderTypeValueForCSC
	 */
	public String getOrderTypeValueForCSC() {
		return mOrderTypeValueForCSC;
	}

	/**
	 * This method sets the orderTypeValueForCSC with parameter value
	 * pOrderTypeValueForCSC.
	 *
	 * @param pOrderTypeValueForCSC            the orderTypeValueForCSC to set
	 */
	public void setOrderTypeValueForCSC(String pOrderTypeValueForCSC) {
		mOrderTypeValueForCSC = pOrderTypeValueForCSC;
	}

	/**
	 * This method returns the orderTypeValueForSOS value.
	 *
	 * @return the orderTypeValueForSOS
	 */
	public String getOrderTypeValueForSOS() {
		return mOrderTypeValueForSOS;
	}

	/**
	 * This method sets the orderTypeValueForSOS with parameter value
	 * pOrderTypeValueForSOS.
	 *
	 * @param pOrderTypeValueForSOS            the orderTypeValueForSOS to set
	 */
	public void setOrderTypeValueForSOS(String pOrderTypeValueForSOS) {
		mOrderTypeValueForSOS = pOrderTypeValueForSOS;
	}

	/**
	 * This method returns the orderedQtyUOM value.
	 *
	 * @return the orderedQtyUOM
	 */
	public String getOrderedQtyUOM() {
		return mOrderedQtyUOM;
	}

	/**
	 * This method sets the orderedQtyUOM with parameter value pOrderedQtyUOM.
	 *
	 * @param pOrderedQtyUOM            the orderedQtyUOM to set
	 */
	public void setOrderedQtyUOM(String pOrderedQtyUOM) {
		mOrderedQtyUOM = pOrderedQtyUOM;
	}

	/**
	 * This method returns the deliveryOptionCustomerPickup value.
	 *
	 * @return the deliveryOptionCustomerPickup
	 */
	public String getDeliveryOptionCustomerPickup() {
		return mDeliveryOptionCustomerPickup;
	}

	/**
	 * This method sets the deliveryOptionCustomerPickup with parameter value
	 * pDeliveryOptionCustomerPickup.
	 *
	 * @param pDeliveryOptionCustomerPickup            the deliveryOptionCustomerPickup to set
	 */
	public void setDeliveryOptionCustomerPickup(
			String pDeliveryOptionCustomerPickup) {
		mDeliveryOptionCustomerPickup = pDeliveryOptionCustomerPickup;
	}

	/**
	 * This method returns the paymentGroupTypePropertyName value.
	 *
	 * @return the paymentGroupTypePropertyName
	 */
	public String getPaymentGroupTypePropertyName() {
		return mPaymentGroupTypePropertyName;
	}

	/**
	 * This method sets the paymentGroupTypePropertyName with parameter value
	 * pPaymentGroupTypePropertyName.
	 *
	 * @param pPaymentGroupTypePropertyName            the paymentGroupTypePropertyName to set
	 */
	public void setPaymentGroupTypePropertyName(
			String pPaymentGroupTypePropertyName) {
		mPaymentGroupTypePropertyName = pPaymentGroupTypePropertyName;
	}

	/**
	 * This method returns the paymentEntryType value.
	 *
	 * @return the paymentEntryType
	 */
	public String getPaymentEntryType() {
		return mPaymentEntryType;
	}

	/**
	 * This method sets the paymentEntryType with parameter value
	 * pPaymentEntryType.
	 *
	 * @param pPaymentEntryType            the paymentEntryType to set
	 */
	public void setPaymentEntryType(String pPaymentEntryType) {
		mPaymentEntryType = pPaymentEntryType;
	}

	/**
	 * Gets the coupon.
	 *
	 * @return the coupon
	 */
	public String getCoupon() {
		return mCoupon;
	}

	/**
	 * Sets the coupon.
	 *
	 * @param pCoupon            the coupon to set
	 */
	public void setCoupon(String pCoupon) {
		mCoupon = pCoupon;
	}

	/**
	 * Gets the vendor funded.
	 *
	 * @return the vendorFunded
	 */
	public String getVendorFunded() {
		return mVendorFunded;
	}

	/**
	 * Sets the vendor funded.
	 *
	 * @param pVendorFunded            the vendorFunded to set
	 */
	public void setVendorFunded(String pVendorFunded) {
		mVendorFunded = pVendorFunded;
	}

	/**
	 * Gets the retail funded.
	 *
	 * @return the retailFunded
	 */
	public String getRetailFunded() {
		return mRetailFunded;
	}

	/**
	 * Sets the retail funded.
	 *
	 * @param pRetailFunded            the retailFunded to set
	 */
	public void setRetailFunded(String pRetailFunded) {
		mRetailFunded = pRetailFunded;
	}

	/**
	 * Gets the discount detail item.
	 *
	 * @return the discountDetailItem
	 */
	public String getDiscountDetailItem() {
		return mDiscountDetailItem;
	}

	/**
	 * Sets the discount detail item.
	 *
	 * @param pDiscountDetailItem            the discountDetailItem to set
	 */
	public void setDiscountDetailItem(String pDiscountDetailItem) {
		mDiscountDetailItem = pDiscountDetailItem;
	}

	/**
	 * Gets the gift message note type.
	 *
	 * @return the giftMessageNoteType
	 */
	public String getGiftMessageNoteType() {
		return mGiftMessageNoteType;
	}

	/**
	 * Sets the gift message note type.
	 *
	 * @param pGiftMessageNoteType            the giftMessageNoteType to set
	 */
	public void setGiftMessageNoteType(String pGiftMessageNoteType) {
		mGiftMessageNoteType = pGiftMessageNoteType;
	}

	/**
	 * Gets the gift message vs note type.
	 *
	 * @return the giftMessageVSNoteType
	 */
	public String getGiftMessageVSNoteType() {
		return mGiftMessageVSNoteType;
	}

	/**
	 * Sets the gift message vs note type.
	 *
	 * @param pGiftMessageVSNoteType            the giftMessageVSNoteType to set
	 */
	public void setGiftMessageVSNoteType(String pGiftMessageVSNoteType) {
		mGiftMessageVSNoteType = pGiftMessageVSNoteType;
	}

	/**
	 * Gets the gift message gw note code.
	 *
	 * @return the giftMessageGWNoteCode
	 */
	public String getGiftMessageGWNoteCode() {
		return mGiftMessageGWNoteCode;
	}

	/**
	 * Sets the gift message gw note code.
	 *
	 * @param pGiftMessageGWNoteCode            the giftMessageGWNoteCode to set
	 */
	public void setGiftMessageGWNoteCode(String pGiftMessageGWNoteCode) {
		mGiftMessageGWNoteCode = pGiftMessageGWNoteCode;
	}

	/**
	 * Gets the gift message gift wrap note text.
	 *
	 * @return the giftMessageGiftWrapNoteText
	 */
	public String getGiftMessageGiftWrapNoteText() {
		return mGiftMessageGiftWrapNoteText;
	}

	/**
	 * Sets the gift message gift wrap note text.
	 *
	 * @param pGiftMessageGiftWrapNoteText            the giftMessageGiftWrapNoteText to set
	 */
	public void setGiftMessageGiftWrapNoteText(
			String pGiftMessageGiftWrapNoteText) {
		mGiftMessageGiftWrapNoteText = pGiftMessageGiftWrapNoteText;
	}

	/**
	 * Gets the charge category shipping.
	 *
	 * @return the chargeCategoryShipping
	 */
	public String getChargeCategoryShipping() {
		return mChargeCategoryShipping;
	}

	/**
	 * Sets the charge category shipping.
	 *
	 * @param pChargeCategoryShipping            the chargeCategoryShipping to set
	 */
	public void setChargeCategoryShipping(String pChargeCategoryShipping) {
		mChargeCategoryShipping = pChargeCategoryShipping;
	}

	/**
	 * Gets the charge name shipping.
	 *
	 * @return the chargeNameShipping
	 */
	public String getChargeNameShipping() {
		return mChargeNameShipping;
	}

	/**
	 * Sets the charge name shipping.
	 *
	 * @param pChargeNameShipping            the chargeNameShipping to set
	 */
	public void setChargeNameShipping(String pChargeNameShipping) {
		mChargeNameShipping = pChargeNameShipping;
	}

	/**
	 * Gets the charge category vas.
	 *
	 * @return the chargeCategoryVAS
	 */
	public String getChargeCategoryVAS() {
		return mChargeCategoryVAS;
	}

	/**
	 * Sets the charge category vas.
	 *
	 * @param pChargeCategoryVAS            the chargeCategoryVAS to set
	 */
	public void setChargeCategoryVAS(String pChargeCategoryVAS) {
		mChargeCategoryVAS = pChargeCategoryVAS;
	}

	/**
	 * Gets the charge name bpp.
	 *
	 * @return the chargeNameBPP
	 */
	public String getChargeNameBPP() {
		return mChargeNameBPP;
	}

	/**
	 * Sets the charge name bpp.
	 *
	 * @param pChargeNameBPP            the chargeNameBPP to set
	 */
	public void setChargeNameBPP(String pChargeNameBPP) {
		mChargeNameBPP = pChargeNameBPP;
	}

	/**
	 * Gets the charge name ship surcharge.
	 *
	 * @return the chargeNameShipSurcharge
	 */
	public String getChargeNameShipSurcharge() {
		return mChargeNameShipSurcharge;
	}

	/**
	 * Sets the charge name ship surcharge.
	 *
	 * @param pChargeNameShipSurcharge            the chargeNameShipSurcharge to set
	 */
	public void setChargeNameShipSurcharge(String pChargeNameShipSurcharge) {
		mChargeNameShipSurcharge = pChargeNameShipSurcharge;
	}

	/**
	 * Gets the charge category ship surcharge.
	 *
	 * @return the chargeCategoryShipSurcharge
	 */
	public String getChargeCategoryShipSurcharge() {
		return mChargeCategoryShipSurcharge;
	}

	/**
	 * Sets the charge category ship surcharge.
	 *
	 * @param pChargeCategoryShipSurcharge            the chargeCategoryShipSurcharge to set
	 */
	public void setChargeCategoryShipSurcharge(
			String pChargeCategoryShipSurcharge) {
		mChargeCategoryShipSurcharge = pChargeCategoryShipSurcharge;
	}

	/**
	 * Gets the charge category e911.
	 *
	 * @return the chargeCategoryE911
	 */
	public String getChargeCategoryE911() {
		return mChargeCategoryE911;
	}

	/**
	 * Sets the charge category e911.
	 *
	 * @param pChargeCategoryE911            the chargeCategoryE911 to set
	 */
	public void setChargeCategoryE911(String pChargeCategoryE911) {
		mChargeCategoryE911 = pChargeCategoryE911;
	}

	/**
	 * Gets the slapper.
	 *
	 * @return the slapper
	 */
	public String getSlapper() {
		return mSlapper;
	}

	/**
	 * Sets the slapper.
	 *
	 * @param pSlapper            the slapper to set
	 */
	public void setSlapper(String pSlapper) {
		mSlapper = pSlapper;
	}

	/**
	 * Gets the non slapper.
	 *
	 * @return the nonSlapper
	 */
	public String getNonSlapper() {
		return mNonSlapper;
	}

	/**
	 * Sets the non slapper.
	 *
	 * @param pNonSlapper            the nonSlapper to set
	 */
	public void setNonSlapper(String pNonSlapper) {
		mNonSlapper = pNonSlapper;
	}

	/**
	 * Gets the tru site id.
	 *
	 * @return the truSiteId
	 */
	public String getTruSiteId() {
		return mTruSiteId;
	}

	/**
	 * Sets the tru site id.
	 *
	 * @param pTruSiteId            the truSiteId to set
	 */
	public void setTruSiteId(String pTruSiteId) {
		mTruSiteId = pTruSiteId;
	}

	/**
	 * Gets the bru site id.
	 *
	 * @return the bruSiteId
	 */
	public String getBruSiteId() {
		return mBruSiteId;
	}

	/**
	 * Sets the bru site id.
	 *
	 * @param pBruSiteId            the bruSiteId to set
	 */
	public void setBruSiteId(String pBruSiteId) {
		mBruSiteId = pBruSiteId;
	}

	/**
	 * Gets the tru site parameter.
	 *
	 * @return the truSiteParameter
	 */
	public String getTruSiteParameter() {
		return mTruSiteParameter;
	}

	/**
	 * Sets the tru site parameter.
	 *
	 * @param pTruSiteParameter            the truSiteParameter to set
	 */
	public void setTruSiteParameter(String pTruSiteParameter) {
		mTruSiteParameter = pTruSiteParameter;
	}

	/**
	 * Gets the bru site parameter.
	 *
	 * @return the bruSiteParameter
	 */
	public String getBruSiteParameter() {
		return mBruSiteParameter;
	}

	/**
	 * Sets the bru site parameter.
	 *
	 * @param pBruSiteParameter            the bruSiteParameter to set
	 */
	public void setBruSiteParameter(String pBruSiteParameter) {
		mBruSiteParameter = pBruSiteParameter;
	}

	/**
	 * Gets the wish list ref field5.
	 *
	 * @return the wishListRefField5
	 */
	public String getWishListRefField5() {
		return mWishListRefField5;
	}

	/**
	 * Sets the wish list ref field5.
	 *
	 * @param pWishListRefField5            the wishListRefField5 to set
	 */
	public void setWishListRefField5(String pWishListRefField5) {
		mWishListRefField5 = pWishListRefField5;
	}

	/**
	 * Gets the registry ref field5.
	 *
	 * @return the registryRefField5
	 */
	public String getRegistryRefField5() {
		return mRegistryRefField5;
	}

	/**
	 * Sets the registry ref field5.
	 *
	 * @param pRegistryRefField5            the registryRefField5 to set
	 */
	public void setRegistryRefField5(String pRegistryRefField5) {
		mRegistryRefField5 = pRegistryRefField5;
	}

	/**
	 * Gets the order entry type web.
	 *
	 * @return the orderEntryTypeWeb
	 */
	public String getOrderEntryTypeWeb() {
		return mOrderEntryTypeWeb;
	}

	/**
	 * Sets the order entry type web.
	 *
	 * @param pOrderEntryTypeWeb            the orderEntryTypeWeb to set
	 */
	public void setOrderEntryTypeWeb(String pOrderEntryTypeWeb) {
		mOrderEntryTypeWeb = pOrderEntryTypeWeb;
	}

	/**
	 * Gets the order payment status.
	 *
	 * @return the orderPaymentStatus
	 */
	public String getOrderPaymentStatus() {
		return mOrderPaymentStatus;
	}

	/**
	 * Sets the order payment status.
	 *
	 * @param pOrderPaymentStatus            the orderPaymentStatus to set
	 */
	public void setOrderPaymentStatus(String pOrderPaymentStatus) {
		mOrderPaymentStatus = pOrderPaymentStatus;
	}

	/**
	 * Gets the order ref field4 for tru.
	 *
	 * @return the orderRefField4ForTRU
	 */
	public String getOrderRefField4ForTRU() {
		return mOrderRefField4ForTRU;
	}

	/**
	 * Sets the order ref field4 for tru.
	 *
	 * @param pOrderRefField4ForTRU            the orderRefField4ForTRU to set
	 */
	public void setOrderRefField4ForTRU(String pOrderRefField4ForTRU) {
		mOrderRefField4ForTRU = pOrderRefField4ForTRU;
	}

	/**
	 * Gets the order ref field4 for bru.
	 *
	 * @return the orderRefField4ForBRU
	 */
	public String getOrderRefField4ForBRU() {
		return mOrderRefField4ForBRU;
	}

	/**
	 * Sets the order ref field4 for bru.
	 *
	 * @param pOrderRefField4ForBRU            the orderRefField4ForBRU to set
	 */
	public void setOrderRefField4ForBRU(String pOrderRefField4ForBRU) {
		mOrderRefField4ForBRU = pOrderRefField4ForBRU;
	}

	/**
	 * Gets the order type map.
	 *
	 * @return the orderTypeMap
	 */
	public Map<String, String> getOrderTypeMap() {
		return mOrderTypeMap;
	}

	/**
	 * Sets the order type map.
	 *
	 * @param pOrderTypeMap            the orderTypeMap to set
	 */
	public void setOrderTypeMap(Map<String, String> pOrderTypeMap) {
		mOrderTypeMap = pOrderTypeMap;
	}

	/**
	 * This method returns the cancelReasonCode value.
	 * 
	 * @return the cancelReasonCode
	 */
	public String getCancelReasonCode() {
		return mCancelReasonCode;
	}

	/**
	 * This method sets the cancelReasonCode with parameter value
	 * pCancelReasonCode.
	 * 
	 * @param pCancelReasonCode
	 *            the cancelReasonCode to set
	 */
	public void setCancelReasonCode(String pCancelReasonCode) {
		mCancelReasonCode = pCancelReasonCode;
	}

	/**
	 * This method returns the saveOmsTransactionDetails value.
	 * 
	 * @return the saveOmsTransactionDetails
	 */
	public boolean isSaveOmsTransactionDetails() {
		return mSaveOmsTransactionDetails;
	}

	/**
	 * This method sets the saveOmsTransactionDetails with parameter value
	 * pSaveOmsTransactionDetails.
	 * 
	 * @param pSaveOmsTransactionDetails
	 *            the saveOmsTransactionDetails to set
	 */
	public void setSaveOmsTransactionDetails(boolean pSaveOmsTransactionDetails) {
		mSaveOmsTransactionDetails = pSaveOmsTransactionDetails;
	}

	/**
	 * This method returns the customerOrderUpdateTransPrefix value.
	 * 
	 * @return the customerOrderUpdateTransPrefix
	 */
	public String getCustomerOrderUpdateTransPrefix() {
		return mCustomerOrderUpdateTransPrefix;
	}

	/**
	 * This method sets the customerOrderUpdateTransPrefix with parameter value
	 * pCustomerOrderUpdateTransPrefix.
	 * 
	 * @param pCustomerOrderUpdateTransPrefix
	 *            the customerOrderUpdateTransPrefix to set
	 */
	public void setCustomerOrderUpdateTransPrefix(
			String pCustomerOrderUpdateTransPrefix) {
		mCustomerOrderUpdateTransPrefix = pCustomerOrderUpdateTransPrefix;
	}

	/**
	 * This method returns the customerOrderCancelTransPrefix value.
	 * 
	 * @return the customerOrderCancelTransPrefix
	 */
	public String getCustomerOrderCancelTransPrefix() {
		return mCustomerOrderCancelTransPrefix;
	}

	/**
	 * This method sets the customerOrderCancelTransPrefix with parameter value
	 * pCustomerOrderCancelTransPrefix.
	 * 
	 * @param pCustomerOrderCancelTransPrefix
	 *            the customerOrderCancelTransPrefix to set
	 */
	public void setCustomerOrderCancelTransPrefix(
			String pCustomerOrderCancelTransPrefix) {
		mCustomerOrderCancelTransPrefix = pCustomerOrderCancelTransPrefix;
	}

	/**
	 * This method returns the customerOrderImportTransPrefix value.
	 * 
	 * @return the customerOrderImportTransPrefix
	 */
	public String getCustomerOrderImportTransPrefix() {
		return mCustomerOrderImportTransPrefix;
	}

	/**
	 * This method sets the customerOrderImportTransPrefix with parameter value
	 * pCustomerOrderImportTransPrefix.
	 * 
	 * @param pCustomerOrderImportTransPrefix
	 *            the customerOrderImportTransPrefix to set
	 */
	public void setCustomerOrderImportTransPrefix(
			String pCustomerOrderImportTransPrefix) {
		mCustomerOrderImportTransPrefix = pCustomerOrderImportTransPrefix;
	}

	/**
	 * This method returns the customerMasterImportTransPrefix value.
	 * 
	 * @return the customerMasterImportTransPrefix
	 */
	public String getCustomerMasterImportTransPrefix() {
		return mCustomerMasterImportTransPrefix;
	}

	/**
	 * This method sets the customerMasterImportTransPrefix with parameter value
	 * pCustomerMasterImportTransPrefix.
	 * 
	 * @param pCustomerMasterImportTransPrefix
	 *            the customerMasterImportTransPrefix to set
	 */
	public void setCustomerMasterImportTransPrefix(
			String pCustomerMasterImportTransPrefix) {
		mCustomerMasterImportTransPrefix = pCustomerMasterImportTransPrefix;
	}

	/**
	 * This method returns the orderListEntityType value.
	 * 
	 * @return the orderListEntityType
	 */
	public String getOrderListEntityType() {
		return mOrderListEntityType;
	}

	/**
	 * This method sets the orderListEntityType with parameter value
	 * pOrderListEntityType.
	 *
	 * @param pOrderListEntityType            the orderListEntityType to set
	 */
	public void setOrderListEntityType(String pOrderListEntityType) {
		mOrderListEntityType = pOrderListEntityType;
	}

	/**
	 * This method returns the orderListSortDirection value.
	 *
	 * @return the orderListSortDirection
	 */
	public String getOrderListSortDirection() {
		return mOrderListSortDirection;
	}

	/**
	 * This method sets the orderListSortDirection with parameter value
	 * pOrderListSortDirection.
	 *
	 * @param pOrderListSortDirection            the orderListSortDirection to set
	 */
	public void setOrderListSortDirection(String pOrderListSortDirection) {
		mOrderListSortDirection = pOrderListSortDirection;
	}

	/**
	 * This method returns the orderListSortField value.
	 *
	 * @return the orderListSortField
	 */
	public String getOrderListSortField() {
		return mOrderListSortField;
	}

	/**
	 * This method sets the orderListSortField with parameter value
	 * pOrderListSortField.
	 *
	 * @param pOrderListSortField            the orderListSortField to set
	 */
	public void setOrderListSortField(String pOrderListSortField) {
		mOrderListSortField = pOrderListSortField;
	}

	/**
	 * This method returns the orderListNoOfRecPerPage value.
	 *
	 * @return the orderListNoOfRecPerPage
	 */
	public long getOrderListNoOfRecPerPage() {
		return mOrderListNoOfRecPerPage;
	}

	/**
	 * This method sets the orderListNoOfRecPerPage with parameter value
	 * pOrderListNoOfRecPerPage.
	 *
	 * @param pOrderListNoOfRecPerPage            the orderListNoOfRecPerPage to set
	 */
	public void setOrderListNoOfRecPerPage(long pOrderListNoOfRecPerPage) {
		mOrderListNoOfRecPerPage = pOrderListNoOfRecPerPage;
	}

	/**
	 * Gets the discount detail order.
	 *
	 * @return the discountDetailOrder
	 */
	public String getDiscountDetailOrder() {
		return mDiscountDetailOrder;
	}

	/**
	 * Sets the discount detail order.
	 *
	 * @param pDiscountDetailOrder            the discountDetailOrder to set
	 */
	public void setDiscountDetailOrder(String pDiscountDetailOrder) {
		mDiscountDetailOrder = pDiscountDetailOrder;
	}

	/**
	 * Gets the discount detail shipping.
	 *
	 * @return the discountDetailShipping
	 */
	public String getDiscountDetailShipping() {
		return mDiscountDetailShipping;
	}

	/**
	 * Sets the discount detail shipping.
	 *
	 * @param pDiscountDetailShipping            the discountDetailShipping to set
	 */
	public void setDiscountDetailShipping(String pDiscountDetailShipping) {
		mDiscountDetailShipping = pDiscountDetailShipping;
	}

	/**
	 * Gets the payment method map.
	 *
	 * @return the paymentMethodMap
	 */
	public Map<String, String> getPaymentMethodMap() {
		return mPaymentMethodMap;
	}

	/**
	 * Sets the payment method map.
	 *
	 * @param pPaymentMethodMap            the paymentMethodMap to set
	 */
	public void setPaymentMethodMap(Map<String, String> pPaymentMethodMap) {
		mPaymentMethodMap = pPaymentMethodMap;
	}

	/**
	 * Gets the charge name gift wrap.
	 *
	 * @return the chargeNameGiftWrap
	 */
	public String getChargeNameGiftWrap() {
		return mChargeNameGiftWrap;
	}

	/**
	 * Sets the charge name gift wrap.
	 *
	 * @param pChargeNameGiftWrap            the chargeNameGiftWrap to set
	 */
	public void setChargeNameGiftWrap(String pChargeNameGiftWrap) {
		mChargeNameGiftWrap = pChargeNameGiftWrap;
	}

	/**
	 * Gets the credit card method.
	 *
	 * @return the creditCardMethod
	 */
	public String getCreditCardMethod() {
		return mCreditCardMethod;
	}

	/**
	 * Sets the credit card method.
	 *
	 * @param pCreditCardMethod            the creditCardMethod to set
	 */
	public void setCreditCardMethod(String pCreditCardMethod) {
		mCreditCardMethod = pCreditCardMethod;
	}

	/**
	 * Gets the pay pal method.
	 *
	 * @return the payPalMethod
	 */
	public String getPayPalMethod() {
		return mPayPalMethod;
	}

	/**
	 * Sets the pay pal method.
	 *
	 * @param pPayPalMethod            the payPalMethod to set
	 */
	public void setPayPalMethod(String pPayPalMethod) {
		mPayPalMethod = pPayPalMethod;
	}

	/**
	 * Gets the proxy first name.
	 *
	 * @return the proxyFirstName
	 */
	public String getProxyFirstName() {
		return mProxyFirstName;
	}

	/**
	 * Sets the proxy first name.
	 *
	 * @param pProxyFirstName            the proxyFirstName to set
	 */
	public void setProxyFirstName(String pProxyFirstName) {
		mProxyFirstName = pProxyFirstName;
	}

	/**
	 * Gets the proxy last name.
	 *
	 * @return the proxyLastName
	 */
	public String getProxyLastName() {
		return mProxyLastName;
	}

	/**
	 * Sets the proxy last name.
	 *
	 * @param pProxyLastName            the proxyLastName to set
	 */
	public void setProxyLastName(String pProxyLastName) {
		mProxyLastName = pProxyLastName;
	}

	/**
	 * Gets the proxy phone.
	 *
	 * @return the proxyPhone
	 */
	public String getProxyPhone() {
		return mProxyPhone;
	}

	/**
	 * Sets the proxy phone.
	 *
	 * @param pProxyPhone            the proxyPhone to set
	 */
	public void setProxyPhone(String pProxyPhone) {
		mProxyPhone = pProxyPhone;
	}

	/**
	 * Gets the proxy email.
	 *
	 * @return the proxyEmail
	 */
	public String getProxyEmail() {
		return mProxyEmail;
	}

	/**
	 * Sets the proxy email.
	 *
	 * @param pProxyEmail            the proxyEmail to set
	 */
	public void setProxyEmail(String pProxyEmail) {
		mProxyEmail = pProxyEmail;
	}

	/**
	 * Gets the order status oh map.
	 *
	 * @return the mOrderStatusOHMap
	 */
	public Map<String, String> getOrderStatusOHMap() {
		return mOrderStatusOHMap;
	}

	/**
	 * Sets the order status oh map.
	 *
	 * @param pOrderStatusOHMap            the mOrderStatusOHMap to set
	 */
	public void setOrderStatusOHMap(Map<String, String> pOrderStatusOHMap) {
		this.mOrderStatusOHMap = pOrderStatusOHMap;
	}

	/**
	 * Gets the order status ol map.
	 *
	 * @return the mOrderStatusOLMap
	 */
	public Map<String, String> getOrderStatusOLMap() {
		return mOrderStatusOLMap;
	}

	/**
	 * Sets the order status ol map.
	 *
	 * @param pOrderStatusOLMap            the mOrderStatusOLMap to set
	 */
	public void setOrderStatusOLMap(Map<String, String> pOrderStatusOLMap) {
		this.mOrderStatusOLMap = pOrderStatusOLMap;
	}

	/**
	 * Gets the layaway order status oh map.
	 *
	 * @return the mLayawayOrderStatusOHMap
	 */
	public Map<String, String> getLayawayOrderStatusOHMap() {
		return mLayawayOrderStatusOHMap;
	}

	/**
	 * Sets the layaway order status oh map.
	 *
	 * @param pLayawayOrderStatusOHMap            the mLayawayOrderStatusOHMap to set
	 */
	public void setLayawayOrderStatusOHMap(
			Map<String, String> pLayawayOrderStatusOHMap) {
		this.mLayawayOrderStatusOHMap = pLayawayOrderStatusOHMap;
	}

	/**
	 * Gets the registry info note type.
	 *
	 * @return the registryInfoNoteType
	 */
	public String getRegistryInfoNoteType() {
		return mRegistryInfoNoteType;
	}

	/**
	 * Sets the registry info note type.
	 *
	 * @param pRegistryInfoNoteType            the registryInfoNoteType to set
	 */
	public void setRegistryInfoNoteType(String pRegistryInfoNoteType) {
		mRegistryInfoNoteType = pRegistryInfoNoteType;
	}

	/**
	 * This method returns the orderDetailsCustomerOrderJsonElement value.
	 *
	 * @return the orderDetailsCustomerOrderJsonElement
	 */
	public String getOrderDetailsCustomerOrderJsonElement() {
		return mOrderDetailsCustomerOrderJsonElement;
	}

	/**
	 * This method sets the orderDetailsCustomerOrderJsonElement with parameter
	 * value pOrderDetailsCustomerOrderJsonElement.
	 *
	 * @param pOrderDetailsCustomerOrderJsonElement            the orderDetailsCustomerOrderJsonElement to set
	 */
	public void setOrderDetailsCustomerOrderJsonElement(
			String pOrderDetailsCustomerOrderJsonElement) {
		mOrderDetailsCustomerOrderJsonElement = pOrderDetailsCustomerOrderJsonElement;
	}

	/**
	 * This method returns the orderDetailsOrderLinesJsonElement value.
	 * 
	 * @return the orderDetailsOrderLinesJsonElement
	 */
	public String getOrderDetailsOrderLinesJsonElement() {
		return mOrderDetailsOrderLinesJsonElement;
	}

	/**
	 * This method sets the orderDetailsOrderLinesJsonElement with parameter
	 * value pOrderDetailsOrderLinesJsonElement.
	 * 
	 * @param pOrderDetailsOrderLinesJsonElement
	 *            the orderDetailsOrderLinesJsonElement to set
	 */
	public void setOrderDetailsOrderLinesJsonElement(
			String pOrderDetailsOrderLinesJsonElement) {
		mOrderDetailsOrderLinesJsonElement = pOrderDetailsOrderLinesJsonElement;
	}

	/**
	 * This method returns the orderDetailsCustomerOrderLineJsonElement value.
	 *
	 * @return the orderDetailsCustomerOrderLineJsonElement
	 */
	public String getOrderDetailsCustomerOrderLineJsonElement() {
		return mOrderDetailsCustomerOrderLineJsonElement;
	}

	/**
	 * This method sets the orderDetailsCustomerOrderLineJsonElement with
	 * parameter value pOrderDetailsCustomerOrderLineJsonElement.
	 * 
	 * @param pOrderDetailsCustomerOrderLineJsonElement
	 *            the orderDetailsCustomerOrderLineJsonElement to set
	 */
	public void setOrderDetailsCustomerOrderLineJsonElement(
			String pOrderDetailsCustomerOrderLineJsonElement) {
		mOrderDetailsCustomerOrderLineJsonElement = pOrderDetailsCustomerOrderLineJsonElement;
	}

	/**
	 * This method returns the orderDetailsShipmentDetailsJsonElement value.
	 * 
	 * @return the orderDetailsShipmentDetailsJsonElement
	 */
	public String getOrderDetailsShipmentDetailsJsonElement() {
		return mOrderDetailsShipmentDetailsJsonElement;
	}

	/**
	 * This method sets the orderDetailsShipmentDetailsJsonElement with
	 * parameter value pOrderDetailsShipmentDetailsJsonElement.
	 * 
	 * @param pOrderDetailsShipmentDetailsJsonElement
	 *            the orderDetailsShipmentDetailsJsonElement to set
	 */
	public void setOrderDetailsShipmentDetailsJsonElement(
			String pOrderDetailsShipmentDetailsJsonElement) {
		mOrderDetailsShipmentDetailsJsonElement = pOrderDetailsShipmentDetailsJsonElement;
	}

	/**
	 * This method returns the orderDetailsShipmentDetailJsonElement value.
	 * 
	 * @return the orderDetailsShipmentDetailJsonElement
	 */
	public String getOrderDetailsShipmentDetailJsonElement() {
		return mOrderDetailsShipmentDetailJsonElement;
	}

	/**
	 * This method sets the orderDetailsShipmentDetailJsonElement with parameter
	 * value pOrderDetailsShipmentDetailJsonElement.
	 * 
	 * @param pOrderDetailsShipmentDetailJsonElement
	 *            the orderDetailsShipmentDetailJsonElement to set
	 */
	public void setOrderDetailsShipmentDetailJsonElement(
			String pOrderDetailsShipmentDetailJsonElement) {
		mOrderDetailsShipmentDetailJsonElement = pOrderDetailsShipmentDetailJsonElement;
	}

	/**
	 * This method returns the orderDetailsCartonsJsonElement value.
	 * 
	 * @return the orderDetailsCartonsJsonElement
	 */
	public String getOrderDetailsCartonsJsonElement() {
		return mOrderDetailsCartonsJsonElement;
	}

	/**
	 * This method sets the orderDetailsCartonsJsonElement with parameter value
	 * pOrderDetailsCartonsJsonElement.
	 * 
	 * @param pOrderDetailsCartonsJsonElement
	 *            the orderDetailsCartonsJsonElement to set
	 */
	public void setOrderDetailsCartonsJsonElement(
			String pOrderDetailsCartonsJsonElement) {
		mOrderDetailsCartonsJsonElement = pOrderDetailsCartonsJsonElement;
	}

	/**
	 * This method returns the orderDetailsCartonJsonElement value.
	 * 
	 * @return the orderDetailsCartonJsonElement
	 */
	public String getOrderDetailsCartonJsonElement() {
		return mOrderDetailsCartonJsonElement;
	}

	/**
	 * This method sets the orderDetailsCartonJsonElement with parameter value
	 * pOrderDetailsCartonJsonElement.
	 * 
	 * @param pOrderDetailsCartonJsonElement
	 *            the orderDetailsCartonJsonElement to set
	 */
	public void setOrderDetailsCartonJsonElement(
			String pOrderDetailsCartonJsonElement) {
		mOrderDetailsCartonJsonElement = pOrderDetailsCartonJsonElement;
	}

	/**
	 * This method returns the orderDetailsCartonDoNbrJsonElement value.
	 * 
	 * @return the orderDetailsCartonDoNbrJsonElement
	 */
	public String getOrderDetailsCartonDoNbrJsonElement() {
		return mOrderDetailsCartonDoNbrJsonElement;
	}

	/**
	 * This method sets the orderDetailsCartonDoNbrJsonElement with parameter
	 * value pOrderDetailsCartonDoNbrJsonElement.
	 * 
	 * @param pOrderDetailsCartonDoNbrJsonElement
	 *            the orderDetailsCartonDoNbrJsonElement to set
	 */
	public void setOrderDetailsCartonDoNbrJsonElement(
			String pOrderDetailsCartonDoNbrJsonElement) {
		mOrderDetailsCartonDoNbrJsonElement = pOrderDetailsCartonDoNbrJsonElement;
	}

	/**
	 * This method returns the orderDetailsDoDetailsJsonElement value.
	 * 
	 * @return the orderDetailsDoDetailsJsonElement
	 */
	public String getOrderDetailsDoDetailsJsonElement() {
		return mOrderDetailsDoDetailsJsonElement;
	}

	/**
	 * This method sets the orderDetailsDoDetailsJsonElement with parameter
	 * value pOrderDetailsDoDetailsJsonElement.
	 * 
	 * @param pOrderDetailsDoDetailsJsonElement
	 *            the orderDetailsDoDetailsJsonElement to set
	 */
	public void setOrderDetailsDoDetailsJsonElement(
			String pOrderDetailsDoDetailsJsonElement) {
		mOrderDetailsDoDetailsJsonElement = pOrderDetailsDoDetailsJsonElement;
	}

	/**
	 * This method returns the orderDetailsDoDetailJsonElement value.
	 * 
	 * @return the orderDetailsDoDetailJsonElement
	 */
	public String getOrderDetailsDoDetailJsonElement() {
		return mOrderDetailsDoDetailJsonElement;
	}

	/**
	 * This method sets the orderDetailsDoDetailJsonElement with parameter value
	 * pOrderDetailsDoDetailJsonElement.
	 * 
	 * @param pOrderDetailsDoDetailJsonElement
	 *            the orderDetailsDoDetailJsonElement to set
	 */
	public void setOrderDetailsDoDetailJsonElement(
			String pOrderDetailsDoDetailJsonElement) {
		mOrderDetailsDoDetailJsonElement = pOrderDetailsDoDetailJsonElement;
	}

	/**
	 * This method returns the orderDetailsdoDetailsDoNbrJsonElement value.
	 * 
	 * @return the orderDetailsdoDetailsDoNbrJsonElement
	 */
	public String getOrderDetailsdoDetailsDoNbrJsonElement() {
		return mOrderDetailsdoDetailsDoNbrJsonElement;
	}

	/**
	 * This method sets the orderDetailsdoDetailsDoNbrJsonElement with parameter
	 * value pOrderDetailsdoDetailsDoNbrJsonElement.
	 * 
	 * @param pOrderDetailsdoDetailsDoNbrJsonElement
	 *            the orderDetailsdoDetailsDoNbrJsonElement to set
	 */
	public void setOrderDetailsdoDetailsDoNbrJsonElement(
			String pOrderDetailsdoDetailsDoNbrJsonElement) {
		mOrderDetailsdoDetailsDoNbrJsonElement = pOrderDetailsdoDetailsDoNbrJsonElement;
	}

	/**
	 * This method returns the orderDetailsDoLineDetailJsonElement value.
	 * 
	 * @return the orderDetailsDoLineDetailJsonElement
	 */
	public String getOrderDetailsDoLineDetailJsonElement() {
		return mOrderDetailsDoLineDetailJsonElement;
	}

	/**
	 * This method sets the orderDetailsDoLineDetailJsonElement with parameter
	 * value pOrderDetailsDoLineDetailJsonElement.
	 * 
	 * @param pOrderDetailsDoLineDetailJsonElement
	 *            the orderDetailsDoLineDetailJsonElement to set
	 */
	public void setOrderDetailsDoLineDetailJsonElement(
			String pOrderDetailsDoLineDetailJsonElement) {
		mOrderDetailsDoLineDetailJsonElement = pOrderDetailsDoLineDetailJsonElement;
	}

	/**
	 * This method returns the orderDetailsCoLineNbrJsonElement value.
	 * 
	 * @return the orderDetailsCoLineNbrJsonElement
	 */
	public String getOrderDetailsCoLineNbrJsonElement() {
		return mOrderDetailsCoLineNbrJsonElement;
	}

	/**
	 * This method sets the orderDetailsCoLineNbrJsonElement with parameter
	 * value pOrderDetailsCoLineNbrJsonElement.
	 * 
	 * @param pOrderDetailsCoLineNbrJsonElement
	 *            the orderDetailsCoLineNbrJsonElement to set
	 */
	public void setOrderDetailsCoLineNbrJsonElement(
			String pOrderDetailsCoLineNbrJsonElement) {
		mOrderDetailsCoLineNbrJsonElement = pOrderDetailsCoLineNbrJsonElement;
	}

	/**
	 * This method returns the orderDetailsShippingInfoJsonElement value.
	 * 
	 * @return the orderDetailsShippingInfoJsonElement
	 */
	public String getOrderDetailsShippingInfoJsonElement() {
		return mOrderDetailsShippingInfoJsonElement;
	}

	/**
	 * This method sets the orderDetailsShippingInfoJsonElement with parameter
	 * value pOrderDetailsShippingInfoJsonElement.
	 * 
	 * @param pOrderDetailsShippingInfoJsonElement
	 *            the orderDetailsShippingInfoJsonElement to set
	 */
	public void setOrderDetailsShippingInfoJsonElement(
			String pOrderDetailsShippingInfoJsonElement) {
		mOrderDetailsShippingInfoJsonElement = pOrderDetailsShippingInfoJsonElement;
	}

	/**
	 * This method returns the orderDetailsOrderLineNumberJsonElement value.
	 * 
	 * @return the orderDetailsOrderLineNumberJsonElement
	 */
	public String getOrderDetailsOrderLineNumberJsonElement() {
		return mOrderDetailsOrderLineNumberJsonElement;
	}

	/**
	 * This method sets the orderDetailsOrderLineNumberJsonElement with
	 * parameter value pOrderDetailsOrderLineNumberJsonElement.
	 * 
	 * @param pOrderDetailsOrderLineNumberJsonElement
	 *            the orderDetailsOrderLineNumberJsonElement to set
	 */
	public void setOrderDetailsOrderLineNumberJsonElement(
			String pOrderDetailsOrderLineNumberJsonElement) {
		mOrderDetailsOrderLineNumberJsonElement = pOrderDetailsOrderLineNumberJsonElement;
	}

	/**
	 * This method returns the orderDetailsChargeDetailsJsonElement value.
	 * 
	 * @return the orderDetailsChargeDetailsJsonElement
	 */
	public String getOrderDetailsChargeDetailsJsonElement() {
		return mOrderDetailsChargeDetailsJsonElement;
	}

	/**
	 * This method sets the orderDetailsChargeDetailsJsonElement with parameter
	 * value pOrderDetailsChargeDetailsJsonElement.
	 * 
	 * @param pOrderDetailsChargeDetailsJsonElement
	 *            the orderDetailsChargeDetailsJsonElement to set
	 */
	public void setOrderDetailsChargeDetailsJsonElement(
			String pOrderDetailsChargeDetailsJsonElement) {
		mOrderDetailsChargeDetailsJsonElement = pOrderDetailsChargeDetailsJsonElement;
	}

	/**
	 * This method returns the orderDetailsChargeDetailJsonElement value.
	 * 
	 * @return the orderDetailsChargeDetailJsonElement
	 */
	public String getOrderDetailsChargeDetailJsonElement() {
		return mOrderDetailsChargeDetailJsonElement;
	}

	/**
	 * This method sets the orderDetailsChargeDetailJsonElement with parameter
	 * value pOrderDetailsChargeDetailJsonElement.
	 * 
	 * @param pOrderDetailsChargeDetailJsonElement
	 *            the orderDetailsChargeDetailJsonElement to set
	 */
	public void setOrderDetailsChargeDetailJsonElement(
			String pOrderDetailsChargeDetailJsonElement) {
		mOrderDetailsChargeDetailJsonElement = pOrderDetailsChargeDetailJsonElement;
	}

	/**
	 * This method returns the orderDetailsChargeCategoryJsonElement value.
	 * 
	 * @return the orderDetailsChargeCategoryJsonElement
	 */
	public String getOrderDetailsChargeCategoryJsonElement() {
		return mOrderDetailsChargeCategoryJsonElement;
	}

	/**
	 * This method sets the orderDetailsChargeCategoryJsonElement with parameter
	 * value pOrderDetailsChargeCategoryJsonElement.
	 * 
	 * @param pOrderDetailsChargeCategoryJsonElement
	 *            the orderDetailsChargeCategoryJsonElement to set
	 */
	public void setOrderDetailsChargeCategoryJsonElement(
			String pOrderDetailsChargeCategoryJsonElement) {
		mOrderDetailsChargeCategoryJsonElement = pOrderDetailsChargeCategoryJsonElement;
	}

	/**
	 * This method returns the orderDetailsChargeNameJsonElement value.
	 * 
	 * @return the orderDetailsChargeNameJsonElement
	 */
	public String getOrderDetailsChargeNameJsonElement() {
		return mOrderDetailsChargeNameJsonElement;
	}

	/**
	 * This method sets the orderDetailsChargeNameJsonElement with parameter
	 * value pOrderDetailsChargeNameJsonElement.
	 * 
	 * @param pOrderDetailsChargeNameJsonElement
	 *            the orderDetailsChargeNameJsonElement to set
	 */
	public void setOrderDetailsChargeNameJsonElement(
			String pOrderDetailsChargeNameJsonElement) {
		mOrderDetailsChargeNameJsonElement = pOrderDetailsChargeNameJsonElement;
	}

	/**
	 * This method returns the orderDetailsChargeCategoryShipping value.
	 * 
	 * @return the orderDetailsChargeCategoryShipping
	 */
	public String getOrderDetailsChargeCategoryShipping() {
		return mOrderDetailsChargeCategoryShipping;
	}

	/**
	 * This method sets the orderDetailsChargeCategoryShipping with parameter
	 * value pOrderDetailsChargeCategoryShipping.
	 * 
	 * @param pOrderDetailsChargeCategoryShipping
	 *            the orderDetailsChargeCategoryShipping to set
	 */
	public void setOrderDetailsChargeCategoryShipping(
			String pOrderDetailsChargeCategoryShipping) {
		mOrderDetailsChargeCategoryShipping = pOrderDetailsChargeCategoryShipping;
	}

	/**
	 * This method returns the orderDetailsChargeNameShipping value.
	 * 
	 * @return the orderDetailsChargeNameShipping
	 */
	public String getOrderDetailsChargeNameShipping() {
		return mOrderDetailsChargeNameShipping;
	}

	/**
	 * This method sets the orderDetailsChargeNameShipping with parameter value
	 * pOrderDetailsChargeNameShipping.
	 * 
	 * @param pOrderDetailsChargeNameShipping
	 *            the orderDetailsChargeNameShipping to set
	 */
	public void setOrderDetailsChargeNameShipping(
			String pOrderDetailsChargeNameShipping) {
		mOrderDetailsChargeNameShipping = pOrderDetailsChargeNameShipping;
	}

	/**
	 * This method returns the orderDetailsChargeNameShippingSurcharge value.
	 * 
	 * @return the orderDetailsChargeNameShippingSurcharge
	 */
	public String getOrderDetailsChargeNameShippingSurcharge() {
		return mOrderDetailsChargeNameShippingSurcharge;
	}

	/**
	 * This method sets the orderDetailsChargeNameShippingSurcharge with
	 * parameter value pOrderDetailsChargeNameShippingSurcharge.
	 * 
	 * @param pOrderDetailsChargeNameShippingSurcharge
	 *            the orderDetailsChargeNameShippingSurcharge to set
	 */
	public void setOrderDetailsChargeNameShippingSurcharge(
			String pOrderDetailsChargeNameShippingSurcharge) {
		mOrderDetailsChargeNameShippingSurcharge = pOrderDetailsChargeNameShippingSurcharge;
	}

	/**
	 * This method returns the orderDetailsChargeCategoryVS value.
	 * 
	 * @return the orderDetailsChargeCategoryVS
	 */
	public String getOrderDetailsChargeCategoryVS() {
		return mOrderDetailsChargeCategoryVS;
	}

	/**
	 * This method sets the orderDetailsChargeCategoryVS with parameter value
	 * pOrderDetailsChargeCategoryVS.
	 * 
	 * @param pOrderDetailsChargeCategoryVS
	 *            the orderDetailsChargeCategoryVS to set
	 */
	public void setOrderDetailsChargeCategoryVS(
			String pOrderDetailsChargeCategoryVS) {
		mOrderDetailsChargeCategoryVS = pOrderDetailsChargeCategoryVS;
	}

	/**
	 * This method returns the orderDetailsChargeCategoryMisc value.
	 * 
	 * @return the orderDetailsChargeCategoryMisc
	 */
	public String getOrderDetailsChargeCategoryMisc() {
		return mOrderDetailsChargeCategoryMisc;
	}

	/**
	 * This method sets the orderDetailsChargeCategoryMisc with parameter value
	 * pOrderDetailsChargeCategoryMisc.
	 * 
	 * @param pOrderDetailsChargeCategoryMisc
	 *            the orderDetailsChargeCategoryMisc to set
	 */
	public void setOrderDetailsChargeCategoryMisc(
			String pOrderDetailsChargeCategoryMisc) {
		mOrderDetailsChargeCategoryMisc = pOrderDetailsChargeCategoryMisc;
	}

	/**
	 * This method returns the orderDetailsChargeNameGiftwrap value.
	 * 
	 * @return the orderDetailsChargeNameGiftwrap
	 */
	public String getOrderDetailsChargeNameGiftwrap() {
		return mOrderDetailsChargeNameGiftwrap;
	}

	/**
	 * This method sets the orderDetailsChargeNameGiftwrap with parameter value
	 * pOrderDetailsChargeNameGiftwrap.
	 * 
	 * @param pOrderDetailsChargeNameGiftwrap
	 *            the orderDetailsChargeNameGiftwrap to set
	 */
	public void setOrderDetailsChargeNameGiftwrap(
			String pOrderDetailsChargeNameGiftwrap) {
		mOrderDetailsChargeNameGiftwrap = pOrderDetailsChargeNameGiftwrap;
	}

	/**
	 * This method returns the orderDetailsChargeNameEwasteFee value.
	 * 
	 * @return the orderDetailsChargeNameEwasteFee
	 */
	public String getOrderDetailsChargeNameEwasteFee() {
		return mOrderDetailsChargeNameEwasteFee;
	}

	/**
	 * This method sets the orderDetailsChargeNameEwasteFee with parameter value
	 * pOrderDetailsChargeNameEwasteFee.
	 * 
	 * @param pOrderDetailsChargeNameEwasteFee
	 *            the orderDetailsChargeNameEwasteFee to set
	 */
	public void setOrderDetailsChargeNameEwasteFee(
			String pOrderDetailsChargeNameEwasteFee) {
		mOrderDetailsChargeNameEwasteFee = pOrderDetailsChargeNameEwasteFee;
	}

	/**
	 * This method returns the orderDetailsChargeAmountJsonElement value.
	 * 
	 * @return the orderDetailsChargeAmountJsonElement
	 */
	public String getOrderDetailsChargeAmountJsonElement() {
		return mOrderDetailsChargeAmountJsonElement;
	}

	/**
	 * This method sets the orderDetailsChargeAmountJsonElement with parameter
	 * value pOrderDetailsChargeAmountJsonElement.
	 * 
	 * @param pOrderDetailsChargeAmountJsonElement
	 *            the orderDetailsChargeAmountJsonElement to set
	 */
	public void setOrderDetailsChargeAmountJsonElement(
			String pOrderDetailsChargeAmountJsonElement) {
		mOrderDetailsChargeAmountJsonElement = pOrderDetailsChargeAmountJsonElement;
	}

	/**
	 * This method returns the orderDetailsSurchargeJsonElement value.
	 * 
	 * @return the orderDetailsSurchargeJsonElement
	 */
	public String getOrderDetailsSurchargeJsonElement() {
		return mOrderDetailsSurchargeJsonElement;
	}

	/**
	 * This method sets the orderDetailsSurchargeJsonElement with parameter
	 * value pOrderDetailsSurchargeJsonElement.
	 * 
	 * @param pOrderDetailsSurchargeJsonElement
	 *            the orderDetailsSurchargeJsonElement to set
	 */
	public void setOrderDetailsSurchargeJsonElement(
			String pOrderDetailsSurchargeJsonElement) {
		mOrderDetailsSurchargeJsonElement = pOrderDetailsSurchargeJsonElement;
	}

	/**
	 * This method returns the orderDetailsOrderTotalsJsonElement value.
	 * 
	 * @return the orderDetailsOrderTotalsJsonElement
	 */
	public String getOrderDetailsOrderTotalsJsonElement() {
		return mOrderDetailsOrderTotalsJsonElement;
	}

	/**
	 * This method sets the orderDetailsOrderTotalsJsonElement with parameter
	 * value pOrderDetailsOrderTotalsJsonElement.
	 * 
	 * @param pOrderDetailsOrderTotalsJsonElement
	 *            the orderDetailsOrderTotalsJsonElement to set
	 */
	public void setOrderDetailsOrderTotalsJsonElement(
			String pOrderDetailsOrderTotalsJsonElement) {
		mOrderDetailsOrderTotalsJsonElement = pOrderDetailsOrderTotalsJsonElement;
	}

	/**
	 * This method returns the orderDetailsCustomerOrderTotalDetailJsonElement
	 * value.
	 * 
	 * @return the orderDetailsCustomerOrderTotalDetailJsonElement
	 */
	public String getOrderDetailsCustomerOrderTotalDetailJsonElement() {
		return mOrderDetailsCustomerOrderTotalDetailJsonElement;
	}

	/**
	 * This method sets the orderDetailsCustomerOrderTotalDetailJsonElement with
	 * parameter value pOrderDetailsCustomerOrderTotalDetailJsonElement.
	 * 
	 * @param pOrderDetailsCustomerOrderTotalDetailJsonElement
	 *            the orderDetailsCustomerOrderTotalDetailJsonElement to set
	 */
	public void setOrderDetailsCustomerOrderTotalDetailJsonElement(
			String pOrderDetailsCustomerOrderTotalDetailJsonElement) {
		mOrderDetailsCustomerOrderTotalDetailJsonElement = pOrderDetailsCustomerOrderTotalDetailJsonElement;
	}

	/**
	 * This method returns the orderDetailsShippingJsonElement value.
	 * 
	 * @return the orderDetailsShippingJsonElement
	 */
	public String getOrderDetailsShippingJsonElement() {
		return mOrderDetailsShippingJsonElement;
	}

	/**
	 * This method sets the orderDetailsShippingJsonElement with parameter value
	 * pOrderDetailsShippingJsonElement.
	 * 
	 * @param pOrderDetailsShippingJsonElement
	 *            the orderDetailsShippingJsonElement to set
	 */
	public void setOrderDetailsShippingJsonElement(
			String pOrderDetailsShippingJsonElement) {
		mOrderDetailsShippingJsonElement = pOrderDetailsShippingJsonElement;
	}

	/**
	 * This method returns the orderDetailsGiftwrapJsonElement value.
	 * 
	 * @return the orderDetailsGiftwrapJsonElement
	 */
	public String getOrderDetailsGiftwrapJsonElement() {
		return mOrderDetailsGiftwrapJsonElement;
	}

	/**
	 * This method sets the orderDetailsGiftwrapJsonElement with parameter value
	 * pOrderDetailsGiftwrapJsonElement.
	 * 
	 * @param pOrderDetailsGiftwrapJsonElement
	 *            the orderDetailsGiftwrapJsonElement to set
	 */
	public void setOrderDetailsGiftwrapJsonElement(
			String pOrderDetailsGiftwrapJsonElement) {
		mOrderDetailsGiftwrapJsonElement = pOrderDetailsGiftwrapJsonElement;
	}

	/**
	 * This method returns the orderDetailsEwasteJsonElement value.
	 * 
	 * @return the orderDetailsEwasteJsonElement
	 */
	public String getOrderDetailsEwasteJsonElement() {
		return mOrderDetailsEwasteJsonElement;
	}

	/**
	 * This method sets the orderDetailsEwasteJsonElement with parameter value
	 * pOrderDetailsEwasteJsonElement.
	 * 
	 * @param pOrderDetailsEwasteJsonElement
	 *            the orderDetailsEwasteJsonElement to set
	 */
	public void setOrderDetailsEwasteJsonElement(
			String pOrderDetailsEwasteJsonElement) {
		mOrderDetailsEwasteJsonElement = pOrderDetailsEwasteJsonElement;
	}

	/**
	 * Gets the customer transaction list response json element.
	 *
	 * @return the mCustomerTransactionListResponseJsonElement
	 */
	public String getCustomerTransactionListResponseJsonElement() {
		return mCustomerTransactionListResponseJsonElement;
	}

	/**
	 * Sets the customer transaction list response json element.
	 *
	 * @param pCustomerTransactionListResponseJsonElement            the mCustomerTransactionListResponseJsonElement to set
	 */
	public void setCustomerTransactionListResponseJsonElement(
			String pCustomerTransactionListResponseJsonElement) {
		this.mCustomerTransactionListResponseJsonElement = pCustomerTransactionListResponseJsonElement;
	}

	/**
	 * Gets the customer orders json element.
	 *
	 * @return the mCustomerOrdersJsonElement
	 */
	public String getCustomerOrdersJsonElement() {
		return mCustomerOrdersJsonElement;
	}

	/**
	 * Sets the customer orders json element.
	 *
	 * @param pCustomerOrdersJsonElement            the mCustomerOrdersJsonElement to set
	 */
	public void setCustomerOrdersJsonElement(String pCustomerOrdersJsonElement) {
		this.mCustomerOrdersJsonElement = pCustomerOrdersJsonElement;
	}

	/**
	 * Gets the customer order json element.
	 *
	 * @return the mCustomerOrderJsonElement
	 */
	public String getCustomerOrderJsonElement() {
		return mCustomerOrderJsonElement;
	}

	/**
	 * Sets the customer order json element.
	 *
	 * @param pCustomerOrderJsonElement            the mCustomerOrderJsonElement to set
	 */
	public void setCustomerOrderJsonElement(String pCustomerOrderJsonElement) {
		this.mCustomerOrderJsonElement = pCustomerOrderJsonElement;
	}

	/**
	 * Gets the order status json element.
	 *
	 * @return the mOrderStatusJsonElement
	 */
	public String getOrderStatusJsonElement() {
		return mOrderStatusJsonElement;
	}

	/**
	 * Sets the order status json element.
	 *
	 * @param pOrderStatusJsonElement            the mOrderStatusJsonElement to set
	 */
	public void setOrderStatusJsonElement(String pOrderStatusJsonElement) {
		this.mOrderStatusJsonElement = pOrderStatusJsonElement;
	}

	/**
	 * Gets the tax request type.
	 *
	 * @return the taxRequestType
	 */
	public String getTaxRequestType() {
		return mTaxRequestType;
	}

	/**
	 * Sets the tax request type.
	 *
	 * @param pTaxRequestType            the taxRequestType to set
	 */
	public void setTaxRequestType(String pTaxRequestType) {
		mTaxRequestType = pTaxRequestType;
	}

	/**
	 * Gets the tax category.
	 *
	 * @return the taxCategory
	 */
	public String getTaxCategory() {
		return mTaxCategory;
	}

	/**
	 * Sets the tax category.
	 *
	 * @param pTaxCategory            the taxCategory to set
	 */
	public void setTaxCategory(String pTaxCategory) {
		mTaxCategory = pTaxCategory;
	}

	/**
	 * Gets the tax state location name.
	 *
	 * @return the taxStateLocationName
	 */
	public String getTaxStateLocationName() {
		return mTaxStateLocationName;
	}

	/**
	 * Sets the tax state location name.
	 *
	 * @param pTaxStateLocationName            the taxStateLocationName to set
	 */
	public void setTaxStateLocationName(String pTaxStateLocationName) {
		mTaxStateLocationName = pTaxStateLocationName;
	}

	/**
	 * Gets the tax city location name.
	 *
	 * @return the taxCityLocationName
	 */
	public String getTaxCityLocationName() {
		return mTaxCityLocationName;
	}

	/**
	 * Sets the tax city location name.
	 *
	 * @param pTaxCityLocationName            the taxCityLocationName to set
	 */
	public void setTaxCityLocationName(String pTaxCityLocationName) {
		mTaxCityLocationName = pTaxCityLocationName;
	}

	/**
	 * Gets the tax county location name.
	 *
	 * @return the taxCountyLocationName
	 */
	public String getTaxCountyLocationName() {
		return mTaxCountyLocationName;
	}

	/**
	 * Sets the tax county location name.
	 *
	 * @param pTaxCountyLocationName            the taxCountyLocationName to set
	 */
	public void setTaxCountyLocationName(String pTaxCountyLocationName) {
		mTaxCountyLocationName = pTaxCountyLocationName;
	}

	/**
	 * Gets the tax sec state location name.
	 *
	 * @return the taxSecStateLocationName
	 */
	public String getTaxSecStateLocationName() {
		return mTaxSecStateLocationName;
	}

	/**
	 * Sets the tax sec state location name.
	 *
	 * @param pTaxSecStateLocationName            the taxSecStateLocationName to set
	 */
	public void setTaxSecStateLocationName(String pTaxSecStateLocationName) {
		mTaxSecStateLocationName = pTaxSecStateLocationName;
	}

	/**
	 * Gets the tax sec city location name.
	 *
	 * @return the taxSecCityLocationName
	 */
	public String getTaxSecCityLocationName() {
		return mTaxSecCityLocationName;
	}

	/**
	 * Sets the tax sec city location name.
	 *
	 * @param pTaxSecCityLocationName            the taxSecCityLocationName to set
	 */
	public void setTaxSecCityLocationName(String pTaxSecCityLocationName) {
		mTaxSecCityLocationName = pTaxSecCityLocationName;
	}

	/**
	 * Gets the tax sec county location name.
	 *
	 * @return the taxSecCountyLocationName
	 */
	public String getTaxSecCountyLocationName() {
		return mTaxSecCountyLocationName;
	}

	/**
	 * Sets the tax sec county location name.
	 *
	 * @param pTaxSecCountyLocationName            the taxSecCountyLocationName to set
	 */
	public void setTaxSecCountyLocationName(String pTaxSecCountyLocationName) {
		mTaxSecCountyLocationName = pTaxSecCountyLocationName;
	}

	/**
	 * Gets the order details item count json element.
	 *
	 * @return the mOrderDetailsItemCountJsonElement
	 */
	public String getOrderDetailsItemCountJsonElement() {
		return mOrderDetailsItemCountJsonElement;
	}

	/**
	 * Sets the order details item count json element.
	 *
	 * @param pOrderDetailsItemCountJsonElement            the mOrderDetailsItemCountJsonElement to set
	 */
	public void setOrderDetailsItemCountJsonElement(
			String pOrderDetailsItemCountJsonElement) {
		this.mOrderDetailsItemCountJsonElement = pOrderDetailsItemCountJsonElement;
	}

	/**
	 * This method returns the orderResponseCustomerBasicInfoJsonElement value.
	 * 
	 * @return the orderResponseCustomerBasicInfoJsonElement
	 */
	public String getOrderResponseCustomerBasicInfoJsonElement() {
		return mOrderResponseCustomerBasicInfoJsonElement;
	}

	/**
	 * This method sets the orderResponseCustomerBasicInfoJsonElement with
	 * parameter value pOrderResponseCustomerBasicInfoJsonElement.
	 * 
	 * @param pOrderResponseCustomerBasicInfoJsonElement
	 *            the orderResponseCustomerBasicInfoJsonElement to set
	 */
	public void setOrderResponseCustomerBasicInfoJsonElement(
			String pOrderResponseCustomerBasicInfoJsonElement) {
		mOrderResponseCustomerBasicInfoJsonElement = pOrderResponseCustomerBasicInfoJsonElement;
	}

	/**
	 * This method returns the orderResponseMessagesJsonElement value.
	 * 
	 * @return the orderResponseMessagesJsonElement
	 */
	public String getOrderResponseMessagesJsonElement() {
		return mOrderResponseMessagesJsonElement;
	}

	/**
	 * This method sets the orderResponseMessagesJsonElement with parameter
	 * value pOrderResponseMessagesJsonElement.
	 * 
	 * @param pOrderResponseMessagesJsonElement
	 *            the orderResponseMessagesJsonElement to set
	 */
	public void setOrderResponseMessagesJsonElement(
			String pOrderResponseMessagesJsonElement) {
		mOrderResponseMessagesJsonElement = pOrderResponseMessagesJsonElement;
	}

	/**
	 * This method returns the orderResponseMessageJsonElement value.
	 * 
	 * @return the orderResponseMessageJsonElement
	 */
	public String getOrderResponseMessageJsonElement() {
		return mOrderResponseMessageJsonElement;
	}

	/**
	 * This method sets the orderResponseMessageJsonElement with parameter value
	 * pOrderResponseMessageJsonElement.
	 * 
	 * @param pOrderResponseMessageJsonElement
	 *            the orderResponseMessageJsonElement to set
	 */
	public void setOrderResponseMessageJsonElement(
			String pOrderResponseMessageJsonElement) {
		mOrderResponseMessageJsonElement = pOrderResponseMessageJsonElement;
	}

	/**
	 * This method returns the orderResponseSeverityJsonElement value.
	 * 
	 * @return the orderResponseSeverityJsonElement
	 */
	public String getOrderResponseSeverityJsonElement() {
		return mOrderResponseSeverityJsonElement;
	}

	/**
	 * This method sets the orderResponseSeverityJsonElement with parameter
	 * value pOrderResponseSeverityJsonElement.
	 * 
	 * @param pOrderResponseSeverityJsonElement
	 *            the orderResponseSeverityJsonElement to set
	 */
	public void setOrderResponseSeverityJsonElement(
			String pOrderResponseSeverityJsonElement) {
		mOrderResponseSeverityJsonElement = pOrderResponseSeverityJsonElement;
	}

	/**
	 * This method returns the orderResponseDescriptionJsonElement value.
	 * 
	 * @return the orderResponseDescriptionJsonElement
	 */
	public String getOrderResponseDescriptionJsonElement() {
		return mOrderResponseDescriptionJsonElement;
	}

	/**
	 * This method sets the orderResponseDescriptionJsonElement with parameter
	 * value pOrderResponseDescriptionJsonElement.
	 * 
	 * @param pOrderResponseDescriptionJsonElement
	 *            the orderResponseDescriptionJsonElement to set
	 */
	public void setOrderResponseDescriptionJsonElement(
			String pOrderResponseDescriptionJsonElement) {
		mOrderResponseDescriptionJsonElement = pOrderResponseDescriptionJsonElement;
	}

	/**
	 * Gets the order details order created date json element.
	 *
	 * @return the mOrderDetailsOrderCreatedDateJsonElement
	 */
	public String getOrderDetailsOrderCreatedDateJsonElement() {
		return mOrderDetailsOrderCreatedDateJsonElement;
	}

	/**
	 * Sets the order details order created date json element.
	 *
	 * @param pOrderDetailsOrderCreatedDateJsonElement            the mOrderDetailsOrderCreatedDateJsonElement to set
	 */
	public void setOrderDetailsOrderCreatedDateJsonElement(
			String pOrderDetailsOrderCreatedDateJsonElement) {
		this.mOrderDetailsOrderCreatedDateJsonElement = pOrderDetailsOrderCreatedDateJsonElement;
	}

	/**
	 * Gets the online pid.
	 * 
	 * @return the online pid
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * Sets the online pid.
	 * 
	 * @param pOnlinePID
	 *            the new online pid
	 */
	public void setOnlinePID(String pOnlinePID) {
		this.mOnlinePID = pOnlinePID;
	}

	/**
	 * Gets the order details customer order notes json element.
	 *
	 * @return the mOrderDetailsCustomerOrderNotesJsonElement
	 */
	public String getOrderDetailsCustomerOrderNotesJsonElement() {
		return mOrderDetailsCustomerOrderNotesJsonElement;
	}

	/**
	 * Sets the order details customer order notes json element.
	 *
	 * @param pOrderDetailsCustomerOrderNotesJsonElement            the mOrderDetailsCustomerOrderNotesJsonElement to set
	 */
	public void setOrderDetailsCustomerOrderNotesJsonElement(
			String pOrderDetailsCustomerOrderNotesJsonElement) {
		this.mOrderDetailsCustomerOrderNotesJsonElement = pOrderDetailsCustomerOrderNotesJsonElement;
	}

	/**
	 * Gets the order details customer order note json element.
	 *
	 * @return the mOrderDetailsCustomerOrderNotJsonElement
	 */
	public String getOrderDetailsCustomerOrderNoteJsonElement() {
		return mOrderDetailsCustomerOrderNoteJsonElement;
	}

	/**
	 * Sets the order details customer order note json element.
	 *
	 * @param pOrderDetailsCustomerOrderNoteJsonElement            the mOrderDetailsCustomerOrderNoteJsonElement to set
	 */
	public void setOrderDetailsCustomerOrderNoteJsonElement(
			String pOrderDetailsCustomerOrderNoteJsonElement) {
		this.mOrderDetailsCustomerOrderNoteJsonElement = pOrderDetailsCustomerOrderNoteJsonElement;
	}

	/**
	 * Gets the order details item id json element.
	 *
	 * @return the mOrderDetailsItemIdJsonElement
	 */
	public String getOrderDetailsItemIdJsonElement() {
		return mOrderDetailsItemIdJsonElement;
	}

	/**
	 * Sets the order details item id json element.
	 *
	 * @param pOrderDetailsItemIdJsonElement            the mOrderDetailsItemIdJsonElement to set
	 */
	public void setOrderDetailsItemIdJsonElement(
			String pOrderDetailsItemIdJsonElement) {
		this.mOrderDetailsItemIdJsonElement = pOrderDetailsItemIdJsonElement;
	}

	/**
	 * Gets the order details product page url json element.
	 *
	 * @return the mOrderDetailsProductPageUrlJsonElement
	 */
	public String getOrderDetailsProductPageUrlJsonElement() {
		return mOrderDetailsProductPageUrlJsonElement;
	}

	/**
	 * Sets the order details product page url json element.
	 *
	 * @param pOrderDetailsProductPageUrlJsonElement            the mOrderDetailsProductPageUrlJsonElement to set
	 */
	public void setOrderDetailsProductPageUrlJsonElement(
			String pOrderDetailsProductPageUrlJsonElement) {
		this.mOrderDetailsProductPageUrlJsonElement = pOrderDetailsProductPageUrlJsonElement;
	}

	/**
	 * Gets the order details note type json element.
	 *
	 * @return the mOrderDetailsNoteTypeJsonElement
	 */
	public String getOrderDetailsNoteTypeJsonElement() {
		return mOrderDetailsNoteTypeJsonElement;
	}

	/**
	 * Sets the order details note type json element.
	 *
	 * @param pOrderDetailsNoteTypeJsonElement            the mOrderDetailsNoteTypeJsonElement to set
	 */
	public void setOrderDetailsNoteTypeJsonElement(
			String pOrderDetailsNoteTypeJsonElement) {
		this.mOrderDetailsNoteTypeJsonElement = pOrderDetailsNoteTypeJsonElement;
	}

	/**
	 * Gets the order details note text json element.
	 *
	 * @return the mOrderDetailsNoteTextJsonElement
	 */
	public String getOrderDetailsNoteTextJsonElement() {
		return mOrderDetailsNoteTextJsonElement;
	}

	/**
	 * Sets the order details note text json element.
	 *
	 * @param pOrderDetailsNoteTextJsonElement            the mOrderDetailsNoteTextJsonElement to set
	 */
	public void setOrderDetailsNoteTextJsonElement(
			String pOrderDetailsNoteTextJsonElement) {
		this.mOrderDetailsNoteTextJsonElement = pOrderDetailsNoteTextJsonElement;
	}

	/**
	 * Gets the order details transaction date json element.
	 *
	 * @return the mOrderDetailsTransactionDateJsonElement
	 */
	public String getOrderDetailsTransactionDateJsonElement() {
		return mOrderDetailsTransactionDateJsonElement;
	}

	/**
	 * Sets the order details transaction date json element.
	 *
	 * @param pOrderDetailsTransactionDateJsonElement            the mOrderDetailsTransactionDateJsonElement to set
	 */
	public void setOrderDetailsTransactionDateJsonElement(
			String pOrderDetailsTransactionDateJsonElement) {
		this.mOrderDetailsTransactionDateJsonElement = pOrderDetailsTransactionDateJsonElement;
	}

	/**
	 * Gets the order details transaction total amount element.
	 *
	 * @return the mOrderDetailsTransactionTotalAmountElement
	 */
	public String getOrderDetailsTransactionTotalAmountElement() {
		return mOrderDetailsTransactionTotalAmountElement;
	}

	/**
	 * Sets the order details transaction total amount element.
	 *
	 * @param pOrderDetailsTransactionTotalAmountElement            the mOrderDetailsTransactionTotalAmountElement to set
	 */
	public void setOrderDetailsTransactionTotalAmountElement(
			String pOrderDetailsTransactionTotalAmountElement) {
		this.mOrderDetailsTransactionTotalAmountElement = pOrderDetailsTransactionTotalAmountElement;
	}

	/**
	 * Gets the order details next payment amount json element.
	 *
	 * @return the mOrderDetailsNextPaymentAmountJsonElement
	 */
	public String getOrderDetailsNextPaymentAmountJsonElement() {
		return mOrderDetailsNextPaymentAmountJsonElement;
	}

	/**
	 * Sets the order details next payment amount json element.
	 *
	 * @param pOrderDetailsNextPaymentAmountJsonElement            the mOrderDetailsNextPaymentAmountJsonElement to set
	 */
	public void setOrderDetailsNextPaymentAmountJsonElement(
			String pOrderDetailsNextPaymentAmountJsonElement) {
		this.mOrderDetailsNextPaymentAmountJsonElement = pOrderDetailsNextPaymentAmountJsonElement;
	}

	/**
	 * Gets the order details remaining amount json element.
	 *
	 * @return the mOrderDetailsRemainingAmountJsonElement
	 */
	public String getOrderDetailsRemainingAmountJsonElement() {
		return mOrderDetailsRemainingAmountJsonElement;
	}

	/**
	 * Sets the order details remaining amount json element.
	 *
	 * @param pOrderDetailsRemainingAmountJsonElement            the mOrderDetailsRemainingAmountJsonElement to set
	 */
	public void setOrderDetailsRemainingAmountJsonElement(
			String pOrderDetailsRemainingAmountJsonElement) {
		this.mOrderDetailsRemainingAmountJsonElement = pOrderDetailsRemainingAmountJsonElement;
	}

	/**
	 * Gets the order details next payment due json element.
	 *
	 * @return the mOrderDetailsNextPaymentDueJsonElement
	 */
	public String getOrderDetailsNextPaymentDueJsonElement() {
		return mOrderDetailsNextPaymentDueJsonElement;
	}

	/**
	 * Sets the order details next payment due json element.
	 *
	 * @param pOrderDetailsNextPaymentDueJsonElement            the mOrderDetailsNextPaymentDueJsonElement to set
	 */
	public void setOrderDetailsNextPaymentDueJsonElement(
			String pOrderDetailsNextPaymentDueJsonElement) {
		this.mOrderDetailsNextPaymentDueJsonElement = pOrderDetailsNextPaymentDueJsonElement;
	}

	/**
	 * Gets the order details is tranction available json element.
	 *
	 * @return the mOrderDetailsIsTranctionAvailableJsonElement
	 */
	public String getOrderDetailsIsTranctionAvailableJsonElement() {
		return mOrderDetailsIsTranctionAvailableJsonElement;
	}

	/**
	 * Sets the order details is tranction available json element.
	 *
	 * @param pOrderDetailsIsTranctionAvailableJsonElement the mOrderDetailsIsTranctionAvailableJsonElement to set
	 */
	public void setOrderDetailsIsTranctionAvailableJsonElement(
			String pOrderDetailsIsTranctionAvailableJsonElement) {
		this.mOrderDetailsIsTranctionAvailableJsonElement = pOrderDetailsIsTranctionAvailableJsonElement;
	}

	/**
	 * Gets the order details is remaining amount json element.
	 *
	 * @return the mOrderDetailsIsRemainingAmountJsonElement
	 */
	public String getOrderDetailsIsRemainingAmountJsonElement() {
		return mOrderDetailsIsRemainingAmountJsonElement;
	}

	/**
	 * Sets the order details is remaining amount json element.
	 *
	 * @param pOrderDetailsIsRemainingAmountJsonElement the mOrderDetailsIsRemainingAmountJsonElement to set
	 */
	public void setOrderDetailsIsRemainingAmountJsonElement(
			String pOrderDetailsIsRemainingAmountJsonElement) {
		this.mOrderDetailsIsRemainingAmountJsonElement = pOrderDetailsIsRemainingAmountJsonElement;
	}

	/**
	 * Gets the order details site name for gw json element.
	 *
	 * @return the mOrderDetailsSiteNameForGWJsonElement
	 */
	public String getOrderDetailsSiteNameForGWJsonElement() {
		return mOrderDetailsSiteNameForGWJsonElement;
	}

	/**
	 * Sets the order details site name for gw json element.
	 *
	 * @param pOrderDetailsSiteNameForGWJsonElement the mOrderDetailsSiteNameForGWJsonElement to set
	 */
	public void setOrderDetailsSiteNameForGWJsonElement(
			String pOrderDetailsSiteNameForGWJsonElement) {
		this.mOrderDetailsSiteNameForGWJsonElement = pOrderDetailsSiteNameForGWJsonElement;
	}

	/**
	 * Gets the order details vas json element.
	 *
	 * @return the mOrderDetailsVasJsonElement
	 */
	public String getOrderDetailsVasJsonElement() {
		return mOrderDetailsVasJsonElement;
	}

	/**
	 * Sets the order details vas json element.
	 *
	 * @param pOrderDetailsVasJsonElement the mOrderDetailsVasJsonElement to set
	 */
	public void setOrderDetailsVasJsonElement(String pOrderDetailsVasJsonElement) {
		this.mOrderDetailsVasJsonElement = pOrderDetailsVasJsonElement;
	}

	/**
	 * Gets the order details bpp hypen json element.
	 *
	 * @return the mOrderDetailsBppHypenJsonElement
	 */
	public String getOrderDetailsBppHypenJsonElement() {
		return mOrderDetailsBppHypenJsonElement;
	}

	/**
	 * Sets the order details bpp hypen json element.
	 *
	 * @param pOrderDetailsBppHypenJsonElement the mOrderDetailsBppHypenJsonElement to set
	 */
	public void setOrderDetailsBppHypenJsonElement(
			String pOrderDetailsBppHypenJsonElement) {
		this.mOrderDetailsBppHypenJsonElement = pOrderDetailsBppHypenJsonElement;
	}

	/**
	 * Gets the order details bpp term json element.
	 *
	 * @return the mOrderDetailsBppTermJsonElement
	 */
	public String getOrderDetailsBppTermJsonElement() {
		return mOrderDetailsBppTermJsonElement;
	}

	/**
	 * Sets the order details bpp term json element.
	 *
	 * @param pOrderDetailsBppTermJsonElement the mOrderDetailsBppTermJsonElement to set
	 */
	public void setOrderDetailsBppTermJsonElement(
			String pOrderDetailsBppTermJsonElement) {
		this.mOrderDetailsBppTermJsonElement = pOrderDetailsBppTermJsonElement;
	}

	/**
	 * Gets the order details bpp charge amount json element.
	 *
	 * @return the mOrderDetailsBppChargeAmountJsonElement
	 */
	public String getOrderDetailsBppChargeAmountJsonElement() {
		return mOrderDetailsBppChargeAmountJsonElement;
	}

	/**
	 * Sets the order details bpp charge amount json element.
	 *
	 * @param pOrderDetailsBppChargeAmountJsonElement the mOrderDetailsBppChargeAmountJsonElement to set
	 */
	public void setOrderDetailsBppChargeAmountJsonElement(
			String pOrderDetailsBppChargeAmountJsonElement) {
		this.mOrderDetailsBppChargeAmountJsonElement = pOrderDetailsBppChargeAmountJsonElement;
	}

	/**
	 * Gets the order details term property name.
	 *
	 * @return the mOrderDetailsTermPropertyName
	 */
	public String getOrderDetailsTermPropertyName() {
		return mOrderDetailsTermPropertyName;
	}

	/**
	 * Sets the order details term property name.
	 *
	 * @param pOrderDetailsTermPropertyName the mOrderDetailsTermPropertyName to set
	 */
	public void setOrderDetailsTermPropertyName(
			String pOrderDetailsTermPropertyName) {
		this.mOrderDetailsTermPropertyName = pOrderDetailsTermPropertyName;
	}

	/**
	 * Gets the order details display name property name.
	 *
	 * @return the mOrderDetailsDisplayNamePropertyName
	 */
	public String getOrderDetailsDisplayNamePropertyName() {
		return mOrderDetailsDisplayNamePropertyName;
	}

	/**
	 * Sets the order details display name property name.
	 *
	 * @param pOrderDetailsDisplayNamePropertyName the mOrderDetailsDisplayNamePropertyName to set
	 */
	public void setOrderDetailsDisplayNamePropertyName(
			String pOrderDetailsDisplayNamePropertyName) {
		this.mOrderDetailsDisplayNamePropertyName = pOrderDetailsDisplayNamePropertyName;
	}
	
	/** Holds mCreditCardTypeMap. */
	private Map<String, String> mCreditCardTypeMap;

	/**
	 * Gets the credit card type map.
	 *
	 * @return the creditCardTypeMap
	 */
	public Map<String, String> getCreditCardTypeMap() {
		return mCreditCardTypeMap;
	}

	/**
	 * Sets the credit card type map.
	 *
	 * @param pCreditCardTypeMap the creditCardTypeMap to set
	 */
	public void setCreditCardTypeMap(Map<String, String> pCreditCardTypeMap) {
		mCreditCardTypeMap = pCreditCardTypeMap;
	}
	
	/** Holds mCountryCodeMap. */
	private Map<String, String> mCountryCodeMap;

	/**
	 * Gets the country code map.
	 *
	 * @return the countryCodeMap
	 */
	public Map<String, String> getCountryCodeMap() {
		return mCountryCodeMap;
	}

	/**
	 * Sets the country code map.
	 *
	 * @param pCountryCodeMap the countryCodeMap to set
	 */
	public void setCountryCodeMap(Map<String, String> pCountryCodeMap) {
		mCountryCodeMap = pCountryCodeMap;
	}

	/**
	 * Gets the order details payment details json element.
	 *
	 * @return the mOrderDetailsPaymentDetailsJsonElement
	 */
	public String getOrderDetailsPaymentDetailsJsonElement() {
		return mOrderDetailsPaymentDetailsJsonElement;
	}

	/**
	 * Sets the order details payment details json element.
	 *
	 * @param pOrderDetailsPaymentDetailsJsonElement the mOrderDetailsPaymentDetailsJsonElement to set
	 */
	public void setOrderDetailsPaymentDetailsJsonElement(
			String pOrderDetailsPaymentDetailsJsonElement) {
		this.mOrderDetailsPaymentDetailsJsonElement = pOrderDetailsPaymentDetailsJsonElement;
	}

	/**
	 * Gets the order details payment detail json element.
	 *
	 * @return the mOrderDetailsPaymentDetailJsonElement
	 */
	public String getOrderDetailsPaymentDetailJsonElement() {
		return mOrderDetailsPaymentDetailJsonElement;
	}

	/**
	 * Sets the order details payment detail json element.
	 *
	 * @param pOrderDetailsPaymentDetailJsonElement the mOrderDetailsPaymentDetailJsonElement to set
	 */
	public void setOrderDetailsPaymentDetailJsonElement(
			String pOrderDetailsPaymentDetailJsonElement) {
		this.mOrderDetailsPaymentDetailJsonElement = pOrderDetailsPaymentDetailJsonElement;
	}

	/**
	 * Gets the order details response status json element.
	 *
	 * @return the mOrderDetailsResponseStatusJsonElement
	 */
	public String getOrderDetailsResponseStatusJsonElement() {
		return mOrderDetailsResponseStatusJsonElement;
	}

	/**
	 * Sets the order details response status json element.
	 *
	 * @param pOrderDetailsResponseStatusJsonElement the mOrderDetailsResponseStatusJsonElement to set
	 */
	public void setOrderDetailsResponseStatusJsonElement(
			String pOrderDetailsResponseStatusJsonElement) {
		this.mOrderDetailsResponseStatusJsonElement = pOrderDetailsResponseStatusJsonElement;
	}

	/**
	 * Gets the order details shipping info array json element.
	 *
	 * @return the mOrderDetailsShippingInfoArrayJsonElement
	 */
	public String getOrderDetailsShippingInfoArrayJsonElement() {
		return mOrderDetailsShippingInfoArrayJsonElement;
	}

	/**
	 * Sets the order details shipping info array json element.
	 *
	 * @param pOrderDetailsShippingInfoArrayJsonElement the mOrderDetailsShippingInfoArrayJsonElement to set
	 */
	public void setOrderDetailsShippingInfoArrayJsonElement(
			String pOrderDetailsShippingInfoArrayJsonElement) {
		this.mOrderDetailsShippingInfoArrayJsonElement = pOrderDetailsShippingInfoArrayJsonElement;
	}

	/**
	 * Gets the order details iscarton available json element.
	 *
	 * @return the mOrderDetailsIscartonAvailableJsonElement
	 */
	public String getOrderDetailsIscartonAvailableJsonElement() {
		return mOrderDetailsIscartonAvailableJsonElement;
	}

	/**
	 * Sets the order details iscarton available json element.
	 *
	 * @param pOrderDetailsIscartonAvailableJsonElement the mOrderDetailsIscartonAvailableJsonElement to set
	 */
	public void setOrderDetailsIscartonAvailableJsonElement(
			String pOrderDetailsIscartonAvailableJsonElement) {
		this.mOrderDetailsIscartonAvailableJsonElement = pOrderDetailsIscartonAvailableJsonElement;
	}

	/**
	 * Gets the order details display order json element.
	 *
	 * @return the mOrderDetailsDisplayOrderJsonElement
	 */
	public String getOrderDetailsDisplayOrderJsonElement() {
		return mOrderDetailsDisplayOrderJsonElement;
	}

	/**
	 * Sets the order details display order json element.
	 *
	 * @param pOrderDetailsDisplayOrderJsonElement the mOrderDetailsDisplayOrderJsonElement to set
	 */
	public void setOrderDetailsDisplayOrderJsonElement(
			String pOrderDetailsDisplayOrderJsonElement) {
		this.mOrderDetailsDisplayOrderJsonElement = pOrderDetailsDisplayOrderJsonElement;
	}


	/**
	 * Gets the shipping method map.
	 *
	 * @return the shippingMethodMap
	 */
	public Map<String, String> getShippingMethodMap() {
		return mShippingMethodMap;
	}

	/**
	 * Sets the shipping method map.
	 *
	 * @param pShippingMethodMap the shippingMethodMap to set
	 */
	public void setShippingMethodMap(Map<String, String> pShippingMethodMap) {
		mShippingMethodMap = pShippingMethodMap;
	}

	/**
	 * This method returns the customerFullDetailsJsonElement value.
	 *
	 * @return the customerFullDetailsJsonElement
	 */
	public String getCustomerFullDetailsJsonElement() {
		return mCustomerFullDetailsJsonElement;
	}

	/**
	 * This method sets the customerFullDetailsJsonElement with parameter value pCustomerFullDetailsJsonElement.
	 *
	 * @param pCustomerFullDetailsJsonElement the customerFullDetailsJsonElement to set
	 */
	public void setCustomerFullDetailsJsonElement(
			String pCustomerFullDetailsJsonElement) {
		mCustomerFullDetailsJsonElement = pCustomerFullDetailsJsonElement;
	}
	
	/**
	 * Holds mCoiServiceName.
	 */
	private String mCoiServiceName;
	/**
	 * Holds mCustomerMasterImportServiceName.
	 */
	private String mCustomerMasterImportServiceName;
	/**
	 * Holds mOrderUpdateServiceName.
	 */
	private String mOrderUpdateServiceName;
	/**
	 * Holds mOrderCancelServiceName.
	 */
	private String mOrderCancelServiceName;
	/**
	 * Holds mOrderNumberJSONPrimitive.
	 */
	private String mOrderNumberJSONPrimitive;
	/**
	 * Holds mExtOrderNumberJSONPrimitive.
	 */
	private String mExtOrderNumberJSONPrimitive;
	/**
	 * Holds mExtCustomerIdJSONPrimitive.
	 */
	private String mExtCustomerIdJSONPrimitive;

	/**
	 * Gets the coi service name.
	 *
	 * @return the coiServiceName
	 */
	public String getCoiServiceName() {
		return mCoiServiceName;
	}

	/**
	 * Sets the coi service name.
	 *
	 * @param pCoiServiceName the coiServiceName to set
	 */
	public void setCoiServiceName(String pCoiServiceName) {
		mCoiServiceName = pCoiServiceName;
	}

	/**
	 * Gets the customer master import service name.
	 *
	 * @return the customerMasterImportServiceName
	 */
	public String getCustomerMasterImportServiceName() {
		return mCustomerMasterImportServiceName;
	}

	/**
	 * Sets the customer master import service name.
	 *
	 * @param pCustomerMasterImportServiceName the customerMasterImportServiceName to set
	 */
	public void setCustomerMasterImportServiceName(
			String pCustomerMasterImportServiceName) {
		mCustomerMasterImportServiceName = pCustomerMasterImportServiceName;
	}

	/**
	 * Gets the order update service name.
	 *
	 * @return the orderUpdateServiceName
	 */
	public String getOrderUpdateServiceName() {
		return mOrderUpdateServiceName;
	}

	/**
	 * Sets the order update service name.
	 *
	 * @param pOrderUpdateServiceName the orderUpdateServiceName to set
	 */
	public void setOrderUpdateServiceName(String pOrderUpdateServiceName) {
		mOrderUpdateServiceName = pOrderUpdateServiceName;
	}

	/**
	 * Gets the order cancel service name.
	 *
	 * @return the orderCancelServiceName
	 */
	public String getOrderCancelServiceName() {
		return mOrderCancelServiceName;
	}

	/**
	 * Sets the order cancel service name.
	 *
	 * @param pOrderCancelServiceName the orderCancelServiceName to set
	 */
	public void setOrderCancelServiceName(String pOrderCancelServiceName) {
		mOrderCancelServiceName = pOrderCancelServiceName;
	}

	/**
	 * Gets the order number json primitive.
	 *
	 * @return the orderNumberJSONPrimitive
	 */
	public String getOrderNumberJSONPrimitive() {
		return mOrderNumberJSONPrimitive;
	}

	/**
	 * Sets the order number json primitive.
	 *
	 * @param pOrderNumberJSONPrimitive the orderNumberJSONPrimitive to set
	 */
	public void setOrderNumberJSONPrimitive(String pOrderNumberJSONPrimitive) {
		mOrderNumberJSONPrimitive = pOrderNumberJSONPrimitive;
	}

	/**
	 * Gets the ext order number json primitive.
	 *
	 * @return the extOrderNumberJSONPrimitive
	 */
	public String getExtOrderNumberJSONPrimitive() {
		return mExtOrderNumberJSONPrimitive;
	}

	/**
	 * Sets the ext order number json primitive.
	 *
	 * @param pExtOrderNumberJSONPrimitive the extOrderNumberJSONPrimitive to set
	 */
	public void setExtOrderNumberJSONPrimitive(String pExtOrderNumberJSONPrimitive) {
		mExtOrderNumberJSONPrimitive = pExtOrderNumberJSONPrimitive;
	}

	/**
	 * Gets the ext customer id json primitive.
	 *
	 * @return the extCustomerIdJSONPrimitive
	 */
	public String getExtCustomerIdJSONPrimitive() {
		return mExtCustomerIdJSONPrimitive;
	}

	/**
	 * Sets the ext customer id json primitive.
	 *
	 * @param pExtCustomerIdJSONPrimitive the extCustomerIdJSONPrimitive to set
	 */
	public void setExtCustomerIdJSONPrimitive(String pExtCustomerIdJSONPrimitive) {
		mExtCustomerIdJSONPrimitive = pExtCustomerIdJSONPrimitive;
	}

	/**
	 * Gets the layaway order history reserve property.
	 *
	 * @return the mLayawayOrderHistoryReserveProperty
	 */
	public String getLayawayOrderHistoryReserveProperty() {
		return mLayawayOrderHistoryReserveProperty;
	}

	/**
	 * Sets the layaway order history reserve property.
	 *
	 * @param pLayawayOrderHistoryReserveProperty the mLayawayOrderHistoryReserveProperty to set
	 */
	public void setLayawayOrderHistoryReserveProperty(
			String pLayawayOrderHistoryReserveProperty) {
		this.mLayawayOrderHistoryReserveProperty = pLayawayOrderHistoryReserveProperty;
	}

	/**
	 * This method returns the generateCOImportRequestInSync value.
	 *
	 * @return the generateCOImportRequestInSync
	 */
	public boolean isGenerateCOImportRequestInSync() {
		return mGenerateCOImportRequestInSync;
	}

	/**
	 * This method sets the generateCOImportRequestInSync with parameter value pGenerateCOImportRequestInSync.
	 *
	 * @param pGenerateCOImportRequestInSync the generateCOImportRequestInSync to set
	 */
	public void setGenerateCOImportRequestInSync(
			boolean pGenerateCOImportRequestInSync) {
		mGenerateCOImportRequestInSync = pGenerateCOImportRequestInSync;
	}

	/**
	 * Gets the order details error message string.
	 *
	 * @return the mOrderDetailsErrorMessageString
	 */
	public String getOrderDetailsErrorMessageString() {
		return mOrderDetailsErrorMessageString;
	}

	/**
	 * Sets the order details error message string.
	 *
	 * @param pOrderDetailsErrorMessageString the mOrderDetailsErrorMessageString to set
	 */
	public void setOrderDetailsErrorMessageString(
			String pOrderDetailsErrorMessageString) {
		this.mOrderDetailsErrorMessageString = pOrderDetailsErrorMessageString;
	}

	/**
	 * Gets the order history error message string.
	 *
	 * @return the mOrderHistoryErrorMessageString
	 */
	public String getOrderHistoryErrorMessageString() {
		return mOrderHistoryErrorMessageString;
	}

	/**
	 * Sets the order history error message string.
	 *
	 * @param pOrderHistoryErrorMessageString the mOrderHistoryErrorMessageString to set
	 */
	public void setOrderHistoryErrorMessageString(
			String pOrderHistoryErrorMessageString) {
		this.mOrderHistoryErrorMessageString = pOrderHistoryErrorMessageString;
	}

	/**
	 * Gets the code json element.
	 *
	 * @return the mCodeJsonElement
	 */
	public String getCodeJsonElement() {
		return mCodeJsonElement;
	}

	/**
	 * Sets the code json element.
	 *
	 * @param pCodeJsonElement the mCodeJsonElement to set
	 */
	public void setCodeJsonElement(String pCodeJsonElement) {
		this.mCodeJsonElement = pCodeJsonElement;
	}

	/**
	 * Gets the description json element.
	 *
	 * @return the mDescriptionJsonElement
	 */
	public String getDescriptionJsonElement() {
		return mDescriptionJsonElement;
	}

	/**
	 * Sets the description json element.
	 *
	 * @param pDescriptionJsonElement the mDescriptionJsonElement to set
	 */
	public void setDescriptionJsonElement(String pDescriptionJsonElement) {
		this.mDescriptionJsonElement = pDescriptionJsonElement;
	}

	/**
	 * Gets the customer master error message string.
	 *
	 * @return the mCustomerMasterErrorMessageString
	 */
	public String getCustomerMasterErrorMessageString() {
		return mCustomerMasterErrorMessageString;
	}

	/**
	 * Sets the customer master error message string.
	 *
	 * @param pCustomerMasterErrorMessageString the mCustomerMasterErrorMessageString to set
	 */
	public void setCustomerMasterErrorMessageString(
			String pCustomerMasterErrorMessageString) {
		this.mCustomerMasterErrorMessageString = pCustomerMasterErrorMessageString;
	}

	/**
	 * Gets the customer order error message string.
	 *
	 * @return the mCustomerOrderErrorMessageString
	 */
	public String getCustomerOrderErrorMessageString() {
		return mCustomerOrderErrorMessageString;
	}

	/**
	 * Sets the customer order error message string.
	 *
	 * @param pCustomerOrderErrorMessageString the mCustomerOrderErrorMessageString to set
	 */
	public void setCustomerOrderErrorMessageString(
			String pCustomerOrderErrorMessageString) {
		this.mCustomerOrderErrorMessageString = pCustomerOrderErrorMessageString;
	}

	/**
	 * Gets the co request rest url.
	 *
	 * @return the mCoRequestRestURL
	 */
	public String getCoRequestRestURL() {
		return mCoRequestRestURL;
	}

	/**
	 * Sets the co request rest url.
	 *
	 * @param pCoRequestRestURL the mCoRequestRestURL to set
	 */
	public void setCoRequestRestURL(String pCoRequestRestURL) {
		this.mCoRequestRestURL = pCoRequestRestURL;
	}

	/**
	 * Gets the delivery option sts.
	 *
	 * @return the deliveryOptionSTS
	 */
	public String getDeliveryOptionSTS() {
		return mDeliveryOptionSTS;
	}

	/**
	 * Sets the delivery option sts.
	 *
	 * @param pDeliveryOptionSTS the deliveryOptionSTS to set
	 */
	public void setDeliveryOptionSTS(String pDeliveryOptionSTS) {
		mDeliveryOptionSTS = pDeliveryOptionSTS;
	}

	/**
	 * Gets the delivery option sta.
	 *
	 * @return the deliveryOptionSTA
	 */
	public String getDeliveryOptionSTA() {
		return mDeliveryOptionSTA;
	}

	/**
	 * Sets the delivery option sta.
	 *
	 * @param pDeliveryOptionSTA the deliveryOptionSTA to set
	 */
	public void setDeliveryOptionSTA(String pDeliveryOptionSTA) {
		mDeliveryOptionSTA = pDeliveryOptionSTA;
	}

	/**
	 * Gets the charge name e911.
	 *
	 * @return the chargeNameE911
	 */
	public String getChargeNameE911() {
		return mChargeNameE911;
	}

	/**
	 * Sets the charge name e911.
	 *
	 * @param pChargeNameE911 the chargeNameE911 to set
	 */
	public void setChargeNameE911(String pChargeNameE911) {
		mChargeNameE911 = pChargeNameE911;
	}

	/**
	 * Gets the transaction decision success.
	 *
	 * @return the transactionDecisionSuccess
	 */
	public String getTransactionDecisionSuccess() {
		return mTransactionDecisionSuccess;
	}

	/**
	 * Sets the transaction decision success.
	 *
	 * @param pTransactionDecisionSuccess the transactionDecisionSuccess to set
	 */
	public void setTransactionDecisionSuccess(String pTransactionDecisionSuccess) {
		mTransactionDecisionSuccess = pTransactionDecisionSuccess;
	}

	/**
	 * Gets the gift message bpp note code.
	 *
	 * @return the giftMessageBPPNoteCode
	 */
	public String getGiftMessageBPPNoteCode() {
		return mGiftMessageBPPNoteCode;
	}

	/**
	 * Sets the gift message bpp note code.
	 *
	 * @param pGiftMessageBPPNoteCode the giftMessageBPPNoteCode to set
	 */
	public void setGiftMessageBPPNoteCode(String pGiftMessageBPPNoteCode) {
		mGiftMessageBPPNoteCode = pGiftMessageBPPNoteCode;
	}

	/**
	 * Gets the uid pipe delimeter.
	 *
	 * @return the uidPipeDelimeter
	 */
	public String getUidPipeDelimeter() {
		return mUidPipeDelimeter;
	}

	/**
	 * Sets the uid pipe delimeter.
	 *
	 * @param pUidPipeDelimeter the uidPipeDelimeter to set
	 */
	public void setUidPipeDelimeter(String pUidPipeDelimeter) {
		mUidPipeDelimeter = pUidPipeDelimeter;
	}

	/**
	 * This method returns the paymentTransactionRecordStatus value.
	 *
	 * @return the paymentTransactionRecordStatus
	 */
	public String getPaymentTransactionRecordStatus() {
		return mPaymentTransactionRecordStatus;
	}

	/**
	 * This method sets the paymentTransactionRecordStatus with parameter value pPaymentTransactionRecordStatus.
	 *
	 * @param pPaymentTransactionRecordStatus the paymentTransactionRecordStatus to set
	 */
	public void setPaymentTransactionRecordStatus(
			String pPaymentTransactionRecordStatus) {
		mPaymentTransactionRecordStatus = pPaymentTransactionRecordStatus;
	}
	/**
	 * Holds mPayInStotePaymentEntryType.
	 */
	private String mPayInStotePaymentEntryType;
	/**
	 * Holds mCsrPaymentEntryType.
	 */
	private String mCsrPaymentEntryType;
	/**
	 * Holds mSosPaymentEntryType.
	 */
	private String mSosPaymentEntryType;
	/**
	 * Holds mMobilePaymentEntryType.
	 */
	private String mMobilePaymentEntryType;
	/**
	 * Holds mWebPaymentEntryType.
	 */
	private String mWebPaymentEntryType;
	/**
	 * Holds mOnHoldState.
	 */
	private List<String> mOnHoldState;
	/**
	 * Holds mShippingFeeSKUType.
	 */
	private String mShippingFeeSKUType;
	/**
	 * Holds mSecurityCode.
	 */
	private String mSecurityCode;
	/**
	 * Holds mSecurityCode.
	 */
	private boolean mGiftCardSecurityCode;

	/**
	 * Gets the pay in stote payment entry type.
	 *
	 * @return the payInStotePaymentEntryType
	 */
	public String getPayInStotePaymentEntryType() {
		return mPayInStotePaymentEntryType;
	}

	/**
	 * Sets the pay in stote payment entry type.
	 *
	 * @param pPayInStotePaymentEntryType the payInStotePaymentEntryType to set
	 */
	public void setPayInStotePaymentEntryType(String pPayInStotePaymentEntryType) {
		mPayInStotePaymentEntryType = pPayInStotePaymentEntryType;
	}

	/**
	 * Gets the csr payment entry type.
	 *
	 * @return the csrPaymentEntryType
	 */
	public String getCsrPaymentEntryType() {
		return mCsrPaymentEntryType;
	}

	/**
	 * Sets the csr payment entry type.
	 *
	 * @param pCsrPaymentEntryType the csrPaymentEntryType to set
	 */
	public void setCsrPaymentEntryType(String pCsrPaymentEntryType) {
		mCsrPaymentEntryType = pCsrPaymentEntryType;
	}

	/**
	 * Gets the sos payment entry type.
	 *
	 * @return the sosPaymentEntryType
	 */
	public String getSosPaymentEntryType() {
		return mSosPaymentEntryType;
	}

	/**
	 * Sets the sos payment entry type.
	 *
	 * @param pSosPaymentEntryType the sosPaymentEntryType to set
	 */
	public void setSosPaymentEntryType(String pSosPaymentEntryType) {
		mSosPaymentEntryType = pSosPaymentEntryType;
	}

	/**
	 * Gets the mobile payment entry type.
	 *
	 * @return the mobilePaymentEntryType
	 */
	public String getMobilePaymentEntryType() {
		return mMobilePaymentEntryType;
	}

	/**
	 * Sets the mobile payment entry type.
	 *
	 * @param pMobilePaymentEntryType the mobilePaymentEntryType to set
	 */
	public void setMobilePaymentEntryType(String pMobilePaymentEntryType) {
		mMobilePaymentEntryType = pMobilePaymentEntryType;
	}

	/**
	 * Gets the web payment entry type.
	 *
	 * @return the webPaymentEntryType
	 */
	public String getWebPaymentEntryType() {
		return mWebPaymentEntryType;
	}

	/**
	 * Sets the web payment entry type.
	 *
	 * @param pWebPaymentEntryType the webPaymentEntryType to set
	 */
	public void setWebPaymentEntryType(String pWebPaymentEntryType) {
		mWebPaymentEntryType = pWebPaymentEntryType;
	}

	/**
	 * Gets the primary first name.
	 *
	 * @return the primaryFirstName
	 */
	public String getPrimaryFirstName() {
		return mPrimaryFirstName;
	}

	/**
	 * Sets the primary first name.
	 *
	 * @param pPrimaryFirstName the primaryFirstName to set
	 */
	public void setPrimaryFirstName(String pPrimaryFirstName) {
		mPrimaryFirstName = pPrimaryFirstName;
	}

	/**
	 * Gets the primary last name.
	 *
	 * @return the primaryLastName
	 */
	public String getPrimaryLastName() {
		return mPrimaryLastName;
	}

	/**
	 * Sets the primary last name.
	 *
	 * @param pPrimaryLastName the primaryLastName to set
	 */
	public void setPrimaryLastName(String pPrimaryLastName) {
		mPrimaryLastName = pPrimaryLastName;
	}

	/**
	 * Gets the primary phone.
	 *
	 * @return the primaryPhone
	 */
	public String getPrimaryPhone() {
		return mPrimaryPhone;
	}

	/**
	 * Sets the primary phone.
	 *
	 * @param pPrimaryPhone the primaryPhone to set
	 */
	public void setPrimaryPhone(String pPrimaryPhone) {
		mPrimaryPhone = pPrimaryPhone;
	}

	/**
	 * Gets the primary email.
	 *
	 * @return the primaryEmail
	 */
	public String getPrimaryEmail() {
		return mPrimaryEmail;
	}

	/**
	 * Sets the primary email.
	 *
	 * @param pPrimaryEmail the primaryEmail to set
	 */
	public void setPrimaryEmail(String pPrimaryEmail) {
		mPrimaryEmail = pPrimaryEmail;
	}

	/**
	 * Gets the order history total no of records json element.
	 *
	 * @return the mOrderHistoryTotalNoOfRecordsJsonElement
	 */
	public String getOrderHistoryTotalNoOfRecordsJsonElement() {
		return mOrderHistoryTotalNoOfRecordsJsonElement;
	}

	/**
	 * Sets the order history total no of records json element.
	 *
	 * @param pOrderHistoryTotalNoOfRecordsJsonElement the mOrderHistoryTotalNoOfRecordsJsonElement to set
	 */
	public void setOrderHistoryTotalNoOfRecordsJsonElement(
			String pOrderHistoryTotalNoOfRecordsJsonElement) {
		this.mOrderHistoryTotalNoOfRecordsJsonElement = pOrderHistoryTotalNoOfRecordsJsonElement;
	}

	/**
	 * This method returns the transactionToRollback value.
	 *
	 * @return the transactionToRollback
	 */
	public boolean isTransactionToRollback() {
		return mTransactionToRollback;
	}

	/**
	 * This method sets the transactionToRollback with parameter value pTransactionToRollback.
	 *
	 * @param pTransactionToRollback the transactionToRollback to set
	 */
	public void setTransactionToRollback(boolean pTransactionToRollback) {
		mTransactionToRollback = pTransactionToRollback;
	}

	/**
	 * Gets the on hold state.
	 *
	 * @return the onHoldState
	 */
	public List<String> getOnHoldState() {
		return mOnHoldState;
	}

	/**
	 * Sets the on hold state.
	 *
	 * @param pOnHoldState the onHoldState to set.
	 */
	public void setOnHoldState(List<String> pOnHoldState) {
		mOnHoldState = pOnHoldState;
	}

	/**
	 * Gets the shipping fee sku type.
	 *
	 * @return the shippingFeeSKUType
	 */
	public String getShippingFeeSKUType() {
		return mShippingFeeSKUType;
	}

	/**
	 * Sets the shipping fee sku type.
	 *
	 * @param pShippingFeeSKUType the shippingFeeSKUType to set
	 */
	public void setShippingFeeSKUType(String pShippingFeeSKUType) {
		mShippingFeeSKUType = pShippingFeeSKUType;
	}

	/**
	 * Gets the security code.
	 *
	 * @return the securityCode
	 */
	public String getSecurityCode() {
		return mSecurityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param pSecurityCode the securityCode to set
	 */
	public void setSecurityCode(String pSecurityCode) {
		mSecurityCode = pSecurityCode;
	}

	/**
	 * Checks if is gift card security code.
	 *
	 * @return the giftCardSecurityCode
	 */
	public boolean isGiftCardSecurityCode() {
		return mGiftCardSecurityCode;
	}

	/**
	 * Sets the gift card security code.
	 *
	 * @param pGiftCardSecurityCode the giftCardSecurityCode to set
	 */
	public void setGiftCardSecurityCode(boolean pGiftCardSecurityCode) {
		mGiftCardSecurityCode = pGiftCardSecurityCode;
	}

	/**
	 * Gets the authorization transaction type.
	 *
	 * @return the authorizationTransactionType
	 */
	public String getAuthorizationTransactionType() {
		return mAuthorizationTransactionType;
	}

	/**
	 * Sets the authorization transaction type.
	 *
	 * @param pAuthorizationTransactionType the authorizationTransactionType to set
	 */
	public void setAuthorizationTransactionType(String pAuthorizationTransactionType) {
		mAuthorizationTransactionType = pAuthorizationTransactionType;
	}

	/**
	 * Gets the settlement transaction type.
	 *
	 * @return the settlementTransactionType
	 */
	public String getSettlementTransactionType() {
		return mSettlementTransactionType;
	}

	/**
	 * Sets the settlement transaction type.
	 *
	 * @param pSettlementTransactionType the settlementTransactionType to set
	 */
	public void setSettlementTransactionType(String pSettlementTransactionType) {
		mSettlementTransactionType = pSettlementTransactionType;
	}
	
	/**
	 * Gets the order details order captured date json element.
	 *
	 * @return the mOrderDetailsOrderCapturedDateJsonElement
	 */
	public String getOrderDetailsOrderCapturedDateJsonElement() {
		return mOrderDetailsOrderCapturedDateJsonElement;
	}

	/**
	 * Sets the order details order captured date json element.
	 *
	 * @param pOrderDetailsOrderCapturedDateJsonElement the mOrderDetailsOrderCapturedDateJsonElement to set
	 */
	public void setOrderDetailsOrderCapturedDateJsonElement(
			String pOrderDetailsOrderCapturedDateJsonElement) {
		this.mOrderDetailsOrderCapturedDateJsonElement = pOrderDetailsOrderCapturedDateJsonElement;
	}
	
	/**
	 * mLayawayOrderRequest.
	 */
	private boolean mLayawayOrderRequest;

	/**
	 * Checks if is layaway order request.
	 *
	 * @return the layawayOrderRequest
	 */
	public boolean isLayawayOrderRequest() {
		return mLayawayOrderRequest;
	}

	/**
	 * Sets the layaway order request.
	 *
	 * @param pLayawayOrderRequest the layawayOrderRequest to set
	 */
	public void setLayawayOrderRequest(boolean pLayawayOrderRequest) {
		mLayawayOrderRequest = pLayawayOrderRequest;
	}
	/**
	 * mPaymentMethodGC.
	 */
	private String mPaymentMethodGC;
	
	/** mPaymentMethodJSONElement. */
	private String mPaymentMethodJSONElement;
	
	/** mSettlementAmountJSONElement. */
	private String mSettlementAmountJSONElement;
	/**
	 * mEstTimeZone.
	 */
	private String mEstTimeZone;
	/**
	 * mLineTotalJSONElement.
	 */
	private String mLineTotalJSONElement;

	/**
	 * Gets the payment method gc.
	 *
	 * @return the paymentMethodGC
	 */
	public String getPaymentMethodGC() {
		return mPaymentMethodGC;
	}

	/**
	 * Sets the payment method gc.
	 *
	 * @param pPaymentMethodGC the paymentMethodGC to set
	 */
	public void setPaymentMethodGC(String pPaymentMethodGC) {
		mPaymentMethodGC = pPaymentMethodGC;
	}

	/**
	 * Gets the payment method json element.
	 *
	 * @return the paymentMethodJSONElement
	 */
	public String getPaymentMethodJSONElement() {
		return mPaymentMethodJSONElement;
	}

	/**
	 * Sets the payment method json element.
	 *
	 * @param pPaymentMethodJSONElement the paymentMethodJSONElement to set
	 */
	public void setPaymentMethodJSONElement(String pPaymentMethodJSONElement) {
		mPaymentMethodJSONElement = pPaymentMethodJSONElement;
	}

	/**
	 * Gets the settlement amount json element.
	 *
	 * @return the settlementAmountJSONElement
	 */
	public String getSettlementAmountJSONElement() {
		return mSettlementAmountJSONElement;
	}

	/**
	 * Sets the settlement amount json element.
	 *
	 * @param pSettlementAmountJSONElement the settlementAmountJSONElement to set
	 */
	public void setSettlementAmountJSONElement(String pSettlementAmountJSONElement) {
		mSettlementAmountJSONElement = pSettlementAmountJSONElement;
	}

	/**
	 * Gets the est time zone.
	 *
	 * @return the estTimeZone
	 */
	public String getEstTimeZone() {
		return mEstTimeZone;
	}

	/**
	 * Sets the est time zone.
	 *
	 * @param pEstTimeZone the estTimeZone to set
	 */
	public void setEstTimeZone(String pEstTimeZone) {
		mEstTimeZone = pEstTimeZone;
	}

	/**
	 * Gets the line total json element.
	 *
	 * @return the lineTotalJSONElement
	 */
	public String getLineTotalJSONElement() {
		return mLineTotalJSONElement;
	}

	/**
	 * Sets the line total json element.
	 *
	 * @param pLineTotalJSONElement the lineTotalJSONElement to set
	 */
	public void setLineTotalJSONElement(String pLineTotalJSONElement) {
		mLineTotalJSONElement = pLineTotalJSONElement;
	}

	/**
	 * Gets the order details ordered quantity property name.
	 *
	 * @return the mOrderDetailsOrderedQuantityPropertyName
	 */
	public String getOrderDetailsOrderedQuantityPropertyName() {
		return mOrderDetailsOrderedQuantityPropertyName;
	}

	/**
	 * Sets the order details ordered quantity property name.
	 *
	 * @param pOrderDetailsOrderedQuantityPropertyName the mOrderDetailsOrderedQuantityPropertyName to set
	 */
	public void setOrderDetailsOrderedQuantityPropertyName(
			String pOrderDetailsOrderedQuantityPropertyName) {
		this.mOrderDetailsOrderedQuantityPropertyName = pOrderDetailsOrderedQuantityPropertyName;
	}

	/**
	 * Gets the order details total quantity property name.
	 *
	 * @return the mOrderDetailsTotalQuantityPropertyName
	 */
	public String getOrderDetailsTotalQuantityPropertyName() {
		return mOrderDetailsTotalQuantityPropertyName;
	}

	/**
	 * Sets the order details total quantity property name.
	 *
	 * @param pOrderDetailsTotalQuantityPropertyName the mOrderDetailsTotalQuantityPropertyName to set
	 */
	public void setOrderDetailsTotalQuantityPropertyName(
			String pOrderDetailsTotalQuantityPropertyName) {
		this.mOrderDetailsTotalQuantityPropertyName = pOrderDetailsTotalQuantityPropertyName;
	}

	/**
	 * This method returns the orderConfirmedJsonElement value.
	 *
	 * @return the orderConfirmedJsonElement
	 */
	public String getOrderConfirmedJsonElement() {
		return mOrderConfirmedJsonElement;
	}

	/**
	 * This method sets the orderConfirmedJsonElement with parameter value pOrderConfirmedJsonElement.
	 *
	 * @param pOrderConfirmedJsonElement the orderConfirmedJsonElement to set
	 */
	public void setOrderConfirmedJsonElement(String pOrderConfirmedJsonElement) {
		mOrderConfirmedJsonElement = pOrderConfirmedJsonElement;
	}

	/**
	 * This method returns the onHoldJsonElement value.
	 *
	 * @return the onHoldJsonElement
	 */
	public String getOnHoldJsonElement() {
		return mOnHoldJsonElement;
	}

	/**
	 * This method sets the onHoldJsonElement with parameter value pOnHoldJsonElement.
	 *
	 * @param pOnHoldJsonElement the onHoldJsonElement to set
	 */
	public void setOnHoldJsonElement(String pOnHoldJsonElement) {
		mOnHoldJsonElement = pOnHoldJsonElement;
	}

	/**
	 * This method returns the minimizeShipmentsJsonElement value.
	 *
	 * @return the minimizeShipmentsJsonElement
	 */
	public String getMinimizeShipmentsJsonElement() {
		return mMinimizeShipmentsJsonElement;
	}

	/**
	 * This method sets the minimizeShipmentsJsonElement with parameter value pMinimizeShipmentsJsonElement.
	 *
	 * @param pMinimizeShipmentsJsonElement the minimizeShipmentsJsonElement to set
	 */
	public void setMinimizeShipmentsJsonElement(String pMinimizeShipmentsJsonElement) {
		mMinimizeShipmentsJsonElement = pMinimizeShipmentsJsonElement;
	}

	/**
	 * This method returns the orderSubtotalJsonElement value.
	 *
	 * @return the orderSubtotalJsonElement
	 */
	public String getOrderSubtotalJsonElement() {
		return mOrderSubtotalJsonElement;
	}

	/**
	 * This method sets the orderSubtotalJsonElement with parameter value pOrderSubtotalJsonElement.
	 *
	 * @param pOrderSubtotalJsonElement the orderSubtotalJsonElement to set
	 */
	public void setOrderSubtotalJsonElement(String pOrderSubtotalJsonElement) {
		mOrderSubtotalJsonElement = pOrderSubtotalJsonElement;
	}

	/**
	 * This method returns the orderTotalJsonElement value.
	 *
	 * @return the orderTotalJsonElement
	 */
	public String getOrderTotalJsonElement() {
		return mOrderTotalJsonElement;
	}

	/**
	 * This method sets the orderTotalJsonElement with parameter value pOrderTotalJsonElement.
	 *
	 * @param pOrderTotalJsonElement the orderTotalJsonElement to set
	 */
	public void setOrderTotalJsonElement(String pOrderTotalJsonElement) {
		mOrderTotalJsonElement = pOrderTotalJsonElement;
	}

	/**
	 * This method returns the orderCancelledJsonElement value.
	 *
	 * @return the orderCancelledJsonElement
	 */
	public String getOrderCancelledJsonElement() {
		return mOrderCancelledJsonElement;
	}

	/**
	 * This method sets the orderCancelledJsonElement with parameter value pOrderCancelledJsonElement.
	 *
	 * @param pOrderCancelledJsonElement the orderCancelledJsonElement to set
	 */
	public void setOrderCancelledJsonElement(String pOrderCancelledJsonElement) {
		mOrderCancelledJsonElement = pOrderCancelledJsonElement;
	}

	/**
	 * This method returns the noteTypeShipping value.
	 *
	 * @return the noteTypeShipping
	 */
	public String getNoteTypeShipping() {
		return mNoteTypeShipping;
	}

	/**
	 * This method sets the noteTypeShipping with parameter value pNoteTypeShipping.
	 *
	 * @param pNoteTypeShipping the noteTypeShipping to set
	 */
	public void setNoteTypeShipping(String pNoteTypeShipping) {
		mNoteTypeShipping = pNoteTypeShipping;
	}

	/**
	 * This method returns the noteTypeMisc value.
	 *
	 * @return the noteTypeMisc
	 */
	public String getNoteTypeMisc() {
		return mNoteTypeMisc;
	}

	/**
	 * This method sets the noteTypeMisc with parameter value pNoteTypeMisc.
	 *
	 * @param pNoteTypeMisc the noteTypeMisc to set.
	 */
	public void setNoteTypeMisc(String pNoteTypeMisc) {
		mNoteTypeMisc = pNoteTypeMisc;
	}

	/**
	 * This method returns the includeCR82Changes value.
	 *
	 * @return the includeCR82Changes
	 */
	public boolean isIncludeCR82Changes() {
		return mIncludeCR82Changes;
	}

	/**
	 * This method sets the includeCR82Changes with parameter value pIncludeCR82Changes.
	 *
	 * @param pIncludeCR82Changes the includeCR82Changes to set
	 */
	public void setIncludeCR82Changes(boolean pIncludeCR82Changes) {
		mIncludeCR82Changes = pIncludeCR82Changes;
	}

	/**
	 * Gets the order details is gift json element.
	 *
	 * @return the mOrderDetailsIsGiftJsonElement
	 */
	public String getOrderDetailsIsGiftJsonElement() {
		return mOrderDetailsIsGiftJsonElement;
	}

	/**
	 * Sets the order details is gift json element.
	 *
	 * @param pOrderDetailsIsGiftJsonElement the mOrderDetailsIsGiftJsonElement to set
	 */
	public void setOrderDetailsIsGiftJsonElement(
			String pOrderDetailsIsGiftJsonElement) {
		this.mOrderDetailsIsGiftJsonElement = pOrderDetailsIsGiftJsonElement;
	}

	/**
	 * Gets the order details gift message json element.
	 *
	 * @return the mOrderDetailsGiftMessageJsonElement
	 */
	public String getOrderDetailsGiftMessageJsonElement() {
		return mOrderDetailsGiftMessageJsonElement;
	}

	/**
	 * Sets the order details gift message json element.
	 *
	 * @param pOrderDetailsGiftMessageJsonElement the mOrderDetailsGiftMessageJsonElement to set
	 */
	public void setOrderDetailsGiftMessageJsonElement(
			String pOrderDetailsGiftMessageJsonElement) {
		this.mOrderDetailsGiftMessageJsonElement = pOrderDetailsGiftMessageJsonElement;
	}

	/**
	 * Gets the order details total shipping surcharge json element.
	 *
	 * @return the mOrderDetailsTotalShippingSurchargeJsonElement
	 */
	public String getOrderDetailsTotalShippingSurchargeJsonElement() {
		return mOrderDetailsTotalShippingSurchargeJsonElement;
	}

	/**
	 * Sets the order details total shipping surcharge json element.
	 *
	 * @param pOrderDetailsTotalShippingSurchargeJsonElement the mOrderDetailsTotalShippingSurchargeJsonElement to set
	 */
	public void setOrderDetailsTotalShippingSurchargeJsonElement(
			String pOrderDetailsTotalShippingSurchargeJsonElement) {
		this.mOrderDetailsTotalShippingSurchargeJsonElement = pOrderDetailsTotalShippingSurchargeJsonElement;
	}

	/**
	 * Gets the order details formatted shipping json element.
	 *
	 * @return the mOrderDetailsFormattedShippingJsonElement
	 */
	public String getOrderDetailsFormattedShippingJsonElement() {
		return mOrderDetailsFormattedShippingJsonElement;
	}

	/**
	 * Sets the order details formatted shipping json element.
	 *
	 * @param pOrderDetailsFormattedShippingJsonElement the mOrderDetailsFormattedShippingJsonElement to set
	 */
	public void setOrderDetailsFormattedShippingJsonElement(
			String pOrderDetailsFormattedShippingJsonElement) {
		this.mOrderDetailsFormattedShippingJsonElement = pOrderDetailsFormattedShippingJsonElement;
	}

	/**
	 * Gets the order details formatted giftwrap json element.
	 *
	 * @return the mOrderDetailsFormattedGiftwrapJsonElement
	 */
	public String getOrderDetailsFormattedGiftwrapJsonElement() {
		return mOrderDetailsFormattedGiftwrapJsonElement;
	}

	/**
	 * Sets the order details formatted giftwrap json element.
	 *
	 * @param pOrderDetailsFormattedGiftwrapJsonElement the mOrderDetailsFormattedGiftwrapJsonElement to set
	 */
	public void setOrderDetailsFormattedGiftwrapJsonElement(
			String pOrderDetailsFormattedGiftwrapJsonElement) {
		this.mOrderDetailsFormattedGiftwrapJsonElement = pOrderDetailsFormattedGiftwrapJsonElement;
	}

	/**
	 * Gets the order details formatted ewaste json element.
	 *
	 * @return the mOrderDetailsFormattedEwasteJsonElement
	 */
	public String getOrderDetailsFormattedEwasteJsonElement() {
		return mOrderDetailsFormattedEwasteJsonElement;
	}

	/**
	 * Sets the order details formatted ewaste json element.
	 *
	 * @param pOrderDetailsFormattedEwasteJsonElement the mOrderDetailsFormattedEwasteJsonElement to set
	 */
	public void setOrderDetailsFormattedEwasteJsonElement(
			String pOrderDetailsFormattedEwasteJsonElement) {
		this.mOrderDetailsFormattedEwasteJsonElement = pOrderDetailsFormattedEwasteJsonElement;
	}

	/**
	 * Gets the order details formatted total shipping surcharge json element.
	 *
	 * @return the mOrderDetailsFormattedTotalShippingSurchargeJsonElement
	 */
	public String getOrderDetailsFormattedTotalShippingSurchargeJsonElement() {
		return mOrderDetailsFormattedTotalShippingSurchargeJsonElement;
	}

	/**
	 * Sets the order details formatted total shipping surcharge json element.
	 *
	 * @param pOrderDetailsFormattedTotalShippingSurchargeJsonElement the mOrderDetailsFormattedTotalShippingSurchargeJsonElement to set
	 */
	public void setOrderDetailsFormattedTotalShippingSurchargeJsonElement(
			String pOrderDetailsFormattedTotalShippingSurchargeJsonElement) {
		this.mOrderDetailsFormattedTotalShippingSurchargeJsonElement = pOrderDetailsFormattedTotalShippingSurchargeJsonElement;
	}

	/**
	 * Gets the order details help and returns url json element.
	 *
	 * @return the mOrderDetailsHelpAndReturnsURLJsonElement
	 */
	public String getOrderDetailsHelpAndReturnsURLJsonElement() {
		return mOrderDetailsHelpAndReturnsURLJsonElement;
	}

	/**
	 * Sets the order details help and returns url json element.
	 *
	 * @param pOrderDetailsHelpAndReturnsURLJsonElement the mOrderDetailsHelpAndReturnsURLJsonElement to set
	 */
	public void setOrderDetailsHelpAndReturnsURLJsonElement(
			String pOrderDetailsHelpAndReturnsURLJsonElement) {
		this.mOrderDetailsHelpAndReturnsURLJsonElement = pOrderDetailsHelpAndReturnsURLJsonElement;
	}

	/**
	 * Gets the order details help and returns json element.
	 *
	 * @return the mOrderDetailsHelpAndReturnsJsonElement
	 */
	public String getOrderDetailsHelpAndReturnsJsonElement() {
		return mOrderDetailsHelpAndReturnsJsonElement;
	}

	/**
	 * Sets the order details help and returns json element.
	 *
	 * @param pOrderDetailsHelpAndReturnsJsonElement the mOrderDetailsHelpAndReturnsJsonElement to set
	 */
	public void setOrderDetailsHelpAndReturnsJsonElement(
			String pOrderDetailsHelpAndReturnsJsonElement) {
		this.mOrderDetailsHelpAndReturnsJsonElement = pOrderDetailsHelpAndReturnsJsonElement;
	}
	/**
	 * Holds orderStatusForPIS.
	 */
	private String mOrderStatusForPIS;
	/**
	 * Holds jsonOrderStatusForPIS.
	 */
	private String mJsonOrderStatusForPIS;
	
	/**
	 * Gets the order status for pis.
	 *
	 * @return the orderStatusForPIS
	 */
	public String getOrderStatusForPIS() {
		return mOrderStatusForPIS;
	}

	/**
	 * Sets the order status for pis.
	 *
	 * @param pOrderStatusForPIS the orderStatusForPIS to set
	 */
	public void setOrderStatusForPIS(String pOrderStatusForPIS) {
		mOrderStatusForPIS = pOrderStatusForPIS;
	}

	/**
	 * Gets the json order status for pis.
	 *
	 * @return the jsonOrderStatusForPIS
	 */
	public String getJsonOrderStatusForPIS() {
		return mJsonOrderStatusForPIS;
	}

	/**
	 * Sets the json order status for pis.
	 *
	 * @param pJsonOrderStatusForPIS the jsonOrderStatusForPIS to set
	 */
	public void setJsonOrderStatusForPIS(String pJsonOrderStatusForPIS) {
		mJsonOrderStatusForPIS = pJsonOrderStatusForPIS;
	}
	/**
	 * Holds truGuftWrapType.
	 */
	private String mTruGuftWrapType;
	/**
	 * Holds bruGiftWrapType.
	 */
	private String mBruGiftWrapType;
	
	/**
	 * Gets the tru guft wrap type.
	 *
	 * @return the truGuftWrapType
	 */
	public String getTruGuftWrapType() {
		return mTruGuftWrapType;
	}

	/**
	 * Sets the tru guft wrap type.
	 *
	 * @param pTruGuftWrapType the truGuftWrapType to set
	 */
	public void setTruGuftWrapType(String pTruGuftWrapType) {
		mTruGuftWrapType = pTruGuftWrapType;
	}

	/**
	 * Gets the bru gift wrap type.
	 *
	 * @return the bruGiftWrapType
	 */
	public String getBruGiftWrapType() {
		return mBruGiftWrapType;
	}

	/**
	 * Sets the bru gift wrap type.
	 *
	 * @param pBruGiftWrapType the bruGiftWrapType to set
	 */
	public void setBruGiftWrapType(String pBruGiftWrapType) {
		mBruGiftWrapType = pBruGiftWrapType;
	}

	/**
	 * Gets the order details charge name value.
	 *
	 * @return the mOrderDetailsChargeNameValue
	 */
	public String getOrderDetailsChargeNameValue() {
		return mOrderDetailsChargeNameValue;
	}

	/**
	 * Sets the order details charge name value.
	 *
	 * @param pOrderDetailsChargeNameValue the mOrderDetailsChargeNameValue to set
	 */
	public void setOrderDetailsChargeNameValue(String pOrderDetailsChargeNameValue) {
		this.mOrderDetailsChargeNameValue = pOrderDetailsChargeNameValue;
	}

	/**
	 * Gets the order details carton count json element.
	 *
	 * @return the mOrderDetailsCartonCountJsonElement
	 */
	public String getOrderDetailsCartonCountJsonElement() {
		return mOrderDetailsCartonCountJsonElement;
	}

	/**
	 * Sets the order details carton count json element.
	 *
	 * @param pOrderDetailsCartonCountJsonElement the mOrderDetailsCartonCountJsonElement to set
	 */
	public void setOrderDetailsCartonCountJsonElement(
			String pOrderDetailsCartonCountJsonElement) {
		this.mOrderDetailsCartonCountJsonElement = pOrderDetailsCartonCountJsonElement;
	}

	/**
	 * Gets the order details shipment item count json element.
	 *
	 * @return the mOrderDetailsShipmentItemCountJsonElement
	 */
	public String getOrderDetailsShipmentItemCountJsonElement() {
		return mOrderDetailsShipmentItemCountJsonElement;
	}

	/**
	 * Sets the order details shipment item count json element.
	 *
	 * @param pOrderDetailsShipmentItemCountJsonElement the mOrderDetailsShipmentItemCountJsonElement to set
	 */
	public void setOrderDetailsShipmentItemCountJsonElement(
			String pOrderDetailsShipmentItemCountJsonElement) {
		this.mOrderDetailsShipmentItemCountJsonElement = pOrderDetailsShipmentItemCountJsonElement;
	}
	
	/**
	 * Holds shippingSurSKUType.
	 */
	private String mShippingSurSKUType;
	/**
	 * Holds layawaySKUType.
	 */
	private String mLayawaySKUType;
	/**
	 * Holds layawayShipVia.
	 */
	private String mLayawayShipVia;
	/**
	 * Holds layawayDeliveryOption.
	 */
	private String mLayawayDeliveryOption;

	/**
	 * Gets the shipping sur sku type.
	 *
	 * @return the shippingSurSKUType
	 */
	public String getShippingSurSKUType() {
		return mShippingSurSKUType;
	}

	/**
	 * Sets the shipping sur sku type.
	 *
	 * @param pShippingSurSKUType the shippingSurSKUType to set
	 */
	public void setShippingSurSKUType(String pShippingSurSKUType) {
		mShippingSurSKUType = pShippingSurSKUType;
	}

	/**
	 * Gets the layaway sku type.
	 *
	 * @return the layawaySKUType
	 */
	public String getLayawaySKUType() {
		return mLayawaySKUType;
	}

	/**
	 * Sets the layaway sku type.
	 *
	 * @param pLayawaySKUType the layawaySKUType to set
	 */
	public void setLayawaySKUType(String pLayawaySKUType) {
		mLayawaySKUType = pLayawaySKUType;
	}

	/**
	 * Gets the layaway ship via.
	 *
	 * @return the layawayShipVia
	 */
	public String getLayawayShipVia() {
		return mLayawayShipVia;
	}

	/**
	 * Sets the layaway ship via.
	 *
	 * @param pLayawayShipVia the layawayShipVia to set
	 */
	public void setLayawayShipVia(String pLayawayShipVia) {
		mLayawayShipVia = pLayawayShipVia;
	}

	/**
	 * Gets the layaway delivery option.
	 *
	 * @return the layawayDeliveryOption
	 */
	public String getLayawayDeliveryOption() {
		return mLayawayDeliveryOption;
	}

	/**
	 * Sets the layaway delivery option.
	 *
	 * @param pLayawayDeliveryOption the layawayDeliveryOption to set
	 */
	public void setLayawayDeliveryOption(String pLayawayDeliveryOption) {
		mLayawayDeliveryOption = pLayawayDeliveryOption;
	}
	/**
	 * Holds paymentStatuForGC.
	 */
	private String mPaymentStatuForGC;
	/**
	 * Holds taxwareDownStatus.
	 */
	private String mTaxwareDownStatus;
	
	/**
	 * Gets the payment statu for gc.
	 *
	 * @return the paymentStatuForGC
	 */
	public String getPaymentStatuForGC() {
		return mPaymentStatuForGC;
	}

	/**
	 * Sets the payment statu for gc.
	 *
	 * @param pPaymentStatuForGC the paymentStatuForGC to set
	 */
	public void setPaymentStatuForGC(String pPaymentStatuForGC) {
		mPaymentStatuForGC = pPaymentStatuForGC;
	}

	/**
	 * Gets the taxware down status.
	 *
	 * @return the taxwareDownStatus
	 */
	public String getTaxwareDownStatus() {
		return mTaxwareDownStatus;
	}

	/**
	 * Sets the taxware down status.
	 *
	 * @param pTaxwareDownStatus the taxwareDownStatus to set
	 */
	public void setTaxwareDownStatus(String pTaxwareDownStatus) {
		mTaxwareDownStatus = pTaxwareDownStatus;
	}
	/**
	 * Holds transactionTypePurchase.
	 */
	private String mTransactionTypePurchase;
	/**
	 * Holds transactionTypePurchaseScore.
	 */
	private String mTransactionTypePurchaseScore;
	
	/**
	 * Gets the transaction type purchase.
	 *
	 * @return the transactionTypePurchase
	 */
	public String getTransactionTypePurchase() {
		return mTransactionTypePurchase;
	}

	/**
	 * Sets the transaction type purchase.
	 *
	 * @param pTransactionTypePurchase the transactionTypePurchase to set
	 */
	public void setTransactionTypePurchase(String pTransactionTypePurchase) {
		mTransactionTypePurchase = pTransactionTypePurchase;
	}

	/**
	 * Gets the transaction type purchase score.
	 *
	 * @return the transactionTypePurchaseScore
	 */
	public String getTransactionTypePurchaseScore() {
		return mTransactionTypePurchaseScore;
	}

	/**
	 * Sets the transaction type purchase score.
	 *
	 * @param pTransactionTypePurchaseScore the transactionTypePurchaseScore to set
	 */
	public void setTransactionTypePurchaseScore(String pTransactionTypePurchaseScore) {
		mTransactionTypePurchaseScore = pTransactionTypePurchaseScore;
	}

	/**
	 * Gets the order details notes json element.
	 *
	 * @return the mOrderDetailsNotesJsonElement
	 */
	public String getOrderDetailsNotesJsonElement() {
		return mOrderDetailsNotesJsonElement;
	}

	/**
	 * Sets the order details notes json element.
	 *
	 * @param pOrderDetailsNotesJsonElement the mOrderDetailsNotesJsonElement to set
	 */
	public void setOrderDetailsNotesJsonElement(
			String pOrderDetailsNotesJsonElement) {
		this.mOrderDetailsNotesJsonElement = pOrderDetailsNotesJsonElement;
	}

	/**
	 * Gets the order details note json element.
	 *
	 * @return the mOrderDetailsNoteJsonElement
	 */
	public String getOrderDetailsNoteJsonElement() {
		return mOrderDetailsNoteJsonElement;
	}

	/**
	 * Sets the order details note json element.
	 *
	 * @param pOrderDetailsNoteJsonElement the mOrderDetailsNoteJsonElement to set
	 */
	public void setOrderDetailsNoteJsonElement(String pOrderDetailsNoteJsonElement) {
		this.mOrderDetailsNoteJsonElement = pOrderDetailsNoteJsonElement;
	}

	/**
	 * Gets the order details carton details json element.
	 *
	 * @return the mOrderDetailsCartonDetailsJsonElement
	 */
	public String getOrderDetailsCartonDetailsJsonElement() {
		return mOrderDetailsCartonDetailsJsonElement;
	}

	/**
	 * Sets the order details carton details json element.
	 *
	 * @param pOrderDetailsCartonDetailsJsonElement the mOrderDetailsCartonDetailsJsonElement to set
	 */
	public void setOrderDetailsCartonDetailsJsonElement(
			String pOrderDetailsCartonDetailsJsonElement) {
		this.mOrderDetailsCartonDetailsJsonElement = pOrderDetailsCartonDetailsJsonElement;
	}

	/**
	 * Gets the order details carton detail json element.
	 *
	 * @return the mOrderDetailsCartonDetailJsonElement
	 */
	public String getOrderDetailsCartonDetailJsonElement() {
		return mOrderDetailsCartonDetailJsonElement;
	}

	/**
	 * Sets the order details carton detail json element.
	 *
	 * @param pOrderDetailsCartonDetailJsonElement the mOrderDetailsCartonDetailJsonElement to set
	 */
	public void setOrderDetailsCartonDetailJsonElement(
			String pOrderDetailsCartonDetailJsonElement) {
		this.mOrderDetailsCartonDetailJsonElement = pOrderDetailsCartonDetailJsonElement;
	}

	/**
	 * Gets the order details do line nbr json element.
	 *
	 * @return the mOrderDetailsDoLineNbrJsonElement
	 */
	public String getOrderDetailsDoLineNbrJsonElement() {
		return mOrderDetailsDoLineNbrJsonElement;
	}

	/**
	 * Sets the order details do line nbr json element.
	 *
	 * @param pOrderDetailsDoLineNbrJsonElement the mOrderDetailsDoLineNbrJsonElement to set
	 */
	public void setOrderDetailsDoLineNbrJsonElement(
			String pOrderDetailsDoLineNbrJsonElement) {
		this.mOrderDetailsDoLineNbrJsonElement = pOrderDetailsDoLineNbrJsonElement;
	}

	/**
	 * Gets the order details do line items json element.
	 *
	 * @return the mOrderDetailsDoLineItemsJsonElement
	 */
	public String getOrderDetailsDoLineItemsJsonElement() {
		return mOrderDetailsDoLineItemsJsonElement;
	}

	/**
	 * Sets the order details do line items json element.
	 *
	 * @param pOrderDetailsDoLineItemsJsonElement the mOrderDetailsDoLineItemsJsonElement to set
	 */
	public void setOrderDetailsDoLineItemsJsonElement(
			String pOrderDetailsDoLineItemsJsonElement) {
		this.mOrderDetailsDoLineItemsJsonElement = pOrderDetailsDoLineItemsJsonElement;
	}

	/**
	 * Gets the order details do line item json element.
	 *
	 * @return the mOrderDetailsDoLineItemJsonElement
	 */
	public String getOrderDetailsDoLineItemJsonElement() {
		return mOrderDetailsDoLineItemJsonElement;
	}

	/**
	 * Sets the order details do line item json element.
	 *
	 * @param pOrderDetailsDoLineItemJsonElement the mOrderDetailsDoLineItemJsonElement to set
	 */
	public void setOrderDetailsDoLineItemJsonElement(
			String pOrderDetailsDoLineItemJsonElement) {
		this.mOrderDetailsDoLineItemJsonElement = pOrderDetailsDoLineItemJsonElement;
	}
	
	/**
	 * Holds shippingNoteDel.
	 */
	private String mShippingNoteDel;
	
	/**
	 * Holds noteTypeForTruck.
	 */
	private String mNoteTypeForTruck;
	

	/**
	 * @return the shippingNoteDel
	 */
	public String getShippingNoteDel() {
		return mShippingNoteDel;
	}

	/**
	 * @param pShippingNoteDel the shippingNoteDel to set
	 */
	public void setShippingNoteDel(String pShippingNoteDel) {
		mShippingNoteDel = pShippingNoteDel;
	}

	/**
	 * @return the noteTypeForTruck
	 */
	public String getNoteTypeForTruck() {
		return mNoteTypeForTruck;
	}

	/**
	 * @param pNoteTypeForTruck the noteTypeForTruck to set
	 */
	public void setNoteTypeForTruck(String pNoteTypeForTruck) {
		mNoteTypeForTruck = pNoteTypeForTruck;
	}
	/**
	 * Holds fulfillmentForDonation.
	 */
	private String mFulfillmentForDonation;
	/**
	 * Holds deliveryOptionForDonation.
	 */
	private String mDeliveryOptionForDonation;

	/**
	 * @return the fulfillmentForDonation
	 */
	public String getFulfillmentForDonation() {
		return mFulfillmentForDonation;
	}

	/**
	 * @param pFulfillmentForDonation the fulfillmentForDonation to set
	 */
	public void setFulfillmentForDonation(String pFulfillmentForDonation) {
		mFulfillmentForDonation = pFulfillmentForDonation;
	}

	/**
	 * @return the deliveryOptionForDonation
	 */
	public String getDeliveryOptionForDonation() {
		return mDeliveryOptionForDonation;
	}

	/**
	 * @param pDeliveryOptionForDonation the deliveryOptionForDonation to set
	 */
	public void setDeliveryOptionForDonation(String pDeliveryOptionForDonation) {
		mDeliveryOptionForDonation = pDeliveryOptionForDonation;
	}

	/**
	 * This method gets shipToFacilityForDonation value
	 *
	 * @return the shipToFacilityForDonation value
	 */
	public String getShipToFacilityForDonation() {
		return mShipToFacilityForDonation;
	}

	/**
	 * This method sets the shipToFacilityForDonation with pShipToFacilityForDonation value
	 *
	 * @param pShipToFacilityForDonation the shipToFacilityForDonation to set
	 */
	public void setShipToFacilityForDonation(String pShipToFacilityForDonation) {
		mShipToFacilityForDonation = pShipToFacilityForDonation;
	}

	/**
	 * This method gets OrderDetailsCartonCheckJsonElement value
	 *
	 * @return the OrderDetailsCartonCheckJsonElement value
	 */
	public String getOrderDetailsCartonCheckJsonElement() {
		return mOrderDetailsCartonCheckJsonElement;
	}

	/**
	 * This method sets the OrderDetailsCartonCheckJsonElement with pOrderDetailsCartonCheckJsonElement value
	 *
	 * @param pOrderDetailsCartonCheckJsonElement the OrderDetailsCartonCheckJsonElement to set
	 */
	public void setOrderDetailsCartonCheckJsonElement(
			String pOrderDetailsCartonCheckJsonElement) {
		this.mOrderDetailsCartonCheckJsonElement = pOrderDetailsCartonCheckJsonElement;
	}

	/**
	 * This method gets OrderDetailsIsCartonCheckJsonElement  value
	 *
	 * @return the OrderDetailsIsCartonCheckJsonElement  value
	 */
	public String getOrderDetailsIsCartonCheckJsonElement() {
		return mOrderDetailsIsCartonCheckJsonElement;
	}

	/**
	 * This method sets the OrderDetailsIsCartonCheckJsonElement  with pOrderDetailsIsCartonCheckJsonElement value
	 *
	 * @param pOrderDetailsIsCartonCheckJsonElement the OrderDetailsIsCartonCheckJsonElement  to set
	 */
	public void setOrderDetailsIsCartonCheckJsonElement(
			String pOrderDetailsIsCartonCheckJsonElement) {
		this.mOrderDetailsIsCartonCheckJsonElement = pOrderDetailsIsCartonCheckJsonElement;
	}

	/**
	 * This method gets OrderDetailsCustomAttributeListJsonElement value
	 *
	 * @return the OrderDetailsCustomAttributeListJsonElement value
	 */
	public String getOrderDetailsCustomAttributeListJsonElement() {
		return mOrderDetailsCustomAttributeListJsonElement;
	}

	/**
	 * This method sets the OrderDetailsCustomAttributeListJsonElement with pOrderDetailsCustomAttributeListJsonElement value
	 *
	 * @param pOrderDetailsCustomAttributeListJsonElement the OrderDetailsCustomAttributeListJsonElement to set
	 */
	public void setOrderDetailsCustomAttributeListJsonElement(
			String pOrderDetailsCustomAttributeListJsonElement) {
		this.mOrderDetailsCustomAttributeListJsonElement = pOrderDetailsCustomAttributeListJsonElement;
	}

	/**
	 * This method gets OrderDetailsNameJsonElement value
	 *
	 * @return the OrderDetailsNameJsonElement value
	 */
	public String getOrderDetailsNameJsonElement() {
		return mOrderDetailsNameJsonElement;
	}

	/**
	 * This method sets the OrderDetailsNameJsonElement with pOrderDetailsNameJsonElement value
	 *
	 * @param pOrderDetailsNameJsonElement the OrderDetailsNameJsonElement to set
	 */
	public void setOrderDetailsNameJsonElement(String pOrderDetailsNameJsonElement) {
		this.mOrderDetailsNameJsonElement = pOrderDetailsNameJsonElement;
	}

	/**
	 * This method gets OrderDetailsValueJsonElement value
	 *
	 * @return the OrderDetailsValueJsonElement value
	 */
	public String getOrderDetailsValueJsonElement() {
		return mOrderDetailsValueJsonElement;
	}

	/**
	 * This method sets the OrderDetailsValueJsonElement with pOrderDetailsValueJsonElement value
	 *
	 * @param pOrderDetailsValueJsonElement the OrderDetailsValueJsonElement to set
	 */
	public void setOrderDetailsValueJsonElement(
			String pOrderDetailsValueJsonElement) {
		this.mOrderDetailsValueJsonElement = pOrderDetailsValueJsonElement;
	}

	/**
	 * This method gets OrderDetailsPrimaryNamesJsonElement value
	 *
	 * @return the OrderDetailsPrimaryNamesJsonElement value
	 */
	public String getOrderDetailsPrimaryNamesJsonElement() {
		return mOrderDetailsPrimaryNamesJsonElement;
	}

	/**
	 * This method sets the OrderDetailsPrimaryNamesJsonElement with pOrderDetailsPrimaryNamesJsonElement value
	 *
	 * @param pOrderDetailsPrimaryNamesJsonElement the OrderDetailsPrimaryNamesJsonElement to set
	 */
	public void setOrderDetailsPrimaryNamesJsonElement(
			String pOrderDetailsPrimaryNamesJsonElement) {
		this.mOrderDetailsPrimaryNamesJsonElement = pOrderDetailsPrimaryNamesJsonElement;
	}

	/**
	 * This method gets OrderDetailsProxyNamesJsonElement value
	 *
	 * @return the OrderDetailsProxyNamesJsonElement value
	 */
	public String getOrderDetailsProxyNamesJsonElement() {
		return mOrderDetailsProxyNamesJsonElement;
	}

	/**
	 * This method sets the OrderDetailsProxyNamesJsonElement with pOrderDetailsProxyNamesJsonElement value
	 *
	 * @param pOrderDetailsProxyNamesJsonElement the OrderDetailsProxyNamesJsonElement to set
	 */
	public void setOrderDetailsProxyNamesJsonElement(
			String pOrderDetailsProxyNamesJsonElement) {
		this.mOrderDetailsProxyNamesJsonElement = pOrderDetailsProxyNamesJsonElement;
	}

	/**
	 * This method gets OrderDetailsShipToFacilityJsonElement value
	 *
	 * @return the OrderDetailsShipToFacilityJsonElement value
	 */
	public String getOrderDetailsShipToFacilityJsonElement() {
		return mOrderDetailsShipToFacilityJsonElement;
	}

	/**
	 * This method sets the OrderDetailsShipToFacilityJsonElement with pOrderDetailsShipToFacilityJsonElement value
	 *
	 * @param pOrderDetailsShipToFacilityJsonElement the OrderDetailsShipToFacilityJsonElement to set
	 */
	public void setOrderDetailsShipToFacilityJsonElement(
			String pOrderDetailsShipToFacilityJsonElement) {
		this.mOrderDetailsShipToFacilityJsonElement = pOrderDetailsShipToFacilityJsonElement;
	}
	
	/**
	 * Holds mRequestSendCountPropertyName.
	 * 
	 */
	private String mRequestSendCountPropertyName;

	/**
	 * Gets the RequestSendCountPropertyName.
	 *
	 * @return the requestSendCountPropertyName
	 */
	
	public String getRequestSendCountPropertyName() {
		return mRequestSendCountPropertyName;
	}
	
	/**
	 * Sets the requestSendCountPropertyName.
	 *
	 * @param pRequestSendCountPropertyName    requestSendCountPropertyName to set
	 */
	public void setRequestSendCountPropertyName(
			String pRequestSendCountPropertyName) {
		this.mRequestSendCountPropertyName = pRequestSendCountPropertyName;
	}

	/**
	 * This method gets orderHistoryOrderTypePropertyName value
	 *
	 * @return the orderHistoryOrderTypePropertyName value
	 */
	public String getOrderHistoryOrderTypePropertyName() {
		return mOrderHistoryOrderTypePropertyName;
	}

	/**
	 * This method sets the orderHistoryOrderTypePropertyName with pOrderHistoryOrderTypePropertyName value
	 *
	 * @param pOrderHistoryOrderTypePropertyName the orderHistoryOrderTypePropertyName to set
	 */
	public void setOrderHistoryOrderTypePropertyName(
			String pOrderHistoryOrderTypePropertyName) {
		mOrderHistoryOrderTypePropertyName = pOrderHistoryOrderTypePropertyName;
	}
	
}
