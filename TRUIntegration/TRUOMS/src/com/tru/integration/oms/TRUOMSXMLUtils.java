package com.tru.integration.oms;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingTools;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.payment.PaymentStatus;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.payment.TRUPaymentStatus;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.xml.customerorder.ObjectFactory;
import com.tru.xml.customerorder.TXML;
import com.tru.xml.customerorder.TXML.Message;
import com.tru.xml.customerorder.TXML.Message.Order;
import com.tru.xml.customerorder.TXML.Message.Order.CustomerInfo;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.AllocationInfo;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ChargeDetails.ChargeDetail;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.CustomFieldList;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.CustomFieldList.CustomField;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.DiscountDetails.DiscountDetail;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.LineReferenceFields;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.Notes;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.Notes.Note;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.PriceInfo;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.Quantity;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo.ShippingAddress;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.TaxDetails;
import com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.TaxDetails.TaxDetail;
import com.tru.xml.customerorder.TXML.Message.Order.PaymentDetails;
import com.tru.xml.customerorder.TXML.Message.Order.PaymentDetails.PaymentDetail;
import com.tru.xml.customerorder.TXML.Message.Order.PaymentDetails.PaymentDetail.BillToDetail;
import com.tru.xml.customerorder.TXML.Message.Order.PaymentDetails.PaymentDetail.PaymentTransactionDetails;
import com.tru.xml.customerorder.TXML.Message.Order.PaymentDetails.PaymentDetail.PaymentTransactionDetails.PaymentTransactionDetail;
import com.tru.xml.customerorder.TXML.Message.Order.ReferenceFields;



/**
 * This is utility class for XML OMS operations.
 */
public class TRUOMSXMLUtils extends TRUOMSUtils {

	/**
	 * This method will populate CustomerOrderObjectXML.
	 * @param pOrder Order
	 * @return customerOrderXML - customerOrderXML
	 */
	public String populateCustomerOrderObjectXML(TRUOrderImpl pOrder){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateCustomerOrderObjectXML]");
			vlogDebug("pOrder : {0}", pOrder);
		}
		String customerOrderXML = null;
		if(pOrder != null){
			// Added Performance statement 
			if(PerformanceMonitor.isEnabled()){
				PerformanceMonitor.startOperation(TRUOMSConstant.POPULATE_CUSTOMER_ORDER_OBJECT);
			}
			ObjectFactory obj = new ObjectFactory();
			TXML tXML = obj.createTXML();
			populateXMLHeaderParameters(obj,tXML,COI_SERVICE,pOrder.getId());//Populate header parameters for Order xml
			setTaxCounter(TRUConstants.INTEGER_NUMBER_ONE);
			// Calling method to populate the customer 1st name and last name.
			setCustomerFirstLastName(pOrder);
			populateXMLMessageParameters(obj,tXML,pOrder);
			// Calling method to generate the XML
			customerOrderXML = generateXML(tXML);
			// Setting back 1st name and last name as Null.
		    setFirstName(null);
		    setLastName(null);
		    setPhoneNumber(null);
		}
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(TRUOMSConstant.POPULATE_CUSTOMER_ORDER_OBJECT);
		}
		if (isLoggingDebug()) {
			vlogDebug("Customer Order XML : {0}", customerOrderXML);
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateCustomerOrderObjectXML]");
		}
		return customerOrderXML;
	}
	
	/**
	 * This method will populate Message parameters for Order xml.
	 * @param pObjFactory - ObjectFactory
	 * @param pTXML - tXML
	 * @param pOrder - Order
	 */
	private void populateXMLMessageParameters(ObjectFactory pObjFactory, TXML pTXML,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLMessageParameters]");
			vlogDebug("pObjFactory: {0} pTXML : {1} pOrder : {2}",pObjFactory,pTXML,pOrder);
		}
		if(pObjFactory == null || pTXML == null || pOrder == null){
			return;
		}
		Message messageXmlObj = pObjFactory.createTXMLMessage();
		Order orderXmlObj = pObjFactory.createTXMLMessageOrder();
		// Calling method to set the Order Header Parameter.
		populateOrderXMLParameters(pObjFactory,orderXmlObj,messageXmlObj,pOrder);
		// Calling method to set the Payment details.
		populateXMLPaymentDetails(pObjFactory,orderXmlObj,pOrder);
		// Calling method to set the Customer Info.
		populateXMLCustomerInfo(pObjFactory,orderXmlObj,pOrder);
		// Calling method to set the Order Reference Fields.
		populateXMLOrderRefFields(pObjFactory,orderXmlObj,pOrder);
		// Calling method to set the Order Line.
		populateXMLOrderLines(pObjFactory,orderXmlObj,pOrder);
		messageXmlObj.setOrder(orderXmlObj);
		pTXML.setMessage(messageXmlObj);
		// Setting back the PayPal and Credit card flag to false.
		setPaypalPG(Boolean.FALSE);
		setCreditCardPG(Boolean.FALSE);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLMessageParameters]");
		}
	}
	
	/**
	 * This method will populate order xml message parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pMessageXmlObj - Message
	 * @param pOrder - Order
	 */
	private void populateOrderXMLParameters(ObjectFactory pObjFactory, Order pOrderXmlObj,
			Message pMessageXmlObj, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderXMLParameters]");
			vlogDebug("pObjFactory: {0} pOrderXmlObj : {1}",pObjFactory,pOrderXmlObj);
			vlogDebug("pMessageXmlObj: {0} pOrder : {1}",pMessageXmlObj,pOrder);
		}
		if(pOrder == null || pObjFactory == null || pOrderXmlObj == null){
			return;
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		String onHoldStatus = null;
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(configuration.getEstTimeZone()));
		pOrderXmlObj.setOrderNumber(pObjFactory.createTXMLMessageOrderOrderNumber(pOrder.getId()));
		if(pOrder.getSubmittedDate() != null){
			pOrderXmlObj.setOrderCaptureDate(pObjFactory.createTXMLMessageOrderOrderCaptureDate(df.format(pOrder.getSubmittedDate())));
		}
		if(isPayInStoreOrder(pOrder)){
			pOrderXmlObj.setOrderType(pObjFactory.createTXMLMessageOrderOrderType(configuration.getOrderTypeMap().get(ORDER_TYPE_PAYINSTORE_KEY)));
		}else{
			pOrderXmlObj.setOrderType(pObjFactory.createTXMLMessageOrderOrderType(configuration.getOrderTypeMap().get(ORDER_TYPE_WEB_KEY)));
		}
		pOrderXmlObj.setOrderSubtotal(pObjFactory.createTXMLMessageOrderOrderSubtotal(String.valueOf(pricingTools.round(getOrderSubTotal(pOrder)))));
		//pOrderXmlObj.setOrderTotal(pObjFactory.createTXMLMessageOrderOrderTotal(String.valueOf(pricingTools.round(pOrder.getPriceInfo().getTotal()))));
		if(StringUtils.isNotBlank(pOrder.getSourceOfOrder())){
			pOrderXmlObj.setEntryType(pObjFactory.createTXMLMessageOrderEntryType(pOrder.getSourceOfOrder()));
		}else{
			pOrderXmlObj.setEntryType(pObjFactory.createTXMLMessageOrderEntryType(configuration.getOrderEntryTypeWeb()));
		}
		pOrderXmlObj.setConfirmed(pObjFactory.createTXMLMessageOrderConfirmed(TRUE));
		pOrderXmlObj.setCanceled(pObjFactory.createTXMLMessageOrderCanceled(FALSE));
		pOrderXmlObj.setMinimizeShipments(pObjFactory.createTXMLMessageOrderMinimizeShipments(TRUE));
		if(isOnlyGiftCardPG(pOrder)){
			onHoldStatus = String.valueOf(getFraudStatus(pOrder));
			pOrderXmlObj.setOnHold(pObjFactory.createTXMLMessageOrderOnHold(onHoldStatus));
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getPaymentStatuForGC()));
		}else if(isPayInStoreOrder(pOrder)){
			pOrderXmlObj.setOnHold(pObjFactory.createTXMLMessageOrderOnHold(TRUE));
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getOrderStatusForPIS()));
		}else{
			onHoldStatus = String.valueOf(getFraudStatus(pOrder));
			pOrderXmlObj.setOnHold(pObjFactory.createTXMLMessageOrderOnHold(onHoldStatus));
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getOrderPaymentStatus()));
		}
		if(isTaxWareDown(pOrder)){
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getTaxwareDownStatus()));
		}
		if(StringUtils.isNotBlank(pOrder.getEnteredBy())){
			pOrderXmlObj.setEnteredBy(pObjFactory.createTXMLMessageOrderEnteredBy(pOrder.getEnteredBy()));
		}
		if(StringUtils.isNotBlank(pOrder.getEnteredLocation())){
			pOrderXmlObj.setEnteredLocation(pObjFactory.createTXMLMessageOrderEnteredLocation(pOrder.getEnteredLocation()));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderXMLParameters]");
		}
	}
	
	/**
	 * This method will populate payment detail xml parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order XML Object
	 * @param pOrder - TRUOrderImpl Object
	 */
	@SuppressWarnings("unchecked")
	private void populateXMLPaymentDetails(ObjectFactory pObjFactory,Order pOrderXmlObj, atg.commerce.order.Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populatePaymentDetails]");
			vlogDebug("pObjFactory: {0} pOrder : {1} pOrderXmlObj:{2}",pObjFactory,pOrder,pOrderXmlObj);
		}
		if(pOrder == null || pObjFactory == null || pOrderXmlObj == null){
			return;
		}
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		if(paymentGroups == null || paymentGroups.isEmpty()){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		TRUCreditCard creditCardPg = null;
		TRUPayPal payPal = null;
		InStorePayment inStorePayment = null;
		ContactInfo billingAddress = null;
		String paymentGroupType = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		String sourceOfOrder = null;
		if(pOrder instanceof TRUOrderImpl){
			sourceOfOrder = ((TRUOrderImpl)pOrder).getSourceOfOrder();
		}else if(pOrder instanceof TRULayawayOrderImpl){
			sourceOfOrder = ((TRULayawayOrderImpl)pOrder).getSourceOfOrder();
		}
		TRUConfiguration truConfiguration = getTruConfiguration();
		PaymentDetails paymentDetails = null;
		paymentDetails = pObjFactory.createTXMLMessageOrderPaymentDetails();
		List<PaymentDetail> paymentDetailList = paymentDetails.getPaymentDetail();
		PaymentDetail paymentDetail = null;
		for(PaymentGroup paymentGroup : paymentGroups){
			if (isLoggingDebug()) {
				vlogDebug("Payment Group: {0}",paymentGroup);
			}
			if(paymentGroup instanceof InStorePayment){
				continue;
			}
			paymentDetail = pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetail();
			paymentDetail.setExternalPaymentDetailId(paymentGroup.getId());
			if(pOrder instanceof TRULayawayOrderImpl){
				paymentDetail.setReqAuthorizationAmount(BigDecimal.valueOf(pricingTools.round(paymentGroup.getAmount())));
			}else{
				paymentDetail.setReqAuthorizationAmount(BigDecimal.valueOf(pricingTools.round(paymentGroup.getAmountAuthorized())));
			}
			paymentDetail.setChargeSequence(BigInteger.valueOf(paymentGroups.indexOf(paymentGroup)+TRUConstants.INTEGER_NUMBER_ONE));
			if(isPayInStoreOrder(pOrder)){
				paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
						omsConfiguration.getPayInStotePaymentEntryType()));
			}else if(StringUtils.isNotBlank(sourceOfOrder)){
				if(truConfiguration.getWebSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
					paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
							omsConfiguration.getWebPaymentEntryType()));
				}
				if(truConfiguration.getMobileSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
					paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
							omsConfiguration.getMobilePaymentEntryType()));
				}
				if(truConfiguration.getSosSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
					paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
							omsConfiguration.getSosPaymentEntryType()));
				}
				if(truConfiguration.getCsrSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
					paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
							omsConfiguration.getCsrPaymentEntryType()));
				}
			}else{
				paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType(
						omsConfiguration.getPaymentEntryType()));
			}
			if(paymentGroup instanceof TRUCreditCard){
				// Calling method to set the Credit card payment details.
				setCreditCardPaymentDetails(paymentGroup,pObjFactory,paymentDetail);
				creditCardPg = (TRUCreditCard) paymentGroup;
				billingAddress = (ContactInfo) creditCardPg.getBillingAddress();
				if (isLoggingDebug()) {
					vlogDebug("credit card billingAddress: {0}",billingAddress);
				}
				pupulateXMLBillingDetails(billingAddress,paymentDetail,pOrder,pObjFactory);
			}else if(paymentGroup instanceof TRUGiftCard){
				// Calling method to set the Gift card payment details.
				setGiftCardPaymentDetails(paymentGroup,pObjFactory,paymentDetail,pOrder);
				pupulateXMLBillingDetails(billingAddress,paymentDetail,pOrder,pObjFactory);
			}else if(paymentGroup instanceof TRUPayPal){
				// Calling method to set the paypal payment details.
				setPaypalPaymentDetails(paymentGroup,pObjFactory,paymentDetail);
				payPal = (TRUPayPal) paymentGroup;
				billingAddress = (ContactInfo) payPal.getBillingAddress();
				if (isLoggingDebug()) {
					vlogDebug("credit card billingAddress: {0}",billingAddress);
				}
				pupulateXMLBillingDetails(billingAddress,paymentDetail,pOrder,pObjFactory);
			}else if(paymentGroup instanceof InStorePayment){
				inStorePayment = (InStorePayment) paymentGroup;
				paymentGroupType = (String) inStorePayment.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
				paymentDetail.setPaymentEntryType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentEntryType
						(omsConfiguration.getPayInStotePaymentEntryType()));
				paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
				pupulateXMLBillingDetails(billingAddress,paymentDetail,pOrder,pObjFactory);
			}
			PaymentTransactionDetails paymentTransactionDetails = pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetails();
			// Calling method to set the Payment transaction details
			if(paymentGroup instanceof TRUGiftCard){
				// Calling method to set the Payment transaction details
				populateXMLPaymentTransactionDetails(paymentGroup,pObjFactory,paymentTransactionDetails,Boolean.TRUE);
			}else{
				populateXMLPaymentTransactionDetails(paymentGroup,pObjFactory,paymentTransactionDetails,Boolean.FALSE);
			}
			paymentDetail.setPaymentTransactionDetails(paymentTransactionDetails);
			paymentDetailList.add(paymentDetail);
		}
		pOrderXmlObj.setPaymentDetails(paymentDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLPaymentDetails]");
		}
	}
	
	/**
	 * This method will set the PayPay Payment details.
	 * @param pPaymentGroup - PaymentGroup
	 * @param pObjFactory - ObjectFactory
	 * @param pPaymentDetail - PaymentDetail
	 */
	private void setPaypalPaymentDetails(PaymentGroup pPaymentGroup,
			ObjectFactory pObjFactory, PaymentDetail pPaymentDetail) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setPaypalPaymentDetails]");
			vlogDebug("pPaymentGroup: {0} pObjFactory : {1} pPaymentDetail:{2}",pPaymentGroup,pObjFactory,pPaymentDetail);
		}
		if(pPaymentGroup == null || pPaymentDetail == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		setPaypalPG(Boolean.TRUE);
		TRUPayPal payPal = (TRUPayPal) pPaymentGroup;
		String paymentGroupType = (String) payPal.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
		pPaymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: setPaypalPaymentDetails]");
		}
	}

	/**
	 * This method will set the Gift Card Payment details.
	 * @param pPaymentGroup - PaymentGroup
	 * @param pObjFactory - ObjectFactory
	 * @param pPaymentDetail - PaymentDetail
	 * @param pOrder - Order Object
	 */
	private void setGiftCardPaymentDetails(PaymentGroup pPaymentGroup,
			ObjectFactory pObjFactory, PaymentDetail pPaymentDetail, atg.commerce.order.Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setGiftCardPaymentDetails]");
			vlogDebug("pPaymentGroup: {0} pObjFactory : {1} pPaymentDetail:{2}",pPaymentGroup,pObjFactory,pPaymentDetail);
		}
		if(pPaymentGroup == null || pPaymentDetail == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		PricingTools pricingTools = getOrderManager().getPromotionTools().getPricingTools();
		TRUGiftCard giftCard = (TRUGiftCard) pPaymentGroup;
		String paymentGroupType = (String) giftCard.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
		pPaymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
		pPaymentDetail.setAccountNumber(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailAccountNumber(
				giftCard.getGiftCardNumber()));
		String giftCardNumber = giftCard.getGiftCardNumber();
		if(StringUtils.isNotBlank(giftCardNumber)){
			int length = giftCardNumber.length();
			String accountDisplayNumber = giftCardNumber.substring(length-TRUConstants.FOUR, length);
			pPaymentDetail.setAccountDisplayNumber(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailAccountDisplayNumber(
					accountDisplayNumber));
		}
		if(omsConfiguration.isGiftCardSecurityCode()){
			pPaymentDetail.setSecurityCode(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailSecurityCode(giftCard.getGiftCardPin()));
		}else{
			pPaymentDetail.setSecurityCode(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailSecurityCode(omsConfiguration.getSecurityCode()));
		}
		if(pOrder instanceof TRULayawayOrderImpl){
			pPaymentDetail.setReqSettlementAmount(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailReqSettlementAmount(
					String.valueOf(pricingTools.round(pPaymentGroup.getAmount()))));
		}else{
			pPaymentDetail.setReqSettlementAmount(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailReqSettlementAmount(
					String.valueOf(pricingTools.round(pPaymentGroup.getAmountAuthorized()))));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: setGiftCardPaymentDetails]");
		}
	}

	/**
	 * This method will set the Credit Card Payment details.
	 * @param pPaymentGroup - PaymentGroup
	 * @param pObjFactory - ObjectFactory
	 * @param pPaymentDetail - PaymentDetail
	 */
	private void setCreditCardPaymentDetails(PaymentGroup pPaymentGroup,
			ObjectFactory pObjFactory, PaymentDetail pPaymentDetail) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setCreditCardPaymentDetails]");
			vlogDebug("pPaymentGroup: {0} pObjFactory : {1} pPaymentDetail:{2}",pPaymentGroup,pObjFactory,pPaymentDetail);
		}
		if(pPaymentGroup == null || pPaymentDetail == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		setCreditCardPG(Boolean.TRUE);
		TRUCreditCard creditCardPg = (TRUCreditCard) pPaymentGroup;
		String paymentGroupType = (String) creditCardPg.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
		pPaymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
		pPaymentDetail.setCardType(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailCardType(
				omsConfiguration.getCreditCardTypeMap().get(creditCardPg.getCreditCardType())));
		pPaymentDetail.setAccountNumber(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailAccountNumber(
				creditCardPg.getCreditCardNumber()));
		String creditCardNumber = creditCardPg.getCreditCardNumber();
		if(StringUtils.isNotBlank(creditCardNumber)){
			int length = creditCardNumber.length();
			String accountDisplayNumber = creditCardNumber.substring(length-TRUConstants.FOUR, length);
			pPaymentDetail.setAccountDisplayNumber(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailAccountDisplayNumber(
					accountDisplayNumber));
		}
		pPaymentDetail.setNameAsOnCard(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailNameAsOnCard(
				creditCardPg.getNameOnCard()));
		pPaymentDetail.setCardExpiryMonth(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailCardExpiryMonth(
				creditCardPg.getExpirationMonth()));
		pPaymentDetail.setCardExpiryYear(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailCardExpiryYear(
				creditCardPg.getExpirationYear()));
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: setCreditCardPaymentDetails]");
		}
	}
	
	/**
	 * This method will populate the Billing Details XML Parameters.
	 * @param pBillingAddress - ContactInfo
	 * @param pPaymentDetail - pPaymentDetail
	 * @param pOrder - Order
	 * @param pObjFactory - ObjectFactory
	 */
	private void pupulateXMLBillingDetails(ContactInfo pBillingAddress,
			PaymentDetail pPaymentDetail, atg.commerce.order.Order pOrder, ObjectFactory pObjFactory) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: pupulateXMLBillingDetails]");
			vlogDebug("pBillingAddress: {0} pPaymentDetail : {1}",pBillingAddress,pPaymentDetail);
			vlogDebug("pOrder: {0} pObjFactory : {1}",pOrder,pObjFactory);
		}
		if(pPaymentDetail == null|| pObjFactory == null){
			return;
		}
		BillToDetail billToDetail = pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetail();
		TRULayawayOrderImpl layawayOrderImpl = null;
		if(pOrder instanceof TRUOrderImpl){
			if(pBillingAddress != null){
				billToDetail.setBillToFirstName(pBillingAddress.getFirstName());
				billToDetail.setBillToLastName(pBillingAddress.getLastName());
				billToDetail.setBillToAddressLine1(pBillingAddress.getAddress1());
				if(StringUtils.isNotBlank(pBillingAddress.getAddress2())){
					billToDetail.setBillToAddressLine2(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToAddressLine2(
							pBillingAddress.getAddress2()));
				}
				billToDetail.setBillToCity(pBillingAddress.getCity());
				billToDetail.setBillToState(pBillingAddress.getState());
				billToDetail.setBillToPostalCode(pBillingAddress.getPostalCode());
				String country = getOmsConfiguration().getCountryCodeMap().get(pBillingAddress.getCountry());
				if(StringUtils.isBlank(country)){
					billToDetail.setBillToCountry(pBillingAddress.getCountry());
				}else{
					billToDetail.setBillToCountry(country);
				}
				billToDetail.setBillToPhone(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToPhone
						(formatPhoneNumer(pBillingAddress.getPhoneNumber())));
			}
			billToDetail.setBillToEmail(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToEmail
					(((TRUOrderImpl)pOrder).getEmail()));
		}else if(pOrder instanceof TRULayawayOrderImpl){
			layawayOrderImpl = (TRULayawayOrderImpl) pOrder;
			billToDetail.setBillToFirstName(layawayOrderImpl.getFirstName());
			billToDetail.setBillToLastName(layawayOrderImpl.getLastName());
			billToDetail.setBillToAddressLine1(layawayOrderImpl.getAddress1());
			if(StringUtils.isNotBlank(layawayOrderImpl.getAddress2())){
				billToDetail.setBillToAddressLine2(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToAddressLine2(
						layawayOrderImpl.getAddress2()));
			}
			billToDetail.setBillToCity(layawayOrderImpl.getCity());
			billToDetail.setBillToState(layawayOrderImpl.getAddrState());
			billToDetail.setBillToPostalCode(layawayOrderImpl.getPostalCode());
			String country = getOmsConfiguration().getCountryCodeMap().get(layawayOrderImpl.getCountry());
			if(StringUtils.isBlank(country)){
				billToDetail.setBillToCountry(layawayOrderImpl.getCountry());
			}else{
				billToDetail.setBillToCountry(country);
			}
			billToDetail.setBillToPhone(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToPhone
					(formatPhoneNumer(layawayOrderImpl.getPhoneNumber())));
			billToDetail.setBillToEmail(pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailBillToDetailBillToEmail
					(layawayOrderImpl.getEmail()));
		}
		pPaymentDetail.setBillToDetail(billToDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: pupulateXMLBillingDetails]");
		}
	}

	/**
	 * This method will populate the Payment Transaction Detail XML Parameters.
	 * @param pPaymentGroup - PaymentGroup
	 * @param pObjFactory - ObjectFactory
	 * @param pPaymentTransactionDetails - PaymentTransactionDetails
	 * @param pIsSettlementTrnasType - Boolean
	 */
	@SuppressWarnings("unchecked")
	private void populateXMLPaymentTransactionDetails(PaymentGroup pPaymentGroup,
			ObjectFactory pObjFactory, PaymentTransactionDetails pPaymentTransactionDetails, Boolean pIsSettlementTrnasType) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateXMLPaymentTransactionDetails]");
			vlogDebug("pPaymentGroup: {0} pPaymentTransactionDetails : {1}",pPaymentGroup,pPaymentTransactionDetails);
			vlogDebug("pIsSettlementTrnasType: {0} pObjFactory : {1}",pIsSettlementTrnasType,pObjFactory);
		}
		if(pPaymentGroup == null || pObjFactory == null){
			return;
		}
		List<PaymentStatus> listOfPaymentStatus = pPaymentGroup.getAuthorizationStatus();
		if (isLoggingDebug()) {
			vlogDebug("listOfCreditStatus: {0}",listOfPaymentStatus);
		}
		if(listOfPaymentStatus == null || listOfPaymentStatus.isEmpty()){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		DateFormat df1 = new SimpleDateFormat(DATE_FORMAT1,Locale.US);
		DateFormat estDateFormat = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		estDateFormat.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		TRUPayPal payPal = null;
		TRUCreditCard creditCard = null;
		Date transactionExpDate = null;
		TRUPaymentStatus truPaymentStatus = null;
		PaymentTransactionDetail paymentTransactionDetail = null;
		List<PaymentTransactionDetail> listOfPaymentTransactionDetail = pPaymentTransactionDetails.getPaymentTransactionDetail();
		for(PaymentStatus lPaymentStatus : listOfPaymentStatus){
			if(!lPaymentStatus.getTransactionSuccess()){
				continue;
			}
			if(isFraudValidationReq(pPaymentGroup, lPaymentStatus) && !((TRUPaymentStatus)lPaymentStatus).isFraud()){
				continue;
			}
			paymentTransactionDetail = pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetail();
			paymentTransactionDetail.setExternalPaymentTransactionId(lPaymentStatus.getTransactionId());
			if(pIsSettlementTrnasType){
				paymentTransactionDetail.setTransactionType(omsConfiguration.getSettlementTransactionType());
			}else{
				paymentTransactionDetail.setTransactionType(omsConfiguration.getAuthorizationTransactionType());
			}
			paymentTransactionDetail.setRequestedAmount(BigDecimal.valueOf(pPaymentGroup.getAmount()));
			if(lPaymentStatus.getTransactionTimestamp() != null){
				paymentTransactionDetail.setRequestedDTTM(estDateFormat.format(lPaymentStatus.getTransactionTimestamp()));
			}
			paymentTransactionDetail.setProcessedAmount(
					pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailProcessedAmount(
					String.valueOf(lPaymentStatus.getAmount()/TRUConstants.HUNDERED)));
			
			if(lPaymentStatus instanceof TRUPaymentStatus){
				truPaymentStatus = (TRUPaymentStatus) lPaymentStatus;
				paymentTransactionDetail.setFollowOnId(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailFollowOnId(
								null));
				paymentTransactionDetail.setFollowOnToken(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailFollowOnToken(
								null));
				paymentTransactionDetail.setRequestId(null);
				if(truPaymentStatus.getTransactionDTTM() != null){
					paymentTransactionDetail.setTransactionDTTM(estDateFormat.format(truPaymentStatus.getTransactionDTTM()));
					transactionExpDate = getTransactionExpDate(truPaymentStatus.getTransactionDTTM());
					if(transactionExpDate != null){
						paymentTransactionDetail.setTransactionExpiryDate(df1.format(transactionExpDate));
					}
				}
			}
			paymentTransactionDetail.setTransactionDecision(omsConfiguration.getTransactionDecisionSuccess());
			if(pPaymentGroup instanceof TRUCreditCard){
				creditCard = (TRUCreditCard) pPaymentGroup;
				paymentTransactionDetail.setRequestToken(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailRequestToken(
								creditCard.getCreditCardNumber()));
				if(StringUtils.isNotBlank(creditCard.getEciFlag())){
					paymentTransactionDetail.setECIFLAG(
							pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailECIFLAG(
									creditCard.getEciFlag()));
				}
				if(StringUtils.isNotBlank(creditCard.getCavv())){
					paymentTransactionDetail.setCAVV(
							pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailCAVV(
							creditCard.getCavv()));
				}
				if(StringUtils.isNotBlank(creditCard.getXid())){
					paymentTransactionDetail.setXID(
							pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailXID(
							creditCard.getXid()));
				}
			}
			if(pPaymentGroup instanceof TRUPayPal){
				payPal = (TRUPayPal) pPaymentGroup;
				paymentTransactionDetail.setRequestToken(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailRequestToken(
								payPal.getToken()));
				paymentTransactionDetail.setPayerID(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailPayerID(
								payPal.getPayerId()));
				paymentTransactionDetail.setPaypalTransactionId(
						pObjFactory.createTXMLMessageOrderPaymentDetailsPaymentDetailPaymentTransactionDetailsPaymentTransactionDetailPaypalTransactionId(
								payPal.getPayPalTransactionId()));
			}
			
			listOfPaymentTransactionDetail.add(paymentTransactionDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLPaymentTransactionDetails]");
		}
	}
	
	/**
	 * This method will populate the Customer Info XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateXMLCustomerInfo(ObjectFactory pObjFactory,Order pOrderXmlObj, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLCustomerInfo]");
			vlogDebug("pOrderXmlObj: {0} pObjFactory : {1}",pOrderXmlObj,pObjFactory);
			vlogDebug("pOrder : {0}",pOrder);
		}
		if(pObjFactory == null || pOrder == null){
			return;
		}
		RepositoryItem profile;
		try {
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pOrder);
			TRUPropertyManager propertyManager = getPropertyManager();
			List<PaymentDetail> paymentDetails = null;
			if(pOrderXmlObj != null && pOrderXmlObj.getPaymentDetails() != null){
				paymentDetails = pOrderXmlObj.getPaymentDetails().getPaymentDetail();
			}
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			BillToDetail billToDetails = null;
			if(paymentDetails != null && !paymentDetails.isEmpty()){
				if(isCreditCardPG()){
					billToDetails = getPaymentDetailBillingInfo(paymentDetails,omsConfiguration.getCreditCardMethod());
				}else if(isPaypalPG()){
					billToDetails = getPaymentDetailBillingInfo(paymentDetails,omsConfiguration.getPayPalMethod());
				}else{
					billToDetails = paymentDetails.get(TRUConstants.ZERO).getBillToDetail();
				}
			}
			CustomerInfo customerInfo = pObjFactory.createTXMLMessageOrderCustomerInfo();
			if(profile != null && !profile.isTransient()){
				customerInfo.setCustomerId(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerId(pOrder.getProfileId()));
				customerInfo.setCustomerUserId(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerUserId(
						(String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName())));
				customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
						(String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName())));
				customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
						(String) profile.getPropertyValue(propertyManager.getLastNamePropertyName())));
				if(billToDetails != null && billToDetails.getBillToPhone() != null){
					customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerPhone(
							formatPhoneNumer(billToDetails.getBillToPhone().getValue())));
				}
				customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
						(String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName())));
			}else{
				if(billToDetails != null){
					customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
							getFirstName()));
					customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
							getLastName()));
					if(billToDetails.getBillToPhone() != null){
						customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
								formatPhoneNumer(billToDetails.getBillToPhone().getValue())));
					}
					if(billToDetails.getBillToEmail() != null){
						customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
								billToDetails.getBillToEmail().getValue()));
					}else{
						customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
								pOrder.getEmail()));
					}
					
				}else{
					// Calling method to set the customer info details from Shipping Group Address.
					setBillingInfoFromShippingGroup(pObjFactory,customerInfo,pOrder);
				}
			}
			if(customerInfo != null){
				if(customerInfo.getCustomerFirstName() == null || StringUtils.isBlank(customerInfo.getCustomerFirstName().getValue())){
					customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
							getFirstName()));
				}
				if(customerInfo.getCustomerLastName() == null || StringUtils.isBlank(customerInfo.getCustomerLastName().getValue())){
					customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
							getLastName()));
				}
				if(getPhoneNumber() != null && (customerInfo.getCustomerPhone() == null || 
						StringUtils.isBlank(customerInfo.getCustomerPhone().getValue()))){
					customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerPhone(
							formatPhoneNumer(getPhoneNumber())));
				}
				if(customerInfo.getCustomerEmail() == null || StringUtils.isBlank(customerInfo.getCustomerEmail().getValue())){
					customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
							pOrder.getEmail()));
				}
				pOrderXmlObj.setCustomerInfo(customerInfo);
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLCustomerInfo]");
		}
	}

	/**
	 * This method is use to get the BillToDetail Object from PayPay/Credit Card PaymentDetail.
	 * @param pPaymentDetails - List of PaymentDetail
	 * @param pPaymentMethod - String : Payment Method
	 * @return billToDetail - BillToDetail Object
	 */
	private BillToDetail getPaymentDetailBillingInfo(List<PaymentDetail> pPaymentDetails, String pPaymentMethod) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getPaymentDetailBillingInfo]");
			vlogDebug("pPaymentDetails: {0} pPaymentMethod : {1}",pPaymentDetails,pPaymentMethod);
		}
		BillToDetail billToDetail = null;
		if(pPaymentDetails == null || pPaymentDetails.isEmpty()){
			return billToDetail;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		for(PaymentDetail lPaymentDetail : pPaymentDetails){
			if(StringUtils.isNotBlank(pPaymentMethod) && pPaymentMethod.equals(omsConfiguration.getCreditCardMethod())){
				billToDetail = lPaymentDetail.getBillToDetail();
				break;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getPaymentDetailBillingInfo]");
			vlogDebug("billToDetail: {0}",billToDetail);
		}
		return billToDetail;
	}
	
	/**
	 * This method will populate the Customer Info from Shipping Group Address.
	 * @param pObjFactory - ObjectFactory
	 * @param pCustomerInfo - CustomerInfo
	 * @param pOrder - TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	private void setBillingInfoFromShippingGroup(ObjectFactory pObjFactory,
			CustomerInfo pCustomerInfo, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: setBillingInfoFromShippingGroup]");
			vlogDebug("pObjFactory: {0} pCustomerInfo : {1} pOrder:{2}",pObjFactory,pCustomerInfo,pOrder);
		}
		if(pCustomerInfo == null || pOrder == null){
			return;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(shippingGroups == null || shippingGroups.isEmpty()){
			return;
		}
		TRUHardgoodShippingGroup truHGSH = null;
		TRUInStorePickupShippingGroup truINPSG=null;
		Address shippingAddress = null;
		for(ShippingGroup lShipGrp : shippingGroups){
			if(lShipGrp instanceof TRUHardgoodShippingGroup){
				truHGSH = (TRUHardgoodShippingGroup) lShipGrp;
				shippingAddress = truHGSH.getShippingAddress();
				break;
			}
			if(lShipGrp instanceof TRUInStorePickupShippingGroup){
				truINPSG = (TRUInStorePickupShippingGroup) lShipGrp;
				break;
			}
		}
		if(shippingAddress != null){
			pCustomerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
					getFirstName()));
			pCustomerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
					getLastName()));
			if(shippingAddress instanceof ContactInfo && ((ContactInfo)shippingAddress).getPhoneNumber() != null){
				pCustomerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerPhone(
						formatPhoneNumer(((ContactInfo)shippingAddress).getPhoneNumber())));
			}
			pCustomerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(pOrder.getEmail()));
		}else if(truINPSG != null){
			pCustomerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
					getFirstName()));
			pCustomerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
					getLastName()));
			pCustomerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerPhone(
					formatPhoneNumer(truINPSG.getPhoneNumber())));
			pCustomerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(truINPSG.getEmail()));
		}else{
			pCustomerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(pOrder.getEmail()));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: setBillingInfoFromShippingGroup]");
		}
	}
	
	/**
	 * This method will populate the Order Reference Fields XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateXMLOrderRefFields(ObjectFactory pObjFactory,
			Order pOrderXmlObj, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLOrderRefFields]");
			vlogDebug("pObjFactory: {0} pOrderXmlObj : {1} pOrder:{2}",pObjFactory,pOrderXmlObj,pOrder);
		}
		if(pObjFactory == null || pOrderXmlObj == null || pOrder == null){
			return;
		}
		ReferenceFields orderReferenceFiled = pObjFactory.createTXMLMessageOrderReferenceFields();
		String siteId = pOrder.getSiteId();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(!StringUtils.isBlank(siteId)){
			if(siteId.contains(omsConfiguration.getTruSiteId())){
				orderReferenceFiled.setReferenceField4(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField4
						(omsConfiguration.getOrderRefField4ForTRU()));
			}else if(siteId.contains(omsConfiguration.getBruSiteId())){
				orderReferenceFiled.setReferenceField4(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField4
						(omsConfiguration.getOrderRefField4ForBRU()));
			}
		}
		if(pOrder.isFinanceAgreed()){
			orderReferenceFiled.setReferenceField7(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField7
					(Integer.toString(pOrder.getFinancingTermsAvailed())));
		}
		String rewardNumber = pOrder.getRewardNumber();
		if(!StringUtils.isBlank(rewardNumber)){
			orderReferenceFiled.setReferenceField10(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField10(rewardNumber));
		}
		pOrderXmlObj.setReferenceFields(orderReferenceFiled);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLOrderRefFields]");
		}
	}

	/**
	 * This method will populate the Order Line XML Parameters.
	 * @param pObjectFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pOrder - TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	private void populateXMLOrderLines(ObjectFactory pObjectFactory, Order pOrderXmlObj, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLOrderLines]");
			vlogDebug("pOrder : {0} pObjectFactory : {1} pOrderXmlObj : {2}", pOrder,pObjectFactory,pOrderXmlObj);
		}
		if(pOrder == null || pObjectFactory == null || pOrderXmlObj == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(isLoggingDebug()){
			vlogDebug("shippingGroups: {0} avaibale in Order : {1}", shippingGroups,pOrder);
		}
		if(shippingGroups == null || shippingGroups.isEmpty()){
			return;
		}
		OrderLines orderLines = pObjectFactory.createTXMLMessageOrderOrderLines();
		List<OrderLine> listOfOrderLine = orderLines.getOrderLine();
		int orderLineCount = TRUConstants.INTEGER_NUMBER_ONE;
		setCommItemAndCommQtyMap(pOrder);// Calling method to set the Commerce Item and qty in Variables.
		setCommerceItemQtyForMulItemDis(pOrder);// Calling method to set the Commerce Item and qty in Variables.
		setLastShipGroup(Boolean.FALSE);
		setOrderDisAndAmountMap(null);
		for(ShippingGroup lShipGroup : shippingGroups){
			List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = lShipGroup.getCommerceItemRelationships();
			if(commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
				continue;
			}
			if(shippingGroups.indexOf(lShipGroup)+TRUConstants.INTEGER_NUMBER_ONE == shippingGroups.size()){
				setLastShipGroup(Boolean.TRUE);
			}
			List<TRUShippingGroupCommerceItemRelationship> newListOfRel = 
					removeGiftWrapRelationships(commerceItemRelationships);//Calling method to remove the GiftWrap relationship from the All relationship
			if(newListOfRel == null || newListOfRel.isEmpty()){
				continue;
			}
			setCommItemRelAmount(getCommerceItemRelAmount(lShipGroup));//Setting the amount of commerce item by amountByAverage
			setRelAndRelQtyForProration(lShipGroup,pOrder);//Setting the relationship and relationship quantity in map
			setLastRelationShip(Boolean.FALSE);
			for(TRUShippingGroupCommerceItemRelationship lCommItemRel : newListOfRel){
				CommerceItem commerceItem = lCommItemRel.getCommerceItem();
				if(isLoggingDebug()){
					vlogDebug("commerceItem: {0} lCommItemRel : {1}", commerceItem,lCommItemRel);
				}
				if(commerceItem instanceof TRUDonationCommerceItem){
					// Calling method to set the Donation Item details.
					populateDonationItemDetailXML(listOfOrderLine,pObjectFactory,pOrder,lCommItemRel,orderLineCount);
					orderLineCount++;
					continue;
				}
				long giftWrapQTY = TRUConstants.LONG_ZERO;
				long relationShipQTY = lCommItemRel.getQuantity();
				List<RepositoryItem> giftItemInfos = lCommItemRel.getGiftItemInfo();
				if(isLoggingDebug()){
					vlogDebug("giftItemInfos: {0}", giftItemInfos);
				}
				if(newListOfRel.indexOf(lCommItemRel)+TRUConstants.INTEGER_NUMBER_ONE == newListOfRel.size()){
					setLastRelationShip(Boolean.TRUE);
				}
				setGiftWrapRel(Boolean.FALSE);
				if(lCommItemRel.getIsGiftItem() && giftItemInfos != null && !giftItemInfos.isEmpty()){
					setGiftWrapRel(Boolean.TRUE);
					for(RepositoryItem giftItemInfo : giftItemInfos){
						long qty = (Long)giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
						giftWrapQTY+=qty;
						if(isLoggingDebug()){
							vlogDebug("isGiftWrapRel: {0} qty : {1}", isGiftWrapRel(),qty);
						}
						// Calling method to set the Gift Wrap Item details.
						populateGiftWrapLineItemXML(pObjectFactory,listOfOrderLine,lCommItemRel,pOrder,qty,giftItemInfo,orderLineCount);
						orderLineCount++;
					}
					if(giftWrapQTY > TRUConstants.ZERO && giftWrapQTY < relationShipQTY){
						// Calling method to set the Normal Item details.
						populateCommerceLineItemXML(pObjectFactory,listOfOrderLine, lCommItemRel, 
								pOrder,relationShipQTY-giftWrapQTY,orderLineCount);
						orderLineCount++;
					}
				}else{
					setGiftWrapRel(Boolean.FALSE);
					if(isLoggingDebug()){
						vlogDebug("isGiftWrapRel: {0} qty : {1}", isGiftWrapRel(),lCommItemRel.getQuantity());
					}
					// Calling method to set the Normal Item details.
					populateCommerceLineItemXML(pObjectFactory,listOfOrderLine, lCommItemRel, pOrder,lCommItemRel.getQuantity(),orderLineCount);
					orderLineCount++;
				}
			}
			setCommItemRelAmount(TRUConstants.DOUBLE_ZERO);// Setting null for all the map which are for pro-ration start
			setCommerceItemRelMapForMulOrderDis(null);
			setCommerceItemRelMapForMulShipDis(null);
			setCommerceItemRelMapForOrderDis(null);
			setCommerceItemRelMapForShipDis(null);
			setCommerceItemRelMapForSurcharge(null);
			setCommerceItemRelMapForEWasteFee(null);
			setCommerceItemRelMapForShipFee(null);
			setCommerceItemRelMapForBPP(null);
			setGWItemIdAndRelID(null);
			setGiftWrapRel(Boolean.FALSE);
		}
		setCommItemQtyMap(null);
		setCommerceItemQTYMapForMulItemDis(null);
		removeExtraParameterFromXML(listOfOrderLine);// Calling method to remove the extra parameters.
		pOrderXmlObj.setOrderLines(orderLines);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLOrderLines]");
		}
	}

	/**
	 * This method will populate the Gift Wrap Order Line XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder - TRUOrderImpl
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pGiftItemInfo - RepositoryItem
	 * @param pOrderLineCount - int Order Line Count
	 */
	private void populateGiftWrapLineItemXML(ObjectFactory pObjFactory,List<OrderLine> pListOfOrderLine,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,TRUOrderImpl pOrder, 
			long pGiftWrapQTY,RepositoryItem pGiftItemInfo, int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateGiftWrapLineItemXML]");
			vlogDebug("pObjFactory : {0} pListOfOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pListOfOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pGiftWrapQTY : {1}", pOrder,pGiftWrapQTY);
			vlogDebug("pGiftItemInfo : {0} pOrderLineCount : {1}", pGiftItemInfo,pOrderLineCount);
		}
		if(pShipGrpCIRel == null || pListOfOrderLine == null || pObjFactory == null){
			return;
		}
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		CommerceItem commItem = pShipGrpCIRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		if(commItem != null){
			itemPriceInfo =  (TRUItemPriceInfo) commItem.getPriceInfo();
		}
		if (isLoggingDebug()) {
			vlogDebug("shippingGroup: {0} commItem:{1}", shippingGroup,commItem);
		}
		OrderLine orderLine = pObjFactory.createTXMLMessageOrderOrderLinesOrderLine();
		orderLine.setLineNumber(Integer.toString(pOrderLineCount));
		orderLine.setItemID(commItem.getCatalogRefId());
		orderLine.setIsGift(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineIsGift(TRUE));
		orderLine.setCommerceItemId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCommerceItemId(commItem.getId()));
		orderLine.setShippingGroupId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingGroupId(shippingGroup.getId()));
		orderLine.setRelationshipId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineRelationshipId(pShipGrpCIRel.getId()));
		// Calling method to populate the Order Line Price Details.
		populateOrderLinePriceDetailXML(pObjFactory,orderLine,itemPriceInfo,pGiftWrapQTY);
		// Calling method to populate the Order line quantity details.
		populateOrderLineQuantityDetailXML(pObjFactory,orderLine,pGiftWrapQTY);
		// Calling method to populate the Order Line Shipping Info Details.
		populateOrderLineShippingInfoXML(pObjFactory,orderLine,pShipGrpCIRel,pOrder);
		// Calling method to set the Order line Discount details.
		populateOrderLineDiscountDetailXML(pObjFactory,orderLine,pShipGrpCIRel,pOrder,pListOfOrderLine,pGiftWrapQTY,pGiftItemInfo);
		// Calling method to populate the Order Line charge details.
		populateOrderLineChargeDetailXML(pObjFactory,pShipGrpCIRel,orderLine
				,pGiftWrapQTY,pListOfOrderLine,Boolean.TRUE,pGiftItemInfo,pOrder);
		// Calling method to populate the Order Line tax details.
		populateOrderLineTaxDetailXML(pObjFactory,pShipGrpCIRel,pOrder,orderLine,pGiftItemInfo,pGiftWrapQTY,pListOfOrderLine);
		// Calling method to populate the Order Line note details.
		populateOrderLineNotesXML(pObjFactory,pShipGrpCIRel,orderLine,pOrder,Boolean.TRUE,pGiftItemInfo);
		// Calling method to populate the Order Line reference Fields.
		populateOrderLineReferenceXML(pObjFactory,pShipGrpCIRel,orderLine,Boolean.TRUE,pGiftItemInfo,pOrder);
		if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			// Calling method to populate the Order Line Customer Attributes.
			populateOrderLineCustomerAttributesXML(pObjFactory,shippingGroup,orderLine);
		}
		pListOfOrderLine.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateGiftWrapLineItemXML]");
		}
	}
	
	/**
	 * This method will populate the Non Gift Wrap Order Line XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder - TRUOrderImpl
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pOrderLineCount - int
	 */
	private void populateCommerceLineItemXML(ObjectFactory pObjFactory,List<OrderLine> pListOfOrderLine,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,TRUOrderImpl pOrder,
			long pGiftWrapQTY,int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateCommerceLineItemXML]");
			vlogDebug("pObjFactory : {0} pListOfOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pListOfOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pGiftWrapQTY : {1}", pOrder,pGiftWrapQTY);
			vlogDebug("pOrderLineCount : {0}",pOrderLineCount);
		}
		if(pShipGrpCIRel == null || pListOfOrderLine == null || pObjFactory == null){
			return;
		}
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		CommerceItem commItem = pShipGrpCIRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		if(commItem != null){
			itemPriceInfo =  (TRUItemPriceInfo) commItem.getPriceInfo();
		}
		if (isLoggingDebug()) {
			vlogDebug("shippingGroup: {0} commItem:{1}", shippingGroup,commItem);
		}
		OrderLine orderLine = pObjFactory.createTXMLMessageOrderOrderLinesOrderLine();
		orderLine.setLineNumber(Integer.toString(pOrderLineCount));
		orderLine.setItemID(commItem.getCatalogRefId());
		orderLine.setIsGift(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineIsGift(FALSE));
		orderLine.setCommerceItemId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCommerceItemId(commItem.getId()));
		orderLine.setShippingGroupId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingGroupId(shippingGroup.getId()));
		orderLine.setRelationshipId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineRelationshipId(pShipGrpCIRel.getId()));
		// Calling method to populate the Order Line Price Details.
		populateOrderLinePriceDetailXML(pObjFactory,orderLine,itemPriceInfo,pGiftWrapQTY);
		// Calling method to populate the Order line quantity details.
		populateOrderLineQuantityDetailXML(pObjFactory,orderLine,pGiftWrapQTY);
		// Calling method to populate the Order Line Shipping Info Details.
		populateOrderLineShippingInfoXML(pObjFactory,orderLine,pShipGrpCIRel,pOrder);
		// Calling method to set the Order line Discount details.
		populateOrderLineDiscountDetailXML(pObjFactory,orderLine,pShipGrpCIRel,pOrder,pListOfOrderLine,pGiftWrapQTY,null);
		// Calling method to populate the Order Line charge details.
		populateOrderLineChargeDetailXML(pObjFactory,pShipGrpCIRel,orderLine
				,pGiftWrapQTY,pListOfOrderLine,Boolean.FALSE,null,pOrder);
		// Calling method to populate the Order Line tax details.
		populateOrderLineTaxDetailXML(pObjFactory,pShipGrpCIRel,pOrder,orderLine,null,pGiftWrapQTY,pListOfOrderLine);
		// Calling method to populate the Order Line note details.
		populateOrderLineNotesXML(pObjFactory,pShipGrpCIRel,orderLine,pOrder,Boolean.FALSE,null);
		// Calling method to populate the Order Line reference Fields.
		populateOrderLineReferenceXML(pObjFactory,pShipGrpCIRel,orderLine,Boolean.FALSE,null,pOrder);
		if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			// Calling method to populate the Order Line Customer Attributes.
			populateOrderLineCustomerAttributesXML(pObjFactory,shippingGroup,orderLine);
		}
		pListOfOrderLine.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateCommerceLineItemXML]");
		}
	}

	/**
	 * This method will populate the Donation Order Line XML Parameters.
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pObjFactory - ObjectFactory
	 * @param pOrder - TRUOrderImpl
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLineCount - int
	 */
	private void populateDonationItemDetailXML(List<OrderLine> pListOfOrderLine, ObjectFactory pObjFactory,
			TRUOrderImpl pOrder,TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateDonationItemDetailXML]");
			vlogDebug("pObjFactory : {0} pListOfOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pListOfOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pOrderLineCount : {1}", pOrder,pOrderLineCount);
		}
		if(pShipGrpCIRel == null || pListOfOrderLine == null || pObjFactory == null){
			return;
		}
		CommerceItem commItem = pShipGrpCIRel.getCommerceItem();
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) commItem.getPriceInfo();
		OrderLine orderLine = pObjFactory.createTXMLMessageOrderOrderLinesOrderLine();
		orderLine.setLineNumber(Integer.toString(pOrderLineCount));
		orderLine.setItemID(commItem.getCatalogRefId());
		orderLine.setIsGift(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineIsGift(FALSE));
		orderLine.setCommerceItemId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCommerceItemId(commItem.getId()));
		orderLine.setShippingGroupId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingGroupId(shippingGroup.getId()));
		orderLine.setRelationshipId(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineRelationshipId(pShipGrpCIRel.getId()));
		// Calling method to populate the Order Line Price Details.
		populateOrderLinePriceDetailXML(pObjFactory,orderLine,itemPriceInfo,pShipGrpCIRel.getQuantity());
		// Calling method to populate the Order line quantity details.
		populateOrderLineQuantityDetailXML(pObjFactory,orderLine,pShipGrpCIRel.getQuantity());
		// CR- 175 Changes START---
		// Calling method to populate the Shipping info for Donation Item
		populateShippingInfoForDonation(pObjFactory,orderLine);
		// CR- 175 Changes END---
		// Calling method to populate the Order Line tax details.
		populateOrderLineTaxDetailXML(pObjFactory,pShipGrpCIRel,pOrder,orderLine,null,pShipGrpCIRel.getQuantity(),pListOfOrderLine);
		pListOfOrderLine.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateDonationItemDetailXML]");
		}
	}

	/**
	 * This method will populate the Order Line Price Detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLine - OrderLine
	 * @param pItemPriceInfo - TRUItemPriceInfo
	 * @param pQuantity - long Item QTY
	 */
	private void populateOrderLinePriceDetailXML(ObjectFactory pObjFactory, OrderLine pOrderLine,
			TRUItemPriceInfo pItemPriceInfo, long pQuantity) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLinePriceDetailXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
			vlogDebug("pItemPriceInfo : {0} pQuantity : {1}",pItemPriceInfo,pQuantity);
		}
		if(pObjFactory == null || pOrderLine == null || pItemPriceInfo == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		PriceInfo orderLinePriceInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLinePriceInfo();
		orderLinePriceInfo.setPrice(BigDecimal.valueOf(pricingTools.round(pItemPriceInfo.getSalePrice())));
		orderLinePriceInfo.setExtendedPrice(pObjFactory.createTXMLMessageOrderOrderLinesOrderLinePriceInfoExtendedPrice
				(Double.toString(pricingTools.round(pItemPriceInfo.getSalePrice() * pQuantity))));
		orderLinePriceInfo.setIsPriceOverridden(
				pObjFactory.createTXMLMessageOrderOrderLinesOrderLinePriceInfoIsPriceOverridden(FALSE));
		pOrderLine.setPriceInfo(orderLinePriceInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLinePriceDetailXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Quantity Detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLine - OrderLine
	 * @param pGiftWrapQTY - long
	 */
	private void populateOrderLineQuantityDetailXML(ObjectFactory pObjFactory,
			OrderLine pOrderLine,
			long pGiftWrapQTY) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineQuantityDetailXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
			vlogDebug("pGiftWrapQTY : {0}",pGiftWrapQTY);
		}
		if(pObjFactory == null || pOrderLine == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Quantity orderLineQuantity = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineQuantity();
		orderLineQuantity.setOrderedQty(BigDecimal.valueOf(pGiftWrapQTY));
		orderLineQuantity.setOrderedQtyUOM(omsConfiguration.getOrderedQtyUOM());
		pOrderLine.setQuantity(orderLineQuantity);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineQuantityDetailXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Shipping Info XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLine - OrderLine
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateOrderLineShippingInfoXML(ObjectFactory pObjFactory,
			OrderLine pOrderLine,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineShippingInfoXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
			vlogDebug("pOrder : {0} pShipGrpCIRel : {1}", pOrder,pShipGrpCIRel);
		}
		if(pObjFactory == null || pOrderLine == null || pShipGrpCIRel == null){
			return;
		}
		//String shipMethod = null;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo orderLineShippingInfo = 
				pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfo();
		if(shippingGroup instanceof TRUHardgoodShippingGroup){
			/*shipMethod = getOmsConfiguration().getShippingMethodMap().get(shippingGroup.getShippingMethod());
			if(StringUtils.isNotBlank(shipMethod)){
				orderLineShippingInfo.setShipVia(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipVia(shipMethod));
			}*/
			orderLineShippingInfo.setDeliveryOption(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoDeliveryOption
					(omsConfiguration.getDeliveryOptionSTA()));
			// Calling method to populate the Shipping Info Address Details.
			populateOrderLineShipInfoShipAddXML(pObjFactory,orderLineShippingInfo,shippingGroup,pOrder);
		}else if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			TRUInStorePickupShippingGroup inStoreShipGrp = (TRUInStorePickupShippingGroup) pShipGrpCIRel.getShippingGroup();
			orderLineShippingInfo.setShipToFacility(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipToFacility
					(inStoreShipGrp.getLocationId()));
			if(pShipGrpCIRel.isWarehousePickup()){
				orderLineShippingInfo.setDeliveryOption(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoDeliveryOption
						(omsConfiguration.getDeliveryOptionSTS()));
				// Calling method to populate the Shipping Info Address Details.
				populateOrderLineShipInfoShipAddXML(pObjFactory,orderLineShippingInfo,inStoreShipGrp,pOrder);
			}else{
				orderLineShippingInfo.setDeliveryOption(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoDeliveryOption
						(omsConfiguration.getDeliveryOptionCustomerPickup()));
				// Calling method to populate the Order Line Allocation Info.
				populateOrderLineAllocationInfoXMl(pObjFactory,pOrderLine,inStoreShipGrp);
			}
		}
		pOrderLine.setShippingInfo(orderLineShippingInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineShippingInfoXML]");
		}
	}

	/**
	 * This method will populate the Order Line Allocation Info XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLine - OrderLine
	 * @param pInStoreShipGrp - OrderLine
	 */
	private void populateOrderLineAllocationInfoXMl(ObjectFactory pObjFactory,
			OrderLine pOrderLine, TRUInStorePickupShippingGroup pInStoreShipGrp) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineAllocationInfoXMl]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
			vlogDebug("pInStoreShipGrp : {0}", pInStoreShipGrp);
		}
		if(pObjFactory == null || pOrderLine == null || pInStoreShipGrp == null){
			return;
		}
		AllocationInfo orderLineAllocationInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineAllocationInfo();
		orderLineAllocationInfo.setFulfillmentFacility(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineAllocationInfoFulfillmentFacility
				(pInStoreShipGrp.getLocationId()));
		pOrderLine.setAllocationInfo(orderLineAllocationInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineAllocationInfoXMl]");
		}
	}

	/**
	 * This method will populate the Order Line Shipping Info Address detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLineShippingInfo - ShippingInfo
	 * @param pShippingGroup - ShippingGroup
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateOrderLineShipInfoShipAddXML(
			ObjectFactory pObjFactory,
			com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo pOrderLineShippingInfo,
			ShippingGroup pShippingGroup, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineShipInfoShipAddXML]");
			vlogDebug("pObjFactory : {0} pOrderLineShippingInfo : {1}", pObjFactory,pOrderLineShippingInfo);
			vlogDebug("pShippingGroup : {0} pOrder : {1}", pShippingGroup,pOrder);
		}
		if(pObjFactory == null || pOrderLineShippingInfo == null || pShippingGroup == null){
			return;
		}
		TRUHardgoodShippingGroup hardgoodSG = null;
		TRUInStorePickupShippingGroup inStoreSG=null;
		String country = null;
		String phoneNumber = null;
		com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo.ShippingAddress orderLineShippingAddress = null;
		String shipMethod = null;
		TRUShippingManager shippingManager = getShippingManager();
		if(pShippingGroup instanceof TRUHardgoodShippingGroup){
			hardgoodSG = (TRUHardgoodShippingGroup) pShippingGroup;
			ContactInfo sgShippingAddress = (ContactInfo) hardgoodSG.getShippingAddress();
			final boolean isMilitaryArea =shippingManager. isMilitaryArea(sgShippingAddress);
			final boolean isPOBOXAddress = shippingManager.isPOBOXAddress(sgShippingAddress.getAddress1());
			if(isMilitaryArea || isPOBOXAddress ){
				shipMethod = getOmsConfiguration().getApoFposhippingMethodMap().get(hardgoodSG.getShippingMethod());
				pOrderLineShippingInfo.setShipVia(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipVia(shipMethod));
			} else {
				shipMethod = getOmsConfiguration().getShippingMethodMap().get(hardgoodSG.getShippingMethod());
				pOrderLineShippingInfo.setShipVia(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipVia(shipMethod));
			}
			if(sgShippingAddress != null){
				orderLineShippingAddress = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddress();
				orderLineShippingAddress.setShipToFirstName(sgShippingAddress.getFirstName());
				orderLineShippingAddress.setShipToLastName(sgShippingAddress.getLastName());
				orderLineShippingAddress.setShipToAddressLine1(sgShippingAddress.getAddress1());
				orderLineShippingAddress.setShipToCity(sgShippingAddress.getCity());
				orderLineShippingAddress.setShipToState(sgShippingAddress.getState());
				orderLineShippingAddress.setShipToPostalCode(sgShippingAddress.getPostalCode());
				if(StringUtils.isNotBlank(sgShippingAddress.getCounty())){
					orderLineShippingAddress.setShipToCounty(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToCounty
							(sgShippingAddress.getCounty()));
				}
				country = getOmsConfiguration().getCountryCodeMap().get(sgShippingAddress.getCountry());
				if(StringUtils.isBlank(country)){
					orderLineShippingAddress.setShipToCountry(sgShippingAddress.getCountry());
				}else{
					orderLineShippingAddress.setShipToCountry(country);
				}
				phoneNumber = formatPhoneNumer(sgShippingAddress.getPhoneNumber());
				orderLineShippingAddress.setShipToPhone(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToPhone
						(phoneNumber));
				if(pOrder != null){
					orderLineShippingAddress.setShipToEmail(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToEmail
							(pOrder.getEmail()));
				}
			}
		}
		if(pShippingGroup instanceof TRUInStorePickupShippingGroup){
			inStoreSG = (TRUInStorePickupShippingGroup) pShippingGroup;
			orderLineShippingAddress = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddress();
			orderLineShippingAddress.setShipToFirstName(inStoreSG.getFirstName());
			orderLineShippingAddress.setShipToLastName(inStoreSG.getLastName());
			phoneNumber = formatPhoneNumer(inStoreSG.getPhoneNumber());
			orderLineShippingAddress.setShipToPhone(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToPhone
					(phoneNumber));
			orderLineShippingAddress.setShipToEmail(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToEmail
					(inStoreSG.getEmail()));
		}
		pOrderLineShippingInfo.setShippingAddress(orderLineShippingAddress);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineShipInfoShipAddXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Discount detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderLine - OrderLine
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pGiftItemInfo - RepositoryItem
	 */
	private void populateOrderLineDiscountDetailXML(ObjectFactory pObjFactory,OrderLine pOrderLine,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			TRUOrderImpl pOrder, List<OrderLine> pListOfOrderLine, long pGiftWrapQTY,
			RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineDiscountDetailXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pListOfOrderLine : {1} pGiftWrapQTY:{2}", pOrder,pListOfOrderLine,pGiftWrapQTY);
			vlogDebug("pGiftItemInfo : {0}",pGiftItemInfo);
		}
		if(pObjFactory == null || pOrderLine == null){
			return;
		}
		double commItemRelAmount = getCommItemRelAmount();
		DiscountDetails orderLineDiscountDetails = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetails();
		List<DiscountDetail> listOfDiscountDetail = orderLineDiscountDetails.getDiscountDetail();
		// Calling method to populate the Item Discount Details.
		populateItemDiscountDetailXML(pObjFactory,pShipGrpCIRel,pListOfOrderLine,pGiftWrapQTY,listOfDiscountDetail,pOrder);
		// Calling method to populate the Order Discount Details.
		populateOrderDiscountDetailXML(pObjFactory,pShipGrpCIRel,pListOfOrderLine,pGiftWrapQTY,listOfDiscountDetail,
				pOrder,commItemRelAmount);
		// Calling method to populate the Shipping Discount Details.
		populateShippingDiscountDetailXML(pObjFactory,pShipGrpCIRel,pListOfOrderLine,pGiftWrapQTY,listOfDiscountDetail,
				pOrder,commItemRelAmount);
		if(pGiftItemInfo != null){
			// Calling method to populate the Gift Wrap Discount Details.
			populateGWDiscountDetailXML(pObjFactory,listOfDiscountDetail,pOrder,pGiftItemInfo);
		}
		pOrderLine.setDiscountDetails(orderLineDiscountDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineDiscountDetailXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Item Discount detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfDiscountDetail - List of DiscountDetail
	 * @param pOrder - TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	private void populateItemDiscountDetailXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			List<OrderLine> pListOfOrderLine,
			long pGiftWrapQTY, List<DiscountDetail> pListOfDiscountDetail,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateItemDiscountDetailXML]");
			vlogDebug("pObjFactory : {0} pShipGrpCIRel : {1}", pObjFactory,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pListOfOrderLine : {1}", pOrder,pListOfOrderLine);
			vlogDebug("pListOfDiscountDetail : {0} pGiftWrapQTY : {1}", pListOfDiscountDetail,pGiftWrapQTY);
		}
		if(pObjFactory == null || pShipGrpCIRel == null || pListOfDiscountDetail == null){
			return;
		}
		CommerceItem commerceItem = pShipGrpCIRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		List<PricingAdjustment> pricingAdj = null;
		if(commerceItem == null || commerceItem.getPriceInfo()== null){
			return;
		}
		itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		pricingAdj = itemPriceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		String promoDesc = null;
		TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		RepositoryItem promotion = null;
		RepositoryItem couponItem = null;
		DiscountDetail discountDetail = null;
		String campaignId = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if(promotion == null){
				continue;
			}
			if (isLoggingDebug()) {
				vlogDebug("promotion: {0}", promotion);
			}
			String couponCode = null;
			String singleUseCode = null;
			double proratedValue = TRUConstants.DOUBLE_ZERO;
			couponItem = priceAdj.getCoupon();
			discountDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetail();
			discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
			if(couponItem != null){
				couponCode = couponItem.getRepositoryId();
			}
			if(StringUtils.isNotBlank(couponCode)){
				if(pOrder != null && pOrder.getSingleUseCoupon() != null && 
						!pOrder.getSingleUseCoupon().isEmpty()){
					singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
				}
				if(!StringUtils.isBlank(singleUseCode)){
					discountDetail.setExtDiscountId(singleUseCode);
				}else{
					discountDetail.setExtDiscountId(couponCode);
				}
			}else{
				discountDetail.setExtDiscountId(promotion.getRepositoryId());
			}
			discountDetail.setDiscountType(omsConfiguration.getCoupon());
			proratedValue = getItemDiscountShare(-priceAdj.getTotalAdjustment(),pShipGrpCIRel,commerceItem
					,itemPriceInfo,pListOfOrderLine,promotion.getRepositoryId(),pGiftWrapQTY);
			if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
					(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
				discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getVendorFunded()));
				discountDetail.setReferenceField3(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField3
						(Double.toString(pricingTools.round(proratedValue))));
			}else{
				if(pricingTools.round(proratedValue) > TRUConstants.DOUBLE_ZERO){
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(pricingTools.round(proratedValue))));
				}else{
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				}
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getRetailFunded()));
			}
			campaignId = (String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName());
			if(StringUtils.isNotBlank(campaignId)){
				discountDetail.setReferenceField2(
						pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField2(campaignId));
			}
			promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
			if(StringUtils.isBlank(promoDesc)){
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
			}
			discountDetail.setReferenceField5(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField5(promoDesc));
			pListOfDiscountDetail.add(discountDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateItemDiscountDetailXML]");
		}
	}

	/**
	 * This method will used to get the Item Discount Share.
	 * @param pTotalAdjustment - double Discounted Amount to be shared
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pCommerceItem - CommerceItem
	 * @param pItemPriceInfo - TRUItemPriceInfo
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pPromotionId - String Promotion ID
	 * @param pGiftWrapQTY - long Item QTY
	 * @return prorateItemShare - double Prorated share for Item.
	 */
	private double getItemDiscountShare(double pTotalAdjustment,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			CommerceItem pCommerceItem, TRUItemPriceInfo pItemPriceInfo,
			List<OrderLine> pListOfOrderLine, String pPromotionId,
			long pGiftWrapQTY) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getItemDiscountShare]");
			vlogDebug("pTotalAdjustment : {0} pCommerceItem : {1} pShipGrpCIRel : {2}", pTotalAdjustment,pCommerceItem,pShipGrpCIRel);
			vlogDebug("pItemPriceInfo : {0} pListOfOrderLine : {1}", pItemPriceInfo,pListOfOrderLine);
			vlogDebug("pPromotionId : {0} pGiftWrapQTY : {1}", pPromotionId,pGiftWrapQTY);
		}
		double prorateItemShare = TRUConstants.DOUBLE_ZERO;
		if(pItemPriceInfo == null || pCommerceItem == null || pShipGrpCIRel == null || 
				pItemPriceInfo.getSalePrice() == TRUConstants.DOUBLE_ZERO){
			return prorateItemShare;
		}
		Map<CommerceItem, Map<String, Long>> mulitemDis = getCommerceItemQTYMapForMulItemDis();
		if(mulitemDis == null || mulitemDis.isEmpty()){
			return pTotalAdjustment;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Long> map = mulitemDis.get(pCommerceItem);
		if(map != null && !map.isEmpty() && map.containsKey(pPromotionId) && map.get(pPromotionId) > pGiftWrapQTY){
			prorateItemShare = (pTotalAdjustment*pGiftWrapQTY)/pCommerceItem.getQuantity();
			map.put(pPromotionId, map.get(pPromotionId)-pGiftWrapQTY);
			mulitemDis.put(pCommerceItem, map);
		}else{
			double discountShared = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine == null){
				return prorateItemShare = pTotalAdjustment-discountShared;
			}
			for(OrderLine lOrderLine : pListOfOrderLine){
				if(StringUtils.isNotBlank(lOrderLine.getItemID()) && lOrderLine.getItemID().equals(pCommerceItem.getCatalogRefId())
						&& lOrderLine.getDiscountDetails() != null && lOrderLine.getDiscountDetails().getDiscountDetail() != null){
					for(DiscountDetail lDiscountDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
						if(StringUtils.isBlank(lDiscountDetail .getExtDiscountDetailId()) || 
								!lDiscountDetail .getExtDiscountDetailId().equals(pPromotionId) || lDiscountDetail.getDiscountAmount() == null ||
								lDiscountDetail.getReferenceField1() == null){
							continue;
						}
						if(omsConfiguration.getRetailFunded().equals(lDiscountDetail.getReferenceField1().getValue())){
							discountShared += lDiscountDetail.getDiscountAmount().doubleValue();
						}else if(omsConfiguration.getVendorFunded().equals(lDiscountDetail.getReferenceField1().getValue())
								&& lDiscountDetail.getReferenceField3() != null){
							try{
								discountShared += Double.parseDouble(lDiscountDetail.getReferenceField3().getValue());
							}catch(NumberFormatException nfe){
								if (isLoggingError()) {
									vlogError("NumberFormatException : {0}", nfe);
								}
							}
						}
					}
				}
			}
			prorateItemShare = pTotalAdjustment-discountShared;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getItemDiscountShare]");
			vlogDebug("prorateItemShare: {0}",prorateItemShare);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(prorateItemShare);
	}

	/**
	 * This method will populate the Order Line Shipping Discount detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfDiscountDetail - List of DiscountDetail
	 * @param pOrder - TRUOrderImpl
	 * @param pCommItemRelAmount - double CommItemRelAmount
	 */
	@SuppressWarnings("unchecked")
	private void populateShippingDiscountDetailXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			List<OrderLine> pListOfOrderLine,
			long pGiftWrapQTY, List<DiscountDetail> pListOfDiscountDetail,
			TRUOrderImpl pOrder, double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateShippingDiscountDetailXML]");
			vlogDebug("pObjFactory : {0} pShipGrpCIRel : {1}", pObjFactory,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pListOfOrderLine : {1}", pOrder,pListOfOrderLine);
			vlogDebug("pListOfDiscountDetail : {0} pGiftWrapQTY : {1}", pListOfDiscountDetail,pGiftWrapQTY);
		}
		if(pShipGrpCIRel == null || pListOfDiscountDetail == null || pObjFactory == null){
			return;
		}
		boolean isLastRelItem = isLastRelationShip();
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		if (isLoggingDebug()) {
			vlogDebug("isLastRelItem: {0} shippingGroup: {1}", isLastRelItem,shippingGroup);
		}
		if(shippingGroup == null || shippingGroup.getPriceInfo() == null || shippingGroup instanceof InStorePickupShippingGroup){
			return;
		}
		TRUShippingPriceInfo shippingPriceInfo = (TRUShippingPriceInfo) shippingGroup.getPriceInfo();
		List<PricingAdjustment> pricingAdj = shippingPriceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		RepositoryItem promotion = null;
		RepositoryItem couponItem = null;
		String promoDesc = null;
		DiscountDetail discountDetail = null;
		String campaignId = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if (isLoggingDebug()) {
				vlogDebug("promotion: {0}", promotion);
			}
			if(promotion == null){
				continue;
			}
			DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
			String couponCode = null;
			String singleUseCode = null;
			double proratedValue = TRUConstants.DOUBLE_ZERO;
			discountDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetail();
			discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
			couponItem = priceAdj.getCoupon();
			if(couponItem != null){
				couponCode = couponItem.getRepositoryId();
			}
			if(!StringUtils.isBlank(couponCode)){
				if(pOrder.getSingleUseCoupon() != null && !pOrder.getSingleUseCoupon().isEmpty()){
					singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
				}
				if(!StringUtils.isBlank(singleUseCode)){
					discountDetail.setExtDiscountId(singleUseCode);
				}else{
					discountDetail.setExtDiscountId(couponCode);
				}
			}else{
				discountDetail.setExtDiscountId(promotion.getRepositoryId());
			}
			discountDetail.setDiscountType(omsConfiguration.getCoupon());
			if(isLastRelItem){
				double share = TRUConstants.DOUBLE_ZERO;
				share = getDiscountShareForLastRel(pListOfOrderLine,promotion,pShipGrpCIRel);
				if(isGiftWrapRel()){
					proratedValue = (-priceAdj.getAdjustment())-share;
					proratedValue = getShipDiscountShareForGW(proratedValue,pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount,
							pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,promotion.getRepositoryId());
				}else{
					proratedValue = (-priceAdj.getAdjustment()) - share;
				}
			}else{
				if(isGiftWrapRel()){
					proratedValue = getShipDiscountShareForGW(-priceAdj.getAdjustment(),pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount,
							pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,promotion.getRepositoryId());
				}else{
					proratedValue = getShipDiscountShare(-priceAdj.getAdjustment(),pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount);
				}
			}
			if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
					(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
				discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getVendorFunded()));
				discountDetail.setReferenceField3(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField3
						(Double.toString(pricingTools.round(proratedValue))));
			}else{
				if(pricingTools.round(proratedValue) > TRUConstants.DOUBLE_ZERO){
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(pricingTools.round(proratedValue))));
				}else{
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				}
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getRetailFunded()));
			}
			campaignId = (String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName());
			if(StringUtils.isNotBlank(campaignId)){
				discountDetail.setReferenceField2(
						pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField2(campaignId));
			}
			// Calling method to get the shipping info from "Shipping Fee" subtype nonMerchSKU
			String shippingFeeUID = getShippingFeeUIDWithDes();
			discountDetail.setReferenceField4(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField4
					(shippingFeeUID));
			promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
			if(StringUtils.isBlank(promoDesc)){
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
			}
			discountDetail.setReferenceField5(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField5(promoDesc));
			pListOfDiscountDetail.add(discountDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateShippingDiscountDetailXML]");
		}
	}

	/**
	 * This method is use to get the Shipping discount share for Gift Wrapped Commerce Items.
	 * @param pDiscountAmountToShare - double Amount to be shared
	 * @param pAmountByAverage - double Amount by average
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount
	 * @param pGiftWrapQTY - long
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pPromotionId - String Promotion ID
	 * @return proratedShipDisForGW - double prorated shipping discoun amount.
	 */
	private double getShipDiscountShareForGW(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,
			long pGiftWrapQTY,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			List<OrderLine> pListOfOrderLine, String pPromotionId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getShipDiscountShareForGW]");
			vlogDebug("pDiscountAmountToShare : {0} pAmountByAverage : {1}", pDiscountAmountToShare,pAmountByAverage);
			vlogDebug("pCommItemRelAmount : {0} pGiftWrapQTY : {1} pShipCIRel:{2}", pCommItemRelAmount,pGiftWrapQTY,pShipCIRel);
			vlogDebug("pPromotionId : {0} pListOfOrderLine:{1}", pPromotionId,pListOfOrderLine);
		}
		double proratedShipDisForGW = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO || pShipCIRel == null){
			return proratedShipDisForGW;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulShipDis = getCommerceItemRelMapForMulShipDis();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(mulShipDis != null && !mulShipDis.isEmpty()){
			Map<TRUShippingGroupCommerceItemRelationship, Long> map = mulShipDis.get(pPromotionId);
			if(map != null && !map.isEmpty()){
				qty = map.get(pShipCIRel);
				map.put(pShipCIRel, qty-pGiftWrapQTY);
				mulShipDis.put(pPromotionId, map);
				if(qty <= pGiftWrapQTY){
					isLastItem = Boolean.TRUE;
				}
			}
		}
		if(!isLastRelationShip()){
			proratedShipDisForGW = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedShipDisForGW = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pShipCIRel.getId().equals(lOrderLine.getRelationshipId().getValue()) && 
							lOrderLine.getDiscountDetails() != null && lOrderLine.getDiscountDetails().getDiscountDetail() != null&&
							!lOrderLine.getDiscountDetails().getDiscountDetail().isEmpty()){
						for(DiscountDetail lDiscountDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
							if(StringUtils.isBlank(lDiscountDetail.getExtDiscountDetailId()) || 
									!lDiscountDetail.getExtDiscountDetailId().equals(pPromotionId) || lDiscountDetail.getDiscountAmount() == null ||
									lDiscountDetail.getReferenceField1() == null){
								continue;
							}
							if(omsConfiguration.getRetailFunded().equals(lDiscountDetail.getReferenceField1().getValue())){
								share+=lDiscountDetail.getDiscountAmount().doubleValue();
							}else if(omsConfiguration.getVendorFunded().equals(lDiscountDetail.getReferenceField1().getValue()) ||
									lDiscountDetail.getReferenceField3() != null){
								try{
									share += Double.parseDouble(lDiscountDetail.getReferenceField3().getValue());
								}catch(NumberFormatException nfe){
									if (isLoggingError()) {
										vlogError("NumberFormatException : {0}", nfe);
									}
								}
							}
						}
					}
				}
			}
			proratedShipDisForGW = proratedShipDisForGW-share;
		}else{
			proratedShipDisForGW = (proratedShipDisForGW*pGiftWrapQTY)/pShipCIRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getShipDiscountShareForGW]");
			vlogDebug("proratedShipDisForGW: {0}",proratedShipDisForGW);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedShipDisForGW);
	}
	

	/**
	 * This method is use to get the Shipping discount share for non Gift Wrapped Commerce Items.
	 * @param pDiscountedAmount - double Amount to be shared
	 * @param pAmountByAverage - double Amount by Average
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount
	 * @return prorateShippingShare - double prorated discount amount.
	 */
	private double getShipDiscountShare(double pDiscountedAmount, double pAmountByAverage,
			double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getShipDiscountShare]");
			vlogDebug("pDiscountedAmount : {0} pAmountByAverage : {1}", pDiscountedAmount,pAmountByAverage);
			vlogDebug("pCommItemRelAmount : {0}", pCommItemRelAmount);
		}
		double prorateShippingShare = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return prorateShippingShare;
		}
		prorateShippingShare = (pDiscountedAmount*pAmountByAverage)/pCommItemRelAmount;
		prorateShippingShare = getOrderManager().getPromotionTools().getPricingTools().round(prorateShippingShare);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getShipDiscountShare]");
			vlogDebug("prorateShippingShare: {0}",prorateShippingShare);
		}
		return prorateShippingShare;
	}

	/**
	 * This method will populate the Order Line Order Discount detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfDiscountDetail - List of DiscountDetail
	 * @param pOrder - TRUOrderImpl
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderDiscountDetailXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			List<OrderLine> pListOfOrderLine,
			long pGiftWrapQTY, List<DiscountDetail> pListOfDiscountDetail,
			TRUOrderImpl pOrder, double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderDiscountDetailXML]");
			vlogDebug("pObjFactory : {0} pShipGrpCIRel : {1}", pObjFactory,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pListOfOrderLine : {1}", pOrder,pListOfOrderLine);
			vlogDebug("pListOfDiscountDetail : {0} pGiftWrapQTY : {1}", pListOfDiscountDetail,pGiftWrapQTY);
		}
		if(pOrder == null || pListOfDiscountDetail == null || pObjFactory == null){
			return;
		}
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
		List<PricingAdjustment> pricingAdj = orderPriceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		double orderSubTotalForProrate = getOrderTotalForDiscount(orderPriceInfo,pOrder);
		if(orderSubTotalForProrate == TRUConstants.DOUBLE_ZERO){
			return;
		}
		boolean isLastRelItem = isLastRelationShip();
		RepositoryItem promotion = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		DiscountDetail discountDetail = null;
		String promoDesc = null;
		String campaignId = null;
		String promoId = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if(promotion == null){
				continue;
			}
			DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
			String couponCode = null;
			String singleUseCode = null;
			double proratedValue = TRUConstants.DOUBLE_ZERO;
			promoId = promotion.getRepositoryId();
			double discountAmountToShare = (-priceAdj.getAdjustment()*pCommItemRelAmount)/orderSubTotalForProrate;
			//double discountAmountToShare = getOrderDiscountToShare(-priceAdj.getAdjustment(),pCommItemRelAmount,
		    //			orderSubTotalForProrate,promoId);
			discountAmountToShare = pricingTools.round(discountAmountToShare);
			discountDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetail();
			discountDetail.setExtDiscountDetailId(promoId);
			RepositoryItem couponItem = priceAdj.getCoupon();
			if(couponItem != null){
				couponCode = couponItem.getRepositoryId();
			}
			if(!StringUtils.isBlank(couponCode)){
				if(pOrder.getSingleUseCoupon() != null && 
						!pOrder.getSingleUseCoupon().isEmpty()){
					singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
				}
				if(!StringUtils.isBlank(singleUseCode)){
					discountDetail.setExtDiscountId(singleUseCode);
				}else{
					discountDetail.setExtDiscountId(couponCode);
				}
			}else{
				discountDetail.setExtDiscountId(promoId);
			}
			discountDetail.setDiscountType(omsConfiguration.getCoupon());
			if(isLastRelItem){
				double share = TRUConstants.DOUBLE_ZERO;
				share = getDiscountShareForLastRel(pListOfOrderLine,promotion,pShipGrpCIRel);
				if(isGiftWrapRel()){
					proratedValue = discountAmountToShare-share;
					proratedValue = getOrderDiscountShareForGW(proratedValue,pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount
							,pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,promoId);
				}else{
					proratedValue = discountAmountToShare - share;
				}
			}else{
				if(isGiftWrapRel()){
					proratedValue = getOrderDiscountShareForGW(discountAmountToShare,pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount
							,pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,promoId);
				}else{
					proratedValue = getOrderDiscountShare(discountAmountToShare,pShipGrpCIRel.getAmountByAverage(),pCommItemRelAmount);
				}
			}
			if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
					(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
				discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getVendorFunded()));
				discountDetail.setReferenceField3(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField3
						(Double.toString(pricingTools.round(proratedValue))));
			}else{
				if(pricingTools.round(proratedValue) > TRUConstants.DOUBLE_ZERO){
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(pricingTools.round(proratedValue))));
				}else{
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
				}
				discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
						(omsConfiguration.getRetailFunded()));
			}
			campaignId = (String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName());
			if(StringUtils.isNotBlank(campaignId)){
				discountDetail.setReferenceField2(
						pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField2(campaignId));
			}
			promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
			if(StringUtils.isBlank(promoDesc)){
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
			}
			discountDetail.setReferenceField5(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField5(promoDesc));
			pListOfDiscountDetail.add(discountDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderDiscountDetailXML]");
		}
	}

	/**
	 * This method is use to get the Discount share for Non Gift Wrap Commerce Items.
	 * @param pDiscountAmountToShare - double Amount to be shared
	 * @param pAmountByAverage - double Amount by Average
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount.
	 * @return proratedOrderDisShare - double
	 */
	private double getOrderDiscountShare(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getOrderDiscountShare]");
			vlogDebug("pDiscountAmountToShare : {0} pAmountByAverage : {1}", pDiscountAmountToShare,pAmountByAverage);
			vlogDebug("pCommItemRelAmount : {0}", pCommItemRelAmount);
		}
		double proratedOrderDisShare = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return proratedOrderDisShare;
		}
		proratedOrderDisShare = (pAmountByAverage*pDiscountAmountToShare)/pCommItemRelAmount;
		proratedOrderDisShare = getOrderManager().getPromotionTools().getPricingTools().round(proratedOrderDisShare);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getOrderDiscountShare]");
			vlogDebug("proratedOrderDisShare: {0}",proratedOrderDisShare);
		}
		return proratedOrderDisShare;
	}

	/**
	 * This method is use to get the Discount share for Gift Wrap Commerce Items.
	 * @param pDiscountAmountToShare - double Amount to be shared
	 * @param pAmountByAverage - double Amount by Average
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount.
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pPromotionId - String Promotion ID
	 * @return proratedOrderDisForGW - double
	 */
	private double getOrderDiscountShareForGW(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,
			long pGiftWrapQTY,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			List<OrderLine> pListOfOrderLine, String pPromotionId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getOrderDiscountShareForGW]");
			vlogDebug("pDiscountAmountToShare : {0} pAmountByAverage : {1}", pDiscountAmountToShare,pAmountByAverage);
			vlogDebug("pCommItemRelAmount : {0} pGiftWrapQTY : {1} pShipCIRel:{2}", pCommItemRelAmount,pGiftWrapQTY,pShipCIRel);
			vlogDebug("pPromotionId : {0} pListOfOrderLine:{1}", pPromotionId,pListOfOrderLine);
		}
		double proratedOrderDisForGW = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO || pShipCIRel == null){
			return proratedOrderDisForGW;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulOrderDis = getCommerceItemRelMapForMulOrderDis();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(mulOrderDis != null && !mulOrderDis.isEmpty()){
			Map<TRUShippingGroupCommerceItemRelationship, Long> map = mulOrderDis.get(pPromotionId);
			if(map != null && !map.isEmpty()){
				qty = map.get(pShipCIRel);
				map.put(pShipCIRel, qty-pGiftWrapQTY);
				mulOrderDis.put(pPromotionId, map);
				if(qty <= pGiftWrapQTY){
					isLastItem = Boolean.TRUE;
				}
			}
		}
		if(!isLastRelationShip()){
			proratedOrderDisForGW = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedOrderDisForGW = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pShipCIRel.getId().equals(lOrderLine.getRelationshipId().getValue()) && 
							lOrderLine.getDiscountDetails() != null && lOrderLine.getDiscountDetails().getDiscountDetail() != null){
						for(DiscountDetail lDiscountDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
							if(StringUtils.isBlank(lDiscountDetail.getExtDiscountDetailId()) || 
									!lDiscountDetail.getExtDiscountDetailId().equals(pPromotionId) || lDiscountDetail.getDiscountAmount() == null ||
									lDiscountDetail.getReferenceField1() == null){
								continue;
							}
							if(omsConfiguration.getRetailFunded().equals(lDiscountDetail.getReferenceField1().getValue())){
								share+=lDiscountDetail.getDiscountAmount().doubleValue();
							}else if(omsConfiguration.getVendorFunded().equals(lDiscountDetail.getReferenceField1().getValue()) ||
									lDiscountDetail.getReferenceField3() != null){
								try{
									share += Double.parseDouble(lDiscountDetail.getReferenceField3().getValue());
								}catch(NumberFormatException nfe){
									if (isLoggingError()) {
										vlogError("NumberFormatException : {0}", nfe);
									}
								}
							}
						}
					}
				}
			}
			proratedOrderDisForGW = proratedOrderDisForGW-share;
		}else{
			proratedOrderDisForGW = (proratedOrderDisForGW*pGiftWrapQTY)/pShipCIRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getOrderDiscountShareForGW]");
			vlogDebug("proratedOrderDisForGW: {0}",proratedOrderDisForGW);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedOrderDisForGW);
	}

	/**
	 * This method is use to get the Discount share for Last Relationship Item.
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pPromotion - RepositoryItem
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @return share - double
	 */
	private double getDiscountShareForLastRel(List<OrderLine> pListOfOrderLine,
			RepositoryItem pPromotion,
			TRUShippingGroupCommerceItemRelationship pShipCIRel) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getDiscountShareForLastRel]");
			vlogDebug("pListOfOrderLine : {0}", pListOfOrderLine);
			vlogDebug("pPromotion : {0} pShipCIRel : {1}", pPromotion,pShipCIRel);
		}
		double share = TRUConstants.DOUBLE_ZERO;
		if(pShipCIRel == null || pListOfOrderLine == null){
			return share;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		for(OrderLine lOrderLine : pListOfOrderLine){
			if(lOrderLine.getShippingGroupId() != null && pShipCIRel.getShippingGroup().getId().equals(lOrderLine.getShippingGroupId().getValue()) && 
					lOrderLine.getRelationshipId() != null && !pShipCIRel.getId().equals(lOrderLine.getRelationshipId().getValue())
					&& lOrderLine.getDiscountDetails() != null && 
					lOrderLine.getDiscountDetails().getDiscountDetail() != null){
				for(DiscountDetail lDisDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
					if((!pPromotion.getRepositoryId().equals(lDisDetail.getExtDiscountDetailId())) || lDisDetail.getDiscountAmount() == null
							|| lDisDetail.getReferenceField1() == null){
						continue;
					}
					if(omsConfiguration.getRetailFunded().equals(lDisDetail.getReferenceField1().getValue())){
						share += lDisDetail.getDiscountAmount().doubleValue();
					}else if(omsConfiguration.getVendorFunded().equals(lDisDetail.getReferenceField1().getValue()) && lDisDetail.getReferenceField3() != null){
						try{
							share += Double.parseDouble(lDisDetail.getReferenceField3().getValue());
						}catch(NumberFormatException nfe){
							if (isLoggingError()) {
								vlogError("NumberFormatException : {0}", nfe);
							}
						}
					}
				}
				
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getDiscountShareForLastRel]");
			vlogDebug("share: {0}",share);
		}
		return share;
	}
	
	/**
	 * This method will populate the Order Line Gift Wrap Discount detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pListOfDiscountDetail - List of DiscountDetail
	 * @param pOrder - TRUOrderImpl
	 * @param pGiftItemInfo - RepositoryItem
	 */
	@SuppressWarnings("unchecked")
	private void populateGWDiscountDetailXML(
			ObjectFactory pObjFactory, List<DiscountDetail> pListOfDiscountDetail, TRUOrderImpl pOrder,
			RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateGWDiscountDetailXML]");
			vlogDebug("pObjFactory : {0} pListOfDiscountDetail : {1}", pObjFactory,pListOfDiscountDetail);
			vlogDebug("pOrder : {0} pGiftItemInfo : {1}", pOrder,pGiftItemInfo);
		}
		if(pGiftItemInfo == null || pOrder == null || pObjFactory == null || pListOfDiscountDetail == null){
			return;
		}
		String gwCommId = (String) pGiftItemInfo.getPropertyValue(((TRUOrderTools)getOrderManager().getOrderTools())
				.getCommercePropertyManager().getGiftWrapCommItemId());
		if(StringUtils.isBlank(gwCommId)){
			return;
		}
		List<PricingAdjustment> pricingAdj = null;
		CommerceItem commerceItem;
		try {
			commerceItem = pOrder.getCommerceItem(gwCommId);
			if(commerceItem == null || commerceItem.getPriceInfo() == null){
				return;
			}
			String promoDesc = null;
			TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
			pricingAdj = itemPriceInfo.getAdjustments();
			if(pricingAdj == null || pricingAdj.isEmpty()){
				return;
			}
			DiscountDetail discountDetail = null;
			RepositoryItem promotion = null;
			RepositoryItem couponItem = null;
			String campaignId = null;
			for(PricingAdjustment priceAdj : pricingAdj){
				promotion = priceAdj.getPricingModel();
				if(promotion == null){
					continue;
				}
				if (isLoggingDebug()) {
					vlogDebug("promotion: {0}", promotion);
				}
				DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
				String couponCode = null;
				String singleUseCode = null;
				discountDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetail();
				discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
				couponItem = priceAdj.getCoupon();
				if(couponItem != null){
					couponCode = couponItem.getRepositoryId();
				}
				if(!StringUtils.isBlank(couponCode)){
					if(pOrder.getSingleUseCoupon() != null && 
							!pOrder.getSingleUseCoupon().isEmpty()){
						singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
					}
					if(!StringUtils.isBlank(singleUseCode)){
						discountDetail.setExtDiscountId(singleUseCode);
					}else{
						discountDetail.setExtDiscountId(couponCode);
					}
				}else{
					discountDetail.setExtDiscountId(promotion.getRepositoryId());
				}
				discountDetail.setDiscountType(omsConfiguration.getCoupon());
				if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
						(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
					discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
					discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
							(omsConfiguration.getVendorFunded()));
					discountDetail.setReferenceField3(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField3
							(Double.toString(pricingTools.round(-priceAdj.getTotalAdjustment()))));
				}else{
					if(pricingTools.round(-priceAdj.getTotalAdjustment()) > TRUConstants.DOUBLE_ZERO){
						discountDetail.setDiscountAmount(new BigDecimal(formatter.format(pricingTools.round(-priceAdj.getTotalAdjustment()))));
					}else{
						discountDetail.setDiscountAmount(new BigDecimal(formatter.format(TRUConstants.DOUBLE_ZERO)));
					}
					discountDetail.setReferenceField1(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField1
							(omsConfiguration.getRetailFunded()));
				}
				campaignId = (String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName());
				if(StringUtils.isNotBlank(campaignId)){
					discountDetail.setReferenceField2(
							pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField2(campaignId));
				}
				// Calling method to get the gift wrap UID
				String chargeName = getGiftInfoUID(pGiftItemInfo);
				discountDetail.setReferenceField4(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField4(
						chargeName));
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
				if(StringUtils.isBlank(promoDesc)){
					promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
				}
				discountDetail.setReferenceField5(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineDiscountDetailsDiscountDetailReferenceField5(promoDesc));
				pListOfDiscountDetail.add(discountDetail);
			}
		} catch (CommerceItemNotFoundException exc) {
			if (isLoggingError()) {
				vlogError("CommerceItemNotFoundException:  While getting commerce item for gwCommId : {0} Exception", 
						gwCommId,exc);
			}
		} catch (InvalidParameterException exc) {
			if (isLoggingError()) {
				vlogError("InvalidParameterException:  While getting commerce item for gwCommId : {0} Exception", 
						gwCommId,exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateGWDiscountDetailXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Charge detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrderLine - OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsGiftLineItem - Boolean
	 * @param pGiftItemInfo - RepositoryItem
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateOrderLineChargeDetailXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			OrderLine pOrderLine, long pGiftWrapQTY,
			List<OrderLine> pListOfOrderLine, Boolean pIsGiftLineItem,
			RepositoryItem pGiftItemInfo, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineChargeDetailXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pGiftWrapQTY : {1} pGiftItemInfo : {2}", pOrder,pGiftWrapQTY,pGiftItemInfo);
			vlogDebug("pListOfOrderLine : {0} pIsGiftLineItem : {1}",pListOfOrderLine,pIsGiftLineItem);
		}
		if(pShipGrpCIRel == null || pObjFactory == null || pOrderLine == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		ChargeDetails orderLineChargeDetails = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetails();
		List<ChargeDetail> chargeDetails = orderLineChargeDetails.getChargeDetail();
		ShippingGroup shipGroup = pShipGrpCIRel.getShippingGroup();
		TRUShippingPriceInfo shipPriceInfo = (TRUShippingPriceInfo) shipGroup.getPriceInfo();
		if (isLoggingDebug()) {
			vlogDebug("Shipping surcharge: {0} Shipping Group {1}", pShipGrpCIRel.getSurcharge(),shipGroup);
			vlogDebug("E-Waste Fee: {0}", pShipGrpCIRel.getE911Fees());
		}
		if(pShipGrpCIRel.getSurcharge() > TRUConstants.DOUBLE_ZERO){
			// Calling method to populate the Shipping Surcharge details.
			populateOrderLineSurchargeDetailXML(pObjFactory,chargeDetails,
					pShipGrpCIRel,pGiftWrapQTY,pListOfOrderLine);
		}
		RepositoryItem bppItem = (RepositoryItem) pShipGrpCIRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem != null){
			// Calling method to populate the BPP Charge details.
			populateOrderLineBPPDetailsXML(pObjFactory,pShipGrpCIRel,chargeDetails,
					pGiftWrapQTY,pListOfOrderLine);
		}
		if(pIsGiftLineItem && pGiftItemInfo != null){
			// Calling method to populate the Gift Wrap Charge details.
			populateOrderLineGiftItemDetailsXML(pObjFactory,chargeDetails,pGiftItemInfo,pOrder,pGiftWrapQTY);
		}
		if(shipGroup != null && (!(shipGroup instanceof TRUInStorePickupShippingGroup)) && 
				shipPriceInfo != null && shipPriceInfo.getRawShipping() > TRUConstants.DOUBLE_ZERO){
			// Calling method to populate the Shipping Fee details.
			populateOrderLineShippingDetailsXML(pObjFactory,pShipGrpCIRel,pListOfOrderLine,pGiftWrapQTY,chargeDetails);
		}
		if(pShipGrpCIRel.getE911Fees() != TRUConstants.DOUBLE_ZERO){
			// Calling method to populate the EWaste Fee details.
			populateOrderLineEW911FeeDetailsXML(pObjFactory,pShipGrpCIRel,pGiftWrapQTY,pListOfOrderLine,chargeDetails);
		}
		pOrderLine.setChargeDetails(orderLineChargeDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineChargeDetailXML]");
		}
	}

	/**
	 * This method will populate the Order Line Shipping Surcharge detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pChargeDetails - List of ChargeDetail
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 */
	private void populateOrderLineSurchargeDetailXML(ObjectFactory pObjFactory,
			List<ChargeDetail> pChargeDetails,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineSurchargeDetailXML]");
			vlogDebug("pObjFactory : {0} pChargeDetails : {1}", pObjFactory,pChargeDetails);
			vlogDebug("pShipGrpCIRel : {0} pGiftWrapQTY : {1} pListOfOrderLine : {2}", pShipGrpCIRel,pGiftWrapQTY,pListOfOrderLine);
		}
		if(pObjFactory == null || pChargeDetails == null || pShipGrpCIRel ==null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail orderLineChargeDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetail();
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		orderLineChargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		orderLineChargeDetail.setChargeCategory(configuration.getChargeCategoryShipSurcharge());
		orderLineChargeDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailChargeName
				(getShippingSurchargeUID()));
		orderLineChargeDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailItemType(SHIPPING_SURCHARGE_ITEM));
		if(isGiftWrapRel()){
			proratedSurcharge = getShippingSurchageShareForGW(pShipGrpCIRel,pShipGrpCIRel.getSurcharge(),pGiftWrapQTY,pListOfOrderLine);
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(proratedSurcharge))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(proratedSurcharge/pGiftWrapQTY))));
		}else{
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(pShipGrpCIRel.getSurcharge()))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(pShipGrpCIRel.getSurcharge()/pShipGrpCIRel.getQuantity()))));
		}
		pChargeDetails.add(orderLineChargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineSurchargeDetailXML]");
		}
	}

	/**
	 * This method is use to get the Shipping Surcharge share for Gift Wrap items.
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pShippingCharge - double
	 * @param pGiftWrapQTY - long
	 * @param pListOfOrderLine - List of OrderLine
	 * @return praratedSurcharge - double prorated shipping surcharge share
	 */
	private double getShippingSurchageShareForGW(TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			double pShippingCharge, long pGiftWrapQTY,List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getShippingSurchageShareForGW]");
			vlogDebug("pShipGrpCIRel : {0} pShippingCharge : {1}", pShipGrpCIRel,pShippingCharge);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
		}
		double praratedSurcharge = TRUConstants.DOUBLE_ZERO;
		if(pShippingCharge == TRUConstants.DOUBLE_ZERO || pShipGrpCIRel == null
				|| pShipGrpCIRel.getQuantity() == TRUConstants.LONG_ZERO){
			return praratedSurcharge;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForSurcharge = getCommerceItemRelMapForSurcharge();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForSurcharge != null && !commerceItemRelMapForSurcharge.isEmpty()){
			qty = commerceItemRelMapForSurcharge.get(pShipGrpCIRel);
			commerceItemRelMapForSurcharge.put(pShipGrpCIRel, qty-pGiftWrapQTY);
			setCommerceItemRelMapForSurcharge(commerceItemRelMapForSurcharge);
			if(qty <= pGiftWrapQTY){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pShipGrpCIRel.getId().equals(lOrderLine.getRelationshipId().getValue())&&
							lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null){
						for(ChargeDetail lChargeDetail: lOrderLine.getChargeDetails().getChargeDetail()){
							if(lChargeDetail.getItemType() != null && SHIPPING_SURCHARGE_ITEM.equals(lChargeDetail.getItemType().getValue())
									&& lChargeDetail.getChargeAmount() != null){
								share += lChargeDetail.getChargeAmount().doubleValue();
							}
						}
					}
				}
			}
			praratedSurcharge = pShippingCharge-share;
		}else{
			praratedSurcharge = (pShippingCharge)*pGiftWrapQTY/pShipGrpCIRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getShippingSurchageShareForGW]");
			vlogDebug("praratedSurcharge: {0}",praratedSurcharge);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(praratedSurcharge);
	}

	/**
	 * This method will populate the Order Line BPP charge detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pChargeDetails - List of ChargeDetail
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 */
	private void populateOrderLineBPPDetailsXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			List<ChargeDetail> pChargeDetails,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineBPPDetailsXML]");
			vlogDebug("pObjFactory : {0} pChargeDetails : {1}", pObjFactory,pChargeDetails);
			vlogDebug("pShipGrpCIRel : {0} pGiftWrapQTY : {1} pListOfOrderLine : {2}", pShipGrpCIRel,pGiftWrapQTY,pListOfOrderLine);
		}
		if(pShipGrpCIRel == null || pChargeDetails == null || pObjFactory == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem bppItem = (RepositoryItem) pShipGrpCIRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem == null){
			return;
		}
		double bppCharge = ((double) bppItem.getPropertyValue(commercePropertyManager.getBppPricePropertyName()))*pShipGrpCIRel.getQuantity();
		if(bppCharge <= TRUConstants.DOUBLE_ZERO){
			return;
		}
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		ChargeDetail orderLineChargeDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetail();
		orderLineChargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		orderLineChargeDetail.setChargeCategory(configuration.getChargeCategoryVAS());
		String bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
		orderLineChargeDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailChargeName
				(bppSKUID));
		orderLineChargeDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailItemType(BPP_ITEM));
		if(isGiftWrapRel()){
			proratedSurcharge = getBPPChageShareForGW(pShipGrpCIRel, bppCharge, pGiftWrapQTY, pListOfOrderLine);
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(proratedSurcharge))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(proratedSurcharge/pShipGrpCIRel.getQuantity()))));
		}else{
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(bppCharge))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(bppCharge/pShipGrpCIRel.getQuantity()))));
		}
		pChargeDetails.add(orderLineChargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineBPPDetailsXML]");
		}
	}

	/**
	 * This method is use to get the BPP charge share for Gift Wrap Items.
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pBppCharge - double
	 * @param pGiftWrapQTY - long
	 * @param pListOfOrderLine - List of OrderLine
	 * @return proratedBPPShare - double prorated share
	 */
	private double getBPPChageShareForGW(TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			double pBppCharge, long pGiftWrapQTY,List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getBPPChageShareForGW]");
			vlogDebug("pShipGrpCIRel : {0} pShippingCharge : {1}", pShipGrpCIRel,pBppCharge);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
		}
		double proratedBPPShare = TRUConstants.DOUBLE_ZERO;
		if(pBppCharge == TRUConstants.DOUBLE_ZERO || pShipGrpCIRel == null
				|| pShipGrpCIRel.getQuantity() == TRUConstants.LONG_ZERO){
			return proratedBPPShare;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForBPP = getCommerceItemRelMapForBPP();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForBPP != null && !commerceItemRelMapForBPP.isEmpty()){
			qty = commerceItemRelMapForBPP.get(pShipGrpCIRel);
			commerceItemRelMapForBPP.put(pShipGrpCIRel, qty-pGiftWrapQTY);
			setCommerceItemRelMapForBPP(commerceItemRelMapForBPP);
			if(qty <= pGiftWrapQTY){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pShipGrpCIRel.getId().equals(lOrderLine.getRelationshipId().getValue())
							&& lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null){
						for(ChargeDetail lChargeDetail: lOrderLine.getChargeDetails().getChargeDetail()){
							if(configuration.getChargeCategoryVAS()
									.equals(lChargeDetail.getChargeCategory()) && lChargeDetail.getItemType() != null
									&& BPP_ITEM.equals(lChargeDetail.getItemType().getValue()) && lChargeDetail.getChargeAmount() != null){
								share += lChargeDetail.getChargeAmount().doubleValue();
							}
						}
					}
				}
			}
			proratedBPPShare = pBppCharge-share;
		}else{
			proratedBPPShare = (pBppCharge)*pGiftWrapQTY/pShipGrpCIRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getBPPChageShareForGW]");
			vlogDebug("proratedBPPShare: {0}",proratedBPPShare);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedBPPShare);
	}

	/**
	 * This method will populate the Order Line Gift Wrap charge detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pChargeDetails - List of ChargeDetail
	 * @param pGiftItemInfo - RepositoryItem
	 * @param pOrder - TRUOrderImpl
	 * @param pGiftWrapQTY - long Item QTY
	 */
	private void populateOrderLineGiftItemDetailsXML(ObjectFactory pObjFactory,
			List<ChargeDetail> pChargeDetails, RepositoryItem pGiftItemInfo,
			TRUOrderImpl pOrder, long pGiftWrapQTY) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineGiftItemDetailsXML]");
			vlogDebug("pObjFactory : {0} pChargeDetails : {1} pGiftItemInfo : {2}", pObjFactory,pChargeDetails,pGiftItemInfo);
			vlogDebug("pOrder : {0} pGiftWrapQTY : {1}", pOrder,pGiftWrapQTY);
		}
		if(pGiftItemInfo == null || pChargeDetails == null || pObjFactory == null){
			return;
		}
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail orderLineChargeDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetail();
		orderLineChargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		orderLineChargeDetail.setChargeCategory(configuration.getChargeCategoryVAS());
		String giftWrapCatalogRefId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
		orderLineChargeDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailChargeName
				(giftWrapCatalogRefId));
		String commerceId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
		try {
			if(pOrder != null){
				CommerceItem commerceItem = pOrder.getCommerceItem(commerceId);
				if(commerceItem != null){
					ItemPriceInfo priceInfo = commerceItem.getPriceInfo();
					if(priceInfo != null){
						orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(priceInfo.getSalePrice()*pGiftWrapQTY))));
						//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
						//		(Double.toString(pricingTools.round(priceInfo.getAmount()/pGiftWrapQTY))));
					}
				}
			}
		} catch (CommerceItemNotFoundException exc) {
			if (isLoggingError()) {
				vlogError("CommerceItemNotFoundException : {0}", exc);
			}
		} catch (InvalidParameterException exc) {
			if (isLoggingError()) {
				vlogError("InvalidParameterException : {0}", exc);
			}
		}
		pChargeDetails.add(orderLineChargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineGiftItemDetailsXML]");
		}
	}

	/**
	 * This method will populate the Order Line shipping price detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pChargeDetails - List of ChargeDetail
	 */
	private void populateOrderLineShippingDetailsXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			List<OrderLine> pListOfOrderLine,
			long pGiftWrapQTY, List<ChargeDetail> pChargeDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineShippingDetailsXML]");
			vlogDebug("pObjFactory : {0} pChargeDetails : {1}", pObjFactory,pChargeDetails);
			vlogDebug("pShipGrpCIRel : {0} pGiftWrapQTY : {1} pListOfOrderLine : {2}", pShipGrpCIRel,pGiftWrapQTY,pListOfOrderLine);
		}
		if(pShipGrpCIRel == null || pChargeDetails == null || pObjFactory == null){
			return;
		}
		ShippingGroup shippingGroup = pShipGrpCIRel.getShippingGroup();
		if(shippingGroup == null || shippingGroup.getPriceInfo() == null){
			return;
		}
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		TRUShippingPriceInfo shipPriceInfo = (TRUShippingPriceInfo) shippingGroup.getPriceInfo();
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail orderLineChargeDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetail();
		orderLineChargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		orderLineChargeDetail.setChargeCategory(configuration.getChargeCategoryShipping());
		orderLineChargeDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailChargeName
				(getShippingFeeUID()));
		orderLineChargeDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailItemType(SHIPPING_ITEM));
		double shipAmountToBeShared = shipPriceInfo.getRawShipping();
		double proratedValue = TRUConstants.DOUBLE_ZERO;
		double commItemRelAmount = getCommItemRelAmount();
		boolean isLastRelItem = isLastRelationShip();
		if(isLastRelItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getShippingGroupId() != null && shippingGroup.getId().equals(lOrderLine.getShippingGroupId().getValue())
							&& lOrderLine.getRelationshipId() != null && !pShipGrpCIRel.getId().equals(lOrderLine.getRelationshipId().getValue())
							&& lOrderLine.getChargeDetails() != null){
						for(ChargeDetail lChargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
							if(lChargeDetail.getItemType() != null && SHIPPING_ITEM.equals(lChargeDetail.getItemType().getValue())&& 
									lChargeDetail.getChargeAmount() != null){
								share += lChargeDetail.getChargeAmount().doubleValue();
							}
						}
					}
				}
			}
			if(isGiftWrapRel()){
				proratedValue = shipAmountToBeShared-share;
				proratedValue = getShippingShareForGW(proratedValue,pShipGrpCIRel.getAmountByAverage()
						,commItemRelAmount,pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,isLastRelItem);
			}else{
				proratedValue = shipAmountToBeShared - share;
			}
		}else{
			if(isGiftWrapRel()){
				proratedValue = getShippingShareForGW(shipAmountToBeShared,pShipGrpCIRel.getAmountByAverage()
						,commItemRelAmount,pGiftWrapQTY,pShipGrpCIRel,pListOfOrderLine,isLastRelItem);
			}else{
				proratedValue = prorateShippingPrice(shipAmountToBeShared,pShipGrpCIRel.getAmountByAverage()
						,commItemRelAmount);
			}
		}
		
		if(isGiftWrapRel()){
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(proratedValue))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(proratedValue/pGiftWrapQTY))));
		}else{
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(proratedValue))));
			//orderLineChargeDetail.setUnitCharge(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(proratedValue/pShipGrpCIRel.getQuantity()))));
		}
		pChargeDetails.add(orderLineChargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineShippingDetailsXML]");
		}
	}

	/**
	 * This method is use to get the Shipping Price share for Gift Wrap Items.
	 * @param pDiscountAmountToShare - double Amount to be shared
	 * @param pAmountByAverage - double Amount by average
	 * @param pCommItemRelAmount - double Commerce Item Rel Amount
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pLCommItemRel - TRUShippingGroupCommerceItemRelationship
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsLastRelItem - boolean Is Last Rel flag
	 * @return proratedValueForShipFee - doube prorated share
	 */
	private double getShippingShareForGW(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,long pGiftWrapQTY,
			TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			List<OrderLine> pListOfOrderLine, boolean pIsLastRelItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getShippingShareForGW]");
			vlogDebug("pDiscountAmountToShare : {0} pAmountByAverage : {1}", pDiscountAmountToShare,pAmountByAverage);
			vlogDebug("pCommItemRelAmount : {0} pGiftWrapQTY : {1}", pCommItemRelAmount,pGiftWrapQTY);
			vlogDebug("pLCommItemRel : {0} pListOfOrderLine : {1} pIsLastRelItem : {2}", pLCommItemRel,pListOfOrderLine,pIsLastRelItem);
		}
		double proratedValueForShipFee = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO || pLCommItemRel ==null){
			return proratedValueForShipFee;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForShipFee = getCommerceItemRelMapForShipFee();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForShipFee != null && !commerceItemRelMapForShipFee.isEmpty()){
			qty = commerceItemRelMapForShipFee.get(pLCommItemRel);
			commerceItemRelMapForShipFee.put(pLCommItemRel, qty-pGiftWrapQTY);
			setCommerceItemRelMapForShipFee(commerceItemRelMapForShipFee);
			if(qty <= pGiftWrapQTY){
				isLastItem = Boolean.TRUE;
			}
		}
		if(!pIsLastRelItem){
			proratedValueForShipFee = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedValueForShipFee = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pLCommItemRel.getId().equals(lOrderLine.getRelationshipId().getValue())
							&& lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null){
						for(ChargeDetail lChargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
							if(lChargeDetail.getItemType() != null && SHIPPING_ITEM.equals(lChargeDetail.getItemType().getValue()) 
									&& lChargeDetail.getChargeAmount() != null){
								share += lChargeDetail.getChargeAmount().doubleValue();
							}
						}
						
					}
				}
			}
			proratedValueForShipFee = proratedValueForShipFee-share;
		}else{
			proratedValueForShipFee = (proratedValueForShipFee*pGiftWrapQTY)/pLCommItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getShippingShareForGW]");
			vlogDebug("proratedValueForShipFee: {0}",proratedValueForShipFee);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedValueForShipFee);
	}

	/**
	 * This method will populate the Order Line EWaste Fee detail XML Parameters.
	 * @param pObjectFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pChargeDetails - List of ChargeDetail
	 */
	private void populateOrderLineEW911FeeDetailsXML(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			List<ChargeDetail> pChargeDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineEW911FeeDetailsXML]");
			vlogDebug("pObjectFactory : {0} pShipCIRel : {1}", pObjectFactory,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pChargeDetails : {0}", pChargeDetails);
		}
		if(pShipCIRel == null || pChargeDetails == null || pObjectFactory == null){
			return;
		}
		DecimalFormat formatter = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail orderLineChargeDetail = pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetail();
		orderLineChargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		orderLineChargeDetail.setChargeCategory(configuration.getChargeCategoryE911());
		String ewasteUID = null;
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		if(pShipCIRel.getCommerceItem() != null){
			RepositoryItem productRef = (RepositoryItem) pShipCIRel.getCommerceItem().getAuxiliaryData().getProductRef();
			// Calling method to get the EWaste UID
			ewasteUID = getCatalogTools().getEWasteUID(productRef);
		}
		orderLineChargeDetail.setChargeName(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailChargeName
				(ewasteUID));
		if(isGiftWrapRel()){
			proratedSurcharge = getE911FeesShareForGW(pShipCIRel,pShipCIRel.getE911Fees(),
					pGiftWrapQTY,pListOfOrderLine);
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(proratedSurcharge))));
			//orderLineChargeDetail.setUnitCharge(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(proratedSurcharge/pGiftWrapQTY))));
		}else{
			orderLineChargeDetail.setChargeAmount(new BigDecimal(formatter.format(pricingTools.round(pShipCIRel.getE911Fees()))));
			//orderLineChargeDetail.setUnitCharge(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineChargeDetailsChargeDetailUnitCharge
			//		(Double.toString(pricingTools.round(pShipCIRel.getE911Fees()/pShipCIRel.getQuantity()))));
		}
		pChargeDetails.add(orderLineChargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineEW911FeeDetailsXML]");
		}
	}

	/**
	 * This method is use to get the EWaste Fee share for Gift Wrap Items.
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pE911Fees - double EWaste Fee to be shared
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @return praratedE911Fees - double prorated share
	 */
	private double getE911FeesShareForGW(TRUShippingGroupCommerceItemRelationship pShipCIRel,
			double pE911Fees, long pGiftWrapQTY,List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getE911FeesShareForGW]");
			vlogDebug("pShipCIRel : {0} pE911Fees : {1}", pShipCIRel,pE911Fees);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
		}
		double praratedE911Fees = TRUConstants.DOUBLE_ZERO;
		if(pE911Fees == TRUConstants.DOUBLE_ZERO || pShipCIRel == null
				|| pShipCIRel.getQuantity() == TRUConstants.LONG_ZERO){
			return praratedE911Fees;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForEWasteFee = getCommerceItemRelMapForEWasteFee();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForEWasteFee != null && !commerceItemRelMapForEWasteFee.isEmpty()){
			qty = commerceItemRelMapForEWasteFee.get(pShipCIRel);
			commerceItemRelMapForEWasteFee.put(pShipCIRel, qty-pGiftWrapQTY);
			setCommerceItemRelMapForEWasteFee(commerceItemRelMapForEWasteFee);
			if(qty <= pGiftWrapQTY){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
				for(OrderLine lOrderLine : pListOfOrderLine){
					if(lOrderLine.getRelationshipId() != null && pShipCIRel.getId().equals(lOrderLine.getRelationshipId().getValue())
							&& lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null){
						for(ChargeDetail lChargeDetail: lOrderLine.getChargeDetails().getChargeDetail()){
							if(configuration.getChargeCategoryE911()
									.equals(lChargeDetail.getChargeCategory()) && lChargeDetail.getChargeAmount() != null){
								share += lChargeDetail.getChargeAmount().doubleValue();
							}
						}
					}
				}
			}
			praratedE911Fees = pE911Fees-share;
		}else{
			praratedE911Fees = (pE911Fees)*pGiftWrapQTY/pShipCIRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getE911FeesShareForGW]");
			vlogDebug("praratedE911Fees: {0}",praratedE911Fees);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(praratedE911Fees);
	}
	
	/**
	 * This method will populate the Order Line Tax detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 * @param pOrderLine - OrderLine
	 * @param pGiftItemInfo - RepositoryItem
	 * @param pGiftWrapQTY - long
	 * @param pListOfOrderLine - List of OrderLine
	 */
	private void populateOrderLineTaxDetailXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			TRUOrderImpl pOrder, OrderLine pOrderLine,
			RepositoryItem pGiftItemInfo, long pGiftWrapQTY,
			List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineTaxDetailXML]");
			vlogDebug("pObjFactory : {0} pShipCIRel : {1}", pObjFactory,pShipCIRel);
			vlogDebug("pOrder : {0} pOrderLine : {1}", pOrder,pOrderLine);
			vlogDebug("pGiftItemInfo : {0} pGiftWrapQTY : {1} pListOfOrderLine:{2}", pGiftItemInfo,pGiftWrapQTY,pListOfOrderLine);
		}
		if(pOrder == null || pOrderLine == null || pObjFactory == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		TaxDetails orderLineTaxDetails = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetails();
		List<TaxDetail> taxDetails = orderLineTaxDetails.getTaxDetail();
		// Calling method to populate the Normal item tax details.
		populateNormalItemTaxDetailsXML(pObjFactory,pShipCIRel,pOrder,taxDetails,pGiftWrapQTY,pListOfOrderLine);
		// Calling method to populate the Gift item tax details.
		populateGiftItemTaxDetailsXML(pObjFactory,pOrder,pGiftItemInfo,taxDetails);
		// Calling method to populate the EWaste item tax details.
		populateEWasteItemTaxDetailsXML(pObjFactory,pShipCIRel,pOrder,taxDetails,pGiftWrapQTY,pListOfOrderLine);
		// Calling method to populate the BPP item tax details.
		populateBPPItemTaxDetailsXML(pObjFactory,pShipCIRel,pOrder,taxDetails,pGiftWrapQTY,pListOfOrderLine);
		pOrderLine.setTaxDetails(orderLineTaxDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineTaxDetailXML]");
		}
	}

	/**
	 * This method will populate the Order Line Normal Items Tax detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 * @param pTaxDetails - List of TaxDetail
	 * @param pGiftWrapQTY - long
	 * @param pListOfOrderLine - List of OrderLine
	 */
	@SuppressWarnings("rawtypes")
	private void populateNormalItemTaxDetailsXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			TRUOrderImpl pOrder, List<TaxDetail> pTaxDetails,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineTaxDetailXML]");
			vlogDebug("pObjFactory : {0} pShipCIRel : {1}", pObjFactory,pShipCIRel);
			vlogDebug("pOrder : {0} pTaxDetails : {1}", pOrder,pTaxDetails);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
		}
		if(pOrder == null || pShipCIRel == null || pObjFactory == null || pTaxDetails == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pShipCIRel.getId());
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		double proratedTaxableAmount = TRUConstants.DOUBLE_ZERO;
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxState()));
			proratedTax = getStateTaxShare(taxPriceInfoObject.getStateTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getStateTaxRate())));
			proratedTaxableAmount = getStateTaxableAmountShare(taxPriceInfoObject.getStateBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCity()));
			proratedTax = getCityTaxShare(taxPriceInfoObject.getCityTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCityTaxRate())));
			proratedTaxableAmount = getCityTaxableAmountShare(taxPriceInfoObject.getCityBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCounty()));
			proratedTax = getCountyTaxShare(taxPriceInfoObject.getCountyTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCountyTaxRate())));
			proratedTaxableAmount = getCountyTaxableAmountShare(taxPriceInfoObject.getCountyBasisAmount(), pShipCIRel, pGiftWrapQTY
					, pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryState()));
			proratedTax = getSecStateTaxShare(taxPriceInfoObject.getSecondaryStateTaxAmount(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryStateTaxRate())));
			proratedTaxableAmount = getSecStateTaxableAmountShare(taxPriceInfoObject.getSecondaryStateBasisAmount(), pShipCIRel, pGiftWrapQTY
					, pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCity()));
			proratedTax = getSecCityTaxShare(taxPriceInfoObject.getSecondaryCityTaxAmount(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCityTaxRate())));
			proratedTaxableAmount = getSecCityTaxableAmountShare(taxPriceInfoObject.getSecondaryCityBasisAmount(), pShipCIRel, pGiftWrapQTY
					, pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCounty()));
			proratedTax = getSecCountyTaxShare(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCountyTaxRate())));
			proratedTaxableAmount = getSecCountyTaxableAmountShare(taxPriceInfoObject.getSecondaryCountyBasisAmount(), pShipCIRel, pGiftWrapQTY
					, pListOfOrderLine, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
					(NORMAL_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateNormalItemTaxDetailsXML]");
		}
	}

	/**
	 * This method will populate the Order Line Gift Wrap Items Tax detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrder - TRUOrderImpl
	 * @param pGiftItemInfo - RepositoryItem
	 * @param pTaxDetails - List of TaxDetail
	 */
	@SuppressWarnings("rawtypes")
	private void populateGiftItemTaxDetailsXML(ObjectFactory pObjFactory,
			TRUOrderImpl pOrder, RepositoryItem pGiftItemInfo,
			List<TaxDetail> pTaxDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateGiftItemTaxDetailsXML]");
			vlogDebug("pObjFactory : {0} pOrder : {1}", pObjFactory,pOrder);
			vlogDebug("pGiftItemInfo : {0} pTaxDetails : {1}", pGiftItemInfo,pTaxDetails);
		}
		if(pOrder == null || pGiftItemInfo == null || pObjFactory == null || pTaxDetails == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		String commerceItemId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
		String giftInfoUID = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
		String relId = null;
		if(getGWItemIdAndRelID() != null){
			relId = getGWItemIdAndRelID().get(commerceItemId);
		}
		TRUTaxPriceInfo taxPriceInfoObject = null;
		if(StringUtils.isNotBlank(relId)){
			taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(relId);
		}
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//String giftInfoUID = getGiftInfoUID(pGiftItemInfo);
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxState()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getStateTax()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getStateTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getStateBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCity()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getCityTax()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCityTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getCityBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCounty()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getCountyTax()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCountyTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getCountyBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryState()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getSecondaryStateTaxAmount()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryStateTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getSecondaryStateBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCity()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getSecondaryCityTaxAmount()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCityTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getSecondaryCityBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(giftInfoUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName
						(giftInfoUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCounty()));
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(taxPriceInfoObject.getSecondaryCountyTaxAmount()));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCountyTaxRate())));
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(taxPriceInfoObject.getSecondaryCountyBasisAmount())));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateGiftItemTaxDetailsXML]");
		}
	}

	/**
	 * This method will populate the Order Line EWaste Items Tax detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 * @param pTaxDetails - List of TaxDetail
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 */
	@SuppressWarnings("rawtypes")
	private void populateEWasteItemTaxDetailsXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			TRUOrderImpl pOrder, List<TaxDetail> pTaxDetails,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateEWasteItemTaxDetailsXML]");
			vlogDebug("pObjFactory : {0} pOrder : {1} pShipCIRel:{2}", pObjFactory,pOrder,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pTaxDetails : {1} pListOfOrderLine:{2}", pGiftWrapQTY,pTaxDetails,pListOfOrderLine);
		}
		if(pOrder == null || pShipCIRel == null || pObjFactory == null || pTaxDetails == null){
			return;
		}
		CommerceItem commerceItem = pShipCIRel.getCommerceItem();
		if(commerceItem == null){
			return;
		}
		TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		if(priceInfo != null && priceInfo.getEwasteFees() > TRUConstants.DOUBLE_ZERO){
			TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
			if(taxPriceInfo == null){
				return;
			}
			Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
			if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
				return;
			}
			TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pShipCIRel.getId()+EW+TRUConstants._1);
			if(taxPriceInfoObject == null){
				return;
			}
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			String uid = getCatalogTools().getEWasteUID((RepositoryItem)commerceItem.getAuxiliaryData().getProductRef());
			DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
			df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
			double proratedTax = TRUConstants.DOUBLE_ZERO;
			double proratedTaxableAmount = TRUConstants.DOUBLE_ZERO;
			String orderId = pOrder.getId();
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxState())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxState()));
				proratedTax = getStateTaxShare(taxPriceInfoObject.getStateTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getStateTaxRate())));
				proratedTaxableAmount = getStateTaxableAmountShare(taxPriceInfoObject.getStateBasisAmount(), pShipCIRel, pGiftWrapQTY, 
						pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCity())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxCity()));
				proratedTax = getCityTaxShare(taxPriceInfoObject.getCityTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getCityTaxRate())));
				proratedTaxableAmount = getCityTaxableAmountShare(taxPriceInfoObject.getCityBasisAmount(), pShipCIRel, pGiftWrapQTY
						, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCounty())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxCounty()));
				proratedTax = getCountyTaxShare(taxPriceInfoObject.getCountyTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getCountyTaxRate())));
				proratedTaxableAmount = getCountyTaxableAmountShare(taxPriceInfoObject.getCountyBasisAmount(), pShipCIRel, pGiftWrapQTY,
						pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryState())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxSecondoryState()));
				proratedTax = getSecStateTaxShare(taxPriceInfoObject.getSecondaryStateTaxAmount(), pShipCIRel, 
						pGiftWrapQTY, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getSecondaryStateTaxRate())));
				proratedTaxableAmount = getSecStateTaxableAmountShare(taxPriceInfoObject.getSecondaryStateBasisAmount(), pShipCIRel, pGiftWrapQTY
						, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCity())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxSecondoryCity()));
				proratedTax = getSecCityTaxShare(taxPriceInfoObject.getSecondaryCityTaxAmount(), pShipCIRel, 
						pGiftWrapQTY, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getSecondaryCityTaxRate())));
				proratedTaxableAmount = getSecCityTaxableAmountShare(taxPriceInfoObject.getSecondaryCityBasisAmount(), pShipCIRel, pGiftWrapQTY
						, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
			if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCounty())){
				TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
				orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
						(omsConfiguration.getChargeCategoryE911()));
				if(StringUtils.isNotBlank(uid)){
					orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(uid));
				}
				orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
				orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
						(taxPriceInfoObject.getTaxSecondoryCounty()));
				proratedTax = getSecCountyTaxShare(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pShipCIRel, 
						pGiftWrapQTY, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
				orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
						formatTaxRate(taxPriceInfoObject.getSecondaryCountyTaxRate())));
				proratedTaxableAmount = getSecCountyTaxableAmountShare(taxPriceInfoObject.getSecondaryCountyBasisAmount(), pShipCIRel, pGiftWrapQTY
						, pListOfOrderLine, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
						Double.toString(proratedTaxableAmount)));
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType
						(EWASTE_ITEM));
				pTaxDetails.add(orderLineTaxDetail);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateEWasteItemTaxDetailsXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line BPP Items Tax detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrder - TRUOrderImpl
	 * @param pTaxDetails - List of TaxDetail
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 */
	@SuppressWarnings("rawtypes")
	private void populateBPPItemTaxDetailsXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			TRUOrderImpl pOrder, List<TaxDetail> pTaxDetails,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateBPPItemTaxDetailsXML]");
			vlogDebug("pObjFactory : {0} pOrder : {1} pShipCIRel:{2}", pObjFactory,pOrder,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pTaxDetails : {1} pListOfOrderLine:{2}", pGiftWrapQTY,pTaxDetails,pListOfOrderLine);
		}
		if(pOrder == null || pShipCIRel == null || pObjFactory == null || pTaxDetails == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem bppItem = (RepositoryItem) pShipCIRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem == null){
			return;
		}
		String bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pShipCIRel.getId()+BPP+TRUConstants._1);
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//String uid = getBPPInfoUID(bppSKUID);
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		double proratedTaxableAmount = TRUConstants.DOUBLE_ZERO;
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxState()));
			proratedTax = getStateTaxShare(taxPriceInfoObject.getStateTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getStateTaxRate())));
			proratedTaxableAmount = getStateTaxableAmountShare(taxPriceInfoObject.getStateBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCity()));
			proratedTax = getCityTaxShare(taxPriceInfoObject.getCityTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCityTaxRate())));
			proratedTaxableAmount = getCityTaxableAmountShare(taxPriceInfoObject.getCityBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxCounty()));
			proratedTax = getCountyTaxShare(taxPriceInfoObject.getCountyTax(), pShipCIRel, pGiftWrapQTY, pListOfOrderLine
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getCountyTaxRate())));
			proratedTaxableAmount = getCountyTaxableAmountShare(taxPriceInfoObject.getCountyBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryState())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryState()));
			proratedTax = getSecStateTaxShare(taxPriceInfoObject.getSecondaryStateTaxAmount(), pShipCIRel, 
					pGiftWrapQTY, pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryStateTaxRate())));
			proratedTaxableAmount = getSecStateTaxableAmountShare(taxPriceInfoObject.getSecondaryStateBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCity())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCity()));
			proratedTax = getSecCityTaxShare(taxPriceInfoObject.getSecondaryCityTaxAmount(), pShipCIRel, 
					pGiftWrapQTY, pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCityTaxRate())));
			proratedTaxableAmount = getSecCityTaxableAmountShare(taxPriceInfoObject.getSecondaryCityBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if(StringUtils.isNotBlank(taxPriceInfoObject.getTaxSecondoryCounty())){
			TaxDetail orderLineTaxDetail = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetail();
			orderLineTaxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			orderLineTaxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			orderLineTaxDetail.setChargeCategory(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeCategory
					(omsConfiguration.getChargeCategoryVAS()));
			if(StringUtils.isNotBlank(bppSKUID)){
				orderLineTaxDetail.setChargeName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailChargeName(bppSKUID));
			}
			orderLineTaxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			orderLineTaxDetail.setTaxLocationName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxLocationName
					(taxPriceInfoObject.getTaxSecondoryCounty()));
			proratedTax = getSecCountyTaxShare(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pShipCIRel, 
					pGiftWrapQTY, pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxAmount(BigDecimal.valueOf(proratedTax));
			orderLineTaxDetail.setTaxRate(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxRate(
					formatTaxRate(taxPriceInfoObject.getSecondaryCountyTaxRate())));
			proratedTaxableAmount = getSecCountyTaxableAmountShare(taxPriceInfoObject.getSecondaryCountyBasisAmount(), pShipCIRel, pGiftWrapQTY, 
					pListOfOrderLine, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			orderLineTaxDetail.setTaxableAmount(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailTaxableAmount(
					Double.toString(proratedTaxableAmount)));
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				orderLineTaxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			orderLineTaxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			orderLineTaxDetail.setItemType(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineTaxDetailsTaxDetailItemType(BPP_ITEM));
			pTaxDetails.add(orderLineTaxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateBPPItemTaxDetailsXML]");
		}
	}
	
	/**
	 * This method is use to get the State Tax Share.
	 * @param pStateTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedStateTax - double prorated tax share
	 */
	private double getStateTaxShare(double pStateTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getStateTaxShare]");
			vlogDebug("pStateTax : {0} pShipCIRel:{1}", pStateTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pStateTax <= TRUConstants.DOUBLE_ZERO){
			return pStateTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxStateLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedStateTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedStateTax = (pStateTax*pGiftWrapQTY)/quantity;
		}else{
			proratedStateTax = pStateTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getStateTaxShare]");
			vlogDebug("proratedStateTax: {0}",proratedStateTax);
		}
		return pricingTools.round(proratedStateTax);
	}
	
	/**
	 * This method is use to get the city Tax Share.
	 * @param pCityTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedCityTax - double prorated tax share
	 */
	private double getCityTaxShare(double pCityTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getCityTaxShare]");
			vlogDebug("pCityTax : {0} pShipCIRel:{1}", pCityTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pCityTax <= TRUConstants.DOUBLE_ZERO){
			return pCityTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxCityLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedCityTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedCityTax = (pCityTax*pGiftWrapQTY)/quantity;
		}else{
			proratedCityTax = pCityTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getCityTaxShare]");
			vlogDebug("proratedCityTax: {0}",proratedCityTax);
		}
		return pricingTools.round(proratedCityTax);
	}
	
	/**
	 * This method is use to get the county Tax Share.
	 * @param pCountyTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedCountyTax - double prorated tax share
	 */
	private double getCountyTaxShare(double pCountyTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getCountyTaxShare]");
			vlogDebug("pCountyTax : {0} pShipCIRel:{1}", pCountyTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pCountyTax <= TRUConstants.DOUBLE_ZERO){
			return pCountyTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxCountyLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedCountyTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedCountyTax = (pCountyTax*pGiftWrapQTY)/quantity;
		}else{
			proratedCountyTax = pCountyTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getCountyTaxShare]");
			vlogDebug("proratedCountyTax: {0}",proratedCountyTax);
		}
		return pricingTools.round(proratedCountyTax);
	}
	
	/**
	 * This method is use to get the secondary state Tax Share.
	 * @param pSecStateTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecStateTax - double prorated tax share
	 */
	private double getSecStateTaxShare(double pSecStateTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecStateTaxShare]");
			vlogDebug("pSecStateTax : {0} pShipCIRel:{1}", pSecStateTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pSecStateTax <= TRUConstants.DOUBLE_ZERO){
			return pSecStateTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxSecStateLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedSecStateTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecStateTax = (pSecStateTax*pGiftWrapQTY)/quantity;
		}else{
			proratedSecStateTax = pSecStateTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecStateTaxShare]");
			vlogDebug("proratedSecStateTax: {0}",proratedSecStateTax);
		}
		return pricingTools.round(proratedSecStateTax);
	}
	
	/**
	 * This method is use to get the secondary city Tax Share.
	 * @param pSecCityTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecCityTax - double prorated tax share
	 */
	private double getSecCityTaxShare(double pSecCityTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecCityTaxShare]");
			vlogDebug("pSecCityTax : {0} pShipCIRel:{1}", pSecCityTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pSecCityTax <= TRUConstants.DOUBLE_ZERO){
			return pSecCityTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxSecCityLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedSecCityTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecCityTax = (pSecCityTax*pGiftWrapQTY)/quantity;
		}else{
			proratedSecCityTax = pSecCityTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecCityTaxShare]");
			vlogDebug("proratedTax: {0}",proratedSecCityTax);
		}
		return pricingTools.round(proratedSecCityTax);
	}
	
	/**
	 * This method is use to get the secondary county Tax Share.
	 * @param pSecCountyTax - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecCountyTax - double prorated tax share
	 */
	private double getSecCountyTaxShare(double pSecCountyTax,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecCountyTaxShare]");
			vlogDebug("pSecCountyTax : {0} pShipCIRel:{1}", pSecCountyTax,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null || pSecCountyTax <= TRUConstants.DOUBLE_ZERO){
			return pSecCountyTax;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if(!omsConfiguration.getTaxSecCountyLocationName().equals(taxDetail.getTaxName())){
						continue;
					}
					if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue()) && 
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue()) &&
							taxDetail.getTaxAmount() != null){
						totalShare += taxDetail.getTaxAmount().doubleValue();
					}
				}
			}
		}
		double proratedSecCountyTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecCountyTax = (pSecCountyTax*pGiftWrapQTY)/quantity;
		}else{
			proratedSecCountyTax = pSecCountyTax-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecCountyTaxShare]");
			vlogDebug("proratedTax: {0}",proratedSecCountyTax);
		}
		return pricingTools.round(proratedSecCountyTax);
	}
	
	/**
	 * This method is use to get the State taxable amount Share.
	 * @param pStateTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedStateTaxableAmt - double prorated tax share
	 */
	private double getStateTaxableAmountShare(double pStateTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getStateTaxableAmountShare]");
			vlogDebug("pStateTax : {0} pShipCIRel:{1}", pStateTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pStateTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxStateLocationName().equals(taxDetail.getTaxName())) ||
							(taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedStateTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedStateTaxableAmt = (pStateTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedStateTaxableAmt = pStateTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getStateTaxableAmountShare]");
			vlogDebug("proratedStateTaxableAmt: {0}",proratedStateTaxableAmt);
		}
		return pricingTools.round(proratedStateTaxableAmt);
	}
	
	/**
	 * This method is use to get the city taxable amount Share.
	 * @param pCityTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedCityTaxableAmt - double prorated tax share
	 */
	private double getCityTaxableAmountShare(double pCityTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getCityTaxableAmountShare]");
			vlogDebug("pCityTax : {0} pShipCIRel:{1}", pCityTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pCityTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxCityLocationName().equals(taxDetail.getTaxName())) ||
							(taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedCityTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedCityTaxableAmt = (pCityTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedCityTaxableAmt = pCityTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getCityTaxableAmountShare]");
			vlogDebug("proratedCityTaxableAmt: {0}",proratedCityTaxableAmt);
		}
		return pricingTools.round(proratedCityTaxableAmt);
	}
	
	/**
	 * This method is use to get the county taxable amount Share.
	 * @param pCountyTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedCountyTaxableAmt - double prorated tax share
	 */
	private double getCountyTaxableAmountShare(double pCountyTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getCountyTaxableAmountShare]");
			vlogDebug("pCountyTax : {0} pShipCIRel:{1}", pCountyTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pCountyTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxCountyLocationName().equals(taxDetail.getTaxName())) ||
							(taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedCountyTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedCountyTaxableAmt = (pCountyTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedCountyTaxableAmt = pCountyTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getCountyTaxableAmountShare]");
			vlogDebug("proratedCountyTaxableAmt: {0}",proratedCountyTaxableAmt);
		}
		return pricingTools.round(proratedCountyTaxableAmt);
	}
	
	/**
	 * This method is use to get the secondary state taxable amount Share.
	 * @param pSecStateTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecStateTaxableAmt - double prorated tax share
	 */
	private double getSecStateTaxableAmountShare(double pSecStateTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecStateTaxableAmountShare]");
			vlogDebug("pSecStateTax : {0} pShipCIRel:{1}", pSecStateTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pSecStateTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxSecStateLocationName().equals(taxDetail.getTaxName())) ||
							(taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedSecStateTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecStateTaxableAmt = (pSecStateTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedSecStateTaxableAmt = pSecStateTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecStateTaxableAmountShare]");
			vlogDebug("proratedSecStateTaxableAmt: {0}",proratedSecStateTaxableAmt);
		}
		return pricingTools.round(proratedSecStateTaxableAmt);
	}
	
	/**
	 * This method is use to get the secondary city taxable amount Share.
	 * @param pSecCityTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecCityTaxableAmt - double prorated tax share
	 */
	private double getSecCityTaxableAmountShare(double pSecCityTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecCityTaxableAmountShare]");
			vlogDebug("pSecCityTax : {0} pShipCIRel:{1}", pSecCityTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pSecCityTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxSecCityLocationName().equals(taxDetail.getTaxName())) ||
							(taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedSecCityTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecCityTaxableAmt = (pSecCityTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedSecCityTaxableAmt = pSecCityTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecCityTaxableAmountShare]");
			vlogDebug("proratedTaxableAmt: {0}",proratedSecCityTaxableAmt);
		}
		return pricingTools.round(proratedSecCityTaxableAmt);
	}
	
	/**
	 * This method is use to get the secondary county taxable amount Share.
	 * @param pSecCountyTaxableAmount - double Amount to be shared
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapQTY - long Item QTY
	 * @param pListOfOrderLine - List of OrderLine
	 * @param pIsBPPItem - Boolean is BPP item
	 * @param pIsNormalItem - Boolean is Normal Item
	 * @param pIsEWasteItem - Boolean is EWaste
	 * @return proratedSecCountyTaxableAmt - double prorated tax share
	 */
	private double getSecCountyTaxableAmountShare(double pSecCountyTaxableAmount,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			long pGiftWrapQTY, List<OrderLine> pListOfOrderLine,
			Boolean pIsBPPItem, Boolean pIsNormalItem, Boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: getSecCountyTaxableAmountShare]");
			vlogDebug("pSecCountyTax : {0} pShipCIRel:{1}", pSecCountyTaxableAmount,pShipCIRel);
			vlogDebug("pGiftWrapQTY : {0} pListOfOrderLine : {1}", pGiftWrapQTY,pListOfOrderLine);
			vlogDebug("pIsBPPItem : {0} pIsNormalItem : {1} pIsEWasteItem:{2}", pIsBPPItem,pIsNormalItem,pIsEWasteItem);
		}
		if(pShipCIRel == null){
			return pSecCountyTaxableAmount;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pShipCIRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderLine != null && !pListOfOrderLine.isEmpty()){
			for(OrderLine orderLine : pListOfOrderLine){
				if(orderLine.getRelationshipId() == null || !orderLine.getRelationshipId().getValue().equals(pShipCIRel.getId())){
					continue;
				}
				totalQty += orderLine.getQuantity().getOrderedQty().longValue();
				if(orderLine.getTaxDetails() == null || orderLine.getTaxDetails().getTaxDetail() == null){
					continue;
				}
				for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
					if((!omsConfiguration.getTaxSecCountyLocationName().equals(taxDetail.getTaxName()))
							|| (taxDetail.getTaxableAmount() == null || taxDetail.getTaxableAmount().getValue() == null)){
						continue;
					}
					try{
						if(pIsEWasteItem && taxDetail.getItemType() != null && EWASTE_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsNormalItem && taxDetail.getItemType() != null && NORMAL_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}else if(pIsBPPItem && taxDetail.getItemType() != null && BPP_ITEM.equals(taxDetail.getItemType().getValue())){
							totalShare += Double.parseDouble(taxDetail.getTaxableAmount().getValue());
						}
					}catch(NumberFormatException nfExp){
						if (isLoggingError()) {
							vlogError("NumberFormatException : {0}", nfExp);
						}
					}
				}
			}
		}
		double proratedSecCountyTaxableAmt = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapQTY<quantity){
			proratedSecCountyTaxableAmt = (pSecCountyTaxableAmount*pGiftWrapQTY)/quantity;
		}else{
			proratedSecCountyTaxableAmt = pSecCountyTaxableAmount-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: getSecCountyTaxableAmountShare]");
			vlogDebug("proratedSecCountyTaxableAmt: {0}",proratedSecCountyTaxableAmt);
		}
		return pricingTools.round(proratedSecCountyTaxableAmt);
	}
	
	/**
	 * This method will populate the Order Line Note detail XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShipGrpCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrderLine - OrderLine
	 * @param pOrder - TRUOrderImpl
	 * @param pIsGiftWrapItem - Boolean Is Gift Wrap Item
	 * @param pGiftItemInfo - RepositoryItem
	 */
	private void populateOrderLineNotesXML(ObjectFactory pObjFactory,
			TRUShippingGroupCommerceItemRelationship pShipGrpCIRel,
			OrderLine pOrderLine, TRUOrderImpl pOrder, Boolean pIsGiftWrapItem,
			RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineNotesXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1} pShipGrpCIRel : {2}", pObjFactory,pOrderLine,pShipGrpCIRel);
			vlogDebug("pOrder : {0} pIsGiftWrapItem : {1} pGiftItemInfo : {2}", pOrder,pIsGiftWrapItem,pGiftItemInfo);
		}
		if(pShipGrpCIRel == null || pObjFactory == null || pOrderLine == null){
			return;
		}
		Notes orderLineNotes = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotes();
		List<Note> listOfNotes = orderLineNotes.getNote();
		Note orderLineNote = null;
		TRUChannelHardgoodShippingGroup channerHGSG = null;
		TRUChannelInStorePickupShippingGroup channelISSG = null;
		TRUHardgoodShippingGroup hardGoodSG = null;
		ShippingGroup shippingGrp = pShipGrpCIRel.getShippingGroup();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		if (isLoggingDebug()) {
			vlogDebug("sg: {0}",shippingGrp);
		}
		String bppSKUID = null;
		String noteText = null;
		if(configuration.isIncludeCR82Changes()){//CR-82 START
			if(shippingGrp != null && shippingGrp.getPriceInfo() != null && 
					shippingGrp.getPriceInfo().getRawShipping() > TRUConstants.DOUBLE_ZERO){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getNoteTypeShipping());
				orderLineNote.setNoteText(getShippingFeeUIDWithDes());
				listOfNotes.add(orderLineNote);
			}
			if(pShipGrpCIRel.getSurcharge() != TRUConstants.DOUBLE_ZERO){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getNoteTypeShipping());
				orderLineNote.setNoteText(getShippingSurUIDWithDes());
				listOfNotes.add(orderLineNote);
			}
			if(pShipGrpCIRel.getE911Fees() != TRUConstants.DOUBLE_ZERO){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getNoteTypeMisc());

				if(pShipGrpCIRel.getCommerceItem() != null){
					RepositoryItem productRef = (RepositoryItem) pShipGrpCIRel.getCommerceItem().getAuxiliaryData().getProductRef();
					orderLineNote.setNoteText(getEWasteNoteText(productRef));
				}
				listOfNotes.add(orderLineNote);
			}
		}//CR-82 END
		if (shippingGrp instanceof TRUHardgoodShippingGroup) {
			hardGoodSG = (TRUHardgoodShippingGroup) shippingGrp;
			String giftMessage = hardGoodSG.getGiftWrapMessage();
			if (isLoggingDebug()) {
				vlogDebug("giftMessage: {0}",giftMessage);
			}
			if(StringUtils.isNotBlank(giftMessage)){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getGiftMessageNoteType());
				orderLineNote.setNoteText(giftMessage);
				listOfNotes.add(orderLineNote);
			}
			if(pIsGiftWrapItem && pGiftItemInfo != null){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getGiftMessageVSNoteType());
				orderLineNote.setNoteCode(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNoteNoteCode
						(configuration.getGiftMessageGWNoteCode()));
				// Calling method to get the Gift Wrap UID
				noteText = getGiftInfoUID(pGiftItemInfo);
				orderLineNote.setNoteText(noteText);
				listOfNotes.add(orderLineNote);
			}
			if(StringUtils.isNotBlank(hardGoodSG.getShippingMethod())){
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				String shipMethod = configuration.getNoteTypeMethodMap().get(hardGoodSG.getShippingMethod());
				if(StringUtils.isNotBlank(shipMethod)){
					orderLineNote.setNoteType(shipMethod);
				}else{
					TRUShippingManager shippingManager = getShippingManager();
					ContactInfo sgShippingAddress = (ContactInfo) hardGoodSG.getShippingAddress();
					final boolean isMilitaryArea =shippingManager. isMilitaryArea(sgShippingAddress);
					final boolean isPOBOXAddress = shippingManager.isPOBOXAddress(sgShippingAddress.getAddress1());
					if(isMilitaryArea || isPOBOXAddress ){
						shipMethod = configuration.getApoFposhippingMethodMap().get(hardGoodSG.getShippingMethod());
						orderLineNote.setNoteType(shipMethod);
					} else {
						shipMethod = configuration.getShippingMethodMap().get(hardGoodSG.getShippingMethod());
						orderLineNote.setNoteType(shipMethod);
					}
				}
				orderLineNote.setNoteText(hardGoodSG.getShippingMethodName()+configuration.getShippingNoteDel()+
						TRUOMSConstant.OPEN_BRACES+hardGoodSG.getDeliveryTimeFrame()+TRUOMSConstant.CLOSED_BRACES);
				listOfNotes.add(orderLineNote);
			}
		}
		if(pShipGrpCIRel != null && pShipGrpCIRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName()) != null){
			RepositoryItem bppItem = (RepositoryItem) pShipGrpCIRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
			bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
			orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
			orderLineNote.setNoteType(configuration.getGiftMessageVSNoteType());
			orderLineNote.setNoteCode(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNoteNoteCode
					(configuration.getGiftMessageBPPNoteCode()));
			if(StringUtils.isNotBlank(bppSKUID)){
				// Calling method to get the BPP UID
				noteText = getBPPInfoUID(bppSKUID);
				orderLineNote.setNoteText(noteText);
			}
			listOfNotes.add(orderLineNote);
		}

		if(shippingGrp instanceof TRUChannelHardgoodShippingGroup){
			channerHGSG = (TRUChannelHardgoodShippingGroup) shippingGrp;
			orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
			orderLineNote.setNoteType(configuration.getRegistryInfoNoteType());
			StringBuilder builder = new StringBuilder();
			builder.append(TRUOMSConstant.CHANNEL_FROM+channerHGSG.getChannelUserName());
			if(!StringUtils.isBlank(channerHGSG.getChannelCoUserName())){
				builder.append(TRUOMSConstant.CHANNEL_AND+channerHGSG.getChannelCoUserName());
			}
			builder.append(TRUOMSConstant.CHANNEL_HASH+channerHGSG.getChannelId());
			if(builder != null){
				orderLineNote.setNoteText(builder.toString());
			}
			listOfNotes.add(orderLineNote);
		}
		if(shippingGrp instanceof TRUChannelInStorePickupShippingGroup){
			channelISSG = (TRUChannelInStorePickupShippingGroup) shippingGrp;
			orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
			orderLineNote.setNoteType(configuration.getRegistryInfoNoteType());
			StringBuilder builder = new StringBuilder();
			builder.append(TRUOMSConstant.CHANNEL_FROM+channelISSG.getChannelUserName());
			if(!StringUtils.isBlank(channelISSG.getChannelCoUserName())){
				builder.append(TRUOMSConstant.CHANNEL_AND+channelISSG.getChannelCoUserName());
			}
			builder.append(TRUOMSConstant.CHANNEL_HASH+channelISSG.getChannelId());
			if(builder != null){
				orderLineNote.setNoteText(builder.toString());
			}
			listOfNotes.add(orderLineNote);
		}
		if (pShipGrpCIRel != null && pShipGrpCIRel.getCommerceItem() != null) {
			CommerceItem commerceItem = pShipGrpCIRel.getCommerceItem();
			TRUItemPriceInfo info = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			if (null != info.getDealID()) {
				orderLineNote = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineNotesNote();
				orderLineNote.setNoteType(configuration.getTprElement());
				StringBuilder builder = new StringBuilder();
				builder.append(info.getDealID()).append(TRUOMSConstant.CHAR_PIPE_DELIMETER);
				builder.append(info.getListPrice()).append(TRUOMSConstant.CHAR_PIPE_DELIMETER);
				builder.append(info.getSalePrice());
				if (builder != null) {
					orderLineNote.setNoteText(builder.toString());
				}
				listOfNotes.add(orderLineNote);
			}
		}
		if(orderLineNotes != null && orderLineNotes.getNote() != null && !orderLineNotes.getNote().isEmpty()){
			pOrderLine.setNotes(orderLineNotes);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineNotesXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Reference Filed XML Parameters.
	 * @param pObjectFactory - ObjectFactory
	 * @param pShipCIRel - TRUShippingGroupCommerceItemRelationship
	 * @param pOrderLine - OrderLine
	 * @param pIsGiftWrap - Boolean - is Gift Wrap Item
	 * @param pOrder - Order
	 * @param pGiftItemInfo - RepositoryItem Item
	 */
	private void populateOrderLineReferenceXML(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pShipCIRel,
			OrderLine pOrderLine, Boolean pIsGiftWrap, RepositoryItem pGiftItemInfo, atg.commerce.order.Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineReferenceXML]");
			vlogDebug("pOrderLine : {0} pObjectFactory : {1}", pOrderLine,pObjectFactory);
			vlogDebug("pIsGiftWrap : {0} pShipCIRel : {1}", pIsGiftWrap,pShipCIRel);
		}
		if(pShipCIRel == null || pOrderLine == null || pObjectFactory == null){
			return;
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) ((CustomCatalogTools)getOrderManager().getCatalogTools()).getCatalogProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		CommerceItem cItem = pShipCIRel.getCommerceItem();
		String siteId = cItem.getAuxiliaryData().getSiteId();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem skuItem = (RepositoryItem) cItem.getAuxiliaryData().getCatalogRef();
		if(isLoggingDebug()){
			vlogDebug("cItem: {0} skuItem : {1} siteId : {2}", cItem,skuItem,siteId);
		}
		LineReferenceFields orderLineReferenceFields = pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFields();
		boolean slapper = Boolean.FALSE;
		if(skuItem != null && !((String) skuItem.getPropertyValue(catalogProperties.getSkuType())).equals
				(catalogProperties.getNonMerchSKUType()) && 
				skuItem.getPropertyValue(catalogProperties.getSlapperItemPropertyName()) != null){
			slapper = (Boolean) skuItem.getPropertyValue(catalogProperties.getSlapperItemPropertyName());
		}
		if(slapper){
			orderLineReferenceFields.setReferenceField2(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField2
					(omsConfiguration.getSlapper()));
		}else{
			orderLineReferenceFields.setReferenceField2(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField2
					(omsConfiguration.getNonSlapper()));
		}
		// Calling method to get the subType of Gift Wrap SKU.
		String skuSubType = getNonMerchSKUSubType(pGiftItemInfo,pOrder);
		if(StringUtils.isNotBlank(skuSubType)){
			if(omsConfiguration.getTruGuftWrapType().equals(skuSubType)){
				orderLineReferenceFields.setReferenceField3(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField3
						(omsConfiguration.getTruSiteParameter()));
			}
			if(omsConfiguration.getBruGiftWrapType().equals(skuSubType)){
				orderLineReferenceFields.setReferenceField3(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField3
						(omsConfiguration.getBruSiteParameter()));
			}
		}
		if(pIsGiftWrap){
			orderLineReferenceFields.setReferenceNumber1(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceNumber1
					(TRUConstants.DIGIT_ONE));
		}else{
			orderLineReferenceFields.setReferenceNumber1(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceNumber1
					(TRUConstants.DIGIT_ZERO));
		}
		orderLineReferenceFields.setReferenceField9(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField9
				(TRUCommerceConstants.YES));
		ShippingGroup shipGroup = pShipCIRel.getShippingGroup();
		if(shipGroup instanceof TRUChannelHardgoodShippingGroup){
			String channelType = ((TRUChannelHardgoodShippingGroup)shipGroup).getChannelType();
			if(isLoggingDebug()){
				vlogDebug("channelType: {0} shipGroup : {1}", channelType,shipGroup);
			}
			orderLineReferenceFields.setReferenceField4(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField4
					(((TRUChannelHardgoodShippingGroup)shipGroup).getChannelId()));
			if(!StringUtils.isBlank(channelType)){
				if(channelType.equalsIgnoreCase(commercePropertyManager.getWishlist())){
					orderLineReferenceFields.setReferenceField5(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField5
							(omsConfiguration.getWishListRefField5()));
				}else if(channelType.equalsIgnoreCase(commercePropertyManager.getRegistry())){
					orderLineReferenceFields.setReferenceField5(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField5
							(omsConfiguration.getRegistryRefField5()));
				}
			}
		}
		if(shipGroup instanceof TRUChannelInStorePickupShippingGroup){
			String channelType = ((TRUChannelInStorePickupShippingGroup)shipGroup).getChannelType();
			if(isLoggingDebug()){
				vlogDebug("channelType: {0} shipGroup : {1}", channelType,shipGroup);
			}
			orderLineReferenceFields.setReferenceField4(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField4
					(((TRUChannelInStorePickupShippingGroup)shipGroup).getChannelId()));
			if(!StringUtils.isBlank(channelType)){
				if(channelType.equalsIgnoreCase(commercePropertyManager.getWishlist())){
					orderLineReferenceFields.setReferenceField5(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField5
							(omsConfiguration.getWishListRefField5()));
				}else if(channelType.equalsIgnoreCase(commercePropertyManager.getRegistry())){
					orderLineReferenceFields.setReferenceField5(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceField5
							(omsConfiguration.getRegistryRefField5()));
				}
			}
		}
		if(shipGroup instanceof TRUHardgoodShippingGroup && !isPayInStoreOrder(pOrder)){
			orderLineReferenceFields.setReferenceNumber3(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceNumber3
					(getEstimateShippingDate(pShipCIRel.getEstimateShipWindowMin())));
			orderLineReferenceFields.setReferenceNumber4(pObjectFactory.createTXMLMessageOrderOrderLinesOrderLineLineReferenceFieldsReferenceNumber4
					(getEstimateShippingDate(pShipCIRel.getEstimateShipWindowMax())));
		}
		pOrderLine.setLineReferenceFields(orderLineReferenceFields);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineReferenceXML]");
		}
	}
	
	/**
	 * This method will populate the Order Line Customer Attribute XML Parameters.
	 * @param pObjFactory - ObjectFactory
	 * @param pShippingGroup - ShippingGroup
	 * @param pOrderLine - OrderLine
	 */
	private void populateOrderLineCustomerAttributesXML(ObjectFactory pObjFactory,
			ShippingGroup pShippingGroup, OrderLine pOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateOrderLineCustomerAttributesXML]");
			vlogDebug("pObjFactory : {0} pShippingGroup : {1} pOrderLine : {2}", pObjFactory,pShippingGroup,pOrderLine);
		}
		if(pOrderLine == null || pObjFactory == null || 
				!(pShippingGroup instanceof TRUInStorePickupShippingGroup)){
			return;
		}
		TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pShippingGroup;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		CustomFieldList orderLineCustomFieldList = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldList();
		List<CustomField> customFields = orderLineCustomFieldList.getCustomField();
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getFirstName())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getPrimaryFirstName()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getFirstName()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getLastName())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getPrimaryLastName()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getLastName()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getEmail())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getPrimaryEmail()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getEmail()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getPhoneNumber())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getPrimaryPhone()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(formatPhoneNumer(inStorePickupShippingGroup.getPhoneNumber())));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltFirstName())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getProxyFirstName()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getAltFirstName()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltLastName())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getProxyLastName()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getAltLastName()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltEmail())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getProxyEmail()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(inStorePickupShippingGroup.getAltEmail()));
			customFields.add(customField);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltPhoneNumber())){
			CustomField customField = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomField();
			customField.setName(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldName
					(omsConfiguration.getProxyPhone()));
			customField.setValue(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineCustomFieldListCustomFieldValue
					(formatPhoneNumer(inStorePickupShippingGroup.getAltPhoneNumber())));
			customFields.add(customField);
		}
		if(customFields != null && !customFields.isEmpty()){
			pOrderLine.setCustomFieldList(orderLineCustomFieldList);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateOrderLineCustomerAttributesXML]");
		}
	}

	/**
	 * This method is use to remove the extra parameter, we are added to handle the
	 * proration logic.
	 * @param pListOfOrderLine - List of OrderLine
	 */
	private void removeExtraParameterFromXML(List<OrderLine> pListOfOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: removeExtraParameterFromXML]");
			vlogDebug("pListOfOrderLine : {0}", pListOfOrderLine);
		}
		if(pListOfOrderLine == null || pListOfOrderLine.isEmpty()){
			return ;
		}
		for(OrderLine lOrderLine : pListOfOrderLine){
			lOrderLine.setShippingGroupId(null);
			lOrderLine.setRelationshipId(null);
			lOrderLine.setCommerceItemId(null);
			if(lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null	&& 
					!lOrderLine.getChargeDetails().getChargeDetail().isEmpty()){
				for(ChargeDetail chargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
					chargeDetail.setItemType(null);
				}
			}
			if(lOrderLine.getTaxDetails() != null && lOrderLine.getTaxDetails().getTaxDetail() != null	&& 
					!lOrderLine.getTaxDetails().getTaxDetail().isEmpty()){
				for(TaxDetail taxDetail : lOrderLine.getTaxDetails().getTaxDetail()){
					taxDetail.setItemType(null);
				}
			}
			
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: removeExtraParameterFromXML]");
		}
	}
	
	/**
	 * This method will populate CustomerOrderObjectXML.
	 * @param pLayawayOrder - TRULayawayOrderImpl
	 * @return customerOrderXML - customerOrderXML
	 */
	public String populateLayawayOrderObjectXML(TRULayawayOrderImpl pLayawayOrder){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayOrderObjectXML]");
			vlogDebug("pLayawayOrder : {0}", pLayawayOrder);
		}
		String customerOrderXML = null;
		if(pLayawayOrder != null){
			// Added Performance statement 
			if(PerformanceMonitor.isEnabled()){
				PerformanceMonitor.startOperation(TRUOMSConstant.POPULATE_LAYAWAY_ORDER_OBJECT);
			}
			ObjectFactory obj = new ObjectFactory();
			TXML tXML = obj.createTXML();
			populateXMLHeaderParameters(obj,tXML,COI_SERVICE,pLayawayOrder.getLayawayOrderid());//Populate header parameters for Order xml
			setTaxCounter(TRUConstants.INTEGER_NUMBER_ONE);
			// Calling method to populate the customer 1st name and last name.
			setCustomerFirstLastName(pLayawayOrder);
			populateLayawayXMLMessageParameters(obj,tXML,pLayawayOrder);
			// Calling method to generate the XML
			customerOrderXML = generateXML(tXML);
			// Setting back 1st name and last name as Null.
		    setFirstName(null);
		    setLastName(null);
		    setPhoneNumber(null);
		}
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(TRUOMSConstant.POPULATE_LAYAWAY_ORDER_OBJECT);
		}
		if (isLoggingDebug()) {
			vlogDebug("Customer Order XML : {0}", customerOrderXML);
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayOrderObjectXML]");
		}
		return customerOrderXML;
	}
	
	/**
	 * This method will populate Message parameters for Order xml.
	 * @param pObjFactory - ObjectFactory
	 * @param pTXML - tXML
	 * @param pLayawayOrder - TRULayawayOrderImpl
	 */
	private void populateLayawayXMLMessageParameters(ObjectFactory pObjFactory, TXML pTXML, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayXMLMessageParameters]");
			vlogDebug("pObjFactory: {0} pTXML : {1} pLayawayOrder : {2}",pObjFactory,pTXML,pLayawayOrder);
		}
		if(pObjFactory == null || pTXML == null){
			return;
		}
		Message messageXmlObj = pObjFactory.createTXMLMessage();
		Order orderXmlObj = pObjFactory.createTXMLMessageOrder();
		// Calling method to set the Order Header Parameter.
		populateLayawayOrderXMLParameters(pObjFactory,orderXmlObj,messageXmlObj,pLayawayOrder);
		// Calling method to set the Payment details.
		populateXMLPaymentDetails(pObjFactory,orderXmlObj,pLayawayOrder);
		// Calling method to set the Customer Info.
		populateXMLLayawayCustomerInfo(pObjFactory,orderXmlObj,pLayawayOrder);
		// Calling method to set the Order Reference Fields.
		populateXMLLayawayOrderRefFields(pObjFactory,orderXmlObj,pLayawayOrder);
		// Calling method to set the Order Line.
		populateXMLLayawayOrderOrderLines(pObjFactory,orderXmlObj,pLayawayOrder);
		messageXmlObj.setOrder(orderXmlObj);
		pTXML.setMessage(messageXmlObj);
		// Setting back the PayPal and Credit card flag to false.
		setPaypalPG(Boolean.FALSE);
		setCreditCardPG(Boolean.FALSE);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayXMLMessageParameters]");
		}
	}

	/**
	 * Method to populate the Order message Parameter specific to Layaway Orders.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pMessageXmlObj - Message
	 * @param pLayawayOrder - TRULayawayOrderImpl
	 */
	private void populateLayawayOrderXMLParameters(ObjectFactory pObjFactory,
			Order pOrderXmlObj, Message pMessageXmlObj,
			TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayOrderXMLParameters]");
			vlogDebug("pObjFactory: {0} pOrderXmlObj : {1}",pObjFactory,pOrderXmlObj);
			vlogDebug("pMessageXmlObj: {0} pLayawayOrder : {1}",pMessageXmlObj,pLayawayOrder);
		}
		if(pLayawayOrder == null || pObjFactory == null || pOrderXmlObj == null){
			return;
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		String onHoldStatus = null;
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(configuration.getEstTimeZone()));
		pOrderXmlObj.setOrderNumber(pObjFactory.createTXMLMessageOrderOrderNumber(pLayawayOrder.getLayawayOrderid()));
		if(pLayawayOrder.getSubmittedDate() != null){
			pOrderXmlObj.setOrderCaptureDate(pObjFactory.createTXMLMessageOrderOrderCaptureDate(df.format(pLayawayOrder.getSubmittedDate())));
		}
		pOrderXmlObj.setOrderType(pObjFactory.createTXMLMessageOrderOrderType(configuration.getOrderTypeMap().get(ORDER_TYPE_LAYAWAY_KEY)));
		pOrderXmlObj.setOrderSubtotal(pObjFactory.createTXMLMessageOrderOrderSubtotal(String.valueOf(pricingTools.round(pLayawayOrder.getPaymentAmount()))));
		pOrderXmlObj.setOrderTotal(pObjFactory.createTXMLMessageOrderOrderTotal(String.valueOf(pricingTools.round(pLayawayOrder.getPaymentAmount()))));
		if(StringUtils.isNotBlank(pLayawayOrder.getSourceOfOrder())){
			pOrderXmlObj.setEntryType(pObjFactory.createTXMLMessageOrderEntryType(pLayawayOrder.getSourceOfOrder()));
		}else{
			pOrderXmlObj.setEntryType(pObjFactory.createTXMLMessageOrderEntryType(configuration.getOrderEntryTypeWeb()));
		}
		pOrderXmlObj.setConfirmed(pObjFactory.createTXMLMessageOrderConfirmed(TRUE));
		pOrderXmlObj.setCanceled(pObjFactory.createTXMLMessageOrderCanceled(FALSE));
		pOrderXmlObj.setMinimizeShipments(pObjFactory.createTXMLMessageOrderMinimizeShipments(TRUE));
		if(isPayInStoreOrder(pLayawayOrder)){
			pOrderXmlObj.setOnHold(pObjFactory.createTXMLMessageOrderOnHold(TRUE));
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getOrderStatusForPIS()));
		}else{
			onHoldStatus = String.valueOf(getFraudStatus(pLayawayOrder));
			pOrderXmlObj.setOnHold(pObjFactory.createTXMLMessageOrderOnHold(onHoldStatus));
			pOrderXmlObj.setPaymentStatus(pObjFactory.createTXMLMessageOrderPaymentStatus(configuration.getOrderPaymentStatus()));
		}
		if(StringUtils.isNotBlank(pLayawayOrder.getEnteredBy())){
			pOrderXmlObj.setEnteredBy(pObjFactory.createTXMLMessageOrderEnteredBy(pLayawayOrder.getEnteredBy()));
		}
		if(StringUtils.isNotBlank(pLayawayOrder.getEnteredLocation())){
			pOrderXmlObj.setEnteredLocation(pObjFactory.createTXMLMessageOrderEnteredLocation(pLayawayOrder.getEnteredLocation()));
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayOrderXMLParameters]");
		}
		
	}
	
	/**
	 * This method will populate the Customer Info XML Parameters for layaway Order.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pLayawayOrder - TRULayawayOrderImpl
	 */
	private void populateXMLLayawayCustomerInfo(ObjectFactory pObjFactory,Order pOrderXmlObj, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLLayawayCustomerInfo]");
			vlogDebug("pOrderXmlObj: {0} pObjFactory : {1}",pOrderXmlObj,pObjFactory);
			vlogDebug("pLayawayOrder : {0}",pLayawayOrder);
		}
		if(pObjFactory == null || pLayawayOrder == null){
			return;
		}
		RepositoryItem profile;
		try {
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pLayawayOrder);
			TRUPropertyManager propertyManager = getPropertyManager();
			CustomerInfo customerInfo = pObjFactory.createTXMLMessageOrderCustomerInfo();
			if(profile != null && !profile.isTransient()){
				customerInfo.setCustomerId(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerId(pLayawayOrder.getProfileId()));
				customerInfo.setCustomerUserId(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerUserId(
						(String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName())));
				customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
						(String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName())));
				customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
						(String) profile.getPropertyValue(propertyManager.getLastNamePropertyName())));
				customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerPhone(
						formatPhoneNumer(pLayawayOrder.getPhoneNumber())));
				customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
						(String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName())));
			}else{
				customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
						getFirstName()));
				customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
						getFirstName()));
				customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
						formatPhoneNumer(pLayawayOrder.getPhoneNumber())));
				customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
						pLayawayOrder.getEmail()));
			}
			if(customerInfo != null){
				if(customerInfo.getCustomerFirstName() == null || StringUtils.isBlank(customerInfo.getCustomerFirstName().getValue())){
					customerInfo.setCustomerFirstName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerFirstName(
							getFirstName()));
				}
				if(customerInfo.getCustomerLastName() == null || StringUtils.isBlank(customerInfo.getCustomerLastName().getValue())){
					customerInfo.setCustomerLastName(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerLastName(
							getLastName()));
				}
				if(customerInfo.getCustomerPhone() == null || StringUtils.isBlank(customerInfo.getCustomerPhone().getValue())){
					customerInfo.setCustomerPhone(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
							formatPhoneNumer(pLayawayOrder.getPhoneNumber())));
				}
				if(customerInfo.getCustomerEmail() == null || StringUtils.isBlank(customerInfo.getCustomerEmail().getValue())){
					customerInfo.setCustomerEmail(pObjFactory.createTXMLMessageOrderCustomerInfoCustomerEmail(
							pLayawayOrder.getEmail()));
				}
				pOrderXmlObj.setCustomerInfo(customerInfo);
			}
			
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLLayawayCustomerInfo]");
		}
	}
	
	/**
	 * This method will populate the Order Reference Fields XML Parameters for Layaway Order.
	 * @param pObjFactory - ObjectFactory
	 * @param pOrderXmlObj - Order
	 * @param pOrder - TRULayawayOrderImpl
	 */
	private void populateXMLLayawayOrderRefFields(ObjectFactory pObjFactory,
			Order pOrderXmlObj, TRULayawayOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLLayawayOrderRefFields]");
			vlogDebug("pObjFactory: {0} pOrderXmlObj : {1} pOrder:{2}",pObjFactory,pOrderXmlObj,pOrder);
		}
		if(pObjFactory == null || pOrderXmlObj == null || pOrder == null){
			return;
		}
		PricingTools pricingTools = getOrderManager().getPromotionTools().getPricingTools();
		ReferenceFields orderReferenceFiled = pObjFactory.createTXMLMessageOrderReferenceFields();
		String siteId = pOrder.getSiteId();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		orderReferenceFiled.setReferenceField1(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField1(pOrder.getLayawayOMSOrderId()));
		if(!StringUtils.isBlank(siteId)){
			if(siteId.contains(omsConfiguration.getTruSiteId())){
				orderReferenceFiled.setReferenceField4(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField4
						(omsConfiguration.getOrderRefField4ForTRU()));
			}else if(siteId.contains(omsConfiguration.getBruSiteId())){
				orderReferenceFiled.setReferenceField4(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceField4
						(omsConfiguration.getOrderRefField4ForBRU()));
			}
		}
		orderReferenceFiled.setReferenceNumber1(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceNumber1(
				Double.toString(pricingTools.round(pOrder.getPaymentAmount()))));
		orderReferenceFiled.setReferenceNumber2(pObjFactory.createTXMLMessageOrderReferenceFieldsReferenceNumber2(
				Double.toString(pricingTools.round(pOrder.getLayawayDueAmount()))));
		pOrderXmlObj.setReferenceFields(orderReferenceFiled);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLLayawayOrderRefFields]");
		}
	}
	
	/**Method to populate the Layaway Order Line details.
	 * 
	 * @param pObjFactory - ObjectFactory Object
	 * @param pOrderXmlObj - Order Object
	 * @param pLayawayOrder - TRULayawayOrderImpl Object
	 */
	private void populateXMLLayawayOrderOrderLines(ObjectFactory pObjFactory,
			Order pOrderXmlObj, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateXMLLayawayOrderOrderLines]");
			vlogDebug("pObjFactory : {0} pOrderXmlObj : {1} pLayawayOrder : {2}", pObjFactory,pOrderXmlObj,pLayawayOrder);
		}
		if(pObjFactory == null || pOrderXmlObj == null || pLayawayOrder == null){
			return;
		}
		OrderLines orderLines = pObjFactory.createTXMLMessageOrderOrderLines();
		List<OrderLine> listOfOrderLine = orderLines.getOrderLine();
		OrderLine orderLine = pObjFactory.createTXMLMessageOrderOrderLinesOrderLine();
		orderLine.setLineNumber(TRUConstants.DIGIT_ONE);
		orderLine.setItemID(getLayawayItemUID());
		orderLine.setLineTotal(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineLineTotal(
				Double.toString(pLayawayOrder.getPaymentAmount())));
		// Calling method to set the Price Details for Layaway Order.
		populateLayawayOrderLinePriceDetailXML(pObjFactory, orderLine, TRUConstants.LONG_ONE,pLayawayOrder);
		// Calling method to populate the Order line quantity details.
		populateOrderLineQuantityDetailXML(pObjFactory,orderLine,TRUConstants.LONG_ONE);
		// Calling method to populate the Shipping Info details.
		populateLayawayShippingInfoXML(pObjFactory, orderLine, pLayawayOrder);
		listOfOrderLine.add(orderLine);
		pOrderXmlObj.setOrderLines(orderLines);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateXMLLayawayOrderOrderLines]");
		}
	}
	
	/**
	 * @param pObjFactory - ObjectFactory Object
	 * @param pOrderLine - OrderLine Object
	 * @param pQuantity - long : quantity
	 * @param pLayawayOrder - TRULayawayOrderImpl Object
	 */
	private void populateLayawayOrderLinePriceDetailXML(
			ObjectFactory pObjFactory, OrderLine pOrderLine,
			long pQuantity, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayOrderLinePriceDetailXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
			vlogDebug("pLayawayOrder : {0} pQuantity : {1}",pLayawayOrder,pQuantity);
		}
		if(pObjFactory == null || pOrderLine == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		PriceInfo orderLinePriceInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLinePriceInfo();
		orderLinePriceInfo.setPrice(BigDecimal.valueOf(pricingTools.round(pLayawayOrder.getPaymentAmount())));
		pOrderLine.setPriceInfo(orderLinePriceInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayOrderLinePriceDetailXML]");
		}
	}

	/**
	 * Method to populate the Shipping Info details for Layaway Order line.
	 * 
	 * @param pObjFactory - ObjectFactory Object
	 * @param pOrderLine - OrderLine Object
	 * @param pLayawayOrder - TRULayawayOrderImpl Object
	 */
	private void populateLayawayShippingInfoXML(ObjectFactory pObjFactory,
			OrderLine pOrderLine, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayShippingInfoXML]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1} pLayawayOrder : {2}", pObjFactory,pOrderLine,pLayawayOrder);
		}
		if(pObjFactory == null || pOrderLine == null || pLayawayOrder == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingInfo shippingInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfo();
		shippingInfo.setShipVia(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipVia(omsConfiguration.getLayawayShipVia()));
		shippingInfo.setDeliveryOption(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoDeliveryOption(
				omsConfiguration.getLayawayDeliveryOption()));
		// Calling method to set the Shipping Info for Layaway Order.
		populateLayawayShipInfoShipAddXML(pObjFactory,shippingInfo,pLayawayOrder);
		pOrderLine.setShippingInfo(shippingInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayShippingInfoXML]");
		}
	}

	/**
	 * Method to populate the Shipping Add to Shipping Info for Layaway order line. 
	 * 
	 * @param pObjFactory - ObjectFactory Object
	 * @param pShippingInfo - ShippingInfo Object
	 * @param pLayawayOrder - TRULayawayOrderImpl Object
	 */
	private void populateLayawayShipInfoShipAddXML(ObjectFactory pObjFactory,
			ShippingInfo pShippingInfo, TRULayawayOrderImpl pLayawayOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateLayawayShipInfoShipAddXML]");
			vlogDebug("pObjFactory : {0} pShippingInfo : {1} pLayawayOrder : {2}", pObjFactory,pShippingInfo,pLayawayOrder);
		}
		if(pObjFactory == null || pShippingInfo == null || pLayawayOrder == null){
			return;
		}
		String country = null;
		String phoneNumber = null;
		ShippingAddress shipAddInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddress();
		shipAddInfo.setShipToFirstName(pLayawayOrder.getFirstName());
		shipAddInfo.setShipToLastName(pLayawayOrder.getLastName());
		shipAddInfo.setShipToAddressLine1(pLayawayOrder.getAddress1());
		if(StringUtils.isNotBlank(pLayawayOrder.getAddress2())){
			shipAddInfo.setShipToAddressLine2(
					pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToAddressLine2(
					pLayawayOrder.getAddress2()));
		}
		shipAddInfo.setShipToCity(pLayawayOrder.getCity());
		shipAddInfo.setShipToState(pLayawayOrder.getAddrState());
		shipAddInfo.setShipToPostalCode(pLayawayOrder.getPostalCode());
		country = getOmsConfiguration().getCountryCodeMap().get(pLayawayOrder.getCountry());
		if(StringUtils.isNotBlank(country)){
			shipAddInfo.setShipToCountry(country);
		}else{
			shipAddInfo.setShipToCountry(pLayawayOrder.getCountry());
		}
		phoneNumber = formatPhoneNumer(pLayawayOrder.getPhoneNumber());
		shipAddInfo.setShipToPhone(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToPhone(
				phoneNumber));
		shipAddInfo.setShipToEmail(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShippingAddressShipToEmail(
				pLayawayOrder.getEmail()));
		pShippingInfo.setShippingAddress(shipAddInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateLayawayShipInfoShipAddXML]");
		}
	}
	
	/**
	 * Method to populate the Shipping/Fulfillment info for Donation Items.
	 * @param pObjFactory - Object of ObjectFactory
	 * @param pOrderLine - Object of OrderLine
	 */
	private void populateShippingInfoForDonation(ObjectFactory pObjFactory,OrderLine pOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSXMLUtils  method: populateShippingInfoForDonation]");
			vlogDebug("pObjFactory : {0} pOrderLine : {1}", pObjFactory,pOrderLine);
		}
		if(pObjFactory == null || pOrderLine == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		com.tru.xml.customerorder.TXML.Message.Order.OrderLines.OrderLine.ShippingInfo orderLineShippingInfo = 
				pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfo();
		orderLineShippingInfo.setDeliveryOption(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoDeliveryOption(
				omsConfiguration.getDeliveryOptionForDonation()));
		orderLineShippingInfo.setShipToFacility(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineShippingInfoShipToFacility(
				omsConfiguration.getShipToFacilityForDonation()));
		AllocationInfo orderLineAllocationInfo = pObjFactory.createTXMLMessageOrderOrderLinesOrderLineAllocationInfo();
		orderLineAllocationInfo.setFulfillmentFacility(pObjFactory.createTXMLMessageOrderOrderLinesOrderLineAllocationInfoFulfillmentFacility
				(omsConfiguration.getFulfillmentForDonation()));
		pOrderLine.setAllocationInfo(orderLineAllocationInfo);
		pOrderLine.setShippingInfo(orderLineShippingInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSXMLUtils  method: populateShippingInfoForDonation]");
		}
		
	}
}