package com.tru.integration.oms;

import java.util.Map;

import atg.nucleus.GenericService;

/**
 * This is configuration file
 * @author PA
 * @version 1.0
 *
 */
public class TRUOMSBaseConfiguration extends GenericService {
	/**Holds mOrderXMLHeaderSource */
	private String mOrderXMLHeaderSource;
	/**Holds mOrderXMLHeaderActionTypeCreate */
	private String mOrderXMLHeaderActionTypeCreate;
	/**Holds mOrderXMLHeaderActionTypeUpdate */
	private String mOrderXMLHeaderActionTypeUpdate;
	/**Holds mOrderXMLHeaderMessageType */
	private String mOrderXMLHeaderMessageType;
	/**Holds mOrderXMLHeaderCompanyId */
	private String mOrderXMLHeaderCompanyId;
	/**Holds sendXMLMessageFromSource */
	private boolean mSendXMLMessageFromSource;
	/**Holds mApoFposhippingMethodMap */
	private Map<String, String> mApoFposhippingMethodMap;
	/**Holds includeCR165Changes */
	private boolean mIncludeCR165Changes;
	/**Holds layawayOrderListEntityType */
	private String mLayawayOrderListEntityType;
	/**
	 * This method returns the orderXMLHeaderSource value
	 *
	 * @return the orderXMLHeaderSource
	 */
	public String getOrderXMLHeaderSource() {
		return mOrderXMLHeaderSource;
	}
	/**
	 * This method sets the orderXMLHeaderSource with parameter value pOrderXMLHeaderSource
	 *
	 * @param pOrderXMLHeaderSource the orderXMLHeaderSource to set
	 */
	public void setOrderXMLHeaderSource(String pOrderXMLHeaderSource) {
		mOrderXMLHeaderSource = pOrderXMLHeaderSource;
	}
	/**
	 * This method returns the orderXMLHeaderActionTypeCreate value
	 *
	 * @return the orderXMLHeaderActionTypeCreate
	 */
	public String getOrderXMLHeaderActionTypeCreate() {
		return mOrderXMLHeaderActionTypeCreate;
	}
	/**
	 * This method sets the orderXMLHeaderActionTypeCreate with parameter value pOrderXMLHeaderActionTypeCreate
	 *
	 * @param pOrderXMLHeaderActionTypeCreate the orderXMLHeaderActionTypeCreate to set
	 */
	public void setOrderXMLHeaderActionTypeCreate(
			String pOrderXMLHeaderActionTypeCreate) {
		mOrderXMLHeaderActionTypeCreate = pOrderXMLHeaderActionTypeCreate;
	}
	/**
	 * This method returns the orderXMLHeaderActionTypeUpdate value
	 *
	 * @return the orderXMLHeaderActionTypeUpdate
	 */
	public String getOrderXMLHeaderActionTypeUpdate() {
		return mOrderXMLHeaderActionTypeUpdate;
	}
	/**
	 * This method sets the orderXMLHeaderActionTypeUpdate with parameter value pOrderXMLHeaderActionTypeUpdate
	 *
	 * @param pOrderXMLHeaderActionTypeUpdate the orderXMLHeaderActionTypeUpdate to set
	 */
	public void setOrderXMLHeaderActionTypeUpdate(
			String pOrderXMLHeaderActionTypeUpdate) {
		mOrderXMLHeaderActionTypeUpdate = pOrderXMLHeaderActionTypeUpdate;
	}
	/**
	 * This method returns the orderXMLHeaderMessageType value
	 *
	 * @return the orderXMLHeaderMessageType
	 */
	public String getOrderXMLHeaderMessageType() {
		return mOrderXMLHeaderMessageType;
	}
	/**
	 * This method sets the orderXMLHeaderMessageType with parameter value pOrderXMLHeaderMessageType
	 *
	 * @param pOrderXMLHeaderMessageType the orderXMLHeaderMessageType to set
	 */
	public void setOrderXMLHeaderMessageType(String pOrderXMLHeaderMessageType) {
		mOrderXMLHeaderMessageType = pOrderXMLHeaderMessageType;
	}
	/**
	 * This method returns the orderXMLHeaderCompanyId value
	 *
	 * @return the orderXMLHeaderCompanyId
	 */
	public String getOrderXMLHeaderCompanyId() {
		return mOrderXMLHeaderCompanyId;
	}
	/**
	 * This method sets the orderXMLHeaderCompanyId with parameter value pOrderXMLHeaderCompanyId
	 *
	 * @param pOrderXMLHeaderCompanyId the orderXMLHeaderCompanyId to set
	 */
	public void setOrderXMLHeaderCompanyId(String pOrderXMLHeaderCompanyId) {
		mOrderXMLHeaderCompanyId = pOrderXMLHeaderCompanyId;
	}
	/**
	 * This method returns the sendXMLMessageFromSource value
	 *
	 * @return the sendXMLMessageFromSource
	 */
	public boolean isSendXMLMessageFromSource() {
		return mSendXMLMessageFromSource;
	}
	/**
	 * This method sets the sendXMLMessageFromSource with parameter value pSendXMLMessageFromSource
	 *
	 * @param pSendXMLMessageFromSource the sendXMLMessageFromSource to set
	 */
	public void setSendXMLMessageFromSource(boolean pSendXMLMessageFromSource) {
		mSendXMLMessageFromSource = pSendXMLMessageFromSource;
	}
	/**
	 * This method returns the apoFposhippingMethodMap value
	 *
	 * @return the apoFposhippingMethodMap
	 */
	public Map<String, String> getApoFposhippingMethodMap() {
		return mApoFposhippingMethodMap;
	}
	/**
	 * This method sets the apoFposhippingMethodMap with parameter value pApoFposhippingMethodMap
	 *
	 * @param pApoFposhippingMethodMap the apoFposhippingMethodMap to set
	 */
	public void setApoFposhippingMethodMap(
			Map<String, String> pApoFposhippingMethodMap) {
		mApoFposhippingMethodMap = pApoFposhippingMethodMap;
	}
	/**Holds mNoteTypeMethodMap */
	private Map<String, String> mNoteTypeMethodMap;
	/**
	 * @return the noteTypeMethodMap
	 */
	public Map<String, String> getNoteTypeMethodMap() {
		return mNoteTypeMethodMap;
	}
	/**
	 * @param pNoteTypeMethodMap the noteTypeMethodMap to set
	 */
	public void setNoteTypeMethodMap(Map<String, String> pNoteTypeMethodMap) {
		mNoteTypeMethodMap = pNoteTypeMethodMap;
	}
	/**
	 * This method returns the includeCR165Changes value
	 *
	 * @return the includeCR165Changes
	 */
	public boolean isIncludeCR165Changes() {
		return mIncludeCR165Changes;
	}
	/**
	 * This method sets the includeCR165Changes with parameter value pIncludeCR165Changes
	 *
	 * @param pIncludeCR165Changes the includeCR165Changes to set
	 */
	public void setIncludeCR165Changes(boolean pIncludeCR165Changes) {
		mIncludeCR165Changes = pIncludeCR165Changes;
	}
	/**
	 * This method gets layawayOrderListEntityType value
	 *
	 * @return the layawayOrderListEntityType value
	 */
	public String getLayawayOrderListEntityType() {
		return mLayawayOrderListEntityType;
	}
	/**
	 * This method sets the layawayOrderListEntityType with pLayawayOrderListEntityType value
	 *
	 * @param pLayawayOrderListEntityType the layawayOrderListEntityType to set
	 */
	public void setLayawayOrderListEntityType(String pLayawayOrderListEntityType) {
		mLayawayOrderListEntityType = pLayawayOrderListEntityType;
	}
}
