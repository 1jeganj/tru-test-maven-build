package com.tru.integration.oms;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.common.TRUConfiguration;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 *This class is for OM business logic.
 *@author PA.
 *@version 1.0.
 */
public class TRUIntegrationManager extends GenericService {
	
	/**
	 * Holds reference for TRUOrderManager. 
	 */
	private TRUOrderManager mOrderManager;
	
	/**
	 * Holds reference for TRUOMSUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	
	/**
	 * Holds reference for TRUOMSAuditTools.
	 */
	private TRUOMSAuditTools mOmsAuditTools;

	/**
	 * Holds reference for mConfiguration. 
	 */
	private TRUConfiguration mConfiguration;

	/**
	 * Holds reference for mOmsErrorTools. 
	 */
	private TRUOMSErrorTools mOmsErrorTools;

	/**
	 * Method to create a json request for customer import and call the OMS service 
	 * and update the Audit and Error repository.
	 * 
	 * @param pCustomerMasterJson : String - JSON request
	 * @param pCustomerId : String - Customer/Profile Id
	 * @param pOrderId : String - Order Id
	 */
	public void callOMSServiceForCustomerImport(String pCustomerMasterJson, String pCustomerId, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: callOMSServiceForCustomerImport]");
			vlogDebug("pCustomerId : {0} pCustomerMasterJson : {1}", pCustomerId,pCustomerMasterJson);
		}
		if(StringUtils.isBlank(pCustomerMasterJson)){
			return;
		}
		String customerDetailJSONResponse = null;
		TRUOMSUtils omsUtils = getOmsUtils();
		TRUOMSConfiguration configuration = omsUtils.getOmsConfiguration();
		try {
			customerDetailJSONResponse = getOmsUtils().omsCustomerMasterServiceCall(TRUOMSConstant.EMPTY_STRING,TRUOMSConstant.EMPTY_STRING
					, getConfiguration().getOmsCustomerMasterServiceURL(),pCustomerMasterJson);
			boolean saveOmsTransactionDetails = configuration.isSaveOmsTransactionDetails();
			String customerMasterImportTransPrefix = configuration.getCustomerMasterImportTransPrefix();
			Date date = Calendar.getInstance().getTime();
			if(saveOmsTransactionDetails){
				getOmsAuditTools().createOMSAuditItem(customerMasterImportTransPrefix+pCustomerId, pCustomerMasterJson, 
						customerDetailJSONResponse, TRUOMSConstant.CUSTOMER_MASTER_IMPORT, TRUOMSConstant.SUCCESS, date);
			}
			String responseStatus = getOmsAuditTools().getOrderRepsonseStausForAudit(customerDetailJSONResponse,TRUOMSConstant.CUSTOMER_MASTER_IMPORT);
			if ((!StringUtils.isBlank(responseStatus))&& responseStatus.equals(TRUOMSConstant.ERROR)) {
				// Create OMS Error Repository Item
				getOmsErrorTools().createOMSErrorItem(customerMasterImportTransPrefix + pCustomerId
						,configuration.getCustomerMasterImportServiceName(),pCustomerMasterJson,pOrderId);
			}
		} catch (IOException exc) {
			if (isLoggingError()) {
				logError("IOException in TRUIntegrationManager.callOMSServiceForCustomerImport", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: callOMSServiceForCustomerImport]");
		}
	}

	/**
	 * Method to create a json request for update order and call the OMS service 
	 * and update the Audit and Error repository.
	 * 
	 * @param pCustomerOrderJSON - String - JSON request
	 * @param pOrderId : String - Order Id
	 */
	public void callOMSServiceForUpdateOrder(String pCustomerOrderJSON, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: callOMSServiceForUpdateOrder]");
			vlogDebug("pOrderId : {0} pCustomerOrderJSON : {1}", pOrderId,pCustomerOrderJSON);
		}
		if(StringUtils.isBlank(pCustomerOrderJSON)){
			return;
		}
		String customerOrderDetailJSONResponse = null;
		try {
			TRUOMSUtils omsUtils = getOmsUtils();
			TRUOMSConfiguration configuration = omsUtils.getOmsConfiguration();
			//
			customerOrderDetailJSONResponse = omsUtils.omsCOImportServiceCall(TRUOMSConstant.EMPTY_STRING,TRUOMSConstant.EMPTY_STRING, 
					getConfiguration().getOmsCustomerOrderImportServiceURL(), pCustomerOrderJSON);
			boolean saveOmsTransactionDetails = configuration.isSaveOmsTransactionDetails();
			String customerOrderUpdateTransPrefix = configuration.getCustomerOrderUpdateTransPrefix();
			Date date = Calendar.getInstance().getTime();
			if(saveOmsTransactionDetails){
				getOmsAuditTools().createOMSAuditItem(customerOrderUpdateTransPrefix+pOrderId, pCustomerOrderJSON, customerOrderDetailJSONResponse, 
						TRUOMSConstant.ORDER_UPDATE_SERVICE, TRUOMSConstant.SUCCESS, date);
			}
			//
			String responseStatus = getOmsAuditTools().getOrderRepsonseStausForAudit(customerOrderDetailJSONResponse,TRUOMSConstant.ORDER_UPDATE_SERVICE);
			if ((!StringUtils.isBlank(responseStatus))&& responseStatus.equals(TRUOMSConstant.ERROR)) {
				// Create OMS Error Repository Item
				getOmsErrorTools().createOMSErrorItem(customerOrderUpdateTransPrefix + pOrderId
						,configuration.getOrderUpdateServiceName(),pCustomerOrderJSON,pOrderId);
			}
		} catch (IOException exc) {
			if (isLoggingError()) {
				logError("IOException in TRUIntegrationManager.callOMSServiceForCustomerImport", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: callOMSServiceForUpdateOrder]");
		}
	}

	/**
	 * Method to create a json request for customer order import and call the OMS service 
	 * and update the Audit and Error repository.
	 * 
	 * @param pOrder : Order Object
	 */
	public void createCustomerOrderImportRequest(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: createCustomerOrderImportRequest]");
			vlogDebug("pOrder : {0}", pOrder);
		}
		if(pOrder == null){
			return;
		}
		String customerOrderJSON = null;
		String orderId = null;
		TRULayawayOrderImpl layawayOrderImpl = null;
		if(pOrder instanceof TRUOrderImpl){
			orderId = pOrder.getId();
			customerOrderJSON = generateCORequest(orderId);
		}else if(pOrder instanceof TRULayawayOrderImpl){
			layawayOrderImpl = (TRULayawayOrderImpl) pOrder;
			orderId = layawayOrderImpl.getId();
			customerOrderJSON = generateLayawayOrderRequest(layawayOrderImpl,pOrder);
		}
		if (isLoggingDebug()) {
			vlogDebug("orderId : {0}", orderId);
		}
		//Process the generated CO request
		processCOImportRequest(customerOrderJSON, orderId);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: createCustomerOrderImportRequest]");
		}
	}

	/**
	 * Generate Customer order request object.
	 * 
	 * @param pOrderId
	 *            - Order Id
	 * @return customerOrderJSON - JSON response
	 */
	public String generateCORequest(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: generateCORequest]");
		}
		TRUOrderImpl order = null;
		String customerOrderJSON = null;
		try {
			order = (TRUOrderImpl) getOrderManager().loadOrder(pOrderId);
			//Populate customer order object
			customerOrderJSON = getOmsUtils().populateCustomerOrderObject(order);
		} catch (CommerceException exc) {
			if(!StringUtils.isBlank(pOrderId)) {
				getOmsErrorTools().updateOmsErrorItemForOrder(pOrderId, TRUOMSConstant.CUSTOMER_ORDER_IMPORT_SERVICE_CALL);
				}
			if(isLoggingError()){
				vlogError("CommerceException : occurred while loading order : {0} with exception : {1}", order,  exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: generateCORequest]");
		}
		return customerOrderJSON;
	}

	/**
	 * Method to get the Layaway Order json request
	 * @param pLayawayOrderImpl : TRULayawayOrderImpl Object
	 * @param pOrder : Order Object
	 * @return customerOrderJSON - customer order json
	 */
	public String generateLayawayOrderRequest(
			TRULayawayOrderImpl pLayawayOrderImpl, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: generateLayawayOrderRequest]");
		}
		String customerOrderJSON = null;
		if(pLayawayOrderImpl == null){
			return customerOrderJSON;
		}
		//Populate Layaway order object
		customerOrderJSON = getOmsUtils().populateLayawayOrderObject(pLayawayOrderImpl);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: generateLayawayOrderRequest]");
		}
		return customerOrderJSON;
		
	}
	
	/**
	 * Generate Customer order XML request object.
	 * 
	 * @param pOrderId
	 *            - Order Id
	 * @return customerOrderJSON - JSON response
	 */
	public String generateCOXMLRequest(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: generateCOXMLRequest]");
		}
		TRUOrderImpl order = null;
		String customerOrderXML = null;
		TRUOMSXMLUtils omsUtils = (TRUOMSXMLUtils) getOmsUtils();
		try {
			order = (TRUOrderImpl) getOrderManager().loadOrder(pOrderId);
			//Populate customer order object
			customerOrderXML = omsUtils.populateCustomerOrderObjectXML(order);
			
		} catch (CommerceException exc) {
			if(!StringUtils.isBlank(pOrderId)) {
				getOmsErrorTools().updateOmsErrorItemForOrder(pOrderId,TRUOMSConstant.CUSTOMER_ORDER_IMPORT_SERVICE_CALL);
				}
			if(isLoggingError()){
				vlogError("CommerceException : occurred while loading order : {0} with exception : {1}", order,  exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: generateCOXMLRequest]");
		}
		return customerOrderXML;
	}
	
	/**
	 * Method to get the Layaway Order xml request
	 * @param pLayawayOrderImpl : TRULayawayOrderImpl Object
	 * @return customerOrderXML - customer order XML
	 */
	public String generateXMLLayawayOrderRequest(
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: generateXMLLayawayOrderRequest]");
		}
		String customerOrderXML = null;
		if(pLayawayOrderImpl == null){
			return customerOrderXML;
		}
		TRUOMSXMLUtils omsUtils = (TRUOMSXMLUtils) getOmsUtils();
		//Populate Layaway order object
		customerOrderXML = omsUtils.populateLayawayOrderObjectXML(pLayawayOrderImpl);
		
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: generateXMLLayawayOrderRequest]");
		}
		return customerOrderXML;
		
	}
	
	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}
	
	/**
	 * @return the omsAuditTools
	 */
	public TRUOMSAuditTools getOmsAuditTools() {
		return mOmsAuditTools;
	}
	
	/**
	 * @return the omsErrorTools
	 */
	public TRUOMSErrorTools getOmsErrorTools() {
		return mOmsErrorTools;
	}

	/**
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}
	
	/**
	 * @param pCustomerOrderImportRequest - String import request
	 * @param pOrderId - String OrderId
	 */
	public void processCOImportRequest(String pCustomerOrderImportRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOImportRequest]");
		}
		if(pCustomerOrderImportRequest == null){
			return;
		}
		try {
			String customerOrderDetailJSONResponse = null;
			TRUOMSConfiguration configuration = getOmsUtils().getOmsConfiguration();
			boolean saveOmsTransactionDetails = configuration.isSaveOmsTransactionDetails();
			String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
			if (!StringUtils.isBlank(pCustomerOrderImportRequest)) {
				customerOrderDetailJSONResponse = getOmsUtils().omsCOImportServiceCall(TRUOMSConstant.EMPTY_STRING,TRUOMSConstant.EMPTY_STRING
						,getConfiguration().getOmsCustomerOrderImportServiceURL(),
						pCustomerOrderImportRequest);
				String responseStatus = getOmsAuditTools().getOrderRepsonseStausForAudit(customerOrderDetailJSONResponse,
						TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE);
				// Create Audit repository item
				if (saveOmsTransactionDetails) {
					getOmsAuditTools().createOMSAuditItem(customerOrderImportTransPrefix + pOrderId,
							pCustomerOrderImportRequest, customerOrderDetailJSONResponse,TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE, null,null);
				}
				if ((!StringUtils.isBlank(responseStatus))&& responseStatus.equals(TRUOMSConstant.ERROR)) {
					// Create OMS Error Repository Item
					getOmsErrorTools().createOMSErrorItem(customerOrderImportTransPrefix + pOrderId
							,configuration.getCoiServiceName(),pCustomerOrderImportRequest,pOrderId);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("Customer Master Import Service Response: {0} ",
						customerOrderDetailJSONResponse);
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				logError("IOException in TRUIntegrationManager.callOMSServiceForCustomerImport", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOImportRequest]");
		}
	}
	
	/**
	 * This method processes the customer order import XML request.
	 * @param pCustomerOrderImportRequest - CO import JSON request
	 * @param pOrderId - Order Id
	 */
	public void processCOImportXMLRequest(String pCustomerOrderImportRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOImportXMLRequest]");
		}
		if(pCustomerOrderImportRequest == null){
			return;
		}
		String customerOrderDetailJSONResponse = null;
		TRUOMSConfiguration configuration = getOmsUtils().getOmsConfiguration();
		boolean saveOmsTransactionDetails = configuration.isSaveOmsTransactionDetails();
		String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
		if ((!StringUtils.isBlank(pCustomerOrderImportRequest)) && saveOmsTransactionDetails) {
			// Create Audit repository item
			getOmsAuditTools().createOMSAuditItem(customerOrderImportTransPrefix + pOrderId,
					pCustomerOrderImportRequest, customerOrderDetailJSONResponse,TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE, null,null);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOImportXMLRequest]");
		}
	}
	
	/**
	 * This method processes the customer order update XML request.
	 * @param pCustomerOrderUpdateXMLRequest - CO import JSON request
	 * @param pOrderId - Order Id
	 */
	public void processCOUpdateXMLRequest(String pCustomerOrderUpdateXMLRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOUpdateXMLRequest]");
		}
		if(pCustomerOrderUpdateXMLRequest == null){
			return;
		}
		String customerOrderUpdateResponse = null;
		TRUOMSConfiguration configuration = getOmsUtils().getOmsConfiguration();
		boolean saveOmsTransactionDetails = configuration.isSaveOmsTransactionDetails();
		String customerOrderUpdateTransPrefix = configuration.getCustomerOrderUpdateTransPrefix();
		if ((!StringUtils.isBlank(pCustomerOrderUpdateXMLRequest)) && saveOmsTransactionDetails) {
			// Create Audit repository item
			getOmsAuditTools().createOMSAuditItem(customerOrderUpdateTransPrefix + pOrderId,
					pCustomerOrderUpdateXMLRequest, customerOrderUpdateResponse,TRUOMSConstant.ORDER_UPDATE_SERVICE, null,null);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: processCOUpdateXMLRequest]");
		}
	}
	
	/**
	 * Method to create a XML request for customer order import and posts the XML to  the OMS Queue 
	 * and update the Audit and Error repository.
	 * 
	 * @param pOrder : Order Object
	 * @return String - request XML
	 */
	public String createCustomerOrderImportXMLRequest(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUIntegrationManager  method: createCustomerOrderImportXMLRequest]");
			vlogDebug("pOrder : {0}", pOrder);
		}
		String customerOrderXML = null;
		String orderId = null;
		TRULayawayOrderImpl layawayOrderImpl = null;
		if(pOrder != null){
			if(pOrder instanceof TRUOrderImpl){
				orderId = pOrder.getId();
				customerOrderXML = generateCOXMLRequest(orderId);
			}else if(pOrder instanceof TRULayawayOrderImpl){
				layawayOrderImpl = (TRULayawayOrderImpl) pOrder;
				customerOrderXML = generateXMLLayawayOrderRequest(layawayOrderImpl);
			}
			if (isLoggingDebug()) {
				vlogDebug("orderId : {0}", orderId);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUIntegrationManager  method: createCustomerOrderImportXMLRequest]");
		}
		return customerOrderXML;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}

	/**
	 * @param pOmsAuditTools the omsAuditTools to set
	 */
	public void setOmsAuditTools(TRUOMSAuditTools pOmsAuditTools) {
		mOmsAuditTools = pOmsAuditTools;
	}

	/**
	 * @param pOmsErrorTools the omsErrorTools to set
	 */
	public void setOmsErrorTools(TRUOMSErrorTools pOmsErrorTools) {
		mOmsErrorTools = pOmsErrorTools;
	}

	/**
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
}
