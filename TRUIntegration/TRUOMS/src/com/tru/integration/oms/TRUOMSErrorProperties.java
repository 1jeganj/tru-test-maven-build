package com.tru.integration.oms;

/**
 * This class is used to hold the OMS error repository properties and item descriptor's name. 
 * @author Professional Access
 * @version 1.0
 */
public class TRUOMSErrorProperties {
	/**
	 *Property to hold  omsErrorRecordItemDescriptorName.
	 */
	private String mOmsErrorRecordItemDescriptorName;
	/**
	 *Property to hold  omsServiceIdPopertyName.
	 */
	private String mOmsServiceIdPopertyName;
	/**
	 *Property to hold  omsSeriveTypePopertyName.
	 */
	private String mOmsSeriveTypePopertyName;
	/**
	 *Property to hold  omsRequestPopertyName.
	 */
	private String mOmsRequestPopertyName;
	/**
	 *Property to hold  idPopertyName.
	 */
	private String mIdPopertyName;
	/**
	 * @return the omsErrorRecordItemDescriptorName
	 */
	public String getOmsErrorRecordItemDescriptorName() {
		return mOmsErrorRecordItemDescriptorName;
	}
	/**
	 * @param pOmsErrorRecordItemDescriptorName the omsErrorRecordItemDescriptorName to set
	 */
	public void setOmsErrorRecordItemDescriptorName(
			String pOmsErrorRecordItemDescriptorName) {
		mOmsErrorRecordItemDescriptorName = pOmsErrorRecordItemDescriptorName;
	}
	/**
	 * @return the omsServiceIdPopertyName
	 */
	public String getOmsServiceIdPopertyName() {
		return mOmsServiceIdPopertyName;
	}
	/**
	 * @param pOmsServiceIdPopertyName the omsServiceIdPopertyName to set
	 */
	public void setOmsServiceIdPopertyName(String pOmsServiceIdPopertyName) {
		mOmsServiceIdPopertyName = pOmsServiceIdPopertyName;
	}
	/**
	 * @return the omsSeriveTypePopertyName
	 */
	public String getOmsSeriveTypePopertyName() {
		return mOmsSeriveTypePopertyName;
	}
	/**
	 * @param pOmsSeriveTypePopertyName the omsSeriveTypePopertyName to set
	 */
	public void setOmsSeriveTypePopertyName(String pOmsSeriveTypePopertyName) {
		mOmsSeriveTypePopertyName = pOmsSeriveTypePopertyName;
	}
	/**
	 * @return the omsRequestPopertyName
	 */
	public String getOmsRequestPopertyName() {
		return mOmsRequestPopertyName;
	}
	/**
	 * @param pOmsRequestPopertyName the omsRequestPopertyName to set
	 */
	public void setOmsRequestPopertyName(String pOmsRequestPopertyName) {
		mOmsRequestPopertyName = pOmsRequestPopertyName;
	}
	/**
	 * @return the idPopertyName
	 */
	public String getIdPopertyName() {
		return mIdPopertyName;
	}
	/**
	 * @param pIdPopertyName the idPopertyName to set
	 */
	public void setIdPopertyName(String pIdPopertyName) {
		mIdPopertyName = pIdPopertyName;
	}
	/**
	 *Property to hold  orderIdPopertyName.
	 */
	private String mOrderIdPopertyName;
	/**
	 * @return the orderIdPopertyName
	 */
	public String getOrderIdPopertyName() {
		return mOrderIdPopertyName;
	}
	/**
	 * @param pOrderIdPopertyName the orderIdPopertyName to set
	 */
	public void setOrderIdPopertyName(String pOrderIdPopertyName) {
		this.mOrderIdPopertyName = pOrderIdPopertyName;
	}
	/**
	 * Holds mRequestSendCountPropertyName.
	 * 
	 */
	private String mRequestSendCountPropertyName;

	/**
	 * Gets the RequestSendCountPropertyName.
	 *
	 * @return the requestSendCountPropertyName
	 */
	
	public String getRequestSendCountPropertyName() {
		return mRequestSendCountPropertyName;
	}
	
	/**
	 * Sets the requestSendCountPropertyName.
	 *
	 * @param pRequestSendCountPropertyName    requestSendCountPropertyName to set
	 */
	public void setRequestSendCountPropertyName(
			String pRequestSendCountPropertyName) {
		this.mRequestSendCountPropertyName = pRequestSendCountPropertyName;
	}
	/**
	 * Holds transactionTimePopertyName.
	 */
	private String mTransactionTimePopertyName;
	
	/**
	 * Gets the transaction time poperty name.
	 *
	 * @return the transactionTimePopertyName
	 */
	public String getTransactionTimePopertyName() {
		return mTransactionTimePopertyName;
	}

	/**
	 * Sets the transaction time poperty name.
	 *
	 * @param pTransactionTimePopertyName            the transactionTimePopertyName to set
	 */
	public void setTransactionTimePopertyName(String pTransactionTimePopertyName) {
		mTransactionTimePopertyName = pTransactionTimePopertyName;
	}
}
