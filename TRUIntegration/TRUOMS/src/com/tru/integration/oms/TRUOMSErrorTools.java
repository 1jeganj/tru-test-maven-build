package com.tru.integration.oms;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.messaging.oms.TRUCustomerMasterImportMessage;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is overriding OOTB GenericService class.
 * 
 * This class is used to update the OMS error repository. 
 *  @author Professional Access
 *  @version 1.0
 */
public class TRUOMSErrorTools extends GenericService{
	
	/** Holds reference for mOmsErrorRepository. */
	private GSARepository mOmsErrorRepository;
	
	/** Holds reference for mOmsConfiguration. */
	private TRUOMSConfiguration mOmsConfiguration;
	
	/** Holds reference for mOmsErrorProperties. */
	private TRUOMSErrorProperties mOmsErrorProperties;
	
	/** Holds reference for mTransactionManager. */
	private TransactionManager mTransactionManager;
	
	/** mPropertyManager. */
	private TRUPropertyManager mPropertyManager;
	
	/** mProfileTools. */
	private TRUProfileTools mProfileTools;
	
	/**
	 * This holds reference for Integration manager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	

	/**
	 * Method to store the request in OMS error repository.
	 * 
	 * @param pOMSSeriveId
	 *            - String : ServiceId
	 * @param pServiceType
	 *            - String : Service Type COI/Customer Import/Cancel
	 *            Order/Update Order
	 * @param pJSONRequest
	 *            - String : JSON request            
	 * @param pOrderId          
	 * 			  - String : OrderId 
	 */
	public void createOMSErrorItem(String pOMSSeriveId, String pServiceType,
			String pJSONRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: createOMSErrorItem]");
			vlogDebug( "pOMSSeriveId : {0} pServiceType : {1} pJSONRequest : {2}", pOMSSeriveId, pServiceType, pJSONRequest);
		}
		MutableRepository omsRepository = getOmsErrorRepository();
		TRUOMSErrorProperties omsErrorProperties = getOmsErrorProperties();
		MutableRepositoryItem omsErrorItem;
		if (omsRepository != null) {
			try {
				omsErrorItem = omsRepository.createItem(omsErrorProperties.getOmsErrorRecordItemDescriptorName());
				omsErrorItem.setPropertyValue(omsErrorProperties.getOmsServiceIdPopertyName(),pOMSSeriveId);
				omsErrorItem.setPropertyValue(omsErrorProperties.getOmsSeriveTypePopertyName(),pServiceType);
				omsErrorItem.setPropertyValue(omsErrorProperties.getOmsRequestPopertyName(),pJSONRequest);
				omsErrorItem.setPropertyValue(omsErrorProperties.getOrderIdPopertyName(),pOrderId);
				omsErrorItem.setPropertyValue(omsErrorProperties.getRequestSendCountPropertyName(),TRUConstants._0);
				omsErrorItem.setPropertyValue(omsErrorProperties.getTransactionTimePopertyName(),new Timestamp((new Date()).getTime()));
				omsRepository.addItem(omsErrorItem);
				getOmsErrorRepository().invalidateCaches();
			} catch (RepositoryException re) {
				if(isLoggingError()){
					vlogError(re, "RepositoryException in method createOMSErrorItem");
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: createOMSErrorItem]");
		}
	}
	
	/**
	 * Method to delete the item after getting the success response from OMS service.
	 * 
	 * @param pReositoryId : String - Repository Item id
	 */
	public void deleteOMSErrorItem(String pReositoryId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: deleteOMSErrorItem]");
			vlogDebug("pReositoryId : {0}",pReositoryId);
		}
		if(StringUtils.isBlank(pReositoryId)){
			return;
		}
		try {
			getOmsErrorRepository().removeItem(pReositoryId, getOmsErrorProperties().getOmsErrorRecordItemDescriptorName());
			getOmsErrorRepository().invalidateCaches();
		} catch (RepositoryException re) {
			if(isLoggingError()){
				vlogError(re, "RepositoryException in method updateOmsErrorItemForProfile");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: deleteOMSErrorItem]");
		}
	}

	/**
	 * Method to get the all requests for COI OMS service. Key is a repository item id and value is Request.
	 * @param pDuration - duration in days
	 * @param pNoOfReqToSend 
	 * @return Map - Map Object
	 */
	public Map<String, String> getAllCOIFailedRequest(int pDuration, int pNoOfReqToSend) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: getAllCOIFailedRequest]");
		}
		RepositoryItem[] omsTransItems = null;
		RepositoryView transItemView = null;
		Map<String, String> coiRequestMap = null;
		int i = TRUConstants.ONE;
		try {
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			transItemView = getIntegrationManager().getOmsAuditTools().getAuditRepository().getView(omsConfiguration.getOmsTransRecordsItemDescriptorName());
			if(transItemView != null){
				QueryBuilder queryBuilder = transItemView.getQueryBuilder();
				QueryExpression serviceType = queryBuilder.createConstantQueryExpression(TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE);
				QueryExpression serviceTypeProperty = queryBuilder.createPropertyQueryExpression(omsConfiguration.getTypePopertyName());
				Query createServiceTypeQuery = queryBuilder.createComparisonQuery(serviceType, serviceTypeProperty, QueryBuilder.EQUALS);
				
				QueryExpression typeQueryExp = queryBuilder.createConstantQueryExpression(TRUOMSConstant.ERROR);
				QueryExpression typeConstantExp = queryBuilder.createPropertyQueryExpression(omsConfiguration.getStatusPopertyName());
				Query createErrorQuery = queryBuilder.createComparisonQuery(typeQueryExp, typeConstantExp, QueryBuilder.EQUALS);
				
				QueryExpression transactionTimePropertyExp = queryBuilder.createPropertyQueryExpression(omsConfiguration.getTransactionTimePopertyName());
				//Get Old date
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, - pDuration);
				Date oldDate = cal.getTime();
				QueryExpression oldDateConstantExp = queryBuilder.createConstantQueryExpression(oldDate);
				Query transactionTimeQuery = queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.GREATER_THAN);
				
				Query[] finalQuery = {createServiceTypeQuery,createErrorQuery,transactionTimeQuery};
				Query createAndQuery = queryBuilder.createAndQuery(finalQuery);
				omsTransItems = transItemView.executeQuery(createAndQuery);
				String coiRequest = null;
				if(omsTransItems != null && omsTransItems.length > TRUConstants.INT_ZERO){
					for(RepositoryItem omsTansItem : omsTransItems){
						MutableRepositoryItem omsTansactionItem = (MutableRepositoryItem) omsTansItem;
						int requestSendCount = (int) omsTansactionItem.getPropertyValue(omsConfiguration.getRequestSendCountPropertyName());
						if (requestSendCount >= TRUConstants._1 ) {
							requestSendCount++;
							omsTansactionItem.setPropertyValue(omsConfiguration.getRequestSendCountPropertyName(), requestSendCount);
						} else {
							omsTansactionItem.setPropertyValue(omsConfiguration.getRequestSendCountPropertyName(), i);
						}
						if(isLoggingDebug()){
							vlogDebug("Customer order impot XML item to resend : {0}", omsTansactionItem);
						}
						coiRequest = (String) omsTansactionItem.getPropertyValue(omsConfiguration.getRequestPopertyName());
						if(StringUtils.isBlank(coiRequest)){
							continue;
						}
						if(coiRequestMap == null){
							coiRequestMap = new HashMap<String, String>();
						}
						if ((int)omsTansactionItem.getPropertyValue(omsConfiguration.getRequestSendCountPropertyName()) <= pNoOfReqToSend) {
							coiRequestMap.put((String) omsTansactionItem.getPropertyValue(omsConfiguration.getTransactionIdPopertyName()), coiRequest);
						}
					}
				}
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getAllCOIFailedRequest", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: getAllCOIFailedRequest]");
		}
		return coiRequestMap;
	}

	/**
	 * Method to get the repositoryItem based on orderId for Update Order OMS service.
	 * @param pOrderId -  orderId
	 * @return RepositoryItem - RepositoryItem[] Object
	 */
	public RepositoryItem[] getUpdateOrderFailedRequest(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: getAllUpdateOrderFailedRequest]");
		}
		RepositoryItem[] omsTransItems = null;
		RepositoryView transItemView = null;
			
		try {
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			transItemView = getIntegrationManager().getOmsAuditTools().getAuditRepository().getView(omsConfiguration.getOmsTransRecordsItemDescriptorName());
			if(transItemView != null && pOrderId != null){
				QueryBuilder queryBuilder = transItemView.getQueryBuilder();
				String orderId = TRUConstants.ORDER_ID_PREPEND+pOrderId;
				QueryExpression transactionId = queryBuilder.createConstantQueryExpression(orderId);
				QueryExpression transactionIdProperty = queryBuilder.createPropertyQueryExpression(omsConfiguration.getTransactionIdPopertyName());
				Query transactionIdQuery = queryBuilder.createComparisonQuery(transactionId, transactionIdProperty, QueryBuilder.EQUALS);
				omsTransItems = transItemView.executeQuery(transactionIdQuery);
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getUpdateOrderFailedRequest", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: getUpdateOrderFailedRequest]");
		}
		return omsTransItems;
	}

	/**
	 * Method to get the all requests for Customer Master Import  OMS service.
	 * @param pDuration -  duration
	 * @param pNoOfReqToSend -  noOfReqToSend
	 * @return List - List Object
	 */
	public List<RepositoryItem> getAllCustomerImportFailedRequest(int pDuration, int pNoOfReqToSend) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: getAllCustomerImportFailedRequest]");
		}
		RepositoryItem[] omsErrorItems = null;
		RepositoryView omsErrorView = null;
		List<RepositoryItem> customerImportRepositoryItems = null;
		int i = TRUConstants.ONE;
		try {
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			TRUOMSErrorProperties omsErrorProperties = getOmsErrorProperties();
			omsErrorView = getOmsErrorRepository().getView(getOmsErrorProperties().getOmsErrorRecordItemDescriptorName());
			if(omsErrorView != null){
				QueryBuilder queryBuilder = omsErrorView.getQueryBuilder();
				QueryExpression serviceType = queryBuilder.createConstantQueryExpression(omsConfiguration.getCustomerMasterImportServiceName());
				QueryExpression serviceTypeProperty = queryBuilder.
						createPropertyQueryExpression(omsErrorProperties.getOmsSeriveTypePopertyName());
				Query createComparisonQuery = queryBuilder.createComparisonQuery(serviceType, serviceTypeProperty, QueryBuilder.EQUALS);
				QueryExpression transactionTimePropertyExp = queryBuilder.createPropertyQueryExpression(omsConfiguration.getTransactionTimePopertyName());
				//Get Old date
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, - pDuration);
				Date oldDate = cal.getTime();
				QueryExpression oldDateConstantExp = queryBuilder.createConstantQueryExpression(oldDate);
				Query transactionTimeQuery = queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.GREATER_THAN);
				Query[] finalQuery = {createComparisonQuery,transactionTimeQuery};
				Query createAndQuery = queryBuilder.createAndQuery(finalQuery);
				omsErrorItems = omsErrorView.executeQuery(createAndQuery);
				String coiRequest = null;
				if(omsErrorItems != null && omsErrorItems.length > TRUConstants.INT_ZERO){
					for(RepositoryItem omsError : omsErrorItems){
						MutableRepositoryItem omsErrorTansactionItem = (MutableRepositoryItem) omsError;
						int requestSendCount = (int) omsErrorTansactionItem.getPropertyValue(omsErrorProperties.getRequestSendCountPropertyName());
						if (requestSendCount >= TRUConstants._1 ) {
							requestSendCount++;
							omsErrorTansactionItem.setPropertyValue(omsErrorProperties.getRequestSendCountPropertyName(), requestSendCount);
						} else {
							omsErrorTansactionItem.setPropertyValue(omsErrorProperties.getRequestSendCountPropertyName(), i);
						}
						coiRequest = (String) omsErrorTansactionItem.getPropertyValue(omsErrorProperties.getOmsRequestPopertyName());
						if(StringUtils.isBlank(coiRequest)){
							continue;
						}
						if(customerImportRepositoryItems == null){
							customerImportRepositoryItems = new ArrayList<RepositoryItem>();
						}
						
						if ((int)omsErrorTansactionItem.getPropertyValue(omsErrorProperties.getRequestSendCountPropertyName()) <= pNoOfReqToSend) {
							customerImportRepositoryItems.add(omsErrorTansactionItem);
						}
						
					}
				}
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getAllCustomerImportFailedRequest", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: getAllCustomerImportFailedRequest]");
		}
		return customerImportRepositoryItems;
	}
	/**
	 * Gets the OMS message items.
	 * @param pDurationToSendRequestAgain  - duration time
	 *
	 * @return errorMessageItems - Repository items
	 */
	public RepositoryItem[] getOMSMessageItems(int pDurationToSendRequestAgain) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: getOMSMessageItems]");
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		MutableRepository errorRepository = getOmsErrorRepository();
		RepositoryItem[] errorMessageItems= null;
		try {
			RepositoryView errorMessageView = errorRepository.getView(getPropertyManager().getOmsErrorMessageItemName());
			QueryBuilder queryBuilder = errorMessageView.getQueryBuilder();
			QueryExpression transactionTimePropertyExp = queryBuilder.createPropertyQueryExpression(configuration.getTransactionTimePopertyName());
			//Get Old date
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, - pDurationToSendRequestAgain);
			Date oldDate = cal.getTime();
			QueryExpression oldDateConstantExp = queryBuilder.createConstantQueryExpression(oldDate);
			Query transactionTimeQuery = queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.GREATER_THAN);
			errorMessageItems = errorMessageView.executeQuery(transactionTimeQuery);
			if(isLoggingError() && errorMessageItems != null){
				vlogError("errorMessageItems length : {0} ", errorMessageItems.length);
			}
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSErrorTools  method: getOMSMessageItems]");
		}
		return errorMessageItems;
	}


	/**
	 * Generate customer master object.
	 *
	 * @param pProfileId the profile id
	 * 
	 * @param pOrderId the order id
	 */
	public void generateCustomerMasterObject(String pProfileId, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: generateCustomerMasterObject]");
		}
		RepositoryItem profileItem = null;
		String populateCustomerMasterImportJson = null;
		TRUPropertyManager propertyManager = getPropertyManager();
		try {
			profileItem = getProfileTools().getProfileRepository().getItem(pProfileId, propertyManager.getUserItemName());
			if(profileItem != null){
				TRUCustomerMasterImportMessage customerMasterImportMessage = new TRUCustomerMasterImportMessage();
				customerMasterImportMessage.setCustomerFirstName((String) profileItem.getPropertyValue(propertyManager.getFirstNamePropertyName()));
				customerMasterImportMessage.setCustomerLastName((String) profileItem.getPropertyValue(propertyManager.getLastNamePropertyName()));
				customerMasterImportMessage.setCustomerEmail((String) profileItem.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerMasterImportMessage.setExternalCustomerId(profileItem.getRepositoryId());
				String rewardNumber = (String) profileItem.getPropertyValue(propertyManager.getRewardNumberPropertyName());
				if(!StringUtils.isBlank(rewardNumber)){
					customerMasterImportMessage.setReferenceField10( (String) profileItem.getPropertyValue(propertyManager.getRewardNumberPropertyName()));
				}
				 populateCustomerMasterImportJson = getIntegrationManager().getOmsUtils().populateCustomerMasterImportDetails(customerMasterImportMessage);
				 if (!StringUtils.isEmpty(populateCustomerMasterImportJson)) {
					 getIntegrationManager().callOMSServiceForCustomerImport(populateCustomerMasterImportJson, pProfileId, pOrderId);
				 }
			}
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("TRUOMSErrorTools :: RepositoryException ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSErrorTools  method: generateCustomerMasterObject]");
		}
	}
	
	/**
	 * Generate customer order object.
	 *
	 * @param pOrderId the order id
	 */
	public void generateCustomerOrderObject(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: generateCustomerOrderObject]");
		}
		String customerOrderImportRequest = null;
		customerOrderImportRequest = getIntegrationManager().generateCORequest(pOrderId);
		 if (!StringUtils.isEmpty(customerOrderImportRequest)) {
			 getIntegrationManager().processCOImportRequest(customerOrderImportRequest, pOrderId);
		 }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSErrorTools  method: generateCustomerOrderObject]");
		}
	}

	/**
	 * Update oms error item.
	 * @param pOrderId - order id
	 * @param pServiceName  - Service name
	 */
	public void updateOmsErrorItemForOrder(String pOrderId, String pServiceName) {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUOMSErrorTools method : updateOmsErrorItemForOrder");
		}
		MutableRepositoryItem omsErrorItem = null;
		MutableRepository omsRepository = getOmsErrorRepository();
		if (omsRepository != null) {
			try {
					omsErrorItem = omsRepository.createItem(getPropertyManager().getOmsErrorMessageItemName());
					omsErrorItem.setPropertyValue(getPropertyManager().getTypePropertyName(), pServiceName);
					omsErrorItem.setPropertyValue(getPropertyManager().getMessagePropertyName(), pOrderId);
					Calendar cal = Calendar.getInstance();
					omsErrorItem.setPropertyValue(getPropertyManager().getTransactionTimePropertyName(), cal.getTime());
					omsRepository.addItem(omsErrorItem);
			} catch (RepositoryException re) {
				if(isLoggingError()){
					vlogError(re, "RepositoryException in method updateOmsErrorItemForOrder");
				}
			} 
		}
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUOMSErrorTools method : updateOmsErrorItemForOrder");
		}
	}
	
	/**
	 * Removes the item from oms error repository.
	 *
	 * @param pRepositoryId the repository id
	 */
	public void removeItemFromOMSErrorRepo(String pRepositoryId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSErrorTools  method: removeItemFromOMSErrorRepo]");
			vlogDebug("pReositoryId : {0}",pRepositoryId);
		}
		if(StringUtils.isBlank(pRepositoryId)){
			return;
		}
		TransactionManager transactionManager  =  getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean isRoolBack = Boolean.FALSE;
		try {
			td.begin(transactionManager, TransactionDemarcation.REQUIRED);
			getOmsErrorRepository().removeItem(pRepositoryId, getPropertyManager().getOmsErrorMessageItemName());
			getOmsErrorRepository().invalidateCaches();
		} catch (RepositoryException exc) {
			isRoolBack = Boolean.TRUE;
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.removeItemFromOMSErrorRepo", exc);
			}
		} catch (TransactionDemarcationException exc) {
			isRoolBack = Boolean.TRUE;
			if (isLoggingError()) {
				logError("TransactionDemarcationException in TRUOMSErrorTools.removeItemFromOMSErrorRepo", exc);
			}
		}finally{
			try {
				td.end(isRoolBack);
			} catch (TransactionDemarcationException exc) {
				if (isLoggingError()) {
					logError("TransactionDemarcationException in TRUOMSErrorTools.removeItemFromOMSErrorRepo", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUOMSErrorTools  method: removeItemFromOMSErrorRepo]");
		}
	}

	/**
	 * Gets the transaction manager.
	 *
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * Sets the transaction manager.
	 *
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}

	/**
	 * Gets the profile tools.
	 *
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the oms error repository.
	 *
	 * @return the omsErrorRepository
	 */
	public GSARepository getOmsErrorRepository() {
		return mOmsErrorRepository;
	}

	/**
	 * Sets the oms error repository.
	 *
	 * @param pOmsErrorRepository the omsErrorRepository to set
	 */
	public void setOmsErrorRepository(GSARepository pOmsErrorRepository) {
		mOmsErrorRepository = pOmsErrorRepository;
	}

	/**
	 * Gets the oms configuration.
	 *
	 * @return the omsConfiguration
	 */
	public TRUOMSConfiguration getOmsConfiguration() {
		return mOmsConfiguration;
	}
	
	/**
	 * Gets the oms error properties.
	 *
	 * @return the omsErrorProperties
	 */
	public TRUOMSErrorProperties getOmsErrorProperties() {
		return mOmsErrorProperties;
	}

	/**
	 * Sets the oms error properties.
	 *
	 * @param pOmsErrorProperties the omsErrorProperties to set
	 */
	public void setOmsErrorProperties(TRUOMSErrorProperties pOmsErrorProperties) {
		mOmsErrorProperties = pOmsErrorProperties;
	}

	/**
	 * Sets the oms configuration.
	 *
	 * @param pOmsConfiguration the omsConfiguration to set
	 */
	public void setOmsConfiguration(TRUOMSConfiguration pOmsConfiguration) {
		mOmsConfiguration = pOmsConfiguration;
	}
	
	/**
	 * This method returns the integrationManager value.
	 *
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}
	
	/**
	 * This method sets the integrationManager with parameter value pIntegrationManager.
	 *
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

}
