package com.tru.integration.oms.constant;

/**
 * This class holds all the constants.
 * @author Professional Access
 * @version 1.0
 */
public class TRUOMSConstant {
	/** constant for ":". */
	public static final String POPULATE_LAYAWAY_ORDER_OBJECT="populateLayawayOrderObject";
	public static final String POPULATE_CUSTOMER_ORDER_OBJECT="populateCustomerOrderObject";
	public static final String HTTP_ERROR_CODE = "Failed : HTTP error code : ";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_VALUE = "application/json";
	public static final String GET_METHOD = "GET";
	public static final String POST_METHOD = "POST";
	public static final String AUTHORIZATION = "Authorization";
	public static final String RESPONSE_STATUS = "responseStatus";
	public static final String MESSAGES = "messages";
	public static final String MESSAGE = "message";
	public static final String TOYS_R_US = "ToysRUs";
	public static final String ORDER_STATUS = "orderStatus";
	public static final String PHONE_REJEX_PATTERN = "(\\d{3})(\\d{3})(\\d+)";
	public static final String PHONE_NUMBER_GRUPING_INDEXING = "$1-$2-$3";
	public static final int RESPONSE_CODE_200 =200;
	public static final String CHANNEL_FROM ="From ";
	public static final String CHANNEL_AND =" & ";
	public static final String CHANNEL_HASH =" #";
	public static final String DUMMY_VALUE ="123";
	public static final String EMPTY_STRING ="";
	public static final String CUSTOMER_ORDER ="customerOrder";
	public static final String REFERENCE_FIELDS ="referenceFields";
	public static final String REFERENCE_NUMBER_1 ="referenceNumber1";
	public static final String REFERENCE_NUMBER_2 ="referenceNumber2";
	public static final String ORDER_LINES ="orderLines";
	public static final String CUSTOMER_ATTRIBUTE_LIST ="customAttributeList";
	public static final String IS_GIFT ="isGift";
	public static final String PRICE_DETAILS ="priceDetails";
	public static final String IS_PRICE_OVERRIDDEN ="isPriceOverridden";
	public static final String EXTENDED_PRICE ="extendedPrice";
	public static final char CHAR_PIPE_DELIMETER ='|';
	public static final String OPEN_BRACES ="(";
	public static final String CLOSED_BRACES =")";
	/** constant for : SUCCESS */
	public static final String SUCCESS ="SUCCESS";
	/** constant for : ERROR */
	public static final String ERROR = "ERROR";
	/**
	 * Holds CUSTOMER_MASTER_IMPORT.
	 */
	public static final String CUSTOMER_MASTER_IMPORT = "CustomerMasterImport";
	/**
	 * Holds constant ORDER_UPDATE_SERVICE.
	 */
	public static final String ORDER_UPDATE_SERVICE = "OrderUpdateService";
	/**
	 * Holds constant CUTOMER_ORDER_IMPORT_SERVICE.
	 */
	public static final String CUTOMER_ORDER_IMPORT_SERVICE = "CustomerOrderImport";
	/**
	 * Holds constant CUSTOMER_ORDER_DETAIL_SERVICE_CALL.
	 */
	public static final String CUSTOMER_ORDER_DETAIL_SERVICE_CALL="CustomerOrderDetailServiceCall";
	/**
	 * Holds constant CUSTOMER_ORDER_LIST_SERVICE_CALL.
	 */
	public static final String CUSTOMER_ORDER_LIST_SERVICE_CALL="CustomerOrderListServiceCall";
	/**
	 * Holds constant CUSTOMER_ORDER_IMPORT_SERVICE_CALL.
	 */
	public static final String CUSTOMER_ORDER_IMPORT_SERVICE_CALL="CustomerOrderImportServiceCall";
	/**
	 * Holds constant CUSTOMER_ORDER_CANCEL_SERVICE_CALL.
	 */
	public static final String CUSTOMER_ORDER_CANCEL_SERVICE_CALL="CustomerOrderCancelServiceCall";
	/**
	 * Holds constant CUSTOMER_MASTER_IMPORT_SERVICE_CALL.
	 */
	public static final String CUSTOMER_MASTER_IMPORT_SERVICE_CALL = "CustomerMasterImportServiceCall";
	/**
	 * Holds constant ORDER.
	 */
	public static final String ORDER = "Order";
	/**
	 * Holds constant INT_TWO.
	 */
	public static final int INT_TWO = 2;
	/**
	 * Holds constant INT_ONE.
	 */
	public static final int INT_ONE = 1;
	/**
	 * Holds constant ORDER_LINE.		
	 */
	public static final String ORDER_LINE = "orderLine";
	/**
	 * Holds constant PERCENTILE_FIVE_D.	
	 */
	public static final String PERCENTILE_FIVE_D = "%05d";
	/**
	 * Holds constant INT_SEVEN.		
	 */
	public static final int INT_SEVEN = 7;
	/**
	 * Holds constant XML_PACKAGE.		
	 */
	public static final String XML_PACKAGE = "com.tru.xml.customerorder";
	/**
	 * Holds constant JAXB_PACKAGE.		
	 */
	public static final String JAXB_PACKAGE = "jaxb.formatted.output";
	/**
	 * Holds constant INITIAL_PAYMENT.		
	 */
	public static final String INITIAL_PAYMENT = "initial payment";
	/**
	 * Holds constant NUMERIC_ONE.		
	 */
	public static final Object NUMERIC_ONE = "1";
	/**
	 * Holds constant INT_ZERO.		
	 */
	public static final int INT_ZERO = 0;
	/**
	 * Holds constant GIFT_CARD.		
	 */
	public static final String GIFT_CARD = "Gift Card";
	/**
	 * The Constant to hold CREDIT_CARD
	 */
	public static final String CREDIT_CARD = "Credit Card";
	/**
	 * The Constant to hold ACCOUNT_PREFIX
	 */
	public static final String ACCOUNT_PREFIX = "XXXXXXXXXXXX";
	/**
	 * Holds constant DATE_FORMAT.		
	 */
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	/**
	 * Holds constant EXPECTED_DATE_FORMAT.		
	 */
	public static final String EXPECTED_DATE_FORMAT = "MMddyyyy";
	/**
	 * Holds constant COLON.		
	 */
	public static final String COLON = ":";
	/** 
	 * The property to hold PARSE_EXCEPTION. 
	 **/
	public static final String PARSE_EXCEPTION = "parsing exception :";
	/**
	 * Holds constant COLON.		
	 */
	public static final String LAYAWAY_NO_ORDER_MESSAGE = "layawayNoOrderMessage";
	/**
	 * Holds constant COLON.		
	 */
	public static final String NO_ORDER_MESSAGE = "noOrderMessage";
	/**
	 * Holds constant COLON.		
	 */
	public static final String NO_ORDER_FOUND_MESSAGE = "noOrderFoundMessage";
	/**
	 * Holds constant COLON.		
	 */
	public static final String LAYAWAY_NO_ORDER_FOUND_MESSAGE = "layawayNoOrderFoundMessage";
	/**
	 * Holds constant COLON.		
	 */
	public static final String ZIP_CODE_NOT_MATCH_MESSAGE = "zipCodeNotMatchMessage";
	/**
	 * Holds constant COLON.		
	 */
	public static final String GIFTCARD_PAYINSTORE_ERROR_MESSAGE = "giftCardpayInStoreErrorMessgae";
	/** Holds constant SPACE_STRING. */
	public static final String SPACE_STRING = " ";
	/** Holds constant HIFEN. */
	public static final String HIFEN = "\\-";
	/** Holds constant CUSTOMER_ORDER_IMPORT. */
	public static final String CUSTOMER_ORDER_IMPORT = "COI";
	/** Holds constant CUSTOMER_ORDER_UPDATE. */
	public static final String CUSTOMER_ORDER_UPDATE = "COU";
	/** Holds constant CUSTOMER_MASTER_IMPORT_CMI. */
	public static final String CUSTOMER_MASTER_IMPORT_CMI = "CMI";
	/** Holds constant APHOSTAPHESE. */
	public static final String APHOSTAPHESE = "\"";
	/**
	 * Holds constant EIGHT.		
	 */
	public static final int INT_EIGHT = 8;
	/**
	 * Holds constant INT_ZERO.		
	 */
	public static final int INT_FIVE = 5;
	/**
	 * Holds constant INT_ZERO.		
	 */
	public static final int INT_NINE = 9;
	/**
	 * Holds constant INT_ZERO.		
	 */
	public static final String HYPEN = "-";
	/**
	 * Holds constant DOLLAR.		
	 */
	public static final String DOLLAR = "$";
	
}
