package com.tru.integration.oms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oms.custorder.AllocationInfo;
import com.oms.custorder.BillToDetail;
import com.oms.custorder.ChargeDetail;
import com.oms.custorder.ChargeDetails;
import com.oms.custorder.CustomAttributeList;
import com.oms.custorder.CustomerInfo;
import com.oms.custorder.CustomerOrder;
import com.oms.custorder.CustomerOrderImport;
import com.oms.custorder.DiscountDetail;
import com.oms.custorder.DiscountDetails;
import com.oms.custorder.Note;
import com.oms.custorder.Notes;
import com.oms.custorder.OrderLine;
import com.oms.custorder.OrderLines;
import com.oms.custorder.PaymentDetail;
import com.oms.custorder.PaymentDetails;
import com.oms.custorder.PriceDetails;
import com.oms.custorder.ReferenceFields;
import com.oms.custorder.ReferenceFields_;
import com.oms.custorder.ShippingAddress;
import com.oms.custorder.ShippingInfo;
import com.oms.custorder.TaxDetail;
import com.oms.custorder.TaxDetails;
import com.oms.custprofile.CustomerFullDetails;
import com.oms.custprofile.CustomerMasterImport;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUShippingPriceInfo;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.messaging.oms.TRUCustomerMasterImportMessage;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This is utility class for OMS operations.
 * @author Professional Access
 * @version 1.0
 */
public class TRUOMSUtils extends TRUBaseOMSUtils {
	
	/**
	 * String BPP.
	 */
	public static final String BPP = "BPP";
	/**
	 * String EW.
	 */
	public static final String EW = "EW";
	
	/**
	 * This method will populate the Order details into CustomerOrder Object to.
	 * generate the JSON message for import service.
	 * 
	 * @param pOrder
	 *            - order
	 * @return String : JSON Request
	 */
	public String populateCustomerOrderObject(TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateCustomerOrderObject]");
			vlogDebug("pOrder : {0}", pOrder);
		}
		String customerOrderJSON = null;
		if(pOrder != null){
			// Added Performance statement 
			if(PerformanceMonitor.isEnabled()){
				PerformanceMonitor.startOperation(TRUOMSConstant.POPULATE_CUSTOMER_ORDER_OBJECT);
			}
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			//Creating an Object of CustomerOrderInport
			CustomerOrderImport customerOrderImport = new CustomerOrderImport();
			CustomerOrder customerOrder = new CustomerOrder();
			setTaxCounter(TRUConstants.INTEGER_NUMBER_ONE);
			// Calling method to populate the customer 1st name and last name.
			setCustomerFirstLastName(pOrder);
			// Customer Order Header
			populateCustomerOrderHeader(customerOrder,pOrder);
			// Populate payment Details
			populatePaymentDetails(customerOrder, pOrder);
			// Populate Customer Info
			populateCustomerInfo(customerOrder, pOrder);
			// Populate Reference Fields.
			populateReferenceFields(customerOrder,pOrder);
			// Populate Item details
			populateOrderLineDetails(customerOrder, pOrder);
			//Adding customer order to the parent node
			customerOrderImport.setCustomerOrder(customerOrder);
			// Setting back flag to false.
			setPaypalPG(Boolean.FALSE);
			setCreditCardPG(Boolean.FALSE);
			//Getting json object
			Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		    customerOrderJSON = gson1.toJson(customerOrderImport); // done
		    // Removing the Reference number START
			JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(customerOrderJSON);
			JsonObject orderDetailsJsonObject = parse.getAsJsonObject();
			JsonObject customerOrderJsonObj = orderDetailsJsonObject.get(TRUOMSConstant.CUSTOMER_ORDER).getAsJsonObject();
			customerOrderJsonObj.remove(omsConfiguration.getOrderTotalJsonElement());
			JsonObject referenceFieldsJsonObject = customerOrderJsonObj.get(TRUOMSConstant.REFERENCE_FIELDS).getAsJsonObject();
			referenceFieldsJsonObject.remove(TRUOMSConstant.REFERENCE_NUMBER_1);
			referenceFieldsJsonObject.remove(TRUOMSConstant.REFERENCE_NUMBER_2);
			// Removing settlementAmount parameter for All Payment Group except Gift Card : Start
			if(customerOrderJsonObj.get(omsConfiguration.getOrderDetailsPaymentDetailsJsonElement()) != null){
				JsonObject paymentDetailsJsonObject = customerOrderJsonObj.get(omsConfiguration.getOrderDetailsPaymentDetailsJsonElement()).getAsJsonObject();
				if(paymentDetailsJsonObject != null && 
						paymentDetailsJsonObject.get(omsConfiguration.getOrderDetailsPaymentDetailJsonElement()) != null){
					JsonArray paymentDetailsJsonArray = paymentDetailsJsonObject.get(omsConfiguration.getOrderDetailsPaymentDetailJsonElement()).getAsJsonArray();
					for (JsonElement jsonElement : paymentDetailsJsonArray) {
						JsonObject PaymentDetailObject = jsonElement.getAsJsonObject();
						String paymentMethod = PaymentDetailObject.get(omsConfiguration.getPaymentMethodJSONElement()).getAsString();
						if(StringUtils.isNotBlank(paymentMethod) && !paymentMethod.equals(omsConfiguration.getPaymentMethodGC())){
							PaymentDetailObject.remove(omsConfiguration.getSettlementAmountJSONElement());
						}
					}
				}
			}
			// Removing settlementAmount parameter for All Payment Group except Gift Card : END
			// Removing Customer Attribute parameter for non ISPU item : Start
			JsonObject orderLinesJsonObject = customerOrderJsonObj.get(TRUOMSConstant.ORDER_LINES).getAsJsonObject();
			JsonArray orderLinesJsonArray = orderLinesJsonObject.get(TRUOMSConstant.ORDER_LINE).getAsJsonArray();
			double lineTotal = TRUConstants.DOUBLE_ZERO;
			for (JsonElement jsonElement : orderLinesJsonArray) {
				JsonObject orderLineObject = jsonElement.getAsJsonObject();
				JsonArray customAttributeListJsonArray = orderLineObject.get(TRUOMSConstant.CUSTOMER_ATTRIBUTE_LIST).getAsJsonArray();
				if(customAttributeListJsonArray.size() <= TRUConstants.ZERO){
					orderLineObject.remove(TRUOMSConstant.CUSTOMER_ATTRIBUTE_LIST);
				}
				lineTotal = orderLineObject.get(omsConfiguration.getLineTotalJSONElement()).getAsDouble();
				if(lineTotal <= TRUConstants.DOUBLE_ZERO){
					orderLineObject.remove(omsConfiguration.getLineTotalJSONElement());
				}
			}
			// Removing the Reference number and Customer Attributes if not available END
			customerOrderJSON = gson1.toJson(orderDetailsJsonObject);
		    // Setting back 1st name and last name as Null.
		    setFirstName(null);
		    setLastName(null);
		    setPhoneNumber(null);
		    if(isLoggingDebug()){
		    	vlogDebug("populateCustomerOrderObject JSON COI Service: {0}", customerOrderJSON);
		    }
		    // Added Performance statement 
		    if(PerformanceMonitor.isEnabled()){
		    	PerformanceMonitor.endOperation(TRUOMSConstant.POPULATE_CUSTOMER_ORDER_OBJECT);
		    }
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCustomerOrderObject]");
		}
		return customerOrderJSON;
	}

	/**
	 * This method is to populate the reference fields for Order.
	 * 
	 * @param pCustomerOrder - CustomerOrder
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateReferenceFields(CustomerOrder pCustomerOrder,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateReferenceFields]");
			vlogDebug("pCustomerOrder : {0} pOrder : {1}" ,pCustomerOrder,  pOrder);
		}
		if(pOrder == null || pCustomerOrder == null){
			return;
		}
		ReferenceFields refField = new ReferenceFields();
		String siteId = pOrder.getSiteId();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(!StringUtils.isBlank(siteId)){
			if(siteId.contains(omsConfiguration.getTruSiteId())){
				refField.setReferenceField4(omsConfiguration.getOrderRefField4ForTRU());
			}else if(siteId.contains(omsConfiguration.getBruSiteId())){
				refField.setReferenceField4(omsConfiguration.getOrderRefField4ForBRU());
			}
		}
		if(pOrder.isFinanceAgreed()){
			refField.setReferenceField7(Integer.toString(pOrder.getFinancingTermsAvailed()));
		}
		String rewardNumber = pOrder.getRewardNumber();
		if(!StringUtils.isBlank(rewardNumber)){
			refField.setReferenceField10(rewardNumber);
		}
		pCustomerOrder.setReferenceFields(refField);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateReferenceFields]");
		}
	}

	/**
	 * This method is to populate the Order Header details.
	 * 
	 * @param pCustomerOrder - CustomerOrder
	 * @param pOrder - TRUOrderImpl
	 */
	private void populateCustomerOrderHeader(CustomerOrder pCustomerOrder,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateCustomerOrderHeader]");
			vlogDebug("pCustomerOrder : {0} pOrder : {1}" ,pCustomerOrder,  pOrder);
		}
		if(pOrder == null || pCustomerOrder == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		DateFormat df = new SimpleDateFormat(DATE_FORMAT,Locale.US);
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		pCustomerOrder.setTcCompanyId(omsConfiguration.getTcCompanyIdValue());
		pCustomerOrder.setOrderNumber(pOrder.getId());
		boolean onHoldStatus = Boolean.FALSE;
		if(pOrder.getSubmittedDate() != null){
			pCustomerOrder.setOrderCapturedDate(df.format(pOrder.getSubmittedDate()));
		}
		if(StringUtils.isNotBlank(pOrder.getSourceOfOrder())){
			pCustomerOrder.setEntryType(pOrder.getSourceOfOrder());
		}else{
			pCustomerOrder.setEntryType(omsConfiguration.getOrderEntryTypeWeb());
		}
		pCustomerOrder.setOrderConfirmed(Boolean.TRUE);
		pCustomerOrder.setMinimizeShipments(Boolean.TRUE);
		pCustomerOrder.setOrderCancelled(Boolean.FALSE);
		if(isPayInStoreOrder(pOrder)){
			pCustomerOrder.setOnHold(Boolean.TRUE);
			pCustomerOrder.setPaymentStatus(omsConfiguration.getJsonOrderStatusForPIS());
			pCustomerOrder.setOrderType(omsConfiguration.getOrderTypeMap().get(ORDER_TYPE_PAYINSTORE_KEY));
		}else{
			onHoldStatus = getFraudStatus(pOrder);
			pCustomerOrder.setOnHold(onHoldStatus);
			pCustomerOrder.setPaymentStatus(omsConfiguration.getOrderPaymentStatus());
			pCustomerOrder.setOrderType(omsConfiguration.getOrderTypeMap().get(ORDER_TYPE_WEB_KEY));
		}
		if(isTaxWareDown(pOrder)){
			pCustomerOrder.setPaymentStatus(omsConfiguration.getTaxwareDownStatus());
		}
		pCustomerOrder.setOrderSubtotal(pricingTools.round(getOrderSubTotal(pOrder)));
		if(StringUtils.isNotBlank(pOrder.getEnteredBy())){
			pCustomerOrder.setEnteredBy(pOrder.getEnteredBy());
		}
		if(StringUtils.isNotBlank(pOrder.getEnteredLocation())){
			pCustomerOrder.setEnteredLocation(pOrder.getEnteredLocation());
		}
		// Removed order total for #CR86 
		//pCustomerOrder.setOrderTotal(pricingTools.round(pOrder.getPriceInfo().getTotal()));
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCustomerOrderHeader]");
		}
	}

	/**
	 * This method will populates the commerce item details to customer order object.
	 * 
	 * @param pCustomerOrder - Customer Order Object
	 * @param pOrder - Order
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private void populateOrderLineDetails(CustomerOrder pCustomerOrder,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineDetails]");
		}
		if(pOrder == null || pCustomerOrder == null){
			return;
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(isLoggingDebug()){
			vlogDebug("shippingGroups: {0} avaibale in Order : {1}", shippingGroups,pOrder);
		}
		if(shippingGroups == null || shippingGroups.isEmpty()){
			return;
		}
		OrderLines orderLines = new OrderLines();
		List<OrderLine> listOfOrderlines = new ArrayList<OrderLine>();
		OrderLine orderLine = null;
		int orderLineCount = TRUConstants.INTEGER_NUMBER_ONE;
		CommerceItem commItem = null;
		ShippingInfo shippingInfo = null;
		setCommerceItemQtyForMulItemDis(pOrder);// Calling method to set the Commerce Item and qty in Variables.
		setCommItemAndCommQtyMap(pOrder);// Calling method to set the Commerce Item and qty in Variables.
		setLastShipGroup(Boolean.FALSE);
		setOrderDisAndAmountMap(null);
		for(ShippingGroup lShipGroup : shippingGroups){
			List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = lShipGroup.getCommerceItemRelationships();
			if(commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
				continue;
			}
			if(shippingGroups.indexOf(lShipGroup)+TRUConstants.INTEGER_NUMBER_ONE == shippingGroups.size()){
				setLastShipGroup(Boolean.TRUE);
			}
			List<TRUShippingGroupCommerceItemRelationship> newListOfRel = 
					removeGiftWrapRelationships(commerceItemRelationships);//Calling method to remove the GiftWrap relationship from the All relationship
			if(newListOfRel == null || newListOfRel.isEmpty()){
				continue;
			}
			setCommItemRelAmount(getCommerceItemRelAmount(lShipGroup));//Setting the amount of commerce item by amountByAverage
			setRelAndRelQtyForProration(lShipGroup,pOrder);//Setting the relationship and relationship quantity in map
			setLastRelationShip(Boolean.FALSE);
			for(TRUShippingGroupCommerceItemRelationship lCommItemRel : newListOfRel){
				CommerceItem commerceItem = lCommItemRel.getCommerceItem();
				if(isLoggingDebug()){
					vlogDebug("commerceItem: {0} lCommItemRel : {1}", commerceItem,lCommItemRel);
				}
				if(commerceItem instanceof TRUDonationCommerceItem){
					populateDonationItemDetails(listOfOrderlines,lCommItemRel,pOrder,orderLineCount);
					orderLineCount++;
					continue;
				}
				long giftWrapQTY = TRUConstants.LONG_ZERO;
				long relationShipQTY = lCommItemRel.getQuantity();
				List<RepositoryItem> giftItemInfos = lCommItemRel.getGiftItemInfo();
				if(isLoggingDebug()){
					vlogDebug("giftItemInfos: {0}", giftItemInfos);
				}
				if(newListOfRel.indexOf(lCommItemRel)+TRUConstants.INTEGER_NUMBER_ONE == newListOfRel.size()){
					setLastRelationShip(Boolean.TRUE);
				}
				boolean isGiftWrapRel = Boolean.FALSE;
				if(lCommItemRel.getIsGiftItem() && giftItemInfos != null && !giftItemInfos.isEmpty()){
					isGiftWrapRel = Boolean.TRUE;
					for(RepositoryItem giftItemInfo : giftItemInfos){
						long qty = (Long)giftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapQuantity());
						giftWrapQTY+=qty;
						if(isLoggingDebug()){
							vlogDebug("isGiftWrapRel: {0} qty : {1}", isGiftWrapRel,qty);
						}
						populateGiftWrapLineItem(listOfOrderlines,lCommItemRel,pOrder,qty,isGiftWrapRel,giftItemInfo,orderLineCount);
						orderLineCount++;
					}
					if(giftWrapQTY > TRUConstants.ZERO && giftWrapQTY < relationShipQTY){
						populateCommerceLineItem(listOfOrderlines, lCommItemRel, 
								pOrder,relationShipQTY-giftWrapQTY,isGiftWrapRel,orderLineCount);
						orderLineCount++;
					}
				}else{
					isGiftWrapRel = Boolean.FALSE;
					if(isLoggingDebug()){
						vlogDebug("isGiftWrapRel: {0} qty : {1}", isGiftWrapRel,lCommItemRel.getQuantity());
					}
					populateCommerceLineItem(listOfOrderlines, lCommItemRel, pOrder,lCommItemRel.getQuantity(),isGiftWrapRel,orderLineCount);
					orderLineCount++;
				}
			}
			setCommItemRelAmount(TRUConstants.DOUBLE_ZERO);// Setting null for all the map which are for pro-ration start
			setCommerceItemRelMapForMulOrderDis(null);
			setCommerceItemRelMapForMulShipDis(null);
			setCommerceItemRelMapForOrderDis(null);
			setCommerceItemRelMapForShipDis(null);
			setCommerceItemRelMapForSurcharge(null);
			setCommerceItemRelMapForEWasteFee(null);
			setCommerceItemRelMapForShipFee(null);
			setCommerceItemRelMapForBPP(null);
			setGWItemIdAndRelID(null);
		}
		setCommItemQtyMap(null);
		setCommerceItemQTYMapForMulItemDis(null);
		List<OrderLine> newListOfOrderLine = removeExtraPropertiesfromOrderLine(listOfOrderlines);// calling the method to remove the parameter which are used for pro-ration and other logic
		orderLines.setOrderLine(newListOfOrderLine);
		pCustomerOrder.setOrderLines(orderLines);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineDetails]");
		}
	}

	/**
	 * Method to remove the extra parameters from OrderLine which are used in to generate the CO Import 
	 * JSON Request.
	 * 
	 * @param pListOfOrderlines : List of OrderLine
	 * @return list - List of Order Line Object after removing unused parameters.
	 */
	private List<OrderLine> removeExtraPropertiesfromOrderLine(
			List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: removeExtraPropertiesfromOrderLine]");
		}
		if(pListOfOrderlines == null || pListOfOrderlines.isEmpty()){
			return pListOfOrderlines;
		}
		List<OrderLine> newListOfOrderLine = new ArrayList<OrderLine>();
		for(OrderLine lOrderLine : pListOfOrderlines){
			lOrderLine.setShippingGroupId(null);
			lOrderLine.setRelationshipId(null);
			lOrderLine.setCommerceItemId(null);
			if(lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null	&& 
					!lOrderLine.getChargeDetails().getChargeDetail().isEmpty()){
				for(ChargeDetail chargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
					chargeDetail.setItemType(null);
				}
			}
			if(lOrderLine.getTaxDetails() != null && lOrderLine.getTaxDetails().getTaxDetail() != null	&& 
					!lOrderLine.getTaxDetails().getTaxDetail().isEmpty()){
				for(TaxDetail taxDetail : lOrderLine.getTaxDetails().getTaxDetail()){
					taxDetail.setItemType(null);
				}
			}
			newListOfOrderLine.add(lOrderLine);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: removeExtraPropertiesfromOrderLine]");
		}
		return newListOfOrderLine;
	}


	/**
	 * Populate order line reference fiels.
	 *
	 * @param pShipItemRel the ship item rel
	 * @param pOrderLine the order line
	 * @param pIsGiftWrap the is gift wrap
	 * @param pGiftItemInfo - Repository Item
	 * @param pOrder - Order Object
	 */
	private void populateOrderLineReferenceFiels(TRUShippingGroupCommerceItemRelationship pShipItemRel,OrderLine pOrderLine,Boolean pIsGiftWrap, 
			RepositoryItem pGiftItemInfo, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineReferenceFiels]");
			vlogDebug("pShipItemRel: {0} pOrderLine : {1} ",pShipItemRel,pOrderLine);
		}
		if(pShipItemRel == null || pOrderLine == null){
			return;
		}
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) ((CustomCatalogTools)getOrderManager().getCatalogTools()).getCatalogProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		CommerceItem cItem = pShipItemRel.getCommerceItem();
		String siteId = cItem.getAuxiliaryData().getSiteId();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem skuItem = (RepositoryItem) cItem.getAuxiliaryData().getCatalogRef();
		if(isLoggingDebug()){
			vlogDebug("cItem: {0} skuItem : {1} siteId : {2}", cItem,skuItem,siteId);
		}
		ReferenceFields_ refFiled = new ReferenceFields_();
		boolean slapper = Boolean.FALSE;
		if(skuItem != null && !((String) skuItem.getPropertyValue(catalogProperties.getSkuType())).equals
				(catalogProperties.getNonMerchSKUType()) && 
				skuItem.getPropertyValue(catalogProperties.getSlapperItemPropertyName()) != null){
			slapper = (Boolean) skuItem.getPropertyValue(catalogProperties.getSlapperItemPropertyName());
		}
		if(slapper){
			refFiled.setReferenceField2(omsConfiguration.getSlapper());
		}else{
			refFiled.setReferenceField2(omsConfiguration.getNonSlapper());
		}
		// Calling method to get the subType of Gift Wrap SKU.
		String skuSubType = getNonMerchSKUSubType(pGiftItemInfo,pOrder);
		if(StringUtils.isNotBlank(skuSubType)){
			if(omsConfiguration.getTruGuftWrapType().equals(skuSubType)){
				refFiled.setReferenceField3(omsConfiguration.getTruSiteParameter());
			}
			if(omsConfiguration.getBruGiftWrapType().equals(skuSubType)){
				refFiled.setReferenceField3(omsConfiguration.getBruSiteParameter());
			}
		}
		if(pIsGiftWrap){
			refFiled.setReferenceNumber1(TRUConstants.LONG_ONE);
		}else{
			refFiled.setReferenceNumber1(TRUConstants.LONG_ZERO);
		}
		refFiled.setReferenceField9(TRUCommerceConstants.YES);
		ShippingGroup shipGroup = pShipItemRel.getShippingGroup();
		if(shipGroup instanceof TRUChannelHardgoodShippingGroup){
			String channelType = ((TRUChannelHardgoodShippingGroup)shipGroup).getChannelType();
			if(isLoggingDebug()){
				vlogDebug("channelType: {0} shipGroup : {1}", channelType,shipGroup);
			}
			refFiled.setReferenceField4(((TRUChannelHardgoodShippingGroup)shipGroup).getChannelId());
			if(!StringUtils.isBlank(channelType)){
				if(channelType.equalsIgnoreCase(commercePropertyManager.getWishlist())){
					refFiled.setReferenceField5(omsConfiguration.getWishListRefField5());
				}else if(channelType.equalsIgnoreCase(commercePropertyManager.getRegistry())){
					refFiled.setReferenceField5(omsConfiguration.getRegistryRefField5());
				}
			}
		}
		if(shipGroup instanceof TRUChannelInStorePickupShippingGroup){
			String channelType = ((TRUChannelInStorePickupShippingGroup)shipGroup).getChannelType();
			if(isLoggingDebug()){
				vlogDebug("channelType: {0} shipGroup : {1}", channelType,shipGroup);
			}
			refFiled.setReferenceField4(((TRUChannelInStorePickupShippingGroup)shipGroup).getChannelId());
			if(!StringUtils.isBlank(channelType)){
				if(channelType.equalsIgnoreCase(commercePropertyManager.getWishlist())){
					refFiled.setReferenceField5(omsConfiguration.getWishListRefField5());
				}else if(channelType.equalsIgnoreCase(commercePropertyManager.getRegistry())){
					refFiled.setReferenceField5(omsConfiguration.getRegistryRefField5());
				}
			}
		}
		if(shipGroup instanceof TRUHardgoodShippingGroup && !isPayInStoreOrder(pOrder)){
			refFiled.setReferenceNumber3(getEstimateShippingDate(pShipItemRel.getEstimateShipWindowMin()));
			refFiled.setReferenceNumber4(getEstimateShippingDate(pShipItemRel.getEstimateShipWindowMax()));
		}
		pOrderLine.setReferenceFields(refFiled);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineReferenceFiels]");
		}
	}

	/**
	 * Populate order line charge details.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine : OrderLine Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is Relationship contain gift wrap item
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pIsGiftLineItem : boolean - Is Item is type of GiftWrap
	 * @param pGiftItemInfo : RepositoryItem - GiftItemInfo RepositoryItem
	 * @param pOrder : TRUOrderImpl Object
	 */
	private void populateOrderLineChargeDetails(
			ShippingGroup pLShipGroup, TRUShippingGroupCommerceItemRelationship pShipItemRel,
			OrderLine pOrderLine, long pGiftWrapRelQty, boolean pIsGiftWrapRel, 
			List<OrderLine> pListOfOrderlines,Boolean pIsGiftLineItem, RepositoryItem pGiftItemInfo, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineChargeDetails]");
			vlogDebug("pLShipGroup: {0} pGiftWrapRelQty : {1}", pLShipGroup,pGiftWrapRelQty);
			vlogDebug("pIsGiftWrapRel: {0} pListOfOrderlines : {1} pIsGiftLineItem : {2}", pIsGiftWrapRel,pListOfOrderlines,pIsGiftLineItem);
			vlogDebug("pGiftItemInfo: {0} pOrder : {1}", pGiftItemInfo,pOrder);
		}
		if(pShipItemRel == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		ChargeDetails chargeDetails = new ChargeDetails();
		List<ChargeDetail> listOfChargeDetails = new ArrayList<ChargeDetail>();
		ShippingGroup shipGroup = pShipItemRel.getShippingGroup();
		TRUShippingPriceInfo shipPriceInfo = (TRUShippingPriceInfo) pLShipGroup.getPriceInfo();
		if (isLoggingDebug()) {
			vlogDebug("Shipping surcharge: {0} Shipping Group {1}", pShipItemRel.getSurcharge(),shipGroup);
			vlogDebug("E-Waste Fee: {0}", pShipItemRel.getE911Fees());
		}
		// Calling method to Populate the charge details for Surcharge
		if(pShipItemRel.getSurcharge() > TRUConstants.DOUBLE_ZERO){
			populateOrderLineSurchargeDetail(listOfChargeDetails,
					pIsGiftWrapRel,pShipItemRel,pGiftWrapRelQty,pListOfOrderlines);
		}
		// Calling method to Populate the charge details for BPP item
		RepositoryItem bppItem = (RepositoryItem) pShipItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem != null){
			populateOrderLineBPPDetails(pShipItemRel,listOfChargeDetails,pIsGiftWrapRel,
					pGiftWrapRelQty,pListOfOrderlines);
		}
		// Calling method to Populate the charge details for Gift item
		if(pIsGiftLineItem && pGiftItemInfo != null){
			populateOrderLineGiftItemDetails(listOfChargeDetails,pGiftItemInfo,pOrder,pGiftWrapRelQty);
		}
		// Calling method to Populate the charge details for Shipping Price
		if(shipGroup != null && (!(shipGroup instanceof TRUInStorePickupShippingGroup)) && 
				shipPriceInfo != null && shipPriceInfo.getRawShipping() > TRUConstants.DOUBLE_ZERO){
			populateOrderLineShippingDetails(pShipItemRel,pListOfOrderlines,pIsGiftWrapRel,pGiftWrapRelQty,listOfChargeDetails);
		}
		// Calling method to Populate the charge details for EWaste Fee
		if(pShipItemRel.getE911Fees() != TRUConstants.DOUBLE_ZERO){
			populateOrderLineEW911FeeDetails(pShipItemRel,pGiftWrapRelQty,pListOfOrderlines,pIsGiftWrapRel,listOfChargeDetails);
		}
		chargeDetails.setChargeDetail(listOfChargeDetails);
		if(chargeDetails.getChargeDetail() != null && !chargeDetails.getChargeDetail().isEmpty()){
			pOrderLine.setChargeDetails(chargeDetails);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineChargeDetails]");
		}
	}

	/**
	 * Method to populate the Line Notes for Each Order Line Item.
	 *
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine - OrderLine Object
	 * @param pOrder - TRUOrderImpl Object
	 * @param pIsGiftWrapItem the is gift wrap item
	 * @param pGiftItemInfo the gift item info
	 */
	private void populateOrderLineNotes(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			OrderLine pOrderLine, TRUOrderImpl pOrder, Boolean pIsGiftWrapItem, RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineNotes]");
			vlogDebug("pShipItemRel: {0} pOrderLine: {1} pOrder: {2} pIsGiftWrapItem: {3} pGiftItemInfo:{4}", 
					pShipItemRel,pOrderLine,pOrder,pIsGiftWrapItem,pGiftItemInfo);
		}
		if(pShipItemRel == null){
			return;
		}
		Notes notes = new Notes();
		Note note = null;
		List<Note> listOfNotes = new ArrayList<Note>();
		TRUChannelHardgoodShippingGroup channerHGSG = null;
		TRUChannelInStorePickupShippingGroup channelISSG = null;
		ShippingGroup sg = pShipItemRel.getShippingGroup();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		if (isLoggingDebug()) {
			vlogDebug("sg: {0}",sg);
		}
		String bppSKUID = null;
		String noteText = null;
		if(configuration.isIncludeCR82Changes()){//CR-82 START
			if(sg != null && sg.getPriceInfo() != null && sg.getPriceInfo().getRawShipping() > TRUConstants.DOUBLE_ZERO){
				note = new Note();
				note.setNoteType(configuration.getNoteTypeShipping());
				note.setNoteText(getShippingFeeUIDWithDes());
				listOfNotes.add(note);
			}
			if(pShipItemRel.getSurcharge() != TRUConstants.DOUBLE_ZERO){
				note = new Note();
				note.setNoteType(configuration.getNoteTypeShipping());
				note.setNoteText(getShippingSurUIDWithDes());
				listOfNotes.add(note);
			}
			if(pShipItemRel.getE911Fees() != TRUConstants.DOUBLE_ZERO){
				note = new Note();
				note.setNoteType(configuration.getNoteTypeMisc());
				if(pShipItemRel.getCommerceItem() != null){
					RepositoryItem productRef = (RepositoryItem) pShipItemRel.getCommerceItem().getAuxiliaryData().getProductRef();
					note.setNoteText(getEWasteNoteText(productRef));
				}
				listOfNotes.add(note);
			}
		}//CR-82 END
		if (pIsGiftWrapItem && sg instanceof TRUHardgoodShippingGroup) {
			String giftMessage = ((TRUHardgoodShippingGroup)sg).getGiftWrapMessage();
			if (isLoggingDebug()) {
				vlogDebug("giftMessage: {0}",giftMessage);
			}
			if(!StringUtils.isBlank(giftMessage)){
				note = new Note();
				note.setNoteType(configuration.getGiftMessageNoteType());
				note.setNoteText(giftMessage);
				listOfNotes.add(note);
			}
			if(pGiftItemInfo != null){
				note = new Note();
				note.setNoteType(configuration.getGiftMessageVSNoteType());
				note.setNoteCode(configuration.getGiftMessageGWNoteCode());
				// Calling method to get the Gift Wrap UID
				noteText = getGiftInfoUID(pGiftItemInfo);
				note.setNoteText(noteText);
				listOfNotes.add(note);
			}
		}
		if(pShipItemRel != null && pShipItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName()) != null){
			RepositoryItem bppItem = (RepositoryItem) pShipItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
			bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
			note = new Note();
			note.setNoteType(configuration.getGiftMessageVSNoteType());
			note.setNoteCode(configuration.getGiftMessageBPPNoteCode());
			if(StringUtils.isNotBlank(bppSKUID)){
				// Calling method to get the BPP UID
				noteText = getBPPInfoUID(bppSKUID);
				note.setNoteText(noteText);
			}
			listOfNotes.add(note);
		}
		if(sg instanceof TRUChannelHardgoodShippingGroup){
			channerHGSG = (TRUChannelHardgoodShippingGroup) sg;
			note = new Note();
			note.setNoteType(configuration.getRegistryInfoNoteType());
			StringBuilder builder = new StringBuilder();
			builder.append(TRUOMSConstant.CHANNEL_FROM+channerHGSG.getChannelUserName());
			if(!StringUtils.isBlank(channerHGSG.getChannelCoUserName())){
				builder.append(TRUOMSConstant.CHANNEL_AND+channerHGSG.getChannelCoUserName());
			}
			builder.append(TRUOMSConstant.CHANNEL_HASH+channerHGSG.getChannelId());
			if(builder != null){
				note.setNoteText(builder.toString());
			}
			listOfNotes.add(note);
		}
		if(sg instanceof TRUChannelInStorePickupShippingGroup){
			channelISSG = (TRUChannelInStorePickupShippingGroup) sg;
			note = new Note();
			note.setNoteType(configuration.getRegistryInfoNoteType());
			StringBuilder builder = new StringBuilder();
			builder.append(TRUOMSConstant.CHANNEL_FROM+channelISSG.getChannelUserName());
			if(!StringUtils.isBlank(channelISSG.getChannelCoUserName())){
				builder.append(TRUOMSConstant.CHANNEL_AND+channelISSG.getChannelCoUserName());
			}
			builder.append(TRUOMSConstant.CHANNEL_HASH+channelISSG.getChannelId());
			if(builder != null){
				note.setNoteText(builder.toString());
			}
			listOfNotes.add(note);
		}
		if(listOfNotes != null && !listOfNotes.isEmpty()){
			notes.setNote(listOfNotes);
		}
		if(notes.getNote() != null && !notes.getNote().isEmpty()){
			pOrderLine.setNotes(notes);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineNotes]");
		}
	}
	
	/**
	 * Populate order line price details.
	 *
	 * @param pOrderLine : OrderLine Object
	 * @param pItemPriceInfo : TRUItemPriceInfo Object
	 * @param pQuantity : long qty
	 */
	private void populateOrderLinePriceDetails(OrderLine pOrderLine,
			TRUItemPriceInfo pItemPriceInfo, long pQuantity) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLinePriceDetails]");
			vlogDebug("pOrderLine: {0} pItemPriceInfo : {1} pQuantity : {2}",pOrderLine,pItemPriceInfo,pQuantity);
		}
		if(pItemPriceInfo == null || pOrderLine == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		PriceDetails priceDetails = new PriceDetails();
		priceDetails.setPrice(pricingTools.round(pItemPriceInfo.getSalePrice()));
		priceDetails.setExtendedPrice(pricingTools.round(pItemPriceInfo.getSalePrice() * pQuantity));
		priceDetails.setIsPriceOverridden(Boolean.FALSE);
		pOrderLine.setUnitPriceAmount(pricingTools.round(pItemPriceInfo.getSalePrice()));//Added as part of critical CR
		pOrderLine.setPriceDetails(priceDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLinePriceDetails]");
		}
	}

	/**
	 * This method is to populate the order line shipping info details.
	 * 
	 * @param pShippingInfo - ShippingInfo
	 * @param pShippingGroup - ShippingGroup
	 * @param pOrder - TRUOrderImpl Object
	 */
	private void populateOrderLineShippingInfo(ShippingInfo pShippingInfo,
			ShippingGroup pShippingGroup, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineShippingInfo]");
			vlogDebug("pShippingInfo: {0} pShippingGroup : {1}",pShippingInfo,pShippingGroup);
		}
		if(pShippingInfo == null || pShippingGroup == null){
			return;
		}
		TRUHardgoodShippingGroup hardgoodSG = null;
		TRUInStorePickupShippingGroup inStoreSG=null;
		ShippingAddress shippingAddress = null;
		String shipMethod = getOmsConfiguration().getShippingMethodMap().get(pShippingGroup.getShippingMethod());
		if(StringUtils.isNotBlank(shipMethod)){
			pShippingInfo.setShipVia(shipMethod);
		}
		String country = null;
		String phoneNumber = null;
		if(pShippingGroup instanceof TRUHardgoodShippingGroup){
			hardgoodSG = (TRUHardgoodShippingGroup) pShippingGroup;
			if(hardgoodSG != null){
				ContactInfo sgShippingAddress = (ContactInfo) hardgoodSG.getShippingAddress();
				if (isLoggingDebug()) {
					vlogDebug("HardGood ShippingAddress: {0}",sgShippingAddress);
				}
				shippingAddress = new ShippingAddress();
				shippingAddress.setFirstName(sgShippingAddress.getFirstName());
				shippingAddress.setLastName(sgShippingAddress.getLastName());
				shippingAddress.setAddressLine1(sgShippingAddress.getAddress1());
				shippingAddress.setAddressLine2(sgShippingAddress.getAddress2());
				shippingAddress.setAddressLine3(sgShippingAddress.getAddress3());
				shippingAddress.setCity(sgShippingAddress.getCity());
				shippingAddress.setStateProv(sgShippingAddress.getState());
				country = getOmsConfiguration().getCountryCodeMap().get(sgShippingAddress.getCountry());
				if(StringUtils.isBlank(country)){
					shippingAddress.setCountry(sgShippingAddress.getCountry());
				}else{
					shippingAddress.setCountry(country);
				}
				String postalCode = setPostalCode(sgShippingAddress.getPostalCode());
				shippingAddress.setPostalCode(postalCode);
				phoneNumber = formatPhoneNumer(sgShippingAddress.getPhoneNumber());
				shippingAddress.setPhone(phoneNumber);
				shippingAddress.setEmail(pOrder.getEmail());
			}
		}
		if(pShippingGroup instanceof TRUInStorePickupShippingGroup){
			inStoreSG = (TRUInStorePickupShippingGroup) pShippingGroup;
			if(inStoreSG != null){
				if (isLoggingDebug()) {
					vlogDebug("inStoreSG: {0}",inStoreSG);
				}
				shippingAddress = new ShippingAddress();
				shippingAddress.setFirstName(inStoreSG.getFirstName());
				shippingAddress.setLastName(inStoreSG.getLastName());
				phoneNumber = formatPhoneNumer(inStoreSG.getPhoneNumber());
				shippingAddress.setPhone(phoneNumber);
				shippingAddress.setEmail(inStoreSG.getEmail());
			}
		}
		if(shippingAddress != null){
			pShippingInfo.setShippingAddress(shippingAddress);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineShippingInfo]");
		}
	}

	/**
	 * Sets the postal code.
	 *
	 * @param pPostalCode the postal code
	 * @return the string
	 */
	private String setPostalCode(String pPostalCode) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setPostalCode]");
		}
		String postalCode = pPostalCode;
		if (!StringUtils.isEmpty(postalCode) && postalCode.length() > TRUOMSConstant.INT_FIVE && !postalCode.contains(TRUOMSConstant.HYPEN)){
			StringBuffer postalCodeBuffer = new StringBuffer();
			String postalCodeString1 = postalCode.substring(TRUOMSConstant.INT_ZERO, TRUOMSConstant.INT_FIVE);
			String postalCodeString2 = postalCode.substring(TRUOMSConstant.INT_FIVE, TRUOMSConstant.INT_NINE);
			postalCodeBuffer.append(postalCodeString1).append(TRUOMSConstant.HYPEN).append(postalCodeString2);
			postalCode = postalCodeBuffer.toString();
			postalCodeBuffer.setLength(TRUOMSConstant.INT_ZERO);
		} else {
			return postalCode;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineShippingInfo]");
		}
		return postalCode;
	}

	/**
	 * This method is to populate payment details.
	 * 
	 * @param pCustomerOrder - Customer Order object
	 * @param pOrder - Order
	 */
	@SuppressWarnings("unchecked")
	private void populatePaymentDetails(CustomerOrder pCustomerOrder,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populatePaymentDetails]");
			vlogDebug("pCustomerOrder: {0} pOrder : {1}",pCustomerOrder,pOrder);
		}
		if(pOrder == null || pCustomerOrder == null){
			return;
		}
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		TRUCreditCard creditCardPg = null;
		TRUPayPal payPal = null;
		TRUGiftCard giftCard = null;
		InStorePayment inStorePayment = null;
		ContactInfo billingAddress = null;
		String paymentGroupType = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		String sourceOfOrder = pOrder.getSourceOfOrder();
		TRUConfiguration truConfiguration = getTruConfiguration();
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			PaymentDetails paymentDetails = new PaymentDetails();
			List<PaymentDetail> listOfPds = new ArrayList<PaymentDetail>();
			PaymentDetail paymentDetail = null;
			for(PaymentGroup paymentGroup : paymentGroups){
				if (isLoggingDebug()) {
					vlogDebug("Payment Group: {0}",paymentGroup);
				}
				if(paymentGroup instanceof InStorePayment){
					continue;
				}
				paymentDetail = new PaymentDetail();
				paymentDetail.setExternalPaymentDetailId(paymentGroup.getId());
				paymentDetail.setAuthorizationAmount(pricingTools.round(paymentGroup.getAmountAuthorized()));
				paymentDetail.setChargeSequence(String.valueOf(paymentGroups.indexOf(paymentGroup)+TRUConstants.INTEGER_NUMBER_ONE));
				if(isPayInStoreOrder(pOrder)){
					paymentDetail.setPaymentEntryType(omsConfiguration.getPayInStotePaymentEntryType());
				}else if(StringUtils.isNotBlank(sourceOfOrder)){
					if(truConfiguration.getWebSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getWebPaymentEntryType());
					}
					if(truConfiguration.getMobileSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getMobilePaymentEntryType());
					}
					if(truConfiguration.getSosSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getSosPaymentEntryType());
					}
					if(truConfiguration.getCsrSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getCsrPaymentEntryType());
					}
				}else{
					paymentDetail.setPaymentEntryType(omsConfiguration.getPaymentEntryType());
				}
				if(paymentGroup instanceof TRUCreditCard){
					setCreditCardPG(Boolean.TRUE);
					creditCardPg = (TRUCreditCard) paymentGroup;
					paymentGroupType = (String) creditCardPg.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					paymentDetail.setCardType(omsConfiguration.getCreditCardTypeMap().get(creditCardPg.getCreditCardType()));
					String creditCardNumber = creditCardPg.getCreditCardNumber();
					paymentDetail.setAccountNumber(creditCardNumber);
					int length = creditCardPg.getCreditCardNumber().length();
					String accountDisplayNumber = creditCardNumber.substring(length-TRUConstants.FOUR, length);
					paymentDetail.setAccountDisplayNumber(XXXXXXXXXXXX+accountDisplayNumber);
					paymentDetail.setNameOnCard(creditCardPg.getNameOnCard());
					paymentDetail.setCardExpiryMonth(creditCardPg.getExpirationMonth());
					paymentDetail.setCardExpiryYear(creditCardPg.getExpirationYear());
					billingAddress = (ContactInfo) creditCardPg.getBillingAddress();
					if (isLoggingDebug()) {
						vlogDebug("credit card billingAddress: {0}",billingAddress);
					}
					setPaymentDetailBillingAddress(billingAddress,paymentDetail,pOrder);
				}else if(paymentGroup instanceof TRUGiftCard){
					giftCard = (TRUGiftCard) paymentGroup;
					paymentGroupType = (String) giftCard.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setSettlementAmount(pricingTools.round(paymentGroup.getAmountAuthorized()));
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					paymentDetail.setAccountNumber(giftCard.getGiftCardNumber());
					paymentDetail.setAccountDisplayNumber(giftCard.getGiftCardNumber());
					if(omsConfiguration.isGiftCardSecurityCode()){
						paymentDetail.setSecurityCode(giftCard.getGiftCardPin());
					}else{
						paymentDetail.setSecurityCode(omsConfiguration.getSecurityCode());
					}
					setBillToDetailsFromOrder(paymentDetail,pOrder);
				}else if(paymentGroup instanceof TRUPayPal){
					setPaypalPG(Boolean.TRUE);
					payPal = (TRUPayPal) paymentGroup;
					paymentGroupType = (String) payPal.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					paymentDetail.setAccountNumber(payPal.getToken());
					billingAddress = (ContactInfo) payPal.getBillingAddress();
					if (isLoggingDebug()) {
						vlogDebug("Paypal billingAddress: {0}",billingAddress);
					}
					setPaymentDetailBillingAddress(billingAddress,paymentDetail,pOrder);
				}else if(paymentGroup instanceof InStorePayment){
					inStorePayment = (InStorePayment) paymentGroup;
					paymentGroupType = (String) inStorePayment.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setPaymentEntryType(omsConfiguration.getPayInStotePaymentEntryType());
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					setBillToDetailsFromOrder(paymentDetail,pOrder);
				}
				// Method to populate the Payment Transactions Details.
				if(paymentGroup instanceof TRUGiftCard){
					populateTransFiledsForSettlement(paymentGroup,paymentDetail);
				}else{
					populatePaymentTransactionFields(paymentGroup,paymentDetail);
				}
				listOfPds.add(paymentDetail);
			}
			paymentDetails.setPaymentDetail(listOfPds);
			pCustomerOrder.setPaymentDetails(paymentDetails);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populatePaymentDetails]");
		}
	}

	/**
	 * This method will populate the customer  details to JSON object.
	 *
	 * @param pCustomerOrder - Customer Order object
	 * @param pOrder - Order
	 */
	private void populateCustomerInfo(CustomerOrder pCustomerOrder,
			TRUOrderImpl pOrder){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateCustomerInfo]");
			vlogDebug("pCustomerOrder: {0} pOrder : {1}",pCustomerOrder,pOrder);
		}
		if(pCustomerOrder == null || pOrder == null){
			return;
		}
		CustomerInfo customerInfo = null;
		RepositoryItem profile;
		try {
			String phoneNumber = null;
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pOrder);
			TRUPropertyManager propertyManager = getPropertyManager();
			List<PaymentDetail> paymentDetails = null;
			if(pCustomerOrder.getPaymentDetails() != null){
				paymentDetails = pCustomerOrder.getPaymentDetails().getPaymentDetail();
			}
			BillToDetail billingDetail = null;
			if(paymentDetails != null && !paymentDetails.isEmpty()){
				if(isCreditCardPG()){
					billingDetail = getCreditCardBillingDetails(paymentDetails);
				}else if(isPaypalPG()){
					billingDetail = getPayPalBillingDetails(paymentDetails);
				}else{
					billingDetail= paymentDetails.get(TRUConstants.ZERO).getBillToDetail();
				}
				if (isLoggingDebug()) {
					vlogDebug("paymentDetails: {0} billingDetail : {1}",paymentDetails,billingDetail);
				}
			}
			customerInfo = new CustomerInfo();
			if(profile != null && !profile.isTransient()){
				customerInfo.setExternalCustomerId(pOrder.getProfileId());
				customerInfo.setCustomerUserId((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerInfo.setCustomerFirstName((String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName()));
				customerInfo.setCustomerLastName((String) profile.getPropertyValue(propertyManager.getLastNamePropertyName()));
				if(billingDetail != null){
					phoneNumber = formatPhoneNumer(billingDetail.getPhone());
					customerInfo.setCustomerPhone(phoneNumber);
				}
				customerInfo.setCustomerEmail((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
			} else{
				if(billingDetail != null){
					customerInfo.setCustomerFirstName(getFirstName());
					customerInfo.setCustomerLastName(getLastName());
					phoneNumber = formatPhoneNumer(billingDetail.getPhone());
					customerInfo.setCustomerPhone(phoneNumber);
					if(StringUtils.isNotBlank(billingDetail.getEmail())){
						customerInfo.setCustomerEmail(billingDetail.getEmail());
					}else{
						customerInfo.setCustomerEmail(pOrder.getEmail());
					}
				}else{
					// Calling method to populate the Customer Info from the Shipping Info if Order is placed from
					// Pay In Store option or Gift Card or Combination of both
					populateCustomerInfoFromShippingGroup(pOrder,customerInfo);
				}
			}
			if(customerInfo != null){
				if(StringUtils.isBlank(customerInfo.getCustomerFirstName())){
					customerInfo.setCustomerFirstName(getFirstName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerLastName())){
					customerInfo.setCustomerLastName(getLastName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerPhone())){
					customerInfo.setCustomerPhone(getPhoneNumber());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerEmail())){
					customerInfo.setCustomerEmail(pOrder.getEmail());
				}
				pCustomerOrder.setCustomerInfo(customerInfo);
			}
		} catch (RepositoryException exc) {
			if (!StringUtils.isEmpty(pOrder.getId())) {
				getOmsErrorTools().updateOmsErrorItemForOrder(pOrder.getId(), TRUOMSConstant.CUSTOMER_ORDER_IMPORT_SERVICE_CALL);
			}
			if(isLoggingError()){
				vlogError("RepositoryException : occurred while getting profile for order : {0} with exception : {1}", pOrder,  exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCustomerInfo]");
		}
	}
	
	/**
	 * Method to populate the Customer Info form Shipping Group.
	 * @param pOrder : TRUOrderImpl
	 * @param pCustomerInfo : CustomerInfo
	 */
	@SuppressWarnings("unchecked")
	private void populateCustomerInfoFromShippingGroup(TRUOrderImpl pOrder,
			CustomerInfo pCustomerInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateCustomerInfoFromShippingGroup]");
			vlogDebug("pOrder: {0} pCustomerInfo : {1}",pOrder,pCustomerInfo);
		}
		if(pOrder == null || pCustomerInfo == null){
			return;
		}
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(shippingGroups == null || shippingGroups.isEmpty()){
			return;
		}
		TRUHardgoodShippingGroup truHGSH = null;
		TRUInStorePickupShippingGroup truINPSG=null;
		Address shippingAddress = null;
		for(ShippingGroup lShipGrp : shippingGroups){
			if(lShipGrp instanceof TRUHardgoodShippingGroup){
				truHGSH = (TRUHardgoodShippingGroup) lShipGrp;
				shippingAddress = truHGSH.getShippingAddress();
				break;
			}
			if(lShipGrp instanceof TRUInStorePickupShippingGroup){
				truINPSG = (TRUInStorePickupShippingGroup) lShipGrp;
				break;
			}
		}
		if(shippingAddress != null){
			pCustomerInfo.setCustomerFirstName(getFirstName());
			pCustomerInfo.setCustomerLastName(getLastName());
			if(shippingAddress instanceof ContactInfo){
				pCustomerInfo.setCustomerPhone(formatPhoneNumer(((ContactInfo)shippingAddress).getPhoneNumber()));
			}
			pCustomerInfo.setCustomerEmail(pOrder.getEmail());
		}else if(truINPSG != null){
			pCustomerInfo.setCustomerFirstName(getFirstName());
			pCustomerInfo.setCustomerLastName(getLastName());
			pCustomerInfo.setCustomerPhone(formatPhoneNumer(truINPSG.getPhoneNumber()));
			pCustomerInfo.setCustomerEmail(truINPSG.getEmail());
		}else{
			pCustomerInfo.setCustomerEmail(pOrder.getEmail());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCustomerInfoFromShippingGroup]");
		}
	}

	/**
	 * This method will populate the customer  details to JSON object.
	 *
	 * @param pCustomerMasterImportMessage - Customer Master Import
	 * @return String : JSON Request
	 */
	public String populateCustomerMasterImportDetails(
			TRUCustomerMasterImportMessage pCustomerMasterImportMessage) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateCustomerMasterImportDetails]");
			vlogDebug("pCustomerMasterImportMessage: {0}",pCustomerMasterImportMessage);
		}
		String customerMasterImportJSON = null;
		if(pCustomerMasterImportMessage != null){			
			CustomerMasterImport customerMasterImport = new CustomerMasterImport();
			CustomerFullDetails customerFullDetails = new CustomerFullDetails();
			if(StringUtils.isBlank(pCustomerMasterImportMessage.getCustomerFirstName())){
				customerFullDetails.setCustomerFirstName(TRUConstants.HYPHEN);
			}else{
				customerFullDetails.setCustomerFirstName(pCustomerMasterImportMessage.getCustomerFirstName());
			}
			if(StringUtils.isBlank(pCustomerMasterImportMessage.getCustomerLastName())){
				customerFullDetails.setCustomerLastName(TRUConstants.HYPHEN);
			}else{
				customerFullDetails.setCustomerLastName(pCustomerMasterImportMessage.getCustomerLastName());
			}
			customerFullDetails.setCustomerEmail(pCustomerMasterImportMessage.getCustomerEmail());
			customerFullDetails.setExternalCustomerId(pCustomerMasterImportMessage.getExternalCustomerId());
			if(!StringUtils.isBlank(pCustomerMasterImportMessage.getReferenceField10())){
				com.oms.custprofile.ReferenceFields referenceFields = new com.oms.custprofile.ReferenceFields();
				referenceFields.setReferenceField10(pCustomerMasterImportMessage.getReferenceField10());
				customerFullDetails.setReferenceFields(referenceFields);
			}
			customerMasterImport.setCustomerFullDetails(customerFullDetails);
			Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
			customerMasterImportJSON = gson1.toJson(customerMasterImport); // done
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCustomerMasterImportDetails]");
			vlogDebug("CustomerMasterImport JSON : {0}", customerMasterImportJSON);
		}
		return customerMasterImportJSON;
	}
	
	/**
	 * Method to get the sum of commerce item relationship amount for given shipping group.
	 * @param pLShipGroup - ShippingGroup Object
	 * @return double - Return the sum of all commerce Item relation average amount  
	 */
	@SuppressWarnings("unchecked")
	public double getCommerceItemRelAmount(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: getCommerceItemRelAmount]");
			vlogDebug("pLShipGroup: {0}", pLShipGroup);
		}
		if(pLShipGroup == null){
			return 0;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		if (isLoggingDebug()) {
			vlogDebug("commerceItemRelationships: {0}", commerceItemRelationships);
		}
		if(commerceItemRelationships == null || commerceItemRelationships.isEmpty()){
			return 0;
		}
		double amount = TRUConstants.DOUBLE_ZERO;
		CommerceItem commerceItem = null;
		TRUBppItemInfo bppItemInfo = null;
		double bppPrice = TRUConstants.DOUBLE_ZERO;
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItem = rel.getCommerceItem();
			if((!(commerceItem instanceof TRUGiftWrapCommerceItem)) && (!(commerceItem instanceof TRUDonationCommerceItem))){
				amount += rel.getAmountByAverage();
			}
			bppItemInfo = rel.getBppItemInfo();
			if(bppItemInfo != null && StringUtils.isNotBlank(bppItemInfo.getId())){
				bppPrice = bppItemInfo.getBppPrice() * rel.getQuantity();
				amount += bppPrice;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getCommerceItemRelAmount]");
			vlogDebug("amount: {0}", amount);
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(amount);
	}

	/**
	 * Method to get the pro-rated shipping discount amount. 
	 *
	 * @param pDiscountedAmount - double - Total shipping discount amount
	 * @param pAmountByAverage - double - commerce item relationship's average amount.
	 * @param pCommItemRelAmount - double - Total amount of commerce item relationship's average amount.
	 * @return double - pro-rated shipping discount amount.
	 */
	private double prorateShippingDiscount(double pDiscountedAmount,
			double pAmountByAverage,double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateShippingDiscount]");
			vlogDebug("pDiscountedAmount: {0} pAmountByAverage : {1} pCommItemRelAmount : {2}", 
					pDiscountedAmount,pAmountByAverage,pCommItemRelAmount);
		}
		double prorateShippingShare = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return prorateShippingShare;
		}
		prorateShippingShare = (pDiscountedAmount*pAmountByAverage)/pCommItemRelAmount;
		prorateShippingShare = getOrderManager().getPromotionTools().getPricingTools().round(prorateShippingShare);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateShippingDiscount]");
			vlogDebug("prorateShippingShare: {0}", prorateShippingShare);
		}
		return prorateShippingShare;
	}
	
	/**
	 * Removes the gift wrap relationships.
	 *
	 * @param pCommerceItemRelationships : List of TRUShippingGroupCommerceItemRelationship
	 * @return : List of TRUShippingGroupCommerceItemRelationship after removing the Gift Wrap Relationships
	 */
	public List<TRUShippingGroupCommerceItemRelationship> removeGiftWrapRelationships(
			List<TRUShippingGroupCommerceItemRelationship> pCommerceItemRelationships) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: removeGiftWrapRelationships]");
			vlogDebug("pCommerceItemRelationships: {0}", pCommerceItemRelationships);
		}
		if(pCommerceItemRelationships == null || pCommerceItemRelationships.isEmpty()){
			return pCommerceItemRelationships;
		}
		CommerceItem commerceItem = null;
		List<TRUShippingGroupCommerceItemRelationship> newListOfRel = new ArrayList<TRUShippingGroupCommerceItemRelationship>();
		for(TRUShippingGroupCommerceItemRelationship commItemRel : pCommerceItemRelationships){
			commerceItem = commItemRel.getCommerceItem();
			if(!(commerceItem instanceof TRUGiftWrapCommerceItem)){
				newListOfRel.add(commItemRel);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: removeGiftWrapRelationships]");
			vlogDebug("newListOfRel: {0}", newListOfRel);
		}
		return newListOfRel;
	}
	
	/**
	 * Populate gift wrap line item.
	 *
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pLCommItemRel : : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder : TRUOrderImpl Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is relationship is having gift wrap item.
	 * @param pGiftItemInfo : RepositoryItem - GiftItemInfo RepositoryItem
	 * @param pOrderLineCount : int
	 */
	private void populateGiftWrapLineItem(List<OrderLine> pListOfOrderlines,TRUShippingGroupCommerceItemRelationship pLCommItemRel, 
			TRUOrderImpl pOrder, long pGiftWrapRelQty, boolean pIsGiftWrapRel, RepositoryItem pGiftItemInfo, int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateGiftWrapLineItem]");
			vlogDebug("pListOfOrderlines: {0} pLCommItemRel:{1}", pListOfOrderlines,pLCommItemRel);
			vlogDebug("pOrder: {0} pGiftWrapRelQty:{1}", pOrder,pGiftWrapRelQty);
			vlogDebug("pIsGiftWrapRel: {0} pGiftItemInfo:{1} pOrderLineCount:{2}", pIsGiftWrapRel,pGiftItemInfo,pOrderLineCount);
		}
		if(pLCommItemRel == null || pListOfOrderlines == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingGroup shippingGroup = pLCommItemRel.getShippingGroup();
		CommerceItem commItem = pLCommItemRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		if(commItem != null){
			itemPriceInfo =  (TRUItemPriceInfo) commItem.getPriceInfo();
		}
		if (isLoggingDebug()) {
			vlogDebug("shippingGroup: {0} commItem:{1}", shippingGroup,commItem);
		}
		OrderLine orderLine = new OrderLine();
		orderLine.setOrderLineNumber(Integer.toString(pOrderLineCount));
		if(commItem != null){
			orderLine.setItemId(commItem.getCatalogRefId());
		}
		orderLine.setIsGift(Boolean.TRUE);
		orderLine.setOrderedQuantity(pGiftWrapRelQty);
		orderLine.setOrderedQtyUOM(omsConfiguration.getOrderedQtyUOM());
		////New Proeprties/////////////////
		orderLine.setCommerceItemId(pLCommItemRel.getCommerceItem().getId());
		orderLine.setShippingGroupId(shippingGroup.getId());
		orderLine.setRelationshipId(pLCommItemRel.getId());
		//New Proeprties/////////////////
		populateOrderLinePriceDetails(orderLine, itemPriceInfo,pGiftWrapRelQty);
		ShippingInfo shippingInfo = new ShippingInfo();
		if(pLCommItemRel.getShippingGroup() instanceof TRUHardgoodShippingGroup){
			shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionSTA());
			populateOrderLineShippingInfo(shippingInfo, pLCommItemRel.getShippingGroup(),pOrder);
		}else if(pLCommItemRel.getShippingGroup() instanceof TRUInStorePickupShippingGroup){
			TRUInStorePickupShippingGroup inStoreShipGrp = (TRUInStorePickupShippingGroup) pLCommItemRel.getShippingGroup();
			shippingInfo.setShipToFacility(inStoreShipGrp.getLocationId());
			if(pLCommItemRel.isWarehousePickup()){
				shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionSTS());
				populateOrderLineShippingInfo(shippingInfo, inStoreShipGrp,pOrder);
			}else{
				shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionCustomerPickup());
				populateOrderLineAllocationInfo(orderLine,inStoreShipGrp);
			}
		}
		orderLine.setShippingInfo(shippingInfo);
		//populate Tax Price Info Details.
		populateOrderLineTaxDetails(pLCommItemRel,pOrder,orderLine,pGiftItemInfo,pGiftWrapRelQty,pListOfOrderlines);
		populateOrderLineDiscountDetail(pLCommItemRel,orderLine,pOrder,pListOfOrderlines,pGiftWrapRelQty,pIsGiftWrapRel
				,pGiftItemInfo);
		//Populate charge details for the order line
		populateOrderLineChargeDetails(pLCommItemRel.getShippingGroup(),pLCommItemRel,orderLine
				,pGiftWrapRelQty,pIsGiftWrapRel,pListOfOrderlines,Boolean.TRUE,pGiftItemInfo,pOrder);
		//Populate notes for the order line
		populateOrderLineNotes(pLCommItemRel,orderLine,pOrder,Boolean.TRUE,pGiftItemInfo);
		//populateOrderLineCustomAttributes
		if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			populateOrderLineCustomerAttributes(shippingGroup,orderLine);
		}
		//Populate order line reference fields.
		populateOrderLineReferenceFiels(pLCommItemRel,orderLine,Boolean.TRUE,pGiftItemInfo,pOrder);
		pListOfOrderlines.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateGiftWrapLineItem]");
		}
	}
	

	/**
	 * Populate commerce line item.
	 *
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder : TRUOrderImpl Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is relationship is having gift wrap item.
	 * @param pOrderLineCount : int
	 */
	private void populateCommerceLineItem(List<OrderLine> pListOfOrderlines,TRUShippingGroupCommerceItemRelationship pLCommItemRel, 
			TRUOrderImpl pOrder, long pGiftWrapRelQty, boolean pIsGiftWrapRel, int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateCommerceLineItem]");
			vlogDebug("pListOfOrderlines: {0} pLCommItemRel:{1}", pListOfOrderlines,pLCommItemRel);
			vlogDebug("pOrder: {0} pGiftWrapRelQty:{1}", pOrder,pGiftWrapRelQty);
			vlogDebug("pIsGiftWrapRel: {0} pOrderLineCount:{1}", pIsGiftWrapRel,pOrderLineCount);
		}
		if(pLCommItemRel == null || pListOfOrderlines == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingGroup shippingGroup = pLCommItemRel.getShippingGroup();
		CommerceItem commItem = pLCommItemRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		if(commItem != null){
			itemPriceInfo =  (TRUItemPriceInfo) commItem.getPriceInfo();
		}
		if (isLoggingDebug()) {
			vlogDebug("shippingGroup: {0} commItem:{1}", shippingGroup,commItem);
		}
		OrderLine orderLine = new OrderLine();
		orderLine.setOrderLineNumber(Integer.toString(pOrderLineCount));
		if(commItem != null){
			orderLine.setItemId(commItem.getCatalogRefId());
		}
		orderLine.setIsGift(Boolean.FALSE);
		orderLine.setOrderedQuantity(pGiftWrapRelQty);
		orderLine.setOrderedQtyUOM(omsConfiguration.getOrderedQtyUOM());
		////New Proeprties/////////////////
		orderLine.setCommerceItemId(pLCommItemRel.getCommerceItem().getId());
		orderLine.setShippingGroupId(shippingGroup.getId());
		orderLine.setRelationshipId(pLCommItemRel.getId());
		////New Proeprties/////////////////
		populateOrderLinePriceDetails(orderLine, itemPriceInfo,pGiftWrapRelQty);
		ShippingInfo shippingInfo = new ShippingInfo();
		if(shippingGroup instanceof TRUHardgoodShippingGroup){
			shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionSTA());
			populateOrderLineShippingInfo(shippingInfo, pLCommItemRel.getShippingGroup(),pOrder);
		}else if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			TRUInStorePickupShippingGroup inStoreShipGrp = (TRUInStorePickupShippingGroup) pLCommItemRel.getShippingGroup();
			shippingInfo.setShipToFacility(inStoreShipGrp.getLocationId());
			if(pLCommItemRel.isWarehousePickup()){
				shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionSTS());
				populateOrderLineShippingInfo(shippingInfo, inStoreShipGrp,pOrder);
			}else{
				shippingInfo.setDeliveryOption(omsConfiguration.getDeliveryOptionCustomerPickup());
				populateOrderLineAllocationInfo(orderLine,inStoreShipGrp);
			}
		}
		orderLine.setShippingInfo(shippingInfo);
		//populate Tax Price Info Details.
		populateOrderLineTaxDetails(pLCommItemRel,pOrder,orderLine,null,pGiftWrapRelQty,pListOfOrderlines);
		populateOrderLineDiscountDetail(pLCommItemRel,orderLine,pOrder,
			pListOfOrderlines,pGiftWrapRelQty,pIsGiftWrapRel,null);
		//Populate charge details for the order line
		populateOrderLineChargeDetails(pLCommItemRel.getShippingGroup(),pLCommItemRel,orderLine,
				pGiftWrapRelQty,pIsGiftWrapRel,pListOfOrderlines,Boolean.FALSE,null,pOrder);
		//Populate notes for the order line
		populateOrderLineNotes(pLCommItemRel,orderLine,pOrder,Boolean.FALSE,null);
		//populateOrderLineCustomAttributes
		if(shippingGroup instanceof TRUInStorePickupShippingGroup){
			populateOrderLineCustomerAttributes(shippingGroup,orderLine);
		}
		//Populate order line reference fields.
		populateOrderLineReferenceFiels(pLCommItemRel,orderLine,Boolean.FALSE,null,pOrder);
		pListOfOrderlines.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateCommerceLineItem]");
		}
	}
	
	/**
	 * Method to set the Commerce Itema and Commerce Item quantity in map.
	 *
	 * @param pOrder : TRUOrderImpl Object
	 */
	@SuppressWarnings("unchecked")
	public void setCommItemAndCommQtyMap(TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setCommItemAndCommQtyMap]");
			vlogDebug("pOrder: {0}", pOrder);
		}
		if(pOrder == null){
			return;
		}
		Map<CommerceItem, Long> commItemQtyMap = getCommItemQtyMap();
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems != null && !commerceItems.isEmpty()){
			for (CommerceItem commerceItem : commerceItems) {
				if(!(commerceItem instanceof TRUGiftWrapCommerceItem)){
					commItemQtyMap.put(commerceItem, commerceItem.getQuantity());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setCommItemAndCommQtyMap]");
		}
		setCommItemQtyMap(commItemQtyMap);
	}
	
	/**
	 * Populate order line discount detail.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine : OrderLine Object
	 * @param pOrder : TRUOrderImpl Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is relationship is having gift wrap item.
	 * @param pGiftItemInfo : RepositoryItem - GiftWrapInfo RepositoryItem Item
	 */
	private void populateOrderLineDiscountDetail(
			TRUShippingGroupCommerceItemRelationship pShipItemRel, 
			OrderLine pOrderLine, TRUOrderImpl pOrder, 
			List<OrderLine> pListOfOrderlines, 
			long pGiftWrapRelQty, Boolean pIsGiftWrapRel,RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineDiscountDetail1]");
			vlogDebug("pShipItemRel: {0} pOrderLine: {1}", pShipItemRel,pOrderLine);
			vlogDebug("pOrder: {0} pListOfOrderlines: {1}", pOrder,pListOfOrderlines);
			vlogDebug("pGiftWrapRelQty: {0} pIsGiftWrapRel: {1}", pGiftWrapRelQty,pIsGiftWrapRel);
		}
		if(pShipItemRel == null || pOrderLine == null){
			return;
		}
		double commItemRelAmount = getCommItemRelAmount();
		DiscountDetails discountDetails = new DiscountDetails();
		List<DiscountDetail> listOfDiscDetails = new ArrayList<DiscountDetail>();
		// Calling method to populate the Item Discount Details
		populateItemDiscountDetails(pShipItemRel,pOrderLine,
				pListOfOrderlines,pGiftWrapRelQty,listOfDiscDetails,pOrder);
		// Calling method to populate the Order Discount Details
		populateOrderDiscountDetails(pShipItemRel,pOrderLine,pOrder,commItemRelAmount,
				pListOfOrderlines,pGiftWrapRelQty,pIsGiftWrapRel,listOfDiscDetails);
		// Calling method to populate the Shipping Discount Details
		populateShippingDiscountDetails(pShipItemRel,pOrderLine,commItemRelAmount,
				pListOfOrderlines,pGiftWrapRelQty,pIsGiftWrapRel,listOfDiscDetails,pOrder);
		// Calling method to populate the Gift Wrap Discount Details
		if(pGiftItemInfo != null){
			populateGiftWrapDiscountDetails(listOfDiscDetails,pOrder,pGiftItemInfo);
		}
		if(listOfDiscDetails != null && !listOfDiscDetails.isEmpty()){
			discountDetails.setDiscountDetail(listOfDiscDetails);
			pOrderLine.setDiscountDetails(discountDetails);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineDiscountDetail1]");
		}
	}

	/**
	 * Populate shipping discount details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine : OrderLine Object
	 * @param pCommItemRelAmount : double - Total ShippingGroupRelationship amount by average for Shipping Group
	 * @param pListOfOrderlines : List of OrderLine Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is relationship is having gift wrap item.
	 * @param pListOfDiscDetails : List of DiscountDetail
	 * @param pOrder : TRUOrderImpl Object
	 */
	@SuppressWarnings("unchecked")
	private void populateShippingDiscountDetails(TRUShippingGroupCommerceItemRelationship pShipItemRel,OrderLine pOrderLine,double pCommItemRelAmount,List<OrderLine> pListOfOrderlines,
			long pGiftWrapRelQty, Boolean pIsGiftWrapRel,List<DiscountDetail> pListOfDiscDetails, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateShippingDiscountDetails]");
			vlogDebug("pShipItemRel: {0} pOrderLine: {1} pCommItemRelAmount: {2} pListOfOrderlines: {3}", pShipItemRel,pOrderLine, pCommItemRelAmount,pListOfOrderlines);
			vlogDebug("pGiftWrapRelQty: {0} pIsGiftWrapRel: {1} pListOfDiscDetails: {2} pOrder: {3}", pGiftWrapRelQty,pIsGiftWrapRel, pListOfDiscDetails,pOrder);
		}
		if(pShipItemRel == null || pOrderLine == null){
			return;
		}
		boolean isLastRelItem = isLastRelationShip();
		ShippingGroup shippingGroup = pShipItemRel.getShippingGroup();
		if (isLoggingDebug()) {
			vlogDebug("isLastRelItem: {0} shippingGroup: {1}", isLastRelItem,shippingGroup);
		}
		if(shippingGroup == null || shippingGroup.getPriceInfo() == null){
			return;
		}
		TRUShippingPriceInfo shippingPriceInfo = (TRUShippingPriceInfo) shippingGroup.getPriceInfo();
		List<PricingAdjustment> pricingAdj = shippingPriceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		RepositoryItem promotion = null;
		RepositoryItem couponItem = null;
		String promoDesc = null;
		DiscountDetail discountDetail = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if (isLoggingDebug()) {
				vlogDebug("promotion: {0}", promotion);
			}
			if(promotion == null || -priceAdj.getAdjustment() <= TRUConstants.DOUBLE_ZERO){
				continue;
			}
			String couponCode = null;
			String singleUseCode = null;
			double proratedValue = TRUConstants.DOUBLE_ZERO;
			discountDetail = new DiscountDetail();
			discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
			couponItem = priceAdj.getCoupon();
			if(couponItem != null){
				couponCode = couponItem.getRepositoryId();
			}
			if(!StringUtils.isBlank(couponCode)){
				if(pOrder.getSingleUseCoupon() != null && !pOrder.getSingleUseCoupon().isEmpty()){
					singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
				}
				if(!StringUtils.isBlank(singleUseCode)){
					discountDetail.setExtDiscountId(singleUseCode);
				}else{
					discountDetail.setExtDiscountId(couponCode);
				}
			}else{
				discountDetail.setExtDiscountId(promotion.getRepositoryId());
			}
			discountDetail.setDiscountType(omsConfiguration.getCoupon());
			if(isLastRelItem){
				double share = TRUConstants.DOUBLE_ZERO;
				 share = getShare(pListOfOrderlines,shippingGroup,promotion,pShipItemRel);
				if(pIsGiftWrapRel){
					proratedValue = (-priceAdj.getAdjustment())-share;
					proratedValue = prorateShipDisForGiftWrapRel(proratedValue,pShipItemRel.getAmountByAverage(),pCommItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,promotion.getRepositoryId(),isLastRelItem);
				}else{
					proratedValue = (-priceAdj.getAdjustment()) - share;
				}
			}else{
				if(pIsGiftWrapRel){
					proratedValue = prorateShipDisForGiftWrapRel(-priceAdj.getAdjustment(),pShipItemRel.getAmountByAverage(),pCommItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,promotion.getRepositoryId(),isLastRelItem);
				}else{
					proratedValue = prorateShippingDiscount(-priceAdj.getAdjustment(),pShipItemRel.getAmountByAverage(),pCommItemRelAmount);
				}
			}if (isLoggingDebug()) {
				vlogDebug("proratedValue: {0}", proratedValue);
			}
			if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && (Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
				discountDetail.setDiscountAmount(TRUConstants.DOUBLE_ZERO);
				discountDetail.setReferenceField1(omsConfiguration.getVendorFunded());
				discountDetail.setReferenceField3(Double.toString(pricingTools.round(proratedValue)));
			}else{
				discountDetail.setDiscountAmount(pricingTools.round(proratedValue));
				discountDetail.setReferenceField1(omsConfiguration.getRetailFunded());
			}
			discountDetail.setReferenceField2((String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName()));
			// Calling method to get the shipping info from "Shipping Fee" subtype nonMerchSKU
			String shippingFeeUID = getShippingFeeUIDWithDes();
			discountDetail.setReferenceField4(shippingFeeUID);
			promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
			if(StringUtils.isBlank(promoDesc)){
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
			}
			discountDetail.setReferenceField5(promoDesc);
			pListOfDiscDetails.add(discountDetail);
		}if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateShippingDiscountDetails]");
		}
	}

	/**
	 * Gets the share.
	 *
	 * @param pListOfOrderlines the list of orderlines
	 * @param pShippingGroup the shipping group
	 * @param pPromotion the promotion
	 * @param pShipItemRel the ship item rel
	 * @return the share
	 */
	private double getShare(List<OrderLine> pListOfOrderlines, ShippingGroup pShippingGroup, RepositoryItem pPromotion, 
			TRUShippingGroupCommerceItemRelationship pShipItemRel) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: getShare]");
		}
		double share = TRUConstants.DOUBLE_ZERO;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		for(OrderLine lOrderLine : pListOfOrderlines){
			if(lOrderLine != null && pShippingGroup.getId().equals(lOrderLine.getShippingGroupId()) && 
					!pShipItemRel.getId().equals(lOrderLine.getRelationshipId()) && lOrderLine.getDiscountDetails() != null && 
					lOrderLine.getDiscountDetails().getDiscountDetail() != null && !lOrderLine.getDiscountDetails().getDiscountDetail().isEmpty()){
				for(DiscountDetail lDisDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
					String promoId = lDisDetail.getExtDiscountDetailId();
					if(StringUtils.isBlank(promoId) || !promoId.equals(pPromotion.getRepositoryId())){
						continue;
					}
					if(omsConfiguration.getRetailFunded().equals(lDisDetail.getReferenceField1())){
						share+=lDisDetail.getDiscountAmount();
					}else if(omsConfiguration.getVendorFunded().equals(lDisDetail.getReferenceField1())){
						try{
							share += Double.parseDouble(lDisDetail.getReferenceField3());
						}catch(NumberFormatException nfe){
							if (isLoggingError()) {
								vlogError("NumberFormatException : {0}", nfe);
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getShare]");
		}
		return share;
	}

	/**
	 * Populate order discount details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine : OrderLine Object
	 * @param pOrder : OrderLine Object
	 * @param pCommItemRelAmount : double - Total ShippingGroupRelationship amount by average for Shipping Group
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pGiftWrapRelQty : long qty
	 * @param pIsGiftWrapRel : boolean - Is relationship is having gift wrap item.
	 * @param pListOfDiscDetails : List of DiscountDetail
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderDiscountDetails(TRUShippingGroupCommerceItemRelationship pShipItemRel,OrderLine pOrderLine, TRUOrderImpl pOrder,double pCommItemRelAmount,
			List<OrderLine> pListOfOrderlines,long pGiftWrapRelQty, Boolean pIsGiftWrapRel,List<DiscountDetail> pListOfDiscDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderDiscountDetails]");
			vlogDebug("pShipItemRel: {0} pOrderLine: {1} pCommItemRelAmount: {2} pListOfOrderlines: {3}", pShipItemRel,pOrderLine, pCommItemRelAmount,pListOfOrderlines);
			vlogDebug("pGiftWrapRelQty: {0} pIsGiftWrapRel: {1} pListOfDiscDetails: {2} pOrder: {3}", pGiftWrapRelQty,pIsGiftWrapRel, pListOfDiscDetails,pOrder);
		}
		if(pShipItemRel == null || pOrderLine == null || pOrder == null || pOrder.getPriceInfo() == null){
			return;
		}
		boolean isLastRelItem = isLastRelationShip();
		TRUOrderPriceInfo orderPriceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
		double orderSubTotalForProrate = getOrderTotalForDiscount(orderPriceInfo,pOrder);
		if (isLoggingDebug()) {
			vlogDebug("isLastRelItem: {0} orderSubTotalForProrate: {1}", isLastRelItem,orderSubTotalForProrate);
		}
		if(orderSubTotalForProrate == TRUConstants.DOUBLE_ZERO){
			return;
		}
		List<PricingAdjustment> pricingAdj = orderPriceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		//CommerceItem commerceItem = pShipItemRel.getCommerceItem();
		RepositoryItem promotion = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingGroup shippingGroup = pShipItemRel.getShippingGroup();
		DiscountDetail discountDetail = null;
		String promoDesc = null;
		String promoId = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if(promotion == null || -priceAdj.getAdjustment() <= TRUConstants.DOUBLE_ZERO){
				continue;
			}
			String couponCode = null;
			String singleUseCode = null;
			double proratedValue = TRUConstants.DOUBLE_ZERO;
			promoId = promotion.getRepositoryId();
			double discountAmountToShare = (-priceAdj.getAdjustment()*pCommItemRelAmount)/orderSubTotalForProrate;
			//double discountAmountToShare = getOrderDiscountToShare(-priceAdj.getAdjustment(),pCommItemRelAmount,
			//		orderSubTotalForProrate,promoId);
			discountDetail = new DiscountDetail();
			discountDetail.setExtDiscountDetailId(promoId);
			RepositoryItem couponItem = priceAdj.getCoupon();
			if(couponItem != null){
				couponCode = couponItem.getRepositoryId();
			}
			if(!StringUtils.isBlank(couponCode)){
				if(pOrder.getSingleUseCoupon() != null && !pOrder.getSingleUseCoupon().isEmpty()){
					singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
				}
				if(!StringUtils.isBlank(singleUseCode)){
					discountDetail.setExtDiscountId(singleUseCode);
				}else{
					discountDetail.setExtDiscountId(couponCode);
				}
			}else{
				discountDetail.setExtDiscountId(promoId);
			}
			discountDetail.setDiscountType(omsConfiguration.getCoupon());
			if(isLastRelItem){
				double share = TRUConstants.DOUBLE_ZERO;
				share = getShare(pListOfOrderlines,shippingGroup,promotion,pShipItemRel);
				if(pIsGiftWrapRel){
					proratedValue = discountAmountToShare-share;
					proratedValue = prorateOrderDisForGiftWrapRel(proratedValue,pShipItemRel.getAmountByAverage(),pCommItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,promoId,isLastRelItem);
				}else{
					proratedValue = discountAmountToShare - share;
				}
			}else{
				if(pIsGiftWrapRel){
					proratedValue = prorateOrderDisForGiftWrapRel(discountAmountToShare,pShipItemRel.getAmountByAverage(),pCommItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,promoId,isLastRelItem);
				}else{
					proratedValue = prorateOrderDiscount(discountAmountToShare,pShipItemRel.getAmountByAverage(),pCommItemRelAmount);
				}
			}if (isLoggingDebug()) {
				vlogDebug("proratedValue: {0}", proratedValue);
			}
			if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
					(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
				discountDetail.setDiscountAmount(TRUConstants.DOUBLE_ZERO);
				discountDetail.setReferenceField1(omsConfiguration.getVendorFunded());
				discountDetail.setReferenceField3(Double.toString(pricingTools.round(proratedValue)));
			}else{
				discountDetail.setDiscountAmount(pricingTools.round(proratedValue));
				discountDetail.setReferenceField1(omsConfiguration.getRetailFunded());
			}
			discountDetail.setReferenceField2((String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName()));
			/*discountDetail.setReferenceField4(omsConfiguration.getDiscountDetailOrder()+omsConfiguration.getUidPipeDelimeter()+
					commerceItem.getCatalogRefId()+omsConfiguration.getUidPipeDelimeter()+commerceItem.getAuxiliaryData().getProductId());*/
			promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
			if(StringUtils.isBlank(promoDesc)){
				promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
			}
			discountDetail.setReferenceField5(promoDesc);
			pListOfDiscDetails.add(discountDetail);
		}if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderDiscountDetails]");
		}
	}

	/**
	 * Populate item discount details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrderLine : OrderLine Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pGiftWrapRelQty : long qty
	 * @param pListOfDiscDetails : List of DiscountDetail
	 * @param pOrder : TRUOrderImpl Object
	 */
	@SuppressWarnings("unchecked")
	private void populateItemDiscountDetails(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			OrderLine pOrderLine, List<OrderLine> pListOfOrderlines,
			long pGiftWrapRelQty,
			List<DiscountDetail> pListOfDiscDetails, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateItemDiscountDetails]");
			vlogDebug("pShipItemRel: {0} pShipItemRel: {1}", pShipItemRel,pOrderLine);
			vlogDebug("pListOfOrderlines: {0} pGiftWrapRelQty: {1}", pListOfOrderlines,pGiftWrapRelQty);
			vlogDebug("pListOfDiscDetails: {0} pOrder: {1}", pListOfDiscDetails,pOrder);
		}
		if(pShipItemRel == null || pOrderLine == null){
			return;
		}
		CommerceItem commerceItem = pShipItemRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = null;
		List<PricingAdjustment> pricingAdj = null;
		if(commerceItem != null && commerceItem.getPriceInfo()!= null){
			itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			String promoDesc = null;
			TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
			pricingAdj = itemPriceInfo.getAdjustments();
			if(pricingAdj != null && !pricingAdj.isEmpty()){
				DiscountDetail discountDetail = null;
				RepositoryItem promotion = null;
				RepositoryItem couponItem = null;
				for(PricingAdjustment priceAdj : pricingAdj){
					promotion = priceAdj.getPricingModel();
					if(promotion == null || -priceAdj.getTotalAdjustment() <= TRUConstants.DOUBLE_ZERO){
						continue;
					}
					if (isLoggingDebug()) {
						vlogDebug("promotion: {0}", promotion);
					}
					String couponCode = null;
					String singleUseCode = null;
					double proratedValue = TRUConstants.DOUBLE_ZERO;
					discountDetail = new DiscountDetail();
					discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
					couponItem = priceAdj.getCoupon();
					if(couponItem != null){
						couponCode = couponItem.getRepositoryId();
					}
					if(!StringUtils.isBlank(couponCode)){
						if(pOrder.getSingleUseCoupon() != null && 
								!pOrder.getSingleUseCoupon().isEmpty()){
							singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
						}
						if(!StringUtils.isBlank(singleUseCode)){
							discountDetail.setExtDiscountId(singleUseCode);
						}else{
							discountDetail.setExtDiscountId(couponCode);
						}
					}else{
						discountDetail.setExtDiscountId(promotion.getRepositoryId());
					}
					discountDetail.setDiscountType(omsConfiguration.getCoupon());
					proratedValue = prorateItemDiscount(-priceAdj.getTotalAdjustment(),pShipItemRel,commerceItem
							,itemPriceInfo,pListOfOrderlines,promotion.getRepositoryId(),pGiftWrapRelQty);
					if (isLoggingDebug()) {
						vlogDebug("proratedValue: {0}", proratedValue);
					}
					discountDetail.setDiscountAmount(pricingTools.round(proratedValue));
					if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
							(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
						discountDetail.setDiscountAmount(TRUConstants.DOUBLE_ZERO);
						discountDetail.setReferenceField1(omsConfiguration.getVendorFunded());
						discountDetail.setReferenceField3(Double.toString(pricingTools.round(proratedValue)));
					}else{
						discountDetail.setDiscountAmount(pricingTools.round(proratedValue));
						discountDetail.setReferenceField1(omsConfiguration.getRetailFunded());
					}
					discountDetail.setReferenceField2((String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName()));
					/*discountDetail.setReferenceField4(omsConfiguration.getDiscountDetailItem()+omsConfiguration.getUidPipeDelimeter()
							+commerceItem.getCatalogRefId()+omsConfiguration.getUidPipeDelimeter()+commerceItem.getAuxiliaryData().getProductId());*/
					promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
					if(StringUtils.isBlank(promoDesc)){
						promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
					}
					discountDetail.setReferenceField5(promoDesc);
					pListOfDiscDetails.add(discountDetail);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateItemDiscountDetails]");
		}
		
	}

	/**
	 * Prorate item discount.
	 *
	 * @param pTotalAdjustment : double - Item Discount Amount
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pCommerceItem : CommerceItem Object
	 * @param pItemPriceInfo : TRUItemPriceInfo Object
	 * @param pListOfOrderlines : list of OrderLine
	 * @param pPromotionId : String - Promotion Id
	 * @param pQty : long - qty
	 * @return : double - pro-rated item discount
	 */
	private double prorateItemDiscount(double pTotalAdjustment,
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			CommerceItem pCommerceItem,
			TRUItemPriceInfo pItemPriceInfo, List<OrderLine> pListOfOrderlines, String pPromotionId, long pQty) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateItemDiscount]");
			vlogDebug("pTotalAdjustment: {0} pShipItemRel: {1} pCommerceItem: {2}", pTotalAdjustment,pShipItemRel,pCommerceItem);
			vlogDebug("pItemPriceInfo: {0} pListOfOrderlines: {1}", pItemPriceInfo,pListOfOrderlines);
			vlogDebug("pPromotionId: {0} pQty: {1}", pPromotionId,pQty);
		}
		double prorateItemShare = TRUConstants.DOUBLE_ZERO;
		if(pItemPriceInfo == null || pCommerceItem == null || pShipItemRel == null || 
				pItemPriceInfo.getSalePrice() == TRUConstants.DOUBLE_ZERO){
			return prorateItemShare;
		}
		Map<CommerceItem, Map<String, Long>> mulitemDis = getCommerceItemQTYMapForMulItemDis();
		if(mulitemDis == null || mulitemDis.isEmpty()){
			return pTotalAdjustment;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Long> map = mulitemDis.get(pCommerceItem);
		if(map != null && !map.isEmpty() && map.containsKey(pPromotionId) && map.get(pPromotionId) > pQty){
			prorateItemShare = (pTotalAdjustment*pQty)/pCommerceItem.getQuantity();
			map.put(pPromotionId, map.get(pPromotionId)-pQty);
			mulitemDis.put(pCommerceItem, map);
		}else{
			double discountShared = TRUConstants.DOUBLE_ZERO;
			if(pListOfOrderlines == null || pListOfOrderlines.isEmpty()){
				return prorateItemShare = pTotalAdjustment-discountShared;
			}
			for(OrderLine orderLine : pListOfOrderlines){
				if((!StringUtils.isBlank(orderLine.getItemId())) && orderLine.getItemId().equals(pCommerceItem.getCatalogRefId()) && 
						orderLine.getDiscountDetails() != null && orderLine.getDiscountDetails().getDiscountDetail() != null && 
						!orderLine.getDiscountDetails().getDiscountDetail().isEmpty()){
					for(DiscountDetail lDisDetail : orderLine.getDiscountDetails().getDiscountDetail()){
						if(StringUtils.isBlank(pPromotionId) || !pPromotionId.equals(lDisDetail.getExtDiscountDetailId())){
							continue;
						}
						if(omsConfiguration.getRetailFunded().equals(lDisDetail.getReferenceField1())){
							discountShared += lDisDetail.getDiscountAmount();
						}else if(omsConfiguration.getVendorFunded().equals(lDisDetail.getReferenceField1())){
							try{
								discountShared += Double.parseDouble(lDisDetail.getReferenceField3());
							}catch(NumberFormatException nfe){
								if (isLoggingError()) {
									vlogError("NumberFormatException : {0}", nfe);
								}
							}
						}
					}
				}
			}
			prorateItemShare = pTotalAdjustment-discountShared;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateItemDiscount]");
			vlogDebug("prorateItemShare: {0}", prorateItemShare);
		}
		return prorateItemShare;
	}
	
	/**
	 * Prorate order discount.
	 *
	 * @param pAdjustment : double - Order Discount Amount
	 * @param pAmountByAverage : double - ShippingGropRelation Amount by average
	 * @param pCommItemRelAmount : double -
	 * @return : double - pro-rated order discount amount.
	 */
	private double prorateOrderDiscount(double pAdjustment, double pAmountByAverage,
			double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateOrderDiscount]");
			vlogDebug("pAdjustment: {0} pAmountByAverage: {1} pCommItemRelAmount: {2}", pAdjustment,pAmountByAverage,pCommItemRelAmount);
		}
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return TRUConstants.DOUBLE_ZERO;
		}
		double proratedValueForRel = (pAmountByAverage*pAdjustment)/pCommItemRelAmount;
		proratedValueForRel = getOrderManager().getPromotionTools().getPricingTools().round(proratedValueForRel);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateOrderDiscount]");
			vlogDebug("proratedValueForRel: {0}", proratedValueForRel);
		}
		return proratedValueForRel;
	}

	/**
	 * Prorate ship dis for gift wrap rel.
	 *
	 * @param pDiscountAmountToShare : double - Shipping Discount Amount
	 * @param pAmountByAverage : double - ShippingGropRelation Amount by average
	 * @param pCommItemRelAmount : double - Total ShippingGroupRelationship amount by average for Shipping Group
	 * @param pGiftWrapRelQty : long qty
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pPromotionId : String - Promotion Id
	 * @param pIsLastRelItem : boolean - Is last item in SGCIRelationship
	 * @return : double - pro-rated shipping discount amount
	 */
	private double prorateShipDisForGiftWrapRel(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,
			long pGiftWrapRelQty,
			TRUShippingGroupCommerceItemRelationship pShipItemRel, List<OrderLine> pListOfOrderlines, String pPromotionId, Boolean pIsLastRelItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateShipDisForGiftWrapRel]");
		}
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return TRUConstants.DOUBLE_ZERO;
		}
		double proratedValueForRel = TRUConstants.DOUBLE_ZERO;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulShipDis = getCommerceItemRelMapForMulShipDis();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(mulShipDis != null && !mulShipDis.isEmpty()){
			Map<TRUShippingGroupCommerceItemRelationship, Long> map = mulShipDis.get(pPromotionId);
			if(map != null && !map.isEmpty()){
				qty = map.get(pShipItemRel);
				map.put(pShipItemRel, qty-pGiftWrapRelQty);
				mulShipDis.put(pPromotionId, map);
				if(qty <= pGiftWrapRelQty){
					isLastItem = Boolean.TRUE;
				}
			}
		}
		if(!pIsLastRelItem){
			proratedValueForRel = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedValueForRel = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId()) && 
						lOrderLine.getDiscountDetails() != null && lOrderLine.getDiscountDetails().getDiscountDetail() != null
						&& !lOrderLine.getDiscountDetails().getDiscountDetail().isEmpty()){
					for(DiscountDetail lDiscountDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
						String promoId = lDiscountDetail.getExtDiscountDetailId();
						if(StringUtils.isBlank(promoId) || !promoId.equals(pPromotionId)){
							continue;
						}
						if(omsConfiguration.getRetailFunded().equals(lDiscountDetail.getReferenceField1())){
							share+=lDiscountDetail.getDiscountAmount();
						}else if(omsConfiguration.getVendorFunded().equals(lDiscountDetail.getReferenceField1())){
							try{
								share += Double.parseDouble(lDiscountDetail.getReferenceField3());
							}catch(NumberFormatException nfe){
								if (isLoggingError()) {
									vlogError("NumberFormatException : {0}", nfe);
								}
							}
						}
					}
				}
			}
			proratedValueForRel = proratedValueForRel-share;
			
		}else{
			proratedValueForRel = (proratedValueForRel*pGiftWrapRelQty)/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateShipDisForGiftWrapRel]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedValueForRel);
		
	}
	
	/**
	 * Prorate order discount for gift wrap relationship.
	 *
	 * @param pDiscountAmountToShare : double - Order Discount amount to share for SGCIRel Amount
	 * @param pAmountByAverage : double - ShippingGropRelation Amount by average
	 * @param pCommItemRelAmount : double - Total ShippingGroupRelationship amount by average for Shipping Group
	 * @param pGiftWrapRelQty long QTY
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pPromotionId : String - Promotion Id
	 * @param pIsLastRelItem : boolean
	 * @return : double - pro-rated Order Discount for GiftWrap Items
	 */
	private double prorateOrderDisForGiftWrapRel(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,
			long pGiftWrapRelQty,
			TRUShippingGroupCommerceItemRelationship pShipItemRel, List<OrderLine> pListOfOrderlines, String pPromotionId, Boolean pIsLastRelItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateOrderDisForGiftWrapRel]");
		}
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return 0;
		}
		double proratedValueForRel = TRUConstants.DOUBLE_ZERO;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulOrderDis = getCommerceItemRelMapForMulOrderDis();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(mulOrderDis != null && !mulOrderDis.isEmpty()){
			Map<TRUShippingGroupCommerceItemRelationship, Long> map = mulOrderDis.get(pPromotionId);
			if(map != null && !map.isEmpty()){
				qty = map.get(pShipItemRel);
				map.put(pShipItemRel, qty-pGiftWrapRelQty);
				mulOrderDis.put(pPromotionId, map);
				if(qty <= pGiftWrapRelQty){
					isLastItem = Boolean.TRUE;
				}
			}
		}
		if(!pIsLastRelItem){
			proratedValueForRel = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedValueForRel = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId()) && 
						lOrderLine.getDiscountDetails() != null && lOrderLine.getDiscountDetails().getDiscountDetail() != null
						&& !lOrderLine.getDiscountDetails().getDiscountDetail().isEmpty()){
					for(DiscountDetail lDiscountDetail : lOrderLine.getDiscountDetails().getDiscountDetail()){
						String promoId = lDiscountDetail.getExtDiscountDetailId();
						if(StringUtils.isBlank(promoId) || !promoId.equals(pPromotionId)){
							continue;
						}
						if(omsConfiguration.getRetailFunded().equals(lDiscountDetail.getReferenceField1())){
							share+=lDiscountDetail.getDiscountAmount();
						}else if(omsConfiguration.getVendorFunded().equals(lDiscountDetail.getReferenceField1())){
							try{
								share += Double.parseDouble(lDiscountDetail.getReferenceField3());
							}catch(NumberFormatException nfe){
								if (isLoggingError()) {
									vlogError("NumberFormatException : {0}", nfe);
								}
							}
						}
					}
				}
			}
			proratedValueForRel = proratedValueForRel-share;
			
		}else{
			proratedValueForRel = (proratedValueForRel*pGiftWrapRelQty)/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateOrderDisForGiftWrapRel]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedValueForRel);
		
	}
	
	/**
	 * Prorate shipping surcharge for gift wrap.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pSurcharge : double - SGCIRelationship Surcharge Amount
	 * @param pGiftWrapRelQty - long qty
	 * @param pListOfOrderlines - List of OrderLine
	 * @return : double - prorated surcharge amount
	 */
	private double prorateShippingSurchargeForGiftWrap(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			double pSurcharge, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateShippingSurchargeForGiftWrap]");
		}
		double praratedSurcharge = TRUConstants.DOUBLE_ZERO;
		if(pSurcharge == TRUConstants.DOUBLE_ZERO || pShipItemRel == null ||
				pShipItemRel.getQuantity() == TRUConstants.LONG_ZERO){
			return praratedSurcharge;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForSurcharge = getCommerceItemRelMapForSurcharge();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForSurcharge != null && !commerceItemRelMapForSurcharge.isEmpty()){
			qty = commerceItemRelMapForSurcharge.get(pShipItemRel);
			commerceItemRelMapForSurcharge.put(pShipItemRel, qty-pGiftWrapRelQty);
			setCommerceItemRelMapForSurcharge(commerceItemRelMapForSurcharge);
			if(qty <= pGiftWrapRelQty){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			ChargeDetails chargeDetails = null;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId())){
					chargeDetails = lOrderLine.getChargeDetails();
					if(chargeDetails != null && chargeDetails.getChargeDetail() != null && 
							!chargeDetails.getChargeDetail().isEmpty()){
						for(ChargeDetail lChargeDetail: chargeDetails.getChargeDetail()){
							if(SHIPPING_SURCHARGE_ITEM.equals(lChargeDetail.getItemType())){
								share += lChargeDetail.getChargeAmount();
							}
						}
					}
				}
			}
			praratedSurcharge = pSurcharge-share;
		}else{
			praratedSurcharge = (pSurcharge)*pGiftWrapRelQty/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateShippingSurchargeForGiftWrap]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(praratedSurcharge);
	}
	
	/**
	 * Prorate e911 fees gift wrap.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pE911Fees : double : EWaste Amount
	 * @param pGiftWrapRelQty : long qty
	 * @param pListOfOrderlines : List of OrderLine
	 * @return : double - prorated EWaste amount for gift warp items
	 */
	private double prorateE911FeesGiftWrap(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			double pE911Fees, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateE911FeesGiftWrap]");
		}
		double praratedE911Fees = TRUConstants.DOUBLE_ZERO;
		if(pE911Fees == TRUConstants.DOUBLE_ZERO || pShipItemRel == null ||
				pShipItemRel.getQuantity() == TRUConstants.LONG_ZERO){
			return praratedE911Fees;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForEWasteFee = getCommerceItemRelMapForEWasteFee();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForEWasteFee != null && !commerceItemRelMapForEWasteFee.isEmpty()){
			qty = commerceItemRelMapForEWasteFee.get(pShipItemRel);
			commerceItemRelMapForEWasteFee.put(pShipItemRel, qty-pGiftWrapRelQty);
			setCommerceItemRelMapForEWasteFee(commerceItemRelMapForEWasteFee);
			if(qty <= pGiftWrapRelQty){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			ChargeDetails chargeDetails = null;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId())){
					chargeDetails = lOrderLine.getChargeDetails();
					if(chargeDetails != null && chargeDetails.getChargeDetail() != null && 
							!chargeDetails.getChargeDetail().isEmpty()){
						for(ChargeDetail lChargeDetail: chargeDetails.getChargeDetail()){
							if(configuration.getChargeCategoryE911()
									.equals(lChargeDetail.getChargeCategory())){
								share += lChargeDetail.getChargeAmount();
							}
						}
					}
				}
			}
			praratedE911Fees = pE911Fees-share;
		}else{
			praratedE911Fees = (pE911Fees)*pGiftWrapRelQty/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateE911FeesGiftWrap]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(praratedE911Fees);
	}
	
	/**
	 * Prorate shipping price for gift wrap rel.
	 *
	 * @param pDiscountAmountToShare : double - Shipping price amount to be shared for gift warp items.
	 * @param pAmountByAverage : double - ShippingGropRelation Amount by average
	 * @param pCommItemRelAmount : double - Total ShippingGroupRelationship amount by average for Shipping Group
	 * @param pGiftWrapRelQty : long
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pIsLastRelItem : boolean -
	 * @return : double - prorated shipping price
	 */
	private double prorateShippingPriceForGiftWrapRel(double pDiscountAmountToShare,
			double pAmountByAverage, double pCommItemRelAmount,
			long pGiftWrapRelQty,
			TRUShippingGroupCommerceItemRelationship pShipItemRel, List<OrderLine> pListOfOrderlines, Boolean pIsLastRelItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateShippingPriceForGiftWrapRel]");
		}
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return 0;
		}
		double proratedValueForShipFee = TRUConstants.DOUBLE_ZERO;
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForShipFee = getCommerceItemRelMapForShipFee();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForShipFee != null && !commerceItemRelMapForShipFee.isEmpty()){
			qty = commerceItemRelMapForShipFee.get(pShipItemRel);
			commerceItemRelMapForShipFee.put(pShipItemRel, qty-pGiftWrapRelQty);
			setCommerceItemRelMapForShipFee(commerceItemRelMapForShipFee);
			if(qty <= pGiftWrapRelQty){
				isLastItem = Boolean.TRUE;
			}
		}
		if(!pIsLastRelItem){
			proratedValueForShipFee = (pDiscountAmountToShare*pAmountByAverage)/pCommItemRelAmount;
		}else{
			proratedValueForShipFee = pDiscountAmountToShare;
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId())
						&& lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null && 
						!lOrderLine.getChargeDetails().getChargeDetail().isEmpty()){
					for(ChargeDetail lChargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
						if(SHIPPING_ITEM.equals(lChargeDetail.getItemType())){
							share += lChargeDetail.getChargeAmount();
						}
					}
					
				}
			}
			proratedValueForShipFee = proratedValueForShipFee-share;
			
		}else{
			proratedValueForShipFee = (proratedValueForShipFee*pGiftWrapRelQty)/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateShippingPriceForGiftWrapRel]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(proratedValueForShipFee);
		
	}
	
	/**
	 * Populate order line customer attributes.
	 *
	 * @param pShippingGroup : ShippingGroup Object
	 * @param pOrderLine : OrderLine Object
	 */
	private void populateOrderLineCustomerAttributes(
			ShippingGroup pShippingGroup, OrderLine pOrderLine) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineCustomerAttributes]");
		}
		if(pOrderLine == null || !(pShippingGroup instanceof TRUInStorePickupShippingGroup)){
			return;
		}
		TRUInStorePickupShippingGroup inStorePickupShippingGroup = (TRUInStorePickupShippingGroup) pShippingGroup;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//CustomAttributeList customAttributeList = new CustomAttributeList();
		//List<CustomAttribute> listOfCustomAttribute = new ArrayList<CustomAttribute>();
		List<CustomAttributeList> listCustomAttributeList = new ArrayList<CustomAttributeList>();
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getFirstName())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getPrimaryFirstName());
			customAttributeList.setValue(inStorePickupShippingGroup.getFirstName());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getLastName())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getPrimaryLastName());
			customAttributeList.setValue(inStorePickupShippingGroup.getLastName());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getEmail())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getPrimaryEmail());
			customAttributeList.setValue(inStorePickupShippingGroup.getEmail());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getPhoneNumber())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getPrimaryPhone());
			customAttributeList.setValue(formatPhoneNumer(inStorePickupShippingGroup.getPhoneNumber()));
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltFirstName())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getProxyFirstName());
			customAttributeList.setValue(inStorePickupShippingGroup.getAltFirstName());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltLastName())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getProxyLastName());
			customAttributeList.setValue(inStorePickupShippingGroup.getAltLastName());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltEmail())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getProxyEmail());
			customAttributeList.setValue(inStorePickupShippingGroup.getAltEmail());
			listCustomAttributeList.add(customAttributeList);
		}
		if(!StringUtils.isBlank(inStorePickupShippingGroup.getAltPhoneNumber())){
			CustomAttributeList customAttributeList = new CustomAttributeList();
			customAttributeList.setName(omsConfiguration.getProxyPhone());
			customAttributeList.setValue(formatPhoneNumer(inStorePickupShippingGroup.getAltPhoneNumber()));
			listCustomAttributeList.add(customAttributeList);
		}
		if(listCustomAttributeList != null && !listCustomAttributeList.isEmpty()){
			pOrderLine.setCustomAttributeList(listCustomAttributeList);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineCustomerAttributes]");
		}
		
	}
	
	/**
	 * Prorate shipping price.
	 *
	 * @param pDiscountedAmount : double - Amount to be shared
	 * @param pAmountByAverage : double - Relationship amount by average
	 * @param pCommItemRelAmount : double - Total Relationship amount by average
	 * @return double : prorated shipping price
	 */
	public double prorateShippingPrice(double pDiscountedAmount,double pAmountByAverage,
			double pCommItemRelAmount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateShippingPrice]");
		}
		double prorateShippingShare = TRUConstants.DOUBLE_ZERO;
		if(pCommItemRelAmount == TRUConstants.DOUBLE_ZERO){
			return prorateShippingShare;
		}
		prorateShippingShare = (pDiscountedAmount*pAmountByAverage)/pCommItemRelAmount;
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateShippingPrice]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(prorateShippingShare);
	}
	
	/**
	 * Populate order line surcharge detail.
	 *
	 * @param pListOfChargeDetails : List of ChargeDetail
	 * @param pIsGiftWrapRel : boolean - Is relationship contain gift wrap item
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pListOfOrderlines : List of OrderLine
	 */
	private void populateOrderLineSurchargeDetail(
			List<ChargeDetail> pListOfChargeDetails, boolean pIsGiftWrapRel,
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineSurchargeDetail]");
		}
		if(pShipItemRel == null || pListOfChargeDetails == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail chargeDetail = new ChargeDetail();
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		String catalogRefId = null;
		if(pShipItemRel.getCommerceItem() != null){
			catalogRefId = pShipItemRel.getCommerceItem().getCatalogRefId();
		}
		//Setting Dummy Value. But Get the confirmation, we need to change the value
		chargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		chargeDetail.setChargeCategory(configuration.getChargeCategoryShipSurcharge());
		chargeDetail.setChargeName(getShippingSurchargeUID());
		chargeDetail.setChargeName(catalogRefId);
		chargeDetail.setItemType(SHIPPING_SURCHARGE_ITEM);
		if(pIsGiftWrapRel){
			proratedSurcharge = prorateShippingSurchargeForGiftWrap(pShipItemRel,pShipItemRel.getSurcharge(),
					pGiftWrapRelQty,pListOfOrderlines);
			chargeDetail.setChargeAmount(pricingTools.round(proratedSurcharge));
			chargeDetail.setUnitCharge(pricingTools.round(proratedSurcharge/pGiftWrapRelQty));
		}else{
			chargeDetail.setChargeAmount(pricingTools.round(pShipItemRel.getSurcharge()));
			chargeDetail.setUnitCharge(pricingTools.round(pShipItemRel.getSurcharge()/pShipItemRel.getQuantity()));
		}
		pListOfChargeDetails.add(chargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineSurchargeDetail]");
		}
	}

	/**
	 * Populate order line gift item details.
	 *
	 * @param pListOfChargeDetails : List of ChargeDetail
	 * @param pGiftItemInfo : RepositoryItem - GiftWrapInfo Object
	 * @param pOrder : TRUOrderImpl Object
	 * @param pGiftWrapRelQty : long qty
	 */
	private void populateOrderLineGiftItemDetails(
			List<ChargeDetail> pListOfChargeDetails,
			RepositoryItem pGiftItemInfo, TRUOrderImpl pOrder,
			long pGiftWrapRelQty) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineGiftItemDetails]");
		}
		if(pGiftItemInfo == null || pListOfChargeDetails == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail chargeDetail = new ChargeDetail();
		//Setting Dummy Value. But Get the confirmation, we need to change the value
		chargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		chargeDetail.setChargeCategory(configuration.getChargeCategoryVAS());
		String giftWrapCatalogRefId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
		chargeDetail.setChargeName(giftWrapCatalogRefId);
		String commerceId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
		try {
			CommerceItem commerceItem = pOrder.getCommerceItem(commerceId);
			if(commerceItem != null){
				ItemPriceInfo priceInfo = commerceItem.getPriceInfo();
				if(priceInfo != null){
					chargeDetail.setChargeAmount(pricingTools.round(priceInfo.getAmount()));
					chargeDetail.setUnitCharge(pricingTools.round(priceInfo.getAmount()/pGiftWrapRelQty));
				}
			}
		} catch (CommerceItemNotFoundException exc) {
			if (isLoggingError()) {
				vlogError("CommerceItemNotFoundException:  While getting commerce item : {0}", exc);
			}
		} catch (InvalidParameterException exc) {
			if (isLoggingError()) {
				vlogError("InvalidParameterException:   While getting commerce item : {0}", exc);
			}
		}
		pListOfChargeDetails.add(chargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineGiftItemDetails]");
		}
	}
	
	/**
	 * Populate order line e w911 fee details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pGiftWrapRelQty : long qty
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pIsGiftWrapRel : boolean - Is relationship contain gift wrap item
	 * @param pListOfChargeDetails : List of ChargeDetail
	 */
	private void populateOrderLineEW911FeeDetails(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines, boolean pIsGiftWrapRel, List<ChargeDetail> pListOfChargeDetails) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineW911FeeDetails]");
		}
		if(pShipItemRel == null || pListOfChargeDetails == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail chargeDetail = new ChargeDetail();
		//Setting Dummy Value. But Get the confirmation, we need to change the value
		chargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		chargeDetail.setChargeCategory(configuration.getChargeCategoryE911());
		String ewasteUID = null;
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		if(pShipItemRel.getCommerceItem() != null){
			RepositoryItem productRef = (RepositoryItem) pShipItemRel.getCommerceItem().getAuxiliaryData().getProductRef();
			// Calling method to get the EWaste UID
			ewasteUID = getCatalogTools().getEWasteUID(productRef);
		}
		chargeDetail.setChargeName(ewasteUID);
		if(pIsGiftWrapRel){
			proratedSurcharge = prorateE911FeesGiftWrap(pShipItemRel,pShipItemRel.getE911Fees(),
					pGiftWrapRelQty,pListOfOrderlines);
			chargeDetail.setChargeAmount(pricingTools.round(proratedSurcharge));
			chargeDetail.setUnitCharge(pricingTools.round(proratedSurcharge/pGiftWrapRelQty));
		}else{
			chargeDetail.setChargeAmount(pShipItemRel.getE911Fees());
			chargeDetail.setUnitCharge(pricingTools.round(pShipItemRel.getE911Fees()/pShipItemRel.getQuantity()));
		}
		pListOfChargeDetails.add(chargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineW911FeeDetails]");
		}
	}
	
	/**
	 * Populate order line shipping details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pIsGiftWrapRel : boolean - Is relationship contain gift wrap item
	 * @param pGiftWrapRelQty : long qty
	 * @param pListOfChargeDetails : List of ChargeDetail
	 */
	private void populateOrderLineShippingDetails(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			List<OrderLine> pListOfOrderlines, boolean pIsGiftWrapRel,
			long pGiftWrapRelQty, List<ChargeDetail> pListOfChargeDetails) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineShippingDetails]");
		}
		if(pShipItemRel == null || pListOfChargeDetails == null){
			return;
		}
		ShippingGroup shippingGroup = pShipItemRel.getShippingGroup();
		if(shippingGroup == null || shippingGroup.getPriceInfo() == null){
			return;
		}
		TRUShippingPriceInfo shipPriceInfo = (TRUShippingPriceInfo) shippingGroup.getPriceInfo();
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail chargeDetail = new ChargeDetail();
		//Setting Dummy Value. But Get the confirmation, we need to change the value
		chargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		chargeDetail.setChargeCategory(configuration.getChargeCategoryShipping());
		chargeDetail.setChargeName(getShippingFeeUID());
		chargeDetail.setItemType(SHIPPING_ITEM);
		double shipAmountToBeShared = shipPriceInfo.getRawShipping();
		double proratedValue = TRUConstants.DOUBLE_ZERO;
		double commItemRelAmount = getCommItemRelAmount();
		boolean isLastRelItem = isLastRelationShip();
		if(isLastRelItem){
			double share = TRUConstants.DOUBLE_ZERO;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && shippingGroup.getId().equals(lOrderLine.getShippingGroupId()) 	&&
						!pShipItemRel.getId().equals(lOrderLine.getRelationshipId()) &&
						lOrderLine.getChargeDetails() != null && lOrderLine.getChargeDetails().getChargeDetail() != null &&
						!lOrderLine.getChargeDetails().getChargeDetail().isEmpty()){
					for(ChargeDetail lChargeDetail : lOrderLine.getChargeDetails().getChargeDetail()){
						if(SHIPPING_ITEM.equals(lChargeDetail.getItemType())){
							share += lChargeDetail.getChargeAmount();
						}
					}
				}
			}
			if(pIsGiftWrapRel){
				proratedValue = shipAmountToBeShared-share;
				proratedValue = prorateShippingPriceForGiftWrapRel(proratedValue,pShipItemRel.getAmountByAverage()
						,commItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,isLastRelItem);
				
			}else{
				proratedValue = shipAmountToBeShared - share;
			}
		}else{
			if(pIsGiftWrapRel){
				proratedValue = prorateShippingPriceForGiftWrapRel(shipAmountToBeShared,pShipItemRel.getAmountByAverage()
						,commItemRelAmount,pGiftWrapRelQty,pShipItemRel,pListOfOrderlines,
						isLastRelItem);
				
			}else{
				proratedValue = prorateShippingPrice(shipAmountToBeShared,pShipItemRel.getAmountByAverage()
						,commItemRelAmount);
			}
		}
		if(pIsGiftWrapRel){
			chargeDetail.setChargeAmount(pricingTools.round(proratedValue));
			chargeDetail.setUnitCharge(pricingTools.round(proratedValue/pGiftWrapRelQty));
		}else{
			chargeDetail.setChargeAmount(pricingTools.round(proratedValue));
			chargeDetail.setUnitCharge(pricingTools.round(proratedValue/pShipItemRel.getQuantity()));
		}
		pListOfChargeDetails.add(chargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineShippingDetails]");
		}
	}

	/**
	 * Sets the rel and rel qty for order dis.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForOrderDis(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForOrderDis]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForOrderDis = getCommerceItemRelMapForOrderDis();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForOrderDis.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForOrderDis(commerceItemRelMapForOrderDis);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForOrderDis]");
		}
	}
	
	/**
	 * Sets the rel and rel qty for ship dis.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForShipDis(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForShipDis]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForShipDis = getCommerceItemRelMapForShipDis();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForShipDis.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForShipDis(commerceItemRelMapForShipDis);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForShipDis]");
		}
	}
	
	/**
	 * Sets the rel and rel qty for ship fee.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForShipFee(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForShipFee]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForShipFee = getCommerceItemRelMapForShipFee();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForShipFee.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForShipFee(commerceItemRelMapForShipFee);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForShipFee]");
		}
	}
	
	/**
	 * Sets the rel and rel qty for bpp.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForBPP(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForBPP]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForBPP = getCommerceItemRelMapForBPP();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForBPP.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForBPP(commerceItemRelMapForBPP);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForBPP]");
		}
	}
	
	/**
	 * Sets the rel and rel qty for e waste fee.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForEWasteFee(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForEWasteFee]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForEWasteFee = getCommerceItemRelMapForEWasteFee();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForEWasteFee.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForEWasteFee(commerceItemRelMapForEWasteFee);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForEWasteFee]");
		}
	}
	
	/**
	 * Sets the rel and rel qty for surcharge.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForSurcharge(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForSurcharge]");
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForSurcharge = getCommerceItemRelMapForSurcharge();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItemRelMapForSurcharge.put(rel,rel.getQuantity());
		}
		setCommerceItemRelMapForSurcharge(commerceItemRelMapForSurcharge);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForSurcharge]");
		}
	}


	/**
	 * Gets the order total for discount.
	 *
	 * @param pOrderPriceInfo : TRUOrderPriceInfo Object
	 * @param pOrder : TRUOrderImpl object
	 * @return double : Order Discount value
	 */
	@SuppressWarnings("unchecked")
	public double getOrderTotalForDiscount(TRUOrderPriceInfo pOrderPriceInfo,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: getOrderTotalForDiscount]");
		}
		double orderTotalForDiscount = TRUConstants.DOUBLE_ZERO;
		if(pOrderPriceInfo == null || pOrder == null){
			return orderTotalForDiscount;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			return orderTotalForDiscount;
		}
		orderTotalForDiscount = pOrderPriceInfo.getRawSubtotal();
		for(CommerceItem lCommerceItem : commerceItems){
			if(lCommerceItem instanceof TRUDonationCommerceItem){
				orderTotalForDiscount -= ((TRUDonationCommerceItem)lCommerceItem).getDonationAmount();
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getOrderTotalForDiscount]");
		}
		return orderTotalForDiscount;
	}
	
	/**
	 * Populate donation item details.
	 *
	 * @param pListOfOrderlines : List of OrderLine
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder : TRUOrderImpl Order
	 * @param pOrderLineCount : int
	 */
	private void populateDonationItemDetails(List<OrderLine> pListOfOrderlines, 
			TRUShippingGroupCommerceItemRelationship pLCommItemRel, TRUOrderImpl pOrder, int pOrderLineCount) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateDonationItemDetails]");
		}
		if(pLCommItemRel == null || pListOfOrderlines == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingGroup shippingGroup = pLCommItemRel.getShippingGroup();
		CommerceItem commItem = pLCommItemRel.getCommerceItem();
		TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) commItem.getPriceInfo();
		OrderLine orderLine = new OrderLine();
		orderLine.setOrderLineNumber(Integer.toString(pOrderLineCount));
		orderLine.setItemId(commItem.getCatalogRefId());
		orderLine.setIsGift(Boolean.FALSE);
		orderLine.setOrderedQuantity(pLCommItemRel.getQuantity());
		orderLine.setOrderedQtyUOM(omsConfiguration.getOrderedQtyUOM());
		////New Proeprties/////////////////
		orderLine.setCommerceItemId(pLCommItemRel.getCommerceItem().getId());
		orderLine.setShippingGroupId(shippingGroup.getId());
		orderLine.setRelationshipId(pLCommItemRel.getId());
		////New Proeprties/////////////////
		populateOrderLinePriceDetails(orderLine, itemPriceInfo,pLCommItemRel.getQuantity());
		//populate Tax Price Info Details.
		populateOrderLineTaxDetails(pLCommItemRel,pOrder,orderLine,null,pLCommItemRel.getQuantity(),pListOfOrderlines);
		pListOfOrderlines.add(orderLine);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateDonationItemDetails]");
		}
	}
	
	/**
	 * Populate order line bpp details.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship Object
	 * @param pListOfChargeDetails : List of ChargeDetail
	 * @param pIsGiftWrapRel : boolean - is relationship contains gift item
	 * @param pGiftWrapRelQty : long - qty
	 * @param pListOfOrderlines : List of OrderLine
	 */
	private void populateOrderLineBPPDetails(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			List<ChargeDetail> pListOfChargeDetails, boolean pIsGiftWrapRel, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Entering into [Class: TRUOMSUtils  method: populateOrderLineBPPDetails]");
		}
		if(pShipItemRel == null || pListOfChargeDetails == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem bppItem = (RepositoryItem) pShipItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem == null){
			return;
		}
		double bppCharge = ((double) bppItem.getPropertyValue(commercePropertyManager.getBppPricePropertyName()))*pShipItemRel.getQuantity();
		if(bppCharge <= TRUConstants.DOUBLE_ZERO){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		ChargeDetail chargeDetail = new ChargeDetail();
		double proratedSurcharge = TRUConstants.DOUBLE_ZERO;
		//Setting Dummy Value. But Get the confirmation, we need to change the value
		chargeDetail.setExtChargeDetailId(TRUOMSConstant.DUMMY_VALUE);
		chargeDetail.setChargeCategory(configuration.getChargeCategoryVAS());
		chargeDetail.setItemType(BPP_ITEM);
		String bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
		chargeDetail.setChargeName(bppSKUID);
		if(pIsGiftWrapRel){
			proratedSurcharge = prorateBPPChargeForGiftWrap(pShipItemRel, bppCharge, pGiftWrapRelQty, pListOfOrderlines);
			chargeDetail.setChargeAmount(pricingTools.round(proratedSurcharge));
			chargeDetail.setUnitCharge(pricingTools.round(proratedSurcharge/pGiftWrapRelQty));
		}else{
			chargeDetail.setChargeAmount(pricingTools.round(bppCharge));
			chargeDetail.setUnitCharge(pricingTools.round(bppCharge/pShipItemRel.getQuantity()));
		}
		pListOfChargeDetails.add(chargeDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineBPPDetails]");
		}
	}
	
	/**
	 * Prorate bpp charge for gift wrap.
	 *
	 * @param pShipItemRel : TRUShippingGroupCommerceItemRelationship - Object
	 * @param pBPPCharge : double - BPP Charge
	 * @param pGiftWrapRelQty - long : Qty
	 * @param pListOfOrderlines - List : List of order line
	 * @return double : Prorated BPP charge for Gift Wrap Item
	 */
	private double prorateBPPChargeForGiftWrap(
			TRUShippingGroupCommerceItemRelationship pShipItemRel,
			double pBPPCharge, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: prorateBPPChargeForGiftWrap]");
		}
		double praratedSurcharge = TRUConstants.DOUBLE_ZERO;
		if(pBPPCharge == TRUConstants.DOUBLE_ZERO || pShipItemRel == null
				|| pShipItemRel.getQuantity() == TRUConstants.LONG_ZERO){
			return praratedSurcharge;
		}
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapForBPP = getCommerceItemRelMapForBPP();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		long qty = TRUConstants.LONG_ZERO;
		boolean isLastItem = Boolean.FALSE;
		if(commerceItemRelMapForBPP != null && !commerceItemRelMapForBPP.isEmpty()){
			qty = commerceItemRelMapForBPP.get(pShipItemRel);
			commerceItemRelMapForBPP.put(pShipItemRel, qty-pGiftWrapRelQty);
			setCommerceItemRelMapForBPP(commerceItemRelMapForBPP);
			if(qty <= pGiftWrapRelQty){
				isLastItem = Boolean.TRUE;
			}
		}
		if(isLastItem){
			double share = TRUConstants.DOUBLE_ZERO;
			ChargeDetails chargeDetails = null;
			for(OrderLine lOrderLine : pListOfOrderlines){
				if(lOrderLine != null && pShipItemRel.getId().equals(lOrderLine.getRelationshipId())){
					chargeDetails = lOrderLine.getChargeDetails();
					if(chargeDetails != null && chargeDetails.getChargeDetail() != null &&
							!chargeDetails.getChargeDetail().isEmpty()){
						for(ChargeDetail lChargeDetail: chargeDetails.getChargeDetail()){
							if(configuration.getChargeCategoryVAS()
									.equals(lChargeDetail.getChargeCategory()) && BPP_ITEM.equals(lChargeDetail.getItemType())){
								share += lChargeDetail.getChargeAmount();
							}
						}
					}
				}
			}
			praratedSurcharge = pBPPCharge-share;
		}else{
			praratedSurcharge = (pBPPCharge)*pGiftWrapRelQty/pShipItemRel.getQuantity();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: prorateBPPChargeForGiftWrap]");
		}
		return getOrderManager().getPromotionTools().getPricingTools().round(praratedSurcharge);
	}
	
	/**
	 * Method to populate the tax details to Order Line Items.
	 *
	 * @param pLCommItemRel - TRUShippingGroupCommerceItemRelationship Object
	 * @param pOrder - TRUOrderImpl Object
	 * @param pOrderLine - OrderLine Object
	 * @param pGiftItemInfo - Repository Item
	 * @param pGiftWrapRelQty the gift wrap rel qty
	 * @param pListOfOrderlines the list of orderlines
	 */
	private void populateOrderLineTaxDetails(
			TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			TRUOrderImpl pOrder, OrderLine pOrderLine, RepositoryItem pGiftItemInfo, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineTaxDetails]");
		}
		if(pOrder == null || pOrderLine == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		TaxDetails taxDetails = new TaxDetails();
		List<TaxDetail> listOfTaxDetail = new ArrayList<TaxDetail>();
		// Calling method to populate the tax details for Noraml Items.
		populateNormalItemTaxDetails(pLCommItemRel,pOrder,listOfTaxDetail,pGiftWrapRelQty,pListOfOrderlines);
		// Calling method to populate the tax details for Gift Wrap Items.
		populateGiftItemTaxDetails(pOrder,pGiftItemInfo,listOfTaxDetail);
		// Calling method to populate the tax details for EWaste Fee.
		populateEWasteTaxDetails(pLCommItemRel,pOrder,listOfTaxDetail,pGiftWrapRelQty,pListOfOrderlines);
		// Calling method to populate the tax details for BPP Item.
		populateBPPItemTaxDetails(pLCommItemRel,pOrder,listOfTaxDetail,pGiftWrapRelQty,pListOfOrderlines);
		if(listOfTaxDetail != null && !listOfTaxDetail.isEmpty()){
			taxDetails.setTaxDetail(listOfTaxDetail);
		}
		if(taxDetails != null && taxDetails.getTaxDetail() != null && !taxDetails.getTaxDetail().isEmpty()){
			pOrderLine.setTaxDetails(taxDetails);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineTaxDetails]");
		}
	}
	
	/**
	 * Method to set the Relationship and Relationship quantity
	 * for all the variable which will be used for pro-ration of amount. 
	 * @param pLShipGroup : ShippingGroup Object
	 * @param pOrder : TRUOrderImpl
	 */
	public void setRelAndRelQtyForProration(ShippingGroup pLShipGroup, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setRelAndRelQtyForProration]");
			vlogDebug("pLShipGroup: {0}", pLShipGroup);
		}
		if(pLShipGroup == null){
			return;
		}
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Oder Discount value for all line items.
		 */
		setRelAndRelQtyForOrderDis(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Shipping Discount value for all line items.
		 */
		setRelAndRelQtyForShipDis(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Shipping Surcharge value for all line items.
		 */
		setRelAndRelQtyForSurcharge(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Ewate Fee value for all line items.
		 */
		setRelAndRelQtyForEWasteFee(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Shipping Fee value for all line items.
		 */
		setRelAndRelQtyForShipFee(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the BPP value line items.
		 */
		setRelAndRelQtyForBPP(pLShipGroup);
		/**
		 * Setting the giftwrapCommerceId and relationship id to variable, which 
		 * will be used to populate the tax details for GiftWrap Items.
		 */
		setGWIdAndRelId(pLShipGroup);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Oder Discount value for all line items.
		 */
		setRelAndRelQtyForMulOrderDis(pLShipGroup,pOrder);
		/**
		 * Setting the relationship and relationship qty to variable, which 
		 * will be used while pro-rating the Shipping Discount value for all line items.
		 */
		setRelAndRelQtyForMulShipDis(pLShipGroup);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setRelAndRelQtyForProration]");
		}
	}
	

	/**
	 * This method will use to populate the Allocation Info if SG type is ISPU.
	 * @param pOrderLine : OrderLine - Object
	 * @param pInStoreShipGrp : TRUInStorePickupShippingGroup - Object
	 */
	private void populateOrderLineAllocationInfo(OrderLine pOrderLine,
			TRUInStorePickupShippingGroup pInStoreShipGrp) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: populateOrderLineAllocationInfo]");
			vlogDebug("pOrderLine: {0} pInStoreShipGrp:{1}", pOrderLine,pInStoreShipGrp);
		}
		if(pOrderLine == null || pInStoreShipGrp == null){
			return;
		}
		AllocationInfo allocationInfo = new AllocationInfo();
		allocationInfo.setFulfillmentFacility(pInStoreShipGrp.getLocationId());
		pOrderLine.setAllocationInfo(allocationInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateOrderLineAllocationInfo]");
		}
	}
	
	/**
	 * This method is used to generate the discount detail for GiftWrap Discount.
	 * @param pListOfDiscDetails : List
	 * @param pOrder : TRUOrderImpl
	 * @param pGiftItemInfo : RepositoryItem
	 */
	@SuppressWarnings("unchecked")
	private void populateGiftWrapDiscountDetails(List<DiscountDetail> pListOfDiscDetails,
			TRUOrderImpl pOrder, RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateGiftWrapDiscountDetails]");
			vlogDebug("pOrder : {0} pListOfDiscDetails : {1} pGiftItemInfo : {2}" ,pOrder,pListOfDiscDetails,pGiftItemInfo);
		}
		if(pGiftItemInfo == null || pOrder == null){
			return;
		}
		String gwCommId = (String) pGiftItemInfo.getPropertyValue(((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager().getGiftWrapCommItemId());
		if(StringUtils.isNotBlank(gwCommId)){
			try {
				List<PricingAdjustment> pricingAdj = null;
				CommerceItem commerceItem = pOrder.getCommerceItem(gwCommId);
				if(commerceItem == null || commerceItem.getPriceInfo()!= null){
					return;
				}
				String promoDesc = null;
				TRUItemPriceInfo itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
				TRUPricingModelProperties pricingModelProp = ((TRUOrderTools) getOrderManager().getOrderTools()).getPricingModelProperties();
				TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
				TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
				pricingAdj = itemPriceInfo.getAdjustments();
				if(pricingAdj != null && !pricingAdj.isEmpty()){
					DiscountDetail discountDetail = null;
					RepositoryItem promotion = null;
					RepositoryItem couponItem = null;
					for(PricingAdjustment priceAdj : pricingAdj){
						promotion = priceAdj.getPricingModel();
						if(promotion == null || -priceAdj.getTotalAdjustment() <= TRUConstants.DOUBLE_ZERO){
							continue;
						}
						if (isLoggingDebug()) {
							vlogDebug("promotion: {0}", promotion);
						}
						String couponCode = null;
						String singleUseCode = null;
						discountDetail = new DiscountDetail();
						discountDetail.setExtDiscountDetailId(promotion.getRepositoryId());
						couponItem = priceAdj.getCoupon();
						if(couponItem != null){
							couponCode = couponItem.getRepositoryId();
						}
						if(!StringUtils.isBlank(couponCode)){
							if(pOrder.getSingleUseCoupon() != null && 
									!pOrder.getSingleUseCoupon().isEmpty()){
								singleUseCode = pOrder.getSingleUseCoupon().get(couponCode);
							}
							if(!StringUtils.isBlank(singleUseCode)){
								discountDetail.setExtDiscountId(singleUseCode);
							}else{
								discountDetail.setExtDiscountId(couponCode);
							}
						}else{
							discountDetail.setExtDiscountId(promotion.getRepositoryId());
						}
						discountDetail.setDiscountType(omsConfiguration.getCoupon());
						if(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName())!= null && 
								(Boolean)(promotion.getPropertyValue(pricingModelProp.getVendorFundedPropertyName()))){
							discountDetail.setDiscountAmount(TRUConstants.DOUBLE_ZERO);
							discountDetail.setReferenceField1(omsConfiguration.getVendorFunded());
							discountDetail.setReferenceField3(Double.toString(pricingTools.round(-priceAdj.getTotalAdjustment())));
						}else{
							discountDetail.setDiscountAmount(pricingTools.round(-priceAdj.getTotalAdjustment()));
							discountDetail.setReferenceField1(omsConfiguration.getRetailFunded());
						}
						discountDetail.setReferenceField2((String)promotion.getPropertyValue(pricingModelProp.getCampaignIdPropertyName()));
						// Calling method to get the gift wrap UID
						String chargeName = getGiftInfoUID(pGiftItemInfo);
						discountDetail.setReferenceField4(chargeName);
						promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDescription());
						if(StringUtils.isBlank(promoDesc)){
							promoDesc = (String) promotion.getPropertyValue(pricingModelProp.getDisplayName());
						}
						discountDetail.setReferenceField5(promoDesc);
						pListOfDiscDetails.add(discountDetail);
					}
				}
				
			} catch (CommerceItemNotFoundException exc) {
				if (isLoggingError()) {
					vlogError("CommerceItemNotFoundException:  While getting commerce item for gwCommId : {0} Exception", 
							gwCommId,exc);
				}
			} catch (InvalidParameterException exc) {
				if (isLoggingError()) {
					vlogError("InvalidParameterException:  While getting commerce item for gwCommId : {0} Exception", 
							gwCommId,exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateGiftWrapDiscountDetails]");
		}
	}
	
	/**
	 * Method to get the UID.
	 * @param pGiftItemInfo : RepositoryItem - GiftItemInfo Repository Item
	 * @return String : UID - Combination of Product discription, sku id and Product id.
	 */
	public String getGiftInfoUID(RepositoryItem pGiftItemInfo) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: getGiftInfoUID]");
			vlogDebug("pGiftItemInfo : {0}" ,pGiftItemInfo);
		}
		String giftInfoUID = null;
		if(pGiftItemInfo == null){
			return giftInfoUID;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
		String giftWrapCatalogRefId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
		String giftWrapProductId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapProductId());
		if (isLoggingDebug()) {
			vlogDebug("giftWrapCatalogRefId : {0} giftWrapProductId : {1}" ,giftWrapCatalogRefId,giftWrapProductId);
		}
		RepositoryItem prodItem = null;
		if(StringUtils.isNotBlank(giftWrapProductId)){
			StringBuffer giftInfoBuffer= new StringBuffer();
			try {
				prodItem = getCatalogTools().findProduct(giftWrapProductId);
				String prodDesc = null;
				if(prodItem != null){
					prodDesc = (String) prodItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
					if(StringUtils.isNotBlank(prodDesc)){
						giftInfoBuffer.append(prodDesc);
						giftInfoBuffer.append(getOmsConfiguration().getUidPipeDelimeter());
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("prodItem : {0} prodDesc : {1}" ,prodItem,prodDesc);
				}
				giftInfoBuffer.append(giftWrapCatalogRefId);
				giftInfoBuffer.append(getOmsConfiguration().getUidPipeDelimeter());
				giftInfoBuffer.append(giftWrapProductId);
				giftInfoUID=giftInfoBuffer.toString();
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting product item for giftWrapProdID : {0} Exception", 
							giftWrapProductId,exc);
				}
			}
		}else{
			giftInfoUID = giftWrapCatalogRefId;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getGiftInfoUID]");
			vlogDebug("Returning with GiftInfoUID : {0}" ,giftInfoUID);
		}
		return giftInfoUID;
	}

	/**
	 * Method to get the UID. : 
	 * @param pBppSKUID : String BPP SKU ID
	 * @return String : UID - Combination of Product discription, sku id and Product id.
	 */
	public String getBPPInfoUID(String pBppSKUID) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: getBPPInfoUID]");
			vlogDebug("pBppSKUID : {0}" ,pBppSKUID);
		}
		String bppInfoUID = null;
		if(StringUtils.isBlank(pBppSKUID)){
			return bppInfoUID;
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		try {
			RepositoryItem skuItem = catalogTools.findSKU(pBppSKUID);
			RepositoryItem prodItem = catalogTools.getDefaultParentProducts(skuItem);
			TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
			if (isLoggingDebug()) {
				vlogDebug("skuItem : {0} prodItem : {1}" ,skuItem,prodItem);
			}
			String prodDisplayName = null;
			String skuDI = null;
			String proId = null;
			StringBuffer bppInfoUIDBuffer= new StringBuffer();
			if(prodItem != null){
				proId = prodItem.getRepositoryId();
				prodDisplayName = (String) prodItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
				if(StringUtils.isNotBlank(prodDisplayName)){
					bppInfoUIDBuffer.append(prodDisplayName);
					bppInfoUIDBuffer.append(getOmsConfiguration().getUidPipeDelimeter());
				}
			}
			if(skuItem != null){
				skuDI = skuItem.getRepositoryId();
			}
			if(StringUtils.isNotBlank(skuDI)){
				bppInfoUIDBuffer.append(skuDI);
				bppInfoUIDBuffer.append(getOmsConfiguration().getUidPipeDelimeter());
			}
			if(StringUtils.isNotBlank(proId)){
				bppInfoUIDBuffer.append(proId);
			}
			bppInfoUID=bppInfoUIDBuffer.toString();
			if(StringUtils.isNotBlank(bppInfoUID) && bppInfoUID.endsWith(getOmsConfiguration().getUidPipeDelimeter())){
				bppInfoUID = bppInfoUID.substring(TRUConstants.INT_ZERO, bppInfoUID.length()-TRUConstants.INTEGER_NUMBER_ONE);
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException:  While getting sku item for id : {0} Exception", 
						pBppSKUID,exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getBPPInfoUID]");
			vlogDebug("Returning with bppInfoUID : {0}" ,bppInfoUID);
		}
		return bppInfoUID;
	}
	
	/**
	 * Sets the GW id and rel id.
	 *
	 * @param pLShipGroup : ShippingGroup Object
	 */
	@SuppressWarnings("unchecked")
	public void setGWIdAndRelId(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setGWIdAndRelId]");
		}
		Map<String, String> gwItemIdAndRelID = getGWItemIdAndRelID();
		if(pLShipGroup == null){
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		CommerceItem commerceItem = null;
		for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
			commerceItem = rel.getCommerceItem();
			if(commerceItem instanceof TRUGiftWrapCommerceItem){
				gwItemIdAndRelID.put(commerceItem.getId(), rel.getId());
			}
		}
		setGWItemIdAndRelID(gwItemIdAndRelID);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setGWIdAndRelId]");
		}
	}
	
	/**
	 * Method to populate the BPP item tax details to Order Line.
	 *
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pOrder : TRUOrderImpl
	 * @param pListOfTaxDetail : List of TaxDetail Object
	 * @param pGiftWrapRelQty : long
	 * @param pListOfOrderlines : List
	 */
	@SuppressWarnings("rawtypes")
	private void populateBPPItemTaxDetails(
			TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			TRUOrderImpl pOrder, List<TaxDetail> pListOfTaxDetail, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateBPPItemTaxDetails]");
			vlogDebug("pLCommItemRel : {0} pOrder : {1} pListOfTaxDetail : {2}" ,pLCommItemRel,pOrder,pListOfTaxDetail);
		}
		if(pOrder == null || pLCommItemRel == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		RepositoryItem bppItem = (RepositoryItem) pLCommItemRel.getPropertyValue(commercePropertyManager.getBppItemInfoPropertyName());
		if(bppItem == null){
			return;
		}
		String bppSKUID = (String) bppItem.getPropertyValue(commercePropertyManager.getBppSkuIdPropertyName());
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pLCommItemRel.getId()+BPP+TRUConstants._1);
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//String uid = getBPPInfoUID(bppSKUID);
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(taxPriceInfoObject.getStateTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxState());
			proratedTax = prorateStateTax(taxPriceInfoObject.getStateTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCityTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCity());
			proratedTax = prorateCityTax(taxPriceInfoObject.getCityTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCountyTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCounty());
			proratedTax = prorateCountyTax(taxPriceInfoObject.getCountyTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryStateTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryState());
			proratedTax = prorateSecStateTax(taxPriceInfoObject.getSecondaryStateTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCityTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCity());
			proratedTax = prorateSecCityTax(taxPriceInfoObject.getSecondaryCityTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCountyTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(bppSKUID)){
				taxDetail.setChargeName(bppSKUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCounty());
			proratedTax = prorateSecCountyTax(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(BPP_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateBPPItemTaxDetails]");
		}
	}
	
	/**
	 * Method to populate the EWaste fee tax details to Order Line.
	 *
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pOrder : TRUOrderImpl
	 * @param pListOfTaxDetail : List of TaxDetail Object
	 * @param pGiftWrapRelQty : long
	 * @param pListOfOrderlines : List
	 */
	@SuppressWarnings("rawtypes")
	private void populateEWasteTaxDetails(
			TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			TRUOrderImpl pOrder, List<TaxDetail> pListOfTaxDetail, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateEWasteTaxDetails]");
			vlogDebug("pLCommItemRel : {0} pOrder : {1} pListOfTaxDetail : {2}" ,pLCommItemRel,pOrder,pListOfTaxDetail);
		}
		if(pOrder == null || pLCommItemRel == null){
			return;
		}
		CommerceItem commerceItem = pLCommItemRel.getCommerceItem();
		if(commerceItem == null){
			return;
		}
		TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
		if(priceInfo != null && priceInfo.getEwasteFees() > TRUConstants.DOUBLE_ZERO){
			TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
			if(taxPriceInfo == null){
				return;
			}
			Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
			if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
				return;
			}
			TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pLCommItemRel.getId()+EW+TRUConstants._1);
			if(taxPriceInfoObject == null){
				return;
			}
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			String uid = getCatalogTools().getEWasteUID((RepositoryItem)commerceItem.getAuxiliaryData().getProductRef());
			DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
			df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
			double proratedTax = TRUConstants.DOUBLE_ZERO;
			String orderId = pOrder.getId();
			if(taxPriceInfoObject.getStateTax() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxState());
				proratedTax = prorateStateTax(taxPriceInfoObject.getStateTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
			if(taxPriceInfoObject.getCityTax() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCity());
				proratedTax = prorateCityTax(taxPriceInfoObject.getCityTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
			if(taxPriceInfoObject.getCountyTax() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCounty());
				proratedTax = prorateCountyTax(taxPriceInfoObject.getCountyTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
						, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
			if(taxPriceInfoObject.getSecondaryStateTaxAmount() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryState());
				proratedTax = prorateSecStateTax(taxPriceInfoObject.getSecondaryStateTaxAmount(), pLCommItemRel, 
						pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
			if(taxPriceInfoObject.getSecondaryCityTaxAmount() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCity());
				proratedTax = prorateSecCityTax(taxPriceInfoObject.getSecondaryCityTaxAmount(), pLCommItemRel, 
						pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
			if(taxPriceInfoObject.getSecondaryCountyTaxAmount() > TRUConstants.DOUBLE_ZERO){
				TaxDetail taxDetail = new TaxDetail();
				taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
				taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
				taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryE911());
				if(StringUtils.isNotBlank(uid)){
					taxDetail.setChargeName(uid);
				}
				taxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
				taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCounty());
				proratedTax = prorateSecCountyTax(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pLCommItemRel, 
						pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);
				taxDetail.setTaxAmount(proratedTax);
				if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
					taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
				}
				taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
				taxDetail.setItemType(EWASTE_ITEM);
				pListOfTaxDetail.add(taxDetail);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateEWasteTaxDetails]");
		}
	}
	
	/**
	 * Method to populate the Gift Item tax details to Order Line.
	 *
	 * @param pOrder : TRUOrderImpl
	 * @param pGiftItemInfo : RepositoryItem
	 * @param pListOfTaxDetail : List of TaxDetail Object
	 */
	@SuppressWarnings("rawtypes")
	private void populateGiftItemTaxDetails(TRUOrderImpl pOrder,
			RepositoryItem pGiftItemInfo, List<TaxDetail> pListOfTaxDetail) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateGiftItemTaxDetails]");
			vlogDebug("pGiftItemInfo : {0} pOrder : {1} pListOfTaxDetail : {2}" ,pGiftItemInfo,pOrder,pListOfTaxDetail);
		}
		if(pOrder == null || pGiftItemInfo == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		String commerceItemId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
		String giftInfoUID = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCatalogRefId());
		String relId = null;
		if(getGWItemIdAndRelID() != null){
			relId = getGWItemIdAndRelID().get(commerceItemId);
		}
		TRUTaxPriceInfo taxPriceInfoObject = null;
		if(StringUtils.isNotBlank(relId)){
			taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(relId);
		}
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//String giftInfoUID = getGiftInfoUID(pGiftItemInfo);
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		if(taxPriceInfoObject.getStateTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxState());
			taxDetail.setTaxAmount(taxPriceInfoObject.getStateTax());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCityTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCity());
			taxDetail.setTaxAmount(taxPriceInfoObject.getCityTax());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCountyTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCounty());
			taxDetail.setTaxAmount(taxPriceInfoObject.getCountyTax());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryStateTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryState());
			taxDetail.setTaxAmount(taxPriceInfoObject.getSecondaryStateTaxAmount());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCityTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCity());
			taxDetail.setTaxAmount(taxPriceInfoObject.getSecondaryCityTaxAmount());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCountyTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setChargeCategory(omsConfiguration.getChargeCategoryVAS());
			if(StringUtils.isNotBlank(giftInfoUID)){
				taxDetail.setChargeName(giftInfoUID);
			}
			taxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCounty());
			taxDetail.setTaxAmount(taxPriceInfoObject.getSecondaryCountyTaxAmount());
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			pListOfTaxDetail.add(taxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateGiftItemTaxDetails]");
		}
	}
	
	/**
	 * Method to populate the Normal Commerce Items tax details to Order Line.
	 *
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pOrder : TRUOrderImpl
	 * @param pListOfTaxDetail : List of TaxDetail Object
	 * @param pGiftWrapRelQty : long
	 * @param pListOfOrderlines : List
	 */
	@SuppressWarnings("rawtypes")
	private void populateNormalItemTaxDetails(
			TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			TRUOrderImpl pOrder, List<TaxDetail> pListOfTaxDetail, long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: populateNormalItemTaxDetails]");
			vlogDebug("pLCommItemRel : {0} pOrder : {1} pListOfTaxDetail : {2}" ,pLCommItemRel,pOrder,pListOfTaxDetail);
		}
		if(pOrder == null || pLCommItemRel == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return;
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return;
		}
		TRUTaxPriceInfo taxPriceInfoObject = (TRUTaxPriceInfo) shipItemRelationsTaxPriceInfos.get(pLCommItemRel.getId());
		if(taxPriceInfoObject == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
		String orderId = pOrder.getId();
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(taxPriceInfoObject.getStateTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxState());
			proratedTax = prorateStateTax(taxPriceInfoObject.getStateTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCityTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCity());
			proratedTax = prorateCityTax(taxPriceInfoObject.getCityTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getCountyTax() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxCounty());
			proratedTax = prorateCountyTax(taxPriceInfoObject.getCountyTax(), pLCommItemRel, pGiftWrapRelQty, pListOfOrderlines
					, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryStateTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxSecStateLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryState());
			proratedTax = prorateSecStateTax(taxPriceInfoObject.getSecondaryStateTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCityTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxSecCityLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCity());
			proratedTax = prorateSecCityTax(taxPriceInfoObject.getSecondaryCityTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if(taxPriceInfoObject.getSecondaryCountyTaxAmount() > TRUConstants.DOUBLE_ZERO){
			TaxDetail taxDetail = new TaxDetail();
			taxDetail.setRequestType(omsConfiguration.getTaxRequestType());
			taxDetail.setTaxCategory(omsConfiguration.getTaxCategory());
			taxDetail.setTaxName(omsConfiguration.getTaxSecCountyLocationName());
			taxDetail.setTaxLocationName(taxPriceInfoObject.getTaxSecondoryCounty());
			proratedTax = prorateSecCountyTax(taxPriceInfoObject.getSecondaryCountyTaxAmount(), pLCommItemRel, 
					pGiftWrapRelQty, pListOfOrderlines, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
			taxDetail.setTaxAmount(proratedTax);
			if(taxPriceInfoObject.getTaxRequestTimestamp() != null){
				taxDetail.setTaxDTTM(df.format(taxPriceInfoObject.getTaxRequestTimestamp()));
			}
			taxDetail.setExtTaxDetailId(getExtTaxDetailId(orderId));
			taxDetail.setItemType(NORMAL_ITEM);
			pListOfTaxDetail.add(taxDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: populateNormalItemTaxDetails]");
		}
	}
	
	/**
	 * @return String - Shipping Charge name combination of Description|SKUID|ProdId.
	 */
	public String getShippingFeeUIDWithDes() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: getShippingFeeUIDWithDes]");
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
		RepositoryItem shippingFeeSKUItem = catalogTools.getDonationOrBPPSKUItem(getOmsConfiguration().getShippingFeeSKUType());
		RepositoryItem shippingFeeProdItem = null;
		String prodDisplayName = null;
		String skuDI = null;
		String proId = null;
		StringBuffer shippingUID = new StringBuffer();
		if(shippingFeeSKUItem != null){
			shippingFeeProdItem = catalogTools.getDefaultParentProducts(shippingFeeSKUItem);
		}
		if(shippingFeeProdItem != null){
			proId = shippingFeeProdItem.getRepositoryId();
			prodDisplayName = (String) shippingFeeProdItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
			if(StringUtils.isNotBlank(prodDisplayName)){
				shippingUID.append(prodDisplayName+getOmsConfiguration().getUidPipeDelimeter());
			}
		}
		if(shippingFeeSKUItem != null){
			skuDI = shippingFeeSKUItem.getRepositoryId();
		}
		if(StringUtils.isNotBlank(skuDI)){
			shippingUID.append(skuDI+getOmsConfiguration().getUidPipeDelimeter());
		}
		if(StringUtils.isNotBlank(proId)){
			shippingUID.append(proId);
		}
		if(shippingUID != null && shippingUID.length() > TRUConstants.ZERO
				&& TRUOMSConstant.CHAR_PIPE_DELIMETER == shippingUID.charAt(shippingUID.length()-TRUConstants.ONE)){
			shippingUID.deleteCharAt(shippingUID.length()-TRUConstants.ONE);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getShippingFeeUIDWithDes]");
			vlogDebug("Returning with ShippingFeeUIDWithDes : {0}" ,shippingUID.toString());
		}
		return shippingUID.toString();
	}
	
	/**
	 * @return String - Shipping Sub Type nonMerchSKU id.
	 */
	public String getShippingFeeUID() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: getShippingFeeUID]");
		}
		String shippingFeeUID = null;
		TRUCatalogTools catalogTools = getCatalogTools();
		RepositoryItem shippingFeeSKUItem = catalogTools.getDonationOrBPPSKUItem(getOmsConfiguration().getShippingFeeSKUType());
		if (isLoggingDebug()) {
			vlogDebug("shippingFeeSKUItem : {0}" ,shippingFeeSKUItem);
		}
		if(shippingFeeSKUItem != null){
			shippingFeeUID = shippingFeeSKUItem.getRepositoryId();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: getShippingFeeUID]");
			vlogDebug("Returning with shippingFeeUID : {0}" ,shippingFeeUID);
		}
		return shippingFeeUID;
	}
	
	/**
	 * Method to get the EWaste note text in the combination of Desc|UID|ItemId
	 * @param pProductRef : RepositoryItem Object
	 * @return String - Return ewaste sku id.
	 */
	public String getEWasteNoteText(RepositoryItem pProductRef){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCatalogTools  method: getEWasteNoteText]");
			vlogDebug("Product Item : {0} pProductRef",pProductRef);
		}
		String ewasteUID = null;
		String displayName = null;
		StringBuffer noteText = new StringBuffer();
		if(pProductRef == null){
			return ewasteUID;
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) catalogTools.getCatalogProperties();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		String eWasteSurchargeProdId = (String) pProductRef.getPropertyValue(catalogProperties.getEwasteSurchargeSkuPropertyName());
		if(StringUtils.isBlank(eWasteSurchargeProdId)){
			vlogDebug("EWate Product Id is NUll, Returing with ewasteUID : {0}",ewasteUID);
			return ewasteUID;
		}
		RepositoryItem eWasteSKUItem = null;
		RepositoryItem ewasteProdItem = null;
		try {
			ewasteProdItem = catalogTools.findProduct(eWasteSurchargeProdId);
			if(ewasteProdItem != null){
				eWasteSKUItem = catalogTools.getDefaultChildSku(ewasteProdItem);
				displayName=(String) eWasteSKUItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
			}
			if(eWasteSKUItem != null){
				ewasteUID = eWasteSKUItem.getRepositoryId();
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException:  While getting sku or product item : {0}", exc);
			}
		}
		noteText.append(displayName).append(configuration.getUidPipeDelimeter()).append(ewasteUID).append(configuration.getUidPipeDelimeter()).append(eWasteSurchargeProdId);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCatalogTools  method: getEWasteNoteText]");
			vlogDebug("EWaste UID noteText  : {0}",noteText);
		}
		return noteText.toString();
	}
	
	/**
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship 
	 * @return noteText - UID|ItemId
	 */
	public String getShippingSurchargeNoteText(TRUShippingGroupCommerceItemRelationship pShipItemRel){
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCatalogTools  method: getShippingSurchargeNoteText]");
			vlogDebug("TRUShippingGroupCommerceItemRelationship  : {0}",pShipItemRel);
		}
		StringBuffer noteText = new StringBuffer();
		String catalogRefId = null;
		String displayName = null;
		String prodId = null;
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) catalogTools.getCatalogProperties();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		if(pShipItemRel.getCommerceItem() != null){
			CommerceItem commerceItem = pShipItemRel.getCommerceItem();
			catalogRefId = commerceItem.getCatalogRefId();
			RepositoryItem skuItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
			displayName = (String) skuItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
			prodId = commerceItem.getAuxiliaryData().getProductId();
		}
		noteText.append(displayName).append(configuration.getUidPipeDelimeter()).append(catalogRefId).append(configuration.getUidPipeDelimeter()).append(prodId);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCatalogTools  method: getShippingSurchargeNoteText]");
			vlogDebug("Shipping surcharge UID noteText  : {0}",noteText);
		}
		return noteText.toString();
	}
}