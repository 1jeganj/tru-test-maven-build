package com.tru.integration.oms;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 * This class is to create audit item with JSON request and response.
 * @author PA.
 * @version 1.0.
 */
public class TRUOMSAuditTools extends GenericService {
	
	/** Holds Response_Details */
	public static final String RESPONSE_DETAILS = "Response_Details";

	/** Holds Resp_Code */
	public static final String RESP_CODE = "Resp_Code";

	/** Holds 25 */
	public static final String STRING_TWENTY_FIVE = "25";

	/**
	 * Holds reference for MessageContentRepository. 
	 */
	private GSARepository mAuditRepository;
	
	/**
	 * Holds reference for TRUOMSProperties. 
	 */
	private TRUOMSConfiguration mOmsConfiguration;
	
	/**
	 * this method will create the transaction details audit item.
	 * @param pTransactionId - String
	 * @param pRequest - String
	 * @param pResponse - String
	 * @param pType - String
	 * @param pStatus - String
	 * @param pTransactionTime - Date Object
	 */
	public void createOMSAuditItem(String pTransactionId, String pRequest, String pResponse, String pType, String pStatus, Date pTransactionTime){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: createOMSAuditItem]");
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		//Getting response status
		String responseStatus = getOrderRepsonseStausForAudit(pResponse, pType);
		MutableRepository auditRepository = getAuditRepository();
		try {
			Date time = Calendar.getInstance().getTime();
			MutableRepositoryItem transDetailsItem = auditRepository.createItem(omsConfiguration.getOmsTransRecordsItemDescriptorName());
			transDetailsItem.setPropertyValue(omsConfiguration.getTransactionIdPopertyName(), pTransactionId);
			transDetailsItem.setPropertyValue(omsConfiguration.getRequestPopertyName(), pRequest);
			transDetailsItem.setPropertyValue(omsConfiguration.getResponsePopertyName(), pResponse);
			transDetailsItem.setPropertyValue(omsConfiguration.getTypePopertyName(), pType);
			transDetailsItem.setPropertyValue(omsConfiguration.getStatusPopertyName(), responseStatus);
			transDetailsItem.setPropertyValue(omsConfiguration.getTransactionTimePopertyName(), time);
			auditRepository.addItem(transDetailsItem);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError(exc, "RepositoryException in method createOMSAuditItem");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: createOMSAuditItem]");
		}
	}
	
	
	/**
	 * This method is to get the repose status whether it is success or error.
	 * @param pResponse - JSON Response
	 * @param pType - type
	 * @return severity - severity 
	 */
	public String getOrderRepsonseStausForAudit(String pResponse, String pType){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: getOrderRepsonseStausForAudit]");
		}
		String severity = null;
		JsonObject messages = null;
		if(pResponse != null){
			TRUOMSConfiguration configuration = getOmsConfiguration();
			JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(pResponse);
			JsonObject mainObj = parse.getAsJsonObject();
			if(pType.equals(TRUOMSConstant.CUSTOMER_MASTER_IMPORT)){
				JsonObject customerDetails = mainObj.getAsJsonObject(configuration.getCustomerFullDetailsJsonElement());
				messages = customerDetails.getAsJsonObject(configuration.getOrderResponseMessagesJsonElement());
			} else{
				JsonObject customerOrder = mainObj.getAsJsonObject(configuration.getOrderDetailsCustomerOrderJsonElement());
				messages = customerOrder.getAsJsonObject(configuration.getOrderResponseMessagesJsonElement());
			}
			if(messages == null){
				severity = TRUOMSConstant.SUCCESS;
			} else{
				severity = TRUOMSConstant.ERROR;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: getOrderRepsonseStausForAudit]");
		}
		return severity;
	}

	/**
	 * @return the auditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}

	/**
	 * @param pAuditRepository the auditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		mAuditRepository = pAuditRepository;
	}

	/**
	 * This method returns the omsConfiguration value
	 *
	 * @return the omsConfiguration
	 */
	public TRUOMSConfiguration getOmsConfiguration() {
		return mOmsConfiguration;
	}

	/**
	 * This method sets the omsConfiguration with parameter value pOmsConfiguration
	 *
	 * @param pOmsConfiguration the omsConfiguration to set
	 */
	public void setOmsConfiguration(TRUOMSConfiguration pOmsConfiguration) {
		mOmsConfiguration = pOmsConfiguration;
	}
	
	/**
	 * Method to get the Customer or Order Id from JSON request.
	 * 
	 * @param pJSONRequest : String - JSON request
	 * @param pType : String : Type of Service
	 * @return - String : Order or Customer Id from JSON request.
	 */
	public String getOrderOrCustomerId(String pJSONRequest, String pType){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: getOrderOrCustomerId]");
			vlogDebug("Service Type : {0}",pType);
		}
		String orderOrCustomerId = null;
		JsonPrimitive jsonPrimitive = null;
		if(pJSONRequest != null){
			TRUOMSConfiguration configuration = getOmsConfiguration();
			JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(pJSONRequest);
			JsonObject mainObj = parse.getAsJsonObject();
			if(pType.equals(TRUOMSConstant.CUSTOMER_MASTER_IMPORT)){
				JsonObject customerDetails = mainObj.getAsJsonObject(configuration.getCustomerFullDetailsJsonElement());
				jsonPrimitive = customerDetails.getAsJsonPrimitive(configuration.getExtCustomerIdJSONPrimitive());
			} else if(pType.equals(TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE)){
				JsonObject customerOrder = mainObj.getAsJsonObject(configuration.getOrderDetailsCustomerOrderJsonElement());
				jsonPrimitive = customerOrder.getAsJsonPrimitive(configuration.getOrderNumberJSONPrimitive());
			}else{
				JsonObject customerOrder1 = mainObj.getAsJsonObject(configuration.getOrderDetailsCustomerOrderJsonElement());
				jsonPrimitive = customerOrder1.getAsJsonPrimitive(configuration.getExtOrderNumberJSONPrimitive());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: getOrderOrCustomerId]");
			vlogDebug("jsonPrimitive : {0}",jsonPrimitive);
		}
		if(jsonPrimitive != null){
			orderOrCustomerId = jsonPrimitive.toString();
		}
		return orderOrCustomerId;
	}

	/**
	 * This method will get the oms audit messages of old configured in days and
	 * deletes the same
	 * 
	 * @param pDurationToDeleteMessagesInDays
	 *            - Duration in days
	 * @throws RepositoryException - if any
	 */
	public void purgeOmsAuditMessages(int pDurationToDeleteMessagesInDays) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: purgeOmsAuditMessages]");
			vlogDebug("pDurationToDeleteMessagesInDays : {0}",pDurationToDeleteMessagesInDays);
		}
		MutableRepository auditRepository = getAuditRepository();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		RepositoryItem[] auditMessages = getAuditMessages(pDurationToDeleteMessagesInDays);
		if(auditMessages != null && auditMessages.length > TRUConstants.INT_ZERO){
			for(RepositoryItem auditMessage : auditMessages){
				auditRepository.removeItem(auditMessage.getRepositoryId(), configuration.getOmsTransRecordsItemDescriptorName());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: purgeOmsAuditMessages]");
		}
	}

	/**
	 * This method returns old audit items based on the configured days.
	 * 
	 * @param pDurationToDeleteMessagesInDays
	 *            - Duration in days
	 * @return auditMessageItems - Repository items
	 */
	private RepositoryItem[] getAuditMessages(int pDurationToDeleteMessagesInDays) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: getAuditMessages]");
			vlogDebug("pDurationToDeleteMessagesInDays : {0}", pDurationToDeleteMessagesInDays);
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		MutableRepository auditRepository = getAuditRepository();
		RepositoryItem[] auditMessageItems= null;
		try {
			RepositoryView autMessageView = auditRepository.getView(configuration.getOmsTransRecordsItemDescriptorName());
			QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
			//Get Old date
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, - pDurationToDeleteMessagesInDays);
			Date oldDate = cal.getTime();
			QueryExpression transactionTimePropertyExp=queryBuilder.createPropertyQueryExpression(configuration.getTransactionTimePopertyName());
            QueryExpression oldDateConstantExp=queryBuilder.createConstantQueryExpression(oldDate);
            
            Query transactionTimeQuery=queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.LESS_THAN);
            
            auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: purgeOmsAuditMessages]");
		}
		return auditMessageItems;
	}
	
	/**
	 * This method is to update the audit message item based on the queue response.
	 * 
	 * @param pCorrelationID - correlationID
	 * @param pXmlResponseMessage - xmlResponseMessage
	 */
	public void updateAuditMessageItem(String pCorrelationID, Serializable pXmlResponseMessage){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: updateAuditMessageItem]");
			vlogDebug("pCorrelationID : {0} pXmlResponseMessage : {1}", pCorrelationID, pXmlResponseMessage);
		}
		if(pCorrelationID == null || pXmlResponseMessage == null){
			if (isLoggingDebug()) {
				logDebug("Correlation Id or reponse message is null so no item is updatedd.");
			}
			return;
		}
		RepositoryItem[] auditMessageItems= null;
		MutableRepositoryItem auditItem = null;
		String responseStatus = parseResponseXML(pXmlResponseMessage.toString());
		MutableRepository auditRepository = getAuditRepository();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		try{
			RepositoryView autMessageView = auditRepository.getView(configuration.getOmsTransRecordsItemDescriptorName());
			QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
			QueryExpression transactionTimePropertyExp=queryBuilder.createPropertyQueryExpression(configuration.getTransactionIdPopertyName());
			QueryExpression oldDateConstantExp=queryBuilder.createConstantQueryExpression(pCorrelationID);
			Query transactionTimeQuery=queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.EQUALS);
			auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
			if(auditMessageItems != null && auditMessageItems.length > TRUConstants.INT_ZERO){
				auditItem = (MutableRepositoryItem) auditMessageItems[0];
			}
			if(isLoggingDebug()){
				vlogDebug("Audit message Item with trans Id : {0} is : {1}", pCorrelationID, auditItem);
			}
			if(auditItem != null && pXmlResponseMessage != null){
				auditItem.setPropertyValue(configuration.getResponsePopertyName(), pXmlResponseMessage.toString());
				auditItem.setPropertyValue(configuration.getStatusPopertyName(), responseStatus);
				auditRepository.updateItem(auditItem);
			}
		}catch(RepositoryException repExc){
			if(isLoggingError()){
				vlogError("RepositoryException : While getting audit message item with trans id : {0} and exception is : {1}", pCorrelationID, repExc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: updateAuditMessageItem]");
		}
	}
	
	/**
	 * This method is to parse the response and return the status based on the
	 * response code.
	 * 
	 * @param pResponse
	 *            - CO response
	 * @return responseStatus - Success/Error
	 */
	private String parseResponseXML(String pResponse) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: parseResponseXML]");
		}
		String responseStatus = null;
		if(pResponse != null){
			try {
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(pResponse));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName(RESPONSE_DETAILS);
				int nodesLen = nodes.getLength();
				for (int i = 0; i < nodesLen; i++) {
					Element element = (Element) nodes.item(i);
					NodeList name = element.getElementsByTagName(RESP_CODE);
					Element line = (Element) name.item(0);
					if(line != null){
						String responseCode = getCharacterDataFromElement(line);
						if(!StringUtils.isEmpty(responseCode) && responseCode != null && responseCode.equals(STRING_TWENTY_FIVE)){
							responseStatus = TRUOMSConstant.SUCCESS;
						}else{
							responseStatus = TRUOMSConstant.ERROR;
						}
					}
				}
			} catch (SAXException exc) {
				if(isLoggingError()){
					vlogError("SAXException : while parsing xml and with exception : {0}", exc); 
				}
			} catch (ParserConfigurationException exc) {
				if(isLoggingError()){
					vlogError("ParserConfigurationException : while getting document instance with exception : {0}", exc); 
				}
			} catch (IOException exc) {
				if(isLoggingError()){
					vlogError("IOException : while parsing input stream and  with exception : {0}", exc); 
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: parseResponseXML]");
		}
		return responseStatus;
	}
	
	/**
	 * This method returns the response code.
	 * @param pEle - Element
	 * @return responseCode - reponseCode
	 */
	public String getCharacterDataFromElement(Element pEle) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSAuditTools  method: getCharacterDataFromElement]");
		}
		Node child = pEle.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSAuditTools  method: getCharacterDataFromElement]");
		}
		return TRUOMSConstant.EMPTY_STRING;
	}
}
