package com.tru.integration.oms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.payment.PaymentStatus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oms.custorder.BillToDetail;
import com.oms.custorder.CustomerInfo;
import com.oms.custorder.CustomerOrder;
import com.oms.custorder.CustomerOrderImport;
import com.oms.custorder.OrderLine;
import com.oms.custorder.OrderLines;
import com.oms.custorder.PaymentDetail;
import com.oms.custorder.PaymentDetails;
import com.oms.custorder.PaymentTransaction;
import com.oms.custorder.PaymentTransactionDetails;
import com.oms.custorder.PriceDetails;
import com.oms.custorder.ReferenceFields;
import com.oms.custorder.ShippingAddress;
import com.oms.custorder.ShippingInfo;
import com.oms.custorder.TaxDetail;
import com.oms.orderlist.CustomerOrderListRequest;
import com.oms.orderlist.CustomerOrderSearchCriteria;
import com.oms.orderlist.SortingCriterion;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.payment.TRUPaymentStatus;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.order.payment.giftcard.TRUGiftCardStatus;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.payment.paypal.TRUPayPalStatus;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.messaging.oms.TRUCustomerOrderUpdateMessage;
import com.tru.userprofiling.TRUPropertyManager;
import com.tru.xml.customerorder.ObjectFactory;
import com.tru.xml.customerorder.TXML;
import com.tru.xml.customerorder.TXML.Header;
import com.tru.xml.customerorder.TXML.Message;


/**
 * This is utility class for OMS operations.
 * @author PA
 * @version 1.1
 */
public class TRUBaseOMSUtils extends GenericService {
	
	/** The Constant XXXXXXXXXXXX. */
	public static final String XXXXXXXXXXXX = "XXXXXXXXXXXX";

	/** The Constant SHIP_TO_ADDRESS. */
	public static final String SHIP_TO_ADDRESS = "Ship to address";

	/** The Constant DOLLAR_SYMBOL. */
	public static final String DOLLAR_SYMBOL = "$";
	
	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT="MM/dd/yyyy hh:mm";
	
	/** The Constant EST_DATE_FORMAT. */
	public static final String EST_DATE_FORMAT="MM/dd/yyyy hh:mm z";
	
	/** The Constant REFERENCE_DATE_FORMAT. */
	public static final String REFERENCE_DATE_FORMAT="MMddYYYY";
	
	/** The Constant DATE_FORMAT1. */
	public static final String DATE_FORMAT1="MM/dd/yyyy";
	
	/** The Constant ORDER_TYPE_WEB_KEY. */
	public static final String ORDER_TYPE_WEB_KEY="web";

	/** The Constant ORDER_TYPE_PAYINSTORE_KEY. */
	public static final String ORDER_TYPE_PAYINSTORE_KEY="payInStote";
	
	/** The Constant OMS_AUTH_HEADER. */
	public static final String OMS_AUTH_HEADER="Basic ZG9tYWRtaW46cGFzc3dvcmQ=";
	
	/** The Constant ORDER_TYPE_LAYAWAY_KEY. */
	public static final String ORDER_TYPE_LAYAWAY_KEY="layway";
	
	/** The Constant NORMAL_ITEM. */
	public static final String NORMAL_ITEM="Normal";
	
	/** The Constant BPP_ITEM. */
	public static final String BPP_ITEM="BPPItem";
	
	/** The Constant EWASTE_ITEM. */
	public static final String EWASTE_ITEM="EWaste";
	
	/** The Constant SHIPPING_ITEM. */
	public static final String SHIPPING_ITEM="Shipping";
	
	/** The Constant SHIPPING_SURCHARGE_ITEM. */
	public static final String SHIPPING_SURCHARGE_ITEM="Shipping Surcharge";
	
	/** The Constant UPDATE_SERVICE. */
	public static final String UPDATE_SERVICE="UPDATE_SERVICE";
	
	/** The Constant COI_SERVICE. */
	public static final String COI_SERVICE="COI_SERVICE";
	
	/** The Constant FOUR_DECIMAL_FORMAT. */
	public static final String FOUR_DECIMAL_FORMAT="%.4f";
	
	/** The Constant SIX_DECIMAL_FORMAT. */
	public static final String SIX_DECIMAL_FORMAT="%.6f";
	/**
	 * String FALSE.
	 */
	public static final String FALSE = "False";
	/**
	 * String TRUE.
	 */
	public static final String TRUE = "True";
	
	/**
	 * Holds reference for TRUOMSProperties.
	 */
	private TRUOMSConfiguration mOmsConfiguration;
	/**
	 * Holds reference for mOrderManager.
	 */
	private TRUOrderManager mOrderManager;
	/**
	 * Holds reference for mPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	/**
	 * Holds reference for mLastRelationShip.
	 */
	private boolean mLastRelationShip;
	/**
	 * Holds reference for mCommItemRelAmount.
	 */
	private double mCommItemRelAmount;
	/**
	 * Holds reference for mCreditCardPG.
	 */
	private boolean mCreditCardPG;
	/**
	 * Holds reference for mPaypalPG.
	 */
	private boolean mPaypalPG;
	
	/** The Catalog tools. */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * This property hold reference for mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	/** The Site context manager. */
	private SiteContextManager mSiteContextManager;
	
	/** Property to hold mCatalogRepository. */
	private Repository mCatalogRepository;
	
	/** The m oms error tools. */
	private TRUOMSErrorTools mOmsErrorTools;
	
	/** The m tru configuration. */
	private TRUConfiguration mTruConfiguration;
	
	/** The m tax counter. */
	private int mTaxCounter;
	
	/** The m first name. */
	private String mFirstName;
	
	/** The m last name. */
	private String mLastName;
	
	/** The m phone number. */
	private String mPhoneNumber;
	/** Property to hold mShippingManager. */
	private TRUShippingManager mShippingManager;
	
	/**
	 * Checks if is last relation ship.
	 *
	 * @return the lastRelationShip
	 */
	public boolean isLastRelationShip() {
		return mLastRelationShip;
	}

	/**
	 * Sets the last relation ship.
	 *
	 * @param pLastRelationShip the lastRelationShip to set
	 */
	public void setLastRelationShip(boolean pLastRelationShip) {
		mLastRelationShip = pLastRelationShip;
	}

	/**
	 * Gets the comm item rel amount.
	 *
	 * @return the commItemRelAmount
	 */
	public double getCommItemRelAmount() {
		return mCommItemRelAmount;
	}

	/**
	 * Sets the comm item rel amount.
	 *
	 * @param pCommItemRelAmount the commItemRelAmount to set
	 */
	public void setCommItemRelAmount(double pCommItemRelAmount) {
		mCommItemRelAmount = pCommItemRelAmount;
	}
	/**
	 * Holds reference for mPropertyManager.
	 */
	public Map<CommerceItem, Long> mCommItemQtyMap = null;

	/**
	 * Gets the comm item qty map.
	 *
	 * @return the commItemRelQtyMap
	 */
	public Map<CommerceItem, Long> getCommItemQtyMap() {
		if(mCommItemQtyMap == null){
			mCommItemQtyMap = new HashMap<CommerceItem, Long>();
		}
		return mCommItemQtyMap;
	}

	/**
	 * Sets the comm item qty map.
	 *
	 * @param pCommItemQtyMap the commItemQtyMap to set
	 */
	public void setCommItemQtyMap(Map<CommerceItem, Long> pCommItemQtyMap) {
		mCommItemQtyMap = pCommItemQtyMap;
	}
	
	/**
	 * Holds reference for mCommerceItemRelMapForOrderDis.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForOrderDis = null;
	
	/**
	 * Gets the commerce item rel map for order dis.
	 *
	 * @return the commerceItemRelMapForOrderDis
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForOrderDis() {
		
		if(mCommerceItemRelMapForOrderDis == null){
			mCommerceItemRelMapForOrderDis = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForOrderDis;
	}

	/**
	 * Sets the commerce item rel map for order dis.
	 *
	 * @param pCommerceItemRelMapForOrderDis the commerceItemRelMapForOrderDis to set
	 */
	public void setCommerceItemRelMapForOrderDis(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForOrderDis) {
		mCommerceItemRelMapForOrderDis = pCommerceItemRelMapForOrderDis;
	}
	
	
	/**
	 * Holds reference for mCommerceItemRelMapForShipDis.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForShipDis = null;
	
	/**
	 * Gets the commerce item rel map for ship dis.
	 *
	 * @return the commerceItemRelMapForShipDis
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForShipDis() {
		if(mCommerceItemRelMapForShipDis == null){
			mCommerceItemRelMapForShipDis = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForShipDis;
	}

	/**
	 * Sets the commerce item rel map for ship dis.
	 *
	 * @param pCommerceItemRelMapForShipDis the commerceItemRelMapForShipDis to set
	 */
	public void setCommerceItemRelMapForShipDis(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForShipDis) {
		mCommerceItemRelMapForShipDis = pCommerceItemRelMapForShipDis;
	}
	
	/**
	 * Holds reference for mCommerceItemRelMapForSurcharge.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForSurcharge = null;
	
	/**
	 * Holds reference for mCommerceItemRelMapForEWasteFee.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForEWasteFee = null;
	
	/**
	 * Holds reference for mCommerceItemRelMapForShipFee.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForShipFee = null;
	/**
	 * Holds reference for mCommerceItemRelMapForShipFee.
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> mCommerceItemRelMapForBPP = null;
	
	/**
	 * Gets the commerce item rel map for surcharge.
	 *
	 * @return the commerceItemRelMapForSurcharge
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForSurcharge() {
		if(mCommerceItemRelMapForSurcharge == null){
			mCommerceItemRelMapForSurcharge = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForSurcharge;
	}

	/**
	 * Sets the commerce item rel map for surcharge.
	 *
	 * @param pCommerceItemRelMapForSurcharge the commerceItemRelMapForSurcharge to set
	 */
	public void setCommerceItemRelMapForSurcharge(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForSurcharge) {
		mCommerceItemRelMapForSurcharge = pCommerceItemRelMapForSurcharge;
	}

	/**
	 * Gets the commerce item rel map for e waste fee.
	 *
	 * @return the commerceItemRelMapForEWasteFee
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForEWasteFee() {
		if(mCommerceItemRelMapForEWasteFee == null){
			mCommerceItemRelMapForEWasteFee = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForEWasteFee;
	}

	/**
	 * Sets the commerce item rel map for e waste fee.
	 *
	 * @param pCommerceItemRelMapForEWasteFee the commerceItemRelMapForEWasteFee to set
	 */
	public void setCommerceItemRelMapForEWasteFee(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForEWasteFee) {
		mCommerceItemRelMapForEWasteFee = pCommerceItemRelMapForEWasteFee;
	}

	/**
	 * Gets the commerce item rel map for ship fee.
	 *
	 * @return the commerceItemRelMapForShipFee
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForShipFee() {
		if(mCommerceItemRelMapForShipFee == null){
			mCommerceItemRelMapForShipFee = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForShipFee;
	}

	/**
	 * Sets the commerce item rel map for ship fee.
	 *
	 * @param pCommerceItemRelMapForShipFee the commerceItemRelMapForShipFee to set
	 */
	public void setCommerceItemRelMapForShipFee(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForShipFee) {
		mCommerceItemRelMapForShipFee = pCommerceItemRelMapForShipFee;
	}
	
	/**
	 * Gets the commerce item rel map for bpp.
	 *
	 * @return the commerceItemRelMapForBPP
	 */
	public Map<TRUShippingGroupCommerceItemRelationship, Long> getCommerceItemRelMapForBPP() {
		if(mCommerceItemRelMapForBPP == null){
			mCommerceItemRelMapForBPP = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
		}
		return mCommerceItemRelMapForBPP;
	}

	/**
	 * Sets the commerce item rel map for bpp.
	 *
	 * @param pCommerceItemRelMapForBPP the commerceItemRelMapForBPP to set
	 */
	public void setCommerceItemRelMapForBPP(
			Map<TRUShippingGroupCommerceItemRelationship, Long> pCommerceItemRelMapForBPP) {
		mCommerceItemRelMapForBPP = pCommerceItemRelMapForBPP;
	}
	
	/**
	 * Checks if is credit card pg.
	 *
	 * @return the creditCardPG
	 */
	public boolean isCreditCardPG() {
		return mCreditCardPG;
	}

	/**
	 * Sets the credit card pg.
	 *
	 * @param pCreditCardPG the creditCardPG to set
	 */
	public void setCreditCardPG(boolean pCreditCardPG) {
		mCreditCardPG = pCreditCardPG;
	}

	/**
	 * Checks if is paypal pg.
	 *
	 * @return the paypalPG
	 */
	public boolean isPaypalPG() {
		return mPaypalPG;
	}

	/**
	 * Sets the paypal pg.
	 *
	 * @param pPaypalPG the paypalPG to set
	 */
	public void setPaypalPG(boolean pPaypalPG) {
		mPaypalPG = pPaypalPG;
	}
	
	/**
	 * Gets the catalog tools.
	 *
	 * @return the catalog tools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}


	/**
	 * Sets the catalog tools.
	 *
	 * @param pCatalogTools the new catalog tools
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	
	/**
	 * Gets the product page util.
	 *
	 * @return the productPageUtil
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * Sets the product page util.
	 *
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}
	
	/**
	 * Gets the site context manager.
	 *
	 * @return the mSiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * Sets the site context manager.
	 *
	 * @param pSiteContextManager            the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		this.mSiteContextManager = pSiteContextManager;
	}
	
	/**
	 * Gets the catalog repository.
	 *
	 * @return the catalogRepository
	 */
	public Repository getCatalogRepository() {
		return mCatalogRepository;
	}

	/**
	 * Sets the catalog repository.
	 *
	 * @param pCatalogRepository the catalogRepository to set
	 */
	public void setCatalogRepository(Repository pCatalogRepository) {
		mCatalogRepository = pCatalogRepository;
	}
	
	/**
	 * This method returns the omsConfiguration value.
	 *
	 * @return the omsConfiguration
	 */
	public TRUOMSConfiguration getOmsConfiguration() {
		return mOmsConfiguration;
	}

	/**
	 * This method sets the omsConfiguration with parameter value pOmsConfiguration.
	 *
	 * @param pOmsConfiguration the omsConfiguration to set
	 */
	public void setOmsConfiguration(TRUOMSConfiguration pOmsConfiguration) {
		mOmsConfiguration = pOmsConfiguration;
	}
	
	/**
	 * Gets the order manager.
	 *
	 * @return the orderManager
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}
	
	/**
	 * Sets the order manager.
	 *
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}
	
	/**
	 * Gets the property manager.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}
	
	/**
	 * Sets the property manager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the oms error tools.
	 *
	 * @return the oms error tools
	 */
	public TRUOMSErrorTools getOmsErrorTools() {
		return mOmsErrorTools;
	}
	
	/**
	 * Sets the oms error tools.
	 *
	 * @param pOmsErrorTools the new oms error tools
	 */
	public void setOmsErrorTools(TRUOMSErrorTools pOmsErrorTools) {
		mOmsErrorTools = pOmsErrorTools;
	}
	/**
	 * Holds reference for mGWItemIdAndRelID.
	 */
	public Map<String, String> mGWItemIdAndRelID = null;


	/**
	 * Gets the GW item id and rel id.
	 *
	 * @return the gWItemIdAndRelID
	 */
	public Map<String, String> getGWItemIdAndRelID() {
		if(mGWItemIdAndRelID == null){
			mGWItemIdAndRelID = new HashMap<String, String>();
		}
		return mGWItemIdAndRelID;
	}

	/**
	 * Sets the gw item id and rel id.
	 *
	 * @param pGWItemIdAndRelID the gWItemIdAndRelID to set
	 */
	public void setGWItemIdAndRelID(Map<String, String> pGWItemIdAndRelID) {
		mGWItemIdAndRelID = pGWItemIdAndRelID;
	}

	/**
	 * Gets the tax counter.
	 *
	 * @return the taxCounter
	 */
	public int getTaxCounter() {
		return mTaxCounter;
	}

	/**
	 * Sets the tax counter.
	 *
	 * @param pTaxCounter the taxCounter to set
	 */
	public void setTaxCounter(int pTaxCounter) {
		mTaxCounter = pTaxCounter;
	}
	
	/**
	 * Gets the tru configuration.
	 *
	 * @return the truConfiguration
	 */
	public TRUConfiguration getTruConfiguration() {
		return mTruConfiguration;
	}

	/**
	 * Sets the tru configuration.
	 *
	 * @param pTruConfiguration the truConfiguration to set
	 */
	public void setTruConfiguration(TRUConfiguration pTruConfiguration) {
		mTruConfiguration = pTruConfiguration;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the firstName
	 */
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param pFirstName the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the lastName
	 */
	public String getLastName() {
		return mLastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param pLastName the lastName to set
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}
	
	/**
	 * Gets the phone number.
	 *
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param pPhoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		mPhoneNumber = pPhoneNumber;
	}
	/**
	 * This method is to re order the order details JSON by grouping based on the carton objects.
	 * @param pOrderDetailsJSON - Order Details JSON to parse
	 * @return JSON - reOrdered JSON
	 */
	public String rearrangeOrderDetailsJSON(String pOrderDetailsJSON){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
		String formatedJsonString = null;
		JsonArray orderLineArray = null;
		Date date = null;
		String formatDate = null;
		String[] dateString = null;
		SimpleDateFormat updateFormatter = null;
		JsonArray paymentDetailArray = null;		
		JsonObject orderLineObject = null;
		JsonObject paymentDetailObject = null;		
		JsonParser pareser = new JsonParser();
		Map<String, String> orderStatusMap = null;
		JsonElement parse = pareser.parse(pOrderDetailsJSON);
		JsonObject orderDetailsJsonObject = parse.getAsJsonObject();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		JsonObject customerOrderNode = orderDetailsJsonObject.getAsJsonObject(configuration.getOrderDetailsCustomerOrderJsonElement());
		//order status conevrt to active from order status map
		JsonElement orderStatus = customerOrderNode.get(getOmsConfiguration().getOrderStatusJsonElement());
		String orderStatusString = orderStatus.getAsString();
		orderStatusMap = getOmsConfiguration().getOrderStatusOLMap();
		if(orderStatusMap.containsKey(orderStatusString)){
			customerOrderNode.remove(getOmsConfiguration().getOrderStatusJsonElement());
			customerOrderNode.addProperty(getOmsConfiguration().getOrderStatusJsonElement(), orderStatusMap.get(orderStatusString));
		}
		//formating date 
		JsonElement orderCapturedDate = customerOrderNode.get(configuration.getOrderDetailsOrderCapturedDateJsonElement());
		String orderCapturedDateString = orderCapturedDate.getAsString();
		dateString = orderCapturedDateString.split(TRUConstants.WHITE_SPACE);
		if (!StringUtils.isBlank(dateString[TRUConstants.ZERO])) {
			SimpleDateFormat formatter = new SimpleDateFormat(TRUConstants.RESPONCE_DATE, Locale.US);
			updateFormatter = new SimpleDateFormat(TRUConstants.ORDER_HISTORY_DATE_DISPLAY, Locale.US);
			try {
				date = formatter.parse(dateString[TRUConstants.ZERO]);
				if (date != null) {
					formatDate = updateFormatter.format(date);
				}
			} catch (ParseException e) {
				if (isLoggingError()) {
					vlogError("ParseException:  While getting promo status items : {0}",e);
				}
			}
			customerOrderNode.remove(configuration.getOrderDetailsOrderCapturedDateJsonElement());
			customerOrderNode.addProperty(configuration.getOrderDetailsOrderCapturedDateJsonElement(),formatDate);
		}
		JsonObject paymentDetailsObject = customerOrderNode.getAsJsonObject(configuration.getOrderDetailsPaymentDetailsJsonElement());
		if (paymentDetailsObject != null) {
			if (paymentDetailsObject.get(configuration.getOrderDetailsPaymentDetailJsonElement()) instanceof JsonObject) {
			    paymentDetailObject = paymentDetailsObject.getAsJsonObject(configuration.getOrderDetailsPaymentDetailJsonElement());
				paymentDetailArray =  new JsonArray();
				paymentDetailArray.add(paymentDetailObject);
			} else {
				paymentDetailArray = (JsonArray) paymentDetailsObject.get(configuration.getOrderDetailsPaymentDetailJsonElement());
			}
			paymentDetailsObject.remove(configuration.getOrderDetailsPaymentDetailJsonElement());
			paymentDetailsObject.add(configuration.getOrderDetailsPaymentDetailJsonElement(), paymentDetailArray);
		}
		if(paymentDetailArray != null){
			for(int i=TRUOMSConstant.INT_ZERO;i<paymentDetailArray.size();i++){
				StringBuffer newAccDisplayNumber=new StringBuffer(); 
				JsonElement cusPayDetailElement=paymentDetailArray.get(i);
				JsonObject cusPayDetailObject=cusPayDetailElement.getAsJsonObject();
				JsonElement accDispNumberJsonElement = cusPayDetailObject.get(configuration.getPaymentDetailsPaymentDetailAccountDisplayNumber());
				if(accDispNumberJsonElement != null){
					String accountDisplayNumber=cusPayDetailObject.get(configuration.getPaymentDetailsPaymentDetailAccountDisplayNumber()).getAsString();
					newAccDisplayNumber.append(TRUOMSConstant.ACCOUNT_PREFIX).append(accountDisplayNumber);
					cusPayDetailObject.remove(configuration.getPaymentDetailsPaymentDetailAccountDisplayNumber());
					cusPayDetailObject.addProperty(configuration.getPaymentDetailsPaymentDetailAccountDisplayNumber(), newAccDisplayNumber.toString());
					newAccDisplayNumber.setLength(TRUOMSConstant.INT_ZERO);
				}
			}
		}
		boolean responseStatus = customerOrderNode.get(configuration.getOrderDetailsResponseStatusJsonElement()).getAsBoolean();
		if (responseStatus) {
		JsonObject orderLinesNodes = customerOrderNode.getAsJsonObject(configuration.getOrderDetailsOrderLinesJsonElement());
		if (orderLinesNodes.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()) instanceof JsonObject) {
			orderLineObject = (JsonObject) orderLinesNodes.get(configuration.getOrderDetailsCustomerOrderLineJsonElement());
			orderLineArray =  new JsonArray();
			orderLineArray.add(orderLineObject);
		} else {
			orderLineArray = (JsonArray) orderLinesNodes.get(configuration.getOrderDetailsCustomerOrderLineJsonElement());
		}
		JsonObject shipmentDetails = customerOrderNode.getAsJsonObject(configuration.getOrderDetailsShipmentDetailsJsonElement());
		if (shipmentDetails != null) {
			    formatedJsonString = rearrangeForCartonItem(orderLineArray,customerOrderNode,orderDetailsJsonObject);
			} else {
				formatedJsonString = rearrangeForNonCartonItem(orderLineArray,customerOrderNode,orderDetailsJsonObject);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
		return formatedJsonString;
	}

	/**
	 * This method is used to get the order status value which is configure at OMS Configuration.
	 * @param pOrderLineArray - Order line array
	 * @param pCustomerOrder - JsonObject
	 * @param pOrderDetailsJsonObject - JsonObject
	 * @return json - Json String
	 */
	private String rearrangeForCartonItem(JsonArray pOrderLineArray, JsonObject pCustomerOrder, JsonObject pOrderDetailsJsonObject) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
         JsonArray displayOrderArray = new JsonArray();
         JsonArray orderLines = null;
         JsonObject displayOrder = new JsonObject();
         JsonArray shippingInfoArray = new JsonArray();
         JsonObject shippingInfo = null;
         double chargeAmount = TRUConstants.DOUBLE_ZERO;
         double gwChargeAmount = TRUConstants.DOUBLE_ZERO;
         double ewasteFee = TRUConstants.DOUBLE_ZERO;
         double shippingSurcharge = TRUConstants.DOUBLE_ZERO;
         double totalshippingSurcharge = TRUConstants.DOUBLE_ZERO;
         String bppTerm = null;
 		 double bppChargeAmount = TRUConstants.DOUBLE_ZERO;
 		 String itemId = null;
 		 JsonObject carton = null;
 		 JsonArray cartonArray = null;
 		 JsonObject shipmentDetail = null;
		 JsonArray shipmentDetailArray = null;
 		 JsonObject doDetailObject = null;
		 JsonArray doDetailsArray = null;
		 JsonObject doLineItemObject = null;
		 JsonArray doLineItemsArray = null;
		 JsonObject cartonDetailObject = null;
		 JsonArray cartonDetailsArray = null;
		 String chargeName = null;
		 int totalQuantity = TRUConstants._0;
 		 TRUOMSConfiguration configuration = getOmsConfiguration();
         int coSize = pOrderLineArray.size();//Added to avoid PMD violations
         for (int c = TRUConstants.ZERO;  c < coSize; c++){
             JsonElement orderLineElement = pOrderLineArray.get(c);
             JsonObject orderLineObject = orderLineElement.getAsJsonObject();
             orderLineObject.addProperty(configuration.getOrderDetailsCartonCheckJsonElement(), Boolean.FALSE);
             shippingInfo = orderLineObject.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
             shippingInfoArray.add(shippingInfo);
         }
         pCustomerOrder.add(configuration.getOrderDetailsShippingInfoJsonElement(), shippingInfoArray);
         pCustomerOrder.add(configuration.getOrderDetailsDisplayOrderJsonElement(), displayOrder);
         JsonObject shipmentDetails = pCustomerOrder.getAsJsonObject(configuration.getOrderDetailsShipmentDetailsJsonElement());
 		 if(shipmentDetails != null) {
 			 if (shipmentDetails.get(configuration.getOrderDetailsShipmentDetailJsonElement()) instanceof JsonObject) {
 				shipmentDetail = shipmentDetails.getAsJsonObject(configuration.getOrderDetailsShipmentDetailJsonElement());
 				shipmentDetailArray = new JsonArray();
 				shipmentDetailArray.add(shipmentDetail);
 			 } else {
 				shipmentDetailArray = (JsonArray) shipmentDetails.get(configuration.getOrderDetailsShipmentDetailJsonElement());
 			 }
 			shipmentDetails.remove(configuration.getOrderDetailsShipmentDetailJsonElement());
 			shipmentDetails.add(configuration.getOrderDetailsShipmentDetailJsonElement(), shipmentDetailArray);
 		 }
 		 int shipmentDetailSize = shipmentDetailArray.size();
 		 for (int s = TRUConstants.ZERO; s < shipmentDetailSize; s++) {
 			JsonElement shipmentIncrementElement = shipmentDetailArray.get(s);
 			JsonObject shipmentObject = shipmentIncrementElement.getAsJsonObject();
 			JsonObject cartons = shipmentObject.getAsJsonObject(configuration.getOrderDetailsCartonsJsonElement());
 			if (cartons != null) {
	 			if (cartons.get(configuration.getOrderDetailsCartonJsonElement()) instanceof JsonObject) {
	 			    carton = cartons.getAsJsonObject(configuration.getOrderDetailsCartonJsonElement());
	 			    cartonArray =  new JsonArray();
	 			    cartonArray.add(carton);
	 			} else {
	 				cartonArray = (JsonArray) cartons.get(configuration.getOrderDetailsCartonJsonElement());
	 			}
	 			cartons.remove(configuration.getOrderDetailsCartonJsonElement());
	 			cartons.add(configuration.getOrderDetailsCartonJsonElement(), cartonArray);
 			}
         int cartonsize = cartonArray.size();//Added to avoid PMD violations
         for (int a = TRUConstants.ZERO;  a < cartonsize; a++){
         orderLines = new JsonArray();
         JsonElement cartonIncrementElement = cartonArray.get(a);
         JsonObject cartonObject = cartonIncrementElement.getAsJsonObject();
         String cartonDoNbr = cartonObject.get(configuration.getOrderDetailsdoDetailsDoNbrJsonElement()).getAsString();
         JsonObject doDetails = pCustomerOrder.getAsJsonObject(configuration.getOrderDetailsDoDetailsJsonElement());
         if (doDetails != null) {
	 			if (doDetails.get(configuration.getOrderDetailsDoDetailJsonElement()) instanceof JsonObject) {
	 				doDetailObject = doDetails.getAsJsonObject(configuration.getOrderDetailsDoDetailJsonElement());
	 			    doDetailsArray =  new JsonArray();
	 			    doDetailsArray.add(doDetailObject);
	 			} else {
	 				doDetailsArray = (JsonArray) doDetails.get(configuration.getOrderDetailsDoDetailJsonElement());
	 			}
	 			doDetails.remove(configuration.getOrderDetailsDoDetailJsonElement());
	 			doDetails.add(configuration.getOrderDetailsDoDetailJsonElement(), doDetailsArray);
			}
         int doDetailsize = doDetailsArray.size();//Added to avoid PMD violations
         for (int b = TRUConstants.ZERO;  b < doDetailsize; b++){
             JsonElement doDetailElement = doDetailsArray.get(b);
             JsonObject doDetailObjectNew = doDetailElement.getAsJsonObject();
             String doDetailDoNbr = doDetailObjectNew.get(configuration.getOrderDetailsdoDetailsDoNbrJsonElement()).getAsString();
             if(cartonDoNbr.equals(doDetailDoNbr)){
            // cartonDetils check with doLineItems in doDetils	 
            	 JsonObject cartonDetails = cartonObject.getAsJsonObject(configuration.getOrderDetailsCartonDetailsJsonElement());
                 if (cartonDetails != null) {
        	 			if (cartonDetails.get(configuration.getOrderDetailsCartonDetailJsonElement()) instanceof JsonObject) {
        	 				cartonDetailObject = cartonDetails.getAsJsonObject(configuration.getOrderDetailsCartonDetailJsonElement());
        	 				cartonDetailsArray =  new JsonArray();
        	 				cartonDetailsArray.add(cartonDetailObject);
        	 			} else {
        	 				cartonDetailsArray = (JsonArray) cartonDetails.get(configuration.getOrderDetailsCartonDetailJsonElement());
        	 			}
        	 			cartonDetails.remove(configuration.getOrderDetailsCartonDetailJsonElement());
        	 			cartonDetails.add(configuration.getOrderDetailsCartonDetailJsonElement(), cartonDetailsArray);
        			}
           int cartonDetailsSize = cartonDetailsArray.size();
           for(int d = TRUConstants.ZERO;  d < cartonDetailsSize; d++) {
        	   JsonElement cartonDetailElement = cartonDetailsArray.get(d);
               JsonObject cartonDetailObjectNew = cartonDetailElement.getAsJsonObject();
               String cartonDetailDoLineNbr = cartonDetailObjectNew.get(configuration.getOrderDetailsDoLineNbrJsonElement()).getAsString();
               JsonObject doLineItems = doDetailObjectNew.getAsJsonObject(configuration.getOrderDetailsDoLineItemsJsonElement());
               if (doLineItems != null) {
    	 			if (doLineItems.get(configuration.getOrderDetailsDoLineItemJsonElement()) instanceof JsonObject) {
    	 				doLineItemObject = doLineItems.getAsJsonObject(configuration.getOrderDetailsDoLineItemJsonElement());
    	 				doLineItemsArray =  new JsonArray();
    	 				doLineItemsArray.add(doLineItemObject);
    	 			} else {
    	 				doLineItemsArray = (JsonArray) doLineItems.get(configuration.getOrderDetailsDoLineItemJsonElement());
    	 			}
    	 			doLineItems.remove(configuration.getOrderDetailsDoLineItemJsonElement());
    	 			doLineItems.add(configuration.getOrderDetailsDoLineItemJsonElement(), doLineItemsArray);
    			}
               int doLineItemsSize = doLineItemsArray.size();
               for(int f = TRUConstants.ZERO;  f < doLineItemsSize; f++) {
            	   JsonElement doLineItemElement = doLineItemsArray.get(f);
                   JsonObject doLineItemObjectNew = doLineItemElement.getAsJsonObject();
                   String doLineItemDoLineNbr = doLineItemObjectNew.get(configuration.getOrderDetailsDoLineNbrJsonElement()).getAsString();
             if (cartonDetailDoLineNbr.equalsIgnoreCase(doLineItemDoLineNbr)) {
            	 String coLineNbr = doLineItemObjectNew.get(configuration.getOrderDetailsCoLineNbrJsonElement()).getAsString();
             int orderLineArraySize = pOrderLineArray.size();//Added to avoid PMD violations
             shippingInfo = new JsonObject();
             for (int c = TRUConstants.ZERO;  c < orderLineArraySize; c++){
                 JsonElement orderLineElementUpdate = pOrderLineArray.get(c);
                 JsonObject orderLineObjectUpdate = orderLineElementUpdate.getAsJsonObject();
                 String orderLineNumber = orderLineObjectUpdate.get(configuration.getOrderDetailsOrderLineNumberJsonElement()).getAsString();
                 if(orderLineNumber.equals(coLineNbr)){
                     orderLines.add(orderLineElementUpdate);
                     orderLineObjectUpdate.remove(configuration.getOrderDetailsCartonCheckJsonElement());
                     orderLineObjectUpdate.addProperty(configuration.getOrderDetailsCartonCheckJsonElement(), Boolean.TRUE);
                     shippingInfo = orderLineObjectUpdate.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
                     cartonObject.add(configuration.getOrderDetailsShippingInfoJsonElement(), shippingInfo);
                 }
                 JsonObject chargeDetailsObject = orderLineObjectUpdate.getAsJsonObject(configuration.getOrderDetailsChargeDetailsJsonElement());
                 String giftWrapItemID = null;
                 if (chargeDetailsObject != null) {
     				JsonArray chargeDetailArray = (JsonArray) chargeDetailsObject.get(configuration.getOrderDetailsChargeDetailJsonElement());
    				int chargeDetailArraySize = chargeDetailArray.size();
    				for (int j = TRUConstants.ZERO; j < chargeDetailArraySize; j++){
    					JsonElement chargeDetailElement = chargeDetailArray.get(j);
    					JsonObject chargeDetailObject = chargeDetailElement.getAsJsonObject();
    					if (chargeDetailObject != null) {
    						String chargeCategory = chargeDetailObject.get(configuration.getOrderDetailsChargeCategoryJsonElement()).getAsString();
    						if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryShipping())) {
    							if (chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()) != null && chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString().equalsIgnoreCase(configuration.getOrderDetailsChargeNameValue())) {
    								shippingSurcharge = chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).
    										getAsDouble();
    								totalshippingSurcharge += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).
    										getAsDouble();
    							} else {
    								chargeAmount += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
    							}
    						}  else if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryVS())){
    							gwChargeAmount += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
    							giftWrapItemID = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
    						} else if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryMisc())){
    							ewasteFee += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
    						} else if (chargeCategory.equals(configuration.getOrderDetailsVasJsonElement())) {
    						    chargeName = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
    							bppTerm = getBppTerm(chargeName);
    							bppChargeAmount = chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
    						}
    					}
    				}
    			}
             	orderLineObjectUpdate.addProperty(configuration.getOrderDetailsSurchargeJsonElement(), shippingSurcharge);
             	orderLineObjectUpdate.addProperty(configuration.getOrderDetailsBppTermJsonElement(), bppTerm);
             	orderLineObjectUpdate.addProperty(configuration.getOrderDetailsBppChargeAmountJsonElement(), bppChargeAmount);
 				itemId = orderLineObjectUpdate.get(configuration.getOrderDetailsItemIdJsonElement()).getAsString();
 				String siteNameForGW = getSiteName(giftWrapItemID);
 				String productPageUrl = getProductPageURL(itemId);
 				orderLineObjectUpdate.addProperty(configuration.getOrderDetailsProductPageUrlJsonElement(),productPageUrl);
 				orderLineObjectUpdate.addProperty(configuration.getOrderDetailsSiteNameForGWJsonElement(),siteNameForGW);
                  }
               }
           }
             	}
             }
         }
         cartonObject.add(configuration.getOrderDetailsOrderLinesJsonElement(),orderLines);
         }
	}
        JsonObject orderTotalsObject = pCustomerOrder.getAsJsonObject(configuration.getOrderDetailsOrderTotalsJsonElement());
  		orderTotalsObject.addProperty(configuration.getOrderDetailsShippingJsonElement(), chargeAmount);
  		orderTotalsObject.addProperty(configuration.getOrderDetailsGiftwrapJsonElement(), gwChargeAmount);
  		orderTotalsObject.addProperty(configuration.getOrderDetailsEwasteJsonElement(), ewasteFee);
  		orderTotalsObject.addProperty(configuration.getOrderDetailsTotalShippingSurchargeJsonElement(), totalshippingSurcharge);
  		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedShippingJsonElement(), convertToString(chargeAmount));
  		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedGiftwrapJsonElement(), convertToString(gwChargeAmount));
  		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedEwasteJsonElement(), convertToString(ewasteFee));
  		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedTotalShippingSurchargeJsonElement(),convertToString(totalshippingSurcharge));
        // pCustomerOrder.remove(configuration.getOrderDetailsOrderLinesJsonElement());
         shippingInfoArray = (JsonArray) pCustomerOrder.get(configuration.getOrderDetailsShippingInfoJsonElement());
         JsonObject displayObject = new JsonObject();
         int cartonsize1 = cartonArray.size();//Added to avoid PMD violations
         JsonArray dispOrderArray = new JsonArray();
         for (int e = TRUConstants.ZERO; e < cartonsize1; e++){
             JsonElement jsonElement = cartonArray.get(e);
             JsonObject cartonObject = jsonElement.getAsJsonObject();
             JsonArray orderLinesArray = (JsonArray) cartonObject.get(configuration.getOrderDetailsOrderLinesJsonElement());
             JsonArray cartonArrayNew = new JsonArray();
             if(displayOrderArray != null && displayOrderArray.size() != TRUConstants.ZERO && !displayOrderArray.isJsonNull()){
            	 JsonElement jsonElementUpdated = null;
  				JsonObject orderLineObjectUpdated = null;
  				JsonObject shipInfoJsonObj = null;
            	 int dispJsonArraySize = displayOrderArray.size();
  				for (int z = TRUConstants.ZERO; z < dispJsonArraySize; z++) {
  					jsonElementUpdated = displayOrderArray.get(z);
  					orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
  					shipInfoJsonObj = orderLineObjectUpdated.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
  					if (shippingInfo.equals(shipInfoJsonObj)) {
  						displayObject = orderLineObjectUpdated;
  						displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.TRUE);
  						break;
  					} else {
  						displayObject = new JsonObject();
  						displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.TRUE);
  					}
  				}
  				if (shippingInfo.equals(shipInfoJsonObj)) {
 					displayObject = orderLineObjectUpdated;
 					displayObject.get(configuration.getOrderDetailsCartonsJsonElement()).getAsJsonArray().add(cartonObject);
 				} else {
 					displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(),TRUConstants.DOUBLE_QUOTE);
 					displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(),shippingInfo);
 					displayObject.get(configuration.getOrderDetailsCartonsJsonElement()).getAsJsonArray().add(cartonObject);
 					dispOrderArray.add(displayObject);
 					displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.TRUE);
 				}
 				displayOrderArray = new JsonArray();
 				displayOrderArray.addAll(dispOrderArray);
 			} else {
 				displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(), shippingInfo);
 				displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.TRUE);
                cartonArrayNew.add(cartonObject);
                displayObject.add(configuration.getOrderDetailsCartonsJsonElement(), cartonArrayNew);
 				displayOrderArray.add(displayObject);
 				dispOrderArray.add(displayObject);
 			}
             displayObject.addProperty(configuration.getOrderDetailsItemCountJsonElement(), orderLinesArray.size());
             int orderLinesArraysize = orderLinesArray.size();
             for (int d = TRUConstants.ZERO;  d < orderLinesArraysize; d++){
                 JsonElement orderLineElementRemove = orderLinesArray.get(d);
                 JsonObject orderLineElementObject = orderLineElementRemove.getAsJsonObject();
                 orderLineElementObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
                 orderLineElementObject.remove(configuration.getOrderDetailsChargeDetailsJsonElement());
             }
             
         }
         if(displayOrderArray != null){
        	 int shipmentItemCount = TRUConstants._0;
        	 for (JsonElement displayObj : dispOrderArray) {
        		 int cartonLineItemCount=TRUOMSConstant.INT_ZERO;
        		 JsonObject displayjsonObj = displayObj.getAsJsonObject();
        		 JsonArray cartonJsonArray = displayjsonObj.get(configuration.getOrderDetailsCartonsJsonElement()).getAsJsonArray();
        		 int cartonsSize = cartonJsonArray.size();
        		 displayjsonObj.addProperty(configuration.getOrderDetailsCartonCountJsonElement(), cartonsSize);
        		 for (JsonElement jsonElement : cartonJsonArray) {
        			 JsonObject cartonObj = jsonElement.getAsJsonObject();
        			 shipmentItemCount += cartonObj.get(configuration.getOrderDetailsOrderLinesJsonElement()).getAsJsonArray().size();
        			 totalQuantity += shipmentItemCount;
        			 cartonLineItemCount++;
        			 cartonObj.addProperty(configuration.getOrderDetailsCartonLineItemCountJsonElement(), cartonLineItemCount);
        		 }
        		 displayjsonObj.addProperty(configuration.getOrderDetailsShipmentItemCountJsonElement(), shipmentItemCount);
        	 }
         }
         //set pickup details for ISPU
         setPickUpDetails(displayOrderArray,Boolean.TRUE);
         displayOrder.add(configuration.getOrderDetailsShippingInfoArrayJsonElement(), displayOrderArray);
         //for carton and non carton logic
         JsonArray nonCartonList = getNonCartonList(pOrderLineArray);
         if (nonCartonList.size() > TRUConstants.ZERO) {
        	 String rearrangeForCartonNonCartonItems = rearrangeForCartonNonCartonItems(nonCartonList,pCustomerOrder,pOrderDetailsJsonObject,totalQuantity);
        	 return rearrangeForCartonNonCartonItems;
         }
         pCustomerOrder.remove(configuration.getOrderDetailsShipmentDetailsJsonElement());
         pCustomerOrder.remove(configuration.getOrderDetailsShippingInfoJsonElement());
         pCustomerOrder.remove(configuration.getOrderDetailsOrderLinesJsonElement());
         pCustomerOrder.addProperty(configuration.getOrderDetailsTotalQuantityPropertyName(), totalQuantity);
         pCustomerOrder.addProperty(configuration.getOrderDetailsIscartonAvailableJsonElement(), true);
         Gson gson = new Gson();
         String formatedJsonString = gson.toJson(pOrderDetailsJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
		return formatedJsonString;
	}

	
	/**
	 * Rearrange for carton non carton items.
	 *
	 * @param pOrderLineArray the order line array
	 * @param pCustomerOrder the customer order
	 * @param pOrderDetailsJsonObject the order details json object
	 * @param pTotalQuantity total quantity
	 * @return the string
	 */
	private String rearrangeForCartonNonCartonItems(JsonArray pOrderLineArray,
			JsonObject pCustomerOrder, JsonObject pOrderDetailsJsonObject, int pTotalQuantity) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getNonCartonList]");
		}
		JsonArray displayOrderArray = new JsonArray();
		double shippingSurcharge = TRUConstants.DOUBLE_ZERO;
		String bppTerm = null;
		double bppChargeAmount = TRUConstants.DOUBLE_ZERO;
		long totalQuantity = TRUConstants.LONG_ZERO;
		String itemId = null;
		TRUOMSConfiguration configuration = getOmsConfiguration();
		JsonObject displayObject = new JsonObject();
		String endDateShipmentObj = null;
		String startDateShipmentObj = null;
		String endDate= null;
		String startDate = null;
		int coSize = pOrderLineArray.size();
		displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), false);
		JsonArray dispOrderArray = new JsonArray();
		String chargeName = null;
		for (int c = TRUConstants.ZERO;  c < coSize; c++) {
			JsonElement jsonElement = pOrderLineArray.get(c);
			JsonObject orderLineObject = jsonElement.getAsJsonObject();
			String giftMessage = getGiftMessage(orderLineObject);
			updateShipViaWithNoteType(orderLineObject);
			JsonObject shippingInfo = orderLineObject.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
			if (shippingInfo != null && orderLineObject.get(configuration.getOrderDetailsOrderedQuantityPropertyName()) != null) {
				String orderedQuantity = orderLineObject.get(configuration.getOrderDetailsOrderedQuantityPropertyName()).getAsString();
				totalQuantity += Long.parseLong(orderedQuantity);
			}
			JsonArray orderLineArrayUpdated = new JsonArray();
			if (displayOrderArray != null && displayOrderArray.size() != TRUConstants.ZERO && !displayOrderArray.isJsonNull()) {
				JsonElement jsonElementUpdated = null;
				JsonObject orderLineObjectUpdated = null;
				JsonObject shipInfoJsonObj = null;
				JsonObject shippingInfoShippingAddress = shippingInfo.get(configuration.getOrderDetailsShippingInfoShippingAddress()).getAsJsonObject();
				JsonObject shipInfoJsonObjShippingAddress = null;
				int displayOrderArraySize = displayOrderArray.size();
				for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
					jsonElementUpdated = displayOrderArray.get(z);
					orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
					shipInfoJsonObj = orderLineObjectUpdated.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
					shipInfoJsonObjShippingAddress = shipInfoJsonObj.get(configuration.getOrderDetailsShippingInfoShippingAddress()).getAsJsonObject();
					if (shippingInfoShippingAddress.equals(shipInfoJsonObjShippingAddress)) {
						displayObject = orderLineObjectUpdated;
						displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.FALSE);
						break;
					} else {
						displayObject = new JsonObject();
						displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.FALSE);
					}
				}
				if (shippingInfoShippingAddress.equals(shipInfoJsonObjShippingAddress)) {
					String asString2 = shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()).getAsString();
					displayObject = orderLineObjectUpdated;
					JsonArray shipmentArray = displayObject.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
					JsonObject asJsonObject = null;
					String asString = null;
					for(int m = TRUConstants.ZERO; m < shipmentArray.size(); m++){
						JsonElement jsonElement2 = shipmentArray.get(m);
						asJsonObject = jsonElement2.getAsJsonObject();
						asString = asJsonObject.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()).getAsString();
						startDate = getStartDate(orderLineObject);
						endDate = getEndDate(orderLineObject);
						if(asJsonObject.get(configuration.getOrderDetailsStartDate())!=null){
						startDateShipmentObj=asJsonObject.get(configuration.getOrderDetailsStartDate()).getAsString();
						}
						if(asJsonObject.get(configuration.getOrderDetailsEndDate())!=null){
						endDateShipmentObj = asJsonObject.get(configuration.getOrderDetailsEndDate()).getAsString();
						}
						if(asString2.equals(asString) && startDate.equals(startDateShipmentObj) && endDate.equalsIgnoreCase(endDateShipmentObj)){
							break;
						} else {
							asJsonObject = new JsonObject();
						}
					}
					orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
					if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement())== null && giftMessage!=null) {
							displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
							displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
							displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.FALSE);
					}
					if(asString2.equals(asString) && startDate.equals(startDateShipmentObj) && endDate.equalsIgnoreCase(endDateShipmentObj)){
						asJsonObject.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray().add(orderLineObject);
					} else {
						asJsonObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
						orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
						orderLineArrayUpdated.add(orderLineObject);
						asJsonObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
						shipmentArray.add(asJsonObject);
					}
				} else {
					displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(),shippingInfo);
					displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.FALSE);
					orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
					orderLineArrayUpdated.add(orderLineObject);
					JsonArray shipmentArray = new JsonArray();
					JsonObject shipmentObject = new JsonObject();
					shipmentObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
					shipmentObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
					shipmentArray.add(shipmentObject);
					displayObject.add(configuration.getShippingInfoShippmentArray(), shipmentArray);
					if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement()) == null && giftMessage!=null) {
							displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
							displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
					}
					dispOrderArray.add(displayObject);
					setStartDateEndDate(dispOrderArray);
				}
				displayOrderArray = new JsonArray();
				displayOrderArray.addAll(dispOrderArray);
			}
			else {
				displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(), shippingInfo);
				displayObject.addProperty(configuration.getOrderDetailsIsCartonCheckJsonElement(), Boolean.FALSE);
				orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
				JsonArray shipmentArray = new JsonArray();
				JsonObject shipmentObject = new JsonObject();
				orderLineArrayUpdated.add(orderLineObject);
				shipmentObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
				shipmentObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
				shipmentArray.add(shipmentObject);
				displayObject.add(configuration.getShippingInfoShippmentArray(), shipmentArray);
				
				if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement()) == null && giftMessage != null ) {
					displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
					displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
				}
				displayOrderArray.add(displayObject);
				setStartDateEndDate(displayOrderArray);
				dispOrderArray.add(displayObject);
			}
			String giftWrapItemID = null;
			JsonObject chargeDetailsObject =  orderLineObject.getAsJsonObject(configuration.getOrderDetailsChargeDetailsJsonElement());
			if (chargeDetailsObject != null) {
				JsonArray chargeDetailArray = (JsonArray) chargeDetailsObject.get(configuration.getOrderDetailsChargeDetailJsonElement());
				int chargeDetailArraySize = chargeDetailArray.size();
				for (int j = TRUConstants.ZERO; j < chargeDetailArraySize; j++){
					JsonElement chargeDetailElement = chargeDetailArray.get(j);
					JsonObject chargeDetailObject = chargeDetailElement.getAsJsonObject();
					if (chargeDetailObject != null) {
						String chargeCategory = chargeDetailObject.get(configuration.getOrderDetailsChargeCategoryJsonElement()).getAsString();
							if (chargeCategory.equals(configuration.getOrderDetailsVasJsonElement())) {
						    chargeName = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
							bppTerm = getBppTerm(chargeName);
							bppChargeAmount = chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
						} else if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryVS())){
							giftWrapItemID = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
						}
					}
				}
			}
			orderLineObject.addProperty(configuration.getOrderDetailsSurchargeJsonElement(), shippingSurcharge);
			orderLineObject.addProperty(configuration.getOrderDetailsBppTermJsonElement(), bppTerm);
			orderLineObject.addProperty(configuration.getOrderDetailsBppChargeAmountJsonElement(), bppChargeAmount);
			itemId = orderLineObject.get(configuration.getOrderDetailsItemIdJsonElement()).getAsString();
			String siteNameForGW = getSiteName(giftWrapItemID);
			String productPageUrl = getProductPageURL(itemId);
			orderLineObject.addProperty(configuration.getOrderDetailsProductPageUrlJsonElement(),productPageUrl);
			orderLineObject.addProperty(configuration.getOrderDetailsSiteNameForGWJsonElement(),siteNameForGW);
		}
		JsonObject displayOrderObject = pCustomerOrder.get(configuration.getOrderDetailsDisplayOrderJsonElement()).getAsJsonObject();
		JsonArray shippingInfoArray = displayOrderObject.get(configuration.getOrderDetailsShippingInfoArrayJsonElement()).getAsJsonArray();
		shippingInfoArray.addAll(displayOrderArray);
		JsonArray putDonationItemToLastObject = putDonationItemToLastObject(shippingInfoArray);
		displayOrderObject.remove(configuration.getOrderDetailsShippingInfoArrayJsonElement());
		displayOrderObject.add(configuration.getOrderDetailsShippingInfoArrayJsonElement(), putDonationItemToLastObject);
		pCustomerOrder.add(configuration.getOrderDetailsDisplayOrderJsonElement(), displayOrderObject);
		int displayOrderArraySize = displayOrderArray.size();
		setShipViaStartEndDate(displayOrderArray);
		setPickUpDetails(displayOrderArray,Boolean.FALSE);
		for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
			long orderedQuantity = TRUConstants.LONG_ZERO;
			JsonElement jsonElementUpdated = displayOrderArray.get(z); 
			JsonObject orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
				JsonArray shipmentArray = orderLineObjectUpdated.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
				JsonObject shipInfoJsonObj = orderLineObjectUpdated.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
				for(int k = TRUConstants.ZERO; k < shipmentArray.size(); k++){
					JsonElement shippmentJson = shipmentArray.get(k);
					JsonObject shippmentJsonObj = shippmentJson.getAsJsonObject();
					JsonArray orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray();
				for (int m = TRUConstants.ZERO; m < orderlineArray.size(); m++) {
					JsonElement orderLineJson = orderlineArray.get(m);
					JsonObject orderLindeJsonObj = orderLineJson.getAsJsonObject();
					orderedQuantity += orderLindeJsonObj.get(configuration.getOrderDetailsOrderedQuantityPropertyName()).getAsLong();
					}
				}
				shipInfoJsonObj.addProperty(configuration.getOrderDetailsItemCountJsonElement(), String.valueOf(orderedQuantity));
		}
		pCustomerOrder.addProperty(configuration.getOrderDetailsTotalQuantityPropertyName(), totalQuantity+pTotalQuantity);
		pCustomerOrder.addProperty(configuration.getOrderDetailsIscartonAvailableJsonElement(), true);
		pCustomerOrder.remove(configuration.getOrderDetailsShipmentDetailsJsonElement());
		pCustomerOrder.remove(configuration.getOrderDetailsShippingInfoJsonElement());
		pCustomerOrder.remove(configuration.getOrderDetailsOrderLinesJsonElement());
		pCustomerOrder.addProperty(configuration.getOrderDetailsHelpAndReturnsJsonElement(), 
				configuration.getOrderDetailsHelpAndReturnsURLJsonElement());
		Gson gson = new Gson();
		String formatedJsonString = gson.toJson(pOrderDetailsJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeForNonCartonItem]");
		}
		return formatedJsonString;
		
	}

	/**
	 * Put donation item to last object.
	 *
	 * @param pShippingInfoArray the shipping info array
	 * @return the newShippingInfoArray
	 */
	private JsonArray putDonationItemToLastObject(JsonArray pShippingInfoArray) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getNonCartonList]");
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		boolean donationExists= Boolean.FALSE;
		int shippingInfoArray = pShippingInfoArray.size();
		JsonArray newShippingInfoArray =  new JsonArray();
		JsonObject donationObject = new JsonObject();
        for (int c = TRUConstants.ZERO;  c < shippingInfoArray; c++){
        	JsonElement shippingInfoArrayElement = pShippingInfoArray.get(c);
            JsonObject shippingInfoArrayObject = shippingInfoArrayElement.getAsJsonObject();
            JsonObject shippingInfo = shippingInfoArrayObject.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
            if (shippingInfo.get(configuration.getOrderDetailsShipToFacilityJsonElement()) != null && shippingInfo.get(configuration.getOrderDetailsShipToFacilityJsonElement()).getAsString().equalsIgnoreCase(configuration.getShipToFacilityForDonation())) {
            	 donationObject = shippingInfoArrayObject;
            	 donationExists = Boolean.TRUE;
            } else {
            	newShippingInfoArray.add(shippingInfoArrayObject);
            }
        }
        if (donationObject != null && donationExists) {
        	newShippingInfoArray.add(donationObject);
        }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeForNonCartonItem]");
		}
		return newShippingInfoArray;
	}

	/**
	 * Gets the non carton list.
	 *
	 * @param pOrderLineArray the order line array
	 * @return the non carton list
	 */
	private JsonArray getNonCartonList(JsonArray pOrderLineArray) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getNonCartonList]");
		}
		JsonArray orderLineArrayWithOutCarton =  new JsonArray();
		TRUOMSConfiguration configuration = getOmsConfiguration();
		int orderLineArraySize = pOrderLineArray.size();//Added to avoid PMD violations
        for (int c = TRUConstants.ZERO;  c < orderLineArraySize; c++){
            JsonElement orderLineElementUpdate = pOrderLineArray.get(c);
            JsonObject orderLineObjectUpdate = orderLineElementUpdate.getAsJsonObject();
            boolean orderLineNumber = orderLineObjectUpdate.get(configuration.getOrderDetailsCartonCheckJsonElement()).getAsBoolean();
            if (!orderLineNumber) {
            	orderLineArrayWithOutCarton.add(orderLineObjectUpdate);
            }
        }
        
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getNonCartonList]");
		}
		return orderLineArrayWithOutCarton;
		
	}

	/**
	 * This method is used to get the order status value which is configure at OMS Configuration.
	 * @param pOrderLineArray - Order line array
	 * @param pCustomerOrder - JsonObject
	 * @param pOrderDetailsJsonObject - JsonObject
	 * @return json - Json formatedJsonString 
	 */
	private String rearrangeForNonCartonItem(JsonArray pOrderLineArray, JsonObject pCustomerOrder, JsonObject pOrderDetailsJsonObject) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: rearrangeForNonCartonItem]");
		}
		JsonArray displayOrderArray = new JsonArray();
		JsonObject displayOrder = new JsonObject();
		double chargeAmount = TRUConstants.DOUBLE_ZERO;
		double gwChargeAmount = TRUConstants.DOUBLE_ZERO;
		double ewasteFee = TRUConstants.DOUBLE_ZERO;
		double shippingSurcharge = TRUConstants.DOUBLE_ZERO;
		double totalshippingSurcharge = TRUConstants.DOUBLE_ZERO;
		String bppTerm = null;
		double bppChargeAmount = TRUConstants.DOUBLE_ZERO;
		long totalQuantity = TRUConstants.LONG_ZERO;
		String itemId = null;
		String formatedJsonString = null;
		TRUOMSConfiguration configuration = getOmsConfiguration();
		JsonObject displayObject = new JsonObject();
		String endDateShipmentObj = null;
		String startDateShipmentObj = null;
		String endDate= null;
		String startDate = null;
		int coSize = pOrderLineArray.size();
		displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), false);
		JsonArray dispOrderArray = new JsonArray();
		String chargeName = null;
		for (int c = TRUConstants.ZERO;  c < coSize; c++) {
			JsonElement jsonElement = pOrderLineArray.get(c);
			JsonObject orderLineObject = jsonElement.getAsJsonObject();
			String giftMessage = getGiftMessage(orderLineObject);
			updateShipViaWithNoteType(orderLineObject);
			JsonObject shippingInfo = orderLineObject.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
			if (shippingInfo != null && orderLineObject.get(configuration.getOrderDetailsOrderedQuantityPropertyName()) != null) {
				String orderedQuantity = orderLineObject.get(configuration.getOrderDetailsOrderedQuantityPropertyName()).getAsString();
				totalQuantity += Long.parseLong(orderedQuantity);
			}
			JsonArray orderLineArrayUpdated = new JsonArray();
			if (displayOrderArray != null && displayOrderArray.size() != TRUConstants.ZERO && !displayOrderArray.isJsonNull()) {
				JsonElement jsonElementUpdated = null;
				JsonObject orderLineObjectUpdated = null;
				JsonObject shipInfoJsonObj = null;
				JsonObject shippingInfoShippingAddress = shippingInfo.get(configuration.getOrderDetailsShippingInfoShippingAddress()).getAsJsonObject();
				JsonObject shipInfoJsonObjShippingAddress = null;
				int displayOrderArraySize = displayOrderArray.size();
				for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
					jsonElementUpdated = displayOrderArray.get(z);
					orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
					shipInfoJsonObj = orderLineObjectUpdated.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
					shipInfoJsonObjShippingAddress = shipInfoJsonObj.get(configuration.getOrderDetailsShippingInfoShippingAddress()).getAsJsonObject();
					if (shippingInfoShippingAddress.equals(shipInfoJsonObjShippingAddress)) {
						displayObject = orderLineObjectUpdated;
						break;
					} else {
						displayObject = new JsonObject();
					}
				}
				if (shippingInfoShippingAddress.equals(shipInfoJsonObjShippingAddress)) {
					String asString2 = shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()).getAsString();
					displayObject = orderLineObjectUpdated;
					JsonArray shipmentArray = displayObject.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
					JsonObject asJsonObject = null;
					String asString = null;
					for(int m = TRUConstants.ZERO; m < shipmentArray.size(); m++){
						JsonElement jsonElement2 = shipmentArray.get(m);
						asJsonObject = jsonElement2.getAsJsonObject();
						asString = asJsonObject.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()).getAsString();
						startDate = getStartDate(orderLineObject);
						endDate = getEndDate(orderLineObject);
						if(asJsonObject.get(configuration.getOrderDetailsStartDate())!=null){
						startDateShipmentObj=asJsonObject.get(configuration.getOrderDetailsStartDate()).getAsString();
						}
						if(asJsonObject.get(configuration.getOrderDetailsEndDate())!=null){
						endDateShipmentObj = asJsonObject.get(configuration.getOrderDetailsEndDate()).getAsString();
						}
						if(asString2.equals(asString) && startDate.equals(startDateShipmentObj) && endDate.equalsIgnoreCase(endDateShipmentObj)){
							break;
						} else {
							asJsonObject = new JsonObject();
						}
					}
					orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
					if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement())== null && giftMessage!=null) {
							displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
							displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
					}
					if(asString2.equals(asString) && startDate.equals(startDateShipmentObj) && endDate.equalsIgnoreCase(endDateShipmentObj)){
						asJsonObject.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray().add(orderLineObject);
					} else {
						asJsonObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
						orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
						orderLineArrayUpdated.add(orderLineObject);
						asJsonObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
						shipmentArray.add(asJsonObject);
					}
				} else {
					displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(),shippingInfo);
					orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
					orderLineArrayUpdated.add(orderLineObject);
					JsonArray shipmentArray = new JsonArray();
					JsonObject shipmentObject = new JsonObject();
					shipmentObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
					shipmentObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
					shipmentArray.add(shipmentObject);
					displayObject.add(configuration.getShippingInfoShippmentArray(), shipmentArray);
					if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement()) == null && giftMessage!=null) {
							displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
							displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
					}
					dispOrderArray.add(displayObject);
					setStartDateEndDate(dispOrderArray);
				}
				displayOrderArray = new JsonArray();
				displayOrderArray.addAll(dispOrderArray);
			}
			else {
				displayObject.add(configuration.getOrderDetailsShippingInfoJsonElement(), shippingInfo);
				orderLineObject.remove(configuration.getOrderDetailsShippingInfoJsonElement());
				JsonArray shipmentArray = new JsonArray();
				JsonObject shipmentObject = new JsonObject();
				orderLineArrayUpdated.add(orderLineObject);
				shipmentObject.add(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), shippingInfo.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()));
				shipmentObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement(), orderLineArrayUpdated);
				shipmentArray.add(shipmentObject);
				displayObject.add(configuration.getShippingInfoShippmentArray(), shipmentArray);
				
				if (displayObject.get(configuration.getOrderDetailsGiftMessageJsonElement()) == null && giftMessage != null ) {
					displayObject.addProperty(configuration.getOrderDetailsGiftMessageJsonElement(), giftMessage);
					displayObject.addProperty(configuration.getOrderDetailsIsGiftJsonElement(), true);
				}
				displayOrderArray.add(displayObject);
				setStartDateEndDate(displayOrderArray);
				dispOrderArray.add(displayObject);
			}
			JsonObject chargeDetailsObject =  orderLineObject.getAsJsonObject(configuration.getOrderDetailsChargeDetailsJsonElement());
			String giftWrapItemID = null;
			if (chargeDetailsObject != null) {
				JsonArray chargeDetailArray = (JsonArray) chargeDetailsObject.get(configuration.getOrderDetailsChargeDetailJsonElement());
				int chargeDetailArraySize = chargeDetailArray.size();
				for (int j = TRUConstants.ZERO; j < chargeDetailArraySize; j++){
					JsonElement chargeDetailElement = chargeDetailArray.get(j);
					JsonObject chargeDetailObject = chargeDetailElement.getAsJsonObject();
					if (chargeDetailObject != null) {
						String chargeCategory = chargeDetailObject.get(configuration.getOrderDetailsChargeCategoryJsonElement()).getAsString();
						if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryShipping())) {
							if (chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()) != null && chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString().equalsIgnoreCase(configuration.getOrderDetailsChargeNameValue())) {
								shippingSurcharge = chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).
										getAsDouble();
								totalshippingSurcharge += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).
										getAsDouble();
							} else {
								chargeAmount += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
							}
						}  else if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryVS())){
							gwChargeAmount += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
						    giftWrapItemID = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
						} else if(chargeCategory.equals(configuration.getOrderDetailsChargeCategoryMisc())){
							ewasteFee += chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
						} else if (chargeCategory.equals(configuration.getOrderDetailsVasJsonElement())) {
						    chargeName = chargeDetailObject.get(configuration.getOrderDetailsChargeNameJsonElement()).getAsString();
							bppTerm = getBppTerm(chargeName);
							bppChargeAmount = chargeDetailObject.get(configuration.getOrderDetailsChargeAmountJsonElement()).getAsDouble();
						}
					}
				}
			}
			orderLineObject.addProperty(configuration.getOrderDetailsSurchargeJsonElement(), shippingSurcharge);
			orderLineObject.addProperty(configuration.getOrderDetailsBppTermJsonElement(), bppTerm);
			orderLineObject.addProperty(configuration.getOrderDetailsBppChargeAmountJsonElement(), bppChargeAmount);
			itemId = orderLineObject.get(configuration.getOrderDetailsItemIdJsonElement()).getAsString();
			String siteNameForGW = getSiteName(giftWrapItemID);
			String productPageUrl = getProductPageURL(itemId);
			orderLineObject.addProperty(configuration.getOrderDetailsProductPageUrlJsonElement(),productPageUrl);
			orderLineObject.addProperty(configuration.getOrderDetailsSiteNameForGWJsonElement(),siteNameForGW);
		}
		JsonObject orderTotalsObject = pCustomerOrder.getAsJsonObject(configuration.getOrderDetailsOrderTotalsJsonElement());
		orderTotalsObject.addProperty(configuration.getOrderDetailsShippingJsonElement(), chargeAmount);
		orderTotalsObject.addProperty(configuration.getOrderDetailsGiftwrapJsonElement(), gwChargeAmount);
		orderTotalsObject.addProperty(configuration.getOrderDetailsEwasteJsonElement(), ewasteFee);
		orderTotalsObject.addProperty(configuration.getOrderDetailsTotalShippingSurchargeJsonElement(), totalshippingSurcharge);
		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedShippingJsonElement(), convertToString(chargeAmount));
		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedGiftwrapJsonElement(), convertToString(gwChargeAmount));
		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedEwasteJsonElement(), convertToString(ewasteFee));
		orderTotalsObject.addProperty(configuration.getOrderDetailsFormattedTotalShippingSurchargeJsonElement(),convertToString(totalshippingSurcharge));
		displayOrder.add(configuration.getOrderDetailsShippingInfoArrayJsonElement(), displayOrderArray);
		int displayOrderArraySize = displayOrderArray.size();
		setShipViaStartEndDate(displayOrderArray);
		setPickUpDetails(displayOrderArray,Boolean.FALSE);
		JsonArray putDonationItemToLastObject = putDonationItemToLastObject(displayOrderArray);
		displayOrder.remove(configuration.getOrderDetailsShippingInfoArrayJsonElement());
		displayOrder.add(configuration.getOrderDetailsShippingInfoArrayJsonElement(), putDonationItemToLastObject);
		pCustomerOrder.add(configuration.getOrderDetailsDisplayOrderJsonElement(), displayOrder);
		for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
			long orderedQuantity = TRUConstants.LONG_ZERO;
			JsonElement jsonElementUpdated = displayOrderArray.get(z); 
			JsonObject orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
				JsonArray shipmentArray = orderLineObjectUpdated.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
				JsonObject shipInfoJsonObj = orderLineObjectUpdated.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
				for(int k = TRUConstants.ZERO; k < shipmentArray.size(); k++){
					JsonElement shippmentJson = shipmentArray.get(k);
					JsonObject shippmentJsonObj = shippmentJson.getAsJsonObject();
					JsonArray orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray();
				for (int m = TRUConstants.ZERO; m < orderlineArray.size(); m++) {
					JsonElement orderLineJson = orderlineArray.get(m);
					JsonObject orderLindeJsonObj = orderLineJson.getAsJsonObject();
					orderedQuantity += orderLindeJsonObj.get(configuration.getOrderDetailsOrderedQuantityPropertyName()).getAsLong();
					}
				}
				shipInfoJsonObj.addProperty(configuration.getOrderDetailsItemCountJsonElement(), String.valueOf(orderedQuantity));
		}
		pCustomerOrder.addProperty(configuration.getOrderDetailsTotalQuantityPropertyName(), totalQuantity);
		pCustomerOrder.addProperty(configuration.getOrderDetailsIscartonAvailableJsonElement(), false);
		pCustomerOrder.remove(configuration.getOrderDetailsShipmentDetailsJsonElement());
		pCustomerOrder.remove(configuration.getOrderDetailsShippingInfoJsonElement());
		pCustomerOrder.remove(configuration.getOrderDetailsOrderLinesJsonElement());
		pCustomerOrder.addProperty(configuration.getOrderDetailsHelpAndReturnsJsonElement(), 
				configuration.getOrderDetailsHelpAndReturnsURLJsonElement());
		pCustomerOrder.add(configuration.getOrderDetailsDisplayOrderJsonElement(), displayOrder);
		Gson gson = new Gson();
		formatedJsonString = gson.toJson(pOrderDetailsJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeForNonCartonItem]");
		}
		return formatedJsonString;
	}

	/**
	 * Sets the pick up details.
	 *
	 * @param pDisplayOrderArray the new pick up details
	 * @param pIsCarton 
	 */
	private void setPickUpDetails(JsonArray pDisplayOrderArray, Boolean pIsCarton) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
		TRUOMSConfiguration configuration=getOmsConfiguration();
   		JsonArray displayOrderArray= pDisplayOrderArray;
   		JsonArray shipmentArray = null;
   		JsonArray orderlineArray = null;
   		int displayOrderArraySize = displayOrderArray.size(); 
   		StringBuffer PrimaryName =  new StringBuffer();
   		StringBuffer ProxyName =  new StringBuffer();
   		for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
   			JsonElement displayOrderElement = displayOrderArray.get(z); 
   			JsonObject displayOrderObject = displayOrderElement.getAsJsonObject();
   			if(pIsCarton) {
   				 shipmentArray = displayOrderObject.get(configuration.getOrderDetailsCartonsJsonElement()).getAsJsonArray();
   			} else {
   				 shipmentArray = displayOrderObject.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
   			}
   			for(int k = TRUConstants.ZERO; k < shipmentArray.size(); k++){
   				JsonElement shippmentElement = shipmentArray.get(k);
   				JsonObject shippmentJsonObj = shippmentElement.getAsJsonObject();
   				if(pIsCarton) {
   					 orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsOrderLinesJsonElement()).getAsJsonArray();
   				} else {
   					orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray();
   				}
   			for (int m = TRUConstants.ZERO; m < orderlineArray.size(); m++) {
   				JsonElement orderLineElement = orderlineArray.get(m);
   				JsonObject orderLindeJsonObj = orderLineElement.getAsJsonObject();
   				if (orderLindeJsonObj.get(configuration.getOrderDetailsCustomAttributeListJsonElement()) != null) {
   					JsonArray customAttributeList = orderLindeJsonObj.get(configuration.getOrderDetailsCustomAttributeListJsonElement()).getAsJsonArray();
   	   				if (customAttributeList != null) {
   	   					for (int a = TRUConstants.ZERO; a < customAttributeList.size(); a++) {
   	   		   				JsonElement customAttributeElement = customAttributeList.get(a);
   	   		   				JsonObject customAttributeObj = customAttributeElement.getAsJsonObject();
   	   		   				String name = customAttributeObj.get(configuration.getOrderDetailsNameJsonElement()).getAsString();
   	   		   				if (!StringUtils.isEmpty(name) && name.equalsIgnoreCase(configuration.getPrimaryFirstName())) {
   	   		   				String PrimaryFirstName = customAttributeObj.get(configuration.getOrderDetailsValueJsonElement()).getAsString();
   	   		   				if (!StringUtils.isEmpty(PrimaryFirstName)) {
   	   		   						PrimaryName.append(PrimaryFirstName);
   	   		   					}
   	   		   				}
   	   		   				if (!StringUtils.isEmpty(name) && name.equalsIgnoreCase(configuration.getPrimaryLastName())) {
   	   		   				String PrimaryLastName = customAttributeObj.get(configuration.getOrderDetailsValueJsonElement()).getAsString();
   	   		   				if (!StringUtils.isEmpty(PrimaryLastName)) {
   	   		   						PrimaryName.append(TRUOMSConstant.SPACE_STRING).append(PrimaryLastName);
   	   		   					}
   	   		   				}
   	   		   				if (!StringUtils.isEmpty(name) && name.equalsIgnoreCase(configuration.getProxyFirstName())) {
   	   		   				String ProxyFirstName = customAttributeObj.get(configuration.getOrderDetailsValueJsonElement()).getAsString();
   	   		   				if (!StringUtils.isEmpty(ProxyFirstName)) {
   	   		   						ProxyName.append(ProxyFirstName);
   	   		   					}
   	   		   				}
   	   		   				if (!StringUtils.isEmpty(name) && name.equalsIgnoreCase(configuration.getProxyLastName())) {
   	   		   				String ProxyLastName = customAttributeObj.get(configuration.getOrderDetailsValueJsonElement()).getAsString();
   	   		   				if (!StringUtils.isEmpty(ProxyLastName)) {
   	   		   						ProxyName.append(TRUOMSConstant.SPACE_STRING).append(ProxyLastName);
   	   		   					}
   	   		   				}
   	   					}
   	   					displayOrderObject.addProperty(configuration.getOrderDetailsPrimaryNamesJsonElement(), PrimaryName.toString());
   	   					displayOrderObject.addProperty(configuration.getOrderDetailsProxyNamesJsonElement(), ProxyName.toString());
   	   		   			PrimaryName.setLength(TRUConstants.ZERO);
   	   		   			ProxyName.setLength(TRUConstants.ZERO);
   	   				}
   				}
   			}
   		}
   	}

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
	}

	/**
	 * Gets the start date.
	 *
	 * @param pOrderLineObject the order line object
	 * @return the start date
	 */
	private String getStartDate(JsonObject pOrderLineObject) {

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
		TRUOMSConfiguration configuration=getOmsConfiguration();
		JsonObject referenceField = null;
		StringBuffer referenceField3 = new StringBuffer();
		String startDate =TRUOMSConstant.EMPTY_STRING;
		Date date = null;
		try{
		Format formattedDate = new SimpleDateFormat(TRUOMSConstant.DATE_FORMAT,Locale.US);
		Format referenceDateFormat = new SimpleDateFormat(TRUOMSConstant.EXPECTED_DATE_FORMAT,Locale.US);
		JsonObject orderLineObject= pOrderLineObject;
		referenceField = orderLineObject.getAsJsonObject(configuration.getOrderDetailsReferenceFields());
		if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
				referenceField3.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
		}
		else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
			referenceField3.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
		}
		if(referenceField3.length()>TRUOMSConstant.INT_ZERO){
				date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField3.toString());
				startDate = formattedDate.format(date);
		}
		referenceField3.setLength(TRUCommerceConstants.INT_ZERO);
		} catch (ParseException parseException) {
			if(isLoggingError()){
				logError(TRUOMSConstant.PARSE_EXCEPTION, parseException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
		return startDate;
	}
	
	
	/**
	 * Gets the end date.
	 *
	 * @param pOrderLineObject the order line object
	 * @return the end date
	 */
	private String getEndDate(JsonObject pOrderLineObject) {

		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getEndDate]");
		}
		TRUOMSConfiguration configuration=getOmsConfiguration();
		JsonObject referenceField = null;
		StringBuffer referenceField4 = new StringBuffer();
		String endDate =TRUOMSConstant.EMPTY_STRING;
		Date date = null;
		try{
		Format formattedDate = new SimpleDateFormat(TRUOMSConstant.DATE_FORMAT,Locale.US);
		Format referenceDateFormat = new SimpleDateFormat(TRUOMSConstant.EXPECTED_DATE_FORMAT,Locale.US);
		JsonObject orderLineObject= pOrderLineObject;
		referenceField = orderLineObject.getAsJsonObject(configuration.getOrderDetailsReferenceFields());
		if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
			referenceField4.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
		}
		else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
		    referenceField4.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
		}
			if (referenceField4.length() > TRUOMSConstant.INT_ZERO) {
				date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField4.toString());
				endDate = formattedDate.format(date);
		}
		referenceField4.setLength(TRUCommerceConstants.INT_ZERO);
		} catch (ParseException parseException) {
			if(isLoggingError()){
				logError(TRUOMSConstant.PARSE_EXCEPTION, parseException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getEndDate]");
		}
		return endDate;
	}

	/**
	 * Sets the start date end date.
	 *
	 * @param pDisplayOrderArray the new start date end date
	 */
	private void setStartDateEndDate(JsonArray pDisplayOrderArray) {
	   		if (isLoggingDebug()) {
	   			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setStartEndDate]");
	   		}
	   		TRUOMSConfiguration configuration=getOmsConfiguration();
	   		JsonObject referenceField = null;
	   		StringBuffer referenceField3 = new StringBuffer();
	   		StringBuffer referenceField4 = new StringBuffer();
	   		Date date=null;
	   		String startDate =TRUOMSConstant.EMPTY_STRING;
	   		String endDate =TRUOMSConstant.EMPTY_STRING;
	   		Format formattedDate = new SimpleDateFormat(TRUOMSConstant.DATE_FORMAT,Locale.US);
	   		Format referenceDateFormat = new SimpleDateFormat(TRUOMSConstant.EXPECTED_DATE_FORMAT,Locale.US);
	   		try {
	   		JsonArray displayOrderArray= pDisplayOrderArray;
	   		int displayOrderArraySize = displayOrderArray.size(); 
	   		for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
	   			JsonElement jsonElementUpdated = displayOrderArray.get(z); 
	   			JsonObject orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
	   			JsonArray shipmentArray = orderLineObjectUpdated.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
	   			for(int k = TRUConstants.ZERO; k < shipmentArray.size(); k++){
	   				JsonElement shippmentJson = shipmentArray.get(k);
	   				JsonObject shippmentJsonObj = shippmentJson.getAsJsonObject();
	   				JsonArray orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray();
	   			for (int m = TRUConstants.ZERO; m < orderlineArray.size(); m++) {
	   				JsonElement orderLineJson = orderlineArray.get(m);
	   				JsonObject orderLindeJsonObj = orderLineJson.getAsJsonObject();
	   				referenceField = orderLindeJsonObj.getAsJsonObject(configuration.getOrderDetailsReferenceFields());
	   				if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
	   					referenceField3.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
	   				}
	   				else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
	   					referenceField3.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
	   				}
	   				if(referenceField3.length()>TRUOMSConstant.INT_ZERO){
	   					date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField3.toString());
	   					startDate = formattedDate.format(date);
	   					shippmentJsonObj.addProperty(configuration.getOrderDetailsStartDate(), startDate);
	   				}else{
	   					shippmentJsonObj.addProperty(configuration.getOrderDetailsStartDate(), TRUOMSConstant.EMPTY_STRING);
	   				}
	   				if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
	   				referenceField4.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
	   				}
	   				else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
	   					referenceField4.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
	   				}
	   				if (referenceField4.length() > TRUOMSConstant.INT_ZERO) {
	   					date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField4.toString());
	   					endDate = formattedDate.format(date);
	   					shippmentJsonObj.addProperty(configuration.getOrderDetailsEndDate(), endDate);
	   				}else{
	   					shippmentJsonObj.addProperty(configuration.getOrderDetailsEndDate(), TRUOMSConstant.EMPTY_STRING);
	   				}
	   			}
	   		}
	   			referenceField3.setLength(TRUCommerceConstants.INT_ZERO);
		   		referenceField4.setLength(TRUCommerceConstants.INT_ZERO);
	   	}
	   		} catch (ParseException parseException) {
	   			if(isLoggingError()){
	   				logError(TRUOMSConstant.PARSE_EXCEPTION, parseException);
	   			}
	   		}
	   		if (isLoggingDebug()) {
	   			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setStartEndDate]");
	   		}
	}

		/**
       * Update ship via with note type.
       *
       * @param pOrderLineObject the order line object
       */
       private void updateShipViaWithNoteType(JsonObject pOrderLineObject) {
              if (isLoggingDebug()) {
                     logDebug("Enter into [Class: TRUBaseOMSUtils  method: updateShipViaWithNoteType]");
              }
              TRUOMSConfiguration configuration = getOmsConfiguration();
              JsonArray noteArray = null;
              JsonObject shippingInfo = null;
              JsonObject notes = pOrderLineObject.getAsJsonObject(configuration.getOrderDetailsNotesJsonElement());
              if (pOrderLineObject != null && notes != null) {
                     noteArray = (JsonArray) notes.get(configuration.getOrderDetailsNoteJsonElement());
                     int noteSize = noteArray.size();
                     for (int j = TRUConstants.ZERO; j < noteSize; j++) {
                           JsonElement noteElement = noteArray.get(j);
                           JsonObject note = noteElement.getAsJsonObject();
                           if (note != null) {
                                  String noteType = note.get(configuration.getOrderDetailsNoteTypeJsonElement()).getAsString();
                                  if (!StringUtils.isEmpty(noteType) && noteType.equalsIgnoreCase(configuration.getOrderLinesNoteNoteTypeFreight())) {
                                         shippingInfo = pOrderLineObject.get(configuration.getOrderDetailsShippingInfoJsonElement()).getAsJsonObject();
                                         shippingInfo.remove(configuration.getOrderDetailsShippingInfoShipViaJsonElement());
                                         shippingInfo.addProperty(configuration.getOrderDetailsShippingInfoShipViaJsonElement(), configuration.getOrderLinesNoteNoteTypeFreight());
                                  }
                           }
                     }
              }
              if (isLoggingDebug()) {
                     logDebug("Exit from [Class: TRUBaseOMSUtils  method: updateShipViaWithNoteType]");
              }
       }


	/**
	 * Sets the ship via start end date.
	 *
	 * @param pDisplayOrderArray the new ship via start end date
	 */
	private void setShipViaStartEndDate(JsonArray pDisplayOrderArray) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
		TRUOMSConfiguration configuration=getOmsConfiguration();
		String shipVia= null;
		String noteType=null;
		String noteText=null;
		JsonObject noteObject = null;
		JsonArray noteArray = null;
		JsonObject referenceField = null;
		StringBuffer referenceField3 = new StringBuffer();
   		StringBuffer referenceField4 = new StringBuffer();
   		String startDate =TRUOMSConstant.EMPTY_STRING;
   		String endDate =TRUOMSConstant.EMPTY_STRING;
		Date date=null;
		int shipmentItemCount=TRUOMSConstant.INT_ZERO;
		String[] shipViaDisplay=null;
		Format formattedDate = new SimpleDateFormat(TRUOMSConstant.DATE_FORMAT,Locale.US);
		Format referenceDateFormat = new SimpleDateFormat(TRUOMSConstant.EXPECTED_DATE_FORMAT,Locale.US);
		try {
		JsonArray displayOrderArray= pDisplayOrderArray;
		int displayOrderArraySize = displayOrderArray.size(); 
		for (int z = TRUConstants.ZERO; z < displayOrderArraySize; z++) {
			JsonElement jsonElementUpdated = displayOrderArray.get(z); 
			JsonObject orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
			shipmentItemCount=TRUOMSConstant.INT_ZERO;
			JsonArray shipmentArray = orderLineObjectUpdated.get(configuration.getShippingInfoShippmentArray()).getAsJsonArray();
				for(int k = TRUConstants.ZERO; k < shipmentArray.size(); k++){
					JsonElement shippmentJson = shipmentArray.get(k);
					JsonObject shippmentJsonObj = shippmentJson.getAsJsonObject();
					JsonArray orderlineArray = shippmentJsonObj.get(configuration.getOrderDetailsCustomerOrderLineJsonElement()).getAsJsonArray();
				for (int m = TRUConstants.ZERO; m < orderlineArray.size(); m++) {
					JsonElement orderLineJson = orderlineArray.get(m);
					JsonObject orderLindeJsonObj = orderLineJson.getAsJsonObject();
					JsonObject notes = orderLindeJsonObj.getAsJsonObject(configuration.getOrderDetailsNotesJsonElement());
					shipVia= shippmentJsonObj.get(configuration.getOrderDetailsShippingInfoShipViaJsonElement()).getAsString();
					referenceField = orderLindeJsonObj.getAsJsonObject(configuration.getOrderDetailsReferenceFields());
					if (orderLindeJsonObj != null && notes != null) { 
						if (notes.get(configuration.getOrderDetailsNoteJsonElement()) instanceof JsonObject) {
							noteObject = notes.getAsJsonObject(configuration.getOrderDetailsNoteJsonElement());
							noteArray = new JsonArray();
							noteArray.add(noteObject);
							notes.remove(configuration.getOrderDetailsNoteJsonElement());
							notes.add(configuration.getOrderDetailsNoteJsonElement(), noteArray);
						} else {
							noteArray = (JsonArray) notes.get(configuration.getOrderDetailsNoteJsonElement());
						}
						int noteSize = noteArray.size();
						for (int j = TRUConstants.ZERO; j < noteSize; j++) {
							JsonElement noteElement = noteArray.get(j);
							JsonObject note = noteElement.getAsJsonObject();
							if (note != null) {
								 noteType = note.get(configuration.getOrderDetailsNoteTypeJsonElement()).getAsString();
							}
							if(!StringUtils.isEmpty(noteType) && noteType.equalsIgnoreCase(shipVia) && note != null){
								noteText = note.get(configuration.getOrderDetailsNoteTextJsonElement()).getAsString();
								shipViaDisplay=noteText.split(TRUOMSConstant.COLON); 
							}
						}
					}
					
					if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
	   					referenceField3.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
					}
	   				else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
	   					referenceField3.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField3()).getAsString());
					}
	   				if(referenceField3.length()>TRUOMSConstant.INT_ZERO && shippmentJsonObj.get(configuration.getOrderDetailsStartDate())==null){
	   					date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField3.toString());
	   					startDate = formattedDate.format(date);
						shippmentJsonObj.addProperty(configuration.getOrderDetailsStartDate(), startDate);
	   				}else{
	   					shippmentJsonObj.addProperty(configuration.getOrderDetailsStartDate(), TRUOMSConstant.EMPTY_STRING);
					}
	   				if(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_SEVEN){
		   				referenceField4.append(TRUOMSConstant.INT_ZERO).append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
		   				}
		   			else if (referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString().length() == TRUOMSConstant.INT_EIGHT) {
		   					referenceField4.append(referenceField.get(configuration.getOrderDetailsReferenceFieldsReferenceField4()).getAsString());
		   				}
		   			if (referenceField4.length() > TRUOMSConstant.INT_ZERO && shippmentJsonObj.get(configuration.getOrderDetailsEndDate())==null ) {
		   					date = (Date) ((DateFormat) referenceDateFormat).parse(referenceField4.toString());
		   					endDate = formattedDate.format(date);
						shippmentJsonObj.addProperty(configuration.getOrderDetailsEndDate(), endDate);
		   			}else{
		   					shippmentJsonObj.addProperty(configuration.getOrderDetailsEndDate(), TRUOMSConstant.EMPTY_STRING);
					}
				}
				shipmentItemCount++;
				shippmentJsonObj.addProperty(configuration.getShippingInfoItemTotalCountJsonElement(), shipmentArray.size());
				shippmentJsonObj.addProperty(configuration.getOrderDetailsShipmentItemCountJsonElement(),shipmentItemCount);
				referenceField3.setLength(TRUCommerceConstants.INT_ZERO);
				referenceField4.setLength(TRUCommerceConstants.INT_ZERO);
				}
			}
		} catch (ParseException parseException) {
			if(isLoggingError()){
				logError(TRUOMSConstant.PARSE_EXCEPTION, parseException);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setShipViaStartEndDate]");
		}
	}

	/**
	 * This method will return Gift Message for notes.
	 *
	 * @param pOrderLineObject order line object
	 * @return giftMessage
	 */
	private String getGiftMessage(JsonObject pOrderLineObject) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getGiftMessage]");
		}
		String giftMessage = null;
		TRUOMSConfiguration configuration = getOmsConfiguration();
		JsonObject noteObject = null;
		JsonArray noteArray = null;
		JsonObject notes = pOrderLineObject.getAsJsonObject(configuration.getOrderDetailsNotesJsonElement());
		if (pOrderLineObject != null && notes != null) {
			if (notes.get(configuration.getOrderDetailsNoteJsonElement()) instanceof JsonObject) {
				noteObject = notes.getAsJsonObject(configuration.getOrderDetailsNoteJsonElement());
				noteArray = new JsonArray();
				noteArray.add(noteObject);
				notes.remove(configuration.getOrderDetailsNoteJsonElement());
				notes.add(configuration.getOrderDetailsNoteJsonElement(), noteArray);
			} else {
				noteArray = (JsonArray) notes.get(configuration.getOrderDetailsNoteJsonElement());
			}
			int noteSize = noteArray.size();
			for (int j = TRUConstants.ZERO; j < noteSize; j++) {
				JsonElement noteElement = noteArray.get(j);
				JsonObject note = noteElement.getAsJsonObject();
				if (note != null) {
					String noteType = note.get(configuration.getOrderDetailsNoteTypeJsonElement()).getAsString();
					if(noteType.equals(configuration.getGiftMessageNoteType())){
						giftMessage = note.get(configuration.getOrderDetailsNoteTextJsonElement()).getAsString();
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getGiftMessage]");
		}
		return giftMessage;
		
	}

	/**
	 * Convert to string.
	 *
	 * @param pDoubleValue DoubleValue
	 * @return the string
	 */
	private String convertToString(double pDoubleValue) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: convertToString]");
		}
		StringBuffer appendedAvailDetail = new StringBuffer();
		String dollarValue = TRUConstants.DOLLER; 
		DecimalFormat df = new DecimalFormat(TRUConstants.DOUBLE_DECIMAL);
		String stringValue = df.format(pDoubleValue);
		appendedAvailDetail.append(dollarValue).append(stringValue);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: convertToString]");
		}
		return appendedAvailDetail.toString();
	}

	/**
	 * This method is to return BPP term value.
	 * @param pBppItemId  - bpp item id
	 * @return bppTerm - bpp term value
	 */
	private String getBppTerm(String pBppItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getBppTerm]");
		}
		String bppTerm = null;
		if (!StringUtils.isBlank(pBppItemId)) {
			Repository catalog = (Repository) getCatalogRepository();
			if (catalog != null) {
				try {
					RepositoryItem bppItem = (RepositoryItem) catalog.getItem(pBppItemId,getCatalogTools().getBppDescriptorName());
					if (bppItem != null) {
						bppTerm = (String) bppItem.getPropertyValue(getOmsConfiguration().getOrderDetailsTermPropertyName());
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError(
								"Repository Exception in TRUCatalogTools @method: getBppItemsBasedOnBppName()",
								e);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getBppTerm]");
		}
		return bppTerm;
	}

	/**
	 * This method is to return the gift wrap name.
	 * @param pItemId  - item Id
	 * @return giftWrapName - gift wrap name
	 */
	private String getSiteName(String pItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getSiteName]");
		}
		RepositoryItem skuItem = null;
		String giftWrapName = null;
		if (!StringUtils.isBlank(pItemId)) {
			try {
				skuItem = getCatalogTools().findSKU(pItemId);
				if (skuItem != null) {
					 giftWrapName = (String) skuItem.getPropertyValue(getOmsConfiguration().getOrderDetailsDisplayNamePropertyName());
				}
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting promo status items : {0}", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getSiteName]");
		}
		return giftWrapName;
	}

	/**
	 * This method is used to get the onlinePId from sku item to construct PDP url. 
	 * @param pItemId - item id
	 * @return ProductPageUrl -  product url
	 */
	private String getProductPageURL(String pItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getProductPageURL]");
		}
		String onlinePID = null;
		String ProductPageUrl = null;
		RepositoryItem skuItem = null;
		if (!StringUtils.isBlank(pItemId)) {
			try {
				 skuItem = getCatalogTools().findSKU(pItemId);
				 if(skuItem != null && skuItem.getPropertyValue(getOmsConfiguration().getOnlinePID()) != null) {
					 onlinePID = (String) skuItem.getPropertyValue(getOmsConfiguration().getOnlinePID());
					 ProductPageUrl = getProductPageUtil().constructPDPURLWithOnlinePID(onlinePID,TRUOMSConstant.TOYS_R_US);
					}
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting promo status items : {0}", exc);
				}
			} 
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getProductPageURL]");
		}
		return ProductPageUrl;
	}
	
	/**
	 * This method is used to check if response as the error code.
	 * @param pCustomerOrderDetailJSONResponse - String
	 * @param pOrderHistoryPage - order history page
	 * @return : boolean
	 */
	public boolean checkErrorMessageTag(String pCustomerOrderDetailJSONResponse, boolean pOrderHistoryPage) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getProductPageURL]");
		}
		JsonObject customerOrderJsonObject = null;
		boolean errorMessageNode = Boolean.FALSE;
		boolean responceStatus = Boolean.TRUE;
		JsonParser parser = new JsonParser();
		JsonElement jsonParser = parser.parse(pCustomerOrderDetailJSONResponse);
		JsonObject asJsonObject = jsonParser.getAsJsonObject();
		if (pOrderHistoryPage) {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getCustomerTransactionListResponseJsonElement());
			 JsonElement totalNoOfRecorders = customerOrderJsonObject.get(getOmsConfiguration().getOrderHistoryTotalNoOfRecordsJsonElement());
			 if (totalNoOfRecorders != null) {
				 long totalNoOfRecorder = totalNoOfRecorders.getAsLong();
				 if (totalNoOfRecorder == TRUConstants.ZERO) {
					 return Boolean.TRUE;
				 }
			 } 
		} else {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getOrderDetailsCustomerOrderJsonElement());
		}
		JsonElement jsonMessgaeObject = customerOrderJsonObject.get(TRUOMSConstant.MESSAGES);
		if (customerOrderJsonObject.get(TRUOMSConstant.RESPONSE_STATUS) != null) {
			responceStatus  = customerOrderJsonObject.get(TRUOMSConstant.RESPONSE_STATUS).getAsBoolean();
		}
		if (jsonMessgaeObject != null || !responceStatus) {
			return Boolean.TRUE;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getProductPageURL]");
		}
		return errorMessageNode;
	}
	
	/**
	 * This method is to generate the payment dates from JSON for layaway orders.
	 * @param pResponse  - Layaway JSON response
	 * @return json - JSON updated
	 */
	public String getPaymentDetailsForLayawayOrder(String pResponse){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
		JsonObject customerOrderMain = null;
		Gson gson = new Gson();
		String json = null;
		String[] noteTextSplitByColon = null;
		String value = null;
		JsonArray customerOrderlineArray = null;
		JsonArray customerDetailArray = null;
		Map<String, String> orderStatusMap = null;
		Date date = null;
        String formatDate = null;
        String[] dateString = null;
        SimpleDateFormat updateFormatter = null;
        JsonArray sortedCustomerOrderLineArray=new JsonArray();
        TRUOMSConfiguration configuration = getOmsConfiguration();
		if(pResponse != null){
			JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(pResponse);
			customerOrderMain = parse.getAsJsonObject();
			JsonObject customerOrder = customerOrderMain.getAsJsonObject(configuration.getOrderDetailsCustomerOrderJsonElement());
			//order status conevrt to active from order status map
			JsonElement orderStatus = customerOrder.get(getOmsConfiguration().getOrderStatusJsonElement());
			String orderStatusString = orderStatus.getAsString();
			orderStatusMap = getOmsConfiguration().getLayawayOrderStatusOHMap();
			if(orderStatusMap.containsKey(orderStatusString)){
				customerOrder.remove(getOmsConfiguration().getOrderStatusJsonElement());
				customerOrder.addProperty(getOmsConfiguration().getOrderStatusJsonElement(), orderStatusMap.get(orderStatusString));
			}
			JsonObject customerOrderNotes = customerOrder.getAsJsonObject(configuration.getOrderDetailsCustomerOrderNotesJsonElement());
			if (customerOrderNotes != null) {
				JsonElement customerOrderNote = customerOrderNotes.get(configuration.getOrderDetailsCustomerOrderNoteJsonElement());
				if (customerOrderNote instanceof JsonObject) {
					customerDetailArray =  new JsonArray();
					customerDetailArray.add(customerOrderNote);
					customerOrderNotes.remove(configuration.getOrderDetailsCustomerOrderNoteJsonElement());
					customerOrderNotes.add(configuration.getOrderDetailsCustomerOrderNoteJsonElement(), customerDetailArray);
				} else {
					customerDetailArray = (JsonArray) customerOrderNotes.get(configuration.getOrderDetailsCustomerOrderNoteJsonElement());
				}
			}
			//formating date 
            JsonElement orderCapturedDate = customerOrder.get(configuration.getOrderDetailsOrderCapturedDateJsonElement());
            String orderCapturedDateString = orderCapturedDate.getAsString();
            dateString = orderCapturedDateString.split(TRUConstants.WHITE_SPACE);
            if (!StringUtils.isBlank(dateString[TRUConstants.ZERO])) {
                  SimpleDateFormat formatter = new SimpleDateFormat(TRUConstants.RESPONCE_DATE, Locale.US);
                  updateFormatter = new SimpleDateFormat(TRUConstants.LAYAWAY_DATE_DISPLAY, Locale.US);
                  try {
                         date = formatter.parse(dateString[TRUConstants.ZERO]);
                         if (date != null) {
                                formatDate = updateFormatter.format(date);
                         }
                  } catch (ParseException e) {
                         if (isLoggingError()) {
                                vlogError("ParseException:  While getting promo status items : {0}",e);
                         }
                  }
                   customerOrder.remove(configuration.getOrderDetailsOrderCapturedDateJsonElement());
                   customerOrder.addProperty(configuration.getOrderDetailsOrderCapturedDateJsonElement(),formatDate);
            }
			JsonObject orderLinesJsonObject = customerOrder.getAsJsonObject(configuration.getOrderDetailsOrderLinesJsonElement());
			updatePaymentDetailsAsAnArray(customerOrder);
			if (orderLinesJsonObject != null) {
				JsonElement orderLine = orderLinesJsonObject.get(configuration.getOrderDetailsCustomerOrderLineJsonElement());
				if (orderLine instanceof JsonObject) {
					customerOrderlineArray = new JsonArray();
					customerOrderlineArray.add(orderLine);
					orderLinesJsonObject.remove(configuration.getOrderDetailsCustomerOrderLineJsonElement());
					orderLinesJsonObject.add(configuration.getOrderDetailsCustomerOrderLineJsonElement() , customerOrderlineArray);
				} else {
					 customerOrderlineArray = (JsonArray) orderLinesJsonObject.get(configuration.getOrderDetailsCustomerOrderLineJsonElement());
				}
			}
			int customerOrderlineArraySize =customerOrderlineArray.size();
			for (int i = TRUConstants.ZERO; i < customerOrderlineArraySize; i++){
				JsonElement coLineJsonElement = customerOrderlineArray.get(i);
				JsonObject coLineJsonObject = coLineJsonElement.getAsJsonObject();
				String itemId = coLineJsonObject.get(configuration.getOrderDetailsItemIdJsonElement()).getAsString();
				String productPageUrl = getProductPageURL(itemId);
				coLineJsonObject.addProperty(configuration.getOrderDetailsProductPageUrlJsonElement(),productPageUrl);
				if(i==TRUOMSConstant.INT_ZERO){
					JsonElement shippingInfo = coLineJsonObject.get(configuration.getOrderDetailsShippingInfoJsonElement());
					JsonObject shippingInfoJsonObject = shippingInfo.getAsJsonObject();
					String deliveryOption=shippingInfoJsonObject.get(configuration.getLayawayOrderLinedeliveryOption()).getAsString();
					customerOrder.addProperty(configuration.getLayawayOrderLinedeliveryOption(),deliveryOption);
				}
			}
			int customerOrderNoteArraySize = customerDetailArray.size();
			 sortedCustomerOrderLineArray= getSortCustomerOrderLineArray(customerDetailArray);
			JsonArray sortedCustomerLayawayPaymentOrderLineArray = new JsonArray();
			JsonObject jsonObject = null;
			for (int e = TRUConstants.ZERO; e < customerOrderNoteArraySize; e++){
				JsonElement customerOrderNoteJsonElement = sortedCustomerOrderLineArray.get(e);
				JsonObject customerOrderNoteObject = customerOrderNoteJsonElement.getAsJsonObject();
				String noteSeq = customerOrderNoteObject.get(configuration.getOrderDetailsNoteSequenceJsonElement()).getAsString();
				String noteType = customerOrderNoteObject.get(configuration.getOrderDetailsNoteTypeJsonElement()).getAsString();
				String noteText = customerOrderNoteObject.get(configuration.getOrderDetailsNoteTextJsonElement()).getAsString();
				if(noteType.equals(TRUConstants.LAYAWAY_PAYMENT)){
					jsonObject=sortedCustomerOrderLineArray.get(e).getAsJsonObject();
					String[] split	= noteText.split(TRUConstants.COMMA);
					for (int d = TRUConstants.ZERO; d < split.length; d++){
						String string = split[d].trim();
						if(string.startsWith(TRUConstants.TRANSACTION_DATE)&&noteSeq.equals(TRUOMSConstant.NUMERIC_ONE)){
							noteTextSplitByColon = string.split(TRUConstants.COLON);
							value = TRUOMSConstant.INITIAL_PAYMENT;
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsTransactionDateJsonElement(), value);
						}else if(string.startsWith(TRUConstants.TRANSACTION_DATE)){
								noteTextSplitByColon = string.split(TRUConstants.COLON);
								value = noteTextSplitByColon[TRUConstants.ONE];
								customerOrderNoteObject.addProperty(configuration.getOrderDetailsTransactionDateJsonElement(), value.trim());
						}
						if(string.startsWith(TRUConstants.TRANSACTION_TOTAL_AMOUNT)){
							noteTextSplitByColon = string.split(TRUConstants.COLON);
							value = noteTextSplitByColon[TRUConstants.ONE];
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsTransactionTotalAmountElement(), value.trim());
						}
						if(string.startsWith(TRUConstants.NEXT_PAYMENT_AMOUNT)){
							noteTextSplitByColon = string.split(TRUConstants.COLON);
							value = noteTextSplitByColon[TRUConstants.INTEGER_NUMBER_ONE];
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsNextPaymentAmountJsonElement(), value.trim());
						}
						if(string.startsWith(TRUConstants.REMAINING_AMOUNT)){
							noteTextSplitByColon = string.split(TRUConstants.COLON);
							value = noteTextSplitByColon[TRUConstants.INTEGER_NUMBER_ONE];
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsRemainingAmountJsonElement(), value.trim());
						}
						if(string.startsWith(TRUConstants.NEXT_PAYMENT_DUE)){
							noteTextSplitByColon = string.split(TRUConstants.COLON);
							value = noteTextSplitByColon[TRUConstants.INTEGER_NUMBER_ONE];
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsNextPaymentDueJsonElement(), value.trim());
						}
					}
						if (customerOrderNoteObject.get(configuration.getOrderDetailsTransactionDateJsonElement()) != null && 
								customerOrderNoteObject.get(configuration.getOrderDetailsTransactionTotalAmountElement()) != null && 
								!StringUtils.isEmpty(customerOrderNoteObject.get(configuration.
										getOrderDetailsTransactionDateJsonElement()).getAsString()) && !StringUtils.
										isEmpty(customerOrderNoteObject.get(configuration.getOrderDetailsTransactionTotalAmountElement()).
												getAsString())) {
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsIsTranctionAvailableJsonElement(), true);
						} else {
							customerOrderNoteObject.addProperty(configuration.getOrderDetailsIsTranctionAvailableJsonElement(), false);
						}
						if (e == sortedCustomerOrderLineArray.size() - TRUConstants.ONE &&
								customerOrderNoteObject.get(configuration.getOrderDetailsRemainingAmountJsonElement()) != null && 
								!StringUtils.isEmpty(customerOrderNoteObject.get(configuration.getOrderDetailsRemainingAmountJsonElement()).
										getAsString())) {
							customerOrderNotes.addProperty(configuration.getOrderDetailsIsRemainingAmountJsonElement(), true);
							customerOrderNotes.addProperty(configuration.getOrderDetailsRemainingAmountJsonElement(), 
									customerOrderNoteObject.get(configuration.getOrderDetailsRemainingAmountJsonElement()).getAsString());
							customerOrderNotes.addProperty(configuration.getOrderDetailsNextPaymentDueJsonElement(), 
									customerOrderNoteObject.get(configuration.getOrderDetailsNextPaymentDueJsonElement()).getAsString());
							customerOrderNotes.addProperty(configuration.getOrderDetailsNextPaymentAmountJsonElement(), 
									customerOrderNoteObject.get(configuration.getOrderDetailsNextPaymentAmountJsonElement()).getAsString());
						}
					String jsonString = gson.toJson(jsonObject);
					JsonElement jsonElement = pareser.parse(jsonString);
					sortedCustomerLayawayPaymentOrderLineArray.add(jsonElement);
				}
			}
			customerOrderNotes.remove(configuration.getOrderDetailsCustomerOrderNoteJsonElement());
			customerOrderNotes.add(configuration.getOrderDetailsCustomerOrderNoteJsonElement(), sortedCustomerLayawayPaymentOrderLineArray);
		}
		json = gson.toJson(customerOrderMain);   
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: rearrangeOrderDetailsJSON]");
		}
		return json;
	}
	

	/**
	 * Gets the sort customer order line array.
	 *
	 * @param pCustomerOrderlineArray the customer orderline array
	 * @return the sort customer order line array
	 */
	public JsonArray getSortCustomerOrderLineArray(JsonArray pCustomerOrderlineArray) {
		JsonParser pareser = new JsonParser();
        List<JsonObject> jsonList = new ArrayList<JsonObject>();
        for (int i = 0; i < pCustomerOrderlineArray.size(); i++) {
            JsonElement jsonElement = pCustomerOrderlineArray.get(i);
            jsonList.add(jsonElement.getAsJsonObject());
        }
        Collections.sort( jsonList, new Comparator<JsonObject>() {

            public int compare(JsonObject pObjA, JsonObject pObjB) {
            	TRUOMSConfiguration configuration = getOmsConfiguration();
                String valA = null;
                String valB = null;
                valA = pObjA.get(configuration.getOrderDetailsNoteSequenceJsonElement()).getAsString();
                valB = pObjB.get(configuration.getOrderDetailsNoteSequenceJsonElement()).getAsString();
                return valA.compareTo(valB);
            }
        });
        Gson gson = new Gson();
        String jsonString = gson.toJson(jsonList);
        JsonElement parse = pareser.parse(jsonString);
        JsonArray customerOrderNoteSorted = parse.getAsJsonArray();
        return customerOrderNoteSorted;
	}
	
	/**
	 * This method is used to update the paymentDetails as an array if its object.
	 * 
	 * @param pCustomerOrder - json object
	 */
	private void updatePaymentDetailsAsAnArray(JsonObject pCustomerOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderHistoryDroplet method : updatePaymentDetailsAsAnArray");
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		JsonElement paymentDetail = null;
		JsonArray paymentDetailArray = null;
		JsonObject paymentDetailsObject = pCustomerOrder.getAsJsonObject(configuration.getOrderDetailsPaymentDetailsJsonElement());
		if (paymentDetailsObject != null) {
			 paymentDetail = paymentDetailsObject.get(configuration.getOrderDetailsPaymentDetailJsonElement());
			if (paymentDetail instanceof JsonObject) {
				paymentDetailArray =  new JsonArray();
				paymentDetailArray.add(paymentDetail);
				paymentDetailsObject.remove(configuration.getOrderDetailsPaymentDetailJsonElement());
				paymentDetailsObject.add(configuration.getOrderDetailsPaymentDetailJsonElement(), paymentDetailArray);
			} 
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: updatePaymentDetailsAsAnArray]");
		}
	}

	/**
	 * This method is used to update the json string with the new status.
	 * 
	 * @param pCustomerOrderDetailJSONResponse - json string
	 * @param pLayawayPage - layaway flag
	 * @return pCustomerOrderDetailJSONResponse json string
	 */
	public String updateJsonObject(String pCustomerOrderDetailJSONResponse, String pLayawayPage) {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderHistoryDroplet method : updateJsonObject");
		}
		Map<String, String> orderStatusMap = null;
		Date date = null;
		String formatDate = null;
		SimpleDateFormat updateFormatter = null;
		String[] dateString = null;
		String orderCreatedDateString = null;
		JsonParser parser = new JsonParser();
		JsonArray customerOrderArray = null;
		JsonElement jsonParser = parser.parse(pCustomerOrderDetailJSONResponse);
		JsonObject asJsonObject = jsonParser.getAsJsonObject();
		JsonObject orderListJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getCustomerTransactionListResponseJsonElement());
		JsonObject customerOrdersJsonObjects = orderListJsonObject.getAsJsonObject(getOmsConfiguration().getCustomerOrdersJsonElement());
		if (customerOrdersJsonObjects != null) {
			JsonElement customerOrderJsonElement = customerOrdersJsonObjects.get(getOmsConfiguration().getCustomerOrderJsonElement());
			if (customerOrderJsonElement instanceof JsonObject) {
				customerOrderArray = new JsonArray();
				customerOrderArray.add(customerOrderJsonElement);
				customerOrdersJsonObjects.remove(getOmsConfiguration().getCustomerOrderJsonElement());
				customerOrdersJsonObjects.add(getOmsConfiguration().getCustomerOrderJsonElement(), customerOrderArray);
			} else {
				customerOrderArray = (JsonArray) customerOrdersJsonObjects.get(getOmsConfiguration().getCustomerOrderJsonElement());
			}
		}
		int customerOrderJsonObjectSize = customerOrderArray.size();
		for (int i = TRUConstants.ZERO ;i < customerOrderJsonObjectSize; i++){
			JsonElement jsonElement = customerOrderArray.get(i);
			JsonObject coJsonObject = jsonElement.getAsJsonObject();
			JsonElement orderStatus = coJsonObject.get(getOmsConfiguration().getOrderStatusJsonElement());
			String orderStatusString = orderStatus.getAsString();
			if (pLayawayPage.equalsIgnoreCase(TRUConstants.TRUE)) {
				orderStatusMap = getOmsConfiguration().getLayawayOrderStatusOHMap();
			} else {
				orderStatusMap = getOmsConfiguration().getOrderStatusOHMap();
			}
			if(orderStatusMap.containsKey(orderStatusString)){
				coJsonObject.remove(getOmsConfiguration().getOrderStatusJsonElement());
				coJsonObject.addProperty(getOmsConfiguration().getOrderStatusJsonElement(), orderStatusMap.get(orderStatusString));
			}
			//format date
			JsonElement orderCreatedDate = coJsonObject.get(getOmsConfiguration().getOrderDetailsOrderCreatedDateJsonElement());
		    orderCreatedDateString = orderCreatedDate.getAsString();
			dateString = orderCreatedDateString.split(TRUConstants.WHITE_SPACE);
			if(!StringUtils.isBlank(dateString[TRUConstants.ZERO])) {
				//formated logic here
				SimpleDateFormat formatter = new SimpleDateFormat(TRUConstants.RESPONCE_DATE,Locale.US);
				if (pLayawayPage.equalsIgnoreCase(TRUConstants.TRUE)) {
					 updateFormatter = new SimpleDateFormat(TRUConstants.LAYAWAY_DATE_DISPLAY,Locale.US);
				} else {
					 updateFormatter = new SimpleDateFormat(TRUConstants.ORDER_HISTORY_DATE_DISPLAY,Locale.US);
				}
				try {
				date = formatter.parse(dateString[TRUConstants.ZERO]);
				if (date != null) {
					formatDate = updateFormatter.format(date);
				}
				} catch (ParseException e) {
					if (isLoggingError()) {
						vlogError("ParseException:  While getting promo status items : {0}", e);
					}
				}
				coJsonObject.remove(getOmsConfiguration().getOrderDetailsOrderCreatedDateJsonElement());
				coJsonObject.addProperty(getOmsConfiguration().getOrderDetailsOrderCreatedDateJsonElement(), formatDate);
			}
		}
		Gson gson = new Gson();
		String json = gson.toJson(asJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUCustomerOrderHistoryDroplet method : updateJsonObject");
		}
		return json;
	}

	
	
	
	/**
	 * This method calls the OMS Service and returns json response.
	 *
	 * @param pUserName - userName for authentication
	 * @param pPassword - password for authentication
	 * @param pTargetURL - target OMS Service URL
	 * @param pInput - input json request to be sent to OMS
	 * @return omsJSONResponse - omsJSONResponse
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String omsOrderDetailsServiceCall(String pUserName, String pPassword, String pTargetURL, String pInput) throws IOException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: omsOrderDetailsServiceCall]");
			vlogDebug("targetURL : {0} input : {1} pUserName : {2} pPassword : {3}", pTargetURL, pInput, pUserName, pPassword);
		}

		String omsJSONResponse = null;
		// Added Performance statement 
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(TRUOMSConstant.CUSTOMER_ORDER_DETAIL_SERVICE_CALL);
		}
		String targetURL = pTargetURL+pInput;
		URL url = new URL(targetURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//String encodedAuth="Basic "+Base64.encodeBase64(authorization.getBytes());
		connection.setRequestProperty(TRUOMSConstant.AUTHORIZATION, OMS_AUTH_HEADER);
		connection.setRequestMethod(TRUOMSConstant.GET_METHOD);
		connection.setRequestProperty(TRUOMSConstant.CONTENT_TYPE, TRUOMSConstant.CONTENT_TYPE_VALUE);
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//Create an OutputStream and write the input
		//OutputStream outputStream = connection.getOutputStream();
		//outputStream.write(input.getBytes());
		//outputStream.flush();
		int responseCode = connection.getResponseCode();
		//If the response code is not 200 - OK, then throw error
		if (responseCode != TRUOMSConstant.RESPONSE_CODE_200) {
			omsJSONResponse = generateDummyErrorResponse(responseCode,connection.getResponseMessage(),Boolean.FALSE);
			return omsJSONResponse;
		}

		BufferedReader responseBuffer = new BufferedReader(
				new InputStreamReader((connection.getInputStream())));
		String output;
		while ((output = responseBuffer.readLine()) != null) {
			omsJSONResponse = output;
			if (isLoggingDebug()){
				logDebug("JSON response is " + omsJSONResponse);
			}
		}
		connection.disconnect();
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.CUSTOMER_ORDER_DETAIL_SERVICE_CALL);
	    }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: omsOrderDetailsServiceCall]");
		}
		return omsJSONResponse;
	}
	
	/**
	 * This method calls the OMS Service and returns json response.
	 *
	 * @param pUserName - userName for authentication
	 * @param pPassword - password for authentication
	 * @param pTargetURL - target OMS Service URL
	 * @param pInput - input json request to be sent to OMS
	 * @return omsJSONResponse - omsJSONResponse
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String omsOrderListServiceCall(String pUserName, String pPassword, String pTargetURL, String pInput) throws IOException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: omsOrderListServiceCall]" );
			vlogDebug("targetURL : {0} input : {1} pUserName : {2} pPassword : {3}", pTargetURL, pInput, pUserName, pPassword);
		}

		String omsJSONResponse = null;
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.startOperation(TRUOMSConstant.CUSTOMER_ORDER_LIST_SERVICE_CALL);
	    }
		URL url = new URL(pTargetURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//String encodedAuth="Basic "+Base64.encodeBase64(authorization.getBytes());
		connection.setRequestProperty(TRUOMSConstant.AUTHORIZATION, OMS_AUTH_HEADER);
		connection.setRequestMethod(TRUOMSConstant.GET_METHOD);
		connection.setRequestProperty(TRUOMSConstant.CONTENT_TYPE, TRUOMSConstant.CONTENT_TYPE_VALUE);
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//Create an OutputStream and write the input
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(pInput.getBytes());
		outputStream.flush();
		int responseCode = connection.getResponseCode();
		//If the response code is not 200 - OK, then throw error
		if (responseCode != TRUOMSConstant.RESPONSE_CODE_200) {
			omsJSONResponse = generateDummyErrorResponse(responseCode,connection.getResponseMessage(),Boolean.TRUE);
			return omsJSONResponse;
		}

		BufferedReader responseBuffer = new BufferedReader(
				new InputStreamReader((connection.getInputStream())));
		String output;
		while ((output = responseBuffer.readLine()) != null) {
			omsJSONResponse = output;
			if (isLoggingDebug()){
				logDebug("JSON response is " + omsJSONResponse);
			}
		}
		connection.disconnect();
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.CUSTOMER_ORDER_LIST_SERVICE_CALL);
	    }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: omsOrderListServiceCall]");
		}
		return omsJSONResponse;
	}
	
	/**
	 * This method calls the OMS Service and returns json response for cancel order.
	 *
	 * @param pUserName - userName for authentication
	 * @param pPassword - password for authentication
	 * @param pTargetURL - target OMS Service URL
	 * @param pInput - input json request to be sent to OMS
	 * @return omsJSONResponse - omsJSONResponse
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String omsCancelOrderServiceCall(String pUserName, String pPassword, String pTargetURL, String pInput) throws IOException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: omsOrderListServiceCall]" );
			vlogDebug("targetURL : {0} input : {1} pUserName : {2} pPassword : {3}", pTargetURL, pInput, pUserName, pPassword);
		}

		String omsJSONResponse = null;
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.startOperation(TRUOMSConstant.CUSTOMER_ORDER_LIST_SERVICE_CALL);
	    }
		URL url = new URL(pTargetURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//String encodedAuth="Basic "+Base64.encodeBase64(authorization.getBytes());
		connection.setRequestProperty(TRUOMSConstant.AUTHORIZATION, OMS_AUTH_HEADER);
		connection.setRequestMethod(TRUOMSConstant.GET_METHOD);
		connection.setRequestProperty(TRUOMSConstant.CONTENT_TYPE, TRUOMSConstant.CONTENT_TYPE_VALUE);
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//Create an OutputStream and write the input
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(pInput.getBytes());
		outputStream.flush();
		int responseCode = connection.getResponseCode();
		//If the response code is not 200 - OK, then throw error
		if (responseCode != TRUOMSConstant.RESPONSE_CODE_200) {
			omsJSONResponse = generateDummyErrorResponse(responseCode,connection.getResponseMessage(),Boolean.TRUE);
			return omsJSONResponse;
		}

		BufferedReader responseBuffer = new BufferedReader(
				new InputStreamReader((connection.getInputStream())));
		String output;
		while ((output = responseBuffer.readLine()) != null) {
			omsJSONResponse = output;
			if (isLoggingDebug()){
				logDebug("JSON response is " + omsJSONResponse);
			}
		}
		connection.disconnect();
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.CUSTOMER_ORDER_LIST_SERVICE_CALL);
	    }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: omsOrderListServiceCall]");
		}
		return omsJSONResponse;
	}
	
	/**
	 * This method will return the dummy response with updated response code and message.
	 * @param pResponseCode - response code 
	 * @param pResponseMessage - response message
	 * @param pOrderHistoryPage - boolean flag
	 * @return omsJSONResponse - omsJSONResponse
	 */
	private String generateDummyErrorResponse(int pResponseCode, String pResponseMessage, boolean pOrderHistoryPage) {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderHistoryDroplet method : generateDummyErrorResponse");
		}
		String errorResponse = null;
		if (pOrderHistoryPage) {
			 errorResponse = getOmsConfiguration().getOrderHistoryErrorMessageString();
		} else {
			errorResponse = getOmsConfiguration().getOrderDetailsErrorMessageString();
		}
		JsonObject customerOrderJsonObject = null;
		JsonParser parser = new JsonParser();
		JsonElement jsonParser = parser.parse(errorResponse);
		JsonObject asJsonObject = jsonParser.getAsJsonObject();
		if (pOrderHistoryPage) {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getCustomerTransactionListResponseJsonElement());
		} else {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getOrderDetailsCustomerOrderJsonElement());
		}
		JsonObject jsonMessagesObject = (JsonObject) customerOrderJsonObject.get(TRUOMSConstant.MESSAGES);
		JsonObject jsonMessageObject = jsonMessagesObject.getAsJsonObject(TRUOMSConstant.MESSAGE);
		if (jsonMessageObject != null) {
			jsonMessageObject.remove(getOmsConfiguration().getCodeJsonElement());
			jsonMessageObject.remove(getOmsConfiguration().getDescriptionJsonElement());
			jsonMessageObject.addProperty(getOmsConfiguration().getCodeJsonElement(), pResponseCode);
			jsonMessageObject.addProperty(getOmsConfiguration().getDescriptionJsonElement(), pResponseMessage);
		}
		Gson gson = new Gson();
		String omsJSONResponse = gson.toJson(asJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUCustomerOrderHistoryDroplet method : generateDummyErrorResponse");
		}
		return omsJSONResponse;
	}
	
	/**
	 * This method will return the dummy response with updated response code and message.
	 * @param pResponseCode - response code 
	 * @param pResponseMessage - response message
	 * @param pCustomerFlag - boolean flag
	 * @return omsJSONResponse - omsJSONResponse
	 */
	private String generateDummyErrorResponseForCMAndCO(int pResponseCode, String pResponseMessage, boolean pCustomerFlag) {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderHistoryDroplet method : generateDummyErrorResponse");
		}
		String errorResponse = null;
		if (pCustomerFlag) {
			 errorResponse = getOmsConfiguration().getCustomerMasterErrorMessageString();
		} else {
			errorResponse = getOmsConfiguration().getCustomerOrderErrorMessageString();
		}
		JsonObject customerOrderJsonObject = null;
		JsonParser parser = new JsonParser();
		JsonElement jsonParser = parser.parse(errorResponse);
		JsonObject asJsonObject = jsonParser.getAsJsonObject();
		if (pCustomerFlag) {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getCustomerFullDetailsJsonElement());
		} else {
			 customerOrderJsonObject = asJsonObject.getAsJsonObject(getOmsConfiguration().getOrderDetailsCustomerOrderJsonElement());
		}
		JsonObject jsonMessagesObject = (JsonObject) customerOrderJsonObject.get(TRUOMSConstant.MESSAGES);
		JsonObject jsonMessageObject = jsonMessagesObject.getAsJsonObject(TRUOMSConstant.MESSAGE);
		if (jsonMessageObject != null) {
			jsonMessageObject.remove(getOmsConfiguration().getCodeJsonElement());
			jsonMessageObject.remove(getOmsConfiguration().getDescriptionJsonElement());
			jsonMessageObject.addProperty(getOmsConfiguration().getCodeJsonElement(), pResponseCode);
			jsonMessageObject.addProperty(getOmsConfiguration().getDescriptionJsonElement(), pResponseMessage);
		}
		Gson gson = new Gson();
		String omsJSONResponse = gson.toJson(asJsonObject);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUCustomerOrderHistoryDroplet method : generateDummyErrorResponse");
		}
		return omsJSONResponse;
	}
	
	
	
	
	/**
	 * This method calls the OMS Service and returns json response.
	 *
	 * @param pUserName - userName for authentication
	 * @param pPassword - password for authentication
	 * @param pTargetURL - target OMS Service URL
	 * @param pInput - input json request to be sent to OMS
	 * @return omsJSONResponse - omsJSONResponse
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String omsCustomerMasterServiceCall(String pUserName, String pPassword, String pTargetURL, String pInput) throws IOException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: omsCustomerMasterServiceCall]");
			vlogDebug("targetURL : {0} input : {1} pUserName : {2} pPassword : {3}", pTargetURL, pInput, pUserName, pPassword);
		}

		String omsJSONResponse = null;
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.startOperation(TRUOMSConstant.CUSTOMER_MASTER_IMPORT_SERVICE_CALL);
	    }
		URL url = new URL(pTargetURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//String encodedAuth="Basic "+Base64.encodeBase64(authorization.getBytes());
		connection.setRequestProperty(TRUOMSConstant.AUTHORIZATION, OMS_AUTH_HEADER);
		connection.setRequestMethod(TRUOMSConstant.POST_METHOD);
		connection.setRequestProperty(TRUOMSConstant.CONTENT_TYPE, TRUOMSConstant.CONTENT_TYPE_VALUE);
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//Create an OutputStream and write the input
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(pInput.getBytes());
		outputStream.flush();
		int responseCode = connection.getResponseCode();
		//If the response code is not 200 - OK, then throw error
		if (responseCode != TRUOMSConstant.RESPONSE_CODE_200) {
			omsJSONResponse = generateDummyErrorResponseForCMAndCO(responseCode,connection.getResponseMessage(),Boolean.TRUE);
			return omsJSONResponse;
		}

		BufferedReader responseBuffer = new BufferedReader(
				new InputStreamReader((connection.getInputStream())));
		String output;
		while ((output = responseBuffer.readLine()) != null) {
			omsJSONResponse = output;
			if (isLoggingDebug()){
				logDebug("JSON response is " + omsJSONResponse);
			}
		}
		connection.disconnect();
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.CUSTOMER_MASTER_IMPORT_SERVICE_CALL);
	    }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: omsCustomerMasterServiceCall]");
		}
		return omsJSONResponse;
	}
	
	
	/**
	 * This method calls the OMS Service and returns json response.
	 *
	 * @param pUserName - userName for authentication
	 * @param pPassword - password for authentication
	 * @param pTargetURL - target OMS Service URL
	 * @param pInput - input json request to be sent to OMS
	 * @return omsJSONResponse - omsJSONResponse
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String omsCOImportServiceCall(String pUserName, String pPassword, String pTargetURL, String pInput) throws IOException{
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: omsCOImportServiceCall]");
			vlogDebug("targetURL : {0} input : {1} pUserName : {2} pPassword : {3}", pTargetURL, pInput, pUserName, pPassword);
		}

		String omsJSONResponse = null;
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.startOperation(TRUOMSConstant.CUSTOMER_ORDER_IMPORT_SERVICE_CALL);
	    }
		URL url = new URL(pTargetURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		//String encodedAuth="Basic "+Base64.encodeBase64(authorization.getBytes());
		connection.setRequestProperty(TRUOMSConstant.AUTHORIZATION, OMS_AUTH_HEADER);
		connection.setRequestMethod(TRUOMSConstant.GET_METHOD);
		connection.setRequestProperty(TRUOMSConstant.CONTENT_TYPE, TRUOMSConstant.CONTENT_TYPE_VALUE);
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//Create an OutputStream and write the input
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(pInput.getBytes());
		outputStream.flush();
		int responseCode = connection.getResponseCode();
		//If the response code is not 200 - OK, then throw error
		if (responseCode != TRUOMSConstant.RESPONSE_CODE_200) {
			omsJSONResponse = generateDummyErrorResponseForCMAndCO(responseCode,connection.getResponseMessage(),Boolean.FALSE);
			return omsJSONResponse;
		}

		BufferedReader responseBuffer = new BufferedReader(
				new InputStreamReader((connection.getInputStream())));
		String output;
		while ((output = responseBuffer.readLine()) != null) {
			omsJSONResponse = output;
			if (isLoggingDebug()){
				logDebug("JSON response is " + omsJSONResponse);
			}
		}
		connection.disconnect();
		// Added Performance statement 
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.CUSTOMER_ORDER_IMPORT_SERVICE_CALL);
	    }
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: omsCOImportServiceCall]");
		}
		return omsJSONResponse;
	}
	
	/**
	 * This method populates order list request.
	 *
	 * @param pPageNumber - Page Number
	 * @param pExternalCustomerId - Customer Id
	 * @param pNoOfRecordsPerPage the no of records per page
	 * @param pLayawayPage - layaway page
	 * @return customerOrderJSON -customerOrderJSON
	 */
	public String populateCustomerOrderListRequest(String pPageNumber, String pExternalCustomerId, long pNoOfRecordsPerPage, String pLayawayPage){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateCustomerOrderListRequest]");
		}
		String customerOrderJSON = null;
		if(pExternalCustomerId != null){
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			CustomerOrderListRequest customerOrderListRequest = new CustomerOrderListRequest();
			CustomerOrderSearchCriteria customerOrderSearchCriteria = new CustomerOrderSearchCriteria();
			customerOrderSearchCriteria.setCurrentPageNumber(pPageNumber);
			customerOrderSearchCriteria.setNoOfRecordsPerPage(pNoOfRecordsPerPage);
			if (pLayawayPage.equalsIgnoreCase(TRUConstants.TRUE)) {
				customerOrderSearchCriteria.setEntityType(omsConfiguration.getLayawayOrderListEntityType());
				customerOrderSearchCriteria.setOrderType(omsConfiguration.getLayawayOrderHistoryReserveProperty());
			}else{
				customerOrderSearchCriteria.setEntityType(omsConfiguration.getOrderListEntityType());
			}
			SortingCriterion sortingCriterion = new SortingCriterion();
			sortingCriterion.setSortDirection(omsConfiguration.getOrderListSortDirection());
			sortingCriterion.setSortField(omsConfiguration.getOrderListSortField());
			customerOrderSearchCriteria.setSortingCriterion(sortingCriterion);
			com.oms.orderlist.CustomerInfo customerInfo = new com.oms.orderlist.CustomerInfo();	
			customerInfo.setExternalCustomerId(pExternalCustomerId);
			customerOrderSearchCriteria.setCustomerInfo(customerInfo);
			customerOrderListRequest.setCustomerOrderSearchCriteria(customerOrderSearchCriteria);
			Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
			customerOrderJSON = gson1.toJson(customerOrderListRequest); // done
			if(isLoggingDebug()){
				vlogDebug("Customer Order List JSON", customerOrderJSON);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateCustomerOrderListRequest]");
		}
		return customerOrderJSON;
	}
	/**
	 * This method is to populate the order details for cancel order service.
	 * @param pOrderId - Order Id
	 * @return customerOrderJSON - Cancel Order JSON
	 */
	public String populateCustomerOrderObjectForCancel(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateCustomerOrderObjectForCancel]");
		}
		String customerOrderJSON = null;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(!StringUtils.isBlank(pOrderId)){
			CustomerOrder customerOrder = new CustomerOrder();
			CustomerOrderImport customerOrderImport = new CustomerOrderImport();
			customerOrder.setOrderNumber(pOrderId);
			customerOrder.setOrderCancelled(Boolean.TRUE);
			customerOrder.setReasonCode(omsConfiguration.getCancelReasonCode());
			customerOrderImport.setCustomerOrder(customerOrder);
			Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
			customerOrderJSON = gson1.toJson(customerOrderImport); // done
			JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(customerOrderJSON);
			JsonObject orderCancelJsonObject = parse.getAsJsonObject();
			JsonObject customerOrderJsonObj = orderCancelJsonObject.get(TRUOMSConstant.CUSTOMER_ORDER).getAsJsonObject();
			customerOrderJsonObj.remove(omsConfiguration.getOrderConfirmedJsonElement());//Removing unwanted attributes
			customerOrderJsonObj.remove(omsConfiguration.getOnHoldJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getMinimizeShipmentsJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getOrderSubtotalJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getOrderTotalJsonElement());
			customerOrderJSON = gson1.toJson(orderCancelJsonObject);
			if(isLoggingDebug()){
				vlogDebug("Customer Order Cancel JSON", customerOrderJSON);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateCustomerOrderObjectForCancel]");
		}
		return customerOrderJSON;
	}
	
	/**
	 * This method will populate the Order details into CustomerOrder Object to
	 * generate the JSON message for update service.
	 * 
	 * @param pCustomerOrderUpdateMessage
	 *            - TRUCustomerOrderUpdateMessage
	 *            
	 * @return String : JSON request
	 */
	public String populateCustomerOrderObjectForUpdate(TRUCustomerOrderUpdateMessage pCustomerOrderUpdateMessage){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateCustomerOrderObjectForUpdate]");
			vlogDebug("pCustomerOrderUpdateMessage : {0}", pCustomerOrderUpdateMessage);
		}
		String customerOrderJSON = null;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(pCustomerOrderUpdateMessage != null){
			CustomerOrderImport customerOrderImport = new CustomerOrderImport();
			CustomerOrder customerOrder = new CustomerOrder();
			customerOrder.setOrderNumber(pCustomerOrderUpdateMessage.getExternalOrderNumber());
			CustomerInfo customerInfo = new CustomerInfo();
			customerInfo.setExternalCustomerId(pCustomerOrderUpdateMessage.getExternalCustomerId());
			customerInfo.setCustomerEmail(pCustomerOrderUpdateMessage.getCustomerEmail());
			customerOrder.setCustomerInfo(customerInfo);
			customerOrderImport.setCustomerOrder(customerOrder);
			Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		    customerOrderJSON = gson1.toJson(customerOrderImport); // done
		    JsonParser pareser = new JsonParser();
			JsonElement parse = pareser.parse(customerOrderJSON);
			JsonObject orderUpdateJsonObject = parse.getAsJsonObject();
			JsonObject customerOrderJsonObj = orderUpdateJsonObject.get(TRUOMSConstant.CUSTOMER_ORDER).getAsJsonObject();
			customerOrderJsonObj.remove(omsConfiguration.getOrderConfirmedJsonElement());//Removing unwanted attributes
			customerOrderJsonObj.remove(omsConfiguration.getOnHoldJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getOrderSubtotalJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getOrderTotalJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getOrderCancelledJsonElement());
			customerOrderJsonObj.remove(omsConfiguration.getMinimizeShipmentsJsonElement());
			customerOrderJSON = gson1.toJson(orderUpdateJsonObject);
		    if(isLoggingDebug()){
		    	vlogDebug("CustomerOrderJSON : {0}", customerOrderJSON);
		    }
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateCustomerOrderObjectForUpdate]");
		}
		return customerOrderJSON;
	}

	/**
	 * Method to populate the json request for Layaway Order.
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 * @return : String - JSON request for Layaway Order
	 */
	public String populateLayawayOrderObject(
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayOrderObject]");
			vlogDebug("pLayawayOrderImpl : {0}",pLayawayOrderImpl);
		}
		String customerOrderJSON = null;
		if(pLayawayOrderImpl == null){
			return customerOrderJSON;
		}
		//
		if(PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(TRUOMSConstant.POPULATE_LAYAWAY_ORDER_OBJECT);
		}
		//
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		CustomerOrderImport customerOrderImport = new CustomerOrderImport();
		CustomerOrder customerOrder = new CustomerOrder();
		setCustomerFirstLastName(pLayawayOrderImpl);
		// Customer Order Header
		populateCustomerLayawayOrderHeader(customerOrder,pLayawayOrderImpl);
		// Populate payment Details
		populateLayawayPaymentDetails(customerOrder, pLayawayOrderImpl);
		// Populate Customer Info
		populateLayawayCustomerInfoFromOrder(customerOrder, pLayawayOrderImpl);
		// Populate Reference Fields.
		populateLayawayReferenceFields(customerOrder,pLayawayOrderImpl);
		// Populate Layaway Order OrderLine details.
		populateLayawayOrderLine(customerOrder,pLayawayOrderImpl);
		//Adding customer order to the parent node
		customerOrderImport.setCustomerOrder(customerOrder);
		// Setting back flag to false.
		setFirstName(null);
		setLastName(null);
		setPhoneNumber(null);
		setPaypalPG(Boolean.FALSE);
		setCreditCardPG(Boolean.FALSE);
		//Getting json object
		Gson gson1 = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
	    customerOrderJSON = gson1.toJson(customerOrderImport); // done
	    // Logic to remove the parameters from JSON Object.
	    JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(customerOrderJSON);
		JsonObject orderDetailsJsonObject = parse.getAsJsonObject();
		JsonObject customerOrderJsonObj = orderDetailsJsonObject.get(TRUOMSConstant.CUSTOMER_ORDER).getAsJsonObject();
		// Removing settlementAmount parameter for All Payment Group except Gift Card : Start
		if(customerOrderJsonObj.get(omsConfiguration.getOrderDetailsPaymentDetailsJsonElement()) != null){
			JsonObject paymentDetailsJsonObject = customerOrderJsonObj.get(omsConfiguration.getOrderDetailsPaymentDetailsJsonElement()).getAsJsonObject();
			if(paymentDetailsJsonObject != null && 
					paymentDetailsJsonObject.get(omsConfiguration.getOrderDetailsPaymentDetailJsonElement()) != null){
				JsonArray paymentDetailsJsonArray = paymentDetailsJsonObject.get(omsConfiguration.getOrderDetailsPaymentDetailJsonElement()).getAsJsonArray();
				for (JsonElement jsonElement : paymentDetailsJsonArray) {
					JsonObject PaymentDetailObject = jsonElement.getAsJsonObject();
					String paymentMethod = PaymentDetailObject.get(omsConfiguration.getPaymentMethodJSONElement()).getAsString();
					if(StringUtils.isNotBlank(paymentMethod) && !paymentMethod.equals(omsConfiguration.getPaymentMethodGC())){
						PaymentDetailObject.remove(omsConfiguration.getSettlementAmountJSONElement());
					}
				}
			}
		}
		// Removing settlementAmount parameter for All Payment Group except Gift Card : END
		JsonObject orderLinesJsonObject = customerOrderJsonObj.get(TRUOMSConstant.ORDER_LINES).getAsJsonObject();
		JsonArray orderLinesJsonArray = orderLinesJsonObject.get(TRUOMSConstant.ORDER_LINE).getAsJsonArray();
		JsonObject priceJsonObject = null;
		for (JsonElement jsonElement : orderLinesJsonArray) {
			JsonObject orderLineObject = jsonElement.getAsJsonObject();
			JsonArray customAttributeListJsonArray = orderLineObject.get(TRUOMSConstant.CUSTOMER_ATTRIBUTE_LIST).getAsJsonArray();
			if(customAttributeListJsonArray.size() <= TRUConstants.ZERO){
				orderLineObject.remove(TRUOMSConstant.CUSTOMER_ATTRIBUTE_LIST);
			}
			orderLineObject.remove(TRUOMSConstant.IS_GIFT);
			priceJsonObject = orderLineObject.get(TRUOMSConstant.PRICE_DETAILS).getAsJsonObject();
			if(priceJsonObject != null){
				priceJsonObject.remove(TRUOMSConstant.IS_PRICE_OVERRIDDEN);
				priceJsonObject.remove(TRUOMSConstant.EXTENDED_PRICE);
			}
		}
		// Finish
		customerOrderJSON = gson1.toJson(orderDetailsJsonObject);
	    if(isLoggingDebug()){
	    	vlogDebug("populateLayawayOrderObject JSON COI Service: {0}", customerOrderJSON);
	    }
	    //
	    if(PerformanceMonitor.isEnabled()){
	    	PerformanceMonitor.endOperation(TRUOMSConstant.POPULATE_LAYAWAY_ORDER_OBJECT);
	    }
	    //
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayOrderObject]");
		}
		return customerOrderJSON;
	}

	/**
	 * Method to populate the Layaway Order OrderLine details.
	 * 
	 * @param pCustomerOrder - CustomerOrder Object
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl Object
	 */
	private void populateLayawayOrderLine(CustomerOrder pCustomerOrder,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayOrderLine]");
			vlogDebug("pCustomerOrder : {0} pLayawayOrderImpl : {1}", pCustomerOrder,pLayawayOrderImpl);
		}
		if(pCustomerOrder == null || pLayawayOrderImpl == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		OrderLines orderLines = new OrderLines();
		List<OrderLine> listOfOrderLine = new ArrayList<OrderLine>();
		OrderLine orderLine = new OrderLine();
		orderLine.setOrderLineNumber(TRUConstants.DIGIT_ONE);
		orderLine.setItemId(getLayawayItemUID());
		orderLine.setLineTotal(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		orderLine.setUnitPriceAmount(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		orderLine.setOrderedQuantity(TRUConstants.LONG_ONE);
		orderLine.setOrderedQtyUOM(omsConfiguration.getOrderedQtyUOM());
		// Calling method to populate the Layaway Order Line Details.
		populateLayawayOrderPriceDetails(orderLine,pLayawayOrderImpl);
		// Calling method to populate the Layaway Order Shipping Details.
		populateLayawayOrderShippingDetails(orderLine,pLayawayOrderImpl);
		listOfOrderLine.add(orderLine);
		orderLines.setOrderLine(listOfOrderLine);
		pCustomerOrder.setOrderLines(orderLines);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayOrderLine]");
		}
	}

	/**
	 * Method to populate the Layaway Order OrderLine Price Details.
	 * 
	 * @param pOrderLine - OrderLine Object
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl Object
	 */
	private void populateLayawayOrderPriceDetails(OrderLine pOrderLine,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayOrderPriceDetails]");
			vlogDebug("pOrderLine : {0} pLayawayOrderImpl : {1}", pOrderLine,pLayawayOrderImpl);
		}
		if(pOrderLine == null || pLayawayOrderImpl == null){
			return;
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		PriceDetails priceDetails = new PriceDetails();
		priceDetails.setPrice(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		pOrderLine.setPriceDetails(priceDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayOrderPriceDetails]");
		}
	}
	
	/**
	 * Method to populate the Layaway Order OrderLine Shipping Info Details.
	 *  
	 * @param pOrderLine - OrderLine Object
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl Object
	 */
	private void populateLayawayOrderShippingDetails(OrderLine pOrderLine,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayOrderShippingDetails]");
			vlogDebug("pOrderLine : {0} pLayawayOrderImpl : {1}", pOrderLine,pLayawayOrderImpl);
		}
		if(pOrderLine == null || pLayawayOrderImpl == null){
			return;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingInfo shippingInfo = new ShippingInfo();
		shippingInfo.setShipVia(omsConfiguration.getLayawayShipVia());
		shippingInfo.setDeliveryOption(omsConfiguration.getLayawayDeliveryOption());
		//Calling method to populate the Shipping Address Details.
		populateLayawayOrderShippingAddDetails(shippingInfo,pLayawayOrderImpl);
		pOrderLine.setShippingInfo(shippingInfo);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayOrderShippingDetails]");
		}
	}

	/**
	 * Method to populate the Layaway Order OrderLine Shipping Info Shipping Add Details.
	 *  
	 * @param pShippingInfo - ShippingInfo Object
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl Object
	 */
	private void populateLayawayOrderShippingAddDetails(
			ShippingInfo pShippingInfo, TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayOrderShippingAddDetails]");
			vlogDebug("pShippingInfo : {0} pLayawayOrderImpl : {1}", pShippingInfo,pLayawayOrderImpl);
		}
		if(pShippingInfo == null || pLayawayOrderImpl == null){
			return;
		}
		String country = null;
		String phoneNumber = null;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		ShippingAddress shippingAddress = new ShippingAddress();
		shippingAddress.setFirstName(pLayawayOrderImpl.getFirstName());
		shippingAddress.setLastName(pLayawayOrderImpl.getLastName());
		shippingAddress.setAddressLine1(pLayawayOrderImpl.getAddress1());
		shippingAddress.setAddressLine2(pLayawayOrderImpl.getAddress2());
		shippingAddress.setCity(pLayawayOrderImpl.getCity());
		shippingAddress.setStateProv(pLayawayOrderImpl.getAddrState());
		shippingAddress.setPostalCode(pLayawayOrderImpl.getPostalCode());
		country = omsConfiguration.getCountryCodeMap().get(pLayawayOrderImpl.getCountry());
		if(StringUtils.isBlank(country)){
			shippingAddress.setCountry(pLayawayOrderImpl.getCountry());
		}else{
			shippingAddress.setCountry(country);
		}
		phoneNumber = formatPhoneNumer(pLayawayOrderImpl.getPhoneNumber());
		shippingAddress.setPhone(phoneNumber);
		shippingAddress.setEmail(pLayawayOrderImpl.getEmail());
		pShippingInfo.setShippingAddress(shippingAddress);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayOrderShippingAddDetails]");
		}
	}

	/**
	 * Method to populate the Reference Fields for Layaway Order.
	 * @param pCustomerOrder - CustomerOrder
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	private void populateLayawayReferenceFields(CustomerOrder pCustomerOrder,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayReferenceFields]");
		}
		if(pLayawayOrderImpl == null || pCustomerOrder == null){
			return;
		}
		PricingTools pricingTools = getOrderManager().getPromotionTools().getPricingTools();
		ReferenceFields refField = new ReferenceFields();
		String siteId = pLayawayOrderImpl.getSiteId();
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		refField.setReferenceField1(pLayawayOrderImpl.getLayawayOMSOrderId());
		if(!StringUtils.isBlank(siteId)){
			if(siteId.contains(omsConfiguration.getTruSiteId())){
				refField.setReferenceField4(omsConfiguration.getOrderRefField4ForTRU());
			}else if(siteId.contains(omsConfiguration.getBruSiteId())){
				refField.setReferenceField4(omsConfiguration.getOrderRefField4ForBRU());
			}
		}
		
		refField.setReferenceNumber1(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		refField.setReferenceNumber2(pricingTools.round(pLayawayOrderImpl.getLayawayDueAmount()));
		pCustomerOrder.setReferenceFields(refField);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayReferenceFields]");
		}
		
	}
	
	/**
	 * This method used to get the credit card billing details.
	 * @param pPaymentDetails : List of PaymentDetail
	 * @return BillToDetail : BillToDetail Object
	 */
	public BillToDetail getCreditCardBillingDetails(List<PaymentDetail> pPaymentDetails) {
		BillToDetail billingDetails = null;
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getCreditCardBillingDetails]");
		}
		if(pPaymentDetails != null && !pPaymentDetails.isEmpty()){
			String paymentMethod = null;
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			for(PaymentDetail paymentDetail : pPaymentDetails){
				paymentMethod = paymentDetail.getPaymentMethod();
				if((!StringUtils.isBlank(paymentMethod)) && paymentMethod.equals(omsConfiguration.getCreditCardMethod())){
					billingDetails = paymentDetail.getBillToDetail();
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getCreditCardBillingDetails]");
		}
		return billingDetails;
	}
	
	/**
	 * Method to populate the Customer Info for Layaway Order.
	 * @param pCustomerOrder - CustomerOrder
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	public void populateLayawayCustomerInfo(CustomerOrder pCustomerOrder,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayCustomerInfo]");
		}
		CustomerInfo customerInfo = null;
		RepositoryItem profile;
		try {
			String phoneNumber = null;
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pLayawayOrderImpl);
			TRUPropertyManager propertyManager = getPropertyManager();
			List<PaymentDetail> paymentDetails = null;
			if(pCustomerOrder.getPaymentDetails() != null){
				paymentDetails = pCustomerOrder.getPaymentDetails().getPaymentDetail();
			}
			BillToDetail billingDetail = null;
			if(paymentDetails != null && !paymentDetails.isEmpty()){
				if(isCreditCardPG()){
					billingDetail = getCreditCardBillingDetails(paymentDetails);
				}else if(isPaypalPG()){
					billingDetail = getPayPalBillingDetails(paymentDetails);
				}else{
					billingDetail= paymentDetails.get(0).getBillToDetail();
				}
				if (isLoggingDebug()) {
					vlogDebug("paymentDetails: {0} billingDetail : {1}",paymentDetails,billingDetail);
				}
			}
			customerInfo = new CustomerInfo();
			if(!profile.isTransient()){
				customerInfo.setExternalCustomerId(pLayawayOrderImpl.getProfileId());
				customerInfo.setCustomerUserId((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerInfo.setCustomerFirstName((String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName()));
				customerInfo.setCustomerLastName((String) profile.getPropertyValue(propertyManager.getLastNamePropertyName()));
				if(billingDetail != null){
					phoneNumber = formatPhoneNumer(billingDetail.getPhone());
					customerInfo.setCustomerPhone(phoneNumber);
				}
				customerInfo.setCustomerEmail((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
			} else{
				if(billingDetail != null){
					customerInfo.setCustomerFirstName(billingDetail.getFirstName());
					customerInfo.setCustomerLastName(billingDetail.getLastName());
					phoneNumber = formatPhoneNumer(billingDetail.getPhone());
					customerInfo.setCustomerPhone(phoneNumber);
					if(StringUtils.isNotBlank(billingDetail.getEmail())){
						customerInfo.setCustomerEmail(billingDetail.getEmail());
					}else{
						customerInfo.setCustomerEmail(pLayawayOrderImpl.getEmail());
					}
				}
			}
			if(customerInfo != null){
				if(StringUtils.isBlank(customerInfo.getCustomerFirstName())){
					customerInfo.setCustomerFirstName(getFirstName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerLastName())){
					customerInfo.setCustomerLastName(getLastName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerPhone())){
					customerInfo.setCustomerPhone(getPhoneNumber());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerEmail())){
					customerInfo.setCustomerEmail(pLayawayOrderImpl.getEmail());
				}
				pCustomerOrder.setCustomerInfo(customerInfo);
			}
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : occurred while getting profile for order : {0} with exception : {1}", pLayawayOrderImpl,  exc);
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayCustomerInfo]");
		}
		
	}

	/**
	 * Method to format the Phone Number.
	 * @param pPhone : String Phone Number
	 * @return - String : Formated Phone Number
	 */
	public String formatPhoneNumer(String pPhone) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: formatPhoneNumer]");
			vlogDebug("pPhone: {0}",pPhone);
		}
		String phoneNumber = null;
		if(!StringUtils.isBlank(pPhone)){
			phoneNumber = pPhone.replaceFirst(TRUOMSConstant.PHONE_REJEX_PATTERN, TRUOMSConstant.PHONE_NUMBER_GRUPING_INDEXING);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: formatPhoneNumer]");
			vlogDebug("phoneNumber: {0}",phoneNumber);
		}
		return phoneNumber;
	}
	/**
	 * This method used to get the paypal billing details.
	 * @param pPaymentDetails : List of PaymentDetail
	 * @return BillToDetail : BillToDetail Object
	 */
	public BillToDetail getPayPalBillingDetails(List<PaymentDetail> pPaymentDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getPayPalBillingDetails]");
		}
		BillToDetail billingDetails = null;
		if(pPaymentDetails != null && !pPaymentDetails.isEmpty()){
			String paymentMethod = null;
			TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
			for(PaymentDetail paymentDetail : pPaymentDetails){
				paymentMethod = paymentDetail.getPaymentMethod();
				if((!StringUtils.isBlank(paymentMethod)) && paymentMethod.equals(omsConfiguration.getPayPalMethod())){
					billingDetails = paymentDetail.getBillToDetail();
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getPayPalBillingDetails]");
		}
		return billingDetails;
	}
	

	/**
	 * Method to set 1st GiftCard Payment Group and then other Payment Groups.
	 * @param pPaymentGroups - List of Payment Group from Order Object
	 * @return : List - List of Payment Group after sorting based on Payment Group type
	 */
	public List<PaymentGroup> orderPaymentGroupBasedOnType(List<PaymentGroup> pPaymentGroups) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: orderPaymentGroupBasedOnType]");
		}
		List<PaymentGroup> paymentGroup = null;
		if(pPaymentGroups == null || pPaymentGroups.isEmpty()){
			return paymentGroup;
		}
		for(PaymentGroup lPaymentGroup : pPaymentGroups){
			if(lPaymentGroup instanceof TRUGiftCard){
				if(paymentGroup == null){
					paymentGroup = new ArrayList<PaymentGroup>();
				}
				paymentGroup.add(lPaymentGroup);
			}
		}
		for(PaymentGroup lPaymentGroup : pPaymentGroups){
			if(lPaymentGroup instanceof InStorePayment){
				continue;
			}
			if(paymentGroup == null){
				paymentGroup = new ArrayList<PaymentGroup>();
			}
			if(!paymentGroup.contains(lPaymentGroup)){
				paymentGroup.add(lPaymentGroup);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: orderPaymentGroupBasedOnType]");
		}
		return paymentGroup;
	}
	
	/**
	 * Method to populate the Billing details for Layaway Order.
	 * @param pPaymentDetail - PaymentDetail
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	private void setBillToDetailsForLayawayOrder(PaymentDetail pPaymentDetail,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setBillToDetailsForLayawayOrder]");
		}
		if(pLayawayOrderImpl == null || pPaymentDetail == null){
			return;
		}
		BillToDetail billToDetail = new BillToDetail();
		billToDetail.setFirstName(pLayawayOrderImpl.getFirstName());
		billToDetail.setLastName(pLayawayOrderImpl.getLastName());
		billToDetail.setAddressLine1(pLayawayOrderImpl.getAddress1());
		billToDetail.setAddressLine2(pLayawayOrderImpl.getAddress2());
		billToDetail.setCity(pLayawayOrderImpl.getCity());
		billToDetail.setStateProv(pLayawayOrderImpl.getAddrState());
		String country = getOmsConfiguration().getCountryCodeMap().get(pLayawayOrderImpl.getCountry());
		if(StringUtils.isBlank(country)){
			billToDetail.setCountry(pLayawayOrderImpl.getCountry());
		}else{
			billToDetail.setCountry(country);
		}
		billToDetail.setPostalCode(pLayawayOrderImpl.getPostalCode());
		billToDetail.setPhone(formatPhoneNumer(pLayawayOrderImpl.getPhoneNumber()));
		billToDetail.setEmail(pLayawayOrderImpl.getEmail());
		pPaymentDetail.setBillToDetail(billToDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setBillToDetailsForLayawayOrder]");
		}
		
	}
	
	/**
	 * Method to populate the Payment Details for Layaway Order.
	 * @param pCustomerOrder - CustomerOrder
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	@SuppressWarnings("unchecked")
	private void populateLayawayPaymentDetails(CustomerOrder pCustomerOrder,
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayPaymentDetails]");
		}
		if(pLayawayOrderImpl == null || pCustomerOrder == null){
			return;
		}
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pLayawayOrderImpl.getPaymentGroups());
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		TRUPayPal payPal = null;
		TRUGiftCard giftCard = null;
		InStorePayment inStorePayment = null;
		ContactInfo billingAddress = null;
		String paymentGroupType = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		TRUConfiguration truConfiguration = getTruConfiguration();
		String sourceOfOrder = pLayawayOrderImpl.getSourceOfOrder();
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			PaymentDetails paymentDetails = new PaymentDetails();
			List<PaymentDetail> listOfPds = new ArrayList<PaymentDetail>();
			PaymentDetail paymentDetail = null;
			for(PaymentGroup paymentGroup : paymentGroups){
				if (isLoggingDebug()) {
					vlogDebug("Payment Group: {0}",paymentGroup);
				}
				if(paymentGroup instanceof InStorePayment){
					continue;
				}
				paymentDetail = new PaymentDetail();
				paymentDetail.setExternalPaymentDetailId(paymentGroup.getId());
				paymentDetail.setAuthorizationAmount(pricingTools.round(paymentGroup.getAmount()));
				paymentDetail.setChargeSequence(String.valueOf(paymentGroups.indexOf(paymentGroup)+TRUConstants.INTEGER_NUMBER_ONE));
				if(isPayInStoreOrder(pLayawayOrderImpl)){
					paymentDetail.setPaymentEntryType(omsConfiguration.getPayInStotePaymentEntryType());
				}else if(StringUtils.isNotBlank(sourceOfOrder)){
					if(truConfiguration.getWebSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getWebPaymentEntryType());
					}
					if(truConfiguration.getMobileSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getMobilePaymentEntryType());
					}
					if(truConfiguration.getSosSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getSosPaymentEntryType());
					}
					if(truConfiguration.getCsrSourceOrderType().equalsIgnoreCase(sourceOfOrder)){
						paymentDetail.setPaymentEntryType(omsConfiguration.getCsrPaymentEntryType());
					}
				}else{
					paymentDetail.setPaymentEntryType(omsConfiguration.getPaymentEntryType());
				}
				if(paymentGroup instanceof TRUCreditCard){
					populateLayawayPaymentDetailsForCC(paymentDetail,paymentGroup,pLayawayOrderImpl);
				}else if(paymentGroup instanceof TRUGiftCard){
					giftCard = (TRUGiftCard) paymentGroup;
					paymentGroupType = (String) giftCard.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setSettlementAmount(pricingTools.round(paymentGroup.getAmount()));
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					paymentDetail.setAccountNumber(giftCard.getGiftCardNumber());
					paymentDetail.setAccountDisplayNumber(giftCard.getGiftCardNumber());
					if(omsConfiguration.isGiftCardSecurityCode()){
						paymentDetail.setSecurityCode(giftCard.getGiftCardPin());
					}else{
						paymentDetail.setSecurityCode(omsConfiguration.getSecurityCode());
					}
					setBillToDetailsForLayawayOrder(paymentDetail,pLayawayOrderImpl);
				}else if(paymentGroup instanceof TRUPayPal){
					payPal = (TRUPayPal) paymentGroup;
					paymentGroupType = (String) payPal.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					paymentDetail.setAccountNumber(payPal.getToken());
					billingAddress = (ContactInfo) payPal.getBillingAddress();
					setPaymentDetailBillingAddress(billingAddress,paymentDetail,pLayawayOrderImpl);
				}else if(paymentGroup instanceof InStorePayment){
					inStorePayment = (InStorePayment) paymentGroup;
					paymentGroupType = (String) inStorePayment.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
					paymentDetail.setPaymentEntryType(omsConfiguration.getPayInStotePaymentEntryType());
					paymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
					setBillToDetailsForLayawayOrder(paymentDetail,pLayawayOrderImpl);
				}
				// Method to populate the Payment Transactions Details.
				if(paymentGroup instanceof TRUGiftCard){
					populateTransFiledsForSettlement(paymentGroup,paymentDetail);
				}else{
					populatePaymentTransactionFields(paymentGroup,paymentDetail);
				}
				listOfPds.add(paymentDetail);
			}
			paymentDetails.setPaymentDetail(listOfPds);
			pCustomerOrder.setPaymentDetails(paymentDetails);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayPaymentDetails]");
		}
		
	}

	/**
	 * This method is to populate the payment details for credit card for layaway orders.
	 * @param pPaymentDetail - PaymentDetail
	 * @param pPaymentGroup - PaymentGroup
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	private void populateLayawayPaymentDetailsForCC(PaymentDetail pPaymentDetail, PaymentGroup pPaymentGroup, 
			TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Ente into [Class: TRUBaseOMSUtils  method: populateLayawayPaymentDetailsForCC]");
		}
		TRUCreditCard creditCardPg = null;
		ContactInfo billingAddress = null;
		String paymentGroupType = null;
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		creditCardPg = (TRUCreditCard) pPaymentGroup;
		paymentGroupType = (String) creditCardPg.getPropertyValue(omsConfiguration.getPaymentGroupTypePropertyName());
		pPaymentDetail.setPaymentMethod(omsConfiguration.getPaymentMethodMap().get(paymentGroupType));
		pPaymentDetail.setCardType(omsConfiguration.getCreditCardTypeMap().get(creditCardPg.getCreditCardType()));
		String creditCardNumber = creditCardPg.getCreditCardNumber();
		pPaymentDetail.setAccountNumber(creditCardNumber);
		int length = creditCardPg.getCreditCardNumber().length();
		String accountDisplayNumber = creditCardNumber.substring(length-TRUConstants.FOUR, length);
		pPaymentDetail.setAccountDisplayNumber(XXXXXXXXXXXX+accountDisplayNumber);
		pPaymentDetail.setNameOnCard(creditCardPg.getNameOnCard());
		pPaymentDetail.setCardExpiryMonth(creditCardPg.getExpirationMonth());
		pPaymentDetail.setCardExpiryYear(creditCardPg.getExpirationYear());
		billingAddress = (ContactInfo) creditCardPg.getBillingAddress();
		setPaymentDetailBillingAddress(billingAddress,pPaymentDetail,pLayawayOrderImpl);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayPaymentDetailsForCC]");
		}
	}

	/**
	 * This method is to populate the payment transaction details.
	 * 
	 * @param pPaymentGroup - PaymentGroup
	 * @param pPaymentDetail - PaymentDetail
	 */
	@SuppressWarnings("unchecked")
	public void populatePaymentTransactionFields(PaymentGroup pPaymentGroup,
			PaymentDetail pPaymentDetail) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populatePaymentTransactionFields]");
			vlogDebug("pPaymentGroup: {0} pPaymentDetail : {1}",pPaymentGroup,pPaymentDetail);
		}
		if(pPaymentGroup == null || pPaymentDetail == null){
			return;
		}
		List<PaymentStatus> listOfCreditStatus = pPaymentGroup.getAuthorizationStatus();
		if (isLoggingDebug()) {
			vlogDebug("listOfCreditStatus: {0}",listOfCreditStatus);
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(listOfCreditStatus != null && !listOfCreditStatus.isEmpty()){
			DateFormat df1 = new SimpleDateFormat(DATE_FORMAT1,Locale.US);
			DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
			df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
			PaymentTransaction payTrans = null;
			TRUPayPal payPal = null;
			PaymentTransactionDetails paymentTrans = new PaymentTransactionDetails();
			List<PaymentTransaction> listOfPaymentTrns = new ArrayList<PaymentTransaction>();
			TRUCreditCard creditCard = null;
			Date transactionExpDate = null;
			TRUPaymentStatus truPaymentStatus = null;
			for(PaymentStatus paymentStatus : listOfCreditStatus){
				if(!paymentStatus.getTransactionSuccess()){
					continue;
				}
				if(isFraudValidationReq(pPaymentGroup, paymentStatus) && !((TRUPaymentStatus)paymentStatus).isFraud()){
					continue;
				}
				payTrans = new PaymentTransaction();
				payTrans.setExternalPaymentTransactionId(paymentStatus.getTransactionId());
				payTrans.setRequestedAmount(pPaymentGroup.getAmount());
				payTrans.setProcessedAmount(paymentStatus.getAmount()/TRUConstants.HUNDERED);
				payTrans.setTransactionDecision(omsConfiguration.getTransactionDecisionSuccess());
				if(paymentStatus.getTransactionTimestamp() != null){
					payTrans.setRequestedDTTM(df.format(paymentStatus.getTransactionTimestamp()));
				}
				if(paymentStatus instanceof TRUPaymentStatus){
					truPaymentStatus = (TRUPaymentStatus) paymentStatus;
					if(truPaymentStatus.getTransactionDTTM() != null){
						payTrans.setTransactionDTTM(df.format(truPaymentStatus.getTransactionDTTM()));
						transactionExpDate = getTransactionExpDate(truPaymentStatus.getTransactionDTTM());
						if(transactionExpDate != null){
							payTrans.setTransactionExpDate(df1.format(transactionExpDate));
						}
					}
					payTrans.setRequestId(null);
					payTrans.setFollowOnId(null);
					payTrans.setFollowOnToken(null);
				}
				payTrans.setTransactionType(omsConfiguration.getAuthorizationTransactionType());
				if(pPaymentGroup instanceof TRUCreditCard){
					creditCard = (TRUCreditCard) pPaymentGroup;
					payTrans.setRequestToken(creditCard.getCreditCardNumber());
					payTrans.setECIFLAG(creditCard.getEciFlag());
					payTrans.setCAVV(creditCard.getCavv());
					payTrans.setXID(creditCard.getXid());
				}
				if(pPaymentGroup instanceof TRUPayPal){
					payPal = (TRUPayPal) pPaymentGroup;
					payTrans.setRequestToken(payPal.getToken());
					payTrans.setPayerID(payPal.getPayerId());
					payTrans.setPaypalTransactionid(payPal.getPayPalTransactionId());
				}
				payTrans.setRecordStatus(omsConfiguration.getPaymentTransactionRecordStatus());//Added this change as part of critical CR
				listOfPaymentTrns.add(payTrans);
			}
			if(!listOfPaymentTrns.isEmpty()){
				if(pPaymentDetail.getPaymentTransactionDetails() != null &&
						pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction() != null && 
								pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction().size() > TRUConstants.ZERO){
					listOfPaymentTrns.addAll(pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction());
				}
				paymentTrans.setPaymentTransaction(listOfPaymentTrns);
			}
			pPaymentDetail.setPaymentTransactionDetails(paymentTrans);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populatePaymentTransactionFields]");
		}
	}
	
	/**
	 * This method is to populate the payment transaction details for Debit Status.
	 * 
	 * @param pPaymentGroup - PaymentGroup
	 * @param pListOfDebitStatus - listOfDebitStatus
	 * @param pListOfPaymentTrns - listOfPaymentTrns
	 */
	public void updatePaymentTranstionForDebitStatus(PaymentGroup pPaymentGroup, List<PaymentStatus> pListOfDebitStatus, 
			List<PaymentTransaction> pListOfPaymentTrns) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: updatePaymentTranstionForDebitStatus]");
		}
		PaymentTransaction payTrans = null;
		TRUCreditCard creditCard = null;
		TRUCreditCardStatus creditCardStatus= null;
		TRUGiftCard giftCard= null;
		TRUPayPalStatus payPalStatus= null;
		TRUGiftCardStatus giftCardStatus = null;
		TRUPayPal payPal = null;
		DateFormat df = new SimpleDateFormat(DATE_FORMAT1,Locale.US);
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		Date transactionExpDate = null;
		if(pPaymentGroup instanceof TRUGiftCard){
			for(PaymentStatus paymentStatus : pListOfDebitStatus){
				payTrans = new PaymentTransaction();
				payTrans.setExternalPaymentTransactionId(paymentStatus.getTransactionId());
				payTrans.setRequestedAmount(pPaymentGroup.getAmount());
				payTrans.setProcessedAmount(paymentStatus.getAmount());
				payTrans.setTransactionDecision(omsConfiguration.getTransactionDecisionSuccess());
				if(paymentStatus.getTransactionTimestamp() != null){
					payTrans.setRequestedDTTM(df.format(paymentStatus.getTransactionTimestamp()));
					transactionExpDate = getTransactionExpDate(paymentStatus.getTransactionTimestamp());
					if(transactionExpDate != null){
						payTrans.setTransactionExpDate(df.format(transactionExpDate));
					}
				}
				payTrans.setTransactionType(omsConfiguration.getAuthorizationTransactionType());
				if(pPaymentGroup instanceof TRUCreditCard){
					creditCard = (TRUCreditCard) pPaymentGroup;
					payTrans.setRequestToken(creditCard.getCreditCardNumber());
				}
				if(pPaymentGroup instanceof TRUGiftCard){
					giftCard = (TRUGiftCard) pPaymentGroup;
					payTrans.setRequestToken(giftCard.getGiftCardNumber());
				}
				if(paymentStatus instanceof TRUCreditCardStatus){
					creditCardStatus = (TRUCreditCardStatus) paymentStatus;
					if(creditCardStatus.getTransactionDTTM() != null){
						payTrans.setTransactionDTTM(df.format(creditCardStatus.getTransactionDTTM()));
					}
					payTrans.setRequestId(null);
				}
				if(paymentStatus instanceof TRUPayPalStatus){
					payPalStatus = (TRUPayPalStatus) paymentStatus;
					if(payPalStatus.getTransactionDTTM() != null){
						payTrans.setTransactionDTTM(df.format(payPalStatus.getTransactionDTTM()));
					}
					payTrans.setRequestId(null);
				}
				if(paymentStatus instanceof TRUGiftCardStatus){
					giftCardStatus = (TRUGiftCardStatus) paymentStatus;
					if(giftCardStatus.getTransactionDTTM() != null){
						payTrans.setTransactionDTTM(df.format(giftCardStatus.getTransactionDTTM()));
					}
					payTrans.setRequestId(null);
				}
				if(pPaymentGroup instanceof TRUPayPal){
					payPal = (TRUPayPal) pPaymentGroup;
					payTrans.setPayerID(payPal.getPayerId());
					payTrans.setPaypalTransactionid(payPal.getPayPalTransactionId());
				}
				pListOfPaymentTrns.add(payTrans);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: updatePaymentTranstionForDebitStatus]");
		}
	}
	
	/** 
	 * This method is to populate the bill to detail for payment details of customer order.
	 * 
	 * @param pBillingAddress - Address
	 * @param pPaymentDetail - PaymentDetail
	 * @param pOrder - Order Object
	 */
	public void setPaymentDetailBillingAddress(ContactInfo pBillingAddress, PaymentDetail pPaymentDetail, Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setPaymentDetailBillingAddress]");
			vlogDebug("pBillingAddress: {0} pPaymentDetail : {1}",pBillingAddress,pPaymentDetail);
		}
		if(pBillingAddress != null && pPaymentDetail != null){
			BillToDetail billToDetail = new BillToDetail();
			billToDetail.setFirstName(pBillingAddress.getFirstName());
			billToDetail.setLastName(pBillingAddress.getLastName());
			billToDetail.setAddressLine1(pBillingAddress.getAddress1());
			billToDetail.setAddressLine2(pBillingAddress.getAddress2());
			billToDetail.setAddressLine3(pBillingAddress.getAddress3());
			billToDetail.setCity(pBillingAddress.getCity());
			billToDetail.setStateProv(pBillingAddress.getState());
			String country = getOmsConfiguration().getCountryCodeMap().get(pBillingAddress.getCountry());
			if(StringUtils.isBlank(country)){
				billToDetail.setCountry(pBillingAddress.getCountry());
			}else{
				billToDetail.setCountry(country);
			}
			billToDetail.setPostalCode(pBillingAddress.getPostalCode());
			billToDetail.setPhone(formatPhoneNumer(pBillingAddress.getPhoneNumber()));
			if(pOrder instanceof TRUOrderImpl){
				billToDetail.setEmail(((TRUOrderImpl)pOrder).getEmail());
			}else if(pOrder instanceof TRULayawayOrderImpl){
				billToDetail.setEmail(((TRULayawayOrderImpl)pOrder).getEmail());
			}
			pPaymentDetail.setBillToDetail(billToDetail);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setPaymentDetailBillingAddress]");
		}
	}

	
	/**
	 * Method to populate the Laywar Order Header fields.
	 * @param pCustomerOrder - CustomerOrder
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	private void populateCustomerLayawayOrderHeader(
			CustomerOrder pCustomerOrder, TRULayawayOrderImpl pLayawayOrderImpl) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: populateCustomerLayawayOrderHeader]");
		}
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		boolean onHoldStatus = Boolean.FALSE;
		DateFormat df = new SimpleDateFormat(DATE_FORMAT,Locale.US);
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		pCustomerOrder.setTcCompanyId(omsConfiguration.getTcCompanyIdValue());
		pCustomerOrder.setOrderNumber(pLayawayOrderImpl.getLayawayOrderid());
		if(pLayawayOrderImpl.getSubmittedDate() != null){
			pCustomerOrder.setOrderCapturedDate(df.format(pLayawayOrderImpl.getSubmittedDate()));
		}
		if(StringUtils.isNotBlank(pLayawayOrderImpl.getSourceOfOrder())){
			pCustomerOrder.setEntryType(pLayawayOrderImpl.getSourceOfOrder());
		}else{
			pCustomerOrder.setEntryType(omsConfiguration.getOrderEntryTypeWeb());
		}
		pCustomerOrder.setOrderConfirmed(Boolean.TRUE);
		if(isPayInStoreOrder(pLayawayOrderImpl)){
			pCustomerOrder.setOnHold(Boolean.TRUE);
		}else{
			onHoldStatus = getFraudStatus(pLayawayOrderImpl);
			pCustomerOrder.setOnHold(onHoldStatus);
		}
		pCustomerOrder.setMinimizeShipments(Boolean.TRUE);
		pCustomerOrder.setPaymentStatus(omsConfiguration.getOrderPaymentStatus());
		pCustomerOrder.setOrderCancelled(Boolean.FALSE);
		pCustomerOrder.setOrderType(omsConfiguration.getOrderTypeMap().get(ORDER_TYPE_LAYAWAY_KEY));
		pCustomerOrder.setOrderSubtotal(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		pCustomerOrder.setOrderTotal(pricingTools.round(pLayawayOrderImpl.getPaymentAmount()));
		if(StringUtils.isNotBlank(pLayawayOrderImpl.getEnteredBy())){
			pCustomerOrder.setEnteredBy(pLayawayOrderImpl.getEnteredBy());
		}
		if(StringUtils.isNotBlank(pLayawayOrderImpl.getEnteredLocation())){
			pCustomerOrder.setEnteredLocation(pLayawayOrderImpl.getEnteredLocation());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateCustomerLayawayOrderHeader]");
		}
		
	}
	
	/**
	 * Method to add 7 more day to transactionDTTM date.
	 * @param pTransactionTimestamp : Date Object
	 * @return - Date 
	 */
	public Date getTransactionExpDate(Date pTransactionTimestamp) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getTransactionExpDate]");
			vlogDebug("pTransactionTimestamp: {0}",pTransactionTimestamp);
		}
		Date transactionExpDate = null;
		if(pTransactionTimestamp == null){
			return transactionExpDate;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(pTransactionTimestamp);
		cal.add(Calendar.DATE, TRUOMSConstant.INT_SEVEN);
		transactionExpDate = cal.getTime();
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getTransactionExpDate]");
			vlogDebug("Returning with transactionExpDate: {0}",transactionExpDate);
		}
		return transactionExpDate;
	}
	
	
	/**
	 * Method to get the Estimated delivery date for OMS.
	 * @param pEstimateShipWindowMin : int
	 * @return String : Date in format MMddYYYY
	 */
	public String getEstimateShippingDate(int pEstimateShipWindowMin) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getEstimateShippingDate]");
			vlogDebug("pEstimateShipWindowMin: {0}",pEstimateShipWindowMin);
		}
		String estDeliveryDate = null;
		DateFormat df = new SimpleDateFormat(REFERENCE_DATE_FORMAT,Locale.US);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, pEstimateShipWindowMin);
		estDeliveryDate = df.format(now.getTime());
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: pEstimateShipWindowMin]");
			vlogDebug("Returning with estDeliveryDate: {0}",estDeliveryDate);
		}
		return estDeliveryDate;
	}
	

	/**
	 * Method to get the Tax Detail Id.
	 * @param pOrderId : String order id
	 * @return : Sting
	 */
	public String getExtTaxDetailId(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getExtTaxDetailId]");
			vlogDebug("pOrderId: {0}",pOrderId);
		}
		String extTaxDetailId = null;
		if(StringUtils.isBlank(pOrderId)){
			return extTaxDetailId;
		}
		int taxCounter = getTaxCounter();
		extTaxDetailId = pOrderId+String.format(TRUOMSConstant.PERCENTILE_FIVE_D,taxCounter);
		setTaxCounter(++taxCounter);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getExtTaxDetailId]");
			vlogDebug("Returning with extTaxDetailId: {0}",extTaxDetailId);
		}
		return extTaxDetailId;
	}

	
	/**
	 * Method to populate the first name and last name in case of Logged user doesnt have first name and last name in Profile.
	 * @param pOrder : TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	public void setCustomerFirstLastName(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setCustomerFirstLastName]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		if(pOrder == null){
			return;
		}
		// Code modified for CR127
		String orderEmail = null;
		TRUPropertyManager propertyManager = getPropertyManager();
		try {
			RepositoryItem profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pOrder);
			if(profile != null && !profile.isTransient()){
				orderEmail = (String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName());
			}else if(pOrder instanceof TRUOrderImpl){
				orderEmail = ((TRUOrderImpl)pOrder).getEmail();
			}else if(pOrder instanceof TRULayawayOrderImpl){
				orderEmail = ((TRULayawayOrderImpl)pOrder).getEmail();
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				vlogError("RepositoryException : {0}", exc);
			}
		}
		if(StringUtils.isNotBlank(orderEmail)){
			int atIndex = orderEmail.indexOf(TRUConstants.AT_SIGN);
			if(atIndex >= TRUConstants.ZERO){
				orderEmail = orderEmail.substring(TRUConstants.ZERO, atIndex);
				setFirstName(orderEmail);
				setLastName(orderEmail);
			}
		}
		List<PaymentGroup>  paymentGroups = pOrder.getPaymentGroups();
		ContactInfo billingAddress = null;
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			for(PaymentGroup pg : paymentGroups){
				if(pg instanceof TRUCreditCard){
					billingAddress = (ContactInfo) (((TRUCreditCard)pg).getBillingAddress());
					break;
				}
				if(pg instanceof TRUPayPal){
					billingAddress = (ContactInfo) (((TRUPayPal)pg).getBillingAddress());
					break;
				}
			}
		}
		if(billingAddress != null){
			setPhoneNumber(formatPhoneNumer(billingAddress.getPhoneNumber()));
			return;
		}
		if(pOrder instanceof TRUOrderImpl){
			Address shippingAddress = null;
			TRUHardgoodShippingGroup truHGSH = null;
			TRUInStorePickupShippingGroup truINPSG=null;
			if(billingAddress == null){
				List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
				for(ShippingGroup lShipGrp : shippingGroups){
					if(lShipGrp instanceof TRUHardgoodShippingGroup){
						truHGSH = (TRUHardgoodShippingGroup) lShipGrp;
						shippingAddress = truHGSH.getShippingAddress();
						break;
					}
					if(lShipGrp instanceof TRUInStorePickupShippingGroup){
						truINPSG = (TRUInStorePickupShippingGroup) lShipGrp;
						break;
					}
				}
			}
			if(shippingAddress != null){
				setPhoneNumber(formatPhoneNumer(((ContactInfo)shippingAddress).getPhoneNumber()));
				return;
			}
			if(truINPSG != null){
				setPhoneNumber(formatPhoneNumer(truINPSG.getPhoneNumber()));
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setCustomerFirstLastName]");
		}
	}

	
	
	/**
	 * Method to Prorate the State tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated tax value
	 */
	public double prorateStateTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateStateTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxStateLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateStateTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	/**
	 * Method to Prorate the City tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated city value
	 */
	public double prorateCityTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateCityTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxCityLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateCityTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	/**
	 * Method to count the State tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated county value
	 */
	public double prorateCountyTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateCountyTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxCountyLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateCountyTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	/**
	 * Method to Prorate the Secondary State tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated tax value
	 */
	public double prorateSecStateTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateSecStateTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxSecStateLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateSecStateTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	/**
	 * Method to Prorate the Secondry City tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated tax value
	 */
	public double prorateSecCityTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateSecCityTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxSecCityLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateSecCityTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	/**
	 * Method to Prorate the Secondary County tax.
	 * @param pTaxValue : double - Tax Value
	 * @param pLCommItemRel : TRUShippingGroupCommerceItemRelationship
	 * @param pGiftWrapRelQty : long - Item qty
	 * @param pListOfOrderlines : List
	 * @param pIsBPPItem : boolean - True if Prorating tax for BPP Item
	 * @param pIsNormalItem : boolean - True if Prorating tax for Normal Item
	 * @param pIsEWasteItem : boolean - True if Prorating tax for EWaste Item
	 * @return : double - Prorated tax value
	 */
	public double prorateSecCountyTax(double pTaxValue,TRUShippingGroupCommerceItemRelationship pLCommItemRel,
			long pGiftWrapRelQty, List<OrderLine> pListOfOrderlines,
			boolean pIsBPPItem,boolean pIsNormalItem,boolean pIsEWasteItem) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: prorateSecCountyTax]");
			vlogDebug("pTaxValue: {0} pLCommItemRel : {1} pGiftWrapRelQty : {2}",pTaxValue,pLCommItemRel,pGiftWrapRelQty);
		}
		if(pLCommItemRel == null){
			return pTaxValue;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		long quantity = pLCommItemRel.getQuantity();
		long totalQty = TRUConstants.LONG_ZERO;
		double totalShare = TRUConstants.DOUBLE_ZERO;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		if(pListOfOrderlines != null && !pListOfOrderlines.isEmpty()){
			for(OrderLine orderLine : pListOfOrderlines){
				if(orderLine.getRelationshipId().equals(pLCommItemRel.getId())){
					totalQty += orderLine.getOrderedQuantity();
					if(orderLine.getTaxDetails() != null && orderLine.getTaxDetails().getTaxDetail() != null 
							&& !orderLine.getTaxDetails().getTaxDetail().isEmpty()){
						for(TaxDetail taxDetail : orderLine.getTaxDetails().getTaxDetail()){
							if(taxDetail.getTaxName().equals(omsConfiguration.getTaxSecCountyLocationName())){
								if(pIsEWasteItem && EWASTE_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsNormalItem && NORMAL_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}else if(pIsBPPItem && BPP_ITEM.equals(taxDetail.getItemType())){
									totalShare += taxDetail.getTaxAmount();
								}
							}
						}
					}
				}
			}
		}
		double proratedTax = TRUConstants.DOUBLE_ZERO;
		if(totalQty+pGiftWrapRelQty<quantity){
			proratedTax = (pTaxValue*pGiftWrapRelQty)/quantity;
		}else{
			proratedTax = pTaxValue-totalShare;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: prorateSecCountyTax]");
			vlogDebug("Returning with proratedTax: {0}",proratedTax);
		}
		return pricingTools.round(proratedTax);
	}
	
	
	/**
	 * Method to get the Fraud Status from Payment Status.
	 * @param pOrder : Order Object
	 * @return boolean : True if Order is having any one  of payment status is in fraud state.
	 */
	@SuppressWarnings("unchecked")
	public boolean getFraudStatus(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getFraudStatus]");
			vlogDebug("pOrder: {0}",pOrder);
		}
		boolean isFraudOrder = Boolean.FALSE;
		if(pOrder == null){
			return isFraudOrder;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			for(PaymentGroup lPaymentGroup : paymentGroups){
				List<PaymentStatus> authorizationStatus = lPaymentGroup.getAuthorizationStatus();
				if(authorizationStatus != null && !authorizationStatus.isEmpty()){
					for(PaymentStatus status : authorizationStatus){
						/*if(status instanceof TRUPaymentStatus && StringUtils.isNotBlank(((TRUPaymentStatus)status).getRecommendationCode())){
							recommendationCode = ((TRUPaymentStatus)status).getRecommendationCode().toLowerCase();
							if(omsConfiguration.getOnHoldState() != null && omsConfiguration.getOnHoldState().contains(recommendationCode)){
								isFraudOrder = Boolean.TRUE;
								break;
							}
						}*/
					}
				}
				if(isFraudOrder){
					break;
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getFraudStatus]");
			vlogDebug("Returning with isFraudOrder: {0}",isFraudOrder);
		}
		return isFraudOrder;
	}
	
	/**
	 * Method to populate the transaction field for Gift Card Settlement.
	 * @param pPaymentGroup - PaymentGroup Object
	 * @param pPaymentDetail - PaymentDetail Object
	 */
	@SuppressWarnings("unchecked")
	public void populateTransFiledsForSettlement(PaymentGroup pPaymentGroup, PaymentDetail pPaymentDetail) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateTransFiledsForSettlement]");
			vlogDebug("pPaymentGroup: {0} pPaymentDetail : {1}",pPaymentGroup,pPaymentDetail);
		}
		if(pPaymentGroup == null || pPaymentDetail == null){
			return;
		}
		List<PaymentStatus> listOfGiftCardStatus = pPaymentGroup.getAuthorizationStatus();
		if (isLoggingDebug()) {
			vlogDebug("listOfCreditStatus: {0}",listOfGiftCardStatus);
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(listOfGiftCardStatus != null && !listOfGiftCardStatus.isEmpty()){
			DateFormat df1 = new SimpleDateFormat(DATE_FORMAT1,Locale.US);
			DateFormat df = new SimpleDateFormat(EST_DATE_FORMAT,Locale.US);
			df.setTimeZone(TimeZone.getTimeZone(omsConfiguration.getEstTimeZone()));
			PaymentTransaction payTrans = null;
			PaymentTransactionDetails paymentTrans = new PaymentTransactionDetails();
			List<PaymentTransaction> listOfPaymentTrns = new ArrayList<PaymentTransaction>();
			Date transactionExpDate = null;
			TRUPaymentStatus truPaymentStatus = null;
			for(PaymentStatus paymentStatus : listOfGiftCardStatus){
				if(!paymentStatus.getTransactionSuccess()){
					continue;
				}
				if(isFraudValidationReq(pPaymentGroup, paymentStatus) && !((TRUPaymentStatus)paymentStatus).isFraud()){
					continue;
				}
				payTrans = new PaymentTransaction();
				payTrans.setExternalPaymentTransactionId(paymentStatus.getTransactionId());
				payTrans.setRequestedAmount(pPaymentGroup.getAmount());
				payTrans.setProcessedAmount(paymentStatus.getAmount()/TRUConstants.HUNDERED);
				payTrans.setTransactionDecision(omsConfiguration.getTransactionDecisionSuccess());
				if(paymentStatus.getTransactionTimestamp() != null){
					payTrans.setRequestedDTTM(df.format(paymentStatus.getTransactionTimestamp()));
				}
				if(paymentStatus instanceof TRUPaymentStatus){
					truPaymentStatus = (TRUPaymentStatus) paymentStatus;
					if(truPaymentStatus.getTransactionDTTM() != null){
						payTrans.setTransactionDTTM(df.format(truPaymentStatus.getTransactionDTTM()));
						transactionExpDate = getTransactionExpDate(truPaymentStatus.getTransactionDTTM());
						if(transactionExpDate != null){
							payTrans.setTransactionExpDate(df1.format(transactionExpDate));
						}
					}
					payTrans.setRequestId(null);
					payTrans.setFollowOnId(null);
					payTrans.setFollowOnToken(null);
				}
				payTrans.setTransactionType(omsConfiguration.getSettlementTransactionType());
				payTrans.setRecordStatus(omsConfiguration.getPaymentTransactionRecordStatus());//Added this change as part of critical CR
				listOfPaymentTrns.add(payTrans);
			}
			if(!listOfPaymentTrns.isEmpty()){
				if(pPaymentDetail.getPaymentTransactionDetails() != null &&
						pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction() != null && 
								pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction().size() > TRUConstants.ZERO){
					listOfPaymentTrns.addAll(pPaymentDetail.getPaymentTransactionDetails().getPaymentTransaction());
				}
				paymentTrans.setPaymentTransaction(listOfPaymentTrns);
			}
			pPaymentDetail.setPaymentTransactionDetails(paymentTrans);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateTransFiledsForSettlement]");
		}
	}
	
	/**
	 * Method to check whether Order is Pay In Store or not. 
	 * 
	 * @param pOrder - Order Object
	 * @return boolean - True : If order is Pay in Store.
	 */
	@SuppressWarnings("unchecked")
	public boolean isPayInStoreOrder(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUOMSUtils  method: isPayInStoreOrder]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		boolean isPayInStore = Boolean.FALSE;
		if(pOrder == null){
			return isPayInStore;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		if(paymentGroups == null || paymentGroups.isEmpty()){
			return isPayInStore;
		}
		for(PaymentGroup lPG : paymentGroups){
			if(lPG instanceof InStorePayment){
				isPayInStore = Boolean.TRUE;
				break;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: isPayInStoreOrder]");
			vlogDebug("Order is Pay In Store : {0}" ,isPayInStore);
		}
		return isPayInStore;
	}
	
	/**
	 * Method to set the Bill To Details for Gift Card and Pay In Store Payment Group.
	 * @param pPaymentDetail - PaymentDetail Object
	 * @param pOrder - TRUOrderImpl Object.
	 */
	public void setBillToDetailsFromOrder(PaymentDetail pPaymentDetail,
			TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUOMSUtils  method: setBillToDetailsFromOrder]");
			vlogDebug("pPaymentDetail: {0} pOrder : {1}", pPaymentDetail,pOrder);
		}
		if(pOrder == null || pPaymentDetail == null){
			return;
		}
		BillToDetail billToDetail = new BillToDetail();
		billToDetail.setEmail(pOrder.getEmail());
		pPaymentDetail.setBillToDetail(billToDetail);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUOMSUtils  method: setBillToDetailsFromOrder]");
		}
	}
	
	/**
	 * Holds reference for mGiftWrapRel.
	 */
	private boolean mGiftWrapRel;
	
	/**
	 * Checks if is gift wrap rel.
	 *
	 * @return the giftWrapRel.
	 */
	public boolean isGiftWrapRel() {
		return mGiftWrapRel;
	}

	/**
	 * Sets the gift wrap rel.
	 *
	 * @param pGiftWrapRel the giftWrapRel to set.
	 */
	public void setGiftWrapRel(boolean pGiftWrapRel) {
		mGiftWrapRel = pGiftWrapRel;
	}
	
	/**
	 * Holds reference for mCommerceItemRelMapForOrderDis.
	 */
	public Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mCommerceItemRelMapForMulOrderDis = null;
	/**
	 * Holds reference for mCommerceItemRelMapForMulShipDis.
	 */
	public Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mCommerceItemRelMapForMulShipDis = null;
	/**
	 * Holds reference for mCommerceItemQTYMapForMulItemDis.
	 */
	public Map<CommerceItem, Map<String, Long>> mCommerceItemQTYMapForMulItemDis = null;
	
	/**
	 * Gets the commerce item rel map for mul order dis.
	 *
	 * @return the commerceItemRelMapForMulOrderDis
	 */
	public Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> getCommerceItemRelMapForMulOrderDis() {
		if(mCommerceItemRelMapForMulOrderDis == null){
			mCommerceItemRelMapForMulOrderDis = new HashMap<String, Map<TRUShippingGroupCommerceItemRelationship, Long>>();
		}
		return mCommerceItemRelMapForMulOrderDis;
	}

	/**
	 * Sets the commerce item rel map for mul order dis.
	 *
	 * @param pCommerceItemRelMapForMulOrderDis the commerceItemRelMapForMulOrderDis to set
	 */
	public void setCommerceItemRelMapForMulOrderDis(
			Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> pCommerceItemRelMapForMulOrderDis) {
		mCommerceItemRelMapForMulOrderDis = pCommerceItemRelMapForMulOrderDis;
	}

	/**
	 * Gets the commerce item rel map for mul ship dis.
	 *
	 * @return the commerceItemRelMapForMulShipDis
	 */
	public Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> getCommerceItemRelMapForMulShipDis() {
		if(mCommerceItemRelMapForMulShipDis == null){
			mCommerceItemRelMapForMulShipDis = new HashMap<String, Map<TRUShippingGroupCommerceItemRelationship, Long>>();
		}
		return mCommerceItemRelMapForMulShipDis;
	}

	/**
	 * Sets the commerce item rel map for mul ship dis.
	 *
	 * @param pCommerceItemRelMapForMulShipDis the commerceItemRelMapForMulShipDis to set
	 */
	public void setCommerceItemRelMapForMulShipDis(
			Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> pCommerceItemRelMapForMulShipDis) {
		mCommerceItemRelMapForMulShipDis = pCommerceItemRelMapForMulShipDis;
	}

	/**
	 * Gets the commerce item qty map for mul item dis.
	 *
	 * @return the commerceItemQTYMapForMulItemDis
	 */
	public Map<CommerceItem, Map<String, Long>> getCommerceItemQTYMapForMulItemDis() {
		if(mCommerceItemQTYMapForMulItemDis == null){
			mCommerceItemQTYMapForMulItemDis = new HashMap<CommerceItem, Map<String, Long>>();
		}
		return mCommerceItemQTYMapForMulItemDis;
	}

	/**
	 * Sets the commerce item qty map for mul item dis.
	 *
	 * @param pCommerceItemQTYMapForMulItemDis the commerceItemQTYMapForMulItemDis to set
	 */
	public void setCommerceItemQTYMapForMulItemDis(
			Map<CommerceItem, Map<String, Long>> pCommerceItemQTYMapForMulItemDis) {
		mCommerceItemQTYMapForMulItemDis = pCommerceItemQTYMapForMulItemDis;
	}
	
	/**
	 * Method to set the Shipping discount, relationship item, and Item QTY in Map.
	 * @param pLShipGroup - ShippingGroup
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForMulShipDis(ShippingGroup pLShipGroup) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setRelAndRelQtyForMulShipDis]");
			vlogDebug("pLShipGroup: {0}", pLShipGroup);
		}
		if(pLShipGroup == null){
			return;
		}
		ShippingPriceInfo priceInfo = pLShipGroup.getPriceInfo();
		if(priceInfo == null){
			return;
		}
		List<PricingAdjustment> pricingAdj = priceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		RepositoryItem promotion = null;
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulShipDis = getCommerceItemRelMapForMulShipDis();
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapDis = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if(promotion == null || -priceAdj.getAdjustment() <= TRUConstants.DOUBLE_ZERO){
				continue;
			}
			commerceItemRelMapDis = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
			for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
				commerceItemRelMapDis.put(rel,rel.getQuantity());
			}
			mulShipDis.put(promotion.getRepositoryId(), commerceItemRelMapDis);
		}
		setCommerceItemRelMapForMulShipDis(mulShipDis);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setRelAndRelQtyForMulShipDis]");
		}
		
	}

	/**
	 * Method to set the Order discount, relationship item, and Item QTY in Map.
	 * @param pLShipGroup - ShippingGroup
	 * @param pOrder - TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	public void setRelAndRelQtyForMulOrderDis(ShippingGroup pLShipGroup, TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setRelAndRelQtyForMulOrderDis]");
			vlogDebug("pLShipGroup: {0}  pOrder : {1}", pLShipGroup,pOrder);
		}
		if(pLShipGroup == null || pOrder == null){
			return;
		}
		OrderPriceInfo priceInfo = pOrder.getPriceInfo();
		if(priceInfo == null){
			return;
		}
		List<PricingAdjustment> pricingAdj = priceInfo.getAdjustments();
		if(pricingAdj == null || pricingAdj.isEmpty()){
			return;
		}
		RepositoryItem promotion = null;
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pLShipGroup.getCommerceItemRelationships();
		Map<String, Map<TRUShippingGroupCommerceItemRelationship, Long>> mulOrderDis = getCommerceItemRelMapForMulOrderDis();
		Map<TRUShippingGroupCommerceItemRelationship, Long> commerceItemRelMapDis = null;
		for(PricingAdjustment priceAdj : pricingAdj){
			promotion = priceAdj.getPricingModel();
			if(promotion == null || -priceAdj.getAdjustment() <= TRUConstants.DOUBLE_ZERO){
				continue;
			}
			commerceItemRelMapDis = new HashMap<TRUShippingGroupCommerceItemRelationship, Long>();
			for(TRUShippingGroupCommerceItemRelationship rel : commerceItemRelationships){
				commerceItemRelMapDis.put(rel,rel.getQuantity());
			}
			mulOrderDis.put(promotion.getRepositoryId(), commerceItemRelMapDis);
		}
		setCommerceItemRelMapForMulOrderDis(mulOrderDis);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setRelAndRelQtyForMulOrderDis]");
		}
	}
	
	/**
	 * Method to set the Item discount, Commerce item, and Item QTY in Map.
	 * @param pOrder - TRUOrderImpl
	 */
	@SuppressWarnings("unchecked")
	public void setCommerceItemQtyForMulItemDis(TRUOrderImpl pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: setCommerceItemQtyForMulItemDis]");
			vlogDebug("pOrder: {0}",pOrder);
		}
		if(pOrder == null){
			return;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			return;
		}
		ItemPriceInfo priceInfo = null;
		List<PricingAdjustment> pricingAdj = null;
		Map<CommerceItem, Map<String, Long>> mulitemDis = getCommerceItemQTYMapForMulItemDis();
		for (CommerceItem commerceItem : commerceItems) {
			if(commerceItem instanceof TRUGiftWrapCommerceItem){
				continue;
			}
			priceInfo = commerceItem.getPriceInfo();
			if(priceInfo == null){
				continue;
			}
			pricingAdj = priceInfo.getAdjustments();
			if(pricingAdj == null || pricingAdj.isEmpty()){
				return;
			}
			RepositoryItem promotion = null;
			Map<String, Long> commerceItemRelMapDis = new HashMap<String, Long>();
			for(PricingAdjustment priceAdj : pricingAdj){
				promotion = priceAdj.getPricingModel();
				if(promotion == null || -priceAdj.getAdjustment() <= TRUConstants.DOUBLE_ZERO){
					continue;
				}
				commerceItemRelMapDis.put(promotion.getRepositoryId(), commerceItem.getQuantity());
			}
			mulitemDis.put(commerceItem, commerceItemRelMapDis);
		}
		setCommerceItemQTYMapForMulItemDis(mulitemDis);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: setCommerceItemQTYMapForMulItemDis]");
		}
	}
	
	/**
	 * Holds reference for mLastShipGroup.
	 */
	private boolean mLastShipGroup;
	
	/**
	 * Checks if is last ship group.
	 *
	 * @return the lastShipGroup
	 */
	public boolean isLastShipGroup() {
		return mLastShipGroup;
	}

	/**
	 * Sets the last ship group.
	 *
	 * @param pLastShipGroup the lastShipGroup to set
	 */
	public void setLastShipGroup(boolean pLastShipGroup) {
		mLastShipGroup = pLastShipGroup;
	}
	
	/**
	 * Method to get the discount amount share.
	 * @param pOrderDiscountAmount : double amount to be share
	 * @param pCommItemRelAmount : double Commerce Item Relationship Amount
	 * @param pOrderSubTotalForProrate : double Order Amount.
	 * @param pPromoId : String
	 * @return double : discount amount share.
	 */
	public double getOrderDiscountToShare(double pOrderDiscountAmount,
			double pCommItemRelAmount, double pOrderSubTotalForProrate, String pPromoId) {
		double share = TRUConstants.DOUBLE_ZERO;
		PricingTools pricingTools = getOrderManager().getPromotionTools().getPricingTools();
		double amountDiscounted = TRUConstants.DOUBLE_ZERO;
		if(getOrderDisAndAmountMap().get(pPromoId) != null){
			amountDiscounted = getOrderDisAndAmountMap().get(pPromoId);
		}
		if(isLastShipGroup()){
			share = pOrderDiscountAmount-amountDiscounted;
		}else{
			share = (pOrderDiscountAmount*pCommItemRelAmount)/pOrderSubTotalForProrate;
			getOrderDisAndAmountMap().put(pPromoId, pricingTools.round(amountDiscounted+share));
		}
		return pricingTools.round(share);
	}
	
	/**
	 * Holds reference for mOrderDisAndAmountMap.
	 */
	public Map<String, Double> mOrderDisAndAmountMap = null;
	
	/**
	 * Gets the order dis and amount map.
	 *
	 * @return the orderDisAndAmountMap.
	 */
	public Map<String, Double> getOrderDisAndAmountMap() {
		if(mOrderDisAndAmountMap == null){
			mOrderDisAndAmountMap = new HashMap<String, Double>();
		}
		return mOrderDisAndAmountMap;
	}

	/**
	 * Sets the order dis and amount map.
	 *
	 * @param pOrderDisAndAmountMap the orderDisAndAmountMap to set.
	 */
	public void setOrderDisAndAmountMap(Map<String, Double> pOrderDisAndAmountMap) {
		mOrderDisAndAmountMap = pOrderDisAndAmountMap;
	}
	
	/**
	 * Method to populate the customer info for COI service from layaway order.
	 * @param pCustomerOrder - CustomerOrder
	 * @param pLayawayOrderImpl - TRULayawayOrderImpl
	 */
	public void populateLayawayCustomerInfoFromOrder(CustomerOrder pCustomerOrder,
			TRULayawayOrderImpl pLayawayOrderImpl){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateLayawayCustomerInfoFromOrder]");
		}
		CustomerInfo customerInfo = null;
		RepositoryItem profile;
		try {
			String phoneNumber = null;
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pLayawayOrderImpl);
			TRUPropertyManager propertyManager = getPropertyManager();
			customerInfo = new CustomerInfo();
			if(!profile.isTransient()){
				customerInfo.setExternalCustomerId(pLayawayOrderImpl.getProfileId());
				customerInfo.setCustomerUserId((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
				customerInfo.setCustomerFirstName((String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName()));
				customerInfo.setCustomerLastName((String) profile.getPropertyValue(propertyManager.getLastNamePropertyName()));
				phoneNumber = formatPhoneNumer(pLayawayOrderImpl.getPhoneNumber());
				customerInfo.setCustomerPhone(phoneNumber);
				customerInfo.setCustomerEmail((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName()));
			} else{
				customerInfo.setCustomerFirstName(getFirstName());
				customerInfo.setCustomerLastName(getLastName());
				phoneNumber = formatPhoneNumer(pLayawayOrderImpl.getPhoneNumber());
				customerInfo.setCustomerPhone(phoneNumber);
				customerInfo.setCustomerEmail(pLayawayOrderImpl.getEmail());
			}
			if(customerInfo != null){
				if(StringUtils.isBlank(customerInfo.getCustomerFirstName())){
					customerInfo.setCustomerFirstName(getFirstName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerLastName())){
					customerInfo.setCustomerLastName(getLastName());
				}
				if(StringUtils.isBlank(customerInfo.getCustomerPhone())){
					customerInfo.setCustomerPhone(formatPhoneNumer(pLayawayOrderImpl.getPhoneNumber()));
				}
				if(StringUtils.isBlank(customerInfo.getCustomerEmail())){
					customerInfo.setCustomerEmail(pLayawayOrderImpl.getEmail());
				}
				pCustomerOrder.setCustomerInfo(customerInfo);
			}
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : occurred while getting profile for order : {0} with exception : {1}", pLayawayOrderImpl,  exc);
			}
		}
		
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateLayawayCustomerInfoFromOrder]");
		}
	}
	
	/**
	 * This method will populate Header parameters for Order xml.
	 * @param pObjFactory - ObjectFactory
	 * @param pTXML - tXML
	 * @param pServiceName - String Service name.
	 * @param pOrderId - String - ATG Order ID
	 */
	public void populateXMLHeaderParameters(ObjectFactory pObjFactory, TXML pTXML, String pServiceName, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateXMLHeaderParameters]");
			vlogDebug("pObjFactory: {0} pTXML : {1} pOrderId : {2}",pObjFactory,pTXML,pOrderId);
		}
		if(pObjFactory == null || pTXML == null){
			return;
		}
		TRUOMSConfiguration configuration = getOmsConfiguration();
		Header createTXMLHeader = pObjFactory.createTXMLHeader();
		createTXMLHeader.setSource(configuration.getOrderXMLHeaderSource());
		if(UPDATE_SERVICE.equals(pServiceName)){
			createTXMLHeader.setActionType(configuration.getOrderXMLHeaderActionTypeUpdate());
		}else if(COI_SERVICE.equals(pServiceName)){
			createTXMLHeader.setActionType(configuration.getOrderXMLHeaderActionTypeCreate());
		}
		createTXMLHeader.setMessageType(configuration.getOrderXMLHeaderMessageType());
		createTXMLHeader.setCompanyID(configuration.getOrderXMLHeaderCompanyId());
		createTXMLHeader.setReferenceID(pObjFactory.createTXMLHeaderReferenceID(pOrderId));
		pTXML.setHeader(createTXMLHeader);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateXMLHeaderParameters]");
		}
	}
	
	/**
	 * This method will populate the Order details into CustomerOrder Object to
	 * generate the xml message for update service.
	 * 
	 * @param pCustomerOrderUpdateMessage
	 *            - TRUCustomerOrderUpdateMessage
	 *            
	 * @return String : xml request
	 */
	public String populateCustomerOrderXMLForUpdate(TRUCustomerOrderUpdateMessage pCustomerOrderUpdateMessage){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: populateCustomerOrderXMLForUpdate]");
			vlogDebug("pCustomerOrderUpdateMessage : {0}", pCustomerOrderUpdateMessage);
		}
		String customerUpdateXML = null;
		if(pCustomerOrderUpdateMessage == null){
			return customerUpdateXML;
		}
		ObjectFactory obj = new ObjectFactory();
		TXML tXML = obj.createTXML();
		populateXMLHeaderParameters(obj,tXML,UPDATE_SERVICE,pCustomerOrderUpdateMessage.getExternalOrderNumber());//Populate header parameters for Order xml
		populateCustomerOrderXMLForUpdate(obj,tXML,pCustomerOrderUpdateMessage);
		customerUpdateXML = generateXML(tXML);
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRUBaseOMSUtils  method: populateCustomerOrderXMLForUpdate]");
			vlogDebug("customerUpdateXML : {0}", customerUpdateXML);
		}
		return customerUpdateXML;
		
	}
	
	/**
	 * This method is use to generate the XML form JAXB Object.
	 * @param pTXML - TXML
	 * @return xmlRequest - String XML
	 */
	public String generateXML(TXML pTXML) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: generateXML]");
			vlogDebug("pTXML : {0}", pTXML);
		}
		String xmlRequest = TRUConstants.EMPTY;
		if(pTXML == null){
			return xmlRequest;
		}
		StringWriter sw = new StringWriter();
		try {
			JAXBContext	context = JAXBContext.newInstance(TRUOMSConstant.XML_PACKAGE);
			Marshaller marshaller = context.createMarshaller();
	        marshaller.setProperty(TRUOMSConstant.JAXB_PACKAGE,Boolean.TRUE);
	        marshaller.marshal(pTXML,sw);
	        xmlRequest = sw.toString();
	        if (isLoggingDebug()) {
	        	vlogDebug("xmlRequest : {0}", xmlRequest);
			}
	        sw.close();
		} catch (JAXBException exc) {
			if (isLoggingError()) {
				vlogError("JAXBException : {0}", exc);
			}
		} catch (IOException exc) {
			if (isLoggingError()) {
				vlogError("IOException : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: generateXML]");
		}
		return xmlRequest;
	}
	
	/**
	 * This method is use for Setting a Customer Order and Customer Info parameters for
	 * update service.
	 * @param pObjectFactory - ObjectFactory
	 * @param pTXML - TXML
	 * @param pCustomerOrderUpdateMessage - TRUCustomerOrderUpdateMessage
	 */
	private void populateCustomerOrderXMLForUpdate(ObjectFactory pObjectFactory,
			TXML pTXML, TRUCustomerOrderUpdateMessage pCustomerOrderUpdateMessage) {
		if (isLoggingDebug()) {
			logDebug("Entering into[Class: TRUBaseOMSUtils  method: populateCustomerOrderXMLForUpdate]");
			vlogDebug("pTXML : {0} pObjectFactory : {1} pCustomerOrderUpdateMessage :{2}", pTXML,pObjectFactory,pCustomerOrderUpdateMessage);
		}
		if(pObjectFactory == null || pTXML == null || pCustomerOrderUpdateMessage == null){
			return;
		}
		Message message = pObjectFactory.createTXMLMessage();
		com.tru.xml.customerorder.TXML.Message.Order messageOrder = pObjectFactory.createTXMLMessageOrder();
		messageOrder.setOrderNumber(pObjectFactory.createTXMLMessageOrderOrderNumber(pCustomerOrderUpdateMessage.getExternalOrderNumber()));
		messageOrder.setCanceled(pObjectFactory.createTXMLMessageOrderCanceled(FALSE));
		com.tru.xml.customerorder.TXML.Message.Order.CustomerInfo customeInfo = pObjectFactory.createTXMLMessageOrderCustomerInfo();
		customeInfo.setCustomerId(pObjectFactory.createTXMLMessageOrderCustomerInfoCustomerId(pCustomerOrderUpdateMessage.getExternalCustomerId()));
		customeInfo.setCustomerUserId(pObjectFactory.createTXMLMessageOrderCustomerInfoCustomerUserId(pCustomerOrderUpdateMessage.getCustomerEmail()));
		messageOrder.setCustomerInfo(customeInfo);
		message.setOrder(messageOrder);
		pTXML.setMessage(message);
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: populateCustomerOrderXMLForUpdate]");
		}
	}

	/**
	 * Method to get the subType of nonMerch sku.
	 * @param pGiftItemInfo - RepositoryItem Object
	 * @param pOrder - Object
	 * @return String - sub type of NonMerch SKU
	 */
	public String getNonMerchSKUSubType(RepositoryItem pGiftItemInfo,
			Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Entering into[Class: TRUBaseOMSUtils  method: getNonMerchSKUSubType]");
			vlogDebug("pGiftItemInfo : {0} pOrder : {1}", pGiftItemInfo,pOrder);
		}
		String skuSubType = null;
		if(pGiftItemInfo == null || pOrder == null){
			return skuSubType;
		}
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) ((CustomCatalogTools)getOrderManager().getCatalogTools()).getCatalogProperties();
		String giftWrapCIId = (String) pGiftItemInfo.getPropertyValue(commercePropertyManager.getGiftWrapCommItemId());
		try {
			CommerceItem commerceItem = pOrder.getCommerceItem(giftWrapCIId);
			if(commerceItem == null){
				return skuSubType;
			}
			RepositoryItem giftWrapSKUItem = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
			if(giftWrapSKUItem != null){
				skuSubType = (String) giftWrapSKUItem.getPropertyValue(catalogProperties.getSubTypePropertyName());
			}
		} catch (CommerceItemNotFoundException exc) {
			if (isLoggingError()) {
				logError("CommerceItemNotFoundException TRUBaseOMSUtils @method: getNonMerchSKUSubType()",exc);
			}
		} catch (InvalidParameterException exc) {
			if (isLoggingError()) {
				logError("InvalidParameterException TRUBaseOMSUtils @method: getNonMerchSKUSubType()",exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getNonMerchSKUSubType]");
			vlogDebug("sku Sub Type : {0}", skuSubType);
		}
		return skuSubType;
	}
	
	/**
	 * Gets the shipping surcharge uid.
	 *
	 * @return String - ShippingSurcharge Sub Type nonMerchSKU id.
	 */
	public String getShippingSurchargeUID() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: getShippingSurchargeUID]");
		}
		String shippingSurchargeUID = null;
		TRUCatalogTools catalogTools = getCatalogTools();
		RepositoryItem shippingSurSKUItem = catalogTools.getDonationOrBPPSKUItem(getOmsConfiguration().getShippingSurSKUType());
		if (isLoggingDebug()) {
			vlogDebug("shippingSurSKUItem : {0}" ,shippingSurSKUItem);
		}
		if(shippingSurSKUItem != null){
			shippingSurchargeUID = shippingSurSKUItem.getRepositoryId();
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with shippingSurUID : {0}" ,shippingSurchargeUID);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getShippingSurchargeUID]");
		}
		return shippingSurchargeUID;
	}
	
	/**
	 * Gets the shipping sur uid with des.
	 *
	 * @return String - Shipping Surchange name combination of Description|SKUID|ProdId.
	 */
	public String getShippingSurUIDWithDes() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: getShippingSurUIDWithDes]");
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) getCatalogTools().getCatalogProperties();
		RepositoryItem shippingSurSKUItem = catalogTools.getDonationOrBPPSKUItem(getOmsConfiguration().getShippingSurSKUType());
		RepositoryItem shippingSurProdItem = null;
		String prodDisplayName = null;
		String skuDI = null;
		String proId = null;
		StringBuffer shippingSurUID = new StringBuffer();
		if(shippingSurSKUItem != null){
			shippingSurProdItem = catalogTools.getDefaultParentProducts(shippingSurSKUItem);
		}
		if(shippingSurProdItem != null){
			proId = shippingSurProdItem.getRepositoryId();
			prodDisplayName = (String) shippingSurProdItem.getPropertyValue(catalogProperties.getProductDisplayNamePropertyName());
			if(StringUtils.isNotBlank(prodDisplayName)){
				shippingSurUID.append(prodDisplayName+getOmsConfiguration().getUidPipeDelimeter());
			}
		}
		if(shippingSurSKUItem != null){
			skuDI = shippingSurSKUItem.getRepositoryId();
		}
		if(StringUtils.isNotBlank(skuDI)){
			shippingSurUID.append(skuDI+getOmsConfiguration().getUidPipeDelimeter());
		}
		if(StringUtils.isNotBlank(proId)){
			shippingSurUID.append(proId);
		}
		if(shippingSurUID != null && shippingSurUID.length() > TRUConstants.ZERO
				&& TRUOMSConstant.CHAR_PIPE_DELIMETER == shippingSurUID.charAt(shippingSurUID.length()-TRUConstants.ONE)){
			shippingSurUID.deleteCharAt(shippingSurUID.length()-TRUConstants.ONE);
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with shippingSurUID : {0}" ,shippingSurUID.toString());
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getShippingSurUIDWithDes]");
		}
		return shippingSurUID.toString();
	}
	
	/**
	 * Gets the layaway item uid.
	 *
	 * @return String - PaymentSkin Sub Type nonMerchSKU id.
	 */
	public String getLayawayItemUID() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: getLayawayItemUID]");
		}
		String layawayItemUID = null;
		TRUCatalogTools catalogTools = getCatalogTools();
		RepositoryItem layawaySKUItem = catalogTools.getDonationOrBPPSKUItem(getOmsConfiguration().getLayawaySKUType());
		if (isLoggingDebug()) {
			vlogDebug("layawaySKUItem : {0}" ,layawaySKUItem);
		}
		if(layawaySKUItem != null){
			layawayItemUID = layawaySKUItem.getRepositoryId();
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with layawayItemUID : {0}" ,layawayItemUID);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getLayawayItemUID]");
		}
		return layawayItemUID;
	}
	
	/**
	 * Gets the order sub total.
	 *
	 * @param pOrder - Order Object
	 * @return double - Order Sub-total excluding item discount, shipping, and handling charge.
	 */
	@SuppressWarnings("unchecked")
	public double getOrderSubTotal(Order pOrder){
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: getOrderSubTotal]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		double orderSubTotal = TRUConstants.DOUBLE_ZERO;
		if(pOrder == null){
			return orderSubTotal;
		}
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		if(commerceItems == null || commerceItems.isEmpty()){
			return orderSubTotal;
		}
		ItemPriceInfo priceInfo = null;
		for (CommerceItem commerceItem : commerceItems) {
			if(commerceItem instanceof TRUGiftWrapCommerceItem){
				continue;
			}
			priceInfo = commerceItem.getPriceInfo();
			if(priceInfo == null){
				continue;
			}
			orderSubTotal += priceInfo.getSalePrice()*commerceItem.getQuantity();
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with orderSubTotal : {0}" ,orderSubTotal);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getOrderSubTotal]");
		}
		return orderSubTotal;
	}
	
	/**
	 * Method to check whether taxware system is down or not.
	 * 
	 * @param pOrder - Order object
	 * @return isTaxWareDown - boolean
	 */
	@SuppressWarnings("rawtypes")
	public boolean isTaxWareDown(Order pOrder){
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: isTaxWareDown]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		boolean isTaxWareDown = Boolean.FALSE;
		if(pOrder == null){
			return isTaxWareDown;	
		}		
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		if(taxPriceInfo == null){
			return isTaxWareDown;	
		}
		Map shipItemRelationsTaxPriceInfos = taxPriceInfo.getShipItemRelationsTaxPriceInfos();
		if(shipItemRelationsTaxPriceInfos == null || shipItemRelationsTaxPriceInfos.isEmpty()){
			return isTaxWareDown;	
		}
		for(Object values : shipItemRelationsTaxPriceInfos.values()){
			if(!(values instanceof TRUTaxPriceInfo)){
				continue;
			}
			isTaxWareDown = ((TRUTaxPriceInfo)values).isTaxwareSystemDown();
			break;
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with isTaxWareDown : {0}" ,isTaxWareDown);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: isTaxWareDown]");
		}
		return isTaxWareDown;
	}
	
	/**
	 * Method to check whether Order is having only Gift Card Payment Group or not.
	 * 
	 * @param pOrder - Order object
	 * @return isOnlyGiftCardPG - boolean
	 */
	@SuppressWarnings("rawtypes")
	public boolean isOnlyGiftCardPG(Order pOrder){
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: isOnlyGiftCardPG]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		boolean isOnlyGiftCardPG = Boolean.FALSE;
		if(pOrder == null){
			return isOnlyGiftCardPG;	
		}
		List paymentGroups = pOrder.getPaymentGroups();
		if(paymentGroups == null || paymentGroups.isEmpty()){
			return isOnlyGiftCardPG;
		}
		for(Object pg : paymentGroups){
			if(pg instanceof TRUCreditCard || pg instanceof TRUPayPal || pg instanceof InStorePayment){
				isOnlyGiftCardPG = Boolean.FALSE;
				break;
			}
			isOnlyGiftCardPG = Boolean.TRUE;
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with isOnlyGiftCardPG : {0}" ,isOnlyGiftCardPG);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: isOnlyGiftCardPG]");
		}
		return isOnlyGiftCardPG;
	}
	
	/**
	 * Method to check whether fraud check is required or not before populating the transaction details.
	 * 
	 * @param pPaymentGroup - PaymentGroup Object
	 * @param pPaymentStatus - PaymentStatus Object
	 * @return isFraudValidationReq - boolean
	 */
	public boolean isFraudValidationReq(PaymentGroup pPaymentGroup,PaymentStatus pPaymentStatus){
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: isFraudValidationReq]");
			vlogDebug("pPaymentGroup : {0} pPaymentStatus : {1}" ,pPaymentGroup,pPaymentStatus);
		}
		boolean isFraudValidationReq = Boolean.FALSE;
		if(pPaymentGroup == null || pPaymentStatus == null){
			return isFraudValidationReq;
		}
		TRUPaymentStatus paymentStatus = null;
		if(pPaymentStatus instanceof TRUPaymentStatus){
			paymentStatus = (TRUPaymentStatus) pPaymentStatus;
		}
		TRUOMSConfiguration omsConfiguration = getOmsConfiguration();
		if(pPaymentGroup instanceof TRUCreditCard){
			isFraudValidationReq = Boolean.TRUE;
		}else if(pPaymentGroup instanceof TRUPayPal){
			isFraudValidationReq = Boolean.TRUE;
		}else if(pPaymentGroup instanceof TRUGiftCard && paymentStatus != null && 
				omsConfiguration.getTransactionTypePurchaseScore().equals(paymentStatus.getTransactionType())){
			isFraudValidationReq = Boolean.TRUE;
		}
		if (isLoggingDebug()) {
			vlogDebug("Returning with isFraudValidationReq : {0}" ,isFraudValidationReq);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: isFraudValidationReq]");
		}
		return isFraudValidationReq;
	}
	
	/**
	 * Method to format the tax rate value.
	 * @param pTaxRate - double
	 * @return taxRate - String formatted tax rate value.
	 */
	public String formatTaxRate(double pTaxRate){
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseOMSUtils  method: formatTaxRate]");
			vlogDebug("pTaxRate : {0}" ,pTaxRate);
		}
		String taxRate = null;
		TRUPricingTools pricingTools = (TRUPricingTools) getOrderManager().getPromotionTools().getPricingTools();
		// Calling method to round the value to 4 decimal value.
		double dTaxRate = pricingTools.round(pTaxRate/TRUConstants.HUNDERED, TRUConstants.SIX);
		taxRate = String.format(SIX_DECIMAL_FORMAT,dTaxRate);
		if (isLoggingDebug()) {
			vlogDebug("Returning with taxRate : {0}" ,taxRate);
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: formatTaxRate]");
		}
		return taxRate;
	}

	/**
	 * This method returns the shippingManager value.
	 *
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * This method sets the shippingManager with parameter value pShippingManager.
	 *
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}
	
}
