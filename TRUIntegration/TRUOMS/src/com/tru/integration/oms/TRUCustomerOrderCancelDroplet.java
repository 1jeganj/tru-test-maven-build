package com.tru.integration.oms;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.TRUConfiguration;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 *This droplet is to Call the cancel order service if customer cancels the order.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerOrderCancelDroplet extends DynamoServlet {
	
	/**
	 * Holds constant RESPONSE.
	 */
	public static final String RESPONSE = "response";
	/**
	 * Holds constant CANCEL_SERVICE.
	 */
	public static final String CANCEL_SERVICE = "Cancel Service";
	/**
	 * Holds constant SUCCESS.
	 */
	public static final String SUCCESS = "Success";
	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName ORDER_ID = ParameterName.getParameterName("orderId");

	/**
	 * Holds OmsUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	
	/**
	 * Holds OmsAuditTools.
	 */
	private TRUOMSAuditTools mOmsAuditTools;
	
	/** This holds the reference for tru configuration. */
	private TRUConfiguration mConfiguration;
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This method returns the omsAuditTools value.
	 *
	 * @return the omsAuditTools
	 */
	public TRUOMSAuditTools getOmsAuditTools() {
		return mOmsAuditTools;
	}

	/**
	 * This method sets the omsAuditTools with parameter value pOmsAuditTools.
	 *
	 * @param pOmsAuditTools the omsAuditTools to set
	 */
	public void setOmsAuditTools(TRUOMSAuditTools pOmsAuditTools) {
		mOmsAuditTools = pOmsAuditTools;
	}
	
	/**
	 * @return the configuration
	 */
	public TRUConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration
	 *            the configuration to set
	 */
	public void setConfiguration(TRUConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
	
	/**
	 * This service method is to call the customer order cancel service if
	 * customer chooses to cancel the order from my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderCancelDroplet method : service");
		}
		String customerCancelOrderJSONResponse = null;
		String orderId = (String) pRequest.getLocalParameter(ORDER_ID);
		if (isLoggingDebug()) {
			vlogDebug("Order id for cancel : {0}", orderId);
		}
		boolean saveOmsTransactionDetails = getOmsUtils().getOmsConfiguration().isSaveOmsTransactionDetails();
		String customerOrderCancelTransPrefix = getOmsUtils().getOmsConfiguration().getCustomerOrderCancelTransPrefix();
		Date date = Calendar.getInstance().getTime();
		String customerOrderRequestJSON = getOmsUtils().populateCustomerOrderObjectForCancel(orderId);
		if(isLoggingDebug()){
			vlogDebug("Customer Order Cancel JSON : {0}", customerOrderRequestJSON);
		}
		//Call Cancel order Service
		customerCancelOrderJSONResponse = getOmsUtils().omsCancelOrderServiceCall(TRUOMSConstant.EMPTY_STRING,
				TRUOMSConstant.EMPTY_STRING, getConfiguration().getOmsCancelOrderServiceURL(), customerOrderRequestJSON);
		if(isLoggingDebug()){
			vlogDebug("Response form OMS  : {0}", customerCancelOrderJSONResponse);
		}
		//Save Details to audit repository
		if(saveOmsTransactionDetails){
			getOmsAuditTools().createOMSAuditItem(customerOrderCancelTransPrefix+orderId, customerOrderRequestJSON, 
					customerCancelOrderJSONResponse, CANCEL_SERVICE, SUCCESS, date);
		}
		pRequest.setParameter(RESPONSE, customerCancelOrderJSONResponse);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderCancelDroplet method : service");
		}
	}

	
}
