package com.tru.integration.oms.scheduler;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.processor.TRUProcSendFulfillmentMessage;
import com.tru.common.TRUConfiguration;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSAuditTools;
import com.tru.integration.oms.TRUOMSConfiguration;
import com.tru.integration.oms.TRUOMSErrorTools;
import com.tru.integration.oms.TRUOMSUtils;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository in case of get success response.
 * @author Professional Access
 * @version 1.0
 */
public class TRUOMSCMIOURedeliveryScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;
	/**
	 * Holds reference for mIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	/**
	 * Holds reference for mSendFulfillmentMessage.
	 */
	private TRUProcSendFulfillmentMessage mSendFulfillmentMessage;
	
	/**
	 * Holds reference for mDurationToSendRequestAgain 
	 */
	private int mDurationToSendRequestAgain;
	
	/**
	 * Overriding OOTB method to re-deliver the failed OMS request.
	 * 
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUOMSRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			List<RepositoryItem> customerImportRepositoryItems = null;
			TRUIntegrationManager integrationManager = getIntegrationManager();
			TRUOMSErrorTools omsErrorTools = integrationManager.getOmsErrorTools();
			customerImportRepositoryItems = omsErrorTools.getAllCustomerImportFailedRequest(getDurationToSendRequestAgain(),getNumberOfTimeToSendRequest());
			TRUOMSUtils omsUtils = integrationManager.getOmsUtils();
			TRUConfiguration configuration = integrationManager.getConfiguration();
			TRUOMSAuditTools omsAuditTools = integrationManager.getOmsAuditTools();
			TRUOMSConfiguration omsConfiguration = omsUtils.getOmsConfiguration();
			boolean saveOmsTransactionDetails = omsConfiguration.isSaveOmsTransactionDetails();
			String responseStatus = null;
			String orderOrCustomerId = null;
			Date date = Calendar.getInstance().getTime();
			if(customerImportRepositoryItems != null && !customerImportRepositoryItems.isEmpty()){
				String customerImportJSONResponse = null;
				String customerMasterImportTransPrefix = omsConfiguration.getCustomerMasterImportTransPrefix();
				for (RepositoryItem customerImportRepositoryItem : customerImportRepositoryItems) {
					try {
						String customerImportJSONRequest=(String) customerImportRepositoryItem.getPropertyValue(omsErrorTools.getOmsErrorProperties().getOmsRequestPopertyName());
						customerImportJSONResponse = omsUtils.omsCustomerMasterServiceCall(TRUOMSConstant.EMPTY_STRING,TRUOMSConstant.EMPTY_STRING, 
								configuration.getOmsCustomerMasterServiceURL(), customerImportJSONRequest);
						if(isLoggingDebug()){
							vlogDebug("Re-sending custoer order update with request : {0} and response : {1}", customerImportJSONRequest,customerImportJSONResponse);
						}
						orderOrCustomerId = omsAuditTools.getOrderOrCustomerId(customerImportJSONRequest, TRUOMSConstant.CUSTOMER_MASTER_IMPORT);
						if(saveOmsTransactionDetails){
							omsAuditTools.createOMSAuditItem(customerMasterImportTransPrefix+orderOrCustomerId, customerImportJSONRequest, 
									customerImportJSONResponse, TRUOMSConstant.CUSTOMER_MASTER_IMPORT, TRUOMSConstant.SUCCESS, date);
						}
						responseStatus = integrationManager.getOmsAuditTools().getOrderRepsonseStausForAudit(customerImportJSONResponse,
								TRUOMSConstant.CUSTOMER_MASTER_IMPORT);
						if ((!StringUtils.isBlank(responseStatus))&& responseStatus.equals(TRUOMSConstant.SUCCESS)) {
							String orderId = (String) customerImportRepositoryItem.getPropertyValue(omsErrorTools.getOmsErrorProperties().getOrderIdPopertyName());
							RepositoryItem[] updatedFailedOrderItems = omsErrorTools.getUpdateOrderFailedRequest(orderId);
							for(RepositoryItem repoItem : updatedFailedOrderItems){
							String transactionId= (String) repoItem.getPropertyValue(omsErrorTools.getOmsConfiguration().getTransactionIdPopertyName());
							String request= (String) repoItem.getPropertyValue(omsErrorTools.getOmsConfiguration().getRequestPopertyName());
							getSendFulfillmentMessage().redeliverOrderMessages(transactionId, request);
							}
							integrationManager.getOmsErrorTools().deleteOMSErrorItem(customerImportRepositoryItem.getRepositoryId());
						}
					} catch (IOException exc) {
						if (isLoggingError()) {
							logError("IOException in TRUOMSRedeliveryScheduler.doScheduledTask", exc);
						}
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUOMSRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * This method gets sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage value
	 */
	public TRUProcSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with pSendFulfillmentMessage value
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRUProcSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}

	/**
	 * This method gets durationToSendRequestAgain value
	 *
	 * @return the durationToSendRequestAgain value
	 */
	public int getDurationToSendRequestAgain() {
		return mDurationToSendRequestAgain;
	}

	/**
	 * This method sets the durationToSendRequestAgain with pDurationToSendRequestAgain value
	 *
	 * @param pDurationToSendRequestAgain the durationToSendRequestAgain to set
	 */
	public void setDurationToSendRequestAgain(int pDurationToSendRequestAgain) {
		mDurationToSendRequestAgain = pDurationToSendRequestAgain;
	}
	
	/**
	 * Holds reference for mNumberOfTimeToSendRequest 
	 */
	private int mNumberOfTimeToSendRequest;
	/**
	 * This method gets mNumberOfTimeToSendRequest value
	 *
	 * @return the numberOfTimeToSendRequest value
	 */
	public int getNumberOfTimeToSendRequest() {
		return mNumberOfTimeToSendRequest;
	}

	/**
	 * This method sets the numberOfTimeToSendRequest with pNumberOfTimeToSendRequest value
	 *
	 * @param pNumberOfTimeToSendRequest the numberOfTimeToSendRequest to set
	 */
	public void setNumberOfTimeToSendRequest(int pNumberOfTimeToSendRequest) {
		mNumberOfTimeToSendRequest = pNumberOfTimeToSendRequest;
	}

}
