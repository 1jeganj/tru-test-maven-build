package com.tru.integration.oms.scheduler;

import java.util.Map;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.processor.TRUProcSendFulfillmentMessage;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSErrorTools;

/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository in case of get success response.
 * @author Professional Access
 * @version 1.0
 */
public class TRUOMSCOIRedeliveryScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;
	/**
	 * Holds reference for mIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	/**
	 * Holds reference for mSendFulfillmentMessage.
	 */
	private TRUProcSendFulfillmentMessage mSendFulfillmentMessage;
	
	/**
	 * Holds reference for mDurationToSendRequestAgain 
	 */
	private int mDurationToSendRequestAgain;
	
	/**
	 * Holds reference for mNumberOfTimeToSendRequest 
	 */
	private int mNumberOfTimeToSendRequest;
	
	/**
	 * Overriding OOTB method to re-deliver the failed OMS request.
	 * 
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			TRUIntegrationManager integrationManager = getIntegrationManager();
			TRUOMSErrorTools omsErrorTools = integrationManager.getOmsErrorTools();
			Map<String, String> allCOIFailedRequest = omsErrorTools.getAllCOIFailedRequest(getDurationToSendRequestAgain(),getNumberOfTimeToSendRequest());
			if(allCOIFailedRequest != null && !allCOIFailedRequest.isEmpty()){
				for (Map.Entry<String, String> entry : allCOIFailedRequest.entrySet()) {
					getSendFulfillmentMessage().redeliverOrderMessages(entry.getKey(), entry.getValue());
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: doScheduledTask() method :: Ends ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * This method gets sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage value
	 */
	public TRUProcSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with pSendFulfillmentMessage value
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRUProcSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}

	/**
	 * This method gets durationToSendRequestAgain value
	 *
	 * @return the durationToSendRequestAgain value
	 */
	public int getDurationToSendRequestAgain() {
		return mDurationToSendRequestAgain;
	}

	/**
	 * This method sets the durationToSendRequestAgain with pDurationToSendRequestAgain value
	 *
	 * @param pDurationToSendRequestAgain the durationToSendRequestAgain to set
	 */
	public void setDurationToSendRequestAgain(int pDurationToSendRequestAgain) {
		mDurationToSendRequestAgain = pDurationToSendRequestAgain;
	}

	/**
	 * This method gets mNumberOfTimeToSendRequest value
	 *
	 * @return the numberOfTimeToSendRequest value
	 */
	public int getNumberOfTimeToSendRequest() {
		return mNumberOfTimeToSendRequest;
	}

	/**
	 * This method sets the numberOfTimeToSendRequest with pNumberOfTimeToSendRequest value
	 *
	 * @param pNumberOfTimeToSendRequest the numberOfTimeToSendRequest to set
	 */
	public void setNumberOfTimeToSendRequest(int pNumberOfTimeToSendRequest) {
		mNumberOfTimeToSendRequest = pNumberOfTimeToSendRequest;
	}

}
