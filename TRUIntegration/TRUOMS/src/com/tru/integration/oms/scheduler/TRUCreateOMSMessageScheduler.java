package com.tru.integration.oms.scheduler;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.processor.TRUProcSendFulfillmentMessage;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.TRUOMSErrorTools;
import com.tru.integration.oms.constant.TRUOMSConstant;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository.
 */
public class TRUCreateOMSMessageScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds reference for mEnable 
	 */
	private boolean mEnable;
	
	/**
	 * Holds reference for mOmsErrorTools 
	 */
	private TRUOMSErrorTools mOmsErrorTools;
	
	/**
	 * mPropertyManager
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * Holds reference for mDurationToSendRequestAgain 
	 */
	private int mDurationToSendRequestAgain;
	
	/**
	 * Holds reference for mSendFulfillmentMessage 
	 */
	private TRUProcSendFulfillmentMessage mSendFulfillmentMessage;

	
	/**
	 *  Overriding OOTB method to re-deliver the failed OMS request.
	 *  
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,
			ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("TRUCreateOMSMessageScheduler :: doScheduledTask() method :: STARTS ");
		}
		String type = null;
		String profileId = null;
		String orderId = null;
		if (isEnable()) {
			RepositoryItem[] omsMessageItems = getOmsErrorTools().getOMSMessageItems(getDurationToSendRequestAgain());
			if (omsMessageItems != null && omsMessageItems.length > TRUConstants.INT_ZERO) {
				for (RepositoryItem errorMessage : omsMessageItems) {
					type = (String) errorMessage.getPropertyValue(getPropertyManager().getTypePropertyName());
					if (!StringUtils.isEmpty(type)) {
						if (type.equalsIgnoreCase(TRUOMSConstant.CUSTOMER_MASTER_IMPORT)) {
							profileId = (String) errorMessage.getPropertyValue(getPropertyManager().getMessagePropertyName());
							orderId = (String) errorMessage.getPropertyValue(getPropertyManager().getMessagePropertyName());
							if (!StringUtils.isEmpty(profileId) && !StringUtils.isEmpty(orderId)) {
								getOmsErrorTools().generateCustomerMasterObject(profileId,orderId);
							}
							getOmsErrorTools().removeItemFromOMSErrorRepo(errorMessage.getRepositoryId());
						} else if (type.equalsIgnoreCase(TRUOMSConstant.CUTOMER_ORDER_IMPORT_SERVICE) && !StringUtils.isEmpty(orderId) && getSendFulfillmentMessage() != null) {
							orderId = (String) errorMessage.getPropertyValue(getPropertyManager().getMessagePropertyName());
								getSendFulfillmentMessage().sendFailedOrders(orderId);
							//getOmsErrorTools().generateCustomerOrderObject(orderId);
							getOmsErrorTools().removeItemFromOMSErrorRepo(errorMessage.getRepositoryId());
						}
						if(isLoggingDebug()){
							vlogDebug("type : {0} and profileId : {1} and orderId : {2}", type,profileId,orderId);
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUCreateOMSMessageScheduler :: doScheduledTask() method :: END ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the omsErrorTools
	 */
	public TRUOMSErrorTools getOmsErrorTools() {
		return mOmsErrorTools;
	}

	/**
	 * @param pOmsErrorTools the omsErrorTools to set
	 */
	public void setOmsErrorTools(TRUOMSErrorTools pOmsErrorTools) {
		mOmsErrorTools = pOmsErrorTools;
	}
	
	/**
	 * This method returns the propertyManager value
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mDurationToSendRequestAgain
	 */
	public int getDurationToSendRequestAgain() {
		return mDurationToSendRequestAgain;
	}

	/**
	 * @param pDurationToSendRequestAgain the mDurationToSendRequestAgain to set
	 */
	public void setDurationToSendRequestAgain(int pDurationToSendRequestAgain) {
		this.mDurationToSendRequestAgain = pDurationToSendRequestAgain;
	}

	/**
	 * This method returns the sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage
	 */
	public TRUProcSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with parameter value pSendFulfillmentMessage
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRUProcSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}
	
}
