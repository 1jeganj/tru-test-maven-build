package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.commerce.order.processor.TRUProcSendFulfillmentMessage;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSUtils;

/**
 *This Sink is to receive the message to post Customer details to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerMasterImportSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds reference TRUOMSUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	/**
	 * mIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	/**
	 * mSendFulfillmentMessage.
	 */
	private TRUProcSendFulfillmentMessage mSendFulfillmentMessage;
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	
	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * This is to receive the message.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		String customerMasterJson = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUCustomerMasterImportMessage customerMasterImportMessage =  null;
		if(objMessage != null && objMessage.getObject() instanceof TRUCustomerMasterImportMessage){
			customerMasterImportMessage =  (TRUCustomerMasterImportMessage) objMessage.getObject();
			String orderId = customerMasterImportMessage.getOrderId();
			customerMasterJson = getOmsUtils().populateCustomerMasterImportDetails(customerMasterImportMessage);
			//Posting the JSON to OM service
			getIntegrationManager().callOMSServiceForCustomerImport(customerMasterJson,customerMasterImportMessage.getExternalCustomerId(),orderId);
			if(orderId != null && getSendFulfillmentMessage() != null){
				TRUCustomerOrderUpdateMessage customerOrderUpdateMessage = populateOrderUpdateMessage(customerMasterImportMessage);
				getSendFulfillmentMessage().sendOrderUpdateMessage(customerOrderUpdateMessage);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSink method : receiveMessage");
		}
	}
	/**
	 * This method is to send the order update message to sink.
	 * 
	 * @param pCustomerMasterImportMessage - CustomerMasterImportMessage
	 * @return customerOrderUpdateMessage
	 */
	public TRUCustomerOrderUpdateMessage populateOrderUpdateMessage(TRUCustomerMasterImportMessage pCustomerMasterImportMessage){
		if(isLoggingDebug()){
			logDebug(" Enter into TRUSendFulfillmentMessage.populateOrderUpdateMessage() ");
		}
		TRUCustomerOrderUpdateMessage customerOrderUpdateMessage = null;
			customerOrderUpdateMessage = new TRUCustomerOrderUpdateMessage();
			customerOrderUpdateMessage.setCustomerEmail(pCustomerMasterImportMessage.getCustomerEmail());
			customerOrderUpdateMessage.setExternalCustomerId(pCustomerMasterImportMessage.getExternalCustomerId());
			customerOrderUpdateMessage.setExternalOrderNumber(pCustomerMasterImportMessage.getOrderId());
		if(isLoggingDebug()){
			logDebug(" Exit from TRUSendFulfillmentMessage.populateOrderUpdateMessage() ");
		}
		return customerOrderUpdateMessage;
	}

	/**
	 * This method gets sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage value
	 */
	public TRUProcSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with pSendFulfillmentMessage value
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRUProcSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}	

}
