package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.Message;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

/**
 * This sink is to send failure order JSON messages.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerSubmitOrderErrorSink extends GenericService implements
		MessageSink {

	/**
	 * This Sink receives message and process it.
	 * @param pPortName - Port name
	 * @param pMessage - Message
	 * @throws JMSException - JMSException
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage)
			throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerSubmitOrderErrorSink method : receiveMessage");
		}
		
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerSubmitOrderErrorSink method : receiveMessage");
		}
	}

}
