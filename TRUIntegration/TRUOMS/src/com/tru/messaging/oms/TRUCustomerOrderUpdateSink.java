package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSUtils;

/**
 * This Sink is to receive the message to post Customer order update to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerOrderUpdateSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds OmsUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	/**
	 * Holds IntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	
	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * Receives customer order update message and posts to OMS.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUCustomerOrderUpdateMessage customerOrderUpdateMessage =  null;
		if(objMessage != null && objMessage.getObject() instanceof TRUCustomerOrderUpdateMessage){
			customerOrderUpdateMessage =  (TRUCustomerOrderUpdateMessage) objMessage.getObject();
			String customerOrderJSON = getOmsUtils().populateCustomerOrderObjectForUpdate(customerOrderUpdateMessage);
			//Need post the JSON to OM service
			if(!StringUtils.isBlank(customerOrderJSON)){
				getIntegrationManager().callOMSServiceForUpdateOrder(customerOrderJSON,customerOrderUpdateMessage.getExternalOrderNumber());
			}
		}
		
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderUpdateSink method : receiveMessage");
		}
	}

	
}
