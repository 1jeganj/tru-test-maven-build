package com.tru.messaging.oms;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSAuditTools;
import com.tru.integration.oms.TRUOMSUtils;

/**
 * This Sink is to receive the message to post Customer order update to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerOrderResponseMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds OmsUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	/**
	 * Holds IntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	
	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * Receives customer order update message and posts to OMS.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderResponseMessageSink method : receiveMessage");
		}
		TextMessage textMessage = null;
		TRUOMSAuditTools auditTools = getIntegrationManager().getOmsAuditTools();
		if(pMessage != null){
			textMessage = (TextMessage) pMessage;
		}
		if(textMessage != null){
			Serializable xmlResponseMessage = textMessage.getText();
			String correlationID = textMessage.getJMSCorrelationID();
			if(isLoggingDebug()){
				vlogDebug("Order message reponse with trans id : {0} is : {1}", correlationID,xmlResponseMessage);
			}
			if(correlationID != null && xmlResponseMessage != null){
				auditTools.updateAuditMessageItem(correlationID,xmlResponseMessage);
				if(isLoggingDebug()){
					vlogDebug("Updated audit message item with trans id : {0}", correlationID);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderResponseMessageSink method : receiveMessage");
		}
	}
}
