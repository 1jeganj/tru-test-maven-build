package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.commerce.fulfillment.SubmitOrder;
import atg.commerce.order.Order;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.commerce.fulfillment.TRUSubmitOrder;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSConfiguration;

/**
 * This class is to create the customer order XML .
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerSubmitOrderXMLMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds reference for use rest service.
	 */
	private boolean mUseRestService;
	
	/**
	 * Holds reference for TRUIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	/**
	 * Holds reference for TRUCustomerSubmitOrderXMLExtMessageSource.
	 */
	private TRUCustomerSubmitOrderXMLExtMessageSource mCustomerSubmitOrderXMLExtMessageSource;
	
	/**
	 * @return useRestService
	 */
	public boolean isUseRestService() {
		return mUseRestService;
	}

	/**
	 * @param pUseRestService - true/false
	 */
	public void setUseRestService(boolean pUseRestService) {
		mUseRestService = pUseRestService;
	}

	/**
	 * @return integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager - TRUIntegrationManager
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}
	
	/** 
	 * This method is used to receive JMS message.
	 * @param pPortName - OPort name
	 * @param pMessage - Message
	 * @throws JMSException -JMSException
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCustomerSubmitOrderXMLMessageSink  method: receiveMessage]");
			vlogDebug("Port name: {0} Message : {1} ", pPortName, pMessage);
		}
		TRUCustomerOrderUpdateMessage customerOrderUpdateMessage =  null;
		String customerOrderXML = null;
		ObjectMessage objMessage = (ObjectMessage) pMessage;
		if(objMessage != null && (objMessage.getObject() instanceof SubmitOrder)){
			SubmitOrder submitOrder =(SubmitOrder)objMessage.getObject();
			TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
			Order order = submitOrder.getOrder();
			TRUSubmitOrder soMessage = (TRUSubmitOrder) submitOrder;
			if((order instanceof TRULayawayOrderImpl && configuration.isLayawayOrderRequest()) || 
					configuration.isGenerateCOImportRequestInSync()){
				if(isLoggingDebug()){
					vlogDebug("Customer Order Import JSON request for Order Id : {0} is : {1} ", 
							soMessage.getOrder().getId(), soMessage.getCustomerOrderImportRequest()); 
				}
				getIntegrationManager().processCOImportRequest(soMessage.getCustomerOrderImportRequest(), 
						soMessage.getOrder().getId());
			}
			else{
				order = submitOrder.getOrder();
				customerOrderXML = getIntegrationManager().createCustomerOrderImportXMLRequest(order);
				getCustomerSubmitOrderXMLExtMessageSource().sendCustomerOrderImportMessage(order, customerOrderXML);
			}
		}else if(objMessage != null && (objMessage.getObject() instanceof TRUCustomerOrderUpdateMessage)){

			customerOrderUpdateMessage =  (TRUCustomerOrderUpdateMessage) objMessage.getObject();
			String customerOrderUpdateXML = getIntegrationManager().getOmsUtils().populateCustomerOrderXMLForUpdate(customerOrderUpdateMessage);
			if(isLoggingDebug()){
				vlogDebug("customerOrderUpdateXML : {0}", customerOrderUpdateXML);
			}
			//Need post the XML to source
			if(getCustomerSubmitOrderXMLExtMessageSource() != null){
				getCustomerSubmitOrderXMLExtMessageSource().sendCustomerOrderUpdateMessage(customerOrderUpdateMessage.getExternalOrderNumber(), customerOrderUpdateXML);
			}
			if(isLoggingDebug()){
				vlogDebug("customerOrderUpdateXML: {0}  posted to source", customerOrderUpdateXML);
			}
		
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCustomerSubmitOrderXMLMessageSink  method: receiveMessage]");
		}
	}

	/**
	 * This method returns the customerSubmitOrderXMLExtMessageSource value
	 *
	 * @return the customerSubmitOrderXMLExtMessageSource
	 */
	public TRUCustomerSubmitOrderXMLExtMessageSource getCustomerSubmitOrderXMLExtMessageSource() {
		return mCustomerSubmitOrderXMLExtMessageSource;
	}

	/**
	 * This method sets the customerSubmitOrderXMLExtMessageSource with parameter value pCustomerSubmitOrderXMLExtMessageSource
	 *
	 * @param pCustomerSubmitOrderXMLExtMessageSource the customerSubmitOrderXMLExtMessageSource to set
	 */
	public void setCustomerSubmitOrderXMLExtMessageSource(
			TRUCustomerSubmitOrderXMLExtMessageSource pCustomerSubmitOrderXMLExtMessageSource) {
		mCustomerSubmitOrderXMLExtMessageSource = pCustomerSubmitOrderXMLExtMessageSource;
	}

}
