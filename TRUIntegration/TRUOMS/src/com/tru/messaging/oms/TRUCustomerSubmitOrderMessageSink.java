package com.tru.messaging.oms;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.commerce.fulfillment.SubmitOrder;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.commerce.fulfillment.TRUSubmitOrder;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.common.TRUConstants;
import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSConfiguration;
import com.tru.integration.oms.constant.TRUOMSConstant;

/**
 * This class is to create the customer order json object.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerSubmitOrderMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds reference for use rest service.
	 */
	private boolean mUseRestService;
	
	/**
	 * Holds reference for TRUIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	/**
	 * @return useRestService
	 */
	public boolean isUseRestService() {
		return mUseRestService;
	}

	/**
	 * @param pUseRestService - true/false
	 */
	public void setUseRestService(boolean pUseRestService) {
		mUseRestService = pUseRestService;
	}

	/**
	 * @return integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager - TRUIntegrationManager
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}
	
	/** 
	 * This method is used to receive JMS message.
	 * @param pPortName - OPort name
	 * @param pMessage - Message
	 * @throws JMSException -JMSException
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCustomerSubmitOrderMessageSink  method: receiveMessage]");
			vlogDebug("Port name: {0} Message : {1} ", pPortName, pMessage);
		}
		ObjectMessage objMessage = (ObjectMessage) pMessage;
		if(objMessage != null && objMessage.getObject() instanceof SubmitOrder ){
			SubmitOrder submitOrder =(SubmitOrder)objMessage.getObject();
			TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
			Order order = submitOrder.getOrder();
			TRUSubmitOrder soMessage = (TRUSubmitOrder) submitOrder;
			if((order instanceof TRULayawayOrderImpl && configuration.isLayawayOrderRequest()) || 
					configuration.isGenerateCOImportRequestInSync()){
				if(isLoggingDebug()){
					vlogDebug("Customer Order Import JSON request for Order Id : {0} is : {1} ", 
							soMessage.getOrder().getId(), soMessage.getCustomerOrderImportRequest()); 
				}
				getIntegrationManager().processCOImportRequest(soMessage.getCustomerOrderImportRequest(), 
						soMessage.getOrder().getId());
			}
			else{
				order = submitOrder.getOrder();
				//Call REST service here to get JSON message
				if(isUseRestService()){
					generateCORequestUsingRestService(order.getId());
				} else{
					getIntegrationManager().createCustomerOrderImportRequest(order);
				}
			}
		}else if(objMessage != null && (objMessage.getObject() instanceof TRUCustomerOrderUpdateMessage)){
			TRUCustomerOrderUpdateMessage customerOrderUpdateMessage =  (TRUCustomerOrderUpdateMessage) objMessage.getObject();
			String customerOrderJSON = getIntegrationManager().getOmsUtils().populateCustomerOrderObjectForUpdate(customerOrderUpdateMessage);
			//Need post the JSON to OM service
			if(!StringUtils.isBlank(customerOrderJSON)){
				getIntegrationManager().callOMSServiceForUpdateOrder(customerOrderJSON,customerOrderUpdateMessage.getExternalOrderNumber());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCustomerSubmitOrderMessageSink  method: receiveMessage]");
		}
	}

	/**
	 * This method is used to generate CO Request using REST webservices.
	 * @param pOrderId - OrderId
	 */
	private void generateCORequestUsingRestService(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUCustomerSubmitOrderMessageSink  method: generateCORequestUsingRestService]");
		}
		TRUOMSConfiguration configuration = getIntegrationManager().getOmsUtils().getOmsConfiguration();
		try {
			URL url = new URL(configuration.getCoRequestRestURL()+pOrderId);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(TRUConstants.POST);
			conn.setRequestProperty(TRUConstants.CONTENT_TYPE, TRUConstants.APPLICATION_JSON);
			OutputStream os = conn.getOutputStream();
			os.flush();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new ProtocolException(TRUOMSConstant.HTTP_ERROR_CODE + conn.getResponseCode());
			}
			conn.disconnect();
		} catch (MalformedURLException exc) {
			if(isLoggingError()){
				vlogError(exc, "MalformedURLException excepton in method receiveMessage : {0}");
			}
		} catch (ProtocolException exc) {
			if(isLoggingError()){
				vlogError(exc, "ProtocolException excepton in method receiveMessage");
			}
		} catch (IOException exc) {
			if(isLoggingError()){
				vlogError(exc, "IOException excepton in method receiveMessage");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUCustomerSubmitOrderMessageSink  method: generateCORequestUsingRestService]");
		}
	}

	
}
