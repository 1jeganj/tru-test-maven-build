package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSUtils;

/**
 * This Sink is to receive the message to post Customer order update XML to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerOrderUpdateXMLMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds OmsUtils.
	 */
	private TRUOMSUtils mOmsUtils;
	/**
	 * Holds IntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	/**
	 * Holds TRUCustomerOrderUpdateXMLExtMessageSource.
	 */
	private TRUCustomerOrderUpdateXMLExtMessageSource mCustomerOrderUpdateXMLExtMessageSource;
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRUOMSUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRUOMSUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	
	/**
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}

	/**
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}

	/**
	 * Receives customer order update message and posts to source.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateXMLMessageSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUCustomerOrderUpdateMessage customerOrderUpdateMessage =  null;
		if(objMessage != null && objMessage.getObject() instanceof TRUCustomerOrderUpdateMessage){
			customerOrderUpdateMessage =  (TRUCustomerOrderUpdateMessage) objMessage.getObject();
			String customerOrderUpdateXML = getOmsUtils().populateCustomerOrderXMLForUpdate(customerOrderUpdateMessage);
			if(isLoggingDebug()){
				vlogDebug("customerOrderUpdateXML : {0}", customerOrderUpdateXML);
			}
			//Need post the XML to source
			if(getCustomerOrderUpdateXMLExtMessageSource() != null){
				getCustomerOrderUpdateXMLExtMessageSource().sendCustomerOrderUpdateMessage(customerOrderUpdateMessage.getExternalOrderNumber(), customerOrderUpdateXML);
			}
			if(isLoggingDebug()){
				vlogDebug("customerOrderUpdateXML: {0}  posted to source", customerOrderUpdateXML);
			}
		}
		
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderUpdateXMLMessageSink method : receiveMessage");
		}
	}

	/**
	 * This method returns the customerOrderUpdateXMLExtMessageSource value
	 *
	 * @return the customerOrderUpdateXMLExtMessageSource
	 */
	public TRUCustomerOrderUpdateXMLExtMessageSource getCustomerOrderUpdateXMLExtMessageSource() {
		return mCustomerOrderUpdateXMLExtMessageSource;
	}

	/**
	 * This method sets the customerOrderUpdateXMLExtMessageSource with parameter value pCustomerOrderUpdateXMLExtMessageSource
	 *
	 * @param pCustomerOrderUpdateXMLExtMessageSource the customerOrderUpdateXMLExtMessageSource to set
	 */
	public void setCustomerOrderUpdateXMLExtMessageSource(
			TRUCustomerOrderUpdateXMLExtMessageSource pCustomerOrderUpdateXMLExtMessageSource) {
		mCustomerOrderUpdateXMLExtMessageSource = pCustomerOrderUpdateXMLExtMessageSource;
	}

	
}
