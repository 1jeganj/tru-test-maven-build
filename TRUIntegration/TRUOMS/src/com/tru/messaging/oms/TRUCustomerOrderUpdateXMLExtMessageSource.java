package com.tru.messaging.oms;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.transaction.TransactionManager;

import atg.adapter.gsa.GSARepository;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integration.oms.TRUIntegrationManager;
import com.tru.integration.oms.TRUOMSErrorTools;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * TRUCustomerSubmitOrderXMLExtMessageSource.
 *
 * @author PA
 * @version 1.0
 */
public class TRUCustomerOrderUpdateXMLExtMessageSource extends GenericService implements
		MessageSource {
	
	/**
	 * mPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * mPortName.
	 */
	private String mPortName;
	
	/**
	 * Started.
	 */
	private boolean mStarted;
	
	/**
	 * Holds reference for mTransactionManager.
	 */
	private TransactionManager mTransactionManager;
	
	/**
	 * mMessageSourceContext.
	 */
	private MessageSourceContext mMessageSourceContext;
	
	/**
	 * mOmsErrorRepository.
	 */
	private GSARepository mOmsErrorRepository;
	
	/**
	 * Holds mTransactionToRollback.
	 */
	private boolean mTransactionToRollback;
	/**
	 * Holds TRUOMSErrorTools.
	 */
	private TRUOMSErrorTools mOmsErrorTools;
	/**
	 * Holds TRUIntegrationManager.
	 */
	private TRUIntegrationManager mIntegrationManager;
	
	/**
	 * This method is to send the master import message.
	 * @param pOrderNumber - Order
	 * @param pCustomerUpdateOrderXML - String order xml
	 */
	public void sendCustomerOrderUpdateMessage(String pOrderNumber, String pCustomerUpdateOrderXML){
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderUpdateXMLExtMessageSource method : sendCustomerOrderUpdateMessage");
			vlogDebug("pOrder : {0}  Customer order update xml to send : {1}", pOrderNumber, pCustomerUpdateOrderXML);
		}
		ObjectMessage objectMessage = null;
		if (pCustomerUpdateOrderXML != null && getMessageSourceContext() != null) {
			try{
				//Create object message
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				//Set Object message
				objectMessage.setObject(pCustomerUpdateOrderXML);
				//Send Customer Master Import Message
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				if(isLoggingDebug()){
					vlogDebug("Customer order update xml sent to external queue : {0}",  pCustomerUpdateOrderXML);
				}
				//Process the generated CO request
				getIntegrationManager().processCOUpdateXMLRequest(pCustomerUpdateOrderXML, pOrderNumber);
				if(isLoggingDebug()){
					vlogDebug("Customer order update xml   :   {0} has been audited.",  pCustomerUpdateOrderXML);
				}
			} catch (JMSException exc) {
				/*if (pOrderNumber != null) {
					getOmsErrorTools().updateOmsErrorItemForOrder(pOrderNumber);
				}*/
				if(isLoggingError()){
					vlogError("JMSException : while connecting to queue with port : {0} and exception is : {1}",getPortName(), exc);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderUpdateXMLExtMessageSource method : sendCustomerOrderUpdateMessage");
		}
	}


	/** 
	 * MessageSourceContext.
	 * @param pMessageSourceContext - MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pMessageSourceContext) {
		mMessageSourceContext = pMessageSourceContext;
	}

	/**
	 * startMessageSource.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : startMessageSource");
		}
		mStarted = true;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSource method : startMessageSource");
		}
	}

	/**
	 * stopMessageSource.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerMasterImportSource method : stopMessageSource");
		}
		mStarted = false;
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerMasterImportSource method : stopMessageSource");
		}
	}

	/**
	 * This method returns the started value.
	 *
	 * @return the started
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method sets the started with parameter value pStarted.
	 *
	 * @param pStarted the started to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/**
	 * This method returns the context value.
	 *
	 * @return the context
	 */
	public MessageSourceContext getMessageSourceContext() {
		return mMessageSourceContext;
	}

	/**
	 * This method returns the portName value.
	 *
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * This method sets the portName with parameter value pPortName.
	 *
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the oms error repository.
	 *
	 * @return the oms error repository
	 */
	public GSARepository getOmsErrorRepository() {
		return mOmsErrorRepository;
	}

	/**
	 * Sets the oms error repository.
	 *
	 * @param pOmsErrorRepository the new oms error repository
	 */
	public void setOmsErrorRepository(GSARepository pOmsErrorRepository) {
		mOmsErrorRepository = pOmsErrorRepository;
	}
	
	/**
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	
	/**
	 * This method returns the transactionToRollback value.
	 *
	 * @return the transactionToRollback
	 */
	public boolean isTransactionToRollback() {
		return mTransactionToRollback;
	}

	/**
	 * This method sets the transactionToRollback with parameter value pTransactionToRollback.
	 *
	 * @param pTransactionToRollback the transactionToRollback to set
	 */
	public void setTransactionToRollback(boolean pTransactionToRollback) {
		mTransactionToRollback = pTransactionToRollback;
	}


	/**
	 * This method returns the omsErrorTools value
	 *
	 * @return the omsErrorTools
	 */
	public TRUOMSErrorTools getOmsErrorTools() {
		return mOmsErrorTools;
	}


	/**
	 * This method sets the omsErrorTools with parameter value pOmsErrorTools
	 *
	 * @param pOmsErrorTools the omsErrorTools to set
	 */
	public void setOmsErrorTools(TRUOMSErrorTools pOmsErrorTools) {
		mOmsErrorTools = pOmsErrorTools;
	}


	/**
	 * This method returns the integrationManager value
	 *
	 * @return the integrationManager
	 */
	public TRUIntegrationManager getIntegrationManager() {
		return mIntegrationManager;
	}


	/**
	 * This method sets the integrationManager with parameter value pIntegrationManager
	 *
	 * @param pIntegrationManager the integrationManager to set
	 */
	public void setIntegrationManager(TRUIntegrationManager pIntegrationManager) {
		mIntegrationManager = pIntegrationManager;
	}
}
