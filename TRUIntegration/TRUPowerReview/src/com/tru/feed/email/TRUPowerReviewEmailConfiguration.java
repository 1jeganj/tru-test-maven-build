package com.tru.feed.email;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;


/**
 * The Class TRUPowerReviewEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUPowerReviewEmailConfiguration extends GenericService {

	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mContactSuccessEmailInfo;

	/** Property to hold Failure Template Email Info. */
	private TemplateEmailInfoImpl mContactFailureEmailInfo;

	/** Property to hold Recipients. */
	private List<String> mRecipients;

	/** Property to hold PowerReviewFeedEnv. */
	private String mPowerReviewFeedEnv;
	
	/** Property to hold EmailSuccessMessage. */
	private String mEmailSuccessMessage;
	
	/** Property to hold EmailFailureMessage. */
	private String mEmailFailureMessage;
	
	/** Property to hold mFileName. */
	private String mFileName;
	
	/** Property to hold mRecordNumber. */
	private String mRecordNumber;
	
	/** Property to hold mTimeStamp. */
	private String mTimeStamp;
	
	/** Property to hold EmailFailmSkuureMessage. */
	private String mSku;
	
	/** Property to hold mOnlinePid. */
	private String mOnlinePid;
	
	/** Property to hold mAttachmentException. */
	private String mAttachmentException;
	
	
	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param pTimeStamp the new time stamp
	 */
	public void setTimeStamp(String pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the new file name
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the record number.
	 *
	 * @return the record number
	 */
	public String getRecordNumber() {
		return mRecordNumber;
	}

	/**
	 * Sets the record number.
	 *
	 * @param pRecordNumber the new record number
	 */
	public void setRecordNumber(String pRecordNumber) {
		mRecordNumber = pRecordNumber;
	}

	/**
	 * Gets the sku.
	 *
	 * @return the sku
	 */
	public String getSku() {
		return mSku;
	}

	/**
	 * Sets the sku.
	 *
	 * @param pSku the new sku
	 */
	public void setSku(String pSku) {
		mSku = pSku;
	}

	/**
	 * Gets the online pid.
	 *
	 * @return the online pid
	 */
	public String getOnlinePid() {
		return mOnlinePid;
	}

	/**
	 * Sets the online pid.
	 *
	 * @param pOnlinePid the new online pid
	 */
	public void setOnlinePid(String pOnlinePid) {
		mOnlinePid = pOnlinePid;
	}

	/**
	 * Gets the attachment exception.
	 *
	 * @return the attachment exception
	 */
	public String getAttachmentException() {
		return mAttachmentException;
	}

	/**
	 * Sets the attachment exception.
	 *
	 * @param pAttachmentException the new attachment exception
	 */
	public void setAttachmentException(String pAttachmentException) {
		mAttachmentException = pAttachmentException;
	}

	/**
	 * Gets the email success message.
	 *
	 * @return the email success message
	 */
	public String getEmailSuccessMessage() {
		return mEmailSuccessMessage;
	}

	/**
	 * Sets the email success message.
	 *
	 * @param pEmailSuccessMessage the new email success message
	 */
	public void setEmailSuccessMessage(String pEmailSuccessMessage) {
		mEmailSuccessMessage = pEmailSuccessMessage;
	}

	/**
	 * Gets the email failure message.
	 *
	 * @return the email failure message
	 */
	public String getEmailFailureMessage() {
		return mEmailFailureMessage;
	}

	/**
	 * Sets the email failure message.
	 *
	 * @param pEmailFailureMessage the new email failure message
	 */
	public void setEmailFailureMessage(String pEmailFailureMessage) {
		mEmailFailureMessage = pEmailFailureMessage;
	}

	/**
	 * Gets the power review feed env.
	 *
	 * @return the power review feed env
	 */
	public String getPowerReviewFeedEnv() {
		return mPowerReviewFeedEnv;
	}

	/**
	 * Sets the power review feed env.
	 *
	 * @param pPowerReviewFeedEnv the new power review feed env
	 */
	public void setPowerReviewFeedEnv(String pPowerReviewFeedEnv) {
		mPowerReviewFeedEnv = pPowerReviewFeedEnv;
	}

	/**
	 * @return the ContactSuccessEmailInfo
	 */
	public TemplateEmailInfoImpl getContactSuccessEmailInfo() {
		return mContactSuccessEmailInfo;
	}

	/**
	 * @param pContactSuccessEmailInfo the akamaiSuccessEmailInfo to set
	 */
	public void setContactSuccessEmailInfo(
			TemplateEmailInfoImpl pContactSuccessEmailInfo) {
		mContactSuccessEmailInfo = pContactSuccessEmailInfo;
	}

	/**
	 * @return the ContactFailureEmailInfo
	 */
	public TemplateEmailInfoImpl getContactFailureEmailInfo() {
		return mContactFailureEmailInfo;
	}

	/**
	 * @param pContactFailureEmailInfo the akamaiFailureEmailInfo to set
	 */
	public void setContactFailureEmailInfo(
			TemplateEmailInfoImpl pContactFailureEmailInfo) {
		mContactFailureEmailInfo = pContactFailureEmailInfo;
	}

	/**
	 * Gets the recipients.
	 *
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 *
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}


}
