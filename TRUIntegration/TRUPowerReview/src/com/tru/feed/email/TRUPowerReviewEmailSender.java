package com.tru.feed.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.TransactionManager;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.FeedErrorInfo;


/**
 * This class is used to send an Email on PowerReview to Users listed.
 * @version 1.0
 * @author PA
 */
public class TRUPowerReviewEmailSender extends GenericService {

	/**
	 * Holds the contactItemDescriptor
	 */
	private String mContactItemDescriptor;

	/**
	 * Holds the TransactionManager
	 */
	private TransactionManager mTransactionManager ;

	/**
	 * Holds reference for MessageContentRepository.
	 */
	private GSARepository mAuditRepository;


	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;

	/** Property to hold mContactEmailConfiguration. */
	private TRUPowerReviewEmailConfiguration mContactEmailConfiguration;


	/**
	 * This method used to send success email on Power Reviews
	 *
	 * @param pMapObject the map object
	 * @param pErrorInfoList the error info list
	 */
	@SuppressWarnings("unchecked")
	public void sendSuccessEmail(Map<String, Object> pMapObject, List<FeedErrorInfo> pErrorInfoList) {
		if (isLoggingDebug()) {
			logDebug("TRUPowerReviewEmailSender :: sendContactUsSuccessEmail() :: START");
		}
		File attachmentFile = null;
		TemplateEmailInfoImpl emailInfo = mContactEmailConfiguration.getContactSuccessEmailInfo();
		SimpleDateFormat formatter = new SimpleDateFormat(TRURRInFeedConstants.PWRTIME, Locale.US);
		String smpDate = formatter.format(new Date());
		pMapObject.put(TRURRInFeedConstants.FEED_TIMEZONE, smpDate);
		clearEmailInfo(emailInfo);

		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(pMapObject);
		} else {
			emailInfo.setTemplateParameters(pMapObject);
		}
		try {
			// pMapObject.get("message");
			String formattedSubject = String.format(mContactEmailConfiguration.getEmailSuccessMessage(),
					mContactEmailConfiguration.getPowerReviewFeedEnv(), smpDate);
			emailInfo.setMessageSubject(formattedSubject);

			String errorDir = (String) pMapObject.get(TRURRInFeedConstants.ERROR_DIRECTORY);
			String fileName = (String) pMapObject.get(TRURRInFeedConstants.XML_FILE_NAME);
			attachmentFile = createAttachmentFile(pErrorInfoList, fileName, errorDir);
			if (attachmentFile != null) {
				File arg[] = new File[TRURRInFeedConstants.INT_ONE];
				arg[TRURRInFeedConstants.INT_ZERO] = attachmentFile;
				emailInfo.setMessageAttachments(arg);
			}
			
			emailInfo.setFillFromTemplate(!TRURRInFeedConstants.FALSE_FLAG);
			getTemplateEmailSender().sendEmailMessage(emailInfo, mContactEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			if (isLoggingError()) {
				logError(e.getMessage(), e);
			}
		} finally {
			if (isLoggingDebug()) {
				logDebug("Entered into finally block");
			}
			if(attachmentFile != null){
				attachmentFile.delete();
			}
		}

	}

	
	/**
	 * This method used to clear email information.
	 *
	 * @param pEmailInfo the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: TRUPowerReviewEmailSender : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: TRUPowerReviewEmailSender : @Method: clearEmailInfo()");
	}

	
	/**
	 * This method used to send failure email.
	 *
	 * @param pMapObject the map object
	 * @param pErrorInfoList the error info list
	 */
	public void sendFailureEmail(Map<String, Object> pMapObject, List<FeedErrorInfo> pErrorInfoList) {
		if (isLoggingDebug()) {
			logDebug("TRUPowerReviewEmailSender :: sendContactUsFailureEmail() :: START");
		}
		File attachmentFile = null;
		try {
			TemplateEmailInfoImpl emailInfo = mContactEmailConfiguration.getContactFailureEmailInfo();
			clearEmailInfo(emailInfo);

			SimpleDateFormat formatter = new SimpleDateFormat(TRURRInFeedConstants.PWRTIME, Locale.US);
			String smpDate = formatter.format(new Date());

			pMapObject.put(TRURRInFeedConstants.FEED_TIMEZONE, smpDate);

			String formattedSubject = String.format(mContactEmailConfiguration.getEmailFailureMessage(),
					mContactEmailConfiguration.getPowerReviewFeedEnv(), smpDate);
			
			emailInfo.setMessageSubject(formattedSubject);
			String errorDir = (String) pMapObject.get(TRURRInFeedConstants.ERROR_DIRECTORY);
			String fileName = (String) pMapObject.get(TRURRInFeedConstants.XML_FILE_NAME);
			attachmentFile = createAttachmentFile(pErrorInfoList, fileName, errorDir);
			
			if (attachmentFile != null) {
				File arg[] = new File[TRURRInFeedConstants.INT_ONE];
				arg[TRURRInFeedConstants.INT_ZERO] = attachmentFile;
				emailInfo.setMessageAttachments(arg);
			}

			pMapObject.put(TRURRInFeedConstants.FEED_TIMEZONE, smpDate);
			emailInfo.setTemplateParameters(pMapObject);
			
			getTemplateEmailSender().sendEmailMessage(emailInfo, mContactEmailConfiguration.getRecipients());
			
		} catch (TemplateEmailException e) {
			if (isLoggingError()) {
				logError(e.getMessage(), e);
			}
		}finally{
			if(attachmentFile != null){
				attachmentFile.delete();
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUPowerReviewEmailSender :: sendContactUsFailureEmail() :: END");
		}
	}
	
	
	/**
	 * Creates the attachment file.
	 *
	 * @param pErrorInfoList the error info list
	 * @param pFileName the file name
	 * @param pErrorDir the error directory
	 * @return file
	 */
	public File createAttachmentFile(List<FeedErrorInfo> pErrorInfoList, String pFileName, String pErrorDir) {

		vlogDebug("Start @Class: TRUPowerReviewEmailSender, @method: createAttachmentFile()");
		File attachement = null;
		if (pErrorInfoList != null && !pErrorInfoList.isEmpty() && !StringUtils.isBlank(pFileName)
				&& !StringUtils.isBlank(pErrorDir)) {
			File errorFolder = new File(pErrorDir);
			
			if (!errorFolder.exists() && !errorFolder.isDirectory()) {
				errorFolder.mkdirs();
			}
			
			attachement = new File(errorFolder, attachementFileName());
			
			try {
				productAttachment(pErrorInfoList, attachement);
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class TRUCatFeedExecutionDataCollectorTasklet method createAttachmentFile()",
							ioe);
				}
			}
		}

		vlogDebug("End @Class: TRUPowerReviewEmailSender, @method: createAttachmentFile()");

		return attachement;
	}
	
	
	/**
	 * Attachement file name.
	 *
	 * @return the string
	 */
	public String attachementFileName() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = stringBuffer.append(TRURRInFeedConstants.ERRORS_POWERREVIEW_FEED)
				.append(TRURRInFeedConstants.PROCESS_ON)
				.append(((new SimpleDateFormat(TRURRInFeedConstants.FEED_DATE_FORMAT_1)).format(new Date())))
				.append(TRURRInFeedConstants.DOT).append(TRURRInFeedConstants.XLS);
		return stringBuffer.toString();
	}
	
	/**
	 * Product attachment.
	 *
	 * @param pErrorInfoList the error info list
	 * @param pAttachment the attachement
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void productAttachment(List<FeedErrorInfo> pErrorInfoList,File pAttachment)throws FileNotFoundException, IOException {
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		outputStream = new FileOutputStream(pAttachment);
		workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(TRURRInFeedConstants.SHEET);
		int rowCount = TRURRInFeedConstants.INT_ZERO;
		HSSFRow rowhead = sheet.createRow(rowCount);
		HSSFCellStyle rowCellStyle = workbook.createCellStyle();
		rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		productAttachHeader(rowhead, rowCellStyle);
		
		rowCount = TRURRInFeedConstants.INT_ONE;
		HSSFCellStyle dataCellStyle = workbook.createCellStyle();
		dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
		
		rowCount = productAttachData(pErrorInfoList, sheet, rowCount,dataCellStyle);
		
		if (null != workbook) {
			workbook.write(outputStream);
		}
		outputStream.close();
	}
	
	/**
	 * Product attach header.
	 *
	 * @param pRowHead the row head
	 * @param pRowCellStyle the row cell style
	 */
	private void productAttachHeader(HSSFRow pRowHead,HSSFCellStyle pRowCellStyle) {
		pRowHead.createCell(TRURRInFeedConstants.INT_ZERO).setCellValue(mContactEmailConfiguration.getFileName());
		pRowHead.createCell(TRURRInFeedConstants.INT_ONE).setCellValue(mContactEmailConfiguration.getRecordNumber());
		pRowHead.createCell(TRURRInFeedConstants.INT_TWO).setCellValue(mContactEmailConfiguration.getTimeStamp());
		pRowHead.createCell(TRURRInFeedConstants.INT_THREE).setCellValue(mContactEmailConfiguration.getSku());
		pRowHead.createCell(TRURRInFeedConstants.INT_FOUR).setCellValue(mContactEmailConfiguration.getOnlinePid());
		pRowHead.createCell(TRURRInFeedConstants.INT_FIVE).setCellValue(mContactEmailConfiguration.getAttachmentException());
		
		pRowHead.getCell(TRURRInFeedConstants.INT_ZERO).setCellStyle(pRowCellStyle);
		pRowHead.getCell(TRURRInFeedConstants.INT_ONE).setCellStyle(pRowCellStyle);
		pRowHead.getCell(TRURRInFeedConstants.INT_TWO).setCellStyle(pRowCellStyle);
		pRowHead.getCell(TRURRInFeedConstants.INT_THREE).setCellStyle(pRowCellStyle);
		pRowHead.getCell(TRURRInFeedConstants.INT_FOUR).setCellStyle(pRowCellStyle);
		pRowHead.getCell(TRURRInFeedConstants.INT_FIVE).setCellStyle(pRowCellStyle);
	}

	
	
	/**
	 * Product attach data.
	 *
	 * @param pErrorInfoList the error info list
	 * @param pSheet the sheet
	 * @param pRowCount the row count
	 * @param pDataCellStyle the data cell style
	 * @return the int
	 */
	private int productAttachData(List<FeedErrorInfo> pErrorInfoList, HSSFSheet pSheet,int pRowCount, HSSFCellStyle pDataCellStyle) {
		
		int rowCount = pRowCount;
		
		for (FeedErrorInfo fse : pErrorInfoList) {
			HSSFRow dataRow = pSheet.createRow(rowCount);
			dataRow.createCell(TRURRInFeedConstants.INT_ZERO).setCellValue(new HSSFRichTextString(fse.getFileName()));
			dataRow.createCell(TRURRInFeedConstants.INT_ONE).setCellValue(new HSSFRichTextString(Integer.toString(rowCount)));
			dataRow.createCell(TRURRInFeedConstants.INT_TWO).setCellValue(new HSSFRichTextString((new SimpleDateFormat(TRURRInFeedConstants.PWRTIME))
							.format(new Date())));
			dataRow.createCell(TRURRInFeedConstants.INT_THREE).setCellValue(new HSSFRichTextString(fse.getRepositoryId()));
			dataRow.createCell(TRURRInFeedConstants.INT_FOUR).setCellValue(new HSSFRichTextString(fse.getOnlinePid()));
			dataRow.createCell(TRURRInFeedConstants.INT_FIVE).setCellValue(new HSSFRichTextString(fse.getErrorMessage()));

			dataRow.getCell(TRURRInFeedConstants.INT_ZERO).setCellStyle(pDataCellStyle);
			dataRow.getCell(TRURRInFeedConstants.INT_ONE).setCellStyle(pDataCellStyle);
			dataRow.getCell(TRURRInFeedConstants.INT_TWO).setCellStyle(pDataCellStyle);
			dataRow.getCell(TRURRInFeedConstants.INT_THREE).setCellStyle(pDataCellStyle);
			dataRow.getCell(TRURRInFeedConstants.INT_FOUR).setCellStyle(pDataCellStyle);
			dataRow.getCell(TRURRInFeedConstants.INT_FIVE).setCellStyle(pDataCellStyle);

			rowCount++;
		}
		return rowCount;
	}


	/**
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * @param pTemplateEmailSender the templateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}


	/**
	 * @return the contactEmailConfiguration
	 */
	public TRUPowerReviewEmailConfiguration getContactEmailConfiguration() {
		return mContactEmailConfiguration;
	}

	/**
	 * @param pContactEmailConfiguration the contactEmailConfiguration to set
	 */
	public void setContactEmailConfiguration(
			TRUPowerReviewEmailConfiguration pContactEmailConfiguration) {
		mContactEmailConfiguration = pContactEmailConfiguration;
	}

	/**
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	/**
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	/**
	 * @return the mAuditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}
	/**
	 * @param pAuditRepository the mAuditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		this.mAuditRepository = pAuditRepository;
	}
	/**
	 * @return the mcontactItemDescriptor
	 */
	public String getcontactItemDescriptor() {
		return mContactItemDescriptor;
	}
	/**
	 * @param pContactItemDescriptor the mcontactItemDescriptor to set
	 */
	public void setContactItemDescriptor(String pContactItemDescriptor) {
		this.mContactItemDescriptor = pContactItemDescriptor;
	}

}

