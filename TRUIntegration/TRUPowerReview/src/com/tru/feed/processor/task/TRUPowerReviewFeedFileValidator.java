package com.tru.feed.processor.task;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import atg.core.util.StringUtils;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.FeedUtils;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;

/**
 * The Class TRUPowerReviewFeedFileValidator.
 */
public class TRUPowerReviewFeedFileValidator  extends AbstractFeedTask {

	/** The Xml schema ns uri. */
	private String mXmlSchemaNsURI;
	
	/** The Schema file xsd uri. */
	private String mSchemaFileXsdUri;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask(com.tru.feed.processor.FeedStepInfo)
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileValidator.executeTask()");
		}

		File file = new File(getFeedConfig().getFileDestinationPath());
		if (FeedUtils.getFileExtension(file.toString()).equals(TRURRInFeedConstants.XML)) {
			
			SchemaFactory schemaFactory= null;
			Schema schema = null;
			Source xmlFile = new StreamSource(file);
			String  lSchemaFileXsdUri = getSchemaFileXsdUri();
			String lXmlSchemaNsURI = getXmlSchemaNsURI();
		
			try {
				
				if(StringUtils.isEmpty(lXmlSchemaNsURI)){
					vlogWarning("XML_SCHEMA_NS_URI Uri is null or Empty in Validate File Task");
				}else{
					schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
				}
				
				if(StringUtils.isEmpty(lSchemaFileXsdUri)){
					vlogError("XSD Uri is null or Empty in Validate File Task");
				}else{
					if(lSchemaFileXsdUri.contains(TRURRInFeedConstants.HTTP_URL_FORMAT)){
						schema = schemaFactory.newSchema(new URL(lSchemaFileXsdUri));
					}
					else{
						schema = schemaFactory.newSchema(new File(getSchemaFileXsdUri()));
					}
				}
				
				Validator validator = schema.newValidator();
				validator.validate(xmlFile);
				
			}catch (SAXException ex){
				pInfo.setException(true);
				if(isLoggingError()){
					logError("Exception : ",ex);
				}
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,ex.getMessage());
				throw new FeedJobExecutionException(ex.getMessage());
			}catch (IOException ioEx){
				pInfo.setException(true);
				if(isLoggingError()){
					logError("Exception : ",ioEx);
				}
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,ioEx.getMessage());
				throw new FeedJobExecutionException(ioEx.getMessage());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileValidator.executeTask()");
		}
		return passToNextTask(pInfo); 
	}

	/**
	 * Gets the schema file xsd uri.
	 *
	 * @return the schema file xsd uri
	 */
	public String getSchemaFileXsdUri() {
		return mSchemaFileXsdUri;
	}

	/**
	 * Sets the schema file xsd uri.
	 *
	 * @param pSchemaFileXsdUri the new schema file xsd uri
	 */
	public void setSchemaFileXsdUri(String pSchemaFileXsdUri) {
		mSchemaFileXsdUri = pSchemaFileXsdUri;
	}
	/**
	 * Gets the xml schema ns uri.
	 *
	 * @return the xml schema ns uri
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}
	/**
	 * Sets the xml schema ns uri.
	 *
	 * @param pXmlSchemaNsURI the new xml schema ns uri
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		mXmlSchemaNsURI = pXmlSchemaNsURI;
	}
}
