package com.tru.feed.processor.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;

/**
 * The Class TRUPowerReviewFeedFileDownloader.
 */
public class TRUPowerReviewFeedFileDownloader extends AbstractFeedTask {

	/** Constant for BUFFER_SIZE. */
	public static final int BUFFER_SIZE = 4096;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}

	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask()
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileDownloader.executeTask()");
		}

		if(!getFeedConfig().isSkipXMLDownload() && TRURRInFeedConstants.HTTP.equals(getFeedConfig().getProtocolType())){
			try {
				downloadFileByHttp(getFeedConfig().getSourceFilePath(),getFeedConfig().getDestinationDir());
			}catch (MalformedURLException e) {
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,TRURRInFeedConstants.URLERROR);
				pInfo.setException(Boolean.TRUE);
				if(isLoggingError()){
					logError("Exception : ",e);
				}
				throw new FeedJobExecutionException(e.getMessage());
			}catch (FileNotFoundException e) {
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,TRURRInFeedConstants.FILENOTEXIST);
				pInfo.setException(Boolean.TRUE);
				if(isLoggingError()){
					logError("Exception : ",e);
				}
				throw new FeedJobExecutionException(e.getMessage());
			}catch (IOException e) {
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,e.getMessage());
				pInfo.setException(Boolean.TRUE);
				if(isLoggingError()){
					logError("Exception : ",e);
				}
				throw new FeedJobExecutionException(e.getMessage());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileDownloader.executeTask()");
		}		
		return passToNextTask(pInfo);
	}
	
	/**
	 * Download file by http.
	 *
	 * @param pXmlfile the xmlfile
	 * @param pSaveDir the save dir
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private boolean downloadFileByHttp(String pXmlfile, String pSaveDir) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileDownloader.downloadFileByHttp()");
		}

		boolean fileDownloadSuccess = Boolean.FALSE;
		if (isLoggingDebug()) {
			logDebug("Start download file frm .... : " + pXmlfile);
		}
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		HttpURLConnection httpsConn = null;
		try {
			URL url = new URL(pXmlfile);
			if (isLoggingDebug()) {
				logDebug("URL constructed.... : ");
			}
			// use proxy setting for PA environments
			if (getFeedConfig().isUseProxy()) {
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(getFeedConfig().getHost(),
						getFeedConfig().getPort()));
				httpsConn = (HttpURLConnection) url.openConnection(proxy);
			} else {
				httpsConn = (HttpURLConnection) url.openConnection();
			}

			if (isLoggingDebug()) {
				logDebug("URL Connnection object created... : ");
			}

			httpsConn.connect();
			if (isLoggingDebug()) {
				logDebug("URL Connnection connect success.... : ");
			}
			int responseCode = httpsConn.getResponseCode();
			if (isLoggingDebug()) {
				logDebug("responseCode.... : " + responseCode);
			}
			// always check HTTP response code first
			if (responseCode != HttpURLConnection.HTTP_OK) {
				if (isLoggingDebug()) {
					logDebug("No file to download. Server replied HTTP code: " + responseCode);
				}
				return Boolean.FALSE;
			}
			// if (responseCode == HttpURLConnection.HTTP_OK) {
			String fileName = TRURRInFeedConstants.EMPTY_STRING;
			String disposition = httpsConn.getHeaderField(TRURRInFeedConstants.CONTENT_DISPOSITION);
			String contentType = httpsConn.getContentType();
			int contentLength = httpsConn.getContentLength();

			if (disposition != null) {
				// extracts file name from header field
				int index = disposition.indexOf(TRURRInFeedConstants.FILE_NAME);
				if (index > TRURRInFeedConstants.INT_ONE) {
					fileName = disposition.substring(index + TRURRInFeedConstants.NUMBER_TEN, disposition.length()
							- TRURRInFeedConstants.INT_ONE);
				}
			} else {
				// extracts file name from URL
				fileName = pXmlfile.substring(pXmlfile.lastIndexOf(TRURRInFeedConstants.SLASH)
						+ TRURRInFeedConstants.INT_ONE, pXmlfile.length());
			}

			if (isLoggingDebug()) {
				logDebug("Content-Type = " + contentType);
				logDebug("Content-Disposition = " + disposition);
				logDebug("Content-Length = " + contentLength);
				logDebug("fileName = " + fileName);
			}
			// opens input stream from the HTTP connection
			inputStream = httpsConn.getInputStream();
			String saveFilePath = pSaveDir + File.separator + fileName;

			// opens an output stream to save into file
			if (isLoggingDebug()) {
				logDebug("saveFilePath = " + saveFilePath);
			}
			outputStream = new FileOutputStream(saveFilePath);
			int bytesRead = TRURRInFeedConstants.MINUS_ONE;
			byte[] buffer = new byte[TRURRInFeedConstants.BUFFER_SIZE];
			while ((bytesRead = inputStream.read(buffer)) != TRURRInFeedConstants.MINUS_ONE) {
				outputStream.write(buffer, TRURRInFeedConstants.INT_ZERO, bytesRead);
			}
			if (isLoggingDebug()) {
				logDebug("File download complete");
			}
			fileDownloadSuccess = Boolean.TRUE;

			if (isLoggingDebug()) {
				logDebug("File downloaded");
			}
		} finally {
			if (null != outputStream) {
				outputStream.close();
			}
			if (null != inputStream) {
				inputStream.close();
			}
			if (null != httpsConn) {
				httpsConn.disconnect();
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileDownloader.downloadFileByHttp()");
		}

		return fileDownloadSuccess;
	}
}
