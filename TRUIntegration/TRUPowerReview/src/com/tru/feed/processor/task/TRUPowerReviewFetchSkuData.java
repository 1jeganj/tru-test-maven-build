package com.tru.feed.processor.task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.adapter.gsa.GSARepository;
import atg.repository.Repository;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;

/**
 * The Class TRUPowerReviewFetchSkuData.
 */
public class TRUPowerReviewFetchSkuData  extends AbstractFeedTask {

	/** The Sql query. */
	private String mSqlQuery;
	
	/** The Catalog. */
	private Repository mCatalog;
	
	/** The Online pid. */
	private String mOnlinePID;
	
	/** The Sku id. */
	private String mSkuId;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}
	/**
	 * Gets the online pid.
	 *
	 * @return the online pid
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * Sets the online pid.
	 *
	 * @param pOnlinePID the new online pid
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}

	/**
	 * Gets the sku id.
	 *
	 * @return the sku id
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Sets the sku id.
	 *
	 * @param pSkuId the new sku id
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the sql query.
	 *
	 * @return the sql query
	 */
	public String getSqlQuery() {
		return mSqlQuery;
	}

	/**
	 * Sets the sql query.
	 *
	 * @param pSqlQuery the new sql query
	 */
	public void setSqlQuery(String pSqlQuery) {
		mSqlQuery = pSqlQuery;
	}
	
	/**
	 * Gets the catalog.
	 *
	 * @return the catalog
	 */
	public Repository getCatalog() {
		return mCatalog;
	}

	/**
	 * Sets the catalog.
	 *
	 * @param pCatalog the new catalog
	 */
	public void setCatalog(Repository pCatalog) {
		mCatalog = pCatalog;
	}

	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask(com.tru.feed.processor.FeedStepInfo)
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFetchSkuData.executeTask()");
		}
		Connection conn = null;
		Map<String,Object> map = new HashMap<String,Object>();
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(getJobName());
		}
		try {
			//pInfo.readData("onlinepid_skuid")
			conn = ((GSARepository)getCatalog()).getDataSource().getConnection();
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(getSqlQuery());
			while(rs.next()){
				String skuId =	rs.getString(getSkuId());
				String onlinePid =	rs.getString(getOnlinePID());
				updateDataMap(map,onlinePid,skuId);
			}
			pInfo.putData(TRURRInFeedConstants.ONLINEPID_SKUID, map);
			rs.close();
		} catch (SQLException e) {
			pInfo.setException(Boolean.TRUE);
			if(isLoggingError()){
				logError("Exception : ",e);
			}
			pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,e.getMessage());
			throw new FeedJobExecutionException(e.getMessage());
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				pInfo.setException(Boolean.TRUE);
				if(isLoggingError()){
					logError("Exception : ",e);
				}
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,e.getMessage());
				throw new FeedJobExecutionException(e.getMessage());
			}
		}
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(getJobName());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFetchSkuData.executeTask()");
		}
		return passToNextTask(pInfo);
	}
	
	
	/**
	 * Update data map.
	 *
	 * @param pMap the map
	 * @param pKey the key
	 * @param pValue the value
	 */
	@SuppressWarnings("unchecked")
	private void updateDataMap(Map<String,Object> pMap,String pKey,String pValue){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFetchSkuData.updateDataMap()");
		}		
		if(pMap.containsKey(pKey)){
			Object obj = pMap.get(pKey);
			if(obj instanceof List){
				List<String> lt = ((List<String>)obj);
				if(!lt.contains(pValue)){
					lt.add((String)pValue);
				}
			}else{
				List<String> lStr = new ArrayList<String>();
				lStr.add((String)obj);
				lStr.add((String)pValue);
				pMap.put(pKey, lStr);
			}
		}else{
			pMap.put(pKey, pValue);	
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFetchSkuData.updateDataMap()");
		}
	}
}
