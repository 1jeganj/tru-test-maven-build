package com.tru.feed.processor.task;

import java.util.ArrayList;
import java.util.List;

import atg.adapter.gsa.GSARepository;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.tru.commerce.review.repository.TRUPowerReviewRepositoryProperties;
import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedErrorInfo;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;
import com.tru.feed.processor.powerreview.inbound.vo.Product;
import com.tru.feed.processor.powerreview.inbound.vo.ProductType;
import com.tru.feed.processor.powerreview.inbound.vo.Products;

/**
 * The Class TRUPowerReviewRepositoryDataWritter.
 */
public class TRUPowerReviewRepositoryDataWritter extends AbstractFeedTask  {

	/** The Feed repository. */
	private GSARepository mFeedRepository;
	
	/** The Repository properties. */
	private TRUPowerReviewRepositoryProperties mRepositoryProperties;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}

	/**
	 * Gets the repository properties.
	 *
	 * @return the repository properties
	 */
	public TRUPowerReviewRepositoryProperties getRepositoryProperties() {
		return mRepositoryProperties;
	}

	/**
	 * Sets the repository properties.
	 *
	 * @param pRepositoryProperties the new repository properties
	 */
	public void setRepositoryProperties(TRUPowerReviewRepositoryProperties pRepositoryProperties) {
		mRepositoryProperties = pRepositoryProperties;
	}

	/**
	 * Gets the feed repository.
	 *
	 * @return the feed repository
	 */
	public GSARepository getFeedRepository() {
		return mFeedRepository;
	}

	/**
	 * Sets the feed repository.
	 *
	 * @param pFeedRepository the new feed repository
	 */
	public void setFeedRepository(GSARepository pFeedRepository) {
		mFeedRepository = pFeedRepository;
	}

	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask()
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewRepositoryDataWritter.executeTask()");
		}
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			PerformanceMonitor.startOperation(getJobName());
		}
		Products prods = (Products)pInfo.getData(TRURRInFeedConstants.PRODUCTS);
		List<FeedErrorInfo> errorInfoList = new ArrayList<FeedErrorInfo>();
		pInfo.putData(TRURRInFeedConstants.SKU_ERROR_INFO, errorInfoList);
		List<Product> newItemList = new ArrayList<Product>();
		if(prods != null){
			List<ProductType> listProd = prods.getProductOrDeletedProduct();
			if(listProd != null){
				try {
					updateProductItems(pInfo,listProd,newItemList);
				} catch (TransactionDemarcationException e) {
					pInfo.setException(Boolean.TRUE);
					if(isLoggingError()){
						logError("Exception : ",e);
					}
					pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,e.getMessage());
					throw new FeedJobExecutionException(e.getMessage());
				}
			}
			
			if(!newItemList.isEmpty()){
				addProductItems(pInfo,newItemList);
			}
		}
		updateFeedInfo(pInfo);
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(getJobName());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewRepositoryDataWritter.executeTask()");
		}		
		return passToNextTask(pInfo);
	}
	

	/**
	 * Update feed info.
	 *
	 * @param pInfo the info
	 */
	private void updateFeedInfo(FeedStepInfo pInfo){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewRepositoryDataWritter.updateFeedInfo()");
		}
		Products prods = (Products)pInfo.getData(TRURRInFeedConstants.PRODUCTS);
		if(prods != null){
			List<ProductType> listProd = prods.getProductOrDeletedProduct();
			if(listProd != null){
				pInfo.setTotalItemCount(listProd.size());
			}
		}else{
			pInfo.setTotalItemCount(TRURRInFeedConstants.INT_ZERO);
		}
		
		List<String> failedItems = pInfo.getFailedItems();
		if(failedItems != null){
			pInfo.setTotalItemsFailed(failedItems.size());
		}else{
			pInfo.setTotalItemsFailed(TRURRInFeedConstants.INT_ZERO);
		}
		List<String> itemsCommitted = pInfo.getItemsCommitted();
		if(itemsCommitted != null && itemsCommitted.size() > TRURRInFeedConstants.INT_ZERO){
			pInfo.setTotalItemsCommitted(itemsCommitted.size());
		}else{
			pInfo.setTotalItemsCommitted(TRURRInFeedConstants.INT_ZERO);
			pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,TRURRInFeedConstants.REPOUPDATE);
			throw new FeedJobExecutionException(TRURRInFeedConstants.REPOUPDATE);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewRepositoryDataWritter.updateFeedInfo()");
		}
	}
	
	/**
	 * Adds the product items.
	 *
	 * @param pInfo the info
	 * @param pNewItemList the new item list
	 */
	@SuppressWarnings("unchecked")
	private void addProductItems(FeedStepInfo pInfo, List<Product> pNewItemList) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewRepositoryDataWritter.addProductItems()");
		}
		List<String> itemsSuccessed = null;
		List<String> itemsFailed = null;

		if (pInfo.getFailedItems() != null) {
			itemsFailed = pInfo.getFailedItems();
		} else {
			itemsFailed = new ArrayList<String>();
			pInfo.setFailedItems(itemsFailed);
		}
		if (pInfo.getItemsCommitted() != null) {
			itemsSuccessed = pInfo.getItemsCommitted();
		} else {
			itemsSuccessed = new ArrayList<String>();
			pInfo.setItemsCommitted(itemsSuccessed);
		}
		
		List<FeedErrorInfo> errorInfoList  = (List<FeedErrorInfo>)pInfo.getData(TRURRInFeedConstants.SKU_ERROR_INFO);
		for (Product prodItem : pNewItemList) {
			if (!StringUtils.isBlank(prodItem.getRepositoryId()) && prodItem.getAverageoverallrating() != null
					&& prodItem.getAverageRatingDecimal() != null && prodItem.getFullreviews() != null) {
				try {
					MutableRepository mutRepo = (MutableRepository) getFeedRepository();
					MutableRepositoryItem mutRepoItem = mutRepo.createItem(prodItem.getRepositoryId(),
							getRepositoryProperties().getPowerReviewItemDescriptorName());
					mutRepoItem.setPropertyValue(getRepositoryProperties().getOnlinePidPropertyName(),
							prodItem.getPageid());
					mutRepoItem.setPropertyValue(getRepositoryProperties().getReviewRatingPropertyName(),
							prodItem.getAverageoverallrating());
					mutRepoItem.setPropertyValue(getRepositoryProperties().getReviewCountPropertyName(), prodItem
							.getFullreviews().floatValue());
					mutRepoItem.setPropertyValue(getRepositoryProperties().getReviewCountDecimalPropertyName(),
							prodItem.getAverageRatingDecimal());
					mutRepo.addItem(mutRepoItem);
					itemsSuccessed.add(prodItem.getRepositoryId());
				} catch (RepositoryException e) {
					itemsFailed.add(prodItem.getRepositoryId());
					if (isLoggingError()) {
						logError("Exception : ", e);
					}
					FeedErrorInfo errorInfo = new FeedErrorInfo();
					errorInfo.setOnlinePid(prodItem.getPageid());
					errorInfo.setRepositoryId(prodItem.getRepositoryId());
					errorInfo.setErrorMessage(e.getMessage());
					errorInfoList.add(errorInfo);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewRepositoryDataWritter.addProductItems()");
		}
	}
	
	
	/**
	 * Update product items.
	 *
	 * @param pInfo the info
	 * @param pProdList the product list
	 * @param pNewItemList the new item list
	 * @throws TransactionDemarcationException the transaction demarcation exception
	 */
	@SuppressWarnings("unchecked")
	private void updateProductItems(FeedStepInfo pInfo, List<ProductType> pProdList, List<Product> pNewItemList)
			throws TransactionDemarcationException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewRepositoryDataWritter.updateProductItems()");
		}
		List<String> itemsSuccessed = null;
		List<String> itemsFailed = null;

		if (pInfo.getFailedItems() != null) {
			itemsFailed = pInfo.getFailedItems();
		} else {
			itemsFailed = new ArrayList<String>();
			pInfo.setFailedItems(itemsFailed);
		}
		if (pInfo.getItemsCommitted() != null) {
			itemsSuccessed = pInfo.getItemsCommitted();
		} else {
			itemsSuccessed = new ArrayList<String>();
			pInfo.setItemsCommitted(itemsSuccessed);
		}
		
		List<FeedErrorInfo> errorInfoList  = (List<FeedErrorInfo>)pInfo.getData(TRURRInFeedConstants.SKU_ERROR_INFO);
		
		TransactionDemarcation td = new TransactionDemarcation();
		try {
			td.begin(getFeedRepository().getTransactionManager(), TRURRInFeedConstants.INT_FOUR);
			for (ProductType prodType : pProdList) {
				Product prod = (Product) prodType;
				if (!StringUtils.isBlank(prod.getRepositoryId()) && prod.getAverageoverallrating() != null
						&& prod.getAverageRatingDecimal() != null && prod.getFullreviews() != null) {
					try {
						MutableRepositoryItem item = mFeedRepository.getItemForUpdate(prod.getRepositoryId(),
								getRepositoryProperties().getPowerReviewItemDescriptorName());
						if (item != null) {
							updateFeedItemToRepo(item, prod);
							itemsSuccessed.add(prod.getRepositoryId());
						} else {
							pNewItemList.add(prod);
						}
					} catch (RepositoryException e) {
						itemsFailed.add(prod.getRepositoryId());
						if (isLoggingError()) {
							logError("Exception : ", e);
						}
						FeedErrorInfo errorInfo = new FeedErrorInfo();
						errorInfo.setOnlinePid(prod.getPageid());
						errorInfo.setRepositoryId(prod.getRepositoryId());
						errorInfo.setErrorMessage(e.getMessage());
						errorInfoList.add(errorInfo);
					}
				}
			}
		} finally {
			td.end();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewRepositoryDataWritter.updateProductItems()");
		}
	}
	
	/**
	 * Update feed item to repository.
	 *
	 * @param pItem the item
	 * @param pProd the product
	 * @throws RepositoryException the repository exception
	 */
	private void updateFeedItemToRepo(MutableRepositoryItem pItem, Product pProd) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewRepositoryDataWritter.updateFeedItemToRepo()");
		}
		pItem.setPropertyValue(getRepositoryProperties().getReviewRatingPropertyName(), pProd.getAverageoverallrating());
		pItem.setPropertyValue(getRepositoryProperties().getReviewCountPropertyName(), pProd.getFullreviews()
				.floatValue());
		pItem.setPropertyValue(getRepositoryProperties().getReviewCountDecimalPropertyName(),
				pProd.getAverageRatingDecimal());
		MutableRepository mutRepo = (MutableRepository) getFeedRepository();
		mutRepo.updateItem(pItem);
		
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewRepositoryDataWritter.updateFeedItemToRepo()");
		}
	}
}
