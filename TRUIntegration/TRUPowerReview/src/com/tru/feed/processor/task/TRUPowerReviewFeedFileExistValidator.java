package com.tru.feed.processor.task;

import java.io.File;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.FeedUtils;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;
import com.tru.feed.processor.exception.FeedJobFileExecutionException;

/**
 * The Class TRUPowerReviewFeedFileExistValidator.
 */
public class TRUPowerReviewFeedFileExistValidator  extends AbstractFeedTask  {

	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask()
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileExistValidator.executeTask()");
		}
		String destinationDir = getFeedConfig().getDestinationDir();
		String fileName = FeedUtils.getFileName(getFeedConfig().getSourceFilePath());
		if(!StringUtils.isBlank(destinationDir) && !StringUtils.isBlank(fileName)){
			File lFile = new File(destinationDir+fileName);
			if (lFile == null || !lFile.exists() || !lFile.isFile()	|| !lFile.canRead()) {
				pInfo.setException(Boolean.TRUE);
				vlogError("File :{0} does not exits in directory : ",fileName, destinationDir);

				String str = String.format(TRURRInFeedConstants.FILE_NOT_EXITS_DIRECTORY_ERROR,fileName, destinationDir);
				pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,str);
				throw new FeedJobFileExecutionException(str);
			}
		}else{
			vlogError("File : {0} , Directory : {1}", fileName,destinationDir);
			pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,TRURRInFeedConstants.FILE_DIR_NOT_CONFIG);
			pInfo.setException(Boolean.TRUE);
			throw new FeedJobFileExecutionException(TRURRInFeedConstants.FILE_DIR_NOT_CONFIG);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileExistValidator.executeTask()");
		}
		return passToNextTask(pInfo);
	}

}
