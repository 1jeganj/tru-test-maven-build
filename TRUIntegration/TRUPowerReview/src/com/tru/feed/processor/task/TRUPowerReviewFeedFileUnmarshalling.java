package com.tru.feed.processor.task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.processor.AbstractFeedTask;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;
import com.tru.feed.processor.powerreview.inbound.vo.Product;
import com.tru.feed.processor.powerreview.inbound.vo.ProductType;
import com.tru.feed.processor.powerreview.inbound.vo.Products;

/**
 * The Class TRUPowerReviewFeedFileUnmarshalling.
 */
public class TRUPowerReviewFeedFileUnmarshalling  extends AbstractFeedTask {

	/** The Jaxb context package. */
	private String mJaxbContextPackage;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}

	/**
	 * Gets the jaxb context package.
	 *
	 * @return the jaxb context package
	 */
	public String getJaxbContextPackage() {
		return mJaxbContextPackage;
	}

	/**
	 * Sets the jaxb context package.
	 *
	 * @param pJaxbContextPackage the new jaxb context package
	 */
	public void setJaxbContextPackage(String pJaxbContextPackage) {
		mJaxbContextPackage = pJaxbContextPackage;
	}

	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#executeTask(com.tru.feed.processor.FeedStepInfo)
	 */
	@Override
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileUnmarshalling.executeTask()");
		}
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			
			PerformanceMonitor.startOperation(getJobName());
		}
		Products products = null;
		try {
			products = unmarshallXmlFileToObject(getFeedConfig().getFileDestinationPath());
		} catch (JAXBException e) {
			pInfo.setException(true);
			if(isLoggingError()){
				logError("Exception : ",e);
			}
			pInfo.putData(TRURRInFeedConstants.ERROR_MESSAGE,e.getMessage());
			throw new FeedJobExecutionException(e.getMessage());
		}
		populateSkuIdProducts(pInfo,products);
		pInfo.putData(TRURRInFeedConstants.PRODUCTS, products);
		if(getFeedConfig().isPerformanceToSubTaskEnabled() && PerformanceMonitor.isEnabled()){
			PerformanceMonitor.endOperation(getJobName());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileUnmarshalling.executeTask()");
		}
		return passToNextTask(pInfo); 
	}
	
	/**
	 * Populate sku id products.
	 *
	 * @param pInfo the info
	 * @param pProducts the products
	 */
	@SuppressWarnings("unchecked")
	private void populateSkuIdProducts(FeedStepInfo pInfo,Products pProducts) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileUnmarshalling.populateSkuIdProducts()");
		}
		List<ProductType> listProduct = pProducts.getProductOrDeletedProduct();
		List<ProductType> list = new ArrayList<ProductType>();
		Map<String, Object> readData = (Map<String, Object>)pInfo.getData(TRURRInFeedConstants.ONLINEPID_SKUID);
		for (ProductType product : listProduct) {
			Object obj = readData.get(product.getPageid());
			if (obj instanceof List) {
				List<String> skuIdList = (List<String>) obj;
					Product prod1 = ((Product) product);
					for (int i = TRURRInFeedConstants.INT_ZERO; i < skuIdList.size(); i++) {
						if (i == TRURRInFeedConstants.INT_ZERO) {
							prod1.setRepositoryId(skuIdList.get(i));
						} else {
							Product prod2 = copyProductData(prod1);
							prod2.setRepositoryId(skuIdList.get(i));
							list.add(prod2);
						}
				}
			} else {
				((Product) product).setRepositoryId((String) obj);
			}
		}
		
		if(!list.isEmpty()){
			listProduct.addAll(list);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileUnmarshalling.populateSkuIdProducts()");
		}
	}
	
	/**
	 * Copy product data.
	 *
	 * @param pProduct the product
	 * @return the product
	 */
	private Product copyProductData(Product pProduct){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileUnmarshalling.copyProductData()");
		}
		Product prod = new Product();
		prod.setPageid(pProduct.getPageid());
		prod.setAverageoverallrating(pProduct.getAverageoverallrating());
		prod.setAverageRatingDecimal(pProduct.getAverageRatingDecimal());
		prod.setBottomLineNoVotes(pProduct.getBottomLineNoVotes());
		prod.setBottomLineYesVotes(pProduct.getBottomLineYesVotes());
		prod.setConfirmstatusgroup(pProduct.getConfirmstatusgroup());
		prod.setDeletedReviews(pProduct.getDeletedReviews());
		prod.setFullreviews(pProduct.getFullreviews());
		prod.setInlinefiles(pProduct.getInlinefiles());
		prod.setInlinequestionfiles(pProduct.getInlinequestionfiles());
		prod.setLocale(pProduct.getLocale());
		prod.setName(pProduct.getName());
		prod.setNewestreviewdate(pProduct.getNewestreviewdate());
		prod.setOldestreviewdate(pProduct.getOldestreviewdate());
		prod.setReviewContentUpdatedDate(pProduct.getReviewContentUpdatedDate());
		prod.setTotalAnswers(pProduct.getTotalAnswers());
		prod.setTotalQuestions(pProduct.getTotalQuestions());
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileUnmarshalling.copyProductData()");
		}
		return prod;
	}
	
	
	
	/**
	 * Unmarshall xml file to object.
	 *
	 * @param pFilePath the file path
	 * @return the products
	 * @throws JAXBException the JAXB exception
	 */
	private Products unmarshallXmlFileToObject(String pFilePath) throws JAXBException {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedFileUnmarshalling.unmarshallXmlFileToObject()");
		}
		Products products = null;
		JAXBContext jaxbContext = JAXBContext.newInstance(getJaxbContextPackage());
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		products = (Products) unmarshaller.unmarshal(new File(pFilePath));
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedFileUnmarshalling.unmarshallXmlFileToObject()");
		}		
		return products;
	}

}
