package com.tru.feed.processor.exception;

/**
 * The Class FeedJobFileExecutionException.
 * 
 * This class is responsible to throw exception while job execution
 * @author PA
 * @version 1.0
 */
public class FeedJobFileExecutionException extends FeedJobExecutionException {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8370178894538967599L;

	/**
	 * Instantiates a new feed job file execution exception.
	 *
	 * @param pMessage the message
	 */
	public FeedJobFileExecutionException(String pMessage){
		super(pMessage);
	}
}
