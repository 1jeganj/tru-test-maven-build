package com.tru.feed.processor.exception;
/**
 * The Class FeedJobExecutionException.
 * 
 * This class is responsible to throw exception while job execution
 * @author PA
 * @version 1.0
 */
public class FeedJobExecutionException extends RuntimeException{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4051258715233104096L;

	/**
	 * Instantiates a new feed job execution exception.
	 *
	 * @param pMessage the message
	 */
	public FeedJobExecutionException(String pMessage){
		super(pMessage);
	}
}
