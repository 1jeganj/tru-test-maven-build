package com.tru.feed.processor;

import atg.nucleus.GenericService;

import com.tru.feed.constants.TRURRInFeedConstants;


/**
 * The abstract Class AbstractFeedTask extending FeedTask behavior .
 */
public abstract class AbstractFeedTask extends GenericService implements FeedTask {
	
	/** The Feed task. */
	private FeedTask mFeedTask;
	
/*	*//** The Initial data. *//*
	private Map<String,Object> mInitialData;*/
	
	/** The Task name. */
	private String mJobName;
	/**
	 * Gets the job name.
	 *
	 * @return the job name
	 */
	public String getJobName() {
		return mJobName;
	}

	/**
	 * Sets the job name.
	 *
	 * @param pJobName the new job name
	 */
	public void setJobName(String pJobName) {
		mJobName = pJobName;
	}

	/**
	 * Gets the feed task.
	 *
	 * @return the feed task
	 */
	public FeedTask getFeedTask() {
		return mFeedTask;
	}

	/**
	 * Sets the feed task.
	 *
	 * @param pFeedTask the new feed task
	 */
	public void setFeedTask(FeedTask pFeedTask) {
		mFeedTask = pFeedTask;
	}

	/* (non-Javadoc)
	 * @see com.tru.feed.processor.FeedTask#passToNextTask()
	 */
	@Override
	public int passToNextTask(FeedStepInfo pInfo){
		if(getFeedTask() == null){
			return TRURRInFeedConstants.INT_ONE;
		}else{
			return getFeedTask().executeTask(pInfo);
		}
	}
}
