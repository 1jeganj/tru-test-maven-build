package com.tru.feed.processor;

import com.tru.feed.processor.exception.FeedJobExecutionException;

/**
 * The Interface FeedTask.
 */
public interface FeedTask {
	
	
	/**
	 * Execute task.
	 *
	 * @param pInfo the info
	 * @return the int
	 * @throws FeedJobExecutionException the feed job execution exception
	 */
	public int executeTask(FeedStepInfo pInfo) throws FeedJobExecutionException;
	
	
	/**
	 * Pass to next task.
	 *
	 * @param pInfo the info
	 * @return the int
	 */
	public int passToNextTask(FeedStepInfo pInfo);
	
	
}
