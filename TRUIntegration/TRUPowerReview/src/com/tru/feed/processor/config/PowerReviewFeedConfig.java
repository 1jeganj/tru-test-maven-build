package com.tru.feed.processor.config;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.feed.processor.FeedUtils;
/**
 * The Class FeedUtils.
 * 
 * This class is responsible to hold power review feed config 
 * @author PA
 * @version 1.0
 */
public class PowerReviewFeedConfig extends GenericService {
	
	/** The Host . */
	private String mHost;
	/** The port. */
	private int mPort;
	/** The User. */
	private String mUser;
	/** The Password. */
	private String mPass;
	/** The SourceDirectory. */
	private String mSourceFilePath;
	/** The destination Directory. */
	private String mDestinationDir;
	/** The ProtocolType. */
	private String mProtocolType;
	/** The FileCount. */
	private int mFileCount = 0;
	/** The FileType. */
	private String mFileType;
	/** The ZipFile. */
	private String mZipFile;
	
	/** The Use proxy. */
	private boolean mUseProxy;
	
	/** The Skip xml download. */
	private boolean mSkipXMLDownload;
	
	/** The Enable performance to sub task. */
	private boolean mPerformanceToSubTaskEnabled;
	
	/** The File destination path. */
	private String mFileDestinationPath;

	/** The Schema file xsd uri. */
	private String mSchemaFileXsdUri;
	
	/** The Env. */
	private String mEnv;
	
	/** The mSuccessDirectory. */
	private String mSuccessDirectory;
	
	/** The mErrorDirectory. */
	private String mErrorDirectory;
	
	/**
	 * Gets the success directory.
	 *
	 * @return the success directory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}

	/**
	 * Sets the success directory.
	 *
	 * @param pSuccessDirectory the new success directory
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
	}

	/**
	 * Gets the error directory.
	 *
	 * @return the error directory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}

	/**
	 * Sets the error directory.
	 *
	 * @param pErrorDirectory the new error directory
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
	}

	/**
	 * Gets the env.
	 *
	 * @return the env
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * Sets the env.
	 *
	 * @param pEnv the new env
	 */
	public void setEnv(String pEnv) {
		mEnv = pEnv;
	}

	/**
	 * Checks if is performance to sub task enabled.
	 *
	 * @return true, if is performance to sub task enabled
	 */
	public boolean isPerformanceToSubTaskEnabled() {
		return mPerformanceToSubTaskEnabled;
	}

	/**
	 * Sets the performance to sub task enabled.
	 *
	 * @param pPerformanceToSubTaskEnabled the new performance to sub task enabled
	 */
	public void setPerformanceToSubTaskEnabled(boolean pPerformanceToSubTaskEnabled) {
		mPerformanceToSubTaskEnabled = pPerformanceToSubTaskEnabled;
	}

	/**
	 * Gets the schema file xsd uri.
	 *
	 * @return the schema file xsd uri
	 */
	public String getSchemaFileXsdUri() {
		return mSchemaFileXsdUri;
	}

	/**
	 * Sets the schema file xsd uri.
	 *
	 * @param pSchemaFileXsdUri the new schema file xsd uri
	 */
	public void setSchemaFileXsdUri(String pSchemaFileXsdUri) {
		mSchemaFileXsdUri = pSchemaFileXsdUri;
	}

	/**
	 * Gets the file destination path.
	 *
	 * @return the file destination path
	 */
	public String getFileDestinationPath() {
		if(StringUtils.isBlank(mFileDestinationPath) && !StringUtils.isBlank(mSourceFilePath) && !StringUtils.isBlank(mDestinationDir)){
			mFileDestinationPath = getDestinationDir()+FeedUtils.getFileName(mSourceFilePath);
		}
		return mFileDestinationPath;
	}

	/**
	 * Checks if is skip xml download.
	 *
	 * @return true, if is skip xml download
	 */
	public boolean isSkipXMLDownload() {
		return mSkipXMLDownload;
	}

	/**
	 * Sets the skip xml download.
	 *
	 * @param pSkipXMLDownload the new skip xml download
	 */
	public void setSkipXMLDownload(boolean pSkipXMLDownload) {
		mSkipXMLDownload = pSkipXMLDownload;
	}


	/**
	 * Checks if is use proxy.
	 *
	 * @return true, if is use proxy
	 */
	public boolean isUseProxy() {
		return mUseProxy;
	}

	/**
	 * Sets the use proxy.
	 *
	 * @param pUseProxy the new use proxy
	 */
	public void setUseProxy(boolean pUseProxy) {
		mUseProxy = pUseProxy;
	}
	
	/**
	 * Gets the host.
	 *
	 * @return the host
	 */
	public String getHost() {
		return mHost;
	}
	
	/**
	 * Sets the host.
	 *
	 * @param pHost the new host
	 */
	public void setHost(String pHost) {
		mHost = pHost;
	}
	
	/**
	 * Gets the port.
	 *
	 * @return the port
	 */
	public int getPort() {
		return mPort;
	}
	
	/**
	 * Sets the port.
	 *
	 * @param pPort the new port
	 */
	public void setPort(int pPort) {
		mPort = pPort;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return mUser;
	}
	
	/**
	 * Sets the user.
	 *
	 * @param pUser the new user
	 */
	public void setUser(String pUser) {
		mUser = pUser;
	}
	
	/**
	 * Gets the pass.
	 *
	 * @return the pass
	 */
	public String getPass() {
		return mPass;
	}
	
	/**
	 * Sets the pass.
	 *
	 * @param pPass the new pass
	 */
	public void setPass(String pPass) {
		mPass = pPass;
	}
	
	/**
	 * Gets the source file path.
	 *
	 * @return the source file path
	 */
	public String getSourceFilePath() {
		return mSourceFilePath;
	}
	
	/**
	 * Sets the source file path.
	 *
	 * @param pSourceFilePath the new source file path
	 */
	public void setSourceFilePath(String pSourceFilePath) {
		mSourceFilePath = pSourceFilePath;
	}
	
	/**
	 * Gets the destination dir.
	 *
	 * @return the destination dir
	 */
	public String getDestinationDir() {
		return mDestinationDir;
	}
	
	/**
	 * Sets the destination dir.
	 *
	 * @param pDestinationDir the new destination dir
	 */
	public void setDestinationDir(String pDestinationDir) {
		mDestinationDir = pDestinationDir;
	}
	
	/**
	 * Gets the protocol type.
	 *
	 * @return the protocol type
	 */
	public String getProtocolType() {
		return mProtocolType;
	}
	
	/**
	 * Sets the protocol type.
	 *
	 * @param pProtocolType the new protocol type
	 */
	public void setProtocolType(String pProtocolType) {
		mProtocolType = pProtocolType;
	}
	
	/**
	 * Gets the file count.
	 *
	 * @return the file count
	 */
	public int getFileCount() {
		return mFileCount;
	}
	
	/**
	 * Sets the file count.
	 *
	 * @param pFileCount the new file count
	 */
	public void setFileCount(int pFileCount) {
		mFileCount = pFileCount;
	}
	
	/**
	 * Gets the file type.
	 *
	 * @return the file type
	 */
	public String getFileType() {
		return mFileType;
	}
	
	/**
	 * Sets the file type.
	 *
	 * @param pFileType the new file type
	 */
	public void setFileType(String pFileType) {
		mFileType = pFileType;
	}
	
	/**
	 * Gets the zip file.
	 *
	 * @return the zip file
	 */
	public String getZipFile() {
		return mZipFile;
	}
	
	/**
	 * Sets the zip file.
	 *
	 * @param pZipFile the new zip file
	 */
	public void setZipFile(String pZipFile) {
		mZipFile = pZipFile;
	}
}
