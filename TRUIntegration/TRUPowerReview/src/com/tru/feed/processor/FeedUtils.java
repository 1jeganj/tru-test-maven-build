package com.tru.feed.processor;

import com.tru.feed.constants.TRURRInFeedConstants;

/**
 * The Class FeedUtils.
 * 
 * @author PA
 * @version 1.0
 */
public class FeedUtils {

	/**
	 * Gets the file extension.
	 *
	 * @param pFilename the filename
	 * @return the file extension
	 */
	public static String getFileExtension(String pFilename)
	{
	  if (pFilename == null) {
	    return null;
	  }
	  int index = pFilename.lastIndexOf(TRURRInFeedConstants.FORTY_SIX);

	  if (index ==TRURRInFeedConstants.MINUS_ONE) {
	    return TRURRInFeedConstants.EMPTY_STRING;
	  }
	  return pFilename.substring(index + TRURRInFeedConstants.INT_ONE);
	}
	
	
	/**
	 * Gets the file name.
	 *
	 * @param pFileName the file name
	 * @return the file name
	 */
	public static String getFileName(String pFileName){
		if (pFileName == null) {
		    return null;
		  }
		//int index = pFilename.lastIndexOf('.');
		
		int lastUnixPos = pFileName.lastIndexOf(TRURRInFeedConstants.FORTY_SEVEN);
		int lastWindowsPos = pFileName.lastIndexOf(TRURRInFeedConstants.NINETY_TWO);
		int index = Math.max(lastUnixPos, lastWindowsPos);
		if (index == TRURRInFeedConstants.MINUS_ONE) {
		    return TRURRInFeedConstants.EMPTY_STRING;
		}
		return pFileName.substring(index + TRURRInFeedConstants.INT_ONE);
	}
	

	  /**
  	 * Gets the parent directory path.
  	 *
  	 * @param pPath the path
  	 * @return the parent dir path
  	 */
	public static String getParentDirPath(String pPath) {
		String lPath = pPath;
		if (lPath == null) {
			return null;
		}
		int lastUnixPos = lPath.lastIndexOf(TRURRInFeedConstants.FORTY_SEVEN);
		int lastWindowsPos = lPath.lastIndexOf(TRURRInFeedConstants.NINETY_TWO);
		int index = Math.max(lastUnixPos, lastWindowsPos);
		if (index <= TRURRInFeedConstants.INT_ZERO) {
			return null;
		}
		while (index == lPath.length() - TRURRInFeedConstants.INT_ONE) {
			lPath = lPath.substring(TRURRInFeedConstants.INT_ZERO, index);
			lastUnixPos = lPath.lastIndexOf(TRURRInFeedConstants.FORTY_SEVEN);
			lastWindowsPos = lPath.lastIndexOf(TRURRInFeedConstants.NINETY_TWO);
			index = Math.max(lastUnixPos, lastWindowsPos);
			if (index <= TRURRInFeedConstants.INT_ZERO){
				return null;
			}
		}
		return lPath.substring(TRURRInFeedConstants.INT_ZERO, (index+TRURRInFeedConstants.INT_ONE));
	}
	
	/**
	 * Gets the file name without extension.
	 *
	 * @param pFilename the filename
	 * @return the file name without extension
	 */
	public static String getFileNameWithoutExtension(String pFilename)
	{
	  if (pFilename == null) {
	    return null;
	  }
	  String fileName = getFileName(pFilename);
	  
	  int index = fileName.lastIndexOf(TRURRInFeedConstants.FORTY_SIX);
	  if (index ==TRURRInFeedConstants.MINUS_ONE) {
	    return TRURRInFeedConstants.EMPTY_STRING;
	  }
	  return fileName.substring(TRURRInFeedConstants.INT_ZERO,index);
	}

}
