/*
 * 
 */
package com.tru.feed.processor;


/**
 * The Class FeedErrorInfo.
 * 
 * This class is responsible to hold feed error info object 
 * @author PA
 * @version 1.0
 */
public class FeedErrorInfo {
	
	/** The Repository id. */
	private String mRepositoryId;
	
	/** The Online pid. */
	private String mOnlinePid;
	
	/** The Error message. */
	private String mErrorMessage;
	
	/** The File name. */
	private String mFileName;
	
	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the new file name
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the repository id.
	 *
	 * @return the repository id
	 */
	public String getRepositoryId() {
		return mRepositoryId;
	}

	/**
	 * Sets the repository id.
	 *
	 * @param pRepositoryId the new repository id
	 */
	public void setRepositoryId(String pRepositoryId) {
		mRepositoryId = pRepositoryId;
	}

	/**
	 * Gets the online pid.
	 *
	 * @return the online pid
	 */
	public String getOnlinePid() {
		return mOnlinePid;
	}

	/**
	 * Sets the online pid.
	 *
	 * @param pOnlinePid the new online pid
	 */
	public void setOnlinePid(String pOnlinePid) {
		mOnlinePid = pOnlinePid;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the new error message
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}
	
}
