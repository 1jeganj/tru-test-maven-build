package com.tru.feed.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class FeedStepInfo.
 * 
 * This class is responsible to hold step data and from pass from one step to another step 
 * @author PA
 * @version 1.0
 */
public class FeedStepInfo {
	
	/**
	 * Instantiates a new feed step info.
	 */
	public FeedStepInfo() {
		this.mDataHolder = new HashMap<String, Object>();
	}

	/** The Data holder. */
	private Map<String, Object> mDataHolder;

	/** The Exception. */
	private boolean mException;
	
	/** The Items committed. */
	private List<String> mItemsCommitted;
	
	/** The Failed items. */
	private List<String> mFailedItems;
	
	/** The Total item count. */
	private int mTotalItemCount;
	
	/** The Total items committed. */
	private int mTotalItemsCommitted;
	
	/** The Total items failed. */
	private int mTotalItemsFailed;

	/**
	 * Gets the total item count.
	 *
	 * @return the total item count
	 */
	public int getTotalItemCount() {
		return mTotalItemCount;
	}

	/**
	 * Sets the total item count.
	 *
	 * @param pTotalItemCount the new total item count
	 */
	public void setTotalItemCount(int pTotalItemCount) {
		mTotalItemCount = pTotalItemCount;
	}

	/**
	 * Gets the total items committed.
	 *
	 * @return the total items committed
	 */
	public int getTotalItemsCommitted() {
		return mTotalItemsCommitted;
	}

	/**
	 * Sets the total items committed.
	 *
	 * @param pTotalItemsCommitted the new total items committed
	 */
	public void setTotalItemsCommitted(int pTotalItemsCommitted) {
		mTotalItemsCommitted = pTotalItemsCommitted;
	}

	/**
	 * Gets the total items failed.
	 *
	 * @return the total items failed
	 */
	public int getTotalItemsFailed() {
		return mTotalItemsFailed;
	}

	/**
	 * Sets the total items failed.
	 *
	 * @param pTotalItemsFailed the new total items failed
	 */
	public void setTotalItemsFailed(int pTotalItemsFailed) {
		mTotalItemsFailed = pTotalItemsFailed;
	}

	/**
	 * Checks if is exception.
	 *
	 * @return true, if is exception
	 */
	public boolean isException() {
		return mException;
	}

	/**
	 * Sets the exception.
	 *
	 * @param pException the new exception
	 */
	public void setException(boolean pException) {
		mException = pException;
	}

	/**
	 * Gets the data.
	 *
	 * @param pKey the key
	 * @return the data
	 */
	public Object getData(String pKey) {
		return mDataHolder.get(pKey);
	}

	/**
	 * Gets the items committed.
	 *
	 * @return the items committed
	 */
	public List<String> getItemsCommitted() {
		return mItemsCommitted;
	}

	/**
	 * Sets the items committed.
	 *
	 * @param pItemsCommitted the new items committed
	 */
	public void setItemsCommitted(List<String> pItemsCommitted) {
		mItemsCommitted = pItemsCommitted;
	}

	/**
	 * Gets the failed items.
	 *
	 * @return the failed items
	 */
	public List<String> getFailedItems() {
		return mFailedItems;
	}

	/**
	 * Sets the failed items.
	 *
	 * @param pFailedItems the new failed items
	 */
	public void setFailedItems(List<String> pFailedItems) {
		mFailedItems = pFailedItems;
	}
	
	/**
	 * Put data.
	 *
	 * @param pKey the key
	 * @param pObject the object
	 */
	@SuppressWarnings("unchecked")
	public void putData(String pKey, Object pObject) {
		if (mDataHolder.containsKey(pKey)) {
			Object obj = mDataHolder.get(pKey);
			if (obj instanceof List) {
				List<String> lt = ((List<String>) obj);
				if (!lt.contains(pObject)) {
					lt.add((String) pObject);
				}
			} else if (pObject instanceof String && obj instanceof String) {
				List<String> lStr = new ArrayList<String>();
				lStr.add((String) obj);
				lStr.add((String) pObject);
				mDataHolder.put(pKey, lStr);
			} else {
				mDataHolder.put(pKey, pObject);
			}
		} else {
			mDataHolder.put(pKey, pObject);
		}

	}
}
