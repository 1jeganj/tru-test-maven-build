package com.tru.feed.constants;


/**
 * The Class TRURRInFeedConstants. Class contains the constants related to PowerReview feed.
 * @author PA
 * @version 1.0
 */
public class TRURRInFeedConstants {
	/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_IMPORT = "yyyyMMddhhmmss";
	
	/** The Constant POWER_REVIEW_REVIEWRATING. */
	public static final String POWER_REVIEW_ENTITY_NAME = "regularSKU";
	
	/** Constant for BUFFER_SIZE. */
	public static final int BUFFER_SIZE = 4096;
	/** The Constant to hold number ten. */
	public static final int NUMBER_TEN = 10;
	/** The Constant to hold number six. */
	public static final int NUMBER_SIX = 6;
	/** The Constant to hold number fourteen. */
	public static final int NUMBER_FOURTEEN = 14;
	/** Constant for Minus One. */
	public static final int MINUS_ONE = -1;
	/** Constant for DIV. */
	public static final String DIV = "div";
	/** Constant for CONTENT_DISPOSITION. */
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	/** Constant for FILE_NAME. */
	public static final String FILE_NAME = "filename=";
	/** Constant for SLASH. */
	public static final String SLASH = "/";
	/** Constant for SLASH. */
	public static final String BACKWORD_SLASH = "\\";
	/** Constant for INLINE_FIRST. */
	public static final String INLINE_FIRST = "pwr/x86p4tq4/inline/00/00/";
	/** Constant for INLINE_LAST. */
	public static final String INLINE_LAST = "-en_US-1-reviews.html";
	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";
	/** The Constant NULL_STRING. */
	public static final String NULL_STRING = "null";
	
	/** Constant to hold zero. */
	public static final int INT_ZERO = 0;
	/** Constant to hold one. */
	public static final int INT_ONE = 1;

	/** Constant to hold one. */
	public static final int INT_TWO = 2;

	/** Constant to hold one. */
	public static final int INT_THREE = 3;

	/** Constant to hold one. */
	public static final int INT_FOUR = 4;

	/** Constant to hold one. */
	public static final int INT_FIVE = 5;
	
	/** The Constant INT_SIX. */
	public static final int INT_SIX=6;
	
	/** The Constant FORTY SIX  */
	public static final int FORTY_SIX = 46;
	
	/** The Constant FORTY SEVEN */
	public static final int FORTY_SEVEN = 47;
	
	/** The Constant NINETY TWO  */
	public static final int NINETY_TWO = 92;
	
	/**
	 * Constant for holding the FALSE_FLAG.
	 */
	public static final boolean FALSE_FLAG = false;

	/** The Constant MAX LIMIT. */
	public static final int MAX_LIMIT = 1;

	/** The Constant _1. */
	public static final int _1 = 1;
	
	/** The Constant MESSAGE */
	public static final String MESSAGE = "message";
	
	/** The Constant PWRSUCCESS*/	
	public static final String PWRSUCCESS ="Power Reviews Feed Processed Successfully";
	
	/** The Constant REPOUPDATE*/	
	public static final String REPOUPDATE ="Power Reviews data Updating to Repository Failed ";
	
	/** The Constant XMLINVALID */
	public static final String XMLINVALID = "Power Reviews XML/XSD Validation Failed";
	
	/** The Constant FILENOTEXIST */
	public static final String FILENOTEXIST = "Power Reviews Feed File does not exists";
	
	/** The Constant URLERROR */
	public static final String URLERROR = "Error while processing Power Review dowonlaod file URL ";
	
	/** The Constant Total Item Count */
	public static final String TOTAL_ITEM_COUNT = "totalItemCount";
	
	/** The Constant Total Item Count updated*/
	public static final String TOTAL_ITEM_COUNT_UPDATED = "totalItemUpdated";
	
	/** The Constant Total Item Count failed */
	public static final String TOTAL_ITEM_COUNT_FAILED = "totalItemFailToUpdate";
	
	/** The Constant TEMPLATE_PARAMETER_MESSAGE_CONTENT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_CONTENT = "messageContent";
	
	/** The Constant JOB_SUCCESS. */
	public static final String JOB_SUCCESS= "Job processed successfully";
	
	/** The Constant SERVERCONFAIL */
	public static final String SERVERCONFAIL = "HTTP URL Connection to Server Failed.So Not able to Download Power Reviews Feed File";
	
	/** The Constant LATESTFEEDERROR */
	public static final String LATESTFEEDERROR = "Job Failed - review_data_summary.xml is not available @";
	
	/** The Constant AT */
	public static final String AT =" at ";
	
	/** The Constant PWRTIME */
	
	public static final String PWRTIME ="dd-MM-YYYY-HH:mm:ss";
	
	
	/** The Constant NOACCESS  */
	public static final String NOACCESS = " - permissions issues.";

	/** The Constant JOBFAIL  */
	public static final String JOBFAIL = "Job Failed - Folder ";
	
	/** The Constant NOFOLDER  */
	public static final String NOFOLDER = " - structure not available";
	
	/** The Constant Colon  */
	public static final String COLON = ":";
	
	/** The Constant DOT  */
	public static final String DOT = ".";
	
	/** The Constant HTTP  */
	public static final String HTTP = "http";
	
	/** The Constant PRODUCTS  */
	public static final String PRODUCTS = "products";
	
	/** The Constant XML  */
	public static final String XML = "xml";
	
	/** The Constant HTTP URL FORMAT  */
	public static final String HTTP_URL_FORMAT = "http://";
	
	/** The Constant ONLINEPID SKUID  */
	public static final String ONLINEPID_SKUID = "onlinepid_skuid";
	
	/** The Constant powerReviewFeed  */
	public static final String POWER_REVIEW_FEED = "powerReviewFeed";
	
	/** The Constant Feed processing  */
	public static final String FEED_PROCESSING = "Feed processing";
	
	/** The Constant exception  */
	public static final String FEED_EXCEPTION = "Exception";
	
	/** The Constant exception  */
	public static final String FEED_TIMEZONE = "timezone";
	
	/** The Constant exception  */
	public static final String DETAIL_EXCEPTION = "detailException";

	/** The Constant skuErrorInfo  */
	public static final String SKU_ERROR_INFO = "skuErrorInfo";
	
	/** The Constant exception  */
	public static final String ERROR_MESSAGE = "errorMessage";
	/** The Constant exception  */
	public static final String FILE_DIR_NOT_CONFIG = "File or Directory path has not been configured";
	/** The Constant SHEET. */
	public static final String SHEET = "sheet";
	/** property to hold PRODUCT_CANONICAL_ERROR_FILENAME. */
	public static final String ERRORS_POWERREVIEW_FEED = "Errors_PowerReviewFeed";
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT_1 = "MMddyyyy'_'HH-mm-ss";
	/** The Constant PROCESS_ON. */
	public static final String PROCESS_ON= "_Processed_";
	/** The Constant XLS. */
	public static final String XLS = "xls";
	/** The Constant UNDER_SCORE. */
	public static final String UNDER_SCORE = "_";
	/** The Constant ERROR_DIRECTORY. */
	public static final String ERROR_DIRECTORY = "errorDirectory";
	/** The Constant XML_FILE_NAME. */
	public static final String XML_FILE_NAME = "fileName";
	/** The Constant JOB_OCCUPIED_MESSAGE. */
	public static final String JOB_OCCUPIED_MESSAGE = "Already Job has been occupied to Task";
	/** The Constant FILE_NOT_EXITS_DIRECTORY_ERROR. */
	public static final String FILE_NOT_EXITS_DIRECTORY_ERROR = "File : %1$s  does not exits directory : %2$s";
}