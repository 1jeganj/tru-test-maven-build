package com.tru.feed.job;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceException;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feed.constants.TRURRInFeedConstants;


/**
 * The Class TRUPowerReviewFeedScheduler.
 * 
 * This class is responsible for review rating feed processing. This scheduler runs in
 * every 24 hours.
 * @author PA
 * @version 1.0
 */
public class TRUPowerReviewFeedScheduler extends SingletonSchedulableService {
	
	/** Property to hold mFeedJob. */
	private TRUPowerReviewFeedJobInvoker mFeedJob;
	
	/** The Enable. */
	private boolean mEnable;
	
	/**
	 * Gets the feed job.
	 *
	 * @return the feed job
	 */
	public TRUPowerReviewFeedJobInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 *
	 * @param pFeedJob the new feed job
	 */
	public void setFeedJob(TRUPowerReviewFeedJobInvoker pFeedJob) {
		mFeedJob = pFeedJob;
	}

	/**
	 * Checks if is enable.
	 *
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Method responsible for starting the feed scheduling based on configured
	 * scheduler.
	 * 
	 * @throws ServiceException
	 *             the service exception
	 */
	@Override
	public void doStartService() throws ServiceException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUPowerReviewFeedScheduler import scheduler doStartService() method");
		}		
		if (!StringUtils.isBlank(getJobName()) && isEnable()) {
			final ScheduledJob job = new ScheduledJob(getJobName(),
					TRURRInFeedConstants.COLON, getAbsoluteName(), getSchedule(),
					this, ScheduledJob.SCHEDULER_THREAD);
			if (isLoggingDebug()) {
				logDebug("The created job object is " + job);
			}
			mJobId = getScheduler().addScheduledJob(job);
			if (isLoggingDebug()) {
				logDebug("The created jobId is  " + mJobId);
			}
		} else {
			if (isLoggingError()) {
				logError("Job Name is Empty hence PowerReviewFeedScheduler could not proceed the job.");
			}
		}
	}

	/**
	 * This method is extended from SingletonSchedulableService class to execute
	 * the scheduler task.
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingInfo()) {
			logInfo("BEGIN : TRUPowerReviewFeedScheduler(doScheduledTask)");
		}
		if(isEnable()){
			getFeedJob().invoke(getJobName());
		}
		if (isLoggingInfo()) {
			logInfo("END : TRUPowerReviewFeedScheduler(doScheduledTask)");
		}
	}


	
}
