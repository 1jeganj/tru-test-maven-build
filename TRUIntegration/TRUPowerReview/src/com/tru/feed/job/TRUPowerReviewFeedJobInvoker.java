package com.tru.feed.job;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.service.perfmonitor.PerformanceMonitor;

import com.tru.feed.constants.TRURRInFeedConstants;
import com.tru.feed.email.TRUPowerReviewEmailSender;
import com.tru.feed.processor.FeedErrorInfo;
import com.tru.feed.processor.FeedStepInfo;
import com.tru.feed.processor.FeedTask;
import com.tru.feed.processor.FeedUtils;
import com.tru.feed.processor.config.PowerReviewFeedConfig;
import com.tru.feed.processor.exception.FeedJobExecutionException;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPowerReviewFeedJobInvoker.
 * 
 * This class is responsible for invoking task and sending job status in mail 
 * @author PA
 * @version 1.0
 */
public class TRUPowerReviewFeedJobInvoker extends GenericService{
	
	/** The Feed task. */
	private FeedTask mFeedTask;
	
	/** The Feed task. */
	private boolean mIsJobOccupied;

	/** The Email sender. */
	private TRUPowerReviewEmailSender mEmailSender;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/** The Feed config. */
	private PowerReviewFeedConfig mFeedConfig;
	
	/**
	 * Gets the feed config.
	 *
	 * @return the feed config
	 */
	public PowerReviewFeedConfig getFeedConfig() {
		return mFeedConfig;
	}

	/**
	 * Sets the feed config.
	 *
	 * @param pFeedConfig the new feed config
	 */
	public void setFeedConfig(PowerReviewFeedConfig pFeedConfig) {
		mFeedConfig = pFeedConfig;
	}
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}


	/**
	 * Gets the feed task.
	 *
	 * @return the feed task
	 */
	public FeedTask getFeedTask() {
		return mFeedTask;
	}

	/**
	 * Sets the feed task.
	 *
	 * @param pFeedTask the new feed task
	 */
	public void setFeedTask(FeedTask pFeedTask) {
		mFeedTask = pFeedTask;
	}
	
	/**
	 * Gets the email sender.
	 *
	 * @return the email sender
	 */
	public TRUPowerReviewEmailSender getEmailSender() {
		return mEmailSender;
	}

	/**
	 * Sets the email sender.
	 *
	 * @param pEmailSender the new email sender
	 */
	public void setEmailSender(TRUPowerReviewEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	
	/**
	 * Invoke.
	 *
	 * @param pJobName the job name
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean invoke(String pJobName) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedJobInvoker.invoke()");
		}
		if (!mIsJobOccupied) {
			FeedStepInfo stepInfo = new FeedStepInfo();
			int flag = TRURRInFeedConstants.INT_ZERO;
			try {
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.startOperation(pJobName);
				}
				mIsJobOccupied = Boolean.TRUE;
				flag = getFeedTask().executeTask(stepInfo);
				if (PerformanceMonitor.isEnabled()){
					PerformanceMonitor.endOperation(pJobName);
				}
			} catch (FeedJobExecutionException fjEx) {
				if (isLoggingError()) {
					logError(fjEx);
				}
				String destFilePath = getDestFileAbsolutePath(getFeedConfig().getErrorDirectory());
				List<FeedErrorInfo> errorInfoList = (List<FeedErrorInfo>) stepInfo
						.getData(TRURRInFeedConstants.SKU_ERROR_INFO);
				moveFileToDirectory(destFilePath);
				Map<String, Object> mapObjet = new HashMap<String, Object>();
				mapObjet.put(TRURRInFeedConstants.MESSAGE, stepInfo.getData(TRURRInFeedConstants.ERROR_MESSAGE));
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT, stepInfo.getTotalItemCount());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_UPDATED, stepInfo.getTotalItemsCommitted());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_FAILED, stepInfo.getTotalItemsFailed());
				mapObjet.put(TRURRInFeedConstants.DETAIL_EXCEPTION, fjEx);
				mapObjet.put(TRURRInFeedConstants.XML_FILE_NAME, destFilePath);
				mapObjet.put(TRURRInFeedConstants.ERROR_DIRECTORY, FeedUtils.getParentDirPath(destFilePath));
				getEmailSender().sendFailureEmail(mapObjet, errorInfoList);
				return false;
			} finally {
				mIsJobOccupied = Boolean.FALSE;
			}

			if (flag == TRURRInFeedConstants.INT_ONE && !stepInfo.isException()) {

				String destFilePath = getDestFileAbsolutePath(getFeedConfig().getSuccessDirectory());
				List<FeedErrorInfo> errorInfoList = (List<FeedErrorInfo>) stepInfo
						.getData(TRURRInFeedConstants.SKU_ERROR_INFO);
				moveFileToDirectory(destFilePath);

				Map<String, Object> mapObjet = new HashMap<String, Object>();
				mapObjet.put(TRURRInFeedConstants.MESSAGE, TRURRInFeedConstants.PWRSUCCESS);
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT, stepInfo.getTotalItemCount());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_UPDATED, stepInfo.getTotalItemsCommitted());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_FAILED, stepInfo.getTotalItemsFailed());
				mapObjet.put(TRURRInFeedConstants.XML_FILE_NAME, destFilePath);
				mapObjet.put(TRURRInFeedConstants.ERROR_DIRECTORY, FeedUtils.getParentDirPath(destFilePath));
				getEmailSender().sendSuccessEmail(mapObjet, errorInfoList);
				if (isLoggingDebug()) {
					logDebug("Exit from :: TRUPowerReviewFeedJobInvoker.invoke()");
				}
				return true;
			} else {

				String destFilePath = getDestFileAbsolutePath(getFeedConfig().getErrorDirectory());
				List<FeedErrorInfo> errorInfoList = (List<FeedErrorInfo>) stepInfo
						.getData(TRURRInFeedConstants.SKU_ERROR_INFO);
				moveFileToDirectory(destFilePath);

				Map<String, Object> mapObjet = new HashMap<String, Object>();
				mapObjet.put(TRURRInFeedConstants.MESSAGE, stepInfo.getData(TRURRInFeedConstants.ERROR_MESSAGE));
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT, stepInfo.getTotalItemCount());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_UPDATED, stepInfo.getTotalItemsCommitted());
				mapObjet.put(TRURRInFeedConstants.TOTAL_ITEM_COUNT_FAILED, stepInfo.getTotalItemsFailed());
				mapObjet.put(TRURRInFeedConstants.ERROR_DIRECTORY, destFilePath);
				mapObjet.put(TRURRInFeedConstants.XML_FILE_NAME, FeedUtils.getParentDirPath(destFilePath));
				getEmailSender().sendFailureEmail(mapObjet, errorInfoList);
				if (isLoggingDebug()) {
					logDebug("Exit from :: TRUPowerReviewFeedJobInvoker.invoke()");
				}
				return false;
			}
		} else {
			throw new FeedJobExecutionException(TRURRInFeedConstants.JOB_OCCUPIED_MESSAGE);
		}

	}
	
	/**
	 * Do invoke feed job.
	 */
	public void doInvokeFeedJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedJobInvoker.doInvokeFeedJob()");
		}
			invoke(TRURRInFeedConstants.POWER_REVIEW_FEED);
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedJobInvoker.doInvokeFeedJob()");
		}
	}
	
	/**
	 * Gets the destination file absolute path.
	 *
	 * @param pDirectoryName the directory name
	 * @return the destination file absolute path
	 */
	private String getDestFileAbsolutePath(String pDirectoryName){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedJobInvoker.getDestFileAbsolutePath()");
		}
		String parentDir = FeedUtils.getParentDirPath(getFeedConfig().getDestinationDir());
		String filePath  = getFeedConfig().getFileDestinationPath();
		StringBuilder sb = new StringBuilder(parentDir);
		sb.append(pDirectoryName);
		File fileDir = new File(sb.toString());
		if(!fileDir.exists()){
			fileDir.mkdirs();
		}
		int lastUnixPos = parentDir.lastIndexOf(TRURRInFeedConstants.FORTY_SEVEN);
		if(lastUnixPos != TRURRInFeedConstants.MINUS_ONE){
			sb.append(TRURRInFeedConstants.SLASH);
		}else{
			sb.append(TRURRInFeedConstants.BACKWORD_SLASH);
		}
		sb.append(FeedUtils.getFileNameWithoutExtension(filePath));
		sb.append(TRURRInFeedConstants.UNDER_SCORE);
		DateFormat dateFormat = new SimpleDateFormat(TRURRInFeedConstants.TIMESTAMP_FORMAT_FOR_IMPORT);
		String dateStr = dateFormat.format(new Date());
		sb.append(dateStr);
		sb.append(TRURRInFeedConstants.DOT);
		sb.append(TRURRInFeedConstants.XML);
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedJobInvoker.getDestFileAbsolutePath()");
		}
		return sb.toString();
	}
	
  /**
   * Move file to directory.
   *
   * @param pDestFilePath the directory name
   */
  private void moveFileToDirectory(String pDestFilePath){
	  if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewFeedJobInvoker.moveFileToDirectory()");
		}
	  if(!StringUtils.isBlank(pDestFilePath)){
		String filePath  = getFeedConfig().getFileDestinationPath();
		
		File destFile = new File(filePath);

		if(destFile.exists()){
			destFile.renameTo(new File(pDestFilePath));
		}
	  }
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewFeedJobInvoker.moveFileToDirectory()");
		}
  }
	
}
