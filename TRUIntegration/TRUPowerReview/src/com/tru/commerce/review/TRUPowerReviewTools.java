/*
 * 
 */
package com.tru.commerce.review;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.commerce.review.repository.TRUPowerReviewRepositoryProperties;

/**
 * The Class TRUPowerReviewTools.
 */
public class TRUPowerReviewTools extends GenericService {

	/** The Repository. */
	private Repository mRepository;
	
	/** The Repository properties. */
	private TRUPowerReviewRepositoryProperties mRepositoryProperties; 

	/**
	 * Gets the repository.
	 *
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * Sets the repository.
	 *
	 * @param pRepository the new repository
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}
	
	/**
	 * Gets the repository properties.
	 *
	 * @return the repository properties
	 */
	public TRUPowerReviewRepositoryProperties getRepositoryProperties() {
		return mRepositoryProperties;
	}

	/**
	 * Sets the repository properties.
	 *
	 * @param pRepositoryProperties the new repository properties
	 */
	public void setRepositoryProperties(TRUPowerReviewRepositoryProperties pRepositoryProperties) {
		mRepositoryProperties = pRepositoryProperties;
	}

	/**
	 * Gets the power review item.
	 *
	 * @param pSkuId the sku id
	 * @return the power review item
	 */
	public RepositoryItem getPowerReviewItem(String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewTools.getPowerReviewItem()");
		}
		
		RepositoryItem item = null;
		try {
			item = getRepository().getItem(pSkuId,getRepositoryProperties().getPowerReviewItemDescriptorName());
		} catch (RepositoryException e) {
			if(isLoggingError()){
				logError("Exception :",e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewTools.getPowerReviewItem()");
		}
		return item;
	}
	
	
	/**
	 * Gets the power review rating.
	 *
	 * @param pSkuId the sku id
	 * @return the power review rating
	 */
	public Float getPowerReviewRating(String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewTools.getPowerReviewRating()");
		}
		Float propValue = null;
		RepositoryItem prItem = getPowerReviewItem(pSkuId);
		if(prItem != null){
			propValue =(Float) prItem.getPropertyValue(getRepositoryProperties().getReviewRatingPropertyName());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewTools.getPowerReviewRating()");
		}
		return propValue;
		
	}
	
	
	/**
	 * Gets the power review count.
	 *
	 * @param pSkuId the sku id
	 * @return the power review count
	 */
	public int getPowerReviewCount(String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewTools.getPowerReviewCount()");
		}
		int propValue = 0;
		RepositoryItem prItem = getPowerReviewItem(pSkuId);
		if(prItem != null){
			Float val =(Float) prItem.getPropertyValue(getRepositoryProperties().getReviewCountPropertyName());
			propValue = Math.round(val);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewTools.getPowerReviewCount()");
		}
		return propValue;
	}
	
	/**
	 * Gets the power review count decimal.
	 *
	 * @param pSkuId the sku id
	 * @return the power review count decimal
	 */
	public Float getPowerReviewCountDecimal(String pSkuId){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUPowerReviewTools.getPowerReviewCountDecimal()");
		}
		Float propValue = null;
		RepositoryItem prItem = getPowerReviewItem(pSkuId);
		if(prItem != null){
			propValue =(Float) prItem.getPropertyValue(getRepositoryProperties().getReviewCountDecimalPropertyName());
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUPowerReviewTools.getPowerReviewCountDecimal()");
		}
		return propValue;
	}
	
}
