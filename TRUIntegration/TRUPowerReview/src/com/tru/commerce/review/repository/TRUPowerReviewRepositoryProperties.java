package com.tru.commerce.review.repository;

import atg.nucleus.GenericService;

/**
 * This class user to hold PowerReviewRepository properties and details
 */
public class TRUPowerReviewRepositoryProperties extends GenericService {

	/** The Power review item descriptor name. */
	private String mPowerReviewItemDescriptorName;
	
	/** The Id property name. */
	private String mIdPropertyName;
	
	/** The Online pid property name. */
	private String mOnlinePidPropertyName;
	
	/** The Review rating property name. */
	private String mReviewRatingPropertyName;
	
	/** The Review count property name. */
	private String mReviewCountPropertyName;
	
	/** The Review count decimal property name. */
	private String mReviewCountDecimalPropertyName;
	
	/**
	 * Gets the power review item descriptor name.
	 *
	 * @return the power review item descriptor name
	 */
	public String getPowerReviewItemDescriptorName() {
		return mPowerReviewItemDescriptorName;
	}
	
	/**
	 * Sets the power review item descriptor name.
	 *
	 * @param pPowerReviewItemDescriptorName the new power review item descriptor name
	 */
	public void setPowerReviewItemDescriptorName(String pPowerReviewItemDescriptorName) {
		mPowerReviewItemDescriptorName = pPowerReviewItemDescriptorName;
	}
	
	/**
	 * Gets the id property name.
	 *
	 * @return the id property name
	 */
	public String getIdPropertyName() {
		return mIdPropertyName;
	}
	
	/**
	 * Sets the id property name.
	 *
	 * @param pIdPropertyName the new id property name
	 */
	public void setIdPropertyName(String pIdPropertyName) {
		mIdPropertyName = pIdPropertyName;
	}
	
	/**
	 * Gets the online pid property name.
	 *
	 * @return the online pid property name
	 */
	public String getOnlinePidPropertyName() {
		return mOnlinePidPropertyName;
	}
	
	/**
	 * Sets the online pid property name.
	 *
	 * @param pOnlinePidPropertyName the new online pid property name
	 */
	public void setOnlinePidPropertyName(String pOnlinePidPropertyName) {
		mOnlinePidPropertyName = pOnlinePidPropertyName;
	}
	
	/**
	 * Gets the review rating property name.
	 *
	 * @return the review rating property name
	 */
	public String getReviewRatingPropertyName() {
		return mReviewRatingPropertyName;
	}
	
	/**
	 * Sets the review rating property name.
	 *
	 * @param pReviewRatingPropertyName the new review rating property name
	 */
	public void setReviewRatingPropertyName(String pReviewRatingPropertyName) {
		mReviewRatingPropertyName = pReviewRatingPropertyName;
	}
	
	/**
	 * Gets the review count property name.
	 *
	 * @return the review count property name
	 */
	public String getReviewCountPropertyName() {
		return mReviewCountPropertyName;
	}
	
	/**
	 * Sets the review count property name.
	 *
	 * @param pReviewCountPropertyName the new review count property name
	 */
	public void setReviewCountPropertyName(String pReviewCountPropertyName) {
		mReviewCountPropertyName = pReviewCountPropertyName;
	}
	
	/**
	 * Gets the review count decimal property name.
	 *
	 * @return the review count decimal property name
	 */
	public String getReviewCountDecimalPropertyName() {
		return mReviewCountDecimalPropertyName;
	}
	
	/**
	 * Sets the review count decimal property name.
	 *
	 * @param pReviewCountDecimalPropertyName the new review count decimal property name
	 */
	public void setReviewCountDecimalPropertyName(String pReviewCountDecimalPropertyName) {
		mReviewCountDecimalPropertyName = pReviewCountDecimalPropertyName;
	}
	
	
}
