package com.tru.integrations.sucn.couponlookup.response;

/**
 * This Class is to maintain the Lookup Response Values.
 * @author PA
 * @version 1.0  
 *
 */
public class CouponLookupResponse {
	
	private String mSucn;
	private String mPromo_Id;
	private int mCode;
	private String mText;
	private String mResponse_Status;
	
	/**
	 * @return the mSucn
	 */
	public String getSucn() {
		return mSucn;
	}
	/**
	 * @param pSucn the mSucn to set
	 */
	public void setSucn(String pSucn) {
		mSucn = pSucn;
	}
	/**
	 * @return the mPromo_id
	 */
	public String getPromoId() {
		return mPromo_Id;
	}
	/**
	 * @param pPromo_id the mPromo_id to set
	 */
	public void setPromoId(String pPromo_id) {
		mPromo_Id = pPromo_id;
	}
	/**
	 * @return the mCode
	 */
	public int getCode() {
		return mCode;
	}
	/**
	 * @param pCode the code to set
	 */
	public void setCode(int pCode) {
		mCode = pCode;
	}
	/**
	 * @return the mText
	 */
	public String getText() {
		return mText;
	}
	/**
	 * @param pText the mText to set
	 */
	public void setText(String pText) {
		mText = pText;
	}
	/**
	 * @return the mResponse_status
	 */
	public String getResponseStatus() {
		return mResponse_Status;
	}
	/**
	 * @param pResponse_status the mResponse_status to set
	 */
	public void setResponseStatus(String pResponse_status) {
		mResponse_Status = pResponse_status;
	}

}
