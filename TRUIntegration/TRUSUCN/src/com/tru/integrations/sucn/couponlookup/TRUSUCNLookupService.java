package com.tru.integrations.sucn.couponlookup;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.databinding.ADBException;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.sucn.common.SUCNIntegrationException;
import com.tru.integrations.sucn.common.TRUSUCNConfiguration;
import com.tru.integrations.sucn.common.TRUSUCNConstants;
import com.tru.integrations.sucn.couponlookup.CouponLookupWebserviceStub.LookupRequestBean;
import com.tru.integrations.sucn.couponlookup.CouponLookupWebserviceStub.LookupResponseBean;
import com.tru.integrations.sucn.couponlookup.response.CouponLookupResponse;
import com.tru.integrations.util.TRUAuditMessageUtil;
import com.tru.integrations.vo.TRUMessageAuditVO;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;

/**
 * This is the Service class , provides Service method for for Coupon Lookup. 
 * After constructing the URL.
 * 
 * @author PA.
 * @version 1.0.
 */

public class TRUSUCNLookupService extends GenericService {
	
	
	/** The Constant SUCN_LOOKUP. */
	public static final String SUCN_LOOKUP = "SUCNLookupService";
	
	/** The Constant SUCN_LOOKUP_SERVICE. */
	public static final String SUCN_LOOKUP_SERVICE = "TRUSUCNLookupService";
	
	/** property to hold tRUConfiguration. */
	private TRUSUCNConfiguration mTRUSUCNConfiguration;

	/** The Tru audit message util. */
	private TRUAuditMessageUtil mTruAuditMessageUtil;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/** The m exception code. */
	private String mExceptionCode;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	/**
	 * Gets the tru audit message util.
	 * 
	 * @return the truAuditMessageUtil
	 */
	public TRUAuditMessageUtil getTruAuditMessageUtil() {
		return mTruAuditMessageUtil;
	}

	/**
	 * Sets the tru audit message util.
	 * 
	 * @param pTruAuditMessageUtil
	 *            the truAuditMessageUtil to set
	 */
	public void setTruAuditMessageUtil(TRUAuditMessageUtil pTruAuditMessageUtil) {
		mTruAuditMessageUtil = pTruAuditMessageUtil;
	}

	/**
	 * This method validates the coupon no entered by user.
	 *
	 * @param pSUCNString            the SUCN string
	 * @param pCustOrderNumber            custOrderNumber
	 * @param pPageName the page name
	 * @return couponResponse String
	 * @throws SUCNIntegrationException             SUCNIntegrationException
	 */

	@SuppressWarnings("unused")
	public CouponLookupResponse lookupCouponDetails(String pSUCNString, String pCustOrderNumber,String pPageName) throws SUCNIntegrationException {

		if (isLoggingDebug()) {
			logDebug("STARTED :: TRUSUCNLookupService ::  lookupCouponDetails");
		}
		CouponLookupWebserviceStub stub = null;
		LookupRequestBean request = null;
		LookupResponseBean response = null;
		CouponLookupResponse couponResponse = null;
		final String couponLookupServiceURL = getTRUSUCNConfiguration().getCouponLookupServiceURL();
		final String couponNumber = pSUCNString;
		final String orderNumber = pCustOrderNumber;
		String lookupServiceExceptionMessage = null;
		request = new LookupRequestBean();
		long starTime = 0;
		long endTime = 0;
		try {
			if (isLoggingDebug()) {
				logDebug("SUCN Coupon Lookup Before Creating Stub...ServiceURL :" + couponLookupServiceURL);
			}

			stub = new CouponLookupWebserviceStub(couponLookupServiceURL);

			if (stub != null) {

				constructRequestForLookupCoupon(request, couponNumber);

				if (isLoggingDebug()) {
					logDebug("SUCN Coupon Lookup  Before Request...Coupon No :" + couponNumber);
					logDebug("SUCN Coupon Lookup  Before Request...Order No :" + orderNumber);
					logDebug("SUCN Coupon Lookup Before Request...ServiceURL :" + couponLookupServiceURL);
					logCustomerInfoSOAPRequestResponse(request, null);
				}
				starTime = Calendar.getInstance().getTimeInMillis();
				getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(TRUSUCNConstants.SUCN_SERVICE), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(TRUSUCNConstants.COUPON_VALIDATION), pCustOrderNumber, null);
				response = stub.lookupCoupon(request);
				endTime = Calendar.getInstance().getTimeInMillis();
				if (response != null) {
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Lookup Response..." + response);
						logCustomerInfoSOAPRequestResponse(null, response);
					}
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(TRUSUCNConstants.SUCN_SERVICE), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(TRUSUCNConstants.COUPON_VALIDATION), pCustOrderNumber, null);
					couponResponse = processCouponLookupResponse(response);
				} else {
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Lookup Response..." + response);
					}
				}
			} else {
				if (isLoggingWarning()) {
					logWarning("Stub is null");
				}
			}
			if (isLoggingDebug()) {
				logDebug("END :: TRUSUCNLookupService ::  lookupCouponDetails");
			}
		} catch (AxisFault pExp) {
			if (isLoggingError()) {
				logError("AxisFault Exception while calling/Processing the Coupon Lookup service ", pExp);
			}
			if(pExp != null && pExp.getCause() != null){
				lookupServiceExceptionMessage = pExp.getCause().toString();
			}
			auditSingleUseCouponLookupService(request, couponResponse, orderNumber, lookupServiceExceptionMessage);
			endTime = Calendar.getInstance().getTimeInMillis();
			sendAlertLogFailure(SUCN_LOOKUP_SERVICE, SUCN_LOOKUP, pPageName, getExceptionCode(), pExp.getMessage(), starTime, endTime);
			throw new SUCNIntegrationException(TRUSUCNConstants.REMOTE_EXCEPTION_THROW, pExp);
		} catch (Exception pExp) {
			if (isLoggingError()) {
				logError("Exception while calling/Processing the Coupon Lookup service ", pExp);
			}
			lookupServiceExceptionMessage = pExp.getCause().toString();
			auditSingleUseCouponLookupService(request, couponResponse, orderNumber, lookupServiceExceptionMessage);
			endTime = Calendar.getInstance().getTimeInMillis();
			sendAlertLogFailure(SUCN_LOOKUP_SERVICE, SUCN_LOOKUP, pPageName, getExceptionCode(), pExp.getMessage(), starTime, endTime);
			throw new SUCNIntegrationException(TRUSUCNConstants.REMOTE_EXCEPTION_THROW, pExp);
		} finally {
			try {
				if (stub != null) {
					stub.cleanup();
				}
			} catch (AxisFault pAxisFaultExpception) {
				lookupServiceExceptionMessage = pAxisFaultExpception.getMessage();
				endTime = Calendar.getInstance().getTimeInMillis();
				sendAlertLogFailure(SUCN_LOOKUP_SERVICE, SUCN_LOOKUP, pPageName, getExceptionCode(), pAxisFaultExpception.getMessage(), starTime, endTime);
				if (isLoggingError()) {
					logError("Exception while calling/Processing the the  Coupon Lookup service  :", pAxisFaultExpception);
				}
			}
		}
		// this method is used to Audit the lookup coupon response and request
		auditSingleUseCouponLookupService(request, couponResponse, orderNumber, lookupServiceExceptionMessage);
		return couponResponse;
	}

	/**
	 * Audit single use coupon lookup service.
	 * 
	 * @param pLookupRequest
	 *            the lookup request
	 * @param pCouponResponse
	 *            the coupon response
	 * @param pOrderNumber
	 *            the order number
	 * @param pLookupServiceExceptionMessage
	 *            the lookup service exception message
	 */
	private void auditSingleUseCouponLookupService(LookupRequestBean pLookupRequest, CouponLookupResponse pCouponResponse,
			String pOrderNumber, String pLookupServiceExceptionMessage) {
		vlogDebug("START: TRUSUCNLookupService.auditSingleUseCouponLookupService()");
		if (getTruAuditMessageUtil().isAuditSUCNMessageIntoDB()) {
			TRUMessageAuditVO messageAuditVO = new TRUMessageAuditVO();
			StringBuffer stringReqAndRes = new StringBuffer();
			if (pLookupRequest != null) {
				stringReqAndRes.append(pLookupRequest.getCouponNumber());
			}
			if (pCouponResponse != null) {
				stringReqAndRes.append(pCouponResponse.getResponseStatus()).append(pCouponResponse.getPromoId());
				messageAuditVO.setMessageData(stringReqAndRes.toString());
			} else {
				messageAuditVO.setMessageData(pLookupServiceExceptionMessage);
			}
			messageAuditVO.setAuditDate(new Timestamp(new Date().getTime()));
			vlogDebug("TRUSUCNLookupService.auditSingleUseCouponLookupService() method, stringReqAndRes:{0}",
					stringReqAndRes.toString());
			messageAuditVO.setMessageType(TRUSUCNConstants.SUCN_LOOKUP_COUPON);
			messageAuditVO.setOrderId(pOrderNumber);
			getTruAuditMessageUtil().auditMessage(messageAuditVO);
		}
		vlogDebug("END: TRUSUCNLookupService.auditSingleUseCouponLookupService()");
	}

	/**
	 * This is the helper method to construct the Request object for the Coupon Lookup Service.
	 * 
	 * 
	 * @param pRequest
	 *            LookupRequestBean
	 * @param pCouponNumber
	 *            couponNumber
	 */
	private void constructRequestForLookupCoupon(LookupRequestBean pRequest, String pCouponNumber) {

		if (isLoggingDebug()) {
			logDebug("Begin :: TRUSUCNLookupService ::  constructRequestForLookupCoupon");
		}

		if ((pCouponNumber != null) && (!pCouponNumber.isEmpty())) {
			pRequest.setCouponNumber(pCouponNumber);
		} else {
			if (isLoggingWarning()) {
				logWarning("couponNumber is null / Empty");
			}
		}
		if (isLoggingDebug()) {
			logDebug("END :: TRUSUCNLookupService ::  constructRequestForLookupCoupon");
		}
	}

	/**
	 * This is the helper method to process the Coupon Lookup Response.
	 * 
	 * @param pResponse
	 *            LookupResponseBean
	 * @return lookupResopnse
	 */
	private CouponLookupResponse processCouponLookupResponse(LookupResponseBean pResponse) {

		if (isLoggingDebug()) {
			logDebug("Begin :: TRUSUCNLookupService ::  processCouponLookupResponse");
		}

		final CouponLookupResponse lookupResopnse = new CouponLookupResponse();
		if (pResponse != null) {
			lookupResopnse.setCode(pResponse.getCode());
			lookupResopnse.setPromoId(pResponse.getPromoId());
			lookupResopnse.setResponseStatus(pResponse.getResponseStatus());
			lookupResopnse.setSucn(pResponse.getCouponNumber());
			lookupResopnse.setText(pResponse.getText());
		}
		if (isLoggingDebug()) {
			logDebug("END :: TRUSUCNLookupService ::  processCouponLookupResponse");
		}
		return lookupResopnse;
	}

	/**
	 * This method prints/logs the Coupon Lookup request and response XML strings.
	 * 
	 * @param pLookupRequest
	 *            - param of type LookupRequestBean.
	 * @param pLookupResponse
	 *            - param of type LookupResponseBean.
	 */
	private void logCustomerInfoSOAPRequestResponse(LookupRequestBean pLookupRequest, LookupResponseBean pLookupResponse) {
		try {
			if (isLoggingDebug()) {
				logDebug("Begin :: TRUSUCNLookupService ::  logCustomerInfoSOAPRequestResponse");
			}
			final SOAPFactory soap12Factory = OMAbstractFactory.getSOAP12Factory();
			OMElement requestOMElement = null;
			OMElement responseOMElement = null;

			if (pLookupRequest != null) {
				requestOMElement = pLookupRequest.getOMElement(pLookupRequest.MY_QNAME, soap12Factory);
				if (requestOMElement != null) {
					final String requestAsString = requestOMElement.toStringWithConsume();

					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Lookup Request XML ..." + requestAsString);
					}
				}
			}
			if (pLookupResponse != null) {
				responseOMElement = pLookupResponse.getOMElement(pLookupResponse.MY_QNAME, soap12Factory);
				if (responseOMElement != null) {
					final String responseAsString = responseOMElement.toStringWithConsume();
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Lookup Responset XML ..." + responseAsString);
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("End :: TRUSUCNLookupService ::  logCustomerInfoSOAPRequestResponse");
			}

		} catch (ADBException pADBException) {
			if (isLoggingError()) {
				logError("Exception while calling/Processing the logCustomerInfoSOAPRequestResponse method ", pADBException);
			}

		} catch (XMLStreamException pXMLStreamException) {
			if (isLoggingError()) {
				logError("Exception while calling/Processing the logCustomerInfoSOAPRequestResponse method ", pXMLStreamException);
			}
		}
	}

	/**
	 * Gets the TRUSUCN configuration.
	 * 
	 * @return the tRUSUCNConfiguration
	 */
	public TRUSUCNConfiguration getTRUSUCNConfiguration() {
		return mTRUSUCNConfiguration;
	}

	/**
	 * Sets the TRUSUCN configuration.
	 * 
	 * @param pTRUSUCNConfiguration
	 *            the tRUSUCNConfiguration to set
	 */
	public void setTRUSUCNConfiguration(TRUSUCNConfiguration pTRUSUCNConfiguration) {
		mTRUSUCNConfiguration = pTRUSUCNConfiguration;
	}
	
	/**
	 * Send alert log failure.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pPageName the page name
	 * @param pResponseCode the response code
	 * @param pError the error
	 * @param pStartTime the start time
	 * @param pEndTime the end time
	 */
	public void sendAlertLogFailure(String pProcessName, String pTask, String pPageName, String pResponseCode, String pError, long pStartTime, long pEndTime) {
		Map<String, String> extraParams = new ConcurrentHashMap<String, String>();
		extraParams.put(TRUSUCNConstants.OPERATION_NAME, pTask);
		if (StringUtils.isNotBlank(pPageName)) {
			extraParams.put(TRUSUCNConstants.PAGE_NAME, pPageName);
		}
		extraParams.put(TRUSUCNConstants.TIME_TAKEN, String.valueOf(pEndTime - pStartTime));
		if (StringUtils.isNotBlank(pError)) {
			extraParams.put(TRUSUCNConstants.ERROR, pError);
		}
		extraParams.put(TRUSUCNConstants.RESPONSE_CODE, pResponseCode);
		getAlertLogger().logFailure(pProcessName, pTask, extraParams);
	}

	/**
	 * @return the mExceptionCode
	 */
	public String getExceptionCode() {
		return mExceptionCode;
	}

	/**
	 * @param pExceptionCode the mExceptionCode to set
	 */
	public void setExceptionCode(String pExceptionCode) {
		mExceptionCode = pExceptionCode;
	}

}
