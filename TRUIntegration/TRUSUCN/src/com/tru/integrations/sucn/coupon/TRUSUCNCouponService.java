package com.tru.integrations.sucn.coupon;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.databinding.ADBException;

import atg.nucleus.GenericService;

import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.Context_type0;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.Context_type1;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.CouponRequestBean;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.CouponResponseBean;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.SourceDetails_type0;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.Status_type0;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.TransactionDetails_type0;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.Type_type1;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.UsageBean;
import com.tru.commerce.integration.sucn.coupon.CouponWebServiceSoapImplStub.VoidTransactionDetails_type0;
import com.tru.integrations.sucn.common.TRUSUCNConfiguration;
import com.tru.integrations.sucn.common.TRUSUCNConstants;
import com.tru.integrations.sucn.coupon.response.CouponResponse;
import com.tru.integrations.util.TRUAuditMessageUtil;
import com.tru.integrations.vo.TRUMessageAuditVO;
import com.tru.logging.TRUIntegrationInfoLogger;

/**
 * This class provides the Service methods for the Use and Void Calls.
 * Construct the Request and process the Response for these calls.
 * 
 * After constructing the URL
 * @author PA.
 * @version 1.0.
 *
 */
public class TRUSUCNCouponService extends GenericService {

	/** property to hold tRUConfiguration. */
	private TRUSUCNConfiguration mTRUSUCNConfiguration;

	/** The Tru audit message util. */
	private TRUAuditMessageUtil mTruAuditMessageUtil;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	/**
	 * Gets the tru audit message util.
	 *
	 * @return the truAuditMessageUtil
	 */
	public TRUAuditMessageUtil getTruAuditMessageUtil() {
		return mTruAuditMessageUtil;
	}

	/**
	 * Sets the tru audit message util.
	 *
	 * @param pTruAuditMessageUtil            the truAuditMessageUtil to set
	 */
	public void setTruAuditMessageUtil(TRUAuditMessageUtil pTruAuditMessageUtil) {
		mTruAuditMessageUtil = pTruAuditMessageUtil;
	}

	/**
	 * This is the Service method for use Coupon. 
	 * This method Updates the status of the coupons use status. the coupon no entered
	 * by user
	 * 
	 * @param pCouponNumber
	 *            the coupon number
	 * @param pOrderNumber
	 *            the order number
	 * @return couponResponse CouponResponse Object
	 */

	@SuppressWarnings("unused")
	public CouponResponse useCouponDetails(String pCouponNumber, String pOrderNumber) {
		if (isLoggingDebug()) {
			logDebug("STARTED :: TRUSUCNCouponService ::  useCouponDetails");
		}
		CouponResponseBean response = null;
		CouponWebServiceSoapImplStub stub = null;
		CouponResponse couponResponse = null;
		final String couponNumber = pCouponNumber;
		final String orderNumber = pOrderNumber;
		String lookupServiceExceptionMessage = null;
		final String couponServiceURL = getTRUSUCNConfiguration().getCouponServiceURL();

		CouponRequestBean request = new CouponRequestBean();
		constructRequestForUseCoupon(request, couponNumber, orderNumber);
		try {
			if (isLoggingDebug()) {
				logDebug("Before creating Stub CouponServiceURL " + couponServiceURL);
			}
			stub = new CouponWebServiceSoapImplStub(couponServiceURL);
			if (stub != null) {
				if (isLoggingDebug()) {
					logDebug("Before Calling UseCoupon Service .. ");
					logCouponInfoSOAPRequestResponse(request, null);
				}
			getIntegrationInfoLogger().logIntegrationEntered(TRUSUCNConstants.SUCN_SERVICE, TRUSUCNConstants.CONSUME_COUPON, pOrderNumber, null);
				response = stub.useCoupon(request);
				if (response != null) {
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Use Response..." + response);
						logCouponInfoSOAPRequestResponse(null, response);
					}
					getIntegrationInfoLogger().logIntegrationExited(TRUSUCNConstants.SUCN_SERVICE, TRUSUCNConstants.CONSUME_COUPON, pOrderNumber, null);
					couponResponse = processCouponUseResponse(response);
				} else {
					if (isLoggingWarning()) {
						logWarning("SUCN Coupon Use Response...");
					}
				}
			} else {
				if (isLoggingWarning()) {
					logWarning("Stub is null");
				}
			}

		} catch (AxisFault pExp) {
			lookupServiceExceptionMessage = pExp.getMessage();
			if (isLoggingError()) {
				logError("AxisFault Exception while calling/Processing the use CouponDetails service ", pExp);
			}
		} catch (Exception pExp) {
			lookupServiceExceptionMessage = pExp.getMessage();
			if (isLoggingError()) {
				logError("Exception while calling/Processing the use CouponDetails service ", pExp);
			}
		} finally {
			try {
				if (stub != null) {
					stub.cleanup();
				}
			} catch (AxisFault pAxisFaultExpception) {
				if (isLoggingError()) {
					logError("Exception while calling/Processing the use CouponDetails Service :", pAxisFaultExpception);
				}
			}
		}
		// this method is used to Audit the lookup coupon response and request
		auditUseAndVoidCouponService(request, couponResponse, orderNumber, lookupServiceExceptionMessage,
				TRUSUCNConstants.SUCN_USE_COUPON);
		return couponResponse;
	}

	/**
	 * Audit use coupon service.
	 *
	 * @param pUseCouponRequest            the use coupon request
	 * @param pCouponResponse            the coupon response
	 * @param pOrderNumber            the order number
	 * @param pLookupServiceExceptionMessage            the lookup service exception message
	 * @param pMessageType the message type
	 */
	private void auditUseAndVoidCouponService(CouponRequestBean pUseCouponRequest, CouponResponse pCouponResponse,
			String pOrderNumber, String pLookupServiceExceptionMessage, String pMessageType) {
		vlogDebug("START: TRUSUCNCouponService.auditUseAndVoidCouponService() method");
		if (getTruAuditMessageUtil().isAuditSUCNMessageIntoDB()) {
			final TRUMessageAuditVO messageAuditVO = new TRUMessageAuditVO();
			final StringBuffer stringReqAndRes = new StringBuffer();
			if (pUseCouponRequest != null) {
				stringReqAndRes.append(pUseCouponRequest.toString());
			}
			if (pCouponResponse != null) {
				stringReqAndRes.append(pCouponResponse.getReturnCode()).append(pCouponResponse.getSUCcouponNumber())
						.append(pCouponResponse.getPreviousUsage());
				messageAuditVO.setMessageData(stringReqAndRes.toString());
			} else {
				messageAuditVO.setMessageData(pLookupServiceExceptionMessage);
			}
			messageAuditVO.setAuditDate(new Timestamp(new Date().getTime()));
			vlogDebug("TRUSUCNCouponService.auditUseAndVoidCouponService() method, stringReqAndRes:{0}",
					stringReqAndRes.toString());
			messageAuditVO.setMessageType(pMessageType);
			messageAuditVO.setOrderId(pOrderNumber);
			getTruAuditMessageUtil().auditMessage(messageAuditVO);
		}
		vlogDebug("END: TRUSUCNCouponService.auditUseAndVoidCouponService() method");
	}

	/**
	 * This Method process the CouponUse Response and create and populate CouponResponse Object.
	 * 
	 * @param pResponse
	 *            Response
	 * @return CouponResponse CouponResponse Object
	 */
	private CouponResponse processCouponUseResponse(CouponResponseBean pResponse) {

		if (isLoggingDebug()) {
			logDebug("Begin :: TRUSUCNCouponService ::  processCouponUseResponse");
		}
		final CouponResponse couponResopnse = new CouponResponse();
		String couponNumber = null;
		int statusCode = 0;
		List<String> transDetails = null;
		List<String> transDates = null;
		List<String> businessDates = null;

		if (pResponse != null) {
			final Context_type1 contextType = pResponse.getContext();
			if (contextType != null) {

				couponNumber = contextType.getCouponNumber();
			}
			final Status_type0 statusType = pResponse.getStatus();
			if (statusType != null) {
				statusCode = statusType.getCode();
			}
			final UsageBean[] usagebeanList = pResponse.getDetails();
			if (usagebeanList != null) {
				transDetails = new ArrayList<String>();
				transDates = new ArrayList<String>();
				businessDates = new ArrayList<String>();

				for (int i = 0; i < usagebeanList.length; i++) {
					transDetails.add(i, usagebeanList[i].getTransactionDetails());
					transDates.add(i, usagebeanList[i].getTransactionDate());
					businessDates.add(i, usagebeanList[i].getBusinessDate());
				}
			}
			couponResopnse.setSUCcouponNumber(couponNumber);
			couponResopnse.setBusinessDate(businessDates);
			couponResopnse.setTransactionDate(transDates);
			couponResopnse.setPreviousUsage(transDetails);
			couponResopnse.setReturnCode(statusCode);

			if (isLoggingDebug()) {
				logDebug("Response Values");
				logDebug("couponNumber :" + couponNumber);
				logDebug("businessDates :" + businessDates);
				logDebug("transDates :" + transDates);
				logDebug("transDetails :" + transDetails);
				logDebug("statusCode :" + statusCode);
			}
		} else {
			if (isLoggingWarning()) {
				logWarning("Response is Null");
			}
		}
		if (isLoggingDebug()) {
			logDebug("End :: TRUSUCNCouponService ::  processCouponUseResponse");
		}

		return couponResopnse;
	}

	/**
	 * This method used to Construct the Request for Use Coupon Service Call.
	 * 
	 * @param pRequest
	 *            Request Object
	 * @param pCouponNumber
	 *            the coupon number
	 * @param pOrderNumber
	 *            the order number
	 */
	private void constructRequestForUseCoupon(CouponRequestBean pRequest, String pCouponNumber, String pOrderNumber) {
		// construct the request object to be sent to UseCouponSerice.

		if (isLoggingDebug()) {
			logDebug("Entering into TRUSUCNCouponService.constructRequestForUseCoupon method");
		}

		final String sucnTransactionDateFormat = getTRUSUCNConfiguration().getSucnTransactionDateFormat();
		final String transactionSource = getTRUSUCNConfiguration().getTransactionSource();

		final SimpleDateFormat transDateFormat = new SimpleDateFormat(sucnTransactionDateFormat, Locale.US);
		final String formattedTransDate = transDateFormat.format(new Date());
		final String sucnBusinessDateFormat = getTRUSUCNConfiguration().getSucnBusinessDateFormat();

		final SimpleDateFormat businessDateFormat = new SimpleDateFormat(sucnBusinessDateFormat, Locale.US);
		final String formattedBusinessDate = businessDateFormat.format(new Date());

		final String couponNumber = (pCouponNumber != null) ? pCouponNumber : TRUSUCNConstants.SUCN_BLANK;
		final String custOrderNum = (pOrderNumber != null) ? pOrderNumber : TRUSUCNConstants.SUCN_BLANK;
		final String sourceId = transactionSource != null ? transactionSource : TRUSUCNConstants.SUCN_BLANK;

		final String clientSequence = getTRUSUCNConfiguration().getClientSequence();
		final String systemRouting = getTRUSUCNConfiguration().getSystemRouting();
		final String contextData = getTRUSUCNConfiguration().getContextData();
		final String transactionId = getTRUSUCNConfiguration().getTransactionId();
		final String store = getTRUSUCNConfiguration().getStore();
		final String register = getTRUSUCNConfiguration().getRegister();
		final String transaction = getTRUSUCNConfiguration().getTransaction();

		/*
		 * if(custOrderNum!=null&&(!custOrderNum.isEmpty())){ transactionId = custOrderNum; }
		 */// commented to fix the Use service Exception
		if (isLoggingDebug()) {
			logDebug("Use Request Parameter Values..");
			logDebug("couponNumber :" + couponNumber);
			logDebug("custOrderNum :" + custOrderNum);
			logDebug("TransactionDateTime :" + formattedTransDate);
			logDebug("BusinessDate :" + formattedBusinessDate);
			logDebug("sourceId :" + sourceId);
			logDebug("clientSequence :" + clientSequence);
			logDebug("systemRouting :" + systemRouting);
			logDebug("contextData :" + contextData);
			logDebug("transactionId :" + transactionId);
			logDebug("store :" + store);
			logDebug("register :" + register);
			logDebug("transaction :" + transaction);
		}
		final Context_type0 context = new Context_type0();

		final Type_type1 type = context.getType().SU;

		context.setType(type);
		context.setClientSequence(clientSequence);
		context.setSystemRouting(systemRouting);
		context.setContextData(contextData);
		context.setCouponNumber(couponNumber);
		final SourceDetails_type0 sourceDetails = new SourceDetails_type0();
		sourceDetails.setSourceId(sourceId);
		sourceDetails.setTransactionId(transactionId);
		final TransactionDetails_type0 transDetail = new TransactionDetails_type0();

		transDetail.setOrderNumber(custOrderNum);
		transDetail.setBusinessDate(formattedBusinessDate);
		transDetail.setTransactionDatetime(formattedTransDate);
		transDetail.setStore(store);
		transDetail.setRegister(register);
		transDetail.setTransaction(transaction);

		pRequest.setContext(context);
		pRequest.setSourceDetails(sourceDetails);
		pRequest.setTransactionDetails(transDetail);

		if (isLoggingDebug()) {
			logDebug("Exit from TRUSUCNCouponService.constructRequestForUseCoupon method");
		}
	}

	/**
	 * This is the Service method for void Coupon. 
	 * This method voids the coupon status and back out the use call.
	 * 
	 * @param pInputMap
	 *            Map<String,String>
	 * @return couponResponse String
	 */

	@SuppressWarnings("unused")
	public CouponResponse voidCouponDetails(Map<String, String> pInputMap) {
		if (isLoggingDebug()) {
			logDebug("STARTED :: TRUSUCNCouponService ::  useCouponDetails");
		}
		CouponResponseBean response = null;
		CouponWebServiceSoapImplStub stub = null;
		CouponResponse couponResponse = null;
		String lookupServiceExceptionMessage = null;
		final String couponServiceURL = getTRUSUCNConfiguration().getCouponServiceURL();
		final String orderNumber = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_ORDER_NUMBER) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_ORDER_NUMBER) : TRUSUCNConstants.SUCN_BLANK;
		CouponRequestBean request = new CouponRequestBean();
		constructRequestForVoidCoupon(request, pInputMap);
		try {
			if (isLoggingDebug()) {
				logDebug("Before creating Stub CouponServiceURL " + couponServiceURL);
			}
			stub = new CouponWebServiceSoapImplStub(couponServiceURL);
			if (stub != null) {
				if (isLoggingDebug()) {
					logDebug("Before Calling VoidCoupon Service ..");
					logCouponInfoSOAPRequestResponse(request, null);
				}
				response = stub.voidCoupon(request);
				if (response != null) {
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Use Response..." + response);
						logCouponInfoSOAPRequestResponse(null, response);
					}
					couponResponse = processCouponVoidResponse(response);
				} else {

					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Use Response..." + response);
					}
				}
			} else {
				if (isLoggingWarning()) {
					logWarning("Stub is null");
				}
			}

		} catch (AxisFault pExp) {
			lookupServiceExceptionMessage = pExp.getCause().toString();
			if (isLoggingError()) {
				logError("AxisFault Exception while calling/Processing the void CouponDetails service ", pExp);
			}
		} catch (Exception pExp) {
			lookupServiceExceptionMessage = pExp.getCause().toString();
			if (isLoggingError()) {
				logError("Exception while calling/Processing the void CouponDetails service ", pExp);
			}
		} finally {
			try {
				if (stub != null) {
					stub.cleanup();
				}
			} catch (AxisFault pAxisFaultExpception) {
				if (isLoggingError()) {
					logError("Exception while calling/Processing the void CouponDetails Service :", pAxisFaultExpception);
				}
			}
		}
		// this method is used to Audit the lookup coupon response and request
		auditUseAndVoidCouponService(request, couponResponse, orderNumber, lookupServiceExceptionMessage,
				TRUSUCNConstants.SUCN_VOID_COUPON);
		return couponResponse;
	}

	/**
	 * This method used to Construct the Request for Void Coupon Service Call.
	 * 
	 * @param pRequest
	 *            Request
	 * @param pInputMap
	 *            Map<String,String>
	 */
	private void constructRequestForVoidCoupon(CouponRequestBean pRequest, Map<String, String> pInputMap) {
		// construct the request object to be sent to UseCouponService.

		if (isLoggingDebug()) {
			logDebug("Entering into TRUSUCNCouponService.constructRequestForUseCoupon method");
		}

		final String sucnTransactionDateFormat = getTRUSUCNConfiguration().getSucnTransactionDateFormat();
		final SimpleDateFormat transDateFormat = new SimpleDateFormat(sucnTransactionDateFormat, Locale.US);
		final String formattedTransDate = transDateFormat.format(new Date());

		final String sucnBusinessDateFormat = getTRUSUCNConfiguration().getSucnBusinessDateFormat();
		final SimpleDateFormat businessDateFormat = new SimpleDateFormat(sucnBusinessDateFormat, Locale.US);
		final String formattedBusinessDate = businessDateFormat.format(new Date());

		final String transactionSource = getTRUSUCNConfiguration().getVoidTransactionSource();

		final String couponNumber = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_SINGLE_USE_COUPON_NUMBER) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_SINGLE_USE_COUPON_NUMBER) : TRUSUCNConstants.SUCN_BLANK;
		final String custOrderNum = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_ORDER_NUMBER) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_ORDER_NUMBER) : TRUSUCNConstants.SUCN_BLANK;
		final String sourceId = transactionSource != null ? transactionSource : TRUSUCNConstants.SUCN_BLANK;
		final String businessDate = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_BUSINIESS_DATE_STRING) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_BUSINIESS_DATE_STRING) : formattedBusinessDate;
		final String transactionDate = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_TRANSACTION_DATE_TIME) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_TRANSACTION_DATE_TIME) : formattedTransDate;
		final String voidBusinessDate = (pInputMap.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_VOID_BUSINIESS_DATE_STRING) != null) ? (String) pInputMap
				.get(TRUSUCNConstants.SUCN_COUPON_PARAMETER_VOID_BUSINIESS_DATE_STRING) : formattedBusinessDate;

		final String clientSequence = getTRUSUCNConfiguration().getClientSequence();
		final String systemRouting = getTRUSUCNConfiguration().getSystemRouting();
		final String contextData = getTRUSUCNConfiguration().getContextData();
		String transactionId = getTRUSUCNConfiguration().getVoidTransactionId();
		final String store = getTRUSUCNConfiguration().getStore();
		final String register = getTRUSUCNConfiguration().getRegister();
		final String transaction = getTRUSUCNConfiguration().getTransaction();
		String userId = TRUSUCNConstants.SUCN_BLANK;

		if (isLoggingDebug()) {

			logDebug("Void Request Parameter Values..");
			logDebug("couponNumber :" + couponNumber);
			logDebug("custOrderNum :" + custOrderNum);
			logDebug("TransactionDateTime :" + transactionDate);
			logDebug("BusinessDate :" + businessDate);
			logDebug("Void BusinessDate :" + voidBusinessDate);
			logDebug("sourceId :" + sourceId);
			logDebug("clientSequence :" + clientSequence);
			logDebug("systemRouting :" + systemRouting);
			logDebug("contextData :" + contextData);
			logDebug("transactionId :" + transactionId);
			logDebug("store :" + store);
			logDebug("register :" + register);
			logDebug("transaction :" + transaction);
		}

		final Context_type0 context = new Context_type0();
		final Type_type1 type = context.getType().SV;

		context.setType(type);
		context.setClientSequence(clientSequence);
		context.setSystemRouting(systemRouting);
		context.setContextData(contextData);
		context.setCouponNumber(couponNumber);

		final SourceDetails_type0 sourceDetails = new SourceDetails_type0();

		sourceDetails.setSourceId(sourceId);
		sourceDetails.setTransactionId(transactionId);

		final TransactionDetails_type0 transDetail = new TransactionDetails_type0();

		transDetail.setOrderNumber(custOrderNum);
		transDetail.setBusinessDate(businessDate);
		transDetail.setTransactionDatetime(transactionDate);
		transDetail.setStore(store);
		transDetail.setRegister(register);
		transDetail.setTransaction(transaction);

		final VoidTransactionDetails_type0 voidTransDetail = new VoidTransactionDetails_type0();

		voidTransDetail.setStore(store);
		voidTransDetail.setRegister(register);
		voidTransDetail.setTransaction(transaction);
		voidTransDetail.setBusinessDate(voidBusinessDate);
		voidTransDetail.setUserId(userId);

		pRequest.setContext(context);
		pRequest.setSourceDetails(sourceDetails);
		pRequest.setTransactionDetails(transDetail);
		pRequest.setVoidTransactionDetails(voidTransDetail);

		if (isLoggingDebug()) {
			logDebug("Exit from TRUSUCNCouponService.constructRequestForVoidCoupon method");
		}
	}

	/**
	 * This method prints/logs the Coupon Use/Void request and response XML. strings.
	 * 
	 * @param pCouponRequest
	 *            - param of type CouponRequestBean.
	 * @param pCouponResponse
	 *            - param of type CouponResponseBean.
	 */
	private void logCouponInfoSOAPRequestResponse(CouponRequestBean pCouponRequest, CouponResponseBean pCouponResponse) {
		try {
			if (isLoggingDebug()) {
				logDebug("Begin :: TRUSUCNCouponService ::  logCouponInfoSOAPRequestResponse");
			}
			final SOAPFactory soap12Factory = OMAbstractFactory.getSOAP12Factory();
			OMElement requestOMElement = null;
			OMElement responseOMElement = null;

			if (pCouponRequest != null) {
				requestOMElement = pCouponRequest.getOMElement(pCouponRequest.MY_QNAME, soap12Factory);
				if (requestOMElement != null) {
					final String requestAsString = requestOMElement.toStringWithConsume();

					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Use/Void Request XML ..." + requestAsString);
					}
				}
			}
			if (pCouponResponse != null) {
				responseOMElement = pCouponResponse.getOMElement(pCouponResponse.MY_QNAME, soap12Factory);
				if (responseOMElement != null) {
					final String responseAsString = responseOMElement.toStringWithConsume();
					if (isLoggingDebug()) {
						logDebug("SUCN Coupon Use/Void Responset XML ..." + responseAsString);
					}
				}
			}
			if (isLoggingDebug()) {
				logDebug("End :: TRUSUCNCouponService ::  logCouponInfoSOAPRequestResponse");
			}

		} catch (ADBException pADBException) {
			if (isLoggingError()) {
				logError("Exception while calling/Processing the logCouponInfoSOAPRequestResponse method ", pADBException);
			}

		} catch (XMLStreamException pXMLStreamException) {
			if (isLoggingError()) {
				logError("Exception while calling/Processing the logCouponInfoSOAPRequestResponse method ", pXMLStreamException);
			}
		}
	}

	/**
	 * This Method process the CouponVoid Response and create and populate CouponResponse Object.
	 * 
	 * @param pResponse
	 *            couponResponseBean
	 * @return couponResopnse
	 */
	private CouponResponse processCouponVoidResponse(CouponResponseBean pResponse) {

		if (isLoggingDebug()) {
			logDebug("Begin :: TRUSUCNCouponService ::  processCouponVoidResponse");
		}
		final CouponResponse couponResopnse = new CouponResponse();
		String couponNumber = null;
		int statusCode = 0;

		if (pResponse != null) {
			final Context_type1 contextType = pResponse.getContext();

			if (contextType != null) {
				couponNumber = contextType.getCouponNumber();
			}
			final Status_type0 statusType = pResponse.getStatus();
			if (statusType != null) {
				statusCode = statusType.getCode();
			}
			couponResopnse.setSUCcouponNumber(couponNumber);
			couponResopnse.setReturnCode(statusCode);
			if (isLoggingDebug()) {
				logDebug("Response Values..");
				logDebug("couponNumber :" + couponNumber);
				logDebug("statusCode :" + statusCode);
			}
		} else {
			if (isLoggingWarning()) {
				logWarning("Response is Null");
			}
		}
		if (isLoggingDebug()) {
			logDebug("End :: TRUSUCNCouponService ::  processCouponVoidResponse");
		}

		return couponResopnse;
	}

	/**
	 * Gets the TRUSUCN configuration.
	 *
	 * @return the tRUSUCNConfiguration
	 */
	public TRUSUCNConfiguration getTRUSUCNConfiguration() {
		return mTRUSUCNConfiguration;
	}

	/**
	 * Sets the TRUSUCN configuration.
	 *
	 * @param pTRUSUCNConfiguration            the tRUSUCNConfiguration to set
	 */
	public void setTRUSUCNConfiguration(TRUSUCNConfiguration pTRUSUCNConfiguration) {
		mTRUSUCNConfiguration = pTRUSUCNConfiguration;
	}

}
