package com.tru.integrations.sucn.coupon.response;

import java.util.List;

/**
 * This Class is to maintain the Coupon Response values. 
 * @author PA
 * @version 1.0
 * @author Professional Access. 
 */
public class CouponResponse {
	
	private String mSUCcouponNumber ;
	private int mReturnCode ;
	private List<String> mPreviousUsage ;
	private List<String> mTransactionDate ;
	private List<String> mBusinessDate ;
	
	/**
	 * @return the mSUCcouponNumber
	 */
	public String getSUCcouponNumber() {
		return mSUCcouponNumber;
	}
	/**
	 * @param pSUCcouponNumber the mSUCcouponNumber to set
	 */
	public void setSUCcouponNumber(String pSUCcouponNumber) {
		mSUCcouponNumber = pSUCcouponNumber;
	}
	/**
	 * @return the mReturnCode
	 */
	public int getReturnCode() {
		return mReturnCode;
	}
	/**
	 * @param pReturnCode the mReturnCode to set
	 */
	public void setReturnCode(int pReturnCode) {
		mReturnCode = pReturnCode;
	}
	/**
	 * @return the mPreviousUsage
	 */
	public List<String> getPreviousUsage() {
		return mPreviousUsage;
	}
	/**
	 * @param pPreviousUsage the mPreviousUsage to set
	 */
	public void setPreviousUsage(List<String> pPreviousUsage) {
		mPreviousUsage = pPreviousUsage;
	}
	/**
	 * @return the mTransactionDate
	 */
	public List<String> getTransactionDate() {
		return mTransactionDate;
	}
	/**
	 * @param pTransactionDate the mTransactionDate to set
	 */
	public void setTransactionDate(List<String> pTransactionDate) {
		mTransactionDate = pTransactionDate;
	}
	/**
	 * @return the mBusinessDate
	 */
	public List<String> getBusinessDate() {
		return mBusinessDate;
	}
	/**
	 * @param pBusinessDate the mBusinessDate to set
	 */
	public void setBusinessDate(List<String> pBusinessDate) {
		mBusinessDate = pBusinessDate;
	}
	

}
