package com.tru.integrations.sucn.common;

/**
 * This is the constants file for declaring SUCN Integration related Constants.
 * @author PA.
 * @version 1.0.
 */
public class TRUSUCNConstants {

	/** to hold the  Constant  Single Use Coupon Number. */
	public static final String SUCN_COUPON_PARAMETER_SINGLE_USE_COUPON_NUMBER = "singleUseCouponNumber";
	
	/**  to hold the Constant  store. */
	public static final String SUCN_COUPON_PARAMETER_STORE = "store";
	
	/**  to hold the Constant  register. */
	public static final String SUCN_COUPON_PARAMETER_REGISTER = "register";
	
	/**  to hold the Constant  transaction. */
	public static final String SUCN_COUPON_PARAMETER_TRANSACTION = "transaction";
	
	/**  to hold the Constant  orderNumber. */
	public static final String SUCN_COUPON_PARAMETER_ORDER_NUMBER = "orderNumber";
	
	/**  to hold the Constant  businessDateString. */
	public static final String SUCN_COUPON_PARAMETER_BUSINIESS_DATE_STRING = "businessDateString";
	
	/**  to hold the Constant  transactionDateString. */
	public static final String SUCN_COUPON_PARAMETER_TRANSACTION_DATE_TIME = "transactionDateTime";
	
	/**  to hold the Constant  sourceId. */
	public static final String SUCN_COUPON_PARAMETER_SOURCE_ID = "sourceId";
	
	/**  to hold the Constant  transactionId. */
	public static final String SUCN_COUPON_PARAMETER_TRANSACTION_ID = "transactionId";
	
	/**  to hold the Constant  T. */
	public static final String  SUCN_PARAMETER_T = "T";
	
	/**  to hold the Constant  voidStore. */
	public static final String SUCN_COUPON_PARAMETER_VOID_STORE = "voidStore";
	
	/**  to hold the Constant  voidRegister. */
	public static final String SUCN_COUPON_PARAMETER_VOID_REGISTER = "voidRegister";
	
	/**  to hold the Constant  voidTransaction. */
	public static final String SUCN_COUPON_PARAMETER_VOID_TRANSACTION = "voidTransaction";
	
	/**  to hold the Constant  voidBusinessDateString. */
	public static final String SUCN_COUPON_PARAMETER_VOID_BUSINIESS_DATE_STRING = "voidBusinessDateString";
	
	/**  to hold the Constant  voidUserId. */
	public static final String SUCN_COUPON_PARAMETER_VOID_USER__ID = "voidUserId";
	
	/**
	 * Constant to hold REMOTE_EXCEPTION_THROW.
	 */
	public static final String REMOTE_EXCEPTION_THROW= "Remote Exception thrown";
	
	/** to hold the  Constant  Blank. */
	public static final String SUCN_BLANK = "";
	
	/** to hold the  Constant  SUCN_SERVICE. */
	public static final String SUCN_SERVICE = "sucnService";

	/** The Constant SUCN_USE_COUPON. */
	public static final String SUCN_USE_COUPON = "SUCN_USE_COUPON";
	
	/** to hold the  Constant  COUPON_VALIDATION. */
	public static final String COUPON_VALIDATION = "couponValidation";

	/** The Constant CONSUME_COUPON. */
	public static final String CONSUME_COUPON = "consumeCoupon";

	/** The Constant SUCN_VOID_COUPON. */
	public static final String SUCN_VOID_COUPON = "SUCN_VOID_COUPON";

	/** The Constant SUCN_LOOKUP_COUPON. */
	public static final String SUCN_LOOKUP_COUPON = "SUCN_LOOKUP_COUPON";
	
	/** The Constant RESPONSE_CODE. */
	public static final String RESPONSE_CODE = "ResponseCode";
	
	/** Holds TIME_TAKEN */
	public static final String TIME_TAKEN = "TimeTaken";
	
	/** The Constant PAGE_NAME. */
	public static final String PAGE_NAME = "Page";
	
	/** Holds OPERATION_NAME */
	public static final String OPERATION_NAME = "operationName";
	
	/**  constant for : ERROR. */
	public static final String ERROR = "ERROR";
}
