package com.tru.integrations.sucn.common;

import atg.nucleus.GenericService;

/**
 *This is the Configuration Class for the SUCN Configuration.
 * @author PA.
 * @version 1.0.
 */
public class TRUSUCNConfiguration extends GenericService {
	
	private String mCouponLookupServiceURL;
	private String mCouponServiceURL ;
	private String 	mSucnTransactionDateFormat;
	private String 	mSucnTransactionTimeFormat;
	private String 	mSucnBusinessDateFormat;
	private String 	mSucnBusinessTimeFormat;
	private String 	mTransactionSource;
	private String 	mClientSequence;
	private String 	mSystemRouting;
	private String 	mContextData;
	private String 	mTransactionId;
	private String 	mStore;
	private String 	mRegister;
	private String 	mTransaction;
	private String 	mVoidTransactionId;
	private String 	mVoidTransactionSource;
	
	
	/**
	 * @return the couponLookupServiceURL
	 */
	public String getCouponLookupServiceURL() {
		return mCouponLookupServiceURL;
	}
	/**
	 * @param pCouponLookupServiceURL the couponLookupServiceURL to set
	 */
	public void setCouponLookupServiceURL(String pCouponLookupServiceURL) {
		mCouponLookupServiceURL = pCouponLookupServiceURL;
	}
	/**
	 * @return the couponServiceURL
	 */
	public String getCouponServiceURL() {
		return mCouponServiceURL;
	}
	/**
	 * @param pCouponServiceURL the couponServiceURL to set
	 */
	public void setCouponServiceURL(String pCouponServiceURL) {
		mCouponServiceURL = pCouponServiceURL;
	}
	/**
	 * @return the sucnTransactionDateFormat
	 */
	public String getSucnTransactionDateFormat() {
		return mSucnTransactionDateFormat;
	}
	/**
	 * @param pSucnTransactionDateFormat the sucnTransactionDateFormat to set
	 */
	public void setSucnTransactionDateFormat(String pSucnTransactionDateFormat) {
		mSucnTransactionDateFormat = pSucnTransactionDateFormat;
	}
	/**
	 * @return the sucnTransactionTimeFormat
	 */
	public String getSucnTransactionTimeFormat() {
		return mSucnTransactionTimeFormat;
	}
	/**
	 * @param pSucnTransactionTimeFormat the sucnTransactionTimeFormat to set
	 */
	public void setSucnTransactionTimeFormat(String pSucnTransactionTimeFormat) {
		mSucnTransactionTimeFormat = pSucnTransactionTimeFormat;
	}
	/**
	 * @return the sucnBusinessDateFormat
	 */
	public String getSucnBusinessDateFormat() {
		return mSucnBusinessDateFormat;
	}
	/**
	 * @param pSucnBusinessDateFormat the sucnBusinessDateFormat to set
	 */
	public void setSucnBusinessDateFormat(String pSucnBusinessDateFormat) {
		mSucnBusinessDateFormat = pSucnBusinessDateFormat;
	}
	/**
	 * @return the transactionSource
	 */
	public String getTransactionSource() {
		return mTransactionSource;
	}
	/**
	 * @param pTransactionSource the transactionSource to set
	 */
	public void setTransactionSource(String pTransactionSource) {
		mTransactionSource = pTransactionSource;
	}
	/**
	 * @return the sucnBusinessTimeFormat
	 */
	public String getSucnBusinessTimeFormat() {
		return mSucnBusinessTimeFormat;
	}
	/**
	 * @param pSucnBusinessTimeFormat the sucnBusinessTimeFormat to set
	 */
	public void setSucnBusinessTimeFormat(String pSucnBusinessTimeFormat) {
		mSucnBusinessTimeFormat = pSucnBusinessTimeFormat;
	}
	/**
	 * @return the clientSequence
	 */
	public String getClientSequence() {
		return mClientSequence;
	}
	/**
	 * @param pClientSequence the clientSequence to set
	 */
	public void setClientSequence(String pClientSequence) {
		mClientSequence = pClientSequence;
	}
	/**
	 * @return the systemRouting
	 */
	public String getSystemRouting() {
		return mSystemRouting;
	}
	/**
	 * @param pSystemRouting the systemRouting to set
	 */
	public void setSystemRouting(String pSystemRouting) {
		mSystemRouting = pSystemRouting;
	}
	/**
	 * @return the contextData
	 */
	public String getContextData() {
		return mContextData;
	}
	/**
	 * @param pContextData the contextData to set
	 */
	public void setContextData(String pContextData) {
		mContextData = pContextData;
	}
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}
	/**
	 * @param pTransactionId the transactionId to set
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}
	/**
	 * @return the store
	 */
	public String getStore() {
		return mStore;
	}
	/**
	 * @param pStore the store to set
	 */
	public void setStore(String pStore) {
		mStore = pStore;
	}
	/**
	 * @return the register
	 */
	public String getRegister() {
		return mRegister;
	}
	/**
	 * @param pRegister the register to set
	 */
	public void setRegister(String pRegister) {
		mRegister = pRegister;
	}
	/**
	 * @return the transaction
	 */
	public String getTransaction() {
		return mTransaction;
	}
	/**
	 * @param pTransaction the transaction to set
	 */
	public void setTransaction(String pTransaction) {
		mTransaction = pTransaction;
	}
	
	/**
	 * @return the voidTransactionId
	 */
	public String getVoidTransactionId() {
		return mVoidTransactionId;
	}
	/**
	 * @param pVoidTransactionId the voidTransactionId to set
	 */
	public void setVoidTransactionId(String pVoidTransactionId) {
		mVoidTransactionId = pVoidTransactionId;
	}
	
	/**
	 * @return the voidTransactionSource
	 */
	public String getVoidTransactionSource() {
		return mVoidTransactionSource;
	}
	/**
	 * @param pVoidTransactionSource the voidTransactionSource to set
	 */
	public void setVoidTransactionSource(String pVoidTransactionSource) {
		mVoidTransactionSource = pVoidTransactionSource;
	}
	

}
