package com.tru.integrations.sucn.common;

/**
 * This is the Exception Class used for SUCN Integration Exceptions.
 * @author PA.
 * @version 1.0.
 *
 */
public class SUCNIntegrationException  extends Exception {
	
	/**
	 * This method is overridden to hold all Integration related Exception.
	 * 
	 * @param pErrorMessage String 
	 */
	public SUCNIntegrationException(String pErrorMessage) {
		super(pErrorMessage);		
	}
	
	/**
	 * This method is overridden to hold all Integration related Exception.
	 * 
	 * @param pErrorMessage String
	 * @param pException Throwable
	 */
	public SUCNIntegrationException(String pErrorMessage, Throwable pException) {
		super(pErrorMessage, pException);		
	}	

}
