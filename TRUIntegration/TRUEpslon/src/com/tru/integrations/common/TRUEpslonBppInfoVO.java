package com.tru.integrations.common;

import java.io.Serializable;

/**
 * This class is used to set the BPP item properties
 * @version 1.0
 * @author Professional Access
 */
public class TRUEpslonBppInfoVO implements Serializable{
	
	/**
	* The Constant serialVersionUID. 
	*/
	private static final long serialVersionUID = -7688059964340876671L;
	/**
	 * property to hold  mBppSkuID.
	 */
	private String mBppSkuID;
	/**
	 * property to hold mBppItemName.
	 */
	private String mBppItemName;
	/**
	 * property to hold mBppItemPrice.
	 */
	private double mBppItemPrice;

	/**
	 * @return the bppSkuID
	 */
	public String getBppSkuID() {
		return mBppSkuID;
	}

	/**
	 * @param pBppSkuID the bppSkuID to set
	 */
	public void setBppSkuID(String pBppSkuID) {
		mBppSkuID = pBppSkuID;
	}

	/**
	 * @return the bppItemName
	 */
	public String getBppItemName() {
		return mBppItemName;
	}

	/**
	 * @param pBppItemName the bppItemName to set
	 */
	public void setBppItemName(String pBppItemName) {
		mBppItemName = pBppItemName;
	}

	/**
	 * @return the bppItemPrice
	 */
	public double getBppItemPrice() {
		return mBppItemPrice;
	}

	/**
	 * @param pBppItemPrice the bppItemPrice to set
	 */
	public void setBppItemPrice(double pBppItemPrice) {
		mBppItemPrice = pBppItemPrice;
	}

}
