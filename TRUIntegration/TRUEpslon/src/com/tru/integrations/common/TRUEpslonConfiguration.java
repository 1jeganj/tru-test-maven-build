package com.tru.integrations.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;

/**
 * This class holds the properties related to Epslon service.
 * 
 *@author Professional Access.
 *@version 1.0.
 *
 */
public class TRUEpslonConfiguration extends GenericService{
	
	/**
	 * Holds the property value of mEpsilonTransactionItemDescriptor.
	 */
	private String  mEpsilonTransactionItemDescriptor;
	
	/**
	 * Holds the property value of mTargetQueuePropertyName.
	 */
	private String  mTargetQueuePropertyName;
	
	/**
	 * Holds the property value of mTransactionIdPropertyName.
	 */
	private String  mEpsilonRequestPropertyName;
	
	/**
	 * Holds the property value of mTransactionIdPropertyName.
	 */
	private String  mEpsilonRequestTypePropertyName;
	
	/**
	 * Holds the property value of mTransactionIdPropertyName.
	 */
	private String  mEpsilonStatusPropertyName;
	
	/**
	 * Holds the property value of mTransactionIdPropertyName.
	 */
	private String  mTransactionTimePropertyName;
	
	/**
	 * Holds the property value of uRLProtocol.
	 */
	private String  mURLProtocol;
	/**
	 * Holds the property value of epslonShipMethodMap.
	 */
	private Map<String,String> mEpslonShipMethodMap;
	
	/**
	 * Holds the property value of mOrderSatusLinkType.
	 */
	private String mOrderSatusLinkType;
	
	/**
	 * Holds the property value of mOrderSatusLinkValue.
	 */
	private String mOrderSatusLinkValue;
	/**
	 * Holds the property value of mTimeZone.
	 */
	private String mTimeZoneFormat;
	/**
	 * Holds the property value of mESBServer.
	 */
	private String mESBServer;
	
	/**
	 * Holds the property value of mESBPort.
	 */
	private String mESBPort;
	
	/**
	 * Holds the property value of mESBParameter.
	 */
	private String mESBParameter;

	/**
	 * Holds the property value of useParameter.
	 */
	private boolean mUseParameter;
	
	/**
	 * Holds the property value of mESBProtocol.
	 */
	private String mESBProtocol;
	/**
	 * Holds the property value of mESBUser.
	 */
	private String mESBUser;
	/**
	 * Holds the property value of mESBPassword.
	 */
	private String mESBPassword;
	/**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 * Holds the property value of mEpslonEditPrefURL.
	 */
	private String mEpslonEditPrefURL;
	 /**
	 * Holds the property value of mEpslonEmailPropertyName.
     */
		private String mTRUEmailPropertyName;
	 /**
	 * Holds the property value of mTrueEmailOptionPropertyName.
	 */
	private String mTrueEmailOptionPropertyName;
	 /**
	 * Holds the property value of mSourcePropertyName.
	 */
	private String mSourcePropertyName;
	 /**
	 * Holds the property value of mMSgIdPropertyname.
	 */
	private String mMsgIdPropertyname;
	 /**
	 * Holds the property value of mTrueEmailOptionValue.
	 */
	private String mTrueEmailOptionValue;
	 /**
	 * Holds the property value of mSourceValue.
	 */
	private String mSourceValue;
	 /**
	 * Holds the property value of mMsgIdValue.
	 */
	private String mMsgIdValue;
	 /**
	 * Holds the property value of mInsertSuccessRecord.
	 */
	private boolean mInsertSuccessRecord;
	
	/**
	 * Checks if is insert success record.
	 *
	 * @return true, if is insert success record
	 */
	public boolean isInsertSuccessRecord() {
		return mInsertSuccessRecord;
	}

	/**
	 * Sets the insert success record.
	 *
	 * @param pInsertSuccessRecord the new insert success record
	 */
	public void setInsertSuccessRecord(boolean pInsertSuccessRecord) {
		mInsertSuccessRecord = pInsertSuccessRecord;
	}

	/**
	 * @return the mUseParameter
	 */
	public boolean isUseParameter() {
		return mUseParameter;
	}

	/**
	 * @param pUseParameter the mUseParameter to set
	 */
	public void setUseParameter(boolean pUseParameter) {
		mUseParameter = pUseParameter;
	}
	
	/**
	 * @return the epslonEditPrefURL
	 */
	public String getEpslonEditPrefURL() {
		return mEpslonEditPrefURL;
	}
	/**
	 * @param pEpslonEditPrefURL the epslonEditPrefURL to set
	 */
	public void setEpslonEditPrefURL(String pEpslonEditPrefURL) {
		mEpslonEditPrefURL = pEpslonEditPrefURL;
	}
	/**
	 * @return the trueEmailOptionPropertyName
	 */
	public String getTrueEmailOptionPropertyName() {
		return mTrueEmailOptionPropertyName;
	}
	/**
	 * @param pTrueEmailOptionPropertyName the trueEmailOptionPropertyName to set
	 */
	public void setTrueEmailOptionPropertyName(String pTrueEmailOptionPropertyName) {
		mTrueEmailOptionPropertyName = pTrueEmailOptionPropertyName;
	}
	/**
	 * @return the sourcePropertyName
	 */
	public String getSourcePropertyName() {
		return mSourcePropertyName;
	}
	/**
	 * @param pSourcePropertyName the sourcePropertyName to set
	 */
	public void setSourcePropertyName(String pSourcePropertyName) {
		mSourcePropertyName = pSourcePropertyName;
	}
	/**
	 * @return the msgIdPropertyname
	 */
	public String getMsgIdPropertyname() {
		return mMsgIdPropertyname;
	}
	/**
	 * @param pMsgIdPropertyname the msgIdPropertyname to set
	 */
	public void setMsgIdPropertyname(String pMsgIdPropertyname) {
		mMsgIdPropertyname = pMsgIdPropertyname;
	}
	/**
	 * @return the trueEmailOptionValue
	 */
	public String getTrueEmailOptionValue() {
		return mTrueEmailOptionValue;
	}
	/**
	 * @param pTrueEmailOptionValue the trueEmailOptionValue to set
	 */
	public void setTrueEmailOptionValue(String pTrueEmailOptionValue) {
		mTrueEmailOptionValue = pTrueEmailOptionValue;
	}
	/**
	 * @return the sourceValue
	 */
	public String getSourceValue() {
		return mSourceValue;
	}
	/**
	 * @param pSourceValue the sourceValue to set
	 */
	public void setSourceValue(String pSourceValue) {
		mSourceValue = pSourceValue;
	}
	/**
	 * @return the msgIdValue
	 */
	public String getMsgIdValue() {
		return mMsgIdValue;
	}
	/**
	 * @param pMsgIdValue the msgIdValue to set
	 */
	public void setMsgIdValue(String pMsgIdValue) {
		mMsgIdValue = pMsgIdValue;
	}
	/**
	 * @return the tRUEmailPropertyName
	 */
	public String getTRUEmailPropertyName() {
		return mTRUEmailPropertyName;
	}
	/**
	 * @param pTRUEmailPropertyName the tRUEmailPropertyName to set
	 */
	public void setTRUEmailPropertyName(String pTRUEmailPropertyName) {
		mTRUEmailPropertyName = pTRUEmailPropertyName;
	}
	
	/**
	 * @return the eSBServer
	 */
	public String getESBServer() {
		return mESBServer;
	}
	/**
	 * @param pESBServer the eSBServer to set
	 */
	public void setESBServer(String pESBServer) {
		mESBServer = pESBServer;
	}
	
	/**
	 * @return the eSBPort
	 */
	public String getESBPort() {
		return mESBPort;
	}
	/**
	 * @param pESBPort the eSBPort to set
	 */
	public void setESBPort(String pESBPort) {
		mESBPort = pESBPort;
	}

	
	
	/**
	 * @return the eSBParameter
	 */
	public String getESBParameter() {
		return mESBParameter;
	}
	/**
	 * @param pESBParameter the pESBParameter to set
	 */
	public void setESBParameter(String pESBParameter) {
		mESBParameter = pESBParameter;
	}
	/**
	 * @return the eSBProtocol
	 */
	public String getESBProtocol() {
		return mESBProtocol;
	}
	/**
	 * @param pESBProtocol the eSBProtocol to set
	 */
	public void setESBProtocol(String pESBProtocol) {
		mESBProtocol = pESBProtocol;
	}
	/**
	 * @return the eSBUser
	 */
	public String getESBUser() {
		return mESBUser;
	}
	/**
	 * @param pESBUser the eSBUser to set
	 */
	public void setESBUser(String pESBUser) {
		mESBUser = pESBUser;
	}
	/**
	 * @return the eSBPassword
	 */
	public String getESBPassword() {
		return mESBPassword;
	}
	/**
	 * @param pESBPassword the eSBPassword to set
	 */
	public void setESBPassword(String pESBPassword) {
		mESBPassword = pESBPassword;
	}
	/**
	 * @return the targetQueue
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}
	/**
	 * @param pTargetQueue the targetQueue to set
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	
	/**
	 * @return the timeZoneFormat
	 */
	public String getTimeZoneFormat() {
		return mTimeZoneFormat;
	}
	/**
	 * @param pTimeZoneFormat the timeZoneFormat to set
	 */
	public void setTimeZoneFormat(String pTimeZoneFormat) {
		mTimeZoneFormat = pTimeZoneFormat;
	}
	
	/**
	 * This method is used to get orderSatusLinkType.
	 * @return orderSatusLinkType String
	 */
	public String getOrderSatusLinkType() {
		return mOrderSatusLinkType;
	}
	/**
	 *This method is used to set orderSatusLinkType.
	 *@param pOrderSatusLinkType String
	 */
	public void setOrderSatusLinkType(String pOrderSatusLinkType) {
		mOrderSatusLinkType = pOrderSatusLinkType;
	}
	/**
	 * This method is used to get orderSatusLinkValue.
	 * @return orderSatusLinkValue String
	 */
	public String getOrderSatusLinkValue() {
		return mOrderSatusLinkValue;
	}
	/**
	 *This method is used to set orderSatusLinkValue.
	 *@param pOrderSatusLinkValue String
	 */
	public void setOrderSatusLinkValue(String pOrderSatusLinkValue) {
		mOrderSatusLinkValue = pOrderSatusLinkValue;
	}
	
	/**
	 * @return the epslonShipMethodMap
	 */
	public Map<String, String> getEpslonShipMethodMap() {
		return mEpslonShipMethodMap;
	}
	/**
	 * @param pEpslonShipMethodMap the epslonShipMethodMap to set
	 */
	public void setEpslonShipMethodMap(Map<String, String> pEpslonShipMethodMap) {
		mEpslonShipMethodMap = pEpslonShipMethodMap;
	}
	
	/**
	 * @return the uRLProtocol
	 */
	public String getURLProtocol() {
		return mURLProtocol;
	}
	/**
	 * @param pURLProtocol the uRLProtocol to set
	 */
	public void setURLProtocol(String pURLProtocol) {
		mURLProtocol = pURLProtocol;
	}
	
	/**
	 * @return the transactionIdPropertyName
	 */
	
	/**
	 * @return the epsilonRequestPropertyName
	 */
	public String getEpsilonRequestPropertyName() {
		return mEpsilonRequestPropertyName;
	}
	/**
	 * @param pEpsilonRequestPropertyName the epsilonRequestPropertyName to set
	 */
	public void setEpsilonRequestPropertyName(String pEpsilonRequestPropertyName) {
		mEpsilonRequestPropertyName = pEpsilonRequestPropertyName;
	}
	/**
	 * @return the epsilonRequestTypePropertyName
	 */
	public String getEpsilonRequestTypePropertyName() {
		return mEpsilonRequestTypePropertyName;
	}
	/**
	 * @param pEpsilonRequestTypePropertyName the epsilonRequestTypePropertyName to set
	 */
	public void setEpsilonRequestTypePropertyName(
			String pEpsilonRequestTypePropertyName) {
		mEpsilonRequestTypePropertyName = pEpsilonRequestTypePropertyName;
	}
	/**
	 * @return the epsilonStatusPropertyName
	 */
	public String getEpsilonStatusPropertyName() {
		return mEpsilonStatusPropertyName;
	}
	/**
	 * @param pEpsilonStatusPropertyName the epsilonStatusPropertyName to set
	 */
	public void setEpsilonStatusPropertyName(String pEpsilonStatusPropertyName) {
		mEpsilonStatusPropertyName = pEpsilonStatusPropertyName;
	}
	/**
	 * @return the transactionTimePropertyName
	 */
	public String getTransactionTimePropertyName() {
		return mTransactionTimePropertyName;
	}
	/**
	 * @param pTransactionTimePropertyName the transactionTimePropertyName to set
	 */
	public void setTransactionTimePropertyName(String pTransactionTimePropertyName) {
		mTransactionTimePropertyName = pTransactionTimePropertyName;
	}
	
	/**
	 * @return the epsilonTransactionItemDescriptor
	 */
	public String getEpsilonTransactionItemDescriptor() {
		return mEpsilonTransactionItemDescriptor;
	}
	/**
	 * @param pEpsilonTransactionItemDescriptor the epsilonTransactionItemDescriptor to set
	 */
	public void setEpsilonTransactionItemDescriptor(
			String pEpsilonTransactionItemDescriptor) {
		mEpsilonTransactionItemDescriptor = pEpsilonTransactionItemDescriptor;
	}
	/**
	 * @return the targetQueuePropertyName
	 */
	public String getTargetQueuePropertyName() {
		return mTargetQueuePropertyName;
	}
	/**
	 * @param pTargetQueuePropertyName the targetQueuePropertyName to set
	 */
	public void setTargetQueuePropertyName(String pTargetQueuePropertyName) {
		mTargetQueuePropertyName = pTargetQueuePropertyName;
	}
	/**
	 * This method sets current timestamp in required format. 
	 * @return dateString String
	 */
	
	public String getCurrentTimeStamp(){
		Date date = new Date(); 
		DateFormat formatter = new SimpleDateFormat(TRUEpslonConstants.DATE_FORMAT,Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone(TRUEpslonConstants.TIME_ZONE));
		String dateString = formatter.format(date);
		if(!StringUtils.isBlank(getTimeZoneFormat())){
		dateString = dateString.concat(TRUEpslonConstants.EMPTY_STRING).concat(getTimeZoneFormat());
		}
		return dateString;
  	}
	/**
	 * This method is used to get the current date in String format. 
	 * @return dateString String
	 */
	public String getCurrentDate(){
		Date date = new Date(); 
		DateFormat formatter = new SimpleDateFormat(TRUEpslonConstants.ONLY_DATE_FORMAT,Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone(TRUEpslonConstants.TIME_ZONE));
		String dateString = formatter.format(date);
		return dateString;
  	}
}
