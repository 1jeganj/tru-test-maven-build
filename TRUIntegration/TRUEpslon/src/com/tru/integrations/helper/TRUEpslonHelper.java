package com.tru.integrations.helper;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.transaction.TransactionManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.activemq.ActiveMQConnectionFactory;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.logging.TRUAlertLogger;

/**
 * This class holds the business logic related to Epslon Email service.
 * 
 * @author PA.
 * @version 1.0.
 */

public class TRUEpslonHelper extends GenericService {
	
	/**
	 * Holds reference for mTransactionManager. 
	 */
	
	private TransactionManager mTransactionManager ;
	
	/**
	 * Holds reference for MessageContentRepository. 
	 */
	private int mPostingCount;
	/**
	 * Holds reference for mAuditRepository. 
	 */
	private GSARepository mAuditRepository;
	/**
	 * Holds the property value of epslonConfiguration
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;

	/**
	 * Holds the property value of profileTools.
	 */
	private ProfileTools mProfileTools;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * This method is used to get epslonConfiguration.
	 * 
	 * @return mEpslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 * This method is used to set epslonConfiguration.
	 * 
	 * @param pEpslonConfiguration
	 *            TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(
			TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * This method is used to get profileTools.
	 * 
	 * @return profileTools ProfileTools
	 */
	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * This method is used to set profileTools.
	 * 
	 * @param pProfileTools
	 *            ProfileTools
	 */
	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	
	/**
	 * @return the auditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}

	/**
	 * @param pAuditRepository the auditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		mAuditRepository = pAuditRepository;
	}
	
	/**
	 * @return the postingCount
	 */
	public int getPostingCount() {
		return mPostingCount;
	}

	/**
	 * @param pPostingCount the postingCount to set
	 */
	public void setPostingCount(int pPostingCount) {
		mPostingCount = pPostingCount;
	}
	
	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/**
	 * This method is used to get user's firstName from his email by querying
	 * the profile repository.
	 * 
	 * @param pEmail
	 *            String
	 * @return firstName String
	 */
	public String getFirstName(String pEmail) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER:::TRUEpslonHelper getFirstName()");
		}
		String firstName = TRUEpslonConstants.EMPTY_STRING;
		if (!StringUtils.isBlank(pEmail)) {
			if (isLoggingDebug()) {
				logDebug("User Email Address::" + pEmail);
			}
			ProfileTools profileTools = getProfileTools();

			if (isLoggingDebug()) {
				logDebug("ProfileTools::" + profileTools);
			}
			if (profileTools != null) {
				RepositoryItem profile = profileTools.getItemFromEmail(pEmail);
				MutableRepositoryItem profileItem = (MutableRepositoryItem) profile;
				if (isLoggingDebug()) {
					logDebug("PROFILE ITEM::" + profileItem);
				}
				PropertyManager propertyManager = profileTools
						.getPropertyManager();
				if (isLoggingDebug()) {
					logDebug("PROPERTY MANAGER::" + propertyManager);
				}

				if (profileItem != null && propertyManager != null) {
					firstName = (String) profileItem
							.getPropertyValue(propertyManager
									.getFirstNamePropertyName());
					if (StringUtils.isBlank(firstName)) {
						if (isLoggingDebug()) {
							logDebug("FIRST NAME is Empty. So we are getting the first name from email");
						}
						firstName = getFirstNameFromEmail(pEmail);
					}
				} else {
					firstName = getFirstNameFromEmail(pEmail);
				}
			}
		} else {
			if (isLoggingDebug()) {
				vlogDebug("User Email Address is NULL");
			}
		}
		if (isLoggingDebug()) {
			logDebug("FIRST NAME::" + firstName);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUEpslonHelper getFirstName()");
		}
		return firstName;
	}

	/**
	 * This method is used to get user's firstName from his email by splitting
	 * his email address. if there is no first name set in the profile
	 * 
	 * @param pEmail
	 *            String
	 * @return firstName String
	 */

	public String getFirstNameFromEmail(String pEmail) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER:::TRUEpslonHelper getFirstnameFromEmail()");
		}
		String firstName = TRUEpslonConstants.EMPTY_STRING;

		if (!StringUtils.isBlank(pEmail)
				&& pEmail.contains(TRUEpslonConstants.AT)) {
			String[] parts = pEmail.split(TRUEpslonConstants.AT);
			firstName = parts[0];
		}
		if (isLoggingDebug()) {
			logDebug("FIRST NAME:::" + firstName);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUEpslonHelper getFirstnameFromEmail()");
		}
		return firstName;
	}

	/**
	 * This method is used to get user's lastName from his email by querying the
	 * profile repository.
	 * 
	 * @param pEmail
	 *            String
	 * @return lastName String
	 */
	public String getLastName(String pEmail) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER:::TRUEpslonHelper getLastName()");
		}
		String lastName = TRUEpslonConstants.EMPTY_STRING;
		if (!StringUtils.isBlank(pEmail)) {
			if (isLoggingDebug()) {
				logDebug("User Email Address::" + pEmail);
			}
			ProfileTools profileTools = getProfileTools();
			if (isLoggingDebug()) {
				logDebug("ProfileTools::" + profileTools);
			}
			if (profileTools != null) {
				RepositoryItem profile = profileTools.getItemFromEmail(pEmail);
				MutableRepositoryItem profileItem = (MutableRepositoryItem) profile;
				if (isLoggingDebug()) {
					logDebug("PROFILE ITEM :::" + profileItem);
				}
				PropertyManager propertyManager = profileTools
						.getPropertyManager();
				if (isLoggingDebug()) {
					logDebug("PROPERTY MANAGER::" + propertyManager);
				}
				if (profileItem != null && propertyManager != null) {
					lastName = (String) profileItem
							.getPropertyValue(propertyManager
									.getLastNamePropertyName());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("LAST NAME::" + lastName);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUEpslonHelper getLastName()");
		}
		return lastName;
	}

	/**
	 * This method is used get store open and close time in 12 hour format.
	 * 
	 * @param pStoreTime
	 *            String
	 * @return finalTime String
	 */
	public String getStoreHourTimeInProperFormat(String pStoreTime) {
		if (isLoggingDebug()) {
			logDebug("ENTER:::TRUEpslonHelper getStoreHourTimeInProperFormat()");
		}
		String finalTime = TRUEpslonConstants.EMPTY_STRING;
		if (!StringUtils.isBlank(pStoreTime)) {
			if (isLoggingDebug()) {
				logDebug("Input Time ::" + pStoreTime);
			}
			try {
				String twentyFourHourTime = pStoreTime;
				if (twentyFourHourTime.contains(TRUEpslonConstants.DOT)) {
					String time = twentyFourHourTime.substring(
							TRUEpslonConstants.ZERO,
							twentyFourHourTime.length()
									- TRUEpslonConstants.TWO);

					while (time.length() < TRUEpslonConstants.FOUR) {
						time = TRUEpslonConstants.ZERO_STRING.concat(time);
					}
					twentyFourHourTime = time.replaceAll(
							TRUEpslonConstants.REGEX_1,
							TRUEpslonConstants.REGEX_2);
				}
				SimpleDateFormat twentyFourHourSDF = new SimpleDateFormat(
						TRUEpslonConstants.TWENTY_FOUR_HOUR_FORMAT, Locale.US);
				SimpleDateFormat twelveHourSDF = new SimpleDateFormat(
						TRUEpslonConstants.TWELVE_HOUR_FORMAT, Locale.US);
				Date twentyFourHourDt = twentyFourHourSDF
						.parse(twentyFourHourTime);
				finalTime = twelveHourSDF.format(twentyFourHourDt);

			} catch (ParseException e) {
				if (isLoggingError()) {
					logError(
							"ParseException in TRUEpslonHelper getStoreHourTimeInProperFormat() : ",
							e);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUEpslonHelper getStoreHourTimeInProperFormat()");
		}
		return finalTime;
	}

	/**
	 * This method converts the response to XML format.
	 * 
	 * @param pObj
	 *            Object
	 * @param pPackageName
	 *            String
	 * @return xmlRequest String
	 */
	public String generateXML(Object pObj, String pPackageName) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonHelper generateXML()");
		}
		String xmlRequest = TRUEpslonConstants.EMPTY_STRING;
		if (pObj != null) {
			StringWriter sw = new StringWriter();
			try {
				JAXBContext context = JAXBContext.newInstance(pPackageName);
				Marshaller marshaller = context.createMarshaller();
				marshaller.setProperty(TRUEpslonConstants.JAXB_PACKAGE_NAME,
						Boolean.TRUE);
				marshaller.marshal(pObj, sw);
				xmlRequest = sw.toString();
				if (isLoggingDebug()) {
					logDebug("TRUEpslonHelper xml Request--- ");
					logDebug(xmlRequest);

				}
				sw.close();
			} catch (JAXBException e) {
				if (isLoggingError()) {
					logError(
							"JAXBException in TRUEpslonHelper generateXML() : ",
							e);
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError("IOException in TRUEpslonHelper generateXML() : ",
							e);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("EXIT::::TRUEpslonHelper generateXML()");
			}
		}
		return xmlRequest;
	}

	/**
	 * This method is used to send the message to the Queue
	 * 
	 * @param pRequestXml
	 *            String
	 * @param pTargetQueue
	 *            String
	 */

	public void postMessageToQueue(String pRequestXml,String pTargetQueue) {
		
	   if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonHelper .postMessageToQueue()");
		}
	   if(!StringUtils.isBlank(pRequestXml)){
	   Connection connection = null;
		try {
		String connectionURL= getEpslonConfiguration().getESBProtocol().concat(TRUEpslonConstants.COLON).concat(TRUEpslonConstants.DSLASH).concat(getEpslonConfiguration().getESBServer()).concat(TRUEpslonConstants.COLON).concat(getEpslonConfiguration().getESBPort());
		if(getEpslonConfiguration().isUseParameter()){
			connectionURL = connectionURL + getEpslonConfiguration().getESBParameter() + TRUEpslonConstants.EQUAL + TRUEpslonConstants.FALSE;
		}
		if (isLoggingDebug()) {
				logDebug("ENTER::::TRUEpslonHelper .postMessageToQueue() " + connectionURL);
			}
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(getEpslonConfiguration().getESBUser(),getEpslonConfiguration().getESBPassword(),connectionURL);
		connection = factory.createConnection();
/*		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(connectionURL);
		connection = factory.createConnection(getEpslonConfiguration().getESBUser(),getEpslonConfiguration().getESBPassword());*/
		connection.start();
		QueueSession session = ((QueueConnection) connection).createQueueSession(false,javax.jms.Session.AUTO_ACKNOWLEDGE);
		Queue queue = (Queue) session.createQueue(pTargetQueue);
		QueueSender sender = session.createSender((javax.jms.Queue) queue);
		TextMessage msg = session.createTextMessage();
		msg.setText(pRequestXml);
		sender.send(msg);
		} catch (JMSException e) {
			if (isLoggingError()) {
				logError("JMSException in TRUEpslonHelper postMessageToQueue() : ", e);
				}
		}finally{
			if(connection != null){
				try {
					connection.stop();
					connection.close();
					if (isLoggingDebug()) {
						vlogDebug("Closing the Connection");
					}
				} catch (JMSException e) {
					if (isLoggingError()) {
						logError(
								"JMSException in TRUEpslonHelper postMessageToQueue() : ",
								e);
					}
				} finally {
					connection = null;
				}
	       }
	   }
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUEpslonHelper postMessageToQueue()");
		}
	}
  }
/**
	 * This method is used to store the failed message items in repository
	 * 
	 * @param pRequestXml
	 *            String
	 * @param pEpsilonMailType
	 *            String
	 * @param pTargetQueue
	 *            String
	 */
public void createEpsilonFailedItem(String pRequestXml,String pEpsilonMailType,String pTargetQueue){
	final TransactionManager tm = getTransactionManager();
	
	TransactionDemarcation td = new TransactionDemarcation();
	boolean lRollbackFlag = false;
	MutableRepository auditRepository = getAuditRepository();
	try {
		td.begin(tm, TransactionDemarcation.REQUIRED);
		
		Date time = Calendar.getInstance().getTime();
		MutableRepositoryItem transDetailsItem = auditRepository.createItem(getEpslonConfiguration().getEpsilonTransactionItemDescriptor());
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getTargetQueuePropertyName(),pTargetQueue);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonRequestPropertyName(), pRequestXml);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonRequestTypePropertyName(), pEpsilonMailType);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonStatusPropertyName(), TRUEpslonConstants.FAILURE);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getTransactionTimePropertyName(), time);
		auditRepository.addItem(transDetailsItem);
	}
	catch(Exception e1){
		if (isLoggingError()) {
			vlogError("Exception while adding the epsilon repository failure Item::",e1);
		}
		lRollbackFlag=true;
	}finally{
		if (tm != null) {
			try {
				td.end(lRollbackFlag);
			} catch (TransactionDemarcationException tD) {
				vlogError("TransactionDemarcationException occured while adding the epsilon repository failure Item ", tD);
				
			}
		}
	}
}

/**
 * This method is used to store the success message items in repository
 * 
 * @param pRequestXml 
 *            String
 * @param pEpsilonMailType
 *            String
 * @param pTargetQueue
 *            String
 */
public void createEpsilonSuccessItem(String pRequestXml,String pEpsilonMailType,String pTargetQueue){
	final TransactionManager tm = getTransactionManager();

	TransactionDemarcation td = new TransactionDemarcation();
	boolean lRollbackFlag = false;
	MutableRepository auditRepository = getAuditRepository();
	try {
		td.begin(tm, TransactionDemarcation.REQUIRED);

		Date time = Calendar.getInstance().getTime();
		MutableRepositoryItem transDetailsItem = auditRepository.createItem(getEpslonConfiguration().getEpsilonTransactionItemDescriptor());
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getTargetQueuePropertyName(),pTargetQueue);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonRequestPropertyName(), pRequestXml);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonRequestTypePropertyName(), pEpsilonMailType);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getEpsilonStatusPropertyName(), TRUEpslonConstants.SUCCESS);
		transDetailsItem.setPropertyValue(getEpslonConfiguration().getTransactionTimePropertyName(), time);
		auditRepository.addItem(transDetailsItem);
	}
	catch(Exception e1){
		if (isLoggingError()) {
			vlogError("Exception while adding the epsilon repository success Item::",e1);
		}
		lRollbackFlag=true;
	}finally{
		if (tm != null) {
			try {
				td.end(lRollbackFlag);
			} catch (TransactionDemarcationException tD) {
				vlogError("TransactionDemarcationException occured while updating the epsilon repository success ", tD);

			}
		}
	}
}



/**
 * This method returns old audit items based on the configured days.
 * 
 * @return auditMessageItems - Repository items
 */
public RepositoryItem[] getAuditMessages() {
	if (isLoggingDebug()) {
		logDebug("Enter into [Class: TRUEpslonHelper  method: getAuditMessages]");
		
	}
	
	MutableRepository auditRepository = getAuditRepository();
	RepositoryItem[] auditMessageItems= null;
	try {
		RepositoryView autMessageView = auditRepository.getView(getEpslonConfiguration().getEpsilonTransactionItemDescriptor());
		QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
		
		QueryExpression epsilonFlagPropertyExp=queryBuilder.createPropertyQueryExpression(getEpslonConfiguration().getEpsilonStatusPropertyName());
        QueryExpression epsilonFlagConstant=queryBuilder.createConstantQueryExpression(TRUEpslonConstants.FAILURE);
        
        Query transactionTimeQuery=queryBuilder.createComparisonQuery(epsilonFlagPropertyExp, epsilonFlagConstant, QueryBuilder.EQUALS);
        
        auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
	} catch (RepositoryException exc) {
		if(isLoggingError()){
			vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
		}
	}
	if (isLoggingDebug()) {
		logDebug("Exit from [Class: TRUEpslonHelper  method: purgeOmsAuditMessages]");
	}
	return auditMessageItems;
}


/**
 * This method will get the epsilon audit messages
 * reposts the same
 * @throws RepositoryException - if any
 */
public void repostAuditMessages() throws RepositoryException {
	if (isLoggingDebug()) {
		logDebug("Enter into [Class: TRUOMSAuditTools  method: purgeOmsAuditMessages]");

	}
	/*MutableRepository auditRepository = getAuditRepository();
	//TRUOMSConfiguration configuration = null; 
			//getOmsConfiguration();
	RepositoryItem[] auditMessages = getAuditMessages(pDurationToDeleteMessagesInDays);
	if(auditMessages != null && auditMessages.length > TRUConstants.INT_ZERO){
		for(RepositoryItem auditMessage : auditMessages){
			auditRepository.removeItem(auditMessage.getRepositoryId(), "itemDescriptorName");
		}
	}*/
	if (isLoggingDebug()) {
		logDebug("Exit from [Class: TRUOMSAuditTools  method: purgeOmsAuditMessages]");
	}
 }


/**
 * @param pRequestXml RequestXml
 * @param pTargetQueue TargetQueue
 * @param pIsUpdated isUpdated
 * @param pItemId ItemId
 * @param pEpsilonEmailType EpsilonEmailType
 */
public void postMessageToQueue1(String pRequestXml, String pTargetQueue,boolean pIsUpdated,String pItemId,String pEpsilonEmailType) {

	if (isLoggingDebug()) {
		vlogDebug("ENTER::::TRUEpslonHelper .postMessageToQueue1()");
	}
	
	int postCount = TRUEpslonConstants.ZERO;
	boolean postingStatus = Boolean.FALSE;
	
	if (!StringUtils.isBlank(pRequestXml)) {
		Connection connection = null;
		while((!postingStatus) && (postCount < TRUEpslonConstants.THREE)){
		try {
			postingStatus = Boolean.TRUE;
			String connectionURL = getEpslonConfiguration()
					.getESBProtocol().concat(TRUEpslonConstants.COLON)
					.concat(TRUEpslonConstants.DSLASH)
					.concat(getEpslonConfiguration().getESBServer())
					.concat(TRUEpslonConstants.COLON)
					.concat(getEpslonConfiguration().getESBPort());
			if(getEpslonConfiguration().isUseParameter()){
				connectionURL = connectionURL + getEpslonConfiguration().getESBParameter() + TRUEpslonConstants.EQUAL + TRUEpslonConstants.FALSE;
			}
			if (isLoggingDebug()) {
					logDebug("ENTER::::TRUEpslonHelper .postMessageToQueue() " + connectionURL);
				}
		
			ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
					getEpslonConfiguration().getESBUser(),
					getEpslonConfiguration().getESBPassword(),
					connectionURL);
			connection = factory.createConnection();
			connection.start();
			QueueSession session = ((QueueConnection) connection)
					.createQueueSession(false,
							javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue) session.createQueue(pTargetQueue);
			QueueSender sender = session
					.createSender((javax.jms.Queue) queue);
			TextMessage msg = session.createTextMessage();
			msg.setText(pRequestXml);
			sender.send(msg);
			if(getEpslonConfiguration().isInsertSuccessRecord()){
				createEpsilonSuccessItem(pRequestXml,pEpsilonEmailType,pTargetQueue);
			}
		} catch (JMSException e) {
			if (isLoggingError()) {
				logError(
						"JMSException in TRUEpslonHelper postMessageToQueue1() : ",
						e);
			}
			postingStatus = Boolean.FALSE;
			postCount=postCount+TRUEpslonConstants.ONE;
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			extra.put(TRUEpslonConstants.TARGETQUEUE, pTargetQueue);	
			extra.put(TRUEpslonConstants.ERROR, e.getMessage());	
			//extra.put(TRUEpslonConstants.RESPONSE_CODE, e.getErrorCode());
			getAlertLogger().logFailure(TRUEpslonConstants.EPSILON, pEpsilonEmailType, extra);
			if (isLoggingDebug()) {
				logDebug("Alert Logging Completed MapValue ::: " + extra );
			}
		}
		finally {
			
			if (isLoggingDebug()) {
				vlogDebug("Entered into finally block");
			}
			if (connection != null) {
				try {
					connection.stop();
					connection.close();
					if (isLoggingDebug()) {
						vlogDebug("Closing the Connection");
					}
				} catch (JMSException e) {
					if (isLoggingError()) {
						logError(
								"JMSException in TRUEpslonHelper postMessageToQueue1() : ",
								e);
					}
				} 
			}
		}
		if((!postingStatus) && (postCount >= TRUEpslonConstants.THREE) && (!pIsUpdated)){
			vlogDebug("Updating to Repository");
			createEpsilonFailedItem(pRequestXml,pEpsilonEmailType,pTargetQueue);
			pIsUpdated = Boolean.TRUE;
		}
		if(postingStatus && pIsUpdated){
			vlogDebug("Removing from  Repository:",pItemId);
			updateEpsilonItem(pItemId);
		}
		}
	  }
	
	if (isLoggingDebug()) {
		vlogDebug("EXIT:::TRUEpslonHelper postMessageToQueue()");
	}
}
    /**
	 * This method is used to remove the message item from repository after successfull resend
	 * @param pItemId String
     */

public void updateEpsilonItem(String pItemId){

	final TransactionManager tm = getTransactionManager();

	TransactionDemarcation td = new TransactionDemarcation();
	boolean lRollbackFlag = false;
	try{	
		td.begin(tm, TransactionDemarcation.REQUIRED);

		RepositoryItem[] failedEpsilonMessageItems = getAuditMessages();
		if(failedEpsilonMessageItems != null){
			for(RepositoryItem messageItem :failedEpsilonMessageItems){
				MutableRepositoryItem messageMutItem = (MutableRepositoryItem)messageItem;
				String itemId = (String)messageMutItem.getRepositoryId();
				if(itemId.equals(pItemId)){
					MutableRepository auditRepository = getAuditRepository();
					messageMutItem.setPropertyValue(getEpslonConfiguration().getEpsilonStatusPropertyName(),TRUEpslonConstants.SUCCESS);
					try {
						auditRepository.updateItem(messageMutItem);
					} catch (RepositoryException e) {
						if(isLoggingError()){
							logError("RepositoryException in TRUEpslonHelper removeEpsilonItem() : ",e);
						}
					}					
				}

			}

		}
	}
	catch(TransactionDemarcationException e1){
		if (isLoggingError()) {
			vlogError("Exception while updating the epsilon repository success Item::",e1);
		}
		lRollbackFlag=true;
	}
	finally{
		if (tm != null) {
			try {
				td.end(lRollbackFlag);
			} catch (TransactionDemarcationException tD) {
				vlogError("TransactionDemarcationException occured while updating the epsilon repository success ", tD);

			}
		}
	}
}


/**
 * This method will get the oms audit messages of old configured in days and
 * deletes the same
 * 
 * 
 * @param pDurationToDeleteMessagesInDays
 *            - Duration in days
 * @throws RepositoryException - if any
 */
public void purgeEpslonAuditMessages(int pDurationToDeleteMessagesInDays){
	if (isLoggingDebug()) {
		logDebug("Enter into [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
		vlogDebug("pDurationToDeleteMessagesInDays : {0}",pDurationToDeleteMessagesInDays);
	}
	final TransactionManager tm = getTransactionManager();

	TransactionDemarcation td = new TransactionDemarcation();
	boolean lRollbackFlag = false;
	try{	
		td.begin(tm, TransactionDemarcation.REQUIRED);
		MutableRepository auditRepository = getAuditRepository();
		RepositoryItem[] auditMessages = getAuditMessages(pDurationToDeleteMessagesInDays);
		if(auditMessages != null && auditMessages.length > TRUEpslonConstants.ZERO){
			for(RepositoryItem auditMessage : auditMessages){
				try{
					auditRepository.removeItem(auditMessage.getRepositoryId(), getEpslonConfiguration().getEpsilonTransactionItemDescriptor());
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError("RepositoryException in TRUEpslonHelper removeEpsilonItem() : ",e);
					}
				}

			}
		}
	}
	catch(TransactionDemarcationException e1){
		if (isLoggingError()) {
			vlogError("Exception while updating the epsilon repository success Item::",e1);
		}
		lRollbackFlag=true;
	}
	finally{
		if (tm != null) {
			try {
				td.end(lRollbackFlag);
			} catch (TransactionDemarcationException tD) {
				vlogError("TransactionDemarcationException occured while updating the epsilon repository success ", tD);

			}
		}
	}
	if (isLoggingDebug()) {
		logDebug("Exit from [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
	}
}

/**
 * This method returns old audit items based on the configured days.
 * 
 * @param pDurationToDeleteMessagesInDays
 *            - Duration in days
 * @return auditMessageItems - Repository items
 */
private RepositoryItem[] getAuditMessages(int pDurationToDeleteMessagesInDays) {
	if (isLoggingDebug()) {
		logDebug("Enter into [Class: TRURadialOMTools  method: getAuditMessages]");
		vlogDebug("pDurationToDeleteMessagesInDays : {0}", pDurationToDeleteMessagesInDays);
	}
	MutableRepository auditRepository = getAuditRepository();
	RepositoryItem[] auditMessageItems= null;
	try {
		RepositoryView autMessageView = auditRepository.getView(getEpslonConfiguration().getEpsilonTransactionItemDescriptor());
		QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, - pDurationToDeleteMessagesInDays);
		Date oldDate = cal.getTime();
		QueryExpression transactionTimePropertyExp=queryBuilder.createPropertyQueryExpression(getEpslonConfiguration().getTransactionTimePropertyName());
        QueryExpression oldDateConstantExp=queryBuilder.createConstantQueryExpression(oldDate);
        
        Query transactionTimeQuery=queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.LESS_THAN);
        
        auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
	} catch (RepositoryException exc) {
		if(isLoggingError()){
			vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
		}
	}
	if (isLoggingDebug()) {
		logDebug("Exit from [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
	}
	return auditMessageItems;
}

}

