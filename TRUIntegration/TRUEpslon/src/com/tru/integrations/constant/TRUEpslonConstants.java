package com.tru.integrations.constant;



/**
 * This class hold all epslon related constants.
 * @author PA
 * @version 1.0
 */
public class TRUEpslonConstants {

	/**
	 * Holds the property value of DATE_FORMAT.
	 */
	public static final String DATE_FORMAT = "MM/dd/yy hh:mm";
	
	/**
	 * Holds the property value of ONLY_DATE_FORMAT.
	 */
	public static final String ONLY_DATE_FORMAT = "MM/dd/yy";
	
	/**
	 * Holds the property value of mResetPasswordURL.
	 */
	public static final String TIME_ZONE = "America/New_York";
	
	 /**
	 * Holds the property value of REGEX_1.
	 */  
	 public static final String REGEX_1= "..(?!$)";
	 
	 /**
	 * Holds the property value of REGEX_2.
	 */  
	 public static final String REGEX_2= "$0:";
 
	/**
	 * Holds the property value of TWELVE_HOUR_FORMAT.
	 */
	public static final String TWELVE_HOUR_FORMAT = "hh:mm a";

	/**
	 * Holds the property value of TWENTY_FOURHOUR_FORMAT.
	 */
	public static final String TWENTY_FOUR_HOUR_FORMAT = "HH:mm";
	
	/**
	 * Holds the property value of ZERO_STRING.
	 */
	public static final String ZERO_STRING = "0";
	
	/**
	 * Holds the property value of ZERO.
	 */
	public static final int ZERO = 0;
	
	/**
	 * Holds the property value of TWO.
	 */
	public static final int TWO = 2;
	
	/**
	 * Holds the property value of FOUR.
	 */
	public static final int FOUR = 4;
	
	/**
	 * Holds the property value of THIRTY ONE.
	 */
	public static final int THIRTY_ONE = 31;
	
	/**
	 * Holds the property value of COLON.
	 */
	public static final String COLON = ":";
	
	/**
	 * Holds the property value of DOT.
	 */
	public static final String DOT = ".";
	 
	/**
	 * Holds the property value of MSG_BODY.
	 */  
	 public static final String MSG_BODY= "Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum lo";
	 
	 /**
	 * Holds the property value of EMAIL_FRIEND_PACKAGE.
	 */  
	 public static final String EMAIL_FRIEND_PACKAGE= "com.tru.integrations.epslon.emailafriend";
		 
     /**
	 * Holds the property value of JAXB_PACKAGE_NAME.
	 */  
	 public static final String JAXB_PACKAGE_NAME = "jaxb.formatted.output";
		 
     /**
	 * Holds the property value of DSLASH.
	 */  
	 public static final String DSLASH = "//";
	 /**
	 * Holds the property value of ACKNOWLEDGEMENT.
	 */  
	 public static final String ACKNOWLEDGEMENT= "acknowledgment";
	 
	 /**
	 * Holds the property value of BACKORDER.
	 */  
	 public static final String BACKORDER= "backOrder";
	 /**
	 * Holds the property value of NOTIFY_ME_PACKAGE_NAME.
	 */  
	 public static final String NOTIFY_ME_PACKAGE_NAME= "com.tru.integrations.epslon.notify";
	 
	 /**
		 * Holds the property value of STORE_DEFAULT_DAY.
		 */  
		 public static final String STORE_DEFAULT_DAY= "SUNDAY";
		
		/**
		 * Holds the property value of AT.
		 */  
		 public static final String AT= "@";
		
		/**
		 * Holds the property value of EMPTY_STRING.
		 */  
		 public static final String EMPTY_STRING= "";
		 
		 /**
		 * Holds the property value of HARDGOOD_SHIPP_CODE.
		 */  
		 public static final String HARDGOOD_SHIPP_CODE= "H";
		 /**
		 * Holds the property value of SHIP_TO_STORE_CODE.
		 */  
		 public static final String SHIP_TO_STORE_CODE= "S";
		 
	     /**
		 * Holds the property value of INSTORE_SHIP_CODE.
		 */  
		 public static final String INSTORE_SHIP_CODE= "I";
		 
		 /**
		 * Holds the property value of USER.
		 */  
		 public static final String USER= "user";
		 
		 /**
		 * Holds the property value of COUNTRY.
		 */  
		 public static final String COUNTRY= "US";
		 
		 /**
		 * Holds the property value of ONE.
		 */  
		 public static final int ONE= 1;
		 /**
		 * Holds the property value of SIX.
		 */  
		 public static final int SIX= 6;
		 
		 /**
		 * Holds the property value of TEST_STORE_NUMBER.
		 */  
		 public static final int TEST_STORE_NUMBER= 1001;
		 
		 /**
		 * Holds the property value of D_ZERO.
		 */  
		 public static final double D_ZERO= 0.00;
		 
		 /**
		 * Holds the property value of DEFAULT_OPEN_TIME.
		 */  
		 public static final double DEFAULT_OPEN_TIME= 10.00;
		 
		 /**
		 * Holds the property value of DEFAULT_CLOSE_TIME.
		 */  
		 public static final double DEFAULT_CLOSE_TIME= 22.00;
		 
		 /**
		 * Holds the property value of ITEM_DESC.
		 */  
		 public static final String ITEM_DESC= "Power Rangers Super Megaforce 2inchi Figure 6 set";
		 /**
		 * Holds the property value of ORDER_ACK_PACKAGE_NAME.
		 */  
		 public static final String ORDER_ACK_PACKAGE_NAME= "com.tru.integrations.epslon.order";
		
		/**
		 * Holds the property value of TEST_VALUE.
		 */  
		 public static final String TEST_VALUE = "Test";
		 
		  /**
		 * Holds the property value of TEST_FAX.
		 */  
		 public static final String TEST_FAX = "61273789898";
		 
		 /**
		 * Holds the property value of TEST_PHONE.
		 */  
		 public static final String TEST_PHONE = "1234567890";

		 /**
		 * Holds the property value of PASSWORD_RESET_PACKAGE.
		 */  
		 public static final String PASSWORD_RESET_PACKAGE= "com.tru.integrations.epslon.reset";
		 
		 /**
		 * Holds the property value of EMAIL_UPDATE_PACKAGE.
		 */  
		 public static final String EMAIL_UPDATE_PACKAGE= "com.tru.integrations.epslon.updateemail";
		
		 /**
		 * Holds the property value of CREATE_ACCOUNT_PACKAGE.
		 */  
		 public static final String CREATE_ACCOUNT_PACKAGE= "com.tru.integrations.epslon";
		 
		 /**
		 * Holds the property value of EMAIL.
		 */  
		public static final String EMAIL= "email";
		/**
		 * Holds the property value of STATUS.
		 */
		public static final String STATUS= "status";
		/**
		 * Holds the property value of SUCCESS.
		 */
		public static final String SUCCESS= "success";
		/**
		 * Holds the property value of FAILURE.
		 */
		public static final String FAILURE= "failure";
		/**
		 * Holds the property value of TWO_HUNDRED.
		 */
		public static final int TWO_HUNDRED = 200;
		
		/**
		 * Holds the property value of OUTPUT.
		 */  
		public static final String OUTPUT= "output";
		/**
		 * Holds the property value of BPP_ITEM_DEFAULT_NAME.
		 */  
		public static final String BPP_ITEM_DEFAULT_NAME= "buyer-protection-plan";
		/**  
		 * constant to hold char pattern. 
		*/
		public static final String CHAR_PATTERN ="[^a-zA-Z0-9 ]";
		
		/**
		 * Holds the property value of SPACE_STRING.
		 */  
		public static final String SPACE_STRING= " ";
			 
		/**
		 * Holds the property value of OPEN_BRACES.
		 */  
		public static final String OPEN_BRACES = "(" ;
		
		/**
		 * Holds the property value of CLOSED_BRACES.
		 */  
		public static final String CLOSED_BRACES = ")" ;
		
		/**
		 * Holds the property value of RESET_PASSWORD_EMAIL_TYPE.
		 */  
		public static final String RESET_PASSWORD_EMAIL_TYPE = "ResetPassword" ;

		/**
		 * Holds the property value of RESET_PASSWORD_EMAIL_TYPE.
		 */  
		public static final String ORDER_ACK_EMAIL_TYPE = "OrderAcknowledge" ;
		
		/**
		 * Holds the property value of ACCOUNT_CREATION_EMAIL_TYPE.
		 */  
		public static final String ACCOUNT_CREATION_EMAIL_TYPE = "AccountCreation" ;

		/**
		 * Holds the property value of EMAILFRIEND_EMAIL_TYPE.
		 */  
		public static final String EMAILFRIEND_EMAIL_TYPE = "EmailAFriend" ;
		
		/**
		 * Holds the property value of CHANGEMAIL_EMAIL_TYPE.
		 */  
		public static final String CHANGEMAIL_EMAIL_TYPE = "ChangeMail" ;

		/**
		 * Holds the property value of CHANGEPWD_EMAIL_TYPE.
		 */  
		public static final String CHANGEPWD_EMAIL_TYPE = "ChangePassword" ;
		
		/**
		 * Holds the property value of NOTIFYME_EMAIL_TYPE.
		 */  
		public static final String NOTIFYME_EMAIL_TYPE = "NotifyMe" ;
		
		/**
		 * Holds the property value of three
		 */
		public static final int THREE = 3;
		
		/**
		 * Holds the property value of PRISLANDTAX.
		 */  
		public static final String PRISLANDTAX = "PR Island Tax";	
		 
		/**
		 * Holds the property value of PRLOCALTAX.
		 */  
		public static final String PRLOCALTAX = "PR Local Tax";	
		 
		/**
		 * Holds the property value of EWASTE.
		 */  
		 public static final String EWASTE = "e-waste fee";
		 
		 /**
		 * Holds the property value of UNDERSCORE.
		 */  
		 public static final String UNDERSCORE = "_";
		 
		 /**
		 * Holds the property value of IS_LAND_TAX.
		 */  
		 public static final String IS_LAND_TAX = "islandTax";
		 
		 /**
		 * Holds the property value of LOCAL_TAX.
		 */  
		 public static final String LOCAL_TAX = "localTax";
		 
		 /**
		 * Holds the property value of EQUAL.
		 */  
		 public static final String EQUAL = "=";
		 
		 /**
		 * Holds the property value of False.
		 */  
		 public static final String FALSE = "false";
		 
		 /**
		  * Holds the property value of BPP_ITEM_DELIVERY.
		  */  
		 public static final String BPP_ITEM_DELIVERY= "Email";
		 
		 /** Constant to hold BILLINGFIRSTNAME. */
		 public static final String  BILLINGFIRSTNAME = "BillingFirstName";
		
		 /** Constant to hold BILLINGLASTNAME. */
		 public static final String  BILLINGLASTNAME = "BillingLastName";
		 
		 /** Constant to hold TARGETQUEUE. */
		 public static final String  TARGETQUEUE = "TARGETQUEUE";
		 
		 /** Constant to hold Exception. */
		 public static final String  EXCEPTION = "Exception";
		 
		 /** Constant to hold Epsilon. */
		 public static final String  EPSILON = "Epsilon";
		 
		 /** Constant to hold Error. */
		 public static final String  ERROR = "Error";
		
		 /** Constant to hold Response code. */
		 public static final String  RESPONSE_CODE = "ResponseCode";
		 
}