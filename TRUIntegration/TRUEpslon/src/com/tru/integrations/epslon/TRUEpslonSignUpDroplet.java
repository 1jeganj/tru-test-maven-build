package com.tru.integrations.epslon;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;

/**
 * This class holds the business logic for sending the sign up email to user.
 * @author Professional Access
 * @version 1.0
 */

public class TRUEpslonSignUpDroplet extends DynamoServlet{

	/**
	 *  property to hold mEnableProxy
	*/
	private boolean mEnableProxy;
	/**
	 * Holds the property value of mTRUEpslonMessageBean.
	 */
	TRUEpslonConfiguration mEpslonConfiguration;

	/**
	 * variable to hold mLocalProxyHost - String.
	 */
	private String mLocalProxyHost;
	
	/**
	 * variable to hold mLocalProxyPort - String.
	 */
	private int mLocalProxyPort;
	
	/**
	 * @return the enableProxy
	 */
	public boolean isEnableProxy() {
		return mEnableProxy;
	}

	/**
	 * @param pEnableProxy the enableProxy to set
	 */
	public void setEnableProxy(boolean pEnableProxy) {
		mEnableProxy = pEnableProxy;
	}

	/**
	 * This method is used to get epslonConfiguration.
	 * @return mEpslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}
	/**
	 * This method is used to get localProxyHost.
	 * @return mLocalProxyHost String
	 */
	public String getLocalProxyHost() {
		return mLocalProxyHost;
	}
	/**
	 *This method is used to set localProxyHost.
	 *@param pLocalProxyHost String
	 */
	public void setLocalProxyHost(String pLocalProxyHost) {
		mLocalProxyHost = pLocalProxyHost;
	}

	/**
	 * This method is used to get localProxyPort.
	 * @return mLocalProxyPort int
	 */
	public int getLocalProxyPort() {
		return mLocalProxyPort;
	}
	/**
	 *This method is used to set localProxyPort.
	 *@param pLocalProxyPort int
	 */
	public void setLocalProxyPort(int pLocalProxyPort) {
		mLocalProxyPort = pLocalProxyPort;
	}

	/**
	 * service() method is used to send the mail to user.
	 * 
	 * @param pRequest - DynamoHttpServletRequest
	 * @param pResponse - DynamoHttpServletResponse
	 * @throws ServletException - ServletException
	 * @throws IOException - IOException 
	 * @throws ServletException 
	 */

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		    throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("TRUEpslonSignUpDroplet Service() : STARTS");
		}
		 String emailId = (String) pRequest.getLocalParameter(TRUEpslonConstants.EMAIL);
		 if(getEpslonConfiguration() != null){
			 
			String lUrl = getEpslonConfiguration().getEpslonEditPrefURL();
			
			 HttpClient client = new HttpClient();
			 if(isEnableProxy()){
				 if (isLoggingDebug()) {
						logDebug("Proxy is enabled and setting local proxy host and port");
						}
			 client.getHostConfiguration().setProxy(getLocalProxyHost(),getLocalProxyPort());
			 }
			 GetMethod method = new GetMethod(lUrl);
			 NameValuePair param1= new NameValuePair(getEpslonConfiguration().getTRUEmailPropertyName(), emailId);
             NameValuePair param2= new NameValuePair(getEpslonConfiguration().getTrueEmailOptionPropertyName(), getEpslonConfiguration().getTrueEmailOptionValue());
             NameValuePair param3= new NameValuePair(getEpslonConfiguration().getSourcePropertyName(), getEpslonConfiguration().getSourceValue());
             NameValuePair param4= new NameValuePair(getEpslonConfiguration().getMsgIdPropertyname(), getEpslonConfiguration().getMsgIdValue());
             NameValuePair[] params = new NameValuePair[]{param1,param2,param3,param4};
             method.setQueryString(params);
            
			try {
				if (isLoggingDebug()) {
				vlogDebug("Request URI:: {0}" , method.getURI());
				}
				int statusCode = client.executeMethod(method);
				if (isLoggingDebug()) {
				vlogDebug("Status Code:: {0}" , statusCode);
				}
				if (statusCode == TRUEpslonConstants.TWO_HUNDRED) {
					pRequest.setParameter(TRUEpslonConstants.STATUS, TRUEpslonConstants.SUCCESS);
					pRequest.serviceLocalParameter(TRUEpslonConstants.OUTPUT, pRequest,pResponse);

				} else {
					pRequest.setParameter(TRUEpslonConstants.STATUS, TRUEpslonConstants.FAILURE);
					pRequest.serviceLocalParameter(TRUEpslonConstants.OUTPUT, pRequest,pResponse);
				}
			} catch (HttpException httpException) {
				if (isLoggingError()) {
					logError("HttpException in TRUEpslonSignUpDroplet", httpException);
					}

			} finally {
				method.releaseConnection();
			}
			
		 }
		 if (isLoggingDebug()) {
				logDebug("TRUEpslonSignUpDroplet Service() : ENDS");
			}
	}
	
}





