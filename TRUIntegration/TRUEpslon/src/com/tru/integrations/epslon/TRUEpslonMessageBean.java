package com.tru.integrations.epslon;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.tru.integrations.common.TRUEpslonBppInfoVO;

/**
 * This class holds the properties for the Epslon msg service.
 * @author Professional Access.
 * @version 1.0.
 */

public class TRUEpslonMessageBean implements Serializable{

	/**
	 * Holds the property value of mShippingDeliveryTimeMap.
	 */
	private Map<String,String> mShippingDeliveryTimeMap;
	
	/**
	 * Holds the property value of mItemEwasteFeeMap.
	 */
	private Map<String,Double> mItemEwasteFeeMap;
	
	/**
	 * Holds the property value of mCustomerLoggedIn.
	 */
	private boolean mCustomerLoggedIn;
	
	/**
	 * Holds the property value of mMessageBeanBppItemMap.
	 */
	private Map<String,TRUEpslonBppInfoVO> mMessageBeanBppItemMap;
	
	/**
	 * Holds the property value of mRecieverFirstname.
	 */
	private String mRecieverFirstname;
	/**
	 * Holds the property value of mOldEmailAddress.
	 */
	private String mOldEmailAddress;
	/**
	 * Holds the property value of mUpdatedEmailAddress.
	 */
	private String mUpdatedEmailAddress;
	/**
	 * Holds the property value of mNotifyEmailFlag.
	 */
	private String mNotifyEmailFlag;
	/**
	 * Holds the property value of mHelpURLName.
	 */
	private String mHelpURLName;
	/**
	 * Holds the property value of mHelpURValue.
	 */
	private String mHelpURValue;
	
	/**
	 * Holds the property value of mOrderSatusLinkType.
	 */
	private String mOrderSatusLinkType;
	
	/**
	 * Holds the property value of mOrderSatusLinkValue.
	 */
	private String mOrderSatusLinkValue;
	
	/**
	 * Holds the property value of mItemName.
	 */
	private String mItemName;
	
	/**
	 * Holds the property value of mOrderID.
	 */
	private String mOrderID;
	/**
	 * Holds the property value of mSenderFirstName.
	 */
	private String mSenderFirstName;
	/**
	 * Holds the property value of mSenderLastName.
	 */
	private String mSenderLastName;
	
	/**
	 * Holds the property value of itemURL.
	 */
	private String mItemURL;
	/**
	 * Holds the property value of itemNumber.
	 */
	private String mItemNumber;
	/**
	 * Holds the property value of itemDescription.
	 */
	private String mItemDescription;
	
	/**
	 * Holds the property value of serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mMyAccountURL.
	 */
	private String mMyAccountURL;
	
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mResetPasswordURL;
	 
	/**
	 * Holds the property value of mPrIslandTax.
	 */
	private BigDecimal mPrIslandTax;
	
	/**
	 * Holds the property value of mPrLocalTax.
	 */
	private BigDecimal mPrLocalTax;
	
	/**
	 * This method is used to get email.
	 * @return mEmail String
	 */
	public String getEmail() {
		return mEmail;
	}
	/**
	 *This method is used to set email.
	 *@param pEmail String
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	/**
	 * This method is used to get epslonSource.
	 * @return mEpslonSource String
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}
	/**
	 *This method is used to set epslonSource.
	 *@param pEpslonSource String
	 */
	public void setEpslonSource(String pEpslonSource) {
		mEpslonSource = pEpslonSource;
	}
	/**
	 * This method is used to get epslonReferenceID.
	 * @return mEpslonReferenceID String
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}
	/**
	 *This method is used to set epslonReferenceID.
	 *@param pEpslonReferenceID String
	 */
	public void setEpslonReferenceID(String pEpslonReferenceID) {
		mEpslonReferenceID = pEpslonReferenceID;
	}
	/**
	 * This method is used to get epslonMessageType.
	 * @return mEpslonMessageType String
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}
	/**
	 *This method is used to set epslonMessageType.
	 *@param pEpslonMessageType String
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}
	/**
	 * This method is used to get epslonBrand.
	 * @return mEpslonBrand String
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}
	/**
	 *This method is used to set epslonBrand.
	 *@param pEpslonBrand String
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}
	/**
	 * This method is used to get epslonCompanyID.
	 * @return mEpslonCompanyID String
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}
	/**
	 *This method is used to set epslonCompanyID.
	 *@param pEpslonCompanyID String
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}
	/**
	 * This method is used to get epslonHeaderTypeMsgLocale.
	 * @return mEpslonHeaderTypeMsgLocale String
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}
	/**
	 *This method is used to set epslonHeaderTypeMsgLocale.
	 *@param pEpslonHeaderTypeMsgLocale String
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}
	/**
	 * This method is used to get epslonHeaderTypeMsgTimeZone.
	 * @return mEpslonHeaderTypeMsgTimeZone String
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}
	/**
	 *This method is used to set epslonHeaderTypeMsgTimeZone.
	 *@param pEpslonHeaderTypeMsgTimeZone String
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}
	/**
	 * This method is used to get epslonHeaderTypeDestination.
	 * @return mEpslonHeaderTypeDestination String
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}
	/**
	 *This method is used to set epslonHeaderTypeDestination.
	 *@param pEpslonHeaderTypeDestination String
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}
	/**
	 * This method is used to get epslonHeaderTypeInternalDateTimeStamp.
	 * @return mEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}
	/**
	 *This method is used to set epslonHeaderTypeInternalDateTimeStamp.
	 *@param pEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}
	/**
	 * This method is used to get myAccountURL.
	 * @return mMyAccountURL String
	 */
	public String getMyAccountURL() {
		return mMyAccountURL;
	}
	/**
	 *This method is used to set myAccountURL.
	 *@param pMyAccountURL String
	 */
	public void setMyAccountURL(String pMyAccountURL) {
		mMyAccountURL = pMyAccountURL;
	}
	/**
	 * This method is used to get resetPasswordURL.
	 * @return mResetPasswordURL String
	 */
	public String getResetPasswordURL() {
		return mResetPasswordURL;
	}
	/**
	 *This method is used to set resetPasswordURL.
	 *@param pResetPasswordURL String
	 */
	public void setResetPasswordURL(String pResetPasswordURL) {
		mResetPasswordURL = pResetPasswordURL;
	}
	/**
	 * This method is used to get itemURL.
	 * @return itemURL String
	 */
	public String getItemURL() {
		return mItemURL;
	}
	/**
	 *This method is used to set itemURL.
	 *@param pItemURL String
	 */
	public void setItemURL(String pItemURL) {
		mItemURL = pItemURL;
	}
	/**
	 * This method is used to get itemNumber.
	 * @return itemNumber String
	 */
	public String getItemNumber() {
		return mItemNumber;
	}
	/**
	 *This method is used to set itemNumber.
	 *@param pItemNumber String
	 */
	public void setItemNumber(String pItemNumber) {
		mItemNumber = pItemNumber;
	}
	/**
	 * This method is used to get itemDescription.
	 * @return itemDescription String
	 */
	public String getItemDescription() {
		return mItemDescription;
	}
	/**
	 *This method is used to set itemDescription.
	 *@param pItemDescription String
	 */
	public void setItemDescription(String pItemDescription) {
		mItemDescription = pItemDescription;
	}
	/**
	 * This method is used to get senderFirstName.
	 * @return senderFirstName String
	 */
	public String getSenderFirstName() {
		return mSenderFirstName;
	}
	/**
	 *This method is used to set senderFirstName.
	 *@param pSenderFirstName String
	 */
	public void setSenderFirstName(String pSenderFirstName) {
		mSenderFirstName = pSenderFirstName;
	}
	/**
	 * This method is used to get senderLastName.
	 * @return senderLastName String
	 */
	public String getSenderLastName() {
		return mSenderLastName;
	}
	/**
	 *This method is used to set senderLastName.
	 *@param pSenderLastName String
	 */
	public void setSenderLastName(String pSenderLastName) {
		mSenderLastName = pSenderLastName;
	}
	/**
	 * This method is used to get orderID.
	 * @return orderID String
	 */
	public String getOrderID() {
		return mOrderID;
	}
	/**
	 *This method is used to set orderID.
	 *@param pOrderID String
	 */
	public void setOrderID(String pOrderID) {
		mOrderID = pOrderID;
	}
	/**
	 * This method is used to get itemName.
	 * @return itemName String
	 */
	public String getItemName() {
		return mItemName;
	}
	/**
	 *This method is used to set itemName.
	 *@param pItemName String
	 */
	public void setItemName(String pItemName) {
		mItemName = pItemName;
	}
	/**
	 * This method is used to get orderSatusLinkType.
	 * @return orderSatusLinkType String
	 */
	public String getOrderSatusLinkType() {
		return mOrderSatusLinkType;
	}
	/**
	 *This method is used to set orderSatusLinkType.
	 *@param pOrderSatusLinkType String
	 */
	public void setOrderSatusLinkType(String pOrderSatusLinkType) {
		mOrderSatusLinkType = pOrderSatusLinkType;
	}
	/**
	 * This method is used to get orderSatusLinkValue.
	 * @return orderSatusLinkValue String
	 */
	public String getOrderSatusLinkValue() {
		return mOrderSatusLinkValue;
	}
	/**
	 *This method is used to set orderSatusLinkValue.
	 *@param pOrderSatusLinkValue String
	 */
	public void setOrderSatusLinkValue(String pOrderSatusLinkValue) {
		mOrderSatusLinkValue = pOrderSatusLinkValue;
	}
	/**
	 * This method is used to get helpURLName.
	 * @return helpURLName String
	 */
	public String getHelpURLName() {
		return mHelpURLName;
	}
	/**
	 *This method is used to set helpURLName.
	 *@param pHelpURLName String
	 */
	public void setHelpURLName(String pHelpURLName) {
		mHelpURLName = pHelpURLName;
	}
	/**
	 * This method is used to get helpURValue.
	 * @return helpURValue String
	 */
	public String getHelpURValue() {
		return mHelpURValue;
	}
	/**
	 *This method is used to set helpURValue.
	 *@param pHelpURValue String
	 */
	public void setHelpURValue(String pHelpURValue) {
		mHelpURValue = pHelpURValue;
	}
	/**
	 * @return the notifyEmailFlag
	 */
	public String getNotifyEmailFlag() {
		return mNotifyEmailFlag;
	}
	/**
	 * @param pNotifyEmailFlag the notifyEmailFlag to set
	 */
	public void setNotifyEmailFlag(String pNotifyEmailFlag) {
		mNotifyEmailFlag = pNotifyEmailFlag;
	}
	/**
	 * @return the oldEmailAddress
	 */
	public String getOldEmailAddress() {
		return mOldEmailAddress;
	}
	/**
	 *This method is used to set helpURValue.
	 *@param pOldEmailAddress String
	 */
	public void setOldEmailAddress(String pOldEmailAddress) {
		mOldEmailAddress = pOldEmailAddress;
	}
	/**
	 * @return the updatedEmailAddress
	 */
	public String getUpdatedEmailAddress() {
		return mUpdatedEmailAddress;
	}
	/**
	 *This method is used to set updatedEmailAddress.
	 *@param pUpdatedEmailAddress String
	 */
	public void setUpdatedEmailAddress(String pUpdatedEmailAddress) {
		mUpdatedEmailAddress = pUpdatedEmailAddress;
	}
	/**
	 * @return the recieverFirstname
	 */
	public String getRecieverFirstname() {
		return mRecieverFirstname;
	}
	/**
	 *This method is used to set recieverFirstname.
	 *@param pRecieverFirstname String
	 */
	public void setRecieverFirstname(String pRecieverFirstname) {
		mRecieverFirstname = pRecieverFirstname;
	}
	/**
	 * @return the messageBeanBppItemMap
	 */
	public Map<String, TRUEpslonBppInfoVO> getMessageBeanBppItemMap() {
		return mMessageBeanBppItemMap;
	}
	/**
	 * @param pMessageBeanBppItemMap the messageBeanBppItemMap to set
	 */
	public void setMessageBeanBppItemMap(
			Map<String, TRUEpslonBppInfoVO> pMessageBeanBppItemMap) {
		mMessageBeanBppItemMap = pMessageBeanBppItemMap;
	}
	/**
	 * @return the customerLoggedIn
	 */
	public boolean isCustomerLoggedIn() {
		return mCustomerLoggedIn;
	}
	/**
	 * @param pCustomerLoggedIn the customerLoggedIn to set
	 */
	public void setCustomerLoggedIn(boolean pCustomerLoggedIn) {
		mCustomerLoggedIn = pCustomerLoggedIn;
	}
	/**
	 * This method is used to get shippingDeliveryTimeMap.
	 * @return shippingDeliveryTimeMap String
	 */
	public Map<String, String> getShippingDeliveryTimeMap() {
		return mShippingDeliveryTimeMap;
	}
	/**
	 *This method is used to set shippingDeliveryTimeMap.
	 *@param pShippingDeliveryTimeMap String
	 */
	public void setShippingDeliveryTimeMap(
			Map<String, String> pShippingDeliveryTimeMap) {
		mShippingDeliveryTimeMap = pShippingDeliveryTimeMap;
	}
	
	/**
	 * This method is used to get Item Ewaste Fee.
	 * @return shippingDeliveryTimeMap String
	 */
	public Map<String, Double> getItemEwasteFeeMap() {
		return mItemEwasteFeeMap;
	}
	/**
	 *This method is used to set item Ewaste Fee.
	 *@param pItemEwasteFeeMap String
	 */
	public void setItemEwasteFeeMap(
			Map<String, Double> pItemEwasteFeeMap) {
		mItemEwasteFeeMap = pItemEwasteFeeMap;
	}

	/**
	 * This method is used to get PR Island Tax details.
	 * @return prIslandTax BigDecimal
	 */
	public BigDecimal getPrIslandTax() {
		return mPrIslandTax;
	}
	/**
	 *This method is used to set prIslandTax.
	 *@param pPrIslandTax BigDecimal
	 */
	public void setPrIslandTax(
			BigDecimal pPrIslandTax) {
		mPrIslandTax = pPrIslandTax;
	}
	
	/**
	 * This method is used to get PR Local Tax details.
	 * @return prLocalTax BigDecimal
	 */
	public BigDecimal getPrLocalTax() {
		return mPrLocalTax;
	}
	/**
	 *This method is used to set prIslandTax.
	 *@param pPrLocalTax BigDecimal
	 */
	public void setPrLocalTax(
			BigDecimal pPrLocalTax) {
		mPrLocalTax = pPrLocalTax;
	}

	
}
