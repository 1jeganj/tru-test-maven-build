package com.tru.integrations.epslon.notify;


import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.notify.NMWA.Message;
import com.tru.integrations.epslon.notify.NMWA.Message.Acknowledgement;
import com.tru.integrations.epslon.notify.NMWA.Message.Acknowledgement.HelpURL;
import com.tru.integrations.epslon.notify.NMWA.Message.BackOrder;
import com.tru.integrations.epslon.notify.NMWA.Message.BackOrder.ItemURL;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Email service.
 * @author Professional Access
 * @version 1.0
 */

public class TRUEpsilonNotifyMeEmailService extends GenericService{
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */
	private boolean mEnablePostMessageQueue;
	
	 /**
		 * Holds the property value of mTargetQueue.
		 */
	private String mTargetQueue;
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private TRUEpslonHelper mEpslonHelper;
	/**
	 * Holds the property value of mEpslonConfiguration.
	 */
	
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	
	/**
	 * @return the enablePostMessageQueue
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}
	/**
	 * @param pEnablePostMessageQueue the enablePostMessageQueue to set
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}
	
	/**
	 * @return the targetQueue
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}
	/**
	 * @param pTargetQueue the targetQueue to set
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	
	/**
	 * This method is used to get epslonConfiguration.
	 * @return mEpslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 *This method is used to set epslonHelper.
	 *@param pEpslonHelper TRUEpslonHelper
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}

	/**
	 * This method sets the message header .
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpsilonNotifyMeEmailService getMessageHeader()");
		}
		
		ObjectFactory factory = new ObjectFactory();
        HeaderType header = factory.createHeaderType();
		
        if(pEpslonMessageBean != null){
		if(pEpslonMessageBean.getEpslonSource() != null){
		header.setSource(pEpslonMessageBean.getEpslonSource());
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
		header.setReferenceID(pEpslonMessageBean.getEpslonReferenceID());
		}
		if(pEpslonMessageBean.getEpslonMessageType() != null){
		header.setMessageType(pEpslonMessageBean.getEpslonMessageType());
		}
		if(pEpslonMessageBean.getEpslonBrand() != null){
		header.setBrand(pEpslonMessageBean.getEpslonBrand());
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
		header.setCompanyID(pEpslonMessageBean.getEpslonCompanyID());
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone()));
		}
		if(getEpslonConfiguration() != null){
		header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
		header.setDestination(factory.createHeaderTypeDestination(pEpslonMessageBean.getEpslonHeaderTypeDestination()));
		}
		if(pEpslonMessageBean.getEmail() != null){
		header.setRecepientEmailAddress(pEpslonMessageBean.getEmail());
		}
        }
		else{
        	if (isLoggingDebug()) {
    			logDebug("TRUEpslonMessageBean is NULL:"+pEpslonMessageBean);
    		}
        }
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpsilonNotifyMeEmailService getMessageHeader()");
		}
		return header;
	}
	
	/**
	 * This method sets the message template and all other parameters.
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 */
	
	public void generateEmailNotificationRequest(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpsilonNotifyMeEmailService generateEmailNotificationRequest()");
		}
		String notifyMeRequestXML = TRUEpslonConstants.EMPTY_STRING;
		String notifyMeTargetQueue = TRUEpslonConstants.EMPTY_STRING;
		
		if (isLoggingDebug()) {
			logDebug("TRUEpsilonNotifyMeEmailService pEpslonMessageBean--> " + pEpslonMessageBean);
		}
		if(pEpslonMessageBean != null){
		ObjectFactory objectFactory = new ObjectFactory();
		NMWA nmwa  = objectFactory.createNMWA();
		HeaderType headerType= getMessageHeader(pEpslonMessageBean);
		
		Message msg = objectFactory.createNMWAMessage();
		if( !StringUtils.isBlank(pEpslonMessageBean.getNotifyEmailFlag()) && pEpslonMessageBean.getNotifyEmailFlag().equals(TRUEpslonConstants.ACKNOWLEDGEMENT)){
		Acknowledgement acknowledgement = getAcknowledgement(pEpslonMessageBean);	
		msg.setAcknowledgement(acknowledgement);
		}
		if(!StringUtils.isBlank(pEpslonMessageBean.getNotifyEmailFlag()) && pEpslonMessageBean.getNotifyEmailFlag().equals(TRUEpslonConstants.BACKORDER)){
		BackOrder backOrder = getBackOrder(pEpslonMessageBean);
		msg.setBackOrder(backOrder);
		}
		nmwa.setHeader(headerType);
		nmwa.setMessage(msg);
		
		String notifyMePackage = TRUEpslonConstants.NOTIFY_ME_PACKAGE_NAME;
		if(getEpslonHelper() != null){
		notifyMeRequestXML = getEpslonHelper().generateXML(nmwa, notifyMePackage);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Notify Me Request XML ---> ");
			logDebug(notifyMeRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		notifyMeTargetQueue = getTargetQueue();
		}
		if (isEnablePostMessageQueue() && !StringUtils.isBlank(notifyMeTargetQueue) && getEpslonHelper() != null ) {
			boolean isUpdated = false;
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.NOTIFYME_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(notifyMeRequestXML,notifyMeTargetQueue,isUpdated,itemId,epsilonEmailType);
		}

		}
		else{
			if (isLoggingDebug()) {
				vlogDebug("MessageBean is null in TRUEpsilonNotifyMeEmailService :generateEmailNotificationRequest()");
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpsilonNotifyMeEmailService generateEmailNotificationRequest()");
		}
	}
	
	
	
	/**
	 * This method is used to set the Acknowledgement object. 
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return backOrder BackOrder
	 */
	
	public Acknowledgement getAcknowledgement(TRUEpslonMessageBean pEpslonMessageBean) {
		if (isLoggingDebug()) {
			logDebug("ENTER::::TRUEpsilonNotifyMeEmailService getAcknowledgement()");
		}
		ObjectFactory objectFactory = new ObjectFactory();
        Acknowledgement acknowledgement = objectFactory.createNMWAMessageAcknowledgement();
        String firstName = TRUEpslonConstants.EMPTY_STRING;
        String lastName = TRUEpslonConstants.EMPTY_STRING;
        String itemDescription = TRUEpslonConstants.EMPTY_STRING;
        String itemNumber = TRUEpslonConstants.EMPTY_STRING;
        String itemName = TRUEpslonConstants.EMPTY_STRING;
        String itemURLValue = TRUEpslonConstants.EMPTY_STRING;
        String helpURLValue = TRUEpslonConstants.EMPTY_STRING;
        String helpURLName = TRUEpslonConstants.EMPTY_STRING;
        String date = TRUEpslonConstants.EMPTY_STRING;
       if(getEpslonHelper() != null && !StringUtils.isBlank(pEpslonMessageBean.getEmail())){
    	  String firstNameFormEmail=getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail());
    	  if(!StringUtils.isBlank(firstNameFormEmail)){
    		  firstName=firstNameFormEmail;
    	  }
    	  String lastNameFromEmail=getEpslonHelper().getLastName(pEpslonMessageBean.getEmail());
    	  if(!StringUtils.isBlank(lastNameFromEmail)){
    		  lastName=lastNameFromEmail;
    	  }
       }
       if(!StringUtils.isBlank(pEpslonMessageBean.getItemDescription())){
			itemDescription=pEpslonMessageBean.getItemDescription();	
		}
       if(!StringUtils.isBlank(pEpslonMessageBean.getItemNumber())){
    	   itemNumber=pEpslonMessageBean.getItemNumber();	
		}
      
       if(!StringUtils.isBlank(pEpslonMessageBean.getItemURL())){
       	itemURLValue=pEpslonMessageBean.getItemURL();	
 		}
       
       if(!StringUtils.isBlank(pEpslonMessageBean.getItemName())){
       	itemName=pEpslonMessageBean.getItemName();	
 		}
       
       if(!StringUtils.isBlank(pEpslonMessageBean.getHelpURValue())){
    	   helpURLValue= pEpslonMessageBean.getHelpURValue();	
		}
       if(!StringUtils.isBlank(pEpslonMessageBean.getHelpURLName())){
    	   helpURLName=pEpslonMessageBean.getHelpURLName();	
		}
		
        date = getEpslonConfiguration().getCurrentDate();
        acknowledgement.setFirstName(firstName);
		acknowledgement.setLastName(lastName);
		acknowledgement.setItemDescription(itemDescription);
		acknowledgement.setItemNumber(itemNumber);
		acknowledgement.setNotificationRequestDT(date);
		
		com.tru.integrations.epslon.notify.NMWA.Message.Acknowledgement.ItemURL itemURL = objectFactory.createNMWAMessageAcknowledgementItemURL();
		
		itemURL.setName(itemName);
		itemURL.setURL(itemURLValue);
		acknowledgement.setItemURL(itemURL);
		
		HelpURL helpURL = objectFactory.createNMWAMessageAcknowledgementHelpURL();
		helpURL.setName(helpURLName);
		helpURL.setURL(helpURLValue);
		acknowledgement.setHelpURL(helpURL);
		
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpsilonNotifyMeEmailService getAcknowledgement()");
		}
		return acknowledgement;
	}
	/**
	 * This method is used to set the BackOrder object. 
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return backOrder BackOrder
	 */
	public BackOrder getBackOrder(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			logDebug("ENTER::::TRUEpsilonNotifyMeEmailService getBackOrder()");
		}
		ObjectFactory objectFactory = new ObjectFactory();
		BackOrder backOrder = objectFactory.createNMWAMessageBackOrder();

		String firstName = TRUEpslonConstants.EMPTY_STRING;
        String lastName = TRUEpslonConstants.EMPTY_STRING;
        String itemDescription = TRUEpslonConstants.EMPTY_STRING;
        String itemNumber = TRUEpslonConstants.EMPTY_STRING;
        String itemName = TRUEpslonConstants.EMPTY_STRING;
        String itemURLValue = TRUEpslonConstants.EMPTY_STRING;
        String helpURLValue = TRUEpslonConstants.EMPTY_STRING;
        String helpURLName = TRUEpslonConstants.EMPTY_STRING;
        
        if(getEpslonHelper() != null && !StringUtils.isBlank(pEpslonMessageBean.getEmail())){
      	  String firstNameFormEmail = getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail());
      	  if(!StringUtils.isBlank(firstNameFormEmail)){
      		  firstName=firstNameFormEmail;
      	  }
      	  String lastNameFromEmail = getEpslonHelper().getLastName(pEpslonMessageBean.getEmail());
      	  if(!StringUtils.isBlank(lastNameFromEmail)){
      		  lastName=lastNameFromEmail;
      	  }
         }
        if(!StringUtils.isBlank(pEpslonMessageBean.getItemDescription())){
 			itemDescription=pEpslonMessageBean.getItemDescription();	
 		}
        if(!StringUtils.isBlank(pEpslonMessageBean.getItemNumber())){
     	   itemNumber=pEpslonMessageBean.getItemNumber();	
 		}
        if(!StringUtils.isBlank(pEpslonMessageBean.getItemURL())){
        	itemURLValue=pEpslonMessageBean.getItemURL();	
  		}
        if(!StringUtils.isBlank(pEpslonMessageBean.getItemName())){
        	itemName=pEpslonMessageBean.getItemName();	
  		}
        
        if(!StringUtils.isBlank(pEpslonMessageBean.getHelpURValue())){
     	   helpURLValue= pEpslonMessageBean.getHelpURValue();	
 		}
        if(!StringUtils.isBlank(pEpslonMessageBean.getHelpURLName())){
     	   helpURLName=pEpslonMessageBean.getHelpURLName();	
 		}
        com.tru.integrations.epslon.notify.NMWA.Message.BackOrder.HelpURL helpURL = objectFactory.createNMWAMessageBackOrderHelpURL();
        
        helpURL.setName(helpURLName);
        helpURL.setURL(helpURLValue);
		backOrder.setFirstName(firstName);
		backOrder.setLastName(lastName);
		backOrder.setItemNumber(itemNumber);
		backOrder.setItemDescription(itemDescription);
		ItemURL itemURL = objectFactory.createNMWAMessageBackOrderItemURL();
		itemURL.setName(itemName);
		itemURL.setURL(itemURLValue);
		backOrder.setItemURL(itemURL);
		backOrder.setHelpURL(helpURL);
		if (isLoggingDebug()) {
			logDebug("EXIT::::TRUEpsilonNotifyMeEmailService getBackOrder");
		}
		return backOrder;
	}
}



