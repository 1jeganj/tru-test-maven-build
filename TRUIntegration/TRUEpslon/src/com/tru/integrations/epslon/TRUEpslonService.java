package com.tru.integrations.epslon;


import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.DetailType.EMCoBrandedAccountCreation;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Email service.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class TRUEpslonService extends GenericService{
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private TRUEpslonHelper mEpslonHelper;
	
	/**
	 * Holds the property value of mEpslonConfiguration.
	 */
	
	private TRUEpslonConfiguration mEpslonConfiguration;
	 /**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */

	private boolean mEnablePostMessageQueue;
	
	/**
	 * @return the enablePostMessageQueue.
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}

	/**
	 * @param pEnablePostMessageQueue the enablePostMessageQueue to set.
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}
	
	/**
	 * @return the targetQueue.
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}
	/**
	 * @param pTargetQueue the targetQueue to set.
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	
	/**
	 * This method is used to get epslonConfiguration.
	 * @return mEpslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 *This method is used to set epslonHelper.
	 *@param pEpslonHelper TRUEpslonHelper
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}

	/**
	 * This method sets the message header .
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonService getMessageHeader()");
		}
		
		ObjectFactory factory = new ObjectFactory();
        HeaderType header = factory.createHeaderType();
		
        if(pEpslonMessageBean != null){
		if(pEpslonMessageBean.getEpslonSource() != null){
		header.setSource(pEpslonMessageBean.getEpslonSource());
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
		header.setReferenceID(pEpslonMessageBean.getEpslonReferenceID());
		}
		if(pEpslonMessageBean.getEpslonMessageType() != null){
		header.setMessageType(pEpslonMessageBean.getEpslonMessageType());
		}
		if(pEpslonMessageBean.getEpslonBrand() != null){
		header.setBrand(pEpslonMessageBean.getEpslonBrand());
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
		header.setCompanyID(pEpslonMessageBean.getEpslonCompanyID());
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone()));
		}
		if(getEpslonConfiguration() != null){
		header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
		header.setDestination(factory.createHeaderTypeDestination(pEpslonMessageBean.getEpslonHeaderTypeDestination()));
		}
		if(pEpslonMessageBean.getEmail() != null){
		header.setRecepientEmailAddress(pEpslonMessageBean.getEmail());
		}
        }
		else{
        	if (isLoggingDebug()) {
    			logDebug("TRUEpslonMessageBean is NULL:"+pEpslonMessageBean);
    		}
        }
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonService getMessageHeader()");
		}
		return header;
	}
	
	/**
	 * This method sets the message template and all other parameters.
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 */
	
	public void generateEpslonRequest(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonService generateEpslonRequest()");
		}
		ObjectFactory factory = new ObjectFactory();
		AccountCreation accounts = factory.createAccountCreation();
		String firstname=TRUEpslonConstants.EMPTY_STRING;
		String lastName=TRUEpslonConstants.EMPTY_STRING;
		String requestXML = TRUEpslonConstants.EMPTY_STRING;
		String regTargetQueue = TRUEpslonConstants.EMPTY_STRING;
		if(!StringUtils.isBlank(pEpslonMessageBean.getEmail()) && getEpslonHelper() != null){
			if(getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail()) != null){
			   firstname = getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail());
			}else{
				if (isLoggingDebug()) {
					logDebug("FIRST NAME FROM getFirstNameFromEmail() IS EMPTY:::");
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("FIRST NAME:"+ firstname);
		}
		EMCoBrandedAccountCreation eMCoBrandedAccountCreation = factory.createDetailTypeEMCoBrandedAccountCreation();
		eMCoBrandedAccountCreation.setFirstName(firstname);
		eMCoBrandedAccountCreation.setLastName(lastName);
		if(!StringUtils.isBlank(TRUEpslonConstants.MSG_BODY)){
		eMCoBrandedAccountCreation.setMessageBody(factory.createDetailTypeEMCoBrandedAccountCreationMessageBody(TRUEpslonConstants.MSG_BODY));
		if (isLoggingDebug()) {
			logDebug("Message Body:"+ TRUEpslonConstants.MSG_BODY);
		}
		}
		/*EMFAOAccountCreation eMFAOAccountCreation = factory.createDetailTypeEMFAOAccountCreation();*/
		
		DetailType detailType = factory.createDetailType();
		if(eMCoBrandedAccountCreation != null){
		detailType.setEMCoBrandedAccountCreation(eMCoBrandedAccountCreation);
		}
		/*if(eMFAOAccountCreation != null){
		detailType.setEMFAOAccountCreation(eMFAOAccountCreation);
		}*/
		if(detailType != null){
		accounts.setMessage(detailType);
		}
		
		HeaderType header = getMessageHeader(pEpslonMessageBean);
		if(header != null){
		accounts.setHeader(header);
		}
		if(accounts.getMessage() != null){
		accounts.setMessage(accounts.getMessage());
		if (isLoggingDebug()) {
			vlogDebug("Account Message ::",accounts.getMessage());
		}
		}
		String regPackageName = TRUEpslonConstants.CREATE_ACCOUNT_PACKAGE;
		if(getEpslonHelper() != null){
		requestXML = getEpslonHelper().generateXML(accounts, regPackageName);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon AccountCreation Request XML ---> ");
			logDebug(requestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		regTargetQueue = getTargetQueue();
		}
		
		if(isEnablePostMessageQueue() && !StringUtils.isBlank(regTargetQueue) && getEpslonHelper() != null){
			boolean isUpdated = false;			
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.ACCOUNT_CREATION_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(requestXML, regTargetQueue,isUpdated,itemId,epsilonEmailType);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonService generateEpslonRequest()");
		}
	}
}



