package com.tru.integrations.epslon.emailafriend;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.emailafriend.EmailAFriend.Message;
import com.tru.integrations.epslon.emailafriend.EmailAFriend.Message.ItemURL;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Email a friend service. 
 * @author PA
 * @version 1.0
 */

public class TRUEpslonEmailAFriendService extends GenericService{
	
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonHelper mEpslonHelper;
	
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	 /**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */

	private boolean mEnablePostMessageQueue;
	
	
	 
	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 *This method is used to set epslonHelper.
	 *@param pEpslonHelper TRUEpslonHelper
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}

	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * This method is used to get targetQueue.
	 * @return targetQueue String
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}

	/**
	 *This method is used to set targetQueue.
	 *@param pTargetQueue String
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}

	/**
	 * This method is used to get enablePostMessageQueue.
	 * @return enablePostMessageQueue boolean
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}

	/**
	 *This method is used to set enablePostMessageQueue.
	 *@param pEnablePostMessageQueue boolean
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}

	/**
	 * This method sets the message template for order acknowledgement email. 
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 */
	
	public void generateEmailAFriendRequest(TRUEpslonMessageBean pEpslonMessageBean){
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonEmailAFriendService generateEmailAFriendRequest()");
		}
        ObjectFactory factory = new ObjectFactory();
		
		EmailAFriend emailAFriend = factory.createEmailAFriend();
		String firstname=TRUEpslonConstants.EMPTY_STRING;
		String lastname=TRUEpslonConstants.EMPTY_STRING;
		String senderFirstName=TRUEpslonConstants.EMPTY_STRING;
		String senderLastName=TRUEpslonConstants.EMPTY_STRING;
		String itemURL1=TRUEpslonConstants.EMPTY_STRING;
		String itemNumber=TRUEpslonConstants.EMPTY_STRING;
		String itemDescription=TRUEpslonConstants.EMPTY_STRING;
		String itemName=TRUEpslonConstants.EMPTY_STRING;
		String emailAFriendRequestXML = TRUEpslonConstants.EMPTY_STRING;
		String emailAFriendTargetQueue = TRUEpslonConstants.EMPTY_STRING;
		
		HeaderType header =getMessageHeader(pEpslonMessageBean);
		ItemURL itemURL = factory.createEmailAFriendMessageItemURL();
		if(!StringUtils.isBlank(pEpslonMessageBean.getItemURL())){
			itemURL1=pEpslonMessageBean.getItemURL();
		}
		if(!StringUtils.isBlank(pEpslonMessageBean.getItemName())){
			itemName = pEpslonMessageBean.getItemName();
		}
		itemURL.setURL(itemURL1);
		itemURL.setName(itemName);
		
		Message msg = factory.createEmailAFriendMessage();
		
		if(!StringUtils.isBlank(pEpslonMessageBean.getItemNumber())){
			itemNumber=	pEpslonMessageBean.getItemNumber();	
		}
		if(!StringUtils.isBlank(pEpslonMessageBean.getItemDescription())){
			itemDescription=pEpslonMessageBean.getItemDescription();
		}
		if(!StringUtils.isBlank(pEpslonMessageBean.getRecieverFirstname())){
			firstname=pEpslonMessageBean.getRecieverFirstname();
		}else{
			if (getEpslonHelper() != null && !StringUtils.isBlank(pEpslonMessageBean.getEmail())) {
				 firstname = getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail());
			}
		}
		
		if (!StringUtils.isBlank(pEpslonMessageBean.getSenderFirstName())) {
		senderFirstName = pEpslonMessageBean.getSenderFirstName();
		}
		msg.setFirstName(firstname);
		msg.setLastName(lastname);
		msg.setItemNumber(itemNumber);
		msg.setItemDescription(itemDescription);
		msg.setItemURL(itemURL);
		msg.setSenderFirstName(senderFirstName);
		msg.setSenderLastName(senderLastName);
		
		emailAFriend.setHeader(header);
		emailAFriend.setMessage(msg);
		String emailAFriendPackage = TRUEpslonConstants.EMAIL_FRIEND_PACKAGE;
		if(getEpslonHelper() != null){
		 emailAFriendRequestXML = getEpslonHelper().generateXML(emailAFriend, emailAFriendPackage);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Email A Friend Request XML ---> ");
			logDebug(emailAFriendRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		emailAFriendTargetQueue = getTargetQueue();
		}
		
		if(isEnablePostMessageQueue() && !StringUtils.isBlank(emailAFriendTargetQueue) && getEpslonHelper() != null){
			boolean isUpdated = false;
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.EMAILFRIEND_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(emailAFriendRequestXML,emailAFriendTargetQueue,isUpdated,itemId,epsilonEmailType);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonEmailAFriendService generateEmailAFriendRequest()");
		}
		
	}
	
	/**
	 * This method sets the message header .
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	 
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonEmailAFriendService getMessageHeader()");
		}
		ObjectFactory factory = new ObjectFactory();
        HeaderType header = factory.createHeaderType();
		
		if(pEpslonMessageBean.getEpslonSource() != null){
		header.setSource(pEpslonMessageBean.getEpslonSource());
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
		header.setReferenceID(pEpslonMessageBean.getEpslonReferenceID());
		}
		if(pEpslonMessageBean.getEpslonMessageType() != null){
		header.setMessageType(pEpslonMessageBean.getEpslonMessageType());
		}
		if(pEpslonMessageBean.getEpslonBrand() != null){
		header.setBrand(pEpslonMessageBean.getEpslonBrand());
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
		header.setCompanyID(pEpslonMessageBean.getEpslonCompanyID());
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone()));
		}
		if(getEpslonConfiguration() != null){
		header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
		header.setDestination(factory.createHeaderTypeDestination(pEpslonMessageBean.getEpslonHeaderTypeDestination()));
		}
		if(pEpslonMessageBean.getEmail() != null){
		header.setRecepientEmailAddress(pEpslonMessageBean.getEmail());
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonEmailAFriendService getMessageHeader()");
		}
		return header;
	}
}
