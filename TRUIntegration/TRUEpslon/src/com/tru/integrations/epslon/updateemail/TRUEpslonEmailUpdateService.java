package com.tru.integrations.epslon.updateemail;


import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.updateemail.Emailupdate.Message;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Email service.
 * @version 1.0.
 * @author Professional Access.
 *
 */

public class TRUEpslonEmailUpdateService extends GenericService {
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private TRUEpslonHelper mEpslonHelper;
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	 /**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */

	private boolean mEnablePostMessageQueue;

	/**
	 * @return the epslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}
	
	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}
	/**
	 * This method sets the message header.
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	 
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonEmailUpdateService getMessageHeader()");
		}
		ObjectFactory factory= new ObjectFactory();
		
        HeaderType header = factory.createHeaderType();
		
		if(pEpslonMessageBean.getEpslonSource() != null){
		header.setSource(pEpslonMessageBean.getEpslonSource());
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
		header.setReferenceID(pEpslonMessageBean.getEpslonReferenceID());
		}
		if(pEpslonMessageBean.getEpslonMessageType() != null){
		header.setMessageType(pEpslonMessageBean.getEpslonMessageType());
		}
		if(pEpslonMessageBean.getEpslonBrand() != null){
		header.setBrand(pEpslonMessageBean.getEpslonBrand());
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
		header.setCompanyID(pEpslonMessageBean.getEpslonCompanyID());
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone()));
		}
		if(getEpslonConfiguration() != null){
		header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
		header.setDestination(factory.createHeaderTypeDestination(pEpslonMessageBean.getEpslonHeaderTypeDestination()));
		}
		if(pEpslonMessageBean.getOldEmailAddress() != null){
		header.setRecepientEmailAddress(pEpslonMessageBean.getOldEmailAddress());
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonEmailUpdateService getMessageHeader()");
		}
		return header;
	}
	
	/**
	 * @return the targetQueue
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}

	/**
	 * @return the enablePostMessageQueue
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}
	
/**
 * This method sets the message template for password reset success.
 * @param pEpslonMessageBean TRUEpslonMessageBean
 */
	
  public void sendEmailUpdateSuccessEmail(TRUEpslonMessageBean pEpslonMessageBean){
		
	   if (isLoggingDebug()) {
			logDebug("ENTER::::TRUEpslonEmailUpdateService sendEmailUpdateSuccessEmail()");
		}
		com.tru.integrations.epslon.updateemail.ObjectFactory objectFactory = new com.tru.integrations.epslon.updateemail.ObjectFactory();
		String firstName = TRUEpslonConstants.EMPTY_STRING; 
		String lastName = TRUEpslonConstants.EMPTY_STRING; 
		String updatedEmail = TRUEpslonConstants.EMPTY_STRING;
		String emailUpdateRequestXML = TRUEpslonConstants.EMPTY_STRING;
		String emailUpdateTargetQueue = TRUEpslonConstants.EMPTY_STRING;
		
		if (!StringUtils.isBlank(pEpslonMessageBean.getUpdatedEmailAddress()) && getEpslonHelper() != null) {
			if(!StringUtils.isBlank(getEpslonHelper().getFirstName(pEpslonMessageBean.getUpdatedEmailAddress()))){
			firstName = getEpslonHelper().getFirstName(pEpslonMessageBean.getUpdatedEmailAddress()).toString();
			}
			if(!StringUtils.isBlank(getEpslonHelper().getLastName(pEpslonMessageBean.getUpdatedEmailAddress()))){
			lastName = getEpslonHelper().getLastName(pEpslonMessageBean.getUpdatedEmailAddress()).toString();
			}
			
		}
		if(!StringUtils.isBlank(pEpslonMessageBean.getUpdatedEmailAddress())){
			updatedEmail = pEpslonMessageBean.getUpdatedEmailAddress();
		}
		
		Emailupdate emailupdate = objectFactory.createEmailupdate();
		Message msg = objectFactory.createEmailupdateMessage();
		HeaderType headerType = getMessageHeader(pEpslonMessageBean);
		
		msg.setUpdatedEmailAddress(updatedEmail);
		msg.setFirstName(firstName);
		msg.setLastName(lastName);
		emailupdate.setMessage(msg);
		emailupdate.setHeader(headerType);
		
		String emailUpdatePackage =  TRUEpslonConstants.EMAIL_UPDATE_PACKAGE;
		if(getEpslonHelper() != null){
		emailUpdateRequestXML = getEpslonHelper().generateXML(emailupdate, emailUpdatePackage);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Email Update Request XML --->");
			logDebug(emailUpdateRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		 emailUpdateTargetQueue = getTargetQueue();
		}
		
		if (isEnablePostMessageQueue() && !StringUtils.isBlank(emailUpdateTargetQueue) && getEpslonHelper() != null) {
			boolean isUpdated = false;
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.CHANGEMAIL_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(emailUpdateRequestXML,emailUpdateTargetQueue,isUpdated,itemId,epsilonEmailType);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonEmailUpdateService sendEmailUpdateSuccessEmail()");
		}
	}

/**
 * @param pEnablePostMessageQueue the enablePostMessageQueue to set
 */
public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
	mEnablePostMessageQueue = pEnablePostMessageQueue;
}
	
/**
 * @param pEpslonConfiguration the epslonConfiguration to set
 */
public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
	mEpslonConfiguration = pEpslonConfiguration;
}
	
 /**
 *This method is used to set epslonHelper.
 *@param pEpslonHelper TRUEpslonHelper
 */
public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
	mEpslonHelper = pEpslonHelper;
}
/**
 * @param pTargetQueue the targetQueue to set
 */
public void setTargetQueue(String pTargetQueue) {
	mTargetQueue = pTargetQueue;
}
}



