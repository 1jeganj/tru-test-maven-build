package com.tru.integrations.epslon.reset;


import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.reset.Password.Message;
import com.tru.integrations.epslon.reset.Password.Message.Reset;
import com.tru.integrations.epslon.reset.Password.Message.Reset.MyAccountURL;
import com.tru.integrations.epslon.reset.Password.Message.Reset.ResetPWURL;
import com.tru.integrations.epslon.reset.Password.Message.ResetSuccess;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class holds the business logic related to Epslon Email service.
 * @version 1.0
 * @author Professional Access
 *
 */

public class TRUEpslonPasswordResetService extends GenericService {
	
	/**
	 * Holds the property value of mEpslonHelper.
	 */
	private TRUEpslonHelper mEpslonHelper;
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	 /**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;
	
	/**
	 *  property to hold enablePostMessageQueue.
	 */

	private boolean mEnablePostMessageQueue;
	
	/**
	 * @return the enablePostMessageQueue
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}

	/**
	 * @param pEnablePostMessageQueue the enablePostMessageQueue to set
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}
	/**
	 * @return the targetQueue
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}
	/**
	 * @param pTargetQueue the targetQueue to set
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	/**
	 * @return the epslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 * @param pEpslonConfiguration the epslonConfiguration to set
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}
	/**
	 * This method is used to get epslonHelper.
	 * @return epslonHelper TRUEpslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 *This method is used to set epslonHelper.
	 *@param pEpslonHelper TRUEpslonHelper
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}
	/**
	 * This method sets the message template for password reset. 
	 * @param pEpslonMessageBean TRUEpslonMessageBean.
	 */
	
	public void resetPasswordEmail(TRUEpslonMessageBean pEpslonMessageBean){
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonPasswordResetService resetPasswordEmail()");
		}
		String firstName = TRUEpslonConstants.EMPTY_STRING;
		String lastName = TRUEpslonConstants.EMPTY_STRING;
		String urlName = TRUEpslonConstants.EMPTY_STRING; 
		String urlValue = TRUEpslonConstants.EMPTY_STRING; 
		String passwordResetRequestXML = TRUEpslonConstants.EMPTY_STRING; 
		String passwordResetTargetQueue = TRUEpslonConstants.EMPTY_STRING; 
		
        ObjectFactory factory = new ObjectFactory();	
        
		Password password = factory.createPassword();
		Message msg = factory.createPasswordMessage();
		
		Reset rst = factory.createPasswordMessageReset();
		if(!StringUtils.isBlank(pEpslonMessageBean.getEmail()) && getEpslonHelper() != null){
			if(!StringUtils.isBlank(getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail()))){
				firstName = getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail()).toString();
				}
				if(!StringUtils.isBlank(getEpslonHelper().getLastName(pEpslonMessageBean.getEmail()))){
				lastName = getEpslonHelper().getLastName(pEpslonMessageBean.getEmail()).toString();
				}
				if(!StringUtils.isBlank(getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail()))){
				urlName = getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail()).toString();
				}
		}
		rst.setFirstName(firstName);
		rst.setLastName(lastName);
		ResetPWURL resetPassURL = factory.createPasswordMessageResetResetPWURL();
		resetPassURL.setName(firstName);
		if(pEpslonMessageBean.getResetPasswordURL() != null){
			urlValue = pEpslonMessageBean.getResetPasswordURL();
		}
		resetPassURL.setURL(urlValue);
		rst.setResetPWURL(resetPassURL);
		MyAccountURL myAccountURL = factory.createPasswordMessageResetMyAccountURL();
		myAccountURL.setName(urlName);
		if(! StringUtils.isBlank(pEpslonMessageBean.getMyAccountURL())){
			myAccountURL.setURL(pEpslonMessageBean.getMyAccountURL());
		}
		rst.getMyAccountURL().add(myAccountURL);
		msg.setReset(rst);
		HeaderType header = getMessageHeader(pEpslonMessageBean);
		
		password.setHeader(header);
		password.setMessage(msg);
		
		String passwordResetPackage = TRUEpslonConstants.PASSWORD_RESET_PACKAGE;
		if(getEpslonHelper() != null){
			passwordResetRequestXML = getEpslonHelper().generateXML(password, passwordResetPackage);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Password Reset Request XML --->");
			logDebug(passwordResetRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
			passwordResetTargetQueue = getTargetQueue();
		}
		if(isEnablePostMessageQueue() && !StringUtils.isBlank(passwordResetTargetQueue) && getEpslonHelper() != null){
			boolean isUpdated = false;
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.RESET_PASSWORD_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(passwordResetRequestXML,passwordResetTargetQueue,isUpdated,itemId,epsilonEmailType);
		}	
		
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonPasswordResetService resetPasswordEmail()");
		}
		
	}
		
	/**
	 * This method sets the message header.
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 * @return header HeaderType
	 */
	 
	public HeaderType getMessageHeader(TRUEpslonMessageBean pEpslonMessageBean) {
		
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUEpslonPasswordResetService getMessageHeader()");
		}
		ObjectFactory factory = new ObjectFactory();
        HeaderType header = factory.createHeaderType();
		
		if(pEpslonMessageBean.getEpslonSource() != null){
		header.setSource(pEpslonMessageBean.getEpslonSource());
		}
		if(pEpslonMessageBean.getEpslonReferenceID() != null){
		header.setReferenceID(pEpslonMessageBean.getEpslonReferenceID());
		}
		if(pEpslonMessageBean.getEpslonMessageType() != null){
		header.setMessageType(pEpslonMessageBean.getEpslonMessageType());
		}
		if(pEpslonMessageBean.getEpslonBrand() != null){
		header.setBrand(pEpslonMessageBean.getEpslonBrand());
		}
		if(pEpslonMessageBean.getEpslonCompanyID() != null){
		header.setCompanyID(pEpslonMessageBean.getEpslonCompanyID());
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale() != null){
		header.setMsgLocale(factory.createHeaderTypeMsgLocale(pEpslonMessageBean.getEpslonHeaderTypeMsgLocale()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone() != null){
		header.setMsgTimeZone(factory.createHeaderTypeMsgTimeZone(pEpslonMessageBean.getEpslonHeaderTypeMsgTimeZone()));
		}
		if(getEpslonConfiguration() != null){
		header.setInternalDateTimeStamp(factory.createHeaderTypeInternalDateTimeStamp(getEpslonConfiguration().getCurrentTimeStamp()));
		}
		if(pEpslonMessageBean.getEpslonHeaderTypeDestination() != null){
		header.setDestination(factory.createHeaderTypeDestination(pEpslonMessageBean.getEpslonHeaderTypeDestination()));
		}
		if(pEpslonMessageBean.getEmail() != null){
		header.setRecepientEmailAddress(pEpslonMessageBean.getEmail());
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonPasswordResetService getMessageHeader()");
		}
		return header;
	}
	
	/**
	 * This method sets the message template for password reset success.
	 * @param pEpslonMessageBean TRUEpslonMessageBean
	 */
	
   public void resetPasswordSuccessEmail(TRUEpslonMessageBean pEpslonMessageBean){
		
	   if (isLoggingDebug()) {
			logDebug("ENTER::::TRUEpslonPasswordResetService resetPasswordSuccessEmail()");
		}
		ObjectFactory factory = new ObjectFactory();
		String firstName = TRUEpslonConstants.EMPTY_STRING; 
		String lastName = TRUEpslonConstants.EMPTY_STRING; 
		String urlName = TRUEpslonConstants.EMPTY_STRING; 
		String passResetSuccessRequestXML = TRUEpslonConstants.EMPTY_STRING; 
		String passwordResetTargetQueue = TRUEpslonConstants.EMPTY_STRING; 
		if (!StringUtils.isBlank(pEpslonMessageBean.getEmail()) && getEpslonHelper() != null) {
			if(!StringUtils.isBlank(getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail()))){
			firstName = getEpslonHelper().getFirstName(pEpslonMessageBean.getEmail()).toString();
			}
			if(!StringUtils.isBlank(getEpslonHelper().getLastName(pEpslonMessageBean.getEmail()))){
			lastName = getEpslonHelper().getLastName(pEpslonMessageBean.getEmail()).toString();
			}
			if(!StringUtils.isBlank(getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail()))){
			urlName = getEpslonHelper().getFirstNameFromEmail(pEpslonMessageBean.getEmail()).toString();
			}
		}
		if (isLoggingDebug()) {
			logDebug("FIRST NAME" + firstName);
		}
		if (isLoggingDebug()) {
			logDebug("LAST NAME" + lastName);
		}
		Password password = factory.createPassword();
		HeaderType header = getMessageHeader(pEpslonMessageBean);

		Message msg = factory.createPasswordMessage();

		ResetSuccess resetSuccess = factory.createPasswordMessageResetSuccess();
		com.tru.integrations.epslon.reset.Password.Message.ResetSuccess.MyAccountURL myAccountURL = factory.createPasswordMessageResetSuccessMyAccountURL();
		myAccountURL.setName(urlName);
		myAccountURL.setURL(pEpslonMessageBean.getMyAccountURL());
		resetSuccess.setFirstName(firstName);
		resetSuccess.setLastName(lastName);
		resetSuccess.getMyAccountURL().add(myAccountURL);
		msg.setResetSuccess(resetSuccess);

		password.setHeader(header);
		password.setMessage(msg);
		String passwordResetPackage = TRUEpslonConstants.PASSWORD_RESET_PACKAGE;
		if(getEpslonHelper() != null){
		passResetSuccessRequestXML = getEpslonHelper().generateXML(password, passwordResetPackage);
		}
		if (isLoggingDebug()) {
			logDebug("<-- Epsilon Password Reset Success Request XML --->");
			logDebug(passResetSuccessRequestXML);
		}
		if(!StringUtils.isBlank(getTargetQueue())){
		passwordResetTargetQueue = getTargetQueue();
		}
		if(isEnablePostMessageQueue() && !StringUtils.isBlank(passwordResetTargetQueue) && getEpslonHelper() != null){
			boolean isUpdated = false;
			String itemId = TRUEpslonConstants.EMPTY_STRING;
			String epsilonEmailType = TRUEpslonConstants.CHANGEPWD_EMAIL_TYPE;
			getEpslonHelper().postMessageToQueue1(passResetSuccessRequestXML,passwordResetTargetQueue,isUpdated,itemId,epsilonEmailType);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUEpslonPasswordResetService resetPasswordSuccessEmail()");
		}
	}
}



