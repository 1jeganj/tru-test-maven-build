package com.tru.integrations.epslon.scheduler;

import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.helper.TRUEpslonHelper;




/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository in case of get success response.
 * @author Professional Access
 * @version 1.0
 */
public class TRUEpsilonRedeliveryScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds the property value of epslonConfiguration
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	/**
	 * Holds the property value of mEpslonHelper
	 */
	private TRUEpslonHelper mEpslonHelper;
	
	/**
	 * @return the epslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 * @param pEpslonHelper the epslonHelper to set
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}

	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;
	
	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration String
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration String
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * Overriding OOTB method to re-deliver the failed Epsilon request.
	 * 
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			repostFailedMessages();
		}
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: ENDS ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

/**
 *  used to repost Failed Messages
 */
public void repostFailedMessages(){
	if(isLoggingDebug()){
		logDebug("TRUEpsilonRedeliveryScheduler :: repostFailedMessages() method :: STARTS ");
	}
	RepositoryItem[] failedEpsilonMessageItems = getEpslonHelper().getAuditMessages();
	if(failedEpsilonMessageItems != null){
		for(RepositoryItem messageItem :failedEpsilonMessageItems ){
			String status = (String) messageItem.getPropertyValue(getEpslonConfiguration().getEpsilonStatusPropertyName());
			if(!StringUtils.isBlank(status) && status.equalsIgnoreCase(TRUEpslonConstants.FAILURE)){
				
				boolean isUpdatedInRep = true;
				String itemId = (String)messageItem.getRepositoryId();
				if(isLoggingDebug()){
					logDebug("Item Pending for Repost::"+itemId);
				}
				String requestXml = (String) messageItem.getPropertyValue(getEpslonConfiguration().getEpsilonRequestPropertyName());
				String targetQueue = (String) messageItem.getPropertyValue(getEpslonConfiguration().getTargetQueuePropertyName());
				String epsilonEmailType = (String) messageItem.getPropertyValue(getEpslonConfiguration().getEpsilonRequestTypePropertyName());
				getEpslonHelper().postMessageToQueue1(requestXml, targetQueue,isUpdatedInRep,itemId,epsilonEmailType);
			}
		}
	}
	if(isLoggingDebug()){
		logDebug("TRUEpsilonRedeliveryScheduler :: repostFailedMessages() method :: END ");
	}
}
}
