package com.tru.integrations.epslon.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.helper.TRUEpslonHelper;

/**
 * This class is overriding OOTB TRUEpsilonPurgeScheduler abstract class.
 * This class is used to get the all the Success OMS request, purge them and success the entry
 * @author Professional Access
 * @version 1.0
 */
public class TRUEpsilonPurgeScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds the property value of epslonConfiguration
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;
	
	/**
	 * Holds the property value of mEpslonHelper
	 */
	private TRUEpslonHelper mEpslonHelper;
	
	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;

	/**
	 * Holds reference for mDurationToDeleteMessages 
	 */
	private int mDurationToDeleteMessagesInDays;
	
	/**
	 * This method returns the durationToDeleteMessagesInDays value
	 *
	 * @return the durationToDeleteMessagesInDays
	 */
	public int getDurationToDeleteMessagesInDays() {
		return mDurationToDeleteMessagesInDays;
	}
	/**
	 * This method sets the durationToDeleteMessagesInDays with parameter value pDurationToDeleteMessagesInDays
	 *
	 * @param pDurationToDeleteMessagesInDays the durationToDeleteMessagesInDays to set
	 */
	public void setDurationToDeleteMessagesInDays(
			int pDurationToDeleteMessagesInDays) {
		mDurationToDeleteMessagesInDays = pDurationToDeleteMessagesInDays;
	}
	
	/**
	 * @return the epslonHelper
	 */
	public TRUEpslonHelper getEpslonHelper() {
		return mEpslonHelper;
	}

	/**
	 * @param pEpslonHelper the epslonHelper to set
	 */
	public void setEpslonHelper(TRUEpslonHelper pEpslonHelper) {
		mEpslonHelper = pEpslonHelper;
	}
	
	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration String
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 *This method is used to set epslonConfiguration.
	 *@param pEpslonConfiguration String
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Overriding OOTB method to re-deliver the failed Epsilon request.
	 * 
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			getEpslonHelper().purgeEpslonAuditMessages(getDurationToDeleteMessagesInDays());
		}
		if(isLoggingDebug()){
			logDebug("TRUEpsilonRedeliveryScheduler :: doScheduledTask() method :: ENDS ");
		}
	}


}
