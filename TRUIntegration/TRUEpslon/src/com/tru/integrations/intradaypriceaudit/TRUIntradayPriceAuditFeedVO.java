package com.tru.integrations.intradaypriceaudit;

import java.io.Serializable;

/**
 * The Class TRUIntradayPriceAuditFeedVO.
 * @author PA.
 * @version 1.0.
 */
public class TRUIntradayPriceAuditFeedVO implements Serializable {
	private static final long serialVersionUID = 1L;
	/** Reference point to the sku. */
	private String mSkuId;
	/** property to hold listPrice. */
	private String mListPrice;
	/** property to hold salePrice. */
	private String mSalePrice;
	/** Locale of the price list to be created. */
	private String mLocale;
	/** Three digit ISO Country code. */
	private String mCountryCode;
	/** Three digit market code. */
	private String mMarketCode;
	/** Facility/Location code for every storefront. */
	private String mStoreNumber;
	/** mEmergencyListPrice. */
	private boolean mEmergencyListPrice;
	/** mEmergencySalePrice. */
	private boolean mEmergencySalePrice;
	/** The m deal id. */
	private String mDealId;

	/**
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * @param pSkuId
	 *            the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * @return the listPrice
	 */
	public String getListPrice() {
		return mListPrice;
	}

	/**
	 * @param pListPrice
	 *            the listPrice to set
	 */
	public void setListPrice(String pListPrice) {
		mListPrice = pListPrice;
	}

	/**
	 * @return the salePrice
	 */
	public String getSalePrice() {
		return mSalePrice;
	}

	/**
	 * @param pSalePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * @param pLocale
	 *            the locale to set
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * @param pCountryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String pCountryCode) {
		mCountryCode = pCountryCode;
	}

	/**
	 * @return the marketCode
	 */
	public String getMarketCode() {
		return mMarketCode;
	}

	/**
	 * @param pMarketCode
	 *            the marketCode to set
	 */
	public void setMarketCode(String pMarketCode) {
		mMarketCode = pMarketCode;
	}

	/**
	 * @return the storeNumber
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}

	/**
	 * @param pStoreNumber
	 *            the storeNumber to set
	 */
	public void setStoreNumber(String pStoreNumber) {
		mStoreNumber = pStoreNumber;
	}

	/**
	 * @return the emergencyListPrice
	 */
	public boolean isEmergencyListPrice() {
		return mEmergencyListPrice;
	}

	/**
	 * @param pEmergencyListPrice
	 *            the emergencyListPrice to set
	 */
	public void setEmergencyListPrice(boolean pEmergencyListPrice) {
		mEmergencyListPrice = pEmergencyListPrice;
	}

	/**
	 * @return the emergencySalePrice
	 */
	public boolean isEmergencySalePrice() {
		return mEmergencySalePrice;
	}

	/**
	 * @param pEmergencySalePrice
	 *            the emergencySalePrice to set
	 */
	public void setEmergencySalePrice(boolean pEmergencySalePrice) {
		mEmergencySalePrice = pEmergencySalePrice;
	}

	/**
	 * Gets the deal id.
	 * 
	 * @return the mDealId
	 */
	public String getDealId() {
		return mDealId;
	}

	/**
	 * Sets the deal id.
	 * 
	 * @param pDealId
	 *            the new deal id
	 */
	public void setDealId(String pDealId) {
		this.mDealId = pDealId;
	}
}
