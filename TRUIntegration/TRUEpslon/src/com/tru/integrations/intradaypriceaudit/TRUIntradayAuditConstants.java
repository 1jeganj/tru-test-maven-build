package com.tru.integrations.intradaypriceaudit;

/**
 * The Class TRUIntradayAuditFeedConstants hold constants related to intraday
 * price audit.
 * @author PA.
 * @version 1.0.
 */
public class TRUIntradayAuditConstants {

	/** The Constant NUMBER_ONE. */
	public static final int INTEGER_NUMBER_ONE = 1;

	/** The Constant PRICE. */
	public static final String PRICE = "price";

	/** The Constant START_HEADER. */
	public static final String START_HEADER = "START";

	/** The Constant NEW_LINE_STRING. */
	public static final String NEW_LINE_STRING = "\n";

	/** The Constant PIPE_DELIMITER. */
	public static final String PIPE_DELIMITER = "|";

	/** The Constant END_TRAILER. */
	public static final String END_TRAILER = "END";

	/** The Constant JMS_MESSAGE. */
	public static final String JMS_MESSAGE = "JMSMessage";
	
	/** The Constant COMMA_STRING. */
	public static final String COMMA_STRING = ",";
	
	/** The Constant LIST_PRICE. */
	public static final String LIST_PRICE = "listPrice";
	
	/** The Constant SALE_PRICE. */
	public static final String SALE_PRICE = "salePrice";

	/** The Constant INTEGER_NUMBER_ZERO. */
	public static final int INTEGER_NUMBER_ZERO = 0;
	
	/** The Constant JMSX_GROUPID. */
	public static final String JMSX_GROUPID = "JMSXGroupID";
	
	/** The Constant INTEGER_SIXTY. */
	public static final int INT_SIXTY = 60;
	
	/** The Constant INTEGER_THOUSAND. */
	public static final int INT_THOUSAND = 1000;
	
	/** The Constant INTEGER_TWENTY_FOUR. */
	public static final int INT_TWENTY_FOUR = 24;
	
	/** The Constant ERROR_MESSAGE_INITIAL. */
	public static final String ERROR_MESSAGE_INITIAL = "Intraday ESB queue unavailable for ";
	
	/** The Constant ERROR_MESSAGE_DAYS. */
	public static final String ERROR_MESSAGE_DAYS = " days ";
	
	/** The Constant ERROR_MESSAGE_HOURS. */
	public static final String ERROR_MESSAGE_HOURS = " hours ";
	
	/** The Constant ERROR_MESSAGE_MINUTES. */
	public static final String ERROR_MESSAGE_MINUTES = " minutes from "; 
	
	/** The Constant STRING_TO. */
	public static final String STRING_TO = " to ";
	
	/** The Constant SERVER_PATH. */
	public static final String SERVER_PATH ="atg.dynamo.server.name";
	
	/** The Constant MESSAGE_SUBJECT. */
	public static final String MESSAGE_SUBJECT = "messageSubject";
	
	/** The Constant SITE_ID. */
	public static final String SITE_ID = "siteId";
	
	/** The Constant ERRORDETAIL. */
	public static final String ERRORDETAIL = "detailDescription";
	
	/** The Constant REPORT. */
	public static final String REPORT = "reportSymptom";
	
	/** The Constant PRIORITY. */
	public static final String PRIORITY = "mailPriority";
	
	/** The Constant QUEUE. */
	public static final String QUEUE = "queueName";
	
	/** The Constant NOTES. */
	public static final String NOTES = "mailNotes";
	
	/** The Constant SOFTWARE. */
	public static final String SOFTWARE = "mailSoftware";
	
	/** The Constant OSNAME. */
	public static final String OSNAME = "mailOsName";
	
	/** The Constant JAVA_VERSION. */
	public static final String JAVA_VERSION = "javaVersion";
	
	/** The Constant OS_HOST_NAME. */
	public static final String OS_HOST_NAME = "osHostName";
	
	/** The Constant OS_NAME_STRING. */
	public static final String OS_NAME_STRING = "os.name";
	
	/** The Constant JAVA_VERSION_STRING. */
	public static final String JAVA_VERSION_STRING = "java.version";
	
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "MMddyyyy'-'HH:mm:ss";
	
	/** The Constant POST_MSG_STARTED. */
	public static final String POST_MSG_STARTED = "Posting Message to Queue - Initiated";
	
	/** The Constant POST_MSG_SUCCESS. */
	public static final String POST_MSG_SUCCESS = "Posting Message to Queue - Successful";
	
	/** The Constant POST_MSG_FAILURE. */
	public static final String POST_MSG_FAILURE = "Posting Message to Queue - Failure";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant INTRADAY_QUEUE. */
	public static final String INTRADAY_QUEUE ="Intraday Price Message Queue";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "feedStatus";
	
	
}
