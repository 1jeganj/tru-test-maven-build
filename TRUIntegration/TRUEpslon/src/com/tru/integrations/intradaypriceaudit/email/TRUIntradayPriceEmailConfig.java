package com.tru.integrations.intradaypriceaudit.email;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

/**
 * The Class TRUIntradayPriceEmailConfig holds email related configurations.
 * @author PA.
 * @version 1.0.
 */
public class TRUIntradayPriceEmailConfig extends GenericService {

	/** The m site id. */
	private String mSiteId;

	/** The m receiver email id. */
	private List<String> mReceiverEmailId;

	/** The m template email sender. */
	private TemplateEmailSender mTemplateEmailSender;

	/** The m template url. */
	private String mTemplateURL;

	/** The m template email info. */
	private TemplateEmailInfoImpl mTemplateEmailInfo;

	/** The m mail priority2. */
	private String mMailPriority2;

	/** The m mail notes. */
	private String mMailNotes;

	/** The m mail software. */
	private String mMailSoftware;

	/** The m mail report symptom. */
	private String mMailReportSymptom;

	/**
	 * Gets the site id.
	 * 
	 * @return the site id
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * Sets the site id.
	 * 
	 * @param pSiteId
	 *            the new site id
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}

	/**
	 * Gets the receiver email id.
	 * 
	 * @return the receiver email id
	 */
	public List<String> getReceiverEmailId() {
		return mReceiverEmailId;
	}

	/**
	 * Sets the receiver email id.
	 * 
	 * @param pReceiverEmailId
	 *            the new receiver email id
	 */
	public void setReceiverEmailId(List<String> pReceiverEmailId) {
		this.mReceiverEmailId = pReceiverEmailId;
	}

	/**
	 * Gets the template email sender.
	 * 
	 * @return the template email sender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the new template email sender
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		this.mTemplateEmailSender = pTemplateEmailSender;
	}

	/**
	 * Gets the template url.
	 * 
	 * @return the template url
	 */
	public String getTemplateURL() {
		return mTemplateURL;
	}

	/**
	 * Sets the template url.
	 * 
	 * @param pTemplateURL
	 *            the new template url
	 */
	public void setTemplateURL(String pTemplateURL) {
		this.mTemplateURL = pTemplateURL;
	}

	/**
	 * Gets the template email info.
	 * 
	 * @return the template email info
	 */
	public TemplateEmailInfoImpl getTemplateEmailInfo() {
		return mTemplateEmailInfo;
	}

	/**
	 * Sets the template email info.
	 * 
	 * @param pTemplateEmailInfo
	 *            the new template email info
	 */
	public void setTemplateEmailInfo(TemplateEmailInfoImpl pTemplateEmailInfo) {
		this.mTemplateEmailInfo = pTemplateEmailInfo;
	}

	/**
	 * Gets the mail priority2.
	 * 
	 * @return the mMailPriority2
	 */
	public String getMailPriority2() {
		return mMailPriority2;
	}

	/**
	 * Sets the mail priority2.
	 * 
	 * @param pMailPriority2
	 *            the new mail priority2
	 */
	public void setMailPriority2(String pMailPriority2) {
		this.mMailPriority2 = pMailPriority2;
	}

	/**
	 * Gets the mail notes.
	 * 
	 * @return the mMailNotes
	 */
	public String getMailNotes() {
		return mMailNotes;
	}

	/**
	 * Sets the mail notes.
	 * 
	 * @param pMailNotes
	 *            the new mail notes
	 */
	public void setMailNotes(String pMailNotes) {
		this.mMailNotes = pMailNotes;
	}

	/**
	 * Gets the mail software.
	 * 
	 * @return the mMailSoftware
	 */
	public String getMailSoftware() {
		return mMailSoftware;
	}

	/**
	 * Sets the mail software.
	 * 
	 * @param pMailSoftware
	 *            the new mail software
	 */
	public void setMailSoftware(String pMailSoftware) {
		this.mMailSoftware = pMailSoftware;
	}

	/**
	 * Gets the mail report symptom.
	 * 
	 * @return the mMailReportSymptom
	 */
	public String getMailReportSymptom() {
		return mMailReportSymptom;
	}

	/**
	 * Sets the mail report symptom.
	 * 
	 * @param pMailReportSymptom
	 *            the new mail report symptom
	 */
	public void setMailReportSymptom(String pMailReportSymptom) {
		this.mMailReportSymptom = pMailReportSymptom;
	}

}
