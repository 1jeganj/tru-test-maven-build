package com.tru.integrations.intradaypriceaudit;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.logging.TRUAlertLogger;
import com.tru.messaging.TRUIntradayPriceMessageSink;

/**
 * The Class is used to post message to TRU messaging queue.
 * @author PA.
 * @version 1.0.
 * 
 */
public class TRUIntradayPriceMessageService extends GenericService {

	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;

	/**
	 * Holds the property value of mTargetQueue.
	 */
	private String mTargetQueue;

	/**
	 * Holds the property value of mEnablePostMessageQueue.
	 */
	private boolean mEnablePostMessageQueue;

	/**
	 * Holds the property value of COLON.
	 */
	public static final String COLON = ":";

	/**
	 * Holds the property value of DSLASH.
	 */
	public static final String DSLASH = "//";

	/**
	 * Holds the property value of mIntradayPriceMessageSink.
	 */
	private TRUIntradayPriceMessageSink mIntradayPriceMessageSink;

	/**
	 * Holds the property value of mLocalMessageJMSId.
	 */
	private String mLocalMessageJMSId;

	/**
	 * Holds the property value of mLocalMessageJMSId.
	 */
	private int mCount;

	/**
	 * Holds the property value of mLocalMessageJMSId.
	 */
	private int mRetryCount;

	/**
	 * Holds the property value of mMessageServiceUtil.
	 */
	private TRUIntradayPriceMessageServiceUtil mMessageServiceUtil;
	
	/**
	 * Holds the property value of mJmsXGroupID.
	 */
	private String mJmsXGroupID;
	
	/**
	 * Holds the property value of mEnableRetryMessageToQueue.
	 */
	private boolean mEnableRetryMessageToQueue;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;

	/**
	 * This method is used to get epslonConfiguration
	 * 
	 * @return epslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 * @return the mIntradayPriceMessageSink
	 */
	public TRUIntradayPriceMessageSink getIntradayPriceMessageSink() {
		return mIntradayPriceMessageSink;
	}

	/**
	 * Gets the jms x group id.
	 *
	 * @return the mJmsXGroupID
	 */
	public String getJmsXGroupID() {
		return mJmsXGroupID;
	}

	/**
	 * @return the mLocalMessageJMSId
	 */
	public String getLocalMessageJMSId() {
		return mLocalMessageJMSId;
	}

	/**
	 * Gets the message service util.
	 * 
	 * @return the message service util
	 */
	public TRUIntradayPriceMessageServiceUtil getMessageServiceUtil() {
		return mMessageServiceUtil;
	}

	/**
	 * @return the mRetryCount
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 * This method is used to get targetQueue
	 * 
	 * @return targetQueue String
	 */
	public String getTargetQueue() {
		return mTargetQueue;
	}

	/**
	 * This method is used to get enablePostMessageQueue
	 * 
	 * @return enablePostMessageQueue boolean
	 */
	public boolean isEnablePostMessageQueue() {
		return mEnablePostMessageQueue;
	}

	/**
	 * Checks if is enable retry message to queue.
	 *
	 * @return the mEnableRetryMessageToQueue
	 */
	public boolean isEnableRetryMessageToQueue() {
		return mEnableRetryMessageToQueue;
	}

	/**
	 * This method is used to send the message to the Queue.
	 * 
	 * @param pRequestStringMessages
	 *            the request string messages
	 * @param pSkuList
	 *            the sku list
	 * @param pJmsId
	 *            the jms id
	 * @param pMessageRecvTime
	 *            the message recv time
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */

	public void postMessageToExternalQueue(List<String> pRequestStringMessages, List<String> pSkuList, String pJmsId, Timestamp pMessageRecvTime,
			String pErrorRepositoryId) {

		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUIntradayPriceMessageService .postMessageToExternalQueue()");
		}
		String startDate = new SimpleDateFormat(TRUIntradayAuditConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUIntradayAuditConstants.POST_MSG_STARTED);
		if (pRequestStringMessages != null && !pRequestStringMessages.isEmpty()) {
			Connection connection = null;
			boolean isQueueConnected = Boolean.TRUE;
			setCounterAndJmsId(pJmsId);

			try {
				String connectionURL = getEpslonConfiguration().getESBProtocol().concat(COLON).concat(DSLASH).concat(getEpslonConfiguration().getESBServer())
						.concat(COLON).concat(getEpslonConfiguration().getESBPort());
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> connectionURL : " + connectionURL);
				}
				ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(getEpslonConfiguration().getESBUser(), getEpslonConfiguration()
						.getESBPassword(), connectionURL);
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Trying to get connection ");
				}
				connection = factory.createConnection();
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Starting Connection ");
				}
				connection.start();
				QueueSession session = ((QueueConnection) connection).createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Create Queue Session : Queue Name : " + getTargetQueue());
				}
				Queue queue = (Queue) session.createQueue(getTargetQueue());
				QueueSender sender = session.createSender((javax.jms.Queue) queue);
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Create Text Message to be sent ");
				}
				TextMessage msg = session.createTextMessage();
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Message JMSXGroupId  : " + getJmsXGroupID());
					logDebug("TRUIntradayPriceMessageService --> Start - Sending Messages to Queue ");
				}
				for (String requestString : pRequestStringMessages) {
					if (isLoggingDebug()) {
						logDebug("TRUIntradayPriceMessageService --> Message Order sent to ESB  : " + requestString);
					}
					msg.setStringProperty(TRUIntradayAuditConstants.JMSX_GROUPID, getJmsXGroupID());
					msg.setText(requestString);
					sender.send(msg);
				}
				isQueueConnected = Boolean.TRUE;
				logSuccessMessage(startDate, TRUIntradayAuditConstants.POST_MSG_SUCCESS);
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageService --> Messages Sent Successfully ");
				}
			} catch (JMSException e) {
				isQueueConnected = Boolean.FALSE;
				logFailedMessage(startDate, TRUIntradayAuditConstants.POST_MSG_FAILURE);
				if (isLoggingError()) {
					logError("JMSException in TRUIntradayPriceMessageService postMessageToExternalQueue() : ", e);
				}
			} finally {
				if (connection != null) {
					try {
						connection.stop();
						connection.close();
						if (isLoggingDebug()) {
							vlogDebug("Closing the Connection");
						}
					} catch (JMSException e) {
						isQueueConnected = Boolean.FALSE;
						logFailedMessage(startDate, TRUIntradayAuditConstants.POST_MSG_FAILURE);
						if (isLoggingError()) {
							logError("JMSException in TRUIntradayPriceMessageService postMessageToExternalQueue() : ", e);
						}
					} finally {
						connection = null;
					}
				}
			}

			if(isEnableRetryMessageToQueue()) {
				retryPostingErrorMessage(pSkuList, pJmsId, pMessageRecvTime, pErrorRepositoryId, isQueueConnected);
			}
			if (isLoggingDebug()) {
				vlogDebug("EXIT:::TRUIntradayPriceMessageService postMessageToExternalQueue()");
			}
		}
	}

	/**
	 * Post message from local queue, post message to external queue if enable
	 * post message is enabled
	 * 
	 * @param pAuditFeedVOs
	 *            the audit feed v os
	 * @param pSkuList
	 *            the sku list
	 * @param pJmsId
	 *            the jms id
	 * @param pMessageRecvTime
	 *            the message recv time
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */
	public void processMessagesToPost(List<TRUIntradayPriceAuditFeedVO> pAuditFeedVOs, List<String> pSkuList, String pJmsId, Timestamp pMessageRecvTime,
			String pErrorRepositoryId) {

		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRUIntradayPriceMessageService processMessagesToPost()");
		}
		int mapSize = pAuditFeedVOs.size();
		int counter = 0;
		List<String> priceMessages = new ArrayList<String>();
		for (TRUIntradayPriceAuditFeedVO intradayPriceAuditFeedVO : pAuditFeedVOs) {
			counter++;
			String priceMessage = mMessageServiceUtil.constructPriceMessage(intradayPriceAuditFeedVO, counter, mapSize);
			priceMessages.add(priceMessage);
		}

		if (isEnablePostMessageQueue() && priceMessages != null && !priceMessages.isEmpty()) {
			if (isLoggingDebug()) {
				vlogDebug("TRUIntradayPriceMessageService price Messages() : {0}" , priceMessages);
			}
			postMessageToExternalQueue(priceMessages, pSkuList, pJmsId, pMessageRecvTime, pErrorRepositoryId);
		}

		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRUIntradayPriceMessageService processMessagesToPost()");
		}

	}

	/**
	 * Retry posting error message.
	 * 
	 * @param pSkuList
	 *            the sku list
	 * @param pJmsId
	 *            the jms id
	 * @param pMessageRecvTime
	 *            the message recv time
	 * @param pErrorRepositoryId
	 *            the error repository id
	 * @param pIsQueueConnected
	 *            the is queue connected
	 */
	private void retryPostingErrorMessage(List<String> pSkuList, String pJmsId, Timestamp pMessageRecvTime, String pErrorRepositoryId, boolean pIsQueueConnected) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER:::TRUIntradayPriceMessageService retryPostingErrorMessage()");
		}
		if (StringUtils.isNotEmpty(pErrorRepositoryId) && pIsQueueConnected) {
			vlogDebug("TRUIntradayPriceMessageService :: Retry Scheduler - Message posted to external queue, remove entry from Error Repository ");
			getMessageServiceUtil().removeMessageFromErrorRepository(pErrorRepositoryId);
		}

		if (!pIsQueueConnected) {
			if (mCount <= mRetryCount) {
				vlogDebug("TRUIntradayPriceMessageService :: Retrying to send to external Queue : retry count : {0}", mCount);
				getIntradayPriceMessageSink().populateIntradayPriceVOwithDetails(pSkuList, pJmsId, pMessageRecvTime, pErrorRepositoryId);
			} else {
				mCount = 0;
				mLocalMessageJMSId = null;
				if (StringUtils.isEmpty(pErrorRepositoryId)) {
					vlogDebug("TRUIntradayPriceMessageService :: LocalQueue - Message posting failed, add message to Error Repository ");
					getMessageServiceUtil().addMessageToErrorRepository(pSkuList, pJmsId, pMessageRecvTime, pErrorRepositoryId);

				} else if (StringUtils.isNotEmpty(pErrorRepositoryId)) {
					vlogDebug("TRUIntradayPriceMessageService :: Retry Scheduler - Message posting failed, update the failure count to Error Repository ");
					boolean thresholdTimeReached = getMessageServiceUtil().isThresholdTimeReached(pMessageRecvTime);
					if (thresholdTimeReached) {
						getMessageServiceUtil().triggerErrorEmail(getTargetQueue());
					}
					getMessageServiceUtil().updateFailureCountToErrorRepository(pErrorRepositoryId);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT:::TRUIntradayPriceMessageService retryPostingErrorMessage()");
		}
	}

	/**
	 * Sets the counter and jms id.
	 * 
	 * @param pJmsId
	 *            the new counter and jms id
	 */
	public void setCounterAndJmsId(String pJmsId) {
		if (getLocalMessageJMSId() == null) {
			setLocalMessageJMSId(pJmsId);
			mCount++;
		} else if (StringUtils.isNotEmpty(getLocalMessageJMSId()) && getLocalMessageJMSId().equalsIgnoreCase(pJmsId)) {
			mCount++;
		} else if (StringUtils.isNotEmpty(getLocalMessageJMSId()) && !getLocalMessageJMSId().equalsIgnoreCase(pJmsId)) {
			mCount = TRUIntradayAuditConstants.INTEGER_NUMBER_ONE;
			setLocalMessageJMSId(pJmsId);
		}
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUIntradayAuditConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUIntradayAuditConstants.MESSAGE, pMessage);
		extenstions.put(TRUIntradayAuditConstants.START_TIME, pStartDate);
		extenstions.put(TRUIntradayAuditConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUIntradayAuditConstants.INTRADAY_QUEUE, TRUIntradayAuditConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUIntradayAuditConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUIntradayAuditConstants.MESSAGE, pMessage);
		extenstions.put(TRUIntradayAuditConstants.START_TIME, pStartDate);
		extenstions.put(TRUIntradayAuditConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUIntradayAuditConstants.INTRADAY_QUEUE, TRUIntradayAuditConstants.FEED_STATUS, null, extenstions);
	}

	/**
	 * This method is used to set enablePostMessageQueue
	 * 
	 * @param pEnablePostMessageQueue
	 *            boolean
	 */
	public void setEnablePostMessageQueue(boolean pEnablePostMessageQueue) {
		mEnablePostMessageQueue = pEnablePostMessageQueue;
	}

	/**
	 * Sets the enable retry message to queue.
	 *
	 * @param pEnableRetryMessageToQueue the new enable retry message to queue
	 */
	public void setEnableRetryMessageToQueue(boolean pEnableRetryMessageToQueue) {
		this.mEnableRetryMessageToQueue = pEnableRetryMessageToQueue;
	}

	/**
	 * This method is used to set epslonConfiguration
	 * 
	 * @param pEpslonConfiguration
	 *            TRUEpslonConfiguration
	 */
	public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
		mEpslonConfiguration = pEpslonConfiguration;
	}

	/**
	 * @param pIntradayPriceMessageSink
	 *            the mIntradayPriceMessageSink to set
	 */
	public void setIntradayPriceMessageSink(TRUIntradayPriceMessageSink pIntradayPriceMessageSink) {
		this.mIntradayPriceMessageSink = pIntradayPriceMessageSink;
	}

	/**
	 * Sets the jms x group id.
	 *
	 * @param pJmsXGroupID the new jms x group id
	 */
	public void setJmsXGroupID(String pJmsXGroupID) {
		this.mJmsXGroupID = pJmsXGroupID;
	}

	/**
	 * @param pLocalMessageJMSId
	 *            the mLocalMessageJMSId to set
	 */
	public void setLocalMessageJMSId(String pLocalMessageJMSId) {
		this.mLocalMessageJMSId = pLocalMessageJMSId;
	}

	/**
	 * Sets the message service util.
	 * 
	 * @param pMessageServiceUtil
	 *            the new message service util
	 */
	public void setMessageServiceUtil(TRUIntradayPriceMessageServiceUtil pMessageServiceUtil) {
		this.mMessageServiceUtil = pMessageServiceUtil;
	}

	/**
	 * @param pRetryCount
	 *            the mRetryCount to set
	 */
	public void setRetryCount(int pRetryCount) {
		this.mRetryCount = pRetryCount;
	}

	/**
	 * This method is used to set targetQueue
	 * 
	 * @param pTargetQueue
	 *            String
	 */
	public void setTargetQueue(String pTargetQueue) {
		mTargetQueue = pTargetQueue;
	}
	
	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

}
