/**
 * TRUPriceProperties.java
 */
package com.tru.integrations.intradaypriceaudit;

import atg.nucleus.GenericService;

/**
 * This class is to hold the Price properties.
 * @author PA.
 * @version 1.0.
 * 
 */
public class TRUIntradayPriceProperties extends GenericService {

	/** This property holds country code property name. */
	private String mCountryCodePropertyName;

	/** This property holds market code property name. */
	private String mMarketCodePropertyName;

	/** This property holds store number property name. */
	private String mStoreNumberPropertyName;

	/** This property holds our price property name. */
	private String mSalePricePropertyName;

	/** This property holds SKU Id property name. */
	private String mSKUIdPropertyName;

	/** This property holds List price property name. */
	private String mListPricePropertyName;

	/** This property holds sale price property name. */
	private String mLocalePropertyName;

	/** This property holds sale price property name. */
	private String mDisplayNamePropertyName;

	/** This property mPriceListPropertyName. */
	private String mPriceListPropertyName;

	/** The property mMessageBodyPropertyName. */
	private String mMessageBodyPropertyName;

	/** The property mLocalQueueJmsIdPropertyName. */
	private String mLocalQueueJmsIdPropertyName;

	/** The property mLocalQueueTimeStampPropertyName. */
	private String mLocalQueueTimeStampPropertyName;

	/** The property mFailureAttemptsPropertyName. */
	private String mFailureAttemptsPropertyName;

	/** The property mIntradayPriceErrorRepoName. */
	private String mIntradayPriceErrorRepoName;

	/** This property holds Deal Id property name. */
	private String mDealIdPropertyName;

	/**
	 * This method will return countryCodePropertyName.
	 * 
	 * @return the countryCodePropertyName
	 */
	public String getCountryCodePropertyName() {
		return mCountryCodePropertyName;
	}

	/**
	 * This method will set mcountryCodePropertyName with
	 * pCountryCodePropertyName
	 * 
	 * @param pCountryCodePropertyName
	 *            the mcountryCodePropertyName to set
	 */
	public void setCountryCodePropertyName(String pCountryCodePropertyName) {
		mCountryCodePropertyName = pCountryCodePropertyName;
	}

	/**
	 * This method will return marketCodePropertyName
	 * 
	 * @return the marketCodePropertyName
	 */
	public String getMarketCodePropertyName() {
		return mMarketCodePropertyName;
	}

	/**
	 * This method will set mmarketCodePropertyName with pMarketCodePropertyName
	 * 
	 * @param pMarketCodePropertyName
	 *            the mmarketCodePropertyName to set
	 */
	public void setMarketCodePropertyName(String pMarketCodePropertyName) {
		mMarketCodePropertyName = pMarketCodePropertyName;
	}

	/**
	 * This method will return storeNumberPropertyName
	 * 
	 * @return the storeNumberPropertyName
	 */
	public String getStoreNumberPropertyName() {
		return mStoreNumberPropertyName;
	}

	/**
	 * This method will set mstoreNumberPropertyName with
	 * pStoreNumberPropertyName
	 * 
	 * @param pStoreNumberPropertyName
	 *            the mstoreNumberPropertyName to set
	 */
	public void setStoreNumberPropertyName(String pStoreNumberPropertyName) {
		mStoreNumberPropertyName = pStoreNumberPropertyName;
	}

	/**
	 * Returns the listPricePropertyName
	 * 
	 * @return the listPricePropertyName
	 */
	public String getListPricePropertyName() {
		return mListPricePropertyName;
	}

	/**
	 * Sets/updates the listPricePropertyName
	 * 
	 * @param pListPricePropertyName
	 *            the listPricePropertyName to set
	 */
	public void setListPricePropertyName(String pListPricePropertyName) {
		mListPricePropertyName = pListPricePropertyName;
	}

	/**
	 * Returns the displayNamePropertyName
	 * 
	 * @return the displayNamePropertyName
	 */
	public String getDisplayNamePropertyName() {
		return mDisplayNamePropertyName;
	}

	/**
	 * Sets/updates the displayNamePropertyName
	 * 
	 * @param pDisplayNamePropertyName
	 *            the displayNamePropertyName to set
	 */
	public void setDisplayNamePropertyName(String pDisplayNamePropertyName) {
		mDisplayNamePropertyName = pDisplayNamePropertyName;
	}

	/**
	 * Returns the sKUIdPropertyName
	 * 
	 * @return the sKUIdPropertyName
	 */
	public String getSKUIdPropertyName() {
		return mSKUIdPropertyName;
	}

	/**
	 * Sets/updates the sKUIdPropertyName
	 * 
	 * @param pSKUIdPropertyName
	 *            the sKUIdPropertyName to set
	 */
	public void setSKUIdPropertyName(String pSKUIdPropertyName) {
		mSKUIdPropertyName = pSKUIdPropertyName;
	}

	/**
	 * Returns the salePricePropertyName
	 * 
	 * @return the salePricePropertyName
	 */
	public String getSalePricePropertyName() {
		return mSalePricePropertyName;
	}

	/**
	 * Sets/updates the salePricePropertyName
	 * 
	 * @param pSalePricePropertyName
	 *            the salePricePropertyName to set
	 */
	public void setSalePricePropertyName(String pSalePricePropertyName) {
		mSalePricePropertyName = pSalePricePropertyName;
	}

	/**
	 * @return the mPriceListPropertyName
	 */
	public String getPriceListPropertyName() {
		return mPriceListPropertyName;
	}

	/**
	 * @param pPriceListPropertyName
	 *            the mPriceListPropertyName to set
	 */
	public void setPriceListPropertyName(String pPriceListPropertyName) {
		this.mPriceListPropertyName = pPriceListPropertyName;
	}

	/**
	 * @return the mLocalePropertyName
	 */
	public String getLocalePropertyName() {
		return mLocalePropertyName;
	}

	/**
	 * @param pLocalePropertyName
	 *            the mLocalePropertyName to set
	 */
	public void setLocalePropertyName(String pLocalePropertyName) {
		this.mLocalePropertyName = pLocalePropertyName;
	}

	/**
	 * Gets the message body property name.
	 * 
	 * @return the message body property name
	 */
	public String getMessageBodyPropertyName() {
		return mMessageBodyPropertyName;
	}

	/**
	 * Sets the message body property name.
	 * 
	 * @param pMessageBodyPropertyName
	 *            the new message body property name
	 */
	public void setMessageBodyPropertyName(String pMessageBodyPropertyName) {
		this.mMessageBodyPropertyName = pMessageBodyPropertyName;
	}

	/**
	 * Gets the local queue jms id property name.
	 * 
	 * @return the local queue jms id property name
	 */
	public String getLocalQueueJmsIdPropertyName() {
		return mLocalQueueJmsIdPropertyName;
	}

	/**
	 * Sets the local queue jms id property name.
	 * 
	 * @param pLocalQueueJmsIdPropertyName
	 *            the new local queue jms id property name
	 */
	public void setLocalQueueJmsIdPropertyName(String pLocalQueueJmsIdPropertyName) {
		this.mLocalQueueJmsIdPropertyName = pLocalQueueJmsIdPropertyName;
	}

	/**
	 * Gets the local queue time stamp property name.
	 * 
	 * @return the local queue time stamp property name
	 */
	public String getLocalQueueTimeStampPropertyName() {
		return mLocalQueueTimeStampPropertyName;
	}

	/**
	 * Sets the local queue time stamp property name.
	 * 
	 * @param pLocalQueueTimeStampPropertyName
	 *            the new local queue time stamp property name
	 */
	public void setLocalQueueTimeStampPropertyName(String pLocalQueueTimeStampPropertyName) {
		this.mLocalQueueTimeStampPropertyName = pLocalQueueTimeStampPropertyName;
	}

	/**
	 * Gets the failure attempts property name.
	 * 
	 * @return the failure attempts property name
	 */
	public String getFailureAttemptsPropertyName() {
		return mFailureAttemptsPropertyName;
	}

	/**
	 * Sets the failure attempts property name.
	 * 
	 * @param pFailureAttemptsPropertyName
	 *            the new failure attempts property name
	 */
	public void setFailureAttemptsPropertyName(String pFailureAttemptsPropertyName) {
		this.mFailureAttemptsPropertyName = pFailureAttemptsPropertyName;
	}

	/**
	 * Gets the intraday price error repo name.
	 * 
	 * @return the intraday price error repo name
	 */
	public String getIntradayPriceErrorRepoName() {
		return mIntradayPriceErrorRepoName;
	}

	/**
	 * Sets the intraday price error repo name.
	 * 
	 * @param pIntradayPriceErrorRepoName
	 *            the new intraday price error repo name
	 */
	public void setIntradayPriceErrorRepoName(String pIntradayPriceErrorRepoName) {
		this.mIntradayPriceErrorRepoName = pIntradayPriceErrorRepoName;
	}

	/**
	 * Gets the deal id property name.
	 * 
	 * @return the mDealIdPropertyName
	 */
	public String getDealIdPropertyName() {
		return mDealIdPropertyName;
	}

	/**
	 * Sets the deal id property name.
	 * 
	 * @param pDealIdPropertyName
	 *            the new deal id property name
	 */
	public void setDealIdPropertyName(String pDealIdPropertyName) {
		this.mDealIdPropertyName = pDealIdPropertyName;
	}
}