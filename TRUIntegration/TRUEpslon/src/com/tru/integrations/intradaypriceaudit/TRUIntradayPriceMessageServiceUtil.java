package com.tru.integrations.intradaypriceaudit;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.DynamoEnv;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.integrations.intradaypriceaudit.email.TRUIntradayPriceEmailConfig;

/**
 * The Class is used to post message to TRU messaging queue.
 * @author PA.
 * @version 1.0.
 * 
 */
public class TRUIntradayPriceMessageServiceUtil extends GenericService {

	/**
	 * Holds the property value of mIntradayErrorRepository.
	 */
	private Repository mIntradayErrorRepository;

	/**
	 * Holds the property value of mThresholdTimeInHours.
	 */
	private int mThresholdTimeInHours;

	/**
	 * Holds the property value of mIntradayPriceProperties.
	 */
	private TRUIntradayPriceProperties mIntradayPriceProperties;

	/**
	 * Holds the property value of mPriceEmailConfig.
	 */
	private TRUIntradayPriceEmailConfig mPriceEmailConfig;

	/**
	 * Holds the property value of mErrorMessageinEmail.
	 */
	private String mErrorMessageinEmail;

	/**
	 * Adds the message to error repository.
	 * 
	 * @param pSkuList
	 *            the sku list
	 * @param pJmsId
	 *            the jms id
	 * @param pMessageRecvTime
	 *            the message recv time
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */
	public void addMessageToErrorRepository(List<String> pSkuList, String pJmsId, Timestamp pMessageRecvTime, String pErrorRepositoryId) {

		MutableRepository mutableinvRepository = null;
		MutableRepositoryItem intradayPriceErrorItem = null;
		if (StringUtils.isEmpty(pErrorRepositoryId)) {
			try {
				mutableinvRepository = (MutableRepository) getIntradayErrorRepository();
				intradayPriceErrorItem = mutableinvRepository.createItem(getIntradayPriceProperties().getIntradayPriceErrorRepoName());

				byte[] encodeToByteArray = convertStringListToByteArray(pSkuList);

				intradayPriceErrorItem.setPropertyValue(getIntradayPriceProperties().getMessageBodyPropertyName(), encodeToByteArray);
				intradayPriceErrorItem.setPropertyValue(getIntradayPriceProperties().getLocalQueueJmsIdPropertyName(), pJmsId);
				intradayPriceErrorItem.setPropertyValue(getIntradayPriceProperties().getLocalQueueTimeStampPropertyName(), pMessageRecvTime);
				intradayPriceErrorItem.setPropertyValue(getIntradayPriceProperties().getFailureAttemptsPropertyName(),
						TRUIntradayAuditConstants.INTEGER_NUMBER_ZERO);
				mutableinvRepository.addItem(intradayPriceErrorItem);

			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(" Repository exception while fetching intradayPriceError :: Debug :: itemDescName = ", e);
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError(" IO exception while converting skuList to byte array ", e);
				}
			}
		}

	}

	/**
	 * Clear email info.
	 *
	 * @param pFeedEmailInfo the feed email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pFeedEmailInfo) {
		vlogDebug("Begin:@Class: TRUFeedEmailService : @Method: clearEmailInfo()");
		pFeedEmailInfo.setTemplateParameters(null);
		pFeedEmailInfo.setMessageAttachments(null);
		vlogDebug("End:@Class: TRUFeedEmailService : @Method: clearEmailInfo()");
	}

	/**
	 * Construct error message.
	 *
	 * @param pActualTimeStampDate the actual time stamp date
	 * @param pLaterTime the later time
	 */
	private void constructErrorMessage(Timestamp pActualTimeStampDate, Timestamp pLaterTime) {

		long diff = pActualTimeStampDate.getTime() - pLaterTime.getTime();
		long minutes = diff / (TRUIntradayAuditConstants.INT_SIXTY * TRUIntradayAuditConstants.INT_THOUSAND) % TRUIntradayAuditConstants.INT_SIXTY;
		long hours = diff / (TRUIntradayAuditConstants.INT_SIXTY * TRUIntradayAuditConstants.INT_SIXTY * TRUIntradayAuditConstants.INT_THOUSAND) %  
				TRUIntradayAuditConstants.INT_TWENTY_FOUR;
		long days = diff / 
				(TRUIntradayAuditConstants.INT_TWENTY_FOUR * TRUIntradayAuditConstants.INT_SIXTY * TRUIntradayAuditConstants.INT_SIXTY * TRUIntradayAuditConstants.INT_THOUSAND);

		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(TRUIntradayAuditConstants.ERROR_MESSAGE_INITIAL);
		if (days != 0) {
			stringBuffer.append(String.valueOf(days)).append(TRUIntradayAuditConstants.ERROR_MESSAGE_DAYS);
		}
		if (hours != 0) {
			stringBuffer.append(String.valueOf(hours)).append(TRUIntradayAuditConstants.ERROR_MESSAGE_HOURS);
		}

		stringBuffer.append(minutes).append(TRUIntradayAuditConstants.ERROR_MESSAGE_MINUTES).append(String.valueOf(pActualTimeStampDate))
				.append(TRUIntradayAuditConstants.STRING_TO).append(String.valueOf(pLaterTime));
		mErrorMessageinEmail = stringBuffer.toString();
	}

	/**
	 * Construct message from auditFeedMap.
	 * 
	 * @param pTruIntradayPriceAuditFeedVO
	 *            the tru intraday price audit feed vo
	 * @param pCounter
	 *            the counter
	 * @param pMapSize
	 *            the map size
	 * @return the string
	 */
	public String constructPriceMessage(TRUIntradayPriceAuditFeedVO pTruIntradayPriceAuditFeedVO, int pCounter, int pMapSize) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER:::TRUIntradayPriceMessageService constructPriceMessage()");
		}
		StringBuffer message = new StringBuffer();
		if (pCounter == TRUIntradayAuditConstants.INTEGER_NUMBER_ONE) {
			message.append(TRUIntradayAuditConstants.START_HEADER).append(TRUIntradayAuditConstants.NEW_LINE_STRING);
		}
		message.append(pTruIntradayPriceAuditFeedVO.getSkuId()).append(TRUIntradayAuditConstants.PIPE_DELIMITER);
		if (StringUtils.isNotEmpty(pTruIntradayPriceAuditFeedVO.getListPrice())) {
			message.append(pTruIntradayPriceAuditFeedVO.getListPrice());
		}
		message.append(TRUIntradayAuditConstants.PIPE_DELIMITER);
		if (StringUtils.isNotEmpty(pTruIntradayPriceAuditFeedVO.getSalePrice())) {
			message.append(pTruIntradayPriceAuditFeedVO.getSalePrice());
		}
		message.append(TRUIntradayAuditConstants.PIPE_DELIMITER).append(pTruIntradayPriceAuditFeedVO.getLocale())
				.append(TRUIntradayAuditConstants.PIPE_DELIMITER).append(pTruIntradayPriceAuditFeedVO.getCountryCode())
				.append(TRUIntradayAuditConstants.PIPE_DELIMITER).append(pTruIntradayPriceAuditFeedVO.getMarketCode())
				.append(TRUIntradayAuditConstants.PIPE_DELIMITER).append(pTruIntradayPriceAuditFeedVO.getStoreNumber())
				.append(TRUIntradayAuditConstants.PIPE_DELIMITER);
		if (StringUtils.isNotEmpty(pTruIntradayPriceAuditFeedVO.getDealId())) {
			message.append(pTruIntradayPriceAuditFeedVO.getDealId());
		}
		if (pCounter == pMapSize) {
			message.append(TRUIntradayAuditConstants.NEW_LINE_STRING).append(TRUIntradayAuditConstants.END_TRAILER);
		}
		if (isLoggingDebug()) {
			vlogDebug("END:::TRUIntradayPriceMessageService constructPriceMessage()" + message.toString());
		}
		return message.toString();
	}

	/**
	 * Convert string list to byte array.
	 * 
	 * @param pSkuList
	 *            the sku list
	 * @return the byte[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private byte[] convertStringListToByteArray(List<String> pSkuList) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(os);
		for (String element : pSkuList) {
			out.writeUTF(element);
		}
		byte[] encodeToByteArray = os.toByteArray();
		out.close();
		os.close();
		return encodeToByteArray;
	}

	/**
	 * Gets the error messagein email.
	 * 
	 * @return the error messagein email
	 */
	public String getErrorMessageinEmail() {
		return mErrorMessageinEmail;
	}

	/**
	 * Gets the intraday error repository.
	 * 
	 * @return the intraday error repository
	 */
	public Repository getIntradayErrorRepository() {
		return mIntradayErrorRepository;
	}

	/**
	 * Gets the intraday price properties.
	 * 
	 * @return the intraday price properties
	 */
	public TRUIntradayPriceProperties getIntradayPriceProperties() {
		return mIntradayPriceProperties;
	}
	
	/**
	 * Gets the price email config.
	 * 
	 * @return the price email config
	 */
	public TRUIntradayPriceEmailConfig getPriceEmailConfig() {
		return mPriceEmailConfig;
	}

	/**
	 * Gets the threshold time in hours.
	 * 
	 * @return the threshold time in hours
	 */
	public int getThresholdTimeInHours() {
		return mThresholdTimeInHours;
	}

	/**
	 * Checks if is threshold time reached.
	 * 
	 * @param pMessageReceivedTime
	 *            the message received time
	 * @return true, if is threshold time reached
	 */
	public boolean isThresholdTimeReached(Timestamp pMessageReceivedTime) {
		Timestamp actualTimeStampDate = null;
		Timestamp laterTime = null;
		Calendar cal = Calendar.getInstance();
		boolean thresholdReached = false;

		if (pMessageReceivedTime != null) {
			cal.setTime(pMessageReceivedTime);
			cal.add(Calendar.HOUR, getThresholdTimeInHours());
			laterTime = new Timestamp(cal.getTime().getTime());
			Date actual = new Date();
			actualTimeStampDate = new Timestamp(actual.getTime());
			thresholdReached = (laterTime.getTime() < actualTimeStampDate.getTime());
			constructErrorMessage(actualTimeStampDate, laterTime);
			if (isLoggingDebug()) {
				logDebug("Message received time + 1hour : " + laterTime + ", actual system time : " + actualTimeStampDate + " thresholdReached : " + 
						thresholdReached);
			}
		}
		return thresholdReached;
	}

	/**
	 * Removes the message from error repository.
	 * 
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */
	public void removeMessageFromErrorRepository(String pErrorRepositoryId) {
		MutableRepository mutableinvRepository = null;
		try {
			mutableinvRepository = (MutableRepository) getIntradayErrorRepository();
			mutableinvRepository.removeItem(pErrorRepositoryId, getIntradayPriceProperties().getIntradayPriceErrorRepoName());
		} catch (RepositoryException exp) {
			if (isLoggingError()) {
				logError(" Repository exception while removing repository intradayPriceError ::: = ", exp);
			}
		}
	}

	/**
	 * Sets the error messagein email.
	 * 
	 * @param pErrorMessageinEmail
	 *            the new error messagein email
	 */
	public void setErrorMessageinEmail(String pErrorMessageinEmail) {
		this.mErrorMessageinEmail = pErrorMessageinEmail;
	}

	/**
	 * Sets the intraday error repository.
	 * 
	 * @param pIntradayErrorRepository
	 *            the new intraday error repository
	 */
	public void setIntradayErrorRepository(Repository pIntradayErrorRepository) {
		this.mIntradayErrorRepository = pIntradayErrorRepository;
	}

	/**
	 * Sets the intraday price properties.
	 * 
	 * @param pIntradayPriceProperties
	 *            the new intraday price properties
	 */
	public void setIntradayPriceProperties(TRUIntradayPriceProperties pIntradayPriceProperties) {
		this.mIntradayPriceProperties = pIntradayPriceProperties;
	}

	/**
	 * Sets the price email config.
	 * 
	 * @param pPriceEmailConfig
	 *            the new price email config
	 */
	public void setPriceEmailConfig(TRUIntradayPriceEmailConfig pPriceEmailConfig) {
		this.mPriceEmailConfig = pPriceEmailConfig;
	}

	/**
	 * Sets the threshold time in hours.
	 * 
	 * @param pThresholdTimeInHours
	 *            the new threshold time in hours
	 */
	public void setThresholdTimeInHours(int pThresholdTimeInHours) {
		this.mThresholdTimeInHours = pThresholdTimeInHours;
	}

	/**
	 * Trigger error email.
	 * 
	 * @param pTargetQueue
	 *            the target queue
	 */
	public void triggerErrorEmail(String pTargetQueue) {
		Map<String, String> templateParameter = new HashMap<String, String>();
		
		TemplateEmailInfoImpl feedEmailInfo = getPriceEmailConfig().getTemplateEmailInfo();
		TemplateEmailSender emailSender = getPriceEmailConfig().getTemplateEmailSender();
		feedEmailInfo.setTemplateURL(getPriceEmailConfig().getTemplateURL());
		feedEmailInfo.setSiteId(getPriceEmailConfig().getSiteId());
		templateParameter.put(TRUIntradayAuditConstants.MESSAGE_SUBJECT, feedEmailInfo.getMessageSubject());
		templateParameter.put(TRUIntradayAuditConstants.SITE_ID, getPriceEmailConfig().getSiteId());
		templateParameter.put(TRUIntradayAuditConstants.REPORT, getPriceEmailConfig().getMailReportSymptom());
		templateParameter.put(TRUIntradayAuditConstants.PRIORITY, getPriceEmailConfig().getMailPriority2());
		templateParameter.put(TRUIntradayAuditConstants.QUEUE, pTargetQueue);
		templateParameter.put(TRUIntradayAuditConstants.NOTES, getPriceEmailConfig().getMailNotes());
		templateParameter.put(TRUIntradayAuditConstants.SOFTWARE, getPriceEmailConfig().getMailSoftware());
		templateParameter.put(TRUIntradayAuditConstants.OSNAME, System.getProperty(TRUIntradayAuditConstants.OS_NAME_STRING));
		templateParameter.put(TRUIntradayAuditConstants.JAVA_VERSION, System.getProperty(TRUIntradayAuditConstants.JAVA_VERSION_STRING));
		String serverName = DynamoEnv.getProperty(TRUIntradayAuditConstants.SERVER_PATH);
		
		templateParameter.put(TRUIntradayAuditConstants.OS_HOST_NAME, serverName);
		templateParameter.put(TRUIntradayAuditConstants.ERRORDETAIL, mErrorMessageinEmail);
		
		clearEmailInfo(feedEmailInfo);
		
		if (feedEmailInfo.getTemplateParameters() != null) {
			feedEmailInfo.getTemplateParameters().putAll(templateParameter);
		} else {
			feedEmailInfo.setTemplateParameters(templateParameter);
		}
		List<String> recipientList = getPriceEmailConfig().getReceiverEmailId();
		try {
			emailSender.sendEmailMessage(feedEmailInfo, recipientList);
		} catch (TemplateEmailException e) {
			if (isLoggingError()) {
				logError(e.getMessage(), e);
			}
		}
	}

	/**
	 * Update failure count to error repository.
	 * 
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */
	public void updateFailureCountToErrorRepository(String pErrorRepositoryId) {
		MutableRepository mutableErrorRepository = null;
		MutableRepositoryItem errorRepoItem = null;
		int failureAttemptCount = 0;
		try {
			mutableErrorRepository = (MutableRepository) getIntradayErrorRepository();
			errorRepoItem = (MutableRepositoryItem) mutableErrorRepository.getItem(pErrorRepositoryId, getIntradayPriceProperties()
					.getIntradayPriceErrorRepoName());
			if (errorRepoItem != null && errorRepoItem.getPropertyValue(getIntradayPriceProperties().getFailureAttemptsPropertyName()) != null) {
				failureAttemptCount = (int) errorRepoItem.getPropertyValue(getIntradayPriceProperties().getFailureAttemptsPropertyName());
				failureAttemptCount++;
			}
			errorRepoItem.setPropertyValue(getIntradayPriceProperties().getFailureAttemptsPropertyName(), failureAttemptCount);
			mutableErrorRepository.updateItem(errorRepoItem);
		} catch (RepositoryException exp) {
			if (isLoggingError()) {
				logError(" Repository exception while updating repository intradayPriceError ::: = ", exp);
			}
		}
	}
}
