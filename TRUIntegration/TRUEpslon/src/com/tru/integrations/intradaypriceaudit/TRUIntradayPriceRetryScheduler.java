package com.tru.integrations.intradaypriceaudit;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.messaging.TRUIntradayPriceMessageSink;

/**
 * This class is TRUIntradayPriceRetryScheduler.
 * 
 * @author PA
 * @version 1.0.
 * 
 */
public class TRUIntradayPriceRetryScheduler extends SingletonSchedulableService {

	/**
	 * Holds the property value of mIntradayPriceMessageSink.
	 */
	private TRUIntradayPriceMessageSink mIntradayPriceMessageSink;

	/**
	 * Holds the property value of mIntradayErrorRepository.
	 */
	private Repository mIntradayErrorRepository;

	/**
	 * Holds the property value of mEnable.
	 */
	private boolean mEnable;

	/**
	 * Holds the property value of mIntradayPriceProperties.
	 */
	private TRUIntradayPriceProperties mIntradayPriceProperties;

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time</b><br>
	 * .
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: PriceAuditFeed.doScheduledTask()");
		}
		if (isEnable()) {
			fetchAllIntradayPriceErrorRepos();
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: PriceAuditFeed.doScheduledTask()");
		}
	}

	/**
	 * Fetch all intraday price error repos.
	 */
	public void fetchAllIntradayPriceErrorRepos() {
		RepositoryItemDescriptor repoItemDesc;
		Timestamp localQueueReceivedTime = null;
		String jmsId = null;
		List<String> skuIdsList = null;
		String repositoryId = null;
		try {

			repoItemDesc = getIntradayErrorRepository().getItemDescriptor(getIntradayPriceProperties().getIntradayPriceErrorRepoName());
			RepositoryView repoView = repoItemDesc.getRepositoryView();
			QueryBuilder queryBuilder = repoView.getQueryBuilder();
			Query errorQuery = queryBuilder.createUnconstrainedQuery();
			RepositoryItem[] repoItems = repoView.executeQuery(errorQuery);
			if (repoItems != null && repoItems.length > 0) {
				for (RepositoryItem repositoryItem : repoItems) {
					byte[] decodeFromByteArray = (byte[]) repositoryItem.getPropertyValue(getIntradayPriceProperties().getMessageBodyPropertyName());

					skuIdsList = decodeMessageEvent(decodeFromByteArray);
					jmsId = (String) repositoryItem.getPropertyValue(getIntradayPriceProperties().getLocalQueueJmsIdPropertyName());
					localQueueReceivedTime = (Timestamp) repositoryItem.getPropertyValue(getIntradayPriceProperties().getLocalQueueTimeStampPropertyName());
					repositoryId = repositoryItem.getRepositoryId();
					if (isLoggingDebug()) {
						logDebug("MessageBody : " + repositoryItem.getPropertyValue(getIntradayPriceProperties().getMessageBodyPropertyName()));
						logDebug("Local Jms Id : " + repositoryItem.getPropertyValue(getIntradayPriceProperties().getLocalQueueJmsIdPropertyName()));
						logDebug("Local Message Received Time : " + repositoryItem.getPropertyValue(getIntradayPriceProperties().getLocalQueueTimeStampPropertyName()));
					}
					if (skuIdsList != null && !skuIdsList.isEmpty()) {
						mIntradayPriceMessageSink.populateIntradayPriceVOwithDetails(skuIdsList, jmsId, localQueueReceivedTime, repositoryId);
					}
				}
			}
		} catch (RepositoryException e) {
			vlogError("RepositoryException while fetching price message error records :  ", e);
		} catch (IOException e) {
			vlogError("IOException while decoding the price message from byte array : ", e);
		}
	}

	/**
	 * Decode message event.
	 * 
	 * @param pDecodeFromByteArray
	 *            the decode from byte array
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private List<String> decodeMessageEvent(byte[] pDecodeFromByteArray) throws IOException {
		// read from byte array
		ByteArrayInputStream byteStream = new ByteArrayInputStream(pDecodeFromByteArray);
		List<String> skuIds = new ArrayList<String>();
		DataInputStream dataInputStream = new DataInputStream(byteStream);
		while (dataInputStream.available() > 0) {
			String element = dataInputStream.readUTF();
			skuIds.add(element);
		}
		return skuIds;
	}

	/**
	 * Gets the intraday price message sink.
	 * 
	 * @return the intraday price message sink
	 */
	public TRUIntradayPriceMessageSink getIntradayPriceMessageSink() {
		return mIntradayPriceMessageSink;
	}

	/**
	 * Sets the intraday price message sink.
	 * 
	 * @param pIntradayPriceMessageSink
	 *            the new intraday price message sink
	 */
	public void setIntradayPriceMessageSink(TRUIntradayPriceMessageSink pIntradayPriceMessageSink) {
		this.mIntradayPriceMessageSink = pIntradayPriceMessageSink;
	}

	/**
	 * Gets the intraday error repository.
	 * 
	 * @return the intraday error repository
	 */
	public Repository getIntradayErrorRepository() {
		return mIntradayErrorRepository;
	}

	/**
	 * Sets the intraday error repository.
	 * 
	 * @param pIntradayErrorRepository
	 *            the new intraday error repository
	 */
	public void setIntradayErrorRepository(Repository pIntradayErrorRepository) {
		this.mIntradayErrorRepository = pIntradayErrorRepository;
	}

	/**
	 * Checks if is enable.
	 * 
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * Gets the intraday price properties.
	 * 
	 * @return the intraday price properties
	 */
	public TRUIntradayPriceProperties getIntradayPriceProperties() {
		return mIntradayPriceProperties;
	}

	/**
	 * Sets the intraday price properties.
	 * 
	 * @param pIntradayPriceProperties
	 *            the new intraday price properties
	 */
	public void setIntradayPriceProperties(TRUIntradayPriceProperties pIntradayPriceProperties) {
		this.mIntradayPriceProperties = pIntradayPriceProperties;
	}
}
