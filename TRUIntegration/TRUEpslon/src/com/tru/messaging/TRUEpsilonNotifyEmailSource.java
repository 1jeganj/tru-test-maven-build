package com.tru.messaging;



import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integrations.common.TRUEpslonConfiguration;
import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;

/**This class perform the functionality of setting the request for notify email event.
 * @author PA.
 * @version 1.0.
 */
public class TRUEpsilonNotifyEmailSource extends GenericService  implements MessageSource {
	
	/**
	 * Holds the property value of epslonConfiguration.
	 */
	private TRUEpslonConfiguration mEpslonConfiguration;

	/**
	 * Holds the property value of mEpslonAckMessageType.
	 */
	private String mEpslonAckMessageType;
	
	/**
	 * Holds the property value of mEpslonBackMessageType.
	 */
	private String mEpslonBackMessageType;
			
	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mPortName.
	 */
	private String mPortName;
	
	/**
	 * Holds the property value of mJmsId.
	 */
	private String mJmsId;
	
	/**
	 * Holds the property value of mContext.
	 */
	private MessageSourceContext mContext;
	/**
	 * Holds the property value of mStarted.
	 */
	private boolean mStarted;
	
	/**
	 * @return the context
	 */
	public MessageSourceContext getContext() {
		return mContext;
	}

	/**
	 * This method is used to get email.
	 * @return email String
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @return the mEpslonAckMessageType
	 */
	public String getEpslonAckMessageType() {
		return mEpslonAckMessageType;
	}

	/**
	 * @return the mEpslonBackMessageType
	 */
	public String getEpslonBackMessageType() {
		return mEpslonBackMessageType;
	}

	/**
	 * This method is used to get epslonBrand.
	 * @return epslonBrand String
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}

	/**
	 * This method is used to get epslonCompanyID.
	 * @return epslonCompanyID String
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}

	/**
	 * This method is used to get epslonConfiguration.
	 * @return epslonConfiguration TRUEpslonConfiguration
	 */
	public TRUEpslonConfiguration getEpslonConfiguration() {
		return mEpslonConfiguration;
	}

	/**
	 * This method is used to get epslonHeaderTypeDestination.
	 * @return epslonHeaderTypeDestination String
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}

	/**
	 * This method is used to get epslonHeaderTypeInternalDateTimeStamp.
	 * @return epslonHeaderTypeInternalDateTimeStamp String
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgLocale.
	 * @return epslonHeaderTypeMsgLocale String
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgTimeZone.
	 * @return epslonHeaderTypeMsgTimeZone String
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}

	/**
	 * This method is used to get epslonMessageType.
	 * @return epslonMessageType String
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}

	/**
	 * This method is used to get epslonReferenceID.
	 * @return epslonReferenceID String
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}

	/**
	 * This method is used to get epslonSource.
	 * @return epslonSource String
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}

	/**
	 * This method is used to get jmsId.
	 * @return jmsId String
	 */
	public String getJmsId() {
		return mJmsId;
	}

	/**
	    * 
	    * @return mContext
	    */
		public MessageSourceContext getMessageSourceContext() {
			return mContext;
		}

	/**
	 * This method is used to get portName.
	 * @return portName String
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * This method is called in TRUEpslonNotifyEmailSource to receive the message and send in destination to be able to be received by queue.
	 * @param pItemDescription - String
	 * @param pItemName - String
	 * @param pItemNumber - String
	 * @param pItemURL - String
	 * @param pHelpURLName - String
	 * @param pHelpURLValue - String
	 * @param pEmail - String
	 * @param pNotifyEmailFlag - String
	 * @throws JMSException - JMSException
	 */
	public void sendNotificationEmail(String pItemDescription,String pItemName,String pItemNumber,String pItemURL,String pHelpURLName,String pHelpURLValue,String pEmail,String pNotifyEmailFlag)
			throws JMSException {
		
		if (isLoggingDebug()) {
			logDebug("START: TRUEpsilonNotifyEmailSource sendNotificationEmail() ");
		}
		
		
		ObjectMessage objectMessage;
		
		TRUEpslonMessageBean epslonObjMsg = new TRUEpslonMessageBean();
		
		if(!StringUtils.isBlank(pEmail)){
			epslonObjMsg.setEmail(pEmail);}
		
		if(!StringUtils.isBlank(getEpslonBrand())){
			epslonObjMsg.setEpslonBrand(getEpslonBrand());}
		if(!StringUtils.isBlank(getEpslonSource())){
			epslonObjMsg.setEpslonSource(getEpslonSource());}
		if(!StringUtils.isBlank(getEpslonReferenceID())){
			epslonObjMsg.setEpslonReferenceID(getEpslonReferenceID());}
		if(!StringUtils.isBlank(getEpslonCompanyID())){
			epslonObjMsg.setEpslonCompanyID(getEpslonCompanyID());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeDestination())){
			epslonObjMsg.setEpslonHeaderTypeDestination(getEpslonHeaderTypeDestination());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeInternalDateTimeStamp())){
			epslonObjMsg.setEpslonHeaderTypeInternalDateTimeStamp(getEpslonHeaderTypeInternalDateTimeStamp());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgTimeZone())){
			epslonObjMsg.setEpslonHeaderTypeMsgTimeZone(getEpslonHeaderTypeMsgTimeZone());}
		if(!StringUtils.isBlank(pNotifyEmailFlag))
		{
			if(pNotifyEmailFlag.equals(TRUEpslonConstants.ACKNOWLEDGEMENT)){
				epslonObjMsg.setEpslonMessageType(getEpslonAckMessageType());
			}else{
				epslonObjMsg.setEpslonMessageType(getEpslonBackMessageType());
			}
		}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgLocale())){
			epslonObjMsg.setEpslonHeaderTypeMsgLocale(getEpslonHeaderTypeMsgLocale());}
		if(!StringUtils.isBlank(pItemName)){
		         epslonObjMsg.setItemDescription(pItemName);}
		if(!StringUtils.isBlank(pItemName)){
		          epslonObjMsg.setItemName(pItemName);}
		if(!StringUtils.isBlank(pItemNumber)){
		         epslonObjMsg.setItemNumber(pItemNumber);}
		if(!StringUtils.isBlank(pItemURL)){
		          epslonObjMsg.setItemURL(pItemURL);}
		if(!StringUtils.isBlank(pHelpURLName)){
		          epslonObjMsg.setHelpURLName(pHelpURLName);}
		if(!StringUtils.isBlank(pHelpURLValue)){
		         epslonObjMsg.setHelpURValue(pHelpURLValue);}
		if(!StringUtils.isBlank(pNotifyEmailFlag)){
	         epslonObjMsg.setNotifyEmailFlag(pNotifyEmailFlag);}
		
		try {
			if (getMessageSourceContext() != null) {
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				
				///objectMessage.setJMSMessageID(getJmsId());

				objectMessage.setObject(epslonObjMsg);

				if (isLoggingDebug()) {
					logDebug("objectMessage in sendNotificationEmail() Method...." + objectMessage);
				}
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				if (isLoggingDebug()) {
					logDebug("messageSourceContext...." + mContext);
				}
				
			}

		} catch (JMSException jmse) {
			if (isLoggingError()) {
				logError("JMS Exception in order Notify Me When Available Messaging::", jmse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUEpsilonNotifyEmailSource sendNotificationEmail()");
		}
	}

	/**
	 * @param pContext the context to set
	 */
	public void setContext(MessageSourceContext pContext) {
		mContext = pContext;
	}

	/**
	 *This method is used to set email.
	 *@param pEmail String
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * @param pEpslonAckMessageType the epslonAckMessageType to set
	 */
	public void setEpslonAckMessageType(String pEpslonAckMessageType) {
		this.mEpslonAckMessageType = pEpslonAckMessageType;
	}

	/**
	 * @param pEpslonBackMessageType the epslonBackMessageType to set
	 */
	public void setEpslonBackMessageType(String pEpslonBackMessageType) {
		this.mEpslonBackMessageType = pEpslonBackMessageType;
	}

	/**
	 *This method is used to set epslonBrand.
	 *@param pEpslonBrand String
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}

	/**
	 *This method is used to set epslonCompanyID.
	 *@param pEpslonCompanyID String
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}
	
   /**
 *This method is used to set epslonConfiguration.
 *@param pEpslonConfiguration TRUEpslonConfiguration
 */
public void setEpslonConfiguration(TRUEpslonConfiguration pEpslonConfiguration) {
	mEpslonConfiguration = pEpslonConfiguration;
}

	/**
	 *This method is used to set epslonHeaderTypeDestination.
	 *@param pEpslonHeaderTypeDestination String
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}
	/**
	 *This method is used to set epslonHeaderTypeInternalDateTimeStamp.
	 *@param pEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}
	
	/**
	 *This method is used to set epslonHeaderTypeMsgLocale.
	 *@param pEpslonHeaderTypeMsgLocale String
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}

	/**
	 *This method is used to set epslonHeaderTypeMsgTimeZone.
	 *@param pEpslonHeaderTypeMsgTimeZone String
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}
	 
	/**
	 *This method is used to set epslonMessageType.
	 *@param pEpslonMessageType String
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}

	/**
	 *This method is used to set epslonReferenceID.
	 *@param pEpslonReferenceID String
	 */
	public void setEpslonReferenceID(String pEpslonReferenceID) {
		mEpslonReferenceID = pEpslonReferenceID;
	}

	/**
	 *This method is used to set epslonSource.
	 *@param pEpslonSource String
	 */
	public void setEpslonSource(String pEpslonSource) {
		mEpslonSource = pEpslonSource;
	}

	/**
	 *This method is used to set jmsId.
	 *@param pJmsId String
	 */
	public void setJmsId(String pJmsId) {
		mJmsId = pJmsId;
	}

	@Override
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}

	/**
	 *This method is used to set portName.
	 *@param pPortName String
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * @param pStarted the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}

	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

}
