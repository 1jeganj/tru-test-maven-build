package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.updateemail.TRUEpslonEmailUpdateService;

/**
 * This class performs the functionality of receiving the messages from.
 * changeEmailQueue for send gift card activation email event.
 *
 * @version 1.0
 * @author Professional Access.
 */

public class TRUEpslonChangeEmailSink extends GenericService implements
		MessageSink {

	/**
	 * Holds the property value of mEpslonEmailUpdateService.
	 */
	private TRUEpslonEmailUpdateService mEpslonEmailUpdateService;
	
	/**
	 * Holds the property value of mEpslonChangeEmailSource.
	 */
	private TRUEpslonChangeEmailSource mEpslonChangeEmailSource;
	
	
	/**
	 * This method is used to get epslonChangeEmailSource.
	 * @return mEpslonChangeEmailSource TRUEpslonChangeEmailSource
	 */
	public TRUEpslonChangeEmailSource getEpslonChangeEmailSource() {
		return mEpslonChangeEmailSource;
	}
	/**
	 * This method is used to get epslonEmailUpdateService.
	 * @return mEpslonEmailUpdateService TRUEpslonEmailUpdateService
	 */
	
	public TRUEpslonEmailUpdateService getEpslonEmailUpdateService() {
		return mEpslonEmailUpdateService;
	}

	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
				String finalMessageID = TRUEpslonConstants.EMPTY_STRING ;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getEpslonChangeEmailSource() != null){
             finalMessageID = getEpslonChangeEmailSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}

	/**
	 * This method performs the functionality of receiving the messages from  giftCardOrderQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			vlogDebug("ENTER::::message received for TRUEpslonChangeEmailSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
			String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
			epslonMessageBean.setEpslonReferenceID(jmsId);
			}else{
				vlogDebug("JMSMessageId is null",pParamMessage.getJMSMessageID());
			}
		}
		if(getEpslonEmailUpdateService() != null){
	getEpslonEmailUpdateService().sendEmailUpdateSuccessEmail(epslonMessageBean);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::message received for TRUEpslonChangeEmailSink . in receiveMessage");
		}

	}
	
	/**
	 *This method is used to set epslonChangeEmailSource.
	 *@param pEpslonChangeEmailSource TRUEpslonChangeEmailSource
	 */
	public void setEpslonChangeEmailSource(
			TRUEpslonChangeEmailSource pEpslonChangeEmailSource) {
		mEpslonChangeEmailSource = pEpslonChangeEmailSource;
	}
	
	/**
	 *This method is used to set epslonEmailUpdateService.
	 *@param pEpslonEmailUpdateService TRUEpslonEmailUpdateService
	 */

	public void setEpslonEmailUpdateService(
			TRUEpslonEmailUpdateService pEpslonEmailUpdateService) {
		mEpslonEmailUpdateService = pEpslonEmailUpdateService;
	}
	
}
