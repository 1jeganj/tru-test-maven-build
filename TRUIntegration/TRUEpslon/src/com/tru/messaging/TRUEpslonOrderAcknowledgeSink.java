package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.order.TRUEpslonOrderAcknowledgeService;

/**
 * This class performs the functionality of receiving the messages from.
 * accountCreationQueue for send gift card activation email event
 * @author Professional access
 * @version 1.0
 */

public class TRUEpslonOrderAcknowledgeSink extends GenericService implements MessageSink {
	 
	 /**
	 * Holds the property value of mOrderAcknowledgeSource.
	 */
	private TRUEpslonOrderAcknowledgeSource mOrderAcknowledgeSource;
	
	 /**
	 * Holds the property value of mOrderAcknowledgeService.
	 */
	private TRUEpslonOrderAcknowledgeService mOrderAcknowledgeService;
	
	/**
	 * This method is used to get orderAcknowledgeSource.
	 * @return orderAcknowledgeSource TRUEpslonOrderAcknowledgeSource
	 */
	public TRUEpslonOrderAcknowledgeSource getOrderAcknowledgeSource() {
		return mOrderAcknowledgeSource;
	}

	/**
	 *This method is used to set orderAcknowledgeSource.
	 *@param pOrderAcknowledgeSource TRUEpslonOrderAcknowledgeSource
	 */
	public void setOrderAcknowledgeSource(
			TRUEpslonOrderAcknowledgeSource pOrderAcknowledgeSource) {
		mOrderAcknowledgeSource = pOrderAcknowledgeSource;
	}

	/**
	 * This method is used to get orderAcknowledgeService.
	 * @return orderAcknowledgeService TRUEpslonOrderAcknowledgeService
	 */
	public TRUEpslonOrderAcknowledgeService getOrderAcknowledgeService() {
		return mOrderAcknowledgeService;
	}

	/**
	 *This method is used to set orderAcknowledgeService.
	 *@param pOrderAcknowledgeService TRUEpslonOrderAcknowledgeService
	 */
	public void setOrderAcknowledgeService(
			TRUEpslonOrderAcknowledgeService pOrderAcknowledgeService) {
		mOrderAcknowledgeService = pOrderAcknowledgeService;
	}

	/**
	 * This method performs the functionality of receiving the messages from orderAcknowledgementQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			logDebug("ENTER: TRUEpslonOrderAcknowledgeSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
				String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
				epslonMessageBean.setEpslonReferenceID(jmsId);
				}else{
					if (isLoggingDebug()) {
					logDebug("JMSMessageId is null"+pParamMessage.getJMSMessageID());
					}
				}
		}
		getOrderAcknowledgeService().generateOrderAckRequest(epslonMessageBean);
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUEpslonOrderAcknowledgeSink . in receiveMessage");
		}

	}
	
	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING ;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getOrderAcknowledgeSource() != null){
             finalMessageID = getOrderAcknowledgeSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}
	
}
