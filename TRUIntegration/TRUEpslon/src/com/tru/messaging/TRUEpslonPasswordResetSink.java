package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.reset.TRUEpslonPasswordResetService;

/**
 * This class performs the functionality of receiving the messages from.
 * passwordResetQueue for send gift card activation email event.
 * @author Professional Access
 * @version 1.0
 */

public class TRUEpslonPasswordResetSink extends GenericService implements MessageSink{

	/**
	 * Holds the property value of mEpslonPasswordResetSource.
	 */
	private TRUEpslonPasswordResetSource mEpslonPasswordResetSource;
	
	
	/**
	 * Holds the property value of mPasswordResetService.
	 */
	private TRUEpslonPasswordResetService mPasswordResetService;
	
	

	/**
	 * @return the mPasswordResetService
	 */
	public TRUEpslonPasswordResetService getPasswordResetService() {
		return mPasswordResetService;
	}



	/**
	 * @param pPasswordResetService the passwordResetService to set
	 */
	public void setPasswordResetService(TRUEpslonPasswordResetService pPasswordResetService) {
		mPasswordResetService = pPasswordResetService;
	}

	
	/**
	 * @return the epslonPasswordResetSource
	 */
	public TRUEpslonPasswordResetSource getEpslonPasswordResetSource() {
		return mEpslonPasswordResetSource;
	}



	/**
	 * @param pEpslonPasswordResetSource the epslonPasswordResetSource to set
	 */
	public void setEpslonPasswordResetSource(
			TRUEpslonPasswordResetSource pEpslonPasswordResetSource) {
		mEpslonPasswordResetSource = pEpslonPasswordResetSource;
	}



	/**
	 * This method performs the functionality of receiving the messages from giftCardOrderQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			vlogDebug("ENTER::::message received for TRUEpslonPasswordResetSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
			String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
			epslonMessageBean.setEpslonReferenceID(jmsId);
			}else{
				vlogDebug("JMSMessageId is null",pParamMessage.getJMSMessageID());
			}
		}
		if(getPasswordResetService() != null){
		getPasswordResetService().resetPasswordEmail(epslonMessageBean);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::message received for TRUEpslonPasswordResetSink . in receiveMessage");
		}

	}
	
	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getEpslonPasswordResetSource() != null){
             finalMessageID = getEpslonPasswordResetSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}
	
}
