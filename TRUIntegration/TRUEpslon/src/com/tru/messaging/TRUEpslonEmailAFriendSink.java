package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.emailafriend.TRUEpslonEmailAFriendService;

/**
 * This class performs the functionality of receiving the messages from
 * emailAFriendQueue for send gift card activation email event.
 * @author Professional access
 * @version 1.0
 */

public class TRUEpslonEmailAFriendSink extends GenericService implements MessageSink {

	 
	/**
	 * Holds the property value of mEmailAFriendSource.
	 */
	private TRUEpslonEmailAFriendSource mEmailAFriendSource;
	
	/**
	 * Holds the property value of mEmailAFriendService.
	 */
	private TRUEpslonEmailAFriendService mEmailAFriendService;
	

	/**
	 * This method is used to get emailAFriendService.
	 * @return emailAFriendService TRUEpslonEmailAFriendService
	 */
	public TRUEpslonEmailAFriendService getEmailAFriendService() {
		return mEmailAFriendService;
	}

	/**
	 * This method is used to get emailAFriendSource.
	 * @return emailAFriendSource TRUEpslonEmailAFriendSource
	 */
	public TRUEpslonEmailAFriendSource getEmailAFriendSource() {
		return mEmailAFriendSource;
	}

	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING ;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getEmailAFriendSource() != null){
             finalMessageID = getEmailAFriendSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}

	/**
	 * This method performs the functionality of receiving the messages from emailAFriendQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			logDebug("ENTER: TRUEpslonAccountSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
				String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
				epslonMessageBean.setEpslonReferenceID(jmsId);
				}else{
					vlogDebug("JMSMessageId is null",pParamMessage.getJMSMessageID());
				}
		}
		getEmailAFriendService().generateEmailAFriendRequest(epslonMessageBean);
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUEpslonAccountSink . in receiveMessage");
		}

	}

	/**
	 *This method is used to set emailAFriendService.
	 *@param pEmailAFriendService TRUEpslonEmailAFriendService
	 */
	public void setEmailAFriendService(
			TRUEpslonEmailAFriendService pEmailAFriendService) {
		mEmailAFriendService = pEmailAFriendService;
	}
	
	/**
	 *This method is used to set emailAFriendSource.
	 *@param pEmailAFriendSource TRUEpslonEmailAFriendSource
	 */
	public void setEmailAFriendSource(
			TRUEpslonEmailAFriendSource pEmailAFriendSource) {
		mEmailAFriendSource = pEmailAFriendSource;
	}
	
}
