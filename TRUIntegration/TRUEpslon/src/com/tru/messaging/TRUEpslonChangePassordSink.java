package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.reset.TRUEpslonPasswordResetService;

/**
 * This class performs the functionality of receiving the messages from changePassword for send gift card activation email event.
 *
 * @version 1.0.
 * @author Professional Access.
 */

public class TRUEpslonChangePassordSink extends GenericService implements
		MessageSink {
	/**
	 * Holds the property value of mEpslonChangePasswordSource.
	 */
	private TRUEpslonChangePasswordSource mEpslonChangePasswordSource;
	
	
	/**
	 * Holds the property value of mPasswordResetService.
	 */
	private TRUEpslonPasswordResetService mPasswordResetService;
	
	/**
	 * This method is used to get epslonChangePasswordSource.
	 * @return mEpslonChangePasswordSource TRUEpslonChangePasswordSource
	 */
	public TRUEpslonChangePasswordSource getEpslonChangePasswordSource() {
		return mEpslonChangePasswordSource;
	}

	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getEpslonChangePasswordSource() != null){
             finalMessageID = getEpslonChangePasswordSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}

	/**
	 * This method is used to get passwordResetService.
	 * @return mPasswordResetService TRUEpslonPasswordResetService
	 */
	public TRUEpslonPasswordResetService getPasswordResetService() {
		return mPasswordResetService;
	}

	/**
	 * This method performs the functionality of receiving the messages from giftCardOrderQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			vlogDebug("ENTER::::message received for TRUEpslonChangePassordSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
			String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
			epslonMessageBean.setEpslonReferenceID(jmsId);
			}else{
				vlogDebug("JMSMessageId is null",pParamMessage.getJMSMessageID());
			}
		}
		if(getPasswordResetService() != null){
		getPasswordResetService().resetPasswordSuccessEmail(epslonMessageBean);
		}
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::message received for TRUEpslonChangePassordSink . in receiveMessage");
		}

	}

	/**
	 *This method is used to set epslonChangePasswordSource.
	 *@param pEpslonChangePasswordSource TRUEpslonChangePasswordSource
	 */
	public void setEpslonChangePasswordSource(
			TRUEpslonChangePasswordSource pEpslonChangePasswordSource) {
		mEpslonChangePasswordSource = pEpslonChangePasswordSource;
	}
	
	/**
	 *This method is used to set passwordResetService.
	 *@param pPasswordResetService TRUEpslonPasswordResetService
	 */
	public void setPasswordResetService(
			TRUEpslonPasswordResetService pPasswordResetService) {
		mPasswordResetService = pPasswordResetService;
	}
	
}
