package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.TRUEpslonService;

/**
 * This class performs the functionality of receiving the messages from accountCreationQueue for send gift card activation email event.
 * 
 * @version 1.0.
 * @author Professional Access.
 */

public class TRUEpslonAccountSink extends GenericService implements MessageSink {
	 
	/**
	 * Holds the property value of mEpslonAccountSource.
	 */
	private TRUEpslonAccountSource mEpslonAccountSource;
	
	/**
	 * Holds the property value of mTRUEpslonService.
	 */
	private TRUEpslonService mTRUEpslonService;

	/**
	 * This method is used to get epslonAccountSource.
	 * @return mEpslonAccountSource TRUEpslonAccountSource
	 */
	public TRUEpslonAccountSource getEpslonAccountSource() {
		return mEpslonAccountSource;
	}

	/**
	 *This method is used to set epslonAccountSource.
	 *@param pEpslonAccountSource TRUEpslonAccountSource
	 */
	public void setEpslonAccountSource(TRUEpslonAccountSource pEpslonAccountSource) {
		mEpslonAccountSource = pEpslonAccountSource;
	}

	/**
	 * This method is used to get tRUEpslonService.
	 * @return mTRUEpslonService TRUEpslonService
	 */
	public TRUEpslonService getTRUEpslonService() {
		return mTRUEpslonService;
	}

	/**
	 *This method is used to set tRUEpslonService.
	 *@param pTRUEpslonService TRUEpslonService
	 */
	public void setTRUEpslonService(TRUEpslonService pTRUEpslonService) {
		mTRUEpslonService = pTRUEpslonService;
	}

	/**
	 * This method performs the functionality of receiving the messages from giftCardOrderQueue.
	 * @param pParamString - String
	 * @param pParamMessage - String
	 * @throws JMSException - JMSException
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			logDebug("ENTER: TRUEpslonAccountSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
				String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
				epslonMessageBean.setEpslonReferenceID(jmsId);
				}else{
					vlogDebug("JMSMessageId is null",pParamMessage.getJMSMessageID());
				}
		}
		getTRUEpslonService().generateEpslonRequest(epslonMessageBean);
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUEpslonAccountSink . in receiveMessage");
		}

	}
	
	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String
	 * @return String
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING ;
		vlogDebug("JMS Message ID in getJMSMessageID):: {0}" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getEpslonAccountSource() != null){
             finalMessageID = getEpslonAccountSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}
	
}
