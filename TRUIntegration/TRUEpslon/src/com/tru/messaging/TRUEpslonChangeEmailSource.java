package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integrations.epslon.TRUEpslonMessageBean;

/**
 * This class is having the method to set the email template like header and msg  once the user changed his mail address.
 * 
 * @author Professional Access
 * @version 1.0
 */

public class TRUEpslonChangeEmailSource extends GenericService  implements MessageSource {

	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mPortName.
	 */
	private String mPortName;
	
	/**
	 * Holds the property value of mJmsId.
	 */
	private String mJmsId;
	
	/**
	 * @return the jmsId
	 */
	public String getJmsId() {
		return mJmsId;
	}
	/**
	 * @param pJmsId the jmsId to set
	 */
	public void setJmsId(String pJmsId) {
		mJmsId = pJmsId;
	}
	/**
	 * @return the portName
	 */
	public String getPortName() {
		return mPortName;
	}
	/**
	 * @param pPortName the portName to set
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return mEmail;
	}
	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	/**
	 * @return the epslonSource
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}
	/**
	 * @param pEpslonSource the epslonSource to set
	 */
	public void setEpslonSource(String pEpslonSource) {
		mEpslonSource = pEpslonSource;
	}
	/**
	 * @return the epslonReferenceID
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}
	/**
	 * @param pEpslonReferenceID the epslonReferenceID to set
	 */
	public void setEpslonReferenceID(String pEpslonReferenceID) {
		mEpslonReferenceID = pEpslonReferenceID;
	}
	/**
	 * @return the epslonMessageType
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}
	/**
	 * @param pEpslonMessageType the epslonMessageType to set
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}
	/**
	 * @return the epslonBrand
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}
	/**
	 * @param pEpslonBrand the epslonBrand to set
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}
	/**
	 * @return the epslonCompanyID
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}
	/**
	 * @param pEpslonCompanyID the epslonCompanyID to set
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}
	/**
	 * @return the epslonHeaderTypeMsgLocale
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}
	/**
	 * @param pEpslonHeaderTypeMsgLocale the epslonHeaderTypeMsgLocale to set
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}
	/**
	 * @return the epslonHeaderTypeMsgTimeZone
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}
	/**
	 * @param pEpslonHeaderTypeMsgTimeZone the epslonHeaderTypeMsgTimeZone to set
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}
	/**
	 * @return the epslonHeaderTypeDestination
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}
	/**
	 * @param pEpslonHeaderTypeDestination the epslonHeaderTypeDestination to set
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}
	/**
	 * @return the epslonHeaderTypeInternalDateTimeStamp
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}
	/**
	 * @param pEpslonHeaderTypeInternalDateTimeStamp the epslonHeaderTypeInternalDateTimeStamp to set
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}
	
	/**
	 *  property to hold mContext.
	 */
	private MessageSourceContext mContext;
	
	/**
	 *  property to hold mStarted.
	 */
	private boolean mStarted;

	/**
	 * @param pContext the mContext to set
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}
	
   /**
    * 
    * @return mContext
    */
	public MessageSourceContext getMessageSourceContext() {
		return mContext;
	}

/**
 * @return the context
 */
public MessageSourceContext getContext() {
	return mContext;
}
/**
 * @param pContext the context to set
 */
public void setContext(MessageSourceContext pContext) {
	mContext = pContext;
}
	/**
	 * This method is called in TRUEpslonAccountSource to receive the message and send in destination to be able to be received by queue.
	 * @param pOldEmail - String
	 * @param pUpdatedEmail - String
	 * @param pMyAccountURL - String
	 * @throws JMSException - JMSException
	 */
	public void sendEmailOnEmailChange(String pOldEmail,String pUpdatedEmail,String pMyAccountURL) throws JMSException {
		
		if (isLoggingDebug()) {
			logDebug("START: TRUEpslonChangeEmailSource sendEmailOnEmailChange() ");
		}
		
		
		ObjectMessage objectMessage;
		
		TRUEpslonMessageBean epslonObjMsg = new TRUEpslonMessageBean();
		if(!StringUtils.isBlank(pOldEmail)){
			epslonObjMsg.setOldEmailAddress(pOldEmail);
		}
		
		if(!StringUtils.isBlank(pUpdatedEmail)){
			epslonObjMsg.setUpdatedEmailAddress(pUpdatedEmail);
		}
		if(!StringUtils.isBlank(getEpslonBrand())){
			epslonObjMsg.setEpslonBrand(getEpslonBrand());}
		if(!StringUtils.isBlank(getEpslonSource())){
			epslonObjMsg.setEpslonSource(getEpslonSource());}
		if(!StringUtils.isBlank(getEpslonReferenceID())){
			epslonObjMsg.setEpslonReferenceID(getEpslonReferenceID());}
		if(!StringUtils.isBlank(getEpslonCompanyID())){
			epslonObjMsg.setEpslonCompanyID(getEpslonCompanyID());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeDestination())){
			epslonObjMsg.setEpslonHeaderTypeDestination(getEpslonHeaderTypeDestination());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeInternalDateTimeStamp())){
			epslonObjMsg.setEpslonHeaderTypeInternalDateTimeStamp(getEpslonHeaderTypeInternalDateTimeStamp());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgTimeZone())){
			epslonObjMsg.setEpslonHeaderTypeMsgTimeZone(getEpslonHeaderTypeMsgTimeZone());}
		if(!StringUtils.isBlank(getEpslonMessageType())){
			epslonObjMsg.setEpslonMessageType(getEpslonMessageType());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgLocale())){
			epslonObjMsg.setEpslonHeaderTypeMsgLocale(getEpslonHeaderTypeMsgLocale());}
		
		try {
			if (getMessageSourceContext() != null) {
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				///objectMessage.setJMSMessageID(getJmsId());

				objectMessage.setObject(epslonObjMsg);

				if (isLoggingDebug()) {
					logDebug("objectMessage in sendEmailOnEmailChange() Method...." + objectMessage);
				}
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				//getEpslonEmailUpdateService().sendEmailUpdateSuccessEmail(epslonObjMsg);
				if (isLoggingDebug()) {
					logDebug("messageSourceContext...." + mContext);
				}
			}

		} catch (JMSException jmse) {
			if (isLoggingError()) {
				logError("JMS Exception in Epslon change email Messaging::", jmse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUEpslonChangeEmailSource sendEmailOnEmailChange()");
		}
	}

	/**
	 * Overriding startMessageSource method.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}

	/**
	 * Overriding stopMessageSource method.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * @param pStarted the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	
}
