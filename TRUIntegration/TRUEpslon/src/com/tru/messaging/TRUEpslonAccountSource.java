package com.tru.messaging;



import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integrations.epslon.TRUEpslonMessageBean;

/**This class perform the functionality of setting the request for myaccount activation email event.
 * 
 * @version 1.0
 * @author Professional Access.
 *
 */
public class TRUEpslonAccountSource extends GenericService  implements MessageSource {

	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mPortName.
	 */
	private String mPortName;
	
	/**
	 * Holds the property value of mJmsId.
	 */
	private String mJmsId;
	
	private MessageSourceContext mContext;
	private boolean mStarted;
	/**
	 * @return the context.
	 */
	public MessageSourceContext getContext() {
		return mContext;
	}
	/**
	 * @return the email.
	 */
	public String getEmail() {
		return mEmail;
	}
	/**
	 * @return the epslonBrand.
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}
	/**
	 * @return the epslonCompanyID.
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}
	/**
	 * @return the epslonHeaderTypeDestination.
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}
	/**
	 * @return the epslonHeaderTypeInternalDateTimeStamp.
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}
	/**
	 * @return the epslonHeaderTypeMsgLocale.
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}
	/**
	 * @return the epslonHeaderTypeMsgTimeZone.
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}
	/**
	 * @return the epslonMessageType.
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}
	/**
	 * @return the epslonReferenceID.
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}
	/**
	 * @return the epslonSource.
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}
	/**
	 * @return the jmsId.
	 */
	public String getJmsId() {
		return mJmsId;
	}
	/**
	    * 
	    * @return mContext.
	    */
		public MessageSourceContext getMessageSourceContext() {
			return mContext;
		}
	/**
	 * @return the portName.
	 */
	public String getPortName() {
		return mPortName;
	}
	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}
	/**
	 * This method is called in TRUEpslonAccountSource to receive the message and send in destination to be able to be received by queue.
	 * @param pEmail - String
	 * @throws JMSException - JMSException
	 */
	public void sendNewAccountEmail(String pEmail)
			throws JMSException {
		
		if (isLoggingDebug()) {
			logDebug("START: TRUEpslonAccountSource sendNewAccountEmail() ");
		}
		
		
		ObjectMessage objectMessage;
		
		TRUEpslonMessageBean epslonObjMsg = new TRUEpslonMessageBean();
		if(!StringUtils.isBlank(pEmail)){
			epslonObjMsg.setEmail(pEmail);
		}else{
			if (isLoggingDebug()) {
				logDebug("Email is Empty" + pEmail);
			}
		}
		if(!StringUtils.isBlank(getEpslonBrand())){
			epslonObjMsg.setEpslonBrand(getEpslonBrand());}
		if(!StringUtils.isBlank(getEpslonSource())){
			epslonObjMsg.setEpslonSource(getEpslonSource());}
		if(!StringUtils.isBlank(getEpslonReferenceID())){
			epslonObjMsg.setEpslonReferenceID(getEpslonReferenceID());}
		if(!StringUtils.isBlank(getEpslonCompanyID())){
			epslonObjMsg.setEpslonCompanyID(getEpslonCompanyID());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeDestination())){
			epslonObjMsg.setEpslonHeaderTypeDestination(getEpslonHeaderTypeDestination());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeInternalDateTimeStamp())){
			epslonObjMsg.setEpslonHeaderTypeInternalDateTimeStamp(getEpslonHeaderTypeInternalDateTimeStamp());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgTimeZone())){
			epslonObjMsg.setEpslonHeaderTypeMsgTimeZone(getEpslonHeaderTypeMsgTimeZone());}
		if(!StringUtils.isBlank(getEpslonMessageType())){
			epslonObjMsg.setEpslonMessageType(getEpslonMessageType());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgLocale())){
			epslonObjMsg.setEpslonHeaderTypeMsgLocale(getEpslonHeaderTypeMsgLocale());}
		
		
		
		try {
			if (getMessageSourceContext() != null) {
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				
				///objectMessage.setJMSMessageID(getJmsId());

				objectMessage.setObject(epslonObjMsg);

				if (isLoggingDebug()) {
					logDebug("objectMessage in sendNewAccountEmail() Method...." + objectMessage);
				}
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				if (isLoggingDebug()) {
					logDebug("messageSourceContext...." + mContext);
				}
			}

		} catch (JMSException jmse) {
			if (isLoggingError()) {
				logError("JMS Exception in Epslon account creation Messaging::", jmse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUEpslonAccountSource sendNewAccountEmail()");
		}
	}
	/**
	 * @param pContext the context to set.
	 */
	public void setContext(MessageSourceContext pContext) {
		mContext = pContext;
	}
	/**
	 * @param pEmail the email to set.
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}
	/**
	 * @param pEpslonBrand the epslonBrand to set.
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}
	/**
	 * @param pEpslonCompanyID the epslonCompanyID to set.
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}
	/**
	 * @param pEpslonHeaderTypeDestination the epslonHeaderTypeDestination to set.
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}
	/**
	 * @param pEpslonHeaderTypeInternalDateTimeStamp the epslonHeaderTypeInternalDateTimeStamp to set.
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}
	
	/**
	 * @param pEpslonHeaderTypeMsgLocale the epslonHeaderTypeMsgLocale to set.
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}
	/**
	 * @param pEpslonHeaderTypeMsgTimeZone the epslonHeaderTypeMsgTimeZone to set.
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}

	
	/**
	 * @param pEpslonMessageType the epslonMessageType to set.
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}
	
   /**
 * @param pEpslonReferenceID the epslonReferenceID to set.
 */
public void setEpslonReferenceID(String pEpslonReferenceID) {
	mEpslonReferenceID = pEpslonReferenceID;
}

/**
 * @param pEpslonSource the epslonSource to set.
 */
public void setEpslonSource(String pEpslonSource) {
	mEpslonSource = pEpslonSource;
}
/**
 * @param pJmsId the jmsId to set.
 */
public void setJmsId(String pJmsId) {
	mJmsId = pJmsId;
}
	/**
	 * @param pContext the mContext to set.
	 */
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}

	/**
	 * @param pPortName the portName to set.
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * @param pStarted the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}

	/** 
	 * This method startMessageSource to log.
	 */
	@Override
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}

	/** 
	 * This method stopMessageSource to log.
	 */
	@Override
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

}
