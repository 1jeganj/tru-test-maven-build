package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

import com.tru.integrations.epslon.TRUEpslonMessageBean;

/**
 * This class is used for setting the message template for the email Once the user has successfully reset his password.
 * @version 1.0
 * @author Professional Access
 */

public class TRUEpslonPasswordResetSuccessSource extends GenericService  implements MessageSource{
	
	
	/**
	 * Holds the property value of mEmail.
	 */
	private String mEmail;
	
	/**
	 * Holds the property value of mEpslonSource.
	 */
	private String mEpslonSource;
	/**
	 * Holds the property value of mEpslonReferenceID.
	 */
	private String mEpslonReferenceID;
	/**
	 * Holds the property value of mEpslonMessageType.
	 */
	private String mEpslonMessageType;
	/**
	 * Holds the property value of mEpslonBrand.
	 */
	private String mEpslonBrand;
	/**
	 * Holds the property value of mEpslonCompanyID.
	 */
	private String mEpslonCompanyID;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgLocale.
	 */
	private String mEpslonHeaderTypeMsgLocale;
	/**
	 * Holds the property value of mEpslonHeaderTypeMsgTimeZone.
	 */
	private String mEpslonHeaderTypeMsgTimeZone;
	/**
	 * Holds the property value of mEpslonHeaderTypeDestination.
	 */
	private String mEpslonHeaderTypeDestination;
	/**
	 * Holds the property value of mEpslonHeaderTypeInternalDateTimeStamp.
	 */
	private String mEpslonHeaderTypeInternalDateTimeStamp;
	
	/**
	 * Holds the property value of mPortName.
	 */
	private String mPortName;
	
	/**
	 * Holds the property value of mJmsId.
	 */
	private String mJmsId;
	/**
	 * Holds the property value of mContext.
	 */
	
	private MessageSourceContext mContext;
	/**
	 * Holds the property value of mStarted.
	 */
	boolean mStarted;

	
	/**
	 * This method is used to get email.
	 * @return mEmail String
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 *This method is used to set email.
	 *@param pEmail String
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * This method is used to get epslonSource.
	 * @return mEpslonSource String
	 */
	public String getEpslonSource() {
		return mEpslonSource;
	}

	/**
	 *This method is used to set epslonSource.
	 *@param pEpslonSource String
	 */
	public void setEpslonSource(String pEpslonSource) {
		mEpslonSource = pEpslonSource;
	}

	/**
	 * This method is used to get epslonReferenceID.
	 * @return mEpslonReferenceID String
	 */
	public String getEpslonReferenceID() {
		return mEpslonReferenceID;
	}

	/**
	 *This method is used to set epslonReferenceID.
	 *@param pEpslonReferenceID String
	 */
	public void setEpslonReferenceID(String pEpslonReferenceID) {
		mEpslonReferenceID = pEpslonReferenceID;
	}

	/**
	 * This method is used to get epslonMessageType.
	 * @return mEpslonMessageType String
	 */
	public String getEpslonMessageType() {
		return mEpslonMessageType;
	}

	/**
	 *This method is used to set epslonMessageType.
	 *@param pEpslonMessageType String
	 */
	public void setEpslonMessageType(String pEpslonMessageType) {
		mEpslonMessageType = pEpslonMessageType;
	}

	/**
	 * This method is used to get epslonBrand.
	 * @return mEpslonBrand String
	 */
	public String getEpslonBrand() {
		return mEpslonBrand;
	}

	/**
	 *This method is used to set epslonBrand.
	 *@param pEpslonBrand String
	 */
	public void setEpslonBrand(String pEpslonBrand) {
		mEpslonBrand = pEpslonBrand;
	}

	/**
	 * This method is used to get epslonCompanyID.
	 * @return mEpslonCompanyID String
	 */
	public String getEpslonCompanyID() {
		return mEpslonCompanyID;
	}

	/**
	 *This method is used to set epslonCompanyID.
	 *@param pEpslonCompanyID String
	 */
	public void setEpslonCompanyID(String pEpslonCompanyID) {
		mEpslonCompanyID = pEpslonCompanyID;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgLocale.
	 * @return mEpslonHeaderTypeMsgLocale String
	 */
	public String getEpslonHeaderTypeMsgLocale() {
		return mEpslonHeaderTypeMsgLocale;
	}

	/**
	 *This method is used to set epslonHeaderTypeMsgLocale.
	 *@param pEpslonHeaderTypeMsgLocale String
	 */
	public void setEpslonHeaderTypeMsgLocale(String pEpslonHeaderTypeMsgLocale) {
		mEpslonHeaderTypeMsgLocale = pEpslonHeaderTypeMsgLocale;
	}

	/**
	 * This method is used to get epslonHeaderTypeMsgTimeZone.
	 * @return mEpslonHeaderTypeMsgTimeZone String
	 */
	public String getEpslonHeaderTypeMsgTimeZone() {
		return mEpslonHeaderTypeMsgTimeZone;
	}

	/**
	 *This method is used to set epslonHeaderTypeMsgTimeZone.
	 *@param pEpslonHeaderTypeMsgTimeZone String
	 */
	public void setEpslonHeaderTypeMsgTimeZone(String pEpslonHeaderTypeMsgTimeZone) {
		mEpslonHeaderTypeMsgTimeZone = pEpslonHeaderTypeMsgTimeZone;
	}

	/**
	 * This method is used to get epslonHeaderTypeDestination.
	 * @return mEpslonHeaderTypeDestination String
	 */
	public String getEpslonHeaderTypeDestination() {
		return mEpslonHeaderTypeDestination;
	}

	/**
	 *This method is used to set epslonHeaderTypeDestination.
	 *@param pEpslonHeaderTypeDestination String
	 */
	public void setEpslonHeaderTypeDestination(String pEpslonHeaderTypeDestination) {
		mEpslonHeaderTypeDestination = pEpslonHeaderTypeDestination;
	}

	/**
	 * This method is used to get epslonHeaderTypeInternalDateTimeStamp.
	 * @return mEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public String getEpslonHeaderTypeInternalDateTimeStamp() {
		return mEpslonHeaderTypeInternalDateTimeStamp;
	}

	/**
	 *This method is used to set epslonHeaderTypeInternalDateTimeStamp.
	 *@param pEpslonHeaderTypeInternalDateTimeStamp String
	 */
	public void setEpslonHeaderTypeInternalDateTimeStamp(
			String pEpslonHeaderTypeInternalDateTimeStamp) {
		mEpslonHeaderTypeInternalDateTimeStamp = pEpslonHeaderTypeInternalDateTimeStamp;
	}

	/**
	 * This method is used to get portName.
	 * @return mPortName String
	 */
	public String getPortName() {
		return mPortName;
	}

	/**
	 *This method is used to set portName.
	 *@param pPortName String
	 */
	public void setPortName(String pPortName) {
		mPortName = pPortName;
	}

	/**
	 * This method is used to get jmsId.
	 * @return mJmsId String
	 */
	public String getJmsId() {
		return mJmsId;
	}

	/**
	 *This method is used to set jmsId.
	 *@param pJmsId String
	 */
	public void setJmsId(String pJmsId) {
		mJmsId = pJmsId;
	}

	@Override
	/**
	 *This method is used to set source context.
	 *@param pmContex
	 */
	
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}
	
   /**
    * 
    * @return mContext
    */
	public MessageSourceContext getMessageSourceContext() {
		return mContext;
	}
	/**
	 * @return the context
	 */
	public MessageSourceContext getContext() {
		return mContext;
	}
	/**
	 * @param pContext the context to set
	 */
public void setContext(MessageSourceContext pContext) {
	mContext = pContext;
}
	/**
	 * This method is called in TRUEpslonPasswordResetSuccessSource to receive the message and send in destination to be able to be received by queue.
	 * @param pEmail - String
	 * @param pMyAccountURL - String
	 * @throws JMSException - JMSException
	 */
	public void sendPasswordResetSuccessEmail(String pEmail, String pMyAccountURL) throws JMSException {
		
		if (isLoggingDebug()) {
			logDebug("START: TRUEpslonPasswordResetSuccessSource sendPasswordResetSuccessEmail() ");
		}
		
		
		ObjectMessage objectMessage;
		
		TRUEpslonMessageBean epslonObjMsg = new TRUEpslonMessageBean();
		if(!StringUtils.isBlank(pEmail)){
			epslonObjMsg.setEmail(pEmail);
		}
		if(!StringUtils.isBlank(getEpslonBrand())){
			epslonObjMsg.setEpslonBrand(getEpslonBrand());}
		if(!StringUtils.isBlank(getEpslonSource())){
			epslonObjMsg.setEpslonSource(getEpslonSource());}
		if(!StringUtils.isBlank(getEpslonReferenceID())){
			epslonObjMsg.setEpslonReferenceID(getEpslonReferenceID());}
		if(!StringUtils.isBlank(getEpslonCompanyID())){
			epslonObjMsg.setEpslonCompanyID(getEpslonCompanyID());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeDestination())){
			epslonObjMsg.setEpslonHeaderTypeDestination(getEpslonHeaderTypeDestination());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeInternalDateTimeStamp())){
			epslonObjMsg.setEpslonHeaderTypeInternalDateTimeStamp(getEpslonHeaderTypeInternalDateTimeStamp());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgTimeZone())){
			epslonObjMsg.setEpslonHeaderTypeMsgTimeZone(getEpslonHeaderTypeMsgTimeZone());}
		if(!StringUtils.isBlank(getEpslonMessageType())){
			epslonObjMsg.setEpslonMessageType(getEpslonMessageType());}
		if(!StringUtils.isBlank(getEpslonHeaderTypeMsgLocale())){
			epslonObjMsg.setEpslonHeaderTypeMsgLocale(getEpslonHeaderTypeMsgLocale());}
		if(!StringUtils.isBlank(pMyAccountURL)){
			epslonObjMsg.setMyAccountURL(pMyAccountURL);}
		
		try {
			if (getMessageSourceContext() != null) {
				objectMessage = getMessageSourceContext().createObjectMessage(getPortName());
				///objectMessage.setJMSMessageID(getJmsId());

				objectMessage.setObject(epslonObjMsg);

				if (isLoggingDebug()) {
					logDebug("objectMessage in sendPasswordResetSuccessEmail() Method...." + objectMessage);
				}
				getMessageSourceContext().sendMessage(getPortName(), objectMessage);
				if (isLoggingDebug()) {
					logDebug("messageSourceContext...." + mContext);
				}
			}

		} catch (JMSException jmse) {
			if (isLoggingError()) {
				logError("JMS Exception in Epslon reset password Success Messaging::", jmse);
			}
		}
		if (isLoggingDebug()) {
			logDebug("END: TRUEpslonPasswordResetSuccessSource sendPasswordResetSuccessEmail()");
		}
	}

	
	/**
	 *This method is used to start message source context.
	 *@param mStarted
	 */	
	public void startMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = true;
		vlogDebug("Inside startMessageSource Method");
	}

	
	/**
	 *This method is used to stop message source context.
	 *@param mStarted
	 */	
	public void stopMessageSource() {
		//To Fix PMD Violation UncommentedEmptyMethod
		mStarted = false;
		vlogDebug("Inside stopMessageSource Method");
	}

	/**
	 * @return the mStarted
	 */
	public boolean isStarted() {
		return mStarted;
	}

	/**
	 * @param pStarted the mStarted to set
	 */
	public void setStarted(boolean pStarted) {
		mStarted = pStarted;
	}


}
