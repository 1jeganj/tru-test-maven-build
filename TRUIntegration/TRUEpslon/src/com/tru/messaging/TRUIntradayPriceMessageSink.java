package com.tru.messaging;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;

import com.tru.integrations.intradaypriceaudit.TRUIntradayAuditConstants;
import com.tru.integrations.intradaypriceaudit.TRUIntradayPriceAuditFeedVO;
import com.tru.integrations.intradaypriceaudit.TRUIntradayPriceMessageService;
import com.tru.integrations.intradaypriceaudit.TRUIntradayPriceProperties;

/**
 * The Class receives message that are posted in local queue.
 * 
 */
public class TRUIntradayPriceMessageSink extends GenericService implements MessageSink {

	/** The m intraday price message service. */
	private TRUIntradayPriceMessageService mIntradayPriceMessageService;

	private List<String> mSkuList;

	private String mSiteId;

	/** The Site context manager. */
	private SiteContextManager mSiteContextManager;

	/** This holds the Default PriceList Property Name **/
	private String mDefaultPriceListPropertyName;

	/** This holds the Default Sale PriceList Property Name **/
	private String mDefaultSalePriceListPropertyName;

	/** The Price list manager. */
	private PriceListManager mPriceListManager;

	/** The Intraday price properties. */
	private TRUIntradayPriceProperties mIntradayPriceProperties;

	/** The m local jms id. */
	private String mLocalJmsId;
	
	/** The m current date. */
	private CurrentDate mCurrentDate;

	/**
	 * This method is used to receive price audit message from local queue.
	 * 
	 * @param pParamString
	 *            - pParamString
	 * @param pParamMessage
	 *            - pParamMessage
	 * @throws JMSException
	 *             - JMSException
	 * 
	 */
	public void receiveMessage(String pParamString, Message pParamMessage) throws JMSException {

		if (isLoggingDebug()) {
			logDebug("ENTER: TRUIntradayPriceMessageSink . in receiveMessage");
		}

		if (null != pParamMessage) {
			TextMessage message = (TextMessage) pParamMessage;
			String jmsId = pParamMessage.getJMSMessageID();
			if (message != null && StringUtils.isNotEmpty(message.getText())) {
				if (isLoggingDebug()) {
					logDebug("TRUIntradayPriceMessageSink . Message received from local Queue" + message.getText());
				}
				String[] skuValues = message.getText().split(TRUIntradayAuditConstants.COMMA_STRING);
				ArrayList<String> skuList = new ArrayList<String>(Arrays.asList(skuValues));

				Timestamp timestamp = new Timestamp(getCurrentDate().getTime());
				populateIntradayPriceVOwithDetails(skuList, jmsId, timestamp, null);
			}
			if (isLoggingDebug()) {
				logDebug("TRUIntradayPriceMessageSink . in receiveMessage" + message.getText());
			}
		}
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUIntradayPriceMessageSink . in receiveMessage");
		}

	}

	/**
	 * Populate intraday price vo with details.
	 * 
	 * @param pSkuList
	 *            the sku list
	 * @param pJmsId
	 *            the jms id
	 * @param pLocalMessageTimestamp
	 *            the local message timestamp
	 * @param pErrorRepositoryId
	 *            the error repository id
	 */
	public void populateIntradayPriceVOwithDetails(List<String> pSkuList, String pJmsId, Timestamp pLocalMessageTimestamp, String pErrorRepositoryId) {
		if (isLoggingDebug()) {
			logDebug("ENTER:  TRUIntradayPriceMessageSink . in constructPriceAuditMessage");
		}
		if (pSkuList != null && !pSkuList.isEmpty()) {

			List<TRUIntradayPriceAuditFeedVO> auditFeedVOs = new ArrayList<TRUIntradayPriceAuditFeedVO>();
			try {
				Site site = getSiteContextManager().getSite(getSiteId());
				for (String skuId : pSkuList) {
					if (StringUtils.isNotEmpty(skuId) && site != null) {
						TRUIntradayPriceAuditFeedVO intradayPriceAuditFeedVO = null;
						intradayPriceAuditFeedVO = populateListPriceDetails(skuId, site, intradayPriceAuditFeedVO);
						intradayPriceAuditFeedVO = populateSalePriceDetails(skuId, site, intradayPriceAuditFeedVO);

						if (intradayPriceAuditFeedVO != null) {
							auditFeedVOs.add(intradayPriceAuditFeedVO);
							vlogDebug(
									"Sku Id: {0} Country Code: {1} Market Code: {2} Store Number: {3}  Locale: {4} SalePrice Value: {5} ListPrice Value: {6} TPR DealId: {7}",
									intradayPriceAuditFeedVO.getSkuId(), intradayPriceAuditFeedVO.getCountryCode(),
									intradayPriceAuditFeedVO.getMarketCode(), intradayPriceAuditFeedVO.getStoreNumber(),
									intradayPriceAuditFeedVO.getLocale(), intradayPriceAuditFeedVO.getSalePrice(),
									intradayPriceAuditFeedVO.getListPrice(), intradayPriceAuditFeedVO.getDealId());
						}
					}
				}
			} catch (SiteContextException siteExc) {
				vlogError("SiteContext Exception while fetching site information ", siteExc);
			} catch (PriceListException priceListExc) {
				vlogError("PriceList Exception while fetching price information ", priceListExc);
			} catch (Exception exc) {
				vlogError("Exception while fetching price information ", exc);
			}
			mIntradayPriceMessageService.processMessagesToPost(auditFeedVOs, pSkuList, pJmsId, pLocalMessageTimestamp, pErrorRepositoryId);
		}
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUIntradayPriceMessageSink . in constructPriceAuditMessage");
		}
	}

	/**
	 * Populate sale price details.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pSite
	 *            the site
	 * @param pIntradayPriceAuditFeedVO
	 *            the intraday price audit feed vo
	 * @return the TRU intraday price audit feed vo
	 * @throws PriceListException
	 *             the price list exception
	 */
	private TRUIntradayPriceAuditFeedVO populateSalePriceDetails(String pSkuId, Site pSite, TRUIntradayPriceAuditFeedVO pIntradayPriceAuditFeedVO)
			throws PriceListException {
		TRUIntradayPriceAuditFeedVO intradayPriceAuditFeedVO = pIntradayPriceAuditFeedVO;
		RepositoryItem salePriceList = (RepositoryItem) pSite.getPropertyValue(getDefaultSalePriceListPropertyName());
		RepositoryItem salePriceRepoItem = getPriceListManager().getPrice(salePriceList, null, pSkuId);
		if (salePriceRepoItem != null) {
			intradayPriceAuditFeedVO = populateDetails(salePriceRepoItem, intradayPriceAuditFeedVO, TRUIntradayAuditConstants.SALE_PRICE);
		}
		return intradayPriceAuditFeedVO;
	}

	/**
	 * Populate list price details.
	 * 
	 * @param pSkuId
	 *            the sku id
	 * @param pSite
	 *            the site
	 * @param pIntradayPriceAuditFeedVO
	 *            the intraday price audit feed vo
	 * @return the TRU intraday price audit feed vo
	 * @throws PriceListException
	 *             the price list exception
	 */
	private TRUIntradayPriceAuditFeedVO populateListPriceDetails(String pSkuId, Site pSite, TRUIntradayPriceAuditFeedVO pIntradayPriceAuditFeedVO)
			throws PriceListException {
		TRUIntradayPriceAuditFeedVO intradayPriceAuditFeedVO = pIntradayPriceAuditFeedVO;
		RepositoryItem listPriceList = (RepositoryItem) pSite.getPropertyValue(getDefaultPriceListPropertyName());
		RepositoryItem listPriceRepoItem = getPriceListManager().getPrice(listPriceList, null, pSkuId);
		if (listPriceRepoItem != null) {
			intradayPriceAuditFeedVO = populateDetails(listPriceRepoItem, intradayPriceAuditFeedVO, TRUIntradayAuditConstants.LIST_PRICE);
		}
		return intradayPriceAuditFeedVO;
	}

	/**
	 * Populate details.
	 * 
	 * @param pPriceRepoItem
	 *            the price repo item
	 * @param pIntradayPriceAuditFeedVO
	 *            the intraday price audit feed vo
	 * @param pPriceType
	 *            the price type
	 * @return the TRU intraday price audit feed vo
	 */
	private TRUIntradayPriceAuditFeedVO populateDetails(RepositoryItem pPriceRepoItem, TRUIntradayPriceAuditFeedVO pIntradayPriceAuditFeedVO, String pPriceType) {
		TRUIntradayPriceAuditFeedVO intradayPriceAuditFeedVO = pIntradayPriceAuditFeedVO;
		if (intradayPriceAuditFeedVO == null) {
			intradayPriceAuditFeedVO = new TRUIntradayPriceAuditFeedVO();
		}

		if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getSKUIdPropertyName()) != null) {
			intradayPriceAuditFeedVO.setSkuId((String) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getSKUIdPropertyName()));
		}

		if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getCountryCodePropertyName()) != null) {
			intradayPriceAuditFeedVO.setCountryCode((String) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getCountryCodePropertyName()));
		}

		if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getMarketCodePropertyName()) != null) {
			intradayPriceAuditFeedVO.setMarketCode((String) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getMarketCodePropertyName()));
		}

		if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getStoreNumberPropertyName()) != null) {
			intradayPriceAuditFeedVO.setStoreNumber((String) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties()
					.getStoreNumberPropertyName()));
		}

		if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getPriceListPropertyName()) != null) {
			RepositoryItem item = (RepositoryItem) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getPriceListPropertyName());
			if (item != null && item.getPropertyValue(getIntradayPriceProperties().getLocalePropertyName()) != null) {
				intradayPriceAuditFeedVO.setLocale((String) item.getPropertyValue(getIntradayPriceProperties().getLocalePropertyName()));
			}
		}

		if (StringUtils.isNotEmpty(pPriceType) && pPriceType.equalsIgnoreCase(TRUIntradayAuditConstants.LIST_PRICE)
				&& pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getListPricePropertyName()) != null) {
			intradayPriceAuditFeedVO.setListPrice(String.valueOf((Double) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties()
					.getListPricePropertyName())));
		} else if (StringUtils.isNotEmpty(pPriceType) && pPriceType.equalsIgnoreCase(TRUIntradayAuditConstants.SALE_PRICE)
				&& pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getListPricePropertyName()) != null) {
			intradayPriceAuditFeedVO.setSalePrice(String.valueOf((Double) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties()
					.getListPricePropertyName())));
			if (pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getDealIdPropertyName()) != null) { 
				intradayPriceAuditFeedVO.setDealId((String) pPriceRepoItem.getPropertyValue(getIntradayPriceProperties().getDealIdPropertyName()));
			}
		}
		return intradayPriceAuditFeedVO;
	}

	/**
	 * Invoke message sink.
	 */
	public void invokeMessageSink() {
		List<String> skuList = getSkuList();
		Timestamp timestamp = new Timestamp(getCurrentDate().getTime());
		populateIntradayPriceVOwithDetails(skuList, getLocalJmsId(), timestamp, null);
	}

	/**
	 * @return the mIntradayPriceMessageService
	 */
	public TRUIntradayPriceMessageService getIntradayPriceMessageService() {
		return mIntradayPriceMessageService;
	}

	/**
	 * @param pIntradayPriceMessageService
	 *            the mIntradayPriceMessageService to set
	 */
	public void setIntradayPriceMessageService(TRUIntradayPriceMessageService pIntradayPriceMessageService) {
		this.mIntradayPriceMessageService = pIntradayPriceMessageService;
	}

	/**
	 * @return the mSiteContextManager
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * @param pSiteContextManager
	 *            the mSiteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		this.mSiteContextManager = pSiteContextManager;
	}

	/**
	 * @return the mDefaultPriceListPropertyName
	 */
	public String getDefaultPriceListPropertyName() {
		return mDefaultPriceListPropertyName;
	}

	/**
	 * @param pDefaultPriceListPropertyName
	 *            the mDefaultPriceListPropertyName to set
	 */
	public void setDefaultPriceListPropertyName(String pDefaultPriceListPropertyName) {
		this.mDefaultPriceListPropertyName = pDefaultPriceListPropertyName;
	}

	/**
	 * @return the mDefaultSalePriceListPropertyName
	 */
	public String getDefaultSalePriceListPropertyName() {
		return mDefaultSalePriceListPropertyName;
	}

	/**
	 * @param pDefaultSalePriceListPropertyName
	 *            the mDefaultSalePriceListPropertyName to set
	 */
	public void setDefaultSalePriceListPropertyName(String pDefaultSalePriceListPropertyName) {
		this.mDefaultSalePriceListPropertyName = pDefaultSalePriceListPropertyName;
	}

	/**
	 * @return the mPriceListManager
	 */
	public PriceListManager getPriceListManager() {
		return mPriceListManager;
	}

	/**
	 * @param pPriceListManager
	 *            the mPriceListManager to set
	 */
	public void setPriceListManager(PriceListManager pPriceListManager) {
		this.mPriceListManager = pPriceListManager;
	}

	/**
	 * @return the mIntradayPriceProperties
	 */
	public TRUIntradayPriceProperties getIntradayPriceProperties() {
		return mIntradayPriceProperties;
	}

	/**
	 * @param pIntradayPriceProperties
	 *            the mIntradayPriceProperties to set
	 */
	public void setIntradayPriceProperties(TRUIntradayPriceProperties pIntradayPriceProperties) {
		this.mIntradayPriceProperties = pIntradayPriceProperties;
	}

	/**
	 * @return the mSiteId
	 */
	public String getSiteId() {
		return mSiteId;
	}

	/**
	 * @param pSiteId
	 *            the mSiteId to set
	 */
	public void setSiteId(String pSiteId) {
		this.mSiteId = pSiteId;
	}

	/**
	 * @return the mSkuList
	 */
	public List<String> getSkuList() {
		return mSkuList;
	}

	/**
	 * @param pSkuList
	 *            the mSkuList to set
	 */
	public void setSkuList(List<String> pSkuList) {
		this.mSkuList = pSkuList;
	}

	/**
	 * Gets the local jms id.
	 * 
	 * @return the local jms id
	 */
	public String getLocalJmsId() {
		return mLocalJmsId;
	}

	/**
	 * Sets the local jms id.
	 * 
	 * @param pLocalJmsId
	 *            the new local jms id
	 */
	public void setLocalJmsId(String pLocalJmsId) {
		this.mLocalJmsId = pLocalJmsId;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the mCurrentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param pCurrentDate the new current date
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		this.mCurrentDate = pCurrentDate;
	}

}