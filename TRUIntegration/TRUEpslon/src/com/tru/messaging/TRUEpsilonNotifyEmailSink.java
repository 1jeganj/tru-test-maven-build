package com.tru.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.integrations.constant.TRUEpslonConstants;
import com.tru.integrations.epslon.TRUEpslonMessageBean;
import com.tru.integrations.epslon.notify.TRUEpsilonNotifyMeEmailService;

/**
 * Class TRUEpsilonNotifyEmailSink.
 * This class performs the functionality of receiving the messages from
 * notifyMeWhenAvailableQueue for send gift card activation email event.
 * @author PA.
 * @version 1.0.
 */

public class TRUEpsilonNotifyEmailSink extends GenericService implements MessageSink {
	 
	 /**
	 * Holds the property value of mNotifyEmailSource.
	 */
	private TRUEpsilonNotifyEmailSource mNotifyEmailSource;
	 
	 /**
	 * Holds the property value of mEpsilonNotifyMeEmailService.
	 */
	private TRUEpsilonNotifyMeEmailService mEpsilonNotifyMeEmailService;
	
	/**
	 * This method is used to get notifyEmailSource.
	 * @return notifyEmailSource TRUEpslonNotifyEmailSource.
	 */
	public TRUEpsilonNotifyEmailSource getNotifyEmailSource() {
		return mNotifyEmailSource;
	}

	/**
	 *This method is used to set notifyEmailSource.
	 *@param pNotifyEmailSource TRUEpslonNotifyEmailSource.
	 */
	public void setNotifyEmailSource(TRUEpsilonNotifyEmailSource pNotifyEmailSource) {
		mNotifyEmailSource = pNotifyEmailSource;
	}

	/**
	 * @return the epsilonNotifyMeEmailService.
	 */
	public TRUEpsilonNotifyMeEmailService getEpsilonNotifyMeEmailService() {
		return mEpsilonNotifyMeEmailService;
	}

	/**
	 * @param pEpsilonNotifyMeEmailService the epsilonNotifyMeEmailService to set.
	 */
	public void setEpsilonNotifyMeEmailService(
			TRUEpsilonNotifyMeEmailService pEpsilonNotifyMeEmailService) {
		mEpsilonNotifyMeEmailService = pEpsilonNotifyMeEmailService;
	}

	/**
	 * This method performs the functionality of receiving the messages from notifyMeWhenAvailableQueue.
	 * @param pParamString - String.
	 * @param pParamMessage - String.
	 * @throws JMSException - JMSException.
	 */
	public void receiveMessage(String pParamString, Message pParamMessage)
			throws JMSException {

		if (isLoggingDebug()) {
			logDebug("ENTER: TRUEpsilonNotifyEmailSink . in receiveMessage");
		}
		TRUEpslonMessageBean epslonMessageBean = null;
		if (null != pParamMessage) {
			epslonMessageBean = (TRUEpslonMessageBean) ((ObjectMessage) pParamMessage)
					.getObject();
			if(pParamMessage.getJMSMessageID() != null){
				String jmsId = getJMSMessageID(pParamMessage.getJMSMessageID());
				epslonMessageBean.setEpslonReferenceID(jmsId);
				}else{
					if (isLoggingDebug()) {
					logDebug("JMSMessageId is null"+pParamMessage.getJMSMessageID());
					}
				}
		}
		getEpsilonNotifyMeEmailService().generateEmailNotificationRequest(epslonMessageBean);
		if (isLoggingDebug()) {
			logDebug("EXIT:  TRUEpsilonNotifyEmailSink . in receiveMessage");
		}

	}
	
	/**
	 * This method performs the functionality formating the JMS MessageId.
	 * @param pMessageId - String.
	 * @return String.
	 */
	public String getJMSMessageID(String pMessageId){
		
		String finalMessageID = TRUEpslonConstants.EMPTY_STRING ;
		vlogDebug("JMS Message ID in getJMSMessageID)::" ,pMessageId );
		if(! StringUtils.isBlank(pMessageId)) {
			String initial = pMessageId;
			if(pMessageId.contains(TRUEpslonConstants.COLON)){
			String[] parts = initial.split(TRUEpslonConstants.COLON);
			String id = parts[TRUEpslonConstants.ONE];
			if(getNotifyEmailSource() != null){
             finalMessageID = getNotifyEmailSource().getEpslonReferenceID().concat(id);
			 }
			}else{
				finalMessageID = pMessageId;
			}
		}
		return finalMessageID;
	}
	
}
