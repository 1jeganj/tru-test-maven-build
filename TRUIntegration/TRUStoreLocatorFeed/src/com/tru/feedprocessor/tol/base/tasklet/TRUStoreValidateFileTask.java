package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class ValidateFileTask. This class is used for validating the input feed file. Project team has to override
 * doExecute() method and do the file validation. If the file validation fails then they should throw
 * FeedSkippableException.
 * 
 * @author PA
 */
public class TRUStoreValidateFileTask extends ValidateDataTask {

	/* The mXmlSchemaNsURI */
	/** The m xml schema ns uri. */
	private String mXmlSchemaNsURI = null;

	/**
	 * Gets the xml schema ns uri.
	 * 
	 * @return the xmlSchemaNsURI
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}

	/**
	 * Sets the xml schema ns uri.
	 * 
	 * @param pXmlSchemaNsURI
	 *            the xmlSchemaNsURI to set
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		mXmlSchemaNsURI = pXmlSchemaNsURI;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute() This is the blank method where project team has to
	 * put the code which will be performed during validation of the file
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreValidateFileTask : @Method: doExecute()");
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		File file = new File(curExecCntx.getFilePath() + curExecCntx.getFileName());
		String startDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		boolean flag = validateFile(file);

		if (!flag) {
			logFailedMessage(startDate, FeedConstants.INVALID_FILE, file.getName());
			throw new FeedSkippableException(getPhaseName(), getStepName(), FeedConstants.INVALID_FILE, null,null);
		}
		logSuccessMessage( startDate, TRUStoreFeedReferenceConstants.FILES_VALID, file.getName());
		getLogger().vlogDebug("End:@Class: TRUStoreValidateFileTask : @Method: doExecute()");
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logFailedMessage(String pStartDate, String pMessage, String pFileName) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FILE_VALIDATION, pFileName, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage ,String pFileName) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FILE_VALIDATION, pFileName, extenstions);
	}

	/**
	 * IT will validate the xml file against XSD if its true it will process the file if false it will throw the invalid
	 * file.
	 * 
	 * @param pFile
	 *            - File
	 * @return boolean
	 */
	public boolean validateFile(File pFile) {
		getLogger().vlogDebug("Begin:@Class: TRUStoreValidateFileTask : @Method: validateFile()");
		boolean isValidFeed = true;
		if (FilenameUtils.getExtension(pFile.toString()).equals(FeedConstants.XML)) {

			SchemaFactory schemaFactory = null;
			Schema schema = null;
			Source xmlFile = new StreamSource(pFile);
			String lSchemaFileXsdUri = getConfigValue(TRUStoreFeedReferenceConstants.SCHEMA_FILE_XSD_URI).toString();
			String lXmlSchemaNsURI = getXmlSchemaNsURI();

			try {
				if (StringUtils.isEmpty(lXmlSchemaNsURI)) {
					getLogger().logWarning("XML_SCHEMA_NS_URI Uri is null or Empty in Validate File Task");
				} else {
					schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
				}
				if (StringUtils.isEmpty(lSchemaFileXsdUri)) {
					getLogger().logError("XSD Uri is null or Empty in Validate File Task");
				} else {
					schema = schemaFactory.newSchema(new File(lSchemaFileXsdUri));
				}
				Validator validator = schema.newValidator();
				validator.validate(xmlFile);

			} catch (SAXException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + TRUStoreFeedReferenceConstants.FILE_NOT_VALID , ex);
				}
			} catch (IOException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + TRUStoreFeedReferenceConstants.FILE_NOT_VALID , ex);
				}
			} 
		}
		getLogger().vlogDebug("End:@Class: TRUStoreValidateFileTask : @Method: validateFile()");
		return isValidFeed;
	}
}
