package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * This class has written in order to check the file is exists in current execution context or not.
 * 
 * 
 */
public class TRUStoreFeedFileExistsTask extends FileExistsTask {
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.tasklet.DataExistsTask#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileExistsTask : @Method: doExecute()");
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		String startDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		if (null != curExecCntx && null != curExecCntx.getFileName()) {
			File lFile = new File(curExecCntx.getFilePath() + curExecCntx.getFileName());
			if (lFile == null || !lFile.exists() || !lFile.isFile() || !lFile.canRead()) {
				logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
				throw new FeedSkippableException(getPhaseName(), getStepName(), FeedConstants.FILE_DOES_NOT_EXISTS,
						null,null);
			}
			logSuccessMessage(startDate, TRUStoreFeedReferenceConstants.FILES_MOVED_SUCCESS);
		} else {
			logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
			throw new FeedSkippableException(getPhaseName(), getStepName(), FeedConstants.FILE_DOES_NOT_EXISTS, null,null);
		}
		getLogger().vlogDebug("End :@Class: TRUStoreFeedFileExistsTask : @Method: doExecute()");
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}

}
