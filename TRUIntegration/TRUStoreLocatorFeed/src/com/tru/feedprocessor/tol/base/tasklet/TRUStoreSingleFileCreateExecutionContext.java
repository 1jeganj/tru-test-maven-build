package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class SingleFileCreateExecutionContext create executionContext queue for a single feed
 * 
 * @author PA
 * @version 1.1
 * 
 */
public class TRUStoreSingleFileCreateExecutionContext extends AbstractCreateFeedExecutionContext {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractCreateFeedExecutionContext #createExecutionContext()
	 */
	@Override
	protected Queue<FeedExecutionContextVO> createExecutionContext() {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreSingleFileCreateExecutionContext : @Method: createExecutionContext()");
		Queue executionContextQueue = new LinkedBlockingQueue<FeedExecutionContextVO>();
		String processingFileName = null;
		String processingFilePath = null;
		FeedExecutionContextVO feedExecutionContextVO = new FeedExecutionContextVO();
		if (getFeedHelper().getConfigValue(FeedConstants.FILEPATH) != null
				&& getFeedHelper().getConfigValue(FeedConstants.FILENAME) != null) {
			processingFilePath = (String) getFeedHelper().getConfigValue(FeedConstants.FILEPATH);
			processingFileName = (String) getFeedHelper().getConfigValue(FeedConstants.FILENAME);
			feedExecutionContextVO.setFileName(processingFileName);
			feedExecutionContextVO.setFilePath(processingFilePath);
		} else {
			feedExecutionContextVO.setFileName(null);
			feedExecutionContextVO.setFilePath(null);
		}
		executionContextQueue.add(feedExecutionContextVO);
		getLogger().vlogDebug(
				"End:@Class: TRUStoreSingleFileCreateExecutionContext : @Method: createExecutionContext()");
		return executionContextQueue;
	}

}
