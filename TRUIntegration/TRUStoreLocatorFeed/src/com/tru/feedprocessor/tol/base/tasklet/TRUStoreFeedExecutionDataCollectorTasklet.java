package com.tru.feedprocessor.tol.base.tasklet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;
import com.tru.feedprocessor.tol.store.TRUStoreFeedProperties;
import com.tru.feedprocessor.tol.store.feedvo.Store;
import com.tru.logging.TRUAlertLogger;

/**
 * The class FeedExecutionDataCollectorTasklet will notify the information for each process context.
 * 
 * @author PA
 * @version 1.1
 * 
 */
public class TRUStoreFeedExecutionDataCollectorTasklet extends FeedExecutionDataCollectorTasklet {

	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	
	/** The m feed store property. */
	public TRUStoreFeedProperties mFeedStoreProperty;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.tasklet.AbstractTasklet#execute(org.springframework.batch.core.StepContribution
	 * , org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public RepeatStatus execute(StepContribution pStepContribution, ChunkContext pChunkContext) throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: execute()");
		super.execute(pStepContribution, pChunkContext);
		boolean isException = Boolean.FALSE;
		boolean isFileInvalid = Boolean.FALSE;
		boolean isFileNotExits = Boolean.FALSE;
		String startDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		String processingDir = getConfigValue(TRUStoreFeedReferenceConstants.PROCESSING_DIRECTORY).toString();
		String badDir = getConfigValue(TRUStoreFeedReferenceConstants.BAD_DIRECTORY).toString();
		String successDir = getConfigValue(TRUStoreFeedReferenceConstants.SUCCESS_DIRECTORY).toString();
		String errorDir = getConfigValue(TRUStoreFeedReferenceConstants.ERROR_DIRECTORY).toString();
		String sourceDir = getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString();
		FeedExecutionContextVO feedExecutionContextVO = getCurrentFeedExecutionContext();
		int totalRecordCount = 0;
		int successRecordCount = 0;
		List<StepInfoVO> stepInfoVOList = feedExecutionContextVO.getStepInfos();
		if (null != stepInfoVOList && !stepInfoVOList.isEmpty()) {
			for (StepInfoVO stepInfoVO : stepInfoVOList) {
				if (null != stepInfoVO && null != stepInfoVO.getFeedFailureProcessException()) {
					isException = Boolean.TRUE;
					if (stepInfoVO.getFeedFailureProcessException().getMessage()
							.contains(FeedConstants.FILE_DOES_NOT_EXISTS)) {
						isFileNotExits = Boolean.TRUE;
						break;
					} else if (stepInfoVO.getStepName().equalsIgnoreCase(
							TRUStoreFeedReferenceConstants.STORE_FEED_FILE_VALIDATION_STEP)
							&& stepInfoVO.getFeedFailureProcessException().getMessage()
									.contains(FeedConstants.INVALID_FILE)) {
						isFileInvalid = Boolean.TRUE;
						break;
					} 
				} else if (null != stepInfoVO
						&& stepInfoVO.getStepName().equalsIgnoreCase(
								TRUStoreFeedReferenceConstants.STORE_FEED_ADD_UPDATE_STEP)) {
					totalRecordCount = stepInfoVO.getReadCount();
					getLogger().vlogDebug(
							"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : totalRecordCount:"
									+ totalRecordCount);
				}
			}
		}
		if (isFileInvalid) {
			moveInvalidFileToBadDir(feedExecutionContextVO.getFileName(), processingDir, badDir);
			logFailedMessage(startDate, TRUStoreFeedReferenceConstants.FEED_FAILED);
		}
		if (isFileNotExits) {
			notifyFileDoseNotExits(sourceDir);
			logFailedMessage(startDate, TRUStoreFeedReferenceConstants.FEED_FAILED);
		}
		List<FeedSkippableException> invalidRecords = null;
		if (!isException) {
			Map<String, List<FeedSkippableException>> allFeedSkippedException = feedExecutionContextVO
					.getAllFeedSkippedException();
			invalidRecords = new ArrayList<FeedSkippableException>();
			for (Entry<String, List<FeedSkippableException>> entry : allFeedSkippedException.entrySet()) {
				List<FeedSkippableException> lFeedSkippableException = entry.getValue();
				for (FeedSkippableException lSkippedException : lFeedSkippableException) {
					if (null != lSkippedException
							&& lSkippedException.getMessage().contains(TRUStoreFeedReferenceConstants.INVALID_RECORD)) {
						invalidRecords.add(lSkippedException);
					}
				}
			}
			if (null != invalidRecords && !invalidRecords.isEmpty()) {
				successRecordCount = totalRecordCount - invalidRecords.size();
				saveSequenceNumber(feedExecutionContextVO);
				moveInvalidRecordsToErrorDir(invalidRecords, feedExecutionContextVO.getFileName(), totalRecordCount,
						errorDir);
				moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir,
						totalRecordCount, successRecordCount, Boolean.FALSE);
				logSuccessMessage(startDate, TRUStoreFeedReferenceConstants.FEED_COMPLETED);
				isException = Boolean.TRUE;
			}
		}
		if (!isException) {
			successRecordCount = totalRecordCount - invalidRecords.size();
			saveSequenceNumber(feedExecutionContextVO);
			boolean isThresholdExceed = Boolean.FALSE;
			if (totalRecordCount > TRUStoreFeedReferenceConstants.NUMBER_ZERO) {
				isThresholdExceed = isThresholdExceeded(totalRecordCount);
			}

			if (isThresholdExceed) {
				moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir, totalRecordCount, successRecordCount,
						Boolean.FALSE);
			} else {
				moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir, totalRecordCount, successRecordCount, Boolean.TRUE);
			}
			
			logSuccessMessage(startDate, TRUStoreFeedReferenceConstants.FEED_COMPLETED);
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: execute()");
		return RepeatStatus.FINISHED;

	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Save sequence number.
	 * 
	 * @param pFeedExecutionContextVO
	 *            the feed execution context vo
	 */
	private void saveSequenceNumber(FeedExecutionContextVO pFeedExecutionContextVO) {
		getLogger().vlogDebug("Start:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: saveSequenceNumber()");
		int seqNumber = FeedConstants.NUMBER_ZERO;
		String fileName = pFeedExecutionContextVO.getFileName();
		seqNumber = Integer.parseInt(fileName.substring(fileName.lastIndexOf(FeedConstants.UNDER_SCORE) + FeedConstants.NUMBER_ONE,
				fileName.lastIndexOf(FeedConstants.DOT)));
		if (seqNumber != FeedConstants.NUMBER_ZERO) {
			RepositoryItem repoItem;
			try {
				repoItem = getFileSequenceRepository().getItem(TRUStoreFeedReferenceConstants.STORE_FEED,
						TRUStoreFeedReferenceConstants.FEED_FILE_SEQUENCE);
				if (repoItem != null) {
					MutableRepositoryItem mutItem = (MutableRepositoryItem) repoItem;
					mutItem.setPropertyValue(getFeedStoreProperty().getSeqProcessedPropertyName(), seqNumber);
					getFileSequenceRepository().updateItem(mutItem);
				} else {
					MutableRepositoryItem mutItem = getFileSequenceRepository().createItem(TRUStoreFeedReferenceConstants.STORE_FEED,
							TRUStoreFeedReferenceConstants.FEED_FILE_SEQUENCE);
					mutItem.setPropertyValue(getFeedStoreProperty().getSeqProcessedPropertyName(), seqNumber);
					getFileSequenceRepository().addItem(mutItem);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError("Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method saveSequenceNumber()", re);
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: saveSequenceNumber()");
	}

	/**
	 * Checks if is threshold exceeded.
	 * 
	 * @param pTotalRecordCount
	 *            total count
	 * @return true, if is threshold exceeded
	 * @throws Exception
	 *             the exception
	 */
	private boolean isThresholdExceeded(int pTotalRecordCount) throws Exception {

		getLogger()
				.vlogDebug("Begin @Class: TRUStoreFeedExecutionDataCollectorTasklet, @method: isThresholdExceeded()");
		if (getConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_DIRECTORY) != null
				&& getConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_FILE_NAME) != null) {
			String StorePreviousDayFilePath = (String) getConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_DIRECTORY);
			String StorePreviousDayFileName = (String) getConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_FILE_NAME);
			double minPriorityThresholdP3 = getFeedEnvConfiguration().getLowerCutoffAlertPriority3();
			double maxPriorityThresholdP3 = getFeedEnvConfiguration().getUpperCutoffAlertPriority3();
			double minPriorityThresholdP2 = getFeedEnvConfiguration().getLowerCutoffAlertPriority2();
			double maxPriorityThresholdP2 = getFeedEnvConfiguration().getUpperCutoffAlertPriority2();
			Writer writer = null;
			BufferedWriter bw = null;
			Reader reader = null;
			BufferedReader br = null;
			File sourceFolder = new File(StorePreviousDayFilePath);
			if (!sourceFolder.exists() && !sourceFolder.isDirectory()) {
				sourceFolder.mkdirs();
			}
			File countFile = new File(StorePreviousDayFilePath + StorePreviousDayFileName);
			if (countFile == null || !countFile.exists() || !countFile.isFile() || !countFile.canRead()) {
				writer = new FileWriter(countFile);
				bw = new BufferedWriter(writer);
				try {
					bw.write(Integer.toString(pTotalRecordCount));
				} catch (IOException e) {
					if (isLoggingError()) {
						logError("Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()", e);
					}
				} finally {
					try {
						if (null != bw) {
							bw.close();
						}
						if (null != writer) {
							writer.close();
						}
					} catch (IOException e) {
						if (isLoggingError()) {
							logError(
									"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()", e);	
						}
					}
				}
				return Boolean.FALSE;
			} else {
				try {
					reader = new FileReader(countFile);
					br = new BufferedReader(reader);
					int count = Integer.parseInt(br.readLine());
					if (pTotalRecordCount < count) {
						Double percentage = calculatePercentage(count, pTotalRecordCount);
						if (percentage >= minPriorityThresholdP3 && percentage <= maxPriorityThresholdP3) { 
							sendThresholdAlertMail(TRUStoreFeedReferenceConstants.TAXONOMY_P_THREE_ALERT, minPriorityThresholdP3, maxPriorityThresholdP3);
							writer = new FileWriter(countFile);
							bw = new BufferedWriter(writer);
							bw.flush();
							bw.write(Integer.toString(pTotalRecordCount));
							return Boolean.TRUE;
						} else if ((percentage > minPriorityThresholdP2 && percentage <= maxPriorityThresholdP2)) {
							sendThresholdAlertMail(TRUStoreFeedReferenceConstants.TAXONOMY_P_THREE_ALERT, minPriorityThresholdP2, maxPriorityThresholdP2);
							return Boolean.TRUE;
						} else if (percentage > maxPriorityThresholdP2) {
							sendThresholdAlertMail(TRUStoreFeedReferenceConstants.TAXONOMY_P_TWO_ALERT, TRUStoreFeedReferenceConstants.ZERO_DOUBLE, maxPriorityThresholdP2);
							return Boolean.TRUE;
						}
					} else {
						writer = new FileWriter(countFile);
						bw = new BufferedWriter(writer);
						bw.flush();
						bw.write(Integer.toString(pTotalRecordCount));
						return Boolean.FALSE;
					}
				} catch (FileNotFoundException e) {
					if (isLoggingError()) {
					logError(
							"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()",
							e);
				}
				} catch (NumberFormatException e) {
					if (isLoggingError()) {
						logError(
								"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()",
								e);	
					}
				} catch (IOException e) {
					if (isLoggingError()) {
						logError("Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()",
													e);
					}
				} finally {
					try {
						if (null != br) {
							br.close();
						}
						if (null != reader) {
							reader.close();
						}
						if (null != bw) {
							bw.close();
						}
						if (null != writer) {
							writer.close();
						}
					} catch (IOException e) {
						if (isLoggingError()) {
							logError(
									"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method isThresholdExceeded()",
									e);
						}
					}
				}

			}
		}
		getLogger().vlogDebug("END @Class: TRUStoreFeedExecutionDataCollectorTasklet, @method: isThresholdExceeded()");
		return Boolean.FALSE;
	}

	/**
	 * Send threshold alert mail.
	 * 
	 * @param pAlert
	 *            the alert
	 * @param pLowerPercentage
	 *            the lower percentage
	 * @param pUpperPercentage
	 *            the upper percentage
	 */
	private void sendThresholdAlertMail(String pAlert, Double pLowerPercentage, Double pUpperPercentage) {
 
		getLogger().vlogDebug(
				"Start @Class: TRUStoreFeedExecutionDataCollectorTasklet, @method: sendThresholdAlertMail()");
		StringBuffer software=new StringBuffer();
		software.append(getFeedEnvConfiguration().getSoftware());
		software.append(TRUStoreFeedReferenceConstants.SPACE).append(FeedConstants.STORE_JOB_NAME);
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		File[] attachmentFile = null;
		String detailException = null; 
		
		String content = null;
		String subject = getFeedEnvConfiguration().getStoreFeedFailureSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);
		

		if(pAlert.equalsIgnoreCase(TRUStoreFeedReferenceConstants.TAXONOMY_P_TWO_ALERT)) {
			detailException = TRUStoreFeedReferenceConstants.THRESHOLD_STORE_DETAILEDEXPP2;
			content = StringUtils.replace(TRUStoreFeedReferenceConstants.JOB_FAILED_THRESHOLD1, TRUStoreFeedReferenceConstants.ZERO_BRACES, 
					String.valueOf(pUpperPercentage));
		} else if (pAlert.equalsIgnoreCase(TRUStoreFeedReferenceConstants.TAXONOMY_P_THREE_ALERT)) {
			detailException = MessageFormat.format(TRUStoreFeedReferenceConstants.THRESHOLD_STORE_DETAILEDEXPP3, pLowerPercentage, pUpperPercentage);
			content = MessageFormat.format(TRUStoreFeedReferenceConstants.JOB_FAILED_THRESHOLD2, pLowerPercentage, pUpperPercentage);
		} 

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pAlert);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.STORE_JOB_NAME, templateParameters,
				attachmentFile);

		getLogger().vlogDebug(
				"End @Class: TRUStoreFeedExecutionDataCollectorTasklet, @method: sendThresholdAlertMail()");

	}

	/**
	 * Calculate percentage.
	 * 
	 * @param pCount
	 *            the count
	 * @param pCurrentFeedRecordCount
	 *            the current feed record count
	 * @return the double
	 */
	private Double calculatePercentage(int pCount, int pCurrentFeedRecordCount) {
		BigDecimal todaysCount = BigDecimal.valueOf(pCurrentFeedRecordCount);
		BigDecimal previuosCount = BigDecimal.valueOf(pCount);
		BigDecimal percentage = previuosCount.subtract(todaysCount)
				.multiply(BigDecimal.valueOf(TRUStoreFeedReferenceConstants.NUMBER_HUNDRED))
				.divide(previuosCount, FeedConstants.NUMBER_TWO, BigDecimal.ROUND_HALF_UP);

		return percentage.doubleValue();
	}

	/**
	 * Move invalid file to bad dir.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pBadDir
	 *            the bad dir
	 */
	public void moveInvalidFileToBadDir(String pFileName, String pProcessingDir, String pBadDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			
			File toFolder = new File(pBadDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(pFileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
			}
		}
		triggerInvalidFeedEmail(pBadDir);
		getLogger().vlogDebug(
				"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
	}

	/**
	 * Trigger invalid feed email.
	 * 
	 * @param pBadDir
	 *            the bad dir
	 */
	public void triggerInvalidFeedEmail(String pBadDir) {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();

		String priority = TRUStoreFeedReferenceConstants.FILE_INVALID;
		String content = TRUStoreFeedReferenceConstants.JOB_FAILED_INVALID_FILE;
		String detailException = getFeedEnvConfiguration().getInvalidStoreDetailExcep();
		
		String sub = getFeedEnvConfiguration().getStoreFeedFailureSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(sub, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.STORE_JOB_NAME, templateParameters);
		getLogger().vlogDebug("End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
	}

	/**
	 * Move invalid records to error dir.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pFeedFileName
	 *            the feed file name
	 * @param pTotalRecordCount
	 *            total record count
	 * @param pErrorDir
	 *            the error dir
	 */
	public void moveInvalidRecordsToErrorDir(List<FeedSkippableException> pInvalidRecords, String pFeedFileName,
			int pTotalRecordCount, String pErrorDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
		File sourceFolder = new File(pErrorDir);
		Writer writer = null;
		BufferedWriter bw = null;
		if (!sourceFolder.exists() && !sourceFolder.isDirectory()) {
			sourceFolder.mkdirs();
		}

		File invalidRecordsFile = new File(sourceFolder, FeedConstants.ERROR + FeedConstants.UNDER_SCORE + pFeedFileName);
		try {
			writer = new FileWriter(invalidRecordsFile);
			bw = new BufferedWriter(writer);
			for (FeedSkippableException fse : pInvalidRecords) {
				bw.write(fse.getFeedItemVO().getRepositoryId());
				bw.newLine();
			}
		} catch (IOException e) {
			if(isLoggingError()) {
				logError(
						"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()",
						e);
			}
		} finally {
			try {
				if (null != bw) {
					bw.close();
				}
				if (null != writer) {
					writer.close();
				}
			} catch (IOException e) {
				if(isLoggingError()) {
					logError(
							"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()",
							e);	
				}
			}
		}
		double percentage = getExceptionThresholdPercentage(pTotalRecordCount, pInvalidRecords.size());
		getLogger().vlogDebug("percentage:", percentage);
		String priority = getExceptionThresholdPriority(percentage);
		getLogger().vlogDebug("priority:", priority);
		triggerFeedProcessedWithErrorsEmail(pInvalidRecords, invalidRecordsFile.getName(), pErrorDir, priority, pTotalRecordCount, pFeedFileName);
		getLogger().vlogDebug(
				"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
	}

	/**
	 * Gets the exception threshold percentage.
	 * 
	 * @param pTotalRecord
	 *            the total record
	 * @param pExceptionCount
	 *            the exception count
	 * @return the exception threshold percentage
	 */
	public double getExceptionThresholdPercentage(int pTotalRecord, int pExceptionCount) {
		BigDecimal totalCount = BigDecimal.valueOf(pTotalRecord);
		BigDecimal exceptionCount = BigDecimal.valueOf(pExceptionCount);
		BigDecimal percentage = exceptionCount.multiply(
				BigDecimal.valueOf(TRUStoreFeedReferenceConstants.NUMBER_HUNDRED)).divide(totalCount,
				FeedConstants.NUMBER_TWO, BigDecimal.ROUND_HALF_UP);
		return percentage.doubleValue();
	}

	/**
	 * Gets the exception threshold priority.
	 * 
	 * @param pPercentage
	 *            the percentage
	 * @return the exception threshold priority
	 */
	public String getExceptionThresholdPriority(double pPercentage) {
		if (pPercentage >= getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriorityRegular()
				&& pPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriorityRegular()) {
			return getFeedEnvConfiguration().getMailPriorityRegular();
		} else if (pPercentage > getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriority3()
				&& pPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriority3()) {
			return getFeedEnvConfiguration().getFailureMailPriority3();
		} else if (pPercentage >= getFeedEnvConfiguration().getCutoffExceptionAlertPriority2()) {
			return getFeedEnvConfiguration().getFailureMailPriority2();
		}
		return FeedConstants.EMPTY;
	}

	/**
	 * Trigger feed processed with errors email.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pPriority
	 *            the priority
	 * @param pTotalRecordCount
	 *            the total record count
	 * @param pFeedFileName
	 *            the feed file name
	 */
	public void triggerFeedProcessedWithErrorsEmail(List<FeedSkippableException> pInvalidRecords, String pErrorFileName,
			String pErrorDir, String pPriority, int pTotalRecordCount, String pFeedFileName) {
		getLogger()
				.vlogDebug(
						"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		File[] attachmentFile = new File[FeedConstants.NUMBER_ONE];
		attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pInvalidRecords, pErrorFileName, pErrorDir, pFeedFileName);

		int successCount = 0;
		if (pInvalidRecords != null) {
			successCount = pTotalRecordCount - pInvalidRecords.size();
		}
		String content = TRUStoreFeedReferenceConstants.FILE_PROCESSED_WITH_ERRORS;

		String subject = getFeedEnvConfiguration().getStoreFeedErrorSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);

		String detailException = getFeedEnvConfiguration().getStoreFeedErrorDetailExcep();

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pPriority);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pInvalidRecords.size());
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, successCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		
		
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.STORE_JOB_NAME, templateParameters,
				attachmentFile);
		getLogger()
				.vlogDebug(
						"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");

	}

	/**
	 * Creates the attachment file.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pFeedFileName
	 *            the feed file name
	 * @return the output stream
	 */
	public File createAttachmentFile(List<FeedSkippableException> pInvalidRecords, String pErrorFileName, String pErrorDir, String pFeedFileName) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		File errorFolder = new File(pErrorDir);
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}
		
		File attachment = new File(errorFolder, attachmentFileName(pErrorFileName));
		try {

			outputStream = new FileOutputStream(attachment);
			workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(TRUStoreFeedReferenceConstants.SHEET);
			int rowCount = FeedConstants.NUMBER_ZERO;
			HSSFRow rowhead = sheet.createRow(rowCount);
			HSSFCellStyle rowCellStyle = workbook.createCellStyle();
			rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			rowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getFeedEnvConfiguration().getFileName());
			rowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getFeedEnvConfiguration().getRecordNumber());
			rowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getFeedEnvConfiguration().getTimeStamp());
			rowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getFeedEnvConfiguration().getAttachmentItemIdLabel());
			rowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(getFeedEnvConfiguration().getAttachmentExceptionReasonLabel());

			rowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(rowCellStyle);
			

			rowCount = FeedConstants.NUMBER_ONE;
			HSSFCellStyle dataCellStyle = workbook.createCellStyle();
			dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			for (FeedSkippableException fse : pInvalidRecords) {
				HSSFRow dataRow = sheet.createRow(rowCount);
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(new HSSFRichTextString(pFeedFileName));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(new HSSFRichTextString(((Store) fse.getFeedItemVO()).getLocationCode()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(fse.getMessage()));

				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);

				rowCount++;
			}
			if (null != workbook) {
				workbook.write(outputStream);
			}
		} catch (IOException ioe) {
			if(isLoggingError()) {
				logError(
						"Error in : @class TRUStoreFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);	
			}
		}
		getLogger()
				.vlogDebug("End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		return attachment;

	}

	/**
	 * Attachement file name.
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachmentFileName(String pFileName) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		String newFileName = pFileName.substring(FeedConstants.NUMBER_ZERO, pFileName.indexOf(FeedConstants.DOT)
				+ FeedConstants.NUMBER_ONE);
		StringBuffer stringBuffer = new StringBuffer(); 
		stringBuffer = stringBuffer.append(newFileName).append(TRUStoreFeedReferenceConstants.XLS);
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		return stringBuffer.toString();

	}

	/**
	 * Move success file to success dir.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pSuccessDir
	 *            the success dir
	 * @param pTotalCount
	 *            the total count
	 * @param pSuccessRecordCount
	 *            the success record count
	 * @param pIsMail
	 *            the is mail
	 */
	public void moveSuccessFileToSuccessDir(String pFileName, String pProcessingDir, String pSuccessDir,
			int pTotalCount, int pSuccessRecordCount, boolean pIsMail) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pSuccessDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(pFileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
				if (pIsMail) {
					triggerFeedProcessedSuccessfullyEmail(pTotalCount, pSuccessRecordCount, pFileName);
				}
			}
		}
		getLogger().vlogDebug(
				"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
	}

	/**
	 * Trigger feed processed successfully email.
	 * 
	 * @param pTotalCount
	 *            the total count
	 * @param pSuccessRecordCount
	 *            the success record count
	 * @param pFileName
	 *            the file name
	 */
	public void triggerFeedProcessedSuccessfullyEmail(int pTotalCount, int pSuccessRecordCount, String pFileName) {
		getLogger()
				.vlogDebug(
						"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessfullyEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUStoreFeedReferenceConstants.JOB_SUCCESS;
		
		String subject = getFeedEnvConfiguration().getStoreFeedSuccessSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);		
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		getFeedEmailService().sendFeedSuccessEmail(FeedConstants.STORE_JOB_NAME, templateParameters);
		getLogger()
				.vlogDebug(
						"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessfullyEmail()");
	}

	/**
	 * Notify file dose not exits.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 */
	public void notifyFileDoseNotExits(String pSourceDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUStoreFeedReferenceConstants.JOB_FAILED_MISSING_FILE;
		String priority = TRUStoreFeedReferenceConstants.SEQ_MISS;
		
		String subject = getFeedEnvConfiguration().getStoreFeedFailureSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, getFeedEnvConfiguration()
				.getDetailedExceptionForNoFeed());
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.STORE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}
	

	/**
	 * Gets the file sequence repository.
	 * 
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 * 
	 * @param pFileSequenceRepository
	 *            the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}
	
	/**
	 * Gets the feed store property.
	 * 
	 * @return the mFeedStoreProperties
	 */
	public TRUStoreFeedProperties getFeedStoreProperty() {
		return mFeedStoreProperty;
	}

	/**
	 * Sets the feed store property.
	 * 
	 * @param pFeedStoreProperty
	 *            - pFeedStoreProperty to set
	 */
	public void setFeedStoreProperty(TRUStoreFeedProperties pFeedStoreProperty) {
		this.mFeedStoreProperty = pFeedStoreProperty;
	}


}
