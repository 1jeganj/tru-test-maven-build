package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.filefilter.RegexFileFilter;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.store.TRUStoreFeedProperties;
import com.tru.logging.TRUAlertLogger;

/**
 * The class TRUStoreFeedFileDownloadTask will download the file from source to processing folder.
 * 
 * @version 1.1
 * 
 */

public class TRUStoreFeedFileDownloadTask extends FileDownloadTask {

	/**
	 * Property to hold destinationDirectory Path.
	 */
	private String mProcessingDir;
	/**
	 * Property to hold SourceDirectory Path.
	 */
	private String mSourceDir;

	/** The Un processed dir. */
	private String mUnProcessedDir;
	
	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	
	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The m feed store property. */
	public TRUStoreFeedProperties mFeedStoreProperty;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	/*
	 * This doExecute() method will download the file from Source Directory and place it in Destination Directory
	 * 
	 * @throws FileNotFoundException - Throws FileNotFoundException
	 * 
	 * @throws IOException - Throws IOException
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.tasklet.FileDownloadTask#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException, Exception {
		
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: execute()");
		String startDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		mSourceDir = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString());
		mProcessingDir = nullIfEmpty(getConfigValue(TRUStoreFeedReferenceConstants.PROCESSING_DIRECTORY).toString());
		mUnProcessedDir = nullIfEmpty(getConfigValue(TRUStoreFeedReferenceConstants.UN_PROCESS_DIRECTORY).toString());
		File sharedFolderFile = new File(mSourceDir); 
		if (sharedFolderFile.exists() && sharedFolderFile.isDirectory() && sharedFolderFile.canRead()){ 
			moveSrcToProcessingDir(startDate);
		} else if(!sharedFolderFile.exists() || !sharedFolderFile.isDirectory()){
			String messageSubject = TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS;
			logFailedMessage(startDate, TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS);
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!sharedFolderFile.canRead()) {
			String messageSubject = TRUStoreFeedReferenceConstants.PERMISSION_ISSUE;
			logFailedMessage(startDate, TRUStoreFeedReferenceConstants.PERMISSION_ISSUE);
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: execute()");
	}

	
	/**
	 * Move src to processing dir.
	 *
	 * @param pStartDate the start date
	 * @return true, if successful
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public boolean moveSrcToProcessingDir(String pStartDate) throws FileNotFoundException, IOException, RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: moveSrcToProcessingDir()");
		File sourceFolder = new File(mSourceDir);
		File processingFolder = new File(mProcessingDir);
		boolean filesExists = Boolean.FALSE;
		File processFile = null;
		File[] sourceFiles = null;
		String filePattern = null;
		if (sourceFolder != null && sourceFolder.exists() && sourceFolder.canRead()
				&& null != getConfigValue(TRUStoreFeedReferenceConstants.FEED_FILE_PATTERN)) {
			filePattern = getConfigValue(TRUStoreFeedReferenceConstants.FEED_FILE_PATTERN).toString();
			sourceFiles = getMatchedFileList(sourceFolder, filePattern);
			if (sourceFiles != null) {
				processFile = getLatestFile(sourceFiles);
				if (processFile != null) {
					setConfigValue(FeedConstants.FILENAME, processFile.getName());
					setConfigValue(FeedConstants.FILEPATH, mProcessingDir);
					setConfigValue(FeedConstants.FILETYPE, getExtenstion(processFile.getName()));
				}
			}
		}
		File unProcessFolder = new File(mUnProcessedDir);
		if (processingFolder != null && processingFolder.exists() && processingFolder.isDirectory() && processingFolder.canWrite()
				&& unProcessFolder != null && unProcessFolder.exists() && unProcessFolder.isDirectory() && unProcessFolder.canWrite()) {
			try {
				sourceFiles = null;
				moveAllFiles(sourceFolder, processingFolder.getCanonicalPath(), processFile,
						unProcessFolder.getCanonicalPath());
			} catch (FileNotFoundException e) {
				if(isLoggingError()) {
					logError("@Class: TRUStoreFeedFileDownloadTask : @Method: moveSrcToProcessingDir(): ", e);	
				}
				logFailedMessage(pStartDate, TRUStoreFeedReferenceConstants.PERMISSION_ISSUE);
			} catch (IOException ioe) {
				if(isLoggingError()) {
					logError("@Class: TRUStoreFeedFileDownloadTask : @Method: moveSrcToProcessingDir(): ", ioe);
				}
				logFailedMessage(pStartDate, TRUStoreFeedReferenceConstants.PERMISSION_ISSUE);

			}
		} else if(!processingFolder.exists() || !processingFolder.isDirectory()){ 
			String messageSubject = TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(processingFolder.getPath(), messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!processingFolder.canRead()){
			String messageSubject = TRUStoreFeedReferenceConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(processingFolder.getPath(), messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		} else if(!unProcessFolder.exists() || !unProcessFolder.isDirectory()){
			String messageSubject = TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(unProcessFolder.getPath(), messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!unProcessFolder.canRead()){
			String messageSubject = TRUStoreFeedReferenceConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(unProcessFolder.getPath(), messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: moveSrcToProcessingDir()");
		return filesExists;
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}

	/**
	 * This method is used to move all file from shared directory to local directory.
	 *
	 * @param pSharedDir            - shared directory
	 * @param pProcessingDir            the processing dir
	 * @param pProcessFile the process file
	 * @param pUnProcessFolder            the un process folder
	 * @throws FileNotFoundException             - Throws FileNotFoundException
	 * @throws IOException             - Throws IOException
	 */
	public void moveAllFiles(File pSharedDir, String pProcessingDir, File pProcessFile, String pUnProcessFolder)
			throws FileNotFoundException, IOException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: moveAllFiles()");
		File[] files = null;
		if (pSharedDir.exists() && pSharedDir.isDirectory()) {
			files = pSharedDir.listFiles();
		}
		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isFile()) {
					if (null != pProcessFile && file.getName().equals(pProcessFile.getName())) {
						moveFileToFolder(pSharedDir + FeedConstants.SLASH + file.getName(), pProcessingDir);
					} else {
						moveFileToFolder(pSharedDir + FeedConstants.SLASH + file.getName(), pUnProcessFolder);
					}
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: moveAllFiles()");
	}

	/**
	 * This method is used for moving the file from source directory to destination directory.
	 * 
	 * @param pFromFile
	 *            - String
	 * @param pToFolder
	 *            - String
	 * @return boolean - value
	 * @throws FileNotFoundException
	 *             - Throws FileNotFoundException
	 * @throws IOException
	 *             - Throws IOException
	 */
	public boolean moveFileToFolder(String pFromFile, String pToFolder) throws FileNotFoundException, IOException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: moveFileToFolder()");
		File fromFile = new File(pFromFile);
		File toFolder = new File(pToFolder);
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		boolean value = fromFile.renameTo(new File(toFolder, fromFile.getName()));
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: moveFileToFolder()");
		return value;
	}

	/**
	 * This method checks for the extension.
	 * 
	 * @param pName
	 *            the name
	 * @return extension
	 */
	public String getExtenstion(String pName) {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: getExtenstion()");
		String[] str = pName.split(TRUStoreFeedReferenceConstants.DOUBLE_SLASH_DOT);
		if (str.length > FeedConstants.NUMBER_ONE) {
			return str[str.length - FeedConstants.NUMBER_ONE];
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: getExtenstion()");
		return null;
	}

	/**
	 * Gets the matched files which are matched with regular expression pattern and also checks whether the file name
	 * contains respective environment name and feed name.
	 * 
	 * @param pDirectory
	 *            the directory
	 * @param pPattern
	 *            the pattern
	 * @return the matched file list
	 */
	public File[] getMatchedFileList(File pDirectory, String pPattern) {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: getMatchedFileList()");
		String fileNameTRUEnv = getConfigValue(TRUStoreFeedReferenceConstants.FILE_NAME_TRU_ENV).toString();
		String fileFeedName = getConfigValue(TRUStoreFeedReferenceConstants.FILE_FEED_NAME).toString();
		FileFilter filter = new RegexFileFilter(pPattern);
		File[] patternMatchfiles = pDirectory.listFiles(filter);
		List<File> finalFiles = null;
		if (patternMatchfiles != null) {
			finalFiles = new ArrayList<File>();
			for (File file : patternMatchfiles) {
				if (file.getName().contains(fileNameTRUEnv) && file.getName().contains(fileFeedName)) {
					finalFiles.add(file);
				}
			}
			File[] finalArray = null;
			if (null != finalFiles) {
				finalArray = new File[finalFiles.size()];
				int i = FeedConstants.NUMBER_ZERO;
				for (File finalFile : finalFiles) {
					finalArray[i] = finalFile;
					i++;
				}
			}
			getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: getMatchedFileList()");
			return finalArray;
		} else {
			getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: getMatchedFileList()");
			return null;
		}
	}

	/**
	 * Gets the latest file based on the file sequence number.
	 *
	 * @param pSourceFiles            the source files
	 * @return the latest file
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public File getLatestFile(File[] pSourceFiles) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedFileDownloadTask : @Method: getLatestFile()");
		int seqNo = FeedConstants.NUMBER_ZERO;
		String name = null;
		TreeMap<Integer, File> fileMap = new TreeMap<Integer, File>();
		List<Integer> fileSequenceList = new ArrayList<Integer>();
		for (File srcFile : pSourceFiles) {
			name = srcFile.getName();
			seqNo = Integer.parseInt(name.substring(name.lastIndexOf(FeedConstants.UNDER_SCORE)
					+ FeedConstants.NUMBER_ONE, name.lastIndexOf(FeedConstants.DOT)));
			fileMap.put(seqNo, srcFile);
			fileSequenceList.add(seqNo);
		}
		File returnFile = null;
		if (checkFileSequence(fileSequenceList)) {
			Collections.sort(fileSequenceList);
			returnFile = fileMap.get(fileSequenceList.get(FeedConstants.NUMBER_ZERO));
		}
		fileMap = null;
		getLogger().vlogDebug("End:@Class: TRUStoreFeedFileDownloadTask : @Method: getLatestFile()");
		return returnFile;
	}
	
	/**
	 * Check file sequence.
	 * 
	 * @param pFileSequenceList
	 *            the file sequence list
	 * @return the boolean
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private Boolean checkFileSequence(List<Integer> pFileSequenceList) throws FeedSkippableException, RepositoryException {

		getLogger().vlogDebug("Start @Class: TRUStoreFeedFileDownloadTask,  @method: checkFileSequence()");

		Boolean retVal = Boolean.FALSE;
		List<Integer> fileSequenceList = pFileSequenceList;
		if (!fileSequenceList.isEmpty()) {
			Collections.sort(fileSequenceList);
			RepositoryItem repoItem = getFileSequenceRepository().getItem(TRUStoreFeedReferenceConstants.STORE_FEED,
					TRUStoreFeedReferenceConstants.FEED_FILE_SEQUENCE);
			if (repoItem != null) {
				Integer seqNo = (Integer) repoItem.getPropertyValue(getFeedStoreProperty().getSeqProcessedPropertyName());
				if (fileSequenceList.get(TRUStoreFeedReferenceConstants.NUMBER_ZERO) != (seqNo + TRUStoreFeedReferenceConstants.NUMBER_ONE)) {
					String messageSubject = getFeedEnvConfiguration().getStoreFeedFilesSequenceNoMatch();
					notifyFileDoseNotExits(mSourceDir, messageSubject);
					getLogger().logError(TRUStoreFeedReferenceConstants.SPACE + getFeedEnvConfiguration().getStoreFeedFilesSequenceNoMatch());
					throw new FeedSkippableException(getPhaseName(), getStepName(), TRUStoreFeedReferenceConstants.FILE_SEQUENCE_NO_MATCH, null, null);
				}
			}
			retVal = Boolean.TRUE;
		}
		getLogger().vlogDebug("End @Class: TRUStoreFeedFileDownloadTask, @method: checkFileSequence()");
		return retVal;
	}
	
	/**
	 * Notify file dose not exits.
	 *
	 * @param pSourceDir            the source dir
	 * @param pMessageSubject the message subject
	 */
	public void notifyFileDoseNotExits(String pSourceDir, String pMessageSubject) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		
		String priority = getFeedEnvConfiguration().getFailureMailPriority2();
		String content = null;
		String detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		if (pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getPriceFeedFilesNoSequence())
				|| pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getStoreFeedFilesSequenceNoMatch())) {
			priority = TRUStoreFeedReferenceConstants.SEQ_MISS;
			content = TRUStoreFeedReferenceConstants.JOB_FAILED_MISSING_SEQ;
			detailException = getFeedEnvConfiguration().getDetailedExceptionForSeqMiss();
		} else if (pMessageSubject.equalsIgnoreCase(getFeedEnvConfiguration().getPriceNoFeedFileSubject())) {
			priority = TRUStoreFeedReferenceConstants.SEQ_MISS;
			content = TRUStoreFeedReferenceConstants.JOB_FAILED_MISSING_FILE;
			detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		} else if(pMessageSubject.contains(TRUStoreFeedReferenceConstants.FOLDER_NOT_EXISTS)){
			content = TRUStoreFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_1 + pSourceDir + TRUStoreFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
			detailException = pSourceDir + TRUStoreFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
		} else if(pMessageSubject.contains(TRUStoreFeedReferenceConstants.PERMISSION_ISSUE)){
			content = TRUStoreFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_1 + pSourceDir + TRUStoreFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
			detailException = pSourceDir + TRUStoreFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
		}
		
		String sub = getFeedEnvConfiguration().getStoreFeedFailureSub();
		String[] args = new String[TRUStoreFeedReferenceConstants.NUMBER_TWO];
		args[TRUStoreFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUStoreFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUStoreFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(sub, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.STORE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUStoreFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
	}

	/**
	 * Gets the file sequence repository.
	 * 
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 * 
	 * @param pFileSequenceRepository
	 *            the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}
	
	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}
	
	/**
	 * Gets the feed store property.
	 * 
	 * @return the mFeedStoreProperties
	 */
	public TRUStoreFeedProperties getFeedStoreProperty() {
		return mFeedStoreProperty;
	}

	/**
	 * Sets the feed store property.
	 * 
	 * @param pFeedStoreProperty
	 *            - pFeedStoreProperty to set
	 */
	public void setFeedStoreProperty(TRUStoreFeedProperties pFeedStoreProperty) {
		this.mFeedStoreProperty = pFeedStoreProperty;
	}
}
