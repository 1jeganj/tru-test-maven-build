package com.tru.feedprocessor.tol;

/**
 * The Class TRUStoreFeedReferenceConstants will have all store related constant properties..
 *
 * @author  PA
 * @version 1.1
 * 
 */
public class TRUStoreFeedReferenceConstants {
	
	/**
	 * The Constant STORE_ENTITY_NAME.
	 */
	public static final String STORE_ENTITY_NAME = "store";
	
	/**
	 * The Constant WEEKDAY_ENTITY_NAME.
	 */
	public static final String WEEKDAY_ENTITY_NAME = "tradingHours";
	
	/**
	 * The Constant SUCCESS_DIRECTORY.
	 */
	public static final String SUCCESS_DIRECTORY = "successDirectory";
	
	/**
	 * The Constant ERROR_DIRECTORY.
	 */
	public static final String ERROR_DIRECTORY = "errorDirectory";
	
	/**
	 * The Constant PREVIOUS_DAY_DIRECTORY.
	 */
	public static final String PREVIOUS_DAY_DIRECTORY = "storePreviousDayFilePath";
	
	/**
	 * The Constant PREVIOUS_DAY_FILE_NAME.
	 */
	public static final String PREVIOUS_DAY_FILE_NAME = "storePreviousDayFileName";
	
	/**
	 * The Constant FILE_PATH.
	 */
	public static final String FILE_PATH = "filePath";
	
	/**
	 * The Constant PROCESSING_DIRECTORY.
	 */
	public static final String PROCESSING_DIRECTORY = "processingDirectory";
	
	/**
	 * The Constant BAD_DIRECTORY.
	 */
	public static final String BAD_DIRECTORY = "badDirectory";
	
	/**
	 * The Constant SLASH.
	 */
	public static final String SLASH = "/";
	
	/**
	 * The Constant DOUBLE_SLASH.
	 */
	public static final String DOUBLE_SLASH_DOT = "\\.";
	
	/**
	 * The Constant DOUBLE_SLASH.
	 */
	public static final String DOUBLE_SLASH = "\\";
	
	/**
	 * The Constant ONE.
	 */
	public static final String ONE = "1";
	
	/**
	 * The Constant MONDAY.
	 */
	public static final String MONDAY = "Monday";
	
	/**
	 * The Constant TUESDAY.
	 */
	public static final String TUESDAY = "Tuesday";
	
	/**
	 * The Constant WEDNESDAY.
	 */
	public static final String WEDNESDAY = "Wednesday";
	
	/**
	 * The Constant THURSDAY.
	 */
	public static final String THURSDAY = "Thursday";
	
	/**
	 * The Constant FRIDAY.
	 */
	public static final String FRIDAY = "Friday";
	
	/**
	 * The Constant ONE.
	 */
	public static final String SATURADAY = "Saturday";
	
	/**
	 * The Constant SUNDAY.
	 */
	public static final String SUNDAY = "Sunday";
	
	/**
	 * The Constant FILE_NOT_FOUND_EXCEPTION.
	 */
	public static final String FILE_NOT_FOUND_EXCEPTION = "FileNotFoundException";
	
	/**
	 * The Constant IOEXCEPTION.
	 */
	public static final String IOEXCEPTION = "IOException";
	
	/**
	 * The Constant FILE_TYPE.
	 */
	public static final String FILE_TYPE = "fileType";
	
	/**
	 * The Constant INVALID_RECORD.
	 */
	public static final String INVALID_RECORD = "Invalid Record:";
	
	/**
	 * The Constant STORE_FEED_FILE_VALIDATION_STEP.
	 */
	public static final String STORE_FEED_FILE_VALIDATION_STEP = "storeFeedFileValidationStep";
	
	/**
	 * The Constant STORE_FEED_FILE_EXISTS_STEP.
	 */
	public static final String STORE_FEED_FILE_EXISTS_STEP = "storeFeedFileExistsStep";
	
	/**
	 * The Constant STORE_FEED_ADD_UPDATE_STEP.
	 */
	public static final String STORE_FEED_ADD_UPDATE_STEP = "storeFeedAddUpdateStep";
	
	/** The Constant XLS. */
	public static final String XLS = "xls";
	
	/** The Constant SHEET. */
	public static final String SHEET = "sheet";
	
	/** The Constant REQUIRED_LOCATION_NAME. */
	public static final String REQUIRED_LOCATION_NAME = "Invalid Record: Location Name Is Required";
	
	/** The Constant REQUIRED_LOCATION_CODE. */
	public static final String REQUIRED_LOCATION_CODE = "Invalid Record: Location Code Is Required";

	/** The Constant REQUIRED_CHAIN_CODE. */
	public static final String REQUIRED_CHAIN_CODE = "Invalid Record: Chain Code Is Required";

	/** The Constant REQUIRED_WAREHOUSE_LOCATION_CODE. */
	public static final String REQUIRED_WAREHOUSE_LOCATION_CODE = "Invalid Record: WareHouse Location Code Is Required";

	/** The Constant REQUIRED_GEO_CODE. */
	public static final String REQUIRED_GEO_CODE = "Invalid Record: Geo Code Is Required";

	/** The Constant REQUIRED_GEO_ZIP. */
	public static final String REQUIRED_GEO_ZIP = "Invalid Record: Geo Zip Is Required";

	/** The Constant REQUIRED_ENTERPRISE_ZONE. */
	public static final String REQUIRED_ENTERPRISE_ZONE = "Invalid Record: Enterprise Zone Is Required";

	/** The Constant REQUIRED_ADDRESS1. */
	public static final String REQUIRED_ADDRESS1 = "Invalid Record: Address1 Is Required";

	/** The Constant REQUIRED_CITY. */
	public static final String REQUIRED_CITY = "Invalid Record: City Is Required";

	/** The Constant REQUIRED_STATE. */
	public static final String REQUIRED_STATE = "Invalid Record: State Is Required";
	
	/** The Constant REQUIRED_STATE. */
	public static final String REQUIRED_OPEN_DATE = "Invalid Record: Open Date Is Required";
	
	/** The Constant REQUIRED_STATE. */
	public static final String UPDATED_DATE_FORMAT = "MM-dd-yyyy";
	
	/** The Constant REQUIRED_STATE. */
	public static final String XML_DATE_FORMAT = "yyyy-MM-dd";
	
	/** The Constant REQUIRED_STATE. */
	public static final String TAXONOMY_P_THREE_ALERT = "Priority 3";
	
	/** The Constant REQUIRED_STATE. */
	public static final String TAXONOMY_P_TWO_ALERT = "Priority 2";
	
	/** The Constant REQUIRED_STATE. */
	public static final String TAXONOMY_P_ONE_ALERT = "Priority 1";
	
	/** property to hold NUMBER_HUNDRED. */
	public static final long NUMBER_HUNDRED = 100;
	
	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";
	
	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;
	
	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;
	
	/** The Constant UN_PROCESS_DIRECTORY. */
	public static final String UN_PROCESS_DIRECTORY = "unProcessDirectory";

	/** The Constant SCHEMA_FILE_XSD_URI. */
	public static final String SCHEMA_FILE_XSD_URI = "schemaFileXsdUri";
	
	/** The Constant FEED_FILE_PATTERN. */
	public static final String FEED_FILE_PATTERN = "feedFilePattern";
	
	/** The Constant FILE_NAME_TRU_ENV. */
	public static final String FILE_NAME_TRU_ENV = "fileNameTRUEnv";
	
	/** The Constant FILE_FEED_NAME. */
	public static final String FILE_FEED_NAME = "fileFeedName";
	
	/** The Constant for Space. */
	public static final String SPACE = " ";
	
	/** The Constant for Space. */
	public static final String SOFT_DELETE_STORE = "X";
	
	/** The Constant for STRING_Y. */
	public static final String STRING_Y = "Y";
	
	/** The Constant for STORE_SERVICES. */
	public static final String STORE_SERVICES = "storeServices";
	
	/** The Constant for STORE_SERVICES. */
	public static final String SERVICE = "serviceName";
	
	/** The Constant for ONLINE_LAYAWAY. */
	public static final String ONLINE_LAYAWAY = "online-layaway";
	
	/** The Constant for PARENTING_CLASSES. */
	public static final String PARENTING_CLASSES = "parenting-classes";
	
	/** The Constant for IN_STORE_PICKUP. */
	public static final String IN_STORE_PICKUP = "in-store-pickup";
	
	/** The Constant for INVENTORY_SEARCH_ONLINE. */
	public static final String INVENTORY_SEARCH_ONLINE = "inventory-search-online";
	
	/** The Constant for SHIP_FROM_STORE. */
	public static final String SHIP_FROM_STORE = "ship-from-store";
	
	/** The Constant for PERSONAL_REGISTRY_ADVISOR. */
	public static final String PERSONAL_REGISTRY_ADVISOR = "personal-registry-advisor";
	
	/** The Constant for KIDS_CLOTHES. */
	public static final String KIDS_CLOTHES = "kids-clothes";
	
	/** The Constant for BABIESRUS_EXPRESS. */
	public static final String BABIESRUS_EXPRESS = "babiesrus-express";
	
	/** The Constant for TOYSRUS_EXPRESS. */
	public static final String TOYSRUS_EXPRESS = "toysrus-express";
	
	/** The Constant for SIDEBYSIDE_STORE. */
	public static final String SIDEBYSIDE_STORE = "sidebyside-store";
	
	/** The Constant for CARSEAT_INSTALLATION. */
	public static final String CARSEAT_INSTALLATION = "car-seat-installation";
	
	/** The Constant for CREATE_AND_TAKE_ACTIVITIES. */
	public static final String CREATE_AND_TAKE_ACTIVITIES = "create-and-take-activities";
	
	/** The Constant for MATERNITY_CLOTHING. */
	public static final String MATERNITY_CLOTHING = "maternity-clothing";
	
	/** The Constant for WI_FI. */
	public static final String WI_FI = "wi-fi";
	
	/** The Constant SOURCE_DIRECTORY. */
	public static final String SOURCE_DIRECTORY = "sourceDirectory";
	
	/** The Constant for ACTIVE_STORE. */
	public static final String ACTIVE_STORE = "S";
	
	/** The Constant for INTEGER_ONE. */
	public static final String INTEGER_ONE = "1";
	
	/** The Constant STORE_FEED. */
	public static final String STORE_FEED = "storefeed";

	/** The Constant FEED_FILE_SEQUENCE. */
	public static final String FEED_FILE_SEQUENCE = "feedfilesequence";

	/** The Constant FILE_SEQUENCE_MISSING. */
	public static final String FILE_SEQUENCE_MISSING = "File sequence missing";

	/** The Constant FILE_SEQUENCE_NO_MATCH. */
	public static final String FILE_SEQUENCE_NO_MATCH = "File sequence no match";

	/** The Constant SEQ_MISS. */
	public static final String SEQ_MISS = "sequenceMiss";

	/** The Constant JOB_FAILED_MISSING_SEQ. */
	public static final String JOB_FAILED_MISSING_SEQ = "Job Failed - File with a missing sequence number";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_MISSING_FILE = "Job Failed - Missing File";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_INVALID_FILE = "Job Failed - XML - XSD Validation Errors";

	/** The Constant JOB_FAILED_FILE_CORRUPT. */
	public static final String JOB_FAILED_FILE_CORRUPT = "Job Failed - Input File is Corrupt";

	/** The Constant JOB_FAILED_THRESHOLD. */
	public static final String JOB_FAILED_THRESHOLD1 = "Job Failed - More than {0}% Invalid Records Found in inbound Feed";

	/** The Constant JOB_FAILED_THRESHOLD. */
	public static final String JOB_FAILED_THRESHOLD2 = "Job gets processed successfully but with errors Threshold >={0} and <={0}%";

	/** The Constant FILE_INVALID. */
	public static final String FILE_INVALID = "fileInvalid";

	/** The Constant JOB_SUCCESS. */
	public static final String JOB_SUCCESS = "File has been successfully processed";

	/** The Constant BUT_WITH_ERRORS. */
	public static final String BUT_WITH_ERRORS = " but with some errors. Please find mail attachment for the errors.";

	/** The Constant PROCESS_FILE_SUCCESS. */
	public static final String PROCESS_FILE_SUCCESS = "File has been successfully processed";

	/** The Constant FILE_PROCESSES_WITH_ERRORS. */
	public static final String FILE_PROCESSED_WITH_ERRORS = "File has been successfully processed, but with some errors. Please find mail attachment for the errors.";

	/** The Constant NUMBER_TWO. */
	public static final int NUMBER_TWO = 2;

	/** The Constant INVALID_FILE. */
	public static final String INVALID_FILE = "Input File is Corrupt.";

	/** The Constant INVALID_STORE_DETAILEDEXP. */
	public static final String INVALID_STORE_DETAILEDEXP = "Store Feed file failed to validate with XSD.";

	/** The Constant THRESHOLD_STORE_DETAILEDEXP. */
	public static final String THRESHOLD_STORE_DETAILEDEXPP2 = "Store feed file processing failed due to more than 40% invalid error records found.";

	/** The Constant THRESHOLD_STORE_DETAILEDEXP. */
	public static final String THRESHOLD_STORE_DETAILEDEXPP3 = "Store feed file processing failed due to {0}% to {1}% invalid error records found.";

	/** The Constant ZERO_BRACES. */
	public static final String ZERO_BRACES = "{0}";

	/** The Constant SUB_DATE_FORMAT. */
	public static final String SUB_DATE_FORMAT = "MMddyyyy-HH:mm:ss";
	
	/** The Constant ZERO_DOUBLE. */
	public static final double ZERO_DOUBLE = 0.0;
	
	/** The property to hold FILE_NOT_VALID. */
	public static final String FILE_NOT_VALID = "        File is NOT valid  :  Reason is  --> ";
	
	/** The Constant PERMISSION_ISSUE. */
	public static final String PERMISSION_ISSUE = "Permission issue";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_1. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_1= "Job Failed - Folder ";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_2. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_2 = " permissions issues";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_1. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_1= "Job Failed - Directory/folder ";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_2. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_2= " structure not available";

	/** The Constant FOLDER_NOT_EXISTS. */
	public static final String FOLDER_NOT_EXISTS = "Folder not exists";
	
	/** The Constant FILE_DOWNLOAD. */
	public static final String FILE_DOWNLOAD = "File Download";
	
	/** The Constant FILE_PARSING. */
	public static final String FILE_PARSING = "File Parsing";
	
	/** The Constant FILE_VALIDATION. */
	public static final String FILE_VALIDATION = "File Validation";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant TIME_TAKEN. */
	public static final String TIME_TAKEN = "Time Taken";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant START_DATE. */
	public static final String START_DATE = "Start Date";

	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant FILES_MOVED_SUCCESS. */
	public static final String FILES_MOVED_SUCCESS = "Files Moved successfully";
	
	/** The Constant NO_MATCH_FIELS. */
	public static final String NO_MATCH_FIELS = "No Match Files";
	
	/** The Constant FILES_VALID. */
	public static final String FILES_VALID = "Files are valid";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_COMPLETED = "Feed completed";
	
	/** The Constant FILES_WRITING. */
	public static final String FILES_WRITING = "Files Writing";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "Feed Status";
	
	/** The Constant FEED_FAILED. */
	public static final String FEED_FAILED = "Feed Failed";
	
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROCESS = "Feed Process";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";
	
	/** The Constant PARSE_TAX. */
	public static final String PARSE_TAX = "Parsing WebsiteTaxonomy File";
	
	/** The Constant PARSE_REG. */
	public static final String PARSE_REG = "Parsing RegistryCategorization File";
	
	/** The Constant PARSE_PROD. */
	public static final String PARSE_PROD = "Parsing ProductCanonical File";
	
	/** The Constant FILE_NO_SEQ. */
	public static final String FILE_NO_SEQ = "File is not in sequence with last processed";
	
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "MMddyyyy'-'HH:mm:ss";
	

}
