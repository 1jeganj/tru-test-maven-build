package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="manager" type="{}String_Maxlength_25" minOccurs="0"/>
 *         &lt;element name="e-mail" type="{}String_Maxlength_50" minOccurs="0"/>
 *         &lt;element name="fax" type="{}String_Maxlength_50" minOccurs="0"/>
 *         &lt;element name="contact-number" type="{}String_Maxlength_20" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * @author PA
 * @version 1.0
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "contact-details")
public class ContactDetails extends BaseFeedProcessVO {

    /** The m manager. */
	@XmlElement(name = "manager")
	private String mManager;
    
    /** The m e mail. */
    @XmlElement(name = "e-mail")
    private String mEMail;
    
    /** The m fax. */
    @XmlElement(name = "fax")
    private String mFax;
    
    /** The m contact number. */
    @XmlElement(name = "contact-number")
    private String mContactNumber;

    /**
     * Gets the value of the manager property.
     *
     * @return the manager
     * possible object is
     * {@link String }
     */
    public String getManager() {
        return mManager;
    }

    /**
     * Sets the value of the manager property.
     *
     * @param pValue the new manager
     * {@link String }
     */
    public void setManager(String pValue) {
        this.mManager = pValue;
    }

    /**
     * Gets the value of the eMail property.
     *
     * @return the e mail
     * possible object is
     * {@link String }
     */
    public String getEMail() {
        return mEMail;
    }

    /**
     * Sets the value of the eMail property.
     *
     * @param pValue the new e mail
     * {@link String }
     */
    public void setEMail(String pValue) {
        this.mEMail = pValue;
    }

    /**
     * Gets the value of the fax property.
     *
     * @return the fax
     * possible object is
     * {@link String }
     */
    public String getFax() {
        return mFax;
    }

    /**
     * Sets the value of the fax property.
     *
     * @param pValue the new fax
     * {@link String }
     */
    public void setFax(String pValue) {
        this.mFax = pValue;
    }

    /**
     * Gets the value of the contactNumber property.
     *
     * @return the contact number
     * possible object is
     * {@link String }
     */
    public String getContactNumber() {
        return mContactNumber;
    }

    /**
     * Sets the value of the contactNumber property.
     *
     * @param pValue the new contact number
     * {@link String }
     */
    public void setContactNumber(String pValue) {
        this.mContactNumber = pValue;
    }

}
