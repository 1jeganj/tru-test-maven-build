package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="monday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="monday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="tuesday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="tuesday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="wednesday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="wednesday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="thursday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="thursday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="friday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="friday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="saturday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="saturday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="sunday-open" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="sunday-close" type="{}Integer_Maxlength_4" minOccurs="0"/>
 *         &lt;element name="timezone" type="{}String_Maxlength_1" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * @author PA
 * @version 1.0
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "store-hours")
public class StoreHours extends BaseFeedProcessVO {
	
	

    /** The m monday open. */
    @XmlElement(name = "monday-open")
    private Integer mMondayOpen;
    
    /** The m monday close. */
    @XmlElement(name = "monday-close")
    private Integer mMondayClose;
    
    /** The m tuesday open. */
    @XmlElement(name = "tuesday-open")
    private Integer mTuesdayOpen;
    
    /** The m tuesday close. */
    @XmlElement(name = "tuesday-close")
    private Integer mTuesdayClose;
    
    /** The m wednesday open. */
    @XmlElement(name = "wednesday-open")
    private Integer mWednesdayOpen;
    
    /** The m wednesday close. */
    @XmlElement(name = "wednesday-close")
    private Integer mWednesdayClose;
    
    /** The m thursday open. */
    @XmlElement(name = "thursday-open")
    private Integer mThursdayOpen;
    
    /** The m thursday close. */
    @XmlElement(name = "thursday-close")
    private Integer mThursdayClose;
    
    /** The m friday open. */
    @XmlElement(name = "friday-open")
    private Integer mFridayOpen;
    
    /** The m friday close. */
    @XmlElement(name = "friday-close")
    private Integer mFridayClose;
    
    /** The m saturday open. */
    @XmlElement(name = "saturday-open")
    private Integer mSaturdayOpen;
    
    /** The m saturday close. */
    @XmlElement(name = "saturday-close")
    private Integer mSaturdayClose;
    
    /** The m sunday open. */
    @XmlElement(name = "sunday-open")
    private Integer mSundayOpen;
    
    /** The m sunday close. */
    @XmlElement(name = "sunday-close")
    private Integer mSundayClose;
    
    /** The m timezone. */
    @XmlElement(name = "timezone")
    private String mTimezone;

    /**
     * Gets the pValue of the mondayOpen property.
     *
     * @return the monday open
     * possible object is
     * {@link Integer }
     */
    public Integer getMondayOpen() {
        return mMondayOpen;
    }

    /**
     * Sets the pValue of the mMondayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMondayOpen(Integer pValue) {
        this.mMondayOpen = pValue;
    }

    /**
     * Gets the pValue of the mMondayClose property.
     *
     * @return the monday close
     * possible object is
     * {@link Integer }
     */
    public Integer getMondayClose() {
        return mMondayClose;
    }

    /**
     * Sets the pValue of the mMondayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMondayClose(Integer pValue) {
        this.mMondayClose = pValue;
    }

    /**
     * Gets the pValue of the tuesdayOpen property.
     *
     * @return the tuesday open
     * possible object is
     * {@link Integer }
     */
    public Integer getTuesdayOpen() {
        return mTuesdayOpen;
    }

    /**
     * Sets the pValue of the tuesdayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTuesdayOpen(Integer pValue) {
        this.mTuesdayOpen = pValue;
    }

    /**
     * Gets the pValue of the tuesdayClose property.
     *
     * @return the tuesday close
     * possible object is
     * {@link Integer }
     */
    public Integer getTuesdayClose() {
        return mTuesdayClose;
    }

    /**
     * Sets the pValue of the tuesdayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTuesdayClose(Integer pValue) {
        this.mTuesdayClose = pValue;
    }

    /**
     * Gets the pValue of the wednesdayOpen property.
     *
     * @return the wednesday open
     * possible object is
     * {@link Integer }
     */
    public Integer getWednesdayOpen() {
        return mWednesdayOpen;
    }

    /**
     * Sets the pValue of the wednesdayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWednesdayOpen(Integer pValue) {
        this.mWednesdayOpen = pValue;
    }

    /**
     * Gets the pValue of the wednesdayClose property.
     *
     * @return the wednesday close
     * possible object is
     * {@link Integer }
     */
    public Integer getWednesdayClose() {
        return mWednesdayClose;
    }

    /**
     * Sets the pValue of the wednesdayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWednesdayClose(Integer pValue) {
        this.mWednesdayClose = pValue;
    }

    /**
     * Gets the pValue of the thursdayOpen property.
     *
     * @return the thursday open
     * possible object is
     * {@link Integer }
     */
    public Integer getThursdayOpen() {
        return mThursdayOpen;
    }

    /**
     * Sets the pValue of the thursdayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThursdayOpen(Integer pValue) {
        this.mThursdayOpen = pValue;
    }

    /**
     * Gets the pValue of the thursdayClose property.
     *
     * @return the thursday close
     * possible object is
     * {@link Integer }
     */
    public Integer getThursdayClose() {
        return mThursdayClose;
    }

    /**
     * Sets the pValue of the thursdayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setThursdayClose(Integer pValue) {
        this.mThursdayClose = pValue;
    }

    /**
     * Gets the pValue of the fridayOpen property.
     *
     * @return the friday open
     * possible object is
     * {@link Integer }
     */
    public Integer getFridayOpen() {
        return mFridayOpen;
    }

    /**
     * Sets the pValue of the fridayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFridayOpen(Integer pValue) {
        this.mFridayOpen = pValue;
    }

    /**
     * Gets the pValue of the fridayClose property.
     *
     * @return the friday close
     * possible object is
     * {@link Integer }
     */
    public Integer getFridayClose() {
        return mFridayClose;
    }

    /**
     * Sets the pValue of the fridayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFridayClose(Integer pValue) {
        this.mFridayClose = pValue;
    }

    /**
     * Gets the pValue of the saturdayOpen property.
     *
     * @return the saturday open
     * possible object is
     * {@link Integer }
     */
    public Integer getSaturdayOpen() {
        return mSaturdayOpen;
    }

    /**
     * Sets the pValue of the saturdayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSaturdayOpen(Integer pValue) {
        this.mSaturdayOpen = pValue;
    }

    /**
     * Gets the pValue of the saturdayClose property.
     *
     * @return the saturday close
     * possible object is
     * {@link Integer }
     */
    public Integer getSaturdayClose() {
        return mSaturdayClose;
    }

    /**
     * Sets the pValue of the saturdayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSaturdayClose(Integer pValue) {
        this.mSaturdayClose = pValue;
    }

    /**
     * Gets the pValue of the sundayOpen property.
     *
     * @return the sunday open
     * possible object is
     * {@link Integer }
     */
    public Integer getSundayOpen() {
        return mSundayOpen;
    }

    /**
     * Sets the pValue of the sundayOpen property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSundayOpen(Integer pValue) {
        this.mSundayOpen = pValue;
    }

    /**
     * Gets the pValue of the sundayClose property.
     *
     * @return the sunday close
     * possible object is
     * {@link Integer }
     */
    public Integer getSundayClose() {
        return mSundayClose;
    }

    /**
     * Sets the pValue of the sundayClose property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSundayClose(Integer pValue) {
        this.mSundayClose = pValue;
    }

    /**
     * Gets the pValue of the timezone property.
     *
     * @return the timezone
     * possible object is
     * {@link String }
     */
    public String getTimezone() {
        return mTimezone;
    }

    /**
     * Sets the pValue of the timezone property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimezone(String pValue) {
        this.mTimezone = pValue;
    }
    
    /**
 	 * Gets the trading hours.
 	 *
 	 * @return tradingHours
 	 */
	 public String getEntityName(){
		 	return FeedConstants.TRADINGHOURS;
		}

}
