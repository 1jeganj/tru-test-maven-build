package com.tru.feedprocessor.tol.store.processor;

import java.util.List;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.store.TRUStoreFeedProperties;
import com.tru.feedprocessor.tol.store.feedvo.Store;
import com.tru.feedprocessor.tol.store.feedvo.StoreHours;

/**
 * The Class TRUStoreItemValueChangedInspector will inspect for each repository item level properties if any property
 * changed then that record will is processed to update if no changes then that record is skipped.
 */
public class TRUStoreItemValueChangedInspector implements IItemValueChangeInspector {

	/** The m feed store property. */
	public TRUStoreFeedProperties mFeedStoreProperty;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/**
	 * Gets the feed store property.
	 * 
	 * @return the mFeedStoreProperties
	 */
	public TRUStoreFeedProperties getFeedStoreProperty() {
		return mFeedStoreProperty;
	}

	/**
	 * Sets the feed store property.
	 * 
	 * @param pFeedStoreProperty
	 *            - pFeedStoreProperty to set
	 */
	public void setFeedStoreProperty(TRUStoreFeedProperties pFeedStoreProperty) {
		this.mFeedStoreProperty = pFeedStoreProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector
	 * #isUpdated(com.tru.feedprocessor .tol.base.vo.BaseFeedProcessVO,
	 * atg.repository.RepositoryItem)
	 */
	@Override
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem) throws FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreItemValueChangedInspector : @Method: isUpdated():: store id:: {0}" , pRepositoryItem.getRepositoryId());
		Store storeVO = (Store) pBaseFeedProcessVO;
		boolean retVal = Boolean.FALSE;

		if (storeVO.getStoreDetail() != null && StringUtils.isNotEmpty(storeVO.getStoreDetail().getLocationName())
				&& !storeVO.getStoreDetail().getLocationName().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getName()))) {
			return true;
		}
		if (storeVO.getStoreDetail() != null && StringUtils.isNotEmpty(storeVO.getStoreDetail().getChainCode())
				&& !storeVO.getStoreDetail().getChainCode().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getChainCode()))) {
			return true;
		}
		if (storeVO.getStoreDetail() != null && StringUtils.isNotEmpty(storeVO.getStoreDetail().getLocationType())
				&& storeVO.getStoreDetail().getLocationType().equalsIgnoreCase(TRUStoreFeedReferenceConstants.ACTIVE_STORE)) {
			if (storeVO.getTaxware() != null && StringUtils.isNotEmpty(storeVO.getTaxware().getGeoCode())
					&& !storeVO.getTaxware().getGeoCode().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getGeoCode()))) {
				return true;
			}
			if (storeVO.getTaxware() != null && StringUtils.isNotEmpty(storeVO.getTaxware().getGeoZip())
					&& !storeVO.getTaxware().getGeoZip().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getGeoZip()))) {
				return true;
			}
			if (storeVO.getTaxware() != null && StringUtils.isNotEmpty(storeVO.getTaxware().getEnterpriseZone())
					&& !storeVO.getTaxware().getEnterpriseZone().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getEnterpriseZone()))) {
				return true;
			}
		}
		if (storeVO.getStoreDetail() != null
				&& !storeVO.getStoreDetail().getWarehouseLocationCode()
						.equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getWarehouseLocationCode()))) {
			return true;
		}
		
		if (storeVO.getStoreDetail() != null && StringUtils.isNotEmpty(storeVO.getStoreDetail().getOpenDate())
				&& !storeVO.getStoreDetail().getOpenDate().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getStartDate()))) {
			return true;
		}
		if (storeVO.getStoreDetail() != null && StringUtils.isNotEmpty(storeVO.getStoreDetail().getCloseDate())
				&& !storeVO.getStoreDetail().getCloseDate().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getEndDate()))) {
			return true;
		}
		retVal = validateAddressDetails(pRepositoryItem, storeVO);

		if (storeVO.getStoreHours() != null && validateHours(storeVO.getStoreHours(), pRepositoryItem)) {
			return true;
		}
		getLogger().vlogDebug("End:@Class: TRUStoreItemValueChangedInspector : @Method: isUpdated()");
		return retVal;
	}

	/**
	 * Validate address details.
	 * 
	 * @param pRepositoryItem
	 *            the repository item
	 * @param pStoreVO
	 *            the store vo
	 * @return true, if successful
	 */
	private boolean validateAddressDetails(RepositoryItem pRepositoryItem, Store pStoreVO) {
		boolean retVal = Boolean.FALSE;
		if (pStoreVO.getStoreDetail() != null
				&& !pStoreVO.getStoreDetail().getLocationType().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getLocationType()))) {
			return true;
		}
		if (pStoreVO.getAddress() != null && !pStoreVO.getAddress().getAddress1().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getAddress1()))) {
			return true;
		}
		if (pStoreVO.getAddress() != null && !pStoreVO.getAddress().getAddress2().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getAddress2()))) {
			return true;
		}
		if (pStoreVO.getAddress() != null && !pStoreVO.getAddress().getCity().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getCity()))) {
			return true;
		}
		if (pStoreVO.getAddress() != null && !pStoreVO.getAddress().getState().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getStateAddress()))) {
			return true;
		}
		if (pStoreVO.getAddress() != null && !pStoreVO.getAddress().getZipCode().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getPostalCode()))) {
			return true;
		}
		retVal = validateContactDetails(pRepositoryItem, pStoreVO);
		return retVal;
	}

	/**
	 * Validate contact details.
	 * 
	 * @param pRepositoryItem
	 *            the repository item
	 * @param pStoreVO
	 *            the store vo
	 * @return true, if successful
	 */
	private boolean validateContactDetails(RepositoryItem pRepositoryItem, Store pStoreVO) {
		if (pStoreVO.getContactDetails() != null
				&& !pStoreVO.getContactDetails().getContactNumber().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getPhoneNumber()))) {
			return true;
		}
		if (pStoreVO.getDirections() != null
				&& !pStoreVO.getDirections().getLatitude().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getLatitude()))) {
			return true;
		}
		if (pStoreVO.getDirections() != null
				&& !pStoreVO.getDirections().getLongitude().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getLongitude()))) {
			return true;
		}
		if (pStoreVO.getContactDetails() != null
				&& !pStoreVO.getContactDetails().getManager().equals(pRepositoryItem.getPropertyValue(getFeedStoreProperty().getManager()))) {
			return true;
		}
		return false;
	}

	/**
	 * Checks for a Validate hours changed or not.
	 * 
	 * @param pStoreHours
	 *            the store hours
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	private boolean validateHours(StoreHours pStoreHours, RepositoryItem pRepositoryItem) {
		getLogger().vlogDebug("Begin:@Class: TRUStoreItemValueChangedInspector : @Method: validateHours()");
		List<RepositoryItem> storeHour = null;
		if (pStoreHours != null && pRepositoryItem != null) {
			storeHour = (List<RepositoryItem>) pRepositoryItem.getPropertyValue(getFeedStoreProperty().getStoreHours());
			for (RepositoryItem hours : storeHour) {
				String day = (String) hours.getPropertyValue(getFeedStoreProperty().getDay());
				if (day.equals(TRUStoreFeedReferenceConstants.MONDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getMondayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getMondayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.TUESDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getTuesdayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getTuesdayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.WEDNESDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getWednesdayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getWednesdayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.THURSDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getThursdayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getThursdayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.FRIDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getFridayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getFridayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.SATURADAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getSaturdayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getSaturdayClose().equals(closingHours)) {
						return true;
					}
				} else if (day.equals(TRUStoreFeedReferenceConstants.SUNDAY)) {
					Double openingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getOpeningHours());
					Double closingHours = (Double) hours.getPropertyValue(getFeedStoreProperty().getClosingHours());
					if (!pStoreHours.getSundayOpen().equals(openingHours)) {
						return true;
					}
					if (!pStoreHours.getSundayClose().equals(closingHours)) {
						return true;
					}
				}

			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreItemValueChangedInspector : @Method: validateHours()");
		return false;
	}
}
