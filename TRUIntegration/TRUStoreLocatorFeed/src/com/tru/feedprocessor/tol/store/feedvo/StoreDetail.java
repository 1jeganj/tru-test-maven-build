package com.tru.feedprocessor.tol.store.feedvo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="location-name" type="{}String_Maxlength_55" minOccurs="0"/>
 *         &lt;element name="location-type" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="region" type="{}Integer_Maxlength_3" minOccurs="0"/>
 *         &lt;element name="district" type="{}Integer_Maxlength_3" minOccurs="0"/>
 *         &lt;element name="chain-code" type="{}String_Maxlength_3" minOccurs="0"/>
 *         &lt;element name="open-date" type="{}CustomISODate" minOccurs="0"/>
 *         &lt;element name="close-date" type="{}CustomISODate" minOccurs="0"/>
 *         &lt;element name="num-of-floors" type="{}Integer_Maxlength_2" minOccurs="0"/>
 *         &lt;element name="tax-rate" type="{}Decimal_6_5" minOccurs="0"/>
 *         &lt;element name="tax-rate-flag" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="warehouse-location-code" type="{}Integer_Maxlength_5" minOccurs="0"/>
 *         &lt;element name="fedex-code" type="{}String_Maxlength_25" minOccurs="0"/>
 *         &lt;element name="fulfilment-group" type="{}String_Maxlength_15" minOccurs="0"/>
 *         &lt;element name="facility-capacity" type="{}String_Maxlength_55" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * @author PA
 * @version 1.0
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "store-detail")
public class StoreDetail extends BaseFeedProcessVO {
	
    /** The m location name. */
    @XmlElement(name = "location-name")
    private String mLocationName;
    
    /** The m location type. */
    @XmlElement(name = "location-type")
    private String mLocationType;
    
    /** The m region. */
    @XmlElement(name = "region")
    private Integer mRegion;
    
    /** The m district. */
    @XmlElement(name = "district")
    private Integer mDistrict;
    
    /** The m chain code. */
    @XmlElement(name = "chain-code")
    private String mChainCode;
    
    /** The m open date. */
    @XmlElement(name = "open-date")
    private String mOpenDate;
    
    /** The m close date. */
    @XmlElement(name = "close-date")
    private String mCloseDate;
    
    /** The m num of floors. */
    @XmlElement(name = "num-of-floors")
    private Integer mNumOfFloors;
    
    /** The m tax rate. */
    @XmlElement(name = "tax-rate")
    private BigDecimal mTaxRate;
    
    /** The m tax rate flag. */
    @XmlElement(name = "tax-rate-flag")
    private String mTaxRateFlag;
    
    /** The m warehouse location code. */
    @XmlElement(name = "warehouse-location-code")
    private Integer mWarehouseLocationCode;
    
    /** The m fedex code. */
    @XmlElement(name = "fedex-code")
    private String mFedexCode;
    
    /** The m fulfilment group. */
    @XmlElement(name = "fulfilment-group")
    private String mFulfilmentGroup;
    
    /** The m facility capacity. */
    @XmlElement(name = "facility-capacity")
    private String mFacilityCapacity;
    /**
 	 * Gets the trading hours.
 	 *
 	 * @return tradingHours
 	 */
	public String getEntityName(){
		return TRUStoreFeedReferenceConstants.WEEKDAY_ENTITY_NAME;
	}
    /**
     * Gets the pValue of the locationName property.
     *
     * @return the location name
     * possible object is
     * {@link String }
     */
    public String getLocationName() {
        return mLocationName;
    }

    /**
     * Sets the pValue of the locationName property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String pValue) {
        this.mLocationName = pValue;
    }

    /**
     * Gets the pValue of the locationType property.
     *
     * @return the location type
     * possible object is
     * {@link String }
     */
    public String getLocationType() {
        return mLocationType;
    }

    /**
     * Sets the pValue of the locationType property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationType(String pValue) {
        this.mLocationType = pValue;
    }

    /**
     * Gets the pValue of the region property.
     *
     * @return the region
     * possible object is
     * {@link Integer }
     */
    public Integer getRegion() {
        return mRegion;
    }

    /**
     * Sets the pValue of the region property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRegion(Integer pValue) {
        this.mRegion = pValue;
    }

    /**
     * Gets the pValue of the district property.
     *
     * @return the district
     * possible object is
     * {@link Integer }
     */
    public Integer getDistrict() {
        return mDistrict;
    }

    /**
     * Sets the pValue of the district property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDistrict(Integer pValue) {
        this.mDistrict = pValue;
    }

    /**
     * Gets the pValue of the chainCode property.
     *
     * @return the chain code
     * possible object is
     * {@link String }
     */
    public String getChainCode() {
        return mChainCode;
    }

    /**
     * Sets the pValue of the chainCode property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainCode(String pValue) {
        this.mChainCode = pValue;
    }

    /**
     * Gets the pValue of the openDate property.
     *
     * @return the open date
     * possible object is
     * {@link String }
     */
    public String getOpenDate() {
        return mOpenDate;
    }

    /**
     * Sets the pValue of the openDate property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenDate(String pValue) {
        this.mOpenDate = pValue;
    }

    /**
     * Gets the pValue of the closeDate property.
     *
     * @return the close date
     * possible object is
     * {@link String }
     */
    public String getCloseDate() {
        return mCloseDate;
    }

    /**
     * Sets the pValue of the closeDate property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCloseDate(String pValue) {
        this.mCloseDate = pValue;
    }

    /**
     * Gets the pValue of the numOfFloors property.
     *
     * @return the num of floors
     * possible object is
     * {@link Integer }
     */
    public Integer getNumOfFloors() {
        return mNumOfFloors;
    }

    /**
     * Sets the pValue of the numOfFloors property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumOfFloors(Integer pValue) {
        this.mNumOfFloors = pValue;
    }

    /**
     * Gets the pValue of the taxRate property.
     *
     * @return the tax rate
     * possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getTaxRate() {
        return mTaxRate;
    }

    /**
     * Sets the pValue of the taxRate property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxRate(BigDecimal pValue) {
        this.mTaxRate = pValue;
    }

    /**
     * Gets the pValue of the taxRateFlag property.
     *
     * @return the tax rate flag
     * possible object is
     * {@link String }
     */
    public String getTaxRateFlag() {
        return mTaxRateFlag;
    }

    /**
     * Sets the pValue of the taxRateFlag property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRateFlag(String pValue) {
        this.mTaxRateFlag = pValue;
    }

    /**
     * Gets the pValue of the warehouseLocationCode property.
     *
     * @return the warehouse location code
     * possible object is
     * {@link Integer }
     */
    public Integer getWarehouseLocationCode() {
        return mWarehouseLocationCode;
    }

    /**
     * Sets the pValue of the warehouseLocationCode property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWarehouseLocationCode(Integer pValue) {
        this.mWarehouseLocationCode = pValue;
    }

    /**
     * Gets the pValue of the fedexCode property.
     *
     * @return the fedex code
     * possible object is
     * {@link String }
     */
    public String getFedexCode() {
        return mFedexCode;
    }

    /**
     * Sets the pValue of the fedexCode property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFedexCode(String pValue) {
        this.mFedexCode = pValue;
    }

    /**
     * Gets the pValue of the fulfilmentGroup property.
     *
     * @return the fulfilment group
     * possible object is
     * {@link String }
     */
    public String getFulfilmentGroup() {
        return mFulfilmentGroup;
    }

    /**
     * Sets the pValue of the fulfilmentGroup property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfilmentGroup(String pValue) {
        this.mFulfilmentGroup = pValue;
    }

    /**
     * Gets the pValue of the facilityCapacity property.
     *
     * @return the facility capacity
     * possible object is
     * {@link String }
     */
    public String getFacilityCapacity() {
        return mFacilityCapacity;
    }

    /**
     * Sets the pValue of the facilityCapacity property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilityCapacity(String pValue) {
        this.mFacilityCapacity = pValue;
    }

}
