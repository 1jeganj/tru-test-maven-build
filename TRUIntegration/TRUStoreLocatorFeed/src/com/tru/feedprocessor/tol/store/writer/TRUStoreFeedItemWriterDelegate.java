package com.tru.feedprocessor.tol.store.writer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.store.feedvo.Store;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class StoreFeedItemWriterDelegate will update or add the store item record based on business logic.
 * 
 * @author PA
 * @version 1.1
 * 
 */
public class TRUStoreFeedItemWriterDelegate extends StoreFeedItemWriterDelegate {

	/**
	 * The entity id map.
	 */
	private Map<String, Map<String, String>> mEntityIdMap = null;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * This method will check whether item need to update for add.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed processvo
	 *@throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception            
	 */
	public void doWrite(BaseFeedProcessVO pBaseFeedProcessVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedItemWriterDelegate : @Method: doWrite()");
		String lItemDiscriptorName = getItemDescriptorName(pBaseFeedProcessVO.getEntityName());
		if (mEntityIdMap != null
				&& mEntityIdMap.get(lItemDiscriptorName).get(pBaseFeedProcessVO.getRepositoryId()) != null) {
			updateItem(pBaseFeedProcessVO);
		} else {
			addItem(pBaseFeedProcessVO);
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedItemWriterDelegate : @Method: doWrite()");
	}


	/**
	 * This method will retrive the entity id map.
	 * 
	 */
	public void doOpen() {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedItemWriterDelegate : @Method: doOpen()");
		
		String startDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUStoreFeedReferenceConstants.FILES_WRITING);
		super.doOpen();

		AbstractFeedItemMapper mapper = (AbstractFeedItemMapper) getMapper();
		mapper.setFeedHelper(getFeedHelper());

		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
		getLogger().vlogDebug("End:@Class: TRUStoreFeedItemWriterDelegate : @Method: doOpen()");
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUStoreFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUStoreFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUStoreFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUStoreFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUStoreFeedReferenceConstants.STORE_FEED, TRUStoreFeedReferenceConstants.FEED_PROCESS, null, extenstions);
	}

	/**
	 * Update item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedItemWriterDelegate : @Method: updateItem()");
		MutableRepositoryItem lCurrentItem = null;
		String locationType = null;
		Store store = (Store) pFeedVO;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		if (lItemDiscriptorName != null) {
			if (store.getStoreDetail() != null) {
				locationType = store.getStoreDetail().getLocationType();
			}
			if (!StringUtils.isEmpty(locationType) && locationType.equalsIgnoreCase(TRUStoreFeedReferenceConstants.SOFT_DELETE_STORE)) {
				lCurrentItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(
						mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()), lItemDiscriptorName);
				if (lCurrentItem != null) {
					getRepository(lRepositoryName).removeItem(lCurrentItem.getRepositoryId(), lItemDiscriptorName);
				}
			} else {
				IFeedItemMapper lFeedItemMapper = (IFeedItemMapper) getMapper();
				lCurrentItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(
						mEntityIdMap.get(lItemDiscriptorName).get(pFeedVO.getRepositoryId()), lItemDiscriptorName);
				beforeUpdateItem(pFeedVO, lCurrentItem);
				lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
				getRepository(lRepositoryName).updateItem(lCurrentItem);
				addUpdateItemToList(pFeedVO);
			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedItemWriterDelegate : @Method: updateItem()");
	}

	/**
	 * Adds the item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@SuppressWarnings("unchecked")
	public void addItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedItemWriterDelegate : @Method: addItem()");
		MutableRepositoryItem lCurrentItem = null;
		String locationType = null;
		Store store = (Store) pFeedVO;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());

		if (lItemDiscriptorName != null) {
			if (store.getStoreDetail() != null) {
				locationType = store.getStoreDetail().getLocationType();
			}
			if (!StringUtils.isEmpty(locationType) && !locationType.equalsIgnoreCase(TRUStoreFeedReferenceConstants.SOFT_DELETE_STORE)) {
				IFeedItemMapper lFeedItemMapper = (IFeedItemMapper) getMapper();
				lCurrentItem = getRepository(lRepositoryName)
						.createItem(pFeedVO.getRepositoryId(), lItemDiscriptorName);
				beforeAddItem(pFeedVO, lCurrentItem);
				lCurrentItem = lFeedItemMapper.map(pFeedVO, lCurrentItem);
				getRepository(lRepositoryName).addItem(lCurrentItem);
				addInsertItemToList(pFeedVO);
			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedItemWriterDelegate : @Method: addItem()");
	}
}
