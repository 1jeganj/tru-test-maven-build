package com.tru.feedprocessor.tol.store.config;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;

/**
 * The Class TRUStoreFeedConfig which will hold the configuration details of store feed.
 * 
 * @version 1.1
 * 
 */
public class TRUStoreFeedConfig extends StoreFeedConfig {
	
	/** The Bad directory. */
	private String mBadDirectory;
	
	/** The Processing directory. */
	private String mProcessingDirectory;
	
	/** The Success directory. */
	private String mSuccessDirectory;
	
	/** The Error directory. */
	private String mErrorDirectory;
	
	/** Property to hold  Unprocces directory. */
	private String mUnProcessDirectory;

	/** The store previous day file path. */
	private String mStorePreviousDayFilePath;

	/** The store previous day file path. */
	private String mStorePreviousDayFileName;
	
	/** The mSchemaFileXsdUri. */
	private String mSchemaFileXsdUri;
	
	/** Property to hold  mSourceDirectory directory. */
	private String mSourceDirectory;
	
	/** Property to hold  mFeedFilePattern directory. */
	private String mFeedFilePattern;

	/**
	 * @return the badDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}

	/**
	 * @return the processingDirectory
	 */
	public String getProcessingDirectory() {
		return mProcessingDirectory;
	}

	/**
	 * @return the successDirectory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}

	/**
	 * @return the errorDirectory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}

	/**
	 * @return the unProcessDirectory
	 */
	public String getUnProcessDirectory() {
		return mUnProcessDirectory;
	}
	
	/**
	 * @return the mSchemaFileXsdUri
	 */
	public String getSchemaFileXsdUri() {
		return mSchemaFileXsdUri;
	}
	
	/**
	 * @return the mSourceDirectory
	 */
	public String getSourceDirectory() {
		return mSourceDirectory;
	}

	/**
	 * Sets the source directory.
	 * 
	 * @param pSourceDirectory
	 *            the new source directory
	 */
	public void setSourceDirectory(String pSourceDirectory) {
		mSourceDirectory = pSourceDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.SOURCE_DIRECTORY, pSourceDirectory);
	}

	/**
	 * Sets the processing directory.
	 * 
	 * @param pProcessingDirectory
	 *            the new processing directory
	 */
	public void setProcessingDirectory(String pProcessingDirectory) {
		mProcessingDirectory = pProcessingDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.PROCESSING_DIRECTORY, pProcessingDirectory);
	}

	/**
	 * Sets the un process directory.
	 * 
	 * @param pUnProcessDirectory
	 *            the new un process directory
	 */
	public void setUnProcessDirectory(String pUnProcessDirectory) {
		mUnProcessDirectory = pUnProcessDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.UN_PROCESS_DIRECTORY, pUnProcessDirectory);
	}
	
	/**
	 * Sets the xsd directory.
	 * 
	 * @param pSchemaFileXsdUri
	 *            the new xsd directory
	 */
	public void setSchemaFileXsdUri(String pSchemaFileXsdUri) {
		this.mSchemaFileXsdUri = pSchemaFileXsdUri;
		setConfigValue(TRUStoreFeedReferenceConstants.SCHEMA_FILE_XSD_URI, pSchemaFileXsdUri);
	}

	/**
	 * Sets the feed file pattern.
	 * 
	 * @param pFeedFilePattern
	 *            the new feed file pattern
	 */
	public void setFeedFilePattern(String pFeedFilePattern) {
		mFeedFilePattern = pFeedFilePattern;
		setConfigValue(TRUStoreFeedReferenceConstants.FEED_FILE_PATTERN, pFeedFilePattern);
	}

	/**
	 * Sets the bad directory.
	 * 
	 * @param pBadDirectory
	 *            the new bad directory
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.BAD_DIRECTORY, pBadDirectory);
	}

	/**
	 * Sets the success directory.
	 * 
	 * @param pSuccessDirectory
	 *            the new success directory
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.SUCCESS_DIRECTORY, pSuccessDirectory);
	}

	/**
	 * Sets the error directory.
	 * 
	 * @param pErrorDirectory
	 *            the new error directory
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
		setConfigValue(TRUStoreFeedReferenceConstants.ERROR_DIRECTORY, pErrorDirectory);
	}

	/**
	 * Sets the file name tru env.
	 * 
	 * @param pFileNameTRUEnv
	 *            the new file name tru env
	 */
	public void setFileNameTRUEnv(String pFileNameTRUEnv) {
		setConfigValue(TRUStoreFeedReferenceConstants.FILE_NAME_TRU_ENV, pFileNameTRUEnv);
	}

	/**
	 * Sets the file feed name.
	 * 
	 * @param pFileFeedName
	 *            the new file feed name
	 */
	public void setFileFeedName(String pFileFeedName) {
		setConfigValue(TRUStoreFeedReferenceConstants.FILE_FEED_NAME, pFileFeedName);
	}

	/**
	 * Gets the store previous day file path.
	 * 
	 * @return the store previous day file path
	 */
	public String getStorePreviousDayFilePath() {
		return mStorePreviousDayFilePath;
	}
	
	/**
	 * Gets the feed file pattern.
	 * 
	 * @return the feed file pattern
	 */
	public String getFeedFilePattern() {
		return mFeedFilePattern;
	}

	/**
	 * Sets the store previous day file path.
	 * 
	 * @param pStorePreviousDayFilePath
	 *            the new store previous day file path
	 */
	public void setStorePreviousDayFilePath(String pStorePreviousDayFilePath) {
		this.mStorePreviousDayFilePath = pStorePreviousDayFilePath;
		setConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_DIRECTORY, pStorePreviousDayFilePath);
	}

	/**
	 * Gets the store previous day file path.
	 * 
	 * @return the store previous day file path
	 */
	public String getStorePreviousDayFileName() {
		return mStorePreviousDayFileName;
	}

	/**
	 * Sets the store previous day file path.
	 * 
	 * @param pStorePreviousDayFileName
	 *            the new store previous day file path
	 */
	public void setStorePreviousDayFileName(String pStorePreviousDayFileName) {
		this.mStorePreviousDayFileName = pStorePreviousDayFileName;
		setConfigValue(TRUStoreFeedReferenceConstants.PREVIOUS_DAY_FILE_NAME, pStorePreviousDayFileName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.store.StoreFeedConfig#afterPropertiesSet()
	 */
	/**
	 * @throws Exception
	 *             if something goes wrong.
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedConfig : @Method: afterPropertiesSet()");
		if (null != getConfigValue(TRUStoreFeedReferenceConstants.PROCESSING_DIRECTORY) &&
				 StringUtils.isBlank(getConfigValue(TRUStoreFeedReferenceConstants.PROCESSING_DIRECTORY).toString())) {
			getLogger().logError("Processing directory path is empty");
		}

		if (null != getConfigValue(TRUStoreFeedReferenceConstants.BAD_DIRECTORY) &&
				 StringUtils.isBlank(getConfigValue(TRUStoreFeedReferenceConstants.BAD_DIRECTORY).toString())) {
			getLogger().logError("Bad directory path is empty");
		}

		if (null != getConfigValue(TRUStoreFeedReferenceConstants.SUCCESS_DIRECTORY) &&
				 StringUtils.isBlank(getConfigValue(TRUStoreFeedReferenceConstants.SUCCESS_DIRECTORY).toString())) {
			getLogger().logError("Success directory path is empty");
		}

		if (null != getConfigValue(TRUStoreFeedReferenceConstants.ERROR_DIRECTORY) &&
				 StringUtils.isBlank(getConfigValue(TRUStoreFeedReferenceConstants.ERROR_DIRECTORY).toString())) {
			getLogger().logError("Error directory path is empty");
		}
		getLogger().vlogDebug("End:@Class: TRUStoreFeedConfig : @Method: afterPropertiesSet()");
	}



}
