package com.tru.feedprocessor.tol.store;

/**
 * This class is a property manager. It extends the
 * atg.commerce.catalog.custom.StoeProperties to add the store locater properties.
 * 
 * @author P.A
 * 
 */
public class TRUStoreFeedProperties {

	/**
	 * The variable to hold "ID" property name.
	 */
	private String mLocationId;
	
	/** The m name. */
	private String mName;
	
	/** The m chain code. */
	private String mChainCode;
	
	/** The m warehouse location code. */
	private String mWarehouseLocationCode;
	
	/** The m address1. */
	private String mAddress1;
	
	/** The m address2. */
	private String mAddress2;
	
	/** The m city. */
	private String mCity;
	
	/** The m state address. */
	private String mStateAddress;
	
	/** The m postal code. */
	private String mPostalCode;
	
	/** The m manager. */
	private String mManager;
	
	/** The m phone number. */
	private String mPhoneNumber;
	
	/** The m latitude. */
	private String mLatitude;
	
	/** The m longitude. */
	private String mLongitude;
	
	/** The m trading hours. */
	private String mTradingHours;
	
	/** The m day. */
	private String mDay;
	
	/** The m opening hours. */
	private String mOpeningHours;
	
	/** The m closing hours. */
	private String mClosingHours;
	
	/** The m store hours. */
	private String mStoreHours;
	
	/** The m online layaway. */
	private String mOnlineLayaway;
	
	/** The m in store pickup. */
	private String mInStorePickup;
	
	/** The m inventory search online. */
	private String mInventorySearchOnline;
	
	/** The m sidebyside store. */
	private String mSidebysideStore;
	
	/** The m ship from store. */
	private String mShipFromStore;
	
	/** The m start date. */
	private String mStartDate;

	/** The m end date. */
	private String mEndDate;
	
	/** The m location type. */
	private String mLocationType;
	
	/** The m location Services. */
	private String mLocationServices;
	
	/** The m service name. */
	private String mServiceName;

	/** The m service name. */
	private String mGeoCode;

	/** The m service name. */
	private String mGeoZip;

	/** The m service name. */
	private String mEnterpriseZone;
	
	/** The m seq processed property name. */
	private String mSeqProcessedPropertyName;

	/**
	 * Gets the start date.
	 *
	 * @return the mStartDate
	 */
	public String getStartDate() {
		return mStartDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param pStartDate the mStartDate to set
	 */
	public void setStartDate(String pStartDate) {
		this.mStartDate = pStartDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the mEndDate
	 */
	public String getEndDate() {
		return mEndDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param pEndDate the mEndDate to set
	 */
	public void setEndDate(String pEndDate) {
		this.mEndDate = pEndDate;
	}

	/**
	 * Gets the location id.
	 *
	 * @return the mLocationId
	 */
	public String getLocationId() {
		return mLocationId;
	}
	
	/**
	 * Sets the location id.
	 *
	 * @param pLocationId - pLocationId to set
	 */
	public void setLocationId(String pLocationId) {
		this.mLocationId = pLocationId;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the mName
	 */
	public String getName() {
		return mName;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param pName - pName to set
	 */
	public void setName(String pName) {
		this.mName = pName;
	}
	
	/**
	 * Gets the chain code.
	 *
	 * @return the mChainCode
	 */
	public String getChainCode() {
		return mChainCode;
	}
	
	/**
	 * Sets the chain code.
	 *
	 * @param pChainCode - pChainCode to set
	 */
	public void setChainCode(String pChainCode) {
		this.mChainCode = pChainCode;
	}
	
	/**
	 * Gets the warehouse location code.
	 *
	 * @return the mWarehouseLocationCode
	 */
	public String getWarehouseLocationCode() {
		return mWarehouseLocationCode;
	}
	
	/**
	 * Sets the warehouse location code.
	 *
	 * @param pWarehouseLocationCode - pWarehouseLocationCode to set
	 */
	public void setWarehouseLocationCode(String pWarehouseLocationCode) {
		this.mWarehouseLocationCode = pWarehouseLocationCode;
	}
	
	/**
	 * Gets the address1.
	 *
	 * @return the mAddress1
	 */
	public String getAddress1() {
		return mAddress1;
	}
	
	/**
	 * Sets the address1.
	 *
	 * @param pAddress1 - pAddress1 to set
	 */
	public void setAddress1(String pAddress1) {
		this.mAddress1 = pAddress1;
	}
	
	/**
	 * Gets the address2.
	 *
	 * @return the mAddress2
	 */
	public String getAddress2() {
		return mAddress2;
	}
	
	/**
	 * Sets the address2.
	 *
	 * @param pAddress2 - pAddress2 to set
	 */
	public void setAddress2(String pAddress2) {
		this.mAddress2 = pAddress2;
	}
	
	/**
	 * Gets the city.
	 *
	 * @return the mCity
	 */
	public String getCity() {
		return mCity;
	}
	
	/**
	 * Sets the city.
	 *
	 * @param pCity - pCity to set
	 */
	public void setCity(String pCity) {
		this.mCity = pCity;
	}
	
	/**
	 * Gets the state address.
	 *
	 * @return the mStateAddress
	 */
	public String getStateAddress() {
		return mStateAddress;
	}
	
	/**
	 * Sets the state address.
	 *
	 * @param pStateAddress the new state address
	 */
	public void setStateAddress(String pStateAddress) {
		this.mStateAddress = pStateAddress;
	}
	
	/**
	 * Gets the postal code.
	 *
	 * @return the mCity
	 */
	public String getPostalCode() {
		return mPostalCode;
	}
	
	/**
	 * Sets the postal code.
	 *
	 * @param pPostalCode - pPostalCode to set
	 */
	public void setPostalCode(String pPostalCode) {
		this.mPostalCode = pPostalCode;
	}
	
	/**
	 * Gets the manager.
	 *
	 * @return the mManager
	 */
	public String getManager() {
		return mManager;
	}
	
	/**
	 * Sets the manager.
	 *
	 * @param pManager - pManager to set
	 */
	public void setManager(String pManager) {
		this.mManager = pManager;
	}
	
	/**
	 * Gets the phone number.
	 *
	 * @return the mPhoneNumber
	 */
	public String getPhoneNumber() {
		return mPhoneNumber;
	}
	
	/**
	 * Sets the phone number.
	 *
	 * @param pPhoneNumber - pPhoneNumber to set
	 */ 
	public void setPhoneNumber(String pPhoneNumber) { 
		this.mPhoneNumber = pPhoneNumber;
	}
	
	/**
	 * Gets the latitude.
	 *
	 * @return the mLatitude
	 */
	public String getLatitude() {
		return mLatitude;
	}
	
	/**
	 * Sets the latitude.
	 *
	 * @param pLatitude - pLatitude to set
	 */
	public void setLatitude(String pLatitude) {
		this.mLatitude = pLatitude;
	}
	
	/**
	 * Gets the longitude.
	 *
	 * @return the mLongitude
	 */
	public String getLongitude() {
		return mLongitude;
	}
	
	/**
	 * Sets the longitude.
	 *
	 * @param pLongitude - pLongitude to set
	 */
	public void setLongitude(String pLongitude) {
		this.mLongitude = pLongitude;
	}
	
	/**
	 * Gets the trading hours.
	 *
	 * @return the mTradingHours
	 */
	public String getTradingHours() {
		return mTradingHours;
	}
	
	/**
	 * Sets the trading hours.
	 *
	 * @param pTradingHours - pTradingHours to set
	 */
	public void setTradingHours(String pTradingHours) {
		this.mTradingHours = pTradingHours;
	}
	
	/**
	 * Gets the day.
	 *
	 * @return the mDay
	 */
	public String getDay() {
		return mDay;
	}
	
	/**
	 * Sets the day.
	 *
	 * @param pDay - pDay to set
	 */
	public void setDay(String pDay) {
		this.mDay = pDay;
	}
	
	/**
	 * Gets the opening hours.
	 *
	 * @return the mOpeningHours
	 */
	public String getOpeningHours() {
		return mOpeningHours;
	}
	
	/**
	 * Sets the opening hours.
	 *
	 * @param pOpeningHours - pOpeningHours to set
	 */
	public void setOpeningHours(String pOpeningHours) {
		this.mOpeningHours = pOpeningHours;
	}
	
	/**
	 * Gets the closing hours.
	 *
	 * @return to mClosingHours to set
	 */
	public String getClosingHours() {
		return mClosingHours;
	}
	
	/**
	 * Sets the closing hours.
	 *
	 * @param pClosingHours - pClosingHours to set
	 */
	public void setClosingHours(String pClosingHours) {
		this.mClosingHours = pClosingHours;
	}
	
	/**
	 * Gets the store hours.
	 *
	 * @return the mStoreHours
	 */
	public String getStoreHours() {
		return mStoreHours;
	}
	
	/**
	 * Sets the store hours.
	 *
	 * @param pStoreHours - pStoreHours to set
	 */
	public void setStoreHours(String pStoreHours) {
		this.mStoreHours = pStoreHours;
	}
	
	/**
	 * Gets the online layaway.
	 *
	 * @return the mOnlineLayaway
	 */
	public String getOnlineLayaway() {
		return mOnlineLayaway;
	}
	
	/**
	 * Sets the online layaway.
	 *
	 * @param pOnlineLayaway - pOnlineLayaway to set
	 */
	public void setOnlineLayaway(String pOnlineLayaway) {
		this.mOnlineLayaway = pOnlineLayaway;
	}
	
	/**
	 * Gets the in store pickup.
	 *
	 * @return the mInStorePickup
	 */
	public String getInStorePickup() {
		return mInStorePickup;
	}
	
	/**
	 * Sets the in store pickup.
	 *
	 * @param pInStorePickup - pInStorePickup to set
	 */
	public void setInStorePickup(String pInStorePickup) {
		this.mInStorePickup = pInStorePickup;
	}
	
	/**
	 * Gets the inventory search online.
	 *
	 * @return the mInventorySearchOnline
	 */
	public String getInventorySearchOnline() {
		return mInventorySearchOnline;
	}
	
	/**
	 * Sets the inventory search online.
	 *
	 * @param pInventorySearchOnline - pInventorySearchOnline to set
	 */
	public void setInventorySearchOnline(String pInventorySearchOnline) {
		this.mInventorySearchOnline = pInventorySearchOnline;
	}
	
	/**
	 * Gets the sidebyside store.
	 *
	 * @return the mSidebysideStore
	 */
	public String getSidebysideStore() {
		return mSidebysideStore;
	}
	
	/**
	 * Sets the sidebyside store.
	 *
	 * @param pSidebysideStore - pSidebysideStore to set
	 */
	public void setSidebysideStore(String pSidebysideStore) {
		this.mSidebysideStore = pSidebysideStore;
	}
	
	/**
	 * Gets the ship from store.
	 *
	 * @return the mShipFromStore
	 */
	public String getShipFromStore() {
		return mShipFromStore;
	}
	
	/**
	 * Sets the ship from store.
	 *
	 * @param pShipFromStore - pShipFromStore to set
	 */
	public void setShipFromStore(String pShipFromStore) {
		this.mShipFromStore = pShipFromStore;
	}

	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return mLocationType;
	}

	/**
	 * @param pLocationType the locationType to set
	 */
	public void setLocationType(String pLocationType) {
		mLocationType = pLocationType;
	}

	/**
	 * Gets the location services.
	 *
	 * @return the location services
	 */
	public String getLocationServices() {
		return mLocationServices;
	}

	/**
	 * Sets the location services.
	 *
	 * @param pLocationServices the new location services
	 */
	public void setLocationServices(String pLocationServices) {
		mLocationServices = pLocationServices;
	}
	
	/**
	 * Gets the ServiceName.
	 *
	 * @return ServiceName
	 */
	public String getServiceName() {
		return mServiceName;
	}
	
	/**
	 * Sets the ServiceName.
	 *
	 * @param pServiceName the new service name
	 */
	public void setServiceName(String pServiceName) {
		mServiceName = pServiceName;
	}

	/**
	 * Gets the geo code.
	 * 
	 * @return the mGeoCode
	 */
	public String getGeoCode() {
		return mGeoCode;
	}

	/**
	 * Sets the geo code.
	 * 
	 * @param pGeoCode
	 *            the new geo code
	 */
	public void setGeoCode(String pGeoCode) {
		this.mGeoCode = pGeoCode;
	}

	/**
	 * Gets the geo zip.
	 * 
	 * @return the mGeoZip
	 */
	public String getGeoZip() {
		return mGeoZip;
	}

	/**
	 * Sets the geo zip.
	 * 
	 * @param pGeoZip
	 *            the new geo zip
	 */
	public void setGeoZip(String pGeoZip) {
		this.mGeoZip = pGeoZip;
	}

	/**
	 * Gets the enterprise zone.
	 * 
	 * @return the mEnterpriseZone
	 */
	public String getEnterpriseZone() {
		return mEnterpriseZone;
	}

	/**
	 * Sets the enterprise zone.
	 * 
	 * @param pEnterpriseZone
	 *            the new enterprise zone
	 */
	public void setEnterpriseZone(String pEnterpriseZone) {
		this.mEnterpriseZone = pEnterpriseZone;
	}
	
	/**
	 * Gets the seq processed property name.
	 * 
	 * @return the seq processed property name
	 */
	public String getSeqProcessedPropertyName() {
		return mSeqProcessedPropertyName;
	}

	/**
	 * Sets the seq processed property name.
	 * 
	 * @param pSeqProcessedPropertyName
	 *            the new seq processed property name
	 */
	public void setSeqProcessedPropertyName(String pSeqProcessedPropertyName) {
		this.mSeqProcessedPropertyName = pSeqProcessedPropertyName;
	}
	
}
