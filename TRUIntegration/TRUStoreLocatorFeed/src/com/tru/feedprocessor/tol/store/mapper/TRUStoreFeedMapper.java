package com.tru.feedprocessor.tol.store.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.store.TRUStoreFeedProperties;
import com.tru.feedprocessor.tol.store.feedvo.Store;
import com.tru.feedprocessor.tol.store.feedvo.StoreHours;

/**
 * The Class TRUStoreFeedMapper will set the vo properties to store repository item.
 * 
 * @author PA
 */
public class TRUStoreFeedMapper extends AbstractFeedItemMapper<Store> {

	/** The m feed store property. */
	public TRUStoreFeedProperties mFeedStoreProperty;

	/** The mLogger. */
	private FeedLogger mLogger;
	
	/** Property To Hold location repository */
	private Repository mLocationRepository;
	
	/** Property To Hold Service Name Mapper */
	private Map<String, String> mServiceNameMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.catalogfeed.ICatFeedItemMapper#map(java. lang.Object,
	 * atg.repository.MutableRepositoryItem)
	 */
	@Override
	public MutableRepositoryItem map(Store pStoreItem, MutableRepositoryItem pRepoItem) throws FeedSkippableException {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreFeedMapper : @Method: map(): store Id:: {0}" , pRepoItem.getRepositoryId());
		populateStoreDetails(pStoreItem, pRepoItem);
		populateContactDetails(pStoreItem, pRepoItem);
		if (pStoreItem.getDirections() != null && pStoreItem.getDirections().getLatitude() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getLatitude(), pStoreItem.getDirections().getLatitude()
					.doubleValue());
		}
		if (pStoreItem.getDirections() != null && pStoreItem.getDirections().getLongitude() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getLongitude(), pStoreItem.getDirections().getLongitude()
					.doubleValue());
		}
		populateStoreDateDetails(pStoreItem, pRepoItem);
		
		List<RepositoryItem> locationServiceRepoItems = createStoreLocatorServices(pStoreItem);
		getLogger().vlogDebug("TRUStoreFeedMapper : @Method: map(): locationServiceRepoItems:: {0}" , locationServiceRepoItems);
		pRepoItem.setPropertyValue(getFeedStoreProperty().getLocationServices(), locationServiceRepoItems);
		
		createStoreHours(pStoreItem, pRepoItem);
		getLogger().vlogDebug("End:@Class: TRUStoreFeedMapper : @Method: map()");
		return pRepoItem;
	}


	/**
	 * Populate store date details.
	 *
	 * @param pStoreItem the store item
	 * @param pRepoItem the repo item
	 */
	private void populateStoreDateDetails(Store pStoreItem, MutableRepositoryItem pRepoItem) {
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getOpenDate() != null
				&& !pStoreItem.getStoreDetail().getOpenDate().isEmpty()) {
			SimpleDateFormat formatter = new SimpleDateFormat(TRUStoreFeedReferenceConstants.XML_DATE_FORMAT, Locale.getDefault());
			Date date = null;
			try {
				date = formatter.parse(pStoreItem.getStoreDetail().getOpenDate());
			} catch (ParseException e) {
				if(isLoggingError()) {
					logError("ParseException in store feed mapper", e);
				}
			}
			pRepoItem.setPropertyValue(getFeedStoreProperty().getStartDate(), date);
		}
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getCloseDate() != null
				&& !pStoreItem.getStoreDetail().getCloseDate().isEmpty()) {
			SimpleDateFormat formatter = new SimpleDateFormat(TRUStoreFeedReferenceConstants.XML_DATE_FORMAT, Locale.getDefault());
			Date date = null;
			try {
				date = formatter.parse(pStoreItem.getStoreDetail().getCloseDate());
			} catch (ParseException e) {
				if(isLoggingError()) {
					logError("ParseException in store feed mapper", e);
				}
			}
			pRepoItem.setPropertyValue(getFeedStoreProperty().getEndDate(), date);
		}
	}


	/**
	 * Populate contact details.
	 *
	 * @param pStoreItem the store item.
	 * @param pRepoItem the repo item.
	 */
	private void populateContactDetails(Store pStoreItem, MutableRepositoryItem pRepoItem) {
		if (pStoreItem.getAddress() != null && pStoreItem.getAddress().getAddress1() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getAddress1(), pStoreItem.getAddress().getAddress1());
		}
		if (pStoreItem.getAddress() != null && pStoreItem.getAddress().getAddress2() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getAddress2(), pStoreItem.getAddress().getAddress2());
		}
		if (pStoreItem.getAddress() != null && pStoreItem.getAddress().getCity() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getCity(), pStoreItem.getAddress().getCity());
		}
		if (pStoreItem.getAddress() != null && pStoreItem.getAddress().getState() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getStateAddress(), pStoreItem.getAddress().getState());
		}
		if (pStoreItem.getAddress() != null && pStoreItem.getAddress().getZipCode() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getPostalCode(), pStoreItem.getAddress().getZipCode());
		}
		if (pStoreItem.getContactDetails() != null && pStoreItem.getContactDetails().getManager() != null) {
			pRepoItem
					.setPropertyValue(getFeedStoreProperty().getManager(), pStoreItem.getContactDetails().getManager());
		}
		if (pStoreItem.getContactDetails() != null && pStoreItem.getContactDetails().getContactNumber() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getPhoneNumber(), pStoreItem.getContactDetails()
					.getContactNumber());
		}
	}


	/**
	 * Populate store details.
	 *
	 * @param pStoreItem the store item.
	 * @param pRepoItem the repo item.
	 */
	private void populateStoreDetails(Store pStoreItem, MutableRepositoryItem pRepoItem) {
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getLocationName() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getName(), pStoreItem.getStoreDetail().getLocationName());
		}
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getChainCode() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getChainCode(), pStoreItem.getStoreDetail()
					.getChainCode());
		}
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getWarehouseLocationCode() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getWarehouseLocationCode(), pStoreItem.getStoreDetail()
					.getWarehouseLocationCode().doubleValue());
		}
		if (pStoreItem.getStoreDetail() != null && pStoreItem.getStoreDetail().getLocationType() != null) {
			pRepoItem.setPropertyValue(getFeedStoreProperty().getLocationType(), pStoreItem.getStoreDetail()
					.getLocationType());
		}
		populateTaxwareAttributes(pStoreItem, pRepoItem);
	}

	/**
	 * Populate taxware attributes.
	 * 
	 * @param pStoreItem
	 *            the store item.
	 * @param pRepoItem
	 *            the repo item.
	 */
	private void populateTaxwareAttributes(Store pStoreItem, MutableRepositoryItem pRepoItem) {
		if (pStoreItem.getStoreDetail() != null && !StringUtils.isBlank(pStoreItem.getStoreDetail().getLocationType())
				&& pStoreItem.getStoreDetail().getLocationType().equalsIgnoreCase(TRUStoreFeedReferenceConstants.ACTIVE_STORE)) {
			if (pStoreItem.getTaxware() != null && pStoreItem.getTaxware().getGeoCode() != null) {
				pRepoItem.setPropertyValue(getFeedStoreProperty().getGeoCode(), pStoreItem.getTaxware().getGeoCode());
			}
			if (pStoreItem.getTaxware() != null && pStoreItem.getTaxware().getGeoZip() != null) {
				pRepoItem.setPropertyValue(getFeedStoreProperty().getGeoZip(), pStoreItem.getTaxware().getGeoZip());
			}
			if (pStoreItem.getTaxware() != null && pStoreItem.getTaxware().getEnterpriseZone() != null) {
				Boolean enterpriseZoneFlag = pStoreItem.getTaxware().getEnterpriseZone().equalsIgnoreCase(TRUStoreFeedReferenceConstants.INTEGER_ONE) ? Boolean.TRUE
						: Boolean.FALSE;
				pRepoItem.setPropertyValue(getFeedStoreProperty().getEnterpriseZone(), enterpriseZoneFlag);
			}
		}
	}


	/**
	 * Creates the store locator services.
	 * 
	 * @param pStoreItem
	 *            the store item.
	 * @return the list.
	 */
	private List<RepositoryItem> createStoreLocatorServices(Store pStoreItem) {
		List<RepositoryItem> storeServicesList = new ArrayList<RepositoryItem>();
		RepositoryItem repoItem = null;
		if (pStoreItem.getEligibilty() != null) {
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getOnlineLayaway()) &&
					 pStoreItem.getEligibilty().getOnlineLayaway().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.ONLINE_LAYAWAY));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getParentingClasses()) &&
					 pStoreItem.getEligibilty().getParentingClasses().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.PARENTING_CLASSES));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getInStorePickup()) &&
					 pStoreItem.getEligibilty().getInStorePickup().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.IN_STORE_PICKUP));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getInventorySearchOnline()) &&
					 pStoreItem.getEligibilty().getInventorySearchOnline().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.INVENTORY_SEARCH_ONLINE));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getShipFromStore()) &&
					 pStoreItem.getEligibilty().getShipFromStore().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.SHIP_FROM_STORE));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getPersonalRegistryAdvisor()) &&
					 pStoreItem.getEligibilty().getPersonalRegistryAdvisor().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.PERSONAL_REGISTRY_ADVISOR));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getKidsClothes()) &&
					 pStoreItem.getEligibilty().getKidsClothes().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.KIDS_CLOTHES));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getBabiesrusExpress()) &&
					 pStoreItem.getEligibilty().getBabiesrusExpress().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.BABIESRUS_EXPRESS));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getToysrusExpress()) &&
					 pStoreItem.getEligibilty().getToysrusExpress().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.TOYSRUS_EXPRESS));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getSidebysideStore()) &&
					 pStoreItem.getEligibilty().getSidebysideStore().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.SIDEBYSIDE_STORE));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getCarSeatInstallation()) &&
					 pStoreItem.getEligibilty().getCarSeatInstallation().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.CARSEAT_INSTALLATION));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getCreateAndTakeActivities()) &&
					 pStoreItem.getEligibilty().getCreateAndTakeActivities().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.CREATE_AND_TAKE_ACTIVITIES));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getMaternityClothing()) &&
					 pStoreItem.getEligibilty().getMaternityClothing().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.MATERNITY_CLOTHING));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
			if (StringUtils.isNotEmpty(pStoreItem.getEligibilty().getWiFi()) &&
					 pStoreItem.getEligibilty().getWiFi().equalsIgnoreCase(TRUStoreFeedReferenceConstants.STRING_Y)) {
				repoItem = getLocationServices(getServiceNameMapper().get(TRUStoreFeedReferenceConstants.WI_FI));
				addRepotoStoreServicesList(storeServicesList, repoItem);
			}
		}
		return storeServicesList;
	}


	/**
	 * Adds the repoto store services list.
	 *
	 * @param pStoreServicesList the store services list
	 * @param pRepoItem the repo item
	 */
	private void addRepotoStoreServicesList(List<RepositoryItem> pStoreServicesList, RepositoryItem pRepoItem) {
		if (pRepoItem != null) {
			pStoreServicesList.add(pRepoItem);
		}
	}

	/**
	 * This method is used to query store service based on service name
	 * 
	 * @param pServiceName
	 *            the service name
	 * @return the location services
	 */
	public RepositoryItem getLocationServices(String pServiceName) {
		if (StringUtils.isBlank(pServiceName)) {
			return null;
		}
		RepositoryView locationRepositoryView = null;
		RepositoryItem storeServiceItem = null;
		RepositoryItem[] storeServiceItems = null;
		try {
			locationRepositoryView = getLocationRepository().getView(TRUStoreFeedReferenceConstants.STORE_SERVICES);
			QueryBuilder queryBuilder = locationRepositoryView.getQueryBuilder();

			QueryExpression serviceIdPropertyExpression = queryBuilder.createPropertyQueryExpression(getFeedStoreProperty().getServiceName());
			QueryExpression serviceIdConstantExpression = queryBuilder.createConstantQueryExpression(pServiceName);

			Query serviceIdQuery = queryBuilder.createComparisonQuery(serviceIdPropertyExpression, serviceIdConstantExpression, QueryBuilder.EQUALS);
			storeServiceItems = locationRepositoryView.executeQuery(serviceIdQuery);
			if (null != storeServiceItems) {
				storeServiceItem = storeServiceItems[0];
			} else {
				getLogger().vlogDebug("TRUStoreFeedMapper.getLocationServices  Does Not exists in repository ");
			}

		} catch (RepositoryException e) {
			if(isLoggingError()) {
				logError("RepositoryException occured", e);
			}
		}
		return storeServiceItem;
	}
	

	/**
	 * This method will update the store hours.
	 * 
	 * @param pStoreItem
	 *            the store vo
	 * @param pRepoItem
	 *            the store item
	 * @throws FeedSkippableException
	 *             - FeedSkippableException
	 */
	private void createStoreHours(Store pStoreItem, MutableRepositoryItem pRepoItem) throws FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUStoreFeedMapper : @Method: createStoreHours()");
		MutableRepositoryItem lHoursItem = null;
		Map<String, String> entitytoRepositoryNameMap = (Map<String, String>) getConfigValue(FeedConstants.ENTITYTOREPOSITORYNAMEMAP);
		List<RepositoryItem> weekDayRepo = new ArrayList<RepositoryItem>();
		StoreHours weekDaysVO = pStoreItem.getStoreHours();
		if (weekDaysVO != null && entitytoRepositoryNameMap != null && !entitytoRepositoryNameMap.isEmpty()) {
			String lRepositoryName = entitytoRepositoryNameMap.get(pStoreItem.getEntityName());
			try {
				lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
				if (lHoursItem != null) {
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getMondayOpen(), weekDaysVO.getMondayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.MONDAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getTuesdayOpen(), weekDaysVO.getTuesdayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.TUESDAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getWednesdayOpen(), weekDaysVO.getWednesdayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.WEDNESDAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getThursdayOpen(), weekDaysVO.getThursdayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.THURSDAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getFridayOpen(), weekDaysVO.getFridayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.FRIDAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getSaturdayOpen(), weekDaysVO.getSaturdayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.SATURADAY);
					
					lHoursItem = getRepository(lRepositoryName).createItem(getFeedStoreProperty().getTradingHours());
					
					updateStoreHours(lHoursItem, weekDayRepo, weekDaysVO.getSundayOpen(), weekDaysVO.getSundayClose(),
							lRepositoryName, TRUStoreFeedReferenceConstants.SUNDAY);
					
				}
			} catch (RepositoryException e) {
				throw new FeedSkippableException(lRepositoryName, lRepositoryName, lRepositoryName, null, e);
			}
		}
		pRepoItem.setPropertyValue(getFeedStoreProperty().getStoreHours(), weekDayRepo);
		getLogger().vlogDebug("End:@Class: TRUStoreFeedMapper : @Method: createStoreHours()");
	}


	/**
	 * Update store hours.
	 *
	 * @param pHoursItem the hours item
	 * @param pWeekDayRepo the week day repo
	 * @param pOpen the open
	 * @param pClose the close
	 * @param pRepositoryName the repository name
	 * @param pDay the day
	 * @throws RepositoryException the repository exception
	 */
	protected void updateStoreHours(MutableRepositoryItem pHoursItem, List<RepositoryItem> pWeekDayRepo, Integer pOpen, Integer pClose, String pRepositoryName, String pDay) throws RepositoryException {
		pHoursItem.setPropertyValue(getFeedStoreProperty().getDay(), pDay);
		if (pOpen != null) {
			pHoursItem.setPropertyValue(getFeedStoreProperty().getOpeningHours(), pOpen
					.doubleValue());
		}
		if (pClose != null) {
			pHoursItem.setPropertyValue(getFeedStoreProperty().getClosingHours(), pClose
				.doubleValue());
		}
		getRepository(pRepositoryName).addItem(pHoursItem);
		pWeekDayRepo.add(pHoursItem);
	}

	/**
	 * Gets the feed store property.
	 * 
	 * @return the mFeedStoreProperties
	 */
	public TRUStoreFeedProperties getFeedStoreProperty() {
		return mFeedStoreProperty;
	}

	/**
	 * Sets the feed store property.
	 * 
	 * @param pFeedStoreProperty
	 *            - pFeedStoreProperty to set
	 */
	public void setFeedStoreProperty(TRUStoreFeedProperties pFeedStoreProperty) {
		this.mFeedStoreProperty = pFeedStoreProperty;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}


	/**
	 * Gets the location repository.
	 *
	 * @return the location repository
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}


	/**
	 * Sets the location repository.
	 *
	 * @param pLocationRepository the new location repository
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		this.mLocationRepository = pLocationRepository;
	}


	/**
	 * Gets the service name mapper.
	 *
	 * @return the service name mapper
	 */
	public Map<String, String> getServiceNameMapper() {
		return mServiceNameMapper;
	}


	/**
	 * Sets the service name mapper.
	 *
	 * @param pServiceNameMapper the service name mapper
	 */
	public void setServiceNameMapper(Map<String, String> pServiceNameMapper) {
		mServiceNameMapper = pServiceNameMapper;
	}
	/**
     * Log error message.
     * 
      * @param pMessage
     *            the message
     * @param pException
     *            the exception
     */
     public void logError(Object pMessage, Throwable pException) {
            getLogger().logErrorMessage(pMessage, pException);
            
     }

     

     /**
     * @return true/false
     */
     public boolean isLoggingError()
       {
         return getLogger().isLoggingError();
       }

}
