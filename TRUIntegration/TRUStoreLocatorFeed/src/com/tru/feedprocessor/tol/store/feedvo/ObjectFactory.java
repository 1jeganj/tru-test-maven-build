package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java.
 * element interface generated in the test package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 * @author PA
 * @version 1.0
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create an instance of {@link Address }.
	 *
	 * @return the address
	 */
	public Address createAddress() {
		return new Address();
	}

	/**
	 * Create an instance of {@link Eligibilty }.
	 *
	 * @return the eligibilty
	 */
	public Eligibilty createEligibilty() {
		return new Eligibilty();
	}

	/**
	 * Create an instance of {@link StoreDetail }.
	 *
	 * @return the store detail
	 */
	public StoreDetail createStoreDetail() {
		return new StoreDetail();
	}

	/**
	 * Create an instance of {@link StoreHours }.
	 *
	 * @return the store hours
	 */
	public StoreHours createStoreHours() {
		return new StoreHours();
	}

	/**
	 * Create an instance of {@link LocationDetail.Store }
	 *
	 * @return the store
	 */
	public Store createLocationDetailStore() {
		return new Store();
	}

	/**
	 * Create an instance of {@link Directions }.
	 *
	 * @return the directions
	 */
	public Directions createDirections() {
		return new Directions();
	}

	/**
	 * Create an instance of {@link LocationDetail }.
	 *
	 * @return the location detail
	 */
	public LocationDetail createLocationDetail() {
		return new LocationDetail();
	}

	/**
	 * Create an instance of {@link ContactDetails }.
	 *
	 * @return the contact details
	 */
	public ContactDetails createContactDetails() {
		return new ContactDetails();
	}

}
