package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="parenting-classes" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="online-layaway" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="in-store-pickup" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="inventory-search-online" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="ship-from-store" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="personal-registry-advisor" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="kids-clothes" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="babiesrus-express" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="toysrus-express" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="sidebyside-store" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="car-seat-installation" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="create-and-take-activities" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="maternity-clothing" type="{}String_Maxlength_1" minOccurs="0"/>
 *         &lt;element name="wi-fi" type="{}String_Maxlength_1" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * @author PA
 * @version 1.0
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "eligibilty")
public class Eligibilty extends BaseFeedProcessVO {

    /** The m parenting classes. */
    @XmlElement(name = "parenting-classes")
    private String mParentingClasses;
    
    /** The m online layaway. */
    @XmlElement(name = "online-layaway")
    private String mOnlineLayaway;
    
    /** The m in store pickup. */
    @XmlElement(name = "in-store-pickup")
    private String mInStorePickup;
    
    /** The m inventory search online. */
    @XmlElement(name = "inventory-search-online")
    private String mInventorySearchOnline;
    
    /** The m ship from store. */
    @XmlElement(name = "ship-from-store")
    private String mShipFromStore;
    
    /** The m personal registry advisor. */
    @XmlElement(name = "personal-registry-advisor")
    private String mPersonalRegistryAdvisor;
    
    /** The m kids clothes. */
    @XmlElement(name = "kids-clothes")
    private String mKidsClothes;
    
    /** The m babiesrus express. */
    @XmlElement(name = "babiesrus-express")
    private String mBabiesrusExpress;
    
    /** The m toysrus express. */
    @XmlElement(name = "toysrus-express")
    private String mToysrusExpress;
    
    /** The m sidebyside store. */
    @XmlElement(name = "sidebyside-store")
    private String mSidebysideStore;
    
    /** The m car seat installation. */
    @XmlElement(name = "car-seat-installation")
    private String mCarSeatInstallation;
    
    /** The m create and take activities. */
    @XmlElement(name = "create-and-take-activities")
    private String mCreateAndTakeActivities;
    
    /** The m maternity clothing. */
    @XmlElement(name = "maternity-clothing")
    private String mMaternityClothing;
    
    /** The m wi fi. */
    @XmlElement(name = "wi-fi")
    private String mWiFi;

    /**
     * Gets the pValue of the parentingClasses property.
     *
     * @return the parenting classes
     * possible object is
     * {@link String }
     */
    public String getParentingClasses() {
        return mParentingClasses;
    }

    /**
     * Sets the pValue of the parentingClasses property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentingClasses(String pValue) {
        this.mParentingClasses = pValue;
    }

    /**
     * Gets the pValue of the onlineLayaway property.
     *
     * @return the online layaway
     * possible object is
     * {@link String }
     */
    public String getOnlineLayaway() {
        return mOnlineLayaway;
    }

    /**
     * Sets the pValue of the onlineLayaway property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnlineLayaway(String pValue) {
        this.mOnlineLayaway = pValue;
    }

    /**
     * Gets the pValue of the inStorePickup property.
     *
     * @return the in store pickup
     * possible object is
     * {@link String }
     */
    public String getInStorePickup() {
        return mInStorePickup;
    }

    /**
     * Sets the pValue of the inStorePickup property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInStorePickup(String pValue) {
        this.mInStorePickup = pValue;
    }

    /**
     * Gets the pValue of the inventorySearchOnline property.
     *
     * @return the inventory search online
     * possible object is
     * {@link String }
     */
    public String getInventorySearchOnline() {
        return mInventorySearchOnline;
    }

    /**
     * Sets the pValue of the inventorySearchOnline property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInventorySearchOnline(String pValue) {
        this.mInventorySearchOnline = pValue;
    }

    /**
     * Gets the pValue of the shipFromStore property.
     *
     * @return the ship from store
     * possible object is
     * {@link String }
     */
    public String getShipFromStore() {
        return mShipFromStore;
    }

    /**
     * Sets the ppValue of the shipFromStore property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromStore(String pValue) {
        this.mShipFromStore = pValue;
    }

    /**
     * Gets the pValue of the personalRegistryAdvisor property.
     *
     * @return the personal registry advisor
     * possible object is
     * {@link String }
     */
    public String getPersonalRegistryAdvisor() {
        return mPersonalRegistryAdvisor;
    }

    /**
     * Sets the pValue of the personalRegistryAdvisor property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalRegistryAdvisor(String pValue) {
        this.mPersonalRegistryAdvisor = pValue;
    }

    /**
     * Gets the pValue of the kidsClothes property.
     *
     * @return the kids clothes
     * possible object is
     * {@link String }
     */
    public String getKidsClothes() {
        return mKidsClothes;
    }

    /**
     * Sets the pValue of the kidsClothes property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKidsClothes(String pValue) {
        this.mKidsClothes = pValue;
    }

    /**
     * Gets the pValue of the babiesrusExpress property.
     *
     * @return the babiesrus express
     * possible object is
     * {@link String }
     */
    public String getBabiesrusExpress() {
        return mBabiesrusExpress;
    }

    /**
     * Sets the pValue of the babiesrusExpress property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBabiesrusExpress(String pValue) {
        this.mBabiesrusExpress = pValue;
    }

    /**
     * Gets the pValue of the toysrusExpress property.
     *
     * @return the toysrus express
     * possible object is
     * {@link String }
     */
    public String getToysrusExpress() {
        return mToysrusExpress;
    }

    /**
     * Sets the pValue of the toysrusExpress property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToysrusExpress(String pValue) {
        this.mToysrusExpress = pValue;
    }

    /**
     * Gets the pValue of the sidebysideStore property.
     *
     * @return the sidebyside store
     * possible object is
     * {@link String }
     */
    public String getSidebysideStore() {
        return mSidebysideStore;
    }

    /**
     * Sets the pValue of the sidebysideStore property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSidebysideStore(String pValue) {
        this.mSidebysideStore = pValue;
    }

    /**
     * Gets the pValue of the carSeatInstallation property.
     *
     * @return the car seat installation
     * possible object is
     * {@link String }
     */
    public String getCarSeatInstallation() {
        return mCarSeatInstallation;
    }

    /**
     * Sets the pValue of the carSeatInstallation property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarSeatInstallation(String pValue) {
        this.mCarSeatInstallation = pValue;
    }

    /**
     * Gets the pValue of the createAndTakeActivities property.
     *
     * @return the creates the and take activities
     * possible object is
     * {@link String }
     */
    public String getCreateAndTakeActivities() {
        return mCreateAndTakeActivities;
    }

    /**
     * Sets the pValue of the createAndTakeActivities property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateAndTakeActivities(String pValue) {
        this.mCreateAndTakeActivities = pValue;
    }

    /**
     * Gets the pValue of the maternityClothing property.
     *
     * @return the maternity clothing
     * possible object is
     * {@link String }
     */
    public String getMaternityClothing() {
        return mMaternityClothing;
    }

    /**
     * Sets the pValue of the maternityClothing property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaternityClothing(String pValue) {
        this.mMaternityClothing = pValue;
    }

    /**
     * Gets the pValue of the wiFi property.
     *
     * @return the wi fi
     * possible object is
     * {@link String }
     */
    public String getWiFi() {
        return mWiFi;
    }

    /**
     * Sets the pValue of the wiFi property.
     * 
     * @param pValue
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWiFi(String pValue) {
        this.mWiFi = pValue;
    }

}
