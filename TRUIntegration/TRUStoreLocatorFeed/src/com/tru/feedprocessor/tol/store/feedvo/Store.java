package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
	 * @author PA
	 * @version 1.0
	 */
@XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {
      "mLocationCode",
      "mStoreDetail",
      "mContactDetails",
      "mEligibilty",
      "mAddress",
      "mDirections",
      "mStoreHours",
      "mTaxware",
  })
  @XmlRootElement(name="store")
  public class Store extends BaseFeedProcessVO{
      
      /**
		 * The contact-details.
		 */
      @XmlElement(name = "contact-details", required = true)
      private ContactDetails mContactDetails;
      
      /**
		 * The eligibilty.
		 */
      @XmlElement(name = "eligibilty")
      private Eligibilty mEligibilty;
      
      /**
		 * The address.
		 */
      @XmlElement(name = "address", required = true)
      private Address mAddress;
      
      /**
		 * The display directions.
		 */
      @XmlElement(name = "directions", required = true)
      private Directions mDirections;
      
      /**
		 * The store-hours.
		 */
      @XmlElement(name = "store-hours", required = true)
      private StoreHours mStoreHours;
      
      /** The m taxware. */
      @XmlElement(name = "taxware")
      private Taxware mTaxware;

		/**
		 * Gets the location code.
		 *
		 * @return the mLocationCode
		 */
		public String getLocationCode() {
			return mLocationCode;
		}

		/**
		 * Sets the location code.
		 *
		 * @param pValue the mLocationCode to set
		 */
		public void setLocationCode(String pValue) {
			this.mLocationCode = pValue;
			setRepositoryId(pValue);
		}

		/**
		 * Gets the store detail.
		 *
		 * @return the storeDetail
		 */
		public StoreDetail getStoreDetail() {
			return mStoreDetail;
		}

		/**
		 * Sets the store detail.
		 *
		 * @param pValue the storeDetail to set
		 */
		public void setStoreDetail(StoreDetail pValue) {
			this.mStoreDetail = pValue;
		}

		/**
		 * Gets the contact details.
		 *
		 * @return the contactDetails
		 */
		public ContactDetails getContactDetails() {
			return mContactDetails;
		}

		/**
		 * Sets the contact details.
		 *
		 * @param pValue the contactDetails to set
		 */
		public void setContactDetails(ContactDetails pValue) {
			this.mContactDetails = pValue;
		}

		/**
		 * Gets the eligibilty.
		 *
		 * @return the eligibilty
		 */
		public Eligibilty getEligibilty() {
			return mEligibilty;
		}

		/**
		 * Sets the eligibilty.
		 *
		 * @param pValue the eligibilty to set
		 */
		public void setEligibilty(Eligibilty pValue) {
			this.mEligibilty = pValue;
		}

		/**
		 * Gets the address.
		 *
		 * @return the mAddress
		 */
		public Address getAddress() {
			return mAddress;
		}

		/**
		 * Sets the address.
		 *
		 * @param pValue the address to set
		 */
		public void setAddress(Address pValue) {
			this.mAddress = pValue;
		}

		/**
		 * Gets the directions.
		 *
		 * @return the directions
		 */
		public Directions getDirections() {
			return mDirections;
		}

		/**
		 * Sets the directions.
		 *
		 * @param pValue the directions to set
		 */
		public void setDirections(Directions pValue) {
			this.mDirections = pValue;
		}

		/**
		 * Gets the store hours.
		 *
		 * @return the storeHours
		 */
		public StoreHours getStoreHours() {
			return mStoreHours;
		}

		/**
		 * Sets the store hours.
		 *
		 * @param pValue the new store hours
		 */
		public void setStoreHours(StoreHours pValue) {
			this.mStoreHours = pValue;
		}

		/**
		 * Gets the taxware.
		 *
		 * @return the mTaxware
		 */
		public Taxware getTaxware() {
			return mTaxware;
		}

		/**
		 * Sets the taxware.
		 *
		 * @param pTaxware the new taxware
		 */
		public void setTaxware(Taxware pTaxware) {
			this.mTaxware = pTaxware;
		}
		
		 /**
	 	 * Gets the store.
	 	 *
	 	 * @return store
	 	 */
		public String getEntityName(){
			return TRUStoreFeedReferenceConstants.STORE_ENTITY_NAME;
		}

		
	      /**
			 * The location-code.
			 */
	      @XmlElement(name = "location-code", required = true)
	      private String mLocationCode;
	      
	      /**
			 * The store-detail.
			 */
	      @XmlElement(name = "store-detail", required = true)
	      private StoreDetail mStoreDetail;
	      
	      /* (non-Javadoc)
	       * @see com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO#getRepositoryId()
	       */
	      @Override
	    public String getRepositoryId() {
	    	// TODO Auto-generated method stub
	    	return mLocationCode;
	    }
      

  }

