package com.tru.feedprocessor.tol.store.feedvo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element name="store">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice maxOccurs="unbounded">
 *                   &lt;element name="location-code" type="{}Integer_Maxlength_5"/>
 *                   &lt;element ref="{}store-detail" minOccurs="0"/>
 *                   &lt;element ref="{}contact-details" minOccurs="0"/>
 *                   &lt;element ref="{}eligibilty" minOccurs="0"/>
 *                   &lt;element ref="{}directions" minOccurs="0"/>
 *                   &lt;element ref="{}store-hours" minOccurs="0"/>
 *                   &lt;element ref="{}address" minOccurs="0"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * @author PA
 * @version 1.0
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mStore"
})
@XmlRootElement(name = "location-detail")
public class LocationDetail {

	/**
	 * The store.
	 */
    @XmlElement(name = "store", required = true)
    private List<Store> mStore;

    /**
     * Gets the value of the store property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the store property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     * getStore().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     *
     * @return the store
     * {@link LocationDetail.Store }
     */
    public List<Store> getStore() {
        if (mStore == null) {
        	mStore = new ArrayList<Store>();
        }
        return this.mStore;
    }


 
}
