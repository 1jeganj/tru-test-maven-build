package com.tru.feedprocessor.tol.store.feedvo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="latitude" type="{}Decimal_12_8" minOccurs="0"/>
 *         &lt;element name="longitude" type="{}Decimal_12_8" minOccurs="0"/>
 *         &lt;element name="direction-URL" type="{}String_Maxlength_50" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * @author PA
 * @version 1.0
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "directions")
public class Directions extends BaseFeedProcessVO {

    /** The m latitude. */
	@XmlElement(name = "latitude")
	private BigDecimal mLatitude;
    
    /** The m longitude. */
    @XmlElement(name = "longitude")
    private BigDecimal mLongitude;
    
    /** The m direction url. */
    @XmlElement(name = "direction-URL")
    private String mDirectionURL;

    /**
     * Gets the value of the latitude property.
     *
     * @return the latitude
     * possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getLatitude() {
        return mLatitude;
    }

    /**
     * Sets the value of the latitude property.
     *
     * @param pValue the new latitude
     * {@link BigDecimal }
     */
    public void setLatitude(BigDecimal pValue) {
        this.mLatitude = pValue;
    }

    /**
     * Gets the value of the longitude property.
     *
     * @return the longitude
     * possible object is
     * {@link BigDecimal }
     */
    public BigDecimal getLongitude() {
        return mLongitude;
    }

    /**
     * Sets the value of the longitude property.
     *
     * @param pValue the new longitude
     * {@link BigDecimal }
     */
    public void setLongitude(BigDecimal pValue) {
        this.mLongitude = pValue;
    }

    /**
     * Gets the value of the directionURL property.
     *
     * @return the direction url
     * possible object is
     * {@link String }
     */
    public String getDirectionURL() {
        return mDirectionURL;
    }

    /**
     * Sets the value of the directionURL property.
     *
     * @param pValue the new direction url
     * {@link String }
     */
    public void setDirectionURL(String pValue) {
        this.mDirectionURL = pValue;
    }

}
