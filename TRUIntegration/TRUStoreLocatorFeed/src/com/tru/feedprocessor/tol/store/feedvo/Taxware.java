package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="geo-code" type="{}String_Maxlength_2" minOccurs="0"/>
 *         &lt;element name="geo-zip" type="{}String_Maxlength_5" minOccurs="0"/>
 *         &lt;element name="enterprise-zone" type="{}String_Maxlength_1" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "taxware")
public class Taxware extends BaseFeedProcessVO {

	@XmlElement(name = "geo-code")
	protected String mGeoCode;

	@XmlElement(name = "geo-zip")
	protected String mGeoZip;

	@XmlElement(name = "enterprise-zone")
	protected String mEnterpriseZone;

	/**
	 * Gets the value of the geoCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGeoCode() {
		return mGeoCode;
	}

	/**
	 * Sets the value of the geoCode property.
	 * 
	 * @param pValue
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGeoCode(String pValue) {
		this.mGeoCode = pValue;
	}

	/**
	 * Gets the value of the geoZip property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGeoZip() {
		return mGeoZip;
	}

	/**
	 * Sets the value of the geoZip property.
	 * 
	 * @param pValue
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGeoZip(String pValue) {
		this.mGeoZip = pValue;
	}

	/**
	 * Gets the value of the enterpriseZone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnterpriseZone() {
		return mEnterpriseZone;
	}

	/**
	 * Sets the value of the enterpriseZone property.
	 * 
	 * @param pValue
	 *            allowed object is {@link String }
	 * 
	 */
	public void setEnterpriseZone(String pValue) {
		this.mEnterpriseZone = pValue;
	}

}
