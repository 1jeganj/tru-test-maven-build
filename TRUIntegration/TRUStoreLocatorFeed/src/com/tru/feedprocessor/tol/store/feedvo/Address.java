package com.tru.feedprocessor.tol.store.feedvo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="address1" type="{}String_Maxlength_25" minOccurs="0"/>
 *         &lt;element name="address2" type="{}String_Maxlength_25" minOccurs="0"/>
 *         &lt;element name="city" type="{}String_Maxlength_20" minOccurs="0"/>
 *         &lt;element name="state" type="{}String_Maxlength_2" minOccurs="0"/>
 *         &lt;element name="zip-code" type="{}String_Maxlength_10" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "address")
public class Address extends BaseFeedProcessVO{

    /** The m address1. */
	@XmlElement(name = "address1")
    protected String mAddress1;
    
    /** The m address2. */
    @XmlElement(name = "address2")
    protected String mAddress2;
    
    /** The m city. */
    @XmlElement(name = "city")
    protected String mCity;
    
    /** The m state. */
    @XmlElement(name = "state")
    protected String mState;
    
    /** The m zip code. */
    @XmlElement(name = "zip-code")
    protected String mZipCode;

    /**
     * Gets the value of the address1 property.
     *
     * @return the address1
     * possible object is
     * {@link String }
     */
    public String getAddress1() {
        return mAddress1;
    }

    /**
     * Sets the value of the address1 property.
     *
     * @param pValue the new address1
     * {@link String }
     */
    public void setAddress1(String pValue) {
        this.mAddress1 = pValue;
    }

    /**
     * Gets the value of the address2 property.
     *
     * @return the address2
     * possible object is
     * {@link String }
     */
    public String getAddress2() {
        return mAddress2;
    }

    /**
     * Sets the value of the address2 property.
     *
     * @param pValue the new address2
     * {@link String }
     */
    public void setAddress2(String pValue) {
        this.mAddress2 = pValue;
    }

    /**
     * Gets the value of the city property.
     *
     * @return the city
     * possible object is
     * {@link String }
     */
    public String getCity() {
        return mCity;
    }

    /**
     * Sets the value of the city property.
     *
     * @param pValue the new city
     * {@link String }
     */
    public void setCity(String pValue) {
        this.mCity = pValue;
    }

    /**
     * Gets the value of the state property.
     *
     * @return the state
     * possible object is
     * {@link String }
     */
    public String getState() {
        return mState;
    }

    /**
     * Sets the value of the state property.
     *
     * @param pValue the new state
     * {@link String }
     */
    public void setState(String pValue) {
        this.mState = pValue;
    }

    /**
     * Gets the value of the zipCode property.
     *
     * @return the zip code
     * possible object is
     * {@link String }
     */
    public String getZipCode() {
        return mZipCode;
    }

    /**
     * Sets the value of the zipCode property.
     *
     * @param pValue the new zip code
     * {@link String }
     */
    public void setZipCode(String pValue) {
        this.mZipCode = pValue;
    }

}
