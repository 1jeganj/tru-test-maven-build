package com.tru.feedprocessor.tol.store.processor;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.TRUStoreFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.store.feedvo.Store;

/**
 * The Class TRUStoreItemValidator will validate the mandatory properties if mandatory properties are empty then that
 * record is skipped and processed the next record.
 */
public class TRUStoreItemValidator implements IFeedItemValidator {

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator#validate(com.tru.feedprocessor.tol.
	 * base.vo.BaseFeedProcessVO)
	 */
	@Override
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {
		getLogger().vlogDebug(
				"Begin:@Class: TRUStoreItemValidator : @Method: validate(): store Id::"
						+ pBaseFeedProcessVO.getRepositoryId());
		Store storeVO = (Store) pBaseFeedProcessVO;
		
		if (storeVO.getLocationCode() == null) {
			throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_LOCATION_CODE, storeVO, null);
		}

		if (storeVO.getStoreDetail() == null) {
			throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.INVALID_RECORD, storeVO,null);
		}
		if (storeVO.getStoreDetail() != null) {
			if (StringUtils.isBlank(storeVO.getStoreDetail().getLocationName())) {
				throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_LOCATION_NAME,
						storeVO,null);
			}
			if (StringUtils.isBlank(storeVO.getStoreDetail().getChainCode())) {
				throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_CHAIN_CODE,
						storeVO,null);
			}
			if (StringUtils.isBlank(storeVO.getStoreDetail().getWarehouseLocationCode().toString())) {
				throw new FeedSkippableException(null, null,
						TRUStoreFeedReferenceConstants.REQUIRED_WAREHOUSE_LOCATION_CODE, storeVO,null);
			}
			if (!StringUtils.isBlank(storeVO.getStoreDetail().getLocationType())
					&& storeVO.getStoreDetail().getLocationType().equalsIgnoreCase(TRUStoreFeedReferenceConstants.ACTIVE_STORE)) {
				if (storeVO.getTaxware() != null && StringUtils.isBlank(storeVO.getTaxware().getGeoCode())) {
					throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_GEO_CODE, storeVO, null);
				}
				if (storeVO.getTaxware() != null && StringUtils.isBlank(storeVO.getTaxware().getGeoZip())) {
					throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_GEO_ZIP, storeVO, null);
				}
				if (storeVO.getTaxware() != null && StringUtils.isBlank(storeVO.getTaxware().getEnterpriseZone())) {
					throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_ENTERPRISE_ZONE, storeVO, null);
				}
			}
		}

		if (storeVO.getAddress() == null) {
			throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.INVALID_RECORD, storeVO,null);
		}

		if (storeVO.getAddress() != null) {
			if (StringUtils.isBlank(storeVO.getAddress().getAddress1())) {
				throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_ADDRESS1, storeVO,null);
			}
			if (StringUtils.isBlank(storeVO.getAddress().getCity())) {
				throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_CITY, storeVO,null);
			}
			if (StringUtils.isBlank(storeVO.getAddress().getState())) {
				throw new FeedSkippableException(null, null, TRUStoreFeedReferenceConstants.REQUIRED_STATE, storeVO,null);
			}
		}
		getLogger().vlogDebug("End:@Class: TRUStoreItemValidator : @Method: validate()");
	}
}
