package com.tru.feedprocessor.tol;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * The Class TRUStoreLocatorFeedScheduler
 * 
 * This class is responsible for Store feed Processing. This scheduler runs in every 1 day
 * 
 * @version 1.0
 * 
 */
public class TRUStoreFeedScheduler extends SingletonSchedulableService {

	/** Property to hold Feed Job. */
	private BatchInvoker mFeedJob;

	/** Property to hold Enable. */
	private boolean mEnable;

	/**
	 * Gets the feed job.
	 * 
	 * @return the mFeedJob
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 * 
	 * @param pFeedJob
	 *            the mFeedJob to set
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		this.mFeedJob = pFeedJob;
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable
	 *            the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * 
	 * This method invokes the Store Feed process operation as per schedule time.
	 * 
	 * @param pScheduler
	 *            - Scheduler instance
	 * @param pJob
	 *            - ScheduledJob instance
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {
		vlogInfo("BEGIN : TRUStoreFeedScheduler method doScheduledTask()");

		if (isEnable()) {
			processStoreFeed();
		}

		vlogInfo("END : TRUStoreFeedScheduler method doScheduledTask()");
	}

	/**
	 * 
	 * This method invokes the Store Feed process operation as per schedule time.
	 * 
	 */
	public void processStoreFeed() {
		vlogInfo("BEGIN : TRUStoreFeedScheduler method StoreFeed()");
		BatchInvoker lBatchInvoker = new BatchInvoker();
		lBatchInvoker.loadContext(new String[] { FeedConstants.JOB_CONFIG_PATH, FeedConstants.STORE_FEED_CONFIG_PATH });
		String[] input = { FeedConstants.STORE_JOB_NAME };

		lBatchInvoker.startJob(input);
		vlogInfo("END : TRUStoreFeedScheduler method StoreFeed()");
	}

}
