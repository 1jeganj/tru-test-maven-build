drop table tru_location_info;
drop table tru_store_locator_extensions;
drop table tru_store_trading_hours;


create table  tru_location_info (	
   	location_id varchar2(5) not null,
	chain_code varchar2(3) not null, 
	warehouse_location_code number(5) not null, 
	manager varchar2(25),
	online_layaway varchar2(1),
	in_store_pickup varchar2(1),
	inventory_search_online varchar2(1),
	sidebyside_store varchar2(1),
	ship_from_store varchar2(1),
	location_type varchar2(1);
	asset_version number(19,0) not null enable,
	constraint tru_location_info_p primary key (location_id,asset_version)
   ) ;
   
   create table tru_store_locator_extensions (
	location_id	varchar2(5)	not null,
	trading_hour_id	varchar2(20),
	asset_version	number(19)	not null,
	sequence_number	integer	not null,
	constraint tru_store_locator_extensions_p primary key (location_id,sequence_number,asset_version)
	);
	
	create table tru_store_trading_hours (
	id varchar2(40) not null,	
	asset_version	number(19)	not null,
	workspace_id	varchar2(40)	not null,
	branch_id	varchar2(40)	not null,
	is_head	number(1,0)	not null,
	version_deleted	number(1,0)	not null,
	version_editable number(1,0)	not null,
	pred_version	number(19,0),
	checkin_date timestamp (6),
	day integer,
	opening_hours number(4),
	closing_hours number(4),
	constraint tru_store_trading_hours_p primary key (id,asset_version)
	);
	
