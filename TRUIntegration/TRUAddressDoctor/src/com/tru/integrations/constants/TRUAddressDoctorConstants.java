package com.tru.integrations.constants;



/**
 * This class hold all addressdoctor related constants.
 */
public class TRUAddressDoctorConstants {

	/**
	 * CONSTANT TO HOLD altruik response
	 */
	public static final String ALTRUIK_RESPONSE_URL = "altruikResponseUrl";

	/**
	 * Constant to hold comma value.
	 */
	public static final int TWO_HUNDRED = 200;

	/**
	 * Constant to hold SLASH value.
	 */
	public static final String ERROR_CODE = "503";

	/**
	 * Constant to hold PORT1 value.
	 */
	public static final int PORT1 = 8192;

	/**
	 * Constant to hold EMPTY_STRING value.
	 */
	public static final String EMPTY_STRING = "";

	/**
	 * Constant to hold OUTPUT value.
	 */
	public static final String OUTPUT = "output";

	/**
	 * Constant to hold HEAD value.
	 */
	public static final String HEAD = "head";

	/**
	 * Constant to hold CANONICAL_URL value.
	 */
	public static final String CANONICAL_URL = "canonicalUrl";
	
	/**
	 * Constant to hold EMPTY_OPARAM value.
	 */
	public static final String EMPTY_OPARAM = "empty";
	
	/**
	 * Constant to hold SEARCHDEX_INPUT_URL value.
	 */
	public static final String SEARCHDEX_INPUT_URL = "searchDexUrl";
	
	/**
	 * Holds the property value of SPACE
	 */  
	 public static final String SPACE= " ";
	 
	 /**
	 * Holds the property value of PERC_TWENTY
	 */  
	 public static final String PERC_TWENTY= "%20";
	 
	/**
	 * Constant to hold SLASH value.
	 */
	public static final String SLASH = "/";

	/**
	 * Constant for ADDRESS_DOCTOR.
	 */
	public static final String ADDRESS_DOCTOR = "addressDoctor";
	
	
	/**
	 * Constant to http exception
	 */
	public static final String HTTP_EXCEPTION = "httpException";


	/**
	 * Constant to io exception
	 */
	public static final String IO_EXCEPTION = "ioexception";
	
	
	/**
	 * Constant to bad request
	 */
	public static final String BAD_REQUEST = "badRequest";

	/**
	 * Constant for ORDER_ID.
	 */
	public static final String ORDER_ID = "orderId";
	
	/**
	 * Constant for USER_ID.
	 */
	public static final String USER_ID = "userId";
	
	/** Property to hold  page. */
	public static final String PAGE = "page";
	
	/** The Constant RESPONSE_CODE. */
	public static final String RESPONSE_CODE = "Response code";
	
	/** The Constant STR_ERROR. */
	public static final String STR_ERROR = "Error";
	
	/** The Constant TASK_NAME. */
	public static final String TASK_NAME = "Task Name";
	
	/** The Constant DEFAULT_ERROR_MESSAGE. */
	public static final String DEFAULT_ERROR_MESSAGE = "Connection timeout";
	
	/** The Constant DEFAULT_TASK_NAME. */
	public static final String DEFAULT_TASK_NAME = "AddAddress";

}