package com.tru.integrations.addressdoctor;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.constants.TRUAddressDoctorConstants;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;


/**
 * This class holds the business logic for the address validation.
 * After constructing the URL .
 * @author PA
 * @version 1.0 
 */
public class TRUAddressDoctorService extends GenericService{

	/**
	 *  property to hold mEnableProxy.
	*/
	private boolean mEnableProxy;
	/**
	 * Holds the property value of mCountryCodePropertyName.
	 */
	private String mCountryCodePropertyName;
	/**
	 * Holds the property value of mZipCodePropertyName.
	 */
	private String mZipCodePropertyName;
	/**
	 * Holds the property value of mAddressLine1PropertyName.
	 */
	private String mAddressLine1PropertyName;
	/**
	 * Holds the property value of mAddressLine2PropertyName.
	 */
	private String mAddressLine2PropertyName;
	/**
	 * Holds the property value of mAddressLine1RequestName.
	 */
	private String mAddressLine1RequestName;
	/**
	 * Holds the property value of mAddressLine2RequestName.
	 */
	private String mAddressLine2RequestName;
	/**
	 * Holds the property value of mCityPropertyName.
	 */
	private String mCityPropertyName;
	/**
	 * Holds the property value of mStatePropertyName.
	 */
	private String mStatePropertyName;
	
	/**
	 * variable to hold mSelectedFacets - String.
	 */
	private String mAddressDoctorURL;
	
	/**
	 * variable to hold mLocalProxyHost - String.
	 */
	private String mLocalProxyHost;
	
	/**
	 * Holds the property value of mCountryCodeValue.
	 */
	private String mCountryCodeValue;
	
	/**
	 * variable to hold mLocalProxyPort - String.
	 */
	private int mLocalProxyPort;
	
	/**
	 *  property to hold mEnableResponse.
	 */
	private boolean mEnableResponse;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	
	
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * This method is used to get enableProxy.
	 * @return mEnableResponse boolean
	 */
	public boolean isEnableResponse() {
		return mEnableResponse;
	}


	/**
	 *This method is used to set enableResponse.
	 *@param pEnableResponse boolean
	 */
	public void setEnableResponse(boolean pEnableResponse) {
		mEnableResponse = pEnableResponse;
	}


	/**
	 * This method is used to get enableProxy.
	 * @return mEnableProxy boolean
	 */
	public boolean isEnableProxy() {
		return mEnableProxy;
	}


	/**
	 *This method is used to set enableProxy.
	 *@param pEnableProxy boolean
	 */
	public void setEnableProxy(boolean pEnableProxy) {
		mEnableProxy = pEnableProxy;
	}


	/**
	 * This method is used to get countryCodePropertyName.
	 * @return mCountryCodePropertyName String
	 */
	public String getCountryCodePropertyName() {
		return mCountryCodePropertyName;
	}


	/**
	 *This method is used to set countryCodePropertyName.
	 *@param pCountryCodePropertyName String
	 */
	public void setCountryCodePropertyName(String pCountryCodePropertyName) {
		mCountryCodePropertyName = pCountryCodePropertyName;
	}


	/**
	 * This method is used to get zipCodePropertyName.
	 * @return mZipCodePropertyName String
	 */
	public String getZipCodePropertyName() {
		return mZipCodePropertyName;
	}


	/**
	 *This method is used to set zipCodePropertyName.
	 *@param pZipCodePropertyName String
	 */
	public void setZipCodePropertyName(String pZipCodePropertyName) {
		mZipCodePropertyName = pZipCodePropertyName;
	}


	/**
	 * This method is used to get addressLine1PropertyName.
	 * @return mAddressLine1PropertyName String
	 */
	public String getAddressLine1PropertyName() {
		return mAddressLine1PropertyName;
	}


	/**
	 *This method is used to set addressLine1PropertyName.
	 *@param pAddressLine1PropertyName String
	 */
	public void setAddressLine1PropertyName(String pAddressLine1PropertyName) {
		mAddressLine1PropertyName = pAddressLine1PropertyName;
	}


	/**
	 * This method is used to get addressLine2PropertyName.
	 * @return mAddressLine2PropertyName String
	 */
	public String getAddressLine2PropertyName() {
		return mAddressLine2PropertyName;
	}


	/**
	 *This method is used to set addressLine2PropertyName.
	 *@param pAddressLine2PropertyName String
	 */
	public void setAddressLine2PropertyName(String pAddressLine2PropertyName) {
		mAddressLine2PropertyName = pAddressLine2PropertyName;
	}

	/**
	 * This method is used to get addressLine1RequestName.
	 * @return mAddressLine1RequestName String
	 */
	public String getAddressLine1RequestName() {
		return mAddressLine1RequestName;
	}


	/**
	 *This method is used to set addressLine1RequestName.
	 *@param pAddressLine1RequestName String
	 */
	public void setAddressLine1RequestName(String pAddressLine1RequestName) {
		mAddressLine1RequestName = pAddressLine1RequestName;
	}
	
	/**
	 * This method is used to get addressLine2RequestName.
	 * @return mAddressLine2RequestName String
	 */
	public String getAddressLine2RequestName() {
		return mAddressLine2RequestName;
	}


	/**
	 *This method is used to set addressLine2RequestName.
	 *@param pAddressLine2RequestName String
	 */
	public void setAddressLine2RequestName(String pAddressLine2RequestName) {
		mAddressLine2RequestName = pAddressLine2RequestName;
	}

	/**
	 * This method is used to get cityPropertyName.
	 * @return mCityPropertyName String
	 */
	public String getCityPropertyName() {
		return mCityPropertyName;
	}


	/**
	 *This method is used to set cityPropertyName.
	 *@param pCityPropertyName String
	 */
	public void setCityPropertyName(String pCityPropertyName) {
		mCityPropertyName = pCityPropertyName;
	}


	/**
	 * This method is used to get statePropertyName.
	 * @return mStatePropertyName String
	 */
	public String getStatePropertyName() {
		return mStatePropertyName;
	}


	/**
	 *This method is used to set statePropertyName.
	 *@param pStatePropertyName String
	 */
	public void setStatePropertyName(String pStatePropertyName) {
		mStatePropertyName = pStatePropertyName;
	}


	/**
	 * This method is used to get addressDoctorURL.
	 * @return mAddressDoctorURL String
	 */
	public String getAddressDoctorURL() {
		return mAddressDoctorURL;
	}


	/**
	 *This method is used to set addressDoctorURL.
	 *@param pAddressDoctorURL String
	 */
	public void setAddressDoctorURL(String pAddressDoctorURL) {
		mAddressDoctorURL = pAddressDoctorURL;
	}


	/**
	 * This method is used to get localProxyHost.
	 * @return mLocalProxyHost String
	 */
	public String getLocalProxyHost() {
		return mLocalProxyHost;
	}


	/**
	 *This method is used to set localProxyHost.
	 *@param pLocalProxyHost String
	 */
	public void setLocalProxyHost(String pLocalProxyHost) {
		mLocalProxyHost = pLocalProxyHost;
	}


	/**
	 * This method is used to get countryCodeValue.
	 * @return mCountryCodeValue String
	 */
	public String getCountryCodeValue() {
		return mCountryCodeValue;
	}


	/**
	 *This method is used to set countryCodeValue.
	 *@param pCountryCodeValue String
	 */
	public void setCountryCodeValue(String pCountryCodeValue) {
		mCountryCodeValue = pCountryCodeValue;
	}


	/**
	 * This method is used to get localProxyPort.
	 * @return mLocalProxyPort int
	 */
	public int getLocalProxyPort() {
		return mLocalProxyPort;
	}


	/**
	 *This method is used to set localProxyPort.
	 *@param pLocalProxyPort int
	 */
	public void setLocalProxyPort(int pLocalProxyPort) {
		mLocalProxyPort = pLocalProxyPort;
	}


	/**
	 * This method validates the address entered by user.
	 * @param pAddress Map<String,String>.
	 * @return addressDoctorResponse String.
	 */
	
	public String validateAddress(Map<String,String> pAddress) {
		
		if(isLoggingDebug()){
			logDebug("STARTED :: TRUAddressDoctorService ::  validateAddress");
    	}
		 String addressDoctorResponse = null;
		 
		 
	        if(pAddress != null && pAddress.get(getZipCodePropertyName()) != null ){
	        	if( pAddress.get(getCityPropertyName()) != null && pAddress.get(getStatePropertyName()) != null){    
	        		if(isLoggingDebug()){
	               	  vlogDebug("CITY::{0}",pAddress.get(getCityPropertyName()));
	               	  vlogDebug("COUNTRY::{0}",getCountryCodeValue());
	               	  vlogDebug("STATE::{0}",pAddress.get(getStatePropertyName()));
	               	  vlogDebug("Address Line 1::{0}",pAddress.get(getAddressLine1PropertyName()));
	               	  vlogDebug("Address Line 2::{0}",pAddress.get(getAddressLine2PropertyName()));
	                  vlogDebug("ZIP CODE ::{0}",pAddress.get(getZipCodePropertyName()));
	      	      
	        		}
	        		 String serURL= getAddressDoctorURL();
	       	         HttpClient client = new HttpClient();
	       	         String city =pAddress.get(getCityPropertyName()).trim().replace(TRUAddressDoctorConstants.SPACE,
	       	        		 TRUAddressDoctorConstants.PERC_TWENTY);
	       	         String state =pAddress.get(getStatePropertyName()).trim().replace(TRUAddressDoctorConstants.SPACE,
	       	        		 TRUAddressDoctorConstants.PERC_TWENTY);
	       	         String finalURL= null;
	       	         if (StringUtils.isNotBlank(serURL)) {
	       	        	finalURL=serURL.concat(TRUAddressDoctorConstants.SLASH).concat(getCountryCodeValue()).
		                		 concat(TRUAddressDoctorConstants.SLASH).concat(pAddress.get(getZipCodePropertyName()).toString()).
		                		 concat(TRUAddressDoctorConstants.SLASH).concat(city).concat(TRUAddressDoctorConstants.SLASH).concat(state);     
	       	         }
	       	         vlogDebug("URL::{0}",finalURL);  
	                 if(isEnableProxy()){
	                    client.getHostConfiguration().setProxy(getLocalProxyHost(),getLocalProxyPort());
	                 }
	                    GetMethod method = new GetMethod(finalURL);
	                    NameValuePair[] params = null;
	                    
	                    if(!StringUtils.isBlank(pAddress.get(getAddressLine1PropertyName()))){
	                    	String addressLine2 = TRUAddressDoctorConstants.EMPTY_STRING;
	                    	if(!StringUtils.isBlank(pAddress.get(getAddressLine2PropertyName()))){
	                        	  addressLine2 = pAddress.get(getAddressLine2PropertyName());
	                          }
	                        NameValuePair param1= new NameValuePair(getAddressLine1RequestName(), pAddress.get(getAddressLine1PropertyName()));
	                        NameValuePair param2= new NameValuePair(getAddressLine2RequestName(), addressLine2);
	                        params = new NameValuePair[]{param1,param2};
	                   
	                    if(params != null && method != null){
	                         method.setQueryString(params);
	                    }
	                    }else{
	                    	 if(isLoggingDebug()){
               	  			  logDebug("Please enter value for address Line1");
               	         	}
	                    }
	                   
	                    try {
                	        	  if(isLoggingDebug()){
                    	  			  logDebug("Request Url::"+method.getURI());
                    	         	}
                	        	  
	                	        	  if(!pAddress.containsKey(TRUAddressDoctorConstants.ORDER_ID) || !pAddress.containsKey(TRUAddressDoctorConstants.USER_ID)){
	                	        		  pAddress.put(TRUAddressDoctorConstants.ORDER_ID, null);
	                	        		  pAddress.put(TRUAddressDoctorConstants.USER_ID, null);
	                	        	  }
	                	        	 
	                	        	  getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(TRUAddressDoctorConstants.ADDRESS_DOCTOR), getIntegrationInfoLogger().getInterfaceNameMap().get(TRUAddressDoctorConstants.ADDRESS_DOCTOR), pAddress.get(TRUAddressDoctorConstants.ORDER_ID) ,pAddress.get(TRUAddressDoctorConstants.USER_ID));
	                                  int statusCode = client.executeMethod(method);
	                                  if(isLoggingDebug()){
	                                	  vlogDebug("Status Code::{0}",statusCode);
	                    	         	}
	                                 
	                                   if(statusCode == TRUAddressDoctorConstants.TWO_HUNDRED){
	                                	   getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(TRUAddressDoctorConstants.ADDRESS_DOCTOR), getIntegrationInfoLogger().getInterfaceNameMap().get(TRUAddressDoctorConstants.ADDRESS_DOCTOR), pAddress.get(TRUAddressDoctorConstants.ORDER_ID),pAddress.get(TRUAddressDoctorConstants.USER_ID));
	                                     if(isEnableResponse()){
	                                		   addressDoctorResponse = method.getResponseBodyAsString();
	                                	   }else{
	                                		   addressDoctorResponse = getResponseBody(method,false);
	                                	   }
	                                     if(isLoggingDebug()){
	                                    	 logDebug("AddressDoctor Response : " + addressDoctorResponse);
	                                     }
	                                   }else{	                                	   
	                                	   addressDoctorResponse = TRUAddressDoctorConstants.ERROR_CODE;
	                                	   if(isLoggingDebug()){
	                                		   vlogDebug("BAD REQUEST ::");
	                                	   }
	                                	   alertLogger(pAddress.get(TRUAddressDoctorConstants.TASK_NAME), TRUAddressDoctorConstants.BAD_REQUEST, pAddress.get(TRUAddressDoctorConstants.PAGE));
	                                   }
	                    } catch(HttpException httpException){
	                    	if(isLoggingError()){
	        					logError("Exception in TRUAddressDoctorService", httpException);
	        				}
	        				alertLogger(pAddress.get(TRUAddressDoctorConstants.TASK_NAME),TRUAddressDoctorConstants.HTTP_EXCEPTION, pAddress.get(TRUAddressDoctorConstants.PAGE));
	                    } catch (IOException exe) {
	                    	if(isLoggingError()){
	        					logError("IOException in TRUAddressDoctorService", exe);
	        				}
	        				alertLogger(pAddress.get(TRUAddressDoctorConstants.TASK_NAME), exe.getMessage(), pAddress.get(TRUAddressDoctorConstants.PAGE));
						} catch (IllegalArgumentException exe) {
	                    	if(isLoggingError()){
	        					logError("IllegalArgumentException in TRUAddressDoctorService", exe);
	        				}
	        				alertLogger(pAddress.get(TRUAddressDoctorConstants.TASK_NAME), exe.getMessage(), pAddress.get(TRUAddressDoctorConstants.PAGE));
						} 
	                    finally{
	                         method.releaseConnection();
	                    }
	               return addressDoctorResponse;
	        }else{
	        	vlogDebug("City or Satate is missing :{0}",pAddress);
	        }
		 }else{
			 vlogDebug("Country Code or Zip code is missing :{0}",pAddress);
		 }
		
		if(isLoggingDebug()){
			logDebug("END :: TRUAddressDoctorService ::  validateAddress");
    	}
		return addressDoctorResponse;
	   }
	
	
	/**
	 * This method used to format the response got from the addressDoctor response. 
	 * @param pMethod GetMethod
	 * @param pGetResponseCode boolean
	 * @return addrResponse String
	 */
	
	private String getResponseBody(GetMethod pMethod , boolean pGetResponseCode) {
		if(isLoggingDebug()){
			logDebug("STARTED :: TRUAddressDoctorService ::  getResponseBody");
    	}
		vlogDebug("Response Code :{0}",pGetResponseCode);
		String addrResponse = null;
		if(pMethod!=null && pMethod.hasBeenUsed()){
			BufferedReader in = null;
			StringWriter stringOut = new StringWriter();
			BufferedWriter dumpOut = new BufferedWriter(stringOut,TRUAddressDoctorConstants.PORT1);
			try{
				 	if(isLoggingDebug()){
						logDebug("TRUAddressDoctorService ::  getResponseBody" + pMethod.getResponseBodyAsStream());
					}
				 in=new BufferedReader(new InputStreamReader(pMethod.getResponseBodyAsStream()));
				 String line = TRUAddressDoctorConstants.EMPTY_STRING;
				 while((line=in.readLine())!=null){
					 dumpOut.write(line);
					 dumpOut.newLine();
				 }
			}catch (IOException ioException) {
				if(isLoggingError()){
					logError("IOException in getResponseBody", ioException);
				}
			}finally{
				try{
					dumpOut.flush();
					dumpOut.close();
					if(in!=null){
						in.close();
					}
				}catch (IOException ioException) {
					if(isLoggingError()){
						logError("IOException in getResponseBody", ioException);
					}
				}
			}
			addrResponse = stringOut.toString();
		}
		if(isLoggingDebug()){
			logDebug("END :: TRUAddressDoctorService ::  getResponseBody");
    	}
		return addrResponse;
	}
	
	/**
	 * Alert logger.
	 *
	 * @param pTaskName the task name
	 * @param pErrorMessage the error message
	 * @param pPageName the page name
	 */
	private void alertLogger(String pTaskName , String pErrorMessage ,String pPageName){
		String errorMessage = TRUAddressDoctorConstants.DEFAULT_ERROR_MESSAGE;
		String taskName = TRUAddressDoctorConstants.DEFAULT_TASK_NAME;
		Map<String, String> extra = new ConcurrentHashMap<String, String>();	
		extra.put(TRUAddressDoctorConstants.RESPONSE_CODE,TRUAddressDoctorConstants.ERROR_CODE );	
		if (StringUtils.isNotBlank(pPageName)) {
			extra.put(TRUAddressDoctorConstants.PAGE, pPageName);
		}
		if (StringUtils.isNotBlank(pTaskName)) {
			taskName = pTaskName;
		}
		if (StringUtils.isNotBlank(pErrorMessage)) {
			errorMessage = pErrorMessage;
		}
		extra.put(TRUAddressDoctorConstants.STR_ERROR, errorMessage);
		getAlertLogger().logFailure(TRUAddressDoctorConstants.ADDRESS_DOCTOR,taskName,extra);
	}
}
