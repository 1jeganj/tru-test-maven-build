package com.tru.integrations.addressdoctor.droplet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.servlet.ServletException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;

import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.integrations.constants.TRUAddressDoctorConstants;
import com.tru.proxy.ProxyConfiguration;


/**
 * The Class TRUAltruikUrlDroplet used to get the dynamic canonical url in PDP page.
 *
 * @author PA
 * @version 1.0
 */
public class TRUAltruikUrlDroplet extends DynamoServlet {
	
	/**
	 *  property to hold mProxyConfiguration.
	*/
	private ProxyConfiguration mProxyConfiguration;
	
	/**
	 * This method is used to get proxyConfiguration.
	 * @return mProxyConfiguration ProxyConfiguration
	 */
	public ProxyConfiguration getProxyConfiguration() {
		return mProxyConfiguration;
	}
	/**
	 *This method is used to set proxyConfiguration.
	 *@param pProxyConfiguration ProxyConfiguration
	 */
	public void setProxyConfiguration(ProxyConfiguration pProxyConfiguration) {
		mProxyConfiguration = pProxyConfiguration;
	}

	/**
	 * This method used to set the altruik URL for the PDP page.
	 * @param pRequest the DynamoHttpServletRequest
	 * @param pResponse the DynamoHttpServletResponse
	 * @throws ServletException the servlet exception
	 * @throws IOException the IOException
	 */
		
		public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
			if(isLoggingDebug()){
			logDebug("BEGIN:: TRUAltruikUrlDroplet.service() method:: ");
			}
			
			
			String altruikResponseUrl = TRUAddressDoctorConstants.EMPTY_STRING;
			altruikResponseUrl = getAltrukaiResponse(pRequest);
			if(!StringUtils.isBlank(altruikResponseUrl)){
			pRequest.setParameter(TRUAddressDoctorConstants.ALTRUIK_RESPONSE_URL, altruikResponseUrl);
	    	pRequest.serviceParameter(TRUAddressDoctorConstants.OUTPUT, pRequest, pResponse);
			}
			else{
				altruikResponseUrl = pRequest.getRequestURI().toString();
				pRequest.setParameter(TRUAddressDoctorConstants.ALTRUIK_RESPONSE_URL, altruikResponseUrl);
		    	pRequest.serviceParameter(TRUAddressDoctorConstants.EMPTY_OPARAM, pRequest, pResponse);
			}
	    	if(isLoggingDebug()){
			logDebug("END:: TRUAltruikUrlDroplet.service() method:: ");
	    	}
		}

	
		/**
		 * This method used to get the searchDex response 
		 * @param pRequest DynamoHttpServletRequest
		 * @return altruikUrl String
		 */
		
		private String getAltrukaiResponse(DynamoHttpServletRequest pRequest) {
			if(isLoggingDebug()){
				logDebug("STARTED :: TRUAltruikUrlDroplet ::  getAltrukaiResponse");
	    	}
			String altruikResponse = TRUAddressDoctorConstants.EMPTY_STRING;
			String altruikUrl = TRUAddressDoctorConstants.EMPTY_STRING;
			JSONObject jsonAltruikResponse = null;
			String searchDexURL = (String) pRequest.getLocalParameter(TRUAddressDoctorConstants.SEARCHDEX_INPUT_URL);
			if(isLoggingDebug()){
             	  logDebug("SearchDex Request URL::::"+searchDexURL);
 	         	   }
			if(!StringUtils.isBlank(searchDexURL)){
			GetMethod method = new GetMethod();
			HttpClient client = new HttpClient();
			
			try {
				method = new GetMethod(searchDexURL);
				if(getProxyConfiguration() != null && getProxyConfiguration().isProxyEnabled()){
					if(isLoggingDebug()){
		             	  logDebug("Proxy Host::"+getProxyConfiguration().getProxyName());
		 	         	   }
					if(isLoggingDebug()){
		             	  logDebug("Proxy Port::"+getProxyConfiguration().getProxyPort());
		 	         	   }
					client.getHostConfiguration().setProxy(getProxyConfiguration().getProxyName(),getProxyConfiguration().getProxyPort());
				}
				 int statusCode = client.executeMethod(method);
                 if(isLoggingDebug()){
               	  vlogDebug("Status Code::{0}",statusCode);
   	         	   }                
                  if(statusCode == TRUAddressDoctorConstants.TWO_HUNDRED){
                	  if(method != null && method.hasBeenUsed()){
          				BufferedReader in = null;
          				StringWriter stringOut = new StringWriter();
          				BufferedWriter dumpOut = new BufferedWriter(stringOut,TRUAddressDoctorConstants.PORT1);
          				try{
          					 if(isLoggingDebug()){
          							logDebug("TRUAltruikUrlDroplet :: getAltrukaiResponse()" + method.getResponseBodyAsStream());
          						}
          					 in=new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
          					 String line = TRUAddressDoctorConstants.EMPTY_STRING;
          					 while((line=in.readLine())!=null){
          						 dumpOut.write(line);
          						 dumpOut.newLine();
          					 }
          				}catch (IOException ioException) {
          					if(isLoggingError()){
          						logError("IOException in getAltrukaiResponse() while writing", ioException);
          					}
          				}finally{
          					try{
          						dumpOut.flush();
          						dumpOut.close();
          						if(in!=null){
          							in.close();
          						}
          					}catch (IOException ioException) {
          						if(isLoggingError()){
          							logError("IOException in getAltrukaiResponse() while flushing", ioException);
          						}
          					}
          				}
          				altruikResponse = stringOut.toString();
          				if(altruikResponse != null){
          				try {
                         	jsonAltruikResponse = new JSONObject(altruikResponse);
                         	altruikUrl = jsonAltruikResponse.getJSONObject(TRUAddressDoctorConstants.HEAD).getString(TRUAddressDoctorConstants.CANONICAL_URL);
                         	if(isLoggingDebug()){
                 	   			logDebug("jsonAltruikResponse ::"+jsonAltruikResponse);
                 	        }
                         }
                         catch (JSONException e) {
                        	 if(isLoggingError()){
             					logError("JSONException in TRUAltruikUrlDroplet", e);
             				}
                         }
          			  }
          			}
                  }else{
                	  StringBuffer altruikUrlRequest = pRequest.getRequestURL();
                	  altruikUrl = altruikUrlRequest.toString();
                	  
                  }
			
			} catch(HttpException httpException){
            	if(isLoggingError()){
					logError("HttpException in TRUAltruikUrlDroplet ::", httpException);
				}
            } 
			catch (IOException e) {
				if(isLoggingError()){
					logError("IOException in TRUAltruikUrlDroplet ::", e);
				}				
			}
			catch(IllegalArgumentException illegalArgumentException){
            	if(isLoggingError()){
					logError("IllegalArgumentException in TRUAltruikUrlDroplet as URL is not proper:",illegalArgumentException);
					
				}
            } 
			finally{
                method.releaseConnection();
            }
			}
			else{
				if(isLoggingDebug()){
					logDebug("SerachDex URL is Empty");
		    	}
			}
			if(isLoggingDebug()){
				logDebug("END :: TRUAltruikUrlDroplet ::  getAltrukaiResponse");
	    	}
			return altruikUrl;
		}
    
 }
