package com.tru.feedprocessor.tol;

// TODO: Auto-generated Javadoc
/**
 * The Class TRUSeoFeedReferenceConstants.
 * 
 */
public class TRUSeoFeedReferenceConstants {

	/** The Constant XLS_SHEET. */
	public static final String XLS_SHEET = "sheet";

	/** The Constant CAP_DELIMITER. */
	public static final char CAP_DELIMITER = '^';

	/** The Constant ID. */
	public static final String ID = "id";

	/** The Constant SEO_VO. */
	public static final String SEO_VO = "com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO";

	/** The Constant SEO_VO. */
	public static final String SEO_CAT_VO = "com.tru.feedprocessor.tol.seo.vo.TRUSeoCatFeedVO";

	/** The Constant META_KEYWORD. */
	public static final String META_KEYWORD = "metaKeyword";

	/** The Constant CANONICAL_URL. */
	public static final String CANONICAL_URL = "canonicalUrl";

	/** The Constant H1. */
	public static final String H1 = "mSeoH1Text";

	/** The Constant ALT_URL. */
	public static final String ALT_URL = "altUrl";

	/** The Constant DEAL_ID. */
	public static final String CONTENT_BLOCK = "contentBlock";

	/** The Constant PAGE_TITLE. */
	public static final String PAGE_TITLE = "pageTitle";

	/** The Constant META_DESCRIPTION. */
	public static final String META_DESCRIPTION = "metaDescription";

	/** The Constant PRICE_LIST_ID. */
	public static final String PRICE_LIST_ID = "id";

	/** The Constant PRICE_ID. */
	public static final String PRICE_ID = "price_id";

	/** The Constant DISPLAY_NAME. */
	public static final String DISPLAY_NAME = "displayName";

	/** The Constant PRICELIST. */
	public static final String PRICELIST = "priceList";

	/** The Constant DOUBLE_SLASH_DOT. */
	public static final String DOUBLE_SLASH_DOT = "\\.";

	/** The Constant PROCESSING_DIRECTORY. */
	public static final String PROCESSING_DIRECTORY = "processingDirectory";

	/** The Constant UN_PROCESS_DIRECTORY. */
	public static final String UN_PROCESS_DIRECTORY = "unProcessDirectory";

	/** The Constant BAD_DIRECTORY. */
	public static final String BAD_DIRECTORY = "badDirectory";

	/** The Constant DOUBLE_VAL. */
	public static final double DOUBLE_VAL = 0.0;

	/** The Constant NUMBER_HUNDRED. */
	public static final int NUMBER_HUNDRED = 100;

	/** The Constant PIPE_DELIMITER_STRING. */
	public static final String PIPE_DELIMITER_STRING = "|";

	/** The Constant IS_NUMBER_REGX. */
	public static final String IS_NUMBER_REGX = "[0-9]+(\\.[0-9]+)";

	/** The Constant INVALID_RECORD. */
	public static final String INVALID_RECORD = "Invalid Record:";

	/** The Constant SUCCESS_DIRECTORY. */
	public static final String SUCCESS_DIRECTORY = "successDirectory";

	/** The Constant ERROR_DIRECTORY. */
	public static final String ERROR_DIRECTORY = "errorDirectory";

	/** The Constant XLS. */
	public static final String XLS = "xls";

	/** The Constant REQUIRED_ID. */
	public static final String REQUIRED_ID = "Invalid Record: Id Is Required";

	/** The Constant INVALID_SALE_PRICE. */
	public static final String INVALID_SALE_PRICE = "Invalid Record: Sale Price Is Not Valid";

	/** The Constant INVALID_LIST_PRICE. */
	public static final String INVALID_LIST_PRICE = "Invalid Record: List Price Is Not Valid";

	/** The Constant INVALID_STORE_NO. */
	public static final String INVALID_STORE_NO = "Invalid Record: Store Number Is Not Valid";

	/** The Constant REQUIRED_DISPLAY_NAME. */
	public static final String REQUIRED_DISPLAY_NAME = "Invalid Record: Display Name Is Required";

	/** The Constant INVALID_COUNTRY_CODE. */
	public static final String INVALID_COUNTRY_CODE = "Invalid Record: Country Code Is Not Valid";

	/** The Constant INVALID_MARKET_CODE. */
	public static final String INVALID_MARKET_CODE = "Invalid Record: Market Code Is Not Valid";

	/** The Constant INVALID_LOCALE. */
	public static final String INVALID_LOCALE = "Invalid Record: Locale Is Not Valid";

	/** The Constant INVALID_SKUID. */
	public static final String INVALID_SKUID = "Invalid Record: Sku Id Is Not Valid";

	/** The Constant FEED_FILE_PATTERN. */
	public static final String FEED_FILE_PATTERN = "feedFilePattern";

	/** The Constant FILE_NAME_TRU_ENV. */
	public static final String FILE_NAME_TRU_ENV = "fileNameTRUEnv";

	/** The Constant FILE_FEED_NAME. */
	public static final String FILE_FEED_NAME = "fileFeedName";

	/** The Constant STRING_VAL_WITH_3_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_3_DIGIT_PATTERN = "^[a-zA-Z]{1,3}$";

	/** The Constant STRING_VAL_WITH_5_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_5_DIGIT_PATTERN = "^[\\w]{1,5}$";

	/** The Constant INTEGER_VAL_WITH_5_DIGIT_PATTERN. */
	public static final String INTEGER_VAL_WITH_5_DIGIT_PATTERN = "^[0-9]{1,5}$";

	/** The Constant DOUBLE_VAL_WITH_DECIMAL_DIGIT_PATTERN. */
	public static final String DOUBLE_VAL_WITH_DECIMAL_DIGIT_PATTERN = "^([0-9]{1,19})?([.]{1}[0-9]{1,7})?$";

	/** The Constant DECIMAL_FORMAT_UP_TO_2_DIGIT. */
	public static final String DECIMAL_FORMAT_UP_TO_2_DIGIT = "0.00";

	/** The Constant FEED_FILE_HEAD_RECORD_KEY. */
	public static final String FEED_FILE_HEAD_RECORD_KEY = "HEADER";

	/** The Constant SALEPRICELIST. */
	public static final String SALEPRICELIST = "salePriceList";

	/** The Constant LISTPRICELIST. */
	public static final String LISTPRICELIST = "listPriceList";

	/** The Constant TRUE. */
	public static final String TRUE = "true";

	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";

	/** The Constant SOURCE_DIRECTORY. */
	public static final String SOURCE_DIRECTORY = "sourceDirectory";

	/** The Constant PRICE_REPOSITORY. */
	public static final String PRICE_REPOSITORY = "priceRepository";

	/** The Constant BASE_PRICE_LIST. */
	public static final String BASE_PRICE_LIST = "basePriceList";

	/** The Constant PRICE_CONSTANT. */
	public static final String PRICE = "price";

	/** The Constant FEED_SKIPPABLE_EXCEPTION. */
	public static final String FEED_SKIPPABLE_EXCEPTION = "FeedSkippableException";

	/** The Constant SEO_FEED. */
	public static final String SEO_FEED = "seofeed";

	/** The Constant FEED_FILE_SEQUENCE. */
	public static final String FEED_FILE_SEQUENCE = "feedfilesequence";

	/** The Constant FILE_SEQUENCE_MISSING. */
	public static final String FILE_SEQUENCE_MISSING = "File sequence missing";

	/** The Constant FILE_SEQUENCE_NO_MATCH. */
	public static final String FILE_SEQUENCE_NO_MATCH = "File sequence no match";

	/** The Constant for Space. */
	public static final String SPACE = " ";

	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;

	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;

	/** The Constant EMAIL_FAIL_MESSAGE1. */
	public static final String EMAIL_FAIL_MESSAGE1_PRICE = "TRU ATG Web Store - USA - ";

	/** The Constant EMAIL_FAIL_MESSAGE2. */
	public static final String EMAIL_FAIL_MESSAGE2_PRICE = " - Price - ";

	/** The Constant EMAIL_FAIL_MESSAGE3. */
	public static final String EMAIL_FAIL_MESSAGE3_PRICE = " Processed with Errors - ";

	/** The Constant EMAIL_FAIL_MESSAGE4. */
	public static final String EMAIL_FAIL_MESSAGE4_PRICE = " Processing Failed - ";

	/** The Constant EMAIL_FAIL_MESSAGE5. */
	public static final String EMAIL_FAIL_MESSAGE5_PRICE = "MMddyyyy'-'hh:mm:ss";

	/** The Constant EMAIL_FAIL_MESSAGE6. */
	public static final String EMAIL_FAIL_MESSAGE6_PRICE = " Processed Successfully - ";

	/** The Constant SEQ_MISS. */
	public static final String SEQ_MISS = "sequenceMiss";

	/** The Constant JOB_FAILED_MISSING_SEQ. */
	public static final String JOB_FAILED_MISSING_SEQ = "Job Failed - File with a missing sequence number";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_MISSING_FILE = "Job Failed - Missing File";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_INVALID_FILE = "Job Failed - XML - XSD Validation Errors";

	/** The Constant JOB_FAILED_FILE_CORRUPT. */
	public static final String JOB_FAILED_FILE_CORRUPT = "Job Failed - Input File is Corrupt";

	/** The Constant FILE_INVALID. */
	public static final String FILE_INVALID = "fileInvalid";

	/** The Constant JOB_SUCCESS. */
	public static final String JOB_SUCCESS = "File has been successfully processed";

	/** The Constant BUT_WITH_ERRORS. */
	public static final String BUT_WITH_ERRORS = " but with some errors. Please find mail attachment for the errors.";

	/** The Constant PROCESS_FILE_SUCCESS. */
	public static final String PROCESS_FILE_SUCCESS = "File has been successfully processed";

	/** The Constant FILE_PROCESSES_WITH_ERRORS. */
	public static final String FILE_PROCESSED_WITH_ERRORS = "File has been successfully processed, but with some errors. Please find mail attachment for the errors.";

	/** The Constant NUMBER_TWO. */
	public static final int NUMBER_TWO = 2;

	/** The Constant INVALID_FILE. */
	public static final String INVALID_FILE = "Input File is Corrupt.";

	/** The Constant SUB_DATE_FORMAT. */
	public static final String SUB_DATE_FORMAT = "MMddyyyy-HH:mm:ss";

	/** The Constant PERMISSION_ISSUE. */
	public static final String PERMISSION_ISSUE = "Permission issue";

	/** The Constant FOLDER_NOT_EXISTS. */
	public static final String FOLDER_NOT_EXISTS = "Folder not exists";

	/** The Constant JOB_FAILED_PERMISSION_ISSUE_1. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_1 = "Job Failed - Folder ";

	/** The Constant JOB_FAILED_PERMISSION_ISSUE_2. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_2 = " permissions issues";

	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_1. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_1 = "Job Failed - Directory/folder ";

	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_2. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_2 = " structure not available";

	/** The Constant DOUBLE_BACKWARDSLASH_UNDERSCORE. */
	public static final String DOUBLE_BACKWARDSLASH_UNDERSCORE = "\\_";;

	/** The Constant ONE. */
	public static final int ONE = 1;

	/** The property to hold PARSE_EXCEPTION. */
	public static final String PARSE_EXCEPTION = "parsing exception :";

	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "yyyy-MM-ddhhmmss";

	/** The Constant DOLLAR_ONLY. */
	public static final String DOLLAR_ONLY = "~";

	/** The Constant DOWNLOAD_SUCCESS. */
	public static final String DOWNLOAD_SUCCESS = "FILES DOWNLOAD_SUCCESS ";

	/** The Constant NO_FILES_TO_DOWNLOAD. */
	public static final String NO_FILES_TO_DOWNLOAD = "NO_FILES_TO_DOWNLOAD";

	/** The Constant FILE_DOWNLOAD_FAILED. */
	public static final String FILE_DOWNLOAD_FAILED = "File Download Failed...";

	/** The Constant FILE_NOT_FOUND. */
	public static final String FILE_NOT_FOUND = "FILE NOT FOUND";

	/** The Constant FILE_NOT_FOUND_EXCEPTION. */
	public static final String FILE_NOT_FOUND_EXCEPTION = "FileNotFoundException ";

	/** The Constant IOEXCEPTION. */
	public static final String IOEXCEPTION = "IOException";

	/** The Constant FEED_FILE_NAME. */
	public static final String FEED_FILE_NAME = "FeedFileName";

	/** The Constant SLASH. */
	public static final String SLASH = "/";

	/** The Constant SEO_ENTITY_NAME. */
	public static final String SEO_PRODUCT_ENTITY_NAME = "sku";

	/** The Constant SEO_CATEGORY_ENTITY_NAME. */
	public static final String SEO_CATEGORY_ENTITY_NAME = "category";

	/** The Constant SEO_FEED_CONFIG_PATH. */
	public final static String SEO_FEED_CONFIG_PATH = "classpath*:seoFeedConfig.xml";

	/** The Constant SEO_JOB_NAME. */
	public final static String SEO_JOB_NAME = "seoFeed";

	/** The Constant SEO_FEED_DATA_VALIDATION_STEP. */
	public static final String SEO_FEED_DATA_VALIDATION_STEP = "seoFeedDataValidationStep";
	
	/** The Constant SEO_FEED_DATA_VALIDATION_STEP. */
	public static final String SEO_FEED_DATA_LOADER_STEP = "seoFeedLoadDataStep";

	/** The Constant TEMPLATE_PARAMETER_MESSAGE_CONTENT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_CONTENT = "messageContent";

	/** The Constant TEMPLATE_PARAMETER_MESSAGE_SUBJECT. */
	public static final String TEMPLATE_PARAMETER_MESSAGE_SUBJECT = "messageSubject";

	/** The Constant TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT = "totalRecordCount";

	/** The Constant TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT = "totalFailedRecordCount";

	/** The Constant TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT. */
	public static final String TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT = "successRecordCount";

	/** The Constant TEMPLATE_PARAMETER. */
	public static final String TEMPLATE_PARAMETER = "TEMPLATE PARAMETER";

	/** The Constant NUMBER_THREE. */
	public static final int NUMBER_THREE = 3;

	/** The Constant PARSED_SUCCESS. */
	public static final String PARSED_SUCCESS = "FILE PARSED SUCCESSFULLY ";

	/** The Constant PARSED_FEED_FILE. */
	public static final String PARSED_FEED_FILE = "PARSED FEED FILE";

	/** The Constant FEED_FILE_TAIL_RECORD_KEY. */
	public static final String FEED_FILE_TAIL_RECORD_KEY = "TRAILER";

	/** The Constant LONG_NUMBER_ZERO. */
	public static final Long LONG_NUMBER_ZERO = 0L;

	/** The Constant NUMBER_FORMAT_EXCEPTION. */
	public static final String NUMBER_FORMAT_EXCEPTION = "NumberFormatException";

	/** The Constant BAD_FILE. */
	public static final String BAD_FILE = "Bad File";

	/** The Constant ARRAYINDEXOUTOFBOUNDS_EXCEPTION. */
	public static final String ARRAYINDEXOUTOFBOUNDS_EXCEPTION = "ArrayIndexOutOfBounds Exception";

	/** The Constant PARSED_VALIDATED. */
	public static final String PARSED_VALIDATED = "FILE PARSED VALIDATED ";

	/** The Constant SUCCESS_RECORD. */
	public static final String SUCCESS_RECORD = "SUCCESS RECORD ";

	/** The Constant REPOSITORY_EXCEPTION. */
	public static final String REPOSITORY_EXCEPTION = "There is an error while updating the data in DB";

	/** The Constant SKU_NOT_FOUND. */
	public static final String SKU_NOT_FOUND = "SKU not found in the DB";

	/** The Constant FEED_COMPLETE. */
	public static final String FEED_COMPLETE = "FEED COMPLETED ";

	/** The Constant FEED_ATTACHMENT_DATE_FORMAT. */
	public static final String FEED_ATTACHMENT_DATE_FORMAT = "yyyy-MM-dd-HH.mm.sss";

	/** The Constant NUMBER_FOUR. */
	public static final int NUMBER_FOUR = 4;

	/** The Constant DOT. */
	public static final String DOT = ".";

	/** The Constant SEO_TAG_OVERRIDE_ITEM_DESC. */
	public static final String SEO_TAG_OVERRIDE_ITEM_DESC = "seoTagOverride";

	/** The Constant PRIORITY. */
	public static final String PRIORITY = "Priority 2";

	/** The Constant SEO_FEED_ADD_UPDATE_STEP. */
	public static final String SEO_FEED_ADD_UPDATE_STEP = "seoFeedAddUpdateStep";

	/** The Constant for CATEGORY. */
	public static final String CATEGORY = "category";

	/** The Constant FEED_FILE_TYPE. */
	public static final String FEED_FILE_TYPE = "feedFileType";

	/** The Constant EQUALS. */
	public static final String EQUALS = "=";

	/** The Constant PRODUCT. */
	public static final String PRODUCT = "product";
	
	/** The Constant PRODUCT. */
	public static final String SKU = "sku";

	/** The Constant DATA_LOADER_CLASS_FOR_SKU. */
	public static final String DATA_LOADER_CLASS_FOR_SKU = "dataLoaderClassForSku";

	/** The Constant DATA_LOADER_CLASS_FOR_CATEGORY. */
	public static final String DATA_LOADER_CLASS_FOR_CATEGORY = "dataLoaderClassForCategory";

	/** The Constant CLASSIFICATION. */
	public static final String CLASSIFICATION = "classification";
	
	/** The Constant CATALOG_REPOSITORY. */
	public static final String CATALOG_REPOSITORY = "catalogRepository";
	
	/** The Constant INVALID_RECORD_SKU. */
	public static final String INVALID_RECORD_SKU = "Invalid Record: There is no sku for the given online pid";
	
	/** The Constant INVALID_RECORD_CATEGORY. */
	public static final String INVALID_RECORD_CATEGORY =  "Invalid Record: There is no category with the given id";
	
	/** The Constant PRODUCT_FILE_NAME. */
	public static final String PRODUCT_FILE_NAME = "productFileName";
	
	/** The Constant PRE_PROCESSING. */
	public static final String PRE_PROCESSING = "PreProcessing";
	
	/** The Constant FILE_DOWNLOAD. */
	public static final String FILE_DOWNLOAD = "File Download";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant TIME_TAKEN. */
	public static final String TIME_TAKEN = "Time Taken";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant START_DATE. */
	public static final String START_DATE = "Start Date";

	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant REPOSITORY_EXCEPTION_MSG. */
	public static final String REPOSITORY_EXCEPTION_MSG = "Repository Exception";
	
	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = " ";
	
	/** The Constant INVALID_RECORD_MAX_SIZE. */
	public static final String INVALID_RECORD_MAX_SIZE = "Invalid Record: Repository Exception";
	
	/** The Constant FILES_MOVED_SUCCESS. */
	public static final String FILES_MOVED_SUCCESS = "File Moved successfully";
	
	/** The Constant NO_MATCH_FIELS. */
	public static final String NO_MATCH_FIELS = "No Match Files";
	
	/** The Constant FILE_VALID. */
	public static final String FILE_VALID = "File is valid";
	
	/** The Constant FILE_NO_SEQ. */
	public static final String FILE_NO_SEQ = "File is not in sequence with last processed";
	
	/** The Constant FILE_PARSING. */
	public static final String FILE_PARSING = "File Parsing";
	
	/** The Constant FILE_VALIDATION. */
	public static final String FILE_VALIDATION = "File Validation";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";
	
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROCESS = "Feed Process";
	
	/** The Constant FILES_WRITING. */
	public static final String FILES_WRITING = "File Writing";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "Feed Status";
	
	/** The Constant FEED_FAILED. */
	public static final String FEED_FAILED = "Feed Failed";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_COMPLETED = "Feed completed";
	
	/** The Constant EXCEPTION. */
	public static final String EXCEPTION = "Exception";
}
