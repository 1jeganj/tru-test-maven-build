package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.seo.TRUSeoFeedConfig;
import com.tru.feedprocessor.tol.seo.exception.TRUSeoFeedException;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUSeoFeedFileDownloadTask.
 */
public class TRUSeoFeedFileDownloadTask extends FileDownloadTask {

	/**
	 * Property to hold destinationDirectory Path.
	 */
	private String mProcessingDir;
	/**
	 * Property to hold SourceDirectory Path.
	 */
	private String mSourceDir;

	/** The Un processed dir. */
	private String mUnProcessedDir;

	/** The Logger. */
	private FeedLogger mLogger;

	/** The Seo feed configuration. */
	private TRUSeoFeedConfig mSeoFeedConfiguration;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;

	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * Do process.
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws Exception
	 *             the exception
	 */

	@Override
	protected void doExecute() throws FeedSkippableException, Exception {

		getLogger().vlogDebug("Start @Class: TRUSeoFeedFileDownloadTask, @method: doProcess()");
		mSourceDir = getSeoFeedConfiguration().getSourceDirectory();
		mProcessingDir = getSeoFeedConfiguration().getProcessingDirectory();
		mUnProcessedDir = getSeoFeedConfiguration().getUnProcessDirectory();
		String startDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		String fileExtension = getSeoFeedConfiguration().getFileType();
		String fileType = (String) getJobParameter(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String filePattern = getSeoFeedConfiguration().getFileName();
		if (StringUtils.isNotBlank(fileType) && fileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.PRODUCT)) {
			filePattern = getSeoFeedConfiguration().getProductFileName();
		} 
		File sharedFolderFile = new File(mSourceDir);
		if (sharedFolderFile.exists() && sharedFolderFile.isDirectory() && sharedFolderFile.canRead()) {
			moveSrcToProcessingDir(mSourceDir, mProcessingDir, fileExtension, filePattern, new HashMap<String, Object>(), startDate);
		} else if (!sharedFolderFile.exists() || !sharedFolderFile.isDirectory()) {
			String messageSubject = TRUSeoFeedReferenceConstants.FOLDER_NOT_EXISTS;
			logFailedMessage(startDate, TRUSeoFeedReferenceConstants.FOLDER_NOT_EXISTS);
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUSeoFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if (!sharedFolderFile.canRead()) {
			String messageSubject = TRUSeoFeedReferenceConstants.PERMISSION_ISSUE;
			logFailedMessage(startDate, TRUSeoFeedReferenceConstants.PERMISSION_ISSUE);
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUSeoFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		}
		getLogger().vlogDebug("End @Class: TRUSeoFeedFileDownloadTask, @method: doProcess()");

	}

	
	/**
	 * Move src to processing dir.
	 *
	 * @param pSourceDir the source dir
	 * @param pProcessingDir the processing dir
	 * @param pFiletype the filetype
	 * @param pFilePattern the file pattern
	 * @param pServiceMap the service map
	 * @param pStartDate the start date
	 * @return true, if successful
	 * @throws TRUSeoFeedException the TRU seo feed exception
	 */
	public boolean moveSrcToProcessingDir(String pSourceDir, String pProcessingDir, String pFiletype, String pFilePattern, Map<String, Object> pServiceMap, String pStartDate)
			throws TRUSeoFeedException {
		getLogger().vlogDebug("Start @Class: TRUSeoFeedFileDownloadTask, @method: moveSrcToProcessingDir()");
		boolean retVal = Boolean.FALSE;
		File sourceFolder = new File(pSourceDir);
		File processingFolder = new File(pProcessingDir);
		List<File> fileLists = null;
		String[] feedFileNames = null;
		StringBuffer filesList = new StringBuffer();
		filesList.setLength(0);
		String timestamp = null;
		String name = null;
		String fileName = null;
		int fileCount = 0;
		List<Date> fileDateList = new ArrayList<Date>();
		Map<String, Object> serviceMap = pServiceMap;
		Date date = null;
		boolean sendFileDoesNotExistMail = false;

		if (sourceFolder != null && sourceFolder.exists() && sourceFolder.canRead()) {
			fileLists = fileLists(pSourceDir, pFilePattern);
			if (!fileLists.isEmpty()) {
				for (File srcFile : fileLists) {
					name = srcFile.getName();
					feedFileNames = name.split(TRUSeoFeedReferenceConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
					timestamp = feedFileNames[TRUSeoFeedReferenceConstants.ONE];
					Format feedFormatter = new SimpleDateFormat(TRUSeoFeedReferenceConstants.DATE_FORMAT, Locale.getDefault());
					Format actuallDateFormat = new SimpleDateFormat(TRUSeoFeedReferenceConstants.DATE_FORMAT, Locale.getDefault());
					try {
						date = (Date) ((DateFormat) feedFormatter).parse(timestamp);
					} catch (java.text.ParseException pe) {
						getLogger().logError(TRUSeoFeedReferenceConstants.SPACE + TRUSeoFeedReferenceConstants.PARSE_EXCEPTION);
						logFailedMessage(pStartDate, pe.getMessage());
						throw new TRUSeoFeedException(TRUSeoFeedReferenceConstants.PARSE_EXCEPTION, pe);
					}
					String feedName = actuallDateFormat.format(date);
					String todayDate = actuallDateFormat.format(new Date());
					if (todayDate.equalsIgnoreCase(feedName)) {
						if (fileCount == 0) {
							fileDateList.add(date);
							fileName = name;
						} else if (fileCount > 0 && date.after(fileDateList.get(fileDateList.size() - TRUSeoFeedReferenceConstants.ONE))) {
							fileDateList.add(date);
							fileName = name;
						}
						fileCount++;
					}
				}
				fileDateList.clear();
				if (fileName != null) {
					String fileType = (String) getJobParameter(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
					if (fileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.PRODUCT)) {
						setConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE, TRUSeoFeedReferenceConstants.PRODUCT);
					} else if (fileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.CATEGORY)) {
						setConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE, TRUSeoFeedReferenceConstants.CATEGORY);
					}
					retVal = movefiles(processingFolder, sourceFolder, fileName, serviceMap, pStartDate);
				} else {
					sendFileDoesNotExistMail = true;
				}
			} else {
				sendFileDoesNotExistMail = true;
			}
		}

		if (sendFileDoesNotExistMail) {
			notifyFileDoseNotExits(mSourceDir, getFeedEnvConfiguration().getSeoFeedFailureSub());
			logFailedMessage(pStartDate, TRUSeoFeedReferenceConstants.FILE_DOWNLOAD_FAILED);
			throw new TRUSeoFeedException(TRUSeoFeedReferenceConstants.FILE_DOWNLOAD_FAILED);

		}

		getLogger().vlogDebug("End @Class: TRUSeoFeedFileDownloadTask, @method: moveSrcToProcessingDir()");

		return retVal;
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUSeoFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUSeoFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUSeoFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUSeoFeedReferenceConstants.SEO_FEED, TRUSeoFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}

	
	/**
	 * Movefiles.
	 *
	 * @param pProcessingFolder the processing folder
	 * @param pSourceFolder the source folder
	 * @param pFileName the file name
	 * @param pServiceMap the service map
	 * @param pStartDate the start date
	 * @return true, if successful
	 * @throws TRUSeoFeedException the TRU seo feed exception
	 */
	private boolean movefiles(File pProcessingFolder, File pSourceFolder, String pFileName, Map<String, Object> pServiceMap, String pStartDate) throws TRUSeoFeedException {
		getLogger().vlogDebug("Start @Class: TRUSeoFeedFileDownloadTask, @method: movefiles()");
		boolean feedfilesExists = Boolean.FALSE;
		if (pProcessingFolder != null && pProcessingFolder.exists() && pProcessingFolder.canWrite()) {
			try {
				feedfilesExists = moveAllFiles(pSourceFolder, pProcessingFolder.getCanonicalPath(), pFileName, pServiceMap);
				if (feedfilesExists) {
					getLogger().logInfo(TRUSeoFeedReferenceConstants.SPACE + TRUSeoFeedReferenceConstants.DOWNLOAD_SUCCESS);
				} else {

					getLogger().logError(TRUSeoFeedReferenceConstants.SPACE + TRUSeoFeedReferenceConstants.NO_FILES_TO_DOWNLOAD);
					logFailedMessage(pStartDate, TRUSeoFeedReferenceConstants.FILE_DOWNLOAD_FAILED);
					throw new TRUSeoFeedException(TRUSeoFeedReferenceConstants.FILE_DOWNLOAD_FAILED);
				}
			} catch (FileNotFoundException e) {
				if (isLoggingError()) {
					logError(TRUSeoFeedReferenceConstants.FILE_NOT_FOUND_EXCEPTION, e);
				}
				logFailedMessage(pStartDate, e.getMessage());
			} catch (IOException e) {
				if (isLoggingError()) {
					logError(TRUSeoFeedReferenceConstants.IOEXCEPTION, e);
				}
				logFailedMessage(pStartDate, e.getMessage());
			}
		}
		getLogger().vlogDebug("End @Class: TRUSeoFeedFileDownloadTask, @method: movefiles()");
		return feedfilesExists;

	}

	/**
	 * Move all files.
	 * 
	 * @param pSharedDir
	 *            the shared dir
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pFileName
	 *            the files list
	 * @param pServiceMap
	 *            the service map
	 * @return true, if successful
	 * @throws TRUSeoFeedException
	 *             the TRU seo feed exception
	 */
	public boolean moveAllFiles(File pSharedDir, String pProcessingDir, String pFileName, Map<String, Object> pServiceMap) throws TRUSeoFeedException {
		Map<String, Object> serviceMap = pServiceMap;
		boolean fileMove = Boolean.FALSE;
		if (pSharedDir.exists() && pSharedDir.isDirectory() && !StringUtils.isBlank(pFileName)) {
			serviceMap.put(TRUSeoFeedReferenceConstants.FEED_FILE_NAME, pFileName);
			fileMove = moveFileToFolder(pSharedDir + TRUSeoFeedReferenceConstants.SLASH + pFileName, pProcessingDir);
			setConfigValue(FeedConstants.FILENAME, pFileName);
			setConfigValue(FeedConstants.FILEPATH, mProcessingDir);
			setConfigValue(FeedConstants.FILETYPE, getExtenstion(pFileName));
			moveRemainingFilesToUnprocessedFolder(pSharedDir, serviceMap, mUnProcessedDir);
		}
		return fileMove;
	}

	/**
	 * This method returns the file extension.
	 * 
	 * @param pName
	 *            the name
	 * @return extension
	 */
	public String getExtenstion(String pName) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedFileDownloadTask : @Method: getExtenstion()");
		String[] str = pName.split(TRUSeoFeedReferenceConstants.DOUBLE_SLASH_DOT);
		if (str.length > FeedConstants.NUMBER_ONE) {
			return str[str.length - FeedConstants.NUMBER_ONE];
		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedFileDownloadTask : @Method: getExtenstion()");
		return null;
	}

	/**
	 * Move remaining files to unprocessed folder.
	 * 
	 * @param pFromDir
	 *            the from dir
	 * @param pServiceMap
	 *            the service map
	 * @param pToDir
	 *            the to dir
	 */
	public void moveRemainingFilesToUnprocessedFolder(File pFromDir, Map<String, Object> pServiceMap, String pToDir) {
		File[] filesInToFolder = pFromDir.listFiles();
		if (filesInToFolder.length > TRUSeoFeedReferenceConstants.NUMBER_ZERO) {
			List<String> filesMovedToUnprocessedFolder = new ArrayList<String>();
			for (File file : filesInToFolder) {
				String fileType = (String) getJobParameter(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
				if (StringUtils.isNotBlank(fileType) && file.getPath().contains(fileType)) {
					filesMovedToUnprocessedFolder.add(file.getName());
					File moveToFolder = new File(pToDir);
					if (!(moveToFolder.exists()) && !(moveToFolder.isDirectory())) {
						moveToFolder.mkdirs();
					}
					file.renameTo(new File(moveToFolder, file.getName()));
				}
			}
		}
	}

	/**
	 * This method is used for moving the file from source directory to
	 * destination directory.
	 * 
	 * @param pFromFile
	 *            - String
	 * @param pToFolder
	 *            - String
	 * @return boolean - value
	 * @throws TRUSeoFeedException
	 *             the TRU seo feed exception
	 */
	public boolean moveFileToFolder(String pFromFile, String pToFolder) throws TRUSeoFeedException {
		File fromFile = new File(pFromFile);
		File toFolder = new File(pToFolder);
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		File[] filesInToFolder = toFolder.listFiles();
		for (File file : filesInToFolder) {
			File moveToFolder = new File(mUnProcessedDir);
			if (!(moveToFolder.exists()) && !(moveToFolder.isDirectory())) {
				moveToFolder.mkdirs();
			}
			file.renameTo(new File(moveToFolder, file.getName()));
		}
		boolean value = fromFile.renameTo(new File(toFolder, fromFile.getName()));
		return value;
	}

	/**
	 * File lists.
	 * 
	 * @param pPath
	 *            - pPath
	 * @param pPattern
	 *            - pPattern
	 * @return the patternMatchfiles
	 */
	private List<File> fileLists(String pPath, String pPattern) {
		List<File> patternMatchingfiles = new ArrayList<File>();
		File dir = new File(pPath);
		getLogger().vlogDebug(pPattern);

		File[] listFiles = dir.listFiles();
		Pattern p = Pattern.compile(pPattern);
		for (int i = 0; i < listFiles.length; i++) {
			Matcher matcher = p.matcher(listFiles[i].getName());
			if (matcher.matches()) {
				getLogger().vlogDebug(listFiles[i].getName());
				patternMatchingfiles.add(listFiles[i]);
			}
		}
		return patternMatchingfiles;
	}

	/**
	 * Notify file dose not exits.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 * @param pMessageSubject
	 *            the message subject
	 */
	public void notifyFileDoseNotExits(String pSourceDir, String pMessageSubject) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedFileDownloadTask : @Method: notifyFileDoseNotExits()");

		Map<String, Object> templateParameters = new HashMap<String, Object>();

		String priority = getFeedEnvConfiguration().getFailureMailPriority2();
		String content = null;
		String detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		if (pMessageSubject.contains(TRUSeoFeedReferenceConstants.FOLDER_NOT_EXISTS)) {
			content = TRUSeoFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_1 + pSourceDir + TRUSeoFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
			detailException = pSourceDir + TRUSeoFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
		} else if (pMessageSubject.contains(TRUSeoFeedReferenceConstants.PERMISSION_ISSUE)) {
			content = TRUSeoFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_1 + pSourceDir + TRUSeoFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
			detailException = pSourceDir + TRUSeoFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
		}

		String sub = getFeedEnvConfiguration().getSeoFeedFailureSub();
		String feedFileType = (String) getJobParameter(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String[] args = constructMessage(feedFileType);
		String formattedSubject = String.format(sub, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(TRUSeoFeedReferenceConstants.SEO_JOB_NAME, templateParameters);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedFileDownloadTask : @Method: notifyFileDoseNotExits()");
	}
	
	/**
	 * Construct message.
	 *
	 * @param feedFileType the feed file type
	 * @return the string[]
	 */
	private String[] constructMessage(String feedFileType) {
		String[] args = new String[TRUSeoFeedReferenceConstants.NUMBER_THREE];
		args[TRUSeoFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		if(StringUtils.isNotEmpty(feedFileType) && feedFileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.CATEGORY)) {
			args[TRUSeoFeedReferenceConstants.NUMBER_ONE] = TRUSeoFeedReferenceConstants.CATEGORY;
		} else {
			args[TRUSeoFeedReferenceConstants.NUMBER_ONE] = TRUSeoFeedReferenceConstants.PRODUCT;
		}
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUSeoFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUSeoFeedReferenceConstants.NUMBER_TWO] = dateFormat.format(currentDate);
		return args;
	}

	/**
	 * Gets the seo feed configuration.
	 * 
	 * @return the seo feed configuration
	 */
	public TRUSeoFeedConfig getSeoFeedConfiguration() {
		return mSeoFeedConfiguration;
	}

	/**
	 * Sets the seo feed configuration.
	 * 
	 * @param pSeoFeedConfiguration
	 *            the new seo feed configuration
	 */
	public void setSeoFeedConfiguration(TRUSeoFeedConfig pSeoFeedConfiguration) {
		this.mSeoFeedConfiguration = pSeoFeedConfiguration;
	}

	/**
	 * Gets the feed env configuration.
	 * 
	 * @return the feed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the feed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		this.mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		this.mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the logger to set
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
}
