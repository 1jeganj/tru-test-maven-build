package com.tru.feedprocessor.tol.base.tasklet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUSeoFeedExecutionDataCollectorTasklet is used to send email
 * notifications and moved the files to configured folders.
 * 
 */
public class TRUSeoFeedExecutionDataCollectorTasklet extends FeedExecutionDataCollectorTasklet {

	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * This method is overridden to check the Exceptions and notify the
	 * success/failure email .
	 * 
	 * @param pStepContribution
	 *            the step contribution
	 * @param pChunkContext
	 *            the chunk context
	 * @return the repeat status
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public RepeatStatus execute(StepContribution pStepContribution, ChunkContext pChunkContext) throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: execute()");
		super.execute(pStepContribution, pChunkContext);
		boolean isException = Boolean.FALSE;
		boolean isFileInvalid = Boolean.FALSE;
		boolean isFileNotExits = Boolean.FALSE;
		String processingDir = getConfigValue(TRUSeoFeedReferenceConstants.PROCESSING_DIRECTORY).toString();
		String badDir = getConfigValue(TRUSeoFeedReferenceConstants.BAD_DIRECTORY).toString();
		String successDir = getConfigValue(TRUSeoFeedReferenceConstants.SUCCESS_DIRECTORY).toString();
		String errorDir = getConfigValue(TRUSeoFeedReferenceConstants.ERROR_DIRECTORY).toString();
		String sourceDir = getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString();
		FeedExecutionContextVO feedExecutionContextVO = getCurrentFeedExecutionContext();
		String startDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		int totalRecordCount = 0;
		int successRecordCount = 0;

		List<StepInfoVO> stepInfoVOList = feedExecutionContextVO.getStepInfos();
		if (null != stepInfoVOList && !stepInfoVOList.isEmpty()) {
			for (StepInfoVO stepInfoVO : stepInfoVOList) {
				if (null != stepInfoVO && null != stepInfoVO.getFeedFailureProcessException()) {
					isException = Boolean.TRUE;
					if (stepInfoVO.getFeedFailureProcessException().getMessage().contains(FeedConstants.FILE_DOES_NOT_EXISTS)) {
						isFileNotExits = Boolean.TRUE;
						break;
					} else if (stepInfoVO.getStepName().equalsIgnoreCase(TRUSeoFeedReferenceConstants.SEO_FEED_DATA_VALIDATION_STEP)
							&& stepInfoVO.getFeedFailureProcessException().getMessage().contains(FeedConstants.INVALID_FILE)) {
						isFileInvalid = Boolean.TRUE;
						break; 
					} else if (stepInfoVO.getStepName().equalsIgnoreCase(TRUSeoFeedReferenceConstants.SEO_FEED_DATA_LOADER_STEP)
							&& stepInfoVO.getFeedFailureProcessException().getMessage().contains(FeedConstants.INVALID_FILE)) {
						isFileInvalid = Boolean.TRUE;
						break;
					}
				} else if (null != stepInfoVO && stepInfoVO.getStepName().equalsIgnoreCase(TRUSeoFeedReferenceConstants.SEO_FEED_ADD_UPDATE_STEP)) {
					totalRecordCount = stepInfoVO.getReadCount();
					getLogger().vlogDebug("Begin:@Class: TRUStoreFeedExecutionDataCollectorTasklet : totalRecordCount:" + totalRecordCount);
				}
			}
		}
		if (isFileInvalid) {
			moveInvalidFileToBadDir(feedExecutionContextVO.getFileName(), processingDir, badDir);
			logFailedMessage(startDate, TRUSeoFeedReferenceConstants.FEED_FAILED);
		}
		if (isFileNotExits) {
			notifyFileDoseNotExits(sourceDir);
			logFailedMessage(startDate, TRUSeoFeedReferenceConstants.FEED_FAILED);
		}
		List<FeedSkippableException> invalidRecords = null;
		if (!isException) {
			Map<String, List<FeedSkippableException>> allFeedSkippedException = feedExecutionContextVO.getAllFeedSkippedException();
			if (null != allFeedSkippedException) {
				invalidRecords = new ArrayList<FeedSkippableException>();
				for (Entry<String, List<FeedSkippableException>> entry : allFeedSkippedException.entrySet()) {
					List<FeedSkippableException> lFeedSkippableException = entry.getValue();
					for (FeedSkippableException lSkippedException : lFeedSkippableException) {
						if (null != lSkippedException && lSkippedException.getMessage().contains(TRUSeoFeedReferenceConstants.INVALID_RECORD)) { 
							invalidRecords.add(lSkippedException);
						}  else if (null != lSkippedException && lSkippedException.getMessage().contains(TRUSeoFeedReferenceConstants.INVALID_RECORD_MAX_SIZE)) {
							invalidRecords.add(lSkippedException);
						}
					}
				}
				if (null != invalidRecords && !invalidRecords.isEmpty()) {
					successRecordCount = totalRecordCount - (invalidRecords.size());
					moveInvalidRecordsToErrorDir(invalidRecords, feedExecutionContextVO.getFileName(), errorDir, totalRecordCount);
					moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir, totalRecordCount,
							successRecordCount, Boolean.FALSE);
					logSuccessMessage(startDate, TRUSeoFeedReferenceConstants.FEED_COMPLETED);
					isException = Boolean.TRUE;
				}
			}
		}

		if (!isException) {
			successRecordCount = totalRecordCount - (invalidRecords.size());
			moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir, totalRecordCount, successRecordCount,
					Boolean.TRUE);
			logSuccessMessage(startDate, TRUSeoFeedReferenceConstants.FEED_COMPLETED);
		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: execute()");
		return RepeatStatus.FINISHED;
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUSeoFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUSeoFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUSeoFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUSeoFeedReferenceConstants.SEO_FEED, TRUSeoFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUSeoFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUSeoFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUSeoFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUSeoFeedReferenceConstants.SEO_FEED, TRUSeoFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}

	/**
	 * Notify file dose not exits. This method used to notify the configured
	 * users if file not exists.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 */
	public void notifyFileDoseNotExits(String pSourceDir) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUSeoFeedReferenceConstants.JOB_FAILED_MISSING_FILE;
		String priority = TRUSeoFeedReferenceConstants.SEQ_MISS;
		String subject = getFeedEnvConfiguration().getSeoFeedFailureSub();
		String feedFileType = (String) getCurrentFeedExecutionContext().getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String[] args = constructMessage(feedFileType);
		
		String formattedSubject = String.format(subject, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, getFeedEnvConfiguration().getDetailedExceptionForNoFeed());
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(TRUSeoFeedReferenceConstants.SEO_JOB_NAME, templateParameters);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
	}

	/**
	 * Construct message.
	 *
	 * @param feedFileType the feed file type
	 * @return the string[]
	 */
	private String[] constructMessage(String feedFileType) {
		String[] args = new String[TRUSeoFeedReferenceConstants.NUMBER_THREE];
		args[TRUSeoFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		if(StringUtils.isNotEmpty(feedFileType) && feedFileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.CATEGORY)) {
			args[TRUSeoFeedReferenceConstants.NUMBER_ONE] = TRUSeoFeedReferenceConstants.CATEGORY;
		} else {
			args[TRUSeoFeedReferenceConstants.NUMBER_ONE] = TRUSeoFeedReferenceConstants.PRODUCT;
		}
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUSeoFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUSeoFeedReferenceConstants.NUMBER_TWO] = dateFormat.format(currentDate);
		return args;
	}

	/**
	 * Move invalid file to bad dir. This method used to move the file bad
	 * directory.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pBadDir
	 *            the bad dir
	 */
	public void moveInvalidFileToBadDir(String pFileName, String pProcessingDir, String pBadDir) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pBadDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}
			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(pFileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
			}
		}
		triggerInvalidFeedEmail();
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
	}

	/**
	 * Trigger invalid feed email. This method used to trigger the invalid feed
	 * mail.
	 * 
	 * @param pBadDir
	 *            the bad dir
	 */
	public void triggerInvalidFeedEmail() {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();

		String priority = TRUSeoFeedReferenceConstants.FILE_INVALID;
		String content = TRUSeoFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT;
		String detailException = TRUSeoFeedReferenceConstants.INVALID_FILE;
		String sub = getFeedEnvConfiguration().getSeoFeedFailureSub();
		String feedFileType = (String) getCurrentFeedExecutionContext().getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String[] args = constructMessage(feedFileType);
		String formattedSubject = String.format(sub, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(TRUSeoFeedReferenceConstants.SEO_JOB_NAME, templateParameters);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
	}

	/**
	 * Move invalid records to error dir. This method used to move the invalid
	 * records to the error directory.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pFeedFileName
	 *            the file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pTotalRecordCount
	 *            the total record count
	 */
	public void moveInvalidRecordsToErrorDir(List<FeedSkippableException> pInvalidRecords, String pFeedFileName, String pErrorDir,
			int pTotalRecordCount) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
		File sourceFolder = new File(pErrorDir);
		Writer writer = null;
		BufferedWriter bw = null;
		if (!sourceFolder.exists() && !sourceFolder.isDirectory()) {
			sourceFolder.mkdirs();
		}

		File invalidRecordsFile = new File(sourceFolder, FeedConstants.ERROR + FeedConstants.UNDER_SCORE + pFeedFileName);
		try {
			writer = new FileWriter(invalidRecordsFile);
			bw = new BufferedWriter(writer);
			for (FeedSkippableException entry : pInvalidRecords) {
				bw.write(entry.getFeedItemVO().toString() + FeedConstants.IFUN + entry.getMessage());
				bw.newLine();
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				logError("Error in : @class TRUSeoFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()", e);
			}
		} finally {
			try {
				if (null != bw) {
					bw.close();
				}
				if (null != writer) {
					writer.close();
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError("Error in : @class TRUSeoFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()", e);
				}
			}
		}
		String priority = TRUSeoFeedReferenceConstants.PRIORITY;
		getLogger().vlogDebug("priority:", priority);
		triggerFeedProcessedWithErrorsEmail(pInvalidRecords, invalidRecordsFile.getName(), pErrorDir, priority, pTotalRecordCount, pFeedFileName);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
	}

	/**
	 * Trigger feed processed with errors email. This method used to trigger the
	 * mail with error records.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pPriority
	 *            the priority
	 * @param pTotalRecordCount
	 *            the total record count
	 * @param pFeedFileName
	 *            the feed file name
	 */
	public void triggerFeedProcessedWithErrorsEmail(List<FeedSkippableException> pInvalidRecords, String pErrorFileName, String pErrorDir,
			String pPriority, int pTotalRecordCount, String pFeedFileName) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		File[] attachmentFile = new File[FeedConstants.NUMBER_ONE];
		attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pInvalidRecords, pErrorFileName, pErrorDir, pFeedFileName);
		int successCount = 0;
		if (pInvalidRecords != null) {
			successCount = pTotalRecordCount - (pInvalidRecords.size());
		}
		String content = TRUSeoFeedReferenceConstants.FILE_PROCESSED_WITH_ERRORS;
		String subject = getFeedEnvConfiguration().getSeoFeedErrorSub();
		String feedFileType = (String) getCurrentFeedExecutionContext().getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String[] args = constructMessage(feedFileType);
		String formattedSubject = String.format(subject, args);

		String detailException = String.format(getFeedEnvConfiguration().getSeoFeedErrorDetailExcep(), args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pPriority);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT,
				(pInvalidRecords.size() / (TRUSeoFeedReferenceConstants.NUMBER_TWO)));
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, successCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());

		getFeedEmailService().sendFeedProcessedWithErrorsEmail(TRUSeoFeedReferenceConstants.SEO_JOB_NAME, templateParameters, attachmentFile);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");
	}

	/**
	 * Creates the attachment file. This method used to create attachment file.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pFeedFileName
	 *            the feed file name
	 * @return the file
	 */
	public File createAttachmentFile(List<FeedSkippableException> pInvalidRecords, String pErrorFileName, String pErrorDir,
			String pFeedFileName) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;

		File errorFolder = new File(pErrorDir);
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}
		File attachment = new File(errorFolder, attachementFileName(pErrorFileName));
		try {
			outputStream = new FileOutputStream(attachment);
			workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(TRUSeoFeedReferenceConstants.XLS_SHEET);
			int rowCount = FeedConstants.NUMBER_ZERO;
			HSSFRow rowhead = sheet.createRow(rowCount);
			HSSFCellStyle rowCellStyle = workbook.createCellStyle();
			rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			rowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getFeedEnvConfiguration().getFileName());
			rowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getFeedEnvConfiguration().getRecordNumber());
			rowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getFeedEnvConfiguration().getTimeStamp());
			rowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getFeedEnvConfiguration().getAttachmentItemIdLabel());
			rowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(getFeedEnvConfiguration().getAttachmentExceptionReasonLabel());

			rowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(rowCellStyle);

			rowCount = FeedConstants.NUMBER_ONE;
			HSSFCellStyle dataCellStyle = workbook.createCellStyle();
			dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			for (FeedSkippableException entry : pInvalidRecords) {
				HSSFRow dataRow = sheet.createRow(rowCount);
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(new HSSFRichTextString(pFeedFileName));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(
						new HSSFRichTextString(((TRUSeoFeedVO) entry.getFeedItemVO()).getId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(((TRUSeoFeedVO) entry.getFeedItemVO()).getErrorMessage()));

				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				rowCount++;
			}
			if (null != workbook) {
				workbook.write(outputStream);
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError("Error in : @class TRUSeoFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);
			}
		} catch (Exception ioe) {
			if (isLoggingError()) {
				logError("Error in : @class TRUSeoFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);
			}
		} finally {
			if (null != workbook) {
				workbook = null;
			}
			try {
				if (null != outputStream) {
					outputStream.close();
				}
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class TRUSeoFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);
				}
			}

		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		return attachment;
	}

	/**
	 * Attachement file name. This method used to construct the attached file
	 * name
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachementFileName(String pFileName) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		String newFileName = pFileName.substring(FeedConstants.NUMBER_ZERO, pFileName.indexOf(FeedConstants.DOT) + FeedConstants.NUMBER_ONE);
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = stringBuffer.append(newFileName).append(TRUSeoFeedReferenceConstants.XLS);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		return stringBuffer.toString();
	}

	/**
	 * Move success file to success dir.
	 * 
	 * This method used to move the files to success directory.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pSuccessDir
	 *            the success dir
	 * @param pTotalCount
	 *            the total count
	 * @param pSuccessRecordCount
	 *            the success record count
	 * @param pIsMail
	 *            the is mail
	 */
	public void moveSuccessFileToSuccessDir(String pFileName, String pProcessingDir, String pSuccessDir, int pTotalCount, int pSuccessRecordCount,
			boolean pIsMail) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pSuccessDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(pFileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
				if (pIsMail) {
					triggerFeedProcessedSuccessEmail(pTotalCount, pSuccessRecordCount);
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
	}

	/**
	 * Trigger feed processed successfully email.
	 * 
	 * @param pTotalCount
	 *            the total count
	 * @param pSuccessRecordCount
	 *            the success record count
	 */
	public void triggerFeedProcessedSuccessEmail(int pTotalCount, int pSuccessRecordCount) {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUSeoFeedReferenceConstants.JOB_SUCCESS;
		String subject = getFeedEnvConfiguration().getSeoFeedSuccessSub();
		String feedFileType = (String) getCurrentFeedExecutionContext().getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		String[] args = constructMessage(feedFileType);
		String formattedSubject = String.format(subject, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		getFeedEmailService().sendFeedSuccessEmail(TRUSeoFeedReferenceConstants.SEO_JOB_NAME, templateParameters);
		getLogger().vlogDebug("End:@Class: TRUSeoFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessEmail()");
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

}
