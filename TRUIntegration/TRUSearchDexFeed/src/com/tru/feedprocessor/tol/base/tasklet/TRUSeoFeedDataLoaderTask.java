package com.tru.feedprocessor.tol.base.tasklet;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.loader.IFeedDataLoader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class TRUSeoFeedDataLoaderTask is used to load the data.
 * 
 */
public class TRUSeoFeedDataLoaderTask extends FeedDataLoaderTask {

	/**
	 * Do execute.
	 * 
	 * @throws Exception
	 *             the exception
	 * @see com.tru.feedprocessor.tol.AbstractTasklet#doExecute()
	 */
	@Override
	protected void doExecute() throws Exception {
		String dataLoaderClassFQCN = null;
		String feedFileType = (String) getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE);
		if (StringUtils.isNotEmpty(feedFileType)) {
			if (feedFileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.PRODUCT)) {
				dataLoaderClassFQCN = (String) getConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_SKU);
			} else if (feedFileType.equalsIgnoreCase(TRUSeoFeedReferenceConstants.CATEGORY)) {
				dataLoaderClassFQCN = (String) getConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_CATEGORY);
			}
		}
		if (dataLoaderClassFQCN != null) {
			IFeedDataLoader fileParser = (IFeedDataLoader) Class.forName(dataLoaderClassFQCN).newInstance();
			setFeedDataLoader(fileParser);
		}
		loadData();

	}

	/**
	 * Parses the file.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Override
	protected void loadData() throws Exception {
		EntityDataProvider lEntityDataProvider = null;
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		curExecCntx.setConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE, getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_TYPE));
		Map<String, List<? extends BaseFeedProcessVO>> lEntityVOsMap = getFeedDataLoader().loadData(curExecCntx);
		lEntityDataProvider = new EntityDataProvider(lEntityVOsMap);
		saveEntityData(lEntityDataProvider);
	}

}
