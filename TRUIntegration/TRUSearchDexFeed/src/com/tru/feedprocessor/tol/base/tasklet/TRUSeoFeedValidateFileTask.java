package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUSeoFeedValidateFileTask is used for validating the input feed file.
 * @version 1.0
 * @author Professional Access
 */
public class TRUSeoFeedValidateFileTask extends ValidateFileTask {

	@Override
	public boolean validateFile(File pFile) {
		String startDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		logSuccessMessage(startDate, TRUSeoFeedReferenceConstants.FILE_VALID, curExecCntx.getFileName());
		return Boolean.TRUE;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage ,String pFileName) {
		String endDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUSeoFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUSeoFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUSeoFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUSeoFeedReferenceConstants.SEO_FEED, TRUSeoFeedReferenceConstants.FILE_VALIDATION, pFileName, extenstions);
	}

}
