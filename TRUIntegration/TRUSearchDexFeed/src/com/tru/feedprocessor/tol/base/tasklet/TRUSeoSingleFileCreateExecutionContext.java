package com.tru.feedprocessor.tol.base.tasklet;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class TRUSeoSingleFileCreateExecutionContext is used set the executionContext for feed file process.
 */
public class TRUSeoSingleFileCreateExecutionContext extends AbstractCreateFeedExecutionContext {

	/**
	 * method createExecutionContext() will create and set the required properties to executionContext vo for feed
	 * process.
	 *
	 * @return the queue
	 * @throws FeedSkippableException the feed skippable exception
	 */
	@Override
	protected Queue<FeedExecutionContextVO> createExecutionContext() throws FeedSkippableException {
		getLogger().vlogDebug(
				"Begin:@Class: TRUSeoSingleFileCreateExecutionContext : @Method: createExecutionContext()");
		Queue executionContextQueue = new LinkedBlockingQueue<FeedExecutionContextVO>();
		String processingFileName = null;
		String processingFilePath = null;
		FeedExecutionContextVO feedExecutionContextVO = new FeedExecutionContextVO();
		if (getFeedHelper().getConfigValue(FeedConstants.FILEPATH) != null && getFeedHelper().getConfigValue(FeedConstants.FILENAME) != null) {
			processingFilePath = (String) getFeedHelper().getConfigValue(FeedConstants.FILEPATH);
			processingFileName = (String) getFeedHelper().getConfigValue(FeedConstants.FILENAME);
			feedExecutionContextVO.setFileName(processingFileName);
			feedExecutionContextVO.setFilePath(processingFilePath);
			getLogger().vlogDebug("TRUSeoSingleFileCreateExecutionContext : processingFileName : {0}" , processingFileName);
			getLogger().vlogDebug("TRUSeoSingleFileCreateExecutionContext : processingFilePath : {0}" , processingFilePath);
		} else {
			feedExecutionContextVO.setFileName(null);
			feedExecutionContextVO.setFilePath(null);
			getLogger().vlogDebug("TRUSeoSingleFileCreateExecutionContext : processingFileName is Null:");
			getLogger().vlogDebug("TRUSeoSingleFileCreateExecutionContext : processingFilePath is Null :");
		}
		executionContextQueue.add(feedExecutionContextVO);
		getLogger().vlogDebug(
				"End:@Class: TRUSeoSingleFileCreateExecutionContext : @Method: createExecutionContext()");
		return executionContextQueue;
	}
}
