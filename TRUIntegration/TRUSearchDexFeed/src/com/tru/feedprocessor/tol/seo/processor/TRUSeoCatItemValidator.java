package com.tru.feedprocessor.tol.seo.processor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoItemValidator will validate the mandatory properties if
 * mandatory properties are empty or invalid then that record is skipped and
 * processed the next record.
 */

public class TRUSeoCatItemValidator implements IFeedItemValidator {

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * the method validate() will validate the mandatory properties if mandatory
	 * properties are empty or invalid then that record is skipped and processed
	 * the next record.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {
		TRUSeoFeedVO seoFeedVO = (TRUSeoFeedVO) pBaseFeedProcessVO;
		getLogger().vlogDebug("Begin:@Class: TRUSeoItemValidator : @Method: validate(): seo Id::" + seoFeedVO.getId());
		if (StringUtils.isBlank(seoFeedVO.getId())) {
			seoFeedVO.setErrorMessage(TRUSeoFeedReferenceConstants.REQUIRED_ID);
			throw new FeedSkippableException(null, null, TRUSeoFeedReferenceConstants.REQUIRED_ID, seoFeedVO, null);
		}

		getLogger().vlogDebug("End:@Class: TRUSeoItemValidator : @Method: validate()");
	}

	/**
	 * Checks if is match pattern.
	 * 
	 * @param pVal
	 *            the val
	 * @param pPattern
	 *            the pattern
	 * @return true, if is match pattern
	 */
	public boolean isPatternMatched(String pVal, String pPattern) {
		Pattern pattern = Pattern.compile(pPattern);
		Matcher matcher = pattern.matcher(pVal);
		if (matcher.matches()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}
}
