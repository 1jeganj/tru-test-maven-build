package com.tru.feedprocessor.tol.seo.processor;

import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceMap;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.FeedItemChangeDetectorProcessor;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoFeedItemChangeDetectorProcessor.
 * 
 * First this class will validate the data according to the respective data type
 * in feed vo, if the validation fails it will skip that record as error record
 * and process the next record.
 * 
 * Also checks whether the price item exists in the database or not based on
 * price id. If price item exists in data base then it will execute the next
 * step(inspector).
 * 
 * Inspector will inspect is there any change information in VO and repository.
 * and in <code>doProcess</code> default is <code>true</code> for
 * <code>isDataUpdated</code>
 * 
 */
public class TRUSeoFeedItemChangeDetectorProcessor extends FeedItemChangeDetectorProcessor {
	/**
	 * ItemValueChangedMapper.
	 */
	private ServiceMap mItemValueChangeInspector;

	/**
	 * The entity id map.
	 */
	private Map<String, Map<String, String>> mEntityIdMap;

	/**
	 * The identifier Store key.
	 */
	private String mIdentifierIdStoreKey;

	/**
	 * First this method will validate the data according to the respective data
	 * type in feed vo, if the validation fails it will skip that record as
	 * error record and process the next record.
	 * 
	 * Also checks whether the price item exists in the database or not based on
	 * price id. If price item exists in data base then it will execute the next
	 * step(inspector).
	 * 
	 * Inspector will inspect is there any change information in VO and
	 * repository. and in <code>doProcess</code> default is <code>true</code>
	 * for <code>isDataUpdated</code>
	 * 
	 * If <code>isDataUpdated</code> is false it will not do any thing in
	 * repository level during writing the data. if <code>isDataUpdated</code>
	 * is true it will take effect for add/update in repository during writing
	 * the data.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @return the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess()");
		TRUSeoFeedVO seoVo = (TRUSeoFeedVO) pFeedVO;
		IFeedItemValidator lFeedValidator = null;
		if (getValidators() != null && !getValidators().isEmpty()) {
			lFeedValidator = (IFeedItemValidator) getValidators().get(pFeedVO.getEntityName());
			try {
				lFeedValidator.validate(seoVo);
			} catch (FeedSkippableException e) {
				getLogger().vlogError("@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess()", e);
				seoVo.setErrorMessage(e.getMessage());
				throw new FeedSkippableException(getPhaseName(), getStepName(), e.getMessage(), pFeedVO, e);
			}
		}
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess() :: after validator call");
		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());

		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		IItemValueChangeInspector lItemValueChangeInspector = null;

		if (mEntityIdMap != null && !mEntityIdMap.isEmpty()) {
			boolean isDataUpdated = true;
			if (getItemValueChangeInspector() != null && !getItemValueChangeInspector().isEmpty()) {
				lItemValueChangeInspector = (IItemValueChangeInspector) getItemValueChangeInspector().get(pFeedVO.getEntityName());

				Map<String, String> entityMap = mEntityIdMap.get(seoVo.getEntityName());

				String repId = entityMap.get(seoVo.getId());
				if (seoVo.getEntityName().equals(TRUSeoFeedReferenceConstants.SKU)) {
					if (StringUtils.isNotBlank(repId)) {
						String[] skuIds = repId.split(FeedConstants.HASH);
						if (skuIds != null && skuIds.length > 0) {
							for (int i = 0; i < skuIds.length; i++) {
								RepositoryItem skuItem = null;
								if (StringUtils.isNotBlank(skuIds[i])) {
									skuItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(skuIds[i], TRUSeoFeedReferenceConstants.SKU);
									if (lItemValueChangeInspector != null && skuItem != null) {
										isDataUpdated = lItemValueChangeInspector.isUpdated(seoVo, skuItem);
										if (isDataUpdated) {
											break;
										}
									}
								}
							}
						} else {
							seoVo.setErrorMessage(TRUSeoFeedReferenceConstants.INVALID_RECORD_SKU);
							throw new FeedSkippableException(null, null, TRUSeoFeedReferenceConstants.INVALID_RECORD_SKU, seoVo, null);
						}
					} else {
						seoVo.setErrorMessage(TRUSeoFeedReferenceConstants.INVALID_RECORD_SKU);
						throw new FeedSkippableException(null, null, TRUSeoFeedReferenceConstants.INVALID_RECORD_SKU, seoVo, null);
					}

				} else if (seoVo.getEntityName().equals(TRUSeoFeedReferenceConstants.CATEGORY)) {
					if (StringUtils.isNotBlank(repId)) {
						MutableRepositoryItem categoryItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(repId,
								TRUSeoFeedReferenceConstants.CLASSIFICATION);
						if (lItemValueChangeInspector != null && categoryItem != null) {
							isDataUpdated = lItemValueChangeInspector.isUpdated(seoVo, categoryItem);
						}
					} else {
						seoVo.setErrorMessage(TRUSeoFeedReferenceConstants.INVALID_RECORD_CATEGORY);
						throw new FeedSkippableException(null, null, TRUSeoFeedReferenceConstants.INVALID_RECORD_CATEGORY, seoVo, null);
					}
				}
			}
			if (isDataUpdated) {
				getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess()" + isDataUpdated);
				return seoVo;
			} else {
				getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess()" + isDataUpdated);
				return null;
			}
		}

		getLogger().vlogDebug("End:@Class: TRUSeoFeedItemChangeDetectorProcessor : @Method: doProcess()");
		return seoVo;
	}

	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifierIdStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the identifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Gets the item value change inspector.
	 * 
	 * @return the itemValueChangedMapper
	 */
	public ServiceMap getItemValueChangeInspector() {
		return mItemValueChangeInspector;
	}

	/**
	 * Sets the item value change inspector.
	 * 
	 * @param pItemValueChangeInspector
	 *            - pItemValueChangeInspector
	 */
	public void setItemValueChangeInspector(ServiceMap pItemValueChangeInspector) {
		mItemValueChangeInspector = pItemValueChangeInspector;
	}

}
