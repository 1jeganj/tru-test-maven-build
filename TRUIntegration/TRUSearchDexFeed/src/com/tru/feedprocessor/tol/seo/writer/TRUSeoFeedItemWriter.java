package com.tru.feedprocessor.tol.seo.writer;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.writer.AbstractFeedItemWriter;
import com.tru.feedprocessor.tol.seo.TRUSeoFeedRepositoryProperties;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUSeoFeedItemWriter is extended to implement customized logic for
 * respective seo feed.
 * 
 * Write the data into Repository by calling add based on the data in
 * mEntityIdMap.
 * 
 * 
 */
public class TRUSeoFeedItemWriter extends AbstractFeedItemWriter {

	/** The entity id map. */
	private Map<String, Map<String, String>> mEntityIdMap;

	/** The Seo feed repository properties. */
	private TRUSeoFeedRepositoryProperties mSeoFeedRepositoryProperties;

	/** The m identifier id store key. */
	private String mIdentifierIdStoreKey;

	/**
	 * this method is used to set the required values.
	 */
	@Override
	public void doOpen() {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemWriter : @Method: doOpen()");
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}

		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());

		getLogger().vlogDebug("End:@Class: TRUSeoFeedItemWriter : @Method: doOpen()");
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUSeoFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUSeoFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUSeoFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUSeoFeedReferenceConstants.SEO_FEED, TRUSeoFeedReferenceConstants.FILES_WRITING, null, extenstions);
	}

	/**
	 * Do write.
	 *
	 * @param pVOItems            the vO items
	 * @throws RepositoryException             the repository exception
	 * @throws FeedSkippableException             the feed skippable exception
	 * @see com.tru.feedprocessor.tol.catalog.writer.AbstractFeedItemWriter.
	 *      AbstractFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemWriter : @Method: doWrite()");
		String startDate = new SimpleDateFormat(TRUSeoFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUSeoFeedReferenceConstants.FILES_WRITING);
		if (mEntityIdMap != null && !mEntityIdMap.isEmpty()) {
			for (BaseFeedProcessVO lBaseFeedProcessVO : pVOItems) {
				TRUSeoFeedVO lfeedVO = (TRUSeoFeedVO) lBaseFeedProcessVO;
				updateItem(lfeedVO, mEntityIdMap);
			}
		}

		getLogger().vlogDebug("End:@Class: TRUSeoFeedItemWriter : @Method: doWrite()");
	}

	/**
	 * Update item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pEntityIdMap
	 *            the entity id map
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void updateItem(BaseFeedProcessVO pFeedVO, Map<String, Map<String, String>> pEntityIdMap) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedItemWriter : @Method: updateItem()");
		TRUSeoFeedVO seoVo = (TRUSeoFeedVO) pFeedVO;
		String lRepositoryName = getEntityRepositoryName(seoVo.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(seoVo.getEntityName());
		try {
			if (lItemDiscriptorName != null) {
				IFeedItemMapper<BaseFeedProcessVO> lISeoFeedItemMapper = (IFeedItemMapper<BaseFeedProcessVO>) getMapperMap().get(seoVo.getEntityName());
				Map<String, String> entityMap = mEntityIdMap.get(seoVo.getEntityName());

				String repId = entityMap.get(seoVo.getId());
				if (seoVo.getEntityName().equals(FeedConstants.SKU) && StringUtils.isNotBlank(repId)) {
					String[] skuIds = repId.split(FeedConstants.HASH);
					for (int i = 0; i < skuIds.length; i++) {

						MutableRepositoryItem skuItem = null;
						if (StringUtils.isNotBlank(skuIds[i])) {
							skuItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(skuIds[i], FeedConstants.SKU);
							if (lISeoFeedItemMapper != null && skuItem != null) {
								skuItem = lISeoFeedItemMapper.map(seoVo, skuItem);
							}
						}

						getRepository(lRepositoryName).updateItem(skuItem);
					}
				} else if (seoVo.getEntityName().equals(TRUSeoFeedReferenceConstants.CATEGORY) && StringUtils.isNotBlank(repId)) {
					MutableRepositoryItem categoryItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(repId,
							TRUSeoFeedReferenceConstants.CLASSIFICATION);
					if (lISeoFeedItemMapper != null && categoryItem != null) {
						categoryItem = lISeoFeedItemMapper.map(seoVo, categoryItem);
						getRepository(lRepositoryName).updateItem(categoryItem);
					}
				}

				addUpdateItemToList(seoVo);
			}
		} catch (Exception exception) {
			getLogger().vlogError(TRUSeoFeedReferenceConstants.EXCEPTION, exception);
			String message = exception.getMessage();
			if (StringUtils.isBlank(message) && exception.getCause() != null) {
				message = exception.getCause().getMessage();
			}
			if (pFeedVO instanceof TRUSeoFeedVO) {
				((TRUSeoFeedVO) pFeedVO).setErrorMessage(TRUSeoFeedReferenceConstants.REPOSITORY_EXCEPTION_MSG + TRUSeoFeedReferenceConstants.EMPTY_STRING
						+ message);
			}
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUSeoFeedReferenceConstants.INVALID_RECORD_MAX_SIZE, null, exception);
		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedItemWriter : @Method: updateItem()");
	}

	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifier id store key
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the new identifier id store key
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		this.mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Gets the seo feed repository properties.
	 * 
	 * @return the seo feed repository properties
	 */
	public TRUSeoFeedRepositoryProperties getSeoFeedRepositoryProperties() {
		return mSeoFeedRepositoryProperties;
	}

	/**
	 * Sets the seo feed repository properties.
	 * 
	 * @param pSeoFeedRepositoryProperties
	 *            the new seo feed repository properties
	 */
	public void setSeoFeedRepositoryProperties(TRUSeoFeedRepositoryProperties pSeoFeedRepositoryProperties) {
		this.mSeoFeedRepositoryProperties = pSeoFeedRepositoryProperties;
	}
}
