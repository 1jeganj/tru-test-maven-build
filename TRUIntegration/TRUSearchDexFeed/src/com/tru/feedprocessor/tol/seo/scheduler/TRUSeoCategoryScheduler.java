package com.tru.feedprocessor.tol.seo.scheduler;

import java.util.Calendar;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feedprocessor.tol.BatchInvoker;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;

/**
 * The Class TRUSeoCategoryScheduler.
 */
public class TRUSeoCategoryScheduler extends SingletonSchedulableService {

	/** Property to hold mFeedJob. */
	private BatchInvoker mFeedJob;
	
	private FeedHelper mFeedHelper;

	/** The Enable. */
	private boolean mEnable;

	/**
	 * Do scheduled task.
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		vlogInfo("Begin @Class: TRUSeoCatalogScheduler, @method: doScheduledTask()");

		if (isEnable()) {
			processSeoCategoryFeed();
		}

		vlogInfo("End @Class: TRUSeoCatalogScheduler, @method: doScheduledTask()");

	}

	/**
	 * Process price feed.
	 */
	public void processSeoCategoryFeed() {
		vlogInfo("Begin @Class: TRUSeoCatalogScheduler, @method: processPriceFeed()");
		getFeedJob().loadContext(new String[] { FeedConstants.JOB_CONFIG_PATH, TRUSeoFeedReferenceConstants.SEO_FEED_CONFIG_PATH });
		Calendar lCDateTime = Calendar.getInstance();
		String[] input = { TRUSeoFeedReferenceConstants.SEO_JOB_NAME,
				TRUSeoFeedReferenceConstants.FEED_FILE_TYPE + TRUSeoFeedReferenceConstants.EQUALS + TRUSeoFeedReferenceConstants.CATEGORY,
				TRUSeoFeedReferenceConstants.SEO_JOB_NAME + TRUSeoFeedReferenceConstants.EQUALS + lCDateTime.getTimeInMillis() };

		getFeedJob().startJob(input);
		vlogInfo("End @Class: TRUSeoCatalogScheduler, @method: processPriceFeed()");
	}

	/**
	 * Gets the feed job.
	 * 
	 * @return the feed job
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 * 
	 * @param pFeedJob
	 *            the new feed job
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		mFeedJob = pFeedJob;
	}

	/**
	 * Checks if is enable.
	 * 
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * @return the feedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * @param pFeedHelper the feedHelper to set
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

}
