package com.tru.feedprocessor.tol.seo.loader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.file.FlatFileParseException;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.FeedHelper;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader;
import com.tru.feedprocessor.tol.base.tasklet.TRUSeoFeedFileDownloadTask;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.seo.TRUSeoFeedConfig;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoFeedLoader.
 * 
 */
public class TRUSeoFeedLoader extends AbstractCSVFeedLoader {


	/** Property to hold logging. */
	public static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(TRUSeoFeedLoader.class);

	/** The mTRUProductAnalyticsFeedConfiguration. */
	private TRUSeoFeedConfig mTRUSeoFeedConfig;

	/** The mTRUProductAnalyticsFileDownloadTask. */
	private TRUSeoFeedFileDownloadTask mSeoFeedFileDownloadTask;

	/** The TRU best seller feed env configuration. */
	private TRUFeedEnvConfiguration mTRUFeedEnvConfiguration;

	/** The TRU best seller feed email service. */
	private TRUFeedEmailService mTRUFeedEmailService;

	/** The Feed feed helper. */
	private FeedHelper mFeedHelper;

	/**
	 * Load data.
	 * 
	 * @param pCurExecCntx
	 *            the cur exec cntx
	 * @return the map< string, list<? extends base feed process v o>>
	 * @throws Exception
	 *             the exception
	 * @see com.tru.feedprocessor.tol.base.parse.IFeedDataParser#parseData(java.io.InputStream)
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(FeedExecutionContextVO pCurExecCntx) throws Exception {

		try {
			doOpen(pCurExecCntx);
			doRead();
		} catch (FlatFileParseException exception) {
			getLogger().logError(exception);
			throw new FeedSkippableException(TRUSeoFeedReferenceConstants.PRE_PROCESSING, TRUSeoFeedReferenceConstants.SEO_FEED_DATA_LOADER_STEP,
					FeedConstants.INVALID_FILE, null, null);
		} finally {
			doClose();
		}
		return mVOHolderMap;
	}
	
	/**
	 * @return the tRUSeoFeedConfig
	 */
	public TRUSeoFeedConfig getTRUSeoFeedConfig() {
		return mTRUSeoFeedConfig;
	}

	/**
	 * Sets the TRU seo feed config.
	 * 
	 * @param pTRUSeoFeedConfig
	 *            the tRUSeoFeedConfig to set
	 */
	public void setTRUSeoFeedConfig(TRUSeoFeedConfig pTRUSeoFeedConfig) {
		mTRUSeoFeedConfig = pTRUSeoFeedConfig;
	}

	/**
	 * Gets the seo feed file download task.
	 * 
	 * @return the seoFeedFileDownloadTask
	 */
	public TRUSeoFeedFileDownloadTask getSeoFeedFileDownloadTask() {
		return mSeoFeedFileDownloadTask;
	}

	/**
	 * Sets the seo feed file download task.
	 * 
	 * @param pSeoFeedFileDownloadTask
	 *            the seoFeedFileDownloadTask to set
	 */
	public void setSeoFeedFileDownloadTask(TRUSeoFeedFileDownloadTask pSeoFeedFileDownloadTask) {
		mSeoFeedFileDownloadTask = pSeoFeedFileDownloadTask;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#
	 * isResolveDuplicate()
	 */
	@Override
	public boolean isResolveDuplicate() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#getColumnNames
	 * ()
	 */
	@Override
	public String[] getColumnNames() {
		String[] stringArray = new String[] { TRUSeoFeedReferenceConstants.ID, TRUSeoFeedReferenceConstants.PAGE_TITLE,
				TRUSeoFeedReferenceConstants.META_DESCRIPTION, TRUSeoFeedReferenceConstants.META_KEYWORD, TRUSeoFeedReferenceConstants.CANONICAL_URL,
				TRUSeoFeedReferenceConstants.H1, TRUSeoFeedReferenceConstants.ALT_URL, TRUSeoFeedReferenceConstants.CONTENT_BLOCK, };
		return stringArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#
	 * getLineMappingVOClass()
	 */
	@Override
	public Class<?> getLineMappingVOClass() throws Exception {
		return Class.forName(TRUSeoFeedReferenceConstants.SEO_VO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#
	 * getDelimiterString()
	 */
	@Override
	public String getDelimiterString() {
		return String.valueOf(TRUSeoFeedReferenceConstants.CAP_DELIMITER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#getLinesToSkip
	 * ()
	 */
	@Override
	public int getLinesToSkip() {
		return TRUSeoFeedReferenceConstants.NUMBER_ONE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#
	 * processEntityMapVO(com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO)
	 */
	@Override
	public List<? extends BaseFeedProcessVO> processEntityMapVO(BaseFeedProcessVO pBaseFeedProcessVO) {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("processEntityMapVO ");
		}
		TRUSeoFeedVO seoFeedVO = (TRUSeoFeedVO) pBaseFeedProcessVO;
		List<TRUSeoFeedVO> baseProcessVOList = new ArrayList<TRUSeoFeedVO>();
		baseProcessVOList.add(seoFeedVO);
		pBaseFeedProcessVO.setEntityName(TRUSeoFeedReferenceConstants.SEO_PRODUCT_ENTITY_NAME);
		return baseProcessVOList;
	}

	/**
	 * Gets the feed helper.
	 * 
	 * @return the feedHelper
	 */
	public FeedHelper getFeedHelper() {
		return mFeedHelper;
	}

	/**
	 * Sets the feed helper.
	 * 
	 * @param pFeedHelper
	 *            the feedHelper to set
	 */
	public void setFeedHelper(FeedHelper pFeedHelper) {
		mFeedHelper = pFeedHelper;
	}

	/**
	 * Gets the TRU best seller feed email service.
	 * 
	 * @return the tRUBestSellerFeedEmailService
	 */
	public TRUFeedEmailService getTRUFeedEmailService() {
		return mTRUFeedEmailService;
	}

	/**
	 * Sets the TRU best seller feed email service.
	 * 
	 * @param pTRUFeedEmailService
	 *            the tRUBestSellerFeedEmailService to set
	 */
	public void setTRUFeedEmailService(TRUFeedEmailService pTRUFeedEmailService) {
		mTRUFeedEmailService = pTRUFeedEmailService;
	}

	/**
	 * Gets the TRU best seller feed env configuration.
	 * 
	 * @return the tRUBestSellerFeedEnvConfiguration
	 */
	public TRUFeedEnvConfiguration getTRUFeedEnvConfiguration() {
		return mTRUFeedEnvConfiguration;
	}

	/**
	 * Sets the TRU best seller feed env configuration.
	 * 
	 * @param pTRUFeedEnvConfiguration
	 *            the tRUBestSellerFeedEnvConfiguration to set
	 */
	public void setTRUFeedEnvConfiguration(TRUFeedEnvConfiguration pTRUFeedEnvConfiguration) {
		mTRUFeedEnvConfiguration = pTRUFeedEnvConfiguration;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public ApplicationLogging getLogger() {
		return mLogger;
	}
}
