package com.tru.feedprocessor.tol.seo.vo;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class TRUSeoFeedVO.
 * 
 */
public class TRUSeoFeedVO extends BaseFeedProcessVO {

	/** The m page title. */
	private String mSeoTagId;

	/** The m page title. */
	private String mPageTitle;

	/** The m meta description. */
	private String mMetaDescription;

	/** The m meta keyword. */
	private String mMetaKeyword;

	/** The m canonical url. */
	private String mCanonicalUrl;

	/** The m seo h1 text. */
	private String mSeoH1Text;

	/** The m alt url. */
	private String mAltUrl;

	/** The m content block. */
	private String mContentBlock;

	/** The Seo id. */
	private String mSeoId;

	/** The Sku id. */
	private String mSkuId;

	/** The m id. */
	private String mId;
	
	/** The m mErrorMessage. */
	private String mErrorMessage;

	/**
	 * Instantiates a new product.
	 */
	public TRUSeoFeedVO() {
		super.setEntityName(TRUSeoFeedReferenceConstants.SEO_PRODUCT_ENTITY_NAME);
	}

	/**
	 * Convert to String.
	 * 
	 * @return the final PricetId
	 */

	@Override
	public String toString() {
		StringBuffer finalSeoDetail = new StringBuffer();
		finalSeoDetail.append(getSeoId()).append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getSkuId())
				.append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getPageTitle()).append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getMetaDescription()).append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getMetaKeyword())
				.append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getCanonicalUrl())
				.append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getSeoH1Text()).append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getAltUrl()).append(TRUSeoFeedReferenceConstants.PIPE_DELIMITER_STRING).append(getContentBlock());
		return finalSeoDetail.toString();
	}

	/**
	 * Gets the page title.
	 * 
	 * @return the page title
	 */
	public String getPageTitle() {
		return mPageTitle;
	}

	/**
	 * Sets the page title.
	 * 
	 * @param pPageTitle
	 *            the new page title
	 */
	public void setPageTitle(String pPageTitle) {
		this.mPageTitle = pPageTitle;
	}

	/**
	 * Gets the meta description.
	 * 
	 * @return the meta description
	 */
	public String getMetaDescription() {
		return mMetaDescription;
	}

	/**
	 * Sets the meta description.
	 * 
	 * @param pMetaDescription
	 *            the new meta description
	 */
	public void setMetaDescription(String pMetaDescription) {
		this.mMetaDescription = pMetaDescription;
	}

	/**
	 * Gets the meta keyword.
	 * 
	 * @return the meta keyword
	 */
	public String getMetaKeyword() {
		return mMetaKeyword;
	}

	/**
	 * Sets the meta keyword.
	 * 
	 * @param pMetaKeyword
	 *            the new meta keyword
	 */
	public void setMetaKeyword(String pMetaKeyword) {
		this.mMetaKeyword = pMetaKeyword;
	}

	/**
	 * Gets the canonical url.
	 * 
	 * @return the canonical url
	 */
	public String getCanonicalUrl() {
		return mCanonicalUrl;
	}

	/**
	 * Sets the canonical url.
	 * 
	 * @param pCanonicalUrl
	 *            the new canonical url
	 */
	public void setCanonicalUrl(String pCanonicalUrl) {
		this.mCanonicalUrl = pCanonicalUrl;
	}

	/**
	 * Gets the seo h1 text.
	 * 
	 * @return the seo h1 text
	 */
	public String getSeoH1Text() {
		return mSeoH1Text;
	}

	/**
	 * Sets the seo h1 text.
	 * 
	 * @param pSeoH1Text
	 *            the new seo h1 text
	 */
	public void setSeoH1Text(String pSeoH1Text) {
		this.mSeoH1Text = pSeoH1Text;
	}

	/**
	 * Gets the alt url.
	 * 
	 * @return the alt url
	 */
	public String getAltUrl() {
		return mAltUrl;
	}

	/**
	 * Sets the alt url.
	 * 
	 * @param pAltUrl
	 *            the new alt url
	 */
	public void setAltUrl(String pAltUrl) {
		this.mAltUrl = pAltUrl;
	}

	/**
	 * Gets the content block.
	 * 
	 * @return the content block
	 */
	public String getContentBlock() {
		return mContentBlock;
	}

	/**
	 * Sets the content block.
	 * 
	 * @param pContentBlock
	 *            the new content block
	 */
	public void setContentBlock(String pContentBlock) {
		this.mContentBlock = pContentBlock;
	}

	/**
	 * Gets the seo tag id.
	 * 
	 * @return the seo tag id
	 */
	public String getSeoTagId() {
		return mSeoTagId;
	}

	/**
	 * Sets the seo tag id.
	 * 
	 * @param pSeoTagId
	 *            the new seo tag id
	 */
	public void setSeoTagId(String pSeoTagId) {
		this.mSeoTagId = pSeoTagId;
	}

	/**
	 * Sets the seo id.
	 * 
	 * @param pSeoId
	 *            the seoId to set
	 */
	public void setSeoId(String pSeoId) {
		mSeoId = pSeoId;
	}

	/**
	 * Sets the sku id.
	 * 
	 * @param pSkuId
	 *            the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the seo id.
	 * 
	 * @return the seoId
	 */
	public String getSeoId() {
		return mSeoId;
	}

	/**
	 * Gets the sku id.
	 * 
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Sets the id.
	 * 
	 * @param pId
	 *            the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}
	
	/**
	 * Gets the error message.
	 *
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param pErrorMessage the errorMessage to set
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}

}
