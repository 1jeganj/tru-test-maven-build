package com.tru.feedprocessor.tol.seo;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.config.AbstractFeedConfig;

/**
 * The Class TRUSeoFeedConfig.
 */
public class TRUSeoFeedConfig extends AbstractFeedConfig {

	/** The Bad directory. */
	private String mBadDirectory;

	/** The Processing directory. */
	private String mProcessingDirectory;

	/** The Success directory. */
	private String mSuccessDirectory;

	/** The Error directory. */
	private String mErrorDirectory;

	/** Property to hold Unprocces directory. */
	private String mUnProcessDirectory;

	/** Property to hold mSourceDirectory directory. */
	private String mSourceDirectory;

	/** The mDataLoaderClassForSku. */
	private String mDataLoaderClassForSku;

	/** The Data loader class for category. */
	private String mDataLoaderClassForCategory;

	/** The Product file name. */
	private String mProductFileName;

	/**
	 * Gets the data loader class fqcn.
	 * 
	 * @return the feedEntityProviderFQCN
	 */
	public String getDataLoaderClassForSku() {
		return mDataLoaderClassForSku;
	}

	/**
	 * Sets the cat feed entry provider pFileParserClassFQCN.
	 * 
	 * @param pDataLoaderClassForSku
	 *            the new data loader class for sku
	 */
	public void setDataLoaderClassForSku(String pDataLoaderClassForSku) {
		mDataLoaderClassForSku = pDataLoaderClassForSku;
		setConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_SKU, pDataLoaderClassForSku);
	}

	/**
	 * Gets the data loader class for category.
	 * 
	 * @return the dataLoaderClassForCategory
	 */
	public String getDataLoaderClassForCategory() {
		return mDataLoaderClassForCategory;
	}

	/**
	 * Sets the data loader class for category.
	 * 
	 * @param pDataLoaderClassForCategory
	 *            the dataLoaderClassForCategory to set
	 */
	public void setDataLoaderClassForCategory(String pDataLoaderClassForCategory) {
		mDataLoaderClassForCategory = pDataLoaderClassForCategory;
		setConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_CATEGORY, pDataLoaderClassForCategory);
	}

	/**
	 * Gets the bad directory.
	 * 
	 * @return the badDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}

	/**
	 * Gets the processing directory.
	 * 
	 * @return the processingDirectory
	 */
	public String getProcessingDirectory() {
		return mProcessingDirectory;
	}

	/**
	 * Gets the success directory.
	 * 
	 * @return the successDirectory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}

	/**
	 * Gets the error directory.
	 * 
	 * @return the errorDirectory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}

	/**
	 * Gets the un process directory.
	 * 
	 * @return the unProcessDirectory
	 */
	public String getUnProcessDirectory() {
		return mUnProcessDirectory;
	}

	/**
	 * Gets the source directory.
	 * 
	 * @return the mSourceDirectory
	 */
	public String getSourceDirectory() {
		return mSourceDirectory;
	}

	/**
	 * Sets the source directory.
	 * 
	 * @param pSourceDirectory
	 *            the new source directory
	 */
	public void setSourceDirectory(String pSourceDirectory) {
		mSourceDirectory = pSourceDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.SOURCE_DIRECTORY, pSourceDirectory);
	}

	/**
	 * Sets the processing directory.
	 * 
	 * @param pProcessingDirectory
	 *            the new processing directory
	 */
	public void setProcessingDirectory(String pProcessingDirectory) {
		mProcessingDirectory = pProcessingDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.PROCESSING_DIRECTORY, pProcessingDirectory);
	}

	/**
	 * Sets the un process directory.
	 * 
	 * @param pUnProcessDirectory
	 *            the new un process directory
	 */
	public void setUnProcessDirectory(String pUnProcessDirectory) {
		mUnProcessDirectory = pUnProcessDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.UN_PROCESS_DIRECTORY, pUnProcessDirectory);
	}

	/**
	 * Sets the bad directory.
	 * 
	 * @param pBadDirectory
	 *            the new bad directory
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.BAD_DIRECTORY, pBadDirectory);
	}

	/**
	 * Sets the success directory.
	 * 
	 * @param pSuccessDirectory
	 *            the new success directory
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.SUCCESS_DIRECTORY, pSuccessDirectory);
	}

	/**
	 * Sets the error directory.
	 * 
	 * @param pErrorDirectory
	 *            the new error directory
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
		setConfigValue(TRUSeoFeedReferenceConstants.ERROR_DIRECTORY, pErrorDirectory);
	}

	/**
	 * Sets the feed file pattern.
	 * 
	 * @param pFeedFilePattern
	 *            the new feed file pattern
	 */
	public void setFeedFilePattern(String pFeedFilePattern) {
		setConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_PATTERN, pFeedFilePattern);
	}

	/**
	 * Sets the file name tru env.
	 * 
	 * @param pFileNameTRUEnv
	 *            the new file name tru env
	 */
	public void setFileNameTRUEnv(String pFileNameTRUEnv) {
		setConfigValue(TRUSeoFeedReferenceConstants.FILE_NAME_TRU_ENV, pFileNameTRUEnv);
	}

	/**
	 * Sets the file feed name.
	 * 
	 * @param pFileFeedName
	 *            the new file feed name
	 */
	public void setFileFeedName(String pFileFeedName) {
		setConfigValue(TRUSeoFeedReferenceConstants.FILE_FEED_NAME, pFileFeedName);
	}
	
	/**
	 * Gets the product file name.
	 *
	 * @return the product file name
	 */
	public String getProductFileName() {
		return mProductFileName;
	}

	/**
	 * Sets the product file name.
	 *
	 * @param pProductFileName the new product file name
	 */
	public void setProductFileName(String pProductFileName) {
		mProductFileName=pProductFileName;
		setConfigValue(TRUSeoFeedReferenceConstants.PRODUCT_FILE_NAME, pProductFileName);
	}

	/**
	 * this method is used to log whether the required properties are set to the
	 * configuration for processing the feed.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUSeoFeedConfig : @Method: afterPropertiesSet()");

		if (null != getConfigValue(FeedConstants.ENTITY_LIST)
				&& ((List<String>) getConfigValue(FeedConstants.ENTITY_LIST)).size() == FeedConstants.NUMBER_ZERO) {
			getLogger().logError("Entity list can not be null in seo feed configuration");
		}
		if (null != getConfigValue(FeedConstants.ENTITY_SQL)
				&& ((Map<String, String>) getConfigValue(FeedConstants.ENTITY_SQL)).size() == FeedConstants.NUMBER_ZERO) {
			getLogger().logError("Entity sql can not be null in seo feed configuration");
		}
		String voName = (String) getConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_SKU);
		if (StringUtils.isBlank(voName)) {
			getLogger().logError("File Parse class FQCN can not be null in seo feed configuration");
		}
		try {
			Class.forName(voName);
		} catch (ClassNotFoundException e) {
			if (isLoggingError()) {
				logError("File Parse class FQCN must be a valid entry", e);
			}
		}
		
		String voCatName = (String) getConfigValue(TRUSeoFeedReferenceConstants.DATA_LOADER_CLASS_FOR_CATEGORY);
		if (StringUtils.isBlank(voCatName)) {
			getLogger().logError("File Parse class FQCN can not be null in seo feed configuration");
		}
		try {
			Class.forName(voCatName);
		} catch (ClassNotFoundException e) {
			if (isLoggingError()) {
				logError("File Parse class FQCN must be a valid entry", e);
			}
		}

		for (String key : ((List<String>) getConfigValue(FeedConstants.ENTITY_LIST))) {
			if (!((Map<String, String>) getConfigValue(FeedConstants.ENTITY_SQL)).containsKey(key)) {
				getLogger().logError("SQL is mandatory for each entity provided in entity list in seo feed configuration");
			}
		}
		if (null != getConfigValue(TRUSeoFeedReferenceConstants.PROCESSING_DIRECTORY)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.PROCESSING_DIRECTORY).toString())) {
			getLogger().logError("Processing directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.UN_PROCESS_DIRECTORY)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.UN_PROCESS_DIRECTORY).toString())) {
			getLogger().logError("Un Process directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.BAD_DIRECTORY)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.BAD_DIRECTORY).toString())) {
			getLogger().logError("Bad directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.SUCCESS_DIRECTORY)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.SUCCESS_DIRECTORY).toString())) {
			getLogger().logError("Success directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.ERROR_DIRECTORY)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.ERROR_DIRECTORY).toString())) {
			getLogger().logError("Error directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_PATTERN)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.FEED_FILE_PATTERN).toString())) {
			getLogger().logError("Error directory path is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.FILE_NAME_TRU_ENV)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.FILE_NAME_TRU_ENV).toString())) {
			getLogger().logError("File environment name is empty");
		}

		if (null != getConfigValue(TRUSeoFeedReferenceConstants.FILE_FEED_NAME)
				&& StringUtils.isBlank(getConfigValue(TRUSeoFeedReferenceConstants.FILE_FEED_NAME).toString())) {
			getLogger().logError("File Feed Name is empty");
		}
		getLogger().vlogDebug("End:@Class: TRUSeoFeedConfig : @Method: afterPropertiesSet()");
	}

}
