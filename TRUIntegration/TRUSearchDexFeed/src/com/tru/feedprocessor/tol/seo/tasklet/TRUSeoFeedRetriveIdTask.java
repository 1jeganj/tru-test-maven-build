package com.tru.feedprocessor.tol.seo.tasklet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The class TRUSEOFeedRetriveIdTask is extended to override two methods to type
 * cast the base vo to price vo.
 * 
 * This is the common class which is used to retrieve the unique id and
 * repository id from the corresponding data source. Using the
 * EntiryResultExtractor it will store the data into feed context.
 */
public class TRUSeoFeedRetriveIdTask extends AbstractRetrieveIdForIdentifierTask {

	/**
	 * this method is used to construct the query based on the feed records.
	 * 
	 * @param pCurrentIdentifier
	 *            the current identifier
	 * @return the query
	 */
	@Override
	protected String getQuery(String pCurrentIdentifier) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedRetrivePriceIdTask : @Method: getQuery()");
		Map<String, String> lEntitySQLMap = (HashMap<String, String>) getConfigValue(FeedConstants.ENTITY_SQL);
		return lEntitySQLMap.get(pCurrentIdentifier);
	}

	/**
	 * Gets the identifiers.
	 * 
	 * @return the identifiers
	 * @see com.tru.feedprocessor.tol.base.tasklet.AbstractRetrieveIdForIdentifierTask#getIdentifiers()
	 */
	@Override
	protected List<String> getIdentifiers() {
		Map<String, List<? extends BaseFeedProcessVO>> lPriceDataMap = getEntityMap();
		return new ArrayList(lPriceDataMap.keySet());
	}

	/**
	 * Gets the entity map.
	 * 
	 * @return mEntityDataProvider
	 */
	protected Map<String, List<? extends BaseFeedProcessVO>> getEntityMap() {
		EntityDataProvider lEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		return lEntityDataProvider.getAllEntityList();
	}
}
