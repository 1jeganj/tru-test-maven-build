package com.tru.feedprocessor.tol.seo.scheduler;

import java.util.Calendar;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feedprocessor.tol.BatchInvoker;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;

/**
 * The Class TRUSeoProductScheduler.
 */
public class TRUSeoProductScheduler extends SingletonSchedulableService {

	/** Property to hold mFeedJob. */
	private BatchInvoker mFeedJob;

	/** The Enable. */
	private boolean mEnable;

	/**
	 * Do scheduled task.
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		vlogInfo("Begin @Class: TRUSeoProductScheduler, @method: doScheduledTask()");

		if (isEnable()) {
			processSeoProductFeed();
		}

		vlogInfo("End @Class: TRUSeoProductScheduler, @method: doScheduledTask()");

	}

	/**
	 * Process price feed.
	 */
	public void processSeoProductFeed() {
		vlogInfo("Begin @Class: TRUSeoProductScheduler, @method: processPriceFeed()");
		getFeedJob().loadContext(new String[] { FeedConstants.JOB_CONFIG_PATH, TRUSeoFeedReferenceConstants.SEO_FEED_CONFIG_PATH });
		Calendar lCDateTime = Calendar.getInstance();
		String[] input = { TRUSeoFeedReferenceConstants.SEO_JOB_NAME,
				TRUSeoFeedReferenceConstants.FEED_FILE_TYPE + TRUSeoFeedReferenceConstants.EQUALS + TRUSeoFeedReferenceConstants.PRODUCT,
				TRUSeoFeedReferenceConstants.SEO_JOB_NAME + TRUSeoFeedReferenceConstants.EQUALS + lCDateTime.getTimeInMillis()};

		getFeedJob().startJob(input);
		vlogInfo("End @Class: TRUSeoProductScheduler, @method: processPriceFeed()");
	}

	/**
	 * Gets the feed job.
	 * 
	 * @return the feed job
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 * 
	 * @param pFeedJob
	 *            the new feed job
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		mFeedJob = pFeedJob;
	}

	/**
	 * Checks if is enable.
	 * 
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

}
