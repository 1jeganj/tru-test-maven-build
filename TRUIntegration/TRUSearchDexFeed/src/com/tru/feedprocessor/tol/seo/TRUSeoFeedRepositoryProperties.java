package com.tru.feedprocessor.tol.seo;

/**
 * The Class TRUSeoFeedRepositoryProperties is used to hold seo feed repository
 * properties.
 * 
 */

public class TRUSeoFeedRepositoryProperties {
	/** property to hold mSeoTagId. */
	private String mSeoTagId;

	/** The m page title. */
	private String mSeoTitle;

	/** The m meta description. */
	private String mSeoMetaDescription;

	/** The m meta keyword. */
	private String mSeoMetaKeyword;

	/** The m canonical url. */
	private String mSeoCanonicalUrl;

	/** The m seo h1 text. */
	private String mSeoH1Text;

	/** The m alt url. */
	private String mSeoAltUrl;

	/** The m content block. */
	private String mSeoContentBlock;

	/** The m seo tag override. */
	private String mSeoTagOverride;

	/**
	 * Gets the seo tag id.
	 * 
	 * @return the seo tag id
	 */
	public String getSeoTagId() {
		return mSeoTagId;
	}

	/**
	 * Sets the seo tag id.
	 * 
	 * @param pSeoTagId
	 *            the new seo tag id
	 */
	public void setSeoTagId(String pSeoTagId) {
		this.mSeoTagId = pSeoTagId;
	}

	/**
	 * Gets the seo title.
	 * 
	 * @return the seo title
	 */
	public String getSeoTitle() {
		return mSeoTitle;
	}

	/**
	 * Sets the seo title.
	 * 
	 * @param pSeoTitle
	 *            the new seo title
	 */
	public void setSeoTitle(String pSeoTitle) {
		this.mSeoTitle = pSeoTitle;
	}

	/**
	 * Gets the seo meta description.
	 * 
	 * @return the seo meta description
	 */
	public String getSeoMetaDescription() {
		return mSeoMetaDescription;
	}

	/**
	 * Sets the seo meta description.
	 * 
	 * @param pSeoMetaDescription
	 *            the new seo meta description
	 */
	public void setSeoMetaDescription(String pSeoMetaDescription) {
		this.mSeoMetaDescription = pSeoMetaDescription;
	}

	/**
	 * Gets the seo meta keyword.
	 * 
	 * @return the seo meta keyword
	 */
	public String getSeoMetaKeyword() {
		return mSeoMetaKeyword;
	}

	/**
	 * Sets the seo meta keyword.
	 * 
	 * @param pSeoMetaKeyword
	 *            the new seo meta keyword
	 */
	public void setSeoMetaKeyword(String pSeoMetaKeyword) {
		this.mSeoMetaKeyword = pSeoMetaKeyword;
	}

	/**
	 * Gets the seo canonical url.
	 * 
	 * @return the seo canonical url
	 */
	public String getSeoCanonicalUrl() {
		return mSeoCanonicalUrl;
	}

	/**
	 * Sets the seo canonical url.
	 * 
	 * @param pSeoCanonicalUrl
	 *            the new seo canonical url
	 */
	public void setSeoCanonicalUrl(String pSeoCanonicalUrl) {
		this.mSeoCanonicalUrl = pSeoCanonicalUrl;
	}

	/**
	 * Gets the seo h1 text.
	 * 
	 * @return the seo h1 text
	 */
	public String getSeoH1Text() {
		return mSeoH1Text;
	}

	/**
	 * Sets the seo h1 text.
	 * 
	 * @param pSeoH1Text
	 *            the new seo h1 text
	 */
	public void setSeoH1Text(String pSeoH1Text) {
		this.mSeoH1Text = pSeoH1Text;
	}

	/**
	 * Gets the seo alt url.
	 * 
	 * @return the seo alt url
	 */
	public String getSeoAltUrl() {
		return mSeoAltUrl;
	}

	/**
	 * Sets the seo alt url.
	 * 
	 * @param pSeoAltUrl
	 *            the new seo alt url
	 */
	public void setSeoAltUrl(String pSeoAltUrl) {
		this.mSeoAltUrl = pSeoAltUrl;
	}

	/**
	 * Gets the seo content block.
	 * 
	 * @return the seo content block
	 */
	public String getSeoContentBlock() {
		return mSeoContentBlock;
	}

	/**
	 * Sets the seo content block.
	 * 
	 * @param pSeoContentBlock
	 *            the new seo content block
	 */
	public void setSeoContentBlock(String pSeoContentBlock) {
		this.mSeoContentBlock = pSeoContentBlock;
	}

	/**
	 * Gets the seo tag override.
	 * 
	 * @return the seo tag override
	 */
	public String getSeoTagOverride() {
		return mSeoTagOverride;
	}

	/**
	 * Sets the seo tag override.
	 * 
	 * @param pSeoTagOverride
	 *            the new seo tag override
	 */
	public void setSeoTagOverride(String pSeoTagOverride) {
		this.mSeoTagOverride = pSeoTagOverride;
	}

}
