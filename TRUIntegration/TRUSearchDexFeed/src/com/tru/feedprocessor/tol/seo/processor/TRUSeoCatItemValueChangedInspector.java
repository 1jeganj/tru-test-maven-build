package com.tru.feedprocessor.tol.seo.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.seo.TRUSeoFeedRepositoryProperties;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoCatItemValueChangedInspector will inspect for each repository
 * item level properties if any property changed then that record will is
 * processed to update if no changes then that record is skipped.
 * 
 */
public class TRUSeoCatItemValueChangedInspector implements IItemValueChangeInspector {

	/** The mLogger. */
	private FeedLogger mLogger;

	/** The Price feed repository properties. */
	private TRUSeoFeedRepositoryProperties mSeoFeedRepositoryProperties;

	/**
	 * The method isUpdated() will inspect for necessary vo properties with
	 * respective repository item level properties if any property changed then
	 * that record will is processed to update if no changes then that record is
	 * skipped.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem) throws FeedSkippableException {
		TRUSeoFeedVO seoFeedVO = (TRUSeoFeedVO) pBaseFeedProcessVO;

		getLogger().vlogDebug("Begin:@Class: TRUSeoItemValueChangedInspector : @Method: isUpdated():: isUpdated :: {0}", Boolean.FALSE);

		RepositoryItem seoTag = null;

		if (pRepositoryItem != null) {
			seoTag = (RepositoryItem) pRepositoryItem.getPropertyValue(getSeoFeedRepositoryProperties().getSeoTagOverride());
		}
		if (seoTag == null) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getAltUrl())
				&& !seoFeedVO.getAltUrl().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoAltUrl()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getCanonicalUrl())
				&& !seoFeedVO.getCanonicalUrl().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoCanonicalUrl()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getContentBlock())
				&& !seoFeedVO.getContentBlock().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoContentBlock()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getMetaDescription())
				&& !seoFeedVO.getMetaDescription().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoMetaDescription()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getMetaKeyword())
				&& !seoFeedVO.getMetaKeyword().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoMetaKeyword()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getPageTitle())
				&& !seoFeedVO.getPageTitle().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoTitle()))) {
			return Boolean.TRUE;
		}

		if (seoFeedVO != null && StringUtils.isNotEmpty(seoFeedVO.getSeoH1Text())
				&& !seoFeedVO.getSeoH1Text().equals(seoTag.getPropertyValue(getSeoFeedRepositoryProperties().getSeoH1Text()))) {
			return Boolean.TRUE;
		}

		getLogger().vlogDebug("End:@Class: TRUSeoItemValueChangedInspector : @Method: isUpdated():: isUpdated :: {0}", Boolean.FALSE);
		return Boolean.FALSE;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the seo feed repository properties.
	 * 
	 * @return the seo feed repository properties
	 */
	public TRUSeoFeedRepositoryProperties getSeoFeedRepositoryProperties() {
		return mSeoFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 * 
	 * @param pSeoFeedRepositoryProperties
	 *            the new seo feed repository properties
	 */
	public void setSeoFeedRepositoryProperties(TRUSeoFeedRepositoryProperties pSeoFeedRepositoryProperties) {
		mSeoFeedRepositoryProperties = pSeoFeedRepositoryProperties;
	}
}
