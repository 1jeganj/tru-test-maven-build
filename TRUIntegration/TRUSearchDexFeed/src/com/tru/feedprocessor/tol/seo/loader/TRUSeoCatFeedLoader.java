package com.tru.feedprocessor.tol.seo.loader;

import java.util.ArrayList;
import java.util.List;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoCatFeedVO;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoFeedLoader.
 * 
 */
public class TRUSeoCatFeedLoader extends TRUSeoFeedLoader {

	/** Property to hold logging. */
	public static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(TRUSeoCatFeedLoader.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#
	 * getLineMappingVOClass()
	 */
	@Override
	public Class<?> getLineMappingVOClass() throws Exception {
		return Class.forName(TRUSeoFeedReferenceConstants.SEO_CAT_VO);
	}


	@Override
	public List<? extends BaseFeedProcessVO> processEntityMapVO(BaseFeedProcessVO pBaseFeedProcessVO) {
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("processEntityMapVO ");
		}
		TRUSeoCatFeedVO seoFeedVO = (TRUSeoCatFeedVO) pBaseFeedProcessVO;
		List<TRUSeoFeedVO> baseProcessVOList = new ArrayList<TRUSeoFeedVO>();
		baseProcessVOList.add(seoFeedVO);
		pBaseFeedProcessVO.setEntityName(TRUSeoFeedReferenceConstants.CATEGORY);
		return baseProcessVOList;
	}


}
