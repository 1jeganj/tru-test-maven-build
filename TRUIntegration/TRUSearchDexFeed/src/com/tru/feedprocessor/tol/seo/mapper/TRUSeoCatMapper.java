package com.tru.feedprocessor.tol.seo.mapper;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.seo.TRUSeoFeedRepositoryProperties;
import com.tru.feedprocessor.tol.seo.vo.TRUSeoFeedVO;

/**
 * The Class TRUSeoCatMapper is used to map the properties of seo category item
 * feed vo to respective item repository properties.
 */
public class TRUSeoCatMapper extends AbstractFeedItemMapper<TRUSeoFeedVO> {

	/** The Seo feed repository properties. */
	private TRUSeoFeedRepositoryProperties mSeoFeedRepositoryProperties;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * this method is used to map the properties of feed vo to respective
	 * repository properties.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pCatRepItem
	 *            the cat rep item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUSeoFeedVO pVOItem, MutableRepositoryItem pCatRepItem) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUSeoCatMapper : @Method: map():: seo id:: {0}", pVOItem.getRepositoryId());

		Boolean isCreated = Boolean.FALSE;
		MutableRepository catalogRepository = getRepository(TRUSeoFeedReferenceConstants.CATALOG_REPOSITORY);

		MutableRepositoryItem seoTag = (MutableRepositoryItem) pCatRepItem.getPropertyValue(getSeoFeedRepositoryProperties().getSeoTagOverride());
		if (seoTag == null) {
			seoTag = catalogRepository.createItem(getSeoFeedRepositoryProperties().getSeoTagOverride());
			isCreated = Boolean.TRUE;
		}

		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoAltUrl(), pVOItem.getAltUrl());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoCanonicalUrl(), pVOItem.getCanonicalUrl());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoContentBlock(), pVOItem.getContentBlock());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoMetaDescription(), pVOItem.getMetaDescription());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoMetaKeyword(), pVOItem.getMetaKeyword());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoTitle(), pVOItem.getPageTitle());
		seoTag.setPropertyValue(getSeoFeedRepositoryProperties().getSeoH1Text(), pVOItem.getSeoH1Text());

		if (isCreated) {
			catalogRepository.addItem(seoTag);
			pCatRepItem.setPropertyValue(getSeoFeedRepositoryProperties().getSeoTagOverride(), seoTag);
		} else {
			catalogRepository.updateItem(seoTag);
		}

		getLogger().vlogDebug("Begin:@Class: TRUSeoCatMapper : @Method: map():: seo id :: {0}", pVOItem.getRepositoryId());
		return pCatRepItem;
	}

	/**
	 * Gets the seo feed repository properties.
	 * 
	 * @return the seo feed repository properties
	 */
	public TRUSeoFeedRepositoryProperties getSeoFeedRepositoryProperties() {
		return mSeoFeedRepositoryProperties;
	}

	/**
	 * Sets the seo feed repository properties.
	 * 
	 * @param pSeoFeedRepositoryProperties
	 *            the new seo feed repository properties
	 */
	public void setSeoFeedRepositoryProperties(TRUSeoFeedRepositoryProperties pSeoFeedRepositoryProperties) {
		mSeoFeedRepositoryProperties = pSeoFeedRepositoryProperties;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

}
