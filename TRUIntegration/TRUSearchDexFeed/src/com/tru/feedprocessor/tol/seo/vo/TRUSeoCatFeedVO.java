package com.tru.feedprocessor.tol.seo.vo;

import com.tru.feedprocessor.tol.TRUSeoFeedReferenceConstants;

/**
 * The Class TRUSeoCatFeedVO.
 * 
 */
public class TRUSeoCatFeedVO extends TRUSeoFeedVO {

	/**
	 * Instantiates a new product.
	 */
	public TRUSeoCatFeedVO() {
		super.setEntityName(TRUSeoFeedReferenceConstants.SEO_CATEGORY_ENTITY_NAME);
	}

}
