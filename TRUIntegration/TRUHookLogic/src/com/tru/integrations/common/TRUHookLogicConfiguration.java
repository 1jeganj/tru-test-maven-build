package com.tru.integrations.common;



import atg.nucleus.GenericService;

/**
 * This class holds the properties related to HookLogic service.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUHookLogicConfiguration extends GenericService{
	/**
	 * Holds the property value of mHooklogicURL.
	 */
	private String mHooklogicURL;
	
	/**
	 * Holds the property value of mTaxonomy.
	 */
	private String mTaxonomy;
	
	/**
	 * Holds the property value of mHlpt.
	 */
	private String mHlpt;
	
	/**
	 * Holds the property value of mPubpagetype.
	 */
	private String mPubpagetype;
	
	/**
	 * Holds the property value of mMinorganic.
	 */
	private String mMinorganic;
	
	/**
	 * Holds the property value of mMinads.
	 */
	private String mMinads;
	
	/**
	 * Holds the property value of mMaxads.
	 */
	private String mMaxads;
	/**
	 * Holds the property value of mPcount.
	 */
	private String mPcount;
	
	/**
	 * Holds the property value of mPgsize.
	 */
	private String mPgsize;
	
	/**
	 * Holds the property value of mPgn.
	 */
	private String mPgn;
	
	/**
	 * Holds the property value of mSort.
	 */
	private String mSort;
	
	/*order page*/
		
	/**
	 * Holds the property value of mHlptOrder.
	 */
	private String mHlptOrder;
	
	/**
	 * Holds the property value of mSkuOrder.
	 */
	private String mSkuOrder;
	
	/**
	 * Holds the property value of mParentSkuOrder.
	 */
	private String mParentSkuOrder;
	
	/**
	 * Holds the property value of mPriceOrder.
	 */
	private String mPriceOrder;
	
	/**
	 * Holds the property value of mQuantity.
	 */
	private String mQuantity;
		
	/**
	 * Holds the property value of mQuantity.
	 */
	private String mOrderId;
	
	/**
	 * Holds the property value of mRegularPrice.
	 */
	private String mRegularPrice;
	
	/**
	 * Holds the property value of mOrderSubTotal.
	 */
	private String mOrderSubTotal;
	
	/**
	 * Holds the property value of mOrderpubPageType.
	 */
	private String mOrderpubPageType;
	
	/**
	 * Holds the property value of mAddId.
	 */
	private String mAddId;
	
	/**
	 * getter method for HooklogicURL.
	 * @return HooklogicURL
	 */
	public String getHooklogicURL() {
		return mHooklogicURL;
	}
	
	/**
	 * setter method for HooklogicURL.
	 * @param pHooklogicURL HooklogicURL.
	 */
	public void setHooklogicURL(String pHooklogicURL) {
		this.mHooklogicURL = pHooklogicURL;
	}
	/**
	 * getter method for Taxonomy.
	 * @return mTaxonomy
	 */
	public String getTaxonomy() {
		return mTaxonomy;
	}

	/**
	 * setter method for Taxonomy.
	 * @param pTaxonomy Taxonomy
	 */
	public void setTaxonomy(String pTaxonomy) {
		this.mTaxonomy = pTaxonomy;
	}
	/**
	 * getter method for Hlpt.
	 * @return mHlpt
	 */
	public String getHlpt() {
		return mHlpt;
	}

	/**
	 * setter method for Hlpt.
	 * @param pHlpt Hlpt
	 */
	public void setHlpt(String pHlpt) {
		this.mHlpt = pHlpt;
	}
	/**
	 * getter method for Pubpagetype.
	 * @return mPubpagetype
	 */
	public String getPubpagetype() {
		return mPubpagetype;
	}

	/**
	 * setter method for Pubpagetype.
	 * @param pPubpagetype Pubpagetype
	 */
	public void setPubpagetype(String pPubpagetype) {
		this.mPubpagetype = pPubpagetype;
	}
	/**
	 * getter method for Minorganic.
	 * @return mMinorganic
	 */
	public String getMinorganic() {
		return mMinorganic;
	}

	/**
	 * setter method for Minorganic.
	 * @param pMinorganic Minorganic
	 */
	public void setMinorganic(String pMinorganic) {
		this.mMinorganic = pMinorganic;
	}
	/**
	 * getter method for Minads.
	 * @return mMinads
	 */
	public String getMinads() {
		return mMinads;
	}

	/**
	 * setter method for Minads.
	 * @param pMinads Minads
	 */
	public void setMinads(String pMinads) {
		this.mMinads = pMinads;
	}
	/**
	 * getter method for Maxads.
	 * @return mMaxads
	 */
	public String getMaxads() {
		return mMaxads;
	}

	/**
	 * setter method for Maxads.
	 * @param pMaxads Maxads
	 */
	public void setMaxads(String pMaxads) {
		this.mMaxads = pMaxads;
	}
	/**
	 * getter method for Pcount.
	 * @return mPcount
	 */
	public String getPcount() {
		return mPcount;
	}

	/**
	 * setter method for Pcount.
	 * @param pPcount Pcount
	 */
	public void setPcount(String pPcount) {
		this.mPcount = pPcount;
	}
	/**
	 * getter method for Pgsize.
	 * @return mPgsize
	 */
	public String getPgsize() {
		return mPgsize;
	}

	/**
	 * setter method for Pgsize.
	 * @param pPgsize Pgsize
	 */
	public void setPgsize(String pPgsize) {
		this.mPgsize = pPgsize;
	}
	/**
	 * getter method for Pgn.
	 * @return mPgn
	 */
	public String getPgn() {
		return mPgn;
	}

	/**
	 * setter method for Pgn.
	 * @param pPgn Pgn
	 */
	public void setPgn(String pPgn) {
		this.mPgn = pPgn;
	}
	/**
	 * getter method for Sort.
	 * @return mSort
	 */
	public String getSort() {
		return mSort;
	}

	/**
	 * setter method for Sort.
	 * @param pSort Sort
	 */
	public void setSort(String pSort) {
		this.mSort = pSort;
	}
	
	/*order*/
	
	/**
	 * getter method for HlptOrder.
	 * @return HlptOrder
	 */
	public String getHlptOrder() {
		return mHlptOrder;
	}

	/**
	 * setter method for HlptOrder.
	 * @param pHlptOrder HlptOrder
	 */
	public void setHlptOrder(String pHlptOrder) {
		this.mHlptOrder = pHlptOrder;
	}
	
	/**
	 * getter method for SkuOrder.
	 * @return SkuOrder
	 */
	public String getSkuOrder() {
		return mSkuOrder;
	}

	/**
	 * setter method for SkuOrder.
	 * @param pSkuOrder SkuOrder
	 */
	public void setSkuOrder(String pSkuOrder) {
		this.mSkuOrder = pSkuOrder;
	}
	
	/**
	 * getter method for ParentSkuOrder.
	 * @return ParentSkuOrder
	 */
	public String getParentSkuOrder() {
		return mParentSkuOrder;
	}

	/**
	 * setter method for ParentSkuOrder.
	 * @param pParentSkuOrder ParentSkuOrder
	 */
	public void setParentSkuOrder(String pParentSkuOrder) {
		this.mParentSkuOrder = pParentSkuOrder;
	}
	
	/**
	 * getter method for PriceOrder.
	 * @return PriceOrder
	 */
	public String getPriceOrder() {
		return mPriceOrder;
	}

	/**
	 * setter method for PriceOrder.
	 * @param pPriceOrder PriceOrder
	 */
	public void setPriceOrder(String pPriceOrder) {
		this.mPriceOrder = pPriceOrder;
	}
	
	/**
	 * getter method for Quantity.
	 * @return Quantity
	 */
	public String getQuantity() {
		return mQuantity;
	}

	/**
	 * setter method for Quantity.
	 * @param pQuantity Quantity
	 */
	public void setQuantity(String pQuantity) {
		this.mQuantity = pQuantity;
	}
	
	/**
	 * getter method for OrderId.
	 * @return OrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * setter method for OrderId.
	 * @param pOrderId OrderId
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * getter method for regularPrice.
	 * @return the regularPrice
	 */
	public String getRegularPrice() {
		return mRegularPrice;
	}

	/**
	 * setter method for regularPrice.
	 * @param pRegularPrice the regularPrice to set
	 */
	public void setRegularPrice(String pRegularPrice) {
		mRegularPrice = pRegularPrice;
	}

	/**
	 * getter method for orderSubTotal.
	 * @return the orderSubTotal
	 */
	public String getOrderSubTotal() {
		return mOrderSubTotal;
	}

	/**
	 * setter method for orderSubTotal.
	 * @param pOrderSubTotal the orderSubTotal to set
	 */
	public void setOrderSubTotal(String pOrderSubTotal) {
		mOrderSubTotal = pOrderSubTotal;
	}

	/**
	 * getter method for orderpubPageType.
	 * @return the orderpubPageType
	 */
	public String getOrderpubPageType() {
		return mOrderpubPageType;
	}

	/**
	 * setter method for orderpubPageType.
	 * @param pOrderpubPageType the orderpubPageType to set
	 */
	public void setOrderpubPageType(String pOrderpubPageType) {
		mOrderpubPageType = pOrderpubPageType;
	}

	/**
	 * getter method for addId.
	 * @return the addId
	 */
	public String getAddId() {
		return mAddId;
	}

	/**
	 * setter method for addId.
	 * @param pAddId the addId to set
	 */
	public void setAddId(String pAddId) {
		mAddId = pAddId;
	}
}
