package com.tru.proxy;

import atg.nucleus.GenericService;

/**
 * This class holds proxy details
 * 
 * @author Sudheer Guduru
 *
 */
public class ProxyConfiguration extends GenericService {
	
	/**
	 * This property holds the ProxyName
	 */
	private String mProxyName;

	/**
	 * This property holds the ProxyPort
	 */
	private int mProxyPort;

	/**
	 * This property holds the ProxyUsername
	 */
	private String mProxyUsername;

	/**
	 * This property holds the ProxyPassword
	 */
	private String mProxyPassword;

	/**
	 * This property holds the ProxyDomain
	 */
	private String mProxyDomain;

	/**
	 * This property holds the ProxyEnabled 
	 */
	private boolean mProxyEnabled;
	
	/**
	 * This property holds the ProxyScheme
	 */
	private String mProxyScheme;

	/**
	 * @return the proxyName
	 */
	public String getProxyName() {
		return mProxyName;
	}

	/**
	 * @param pProxyName
	 *            the proxyName to set
	 */
	public void setProxyName(String pProxyName) {
		mProxyName = pProxyName;
	}

	/**
	 * @return the proxyPort
	 */
	public int getProxyPort() {
		return mProxyPort;
	}

	/**
	 * @param pProxyPort
	 *            the proxyPort to set
	 */
	public void setProxyPort(int pProxyPort) {
		mProxyPort = pProxyPort;
	}

	/**
	 * @return the proxyUsername
	 */
	public String getProxyUsername() {
		return mProxyUsername;
	}

	/**
	 * @param pProxyUsername
	 *            the proxyUsername to set
	 */
	public void setProxyUsername(String pProxyUsername) {
		mProxyUsername = pProxyUsername;
	}

	/**
	 * @return the proxyPassword
	 */
	public String getProxyPassword() {
		return mProxyPassword;
	}

	/**
	 * @param pProxyPassword
	 *            the proxyPassword to set
	 */
	public void setProxyPassword(String pProxyPassword) {
		mProxyPassword = pProxyPassword;
	}

	/**
	 * @return the proxyDomain
	 */
	public String getProxyDomain() {
		return mProxyDomain;
	}

	/**
	 * @param pProxyDomain
	 *            the proxyDomain to set
	 */
	public void setProxyDomain(String pProxyDomain) {
		mProxyDomain = pProxyDomain;
	}

	/**
	 * @return the proxyEnabled
	 */
	public boolean isProxyEnabled() {
		return mProxyEnabled;
	}

	/**
	 * @param pProxyEnabled
	 *            the proxyEnabled to set
	 */
	public void setProxyEnabled(boolean pProxyEnabled) {
		mProxyEnabled = pProxyEnabled;
	}

	/**
	 * @return the proxyScheme
	 */
	public String getProxyScheme() {
		return mProxyScheme;
	}

	/**
	 * @param pProxyScheme the proxyScheme to set
	 */
	public void setProxyScheme(String pProxyScheme) {
		mProxyScheme = pProxyScheme;
	}
	
}
