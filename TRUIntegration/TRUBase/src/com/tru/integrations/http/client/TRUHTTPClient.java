package com.tru.integrations.http.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.constants.TRUConstants;
import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.proxy.ProxyConfiguration;

/**
 * This is created for the HttpClient.
 * @author SGuduru
 * @version 1.0
 *
 */
public class TRUHTTPClient extends GenericService  {

	private ProxyConfiguration mProxyConfiguration;

	/**
	 * @return the proxyConfiguration
	 */
	public ProxyConfiguration getProxyConfiguration() {
		return mProxyConfiguration;
	}
	/**
	 * @param pProxyConfiguration the proxyConfiguration to set
	 */
	public void setProxyConfiguration(ProxyConfiguration pProxyConfiguration) {
		mProxyConfiguration = pProxyConfiguration;
	}
	
	/**
	 * This method is used to execute HTTP POST Connection requests.
	 *
	 * @param pTargetUrl  target API URL
	 * @param pPayload request Pay load
	 * @param pHeaderMap  the map to have request header.
	 * @param pIsAuthRequired  the flag to tell whether Authentication required or not.
	 * @param pTimeOut  connection timeout.
	 * @param pRequestTimeout request timeout.
	 * @param pIsProxyRequired  the flag to tell whether proxy required or not.
	 * @return response  API response string
	 * @throws TRUIntegrationException    if any integration errors.
	 */
	public String executeHttpPost(String pTargetUrl, String pPayload, Map<String, String> pHeaderMap, 
			boolean pIsAuthRequired, int pTimeOut, int pRequestTimeout, boolean pIsProxyRequired) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: execute() :: START");
		}
		String response = null;
		int responseCode = TRUConstants.NUMERIC_ZERO;
		try {
			final MultiThreadedHttpConnectionManager connManager = new MultiThreadedHttpConnectionManager();
			HttpClient httpClient = new HttpClient(connManager);
			PostMethod httpMethod = new PostMethod(pTargetUrl);
			HostConfiguration config = new HostConfiguration();

			String proxyIPAddress = getProxyConfiguration().getProxyName();
			int proxyPort = getProxyConfiguration().getProxyPort();
			if (isLoggingDebug()) {
				vlogDebug("TRUHTTPClient.execute, isAuthRequired:{0} proxyIPAddress:{1} proxyPort:{2}",
						pIsAuthRequired, proxyIPAddress, proxyPort);
			}
			if (pIsProxyRequired && getProxyConfiguration().isProxyEnabled() && proxyIPAddress != null) {
				URI uri = new URI(pTargetUrl);
				if (isLoggingDebug()) {
					vlogDebug("TRUHTTPClient.executeHttpRequest,host:{0}port:{1}",uri.getHost(), uri.getPort());
				}
				config.setHost(uri.getHost(), uri.getPort());
				config.setProxy(proxyIPAddress, Integer.valueOf(proxyPort));
				httpClient.setHostConfiguration(config);
			}
			httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(pTimeOut);
			//            if(requestTimeout != 0 || getResTimeOut() != 0) {
			//                httpClient.getHttpConnectionManager().getParams()
			//                .setSoTimeout(pRequestTimeout != 0 ? pRequestTimeout : getResTimeOut());
			//            }

			httpMethod.setDoAuthentication(pIsAuthRequired);
			if(pHeaderMap != null && !pHeaderMap.isEmpty()){
				for(String key : pHeaderMap.keySet()){
					httpMethod.addRequestHeader(key, pHeaderMap.get(key));
				}	
			}
			StringRequestEntity requestEntity = new StringRequestEntity(pPayload, TRUConstants.CONTENT_TYPE_VALUE, TRUConstants.CHAR_ENCODE_VALUE);
			httpMethod.setRequestEntity(requestEntity);
			responseCode = httpClient.executeMethod(httpMethod);
			response = httpMethod.getResponseBodyAsString();
			if (isLoggingDebug()) {
				vlogDebug("TRUHTTPClient.execute(),responseCode:{0}response:{1}",responseCode, response);
			}
		} catch (UnsupportedEncodingException use) {
			if (isLoggingError()) {
				vlogError("TRUHTTPClient.execute(), UnsupportedEncodingException:{0}", use);
			}
			throw new TRUIntegrationException(use);
		} catch (IOException ex) {
			if (isLoggingDebug()) {
				vlogError("TRUHTTPClient.execute(), IOException:{0}", ex);
			}
			throw new TRUIntegrationException(ex);
		} catch (URISyntaxException use) {
			if (isLoggingError()) {
				vlogError("TRUHTTPClient.execute(), URISyntaxException:{0}", use);
			}
			throw new TRUIntegrationException(use);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUHTTPClient.execute, responseCode:{0} response:{1}", responseCode, response);
		}
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: execute() :: END");
		}
		return response;
	}
	
	/**
	 * This method is used to execute HTTP POST Connection requests.
	 *
	 * @param pTargetUrl  target API URL
	 * @param pPayload request Pay load
	 * @param pHeaderMap  the map to have request header.
	 * @param pIsAuthRequired  the flag to tell whether Authentication required or not.
	 * @param pTimeOut  connection timeout.
	 * @param pRequestTimeout request timeout.
	 * @param pIsProxyRequired  the flag to tell whether proxy required or not.
	 * @return response  API response string
	 * @throws TRUIntegrationException    if any integration errors.
	 */
	public String executeHttpPut(String pTargetUrl, String pPayload, Map<String, String> pHeaderMap, boolean pIsAuthRequired, 
			int pTimeOut, int pRequestTimeout, boolean pIsProxyRequired) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUPDPHTTPClient :: execute() :: START");
		}
		String response = null;
		int responseCode = TRUConstants.NUMERIC_ZERO;
		try {
			if (isLoggingDebug()) {
				vlogDebug("TRUPDPHTTPClient.execute, pTargetUrl{0} pPayload{1}",pTargetUrl, pPayload);
			}
			final MultiThreadedHttpConnectionManager connManager = new MultiThreadedHttpConnectionManager();
			HttpClient httpClient = new HttpClient(connManager);
			PutMethod httpMethod = new PutMethod(pTargetUrl);
			HostConfiguration config = new HostConfiguration();

			String proxyIPAddress = getProxyConfiguration().getProxyName();
			int proxyPort = getProxyConfiguration().getProxyPort();
			if (isLoggingDebug()) {
				vlogDebug("TRUPDPHTTPClient.execute, isAuthRequired:{0} proxyIPAddress:{1} proxyPort:{2}",
						pIsAuthRequired, proxyIPAddress, proxyPort);
			}
			if (pIsProxyRequired && getProxyConfiguration().isProxyEnabled() && proxyIPAddress != null) {
				URI uri = new URI(pTargetUrl);
				if (isLoggingDebug()) {
					vlogDebug("TRUPDPHTTPClient.executeHttpRequest,host:{0}port:{1}",uri.getHost(), uri.getPort());
				}
				config.setHost(uri.getHost(), uri.getPort());
				config.setProxy(proxyIPAddress, Integer.valueOf(proxyPort));
				httpClient.setHostConfiguration(config);
			}
			httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(pTimeOut);
			//            if(requestTimeout != 0 || getResTimeOut() != 0) {
			//                httpClient.getHttpConnectionManager().getParams()
			//                .setSoTimeout(pRequestTimeout != 0 ? pRequestTimeout : getResTimeOut());
			//            }

			httpMethod.setDoAuthentication(pIsAuthRequired);
			if(pHeaderMap != null && !pHeaderMap.isEmpty()){
				for(String key : pHeaderMap.keySet()){
					httpMethod.addRequestHeader(key, pHeaderMap.get(key));
				}	
			}
			StringRequestEntity requestEntity = new StringRequestEntity(pPayload, TRUConstants.CONTENT_TYPE_VALUE, TRUConstants.CHAR_ENCODE_VALUE);
			httpMethod.setRequestEntity(requestEntity);
			responseCode = httpClient.executeMethod(httpMethod);
			response = httpMethod.getResponseBodyAsString();
			if (isLoggingDebug()) {
				vlogDebug("TRUPDPHTTPClient.execute(),responseCode:{0}response:{1}",responseCode, response);
			}
		} catch (UnsupportedEncodingException use) {
			if (isLoggingError()) {
				vlogError("TRUPDPHTTPClient.execute(), UnsupportedEncodingException:{0}", use);
			}
			throw new TRUIntegrationException(use);
		} catch (IOException ex) {
			if (isLoggingDebug()) {
				vlogError("TRUPDPHTTPClient.execute(), IOException:{0}", ex);
			}
			throw new TRUIntegrationException(ex);
		} catch (URISyntaxException use) {
			if (isLoggingError()) {
				vlogError("TRUPDPHTTPClient.execute(), URISyntaxException:{0}", use);
			}
			throw new TRUIntegrationException(use);
		}
		if (isLoggingDebug()) {
			vlogDebug("TRUPDPHTTPClient.execute, responseCode:{0} response:{1}", responseCode, response);
		}
		if (isLoggingDebug()) {
			logDebug("TRUPDPHTTPClient :: execute() :: END");
		}
		return response;
	}

	/**
	 * This method is used to execute HTTP POST Connection requests.
	 * This is implementation of Apache Components 4.5.1
	 * 
	 * @param pTargetUrl  target API URL
	 * @param pPayload request Pay load
	 * @param pHeaderMap  the map to have request header.
	 * @param pIsAuthRequired  the flag to tell whether Authentication required or not.
	 * @param pTimeOut  connection timeout.
	 * @param pRequestTimeout request timeout.
	 * @param pIsProxyRequired  the flag to tell whether proxy required or not.
	 * @return response  API response string
	 * @throws TRUIntegrationException    if any integration errors.
	 */
	public String executeHttpPostRequest(String pTargetUrl, String pPayload, Map<String, String> pHeaderMap, boolean pIsAuthRequired, 
			int pTimeOut, int pRequestTimeout, boolean pIsProxyRequired) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: executeHttpRequest() :: START");
		}
		String response = null;
		int responseCode = TRUConstants.NUMERIC_ZERO;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;

		try {
			SSLContext sslcontext = SSLContexts.createDefault();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { TRUConstants.TLSV_ONE, 
					TRUConstants.TL_SV, TRUConstants.TLS_V, TRUConstants.SSLV_THREE }, null, 
					SSLConnectionSocketFactory.getDefaultHostnameVerifier());

			httpClient = HttpClients.createDefault();
			HttpHost target = new HttpHost(pTargetUrl);

			// Creating the HTTP Post and adding the header parameters
			HttpPost request = new HttpPost(target.getHostName());
			if(pHeaderMap != null && !pHeaderMap.isEmpty()){
				for(String key : pHeaderMap.keySet()){
					request.addHeader(key, pHeaderMap.get(key));
				}	
			}
			request.setEntity(new StringEntity(pPayload));

			RequestConfig config = RequestConfig.custom().build();
			if (pIsProxyRequired && getProxyConfiguration().isProxyEnabled()) {
				HttpHost proxy = null;
				if(!StringUtils.isBlank(getProxyConfiguration().getProxyScheme())) {
					proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort(), 
							getProxyConfiguration().getProxyScheme());
				} else {
					proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort());
				}
				config = RequestConfig.copy(config).setProxy(proxy).build();
				httpClient = HttpClients.custom().setProxy(proxy).setSSLSocketFactory(sslsf).build();
			} else {
				httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			}
			config = RequestConfig.copy(config).setAuthenticationEnabled(pIsAuthRequired).build();
			if (pTimeOut != 0) {
				config = RequestConfig.copy(config).setConnectionRequestTimeout(pRequestTimeout).build();
				config = RequestConfig.copy(config).setConnectTimeout(pTimeOut).build();
			}

			request.setConfig(config);

			if (isLoggingDebug()) {
				logDebug("Before executing http call");
				vlogDebug("Request : {0}" , request);
				vlogDebug("Config Parameters for Request {0}, {1}, {2}, {3}", 
						config.getProxy(), config.isAuthenticationEnabled(), 
						config.getConnectionRequestTimeout(), target.getHostName());
			}
			httpResponse = httpClient.execute(request);
			if (isLoggingDebug()) {
				logDebug("After executing http call");
				vlogDebug("Response : {0}" , httpResponse);
			}
			if (httpResponse !=  null) {
				responseCode = httpResponse.getStatusLine().getStatusCode();
				response = EntityUtils.toString(httpResponse.getEntity());
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUHTTPClient.executeHttpRequest, responseCode:{0} response:{1}", responseCode, response);
			}
		} catch (ClientProtocolException cpe) {
			if (isLoggingError()) {
				logError("ClientProtocolException Exception while processing Request", cpe);
			}
			throw new TRUIntegrationException(cpe);
		} catch (IOException ie) {
			if (isLoggingError()) {
				logError("IOException Exception while processing Request", ie);
			}
			throw new TRUIntegrationException(ie);
		}  finally {
			try {
				if (httpClient != null) {
					httpClient.close();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException ie) {
				if (isLoggingError()) {
					logError("IO Exception while closing HttpClient or Http Response.", ie);
				}
				throw new TRUIntegrationException(ie);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: executeHttpRequest() :: END");
		}
		return response;
	}


	/**
	 * This method is used to execute HTTP POST Connection requests.
	 * This is implementation of Apache Components 4.5.1
	 * 
	 * @param pTargetUrl  target API URL
	 * @param pHeaderMap  the map to have request header.
	 * @param pIsAuthRequired  the flag to tell whether Authentication required or not.
	 * @param pTimeOut  connection timeout.
	 * @param pRequestTimeout request timeout.
	 * @param pIsProxyRequired  the flag to tell whether proxy required or not.
	 * @return response  API response string
	 * @throws TRUIntegrationException    if any integration errors.
	 */
	public String executeHttpGetRequest(String pTargetUrl, Map<String, String> pHeaderMap, boolean pIsAuthRequired, 
			int pTimeOut, int pRequestTimeout, boolean pIsProxyRequired) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: executeHttpRequest() :: START");
		}
		String response = null;
		int responseCode = TRUConstants.NUMERIC_ZERO;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;

		try {
			httpClient = HttpClients.createDefault();

			// Creating the HTTP Get and adding the header parameters
			HttpGet request = new HttpGet(pTargetUrl);
			if(pHeaderMap != null && !pHeaderMap.isEmpty()){
				for(String key : pHeaderMap.keySet()){
					request.addHeader(key, pHeaderMap.get(key));
				}	
			}

			RequestConfig config = RequestConfig.custom().build();
			if (pIsProxyRequired && getProxyConfiguration().isProxyEnabled()) {
				HttpHost proxy = null;
				if(!StringUtils.isBlank(getProxyConfiguration().getProxyScheme())) {
					proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort(), 
							getProxyConfiguration().getProxyScheme());
				} else {
					proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort());
				}
				config = RequestConfig.copy(config).setProxy(proxy).build();
			}
			config = RequestConfig.copy(config).setAuthenticationEnabled(pIsAuthRequired).build();
			if (pTimeOut != 0) {
				config = RequestConfig.copy(config).setConnectionRequestTimeout(pRequestTimeout).build();
				config = RequestConfig.copy(config).setConnectTimeout(pTimeOut).build();
			}

			request.setConfig(config);

			if (isLoggingDebug()) {
				logDebug("Before executing http call");
				vlogDebug("Request : {0}" , request);
				vlogDebug("Config Parameters for Request {0}, {1}, {2}", 
						config.getProxy(), config.isAuthenticationEnabled(), 
						config.getConnectionRequestTimeout());
			}

			httpResponse = httpClient.execute(request);
			if (isLoggingDebug()) {
				logDebug("After executing http call");
				vlogDebug("Response : {0}" , httpResponse);
			}
			if (httpResponse !=  null) {
				responseCode = httpResponse.getStatusLine().getStatusCode();
				//response = EntityUtils.toString(httpResponse.getEntity());
				response = EntityUtils.toString(httpResponse.getEntity(), TRUConstants.CHAR_ENCODE_VALUE);
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUHTTPClient.executeHttpRequest, responseCode:{0} response:{1}", responseCode, response);
			}
		} catch (ClientProtocolException cpe) {
			if (isLoggingError()) {
				logError("ClientProtocolException Exception while processing Request", cpe);
			}
			throw new TRUIntegrationException(cpe);
		} catch (IOException ie) {
			if (isLoggingError()) {
				logError("IOException Exception while processing Request", ie);
			}
			throw new TRUIntegrationException(ie);
		}  finally {
			try {
				if (httpClient != null) {
					httpClient.close();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException ie) {
				if (isLoggingError()) {
					logError("IO Exception while closing HttpClient or Http Response.", ie);
				}
				throw new TRUIntegrationException(ie);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUHTTPClient :: executeHttpRequest() :: END");
		}
		return response;
	}
	
	/**
	 * This method is used to execute HTTP POST Connection requests.
	 * This is implementation of Apache Components 4.5.1
	 * 
	 * @param pTargetUrl  target API URL
	 * @param pHeaderMap  the map to have request header.
	 * @param pIsAuthRequired  the flag to tell whether Authentication required or not.
	 * @param pTimeOut  connection timeout.
	 * @param pRequestTimeout request timeout.
	 * @param pIsProxyRequired  the flag to tell whether proxy required or not.
	 * @return response  API response string
	 * @throws TRUIntegrationException    if any integration errors.
	 */
	public String executeHttpGetPDPRequest(String pTargetUrl, Map<String, String> pHeaderMap, boolean pIsAuthRequired, int pTimeOut, 
			int pRequestTimeout, boolean pIsProxyRequired) throws TRUIntegrationException {
		if (isLoggingDebug()) {
			logDebug("TRUPDPHTTPClient :: executeHttpRequest() :: START");
		}
		String response = null;
		int responseCode = TRUConstants.NUMERIC_ZERO;
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;

		try {
			if (isLoggingDebug()) {
				vlogDebug("TRUPDPHTTPClient.execute, pTargetUrl{0}",pTargetUrl);
			}
			httpClient = HttpClients.createDefault();

			// Creating the HTTP Get and adding the header parameters
			HttpGet request = new HttpGet(pTargetUrl);
			if(pHeaderMap != null && !pHeaderMap.isEmpty()){
				for(String key : pHeaderMap.keySet()){
					request.addHeader(key, pHeaderMap.get(key));
				}	
			}

			RequestConfig config = RequestConfig.custom().build();
				if (pIsProxyRequired && getProxyConfiguration().isProxyEnabled()) {
					HttpHost proxy = null;
					if(!StringUtils.isBlank(getProxyConfiguration().getProxyScheme())) {
						proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort(), 
								getProxyConfiguration().getProxyScheme());
					} else {
						proxy = new HttpHost(getProxyConfiguration().getProxyName(), getProxyConfiguration().getProxyPort());
					}
					config = RequestConfig.copy(config).setProxy(proxy).build();
				}
				config = RequestConfig.copy(config).setAuthenticationEnabled(pIsAuthRequired).build();
			if (pTimeOut != 0) {
				config = RequestConfig.copy(config).setConnectionRequestTimeout(pRequestTimeout).build();
				config = RequestConfig.copy(config).setConnectTimeout(pTimeOut).build();
			}

			request.setConfig(config);

			if (isLoggingDebug()) {
				logDebug("Before executing http call");
				vlogDebug("Request : {0}" , request);
				vlogDebug("Config Parameters for Request {0}, {1}, {2}", 
						config.getProxy(), config.isAuthenticationEnabled(), 
						config.getConnectionRequestTimeout());
			}

			httpResponse = httpClient.execute(request);
			if (isLoggingDebug()) {
				logDebug("After executing http call");
				vlogDebug("Response : {0}" , httpResponse);
			}
			if (httpResponse !=  null) {
				responseCode = httpResponse.getStatusLine().getStatusCode();
				response = EntityUtils.toString(httpResponse.getEntity());
			}
			if (isLoggingDebug()) {
				vlogDebug("TRUPDPHTTPClient.executeHttpRequest, responseCode:{0} response:{1}", responseCode, response);
			}
		} catch (ClientProtocolException cpe) {
			if (isLoggingError()) {
				logError("ClientProtocolException Exception while processing Request", cpe);
			}
			throw new TRUIntegrationException(cpe);
		} catch (IOException ie) {
			if (isLoggingError()) {
				logError("IOException Exception while processing Request", ie);
			}
			throw new TRUIntegrationException(ie);
		}  finally {
			try {
				if (httpClient != null) {
					httpClient.close();
				}
				if (httpResponse != null) {
					httpResponse.close();
				}
			} catch (IOException ie) {
				if (isLoggingError()) {
					logError("IO Exception while closing HttpClient or Http Response.", ie);
				}
				throw new TRUIntegrationException(ie);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUPDPHTTPClient :: executeHttpRequest() :: END");
		}
		return response;
	}
}
