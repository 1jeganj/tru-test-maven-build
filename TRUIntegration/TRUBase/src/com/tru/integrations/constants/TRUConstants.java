package com.tru.integrations.constants;

/**
 * This class is used to define the constants.
 * 
 * @author Sudheer Guduru
 * @version 1.0
 */
public class TRUConstants {
	
	public static final int NUMERIC_ZERO = 0;
	public static final int NUMERIC_ONE = 1;
	public static final int NUMERIC_TWO = 2;
	public static final int NUMERIC_THREE = 3;
	public static final int NUMERIC_FOUR = 4;
	public static final int NUMERIC_FIVE = 5;
	
	public static final String EQUAL = "=";
	public static final String PIPELINE = "|";
	public static final String COMMA = ",";
	public static final String HYPEN = "-";
	public static final String AMPERSAND = "&";
	public static final String DOT = ".";
	public static final String SPACE = " ";
	public static final String QUESTION_MARK = "?";
	
	public static final String AMPERSAND_HTML = "%26";
	public static final String SPACE_HTML = "%20";
	
	public static final String CONTENT_TYPE_VALUE = "application/json";
	
	public static final String CHAR_ENCODE_VALUE = "UTF-8";
	
	public static final String TLSV_ONE = "TLSv1";
	
	public static final String TL_SV = "TLSv1.1";
	
	public static final String TLS_V = "TLSv1.2";
	
	public static final String SSLV_THREE = "SSLv3";
	
	public static final int SUCCESS = 200; 
	

}
