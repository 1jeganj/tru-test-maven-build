package com.tru.integrations.exception;


/**
 * This exception class used to thrown for error caused during SSL connections.
 * 
 * @author PA
 * @version 1.0
 */
public class TRUSSLConfigurationException extends Exception {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -2345834567387654563L;

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 */
	public TRUSSLConfigurationException(String pMessage) {
		super(pMessage);
	}

	/**
	 * This method is used for get the exception details.
	 * 
	 * @param pMessage - String
	 * @param pSourceException - Throwable
	 */
	public TRUSSLConfigurationException(String pMessage, Throwable pSourceException) {
		super(pMessage, pSourceException);
	}
}
