package com.tru.integrations.util;
 

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.transaction.TransactionManager;

import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import com.tru.integrations.constants.TRUConstants;
import com.tru.integrations.vo.TRUMessageAuditVO;

/**
 * This is the Service class , provides Service method for for Coupon Lookup. 
 * After constructing the URL.
 * @author PA
 * @version 1.0. 
 */

public class TRUAuditMessageUtil extends GenericService{	
	/**
     * Reference to the Transaction Manager.
     */
	private TransactionManager mTransactionManager;
	
	/** The Audit sucn message into db. */
	private boolean mAuditSUCNMessageIntoDB;
	
	/** The m enable log req res to db. */
	private boolean mEnableLogReqResToDB;
	
	/** The Item descriptor audit message. */
	private String mItemDescriptorAuditMessage;
	
	/** The Message type. */
	private String mMessageType;
	
	/** The Message data. */
	private String mMessageData;
	
	/** The Audit date. */
	private String mAuditDate;
	
	/** The Order id. */
	private String mOrderId;
	
	/**Added only for testing audit message retreival : Start*/
	
	/** The Constant STRFORMAT1. */
	private static final String STRFORMAT1 ="dd-MM-yy";
	
	/** The Constant STRFORMAT2. */
	private static final String STRFORMAT2 ="yyyy-MM-dd HH:mm:ss zzz";
	
	/** The Constant RQLSTAT1. */
	private static final String RQLSTAT1 ="(auditDate > datetime(\"";
	
	/** The Constant RQLSTAT2. */
	private static final String RQLSTAT2 ="\")) AND (auditDate < datetime(\"";
	
	/** The Constant RQLSTAT3. */
	private static final String RQLSTAT3 ="\"))";
	
	/**Added only for testing audit message retreival : End*/
	
	/**
	 * @return the auditSUCNMessageIntoDB
	 */
	public boolean isAuditSUCNMessageIntoDB() {
		return mAuditSUCNMessageIntoDB;
	}

	/**
	 * @param pAuditSUCNMessageIntoDB the auditSUCNMessageIntoDB to set
	 */
	public void setAuditSUCNMessageIntoDB(boolean pAuditSUCNMessageIntoDB) {
		mAuditSUCNMessageIntoDB = pAuditSUCNMessageIntoDB;
	}

	
	
	/**
	 * @return the itemDescriptorAuditMessage
	 */
	public String getItemDescriptorAuditMessage() {
		return mItemDescriptorAuditMessage;
	}

	/**
	 * @param pItemDescriptorAuditMessage the itemDescriptorAuditMessage to set
	 */
	public void setItemDescriptorAuditMessage(String pItemDescriptorAuditMessage) {
		mItemDescriptorAuditMessage = pItemDescriptorAuditMessage;
	}

	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return mMessageType;
	}

	/**
	 * @param pMessageType the messageType to set
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}

	/**
	 * @return the messageData
	 */
	public String getMessageData() {
		return mMessageData;
	}

	/**
	 * @param pMessageData the messageData to set
	 */
	public void setMessageData(String pMessageData) {
		mMessageData = pMessageData;
	}

	/**
	 * @return the auditDate
	 */
	public String getAuditDate() {
		return mAuditDate;
	}

	/**
	 * @param pAuditDate the auditDate to set
	 */
	public void setAuditDate(String pAuditDate) {
		mAuditDate = pAuditDate;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * @param pTransactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	/** The Message content repository. */
	private Repository mMessageContentRepository;
	
	/**
	 * @return the messageContentRepository
	 */
	public Repository getMessageContentRepository() {
		return mMessageContentRepository;
	}

	/**
	 * @param pMessageContentRepository the messageContentRepository to set
	 */
	public void setMessageContentRepository(Repository pMessageContentRepository) {
		mMessageContentRepository = pMessageContentRepository;
	}
	
	/**
	 * This method is used to audit the messages that are sent/received as part of SUCN.
	 *
	 * @param pMessageAuditVO - TRUMessageAuditVO
	 */
	public void auditMessage(TRUMessageAuditVO pMessageAuditVO) {
		vlogDebug("TRUOrderManager..auditMessage() orderid:{0}", pMessageAuditVO.getOrderId());
		MutableRepository messageContentRepository = (MutableRepository) getMessageContentRepository();
		TransactionManager transactionManager = getTransactionManager();
		TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
		Boolean doRollBack = Boolean.FALSE;
		try {
			if (transactionManager != null) {
				transactionDemarcation.begin(transactionManager, TransactionDemarcation.REQUIRES_NEW);
				MutableRepositoryItem auditMessageItem = messageContentRepository.createItem(getItemDescriptorAuditMessage());
				auditMessageItem.setPropertyValue(getOrderId(), pMessageAuditVO.getOrderId());
				auditMessageItem.setPropertyValue(getMessageType(), pMessageAuditVO.getMessageType());
				auditMessageItem.setPropertyValue(getMessageData(), pMessageAuditVO.getMessageData());
				auditMessageItem.setPropertyValue(getAuditDate(), pMessageAuditVO.getAuditDate());
				messageContentRepository.addItem(auditMessageItem);
			}
		} catch (RepositoryException repExp) {
			doRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("RepositoryException in TRUOrderManager..auditMessage() OrderId:{0} repExp:{1}",pMessageAuditVO.getOrderId(), repExp);
			}
		} catch (TransactionDemarcationException e) {
			doRollBack = Boolean.TRUE;
			if (isLoggingError()) {
				vlogError("TransactionDemarcationException in TRUOrderManager..auditMessage() OrderId:{0} TransactionDemarcationException:{1}",
						pMessageAuditVO.getOrderId(), e);
			}
		}  finally {
			try {
				if (transactionDemarcation != null) {
					if(isLoggingInfo()){
						logInfo("Transaction to be roll back in auditMessage");
					}					
					transactionDemarcation.end(doRollBack);
				}   
			} catch (TransactionDemarcationException demarcationException) {
				if (isLoggingError()) {
					vlogError("Transaction demarcation Exception TRUOrderManager..auditMessage() OrderId:{0} tranDmrExp:{1}",
							pMessageAuditVO.getOrderId(),demarcationException);
				}
			}
		}
	}
	
	/**
	 * Retreive audit message by order id.
	 *
	 * @param pOrderId the order id
	 * @return the repository item[]
	 */
	public RepositoryItem[] retreiveAuditMessageByOrderId(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("TRUAuditMessageUtil (retreiveAuditMessageByOrderId) , START");
			vlogDebug("Order Id : {0}", pOrderId);
		}
		RepositoryItem [] auditMessageItems = null;
		if (StringUtils.isBlank(pOrderId)) {
			return null;
		}
		try {
			final RepositoryView auditMessageView = getMessageContentRepository().getView(getItemDescriptorAuditMessage());
			final QueryBuilder queryBuilder = auditMessageView.getQueryBuilder();
			final QueryExpression orderIdPropExpression = queryBuilder.createPropertyQueryExpression(getOrderId());
			final QueryExpression orderIdValueExpression = queryBuilder.createConstantQueryExpression(pOrderId);
			//final Query containsQuery = queryBuilder.createComparisonQuery(orderIdPropExpression, orderIdValueExpression, QueryBuilder.EQUALS);
			Query containsQuery = queryBuilder.createPatternMatchQuery(orderIdPropExpression, orderIdValueExpression, QueryBuilder.CONTAINS);
			//Get items
			auditMessageItems = auditMessageView.executeQuery(containsQuery);
			
		} catch (RepositoryException exe) {
			if (isLoggingError()) {
				logError("RepositoryException occurs while retreiving audit messages. ", exe);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUAuditMessageUtil (retreiveAuditMessageByOrderId) , END");
		}
		return auditMessageItems;
	}
	
	/**
	 * Retreive audit message by audit date.
	 *
	 * @param pAuditDate the audit date
	 * @return the repository item[]
	 */
	public RepositoryItem[] retreiveAuditMessageByAuditDate(String pAuditDate) {
		if (isLoggingDebug()) {
			logDebug("TRUAuditMessageUtil (retreiveAuditMessageByOrderId) , START");
			vlogDebug("Order Id : {0}", pAuditDate);
		}
		RepositoryItem [] auditMessageItems = null;
		if (StringUtils.isBlank(pAuditDate)) {
			return null;
		}
		try {
			final RepositoryView auditMessageView = getMessageContentRepository().getView(getItemDescriptorAuditMessage());
			DateFormat formatter= new SimpleDateFormat(STRFORMAT1);
			DateFormat reqFormatter= new SimpleDateFormat(STRFORMAT2);
		    Date date = formatter.parse(pAuditDate);
		    //To get next date
		    Calendar cal = Calendar.getInstance(); 
		    cal.setTime(date); 
		    cal.add(Calendar.DATE, TRUConstants.NUMERIC_ONE);
		    Date nextDate = cal.getTime();
		    //Get in String format 
		    String enteredDate = reqFormatter.format(date);
		    String nextEnteredDate = reqFormatter.format(nextDate);
		    /*Timestamp auditDate = new Timestamp(date.getTime());
			final QueryBuilder queryBuilder = auditMessageView.getQueryBuilder();
			final QueryExpression orderIdPropExpression = queryBuilder.createPropertyQueryExpression(getAuditDate());
			final QueryExpression orderIdValueExpression = queryBuilder.createConstantQueryExpression(auditDate);
			final Query containsQuery = queryBuilder.createComparisonQuery(orderIdPropExpression, orderIdValueExpression, QueryBuilder.EQUALS);
			//final Query containsQuery = queryBuilder.createPatternMatchQuery(orderIdPropExpression, orderIdValueExpression, QueryBuilder.CONTAINS);
			//Get items
			auditMessageItems = auditMessageView.executeQuery(containsQuery);*/	
			Object params[] = new Object[TRUConstants.NUMERIC_ONE];
			RqlStatement statement = RqlStatement.parseRqlStatement(RQLSTAT1+enteredDate+RQLSTAT2+nextEnteredDate+RQLSTAT3);
		    auditMessageItems =statement.executeQuery (auditMessageView, params);
			
		} catch (RepositoryException exe) {
			if (isLoggingError()) {
				logError("RepositoryException occurs while retreiving audit messages. ", exe);
			}
		} catch (ParseException exe) {
			if (isLoggingError()) {
				logError("RepositoryException occurs while retreiving audit messages. ", exe);
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUAuditMessageUtil (retreiveAuditMessageByOrderId) , END");
		}
		return auditMessageItems;
	}

	/**
	 * Checks if is enable log req res to db.
	 *
	 * @return the mEnableLogReqResToDB
	 */
	public boolean isEnableLogReqResToDB() {
		return mEnableLogReqResToDB;
	}

	/**
	 * Sets the enable log req res to db.
	 *
	 * @param pEnableLogReqResToDB the new enable log req res to db
	 */
	public void setEnableLogReqResToDB(boolean pEnableLogReqResToDB) {
		this.mEnableLogReqResToDB = pEnableLogReqResToDB;
	}
	
	
}
