/**
 * 
 */
package com.tru.integrations.util;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * The Class TestAuditMessageFormHandler.
 */
public class TestAuditMessageFormHandler extends GenericFormHandler {
	
	private static final String ORDER_ID = "orderId";
	/** The m option. */
	private String mOption;
	
	/** The m opt value. */
	private String mOptValue;
	
	/** The m audit items. */
	private RepositoryItem[] mAuditItems;
	
	/** The m message util. */
	private TRUAuditMessageUtil mMessageUtil;
	
	/**
	 * Handle get audit message.
	 *
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 */
	public boolean handleGetAuditMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (StringUtils.isNotBlank(getOptValue())) {
			if (ORDER_ID.equalsIgnoreCase(getOption())) {
				setAuditItems(getMessageUtil().retreiveAuditMessageByOrderId(getOptValue()));
			}else {
				setAuditItems(getMessageUtil().retreiveAuditMessageByAuditDate(getOptValue()));
			}
		}
		return true;
	}
	
	/**
	 * Gets the option.
	 *
	 * @return the option
	 */
	public String getOption() {
		return mOption;
	}
	
	/**
	 * Sets the option.
	 *
	 * @param pMOption the new option
	 */
	public void setOption(String pMOption) {
		mOption = pMOption;
	}
	
	/**
	 * Gets the opt value.
	 *
	 * @return the opt value
	 */
	public String getOptValue() {
		return mOptValue;
	}
	
	/**
	 * Sets the opt value.
	 *
	 * @param pMOptValue the new opt value
	 */
	public void setOptValue(String pMOptValue) {
		mOptValue = pMOptValue;
	}
	
	/**
	 * Gets the audit items.
	 *
	 * @return the audit items
	 */
	public RepositoryItem[] getAuditItems() {
		return mAuditItems;
	}
	
	/**
	 * Sets the audit items.
	 *
	 * @param pMAuditItems the new audit items
	 */
	public void setAuditItems(RepositoryItem[] pMAuditItems) {
		mAuditItems = pMAuditItems;
	}
	
	/**
	 * Gets the message util.
	 *
	 * @return the message util
	 */
	public TRUAuditMessageUtil getMessageUtil() {
		return mMessageUtil;
	}
	
	/**
	 * Sets the message util.
	 *
	 * @param pMMessageUtil the new message util
	 */
	public void setMessageUtil(TRUAuditMessageUtil pMMessageUtil) {
		mMessageUtil = pMMessageUtil;
	}
	
}
