package com.tru.integrations.vo;

import java.sql.Timestamp;

/**
 * This class holds the attributes used for auditing the SUCN service details.
 * 
 * @author PA
 * 
 */
public class TRUMessageAuditVO {
	/**
	 * Holds the order id.
	 */
	private String mOrderId;
	/**
	 * holds the message type
	 */
	private String mMessageType;
	/**
	 * Holds the message data
	 */
	private String mMessageData;
	/**
	 * Holds the audit date
	 */
	private Timestamp mAuditDate;

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId
	 *            the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return mMessageType;
	}

	/**
	 * @param pMessageType
	 *            the messageType to set
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}

	/**
	 * @return the messageData
	 */
	public String getMessageData() {
		return mMessageData;
	}

	/**
	 * @param pMessageData
	 *            the messageData to set
	 */
	public void setMessageData(String pMessageData) {
		mMessageData = pMessageData;
	}

	/**
	 * @return the auditDate
	 */
	public Timestamp getAuditDate() {
		return mAuditDate;
	}

	/**
	 * @param pAuditDate the auditDate to set
	 */
	public void setAuditDate(Timestamp pAuditDate) {
		mAuditDate = pAuditDate;
	}
}
