package com.tru.integrations.registry.util;

/**
 * This class holds the constants used in Registry Services
 * @author Sandeep Vishwakarma
 *
 */
public class TRURegistryConstant {
	
	public static final String SLASH = "/";
	public static final String ACCEPT = "accept";
	public static final String CONTENT_TYPE = "content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String COUNTRY_US = "US";
	public static final String LANGUAGE_EN = "en";
	public static final String RESPONSE_STATUS_SUCCESS = "SUCCESS";
	public static final String RESPONSE_STATUS_FAILURE = "FAILURE";
	public static final String PURCHASE_UPDATE_INDICATOR_I = "I";
}
