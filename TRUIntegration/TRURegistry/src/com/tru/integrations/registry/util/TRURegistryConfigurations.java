package com.tru.integrations.registry.util;

import atg.nucleus.GenericService;

/**
 * This class contains the configuration details of registry services.
 * @version 1.0
 * @author Professional Access
 */
public class TRURegistryConfigurations extends GenericService {

	private String mDetailsURl;
	private String mQueryParam;
	private boolean mAuthRequired;
	private int mTimeOut;
	private int mRequestTimeout;
	private boolean mProxyRequired;
	private String mUpdateItemURL;
	private String mAddItemURL;
	private String mWishlistDetailsURl;
	

	/**
	 * @return the wishlistDetailsURl
	 */
	public String getWishlistDetailsURl() {
		return mWishlistDetailsURl;
	}
	/**
	 * @param pWishlistDetailsURl the wishlistDetailsURl to set
	 */
	public void setWishlistDetailsURl(String pWishlistDetailsURl) {
		mWishlistDetailsURl = pWishlistDetailsURl;
	}
	
	/**
	 * @return the detailsURl
	 */
	public String getDetailsURl() {
		return mDetailsURl;
	}
	/**
	 * @param pDetailsURl the detailsURl to set
	 */
	public void setDetailsURl(String pDetailsURl) {
		mDetailsURl = pDetailsURl;
	}
	/**
	 * @return the authRequired
	 */
	public boolean isAuthRequired() {
		return mAuthRequired;
	}
	/**
	 * @param pAuthRequired the authRequired to set
	 */
	public void setAuthRequired(boolean pAuthRequired) {
		mAuthRequired = pAuthRequired;
	}
	/**
	 * @return the timeOut
	 */
	public int getTimeOut() {
		return mTimeOut;
	}
	/**
	 * @param pTimeOut the timeOut to set
	 */
	public void setTimeOut(int pTimeOut) {
		mTimeOut = pTimeOut;
	}
	/**
	 * @return the requestTimeout
	 */
	public int getRequestTimeout() {
		return mRequestTimeout;
	}
	/**
	 * @param pRequestTimeout the requestTimeout to set
	 */
	public void setRequestTimeout(int pRequestTimeout) {
		mRequestTimeout = pRequestTimeout;
	}
	/**
	 * @return the proxyRequired
	 */
	public boolean isProxyRequired() {
		return mProxyRequired;
	}
	/**
	 * @param pProxyRequired the proxyRequired to set
	 */
	public void setProxyRequired(boolean pProxyRequired) {
		mProxyRequired = pProxyRequired;
	}
	/**
	 * @return the queryParam
	 */
	public String getQueryParam() {
		return mQueryParam;
	}
	/**
	 * @param pQueryParam the queryParam to set
	 */
	public void setQueryParam(String pQueryParam) {
		mQueryParam = pQueryParam;
	}
	/**
	 * @return the updateItemURL
	 */
	public String getUpdateItemURL() {
		return mUpdateItemURL;
	}
	/**
	 * @param pUpdateItemURL the updateItemURL to set
	 */
	public void setUpdateItemURL(String pUpdateItemURL) {
		mUpdateItemURL = pUpdateItemURL;
	}
	
	/**
	 * @return the addItemURL
	 */
	public String getAddItemURL() {
		return mAddItemURL;
	}
	/**
	 * @param pAddItemURL the updateItemURL to set
	 */
	public void setAddItemURL(String pAddItemURL) {
		mAddItemURL = pAddItemURL;
	}
	
}
