package com.tru.integrations.registry.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.exception.RegistryIntegrationException;

/**
 * This is the helper class of Registry Service
 * @author Sandeep Vishwakarma
 *
 */
public class TRURegistryHelper extends GenericService{
	
	/**
	 * prepare the JSONObject 
	 * @param pData data
	 * @return request  
	 * @throws RegistryIntegrationException of RegistryIntegrationException          
	 */
	public String prepareJSONObject(Object pData) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : prepareJSONObject(): START");
		}
		String request = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			request = objectMapper.writeValueAsString(pData);
		} catch (JsonProcessingException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new RegistryIntegrationException(e.getMessage(), e);
		}
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : prepareJSONObject(): END");
		}
		return request;
	}
	
	/**
	 * parse the response
	 * @param pResponse response
	 * @return RegistryResponse  
	 * @throws RegistryIntegrationException of RegistryIntegrationException           
	 */
	public RegistryResponse parseResponse(String pResponse) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : parseResponse(): START");
			vlogDebug("Response jsonObject :: {0}", pResponse);
		}
		ObjectMapper objectMapper = new ObjectMapper();
		RegistryResponse registryResponse;
		try {
			registryResponse = objectMapper.readValue(pResponse, RegistryResponse.class);
		} catch (IOException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new RegistryIntegrationException(e.getMessage(), e);
		}
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper :: ResponseObject :: "+ registryResponse);
			logDebug("TRURegistryHelper : parseResponse(): END");
		}
		return registryResponse;
	}
	
	/**
	 * This method converts json to more readable format.
	 * @param pObject of Object
	 * @param pJson of String
	 * @return formattedJson of String
	 */
	public String getFormattedJson(Object pObject, String pJson) {
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : getFormattedJson(): START");
		}
		String formattedJson = null;
		try {
			formattedJson = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(pObject);
		} catch (JsonProcessingException jsonExp) {
			if (isLoggingError()) {	
				logError(" JsonProcessingException :", jsonExp);		
			}
			formattedJson = pJson;
		}
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : getFormattedJson(): END");
		}
		return formattedJson;
	}
	
	/**
	 * This methods populates header keys and values before making Registry call
	 * @return headers of Map<String, String>
	 */
	public Map<String, String> getHeaderValue(){
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : getHeaderValue(): START");
		}
		Map<String, String> headers = new HashMap<String, String>();
		headers.put(TRURegistryConstant.ACCEPT, TRURegistryConstant.APPLICATION_JSON);
		headers.put(TRURegistryConstant.CONTENT_TYPE, TRURegistryConstant.APPLICATION_JSON);
		if(isLoggingDebug()){
			logDebug("TRURegistryHelper : getHeaderValue(): ENDS");
		}
		return headers;
	}
}
