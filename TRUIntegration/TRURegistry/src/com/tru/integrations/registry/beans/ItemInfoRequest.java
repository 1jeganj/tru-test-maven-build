package com.tru.integrations.registry.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class holds information of Registry Items
 * @author Sandeep Vishwakarma
 *
 */

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ItemInfoRequest {

	@JsonIgnore
	private String mRegistryNumber;
	
	@JsonIgnore
	private String mRegistryType;
	
	@JsonIgnore
	private List<Product> mProduct;
	
	@JsonIgnore
	private String mMessageId;
	
	@JsonIgnore
	private Locale mLocale;

	/**
	 * @return the registryNumber
	 */
	@JsonProperty("registryNumber")
	public String getRegistryNumber() {
		return mRegistryNumber;
	}

	/**
	 * @param pRegistryNumber the registryNumber to set
	 */
	public void setRegistryNumber(String pRegistryNumber) {
		mRegistryNumber = pRegistryNumber;
	}

	/**
	 * @return the registryType
	 */
	@JsonProperty("registryType")
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}

	/**
	 * @return the product
	 */
	@JsonProperty("product")
	public List<Product> getProduct() {
		return mProduct;
	}

	/**
	 * @param pProduct the product to set
	 */
	public void setProduct(List<Product> pProduct) {
		mProduct = pProduct;
	}

	/**
	 * @return the messageId
	 */
	@JsonProperty("messageId")
	public String getMessageId() {
		return mMessageId;
	}

	/**
	 * @param pMessageId the messageId to set
	 */
	public void setMessageId(String pMessageId) {
		mMessageId = pMessageId;
	}

	/**
	 * @return the locale
	 */
	@JsonProperty("locale")
	public Locale getLocale() {
		return mLocale;
	}

	/**
	 * @param pLocale the locale to set
	 */
	public void setLocale(Locale pLocale) {
		mLocale = pLocale;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ItemInfoRequest [mRegistryNumber=" + mRegistryNumber
				+ ", mRegistryType=" + mRegistryType + ", mProducts="
				+ mProduct + ", mMessageId=" + mMessageId + ", mLocale="
				+ mLocale + "]";
	}
	
}