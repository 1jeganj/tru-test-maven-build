package com.tru.integrations.registry.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class holds locale information in Registry
 * 
 * @author Sandeep Vishwakarma
 * 
 */

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

	@JsonIgnore
	private String mSkn;

	@JsonIgnore
	private String mUpc;

	@JsonIgnore
	private String mUid;

	@JsonIgnore
	private String mColor;

	@JsonIgnore
	private String mSize;

	@JsonIgnore
	private String mSknOrigin;

	@JsonIgnore
	private String mNonSpecificIndicator;

	@JsonIgnore
	private String mItemRequested;

	@JsonIgnore
	private String mItemPurchased;

	@JsonIgnore
	private String mPurchaseUpdateIndicator;

	@JsonIgnore
	private String mProductDescription;

	@JsonIgnore
	private String mReferenceRegistryIndicator;

	@JsonIgnore
	private String mReferenceRegistryNumber;

	@JsonIgnore
	private String mWebOrderNumber;

	@JsonIgnore
	private String mSource;

	@JsonIgnore
	private List<Categories> mCategories;

	@JsonIgnore
	private String mItemType;

	/**
	 * @return the skn
	 */
	@JsonProperty("skn")
	public String getSkn() {
		return mSkn;
	}

	/**
	 * @param pSkn
	 *            the skn to set
	 */
	public void setSkn(String pSkn) {
		mSkn = pSkn;
	}

	/**
	 * @return the upc
	 */
	@JsonProperty("upc")
	public String getUpc() {
		return mUpc;
	}

	/**
	 * @param pUpc
	 *            the upc to set
	 */
	public void setUpc(String pUpc) {
		mUpc = pUpc;
	}

	/**
	 * @return the uid
	 */
	@JsonProperty("uid")
	public String getUid() {
		return mUid;
	}

	/**
	 * @param pUid
	 *            the uid to set
	 */
	public void setUid(String pUid) {
		mUid = pUid;
	}

	/**
	 * @return the color
	 */
	@JsonProperty("color")
	public String getColor() {
		return mColor;
	}

	/**
	 * @param pColor
	 *            the color to set
	 */
	public void setColor(String pColor) {
		mColor = pColor;
	}

	/**
	 * @return the size
	 */
	@JsonProperty("size")
	public String getSize() {
		return mSize;
	}

	/**
	 * @param pSize
	 *            the size to set
	 */
	public void setSize(String pSize) {
		mSize = pSize;
	}

	/**
	 * @return the sknOrigin
	 */
	@JsonProperty("sknOrigin")
	public String getSknOrigin() {
		return mSknOrigin;
	}

	/**
	 * @param pSknOrigin
	 *            the sknOrigin to set
	 */
	public void setSknOrigin(String pSknOrigin) {
		mSknOrigin = pSknOrigin;
	}

	/**
	 * @return the nonSpecificIndicator
	 */
	@JsonProperty("nonSpecificIndicator")
	public String getNonSpecificIndicator() {
		return mNonSpecificIndicator;
	}

	/**
	 * @param pNonSpecificIndicator
	 *            the nonSpecificIndicator to set
	 */
	public void setNonSpecificIndicator(String pNonSpecificIndicator) {
		mNonSpecificIndicator = pNonSpecificIndicator;
	}

	/**
	 * @return the itemRequested
	 */
	@JsonProperty("itemRequested")
	public String getItemRequested() {
		return mItemRequested;
	}

	/**
	 * @param pItemRequested
	 *            the itemRequested to set
	 */
	public void setItemRequested(String pItemRequested) {
		mItemRequested = pItemRequested;
	}

	/**
	 * @return the iemPurchased
	 */
	@JsonProperty("itemPurchased")
	public String getItemPurchased() {
		return mItemPurchased;
	}

	/**
	 * @param pItemPurchased
	 *            the itemPurchased to set
	 */
	public void setItemPurchased(String pItemPurchased) {
		mItemPurchased = pItemPurchased;
	}

	/**
	 * @return the purchaseUpdateIndicator
	 */
	@JsonProperty("purchaseUpdateIndicator")
	public String getPurchaseUpdateIndicator() {
		return mPurchaseUpdateIndicator;
	}

	/**
	 * @param pPurchaseUpdateIndicator
	 *            the purchaseUpdateIndicator to set
	 */
	public void setPurchaseUpdateIndicator(String pPurchaseUpdateIndicator) {
		mPurchaseUpdateIndicator = pPurchaseUpdateIndicator;
	}

	/**
	 * @return the productDescription
	 */
	@JsonProperty("productDescription")
	public String getProductDescription() {
		return mProductDescription;
	}

	/**
	 * @param pProductDescription
	 *            the productDescription to set
	 */
	public void setProductDescription(String pProductDescription) {
		mProductDescription = pProductDescription;
	}

	/**
	 * @return the referenceRegistryIndicator
	 */
	@JsonProperty("referenceRegistryIndicator")
	public String getReferenceRegistryIndicator() {
		return mReferenceRegistryIndicator;
	}

	/**
	 * @param pReferenceRegistryIndicator
	 *            the referenceRegistryIndicator to set
	 */
	public void setReferenceRegistryIndicator(String pReferenceRegistryIndicator) {
		mReferenceRegistryIndicator = pReferenceRegistryIndicator;
	}

	/**
	 * @return the referenceRegistryNumber
	 */
	@JsonProperty("referenceRegistryNumber")
	public String getReferenceRegistryNumber() {
		return mReferenceRegistryNumber;
	}

	/**
	 * @param pReferenceRegistryNumber
	 *            the referenceRegistryNumber to set
	 */
	public void setReferenceRegistryNumber(String pReferenceRegistryNumber) {
		mReferenceRegistryNumber = pReferenceRegistryNumber;
	}

	/**
	 * @return the webOrderNumber
	 */
	@JsonProperty("webOrderNumber")
	public String getWebOrderNumber() {
		return mWebOrderNumber;
	}

	/**
	 * @param pWebOrderNumber
	 *            the webOrderNumber to set
	 */
	public void setWebOrderNumber(String pWebOrderNumber) {
		mWebOrderNumber = pWebOrderNumber;
	}

	/**
	 * @return the source
	 */
	@JsonProperty("source")
	public String getSource() {
		return mSource;
	}

	/**
	 * @param pSource
	 *            the source to set
	 */
	public void setSource(String pSource) {
		mSource = pSource;
	}

	/**
	 * @return the categories
	 */
	@JsonProperty("categories")
	public List<Categories> getCategories() {
		return mCategories;
	}

	/**
	 * @param pCategories
	 *            the categories to set
	 */
	public void setCategories(List<Categories> pCategories) {
		mCategories = pCategories;
	}

	/**
	 * @return the itemType
	 */
	@JsonProperty("itemType")
	public String getItemType() {
		return mItemType;
	}

	/**
	 * @param pItemType
	 *            the itemType to set
	 */
	public void setItemType(String pItemType) {
		mItemType = pItemType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [mSkn=" + mSkn + ", mUpc=" + mUpc + ", mUid=" + mUid
				+ ", mColor=" + mColor + ", mSize=" + mSize + ", mSknOrigin="
				+ mSknOrigin + ", mNonSpecificIndicator="
				+ mNonSpecificIndicator + ", mItemRequested=" + mItemRequested
				+ ", mItemPurchased=" + mItemPurchased
				+ ", mPurchaseUpdateIndicator=" + mPurchaseUpdateIndicator
				+ ", mProductDescription=" + mProductDescription
				+ ", mReferenceRegistryIndicator="
				+ mReferenceRegistryIndicator + ", mReferenceRegistryNumber="
				+ mReferenceRegistryNumber + ", mWebOrderNumber="
				+ mWebOrderNumber + ", mSource=" + mSource + ", mCategories="
				+ mCategories + ", mItemType=" + mItemType + "]";
	}

}
