package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class contains User details in Registry.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class People {
	
	@JsonIgnore
	private String mPeopleId;

	@JsonIgnore
	private String mContactType;
	
	@JsonIgnore
	private String mFirstName;
	
	@JsonIgnore
	private String mLastName;
	
	@JsonIgnore
	private String mMaidenName;
	
	@JsonIgnore
	private String mAlternateFirstName;
	
	@JsonIgnore
	private String mAlternateLastName;
	
	@JsonIgnore
	private String mCity;
	
	@JsonIgnore
	private String mState;
	
	@JsonIgnore
	private String mAddressOne;
	
	@JsonIgnore
	private String mAddressTwo;
	
	@JsonIgnore
	private String mZip;
	
	@JsonIgnore
	private String mPhoneNumber;
	
	@JsonIgnore
	private String mPhoneType;
	
	@JsonIgnore
	private String mSmsOptIn;
	
	@JsonIgnore
	private String mEmail;
	
	@JsonIgnore
	private String mPreferredShipIndicator;
	
	@JsonIgnore
	private String mBruEmailOptIn;
	
	@JsonIgnore
	private String mDisneyOptIn;

	/**
	 * @return the peopleId
	 */
	@JsonProperty("peopleId")
	public String getPeopleId() {
		return mPeopleId;
	}

	/**
	 * @param pPeopleId the peopleId to set
	 */
	public void setPeopleId(String pPeopleId) {
		mPeopleId = pPeopleId;
	}

	/**
	 * @return the contactType
	 */
	@JsonProperty("contactType")
	public String getContactType() {
		return mContactType;
	}

	/**
	 * @param pContactType the contactType to set
	 */
	public void setContactType(String pContactType) {
		mContactType = pContactType;
	}

	/**
	 * @return the firstName
	 */
	@JsonProperty("firstName")
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * @param pFirstName the firstName to set
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * @return the lastName
	 */
	@JsonProperty("lastName")
	public String getLastName() {
		return mLastName;
	}

	/**
	 * @param pLastName the lastName to set
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}

	/**
	 * @return the maidenName
	 */
	@JsonProperty("maidenName")
	public String getMaidenName() {
		return mMaidenName;
	}

	/**
	 * @param pMaidenName the maidenName to set
	 */
	public void setMaidenName(String pMaidenName) {
		mMaidenName = pMaidenName;
	}

	/**
	 * @return the alternateFirstName
	 */
	@JsonProperty("alternateFirstName")
	public String getAlternateFirstName() {
		return mAlternateFirstName;
	}

	/**
	 * @param pAlternateFirstName the alternateFirstName to set
	 */
	public void setAlternateFirstName(String pAlternateFirstName) {
		mAlternateFirstName = pAlternateFirstName;
	}

	/**
	 * @return the alternateLastName
	 */
	@JsonProperty("alternateLastName")
	public String getAlternateLastName() {
		return mAlternateLastName;
	}

	/**
	 * @param pAlternateLastName the alternateLastName to set
	 */
	public void setAlternateLastName(String pAlternateLastName) {
		mAlternateLastName = pAlternateLastName;
	}

	/**
	 * @return the city
	 */
	@JsonProperty("city")
	public String getCity() {
		return mCity;
	}

	/**
	 * @param pCity the city to set
	 */
	public void setCity(String pCity) {
		mCity = pCity;
	}

	/**
	 * @return the state
	 */
	@JsonProperty("state")
	public String getState() {
		return mState;
	}

	/**
	 * @param pState the state to set
	 */
	public void setState(String pState) {
		mState = pState;
	}

	/**
	 * @return the addressOne
	 */
	@JsonProperty("addressOne")
	public String getAddressOne() {
		return mAddressOne;
	}

	/**
	 * @param pAddressOne the addressOne to set
	 */
	public void setAddressOne(String pAddressOne) {
		mAddressOne = pAddressOne;
	}

	/**
	 * @return the addressTwo
	 */
	@JsonProperty("addressTwo")
	public String getAddressTwo() {
		return mAddressTwo;
	}

	/**
	 * @param pAddressTwo the addressTwo to set
	 */
	public void setAddressTwo(String pAddressTwo) {
		mAddressTwo = pAddressTwo;
	}

	/**
	 * @return the zip
	 */
	@JsonProperty("zip")
	public String getZip() {
		return mZip;
	}

	/**
	 * @param pZip the zip to set
	 */
	public void setZip(String pZip) {
		mZip = pZip;
	}

	/**
	 * @return the phoneNumber
	 */
	@JsonProperty("phoneNumber")
	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	/**
	 * @param pPhoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String pPhoneNumber) {
		mPhoneNumber = pPhoneNumber;
	}

	/**
	 * @return the phoneType
	 */
	@JsonProperty("phoneType")
	public String getPhoneType() {
		return mPhoneType;
	}

	/**
	 * @param pPhoneType the phoneType to set
	 */
	public void setPhoneType(String pPhoneType) {
		mPhoneType = pPhoneType;
	}

	/**
	 * @return the smsOptIn
	 */
	@JsonProperty("smsOptIn")
	public String getSmsOptIn() {
		return mSmsOptIn;
	}

	/**
	 * @param pSmsOptIn the smsOptIn to set
	 */
	public void setSmsOptIn(String pSmsOptIn) {
		mSmsOptIn = pSmsOptIn;
	}

	/**
	 * @return the email
	 */
	@JsonProperty("email")
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * @return the preferredShipIndicator
	 */
	@JsonProperty("preferredShipIndicator")
	public String getPreferredShipIndicator() {
		return mPreferredShipIndicator;
	}

	/**
	 * @param pPreferredShipIndicator the preferredShipIndicator to set
	 */
	public void setPreferredShipIndicator(String pPreferredShipIndicator) {
		mPreferredShipIndicator = pPreferredShipIndicator;
	}

	/**
	 * @return the bruEmailOptIn
	 */
	@JsonProperty("bruEmailOptIn")
	public String getBruEmailOptIn() {
		return mBruEmailOptIn;
	}

	/**
	 * @param pBruEmailOptIn the bruEmailOptIn to set
	 */
	public void setBruEmailOptIn(String pBruEmailOptIn) {
		mBruEmailOptIn = pBruEmailOptIn;
	}

	/**
	 * @return the disneyOptIn
	 */
	@JsonProperty("disneyOptIn")
	public String getDisneyOptIn() {
		return mDisneyOptIn;
	}

	/**
	 * @param pDisneyOptIn the disneyOptIn to set
	 */
	public void setDisneyOptIn(String pDisneyOptIn) {
		mDisneyOptIn = pDisneyOptIn;
	}
	
	/**
	 * This method returns the user details in registry.
	 * @return  String object 
	 */
	@Override
	public String toString() {
		return "People [mPeopleId=" + mPeopleId + ", mContactType=" + 
				mContactType + ", mFirstName=" + mFirstName + ", mLastName=" + 
				mLastName + ", mMaidenName=" + mMaidenName + 
				", mAlternateFirstName=" + mAlternateFirstName + 
				", mAlternateLastName=" + mAlternateLastName + ", mCity=" + 
				mCity + ", mState=" + mState + ", mAddressOne=" + mAddressOne + 
				", mAddressTwo=" + mAddressTwo + ", mZip=" + mZip + 
				", mPhoneNumber=" + mPhoneNumber + ", mPhoneType=" + 
				mPhoneType + ", mSmsOptIn=" + mSmsOptIn + ", mEmail=" + 
				mEmail + ", mPreferredShipIndicator=" + 
				mPreferredShipIndicator + ", mBruEmailOptIn=" + 
				mBruEmailOptIn + ", mDisneyOptIn=" + mDisneyOptIn + "]";
	}
	
}
