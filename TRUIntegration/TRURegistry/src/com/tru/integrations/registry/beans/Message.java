package com.tru.integrations.registry.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class contains Message details to be displayed in Registry.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Message {

	@JsonIgnore
	private String mMessageType;
	
	@JsonIgnore
	private List<String> mDisplayCode;
	
	@JsonIgnore
	private String mMessageId;

	/**
	 * @return the messageType.
	 */
	@JsonProperty("messageType")
	public String getMessageType() {
		return mMessageType;
	}

	/**
	 * @param pMessageType the messageType to set.
	 */
	public void setMessageType(String pMessageType) {
		mMessageType = pMessageType;
	}

	/**
	 * @return the displayCode.
	 */
	@JsonProperty("displayCode")
	public List<String> getDisplayCode() {
		return mDisplayCode;
	}

	/**
	 * @param pDisplayCode the displayCode to set.
	 */
	public void setDisplayCode(List<String> pDisplayCode) {
		mDisplayCode = pDisplayCode;
	}

	/**
	 * @return the messageId.
	 */
	@JsonProperty("messageId")
	public String getMessageId() {
		return mMessageId;
	}

	/**
	 * @param pMessageId the messageId to set.
	 */
	public void setMessageId(String pMessageId) {
		mMessageId = pMessageId;
	}

	/**
	 * @return string
	 */
	@Override
	public String toString() {
		return "Message [mMessageType=" + mMessageType + ", mDisplayCode="+
	            mDisplayCode + ", mMessageId=" + mMessageId + "]";
	}
	
	
}
