package com.tru.integrations.registry.beans;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is class contains Registry Details.
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RegistryAccountDetailList {

	@JsonIgnore
	private String mRegistryNumber;
	
	@JsonIgnore
	private String mRegistryType;
	
	
	@JsonIgnore
	private String mEventType;
	
	@JsonIgnore
	private String mEventDate;
	
	@JsonIgnore
	private String mModifiedDate;
	
	@JsonIgnore
	private String mCoregistrantFirstName;
	
	@JsonIgnore
	private String mCoregistrantLastName;
	
	@JsonIgnore
	private String mDeleteRegistryIndicator;
	
	@JsonIgnore
	private String mCreateDate;
	
	@JsonIgnore
	private String mEventDescription;

	/**
	 * @return the eventDescription
	 */
	@JsonProperty("eventDescription")
	public String getEventDescription() {
		return mEventDescription;
	}

	/**
	 * @param pEventDescription the eventDescription to set
	 */
	public void setEventDescription(String pEventDescription) {
		mEventDescription = pEventDescription;
	}

	/**
	 * @return the createDate
	 */
	@JsonProperty("createDate")
	public String getCreateDate() {
		return mCreateDate;
	}

	/**
	 * @param pCreateDate the createDate to set
	 */
	public void setCreateDate(String pCreateDate) {
		mCreateDate = pCreateDate;
	}

	/**
	 * @return the registryNumber
	 */
	@JsonProperty("registryNumber")
	public String getRegistryNumber() {
		return mRegistryNumber;
	}

	/**
	 * @param pRegistryNumber the registryNumber to set
	 */
	public void setRegistryNumber(String pRegistryNumber) {
		mRegistryNumber = pRegistryNumber;
	}

	/**
	 * @return the registryType
	 */
	@JsonProperty("registryType")
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}


	/**
	 * @return the eventType
	 */
	@JsonProperty("eventType")
	public String getEventType() {
		return mEventType;
	}

	/**
	 * @param pEventType the eventType to set
	 */
	public void setEventType(String pEventType) {
		mEventType = pEventType;
	}

	/**
	 * @return the eventDate
	 */
	@JsonProperty("eventDate")
	public String getEventDate() {
		return mEventDate;
	}

	/**
	 * @param pEventDate the eventDate to set
	 */
	
	public void setEventDate(String pEventDate) {
		mEventDate = pEventDate;
	}

	/**
	 * @return the mModifiedDate
	 */
	@JsonProperty("modifiedDate")
	public String getModifiedDate() {
		return mModifiedDate;
	}

	/**
	 * @param pModifiedDate the mModifiedDate to set
	 */
	public void setModifiedDate(String pModifiedDate) {
		this.mModifiedDate = pModifiedDate;
	}

	/**
	 * @return the mCoregistrantFirstName
	 */
	@JsonProperty("coregistrantFirstName")
	public String getCoregistrantFirstName() {
		return mCoregistrantFirstName;
	}

	/**
	 * @param pCoregistrantFirstName the mCoregistrantFirstName to set
	 */
	public void setCoregistrantFirstName(String pCoregistrantFirstName) {
		this.mCoregistrantFirstName = pCoregistrantFirstName;
	}

	/**
	 * @return the mCoregistrantLastName
	 */
	@JsonProperty("coregistrantLastName")
	public String getCoregistrantLastName() {
		return mCoregistrantLastName;
	}

	/**
	 * @param pCoregistrantLastName the mCoregistrantLastName to set
	 */
	public void setCoregistrantLastName(String pCoregistrantLastName) {
		this.mCoregistrantLastName = pCoregistrantLastName;
	}

	/**
	 * @return the mDeleteRegistryIndicator
	 */
	@JsonProperty("deleteRegistryIndicator")
	public String getDeleteRegistryIndicator() {
		return mDeleteRegistryIndicator;
	}

	/**
	 * @param pDeleteRegistryIndicator the mDeleteRegistryIndicator to set
	 */
	public void setDeleteRegistryIndicator(String pDeleteRegistryIndicator) {
		this.mDeleteRegistryIndicator = pDeleteRegistryIndicator;
	}


	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegistryDetail [mRegistryNumber=" + mRegistryNumber
				+ ", mRegistryType=" + mRegistryType + ", mEventType=" + mEventType
				+ ", mEventDate=" + mEventDate +", mModifiedDate="+ mModifiedDate +", mCoregistrantFirstName="+ mCoregistrantFirstName+", mCoregistrantLastName="+ mCoregistrantLastName+", mDeleteRegistryIndicator="+ mDeleteRegistryIndicator+", mCreateDate="+ mCreateDate+",mEventDescription="+ mEventDescription+"]";
	}
	
	
}
