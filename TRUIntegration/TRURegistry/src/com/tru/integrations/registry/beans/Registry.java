package com.tru.integrations.registry.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is class contains Registry Details.
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Registry {

	@JsonIgnore
	private String mEmail;
	
	@JsonIgnore
	private List<RegistryAccountDetailList> mRegistryAccountDetailList;
	
	@JsonIgnore
	private RegistryAccountHeader mRegistryAccountHeader;
	
	
	

	/**
	 * @return the mEmail
	 */
	@JsonProperty("email")
	public String getEmail() {
		return mEmail;
	}




	/**
	 * @param pEmail the mEmail to set
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}




	/**
	 * @return the mRegistryAccountDetailList
	 */
	@JsonProperty("registryAccountDetailList")
	public List<RegistryAccountDetailList> getRegistryAccountDetailList() {
		return mRegistryAccountDetailList;
	}




	/**
	 * @param pRegistryAccountDetailList the mRegistryAccountDetailList to set
	 */
	public void setRegistryAccountDetailList(
			List<RegistryAccountDetailList> pRegistryAccountDetailList) {
		mRegistryAccountDetailList = pRegistryAccountDetailList;
	}




	/**
	 * @return the mRegistryAccountHeader
	 */
	@JsonProperty("registryAccountHeader")
	public RegistryAccountHeader getRegistryAccountHeader() {
		return mRegistryAccountHeader;
	}




	/**
	 * @param pRegistryAccountHeader the mRegistryAccountHeader to set
	 */
	public void setRegistryAccountHeader(
			RegistryAccountHeader pRegistryAccountHeader) {
		mRegistryAccountHeader = pRegistryAccountHeader;
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Registry [mEmail=" + mEmail
				+ ", mRegistryAccountDetailList=" + mRegistryAccountDetailList + ", mRegistryAccountHeader="
				+ mRegistryAccountHeader + "]";
	}
	
	
}
