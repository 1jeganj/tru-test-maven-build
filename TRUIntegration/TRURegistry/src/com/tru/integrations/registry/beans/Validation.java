package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class conatins Valdiation Error in Registry Responses
 * @author Sandeep Vishwakarma
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Validation {
	
	@JsonIgnore
	private Error mError;

	/**
	 * @return the error
	 */
	@JsonProperty("error")
	public Error getError() {
		return mError;
	}

	/**
	 * @param pError the error to set
	 */
	public void setError(Error pError) {
		mError = pError;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Validation [mError=" + mError + "]";
	}
	
}
