package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class describes Response Status in Registry.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ResponseStatus {
	
	@JsonIgnore
	private String mIndicator;
	
	@JsonIgnore
	private Message mMessage;

	/**
	 * @return the indicator
	 */
	@JsonProperty("indicator")
	public String getIndicator() {
		return mIndicator;
	}

	/**
	 * @param pIndicator the indicator to set
	 */
	public void setIndicator(String pIndicator) {
		mIndicator = pIndicator;
	}

	/**
	 * @return the message
	 */
	@JsonProperty("message")
	public Message getMessage() {
		return mMessage;
	}

	/**
	 * @param pMessage the message to set
	 */
	public void setMessage(Message pMessage) {
		mMessage = pMessage;
	}

	/**
	 * This method returns the registry details.
	 * @return String object 
	 */
	@Override
	public String toString() {
		return "ResponseStatus [mIndicator=" + mIndicator + ", mMessage=" + 
				mMessage + "]";
	}
	
}
