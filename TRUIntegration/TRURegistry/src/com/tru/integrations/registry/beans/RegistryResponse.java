package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is response bean of registry.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RegistryResponse {

	@JsonIgnore
	private String mServiceTime;
	
	@JsonIgnore
	private RegistryDetail mRegistryDetail;
	
	@JsonIgnore
	private ResponseStatus mResponseStatus;
	
	@JsonIgnore
	private Locale mLocale;
	
	@JsonIgnore
	private String mRegistryNumber;
	
	@JsonIgnore
	private String mRegistryType;
	
	@JsonIgnore
	private String mPeopleId;

	@JsonIgnore
	private Validation mValidation;
	
	/**
	 * @return the serviceTime
	 */
	@JsonProperty("serviceTime")
	public String getServiceTime() {
		return mServiceTime;
	}

	/**
	 * @param pServiceTime the serviceTime to set
	 */
	public void setServiceTime(String pServiceTime) {
		mServiceTime = pServiceTime;
	}

	/**
	 * @return the registryDetail
	 */
	@JsonProperty("registryDetail")
	public RegistryDetail getRegistryDetail() {
		return mRegistryDetail;
	}

	/**
	 * @param pRegistryDetail the registryDetail to set
	 */
	public void setRegistryDetail(RegistryDetail pRegistryDetail) {
		mRegistryDetail = pRegistryDetail;
	}

	/**
	 * @return the responseStatus
	 */
	@JsonProperty("responseStatus")
	public ResponseStatus getResponseStatus() {
		return mResponseStatus;
	}

	/**
	 * @param pResponseStatus the responseStatus to set
	 */
	public void setResponseStatus(ResponseStatus pResponseStatus) {
		mResponseStatus = pResponseStatus;
	}

	/**
	 * @return the locale
	 */
	@JsonProperty("locale")
	public Locale getLocale() {
		return mLocale;
	}

	/**
	 * @param pLocale the locale to set
	 */
	public void setLocale(Locale pLocale) {
		mLocale = pLocale;
	}
	
	/**
	 * @return the registryNumber
	 */
	public String getRegistryNumber() {
		return mRegistryNumber;
	}

	/**
	 * @param pRegistryNumber the registryNumber to set
	 */
	public void setRegistryNumber(String pRegistryNumber) {
		mRegistryNumber = pRegistryNumber;
	}

	/**
	 * @return the registryType
	 */
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}

	/**
	 * @return the peopleId
	 */
	public String getPeopleId() {
		return mPeopleId;
	}

	/**
	 * @param pPeopleId the peopleId to set
	 */
	public void setPeopleId(String pPeopleId) {
		mPeopleId = pPeopleId;
	}

	/**
	 * @return the validation
	 */
	public Validation getValidation() {
		return mValidation;
	}

	/**
	 * @param pValidation the validation to set
	 */
	public void setValidation(Validation pValidation) {
		mValidation = pValidation;
	}

	/**
	 * This method returns the registry response.
	 * @return String object 
	 */
	@Override
	public String toString() {
		return "RegistryResponse [mServiceTime=" + mServiceTime + 
				", mRegistryDetail=" + mRegistryDetail + ", mResponseStatus=" + 
				mResponseStatus + ", mLocale=" + mLocale + 
				", mRegistryNumber=" + mRegistryNumber + ", mRegistryType=" + 
				mRegistryType + ", mPeopleId=" + mPeopleId + ", mValidation=" + 
				mValidation + "]";
	}
	
	

}
