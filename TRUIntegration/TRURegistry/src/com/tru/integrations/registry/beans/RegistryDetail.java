package com.tru.integrations.registry.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is class contains Registry Details.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RegistryDetail {

	@JsonIgnore
	private String mRegistryNumber;
	
	@JsonIgnore
	private String mRegistryType;
	
	@JsonIgnore
	private String mEmailAddress;
	
	@JsonIgnore
	private String mEventType;
	
	@JsonIgnore
	private String mEventDate;
	
	@JsonIgnore
	private String mDecor;
	
	@JsonIgnore
	private String mMailIndicator;
	
	@JsonIgnore
	private String mEmailIndicator;
	
	@JsonIgnore
	private String mAdopting;
	
	@JsonIgnore
	private String mComments;
	
	@JsonIgnore
	private String mAdditionalInformation;
	
	@JsonIgnore
	private String mPassword;
	
	@JsonIgnore
	private List<People> mPeople;
	
	@JsonIgnore
	private List<Product> mProduct;
	
	@JsonIgnore
	private String mPreferredLanguage;
	
	@JsonIgnore
	private String mRegistrantMessageIndicator;
	
	@JsonIgnore
	private String mLoyaltyFlag;
	
	@JsonIgnore
	private String mLoyaltyNumber;
	
	@JsonIgnore
	private String mFirstBaby;
	
	@JsonIgnore
	private String mBabyGender;
	
	@JsonIgnore
	private String mMultiplesIndicator;
	
	@JsonIgnore
	private String mGiftCardsIndicator;
	
	@JsonIgnore
	private String mBatteriesIndicator;
	
	@JsonIgnore
	private String mBppIndicator;
	
	@JsonIgnore
	private String mHolidayCelebrated;
	
	@JsonIgnore
	private String mHolidayCelebratedText;
	
	@JsonIgnore
	private String mSearchOnPartner;
	
	@JsonIgnore
	private String mPurchaseAlertIndicator;
	
	@JsonIgnore
	private String mVanityUrl;
	
	@JsonIgnore
	private String mPublic;

	@JsonIgnore
	private Registry mRegistry;
	
	@JsonIgnore
	private Locale mLocale;
	
	@JsonIgnore
	private ResponseStatus mResponseStatus;
	
	
	
	
	/**
	 * @return the registry
	 */
	@JsonProperty("registry")
	public Registry getRegistry() {
		return mRegistry;
	}

	/**
	 * @param pRegistry the registry to set
	 */
	public void setRegistry(Registry pRegistry) {
		mRegistry = pRegistry;
	}

	/**
	 * @return the locale
	 */
	@JsonProperty("locale")
	public Locale getLocale() {
		return mLocale;
	}

	/**
	 * @param pLocale the locale to set
	 */
	public void setLocale(Locale pLocale) {
		mLocale = pLocale;
	}

	/**
	 * @return the responseStatus
	 */
	@JsonProperty("responseStatus")
	public ResponseStatus getResponseStatus() {
		return mResponseStatus;
	}

	/**
	 * @param pResponseStatus the responseStatus to set
	 */
	public void setResponseStatus(ResponseStatus pResponseStatus) {
		mResponseStatus = pResponseStatus;
	}

	/**
	 * @return the registryNumber
	 */
	@JsonProperty("registryNumber")
	public String getRegistryNumber() {
		return mRegistryNumber;
	}

	/**
	 * @param pRegistryNumber the registryNumber to set
	 */
	public void setRegistryNumber(String pRegistryNumber) {
		mRegistryNumber = pRegistryNumber;
	}

	/**
	 * @return the registryType
	 */
	@JsonProperty("registryType")
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}

	/**
	 * @return the emailAddress
	 */
	@JsonProperty("emailAddress")
	public String getEmailAddress() {
		return mEmailAddress;
	}

	/**
	 * @param pEmailAddress the emailAddress to set
	 */
	public void setEmailAddress(String pEmailAddress) {
		mEmailAddress = pEmailAddress;
	}

	/**
	 * @return the eventType
	 */
	@JsonProperty("eventType")
	public String getEventType() {
		return mEventType;
	}

	/**
	 * @param pEventType the eventType to set
	 */
	public void setEventType(String pEventType) {
		mEventType = pEventType;
	}

	/**
	 * @return the eventDate
	 */
	@JsonProperty("eventDate")
	public String getEventDate() {
		return mEventDate;
	}

	/**
	 * @param pEventDate the eventDate to set
	 */
	public void setEventDate(String pEventDate) {
		mEventDate = pEventDate;
	}

	/**
	 * @return the decor
	 */
	@JsonProperty("decor")
	public String getDecor() {
		return mDecor;
	}

	/**
	 * @param pDecor the decor to set
	 */
	public void setDecor(String pDecor) {
		mDecor = pDecor;
	}

	/**
	 * @return the mailIndicator
	 */
	@JsonProperty("mailIndicator")
	public String getMailIndicator() {
		return mMailIndicator;
	}

	/**
	 * @param pMailIndicator the mailIndicator to set
	 */
	public void setMailIndicator(String pMailIndicator) {
		mMailIndicator = pMailIndicator;
	}

	/**
	 * @return the emailIndicator
	 */
	@JsonProperty("emailIndicator")
	public String getEmailIndicator() {
		return mEmailIndicator;
	}

	/**
	 * @param pEmailIndicator the emailIndicator to set
	 */
	public void setEmailIndicator(String pEmailIndicator) {
		mEmailIndicator = pEmailIndicator;
	}

	/**
	 * @return the adopting
	 */
	@JsonProperty("adopting")
	public String getAdopting() {
		return mAdopting;
	}

	/**
	 * @param pAdopting the adopting to set
	 */
	public void setAdopting(String pAdopting) {
		mAdopting = pAdopting;
	}

	/**
	 * @return the comments
	 */
	@JsonProperty("comments")
	public String getComments() {
		return mComments;
	}

	/**
	 * @param pComments the comments to set
	 */
	public void setComments(String pComments) {
		mComments = pComments;
	}

	/**
	 * @return the additionalInformation
	 */
	@JsonProperty("additionalInformation")
	public String getAdditionalInformation() {
		return mAdditionalInformation;
	}

	/**
	 * @param pAdditionalInformation the additionalInformation to set
	 */
	public void setAdditionalInformation(String pAdditionalInformation) {
		mAdditionalInformation = pAdditionalInformation;
	}

	/**
	 * @return the password
	 */
	@JsonProperty("password")
	public String getPassword() {
		return mPassword;
	}

	/**
	 * @param pPassword the password to set
	 */
	public void setPassword(String pPassword) {
		mPassword = pPassword;
	}

	/**
	 * @return the people
	 */
	@JsonProperty("people")
	public List<People> getPeople() {
		return mPeople;
	}

	/**
	 * @param pPeople the people to set
	 */
	public void setPeople(List<People> pPeople) {
		mPeople = pPeople;
	}
	
	/**
	 * @return the product
	 */
	@JsonProperty("product")
	public List<Product> getProduct() {
		return mProduct;
	}

	/**
	 * @param pProduct the product to set
	 */
	public void setProduct(List<Product> pProduct) {
		mProduct = pProduct;
	}

	/**
	 * @return the preferredLanguage
	 */
	@JsonProperty("preferredLanguage")
	public String getPreferredLanguage() {
		return mPreferredLanguage;
	}

	/**
	 * @param pPreferredLanguage the preferredLanguage to set
	 */
	public void setPreferredLanguage(String pPreferredLanguage) {
		mPreferredLanguage = pPreferredLanguage;
	}

	/**
	 * @return the registrantMessageIndicator
	 */
	@JsonProperty("registrantMessageIndicator")
	public String getRegistrantMessageIndicator() {
		return mRegistrantMessageIndicator;
	}

	/**
	 * @param pRegistrantMessageIndicator the registrantMessageIndicator to set
	 */
	public void setRegistrantMessageIndicator(String pRegistrantMessageIndicator) {
		mRegistrantMessageIndicator = pRegistrantMessageIndicator;
	}

	/**
	 * @return the loyaltyFlag
	 */
	@JsonProperty("loyaltyFlag")
	public String getLoyaltyFlag() {
		return mLoyaltyFlag;
	}

	/**
	 * @param pLoyaltyFlag the loyaltyFlag to set
	 */
	public void setLoyaltyFlag(String pLoyaltyFlag) {
		mLoyaltyFlag = pLoyaltyFlag;
	}

	/**
	 * @return the loyaltyNumber
	 */
	@JsonProperty("loyaltyNumber")
	public String getLoyaltyNumber() {
		return mLoyaltyNumber;
	}

	/**
	 * @param pLoyaltyNumber the loyaltyNumber to set
	 */
	public void setLoyaltyNumber(String pLoyaltyNumber) {
		mLoyaltyNumber = pLoyaltyNumber;
	}

	/**
	 * @return the firstBaby
	 */
	@JsonProperty("firstBaby")
	public String getFirstBaby() {
		return mFirstBaby;
	}

	/**
	 * @param pFirstBaby the firstBaby to set
	 */
	public void setFirstBaby(String pFirstBaby) {
		mFirstBaby = pFirstBaby;
	}

	/**
	 * @return the babyGender
	 */
	@JsonProperty("babyGender")
	public String getBabyGender() {
		return mBabyGender;
	}

	/**
	 * @param pBabyGender the babyGender to set
	 */
	public void setBabyGender(String pBabyGender) {
		mBabyGender = pBabyGender;
	}

	/**
	 * @return the multiplesIndicator
	 */
	@JsonProperty("multiplesIndicator")
	public String getMultiplesIndicator() {
		return mMultiplesIndicator;
	}

	/**
	 * @param pMultiplesIndicator the multiplesIndicator to set
	 */
	public void setMultiplesIndicator(String pMultiplesIndicator) {
		mMultiplesIndicator = pMultiplesIndicator;
	}

	/**
	 * @return the giftCardsIndicator
	 */
	@JsonProperty("giftCardsIndicator")
	public String getGiftCardsIndicator() {
		return mGiftCardsIndicator;
	}

	/**
	 * @param pGiftCardsIndicator the giftCardsIndicator to set
	 */
	public void setGiftCardsIndicator(String pGiftCardsIndicator) {
		mGiftCardsIndicator = pGiftCardsIndicator;
	}

	/**
	 * @return the batteriesIndicator
	 */
	@JsonProperty("batteriesIndicator")
	public String getBatteriesIndicator() {
		return mBatteriesIndicator;
	}

	/**
	 * @param pBatteriesIndicator the batteriesIndicator to set
	 */
	public void setBatteriesIndicator(String pBatteriesIndicator) {
		mBatteriesIndicator = pBatteriesIndicator;
	}

	/**
	 * @return the bppIndicator
	 */
	@JsonProperty("bppIndicator")
	public String getBppIndicator() {
		return mBppIndicator;
	}

	/**
	 * @param pBppIndicator the bppIndicator to set
	 */
	public void setBppIndicator(String pBppIndicator) {
		mBppIndicator = pBppIndicator;
	}

	/**
	 * @return the holidayCelebrated
	 */
	@JsonProperty("holidayCelebrated")
	public String getHolidayCelebrated() {
		return mHolidayCelebrated;
	}

	/**
	 * @param pHolidayCelebrated the holidayCelebrated to set
	 */
	public void setHolidayCelebrated(String pHolidayCelebrated) {
		mHolidayCelebrated = pHolidayCelebrated;
	}

	/**
	 * @return the holidayCelebratedText
	 */
	@JsonProperty("holidayCelebratedText")
	public String getHolidayCelebratedText() {
		return mHolidayCelebratedText;
	}

	/**
	 * @param pHolidayCelebratedText the holidayCelebratedText to set
	 */
	public void setHolidayCelebratedText(String pHolidayCelebratedText) {
		mHolidayCelebratedText = pHolidayCelebratedText;
	}

	/**
	 * @return the searchOnPartner
	 */
	@JsonProperty("searchOnPartner")
	public String getSearchOnPartner() {
		return mSearchOnPartner;
	}

	/**
	 * @param pSearchOnPartner the searchOnPartner to set
	 */
	public void setSearchOnPartner(String pSearchOnPartner) {
		mSearchOnPartner = pSearchOnPartner;
	}

	/**
	 * @return the purchaseAlertIndicator
	 */
	@JsonProperty("purchaseAlertIndicator")
	public String getPurchaseAlertIndicator() {
		return mPurchaseAlertIndicator;
	}

	/**
	 * @param pPurchaseAlertIndicator the purchaseAlertIndicator to set
	 */
	public void setPurchaseAlertIndicator(String pPurchaseAlertIndicator) {
		mPurchaseAlertIndicator = pPurchaseAlertIndicator;
	}

	/**
	 * @return the vanityUrl
	 */
	@JsonProperty("vanityUrl")
	public String getVanityUrl() {
		return mVanityUrl;
	}

	/**
	 * @param pVanityUrl the vanityUrl to set
	 */
	public void setVanityUrl(String pVanityUrl) {
		mVanityUrl = pVanityUrl;
	}

	/**
	 * @return the public
	 */
	@JsonProperty("public")
	public String getPublic() {
		return mPublic;
	}

	/**
	 * @param pPublic the public to set
	 */
	public void setPublic(String pPublic) {
		mPublic = pPublic;
	}

	
	/**
	 * This method returns the registry details.
	 * @return String object 
	 */
	@Override
	public String toString() {
		return "RegistryDetail [mRegistryNumber=" + mRegistryNumber + 
				", mRegistryType=" + mRegistryType + ", mEmailAddress=" + 
				mEmailAddress + ", mEventType=" + mEventType + 
				", mEventDate=" + mEventDate + ", mDecor=" + mDecor + 
				", mMailIndicator=" + mMailIndicator + ", mEmailIndicator=" + 
				mEmailIndicator + ", mAdopting=" + mAdopting + ", mComments=" + 
				mComments + ", mAdditionalInformation=" + 
				mAdditionalInformation + ", mPassword=" + mPassword + 
				", mPeople=" + mPeople + ", mProduct=" + mProduct + 
				", mPreferredLanguage=" + mPreferredLanguage + 
				", mRegistrantMessageIndicator=" + 
				mRegistrantMessageIndicator + ", mLoyaltyFlag=" + 
				mLoyaltyFlag + ", mLoyaltyNumber=" + mLoyaltyNumber + 
				", mFirstBaby=" + mFirstBaby + ", mBabyGender=" + mBabyGender + 
				", mMultiplesIndicator=" + mMultiplesIndicator + 
				", mGiftCardsIndicator=" + mGiftCardsIndicator + 
				", mBatteriesIndicator=" + mBatteriesIndicator + 
				", mBppIndicator=" + mBppIndicator + ", mHolidayCelebrated=" + 
				mHolidayCelebrated + ", mHolidayCelebratedText=" + 
				mHolidayCelebratedText + ", mSearchOnPartner=" + 
				mSearchOnPartner + ", mPurchaseAlertIndicator=" + 
				mPurchaseAlertIndicator + ", mVanityUrl=" + mVanityUrl + 
				", mPublic=" + mPublic + "]";
	}
	
	
}
