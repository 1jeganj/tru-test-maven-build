package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is class contains Registry Details.
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class RegistryAccountHeader {

	@JsonIgnore
	private String mCity;
	
	@JsonIgnore
	private String mFirstName;
	
	@JsonIgnore
	private String mLastName;
	
	@JsonIgnore
	private String mMaidenName;
	
	
	@JsonIgnore
	private String mPeopleId;
	
	@JsonIgnore
	private String mState;

	
	
	/**
	 * @return the mCity
	 */
	@JsonProperty("city")
	public String getCity() {
		return mCity;
	}



	/**
	 * @param pCity the mCity to set
	 */
	public void setCity(String pCity) {
		this.mCity = pCity;
	}



	/**
	 * @return the mFirstName
	 */
	@JsonProperty("firstName")
	public String getFirstName() {
		return mFirstName;
	}



	/**
	 * @param pFirstName the mFirstName to set
	 */
	public void setFirstName(String pFirstName) {
		this.mFirstName = pFirstName;
	}



	/**
	 * @return the mLastName
	 */
	@JsonProperty("lastName")
	public String getLastName() {
		return mLastName;
	}



	/**
	 * @param pLastName the mLastName to set
	 */
	public void setLastName(String pLastName) {
		this.mLastName = pLastName;
	}



	/**
	 * @return the mMaidenName
	 */
	@JsonProperty("maidenName")
	public String getMaidenName() {
		return mMaidenName;
	}



	/**
	 * @param pMaidenName the mMaidenName to set
	 */
	public void setMaidenName(String pMaidenName) {
		this.mMaidenName = pMaidenName;
	}



	/**
	 * @return the mPeopleId
	 */
	@JsonProperty("peopleId")
	public String getPeopleId() {
		return mPeopleId;
	}



	/**
	 * @param pPeopleId the mPeopleId to set
	 */
	public void setPeopleId(String pPeopleId) {
		this.mPeopleId = pPeopleId;
	}



	/**
	 * @return the mState
	 */
	@JsonProperty("state")
	public String getState() {
		return mState;
	}



	/**
	 * @param pState the mState to set
	 */
	public void setState(String pState) {
		this.mState = pState;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegistryDetail [mState=" + mState
				+ ", mPeopleId=" + mPeopleId + ", mMaidenName="
				+ mMaidenName + ", mLastName=" + mLastName
				+ ", mFirstName=" + mFirstName + ", mCity=" + mCity
				 +"]";
	}
	
	
}
