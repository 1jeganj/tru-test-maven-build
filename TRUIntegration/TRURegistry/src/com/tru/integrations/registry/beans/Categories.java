package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class holds locale information in Registry
 * @author Sandeep Vishwakarma
 *
 */

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Categories {

	@JsonIgnore
	private String mCategoryId;
	
	@JsonIgnore
	private String mSubcategoryId;

	@JsonIgnore
	private String mPrimaryInd;
	
	@JsonIgnore
	private String mSortOrder;

	/**
	 * @return the categoryId
	 */
	@JsonProperty("categoryId")
	public String getCategoryId() {
		return mCategoryId;
	}

	/**
	 * @param pCategoryId the categoryId to set
	 */
	public void setCategoryId(String pCategoryId) {
		mCategoryId = pCategoryId;
	}

	/**
	 * @return the subcategoryId
	 */
	@JsonProperty("subcategoryId")
	public String getSubcategoryId() {
		return mSubcategoryId;
	}

	/**
	 * @param pSubcategoryId the subcategoryId to set
	 */
	public void setSubcategoryId(String pSubcategoryId) {
		mSubcategoryId = pSubcategoryId;
	}

	/**
	 * @return the primaryInd
	 */
	@JsonProperty("primaryInd")
	public String getPrimaryInd() {
		return mPrimaryInd;
	}

	/**
	 * @param pPrimaryInd the primaryInd to set
	 */
	public void setPrimaryInd(String pPrimaryInd) {
		mPrimaryInd = pPrimaryInd;
	}

	/**
	 * @return the sortOrder
	 */
	@JsonProperty("sortOrder")
	public String getSortOrder() {
		return mSortOrder;
	}

	/**
	 * @param pSortOrder the sortOrder to set
	 */
	public void setSortOrder(String pSortOrder) {
		mSortOrder = pSortOrder;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Categories [mCategoryId=" + mCategoryId + ", mSubcategoryId="
				+ mSubcategoryId + ", mPrimaryInd=" + mPrimaryInd
				+ ", mSortOrder=" + mSortOrder + "]";
	}
	
}
