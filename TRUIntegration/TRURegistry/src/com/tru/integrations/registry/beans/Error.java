package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class stores error coming Registry Response
 * @author Sandeep Vishwakarma
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Error {
	
	@JsonIgnore
	private String mInformational;

	/**
	 * @return the informational
	 */
	@JsonProperty("informational")
	public String getInformational() {
		return mInformational;
	}

	/**
	 * @param pInformational the informational to set
	 */
	public void setInformational(String pInformational) {
		mInformational = pInformational;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Error [mInformational=" + mInformational + "]";
	}

	
}
