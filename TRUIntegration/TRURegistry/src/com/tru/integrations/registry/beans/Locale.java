package com.tru.integrations.registry.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class holds locale information in Registry.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */

@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Locale {

	@JsonIgnore
	private String mCountry;
	
	@JsonIgnore
	private String mLanguage;

	/**
	 * @return the country.
	 */
	@JsonProperty("country")
	public String getCountry() {
		return mCountry;
	}

	/**
	 * @param pCountry the country to set.
	 */
	public void setCountry(String pCountry) {
		mCountry = pCountry;
	}

	/**
	 * @return the language.
	 */
	@JsonProperty("language")
	public String getLanguage() {
		return mLanguage;
	}

	/**
	 * @param pLanguage the language to set.
	 */
	public void setLanguage(String pLanguage) {
		mLanguage = pLanguage;
	}

	/**
	 * @return string
	 */
	@Override
	public String toString() {
		return "Locale [mCountry=" + mCountry + ", mLanguage=" + mLanguage +
				"]";
	}
	
	
}
