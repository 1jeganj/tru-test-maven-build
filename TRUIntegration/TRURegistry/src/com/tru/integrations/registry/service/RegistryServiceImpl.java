package com.tru.integrations.registry.service;

import atg.nucleus.GenericService;

import com.tru.integrations.exception.TRUIntegrationException;
import com.tru.integrations.http.client.TRUHTTPClient;
import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.util.TRURegistryConfigurations;
import com.tru.integrations.registry.util.TRURegistryConstant;
import com.tru.integrations.registry.util.TRURegistryHelper;

/**
 * This is implementation class of RegistryService.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
public class RegistryServiceImpl extends GenericService implements RegistryService {
	
	private TRUHTTPClient mHttpClient;
	private TRURegistryConfigurations mRegistryConfiguration;
	private TRURegistryHelper mRegistryHelper;
	
	/**
	 * @return the registryHelper
	 */
	public TRURegistryHelper getRegistryHelper() {
		return mRegistryHelper;
	}

	/**
	 * @param pRegistryHelper the registryHelper to set
	 */
	public void setRegistryHelper(TRURegistryHelper pRegistryHelper) {
		mRegistryHelper = pRegistryHelper;
	}


	
	/**
	 * @return the httpClient
	 */
	public TRUHTTPClient getHttpClient() {
		return mHttpClient;
	}

	/**
	 * @param pHttpClient the httpClient to set
	 */
	public void setHttpClient(TRUHTTPClient pHttpClient) {
		mHttpClient = pHttpClient;
	}


	/**
	 * @return the registryConfiguration
	 */
	public TRURegistryConfigurations getRegistryConfiguration() {
		return mRegistryConfiguration;
	}

	/**
	 * @param pRegistryConfiguration the registryConfiguration to set
	 */
	public void setRegistryConfiguration(
			TRURegistryConfigurations pRegistryConfiguration) {
		mRegistryConfiguration = pRegistryConfiguration;
	}

	/**
	 * This method retrieves the Registry Details.
	 * @param pRegistryId of String
	 * @param pRegistryType of String
	 * @return registryResponse of RegistryResponse
	 * @throws TRUIntegrationException of TRUIntegrationException
	 */
	public RegistryResponse getDetails(String pRegistryId, String pRegistryType) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: getDetails() :: STARTS");
			vlogDebug("RegistryId {0}", pRegistryId);
			vlogDebug("RegistryType {0}", pRegistryType);
		}
		StringBuffer url = new StringBuffer(mRegistryConfiguration.getDetailsURl());
		url.append(pRegistryId).append(TRURegistryConstant.SLASH)
				.append(pRegistryType)
				.append(mRegistryConfiguration.getQueryParam());
		String targetURL = new String(url);
		RegistryResponse registryResponse = transact(targetURL, null);
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: getDetails() :: END");
		}
		return registryResponse;
	}
	
	/**
	 * This method updates the Registry items.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @return registryResponse of RegistryResponse
	 * @throws TRUIntegrationException of TRUIntegrationException
	 * @throws RegistryIntegrationException 
	 */
	@Override
	public RegistryResponse updateItems(ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl : updateItems(): START");
		}
		RegistryResponse registryResponse = transact(mRegistryConfiguration.getUpdateItemURL(), pItemInfoRequest);
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl : updateItems(): START");
		}
		return registryResponse;
	}
	
	/**
	 * This method add the Registry items.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @return registryResponse of RegistryResponse
	 * @throws TRUIntegrationException of TRUIntegrationException
	 * @throws RegistryIntegrationException 
	 */
	@Override
	public RegistryResponse addItemToRegistry(ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl : addItemToRegistry(): START");
		}
		RegistryResponse registryResponse = transactPDP(mRegistryConfiguration.getAddItemURL(), pItemInfoRequest);
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl : addItemToRegistry(): START");
		}
		return registryResponse;
	}
	
	
	/**
	 * This method retrieves the Registry Details.
	 * @param pRegistryId of String
	 * @param pDeleteFlag of String
	 * @return registryResponse of RegistryResponse
	 * @throws TRUIntegrationException of TRUIntegrationException
	 */
	public RegistryResponse getWishlistItems(String pRegistryId, String pDeleteFlag) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: getWishlistItems() :: STARTS");
			vlogDebug("RegistryId {0}", pRegistryId);
			vlogDebug("DeleteFlag {0}", pDeleteFlag);
		}
		StringBuffer url = new StringBuffer(mRegistryConfiguration.getWishlistDetailsURl());
		url.append(pRegistryId).append(TRURegistryConstant.SLASH)
				.append(pDeleteFlag).append(TRURegistryConstant.SLASH)
				.append(mRegistryConfiguration.getQueryParam());
		String targetURL = new String(url);
		RegistryResponse registryResponse = transactPDP(targetURL, null);
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: getWishlistItems() :: END");
		}
		return registryResponse;
	}
	
	
	/**
	
	/**
	 * This method is used to make a Http Post Connection to Registry.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @param pUrl of String
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	private RegistryResponse transact(String pUrl, ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: transact() :: START");
		}
		RegistryResponse registryResponse = null;
		String payload = null;
		String response = null;
		try {
			if(pItemInfoRequest != null){
				payload = mRegistryHelper.prepareJSONObject(pItemInfoRequest);
				if(isLoggingDebug()){
					vlogDebug("Request Json Object :: {0}", mRegistryHelper.getFormattedJson(pItemInfoRequest, payload));
				} 
			}
			if (payload == null) {
				response = mHttpClient.executeHttpGetRequest(pUrl,
						mRegistryHelper.getHeaderValue(),
						mRegistryConfiguration.isAuthRequired(),
						mRegistryConfiguration.getTimeOut(),
						mRegistryConfiguration.getRequestTimeout(),
						mRegistryConfiguration.isProxyRequired());
			} else {
				response = mHttpClient.executeHttpPostRequest(pUrl, payload,
						mRegistryHelper.getHeaderValue(),
						mRegistryConfiguration.isAuthRequired(),
						mRegistryConfiguration.getTimeOut(),
						mRegistryConfiguration.getRequestTimeout(),
						mRegistryConfiguration.isProxyRequired());
			}
		} catch (TRUIntegrationException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new RegistryIntegrationException(e);
		}
		
		if(response != null) {
			registryResponse = mRegistryHelper.parseResponse(response);
		}
		if(isLoggingDebug()){
			vlogDebug("RegistryServiceImpl Json Object :: {0}", mRegistryHelper.getFormattedJson(registryResponse, response));
			logDebug("RegistryServiceImpl :: transact() :: END");
		}
		return registryResponse;
	}
	
	/**
	 * This method is used to make a Http Post Connection to Registry.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @param pUrl of String
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	private RegistryResponse transactPDP(String pUrl, ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException {
		if(isLoggingDebug()){
			logDebug("RegistryServiceImpl :: transact() :: START");
		}
		RegistryResponse registryResponse = null;
		String payload = null;
		String response = null;
		try {
			if(pItemInfoRequest != null){
				payload = mRegistryHelper.prepareJSONObject(pItemInfoRequest);
				if(isLoggingDebug()){
					vlogDebug("Request Json Object :: {0}", mRegistryHelper.getFormattedJson(pItemInfoRequest, payload));
				} 
			}
			if (payload == null) {
				// Get Wishlist details
				response = mHttpClient.executeHttpGetPDPRequest(pUrl,
						mRegistryHelper.getHeaderValue(),
						mRegistryConfiguration.isAuthRequired(),
						mRegistryConfiguration.getTimeOut(),
						mRegistryConfiguration.getRequestTimeout(),
						mRegistryConfiguration.isProxyRequired());
			} else {
				// add registry
				response = mHttpClient.executeHttpPut(pUrl, payload,
						mRegistryHelper.getHeaderValue(),
						mRegistryConfiguration.isAuthRequired(),
						mRegistryConfiguration.getTimeOut(),
						mRegistryConfiguration.getRequestTimeout(),
						mRegistryConfiguration.isProxyRequired());
			}
		} catch (TRUIntegrationException e) {
			if(isLoggingError()) {
				logError(e.getMessage(), e);
			}
			throw new RegistryIntegrationException(e);
		}
		
		if(response != null) {
			registryResponse = mRegistryHelper.parseResponse(response);
		}
		if(isLoggingDebug()){
			vlogDebug("RegistryServiceImpl Json Object :: {0}", mRegistryHelper.getFormattedJson(registryResponse, response));
			logDebug("RegistryServiceImpl :: transact() :: END");
		}
		return registryResponse;
	}
}
