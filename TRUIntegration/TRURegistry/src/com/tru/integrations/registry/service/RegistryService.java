package com.tru.integrations.registry.service;

import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.RegistryResponse;
import com.tru.integrations.registry.exception.RegistryIntegrationException;

/**
 * Contains api for retrieving and updating the registry Items.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
public interface RegistryService {

	/**
	 * This method retrieves the Registry Details.
	 * @param pRegistryId of String
	 * @param pRegistryType of String
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	RegistryResponse getDetails(String pRegistryId, String pRegistryType) throws RegistryIntegrationException;
	
	/**
	 * This method updates the Registry items.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	RegistryResponse updateItems(ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException;
	
	/**
	 * This method add the Registry and wishlist items.
	 * @param pItemInfoRequest of ItemInfoRequest
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	RegistryResponse addItemToRegistry(ItemInfoRequest pItemInfoRequest) throws RegistryIntegrationException;
	
	/**
	 * This method retrieves the wishlist Details.
	 * @param pRegistryNumber of String
	 * @param pDeletedFlag of String
	 * @return registryResponse of RegistryResponse
	 * @throws RegistryIntegrationException of RegistryIntegrationException
	 */
	RegistryResponse getWishlistItems(String pRegistryNumber,String pDeletedFlag) throws RegistryIntegrationException;

}
