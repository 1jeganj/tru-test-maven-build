package com.tru.integrations.registry.test;

import java.util.ArrayList;
import java.util.List;

import atg.nucleus.GenericService;

import com.tru.integrations.registry.beans.ItemInfoRequest;
import com.tru.integrations.registry.beans.Locale;
import com.tru.integrations.registry.beans.Product;
import com.tru.integrations.registry.exception.RegistryIntegrationException;
import com.tru.integrations.registry.service.RegistryServiceImpl;

/**
 * This is a class for testing Registry serivces.
 * @author Sandeep Vishwakarma
 * @version 1.0
 */
public class RegistryTest extends GenericService {
	/**
	 * property to hold mSkn.	
	 */
	private String mSkn;
	/**
	 * property to hold mUpc.
	 */
	private String mUpc;
	/**
	 * property to hold mUid.
	 */
	private String mUid;
	/**
	 * property to hold mColor.
	 */
	private String mColor;
	/**
	 * property to hold mSize.
	 */
	private String mSize;
	/**
	 * property to hold mSknOrigin.
	 */
	private String mSknOrigin;
	/**
	 * property to hold mNonSpecificIndicator.
	 */
	private String mNonSpecificIndicator;
	/**
	 * property to hold mItemPurchased.
	 */
	private String mItemPurchased;
	/**
	 * property to hold mPurchaseUpdateIndicator.
	 */
	private String mPurchaseUpdateIndicator;
	/**
	 * property to hold mProductDescription.
	 */
	private String mProductDescription;
	/**
	 * property to hold mMessageId.
	 */
	private String mMessageId;
	/**
	 * property to hold mCountry.
	 */
	private String mCountry;
	/**
	 * property to hold mLanguage.
	 */
	private String mLanguage;
	/**
	 * property to hold mRegistryServiceImpl.
	 */
	private RegistryServiceImpl mRegistryServiceImpl;
	/**
	 * property to hold mRegistryId.
	 */
	private String mRegistryId;
	/**
	 * property to hold mRegistryType.
	 */
	private String mRegistryType;
	
	/**
	 * @return the registryId
	 */
	public String getRegistryId() {
		return mRegistryId;
	}

	/**
	 * @param pRegistryId the registryId to set
	 */
	public void setRegistryId(String pRegistryId) {
		mRegistryId = pRegistryId;
	}

	/**
	 * @return the registryType
	 */
	public String getRegistryType() {
		return mRegistryType;
	}

	/**
	 * @param pRegistryType the registryType to set
	 */
	public void setRegistryType(String pRegistryType) {
		mRegistryType = pRegistryType;
	}

	/**
	 * @return the registryServiceImpl
	 */
	public RegistryServiceImpl getRegistryServiceImpl() {
		return mRegistryServiceImpl;
	}

	/**
	 * @param pRegistryServiceImpl the registryServiceImpl to set
	 */
	public void setRegistryServiceImpl(RegistryServiceImpl pRegistryServiceImpl) {
		mRegistryServiceImpl = pRegistryServiceImpl;
	}
	
	/**
	 * @return the skn
	 */
	public String getSkn() {
		return mSkn;
	}

	/**
	 * @param pSkn the skn to set
	 */
	public void setSkn(String pSkn) {
		mSkn = pSkn;
	}

	/**
	 * @return the upc
	 */
	public String getUpc() {
		return mUpc;
	}

	/**
	 * @param pUpc the upc to set
	 */
	public void setUpc(String pUpc) {
		mUpc = pUpc;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return mUid;
	}

	/**
	 * @param pUid the uid to set
	 */
	public void setUid(String pUid) {
		mUid = pUid;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return mColor;
	}

	/**
	 * @param pColor the color to set
	 */
	public void setColor(String pColor) {
		mColor = pColor;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return mSize;
	}

	/**
	 * @param pSize the size to set
	 */
	public void setSize(String pSize) {
		mSize = pSize;
	}

	/**
	 * @return the sknOrigin
	 */
	public String getSknOrigin() {
		return mSknOrigin;
	}

	/**
	 * @param pSknOrigin the sknOrigin to set
	 */
	public void setSknOrigin(String pSknOrigin) {
		mSknOrigin = pSknOrigin;
	}

	/**
	 * @return the nonSpecificIndicator
	 */
	public String getNonSpecificIndicator() {
		return mNonSpecificIndicator;
	}

	/**
	 * @param pNonSpecificIndicator the nonSpecificIndicator to set
	 */
	public void setNonSpecificIndicator(String pNonSpecificIndicator) {
		mNonSpecificIndicator = pNonSpecificIndicator;
	}

	/**
	 * @return the itemPurchased
	 */
	public String getItemPurchased() {
		return mItemPurchased;
	}

	/**
	 * @param pItemPurchased the itemPurchased to set
	 */
	public void setItemPurchased(String pItemPurchased) {
		mItemPurchased = pItemPurchased;
	}

	/**
	 * @return the purchaseUpdateIndicator
	 */
	public String getPurchaseUpdateIndicator() {
		return mPurchaseUpdateIndicator;
	}

	/**
	 * @param pPurchaseUpdateIndicator the purchaseUpdateIndicator to set
	 */
	public void setPurchaseUpdateIndicator(String pPurchaseUpdateIndicator) {
		mPurchaseUpdateIndicator = pPurchaseUpdateIndicator;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return mProductDescription;
	}

	/**
	 * @param pProductDescription the productDescription to set
	 */
	public void setProductDescription(String pProductDescription) {
		mProductDescription = pProductDescription;
	}

	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return mMessageId;
	}

	/**
	 * @param pMessageId the messageId to set
	 */
	public void setMessageId(String pMessageId) {
		mMessageId = pMessageId;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return mCountry;
	}

	/**
	 * @param pCountry the country to set
	 */
	public void setCountry(String pCountry) {
		mCountry = pCountry;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return mLanguage;
	}

	/**
	 * @param pLanguage the language to set
	 */
	public void setLanguage(String pLanguage) {
		mLanguage = pLanguage;
	}

	/**
	 * This method test Registry Service.
	 * @throws RegistryIntegrationException of RegistryIntegrationException 
	 */
	public void testRegistry() throws  RegistryIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: testRegistry :: STARTS");
		}
		mRegistryServiceImpl.getDetails(mRegistryId, mRegistryType);
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: testRegistry :: ENDS");
		}
	}
	
	/**
	 * This method is use to update Registry items.
	 * @throws RegistryIntegrationException of RegistryIntegrationException 
	 */
	public void testUpdateItems() throws  RegistryIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: testUpdateItems :: STARTS");
		}
		mRegistryServiceImpl.updateItems(createUpdateItemRequest());
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: testUpdateItems :: ENDS");
		}
	}
	
	/**
	 * This method gives ItemInfoRequest object.
	 * @return itemInfo of ItemInfoRequest
	 */
	private ItemInfoRequest createUpdateItemRequest(){
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: createUpdateItemRequest :: STARTS");
		}
		ItemInfoRequest itemInfo = new ItemInfoRequest();
		itemInfo.setRegistryNumber(mRegistryId);
		itemInfo.setRegistryType(mRegistryType);
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();
		product.setSkn(getSkn());
		product.setUpc(getUpc());
		product.setUid(getUid());
		product.setColor(getColor());
		product.setSize(getSize());
		product.setSknOrigin(getSknOrigin());
		product.setNonSpecificIndicator(getNonSpecificIndicator());
		product.setItemPurchased(getItemPurchased());
		product.setPurchaseUpdateIndicator(getPurchaseUpdateIndicator());
		product.setProductDescription(getProductDescription());
		products.add(product);
		itemInfo.setProduct(products);
		itemInfo.setMessageId(getMessageId());
		Locale locale = new Locale();
		locale.setCountry(getCountry());
		locale.setLanguage(getLanguage());
		itemInfo.setLocale(locale);
		if (isLoggingDebug()) {
			vlogDebug("RegistryTest :: createUpdateItemRequest :: STARTS");
		}
		return itemInfo;
	}

}
