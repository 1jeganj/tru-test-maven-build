package com.tru.radial.util;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLongArray;

import atg.nucleus.GenericService;

import com.tru.radial.common.IRadialConstants;

/**
 * 
 * @author PA
 *
 */
public class TRURadialServiceStatisticsLog extends GenericService  {

    private static final String TIME_MARKS =
            "<50ms, <100ms, <200ms, <500ms, <1s, <2s, <5s, <10s, <20s, <50s, <100s, <200s, <500s, >=500s";
    private static final long[] TIMES_ARRAY =
            {50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, Long.MAX_VALUE};

    /**
     * We are aware that this field is not thread safe, but decided this is acceptable and more robust
     */
    private long mLastTimeLogged = 0;

    /** The m log time interval. */
    private long mLogTimeInterval;

    /** The m statistics. */
    private Map<String, AtomicLongArray> mStatistics = new ConcurrentHashMap<String, AtomicLongArray>();
    
    /** The m enable. */
    private boolean mEnable = false;

    /**
     * Accumulate mStatistics about various request execution times
     *
     * @param pDeltaTime timeout value in ms
     * @param pServiceClass target service class
     * @param pServiceOperationName method of target service
     */
    public void addStatisticItem(long pDeltaTime, Class pServiceClass, String pServiceOperationName) {
    	if (!mEnable) {
    		return;
    	}
        int positionToIncrement = 0;
        for (long time : TIMES_ARRAY) {
            if (pDeltaTime < time) {
                break;
            }
            positionToIncrement++;
        }
        String serviceIdentifier = pServiceClass.getSimpleName().concat(IRadialConstants.DOT).concat(pServiceOperationName);
        AtomicLongArray currentTimesArray = mStatistics.get(serviceIdentifier);
        if (currentTimesArray == null) {
            currentTimesArray = new AtomicLongArray(TIMES_ARRAY.length);
            mStatistics.put(serviceIdentifier, currentTimesArray);
        }
        currentTimesArray.getAndIncrement(positionToIncrement);
        logstat();
    }

    /**
     * Every mLogTimeInterval ms we log latest timing mStatistics
     */
    private void logstat() {
        if (isLoggingInfo()) {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            if ((currentTime - mLastTimeLogged) >= mLogTimeInterval) {
                mLastTimeLogged = currentTime;

                logInfo("BEGIN ServiceStatisticsGatherer (time distribution mStatistics):");
                logInfo(TIME_MARKS);
                for(Map.Entry<String, AtomicLongArray> entry : mStatistics.entrySet()) {
                    logInfo(entry.getKey() + ": " + entry.getValue().toString());
                }
                logInfo("END ServiceStatisticsGatherer");
            }
        }
    }

    /**
     * Sets the log time interval.
     *
     * @param pLogTimeInterval the new log time interval
     */
    public void setLogTimeInterval(long pLogTimeInterval) {
        this.mLogTimeInterval = pLogTimeInterval;
    }

	/**
	 * Checks if is enable.
	 *
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		this.mEnable = pEnable;
	}

}
