package com.tru.radial.util;

import java.util.Date;

import com.tru.radial.common.IRadialConstants;

/**
 * The Class TRUServiceMonitorRequest.
 * @author PA
 *
 */
public class TRUServiceMonitorRequest  {
	/** The Ip address. */
	private String mIpAddress;
	/** The Order id. */
	private String mOrderId;
	/** The Source. */
	private String mSource; 
	/** The Current tab. */
	private String mCurrentTab;
	/** The Calls count. */
	private long mCallsCount=IRadialConstants.INT_ONE;
	/** The Last modified date. */
	private Date mLastModifiedDate;

	/**
	 * Instantiates a new TRU service monitor request.
	 */
	public TRUServiceMonitorRequest(){
		super();
	}

	/**
	 * Instantiates a new TRU service monitor request.
	 *
	 * @param pClientIpAddress the client ip address
	 * @param pSource the source
	 * @param pOrderId the order id
	 * @param pCurrentTab the current tab
	 */
	public TRUServiceMonitorRequest (String pClientIpAddress,String pSource,String pOrderId, String pCurrentTab) {
		this.mIpAddress=pClientIpAddress;
		this.mSource=pSource;
		this.mOrderId=pOrderId;
		this.mCurrentTab=pCurrentTab;
		this.mLastModifiedDate=new Date();
	}

	/**
	 * @return the mSource
	 */
	public String getSource() {
		return mSource;
	}
	
	/**
	 * Sets the source.
	 *
	 * @param pSource the new source
	 */
	public void setSource(String pSource) {
		this.mSource = pSource;
	}
	
	/**
	 * Gets the current tab.
	 *
	 * @return the mcurrentTab
	 */
	public String getCurrentTab() {
		return mCurrentTab;
	}
	
	/**
	 * Sets the current tab.
	 *
	 * @param pCurrentTab the new current tab
	 */
	public void setCurrentTab(String pCurrentTab) {
		this.mCurrentTab = pCurrentTab;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();	
		sb.append("\n" + this.getClass().getName() + "\n");
		sb.append("IP Address = " + getIpAddress());
		sb.append("Page Name = " + getCurrentTab());
		sb.append(",source = " + getSource());
		sb.append(",Last Modified Date = " + getLastModifiedDate());
		sb.append(",Total Calls =" + getCallsCount());
		sb.append("\n");
		return sb.toString();
	}
	/**
	 * @return the mIpAddress
	 */
	public String getIpAddress() {
		return mIpAddress;
	}
	
	/**
	 * Sets the ip address.
	 *
	 * @param pIpAddress the new ip address
	 */
	public void setIpAddress(String pIpAddress) {
		this.mIpAddress = pIpAddress;
	}

	/**
	 * @return the mCallsCount
	 */
	public long getCallsCount() {
		return mCallsCount;
	}
	
	/**
	 * Sets the calls count.
	 *
	 * @param pCallsCount the new calls count
	 */
	public void setCallsCount(long pCallsCount) {
		this.mCallsCount = pCallsCount;
	}
	/**
	 * @return the mLastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return mLastModifiedDate;
	}
	
	/**
	 * Sets the last modified date.
	 *
	 * @param pLastModifiedDate the new last modified date
	 */
	public void setLastModifiedDate(Date pLastModifiedDate) {
		this.mLastModifiedDate = pLastModifiedDate;
	}

	/**
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
}
