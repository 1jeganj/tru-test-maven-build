package com.tru.radial.util;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.nucleus.ServiceAdminServlet;
import atg.nucleus.ServiceException;

import com.tru.radial.common.IRadialConstants;

/**
 * The Class TRURadialServiceRequestMonitor.
 *
 * @author PA
 */
public class TRURadialServiceRequestMonitor extends GenericService {
	/** The Request entires. */
	private Map<String, List<TRUServiceMonitorRequest>> mRequestEntires = null;
	/** The Request details. */
	private List<TRUServiceMonitorRequest> mRequestDetails;
	/** reference for TABLE_BORDER*/
	public static final String TABLE_BORDER = "<table border>";
	/** reference for ROW_TABLE_END */
	public static final String ROW_TABLE_END = "</tr>";
	/** reference for HEAD_TABLE_END */
	public static final String HEAD_TABLE_END = "</th>";
	/** reference for ROW_TABLE_START */
	public static final String HEAD_TABLE_START = "<th>";
	/** reference for ROW_TABLE_START */
	public static final String ROW_TABLE_START = "<tr>";
	/** The Constant TABLE_DATA_START. */
	public static final String TABLE_DATA_START = "<td>";
	/** The Constant TABLE_DATA_END. */
	public static final String TABLE_DATA_END = "</td>";
	/** reference for TABLE_END*/
	public static final String TABLE_END = "</table>";
	/** The Constant FORM_ACTION. */
	public static final String FORM_ACTION = "<form action=\"";
	/** The Constant METHOD_POST. */
	public static final String METHOD_POST = "\" method=POST>";
	/** The Constant INPUT_TEXT. */
	public static final String INPUT_TEXT = "<input type='text' name='orderId'>";
	/** The Constant INPUT_SUBMIT. */
	public static final String INPUT_SUBMIT = "<input type='submit' name='submit' value='Submit'>";
	/** The Constant FORM_CLOSURE. */
	public static final String FORM_CLOSURE = "</form>";
	/** The Enable. */
	private boolean mEnable = false;
	/** The Constant IPAddress. */
	public static final String IPADDRESS = "IPAddress";
	/** The Constant Source. */
	public static final String SOURCE = "Source";
	/** The Constant Page. */
	public static final String PAGE = "Page";
	/** The Constant LastModified. */
	public static final String LASTMODIFIED = "LastModified";
	/** The Constant Count. */
	public static final String COUNT = "Count";

	/**
	 * Do start service.
	 *
	 * @throws ServiceException the service exception
	 */
	public void doStartService() throws ServiceException  {
		if (isLoggingDebug()) {
			vlogDebug("Initinalized:: TRURadialServiceRequestMonitor.doStartService()");
		}
		mRequestEntires = new ConcurrentHashMap<String, List<TRUServiceMonitorRequest>>();
		if (mRequestDetails == null) {
			mRequestDetails = new CopyOnWriteArrayList<TRUServiceMonitorRequest>();
		}
	}

	/**
	 * Do stop service.
	 *
	 * @throws ServiceException the service exception
	 */
	public void doStopService() throws ServiceException  {
		mRequestEntires = new ConcurrentHashMap<String, List<TRUServiceMonitorRequest>>();
		mRequestDetails = new CopyOnWriteArrayList<TRUServiceMonitorRequest>();
	}

	/**
	 * Adds the request entry.
	 *
	 * @param pRequest the request
	 */
	public synchronized void addRequestEntry(TRUServiceMonitorRequest pRequest) {
		if (isLoggingDebug()) {
			vlogDebug("START:: TRURadialServiceRequestMonitor.addRequestEntry()");
		}
		if (!mEnable) {
			return;
		}
		boolean isIpaddressExist = false;
		TRUServiceMonitorRequest existingRequest = getRequestEntry(pRequest.getIpAddress(), 
				pRequest.getSource(),
				pRequest.getCurrentTab());
		if (existingRequest == null) {
			if (mRequestDetails == null) {
				mRequestDetails = new CopyOnWriteArrayList<TRUServiceMonitorRequest>();
			}
			Set<String> keySet = getRequestEntires().keySet();
			for (String key : keySet) {
				if (key.equalsIgnoreCase((pRequest.getIpAddress()))) {
					isIpaddressExist = true;
				}
			}
			getRequestDetails().add(pRequest);
			if (!isIpaddressExist) {
				getRequestEntires().put(pRequest.getIpAddress(), getRequestDetails());

			}
		} else {
			long callsCount = existingRequest.getCallsCount();
			existingRequest.setCallsCount(callsCount+IRadialConstants.INT_ONE);
			existingRequest.setCurrentTab(pRequest.getCurrentTab());
			existingRequest.setLastModifiedDate(new Date());
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRURadialServiceRequestMonitor.addRequestEntry()");
		}
	}

	/**
	 * Gets the request entry.
	 *
	 * @param pIpAddress the ip address
	 * @param pSource the source
	 * @param pPage the page
	 * @return the request entry
	 */
	public synchronized TRUServiceMonitorRequest getRequestEntry(String pIpAddress, String pSource, String pPage) {
		if (isLoggingDebug()) {
			vlogDebug("START:: TRURadialServiceRequestMonitor.getRequestEntry()");
		}
		List<TRUServiceMonitorRequest> entries = getRequestEntires().getOrDefault(pIpAddress, null);
		String source = null;
		String ipAddress = null;
		String currentTab = null;
		if (entries !=null && !entries.isEmpty()) {
			for (TRUServiceMonitorRequest truServiceMonitorRequest : entries) {
				source = truServiceMonitorRequest.getSource();
				currentTab = truServiceMonitorRequest.getCurrentTab();
				ipAddress = truServiceMonitorRequest.getIpAddress();
				if (((StringUtils.isNotBlank(source) && source.equalsIgnoreCase(pSource) )&& 
						(StringUtils.isNotBlank(ipAddress) && ipAddress.equalsIgnoreCase(pIpAddress)) &&
						(StringUtils.isNotBlank(currentTab) &&  currentTab.equalsIgnoreCase(pPage)) )) {
					return truServiceMonitorRequest;
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("END:: TRURadialServiceRequestMonitor.getRequestEntry()");
		}
		return null;
	}

	/**
	 * Removes the request entries.
	 */
	public synchronized void removeRequestEntries () {
		getRequestEntires().clear();
		getRequestDetails().clear();
	}

	/**
	 * Creates and returns a new Servlet that will administer this service.
	 * @return CacheAdminServlet method
	 **/
	protected Servlet createAdminServlet() {
		return new TRURadialSrvReqMoniSrvAdminServlet(this, getNucleus());
	}

	/**
	 * The Class TRURadialSrvReqMoniSrvAdminServlet.
	 */
	@SuppressWarnings({ "rawtypes", "serial" })
	class TRURadialSrvReqMoniSrvAdminServlet extends ServiceAdminServlet {
		
		/**
		 * Instantiates a new admin servlet.
		 *
		 * @param pTRURadialServiceRequestMonitor the TRU radial service request monitor
		 * @param pNucleus the nucleus
		 */
		@SuppressWarnings("unchecked")
		public TRURadialSrvReqMoniSrvAdminServlet(TRURadialServiceRequestMonitor pTRURadialServiceRequestMonitor, Nucleus pNucleus) {
			super(pTRURadialServiceRequestMonitor, pNucleus);

		}

		/**
		 * This method is used to print the info of the request count for a order.
		 *
		 * @param pRequest the request
		 * @param pResponse the response
		 * @param pOut the out
		 * @throws ServletException the servlet exception
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		protected void printAdmin(HttpServletRequest pRequest, HttpServletResponse pResponse, ServletOutputStream pOut)
				throws ServletException, IOException {
			TRURadialServiceRequestMonitor srvReqMonitService = (TRURadialServiceRequestMonitor) this.mService;
			List<TRUServiceMonitorRequest> requestDetails = srvReqMonitService.getRequestDetails();
			pOut.println(TABLE_BORDER);
			pOut.println(ROW_TABLE_START);
			pOut.println(HEAD_TABLE_START + IPADDRESS + HEAD_TABLE_END);
			pOut.println(HEAD_TABLE_START + SOURCE + HEAD_TABLE_END);
			pOut.println(HEAD_TABLE_START + PAGE + HEAD_TABLE_END);
			pOut.println(HEAD_TABLE_START + LASTMODIFIED + HEAD_TABLE_END);
			pOut.println(HEAD_TABLE_START + COUNT + HEAD_TABLE_END);
			pOut.println(ROW_TABLE_END);
			synchronized (requestDetails) {
			for (TRUServiceMonitorRequest truServiceMonitorRequest : requestDetails) {
				pOut.println(ROW_TABLE_START);
				pOut.println(TABLE_DATA_START +truServiceMonitorRequest.getIpAddress() + TABLE_DATA_END);
				pOut.println(TABLE_DATA_START +truServiceMonitorRequest.getSource() + TABLE_DATA_END);
				pOut.println(TABLE_DATA_START +truServiceMonitorRequest.getCurrentTab() + TABLE_DATA_END);
				pOut.println(TABLE_DATA_START +truServiceMonitorRequest.getLastModifiedDate() + TABLE_DATA_END);
				pOut.println(TABLE_DATA_START +truServiceMonitorRequest.getCallsCount() + TABLE_DATA_END);
				pOut.println(ROW_TABLE_END);
			}
		}
			pOut.println(TABLE_END);
			super.printAdmin(pRequest, pResponse, pOut);
		}
	}

	/**
	 * @return the mEnable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the new enable
	 */
	public void setEnable(boolean pEnable) {
		this.mEnable = pEnable;
	}

	/**
	 * @return the mRequestEntires
	 */
	public Map<String, List<TRUServiceMonitorRequest>> getRequestEntires() {
		return mRequestEntires;
	}

	/**
	 * Sets the request entires.
	 *
	 * @param pRequestEntires the request entires
	 */
	public void setRequestEntires(
			Map<String, List<TRUServiceMonitorRequest>> pRequestEntires) {
		this.mRequestEntires = pRequestEntires;
	}

	/**
	 * Gets the request details.
	 *
	 * @return the request details
	 */
	public List<TRUServiceMonitorRequest> getRequestDetails() {
		return mRequestDetails;
	}

	/**
	 * Sets the request details.
	 *
	 * @param pRequestDetails the new request details
	 */
	public void setRequestDetails(List<TRUServiceMonitorRequest> pRequestDetails) {
		this.mRequestDetails = pRequestDetails;
	}
}