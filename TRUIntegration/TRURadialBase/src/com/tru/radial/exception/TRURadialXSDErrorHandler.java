package com.tru.radial.exception;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

/**
 * @author PA
 * The Class TRURadialXSDErrorHandler.
 */
public class TRURadialXSDErrorHandler implements ErrorHandler {
	
	public static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(TRURadialXSDErrorHandler.class);

	private List<SAXParseException> mExceptions = new ArrayList<SAXParseException>();
	private List<SAXParseException> mWarnings = new ArrayList<SAXParseException>();

	@Override
	public void warning(SAXParseException pException) throws SAXException {
		if (mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRURadialXSDErrorHandler.warnings :: " + pException.getMessage());
		}
		mWarnings.add(pException);
	}

	@Override
	public void error(SAXParseException pException) throws SAXException {
		if (mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRURadialXSDErrorHandler.error :: " + pException.getMessage());
		}
		mExceptions.add(pException);
	}

	@Override
	public void fatalError(SAXParseException pException) throws SAXException {
		if (mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRURadialXSDErrorHandler.fatalError ::" + pException.getMessage());
		}
		mExceptions.add(pException);
	}

	/**
	 * Gets the exceptions.
	 *
	 * @return the exceptions
	 */
	public List<SAXParseException> getExceptions() {
		return mExceptions;
	}

	/**
	 * Sets the exceptions.
	 *
	 * @param pExceptions the new exceptions
	 */
	public void setExceptions(List<SAXParseException> pExceptions) {
		this.mExceptions = pExceptions;
	}

	/**
	 * Gets the warnings.
	 *
	 * @return the warnings
	 */
	public List<SAXParseException> getWarnings() {
		return mWarnings;
	}

	/**
	 * Sets the warnings.
	 *
	 * @param pWarnings the new warnings
	 */
	public void setWarnings(List<SAXParseException> pWarnings) {
		this.mWarnings = pWarnings;
	}

}
