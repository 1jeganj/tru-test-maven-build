package com.tru.radial.exception;

/**
 * @author PA
 * The Class TRURadialRequestBuilderException.
 */
public class TRURadialRequestBuilderException extends RuntimeException
{
	/**
	 * Version Identifier Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Instantiates a new tRU radial request builder exception.
     *
     * @param pMessage the message
     */
    public TRURadialRequestBuilderException(String pMessage) {
        super(pMessage);
    }

    /**
     * Instantiates a new tRU radial request builder exception.
     *
     * @param pE the p e
     */
    public TRURadialRequestBuilderException(Throwable pE) {
        super(pE);
    }

    /**
     * Instantiates a new tRU radial request builder exception.
     *
     * @param pMessage the message
     * @param pE the p e
     */
    public TRURadialRequestBuilderException(String pMessage, Throwable pE) {
        super(pMessage, pE);
    }
}
