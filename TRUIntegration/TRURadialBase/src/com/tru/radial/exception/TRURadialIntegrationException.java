package com.tru.radial.exception;

/**
 * This exception will be thrown when the application tries to interact with Payment 
 * Service to execute any of the payment service.
 * This exception wraps the following exceptions and gives the callers a way to identify
 * the reason for exception. The calling methods which captures this exception 
 * should verify the specific value in message property of the exception.
 * InterruptedException - thrown if an InterruptedException has occurred from the Socket. 
 * IOException - thrown if an I/O error occurs when creating the socket.
 * 
 * @author PA
 * @version 1.0
 */
public class TRURadialIntegrationException extends Exception  {
	
	private static final String  DEFAULT_ERROR_MESSAGE = "Radial Integration exception";
	/**
	 * Version Identifier Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;
	
	/** The m class name. */
	private String mClassName;
	
	/** The m error message. */
	private String mErrorMessage;
	
	/**
	 * @return the mClassName
	 */
	public String getClassName() {
		return mClassName;
	}

	/**
	 * @param pMClassName the mClassName to set
	 */
	public void setClassName(String pMClassName) {
		mClassName = pMClassName;
	}

	/**
	 * @return the mErrorMessage
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * @param pMErrorMessage the mErrorMessage to set
	 */
	public void setErrorMessage(String pMErrorMessage) {
		mErrorMessage = pMErrorMessage;
	}

	/**
	 * Constructs a new exception with <code>null</code> as its detail message.
	 */
	public TRURadialIntegrationException() {
		super();
	}
	
	/**
	 * Constructs a new exception with the specified detail message.
	 * 
	 * @param pMessage message   the detail message. The detail message is saved for 
     * later retrieval by the {@link #getMessage()} method.
	 */
	public TRURadialIntegrationException(String pMessage) {
		super(pMessage);
	}
	
	/**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     * This constructor is useful for exceptions that are little more than
     * wrappers for other throwables.
     * @param  pThrowable the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
	public TRURadialIntegrationException(Throwable pThrowable) {
		super(pThrowable);
		String errorMessage = DEFAULT_ERROR_MESSAGE ;
		if(pThrowable.getCause() !=null){
			errorMessage =pThrowable.getCause().getMessage();
			setClassName(pThrowable.getClass().getSimpleName());
		}
		setErrorMessage(errorMessage);
		
	}
	
    /**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     * This constructor is useful for exceptions that are little more than
     * wrappers for other throwables.
     * @param pMessage message   the detail message. The detail message is saved for 
     *          later retrieval by the {@link #getMessage()} method.
     * @param  pThrowable the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
	public TRURadialIntegrationException(String pMessage, Throwable pThrowable) {
        super(pMessage, pThrowable);
    }
	
	/**
	 * Instantiates a new TRU radial integration exception.
	 *
	 * @param pClassName the class name
	 * @param pErrorMessage the error message
	 * @param pThrowable the throwable
	 */
	public TRURadialIntegrationException(String pClassName, String pErrorMessage ,Throwable pThrowable) {
		super(pThrowable);
		mClassName = pClassName;
		mErrorMessage = pErrorMessage;
	}

}
