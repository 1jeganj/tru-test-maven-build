package com.tru.radial.common;

import org.apache.http.Header;
import org.apache.http.HttpMessage;

/**
 * @author PA
 * The Class Headered.
 */
public abstract class Headered
{
    
    /**
     * Gets the http message.
     *
     * @return the http message
     */
    protected abstract HttpMessage getHttpMessage();

    /**
     * Gets the headers string.
     *
     * @return the headers string
     */
    public String getHeadersString() {
        Header[] headers = getHttpMessage().getAllHeaders();
        StringBuilder builder = new StringBuilder();

        for (int i = IRadialConstants.INT_ZERO; i < headers.length; i++) {
            Header header = headers[i];
            builder.append(header.getName())
                    .append(IRadialConstants.SEMI_COLON)
                    .append(header.getValue());

            if (i < headers.length - IRadialConstants.INT_ONE){
            	builder.append(IRadialConstants.NEXT_LINE);	
            }                
        }
        return builder.toString();
    }
}
