package com.tru.radial.common;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.common.beans.TRURadialError;
import com.tru.radial.payment.CreditCardAuthReplyType;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.ObjectFactory;
import com.tru.radial.payment.PLCCAuthReplyType;
import com.tru.radial.payment.PayPalDoAuthorizationReplyType;
import com.tru.radial.payment.PayPalDoExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalSetExpressCheckoutReplyType;
import com.tru.radial.payment.StoredValueBalanceReplyType;
import com.tru.radial.payment.StoredValueRedeemReplyType;
import com.tru.radial.payment.StoredValueRedeemVoidReplyType;
import com.tru.radial.payment.TermsAndConditionsReplyType;
import com.tru.radial.payment.ValidateCardReplyType;


/**
 * @author PA
 * The Class AbstractRequestBuilder.
 */
public abstract class AbstractRequestBuilder  implements IRadialRequestBuilder {
	
	/** The logger. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(AbstractRequestBuilder.class);
	
	/** The m response. */
	private IRadialResponse mResponse;
	
	
	/** The m fault response type. */
	private FaultResponseType mFaultResponseType = null;
	
	
	/** The m radial object factory. */
	private ObjectFactory mRadialObjectFactory = new ObjectFactory(); 
	
	/**
	 * Gets the response.
	 *
	 * @return the response
	 */
	public IRadialResponse getResponse() {
		return mResponse;
	}


	/**
	 * Sets the response.
	 *
	 * @param pResponse the new response
	 */
	public void setResponse(IRadialResponse pResponse) {
		this.mResponse = pResponse;
	}


	
	/**
	 * Gets the radial object factory.
	 *
	 * @return the radial object factory
	 */
	public ObjectFactory getRadialObjectFactory() {
		return mRadialObjectFactory;
	}


	/**
	 * Sets the radial object factory.
	 *
	 * @param pRadialObjectFactory the new radial object factory
	 */
	public void setRadialObjectFactory(ObjectFactory pRadialObjectFactory) {
		this.mRadialObjectFactory = pRadialObjectFactory;
	}
	
	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public String getTimeStamp(){
		Date now=new Date();
		DateFormat formatter = new SimpleDateFormat(IRadialConstants.ORDER_DATE_FORMAT,Locale.US);
		return formatter.format(now);
	}
	
	
	/**
	 * Format xml.
	 *
	 * @param pInput the input
	 * @return the string
	 */
	public String formatXML(String pInput)
	{
		try 
		{
			Document doc = DocumentHelper.parseText(pInput);  
			StringWriter sw = new StringWriter();  
			OutputFormat format = OutputFormat.createPrettyPrint();  
			format.setIndent(true);
			format.setIndentSize(IRadialConstants.THREE); 
			XMLWriter xw = new XMLWriter(sw, format);  
			xw.write(doc);  
			return sw.toString();
		}
		catch(IOException e)
		{	
			if(LOGGER.isLoggingError()){
				LOGGER.logError("formatXML exception", e);	
			}			
			return pInput;
		}
		catch(DocumentException e)
		{	
			if(LOGGER.isLoggingError()){
				LOGGER.logError("formatXML exception", e);	
			}			
			return pInput;
		}
	}

	/**
	 * @return the mFaultResponseType
	 */
	public FaultResponseType getFaultResponseType() {
		return mFaultResponseType;
	}

	/**
	 * @param pFaultResponseType the mFaultResponseType to set
	 */
	public void setFaultResponseType(FaultResponseType pFaultResponseType) {
		this.mFaultResponseType = pFaultResponseType;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildFaultResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildFaultResponse(IRadialResponse pPaymentResponse) {
		pPaymentResponse.setSuccess(Boolean.FALSE);
		pPaymentResponse.setResponseCode(IRadialConstants.RADIAL_FAILURE_RESPONSE_CODE);
		TRURadialError error = new TRURadialError(getFaultResponseType().getCode(), 
				getFaultResponseType().getDescription(),
				String.valueOf(getFaultResponseType().getCreateTimestamp()));		
		if(IRadialConstants.RADIAL_SOCKET_TIMEOUT_EXCEPTION_CODE.equalsIgnoreCase(getFaultResponseType().getCode())){
			error = new TRURadialError(IRadialConstants.TRU_PAYPAL_TIMEOUT_ERR_KEY, IRadialConstants.TRU_PAYPAL_TIMEOUT_ERR_KEY);
		}
		if(IRadialConstants.PAN_DOESNOT_PASS_LUHN_CHECK_EXCEPTION.equalsIgnoreCase(getFaultResponseType().getCode())){
			error = new TRURadialError(IRadialConstants.TRU_PAYPAL_TIMEOUT_ERR_KEY, IRadialConstants.TRU_PAYPAL_TIMEOUT_ERR_KEY);
		}
		pPaymentResponse.setRadialError(error);
	}	
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {			
		pPaymentResponse.setSuccess(Boolean.FALSE);
		pPaymentResponse.setResponseCode(pPaymentResponse.getResponseCode());
		TRURadialError radialError = new TRURadialError(IRadialConstants.TRU_PAYPAL_FAILURE_ERR_KEY, IRadialConstants.TRU_PAYPAL_GENERIC_ERR_KEY);
		
		if(IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE.equalsIgnoreCase(pPaymentResponse.getResponseCode())){
			radialError = new TRURadialError(IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE, IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE);
			pPaymentResponse.setTimeoutReponse(Boolean.TRUE);
		}else{
			if(pPaymentResponse instanceof PayPalDoExpressCheckoutReplyType){
				PayPalDoExpressCheckoutReplyType response = (PayPalDoExpressCheckoutReplyType)pPaymentResponse;					
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);	
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pPaymentResponse instanceof PayPalSetExpressCheckoutReplyType){
				PayPalSetExpressCheckoutReplyType  response = (PayPalSetExpressCheckoutReplyType)pPaymentResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pPaymentResponse instanceof PayPalDoExpressCheckoutReplyType){
				PayPalDoExpressCheckoutReplyType  response = (PayPalDoExpressCheckoutReplyType)pPaymentResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pPaymentResponse instanceof PayPalDoAuthorizationReplyType){
				PayPalDoAuthorizationReplyType  response = (PayPalDoAuthorizationReplyType)pPaymentResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}
		}						
		pPaymentResponse.setRadialError(radialError);
	}
	
	/**
	 *This method will construct TIMOUT response if the store didnt get any response from the service .
	 *
	 * @param pPaymentResponse the payment response
	 */
	public void buildTimeOutResponse(IRadialResponse pPaymentResponse) {		
		pPaymentResponse.setSuccess(Boolean.FALSE);
		pPaymentResponse.setTimeoutReponse(Boolean.TRUE);
		pPaymentResponse.setResponseCode(IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE);
		TRURadialError radialError = new TRURadialError(IRadialConstants.TRU_PAYPAL_TIMEOUT_ERR_KEY, 
				IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE,String.valueOf(getTimeStamp()));
		pPaymentResponse.setRadialError(radialError);				
	}	
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse, java.lang.Object)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse,Object pRadialResponse) {			
		pPaymentResponse.setSuccess(Boolean.FALSE);
		pPaymentResponse.setResponseCode(pPaymentResponse.getResponseCode());
		TRURadialError radialError = new TRURadialError(IRadialConstants.TRU_PAYPAL_FAILURE_ERR_KEY, IRadialConstants.TRU_PAYPAL_GENERIC_ERR_KEY);
		
		if(IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE.equalsIgnoreCase(pPaymentResponse.getResponseCode())){
			radialError = new TRURadialError(IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE, IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE);
			pPaymentResponse.setTimeoutReponse(Boolean.TRUE);
		}else{
			if(pRadialResponse instanceof PayPalDoExpressCheckoutReplyType){
				PayPalDoExpressCheckoutReplyType response = (PayPalDoExpressCheckoutReplyType)pRadialResponse;					
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);	
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pRadialResponse instanceof PayPalSetExpressCheckoutReplyType){
				PayPalSetExpressCheckoutReplyType  response = (PayPalSetExpressCheckoutReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pRadialResponse instanceof PayPalDoExpressCheckoutReplyType){
				PayPalDoExpressCheckoutReplyType  response = (PayPalDoExpressCheckoutReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if(pRadialResponse instanceof PayPalDoAuthorizationReplyType){
				PayPalDoAuthorizationReplyType  response = (PayPalDoAuthorizationReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getErrorCode(), 
						response.getErrorMessage(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if (pRadialResponse instanceof PLCCAuthReplyType ){
				PLCCAuthReplyType  response = (PLCCAuthReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode(), 
						response.getResponseCode(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if (pRadialResponse instanceof CreditCardAuthReplyType ){
				CreditCardAuthReplyType  response = (CreditCardAuthReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode(), 
						response.getResponseCode(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if (pRadialResponse instanceof ValidateCardReplyType ){
				ValidateCardReplyType  response = (ValidateCardReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode(), 
						response.getResponseCode(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode());
			}else if (pRadialResponse instanceof TermsAndConditionsReplyType ){
				TermsAndConditionsReplyType response = (TermsAndConditionsReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode().value(), 
						response.getResponseCode().value(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode().value());
			}else if (pRadialResponse instanceof StoredValueBalanceReplyType ){
				StoredValueBalanceReplyType response = (StoredValueBalanceReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode().value(), 
						response.getResponseCode().value(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode().value());
			}else if (pRadialResponse instanceof StoredValueRedeemReplyType ){
				StoredValueRedeemReplyType response = (StoredValueRedeemReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode().value(), 
						response.getResponseCode().value(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode().value());
			}else if (pRadialResponse instanceof StoredValueRedeemVoidReplyType ){
				StoredValueRedeemVoidReplyType response = (StoredValueRedeemVoidReplyType)pRadialResponse;
				radialError = new TRURadialError(
						response.getResponseCode().value(), 
						response.getResponseCode().value(),
						getTimeStamp()
				);
				pPaymentResponse.setResponseCode(response.getResponseCode().value());
			}
		}	
		pPaymentResponse.setRadialError(radialError);
	}
	
}
