package com.tru.radial.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;

/**
 * @author PA
 * The Class TRURadialConfiguration.
 */
public class TRURadialConfiguration extends GenericService{

	/**
	 * Holds the property value of mFinancingCharge.
	 */
	private double mFinancingCharge;
	
	/**
	 * Holds the property value of mProxyRequired.
	 */
	private boolean mProxyEnabled;
	/**
	 * Holds the property value of mProxyHost.
	 */
	private String mProxyHost;	
	/**
	 * Holds the property value of mProxyPort.
	 */
	private int mProxyPort;	
	/**
	 * Holds the property value of mApiKey.
	 */
	private String mApiKey;
	
	/** The m service host. */
	private String mServiceHost;

	/** The m schema version. */
	private String mSchemaVersion;
	
	/** The m connection timeout. */
	private int mConnectionTimeout;
	
	/** The m service resource path. */
	private Map<String, String> mServiceResourcePath= new HashMap<String, String>();
	
	/** The m xsd mapping. */
	private Map<String, String> mXsdMapping= new HashMap<String, String>();
	
	/** The m xsd including list. */
	private List<String> mXsdIncludingList = new ArrayList<>();
	
	/** The m max re try. */
	private int mMaxReTry ;
	
	/** The m request content type. */
	private String mRequestContentType = null;
	
	/** This holds max connection per route. */
	private int mMaxConnPerRoute;
	
	/** This holds max connectionTotal. */
	private int mMaxConnTotal;
	
	/** This holds max time interval delay to call for retry. */
	private int mRetryDelay;
	
	/** The m audit sucn message into db. */
	private boolean mAuditSUCNMessageIntoDB;
	
	/** The m radial invalid paypal error codes. */
	private Map<String, String> mRadialInvalidPaypalErrorCodes = new HashMap<String, String>();
	
	/** The nonce request parameters. */
	private Map<String, String> mNonceRequestParameters;
	
	/** The nonce service host. */
	private String mNonceServiceHost;
	
	/** The nonce resource path. */
	private String mNonceResourcepath;
	
	/** The radialPayment JSPath. */
	private String mRadialPaymentJSPath;
	
	/**
	 * @return the nonceRequestParameters
	 */
	public Map<String, String> getNonceRequestParameters() {
		return mNonceRequestParameters;
	}

	/** The paypal redirect url. */
	private String mPaypalRedirectURL;
	
	/** The pay pal commit param. */
	private String mPayPalCommitParam;
	
	/** The enable do void. */
	private boolean mEnableDoVoid;



	/** The api user name. */
	private String mApiUserName ;
	
	/** The pay pal environment. */
	private String mPayPalEnvironment ;
	
	/**
	 * Holds the property value of mMaxNonceRetryAttempts.
	 */
	private String mMaxNonceRetryAttempts;
	/**
	 * Holds the property value of mMaxGetTokenRetryAttempts.
	 */
	private String mMaxGetTokenRetryAttempts;
	/**
	 * Holds the property value of mMaxTokenizeAndAuthorizeRetryAttempts.
	 */
	private String mMaxTokenizeAndAuthorizeRetryAttempts;
	/**
	 * Holds the property value of mMaxAuthorizeSavedTokenRetryAttempts.
	 */
	private String mMaxAuthorizeSavedTokenRetryAttempts;
	
	/** The m paypal state codes. */
	private Map<String, String> mPaypalStateCodes= new HashMap<String, String>();
	
	/**
	 * @return the mPaypalStateCodes
	 */
	public Map<String, String> getPaypalStateCodes() {
		return mPaypalStateCodes;
	}

	/**
	 * Sets the paypal state codes.
	 *
	 * @param pPaypalStateCodes the paypal state codes
	 */
	public void setPaypalStateCodes(Map<String, String> pPaypalStateCodes) {
		mPaypalStateCodes = pPaypalStateCodes;
	}
	
	/**
	 * @return the maxNonceRetryAttempts
	 */
	public String getMaxNonceRetryAttempts() {
		return mMaxNonceRetryAttempts;
	}

	/**
	 * @param pMaxNonceRetryAttempts the maxNonceRetryAttempts to set
	 */
	public void setMaxNonceRetryAttempts(String pMaxNonceRetryAttempts) {
		mMaxNonceRetryAttempts = pMaxNonceRetryAttempts;
	}

	/**
	 * @return the maxGetTokenRetryAttempts
	 */
	public String getMaxGetTokenRetryAttempts() {
		return mMaxGetTokenRetryAttempts;
	}

	/**
	 * @param pMaxGetTokenRetryAttempts the maxGetTokenRetryAttempts to set
	 */
	public void setMaxGetTokenRetryAttempts(String pMaxGetTokenRetryAttempts) {
		mMaxGetTokenRetryAttempts = pMaxGetTokenRetryAttempts;
	}

	/**
	 * @return the maxTokenizeAndAuthorizeRetryAttempts
	 */
	public String getMaxTokenizeAndAuthorizeRetryAttempts() {
		return mMaxTokenizeAndAuthorizeRetryAttempts;
	}

	/**
	 * @param pMaxTokenizeAndAuthorizeRetryAttempts the maxTokenizeAndAuthorizeRetryAttempts to set
	 */
	public void setMaxTokenizeAndAuthorizeRetryAttempts(
			String pMaxTokenizeAndAuthorizeRetryAttempts) {
		mMaxTokenizeAndAuthorizeRetryAttempts = pMaxTokenizeAndAuthorizeRetryAttempts;
	}

	/**
	 * @return the maxAuthorizeSavedTokenRetryAttempts
	 */
	public String getMaxAuthorizeSavedTokenRetryAttempts() {
		return mMaxAuthorizeSavedTokenRetryAttempts;
	}

	/**
	 * @param pMaxAuthorizeSavedTokenRetryAttempts the maxAuthorizeSavedTokenRetryAttempts to set
	 */
	public void setMaxAuthorizeSavedTokenRetryAttempts(
			String pMaxAuthorizeSavedTokenRetryAttempts) {
		mMaxAuthorizeSavedTokenRetryAttempts = pMaxAuthorizeSavedTokenRetryAttempts;
	}

	/**
	 * @param pNonceRequestParameters the nonceRequestParameters to set
	 */
	public void setNonceRequestParameters(
			Map<String, String> pNonceRequestParameters) {
		mNonceRequestParameters = pNonceRequestParameters;
	}

	/**
	 * @return the nonceServiceHost
	 */
	public String getNonceServiceHost() {
		return mNonceServiceHost;
	}

	/**
	 * @param pNonceServiceHost the nonceServiceHost to set
	 */
	public void setNonceServiceHost(String pNonceServiceHost) {
		mNonceServiceHost = pNonceServiceHost;
	}

	/**
	 * @return the nonceResourcepath
	 */
	public String getNonceResourcepath() {
		return mNonceResourcepath;
	}

	/**
	 * @param pNonceResourcepath the nonceResourcepath to set
	 */
	public void setNonceResourcepath(String pNonceResourcepath) {
		mNonceResourcepath = pNonceResourcepath;
	}

	/**
	 * @return the radialPaymentJSPath
	 */
	public String getRadialPaymentJSPath() {
		return mRadialPaymentJSPath;
	}

	/**
	 * @param pRadialPaymentJSPath the radialPaymentJSPath to set
	 */
	public void setRadialPaymentJSPath(String pRadialPaymentJSPath) {
		mRadialPaymentJSPath = pRadialPaymentJSPath;
	}

	/**
	 * Gets the schema version.
	 *
	 * @return the schema version
	 */
	public String getSchemaVersion() {
		return mSchemaVersion;
	}

	/**
	 * Sets the schema version.
	 *
	 * @param pSchemaVersion the new schema version
	 */
	public void setSchemaVersion(String pSchemaVersion) {
		this.mSchemaVersion = pSchemaVersion;
	}

	/**
	 * Gets the service host.
	 *
	 * @return the service host
	 */
	public String getServiceHost() {
		return mServiceHost;
	}

	/**
	 * Sets the service host.
	 *
	 * @param pServiceHost the new service host
	 */
	public void setServiceHost(String pServiceHost) {
		this.mServiceHost = pServiceHost;
	}

	/**
	 * Gets the service resource path.
	 *
	 * @return the service resource path
	 */
	public Map<String, String> getServiceResourcePath() {
		return mServiceResourcePath;
	}

	/**
	 * Sets the service resource path.
	 *
	 * @param pServiceResourcePath the m service resource path
	 */
	public void setServiceResourcePath(Map<String, String> pServiceResourcePath) {
		this.mServiceResourcePath = pServiceResourcePath;
	}

	/**
	 * Checks if is proxy enabled.
	 *
	 * @return true, if is proxy enabled
	 */
	public boolean isProxyEnabled() {
		return mProxyEnabled;
	}

	/**
	 * Sets the proxy enabled.
	 *
	 * @param pProxyEnabled the new proxy enabled
	 */
	public void setProxyEnabled(boolean pProxyEnabled) {
		this.mProxyEnabled = pProxyEnabled;
	}

	/**
	 * Gets the proxy host.
	 *
	 * @return the proxy host
	 */
	public String getProxyHost() {
		return mProxyHost;
	}

	/**
	 * Sets the proxy host.
	 *
	 * @param pProxyHost the new proxy host
	 */
	public void setProxyHost(String pProxyHost) {
		this.mProxyHost = pProxyHost;
	}

	/**
	 * Gets the proxy port.
	 *
	 * @return the proxy port
	 */
	public int getProxyPort() {
		return mProxyPort;
	}

	/**
	 * Sets the proxy port.
	 *
	 * @param pProxyPort the new proxy port
	 */
	public void setProxyPort(int pProxyPort) {
		this.mProxyPort = pProxyPort;
	}

	/**
	 * Gets the api key.
	 *
	 * @return the api key
	 */
	public String getApiKey() {
		return mApiKey;
	}

	/**
	 * Sets the api key.
	 *
	 * @param pApiKey the new api key
	 */
	public void setApiKey(String pApiKey) {
		this.mApiKey = pApiKey;
	}

	/**
	 * Gets the xsd mapping.
	 *
	 * @return the xsd mapping
	 */
	public Map<String, String> getXsdMapping() {
		return mXsdMapping;
	}

	/**
	 * Sets the xsd mapping.
	 *
	 * @param pXsdMapping the xsd mapping
	 */
	public void setXsdMapping(Map<String, String> pXsdMapping) {
		this.mXsdMapping = pXsdMapping;
	}

	/**
	 * Gets the xsd including list.
	 *
	 * @return the xsd including list
	 */
	public List<String> getXsdIncludingList() {
		return mXsdIncludingList;
	}

	/**
	 * Sets the xsd including list.
	 *
	 * @param pXsdIncludingList the new xsd including list
	 */
	public void setXsdIncludingList(List<String> pXsdIncludingList) {
		this.mXsdIncludingList = pXsdIncludingList;
	}

	/**
	 * Gets the connection timeout.
	 *
	 * @return the connection timeout
	 */
	public int getConnectionTimeout() {
		return mConnectionTimeout;
	}

	/**
	 * Sets the connection timeout.
	 *
	 * @param pConnectionTimeout the new connection timeout
	 */
	public void setConnectionTimeout(int pConnectionTimeout) {
		this.mConnectionTimeout = pConnectionTimeout;
	}

	/**
	 * Gets the request content type.
	 *
	 * @return the request content type
	 */
	public String getRequestContentType() {
		return mRequestContentType;
	}

	/**
	 * Sets the request content type.
	 *
	 * @param pRequestContentType the new request content type
	 */
	public void setRequestContentType(String pRequestContentType) {
		this.mRequestContentType = pRequestContentType;
	}

	/**
	 * Gets the max conn per route.
	 *
	 * @return the max conn per route
	 */
	public int getMaxConnPerRoute() {
		return mMaxConnPerRoute;
	}

	/**
	 * Sets the max conn per route.
	 *
	 * @param pMaxConnPerRoute the new max conn per route
	 */
	public void setMaxConnPerRoute(int pMaxConnPerRoute) {
		this.mMaxConnPerRoute = pMaxConnPerRoute;
	}

	/**
	 * Gets the max conn total.
	 *
	 * @return the max conn total
	 */
	public int getMaxConnTotal() {
		return mMaxConnTotal;
	}

	/**
	 * Sets the max conn total.
	 *
	 * @param pMaxConnTotal the new max conn total
	 */
	public void setMaxConnTotal(int pMaxConnTotal) {
		this.mMaxConnTotal = pMaxConnTotal;
	}

	/**
	 * Gets the retry delay.
	 *
	 * @return the retry delay
	 */
	public int getRetryDelay() {
		return mRetryDelay;
	}

	/**
	 * Sets the retry delay.
	 *
	 * @param pRetryDelay the new retry delay
	 */
	public void setRetryDelay(int pRetryDelay) {
		this.mRetryDelay = pRetryDelay;
	}

	/**
	 * Gets the max re try.
	 *
	 * @return the max re try
	 */
	public int getMaxReTry() {
		return mMaxReTry;
	}

	/**
	 * Holds Configurable value for radial service connectivity. 
	 *
	 * @param pMaxReTry the new max re try
	 */
	public void setMaxReTry(int pMaxReTry) {
		this.mMaxReTry = pMaxReTry;
	}

	
	
	/**
	 * @return the auditSUCNMessageIntoDB
	 */
	public boolean isAuditSUCNMessageIntoDB() {
		return mAuditSUCNMessageIntoDB;
	}

	/**
	 * @param pAuditSUCNMessageIntoDB the auditSUCNMessageIntoDB to set
	 */
	public void setAuditSUCNMessageIntoDB(boolean pAuditSUCNMessageIntoDB) {
		mAuditSUCNMessageIntoDB = pAuditSUCNMessageIntoDB;
	}

	/**
	 * Gets the radial invalid paypal error codes.
	 *
	 * @return the radial invalid paypal error codes
	 */
	public Map<String, String> getRadialInvalidPaypalErrorCodes() {
		return mRadialInvalidPaypalErrorCodes;
	}

	/**
	 * Sets the radial invalid paypal error codes.
	 *
	 * @param pRadialInvalidPaypalErrorCodes the radial invalid paypal error codes
	 */
	public void setRadialInvalidPaypalErrorCodes(
			Map<String, String> pRadialInvalidPaypalErrorCodes) {
		mRadialInvalidPaypalErrorCodes = pRadialInvalidPaypalErrorCodes;
	}

	/**
	 * Gets the paypal redirect url.
	 *
	 * @return the mPaypalRedirectURL
	 */
	public String getPaypalRedirectURL() {
		return mPaypalRedirectURL;
	}

	/**
	 * Sets the paypal redirect URL.
	 *
	 * @param pPaypalRedirectURL the new paypal redirect URL
	 */
	public void setPaypalRedirectURL(String pPaypalRedirectURL) {
		this.mPaypalRedirectURL = pPaypalRedirectURL;
	}

	/**
	 * Gets the pay pal commit param.
	 *
	 * @return the mPayPalCommitParam
	 */
	public String getPayPalCommitParam() {
		return mPayPalCommitParam;
	}

	/**
	 * Sets the pay pal commit param.
	 *
	 * @param pPayPalCommitParam the new pay pal commit param
	 */
	public void setPayPalCommitParam(String pPayPalCommitParam) {
		this.mPayPalCommitParam = pPayPalCommitParam;
	}

	/**
	 * Checks if is enable do void.
	 *
	 * @return the mEnableDoVoid
	 */
	public boolean isEnableDoVoid() {
		return mEnableDoVoid;
	}

	/**
	 * Sets the enable do void.
	 *
	 * @param pEnableDoVoid the new enable do void
	 */
	public void setEnableDoVoid(boolean pEnableDoVoid) {
		this.mEnableDoVoid = pEnableDoVoid;
	}

	/**
	 * Gets the api user name.
	 *
	 * @return the mApiUserName
	 */
	public String getApiUserName() {
		return mApiUserName;
	}

	/**
	 * Sets the api user name.
	 *
	 * @param pAPIUserName the mApiUserName to set
	 */
	public void setApiUserName(String pAPIUserName) {
		this.mApiUserName = pAPIUserName;
	}

	/**
	 * Gets the pay pal environment.
	 *
	 * @return the mPayPalEnvironment
	 */
	public String getPayPalEnvironment() {
		return mPayPalEnvironment;
	}

	/**
	 * Sets the pay pal environment.
	 *
	 * @param pPayPalEnvironment the new pay pal environment
	 */
	public void setPayPalEnvironment(String pPayPalEnvironment) {
		this.mPayPalEnvironment = pPayPalEnvironment;
	}
	
	/** The m enable log req res to db. */
	private boolean mEnableLogReqResToDB;
	
	
	/**
	 * Checks if is enable log req res to db.
	 *
	 * @return the mEnableLogReqResToDB
	 */
	public boolean isEnableLogReqResToDB() {
		return mEnableLogReqResToDB;
	}
	
	/**
	 * Sets the enable log req res to db.
	 *
	 * @param pEnableLogReqResToDB the new enable log req res to db
	 */
	public void setEnableLogReqResToDB(boolean pEnableLogReqResToDB) {
		this.mEnableLogReqResToDB = pEnableLogReqResToDB;
	}

	/**  property to hold mEnableVerboseDebug. */
	private boolean mEnableVerboseDebug;
	
	/**  property to hold mEnableIntegrationLog. */
	private boolean mEnableIntegrationLog;
	
	/**
	 * @return the enableVerboseDebug
	 */
	public boolean isEnableVerboseDebug() {
		return mEnableVerboseDebug;
	}
	/**
	 * @param pEnableVerboseDebug the enableVerboseDebug to set
	 */
	public void setEnableVerboseDebug(boolean pEnableVerboseDebug) {
		mEnableVerboseDebug = pEnableVerboseDebug;
	}

	/**
	 * @return the mEnableIntegrationLog
	 */
	public boolean isEnableIntegrationLog() {
		return mEnableIntegrationLog;
	}

	/**
	 * @param pEnableIntegrationLog the mEnableIntegrationLog to set
	 */
	public void setEnableIntegrationLog(boolean pEnableIntegrationLog) {
		mEnableIntegrationLog = pEnableIntegrationLog;
	}

	/**
	 * Holds the property value of radialInvalidCardErrorCodes.
	 */
	private Map<String, String> mRadialInvalidCardErrorCodes;
	
	/**
	 * @return the radialInvalidCardErrorCodes
	 */
	public Map<String, String> getRadialInvalidCardErrorCodes() {
		return mRadialInvalidCardErrorCodes;
	}

	/**
	 * @param pRadialInvalidCardErrorCodes the radialInvalidCardErrorCodes to set
	 */
	public void setRadialInvalidCardErrorCodes(
			Map<String, String> pRadialInvalidCardErrorCodes) {
		mRadialInvalidCardErrorCodes = pRadialInvalidCardErrorCodes;
	}

	/**
	 * This method is used to get financingCharge.
	 * @return financingCharge String
	 */
	public double getFinancingCharge() {
		return mFinancingCharge;
	}

	/**
	 *This method is used to set financingCharge.
	 *@param pFinancingCharge String
	 */
	public void setFinancingCharge(double pFinancingCharge) {
		mFinancingCharge = pFinancingCharge;
	}

	/** The Soft allocation service host. */
	private String mSoftAllocationServiceHost;

	/**
	 * @return the softAllocationServiceHost
	 */
	public String getSoftAllocationServiceHost() {
		return mSoftAllocationServiceHost;
	}

	/**
	 * @param pSoftAllocationServiceHost the softAllocationServiceHost to set
	 */
	public void setSoftAllocationServiceHost(String pSoftAllocationServiceHost) {
		mSoftAllocationServiceHost = pSoftAllocationServiceHost;
	}
	
	/** The Soft allocation content type. */
	private String mSoftAllocationContentType;

	/**
	 * @return the softAllocationContentType
	 */
	public String getSoftAllocationContentType() {
		return mSoftAllocationContentType;
	}

	/**
	 * @param pSoftAllocationContentType the softAllocationContentType to set
	 */
	public void setSoftAllocationContentType(String pSoftAllocationContentType) {
		mSoftAllocationContentType = pSoftAllocationContentType;
	}
	
	/** The Response codes to correct cvv or avs error. */
	private List<String> mResponseCodesToCorrectCVVOrAVSError;

	/**
	 * @return the responseCodesToCorrectCVVOrAVSError
	 */
	public List<String> getResponseCodesToCorrectCVVOrAVSError() {
		return mResponseCodesToCorrectCVVOrAVSError;
	}

	/**
	 * @param pResponseCodesToCorrectCVVOrAVSError the responseCodesToCorrectCVVOrAVSError to set
	 */
	public void setResponseCodesToCorrectCVVOrAVSError(
			List<String> pResponseCodesToCorrectCVVOrAVSError) {
		mResponseCodesToCorrectCVVOrAVSError = pResponseCodesToCorrectCVVOrAVSError;
	}
	
	/** The m virgin islands state name. */
	private String mVirginIslandsStateName;

	/**
	 * Gets the virgin islands state name.
	 *
	 * @return the virginIslandsStateName
	 */
	public String getVirginIslandsStateName() {
		return mVirginIslandsStateName;
	}

	/**
	 * Sets the virgin islands state name.
	 *
	 * @param pVirginIslandsStateName the virginIslandsStateName to set
	 */
	public void setVirginIslandsStateName(String pVirginIslandsStateName) {
		mVirginIslandsStateName = pVirginIslandsStateName;
	}
	
	
}
