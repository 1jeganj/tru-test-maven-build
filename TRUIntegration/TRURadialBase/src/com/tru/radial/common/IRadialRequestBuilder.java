package com.tru.radial.common;

import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.exception.TRURadialResponseBuilderException;

/**
 * @author PA
 * The Interface IRadialRequestBuilder.
 */
public interface IRadialRequestBuilder {
	
	/**
	 * Builds the request.
	 *
	 * @param pPaymentRequest the payment request
	 * @throws TRURadialRequestBuilderException the tRU radial request builder exception
	 */
	void buildRequest(IRadialRequest pPaymentRequest) throws TRURadialRequestBuilderException;
	
	/**
	 * Builds the response.
	 *
	 * @param pPaymentResponse the payment response
	 * @throws TRURadialResponseBuilderException the tRU radial response builder exception
	 */
	void buildResponse(IRadialResponse pPaymentResponse) throws TRURadialResponseBuilderException;
	
	/**
	 * Builds the fault response.
	 *
	 * @param pPaymentResponse the payment response
	 */
	void buildFaultResponse(IRadialResponse pPaymentResponse);
	
	/**
	 * Builds the error response.
	 *
	 * @param pPaymentResponse the payment response
	 */
	void buildErrorResponse(IRadialResponse pPaymentResponse);
	
	/**
	 * Builds the error response.
	 *
	 * @param pPaymentResponse the payment response
	 * @param pRadialResponse the radial response
	 */
	void buildErrorResponse(IRadialResponse pPaymentResponse , Object pRadialResponse);

}
