package com.tru.radial.common.beans;

/**
 * The Class AbstractRadialResponse.
 */
public  abstract class AbstractRadialResponse implements IRadialResponse{

	/** The m radial error. */
	protected TRURadialError mRadialError;
	
	/** The m response code. */
	protected String mResponseCode;
	
	/** The m is success. */
	protected boolean mIsSuccess;
	
	/** The m is time out response. */
	protected boolean mIsTimeOutResponse;
	
	/** The m time stamp. */
	protected String mTimeStamp;
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public abstract  void setResponseCode(String pValue);
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setSuccess(boolean)
	 */
	@Override
	public abstract void setSuccess(boolean pValue);
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public abstract void setTimeoutReponse(boolean pTmeoutReponse);
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#getResponseCode()
	 */
	@Override
	public String getResponseCode() {
		return mResponseCode;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#isSuccess()
	 */
	@Override
	public boolean isSuccess() {
		return mIsSuccess;
	}

	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#isTimeOutResponse()
	 */
	@Override
	public boolean isTimeOutResponse() {
		return mIsTimeOutResponse;
	}

	

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setRadialError(com.tru.radial.common.beans.TRURadialError)
	 */
	@Override
	public void setRadialError(TRURadialError pRadialError) {
		mRadialError =pRadialError;
		
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#getRadialError()
	 */
	@Override
	public TRURadialError getRadialError() {
		return mRadialError;
	}
	
	/**
	 * Gets the time stamp.
	 *
	 * @return the mTimeStamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}
	
	/**
	 * Sets the time stamp.
	 *
	 * @param pTimeStamp the new time stamp
	 */
	public void setTimeStamp(String pTimeStamp) {
		this.mTimeStamp = pTimeStamp;
	}
	

}
