package com.tru.radial.common.beans;


/**
 * This is a bean class used to contain error code and error message for PayPal Request.
 * 
 * @author PA
 * @version 1.0
 */

public class TRURadialError {

	/**
	 * Property to hold mErrorCode.
	 */
	private String mErrorCode;
	/**
	 * Property to hold mErrorMessage.
	 */
	private String mErrorMessage;

	
	/** The m time stamp. */
	private String mTimeStamp;
	/**
	 * Constructor.
	 * 
	 * @param pErrorCode the ErrorCode to set
	 * @param pErrorMessage the errorMessage to set
	 */
	public TRURadialError(String pErrorCode,String pErrorMessage){
		this.mErrorCode=pErrorCode;
		this.mErrorMessage=pErrorMessage;
	}

	/**
	 * Instantiates a new TRU radial error.
	 *
	 * @param pErrorCode the error code
	 * @param pErrorMessage the error message
	 * @param pTimeStamp the time stamp
	 */
	public TRURadialError(String pErrorCode, String pErrorMessage,
			String pTimeStamp) {
		super();
		this.mErrorCode = pErrorCode;
		this.mErrorMessage = pErrorMessage;
		this.mTimeStamp = pTimeStamp;
	}

	/**
	 * @return the errorCode.
	 */
	public String getErrorCode() {
		return mErrorCode;
	}

	/**
	 * @param pErrorCode the errorCode to set.
	 */
	public void setErrorCode(String pErrorCode) {
		mErrorCode = pErrorCode;
	}

	/**
	 * @return the errorMessage.
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * @param pErrorMessage the errorMessage to set.
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}

	
	/**
	 * @return the mTimeStamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param pTimeStamp the new time stamp
	 */
	public void setTimeStamp(String pTimeStamp) {
		this.mTimeStamp = pTimeStamp;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TRURadialError [mErrorCode=" + mErrorCode + ", mErrorMessage="
				+ mErrorMessage + ", mTimeStamp=" + mTimeStamp + "]";
	}

	
}
