package com.tru.radial.common.beans;



/**
 * @author PA
 * The Interface IRadialResponse.
 */
public interface IRadialResponse {
	
	/**
	 * Sets the response code.
	 *
	 * @param pValue the new response code
	 */
	void setResponseCode(String pValue);
	
	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	String getResponseCode();
	
	/**
	 * This method will check for whether the transaction is failed or successful.
	 * @return boolean type
	 */
	boolean isSuccess();
	/**
	 * This method will check for errors.
	 * @param pErrors as list type
	 */
	/**
	 * This method will set Success.
	 * @param pValue as boolean type
	 */
	void setSuccess(boolean pValue);
	
	/**
	 * Checks if is time out response.
	 *
	 * @return true, if is time out response
	 */
	boolean isTimeOutResponse();
	
	/**
	 * Sets the timeout reponse.
	 *
	 * @param pTmeoutReponse the new timeout reponse
	 */
	void setTimeoutReponse(boolean pTmeoutReponse);

	/**
	 * Sets the radial error.
	 *
	 * @param pRadialError the new radial error
	 */
	void setRadialError(TRURadialError pRadialError);
	
	/**
	 * Gets the radial error.
	 *
	 * @return the radial error
	 */
	TRURadialError getRadialError();
}
