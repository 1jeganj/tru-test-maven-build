package com.tru.radial.common;

/**
 * The Interface IRadialConstants.
 */
public class IRadialConstants {

	/** The Constant ORDER_DATE_FORMAT. */
	public static final String ORDER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
	
	  /** The Constant THREE. */
	public static final int THREE = 3;
  	
  	/** The http success state. */
	public static final int HTTP_SUCCESS_STATE = 200;
  	
  	
  	/** The Constant RADIAL_SUCCESS_RESPONSE_CODE. */
	public static final String RADIAL_SUCCESS_RESPONSE_CODE = "Success" ;
	
	/** The Constant RADIAL_SUCCESS_RESPONSE_CODE. */
	public static final String RADIAL_SUCCESS_WITH_WARNING_RESPONSE_CODE = "SuccessWithWarning" ;
	
	/** The Constant RADIAL_SOCKET_TIMEOUT_EXCEPTION_CODE. */
	public static final String RADIAL_SOCKET_TIMEOUT_EXCEPTION_CODE = "SocketTimeoutException";
  			
    /** The Constant RADIAL_FAILURE_RESPONSE_CODE. */
	public static final String	RADIAL_FAILURE_RESPONSE_CODE="Failure";
    
    /** The Constant SCHEMA_VERSION. */
	public static final  String SCHEMA_VERSION = "1.2";
    
    /** The Constant INT_ZERO. */
	public static final int INT_ZERO=0;
     
     /** The int one. */
	public static final int INT_ONE=1;
	
	  /** The int four. */
		public static final int INT_FOUR=4;
    
     /** The semi colon. */
	public static final String SEMI_COLON = ": ";
    
    /** The next line. */
	public static final char NEXT_LINE = '\n';
    
    /** The UT f_8. */
	public static final String UTF_8= "UTF-8";
    
    /** The empty string. */
	public static final String EMPTY_STRING = "";
    
    /** The package name. */
	public static final String PACKAGE_NAME= "com.tru.radial.payment";
	
	/** The Constant INVENTORY_PAKAGE_NAME. */
	public static final String INVENTORY_PAKAGE_NAME= "com.tru.radial.inventory";
	
    /** The content type key. */
	public static final String CONTENT_TYPE_KEY = "Content-Type";
    
    /** The api key. */
	public static final String API_KEY="ApiKey";
	
	/** The Constant THREAD_SLEEP_TIME. */
	public static final int THREAD_SLEEP_TIME = 500;
	
	/** The Constant MAX_TOTAL_CONNECTIONS. */
	public static final int MAX_TOTAL_CONNECTIONS = 200;
	
	/** The Constant MAX_CONN_PER_ROUTE. */
	public static final int MAX_CONN_PER_ROUTE = 20 ;
	
	/** The Constant DOT. */
	public static final String DOT = ".";
	
	/** The Constant HTTP. */
	public static final String HTTP = "http";
	
	/** The Constant RADIAL_TIMEOUT_RESPONSE_CODE. */
	public static final String RADIAL_TIMEOUT_RESPONSE_CODE = "500" ;
	
	/** The Constant RADIAL_TIMEOUT_RESPONSE_MESSAGE. */
	public static final String RADIAL_TIMEOUT_RESPONSE_MESSAGE = "Connection failed";
	
	/** The Constant TRU_PAYPAL_GENERIC_ERR_KEY. */
	public static final String TRU_PAYPAL_GENERIC_ERR_KEY = "PAYPAL_GENERIC_ERR_KEY";
	
	/** The Constant TRU_PAYPAL_TIMEOUT_ERR_KEY for timeout error. */
	public static final String TRU_PAYPAL_TIMEOUT_ERR_KEY = "PAYPAL_TIME_OUT_ERR_KEY";
	
	/** The Constant TRU_PAYPAL_FAILURE_ERR_KEY for failure response. */
	public static final String TRU_PAYPAL_FAILURE_ERR_KEY = "PAYPAL_FAILURE_ERR_KEY";
	
	/** The Constant TRU_RADIAL_NO_RESPONSE_DESC. */
	public static final String TRU_RADIAL_NO_RESPONSE_DESC = "No response retrieved from the radial and consideres as tiemout";
	
	/** The Constant STRING_ONE. */
	public static final String STRING_ONE  = "1";
	
	/** The Constant STRING_ZERO. */
	public static final String STRING_ZERO  = "0";
	
	/** The Constant PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE. */
	public static final String PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTREQUEST = "pPaymentRequest is not instance of DoAuthorizationPaymentRequest";
	
	/** The Constant PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE. */
	public static final String PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE = "pPaymentResponse is not instance of DoAuthorizationPaymentResponse";
	
	/** The Constant PPAYMENTREQUEST_INSTANCE_DOEXPRESSCHECKOUTPAYMENTREQUEST. */
	public static final String PPAYMENTREQUEST_INSTANCE_DOEXPRESSCHECKOUTPAYMENTREQUEST="pPaymentRequest should be instance of DoExpressCheckoutPaymentRequest";
	
	/** The Constant PPAYMENTRESPONSE_NOT_INSTANCE_DOEXPRESSCHECKOUTPAYMENTRESPONSE. */
	public static final String PPAYMENTRESPONSE_NOT_INSTANCE_DOEXPRESSCHECKOUTPAYMENTRESPONSE="pPaymentResponse not instance of DoExpressCheckoutPaymentResponse";
	
	/** The int two. */
	public static final int INT_TWO=2;
	
	/** The Constant CAUGHT_IO_EXCEPTION. */
	public static final String CAUGHT_IO_EXCEPTION="Caught IO Exception while reading mResponse body.";
	
	/** The Constant XSDFILENAME_IS_NOT_FOUND. */
	public static final String XSDFILENAME_IS_NOT_FOUND="xsdFileName for the pOperation is not found";
	
	/** The Constant RESOURCE_PATH_NOT_FOUND. */
	public static final String RESOURCE_PATH_NOT_FOUND="Resource path for the pOperation is not found";
	
	/** The Constant INVENTORY. */
	public static final String INVENTORY="inventory";
	
	/** The Constant APPLEPAYDWAUTH. */
	public static final String APPLEPAYDWAUTH="applepayDWAuth";
	
	/** The Constant HYPHEN. */
	public static final String HYPHEN = "-";
	
	/** The Constant APPLEPAY_AUTH_RESPONSE. */
	public static final String APPLEPAY_AUTH_RESPONSE = "APPROVED";
	
	/** The Constant CREDIT_CARD_AUTH_RESPONSE. */
	public static final String CREDIT_CARD_AUTH_RESPONSE = "APPROVED";
	
	/** The Constant VALIDATE. */
	public static final String VALIDATE = "Validate";
	
	/** The Constant VALIDATE_CC_SUCCESS_CODE. */
	public static final String VALIDATE_CC_SUCCESS_CODE = "valid";
	
	/** The Constant PAY_CONTEXT_PACKAGE. */
	
	public static final String PAYMENT_CONTEXT_PACKAGE = "com.tru.radial.payment.PaymentContextType";
	
    /** The Constant CARD_SEC_CODE. */
	
	public static final String CARD_SEC_CODE = "CardSecurityCode";
	
	 /** The Constant ENC_CHAR. */
	
	public static final String ENC_CHAR = "X";
	
	/** The Constant DOUBLE_DECIMAL. */
	public static final String DOUBLE_DECIMAL = "0.00";
	
	/** The Constant PAN_DOESNOT_PASS_LUHN_CHECK_EXCEPTION. */
	public static final String PAN_DOESNOT_PASS_LUHN_CHECK_EXCEPTION = "PanDoesNotPassLuhnCheckException";
	
	/** The Constant PROMO_OPERATION. */
	public static final String PROMO_OPERATION = "Promo";
	
	/** The Constant NONCE_OPRATION. */
	public static final String NONCE_OPRATION = "nonceOpration";
	
	/** The Constant SOFT_ALLOCATIONS. */
	public static final String SOFT_ALLOCATIONS = "softAllocations";
	
	/** The Constant RADIAL_PAYMENT_ERROR_KEY. */
	public static final String RADIAL_PAYMENT_ERROR_KEY = "50002";
	
	/** The Constant NONCE. */
	public static final String NONCE ="nonce";
	
	/** The Constant EXPIRES_IN_SECONDS. */
	public static final String EXPIRES_IN_SECONDS ="expires_in_seconds";
	
	/** The Constant JWT. */
	public static final String JWT ="jwt";
	

	/** The Constant holds the TaxProcessor. */
	public static final String STR_TAX_PROCESSOR = "TaxProcessor";
	/** The Constant holds the CreditCardProcessor. */
	public static final String STR_CREDIT_CARD_PROCESSOR = "CreditCardPaymentProcessor";
	/** The Constant holds the Authorize. */
	public static final String STR_AUTHORISE = "authorise";
	/** The Constant holds the Authorize. */
	public static final String STR_PLCC_AUTHORISE = "plccAuthorise";
	/** The Constant holds the TaxProcessor. */
	public static final String STR_GIFTCARD_PROCESSOR = "GiftCardPaymentProcessor";
	/** The Constant holds the TaxProcessor. */
	public static final String STR_GIFTCARD_INQUIRY = "balanceInquiry";
	/** The Constant holds the TaxProcessor. */
	public static final String STR_PAYPAL_PROCESSOR = "PayPalPaymentProcessor";
	/** The Constant holds the TaxProcessor. */
	public static final String STR_INVENTORY_PROCESSOR = "InventoryProcessor";
	/** The Constant holds the Calculate Tax. */
	public static final String STR_CALCULATE_TAX = "calculateTax";
	/** The Constant holds the PageName. */
	public static final String STR_PAGE_NAME = "Page";
	/** The Constant holds the ErrorMessage. */
	public static final String STR_ERROR_MSG = "Error";
	/** The Constant holds the ResponseCode. */
	public static final String STR_REPPONSE_CODE ="ResponseCode";
	/** The Constant holds the TimeTaken. */
	public static final String STR_TIME_TAKEN ="TimeTaken";
	/** The Constant holds the Empty String. */
	public static final String STR_EMPTY ="EMPTY";
	/** The Constant holds the Ops Name. */
	public static final String OPERATION_NAME ="operationName";
	
	/**  The Constant holds the Time Entry. */
	public static final String START_TIME ="TimeStamp";
	
	/** The Constant PAGE_NAME. */
	public static final String PAGE_NAME ="pageName";
	
	/** The Constant STR_RADIAL. */
	public static final String STR_RADIAL = "Radial";
	
	/** The Constant INT_FIVE. */
	public static final int INT_FIVE = 5;
	 /**
     * constants for Milliseconds.
     */
    public static final String STR_MILLISECONDS = "MSec";
     /** The Constant SPACE. */
 	public static final String SPACE = " ";
 	
 	/** The Constant STR_APPLE_PAY_PROCESSOR. */
	public static final String STR_APPLE_PAY_PROCESSOR = "TRURadialApplePayProcessor";
	 
	 /** The Constant APPLE_PAY_AUTHROIZATION. */
 	public static final String APPLE_PAY_AUTHROIZATION = "applepayAuthroization";
 	
 	/** The Constant RADIAL_PAYMENT. */
 	public static final String RADIAL_PAYMENT = "radialPayment";
 	
 	/** The Constant STR_CREDIT_CARD_RADIAL_PAYMENT. */
 	public static final String STR_CREDIT_CARD_RADIAL_PAYMENT = "creditCardPaymentService";
 	
 	/** The Constant STR_GIFT_CARD_RADIAL_PAYMENT. */
 	public static final String STR_GIFT_CARD_RADIAL_PAYMENT = "giftCardPaymentService";
 	
 	/** The Constant STR_GIFT_CARD_RADIAL_PAYMENT. */
 	public static final String STR_PAY_PAL_RADIAL_PAYMENT = "payapalPaymentService";
 	
 	/** The Constant STR_GIFT_CARD_RADIAL_PAYMENT. */
 	public static final String STR_APPLE_PAY_RADIAL_PAYMENT = "applePayPaymentService";
 	
 	/** The Constant RADIAL_TAX. */
 	public static final String RADIAL_TAX = "radialtax";
 	
 	/** The Constant RADIAL_INVENTORY. */
 	public static final String RADIAL_INVENTORY = "radialInventory";
 	
 	/** The Constant VIRGIN_ISLANDS. */
	 public static final String VIRGIN_ISLANDS = "VIRGIN ISLANDS";
 	
}
