package com.tru.radial.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;

import com.tru.radial.common.Headered;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.exception.TRURadialRequestBuilderException;

/**
 * @author PA
 * The Class RadialGatewayResponse.
 */
public class RadialGatewayResponse extends Headered {
	
    /** The m response. */
    private HttpResponse mResponse;
    
    /** The m response body. */
    private String mResponseBody;

    /**
     * Instantiates a new radial gateway response.
     *
     * @param pResponse the response
     * @throws TRURadialRequestBuilderException the TRU radial request builder exception
     */
    public RadialGatewayResponse(HttpResponse pResponse) throws TRURadialRequestBuilderException{
        this.mResponse = pResponse;
        this.mResponseBody = extractResponseBody();
    }

    /**
     * Extract response body.
     *
     * @return the string
     * @throws TRURadialRequestBuilderException the TRU radial request builder exception
     */
    private String extractResponseBody() throws TRURadialRequestBuilderException
    {
        HttpEntity entity = mResponse.getEntity();
        try {
            if (entity != null)
            {
                InputStream instream = entity.getContent();
                try
                {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(instream, IRadialConstants.UTF_8));

                    StringBuilder response = new StringBuilder();
                    String inputLine = reader.readLine();
                    while (inputLine != null)
                    {
                        response.append(inputLine);
                        inputLine = reader.readLine();
                    }
                    return response.toString();
                } finally {
                    // Closing the input stream will trigger connection release
                    instream.close();
                    
                }
            }
        } catch (IOException e) {
            throw new TRURadialRequestBuilderException(IRadialConstants.CAUGHT_IO_EXCEPTION, e);
        }
        return IRadialConstants.EMPTY_STRING;
    }

    /**
     * Gets the response body.
     *
     * @return the response body
     */
    public String getResponseBody() {
        return mResponseBody;
    }

    /**
     * Gets the status code.
     *
     * @return the status code
     */
    public int getStatusCode()
    {
    	int status = 0;
    	if (mResponse != null ) {
    		status = mResponse.getStatusLine().getStatusCode();
    	}
        return status;
    }

    /* (non-Javadoc)
     * @see com.tru.radial.common.Headered#getHttpMessage()
     */
    @Override
	protected HttpMessage getHttpMessage() {
		 return mResponse;
	}


}
