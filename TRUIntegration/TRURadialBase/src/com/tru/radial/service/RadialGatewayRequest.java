package com.tru.radial.service;

import java.io.Serializable;



/**
 * @author PA
 * The Class RadialGatewayRequest.
 */
public class RadialGatewayRequest implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The m api key. */
	private String mApiKey;
    
    /** The m service host. */
    private String mServiceHost;
    
    /** The m service resourcepath. */
    private String mServiceResourcepath;
    
    /** The m request payload. */
    private String mRequestPayload;
    
    /** The m connection timeout. */
    private int mConnectionTimeout;

    /**
     * Instantiates a new radial gateway request.
     *
     * @param pApiKey the api key
     * @param pServiceHost the service host
     * @param pServiceResourcepath the service resourcepath
     * @param pRequestPayload the request payload
     * @param pConnectionTimeout the m connection timeout
     */
    public RadialGatewayRequest(String pApiKey, String pServiceHost, String pServiceResourcepath,String pRequestPayload,int pConnectionTimeout ) {
        this.setApiKey(pApiKey);
        this.mServiceHost = pServiceHost;
        this.setServiceResourcepath(pServiceResourcepath);
        this.mRequestPayload = pRequestPayload;
        this.mConnectionTimeout = pConnectionTimeout;
    }

	/**
	 * Gets the api key.
	 *
	 * @return the api key
	 */
	public String getApiKey() {
		return mApiKey;
	}

	/**
	 * Sets the api key.
	 *
	 * @param pApiKey the new api key
	 */
	public void setApiKey(String pApiKey) {
		this.mApiKey = pApiKey;
	}

	/**
	 * Gets the service host.
	 *
	 * @return the service host
	 */
	public String getServiceHost() {
		return mServiceHost;
	}

	/**
	 * Sets the service host.
	 *
	 * @param pServiceHost the new service host
	 */
	public void setServiceHost(String pServiceHost) {
		this.mServiceHost = pServiceHost;
	}

	
	/**
	 * Gets the request payload.
	 *
	 * @return the request payload
	 */
	public String getRequestPayload() {
		return mRequestPayload;
	}

	/**
	 * Sets the request payload.
	 *
	 * @param pRequestPayload the new request payload
	 */
	public void setRequestPayload(String pRequestPayload) {
		this.mRequestPayload = pRequestPayload;
	}

	/**
	 * Gets the service resourcepath.
	 *
	 * @return the service resourcepath
	 */
	public String getServiceResourcepath() {
		return mServiceResourcepath;
	}

	/**
	 * Sets the service resourcepath.
	 *
	 * @param pServiceResourcepath the new service resourcepath
	 */
	public void setServiceResourcepath(String pServiceResourcepath) {
		this.mServiceResourcepath = pServiceResourcepath;
	}

	/**
	 * Gets the connection timeout.
	 *
	 * @return the connection timeout
	 */
	public int getConnectionTimeout() {
		return mConnectionTimeout;
	}

	/**
	 * Sets the connection timeout.
	 *
	 * @param pConnectionTimeout the new connection timeout
	 */
	public void setConnectionTimeout(int pConnectionTimeout) {
		this.mConnectionTimeout = pConnectionTimeout;
	}

	
}
