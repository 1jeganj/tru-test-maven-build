
package com.tru.radial.service;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.integrations.util.TRUAuditMessageUtil;
import com.tru.integrations.vo.TRUMessageAuditVO;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.exception.TRURadialRequestBuilderException;

/**
 * @author PA
 * The Class TRURadialBaseProcessor.
 */
public class TRURadialBaseProcessor extends GenericService   {
	 
	/** The m radialconfig. */
	private TRURadialConfiguration mRadialconfig;
	
	/** The mClientservice. */
	private TRURadialHttpClientService mClientservice;

	/** The m xsd validation required. */
	private boolean mXsdValidationRequired;
	
	/** The audit message util. */
	private TRUAuditMessageUtil mAuditMessageUtil;
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/** The m exception error code. */
	private String mExceptionErrorCode;
	
	/** The Constant STR_REQUEST. */
	private static final String STR_REQUEST = "=====================Request=====================\n";
	
	/** The Constant STR_RESPONSE. */
	private static final String STR_RESPONSE = "=====================Response====================\n";
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * Transform to object.
	 *
	 * @param <T> the generic type
	 * @param pContent the content
	 * @param pClasz the clasz
	 * @return the t
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	protected <T> T transformToObject(String pContent, Class<T> pClasz) throws TRURadialIntegrationException{
		try {
			JAXBContext jc = JAXBContext.newInstance( pClasz );
			Unmarshaller u = jc.createUnmarshaller();
			StreamSource source = new StreamSource( new StringReader( pContent ));
			return u.unmarshal(source, pClasz).getValue();
		} catch (JAXBException e) {
			vlogError("Exception while transformToObject content: {0}", pContent);
			vlogError("Exception while transformToObject:{0}", e);
			throw new TRURadialIntegrationException(e);
		}
	}

	/**
	 * Transform to xml.
	 *
	 * @param <T> the generic type
	 * @param pObject the object
	 * @param pPackage the package
	 * @return the string
	 */
	protected <T> String transformToXML(T pObject, String pPackage) {
		try {
			StringWriter stringWriter = new StringWriter();
			JAXBContext jc = JAXBContext.newInstance(pPackage);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(pObject, stringWriter);
			return stringWriter.toString();
		} catch (JAXBException e) {
			vlogError("Exception while marshalling:", e.getMessage());
		}
		return null;
		
	}
	
	/**
	 * Gets the xSD filenames.
	 *
	 * @param pOperation the operation
	 * @return the xSD filenames
	 * @throws TRURadialIntegrationException the tRU radial integration exception
	 */
	public String[] getXSDFilenames(String pOperation) throws TRURadialIntegrationException {
		String xsdFileName = mRadialconfig.getXsdMapping().get(pOperation);
		if (StringUtils.isBlank(xsdFileName)) {
			throw new TRURadialIntegrationException(IRadialConstants.XSDFILENAME_IS_NOT_FOUND);
		}
		List<String> xsdIncludingList = mRadialconfig.getXsdIncludingList();
		xsdIncludingList.add(xsdFileName);
		String[] xsdsourceFileNames = xsdIncludingList.toArray(new String[xsdIncludingList.size()]);
		
		return xsdsourceFileNames;
	}
	
	/**
	 * Creates the radial request.
	 *
	 * @param pXmlPayload the xml payload
	 * @param pOperation the operation
	 * @return the radial gateway request
	 * @throws TRURadialIntegrationException the tRU radial integration exception
	 */
	private RadialGatewayRequest createRadialRequest(String pXmlPayload, String pOperation) throws TRURadialIntegrationException {
		String apiKey = mRadialconfig.getApiKey();
		String radialServiceHost = null;
		String resourcepath = null;
		int connectionTimeout = mRadialconfig.getConnectionTimeout();
		if(pOperation == IRadialConstants.NONCE_OPRATION){
			radialServiceHost =mRadialconfig.getNonceServiceHost();
			resourcepath = mRadialconfig.getNonceResourcepath();
		} else if(pOperation == IRadialConstants.SOFT_ALLOCATIONS) {
			radialServiceHost = mRadialconfig.getSoftAllocationServiceHost();
			resourcepath = mRadialconfig.getServiceResourcePath().get(pOperation);
		} else {
			radialServiceHost = mRadialconfig.getServiceHost();
			resourcepath = mRadialconfig.getServiceResourcePath().get(pOperation);
		}
		if (StringUtils.isBlank(resourcepath)) {
			throw new TRURadialIntegrationException(IRadialConstants.RESOURCE_PATH_NOT_FOUND);
		}
		vlogDebug("ApiKey :{0} RadialServiceHost :{1} ResourcePath :{2} ConnectionTimout :{3}", apiKey, radialServiceHost,
				resourcepath, connectionTimeout);
		return new RadialGatewayRequest(apiKey,radialServiceHost,resourcepath,pXmlPayload,connectionTimeout );
		
	}
	
	/**
	 * Execute request.
	 *
	 * @param pXmlPayload the xml payload
	 * @param pOperation the operation
	 * @param pRetry the retry
	 * @return the radial gateway response
	 * @throws TRURadialIntegrationException the tRU radial integration exception
	 */
	protected RadialGatewayResponse executeRequest(String pXmlPayload, String pOperation, int pRetry) throws TRURadialIntegrationException {
		RadialGatewayResponse response = null;
		int retry = 0;
		retryLoop:
			do {
				try {
					response = getClientservice().executeRequest(createRadialRequest(pXmlPayload, pOperation));
					if (response.getStatusCode() == HttpStatus.SC_OK) {
						if(isLoggingDebug()){
							logDebug("Found Success Response exiting retryLoop::");
						}
						break retryLoop;
					}
					retry++;

				} catch (ClientProtocolException clientProtExcep) {
					if(isLoggingError()){
						vlogError("ClientProtocolException while accessing Radial {0}", clientProtExcep);
					}
					throw new TRURadialIntegrationException(clientProtExcep);

				} catch (IOException ioExp) {
					if (isLoggingError()) {
						vlogError("IOException while accessing Radial {0}", ioExp);
					}
					retry++;
					if (retry == pRetry) {
						throw new TRURadialIntegrationException(ioExp);
					}
				} catch (TRURadialRequestBuilderException radReqExp) {
					if(isLoggingError()){
						vlogError("ClientProtocolException while accessing Radial {0}", radReqExp);
					}
					throw new TRURadialIntegrationException(radReqExp);
				}

				if (retry > IRadialConstants.INT_ZERO && retry < pRetry) {
					try {
						Thread.sleep(getRadialconfig().getRetryDelay());
					} catch (InterruptedException e) {
						if(isLoggingError()){
							vlogError("InterruptedException {0}", e);
						}
					}
				}
			} while (retry < pRetry);

		return response;

	}
	
	
	/**
	 * Gets the clientservice.
	 *
	 * @return the clientservice
	 */
	public TRURadialHttpClientService getClientservice() {
		return mClientservice;
	}


	/**
	 * Sets the clientservice.
	 *
	 * @param pClientservice the new clientservice
	 */
	public void setClientservice(TRURadialHttpClientService pClientservice) {
		this.mClientservice = pClientservice;
	}

	/**
	 * Gets the radialconfig.
	 *
	 * @return the radialconfig
	 */
	public TRURadialConfiguration getRadialconfig() {
		return mRadialconfig;
	}

	/**
	 * Sets the radialconfig.
	 *
	 * @param pRadialconfig the new radialconfig
	 */
	public void setRadialconfig(TRURadialConfiguration pRadialconfig) {
		this.mRadialconfig = pRadialconfig;
	}

	
	
	/**
	 * Checks if is xsd validation required.
	 *
	 * @return true, if is xsd validation required
	 */
	public boolean isXsdValidationRequired() {
		return mXsdValidationRequired;
	}


	/**
	 * Sets the xsd validation required.
	 *
	 * @param pXsdValidationRequired the new xsd validation required
	 */
	public void setXsdValidationRequired(boolean pXsdValidationRequired) {
		this.mXsdValidationRequired = pXsdValidationRequired;
	}

	
	/**
	 * Gets the audit message util.
	 *
	 * @return the mAuditMessageUtil
	 */
	public TRUAuditMessageUtil getAuditMessageUtil() {
		return mAuditMessageUtil;
	}

	/**
	 * Sets the audit message util.
	 *
	 * @param pAuditMessageUtil the new audit message util
	 */
	public void setAuditMessageUtil(TRUAuditMessageUtil pAuditMessageUtil) {
		this.mAuditMessageUtil = pAuditMessageUtil;
	}

	/**
	 * This method used for logging req/res in DB.
	 *
	 * @param pOrderId the order id
	 * @param pAPIName the aPI name
	 * @param pReq the pReq
	 * @param pRes the pRes
	 */
	public void auditReqResToDB(String pOrderId,String pAPIName,String pReq,String pRes){
		if(isLoggingDebug()){
			//vlogDebug("Start Of auditReqResToDB() {0}, {1},{2},{3}",pOrderId,pAPIName,req,res);
			vlogDebug("Start Of auditReqResToDB() {0}, {1}",pOrderId,pAPIName);
		}
		if(getAuditMessageUtil().isEnableLogReqResToDB()){
			TRUMessageAuditVO messageAuditVO = new TRUMessageAuditVO();
			messageAuditVO.setOrderId(pOrderId);
			messageAuditVO.setMessageType(pAPIName);
			StringBuffer reqANDres = new StringBuffer();
			reqANDres.append(STR_REQUEST).append(pReq);
			reqANDres.append(STR_RESPONSE).append(pRes);
			messageAuditVO.setMessageData(reqANDres.toString());
			messageAuditVO.setAuditDate(new Timestamp(new Date().getTime()));
			getAuditMessageUtil().auditMessage(messageAuditVO);
		}
		if(isLoggingDebug()){
			vlogDebug("End Of auditReqResToDB()");
		}
	}
	/**
	 * This method used for logging req/res and other parameters in case of error scenarios.
	 *
	 * @param pRequest String
	 * @param pResponse String
	 * @param pOrderId String
	 * @param pEmail the String
	 * @param pApiName the String
	 */
	public void populateErrorParameters(String pRequest,String pResponse,String pOrderId, String pEmail,String pApiName){
		if(isLoggingDebug()){
			vlogDebug("Start Of populateErrorParameters()");
		}  
		vlogError("Error Occured while calling "+pApiName);
		vlogError("Order ID :"+pOrderId);
		vlogError("Email Id :"+pEmail);
		vlogError("API Name :"+pApiName);
		vlogError("Request :"+pRequest);
		vlogError("Response :"+pResponse);
		if(isLoggingDebug()){
			vlogDebug("End Of populateErrorParameters()");
		}
	}
	
	/**
	 * This method used for encrypting the card number for logging purpose.
	 *
	 * @param pAccNumber String
	 * @return the encrypted card number
	 */
	public String getEncryptedCardNumber(String pAccNumber){
		if(isLoggingDebug()){
			vlogDebug("Start Of getEncryptedCardNumber()");
		}
		String encCardNum = IRadialConstants.EMPTY_STRING;
		if(!StringUtils.isBlank(pAccNumber)){
		int length = pAccNumber.length();
		StringBuffer cardNumber = new StringBuffer(pAccNumber);
		StringBuffer charString = new StringBuffer();
		for(int i=IRadialConstants.INT_ZERO;i<length-IRadialConstants.INT_FIVE;i++){
			charString.append(IRadialConstants.ENC_CHAR);
		}
		encCardNum = cardNumber.replace(IRadialConstants.INT_ZERO, length - IRadialConstants.INT_FOUR, charString.toString()).toString();
		}
		if(isLoggingDebug()){
			vlogDebug("End Of getEncryptedCardNumber()");
		}
		return encCardNum;
	}
	
	/**
	 * This method used for encrypting the cvv number for logging purpose.
	 *
	 * @param pCvv String
	 * @return the encrypted CVV number
	 */	
	public String getEncryptedCVVNumber(String pCvv){
		if(isLoggingDebug()){
			vlogDebug("Start Of getEncryptedCVVNumber()");
		}
		String encCVV = IRadialConstants.EMPTY_STRING;
		if(!StringUtils.isBlank(pCvv)){
		int cvvLength = pCvv.length();
		StringBuffer encryptChar = new StringBuffer();
		for(int i=IRadialConstants.INT_ZERO;i< cvvLength;i++){
			encryptChar.append(IRadialConstants.ENC_CHAR);
		}
		encCVV = encryptChar.toString();
		}
		if(isLoggingDebug()){
			vlogDebug("End Of getEncryptedCVVNumber()");
		}
		return encCVV;
	}
	
	/**
	 * Alert logger.
	 *
	 * @param pProcessName the process name
	 * @param pTask the task
	 * @param pOperationName the operation name
	 * @param pTimeTaken the time taken
	 * @param pResponseCode the response code
	 * @param pError the error
	 * @param pPageName the page name
	 */
	public void alertLogger(String pProcessName, String pTask,String pOperationName,String pTimeTaken, String pResponseCode, String pError,String pPageName) {
		Map<String, String> extraParams = new ConcurrentHashMap<String, String>();
		extraParams.put(IRadialConstants.OPERATION_NAME, pOperationName);
		if (StringUtils.isNotBlank(pPageName)) {
			extraParams.put(IRadialConstants.STR_PAGE_NAME, pPageName);
		}
		extraParams.put(IRadialConstants.STR_TIME_TAKEN, pTimeTaken);
		if (StringUtils.isNotBlank(pError)) {
			extraParams.put(IRadialConstants.STR_ERROR_MSG, pError);
		}
		if (StringUtils.isNotBlank(pResponseCode)) {
			extraParams.put(IRadialConstants.STR_REPPONSE_CODE, pResponseCode);
		}
		getAlertLogger().logFailure(pProcessName, pTask, extraParams);
	}

	/**
	 * @return the mExceptionErrorCode
	 */
	public String getExceptionErrorCode() {
		return mExceptionErrorCode;
	}

	/**
	 * @param pExceptionErrorCode the mExceptionErrorCode to set
	 */
	public void setExceptionErrorCode(String pExceptionErrorCode) {
		mExceptionErrorCode = pExceptionErrorCode;
	}
}
