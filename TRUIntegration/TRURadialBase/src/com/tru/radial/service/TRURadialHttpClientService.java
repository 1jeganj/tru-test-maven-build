/*
 * 
 */
package com.tru.radial.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.TRURadialConfiguration;
import com.tru.radial.exception.TRURadialRequestBuilderException;

/**
 * @author PA
 * The Class TRURadialHttpClientService.
 */
public class TRURadialHttpClientService extends GenericService {

	
	/** The m http clientl. */
	private CloseableHttpClient mHttpClientl = null;
	
	/** The m conn manager. */
	private PoolingHttpClientConnectionManager mConnManager = null;
	
	/** The m radial configuration. */
	private TRURadialConfiguration mRadialConfiguration ;
	/**
	 * Method used for initialize configuration of Http Client.
	 * @throws ServiceException
	 *             if an ServiceException occurs
	 */
	public void doStartService() throws ServiceException  {
		if(isLoggingDebug()){
			logDebug("Start : TRURadialHttpClientService.doStartService() ");
		}
		createHttpClient();		
		if(isLoggingDebug()){
			logDebug("End : TRURadialHttpClientService.doStartService() ");
		}
	}

	/**
	 * Method used for closing HTTP Connection and manager.
	 * @throws ServiceException
	 *             if an ServiceException occurs
	 */
	@Override
	public void doStopService() throws ServiceException {
		if (mHttpClientl != null ) {
			try {
				mHttpClientl.close();
				mHttpClientl = null;
				mConnManager.close();
				mConnManager = null;
			} catch (IOException ioException) {
				if(isLoggingError()){
					logError("Colud not initialize HTTP Client", ioException);
				}
			}
		}	
		super.doStopService();	
	}
	
	/**
	 * Execute request.
	 *
	 * @param pDetails the details
	 * @return the radial gateway response
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TRURadialRequestBuilderException the TRU radial request builder exception
	 */
	public RadialGatewayResponse executeRequest(RadialGatewayRequest pDetails) throws ClientProtocolException, IOException,TRURadialRequestBuilderException {
		RadialGatewayResponse response = null;
		HttpUriRequest lRequest = buildRequest(requestURI(pDetails), pDetails.getApiKey(), pDetails.getRequestPayload(),pDetails.getConnectionTimeout());
		response = execute(lRequest);		
		return response;

	}
	
	 /**
 	 * Request uri.
 	 *
 	 * @param pDetails the details
 	 * @return the string
 	 */
 	private String requestURI(RadialGatewayRequest pDetails) {
	        StringBuilder uriBuilder = new StringBuilder();
	        uriBuilder.append(pDetails.getServiceHost())
	        		  //.append("/")
	                  .append(pDetails.getServiceResourcepath());

	        return uriBuilder.toString();
	    }
	 
	/**
	 * Execute.
	 *
	 * @param pRequest the m request
	 * @return the radial gateway response
	 * @throws ClientProtocolException the client protocol exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TRURadialRequestBuilderException the TRU radial request builder exception
	 */
	public RadialGatewayResponse execute(HttpUriRequest pRequest) throws ClientProtocolException, IOException,TRURadialRequestBuilderException
	{
		RadialGatewayResponse httpResponse = null ;
		if (mHttpClientl == null) {
			mHttpClientl = createHttpClient();
		}
		
		if (mConnManager !=null) {
			PoolStats stats1 = mConnManager.getTotalStats();
			if (isLoggingDebug()) {
				vlogDebug("Connections Available: ", stats1.getAvailable());
				vlogDebug("Connections Leased: " ,stats1.getLeased());
				vlogDebug("Connections pending: ",stats1.getPending());
				vlogDebug("Connections max: " ,stats1.getMax());
			}
			
		}
		
		httpResponse = new RadialGatewayResponse(mHttpClientl.execute(pRequest));
		return httpResponse;

	}
	
	
	/**
	 * Creates the http client.
	 *
	 * @return the closeable http client
	 */
	private CloseableHttpClient createHttpClient(){
		
		CloseableHttpClient httpClient;
		if (mConnManager == null) {
			mConnManager = new PoolingHttpClientConnectionManager();
		}
	    if (!getRadialConfiguration().isProxyEnabled()) {
	    	    httpClient = HttpClients.custom()
    			.setConnectionManager(mConnManager)
    			.setMaxConnPerRoute(getRadialConfiguration().getMaxConnPerRoute())
	    		.setMaxConnTotal(getRadialConfiguration().getMaxConnTotal())
    			.build();
	    } else {
	    	HttpHost httpHost  = new HttpHost(getRadialConfiguration().getProxyHost(),getRadialConfiguration().getProxyPort());
		    httpClient = HttpClients.custom()
	    			.setConnectionManager(mConnManager)
	    			.setProxy(httpHost)
	    			.setMaxConnPerRoute(getRadialConfiguration().getMaxConnPerRoute())
	    			.setMaxConnTotal(getRadialConfiguration().getMaxConnTotal())
	    			.build();
	    }
	    
		return httpClient;
	}
	

	
	
 	/**
	  * Builds the request.
	  *
	  * @param pURI the uri
	  * @param pAPIKey the aPI key
	  * @param pBody the body
	  * @param pMinConnectionTimeout the min connection timeout
	  * @return the http uri request
	  * @throws UnsupportedEncodingException the unsupported encoding exception
	  */
	private HttpUriRequest buildRequest(String pURI, String pAPIKey, String pBody, int pMinConnectionTimeout) throws UnsupportedEncodingException
	{	
		HttpHost proxy  = new HttpHost(getRadialConfiguration().getProxyHost(),getRadialConfiguration().getProxyPort(), IRadialConstants.HTTP);
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(pMinConnectionTimeout)
				.setConnectionRequestTimeout(pMinConnectionTimeout)
				.setSocketTimeout(pMinConnectionTimeout)
				.setProxy(proxy)
				.build();
		HttpPost request = new HttpPost(pURI);
		if(pURI.contains(IRadialConstants.INVENTORY)) {
        	request.addHeader(IRadialConstants.CONTENT_TYPE_KEY, getRadialConfiguration().getSoftAllocationContentType());
        } else {
        	request.addHeader(IRadialConstants.CONTENT_TYPE_KEY, getRadialConfiguration().getRequestContentType());
        }

		request.setConfig(config);
		addRequestBodyAndContentType(request, pBody, pAPIKey);
		vlogDebug(" Request header :{0}",request);
		return request;

	}


 	/**
	  * Adds the request body and content type.
	  *
	  * @param pRequest the request
	  * @param pBody the body
	  * @param pAPIKey the aPI key
	  * @throws UnsupportedEncodingException the unsupported encoding exception
	  */
	 private static void addRequestBodyAndContentType(HttpEntityEnclosingRequest pRequest, String pBody, String pAPIKey) throws UnsupportedEncodingException
	 {
		// pRequest.addHeader(IRadialConstants.CONTENT_TYPE_KEY, IRadialConstants.CONTENT_TYPE);
		 pRequest.addHeader(IRadialConstants.API_KEY,pAPIKey);
		 pRequest.setEntity(new StringEntity(pBody));
	 }


	/**
	 * Gets the radial configuration.
	 *
	 * @return the radial configuration
	 */
	public TRURadialConfiguration getRadialConfiguration() {
		return mRadialConfiguration;
	}

	/**
	 * Sets the radial configuration.
	 *
	 * @param pRadialConfiguration the new radial configuration
	 */
	public void setRadialConfiguration(TRURadialConfiguration pRadialConfiguration) {
		this.mRadialConfiguration = pRadialConfiguration;
	}

	
	
}
