package com.tru.radial.validation;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.exception.TRURadialXSDErrorHandler;

/**
 * The Class TRURadialXSDValidator.
 */
public class TRURadialXSDValidator {

	/** The logger. */
	public static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(TRURadialXSDValidator.class);
	

	/**
	 * Validate provided XML against the provided XSD schema files.
	 *
	 * @param pXmlString Path/name of XML file to be validated;
	 *    should not be null or empty.
	 * @param pXsdNames XSDs against which to validate the XML;
	 *    should not be null or empty.
	 * @return true, if successful
	 */
	public static boolean validateXMLWithXSD(final String pXmlString, final String[] pXsdNames)
	{
		if(mLogger.isLoggingDebug()){
			mLogger.logDebug("validateXMLWithXSD process Started.");
		}

		boolean isVaild = true;

		if (pXmlString == null || pXmlString.isEmpty())
		{
			mLogger.logError("Path/name of XML to be validated cannot be null.");
			return false;
		}
		if (pXsdNames == null || pXsdNames.length < IRadialConstants.INT_ONE)
		{
			mLogger.logError("At least one XSD must be provided to validate XML against.");
			return false;
		}
		final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		final StreamSource[] xsdSources = generateStreamSourcesFromXsdPaths(pXsdNames);

		try
		{
			final Schema schema = schemaFactory.newSchema(xsdSources);
			final Validator validator = schema.newValidator();
			if(mLogger.isLoggingDebug()){
				mLogger.logDebug("Validating " + pXmlString + " against XSDs " + Arrays.toString(pXsdNames) + "...");
			}
			StreamSource source = new StreamSource(new StringReader(pXmlString));
			TRURadialXSDErrorHandler customerrorHandler = new TRURadialXSDErrorHandler();
			validator.setErrorHandler(customerrorHandler);
			validator.validate(source);

			if (customerrorHandler.getExceptions() != null && customerrorHandler.getExceptions().size() > 0) {
				
				if (mLogger.isLoggingDebug() && customerrorHandler.getExceptions() != null) {
					mLogger.logDebug("TRURadialXSDErrorHandler.getExceptions() ::" + customerrorHandler.getExceptions().size() + " exceptions/errors");
				}
				
				isVaild = false;
			}

			if (customerrorHandler.getWarnings() != null && customerrorHandler.getWarnings().size() > 0) {
				
				if (mLogger.isLoggingDebug() && customerrorHandler.getWarnings() != null) {
					mLogger.logDebug("TRURadialXSDErrorHandler.getWarnings() ::" + customerrorHandler.getWarnings().size() + " Warnings");
				}
				isVaild = false;
			}
			
		} catch (IOException exception)  {
			mLogger.logError("ERROR: Unable to validate " + pXmlString+ " against XSDs " + Arrays.toString(pXsdNames)+ " - " , exception);
			isVaild = false;
		} catch (SAXException exception) {
			mLogger.logError(exception);
			mLogger.logWarning("Warning: Found SAXException not handled by CustomHandler " + pXmlString+ " against XSDs " + Arrays.toString(pXsdNames)+ " - " , exception);
			isVaild = false;
		} finally{
			if(mLogger.isLoggingDebug()){
				mLogger.logDebug("Validation process completed.");
			}
		}		
		return isVaild;
	}

		/**
		 * Generate stream sources from xsd paths.
		 *
		 * @param pXsdFilesPaths the xsd files paths
		 * @return the stream source[]
		 */
		private static StreamSource[] generateStreamSourcesFromXsdPaths(
				final String[] pXsdFilesPaths)
		{
			final List<StreamSource> streamSources = new ArrayList<>();
			for (final String xsdPath : pXsdFilesPaths)
			{
				InputStream resourceAsStream = TRURadialXSDValidator.class.getClassLoader().getResourceAsStream(xsdPath);
				/*	URL resource = TRURadialXSDValidator.class.getClassLoader().getResource(xsdPath);
			resource.toExternalForm();*/
				streamSources.add(new StreamSource(resourceAsStream));

			}
			return streamSources.toArray(new StreamSource[pXsdFilesPaths.length]);
		}

		/**
		 * Validate source with xsd.
		 *
		 * @param pSource the source
		 * @param pXsdNames the xsd names
		 * @return true, if successful
		 */
		public static boolean validateSourceWithXSD(JAXBSource pSource, final String[] pXsdNames ) {
			mLogger.logDebug("validateSourceWithXSD process Started.");
			boolean isVaild = true;

			if (pSource == null)
			{
				mLogger.logError("source to be validated cannot be null.");
				return false;
			}
			if (pXsdNames == null || pXsdNames.length < IRadialConstants.INT_ONE)
			{
				mLogger.logError("At least one XSD must be provided to validate XML against.");
				return false;
			}
			final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			final StreamSource[] xsdSources = generateStreamSourcesFromXsdPaths(pXsdNames);

			try
			{
				final Schema schema = schemaFactory.newSchema(xsdSources);
				final Validator validator = schema.newValidator();
				mLogger.logDebug("Validating " + pSource.getClass()+ " against XSDs " + Arrays.toString(pXsdNames) + "...");
				validator.setErrorHandler(new TRURadialXSDErrorHandler());
				validator.validate(pSource);
			}
			catch (IOException | SAXException exception)  
			{
				mLogger.logError("ERROR: Unable to validate " + pSource.getClass()
						+ " against XSDs " + Arrays.toString(pXsdNames)
						+ " - " , exception);
			}
			mLogger.logDebug("Validation process completed.");

			return isVaild;
		}

}
