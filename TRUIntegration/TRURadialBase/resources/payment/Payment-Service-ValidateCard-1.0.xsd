<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified"
            targetNamespace="http://api.gsicommerce.com/schema/checkout/1.0"
            xmlns="http://api.gsicommerce.com/schema/checkout/1.0"
            version="${project.version}" >

    <xsd:include schemaLocation="Payment-Datatypes-1.0.xsd"/>

    <xsd:element name="ValidateCardRequest" type="ValidateCardRequestType">
        <xsd:annotation>
            <xsd:documentation xml:lang="en">
                The Request Message for validating a credit card number.
            </xsd:documentation>
        </xsd:annotation>

    </xsd:element>
    <xsd:element name="ValidateCardReply" type="ValidateCardReplyType">
        <xsd:annotation>
            <xsd:documentation xml:lang="en">
                The Reply Message for credit card number validation.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>

    <xsd:complexType name="ValidateCardRequestType">
	    <xsd:sequence>
	        <xsd:element name="PaymentContext" type="PaymentContextType">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    The PaymentContext combines with the tendertype in the URI to uniquely identify a Payment Transaction for an order.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="ExpirationDate" type="xsd:gYearMonth">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    Expiration date of the credit card.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	
	        <xsd:choice minOccurs="0">
	            <xsd:element name="CardSecurityCode" type="CardSecurityCodeType">
	                <xsd:annotation>
	                    <xsd:documentation xml:lang="en">
	                        The CVV2 code found on the back of credit cards.
	                    </xsd:documentation>
	                </xsd:annotation>
	            </xsd:element>
	            <xsd:element name="EncryptedCardSecurityCode" type="EncryptedPayloadType">
	                <xsd:annotation>
	                    <xsd:documentation xml:lang="en">
	                        Used to validate card not present transactions
	                    </xsd:documentation>
	                </xsd:annotation>
	            </xsd:element>
	        </xsd:choice>
	
	        <xsd:element name="CurrencyCode" type="ISOCurrencyCodeType">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    A three character ISO currency code that the store is configured.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="BillingFirstName" type="CustomerNameType">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    First name of the customer on the Billing Address of the credit card
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="BillingLastName" type="CustomerNameType">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    Last name of the customer on the Billing Address of the credit card
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="BillingAddress" type="PhysicalAddressType">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    Billing Address of the credit card.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="BillingPhoneNo" type="xsd:string" >
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    Billing phone number of the customer on the Billing Address of the credit card
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="CustomerEmail" type="EmailAddress">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    E-mail address of the customer making the purchase.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	        <xsd:element name="CustomerIPAddress" type="IPv4Address">
	            <xsd:annotation>
	                <xsd:documentation xml:lang="en">
	                    IP Address of the customer making the purchase.
	                </xsd:documentation>
	            </xsd:annotation>
	        </xsd:element>
	    </xsd:sequence>
        <xsd:attribute name="requestId" type="RequestId" use="required"/>
        <xsd:attribute name="sessionId" type="RequestId" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="ValidateCardReplyType">
        <xsd:sequence>
            <xsd:element name="PaymentContext" type="PaymentContextType"/>
            <xsd:element name="ResponseCode" type="xsd:string">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        Response code for credit card validation.  This includes Valid, invalid, timeout and several correction codes.
                        Please see supporting documentation for a full list of these codes.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:any minOccurs="0" maxOccurs="unbounded" namespace="##any" processContents="skip">
                <xsd:annotation>
                    <xsd:documentation xml:lang="en">
                        This xsd:any element indicates that future optional elements may show up in this location of the XML.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:any>
        </xsd:sequence>
        <xsd:attribute name="sessionId" type="RequestId" use="optional"/>
    </xsd:complexType>

</xsd:schema>
