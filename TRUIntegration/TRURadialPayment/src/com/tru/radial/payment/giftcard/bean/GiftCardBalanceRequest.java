package com.tru.radial.payment.giftcard.bean;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class GiftCardBalanceRequest for capture required i/p params for balance request..
 */
public class GiftCardBalanceRequest implements IRadialRequest {

	/** The m gift card number. */
	private String mGiftCardNumber;
	
	/** The m pin. */
	private String mPin;
	
	
	/**
	 * Gets the GiftCardNumber.
	 * 
	 * @return the mGiftCardNumber
	 */
	public String getGiftCardNumber() {
		return mGiftCardNumber;
	}

	/**
	 * Sets the GiftCardNumber.
	 * 
	 * @param pGiftCardNumber the mGiftCardNumber to set
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		this.mGiftCardNumber = pGiftCardNumber;
	}
	
	/**
	 * Sets the pin.
	 *
	 * @param pPin   the mPin to set
	 */
	public void setPin(String pPin) {
		this.mPin = pPin;
	}

	/**
	 * Gets the pin.
	 *
	 * @return the mPin
	 */
	public String getPin() {
		return mPin;
	}

	
}
