package com.tru.radial.payment.giftcard.bean;

import java.math.BigDecimal;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * The Class GiftCardReedemResponse.
 */
public class GiftCardReedemResponse extends AbstractRadialResponse implements IRadialResponse {


	
	/** The m balance amount. */
	private BigDecimal mBalanceAmount ;

	/**
	 * Gets the balance amount.
	 *
	 * @return the mBalanceAmount
	 */
	public BigDecimal getBalanceAmount() {
		return mBalanceAmount;
	}

	/**
	 * Sets the balance amount.
	 *
	 * @param pBigDecimal the new balance amount
	 */
	public void setBalanceAmount(BigDecimal pBigDecimal) {
		this.mBalanceAmount = pBigDecimal;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;
		
	}

	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setErrors(java.util.List)
	 */

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess = pValue;
		
	}
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = pTmeoutReponse;
	}

	
}
