package com.tru.radial.payment.giftcard.bean;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class GiftCardBalanceRequest for capture required i/p params for balance request..
 */
public class GiftCardReedemRequest implements IRadialRequest {

	/** The m gift card number. */
	private String mGiftCardNumber;
	
	/** The m pin. */
	private String mPin;
	
	/** The m order id. */
	private String mOrderId;
	
	/** The m amount. */
	private double mAmount;
	
	private String mTransactionId;
	/**
	 * Gets the GiftCardNumber.
	 * 
	 * @return the mGiftCardNumber
	 */
	public String getGiftCardNumber() {
		return mGiftCardNumber;
	}

	/**
	 * Sets the GiftCardNumber.
	 * 
	 * @param pGiftCardNumber the mGiftCardNumber to set
	 */
	public void setGiftCardNumber(String pGiftCardNumber) {
		this.mGiftCardNumber = pGiftCardNumber;
	}
	
	/**
	 * Sets the pin.
	 *
	 * @param pPin   the mPin to set
	 */
	public void setPin(String pPin) {
		this.mPin = pPin;
	}

	/**
	 * Gets the pin.
	 *
	 * @return the mPin
	 */
	public String getPin() {
		return mPin;
	}
	
	/**
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId the mOrderId to set
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the mAmount
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param pAmount the new amount
	 */
	public void setAmount(double pAmount) {
		this.mAmount = pAmount;
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the mTransactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId the new transaction id
	 */
	public void setTransactionId(String pTransactionId) {
		this.mTransactionId = pTransactionId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GiftCardReedemRequest [mGiftCardNumber=" + mGiftCardNumber
				+ ", mPin=" + mPin + ", mOrderId=" + mOrderId + ", mAmount="
				+ mAmount + ", mTransactionId=" + mTransactionId + "]";
	}
	
	
	
}
