package com.tru.radial.payment.paypal.bean;

import java.util.HashMap;
import java.util.Map;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * This class has methods to set request properties for DoVoid PayPal API call.
 * 
 * @author PA
 * @version 1.0
 */
public class DoVoidRequest implements IRadialRequest ,  PayPalRequest {

	/**
	 * Property to hold Informational note about this void that is displayed to the buyer in email.
	 */
	private String mNote;
	/**
	 * Property to hold Original authorization ID specifying the authorization the order ID.
	 */
	private String mAuthorizationId;
	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * Property to hold mHashMap.
	 */
	private  Map<String,Object> mRequestParameter=new HashMap<String, Object>();
	/**
	 * @return the authorizationId.
	 */
	public String getAuthorizationId() {
		return mAuthorizationId;
	}

	/**
	 * @param pAuthorizationId the authorizationId to set.
	 */
	public void setAuthorizationId(String pAuthorizationId) {
		mAuthorizationId = pAuthorizationId;
	}

	/**
	 * @return the Note.
	 */
	public String getNote() {
		return mNote;
	}

	/**
	 * @param pNote the Note to set.
	 */
	public void setNote(String pNote) {
		mNote = pNote;
	}

	/**
	 * @return the siteId.
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set.
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * This method is to set any extra properties from Request.
	 * @param pParameter the Parameter to get
	 * @return the object
	 */
	@Override
	public Object getRequestParameter(String pParameter) {
		Object object = mRequestParameter.get(pParameter);
		return object;
	}
	/**
	 * This method is to set any extra properties in Request.
	 * @param pValue to set
	 * @param pParam the Parameter to set
	 */
	@Override
	public void setRequestParameter(String pParam, Object pValue) {
		if(mRequestParameter!= null){
			mRequestParameter.put(pParam, pValue);
		}else {
			mRequestParameter=new HashMap<String, Object>();
			mRequestParameter.put(pParam, pValue);
		}
	}	
}
