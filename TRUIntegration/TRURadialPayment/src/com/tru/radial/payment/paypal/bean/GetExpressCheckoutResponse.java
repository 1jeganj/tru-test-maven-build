package com.tru.radial.payment.paypal.bean;

import java.util.List;

import atg.core.util.ContactInfo;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * Part of asset release version 15.0.
 * @author PA
 * @version 1.0
 */
public class GetExpressCheckoutResponse  extends AbstractRadialResponse implements  IRadialResponse{

	/** The m order id. */
	private String mOrderId;
		
	
	
	/**
	 * Property to hold mPayerId.
	 */
	private String mPayerId;
		
	/**
	 * Property to hold mEmail.
	 */
	private String mEmail;
	
	/**
	 * Property to hold mPayerStatus.
	 */
	private String mPayerStatus;

	/** The payer name. */
	private PayerNameDetails mPayerName;
	
	/** The payer country. */
	private String mPayerCountry;
	
	/** The shipping address. */
	private ContactInfo mShippingAddress;
	/**
	 * Property to hold mPhoneNum.
	 */
	private String mPhoneNum;
	
	
	
	//TODO:Cleanup properties
	
	
	/**
	 * Property to hold mBillingAddress.
	 */
	private String mAddressStatus;
	/**
	 * Property to hold mBillingAddress.
	 */
	private ContactInfo mBillingAddress;
	/**
	 * Property to hold mTimeStamp.
	 */
	private String mTimeStamp;
	
	
	/**
	 * Property to hold mCorrelationId.
	 */
	private String mCorrelationId;
	
	/**
	 * Property to hold mPaymentRequestList.
	 */	
	private List<PaymentDetails> mPaymentRequestList;
	
	
	/**
	 * Property to hold mFirstName.
	 */
	private String mFirstName;
	/**
	 * Property to hold mMiddlename.
	 */
	private String mMiddlename;
	/**
	 * Property to hold mLastName.
	 */
	private String mLastName;
	
	/**
	 * Property to hold mCheckOutStatus.
	 */
	private String mCheckOutStatus;
	/**
	 * Property to hold mCountryCode.
	 */
	private String mCountryCode;

	/**
	 * Property to hold mToken.
	 */
	private String mToken;
	
	
	
	/**
	 * Property to hold mSuccess.
	 */
	//private boolean mSuccess;

	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;


	
	
	/**
	 * @return the mToken
	 */
	public String getToken() {
		return mToken;
	}

	/**
	 * @param pToken the token to set.
	 */
	public void setToken(String pToken) {
		mToken = pToken;
	}
 

	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId - the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * Gets the payer name.
	 *
	 * @return the payer name
	 */
	public PayerNameDetails getPayerName() {
		return mPayerName;
	}

	/**
	 * Sets the payer name.
	 *
	 * @param pPayerName - the new payer name
	 */
	public void setPayerName(PayerNameDetails pPayerName) {
		this.mPayerName = pPayerName;
	}

	/**
	 * Gets the payer country.
	 *
	 * @return the payer country
	 */
	public String getPayerCountry() {
		return mPayerCountry;
	}

	/**
	 * Sets the pPayerCountry country.
	 *
	 * @param pPayerCountry - the new payer country
	 */
	public void setPayerCountry(String pPayerCountry) {
		this.mPayerCountry = pPayerCountry;
	}

	/**
	 * Gets the shipping address.
	 *
	 * @return the shipping address
	 */
	public ContactInfo getShippingAddress() {
		return mShippingAddress;
	}

	/**
	 * Sets the shipping address.
	 *
	 * @param pShippingAddress - the new shipping address
	 */
	public void setShippingAddress(ContactInfo pShippingAddress) {
		this.mShippingAddress = pShippingAddress;
	}

	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	
	
	
	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	@Override
	public String getResponseCode() {
		return mResponseCode;		
	}
		
	
	

	
	/**
	 * @param pValue - value
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode=pValue;
	}
	

	
	
	/**
	 * @param pSuccess the success to set
	 */
	public void setSuccess(boolean pSuccess) {
		super.mIsSuccess = pSuccess;
	}

	/**
	 * @return the paymentRequestList.
	 */
	public List<PaymentDetails> getPaymentRequestList() {
		return mPaymentRequestList;
	}

	/**
	 * @param pPaymentRequestList the paymentRequestList to set.
	 */
	public void setPaymentRequestList(List<PaymentDetails> pPaymentRequestList) {
		mPaymentRequestList = pPaymentRequestList;
	}

	/**
	 * @return the email.
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * @param pEmail the email to set.
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * @return the payerId.
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * @param pPayerId the payerId to set.
	 */
	public void setPayerId(String pPayerId) {
		mPayerId = pPayerId;
	}

	/**
	 * @return the payerStatus.
	 */
	public String getPayerStatus() {
		return mPayerStatus;
	}

	/**
	 * @param pPayerStatus the payerStatus to set.
	 */
	public void setPayerStatus(String pPayerStatus) {
		mPayerStatus = pPayerStatus;
	}

	/**
	 * @return the firstName.
	 */
	public String getFirstName() {
		return mFirstName;
	}

	/**
	 * @param pFirstName the firstName to set.
	 */
	public void setFirstName(String pFirstName) {
		mFirstName = pFirstName;
	}

	/**
	 * @return the middle name.
	 */
	public String getMiddlename() {
		return mMiddlename;
	}

	/**
	 * @param pMiddlename the middle name to set.
	 */
	public void setMiddlename(String pMiddlename) {
		mMiddlename = pMiddlename;
	}

	/**
	 * @return the lastName.
	 */
	public String getLastName() {
		return mLastName;
	}

	/**
	 * @param pLastName the lastName to set.
	 */
	public void setLastName(String pLastName) {
		mLastName = pLastName;
	}

	/**
	 * @return the phoneNum.
	 */
	public String getPhoneNum() {
		return mPhoneNum;
	}

	/**
	 * @param pPhoneNum the phoneNum to set.
	 */
	public void setPhoneNum(String pPhoneNum) {
		mPhoneNum = pPhoneNum;
	}

	/**
	 * @return the checkOutStatus.
	 */
	public String getCheckOutStatus() {
		return mCheckOutStatus;
	}

	/**
	 * @param pCheckOutStatus the checkOutStatus to set.
	 */
	public void setCheckOutStatus(String pCheckOutStatus) {
		mCheckOutStatus = pCheckOutStatus;
	}

	/**
	 * @return the countryCode.
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * @param pCountryCode the countryCode to set.
	 */
	public void setCountryCode(String pCountryCode) {
		mCountryCode = pCountryCode;
	}

	/**
	 * @return the mTimeStamp.
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * @param pTimeStamp the mTimeStamp to set.
	 */
	public void setTimeStamp(String pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}



	/**
	 * @return the correlationId
	 */
	public String getCorrelationId() {
		return mCorrelationId;
	}

	/**
	 * @param pCorrelationId the correlationId to set.
	 */
	public void setCorrelationId(String pCorrelationId) {
		mCorrelationId = pCorrelationId;
	}


	/**
	 * @return the addressStatus
	 */
	public String getAddressStatus() {
		return mAddressStatus;
	}

	/**
	 * @param pAddressStatus the addressStatus to set.
	 */
	public void setAddressStatus(String pAddressStatus) {
		mAddressStatus = pAddressStatus;
	}
	/**
	 * @return the billingAddress.
	 */
	public ContactInfo getBillingAddress() {
		if(mBillingAddress == null){
			mBillingAddress = new ContactInfo();
		}
		return mBillingAddress;
	}

	/**
	 * @param pBillingAddress the billingAddress to set.
	 */
	public void setBillingAddress(ContactInfo pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTimeoutReponse) {
		super.mIsTimeOutResponse = pTimeoutReponse;
		
	}
	

	/** The m ship to name. */
	private String mShipToName;




	/**
	 * Gets the ship to name.
	 *
	 * @return the mShipToName
	 */
	public String getShipToName() {
		return mShipToName;
	}

	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		this.mShipToName = pShipToName;
	}
	
	
}
