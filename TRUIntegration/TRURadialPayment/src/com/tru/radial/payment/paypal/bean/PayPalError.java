package com.tru.radial.payment.paypal.bean;


/**
 * This is a bean class used to contain error code and error message for PayPal Request.
 * 
 * @author PA
 * @version 1.0
 */

public class PayPalError {

	/**
	 * Property to hold mErrorCode.
	 */
	private String mErrorCode;
	/**
	 * Property to hold mErrorMessage.
	 */
	private String mErrorMessage;

	/**
	 * Constructor.
	 * 
	 * @param pErrorCode the ErrorCode to set
	 * @param pErrorMessage the errorMessage to set
	 */
	public PayPalError(String pErrorCode,String pErrorMessage){
		this.mErrorCode=pErrorCode;
		this.mErrorMessage=pErrorMessage;
	}

	/**
	 * @return the errorCode.
	 */
	public String getErrorCode() {
		return mErrorCode;
	}

	/**
	 * @param pErrorCode the errorCode to set.
	 */
	public void setErrorCode(String pErrorCode) {
		mErrorCode = pErrorCode;
	}

	/**
	 * @return the errorMessage.
	 */
	public String getErrorMessage() {
		return mErrorMessage;
	}

	/**
	 * @param pErrorMessage the errorMessage to set.
	 */
	public void setErrorMessage(String pErrorMessage) {
		mErrorMessage = pErrorMessage;
	}

	/**
	 * Overriding To string Method to Print the Request Object. 
	 * 
	 * @return the String object
	 */
	@Override
	public String toString() {
		return new StringBuilder().append("ErrorCode : " + getErrorCode()).append(", ErrorMessage : " + getErrorMessage()).toString();		
	}
}
