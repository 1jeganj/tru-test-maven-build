package com.tru.radial.payment.paypal;

import atg.nucleus.GenericService;

import com.tru.common.cml.SystemException;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.payment.paypal.processor.TRUPayPalPaymentProcessor;

/**
 *@author PA
 *@version 1.0
 */
public class JDAPaypalAdaptorImpl extends GenericService implements PayPalAdaptor{

	
	/** The m paypal service. */
	private TRUPayPalPaymentProcessor mPaypalService;
	/**
	 *@param pPaymentRequest - paymentRequest
	 *@throws SystemException - systemException
	 *@return PayPalResponse
	 */
	@Override
	public IRadialResponse setExpressCheckout(IRadialRequest pPaymentRequest)
			throws SystemException {
		try {
			return getPaypalService().setExpressCheckout(pPaymentRequest);
		} catch (TRURadialIntegrationException e) {
			if(isLoggingError()){
				logError("Got TRURadialIntegrationException" , e);
			}
			throw new SystemException(e.getMessage(),e);
		}
	}
	
	/**
	 *@param pPaymentRequest - paymentRequest
	 *@throws SystemException - systemException
	 *@return PayPalResponse
	 */
	@Override
	public IRadialResponse getExpressCheckout(IRadialRequest pPaymentRequest)
			throws SystemException {
		//return null;//getPaypalService().processGetExpressCheckout(pPaymentRequest);
		 try {
			return getPaypalService().getExpressCheckout(pPaymentRequest);
		} catch (TRURadialIntegrationException e) {
			if(isLoggingError()){
				logError("Got TRURadialIntegrationException" , e);
			}
			throw new SystemException(e.getMessage(),e);
		}
	}
	/**
	 *@param pPaymentRequest - paymentRequest
	 *@throws SystemException - systemException
	 *@return PayPalResponse
	 */
	@Override
	public IRadialResponse doExpressCheckoutPayment(IRadialRequest pPaymentRequest)
			throws SystemException {
		//return null;//getPaypalService().processDoExpressCheckout(pPaymentRequest);
		 try {
				return getPaypalService().doExpressCheckoutPayment(pPaymentRequest);
			} catch (TRURadialIntegrationException e) {
			if(isLoggingError()){
				logError("Got TRURadialIntegrationException" , e);
			}
				throw new SystemException(e.getMessage(),e);
			}
	}
	/**
	 *@param pPaymentRequest - paymentRequest
	 *@throws SystemException - systemException
	 *@return PayPalResponse
	 */
	@Override
	public IRadialResponse doAuthorizaton(IRadialRequest pPaymentRequest)
			throws SystemException {
		//return null;//getPaypalService().processDoAuthorization(pPaymentRequest);
		 try {
				return getPaypalService().doAuthorizaton(pPaymentRequest);
			} catch (TRURadialIntegrationException e) {
				if(isLoggingError()){
					logError("Got TRURadialIntegrationException" , e);
				}
				throw new SystemException(e.getMessage(),e);
			}
	}

	/**
	 * Gets the paypal service.
	 *
	 * @return the paypal service
	 */
	public TRUPayPalPaymentProcessor getPaypalService() {
		return mPaypalService;
	}

	/**
	 * Sets the paypal service.
	 *
	 * @param pPaypalService the new paypal service
	 */
	public void setPaypalService(TRUPayPalPaymentProcessor pPaypalService) {
		mPaypalService = pPaypalService;
	}
	
	
}
