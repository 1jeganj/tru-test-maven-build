package com.tru.radial.payment.paypal.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * This class has methods to set request properties for Authorization API call.
 * @author PA
 * @version 1.0
 */
public class DoAuthorizationPaymentRequest implements IRadialRequest, PayPalRequest {
	
	/** The m order id. */
	private String mOrderId;
	
	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;
	/**
	 * Property to hold mPaymentRequestList.
	 */	
	private List<PaymentDetails> mPaymentRequestList;

	/**
	 * Property to hold mIpAddress.
	 */
	private String mIpAddress;
	/**
	 * Property to hold mTransactionId.
	 */
	private String mTransactionId;
	

	/**
	 * Property to hold mBillingAddress
	 */
	private Address mBillingAddress;
	/**
	 * Property to hold mCurrencyCode
	 */
	private String mCurrencyCode;
	/**
	 * Property to hold mHashMap
	 */
	private  Map<String,Object> mRequestParameter=new HashMap<String, Object>();
	/**
	 * Property to hold mShippingAddress.
	 */
	private Address mShippingAddress;
	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * Property to hold mPaymentAction.
	 */
	private String mPaymentAction;
	
	/** The m page name. */
	private String mPageName;
	
	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the new page name
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}
	/**
	 * @return the transactionId.
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * @param pTransactionId the transactionId to set.
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return mIpAddress;
	}

	/**
	 * @param pIpAddress the ipAddress to set.
	 */
	public void setIpAddress(String pIpAddress) {
		mIpAddress = pIpAddress;
	}
	
	/**
	 * @return the paymentAction.
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set.
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the billingAddress.
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set.
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	/**
	 * @return the currencyCode.
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set.
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**

	/**
	 * @return the shippingAddress.
	 */
	public Address getShippingAddress() {
		return mShippingAddress;
	}
	/**
	 * @param pShippingAddress the shippingAddress to set.
	 */
	public void setShippingAddress(Address pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}
	/**
	 * @return the siteId.
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set.
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * This method is to set any extra properties from Request.
	 * @param pParameter the Parameter to get
	 * @return the object
	 */
	@Override
	public Object getRequestParameter(String pParameter) {
		Object object = mRequestParameter.get(pParameter);
		return object;
	}
	/**
	 * This method is to set any extra properties in Request.
	 * @param pValue to set
	 * @param pParam the Parameter to set
	 */
	@Override
	public void setRequestParameter(String pParam, Object pValue) {
		if(mRequestParameter!= null){
			mRequestParameter.put(pParam, pValue);
		}else {
			mRequestParameter=new HashMap<String, Object>();
			mRequestParameter.put(pParam, pValue);
		}
	}

	/**
	 * @return the paymentRequestList.
	 */
	public List<PaymentDetails> getPaymentRequestList() {
		return mPaymentRequestList;
	}

	/**
	 * @param pPaymentRequestList the paymentRequestList to set.
	 */
	public void setPaymentRequestList(List<PaymentDetails> pPaymentRequestList) {
		mPaymentRequestList = pPaymentRequestList;
	}
	
	/**
	 * Overriding To string Method to Print the Request Object .
	 * 
	 * @return the object
	 *//*
	@Override
	public String toString() {
		return new StringBuilder().
				append("  Amount : " + getAmount()).append(", CurrencyCode" + getCurrencyCode()).append(", IpAddress : "  + getIpAddress()).
				append(", PaymentAction: " + getPaymentAction()).append(", SiteId" +getSiteId()).append(", TransactionId: "  + getTransactionId())
				.append("BillingAddress" +getBillingAddress() ).append(", PaymentRequests : " + getPaymentRequestList())
				.append(", ShippingAddress :" + getShippingAddress())
				.toString();
	}*/

	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}
	/**
	 * overrides the toString method.
	 *
	 * @return String
	 */
	@Override
	public String toString() {
		return "DoAuthorizationPaymentRequest [mOrderId=" + mOrderId
				+ ", mAmount=" + mAmount + ", mIpAddress=" + mIpAddress
				+ ", mCurrencyCode=" + mCurrencyCode + ", mRequestId="
				+ mRequestId + "]";
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	
	/** The request id. */
	private String mRequestId;
	
	
	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public String getRequestId() {
		return mRequestId;
	}

	/**
	 * Sets the request id.
	 *
	 * @param pRequestId the new request id
	 */
	public void setRequestId(String pRequestId) {
		this.mRequestId = pRequestId;
	}

}
