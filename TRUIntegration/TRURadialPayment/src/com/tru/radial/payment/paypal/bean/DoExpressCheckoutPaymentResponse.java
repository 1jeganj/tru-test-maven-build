package com.tru.radial.payment.paypal.bean;

import java.util.List;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.payment.paypal.PaypalConstants;


/**
 * This class has methods to set response properties for DoExpressCheckoutPayment API call.
 * @author PA
 * @version 1.0
 */
public class DoExpressCheckoutPaymentResponse extends AbstractRadialResponse  implements  IRadialResponse {
	
	/** The m order id. */
	private String mOrderId;
	
	/**
	 * Property to hold mTranscationId.
	 */
	private String mTranscationId;
	
	/** The m payment info resp. */
	private PaymentInfoResp mPaymentInfoResp;		
	
	
	/**
	 * Property to hold mVersion.
	 */
	private String mVersion;
	
	/**
	 * Property to hold mCorrelationId.
	 */
	private String mCorrelationId;
	/**
	 * Property to hold mToken.
	 */
	private String mToken;


	/**
	 * Payment Reponce Details.
	 */
	private List<PaymentInfoResp> mPaymentInfo;
	
	/**
	 * Property to hold mErrors.
	 */
	private List<PayPalError> mErrors;

	
	/**
	 * Property to hold mPaymentStatus
	 */
	private String mPaymentStatus;
	/**
	 * Property to hold mSuccess.
	 */

	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;
	
	/**
	 * Getter for the Payment info.
	 * 
	 * @return List PaymentInfo
	 */
	public List<PaymentInfoResp> getPaymentInfo() {
		return mPaymentInfo;
	}

	/**
	 * Setter for the PaymentInfo.
	 * 
	 * @param pPaymentInfo of type list
	 */
	public void setPaymentInfo(List<PaymentInfoResp> pPaymentInfo) {
		this.mPaymentInfo = pPaymentInfo;
	}

	/**
	 * @return the correlationId.
	 */
	public String getCorrelationId() {
		return mCorrelationId;
	}
	/**
	 * @param pCorrelationId the correlationId to set
	 */
	public void setCorrelationId(String pCorrelationId) {
		mCorrelationId = pCorrelationId;
	}
	/**
	 * @return the token.
	 */
	public String getToken() {
		return mToken;
	}

	/**
	 * @param pToken the token to set.
	 */
	public void setToken(String pToken) {
		mToken = pToken;
	}
	
	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the errors.
	 */
	public List<PayPalError> getErrors() {
		return mErrors;
	}

	/**
	 * @param pErrors the errors to set.
	 */
	public void setErrors(List<PayPalError> pErrors) {
		mErrors = pErrors;
	}

	
	/**
	 * @return the paymentStatus.
	 */
	public String getPaymentStatus() {
		return mPaymentStatus;
	}
	/**
	 * @param pPaymentStatus the PaymentStatus to set.
	 */
	public void setPaymentStatus(String pPaymentStatus) {
		mPaymentStatus = pPaymentStatus;
	}
	
	/**
	 * @param pSuccess the success to set.
	 */
	public void setSuccess(boolean pSuccess) {
		super.mIsSuccess = pSuccess;
	}
	
	/**
	 * @return the mTimeStamp.
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}
	/**
	 * @param pTimeStamp the mTimeStamp to set.
	 */
	public void setTimeStamp(String pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}
	/**
	 * @return the version.
	 */
	public String getVersion() {
		return mVersion;
	}
	/**
	 * @param pVersion the version to set.
	 */
	public void setVersion(String pVersion) {
		mVersion = pVersion;
	}
	
	/**
	 * @return the transcationId.
	 */
	public String getTranscationId() {
		return mTranscationId;
	}
	/**
	 * @param pTranscationId the transcationId to set.
	 */
	public void setTranscationId(String pTranscationId) {
		mTranscationId = pTranscationId;
	}

	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 */
	@Override
	public String toString() {				
		return new StringBuilder().			
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.AMOUNT).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getAmount()).			
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.CORRELATION_ID).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getCorrelationId()).
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.ERROR_LIST).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getErrors()).
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.PAYMENT_STATUS).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getPaymentStatus()).			
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.TIME_STAMP).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getTimeStamp()).
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.TOKEN).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getToken()).
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.TRANSACTION_ID).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).append(getVersion()).
			append(PaypalConstants.COMMA).append(PaypalConstants.SPACE).append(PaypalConstants.PAYMENT_INFO).append(PaypalConstants.SPACE).append(PaypalConstants.COLON_STRING).append(PaypalConstants.SPACE).
			append(PaypalConstants.OPEN_BRACKET).append(PaypalConstants.SPACE).append(getPaymentInfo()).append(PaypalConstants.SPACE).append(PaypalConstants.CLOSE_BRACKET).toString();
	}


	
	
	
	/**
	 *@param pValue - value
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode=pValue;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * Gets the payment info resp.
	 *
	 * @return the payment info resp
	 */
	public PaymentInfoResp getPaymentInfoResp() {
		return mPaymentInfoResp;
	}

	/**
	 * Sets the payment info resp.
	 * @param pLPaymentInfoResp - the new payment info resp
	 */
	public void setPaymentInfoResp(PaymentInfoResp pLPaymentInfoResp) {
		this.mPaymentInfoResp = pLPaymentInfoResp;
	}

	/** The m timeout reponse. */
	private boolean mTimeoutReponse;
	
	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#isTimeOutResponse()
	 */
	@Override
	public boolean isTimeOutResponse() {
		return mTimeoutReponse;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTimeoutReponse) {
		mTimeoutReponse = pTimeoutReponse;
		
	}
	
	
}