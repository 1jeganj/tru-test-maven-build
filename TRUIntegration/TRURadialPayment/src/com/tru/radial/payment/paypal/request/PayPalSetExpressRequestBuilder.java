package com.tru.radial.payment.paypal.request;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.RadialAddressUtil;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PayPalSetExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalSetExpressCheckoutRequestType;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutRequest;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;

/**
 * @author PA
 * The Class PayPalSetExpressRequestBuilder.
 */
public class PayPalSetExpressRequestBuilder extends AbstractRequestBuilder {

	/** The m set express checkout request type. */
	PayPalSetExpressCheckoutRequestType mSetExpressCheckoutRequestType = null;
	
	/** The m set express checkout reply type. */
	PayPalSetExpressCheckoutReplyType mSetExpressCheckoutReplyType = null;
	
	/** The m fault response type. */
	FaultResponseType mFaultResponseType = null;

	/** The m request object. */
	JAXBElement<PayPalSetExpressCheckoutRequestType> mRequestObject= null;
	
	/** The m response. */
	SetExpressCheckoutResponse mResponse = null;
	
	/**
	 * Instantiates a new pay pal set express request builder.
	 */
	public PayPalSetExpressRequestBuilder(){ 		
		initSetExpressCheckout();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest)throws TRURadialRequestBuilderException {
		SetExpressCheckoutRequest lSetExpressCheckoutRequest = null;
		if (!(pPaymentRequest instanceof SetExpressCheckoutRequest)) {
			throw new UnsupportedOperationException();
		}
		lSetExpressCheckoutRequest = (SetExpressCheckoutRequest)pPaymentRequest;
		mSetExpressCheckoutRequestType.setOrderId(lSetExpressCheckoutRequest.getOrderId());
		mSetExpressCheckoutRequestType.setReturnUrl(lSetExpressCheckoutRequest.getReturnURL());
		mSetExpressCheckoutRequestType.setCancelUrl(lSetExpressCheckoutRequest.getCancelURL());
		mSetExpressCheckoutRequestType.setLocaleCode(lSetExpressCheckoutRequest.getLocaleCode());
		mSetExpressCheckoutRequestType.setAmount(getAmount());
		mSetExpressCheckoutRequestType.getAmount().setValue((new BigDecimal(lSetExpressCheckoutRequest.getAmount()).setScale(IRadialConstants.INT_TWO, BigDecimal.ROUND_HALF_EVEN)));
		mSetExpressCheckoutRequestType.getAmount().setCurrencyCode(ISOCurrencyCodeType.USD);
		if(StringUtils.isNotBlank(lSetExpressCheckoutRequest.getAddOverRide())){
			mSetExpressCheckoutRequestType.setAddressOverride(lSetExpressCheckoutRequest.getAddOverRide());
		}
		if(StringUtils.isNotBlank(lSetExpressCheckoutRequest.getNoShippingAddressDisplay())){
			mSetExpressCheckoutRequestType.setNoShippingAddressDisplay(lSetExpressCheckoutRequest.getNoShippingAddressDisplay());
		}
		
		if(lSetExpressCheckoutRequest.isStanderdPayPal() || StringUtils.isNotBlank(lSetExpressCheckoutRequest.getShipToName())){
			//user having ispu only and trying to do standard paypal .
			if(StringUtils.isNotBlank(lSetExpressCheckoutRequest.getShipToName())){
			//	mSetExpressCheckoutRequestType.setAddressOverride(IRadialConstants.STRING_ONE);
				mSetExpressCheckoutRequestType.setShipToName(lSetExpressCheckoutRequest.getShipToName());
			//	mSetExpressCheckoutRequestType.setNoShippingAddressDisplay(IRadialConstants.STRING_ONE);
			//	if(null != lSetExpressCheckoutRequest.getShippingAddress() && !StringUtils.isBlank(lSetExpressCheckoutRequest.getShippingAddress().getAddress1())){
					mSetExpressCheckoutRequestType.setShippingAddress(RadialAddressUtil.copyAddress(lSetExpressCheckoutRequest.getShippingAddress()));
			//	}
			}			
		}
		/*	LineItemsType itemsType=new LineItemsType();
		if(lSetExpressCheckoutRequest.getLineItems()!=null && !lSetExpressCheckoutRequest.getLineItems().isEmpty()){
			List<LineItem> lineItems = lSetExpressCheckoutRequest.getLineItems();
			for(LineItem lineItem : lineItems){
				LineItemType itemType = new LineItemType();
				itemType.setName(lineItem.getName());
				itemType.setQuantity(lineItem.getQty());
				itemType.setSequenceNumber(String.valueOf(lineItem.getSeqNo()));
				AmountNegativeAllowedType itemUnitAmount = new AmountNegativeAllowedType();
				itemUnitAmount.setCurrencyCode(ISOCurrencyCodeType.USD);
				itemUnitAmount.setValue(new BigDecimal(lineItem.getUnitPrice()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				//itemUnitAmount.setValue(BigDecimal.valueOf(lineItem.getUnitPrice()));
				itemType.setUnitAmount(itemUnitAmount);
				itemsType.getLineItem().add(itemType);
			}
		}		
		mSetExpressCheckoutRequestType.setLineItems(itemsType);*/
		
		mSetExpressCheckoutRequestType.setSchemaVersion(IRadialConstants.SCHEMA_VERSION);	
		setRequestObject(getRadialObjectFactory().createPayPalSetExpressCheckoutRequest(mSetExpressCheckoutRequestType));
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof SetExpressCheckoutResponse)) {
			throw new UnsupportedOperationException();
		}
		SetExpressCheckoutResponse response = (SetExpressCheckoutResponse)pPaymentResponse;
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getSetExpressCheckoutReplyType().getResponseCode())){
			response.setToken(getSetExpressCheckoutReplyType().getToken());
			response.setSuccess(Boolean.TRUE);		
			response.setTimeStamp(getTimeStamp());	
			response.setResponseCode(getSetExpressCheckoutReplyType().getResponseCode());
		}else{
			buildErrorResponse(pPaymentResponse);
		}	
	}
	
	

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof SetExpressCheckoutResponse)) {
			throw new UnsupportedOperationException();
		}
		SetExpressCheckoutResponse response = (SetExpressCheckoutResponse)pPaymentResponse;				
		super.buildErrorResponse(response,getSetExpressCheckoutReplyType());
		response.setTimeStamp(getTimeStamp());
	}
	

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	private AmountType getAmount() {
		return new AmountType();
	}


	/**
	 * *.
	 */
	private void initSetExpressCheckout(){
		mSetExpressCheckoutRequestType = createPayPalSetExpressCheckoutRequest();
	}
	
	/**
	 * Creates the pay pal set express checkout request.
	 *
	 * @return the pay pal set express checkout request type
	 */
	private PayPalSetExpressCheckoutRequestType createPayPalSetExpressCheckoutRequest() {

		return getRadialObjectFactory().createPayPalSetExpressCheckoutRequestType();
	}


	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<PayPalSetExpressCheckoutRequestType> pGetRequestObject() {
		return mRequestObject;
	}


	/**
	 * Sets the request object.
	 *
	 * @param pRequestType the new request object
	 */
	public void setRequestObject(
			JAXBElement<PayPalSetExpressCheckoutRequestType> pRequestType) {
		this.mRequestObject = pRequestType;
	}


	/**
	 * Gets the sets the express checkout reply type.
	 *
	 * @return the sets the express checkout reply type
	 */
	public PayPalSetExpressCheckoutReplyType getSetExpressCheckoutReplyType() {
		return mSetExpressCheckoutReplyType;
	}

	/**
	 * Sets the sets the express checkout reply type.
	 *
	 * @param pSetExpressCheckoutReplyType the new sets the express checkout reply type
	 */
	public void setSetExpressCheckoutReplyType(
			PayPalSetExpressCheckoutReplyType pSetExpressCheckoutReplyType) {
		this.mSetExpressCheckoutReplyType = pSetExpressCheckoutReplyType;
	}

	
	/**
	 * Gets the fault response type.
	 *
	 * @return the fault response type
	 */
	public FaultResponseType getFaultResponseType() {
		return mFaultResponseType;
	}

	/**
	 * Sets the fault response type.
	 *
	 * @param pFaultResponseType the new fault response type
	 */
	public void setFaultResponseType(FaultResponseType pFaultResponseType) {
		this.mFaultResponseType = pFaultResponseType;
	}

}
