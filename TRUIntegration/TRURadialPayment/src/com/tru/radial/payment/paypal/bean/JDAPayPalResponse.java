package com.tru.radial.payment.paypal.bean;

import java.util.List;

/**
 * This interface has methods to set Payment plugin response.
 * @author PA
 * @version 1.0
 */
public interface JDAPayPalResponse  extends PayPalResponse{

	/**
	 * This method will fetch error list.
	 * @return List type
	 */
	List<PayPalError> getErrors();
	/**
	 * This method will check for whether the transaction is failed or successful.
	 * @return boolean type
	 */
	/**
	 * This method will check for errors.
	 * @param pErrors as list type
	 */
	void setErrors(List<PayPalError> pErrors);
	/**
	 * This method will set Success.
	 * @param pValue as boolean type
	 */
	
	
	/**
	 * Sets the response code.
	 *
	 * @param pValue the new response code
	 */
	void setResponseCode(String pValue);
	
	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	String getResponseCode();
	
	/**
	 * Sets the error message.
	 *
	 * @param pValue the new error message
	 */
	void setErrorMessage(String pValue);
	
	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	String getErrorMessage();
	
	/**
	 * Sets the short error message.
	 *
	 * @param pValue the new short error message
	 */
	void setShortErrorMessage(String pValue);
	
	/**
	 * Gets the short error message.
	 *
	 * @return the short error message
	 */
	String getShortErrorMessage();
	
	/**
	 * Sets the error code.
	 *
	 * @param pValue the new error code
	 */
	void setErrorCode(String pValue);
	
	
	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	String getErrorCode();
	
}
