package com.tru.radial.payment.paypal.bean;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.Address;
import atg.core.util.ContactInfo;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * This class has methods to set request properties for DoExpressCheckoutPaymentRequest API call.
 * @author PA
 *  @version 1.0
 */
public class DoExpressCheckoutPaymentRequest implements PayPalRequest,IRadialRequest {
	
	/** 
	 * The order id. 
	 */
	private String mOrderId;
	
	/**
	 * Property to hold mTokenId.
	 */
	private String mTokenId;

	
	/**
	 * Property to hold mPayerId.
	 */
	private String mPayerId;
	
	
	/** The m ship to name. */
	private String mShipToName;
	
	/** The m pick up store id. */
	private String mPickUpStoreId;
	
	/** The m shipping cost. */
	private BigDecimal mShippingCost;
	
	/** The m tax amount. */
	private BigDecimal mTaxAmount;
	
	/**
	 * Property to hold A message ID used for uniquely identify a message.
	 */
	private String mMsgSubId;
	/**
	 * Property to hold mReqBillingAddress.
	 */
	private String mReqBillingAddress;
	/**
	 * Property to hold mPaymentRequestList.
	 */	
	private List<PaymentDetails> mPaymentRequestList;
	/**
	 * Property to hold mCancelURL.
	 */
	private String mCancelURL;
	/**
	 * Property to hold mReturnURL.
	 */
	private String mReturnURL;
	
	/**
	 * Property to hold mIpAddress.
	 */
	private String mIpAddress;
	/**
	 * Property to hold mTransactionId.
	 */
	private String mTransactionId;
	
	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;
	/**
	 * Property to hold mBillingAddress.
	 */
	private Address mBillingAddress;
	/**
	 * Property to hold mCurrencyCode.
	 */
	private String mCurrencyCode;
	/**
	 * Property to hold mHashMap.
	 */
	private  Map<String,Object> mRequestParameter=new HashMap<String, Object>();
	/**
	 * Property to hold mShippingAddress.
	 */
	private ContactInfo mShippingAddress;
	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * Property to hold mPaymentAction.
	 */
	private String mPaymentAction;
	
	/** The m page name. */
	private String mPageName;
	
	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the new page name
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}

	/**
	 * @return the transactionId.
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * @param pTransactionId the transactionId to set.
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * @return the payerId.
	 */
	public String getPayerId() {
		return mPayerId;
	}

	/**
	 * @param pPayerId
	 *            the payerId to set.
	 */
	public void setPayerId(String pPayerId) {
		mPayerId = pPayerId;
	}

	/**
	 * @return the ipAddress.
	 */
	public String getIpAddress() {
		return mIpAddress;
	}

	/**
	 * @param pIpAddress the ipAddress to set.
	 */
	public void setIpAddress(String pIpAddress) {
		mIpAddress = pIpAddress;
	}
	/**
	 * @return the tokenId.
	 */
	public String getTokenId() {
		return mTokenId;
	}
	/**
	 * @param pTokenId the tokenId to set.
	 */
	public void setTokenId(String pTokenId) {
		mTokenId = pTokenId;
	}
	
	/**
	 * @return the paymentAction.
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set.
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the billingAddress.
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set.
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	/**
	 * @return the currencyCode.
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set.
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**

	/**
	 * @return the shippingAddress.
	 */
	public ContactInfo getShippingAddress() {
		return mShippingAddress;
	}
	/**
	 * @param pShippingAddress the shippingAddress to set.
	 */
	public void setShippingAddress(ContactInfo pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}
	/**
	 * @return the siteId.
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set.
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * This method is to set any extra properties from Request.
	 * @param pParameter the Parameter to get
	 * @return the object
	 */
	@Override
	public Object getRequestParameter(String pParameter) {
		Object object = mRequestParameter.get(pParameter);
		return object;
	}
	/**
	 * This method is to set any extra properties in Request.
	 * @param pValue to set
	 * @param pParam the Parameter to set
	 */
	@Override
	public void setRequestParameter(String pParam, Object pValue) {
		if(mRequestParameter!= null){
			mRequestParameter.put(pParam, pValue);
		}else {
			mRequestParameter=new HashMap<String, Object>();
			mRequestParameter.put(pParam, pValue);
		}
	}

	/**
	 * @return the cancelURL.
	 */
	public String getCancelURL() {
		return mCancelURL;
	}

	/**
	 * @param pCancelURL the cancelURL to set.
	 */
	public void setCancelURL(String pCancelURL) {
		mCancelURL = pCancelURL;
	}

	/**
	 * @return the returnURL.
	 */
	public String getReturnURL() {
		return mReturnURL;
	}

	/**
	 * @param pReturnURL the returnURL to set.
	 */
	public void setReturnURL(String pReturnURL) {
		mReturnURL = pReturnURL;
	}

	/**
	 * @return the paymentRequestList.
	 */
	public List<PaymentDetails> getPaymentRequestList() {
		return mPaymentRequestList;
	}

	/**
	 * @param pPaymentRequestList the paymentRequestList to set.
	 */
	public void setPaymentRequestList(List<PaymentDetails> pPaymentRequestList) {
		mPaymentRequestList = pPaymentRequestList;
	}

	/**
	 * @return the reqBillingAddress.
	 */
	public String getReqBillingAddress() {
		return mReqBillingAddress;
	}

	/**
	 * @param pReqBillingAddress the reqBillingAddress to set.
	 */
	public void setReqBillingAddress(String pReqBillingAddress) {
		mReqBillingAddress = pReqBillingAddress;
	}

	/**
	 * @return the msgSubId.
	 */
	public String getMsgSubId() {
		return mMsgSubId;
	}

	/**
	 * @param pMsgSubId the msgSubId to set.
	 */
	public void setMsgSubId(String pMsgSubId) {
		mMsgSubId = pMsgSubId;
	}
	
	/** The Standard pay pal. */
	private boolean mStanderdPayPal;
	
	/**
	 * @return the standerdPayPal
	 */
	public boolean isStanderdPayPal() {
		return mStanderdPayPal;
	}
	/**
	 * @param pStanderdPayPal the standerdPayPal to set
	 */
	public void setStanderdPayPal(boolean pStanderdPayPal) {
		mStanderdPayPal = pStanderdPayPal;
	}
	
	/** The Express pay pal. */
	private boolean mExpressPayPal;

	/**
	 * @return the expressPayPal
	 */
	public boolean isExpressPayPal() {
		return mExpressPayPal;
	}
	/**
	 * @param pExpressPayPal the expressPayPal to set
	 */
	public void setExpressPayPal(boolean pExpressPayPal) {
		mExpressPayPal = pExpressPayPal;
	}
	
	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}
	
	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	

	/**
	 * Gets the ship to name.
	 *
	 * @return the ship to name
	 */
	public String getShipToName() {
		return mShipToName;
	}

	/**
	 * Sets the ship to name.
	 *
	 * @param pShipToName the new ship to name
	 */
	public void setShipToName(String pShipToName) {
		this.mShipToName = pShipToName;
	}

	/**
	 * Gets the pick up store id.
	 *
	 * @return the pick up store id
	 */
	public String getPickUpStoreId() {
		return mPickUpStoreId;
	}

	/**
	 * Sets the pick up store id.
	 *
	 * @param pPickUpStoreId the new pick up store id
	 */
	public void setPickUpStoreId(String pPickUpStoreId) {
		this.mPickUpStoreId = pPickUpStoreId;
	}

	
	/**
	 * Gets the shipping cost.
	 *
	 * @return the shipping cost
	 */
	public BigDecimal getShippingCost() {
		return mShippingCost;
	}

	/**
	 * Sets the shipping cost.
	 *
	 * @param pShippingCost the new shipping cost
	 */
	public void setShippingCost(BigDecimal pShippingCost) {
		this.mShippingCost = pShippingCost;
	}

	/**
	 * Gets the tax amount.
	 *
	 * @return the tax amount
	 */
	public BigDecimal getTaxAmount() {
		return mTaxAmount;
	}

	/**
	 * Sets the tax amount.
	 *
	 * @param pTaxAmount the new tax amount
	 */
	public void setTaxAmount(BigDecimal pTaxAmount) {
		this.mTaxAmount = pTaxAmount;
	}

	/** The request id. */
	private String mRequestId;
	
	
	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public String getRequestId() {
		return mRequestId;
	}

	/**
	 * Sets the request id.
	 *
	 * @param pRequestId the new request id
	 */
	public void setRequestId(String pRequestId) {
		this.mRequestId = pRequestId;
	}

	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 */
	/*@Override
	public String toString() {
		
		boolean isPayerIdEmpty= false;
		if(StringUtils.isBlank(getPayerId()))
		{
			isPayerIdEmpty= true;
		}
		return new StringBuilder().
				append("  Amount : " + getAmount()).append(", GetCancelUrl" + getCancelURL()).append(", CurrencyCode : "  + getCurrencyCode()).
				append(", IP Address : " + getIpAddress()).append(", PayerId Is  Empty::" +isPayerIdEmpty ).
				append(", PaymentAction: "  + getPaymentAction()).append(", ReqBillingAddress" +getReqBillingAddress() ).
				append(", ReturnUrl : " + getReturnURL()).
				append(", Site Id "+ getSiteId()).append(", Token :" + getTokenId()).
				append(", TransactionId" + getTransactionId()).append(", BillingAddress" + getBillingAddress()).
				append(", PaymentRequestList"+getPaymentRequestList()).
				append(", ShippingAddress" + getShippingAddress()).
				append(", mStanderdPayPal"+ isStanderdPayPal()).
				append(", mExpressPayPal"+ isExpressPayPal()).toString();
		}
	*/
	/** The m sub total. */
	private double mItemsAmount;

	/**
	 * Gets the items amount.
	 *
	 * @return the items amount
	 */
	public double getItemsAmount() {
		return mItemsAmount;
	}

	/**
	 * Sets the items amount.
	 *
	 * @param pItemsAmount the new items amount
	 */
	public void setItemsAmount(double pItemsAmount) {
		this.mItemsAmount = pItemsAmount;
	}
/**
 * @return the String object
 */
	@Override
	public String toString() {
		return "DoExpressCheckoutPaymentRequest [mOrderId=" + mOrderId
				+ ", mTokenId=" + mTokenId + ", mPayerId=" + mPayerId
				+ ", mShipToName=" + mShipToName + ", mPickUpStoreId="
				+ mPickUpStoreId + ", mTaxAmount=" + mTaxAmount
				+ ", mShippingAddress=" + mShippingAddress
				+ ", mStanderdPayPal=" + mStanderdPayPal + ", mExpressPayPal="
				+ mExpressPayPal + "]";
	}
	
	
	/** The commerce items. */
	private List<LineItem> mLineItems;
	
	/**
	 * Gets the line items.
	 *
	 * @return the line items
	 */
	public List<LineItem> getLineItems() {
		return mLineItems;
	}

	/**
	 * Sets the line items.
	 *
	 * @param pLineItems the new line items
	 */
	public void setLineItems(List<LineItem> pLineItems) {
		this.mLineItems = pLineItems;
	}
	
}
