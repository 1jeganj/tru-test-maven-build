package com.tru.radial.payment.paypal.bean;

import java.util.List;

/**
 * This interface has methods to set Payment plugin response.
 * @author PA
 * @version 1.0
 */
public interface PayPalResponse {

	/**
	 * This method will fetch error list.
	 * @return List type
	 */
	List<PayPalError> getErrors();
	/**
	 * This method will check for whether the transaction is failed or successful.
	 * @return boolean type
	 */
	//boolean mIsSuccess();
	/**
	 * This method will check for errors.
	 * @param pErrors as list type
	 */
	void setErrors(List<PayPalError> pErrors);
	/**
	 * This method will set Success.
	 * @param pValue as boolean type
	 */
//	void setSuccess(boolean pValue);
	
	/**
	 * Checks if is time out response.
	 *
	 * @return true, if is time out response
	 */
//	boolean mIsTimeOutResponse();
	
	/**
	 * Sets the timeout reponse.
	 *
	 * @param pTmeoutReponse the new timeout reponse
	 */
//	void setTimeoutReponse(boolean pTmeoutReponse);

	/**
	 * Gets the pay pal error.
	 *
	 * @return the pay pal error
	 */
	 PayPalError getPayPalError();

	/**
	 * Sets the pay pal error.
	 *
	 * @param pPayPalError the new pay pal error
	 */
	void setPayPalError(PayPalError pPayPalError) ;
	
	
}
