package com.tru.radial.payment.paypal.bean;

import java.io.Serializable;
/**
 * AuthorizationInfo is used for authorization.
 * 
 * @author PA
 * @version 1.0
 *
 */
public class AuthorizationInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2637227887387992996L;

	/** The m payment status. */
	private String mPaymentStatus;
	
	/** The m pending reason. */
	private String mPendingReason;
	
	/** The m reason code. */
	private String mReasonCode;

	/**
	 * Gets the payment status.
	 *
	 * @return the payment status
	 */
	public String getPaymentStatus() {
		return mPaymentStatus;
	}

	/**
	 * Sets the payment status.
	 *
	 * @param pPaymentStatus the new payment status
	 */
	public void setPaymentStatus(String pPaymentStatus) {
		this.mPaymentStatus = pPaymentStatus;
	}

	/**
	 * Gets the pending reason.
	 *
	 * @return the pending reason
	 */
	public String getPendingReason() {
		return mPendingReason;
	}

	/**
	 * Sets the pending reason.
	 *
	 * @param pPendingReason the new pending reason
	 */
	public void setPendingReason(String pPendingReason) {
		this.mPendingReason = pPendingReason;
	}

	/**
	 * Gets the reason code.
	 *
	 * @return the reason code
	 */
	public String getReasonCode() {
		return mReasonCode;
	}

	/**
	 * Sets the reason code.
	 *
	 * @param pReasonCode the new reason code
	 */
	public void setReasonCode(String pReasonCode) {
		this.mReasonCode = pReasonCode;
	}
	/**
	 * overrides the toString method.
	 *
	 * @return String
	 */
	@Override
	public String toString() {
		return "AuthorizationInfo [mPaymentStatus=" + mPaymentStatus
				+ ", mPendingReason=" + mPendingReason + ", mReasonCode="
				+ mReasonCode + "]";
	}
	
	
	
}
