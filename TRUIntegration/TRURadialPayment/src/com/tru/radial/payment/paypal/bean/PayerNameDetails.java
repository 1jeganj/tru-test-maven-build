package com.tru.radial.payment.paypal.bean;

import java.io.Serializable;

/**
 * @author PA
 *
 */
public class PayerNameDetails implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5616818981779061626L;

	/** The m honorific. */
	private String mHonorific;
	
	/** The m first name. */
	private String mFirstName;
	
	/** The m last name. */
	private String mLastName;
	
	/** The m middle name. */
	private String mMiddleName;
	
	/**
	 * Gets the honorific.
	 *
	 * @return the honorific
	 */
	public String getHonorific() {
		return mHonorific;
	}
	
	/**
	 * Sets the honorific.
	 *
	 * @param pHonorific the new honorific
	 */
	public void setHonorific(String pHonorific) {
		this.mHonorific = pHonorific;
	}
	
	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return mFirstName;
	}
	
	/**
	 * Sets the first name.
	 *
	 * @param pFirstName the new first name
	 */
	public void setFirstName(String pFirstName) {
		this.mFirstName = pFirstName;
	}
	
	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return mLastName;
	}
	
	/**
	 * Sets the last name.
	 *
	 * @param pLastName the new last name
	 */
	public void setLastName(String pLastName) {
		this.mLastName = pLastName;
	}
	
	/**
	 * Gets the middle name.
	 *
	 * @return the middle name
	 */
	public String getMiddleName() {
		return mMiddleName;
	}
	
	/**
	 * Sets the middle name.
	 *
	 * @param pMiddleName the new middle name
	 */
	public void setMiddleName(String pMiddleName) {
		this.mMiddleName = pMiddleName;
	}
	
	
	
}
