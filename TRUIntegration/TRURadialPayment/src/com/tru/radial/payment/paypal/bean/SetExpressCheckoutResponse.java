package com.tru.radial.payment.paypal.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * Bean class contain response fields for SetExpressCheckout Call.
 * 
 * @author PA
 * @version 1.0
 */
public class SetExpressCheckoutResponse extends AbstractRadialResponse implements  IRadialResponse{
	
	/** 
	 * The order id. 
	 */
	private String mOrderId;
	
	/**
	 * Property to hold mToken.
	 */
	private String mToken;
	
	/**
	 * @return the mToken
	 */
	public String getToken() {
		return mToken;
	}
	
	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}
	
	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	/**
	 * @param pToken the token to set
	 */
	public void setToken(String pToken) {
		mToken = pToken;
	}	
		
	

	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		mResponseCode=pValue;		
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess = pValue;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTimeoutReponse) {
		super.mIsTimeOutResponse = pTimeoutReponse;
		
	}
	
}
