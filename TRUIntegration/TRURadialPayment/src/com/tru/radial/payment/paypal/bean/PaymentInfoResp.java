package com.tru.radial.payment.paypal.bean;


/**
 * 
 * Payment Reponce info bean having detail of the.
 * nth payment info object .
 * @author dnegi
 * @version 1.0
 */
public class PaymentInfoResp {
	
	/**
	 * Nth Payment Info Transation id.
	 */
	private String mPaymentInfoTransationId;
	
	/**
	 * Nth Payment Info amount.  
	 */
	private String mPaymentInfoAmt;
	
	/**
	 * nth Payment Status. 
	 */
	private String mPaymentStatus;
	
	/**
	 * nth Pending Reason.
	 */
	private String mPendingReasons;
	
	/**
	 * nth Reason code. 
	 */
	private String mReasonCode;
	/**
	 * Property to hold mPayPalError.
	 */
	private PayPalError mPayPalError;
	
	/**
	 * @return the payPalError.
	 */
	public PayPalError getPayPalError() {
		return mPayPalError;
	}

	/**
	 * @param pPayPalError the payPalError to set.
	 */
	public void setPayPalError(PayPalError pPayPalError) {
		mPayPalError = pPayPalError;
	}

	/**
	 * 
	 * Getter for reason code.
	 * @return String Reason Code 
	 */
	public String getReasonCode() {
		return mReasonCode;
	}
	
	/**
	 * Setter for the reason code.
	 * @param pReasonCode ReasonCode
	 */
	public void setReasonCode(String pReasonCode) {
		this.mReasonCode = pReasonCode;
	}

	/**
	 * Getter for the trasaction id 
	 * @return String Payment transation id 
	 */
	public String getPaymentInfoTransationId() {
		return mPaymentInfoTransationId;
	}
	
	/**
	 *  Getter for the payment trasation id.
	 * @param pPaymentInfoTransationId of type string 
	 */
	public void setPaymentInfoTransationId(String pPaymentInfoTransationId) {
		this.mPaymentInfoTransationId = pPaymentInfoTransationId;
	}
	
	/**
	 * Getter.
	 * 
	 * @return String payment info amount 
	 */
	public String getPaymentInfoAmt() {
		return mPaymentInfoAmt;
	}
	
	/**
	 * Setter for payment info amount. 
	 * @param pPaymentInfoAmt PaymentInfoAmt
	 */
	public void setPaymentInfoAmt(String pPaymentInfoAmt) {
		this.mPaymentInfoAmt = pPaymentInfoAmt;
	}
	
	/**
	 * Payment Status.
	 * 
	 * @return String Payment status 
	 */
	public String getPaymentStatus() {
		return mPaymentStatus;
	}
	
	/**
	 * Setter for payment status. 
	 * @param pPaymentStatus PaymentStatus
	 */
	public void setPaymentStatus(String pPaymentStatus) {
		this.mPaymentStatus = pPaymentStatus;
	}
	
	/**
	 * Getter Pensing reason.
	 * @return String Pending Reason
	 */
	public String getPendingReasons() {
		return mPendingReasons;
	}
	
	/**
	 * 
	 * Setter for Pending reason.
	 * @param pPendingReasons of type String 
	 */
	public void setPendingReasons(String pPendingReasons) {
		this.mPendingReasons = pPendingReasons;
	}
	
	/**
	 * Overriding To string Method to Print the Request Object. 
	 * 
	 * @return the String object
	 */
	@Override
	public String toString() {
		return new StringBuilder().append(" PaymenInforAmt : " + getPaymentInfoAmt()).
				append(" , PaymentInfoTransationId : " + getPaymentInfoTransationId()).append(" , PayPalError Error: " + getPayPalError()).
			    append(" ,PaymentStatus :" + getPaymentStatus()).
			    append(", Pending reasons : " + getPendingReasons()).append(" , ReasonCode: " + getReasonCode()).toString();
		
	}

}
