package com.tru.radial.payment.paypal.processor;

import java.util.Calendar;

import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;

import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.PayPalDoAuthorizationReplyType;
import com.tru.radial.payment.PayPalDoExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalGetExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalSetExpressCheckoutReplyType;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentResponse;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentResponse;
import com.tru.radial.payment.paypal.bean.GetExpressCheckOutRequest;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.bean.PayPalRequest;
import com.tru.radial.payment.paypal.bean.PayPalResponse;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutRequest;
import com.tru.radial.payment.paypal.bean.SetExpressCheckoutResponse;
import com.tru.radial.payment.paypal.request.PayPalDoAuthRequestBuilder;
import com.tru.radial.payment.paypal.request.PayPalDoExpressRequestBuilder;
import com.tru.radial.payment.paypal.request.PayPalGetExpressRequestBuilder;
import com.tru.radial.payment.paypal.request.PayPalSetExpressRequestBuilder;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;
import com.tru.radial.validation.TRURadialXSDValidator;

/**
 * <p>
 * This is paypal implementation call for all radial based APIs for checkout.
 * </p>
 * The Class TRUPayPalPaymentProcessor.
 */
public class TRUPayPalPaymentProcessor extends TRURadialBaseProcessor {
	
	
	/** The Constant SET_EXPRESS_CHECKOUT_FILENAME. */
	private  static final String  SET_EXPRESS_CHECKOUT_FILENAME = "setExpressCheckout";

	/** The Constant GET_EXPRESS_CHECKOUT_FILENAME. */
	private  static final String  GET_EXPRESS_CHECKOUT_FILENAME = "getExpressCheckout";
	
	/** The Constant DO_EXPRESS_CHECKOUT_FILENAME. */
	private  static final String  DO_EXPRESS_CHECKOUT_FILENAME = "doExpressCheckout";
		
	/** The Constant DO_AUTHORIZATION_CHECKOUT_FILENAME. */
	private  static final String  DO_AUTHORIZATION_CHECKOUT_FILENAME = "doPaypalAuth";
		
	/** To manage xsd validation. */
	private boolean mXsdValidationRequired = true;
	
	/** The m integration info logger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	

	
	/**
	 * This method is used to invoke setExpressCheckout call of PayPal API.
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public IRadialResponse setExpressCheckout(IRadialRequest pPaymentRequest) throws TRURadialIntegrationException {				
		if(isLoggingDebug()){
			logDebug("Start setExpressCheckout()");
		}
		//construct request
		IRadialResponse setExpressResponse = new SetExpressCheckoutResponse();
		long starTime = 0;
		long endTime = 0;
		String responseXML=null;
		boolean validateXMLWithXSD = false;
		PayPalSetExpressRequestBuilder setExpressBuilder = new PayPalSetExpressRequestBuilder();
		String pageName = ((SetExpressCheckoutRequest)pPaymentRequest).getPageName();
		try {
			//Create the readial request
			setExpressBuilder.buildRequest(pPaymentRequest);
			//do transform to xml
			String requestXML = transformToXML(setExpressBuilder.pGetRequestObject(), IRadialConstants.PACKAGE_NAME);
			if(isLoggingDebug()){
				vlogDebug("Request XML :{0}",setExpressBuilder.formatXML(requestXML));
			}
			//validate xml with xsd
			if (isXsdValidationRequired()) {
				validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(SET_EXPRESS_CHECKOUT_FILENAME));
			}
			if(isLoggingDebug()){
				vlogDebug("XSD Validation for the setExpressCheckout Request is {0}",validateXMLWithXSD);
			}
			//call service
			
			SetExpressCheckoutRequest lSetExpressCheckoutRequest = null;
			lSetExpressCheckoutRequest = (SetExpressCheckoutRequest)pPaymentRequest;
			
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(SET_EXPRESS_CHECKOUT_FILENAME), lSetExpressCheckoutRequest.getOrderId(), null);
			RadialGatewayResponse response = executeRequest(requestXML, SET_EXPRESS_CHECKOUT_FILENAME, getRadialconfig().getMaxReTry());
			endTime = Calendar.getInstance().getTimeInMillis();
			if(null != response){
				responseXML =setExpressBuilder.formatXML(response.getResponseBody());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(SET_EXPRESS_CHECKOUT_FILENAME), lSetExpressCheckoutRequest.getOrderId(), null);
			}
			if(isLoggingDebug()){			
				vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
				vlogDebug("Response XML :{0}",responseXML);
			}		
			super.auditReqResToDB(((SetExpressCheckoutRequest)pPaymentRequest).getOrderId(), SET_EXPRESS_CHECKOUT_FILENAME, requestXML, responseXML);
			 if(response == null){	
				 //This is for if not response from service,Then treat as timeout and create error code for front end purpose.
				 setExpressBuilder.buildTimeOutResponse(setExpressResponse);
				 String errorMessage = null;
				 if (setExpressResponse != null && setExpressResponse.getRadialError() != null) {
					 errorMessage = setExpressResponse.getRadialError().getErrorMessage();
				 }
			 	 alertLogger(String.valueOf(endTime-starTime),setExpressResponse.getResponseCode(),errorMessage,pageName,SET_EXPRESS_CHECKOUT_FILENAME);
			}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				setExpressBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				setExpressBuilder.buildFaultResponse(setExpressResponse);
				String errorMessage = null;
				String responseCode = null;
				if (setExpressResponse != null && setExpressResponse.getRadialError() != null) {
					errorMessage = setExpressResponse.getRadialError().getErrorMessage();
					responseCode = setExpressResponse.getRadialError().getErrorCode();
				}
				alertLogger(String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,SET_EXPRESS_CHECKOUT_FILENAME);
			} else if (!response.getResponseBody().isEmpty() ) {
				setExpressBuilder.setSetExpressCheckoutReplyType(transformToObject(response.getResponseBody(), PayPalSetExpressCheckoutReplyType.class));
				setExpressBuilder.buildResponse(setExpressResponse);
				if (!IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(setExpressResponse.getResponseCode())) {
					String errorMessage = null;
					if (setExpressResponse != null && setExpressResponse.getRadialError() != null) {
						errorMessage = setExpressResponse.getRadialError().getErrorMessage();
					}
					alertLogger(String.valueOf(endTime-starTime),setExpressResponse.getResponseCode(),errorMessage,pageName,SET_EXPRESS_CHECKOUT_FILENAME);
				}
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,SET_EXPRESS_CHECKOUT_FILENAME);
			throw exe;
		} 		
		 if(isLoggingDebug()){
		 logDebug("End setExpressCheckout()");
		}
		return setExpressResponse;
	}
	

	/**
	 * This method is used to invoke getExpressCheckoutDetails call of PayPal API. 
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */	
	public IRadialResponse getExpressCheckout(IRadialRequest pPaymentRequest) throws TRURadialIntegrationException {
		if(isLoggingDebug()){
			logDebug("Start getExpressCheckout()");
		}
		//construct request
		IRadialResponse getExpressResponse = new GetExpressCheckoutResponse();
		PayPalGetExpressRequestBuilder getExpressBuilder = new PayPalGetExpressRequestBuilder();
		getExpressBuilder.buildRequest(pPaymentRequest);
		long starTime = 0;
		long endTime = 0;
		String pageName = ((GetExpressCheckOutRequest)pPaymentRequest).getPageName();
		//do transform to xml
		String requestXML = transformToXML(getExpressBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
		try {
			if(isLoggingDebug()){
				vlogDebug("{0}",getExpressBuilder.formatXML(requestXML));
			}
			//validate xml with xsd
			if (isXsdValidationRequired()) {
				boolean validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(GET_EXPRESS_CHECKOUT_FILENAME));
				if(isLoggingDebug()){
					vlogDebug("XSD Validation for the setExpressCheckout Request is {0}",validateXMLWithXSD);
				}
			}
			//call service
			
			GetExpressCheckOutRequest getExpressChkoutReqVO = null;
			getExpressChkoutReqVO = (GetExpressCheckOutRequest)pPaymentRequest;
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(GET_EXPRESS_CHECKOUT_FILENAME), getExpressChkoutReqVO.getOrderId(), null);
			RadialGatewayResponse response = executeRequest(requestXML, GET_EXPRESS_CHECKOUT_FILENAME, getRadialconfig().getMaxReTry());
			endTime = Calendar.getInstance().getTimeInMillis();
			String responseXML=null;
			if(response != null){
				responseXML = getExpressBuilder.formatXML(response.getResponseBody());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(GET_EXPRESS_CHECKOUT_FILENAME), getExpressChkoutReqVO.getOrderId(), null);
			}
			if(isLoggingDebug()){			
				vlogDebug("Response XML :{0}",responseXML);
			}
			
			/*if(isEnablePaypalTimeOutTesting()){
				response = null;
			}*/
			 super.auditReqResToDB(((GetExpressCheckOutRequest)pPaymentRequest).getOrderId(), GET_EXPRESS_CHECKOUT_FILENAME, requestXML, responseXML);
			if(response == null){	
				//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
				getExpressBuilder.buildTimeOutResponse(getExpressResponse);
				String errorMessage = null;
				if (getExpressResponse != null && getExpressResponse.getRadialError() != null) {
					errorMessage = getExpressResponse.getRadialError().getErrorMessage();
				}
			 	alertLogger(String.valueOf(endTime-starTime),getExpressResponse.getResponseCode(),errorMessage,pageName,GET_EXPRESS_CHECKOUT_FILENAME);
			}else if (response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				getExpressBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				getExpressBuilder.buildFaultResponse(getExpressResponse);
				String errorMessage = null;
				String responseCode = null;
				if (getExpressResponse != null && getExpressResponse.getRadialError() != null) {
					errorMessage = getExpressResponse.getRadialError().getErrorMessage();
					responseCode = getExpressResponse.getRadialError().getErrorCode();
				}
				alertLogger(String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,GET_EXPRESS_CHECKOUT_FILENAME);
			}else if (!response.getResponseBody().isEmpty() ) {
				getExpressBuilder.setGetExpressCheckoutReplyType(transformToObject(response.getResponseBody(), PayPalGetExpressCheckoutReplyType.class));
				getExpressBuilder.buildResponse(getExpressResponse);
				//Validate the State Code and State Name 
				if(getExpressResponse.isSuccess()){
					ContactInfo shippingAddress = ((GetExpressCheckoutResponse)getExpressResponse).getShippingAddress();
					if(shippingAddress!=null){
						updateAddressState(shippingAddress);
					}
						
					ContactInfo billingAddress = ((GetExpressCheckoutResponse)getExpressResponse).getBillingAddress();
					if(billingAddress!=null){
						updateAddressState(billingAddress);
					}
				}
				if (!IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getExpressResponse.getResponseCode())) {
					String errorMessage = null;
					if (getExpressResponse != null && getExpressResponse.getRadialError() != null) {
						errorMessage = getExpressResponse.getRadialError().getErrorMessage();
					}
					alertLogger(String.valueOf(endTime-starTime),getExpressResponse.getResponseCode(),errorMessage,pageName,GET_EXPRESS_CHECKOUT_FILENAME);
				}
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,GET_EXPRESS_CHECKOUT_FILENAME);
			throw exe;
		} 		
		 if(isLoggingDebug()){
				logDebug("End getExpressCheckout()");
			}
		return getExpressResponse;
	}

	
	/**
	 * Update address state.
	 *
	 * @param pAddress the address
	 */
	private void updateAddressState(ContactInfo pAddress) {
		String state = pAddress.getState();
		if(isLoggingDebug()){
			logDebug("Start updateAddressState() and State Value :"+state);
		}
		//Default Name
		if(StringUtils.isNotBlank(state) && state.length()>IRadialConstants.INT_TWO && getRadialconfig().getPaypalStateCodes()!=null ){
			String stateName = state.toUpperCase();
			String stateCode = null;
			if( getRadialconfig().getVirginIslandsStateName().equalsIgnoreCase(stateName)){
				stateCode = getRadialconfig().getPaypalStateCodes().get(IRadialConstants.VIRGIN_ISLANDS);
			}else{
				stateCode = getRadialconfig().getPaypalStateCodes().get(stateName);
			}			
			if(isLoggingDebug()){
				vlogDebug("State Code:{0} and State Name:{1}",stateCode,stateName);
			}
			if(StringUtils.isNotBlank(stateCode)){
				pAddress.setState(stateCode);
			}
		}
	}

	/**
	 * This method is used to invoke doExpressCheckoutPayment call of PayPal API. 
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */	
	public IRadialResponse doExpressCheckoutPayment(IRadialRequest pPaymentRequest) throws TRURadialIntegrationException {
		if(isLoggingDebug()){
			logDebug("Start doExpressCheckoutPayment()");
		}
		//construct request
		IRadialResponse doExpressResponse = new DoExpressCheckoutPaymentResponse();
		PayPalDoExpressRequestBuilder doExpressRequestBuilder = new PayPalDoExpressRequestBuilder();
		long starTime = 0;
		long endTime = 0;
		String pageName = ((DoExpressCheckoutPaymentRequest)pPaymentRequest).getPageName();
		try {
			doExpressRequestBuilder.buildRequest(pPaymentRequest);
			//do transform to xml
			String requestXML = transformToXML(doExpressRequestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
			if(isLoggingDebug()){
				vlogDebug("{0}",doExpressRequestBuilder.formatXML(requestXML));
			}
			//validate xml with xsd
			if (isXsdValidationRequired()) {
				boolean validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(DO_EXPRESS_CHECKOUT_FILENAME));
				if(isLoggingDebug()){
					vlogDebug("XSD Validation for the doExpressCheckoutPayment Request is {0}",validateXMLWithXSD);
				}
			}
			//call service
			DoExpressCheckoutPaymentRequest doExpressCheckoutRequestVO = (DoExpressCheckoutPaymentRequest)pPaymentRequest;
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(DO_EXPRESS_CHECKOUT_FILENAME), doExpressCheckoutRequestVO.getOrderId(), null);
			RadialGatewayResponse response = executeRequest(requestXML, DO_EXPRESS_CHECKOUT_FILENAME, getRadialconfig().getMaxReTry());			
			endTime = Calendar.getInstance().getTimeInMillis();
			String responseXML=null;
			if(response != null){
				responseXML = doExpressRequestBuilder.formatXML(response.getResponseBody());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(DO_EXPRESS_CHECKOUT_FILENAME), doExpressCheckoutRequestVO.getOrderId(), null);
			}		
			if(isLoggingDebug()){			
				vlogDebug("Response XML :{0}",responseXML);
			}
			super.auditReqResToDB(((DoExpressCheckoutPaymentRequest)pPaymentRequest).getOrderId(), DO_EXPRESS_CHECKOUT_FILENAME, requestXML, responseXML);
			if(response == null){
				//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
				doExpressRequestBuilder.buildTimeOutResponse(doExpressResponse);
				String errorMessage = null;
				if (doExpressResponse != null && doExpressResponse.getRadialError() != null) {
					errorMessage = doExpressResponse.getRadialError().getErrorMessage();
				}
				alertLogger(String.valueOf(endTime-starTime),doExpressResponse.getResponseCode(),errorMessage,pageName,DO_EXPRESS_CHECKOUT_FILENAME);
			}else if ( response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				doExpressRequestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				doExpressRequestBuilder.buildFaultResponse(doExpressResponse);
				String errorMessage = null;
				String responseCode = null;
				if (doExpressResponse != null && doExpressResponse.getRadialError() != null) {
					errorMessage = doExpressResponse.getRadialError().getErrorMessage();
					responseCode = doExpressResponse.getRadialError().getErrorCode();
				}
				alertLogger(String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,DO_EXPRESS_CHECKOUT_FILENAME);
			} else if (!response.getResponseBody().isEmpty() ) {
				doExpressRequestBuilder.setDoExpressCheckoutReplyType(transformToObject(response.getResponseBody(), PayPalDoExpressCheckoutReplyType.class));
				doExpressRequestBuilder.buildResponse(doExpressResponse);
				if (!IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(doExpressResponse.getResponseCode())) {
					String errorMessage = null;
					if (doExpressResponse != null && doExpressResponse.getRadialError() != null) {
						errorMessage = doExpressResponse.getRadialError().getErrorMessage();
					}
					alertLogger(String.valueOf(endTime-starTime),doExpressResponse.getResponseCode(),errorMessage,pageName,DO_EXPRESS_CHECKOUT_FILENAME);
				}
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,DO_EXPRESS_CHECKOUT_FILENAME);
			throw exe;
		} 		
		if(isLoggingDebug()){
			logDebug("End doExpressCheckoutPayment()");
		}
		return doExpressResponse;
	}

	/**
	 * This method is used to invoke doAuthorizaton call of PayPal API. 
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */	
	public IRadialResponse doAuthorizaton(IRadialRequest pPaymentRequest) throws TRURadialIntegrationException {
		if(isLoggingDebug()){
			logDebug("Start doAuthorizaton()");
		}
		boolean validateXMLWithXSD = false;
		String responseXML = null;
		//construct request
		IRadialResponse doAuthResponse = new DoAuthorizationPaymentResponse();
		PayPalDoAuthRequestBuilder doAuthorizationRequestBuilder = new PayPalDoAuthRequestBuilder();
		long starTime = 0;
		long endTime = 0;
		String pageName = ((DoAuthorizationPaymentRequest)pPaymentRequest).getPageName();
		try {
			doAuthorizationRequestBuilder.buildRequest(pPaymentRequest);
			//do transform to xml
			String requestXML = transformToXML(doAuthorizationRequestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
			if(isLoggingDebug()){
				vlogDebug("{0}",requestXML);
			}
			
			//validate xml with xsd
			if (isXsdValidationRequired()) {
				validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(requestXML, getXSDFilenames(DO_AUTHORIZATION_CHECKOUT_FILENAME));
			}
			//call service
			DoAuthorizationPaymentRequest doAuthorizationCheckoutVO = (DoAuthorizationPaymentRequest)pPaymentRequest;	
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
					getIntegrationInfoLogger().getInterfaceNameMap().get(DO_AUTHORIZATION_CHECKOUT_FILENAME), doAuthorizationCheckoutVO.getOrderId(), null);
			RadialGatewayResponse response = executeRequest(requestXML, DO_AUTHORIZATION_CHECKOUT_FILENAME, getRadialconfig().getMaxReTry());
			endTime = Calendar.getInstance().getTimeInMillis();

			if(response != null){
				responseXML = doAuthorizationRequestBuilder.formatXML(response.getResponseBody());
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_PAYMENT), 
						getIntegrationInfoLogger().getInterfaceNameMap().get(DO_AUTHORIZATION_CHECKOUT_FILENAME), doAuthorizationCheckoutVO.getOrderId(), null);
			}
			if(isLoggingDebug()){			
				vlogDebug("Is validateXMLWithXSD success ? {0}",validateXMLWithXSD);
				vlogDebug("Response XML :{0}",responseXML);
			}
			super.auditReqResToDB(((DoAuthorizationPaymentRequest)pPaymentRequest).getOrderId(), DO_AUTHORIZATION_CHECKOUT_FILENAME, requestXML, responseXML);
			if(response == null){
				//This is for if not response from service,Then treat as timeout and create error code for front end purpose.
				doAuthorizationRequestBuilder.buildTimeOutResponse(doAuthResponse);
				String errorMessage = null;
				if (doAuthResponse != null && doAuthResponse.getRadialError() != null) {
					errorMessage = doAuthResponse.getRadialError().getErrorMessage();
				}
				alertLogger(String.valueOf(endTime-starTime),doAuthResponse.getResponseCode(),errorMessage,pageName,DO_AUTHORIZATION_CHECKOUT_FILENAME);
			}else if (response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				doAuthorizationRequestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				doAuthorizationRequestBuilder.buildFaultResponse(doAuthResponse);
				String errorMessage = null;
				String responseCode = null;
				if (doAuthResponse != null && doAuthResponse.getRadialError() != null) {
					errorMessage = doAuthResponse.getRadialError().getErrorMessage();
					responseCode = doAuthResponse.getRadialError().getErrorCode();
				}
				alertLogger(String.valueOf(endTime-starTime),responseCode,errorMessage,pageName,DO_AUTHORIZATION_CHECKOUT_FILENAME);
			} else if (!response.getResponseBody().isEmpty() ) {
				doAuthorizationRequestBuilder.setDoAuthorizationReplyType(transformToObject(response.getResponseBody(), PayPalDoAuthorizationReplyType.class));
				doAuthorizationRequestBuilder.buildResponse(doAuthResponse);
				if (!(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(doAuthResponse.getResponseCode()) || IRadialConstants.RADIAL_SUCCESS_WITH_WARNING_RESPONSE_CODE.equalsIgnoreCase(doAuthResponse.getResponseCode()))) {
					String errorMessage = null;
					if (doAuthResponse != null && doAuthResponse.getRadialError() != null) {
						errorMessage = doAuthResponse.getRadialError().getErrorMessage();
					}
					alertLogger(String.valueOf(endTime-starTime),doAuthResponse.getResponseCode(),errorMessage,pageName,DO_AUTHORIZATION_CHECKOUT_FILENAME);
				}
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage(),pageName,DO_AUTHORIZATION_CHECKOUT_FILENAME);
			throw exe;
		}		
		if(isLoggingDebug()){
			logDebug("End doAuthorizaton()");
		}
		return doAuthResponse;
	}

	/**
	 * This method is used to invoke doVoid call of PayPal API. 
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */	
	PayPalResponse doVoid(PayPalRequest pPaymentRequest) throws TRURadialIntegrationException {		
		return null;
	}

	
	/**
	 * Checks if is xsd validation required.
	 *
	 * @return true, if is xsd validation required
	 */
	public boolean isXsdValidationRequired() {
		return mXsdValidationRequired;
	}


	/**
	 * Sets the xsd validation required.
	 *
	 * @param pXsdValidationRequired the new xsd validation required
	 */
	public void setXsdValidationRequired(boolean pXsdValidationRequired) {
		this.mXsdValidationRequired = pXsdValidationRequired;
	}

	/**
	 * This method logs the alerts in case of down scenarios.
	 *
	 * @param pTimeTaken the time taken
	 * @param pResponseCode the response code
	 * @param pError the error
	 * @param pPageName the page name
	 * @param pServiceType the service type
	 */
	public void alertLogger(String pTimeTaken,String pResponseCode, String pError,String pPageName,String pServiceType){
		if (isLoggingDebug()) {
			logDebug("TRUPayPalPaymentProcessor  (alertLogger) Start");
		}
		super.alertLogger(IRadialConstants.STR_PAYPAL_PROCESSOR, pServiceType, pServiceType, pTimeTaken,pResponseCode,pError,pPageName);
		if (isLoggingDebug()) {
			logDebug("TRUPayPalPaymentProcessor  (alertLogger) End");
		}
	}

}
