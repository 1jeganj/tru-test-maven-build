package com.tru.radial.payment.paypal.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * This class has methods to set response properties for Authorization API call.
 * @author PA
 * @version 1.0
 */
public class DoAuthorizationPaymentResponse  extends AbstractRadialResponse implements IRadialResponse{


	
	/** The m authorization info. */
	private AuthorizationInfo mAuthorizationInfo;
	
	
	/**
	 * Property to hold mCorrelationId.
	 */
	private String mCorrelationId;
	/**
	 * Property to hold mToken.
	 */
	private String mToken;
	/**
	 * Property to hold mTranscationId.
	 */
	private String mTranscationId;
	
	/**
	 * Property to hold mPaymentStatus.
	 */
	private String mPaymentStatus;
	
	
	
	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;

	/**
	 * Pending Reasons .
	 * 
	 */
	private String mPendingReasons;

	
	/**
	 * @return the correlationId.
	 */
	public String getCorrelationId() {
		return mCorrelationId;
	}
	/**
	 * @param pCorrelationId the correlationId to set.
	 */
	public void setCorrelationId(String pCorrelationId) {
		mCorrelationId = pCorrelationId;
	}
	/**
	 * @return the token.
	 */
	public String getToken() {
		return mToken;
	}

	/**
	 * @param pToken the token to set.
	 */
	public void setToken(String pToken) {
		mToken = pToken;
	}
	/**
	 * @return the transcationId.
	 */
	public String getTranscationId() {
		return mTranscationId;
	}
	/**
	 * @param pTranscationId the transcationId to set.
	 */
	public void setTranscationId(String pTranscationId) {
		mTranscationId = pTranscationId;
	}
	/**
	 * @return the PaymentStatus.
	 */
	public String getPaymentStatus() {
		return mPaymentStatus;
	}
	/**
	 * @param pPaymentStatus the PaymentStatus to set.
	 */
	public void setPaymentAction(String pPaymentStatus) {
		mPaymentStatus = pPaymentStatus;
	}

	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	
	
	/**
	 * @param pSuccess the success to set.
	 */
	public void setSuccess(boolean pSuccess) {
		super.mIsSuccess = pSuccess;
	}
	
	
	/**
	 * mPendingReasons Getter.
	 * 	
	 * @return String mPendingReasons
	 */
	public String getPendingReasons() {
		return mPendingReasons;
	}
	
	/**
	 * Setter for mPendingReasons.
	 * @param pPendingReasons of type String 
	 */ 
	public void setPendingReasons(String pPendingReasons) {
		this.mPendingReasons = pPendingReasons;
	}
	
	
	/**
	 * sets the response code.
	 *
	 * @param pValue String
	 */
	@Override
	public void setResponseCode(String pValue) {
		mResponseCode=pValue;
	}


	
	/**
	 * Gets the authorization info.
	 *
	 * @return the authorization info
	 */
	public AuthorizationInfo getAuthorizationInfo() {
		return mAuthorizationInfo;
	}
	
	/**
	 * Sets the authorization info.
	 *
	 * @param pAuthorizationInfo the new authorization info
	 */
	public void setAuthorizationInfo(AuthorizationInfo pAuthorizationInfo) {
		this.mAuthorizationInfo = pAuthorizationInfo;
	}
	
	
	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTimeoutReponse) {
		super.mIsTimeOutResponse = pTimeoutReponse;
		
	}	
	
}