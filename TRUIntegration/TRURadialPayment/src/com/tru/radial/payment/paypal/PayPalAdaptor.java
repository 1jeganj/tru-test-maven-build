package com.tru.radial.payment.paypal;

import com.tru.common.cml.SystemException;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * This interface provides all methods to invoke PayPal NVP(Express Checkout) API.
 * 
 * @author PA
 * @version 1.0
 */
public interface PayPalAdaptor {

	/**
	 * This method is used to invoke setExpressCheckout call of PayPal API.
	 * 
	 * @param pPaymentRequest
	 *            of type PayPalRequest
	 * @throws SystemException
	 *             throws SystemException
	 * @return PayPalResponse
	 */
	IRadialResponse setExpressCheckout(IRadialRequest pPaymentRequest) throws SystemException;

	/**
	 * This method is used to invoke getExpressCheckoutDetails call of PayPal API. 
	 * 
	 * @param pPaymentRequest
	 *            of type PayPalRequest
	 * @throws SystemException
	 *             throws SystemException
	 * @return PayPalResponse
	 */	
	IRadialResponse getExpressCheckout(IRadialRequest pPaymentRequest) throws SystemException;

	/**
	 * This method is used to invoke doExpressCheckoutPayment call of PayPal API. 
	 * 
	 * @param pPaymentRequest
	 *            of type PayPalRequest
	 * @throws SystemException
	 *             throws SystemException
	 * @return PayPalResponse
	 */	
	IRadialResponse doExpressCheckoutPayment(IRadialRequest pPaymentRequest) throws SystemException;

	/**
	 * This method is used to invoke doAuthorizaton call of PayPal API. 
	 * 
	 * @param pPaymentRequest
	 *            of type PayPalRequest
	 * @throws SystemException
	 *             throws SystemException
	 * @return PayPalResponse
	 */	
	IRadialResponse doAuthorizaton(IRadialRequest pPaymentRequest) throws SystemException;

}
