package com.tru.radial.payment.paypal.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
* This class has methods to set request properties for CreateToken API call.
* @author PA
* @version 1.0
*/
public class GetExpressCheckOutRequest implements PayPalRequest, IRadialRequest {
	
	/** 
	 * The order id. 
	 */
	private String mOrderId;
	
	/**
	 * Property to hold mToken.
	 */
	private String mTokenId;
	
	/**
	 * Property to hold mCurrencyCode.
	 */
	private String mCurrencyCode;
	
	//TODO :Start Cleanup properties after radial integration
	
	/**
	 * Property to hold mPaymentRequestList.
	 */
	private List<PaymentDetails> mPaymentRequestList;
	
	/**
	 * Property to hold mCancelURL.
	 */
	private String mCancelURL;
	/**
	 * Property to hold mLocaleCode.
	 */
	private String mLocaleCode;
	
	/**
	 * Property to hold mReturnURL.
	 */
	private String mReturnURL;
	
	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;
	/**
	 * Property to hold mBillingAddress.
	 */
	private Address mBillingAddress;
	
	/**
	 * Property to hold mHashMap.
	 */
	private  Map<String,Object> mRequestParameter=new HashMap<String, Object>();
	/**
	 * Property to hold mShippingAddress.
	 */
	private Address mShippingAddress;
	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * Property to hold mPaymentAction.
	 */
	private String mPaymentAction;
	
	/** The m page name. */
	private String mPageName;
	
	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the new page name
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}
	
	//TODO :End Cleanup properties after radial integration
	
	/**
	 * @return the cancelURL.
	 */
	public String getCancelURL() {
		return mCancelURL;
	}
	/**
	 * @param pCancelURL the cancelURL to set.
	 */
	public void setCancelURL(String pCancelURL) {
		mCancelURL = pCancelURL;
	}
	/**
	 * @return the localeCode.
	 */
	public String getLocaleCode() {
		return mLocaleCode;
	}
	/**
	 * @param pLocaleCode the localeCode to set.
	 */
	public void setLocaleCode(String pLocaleCode) {
		mLocaleCode = pLocaleCode;
	}
	
	/**
	 * @return the returnURL.
	 */
	public String getReturnURL() {
		return mReturnURL;
	}
	/**
	 * @param pReturnURL the returnURL to set.
	 */
	public void setReturnURL(String pReturnURL) {
		mReturnURL = pReturnURL;
	}
	
	/**
	 * @return the paymentAction.
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set.
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the amount.
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set.
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the billingAddress.
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set.
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	/**
	 * @return the currencyCode.
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set.
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**

	/**
	 * @return the shippingAddress.
	 */
	public Address getShippingAddress() {
		return mShippingAddress;
	}
	/**
	 * @param pShippingAddress the shippingAddress to set.
	 */
	public void setShippingAddress(Address pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}
	/**
	 * @return the siteId.
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set.
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * This method is to set any extra properties from Request.
	 * @param pParameter the Parameter to get
	 * @return the object
	 */
	@Override
	public Object getRequestParameter(String pParameter) {
		Object object = mRequestParameter.get(pParameter);
		return object;
	}
	/**
	 * This method is to set any extra properties in Request.
	 * @param pValue to set
	 * @param pParam the Parameter to set
	 */
	@Override
	public void setRequestParameter(String pParam, Object pValue) {
		if(mRequestParameter!= null){
			mRequestParameter.put(pParam, pValue);
		}else {
			mRequestParameter=new HashMap<String, Object>();
			mRequestParameter.put(pParam, pValue);
		}
	}
	/**
	 * @return the paymentRequestList.
	 */
	public List<PaymentDetails> getPaymentRequestList() {
		return mPaymentRequestList;
	}
	/**
	 * @param pPaymentRequestList the paymentRequestList to set.
	 */
	public void setPaymentRequestList(List<PaymentDetails> pPaymentRequestList) {
		mPaymentRequestList = pPaymentRequestList;
	}
	
	
	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}
	
	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	/**
	 * @return the tokenId.
	 */
	public String getTokenId() {
		return mTokenId;
	}
	/**
	 * @param pTokenId the tokenId to set.
	 */
	public void setTokenId(String pTokenId) {
		mTokenId = pTokenId;
	}
	/**
	 * Overriding To string Method to Print the Request Object.
	 * @return the String object
	 */
	@Override
	public String toString() {
		return "GetExpressCheckOutRequest [mOrderId=" + mOrderId
				+ ", mTokenId=" + mTokenId + ", mCurrencyCode=" + mCurrencyCode
				+ "]";
	}
	
	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 *//*
	@Override
	public String toString() {
		
		return new StringBuilder().
				append("  Amount : " + getAmount()).append(", GetCancelUrl" + getCancelURL()).append(", CurrencyCode : "  + getCurrencyCode()).
				append(", Local Code : " + getLocaleCode()).
				append(", PaymentAction: "  + getPaymentAction()).append(", ReturnUrl : " + getReturnURL())
				.append(", Site Id "+ getSiteId()).append(", Token :" + getTokenId() ).append(", BillingAddress" + getBillingAddress()).
				append(", PaymentRequestList"+getPaymentRequestList()).toString();
		}*/
	
	
	
}
