package com.tru.radial.payment.paypal.request;

import javax.xml.bind.JAXBElement;

import atg.core.util.ContactInfo;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.RadialAddressUtil;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PayPalGetExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalGetExpressCheckoutRequestType;
import com.tru.radial.payment.paypal.bean.GetExpressCheckOutRequest;
import com.tru.radial.payment.paypal.bean.GetExpressCheckoutResponse;

/**
 * @author PA
 * The Class PayPalGetExpressRequestBuilder.
 */
public class PayPalGetExpressRequestBuilder extends AbstractRequestBuilder {

	/** The get express checkout request type. */
	private PayPalGetExpressCheckoutRequestType mGetExpressCheckoutRequest = null;
	
	/** The get express checkout reply type. */
	private PayPalGetExpressCheckoutReplyType mGetExpressCheckoutReplyType = null;
	

	/** The request object. */
	private JAXBElement<PayPalGetExpressCheckoutRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new pay pal get express request builder.
	 */
	public PayPalGetExpressRequestBuilder(){ 		
		initGetExpressCheckout();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest)throws TRURadialRequestBuilderException {
		GetExpressCheckOutRequest getExpressChkoutReqVO = null;
		if (!(pPaymentRequest instanceof GetExpressCheckOutRequest)) {
			throw new UnsupportedOperationException();
		}
		getExpressChkoutReqVO = (GetExpressCheckOutRequest)pPaymentRequest;
		mGetExpressCheckoutRequest.setOrderId(getExpressChkoutReqVO.getOrderId());
		mGetExpressCheckoutRequest.setToken(getExpressChkoutReqVO.getTokenId());
		mGetExpressCheckoutRequest.setCurrencyCode(ISOCurrencyCodeType.USD);	
		mGetExpressCheckoutRequest.setSchemaVersion(IRadialConstants.SCHEMA_VERSION);
		setRequestObject(getRadialObjectFactory().createPayPalGetExpressCheckoutRequest(mGetExpressCheckoutRequest));
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof GetExpressCheckoutResponse)) {
			throw new UnsupportedOperationException();
		}
		GetExpressCheckoutResponse response = (GetExpressCheckoutResponse)pPaymentResponse;		
		
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getGetExpressCheckoutReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);
			response.setPayerId(getGetExpressCheckoutReplyType().getPayerId());			
			response.setEmail(getGetExpressCheckoutReplyType().getPayerEmail());
			response.setPayerStatus(getGetExpressCheckoutReplyType().getPayerStatus());			
			response.setFirstName(getGetExpressCheckoutReplyType().getPayerName().getFirstName());
			response.setLastName(getGetExpressCheckoutReplyType().getPayerName().getLastName());
			response.setMiddlename(getGetExpressCheckoutReplyType().getPayerName().getMiddleName());			
			response.setPhoneNum(getGetExpressCheckoutReplyType().getPayerPhone());			
			//response.setShippingAddress(RadialAddressUtil.copyAddress(getGetExpressCheckoutReplyType().getShippingAddress()));
			response.setShippingAddress((ContactInfo)RadialAddressUtil.getShippingContanctInfo(getGetExpressCheckoutReplyType()));
			//response.setBillingAddress((ContactInfo)RadialAddressUtil.copyAddress(getGetExpressCheckoutReplyType().getBillingAddress()));	
			response.setBillingAddress((ContactInfo)RadialAddressUtil.getBillingContactInfo(getGetExpressCheckoutReplyType()));
			response.setPayerCountry(getGetExpressCheckoutReplyType().getPayerCountry());
			response.setResponseCode(getGetExpressCheckoutReplyType().getResponseCode());
			response.setShipToName(getGetExpressCheckoutReplyType().getShipToName());
		}else{
			buildErrorResponse(pPaymentResponse);
		}
		
					
	}
		
	/**
	 * It will constructs Error response.
	 *
	 * @param pPaymentResponse the payment response
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {			
		if (!(pPaymentResponse instanceof GetExpressCheckoutResponse)) {
			throw new UnsupportedOperationException();
		}
		GetExpressCheckoutResponse response = (GetExpressCheckoutResponse)pPaymentResponse;				
		super.buildErrorResponse(pPaymentResponse,getGetExpressCheckoutReplyType());		
		response.setTimeStamp(getTimeStamp());	
	}
	

	
	
	/**
	 * Inits the get express checkout.
	 */
	private void initGetExpressCheckout(){
		mGetExpressCheckoutRequest = createPayPalGetExpressCheckoutRequest();
	}
	
	/**
	 * Creates the pay pal get express checkout request.
	 *
	 * @return the pay pal get express checkout request type
	 */
	private PayPalGetExpressCheckoutRequestType createPayPalGetExpressCheckoutRequest() {

		return getRadialObjectFactory().createPayPalGetExpressCheckoutRequestType();
	}


	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<PayPalGetExpressCheckoutRequestType> getRequestObject() {
		return mRequestObject;
	}


	/**
	 * Sets the request object.
	 *
	 * @param pRequestType the new request object
	 */
	public void setRequestObject(
			JAXBElement<PayPalGetExpressCheckoutRequestType> pRequestType) {
		this.mRequestObject = pRequestType;
	}


	/**
	 * Gets the gets the express checkout reply type.
	 *
	 * @return the gets the express checkout reply type
	 */
	public PayPalGetExpressCheckoutReplyType getGetExpressCheckoutReplyType() {
		return mGetExpressCheckoutReplyType;
	}

	/**
	 * Sets the gets the express checkout reply type.
	 *
	 * @param pGetExpressCheckoutReplyType the new gets the express checkout reply type
	 */
	public void setGetExpressCheckoutReplyType(
			PayPalGetExpressCheckoutReplyType pGetExpressCheckoutReplyType) {
		this.mGetExpressCheckoutReplyType = pGetExpressCheckoutReplyType;
	}

	
	
}
