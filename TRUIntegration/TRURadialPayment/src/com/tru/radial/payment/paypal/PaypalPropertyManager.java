package com.tru.radial.payment.paypal;

import atg.nucleus.GenericService;
/**
 * This class is the property manager for all the PayPal related properties.
 * @author PA
 * @version 1.0
 */
public class PaypalPropertyManager extends GenericService {
	/**
	 * Member variable to hold the value for mPayPalIpAddrressTagName.
	 */
	private String mPayPalSetExpressMaxAmount;
	/**
	 * Member variable to hold the value for mPayPalIpAddrressTagName.
	 */
	private String mPayPalIpAddrressTagName;
	/**
	 * Member variable to hold the value for mPayPalShipToNamePropertyName.
	 */
	private String mPayPalShipToNamePropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToStreetPropertyName.
	 */
	private String mPayPalShipToStreetPropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToStreet2PropertyName.
	 */
	private String mPayPalShipToStreet2PropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToCityPropertyName.
	 */
	private String mPayPalShipToCityPropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToStatePropertyName.
	 */
	private String mPayPalShipToStatePropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToZipPropertyName.
	 */
	private String mPayPalShipToZipPropertyName;
	/**
	 * Member variable to hold the value for mPayPalShipToCountryPropertyName.
	 */
	private String mPayPalShipToCountryPropertyName;
	/**
	 * Member variable to hold the value for mpayPalBillToAddressStatusTagName.
	 */
	private String mPayPalShipToPhoneNumPropertyName;
	/**
	 * Member variable to hold the value for mpayPalTaxAmtTagName.
	 */
	private String mPayPalTaxAmtTagName;
	/**
	 * Member variable to hold the value for mPayPalBillToFirstNameTagName.
	 */
	private String mPayPalBillToFirstNameTagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoNameTagName.
	 */
	private String mPayPalBilltoNameTagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoStreetTagName.
	 */
	private String mPayPalBilltoStreetTagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoStreet2TagName.
	 */
	private String mPayPalBilltoStreet2TagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoCityTagName.
	 */
	private String mPayPalBilltoCityTagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoStateTagName.
	 */
	private String mPayPalBilltoStateTagName;
	/**
	 * Member variable to hold the value for mPayPalBilltoZipTagName.
	 */
	private String mPayPalBilltoZipTagName;
	/**
	 * Member variable to hold the value for mPayPalBillToAddressStatusTagName.
	 */
	private String mPayPalBillToAddressStatusTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerCountryCodeTagName.
	 */
	private String mPayPalPayerCountryCodeTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerCheckOutStatusTagName.
	 */
	private String mPayPalPayerCheckOutStatusTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerFirstNameTagName.
	 */
	private String mPayPalPayerFirstNameTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerMiddleNameTagName.
	 */
	private String mPayPalPayerMiddleNameTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerLastNameTagName.
	*/
	private String mPayPalPayerLastNameTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerPhoneNumTagName.
	 */
	private String mPayPalPayerPhoneNumTagName;
	/**
	 * Member variable to hold the value for mPayPalPayerStatusTagName.
	 */
	private String mPayPalPayerStatusTagName;
	/**
	 * Member variable to hold the value for mPayPalEmailTagName.
	 */
	private String mPayPalEmailTagName;
	/**
	 * Member variable to hold the value for mPayPalVersionTagName.
	 */
	private String mPayPalVersionTagName;
	/**
	 * Member variable to hold the value for mPayPalBuildTagName.
	 */
	private String mPayPalBuildTagName;
	/**
	 * Member variable to hold the value for mPayPalTimeStampTagName.
	 */
	private String mPayPalTimeStampTagName;
	/**
	 * Member variable to hold the value for mPayPalReqBillingAddress.
	 */
	private String mPayPalReqBillingAddress;
	/**
	 * Member variable to hold the value for mPayPalShiptoPhoneNumTagName.
	 */
	private String mPayPalShiptoPhoneNumTagName;
	/**
	 * Member variable to hold the value for PayPalShipToCountryTagName.
	 */
	private String mPayPalShiptoCountryTagName;
	/**
	 * Member variable to hold the value for PayPalShiptoZipTagName.
	 */
	private String mPayPalShiptoZipTagName;
	/**
	 * Member variable to hold the value for PayPalShippingAmountTagName.
	 */
	private String mPayPalShippingAmountTagName;
	/**
	 * Member variable to hold the value for PayPalItemAmtTagName.
	 */
	private String mPayPalItemAmtTagName;
	/**
	 * Member variable to hold the value for PayPalInvcNbrTagName.
	 */
	private String mPayPalInvcNbrTagName;
	/**
	 * Member variable to hold the value for PayPalShiptoStateTagName.
	 */
	private String	mPayPalShiptoStateTagName;
	/**
	 * Member variable to hold the value for PayPalShiptoCityTagName.
	 */
	private String mPayPalShiptoCityTagName;
	/**
	 * Member variable to hold the value for PayPalShiptoStreet2TagName.
	 */
	private String	mPayPalShiptoStreet2TagName;
	/**
	 * Member variable to hold the value for PayPalShiptoStreetTagName.
	 */
	private String	mPayPalShiptoStreetTagName;
	/**
	 * Member variable to hold the value for PayPalShiptoNameTagName.
	 */
	private String mPayPalShiptoNameTagName;
	/**
	 * Member variable to hold the value for PayPalAddrOverrideTagName.
	 */
	private String mPayPalAddrOverrideTagName;
	/**
	 * Member variable to hold the value for PayPalTokenPropertyName.
	 */
	private String mPayPalTokenPropertyName;
	/**
	 * Member variable to hold the value for PayPalCoRelationIdPropertyName.
	 */
	private String mPayPalCoRelationIdPropertyName;
	/**
	 * Member variable to hold the value for PayPalTransactionIdPropertyName.
	 */
	private String mPayPalTransactionIdPropertyName;
	/**
	 * Member variable to hold the value for PayPalPayerIdPropertyName.
	 */
	private String mPayPalPayerIdPropertyName;
	/**
	 * Member variable to hold the value for PayPalTagName "ACK"
	 */
	private String mPayPalAcknTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "CANCELURL"
	 */
	private String mPayPalCancelURLTagName;
	/**
	 * Member variable to hold the value for PayPalTokenTagName "TOKEN"
	 */
	private String mPayPalTokenTagName = null;
	/**
	 * Member variable to hold the value for PayPalTagName "PAYMENTINFO_0_PAYMENTTYPE "
	 */
	private String mPayPalPaymentTypeTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTINFO_0_TRANSACTIONID"
	 */
	private String mPayPalTransactionIdTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTINFO_0_AMT"
	 */
	private String mPayPalDoExprsChkPayAmtTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "ORDERTIME"
	 */
	private String mPayPalDoCaptureOrderTimeTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "FEEAMT"
	 */
	private String mPayPalFeeAmtTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTINFO_0_FEEAMT".
	 */
	private String mPayPalDoExprsChkPayFeeAmtTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTINFO_0_PAYMENTSTATUS".
	 */
	private String mPayPalPaymentStatusTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTREQUEST_0_AMT".
	 */
	private String mPayPalTotalAmtTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYMENTREQUEST_0_CURRENCYCODE".
	 */
	private String mPayPalCurrencyCodeTagName;
	/**
	 * Member variable to hold the value for PayPalTagName  "PAYERID".
	 */
	private String mPayPalPayerIdTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "PAYMENTREQUEST_0_PAYMENTACTION".
	 */
	private String mPayPalPaymentActionTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "L_ERRORCODE0".
	 */
	private String mPayPalErrorCodeTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "L_SHORTMESSAGE0".
	 */
	private String mPayPalShortMessageTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "L_LONGMESSAGE0".
	 */
	private String mPayPalLongMessageTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "CORRELATIONID".
	 */
	private String mPayPalCorrelationIdTagName;
	/**
	 * Member variable to hold the value for PayPalTagName "RETURNURL".
	 */
	private String mPayPalReturnURLTagName;
	/**
	 * Member variable to hold the value for PayPalTagName REQCONFIRMSHIPPING.
	 */
	private String mPayPalReqConfrmShippingTagName;
	/**
	 * Member variable to hold the value for PayPalTagName AUTHORIZATIONID.
	 */
	private String mPayPalAuthorizationIdTagName;
	/**
	 * Member variable to hold the value for PayPalTagName COMPLETETYPE.
	 */
	private String mPayPalCompleteTypeTagName;
	/**
	 * Member variable to hold the value for PayPalTagName AMT.
	 */
	private String mPayPalTransactionAmtTagName;
	/**
	 * Member variable to hold the value for PayPal Transaction ID.
	 */
	private String mPayPalTransactionId;
	/**
	 * Member variable to hold the value for PayPal Invoice ID.
	 */
	private String mPayPalInvoiceId;
	/**
	 * Member variable to hold the value for PayPal Refund Type.
	 */
	private String mPayPalRefundType;
	/**
	 * Member variable to hold the value for PayPal Currency Code.
	 */
	private String mPayPalCurrencyCode;
	/**
	 * Member variable to hold the value for PayPal Note.
	 */
	private String mPayPalNote;
	/**
	 * Member variable to hold the value for PayPal Refund Transaction ID.
	 */
	private String mPayPalRefundTransactionId;
	/**
	 * Member variable to hold the value for PayPal Fee Fund Amount.
	 */
	private String mPayPalFeeFundAmt;
	/**
	 * Member variable to hold the value for PayPal Gross Refund Amount.
	 */
	private String mPayPalGrossRefundAmt;
	/**
	 * Member variable to hold the value for PayPal Net Refund Amount.
	 */
	private String mPayPalNetRefundAmt;
	/**
	 * Member variable to hold the value for PayPal Total Refund Amount.
	 */
	private String mPayPalTotalRefundAmt;
	/**
	 * Member variable to hold the value for PayPalTagName Amount.
	 */
	private String mPaymentInfoAMTPropertyName;
	/**
	 * Member variable to hold the value for PayPalRefundStatus.
	 */
	private String mPayPalRefundStatus;
	/**
	 * Member variable to hold the value for PayPalTagName  "METHOD".
	 */
	private String mPayPalMethodTagName ;
	/**
	 * Member variable to hold the value for PayPalTagName  "MSGSUBID".
	 */
	private String mPayPalMessageSubIdTagName ;
	/**
	 * Member variable to hold the value for PayPalPaymentErrorCodeTagName.
	 */
	private String mPayPalPaymentErrorCodeTagName;
	/**
	 * Member variable to hold the value for PayPalPaymentLongMessageTagName.
	 */
	private String mPayPalPaymentLongMessageTagName;
	/**
	 * Member variable to hold the value for PayPalPaymentReqInfoErrorCodeTagName.
	 */
	private String mPayPalPaymentReqInfoErrorCodeTagName;
	/**
	 * Member variable to hold the value for PayPalPaymentReqInfoLongMessageTagName.
	 */
	private String mPayPalPaymentReqInfoLongMessageTagName;
	/**
	 * @return the paymentInfoAMTPropertyName.
	 */
	public String getPaymentInfoAMTPropertyName() {
		return mPaymentInfoAMTPropertyName;
	}
	/**
	 * @param pPaymentInfoAMTPropertyName the paymentInfoAMTPropertyName to set.
	 */
	public void setPaymentInfoAMTPropertyName(String pPaymentInfoAMTPropertyName) {
		mPaymentInfoAMTPropertyName = pPaymentInfoAMTPropertyName;
	}
	/**
	 * @return the payPalTokenPropertyName.
	 */
	public String getPayPalTokenPropertyName() {
		return mPayPalTokenPropertyName;
	}
	/**
	 * @param pPayPalTokenPropertyName the payPalTokenPropertyName to set.
	 */
	public void setPayPalTokenPropertyName(String pPayPalTokenPropertyName) {
		mPayPalTokenPropertyName = pPayPalTokenPropertyName;
	}
	/**
	 * @return the payPalCoRelationIdPropertyName.
	 */
	public String getPayPalCoRelationIdPropertyName() {
		return mPayPalCoRelationIdPropertyName;
	}
	/**
	 * @param pPayPalCoRelationIdPropertyName the payPalCoRelationIdPropertyName to set.
	 */
	public void setPayPalCoRelationIdPropertyName(String pPayPalCoRelationIdPropertyName) {
		mPayPalCoRelationIdPropertyName = pPayPalCoRelationIdPropertyName;
	}
	/**
	 * @return the payPalTransactionIdPropertyName.
	 */
	public String getPayPalTransactionIdPropertyName() {
		return mPayPalTransactionIdPropertyName;
	}
	/**
	 * @param pPayPalTransactionIdPropertyName the payPalTransactionIdPropertyName to set.
	 */
	public void setPayPalTransactionIdPropertyName(String pPayPalTransactionIdPropertyName) {
		mPayPalTransactionIdPropertyName = pPayPalTransactionIdPropertyName;
	}
	/**
	 * @return the payPalPayerIdPropertyName.
	 */
	public String getPayPalPayerIdPropertyName() {
		return mPayPalPayerIdPropertyName;
	}
	/**
	 * @param pPayPalPayerIdPropertyName the payPalPayerIdPropertyName to set.
	 */
	public void setPayPalPayerIdPropertyName(String pPayPalPayerIdPropertyName) {
		mPayPalPayerIdPropertyName = pPayPalPayerIdPropertyName;
	}
	/**
	 * @return the payPalAcknTagName.
	 */
	public String getPayPalAcknTagName() {
		return mPayPalAcknTagName;
	}
	/**
	 * @param pPayPalAcknTagName the payPalAcknTagName to set.
	 */
	public void setPayPalAcknTagName(String pPayPalAcknTagName) {
		mPayPalAcknTagName = pPayPalAcknTagName;
	}
	/**
	 * @return the payPalTokenTagName.
	 */
	public String getPayPalTokenTagName() {
		return mPayPalTokenTagName;
	}
	/**
	 * @param pPayPalTokenTagName the payPalTokenTagName to set.
	 */
	public void setPayPalTokenTagName(String pPayPalTokenTagName) {
		mPayPalTokenTagName = pPayPalTokenTagName;
	}
	/**
	 * @return the payPalPaymentTypeTagName.
	 */
	public String getPayPalPaymentTypeTagName() {
		return mPayPalPaymentTypeTagName;
	}
	/**
	 * @param pPayPalPaymentTypeTagName the payPalPaymentTypeTagName to set.
	 */
	public void setPayPalPaymentTypeTagName(String pPayPalPaymentTypeTagName) {
		mPayPalPaymentTypeTagName = pPayPalPaymentTypeTagName;
	}
	/**
	 * @return the payPalTransactionIdTagName.
	 */
	public String getPayPalTransactionIdTagName() {
		return mPayPalTransactionIdTagName;
	}
	/**
	 * @param pPayPalTransactionIdTagName the payPalTransactionIdTagName to set.
	 */
	public void setPayPalTransactionIdTagName(String pPayPalTransactionIdTagName) {
		mPayPalTransactionIdTagName = pPayPalTransactionIdTagName;
	}
	/**
	 * @return the payPalDoExprsChkPayAmtTagName.
	 */
	public String getPayPalDoExprsChkPayAmtTagName() {
		return mPayPalDoExprsChkPayAmtTagName;
	}
	/**
	 * @param pPayPalDoExprsChkPayAmtTagName the payPalDoExprsChkPayAmtTagName to set.
	 */
	public void setPayPalDoExprsChkPayAmtTagName(String pPayPalDoExprsChkPayAmtTagName) {
		mPayPalDoExprsChkPayAmtTagName = pPayPalDoExprsChkPayAmtTagName;
	}
	/**
	 * @return the payPalDoCaptureOrderTimeTagName.
	 */
	public String getPayPalDoCaptureOrderTimeTagName() {
		return mPayPalDoCaptureOrderTimeTagName;
	}
	/**
	 * @param pPayPalDoCaptureOrderTimeTagName the payPalDoCaptureOrderTimeTagName to set.
	 */
	public void setPayPalDoCaptureOrderTimeTagName(String pPayPalDoCaptureOrderTimeTagName) {
		mPayPalDoCaptureOrderTimeTagName = pPayPalDoCaptureOrderTimeTagName;
	}
	/**
	 * @return the payPalFeeAmtTagName.
	 */
	public String getPayPalFeeAmtTagName() {
		return mPayPalFeeAmtTagName;
	}
	/**
	 * @param pPayPalFeeAmtTagName the payPalFeeAmtTagName to set.
	 */
	public void setPayPalFeeAmtTagName(String pPayPalFeeAmtTagName) {
		mPayPalFeeAmtTagName = pPayPalFeeAmtTagName;
	}
	/**
	 * @return the payPalDoExprsChkPayFeeAmtTagName.
	 */
	public String getPayPalDoExprsChkPayFeeAmtTagName() {
		return mPayPalDoExprsChkPayFeeAmtTagName;
	}
	/**
	 * @param pPayPalDoExprsChkPayFeeAmtTagName the payPalDoExprsChkPayFeeAmtTagName to set.
	 */
	public void setPayPalDoExprsChkPayFeeAmtTagName(String pPayPalDoExprsChkPayFeeAmtTagName) {
		mPayPalDoExprsChkPayFeeAmtTagName = pPayPalDoExprsChkPayFeeAmtTagName;
	}
	/**
	 * @return the payPalPaymentStatusTagName.
	 */
	public String getPayPalPaymentStatusTagName() {
		return mPayPalPaymentStatusTagName;
	}
	/**
	 * @param pPayPalPaymentStatusTagName the payPalPaymentStatusTagName to set.
	 */
	public void setPayPalPaymentStatusTagName(String pPayPalPaymentStatusTagName) {
		this.mPayPalPaymentStatusTagName = pPayPalPaymentStatusTagName;
	}
	/**
	 * @return the payPalTotalAmtTagName.
	 */
	public String getPayPalTotalAmtTagName() {
		return mPayPalTotalAmtTagName;
	}
	/**
	 * @param pPayPalTotalAmtTagName the payPalTotalAmtTagName to set.
	 */
	public void setPayPalTotalAmtTagName(String pPayPalTotalAmtTagName) {
		mPayPalTotalAmtTagName = pPayPalTotalAmtTagName;
	}
	/**
	 * @return the payPalCurrencyCodeTagName.
	 */
	public String getPayPalCurrencyCodeTagName() {
		return mPayPalCurrencyCodeTagName;
	}
	/**
	 * @param pPayPalCurrencyCodeTagName the payPalCurrencyCodeTagName to set.
	 */
	public void setPayPalCurrencyCodeTagName(String pPayPalCurrencyCodeTagName) {
		mPayPalCurrencyCodeTagName = pPayPalCurrencyCodeTagName;
	}
	/**
	 * @return the payPalPayerIdTagName.
	 */
	public String getPayPalPayerIdTagName() {
		return mPayPalPayerIdTagName;
	}
	/**
	 * @param pPayPalPayerIdTagName the payPalPayerIdTagName to set.
	 */
	public void setPayPalPayerIdTagName(String pPayPalPayerIdTagName) {
		mPayPalPayerIdTagName = pPayPalPayerIdTagName;
	}
	/**
	 * @return the payPalPaymentActionTagName.
	 */
	public String getPayPalPaymentActionTagName() {
		return mPayPalPaymentActionTagName;
	}
	/**
	 * @param pPayPalPaymentActionTagName the payPalPaymentActionTagName to set.
	 */
	public void setPayPalPaymentActionTagName(String pPayPalPaymentActionTagName) {
		mPayPalPaymentActionTagName = pPayPalPaymentActionTagName;
	}
	/**
	 * @return the payPalErrorCodeTagName.
	 */
	public String getPayPalErrorCodeTagName() {
		return mPayPalErrorCodeTagName;
	}
	/**
	 * @param pPayPalErrorCodeTagName the payPalErrorCodeTagName to set
	 */
	public void setPayPalErrorCodeTagName(String pPayPalErrorCodeTagName) {
		mPayPalErrorCodeTagName = pPayPalErrorCodeTagName;
	}
	/**
	 * @return the payPalShortMessageTagName
	 */
	public String getPayPalShortMessageTagName() {
		return mPayPalShortMessageTagName;
	}
	/**
	 * @param pPayPalShortMessageTagName the payPalShortMessageTagName to set.
	 */
	public void setPayPalShortMessageTagName(String pPayPalShortMessageTagName) {
		mPayPalShortMessageTagName = pPayPalShortMessageTagName;
	}
	/**
	 * @return the payPalLongMessageTagName.
	 */
	public String getPayPalLongMessageTagName() {
		return mPayPalLongMessageTagName;
	}
	/**
	 * @param pPayPalLongMessageTagName the payPalLongMessageTagName to set.
	 */
	public void setPayPalLongMessageTagName(String pPayPalLongMessageTagName) {
		mPayPalLongMessageTagName = pPayPalLongMessageTagName;
	}
	/**
	 * @return the payPalCorrelationIdTagName.
	 */
	public String getPayPalCorrelationIdTagName() {
		return mPayPalCorrelationIdTagName;
	}
	/**
	 * @param pPayPalCorrelationIdTagName the payPalCorrelationIdTagName to set.
	 */
	public void setPayPalCorrelationIdTagName(String pPayPalCorrelationIdTagName) {
		mPayPalCorrelationIdTagName = pPayPalCorrelationIdTagName;
	}
	/**
	 * @return the payPalReturnURLTagName.
	 */
	public String getPayPalReturnURLTagName() {
		return mPayPalReturnURLTagName;
	}
	/**
	 * @param pPayPalReturnURLTagName the payPalReturnURLTagName to set.
	 */
	public void setPayPalReturnURLTagName(String pPayPalReturnURLTagName) {
		mPayPalReturnURLTagName = pPayPalReturnURLTagName;
	}
	/**
	 * @return the payPalReqConfrmShippingTagName.
	 */
	public String getPayPalReqConfrmShippingTagName() {
		return mPayPalReqConfrmShippingTagName;
	}
	/**
	 * @param pPayPalReqConfrmShippingTagName the payPalReqConfrmShippingTagName to set.
	 */
	public void setPayPalReqConfrmShippingTagName(String pPayPalReqConfrmShippingTagName) {
		mPayPalReqConfrmShippingTagName = pPayPalReqConfrmShippingTagName;
	}
	/**
	 * @return the payPalAuthorizationIdTagName.
	 */
	public String getPayPalAuthorizationIdTagName() {
		return mPayPalAuthorizationIdTagName;
	}
	/**
	 * @param pPayPalAuthorizationIdTagName the payPalAuthorizationIdTagName to set
	 */
	public void setPayPalAuthorizationIdTagName(String pPayPalAuthorizationIdTagName) {
		mPayPalAuthorizationIdTagName = pPayPalAuthorizationIdTagName;
	}
	/**
	 * @return the payPalCompleteTypeTagName
	 */
	public String getPayPalCompleteTypeTagName() {
		return mPayPalCompleteTypeTagName;
	}
	/**
	 * @param pPayPalCompleteTypeTagName the payPalCompleteTypeTagName to set.
	 */
	public void setPayPalCompleteTypeTagName(String pPayPalCompleteTypeTagName) {
		mPayPalCompleteTypeTagName = pPayPalCompleteTypeTagName;
	}
	/**
	 * @return the payPalTransactionAmtTagName.
	 */
	public String getPayPalTransactionAmtTagName() {
		return mPayPalTransactionAmtTagName;
	}
	/**
	 * @param pPayPalTransactionAmtTagName the payPalTransactionAmtTagName to set.
	 */
	public void setPayPalTransactionAmtTagName(String pPayPalTransactionAmtTagName) {
		mPayPalTransactionAmtTagName = pPayPalTransactionAmtTagName;
	}
	/**
	 * @return the payPalTransactionId.
	 */
	public String getPayPalTransactionId() {
		return mPayPalTransactionId;
	}
	/**
	 * @param pPayPalTransactionId the payPalTransactionId to set.
	 */
	public void setPayPalTransactionId(String pPayPalTransactionId) {
		mPayPalTransactionId = pPayPalTransactionId;
	}
	/**
	 * @return the payPalInvoiceId.
	 */
	public String getPayPalInvoiceId() {
		return mPayPalInvoiceId;
	}
	/**
	 * @param pPayPalInvoiceId the payPalInvoiceId to set.
	 */
	public void setPayPalInvoiceId(String pPayPalInvoiceId) {
		mPayPalInvoiceId = pPayPalInvoiceId;
	}
	/**
	 * @return the payPalRefundType.
	 */
	public String getPayPalRefundType() {
		return mPayPalRefundType;
	}
	/**
	 * @param pPayPalRefundType the payPalRefundType to set.
	 */
	public void setPayPalRefundType(String pPayPalRefundType) {
		mPayPalRefundType = pPayPalRefundType;
	}
	/**
	 * @return the payPalCurrencyCode.
	 */
	public String getPayPalCurrencyCode() {
		return mPayPalCurrencyCode;
	}
	/**
	 * @param pPayPalCurrencyCode the payPalCurrencyCode to set.
	 */
	public void setPayPalCurrencyCode(String pPayPalCurrencyCode) {
		mPayPalCurrencyCode = pPayPalCurrencyCode;
	}
	/**
	 * @return the payPalNote.
	 */
	public String getPayPalNote() {
		return mPayPalNote;
	}
	/**
	 * @param pPayPalNote the payPalNote to set
	 */
	public void setPayPalNote(String pPayPalNote) {
		mPayPalNote = pPayPalNote;
	}
	/**
	 * @return the payPalRefundTransactionId.
	 */
	public String getPayPalRefundTransactionId() {
		return mPayPalRefundTransactionId;
	}
	/**
	 * @param pPayPalRefundTransactionId the payPalRefundTransactionId to set.
	 */
	public void setPayPalRefundTransactionId(String pPayPalRefundTransactionId) {
		mPayPalRefundTransactionId = pPayPalRefundTransactionId;
	}
	/**
	 * @return the payPalFeeFundAmt.
	 */
	public String getPayPalFeeFundAmt() {
		return mPayPalFeeFundAmt;
	}
	/**
	 * @param pPayPalFeeFundAmt the payPalFeeFundAmt to set.
	 */
	public void setPayPalFeeFundAmt(String pPayPalFeeFundAmt) {
		mPayPalFeeFundAmt = pPayPalFeeFundAmt;
	}
	/**
	 * @return the payPalGrossRefundAmt.
	 */
	public String getPayPalGrossRefundAmt() {
		return mPayPalGrossRefundAmt;
	}
	/**
	 * @param pPayPalGrossRefundAmt the payPalGrossRefundAmt to set.
	 */
	public void setPayPalGrossRefundAmt(String pPayPalGrossRefundAmt) {
		mPayPalGrossRefundAmt = pPayPalGrossRefundAmt;
	}
	/**
	 * @return the payPalNetRefundAmt.
	 */
	public String getPayPalNetRefundAmt() {
		return mPayPalNetRefundAmt;
	}
	/**
	 * @param pPayPalNetRefundAmt the payPalNetRefundAmt to set.
	 */
	public void setPayPalNetRefundAmt(String pPayPalNetRefundAmt) {
		mPayPalNetRefundAmt = pPayPalNetRefundAmt;
	}
	/**
	 * @return the payPalTotalRefundAmt.
	 */
	public String getPayPalTotalRefundAmt() {
		return mPayPalTotalRefundAmt;
	}
	/**
	 * @return the payPalRefundStatus.
	 */
	public String getPayPalRefundStatus() {
		return mPayPalRefundStatus;
	}
	/**
	 * @param pPayPalRefundStatus the payPalRefundStatus to set.
	 */
	public void setPayPalRefundStatus(String pPayPalRefundStatus) {
		mPayPalRefundStatus = pPayPalRefundStatus;
	}
	/**
	 * @return the payPalMethodTagName.
	 */
	public String getPayPalMethodTagName() {
		return mPayPalMethodTagName;
	}
	/**
	 * @param pPayPalMethodTagName the payPalMethodTagName to set.
	 */
	public void setPayPalMethodTagName(String pPayPalMethodTagName) {
		mPayPalMethodTagName = pPayPalMethodTagName;
	}
	/**
	 * @return the payPalCancelURLTagName.
	 */
	public String getPayPalCancelURLTagName() {
		return mPayPalCancelURLTagName;
	}
	/**
	 * @param pPayPalCancelURLTagName the payPalCancelURLTagName to set.
	 */
	public void setPayPalCancelURLTagName(String pPayPalCancelURLTagName) {
		mPayPalCancelURLTagName = pPayPalCancelURLTagName;
	}
	/**
	 * @return the payPalMessageSubIdTagName.
	 */
	public String getPayPalMessageSubIdTagName() {
		return mPayPalMessageSubIdTagName;
	}
	/**
	 * @param pPayPalMessageSubIdTagName the payPalMessageSubIdTagName to set.
	 */
	public void setPayPalMessageSubIdTagName(String pPayPalMessageSubIdTagName) {
		mPayPalMessageSubIdTagName = pPayPalMessageSubIdTagName;
	}
	/**
	 * @return the payPalAddrOverrideTagName.
	 */
	public String getPayPalAddrOverrideTagName() {
		return mPayPalAddrOverrideTagName;
	}
	/**
	 * @param pPayPalAddrOverrideTagName the payPalAddrOverrideTagName to set.
	 */
	public void setPayPalAddrOverrideTagName(String pPayPalAddrOverrideTagName) {
		mPayPalAddrOverrideTagName = pPayPalAddrOverrideTagName;
	}
	/**
	 * @return the payPalShiptoNameTagName.
	 */
	public String getPayPalShiptoNameTagName() {
		return mPayPalShiptoNameTagName;
	}
	/**
	 * @param pPayPalShiptoNameTagName the payPalShiptoNameTagName to set.
	 */
	public void setPayPalShiptoNameTagName(String pPayPalShiptoNameTagName) {
		mPayPalShiptoNameTagName = pPayPalShiptoNameTagName;
	}
	/**
	 * @return the payPalShiptoStreetTagName.
	 */
	public String getPayPalShiptoStreetTagName() {
		return mPayPalShiptoStreetTagName;
	}
	/**
	 * @param pPayPalShiptoStreetTagName the payPalShiptoStreetTagName to set.
	 */
	public void setPayPalShiptoStreetTagName(String pPayPalShiptoStreetTagName) {
		mPayPalShiptoStreetTagName = pPayPalShiptoStreetTagName;
	}
	/**
	 * @return the payPalShiptoStreet2TagName.
	 */
	public String getPayPalShiptoStreet2TagName() {
		return mPayPalShiptoStreet2TagName;
	}
	/**
	 * @param pPayPalShiptoStreet2TagName the payPalShiptoStreet2TagName to set.
	 */
	public void setPayPalShiptoStreet2TagName(String pPayPalShiptoStreet2TagName) {
		mPayPalShiptoStreet2TagName = pPayPalShiptoStreet2TagName;
	}
	/**
	 * @return the payPalShiptoCityTagName.
	 */
	public String getPayPalShiptoCityTagName() {
		return mPayPalShiptoCityTagName;
	}
	/**
	 * @param pPayPalShiptoCityTagName the payPalShiptoCityTagName to set.
	 */
	public void setPayPalShiptoCityTagName(String pPayPalShiptoCityTagName) {
		mPayPalShiptoCityTagName = pPayPalShiptoCityTagName;
	}
	/**
	 * @return the payPalShiptoStateTagName.
	 */
	public String getPayPalShiptoStateTagName() {
		return mPayPalShiptoStateTagName;
	}
	/**
	 * @param pPayPalShiptoStateTagName the payPalShiptoStateTagName to set.
	 */
	public void setPayPalShiptoStateTagName(String pPayPalShiptoStateTagName) {
		mPayPalShiptoStateTagName = pPayPalShiptoStateTagName;
	}
	/**
	 * @return the payPalShiptoCountryTagName.
	 */
	public String getPayPalShiptoCountryTagName() {
		return mPayPalShiptoCountryTagName;
	}
	/**
	 * @param pPayPalShiptoCountryTagName the payPalShiptoCountryTagName to set.
	 */
	public void setPayPalShiptoCountryTagName(String pPayPalShiptoCountryTagName) {
		mPayPalShiptoCountryTagName = pPayPalShiptoCountryTagName;
	}
	/**
	 * @return the payPalShiptoZipTagName.
	 */
	public String getPayPalShiptoZipTagName() {
		return mPayPalShiptoZipTagName;
	}
	/**
	 * @param pPayPalShiptoZipTagName the payPalShiptoZipTagName to set.
	 */
	public void setPayPalShiptoZipTagName(String pPayPalShiptoZipTagName) {
		mPayPalShiptoZipTagName = pPayPalShiptoZipTagName;
	}
	/**
	 * @return the payPalShippingAmountTagName.
	 */
	public String getPayPalShippingAmountTagName() {
		return mPayPalShippingAmountTagName;
	}
	/**
	 * @param pPayPalShippingAmountTagName the payPalShippingAmountTagName to set
	 */
	public void setPayPalShippingAmountTagName(String pPayPalShippingAmountTagName) {
		mPayPalShippingAmountTagName = pPayPalShippingAmountTagName;
	}
	/**
	 * @return the payPalItemAmtTagName
	 */
	public String getPayPalItemAmtTagName() {
		return mPayPalItemAmtTagName;
	}
	/**
	 * @param pPayPalItemAmtTagName the payPalItemAmtTagName to set.
	 */
	public void setPayPalItemAmtTagName(String pPayPalItemAmtTagName) {
		mPayPalItemAmtTagName = pPayPalItemAmtTagName;
	}
	/**
	 * @return the payPalInvcNbrTagName.
	 */
	public String getPayPalInvcNbrTagName() {
		return mPayPalInvcNbrTagName;
	}
	/**
	 * @param pPayPalInvcNbrTagName the payPalInvcNbrTagName to set.
	 */
	public void setPayPalInvcNbrTagName(String pPayPalInvcNbrTagName) {
		mPayPalInvcNbrTagName = pPayPalInvcNbrTagName;
	}
	/**
	 * @return the payPalShiptoPhoneNumTagName.
	 */
	public String getPayPalShiptoPhoneNumTagName() {
		return mPayPalShiptoPhoneNumTagName;
	}
	/**
	 * @param pPayPalShiptoPhoneNumTagName the payPalShiptoPhoneNumTagName to set.
	 */
	public void setPayPalShiptoPhoneNumTagName(String pPayPalShiptoPhoneNumTagName) {
		mPayPalShiptoPhoneNumTagName = pPayPalShiptoPhoneNumTagName;
	}
	/**
	 * @return the payPalReqBillingAddress.
	 */
	public String getPayPalReqBillingAddress() {
		return mPayPalReqBillingAddress;
	}
	/**
	 * @param pPayPalReqBillingAddress the payPalReqBillingAddress to set.
	 */
	public void setPayPalReqBillingAddress(String pPayPalReqBillingAddress) {
		mPayPalReqBillingAddress = pPayPalReqBillingAddress;
	}
	/**
	 * @return the payPalTimeStampTagName.
	 */
	public String getPayPalTimeStampTagName() {
		return mPayPalTimeStampTagName;
	}
	/**
	 * @param pPayPalTimeStampTagName the payPalTimeStampTagName to set.
	 */
	public void setPayPalTimeStampTagName(String pPayPalTimeStampTagName) {
		mPayPalTimeStampTagName = pPayPalTimeStampTagName;
	}
	/**
	 * @return the payPalVersionTagName.
	 */
	public String getPayPalVersionTagName() {
		return mPayPalVersionTagName;
	}
	/**
	 * @param pPayPalVersionTagName the payPalVersionTagName to set.
	 */
	public void setPayPalVersionTagName(String pPayPalVersionTagName) {
		mPayPalVersionTagName = pPayPalVersionTagName;
	}
	/**
	 * @return the payPalBuildTagName.
	 */
	public String getPayPalBuildTagName() {
		return mPayPalBuildTagName;
	}
	/**
	 * @param pPayPalBuildTagName the payPalBuildTagName to set.
	 */
	public void setPayPalBuildTagName(String pPayPalBuildTagName) {
		mPayPalBuildTagName = pPayPalBuildTagName;
	}
	/**
	 * @return the payPalEmailTagName.
	 */
	public String getPayPalEmailTagName() {
		return mPayPalEmailTagName;
	}
	/**
	 * @param pPayPalEmailTagName the payPalEmailTagName to set.
	 */
	public void setPayPalEmailTagName(String pPayPalEmailTagName) {
		mPayPalEmailTagName = pPayPalEmailTagName;
	}
	/**
	 * @return the payPalPayerStatusTagName.
	 */
	public String getPayPalPayerStatusTagName() {
		return mPayPalPayerStatusTagName;
	}
	/**
	 * @param pPayPalPayerStatusTagName the payPalPayerStatusTagName to set.
	 */
	public void setPayPalPayerStatusTagName(String pPayPalPayerStatusTagName) {
		mPayPalPayerStatusTagName = pPayPalPayerStatusTagName;
	}
	/**
	 * @return the payPalPayerFirstNameTagName.
	 */
	public String getPayPalPayerFirstNameTagName() {
		return mPayPalPayerFirstNameTagName;
	}
	/**
	 * @param pPayPalPayerFirstNameTagName the payPalPayerFirstNameTagName to set.
	 */
	public void setPayPalPayerFirstNameTagName(String pPayPalPayerFirstNameTagName) {
		mPayPalPayerFirstNameTagName = pPayPalPayerFirstNameTagName;
	}
	/**
	 * @return the payPalPayerMiddleNameTagName.
	 */
	public String getPayPalPayerMiddleNameTagName() {
		return mPayPalPayerMiddleNameTagName;
	}
	/**
	 * @param pPayPalPayerMiddleNameTagName the payPalPayerMiddleNameTagName to set.
	 */
	public void setPayPalPayerMiddleNameTagName(String pPayPalPayerMiddleNameTagName) {
		mPayPalPayerMiddleNameTagName = pPayPalPayerMiddleNameTagName;
	}
	/**
	 * @return the payPalPayerLastNameTagName.
	 */
	public String getPayPalPayerLastNameTagName() {
		return mPayPalPayerLastNameTagName;
	}
	/**
	 * @param pPayPalPayerLastNameTagName the payPalPayerLastNameTagName to set.
	 */
	public void setPayPalPayerLastNameTagName(String pPayPalPayerLastNameTagName) {
		mPayPalPayerLastNameTagName = pPayPalPayerLastNameTagName;
	}
	/**
	 * @return the payPalPayerPhoneNumTagName.
	 */
	public String getPayPalPayerPhoneNumTagName() {
		return mPayPalPayerPhoneNumTagName;
	}
	/**
	 * @param pPayPalPayerPhoneNumTagName the payPalPayerPhoneNumTagName to set.
	 */
	public void setPayPalPayerPhoneNumTagName(String pPayPalPayerPhoneNumTagName) {
		mPayPalPayerPhoneNumTagName = pPayPalPayerPhoneNumTagName;
	}
	/**
	 * @return the payPalPayerCheckOutStatusTagName.
	 */
	public String getPayPalPayerCheckOutStatusTagName() {
		return mPayPalPayerCheckOutStatusTagName;
	}
	/**
	 * @param pPayPalPayerCheckOutStatusTagName the payPalPayerCheckOutStatusTagName to set.
	 */
	public void setPayPalPayerCheckOutStatusTagName(String pPayPalPayerCheckOutStatusTagName) {
		mPayPalPayerCheckOutStatusTagName = pPayPalPayerCheckOutStatusTagName;
	}
	/**
	 * @return the payPalPayerCountryCodeTagName.
	 */
	public String getPayPalPayerCountryCodeTagName() {
		return mPayPalPayerCountryCodeTagName;
	}
	/**
	 * @param pPayPalPayerCountryCodeTagName the payPalPayerCountryCodeTagName to set.
	 */
	public void setPayPalPayerCountryCodeTagName(String pPayPalPayerCountryCodeTagName) {
		mPayPalPayerCountryCodeTagName = pPayPalPayerCountryCodeTagName;
	}
	/**
	 * @return the payPalBilltoNameTagName.
	 */
	public String getPayPalBilltoNameTagName() {
		return mPayPalBilltoNameTagName;
	}
	/**
	 * @param pPayPalBilltoNameTagName the payPalBilltoNameTagName to set.
	 */
	public void setPayPalBilltoNameTagName(String pPayPalBilltoNameTagName) {
		mPayPalBilltoNameTagName = pPayPalBilltoNameTagName;
	}
	/**
	 * @return the payPalBilltoStreetTagName.
	 */
	public String getPayPalBilltoStreetTagName() {
		return mPayPalBilltoStreetTagName;
	}
	/**
	 * @param pPayPalBilltoStreetTagName the payPalBilltoStreetTagName to set.
	 */
	public void setPayPalBilltoStreetTagName(String pPayPalBilltoStreetTagName) {
		mPayPalBilltoStreetTagName = pPayPalBilltoStreetTagName;
	}
	/**
	 * @return the payPalBilltoStreet2TagName.
	 */
	public String getPayPalBilltoStreet2TagName() {
		return mPayPalBilltoStreet2TagName;
	}
	/**
	 * @param pPayPalBilltoStreet2TagName the payPalBilltoStreet2TagName to set.
	 */
	public void setPayPalBilltoStreet2TagName(String pPayPalBilltoStreet2TagName) {
		mPayPalBilltoStreet2TagName = pPayPalBilltoStreet2TagName;
	}
	/**
	 * @return the payPalBilltoCityTagName.
	 */
	public String getPayPalBilltoCityTagName() {
		return mPayPalBilltoCityTagName;
	}
	/**
	 * @param pPayPalBilltoCityTagName the payPalBilltoCityTagName to set.
	 */
	public void setPayPalBilltoCityTagName(String pPayPalBilltoCityTagName) {
		mPayPalBilltoCityTagName = pPayPalBilltoCityTagName;
	}
	/**
	 * @return the payPalBilltoStateTagName.
	 */
	public String getPayPalBilltoStateTagName() {
		return mPayPalBilltoStateTagName;
	}
	/**
	 * @param pPayPalBilltoStateTagName the payPalBilltoStateTagName to set.
	 */
	public void setPayPalBilltoStateTagName(String pPayPalBilltoStateTagName) {
		mPayPalBilltoStateTagName = pPayPalBilltoStateTagName;
	}
	/**
	 * @return the payPalBilltoZipTagName.
	 */
	public String getPayPalBilltoZipTagName() {
		return mPayPalBilltoZipTagName;
	}
	/**
	 * @param pPayPalBilltoZipTagName the payPalBilltoZipTagName to set.
	 */
	public void setPayPalBilltoZipTagName(String pPayPalBilltoZipTagName) {
		mPayPalBilltoZipTagName = pPayPalBilltoZipTagName;
	}
	/**
	 * @return the payPalBillToAddressStatusTagName.
	 */
	public String getPayPalBillToAddressStatusTagName() {
		return mPayPalBillToAddressStatusTagName;
	}
	/**
	 * @param pPayPalBillToAddressStatusTagName the payPalBillToAddressStatusTagName to set.
	 */
	public void setPayPalBillToAddressStatusTagName(String pPayPalBillToAddressStatusTagName) {
		mPayPalBillToAddressStatusTagName = pPayPalBillToAddressStatusTagName;
	}
	/**
	 * @return the payPalBillToFirstNameTagName.
	 */
	public String getPayPalBillToFirstNameTagName() {
		return mPayPalBillToFirstNameTagName;
	}
	/**
	 * @param pPayPalBillToFirstNameTagName the payPalBillToFirstNameTagName to set.
	 */
	public void setPayPalBillToFirstNameTagName(String pPayPalBillToFirstNameTagName) {
		mPayPalBillToFirstNameTagName = pPayPalBillToFirstNameTagName;
	}
	/**
	 * @return the payPalTaxAmtTagName.
	 */
	public String getPayPalTaxAmtTagName() {
		return mPayPalTaxAmtTagName;
	}
	/**
	 * @param pPayPalTaxAmtTagName the payPalTaxAmtTagName to set.
	 */
	public void setPayPalTaxAmtTagName(String pPayPalTaxAmtTagName) {
		mPayPalTaxAmtTagName = pPayPalTaxAmtTagName;
	}
	/**
	 * @return the payPalShipToNamePropertyName.
	 */
	public String getPayPalShipToNamePropertyName() {
		return mPayPalShipToNamePropertyName;
	}
	/**
	 * @param pPayPalShipToNamePropertyName the payPalShipToNamePropertyName to set.
	 */
	public void setPayPalShipToNamePropertyName(String pPayPalShipToNamePropertyName) {
		mPayPalShipToNamePropertyName = pPayPalShipToNamePropertyName;
	}
	/**
	 * @return the payPalShipToStreetPropertyName.
	 */
	public String getPayPalShipToStreetPropertyName() {
		return mPayPalShipToStreetPropertyName;
	}
	/**
	 * @param pPayPalShipToStreetPropertyName the payPalShipToStreetPropertyName to set.
	 */
	public void setPayPalShipToStreetPropertyName(String pPayPalShipToStreetPropertyName) {
		mPayPalShipToStreetPropertyName = pPayPalShipToStreetPropertyName;
	}
	/**
	 * @return the payPalShipToStreet2PropertyName.
	 */
	public String getPayPalShipToStreet2PropertyName() {
		return mPayPalShipToStreet2PropertyName;
	}
	/**
	 * @param pPayPalShipToStreet2PropertyName the payPalShipToStreet2PropertyName to set.
	 */
	public void setPayPalShipToStreet2PropertyName(String pPayPalShipToStreet2PropertyName) {
		mPayPalShipToStreet2PropertyName = pPayPalShipToStreet2PropertyName;
	}
	/**
	 * @return the payPalShipToCityPropertyName.
	 */
	public String getPayPalShipToCityPropertyName() {
		return mPayPalShipToCityPropertyName;
	}
	/**
	 * @param pPayPalShipToCityPropertyName the payPalShipToCityPropertyName to set.
	 */
	public void setPayPalShipToCityPropertyName(String pPayPalShipToCityPropertyName) {
		mPayPalShipToCityPropertyName = pPayPalShipToCityPropertyName;
	}
	/**
	 * @return the payPalShipToStatePropertyName.
	 */
	public String getPayPalShipToStatePropertyName() {
		return mPayPalShipToStatePropertyName;
	}
	/**
	 * @param pPayPalShipToStatePropertyName the payPalShipToStatePropertyName to set.
	 */
	public void setPayPalShipToStatePropertyName(String pPayPalShipToStatePropertyName) {
		mPayPalShipToStatePropertyName = pPayPalShipToStatePropertyName;
	}
	/**
	 * @return the payPalShipToZipPropertyName.
	 */
	public String getPayPalShipToZipPropertyName() {
		return mPayPalShipToZipPropertyName;
	}
	/**
	 * @param pPayPalShipToZipPropertyName the payPalShipToZipPropertyName to set.
	 */
	public void setPayPalShipToZipPropertyName(String pPayPalShipToZipPropertyName) {
		mPayPalShipToZipPropertyName = pPayPalShipToZipPropertyName;
	}
	/**
	 * @return the payPalShipToCountryPropertyName.
	 */
	public String getPayPalShipToCountryPropertyName() {
		return mPayPalShipToCountryPropertyName;
	}
	/**
	 * @param pPayPalShipToCountryPropertyName the payPalShipToCountryPropertyName to set.
	 */
	public void setPayPalShipToCountryPropertyName(String pPayPalShipToCountryPropertyName) {
		mPayPalShipToCountryPropertyName = pPayPalShipToCountryPropertyName;
	}
	/**
	 * @return the payPalShipToPhoneNumPropertyName.
	 */
	public String getPayPalShipToPhoneNumPropertyName() {
		return mPayPalShipToPhoneNumPropertyName;
	}
	/**
	 * @param pPayPalShipToPhoneNumPropertyName the payPalShipToPhoneNumPropertyName to set.
	 */
	public void setPayPalShipToPhoneNumPropertyName(String pPayPalShipToPhoneNumPropertyName) {
		mPayPalShipToPhoneNumPropertyName = pPayPalShipToPhoneNumPropertyName;
	}
	/**
	 * @return the payPalIpAddrressTagName.
	 */
	public String getPayPalIpAddrressTagName() {
		return mPayPalIpAddrressTagName;
	}
	/**
	 * @param pPayPalIpAddrressTagName the payPalIpAddrressTagName to set.
	 */
	public void setPayPalIpAddrressTagName(String pPayPalIpAddrressTagName) {
		mPayPalIpAddrressTagName = pPayPalIpAddrressTagName;
	}	
	/**
	 * @return the payPalPaymentErrorCodeTagName.
	 */
	public String getPayPalPaymentErrorCodeTagName() {
		return mPayPalPaymentErrorCodeTagName;
	}
	/**
	 * @param pPayPalPaymentErrorCodeTagName the payPalPaymentErrorCodeTagName to set.
	 */
	public void setPayPalPaymentErrorCodeTagName(String pPayPalPaymentErrorCodeTagName) {
		mPayPalPaymentErrorCodeTagName = pPayPalPaymentErrorCodeTagName;
	}
	/**
	 * @return the payPalPaymentLongMessageTagName.
	 */
	public String getPayPalPaymentLongMessageTagName() {
		return mPayPalPaymentLongMessageTagName;
	}
	/**
	 * @param pPayPalPaymentLongMessageTagName the payPalPaymentLongMessageTagName to set.
	 */
	public void setPayPalPaymentLongMessageTagName(String pPayPalPaymentLongMessageTagName) {
		mPayPalPaymentLongMessageTagName = pPayPalPaymentLongMessageTagName;
	}
	/**
	 * @return the payPalPaymentReqInfoErrorCodeTagName.
	 */
	public String getPayPalPaymentReqInfoErrorCodeTagName() {
		return mPayPalPaymentReqInfoErrorCodeTagName;
	}
	/**
	 * @param pPayPalPaymentReqInfoErrorCodeTagName the payPalPaymentReqInfoErrorCodeTagName to set.
	 */
	public void setPayPalPaymentReqInfoErrorCodeTagName(String pPayPalPaymentReqInfoErrorCodeTagName) {
		mPayPalPaymentReqInfoErrorCodeTagName = pPayPalPaymentReqInfoErrorCodeTagName;
	}
	/**
	 * @return the payPalPaymentReqInfoLongMessageTagName.
	 */
	public String getPayPalPaymentReqInfoLongMessageTagName() {
		return mPayPalPaymentReqInfoLongMessageTagName;
	}
	/**
	 * @param pPayPalPaymentReqInfoLongMessageTagName the payPalPaymentReqInfoLongMessageTagName to set.
	 */
	public void setPayPalPaymentReqInfoLongMessageTagName(String pPayPalPaymentReqInfoLongMessageTagName) {
		mPayPalPaymentReqInfoLongMessageTagName = pPayPalPaymentReqInfoLongMessageTagName;
	}
	/**
	 * @return the payPalSetExpressMaxAmount.
	 */
	public String getPayPalSetExpressMaxAmount() {
		return mPayPalSetExpressMaxAmount;
	}
	/**
	 * @param pPayPalSetExpressMaxAmount the payPalSetExpressMaxAmount to set.
	 */
	public void setPayPalSetExpressMaxAmount(String pPayPalSetExpressMaxAmount) {
		mPayPalSetExpressMaxAmount = pPayPalSetExpressMaxAmount;
	}
	
	/** The Pay pal no shipping tag name. */
	private String mPayPalNoShippingTagName;
	/**
	 * @return the payPalNoShippingTagName
	 */
	public String getPayPalNoShippingTagName() {
		return mPayPalNoShippingTagName;
	}
	/**
	 * @param pPayPalNoShippingTagName the payPalNoShippingTagName to set
	 */
	public void setPayPalNoShippingTagName(String pPayPalNoShippingTagName) {
		mPayPalNoShippingTagName = pPayPalNoShippingTagName;
	}
}
