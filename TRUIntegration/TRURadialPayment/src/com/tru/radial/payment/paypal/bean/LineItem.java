package com.tru.radial.payment.paypal.bean;

import java.io.Serializable;

/**
 * @author PA
 * The Class written for holding the LineItem values which are required for paypal request.
 */
public class LineItem implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7965484367893492717L;

	/** The m name. */
	private String mName;
	
	/** The m seq no. */
	private int mSeqNo;
	
	/** The m qty. */
	private int mQty;
	
	/** The m unit price. */
	private double mUnitPrice;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * Sets the name.
	 *
	 * @param pName the new name
	 */
	public void setName(String pName) {
		this.mName = pName;
	}

	/**
	 * Gets the seq no.
	 *
	 * @return the seq no
	 */
	public int getSeqNo() {
		return mSeqNo;
	}

	/**
	 * Sets the seq no.
	 *
	 * @param pSeqNo the new seq no
	 */
	public void setSeqNo(int pSeqNo) {
		this.mSeqNo = pSeqNo;
	}

	/**
	 * Gets the qty.
	 *
	 * @return the qty
	 */
	public int getQty() {
		return mQty;
	}

	/**
	 * Sets the qty.
	 *
	 * @param pQty the new qty
	 */
	public void setQty(int pQty) {
		this.mQty = pQty;
	}

	/**
	 * Gets the unit price.
	 *
	 * @return the unit price
	 */
	public double getUnitPrice() {
		return mUnitPrice;
	}

	/**
	 * Sets the unit price.
	 *
	 * @param pUnitPrice the new unit price
	 */
	public void setUnitPrice(double pUnitPrice) {
		this.mUnitPrice = pUnitPrice;
	}
/**
 * @return String
 */
	@Override
	public String toString() {
		return "LineItem [mName=" + mName + ", mSeqNo=" + mSeqNo + ", mQty="
				+ mQty + ", mUnitPrice=" + mUnitPrice + "]";
	}
	
	
	
}
