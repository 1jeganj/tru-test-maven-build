package com.tru.radial.payment.paypal.bean;

import java.util.List;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * This class has methods to set request properties for DoVoid PayPal API call
 * 
 * @author PA
 *
 */
public class DoVoidResponse extends AbstractRadialResponse implements IRadialResponse  {
	/**
	 * Property to hold A message ID used for uniquely identify a message.
	 */
	private String mMsgSubId;
	/**
	 * Property to hold Original authorization ID specifying the authorization order ID.
	 */
	private String mAuthorizationId;
	/**
	 * Property to hold mErrors
	 */
	private List<PayPalError> mErrors;
	/**
	 * Property to hold mStatusMessage
	 */
	private String mStatusMessage;
	/**
	 * Property to hold mSuccess
	 */
	private boolean mSuccess;

	/**
	 * @return the authorizationId
	 */
	public String getAuthorizationId() {
		return mAuthorizationId;
	}

	/**
	 * @param pAuthorizationId the authorizationId to set
	 */
	public void setAuthorizationId(String pAuthorizationId) {
		mAuthorizationId = pAuthorizationId;
	}

	/**
	 * @return the message ID
	 */
	public String getMsgSubId() {
		return mMsgSubId;
	}

	/**
	 * @param pMsgSubId
	 *            the message Id to set
	 */
	public void setMsgSubId(String pMsgSubId) {
		mMsgSubId = pMsgSubId;
	}

	/**
	 * @return the errors
	 */
	public List<PayPalError> getErrors() {
		return mErrors;
	}

	/**
	 * @param pErrors the errors to set
	 */
	public void setErrors(List<PayPalError> pErrors) {
		mErrors = pErrors;
	}
	
	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return mStatusMessage;
	}
	/**
	 * @param pStatusMessage the statusMessage to set
	 */
	public void setStatusMessage(String pStatusMessage) {
		mStatusMessage = pStatusMessage;
	}
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return mSuccess;
	}
	/**
	 * @param pSuccess the success to set
	 */
	public void setSuccess(boolean pSuccess) {
		mSuccess = pSuccess;
	}
	/** The m timeout reponse. */
	private boolean mTimeoutReponse;
	
	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#isTimeOutResponse()
	 */
	@Override
	public boolean isTimeOutResponse() {
		return mTimeoutReponse;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.payment.paypal.bean.PayPalResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTimeoutReponse) {
		mTimeoutReponse = pTimeoutReponse;
		
	}
	

	
	@Override
	public void setResponseCode(String pValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getResponseCode() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
