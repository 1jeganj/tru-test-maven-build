package com.tru.radial.payment.paypal.request;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PayPalDoAuthorizationReplyType;
import com.tru.radial.payment.PayPalDoAuthorizationRequestType;
import com.tru.radial.payment.paypal.bean.AuthorizationInfo;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoAuthorizationPaymentResponse;


/**
 * @author PA
 * The Class PayPalDoAuthRequestBuilder.
 */
public class PayPalDoAuthRequestBuilder extends AbstractRequestBuilder {

	
	/** The do authorization request type. */
	private PayPalDoAuthorizationRequestType mDoAuthorizationRequestType  = null;
	
	
	/** The do authorization reply type. */
	private PayPalDoAuthorizationReplyType mDoAuthorizationReplyType = null;
	

	
	/** The m request object. */
	private JAXBElement<PayPalDoAuthorizationRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new pay pal do auth request builder.
	 */
	public PayPalDoAuthRequestBuilder(){ 		
		initDoAuthrizationCheckout();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest)throws TRURadialRequestBuilderException {
		DoAuthorizationPaymentRequest doAuthorizationCheckoutVO = null;
		if (!(pPaymentRequest instanceof DoAuthorizationPaymentRequest)) {
			throw new UnsupportedOperationException(IRadialConstants.PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTREQUEST);
		}
		doAuthorizationCheckoutVO = (DoAuthorizationPaymentRequest)pPaymentRequest;		
		mDoAuthorizationRequestType.setOrderId(doAuthorizationCheckoutVO.getOrderId());
		mDoAuthorizationRequestType.setRequestId(doAuthorizationCheckoutVO.getRequestId());		
		AmountType amountType=new AmountType();
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
		amountType.setValue(BigDecimal.valueOf(doAuthorizationCheckoutVO.getAmount()));
		mDoAuthorizationRequestType.setAmount(amountType);
		mDoAuthorizationRequestType.setSchemaVersion(IRadialConstants.SCHEMA_VERSION);	
		setRequestObject(getRadialObjectFactory().createPayPalDoAuthorizationRequest(mDoAuthorizationRequestType));
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof DoAuthorizationPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE);
		}		
		DoAuthorizationPaymentResponse response = (DoAuthorizationPaymentResponse)pPaymentResponse;		
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getDoAuthorizationReplyType().getResponseCode()) ||
				IRadialConstants.RADIAL_SUCCESS_WITH_WARNING_RESPONSE_CODE.equalsIgnoreCase(getDoAuthorizationReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);
			AuthorizationInfo authorizationInfo=new AuthorizationInfo();
			authorizationInfo.setPaymentStatus(getDoAuthorizationReplyType().getAuthorizationInfo().getPaymentStatus());
			authorizationInfo.setPendingReason(getDoAuthorizationReplyType().getAuthorizationInfo().getPendingReason());
			authorizationInfo.setReasonCode(getDoAuthorizationReplyType().getAuthorizationInfo().getReasonCode());
			response.setAuthorizationInfo(authorizationInfo);
			response.setResponseCode(getDoAuthorizationReplyType().getResponseCode());
		}else{
			buildErrorResponse(pPaymentResponse);
		}
		
	}
	
	/**
	 * <p>
	 * If the input request XML was not successful for some reason, This method process the SOAP fault element, 
	 * which gives you status information, error information, or both.
	 *
	 * @param pPaymentResponse the payment response
	 */
	@Override
	public void buildFaultResponse(IRadialResponse pPaymentResponse) {				
		if (!(pPaymentResponse instanceof DoAuthorizationPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE);
		}
		DoAuthorizationPaymentResponse response = (DoAuthorizationPaymentResponse)pPaymentResponse;
		super.buildFaultResponse(response);
		response.setTimeStamp(getTimeStamp());
	}

	
	/**
	 * It will constructs Error response.
	 * @param  pPaymentResponse - pPaymentResponse
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {			
		if (!(pPaymentResponse instanceof DoAuthorizationPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PAYMENTREQUEST_NOT_INSTANCE_DOAUTHORIZATIONPAYMENTRESPONSE);
		}
		DoAuthorizationPaymentResponse response = (DoAuthorizationPaymentResponse)pPaymentResponse;
		super.buildErrorResponse(pPaymentResponse,getDoAuthorizationReplyType());
		response.setTimeStamp(getTimeStamp());
	}
	

	
	
	/**
	 * Inits the do authrization checkout.
	 */
	private void initDoAuthrizationCheckout(){
		mDoAuthorizationRequestType = createPayPalDoAuthorizationRequestType();
	}
	
	
	/**
	 * Creates the pay pal do express checkout request.
	 *
	 * @return the pay pal do express checkout request type
	 */
	private PayPalDoAuthorizationRequestType createPayPalDoAuthorizationRequestType() {

		return getRadialObjectFactory().createPayPalDoAuthorizationRequestType();
	}



	/**
	 * Gets the do authorization reply type.
	 *
	 * @return the do authorization reply type
	 */
	public PayPalDoAuthorizationReplyType getDoAuthorizationReplyType() {
		return mDoAuthorizationReplyType;
	}

	
	/**
	 * Sets the do authorization reply type.
	 *
	 * @param pDoAuthorizationReplyType the new do authorization reply type
	 */
	public void setDoAuthorizationReplyType(
			PayPalDoAuthorizationReplyType pDoAuthorizationReplyType) {
		this.mDoAuthorizationReplyType = pDoAuthorizationReplyType;
	}



	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<PayPalDoAuthorizationRequestType> getRequestObject() {
		return mRequestObject;
	}

	
	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the new request object
	 */
	public void setRequestObject(
			JAXBElement<PayPalDoAuthorizationRequestType> pRequestObject) {
		this.mRequestObject = pRequestObject;
	}

}
