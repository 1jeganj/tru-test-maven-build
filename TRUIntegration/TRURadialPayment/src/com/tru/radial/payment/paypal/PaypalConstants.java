package com.tru.radial.payment.paypal;


/**
* This class will help to fetch the different constant used for PaymentPlugin Integrations.
* @author PA
* @version 1.0
*/
public class PaypalConstants {
	/**
	 * Constant to hold the value of Success.
	 */
	public static final String SUCCESS = "Success";
	/**
	 * Constant to hold the value of Failed.
	 */
	public static final String FAILURE = "Failed";
	/**
	 * Constant to hold the value of POST.
	 */
	public static final String POST = "POST";
	/**
	 * Constant to hold the value of AuthorizationId.
	 */
	public static final String AUTHORIZATIONID = "AuthorizationId";
	/**
	 * Constant to hold the value of CompleteType.
	 */
	public static final String COMPLETETYPE = "CompleteType";
	/**
	 * Constant to hold the value of Amount.
	 */
	public static final String AMOUNT = "Amount";
	/**
	 * Constant to hold the value of default.
	 */
	public static final String DEFAULT = "default";
	/**
	 * Constant to hold the value of proxyHost.
	 */
	public static final String HTTPS_PROXY_HOST = "https.proxyHost";
	/**
	 *  Constant to hold the value of proxyPort.
	 */
	public static final String HTTPS_PROXY_PORT = "https.proxyPort";

	/**
	 * Constant to hold the value of CONTENT_TYPE.
	 */
	public static final String CONTENT_TYPE = "Content-Type";
	/**
	 * Constant to hold the value of CONTENT_LENGTH.
	 */
	public static final String CONTENT_LENGTH = "Content-Length";
	/**
	 * Constant to hold the value of APPLICATION_OR_XML.
	 */
	public static final String APPLICATION_OR_XML = "application/x-www-form-urlencoded";
	/**
	 * Constant to hold the value of RETURN_URL.
	 */
	public static final String RETURN_URL = "RETURN_URL";
	/**
	 * Constant to hold the value of CANCEL_URL.
	 */
	public static final String CANCEL_URL = "CANCEL_URL";
	/**
	 * Constant to hold the value of VERSION.
	 */
	public static final String VERSION = "VERSION=";
	/**
	 * Constant to hold the value of USER.
	 */
	public static final String USER = "USER=";
	/**
	 * Constant to hold the value of ERROR_PAYAPAL_USER.
	 */
	public static final String ERROR_PAYAPAL_USER = "USER";
	/**
	 * Constant to hold the value of SIGNATURE.
	 */
	public static final String SIGNATURE = "SIGNATURE=";
	/**
	 * Constant to hold the value of PWD.
	 */
	public static final String PWD = "PWD=";
	/**
	 * Constant to hold the value of ERROR_PAYPAL_PWD.
	 */
	public static final String ERROR_PAYPAL_PWD = "PWD";
	/**
	 * Constant to hold the value of AMPERSAND.
	 */
	public static final String AMPERSAND = "&";
	/**
	 * Constant to hold the value of EQUAL_TO.
	 */
	public static final String EQUAL_TO = "=";
	/**
	 * Constant to hold the value of FOUR_THOUSAND.
	 */
    public static final int FOUR_THOUSAND = 4000;
    /**
	 * Constant to hold the value of SALE.
	 */
	public static final String SALE = "Sale";
	 /**
	 * Constant to hold the value of DIGIT_TWO.
	 */
	public static final int DIGIT_TWO = 2;
	 /**
	 * Constant to hold the value of DOUBLE_ZERO.
	 */
	public static final double DOUBLE_ZERO = 0.0;
	 /**
	 * Constant to hold the value of ZERO.
	 */
	public static final String DIGIT_ZERO = "0";
	/**
	 * Constant to hold the value of ZERO.
	 */
	public static final String DO_ADDRESS_OVERRIDE_PAYPAL = "1";
	/**
	 * Constant to hold the value of ZERO.
	 */
	public static final String REPLACE_PATTERN = "$$";
	/**
	 * Constant to hold the value of -1.
	 */
	public static final int DIGIT_ONE = 1;
	/**
	 * Constant to hold the value of 5.
	 */
	public static final int DIGIT_FIVE = 5;
	
	/** The Constant CONSTANT_EMPTY. */
	public static final String EMPTY_STRING = "";
	/**
	 * Constant to hold the value UTF-8 Encoding format.
	 */ 
	public static final String ENCODING_FORMAT = "UTF-8";
	/**
	 * Constant to hold the value  SSLUtil JRE.
	 */
	public static final String SSLUTIL_JRE = "sslutil.jre";
	/**
	 * Constant to hold the value SSLUtil Protocol
	 */ 
	public static final String SSLUTIL_PROTOCOL = "sslutil.protocol";
	/**
	 * Constant to hold PayPal Device IP Address Header.
	 */ 
	public static final String PAYPAL_DEVICE_IPADDRESS_HEADER = "X-PAYPAL-DEVICE-IPADDRESS";
	/**
	 * Constant to hold the value HTTP POST method.
	 */ 
	public static final String HTTP_POST_METHOD = "POST";
	/**
	 * Constant to hold the value HTTP GET method
	 */ 
	public static final String HTTP_GET_METHOD = "GET";
	/**
	 * Constant to hold the value HTTP PUT method.
	 */ 
	public static final String HTTP_PUT_METHOD = "PUT";
	/**
	 * Constant to hold the value HTTP PATCH method
	 */ 
	public static final String HTTP_PATCH_METHOD = "PATCH";
	/**
	 * Constant to hold the value /?
	 */ 
	public static final String URL_APPEND_MARK = "?";
	/**
	 * Constant to hold the value of ORDER_PAYMENT_ACTION
	 */ 
	public static final String ORDER_PAYMENT_ACTION = "Order";
	/**
	 * constant to hold 1 for order includes only electronic items.
	 */
	public static final String  ALL_ELECTRONICITEMS ="1";
	
	/**
	 * constant to hold 1 for REQBILLINGADDRESS.
	 */
	public static final String REQ_BILLING_ADDRESS ="1";
	
	/**
	 * constant to hold 0 for order includes hardgood items.
	 */
	public static final String  INCLUDES_HARDGOODITEMS="0";	
	/**
	 * constant to hold Sale.
	 */
	public static final String PAYPAL_PAYMENT_ACTION_SALE= "Sale";

	/**
	 * constant to hold application/x-www-form-urlencoded.
	 */
	public static final String PAYPAL_HTTP_CONTENT_TYP = "application/x-www-form-urlencoded";

	/**
	 * POST method is used for PAYPAL.
	 */
	public static final String PAYPAL_METHOD_TYPE = "POST";
	/**
	 * constant to hold VERSION.
	 */
	public static final String PAYPAL_VERSION_TAG = "VERSION";
	/**
	 * constant to hold Success.
	 */
	public static final String PAYPAL_SUCCESS_STATUS = "Success";
	/**
	 * constant to hold SuccessWithWarning.
	 */
	public static final String PAYPAL_WARNING_STATUS = "SuccessWithWarning";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_DOEXPRCHKOUTPAYMENT_METHOD = "DoExpressCheckoutPayment";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_GETEXPR_CHKOUT_METHOD = "GetExpressCheckoutDetails";
	/**
	 * constant to hold SetExpressCheckout.
	 */
	public static final String PAYPAL_SETEXPRESSCHECKOUT_METHOD = "SetExpressCheckout";
	 /**
     * Holds colon public static final String.
     */
    public static final String COLON_STRING = ":";
    /**
     * Constant to hold string "".
     */
    public static final String BLANK_STRING = "";
    /**
	 * Holds the message for null request.
	 */
	public static final String GET_EXP_CHK_REQUEST_NULL_MSG = "GetExpCheckoutRequest is null. GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for Invalid Response.
	 */
	public static final String GET_EXP_CHK_INVALID_RESPONSE_MSG = "PayPal Response is null/blank, GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for errror response.
	 */
	public static final String GET_EXP_CHK_ERROR_RESPONSE_MSG = "Error response from PayPal, GetExpressCheckoutDetails failed";
	 /**
     * Constant to hold the paypal date format.
     */
    public static final String ORDER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    /**
     * Constant to hold the int value 1.
     */
    public static final int ONE = 1;
    /**
     * Constant to hold the constant for "_".
     */
    public static final String UNDER_SCORE = "_";     
 
    /**
     * Constant to hold the constant for "PAYPAL_PAYMENT_AUTH_ERROR".
     */
    public static final String PAYPAL_PAYMENT_AUTH_ERROR = "PAYPAL_PAYMENT_AUTH_ERROR";
    
    /**
     * Constant to hold the constant for "PAYPAL_PAYMENT_DEBIT_ERROR".
     */
    public static final String REQUEST_ID = "requestId";  
   
    /**
     * Constant to hold the constant for firstName.
     */
    public static final String FIRSTNAME = "firstName";    
    
    /**
     * Constant to hold the constant for lastName.
     */
    public static final String LASTNAME = "lastName";
   
    
    /**
     * Constant to hold the constant for address1.
     */
    public static final String ADDRESS1 = "address1";    
    
    /**
     * Constant to hold the constant for address2.
     */
    public static final String ADDRESS2 = "address2";    
    
    /**
     * Constant to hold the constant for city.
     */
    public static final String CITY = "city";    
    
    /**
     * Constant to hold the constant for state.
     */
    public static final String STATE = "state";    
    
    /**
     * Constant to hold the constant for country.
     */
    public static final String COUNTRY = "country";
    
    /**
     * Constant to hold the constant for postalCode.
     */
    public static final String POSTAL_CODE = "postalCode";
    /**
     * Constant to hold the constant for phoneNumber.
     */
    public static final String PHONE_NUMBER = "phoneNumber";
    /**
     * Constant to hold the constant for mobilePhone.
     */
    public static final String MOBILE_NUMBER = "mobilePhone";
    
    /**
     * Constant to hold the number maximum fractional digits allowed for payparequest.
     */
    public static final int MAXIMUM_FRACTIONALDIGITS = 2;
    
    /**
     * Constant to hold the cart.
     */
    public static final String CART = "Cart";   
    
    /**
     * Constant to hold the zero value.
     */
    public static final int ORDERAMT_ZERO = 0;
    
    /**
     * Constant to hold the zero value.
     */
    public static final double ORDER_ZEROAMT = 0d;
    
    /**
     * Constant to hold comments.
     */
    public static final String COMMENTS = "comments";     
    
    /**
     * Constant to hold InvalidPaymentGroupParameter.
     */
    public static final String INVALIDPAYMENTGROUP_PARAMETER = "InvalidPaymentGroupParameter";  
    
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String PAYPAL_LOGGING_IDENTIFIER = "TRUProcValidatePayPal";
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String USER_MESSAGE_BUNDLE = "atg.commerce.order.UserMessages";
    /**
     * Constant to hold USD.
     */
    public static final String USD = "USD";    
    /**
     * Constant to hold ZERO_AUTH_TRANS_ID.
     */
    public static final String ZERO_AUTH_TRANSACTION_ID = "ZERO_AUTH_TRANS_ID";    
    /**
     * Constant to hold ZERO_DEBIT_TRANS_ID.
     */
    public static final String ZERO_DEBIT_TRANSACTION_ID = "ZERO_DEBIT_TRANS_ID";    
	/**
	 * Constant to hold the value of HTTP_RESPONSE_200.
	 */ 
	public static final int HTTP_RESPONSE_200 = 200;
	/**
	 * Constant to hold the value of HTTP RESPONSE 300.
	 */ 
	public static final int HTTP_RESPONSE_300 = 300;
	/**
	 * Constant to hold the value of HTTP RESPONSE 500.
	 */ 
	public static final int HTTP_RESPONSE_500 = 500;
	/**
	 * Constant to hold the value of HTTP RESPONSE 500.
	 */ 
	public static final String PAYPAL_RETRY_ERROR = "retry fails..  check log for more information";
	/**
	 * Constant to hold the value of HTTP RESPONSE 500.
	 */ 
	public static final String PAYPAL_EXCEPTION_TEXT = "Response Code : $$ with response : ";
	
	/**
	 * Constant to hold the Paypal reponse param name.
	 */
	public static final String PAYPAL_RESP_MSG_PARAM_NAME_PAYMENTINFO = "PAYMENTINFO_";
	
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_AUTH_RESP_PARAM_NAME_PENDINGREASON = "PENDINGREASON";
	
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_RESP_PARAM_NAME_REASONCODE = "_REASONCODE";
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_RESP_PARAM_NAME_PENDINGREASON = "_PENDINGREASON";
	
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_RESP_MSG_PARAM_NAME_PAYMENTSTATUS = "_PAYMENTSTATUS";
	
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_RESP_MSG_PARAM_NAME_AMT = "_AMT";
	
	/**
	 * Constant to hold the Paypal reponse param name. 
	 */
	public static final String PAYPAL_RESP_MSG_PARAM_NAME_TRANSACTIONID = "_TRANSACTIONID";
	/**
	 * Constant to hold the value of PayPal Method Failure.
	 */
	public static final String ACK_FAILURE = "Failure";
	/**
	 * Constant to hold the value of INTEGER_ZERO
	 */
	public static final int INTEGER_ZERO = 0;
	/**
	 * Constant to hold the value of SuccessWithWarning.
	 */
	public static final String SUCCESS_WITH_WARNING = "SuccessWithWarning";
	/**
	 * Constant to hold the value of PayPal Method FailureWithWarning.
	 */
	public static final String ACK_WITH_FAILURE_WARN = "FailureWithWarning";
	/**
	 * Constant for DOUBLE_DOT.
	 */
	public static final String HYPHEN = "-";
	/**
	 * Constant for ACK.
	 */
	public static final String ACK = "ACK";
	/**
	 * Constant for BUILD.
	 */
	public static final String BUILD = "Build";
	/**
	 * Constant for CORRELATION_ID.
	 */
	public static final String CORRELATION_ID = "CorrelationId";	
	/**
	 * Constant for ERROR_LIST.
	 */
	public static final String ERROR_LIST = "Error List";
	/**
	 * Constant for PAYMENT_STATUS.
	 */
	public static final String PAYMENT_STATUS = "PaymentStatus";
	/**
	 * Constant for STATUS_MESSAGE.
	 */
	public static final String STATUS_MESSAGE = "StatusMessage";
	/**
	 * Constant for TIME_STAMP.
	 */
	public static final String TIME_STAMP = "TimeStamp";
	/**
	 * Constant for TOKEN.
	 */
	public static final String TOKEN = "Token";
	/**
	 * Constant for TRANSACTION_ID.
	 */
	public static final String TRANSACTION_ID = "TransactionId";
	/**
	 * Constant for PAYMENT_INFO.
	 */
	public static final String PAYMENT_INFO = "PaymentInfo";
	/** 
	 * The Constant OPEN_BRACKET.
	 */
	public static final String OPEN_BRACKET="[";
	/** 
	 * The Constant CLOSE_BRACKET.
	 */
	public static final String CLOSE_BRACKET="]";
	/** The Constant SPACE. */
	public static final String SPACE = " ";
	/**
	 * Static property to hold comma.
	 */
	public static final String COMMA = ",";
	
}

