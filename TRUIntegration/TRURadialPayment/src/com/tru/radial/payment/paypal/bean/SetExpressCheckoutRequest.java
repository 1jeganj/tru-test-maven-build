package com.tru.radial.payment.paypal.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
* This class has methods to set request properties for CreateToken API call.
* @author PA
* @version 1.0
*/
public class SetExpressCheckoutRequest implements PayPalRequest, IRadialRequest {
	
	
	/** 
	 * The order id. 
	 */
	private String mOrderId;
		
	/**
	 * Property to hold mReturnURL.
	 */
	private String mReturnURL;
	
	/**
	 * Property to hold mCancelURL.
	 */
	private String mCancelURL;
	
	/**
	 * Property to hold mLocaleCode.
	 */
	private String mLocaleCode;
	
	/**
	 * Property to hold mAmount.
	 */
	private double mAmount;
	
	/**
	 * Property to hold mCurrencyCode.
	 */
	private String mCurrencyCode;
	
	/**
	 * Property to hold mAddOverRide.
	 */
	private String mAddOverRide;	
	
	/** The ship to home. */
	private String mShipToName;
	
	
	/** The shipping address. */
	private Address mShippingAddress;
	
	/** The Line items total. */
	private double mLineItemsTotal;
	
	/** The shipping total. */
	private double mShippingTotal;
	
	/** The tax total. */
	private double mTaxTotal;
	
	/** The commerce items. */
	//private List<LineItem> mLineItems;
	
	
	/** The recurring. */
	private boolean mRecurring;
	
	/** The installment. */
	private boolean mInstallment;
	
	/** The schema version. */
	private String mSchemaVersion;
	
	/** The m page name. */
	private String mPageName;
	
	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * Sets the page name.
	 *
	 * @param pPageName the new page name
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}

	/**
	 * Gets the order id.
	 *
	 * @return the order id
	 */
	public String getOrderId() {
		return mOrderId;
	}
	
	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}
	
	/**
	 * Gets the ship to home.
	 * 
	 * @return the ship to home
	 */
	public String getShipToName() {
		return mShipToName;
	}
	
	/**
	 * Sets the ship to home.
	 * 
	 * @param pShipToName
	 *            the new ship to home
	 */
	public void setShipToName(String pShipToName) {
		this.mShipToName = pShipToName;
	}
	
	/**
	 * Gets the shipping address.
	 * 
	 * @return the shipping address
	 */
	public Address getShippingAddress() {
		return mShippingAddress;
	}
	
	/**
	 * Sets the shipping address.
	 * 
	 * @param pShippingAddress
	 *            the new shipping address
	 */
	public void setShippingAddress(Address pShippingAddress) {
		this.mShippingAddress = pShippingAddress;
	}
	
	/**
	 * Gets the order total.
	 * 
	 * @return the order total
	 */
	public double getLineItemsTotal() {
		return mLineItemsTotal;
	}
	
	/**
	 * Sets the order total.
	 *
	 * @param pLineItemsTotal the new line items total
	 */
	public void setLineItemsTotal(double pLineItemsTotal) {
		this.mLineItemsTotal = pLineItemsTotal;
	}
	
	/**
	 * Gets the shipping total.
	 * 
	 * @return the shipping total
	 */
	public double getShippingTotal() {
		return mShippingTotal;
	}
	
	/**
	 * Sets the shipping total.
	 * 
	 * @param pShippingTotal
	 *            the new shipping total
	 */
	public void setShippingTotal(double pShippingTotal) {
		this.mShippingTotal = pShippingTotal;
	}
	
	/**
	 * Gets the tax total.
	 * 
	 * @return the tax total
	 */
	public double getTaxTotal() {
		return mTaxTotal;
	}
	
	/**
	 * Sets the tax total.
	 * 
	 * @param pTaxTotal
	 *            the new tax total
	 */
	public void setTaxTotal(double pTaxTotal) {
		this.mTaxTotal = pTaxTotal;
	}		
	
	/**
	 * Gets the line items.
	 *
	 * @return the line items
	 *//*
	public List<LineItem> getLineItems() {
		return mLineItems;
	}

	*//**
	 * Sets the line items.
	 *
	 * @param pLineItems the new line items
	 *//*
	public void setLineItems(List<LineItem> pLineItems) {
		this.mLineItems = pLineItems;
	}
*/
	/**
	 * Checks if is recurring.
	 * 
	 * @return true, if is recurring
	 */
	public boolean isRecurring() {
		return mRecurring;
	}
	
	/**
	 * Sets the recurring.
	 * 
	 * @param pRecurring
	 *            the new recurring
	 */
	public void setRecurring(boolean pRecurring) {
		this.mRecurring = pRecurring;
	}
	
	/**
	 * Checks if is installment.
	 *
	 * @return true, if is installment
	 */
	public boolean isInstallment() {
		return mInstallment;
	}
	
	/**
	 * Sets the installment.
	 *
	 * @param pInstallment the new installment
	 */
	public void setInstallment(boolean pInstallment) {
		this.mInstallment = pInstallment;
	}
	
	/**
	 * Gets the schema version.
	 *
	 * @return the schema version
	 */
	public String getSchemaVersion() {
		return mSchemaVersion;
	}
	
	/**
	 * Sets the schema version.
	 *
	 * @param pSchemaVersion the new schema version
	 */
	public void setSchemaVersion(String pSchemaVersion) {
		this.mSchemaVersion = pSchemaVersion;
	}

	//TODO :Start  Cleanup required for the properties after radial paypal integration
	/**
	 * Property to hold mReqBillingAddress.
	 */
	private String mReqBillingAddress;	
	/**
	 * Property to hold mPaymentRequestList.
	 */	
	private List<PaymentDetails> mPaymentRequestList;
	
	/**
	 * Property to hold mBillingAddress.
	 */
	private Address mBillingAddress;
	
	/**
	 * Property to hold mHashMap.
	 */
	private  Map<String,Object> mRequestParameter=new HashMap<String, Object>();
	/**
	 * Property to hold mSiteId.
	 */
	private String mSiteId;
	/**
	 * Property to hold mPaymentAction.
	 */
	private String mPaymentAction;
	
	
	//TODO :End  Cleanup required for the properties after radial paypal integration
	
	/**
	 * @return the cancelURL
	 */
	public String getCancelURL() {
		return mCancelURL;
	}
	/**
	 * @param pCancelURL the cancelURL to set
	 */
	public void setCancelURL(String pCancelURL) {
		mCancelURL = pCancelURL;
	}
	/**
	 * @return the localeCode
	 */
	public String getLocaleCode() {
		return mLocaleCode;
	}
	/**
	 * @param pLocaleCode the localeCode to set
	 */
	public void setLocaleCode(String pLocaleCode) {
		mLocaleCode = pLocaleCode;
	}	
	/**
	 * @return the returnURL
	 */
	public String getReturnURL() {
		return mReturnURL;
	}
	/**
	 * @param pReturnURL the returnURL to set
	 */
	public void setReturnURL(String pReturnURL) {
		mReturnURL = pReturnURL;
	}	
	/**
	 * @return the paymentAction
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return mAmount;
	}
	/**
	 * @param pAmount the amount to set
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}
	/**
	 * @return the billingAddress
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	/**
	 * @param pBillingAddress the billingAddress to set
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return mSiteId;
	}
	/**
	 * @param pSiteId the siteId to set
	 */
	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}
	/**
	 * This method is to set any extra properties from Request.
	 * @param pParameter the Parameter to get
	 * @return the object
	 */
	@Override
	public Object getRequestParameter(String pParameter) {
		Object object = mRequestParameter.get(pParameter);
		return object;
	}
	/**
	 * This method is to set any extra properties in Request.
	 * @param pValue to set
	 * @param pParam the Parameter to set
	 */
	@Override
	public void setRequestParameter(String pParam, Object pValue) {
		if(mRequestParameter!= null){
			mRequestParameter.put(pParam, pValue);
		}else {
			mRequestParameter=new HashMap<String, Object>();
			mRequestParameter.put(pParam, pValue);
		}
	}
	/**
	 * @return the paymentRequestList
	 */
	public List<PaymentDetails> getPaymentRequestList() {
		return mPaymentRequestList;
	}
	/**
	 * @param pPaymentRequestList the paymentRequestList to set
	 */
	public void setPaymentRequestList(List<PaymentDetails> pPaymentRequestList) {
		mPaymentRequestList = pPaymentRequestList;
	}
	/**
	 * @return the reqBillingAddress
	 */
	public String getReqBillingAddress() {
		return mReqBillingAddress;
	}
	/**
	 * @param pReqBillingAddress the reqBillingAddress to set
	 */
	public void setReqBillingAddress(String pReqBillingAddress) {
		mReqBillingAddress = pReqBillingAddress;
	}
	
	
	
	/** The Standard pay pal. */
	private boolean mStanderdPayPal;
	
	/**
	 * @return the standerdPayPal
	 */
	public boolean isStanderdPayPal() {
		return mStanderdPayPal;
	}
	/**
	 * @param pStanderdPayPal the standerdPayPal to set
	 */
	public void setStanderdPayPal(boolean pStanderdPayPal) {
		mStanderdPayPal = pStanderdPayPal;
	}
	
	/** The Express pay pal. */
	private boolean mExpressPayPal;

	/**
	 * @return the expressPayPal
	 */
	public boolean isExpressPayPal() {
		return mExpressPayPal;
	}
	/**
	 * @param pExpressPayPal the expressPayPal to set
	 */
	public void setExpressPayPal(boolean pExpressPayPal) {
		mExpressPayPal = pExpressPayPal;
	}
	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 */
	/*@Override
	public String toString() {
		return new StringBuilder().append("ReqBillingAddress : " + getReqBillingAddress()).
				append(", Amount : " + getAmount()).append(", GetCancelUrl" + getCancelURL()).
				append(", CurrencyCode : "  + getCurrencyCode()).append(", Local Code : " + getLocaleCode()).
				append(", PaymentAction: "  + getPaymentAction()).append(", ReqBillingAddress : "+  getReqBillingAddress()).
				append(", ReturnUrl : " + getReturnURL()).append(", Site Id "+ getSiteId()).append(", BillingAddress" + getBillingAddress()).
				append(", PaymentRequestList"+getPaymentRequestList()).
				append(", mStanderdPayPal"+ isStanderdPayPal()).
				append(", mExpressPayPal"+ isExpressPayPal() ).toString();

	}
*/
	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 */
	@Override
	public String toString() {
		return "SetExpressCheckoutRequest [mOrderId=" + mOrderId
				+ ", mReturnURL=" + mReturnURL + ", mCancelURL=" + mCancelURL
				+ ", mAmount=" + mAmount + ", mAddOverRide=" + mAddOverRide
				+ ", mShipToName=" + mShipToName + ", mShippingAddress="
				+ mShippingAddress + ", mlineItemsTotal=" + mLineItemsTotal
				+ ", mShippingTotal=" + mShippingTotal + ", mTaxTotal="
				+ mTaxTotal + ", mStanderdPayPal=" + mStanderdPayPal
				+ ", mExpressPayPal=" + mExpressPayPal + "]";
	}
	
	/** The No Shipping Address Display. */
	private String mNoShippingAddressDisplay;

	/**
	 * @return the noShippingAddressDisplay
	 */
	public String getNoShippingAddressDisplay() {
		return mNoShippingAddressDisplay;
	}
	
	/**
	 * @param pNoShippingAddressDisplay the noShippingAddressDisplay to set
	 */
	public void setNoShippingAddressDisplay(String pNoShippingAddressDisplay) {
		this.mNoShippingAddressDisplay = pNoShippingAddressDisplay;
	}

	/**
	 * @return the mAddOverRide
	 */
	public String getAddOverRide() {
		return mAddOverRide;
	}

	/**
	 * @param pAddOverRide the mAddOverRide to set
	 */
	public void setAddOverRide(String pAddOverRide) {
		this.mAddOverRide = pAddOverRide;
	}
	
	
}
