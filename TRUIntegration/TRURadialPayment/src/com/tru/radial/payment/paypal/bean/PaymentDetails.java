package com.tru.radial.payment.paypal.bean;

import atg.core.util.ContactInfo;

/**
* This class has methods to set request properties for PaymentRequest  call.
* @author PA
* @version 1.0
*
*/
public class PaymentDetails {
	/**
	 * Property to hold mTaxAmt.
	 */
	private String mTaxAmt;
	/**
	 * Property to hold mCURRENCYCODE.
	 */
	private String mCurrencyCode;
	/**
	 * Property to hold mPAYMENTACTION.
	 */
	private String mPaymentAction;
	/**
	 * Property to hold mAMT.
	 */
	private String mAmt;
	/**
	 * Property to hold mShippingAddress.
	 */
	private ContactInfo mShippingAddress;
	/**
	 * Property to hold mItemAmt.
	 */
	private String mItemAmt;
	/**
	 * Property to hold mShippingAmt.
	 */
	private String mShippingAmt;
	/**
	 * Property to hold mInvNum.
	 */
	private String mInvNum;
	/**
	 * Property to hold mPayPalError.
	 */
	private PayPalError mPayPalError;
	/**
	 * @return the currencyCode.
	 */
	public String getCurrencyCode() {
		return mCurrencyCode;
	}
	/**
	 * @param pCurrencyCode the currencyCode to set.
	 */
	public void setCurrencyCode(String pCurrencyCode) {
		mCurrencyCode = pCurrencyCode;
	}
	/**
	 * @return the paymentAction.
	 */
	public String getPaymentAction() {
		return mPaymentAction;
	}
	/**
	 * @param pPaymentAction the paymentAction to set.
	 */
	public void setPaymentAction(String pPaymentAction) {
		mPaymentAction = pPaymentAction;
	}
	/**
	 * @return the amt.
	 */
	public String getAmt() {
		return mAmt;
	}
	/**
	 * @param pAmt the amt to set.
	 */
	public void setAmt(String pAmt) {
		mAmt = pAmt;
	}
	/**
	 * @return the itemAmt.
	 */
	public String getItemAmt() {
		return mItemAmt;
	}
	/**
	 * @param pItemAmt the itemAmt to set.
	 */
	public void setItemAmt(String pItemAmt) {
		mItemAmt = pItemAmt;
	}
	/**
	 * @return the shippingAmt.
	 */
	public String getShippingAmt() {
		return mShippingAmt;
	}
	/**
	 * @param pShippingAmt the shippingAmt to set.
	 */
	public void setShippingAmt(String pShippingAmt) {
		mShippingAmt = pShippingAmt;
	}
	/**
	 * @return the invNum.
	 */
	public String getInvNum() {
		return mInvNum;
	}
	/**
	 * @param pInvNum the invNum to set.
	 */
	public void setInvNum(String pInvNum) {
		mInvNum = pInvNum;
	}
	/**
	 * @return the taxAmt.
	 */
	public String getTaxAmt() {
		return mTaxAmt;
	}
	/**
	 * @param pTaxAmt the taxAmt to set.
	 */
	public void setTaxAmt(String pTaxAmt) {
		mTaxAmt = pTaxAmt;
	}
	/**
	 * @return the shippingAddress.
	 */
	public ContactInfo getShippingAddress() {
		if(mShippingAddress == null){
			mShippingAddress = new ContactInfo();
		}
		return mShippingAddress;
	}
	/**
	 * @param pShippingAddress the shippingAddress to set.
	 */
	public void setShippingAddress(ContactInfo pShippingAddress) {
		mShippingAddress = pShippingAddress;
	}
	
	/**
	 * @return the payPalError.
	 */
	public PayPalError getPayPalError() {
		return mPayPalError;
	}

	/**
	 * @param pPayPalError the payPalError to set.
	 */
	public void setPayPalError(PayPalError pPayPalError) {
		mPayPalError = pPayPalError;
	}
	
	/**
	 * Overriding To string Method to Print the Request Object.
	 * 
	 * @return the String object
	 */
	@Override
	public String toString() {
		
		return new StringBuilder(" Payment Details [::::::").append( ", Amt  :" + getAmt()).append(", CurrrencyCode : "+ getCurrencyCode()).
				append(", InvNum :" + getInvNum()).append(", ItemAmount : " + getItemAmt()).append(", PaymentAction : " +getPaymentAction()).
				append(", ShippingAmnt :" + getShippingAmt()).
				append(", Tax Ammount : " + getTaxAmt()).append(", Shipping Address : "+ getShippingAddress() ).
				append(" ::::] ").append(", PayPal Error :: "+getPayPalError()).toString();
	}
}
