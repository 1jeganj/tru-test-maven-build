package com.tru.radial.payment.paypal.bean;



/**
 * @version 1.0
 * @author PA
 * This interface has methods to set Payment plugin request.
 * 
 * @version 1.0
 */
public interface PayPalRequest{
	/**
	 * This method is used for fetching request params to be used for different payment plugins.
	 *
	 * @param pParameter String type
	 * @return object
	 */
	Object getRequestParameter(String pParameter);
	/**
	 * This method is used for fetching siteId  to be used for different payment plugins.
	 * @return String type
	 */
	String getSiteId();
	/**
	 * This method is used for setting request params to be used for different payment plugins.
	 * @param pParam2 Object type
	 * @param  pParam1 String type
	 */
	void setRequestParameter(String pParam1, Object pParam2);

	/**
	 * This method is used for setting siteId to be used for different payment plugins.
	 * @param pSiteId String type
	 */
	void setSiteId(String pSiteId);
}
