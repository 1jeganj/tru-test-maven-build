package com.tru.radial.payment.paypal.request;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.RadialAddressUtil;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PayPalDoExpressCheckoutReplyType;
import com.tru.radial.payment.PayPalDoExpressCheckoutRequestType;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentRequest;
import com.tru.radial.payment.paypal.bean.DoExpressCheckoutPaymentResponse;
import com.tru.radial.payment.paypal.bean.PaymentInfoResp;

/**
 * @author PA
 * The Class PayPalDoExpressRequestBuilder.
 */
public class PayPalDoExpressRequestBuilder extends AbstractRequestBuilder {

	/** The get express checkout request type. */
	private PayPalDoExpressCheckoutRequestType mDoExpressCheckoutRequestType = null;
	
	/** The get express checkout reply type. */
	private PayPalDoExpressCheckoutReplyType mDoExpressCheckoutReplyType = null;
	

	/** The request object. */
	private JAXBElement<PayPalDoExpressCheckoutRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new pay pal do express request builder.
	 */
	public PayPalDoExpressRequestBuilder(){ 		
		initDoExpressCheckout();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest)throws TRURadialRequestBuilderException {
		DoExpressCheckoutPaymentRequest doExpressCheckoutRequestVO = null;
		if (!(pPaymentRequest instanceof DoExpressCheckoutPaymentRequest)) {
			throw new UnsupportedOperationException(IRadialConstants.PPAYMENTREQUEST_INSTANCE_DOEXPRESSCHECKOUTPAYMENTREQUEST);
		}
		doExpressCheckoutRequestVO = (DoExpressCheckoutPaymentRequest)pPaymentRequest;
		
		if(StringUtils.isNotBlank(doExpressCheckoutRequestVO.getPickUpStoreId())){
			mDoExpressCheckoutRequestType.setPickUpStoreId(doExpressCheckoutRequestVO.getPickUpStoreId());
		}
		mDoExpressCheckoutRequestType.setRequestId(doExpressCheckoutRequestVO.getRequestId());
		mDoExpressCheckoutRequestType.setOrderId(doExpressCheckoutRequestVO.getOrderId());
		mDoExpressCheckoutRequestType.setToken(doExpressCheckoutRequestVO.getTokenId());
		mDoExpressCheckoutRequestType.setPayerId(doExpressCheckoutRequestVO.getPayerId());
		AmountType amountType=new AmountType();
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
		amountType.setValue(BigDecimal.valueOf(doExpressCheckoutRequestVO.getAmount()));
		mDoExpressCheckoutRequestType.setAmount(amountType);		
		if(StringUtils.isNotBlank(doExpressCheckoutRequestVO.getShipToName())){
			mDoExpressCheckoutRequestType.setShipToName(doExpressCheckoutRequestVO.getShipToName());
			mDoExpressCheckoutRequestType.setShippingAddress(RadialAddressUtil.copyAddress(doExpressCheckoutRequestVO.getShippingAddress()) );
		}
		//Setting order's total,shippingCost and taxAmount
		/*LineItemsType itemsType=new LineItemsType();
		AmountNegativeAllowedType orderSubTotal=new AmountNegativeAllowedType();
		orderSubTotal.setCurrencyCode(ISOCurrencyCodeType.USD);
		orderSubTotal.setValue(BigDecimal.valueOf(doExpressCheckoutRequestVO.getItemsAmount()));
		
		AmountType shipTotal=new AmountType();
		shipTotal.setCurrencyCode(ISOCurrencyCodeType.USD);
		shipTotal.setValue(doExpressCheckoutRequestVO.getShippingCost());
		
		AmountType taxTotal=new AmountType();
		taxTotal.setCurrencyCode(ISOCurrencyCodeType.USD);
		taxTotal.setValue(doExpressCheckoutRequestVO.getTaxAmount());
		
		itemsType.setLineItemsTotal(orderSubTotal);
		itemsType.setShippingTotal(shipTotal);
		itemsType.setTaxTotal(taxTotal);
		
		if(doExpressCheckoutRequestVO.getLineItems()!=null && !doExpressCheckoutRequestVO.getLineItems().isEmpty()){
			List<LineItem> lineItems = doExpressCheckoutRequestVO.getLineItems();
			for(LineItem lineItem : lineItems){
				LineItemType itemType = new LineItemType();
				itemType.setName(lineItem.getName());
				itemType.setQuantity(lineItem.getQty());
				itemType.setSequenceNumber(String.valueOf(lineItem.getSeqNo()));
				AmountNegativeAllowedType itemUnitAmount = new AmountNegativeAllowedType();
				itemUnitAmount.setCurrencyCode(ISOCurrencyCodeType.USD);
				itemUnitAmount.setValue(new BigDecimal(lineItem.getUnitPrice()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				//itemUnitAmount.setValue(BigDecimal.valueOf(lineItem.getUnitPrice()));
				itemType.setUnitAmount(itemUnitAmount);
				itemsType.getLineItem().add(itemType);
			}
		}			
		//mDoExpressCheckoutRequestType.setLineItems(itemsType);
*/		mDoExpressCheckoutRequestType.setSchemaVersion(IRadialConstants.SCHEMA_VERSION);		
		setRequestObject(getRadialObjectFactory().createPayPalDoExpressCheckoutRequest(mDoExpressCheckoutRequestType));
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof DoExpressCheckoutPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PPAYMENTRESPONSE_NOT_INSTANCE_DOEXPRESSCHECKOUTPAYMENTRESPONSE);
		}
		DoExpressCheckoutPaymentResponse response = (DoExpressCheckoutPaymentResponse)pPaymentResponse;		
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getDoExpressCheckoutReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);
			response.setTranscationId(getDoExpressCheckoutReplyType() .getTransactionID());			
			PaymentInfoResp paymentInfoResp=new PaymentInfoResp();
			paymentInfoResp.setReasonCode(getDoExpressCheckoutReplyType().getPaymentInfo().getReasonCode());
			paymentInfoResp.setPendingReasons(getDoExpressCheckoutReplyType().getPaymentInfo().getPendingReason());
			paymentInfoResp.setPaymentStatus(getDoExpressCheckoutReplyType().getPaymentInfo().getPaymentStatus());
			response.setPaymentInfoResp(paymentInfoResp);	
			response.setResponseCode(getDoExpressCheckoutReplyType().getResponseCode());
		}else{
			buildErrorResponse(pPaymentResponse);
		}
			
	}
	
	@Override
	public void buildFaultResponse(IRadialResponse pPaymentResponse) {				
		if (!(pPaymentResponse instanceof DoExpressCheckoutPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PPAYMENTRESPONSE_NOT_INSTANCE_DOEXPRESSCHECKOUTPAYMENTRESPONSE);
		}
		DoExpressCheckoutPaymentResponse response = (DoExpressCheckoutPaymentResponse)pPaymentResponse;	
		super.buildFaultResponse(response);			
		response.setTimeStamp(String.valueOf(getFaultResponseType().getCreateTimestamp()));			
	}

	
	
	/**
	 * <>This method will process the failure response.
	 *
	 * @param pPaymentResponse the payment response
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {			
		if (!(pPaymentResponse instanceof DoExpressCheckoutPaymentResponse)) {
			throw new UnsupportedOperationException(IRadialConstants.PPAYMENTRESPONSE_NOT_INSTANCE_DOEXPRESSCHECKOUTPAYMENTRESPONSE);
		}
		DoExpressCheckoutPaymentResponse response = (DoExpressCheckoutPaymentResponse)pPaymentResponse;
		super.buildErrorResponse(pPaymentResponse,getDoExpressCheckoutReplyType());
		response.setTimeStamp(getTimeStamp());	
	}
	

	
	
	/**
	 * Inits the get express checkout.
	 */
	private void initDoExpressCheckout(){
		mDoExpressCheckoutRequestType = createPayPalDoExpressCheckoutRequest();
	}
	
	
	/**
	 * Creates the pay pal do express checkout request.
	 *
	 * @return the pay pal do express checkout request type
	 */
	private PayPalDoExpressCheckoutRequestType createPayPalDoExpressCheckoutRequest() {

		return getRadialObjectFactory().createPayPalDoExpressCheckoutRequestType();
	}


	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<PayPalDoExpressCheckoutRequestType> getRequestObject() {
		return mRequestObject;
	}


	/**
	 * Sets the request object.
	 *
	 * @param pRequestType the new request object
	 */
	public void setRequestObject(
			JAXBElement<PayPalDoExpressCheckoutRequestType> pRequestType) {
		this.mRequestObject = pRequestType;
	}

	
	

	
	/**
	 * Gets the do express checkout reply type.
	 *
	 * @return the do express checkout reply type
	 */
	public PayPalDoExpressCheckoutReplyType getDoExpressCheckoutReplyType() {
		return mDoExpressCheckoutReplyType;
	}

	
	/**
	 * Sets the do express checkout reply type.
	 *
	 * @param pDoExpressCheckoutReplyType the new do express checkout reply type
	 */
	public void setDoExpressCheckoutReplyType(
			PayPalDoExpressCheckoutReplyType pDoExpressCheckoutReplyType) {
		this.mDoExpressCheckoutReplyType = pDoExpressCheckoutReplyType;
	}

	
}
