package com.tru.radial.payment.promo.bean;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class PromoFinanceRequest for capture required i/p params for promo request..
 */
public class PromoFinanceRequest implements IRadialRequest {

	/** The mRetryCount. */
	private int mRetryCount;
	
	/** The mCvvNum. */
	private String mCvvNum;
	
	/** The m expiration month. */
	private String mExpirationMonth;
	
	/** The m expiration year. */
	private String mExpirationYear;
	
	/** Property to hold the  merchant ref number. */
	private String mMerchantRefNumber;
	
	/** The m credit card number. */
	private String mCreditCardNumber;
	
	/** Property to hold the  transaction id. */
	private String mTransactionId;
	
	/** The m amount. */
	private double mAmount;
	
	/** The m billing address. */
	private Address mBillingAddress;
	
	/** Property to hold the  email. */
	private String mEmail;

	/** The m unique id. */
	private Long mUniqueId;
	
	/** The m true client ip address. */
	private String mTrueClientIpAddress;
	
	/** The m schema version. */
	private String mSchemaVersion;

	/**
	 * This method is used to get retryCount.
	 * @return retryCount String
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 * This method is used to set retryCount.
	 *
	 * @param pRetryCount the new retry count
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/**
	 * This method is used to get cvvNum.
	 * @return cvvNum String
	 */
	public String getCvvNum() {
		return mCvvNum;
	}

	/**
	 * This method is used to set cvvNum.
	 *
	 * @param pCvvNum the new cvv num
	 */
	public void setCvvNum(String pCvvNum) {
		mCvvNum = pCvvNum;
	}

	/**
	 * This method is used to get expirationMonth.
	 * @return expirationMonth String
	 */
	public String getExpirationMonth() {
		return mExpirationMonth;
	}

	/**
	 * This method is used to set expirationMonth.
	 *
	 * @param pExpirationMonth the new expiration month
	 */
	public void setExpirationMonth(String pExpirationMonth) {
		mExpirationMonth = pExpirationMonth;
	}

	/**
	 * This method is used to get expirationYear.
	 * @return expirationYear String
	 */
	public String getExpirationYear() {
		return mExpirationYear;
	}

	/**
	 * This method is used to set expirationYear.
	 *
	 * @param pExpirationYear the new expiration year
	 */
	public void setExpirationYear(String pExpirationYear) {
		mExpirationYear = pExpirationYear;
	}

	/**
	 * This method is used to get merchantRefNumber.
	 * @return merchantRefNumber String
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}

	/**
	 * This method is used to set merchantRefNumber.
	 *
	 * @param pMerchantRefNumber the new merchant ref number
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}

	/**
	 * This method is used to get creditCardNumber.
	 * @return creditCardNumber String
	 */
	public String getCreditCardNumber() {
		return mCreditCardNumber;
	}

	/**
	 * This method is used to set creditCardNumber.
	 *
	 * @param pCreditCardNumber the new credit card number
	 */
	public void setCreditCardNumber(String pCreditCardNumber) {
		mCreditCardNumber = pCreditCardNumber;
	}

	/**
	 * This method is used to get transactionId.
	 * @return transactionId String
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * This method is used to set transactionId.
	 *
	 * @param pTransactionId the new transaction id
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * This method is used to get amount.
	 * @return amount String
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * This method is used to set amount.
	 *
	 * @param pAmount the new amount
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}

	/**
	 * This method is used to get billingAddress.
	 * @return billingAddress String
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}

	/**
	 * This method is used to set billingAddress.
	 *
	 * @param pBillingAddress the new billing address
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}

	/**
	 * This method is used to get email.
	 * @return email String
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * This method is used to set email.
	 *
	 * @param pEmail the new email
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * This method is used to get uniqueId.
	 * @return uniqueId String
	 */
	public Long getUniqueId() {
		return mUniqueId;
	}

	/**
	 * This method is used to set uniqueId.
	 *
	 * @param pUniqueId the new unique id
	 */
	public void setUniqueId(Long pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**
	 * This method is used to get trueClientIpAddress.
	 * @return trueClientIpAddress String
	 */
	public String getTrueClientIpAddress() {
		return mTrueClientIpAddress;
	}

	/**
	 * This method is used to set trueClientIpAddress.
	 *
	 * @param pTrueClientIpAddress the new true client ip address
	 */
	public void setTrueClientIpAddress(String pTrueClientIpAddress) {
		mTrueClientIpAddress = pTrueClientIpAddress;
	}

	/**
	 * This method is used to get schemaVersion.
	 * @return schemaVersion String
	 */
	public String getSchemaVersion() {
		return mSchemaVersion;
	}

	/**
	 * This method is used to set schemaVersion.
	 *
	 * @param pSchemaVersion the new schema version
	 */
	public void setSchemaVersion(String pSchemaVersion) {
		mSchemaVersion = pSchemaVersion;
	}
	
	
	
}
