package com.tru.radial.payment.promo.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * The Class PromoFinanceResponse.
 */
public class PromoFinanceResponse extends AbstractRadialResponse implements IRadialResponse {
	
	/** The mTermsAndConditionText. */
	private String mTermsAndConditionText ;
	
	/** The mPromoFinanceRate. */
	private String mPromoFinanceRate ;

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;
		
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setErrors(java.util.List)
	 */

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess = pValue;
		
	}
	

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.IRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = pTmeoutReponse;
	}

	/**
	 * This method is used to get termsAndConditionText.
	 * @return termsAndConditionText String
	 */
	public String getTermsAndConditionText() {
		return mTermsAndConditionText;
	}

	/**
	 * This method is used to set termsAndConditionText.
	 *
	 * @param pTermsAndConditionText the new terms and condition text
	 */
	public void setTermsAndConditionText(String pTermsAndConditionText) {
		mTermsAndConditionText = pTermsAndConditionText;
	}

	/**
	 * This method is used to get promoFinanceRate.
	 * @return promoFinanceRate String
	 */
	public String getPromoFinanceRate() {
		return mPromoFinanceRate;
	}

	/**
	 * This method is used to set promoFinanceRate.
	 *
	 * @param pPromoFinanceRate the new promo finance rate
	 */
	public void setPromoFinanceRate(String pPromoFinanceRate) {
		mPromoFinanceRate = pPromoFinanceRate;
	}

	
}
