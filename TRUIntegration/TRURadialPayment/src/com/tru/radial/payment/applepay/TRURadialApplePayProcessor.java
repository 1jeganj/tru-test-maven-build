/**
 * 
 */
package com.tru.radial.payment.applepay;

import java.util.Calendar;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.exception.TRURadialResponseBuilderException;
import com.tru.radial.payment.CreditCardAuthReplyType;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.applepay.bean.TRURadialApplePayResponse;
import com.tru.radial.payment.applepay.request.ApplePayDWRequestBuilder;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;
import com.tru.radial.validation.TRURadialXSDValidator;

/**
 * @author PA
 * The Class TRURadialApplePayProcessor.
 */
public class TRURadialApplePayProcessor extends TRURadialBaseProcessor {


	//private  static final String  PAYMENT_APPLE_PAY_DW = "applepayDW";

	/** To manage xsd validation. */
	private boolean mXsdValidationRequired = true;
	
	/** The m page name. */
	private String mPageName;

	/**
	 * This method is used to invoke setExpressCheckout call of PayPal API.
	 *
	 * @param pPaymentRequest            of type PayPalRequest
	 * @return PayPalResponse
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public IRadialResponse applepayAuthroization(IRadialRequest pPaymentRequest) throws TRURadialIntegrationException {
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRURadialApplePayProcessor.applepayAuthroization ::START");
		}
		boolean validateXMLWithXSD = false;
		//construct request
		IRadialResponse applepayresp = new TRURadialApplePayResponse();	
		ApplePayDWRequestBuilder applePayDWRequestBuilder = new ApplePayDWRequestBuilder();
		long starTime = 0;
		long endTime = 0;
		try {
			//Create the readial request
			applePayDWRequestBuilder.buildRequest(pPaymentRequest);
			//do transform to xml
			String xmlString = transformToXML(applePayDWRequestBuilder.getRequestObject(), IRadialConstants.PACKAGE_NAME);
			if(isLoggingDebug()){
				vlogDebug("{0}", applePayDWRequestBuilder.formatXML(xmlString));
			}
			//validate xml with xsd
			if (isXsdValidationRequired()) {
					validateXMLWithXSD = TRURadialXSDValidator.validateXMLWithXSD(xmlString, getXSDFilenames(IRadialConstants.APPLEPAYDWAUTH));
				}
			if((isLoggingDebug()) && (isXsdValidationRequired())){
				vlogDebug("XSD Validation for the applePayDWRequestBuilder Request is -> {0}",validateXMLWithXSD);
			}
			
			starTime = Calendar.getInstance().getTimeInMillis();
			//call service
			RadialGatewayResponse response = executeRequest(xmlString, IRadialConstants.APPLEPAYDWAUTH, getRadialconfig().getMaxReTry());	
			endTime = Calendar.getInstance().getTimeInMillis();
			if(isLoggingDebug()){			
				if(null != response){
					vlogDebug("{0}",applePayDWRequestBuilder.formatXML(response.getResponseBody()));
				}else{
					vlogDebug("RadialGatewayResponse is null for {0}",IRadialConstants.APPLEPAYDWAUTH);
				}			
			}
			if (response == null) {
				((TRURadialApplePayResponse)applepayresp).setSuccess(Boolean.FALSE);
				((TRURadialApplePayResponse)applepayresp).setTimeoutReponse(Boolean.TRUE);
				alertLogger(String.valueOf(endTime-starTime),IRadialConstants.RADIAL_TIMEOUT_RESPONSE_CODE,IRadialConstants.RADIAL_TIMEOUT_RESPONSE_MESSAGE);
			}else if(response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				applePayDWRequestBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				applePayDWRequestBuilder.buildFaultResponse(applepayresp);
				String responseCode = null;
				String errorMessage = null;
				if (applepayresp != null && applepayresp.getRadialError() != null) {
					responseCode = applepayresp.getRadialError().getErrorCode();
					errorMessage = applepayresp.getRadialError().getErrorMessage();
				}
				alertLogger(String.valueOf(endTime-starTime),responseCode,errorMessage);
			} else if (!response.getResponseBody().isEmpty() ) {
				try {
					applePayDWRequestBuilder.setCreditCardAuthReplyType(transformToObject(response.getResponseBody(), CreditCardAuthReplyType.class));
					applePayDWRequestBuilder.buildResponse(applepayresp);
				} catch (TRURadialResponseBuilderException exe) {
					if (isLoggingError()) {
						logError("TRURadialResponseBuilderException in @Class::TRURadialApplePayProcessor::@method::applepayAuthroization", exe);
						endTime = Calendar.getInstance().getTimeInMillis();
						String errorMessage = null;
						if (exe.getCause() != null) {
							errorMessage = exe.getCause().getMessage();
						}
						alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),errorMessage);
					}
				}
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			alertLogger(String.valueOf(endTime-starTime),getExceptionErrorCode(),exe.getErrorMessage());
			throw exe;
		}
		if (isLoggingDebug()) {
			vlogDebug("@Class::TRURadialApplePayProcessor.applepayAuthroization ::END");
		}
		return applepayresp;
	}



	/**
	 * @return the mXsdValidationRequired
	 */
	public boolean isXsdValidationRequired() {
		return mXsdValidationRequired;
	}



	/**
	 * Sets the xsd validation required.
	 *
	 * @param pXsdValidationRequired the new xsd validation required
	 */
	public void setXsdValidationRequired(boolean pXsdValidationRequired) {
		this.mXsdValidationRequired = pXsdValidationRequired;
	}
	
	/**
	 * This method logs the alerts in case of down scenarios.
	 *
	 * @param pTimeTaken - String
	 * @param pFailureKey the failure key
	 * @param pFailureReason the failure reason
	 */
	public void alertLogger(String pTimeTaken,String pFailureKey, String pFailureReason){
		if (isLoggingDebug()) {
			logDebug("TRURadialApplePayProcessor  (alertLogger) Start");
		}
		super.alertLogger(IRadialConstants.STR_APPLE_PAY_PROCESSOR, IRadialConstants.APPLE_PAY_AUTHROIZATION, IRadialConstants.APPLEPAYDWAUTH, pTimeTaken, pFailureKey, pFailureReason,getPageName());
		if (isLoggingDebug()) {
			logDebug("TRURadialApplePayProcessor  (alertLogger) End");
		}
	}

	/**
	 * @return the mPageName
	 */
	public String getPageName() {
		return mPageName;
	}

	/**
	 * @param pPageName the mPageName to set
	 */
	public void setPageName(String pPageName) {
		mPageName = pPageName;
	}
}


