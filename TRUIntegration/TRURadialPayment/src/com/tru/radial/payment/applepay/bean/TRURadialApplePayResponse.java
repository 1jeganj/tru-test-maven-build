/**
 * 
 */
package com.tru.radial.payment.applepay.bean;

import java.math.BigDecimal;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * @author Siva, Gopishetti {eclipse.ini}
 *
 */
public class TRURadialApplePayResponse extends AbstractRadialResponse implements IRadialResponse {

	
	private String mOrderId;
	private String mPaymentAccountUniqueId;
	private boolean mIsToken;
	private String mResponseCode;
	private String mAuthorizationResponseCode;
	private String mBankAuthorizationCode;
	private String mCVV2ResponseCode;
	private String mAVSResponseCode;
	private BigDecimal mAmountAuthorized;
	private String mTenderType;
	private boolean mSuccess;
	private boolean mTimeoutReponse;
	

	@Override
	public void setSuccess(boolean pValue) {
		mSuccess = pValue;
		
	}

	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		mTimeoutReponse = pTmeoutReponse;
		
	}

	/**
	 * @return the mOrderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * Sets the order id.
	 *
	 * @param pOrderId the new order id
	 */
	public void setOrderId(String pOrderId) {
		this.mOrderId = pOrderId;
	}

	/**
	 * @return the mPaymentAccountUniqueId
	 */
	public String getPaymentAccountUniqueId() {
		return mPaymentAccountUniqueId;
	}

	/**
	 * Sets the payment account unique id.
	 *
	 * @param pPaymentAccountUniqueId the new payment account unique id
	 */
	public void setPaymentAccountUniqueId(String pPaymentAccountUniqueId) {
		this.mPaymentAccountUniqueId = pPaymentAccountUniqueId;
	}

	/**
	 * @return the mResponseCode
	 */
	public String getResponseCode() {
		return mResponseCode;
	}

	/**
	 * Sets the response code.
	 *
	 * @param pResponseCode the new response code
	 */
	public void setResponseCode(String pResponseCode) {
		this.mResponseCode = pResponseCode;
	}

	/**
	 * @return the mAuthorizationResponseCode
	 */
	public String getAuthorizationResponseCode() {
		return mAuthorizationResponseCode;
	}

	/**
	 * Sets the authorization response code.
	 *
	 * @param pAuthorizationResponseCode the new authorization response code
	 */
	public void setAuthorizationResponseCode(String pAuthorizationResponseCode) {
		this.mAuthorizationResponseCode = pAuthorizationResponseCode;
	}

	/**
	 * @return the mBankAuthorizationCode
	 */
	public String getBankAuthorizationCode() {
		return mBankAuthorizationCode;
	}

	/**
	 * Sets the bank authorization code.
	 *
	 * @param pBankAuthorizationCode the new bank authorization code
	 */
	public void setBankAuthorizationCode(String pBankAuthorizationCode) {
		this.mBankAuthorizationCode = pBankAuthorizationCode;
	}

	/**
	 * @return the mCVV2ResponseCode
	 */
	public String getCVV2ResponseCode() {
		return mCVV2ResponseCode;
	}

	/**
	 * Sets the CVV 2 response code.
	 *
	 * @param pCVV2ResponseCode the new CVV 2 response code
	 */
	public void setCVV2ResponseCode(String pCVV2ResponseCode) {
		this.mCVV2ResponseCode = pCVV2ResponseCode;
	}

	/**
	 * @return the mAVSResponseCode
	 */
	public String getAVSResponseCode() {
		return mAVSResponseCode;
	}

	/**
	 * Sets the AVS response code.
	 *
	 * @param pAVSResponseCode the new AVS response code
	 */
	public void setAVSResponseCode(String pAVSResponseCode) {
		this.mAVSResponseCode = pAVSResponseCode;
	}

	/**
	 * @return the mAmountAuthorized
	 */
	public BigDecimal getAmountAuthorized() {
		return mAmountAuthorized;
	}

	/**
	 * Sets the amount authorized.
	 *
	 * @param pAmountAuthorized the new amount authorized
	 */
	public void setAmountAuthorized(BigDecimal pAmountAuthorized) {
		this.mAmountAuthorized = pAmountAuthorized;
	}

	/**
	 * @return the mTenderType
	 */
	public String getTenderType() {
		return mTenderType;
	}

	/**
	 * Sets the tender type.
	 *
	 * @param pTenderType the new tender type
	 */
	public void setTenderType(String pTenderType) {
		this.mTenderType = pTenderType;
	}

	/**
	 * @return the mSuccess
	 */
	public boolean isSuccess() {
		return mSuccess;
	}

	/**
	 * @return the mTimeoutReponse
	 */
	public boolean isTimeoutReponse() {
		return mTimeoutReponse;
	}

	/**
	 * @return the mIsToken
	 */
	public boolean isIsToken() {
		return mIsToken;
	}

	/**
	 * @param pIsToken the mIsToken to set
	 */
	public void setIsToken(boolean pIsToken) {
		this.mIsToken = pIsToken;
	}
	
}
