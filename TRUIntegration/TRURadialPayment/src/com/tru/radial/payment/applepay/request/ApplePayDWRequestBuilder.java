/**
 * 
 */
package com.tru.radial.payment.applepay.request;

import javax.xml.bind.JAXBElement;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.exception.TRURadialResponseBuilderException;
import com.tru.radial.payment.CreditCardAuthReplyType;
import com.tru.radial.payment.CreditCardAuthRequestType;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.payment.PhysicalAddressType;
import com.tru.radial.payment.WalletPaymentInformationType;
import com.tru.radial.payment.applepay.bean.TRURadialApplePayRequest;
import com.tru.radial.payment.applepay.bean.TRURadialApplePayResponse;

/**
 * @author PA
 * The Class ApplePayDWRequestBuilder.
 */
public class ApplePayDWRequestBuilder extends AbstractRequestBuilder {
	
	/** The m set apple pay DW  request type. */
	private CreditCardAuthRequestType mCreditCardAuthRequestType = null;
	
	/** The m set apple pay DW reply type. */
	private CreditCardAuthReplyType mCreditCardAuthReplyType = null;
	
	/** The m fault response type. */
	private FaultResponseType mFaultResponseType = null;

	/** The m request object. */
	private JAXBElement<CreditCardAuthRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new pay pal set express request builder.
	 */
	public ApplePayDWRequestBuilder(){ 		
		setCreditCardAuthRequestType(getRadialObjectFactory().createCreditCardAuthRequestType());
	}

	/**
	 * Builds the request.
	 *
	 * @param pPaymentRequest the payment request
	 * @throws TRURadialRequestBuilderException the TRU radial request builder exception
	 * @Override 
	 */
	public void buildRequest(IRadialRequest pPaymentRequest) throws TRURadialRequestBuilderException {
		TRURadialApplePayRequest applepayAuthReq = (TRURadialApplePayRequest)pPaymentRequest;
		CreditCardAuthRequestType creditCardAuthRequest = getRadialObjectFactory().createCreditCardAuthRequestType();
		WalletPaymentInformationType walletPaymentInformation= getRadialObjectFactory().createWalletPaymentInformationType();
		creditCardAuthRequest.setRequestId(applepayAuthReq.getRequestId());
		walletPaymentInformation.setApplePayTransactionId(applepayAuthReq.getApplePayTransactionId());
		walletPaymentInformation.setData(applepayAuthReq.getData());
		walletPaymentInformation.setEphemeralPublicKey(applepayAuthReq.getEphemeralPublicKey());
		walletPaymentInformation.setPublicKeyHash(applepayAuthReq.getPublicKeyHash());
		walletPaymentInformation.setSignature(applepayAuthReq.getSignature());
		walletPaymentInformation.setVersion(applepayAuthReq.getVersion());
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeWalletPaymentInformation(walletPaymentInformation));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeOrderId(applepayAuthReq.getOrderId()));		
		PhysicalAddressType billingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		billingPhysicalAddress.setLine1(applepayAuthReq.getBillingAddressLine1());
		billingPhysicalAddress.setLine2(applepayAuthReq.getBillingAddressLine1());
		billingPhysicalAddress.setLine3(applepayAuthReq.getBillingAddressLine1());
		billingPhysicalAddress.setLine4(applepayAuthReq.getBillingAddressLine1());
		billingPhysicalAddress.setCity(applepayAuthReq.getBillingAddressCity());
		billingPhysicalAddress.setMainDivision(applepayAuthReq.getBillingAddressState());
		billingPhysicalAddress.setCountryCode(applepayAuthReq.getBillingAddressCountryCode());
		billingPhysicalAddress.setPostalCode(applepayAuthReq.getBillingAddressPostalCode());
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeBillingFirstName(applepayAuthReq.getBillingFirstName()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeBillingLastName(applepayAuthReq.getBillingLastName()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeBillingPhoneNo(applepayAuthReq.getBillingPhoneNo()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeBillingAddress(billingPhysicalAddress));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeCustomerEmail(applepayAuthReq.getCustomerEmail()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeCustomerIPAddress(applepayAuthReq.getCustomerIPAddress()));	
		PhysicalAddressType shippingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		shippingPhysicalAddress.setLine1(applepayAuthReq.getShippingAddressLine1());
		shippingPhysicalAddress.setLine2(applepayAuthReq.getShippingAddressLine1());
		shippingPhysicalAddress.setLine3(applepayAuthReq.getShippingAddressLine1());
		shippingPhysicalAddress.setLine4(applepayAuthReq.getShippingAddressLine1());
		shippingPhysicalAddress.setCity(applepayAuthReq.getShippingCity());
		shippingPhysicalAddress.setMainDivision(applepayAuthReq.getShippingState());
		shippingPhysicalAddress.setCountryCode(applepayAuthReq.getShippingCountryCode());
		shippingPhysicalAddress.setPostalCode(applepayAuthReq.getShippingPostalCode());
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeShipToFirstName(applepayAuthReq.getShippingFirstName()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeShipToLastName(applepayAuthReq.getShippingLastName()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeShipToPhoneNo(applepayAuthReq.getShippingPhoneNo()));
		creditCardAuthRequest.getContent().add(getRadialObjectFactory().
				createCreditCardAuthRequestTypeShippingAddress(billingPhysicalAddress));
		setRequestObject(getRadialObjectFactory().createCreditCardAuthRequest(creditCardAuthRequest));
	}

	/**
	 * Builds the response.
	 *
	 * @param pPaymentResponse the payment response
	 * @throws TRURadialResponseBuilderException the TRU radial response builder exception
	 * @Override 
	 */
	public void buildResponse(IRadialResponse pPaymentResponse)
			throws TRURadialResponseBuilderException {
		if (!(pPaymentResponse instanceof TRURadialApplePayResponse)) {
			throw new UnsupportedOperationException();
		}
		TRURadialApplePayResponse response = (TRURadialApplePayResponse)pPaymentResponse;
		if(response != null){
			response.setOrderId(getCreditCardAuthReplyType().getPaymentContext().getOrderId());
			response.setPaymentAccountUniqueId(getCreditCardAuthReplyType().getPaymentContext().getPaymentAccountUniqueId().getValue());
			response.setIsToken(getCreditCardAuthReplyType().getPaymentContext().getPaymentAccountUniqueId().isIsToken());
			response.setResponseCode(getCreditCardAuthReplyType().getResponseCode());
			response.setAuthorizationResponseCode(getCreditCardAuthReplyType().getAuthorizationResponseCode());
			response.setBankAuthorizationCode(getCreditCardAuthReplyType().getBankAuthorizationCode());
			response.setCVV2ResponseCode(getCreditCardAuthReplyType().getCVV2ResponseCode());
			response.setAVSResponseCode(getCreditCardAuthReplyType().getAVSResponseCode());
			response.setAmountAuthorized(getCreditCardAuthReplyType().getAmountAuthorized().getValue());
			response.setTenderType((getCreditCardAuthReplyType().getTenderType()));
			//response.setSuccess(Boolean.TRUE);		
			response.setTimeStamp(getTimeStamp());	
		}
	}
	
	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<CreditCardAuthRequestType> getRequestObject() {
		return mRequestObject;
	}


	/**
	 * Sets the request object.
	 *
	 * @param pRequestType the new request object
	 */
	public void setRequestObject(
			JAXBElement<CreditCardAuthRequestType> pRequestType) {
		this.mRequestObject = pRequestType;
	}



	
	/**
	 * Gets the fault response type.
	 *
	 * @return the fault response type
	 */
	public FaultResponseType getFaultResponseType() {
		return mFaultResponseType;
	}

	/**
	 * Sets the fault response type.
	 *
	 * @param pFaultResponseType the new fault response type
	 */
	public void setFaultResponseType(FaultResponseType pFaultResponseType) {
		this.mFaultResponseType = pFaultResponseType;
	}

	/**
	 * @return the mCreditCardAuthRequestType
	 */
	public CreditCardAuthRequestType getCreditCardAuthRequestType() {
		return mCreditCardAuthRequestType;
	}

	/**
	 * Sets the credit card auth request type.
	 *
	 * @param pCreditCardAuthRequestType the new credit card auth request type
	 */
	public void setCreditCardAuthRequestType(CreditCardAuthRequestType pCreditCardAuthRequestType) {
		this.mCreditCardAuthRequestType = pCreditCardAuthRequestType;
	}

	/**
	 * @return the mCreditCardAuthReplyType
	 */
	public CreditCardAuthReplyType getCreditCardAuthReplyType() {
		return mCreditCardAuthReplyType;
	}

	/**
	 * Sets the credit card auth reply type.
	 *
	 * @param pCreditCardAuthReplyType the new credit card auth reply type
	 */
	public void setCreditCardAuthReplyType(CreditCardAuthReplyType pCreditCardAuthReplyType) {
		this.mCreditCardAuthReplyType = pCreditCardAuthReplyType;
	}


}
