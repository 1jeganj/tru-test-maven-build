package com.tru.radial.payment.creditcard.bean;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class CreditCardAuthorizationRequest.
 */
public class CreditCardAuthorizationRequest implements IRadialRequest {
	
	/** The mRetryCount. */
	private int mRetryCount;
	
	/** The mCvvNum. */
	private String mCvvNum;
	
	/** The m expiration month. */
	private String mExpirationMonth;
	
	/** The m expiration year. */
	private String mExpirationYear;
	
	/** Property to hold the  merchant ref number. */
	private String mMerchantRefNumber;
	
	/** The m credit card number. */
	private String mCreditCardNumber;
	
	/** Property to hold the  transaction id. */
	private String mTransactionId;
	
	/** The m amount. */
	private double mAmount;
	
	/** The m billing address. */
	private Address mBillingAddress;
	
	/** Property to hold the  email. */
	private String mEmail;

	/** The m unique id. */
	private Long mUniqueId;
	
	/** The m true client ip address. */
	private String mTrueClientIpAddress;
	
	/** The m schema version. */
	private String mSchemaVersion;
	
	/** The m ship to first name. */
	private String mShipToFirstName;
	
	/** The m ship to last name. */
	private String mShipToLastName ;
	
	/** The m ship to phone. */
	private String mShipToPhone ;
	
	/** The m ship address 1. */
	private String mShipAddress1;
	
	/** The m ship address 2. */
	private String mShipAddress2;
	
	/** The m ship city. */
	private String mShipCity;
	
	/** The m ship country. */
	private String mShipCountry;
	
	/** The m ship state. */
	private String mShipState;
	
	/** The m ship postal code. */
	private String mShipPostalCode;
	
	/**
	 * Gets the merchant ref number.
	 *
	 * @return the mMerchantRefNumber
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}

	/**
	 * Sets the merchant ref number.
	 *
	 * @param pMerchantRefNumber the new merchant ref number
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}

	/**
	 * Gets the credit card number.
	 *
	 * @return the mCreditCardNumber
	 */
	public String getCreditCardNumber() {
		return mCreditCardNumber;
	}

	/**
	 * Sets the credit card number.
	 *
	 * @param pCreditCardNumber the new credit card number
	 */
	public void setCreditCardNumber(String pCreditCardNumber) {
		mCreditCardNumber = pCreditCardNumber;
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the mTransactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId the new transaction id
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the mAmount
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param pAmount the new amount
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}

	/**
	 * Gets the billing address.
	 *
	 * @return the mBillingAddress
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}

	/**
	 * Sets the billing address.
	 *
	 * @param pBillingAddress the new billing address
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}

	/**
	 * Gets the expiration month.
	 *
	 * @return the mExpirationMonth
	 */
	public String getExpirationMonth() {
		return mExpirationMonth;
	}

	/**
	 * Sets the expiration month.
	 *
	 * @param pExpirationMonth the new expiration month
	 */
	public void setExpirationMonth(String pExpirationMonth) {
		mExpirationMonth = pExpirationMonth;
	}

	/**
	 * Gets the expiration year.
	 *
	 * @return the mExpirationYear
	 */
	public String getExpirationYear() {
		return mExpirationYear;
	}

	/**
	 * Sets the expiration year.
	 *
	 * @param pExpirationYear the new expiration year
	 */
	public void setExpirationYear(String pExpirationYear) {
		mExpirationYear = pExpirationYear;
	}

	/**
	 * Gets the email.
	 *
	 * @return the mEmail
	 */
	public String getEmail() {
		return mEmail;
	}

	/**
	 * Sets the email.
	 *
	 * @param pEmail the new email
	 */
	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	/**
	 * Gets the unique id.
	 *
	 * @return the mUniqueId
	 */
	public Long getUniqueId() {
		return mUniqueId;
	}

	/**
	 * Sets the unique id.
	 *
	 * @param pUniqueId the new unique id
	 */
	public void setUniqueId(Long pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**
	 * Gets the true client ip address.
	 *
	 * @return the mTrueClientIpAddress
	 */
	public String getTrueClientIpAddress() {
		return mTrueClientIpAddress;
	}

	/**
	 * Sets the true client ip address.
	 *
	 * @param pTrueClientIpAddress the new true client ip address
	 */
	public void setTrueClientIpAddress(String pTrueClientIpAddress) {
		mTrueClientIpAddress = pTrueClientIpAddress;
	}

	/**
	 * Gets the schema version.
	 *
	 * @return the mSchemaVersion
	 */
	public String getSchemaVersion() {
		return mSchemaVersion;
	}

	/**
	 * Sets the schema version.
	 *
	 * @param pSchemaVersion the new schema version
	 */
	public void setSchemaVersion(String pSchemaVersion) {
		mSchemaVersion = pSchemaVersion;
	}

	/**
	 * Gets the ship to first name.
	 *
	 * @return the shipToFirstName
	 */
	public String getShipToFirstName() {
		return mShipToFirstName;
	}

	/**
	 * Sets the ship to first name.
	 *
	 * @param pShipToFirstName the shipToFirstName to set
	 */
	public void setShipToFirstName(String pShipToFirstName) {
		mShipToFirstName = pShipToFirstName;
	}

	/**
	 * Gets the ship to last name.
	 *
	 * @return the shipToLastName
	 */
	public String getShipToLastName() {
		return mShipToLastName;
	}

	/**
	 * Sets the ship to last name.
	 *
	 * @param pShipToLastName the shipToLastName to set
	 */
	public void setShipToLastName(String pShipToLastName) {
		mShipToLastName = pShipToLastName;
	}

	/**
	 * Gets the ship to phone.
	 *
	 * @return the shipToPhone
	 */
	public String getShipToPhone() {
		return mShipToPhone;
	}

	/**
	 * Sets the ship to phone.
	 *
	 * @param pShipToPhone the shipToPhone to set
	 */
	public void setShipToPhone(String pShipToPhone) {
		mShipToPhone = pShipToPhone;
	}

	/**
	 * Gets the ship address 1.
	 *
	 * @return the shipAddress1
	 */
	public String getShipAddress1() {
		return mShipAddress1;
	}

	/**
	 * Sets the ship address 1.
	 *
	 * @param pShipAddress1 the shipAddress1 to set
	 */
	public void setShipAddress1(String pShipAddress1) {
		mShipAddress1 = pShipAddress1;
	}

	/**
	 * Gets the ship address 2.
	 *
	 * @return the shipAddress2
	 */
	public String getShipAddress2() {
		return mShipAddress2;
	}

	/**
	 * Sets the ship address 2.
	 *
	 * @param pShipAddress2 the shipAddress2 to set
	 */
	public void setShipAddress2(String pShipAddress2) {
		mShipAddress2 = pShipAddress2;
	}

	/**
	 * Gets the ship city.
	 *
	 * @return the shipCity
	 */
	public String getShipCity() {
		return mShipCity;
	}

	/**
	 * Sets the ship city.
	 *
	 * @param pShipCity the shipCity to set
	 */
	public void setShipCity(String pShipCity) {
		mShipCity = pShipCity;
	}

	/**
	 * Gets the ship state.
	 *
	 * @return the shipState
	 */
	public String getShipState() {
		return mShipState;
	}

	/**
	 * Sets the ship state.
	 *
	 * @param pShipState the shipState to set
	 */
	public void setShipState(String pShipState) {
		mShipState = pShipState;
	}

	/**
	 * Gets the ship country.
	 *
	 * @return the shipCountry
	 */
	public String getShipCountry() {
		return mShipCountry;
	}

	/**
	 * Sets the ship country.
	 *
	 * @param pShipCountry the shipCountry to set
	 */
	public void setShipCountry(String pShipCountry) {
		mShipCountry = pShipCountry;
	}

	/**
	 * Gets the ship postal code.
	 *
	 * @return the shipPostalCode
	 */
	public String getShipPostalCode() {
		return mShipPostalCode;
	}

	/**
	 * Sets the ship postal code.
	 *
	 * @param pShipPostalCode the shipPostalCode to set
	 */
	public void setShipPostalCode(String pShipPostalCode) {
		mShipPostalCode = pShipPostalCode;
	}

	/**
	 * This method is used to get cvvNum.
	 * @return mCvvNum String
	 */
	public String getCvvNum() {
		return mCvvNum;
	}

	/**
	 *This method is used to set cvvNum.
	 *@param pCvvNum String
	 */
	public void setCvvNum(String pCvvNum) {
		mCvvNum = pCvvNum;
	}

	/**
	 * This method is used to get retryCount.
	 * @return mRetryCount int
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 *This method is used to set retryCount.
	 *@param pRetryCount int
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}
	
	/** The Request to correct cvv or avs error. */
	private boolean mRequestToCorrectCVVOrAVSError;

	/**
	 * @return the requestToCorrectCVVOrAVSError
	 */
	public boolean isRequestToCorrectCVVOrAVSError() {
		return mRequestToCorrectCVVOrAVSError;
	}

	/**
	 * @param pRequestToCorrectCVVOrAVSError the requestToCorrectCVVOrAVSError to set
	 */
	public void setRequestToCorrectCVVOrAVSError(
			boolean pRequestToCorrectCVVOrAVSError) {
		mRequestToCorrectCVVOrAVSError = pRequestToCorrectCVVOrAVSError;
	}

}
