package com.tru.radial.payment.creditcard.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * The Class CreditCardValidationResponse.
 */
public class CreditCardValidationResponse extends AbstractRadialResponse implements IRadialResponse {
	
	/** The m credit card token. */
	private String mCreditCardToken;

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess =pValue;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = pTmeoutReponse;
	}

	/**
	 * Gets the credit card token.
	 *
	 * @return the credit card token
	 */
	public String getCreditCardToken() {
		return mCreditCardToken;
	}

	/**
	 * Sets the credit card token.
	 *
	 * @param pCreditCardToken the new credit card token
	 */
	public void setCreditCardToken(String pCreditCardToken) {
		mCreditCardToken = pCreditCardToken;
	}

}
