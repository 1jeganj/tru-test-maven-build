/**
 * 
 */
package com.tru.radial.payment.creditcard.bean;

import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.json.JSONObject;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.exception.TRURadialResponseBuilderException;

/**
 * @author PA
 *
 */
public class TRURadialGetNonceBuilder extends AbstractRequestBuilder {
	
	/** The m radial get nonce response. */
	private String mRadialGetNonceResponse;
	
	/**
	 * @return the radialGetNonceResponse
	 */
	public String getRadialGetNonceResponse() {
		return mRadialGetNonceResponse;
	}

	/**
	 * @param pRadialGetNonceResponse the radialGetNonceResponse to set
	 */
	public void setRadialGetNonceResponse(String pRadialGetNonceResponse) {
		mRadialGetNonceResponse = pRadialGetNonceResponse;
	}

	/**
	 * Builds the request.
	 *
	 * @param pPaymentRequest the payment request
	 * @throws TRURadialRequestBuilderException the tRU radial request builder exception
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest)
			throws TRURadialRequestBuilderException {
	}

	/**
	 * Builds the response.
	 *
	 * @param pPaymentResponse the payment response
	 * @throws TRURadialResponseBuilderException the tRU radial response builder exception
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse)
			throws TRURadialResponseBuilderException {
		TRURadialGetNonceResponse paymentResponse = (TRURadialGetNonceResponse)pPaymentResponse;
		JSONObject json = null;
		if(mRadialGetNonceResponse == null) {
			paymentResponse.setRadilalPaymentErrorKey(IRadialConstants.RADIAL_PAYMENT_ERROR_KEY);
			return;
		}
		try {
			if (StringUtils.isNotEmpty(mRadialGetNonceResponse)) {
				json = new JSONObject(mRadialGetNonceResponse);
				if(json.has(IRadialConstants.NONCE)){
					paymentResponse.setNonce(json.getString(IRadialConstants.NONCE));
				}else{
					paymentResponse.setRadilalPaymentErrorKey(IRadialConstants.RADIAL_PAYMENT_ERROR_KEY);
				}
				if(json.has(IRadialConstants.EXPIRES_IN_SECONDS) || json.has(IRadialConstants.JWT)){
					paymentResponse.setExpiresInSeconds(json.getString(IRadialConstants.EXPIRES_IN_SECONDS));
					paymentResponse.setJwt(json.getString(IRadialConstants.JWT));
				}
			}
		} catch (JSONException e) {
			LOGGER.logError("TRUPaymentIntegrationException at TRURadialPaymentProcessor/constructCreditCardValidationRequest method :{0}",e);
			throw new TRURadialRequestBuilderException(e);
		}
	}

}
