package com.tru.radial.payment.creditcard.builder;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PLCCAuthReplyType;
import com.tru.radial.payment.PLCCAuthRequestType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.PhysicalAddressType;
import com.tru.radial.payment.creditcard.bean.PLCCCardAuthorizationRequest;
import com.tru.radial.payment.creditcard.bean.PLCCCardAuthorizationResponse;

/**
 * The Class PLCCCardAuthorizationBuilder.
 */
public class PLCCCardAuthorizationBuilder extends AbstractRequestBuilder{

	/** The Constant LOGGER. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(PLCCCardAuthorizationBuilder.class);
	
	/** The m PLCC auth request type. */
	private PLCCAuthRequestType mPLCCAuthRequestType = null;

	/** The m PLCC auth reply type. */
	private PLCCAuthReplyType mPLCCAuthReplyType = null;
	
	/** The m request object. */
	private JAXBElement<PLCCAuthRequestType> mRequestObject = null;
	
	/**
	 * Instantiates a new PLCC card authorization builder.
	 */
	public PLCCCardAuthorizationBuilder() {
		mPLCCAuthRequestType = getRadialObjectFactory().createPLCCAuthRequestType();
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPLCCCardAuthorizationRequest) throws TRURadialRequestBuilderException {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildRequest) : Start");
		}
		PLCCCardAuthorizationRequest plccCardAuthorizationRequest = null;
		if (!(pPLCCCardAuthorizationRequest instanceof PLCCCardAuthorizationRequest)) {
			throw new UnsupportedOperationException();
		}
		plccCardAuthorizationRequest = (PLCCCardAuthorizationRequest) pPLCCCardAuthorizationRequest;
		
		PhysicalAddressType billingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();
		paymentContextType.setOrderId(plccCardAuthorizationRequest.getMerchantRefNumber());
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		if(StringUtils.isNumericOnly(plccCardAuthorizationRequest.getCreditCardNumber())){
			paymentAccountUniqueIdType.setIsToken(false);
		} else {
			paymentAccountUniqueIdType.setIsToken(true);
		}
		paymentAccountUniqueIdType.setValue(plccCardAuthorizationRequest.getCreditCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		mPLCCAuthRequestType.setRequestId(String.valueOf(plccCardAuthorizationRequest.getUniqueId()));
		//It is needed for OMS, so setting back to pass.
		plccCardAuthorizationRequest.setTransactionId((String.valueOf(plccCardAuthorizationRequest.getUniqueId())));
		
		mPLCCAuthRequestType.setPaymentContext(paymentContextType);
		
		DecimalFormat df = new DecimalFormat(TRURadialConstants.DOUBLE_DECIMAL);      

		BigDecimal orderAmount = new BigDecimal(df.format(plccCardAuthorizationRequest.getAmount()));
		AmountType amountType = getRadialObjectFactory().createAmountType();
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
		amountType.setValue(orderAmount);
		mPLCCAuthRequestType.setAmount(amountType);
		if(plccCardAuthorizationRequest.getCvvNumber() != null){
			mPLCCAuthRequestType.setCardSecurityCode(plccCardAuthorizationRequest.getCvvNumber());
		}
		mPLCCAuthRequestType.setBillingFirstName(plccCardAuthorizationRequest.getBillingAddress().getFirstName());
		mPLCCAuthRequestType.setBillingLastName(plccCardAuthorizationRequest.getBillingAddress().getLastName());
		
		billingPhysicalAddress.setLine1(plccCardAuthorizationRequest.getBillingAddress().getAddress1());
		String address2 = plccCardAuthorizationRequest.getBillingAddress().getAddress2();
		if(!StringUtils.isBlank(address2)){
			billingPhysicalAddress.setLine2(address2);
		}
		billingPhysicalAddress.setCity(plccCardAuthorizationRequest.getBillingAddress().getCity());
		billingPhysicalAddress.setMainDivision(plccCardAuthorizationRequest.getBillingAddress().getState());
		billingPhysicalAddress.setCountryCode(plccCardAuthorizationRequest.getBillingAddress().getCountry());
		billingPhysicalAddress.setPostalCode(plccCardAuthorizationRequest.getBillingAddress().getPostalCode());
		mPLCCAuthRequestType.setBillingAddress(billingPhysicalAddress);
		
		// START: Fix for GEOFUS-1294
		mPLCCAuthRequestType.setIsRequestToCorrectCVVOrAVSError(plccCardAuthorizationRequest.isRequestToCorrectCVVOrAVSError());

		/*if(plccCardAuthorizationRequest.getRetryCount() > IRadialConstants.INT_ZERO){
			mPLCCAuthRequestType.setIsRequestToCorrectCVVOrAVSError(true);
		}else{
			mPLCCAuthRequestType.setIsRequestToCorrectCVVOrAVSError(false);
		}*/
		// END: Fix for GEOFUS-1294
		
		setRequestObject(getRadialObjectFactory().createPLCCAuthRequest(mPLCCAuthRequestType));
		
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildRequest) : End");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildResponse) : Start");
		}
		PLCCCardAuthorizationResponse response = null;
		if (!(pPaymentResponse instanceof PLCCCardAuthorizationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (PLCCCardAuthorizationResponse)pPaymentResponse;
		if(IRadialConstants.CREDIT_CARD_AUTH_RESPONSE.equalsIgnoreCase(getPLCCAuthReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);		
			response.setAuthResponseCode(getPLCCAuthReplyType().getAuthorizationResponseCode());
			response.setBankAuthCode(getPLCCAuthReplyType().getBankAuthorizationCode());
			response.setCvv2Code(getPLCCAuthReplyType().getCVV2ResponseCode());
			response.setAvsCode(getPLCCAuthReplyType().getAVSResponseCode());
			response.setResponseCode(getPLCCAuthReplyType().getResponseCode());
			response.setAvsDescriptiveResult(getPLCCAuthReplyType().getAVSResponseCodeDesc());
		}else{
			response.setAuthResponseCode(getPLCCAuthReplyType().getAuthorizationResponseCode());
			response.setBankAuthCode(getPLCCAuthReplyType().getBankAuthorizationCode());
			response.setCvv2Code(getPLCCAuthReplyType().getCVV2ResponseCode());
			response.setAvsCode(getPLCCAuthReplyType().getAVSResponseCode());
			response.setResponseCode(getPLCCAuthReplyType().getResponseCode());
			response.setAvsDescriptiveResult(getPLCCAuthReplyType().getAVSResponseCodeDesc());
			buildErrorResponse(response);
		}	
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildResponse) : End");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildErrorResponse) : Start");
		}
		PLCCCardAuthorizationResponse response = null;
		if (!(pPaymentResponse instanceof PLCCCardAuthorizationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (PLCCCardAuthorizationResponse)pPaymentResponse;				
		super.buildErrorResponse(pPaymentResponse,getPLCCAuthReplyType());		
		response.setTimeStamp(getTimeStamp());
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PLCCCardAuthorizationBuilder  (buildErrorResponse) : End");
		}
	}

	/**
	 * @return the mPLCCAuthRequestType
	 */
	public PLCCAuthRequestType getPLCCAuthRequestType() {
		return mPLCCAuthRequestType;
	}

	/**
	 * Sets the PLCC auth request type.
	 *
	 * @param pPLCCAuthRequestType the new PLCC auth request type
	 */
	public void setPLCCAuthRequestType(PLCCAuthRequestType pPLCCAuthRequestType) {
		mPLCCAuthRequestType = pPLCCAuthRequestType;
	}

	/**
	 * Gets the PLCC auth reply type.
	 *
	 * @return the mPLCCAuthReplyType
	 */
	public PLCCAuthReplyType getPLCCAuthReplyType() {
		return mPLCCAuthReplyType;
	}

	/**
	 * Sets the PLCC auth reply type.
	 *
	 * @param pPLCCAuthReplyType the new PLCC auth reply type
	 */
	public void setPLCCAuthReplyType(PLCCAuthReplyType pPLCCAuthReplyType) {
		mPLCCAuthReplyType = pPLCCAuthReplyType;
	}

	/**
	 * @return the mRequestObject
	 */
	public JAXBElement<PLCCAuthRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the m request object.
	 *
	 * @param pRequestObject the new m request object
	 */
	public void setRequestObject(JAXBElement<PLCCAuthRequestType> pRequestObject) {
		mRequestObject = pRequestObject;
	}

}
