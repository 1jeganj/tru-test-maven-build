package com.tru.radial.payment.creditcard.bean;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * This class has methods to set radial nonce  service request.
 * @author PA
 * @version 1.0
 */
public class TRURadialGetNonceRequest implements IRadialRequest{
	
	/**
	 * Holds the property value of mRequestString.
	 */
	private String mRequestString;


	/**
	 * Gets the request string.
	 *
	 * @return the request string
	 */
	public String getRequestString() {
		return mRequestString;
	}

	/**
	 *This method is used to set requestString.
	 *@param pRequestString String
	 */
	public void setRequestString(String pRequestString) {
		mRequestString = pRequestString;
	}





}
