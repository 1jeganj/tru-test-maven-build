package com.tru.radial.payment.creditcard.builder;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.CreditCardAuthReplyType;
import com.tru.radial.payment.CreditCardAuthRequestType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.POSMethodType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.PhysicalAddressType;
import com.tru.radial.payment.creditcard.bean.CreditCardAuthorizationRequest;
import com.tru.radial.payment.creditcard.bean.CreditCardAuthorizationResponse;

/**
 * The Class CreditCardAuthorizationBuilder.
 */
public class CreditCardAuthorizationBuilder extends AbstractRequestBuilder{
	
	/** The Constant LOGGER. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(CreditCardAuthorizationBuilder.class);
	
	/** The m PLCC auth request type. */
	private CreditCardAuthRequestType mCreditCardAuthRequestType = null;

	/** The m PLCC auth reply type. */
	private CreditCardAuthReplyType mCreditCardAuthReplyType = null;
	
	/** The m request object. */
	private JAXBElement<CreditCardAuthRequestType> mRequestObject = null;
	
	/**
	 * Instantiates a new credit card authorization builder.
	 */
	public CreditCardAuthorizationBuilder() {
		setCreditCardAuthRequestType(getRadialObjectFactory().createCreditCardAuthRequestType());
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pCreditCardAuthorizationRequest) throws TRURadialRequestBuilderException {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildRequest) : Start");
		}
		CreditCardAuthorizationRequest creditCardAuthorizationRequest = null;
		if (!(pCreditCardAuthorizationRequest instanceof CreditCardAuthorizationRequest)) {
			throw new UnsupportedOperationException();
		}
		creditCardAuthorizationRequest = (CreditCardAuthorizationRequest) pCreditCardAuthorizationRequest;
		//sets the payment context details
		setPaymentContextDetails(mCreditCardAuthRequestType,creditCardAuthorizationRequest);
		
		String expiryDate = creditCardAuthorizationRequest.getExpirationYear().concat(TRURadialConstants.HYPHEN).concat(creditCardAuthorizationRequest.getExpirationMonth());
	    DateFormat format = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT, Locale.ENGLISH);
	    Date date;
	    XMLGregorianCalendar xgc = null;
		try {
			date = format.parse(expiryDate);
			String expDate = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT).format(date);
			xgc = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(expDate);
		} catch (ParseException | DatatypeConfigurationException exp) {
			LOGGER.logError("TRUPaymentIntegrationException at TRURadialPaymentProcessor/creditCardAuthorizationRequest method :{0}",exp);
			throw new TRURadialRequestBuilderException(exp);
		}
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeExpirationDate(xgc));
		if(creditCardAuthorizationRequest.getCvvNum() != null){
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeCardSecurityCode(creditCardAuthorizationRequest.getCvvNum()));
		}
		//sets the amount
		DecimalFormat df = new DecimalFormat(TRURadialConstants.DOUBLE_DECIMAL);      
		BigDecimal orderAmount = new BigDecimal(df.format(creditCardAuthorizationRequest.getAmount()));
		AmountType amountType = getRadialObjectFactory().createAmountType();
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
		amountType.setValue(orderAmount);
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeAmount(amountType));
		mCreditCardAuthRequestType.setRequestId(String.valueOf(creditCardAuthorizationRequest.getUniqueId()));

		//It is needed for OMS, so setting back to pass.
		creditCardAuthorizationRequest.setTransactionId((String.valueOf(creditCardAuthorizationRequest.getUniqueId())));
		//sets the user billing details
		setBillingAddressForCCAuth(mCreditCardAuthRequestType,creditCardAuthorizationRequest);
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeCustomerEmail(creditCardAuthorizationRequest.getEmail()));
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeCustomerIPAddress(creditCardAuthorizationRequest.getTrueClientIpAddress()));
		//sets the user shipping details.
		setUserShippingDetails(mCreditCardAuthRequestType,creditCardAuthorizationRequest);

		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypePOSMethod(POSMethodType.VIRTUAL_WALLET));
		
		// START: Fix for GEOFUS-1294
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeIsRequestToCorrectCVVOrAVSError(
				creditCardAuthorizationRequest.isRequestToCorrectCVVOrAVSError()));
		
		/*if(creditCardAuthorizationRequest.getRetryCount() > IRadialConstants.INT_ZERO){
			mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeIsRequestToCorrectCVVOrAVSError(true));
		}else{
			mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeIsRequestToCorrectCVVOrAVSError(false));
		}*/
		// END: Fix for GEOFUS-1294
		
		//sets the Secure verification data
		//setSecureVerificationData(creditCardAuthRequest);
		mCreditCardAuthRequestType.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeSchemaVersion(creditCardAuthorizationRequest.getSchemaVersion()));
		
		setRequestObject(getRadialObjectFactory().createCreditCardAuthRequest(mCreditCardAuthRequestType));
		
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildRequest) : End");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildResponse) : Start");
		}
		CreditCardAuthorizationResponse response = null;
		if (!(pPaymentResponse instanceof CreditCardAuthorizationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (CreditCardAuthorizationResponse)pPaymentResponse;
		if(IRadialConstants.CREDIT_CARD_AUTH_RESPONSE.equalsIgnoreCase(getCreditCardAuthReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);		
			response.setAuthResponseCode(getCreditCardAuthReplyType().getAuthorizationResponseCode());
			response.setBankAuthCode(getCreditCardAuthReplyType().getBankAuthorizationCode());
			response.setCvv2Code(getCreditCardAuthReplyType().getCVV2ResponseCode());
			response.setAvsCode(getCreditCardAuthReplyType().getAVSResponseCode());
			response.setResponseCode(getCreditCardAuthReplyType().getResponseCode());
		}else{
			response.setAuthResponseCode(getCreditCardAuthReplyType().getAuthorizationResponseCode());
			response.setBankAuthCode(getCreditCardAuthReplyType().getBankAuthorizationCode());
			response.setCvv2Code(getCreditCardAuthReplyType().getCVV2ResponseCode());
			response.setAvsCode(getCreditCardAuthReplyType().getAVSResponseCode());
			response.setResponseCode(getCreditCardAuthReplyType().getResponseCode());
			buildErrorResponse(response);
		}	
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildResponse) : End");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildErrorResponse) : Start");
		}
		CreditCardAuthorizationResponse response = null;
		if (!(pPaymentResponse instanceof CreditCardAuthorizationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (CreditCardAuthorizationResponse)pPaymentResponse;				
		super.buildErrorResponse(pPaymentResponse,getCreditCardAuthReplyType());		
		response.setTimeStamp(getTimeStamp());
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (buildErrorResponse) : End");
		}
	}
	
	/**
	 * This method is used to set the payment context details
	 * @param pCreditCardAuthRequest CreditCardAuthRequestType
	 * @param pCreditCardAuthorizationRequest TRUCreditCardInfo
	 */
	public void setPaymentContextDetails(CreditCardAuthRequestType pCreditCardAuthRequest,CreditCardAuthorizationRequest pCreditCardAuthorizationRequest){
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setPaymentContextDetails) : START");
		}
        PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();
		paymentContextType.setOrderId(pCreditCardAuthorizationRequest.getMerchantRefNumber());
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		if(StringUtils.isNumericOnly(pCreditCardAuthorizationRequest.getCreditCardNumber())){
			paymentAccountUniqueIdType.setIsToken(false);
		} else {
			paymentAccountUniqueIdType.setIsToken(true);
		}
		paymentAccountUniqueIdType.setValue(pCreditCardAuthorizationRequest.getCreditCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypePaymentContext(paymentContextType));
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setPaymentContextDetails) : END");
		}
	}
	
	/**
	 * This method is used to set the user shipping details
	 * @param pCreditCardAuthRequest CreditCardAuthRequestType
	 * @param pCreditCardAuthorizationRequest TRUCreditCardInfo
	 */
	public void setBillingAddressForCCAuth(CreditCardAuthRequestType pCreditCardAuthRequest,CreditCardAuthorizationRequest pCreditCardAuthorizationRequest){
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setBillingAddressForCCAuth) : START");
		}
		String billingPhone = TRURadialConstants.EMPTY;
		PhysicalAddressType billingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		ContactInfo contactInfo = (ContactInfo) pCreditCardAuthorizationRequest.getBillingAddress();
		billingPhone = contactInfo.getPhoneNumber();
		billingPhysicalAddress.setLine1(pCreditCardAuthorizationRequest.getBillingAddress().getAddress1());
		if(!StringUtils.isBlank(pCreditCardAuthorizationRequest.getBillingAddress().getAddress2())){
			billingPhysicalAddress.setLine2(pCreditCardAuthorizationRequest.getBillingAddress().getAddress2());
		}
		billingPhysicalAddress.setCity(pCreditCardAuthorizationRequest.getBillingAddress().getCity());
		billingPhysicalAddress.setMainDivision(pCreditCardAuthorizationRequest.getBillingAddress().getState());
		billingPhysicalAddress.setCountryCode(pCreditCardAuthorizationRequest.getBillingAddress().getCountry());
		billingPhysicalAddress.setPostalCode(pCreditCardAuthorizationRequest.getBillingAddress().getPostalCode());
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeBillingFirstName(pCreditCardAuthorizationRequest.getBillingAddress().getFirstName()));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeBillingLastName(pCreditCardAuthorizationRequest.getBillingAddress().getLastName()));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeBillingPhoneNo(billingPhone));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeBillingAddress(billingPhysicalAddress));
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setBillingAddressForCCAuth) : END");
		}
		
	}
	
	/**
	 * This method is used to set the user shipping details
	 * @param pCreditCardAuthRequest CreditCardAuthRequestType
	 * @param pCreditCardAuthorizationRequest TRUCreditCardInfo
	 */
	public void setUserShippingDetails(CreditCardAuthRequestType pCreditCardAuthRequest,CreditCardAuthorizationRequest pCreditCardAuthorizationRequest){
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setUserShippingDetails) : START");
		}
		
		PhysicalAddressType shippingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		shippingPhysicalAddress.setLine1(pCreditCardAuthorizationRequest.getShipAddress1());
		shippingPhysicalAddress.setCity(pCreditCardAuthorizationRequest.getShipCity());
		if(!StringUtils.isBlank(pCreditCardAuthorizationRequest.getShipAddress2())){
			shippingPhysicalAddress.setLine2(pCreditCardAuthorizationRequest.getShipAddress2());
		}
		shippingPhysicalAddress.setMainDivision(pCreditCardAuthorizationRequest.getShipState());
		shippingPhysicalAddress.setCountryCode(pCreditCardAuthorizationRequest.getShipCountry());
		shippingPhysicalAddress.setPostalCode(pCreditCardAuthorizationRequest.getShipPostalCode());
		
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeShipToFirstName(pCreditCardAuthorizationRequest.getShipToFirstName()));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeShipToLastName(pCreditCardAuthorizationRequest.getShipToLastName()));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeShipToPhoneNo(pCreditCardAuthorizationRequest.getShipToPhone()));
		pCreditCardAuthRequest.getContent().add(getRadialObjectFactory().createCreditCardAuthRequestTypeShippingAddress(shippingPhysicalAddress));
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardAuthorizationBuilder  (setUserShippingDetails) : END");
		}
	}

	/**
	 * @return the mCreditCardAuthRequestType
	 */
	public CreditCardAuthRequestType getCreditCardAuthRequestType() {
		return mCreditCardAuthRequestType;
	}

	/**
	 * Sets the credit card auth request type.
	 *
	 * @param pCreditCardAuthRequestType the new credit card auth request type
	 */
	public void setCreditCardAuthRequestType(CreditCardAuthRequestType pCreditCardAuthRequestType) {
		mCreditCardAuthRequestType = pCreditCardAuthRequestType;
	}

	/**
	 * @return the mCreditCardAuthReplyType
	 */
	public CreditCardAuthReplyType getCreditCardAuthReplyType() {
		return mCreditCardAuthReplyType;
	}

	/**
	 * Sets the credit card auth reply type.
	 *
	 * @param pCreditCardAuthReplyType the new credit card auth reply type
	 */
	public void setCreditCardAuthReplyType(CreditCardAuthReplyType pCreditCardAuthReplyType) {
		mCreditCardAuthReplyType = pCreditCardAuthReplyType;
	}

	/**
	 * @return the mRequestObject
	 */
	public JAXBElement<CreditCardAuthRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the new request object
	 */
	public void setRequestObject(JAXBElement<CreditCardAuthRequestType> pRequestObject) {
		mRequestObject = pRequestObject;
	}
}
