package com.tru.radial.payment.creditcard.bean;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class CreditCardValidationRequest.
 */
public class CreditCardValidationRequest implements IRadialRequest {
	
	/** Property to hold the  merchant ref number. */
	private String mMerchantRefNumber;
	
	/** The m credit card number. */
	private String mCreditCardNumber;
	
	/** The m credit card type. */
	private String mCreditCardType;
	
	/** The m expiration year. */
	private String mExpirationYear;
	
	/** The m expiration month. */
	private String mExpirationMonth;
	/** Property to hold the  cvv. */
	private String mCvv;
	
	/** The m billing address. */
	private Address mBillingAddress;
	
	/** The m unique id. */
	private Long mUniqueId;
	
	/** The m true client ip address. */
	private String mTrueClientIpAddress;
	
	/**
	 * Gets the merchant ref number.
	 *
	 * @return the merchant ref number
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}
	
	/**
	 * Sets the merchant ref number.
	 *
	 * @param pMerchantRefNumber the new merchant ref number
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}
	
	/**
	 * Gets the credit card number.
	 *
	 * @return the credit card number
	 */
	public String getCreditCardNumber() {
		return mCreditCardNumber;
	}
	
	/**
	 * Sets the credit card number.
	 *
	 * @param pCreditCardNumber the new credit card number
	 */
	public void setCreditCardNumber(String pCreditCardNumber) {
		mCreditCardNumber = pCreditCardNumber;
	}
	
	/**
	 * Gets the credit card type.
	 *
	 * @return the credit card type
	 */
	public String getCreditCardType() {
		return mCreditCardType;
	}
	
	/**
	 * Sets the credit card type.
	 *
	 * @param pCreditCardType the new credit card type
	 */
	public void setCreditCardType(String pCreditCardType) {
		mCreditCardType = pCreditCardType;
	}
	
	/**
	 * Gets the expiration year.
	 *
	 * @return the expiration year
	 */
	public String getExpirationYear() {
		return mExpirationYear;
	}
	
	/**
	 * Sets the expiration year.
	 *
	 * @param pExpirationYear the new expiration year
	 */
	public void setExpirationYear(String pExpirationYear) {
		mExpirationYear = pExpirationYear;
	}
	
	/**
	 * Gets the expiration month.
	 *
	 * @return the expiration month
	 */
	public String getExpirationMonth() {
		return mExpirationMonth;
	}
	
	/**
	 * Sets the expiration month.
	 *
	 * @param pExpirationMonth the new expiration month
	 */
	public void setExpirationMonth(String pExpirationMonth) {
		mExpirationMonth = pExpirationMonth;
	}
	
	/**
	 * Gets the cvv.
	 *
	 * @return the cvv
	 */
	public String getCvv() {
		return mCvv;
	}
	
	/**
	 * Sets the cvv.
	 *
	 * @param pCvv the new cvv
	 */
	public void setCvv(String pCvv) {
		mCvv = pCvv;
	}
	
	/**
	 * Gets the billing address.
	 *
	 * @return the billing address
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}
	
	/**
	 * Sets the billing address.
	 *
	 * @param pBillingAddress the new billing address
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}

	/**
	 * Gets the unique id.
	 *
	 * @return the unique id
	 */
	public Long getUniqueId() {
		return mUniqueId;
	}

	/**
	 * Sets the unique id.
	 *
	 * @param pUniqueId the new unique id
	 */
	public void setUniqueId(Long pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**
	 * Gets the true client ip address.
	 *
	 * @return the true client ip address
	 */
	public String getTrueClientIpAddress() {
		return mTrueClientIpAddress;
	}

	/**
	 * Sets the true client ip address.
	 *
	 * @param pTrueClientIpAddress the new true client ip address
	 */
	public void setTrueClientIpAddress(String pTrueClientIpAddress) {
		mTrueClientIpAddress = pTrueClientIpAddress;
	}
}
