package com.tru.radial.payment.creditcard.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * The Class TRURadialGetNonceResponse.
 */
public class TRURadialGetNonceResponse extends AbstractRadialResponse implements IRadialResponse {

	/** The  hold  nonce. */
	private  String mNonce ;

	/** The hold  EXPIRES_IN_SECONDS. */
	private  String mExpiresInSeconds;

	/** The hold JWT. */
	private String mJwt ;

	/** The hold  RADIAL_PAYMENT_ERROR_KEY. */
	private String mRadilalPaymentErrorKey;

	/**
	 * Gets the nonce.
	 *
	 * @return the nonce
	 */
	public String getNonce() {
		return mNonce;
	}

	/**
	 * Sets the nonce.
	 *
	 * @param pNonce the nonce to set
	 */
	public void setNonce(String pNonce) {
		mNonce = pNonce;
	}

	/**
	 * Gets the expires in seconds.
	 *
	 * @return the expiresInSeconds
	 */
	public String getExpiresInSeconds() {
		return mExpiresInSeconds;
	}

	/**
	 * Sets the expires in seconds.
	 *
	 * @param pExpiresInSeconds the expiresInSeconds to set
	 */
	public void setExpiresInSeconds(String pExpiresInSeconds) {
		mExpiresInSeconds = pExpiresInSeconds;
	}

	/**
	 * Gets the jwt.
	 *
	 * @return the jwt
	 */
	public String getJwt() {
		return mJwt;
	}

	/**
	 * Sets the jwt.
	 *
	 * @param pJwt the jwt to set
	 */
	public void setJwt(String pJwt) {
		mJwt = pJwt;
	}

	/**
	 * Gets the radilal payment error key.
	 *
	 * @return the radilalPaymentErrorKey
	 */
	public String getRadilalPaymentErrorKey() {
		return mRadilalPaymentErrorKey;
	}

	/**
	 * Sets the radilal payment error key.
	 *
	 * @param pRadilalPaymentErrorKey the radilalPaymentErrorKey to set
	 */
	public void setRadilalPaymentErrorKey(String pRadilalPaymentErrorKey) {
		mRadilalPaymentErrorKey = pRadilalPaymentErrorKey;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess =pValue;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = pTmeoutReponse;
	}

}
