package com.tru.radial.payment.creditcard.builder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.PhysicalAddressType;
import com.tru.radial.payment.ValidateCardReplyType;
import com.tru.radial.payment.ValidateCardRequestType;
import com.tru.radial.payment.creditcard.bean.CreditCardValidationRequest;
import com.tru.radial.payment.creditcard.bean.CreditCardValidationResponse;

/**
 * The Class CreditCardValidationBuilder.
 */
public class CreditCardValidationBuilder extends AbstractRequestBuilder {
	
	/** The Constant LOGGER. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(CreditCardValidationBuilder.class);
	
	/** The m validate card request type. */
	private ValidateCardRequestType mValidateCardRequestType = null;
	
	/** The m validate card reply type. */
	private ValidateCardReplyType mValidateCardReplyType = null;
	
	/** The m request object. */
	private JAXBElement<ValidateCardRequestType> mRequestObject = null;
	
	/**
	 * Instantiates a new credit card validation builder.
	 */
	public CreditCardValidationBuilder() {
		setValidateCardRequestType(getRadialObjectFactory().createValidateCardRequestType());
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest) throws TRURadialRequestBuilderException {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder (buildRequest) : Start");
		}
		CreditCardValidationRequest creditCardValidationRequest =null;
		if (!(pPaymentRequest instanceof CreditCardValidationRequest)) {
			throw new UnsupportedOperationException();
		}
		creditCardValidationRequest = (CreditCardValidationRequest) pPaymentRequest;
		
		setPaymentContextDetailsForCreditCardValidation(mValidateCardRequestType, creditCardValidationRequest);
		
		String expiryDate = creditCardValidationRequest.getExpirationYear().concat(TRURadialConstants.HYPHEN).concat(creditCardValidationRequest.getExpirationMonth());
	    DateFormat format = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT, Locale.ENGLISH);
	    Date date;
	    XMLGregorianCalendar xgc = null;
		try {
			date = format.parse(expiryDate);
			String expDate = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT).format(date);
			xgc = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(expDate);
		} catch (ParseException | DatatypeConfigurationException exp) {
			LOGGER.logError("TRUPaymentIntegrationException at TRURadialPaymentProcessor/constructCreditCardValidationRequest method :{0}",exp);
			throw new TRURadialRequestBuilderException(exp);
		}
		mValidateCardRequestType.setExpirationDate(xgc);
		
		String cvv = TRURadialConstants.EMPTY;
		if(!StringUtils.isBlank(creditCardValidationRequest.getCvv())){
			cvv = creditCardValidationRequest.getCvv();
		}
		mValidateCardRequestType.setCardSecurityCode(cvv);
		
		//EncryptedCardSecurityCode property mapping will be done here

		mValidateCardRequestType.setCurrencyCode(ISOCurrencyCodeType.USD);
		mValidateCardRequestType.setBillingFirstName(creditCardValidationRequest.getBillingAddress().getFirstName());
		mValidateCardRequestType.setBillingLastName(creditCardValidationRequest.getBillingAddress().getLastName());
		String billingPhone = TRURadialConstants.EMPTY;
		ContactInfo contactInfo = (ContactInfo) creditCardValidationRequest.getBillingAddress();
		billingPhone = contactInfo.getPhoneNumber();
		mValidateCardRequestType.setBillingPhoneNo(billingPhone);
		setBillingAddressForCCValidate(mValidateCardRequestType, creditCardValidationRequest);	
		mValidateCardRequestType.setCustomerEmail(contactInfo.getEmail());
		mValidateCardRequestType.setCustomerIPAddress(creditCardValidationRequest.getTrueClientIpAddress());
		mValidateCardRequestType.setRequestId(String.valueOf(creditCardValidationRequest.getUniqueId()));
		
		setRequestObject(getRadialObjectFactory().createValidateCardRequest(getValidateCardRequestType()));
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder (buildRequest) : End");
		}
	}
	
	/**
	 * Sets the payment context details for credit card validation.
	 *
	 * @param pCreditCardValidateRequest the credit card validate request
	 * @param pCreditCardValidationRequest the credit card validation request
	 */
	public void setPaymentContextDetailsForCreditCardValidation(ValidateCardRequestType pCreditCardValidateRequest,CreditCardValidationRequest pCreditCardValidationRequest){
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (setPaymentContextDetails) : START");
		}
        PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();
        paymentContextType.setOrderId(pCreditCardValidationRequest.getMerchantRefNumber());
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		if(StringUtils.isNumericOnly(pCreditCardValidationRequest.getCreditCardNumber())){
			paymentAccountUniqueIdType.setIsToken(false);
		} else {
			paymentAccountUniqueIdType.setIsToken(true);
		}
		paymentAccountUniqueIdType.setValue(pCreditCardValidationRequest.getCreditCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		pCreditCardValidateRequest.setPaymentContext(paymentContextType);
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (setPaymentContextDetails) : END");
		}
	}
	
	/**
	 * Sets the billing address for CC validate.
	 *
	 * @param pCreditCardValidateRequest the credit card validate request
	 * @param pCreditCardValidationRequest the credit card validation request
	 */
	public void setBillingAddressForCCValidate(ValidateCardRequestType pCreditCardValidateRequest,CreditCardValidationRequest pCreditCardValidationRequest){
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (setBillingAddressForCCValidate) : START");
		}
		PhysicalAddressType billingPhysicalAddress = getRadialObjectFactory().createPhysicalAddressType();
		billingPhysicalAddress.setLine1(pCreditCardValidationRequest.getBillingAddress().getAddress1());
		if(!StringUtils.isBlank(pCreditCardValidationRequest.getBillingAddress().getAddress2())){
			billingPhysicalAddress.setLine2(pCreditCardValidationRequest.getBillingAddress().getAddress2());
		}
		billingPhysicalAddress.setCity(pCreditCardValidationRequest.getBillingAddress().getCity());
		billingPhysicalAddress.setMainDivision(pCreditCardValidationRequest.getBillingAddress().getState());
		billingPhysicalAddress.setCountryCode(pCreditCardValidationRequest.getBillingAddress().getCountry());
		billingPhysicalAddress.setPostalCode(pCreditCardValidationRequest.getBillingAddress().getPostalCode());
		pCreditCardValidateRequest.setBillingAddress(billingPhysicalAddress);
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (setBillingAddressForCCValidate) : END");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (buildResponse) : Start");
		}
		CreditCardValidationResponse response = null;
		if (!(pPaymentResponse instanceof CreditCardValidationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (CreditCardValidationResponse)pPaymentResponse;
		if(IRadialConstants.VALIDATE_CC_SUCCESS_CODE.equalsIgnoreCase(getValidateCardReplyType().getResponseCode())){
			response.setSuccess(Boolean.TRUE);		
			PaymentContextType paymentContextType = getValidateCardReplyType().getPaymentContext();
			//String orderId = paymentContextType.getOrderId();
			PaymentAccountUniqueIdType paymentAccountUniqueIdType = paymentContextType.getPaymentAccountUniqueId();
			//boolean isToken = paymentAccountUniqueIdType.isIsToken();
			response.setCreditCardToken(paymentAccountUniqueIdType.getValue());
			response.setResponseCode(getValidateCardReplyType().getResponseCode());
		}else{
			buildErrorResponse(response);
		}	
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (buildResponse) : End");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.AbstractRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (buildErrorResponse) : Start");
		}
		CreditCardValidationResponse response = null;
		if (!(pPaymentResponse instanceof CreditCardValidationResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (CreditCardValidationResponse)pPaymentResponse;				
		super.buildErrorResponse(pPaymentResponse,getValidateCardReplyType());		
		response.setTimeStamp(getTimeStamp());
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("CreditCardValidationBuilder  (buildErrorResponse) : End");
		}
	}

	/**
	 * Gets the validate card request type.
	 *
	 * @return the validate card request type
	 */
	public ValidateCardRequestType getValidateCardRequestType() {
		return mValidateCardRequestType;
	}

	/**
	 * Sets the validate card request type.
	 *
	 * @param pValidateCardRequestType the new validate card request type
	 */
	public void setValidateCardRequestType(ValidateCardRequestType pValidateCardRequestType) {
		mValidateCardRequestType = pValidateCardRequestType;
	}

	/**
	 * Gets the validate card reply type.
	 *
	 * @return the validate card reply type
	 */
	public ValidateCardReplyType getValidateCardReplyType() {
		return mValidateCardReplyType;
	}

	/**
	 * Sets the validate card reply type.
	 *
	 * @param pValidateCardReplyType the new validate card reply type
	 */
	public void setValidateCardReplyType(ValidateCardReplyType pValidateCardReplyType) {
		mValidateCardReplyType = pValidateCardReplyType;
	}

	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<ValidateCardRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the new request object
	 */
	public void setRequestObject(JAXBElement<ValidateCardRequestType> pRequestObject) {
		mRequestObject = pRequestObject;
	}

}
