package com.tru.radial.payment.creditcard.builder;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.ObjectFactory;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.PhysicalAddressType;
import com.tru.radial.payment.TermsAndConditionsReplyType;
import com.tru.radial.payment.TermsAndConditionsRequestType;
import com.tru.radial.payment.promo.bean.PromoFinanceRequest;
import com.tru.radial.payment.promo.bean.PromoFinanceResponse;

/**
 * The Class PromoFinanceBuilder.
 */
public class PromoFinanceBuilder extends AbstractRequestBuilder {
	
	/** The Constant LOGGER. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(PromoFinanceBuilder.class);
	
	/** The mTermsAndConditionsRequestType. */
	private TermsAndConditionsRequestType mTermsAndConditionsRequestType = null;
	
	/** The mTermsAndConditionsReplyType. */
	private TermsAndConditionsReplyType mTermsAndConditionsReplyType = null;
	
	/** The m request object. */
	private JAXBElement<TermsAndConditionsRequestType> mRequestObject = null;
	
	/**
	 * Instantiates a new promo finance builder.
	 */
	public PromoFinanceBuilder() {
		mTermsAndConditionsRequestType = getRadialObjectFactory().createTermsAndConditionsRequestType();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPromoRequest) throws TRURadialRequestBuilderException {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder (buildRequest) : Start");
		}
		
		PromoFinanceRequest promoFinanceRequest = null;
		
		if (!(pPromoRequest instanceof PromoFinanceRequest)) {
			throw new UnsupportedOperationException();
		}
		promoFinanceRequest = (PromoFinanceRequest)pPromoRequest;
		String expiryDate = TRURadialConstants.EMPTY;
		String address1 = TRURadialConstants.EMPTY;
		String address2 = TRURadialConstants.EMPTY;
		String city = TRURadialConstants.EMPTY;
		String state = TRURadialConstants.EMPTY;
		String country = TRURadialConstants.EMPTY;
		String postalCode = TRURadialConstants.EMPTY;
	    ObjectFactory radialObjFactory = new ObjectFactory();
	    if(promoFinanceRequest.getBillingAddress() != null){
	    if(promoFinanceRequest.getBillingAddress().getAddress1() != null){
	    address1=promoFinanceRequest.getBillingAddress().getAddress1();}
	    if(promoFinanceRequest.getBillingAddress().getAddress2() != null){
	    address2=promoFinanceRequest.getBillingAddress().getAddress2();}
	    if(promoFinanceRequest.getBillingAddress().getCity() != null){
	    	city=promoFinanceRequest.getBillingAddress().getCity();}
	    if(promoFinanceRequest.getBillingAddress().getState() != null){
	    	state=promoFinanceRequest.getBillingAddress().getState();}
	    if(promoFinanceRequest.getBillingAddress().getCountry() != null){
	    	country=promoFinanceRequest.getBillingAddress().getCountry();}
	    if(promoFinanceRequest.getBillingAddress().getPostalCode() != null){
	    	postalCode=promoFinanceRequest.getBillingAddress().getPostalCode();}
	    }
		
		mTermsAndConditionsRequestType.setRequestId(String.valueOf(promoFinanceRequest.getUniqueId()));
		
		PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();
		paymentContextType.setOrderId(promoFinanceRequest.getMerchantRefNumber());
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		if( promoFinanceRequest.getCreditCardNumber() != null && StringUtils.isNumericOnly(promoFinanceRequest.getCreditCardNumber())){
			paymentAccountUniqueIdType.setIsToken(false);
		} else {
			paymentAccountUniqueIdType.setIsToken(true);
		}
		paymentAccountUniqueIdType.setValue(promoFinanceRequest.getCreditCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		
		mTermsAndConditionsRequestType.setPaymentContext(paymentContextType);
		
		AmountType amountType = getRadialObjectFactory().createAmountType();
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
        
		BigDecimal financingCharge = new BigDecimal(promoFinanceRequest.getAmount());
		BigDecimal finCharge = financingCharge.setScale(2, BigDecimal.ROUND_CEILING);
		amountType.setValue(finCharge);
		mTermsAndConditionsRequestType.setAmount(amountType);
		if(promoFinanceRequest.getExpirationYear() != null && promoFinanceRequest.getExpirationMonth() != null){
		expiryDate = promoFinanceRequest.getExpirationYear().concat(TRURadialConstants.HYPHEN).concat(promoFinanceRequest.getExpirationMonth());
		}
		XMLGregorianCalendar xgc = null;
		if(!StringUtils.isBlank(expiryDate)){
		DateFormat format = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT, Locale.ENGLISH);
	    Date date;
	   
		try {
			date = format.parse(expiryDate);
			String expDate = new SimpleDateFormat(TRURadialConstants.EXP_DATE_FORMAT).format(date);
			xgc = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(expDate);
		} catch (ParseException | DatatypeConfigurationException exp) {
			LOGGER.logError("TRUPaymentIntegrationException at PromoFinanceBuilder method :{0}",exp);
			throw new TRURadialRequestBuilderException(exp);
		}
		}
		if(xgc != null){
			mTermsAndConditionsRequestType.setExpirationDate(xgc);
		}
		if(!StringUtils.isBlank(promoFinanceRequest.getCvvNum())){
			mTermsAndConditionsRequestType.setCardSecurityCode(promoFinanceRequest.getCvvNum());
		}
		PhysicalAddressType billingPhysicalAddress = radialObjFactory.createPhysicalAddressType();
		
		if(!StringUtils.isBlank(city)){
			billingPhysicalAddress.setLine1(address1);
			if(!StringUtils.isBlank(address2)){
				billingPhysicalAddress.setLine2(address2);
			}
			billingPhysicalAddress.setCity(city);
			billingPhysicalAddress.setMainDivision(state);
			billingPhysicalAddress.setCountryCode(country);	
			billingPhysicalAddress.setPostalCode(postalCode);	
			mTermsAndConditionsRequestType.setBillingAddress(billingPhysicalAddress);
		}
		setRequestObject(getRadialObjectFactory().createTermsAndConditionsRequest(mTermsAndConditionsRequestType));
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder (buildRequest) : End");
		}
	}
	

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder  (buildResponse) : Start");
		}
		PromoFinanceResponse response = null;
		if (!(pPaymentResponse instanceof PromoFinanceResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (PromoFinanceResponse)pPaymentResponse;
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getTermsAndConditionsReplyType().getResponseCode().toString())){
			response.setSuccess(Boolean.TRUE);		
			response.setPromoFinanceRate(getTermsAndConditionsReplyType().getFinanceRate());
			response.setTermsAndConditionText(getTermsAndConditionsReplyType().getTermsAndConditionsText());
			response.setResponseCode(getTermsAndConditionsReplyType().getResponseCode().toString());
			
		}else{
			buildErrorResponse(response);
		}	
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder  (buildResponse) : End");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.AbstractRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder  (buildErrorResponse) : Start");
		}
		PromoFinanceResponse response = null;
		if (!(pPaymentResponse instanceof PromoFinanceResponse)) {
			throw new UnsupportedOperationException();
		}
		response = (PromoFinanceResponse)pPaymentResponse;				
		super.buildErrorResponse(pPaymentResponse,getTermsAndConditionsReplyType());		
		response.setTimeStamp(getTimeStamp());
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("PromoFinanceBuilder  (buildErrorResponse) : End");
		}
	}

	/**
	 * This method is used to get termsAndConditionsReplyType.
	 * @return termsAndConditionsReplyType String
	 */
	public TermsAndConditionsReplyType getTermsAndConditionsReplyType() {
		return mTermsAndConditionsReplyType;
	}

	/**
	 * This method is used to set termsAndConditionsReplyType.
	 *
	 * @param pTermsAndConditionsReplyType the new terms and conditions reply type
	 */
	public void setTermsAndConditionsReplyType(
			TermsAndConditionsReplyType pTermsAndConditionsReplyType) {
		mTermsAndConditionsReplyType = pTermsAndConditionsReplyType;
	}

	/**
	 * This method is used to get termsAndConditionsRequestType.
	 * @return termsAndConditionsRequestType String
	 */
	public TermsAndConditionsRequestType getTermsAndConditionsRequestType() {
		return mTermsAndConditionsRequestType;
	}

	/**
	 * This method is used to get requestObject.
	 * @return requestObject String
	 */
	public JAXBElement<TermsAndConditionsRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * This method is used to set requestObject.
	 *
	 * @param pRequestObject the new request object
	 */
	public void setRequestObject(
			JAXBElement<TermsAndConditionsRequestType> pRequestObject) {
		mRequestObject = pRequestObject;
	}

}
