package com.tru.radial.payment.creditcard.bean;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.common.beans.TRURadialError;

/**
 * The Class CreditCardAuthorizationResponse.
 */
public class CreditCardAuthorizationResponse extends AbstractRadialResponse implements IRadialResponse {
	
	/** holds the mAuthResponseCode. */
	private String mAuthResponseCode;
	
	/** holds the mBankAuthCode. */
	private String mBankAuthCode;
	
	/** holds the mCvv2Code. */
	private String mCvv2Code;
	
	/** holds the mAvsCode. */
	private String mAvsCode;
	

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setResponseCode(java.lang.String)
	 */
	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;	
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setSuccess(boolean)
	 */
	@Override
	public void setSuccess(boolean pValue) {
		super.mIsSuccess = pValue;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setTimeoutReponse(boolean)
	 */
	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = pTmeoutReponse;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.beans.AbstractRadialResponse#setRadialError(com.tru.radial.common.beans.TRURadialError)
	 */
	@Override
	public void setRadialError(TRURadialError pRadialError) {
		super.mRadialError = pRadialError;
	}

	/**
	 * Gets the auth response code.
	 *
	 * @return the mAuthResponseCode
	 */
	public String getAuthResponseCode() {
		return mAuthResponseCode;
	}

	/**
	 * Sets the auth response code.
	 *
	 * @param pAuthResponseCode the new auth response code
	 */
	public void setAuthResponseCode(String pAuthResponseCode) {
		mAuthResponseCode = pAuthResponseCode;
	}

	/**
	 * Gets the bank auth code.
	 *
	 * @return the mBankAuthCode
	 */
	public String getBankAuthCode() {
		return mBankAuthCode;
	}

	/**
	 * Sets the bank auth code.
	 *
	 * @param pBankAuthCode the new bank auth code
	 */
	public void setBankAuthCode(String pBankAuthCode) {
		mBankAuthCode = pBankAuthCode;
	}

	/**
	 * Gets the cvv 2 code.
	 *
	 * @return the mCvv2Code
	 */
	public String getCvv2Code() {
		return mCvv2Code;
	}

	/**
	 * Sets the cvv 2 code.
	 *
	 * @param pCvv2Code the new cvv 2 code
	 */
	public void setCvv2Code(String pCvv2Code) {
		mCvv2Code = pCvv2Code;
	}

	/**
	 * Gets the avs code.
	 *
	 * @return the mAvsCode
	 */
	public String getAvsCode() {
		return mAvsCode;
	}

	/**
	 * Sets the avs code.
	 *
	 * @param pAvsCode the new avs code
	 */
	public void setAvsCode(String pAvsCode) {
		mAvsCode = pAvsCode;
	}
}
