package com.tru.radial.payment.creditcard.bean;

import atg.core.util.Address;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * The Class PLCCCardAuthorizationRequest.
 */
public class PLCCCardAuthorizationRequest implements IRadialRequest {
	
	/** Property to hold the  mRetryCount. */
	private int mRetryCount;
	
	/** Property to hold the  mCvvNumber. */
	private String mCvvNumber;
	
	/** Property to hold the  merchant ref number. */
	private String mMerchantRefNumber;
	
	/** The m credit card number. */
	private String mCreditCardNumber;
	
	/** Property to hold the  transaction id. */
	private String mTransactionId;
	
	/** The m amount. */
	private double mAmount;
	
	/** The m billing address. */
	private Address mBillingAddress;
	
	/** The m unique id. */
	private Long mUniqueId;

	/**
	 * Gets the merchant ref number.
	 *
	 * @return the mMerchantRefNumber
	 */
	public String getMerchantRefNumber() {
		return mMerchantRefNumber;
	}

	/**
	 * Sets the merchant ref number.
	 *
	 * @param pMerchantRefNumber the new merchant ref number
	 */
	public void setMerchantRefNumber(String pMerchantRefNumber) {
		mMerchantRefNumber = pMerchantRefNumber;
	}

	/**
	 * Gets the credit card number.
	 *
	 * @return the mCreditCardNumber
	 */
	public String getCreditCardNumber() {
		return mCreditCardNumber;
	}

	/**
	 * Sets the credit card number.
	 *
	 * @param pCreditCardNumber the new credit card number
	 */
	public void setCreditCardNumber(String pCreditCardNumber) {
		mCreditCardNumber = pCreditCardNumber;
	}

	/**
	 * Gets the transaction id.
	 *
	 * @return the mTransactionId
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * Sets the transaction id.
	 *
	 * @param pTransactionId the new transaction id
	 */
	public void setTransactionId(String pTransactionId) {
		mTransactionId = pTransactionId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the mAmount
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param pAmount the new amount
	 */
	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}

	/**
	 * Gets the billing address.
	 *
	 * @return the mBillingAddress
	 */
	public Address getBillingAddress() {
		return mBillingAddress;
	}

	/**
	 * Sets the billing address.
	 *
	 * @param pBillingAddress the new billing address
	 */
	public void setBillingAddress(Address pBillingAddress) {
		mBillingAddress = pBillingAddress;
	}

	/**
	 * Gets the unique id.
	 *
	 * @return the mUniqueId
	 */
	public Long getUniqueId() {
		return mUniqueId;
	}

	/**
	 * Sets the unique id.
	 *
	 * @param pUniqueId the new unique id
	 */
	public void setUniqueId(Long pUniqueId) {
		mUniqueId = pUniqueId;
	}

	/**
	 * This method is used to get cvvNumber.
	 * @return mCvvNumber String
	 */
	public String getCvvNumber() {
		return mCvvNumber;
	}

	/**
	 *This method is used to set cvvNumber.
	 *@param pCvvNumber String
	 */
	public void setCvvNumber(String pCvvNumber) {
		mCvvNumber = pCvvNumber;
	}

	/**
	 * This method is used to get retryCount.
	 * @return mRetryCount int
	 */
	public int getRetryCount() {
		return mRetryCount;
	}

	/**
	 *This method is used to set retryCount.
	 *@param pRetryCount int
	 */
	public void setRetryCount(int pRetryCount) {
		mRetryCount = pRetryCount;
	}

	/** The Request to correct cvv or avs error. */
	private boolean mRequestToCorrectCVVOrAVSError;

	/**
	 * @return the requestToCorrectCVVOrAVSError
	 */
	public boolean isRequestToCorrectCVVOrAVSError() {
		return mRequestToCorrectCVVOrAVSError;
	}

	/**
	 * @param pRequestToCorrectCVVOrAVSError the requestToCorrectCVVOrAVSError to set
	 */
	public void setRequestToCorrectCVVOrAVSError(
			boolean pRequestToCorrectCVVOrAVSError) {
		mRequestToCorrectCVVOrAVSError = pRequestToCorrectCVVOrAVSError;
	}
}
