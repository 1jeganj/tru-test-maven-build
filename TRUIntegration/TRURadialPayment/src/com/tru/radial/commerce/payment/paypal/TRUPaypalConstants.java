package com.tru.radial.commerce.payment.paypal;


/**
* This class will help to fetch the different constant used for PaymentPlugin Integrations.
* @author PA
* @version 1.0
*/
public class TRUPaypalConstants {
	/**
	 * Constant to hold the value of Success.
	 */
	public static final String SUCCESS = "Success";
	/**
	 * Constant to hold the value of Failed.
	 */
	public static final String FAILURE = "Failed";
	/**
	 * Constant to hold the value of POST.
	 */
	public static final String POST = "POST";
	/**
	 * Constant to hold the value of AuthorizationId.
	 */
	public static final String AUTHORIZATIONID = "AuthorizationId";
	/**
	 * Constant to hold the value of CompleteType.
	 */
	public static final String COMPLETETYPE = "CompleteType";
	/**
	 * Constant to hold the value of CompleteType.
	 */
	public static final String COMPLETE = "Complete";
	/**
	/**
	 * Constant to hold the value of Amount.
	 */
	public static final String AMOUNT = "Amount";
	/**
	 * Constant to hold the value of default.
	 */
	public static final String DEFAULT = "default";
	/**
	 * Constant to hold the value of proxyHost.
	 */
	public static final String HTTP_PROXY_HOST = "proxyHost";
	/**
	 *  Constant to hold the value of proxyPort.
	 */
	public static final String HTTP_PROXY_PORT = "proxyPort";

	/**
	 * Constant to hold the value of CONTENT_TYPE.
	 */
	public static final String CONTENT_TYPE = "Content-Type";
	/**
	 * Constant to hold the value of CONTENT_LENGTH.
	 */
	public static final String CONTENT_LENGTH = "Content-Length";
	/**
	 * Constant to hold the value of APPLICATION_OR_XML.
	 */
	public static final String APPLICATION_OR_XML = "application/x-www-form-urlencoded";
	/**
	 * Constant to hold the value of RETURN_URL.
	 */
	public static final String RETURN_URL = "RETURN_URL";
	/**
	 * Constant to hold the value of CANCEL_URL.
	 */
	public static final String CANCEL_URL = "CANCEL_URL";
	/**
	 * Constant to hold the value of VERSION.
	 */
	public static final String VERSION = "VERSION=";
	/**
	 * Constant to hold the value of USER.
	 */
	public static final String USER = "USER=";
	/**
	 * Constant to hold the value of SIGNATURE.
	 */
	public static final String SIGNATURE = "SIGNATURE=";
	/**
	 * Constant to hold the value of PWD.
	 */
	public static final String PWD = "PWD=";
	/**
	 * Constant to hold the value of AMPERSAND.
	 */
	public static final String AMPERSAND = "&";
	/**
	 * Constant to hold the value of EQUAL_TO.
	 */
	public static final String EQUAL_TO = "=";
	/**
	 * Constant to hold the value of FOUR_THOUSAND.
	 */
    public static final int FOUR_THOUSAND = 4000;
    /**
	 * Constant to hold the value of SALE.
	 */
	public static final String SALE = "Sale";
	 /**
	 * Constant to hold the value of DIGIT_TWO.
	 */
	public static final int DIGIT_TWO = 2;
	 /**
	 * Constant to hold the value of DOUBLE_ZERO.
	 */
	public static final double DOUBLE_ZERO = 0.0;
	 /**
	 * Constant to hold the value of ZERO.
	 */
	public static final String DIGIT_ZERO = "0";
	/**
	 * Constant to hold the value of ZERO.
	 */
	public static final String REPLACE_PATTERN = "$$";
	/**
	 * Constant to hold the value of -1.
	 */
	public static final int DIGIT_ONE = 1;
	/**
	 * Constant to hold the value of 5.
	 */
	public static final int DIGIT_FIVE = 5;
	
	/**
	 * constant to hold 1 for order includes only electronic items.
	 */
	public static final String  ALL_ELECTRONICITEMS ="1";
	
	/**
	 * constant to hold 0 for order includes hardgood items.
	 */
	public static final String  INCLUDES_HARDGOODITEMS="0";	
	/**
	 * constant to hold Sale.
	 */
	public static final String PAYPAL_PAYMENT_ACTION_SALE= "Sale";
	/**
	 * constant to hold Order.
	 */
	public static final String PAYPAL_PAYMENT_ACTION_ORDER= "Order";

	/**
	 * constant to hold application/x-www-form-urlencoded.
	 */
	public static final String PAYPAL_HTTP_CONTENT_TYP = "application/x-www-form-urlencoded";

	/**
	 * POST method is used for PAYPAL.
	 */
	public static final String PAYPAL_METHOD_TYPE = "POST";
	/**
	 * constant to hold VERSION.
	 */
	public static final String PAYPAL_VERSION_TAG = "VERSION";
	/**
	 * constant to hold Success.
	 */
	public static final String PAYPAL_SUCCESS_STATUS = "Success";
	/**
	 * constant to hold SuccessWithWarning.
	 */
	public static final String PAYPAL_WARNING_STATUS = "SuccessWithWarning";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_DOEXPRCHKOUTPAYMENT_METHOD = "DoExpressCheckoutPayment";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_GETEXPR_CHKOUT_METHOD = "GetExpressCheckoutDetails";
	/**
	 * constant to hold SetExpressCheckout.
	 */
	public static final String PAYPAL_SETEXPRESSCHECKOUT_METHOD = "SetExpressCheckout";
	 /**
     * Holds colon public static final String.
     */
    public static final String COLON_STRING = ":";
    /**
     * Constant to hold string "".
     */
    public static final String BLANK_STRING = "";
    
    /**
     * Constant to hold string " ".
     */
    public static final String SPACE_STRING = " ";
    /**
	 * Holds the message for null request.
	 */
	public static final String GET_EXP_CHK_REQUEST_NULL_MSG = "GetExpCheckoutRequest is null. GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for Invalid Response.
	 */
	public static final String GET_EXP_CHK_INVALID_RESPONSE_MSG = "PayPal Response is null/blank, GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for error response.
	 */
	public static final String GET_EXP_CHK_ERROR_RESPONSE_MSG = "Error response from PayPal, GetExpressCheckoutDetails failed";
	 /**
     * Constant to hold the paypal date format.
     */
    public static final String ORDER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    /**
     * Constant to hold the int value 1.
     */
    public static final int ONE = 1;
    /**
     * Constant to hold the constant for "_".
     */
    public static final String UNDER_SCORE = "_";     
 
    /**
     * Constant to hold the constant for "PAYPAL_PAYMENT_AUTH_ERROR".
     */
    public static final String PAYPAL_PAYMENT_AUTH_ERROR = "PAYPAL_PAYMENT_AUTH_ERROR";
    
    /**
     * Constant to hold the constant for "PAYPAL_PAYMENT_DEBIT_ERROR".
     */
    public static final String REQUEST_ID = "requestId";
    
    /**
     * Constant to hold the number maximum fractional digits allowed for paypalrequest.
     */
    public static final int MAXIMUM_FRACTIONALDIGITS = 2;
    
    /**
     * Constant to hold the cart.
     */
    public static final String CART = "Cart";   
    
    /**
     * Constant to hold the zero value.
     */
    public static final int ORDERAMT_ZERO = 0;
    
    /**
     * Constant to hold the zero value.
     */
    public static final double ORDER_ZEROAMT = 0d;
    
    /**
     * Constant to hold comments.
     */
    public static final String COMMENTS = "comments";     
    
    /**
     * Constant to hold InvalidPaymentGroupParameter.
     */
    public static final String INVALIDPAYMENTGROUP_PARAMETER = "InvalidPaymentGroupParameter";  
    
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String PAYPAL_LOGGING_IDENTIFIER = "TRUProcValidatePayPal";
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String USER_MESSAGE_BUNDLE = "atg.commerce.order.UserMessages";
    /**
     * Constant to hold USD.
     */
    public static final String USD = "USD";    
    /**
     * Constant to hold ZERO_AUTH_TRANS_ID.
     */
    public static final String ZERO_AUTH_TRANSACTION_ID = "ZERO_AUTH_TRANS_ID";    
    /**
     * Constant to hold ZERO_DEBIT_TRANS_ID.
     */
    public static final String ZERO_DEBIT_TRANSACTION_ID = "ZERO_DEBIT_TRANS_ID";
    /**
     * Constant to hold PAYPAL_TOKEN_EMPTY.
     */    
	public static final String PAYPAL_TOKEN_EMPTY = "PAYPAL_TOKEN_EMPTY";
	/**
     * Constant to hold PAYPAL_TOKEN_NOT_VALID.
     */    
	public static final String PAYPAL_TOKEN_NOT_VALID = "PAYPAL_TOKEN_NOT_VALID";
	
	/**
     * Constant to hold PAYPAL_GROUP_NOT_VALID.
     */    
	public static final String PAYPAL_GROUP_NOT_VALID = "PAYPAL_GROUP_NOT_VALID";
	/**
     * Constant to hold PAYPAL_TOKEN_EMPTY.
     */    
	public static final String INVALID_PAYPAL_SHIPPING_ADDRESS = "INVALID_PAYPAL_SHIPPING_ADDRESS";
	/**
	 * The Constant for isAlreadyToken.
	 */
	public static final String PAYPAL_TOKEN_ALREADY_TAKEN = "isAlreadyToken";
	/**  Constant to hold United States. */
	public static final String UNITED_STATES = "US";
	
	/**  Constant to hold PAYPAL_IP_ADDRESS. */
	public static final String PAYPAL_IP_ADDRESS = "X-FORWARDED-FOR";
	
	/**  Constant to hold PAYPAL_APPROVED. */
	public static final String PAYPAL_APPROVED = "approved";

	/**  Constant to hold INDEX ZERO. */
	public static final int INDEX_ZERO = 0;
	
	/**  Constant to hold INDEX ONE. */
	public static final int INDEX_ONE = 1;
	
	 /**
     * Constant to hold the constant for "TRU_ERROR_RADIAL_PAYPAL_DISABLED".
     */
    public static final String TRU_ERROR_RADIAL_PAYPAL_DISABLED = "tru_error_radial_paypal_disabled";
}

