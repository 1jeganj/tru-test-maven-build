package com.tru.radial.commerce.payment.giftcard;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.StoredValueRedeemVoidReplyType;
import com.tru.radial.payment.StoredValueRedeemVoidRequestType;
import com.tru.radial.payment.giftcard.bean.GiftCardVoidRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardVoidResponse;

/**
 * The Class GiftCardVoidRequestBuilder.
 */
public class GiftCardVoidRequestBuilder extends AbstractRequestBuilder {
	
	/** The Constant LOGGER. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(GiftCardVoidRequestBuilder.class);
	
	/** The Redeem Void Request Type. */
	private StoredValueRedeemVoidRequestType mRedeemVoidRequestType = null;
	
	/** The Stored Value Redeem Void Reply Type. */
	private StoredValueRedeemVoidReplyType mStoredValueRedeemVoidReplyType = null;
	
	/** The Request Object. */
	private JAXBElement<StoredValueRedeemVoidRequestType> mRequestObject = null;
	
	/**
	 * Instantiates a new Gift Card Void RequestBuilder.
	 */
	public GiftCardVoidRequestBuilder() {
		mRedeemVoidRequestType = getRadialObjectFactory().createStoredValueRedeemVoidRequestType();
	}
	

	/**
	 * Gets the redeem void request type.
	 *
	 * @return the mRedeemVoidRequestType
	 */
	public StoredValueRedeemVoidRequestType getRedeemVoidRequestType() {
		return mRedeemVoidRequestType;
	}

	/**
	 * Sets the redeem void request type.
	 *
	 * @param pRedeemVoidRequestType the pRedeemVoidRequestType to set
	 */
	public void setRedeemVoidRequestType(StoredValueRedeemVoidRequestType pRedeemVoidRequestType) {
		this.mRedeemVoidRequestType = pRedeemVoidRequestType;
	}

	/**
	 * Gets the request object.
	 *
	 * @return the mRequestObject
	 */
	public JAXBElement<StoredValueRedeemVoidRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the mRequestObject to set
	 */
	public void setRequestObject(JAXBElement<StoredValueRedeemVoidRequestType> pRequestObject) {
		this.mRequestObject = pRequestObject;
	}

	/**
	 * Gets the stored value redeem void reply type.
	 *
	 * @return the mStoredValueRedeemVoidReplyType
	 */
	public StoredValueRedeemVoidReplyType getStoredValueRedeemVoidReplyType() {
		return mStoredValueRedeemVoidReplyType;
	}

	/**
	 * Sets the stored value redeem void reply type.
	 *
	 * @param pStoredValueRedeemVoidReplyType the pStoredValueRedeemVoidReplyType to set
	 */
	public void setStoredValueRedeemVoidReplyType(StoredValueRedeemVoidReplyType pStoredValueRedeemVoidReplyType) {
		this.mStoredValueRedeemVoidReplyType = pStoredValueRedeemVoidReplyType;
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pPaymentRequest) throws TRURadialRequestBuilderException {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardVoidRequestBuilder.buildRequest :: START");
		}
		GiftCardVoidRequest voidRequest = (GiftCardVoidRequest) pPaymentRequest;
		
		PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();
		paymentContextType.setOrderId(voidRequest.getOrderId());
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		paymentAccountUniqueIdType.setIsToken(false);
		paymentAccountUniqueIdType.setValue(voidRequest.getGiftCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		AmountType amountType = getRadialObjectFactory().createAmountType();
		amountType.setValue(BigDecimal.valueOf(voidRequest.getAmount()));
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);
		mRedeemVoidRequestType.setPaymentContext(paymentContextType);
		mRedeemVoidRequestType.setAmount(amountType);
		mRedeemVoidRequestType.setRequestId(voidRequest.getTransactionId());
		mRedeemVoidRequestType.setPin(voidRequest.getPin());
		setRequestObject(getRadialObjectFactory().createStoredValueRedeemVoidRequest(mRedeemVoidRequestType));
		
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardVoidRequestBuilder.buildRequest :: END");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse)  {
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardVoidRequestBuilder.buildResponse :: START");
		}
		if (!(pPaymentResponse instanceof GiftCardVoidResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardVoidResponse response = (GiftCardVoidResponse)pPaymentResponse;
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getStoredValueRedeemVoidReplyType().getResponseCode().value())){
			response.setResponseCode(getStoredValueRedeemVoidReplyType().getResponseCode().value());
			response.setSuccess(Boolean.TRUE);		
		}else{
			buildErrorResponse(pPaymentResponse);
		}
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardVoidRequestBuilder.buildResponse :: END");
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.AbstractRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof GiftCardVoidResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardVoidResponse response = (GiftCardVoidResponse)pPaymentResponse;		
		super.buildErrorResponse(response,getStoredValueRedeemVoidReplyType());
		response.setTimeStamp(getTimeStamp());
	}
	
}
