package com.tru.radial.commerce.payment.paypal.util;

/**
 * The custom exception which will be used at all PayPal operations.
 * @author PA
 * @version 1.0
 */

public class TRUPayPalException extends Exception {

	static final long serialVersionUID = 0L;
	/**
	 * Overridden Constructor.
	 */
	public TRUPayPalException() {
		super();
	}
	/**
	 * @param pMessage the message
	 * @param pNestedException the nestedException
	 */
	public TRUPayPalException(String pMessage, Throwable pNestedException) {
		super(pMessage, pNestedException);
	}
	/**
	 * @param pMessage the message
	 */
	public TRUPayPalException(String pMessage) {
		super(pMessage);
	}
	/**
	 * @param pNestedException the nestedException
	 */
	public TRUPayPalException(Throwable pNestedException) {
		super(pNestedException);
	}

}