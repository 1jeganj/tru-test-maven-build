package com.tru.radial.commerce.payment.giftcard;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.payment.AmountType;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.PaymentContextType;
import com.tru.radial.payment.StoredValueRedeemReplyType;
import com.tru.radial.payment.StoredValueRedeemRequestType;
import com.tru.radial.payment.giftcard.bean.GiftCardReedemRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardReedemResponse;

/**
 * The Class GiftCardReedemRequestBuilder.
 */
public class GiftCardReedemRequestBuilder extends AbstractRequestBuilder {

	/** The Constant mLogger. */
	public static final ApplicationLogging LOGGER = ClassLoggingFactory.getFactory().getLoggerForClass(GiftCardReedemRequestBuilder.class);
	
	
	/** The m gift card balance request. */
	private StoredValueRedeemRequestType mRedeemRequestType = null;
	
	/** The redeem reply type. */
	private StoredValueRedeemReplyType mRedeemReplyType = null;
	
	/** The m request object. */
	private JAXBElement<StoredValueRedeemRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new gift card reedem request builder.
	 */
	public GiftCardReedemRequestBuilder() {
		mRedeemRequestType = getRadialObjectFactory().createStoredValueRedeemRequestType();
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pGiftCardReedemRequest)
			throws TRURadialRequestBuilderException {		
		GiftCardReedemRequest redeemRequest = (GiftCardReedemRequest)pGiftCardReedemRequest;		
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardReedemRequestBuilder  (buildRequest) : Start");
		}
		PaymentContextType paymentContextType = getRadialObjectFactory().createPaymentContextType();		
		paymentContextType.setOrderId(redeemRequest.getOrderId());		
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();
		paymentAccountUniqueIdType.setIsToken(false);
		paymentAccountUniqueIdType.setValue(redeemRequest.getGiftCardNumber());
		paymentContextType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		mRedeemRequestType.setPaymentContext(paymentContextType);
		
		//Setting amount
		AmountType amountType = getRadialObjectFactory().createAmountType();
		amountType.setValue(BigDecimal.valueOf(redeemRequest.getAmount()));
		amountType.setCurrencyCode(ISOCurrencyCodeType.USD);		
		mRedeemRequestType.setAmount(amountType);
		
		mRedeemRequestType.setRequestId(redeemRequest.getTransactionId());
		mRedeemRequestType.setPin(redeemRequest.getPin());
		setRequestObject(getRadialObjectFactory().createStoredValueRedeemRequest(mRedeemRequestType));		
		if (LOGGER.isLoggingDebug()) {
			LOGGER.logDebug("GiftCardReedemRequestBuilder  (buildRequest) : End");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse){
		if (!(pPaymentResponse instanceof GiftCardReedemResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardReedemResponse response = (GiftCardReedemResponse)pPaymentResponse;
		if (IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getRedeemReplyType().getResponseCode().value())) {
			response.setSuccess(Boolean.TRUE);
			response.setBalanceAmount(getRedeemReplyType().getBalanceAmount().getValue());
			response.setResponseCode(getRedeemReplyType().getResponseCode().value());
		} else {
			buildErrorResponse(pPaymentResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.AbstractRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof GiftCardReedemResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardReedemResponse response = (GiftCardReedemResponse)pPaymentResponse;
		super.buildErrorResponse(response,getRedeemReplyType());
		response.setTimeStamp(getTimeStamp());
	}

	/**
	 * Gets the redeem request type.
	 *
	 * @return the redeemRequestType
	 */
	public StoredValueRedeemRequestType getRedeemRequestType() {
		return mRedeemRequestType;
	}

	/**
	 * Sets the redeem request type.
	 *
	 * @param pRedeemRequestType the redeemRequestType to set
	 */
	public void setRedeemRequestType(StoredValueRedeemRequestType pRedeemRequestType) {
		this.mRedeemRequestType = pRedeemRequestType;
	}

	/**
	 * Gets the redeem reply type.
	 *
	 * @return the redeemReplyType
	 */
	public StoredValueRedeemReplyType getRedeemReplyType() {
		return mRedeemReplyType;
	}

	/**
	 * Sets the redeem reply type.
	 *
	 * @param pRedeemReplyType the redeemReplyType to set
	 */
	public void setRedeemReplyType(StoredValueRedeemReplyType pRedeemReplyType) {
		this.mRedeemReplyType = pRedeemReplyType;
	}

	/**
	 * Gets the request object.
	 *
	 * @return the mRequestObject
	 */
	public JAXBElement<StoredValueRedeemRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the mRequestObject to set
	 */
	public void setRequestObject(
			JAXBElement<StoredValueRedeemRequestType> pRequestObject) {
		this.mRequestObject = pRequestObject;
	}

	
}
