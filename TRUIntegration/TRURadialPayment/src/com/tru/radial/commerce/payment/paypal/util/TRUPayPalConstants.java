package com.tru.radial.commerce.payment.paypal.util;
/**
 * The constant file to hold all the constants for the payPal.
 * @author PA
 * @version 1.0
 */
public class TRUPayPalConstants {
	
	/**
	 * constant to hold payPalToken.
	 */
	public static final String PAYPAL_TOKEN = "payPalToken";
	/**
	 * constant to hold payerId.
	 */
	public static final String PAYER_ID = "payerId";

	/**
     * constant to hold the constant for "token".
     */
    public static final String RESPONSE_TOKEN = "token";

    /**
     * constant to hold the constant for "PAYPAL_PAYMENT_DEBIT_ERROR".
     */
    public static final String REQUEST_ID = "requestId";
    
    /**
	 * constant to hold 1 for order includes only electronic items.
	 */
	public static final String  ALL_ELECTRONICITEMS ="1";
	
	/**
	 * constant to hold 0 for order includes hardgood items.
	 */
	public static final String  INCLUDES_HARDGOODITEMS="0";	
	/**
	 * constant to hold Sale.
	 */
	public static final String PAYPAL_PAYMENT_ACTION_SALE= "Sale";
	/**
	 * Content Type constant.
	 */
	public static final String CONTENT_TYPE = "Content-Type";
	/**
	 * constant to hold application/x-www-form-urlencoded.
	 */
	public static final String PAYPAL_HTTP_CONTENT_TYP = "application/x-www-form-urlencoded";
	/**
	 * constant to hold Content-Length.
	 */
	public static final String CONTENT_LENGTH = "Content-Length";
	/**
	 * POST method is used for PAYPAL.
	 */
	public static final String PAYPAL_METHOD_TYPE = "POST";
	/**
	 * constant to hold VERSION.
	 */
	public static final String PAYPAL_VERSION_TAG = "VERSION";
	/**
	 * constant to hold Success.
	 */
	public static final String PAYPAL_SUCCESS_STATUS = "Success";
	/**
	 * constant to hold SuccessWithWarning.
	 */
	public static final String PAYPAL_WARNING_STATUS = "SuccessWithWarning";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_DOEXPRCHKOUTPAYMENT_METHOD = "DoExpressCheckoutPayment";
	/**
	 * constant to hold DoExpressCheckoutPayment.
	 */
	public static final String PAYPAL_GETEXPR_CHKOUT_METHOD = "GetExpressCheckoutDetails";
	/**
	 * constant to hold SetExpressCheckout.
	 */
	public static final String PAYPAL_SETEXPRESSCHECKOUT_METHOD = "SetExpressCheckout";
	 /**
     * Holds colon public static final String.
     */
    public static final String COLON_STRING = ":";
    /**
     * Constant to hold string "".
     */
    public static final String BLANK_STRING = "";
    /**
	 * Holds the message for null request.
	 */
	public static final String GET_EXP_CHK_REQUEST_NULL_MSG = "GetExpCheckoutRequest is null. GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for Invalid Response.
	 */
	public static final String GET_EXP_CHK_INVALID_RESPONSE_MSG = "PayPal Response is null/blank, GetExpressCheckoutDetails failed";
	/**
	 * Holds the message for error response.
	 */
	public static final String GET_EXP_CHK_ERROR_RESPONSE_MSG = "Error response from PayPal, GetExpressCheckoutDetails failed";
	 /**
     * Constant to hold the paypal date format.
     */
    public static final String ORDER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    /**
     * Constant to hold the int value 1.
     */
    public static final int ONE = 1;
    /**
     * Constant to hold the constant for "_".
     */
    public static final String UNDER_SCORE = "_";     
 
    /**
     * Constant to hold the constant for "PAYPAL_PAYMENT_AUTH_ERROR".
     */
    public static final String PAYPAL_PAYMENT_AUTH_ERROR = "PAYPAL_PAYMENT_AUTH_ERROR";
   
    /**
     * Constant to hold the number maximum fractional digits allowed for paypalrequest.
     */
    public static final int MAXIMUM_FRACTIONALDIGITS = 2;
    
    /**
     * Constant to hold the cart.
     */
    public static final String CART = "Cart";   
    
    /**
     * Constant to hold the zero value.
     */
    public static final int ORDERAMT_ZERO = 0;
    
    /**
     * Constant to hold the zero value.
     */
    public static final double ORDER_ZEROAMT = 0d;
    
    /**
     * Constant to hold comments.
     */
    public static final String COMMENTS = "comments";     
    
    /**
     * Constant to hold InvalidPaymentGroupParameter.
     */
    public static final String INVALIDPAYMENTGROUP_PARAMETER = "InvalidPaymentGroupParameter";  
    
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String PAYPAL_LOGGING_IDENTIFIER = "TRUProcValidatePayPal";
    /**
     * Constant to hold the PAYPAL_LOGGING_IDENTIFIER.
     */
    public static final String USER_MESSAGE_BUNDLE = "atg.commerce.order.UserMessages";
    /**
     * Constant to hold USD.
     */
    public static final String USD = "USD";    
    /**
     * Constant to hold ZERO_AUTH_TRANS_ID.
     */
    public static final String ZERO_AUTH_TRANSACTION_ID = "ZERO_AUTH_TRANS_ID";    
    /**
     * Constant to hold ZERO_DEBIT_TRANS_ID.
     */
    public static final String ZERO_DEBIT_TRANSACTION_ID = "ZERO_DEBIT_TRANS_ID";
    /**
     * Constant to hold PAYPAL_COMMIT_HANDLER_METHOD_NAME.
     */
	public static final String PAYPAL_COMMIT_HANDLER_METHOD_NAME = "handlePaypalCommit";
	/**
     * Constant to hold PAYPAL_CHECKOUT_ORDERREVIEW_HANDLER_METHOD_NAME.
     */
	public static final String PAYPAL_CHECKOUT_ORDERREVIEW_HANDLER_METHOD_NAME = "handleCheckoutWithPayPalFromOrderReview";    

	/**
     * Constant to hold PAYPAL_HANDLER_EXPRESS_CHECKOUT.
     */
	public static final String PAYPAL_HANDLER_EXPRESS_CHECKOUT = "handleExpressCheckoutWithPayPal";
	
	/**
     * Constant to hold PAYPAL_HANDLER_CART_EXPRESS_CHECKOUT.
     */
	public static final String PAYPAL_HANDLER_CART_EXPRESS_CHECKOUT = "handleCartExpressCheckoutWithPayPal";
	
	/**
     * Constant to hold PAYPAL_COMMIT_HANDLER_CART_EXPRESS_CHECKOUT.
     */
	public static final String PAYPAL_COMMIT_HANDLER_CART_EXPRESS_CHECKOUT = "handleCartExpressCheckoutWithPayPal";
	
	/**
     * Constant to hold PAYPAL_DROPLET_CHECK_EXPRESS_CHECKOUT.
     */
	public static final String PAYPAL_DROPLET_CHECK_EXPRESS_CHECKOUT = "TRUCheckPaypalExpressCheckoutDroplet.isPaypalTokenAlreadyCaptured";
	
	/**
	 * constant to hold PAGE_NAME.
	 */
	public static final String PAYMENT_PAGE_NAME = "paymentPage";
	
	/**
	 * constant to hold CART_PAGE_NAME.
	 */
	public static final String CART_PAGE_NAME = "cartPage";

	/**
	 * constant to hold REVIEW_PAGE_NAME.
	 */
	public static final String REVIEW_PAGE_NAME = "reviewPage";
	
	/**
	 * constant to hold PAYPAL_STATUS_ITEMDESC_NAME.
	 */
	public static final String PAYPAL_STATUS_ITEMDESC_NAME = "payPalStatus";
	
	/**
	 * constant to hold PAYPAL_TOKEN_TIMESTAMP.
	 */
	public static final String PAYPAL_TOKEN_TIMESTAMP = "payPalTokenTimeStamp";
	
	/**
	 * Constant to hold the value of 30.
	 */
	public static final long LONG_DIGIT_THIRTY = 30L;
	
	/**
	 * Constant to hold the value of 11.
	 */
	public static final int DIGIT_ELEVEN = 11;
	/**
	 * Constant to hold the value of Integer Number One.
	 */
	public static final int INTEGER_NUMBER_ONE = 1;
	/**
     * constant to hold the constant for ownerId.
     */
    public static final String PAYPAL_EMAIL_ID = "email";
    /**
     * constant to hold the constant for TIME_ZONE_GMT.
     */
    public static final String TIME_ZONE_GMT = "GMT";
    /**
	 * The Constant for ADDRESS1_PARAM which was used in TRUShippingPricingEngine.
	 */
	public static final String ADDRESS1_PARAM="Address1";
	
	/**
     * Constant for ADDRESS_PARAM.
     */
    public static final String ADDRESS_PARAM="Address";
	
	/**
	 * The Constant for STATE_PARAM which was used in TRUShippingPricingEngine.
	 */
	public static final String STATE_PARAM = "State";
	/**
	 * The Constant for ORDER_PARAM which was used in TRUShippingPricingEngine.
	 */
	public static final String ORDER_PARAM = "Order";
	
	/**
	 * The Constant for DEFAULT_ADDRESS_PARAM which was used in shipping form.
	 */
	public static final String DEFAULT_ADDRESS_PARAM = "defaultAddress";
	
	/**
	 * The Constant for SHIPPINGGROUPCONTAINER_PARAM which was used in TRUShippingPricingEngine.
	 */
	public static final String SHIPPINGGROUPCONTAINER_PARAM = "ShippingGroupMapContainer";
	
	/**
	 * The Constant for TOKEN_NOT_EXIST which.
	 */
	public static final String TOKEN_NOT_EXIST = "notTokenExist";
	
	/** The Constant PAYPAL_SET_EXPRESS_CHECKOUT. */
	public static final String PAYPAL_SET_EXPRESS_CHECKOUT = "PAYPAL_SET_EXPRESS_CHECKOUT";
	
	/** The Constant PAYPAL_GET_EXPRESS_CHECKOUT. */
	public static final String PAYPAL_GET_EXPRESS_CHECKOUT= "PAYPAL_GET_EXPRESS_CHECKOUT";
	
	/** The Constant PAYPAL_DO_EXPRESS_CHECKOUT. */
	public static final String PAYPAL_DO_EXPRESS_CHECKOUT= "PAYPAL_DO_EXPRESS_CHECKOUT";
	
	/** The Constant PAYPAL_DO_AUTHORIZATION. */
	public static final String PAYPAL_DO_AUTHORIZATION= "PAYPAL_DO_AUTHORIZATION";
	
	/** The Constant PAYPAL_DO_VOID. */
	public static final String PAYPAL_DO_VOID= "PAYPAL_DO_VOID";
	
	/**
     * Constant to hold PAYPAL_HANDLER_EXPRESS_CHECKOUT.
     */
	public static final String PAYPAL_SET_HANDLER_EXPRESS_CHECKOUT = "handleSetExpressCheckoutWithPayPal";
	/**
     * Constant to hold STR_PAY_PAL.
     */
	public static final CharSequence STR_PAY_PAL = "Pay Pal";
	
	/**
     * Constant to hold the int value 3.
     */
    public static final int THREE = 3;
    
    /**
     * Constant to hold DISABLED.
     */
	public static final String DISABLED = "disabled";
	  /**
     * Constant to hold EMPTY_STRING.
     */
	public static final String EMPTY_STRING = "";
	
	

}
