package com.tru.radial.commerce.payment.giftcard;

import javax.xml.bind.JAXBElement;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.StoredValueBalanceReplyType;
import com.tru.radial.payment.StoredValueBalanceRequestType;
import com.tru.radial.payment.giftcard.bean.GiftCardBalanceRequest;
import com.tru.radial.payment.giftcard.bean.GiftCardBalanceResponse;

/**
 * The Class GiftCardBalanceRequestBuilder.
 */
public class GiftCardBalanceRequestBuilder extends AbstractRequestBuilder {

	/** The Constant mLogger. */
	public static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(GiftCardBalanceRequestBuilder.class);
	
	
	/** The m gift card balance request. */
	private StoredValueBalanceRequestType mGiftCardBalanceRequest = null;
	
	/** The m gift card balance response. */
	private StoredValueBalanceReplyType mGiftCardBalanceResponse = null;
	
	
	

	/** The m request object. */
	private JAXBElement<StoredValueBalanceRequestType> mRequestObject= null;
	
	
	/**
	 * Instantiates a new gift card balance request builder.
	 */
	public GiftCardBalanceRequestBuilder() {
		mGiftCardBalanceRequest = getRadialObjectFactory().createStoredValueBalanceRequestType();
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pGiftCardBalanceRequest)
			throws TRURadialRequestBuilderException {		
		GiftCardBalanceRequest balanceRequest = (GiftCardBalanceRequest)pGiftCardBalanceRequest;		
		if (mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRURadialPaymentProcessor  (constructGiftCardRequest) : Start");
		}
		mGiftCardBalanceRequest.setCurrencyCode(ISOCurrencyCodeType.USD);		
		PaymentAccountUniqueIdType paymentAccountUniqueIdType = getRadialObjectFactory().createPaymentAccountUniqueIdType();		
		paymentAccountUniqueIdType.setIsToken(false);		
		paymentAccountUniqueIdType.setValue(balanceRequest.getGiftCardNumber());		
		mGiftCardBalanceRequest.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		mGiftCardBalanceRequest.setPin(balanceRequest.getPin());		
		setRequestObject(getRadialObjectFactory().createStoredValueBalanceRequest(mGiftCardBalanceRequest));		
		if (mLogger.isLoggingDebug()) {
			mLogger.logDebug("TRURadialPaymentProcessor  (constructGiftCardRequest) : End");
		}
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pPaymentResponse){
		if (!(pPaymentResponse instanceof GiftCardBalanceResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardBalanceResponse response = (GiftCardBalanceResponse)pPaymentResponse;
		if(IRadialConstants.RADIAL_SUCCESS_RESPONSE_CODE.equalsIgnoreCase(getGiftCardBalanceResponse().getResponseCode().value())){
			response.setResponseCode(getGiftCardBalanceResponse().getResponseCode().value());
			response.setBalanceAmount(getGiftCardBalanceResponse().getBalanceAmount().getValue());
			response.setSuccess(Boolean.TRUE);		
		}else{
			buildErrorResponse(pPaymentResponse);
		}	
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.AbstractRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pPaymentResponse) {
		if (!(pPaymentResponse instanceof GiftCardBalanceResponse)) {
			throw new UnsupportedOperationException();
		}
		GiftCardBalanceResponse response = (GiftCardBalanceResponse)pPaymentResponse;		
		super.buildErrorResponse(response,getGiftCardBalanceResponse());
		response.setTimeStamp(getTimeStamp());
		
	}

	/**
	 * Gets the gift card balance request.
	 *
	 * @return the gift card balance request
	 */
	public StoredValueBalanceRequestType getGiftCardBalanceRequest() {
		return mGiftCardBalanceRequest;
	}

	/**
	 * Sets the gift card balance request.
	 *
	 * @param pGiftCardBalanceRequest the new gift card balance request
	 */
	public void setGiftCardBalanceRequest(
			StoredValueBalanceRequestType pGiftCardBalanceRequest) {
		this.mGiftCardBalanceRequest = pGiftCardBalanceRequest;
	}

	/**
	 * Gets the gift card balance response.
	 *
	 * @return the gift card balance response
	 */
	public StoredValueBalanceReplyType getGiftCardBalanceResponse() {
		return mGiftCardBalanceResponse;
	}

	/**
	 * Sets the gift card balance response.
	 *
	 * @param pGiftCardBalanceResponse the new gift card balance response
	 */
	public void setGiftCardBalanceResponse(
			StoredValueBalanceReplyType pGiftCardBalanceResponse) {
		this.mGiftCardBalanceResponse = pGiftCardBalanceResponse;
	}

	

	/**
	 * Gets the request object.
	 *
	 * @return the mRequestObject
	 */
	public JAXBElement<StoredValueBalanceRequestType> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestObject the mRequestObject to set
	 */
	public void setRequestObject(
			JAXBElement<StoredValueBalanceRequestType> pRequestObject) {
		this.mRequestObject = pRequestObject;
	}

	
}
