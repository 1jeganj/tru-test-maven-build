package com.tru.radial.integration.payment.paypal;

import java.util.Map;

import com.tru.radial.payment.paypal.bean.PayPalRequest;


/**
 * This interface contains map method to populate any other fields required.
 * @version 1.0
 * @author PA
 * @version 1.0
 */
public interface IPaypalProviderMapper {
	
	/**
	 * 
	 * @param pPluginRequest -IPaymentPluginRequest type object.
	 * @return map object
	 */
	 Map<String,Object> map(PayPalRequest pPluginRequest);

}
