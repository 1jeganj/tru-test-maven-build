package com.tru.radial.integration.payment.paypal;
/**
 * This class is PaypalUtilTestScenarioImpl is used to test scenario for paypal.
 * @author PA
 * @version 1.0 
 * 
 */
public class PaypalUtilTestScenarioImpl {
	/**
	 * Property to hold DoExpressCheckoutErrorItemAmt.
	 */
	private String mDoExpressCheckoutErrorItemAmt;
	/**
	 * Property to hold DoExpressCheckoutErrorTaxAmt.
	 */
	private String mDoExpressCheckoutErrorTaxAmt;
	/**
	 * Property to hold DoExpressCheckoutErrorShippingAmt.
	 */
	private String mDoExpressCheckoutErrorShippingAmt;
	/**
	 * Property to hold EnablePayPalServiceFailure.
	 */
	private boolean mEnablePayPalServiceFailure;
	/**
	 * Property to hold EnableSetExpressCheckoutFailure.
	 */
	private boolean mEnableSetExpressCheckoutFailure;
	/**
	 * Property to hold EnableGetExpressCheckoutFailure.
	 */
	private boolean mEnableGetExpressCheckoutFailure;
	/**
	 * Property to hold EnableDoExpressCheckoutPaymentFailure.
	 */
	private boolean mEnableDoExpressCheckoutPaymentFailure;
	/**
	 * Property to hold EnableDoAuthorizationFailure.
	 */
	private boolean mEnableDoAuthorizationFailure;
	/**
	 * Property to hold EnableDoVoidFailure.
	 */
	private boolean mEnableDoVoidFailure;
	/**
	 * Property to hold FailureApiUserName.
	 */
	private String mFailureApiUserName;
	/**
	 * Property to hold FailureApiPassword.
	 */
	private String mFailureApiPassword;
	/**
	 * Property to hold DoAuthorizationErrorAmt.
	 */
	private String mDoVoidErrorAuthorizationId;
	/**
	 * Property to hold ApplyDoExpressCheckoutPaymentError.
	 */
	private boolean mApplyDoVoidError;
	/**
	 * Property to hold DoAuthorizationErrorAmt.
	 */
	private String mDoAuthorizationErrorAmt;
	/**
	 * Property to hold ApplyDoExpressCheckoutPaymentError.
	 */
	private boolean mApplyDoAuthorizationError;
	/**
	 * Property to hold ApplyDoExpressCheckoutPaymentError.
	 */
	private boolean mApplyDoExpressCheckoutPaymentError;
	/**
	 * Property to hold ApplySetExpressError.
	 */
	private String mDoExpressCheckoutErrorAmt;
	/**
	 * Property to hold ApplySetExpressError.
	 */
	private boolean mApplyGetExpressError;
	/**
	 * Property to hold ErrorGetExpressToken.
	 */
	private String mErrorGetExpressToken;
	/**
	 * Property to hold ApplySetExpressError.
	 */
	private boolean mApplySetExpressError;
	/**
	 * Property to hold SetExpressMaxAmount.
	 */
	private String mSetExpressMaxAmount;
	
	
	/**
	 * @return the applySetExpressError.
	 */
	public boolean isApplySetExpressError() {
		return mApplySetExpressError;
	}
	/**
	 * @param pApplySetExpressError the applySetExpressError to set.
	 */
	public void setApplySetExpressError(boolean pApplySetExpressError) {
		mApplySetExpressError = pApplySetExpressError;
	}
	/**
	 * @return the setExpressMaxAmount.
	 */
	public String getSetExpressMaxAmount() {
		return mSetExpressMaxAmount;
	}
	/**
	 * @param pSetExpressMaxAmount the setExpressMaxAmount to set.
	 */
	public void setSetExpressMaxAmount(String pSetExpressMaxAmount) {
		mSetExpressMaxAmount = pSetExpressMaxAmount;
	}
	/**
	 * @return the applyGetExpressError.
	 */
	public boolean isApplyGetExpressError() {
		return mApplyGetExpressError;
	}
	/**
	 * @param pApplyGetExpressError the applyGetExpressError to set.
	 */
	public void setApplyGetExpressError(boolean pApplyGetExpressError) {
		mApplyGetExpressError = pApplyGetExpressError;
	}
	/**
	 * @return the errorGetExpressToken.
	 */
	public String getErrorGetExpressToken() {
		return mErrorGetExpressToken;
	}
	/**
	 * @param pErrorGetExpressToken the errorGetExpressToken to set.
	 */
	public void setErrorGetExpressToken(String pErrorGetExpressToken) {
		mErrorGetExpressToken = pErrorGetExpressToken;
	}
	/**
	 * @return the applyDoExpressCheckoutPaymentError.
	 */
	public boolean isApplyDoExpressCheckoutPaymentError() {
		return mApplyDoExpressCheckoutPaymentError;
	}
	/**
	 * @param pApplyDoExpressCheckoutPaymentError the applyDoExpressCheckoutPaymentError to set.
	 */
	public void setApplyDoExpressCheckoutPaymentError(
			boolean pApplyDoExpressCheckoutPaymentError) {
		mApplyDoExpressCheckoutPaymentError = pApplyDoExpressCheckoutPaymentError;
	}
	/**
	 * @return the doExpressCheckoutErrorAmt.
	 */
	public String getDoExpressCheckoutErrorAmt() {
		return mDoExpressCheckoutErrorAmt;
	}
	/**
	 * @param pDoExpressCheckoutErrorAmt the doExpressCheckoutErrorAmt to set.
	 */
	public void setDoExpressCheckoutErrorAmt(String pDoExpressCheckoutErrorAmt) {
		mDoExpressCheckoutErrorAmt = pDoExpressCheckoutErrorAmt;
	}
	/**
	 * @return the applyDoAuthorizationError.
	 */
	public boolean isApplyDoAuthorizationError() {
		return mApplyDoAuthorizationError;
	}
	/**
	 * @param pApplyDoAuthorizationError the applyDoAuthorizationError to set.
	 */
	public void setApplyDoAuthorizationError(boolean pApplyDoAuthorizationError) {
		mApplyDoAuthorizationError = pApplyDoAuthorizationError;
	}
	/**
	 * @return the doAuthorizationErrorAmt.
	 */
	public String getDoAuthorizationErrorAmt() {
		return mDoAuthorizationErrorAmt;
	}
	/**
	 * @param pDoAuthorizationErrorAmt the doAuthorizationErrorAmt to set.
	 */
	public void setDoAuthorizationErrorAmt(String pDoAuthorizationErrorAmt) {
		mDoAuthorizationErrorAmt = pDoAuthorizationErrorAmt;
	}
	/**
	 * @return the doVoidErrorAuthorizationId.
	 */
	public String getDoVoidErrorAuthorizationId() {
		return mDoVoidErrorAuthorizationId;
	}
	/**
	 * @param pDoVoidErrorAuthorizationId the doVoidErrorAuthorizationId to set.
	 */
	public void setDoVoidErrorAuthorizationId(String pDoVoidErrorAuthorizationId) {
		mDoVoidErrorAuthorizationId = pDoVoidErrorAuthorizationId;
	}
	/**
	 * @return the applyDoVoidError.
	 */
	public boolean isApplyDoVoidError() {
		return mApplyDoVoidError;
	}
	/**
	 * @param pApplyDoVoidError the applyDoVoidError to set.
	 */
	public void setApplyDoVoidError(boolean pApplyDoVoidError) {
		mApplyDoVoidError = pApplyDoVoidError;
	}
	/**
	 * @return the enablePayPalServiceFailure.
	 */
	public boolean isEnablePayPalServiceFailure() {
		return mEnablePayPalServiceFailure;
	}
	/**
	 * @param pEnablePayPalServiceFailure the enablePayPalServiceFailure to set.
	 */
	public void setEnablePayPalServiceFailure(boolean pEnablePayPalServiceFailure) {
		mEnablePayPalServiceFailure = pEnablePayPalServiceFailure;
	}
	/**
	 * @return the enableSetExpressCheckoutFailure.
	 */
	public boolean isEnableSetExpressCheckoutFailure() {
		return mEnableSetExpressCheckoutFailure;
	}
	/**
	 * @param pEnableSetExpressCheckoutFailure the enableSetExpressCheckoutFailure to set.
	 */
	public void setEnableSetExpressCheckoutFailure(
			boolean pEnableSetExpressCheckoutFailure) {
		mEnableSetExpressCheckoutFailure = pEnableSetExpressCheckoutFailure;
	}
	/**
	 * @return the enableGetExpressCheckoutFailure.
	 */
	public boolean isEnableGetExpressCheckoutFailure() {
		return mEnableGetExpressCheckoutFailure;
	}
	/**
	 * @param pEnableGetExpressCheckoutFailure the enableGetExpressCheckoutFailure to set.
	 */
	public void setEnableGetExpressCheckoutFailure(
			boolean pEnableGetExpressCheckoutFailure) {
		mEnableGetExpressCheckoutFailure = pEnableGetExpressCheckoutFailure;
	}
	/**
	 * @return the enableDoExpressCheckoutPaymentFailure.
	 */
	public boolean isEnableDoExpressCheckoutPaymentFailure() {
		return mEnableDoExpressCheckoutPaymentFailure;
	}
	/**
	 * @param pEnableDoExpressCheckoutPaymentFailure the enableDoExpressCheckoutPaymentFailure to set.
	 */
	public void setEnableDoExpressCheckoutPaymentFailure(
			boolean pEnableDoExpressCheckoutPaymentFailure) {
		mEnableDoExpressCheckoutPaymentFailure = pEnableDoExpressCheckoutPaymentFailure;
	}
	/**
	 * @return the enableDoAuthorizationFailure.
	 */
	public boolean isEnableDoAuthorizationFailure() {
		return mEnableDoAuthorizationFailure;
	}
	/**
	 * @param pEnableDoAuthorizationFailure the enableDoAuthorizationFailure to set.
	 */
	public void setEnableDoAuthorizationFailure(
			boolean pEnableDoAuthorizationFailure) {
		mEnableDoAuthorizationFailure = pEnableDoAuthorizationFailure;
	}
	/**
	 * @return the enableDoVoidFailure.
	 */
	public boolean isEnableDoVoidFailure() {
		return mEnableDoVoidFailure;
	}
	/**
	 * @param pEnableDoVoidFailure the enableDoVoidFailure to set.
	 */
	public void setEnableDoVoidFailure(boolean pEnableDoVoidFailure) {
		mEnableDoVoidFailure = pEnableDoVoidFailure;
	}
	/**
	 * @return the failureApiUserName.
	 */
	public String getFailureApiUserName() {
		return mFailureApiUserName;
	}
	/**
	 * @param pFailureApiUserName the failureApiUserName to set.
	 */
	public void setFailureApiUserName(String pFailureApiUserName) {
		mFailureApiUserName = pFailureApiUserName;
	}
	/**
	 * @return the failureApiPassword.
	 */
	public String getFailureApiPassword() {
		return mFailureApiPassword;
	}
	/**
	 * @param pFailureApiPassword the failureApiPassword to set.
	 */
	public void setFailureApiPassword(String pFailureApiPassword) {
		mFailureApiPassword = pFailureApiPassword;
	}
	/**
	 * @return the doExpressCheckoutErrorItemAmt.
	 */
	public String getDoExpressCheckoutErrorItemAmt() {
		return mDoExpressCheckoutErrorItemAmt;
	}
	/**
	 * @param pDoExpressCheckoutErrorItemAmt the doExpressCheckoutErrorItemAmt to set.
	 */
	public void setDoExpressCheckoutErrorItemAmt(
			String pDoExpressCheckoutErrorItemAmt) {
		mDoExpressCheckoutErrorItemAmt = pDoExpressCheckoutErrorItemAmt;
	}
	/**
	 * @return the doExpressCheckoutErrorTaxAmt.
	 */
	public String getDoExpressCheckoutErrorTaxAmt() {
		return mDoExpressCheckoutErrorTaxAmt;
	}
	/**
	 * @param pDoExpressCheckoutErrorTaxAmt the doExpressCheckoutErrorTaxAmt to set.
	 */
	public void setDoExpressCheckoutErrorTaxAmt(String pDoExpressCheckoutErrorTaxAmt) {
		mDoExpressCheckoutErrorTaxAmt = pDoExpressCheckoutErrorTaxAmt;
	}
	/**
	 * @return the doExpressCheckoutErrorShippingAmt.
	 */
	public String getDoExpressCheckoutErrorShippingAmt() {
		return mDoExpressCheckoutErrorShippingAmt;
	}
	/**
	 * @param pDoExpressCheckoutErrorShippingAmt the doExpressCheckoutErrorShippingAmt to set.
	 */
	public void setDoExpressCheckoutErrorShippingAmt(
			String pDoExpressCheckoutErrorShippingAmt) {
		mDoExpressCheckoutErrorShippingAmt = pDoExpressCheckoutErrorShippingAmt;
	}
}
