package com.tru.radial.integration.payment.paypal;

import java.util.Map;

import com.tru.radial.payment.paypal.bean.PayPalRequest;


/**
 * This class will implement map method in order to populate any extra field apart from.
 * provided by payment plugin component.
 * @version 1.0
 * @author PA
 *  @version 1.0
 */
public class DefaultPaypalProviderMapper implements IPaypalProviderMapper {

	/**
	 * This method will be used to populate extra fields.
	 *  @param pPluginRequest - IPaymentPluginRequest object to hold all the required fields.
	 *  @return Map 
	 */
	@Override
	public Map<String, Object> map(PayPalRequest pPluginRequest) {
		
		return null;
	}

}
