package com.tru.radial.integration.inventory.bean;

import java.math.BigInteger;
import java.util.Map;

import com.tru.radial.common.beans.AbstractRadialResponse;
import com.tru.radial.common.beans.IRadialResponse;

/**
 * This class has methods to set request properties for Soft Allocation
 * @author PA
 *  @version 1.0
 */
public class RadialInventoryResponse extends AbstractRadialResponse implements IRadialResponse {

	/** The Allocation response. */
	private Map<String, BigInteger> mAllocationResponse;

	/** The Reservation id. */
	private BigInteger mReservationId;

	/**
	 * hold to property transactionSuccess.
	 */
	private boolean mTransactionSuccess;
	
	/**
	 * Gets the allocation response.
	 *
	 * @return the allocation response
	 */
	public Map<String, BigInteger> getAllocationResponse() {
		return mAllocationResponse;
	}
	/**
	 * @param pAllocationResponse the allocationResponse to set
	 */
	public void setAllocationResponse(Map<String, BigInteger> pAllocationResponse) {
		mAllocationResponse = pAllocationResponse;
	}
	/**
	 * @return the reservationId
	 */
	public BigInteger getReservationId() {
		return mReservationId;
	}
	/**
	 * @param pReservationId the reservationId to set
	 */
	public void setReservationId(BigInteger pReservationId) {
		mReservationId = pReservationId;
	}
	/**
	 * @return the transactionSuccess
	 */
	public boolean isTransactionSuccess() {
		return mTransactionSuccess;
	}
	/**
	 * @param pTransactionSuccess the transactionSuccess to set
	 */
	public void setTransactionSuccess(boolean pTransactionSuccess) {
		mTransactionSuccess = pTransactionSuccess;
	}

	@Override
	public void setResponseCode(String pValue) {
		super.mResponseCode = pValue;
	}	
	
	@Override
	public void setSuccess(boolean pValue) {
		mTransactionSuccess = pValue;
	}


	@Override
	public void setTimeoutReponse(boolean pTmeoutReponse) {
		super.mIsTimeOutResponse = Boolean.FALSE;
	}
	
	
}
