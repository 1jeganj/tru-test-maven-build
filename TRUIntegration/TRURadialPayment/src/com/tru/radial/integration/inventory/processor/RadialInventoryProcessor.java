package com.tru.radial.integration.inventory.processor;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;
import com.tru.radial.integration.inventory.bean.RadialInventoryResponse;
import com.tru.radial.integration.inventory.builder.RadialInventoryBuilder;
import com.tru.radial.inventory.AllocationResponseMessage;
import com.tru.radial.payment.FaultResponseType;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;

/**
 * This interface has methods to Soft Allocation
 * @version 1.0
 * @author PA
 * @version 1.0
 */
public class RadialInventoryProcessor extends TRURadialBaseProcessor {

	/** The Constant SOFT_ALLOCATIONS. */
	private  static final String SOFT_ALLOCATIONS = "softAllocations";

	
	/** The Constant MAX_RETRY_COUNT. */
	private static final int MAX_RETRY_COUNT = 2;
	
	/**
	 * radial soft allocation.
	 *
	 * @param pRadialInventoryRequest the radial inventory request
	 * @return the radial inventory response
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public IRadialResponse radialSoftAllocations(RadialInventoryRequest pRadialInventoryRequest) throws TRURadialIntegrationException {
		//construct request
		IRadialResponse radialInventoryResponse = new RadialInventoryResponse();
		getClass().getClassLoader().getResource(this.toString());
		RadialInventoryBuilder radialInventoryBuilder = new RadialInventoryBuilder();
		long starTime = 0;
		long endTime = 0;
		try {
			radialInventoryBuilder.buildRequest(pRadialInventoryRequest);
			String xmlString = transformToXML(radialInventoryBuilder.pGetRequestObject(), IRadialConstants.INVENTORY_PAKAGE_NAME);
			if(isLoggingDebug()){
				vlogDebug("{0}", xmlString);
			}
			
			RadialInventoryRequest lRadialInventoryRequest;
			lRadialInventoryRequest = (RadialInventoryRequest)pRadialInventoryRequest;
			starTime = Calendar.getInstance().getTimeInMillis();
			getIntegrationInfoLogger().logIntegrationEntered(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_INVENTORY), getIntegrationInfoLogger().getInterfaceNameMap().get(SOFT_ALLOCATIONS), lRadialInventoryRequest.getOrderId(), null);
			RadialGatewayResponse response = executeRequest(xmlString, SOFT_ALLOCATIONS, MAX_RETRY_COUNT);
			endTime = Calendar.getInstance().getTimeInMillis();

			if(response == null) {
				radialInventoryBuilder.buildErrorResponse(radialInventoryResponse);
				if(isLoggingDebug()){
					vlogDebug("radialSoftAllocations reponse is null");
				}
				Map<String, String> extra = new ConcurrentHashMap<String, String>();
				extra.put(IRadialConstants.OPERATION_NAME,  SOFT_ALLOCATIONS);
				//extra.put(IRadialConstants.START_TIME, String.valueOf(starTime));
				extra.put(IRadialConstants.STR_TIME_TAKEN, String.valueOf(endTime-starTime));
				extra.put(IRadialConstants.STR_REPPONSE_CODE,IRadialConstants.STRING_ZERO);
				getAlertLogger().logFailure(IRadialConstants.STR_INVENTORY_PROCESSOR, SOFT_ALLOCATIONS, extra);
				return radialInventoryResponse;
			} 
			if(isLoggingDebug()){
				vlogDebug("{0}",radialInventoryBuilder.formatXML(response.getResponseBody()));
			}
			if (response !=null && response.getStatusCode() != IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				radialInventoryBuilder.setFaultResponseType(transformToObject(response.getResponseBody(), FaultResponseType.class));
				radialInventoryBuilder.buildFaultResponse(radialInventoryResponse);
				Map<String, String> extra = new ConcurrentHashMap<String, String>();
				extra.put(IRadialConstants.OPERATION_NAME,  SOFT_ALLOCATIONS);
			    //extra.put(IRadialConstants.START_TIME, String.valueOf(starTime));
				extra.put(IRadialConstants.STR_TIME_TAKEN, String.valueOf(endTime-starTime));
				extra.put(IRadialConstants.STR_REPPONSE_CODE,String.valueOf(response.getStatusCode()));
				getAlertLogger().logFailure(IRadialConstants.STR_INVENTORY_PROCESSOR, SOFT_ALLOCATIONS, extra);
			} else if (response !=null && response.getStatusCode() == IRadialConstants.HTTP_SUCCESS_STATE  && !response.getResponseBody().isEmpty() ) {
				getIntegrationInfoLogger().logIntegrationExited(getIntegrationInfoLogger().getServiceNameMap().get(IRadialConstants.RADIAL_INVENTORY), getIntegrationInfoLogger().getInterfaceNameMap().get(SOFT_ALLOCATIONS), lRadialInventoryRequest.getOrderId(), null);
				radialInventoryBuilder.setAllocationResponseMessage(transformToObject(response.getResponseBody(), AllocationResponseMessage.class));
				radialInventoryBuilder.buildResponse(radialInventoryResponse);
			}
		} catch (TRURadialIntegrationException exe) {
			endTime = Calendar.getInstance().getTimeInMillis();
			Map<String, String> extra = new ConcurrentHashMap<String, String>();
			extra.put(IRadialConstants.OPERATION_NAME,  SOFT_ALLOCATIONS);
			extra.put(IRadialConstants.STR_TIME_TAKEN, String.valueOf(endTime-starTime));
			if(!StringUtils.isBlank(exe.getErrorMessage())){
				extra.put(IRadialConstants.STR_ERROR_MSG,exe.getErrorMessage());
			}
			getAlertLogger().logFailure(IRadialConstants.STR_INVENTORY_PROCESSOR, SOFT_ALLOCATIONS, extra);
			throw exe;
		} 
		return radialInventoryResponse;
	}
}
