package com.tru.radial.integration.inventory.builder;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.tru.radial.common.AbstractRequestBuilder;
import com.tru.radial.common.beans.IRadialRequest;
import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialRequestBuilderException;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;
import com.tru.radial.integration.inventory.bean.RadialInventoryResponse;
import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.inventory.AllocationRequestMessage;
import com.tru.radial.inventory.AllocationResponse;
import com.tru.radial.inventory.AllocationResponseMessage;
import com.tru.radial.inventory.ObjectFactory;
import com.tru.radial.inventory.OrderItem;
import com.tru.radial.payment.FaultResponseType;

/**
 * @author PA
 * The Class PayPalSetExpressRequestBuilder.
 */
public class RadialInventoryBuilder extends AbstractRequestBuilder {

	/** The m radial object factory. */
	private ObjectFactory mRadialInventoryObjectFactory = new ObjectFactory();
	
	/** The m set express checkout request type. */
	AllocationRequestMessage mAllocationRequestMessage = null;
	
	/** The m set express checkout reply type. */
	AllocationResponseMessage mAllocationResponseMessage = null;
	
	/** The m fault response type. */
	FaultResponseType mFaultResponseType = null;

	/** The m request object. */
	JAXBElement<AllocationRequestMessage> mRequestObject= null;
	
	/** The m response. */
	RadialInventoryResponse mResponse = null;
	
	/**
	 * Instantiates a new pay pal set express request builder.
	 */
	public RadialInventoryBuilder(){ 		
		initAllocationRequestMessage();
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildRequest(com.tru.radial.common.beans.IRadialRequest)
	 */
	@Override
	public void buildRequest(IRadialRequest pRadialRequest)throws TRURadialRequestBuilderException {
		RadialInventoryRequest lRadialInventoryRequest;
		if (!(pRadialRequest instanceof RadialInventoryRequest)) {
			throw new UnsupportedOperationException();
		}
		lRadialInventoryRequest = (RadialInventoryRequest)pRadialRequest;
		mAllocationRequestMessage.setRequestId(lRadialInventoryRequest.getOrderId());
		Map<String, Long> allocationRequest = lRadialInventoryRequest.getAllocationRequest();
		int lineId = TRURadialConstants.INT_ZERO;
		for (String itemId : allocationRequest.keySet()) {
			OrderItem createOrderItem = getRadialInventoryObjectFactory().createOrderItem();
			createOrderItem.setLineId(new BigInteger(Integer.toString(++lineId)));
			createOrderItem.setItemId(itemId);
			createOrderItem.setQuantity(new BigInteger(allocationRequest.get(itemId).toString()));
			mAllocationRequestMessage.getOrderItem().add(createOrderItem);
		}
		setRequestObject(getRadialInventoryObjectFactory().createAllocationRequestMessage(mAllocationRequestMessage));
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildResponse(IRadialResponse pRadialResponse) {
		if (!(pRadialResponse instanceof RadialInventoryResponse)) {
			throw new UnsupportedOperationException();
		}
		RadialInventoryResponse response = (RadialInventoryResponse)pRadialResponse;
		if(mAllocationResponseMessage.getReservationId() != null){
		response.setReservationId(mAllocationResponseMessage.getReservationId());
		List<AllocationResponse> radialAllocationResponse = mAllocationResponseMessage.getAllocationResponse();
		
		Map<String, BigInteger> truAllocationResponse = new HashMap<String, BigInteger>();
		
		for (AllocationResponse allocationResponse : radialAllocationResponse) {
			truAllocationResponse.put(allocationResponse.getItemId(), allocationResponse.getAmountAllocated());
		}
		response.setAllocationResponse(truAllocationResponse);
		response.setTransactionSuccess(Boolean.TRUE);
		}
		else{
			response.setTransactionSuccess(Boolean.FALSE);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildFaultResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildFaultResponse(IRadialResponse pRadialResponse) {		
		if (!(pRadialResponse instanceof RadialInventoryResponse)) {
			throw new UnsupportedOperationException();
		}
		RadialInventoryResponse response = (RadialInventoryResponse)pRadialResponse;
		response.setTransactionSuccess(Boolean.FALSE);
	}

	/* (non-Javadoc)
	 * @see com.tru.radial.common.IRadialRequestBuilder#buildErrorResponse(com.tru.radial.common.beans.IRadialResponse)
	 */
	@Override
	public void buildErrorResponse(IRadialResponse pRadialResponse) {
		if (!(pRadialResponse instanceof RadialInventoryResponse)) {
			throw new UnsupportedOperationException();
		}
		RadialInventoryResponse response = (RadialInventoryResponse)pRadialResponse;
		response.setTransactionSuccess(Boolean.FALSE);
	}
	

	/**
	 * Inits the allocation request message.
	 */
	private void initAllocationRequestMessage(){
		mAllocationRequestMessage = createAllocationRequestMessage();
	}
	
	/**
	 * Creates the pay pal set express checkout request.
	 *
	 * @return the pay pal set express checkout request type
	 */
	private AllocationRequestMessage createAllocationRequestMessage() {
		return getRadialInventoryObjectFactory().createAllocationRequestMessage();
	}


	/**
	 * Gets the request object.
	 *
	 * @return the request object
	 */
	public JAXBElement<AllocationRequestMessage> pGetRequestObject() {
		return mRequestObject;
	}

	/**
	 * Sets the request object.
	 *
	 * @param pRequestType the new request object
	 */
	public void setRequestObject(
			JAXBElement<AllocationRequestMessage> pRequestType) {
		this.mRequestObject = pRequestType;
	}

	/**
	 * Gets the fault response type.
	 *
	 * @return the fault response type
	 */
	public FaultResponseType getFaultResponseType() {
		return mFaultResponseType;
	}

	/**
	 * Sets the fault response type.
	 *
	 * @param pFaultResponseType the new fault response type
	 */
	public void setFaultResponseType(FaultResponseType pFaultResponseType) {
		this.mFaultResponseType = pFaultResponseType;
	}

	/**
	 * @return the allocationRequestMessage
	 */
	public AllocationRequestMessage getAllocationRequestMessage() {
		return mAllocationRequestMessage;
	}

	/**
	 * @param pAllocationRequestMessage the allocationRequestMessage to set
	 */
	public void setAllocationRequestMessage(
			AllocationRequestMessage pAllocationRequestMessage) {
		mAllocationRequestMessage = pAllocationRequestMessage;
	}

	/**
	 * @return the allocationResponseMessage
	 */
	public AllocationResponseMessage getAllocationResponseMessage() {
		return mAllocationResponseMessage;
	}

	/**
	 * @param pAllocationResponseMessage the allocationResponseMessage to set
	 */
	public void setAllocationResponseMessage(
			AllocationResponseMessage pAllocationResponseMessage) {
		mAllocationResponseMessage = pAllocationResponseMessage;
	}

	/**
	 * @return the response
	 */
	public RadialInventoryResponse getResponse() {
		return mResponse;
	}

	/**
	 * @param pResponse the response to set
	 */
	public void setResponse(RadialInventoryResponse pResponse) {
		mResponse = pResponse;
	}

	/**
	 * @return the requestObject
	 */
	public JAXBElement<AllocationRequestMessage> getRequestObject() {
		return mRequestObject;
	}

	/**
	 * @return the radialInventoryObjectFactory
	 */
	public ObjectFactory getRadialInventoryObjectFactory() {
		return mRadialInventoryObjectFactory;
	}

	/**
	 * @param pRadialInventoryObjectFactory the radialInventoryObjectFactory to set
	 */
	public void setRadialInventoryObjectFactory(
			ObjectFactory pRadialInventoryObjectFactory) {
		mRadialInventoryObjectFactory = pRadialInventoryObjectFactory;
	}

}
