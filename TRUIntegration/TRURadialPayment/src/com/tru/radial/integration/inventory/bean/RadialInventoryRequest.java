package com.tru.radial.integration.inventory.bean;

import java.util.Map;

import com.tru.radial.common.beans.IRadialRequest;

/**
 * This class has methods to set request properties for Soft Allocation
 * @author PA
 *  @version 1.0
 */
public class RadialInventoryRequest implements IRadialRequest {
	
	/** The Order id. */
	private String mOrderId;
	
	/** The Allocation details. */
	private Map<String, Long> mAllocationRequest;

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return mOrderId;
	}

	/**
	 * @param pOrderId the orderId to set
	 */
	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/**
	 * @return the allocationRequest
	 */
	public Map<String, Long> getAllocationRequest() {
		return mAllocationRequest;
	}

	/**
	 * @param pAllocationRequest the allocationRequest to set
	 */
	public void setAllocationRequest(Map<String, Long> pAllocationRequest) {
		mAllocationRequest = pAllocationRequest;
	}
}
