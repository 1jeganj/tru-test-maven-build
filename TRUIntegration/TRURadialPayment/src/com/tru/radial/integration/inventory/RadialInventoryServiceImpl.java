package com.tru.radial.integration.inventory;

import atg.nucleus.GenericService;

import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;
import com.tru.radial.integration.inventory.processor.RadialInventoryProcessor;

/**
 * This interface has methods to Soft Allocation
 * @version 1.0
 * @author PA
 * @version 1.0
 */
public class RadialInventoryServiceImpl extends GenericService implements RadialInventoryService {
	private RadialInventoryProcessor mRadialInventoryProcessor;
	
	/**
	 * @return the radialInventoryProcessor
	 */
	public RadialInventoryProcessor getRadialInventoryProcessor() {
		return mRadialInventoryProcessor;
	}

	/**
	 * @param pRadialInventoryProcessor the radialInventoryProcessor to set
	 */
	public void setRadialInventoryProcessor(
			RadialInventoryProcessor pRadialInventoryProcessor) {
		mRadialInventoryProcessor = pRadialInventoryProcessor;
	}

	/**
	 * radial soft allocations.
	 *
	 * @param pRadialInventoryRequest the radial inventory request
	 * @return the radial inventory response
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	public IRadialResponse radialSoftAllocations(RadialInventoryRequest pRadialInventoryRequest) throws TRURadialIntegrationException {
		return getRadialInventoryProcessor().radialSoftAllocations(pRadialInventoryRequest);
	}
}
