package com.tru.radial.integration.inventory;

import com.tru.radial.common.beans.IRadialResponse;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.integration.inventory.bean.RadialInventoryRequest;

/**
 * This interface has methods to Soft Allocation
 * @version 1.0
 * @author PA
 * @version 1.0
 */
public interface RadialInventoryService {

	/**
	 * Radial soft allocations.
	 *
	 * @param pRadialInventoryRequest the radial inventory request
	 * @return the radial inventory response
	 * @throws TRURadialIntegrationException the TRU radial integration exception
	 */
	IRadialResponse radialSoftAllocations(RadialInventoryRequest pRadialInventoryRequest) throws TRURadialIntegrationException;

}
