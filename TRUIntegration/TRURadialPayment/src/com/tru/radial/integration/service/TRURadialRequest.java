package com.tru.radial.integration.service;

/**
 * This class has methods to set radial service request.
 * @author PA
 * @version 1.0
 */
public class TRURadialRequest {
	
	/**
	 * Holds the property value of mServiceURL.
	 */
	private String mServiceURL;
	
	/**
	 * This method is used to get serviceURL.
	 * @return serviceURL String
	 */
	public String getServiceURL() {
		return mServiceURL;
	}

	/**
	 *This method is used to set serviceURL.
	 *@param pServiceURL String
	 */
	public void setServiceURL(String pServiceURL) {
		mServiceURL = pServiceURL;
	}

	/**
	 * This method is used to get requestString.
	 * @return requestString String
	 */
	public String getRequestString() {
		return mRequestString;
	}

	/**
	 *This method is used to set requestString.
	 *@param pRequestString String
	 */
	public void setRequestString(String pRequestString) {
		mRequestString = pRequestString;
	}

	/**
	 * Holds the property value of mRequestString.
	 */
	private String mRequestString;
	
	
	

}
