package com.tru.radial.integration.util;

import atg.core.util.Address;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.payment.PayPalAddressType;
import com.tru.radial.payment.PayPalGetExpressCheckoutReplyType;
import com.tru.radial.payment.PhysicalAddressType;

/**
 * The Class RadialAddressUtil.
 */
public class RadialAddressUtil {

	/**
	 * This method copy the paypal format address to ATG OOTB address object.
	 * 
	 * @param pAddressType
	 *            the address type
	 * @return the address
	 */
	public static Address copyAddress(PayPalAddressType pAddressType) {
		if (pAddressType == null) {
			return null;
		}
		ContactInfo address = new ContactInfo();
		address.setAddress1(pAddressType.getLine1());
		address.setAddress2(pAddressType.getLine2());
		address.setAddress3(pAddressType.getLine3());
		address.setCity(pAddressType.getCity());
		address.setState(pAddressType.getMainDivision());
		address.setCountry(pAddressType.getCountryCode());
		address.setPostalCode(pAddressType.getPostalCode());
		return address;
	}

	/**
	 * Copy address.
	 * 
	 * @param pAddressType
	 *            the address type
	 * @return the physical address type
	 */
	public static PhysicalAddressType copyAddress(Address pAddressType) {
		if (pAddressType == null) {
			return null;
		}
		PhysicalAddressType address = new PhysicalAddressType();
		address.setLine1(pAddressType.getAddress1());
		if (StringUtils.isNotBlank(pAddressType.getAddress2())) {
			address.setLine2(pAddressType.getAddress2());
		}
		if (StringUtils.isNotBlank(pAddressType.getAddress3())) {
			address.setLine3(pAddressType.getAddress3());
		}
		address.setCity(pAddressType.getCity());
		address.setMainDivision(pAddressType.getState());
		address.setCountryCode(pAddressType.getCountry());
		address.setPostalCode(pAddressType.getPostalCode());
		return address;
	}

	/**
	 * Gets the shipping address.
	 * 
	 * @param pResponse
	 *            the response
	 * @return the shipping address
	 */
	public static Address getShippingContanctInfo(
			PayPalGetExpressCheckoutReplyType pResponse) {
		ContactInfo address = (ContactInfo) copyAddress(pResponse
				.getShippingAddress());
		if (null == address) {
			return null;
		}
		address.setFirstName(pResponse.getPayerName().getFirstName());
		address.setLastName(pResponse.getPayerName().getLastName());
		address.setEmail(pResponse.getPayerEmail());
		address.setPhoneNumber(pResponse.getPayerPhone().replaceAll(
				IRadialConstants.HYPHEN, IRadialConstants.EMPTY_STRING));
		return address;
	}

	/**
	 * Gets the billing address.
	 * 
	 * @param pResponse
	 *            the response
	 * @return the billing address
	 */
	public static Address getBillingContactInfo(
			PayPalGetExpressCheckoutReplyType pResponse) {
		ContactInfo address = (ContactInfo) copyAddress(pResponse
				.getBillingAddress());
		if (null == address) {
			return null;
		}
		address.setFirstName(pResponse.getPayerName().getFirstName());
		address.setMiddleName(pResponse.getPayerName().getMiddleName());
		address.setLastName(pResponse.getPayerName().getLastName());
		address.setEmail(pResponse.getPayerEmail());
		address.setPhoneNumber(pResponse.getPayerPhone().replaceAll(
				IRadialConstants.HYPHEN, IRadialConstants.EMPTY_STRING));		
		return address;
	}
}
