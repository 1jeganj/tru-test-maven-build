package com.tru.radial.integration.util;

import atg.nucleus.GenericService;
import atg.service.idgen.IdGeneratorException;
import atg.service.idgen.SQLIdGenerator;

/**
 * TRUUIDGenerator is used populate the UID.
 * 
 * @author PA
 * @version 1.0
 *
 */
public class TRUUIDGenerator extends GenericService {

	/**
     * Holds component for IDGenerator.
     */
    private SQLIdGenerator mIdGenerator;
    
    /**
     * @return the idGenerator
     */
    public SQLIdGenerator getIdGenerator() {
        return mIdGenerator;
    }


    /**
     * @param pIdGenerator the pIdGenerator to set
     */
    public void setIdGenerator(SQLIdGenerator pIdGenerator) {
        mIdGenerator = pIdGenerator;
    }
	/**
	 * Holds idspaces space name for paymentID.
	 */
	private String mPaymentIdspaceName;

	/**
	 * Holds value for max number of digits allowed for ID.
	 */
	private int mMaxDigitAllowed;

	/**
	 * Holds max value for Id.
	 */
	private long mMaxIdValue;

	/**
	 * @return the maxDigitAllowed
	 */
	public int getMaxDigitAllowed() {
		return mMaxDigitAllowed;
	}

	/**
	 * @param pMaxDigitAllowed
	 *            the pMaxDigitAllowed to set
	 */
	public void setMaxDigitAllowed(int pMaxDigitAllowed) {
		mMaxDigitAllowed = pMaxDigitAllowed;
	}

	/**
	 * @return the m_lMaxIdValue
	 */
	public long getMaxIdValue() {
		return mMaxIdValue;
	}

	/**
	 * @param pMaxIdValue
	 *            the pMaxIdValue to set
	 */
	public void setMaxIdValue(long pMaxIdValue) {
		mMaxIdValue = pMaxIdValue;
	}

	/**
	 * @return the paymentIdspaceName
	 */
	public String getPaymentIdspaceName() {
		return mPaymentIdspaceName;
	}

	/**
	 * @param pPaymentIdspaceName
	 *            the pPaymentIdspaceName to set
	 */
	public void setPaymentIdspaceName(String pPaymentIdspaceName) {
		mPaymentIdspaceName = pPaymentIdspaceName;
	}

	/**
	 * Method to get Unique payment ID.
	 * 
	 * @return l_lPaymentId long
	 */
	public long getPaymentId() {
		long paymentId = 0;
		try {
			paymentId = getIdGenerator().generateLongId(getPaymentIdspaceName());
		} catch (IdGeneratorException e) {
			if (isLoggingError()) {
				logError("Unable to get UniquePaymentId from IdGenerator.", e);
			}
		}
		if (paymentId > getMaxIdValue()) {
			paymentId = truncateId(paymentId);
		}
		return paymentId;
	}

	/**
	 * Method to get truncated Id.
	 * 
	 * @param pPaymentId
	 *            long
	 * @return paymentID long
	 */
	private long truncateId(long pPaymentId) {
		long paymentID = 0;
		String paymentIdString = String.valueOf(pPaymentId);
		paymentIdString = paymentIdString.trim();
		paymentIdString = paymentIdString.substring(paymentIdString.length()
				- getMaxDigitAllowed());
		paymentID = Long.parseLong(paymentIdString);
		if (paymentID == 0) {
			paymentID = getPaymentId();
		}
		return paymentID;
	}
}
