package com.tru.radial.integration.util;

/**
 * @author Ankit, Pandey {eclipse.ini}
 *
 */
public class TRURadialConstants {
	
	/**
	 * Holds the constant POST.
	 */
	public  final static String STR_POST="POST";
	/**
	 * Holds the constant API KEY.
	 */
	public  final static String STR_APIKEY = "apiKey";
	/**
	 * Holds the constant API KEY.
	 */
	public  final static String STR_CONTENT_TYPE = "Content-Type";
	/**
	 * Holds the constant Content Type Desc.
	 */
	public  final static String STR_CONTENT_TYPE_DESC = "application/json;charset=UTF-8";
	
	/** The Constant for string . */
	public static final String RADIAL_MAR_UNMAR_PACKAGE = "com.tru.radial.payment";
	
	/** The Constant for string jaxb formatted output . */
	public static final String JAXB_FORMATTED_OUTPUT = "jaxb.formatted.output";
	
	/** The Constant for string VISA . */
	public static final String VISA = "Visa";
	
	/** The Constant for string MASTER_CARD . */
	public static final String MASTER_CARD = "Mastercard";
	
	/** The Constant for string DISCOVER . */
	public static final String DISCOVER = "Discover";
	
	/** The Constant for string AMEX . */
	public static final String AMEX = "American Express";
	
	/** The Constant for string EXP_DATE_FORMAT . */
	public static final String EXP_DATE_FORMAT = "yyyy-MM";
	
	/** The Constant for string EMPTY . */
	public static final String EMPTY = "";
	
	/** The Constant for string HYPHEN . */
	public static final String HYPHEN = "-";
	
	/** The Constant for string LAYAWAY_ORDER_TYPE . */
	public static final String LAYAWAY_ORDER_TYPE = "layawayOrder";
	
	/** The Constant for string INT_THREE . */
	public static final int INT_THREE = 3;
	
	/** The Constant for string HYPHEN . */
	public static final String YES_FLAG = "Y";
	
	/** The Constant for string HYPHEN . */
	public static final String DEFAUL_AUTH_STATUS = "A";
	
	/** The Constant for string HYPHEN . */
	public static final String DEFAULT_CAVVUCAF = "gsdsXXggggg";
	
	/** The Constant for string HYPHEN . */
	public static final String DEFAULT_ECI = "05";
	
	/** The Constant for string HYPHEN . */
	public static final String DEFAULT_PAY_AUTH_RESP = "eJydVNtu4jAQ/RVE37oCJ+HSggZLKbQSqrpLuSy8mmSSWAsOjR2g+/U7DhAi1IfdnYdkfDznzLHjGOZJhjiaYZBnyOENtRYx1mQ4qIt1ELpeq13nMPGnqG/BPWZapoq7TafpAbsMSSMLEqEMBxF8PI2/804RwM5D2GI2HvFeNYCdQGBX9iS3mSZXRxnSTPEMhUHuOa7rdD2n5j70272+R/QCh52l+Ns0J/5ju2ubViGgRWaogk+adICVI8DjLlVIFbSOMgd2dbATijtF3BdhM9ImFOYrDkZuq64eratWF1iBgzbC5Jr7wM4ZBGK/58mvxSx6eniXs9l66Ps/li/fPvzVs08xIHO2BDCQ3HHJFL0Llr+J00yaZMtbp5orAMxaYcW34jCTsaJmGdaO243Sg3pizK7P2OFwaB5azTSLmUeLYE6PUUGoZXxXP7EwHKso/SfaUKhUyUBs5G9h6BC8oUnSsFZ6+0pmPrVKLps+Dxsk1QjctmpYxGm5HdJkX4tWVvY3XW7NZlo0dCJc2+BGiMMUI7QnAmuL6XhQvyu2UvXyaCmj9dqPwsXoJTq8TpbEHskYtfkfC5f2VYWL3k+xybGcu4xKX2fTpy9U2YlL4S3wip+nylXH6Y2EEVRTScvpK7H4A4s7wJ6n6t3wB1RLYIQ=";
	
	/** The Constant for string DEFAULT_IP . */
	public static final String DEFAULT_IP = "208.247.73.130";
	
	/** The Constant for string DEFAULT_EMAIL . */
	public static final String DEFAULT_EMAIL = "abc@xyz.com";
	
	/** The Constant for integer value ZERO . */
	public static final int INT_ZERO = 0;
	
	/** The Constant for integer value ONE . */
	public static final int INT_ONE = 1;
	
	/** The Constant for string PLCC . */
	public static final String PLCC = "RUSPrivateLabelCard";
	
	/** The Constant for string COBRANDED . */
	public static final String COBRANDED = "RUSCOBrandedCard";
	
	/** The Constant for string RES_CODE_APPROVED . */
	public static final String RES_CODE_APPROVED = "APPROVED";
	
	/** The Constant for string PROMO_CODE_SUCCESS . */
	public static final String PROMO_CODE_SUCCESS = "SUCCESS";
	/** The Constant for string PROMO_CODE_FAIL . */
	public static final String PROMO_CODE_FAIL = "FAIL";
	/** The Constant for string PROMO_CODE_TIMEOUT . */
	public static final String PROMO_CODE_TIMEOUT = "TIMEOUT";
	
	/** The Constant for string PROMO_PACKAGE_NAME . */
	public static final String PROMO_PACKAGE_NAME = "com.tru.radial.promo";
	
	/** The Constant for string PROMO_PLAN_VALUE . */
	public static final String PROMO_PLAN_VALUE ="PROMO";
	
	/** The Constant for INT_TWO . */
	public static final int INT_TWO = 2;
		
	/** The Constant for string TEST1 . */
	public static final String TEST1 = "test1";
	
	/** The Constant for string TEST2 . */
	public static final String TEST2 = "test2"; 
	
	/** The Constant for HUNDRED . */
	public static final double HUNDRED = 100.00;

	/** The Constant DOUBLE_DECIMAL. */
	public static final String DOUBLE_DECIMAL = "0.00";

}
