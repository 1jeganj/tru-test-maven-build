import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.tru.radial.integration.util.TRURadialConstants;
import com.tru.radial.payment.ISOCurrencyCodeType;
import com.tru.radial.payment.ObjectFactory;
import com.tru.radial.payment.PaymentAccountUniqueIdType;
import com.tru.radial.payment.StoredValueBalanceReplyType;
import com.tru.radial.payment.StoredValueBalanceRequestType;

public class RadialGiftCardBalanceTest {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws MalformedURLException, IOException {
		String requestInput = "";
		
		ObjectFactory objectFactory = new ObjectFactory();
		StoredValueBalanceRequestType storedValueBalanceRequestType = objectFactory.createStoredValueBalanceRequestType();
		
		PaymentAccountUniqueIdType paymentAccountUniqueIdType= objectFactory.createPaymentAccountUniqueIdType();
		paymentAccountUniqueIdType.setIsToken(false);
		paymentAccountUniqueIdType.setValue("5896000000000007");
		storedValueBalanceRequestType.setCurrencyCode(ISOCurrencyCodeType.USD);
		storedValueBalanceRequestType.setPaymentAccountUniqueId(paymentAccountUniqueIdType);
		
		JAXBElement<StoredValueBalanceRequestType> createStoredValueBalanceRequest = objectFactory.createStoredValueBalanceRequest(storedValueBalanceRequestType);
		requestInput = marshalXML(createStoredValueBalanceRequest, "com.tru.radial.payment");
		
		System.out.println(formatXML(requestInput));
		HttpURLConnection connection = (HttpURLConnection) new URL("https://tst01-epapi-na.gsipartners.com/v1.0/stores/TRUUS2/payments/storedvalue/balance/VL.xml").openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("apiKey", "6NT7NYhtJB3kW9I02y0GwLI3EWFyUxGh");
		connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);

		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(requestInput.getBytes());
		outputStream.flush();
		connection.connect();

		Object responseMarshalObj = unMarshalXML(connection, "com.tru.radial.payment");

		//Constructing Response
		JAXBElement<StoredValueBalanceReplyType> storedValueBalanceReplyTypeJaxbElement = (JAXBElement<StoredValueBalanceReplyType>) responseMarshalObj;
		
		//Marshaling the Response and printing the Response in xml format
		String responseXML = marshalXML(storedValueBalanceReplyTypeJaxbElement, "com.tru.radial.payment");
		System.out.println("***********Response XML**********\n"+formatXML(responseXML));

		//Casting to the Response object
		StoredValueBalanceReplyType storedValueBalanceReplyType = storedValueBalanceReplyTypeJaxbElement.getValue();
		System.out.println("BalanceAmount----->"+storedValueBalanceReplyType.getBalanceAmount().getValue());
		connection.disconnect();
	}

	public static String formatXML(String input)
	{
		try 
		{
			Document doc = DocumentHelper.parseText(input);  
			StringWriter sw = new StringWriter();  
			OutputFormat format = OutputFormat.createPrettyPrint();  
			format.setIndent(true);
			format.setIndentSize(3); 
			XMLWriter xw = new XMLWriter(sw, format);  
			xw.write(doc);  

			return sw.toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return input;
		}
	}
	
	public  static String marshalXML(Object pJAXBObject, String pPackage) {
		String xmlRequest = null;
		if(pJAXBObject == null){
			return xmlRequest;
		}
		StringWriter sw = new StringWriter();
		try {
			JAXBContext	context = JAXBContext.newInstance(pPackage);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(TRURadialConstants.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
			marshaller.marshal(pJAXBObject, sw);
			xmlRequest = sw.toString();
			sw.close();
		} catch (JAXBException exc) {
			exc.printStackTrace();
		} catch (IOException exc) {
			exc.printStackTrace();
		}
		return xmlRequest;
	}

	public static Object unMarshalXML(HttpURLConnection pConnection, String pPackage) {
		Object unmarshalObj = null;
		InputStream inputStream = null;
		try{
			inputStream = pConnection.getInputStream();
			JAXBContext jaxbContext = JAXBContext.newInstance(pPackage);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			unmarshalObj = jaxbUnmarshaller.unmarshal(inputStream);
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return unmarshalObj;
	}
}
