/*
 * 
 */
package com.tru.feedprocessor.email;

import java.util.List;

/**
 * The Class TRUFeedEnvConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBestSellerFeedEnvConfiguration {

	/** The OS host name. */
	private String mOSHostName;

	/** The detailed exception for no feed. */
	private String mDetailedExceptionForNoFeed;

	/** The detailed exception for invalid feed. */
	private String mDetailedExceptionForInvalidFeed;

	/** The failure mail priority. */
	private String mFailureMailPriority2;

	/** The Attachment item id label. */
	private String mAttachmentItemIdLabel;

	/** The Attachment exception reason label. */
	private String mAttachmentExceptionReasonLabel;

	/** The BestSell files moved to unprocessed folder subject. */
	private String mBestSellerFilesMovedToUnproccessedFolderSubject;

	/** The BestSeller feed message from. */
	private String mBestSellerFeedMessageFrom;
	
	/** The BestSeller feed message to. */
	private List<String> mBestSellerFeedMessageTo;
	
	/** The Best seller success sub. */
	private String mBestSellerSuccessSub;
	
	/** The Best seller error sub. */
	private String mBestSellerErrorSub;
	
	/** The Best seller failure sub. */
	private String mBestSellerFailureSub;
	
	/** The Env. */
	private String mEnv;
	
	/** The Best seller success with errors. */
	private String mBestSellerSuccessWithErrors;
	
	/** The Best seller success. */
	private String mBestSellerSuccess;
	
	/** The Attachment item id label. */
	private String mFileName;
	
	/** The Page identifier label. */
	private String mRecordNumber;
	
	/** The Project name label. */
	private String mTimeStamp;
	
	/**
	 * Gets the best seller files moved to unproccessed folder subject.
	 *
	 * @return the bestSellerFilesMovedToUnproccessedFolderSubject
	 */
	public String getBestSellerFilesMovedToUnproccessedFolderSubject() {
		return mBestSellerFilesMovedToUnproccessedFolderSubject;
	}

	/**
	 * Sets the best seller files moved to unproccessed folder subject.
	 *
	 * @param pBestSellerFilesMovedToUnproccessedFolderSubject the bestSellerFilesMovedToUnproccessedFolderSubject to set
	 */
	public void setBestSellerFilesMovedToUnproccessedFolderSubject(
			String pBestSellerFilesMovedToUnproccessedFolderSubject) {
		mBestSellerFilesMovedToUnproccessedFolderSubject = pBestSellerFilesMovedToUnproccessedFolderSubject;
	}


	/**
	 * Gets the OS host name.
	 * 
	 * @return the OS host name
	 */
	public String getOSHostName() {
		return mOSHostName;
	}

	/**
	 * Sets the OS host name.
	 * 
	 * @param pOSHostName the new OS host name
	 */
	public void setOSHostName(String pOSHostName) {
		mOSHostName = pOSHostName;
	}

	/**
	 * Gets the detailed exception for no feed.
	 * 
	 * @return the detailed exception for no feed
	 */
	public String getDetailedExceptionForNoFeed() {
		return mDetailedExceptionForNoFeed;
	}

	/**
	 * Sets the detailed exception for no feed.
	 * 
	 * @param pDetailedExceptionForNoFeed  the new detailed exception for no feed
	 */
	public void setDetailedExceptionForNoFeed(String pDetailedExceptionForNoFeed) {
		mDetailedExceptionForNoFeed = pDetailedExceptionForNoFeed;
	}

	/**
	 * Gets the detailed exception for invalid feed.
	 * 
	 * @return the detailed exception for invalid feed
	 */
	public String getDetailedExceptionForInvalidFeed() {
		return mDetailedExceptionForInvalidFeed;
	}

	/**
	 * Sets the detailed exception for invalid feed.
	 * 
	 * @param pDetailedExceptionForInvalidFeed the new detailed exception for invalid feed
	 */
	public void setDetailedExceptionForInvalidFeed(String pDetailedExceptionForInvalidFeed) {
		mDetailedExceptionForInvalidFeed = pDetailedExceptionForInvalidFeed;
	}

	/**
	 * Gets the attachment item id label.
	 * 
	 * @return the attachment item id label
	 */
	public String getAttachmentItemIdLabel() {
		return mAttachmentItemIdLabel;
	}

	/**
	 * Sets the attachment item id label.
	 * 
	 * @param pAttachmentItemIdLabel  the new attachment item id label
	 */
	public void setAttachmentItemIdLabel(String pAttachmentItemIdLabel) {
		mAttachmentItemIdLabel = pAttachmentItemIdLabel;
	}

	/**
	 * Gets the attachment exception reason label.
	 * 
	 * @return the attachment exception reason label
	 */
	public String getAttachmentExceptionReasonLabel() {
		return mAttachmentExceptionReasonLabel;
	}

	/**
	 * Sets the attachment exception reason label.
	 * 
	 * @param pAttachmentExceptionReasonLabel the new attachment exception reason label
	 */
	public void setAttachmentExceptionReasonLabel(String pAttachmentExceptionReasonLabel) {
		mAttachmentExceptionReasonLabel = pAttachmentExceptionReasonLabel;
	}

	/**
	 * Gets the failure mail priority2.
	 * 
	 * @return the failure mail priority2
	 */
	public String getFailureMailPriority2() {
		return mFailureMailPriority2;
	}

	/**
	 * Sets the failure mail priority2.
	 * 
	 * @param pFailureMailPriority2 the new failure mail priority2
	 */
	public void setFailureMailPriority2(String pFailureMailPriority2) {
		mFailureMailPriority2 = pFailureMailPriority2;
	}

	/**
	 * Gets the best seller feed message from.
	 *
	 * @return the mCatalogFeedMessageFrom
	 */
	public String getBestSellerFeedMessageFrom() {
		return mBestSellerFeedMessageFrom;
	}

	/**
	 * Sets the catalog feed message from.
	 *
	 * @param pBestSellerFeedMessageFrom the new best seller feed message from
	 */
	public void setBestSellerFeedMessageFrom(String pBestSellerFeedMessageFrom) {
		this.mBestSellerFeedMessageFrom = pBestSellerFeedMessageFrom;
	}

	/**
	 * Gets the best seller feed message to.
	 *
	 * @return the mCatalogFeedMessageTo
	 */
	public List<String> getBestSellerFeedMessageTo() {
		return mBestSellerFeedMessageTo;
	}

	/**
	 * Sets the catalog feed message to.
	 *
	 * @param pBestSellerFeedMessageTo the new best seller feed message to
	 */
	public void setBestSellerFeedMessageTo(List<String> pBestSellerFeedMessageTo) {
		this.mBestSellerFeedMessageTo = pBestSellerFeedMessageTo;
	}

	/**
	 * Gets the best seller success sub.
	 *
	 * @return the bestSellerSuccessSub
	 */
	public String getBestSellerSuccessSub() {
		return mBestSellerSuccessSub;
	}

	/**
	 * Sets the best seller success sub.
	 *
	 * @param pBestSellerSuccessSub the bestSellerSuccessSub to set
	 */
	public void setBestSellerSuccessSub(String pBestSellerSuccessSub) {
		mBestSellerSuccessSub = pBestSellerSuccessSub;
	}

	/**
	 * Gets the best seller error sub.
	 *
	 * @return the bestSellerErrorSub
	 */
	public String getBestSellerErrorSub() {
		return mBestSellerErrorSub;
	}

	/**
	 * Sets the best seller error sub.
	 *
	 * @param pBestSellerErrorSub the bestSellerErrorSub to set
	 */
	public void setBestSellerErrorSub(String pBestSellerErrorSub) {
		mBestSellerErrorSub = pBestSellerErrorSub;
	}

	/**
	 * Gets the best seller failure sub.
	 *
	 * @return the bestSellerFailureSub
	 */
	public String getBestSellerFailureSub() {
		return mBestSellerFailureSub;
	}

	/**
	 * Sets the best seller failure sub.
	 *
	 * @param pBestSellerFailureSub the bestSellerFailureSub to set
	 */
	public void setBestSellerFailureSub(String pBestSellerFailureSub) {
		mBestSellerFailureSub = pBestSellerFailureSub;
	}

	/**
	 * Gets the env.
	 *
	 * @return the env
	 */
	public String getEnv() {
		return mEnv;
	}

	/**
	 * Sets the env.
	 *
	 * @param pEnv the env to set
	 */
	public void setEnv(String pEnv) {
		mEnv = pEnv;
	}

	/**
	 * Gets the best seller success with errors.
	 *
	 * @return the bestSellerSuccessWithErrors
	 */
	public String getBestSellerSuccessWithErrors() {
		return mBestSellerSuccessWithErrors;
	}

	/**
	 * Sets the best seller success with errors.
	 *
	 * @param pBestSellerSuccessWithErrors the bestSellerSuccessWithErrors to set
	 */
	public void setBestSellerSuccessWithErrors(String pBestSellerSuccessWithErrors) {
		mBestSellerSuccessWithErrors = pBestSellerSuccessWithErrors;
	}

	/**
	 * Gets the best seller success.
	 *
	 * @return the bestSellerSuccess
	 */
	public String getBestSellerSuccess() {
		return mBestSellerSuccess;
	}

	/**
	 * Sets the best seller success.
	 *
	 * @param pBestSellerSuccess the bestSellerSuccess to set
	 */
	public void setBestSellerSuccess(String pBestSellerSuccess) {
		mBestSellerSuccess = pBestSellerSuccess;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * Gets the record number.
	 *
	 * @return the recordNumber
	 */
	public String getRecordNumber() {
		return mRecordNumber;
	}

	/**
	 * Sets the record number.
	 *
	 * @param pRecordNumber the recordNumber to set
	 */
	public void setRecordNumber(String pRecordNumber) {
		mRecordNumber = pRecordNumber;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param pTimeStamp the timeStamp to set
	 */
	public void setTimeStamp(String pTimeStamp) {
		mTimeStamp = pTimeStamp;
	}
}
