package com.tru.feedprocessor.email;

import java.io.File;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

/**
 * This class extends OOTB DynamoServlet to Email operation.
 * @author Professional Access
 * @version 1.0
 */
public class TRUBestSellerFeedEmailService extends GenericService {

	/** property to hold Promotion Export Configuration. */
	private TRUBestSellerFeedEmailConfiguration mTRUBestSellerFeedEmailConfiguration;

	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;

	/**
	 * Send feed success email.
	 *
	 * @param pParams            the params
	 * @param pInvalidRecordsFile 
	 */
	public void sendFeedSuccessEmail(Map<String, Object> pParams, File pInvalidRecordsFile) {
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
		try {
			Map<String, Object> servieMap = (Map<String, Object>) pParams.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER);
			TRUBestSellerFeedEmailConfiguration truFeedEmailConfiguration = getTRUBestSellerFeedEmailConfiguration();
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getSuccessTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(servieMap);
			} else {
				emailInfo.setTemplateParameters(servieMap);
			}
			
			if (pInvalidRecordsFile != null) {
				
				File[] attachments = {pInvalidRecordsFile};
				emailInfo.setMessageAttachments(attachments);
			}
			
			emailInfo.setMessageSubject((String) servieMap.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
	}

	/**
	 * Send feed failure email.
	 *
	 * @param pParams the params
	 */
	public void sendFeedFailureEmail(Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
		try {
			Map<String, Object> servieMap = (Map<String, Object>) pParams.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER);
			TRUBestSellerFeedEmailConfiguration truFeedEmailConfiguration = getTRUBestSellerFeedEmailConfiguration();
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getFailureTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(servieMap);
			} else {
				emailInfo.setTemplateParameters(servieMap);
			}
			emailInfo.setMessageSubject((String) servieMap.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
	}
	
	/**
	 * Send files moved to unproccessed folder email.
	 *
	 * @param pParams the params
	 */
	public void sendFilesMovedToUnproccessedFolderEmail(Map<String, Object> pParams) {
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
		try {
			Map<String, Object> servieMap = (Map<String, Object>) pParams.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER);
			TRUBestSellerFeedEmailConfiguration truFeedEmailConfiguration = getTRUBestSellerFeedEmailConfiguration();
			TemplateEmailInfoImpl emailInfo = truFeedEmailConfiguration.getFilesToUnproccessedTemplateEmailInfo();
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(servieMap);
			} else {
				emailInfo.setTemplateParameters(servieMap);
			}
			emailInfo.setMessageSubject((String) servieMap.get(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, truFeedEmailConfiguration.getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: sendFeedSuccessEmail()");
	}
	/**
	 * Clear email info.
	 * 
	 * @param pEmailInfo
	 *            the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		vlogDebug("Begin:@Class: TRUBestSellerFeedEmailService : @Method: clearEmailInfo()");
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		vlogDebug("End:@Class: TRUBestSellerFeedEmailService : @Method: clearEmailInfo()");
	}
	

	/**
	 * Gets the template email sender.
	 * 
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Gets the TRU best seller feed email configuration.
	 *
	 * @return the tRUBestSellerFeedEmailConfiguration
	 */
	public TRUBestSellerFeedEmailConfiguration getTRUBestSellerFeedEmailConfiguration() {
		return mTRUBestSellerFeedEmailConfiguration;
	}

	/**
	 * Sets the TRU best seller feed email configuration.
	 *
	 * @param pTRUBestSellerFeedEmailConfiguration the tRUBestSellerFeedEmailConfiguration to set
	 */
	public void setTRUBestSellerFeedEmailConfiguration(
			TRUBestSellerFeedEmailConfiguration pTRUBestSellerFeedEmailConfiguration) {
		mTRUBestSellerFeedEmailConfiguration = pTRUBestSellerFeedEmailConfiguration;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the mTemplateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
}
