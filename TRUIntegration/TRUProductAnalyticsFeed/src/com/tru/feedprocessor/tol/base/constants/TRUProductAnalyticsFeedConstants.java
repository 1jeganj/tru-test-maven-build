package com.tru.feedprocessor.tol.base.constants;

/**
 * The Class TRUProductAnalyticsFeedConstants.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedConstants {
	
	
	public static final String BEST_SELLER_FEED = "BestSellerFeed";
	
	/** property to hold ONE. */
	public static final int ONE = 1;
	
	/**
	 * property to hold DOLLAR_ONLY.
	 */
	public static final String DOLLAR_ONLY = "~";
	
	/** The Constant for Space. */
	public static final String SPACE = " ";

	/** The Constant FILE_DOWNLOAD_FAILED. */
	public static final String FILE_DOWNLOAD_FAILED = "File Download Failed...";
	/** The Constant NO_FILES_TO_DOWNLOAD. */
	public static final String NO_FILES_TO_DOWNLOAD = "NO FILES TO DOWNLOAD";
	/** The Constant NO_FILES_TO_DOWNLOAD. */
	public static final String FILE_NOT_FOUND = "FILE NOT FOUND";
	/** The Constant DOWNLOAD_SUCCESS. */
	public static final String DOWNLOAD_SUCCESS = "FILES DOWNLOAD_SUCCESS ";
	/** The Constant PARSED_SUCCESS. */
	public static final String PARSED_SUCCESS = "FILE PARSED SUCCESSFULLY ";
	/** The Constant PARSED_VALIDATED. */
	public static final String PARSED_VALIDATED = "FILE PARSED VALIDATED ";
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROCESS = "FEED FILE PROCESSING ";
	/** The Constant FEED_PROCESS_COMPLETE. */
	public static final String FEED_PROCESS_COMPLETE = "FEED FILE PROCESSING COMPLETED ";
	/** The Constant FEED_COMPLETE. */
	public static final String FEED_COMPLETE = "FEED COMPLETED ";
	/** The Constant FEED_PROCESS_FAILED. */
	public static final String FEED_PROCESS_FAILED = "FEED PROCESSING FAILED";
	/**
	 * The Property to hold FILE_NOT_FOUND_EXCEPTION.
	 */
	public static final String FILE_NOT_FOUND_EXCEPTION = "FileNotFoundException ";

	/** The property to hold IOEXCEPTION. */
	public static final String IOEXCEPTION = "IOException";
	
	/** The property to hold REPOSITORY_EXCEPTION. */
	public static final String REPOSITORY_EXCEPTION = "There is an error while updating the data in DB";
	
	/** The property to hold ARRAYINDEXOUTOFBOUNDS_EXCEPTION. */
	public static final String ARRAYINDEXOUTOFBOUNDS_EXCEPTION = "ArrayIndexOutOfBoundsException";
	
	/**
	 * The Constant SLASH.
	 */
	public static final String SLASH = "/";
	
	/** The property to hold PARSE_EXCEPTION. */
	public static final String PARSE_EXCEPTION = "parsing exception :";
	
	/** The property to hold NUMBER_FORMAT_EXCEPTION. */
	public static final String NUMBER_FORMAT_EXCEPTION = "NUMBER FORMAT EXCEPTION";
	/** The property to hold ILLEGALARGUMENT_EXCEPTION. */
	public static final String ILLEGALARGUMENT_EXCEPTION = "illegar argument exception";
	
	/**
	 * property to hold DOUBLE_BACKWARDSLASH_UNDERSCORE.
	 */
	public static final String DOUBLE_BACKWARDSLASH_UNDERSCORE = "\\_";
	
	/**
	 * property to hold dateFormat.
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	
	/**
	 *  property to hold FEED_DATE_FORMAT.
	 */
	public static final String FEED_DATE_FORMAT = "yyyy-MM-ddhhmmss";
	
	/**
	 * property to hold FEED_FILE_NAME.
	 */
	public static final String FEED_FILE_NAME = "FeedFileName";
	
	/** The Constant PIPE_DELIMITER. */
	public static final char PIPE_DELIMITER = '|';
	

	/**
	 * property to hold FEED_FILE_NAME.
	 */
	public static final String PARSED_FEED_FILE = "PARSED FEED FILE";
	
	/** The Constant FEED_FILE_TAIL_RECORD_KEY. */
	public static final String FEED_FILE_TAIL_RECORD_KEY = "TRAILER";
	
	/** The Constant FEED_FILE_HEAD_RECORD_KEY. */
	public static final String FEED_FILE_HEAD_RECORD_KEY = "HEADER";
	
	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;

	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;
	
	/** The Constant LONG_NUMBER_ONE. */
	public static final long LONG_NUMBER_ONE = 1;
	
	/** The Constant LONG_NUMBER_ONE. */
	public static final long LONG_NUMBER_ZERO = 0;
	
	/** The Constant NUMBER_TWO. */
	public static final int NUMBER_TWO = 2;
	
	/**
	 * property to hold FEED_FILE_NAME.
	 */
	public static final String BAD_FILE = "BAD FILE";
	
	/** The Constant SKU_NOT_FOUND. */
	public static final String SKU_NOT_FOUND = "SKU not found in the DB";
	
	/** The Constant ERROR. */
	public static final String ERROR = "Error";
	
	/** The Constant UNDER_SCORE. */
	public static final String UNDER_SCORE = "_";
	
	/** The Constant SEPARATOR. */
	public static final String SEPARATOR = " --> ";
	
	/** The Constant SUCCESS_RECORD. */
	public static final String SUCCESS_RECORD = "SUCCESS RECORD ";
	
	/** The Constant SKU_ID_NOT_FOUND_IN_THE_DB. */
	public static final String SKU_ID_NOT_FOUND_IN_THE_DB = "SKU Id not found in the DB";
	
	/** The Constant SUB_DATE_FORMAT. */
	public static final String SUB_DATE_FORMAT = "MMddyyyy-HH:mm:ss";
	
	/** The Constant SUB_DATE_FORMAT. */
	public static final String FEED_ATTACHMENT_DATE_FORMAT = "yyyy-MM-dd-HH.mm.sss";
	
	/** The Constant XLS_SHEET. */
	public static final String XLS_SHEET = "sheet";

	/** The Constant NUMBER_THREE. */
	public static final int NUMBER_THREE = 3;

	/** The Constant NUMBER_FOUR. */
	public static final int NUMBER_FOUR = 4;

	/** The Constant DOT. */
	public static final String DOT = ".";
	
	/** The Constant XLS_SHEET. */
	public static final String XLS = "xls";
	
	/** The Constant FILE_DOWNLOAD. */
	public static final String FILE_DOWNLOAD = "File Download";
	
	/** The Constant FILE_PARSING. */
	public static final String FILE_PARSING = "File Parsing";
	
	/** The Constant FILE_VALIDATION. */
	public static final String FILE_VALIDATION = "File Validation";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant TIME_TAKEN. */
	public static final String TIME_TAKEN = "Time Taken";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant START_DATE. */
	public static final String START_DATE = "Start Date";

	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant FILES_MOVED_SUCCESS. */
	public static final String FILES_MOVED_SUCCESS = "File Moved successfully";
	
	/** The Constant NO_MATCH_FIELS. */
	public static final String NO_MATCH_FIELS = "No Match Files";
	
	/** The Constant FILES_VALID. */
	public static final String FILES_VALID = "File is valid";
	
	/** The Constant FILES_NOT_VALID. */
	public static final String FILES_NOT_VALID = "File is not valid";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_COMPLETED = "Feed completed";
	
	/** The Constant FILES_WRITING. */
	public static final String FILES_WRITING = "File Writing";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "Feed Status";
	
	/** The Constant FEED_FAILED. */
	public static final String FEED_FAILED = "Feed Failed";
	
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROC = "Feed Process";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";
	
	/** The Constant PARSE_TAX. */
	public static final String PARSE_TAX = "Parsing WebsiteTaxonomy File";
	
	/** The Constant PARSE_REG. */
	public static final String PARSE_REG = "Parsing RegistryCategorization File";
	
	/** The Constant PARSE_PROD. */
	public static final String PARSE_PROD = "Parsing ProductCanonical File";
	
	/** The Constant FILE_NO_SEQ. */
	public static final String FILE_NO_SEQ = "File is not in sequence with last processed";

	
}
