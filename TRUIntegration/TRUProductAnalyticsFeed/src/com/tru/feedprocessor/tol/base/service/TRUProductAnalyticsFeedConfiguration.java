package com.tru.feedprocessor.tol.base.service;

import atg.nucleus.GenericService;

/**
 * The Class TRUProductAnalyticsFeedConfiguration.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedConfiguration extends GenericService {
	
	/** The m source directory. */
	private String mSourceDirectory;
	
	/** The m bad directory. */
	private String mBadDirectory;
	
	/** The m processing directory. */
	private String mProcessingDirectory;
	
	/** The m success directory. */
	private String mSuccessDirectory;
	
	/** The m error directory. */
	private String mErrorDirectory;
	
	/** The m unproccessed directory. */
	private String mUnproccessedDirectory;
	
	/** The m file name. */
	private String mFileName;
	
	/** The m file pathprocessing. */
	private String mFilePathprocessing;
	
	/** The m file type. */
	private String mFileType;
	
	/** The m error foldererror. */
	private String mErrorFoldererror;
	
	/** The m  records toprocess. */
	private int mRecordsToProcess;
	
	/**
	 * Gets the records to process.
	 *
	 * @return the recordsToProcess
	 */
	public int getRecordsToProcess() {
		return mRecordsToProcess;
	}

	/**
	 * Sets the records to process.
	 *
	 * @param pRecordsToProcess the recordsToProcess to set
	 */
	public void setRecordsToProcess(int pRecordsToProcess) {
		mRecordsToProcess = pRecordsToProcess;
	}

	/**
	 * Gets the source directory.
	 *
	 * @return the sourceDirectory
	 */
	public String getSourceDirectory() {
		return mSourceDirectory;
	}
	
	/**
	 * Sets the source directory.
	 *
	 * @param pSourceDirectory the sourceDirectory to set
	 */
	public void setSourceDirectory(String pSourceDirectory) {
		mSourceDirectory = pSourceDirectory;
	}
	
	/**
	 * Gets the bad directory.
	 *
	 * @return the badDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}
	
	/**
	 * Sets the bad directory.
	 *
	 * @param pBadDirectory the badDirectory to set
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
	}
	
	/**
	 * Gets the processing directory.
	 *
	 * @return the processingDirectory
	 */
	public String getProcessingDirectory() {
		return mProcessingDirectory;
	}
	
	/**
	 * Sets the processing directory.
	 *
	 * @param pProcessingDirectory the processingDirectory to set
	 */
	public void setProcessingDirectory(String pProcessingDirectory) {
		mProcessingDirectory = pProcessingDirectory;
	}
	
	/**
	 * Gets the success directory.
	 *
	 * @return the successDirectory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}
	
	/**
	 * Sets the success directory.
	 *
	 * @param pSuccessDirectory the successDirectory to set
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
	}
	
	/**
	 * Gets the error directory.
	 *
	 * @return the errorDirectory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}
	
	/**
	 * Sets the error directory.
	 *
	 * @param pErrorDirectory the errorDirectory to set
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
	}
	
	/**
	 * Gets the unproccessed directory.
	 *
	 * @return the unproccessedDirectory
	 */
	public String getUnproccessedDirectory() {
		return mUnproccessedDirectory;
	}
	
	/**
	 * Sets the unproccessed directory.
	 *
	 * @param pUnproccessedDirectory the unproccessedDirectory to set
	 */
	public void setUnproccessedDirectory(String pUnproccessedDirectory) {
		mUnproccessedDirectory = pUnproccessedDirectory;
	}
	
	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return mFileName;
	}
	
	/**
	 * Sets the file name.
	 *
	 * @param pFileName the fileName to set
	 */
	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}
	
	/**
	 * Gets the file pathprocessing.
	 *
	 * @return the filePathprocessing
	 */
	public String getFilePathprocessing() {
		return mFilePathprocessing;
	}
	
	/**
	 * Sets the file pathprocessing.
	 *
	 * @param pFilePathprocessing the filePathprocessing to set
	 */
	public void setFilePathprocessing(String pFilePathprocessing) {
		mFilePathprocessing = pFilePathprocessing;
	}
	
	/**
	 * Gets the file type.
	 *
	 * @return the fileType
	 */
	public String getFileType() {
		return mFileType;
	}
	
	/**
	 * Sets the file type.
	 *
	 * @param pFileType the fileType to set
	 */
	public void setFileType(String pFileType) {
		mFileType = pFileType;
	}
	
	/**
	 * Gets the error foldererror.
	 *
	 * @return the errorFoldererror
	 */
	public String getErrorFoldererror() {
		return mErrorFoldererror;
	}
	
	/**
	 * Sets the error foldererror.
	 *
	 * @param pErrorFoldererror the errorFoldererror to set
	 */
	public void setErrorFoldererror(String pErrorFoldererror) {
		mErrorFoldererror = pErrorFoldererror;
	}
	
	
}
