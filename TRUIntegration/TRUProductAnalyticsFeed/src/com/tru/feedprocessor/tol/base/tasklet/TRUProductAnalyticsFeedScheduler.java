package com.tru.feedprocessor.tol.base.tasklet;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feedprocessor.tol.base.service.TRUProductAnalyticsFeedService;

/**
 * The Class TRUProductAnalyticsFeedScheduler.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedScheduler extends SingletonSchedulableService {
	
	/** The TRU product analytics feed service. */
	private TRUProductAnalyticsFeedService mTRUProductAnalyticsFeedService;
	
	/** property to hold Enabled flag. */
	private boolean mEnabled;
	
	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param pEnabled
	 *            the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	
	/**
	 * Gets the TRU product analytics feed service.
	 *
	 * @return the tRUProductAnalyticsFeedService
	 */
	public TRUProductAnalyticsFeedService getTRUProductAnalyticsFeedService() {
		return mTRUProductAnalyticsFeedService;
	}

	/**
	 * Sets the TRU product analytics feed service.
	 *
	 * @param pTRUProductAnalyticsFeedService the tRUProductAnalyticsFeedService to set
	 */
	public void setTRUProductAnalyticsFeedService(TRUProductAnalyticsFeedService pTRUProductAnalyticsFeedService) {
		mTRUProductAnalyticsFeedService = pTRUProductAnalyticsFeedService;
	}

	/** doScheduledTask method.
	 * @see atg.service.scheduler.SingletonSchedulableService#doScheduledTask(atg.service.scheduler.Scheduler, atg.service.scheduler.ScheduledJob)
	 * @param pArg0 - Arg0
	 * @param pArg1 - Arg1
	 */
	@Override
	public void doScheduledTask(Scheduler pArg0, ScheduledJob pArg1) {
		// TODO Auto-generated method stub
		
		vlogDebug("Start @Class: TRUProductAnalyticsFeedScheduler, @method: doScheduledTask()");
		
		if(isEnabled()){
			processProductAnalyticsFeed();
		}
		
		vlogDebug("End @Class: TRUProductAnalyticsFeedScheduler, @method: doScheduledTask()");

	}
	
	/**
	 * Process product analytics feed.
	 */
	public void processProductAnalyticsFeed(){
		vlogDebug("Start @Class: TRUProductAnalyticsFeedScheduler, @method: processProductAnalyticsFeed()");
		
		getTRUProductAnalyticsFeedService().doService();
		
		vlogDebug("End @Class: TRUProductAnalyticsFeedScheduler, @method: processProductAnalyticsFeed()");
	}
	
}
