package com.tru.feedprocessor.tol.base.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.base.constants.TRUProductAnalyticsFeedConstants;
import com.tru.feedprocessor.tol.base.logger.TRUAnalyticsFeedLogger;
import com.tru.feedprocessor.tol.base.service.TRUProductAnalyticsFeedConfiguration;
import com.tru.feedprocessor.tol.base.service.TRUProductAnalyticsFeedProperty;
import com.tru.feedprocessor.tol.base.vo.TRUProductAnalyticsFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUProductAnalyticsFeedMapper.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedMapper extends GenericService {
	
	/** The mLogger. */
	private TRUAnalyticsFeedLogger mLogger;
	
	/** The mTRUProductAnalyticsFeedConfiguration. */
	private TRUProductAnalyticsFeedConfiguration mTRUProductAnalyticsFeedConfiguration;
	/**
	 * The variable to hold "ProductCatalogRepository" property name.
	 */
	private MutableRepository mProductCatalogRepository;
	
	/**
	 * The variable to hold "FeedRepository" property name.
	 */
	private MutableRepository mFeedRepository;

	/** The TRU product analytics feed property. */
	private TRUProductAnalyticsFeedProperty mTRUProductAnalyticsFeedProperty;

	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public TRUAnalyticsFeedLogger getLogger() {
		return mLogger;
	}
	

	/**
	 * Gets the TRU product analytics feed configuration.
	 *
	 * @return the tRUProductAnalyticsFeedConfiguration
	 */
	public TRUProductAnalyticsFeedConfiguration getTRUProductAnalyticsFeedConfiguration() {
		return mTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Sets the TRU product analytics feed configuration.
	 *
	 * @param pTRUProductAnalyticsFeedConfiguration the tRUProductAnalyticsFeedConfiguration to set
	 */
	public void setTRUProductAnalyticsFeedConfiguration(
			TRUProductAnalyticsFeedConfiguration pTRUProductAnalyticsFeedConfiguration) {
		mTRUProductAnalyticsFeedConfiguration = pTRUProductAnalyticsFeedConfiguration;
	}
	
	/**
	 * Sets the logger.
	 *
	 * @param pLogger the logger to set
	 */
	public void setLogger(TRUAnalyticsFeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/**
	 * Gets the product catalog repository.
	 *
	 * @return the productCatalogRepository
	 */
	public MutableRepository getProductCatalogRepository() {
		return mProductCatalogRepository;
	}


	/**
	 * Sets the product catalog repository.
	 *
	 * @param pProductCatalogRepository the productCatalogRepository to set
	 */
	public void setProductCatalogRepository(MutableRepository pProductCatalogRepository) {
		mProductCatalogRepository = pProductCatalogRepository;
	}


	/**
	 * Gets the feed repository.
	 *
	 * @return the feedRepository
	 */
	public MutableRepository getFeedRepository() {
		return mFeedRepository;
	}


	/**
	 * Sets the feed repository.
	 *
	 * @param pFeedRepository the feedRepository to set
	 */
	public void setFeedRepository(MutableRepository pFeedRepository) {
		mFeedRepository = pFeedRepository;
	}

	/**
	 * Gets the TRU product analytics feed property.
	 *
	 * @return the tRUProductAnalyticsFeedProperty
	 */
	public TRUProductAnalyticsFeedProperty getTRUProductAnalyticsFeedProperty() {
		return mTRUProductAnalyticsFeedProperty;
	}


	/**
	 * Sets the TRU product analytics feed property.
	 *
	 * @param pTRUProductAnalyticsFeedProperty the tRUProductAnalyticsFeedProperty to set
	 */
	public void setTRUProductAnalyticsFeedProperty(TRUProductAnalyticsFeedProperty pTRUProductAnalyticsFeedProperty) {
		mTRUProductAnalyticsFeedProperty = pTRUProductAnalyticsFeedProperty;
	}


	/**
	 * Createvos and map to repository.
	 * 
	 * @param pServiceMap
	 *            the service map
	 * @return the boolean
	 */
	public Boolean createvosAndMapToRepository(Map<String, Object> pServiceMap){
		Map<String, Object> serviceMap = pServiceMap;
		boolean retValue = Boolean.FALSE;
		int successRecords = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
		String startDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_WRITING, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FEED_PROC);
		List<TRUProductAnalyticsFeedVO> failedRecords = new ArrayList<TRUProductAnalyticsFeedVO>();
		List<TRUProductAnalyticsFeedVO> skuNotFoundRecords = new ArrayList<TRUProductAnalyticsFeedVO>();
		serviceMap.put(TRUProductAnalyticsFeedConstants.REPOSITORY_EXCEPTION, failedRecords);
		serviceMap.put(TRUProductAnalyticsFeedConstants.SKU_NOT_FOUND, skuNotFoundRecords);
		getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.FEED_PROCESS);
		if(serviceMap.get(TRUProductAnalyticsFeedConstants.PARSED_FEED_FILE) != null){
			List<String[]> allParsedRecords = (List<String[]>) serviceMap.get(TRUProductAnalyticsFeedConstants.PARSED_FEED_FILE);
			if(!allParsedRecords.isEmpty()){
				int recordsSize = allParsedRecords.size();
				int counter = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
				List<TRUProductAnalyticsFeedVO> voList = new ArrayList<TRUProductAnalyticsFeedVO>();
				for (int i = TRUProductAnalyticsFeedConstants.NUMBER_ONE; i < recordsSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE; i++) {
					retValue = Boolean.FALSE;
					String[] record = allParsedRecords.get(i);
					String skuId = record[TRUProductAnalyticsFeedConstants.NUMBER_ZERO];
					long quantity = Long.parseLong(record[TRUProductAnalyticsFeedConstants.NUMBER_ONE]);
					TRUProductAnalyticsFeedVO feedVO = new TRUProductAnalyticsFeedVO();
					feedVO.setSkuId(skuId);
					feedVO.setQuantity(quantity);
					counter++;
					voList.add(feedVO);
					if(counter == getTRUProductAnalyticsFeedConfiguration().getRecordsToProcess() || i == recordsSize - 
							TRUProductAnalyticsFeedConstants.NUMBER_TWO){
						successRecords+=mapVOToRepository(voList, skuNotFoundRecords, failedRecords);
						counter = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
						voList.clear();
						retValue = Boolean.TRUE;
					}
				}
				serviceMap.put(TRUProductAnalyticsFeedConstants.SUCCESS_RECORD, successRecords);
			}
		}
		String startDate2 = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate2, TRUProductAnalyticsFeedConstants.FEED_PROCESS_COMPLETE, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FEED_COMPLETE);
		getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.FEED_PROCESS_COMPLETE);
		return retValue;
	}

	/**
	 * Map vo to repository.
	 *
	 * @param pList            the list
	 * @param pSkuNotFoundRecords            the sku not found records
	 * @param pFailedRecords            the failed records
	 * @return the int
	 */
	private int mapVOToRepository(List<TRUProductAnalyticsFeedVO> pList, List<TRUProductAnalyticsFeedVO> pSkuNotFoundRecords, List<TRUProductAnalyticsFeedVO> pFailedRecords){
		List<TRUProductAnalyticsFeedVO> failedRecords = pFailedRecords;
		List<TRUProductAnalyticsFeedVO> skuNotFoundRecords = pSkuNotFoundRecords;
		int successRecords = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
		if(!pList.isEmpty()){
			for(TRUProductAnalyticsFeedVO feedVO : pList){
				MutableRepositoryItem repoItem = null;
				String skuId = feedVO.getSkuId();
				long quantity = feedVO.getQuantity();
				try{
					if(checkSKU(skuId)){
						repoItem = getFeedRepository().getItemForUpdate(skuId, getTRUProductAnalyticsFeedProperty().getProductanalytics());
						if(repoItem != null){
							repoItem.setPropertyValue(getTRUProductAnalyticsFeedProperty().getQuantity(), quantity);
							getFeedRepository().updateItem(repoItem);
						} else{
							repoItem = getFeedRepository().createItem(skuId, getTRUProductAnalyticsFeedProperty().getProductanalytics());
							repoItem.setPropertyValue(getTRUProductAnalyticsFeedProperty().getQuantity(), quantity);
							getFeedRepository().addItem(repoItem);
						}
						successRecords++;
					} else{
						skuNotFoundRecords.add(feedVO);
					}
				} catch (RepositoryException re){
					if (isLoggingError()) {	
						logError(TRUProductAnalyticsFeedConstants.REPOSITORY_EXCEPTION, re);	
					}
					failedRecords.add(feedVO);
				}
			}
		}
		return successRecords;
	}
	
	
	/**
	 * Check sku.
	 *
	 * @param pSkuId the sku id
	 * @return true, if successful
	 * @throws RepositoryException the repository exception
	 */
	private boolean checkSKU(String pSkuId) throws RepositoryException {
		RepositoryItem skuRepositoryItem = null;
		boolean prodId = Boolean.FALSE;
		skuRepositoryItem = getProductCatalogRepository().getItem(pSkuId, getTRUProductAnalyticsFeedProperty().getSku());
		if(skuRepositoryItem != null){
			prodId = Boolean.TRUE;
		}
		return prodId;
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 * @param pStepName the step name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage, String pFileName, String pStepName) {
		String endDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductAnalyticsFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductAnalyticsFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductAnalyticsFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED, pStepName, pFileName, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

}
