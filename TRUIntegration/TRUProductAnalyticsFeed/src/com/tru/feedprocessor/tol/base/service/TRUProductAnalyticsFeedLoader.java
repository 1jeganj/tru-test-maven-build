package com.tru.feedprocessor.tol.base.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import au.com.bytecode.opencsv.CSVReader;

import com.tru.feedprocessor.email.TRUBestSellerFeedEmailConstants;
import com.tru.feedprocessor.email.TRUBestSellerFeedEmailService;
import com.tru.feedprocessor.email.TRUBestSellerFeedEnvConfiguration;
import com.tru.feedprocessor.tol.base.constants.TRUProductAnalyticsFeedConstants;
import com.tru.feedprocessor.tol.base.exception.TRUAnalyticsFeedException;
import com.tru.feedprocessor.tol.base.logger.TRUAnalyticsFeedLogger;
import com.tru.feedprocessor.tol.base.vo.TRUProductAnalyticsFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUProductAnalyticsFeedLoader.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedLoader extends GenericService {
	
	/** The mLogger. */
	private TRUAnalyticsFeedLogger mLogger;
	
	/** The mTRUProductAnalyticsFeedConfiguration. */
	private TRUProductAnalyticsFeedConfiguration mTRUProductAnalyticsFeedConfiguration;
	
	/** The mTRUProductAnalyticsFileDownloadTask. */
	private TRUProductAnalyticsFileDownloadTask mTRUProductAnalyticsFileDownloadTask;
	
	/** The TRU best seller feed env configuration. */
	private TRUBestSellerFeedEnvConfiguration mTRUBestSellerFeedEnvConfiguration;
	
	/** The TRU best seller feed email service. */
	private TRUBestSellerFeedEmailService mTRUBestSellerFeedEmailService;
	

	/**
	 * Gets the TRU best seller feed email service.
	 *
	 * @return the tRUBestSellerFeedEmailService
	 */
	public TRUBestSellerFeedEmailService getTRUBestSellerFeedEmailService() {
		return mTRUBestSellerFeedEmailService;
	}


	/**
	 * Sets the TRU best seller feed email service.
	 *
	 * @param pTRUBestSellerFeedEmailService the tRUBestSellerFeedEmailService to set
	 */
	public void setTRUBestSellerFeedEmailService(
			TRUBestSellerFeedEmailService pTRUBestSellerFeedEmailService) {
		mTRUBestSellerFeedEmailService = pTRUBestSellerFeedEmailService;
	}


	/**
	 * Gets the TRU best seller feed env configuration.
	 *
	 * @return the tRUBestSellerFeedEnvConfiguration
	 */
	public TRUBestSellerFeedEnvConfiguration getTRUBestSellerFeedEnvConfiguration() {
		return mTRUBestSellerFeedEnvConfiguration;
	}


	/**
	 * Sets the TRU best seller feed env configuration.
	 *
	 * @param pTRUBestSellerFeedEnvConfiguration the tRUBestSellerFeedEnvConfiguration to set
	 */
	public void setTRUBestSellerFeedEnvConfiguration(
			TRUBestSellerFeedEnvConfiguration pTRUBestSellerFeedEnvConfiguration) {
		mTRUBestSellerFeedEnvConfiguration = pTRUBestSellerFeedEnvConfiguration;
	}


	/**
	 * Gets the TRU product analytics file download task.
	 *
	 * @return the tRUProductAnalyticsFileDownloadTask
	 */
	public TRUProductAnalyticsFileDownloadTask getTRUProductAnalyticsFileDownloadTask() {
		return mTRUProductAnalyticsFileDownloadTask;
	}


	/**
	 * Sets the TRU product analytics file download task.
	 *
	 * @param pTRUProductAnalyticsFileDownloadTask the tRUProductAnalyticsFileDownloadTask to set
	 */
	public void setTRUProductAnalyticsFileDownloadTask(
			TRUProductAnalyticsFileDownloadTask pTRUProductAnalyticsFileDownloadTask) {
		mTRUProductAnalyticsFileDownloadTask = pTRUProductAnalyticsFileDownloadTask;
	}


	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public TRUAnalyticsFeedLogger getLogger() {
		return mLogger;
	}
	

	/**
	 * Gets the TRU product analytics feed configuration.
	 *
	 * @return the tRUProductAnalyticsFeedConfiguration
	 */
	public TRUProductAnalyticsFeedConfiguration getTRUProductAnalyticsFeedConfiguration() {
		return mTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Sets the TRU product analytics feed configuration.
	 *
	 * @param pTRUProductAnalyticsFeedConfiguration the tRUProductAnalyticsFeedConfiguration to set
	 */
	public void setTRUProductAnalyticsFeedConfiguration(
			TRUProductAnalyticsFeedConfiguration pTRUProductAnalyticsFeedConfiguration) {
		mTRUProductAnalyticsFeedConfiguration = pTRUProductAnalyticsFeedConfiguration;
	}
	
	/**
	 * Sets the logger.
	 *
	 * @param pLogger the logger to set
	 */
	public void setLogger(TRUAnalyticsFeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/**
	 * Parses the file.
	 *
	 * @param pServiceMap the service map
	 * @throws TRUAnalyticsFeedException the TRU analytics feed exception
	 */
	public void parseFile(Map<String, Object> pServiceMap) throws TRUAnalyticsFeedException{
		
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: parseFile()");
		Map<String, Object> serviceMap = pServiceMap;
		String startDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		FileReader fileReader = null;
		File file = null;
		BufferedReader bufferedReader = null;
		CSVReader csvReader = null;	
		String fileName = (String) serviceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME);
		try {
			file = new File(getTRUProductAnalyticsFeedConfiguration().getProcessingDirectory()+fileName);
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			csvReader = new CSVReader(bufferedReader, TRUProductAnalyticsFeedConstants.PIPE_DELIMITER);
			List<String[]> allParsedRecords = csvReader.readAll();
			serviceMap.put(TRUProductAnalyticsFeedConstants.PARSED_FEED_FILE, allParsedRecords);
			getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.PARSED_SUCCESS);
			logSuccessMessage(startDate, TRUProductAnalyticsFeedConstants.FILE_PARSING, fileName, TRUProductAnalyticsFeedConstants.FILE_PARSING);
		} catch (FileNotFoundException fe) {
			if (isLoggingError()) {	
				logError(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.PARSE_EXCEPTION);	
			}
			logFailedMessage(startDate, fe.getMessage(), fileName, TRUProductAnalyticsFeedConstants.FILE_PARSING);
			throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.PARSE_EXCEPTION, fe);
		} catch (IOException ie) {
			if (isLoggingError()) {	
				logError(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.PARSE_EXCEPTION);	
			}
			logFailedMessage(startDate, ie.getMessage(), fileName, TRUProductAnalyticsFeedConstants.FILE_PARSING);
			throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.IOEXCEPTION, ie);
		} finally {
			try {
				if (csvReader != null) {
					csvReader.close();
					csvReader = null;
				}
				if (bufferedReader != null) {
					bufferedReader.close();
					bufferedReader = null;
				}
				if (fileReader != null) {
					fileReader.close();
					fileReader = null;
				}
			} catch (IOException ioe) {
				if (isLoggingError()) {	
					logError("@Class: TRUProductAnalyticsFeedParseFile : @Method: parseFile(): ", ioe);	
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUProductAnalyticsFeedParseFile : @Method: parseFile()");
	}
	
	/**
	 * Validate file.
	 *
	 * @param pServiceMap            the service map
	 * @return the boolean
	 * @throws TRUAnalyticsFeedException             the TRU analytics feed exception
	 */
	@SuppressWarnings("unchecked")
	public Boolean validateFile(Map<String, Object> pServiceMap) throws TRUAnalyticsFeedException{
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: validateFile()");
		Boolean retVal = Boolean.TRUE;
		String startDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		if(pServiceMap.get(TRUProductAnalyticsFeedConstants.PARSED_FEED_FILE) != null){
			List<String[]> allParsedRecords = (List<String[]>) pServiceMap.get(TRUProductAnalyticsFeedConstants.PARSED_FEED_FILE);
			if(!allParsedRecords.isEmpty()){
				int recSize = allParsedRecords.size();
				if (TRUProductAnalyticsFeedConstants.FEED_FILE_HEAD_RECORD_KEY.equals(allParsedRecords
						.get(TRUProductAnalyticsFeedConstants.NUMBER_ZERO)[TRUProductAnalyticsFeedConstants.NUMBER_ZERO])&& 
						TRUProductAnalyticsFeedConstants.FEED_FILE_TAIL_RECORD_KEY.equals(allParsedRecords.get(recSize- 
								TRUProductAnalyticsFeedConstants.NUMBER_ONE)[TRUProductAnalyticsFeedConstants.NUMBER_ZERO])&&
								!StringUtils.isBlank(allParsedRecords.get(recSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE)[TRUProductAnalyticsFeedConstants.NUMBER_ONE])&&
								!StringUtils.isBlank(allParsedRecords.get(recSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE)[TRUProductAnalyticsFeedConstants.NUMBER_TWO])) {
					Long quantityTotal =  TRUProductAnalyticsFeedConstants.LONG_NUMBER_ZERO;
					try{
						for (int i = TRUProductAnalyticsFeedConstants.NUMBER_ONE; i < recSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE; i++) {
							String[] record = allParsedRecords.get(i);
							quantityTotal += Long.parseLong(record[TRUProductAnalyticsFeedConstants.NUMBER_ONE]);
						}
						int recordsSizeInTailRecord = Integer.parseInt(allParsedRecords.get(recSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE)[TRUProductAnalyticsFeedConstants.NUMBER_ONE]);
						long quantityTotalInTailRecord = Long.parseLong(allParsedRecords.get(recSize - TRUProductAnalyticsFeedConstants.NUMBER_ONE)[TRUProductAnalyticsFeedConstants.NUMBER_TWO]);
						if(!(recSize == recordsSizeInTailRecord && quantityTotal == quantityTotalInTailRecord)){
							moveFileToBadFolder(pServiceMap);
							retVal = Boolean.FALSE;
							logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_NOT_VALID, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
							throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.BAD_FILE);
						}
						if(retVal){
							logSuccessMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_VALID, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
						}
					} catch (NumberFormatException ne) {
						if (isLoggingError()) {	
							logError(TRUProductAnalyticsFeedConstants.NUMBER_FORMAT_EXCEPTION);	
						}
						moveFileToBadFolder(pServiceMap);
						retVal = Boolean.FALSE;
						logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.BAD_FILE, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
						throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.BAD_FILE, ne);
					} catch (ArrayIndexOutOfBoundsException ae) {
						if (isLoggingError()) {	
							logError(TRUProductAnalyticsFeedConstants.ARRAYINDEXOUTOFBOUNDS_EXCEPTION);	
						}
						moveFileToBadFolder(pServiceMap);
						retVal = Boolean.FALSE;
						logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.BAD_FILE, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
						throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.BAD_FILE, ae);
					}
				} else {
					moveFileToBadFolder(pServiceMap);
					retVal = Boolean.FALSE;
					logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_NOT_VALID, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
					throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.BAD_FILE);
				}
				getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.PARSED_VALIDATED);
			} else {
				retVal = Boolean.FALSE;
				logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_NOT_VALID, (String)pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME), TRUProductAnalyticsFeedConstants.FILE_VALIDATION);
			}
		}
		getLogger().vlogDebug("End:@Class: TRUProductAnalyticsFeedParseFile : @Method: validateFile()");
		
		return retVal;
	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 * @param pStepName the step name
	 */
	private void logFailedMessage(String pStartDate, String pMessage, String pFileName, String pStepName) {
		String endDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductAnalyticsFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductAnalyticsFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductAnalyticsFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED, pStepName, pFileName, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 * @param pStepName the step name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage, String pFileName, String pStepName) {
		String endDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductAnalyticsFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductAnalyticsFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductAnalyticsFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED, pStepName, pFileName, extenstions);
	}
	
	/**
	 * Move file to bad folder.
	 * 
	 * @param pServiceMap
	 *            the service map
	 */
	private void moveFileToBadFolder(Map<String, Object> pServiceMap){
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: moveFileToBadFolder()");
		String fileName = (String) pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME);
		File fromFile = new File(getTRUProductAnalyticsFeedConfiguration().getProcessingDirectory()+fileName);
		File toFolder = new File(getTRUProductAnalyticsFeedConfiguration().getBadDirectory());
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		fromFile.renameTo(new File(toFolder, fromFile.getName()));
		
		String sub = getTRUBestSellerFeedEnvConfiguration().getBestSellerFailureSub();
		String[] args = new String[TRUProductAnalyticsFeedConstants.NUMBER_TWO];
		args[TRUProductAnalyticsFeedConstants.NUMBER_ZERO] = getTRUBestSellerFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.SUB_DATE_FORMAT);
		args[TRUProductAnalyticsFeedConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(sub, (Object[])args);
		String priority = getTRUBestSellerFeedEnvConfiguration().getFailureMailPriority2();
		String detailedException = getTRUBestSellerFeedEnvConfiguration().getDetailedExceptionForInvalidFeed();
		
		getTRUProductAnalyticsFileDownloadTask().notifyFileDoseNotExits(pServiceMap, formattedSubject,
				getTRUProductAnalyticsFeedConfiguration().getBadDirectory(), priority, detailedException);
		
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: moveFileToBadFolder()");
	}
	
	/**
	 * Move file to success folder.
	 * 
	 * @param pServiceMap
	 *            the service map
	 */
	public void moveFileToSuccessFolder(Map<String, Object> pServiceMap){
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: moveFileToSuccessFolder()");
		String fileName = (String) pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME);
		File fromFile = new File(getTRUProductAnalyticsFeedConfiguration().getProcessingDirectory()+fileName);
		File toFolder = new File(getTRUProductAnalyticsFeedConfiguration().getSuccessDirectory());
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		fromFile.renameTo(new File(toFolder, fromFile.getName()));
		
		getLogger().vlogDebug("Start:@Class: TRUProductAnalyticsFeedParseFile : @Method: moveFileToSuccessFolder()");
	}
	
	/**
	 * Move invalid records to error dir.
	 * 
	 * @param pServiceMap
	 *            the service map
	 * @throws TRUAnalyticsFeedException
	 *             the TRU analytics feed exception
	 */
	@SuppressWarnings("unchecked")
	public void moveInvalidRecordsToErrorDir(Map<String, Object> pServiceMap) throws TRUAnalyticsFeedException{

		String fileName = (String) pServiceMap.get(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME);
		int failedRecordsCount = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
		int skuNotFoundRecordsCount = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
		int successRecordCount = (int) pServiceMap.get(TRUProductAnalyticsFeedConstants.SUCCESS_RECORD);
		List<TRUProductAnalyticsFeedVO> failedRecords = (List<TRUProductAnalyticsFeedVO>) pServiceMap.get(TRUProductAnalyticsFeedConstants.REPOSITORY_EXCEPTION);
		List<TRUProductAnalyticsFeedVO> skuNotFoundRecords = (List<TRUProductAnalyticsFeedVO>) pServiceMap.get(TRUProductAnalyticsFeedConstants.SKU_NOT_FOUND);
		File errorFolder = new File(getTRUProductAnalyticsFeedConfiguration().getErrorDirectory());
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}

		if(!failedRecords.isEmpty() || !skuNotFoundRecords.isEmpty()){
			File attachment = new File(errorFolder, attachementFileName(fileName));
			try {
				outputStream = new FileOutputStream(attachment);
				workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet(TRUProductAnalyticsFeedConstants.XLS_SHEET);
				Integer rowCount = TRUProductAnalyticsFeedConstants.NUMBER_ZERO;
				HSSFRow rowhead = sheet.createRow(rowCount);
				HSSFCellStyle rowCellStyle = workbook.createCellStyle();
				
				createRowHead(rowhead, rowCellStyle);
				rowCount = TRUProductAnalyticsFeedConstants.NUMBER_ONE;
				HSSFCellStyle dataCellStyle = workbook.createCellStyle();
				dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
				dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
				dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
				dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);
				
				for(TRUProductAnalyticsFeedVO feedVO: failedRecords){
					String errorMsg = TRUProductAnalyticsFeedConstants.REPOSITORY_EXCEPTION;
					createDataRow(fileName, rowCount, dataCellStyle, feedVO, errorMsg, sheet);
					rowCount++;
					failedRecordsCount++;
				}
				for(TRUProductAnalyticsFeedVO feedVO: skuNotFoundRecords){
					String errorMsg = TRUProductAnalyticsFeedConstants.SKU_NOT_FOUND;
					createDataRow(fileName, rowCount, dataCellStyle, feedVO, errorMsg, sheet);
					rowCount++;
					skuNotFoundRecordsCount++;
				}
				if (null != workbook) {
					workbook.write(outputStream);
				}

			} catch (IOException ioException) {
				if (isLoggingError()) {
					logError("Error in : @class TRUProductAnalyticsFeedLoader method createAttachmentFile()", ioException);
				}
				throw new TRUAnalyticsFeedException(
						TRUProductAnalyticsFeedConstants.IOEXCEPTION,
						ioException);
			} finally {
				
				if (null != workbook) {
					workbook = null;
				}
				try {
					if (null != outputStream) {
						outputStream.close();
					}
				} catch (IOException ioe) {
					if (isLoggingError()) {
						logError("Error in : @class TRUProductAnalyticsFeedLoader method createAttachmentFile()", ioe);
					}
				}
			}

			String sub = getTRUBestSellerFeedEnvConfiguration().getBestSellerErrorSub();
			String[] args = new String[TRUProductAnalyticsFeedConstants.NUMBER_TWO];
			args[TRUProductAnalyticsFeedConstants.NUMBER_ZERO] = getTRUBestSellerFeedEnvConfiguration().getEnv();
			Date currentDate = new Date();
			DateFormat dateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.SUB_DATE_FORMAT);
			args[TRUProductAnalyticsFeedConstants.NUMBER_ONE] = dateFormat.format(currentDate);
			String formattedSubject = String.format(sub, (Object[])args);

			triggerFeedProcessedWithErrorsEmail(pServiceMap,formattedSubject,
					successRecordCount, failedRecordsCount, skuNotFoundRecordsCount, attachment);
		} else {

			String sub = getTRUBestSellerFeedEnvConfiguration().getBestSellerSuccessSub();
			String[] args = new String[TRUProductAnalyticsFeedConstants.NUMBER_TWO];
			args[TRUProductAnalyticsFeedConstants.NUMBER_ZERO] = getTRUBestSellerFeedEnvConfiguration().getEnv();
			Date currentDate = new Date();
			DateFormat dateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.SUB_DATE_FORMAT);
			args[TRUProductAnalyticsFeedConstants.NUMBER_ONE] = dateFormat.format(currentDate);
			String formattedSubject = String.format(sub, (Object[])args);

			triggerFeedProcessedSuccessfullyEmail(pServiceMap,formattedSubject,
					successRecordCount, failedRecordsCount, skuNotFoundRecordsCount);
		}
		getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.FEED_COMPLETE);

	}


	/**
	 * Creates the data row.
	 *
	 * @param pFileName the file name
	 * @param pRowCount the row count
	 * @param pDataCellStyle the data cell style
	 * @param pFeedVO the feed vo
	 * @param pErrorMsg the error msg
	 * @param pSheet the sheet
	 */
	protected void createDataRow(String pFileName, int pRowCount,
			HSSFCellStyle pDataCellStyle, TRUProductAnalyticsFeedVO pFeedVO, String pErrorMsg, HSSFSheet pSheet) {
		HSSFRow dataRow = pSheet.createRow(pRowCount);
		dataRow.createCell(TRUProductAnalyticsFeedConstants.NUMBER_ZERO).setCellValue(new HSSFRichTextString(pFileName));
		dataRow.createCell(TRUProductAnalyticsFeedConstants.NUMBER_ONE).setCellValue(new HSSFRichTextString(Integer.toString(pRowCount)));
		dataRow.createCell(TRUProductAnalyticsFeedConstants.NUMBER_TWO).setCellValue(
				new HSSFRichTextString((new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_ATTACHMENT_DATE_FORMAT)).format(new Date())));
		dataRow.createCell(TRUProductAnalyticsFeedConstants.NUMBER_THREE).setCellValue(
				new HSSFRichTextString(pFeedVO.getSkuId()));
		dataRow.createCell(TRUProductAnalyticsFeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(pErrorMsg));

		dataRow.getCell(TRUProductAnalyticsFeedConstants.NUMBER_ZERO).setCellStyle(pDataCellStyle);
		dataRow.getCell(TRUProductAnalyticsFeedConstants.NUMBER_ONE).setCellStyle(pDataCellStyle);
		dataRow.getCell(TRUProductAnalyticsFeedConstants.NUMBER_TWO).setCellStyle(pDataCellStyle);
		dataRow.getCell(TRUProductAnalyticsFeedConstants.NUMBER_THREE).setCellStyle(pDataCellStyle);
		dataRow.getCell(TRUProductAnalyticsFeedConstants.NUMBER_FOUR).setCellStyle(pDataCellStyle);
		
	}


	/**
	 * Creates the row head.
	 *
	 * @param pRowhead the rowhead
	 * @param pRowCellStyle the row cell style
	 */
	protected void createRowHead(HSSFRow pRowhead, HSSFCellStyle pRowCellStyle) {
		pRowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		pRowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		pRowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
		pRowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
		pRowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
		pRowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

		pRowhead.createCell(TRUProductAnalyticsFeedConstants.NUMBER_ZERO).setCellValue(getTRUBestSellerFeedEnvConfiguration().getFileName());
		pRowhead.createCell(TRUProductAnalyticsFeedConstants.NUMBER_ONE).setCellValue(getTRUBestSellerFeedEnvConfiguration().getRecordNumber());
		pRowhead.createCell(TRUProductAnalyticsFeedConstants.NUMBER_TWO).setCellValue(getTRUBestSellerFeedEnvConfiguration().getTimeStamp());
		pRowhead.createCell(TRUProductAnalyticsFeedConstants.NUMBER_THREE).setCellValue(getTRUBestSellerFeedEnvConfiguration().getAttachmentItemIdLabel());
		pRowhead.createCell(TRUProductAnalyticsFeedConstants.NUMBER_FOUR).setCellValue(getTRUBestSellerFeedEnvConfiguration().getAttachmentExceptionReasonLabel());

		pRowhead.getCell(TRUProductAnalyticsFeedConstants.NUMBER_ZERO).setCellStyle(pRowCellStyle);
		pRowhead.getCell(TRUProductAnalyticsFeedConstants.NUMBER_ONE).setCellStyle(pRowCellStyle);
		pRowhead.getCell(TRUProductAnalyticsFeedConstants.NUMBER_TWO).setCellStyle(pRowCellStyle);
		pRowhead.getCell(TRUProductAnalyticsFeedConstants.NUMBER_THREE).setCellStyle(pRowCellStyle);
		pRowhead.getCell(TRUProductAnalyticsFeedConstants.NUMBER_FOUR).setCellStyle(pRowCellStyle);
	}
	
	
	/**
	 * Attachement file name. This method used to construct the attached file name
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachementFileName(String pFileName) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		String newFileName = pFileName.substring(TRUProductAnalyticsFeedConstants.NUMBER_ZERO, pFileName.indexOf(TRUProductAnalyticsFeedConstants.DOT) + TRUProductAnalyticsFeedConstants.NUMBER_ONE);
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = stringBuffer.append(newFileName).append(TRUProductAnalyticsFeedConstants.XLS);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		return stringBuffer.toString();
	}
	
	/**
	 * Trigger feed processed with errors email.
	 *
	 * @param pServiceMap the service map
	 * @param pSubject the subject
	 * @param pSuccessRecordCount the success record count
	 * @param pFailedRecordsCount the failed records count
	 * @param pSkuNotFoundRecordsCount the sku not found records count
	 * @param pInvalidRecordsFile the invalid records file
	 */
	protected void triggerFeedProcessedWithErrorsEmail(
			Map<String, Object> pServiceMap,
			String pSubject, int pSuccessRecordCount,
			int pFailedRecordsCount, int pSkuNotFoundRecordsCount,
			File pInvalidRecordsFile) {

		getLogger().vlogDebug(
				"Start @Class: TRUProductAnalyticsFeedLoader, @method: triggerFeedProcessedSuccessfullyEmail()");
		Map<String, Object> serviceMap = pServiceMap;
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = getTRUBestSellerFeedEnvConfiguration().getBestSellerSuccessWithErrors();
		
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pSubject);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pSuccessRecordCount+
				pFailedRecordsCount + pSkuNotFoundRecordsCount);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pFailedRecordsCount + pSkuNotFoundRecordsCount);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		serviceMap.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER, templateParameters);
		getTRUBestSellerFeedEmailService().sendFeedSuccessEmail(serviceMap, pInvalidRecordsFile);
		getLogger().vlogDebug(
				"End @Class: TRUProductAnalyticsFeedLoader, @method: triggerFeedProcessedSuccessfullyEmail()");

	
		
	}


	/**
	 * Trigger feed processed successfully email.
	 *
	 * @param pServiceMap the service map
	 * @param pSubject the subject
	 * @param pSuccessRecordCount the success record count
	 * @param pFailedRecordsCount the failed records count
	 * @param pSkuNotFoundRecordsCount the sku not found records count
	 */
	public void triggerFeedProcessedSuccessfullyEmail(Map<String, Object> pServiceMap, String pSubject, int pSuccessRecordCount,
			int pFailedRecordsCount, int pSkuNotFoundRecordsCount) {

		getLogger().vlogDebug(
				"Start @Class: TRUProductAnalyticsFeedLoader, @method: triggerFeedProcessedSuccessfullyEmail()");
		Map<String, Object> serviceMap = pServiceMap;
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		
		String content = getTRUBestSellerFeedEnvConfiguration().getBestSellerSuccess();
		
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pSubject);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pSuccessRecordCount+
				pFailedRecordsCount + pSkuNotFoundRecordsCount);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, pFailedRecordsCount + pSkuNotFoundRecordsCount);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		serviceMap.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER, templateParameters);
		getTRUBestSellerFeedEmailService().sendFeedSuccessEmail(serviceMap, null);
		getLogger().vlogDebug(
				"End @Class: TRUProductAnalyticsFeedLoader, @method: triggerFeedProcessedSuccessfullyEmail()");

	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
