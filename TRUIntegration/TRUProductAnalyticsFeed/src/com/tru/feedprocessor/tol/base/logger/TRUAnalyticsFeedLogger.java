package com.tru.feedprocessor.tol.base.logger;

import atg.nucleus.GenericService;

/**
 * The Class TRUAnalyticsFeedLogger.
 * 
 * It is used to capture the log information in any of the implemented classes
 * 
 * @version 1.1
 * @author Professional Access
 */
public class TRUAnalyticsFeedLogger extends GenericService {

	/**
	 * Log debug message.
	 * 
	 * @param pMessage
	 *            the message
	 */
	public void logDebugMessage(Object pMessage) {
		if (isLoggingDebug() && null != pMessage) {
			logDebug(pMessage.toString());
		}
	}

	/**
	 * Log error message.
	 * 
	 * @param pMessage
	 *            the message
	 * @param pException
	 *            the exception
	 */
	public void logErrorMessage(Object pMessage, Throwable pException) {
		if (isLoggingError() && null != pMessage) {
			logError(pMessage.toString(), pException);
		}
	}

	/**
	 * Log info message.
	 * 
	 * @param pMessage
	 *            the message
	 */
	public void logInfoMessage(Object pMessage) {
		if (isLoggingInfo() && null != pMessage) {
			logInfo(pMessage.toString());
		}
	}
}
