package com.tru.feedprocessor.tol.base.service;

import atg.nucleus.GenericService;

/**
 * The Class TRUProductAnalyticsFeedProperty.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedProperty extends GenericService {
	
	/** The m id. */
	private String mId;
	
	/** The m sku. */
	private String mSku;
	
	/** The m productanalytics. */
	private String mProductanalytics;
	
	/** The m quantity. */
	private String mQuantity;
	
	/** The m parent products. */
	private String mParentProducts;
	
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return mId;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param pId the id to set
	 */
	public void setId(String pId) {
		mId = pId;
	}
	
	/**
	 * Gets the sku.
	 *
	 * @return the sku
	 */
	public String getSku() {
		return mSku;
	}
	
	/**
	 * Sets the sku.
	 *
	 * @param pSku the sku to set
	 */
	public void setSku(String pSku) {
		mSku = pSku;
	}
	
	/**
	 * Gets the productanalytics.
	 *
	 * @return the productanalytics
	 */
	public String getProductanalytics() {
		return mProductanalytics;
	}
	
	/**
	 * Sets the productanalytics.
	 *
	 * @param pProductanalytics the productanalytics to set
	 */
	public void setProductanalytics(String pProductanalytics) {
		mProductanalytics = pProductanalytics;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public String getQuantity() {
		return mQuantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param pQuantity the quantity to set
	 */
	public void setQuantity(String pQuantity) {
		mQuantity = pQuantity;
	}
	
	/**
	 * Gets the parent products.
	 *
	 * @return the parentProducts
	 */
	public String getParentProducts() {
		return mParentProducts;
	}
	
	/**
	 * Sets the parent products.
	 *
	 * @param pParentProducts the parentProducts to set
	 */
	public void setParentProducts(String pParentProducts) {
		mParentProducts = pParentProducts;
	}
	
	
}
