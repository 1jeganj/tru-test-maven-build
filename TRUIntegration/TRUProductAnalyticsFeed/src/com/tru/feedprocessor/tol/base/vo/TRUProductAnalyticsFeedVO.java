package com.tru.feedprocessor.tol.base.vo;

/**
 * The Class TRUProductAnalyticsFeedVO.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedVO {
	
	/** The Sku id. */
	private String mSkuId;
	
	/** The Quantity. */
	private long mQuantity;
	
	/**
	 * Gets the sku id.
	 *
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	
	/**
	 * Sets the sku id.
	 *
	 * @param pSkuId the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public long getQuantity() {
		return mQuantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param pQuantity the quantity to set
	 */
	public void setQuantity(long pQuantity) {
		mQuantity = pQuantity;
	}
	
	
}
