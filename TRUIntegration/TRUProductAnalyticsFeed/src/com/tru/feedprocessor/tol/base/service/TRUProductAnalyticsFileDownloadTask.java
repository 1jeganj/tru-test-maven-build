package com.tru.feedprocessor.tol.base.service;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.filefilter.RegexFileFilter;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.feedprocessor.email.TRUBestSellerFeedEmailConstants;
import com.tru.feedprocessor.email.TRUBestSellerFeedEmailService;
import com.tru.feedprocessor.email.TRUBestSellerFeedEnvConfiguration;
import com.tru.feedprocessor.tol.base.constants.TRUProductAnalyticsFeedConstants;
import com.tru.feedprocessor.tol.base.exception.TRUAnalyticsFeedException;
import com.tru.feedprocessor.tol.base.logger.TRUAnalyticsFeedLogger;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUProductAnalyticsFileDownloadTask.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFileDownloadTask extends GenericService {
	
	/** The mLogger. */
	private TRUAnalyticsFeedLogger mLogger;
	
	/** The mTRUProductAnalyticsFeedConfiguration. */
	private TRUProductAnalyticsFeedConfiguration mTRUProductAnalyticsFeedConfiguration;
	
	/** The TRU best seller feed env configuration. */
	private TRUBestSellerFeedEnvConfiguration mTRUBestSellerFeedEnvConfiguration;
	
	/** The TRU best seller feed email service. */
	private TRUBestSellerFeedEmailService mTRUBestSellerFeedEmailService;
	

	/**
	 * Gets the TRU best seller feed env configuration.
	 *
	 * @return the tRUBestSellerFeedEnvConfiguration
	 */
	public TRUBestSellerFeedEnvConfiguration getTRUBestSellerFeedEnvConfiguration() {
		return mTRUBestSellerFeedEnvConfiguration;
	}


	/**
	 * Sets the TRU best seller feed env configuration.
	 *
	 * @param pTRUBestSellerFeedEnvConfiguration the tRUBestSellerFeedEnvConfiguration to set
	 */
	public void setTRUBestSellerFeedEnvConfiguration(
			TRUBestSellerFeedEnvConfiguration pTRUBestSellerFeedEnvConfiguration) {
		mTRUBestSellerFeedEnvConfiguration = pTRUBestSellerFeedEnvConfiguration;
	}


	/**
	 * Gets the TRU best seller feed email service.
	 *
	 * @return the tRUBestSellerFeedEmailService
	 */
	public TRUBestSellerFeedEmailService getTRUBestSellerFeedEmailService() {
		return mTRUBestSellerFeedEmailService;
	}


	/**
	 * Sets the TRU best seller feed email service.
	 *
	 * @param pTRUBestSellerFeedEmailService the tRUBestSellerFeedEmailService to set
	 */
	public void setTRUBestSellerFeedEmailService(
			TRUBestSellerFeedEmailService pTRUBestSellerFeedEmailService) {
		mTRUBestSellerFeedEmailService = pTRUBestSellerFeedEmailService;
	}


	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public TRUAnalyticsFeedLogger getLogger() {
		return mLogger;
	}
	

	/**
	 * Gets the TRU product analytics feed configuration.
	 *
	 * @return the tRUProductAnalyticsFeedConfiguration
	 */
	public TRUProductAnalyticsFeedConfiguration getTRUProductAnalyticsFeedConfiguration() {
		return mTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Sets the TRU product analytics feed configuration.
	 *
	 * @param pTRUProductAnalyticsFeedConfiguration the tRUProductAnalyticsFeedConfiguration to set
	 */
	public void setTRUProductAnalyticsFeedConfiguration(
			TRUProductAnalyticsFeedConfiguration pTRUProductAnalyticsFeedConfiguration) {
		mTRUProductAnalyticsFeedConfiguration = pTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Sets the logger.
	 *
	 * @param pLogger the logger to set
	 */
	public void setLogger(TRUAnalyticsFeedLogger pLogger) {
		mLogger = pLogger;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
	
	/**
	 * Do process.
	 *
	 * @param pServiceMap the service map
	 * @return true, if successful
	 * @throws TRUAnalyticsFeedException the TRU analytics feed exception
	 */
	public boolean doProcess(Map<String, Object> pServiceMap) throws TRUAnalyticsFeedException {
		
		getLogger().vlogDebug("Start @Class: TRUProductAnalyticsFileDownloadTask, @method: doProcess()");
		// mSourceDir will gets the source directory from the storeFeedConfig
		boolean retVal = Boolean.FALSE;
		String sourceDir = getTRUProductAnalyticsFeedConfiguration().getSourceDirectory();
		String processingDir = getTRUProductAnalyticsFeedConfiguration().getProcessingDirectory();
		String fileExtension = getTRUProductAnalyticsFeedConfiguration().getFileType();
		String filePattern = getTRUProductAnalyticsFeedConfiguration().getFileName();
		File sharedFolderFile = new File(sourceDir);
		if (sharedFolderFile.exists() && sharedFolderFile.isDirectory() && sharedFolderFile.canRead()) {
			retVal =  moveSrcToProcessingDir(sourceDir, processingDir, fileExtension, filePattern, pServiceMap);
		}

		getLogger().vlogDebug("End @Class: TRUProductAnalyticsFileDownloadTask, @method: doProcess()");
		
		return retVal;

	}
	
	/**
	 * Move src to processing dir.
	 *
	 * @param pSourceDir            the source dir
	 * @param pProcessingDir            the processing dir
	 * @param pFiletype            the filetype
	 * @param pFilePattern            the file pattern
	 * @param pServiceMap the service map
	 * @return true, if successful
	 * @throws TRUAnalyticsFeedException the TRU analytics feed exception
	 */
	public boolean moveSrcToProcessingDir(String pSourceDir, String pProcessingDir, String pFiletype, String pFilePattern,
			Map<String, Object> pServiceMap) throws TRUAnalyticsFeedException{
		getLogger().vlogDebug("Start @Class: TRUProductAnalyticsFileDownloadTask, @method: moveSrcToProcessingDir()");
		boolean retVal = Boolean.FALSE;
		String startDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		File sourceFolder = new File(pSourceDir);
		File processingFolder = new File(pProcessingDir);
		List<File> fileLists = null;
		String[] feedFileNames = null;
		StringBuffer filesList = new StringBuffer();
		filesList.setLength(0);
		String timestamp = null;
		String name = null;
		String fileName = null;
		int fileCount = 0;
		List<Date> fileDateList = new ArrayList<Date>();
		boolean filesExists = Boolean.FALSE;
		Map<String, Object> serviceMap = pServiceMap;
		Date date = null;
		boolean sendFileDoesNotExistMail = false;
		
		
		if (sourceFolder != null && sourceFolder.exists() && sourceFolder.canRead()) {
			fileLists = fileLists(pSourceDir, pFilePattern);
			if (!fileLists.isEmpty()) {
				for (File srcFile : fileLists) {
					name = srcFile.getName();
					feedFileNames = name.split(TRUProductAnalyticsFeedConstants.DOUBLE_BACKWARDSLASH_UNDERSCORE);
					timestamp = feedFileNames[TRUProductAnalyticsFeedConstants.ONE];
					Format feedFormatter = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT, Locale.getDefault());
					Format actuallDateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.DATE_FORMAT, Locale.getDefault());
					try {
						date = (Date) ((DateFormat) feedFormatter).parse(timestamp);
					} catch (java.text.ParseException pe) {
						getLogger().logError(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.PARSE_EXCEPTION);
						throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.PARSE_EXCEPTION, pe);
					}
					String feedName = actuallDateFormat.format(date);
					String todayDate = actuallDateFormat.format(new Date());
					if (name.contains(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED) && todayDate.equalsIgnoreCase(feedName)) {
						filesExists = Boolean.TRUE;
						if (fileCount == 0) {
							fileDateList.add(date);
							fileName = name + TRUProductAnalyticsFeedConstants.DOLLAR_ONLY;
						} else if (fileCount > 0 && date.after(fileDateList.get(fileDateList.size() - TRUProductAnalyticsFeedConstants.ONE))) {
							fileDateList.add(date);
							fileName = name + TRUProductAnalyticsFeedConstants.DOLLAR_ONLY;
						}
						fileCount++;
					}
				}
				fileDateList.clear();
				if(filesExists){
					filesList.append(fileName);
					retVal = movefiles(processingFolder, sourceFolder, filesList, serviceMap);
					if(retVal){
						logSuccessMessage(startDate, TRUProductAnalyticsFeedConstants.FILES_MOVED_SUCCESS);
					}
				} else{
					sendFileDoesNotExistMail = true;
				}
				
			} else{
				sendFileDoesNotExistMail = true;
			}
		}
		
		if(sendFileDoesNotExistMail){
			getLogger().logError(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.NO_FILES_TO_DOWNLOAD);
			
			String sub = getTRUBestSellerFeedEnvConfiguration().getBestSellerFailureSub();
			String[] args = new String[TRUProductAnalyticsFeedConstants.NUMBER_TWO];
			args[TRUProductAnalyticsFeedConstants.NUMBER_ZERO] = getTRUBestSellerFeedEnvConfiguration().getEnv();
			Date currentDate = new Date();
			DateFormat dateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.SUB_DATE_FORMAT);
			args[TRUProductAnalyticsFeedConstants.NUMBER_ONE] = dateFormat.format(currentDate);
			String formattedSubject = String.format(sub, args);
			String priority = getTRUBestSellerFeedEnvConfiguration().getFailureMailPriority2();
			String detailException = getTRUBestSellerFeedEnvConfiguration().getDetailedExceptionForNoFeed();
			
			notifyFileDoseNotExits(serviceMap, formattedSubject,
					getTRUProductAnalyticsFeedConfiguration().getSourceDirectory(), priority, detailException);
			logFailedMessage(startDate, TRUProductAnalyticsFeedConstants.NO_FILES_TO_DOWNLOAD);
			throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.FILE_DOWNLOAD_FAILED);
			
		}
		
		getLogger().vlogDebug("End @Class: TRUProductAnalyticsFileDownloadTask, @method: moveSrcToProcessingDir()");
		
		return retVal;
	}
	
	/**
	 * Log failed message.
	 
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductAnalyticsFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductAnalyticsFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductAnalyticsFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED, TRUProductAnalyticsFeedConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUProductAnalyticsFeedConstants.MESSAGE, pMessage);
		extenstions.put(TRUProductAnalyticsFeedConstants.START_TIME, pStartDate);
		extenstions.put(TRUProductAnalyticsFeedConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUProductAnalyticsFeedConstants.BEST_SELLER_FEED, TRUProductAnalyticsFeedConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	/**
	 * The method is used to move files from source.
	 * 
	 * @param pProcessingFolder
	 *            the processing folder
	 * @param pSourceFolder
	 *            the source folder
	 * @param pFilesList
	 *            the files list
	 * @param pServiceMap
	 *            the service map
	 * @return true, if successful
	 * @throws TRUAnalyticsFeedException
	 *             the TRU analytics feed exception
	 */
	private boolean movefiles(File pProcessingFolder, File pSourceFolder, StringBuffer pFilesList, Map<String, Object> pServiceMap)
			throws TRUAnalyticsFeedException {
		getLogger().vlogDebug("Start @Class: TRUFileDownloadTask, @method: movefiles()");
		boolean feedfilesExists = Boolean.FALSE;
		if (pProcessingFolder != null && pProcessingFolder.exists() && pProcessingFolder.canWrite()) {
			try {
				feedfilesExists = moveAllFiles(pSourceFolder, pProcessingFolder.getCanonicalPath(), pFilesList, pServiceMap);
				pFilesList.setLength(0);
				if (feedfilesExists) {
					getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.DOWNLOAD_SUCCESS);
				} else {
					
					getLogger().logError(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.NO_FILES_TO_DOWNLOAD);
					throw new TRUAnalyticsFeedException(TRUProductAnalyticsFeedConstants.FILE_DOWNLOAD_FAILED);
				}
			} catch (FileNotFoundException e) {
				if (isLoggingError()) {
					logError(TRUProductAnalyticsFeedConstants.FILE_NOT_FOUND_EXCEPTION, e);
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError(TRUProductAnalyticsFeedConstants.IOEXCEPTION, e);
				}
			}
		}
		getLogger().vlogDebug("End @Class: TRUFileDownloadTask, @method: movefiles()");
		return feedfilesExists;

	}
	
	/**
	 * Move all files.
	 *
	 * @param pSharedDir            the shared dir
	 * @param pProcessingDir            the processing dir
	 * @param pFilesList            the files list
	 * @param pServiceMap the service map
	 * @return true, if successful
	 * @throws TRUAnalyticsFeedException the TRU analytics feed exception
	 */
	public boolean moveAllFiles(File pSharedDir, String pProcessingDir, StringBuffer pFilesList, Map<String, Object> pServiceMap)
			throws TRUAnalyticsFeedException {
		String fileNames = null;
		Map<String, Object> serviceMap = pServiceMap;
		boolean fileMove = Boolean.FALSE;
		if (pSharedDir.exists() && pSharedDir.isDirectory()) {
			fileNames = pFilesList.toString();
		}
		String[] feedNames = fileNames.split(TRUProductAnalyticsFeedConstants.DOLLAR_ONLY);
		if (feedNames.length > 0) {
			for (String feedsName : feedNames) {
				// move files to the folder
				if (!StringUtils.isBlank(feedsName)) {
					serviceMap.put(TRUProductAnalyticsFeedConstants.FEED_FILE_NAME, feedsName);
					fileMove = moveFileToFolder(pSharedDir + TRUProductAnalyticsFeedConstants.SLASH + feedsName, pProcessingDir);
					moveRemainingFilesToUnprocessedFolder(pSharedDir, serviceMap,
							getTRUProductAnalyticsFeedConfiguration().getUnproccessedDirectory());
				}
			}
		}
		return fileMove;
	}
	
	/**
	 * Move remaining files to unprocessed folder.
	 *
	 * @param pFromDir the from dir
	 * @param pServiceMap the service map
	 * @param pToDir the to dir
	 */
	public void moveRemainingFilesToUnprocessedFolder(File pFromDir, Map<String, Object> pServiceMap, String pToDir){
		File[] filesInToFolder = pFromDir.listFiles();
		if(filesInToFolder.length > TRUBestSellerFeedEmailConstants.ZERO){
			List<String> filesMovedToUnprocessedFolder = new ArrayList<String>();
			for(File file: filesInToFolder){
				filesMovedToUnprocessedFolder.add(file.getName());
				File moveToFolder = new File(pToDir);
				if (!(moveToFolder.exists()) && !(moveToFolder.isDirectory())) {
					moveToFolder.mkdirs();
				}
				file.renameTo(new File(moveToFolder, file.getName()));
	    	}
			notifyFilesMovedToUnproccessedFolder(pServiceMap, filesMovedToUnprocessedFolder,
					getTRUBestSellerFeedEnvConfiguration().getBestSellerFilesMovedToUnproccessedFolderSubject());
		}
	}
	
	/**
	 * This method is used for moving the file from source directory to destination directory.
	 *
	 * @param pFromFile            - String
	 * @param pToFolder            - String
	 * @return boolean - value
	 * @throws TRUAnalyticsFeedException the TRU analytics feed exception
	 */
	public boolean moveFileToFolder(String pFromFile, String pToFolder) throws TRUAnalyticsFeedException {
		File fromFile = new File(pFromFile);
		File toFolder = new File(pToFolder);
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		File[] filesInToFolder = toFolder.listFiles();
		for(File file: filesInToFolder){
			File moveToFolder = new File(getTRUProductAnalyticsFeedConfiguration().getUnproccessedDirectory());
			if (!(moveToFolder.exists()) && !(moveToFolder.isDirectory())) {
				moveToFolder.mkdirs();
			}
			file.renameTo(new File(moveToFolder, file.getName()));
    	}
		boolean value = fromFile.renameTo(new File(toFolder, fromFile.getName()));
		return value;
	}
	
	/**
	 * File lists.
	 * 
	 * @param pPath
	 *            - pPath
	 * @param pPattern
	 *            - pPattern
	 * @return the patternMatchfiles
	 */
	private List<File> fileLists(String pPath, String pPattern) {
		List<File> patternMatchingfiles = new ArrayList<File>();
		File dir = new File(pPath);
		getLogger().vlogDebug(pPattern);
			FileFilter filter = new RegexFileFilter(pPattern);
			File[] patternMatchfiles = dir.listFiles(filter);
			if (patternMatchfiles != null) {
				for (File files : patternMatchfiles) {
					getLogger().vlogDebug(files.getName());
					patternMatchingfiles.add(files);
				}
			}
		return patternMatchingfiles;
	}
	
	/**
	 * Notify file dose not exits.
	 *
	 * @param pServiceMap the service map
	 * @param pSubject the subject
	 * @param pSource the source
	 * @param pPriority the priority
	 * @param pDetailException the detail exception
	 */
	public void notifyFileDoseNotExits(Map<String, Object> pServiceMap , String pSubject, String pSource, String pPriority, String pDetailException) {

		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");
		
		Map<String, Object> serviceMap = pServiceMap;
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pSubject);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pPriority);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, pDetailException);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getTRUBestSellerFeedEnvConfiguration()
				.getOSHostName());
		serviceMap.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER, templateParameters);
		getTRUBestSellerFeedEmailService().sendFeedFailureEmail(serviceMap);
		getLogger().logInfo(TRUProductAnalyticsFeedConstants.SPACE + TRUProductAnalyticsFeedConstants.FEED_PROCESS_FAILED);
		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFileDoseNotExits()");
	}
	
	/**
	 * Notify files moved to unproccessed folder.
	 *
	 * @param pServiceMap the service map
	 * @param pFilesMovedList the files moved list
	 * @param pSubject the subject
	 */
	public void notifyFilesMovedToUnproccessedFolder(Map<String, Object> pServiceMap ,List<String> pFilesMovedList, String pSubject){
		getLogger().vlogDebug("Start @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFilesMovedToUnproccessedFolder()");
		
		Map<String, Object> serviceMap = pServiceMap;
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, pSubject);
		templateParameters.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER_FILES_LIST, pFilesMovedList);
		serviceMap.put(TRUBestSellerFeedEmailConstants.TEMPLATE_PARAMETER, templateParameters);
		getTRUBestSellerFeedEmailService().sendFilesMovedToUnproccessedFolderEmail(serviceMap);
		
		getLogger().vlogDebug("End @Class: TRUCatFeedExecutionDataCollectorTasklet, @method: notifyFilesMovedToUnproccessedFolder()");
		
	}
}
