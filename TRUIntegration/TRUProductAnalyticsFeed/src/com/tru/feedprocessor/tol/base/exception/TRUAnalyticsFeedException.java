package com.tru.feedprocessor.tol.base.exception;

/**
 * The Class TRUAnalyticsFeedException.
 * 
 * This is the exception class which will be used for feed process
 * If any error comes then we will catch the exception in abstract class
 * and create a object of this class and it will be stored in the step execution.
 * 
 * @version 1.1
 * @author Professional Access
 */
public class TRUAnalyticsFeedException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6166762066097275207L;
	
	/**
	 * FeedSkippableException constructor.
	 * 
	 * @param pMessage the Message
	 */
	public TRUAnalyticsFeedException(String pMessage){
		super(pMessage);
	}

	/**
	 * FeedSkippableException constructor.
	 *
	 * @param pMessage the Message
	 * @param pException the exception
	 */
	public TRUAnalyticsFeedException(String pMessage, Exception pException){
		super(pMessage, pException);
	}
}
