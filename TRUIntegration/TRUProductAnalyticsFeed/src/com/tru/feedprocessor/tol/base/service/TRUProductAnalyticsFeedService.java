package com.tru.feedprocessor.tol.base.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import atg.nucleus.GenericService;

import com.tru.feedprocessor.email.TRUBestSellerFeedEnvConfiguration;
import com.tru.feedprocessor.tol.base.constants.TRUProductAnalyticsFeedConstants;
import com.tru.feedprocessor.tol.base.exception.TRUAnalyticsFeedException;
import com.tru.feedprocessor.tol.base.processor.TRUProductAnalyticsFeedMapper;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUProductAnalyticsFeedService.
 * @author PA
 * @version 1.0
 */
public class TRUProductAnalyticsFeedService extends GenericService {
	
	/** The m tru product analytics file download task. */
	private TRUProductAnalyticsFileDownloadTask mTRUProductAnalyticsFileDownloadTask;
	
	/** The m tru product analytics feed loader. */
	private TRUProductAnalyticsFeedLoader mTRUProductAnalyticsFeedLoader;
	
	/** The m tru product analytics feed mapper. */
	private TRUProductAnalyticsFeedMapper mTRUProductAnalyticsFeedMapper;
	
	/** The TRU best seller feed env configuration. */
	private TRUBestSellerFeedEnvConfiguration mTRUBestSellerFeedEnvConfiguration;
	
	/** The TRU product analytics feed configuration. */
	private TRUProductAnalyticsFeedConfiguration mTRUProductAnalyticsFeedConfiguration;
	
	
	
	/**
	 * Gets the TRU product analytics feed configuration.
	 *
	 * @return the tRUProductAnalyticsFeedConfiguration
	 */
	public TRUProductAnalyticsFeedConfiguration getTRUProductAnalyticsFeedConfiguration() {
		return mTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Sets the TRU product analytics feed configuration.
	 *
	 * @param pTRUProductAnalyticsFeedConfiguration the tRUProductAnalyticsFeedConfiguration to set
	 */
	public void setTRUProductAnalyticsFeedConfiguration(
			TRUProductAnalyticsFeedConfiguration pTRUProductAnalyticsFeedConfiguration) {
		mTRUProductAnalyticsFeedConfiguration = pTRUProductAnalyticsFeedConfiguration;
	}



	/**
	 * Gets the TRU best seller feed env configuration.
	 *
	 * @return the tRUBestSellerFeedEnvConfiguration
	 */
	public TRUBestSellerFeedEnvConfiguration getTRUBestSellerFeedEnvConfiguration() {
		return mTRUBestSellerFeedEnvConfiguration;
	}



	/**
	 * Sets the TRU best seller feed env configuration.
	 *
	 * @param pTRUBestSellerFeedEnvConfiguration the tRUBestSellerFeedEnvConfiguration to set
	 */
	public void setTRUBestSellerFeedEnvConfiguration(
			TRUBestSellerFeedEnvConfiguration pTRUBestSellerFeedEnvConfiguration) {
		mTRUBestSellerFeedEnvConfiguration = pTRUBestSellerFeedEnvConfiguration;
	}



	/**
	 * Gets the TRU product analytics feed mapper.
	 *
	 * @return the tRUProductAnalyticsFeedMapper
	 */
	public TRUProductAnalyticsFeedMapper getTRUProductAnalyticsFeedMapper() {
		return mTRUProductAnalyticsFeedMapper;
	}



	/**
	 * Sets the TRU product analytics feed mapper.
	 *
	 * @param pTRUProductAnalyticsFeedMapper the tRUProductAnalyticsFeedMapper to set
	 */
	public void setTRUProductAnalyticsFeedMapper(TRUProductAnalyticsFeedMapper pTRUProductAnalyticsFeedMapper) {
		mTRUProductAnalyticsFeedMapper = pTRUProductAnalyticsFeedMapper;
	}



	/**
	 * Gets the TRU product analytics feed loader.
	 *
	 * @return the tRUProductAnalyticsFeedLoader
	 */
	public TRUProductAnalyticsFeedLoader getTRUProductAnalyticsFeedLoader() {
		return mTRUProductAnalyticsFeedLoader;
	}



	/**
	 * Sets the TRU product analytics feed loader.
	 *
	 * @param pTRUProductAnalyticsFeedLoader the tRUProductAnalyticsFeedLoader to set
	 */
	public void setTRUProductAnalyticsFeedLoader(TRUProductAnalyticsFeedLoader pTRUProductAnalyticsFeedLoader) {
		mTRUProductAnalyticsFeedLoader = pTRUProductAnalyticsFeedLoader;
	}



	/**
	 * Gets the TRU product analytics file download task.
	 *
	 * @return the tRUProductAnalyticsFileDownloadTask
	 */
	public TRUProductAnalyticsFileDownloadTask getTRUProductAnalyticsFileDownloadTask() {
		return mTRUProductAnalyticsFileDownloadTask;
	}



	/**
	 * Sets the TRU product analytics file download task.
	 *
	 * @param pTRUProductAnalyticsFileDownloadTask the tRUProductAnalyticsFileDownloadTask to set
	 */
	public void setTRUProductAnalyticsFileDownloadTask(TRUProductAnalyticsFileDownloadTask pTRUProductAnalyticsFileDownloadTask) {
		mTRUProductAnalyticsFileDownloadTask = pTRUProductAnalyticsFileDownloadTask;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}




	/**
	 * Do service.
	 */
	public void doService(){
		try{
			
			boolean isInvalidFeed = false;
			
			Map<String, Object> serviceMap = new HashMap<String, Object>();
			if(getTRUProductAnalyticsFileDownloadTask().doProcess(serviceMap)){
				getTRUProductAnalyticsFeedLoader().parseFile(serviceMap);
				if(getTRUProductAnalyticsFeedLoader().validateFile(serviceMap)){
					if(getTRUProductAnalyticsFeedMapper().createvosAndMapToRepository(serviceMap)){
						getTRUProductAnalyticsFeedLoader().moveFileToSuccessFolder(serviceMap);
					}
					getTRUProductAnalyticsFeedLoader().moveInvalidRecordsToErrorDir(serviceMap);
				}  else {
					isInvalidFeed = true;
					
				}
			}
			
			if(isInvalidFeed){
				
				String sub = getTRUBestSellerFeedEnvConfiguration().getBestSellerFailureSub();
				String[] args = new String[TRUProductAnalyticsFeedConstants.NUMBER_TWO];
				args[TRUProductAnalyticsFeedConstants.NUMBER_ZERO] = getTRUBestSellerFeedEnvConfiguration().getEnv();
				Date currentDate = new Date();
				DateFormat dateFormat = new SimpleDateFormat(TRUProductAnalyticsFeedConstants.SUB_DATE_FORMAT);
				args[TRUProductAnalyticsFeedConstants.NUMBER_ONE] = dateFormat.format(currentDate);
				String formattedSubject = String.format(sub, args);
				String priority = getTRUBestSellerFeedEnvConfiguration().getFailureMailPriority2();
				String detailedException = getTRUBestSellerFeedEnvConfiguration().getDetailedExceptionForInvalidFeed();
				
				getTRUProductAnalyticsFileDownloadTask().notifyFileDoseNotExits(serviceMap, formattedSubject,
						getTRUProductAnalyticsFeedConfiguration().getBadDirectory(), priority, detailedException);
			}
			
		} catch (TRUAnalyticsFeedException fe) {
			vlogError("TRUAnalsyst Exception Thrown", fe);
		}	
	}
	
	
	
}
