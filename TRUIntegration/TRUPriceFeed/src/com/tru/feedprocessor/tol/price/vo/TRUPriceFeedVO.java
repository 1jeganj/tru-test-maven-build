package com.tru.feedprocessor.tol.price.vo;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;

/**
 * The Class PriceFeedVO.
 *  @version 1.0
 *  @author Professional Access
 */
public class TRUPriceFeedVO extends BaseFeedProcessVO {
	
	/** property to hold countryCode. */
	private String mCountryCode;

	/** property to hold displayName. */
	private String mDisplayName;

	/** property to hold listPrice. */
	private String mListPrice;

	/** property to hold locale. */
	private String mLocale;

	/** property to hold marketCode. */
	private String mMarketCode;

	/** property to hold skuId. */
	private String mPriceId;

	/** property to hold priceListId. */
	private String mPriceListId;

	/** property to hold salePrice. */
	private String mSalePrice;

	/** property to hold skuId. */
	private String mSkuId;

	/** property to hold storeNumber. */
	private String mStoreNumber;

	/** property to hold dealId. */
	private String mDealId;

	/**
	 * Instantiates a new product.
	 */
	public TRUPriceFeedVO() {
		super.setEntityName(FeedConstants.PRICE_ENTITY_NAME);
	}

	/**
	 * Gets the country code.
	 * 
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * Gets the display name.
	 * 
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Gets the list price.
	 * 
	 * @return the listPrice
	 */
	public String getListPrice() {
		return mListPrice;
	}

	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * Gets the market code.
	 * 
	 * @return the marketCode
	 */
	public String getMarketCode() {
		return mMarketCode;
	}

	/**
	 * Gets the price id.
	 * 
	 * @return the priceId
	 */
	public String getPriceId() {
		return mPriceId;
	}

	/**
	 * Gets the price list id.
	 * 
	 * @return the priceListId
	 */
	public String getPriceListId() {
		return mPriceListId;
	}

	/**
	 * Gets the sale price.
	 * 
	 * @return the salePrice
	 */
	public String getSalePrice() {
		return mSalePrice;
	}

	/**
	 * Gets the sku id.
	 * 
	 * @return the skuId
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Gets the store number.
	 * 
	 * @return the storeNumber
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}

	/**
	 * Sets the country code.
	 * 
	 * @param pCountryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String pCountryCode) {
		mCountryCode = pCountryCode;
	}

	/**
	 * Sets the display name.
	 * 
	 * @param pDisplayName
	 *            the new display name
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	/**
	 * Sets the list price.
	 * 
	 * @param pListPrice
	 *            the listPrice to set
	 */
	public void setListPrice(String pListPrice) {
		mListPrice = pListPrice;
	}

	/**
	 * Sets the locale.
	 * 
	 * @param pLocale
	 *            the locale to set
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}

	/**
	 * Sets the market code.
	 * 
	 * @param pMarketCode
	 *            the marketCode to set
	 */
	public void setMarketCode(String pMarketCode) {
		mMarketCode = pMarketCode;
	}

	/**
	 * Sets the price id.
	 * 
	 * @param pPriceId
	 *            the priceId to set
	 */
	public void setPriceId(String pPriceId) {
		mPriceId = pPriceId;
	}

	/**
	 * Sets the price list id.
	 * 
	 * @param pPriceListId
	 *            the priceListId to set
	 */
	public void setPriceListId(String pPriceListId) {
		mPriceListId = pPriceListId;
	}

	/**
	 * Sets the sale price.
	 * 
	 * @param pSalePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * Sets the sku id.
	 * 
	 * @param pSkuId
	 *            the skuId to set
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Sets the store number.
	 * 
	 * @param pStoreNumber
	 *            the storeNumber to set
	 */
	public void setStoreNumber(String pStoreNumber) {
		mStoreNumber = pStoreNumber;
	}

	/**
	 * Gets the m deal id.
	 * 
	 * @return the mDealId
	 */
	public String getDealId() {
		return mDealId;
	}

	/**
	 * Sets the m deal id.
	 * 
	 * @param pDealId
	 *            the new deal id
	 */
	public void setDealId(String pDealId) {
		this.mDealId = pDealId;
	}

	/**
	 * Convert to String.
	 * 
	 * @return the final PricetId
	 */
	@Override
	public String toString() {
		StringBuffer finalPricetId = new StringBuffer();
		finalPricetId.append(getSkuId()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getListPrice()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getSalePrice()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getLocale()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getCountryCode()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getMarketCode()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getStoreNumber()).append(TRUPriceFeedReferenceConstants.PIPE_DELIMITER_STRING)
				.append(getDealId());
		return finalPricetId.toString();
	}

}
