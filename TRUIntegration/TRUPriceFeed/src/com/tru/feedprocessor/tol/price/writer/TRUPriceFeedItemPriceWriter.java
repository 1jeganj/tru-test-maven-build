package com.tru.feedprocessor.tol.price.writer;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFeedItemPriceWriter is extended to implement customized logic for respective price feed.
 * 
 * Write the data into Repository by calling add based on the data in mEntityIdMap.
 * 
 * @version 1.0
 * @author Professional Access
 * 
 */
public class TRUPriceFeedItemPriceWriter extends PriceFeedItemPriceWriter {

	/** The entity id map. */
	private Map<String, Map<String, String>> mEntityIdMap;
	
	/** The Price feed repository properties. */
	private TRUPriceFeedRepositoryProperties mPriceFeedRepositoryProperties;
	
	/** mRemoveExtendedPricesOnFullFeed. */
	private boolean mRemoveExtendedPricesOnFullFeed;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * Check entity exists.
	 * 
	 * @param pVo
	 *            the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(TRUPriceFeedVO pVo) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: checkEntityExists()");
		boolean exists = Boolean.FALSE;
		if (pVo != null) {

			Map<String, String> priceMap = mEntityIdMap.get(pVo.getPriceListId());
			if (null != priceMap && !priceMap.isEmpty() && priceMap.containsKey(pVo.getSkuId())) {
				exists = Boolean.TRUE;
			}
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: checkEntityExists()");
		return exists;
	}

	/**
	 * this method is used to set the required values.
	 */
	@Override
	public void doOpen() {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: doOpen()");
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}

		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
		
		if (isRemoveExtendedPricesOnFullFeed()) {
			try {
				removeAllExtendedPriceItems();
			} catch (Exception e) {
				if(isLoggingError()){
					logError("TRUPriceFeedItemPriceWriter : Exception occured when removing extended prices",e);
				}
				if(e.getMessage() != null){
					logFailedMessage(startDate, e.getMessage());
				}else {
					logFailedMessage(startDate, TRUPriceFeedReferenceConstants.FEED_EXCEPTION);
				}
			}
		}
		logSuccessMessage(startDate,  TRUPriceFeedReferenceConstants.FILES_WRITING);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: doOpen()");
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess(TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FEED_PROCESS, null, extenstions);
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds( TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FEED_PROCESS, null, extenstions);
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @param pVOItems
	 *            the vO items
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @see com.tru.feedprocessor.tol.catalog.writer.AbstractFeedItemWriter.
	 *      AbstractFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: doWrite()");

		if (mEntityIdMap != null && !mEntityIdMap.isEmpty()) {
			for (BaseFeedProcessVO lBaseFeedProcessVO : pVOItems) {
				TRUPriceFeedVO lfeedVO = (TRUPriceFeedVO) lBaseFeedProcessVO;
				if (checkEntityExists(lfeedVO)) {
					updateItem(lfeedVO);
				} else {
					addItem(lfeedVO);
				}
			}
		}
		
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: doWrite()");
	}

	/**
	 * Removes the all extended price items.
	 * 
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private void removeAllExtendedPriceItems() throws RepositoryException {
		RepositoryView repoView = getRepository(TRUPriceFeedReferenceConstants.PRICE_REPOSITORY).getView(TRUPriceFeedReferenceConstants.PRICELIST);
		RepositoryItem[] priceListItems = null;
		if (repoView != null) {
			QueryBuilder qb = repoView.getQueryBuilder();
			Query getFeedsQuery = qb.createUnconstrainedQuery();
			priceListItems = repoView.executeQuery(getFeedsQuery);

			if (priceListItems != null && priceListItems.length > 0) {
				for (RepositoryItem priceListItem : priceListItems) {
					if (priceListItem.getPropertyValue(getPriceFeedRepositoryProperties().getBasePriceListPropertyName()) != null) {
						RepositoryView priceRepoView = getRepository(TRUPriceFeedReferenceConstants.PRICE_REPOSITORY).getView(
								TRUPriceFeedReferenceConstants.PRICE);
						if (priceRepoView != null) {
							RepositoryItem[] priceItems = null;
							QueryBuilder queryBuilder = priceRepoView.getQueryBuilder();
							QueryExpression priceListIdExpression = queryBuilder.createConstantQueryExpression(priceListItem.getRepositoryId());
							QueryExpression priceListIdProperty = queryBuilder.createPropertyQueryExpression(TRUPriceFeedReferenceConstants.PRICELIST);

							Query priceListIdQuery = queryBuilder.createComparisonQuery(priceListIdExpression, priceListIdProperty, QueryBuilder.EQUALS);
							priceItems = priceRepoView.executeQuery(priceListIdQuery);
							removePriceItems(priceListItem, priceItems);
						}
					}
				}
			}
		}
	}

	/**
	 * Removes the item.
	 *
	 * @param pPriceListItem the price list item
	 * @param pPriceItems the price items
	 * @throws RepositoryException the repository exception
	 */
	private void removePriceItems(RepositoryItem pPriceListItem, RepositoryItem[] pPriceItems) throws RepositoryException {
		if (pPriceItems != null && pPriceItems.length > 0) {
			getLogger().vlogDebug(
					"TRUPriceFeedItemPriceWriter : removeAllExtendedPriceItems() priceListId : " + pPriceListItem.getRepositoryId()
							+ " priceItems : " + Arrays.toString(pPriceItems));
			for (RepositoryItem priceItem : pPriceItems) {
				getRepository(TRUPriceFeedReferenceConstants.PRICE_REPOSITORY).removeItem(priceItem.getRepositoryId(),
						TRUPriceFeedReferenceConstants.PRICE);
			}
		}
	}

	/**
	 * Update item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void updateItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: updateItem()"); 
		TRUPriceFeedVO vo = (TRUPriceFeedVO) pFeedVO;
		MutableRepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());

		if (lItemDiscriptorName != null) {
			IFeedItemMapper<BaseFeedProcessVO> lICatFeedItemMapper = (IFeedItemMapper<BaseFeedProcessVO>) getMapperMap()
					.get(lItemDiscriptorName);
			String repId = mEntityIdMap.get(vo.getPriceListId()).get(vo.getSkuId());
			lCurrentItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(repId,lItemDiscriptorName);

			beforeUpdateItem(pFeedVO, lCurrentItem);

			lCurrentItem = lICatFeedItemMapper.map(pFeedVO, lCurrentItem);
			getRepository(lRepositoryName).updateItem(lCurrentItem);

			addUpdateItemToList(pFeedVO);
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: updateItem()");
	}

	/**
	 * Before add item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pCurrentItem
	 *            the current item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void beforeAddItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem) throws RepositoryException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: beforeAddItem()");
		TRUPriceFeedVO vo = (TRUPriceFeedVO) pFeedVO;
		RepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		lCurrentItem = getRepository(lRepositoryName).getItem(vo.getPriceListId(), FeedConstants.PRICELIST);
		pCurrentItem.setPropertyValue(getFeedProperty().getPriceListName(), lCurrentItem);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: beforeAddItem()");
	}

	/**
	 * Before update item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pCurrentItem
	 *            the current item
	 * @throws RepositoryException
	 *             the repository exception
	 */
	public void beforeUpdateItem(BaseFeedProcessVO pFeedVO, MutableRepositoryItem pCurrentItem)
			throws RepositoryException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: beforeUpdateItem()");
		TRUPriceFeedVO vo = (TRUPriceFeedVO) pFeedVO;
		RepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		lCurrentItem = getRepository(lRepositoryName).getItem(vo.getPriceListId(), FeedConstants.PRICELIST);
		pCurrentItem.setPropertyValue(getFeedProperty().getPriceListName(), lCurrentItem);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: beforeUpdateItem()");
	}

	/**
	 * Adds the item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void addItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceWriter : @Method: addItem()");
		MutableRepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());

		if (lItemDiscriptorName != null) {
			IFeedItemMapper<BaseFeedProcessVO> lIPriceFeedItemMapper = (IFeedItemMapper<BaseFeedProcessVO>) getMapperMap()
					.get(lItemDiscriptorName);
			lCurrentItem = getRepository(lRepositoryName).createItem(lItemDiscriptorName);
			beforeAddItem(pFeedVO, lCurrentItem);
			lCurrentItem = lIPriceFeedItemMapper.map(pFeedVO, lCurrentItem);

			getRepository(lRepositoryName).addItem(lCurrentItem);
			addInsertItemToList(pFeedVO);
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceWriter : @Method: addItem()");
	}

	/**
	 * Gets the price feed repository properties.
	 * 
	 * @return the price feed repository properties
	 */
	public TRUPriceFeedRepositoryProperties getPriceFeedRepositoryProperties() {
		return mPriceFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 * 
	 * @param pPriceFeedRepositoryProperties
	 *            the new price feed repository properties
	 */
	public void setPriceFeedRepositoryProperties(TRUPriceFeedRepositoryProperties pPriceFeedRepositoryProperties) {
		mPriceFeedRepositoryProperties = pPriceFeedRepositoryProperties;
	}

	
	/**
	 * Checks if is removes the extended prices on full feed.
	 *
	 * @return true, if is removes the extended prices on full feed
	 */
	public boolean isRemoveExtendedPricesOnFullFeed() {
		return mRemoveExtendedPricesOnFullFeed;
	}

	/**
	 * Sets the removes the extended prices on full feed.
	 *
	 * @param pRemoveExtendedPricesOnFullFeed the new removes the extended prices on full feed
	 */
	public void setRemoveExtendedPricesOnFullFeed(boolean pRemoveExtendedPricesOnFullFeed) {
		this.mRemoveExtendedPricesOnFullFeed = pRemoveExtendedPricesOnFullFeed;
	}
}
