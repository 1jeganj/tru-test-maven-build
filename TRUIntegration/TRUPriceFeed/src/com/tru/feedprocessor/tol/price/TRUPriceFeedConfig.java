package com.tru.feedprocessor.tol.price;

import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;

/**
 * The Class TRUPriceFeedConfig. This class is extended to load properties which are specific to Price feed, which will
 * hold the configuration details of price.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFeedConfig extends PriceFeedConfig {
	
	/** The Bad directory. */
	private String mBadDirectory;
	
	/** The Processing directory. */
	private String mProcessingDirectory;
	
	/** The Success directory. */
	private String mSuccessDirectory;
	
	/** The Error directory. */
	private String mErrorDirectory;
	
	/** Property to hold  Unprocces directory. */
	private String mUnProcessDirectory;
	
	/** Property to hold  mSourceDirectory directory. */
	private String mSourceDirectory;
	

	/**
	 * Gets the bad directory.
	 *
	 * @return the badDirectory
	 */
	public String getBadDirectory() {
		return mBadDirectory;
	}

	/**
	 * Gets the processing directory.
	 *
	 * @return the processingDirectory
	 */
	public String getProcessingDirectory() {
		return mProcessingDirectory;
	}

	/**
	 * Gets the success directory.
	 *
	 * @return the successDirectory
	 */
	public String getSuccessDirectory() {
		return mSuccessDirectory;
	}

	/**
	 * Gets the error directory.
	 *
	 * @return the errorDirectory
	 */
	public String getErrorDirectory() {
		return mErrorDirectory;
	}

	/**
	 * Gets the un process directory.
	 *
	 * @return the unProcessDirectory
	 */
	public String getUnProcessDirectory() {
		return mUnProcessDirectory;
	}

	/**
	 * Gets the source directory.
	 *
	 * @return the mSourceDirectory
	 */
	public String getSourceDirectory() {
		return mSourceDirectory;
	}

	/**
	 * Sets the source directory.
	 * 
	 * @param pSourceDirectory
	 *            the new source directory
	 */
	public void setSourceDirectory(String pSourceDirectory) {
		mSourceDirectory = pSourceDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.SOURCE_DIRECTORY, pSourceDirectory);
	}
	
	/**
	 * Sets the processing directory.
	 * 
	 * @param pProcessingDirectory
	 *            the new processing directory
	 */
	public void setProcessingDirectory(String pProcessingDirectory) {
		mProcessingDirectory = pProcessingDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.PROCESSING_DIRECTORY, pProcessingDirectory);
	}

	/**
	 * Sets the un process directory.
	 * 
	 * @param pUnProcessDirectory
	 *            the new un process directory
	 */
	public void setUnProcessDirectory(String pUnProcessDirectory) {
		mUnProcessDirectory = pUnProcessDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.UN_PROCESS_DIRECTORY, pUnProcessDirectory);
	}

	/**
	 * Sets the bad directory.
	 * 
	 * @param pBadDirectory
	 *            the new bad directory
	 */
	public void setBadDirectory(String pBadDirectory) {
		mBadDirectory = pBadDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.BAD_DIRECTORY, pBadDirectory);
	}

	/**
	 * Sets the success directory.
	 * 
	 * @param pSuccessDirectory
	 *            the new success directory
	 */
	public void setSuccessDirectory(String pSuccessDirectory) {
		mSuccessDirectory = pSuccessDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.SUCCESS_DIRECTORY, pSuccessDirectory);
	}

	/**
	 * Sets the error directory.
	 * 
	 * @param pErrorDirectory
	 *            the new error directory
	 */
	public void setErrorDirectory(String pErrorDirectory) {
		mErrorDirectory = pErrorDirectory;
		setConfigValue(TRUPriceFeedReferenceConstants.ERROR_DIRECTORY, pErrorDirectory);
	}

	/**
	 * Sets the feed file pattern.
	 * 
	 * @param pFeedFilePattern
	 *            the new feed file pattern
	 */
	public void setFeedFilePattern(String pFeedFilePattern) {
		setConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN, pFeedFilePattern);
	}

	/**
	 * Sets the file name tru env.
	 * 
	 * @param pFileNameTRUEnv
	 *            the new file name tru env
	 */
	public void setFileNameTRUEnv(String pFileNameTRUEnv) {
		setConfigValue(TRUPriceFeedReferenceConstants.FILE_NAME_TRU_ENV, pFileNameTRUEnv);
	}

	/**
	 * Sets the file feed name.
	 * 
	 * @param pFileFeedName
	 *            the new file feed name
	 */
	public void setFileFeedName(String pFileFeedName) {
		setConfigValue(TRUPriceFeedReferenceConstants.FILE_FEED_NAME, pFileFeedName);
	}

	/**
	 * this method is used to log whether the required properties are set to the configuration for processing the feed.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedConfig : @Method: afterPropertiesSet()");

		if (null != getConfigValue(FeedConstants.ENTITY_LIST) && 
				((List<String>) getConfigValue(FeedConstants.ENTITY_LIST)).size() == FeedConstants.NUMBER_ZERO) {
			getLogger().logError("Entity list can not be null in price feed configuration");
		}
		if (null != getConfigValue(FeedConstants.ENTITY_SQL) && 
				((Map<String, String>) getConfigValue(FeedConstants.ENTITY_SQL)).size() == FeedConstants.NUMBER_ZERO) {
			getLogger().logError("Entity sql can not be null in price feed configuration");
		}
		String voName = (String) getConfigValue(FeedConstants.DATA_LOADER_CLASS_FQCN);
		if (StringUtils.isBlank(voName)) {
			getLogger().logError("File Parse class FQCN can not be null in price feed configuration");
		}
		try {
			Class.forName(voName);
		} catch (ClassNotFoundException e) {
			if(isLoggingError()){
				logError("File Parse class FQCN must be a valid entry", e);
			}
		}

		for (String key : ((List<String>) getConfigValue(FeedConstants.ENTITY_LIST))) {
			if (!((Map<String, String>) getConfigValue(FeedConstants.ENTITY_SQL)).containsKey(key)) {
				getLogger().logError(
						"SQL is mandatory for each entity provided in entity list in price feed configuration");
			}
		}
		if (null != getConfigValue(TRUPriceFeedReferenceConstants.PROCESSING_DIRECTORY) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.PROCESSING_DIRECTORY).toString())) {
			getLogger().logError("Processing directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.UN_PROCESS_DIRECTORY) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.UN_PROCESS_DIRECTORY).toString())) {
			getLogger().logError("Un Process directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.BAD_DIRECTORY) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.BAD_DIRECTORY).toString())) {
			getLogger().logError("Bad directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.SUCCESS_DIRECTORY) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.SUCCESS_DIRECTORY).toString())) {
			getLogger().logError("Success directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.ERROR_DIRECTORY) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.ERROR_DIRECTORY).toString())) {
			getLogger().logError("Error directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN).toString())) {
			getLogger().logError("Error directory path is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.FILE_NAME_TRU_ENV) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.FILE_NAME_TRU_ENV).toString())) {
			getLogger().logError("File environment name is empty");
		}

		if (null != getConfigValue(TRUPriceFeedReferenceConstants.FILE_FEED_NAME) && 
				StringUtils.isBlank(getConfigValue(TRUPriceFeedReferenceConstants.FILE_FEED_NAME).toString())) {
			getLogger().logError("File Feed Name is empty");
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedConfig : @Method: afterPropertiesSet()");
	}

	/**
	 * Gets the file name tru env.
	 * 
	 * @return the file name tru env
	 */
	public String getFileNameTRUEnv() {
		return (String) getConfigValue(TRUPriceFeedReferenceConstants.FILE_NAME_TRU_ENV);
	}

	
	/**
	 * Gets the feed file pattern.
	 *
	 * @return the feed file pattern
	 */
	public String getFeedFilePattern() {
		return (String) getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN);
	}

	
	/**
	 * Gets the file feed name.
	 *
	 * @return the file feed name
	 */
	public String getFileFeedName() {
		return (String) getConfigValue(TRUPriceFeedReferenceConstants.FILE_FEED_NAME);
	}

}
