package com.tru.feedprocessor.tol.price.processor;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class TRUPriceItemValueChangedInspector will inspect for each repository item level properties if any property
 * changed then that record will is processed to update if no changes then that record is skipped.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceItemValueChangedInspector implements IItemValueChangeInspector {

	/** The mLogger. */
	private FeedLogger mLogger;

	/** The Price feed repository properties. */
	private TRUPriceFeedRepositoryProperties mPriceFeedRepositoryProperties;

	/**
	 * The method isUpdated() will inspect for necessary vo properties with respective repository item level properties
	 * if any property changed then that record will is processed to update if no changes then that record is skipped.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @param pRepositoryItem
	 *            the repository item
	 * @return true, if is updated
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public boolean isUpdated(BaseFeedProcessVO pBaseFeedProcessVO, RepositoryItem pRepositoryItem) throws FeedSkippableException {
		TRUPriceFeedVO price = (TRUPriceFeedVO) pBaseFeedProcessVO;
		getLogger().vlogDebug("Begin:@Class: TRUPriceItemValueChangedInspector : @Method: isUpdated():: price id:: {0}" , price.getSkuId());
		if (StringUtils.isNotEmpty(price.getPriceListId()) && price.getPriceListId().contains(TRUPriceFeedReferenceConstants.LIST_PRICE)) {
			if ((price.getListPrice() != null && pRepositoryItem.getPropertyValue(TRUPriceFeedReferenceConstants.LIST_PRICE) != null && (Double
					.parseDouble(price.getListPrice()) != ((Double) pRepositoryItem.getPropertyValue(TRUPriceFeedReferenceConstants.LIST_PRICE))))) {
				getLogger().vlogDebug("Begin:@Class: TRUPriceItemValidator : @Method: validate():: isUpdated ::", Boolean.TRUE);
				return Boolean.TRUE;
			}
		} else if (StringUtils.isNotEmpty(price.getPriceListId()) && 
				price.getPriceListId().contains(TRUPriceFeedReferenceConstants.SALE_PRICE) && 
				(price.getSalePrice() != null && pRepositoryItem.getPropertyValue(TRUPriceFeedReferenceConstants.LIST_PRICE) != null && (Double
						.parseDouble(price.getSalePrice()) != ((Double) pRepositoryItem.getPropertyValue(TRUPriceFeedReferenceConstants.LIST_PRICE))))) {
			getLogger().vlogDebug("Begin:@Class: TRUPriceItemValidator : @Method: validate():: isUpdated ::", Boolean.TRUE);
			return Boolean.TRUE;
		}

		if ((StringUtils.isNotEmpty(price.getDealId()) && !price.getDealId().equalsIgnoreCase(
				(String) pRepositoryItem.getPropertyValue(getPriceFeedRepositoryProperties().getDealId())))) {
			return Boolean.TRUE;
		}

		getLogger().vlogDebug("Begin:@Class: TRUPriceItemValueChangedInspector : @Method: isUpdated():: isUpdated :: {0}" , Boolean.FALSE);
		return Boolean.FALSE;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

	/**
	 * Gets the price feed repository properties.
	 * 
	 * @return the price feed repository properties
	 */
	public TRUPriceFeedRepositoryProperties getPriceFeedRepositoryProperties() {
		return mPriceFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 * 
	 * @param pPriceFeedRepositoryProperties
	 *            the new price feed repository properties
	 */
	public void setPriceFeedRepositoryProperties(TRUPriceFeedRepositoryProperties pPriceFeedRepositoryProperties) {
		mPriceFeedRepositoryProperties = pPriceFeedRepositoryProperties;
	}
}
