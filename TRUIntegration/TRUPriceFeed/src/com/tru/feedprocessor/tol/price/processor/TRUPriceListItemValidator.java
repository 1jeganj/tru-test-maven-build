package com.tru.feedprocessor.tol.price.processor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.StringUtils;

import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class TRUPriceListItemValidator will validate the mandatory properties if mandatory properties are empty or
 * invalid then that record is skipped and processed the next record.
 * @version 1.0
 * @author Professional Access
 */

public class TRUPriceListItemValidator implements IFeedItemValidator {

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * the method validate() will validate the mandatory properties if mandatory
	 * properties are empty or invalid then that record is skipped and processed
	 * the next record.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void validate(BaseFeedProcessVO pBaseFeedProcessVO) throws FeedSkippableException {
		TRUPriceFeedVO priceFeedVO = (TRUPriceFeedVO) pBaseFeedProcessVO;
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceListItemValidator : @Method: validate():: price list id::" + priceFeedVO.getRepositoryId());
		if (StringUtils.isBlank(priceFeedVO.getCountryCode())) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.REQUIRED_COUNTRY_CODE,
					priceFeedVO,null);
		}

		if (StringUtils.isBlank(priceFeedVO.getMarketCode())) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.REQUIRED_MARKET_CODE,
					priceFeedVO,null);
		}

		if (StringUtils.isBlank(priceFeedVO.getLocale())) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.REQUIRED_LOCALE, priceFeedVO,null);
		}

		if (StringUtils.isBlank(priceFeedVO.getDisplayName())) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.REQUIRED_DISPLAY_NAME,
					priceFeedVO,null);
		}

		if (!isPatternMatched(priceFeedVO.getCountryCode(),
				TRUPriceFeedReferenceConstants.STRING_VAL_WITH_3_DIGIT_PATTERN)) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_COUNTRY_CODE,
					priceFeedVO,null);
		}

		if (!isPatternMatched(priceFeedVO.getMarketCode(), TRUPriceFeedReferenceConstants.STRING_VAL_WITH_3_DIGIT_PATTERN)) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_MARKET_CODE,
					priceFeedVO,null);
		}

		if (!isPatternMatched(priceFeedVO.getLocale(), TRUPriceFeedReferenceConstants.STRING_VAL_WITH_5_DIGIT_PATTERN)) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_LOCALE, priceFeedVO,null);
		}

		if (StringUtils.isBlank(priceFeedVO.getRepositoryId())) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_RECORD, priceFeedVO,null);
		}
		
		if (!isPatternMatched(priceFeedVO.getStoreNumber(),
				TRUPriceFeedReferenceConstants.STRING_VAL_WITH_NUMBER_5_DIGIT_PATTERN)) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_STORE_NO, priceFeedVO,null);
		}
		
		if (StringUtils.isBlank(priceFeedVO.getListPrice()) && !StringUtils.isBlank(priceFeedVO.getBusinessId()) && (priceFeedVO.getBusinessId().contains(TRUPriceFeedReferenceConstants.LIST_PRICE) || priceFeedVO.getBusinessId().contains(TRUPriceFeedReferenceConstants.SALE_PRICE))) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.REQUIRED_LIST_PRICE,
					priceFeedVO,null);
		}
		
		if (!isPatternMatched(priceFeedVO.getListPrice(),
				TRUPriceFeedReferenceConstants.DOUBLE_VAL_WITH_DECIMAL_DIGIT_PATTERN) && !StringUtils.isBlank(priceFeedVO.getBusinessId()) && (priceFeedVO.getBusinessId().contains(TRUPriceFeedReferenceConstants.LIST_PRICE) || priceFeedVO.getBusinessId().contains(TRUPriceFeedReferenceConstants.SALE_PRICE))) {
			throw new FeedSkippableException(null, null, TRUPriceFeedReferenceConstants.INVALID_LIST_PRICE, priceFeedVO,null);
		}
		
		getLogger().vlogDebug(
				"End:@Class: TRUPriceListItemValidator : @Method: validate():: price list id::" + priceFeedVO.getRepositoryId());
	}

	/**
	 * Checks if is match pattern.
	 * 
	 * @param pVal
	 *            the val
	 * @param pPattern
	 *            the pattern
	 * @return true, if is match pattern
	 */
	public boolean isPatternMatched(String pVal, String pPattern) {
		Pattern pattern = Pattern.compile(pPattern);
		Matcher matcher = pattern.matcher(pVal);
		if (matcher.matches()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * Gets the logger.
	 * 
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param pLogger
	 *            the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

}
