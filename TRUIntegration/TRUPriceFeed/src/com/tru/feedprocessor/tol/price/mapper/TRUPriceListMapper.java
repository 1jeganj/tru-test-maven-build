package com.tru.feedprocessor.tol.price.mapper;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.logger.FeedLogger;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class TRUPriceListMapper is used to map the properties of price list feed vo to respective item repository
 * properties.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceListMapper extends AbstractFeedItemMapper<TRUPriceFeedVO> {

	/** The Price feed repository properties. */
	private TRUPriceFeedRepositoryProperties mPriceFeedRepositoryProperties;

	/** The mLogger. */
	private FeedLogger mLogger;

	/**
	 * this method is used to map the properties of feed vo to respective repository properties.
	 * 
	 * @param pVOItem
	 *            the VO item
	 * @param pRepoItem
	 *            the repo item
	 * @return the mutable repository item
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public MutableRepositoryItem map(TRUPriceFeedVO pVOItem, MutableRepositoryItem pRepoItem)
			throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceListMapper : @Method: map():: price list id:: {0}" , pVOItem.getRepositoryId());
		pRepoItem.setPropertyValue(getPriceFeedRepositoryProperties().getDisplayName(), pVOItem.getDisplayName());
		pRepoItem.setPropertyValue(getPriceFeedRepositoryProperties().getLocale(), pVOItem.getLocale());
		getLogger().vlogDebug("Begin:@Class: TRUPriceListMapper : @Method: map():: price list id :: {0}" , pVOItem.getRepositoryId());
		return pRepoItem;
	}

	/**
	 * Gets the price feed repository properties.
	 * 
	 * @return the price feed repository properties
	 */
	public TRUPriceFeedRepositoryProperties getPriceFeedRepositoryProperties() {
		return mPriceFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 * 
	 * @param pPriceFeedRepositoryProperties
	 *            the new price feed repository properties
	 */
	public void setPriceFeedRepositoryProperties(TRUPriceFeedRepositoryProperties pPriceFeedRepositoryProperties) {
		mPriceFeedRepositoryProperties = pPriceFeedRepositoryProperties;
	}

	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public FeedLogger getLogger() {
		return mLogger;
	}

	/**
	 * Sets the logger.
	 *
	 * @param pLogger the new logger
	 */
	public void setLogger(FeedLogger pLogger) {
		mLogger = pLogger;
	}

}
