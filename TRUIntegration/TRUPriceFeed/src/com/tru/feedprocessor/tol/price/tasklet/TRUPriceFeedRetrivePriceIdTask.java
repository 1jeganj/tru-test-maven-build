package com.tru.feedprocessor.tol.price.tasklet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The class TRUPriceFeedRetrivePriceIdTask is extended to override two methods to type cast the base vo to price vo.
 * 
 * This is the common class which is used to retrieve the unique id and repository id from the corresponding data
 * source. Using teh EntiryResultExtractor it will store the data into feed context.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedRetrivePriceIdTask extends PriceFeedRetrivePriceIdTask {

	/**
	 * this method is used to construct the query based on the feed records.
	 * 
	 * @param pCurrentIdentifier
	 *            the current identifier
	 * @return the query
	 */
	@Override
	protected String getQuery(String pCurrentIdentifier) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedRetrivePriceIdTask : @Method: getQuery()");
		Map<String, String> lEntitySQLMap = (HashMap<String, String>) getConfigValue(FeedConstants.ENTITY_SQL);
		return lEntitySQLMap.get(pCurrentIdentifier);
	}

	/**
	 * this method is overridden to return the list of price list ids.
	 * 
	 * @return the sets the
	 */
	@Override
	protected Set<String> priceListIds() {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedRetrivePriceIdTask : @Method: priceListIds()");
		Set<String> priceListId = new HashSet<String>();
		List<? extends BaseFeedProcessVO> priceEntityList = getEntityMap().get(FeedConstants.PRICE_ENTITY_NAME);
		for (BaseFeedProcessVO vo : priceEntityList) {
			TRUPriceFeedVO price = (TRUPriceFeedVO) vo;
			priceListId.add(price.getPriceListId());
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedRetrivePriceIdTask : @Method: priceListIds()");
		return priceListId;
	}

	/**
	 * getQuerMap.
	 * @param  pCurrentIdentifier - CurrentIdentifier
	 * @return Map
	 */
	
	@Override
	protected Map<String, StringBuffer> getQuerMap(String pCurrentIdentifier) {
		String lQuery = getQuery(pCurrentIdentifier);
		Map<String, StringBuffer> lPriceQueryMap = new HashMap<String, StringBuffer>();

		for (String ids : priceListIds()) {
			StringBuffer lMapQuery = new StringBuffer(lQuery);
			lMapQuery.replace(lMapQuery.indexOf(FeedConstants.HASH),
					lMapQuery.indexOf(FeedConstants.HASH) + FeedConstants.NUMBER_ONE, FeedConstants.QUOTE + ids + FeedConstants.QUOTE);
			lPriceQueryMap.put(ids, lMapQuery);
		}
		return lPriceQueryMap;
	}
}
