package com.tru.feedprocessor.tol.price.scheduler;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feedprocessor.tol.BatchInvoker;
import com.tru.feedprocessor.tol.FeedConstants;

/**
 * The Class TRUPriceFeedJobScheduler. This class is used to run feed schedule tasks.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedJobScheduler extends SingletonSchedulableService {

	/** Property to hold mFeedJob. */
	private BatchInvoker mFeedJob;

	/** The Enable. */
	private boolean mEnable;

	/**
	 *@param pScheduler 	- Scheduler
	 *@param pScheduledJob	- ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		vlogInfo("Begin @Class: TRUPriceFeedJobScheduler, @method: doScheduledTask()");

		if (isEnable()) {
			processPriceFeed();
		}

		vlogInfo("End @Class: TRUPriceFeedJobScheduler, @method: doScheduledTask()");

	}

	/**
	 * Process price feed.
	 */
	public void processPriceFeed() {
		vlogInfo("Begin @Class: TRUPriceFeedJobScheduler, @method: processPriceFeed()");
		getFeedJob().loadContext(new String[] { FeedConstants.JOB_CONFIG_PATH, FeedConstants.PRICE_FEED_CONFIG_PATH });
		String[] input = { FeedConstants.PRICE_JOB_NAME };

		getFeedJob().startJob(input);
		vlogInfo("End @Class: TRUPriceFeedJobScheduler, @method: processPriceFeed()");
	}

	/**
	 * Gets the feed job.
	 * 
	 * @return the feed job
	 */
	public BatchInvoker getFeedJob() {
		return mFeedJob;
	}

	/**
	 * Sets the feed job.
	 * 
	 * @param pFeedJob
	 *            the new feed job
	 */
	public void setFeedJob(BatchInvoker pFeedJob) {
		mFeedJob = pFeedJob;
	}

	/**
	 * Checks if is enable.
	 * 
	 * @return true, if is enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 * 
	 * @param pEnable
	 *            the new enable
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

}
