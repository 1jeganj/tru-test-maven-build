package com.tru.feedprocessor.tol.price.loader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFeedCSVLoader is used to load the feed data into respective feed VO.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFeedCSVLoader extends AbstractCSVFeedLoader {
	
	
	/** Property to hold logging. */
	public static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(TRUPriceFeedCSVLoader.class);

	/**
	 * this method is used to load the feed data into respective feed VO.
	 * 
	 * @param pBaseFeedProcessVO
	 *            the base feed process vo
	 * @return the list<? extends base feed process v o>
	 */
	@Override
	public List<? extends BaseFeedProcessVO> processEntityMapVO(BaseFeedProcessVO pBaseFeedProcessVO) {
		
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("processEntityMapVO ");
		}

		List<BaseFeedProcessVO> baseProcessVOList = new ArrayList<BaseFeedProcessVO>();
		
		pBaseFeedProcessVO.setEntityName(FeedConstants.PRICE_ENTITY_NAME);
		createListPriceList(pBaseFeedProcessVO, baseProcessVOList);
		createSalePriceList(pBaseFeedProcessVO, baseProcessVOList);
		return baseProcessVOList;
	}

	/**
	 * This is used to create list price list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            - pBaseFeedProcessVO
	 * @param pBaseProcessVOList
	 *            - pBaseProcessVOList
	 */
	private void createListPriceList(BaseFeedProcessVO pBaseFeedProcessVO, List<BaseFeedProcessVO> pBaseProcessVOList) {
		TRUPriceFeedVO listPriceFeedVO = setVOProperties((TRUPriceFeedVO) pBaseFeedProcessVO, TRUPriceFeedReferenceConstants.LISTPRICELIST);
		listPriceFeedVO.setPriceListId(getFinalPriceListId(listPriceFeedVO, TRUPriceFeedReferenceConstants.LIST_PRICE));
		pBaseProcessVOList.add((BaseFeedProcessVO) listPriceFeedVO);
	}

	/**
	 * * This is used to create sale price list.
	 * 
	 * @param pBaseFeedProcessVO
	 *            - pBaseFeedProcessVO
	 * @param pBaseProcessVOList
	 *            - pBaseProcessVOList
	 */
	private void createSalePriceList(BaseFeedProcessVO pBaseFeedProcessVO, List<BaseFeedProcessVO> pBaseProcessVOList) {
		TRUPriceFeedVO salePriceFeedVO = setVOProperties((TRUPriceFeedVO) pBaseFeedProcessVO, TRUPriceFeedReferenceConstants.SALEPRICELIST);
		salePriceFeedVO.setPriceListId(getFinalPriceListId(salePriceFeedVO, TRUPriceFeedReferenceConstants.SALE_PRICE));
		pBaseProcessVOList.add((BaseFeedProcessVO) salePriceFeedVO);
	}

	/**
	 * This method is used to copy properties.
	 * 
	 * @param pFeedVO
	 *            - pFeedVO
	 * @param pPriceListId
	 *            - pPriceListId
	 * @return TRUPriceFeedVO - TRUPriceFeedVO
	 */
	protected TRUPriceFeedVO setVOProperties(TRUPriceFeedVO pFeedVO, String pPriceListId) {
		
		if (mLogging.isLoggingDebug()) {
			mLogging.logDebug("setVOProperties : sku : " + pFeedVO.getSkuId());
		}
		TRUPriceFeedVO vo = new TRUPriceFeedVO();
		vo.setEntityName(pFeedVO.getEntityName());
		vo.setDisplayName(pPriceListId);
		vo.setLocale(pFeedVO.getLocale());
		vo.setSkuId(pFeedVO.getSkuId());
		vo.setCountryCode(pFeedVO.getCountryCode());
		vo.setMarketCode(pFeedVO.getMarketCode());
		vo.setSalePrice(pFeedVO.getSalePrice());
		vo.setListPrice(pFeedVO.getListPrice());
		vo.setStoreNumber(pFeedVO.getStoreNumber());
		vo.setDealId(pFeedVO.getDealId());
		StringBuffer businessId = new StringBuffer();
		businessId.append(pFeedVO.getSkuId()).append(TRUPriceFeedReferenceConstants.UNDERSCORE).append(pPriceListId);
		vo.setBusinessId(businessId.toString());
		vo.setSkuId(pFeedVO.getSkuId());
		vo.setSkipped(pFeedVO.isSkipped());
		return vo;
	}
	
	/**
	 * this method is used to load the feed data into entity map.
	 * 
	 * @param pEntityList
	 *            the base feed process vo
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
    @Override
    protected void addEntityToMap(List<? extends BaseFeedProcessVO> pEntityList) throws Exception {
           for (BaseFeedProcessVO entityVO : pEntityList) {

                  String entityName = entityVO.getEntityName();

                  if (isResolveDuplicate()) {
                        List<BaseFeedProcessVO> mappedEntityList = resolveDuplicate(entityName, entityVO);
                        mVOHolderMap.put(entityName, mappedEntityList);

                  } else {
                        if (mVOHolderMap.containsKey(entityName)) {
                               List<BaseFeedProcessVO> mappedEntityList = (List<BaseFeedProcessVO>) mVOHolderMap.get(entityName);
                               mappedEntityList.add(entityVO);
                               mVOHolderMap.put(entityName, mappedEntityList);
                        } else {
                               List<BaseFeedProcessVO> mappedEntityList = new ArrayList<BaseFeedProcessVO>();
                               mappedEntityList.add(entityVO);
                               mVOHolderMap.put(entityName, mappedEntityList);
                        }
                  }
           }
    }


	/**
	 * this method is used to set respective VO class.
	 * 
	 * @return the line mapping vo class
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public Class<?> getLineMappingVOClass() throws Exception {
		return Class.forName(TRUPriceFeedReferenceConstants.PRICE_VO);
	}

	/**
	 * this method is used to skip number of lines to process in over case we need to skip one line(header).
	 * 
	 * @return the lines to skip
	 */
	@Override
	public int getLinesToSkip() {
		return FeedConstants.NUMBER_ONE;
	}

	/**
	 * this method is to specify property names which we want to load from feed record data.
	 * 
	 * @return the column names
	 */
	@Override
	public String[] getColumnNames() {
		String[] stringArray = new String[] { TRUPriceFeedReferenceConstants.SKU_ID,
				TRUPriceFeedReferenceConstants.LIST_PRICE, TRUPriceFeedReferenceConstants.SALE_PRICE,
				TRUPriceFeedReferenceConstants.LOCALE, TRUPriceFeedReferenceConstants.COUNTRY_CODE,
				TRUPriceFeedReferenceConstants.MARKET_CODE, TRUPriceFeedReferenceConstants.STORE_NUMBER, TRUPriceFeedReferenceConstants.DEAL_ID, };
		return stringArray;
	}

	/**
	 * this method is to specify delimiter by which record data is separated in the feed file.
	 * 
	 * @return the delimiter string
	 */
	@Override
	public String getDelimiterString() {
		return String.valueOf(TRUPriceFeedReferenceConstants.PIPE_DELIMITER);
	}

	/**
	 * this method is to return true to resolve the duplicates false to not resolve the duplicates.
	 * 
	 * @return true, if is resolve duplicate
	 */
	@Override
	public boolean isResolveDuplicate() {
		return Boolean.FALSE;
	}

	/**
	 * this method is used to combine price list with required properties.
	 * 
	 * @param pPriceFeedVO
	 *            the price feed vo
	 * @param pPriceType
	 *            the pPriceType   
	 * @return the final price list id
	 */
	public String getFinalPriceListId(TRUPriceFeedVO pPriceFeedVO, String pPriceType) {
		StringBuffer finalPriceListId = new StringBuffer();
		finalPriceListId.append(pPriceType).append(FeedConstants.UNDER_SCORE).
			append(pPriceFeedVO.getCountryCode()).append(FeedConstants.UNDER_SCORE).
			append(pPriceFeedVO.getMarketCode()).append(FeedConstants.UNDER_SCORE).
			append(pPriceFeedVO.getStoreNumber()).append(FeedConstants.UNDER_SCORE).
			append(pPriceFeedVO.getLocale());
		return finalPriceListId.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.loader.AbstractCSVFeedLoader#loadData(com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO)
	 */
	@Override
	public Map<String, List<? extends BaseFeedProcessVO>> loadData(
			FeedExecutionContextVO pCurExecCntx) throws Exception {
		// TODO Auto-generated method stub
		
		TRUAlertLogger alertLogger = (TRUAlertLogger) pCurExecCntx.getConfigValue(TRUPriceFeedReferenceConstants.ALERT_LOGGER);
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		logSuccessMessage(startDate, TRUPriceFeedReferenceConstants.FILE_PARSING, pCurExecCntx.getFileName(), alertLogger);
		return super.loadData(pCurExecCntx);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 * @param pAlertLogger the alert logger
	 */
	private void logSuccessMessage(String pStartDate, String pMessage, String pFileName, TRUAlertLogger pAlertLogger) {
		String endDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceFeedReferenceConstants.END_TIME, endDate);
		pAlertLogger.logFeedSuccess(TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FILE_PARSING, pFileName, extenstions);
	}

}
