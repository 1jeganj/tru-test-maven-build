package com.tru.feedprocessor.tol.price;

/**
 * The Class TRUPriceFeedRepositoryProperties.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedRepositoryProperties {
	/** property to hold skuId. */
	private String mSkuId;

	/** property to hold listPrice. */
	private String mListPrice;

	/** property to hold salePrice. */
	private String mSalePrice;

	/** property to hold locale. */
	private String mLocale;

	/** property to hold countryCode. */
	private String mCountryCode;

	/** property to hold marketCode. */
	private String mMarketCode;

	/** property to hold storeNumber. */
	private String mStoreNumber;

	/** property to hold dealId. */
	private String mDealId;

	/** property to hold skuId. */
	private String mPriceId;

	/** property to hold priceListId. */
	private String mPriceListId;

	/** property to hold displayName. */
	private String mDisplayName;
	
	/** The Price list. */
	private String mPriceListPropertyName;
	
	/** The Base price list. */
	private String mBasePriceListPropertyName;
	
	/** The Feedfilesequence item descriptor. */
	private String mFeedfilesequenceItemDescriptor;
	
	/** The m seq processed property name. */
	private String mSeqProcessedPropertyName;


	/**
	 * Gets the sku id.
	 * 
	 * @return the sku id
	 */
	public String getSkuId() {
		return mSkuId;
	}

	/**
	 * Sets the sku id.
	 * 
	 * @param pSkuId
	 *            the new sku id
	 */
	public void setSkuId(String pSkuId) {
		mSkuId = pSkuId;
	}

	/**
	 * Gets the list price.
	 * 
	 * @return the list price
	 */
	public String getListPrice() {
		return mListPrice;
	}

	/**
	 * Sets the list price.
	 * 
	 * @param pListPrice
	 *            the new list price
	 */
	public void setListPrice(String pListPrice) {
		mListPrice = pListPrice;
	}

	/**
	 * Gets the sale price.
	 * 
	 * @return the sale price
	 */
	public String getSalePrice() {
		return mSalePrice;
	}

	/**
	 * Sets the sale price.
	 * 
	 * @param pSalePrice
	 *            the new sale price
	 */
	public void setSalePrice(String pSalePrice) {
		mSalePrice = pSalePrice;
	}

	/**
	 * Gets the locale.
	 * 
	 * @return the locale
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * Sets the locale.
	 * 
	 * @param pLocale
	 *            the new locale
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}

	/**
	 * Gets the country code.
	 * 
	 * @return the country code
	 */
	public String getCountryCode() {
		return mCountryCode;
	}

	/**
	 * Sets the country code.
	 * 
	 * @param pCountryCode
	 *            the new country code
	 */
	public void setCountryCode(String pCountryCode) {
		mCountryCode = pCountryCode;
	}

	/**
	 * Gets the market code.
	 * 
	 * @return the market code
	 */
	public String getMarketCode() {
		return mMarketCode;
	}

	/**
	 * Sets the market code.
	 * 
	 * @param pMarketCode
	 *            the new market code
	 */
	public void setMarketCode(String pMarketCode) {
		mMarketCode = pMarketCode;
	}

	/**
	 * Gets the store number.
	 * 
	 * @return the store number
	 */
	public String getStoreNumber() {
		return mStoreNumber;
	}

	/**
	 * Sets the store number.
	 * 
	 * @param pStoreNumber
	 *            the new store number
	 */
	public void setStoreNumber(String pStoreNumber) {
		mStoreNumber = pStoreNumber;
	}

	/**
	 * Gets the price id.
	 * 
	 * @return the price id
	 */
	public String getPriceId() {
		return mPriceId;
	}

	/**
	 * Sets the price id.
	 * 
	 * @param pPriceId
	 *            the new price id
	 */
	public void setPriceId(String pPriceId) {
		mPriceId = pPriceId;
	}

	/**
	 * Gets the price list id.
	 * 
	 * @return the price list id
	 */
	public String getPriceListId() {
		return mPriceListId;
	}

	/**
	 * Sets the price list id.
	 * 
	 * @param pPriceListId
	 *            the new price list id
	 */
	public void setPriceListId(String pPriceListId) {
		mPriceListId = pPriceListId;
	}

	/**
	 * Gets the display name.
	 * 
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Sets the display name.
	 * 
	 * @param pDisplayName
	 *            the new display name
	 */
	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}
	

	/**
	 * @return the priceListPropertyName
	 */
	public String getPriceListPropertyName() {
		return mPriceListPropertyName;
	}

	/**
	 * @param pPriceListPropertyName the priceListPropertyName to set
	 */
	public void setPriceListPropertyName(String pPriceListPropertyName) {
		mPriceListPropertyName = pPriceListPropertyName;
	}

	/**
	 * @return the basePriceListPropertyName
	 */
	public String getBasePriceListPropertyName() {
		return mBasePriceListPropertyName;
	}

	/**
	 * @param pBasePriceListPropertyName the basePriceListPropertyName to set
	 */
	public void setBasePriceListPropertyName(String pBasePriceListPropertyName) {
		mBasePriceListPropertyName = pBasePriceListPropertyName;
	}

	/**
	 * Gets the deal id.
	 * 
	 * @return the mDealId
	 */
	public String getDealId() {
		return mDealId;
	}

	/**
	 * Sets the deal id.
	 * 
	 * @param pDealId
	 *            the new deal id
	 */
	public void setDealId(String pDealId) {
		this.mDealId = pDealId;
	}

	/**
	 * Gets the feedfilesequence item descriptor.
	 *
	 * @return the feedfilesequence item descriptor
	 */
	public String getFeedfilesequenceItemDescriptor() {
		return mFeedfilesequenceItemDescriptor;
	}

	/**
	 * Sets the feedfilesequence item descriptor.
	 *
	 * @param pFeedfilesequenceItemDescriptor the new feedfilesequence item descriptor
	 */
	public void setFeedfilesequenceItemDescriptor(String pFeedfilesequenceItemDescriptor) {
		mFeedfilesequenceItemDescriptor = pFeedfilesequenceItemDescriptor;
	}

	/**
	 * Gets the seq processed property name.
	 * 
	 * @return the seq processed property name
	 */
	public String getSeqProcessedPropertyName() {
		return mSeqProcessedPropertyName;
	}

	/**
	 * Sets the seq processed property name.
	 * 
	 * @param pSeqProcessedPropertyName
	 *            the new seq processed property name
	 */
	public void setSeqProcessedPropertyName(String pSeqProcessedPropertyName) {
		this.mSeqProcessedPropertyName = pSeqProcessedPropertyName;
	}

}
