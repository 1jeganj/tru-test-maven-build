package com.tru.feedprocessor.tol.price.reader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.epub.project.Process;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.EntityDataProvider;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;
import com.tru.merchutil.tol.workflow.WorkFlowTools;

/**
 * The Class TRUPriceFeedItemPriceListReader will read the feed vo records according to batch(no of record configured)
 * for processing .
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedItemPriceListReader extends PriceFeedItemPriceListReader {

	/**
	 * The iterator.
	 */
	Iterator<? extends BaseFeedProcessVO> mIterator;
	
	/** The WorkFlow Tools. */
	private WorkFlowTools mWorkFlowTools;

	/**
	 * The method doOpen() will read the feed vo records according to batch(no of record configured) for processing.
	 * 
	 */
	@Override
	protected void doOpen() {

		if (getConfigValue(FeedConstants.VERSIONED) != null && ((Boolean) getConfigValue(FeedConstants.VERSIONED))) {
			getWorkFlowTools().pushDevelopmentLine((Process) retriveData(FeedConstants.PROCESS));
		}
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListReader : @Method: doOpen()");
		String uniquePriceListId = null;
		boolean changeInPriceListId = Boolean.FALSE;
		EntityDataProvider lEntityDataProvider = (EntityDataProvider) retriveData(FeedConstants.ENTITY_PROVIDER);
		Map<String, List<? extends BaseFeedProcessVO>> lProviderEntityList = lEntityDataProvider.getAllEntityList();
		List<BaseFeedProcessVO> lBaseFeedProcessVOList = new ArrayList<BaseFeedProcessVO>();
		if (lProviderEntityList != null && !lProviderEntityList.isEmpty()) {
			getLogger().vlogDebug("lProviderEntityList size:", lProviderEntityList.size());
			for (BaseFeedProcessVO baseFeedProcessVO : lProviderEntityList.get(FeedConstants.PRICE_ENTITY_NAME)) {
				TRUPriceFeedVO feedVO = (TRUPriceFeedVO) baseFeedProcessVO;
				if (!StringUtils.isBlank(uniquePriceListId)) {
					if (!uniquePriceListId.equalsIgnoreCase(feedVO.getPriceListId())) {
						changeInPriceListId = Boolean.TRUE;
						uniquePriceListId = feedVO.getPriceListId();
					} else {
						changeInPriceListId = Boolean.FALSE;
					}
				} else {
					uniquePriceListId = feedVO.getPriceListId();
					changeInPriceListId = Boolean.TRUE;
				}
				if (changeInPriceListId) {
					lBaseFeedProcessVOList.add(setVOProperties(feedVO, uniquePriceListId));
				}
			}
		}
		getLogger().vlogDebug("vo list size:", lBaseFeedProcessVOList.size());
		mIterator = lBaseFeedProcessVOList.iterator();
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceListReader : @Method: doOpen()");
	}

	/**
	 * This method is used to loop in and return BaseProcessVO object from the mIterator.
	 * 
	 * @return the base feed process vo
	 */
	protected BaseFeedProcessVO next() {
		if (mIterator.hasNext()) {
			return mIterator.next();
		}
		return null;
	}

	/**
	 * This method is used to set VO properties with respect to price list id.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @param pPriceListId
	 *            the price list id
	 * @return the price feed vo
	 */
	protected TRUPriceFeedVO setVOProperties(TRUPriceFeedVO pFeedVO, String pPriceListId) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListReader : @Method: setVOProperties()");
		TRUPriceFeedVO vo = new TRUPriceFeedVO();
		vo.setRepositoryId(pPriceListId);
		vo.setEntityName(FeedConstants.PRICELIST);
		vo.setDisplayName(pPriceListId);
		vo.setPriceListId(pPriceListId);
		vo.setLocale(pFeedVO.getLocale());
		vo.setSkuId(pFeedVO.getSkuId());
		vo.setCountryCode(pFeedVO.getCountryCode());
		vo.setMarketCode(pFeedVO.getMarketCode());
		vo.setSalePrice(pFeedVO.getSalePrice());
		vo.setListPrice(pFeedVO.getListPrice());
		vo.setStoreNumber(pFeedVO.getStoreNumber());
		vo.setDealId(pFeedVO.getDealId());
		StringBuffer businessId = new StringBuffer();
		businessId.append(pFeedVO.getSkuId()).append(TRUPriceFeedReferenceConstants.UNDERSCORE).append(pPriceListId);
		vo.setBusinessId(businessId.toString());
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceListReader : @Method: setVOProperties()");
		return vo;
	}

	/**
	 * Gets the work flow tools.
	 *
	 * @return the work flow tools
	 */
	public WorkFlowTools getWorkFlowTools() {
		return mWorkFlowTools;
	}

	/**
	 * Sets the work flow tools.
	 *
	 * @param pWorkFlowTools the new work flow tools
	 */
	public void setWorkFlowTools(WorkFlowTools pWorkFlowTools) {
		this.mWorkFlowTools = pWorkFlowTools;
	}
}
