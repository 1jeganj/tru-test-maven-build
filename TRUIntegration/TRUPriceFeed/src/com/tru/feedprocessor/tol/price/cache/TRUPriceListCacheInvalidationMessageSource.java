/**
 * 
 */
package com.tru.feedprocessor.tol.price.cache;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import atg.core.util.StringUtils;
import atg.dms.patchbay.MessageSource;
import atg.dms.patchbay.MessageSourceContext;
import atg.nucleus.GenericService;

/**
 * This class is to send message in order to invalidate cache for priceList repository post feed import job. It will
 * implement MesageSource
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceListCacheInvalidationMessageSource extends GenericService implements MessageSource {

	/** property to hold started. */
	private boolean mStarted;

	/** property to hold portName. */
	private String mPortName;

	/** property to hold context. */
	MessageSourceContext mContext;

	/**
	 * Checks if is started.
	 * 
	 * @return the started
	 */
	public boolean isStarted() {
		return this.mStarted;
	}

	/**
	 * Sets the started.
	 * 
	 * @param pStarted
	 *            the started to set
	 */
	public void setStarted(boolean pStarted) {
		this.mStarted = pStarted;
	}

	/**
	 * Gets the port name.
	 * 
	 * @return the portName
	 */
	public String getPortName() {
		return this.mPortName;
	}

	/**
	 * Sets the port name.
	 * 
	 * @param pPortName
	 *            the portName to set
	 */
	public void setPortName(String pPortName) {
		this.mPortName = pPortName;
	}

	/**
	 * Gets the context.
	 * 
	 * @return the context
	 */
	public MessageSourceContext getContext() {
		return this.mContext;
	}

	/**
	 * Sets the context.
	 * 
	 * @param pContext
	 *            the context to set
	 */
	public void setContext(MessageSourceContext pContext) {
		this.mContext = pContext;
	}

	/**
	 * This method is used to send a JMS message in order to invalidate price list repository cache.
	 */
	public void sendMessage() {
		vlogDebug("Begin: @Class: TRUPriceListCacheInvalidationMessageSource : @Method: sendMessage()");
		ObjectMessage message = null;
		try {
			if (mStarted && getContext() != null) {
				// begin transaction

				// send message
				if (!StringUtils.isBlank(getPortName())) {
					message = mContext.createObjectMessage(getPortName());
					mContext.sendMessage(getPortName(), message);
				} else {
					message = mContext.createObjectMessage();
					mContext.sendMessage(message);
				}

				vlogDebug("message : {0}", message);
			}
		} catch (JMSException pJmsException) {
			vlogError("JMS Exception : {0}", pJmsException);
		}
		vlogDebug("End: @Class: TRUPriceListCacheInvalidationMessageSource : @Method: sendMessage()");
	}

	/**
	 * @param pContext	- MessageSourceContext
	 */
	@Override
	public void setMessageSourceContext(MessageSourceContext pContext) {
		mContext = pContext;

	}

	/**
	 * It is being used to start message source.
	 */
	@Override
	public void startMessageSource() {
		mStarted = true;

	}

	/**
	 * It is being used to stop message source.
	 */
	@Override
	public void stopMessageSource() {
		// TODO Auto-generated method stub
		mStarted = false;
	}

}
