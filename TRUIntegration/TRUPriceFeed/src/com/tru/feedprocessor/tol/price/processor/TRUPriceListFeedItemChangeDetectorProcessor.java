package com.tru.feedprocessor.tol.price.processor;

import java.util.HashMap;
import java.util.Map;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class TRUPriceListFeedItemChangeDetectorProcessor.
 * 
 * this class will validate the data according to the respective data type in price list vo, if the validation fails it
 * will skip that record as error record and process the next record.
 * 
 * Also checks whether the price list item exists in the database or not based on price list id.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceListFeedItemChangeDetectorProcessor extends TRUPriceFeedItemChangeDetectorProcessor {
	/**
	 * The entity id map.
	 */
	private Map<String, Map<String, String>> mEntityIdMap;

	/**
	 * Check whether the price list item exists in the database or not based on price list id.
	 * 
	 * @param pVo
	 *            the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(TRUPriceFeedVO pVo) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: checkEntityExists()");
		boolean exists = false;
		Map<String, String> priceListMap = (HashMap<String, String>) mEntityIdMap.get(FeedConstants.PRICELIST);
		if (null != priceListMap && !priceListMap.isEmpty() && priceListMap.containsKey(pVo.getRepositoryId())) {
			exists = true;
		}
		getLogger().vlogDebug(
				"End:@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: checkEntityExists() :{0}" , exists);
		return exists;
	}

	/**
	 * will validate the data according to the respective data type in price list vo, if the validation fails it will
	 * skip that record as error record and process the next record.
	 * 
	 * Also checks whether the price list item exists in the database or not based on price list id.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @return the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: checkEntityExists()");
		TRUPriceFeedVO price = (TRUPriceFeedVO) pFeedVO;
		IFeedItemValidator lFeedValidator = null;
		if (getValidators() != null && !getValidators().isEmpty()) {
			lFeedValidator = (IFeedItemValidator) getValidators().get(pFeedVO.getEntityName());
			try {
				if(lFeedValidator != null) {
					lFeedValidator.validate(price);
				}
			} catch (FeedSkippableException ex) {
				getLogger().vlogError("@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: doProcess() {0}", ex + FeedConstants.SPACE + price.getSkuId());
				throw new FeedSkippableException(getPhaseName(), getStepName(), ex.getMessage() + FeedConstants.SPACE + price.getSkuId(), pFeedVO,ex);
			}
		}

		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());

		if (mEntityIdMap != null && !mEntityIdMap.isEmpty() && !checkEntityExists(price)) {
			getLogger().vlogDebug(
					"End:@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: checkEntityExists() {0}", pFeedVO.getRepositoryId());
			return pFeedVO;
		}
		getLogger().vlogDebug("End:@Class: TRUPriceListFeedItemChangeDetectorProcessor : @Method: checkEntityExists()");
		return null;
	}
}
