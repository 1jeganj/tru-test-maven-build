/**
 * 
 */
package com.tru.feedprocessor.tol.price.cache;

import javax.jms.JMSException;
import javax.jms.Message;

import atg.adapter.gsa.GSARepository;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;
import atg.service.cache.Cache;

/**
 * This class is used to receive the message from source and invalidate the cache of priceList repository. It will
 * implement MessageSink.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceListCacheInvalidationMessageSink extends GenericService implements MessageSink {

	/** property to hold priceList. */
	private GSARepository mPriceList;

	/** property to hold priceCache. */
	private Cache mPriceCache;

	/**
	 * Gets the price cache.
	 * 
	 * @return the priceCache
	 */
	public Cache getPriceCache() {
		return this.mPriceCache;
	}

	/**
	 * Sets the price cache.
	 * 
	 * @param pPriceCache
	 *            the priceCache to set
	 */
	public void setPriceCache(Cache pPriceCache) {
		this.mPriceCache = pPriceCache;
	}

	/**
	 * This method was overridden to invalidate the price list repository cache.
	 * 
	 * @param pPortName
	 *            - port name
	 * @param pMessage
	 *            - message
	 * @throws JMSException
	 *             - JMS exception
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		vlogDebug("Begin:@Class: TRUPriceListCacheInvalidationMessageSink : @Method: receiveMessage()");
		vlogDebug("pPortName : {0}", pPortName);
		vlogDebug("pMessage : {0}", pMessage);

		// invalidate the price list repository cache.
		if (getPriceList() != null) {
			getPriceList().invalidateCaches();
			getPriceCache().flush();

		}
		vlogDebug("End:@Class: TRUPriceListCacheInvalidationMessageSink : @Method: receiveMessage()");
	}

	/**
	 * Gets the price list.
	 * 
	 * @return the priceList
	 */
	public GSARepository getPriceList() {
		return mPriceList;
	}

	/**
	 * Sets the price list.
	 * 
	 * @param pPriceList
	 *            the priceList to set
	 */
	public void setPriceList(GSARepository pPriceList) {
		mPriceList = pPriceList;
	}
}
