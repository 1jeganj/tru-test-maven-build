package com.tru.feedprocessor.tol.price.processor;

import java.util.Map;

import atg.nucleus.ServiceMap;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.processor.FeedItemChangeDetectorProcessor;
import com.tru.feedprocessor.tol.base.processor.support.IFeedItemValidator;
import com.tru.feedprocessor.tol.base.processor.support.IItemValueChangeInspector;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class TRUPriceFeedItemChangeDetectorProcessor.
 * 
 * First this class will validate the data according to the respective data type in feed vo, if the validation fails it
 * will skip that record as error record and process the next record.
 * 
 * Also checks whether the price item exists in the database or not based on price id. If price item exists in data base
 * then it will execute the next step(inspector).
 * 
 * Inspector will inspect is there any change information in VO and repository. and in <code>doProcess</code> default is
 * <code>true</code> for <code>isDataUpdated</code>
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedItemChangeDetectorProcessor extends FeedItemChangeDetectorProcessor {
	/**
	 * ItemValueChangedMapper.
	 */
	private ServiceMap mItemValueChangeInspector;

	/**
	 * The entity id map.
	 */
	private Map<String, Map<String, String>> mEntityIdMap;

	/**
	 * The identifier Store key.
	 */
	private String mIdentifierIdStoreKey;

	/**
	 * Gets the identifier id store key.
	 * 
	 * @return the identifierIdStoreKey
	 */
	public String getIdentifierIdStoreKey() {
		return mIdentifierIdStoreKey;
	}

	/**
	 * Sets the identifier id store key.
	 * 
	 * @param pIdentifierIdStoreKey
	 *            the identifierIdStoreKey to set
	 */
	public void setIdentifierIdStoreKey(String pIdentifierIdStoreKey) {
		mIdentifierIdStoreKey = pIdentifierIdStoreKey;
	}

	/**
	 * Gets the item value change inspector.
	 * 
	 * @return the itemValueChangedMapper
	 */
	public ServiceMap getItemValueChangeInspector() {
		return mItemValueChangeInspector;
	}

	/**
	 * Sets the item value change inspector.
	 * 
	 * @param pItemValueChangeInspector
	 *            - pItemValueChangeInspector
	 */
	public void setItemValueChangeInspector(ServiceMap pItemValueChangeInspector) {
		mItemValueChangeInspector = pItemValueChangeInspector;
	}

	/**
	 * Check whether the price item exists in the database or not based on price id.
	 * 
	 * @param pVo
	 *            the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(TRUPriceFeedVO pVo) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: checkEntityExists()");
		boolean exists = Boolean.FALSE;
		if (pVo != null) {
			Map<String, String> priceMap = mEntityIdMap.get(pVo.getPriceListId());

			if (null != priceMap && !priceMap.isEmpty() && priceMap.containsKey(pVo.getSkuId())) {
				exists = Boolean.TRUE;
			}
		}
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: checkEntityExists()" + exists);
		return exists;
	}

	/**
	 * First this method will validate the data according to the respective data
	 * type in feed vo, if the validation fails it will skip that record as
	 * error record and process the next record.
	 * 
	 * Also checks whether the price item exists in the database or not based on
	 * price id. If price item exists in data base then it will execute the next
	 * step(inspector).
	 * 
	 * Inspector will inspect is there any change information in VO and
	 * repository. and in <code>doProcess</code> default is <code>true</code>
	 * for <code>isDataUpdated</code>
	 * 
	 * If <code>isDataUpdated</code> is false it will not do any thing in
	 * repository level during writing the data. if <code>isDataUpdated</code>
	 * is true it will take effect for add/update in repository during writing
	 * the data.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @return the base feed process vo
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	@Override
	public BaseFeedProcessVO doProcess(BaseFeedProcessVO pFeedVO) throws FeedSkippableException, RepositoryException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess()");
		TRUPriceFeedVO price = (TRUPriceFeedVO) pFeedVO;
		IFeedItemValidator lFeedValidator = null;
		if (getValidators() != null && !getValidators().isEmpty()) {
			lFeedValidator = (IFeedItemValidator) getValidators().get(pFeedVO.getEntityName());
			try {
				lFeedValidator.validate(price);
			} catch (FeedSkippableException e) {
				getLogger().vlogError("@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess() {0} ", e.getMessage() + FeedConstants.SPACE + price.getSkuId());
				throw new FeedSkippableException(getPhaseName(), getStepName(), e.getMessage() + FeedConstants.SPACE + price.getSkuId() + FeedConstants.SPACE+ price.getSkuId(), pFeedVO, e);
			}
		}
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess() :: after validator call");
		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());

		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());
		IItemValueChangeInspector lItemValueChangeInspector = null;

		if (mEntityIdMap != null && !mEntityIdMap.isEmpty() && checkEntityExists(price)) {
			boolean isDataUpdated = true;
			if (getItemValueChangeInspector() != null && !getItemValueChangeInspector().isEmpty()) {
				lItemValueChangeInspector = (IItemValueChangeInspector) getItemValueChangeInspector().get(
						pFeedVO.getEntityName());
				
				String repId = mEntityIdMap.get(price.getPriceListId()).get(price.getSkuId());
				RepositoryItem lCurrentItem = (MutableRepositoryItem) getRepository(lRepositoryName).getItem(repId, lItemDiscriptorName);
				
				if (lItemValueChangeInspector != null && lCurrentItem != null) {
					isDataUpdated = lItemValueChangeInspector.isUpdated(price, lCurrentItem);
				}
			}
			if (isDataUpdated) {
				getLogger().vlogDebug(
						"Begin:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess()" + isDataUpdated);
				return price;
			} else {
				getLogger().vlogDebug(
						"Begin:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess()" + isDataUpdated);
				return null;
			}
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemChangeDetectorProcessor : @Method: doProcess()");
		return price;
	}
}
