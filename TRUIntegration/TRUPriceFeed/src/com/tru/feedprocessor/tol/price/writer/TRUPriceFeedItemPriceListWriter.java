package com.tru.feedprocessor.tol.price.writer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.mapper.AbstractFeedItemMapper;
import com.tru.feedprocessor.tol.base.mapper.IFeedItemMapper;
import com.tru.feedprocessor.tol.base.vo.BaseFeedProcessVO;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;

/**
 * The Class PriceFeedItemPriceListWriter is extended to implement customized logic for respective price feed.
 * 
 * Write the data into Repository by calling add based on the data in mEntityIdMap.
 * 
 * @version 1.0
 *  @author Professional Access
 */
public class TRUPriceFeedItemPriceListWriter extends PriceFeedItemPriceListWriter {
	/** The entity id map. */
	private Map<String, Map<String, String>> mEntityIdMap;

	/**
	 * This method is used to check whether price repositoryID is present in entity map of pricelist.
	 * 
	 * @param pVo
	 *            the vo
	 * @return true, if successful
	 */
	private boolean checkEntityExists(TRUPriceFeedVO pVo) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListWriter : @Method: checkEntityExists()");
		boolean exists = Boolean.FALSE;
		Map<String, String> priceListMap = (HashMap) mEntityIdMap.get(TRUPriceFeedReferenceConstants.PRICELIST);
		if (null != priceListMap && !priceListMap.isEmpty() && priceListMap.containsKey(pVo.getRepositoryId())) {
			exists = Boolean.TRUE;
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceListWriter : @Method: checkEntityExists()" + exists);
		return exists;
	}

	/**
	 * this method is used to set the required values to the entity map.
	 */

	@Override
	public void doOpen() {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListWriter : @Method: doOpen()");
		Collection values = getMapperMap().values();
		Iterator iterator = values.iterator();
		while (iterator.hasNext()) {
			AbstractFeedItemMapper itemMapper = (AbstractFeedItemMapper) iterator.next();
			itemMapper.setFeedHelper(getFeedHelper());
		}
		mEntityIdMap = (Map<String, Map<String, String>>) retriveData(getIdentifierIdStoreKey());
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceListWriter : @Method: doOpen()");
	}

	/**
	 * (non-Javadoc).
	 * 
	 * @param pVOItems
	 *            the vO items
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @see com.tru.feedprocessor.tol.catalog.writer.AbstractFeedItemWriter.
	 *      AbstractFeedItemWriter#write(java.util.List)
	 */
	@Override
	public void doWrite(List<? extends BaseFeedProcessVO> pVOItems) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListWriter : @Method: doOpen()");
		for (BaseFeedProcessVO lBaseFeedProcessVO : pVOItems) {
			TRUPriceFeedVO priceVO = (TRUPriceFeedVO) lBaseFeedProcessVO;
			if (mEntityIdMap != null && !mEntityIdMap.isEmpty() && !checkEntityExists(priceVO)) {
				addItem(priceVO);
			}
		}
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListWriter : @Method: doOpen()");
	}

	/**
	 * Adds the feed VO object details into the repository item.
	 * 
	 * @param pFeedVO
	 *            the feed vo
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public void addItem(BaseFeedProcessVO pFeedVO) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedItemPriceListWriter : @Method: addItem()");
		MutableRepositoryItem lCurrentItem = null;
		String lRepositoryName = getEntityRepositoryName(pFeedVO.getEntityName());
		String lItemDiscriptorName = getItemDescriptorName(pFeedVO.getEntityName());

		if (lItemDiscriptorName != null) {
			RepositoryItem priceListItem = getRepository(lRepositoryName).getItem(pFeedVO.getRepositoryId(),
					FeedConstants.PRICELIST);
			if (null == priceListItem) {
				IFeedItemMapper lICatFeedItemMapper = (IFeedItemMapper) getMapperMap().get(lItemDiscriptorName);

				lCurrentItem = getRepository(lRepositoryName)
						.createItem(pFeedVO.getRepositoryId(), lItemDiscriptorName);
				beforeAddItem(pFeedVO, lCurrentItem);
				lCurrentItem = lICatFeedItemMapper.map(pFeedVO, lCurrentItem);

				getRepository(lRepositoryName).addItem(lCurrentItem);
				addInsertItemToList(pFeedVO);
			}
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedItemPriceListWriter : @Method: addItem()");
	}
}
