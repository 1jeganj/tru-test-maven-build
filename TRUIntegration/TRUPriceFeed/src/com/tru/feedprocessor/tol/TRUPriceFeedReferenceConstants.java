package com.tru.feedprocessor.tol;

/**
 * The Class PriceFeedReferenceConstants.
 * @version 1.0
 * @author Professional Access
 */
public class TRUPriceFeedReferenceConstants {

	/** The Constant XLS_SHEET. */
	public static final String XLS_SHEET = "sheet";

	/** The Constant PIPE_DELIMITER. */
	public static final char PIPE_DELIMITER = '|';

	/** The Constant SKU_ID. */
	public static final String SKU_ID = "skuId";

	/** The Constant PRICE_VO. */
	public static final String PRICE_VO = "com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO";

	/** The Constant LOCALE. */
	public static final String LOCALE = "locale";

	/** The Constant COUNTRY_CODE. */
	public static final String COUNTRY_CODE = "countryCode";

	/** The Constant MARKET_CODE. */
	public static final String MARKET_CODE = "marketCode";

	/** The Constant STORE_NUMBER. */
	public static final String STORE_NUMBER = "storeNumber";

	/** The Constant DEAL_ID. */
	public static final String DEAL_ID = "dealID";

	/** The Constant LIST_PRICE. */
	public static final String LIST_PRICE = "listPrice";

	/** The Constant SALE_PRICE. */
	public static final String SALE_PRICE = "salePrice";

	/** The Constant PRICE_LIST_ID. */
	public static final String PRICE_LIST_ID = "id";

	/** The Constant PRICE_ID. */
	public static final String PRICE_ID = "price_id";

	/** The Constant DISPLAY_NAME. */
	public static final String DISPLAY_NAME = "displayName";

	/** The Constant PRICELIST. */
	public static final String PRICELIST = "priceList";

	/** The Constant DOUBLE_SLASH_DOT. */
	public static final String DOUBLE_SLASH_DOT = "\\.";

	/** The Constant PROCESSING_DIRECTORY. */
	public static final String PROCESSING_DIRECTORY = "processingDirectory";

	/** The Constant UN_PROCESS_DIRECTORY. */
	public static final String UN_PROCESS_DIRECTORY = "unProcessDirectory";

	/** The Constant BAD_DIRECTORY. */
	public static final String BAD_DIRECTORY = "badDirectory";

	/** The Constant DOUBLE_VAL. */
	public static final double DOUBLE_VAL = 0.0;

	/** The Constant NUMBER_HUNDRED. */
	public static final int NUMBER_HUNDRED = 100;

	/** The Constant PIPE_DELIMITER_STRING. */
	public static final String PIPE_DELIMITER_STRING = "|";

	/** The Constant IS_NUMBER_REGX. */
	public static final String IS_NUMBER_REGX = "[0-9]+(\\.[0-9]+)";

	/** The Constant INVALID_RECORD. */
	public static final String INVALID_RECORD = "Invalid Record:";

	/** The Constant SUCCESS_DIRECTORY. */
	public static final String SUCCESS_DIRECTORY = "successDirectory";

	/** The Constant ERROR_DIRECTORY. */
	public static final String ERROR_DIRECTORY = "errorDirectory";

	/** The Constant PRICE_FEED_DATA_VALIDATION_STEP. */
	public static final String PRICE_FEED_DATA_VALIDATION_STEP = "priceFeedDataValidationStep";

	/** The Constant PRICE_FEED_DATA_EXISTS_STEP. */
	public static final String PRICE_FEED_DATA_EXISTS_STEP = "priceFeedDataExistsStep";

	/** The Constant XLS. */
	public static final String XLS = "xls";

	/** The Constant REQUIRED_COUNTRY_CODE. */
	public static final String REQUIRED_COUNTRY_CODE = "Invalid Record: Country Code Is Required";

	/** The Constant REQUIRED_MARKET_CODE. */
	public static final String REQUIRED_MARKET_CODE = "Invalid Record: Market Code Is Required";

	/** The Constant REQUIRED_LOCALE. */
	public static final String REQUIRED_LOCALE = "Invalid Record: Locale Is Required";

	/** The Constant REQUIRED_SKUID. */
	public static final String REQUIRED_SKUID = "Invalid Record: Sku Id Is Required";

	/** The Constant REQUIRED_STORE_NO. */
	public static final String REQUIRED_STORE_NO = "Invalid Record: Store Number Is Required";

	/** The Constant REQUIRED_LIST_PRICE. */
	public static final String REQUIRED_LIST_PRICE = "Invalid Record: List Price Is Required";

	/** The Constant REQUIRED_SALE_PRICE. */
	public static final String REQUIRED_SALE_PRICE = "Invalid Record: Sale Price Is Required";

	/** The Constant INVALID_SALE_PRICE. */
	public static final String INVALID_SALE_PRICE = "Invalid Record: Sale Price Is Not Valid";

	/** The Constant INVALID_LIST_PRICE. */
	public static final String INVALID_LIST_PRICE = "Invalid Record: List Price Is Not Valid";

	/** The Constant INVALID_STORE_NO. */
	public static final String INVALID_STORE_NO = "Invalid Record: Store Number Is Not Valid";

	/** The Constant REQUIRED_DISPLAY_NAME. */
	public static final String REQUIRED_DISPLAY_NAME = "Invalid Record: Display Name Is Required";

	/** The Constant INVALID_COUNTRY_CODE. */
	public static final String INVALID_COUNTRY_CODE = "Invalid Record: Country Code Is Not Valid";

	/** The Constant INVALID_MARKET_CODE. */
	public static final String INVALID_MARKET_CODE = "Invalid Record: Market Code Is Not Valid";

	/** The Constant INVALID_LOCALE. */
	public static final String INVALID_LOCALE = "Invalid Record: Locale Is Not Valid";

	/** The Constant INVALID_SKUID. */
	public static final String INVALID_SKUID = "Invalid Record: Sku Id Is Not Valid";

	/** The Constant FEED_FILE_PATTERN. */
	public static final String FEED_FILE_PATTERN = "feedFilePattern";

	/** The Constant FILE_NAME_TRU_ENV. */
	public static final String FILE_NAME_TRU_ENV = "fileNameTRUEnv";

	/** The Constant FILE_FEED_NAME. */
	public static final String FILE_FEED_NAME = "fileFeedName";

	/** The Constant STRING_VAL_WITH_3_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_3_DIGIT_PATTERN = "^[a-zA-Z]{1,3}$";

	/** The Constant STRING_VAL_WITH_5_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_5_DIGIT_PATTERN = "^[\\w]{1,5}$";
	
	/** The Constant STRING_VAL_WITH_NUMBER_5_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_NUMBER_5_DIGIT_PATTERN = "(?<!\\d)+[0-9]{5}$";

	/** The Constant INTEGER_VAL_WITH_5_DIGIT_PATTERN. */
	public static final String INTEGER_VAL_WITH_5_DIGIT_PATTERN = "^[0-9]{1,5}$";

	/** The Constant DOUBLE_VAL_WITH_DECIMAL_DIGIT_PATTERN. */
	public static final String DOUBLE_VAL_WITH_DECIMAL_DIGIT_PATTERN = "^([0-9]{1,19})?([.]{1}[0-9]{1,7})?$";

	/** The Constant DECIMAL_FORMAT_UP_TO_2_DIGIT. */
	public static final String DECIMAL_FORMAT_UP_TO_2_DIGIT = "0.00";
	
	/** The Constant FEED_FILE_HEAD_RECORD_KEY. */
	public static final String FEED_FILE_HEAD_RECORD_KEY = "HEADER";
	
	/** The Constant SALEPRICELIST. */
	public static final String SALEPRICELIST = "salePriceList";
	
	/** The Constant LISTPRICELIST. */
	public static final String LISTPRICELIST = "listPriceList";

	/** The Constant TRUE. */
	public static final String TRUE = "true";
	
	/** The Constant UNDERSCORE. */
	public static final String UNDERSCORE = "_";
	
	/** The Constant SOURCE_DIRECTORY. */
	public static final String SOURCE_DIRECTORY = "sourceDirectory";
	
	/** The Constant PRICE_REPOSITORY. */
	public static final String PRICE_REPOSITORY = "priceRepository";
	
	/** The Constant BASE_PRICE_LIST. */
	public static final String BASE_PRICE_LIST = "basePriceList";
	
	/** The Constant PRICE_CONSTANT. */
	public static final String PRICE = "price";
	
	/** The Constant FEED_SKIPPABLE_EXCEPTION. */
	public static final String FEED_SKIPPABLE_EXCEPTION = "FeedSkippableException";
	
	/** The Constant PRICE_FEED. */
	public static final String PRICE_FEED = "pricefeed";

	/** The Constant FEED_FILE_SEQUENCE. */
	public static final String FEED_FILE_SEQUENCE = "feedfilesequence";

	/** The Constant FILE_SEQUENCE_MISSING. */
	public static final String FILE_SEQUENCE_MISSING = "File sequence missing";

	/** The Constant FILE_SEQUENCE_NO_MATCH. */
	public static final String FILE_SEQUENCE_NO_MATCH = "File sequence no match";
	
	/** The Constant for Space. */
	public static final String SPACE = " ";
	
	/** The Constant NUMBER_ZERO. */
	public static final int NUMBER_ZERO = 0;
	
	/** The Constant NUMBER_ONE. */
	public static final int NUMBER_ONE = 1;

	/** The Constant EMAIL_FAIL_MESSAGE1. */
	public static final String EMAIL_FAIL_MESSAGE1_PRICE = "TRU ATG Web Store - USA - ";

	/** The Constant EMAIL_FAIL_MESSAGE2. */
	public static final String EMAIL_FAIL_MESSAGE2_PRICE = " - Price - ";

	/** The Constant EMAIL_FAIL_MESSAGE3. */
	public static final String EMAIL_FAIL_MESSAGE3_PRICE = " Processed with Errors - ";

	/** The Constant EMAIL_FAIL_MESSAGE4. */
	public static final String EMAIL_FAIL_MESSAGE4_PRICE = " Processing Failed - ";

	/** The Constant EMAIL_FAIL_MESSAGE5. */
	public static final String EMAIL_FAIL_MESSAGE5_PRICE = "MMddyyyy'-'hh:mm:ss";

	/** The Constant EMAIL_FAIL_MESSAGE6. */
	public static final String EMAIL_FAIL_MESSAGE6_PRICE = " Processed Successfully - ";

	/** The Constant SEQ_MISS. */
	public static final String SEQ_MISS = "sequenceMiss";

	/** The Constant JOB_FAILED_MISSING_SEQ. */
	public static final String JOB_FAILED_MISSING_SEQ = "Job Failed - File with a missing sequence number";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_MISSING_FILE = "Job Failed - Missing File";

	/** The Constant JOB_FAILED_MISSING_FILE. */
	public static final String JOB_FAILED_INVALID_FILE = "Job Failed - XML - XSD Validation Errors";

	/** The Constant JOB_FAILED_FILE_CORRUPT. */
	public static final String JOB_FAILED_FILE_CORRUPT = "Job Failed - Input File is Corrupt";

	/** The Constant FILE_INVALID. */
	public static final String FILE_INVALID = "fileInvalid";

	/** The Constant JOB_SUCCESS. */
	public static final String JOB_SUCCESS = "File has been successfully processed";

	/** The Constant BUT_WITH_ERRORS. */
	public static final String BUT_WITH_ERRORS = " but with some errors. Please find mail attachment for the errors.";

	/** The Constant PROCESS_FILE_SUCCESS. */
	public static final String PROCESS_FILE_SUCCESS = "File has been successfully processed";

	/** The Constant FILE_PROCESSES_WITH_ERRORS. */
	public static final String FILE_PROCESSED_WITH_ERRORS = "File has been successfully processed, but with some errors. Please find mail attachment for the errors.";

	/** The Constant NUMBER_TWO. */
	public static final int NUMBER_TWO = 2;

	/** The Constant INVALID_FILE. */
	public static final String INVALID_FILE = "Input File is Corrupt";

	/** The Constant SUB_DATE_FORMAT. */
	public static final String SUB_DATE_FORMAT = "MMddyyyy-HH:mm:ss";
	
	/** The Constant FEED_DATE_FORMAT. */
	public static final String FEED_DATE_FORMAT = "MMddyyyy'-'HH:mm:ss";
	
	/** The Constant PERMISSION_ISSUE. */
	public static final String PERMISSION_ISSUE = "Permission issue";
	
	/** The Constant FOLDER_NOT_EXISTS. */
	public static final String FOLDER_NOT_EXISTS = "Folder not exists";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_1. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_1= "Job Failed - Folder ";
	
	/** The Constant JOB_FAILED_PERMISSION_ISSUE_2. */
	public static final String JOB_FAILED_PERMISSION_ISSUE_2= " permissions issues";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_1. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_1= "Job Failed - Directory/folder ";
	
	/** The Constant JOB_FAILED_FOLDER_NOT_EXISTS_2. */
	public static final String JOB_FAILED_FOLDER_NOT_EXISTS_2= " structure not available";
	
	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT = "(?<!\\d)+[0-9]{4}-[0-9]{2}-[0-9]{2}$";
	
	/** The Constant STRING_VAL_WITH_NUMBER_6_DIGIT_PATTERN. */
	public static final String STRING_VAL_WITH_NUMBER_6_DIGIT_PATTERN = "(?<!\\d)+[0-9]{6}$";
	
	/** The Constant INVALID_DATE. */
	public static final String INVALID_DATE = " Invalid Date";
	
	/** The Constant INVALID_SEQ. */
	public static final String INVALID_SEQ = " Invalid Sequence";
	
	/** The Constant INVALID_TRAILER_RECORD_COUNT. */
	public static final String INVALID_TRAILER_RECORD_COUNT = " Invalid tail record count";
	
	/** The Constant INVALID_TRAILER_RECORD_TOTAL. */
	public static final String INVALID_TRAILER_RECORD_TOTAL = " Invalid tail record total";
	
	/** The Constant INVALID_HEADER_DET. */
	public static final String INVALID_HEADER_DET = " Invalid Header";
	
	/** The Constant INVALID_TRAILER_DET. */
	public static final String INVALID_TRAILER_DET = " Invalid Trailer";
	
	/** The Constant FILE_DOWNLOAD. */
	public static final String FILE_DOWNLOAD = "File Download";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant TIME_TAKEN. */
	public static final String TIME_TAKEN = "Time Taken";
	
	/** The Constant START_TIME. */
	public static final String START_TIME = "Start Time";
	
	/** The Constant START_DATE. */
	public static final String START_DATE = "Start Date";

	/** The Constant END_TIME. */
	public static final String END_TIME = "End Time";
	
	/** The Constant FILES_MOVED_SUCCESS. */
	public static final String FILES_MOVED_SUCCESS = "File Moved successfully";
	
	/** The Constant NO_MATCH_FIELS. */
	public static final String NO_MATCH_FIELS = "No Match Files";
	
	/** The Constant FILE_VALID. */
	public static final String FILE_VALID = "File is valid";
	
	/** The Constant FILE_NO_SEQ. */
	public static final String FILE_NO_SEQ = "File is not in sequence with last processed";
	
	/** The Constant FILE_PARSING. */
	public static final String FILE_PARSING = "File Parsing";
	
	/** The Constant FILE_VALIDATION. */
	public static final String FILE_VALIDATION = "File Validation";
	
	/** The Constant ALERT_LOGGER. */
	public static final String ALERT_LOGGER = "Alert logger";
	
	/** The Constant FEED_PROCESS. */
	public static final String FEED_PROCESS = "Feed Process";
	
	/** The Constant FILES_WRITING. */
	public static final String FILES_WRITING = "File Writing";
	
	/** The Constant FEED_STATUS. */
	public static final String FEED_STATUS = "Feed Status";
	
	/** The Constant FEED_FAILED. */
	public static final String FEED_FAILED = "Feed Failed";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_COMPLETED = "Feed completed";
	
	/** The Constant FEED_COMPLETED. */
	public static final String FEED_EXCEPTION = "Exception in removeAllExtendedPriceItems";

}
