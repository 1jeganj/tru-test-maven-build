package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFileExistsTask is used to check whether file exists or not.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFileExistsTask extends FileExistsTask {

	/**
	 * we need to override doExecute() to check whether file exists or not in the specified processing directory.
	 * 
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFileExistsTask : @Method: doExecute()");
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		if (null != curExecCntx && null != curExecCntx.getFileName()) {
			getLogger().vlogDebug("TRUPriceFileExistsTask : curExecCntx: {0}::: FilePath : {1}::: FileName : {2} ",curExecCntx,curExecCntx.getFilePath(),curExecCntx.getFileName());
			File lFile = new File(curExecCntx.getFilePath() + curExecCntx.getFileName());
			getLogger().vlogDebug("TRUPriceFileExistsTask : lFile : {0} " , lFile);
			getLogger().vlogDebug("TRUPriceFileExistsTask : lFile Can Read : {0}" , lFile.canRead());
			getLogger().vlogDebug("TRUPriceFileExistsTask : lFile is File : {0} " , lFile.isFile());
			if (lFile == null || !lFile.exists() || !lFile.isFile() || !lFile.canRead()) {
				logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
				throw new FeedSkippableException(getPhaseName(), getStepName(), FeedConstants.FILE_DOES_NOT_EXISTS,
						null,null);
			}
			logSuccessMessage(startDate, TRUPriceFeedReferenceConstants.FILES_MOVED_SUCCESS);
		} else {
			logFailedMessage(startDate, FeedConstants.FILE_DOES_NOT_EXISTS);
			throw new FeedSkippableException(getPhaseName(), getStepName(), FeedConstants.FILE_DOES_NOT_EXISTS, null,null);
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFileExistsTask : @Method: doExecute()");
	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds( TRUPriceFeedReferenceConstants.PRICE_FEED,  TRUPriceFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess( TRUPriceFeedReferenceConstants.PRICE_FEED,  TRUPriceFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

}
