package com.tru.feedprocessor.tol.base.tasklet;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.filefilter.RegexFileFilter;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFeedFileDownloadTask is used move feed file from shared
 * directory to processing directory.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFeedFileDownloadTask extends FileDownloadTask {
	/**
	 * Property to hold destinationDirectory Path.
	 */
	private String mProcessingDir;
	/**
	 * Property to hold SourceDirectory Path.
	 */
	private String mSourceDir;

	/** The Un processed dir. */
	private String mUnProcessedDir;

	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	
	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;
	
	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;
	
	/** The Price feed repository properties. */
	private TRUPriceFeedRepositoryProperties mPriceFeedRepositoryProperties;
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}


	/**
	 * This doExecute() method will download the file from Source Directory and
	 * place it in Destination Directory.
	 *
	 * @throws FeedSkippableException             the feed skippable exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws RepositoryException the repository exception
	 */
	@Override
	protected void doExecute() throws FeedSkippableException, FileNotFoundException, IOException, RepositoryException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: doExecute()");
		mSourceDir = nullIfEmpty(getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString());
		mProcessingDir = nullIfEmpty(getConfigValue(TRUPriceFeedReferenceConstants.PROCESSING_DIRECTORY).toString());
		mUnProcessedDir = nullIfEmpty(getConfigValue(TRUPriceFeedReferenceConstants.UN_PROCESS_DIRECTORY).toString());
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		setConfigValue(TRUPriceFeedReferenceConstants.START_DATE, startDate);
		File sharedFolderFile = new File(mSourceDir);
		if (sharedFolderFile.exists() && sharedFolderFile.isDirectory() && sharedFolderFile.canRead()) {
			moveSrcToProcessingDir(startDate);
		} else if(!sharedFolderFile.exists() || !sharedFolderFile.isDirectory()){
			String messageSubject = TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			logFailedMessage(startDate, TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!sharedFolderFile.canRead()) {
			String messageSubject = TRUPriceFeedReferenceConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(mSourceDir, messageSubject);
			logFailedMessage(startDate, TRUPriceFeedReferenceConstants.PERMISSION_ISSUE);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		}
		
		getLogger().vlogDebug("TRUPriceFeedFileDownloadTask :Source Directory: {0} ", mSourceDir);
		getLogger().vlogDebug("TRUPriceFeedFileDownloadTask :Processing Directory: {0}", mProcessingDir);
		getLogger().vlogDebug("TRUPriceFeedFileDownloadTask :UnProcessing Directory: {0}", mUnProcessedDir);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: doExecute()");
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put(TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put(TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put(TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds(TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FILE_DOWNLOAD, null, extenstions);
	}
	

	
	/**
	 * Move src to processing dir.
	 *
	 * @param pStartDate the start date
	 * @return true, if successful
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws RepositoryException the repository exception
	 * @throws FeedSkippableException the feed skippable exception
	 */
	public boolean moveSrcToProcessingDir(String pStartDate) throws FileNotFoundException, IOException, RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: moveSrcToProcessingDir()");
		File sourceFolder = new File(mSourceDir);
		File processingFolder = new File(mProcessingDir);
		boolean filesExists = Boolean.FALSE;
		File processFile = null;
		File[] sourceFiles = null;
		String filePattern = null;
		if (sourceFolder != null && sourceFolder.exists() && sourceFolder.canRead()
				&& null != getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN)) {
			filePattern = getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_PATTERN).toString();
			sourceFiles = getMatchedFileList(sourceFolder, filePattern);
			getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : filePattern : {0} sourceFiles {1} ", filePattern, sourceFiles);
			if (sourceFiles != null) {

				processFile = getLatestFile(sourceFiles);

				if (processFile != null) {
					getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : processFile : {0}", processFile);
					getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : processFile Name: {0}", processFile.getName());
					setConfigValue(FeedConstants.FILENAME, processFile.getName());
					setConfigValue(FeedConstants.FILEPATH, mProcessingDir);
					setConfigValue(FeedConstants.FILETYPE, getExtenstion(processFile.getName()));
				}
			}
		} 
		File unProcessFolder = new File(mUnProcessedDir);
		if (processingFolder != null && processingFolder.exists() && processingFolder.isDirectory() && processingFolder.canWrite() && unProcessFolder != null
				&& unProcessFolder.exists() && unProcessFolder.isDirectory() && unProcessFolder.canWrite()) {
			try {
				getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : Before moveAllFiles processFile : {0}", processFile);
				getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : unProcessFolder : {0}", unProcessFolder);
				getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : processingFolder : {0}", processingFolder);
				getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : sourceFolder : {0}", sourceFolder);
				sourceFiles = null;
				moveAllFiles(sourceFolder, processingFolder.getCanonicalPath(), processFile, unProcessFolder.getCanonicalPath());
			} catch (FileNotFoundException e) {
				if (isLoggingError()) {
					logError("@Class: TRUPriceFeedFileDownloadTask : @Method: moveSrcToProcessingDir(): ", e);
				}
				if(e.getMessage() != null){
					logFailedMessage(pStartDate, e.getMessage());
				}else {
					logFailedMessage(pStartDate, TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS);
				}
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("@Class: TRUPriceFeedFileDownloadTask : @Method: moveSrcToProcessingDir(): ", ioe);
				}
				if(ioe.getMessage() != null){
					logFailedMessage(pStartDate, ioe.getMessage());
				}else {
					logFailedMessage(pStartDate, TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS);
				}
			}
		} else if(!processingFolder.exists() || !processingFolder.isDirectory()){ 
			String messageSubject = TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(processingFolder.getPath(), messageSubject);
			logFailedMessage(pStartDate, TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!processingFolder.canRead()){
			String messageSubject = TRUPriceFeedReferenceConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(processingFolder.getPath(), messageSubject);
			logFailedMessage(pStartDate, TRUPriceFeedReferenceConstants.PERMISSION_ISSUE);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		} else if(!unProcessFolder.exists() || !unProcessFolder.isDirectory()){
			String messageSubject = TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS;
			notifyFileDoseNotExits(unProcessFolder.getPath(), messageSubject);
			logFailedMessage(pStartDate, TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS, null, null);
		} else if(!unProcessFolder.canRead()){
			String messageSubject = TRUPriceFeedReferenceConstants.PERMISSION_ISSUE;
			notifyFileDoseNotExits(unProcessFolder.getPath(), messageSubject);
			logFailedMessage( pStartDate, TRUPriceFeedReferenceConstants.PERMISSION_ISSUE);
			throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.PERMISSION_ISSUE, null, null);
		}

		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: moveSrcToProcessingDir() : File Exists:{0}", filesExists);
		return filesExists;
	}

	/**
	 * This method is used to move all file from shared directory to processing
	 * or unprocessed directory.
	 * 
	 * @param pSharedDir
	 *            - shared directory
	 * @param pProcessingDir
	 *            the processing dir
	 * @param pProcessFile
	 *            the process file
	 * @param pUnProcessFolder
	 *            the un process folder
	 * @throws FileNotFoundException
	 *             - Throws FileNotFoundException
	 * @throws IOException
	 *             - Throws IOException
	 */
	public void moveAllFiles(File pSharedDir, String pProcessingDir, File pProcessFile, String pUnProcessFolder) throws FileNotFoundException,
			IOException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: moveAllFiles()");
		File[] files = null;
		if (pSharedDir.exists() && pSharedDir.isDirectory()) {
			files = pSharedDir.listFiles();
		}
		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isFile()) {
					if (null != pProcessFile && file.getName().equals(pProcessFile.getName())) {
						moveFileToFolder(pSharedDir + FeedConstants.SLASH + file.getName(), pProcessingDir);
					} else {
						moveFileToFolder(pSharedDir + FeedConstants.SLASH + file.getName(), pUnProcessFolder);
					}
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: moveAllFiles()");
	}

	/**
	 * This method is used for moving the file from source directory to
	 * destination directory.
	 * 
	 * @param pFromFile
	 *            - String
	 * @param pToFolder
	 *            - String
	 * @return boolean - boolean
	 * @throws FileNotFoundException
	 *             - Throws FileNotFoundException
	 * @throws IOException
	 *             - Throws IOException
	 */
	public boolean moveFileToFolder(String pFromFile, String pToFolder) throws FileNotFoundException, IOException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: moveFileToFolder()");
		getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : FromFile : {0} To File : {1}", pFromFile, pToFolder);
		File fromFile = new File(pFromFile);
		File toFolder = new File(pToFolder);
		if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
			toFolder.mkdirs();
		}
		boolean value = fromFile.renameTo(new File(toFolder, fromFile.getName()));
		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: moveFileToFolder()");
		return value;
	}

	/**
	 * This method returns the file extension.
	 * 
	 * @param pName
	 *            the name
	 * @return extension
	 */
	public String getExtenstion(String pName) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: getExtenstion()");
		String[] str = pName.split(TRUPriceFeedReferenceConstants.DOUBLE_SLASH_DOT);
		if (str.length > FeedConstants.NUMBER_ONE) {
			return str[str.length - FeedConstants.NUMBER_ONE];
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: getExtenstion()");
		return null;
	}

	/**
	 * Gets the latest file based on the file sequence number.
	 * 
	 * @param pSourceFiles
	 *            the source files
	 * @return the latest file
	 * @throws RepositoryException
	 *             the repository exception
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 */
	public File getLatestFile(File[] pSourceFiles) throws RepositoryException, FeedSkippableException {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: getLatestFile()");
		int seqNo = FeedConstants.NUMBER_ZERO;
		String name = null;
		TreeMap<Integer, File> fileMap = new TreeMap<Integer, File>();
		List<Integer> fileSequenceList = new ArrayList<Integer>();
		for (File srcFile : pSourceFiles) {
			name = srcFile.getName();
			seqNo = Integer.parseInt(name.substring(name.lastIndexOf(FeedConstants.UNDER_SCORE) + FeedConstants.NUMBER_ONE,
					name.lastIndexOf(FeedConstants.DOT)));
			fileMap.put(seqNo, srcFile);
			fileSequenceList.add(seqNo);
		}

		File returnFile = null;
		if (checkFileSequence(fileSequenceList)) {
			Collections.sort(fileSequenceList);
			returnFile = fileMap.get(fileSequenceList.get(FeedConstants.NUMBER_ZERO));
		}

		fileMap = null;
		getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: getLatestFile()");
		return returnFile;
	}

	/**
	 * Check file sequence.
	 * 
	 * @param pFileSequenceList
	 *            the file sequence list
	 * @return the boolean
	 * @throws FeedSkippableException
	 *             the feed skippable exception
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private Boolean checkFileSequence(List<Integer> pFileSequenceList) throws FeedSkippableException, RepositoryException {

		getLogger().vlogDebug("Start @Class: TRUPriceFeedFileDownloadTask,  @method: checkFileSequence()");

		Boolean retVal = Boolean.FALSE;
		List<Integer> fileSequenceList = pFileSequenceList;
		if (!fileSequenceList.isEmpty()) {
			Collections.sort(fileSequenceList);
			RepositoryItem repoItem = getFileSequenceRepository().getItem(TRUPriceFeedReferenceConstants.PRICE_FEED,
					TRUPriceFeedReferenceConstants.FEED_FILE_SEQUENCE);
			if (repoItem != null) {
				Integer seqNo = (Integer) repoItem.getPropertyValue(getPriceFeedRepositoryProperties().getSeqProcessedPropertyName());
				if (fileSequenceList.get(TRUPriceFeedReferenceConstants.NUMBER_ZERO) != (seqNo + TRUPriceFeedReferenceConstants.NUMBER_ONE)) {
					String messageSubject = getFeedEnvConfiguration().getPriceFeedFilesSequenceNoMatch();
					notifyFileDoseNotExits(mSourceDir, messageSubject);
					getLogger().logError(TRUPriceFeedReferenceConstants.SPACE + getFeedEnvConfiguration().getPriceFeedFilesSequenceNoMatch());
					logFailedMessage(getConfigValue(TRUPriceFeedReferenceConstants.START_DATE).toString(), TRUPriceFeedReferenceConstants.FILE_NO_SEQ);
					throw new FeedSkippableException(getPhaseName(), getStepName(), TRUPriceFeedReferenceConstants.FILE_SEQUENCE_NO_MATCH, null, null);
				}
			}
			retVal = Boolean.TRUE;
		}
		getLogger().vlogDebug("End @Class: TRUPriceFeedFileDownloadTask, @method: checkFileSequence()");
		return retVal;
	}


	/**
	 * Gets the matched files which are matched with regular expression pattern
	 * and also checks whether the file name contains respective environment
	 * name and feed name.
	 * 
	 * @param pDirectory
	 *            the directory
	 * @param pPattern
	 *            the pattern
	 * @return the matched file list
	 */
	public File[] getMatchedFileList(File pDirectory, String pPattern) {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedFileDownloadTask : @Method: getMatchedFileList()");
		String fileNameTRUEnv = getConfigValue(TRUPriceFeedReferenceConstants.FILE_NAME_TRU_ENV).toString();
		String fileFeedName = getConfigValue(TRUPriceFeedReferenceConstants.FILE_FEED_NAME).toString();
		getLogger().vlogDebug("TRUPriceFeedFileDownloadTask : fileNameTRUEnv {0}: fileFeedName {0} : pPattern {0}", fileNameTRUEnv, fileFeedName,
				pPattern);
		FileFilter filter = new RegexFileFilter(pPattern);
		File[] patternMatchfiles = pDirectory.listFiles(filter);
		List<File> finalFiles = null;
		if (patternMatchfiles != null) {
			finalFiles = new ArrayList<File>();
			for (File file : patternMatchfiles) {
				if (file.getName().contains(fileNameTRUEnv) && file.getName().contains(fileFeedName)) {
					finalFiles.add(file);
				}
			}
			File[] finalArray = null;
			if (null != finalFiles) {
				finalArray = new File[finalFiles.size()];
				int i = FeedConstants.NUMBER_ZERO;
				for (File finalFile : finalFiles) {
					finalArray[i] = finalFile;
					i++;
				}
			}
			getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: getMatchedFileList() finalArray: {0}", finalArray);
			return finalArray;
		} else {
			getLogger().vlogDebug("End:@Class: TRUPriceFeedFileDownloadTask : @Method: getMatchedFileList() Empty finalArray");
			logFailedMessage(getConfigValue(TRUPriceFeedReferenceConstants.START_DATE).toString(), TRUPriceFeedReferenceConstants.NO_MATCH_FIELS);
			return null;
		}
	}
	
	/**
	 * Notify file dose not exits. This method used to notify the configured users if file not exists.
	 *
	 * @param pSourceDir            the source dir
	 * @param pMsgSubject the msg subject
	 */
	public void notifyFileDoseNotExits(String pSourceDir, String pMsgSubject) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String priority = getFeedEnvConfiguration().getFailureMailPriority2();
		String content = null;
		String detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		if (pMsgSubject.equalsIgnoreCase(getFeedEnvConfiguration().getPriceFeedFilesNoSequence())
				|| pMsgSubject.equalsIgnoreCase(getFeedEnvConfiguration().getPriceFeedFilesSequenceNoMatch())) {
			priority = TRUPriceFeedReferenceConstants.SEQ_MISS;
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_MISSING_SEQ;
			detailException = getFeedEnvConfiguration().getDetailedExceptionForSeqMiss();
		} else if (pMsgSubject.equalsIgnoreCase(getFeedEnvConfiguration().getPriceNoFeedFileSubject())) {
			priority = TRUPriceFeedReferenceConstants.SEQ_MISS;
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_MISSING_FILE;
			detailException = getFeedEnvConfiguration().getDetailedExceptionForNoFeed();
		} else if(pMsgSubject.contains(TRUPriceFeedReferenceConstants.FOLDER_NOT_EXISTS)){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_1 + pSourceDir + TRUPriceFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
			detailException = pSourceDir + TRUPriceFeedReferenceConstants.JOB_FAILED_FOLDER_NOT_EXISTS_2;
		} else if(pMsgSubject.contains(TRUPriceFeedReferenceConstants.PERMISSION_ISSUE)){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_1 + pSourceDir + TRUPriceFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
			detailException = pSourceDir + TRUPriceFeedReferenceConstants.JOB_FAILED_PERMISSION_ISSUE_2;
		}
		String subject = getFeedEnvConfiguration().getPriceFeedFailureSub();
		String[] args = new String[TRUPriceFeedReferenceConstants.NUMBER_TWO];
		args[TRUPriceFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUPriceFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.PRICE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
	}

	/**
	 * Gets the file sequence repository.
	 * 
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 * 
	 * @param pFileSequenceRepository
	 *            the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}
	
	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the price feed repository properties.
	 *
	 * @return the priceFeedRepositoryProperties
	 */
	public TRUPriceFeedRepositoryProperties getPriceFeedRepositoryProperties() {
		return mPriceFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 *
	 * @param pPriceFeedRepositoryProperties the priceFeedRepositoryProperties to set
	 */
	public void setPriceFeedRepositoryProperties(
			TRUPriceFeedRepositoryProperties pPriceFeedRepositoryProperties) {
		mPriceFeedRepositoryProperties = pPriceFeedRepositoryProperties;
	}
}
