package com.tru.feedprocessor.tol.base.tasklet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.email.TRUFeedEmailService;
import com.tru.feedprocessor.email.TRUFeedEnvConfiguration;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.feedprocessor.tol.base.vo.StepInfoVO;
import com.tru.feedprocessor.tol.price.TRUPriceFeedRepositoryProperties;
import com.tru.feedprocessor.tol.price.vo.TRUPriceFeedVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFeedExecutionDataCollectorTasklet is used to send email notifications and moved the files to
 * configured folders.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFeedExecutionDataCollectorTasklet extends FeedExecutionDataCollectorTasklet {

	/** The Feed email service. */
	private TRUFeedEmailService mFeedEmailService;

	/** The Fed env configuration. */
	private TRUFeedEnvConfiguration mFeedEnvConfiguration;

	/** The File sequence repository. */
	private MutableRepository mFileSequenceRepository;
	
	/** The Price feed repository properties. */
	private TRUPriceFeedRepositoryProperties mPriceFeedRepositoryProperties;

	/**
	 * This method is overridden to check the Exceptions and notify the success/failure email .
	 * 
	 * @param pStepContribution
	 *            the step contribution
	 * @param pChunkContext
	 *            the chunk context
	 * @return the repeat status
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public RepeatStatus execute(StepContribution pStepContribution, ChunkContext pChunkContext) throws Exception {
		getLogger().vlogDebug("Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: execute()");
		super.execute(pStepContribution, pChunkContext);
		boolean isException = Boolean.FALSE;
		boolean isFileInvalid = Boolean.FALSE;
		boolean isFileNotExits = Boolean.FALSE;
		String processingDir = getConfigValue(TRUPriceFeedReferenceConstants.PROCESSING_DIRECTORY).toString();
		String badDir = getConfigValue(TRUPriceFeedReferenceConstants.BAD_DIRECTORY).toString();
		String successDir = getConfigValue(TRUPriceFeedReferenceConstants.SUCCESS_DIRECTORY).toString();
		String errorDir = getConfigValue(TRUPriceFeedReferenceConstants.ERROR_DIRECTORY).toString();
		String sourceDir = getConfigValue(FeedConstants.FPT_SFTP_SOURCE_DIR).toString();
		FeedExecutionContextVO feedExecutionContextVO = getCurrentFeedExecutionContext();
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		int totalRecordCount = 0;
		int successRecordCount = 0;
		if (null != getConfigValue(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT)) {
			totalRecordCount = Integer.valueOf(getConfigValue(
					TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT).toString());
		}
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : totalRecordCount: {0}" , totalRecordCount);
		List<StepInfoVO> stepInfoVOList = feedExecutionContextVO.getStepInfos();
		if (null != stepInfoVOList && !stepInfoVOList.isEmpty()) {
			for (StepInfoVO stepInfoVO : stepInfoVOList) {
				if (null != stepInfoVO && null != stepInfoVO.getFeedFailureProcessException()) {
					isException = Boolean.TRUE;
					if (stepInfoVO.getFeedFailureProcessException().getMessage()
							.contains(FeedConstants.FILE_DOES_NOT_EXISTS)) {
						isFileNotExits = Boolean.TRUE;
						break;
					} else if (stepInfoVO.getStepName().equalsIgnoreCase(
							TRUPriceFeedReferenceConstants.PRICE_FEED_DATA_VALIDATION_STEP) &&
							stepInfoVO.getFeedFailureProcessException().getMessage()
									.contains(FeedConstants.INVALID_FILE)) {
						isFileInvalid = Boolean.TRUE;
						break;
					} 
				}
			}
		}
		if (isFileInvalid) {
			moveInvalidFileToBadDir(feedExecutionContextVO, processingDir, badDir);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_HEAD_RECORD_KEY, null);
			feedExecutionContextVO.setConfigValue(FeedConstants.FEED_FILE_TAIL_RECORD_KEY, null);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_DATE, null);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_SEQ, null);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT, null);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL, null);
			feedExecutionContextVO.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE, null);
		}
		if (isFileNotExits) {
			notifyFileDoseNotExits(sourceDir);
		}
		Map<String, FeedSkippableException> invalidRecords = null;
		Map<String, String> eliminateDuplicateInvalidRecords = null;
		if (!isException) {
			Map<String, List<FeedSkippableException>> allFeedSkippedException = feedExecutionContextVO
					.getAllFeedSkippedException();
			if (null != allFeedSkippedException) {
				invalidRecords = new HashMap<String, FeedSkippableException>();
				eliminateDuplicateInvalidRecords = new HashMap<String, String>();
				for (Entry<String, List<FeedSkippableException>> entry : allFeedSkippedException.entrySet()) {
					List<FeedSkippableException> lFeedSkippableException = entry.getValue();
					for (FeedSkippableException lSkippedException : lFeedSkippableException) {
						if (null != lSkippedException && 
								lSkippedException.getMessage().contains(
										TRUPriceFeedReferenceConstants.INVALID_RECORD) && 
										!invalidRecords.containsKey(lSkippedException.getFeedItemVO().getBusinessId())) {
							String skuId = ((TRUPriceFeedVO)lSkippedException.getFeedItemVO()).getSkuId();
							if(skuId !=null && eliminateDuplicateInvalidRecords.get(skuId) == null){
								invalidRecords.put(skuId, lSkippedException);
								eliminateDuplicateInvalidRecords.put(skuId, lSkippedException.getMessage());
							}else if(skuId != null && eliminateDuplicateInvalidRecords.get(skuId) != null && 
									!eliminateDuplicateInvalidRecords.get(skuId).equalsIgnoreCase(lSkippedException.getMessage())){
								invalidRecords.put(skuId, lSkippedException);
								eliminateDuplicateInvalidRecords.put(skuId, lSkippedException.getMessage());
							}
						}
					}
				}
				if (null != invalidRecords && !invalidRecords.isEmpty()) {
					successRecordCount = totalRecordCount - (invalidRecords.size()) / (TRUPriceFeedReferenceConstants.NUMBER_TWO);
					saveSequenceNumber(feedExecutionContextVO);
					moveInvalidRecordsToErrorDir(invalidRecords, feedExecutionContextVO.getFileName(), errorDir,
							totalRecordCount);
					moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir,
							totalRecordCount, successRecordCount, Boolean.FALSE);
					isException = Boolean.TRUE;
				}
			}
		}

		if (!isException) {
			successRecordCount = totalRecordCount - (invalidRecords.size()) / (TRUPriceFeedReferenceConstants.NUMBER_TWO);
			saveSequenceNumber(feedExecutionContextVO);
			moveSuccessFileToSuccessDir(feedExecutionContextVO.getFileName(), processingDir, successDir,
					totalRecordCount, successRecordCount, Boolean.TRUE);
		}
		
		
		if(isFileInvalid){
			logFailedMessage(startDate, TRUPriceFeedReferenceConstants.FEED_FAILED);
		}else if(isFileNotExits){
			logFailedMessage(startDate, TRUPriceFeedReferenceConstants.FEED_FAILED);
		}else {
			logSuccessMessage(startDate, TRUPriceFeedReferenceConstants.FEED_COMPLETED);
		}
		
		getLogger().vlogDebug("End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: execute()");
		return RepeatStatus.FINISHED;
	}
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logFailedMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds( TRUPriceFeedReferenceConstants.PRICE_FEED,  TRUPriceFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 */
	private void logSuccessMessage(String pStartDate, String pMessage) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess( TRUPriceFeedReferenceConstants.PRICE_FEED,  TRUPriceFeedReferenceConstants.FEED_STATUS, null, extenstions);
	}

	/**
	 * Save sequence number.
	 * 
	 * @param pFeedExecutionContextVO
	 *            the feed execution context vo
	 */
	private void saveSequenceNumber(FeedExecutionContextVO pFeedExecutionContextVO) {
		getLogger().vlogDebug("Start:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: saveSequenceNumber()");
		int seqNumber = FeedConstants.NUMBER_ZERO;
		String fileName = pFeedExecutionContextVO.getFileName();
		seqNumber = Integer.parseInt(fileName.substring(fileName.lastIndexOf(FeedConstants.UNDER_SCORE) + FeedConstants.NUMBER_ONE,
				fileName.lastIndexOf(FeedConstants.DOT)));
		if (seqNumber != FeedConstants.NUMBER_ZERO) {
			RepositoryItem repoItem;
			try {
				repoItem = getFileSequenceRepository().getItem(TRUPriceFeedReferenceConstants.PRICE_FEED,
						TRUPriceFeedReferenceConstants.FEED_FILE_SEQUENCE);
				if (repoItem != null) {
					MutableRepositoryItem mutItem = (MutableRepositoryItem) repoItem;
					mutItem.setPropertyValue(getPriceFeedRepositoryProperties().getSeqProcessedPropertyName(), seqNumber);
					getFileSequenceRepository().updateItem(mutItem);
				} else {
					MutableRepositoryItem mutItem = getFileSequenceRepository().createItem(TRUPriceFeedReferenceConstants.PRICE_FEED,
							TRUPriceFeedReferenceConstants.FEED_FILE_SEQUENCE);
					mutItem.setPropertyValue(getPriceFeedRepositoryProperties().getSeqProcessedPropertyName(), seqNumber);
					getFileSequenceRepository().addItem(mutItem);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError("Error in : @class TRUPriceFeedExecutionDataCollectorTasklet method frameSequenceNumber()", re);
				}
			}
		}
		getLogger().vlogDebug("End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: saveSequenceNumber()");
	}

	/**
	 * Gets the exception threshold percentage. Calculates the exception threshold percentage.
	 * 
	 * @param pTotalRecord
	 *            the total record
	 * @param pExceptionCount
	 *            the exception count
	 * @return the exception threshold percentage
	 */
	public double getExceptionThresholdPercentage(int pTotalRecord, int pExceptionCount) {
		BigDecimal totalCount = BigDecimal.valueOf(pTotalRecord);
		BigDecimal exceptionCount = BigDecimal.valueOf(pExceptionCount);
		BigDecimal percentage = exceptionCount.multiply(
				BigDecimal.valueOf(TRUPriceFeedReferenceConstants.NUMBER_HUNDRED)).divide(totalCount,
				FeedConstants.NUMBER_TWO, BigDecimal.ROUND_HALF_UP);
		return percentage.doubleValue();
	}

	/**
	 * Gets the exception threshold priority. Calculates the exception threshold percentage by priority.
	 * 
	 * @param pPercentage
	 *            the percentage
	 * @return the exception threshold priority
	 */
	public String getExceptionThresholdPriority(double pPercentage) {
		if (pPercentage >= getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriorityRegular() && 
				pPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriorityRegular()) {
			return getFeedEnvConfiguration().getMailPriorityRegular();
		} else if (pPercentage > getFeedEnvConfiguration().getLowerCutoffExceptionAlertPriority3() && 
				pPercentage <= getFeedEnvConfiguration().getUpperCutoffExceptionAlertPriority3()) {
			return getFeedEnvConfiguration().getFailureMailPriority3();
		} else if (pPercentage >= getFeedEnvConfiguration().getCutoffExceptionAlertPriority2()) {
			return getFeedEnvConfiguration().getFailureMailPriority2();
		}
		return FeedConstants.EMPTY;
	}

	/**
	 * Notify file dose not exits. This method used to notify the configured users if file not exists.
	 * 
	 * @param pSourceDir
	 *            the source dir
	 */
	public void notifyFileDoseNotExits(String pSourceDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUPriceFeedReferenceConstants.JOB_FAILED_MISSING_FILE;
		String priority = TRUPriceFeedReferenceConstants.SEQ_MISS;

		
		String subject = getFeedEnvConfiguration().getPriceFeedFailureSub();
		String[] args = new String[TRUPriceFeedReferenceConstants.NUMBER_TWO];
		args[TRUPriceFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUPriceFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);
		
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, getFeedEnvConfiguration()
				.getDetailedExceptionForNoFeed());
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.PRICE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: notifyFileDoseNotExits()");
	}

	
	/**
	 * Move invalid file to bad dir.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pProcessingDir the processing dir
	 * @param pBadDir the bad dir
	 */
	public void moveInvalidFileToBadDir(FeedExecutionContextVO pFeedExecutionContextVO, String pProcessingDir, String pBadDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
		String fileName = pFeedExecutionContextVO.getFileName();
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pBadDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(fileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
			}
		}
		triggerInvalidFeedEmail(pFeedExecutionContextVO, pBadDir);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveInvalidFileToBadDir()");
	}

	
	/**
	 * Trigger invalid feed email.
	 *
	 * @param pFeedExecutionContextVO the feed execution context vo
	 * @param pBadDir the bad dir
	 */
	public void triggerInvalidFeedEmail(FeedExecutionContextVO pFeedExecutionContextVO, String pBadDir) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();

		String priority = TRUPriceFeedReferenceConstants.FILE_INVALID;
		String content = null;
		String detailException = null;
		
		if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_HEAD_RECORD_KEY) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_HEADER_DET;
			detailException = TRUPriceFeedReferenceConstants.INVALID_HEADER_DET;
		}else if(pFeedExecutionContextVO.getConfigValue(FeedConstants.FEED_FILE_TAIL_RECORD_KEY) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_TRAILER_DET;
			detailException = TRUPriceFeedReferenceConstants.INVALID_TRAILER_DET;
		}else if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.INVALID_DATE) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_DATE;
			detailException = TRUPriceFeedReferenceConstants.INVALID_DATE;
		}else if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.INVALID_SEQ) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_SEQ;
			detailException = TRUPriceFeedReferenceConstants.INVALID_SEQ;
		}else if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT;
			detailException = TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT;
		}else if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL;
			detailException = TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL;
		}else if(pFeedExecutionContextVO.getConfigValue(TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE) != null){
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT+TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE;
			detailException = TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE;
		}else {
			content = TRUPriceFeedReferenceConstants.JOB_FAILED_FILE_CORRUPT;
			detailException = TRUPriceFeedReferenceConstants.INVALID_FILE;
		}

		String sub = getFeedEnvConfiguration().getPriceFeedFailureSub();
		String[] args = new String[TRUPriceFeedReferenceConstants.NUMBER_TWO];
		args[TRUPriceFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUPriceFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(sub, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, priority);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration()
				.getOSHostName());
		getFeedEmailService().sendFeedFailureEmail(FeedConstants.PRICE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerInvalidFeedEmail()");
	}

	/**
	 * Move invalid records to error dir. This method used to move the invalid records to the error directory.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pFeedFileName
	 *            the file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pTotalRecordCount
	 *            the total record count
	 */
	public void moveInvalidRecordsToErrorDir(Map<String, FeedSkippableException> pInvalidRecords, String pFeedFileName,
			String pErrorDir, int pTotalRecordCount) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
		File sourceFolder = new File(pErrorDir);
		Writer writer = null;
		BufferedWriter bw = null;
		if (!sourceFolder.exists() && !sourceFolder.isDirectory()) {
			sourceFolder.mkdirs();
		}

		File invalidRecordsFile = new File(sourceFolder, FeedConstants.ERROR + FeedConstants.UNDER_SCORE + pFeedFileName);
		try {
			writer = new FileWriter(invalidRecordsFile);
			bw = new BufferedWriter(writer);
			for (Map.Entry<String, FeedSkippableException> entry : pInvalidRecords.entrySet()) {
				bw.write(entry.getValue().getFeedItemVO().toString() + FeedConstants.IFUN + 
						entry.getValue().getMessage());
				bw.newLine();
			}
		} catch (IOException e) {
			if (isLoggingError()) {
				logError("Error in : @class TRUPriceFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()", e);
			}
		} finally {
			try {
				if (null != bw) {
					bw.close();
				}
				if (null != writer) {
					writer.close();
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError("Error in : @class TRUPriceFeedExecutionDataCollectorTasklet method moveInvalidRecordsToErrorDir()", e);
				}
			}
		}
		double percentage = getExceptionThresholdPercentage(pTotalRecordCount, pInvalidRecords.size());
		getLogger().vlogDebug("percentage:", percentage);
		String priority = getExceptionThresholdPriority(percentage);
		getLogger().vlogDebug("priority:", priority);
		triggerFeedProcessedWithErrorsEmail(pInvalidRecords, invalidRecordsFile.getName(), pErrorDir, priority,
				pTotalRecordCount, pFeedFileName);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveInvalidRecordsToErrorDir()");
	}

	/**
	 * Trigger feed processed with errors email. This method used to trigger the
	 * mail with error records.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pPriority
	 *            the priority
	 * @param pTotalRecordCount
	 *            the total record count
	 * @param pFeedFileName
	 *            the feed file name
	 */
	public void triggerFeedProcessedWithErrorsEmail(Map<String, FeedSkippableException> pInvalidRecords,
			String pErrorFileName, String pErrorDir, String pPriority, int pTotalRecordCount, String pFeedFileName) {
		getLogger()
				.vlogDebug(
						"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		File[] attachmentFile = new File[FeedConstants.NUMBER_ONE];
		attachmentFile[FeedConstants.NUMBER_ZERO] = createAttachmentFile(pInvalidRecords, pErrorFileName, pErrorDir, pFeedFileName);
		int successCount = 0;
		if (pInvalidRecords != null) {
			successCount = pTotalRecordCount - (pInvalidRecords.size()) / (TRUPriceFeedReferenceConstants.NUMBER_TWO);
		}
		String content = TRUPriceFeedReferenceConstants.FILE_PROCESSED_WITH_ERRORS;
		String subject = getFeedEnvConfiguration().getPriceFeedErrorSub();
		String[] args = new String[TRUPriceFeedReferenceConstants.NUMBER_TWO];
		args[TRUPriceFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUPriceFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);

		String detailException = getFeedEnvConfiguration().getPriceFeedErrorDetailExcep();

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_DETAIL_EXCEPTION, detailException);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_PRIORITY, pPriority);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalRecordCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_FAILED_RECORD_COUNT, (pInvalidRecords.size()/(TRUPriceFeedReferenceConstants.NUMBER_TWO)));
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, successCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_OS_HOST_NAME, getFeedEnvConfiguration().getOSHostName());
		
		getFeedEmailService().sendFeedProcessedWithErrorsEmail(FeedConstants.PRICE_JOB_NAME, templateParameters,
				attachmentFile);
		getLogger()
				.vlogDebug(
						"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedWithErrorsEmail()");
	}

	/**
	 * Creates the attachment file. This method used to create attachment file.
	 * 
	 * @param pInvalidRecords
	 *            the invalid records
	 * @param pErrorFileName
	 *            the error file name
	 * @param pErrorDir
	 *            the error dir
	 * @param pFeedFileName
	 *            the feed file name
	 * @return the file
	 */
	public File createAttachmentFile(Map<String, FeedSkippableException> pInvalidRecords, String pErrorFileName, String pErrorDir, String pFeedFileName) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		OutputStream outputStream = null;
		HSSFWorkbook workbook = null;
		
		File errorFolder = new File(pErrorDir);
		if (!errorFolder.exists() && !errorFolder.isDirectory()) {
			errorFolder.mkdirs();
		}
		File attachment = new File(errorFolder, attachementFileName(pErrorFileName));
		try { 
			outputStream = new FileOutputStream(attachment);
			workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(TRUPriceFeedReferenceConstants.XLS_SHEET);
			int rowCount = FeedConstants.NUMBER_ZERO;
			HSSFRow rowhead = sheet.createRow(rowCount);
			HSSFCellStyle rowCellStyle = workbook.createCellStyle();
			rowCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			rowCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			rowCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			rowCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			rowhead.createCell(FeedConstants.NUMBER_ZERO).setCellValue(getFeedEnvConfiguration().getFileName());
			rowhead.createCell(FeedConstants.NUMBER_ONE).setCellValue(getFeedEnvConfiguration().getRecordNumber());
			rowhead.createCell(FeedConstants.NUMBER_TWO).setCellValue(getFeedEnvConfiguration().getTimeStamp());
			rowhead.createCell(FeedConstants.NUMBER_THREE).setCellValue(getFeedEnvConfiguration().getAttachmentItemIdLabel());
			rowhead.createCell(FeedConstants.NUMBER_FOUR).setCellValue(getFeedEnvConfiguration().getAttachmentExceptionReasonLabel());

			rowhead.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_ONE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_TWO).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_THREE).setCellStyle(rowCellStyle);
			rowhead.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(rowCellStyle);
			
			rowCount = FeedConstants.NUMBER_ONE;
			HSSFCellStyle dataCellStyle = workbook.createCellStyle();
			dataCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderTop(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderRight(HSSFCellStyle.BORDER_THICK);
			dataCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THICK);

			for (Map.Entry<String, FeedSkippableException> entry : pInvalidRecords.entrySet()) {
				HSSFRow dataRow = sheet.createRow(rowCount);
				dataRow.createCell(FeedConstants.NUMBER_ZERO).setCellValue(new HSSFRichTextString(pFeedFileName));
				dataRow.createCell(FeedConstants.NUMBER_ONE).setCellValue(new HSSFRichTextString(Integer.toString(rowCount)));
				dataRow.createCell(FeedConstants.NUMBER_TWO).setCellValue(
						new HSSFRichTextString((new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT)).format(new Date())));
				dataRow.createCell(FeedConstants.NUMBER_THREE).setCellValue(
						new HSSFRichTextString(((TRUPriceFeedVO) entry.getValue().getFeedItemVO()).getSkuId()));
				dataRow.createCell(FeedConstants.NUMBER_FOUR).setCellValue(new HSSFRichTextString(entry.getValue().getMessage()));

				dataRow.getCell(FeedConstants.NUMBER_ZERO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_ONE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_TWO).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_THREE).setCellStyle(dataCellStyle);
				dataRow.getCell(FeedConstants.NUMBER_FOUR).setCellStyle(dataCellStyle);
				rowCount++;
			}
			if (null != workbook) {
				workbook.write(outputStream);
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError("Error in : @class TRUPriceFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);
			}
		} finally {
			if (null != workbook) {
				workbook = null;
			}
			try {
				if (null != outputStream) {
					outputStream.close();
				}
			} catch (IOException ioe) {
				if (isLoggingError()) {
					logError("Error in : @class TRUPriceFeedExecutionDataCollectorTasklet method createAttachmentFile()", ioe);
				}
			}

		}
		getLogger()
				.vlogDebug("End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: createAttachmentFile()");
		return attachment;
	}

	/**
	 * Attachement file name. This method used to construct the attached file name
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the string
	 */
	public String attachementFileName(String pFileName) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		String newFileName = pFileName.substring(FeedConstants.NUMBER_ZERO, pFileName.indexOf(FeedConstants.DOT) + FeedConstants.NUMBER_ONE);
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer = stringBuffer.append(newFileName).append(TRUPriceFeedReferenceConstants.XLS);
		getLogger().vlogDebug("End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: attachementFileName()");
		return stringBuffer.toString();
	}

	/**
	 * Move success file to success dir.
	 * 
	 * This method used to move the files to success directory.
	 *
	 * @param pFileName            the file name
	 * @param pProcessingDir            the processing dir
	 * @param pSuccessDir            the success dir
	 * @param pTotalCount            the total count
	 * @param pSuccessRecordCount the success record count
	 * @param pIsMail            the is mail
	 */
	public void moveSuccessFileToSuccessDir(String pFileName, String pProcessingDir, String pSuccessDir,
			int pTotalCount, int pSuccessRecordCount, boolean pIsMail) {
		getLogger().vlogDebug(
				"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
		File sourceFolder = new File(pProcessingDir);
		if (sourceFolder.exists() && sourceFolder.isDirectory() && sourceFolder.canRead()) {
			File toFolder = new File(pSuccessDir);
			if (!(toFolder.exists()) && !(toFolder.isDirectory())) {
				toFolder.mkdirs();
			}

			File[] sourceFiles = sourceFolder.listFiles();
			if (sourceFiles != null && sourceFiles.length > 0) {
				for (File file : sourceFiles) {
					if (file.isFile() && file.getName().contains(pFileName)) {
						file.renameTo(new File(toFolder, file.getName()));
					}
				}
				if (pIsMail) {
					triggerFeedProcessedSuccessEmail(pTotalCount, pSuccessRecordCount);
				}
			}
		}
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: moveSuccessFileToSuccessDir()");
	}

	/**
	 * Trigger feed processed successfully email.
	 *
	 * @param pTotalCount            the total count
	 * @param pSuccessRecordCount the success record count
	 */
	public void triggerFeedProcessedSuccessEmail(int pTotalCount, int pSuccessRecordCount) {
		getLogger()
				.vlogDebug(
						"Begin:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessEmail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		String content = TRUPriceFeedReferenceConstants.JOB_SUCCESS;
		String subject = getFeedEnvConfiguration().getPriceFeedSuccessSub();
		String[] args = new String[TRUPriceFeedReferenceConstants.NUMBER_TWO];
		args[TRUPriceFeedReferenceConstants.NUMBER_ZERO] = getFeedEnvConfiguration().getEnv();
		Date currentDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat(TRUPriceFeedReferenceConstants.SUB_DATE_FORMAT);
		args[TRUPriceFeedReferenceConstants.NUMBER_ONE] = dateFormat.format(currentDate);
		String formattedSubject = String.format(subject, args);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_SUBJECT, formattedSubject);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_MESSAGE_CONTENT, content);

		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, pTotalCount);
		templateParameters.put(TRUFeedEmailConstants.TEMPLATE_PARAMETER_SUCCESS_RECORD_COUNT, pSuccessRecordCount);
		getFeedEmailService().sendFeedSuccessEmail(FeedConstants.PRICE_JOB_NAME, templateParameters);
		getLogger().vlogDebug(
				"End:@Class: TRUPriceFeedExecutionDataCollectorTasklet : @Method: triggerFeedProcessedSuccessEmail()");
	}

	/**
	 * Gets the feed email service.
	 * 
	 * @return the feed email service
	 */
	public TRUFeedEmailService getFeedEmailService() {
		return mFeedEmailService;
	}

	/**
	 * Sets the feed email service.
	 * 
	 * @param pFeedEmailService
	 *            the new feed email service
	 */
	public void setFeedEmailService(TRUFeedEmailService pFeedEmailService) {
		mFeedEmailService = pFeedEmailService;
	}

	/**
	 * Gets the fed env configuration.
	 * 
	 * @return the fed env configuration
	 */
	public TRUFeedEnvConfiguration getFeedEnvConfiguration() {
		return mFeedEnvConfiguration;
	}

	/**
	 * Sets the fed env configuration.
	 * 
	 * @param pFeedEnvConfiguration
	 *            the new feed env configuration
	 */
	public void setFeedEnvConfiguration(TRUFeedEnvConfiguration pFeedEnvConfiguration) {
		mFeedEnvConfiguration = pFeedEnvConfiguration;
	}

	/**
	 * Gets the file sequence repository.
	 * 
	 * @return the fileSequenceRepository
	 */
	public MutableRepository getFileSequenceRepository() {
		return mFileSequenceRepository;
	}

	/**
	 * Sets the file sequence repository.
	 * 
	 * @param pFileSequenceRepository
	 *            the fileSequenceRepository to set
	 */
	public void setFileSequenceRepository(MutableRepository pFileSequenceRepository) {
		mFileSequenceRepository = pFileSequenceRepository;
	}
	
	/**
	 * Gets the price feed repository properties.
	 * 
	 * @return the price feed repository properties
	 */
	public TRUPriceFeedRepositoryProperties getPriceFeedRepositoryProperties() {
		return mPriceFeedRepositoryProperties;
	}

	/**
	 * Sets the price feed repository properties.
	 * 
	 * @param pPriceFeedRepositoryProperties
	 *            the new price feed repository properties
	 */
	public void setPriceFeedRepositoryProperties(TRUPriceFeedRepositoryProperties pPriceFeedRepositoryProperties) {
		mPriceFeedRepositoryProperties = pPriceFeedRepositoryProperties;
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
