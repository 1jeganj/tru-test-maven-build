package com.tru.feedprocessor.tol.base.tasklet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.bytecode.opencsv.CSVReader;

import com.tru.feedprocessor.email.TRUFeedEmailConstants;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.feedprocessor.tol.TRUPriceFeedReferenceConstants;
import com.tru.feedprocessor.tol.base.exception.FeedSkippableException;
import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;
import com.tru.logging.TRUAlertLogger;

/**
 * The Class TRUPriceFeedValidateFileTask is used for validating the input feed file.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRUPriceFeedValidateFileTask extends ValidateFileTask {

	/**
	 * we need to override doExecute() method and do the file validation. this method will validate whether the first
	 * record is having key name HEADER or not and last record is having key word TRAILER or not, total number of
	 * records is equal to the number mentioned in the last record, sum of the sale price is equal to the price amount
	 * mentioned in the last record. If the file validation fails then it will return false, if validation pass it will
	 * return true.
	 * 
	 * @param pFile
	 *            the file
	 * @return true, if successful
	 */
	@Override
	public boolean validateFile(File pFile) {
		getLogger().vlogDebug("Begin:@Class: PriceFeedValidateFileTask : @Method: validateFile()");
		boolean isValidFile = Boolean.FALSE;
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		CSVReader csvReader = null;
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		try {
			fileReader = new FileReader(pFile);
			bufferedReader = new BufferedReader(fileReader);
			csvReader = new CSVReader(bufferedReader, TRUPriceFeedReferenceConstants.PIPE_DELIMITER);
			List<String[]> allPriceFeedRecords = csvReader.readAll();
			if (null != allPriceFeedRecords && !allPriceFeedRecords.isEmpty()) {
				int recordsSize = allPriceFeedRecords.size(); 
				getLogger().vlogDebug("records size:: {0}" , recordsSize);
				
				if(!TRUPriceFeedReferenceConstants.FEED_FILE_HEAD_RECORD_KEY.equals(allPriceFeedRecords.get(FeedConstants.NUMBER_ZERO)[FeedConstants.NUMBER_ZERO])){
					curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.FEED_FILE_HEAD_RECORD_KEY, TRUPriceFeedReferenceConstants.FEED_FILE_HEAD_RECORD_KEY);
					return isValidFile;
				}else if(!FeedConstants.FEED_FILE_TAIL_RECORD_KEY.equals(allPriceFeedRecords.get(recordsSize - FeedConstants.NUMBER_ONE)[FeedConstants.NUMBER_ZERO])){
					curExecCntx.setConfigValue(FeedConstants.FEED_FILE_TAIL_RECORD_KEY, FeedConstants.FEED_FILE_TAIL_RECORD_KEY);
					return isValidFile;
				}else if(!isPatternMatched(allPriceFeedRecords.get(FeedConstants.NUMBER_ZERO)[FeedConstants.NUMBER_ONE], TRUPriceFeedReferenceConstants.DATE_FORMAT)){
					curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_DATE, TRUPriceFeedReferenceConstants.INVALID_DATE);
					return isValidFile;
				}else if(!isPatternMatched(allPriceFeedRecords.get(FeedConstants.NUMBER_ZERO)[FeedConstants.NUMBER_TWO], TRUPriceFeedReferenceConstants.STRING_VAL_WITH_NUMBER_6_DIGIT_PATTERN)){
					curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_SEQ, TRUPriceFeedReferenceConstants.INVALID_SEQ);
					return isValidFile;
				}else {
					double salePriceTotal = TRUPriceFeedReferenceConstants.DOUBLE_VAL;
					for (int i = FeedConstants.NUMBER_ONE; i < recordsSize - FeedConstants.NUMBER_ONE; i++) {
						String[] record = allPriceFeedRecords.get(i);
						salePriceTotal += Double.parseDouble(record[FeedConstants.NUMBER_TWO]);
					}
					getLogger().vlogDebug("sale price total:: {0}" ,salePriceTotal);
					DecimalFormat decimalFormat = new DecimalFormat(
							TRUPriceFeedReferenceConstants.DECIMAL_FORMAT_UP_TO_2_DIGIT);
					String salePriceTotalDecimalFormat = decimalFormat.format(salePriceTotal);
					getLogger().vlogDebug("sale price total after decimal format::{0}" , salePriceTotalDecimalFormat);
					double salePriceInTailRecord = (Double.parseDouble(allPriceFeedRecords.get(recordsSize - 
							FeedConstants.NUMBER_ONE)[FeedConstants.NUMBER_TWO])) / 
							TRUPriceFeedReferenceConstants.NUMBER_HUNDRED;
					getLogger().vlogDebug("sale price total mentioned in feed file::{0}" , salePriceInTailRecord);
					double recordSizeInTailRecord = Double.parseDouble(allPriceFeedRecords.get(recordsSize - 
							FeedConstants.NUMBER_ONE)[FeedConstants.NUMBER_ONE]);
					getLogger().vlogDebug("total records count mentioned in feed file::{0}" , recordSizeInTailRecord);
					if (recordSizeInTailRecord == recordsSize && Double.parseDouble(salePriceTotalDecimalFormat) == salePriceInTailRecord) {
						isValidFile = Boolean.TRUE; 
						int priceRecordsSize = recordsSize - TRUPriceFeedReferenceConstants.NUMBER_TWO; 
						setConfigValue(TRUFeedEmailConstants.TEMPLATE_PARAMETER_TOTAL_RECORD_COUNT, priceRecordsSize);
					}else if(recordSizeInTailRecord != recordsSize){
						curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT, TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_COUNT);
						return isValidFile;
					}else if(Double.parseDouble(salePriceTotalDecimalFormat) != salePriceInTailRecord){
						curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL, TRUPriceFeedReferenceConstants.INVALID_TRAILER_RECORD_TOTAL);
						return isValidFile;
					}
				}
				
			}
			getLogger().vlogDebug("is valid file::" + isValidFile);
		} catch (IOException ioe) {
			if(isLoggingError()){
				logError("@Class: PriceFeedValidateFileTask : @Method: validateFile(): ", ioe);
			}
		} catch (NumberFormatException nfe) {
			curExecCntx.setConfigValue(TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE, TRUPriceFeedReferenceConstants.INVALID_SALE_PRICE);
			if(isLoggingError()){
				logError("@Class: PriceFeedValidateFileTask : @Method: validateFile(): ", nfe);
			}
		} finally {
			try {
				if (csvReader != null) {
					csvReader.close();
					csvReader = null;
				}
				if (bufferedReader != null) {
					bufferedReader.close();
					bufferedReader = null;
				}
				if (fileReader != null) {
					fileReader.close();
					fileReader = null;
				}
			} catch (IOException ioe) {
				if(isLoggingError()){
					logError("@Class: PriceFeedValidateFileTask : @Method: validateFile(): ", ioe);
				}
			}
		}

		getLogger().vlogDebug("End:@Class: PriceFeedValidateFileTask : @Method: validateFile()");
		return isValidFile;
	}
	
	/**
	 * Checks if is match pattern.
	 * 
	 * @param pVal
	 *            the val
	 * @param pPattern
	 *            the pattern
	 * @return true, if is match pattern
	 */
	public boolean isPatternMatched(String pVal, String pPattern) {
		Pattern pattern = Pattern.compile(pPattern);
		Matcher matcher = pattern.matcher(pVal);
		if (matcher.matches()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/* (non-Javadoc)
	 * @see com.tru.feedprocessor.tol.base.tasklet.ValidateFileTask#doExecute()
	 */
	@Override
	protected void doExecute() throws FeedSkippableException {
		// TODO Auto-generated method stub
		FeedExecutionContextVO curExecCntx = getCurrentFeedExecutionContext();
		File file = new File(curExecCntx.getFilePath()+curExecCntx.getFileName());
		String startDate = new SimpleDateFormat(TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		boolean flag = validateFile (file);
		
		if(!flag){
			logFailedMessage(startDate, FeedConstants.INVALID_FILE, file.getName());
			throw new FeedSkippableException(getPhaseName(), getStepName(),
					FeedConstants.INVALID_FILE,  null,null);
		}
		
		logSuccessMessage(startDate, TRUPriceFeedReferenceConstants.FILE_VALID , file.getName());
	}
	
	
	/**
	 * Log failed message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logFailedMessage(String pStartDate, String pMessage, String pFileName) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedFaileds( TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FILE_VALIDATION, pFileName, extenstions);
	}
	
	
	/**
	 * Log success message.
	 *
	 * @param pStartDate the start date
	 * @param pMessage the message
	 * @param pFileName the file name
	 */
	private void logSuccessMessage(String pStartDate, String pMessage, String pFileName) {
		String endDate = new SimpleDateFormat( TRUPriceFeedReferenceConstants.FEED_DATE_FORMAT).format(new Date());
		Map<String, String> extenstions = new ConcurrentHashMap<String, String>();
		extenstions.put( TRUPriceFeedReferenceConstants.MESSAGE, pMessage);
		extenstions.put( TRUPriceFeedReferenceConstants.START_TIME, pStartDate);
		extenstions.put( TRUPriceFeedReferenceConstants.END_TIME, endDate);
		getAlertLogger().logFeedSuccess( TRUPriceFeedReferenceConstants.PRICE_FEED, TRUPriceFeedReferenceConstants.FILE_VALIDATION, pFileName, extenstions);
	}
	
	/** The Alert Logger. */
	private TRUAlertLogger mAlertLogger;
	

	/**
	 * Gets the alert logger.
	 *
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * Sets the alert logger.
	 *
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}
}
