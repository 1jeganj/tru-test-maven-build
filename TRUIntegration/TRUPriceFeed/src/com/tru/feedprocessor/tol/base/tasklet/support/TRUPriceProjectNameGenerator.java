package com.tru.feedprocessor.tol.base.tasklet.support;

import com.tru.feedprocessor.tol.base.vo.FeedExecutionContextVO;

/**
 * The Class ProjectNameGenerator. This class is used to generate the name which
 * will be used during project creation in BCC Project team can over ride the.
 * getProjectName() method to give the project specific BCC's project name
 *
 * @author PA
 * @version 1.1
 * 
 */
public class TRUPriceProjectNameGenerator implements IProjectNameGenerator {

	/**
	 * @param pFeedExecCntx	-FeedExecutionContextVO
	 * @return the file name.
	 */
	@Override
	public String getProjectName(FeedExecutionContextVO pFeedExecCntx) {
		
		return pFeedExecCntx.getFileName();
		
	}

}
