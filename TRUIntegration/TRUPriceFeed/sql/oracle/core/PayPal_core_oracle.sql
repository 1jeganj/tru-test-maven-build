CREATE TABLE parade_paypal_pament_group
(
	payer_id VARCHAR2(40),
	payPal_token VARCHAR2(40),
	payPal_corelation_id VARCHAR2(40),
	payPal_transaction_id VARCHAR2(50),
	payment_group_id VARCHAR2(40) NOT NULL,
	CONSTRAINT PAYPAL_PG_PK PRIMARY KEY (payment_group_id)
) ;

CREATE TABLE parade_billing_address
(
	payment_group_id VARCHAR2(40) NOT NULL,
    prefix VARCHAR2(40),
	first_name VARCHAR2(40),
	middle_name VARCHAR2(10),
	last_name VARCHAR2(40),
	suffix VARCHAR2(40),
	address_1 VARCHAR2(40),
	address_2 VARCHAR2(40),
	address_3 VARCHAR2(40),
	city VARCHAR2(40),
	state VARCHAR2(40),
    county VARCHAR2(40),
	postal_code VARCHAR2(40),
	country VARCHAR2(40),
	phone_number VARCHAR2(40),
	email VARCHAR2(40),
	CONSTRAINT PAYPAL_BILLING_ADD_PK PRIMARY KEY (payment_group_id)
) ;