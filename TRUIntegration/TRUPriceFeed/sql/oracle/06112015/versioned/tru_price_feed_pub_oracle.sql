drop table tru_custom_price;

CREATE TABLE tru_custom_price (
	price_id 		varchar2(254)	NOT NULL,
	country_code 		varchar2(3)	NOT NULL,
	market_code 		varchar2(3)	NOT NULL,
	store_number 		number(5)	NOT NULL,
	asset_version number(19) not null enable,
	PRIMARY KEY(price_id,asset_version)
);