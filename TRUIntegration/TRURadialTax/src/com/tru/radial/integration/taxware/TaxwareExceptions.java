/**
 * TaxwareExceptions
 */
package com.tru.radial.integration.taxware;

/**
 * This custom class is used to handle the exception.
 * @author Professional Access
 * @version 1.0
 */
public class TaxwareExceptions extends Exception {
 
	/**
	 * generated serialVersionUID
	 */
	private static final long serialVersionUID = 8622112186793878607L;

	/**
	 * This constructor is used to get the exception message details.
	 * @param pStrMessage the message
	 */
  public TaxwareExceptions(String pStrMessage) {
	 super(pStrMessage);
	}
}

