/**
 * 
 */
package com.tru.radial.integration.taxware;


/**
 * This class used to create the hold the all constant for taxware integration
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUTaxwareConstant {

	public static final String CALC_TAX = "CalcTax";
	public static final String CALCULATE_TAX = "CalculateTax";
	public static final String CAL_TAX = "CalTax";
	public static final CharSequence GENERAL_DATA_ERROR = null;
	public static final String TRU_TAXWARE_GENERAL_DATA_ERROR = "tru_taxware_generalData_Error";
	public static final double DOUBLE_ZERO = 0.00;
	public static final String ORDER = "ORDER";
	public static final String CART = "cart";
	public static final String ALL = "ALL";
	public static final int NUMERIC_ZERO = 0;
	public static final int NUMERIC_ONE = 1;
	public static final int NUMERIC_THREE = 3;
	public static final int NUMERIC_FOUR = 4;
	public static final int NUMERIC_SEVEN = 7;
	public static final long MINUS_ONE = -1;
	public static final double DECIMAL_ZERO = 0.0;
	public static final long NUMBER_ONE = 1;
	
	public static final String RADIAL_TAXWARE_STUB_PACKAGE = "com.tru.radial.taxquote";
	public static final String BILLING_MAILINGADDRESS_ID = "bill_dest1";
	public static final String SUPPRESS_WARNING_UNCHECKED = "unchecked";
	public static final String SUPPRESS_WARNING_RAWTYPES = "rawtypes";
	public static final String SHIPGROUPTYPE2_ID = "SG_";
	public static final String COUNTRYCODE_US = "US";
	public static final String FEETYPE_EWASTE = "eWaste_Fee";
	public static final String RECYCLING_FEE = "RECYCLING_FEE";
	public static final String GIFTPACKAGING2_ITEMID = "_GW";
	
	public static final int NUMERIC_200 = 200;
	public static final int NUMERIC_400 = 400;
	public static final int NUMERIC_500 = 500;
	public static final String EMPTY_STRING = "";
	
	public static final String REQUESTMETHOD_POST = "POST";
	public static final String REQUESTPROPERTY_APIKEY = "apiKey";
	public static final String REQUESTPROPERTY_CONTENT_TYPE = "Content-Type";
	public static final String REQUESTPROPERTY_CONTENT_TYPE_VALUE = "application/json;charset=utf-8";
	public static final String JAXB_FORMATTED_OUTPUT = "jaxb.formatted.output";
	public static final String TERRITORY= "TERRITORY";
	public static final String PUERTO_RICO = "PUERTO RICO";
	public static final String UNDERSCORE = "_";
	public static final String MERCHTAX = "MERCHTAX";
	public static final String MERCHDISCOUNTTAX = "MERCHDISCOUNTTAX";
	public static final String SHIPPINGTAX = "SHIPPINGTAX";
	public static final String SHIPPINGDISCOUNTTAX = "SHIPPINGDISCOUNTTAX";
	public static final String GWTAX = "GWTAX";
	public static final String GWDISCOUNTTAX = "GWDISCOUNTTAX";
	public static final String COUNTY = "COUNTY";
	public static final String TAX_CLASS = "TAXCLASS";
	public static final String TAX_TRANSACTION_ID = "TaxTransactionId";
	public static final String TAX_QUOTE = "taxQuote";
}
