package com.tru.radial.integration.taxware;

import java.util.Date;
import java.util.Map;

import atg.payment.tax.TaxStatus;
/**
 * This custom class implements the OTB TaxStatus interface for the TRU req.
 * @author PA
 * @version 1.0
 * 
 */
public class RadialTaxWareStatus implements TaxStatus {
	/**
	 * Holds serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * hold to property transactionId.
	 */
	private String mTransactionId;
	/**
	 * hold to property transactionSuccess.
	 */
	private boolean mTransactionSuccess;
	/**
	 * hold to property errorMessage.
	 */
	private String mErrorMessage;
	/**
	 * hold to property transactionTimestamp.
	 */
	private Date mTransactionTimestamp;
	/**
	 * hold to property amount.
	 */
	private double mAmount;
	
	private int mConnectionStatus;
	
	private long mServiceTransactionTime;
	
	/**
	 * Property to hold radialTaxPriceInfos.
	 */
	private Map<String, String> mRadialTaxPriceInfos;
	
    /**
	 * @return the radialTaxPriceInfos
	 */
	public Map<String, String> getRadialTaxPriceInfos() {
		return mRadialTaxPriceInfos;
	}
	/**
	 * @param pRadialTaxPriceInfos the radialTaxPriceInfos to set
	 */
	public void setRadialTaxPriceInfos(Map<String, String> pRadialTaxPriceInfos) {
		mRadialTaxPriceInfos = pRadialTaxPriceInfos;
	}
	
	/**
	 * @return the transactionId
	 */
    public String getTransactionId() {
        return mTransactionId;
    }
    /**
	 * @param pTransactionId the transactionId to set
	 */
    public void setTransactionId(String pTransactionId) {
        mTransactionId = pTransactionId;
    }    
    /**
	 * @return the transactionSuccess
	 */
    public boolean getTransactionSuccess() {
        return mTransactionSuccess;
    }
    /**
	 * @param pTransactionSuccess the transactionSuccess to set
	 */
    public void setTransactionSuccess(boolean pTransactionSuccess) {
        mTransactionSuccess = pTransactionSuccess;
    }   
    /**
	 * @return the errorMessage
	 */
    public String getErrorMessage() {
        return mErrorMessage;
    }
    /**
	 * @param pErrorMessage the errorMessage to set
	 */
    public void setErrorMessage(String pErrorMessage) {
        mErrorMessage = pErrorMessage;
    }    
    /**
	 * @return the transactionTimestamp
	 */
    public Date getTransactionTimestamp() {
        return mTransactionTimestamp;
    }
    /**
	 * @param pTransactionTimestamp the transactionTimestamp to set
	 */
    public void setTransactionTimestamp(Date pTransactionTimestamp) {
        mTransactionTimestamp = pTransactionTimestamp;
    }
    /**
	 * @return the amount
	 */
    public double getAmount() {
        return mAmount;
    }
    /**
	 * @param pAmount the amount to set
	 */
    public void setAmount(double pAmount) {
        mAmount = pAmount;
    }        
   
	@Override
	public double getCityTax() {
		return 0;
	}
	
	/**
	 * Gets the country tax.
	 *
	 * @return the country tax
	 */
	@Override
	public double getCountryTax() {
		return 0;
	}
	
	/**
	 * Gets the county tax.
	 *
	 * @return the county tax
	 */
	@Override
	public double getCountyTax() {
		return 0;
	}
	
	/**
	 * Gets the district tax.
	 *
	 * @return the district tax
	 */
	@Override
	public double getDistrictTax() {
		return 0;
	}
	
	/**
	 * Gets the state tax.
	 *
	 * @return the state tax
	 */
	@Override
	public double getStateTax() {
		return 0;
	}
	
	/** The Radial tax fault details. */
	private String  mRadialTaxFaultDetails;

	/**
	 * @return the radialTaxFaultDetails
	 */
	public String getRadialTaxFaultDetails() {
		return mRadialTaxFaultDetails;
	}

	/**
	 * @param pRadialTaxFaultDetails the radialTaxFaultDetails to set
	 */
	public void setRadialTaxFaultDetails(String pRadialTaxFaultDetails) {
		mRadialTaxFaultDetails = pRadialTaxFaultDetails;
	}
	
	/** The Estimated sales tax. */
	private double mEstimatedSalesTax;
	
	/** The Estimated local tax. */
	private double mEstimatedLocalTax;
	
	/** The Estimated island tax. */
	private double mEstimatedIslandTax;
	
	/**
	 * @return the estimatedSalesTax
	 */
	public double getEstimatedSalesTax() {
		return mEstimatedSalesTax;
	}
	/**
	 * @param pEstimatedSalesTax the estimatedSalesTax to set
	 */
	public void setEstimatedSalesTax(double pEstimatedSalesTax) {
		mEstimatedSalesTax = pEstimatedSalesTax;
	}
	/**
	 * @return the estimatedLocalTax
	 */
	public double getEstimatedLocalTax() {
		return mEstimatedLocalTax;
	}
	/**
	 * @param pEstimatedLocalTax the estimatedLocalTax to set
	 */
	public void setEstimatedLocalTax(double pEstimatedLocalTax) {
		mEstimatedLocalTax = pEstimatedLocalTax;
	}
	/**
	 * @return the estimatedIslandTax
	 */
	public double getEstimatedIslandTax() {
		return mEstimatedIslandTax;
	}
	/**
	 * @param pEstimatedIslandTax the estimatedIslandTax to set
	 */
	public void setEstimatedIslandTax(double pEstimatedIslandTax) {
		mEstimatedIslandTax = pEstimatedIslandTax;
	}
	
	/**
	 * Gets the connection status.
	 *
	 * @return the connection status
	 */
	public int getConnectionStatus() {
		return mConnectionStatus;
	}
	
	/**
	 * Sets the connection status.
	 *
	 * @param pConnectionStatus the new connection status
	 */
	public void setConnectionStatus(int pConnectionStatus) {
		mConnectionStatus = pConnectionStatus;
	}
	
	/**
	 * Gets the service transaction time.
	 *
	 * @return the service transaction time
	 */
	public long getServiceTransactionTime() {
		return mServiceTransactionTime;
	}
	
	/**
	 * Sets the service transaction time.
	 *
	 * @param pServiceTransactionTime the new service transaction time
	 */
	public void setServiceTransactionTime(long pServiceTransactionTime) {
		mServiceTransactionTime = pServiceTransactionTime;
	}
	
}
