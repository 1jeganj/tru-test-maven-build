/**
 * 
 */
package com.tru.radial.integration.taxware;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.service.util.CurrentDate;

import com.tru.radial.taxquote.DiscountResponse;
import com.tru.radial.taxquote.FaultResponseType;
import com.tru.radial.taxquote.FeeResponse;
import com.tru.radial.taxquote.MerchandisePriceGroupResponse;
import com.tru.radial.taxquote.OrderItemListResponse;
import com.tru.radial.taxquote.OrderItemResponse;
import com.tru.radial.taxquote.PromotionalDiscountsResponse;
import com.tru.radial.taxquote.Tax;
import com.tru.radial.taxquote.TaxData;
import com.tru.radial.taxquote.TaxDutyQuoteResponse;
import com.tru.radial.taxquote.TaxGroup;
import com.tru.radial.taxquote.TdfShipGroupResponseType;

/**
 * This helper class is written to parse the request/response which is going/coming from geo code and sale tax service.
 * @author Professional Access
 */
public class TRURadialTaxwareHelper extends GenericService {
	
	/** The Context. */
	private JAXBContext mContext =  null;

	/**
	 * Gets the JAXB context instance.
	 *
	 * @return the JAXB context instance
	 * @throws JAXBException  
	 */
	public void getJAXBContextInstance() throws JAXBException {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRURadialTaxwareHelper.getJAXBContextInstance()");
		}
		mContext =   JAXBContext.newInstance(TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
		if (isLoggingDebug()) {
			vlogDebug("EXIT::::TRURadialTaxwareHelper().getJAXBContextInstance");
		}
	}   
	
	/**
	 * Construct tax ware status.
	 *
	 * @param pTaxDutyQuoteResponse the tax duty quote response
	 * @return the radial tax ware status
	 */
	public RadialTaxWareStatus constructTaxWareStatus(TaxDutyQuoteResponse pTaxDutyQuoteResponse) {
		if(isLoggingDebug()) {
			vlogDebug("TRURadialTaxwareHelper.constructTaxWareStatus() : START");
		}
		RadialTaxWareStatus taxWareStatus = new RadialTaxWareStatus();
		List<Element> any = pTaxDutyQuoteResponse.getAny();
		for (int i = 0; i < any.size(); i++) {
			Element e = any.get(i);
			String nodeName = e.getNodeName();
			if(StringUtils.isNotBlank(nodeName) && nodeName.equalsIgnoreCase(TRUTaxwareConstant.TAX_TRANSACTION_ID)) {
				Node child = e.getFirstChild();
				if (child != null && child instanceof CharacterData) {
					CharacterData cd = (CharacterData) child;
					taxWareStatus.setTransactionId(cd.getData());
				}
			}
		}
		List<TdfShipGroupResponseType> shipGroup = pTaxDutyQuoteResponse.getShipping().getShipGroups().getShipGroup();
		for (TdfShipGroupResponseType tdfShipGroupResponseType : shipGroup) {
			OrderItemListResponse items = tdfShipGroupResponseType.getItems();
			List<OrderItemResponse> orderItem = items.getOrderItem();
			for (OrderItemResponse orderItemResponse : orderItem) {
				updateTaxWareStatusWithMerchandisePrice(taxWareStatus, tdfShipGroupResponseType, orderItemResponse);

				if(null != orderItemResponse.getPricing().getShipping()) {
					updateTaxWareStatusWithShippingPrice(taxWareStatus, tdfShipGroupResponseType, orderItemResponse);
				}

				if(null != orderItemResponse.getGifting()) {
					updateTaxWareStatusWithGiftingPrice(taxWareStatus, tdfShipGroupResponseType, orderItemResponse);
				}

				if(null != orderItemResponse.getPricing().getFees()) {
					updateTaxWareStatusWithAdditionalFee(taxWareStatus, tdfShipGroupResponseType, orderItemResponse);
				}
			}
		}
		taxWareStatus.setTransactionSuccess(Boolean.TRUE);
		taxWareStatus.setTransactionTimestamp(getCurrentDate().getTimeAsTimestamp());
		if(isLoggingDebug()) {
			vlogDebug("totalTaxAmout--------> {0} ", taxWareStatus.getAmount());
			vlogDebug("estimatedSalesTax--------> {0}", taxWareStatus.getEstimatedSalesTax());
			vlogDebug("estimatedIslandTax--------> {0}", taxWareStatus.getEstimatedIslandTax());
			vlogDebug("estimatedLocalTax--------> {0}", taxWareStatus.getEstimatedLocalTax());
		}
		return taxWareStatus;
	}
	
	/**
	 * Update tax ware status with additional fee.
	 *
	 * @param pTaxWareStatus the tax ware status
	 * @param pTdfShipGroupResponseType the tdf ship group response type
	 * @param pOrderItemResponse the order item response
	 */
	public void updateTaxWareStatusWithAdditionalFee(RadialTaxWareStatus pTaxWareStatus, TdfShipGroupResponseType pTdfShipGroupResponseType,
			OrderItemResponse pOrderItemResponse) {
		String generateXML = null;
		Map<String, String> radialTaxPriceInfoMap = pTaxWareStatus.getRadialTaxPriceInfos();
		if(radialTaxPriceInfoMap == null) {
			radialTaxPriceInfoMap = new HashMap<String, String>();
		}
		
		if(null != pOrderItemResponse.getPricing().getFees()) {
			List<FeeResponse> feeTaxList = pOrderItemResponse.getPricing().getFees().getFee();
			for (FeeResponse feeResponse : feeTaxList ) {
				TaxData taxData = feeResponse.getCharge().getTaxData();
				TaxGroup taxGroup = taxData.getTaxes();
				String taxClass = taxData.getTaxClass();
				radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+feeResponse.getFeeId()
						+TRUTaxwareConstant.UNDERSCORE+feeResponse.getFeeType()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.TAX_CLASS, taxClass);
				taxData.setTaxClass(null);
				List<Tax> listOfFeeTax = taxGroup.getTax();
				boolean eligibleForPRTax = isEligibleForPRTax(listOfFeeTax);
				for (Tax feeTax : listOfFeeTax) {
					updateTaxWareStatusWithRadialTax(pTaxWareStatus, feeTax, eligibleForPRTax);
				}
				generateXML = generateXML(taxData, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
				radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
						pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+feeResponse.getFeeId()+TRUTaxwareConstant.UNDERSCORE+feeResponse.getFeeType(), generateXML);
			}
		}
		pTaxWareStatus.setRadialTaxPriceInfos(radialTaxPriceInfoMap);
	}
	
	/**
	 * Update tax ware status with gifting price.
	 *
	 * @param pTaxWareStatus the tax ware status
	 * @param pTdfShipGroupResponseType the tdf ship group response type
	 * @param pOrderItemResponse the order item response
	 */
	public void updateTaxWareStatusWithGiftingPrice(RadialTaxWareStatus pTaxWareStatus, TdfShipGroupResponseType pTdfShipGroupResponseType,
			OrderItemResponse pOrderItemResponse) {
		Map<String, String> radialTaxPriceInfoMap = pTaxWareStatus.getRadialTaxPriceInfos();

		if(radialTaxPriceInfoMap == null) {
			radialTaxPriceInfoMap = new HashMap<String, String>();
		}

		String generateXML = null;
		
		if(null != pOrderItemResponse.getGifting()) {
			TaxData taxData = pOrderItemResponse.getGifting().getPricing().getTaxData();
			TaxGroup taxGroup = taxData.getTaxes();
			String taxClass = taxData.getTaxClass();
			radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
					pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.GWTAX+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.TAX_CLASS, taxClass);
			taxData.setTaxClass(null);
			List<Tax> listOfGiftingTax = taxGroup.getTax();
			boolean eligibleForPRTax = isEligibleForPRTax(listOfGiftingTax);
			for (Tax giftingTax : listOfGiftingTax) {
				updateTaxWareStatusWithRadialTax(pTaxWareStatus, giftingTax, eligibleForPRTax);
			}
			generateXML = generateXML(taxData, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
			radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
					pOrderItemResponse.getItemDesc()+
					TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.GWTAX, generateXML);
			if(null != pOrderItemResponse.getGifting().getPricing().getPromotionalDiscounts()) {
				PromotionalDiscountsResponse promotionalDiscounts2 = pOrderItemResponse.getGifting().getPricing().getPromotionalDiscounts();
				List<DiscountResponse> discount2 = promotionalDiscounts2.getDiscount();
				for (DiscountResponse discountResponse :discount2 ) {
					TaxGroup taxes = discountResponse.getTaxes();
					List<Tax> listOfGiftingDiscountTax = taxes.getTax();
					boolean eligibleForPRDiscountTax = isEligibleForPRTax(listOfGiftingDiscountTax);
					for (Tax giftingDiscountTax : listOfGiftingDiscountTax) {
						updateTaxWareStatusWithRadialTax(pTaxWareStatus, giftingDiscountTax, eligibleForPRDiscountTax);
					}
					generateXML = generateXML(taxes, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
					radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
							pOrderItemResponse.getItemDesc()+
							TRUTaxwareConstant.UNDERSCORE+discountResponse.getId()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.GWDISCOUNTTAX, generateXML);
				}
			}
		}
		pTaxWareStatus.setRadialTaxPriceInfos(radialTaxPriceInfoMap);
	}
	
	/**
	 * Update tax ware status with shipping price.
	 *
	 * @param pTaxWareStatus the tax ware status
	 * @param pTdfShipGroupResponseType the tdf ship group response type
	 * @param pOrderItemResponse the order item response
	 */
	public void updateTaxWareStatusWithShippingPrice(RadialTaxWareStatus pTaxWareStatus, TdfShipGroupResponseType pTdfShipGroupResponseType,
			OrderItemResponse pOrderItemResponse) {
		Map<String, String> radialTaxPriceInfoMap = pTaxWareStatus.getRadialTaxPriceInfos();

		if(radialTaxPriceInfoMap == null) {
			radialTaxPriceInfoMap = new HashMap<String, String>();
		}

		String generateXML = null;
		
		if(null != pOrderItemResponse.getPricing().getShipping()) {
			TaxData taxData = pOrderItemResponse.getPricing().getShipping().getTaxData();
			TaxGroup taxGroup = taxData.getTaxes();
			List<Tax> listOfShippingTax = taxGroup.getTax();
			boolean eligibleForPRTax = isEligibleForPRTax(listOfShippingTax);
			for (Tax shippingTax : listOfShippingTax) {
				updateTaxWareStatusWithRadialTax(pTaxWareStatus, shippingTax, eligibleForPRTax);
			}
			generateXML = generateXML(taxData, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
			radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+pOrderItemResponse.getItemDesc()+
					TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.SHIPPINGTAX, generateXML);
			if(null != pOrderItemResponse.getPricing().getShipping().getPromotionalDiscounts()) {
				PromotionalDiscountsResponse promotionalDiscounts1 = pOrderItemResponse.getPricing().getShipping().getPromotionalDiscounts();
				List<DiscountResponse> discount1 = promotionalDiscounts1.getDiscount();
				for (DiscountResponse discountResponse : discount1) {
					TaxGroup taxes = discountResponse.getTaxes();
					List<Tax> listOfShippingDiscountTax = taxes.getTax();
					boolean eligibleForPRDiscountTax = isEligibleForPRTax(listOfShippingDiscountTax);
					for (Tax shippingDiscountTax : listOfShippingDiscountTax) {
						updateTaxWareStatusWithRadialTax(pTaxWareStatus, shippingDiscountTax, eligibleForPRDiscountTax);
					}
					generateXML = generateXML(taxes, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
					radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
							pOrderItemResponse.getItemDesc()+
							TRUTaxwareConstant.UNDERSCORE+discountResponse.getId()+TRUTaxwareConstant.UNDERSCORE+
							TRUTaxwareConstant.SHIPPINGDISCOUNTTAX, generateXML);
				}
			}
		}
		pTaxWareStatus.setRadialTaxPriceInfos(radialTaxPriceInfoMap);
	}
	
	
	/**
	 * Update tax ware status with merchandise price.
	 *
	 * @param pTaxWareStatus the tax ware status
	 * @param pTdfShipGroupResponseType the tdf ship group response type
	 * @param pOrderItemResponse the order item response
	 */
	public void updateTaxWareStatusWithMerchandisePrice(RadialTaxWareStatus pTaxWareStatus, TdfShipGroupResponseType pTdfShipGroupResponseType,
			OrderItemResponse pOrderItemResponse) {
		Map<String, String> radialTaxPriceInfoMap = pTaxWareStatus.getRadialTaxPriceInfos();

		if(radialTaxPriceInfoMap == null) {
			radialTaxPriceInfoMap = new HashMap<String, String>();
		}

		String generateXML = null;
		MerchandisePriceGroupResponse merchandise = pOrderItemResponse.getPricing().getMerchandise();
		TaxData taxData = merchandise.getTaxData();
		TaxGroup taxGroup = taxData.getTaxes();
		String taxClass = taxData.getTaxClass();
		radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
				pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.MERCHTAX+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.TAX_CLASS, taxClass);
		taxData.setTaxClass(null);
		List<Tax> listOfMerchandiseTax = taxGroup.getTax();
		boolean eligibleForPRTax = isEligibleForPRTax(listOfMerchandiseTax);
		for (Tax merchandiseTax : listOfMerchandiseTax) {
			updateTaxWareStatusWithRadialTax(pTaxWareStatus, merchandiseTax, eligibleForPRTax);
		}
		generateXML = generateXML(taxData, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
		radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
				pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.MERCHTAX, generateXML);
		com.tru.radial.taxquote.ObjectFactory objectFactory = new com.tru.radial.taxquote.ObjectFactory();
		if(null != merchandise.getPromotionalDiscounts()) {
			PromotionalDiscountsResponse promotionalDiscounts = merchandise.getPromotionalDiscounts();
			List<DiscountResponse> discount = promotionalDiscounts.getDiscount();
			for (DiscountResponse discountResponse : discount) {
				TaxGroup taxes = discountResponse.getTaxes();
				TaxData taxData2 = objectFactory.createTaxData();
				taxData2.setTaxes(taxes);
				List<Tax> listOfMerchandiseDiscountTax = taxes.getTax();
				boolean eligibleForPRDiscountTax = isEligibleForPRTax(listOfMerchandiseDiscountTax);
				for (Tax merchandiseDiscountTax : listOfMerchandiseDiscountTax) {
					updateTaxWareStatusWithRadialTax(pTaxWareStatus, merchandiseDiscountTax, eligibleForPRDiscountTax);
				}
				generateXML = generateXML(taxData2, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
				radialTaxPriceInfoMap.put(pTdfShipGroupResponseType.getId()+TRUTaxwareConstant.UNDERSCORE+
						pOrderItemResponse.getItemDesc()+TRUTaxwareConstant.UNDERSCORE+
						discountResponse.getId()+TRUTaxwareConstant.UNDERSCORE+TRUTaxwareConstant.MERCHDISCOUNTTAX, generateXML);
			}
		}
		pTaxWareStatus.setRadialTaxPriceInfos(radialTaxPriceInfoMap);
	}
	
	/**
	 * Update tax ware status with radial tax.
	 *
	 * @param pTaxWareStatus the tax ware status
	 * @param pRadialTax the radial tax
	 * @param pIsEligibleForPRTax the is eligible for PR tax
	 */
	public void updateTaxWareStatusWithRadialTax(RadialTaxWareStatus pTaxWareStatus, Tax pRadialTax, boolean pIsEligibleForPRTax) {
		double totalTaxAmout = pTaxWareStatus.getAmount();
		double estimatedSalesTax = pTaxWareStatus.getEstimatedSalesTax();
		double estimatedIslandTax = pTaxWareStatus.getEstimatedIslandTax();
		double estimatedLocalTax = pTaxWareStatus.getEstimatedLocalTax();

		totalTaxAmout = totalTaxAmout + pRadialTax.getCalculatedTax().doubleValue();
		//estimatedIslandTax: <Jurisdiction jurisdictionLevel="TERRITORY" jurisdictionId="45699">PUERTO RICO</Jurisdiction>
		if(pRadialTax.getJurisdiction().getJurisdictionLevel().equalsIgnoreCase(TRUTaxwareConstant.TERRITORY) && 
				pRadialTax.getJurisdiction().getValue().equalsIgnoreCase(TRUTaxwareConstant.PUERTO_RICO)) {
			estimatedIslandTax = estimatedIslandTax + pRadialTax.getCalculatedTax().doubleValue();
		}
		//estimatedLocalTax: <Jurisdiction jurisdictionLevel="COUNTY" jurisdictionId="79494">PONCE</Jurisdiction>
		else if(pRadialTax.getJurisdiction().getJurisdictionLevel().equalsIgnoreCase(TRUTaxwareConstant.COUNTY) && pIsEligibleForPRTax){
			estimatedLocalTax = estimatedLocalTax + pRadialTax.getCalculatedTax().doubleValue();
		} else {
			estimatedSalesTax = estimatedSalesTax + pRadialTax.getCalculatedTax().doubleValue();
		}
		pTaxWareStatus.setAmount(totalTaxAmout);
		pTaxWareStatus.setEstimatedSalesTax(estimatedSalesTax);
		pTaxWareStatus.setEstimatedIslandTax(estimatedIslandTax);
		pTaxWareStatus.setEstimatedLocalTax(estimatedLocalTax);
	}
	
	/**
	 * This method converts the response to XML format.
	 * @param pObj Object
	 * @param pPackageName String
	 * @return xmlRequest  String
	 */
	public String generateXML(Object pObj, String pPackageName) {
		if (isLoggingDebug()) {
			vlogDebug("ENTER::::TRURadialTaxwareHelper generateXML()");
		}
		String xmlRequest = TRUTaxwareConstant.EMPTY_STRING;
		if(pObj != null){
			StringWriter sw = new StringWriter();
			try {
				if(mContext == null) {
					if (isLoggingDebug()) {
						vlogDebug("TRURadialTaxwareHelper generateXML mContext object is null hence calling the getJAXBContextInstance()");
					}
					getJAXBContextInstance();
				}
				Marshaller marshaller = mContext.createMarshaller();
				marshaller.marshal(pObj, sw);
				xmlRequest = sw.toString();
				if (isLoggingDebug()) {
					vlogDebug("TRURadialTaxwareHelper generateXML xml Request--- {0} ", xmlRequest);
				}
				sw.close();
			} catch (JAXBException e) {
				if (isLoggingError()) {
					logError("JAXBException in TRURadialTaxwareHelper generateXML() : ", e);
				}
			} catch (IOException e) {
				if (isLoggingError()) {
					logError("IOException in TRURadialTaxwareHelper generateXML() : ", e);
				}
			}
			if (isLoggingDebug()) {
				vlogDebug("EXIT::::TRURadialTaxwareHelper generateXML()");
			}
		}
		return xmlRequest;
	}

	/**
	 * Construct fault tax ware status.
	 *
	 * @param pFaultResponseType the fault response type
	 * @return the radial tax ware status
	 */
	public RadialTaxWareStatus constructFaultTaxWareStatus(
			FaultResponseType pFaultResponseType) {
		String faultResponseXML = generateXML(pFaultResponseType, TRUTaxwareConstant.RADIAL_TAXWARE_STUB_PACKAGE);
		RadialTaxWareStatus taxWareStatus = new RadialTaxWareStatus();
		taxWareStatus.setRadialTaxFaultDetails(faultResponseXML);
		taxWareStatus.setAmount(0);
		taxWareStatus.setTransactionSuccess(Boolean.FALSE);
		taxWareStatus.setTransactionTimestamp(getCurrentDate().getTimeAsTimestamp());
		return taxWareStatus;
	}

	/**
	 * property to hold currentDate.
	 */
	private CurrentDate mCurrentDate;

	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * @param pCurrentDate the currentDate to set
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}

	/**
	 * Construct error tax duty quote response.
	 *
	 * @return the radial tax ware status
	 */
	public RadialTaxWareStatus constructErrorTaxDutyQuoteResponse() {
		RadialTaxWareStatus taxWareStatus = new RadialTaxWareStatus();
		taxWareStatus.setAmount(TRUTaxwareConstant.NUMERIC_ZERO);
		taxWareStatus.setTransactionSuccess(Boolean.FALSE);
		taxWareStatus.setTransactionTimestamp(getCurrentDate().getTimeAsTimestamp());
		return taxWareStatus;
	}
	
	/**
	 * Checks if is eligible for pr tax.
	 *
	 * @param pListOfGiftingTax the list of gifting tax
	 * @return true, if is eligible for pr tax
	 */
	private boolean isEligibleForPRTax(List<Tax> pListOfGiftingTax) {
		for (Tax tax : pListOfGiftingTax) {
			//tax: <Jurisdiction jurisdictionLevel="TERRITORY" jurisdictionId="45699">PUERTO RICO</Jurisdiction>
			if(tax.getJurisdiction().getJurisdictionLevel().equalsIgnoreCase(TRUTaxwareConstant.TERRITORY) && 
					tax.getJurisdiction().getValue().equalsIgnoreCase(TRUTaxwareConstant.PUERTO_RICO)) {
				return true;
			}
		}
		return false;
	}
}
