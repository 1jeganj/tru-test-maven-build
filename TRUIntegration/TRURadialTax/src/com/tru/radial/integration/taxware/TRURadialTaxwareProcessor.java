package com.tru.radial.integration.taxware;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;

import javax.xml.datatype.DatatypeConfigurationException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.tru.radial.common.IRadialConstants;
import com.tru.radial.exception.TRURadialIntegrationException;
import com.tru.radial.service.RadialGatewayResponse;
import com.tru.radial.service.TRURadialBaseProcessor;
import com.tru.radial.taxquote.FaultResponseType;
import com.tru.radial.taxquote.TaxDutyQuoteResponse;

/**
 * This class implements the multiple items requests sent to Taxware, where items are represented
 * by the shipping destination group objects passed in TaxRequestInfo.
 * 
 * @author PA
 * @version 1.0
 * 
 */
public class TRURadialTaxwareProcessor extends TRURadialBaseProcessor {
	
	/** The Radial taxware helper. */
	private TRURadialTaxwareHelper mRadialTaxwareHelper;
	
	/**
	 * @return the radialTaxwareHelper
	 */
	public TRURadialTaxwareHelper getRadialTaxwareHelper() {
		return mRadialTaxwareHelper;
	}

	/**
	 * @param pRadialTaxwareHelper the radialTaxwareHelper to set
	 */
	public void setRadialTaxwareHelper(TRURadialTaxwareHelper pRadialTaxwareHelper) {
		mRadialTaxwareHelper = pRadialTaxwareHelper;
	}

	/**
	 * This method is used calculate process.
	 *
	 * @param pTaxQuoteRequestXML the tax quote request XML
	 * @return recordResult -TaxStatus Object
	 */
	public RadialTaxWareStatus calculateRadialTaxQuote(String pTaxQuoteRequestXML) {
		RadialTaxWareStatus taxWareStatus = null;
		if(pTaxQuoteRequestXML != null ) {
			if (isLoggingDebug()) {
				vlogDebug("Taxware Request :{0}",pTaxQuoteRequestXML);
			}
			try {
				taxWareStatus =	calculateTaxes(pTaxQuoteRequestXML);
			} catch (DatatypeConfigurationException e) {
				if(isLoggingError()) {
					vlogError("DatatypeConfigurationException in TRURadialTaxwareProcessor.calculateRadialTaxQuote() {0}", e);
				}
				taxWareStatus = new RadialTaxWareStatus();
				taxWareStatus.setAmount(0);
				taxWareStatus.setTransactionSuccess(Boolean.FALSE);
			}
		}else{
			if (isLoggingDebug()) {
				vlogDebug("Taxware Request is NULL");
			}
		}
		return taxWareStatus;
	}

	
	/**
	 * Calculate taxes.
	 *
	 * @param pTaxQuoteRequestXML the tax quote request xml
	 * @return the radial tax ware status
	 * @throws DatatypeConfigurationException the datatype configuration exception
	 */
	public RadialTaxWareStatus calculateTaxes(String pTaxQuoteRequestXML) throws DatatypeConfigurationException {
		RadialTaxWareStatus taxWareStatus = null;
		try {
			RadialGatewayResponse response = executeRequest(pTaxQuoteRequestXML, TRUTaxwareConstant.TAX_QUOTE, TRUTaxwareConstant.NUMERIC_ZERO);
			if(response == null) {
				taxWareStatus = getRadialTaxwareHelper().constructErrorTaxDutyQuoteResponse();
			} else if(response.getStatusCode() == TRUTaxwareConstant.NUMERIC_200) {
				vlogDebug("Taxware Response :{0}",formatXML(response.getResponseBody()));
				Object taxDutyQuoteResponseObj =  transformToObject(response.getResponseBody(), TaxDutyQuoteResponse.class);
				taxWareStatus = getRadialTaxwareHelper().constructTaxWareStatus((TaxDutyQuoteResponse)taxDutyQuoteResponseObj);
			} else if(response.getStatusCode() == TRUTaxwareConstant.NUMERIC_400 || response.getStatusCode() == TRUTaxwareConstant.NUMERIC_500) {
				vlogDebug("Taxware Response :{0}",formatXML(response.getResponseBody()));
				Object faultResponseTypeObj =  transformToObject(response.getResponseBody(), FaultResponseType.class);
				taxWareStatus = getRadialTaxwareHelper().constructFaultTaxWareStatus((FaultResponseType)faultResponseTypeObj);
			}
			if (taxWareStatus != null && response != null) {
				taxWareStatus.setConnectionStatus(response.getStatusCode());
			}
		} catch (TRURadialIntegrationException te) {
			if(isLoggingError()) {
				vlogError("TRURadialIntegrationException in TRURadialTaxwareProcessor.calculateTaxes() {0}", te);
			}
			taxWareStatus = getRadialTaxwareHelper().constructErrorTaxDutyQuoteResponse();
		}
		long time = Calendar.getInstance().getTimeInMillis();
		time = Calendar.getInstance().getTimeInMillis() - time;
		if(taxWareStatus != null){
			taxWareStatus.setServiceTransactionTime(time);
		}
		return taxWareStatus;
	}

	/**
	 * Format xml.
	 *
	 * @param pInput the input
	 * @return the string
	 */
	public String formatXML(String pInput) {
		try {
			Document doc = DocumentHelper.parseText(pInput);
			StringWriter sw = new StringWriter();
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setIndent(true);
			format.setIndentSize(IRadialConstants.THREE);
			XMLWriter xw = new XMLWriter(sw, format);
			xw.write(doc);
			return sw.toString();
		} catch (IOException e) {
			if (isLoggingError()) {
				logError("formatXML exception", e);
			}
			return pInput;
		} catch (DocumentException e) {
			if (isLoggingError()) {
				logError("formatXML exception", e);
			}
			return pInput;
		}
	}
}
