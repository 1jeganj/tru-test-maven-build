package com.tru.gfs.integration;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.gfs.order.BillTo;
import com.gfs.order.Error;
import com.gfs.order.LineItem;
import com.gfs.order.ManageOnlineOrderWebService;
import com.gfs.order.ObjectFactory;
import com.gfs.order.OrderDetailRequest;
import com.gfs.order.OrderDetailRequest.OrderDetailReq;
import com.gfs.order.OrderDetailResponse;
import com.gfs.order.OrderDetailResponse.OrderDetail;
import com.gfs.order.OrderSummaryLookupRequest;
import com.gfs.order.OrderSummaryLookupRequest.OrderSummaryLookupReq;
import com.gfs.order.OrderSummaryLookupResponse;
import com.gfs.order.OrderSummaryLookupResponse.OrderSummary;
import com.gfs.order.Payment;
import com.gfs.order.PaymentHistory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 *  This class is for GFS integration utility purpose.
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUGFSUtils extends GenericService {

	private static final String GFS_ORDER_SUMMARY_OPR = "GFSOrderSummary";

	private static final String GFSORDER_SUMMARY_LOOKUP = "orderSummaryLookup";

	private static final String NULL_RESPONSE_CODE = "200";

	private static final String GFS_ORDER_DETAILS_SERVICE = "GFSOrderDetailsService";

	private static final String GFS_ORDER_SERVICE = "ManageOnlineOrderWebService";

	/**
	 * Holds reference for TRURadialOMUtils
	 */
	private TRURadialOMUtils mOmUtils;
	
	/** This holds OM configuration. */
	private TRURadialOMConfiguration mOmConfiguration;
	
	/**
	 * This method is to populate request for order summary lookup service.
	 * 
	 * @param pCustomerEmail
	 *            - customerEmail
	 * @return orderSummaryLookupRequest - orderSummaryLookupRequest
	 */
	public OrderSummaryLookupRequest populateOrderSummaryLookupRequest(String pCustomerEmail){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: populateOrderSummaryLookupRequest()");
		}
		ObjectFactory objFactory = new ObjectFactory();
		OrderSummaryLookupRequest orderSummaryLookupRequest = objFactory.createOrderSummaryLookupRequest();
		OrderSummaryLookupReq orderSummaryLookupReq = objFactory.createOrderSummaryLookupRequestOrderSummaryLookupReq();
		orderSummaryLookupReq.setDeliveryMethod(getOmConfiguration().getLayawayDeliveryMethod());
		orderSummaryLookupReq.setEmail(pCustomerEmail);
		orderSummaryLookupReq.setTransactionID(generateTransactionId(pCustomerEmail));
		orderSummaryLookupRequest.setOrderSummaryLookupReq(orderSummaryLookupReq);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUGFSUtils method: populateOrderSummaryLookupRequest()");
		}
		return orderSummaryLookupRequest;
	}
	
	/**
	 * This method is to call the Order Look up service
	 * 
	 * @param pOrderSummaryLookupRequest
	 *            - OrderSummaryLookupRequest
	 * @return orderSummaryLookupResponse - Look up response
	 */
	public OrderSummaryLookupResponse callOrderSummaryLookupService(OrderSummaryLookupRequest pOrderSummaryLookupRequest){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: callOrderSummaryLookupService()");
		}
		URL url = null;
		QName qname = null;
		OrderSummaryLookupResponse orderSummaryLookupResponse = null;
		HttpURLConnection connection = null;
		try {
			url = new URL(getOmConfiguration().getWsdlLocation());
			qname = new QName(getOmConfiguration().getNameSpace(), getOmConfiguration().getServiceName());
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			Service service = Service.create(url, qname);
			ManageOnlineOrderWebService manageOnlineOrderWebService = service.getPort(ManageOnlineOrderWebService.class);
			orderSummaryLookupResponse = manageOnlineOrderWebService.orderSummaryLookup(pOrderSummaryLookupRequest);
			} 
		catch (IOException e) {
				if (isLoggingError()) {
					logError("Connection refused: connect failed due to service down", e);
				}
			}
		finally{
				if (connection != null) {
					connection.disconnect();
			    }
			}
		if(isLoggingDebug()){
			vlogDebug("Order Lookup Response : {0}", orderSummaryLookupResponse);
			logDebug("Exit from class: TRUGFSUtils method: callOrderSummaryLookupService()");
		}
		return orderSummaryLookupResponse;
	}
	
	/**
	 * This method is to parse the order lookup response
	 * @param pOrderSummaryLookupResponse - OrderSummaryLookupResponse
	 * @param pEndTime - The endTime
	 * @param pStarTime - The startTime
	 * @return response as String
	 */
	public String parseOrderSummaryLookupResponse(OrderSummaryLookupResponse pOrderSummaryLookupResponse, long pStarTime, long pEndTime){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: parseOrderSummaryLookupResponse()");
		}
		JsonObject jsonObj=new JsonObject();
		JsonArray jsonAry = new JsonArray();
		if(pOrderSummaryLookupResponse !=null){
		List<OrderSummary> orderSummaryList=pOrderSummaryLookupResponse.getOrderSummary();
		if(!orderSummaryList.isEmpty()){
		for (OrderSummary orderSummary : orderSummaryList) {
			JsonObject ordObj=new JsonObject();
			constructJsonObjectwithOrderSummary(ordObj, orderSummary);
			jsonAry.add(ordObj);
		}
		}
		else{
			if(TRURadialOMConstants.FAILURE.equalsIgnoreCase(pOrderSummaryLookupResponse.getStatus())){
				JsonObject errObj=new JsonObject();
				errObj.addProperty(TRURadialOMConstants.MESSAGE, getOmConfiguration().getInvalidEmailIDFound());
				jsonAry.add(errObj);
				getOmUtils().sendAlertLogFailure(GFS_ORDER_SERVICE, GFSORDER_SUMMARY_LOOKUP, GFS_ORDER_SUMMARY_OPR, pOrderSummaryLookupResponse.getStatus(), getOmConfiguration().getServiceError(), pStarTime, pEndTime);
				}
			}
		}
		else{
			JsonObject seviceErrorObj=new JsonObject();
			seviceErrorObj.addProperty(TRURadialOMConstants.MESSAGE, getOmConfiguration().getServiceDown());
			jsonAry.add(seviceErrorObj);
			getOmUtils().sendAlertLogFailure(GFS_ORDER_SERVICE, GFSORDER_SUMMARY_LOOKUP, GFS_ORDER_SUMMARY_OPR, NULL_RESPONSE_CODE, getOmConfiguration().getServiceDown(), pStarTime, pEndTime);
		}
		jsonObj.add(TRURadialOMConstants.ORDERLIST, jsonAry);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUGFSUtils method: parseOrderSummaryLookupResponse()"+jsonObj.toString());
		}
		return jsonObj.toString();
	}
		
	/**
	 * This method is to construct json object with orders summary list.
	 * @param pOrdObj - json order object
	 * @param pOrderSummary - json object
	 */
	private void constructJsonObjectwithOrderSummary(JsonObject pOrdObj, OrderSummary pOrderSummary) {
		pOrdObj.addProperty(TRURadialOMConstants.ORDERNUMBER, pOrderSummary.getPartnerOrderNumber());
		String orderCreationDate = pOrderSummary.getOrderCreationDate();		
		DateFormat dateFormat = new SimpleDateFormat(TRURadialOMConstants.LAYAWAY_DATE_FORMAT);
		DateFormat dateFormat1 = new SimpleDateFormat(TRURadialOMConstants.LAYAWAY_SIMPLE_DATE_FORMAT);
		Date tempDate = null;
		try {
			tempDate = dateFormat1.parse(orderCreationDate);
		} catch (ParseException exce) {
			if (isLoggingError()) {
				logError("ParseException:  While parsing layaway Date", exce);
			}
		}
		orderCreationDate = dateFormat.format(tempDate);		
		pOrdObj.addProperty(TRURadialOMConstants.CREATION_DATE, orderCreationDate);
		pOrdObj.addProperty(TRURadialOMConstants.ORDER_TOTAL, pOrderSummary.getOriginalOrderTotal());
		pOrdObj.addProperty(TRURadialOMConstants.ORDER_STATUS, pOrderSummary.getOrderStatus());
		pOrdObj.addProperty(TRURadialOMConstants.BALANCE_DUE, pOrderSummary.getBalanceDue());
	}
	
	/**
	 * This method is to populate request for order details service.
	 * 
	 * @param pOrderId
	 *            - order Id
	 * @return orderDetailRequest - orderDetailRequest
	 */
	public OrderDetailRequest populateOrderDetailRequest(String pOrderId){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: populateOrderDetailRequest()");
		}
		ObjectFactory objFactory = new ObjectFactory();
		OrderDetailRequest orderDetailRequest = objFactory.createOrderDetailRequest();
		OrderDetailReq orderDetailReq = objFactory.createOrderDetailRequestOrderDetailReq();
		orderDetailReq.setDeliveryMethod(getOmConfiguration().getLayawayDeliveryMethod());
		orderDetailReq.setPartnerOrderNumber(Long.parseLong(pOrderId));
		orderDetailReq.setTransactionID(generateTransactionId(pOrderId));
		orderDetailRequest.setOrderDetailReq(orderDetailReq);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUGFSUtils method: populateOrderDetailRequest()");
		}
		return orderDetailRequest;
	}
	
	/**
	 * Generate transaction id.
	 *
	 * @param pId the id
	 * @return the string
	 */
	private String generateTransactionId(String pId) {
		if (isLoggingDebug()) {
			logDebug("Enter into class: TRUGFSUtils method: generateTransactionId(String pId)");
		}
		if(StringUtils.isBlank(pId)){
			return null;
		}
		StringBuilder stringBuilder = new StringBuilder();
		Date today = Calendar.getInstance().getTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TRURadialOMConstants.LAYAWAY_TRANSACTION_ID_DATE_FORMAT);
		String transID = simpleDateFormat.format(today);
		if (isLoggingDebug()) {
			logDebug("Exit from class: TRUGFSUtils method: generateTransactionId(String pId)");
		}
		return stringBuilder.append(pId).append(transID).toString();
	}
	
	/**
	 * This method is to call the Order Details service.
	 *
	 * @param pOrderDetailRequest 				 - OrderDetailRequest
	 * @return object - OrderDetailResponse
	 */
	public OrderDetailResponse callOrderDetailService(OrderDetailRequest pOrderDetailRequest){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: callOrderDetailService()");
		}
		URL url = null;
		QName qname = null;
		OrderDetailResponse detailResponse = null;
		HttpURLConnection connection = null;
		try {
			url = new URL(getOmConfiguration().getWsdlLocation());
			qname = new QName(getOmConfiguration().getNameSpace(), getOmConfiguration().getServiceName());
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			Service service = Service.create(url, qname);
			ManageOnlineOrderWebService manageOnlineOrderWebService = service.getPort(ManageOnlineOrderWebService.class);
			detailResponse = manageOnlineOrderWebService.orderDetail(pOrderDetailRequest);
			} 
		catch (IOException e) {
				if (isLoggingError()) {
					logError("Connection refused: connect failed due to service down", e);
				}
			}
		finally{
			if (connection != null) {
				connection.disconnect();
		    }
		}
		if(isLoggingDebug()){
			vlogDebug("Order Detail Response : {0}", detailResponse);
			logDebug("Exit from class: TRUGFSUtils method: callOrderDetailService()");
		}
		return detailResponse;
	}
		
	/**
	 * This method is to parse the order lookup response.
	 *
	 * @param pDetailResponse  - OrderSummaryLookupResponse
	 * @param pStartTime  - The startTime 
	 * @param pEndTime  - The endTime
	 * @return response - response in String
	 */
	public String parseOrderDetailResponse(OrderDetailResponse pDetailResponse, long pStartTime, long pEndTime){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRUGFSUtils method: parseOrderDetailResponse()");
		}
		Gson gson=new Gson();
		String responseJson=null;
		JsonObject paymentHistoryJsonObj = null;
		JsonObject lineItemJson = null;
		JsonObject orderDetailsJson = new JsonObject();
		//JsonArray orderDetailsArray= null;
		JsonObject orderDetailResponse = new JsonObject();
		if(pDetailResponse !=null){
		OrderDetail orderDetail=pDetailResponse.getOrderDetail();
		if(orderDetail != null){
		JsonArray lineItemJsonArray = new JsonArray();
		JsonArray paymentHistoryJsonArray = new JsonArray();
		String orderStatus=orderDetail.getOrderStatus();
		long orderNumber=orderDetail.getPartnerOrderNumber();
		int locationID=orderDetail.getLocationID();
		String orderCreationDate = orderDetail.getOrderCreationDate();
		DateFormat dateFormat = new SimpleDateFormat(TRURadialOMConstants.LAYAWAY_DATE_FORMAT);
		DateFormat dateFormat1 = new SimpleDateFormat(TRURadialOMConstants.LAYAWAY_SIMPLE_DATE_FORMAT);
		Date tempDate = null;
		try {
			tempDate = dateFormat1.parse(orderCreationDate);
		} catch (ParseException exce) {
			if (isLoggingError()) {
				logError("ParseException:  While parsing layaway Date", exce);
			}
		}
		orderCreationDate = dateFormat.format(tempDate);
		Payment payment = orderDetail.getPayment();
		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
		BigDecimal originalOrderTotal = payment.getOriginalOrderTotal();
		BigDecimal downPayment = payment.getDownPayment();
		BigDecimal additionalPayment = payment.getAdditionalPayment();
		BigDecimal outstandingBalance = null;
		BigDecimal totalPaid = null;
		if (downPayment != null && additionalPayment != null) {
			totalPaid = downPayment.add(additionalPayment);
		}
		if (originalOrderTotal != null && totalPaid != null) {
			outstandingBalance = originalOrderTotal.subtract(totalPaid);
		}
		orderDetailsJson.addProperty(TRURadialOMConstants.ORDER_STATUS, orderStatus);
		orderDetailsJson.addProperty(TRURadialOMConstants.PARTNER_ORDER_NUMBER, orderNumber);
		orderDetailsJson.addProperty(TRURadialOMConstants.ORDER_CREATION_DATE, orderCreationDate);
		orderDetailsJson.addProperty(TRURadialOMConstants.LOCATION_ID, locationID);
		orderDetailsJson.addProperty(TRURadialOMConstants.ORDER_TOTAL, originalOrderTotal);
		orderDetailsJson.addProperty(TRURadialOMConstants.DOWN_PAYMENT, downPayment);
		if(outstandingBalance !=null){
		orderDetailsJson.addProperty(TRURadialOMConstants.OUTSTANDING_BALANCE, decimalFormat.format(outstandingBalance.doubleValue()));
		}
		orderDetailsJson.addProperty(TRURadialOMConstants.HOWTO_GETIT, getOmConfiguration().getHowToGetItProperty());
		BillTo billTo=orderDetail.getBillTo();
		String firstName=billTo.getFirstName();
		String lastName=billTo.getLastName();
		orderDetailsJson.addProperty(TRURadialOMConstants.FIRSTNAME, firstName);
		orderDetailsJson.addProperty(TRURadialOMConstants.LASTNAME, lastName);
		List<PaymentHistory> paymentHistoryList=orderDetail.getPaymentHistory();
		for(PaymentHistory paymentHistory:paymentHistoryList){
			paymentHistoryJsonObj = new JsonObject();
			BigDecimal amountPaid=paymentHistory.getAmountPaid();
			String date=paymentHistory.getDate();
			try {
				tempDate = dateFormat1.parse(date);
			} catch (ParseException exce) {
				if (isLoggingError()) {
					logError("ParseException:  While parsing layaway Date", exce);
				}
			}
			date = dateFormat.format(tempDate);
			paymentHistoryJsonObj.addProperty(TRURadialOMConstants.DATE, date);
			paymentHistoryJsonObj.addProperty(TRURadialOMConstants.AMOUNT_PAID, decimalFormat.format(amountPaid.doubleValue()));
			paymentHistoryJsonArray.add(paymentHistoryJsonObj);
		}
		orderDetailsJson.add(TRURadialOMConstants.PAYMENT_HISTORY, paymentHistoryJsonArray);
		List<LineItem> lineItemList=orderDetail.getLineItem();
		for (LineItem lineItem : lineItemList) {
			lineItemJson = new JsonObject();
			String description= lineItem.getDescription();
			String lineStatus=lineItem.getLineStatus();
			int quantity=lineItem.getQuantity();
			lineItemJson.addProperty(TRURadialOMConstants.DESCRIPTION, description);
			lineItemJson.addProperty(TRURadialOMConstants.LINE_STATUS, lineStatus);
			lineItemJson.addProperty(TRURadialOMConstants.QUANTITY, quantity);
			lineItemJsonArray.add(lineItemJson);
		}
		orderDetailsJson.add(TRURadialOMConstants.LINE_ITEM, lineItemJsonArray);
		/*if(orderDetailsJson instanceof JsonObject){
			orderDetailsArray = new JsonArray();
			orderDetailsArray.add(orderDetailsJson);
		}*/
		orderDetailResponse.add(TRURadialOMConstants.ORDER_DETAIL, orderDetailsJson);
		responseJson = gson.toJson(orderDetailResponse);
		}
		else {
			if(TRURadialOMConstants.FAILURE.equalsIgnoreCase(pDetailResponse.getStatus())){
			List<Error> errorList=pDetailResponse.getError();
			for (Error error : errorList) {
				JsonObject errObj=new JsonObject();
				String errorMessage = error.getErrorMessage();
				errObj.addProperty(TRURadialOMConstants.MESSAGE, errorMessage);
				orderDetailResponse.add(TRURadialOMConstants.ORDER_DETAIL, errObj);
				responseJson = gson.toJson(orderDetailResponse);
					}
				getOmUtils().sendAlertLogFailure(GFS_ORDER_SERVICE, GFS_ORDER_DETAILS_SERVICE, GFS_ORDER_DETAILS_SERVICE, pDetailResponse.getStatus(), pDetailResponse.getError().toString(), pStartTime, pEndTime);
				}
			}
		}
		else{
			JsonObject seviceErrorObj=new JsonObject();
			seviceErrorObj.addProperty(TRURadialOMConstants.MESSAGE, getOmConfiguration().getServiceDown());
			orderDetailResponse.add(TRURadialOMConstants.ORDER_DETAIL, seviceErrorObj);
			responseJson = gson.toJson(orderDetailResponse);
			getOmUtils().sendAlertLogFailure(GFS_ORDER_SERVICE, GFS_ORDER_DETAILS_SERVICE, GFS_ORDER_DETAILS_SERVICE, NULL_RESPONSE_CODE, getOmConfiguration().getServiceDown(), pStartTime, pEndTime);

		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUGFSUtils method: parseOrderDetailResponse()");
		}
		return responseJson;
	}

	/**
	 * This is to get the value for omUtils
	 *
	 * @return the omUtils value
	 */
	public TRURadialOMUtils getOmUtils() {
		return mOmUtils;
	}

	/**
	 * This method to set the pOmUtils with omUtils
	 * 
	 * @param pOmUtils the omUtils to set
	 */
	public void setOmUtils(TRURadialOMUtils pOmUtils) {
		mOmUtils = pOmUtils;
	}
	
	/**
	 * This is to get the Value for omConfiguration.
	 *
	 * @return the omConfiguration Value
	 */
	public TRURadialOMConfiguration getOmConfiguration() {
		return mOmConfiguration;
	}

	/**
	 * This method to set the pOmConfiguration with omConfiguration.
	 *
	 * @param pOmConfiguration
	 *            the omConfiguration to set
	 */
	public void setOmConfiguration(TRURadialOMConfiguration pOmConfiguration) {
		mOmConfiguration = pOmConfiguration;
	}
}
