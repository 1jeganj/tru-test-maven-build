package com.tru.commerce.order.processor;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBElement;

import atg.commerce.CommerceException;
import atg.commerce.fulfillment.SubmitOrder;
import atg.commerce.order.Order;
import atg.service.pipeline.PipelineResult;

import com.jda.createOrder.Fault;
import com.jda.createOrder.OrderCreateResponse;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.radialom.TRURadialOMTools;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 * This class overridden to generate the customer order import request in sync
 * with order placement.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMSendFulfillmentMessage extends TRUSendFulfillmentMessage {
	
	/** Holds reference for mEnabled. */
	private boolean mEnabled;
	
	/** The m xsd xml validation enable. */
	private boolean mXsdXmlValidationEnable;
	
	private TRURadialOMTools mOmTools;
	
	/**
	 * Checks if is xsd xml validation enable.
	 *
	 * @return true, if is xsd xml validation enable
	 */
	public boolean isXsdXmlValidationEnable() {
		return mXsdXmlValidationEnable;
	}

	/**
	 * Sets the xsd xml validation enable.
	 *
	 * @param pXsdXmlValidationEnable the new xsd xml validation enable
	 */
	public void setXsdXmlValidationEnable(boolean pXsdXmlValidationEnable) {
		 mXsdXmlValidationEnable = pXsdXmlValidationEnable;
	}

	/**
	 * Holds TRURadialOMUtils
	 */
	private TRURadialOMUtils mOmUtils;
	
	
	@Override
	public int runProcess(Object pParam, PipelineResult pResult) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: runProcess]");
		}
		if(!isEnabled()){
			return TRUConstants._1;
		}
		try {
			Serializable eventToSend = createEventToSend(pParam, pResult);

			if (eventToSend == null) {
				return TRUConstants._1;
			}
			sendObjectMessage(eventToSend, getEventType(eventToSend),
					getPortName());
		} catch (Exception exception) {
			if (isLoggingError()) {
				vlogError(exception,
						"Exception in method TRURadialOMSendFulfillmentMessage.runProcess()");
			}
			return TRUConstants._1;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from[Class: TRURadialOMSendFulfillmentMessage  method: runProcess]");
		}
		return TRUConstants._1;
		}
	
	/**
	 * This method will send XML message from source
	 * @param pObjectMessage - ObjectMessage
	 * @param pType - JMS type
	 * @param pPortName - PortName
	 * 
	 */
	@Override
	public void sendObjectMessage(Serializable pObjectMessage, String pType,
			String pPortName)  {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: sendObjectMessage]");
		}
		String customerOrderXML = null;
		boolean valid=Boolean.FALSE;
		TRULayawayOrderImpl layawayOrderImpl = null;
		Order order = null;
		SubmitOrder submitOrder = null;
		String customerOrderImportTransPrefix = getOmUtils().getOmConfiguration().getCustomerOrderImportTransPrefix();
		try{
		if(pObjectMessage instanceof SubmitOrder){
			submitOrder = (SubmitOrder) pObjectMessage;
			if(submitOrder != null){
				order = submitOrder.getOrder();
			}
		}
		if (order != null && isAllowMessageSending())
		{
			if(order instanceof TRULayawayOrderImpl){
				layawayOrderImpl = (TRULayawayOrderImpl) order;
				customerOrderXML = getOmUtils().populateLayawayPaymentRequestXML(layawayOrderImpl);
				if(isLoggingDebug()){
					vlogDebug("customerOrderXML : {0} ", customerOrderXML); 
				}
			}else if(order instanceof TRUOrderImpl){
				TRUOrderImpl order1 = (TRUOrderImpl) order;
				customerOrderXML = getOmUtils().populateOrderCreateRequestXML(order1);
			}
			if(getOmUtils().getOmConfiguration().isCallCreateOrderRESTService()){
				callCreateOrderRESTService(customerOrderXML);
			}
			if(isXsdXmlValidationEnable()){
				valid=getOmUtils().validateFile(customerOrderXML);
			}
			if(valid){
				TextMessage om=getMessageSourceContext().createTextMessage(pPortName);
				om.setJMSCorrelationID(customerOrderImportTransPrefix+order.getId());
				om.setText(customerOrderXML);
				om.setJMSType(pType);
				//Send message
				getMessageSourceContext().sendMessage(pPortName, om);
				//Audit Order Create Request
				getOmUtils().processOrderCreateRequest(customerOrderXML, order.getId());
				if(isLoggingDebug()){
					vlogDebug("Order create request audited. Order XML : {0} Order Id : {1}", customerOrderXML, order.getId());
				}
			}else if(!isXsdXmlValidationEnable()){
				TextMessage om=getMessageSourceContext().createTextMessage(pPortName);
				om.setJMSCorrelationID(customerOrderImportTransPrefix+order.getId());
				om.setText(customerOrderXML);
				om.setJMSType(pType);
				//Send message
				getMessageSourceContext().sendMessage(pPortName, om);
				//Audit Order Create Request
				getOmUtils().processOrderCreateRequest(customerOrderXML, order.getId());
			}
				if(isLoggingDebug()){
					vlogDebug("Customer order import xml sent to  queue : {0}",  customerOrderXML);
				}
			//Process the generated CO request
			//getIntegrationManager().processCOImportXMLRequest(customerOrderXML, order.getId());
			if(isLoggingDebug()){
				vlogDebug("Customer order import xml   :   {0} has been audited.",  customerOrderXML);
			}
		} else{
			super.sendObjectMessage(pObjectMessage, pType, pPortName);
		}
		}catch(JMSException jmsException){
			if (order.getId() != null) {
				if (isLoggingError()) {
					vlogError(jmsException,	"JMSException in method TRURadialOMSendFulfillmentMessage.sendObjectMessage()");
				}
				getOmUtils().getTruRadialOMTools().updateOmsErrorItemForOrder(order.getId(), TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE,jmsException,customerOrderXML);
			}
		}catch(Exception exception){
			if (order.getId() != null) {
				if (isLoggingError()) {
					vlogError(exception, "Exception in method TRURadialOMSendFulfillmentMessage.sendObjectMessage()");
				}
				getOmUtils().getTruRadialOMTools().updateOmsErrorItemForOrder(order.getId(), TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE,exception,customerOrderXML);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMSendFulfillmentMessage  method: sendObjectMessage]");
		}
	}
	
	
	/**
	 * This method is to send failed orders
	 * @param pOrderId - Order id
	 */
	public void sendFailedOrders(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: sendFailedOrders]");
		}
		if(pOrderId == null){
			return;
		}
		String customerOrderXML = null;
		TRULayawayOrderImpl layawayOrderImpl = null;
		TRUOrderImpl orderImpl = null;
		Order order = null;
		TRURadialOMConfiguration configuration = getOmUtils().getOmConfiguration();
		String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
		try {
			order = getOmUtils().getOrderManager().loadOrder(pOrderId);
			if (order != null && isAllowMessageSending())
			{
				if(order instanceof TRULayawayOrderImpl){
					layawayOrderImpl = (TRULayawayOrderImpl) order;
					customerOrderXML = getOmUtils().populateLayawayPaymentRequestXML(layawayOrderImpl);
					if(isLoggingDebug()){
						vlogDebug("customerOrderXML : {0} ", customerOrderXML); 
					}
				}else if(order instanceof TRUOrderImpl){
					orderImpl = (TRUOrderImpl) order;
					customerOrderXML = getOmUtils().populateOrderCreateRequestXML(orderImpl);
				}
				ObjectMessage om = getMessageSourceContext().createObjectMessage(getPortName());
				om.setJMSCorrelationID(customerOrderImportTransPrefix+order.getId());
				om.setObject(customerOrderXML);
				om.setJMSType(null);
			}else if(order !=null && isAllowMessageSending()){
				customerOrderXML =getOmTools().getLaywayOrderFromErrorRepoistory(pOrderId);
				ObjectMessage om = getMessageSourceContext().createObjectMessage(getPortName());
				om.setJMSCorrelationID(customerOrderImportTransPrefix+pOrderId);
				om.setObject(customerOrderXML);
				om.setJMSType(null);
				//Send message
				getMessageSourceContext().sendMessage(getPortName(), om);
				//Process the generated CO request
				getOmUtils().processOrderCreateRequest(customerOrderXML, order.getId());
				if(isLoggingDebug()){
					vlogDebug("Customer order import xml sent to  queue : {0}",  customerOrderXML);
				}
			}
		} catch (CommerceException exc) {
			if(isLoggingError()){
				vlogError("CommerceException : while loading order with order id : {0} and exception is : {1}",pOrderId, exc);
			}
		} catch (JMSException exc) {
			if(isLoggingError()){
				vlogError("JMSException : while delivering failed orders to queue. order id : {0} and exception is : {1}",pOrderId, exc);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Customer order import xml   :   {0} has been audited.",  customerOrderXML);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMSendFulfillmentMessage  method: sendFailedOrders]");
		}
	}
	
	/**
	 * This method is to call create order service.
	 * 
	 * @param pCustomerOrderXML
	 *            - Request XML
	 */
	@SuppressWarnings("unchecked")
	private void callCreateOrderRESTService(String pCustomerOrderXML) {
		if(isLoggingDebug()){
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: callCreateOrderRESTService]");
		}
		String responseXML = null;
		TRURadialOMConfiguration configuration = getOmUtils().getOmConfiguration();
		Object responseObj = getOmUtils().callOrderService(pCustomerOrderXML, configuration.getOrderCreateXSDPackage(), configuration.getOrderCreateServiceURL());
		 if (responseObj instanceof Fault){
			Fault faultResponse = (Fault) responseObj;
			responseXML = getOmUtils().marshalRequestXML(faultResponse, configuration.getOrderCreateXSDPackage());
		} else {
			JAXBElement<OrderCreateResponse> orderResponse = (JAXBElement<OrderCreateResponse>) responseObj;
			responseXML = getOmUtils().marshalRequestXML(orderResponse, configuration.getOrderCreateXSDPackage());
		}
		if(isLoggingDebug()){
			vlogDebug("Order Create Response XML : {0}", responseXML);
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: callCreateOrderRESTService]");
		}
	}

	/**
	 * This method is to re deliver the create/update xmls to queue in case of any fail overs.
	 * @param pTransId - Transaction Id
	 * @param pOrderMessageXML - create/update xml for  redelivery
	 */
	public void redeliverOrderMessages(String pTransId, String pOrderMessageXML) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: redeliverOrderMessages]");
			vlogDebug("Trans Id : {0} order message xml : {1}", pTransId, pOrderMessageXML);
		}
		if(pTransId == null && pOrderMessageXML == null){
			return;
		}
		try{
			TextMessage om = getMessageSourceContext().createTextMessage(getPortName());
			om.setJMSCorrelationID(pTransId);
			om.setText(pOrderMessageXML);
			om.setJMSType(null);
			//Send message
			getMessageSourceContext().sendMessage(getPortName(), om);
			//Process the generated CO request
			getOmUtils().processOrderCreateRequest(pOrderMessageXML, pTransId);
			if(isLoggingDebug()){
				vlogDebug("Customer order import redelivery xml sent to  queue : {0}",  pOrderMessageXML);
			}
		} catch (JMSException exc) {
			if(isLoggingError()){
				vlogError("JMSException : while re delivering failed xml messages  to queue. trans  id : {0} and exception is : {1}",pTransId, exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMSendFulfillmentMessage  method: redeliverOrderMessages]");
		}
	}

	/**
	 * This method is to send order update XML message
	 * 
	 * @param pOrdeId
	 *            - Order Id
	 * @param pProfileId
	 *            - Profile Id
	 */
	@Override
	public void sendRadialOrderUpdateMessage(String pOrdeId, String pProfileId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMSendFulfillmentMessage  method: sendRadialOrderUpdateMessage]");
		}
		String orderUpdatePrefix = getOmUtils().getOmConfiguration().getCustomerOrderUpdateTransPrefix();
		String updateRequest = null;
		try {
			 updateRequest = getOmUtils().populateOrderUpdateRequest(pOrdeId, pProfileId);
			TextMessage om = getMessageSourceContext().createTextMessage(getPortName());
			om.setJMSCorrelationID(orderUpdatePrefix+pOrdeId);
			om.setText(updateRequest);
			om.setJMSType(null);
			//Send message
			getMessageSourceContext().sendMessage(getPortName(), om);
			//Process the order update request.
			getOmUtils().processOrderUpdateRequest(updateRequest, pOrdeId);
			if(isLoggingDebug()){
				vlogDebug("Customer order update xml sent to  queue : {0} : Order Id : {1}",  updateRequest, pOrdeId);
			}
		} catch (JMSException exc) {
			if (pOrdeId != null) {
				getOmUtils().getTruRadialOMTools().updateOmsErrorItemForOrder(pOrdeId, TRURadialOMConstants.ORDER_UPDATE_SERVICE,exc,updateRequest);
			}
			if(isLoggingError()){
				vlogError("JMSException : While seding order update XML message for order : {0} and exception details are : {1}   ", pOrdeId, exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMSendFulfillmentMessage  method: sendRadialOrderUpdateMessage]");
		}
	}
	
	/**
	 * This method returns the enabled value
	 *
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * This method sets the enabled with parameter value pEnabled
	 *
	 * @param pEnabled the enabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * This is to get the value for omUtils
	 *
	 * @return the omUtils value
	 */
	public TRURadialOMUtils getOmUtils() {
		return mOmUtils;
	}

	/**
	 * This method to set the pOmUtils with omUtils
	 * 
	 * @param pOmUtils the omUtils to set
	 */
	public void setOmUtils(TRURadialOMUtils pOmUtils) {
		mOmUtils = pOmUtils;
	}

	/**
	 * This method is to get omTools
	 *
	 * @return the omTools
	 */
	public TRURadialOMTools getOmTools() {
		return mOmTools;
	}

	/**
	 * This method sets omTools with pOmTools
	 *
	 * @param pOmTools the omTools to set
	 */
	public void setOmTools(TRURadialOMTools pOmTools) {
		mOmTools = pOmTools;
	}
}
