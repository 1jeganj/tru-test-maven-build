package com.tru.jda.messaging;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.jda.integration.radialom.TRURadialOMTools;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 * This Sink is to receive the message to post Customer order update to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMResponseMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds OmsUtils.
	 */
	private TRURadialOMUtils mOmsUtils;
	
	/**
	 * HOlds omTools
	 */
	private TRURadialOMTools mOmTools;
	
	/**
	 * Receives customer order update message and posts to OMS.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRUCustomerOrderResponseMessageSink method : receiveMessage");
		}
		TextMessage textMessage = null;
		//TRUOMSAuditTools auditTools = getIntegrationManager().getOmsAuditTools();
		if(pMessage != null){
			textMessage = (TextMessage) pMessage;
		}
		if(textMessage != null){
			Serializable xmlResponseMessage = textMessage.getText();
			String correlationID = textMessage.getJMSCorrelationID();
			if(isLoggingDebug()){
				vlogDebug("Order message reponse with trans id : {0} is : {1}", correlationID,xmlResponseMessage);
			}
			if(correlationID != null && xmlResponseMessage != null){
				getOmTools().updateAuditMessageItem(correlationID,xmlResponseMessage);
				if(isLoggingDebug()){
					vlogDebug("Updated audit message item with trans id : {0}", correlationID);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRUCustomerOrderResponseMessageSink method : receiveMessage");
		}
	}

	/**
	 * This is to get the value for omsUtils
	 *
	 * @return the omsUtils value
	 */
	public TRURadialOMUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method to set the pOmsUtils with omsUtils
	 * 
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRURadialOMUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This is to get the value for omTools
	 *
	 * @return the omTools value
	 */
	public TRURadialOMTools getOmTools() {
		return mOmTools;
	}

	/**
	 * This method to set the pOmTools with omTools
	 * 
	 * @param pOmTools the omTools to set
	 */
	public void setOmTools(TRURadialOMTools pOmTools) {
		mOmTools = pOmTools;
	}
}
