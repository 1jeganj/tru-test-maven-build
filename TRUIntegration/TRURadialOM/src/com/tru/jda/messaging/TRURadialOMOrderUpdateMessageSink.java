package com.tru.jda.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;

import com.tru.commerce.order.processor.TRURadialOMSendFulfillmentMessage;
import com.tru.jda.integration.utils.TRURadialOMUtils;
import com.tru.messaging.oms.TRUCustomerOrderUpdateMessage;

/**
 * This Sink is to receive the message to post Customer order update to OM.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMOrderUpdateMessageSink extends GenericService implements
		MessageSink {
	
	/**
	 * Holds OmsUtils.
	 */
	private TRURadialOMUtils mOmsUtils;
	
	/**
	 * Holds sendFulfillmentMessage
	 */
	private TRURadialOMSendFulfillmentMessage mSendFulfillmentMessage;
	
	/**
	 * Receives customer order update message and posts to OMS.
	 * @param pPortName - Port Name
	 * @param pMessage - Message
	 * @throws JMSException - if any
	 */
	@Override
	public void receiveMessage(String pPortName, Message pMessage) throws JMSException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRURadialOMOrderUpdateMessageSink method : receiveMessage");
		}
		ObjectMessage objMessage = null;
		if(pMessage != null){
			objMessage = (ObjectMessage) pMessage;
		}
		TRUCustomerOrderUpdateMessage customerOrderUpdateMessage =  null;
		if(objMessage != null && objMessage.getObject() instanceof TRUCustomerOrderUpdateMessage){
			customerOrderUpdateMessage =  (TRUCustomerOrderUpdateMessage) objMessage.getObject();
			String customerId = customerOrderUpdateMessage.getExternalCustomerId();
			String orderNumber = customerOrderUpdateMessage.getExternalOrderNumber();
			getSendFulfillmentMessage().sendRadialOrderUpdateMessage(orderNumber, customerId);
		}
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRURadialOMOrderUpdateMessageSink method : receiveMessage");
		}
	}

	/**
	 * This is to get the value for omsUtils
	 *
	 * @return the omsUtils value
	 */
	public TRURadialOMUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method to set the pOmsUtils with omsUtils
	 * 
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRURadialOMUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This is to get the value for sendFulfillmentMessage
	 *
	 * @return the sendFulfillmentMessage value
	 */
	public TRURadialOMSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method to set the pSendFulfillmentMessage with sendFulfillmentMessage
	 * 
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRURadialOMSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}
}
