package com.tru.jda.integration.droplet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.gfs.order.OrderDetailRequest;
import com.gfs.order.OrderDetailResponse;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jda.orderdetails.Fault;
import com.jda.orderdetails.OrderHeader;
import com.jda.orderdetails.OrderLookupResponse;
import com.jda.orderdetails.OrderLookupResponseList;
import com.tru.common.TRUConstants;
import com.tru.gfs.integration.TRUGFSUtils;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 * This Droplet is used to Call the OrderDetailLookup Service for Customer Order Details.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMOrderDetailLookupDroplet extends DynamoServlet {
	
	private static final String ORDER_DETAILS_INVALID_ZIP = "InvalidZip";

	/** The Constant ORDER_DETAILS_LOOKUP. */
	public static final String ORDER_DETAILS_LOOKUP = "OrderDetailsLookupService";

	/** The Constant ORDER_DETAILS_LOOKUP_SERVICE. */
	public static final String ORDER_DETAILS_LOOKUP_SERVICE = "TRURadialOMOrderDetailLookupDroplet";
	
	private static final String NULL_RESPONSE_CODE = "200";
	
	/** The Constant ORDER_ID. */
	public static final ParameterName ORDER_ID = ParameterName.getParameterName(TRURadialOMConstants.ORDER_ID);
	
	/** The Constant EMAIL. */
	public static final ParameterName EMAIL = ParameterName.getParameterName(TRURadialOMConstants.CUSTOMER_EMAIL);
	
	/** The Constant LAYAWAY_PAGE. */
	public static final ParameterName LAYAWAY_PAGE = ParameterName.getParameterName(TRURadialOMConstants.LAYAWAY_PAGE);
	
	public static final ParameterName POSTAL_CODE = ParameterName.getParameterName(TRURadialOMConstants.POSTAL_CODE);

	/** Holds the TRURadialOMUtils. */
	private TRURadialOMUtils mOmsUtils;
	
	/** This holds the reference for TRURadialOMConfiguration. */
	private TRURadialOMConfiguration mOmConfiguration;
	
	/** Holds The mGfsUtils. */
	private TRUGFSUtils mGfsUtils;
	
	/** Holds DummyGfsOrderDetailsresponseXML.. */
	private String mDummyGfsOrderDetailsresponseXML;
	
	/**  Holds JsonObject. */
	private String mXmlObject;
	
	/**  Holds mTestMode. */
	private boolean mTestMode;
	
	/**
	 * Gets the dummy GfsOrderDetailsResponse xml.
	 *
	 * @return mDummyGfsOrderDetailsresponseXML
	 */
	public String getDummyGfsOrderDetailsresponseXML() {
		return mDummyGfsOrderDetailsresponseXML;
	}
	
	/**
	 * Sets the dummy GfsOrderDetailsResponse xml.
	 *
	 * @param pDummyGfsOrderDetailsresponseXML - the mDummyGfsOrderDetailsresponseXML to set
	 */
	public void setDummyGfsOrderDetailsresponseXML(String pDummyGfsOrderDetailsresponseXML) {
		this.mDummyGfsOrderDetailsresponseXML = pDummyGfsOrderDetailsresponseXML;
	}
	/**
	 * This method returns the GFSUtils value.
	 * @return mGFSUtils
	 */
	public TRUGFSUtils getGfsUtils() {
		return mGfsUtils;
	}
	
	
	/**
	 * Sets the GfsUtils.
	 * @param pGfsUtils the new GfsUtils
	 */
	public void setGfsUtils(TRUGFSUtils pGfsUtils) {
		this.mGfsUtils = pGfsUtils;
	}
		
	/**
	 * Checks if is test mode.
	 * @return mTestMode
	 */
	public boolean isTestMode() {
		return mTestMode;
	}
	
	/**
	 * Sets the test mode.
	 * @param pTestMode - the mTestMode to set
	 */
	public void setTestMode(boolean pTestMode) {
		this.mTestMode = pTestMode;
	}
	
	/**
	 * This service method is to call the order detail lookup service to display
	 * the order details in my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRURadialOMOrderDetailLookupDroplet method : service");
		}
		String orderId = (String) pRequest.getLocalParameter(ORDER_ID);
		String postalCode = (String) pRequest.getLocalParameter(POSTAL_CODE);
		final Profile profile =(Profile) pRequest.getObjectParameter(TRUConstants.PROFILE);
		String jsonResponseString = null;
		Gson gson = new Gson();
		JsonObject customerOrder = new JsonObject();
		Map<String,Object> formatedObj=null;
		String laywayPage = (String) pRequest.getLocalParameter(LAYAWAY_PAGE);
		TRURadialOMConfiguration omConfiguration = getOmConfiguration();
		String responseXML = null;
		JsonObject orderDetailLookupResponseArrayObject=null;
		if (isLoggingDebug()) {
			vlogDebug("Order id for OrderDetails : {0} | laywaypage :{1} ", orderId, laywayPage);
		}
		Object orderDetailLookupResponseObject = null;
		long startTime = 0;
		long endTime = 0;
	if(StringUtils.isNotBlank(orderId) && StringUtils.isNumericOnly(orderId)){
		if ((TRURadialOMConstants.TRUE).equalsIgnoreCase(laywayPage)) {
			OrderDetailResponse gfsOrderDetailLookupResponse =null;
			if(isTestMode()){
				orderDetailLookupResponseObject =getDummyGfsOrderDetailsresponseXML();
			} else{
				OrderDetailRequest gfsOrderDetailLookupRequest = getGfsUtils().populateOrderDetailRequest(orderId);
				getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationEntered(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
						getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(TRUConstants.LAYAWAY_ORDER_DETAIL), orderId, profile.getRepositoryId());
				startTime = Calendar.getInstance().getTimeInMillis();
				gfsOrderDetailLookupResponse = getGfsUtils().callOrderDetailService(gfsOrderDetailLookupRequest);
				endTime = Calendar.getInstance().getTimeInMillis();
				
				if(gfsOrderDetailLookupResponse != null){
					getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationExited(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
							getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(TRUConstants.LAYAWAY_ORDER_DETAIL), orderId, profile.getRepositoryId());	
				}
				orderDetailLookupResponseObject = getGfsUtils().parseOrderDetailResponse(gfsOrderDetailLookupResponse, startTime, endTime);
			}
			JsonParser parser = new JsonParser();
			JsonElement parse = parser.parse((String)orderDetailLookupResponseObject);
			JsonObject asJsonObject = parse.getAsJsonObject();
			formatedObj= getOmsUtils().jsonToMap(asJsonObject);
			customerOrder.add(getOmConfiguration().getCustomerOrderProperty(),asJsonObject);
			jsonResponseString=gson.toJson(customerOrder);
			if(isLoggingDebug()){
				vlogDebug("Layaway Order Detail Map  : {0}", formatedObj);
			}
		}
		else{
			if(isTestMode()){
				responseXML = getXmlObject();
				orderDetailLookupResponseArrayObject = getOmsUtils().parseOrderDetailLookupResponse(responseXML);
				formatedObj= getOmsUtils().jsonToMap(orderDetailLookupResponseArrayObject);
				customerOrder.add(getOmConfiguration().getCustomerOrderProperty(), orderDetailLookupResponseArrayObject);
				jsonResponseString=gson.toJson(customerOrder);
			}
			else{
				String orderDetailLookupRequest = null;
				String orderLookupXSDPackage = omConfiguration.getOrderDetailsLookupXSDPackage();
				orderDetailLookupRequest = getOmsUtils().populateOrderDetailLookupRequest(orderId, orderLookupXSDPackage);
				if (isLoggingDebug()) {
					vlogDebug("Order Detail Request XML : {0}", orderDetailLookupRequest);
				}
				boolean isLoggedIn = !profile.isTransient();
				if(isLoggedIn){
				startTime = Calendar.getInstance().getTimeInMillis();
				getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationEntered(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS),
						getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(ORDER_DETAILS_LOOKUP), orderId, profile.getRepositoryId());
				orderDetailLookupResponseObject = getOmsUtils().callOrderService(orderDetailLookupRequest,orderLookupXSDPackage ,omConfiguration.getOrderDetailsLookupServiceURL());
				endTime = Calendar.getInstance().getTimeInMillis();
				}
				else{
					if(StringUtils.isBlank(postalCode) || postalCode.length() < TRUConstants.FIVE){
						getOrderDetailsForInvalidPostal(pRequest, pResponse, startTime, endTime);
						return;
					}
					else{
					startTime = Calendar.getInstance().getTimeInMillis();
					getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationExited(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS),
							getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(ORDER_DETAILS_LOOKUP), orderId, profile.getRepositoryId());
					orderDetailLookupResponseObject = getOmsUtils().callOrderService(orderDetailLookupRequest,orderLookupXSDPackage ,omConfiguration.getOrderDetailsLookupServiceURL());
					endTime = Calendar.getInstance().getTimeInMillis();
						}
					}
				if(orderDetailLookupResponseObject != null){
				if(orderDetailLookupResponseObject instanceof Fault){
					Fault faultResponse = (Fault) orderDetailLookupResponseObject;
					responseXML = getOmsUtils().marshalRequestXML(faultResponse, orderLookupXSDPackage);
					jsonResponseString = getOmsUtils().getOmConfiguration().getInvalidOrderIDDummyOrderDetailsJson();
					formatedObj = getOmsUtils().convertStringJson(jsonResponseString);
					getOmsUtils().sendAlertLogFailure(ORDER_DETAILS_LOOKUP_SERVICE, ORDER_DETAILS_LOOKUP, ORDER_DETAILS_LOOKUP, faultResponse.getCode(), getOmConfiguration().getServiceError(), startTime, endTime);
				} 
				else {
					getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationExited(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS),
							getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(ORDER_DETAILS_LOOKUP), orderId, profile.getRepositoryId());
					JAXBElement<OrderLookupResponseList> lookupResponseList = (JAXBElement<OrderLookupResponseList>) orderDetailLookupResponseObject;
					OrderLookupResponseList lookupResponse = lookupResponseList.getValue();
					OrderLookupResponse orderLookupResponse = lookupResponse.getOrderLookupResponse();
					OrderHeader orderHeader = orderLookupResponse.getOrderHeader();
					if(isLoggingDebug()){
						vlogDebug("Order details response order header : {0}", orderHeader);
					}
					if(!isLoggedIn && StringUtils.isNotBlank(postalCode)){
						String orderPostalCode=orderHeader.getBillingAddress().getPostalCode();
						if(orderPostalCode.length() > TRUConstants.FIVE && postalCode.length() == TRUConstants.FIVE){
							orderPostalCode = orderPostalCode.substring(TRUConstants.ZERO, TRUConstants.FIVE);
						}
						if(!postalCode.equalsIgnoreCase(orderPostalCode)){
							getOrderDetailsForInvalidPostal(pRequest, pResponse, startTime, endTime);
							return;
						}
					}
					responseXML = getOmsUtils().marshalRequestXML(lookupResponseList, orderLookupXSDPackage);
					orderDetailLookupResponseArrayObject = getOmsUtils().parseOrderDetailLookupResponse(responseXML);
					formatedObj= getOmsUtils().jsonToMap(orderDetailLookupResponseArrayObject);
					customerOrder.add(getOmConfiguration().getCustomerOrderProperty(),orderDetailLookupResponseArrayObject);
					jsonResponseString=gson.toJson(customerOrder);
					}
				}
				else{
					jsonResponseString = getOmConfiguration().getOrderDetailsServiceDownDummyJson();
					formatedObj = getOmsUtils().convertStringJson(jsonResponseString);
					getOmsUtils().sendAlertLogFailure(ORDER_DETAILS_LOOKUP_SERVICE, ORDER_DETAILS_LOOKUP, ORDER_DETAILS_LOOKUP, NULL_RESPONSE_CODE, getOmConfiguration().getServiceDown(), startTime, endTime);
				}
			}
		}
	}
	else{
		jsonResponseString = getOmsUtils().getOmConfiguration().getInvalidOrderIDDummyOrderDetailsJson();
		formatedObj = getOmsUtils().convertStringJson(jsonResponseString);
		}
		if(isLoggingDebug()){
			vlogDebug("Order Detail lookup response object  : {0}", jsonResponseString);
			vlogDebug("Order Detail lookup response object  : {0}", formatedObj);
		}
		
		pRequest.setParameter(TRURadialOMConstants.RESPONSE, jsonResponseString);
		pRequest.setParameter(TRURadialOMConstants.MOBILE_RESPONSE, formatedObj);
		pRequest.serviceLocalParameter(TRURadialOMConstants.OUTPUT, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRURadialOMOrderDetailLookupDroplet method : service");
		}
	}

	/**
	 * This method is to parse the order lookup response.
	 *
	 * @param pRequest  - DynamoHttpServletRequest
	 * @param pResponse -DynamoHttpServletResponse
	 * @param pStartTime - operation TimeStamp
	 * @param pEndTime - operation endTime
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	public void getOrderDetailsForInvalidPostal(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, long pStartTime, long pEndTime) throws ServletException, IOException {
		String jsonResponseString;
		Map<String, Object> formatedObj;
		jsonResponseString = getOmsUtils().getOmConfiguration().getOrderDetailsJsonForInvalidZip();
		formatedObj = getOmsUtils().convertStringJson(jsonResponseString);
		pRequest.setParameter(TRURadialOMConstants.RESPONSE, jsonResponseString);
		pRequest.setParameter(TRURadialOMConstants.MOBILE_RESPONSE, formatedObj);
		pRequest.serviceLocalParameter(TRURadialOMConstants.OUTPUT, pRequest, pResponse);
		getOmsUtils().sendAlertLogFailure(ORDER_DETAILS_LOOKUP_SERVICE, ORDER_DETAILS_LOOKUP, ORDER_DETAILS_INVALID_ZIP, NULL_RESPONSE_CODE, ORDER_DETAILS_INVALID_ZIP, pStartTime, pEndTime);
	}
	
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRURadialOMUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRURadialOMUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	
	/**
	 * This is to get the value for omConfiguration.
	 *
	 * @return the omConfiguration value
	 */
	public TRURadialOMConfiguration getOmConfiguration() {
		return mOmConfiguration;
	}
	
	/**
	 * This method to set the pOmConfiguration with omConfiguration.
	 *
	 * @param pOmConfiguration the omConfiguration to set
	 */
	public void setOmConfiguration(TRURadialOMConfiguration pOmConfiguration) {
		mOmConfiguration = pOmConfiguration;
	}

	/**
	 * Gets the xml object.
	 *
	 * @return the xml object
	 */
	public String getXmlObject() {
		return mXmlObject;
	}

	/**
	 * Sets the xml object.
	 *
	 * @param pXmlObject the new xml object
	 */
	public void setXmlObject(String pXmlObject) {
		 mXmlObject = pXmlObject;
	}
}