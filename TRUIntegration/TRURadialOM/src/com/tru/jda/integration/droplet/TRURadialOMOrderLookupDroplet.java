package com.tru.jda.integration.droplet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.xml.bind.JAXBElement;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.gfs.order.OrderSummaryLookupRequest;
import com.gfs.order.OrderSummaryLookupResponse;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jda.orderlist.CustomerLookupResponse;
import com.jda.orderlist.Fault;
import com.tru.common.TRUConstants;
import com.tru.gfs.integration.TRUGFSUtils;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 * This droplet is to Call the order list service.
 * 
 * @version 1.0
 * @author Professional Access
 */
public class TRURadialOMOrderLookupDroplet extends DynamoServlet {
	
	/**
	 * Holds constant NULL_RESPONSE_CODE .
	 */
	private static final String NULL_RESPONSE_CODE = "200";

	/**
	 * Holds constant ORDER_LOOKUP .
	 */
	public static final String ORDER_LOOKUP = "OrderLookupService";

	/**
	 * Holds constant ORDER_LOOKUP_SERVICE .
	 */
	public static final String ORDER_LOOKUP_SERVICE = "TRURadialOMOrderLookupDroplet";

	/**
	 * Parameter Name to hold productList.
	 */
	public static final ParameterName CUSTOMER_ID = ParameterName.getParameterName("customerId");
	/**
	 * Parameter Name to hold email.
	 */
	public static final ParameterName EMAIL = ParameterName.getParameterName("email");
	/**
	 * Parameter Name to hold layawayPage.
	 */
	public static final ParameterName LAYAWAY_PAGE = ParameterName.getParameterName("layawayPage");

	/**
	 * Holds constant RESPONSE.
	 */
	public static final String RESPONSE = "response";

	/**
	 * Holds OmsUtils.
	 */
	private TRURadialOMUtils mOmsUtils;


	/**
	 * Holds OmsEnable.
	 */
	private boolean mOmsTestModeEnable;
	
	/**
	 * Holds OmsDummyProfileID.
	 */
	private String mOmsDummyProfileID;

	/**
	 * Holds DummyOrderResponse.
	 */
	private String mDummyLayawayOrderResponse;

	/**
	 * Holds dummyLayawayEmail
	 */
	private String mDummyLayawayEmail;	
	/**
	 * 
	 * @return mDummyLayawayOrderResponse
	 */
	public String getDummyLayawayOrderResponse() {
		return mDummyLayawayOrderResponse;
	}
	/**
	 * 
	 * @param pDummyLayawayOrderResponse
	 * 				the mDummyLayawayOrderResponse to set
	 */
	public void setDummyLayawayOrderResponse(String pDummyLayawayOrderResponse) {
		this.mDummyLayawayOrderResponse = pDummyLayawayOrderResponse;
	}

	/** This holds the reference for tru configuration */
	private TRURadialOMConfiguration mConfiguration;

	/**
	 * Holds DummyOrderResponse.
	 */
	private String mDummyOrderResponse;
	
	
	/**
	 * This method returns the omsDummyProfileID value.
	 * 
	 * @return the omsDummyProfileID
	 */
	public String getOmsDummyProfileID() {
		return mOmsDummyProfileID;
	}

	/**
	 * This method sets the OmsDummyProfileID with parameter value pOmsDummyProfileID.
	 * 
	 * @param pOmsDummyProfileID
	 *            the omsDummyProfileID to set
	 */
	public void setOmsDummyProfileID(String pOmsDummyProfileID) {
		this.mOmsDummyProfileID = pOmsDummyProfileID;
	}

	/**
	 * This method returns the omsUtils value.
	 * 
	 * @return the omsUtils
	 */
	public TRURadialOMUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 * 
	 * @param pOmsUtils
	 *            the omsUtils to set
	 */
	public void setOmsUtils(TRURadialOMUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}

	/**
	 * This method returns the OmsTestModeEnable value.
	 * 
	 * @return the OmsTestModeEnable
	 */
	public boolean isOmsTestModeEnable() {
		return mOmsTestModeEnable;
	}

	/**
	 * @param pOmsTestModeEnable
	 *            the mOmsTestModeEnable to set
	 */
	public void setOmsTestModeEnable(boolean pOmsTestModeEnable) {
		this.mOmsTestModeEnable = pOmsTestModeEnable;
	}

	/**
	 * @return the mDummyOrderResponse
	 */
	public String getDummyOrderResponse() {
		return mDummyOrderResponse;
	}

	/**
	 * @param pDummyOrderResponse
	 *            the mDummyOrderResponse to set
	 */
	public void setDummyOrderResponse(String pDummyOrderResponse) {
		this.mDummyOrderResponse = pDummyOrderResponse;
	}
	
	/**
	 * @return the configuration
	 */
	public TRURadialOMConfiguration getConfiguration() {
		return mConfiguration;
	}

	/**
	 * @param pConfiguration
	 *            the configuration to set
	 */
	public void setConfiguration(TRURadialOMConfiguration pConfiguration) {
		mConfiguration = pConfiguration;
	}
	/**
	 * Holds GFSUtils.
	 */
	private TRUGFSUtils mGfsUtils;
	
	/**
	 * This method returns the GFSUtils value.
	 * @return mGFSUtils
	 */
	public TRUGFSUtils getGfsUtils() {
		return mGfsUtils;
	}
	
	/**
	 * @param pGfsUtils
	 * 			- the mGfsUtils to set
	 */
	public void setGfsUtils(TRUGFSUtils pGfsUtils) {
		this.mGfsUtils = pGfsUtils;
	}

	/**
	 * This service method is to call the customer order list service if
	 * customer views orders from my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		String customerId = (String) pRequest.getLocalParameter(CUSTOMER_ID);
		String customerEmail = (String) pRequest.getLocalParameter(EMAIL);
		String layawayPage = (String) pRequest.getLocalParameter(LAYAWAY_PAGE);
		OrderSummaryLookupRequest orderSummaryLookupRequest  = null;
		OrderSummaryLookupResponse orderSummaryLookupResponse = null;
		Object customerOrderLookuresponse = null;
		String requestXML = null;
		Map<String,Object> formatedObj=null;
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRURadialOMOrderLookupDroplet method : service");
			vlogDebug("CustomerId :{0} ", customerId);
		}
		long startTime = 0;
		long endTime = 0;
		//layawayPage Response
		if(TRUConstants.TRUE.equalsIgnoreCase(layawayPage)) {
			if (isOmsTestModeEnable()) {
				if (!StringUtils.isBlank(getDummyLayawayEmail())) {
					orderSummaryLookupRequest = getGfsUtils().populateOrderSummaryLookupRequest(getDummyLayawayEmail());
					startTime = Calendar.getInstance().getTimeInMillis();
					orderSummaryLookupResponse = getGfsUtils().callOrderSummaryLookupService(orderSummaryLookupRequest);
					endTime = Calendar.getInstance().getTimeInMillis();
					customerOrderLookuresponse = getGfsUtils().parseOrderSummaryLookupResponse(orderSummaryLookupResponse, startTime, endTime);
				} 
				else {
					customerOrderLookuresponse = getDummyLayawayOrderResponse();
				}
			} else {
				getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationEntered(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
						getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(TRUConstants.LAYAWAY_ORDER_HISTORY), null, customerId);
				orderSummaryLookupRequest = getGfsUtils().populateOrderSummaryLookupRequest(customerEmail);
				startTime = Calendar.getInstance().getTimeInMillis();
				orderSummaryLookupResponse = getGfsUtils().callOrderSummaryLookupService(orderSummaryLookupRequest);
				
				if(orderSummaryLookupResponse != null){
					getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationExited(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
							getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(TRUConstants.LAYAWAY_ORDER_HISTORY), null, customerId);
				}
				endTime = Calendar.getInstance().getTimeInMillis();
				customerOrderLookuresponse = getGfsUtils().parseOrderSummaryLookupResponse(orderSummaryLookupResponse, startTime, endTime);
			}
		}
		//Order Lookup Response
		else {
			Object orderCreateServiceResponseObj = null;    
			TRURadialOMConfiguration configuration = getConfiguration();
			if(isOmsTestModeEnable()) {
				if (StringUtils.isNotBlank(getOmsDummyProfileID())) {
					requestXML = getOmsUtils().populateOrderLookupRequest(getOmsDummyProfileID(), configuration.getOrderLookupXSDpackage());
					startTime = Calendar.getInstance().getTimeInMillis();
					orderCreateServiceResponseObj = getOmsUtils().callOrderService(requestXML, configuration.getOrderLookupXSDpackage(), configuration.getOrderLookupServiceURL());
					endTime = Calendar.getInstance().getTimeInMillis();
					customerOrderLookuresponse = getCustomerOrderLookupResponse(orderCreateServiceResponseObj, configuration, startTime, endTime);
				}
				else {
					String xmlResponse = getDummyOrderResponse();
					customerOrderLookuresponse = getOmsUtils().parseOrderLookupResponse(xmlResponse);
				}
			} else {
				requestXML = getOmsUtils().populateOrderLookupRequest(customerId, configuration.getOrderLookupXSDpackage());
				startTime = Calendar.getInstance().getTimeInMillis();
				getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationEntered(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
						getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(ORDER_LOOKUP), null, customerId);
				orderCreateServiceResponseObj = getOmsUtils().callOrderService(requestXML, configuration.getOrderLookupXSDpackage(), configuration.getOrderLookupServiceURL());
				if(orderCreateServiceResponseObj != null){
					getGfsUtils().getOmUtils().getIntegrationInfoLogger().logIntegrationExited(getGfsUtils().getOmUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
							getGfsUtils().getOmUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(ORDER_LOOKUP), null, customerId);
				}
				endTime = Calendar.getInstance().getTimeInMillis();
				customerOrderLookuresponse = getCustomerOrderLookupResponse(orderCreateServiceResponseObj, configuration, startTime, endTime);
			}
		}
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse((String) customerOrderLookuresponse);
		JsonObject asJsonObject = parse.getAsJsonObject();
		formatedObj= getOmsUtils().jsonToMap(asJsonObject);
		if(isLoggingDebug()){
			vlogDebug("Order Cancel response Map  : {0}", formatedObj);
		}
		if (isLoggingDebug()) {
			vlogDebug("Customer Order look up XML : {0}", requestXML);
			vlogDebug("JSON Formatted response is : {0}", customerOrderLookuresponse);
		}
		pRequest.setParameter(RESPONSE, customerOrderLookuresponse);
		pRequest.setParameter(TRURadialOMConstants.MOBILE_RESPONSE, formatedObj);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRURadialOMOrderLookupDroplet method : service()");
		}
	}
	
	/**
	 * Gets the customer order lookup response.
	 *
	 * @param pOrderCreateServiceResponseObj the order create service response obj
	 * @param pConfiguration the configuration
	 * @param pStartTime the startTime
	 * @param pEndTime the endTime
	 * @return the customer order lookup response
	 */
	@SuppressWarnings("unchecked")
	public Object getCustomerOrderLookupResponse(Object pOrderCreateServiceResponseObj, TRURadialOMConfiguration pConfiguration, long pStartTime, long pEndTime) {
		Object customerOrderLookuresponse = null;
		if(pOrderCreateServiceResponseObj != null){
			if (pOrderCreateServiceResponseObj instanceof Fault) {
			customerOrderLookuresponse = getOmsUtils().getOmConfiguration().getInvalidCustomerIDDummyOrderListJson();			
			getOmsUtils().sendAlertLogFailure(ORDER_LOOKUP_SERVICE, ORDER_LOOKUP, ORDER_LOOKUP,((Fault) pOrderCreateServiceResponseObj).getCode(), getConfiguration().getServiceError(), pStartTime, pEndTime);
			} 
			else{
			JAXBElement<CustomerLookupResponse> lookupResponse = (JAXBElement<CustomerLookupResponse>) pOrderCreateServiceResponseObj;
			String responseXML = getOmsUtils().marshalRequestXML(lookupResponse, pConfiguration.getOrderLookupXSDpackage());
			customerOrderLookuresponse = getOmsUtils().parseOrderLookupResponse(responseXML);
				}
		}
		else{
			customerOrderLookuresponse = getConfiguration().getOrderLookupServiceDownDummyJson();
			getOmsUtils().sendAlertLogFailure(ORDER_LOOKUP_SERVICE, ORDER_LOOKUP, ORDER_LOOKUP, NULL_RESPONSE_CODE, getConfiguration().getServiceDown(), pStartTime, pEndTime);
			}
		return customerOrderLookuresponse;
	}

	/**
	 * This is to get the value for dummyLayawayEmail
	 *
	 * @return the dummyLayawayEmail value
	 */
	public String getDummyLayawayEmail() {
		return mDummyLayawayEmail;
	}
	/**
	 * This method to set the pDummyLayawayEmail with dummyLayawayEmail
	 * 
	 * @param pDummyLayawayEmail the dummyLayawayEmail to set
	 */
	public void setDummyLayawayEmail(String pDummyLayawayEmail) {
		mDummyLayawayEmail = pDummyLayawayEmail;
	}
}