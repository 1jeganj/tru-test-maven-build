package com.tru.jda.integration.droplet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.ServletException;
import javax.xml.bind.JAXBElement;

import atg.commerce.CommerceException;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jda.ordercancel.Fault;
import com.jda.ordercancel.OrderCancelResponse;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.utils.TRURadialOMUtils;

/**
 *This droplet is to Call the cancel order service if customer cancels the order.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMOrderCancelDroplet extends DynamoServlet {
	
	private static final String NULL_RESPONSE_CODE = "200";
	private static final String CANCEL_ORDER_SERVICE = "CancelOrderService";
	private static final String CANCEL_ORDER_SERVICE_PROCESS = "TRURadialOMOrderCancelDroplet";
	/**
	 * Holds constant RESPONSE.
	 */
	public static final String RESPONSE = "response";
	/**
	 * Holds constant SUCCESS.
	 */
	public static final String SUCCESS = "Success";
	/**
	 * Parameter Name to hold orderId.
	 */
	public static final ParameterName ORDER_ID = ParameterName.getParameterName("orderId");

	/**
	 * Parameter Name to hold email.
	 */
	public static final ParameterName EMAIL = ParameterName.getParameterName("email");
	/**
	 * Holds OmsUtils.
	 */
	private TRURadialOMUtils mOmsUtils;
	
	
	/** This holds the reference for tru configuration. */
	private TRURadialOMConfiguration mOmConfiguration;
	
	/**
	 * This service method is to call the customer order cancel service if
	 * customer chooses to cancel the order from my account.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest,	DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRURadialOMOrderCancelDroplet method : service");
		}
		String responseXML = null;
		Object orderCancelResponseObject = null;
		Map<String,Object> formatedObj=null;
		Gson gson = new Gson();
		JsonObject customerOrder = new JsonObject();
		String orderId = (String) pRequest.getLocalParameter(ORDER_ID);
		String email = (String) pRequest.getLocalParameter(EMAIL);
		if (isLoggingDebug()) {
			vlogDebug("Order id for cancel : {0}", orderId);
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		String cancelRequestXML = getOmsUtils().populateOrderCancelRequest(orderId, configuration.getOrderCancelXSDPath());
		if(isLoggingDebug()){
			vlogDebug("Order Cancel Request XML : {0}", cancelRequestXML);
		}
		final Profile profile =(Profile) pRequest.getObjectParameter(TRUConstants.PROFILE);		
		boolean isGuest = profile.isTransient();
		if(isGuest) {
			if(StringUtils.isBlank(email) || StringUtils.isBlank(orderId)  ){
				getInvalidCancelOrderAttempt(pRequest, pResponse, getOmConfiguration().getInvalidCancelOrderAttemptJson());
				return;
			}
			else {
				try {
					TRUOrderImpl order = (TRUOrderImpl) getOmsUtils().getOrderManager().loadOrder(orderId);
					String customerEmail  = order.getEmail();
					if(!customerEmail.trim().equalsIgnoreCase(email.trim())){
						getInvalidCancelOrderAttempt(pRequest, pResponse, getOmConfiguration().getInvalidCancelOrderAttemptJson());
						return;					
					}
				}
				catch (CommerceException e) {
					if (isLoggingError()) {
						logError("Exception while loading the order for order number : " + orderId, e);
					}
					getInvalidCancelOrderAttempt(pRequest, pResponse, getOmConfiguration().getInvalidCancelOrderJson());
					return;
				}
			}
		}
		//Call Cancel order Service
		getOmsUtils().getIntegrationInfoLogger().logIntegrationEntered(getOmsUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
				getOmsUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(CANCEL_ORDER_SERVICE), orderId, profile.getRepositoryId());
		long startTime = Calendar.getInstance().getTimeInMillis();
		orderCancelResponseObject = getOmsUtils().callOrderService(cancelRequestXML, configuration.getOrderCancelXSDPath(), configuration.getOrderCancelServiceURL());
		long endTime = Calendar.getInstance().getTimeInMillis();
		if(orderCancelResponseObject != null){
			if( orderCancelResponseObject instanceof Fault){
				Fault faultResponse = (Fault) orderCancelResponseObject;
				responseXML = getOmsUtils().marshalRequestXML(faultResponse, configuration.getOrderCancelXSDPath());
				getOmsUtils().sendAlertLogFailure(CANCEL_ORDER_SERVICE_PROCESS, CANCEL_ORDER_SERVICE, CANCEL_ORDER_SERVICE, faultResponse.getCode(), getOmConfiguration().getServiceError(), startTime, endTime);
			} else {
				JAXBElement<OrderCancelResponse> orderCancelObj = (JAXBElement<OrderCancelResponse>) orderCancelResponseObject;
				responseXML = getOmsUtils().marshalRequestXML(orderCancelObj, configuration.getOrderCancelXSDPath());
				getOmsUtils().getIntegrationInfoLogger().logIntegrationExited(getOmsUtils().getIntegrationInfoLogger().getServiceNameMap().get(TRUConstants.RADIAL_OMS), 
						getOmsUtils().getIntegrationInfoLogger().getInterfaceNameMap().get(CANCEL_ORDER_SERVICE), orderId, profile.getRepositoryId());
				}
		} else {
			responseXML = getServiceDownCancelResponse();
			getOmsUtils().sendAlertLogFailure(CANCEL_ORDER_SERVICE_PROCESS, CANCEL_ORDER_SERVICE, CANCEL_ORDER_SERVICE, NULL_RESPONSE_CODE, getOmConfiguration().getServiceDown(), startTime, endTime);
		}
		if(isLoggingDebug()){
			vlogDebug("Order cancel response object  : {0}", responseXML);
		}
		String cancelJsonResponse = getOmsUtils().convertXMLtoJSON(responseXML);
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(cancelJsonResponse);
		JsonObject asJsonObject = parse.getAsJsonObject();
		formatedObj= getOmsUtils().jsonToMap(asJsonObject);
		customerOrder.add(getOmConfiguration().getCustomerOrderProperty(),asJsonObject);
		cancelJsonResponse=gson.toJson(customerOrder);
		if(isLoggingDebug()){
			vlogDebug("Order Cancel response Map  : {0}", formatedObj);
		}
		pRequest.setParameter(RESPONSE, cancelJsonResponse);
		pRequest.setParameter(TRURadialOMConstants.MOBILE_RESPONSE, formatedObj);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
		if(isLoggingDebug()){
			logDebug("Exit from Class : TRURadialOMOrderCancelDroplet method : service");
		}
	}
	
	/**
	 * This  method is to send cancel order response for invalid attempt
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @param pInvalidCancelResponse
	 *            - Dummy cancel response for Invalid inputs        
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	private void getInvalidCancelOrderAttempt(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pInvalidCancelResponse) throws ServletException,
			IOException {
		Map<String, Object> formatedObj;
		formatedObj = getOmsUtils().convertStringJson(pInvalidCancelResponse);
		pRequest.setParameter(RESPONSE, pInvalidCancelResponse);
		pRequest.setParameter(TRURadialOMConstants.MOBILE_RESPONSE, formatedObj);
		pRequest.serviceLocalParameter(TRUConstants.OUTPUT, pRequest, pResponse);
	}
	/**
	 * This method returns the omsUtils value.
	 *
	 * @return the omsUtils
	 */
	public TRURadialOMUtils getOmsUtils() {
		return mOmsUtils;
	}

	/**
	 * This method sets the omsUtils with parameter value pOmsUtils.
	 *
	 * @param pOmsUtils the omsUtils to set
	 */
	public void setOmsUtils(TRURadialOMUtils pOmsUtils) {
		mOmsUtils = pOmsUtils;
	}
	/**
	 * This is to get the value for omConfiguration
	 *
	 * @return the omConfiguration value
	 */
	public TRURadialOMConfiguration getOmConfiguration() {
		return mOmConfiguration;
	}
	/**
	 * This method to set the pOmConfiguration with omConfiguration
	 * 
	 * @param pOmConfiguration the omConfiguration to set
	 */
	public void setOmConfiguration(TRURadialOMConfiguration pOmConfiguration) {
		mOmConfiguration = pOmConfiguration;
	}
	
	/**
	 * Holds value for mServiceDownCancelResponse.
	 */
	private String mServiceDownCancelResponse;

	/**
	 * This is to get the value for mServiceDownCancelResponse
	 *
	 * @return the mServiceDownCancelResponse value
	 */
	public String getServiceDownCancelResponse() {
		return mServiceDownCancelResponse;
	}

	/**
	 * This method to set the pServiceDownCancelResponse with omConfiguration
	 * 
	 * @param pServiceDownCancelResponse the mServiceDownCancelResponse to set
	 */
	public void setServiceDownCancelResponse(String pServiceDownCancelResponse) {
		this.mServiceDownCancelResponse = pServiceDownCancelResponse;
	}
	
	
}
