package com.tru.jda.integration.utils;


import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePayment;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupNotFoundException;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingTools;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.promotion.TRUPromotionTools;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.payment.PaymentStatus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.jda.createOrder.Address;
import com.jda.createOrder.Authorization;
import com.jda.createOrder.BrowserData;
import com.jda.createOrder.Context;
import com.jda.createOrder.CreditCard;
import com.jda.createOrder.CustomAttribute;
import com.jda.createOrder.CustomAttributes;
import com.jda.createOrder.Customer;
import com.jda.createOrder.CustomerName;
import com.jda.createOrder.CustomizationData;
import com.jda.createOrder.CustomizationList;
import com.jda.createOrder.Description;
import com.jda.createOrder.Discount;
import com.jda.createOrder.ExtendedAuthorizationResponseCodes;
import com.jda.createOrder.HTTPAcceptData;
import com.jda.createOrder.Hours;
import com.jda.createOrder.ItemFee;
import com.jda.createOrder.ItemFeeList;
import com.jda.createOrder.Merchandise;
import com.jda.createOrder.Message;
import com.jda.createOrder.ObjectFactory;
import com.jda.createOrder.OrderCreateRequest;
import com.jda.createOrder.OrderItem;
import com.jda.createOrder.OrderItems;
import com.jda.createOrder.OrderRequest;
import com.jda.createOrder.PayPal;
import com.jda.createOrder.PayPalPayerInfo;
import com.jda.createOrder.PaymentAccountUniqueId;
import com.jda.createOrder.PaymentContext;
import com.jda.createOrder.Payments;
import com.jda.createOrder.PrePaidPayment;
import com.jda.createOrder.Pricing;
import com.jda.createOrder.PromotionalDiscounts;
import com.jda.createOrder.SessionInfo;
import com.jda.createOrder.Shipping;
import com.jda.createOrder.SourceId;
import com.jda.createOrder.StoredValueCard;
import com.jda.createOrder.TaxData;
import com.jda.createOrder.Taxes;
import com.jda.ordercancel.OrderCancelRequest;
import com.jda.orderdetails.OrderLookupRequestType;
import com.jda.orderdetails.OrderSearchType;
import com.jda.orderlist.CustomerLookupRequestType;
import com.jda.orderlist.CustomerSearchType;
import com.jda.orderupdate.OrderUpdateRequest;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.commerce.order.TRUBppItemInfo;
import com.tru.commerce.order.TRUChannelHardgoodShippingGroup;
import com.tru.commerce.order.TRUChannelInStorePickupShippingGroup;
import com.tru.commerce.order.TRUCommerceItemImpl;
import com.tru.commerce.order.TRUCommerceItemMetaInfo;
import com.tru.commerce.order.TRUCreditCard;
import com.tru.commerce.order.TRUDonationCommerceItem;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUGiftWrapCommerceItem;
import com.tru.commerce.order.TRUHardgoodShippingGroup;
import com.tru.commerce.order.TRUInStorePickupShippingGroup;
import com.tru.commerce.order.TRULayawayOrderImpl;
import com.tru.commerce.order.TRUOrderImpl;
import com.tru.commerce.order.TRUOrderTools;
import com.tru.commerce.order.TRUShippingGroupCommerceItemRelationship;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.commerce.order.payment.creditcard.TRUCreditCardStatus;
import com.tru.commerce.payment.applepay.TRUApplePay;
import com.tru.commerce.payment.applepay.TRUApplePayStatus;
import com.tru.commerce.payment.paypal.TRUPayPal;
import com.tru.commerce.pricing.TRUItemPriceInfo;
import com.tru.commerce.pricing.TRUOrderPriceInfo;
import com.tru.commerce.pricing.TRUPricingModelProperties;
import com.tru.commerce.pricing.TRUPricingTools;
import com.tru.commerce.pricing.TRUTaxPriceInfo;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.storelocator.tol.StoreLocatorPropertyManager;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRURadialOMUtils.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMUtils extends TRUBaseRadialOMUtils {

	/** The Constant PROMOTION_QUALIFIER_ITEM_LABEL. */
	public static final String PROMOTION_QUALIFIER_ITEM_LABEL = "qual";

	/** The Constant PROMOTION_TARGETER_ITEM_LABEL. */
	public static final String PROMOTION_TARGETER_ITEM_LABEL = "tar";

	/** Holds the pipe delimiter. */
	public static final String PIPE_DELIMETRE = "\\|";

	/** Holds the StoreLocationRepository. */
	private Repository mStoreLocationRepository;
	
	/** Holds the LocationPropertyManager. */
	private StoreLocatorPropertyManager mLocationPropertyManager;
	
	/** The m order repository. */
	private Repository mOrderRepository;

	/**
	 * Gets the order repository.
	 *
	 * @return the orderRepository
	 */
	public Repository getOrderRepository() {
		return mOrderRepository;
	}

	/**
	 * Sets the order repository.
	 *
	 * @param pOrderRepository the orderRepository to set
	 */
	public void setOrderRepository(Repository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}

	/**
	 * Gets the store location repository.
	 *
	 * @return the store location repository
	 */
	public Repository getStoreLocationRepository() {
		return mStoreLocationRepository;
	}

	/**
	 * Sets the store location repository.
	 *
	 * @param pStoreLocationRepository the new store location repository
	 */
	public void setStoreLocationRepository(Repository pStoreLocationRepository) {
		this.mStoreLocationRepository = pStoreLocationRepository;
	}

	/**
	 * Gets the location property manager.
	 *
	 * @return the locationPropertyManager
	 */
	public StoreLocatorPropertyManager getLocationPropertyManager() {
		return mLocationPropertyManager;
	}

	/**
	 * Sets the location property manager.
	 *
	 * @param pLocationPropertyManager - the locationPropertyManager to set
	 */
	public void setLocationPropertyManager(
			StoreLocatorPropertyManager pLocationPropertyManager) {
		this.mLocationPropertyManager = pLocationPropertyManager;
	}

	/**
	 * This method is to populate the order detail lookup request XML to call
	 * the service.
	 * 
	 * @param pOrderId
	 *            - Order Id
	 * @param pOrderDetailsLookupXSDPackage
	 *            - XSD package for marshaling object to XML format.
	 * @return requestXML - order details look up request XML
	 */
	public String populateOrderDetailLookupRequest(String pOrderId, String pOrderDetailsLookupXSDPackage) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderDetailLookupRequest()");
			vlogDebug("Order Id : {0} | pXSDPackage :{1}", pOrderId, pOrderDetailsLookupXSDPackage);
		}
		String requestXML = null;
		if(pOrderId != null){
			com.jda.orderdetails.ObjectFactory objectFactory = new com.jda.orderdetails.ObjectFactory();
			OrderLookupRequestType orderLookupRequest = objectFactory.createOrderLookupRequestType();
			OrderSearchType orderSearch = objectFactory.createOrderSearchType();
			orderSearch.getWebOrderID().add(pOrderId);
			orderLookupRequest.setOrderSearch(orderSearch);
			JAXBElement<OrderLookupRequestType> createOrderLookupRequest = objectFactory.createOrderLookupRequest(orderLookupRequest);
			//Marshal the request
			requestXML = marshalRequestXML(createOrderLookupRequest, pOrderDetailsLookupXSDPackage);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderDetailLookupRequest()");	
		}
		return requestXML;
	}

	/**
	 * This method is to generate the Order cancel request.
	 * 
	 * @param pOrderId
	 *            - Order Id
	 * @param pOrderCancelXSDPath
	 *            - Order Cancel XSD path
	 * @return requestXML - Request XML
	 */
	public String populateOrderCancelRequest(String pOrderId, String pOrderCancelXSDPath) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderCancelRequest()");
			vlogDebug("Customer Id : {0} | pXSDPackage :{1}", pOrderId, pOrderCancelXSDPath);
		}
		String requestXML = null;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		if(pOrderId != null){
			com.jda.ordercancel.ObjectFactory objectFactory = new com.jda.ordercancel.ObjectFactory();
			OrderCancelRequest orderCancelRequest = objectFactory.createOrderCancelRequest();
			orderCancelRequest.setCustomerOrderId(BigInteger.valueOf(Long.parseLong(pOrderId)));
			orderCancelRequest.setReasonCode(configuration.getOrderCancelReasonCode());
			orderCancelRequest.setReason(configuration.getOrderCancelReason());
			JAXBElement<OrderCancelRequest> orderCancelRequest2 = objectFactory.createOrderCancelRequest(orderCancelRequest);
			//Marshal the request
			requestXML = marshalRequestXML(orderCancelRequest2, pOrderCancelXSDPath);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderCancelRequest()");	
		}
		return requestXML;
	}

	/**
	 * This method is to populate the order look up request based on the ATG
	 * profile Id.
	 *
	 * @param pCustomerId
	 * 					- ATG profile Id
	 * @param pXSDPackage
	 * 					- XSD package
	 * @return the string
	 */
	public String populateOrderLookupRequest(String pCustomerId, String pXSDPackage){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderLookupRequest()");
			vlogDebug("Customer Id : {0} | pXSDPackage :{1}", pCustomerId, pXSDPackage);
		}
		String requestXML = null;
		if(pCustomerId != null){
			com.jda.orderlist.ObjectFactory objectFactory = new com.jda.orderlist.ObjectFactory();
			CustomerLookupRequestType customerLookupReqType = objectFactory.createCustomerLookupRequestType();
			CustomerSearchType customerSearch = objectFactory.createCustomerSearchType();
			customerSearch.setCustomerId(pCustomerId);
			customerLookupReqType.setCustomerSearch(customerSearch);
			JAXBElement<CustomerLookupRequestType> orderLookupRequest = objectFactory.createOrderLookupRequest(customerLookupReqType);
			//Marshal the request
			requestXML = marshalRequestXML(orderLookupRequest, pXSDPackage);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderLookupRequest()");	
		}
		return requestXML;
	}

	/**
	 * This method is to populate request for Order create service to post the
	 * order XML to Radial OM.
	 *
	 * @param pOrder the order
	 * @return the string
	 */
	public String populateOrderCreateRequestXML(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderCreateRequestXML()");
			vlogDebug("Order : {0}", pOrder);
		}
		String requestXML = null;
		if(pOrder != null){
			ObjectFactory objectFactory = new ObjectFactory();
			OrderCreateRequest createOrderCreateRequest = objectFactory.createOrderCreateRequest();
			OrderRequest orderRequestXMLObj = objectFactory.createOrderRequest();
			TRUOrderImpl order = (TRUOrderImpl) pOrder;
			//Populate order payment details
			populateOrderPayments(order, objectFactory, orderRequestXMLObj);
			//Populate Order Line Details
			populateOrderItemDetails(order, objectFactory, orderRequestXMLObj);
			//Populate Order Header Attributes
			populateOrderHeader(order, objectFactory, orderRequestXMLObj);
			//Populate Context Attributes
			populateOrderContextAttributes(order, objectFactory, createOrderCreateRequest);
			createOrderCreateRequest.setOrder(orderRequestXMLObj);
			JAXBElement<OrderCreateRequest> createOrderCreateRequest2 = objectFactory.createOrderCreateRequest(createOrderCreateRequest);
			requestXML = marshalRequestXML(createOrderCreateRequest2, getOmConfiguration().getOrderCreateXSDPackage());
		}
		if(isLoggingDebug()){
			vlogDebug("Order Create Request XML : {0} ", requestXML);
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderCreateRequestXML()");
		}
		return requestXML;
	}
	
	/**
	 * This method is to populate the Order update request XML.
	 * 
	 * @param pOrdeId
	 *            - Order Id
	 * @param pProfileId
	 *            - Profile Id
	 * @return requestXML - Request XML
	 */
	public String populateOrderUpdateRequest(String pOrdeId, String pProfileId){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderUpdateRequest()");
			vlogDebug("Order  : {0} Profile : {1} ", pOrdeId, pProfileId);
		}
		String requestXML = null;
		if(pOrdeId != null && pProfileId != null) {
			TRURadialOMConfiguration configuration = getOmConfiguration();
			com.jda.orderupdate.ObjectFactory objectFactory = new com.jda.orderupdate.ObjectFactory();
			OrderUpdateRequest orderUpdateRequest = objectFactory.createOrderUpdateRequest();
			orderUpdateRequest.setStoreCode(configuration.getSourceIdValue());
			com.jda.orderupdate.Customer customer = objectFactory.createCustomer();
			customer.setId(BigInteger.valueOf(Long.parseLong(pProfileId)));
			customer.setWebOrder(BigInteger.valueOf(Long.parseLong(pOrdeId)));
			orderUpdateRequest.setCustomer(customer);
			JAXBElement<OrderUpdateRequest> orderUpdateRequest2 = objectFactory.createOrderUpdateRequest(orderUpdateRequest);
			requestXML = marshalRequestXML(orderUpdateRequest2, configuration.getOrderUpdateXSDPackage());
			if(isLoggingDebug()){
				vlogDebug("Order Update Request XML : {0} ", requestXML);
				logDebug("Exit from class: TRURadialOMUtils method: populateOrderUpdateRequest()");
			}
		}
		return requestXML;
	}


	/**
	 * This method is to populate the order context attributes.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCreateOrderCreateRequest
	 *            - Order XML Object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderContextAttributes(Order pOrder,
			ObjectFactory pObjectFactory, OrderCreateRequest pCreateOrderCreateRequest) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderContextAttributes()");
			vlogDebug("Order : {0}", pOrder);
		}
		if(pOrder == null || pObjectFactory == null || pCreateOrderCreateRequest == null){
			return;
		}
		TRUOrderImpl order = null;
		if(pOrder instanceof TRUOrderImpl){
			order = (TRUOrderImpl) pOrder;
		}
		Context context = pObjectFactory.createContext();
		context.setLocale(getEncodedString(TRUConstants.LOCALE_EN_US));
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			context.setTdlOrderTimestamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		BrowserData browserData = pObjectFactory.createBrowserData();
		browserData.setHostName(getEncodedString(order.getHostName()));
		browserData.setIPAddress(getEncodedString(order.getCustomerIPAddress()));
		browserData.setSessionId(getEncodedString(order.getBrowserSessionId()));
		browserData.setUserAgent(getEncodedString(order.getBrowserID()));
		browserData.setConnection(getEncodedString(order.getBrowserConnection()));
		if(getOmConfiguration().isIncludeDummyCookie()){
			browserData.setCookies(getEncodedString(getOmConfiguration().getDummyCookie()));
		}else{
			browserData.setCookies(getEncodedString(order.getBrowserCookie()));
		}
		browserData.setJavascriptData(getEncodedString(order.getDeviceID()));
		if(getOmConfiguration().isIncludeJavaScript()){
			browserData.setJavascriptData(getEncodedString(getOmConfiguration().getJavascriptData()));
		}
		browserData.setReferrer(getEncodedString(order.getBrowserReferer()));
		HTTPAcceptData httpAcceptData = pObjectFactory.createHTTPAcceptData();
		httpAcceptData.setContentTypes(getEncodedString(order.getBrowserAccept()));
		httpAcceptData.setEncoding(getEncodedString(order.getBrowserAcceptEncoding()));
		httpAcceptData.setCharSet(getEncodedString(order.getBrowserAcceptCharset()));
		httpAcceptData.setLanguage(getEncodedString(order.getBrowserIdLanguageCode()));
		browserData.setHTTPAcceptData(httpAcceptData);
		context.setBrowserData(browserData);
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			for(PaymentGroup paymentGroup : paymentGroups){
				if(paymentGroup instanceof TRUPayPal){
					TRUPayPal paypalPG = (TRUPayPal) paymentGroup;
					PayPalPayerInfo payPalPayerInfo = pObjectFactory.createPayPalPayerInfo();
					payPalPayerInfo.setPayPalPayerID(getEncodedString(paypalPG.getPayerId()));
					payPalPayerInfo.setPayPalPayerStatus(getEncodedString(getOmConfiguration().getPaypalPayerStatus()));
					payPalPayerInfo.setPayPalAddressStatus(getEncodedString(getOmConfiguration().getPaypalAddressStatus()));//Need to update
					context.setPayPalPayerInfo(payPalPayerInfo);
				}
			}
		}
		SessionInfo sessionInfo = pObjectFactory.createSessionInfo();
		sessionInfo.setTimeSpentOnSite(getEncodedString(order.getTimeSpentOnSite()));
		sessionInfo.setAuthorizationAttempts(BigInteger.valueOf(TRUConstants.TWO));
		context.setSessionInfo(sessionInfo);
		pCreateOrderCreateRequest.setContext(context);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderContextAttributes()");
		}
	}
	
	/**
	 * This method is to populate the order context attributes.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCreateOrderCreateRequest
	 *            - Order XML Object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderContextAttributesForLayawayPayment(Order pOrder,
			ObjectFactory pObjectFactory, OrderCreateRequest pCreateOrderCreateRequest) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderContextAttributesForLayawayPayment()");
			vlogDebug("Order : {0}", pOrder);
		}
		if(pOrder == null || pObjectFactory == null || pCreateOrderCreateRequest == null){
			return;
		}
		TRULayawayOrderImpl order =null;
		if(pOrder instanceof TRULayawayOrderImpl){
			order = (TRULayawayOrderImpl) pOrder;
		}
		Context context = pObjectFactory.createContext();
		context.setLocale(getEncodedString(TRUConstants.LOCALE_EN_US));
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			context.setTdlOrderTimestamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		BrowserData browserData = pObjectFactory.createBrowserData();
		browserData.setHostName(getEncodedString(order.getHostName()));
		browserData.setIPAddress(getEncodedString(order.getCustomerIPAddress()));
		browserData.setSessionId(getEncodedString(order.getBrowserSessionId()));
		browserData.setUserAgent(getEncodedString(order.getBrowserID()));
		browserData.setConnection(getEncodedString(order.getBrowserConnection()));
		if(getOmConfiguration().isIncludeDummyCookie()){
			browserData.setCookies(getEncodedString(getOmConfiguration().getDummyCookie()));
		}else{
			browserData.setCookies(getEncodedString(order.getBrowserCookie()));
		}
		browserData.setJavascriptData(getEncodedString(order.getDeviceID()));
		if(getOmConfiguration().isIncludeJavaScript()){
			browserData.setJavascriptData(getEncodedString(getOmConfiguration().getJavascriptData()));
		}
		browserData.setReferrer(getEncodedString(order.getBrowserReferer()));
		HTTPAcceptData httpAcceptData = pObjectFactory.createHTTPAcceptData();
		httpAcceptData.setContentTypes(getEncodedString(order.getBrowserAccept()));
		httpAcceptData.setEncoding(getEncodedString(order.getBrowserAcceptEncoding()));
		httpAcceptData.setCharSet(getEncodedString(order.getBrowserAcceptCharset()));
		httpAcceptData.setLanguage(getEncodedString(order.getBrowserIdLanguageCode()));
		browserData.setHTTPAcceptData(httpAcceptData);
		context.setBrowserData(browserData);
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		SessionInfo sessionInfo = pObjectFactory.createSessionInfo();
		sessionInfo.setTimeSpentOnSite(getEncodedString(order.getTimeSpentOnSite()));
		sessionInfo.setAuthorizationAttempts(BigInteger.valueOf(TRUConstants.TWO));
		context.setSessionInfo(sessionInfo);
		pCreateOrderCreateRequest.setContext(context);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderContextAttributesForLayawayPayment()");
		}
	}

	/**
	 * Populate order line details.
	 *
	 * @param pOrderImpl
	 * 				- Order
	 * @param pObjectFactory
	 * 				-  Object factory
	 * @param pOrderXMLObj
	 * 				-  the order XML Object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderItemDetails(TRUOrderImpl pOrderImpl, ObjectFactory pObjectFactory,OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderItemDetails()");
			vlogDebug("Order : {0}", pOrderImpl);
		}
		if(pOrderImpl == null || pObjectFactory == null || pOrderXMLObj == null) {
			return;
		}
		OrderItems orderItems =  pObjectFactory.createOrderItems();
		List<ShippingGroup> shippingGroups = pOrderImpl.getShippingGroups();
		ShippingGroup shippingGroup = null;
		Map<String, Double> orderDiscountShareMap = prepareOrderDiscountShareMap(pOrderImpl);
		Map<String, Double> shippingChargeMap = prepareShippingChargeMap(pOrderImpl);
		Map<String, Double> shippingDiscountShareMap = prepareShippingDiscountShareMap(pOrderImpl);
		Map<String, Double> itemDiscountShareMap = prepareItemDiscountShareMap(pOrderImpl);
		Iterator<ShippingGroup> groupIterator = shippingGroups.iterator();
		while(groupIterator.hasNext()) {
			shippingGroup = groupIterator.next();
			if (!(shippingGroup.getCommerceItemRelationshipCount() > 0 )) {
				continue;
			}
			if(shippingGroup instanceof TRUHardgoodShippingGroup) {
				TRUHardgoodShippingGroup hdShippingGroup = (TRUHardgoodShippingGroup)shippingGroup;
				createOrderItemForS2HItem(pOrderImpl, hdShippingGroup, orderItems, pObjectFactory, pOrderXMLObj, orderDiscountShareMap, shippingChargeMap, shippingDiscountShareMap, itemDiscountShareMap);
			}else if(shippingGroup instanceof TRUInStorePickupShippingGroup) {
				TRUInStorePickupShippingGroup inStoreShippingGroup = (TRUInStorePickupShippingGroup)shippingGroup;
				if(isLoggingDebug()) {
					vlogDebug("InStorePickupShippingGroup : {0}", inStoreShippingGroup);
				}
				createOrderItemForISPUItem(pOrderImpl, inStoreShippingGroup, orderItems, pObjectFactory, pOrderXMLObj, orderDiscountShareMap);
			}
		}
		pOrderXMLObj.setOrderItems(orderItems);
		setLineNumber(TRUConstants._0);
		if(isLoggingDebug()) {
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderItemDetails()");
		}
	}
	
	/**
	 * Gets the commerce item RelationShip for instore Items.
	 *
	 * @param pOrderImpl
	 * 				- The OrderImpl
	 * @param pInStoreShippingGroup
	 * 				-The InStoreShippingGroup
	 * @param pOrderItems
	 * 				- The OrderItems
	 * @param pObjectFactory
	 * 				- The ObjectFactory
	 * @param pOrderXMLObj
	 * 				- The Order XML Object
	 * @param pOrderDiscountShareMap - Map
	 */
	@SuppressWarnings("unchecked")
	private void createOrderItemForISPUItem(TRUOrderImpl pOrderImpl,
			TRUInStorePickupShippingGroup pInStoreShippingGroup, OrderItems pOrderItems, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj, Map<String, Double> pOrderDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: createOrderItemForISPUItem()");
		}
		if (pOrderImpl == null || pInStoreShippingGroup == null || pOrderItems == null || pObjectFactory == null || pOrderXMLObj == null) {
			return;
		}
		List<TRUShippingGroupCommerceItemRelationship> shippingCommerceItemRelationList = pInStoreShippingGroup.getCommerceItemRelationships();
		TRUCommerceItemMetaInfo itemMetaInfo = null;
		int lineNumber = getLineNumber();
		for(TRUShippingGroupCommerceItemRelationship shipItemRel : shippingCommerceItemRelationList) {
			if(shipItemRel.getMetaInfo() != null && !shipItemRel.getMetaInfo().isEmpty()) {
				final ListIterator<TRUCommerceItemMetaInfo> listIterator = shipItemRel.
						getMetaInfo().listIterator(shipItemRel.getMetaInfo().size());
				while (listIterator.hasPrevious()) {
					itemMetaInfo = listIterator.previous();
					lineNumber = getLineNumber() + TRUConstants.SIZE_ONE;
					setLineNumber(lineNumber);
					if(isLoggingDebug()) {
						vlogDebug("ShippingGroupcommerceItemRelationship : {0} CommerceItemMetaInfo :{1} ", shipItemRel, itemMetaInfo);
						vlogDebug("Line Item Number : {0}", lineNumber);
					}
					OrderItem orderItem = pObjectFactory.createOrderItem();
					orderItem.setWebLineId(BigInteger.valueOf(lineNumber));
					orderItem.setItemId(getEncodedString((String)itemMetaInfo.getPropertyValue(getCommercePropertyManager().getCatalogRefIdPropertyName())));
					orderItem.setQuantity(BigInteger.valueOf((long)itemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName())));
					TRUCommerceItemImpl commItem = (TRUCommerceItemImpl) shipItemRel.getCommerceItem();
					populatePriceDetailsForOrderItem(pOrderImpl, pObjectFactory, orderItem, shipItemRel, commItem, itemMetaInfo, pOrderDiscountShareMap, null, null, null);
					RepositoryItem skuItem = (RepositoryItem)commItem.getAuxiliaryData().getCatalogRef();
					Description description = pObjectFactory.createDescription();
					description.setDescription(getEncodedString((String)skuItem.getPropertyValue(getCommercePropertyManager().getDisplayNamePropertyName())));
					populateColorAndSize( (RepositoryItem)commItem.getAuxiliaryData().getProductRef(), skuItem, description);
					orderItem.setDescription((description));
					orderItem.setShippingMethod(getEncodedString(getOmConfiguration().getShippingMethodForISPU()));
					long softAmountAllocated = commItem.getSoftAmountAllocated();
					if(softAmountAllocated > TRURadialOMConstants.INT_ZERO && commItem.getReservationId() != null && !commItem.getReservationId().isEmpty()){
						orderItem.setReservationId(BigInteger.valueOf(Long.parseLong(commItem.getReservationId())));
					}
					populateOrderItemCustomAttributes(pOrderImpl, pInStoreShippingGroup, shipItemRel, itemMetaInfo, pObjectFactory, orderItem);
					populateShippingAddress(pOrderImpl, pInStoreShippingGroup, pObjectFactory, orderItem);
					populateItemFeeDetails(pObjectFactory, pOrderImpl, shipItemRel, commItem, itemMetaInfo, orderItem);
					// Checking for BPPItem Details
					TRUBppItemInfo bppItemInfo = shipItemRel.getBppItemInfo();
					if(isLoggingDebug()) {
						vlogDebug("BPPItemInfo : {0}", bppItemInfo);
					}
					CustomizationList customizationList = pObjectFactory.createCustomizationList();
					if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())&& StringUtils.isNotBlank(bppItemInfo.getBppItemId())) {
						lineNumber = lineNumber+TRUConstants.INTEGER_NUMBER_ONE;
						populateBPPCustomizationData(pObjectFactory, shipItemRel, bppItemInfo, customizationList, itemMetaInfo, pOrderImpl);
					}
					if(customizationList != null && !customizationList.getCustomizationData().isEmpty()){
						orderItem.setCustomizationList(customizationList);
					}
					pOrderItems.getOrderItem().add(orderItem);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: createOrderItemForISPUItem()");
		}
	}
	
	/**
	 * Gets the commerce item RelationShip for HardGood Items.
	 *
	 * @param pOrderImpl the order Impl
	 * @param pHdShippingGroup the hd shipping group
	 * @param pOrderItems the order items
	 * @param pObjectFactory the object factory
	 * @param pOrderXMLObj the order request
	 * @param pOrderDiscountShareMap - Map
	 * @param pShippingChargeMap  - shipping charge map
	 * @param pShippingDiscountShareMap  - Shipping discount share map
	 * @param pItemDiscountShareMap - Item discount share map
	 */
	@SuppressWarnings("unchecked")
	private void createOrderItemForS2HItem(TRUOrderImpl pOrderImpl,
			TRUHardgoodShippingGroup pHdShippingGroup, OrderItems pOrderItems, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj, Map<String, Double> pOrderDiscountShareMap, Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap, Map<String, Double> pItemDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: createOrderItemForS2HItem()");
		}
		if(pOrderImpl == null || pHdShippingGroup== null || pOrderItems==null || pObjectFactory==null) {
			return;
		}
		TRUCommerceItemMetaInfo itemMetaInfo = null;
		String shipMethod = null;
		List<TRUShippingGroupCommerceItemRelationship> commerceItemRelationships = pHdShippingGroup.getCommerceItemRelationships();
		atg.core.util.Address shippingAddress = pHdShippingGroup.getShippingAddress();
		if(isLoggingDebug()) {
			vlogDebug("HardgoodShippingGroup : {0} ShippingAddress : {1} TRUShippingGroupCommerceItemRelationship : {2} ", pHdShippingGroup, shippingAddress, commerceItemRelationships);
		}
		if(commerceItemRelationships != null && commerceItemRelationships.isEmpty()) {
			return ;
		}
		int lineNumber = getLineNumber();
		for(TRUShippingGroupCommerceItemRelationship shipItemRel : commerceItemRelationships){
			TRUCommerceItemImpl commItem = (TRUCommerceItemImpl) shipItemRel.getCommerceItem();
			if(commItem instanceof TRUDonationCommerceItem) {
				lineNumber = getLineNumber() + TRUConstants.SIZE_ONE;
				setLineNumber(lineNumber);
				// Setting the Donation Item Details
				populateDonationItemDetails(pOrderImpl, commItem, pObjectFactory, pOrderItems, pOrderXMLObj, shipItemRel);
			} else if(!(commItem instanceof TRUGiftWrapCommerceItem)&& shipItemRel.getMetaInfo() != null &&
					!shipItemRel.getMetaInfo().isEmpty() && StringUtils.isNotBlank(shippingAddress.getFirstName())) {
				final ListIterator<TRUCommerceItemMetaInfo> listIterator = shipItemRel.
						getMetaInfo().listIterator(shipItemRel.getMetaInfo().size());
				while(listIterator.hasPrevious()) {
					lineNumber = getLineNumber() + TRUConstants.SIZE_ONE;
					setLineNumber(lineNumber);
					itemMetaInfo = listIterator.previous();
					if(isLoggingDebug()) {
						vlogDebug("CommerceItem  : {0} ItemMetoInfo : {1}", commItem , itemMetaInfo);
					}
					OrderItem orderItem = pObjectFactory.createOrderItem();
					orderItem.setWebLineId(BigInteger.valueOf(lineNumber));
					orderItem.setItemId(getEncodedString((String)itemMetaInfo.getPropertyValue(getCommercePropertyManager().getCatalogRefIdPropertyName())));
					orderItem.setQuantity(BigInteger.valueOf((long)itemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName())));
					populatePriceDetailsForOrderItem(pOrderImpl, pObjectFactory, orderItem, shipItemRel, commItem, itemMetaInfo, pOrderDiscountShareMap, pShippingChargeMap, pShippingDiscountShareMap, pItemDiscountShareMap);
					RepositoryItem skuItem = (RepositoryItem)commItem.getAuxiliaryData().getCatalogRef();
					Description description = pObjectFactory.createDescription();
					description.setDescription(getEncodedString((String)skuItem.getPropertyValue(getCommercePropertyManager().getDisplayNamePropertyName())));
					populateColorAndSize((RepositoryItem)commItem.getAuxiliaryData().getProductRef(), skuItem, description);
					orderItem.setDescription((description));
					final boolean isMilitaryArea =getShippingManager().isMilitaryArea(shippingAddress);
					final boolean isPOBOXAddress = getShippingManager().isPOBOXAddress(shippingAddress.getAddress1());
					if(isMilitaryArea || isPOBOXAddress ){
						shipMethod = getOmConfiguration().getApoFposhippingMethodMap().get(pHdShippingGroup.getShippingMethod());
						orderItem.setShippingMethod(getEncodedString(shipMethod));
					} else {
						shipMethod = getOmConfiguration().getShippingMethodMap().get(pHdShippingGroup.getShippingMethod());
						orderItem.setShippingMethod(getEncodedString(shipMethod));
					}
					long softAmountAllocated = commItem.getSoftAmountAllocated();
					if(softAmountAllocated > TRURadialOMConstants.INT_ZERO && commItem.getReservationId() != null && !commItem.getReservationId().isEmpty()){
						orderItem.setReservationId(BigInteger.valueOf(Long.parseLong(commItem.getReservationId())));
					}
					populateOrderItemCustomAttributes(pOrderImpl, shipItemRel.getShippingGroup(), shipItemRel, itemMetaInfo, pObjectFactory, orderItem);
					populateShippingAddress(pOrderImpl, shipItemRel.getShippingGroup(), pObjectFactory, orderItem);
					if(getOmConfiguration().isIncludeBillingAddress()){
						populateDummyBillingAddress(orderItem, pObjectFactory, pOrderXMLObj);
					}
					populateItemFeeDetails(pObjectFactory, pOrderImpl, shipItemRel, commItem, itemMetaInfo, orderItem);
					TRUBppItemInfo bppItemInfo = shipItemRel.getBppItemInfo();
					CustomizationList customizationList = pObjectFactory.createCustomizationList();
					if (null != bppItemInfo && StringUtils.isNotBlank(bppItemInfo.getId())&& StringUtils.isNotBlank(bppItemInfo.getBppItemId())) {
						populateBPPCustomizationData(pObjectFactory, shipItemRel, bppItemInfo, customizationList, itemMetaInfo, pOrderImpl);
					}
					if(itemMetaInfo.getPropertyValue(getCommercePropertyManager().getGiftWrapItemPropertyName()) != null){
						RepositoryItem metaInfoGWCommItem = (RepositoryItem) itemMetaInfo.getPropertyValue(getCommercePropertyManager().getGiftWrapItemPropertyName());
						if(metaInfoGWCommItem != null){
							String metaInfoGWCommId=(String)metaInfoGWCommItem.getRepositoryId();
							List<RepositoryItem> giftItemInfo = shipItemRel.getGiftItemInfo();
							if(giftItemInfo != null && !giftItemInfo.isEmpty()) {
								for(RepositoryItem giftItem : giftItemInfo) {
									String giftItemGWCommId = (String) giftItem.getPropertyValue(getCommercePropertyManager().getGiftWrapCommItemId());
									if(StringUtils.isNotBlank(giftItemGWCommId) && StringUtils.isNotBlank(metaInfoGWCommId) && giftItemGWCommId.equals(metaInfoGWCommId)){
										populateGiftwrapCustomizationData(pObjectFactory, shipItemRel, giftItem, pOrderImpl, itemMetaInfo, customizationList) ;
									}
								}
							}
						}
					}
					if(shipItemRel.getShippingGroup() instanceof TRUHardgoodShippingGroup){
						populateGiftMessage(shipItemRel.getShippingGroup(), pObjectFactory, orderItem, customizationList);
					}
					if(shipItemRel.getShippingGroup() instanceof  TRUChannelHardgoodShippingGroup){
						populateRegistryCustomizationData(shipItemRel.getShippingGroup(), pObjectFactory, orderItem, customizationList);
					}
					if(customizationList != null && !customizationList.getCustomizationData().isEmpty()){
						orderItem.setCustomizationList(customizationList);
					}
					pOrderItems.getOrderItem().add(orderItem);
				}		
			}
		} 
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: createOrderItemForS2HItem()");
		}
	}

	/**
	 * This method is populate the dummy billing address.
	 * 
	 * @param pOrderItem
	 *            - OrderItem
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Order XML Object
	 */
	private void populateDummyBillingAddress(OrderItem pOrderItem, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateDummyBillingAddress()");
		}
		Address address = pObjectFactory.createAddress();
		List<Address> addresses = pOrderItem.getAddress();
		if(addresses != null && !addresses.isEmpty()){
			Address address1 = addresses.get(TRUConstants._0);
			address.setType(getEncodedString(getOmConfiguration().getAddressTypeBilling()));
			address.setLine1(getEncodedString(address1.getLine1()));
			address.setCity(getEncodedString(address1.getCity()));
			address.setState(getEncodedString(address1.getState()));
			address.setCountryCode(getEncodedString(address1.getCountryCode()));
			address.setPostalCode(getEncodedString(address1.getPostalCode()));
			address.setPhone(getEncodedString(address1.getPhone()));
			address.setCustomer(address1.getCustomer());
			pOrderXMLObj.setAddress(address);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateDummyBillingAddress()");
		}
	}

	/**
	 * Populate donation item details.
	 * @param pOrderImpl - TRUOrderImpl
	 * @param pCommItem the CommerceItem
	 * @param pObjectFactory the object factory
	 * @param pOrderItems the order items
	 * @param pOrderXMLObj the order xml obj
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship
	 */
	private void populateDonationItemDetails(TRUOrderImpl pOrderImpl, CommerceItem pCommItem,
			ObjectFactory pObjectFactory, OrderItems pOrderItems, OrderRequest pOrderXMLObj, TRUShippingGroupCommerceItemRelationship pShipItemRel) {
		if(isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: populateDonationItemDetails()");
			vlogDebug("CommerceItem : {0}", pCommItem);
		}
		TRUDonationCommerceItem donationItem = (TRUDonationCommerceItem) pCommItem;
		TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) donationItem.getPriceInfo();
		TRURadialOMConfiguration configuration = getOmConfiguration();
		OrderItem orderItem = pObjectFactory.createOrderItem();
		orderItem.setItemId(getEncodedString(donationItem.getCatalogRefId()));
		orderItem.setWebLineId(BigInteger.valueOf(getLineNumber()));
		orderItem.setQuantity(BigInteger.valueOf(donationItem.getQuantity()));
		Pricing pricing = pObjectFactory.createPricing();
		Merchandise merchandise = pObjectFactory.createMerchandise();
		merchandise.setAmount(BigDecimal.valueOf(donationItem.getDonationAmount()));
		merchandise.setUnitPrice(BigDecimal.valueOf(priceInfo.getSalePrice()));
		RepositoryItem skuItem = (RepositoryItem)donationItem.getAuxiliaryData().getCatalogRef();
		Description description = pObjectFactory.createDescription();
		description.setDescription(getEncodedString((String)skuItem.getPropertyValue(getCommercePropertyManager().getDisplayNamePropertyName())));
		populateColorAndSize((RepositoryItem)donationItem.getAuxiliaryData().getProductRef(), skuItem, description);
		orderItem.setDescription((description));
		pricing.setMerchandise(merchandise);
		orderItem.setPricing(pricing);
		orderItem.setShippingMethod(getEncodedString(getOmConfiguration().getShippingMethodForDonation()));
		long softAmountAllocated = donationItem.getSoftAmountAllocated();
		if(softAmountAllocated > TRURadialOMConstants.INT_ZERO && StringUtils.isNotEmpty(donationItem.getReservationId())) {
			orderItem.setReservationId(BigInteger.valueOf(Long.parseLong(donationItem.getReservationId())));
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
		Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();
		populateDonationItemTaxData(pObjectFactory, donationItem, pShipItemRel, merchandise, taxPriceInfos);
		Address billingAddress = pOrderXMLObj.getAddress();
		Address donationShippingAddr = null;
		if(billingAddress != null) {
			donationShippingAddr = new Address();
			donationShippingAddr.setType(TRURadialOMConstants.DONATION_ADDRESS_TYPE_SHIPPING);
			donationShippingAddr.setLine1(billingAddress.getLine1());
			donationShippingAddr.setLine2(billingAddress.getLine2());
			donationShippingAddr.setLine3(billingAddress.getLine3());
			donationShippingAddr.setCity(billingAddress.getCity());
			donationShippingAddr.setState(billingAddress.getState());
			donationShippingAddr.setCountryCode(billingAddress.getCountryCode());
			donationShippingAddr.setPostalCode(billingAddress.getPostalCode());
			donationShippingAddr.setPhone(billingAddress.getPhone());
			donationShippingAddr.setCustomer(billingAddress.getCustomer());
			orderItem.getAddress().add(donationShippingAddr);
		}
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeListPrice(), String.valueOf(priceInfo.getListPrice()));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeItemPrice(), String.valueOf(priceInfo.getSalePrice()));
		orderItem.getCustomAttributes().add(customAttributes);
		pOrderItems.getOrderItem().add(orderItem);
		if(isLoggingDebug()) {
			vlogDebug("BillingAddress : {0} DonationItemAddress : {1}", billingAddress, donationShippingAddr);
			logDebug("Exit from class: TRURadialOMUtils method: populateDonationItemDetails()");
		}
	}
	
	/**
	 * This method is to populate the donation item tax details.
	 *
	 * @param pObjectFactory - ObjectFactory
	 * @param pCommItem - TRUDonationCommerceItem
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship
	 * @param pMerchandise - Merchandise
	 * @param pTaxPriceInfos  - TaxPriceInfos Map
	 */
	private void populateDonationItemTaxData(ObjectFactory pObjectFactory, TRUDonationCommerceItem pCommItem, TRUShippingGroupCommerceItemRelationship pShipItemRel,
			Merchandise pMerchandise, Map<String, String> pTaxPriceInfos) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateDonationItemTaxData()");
		}
		if(pTaxPriceInfos == null || pShipItemRel == null || pObjectFactory == null || pMerchandise == null || pCommItem == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if (pTaxPriceInfos != null && !pTaxPriceInfos.isEmpty()){
			String donationItemTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
					TRURadialOMConstants.UNDERSCORE+pCommItem.getId()+TRURadialOMConstants.UNDERSCORE	+TRURadialOMConstants.MERCHTAX;
			
			String donationItemTaxKeyTaxClass = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
					TRURadialOMConstants.UNDERSCORE+pCommItem.getId()+TRURadialOMConstants.UNDERSCORE	+TRURadialOMConstants.MERCHTAX+
					TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.TAX_CLASS;
			if(isLoggingDebug()){
				vlogDebug("Key to get item tax data for donation item : {0}    is : {1} ", pCommItem.getCatalogRefId(), donationItemTaxKey);
				vlogDebug("Donation Item Tax Key TaxClass : {0}", donationItemTaxKeyTaxClass);
			}
			if(!StringUtils.isBlank(donationItemTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(donationItemTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, TaxData.class);
					taxDataObj.setTaxes(taxes);
					taxDataObj.setTaxClass(pTaxPriceInfos.get(donationItemTaxKeyTaxClass));
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the Item tax data  XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pMerchandise.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateDonationItemTaxData()");
		}
	}

	/**
	 * This method is to populate the Gift message.
	 * 
	 * @param pShippingGroup
	 *            - ShippingGroup
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderItem
	 *            - OrderItem
	 * @param pCustomizationList - CustomizationList
	 */
	private void populateGiftMessage(ShippingGroup pShippingGroup,
			ObjectFactory pObjectFactory, OrderItem pOrderItem, CustomizationList pCustomizationList) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateGiftMessage()");
		}
		if(pShippingGroup == null || pObjectFactory == null || pOrderItem == null || pCustomizationList == null){
			return;
		}
		TRUHardgoodShippingGroup hgSG = (TRUHardgoodShippingGroup) pShippingGroup;
		if(isLoggingDebug()) {
			vlogDebug("HardGoodShippingGroup : {0} ", hgSG);
		}
		if(StringUtils.isNotBlank(hgSG.getGiftWrapMessage())){
			CustomizationData customizationData = pObjectFactory.createCustomizationData();
			JAXBElement<String> type = pObjectFactory.createCustomizationDataType(getEncodedString(getOmConfiguration().getCustomizationDataGiftMessageType()));
			customizationData.getNameOrValueOrType().add(type);
			Message message = pObjectFactory.createMessage();
			message.setBody(getEncodedString(hgSG.getGiftWrapMessage()));
			JAXBElement<Message> message2 = pObjectFactory.createCustomizationDataMessage(message);
			customizationData.getNameOrValueOrType().add(message2);
			pCustomizationList.getCustomizationData().add(customizationData);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateGiftMessage()");
		}
	}

	/**
	 * This method is to populate the item fee list.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderImpl - TRUOrderImpl
	 * @param pShipItemRel
	 *            - TRUShippingGroupCommerceItemRelationship
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - TRUCommerceItemMetaInfo
	 * @param pOrderItem
	 *            - pOrderItem
	 */
	private void populateItemFeeDetails(ObjectFactory pObjectFactory, TRUOrderImpl pOrderImpl, TRUShippingGroupCommerceItemRelationship pShipItemRel,
			CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, OrderItem pOrderItem) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateItemFeeDetails()");
		}
		if(pShipItemRel == null || pCommItem == null || pItemMetaInfo == null || pOrderItem == null){
			return;
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) catalogTools.getCatalogProperties();
		ItemFeeList itemFeeList = pObjectFactory.createItemFeeList();
		if(pShipItemRel.getE911Fees() > TRUConstants.DOUBLE_ZERO){
			ItemFee itemFee = pObjectFactory.createItemFee();
			RepositoryItem prodItem = (RepositoryItem) pCommItem.getAuxiliaryData().getProductRef();
			String eWasteSurchargeProdId = (String) prodItem.getPropertyValue(catalogProperties.getEwasteSurchargeSkuPropertyName());
			try {
				// Getting Repository Item for EWaste Product ID
				RepositoryItem eWasteProductItem = catalogTools.findProduct(eWasteSurchargeProdId);
				// Getting Product of EWaste SKU
				RepositoryItem eWasteSKUItem = catalogTools.getDefaultChildSku(eWasteProductItem);
				if(eWasteSKUItem != null){
					String skuId = eWasteSKUItem.getRepositoryId();
					String feeName = (String) eWasteSKUItem.getPropertyValue(getCommercePropertyManager().getDisplayNamePropertyName());
					long metaInfoQty = (long) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName());
					double eWastePrice = (pShipItemRel.getE911Fees())*metaInfoQty/pShipItemRel.getQuantity();
					itemFee.setItemFeeAmount(BigDecimal.valueOf(eWastePrice));
					itemFee.setItemFeeDescription(feeName);
					itemFee.setItemFeeName(getOmConfiguration().getItemFeeNameEWaste());
					itemFee.setItemFeeSku(skuId);
				}
				TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
				Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();
				populateItemFeeTaxData(pObjectFactory, pShipItemRel, pCommItem, pItemMetaInfo, taxPriceInfos,itemFee);
				itemFeeList.getItemFee().add(itemFee);
			} catch (RepositoryException repExc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting sku or product item : {0}", repExc);
				}
			}
		}
		if(itemFeeList != null && !itemFeeList.getItemFee().isEmpty()){
			pOrderItem.setItemFeeList(itemFeeList);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemFeeDetails()");
		}
	}

	/**
	 * This method is to populate the item fee tax data.
	 * @param pObjectFactory 
	 * 			- ObjectFactory
	 * @param pShipItemRel
	 *            - ShipItemRel
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - MetaInfo
	 * @param pTaxPriceInfos
	 *            - Map
	 * @param pItemFee
	 *            - ItemFee
	 */
	private void populateItemFeeTaxData(ObjectFactory pObjectFactory, TRUShippingGroupCommerceItemRelationship pShipItemRel,
			CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo,	Map<String, String> pTaxPriceInfos, ItemFee pItemFee) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateItemFeeTaxData()");
		}
		if(pShipItemRel == null || pCommItem == null || pItemMetaInfo == null || pItemFee == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if (pTaxPriceInfos != null && !pTaxPriceInfos.isEmpty()){
			String itemFeeTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()
					+TRURadialOMConstants.UNDERSCORE	+pItemFee.getItemFeeSku()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.FEETAX;
			
			String itemFeeTaxKeyTaxClass = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE+
					pItemFee.getItemFeeSku()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.FEETAX+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.TAX_CLASS;
			
			if(isLoggingDebug()){
				vlogDebug("Key to get item fee tax data for item : {0} and fee sku id : {1}    is : {2} ", pCommItem.getCatalogRefId(),pItemFee.getItemFeeSku(),  itemFeeTaxKey);
			}
			if(!StringUtils.isBlank(itemFeeTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(itemFeeTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
					taxDataObj.setTaxes(taxes);
					taxDataObj.setTaxClass(pTaxPriceInfos.get(itemFeeTaxKeyTaxClass));
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the Item Fee tax data XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pItemFee.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemFeeTaxData()");
		}
	}

	/**
	 * This method is to populate the price details for item.
	 *
	 * @param pOrderImpl 			- TRUOrderImpl
	 * @param pObjectFactory            - Object Factory
	 * @param pOrderItem            - OrderItem
	 * @param pShipItemRel            - TRUShippingGroupCommerceItemRelationship
	 * @param pCommItem            - CommerceItem
	 * @param pItemMetaInfo            - MetaInfo
	 * @param pOrderDiscountShareMap - Map
	 * @param pShippingChargeMap  - shipping charge share map
	 * @param pShippingDiscountShareMap - Shipping disocount share map
	 * @param pItemDiscountShareMap - Item discount share map
	 */
	private void populatePriceDetailsForOrderItem(TRUOrderImpl pOrderImpl, ObjectFactory pObjectFactory,
			OrderItem pOrderItem,	TRUShippingGroupCommerceItemRelationship pShipItemRel,	CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, Map<String, Double> pOrderDiscountShareMap, Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap, Map<String, Double> pItemDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populatePriceDetailsForOrderItem()");
		}
		if (pObjectFactory == null || pOrderItem== null || pShipItemRel== null || pCommItem== null || pItemMetaInfo == null){
			return;
		}
		Pricing pricing = pObjectFactory.createPricing();
		populateItemPriceDetails(pOrderImpl, pObjectFactory,pOrderItem,pShipItemRel,pCommItem, pItemMetaInfo, pricing, pOrderDiscountShareMap, pItemDiscountShareMap);
		populateShippingPriceDetails(pOrderImpl, pObjectFactory,pOrderItem,pShipItemRel,pCommItem, pItemMetaInfo, pricing, pShippingChargeMap, pShippingDiscountShareMap);
		pOrderItem.setPricing(pricing);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populatePriceDetailsForOrderItem()");
		}
	}

	/**
	 * This method is to populate the item price details.
	 * @param pOrderImpl 
	 * 			- TRUOrderImpl
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderItem
	 *            - OrderItem
	 * @param pShipItemRel
	 *            - ShipItemRel
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - Meta info
	 * @param pPricing
	 *            - Pricing
	 * @param pOrderDiscountShareMap - Order discount share map
	 * @param pItemDiscountShareMap  - Item discount share map
	 */
	private void populateItemPriceDetails(TRUOrderImpl pOrderImpl, ObjectFactory pObjectFactory,
			OrderItem pOrderItem,	TRUShippingGroupCommerceItemRelationship pShipItemRel,	CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, Pricing pPricing, Map<String, Double> pOrderDiscountShareMap, Map<String, Double> pItemDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateItemPriceDetails()");
		}
		if (pObjectFactory == null || pOrderItem== null || pShipItemRel== null || pCommItem== null || pPricing == null || pItemMetaInfo == null){
			return;
		}
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
		Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();
		Merchandise merchandise = pObjectFactory.createMerchandise();
		PromotionalDiscounts discounts = pObjectFactory.createPromotionalDiscounts();
		getItemDiscountShare(pObjectFactory, pItemMetaInfo, pOrderImpl, pOrderItem, pCommItem, discounts, pShipItemRel, pItemDiscountShareMap);
		populateOrderDiscountsForItem(pObjectFactory, pOrderDiscountShareMap, discounts, pItemMetaInfo, pCommItem, taxPriceInfos, pShipItemRel, TRURadialOMConstants.PROMO_LEVEL_ORDER);
		updateOrderItemPrices(pOrderItem, merchandise, discounts, pItemMetaInfo);
		populateItemTaxData(pObjectFactory, pCommItem, pItemMetaInfo, taxPriceInfos, pShipItemRel, merchandise);
		if(discounts.getDiscount() != null && !discounts.getDiscount().isEmpty()){
			merchandise.setPromotionalDiscounts(discounts);
		}
		pPricing.setMerchandise(merchandise);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemPriceDetails()");
		}
	}

	/**
	 * This method is to update the item 
	 * 	- Extended price
	 *    - Unit price.
	 *
	 * @param pOrderItem - OrderItem
	 * @param pMerchandise - Merchandise
	 * @param pPromotionalDiscounts - PromotionalDiscounts
	 * @param pItemMetaInfo - TRUCommerceItemMetaInfo
	 */
	private void updateOrderItemPrices(OrderItem pOrderItem, Merchandise pMerchandise, PromotionalDiscounts pPromotionalDiscounts, TRUCommerceItemMetaInfo pItemMetaInfo) {
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: updateOrderItemPrices()");
		}
		if (pOrderItem == null || pMerchandise== null || pPromotionalDiscounts== null || pItemMetaInfo == null){
			return;
		}
		TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		double itemDiscountShare = TRUConstants.DOUBLE_ZERO;
		double unitDiscShareForSingleQty = TRUConstants.DOUBLE_ZERO;
		double unitPrice= (double)pItemMetaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
		if(pPromotionalDiscounts != null && pPromotionalDiscounts.getDiscount() != null && !pPromotionalDiscounts.getDiscount().isEmpty()){
			List<Discount> discounts = pPromotionalDiscounts.getDiscount();
			for (Discount discount : discounts) {
				double amount2 = discount.getAmount().doubleValue();
				unitDiscShareForSingleQty += amount2/pOrderItem.getQuantity().longValue(); 
				itemDiscountShare +=  amount2;
			}
		}
		pMerchandise.setUnitPrice(BigDecimal.valueOf(pricingTools.round(unitPrice - pricingTools.round(unitDiscShareForSingleQty))));
		pMerchandise.setAmount(BigDecimal.valueOf(pricingTools.round((unitPrice*pOrderItem.getQuantity().longValue())-itemDiscountShare)));
		
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: updateOrderItemPrices()");
		}
	}
	
	/**
	 * This method is to populate the item tax data.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - Meta Info
	 * @param pTaxPriceInfos
	 *            - taxPriceInfo
	 * @param pShipItemRel
	 *            - ShipItemRel
	 * @param pMerchandise
	 *            - Merchandise
	 */
	private void populateItemTaxData(ObjectFactory pObjectFactory, CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, Map<String, String> pTaxPriceInfos,
			TRUShippingGroupCommerceItemRelationship pShipItemRel,	Merchandise pMerchandise) {
		if(isLoggingDebug()){
			logDebug("Enter into  class: TRURadialOMUtils method: populateItemTaxData()");
		}
		if (pObjectFactory == null || pShipItemRel== null || pCommItem== null || pItemMetaInfo == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if (pTaxPriceInfos != null && !pTaxPriceInfos.isEmpty()){
			String itemTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
					TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE	+TRURadialOMConstants.MERCHTAX;
			
			String itemTaxKeyTaxClass = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
					TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE	+TRURadialOMConstants.MERCHTAX+
					TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.TAX_CLASS;
			if(isLoggingDebug()){
				vlogDebug("Key to get item tax data for item : {0}    is : {1} ", pCommItem.getCatalogRefId(), itemTaxKey);
			}
			if(!StringUtils.isBlank(itemTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(itemTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, TaxData.class);
					taxDataObj.setTaxes(taxes);
					taxDataObj.setTaxClass(pTaxPriceInfos.get(itemTaxKeyTaxClass));
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the Item tax data  XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pMerchandise.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemTaxData()");
		}
	}

	/**
	 * This method is to populate the tax data from item discounts.
	 *
	 * @param pObjectFactory 
	 * 					 - object factory
	 * @param pCommItem
	 * 				 - commerceItem
	 * @param pItemMetaInfo
	 * 				- Meta Info
	 * @param pShipItemRel
	 * 				- TRUShippingGroupCommerceItemRelationship
	 * @param pDiscount
	 * 				- The Discount
	 * @param pTaxPriceInfos
	 * 				- The TaxpriceInfos
	 */
	private void populateItemDisocuntTaxData(ObjectFactory pObjectFactory,
			CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo,	TRUShippingGroupCommerceItemRelationship pShipItemRel, Discount pDiscount, Map<String, String> pTaxPriceInfos) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateItemDisocuntTaxData()");
		}
		if(pObjectFactory == null || pCommItem== null || pShipItemRel == null || pItemMetaInfo == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if(pTaxPriceInfos != null && !pTaxPriceInfos.isEmpty()){
			String itemDiscTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId
					+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE
					+pDiscount.getExternalPromoId()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.MERCHDISCOUNTTAX;
			if(isLoggingDebug()){
				vlogDebug("Key to get item discount tax data for item : {0} is    : {1}", pCommItem.getCatalogRefId(), itemDiscTaxKey);
			}
			if(!StringUtils.isBlank(itemDiscTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(itemDiscTaxKey);
				if(isLoggingDebug()){
					vlogDebug("Tax Data String : {0}", taxDataString);
				}
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
					taxDataObj.setTaxes(taxes);
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the item discount tax data  XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pDiscount.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemDisocuntTaxData()");
		}
	}

	/**
	 * This method is to populate the shipping price details.
	 *
	 * @param pOrderImpl 			- TRUOrderImpl
	 * @param pObjectFactory            - ObjectFactory
	 * @param pOrderItem            - OrderItem
	 * @param pShipItemRel            - ShipItemRel
	 * @param pCommItem            - CommerceItem
	 * @param pItemMetaInfo            - Meta Info
	 * @param pPricing            - Pricing
	 * @param pShippingChargeMap the shipping charge map
	 * @param pShippingDiscountShareMap the shipping discount share map
	 */
	private void populateShippingPriceDetails(TRUOrderImpl pOrderImpl, ObjectFactory pObjectFactory,
			OrderItem pOrderItem,	TRUShippingGroupCommerceItemRelationship pShipItemRel,	CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, Pricing pPricing, Map<String, Double> pShippingChargeMap, Map<String, Double> pShippingDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateShippingPriceDetails()");
		}
		if (pOrderImpl == null || pObjectFactory == null || pOrderItem== null || pShipItemRel== null || pCommItem== null || pPricing == null|| pItemMetaInfo == null){
			return;
		}
		ShippingGroup shippingGroup = pShipItemRel.getShippingGroup();
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		if(shippingGroup instanceof HardgoodShippingGroup){
			TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
			Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();
			TRUHardgoodShippingGroup shipGroup = (TRUHardgoodShippingGroup) shippingGroup;
			Shipping shipping = pObjectFactory.createShipping();
			TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) pCommItem.getPriceInfo();
			long metaInfoQuantity=(long) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName());
			double shippingSurcharge = pricingTools.round(priceInfo.getItemShippingSurcharge()*metaInfoQuantity/pCommItem.getQuantity());
			if(isLoggingDebug()){
				vlogDebug("Shipping surcharge for metainfo : {0} is {1}", pItemMetaInfo.getId(), shippingSurcharge);
			}
			if(pShippingChargeMap != null && !pShippingChargeMap.isEmpty()){
				double shippingCharge = pShippingChargeMap.get(pItemMetaInfo.getId());
				double shippingAmount = pricingTools.round(shippingCharge + shippingSurcharge);
				if(isLoggingDebug()){
					vlogDebug("ShippingCharge amount : {0} metainfoId : {1}  ",shippingCharge,pItemMetaInfo.getId());
					vlogDebug("Sum of shipping charge and surcharge is : {0}", shippingCharge + shippingSurcharge);
					vlogDebug("Shipping Amount : {0}", shippingAmount);
				}
				shipping.setAmount(BigDecimal.valueOf(shippingAmount));
			}
			populateShippingDiscounts(pObjectFactory, pCommItem, pItemMetaInfo, shipGroup, pShipItemRel, shipping, taxPriceInfos, pShippingDiscountShareMap);
			populateShippingTaxData(pObjectFactory, pCommItem, pItemMetaInfo, taxPriceInfos, pShipItemRel, shipping);
			pPricing.setShipping(shipping);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingPriceDetails()");
		}
	}

	
	/**
	 * Gets the shipping group id.
	 *
	 * @param pShipItemRel the ship item rel
	 * @return the shipping group id
	 */
	public String getShippingGroupId(TRUShippingGroupCommerceItemRelationship pShipItemRel){
		if(isLoggingDebug()){
			logDebug("Enter from class: TRURadialOMUtils method: getShippingGroupId()");
		}
		String shipGrpId = null;
		ShippingGroup shippingGroup = pShipItemRel.getShippingGroup();
		if(shippingGroup instanceof TRUHardgoodShippingGroup){
			TRUHardgoodShippingGroup hgSG = (TRUHardgoodShippingGroup) shippingGroup;
			if(!hgSG.isMarkerShippingGroup()){
				List<String> dependencyShippingGruoupIds = hgSG.getDependencyShippingGruoupIds();
				if(dependencyShippingGruoupIds != null && !dependencyShippingGruoupIds.isEmpty()){
					shipGrpId = dependencyShippingGruoupIds.get(TRUConstants._0);
				}
			}else{
				shipGrpId = pShipItemRel.getShippingGroupId();
			}
		}else if( shippingGroup instanceof TRUChannelHardgoodShippingGroup){
			TRUChannelHardgoodShippingGroup channelHgSG = (TRUChannelHardgoodShippingGroup) shippingGroup;
			if(!channelHgSG.isMarkerShippingGroup()){
				List<String> dependencyShippingGruoupIds = channelHgSG.getDependencyShippingGruoupIds();
				if(dependencyShippingGruoupIds != null && !dependencyShippingGruoupIds.isEmpty()){
					shipGrpId = dependencyShippingGruoupIds.get(TRUConstants._0);
				}
			}else{
				shipGrpId = pShipItemRel.getShippingGroupId();
			}
		} else{
			shipGrpId = pShipItemRel.getShippingGroupId();
		}
		if(isLoggingDebug()){
			logDebug("Final shippingGroupID :"+shipGrpId);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: getShippingGroupId()");
		}
		return shipGrpId;
	}
	/**
	 * This method is to populate the tax data for shipping charge.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - Meta Info
	 * @param pTaxPriceInfos
	 *            - TaxPriceInfo
	 * @param pShipItemRel
	 *            - ShipItemRel
	 * @param pShipping
	 *            - Shipping
	 */
	private void populateShippingTaxData(ObjectFactory pObjectFactory, CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo,
			Map<String, String> pTaxPriceInfos, TRUShippingGroupCommerceItemRelationship pShipItemRel,	Shipping pShipping) {
		if(isLoggingDebug()){
			logDebug("Enter from class: TRURadialOMUtils method: populateShippingTaxData()");
		}
		if (pObjectFactory == null || pShipItemRel== null || pCommItem== null || pItemMetaInfo == null|| pItemMetaInfo == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if(pTaxPriceInfos != null && !pTaxPriceInfos.isEmpty()){
			String shipTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId
					+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE
					+TRURadialOMConstants.SHIPPINGTAX;
			if(isLoggingDebug()){
				vlogDebug("Key to get shipping tax data with shipping method : {0} is    : {1}", pShipItemRel.getShippingGroup().getShippingMethod(), shipTaxKey);
			}
			if(!StringUtils.isBlank(shipTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(shipTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
					taxDataObj.setTaxes(taxes);
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the shipping tax data XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pShipping.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingTaxData()");
		}
	}

	/**
	 * This method is to populate the shipping discounts.
	 * 
	 * @param pObjectFactory 
	 * 			- ObjectFactory
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - Meta Info
	 * @param pShippingGroup
	 *            - ShippingGroup
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship
	 * @param pShipping
	 *            - Shipping
	 * @param pTaxPriceInfos - Map
	 * @param pShippingDiscountShareMap - Shipping discount share map
	 */
	private void populateShippingDiscounts(ObjectFactory pObjectFactory, CommerceItem pCommItem,	TRUCommerceItemMetaInfo pItemMetaInfo, ShippingGroup pShippingGroup, TRUShippingGroupCommerceItemRelationship pShipItemRel, Shipping pShipping, Map<String, String> pTaxPriceInfos, Map<String, Double> pShippingDiscountShareMap) {
		if(isLoggingDebug()){
			logDebug("Enter from class: TRURadialOMUtils method: populateShippingDiscounts()");
			vlogDebug("Meta Info Id : {0}  pShippingDiscountShareMap : {1} pShippingGroup : {2}", pItemMetaInfo.getId(), pShippingDiscountShareMap, pShippingGroup.getId());
		}
		if(pObjectFactory == null || pCommItem== null || pShipping == null || pItemMetaInfo == null || pShippingGroup == null){
			return;
		}
		PromotionalDiscounts promotionalDiscounts = pObjectFactory.createPromotionalDiscounts();	
		Discount discount = null;
		if(promotionalDiscounts == null || pShippingDiscountShareMap == null){
			return;
		}
		if(pShippingDiscountShareMap != null && !pShippingDiscountShareMap.isEmpty()){
			Iterator<String> shippingDiscIterator = pShippingDiscountShareMap.keySet().iterator();
			while(shippingDiscIterator.hasNext()){
				String key = shippingDiscIterator.next();
				double discShare = pShippingDiscountShareMap.get(key);
				if(isLoggingDebug()){
					vlogDebug("shippingDiscount share  amount{0} metainfoId{1} ",discShare,pItemMetaInfo.getId());
				}
				String[] keySplit = key.split(PIPE_DELIMETRE);
				String metaInfoId = keySplit[TRURadialOMConstants.INT_ZERO];
				String promoId = keySplit[TRURadialOMConstants.ONE];
				String couponCode = keySplit[TRURadialOMConstants.INT_TWO];
				if(metaInfoId.equals(pItemMetaInfo.getId())){
					discount = pObjectFactory.createDiscount();
					populateDiscountDetails(pObjectFactory, promoId, promotionalDiscounts, discShare, TRURadialOMConstants.PROMO_LEVEL_SHIPPING, discount, couponCode);
					populateShippingDicountTaxData(pObjectFactory, pCommItem, pItemMetaInfo,pShippingGroup, pShipItemRel,  pTaxPriceInfos, discount);
				}
			}
			if(promotionalDiscounts.getDiscount() != null && !promotionalDiscounts.getDiscount().isEmpty()){
				pShipping.setPromotionalDiscounts(promotionalDiscounts);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingDiscounts()");
		}
	}

	/**
	 * This method is to populate the tax data for shipping discounts if there
	 * are any available..
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCommItem
	 *            - CommerceItem
	 * @param pItemMetaInfo
	 *            - Meta Info
	 * @param pShippingGroup
	 *            - ShippingGroup
	 * @param pShipItemRel - TRUShippingGroupCommerceItemRelationship
	 * @param pTaxPriceInfos
	 *            - Map
	 * @param pDiscount
	 *            - Discount
	 */
	private void populateShippingDicountTaxData(ObjectFactory pObjectFactory,
			CommerceItem pCommItem, TRUCommerceItemMetaInfo pItemMetaInfo, ShippingGroup pShippingGroup, TRUShippingGroupCommerceItemRelationship pShipItemRel, Map<String, String> pTaxPriceInfos, Discount pDiscount) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateShippingDicountTaxData()");
		}
		if(pObjectFactory == null || pShipItemRel == null || pCommItem== null || pDiscount == null || pItemMetaInfo == null || pShippingGroup == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if(pTaxPriceInfos!= null && !pTaxPriceInfos.isEmpty()){
			String shipDiscTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId
					+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE
					+pDiscount.getExternalPromoId()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.SHIPPINGDISCOUNTTAX;
			if(isLoggingDebug()){
				vlogDebug("Key to get Shipping discount tax data with shipping method : {0}   is    : {1}", pShipItemRel.getShippingGroup().getShippingMethod(), shipDiscTaxKey);
			}
			if(!StringUtils.isBlank(shipDiscTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(shipDiscTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
					taxDataObj.setTaxes(taxes);
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the Shipping discount tax data XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				if(taxDataObj.getTaxes() != null){
					pDiscount.setTaxData(taxDataObj);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingDicountTaxData()");
		}
	}
	
	/**
	 * Used to set the BPP Tax details.
	 * @param pObjectFactory
	 * 				the ObjectFactory
	 * @param pItemMetaInfo
	 * 				the ItemMetaInfo
	 * @param pShipItemRel
	 * 				the ShipItemRel
	 * @param pTaxPriceInfos
	 * 				the TaxPriceInfos
	 * @param pBppItemInfo
	 * 				the BppItemInfo
	 * @param pCustomizationData
	 * 				the CustomizationData
	 */
	private void populateBPPTaxData(ObjectFactory pObjectFactory,
			 TRUCommerceItemMetaInfo pItemMetaInfo,  TRUShippingGroupCommerceItemRelationship pShipItemRel, Map<String, String> pTaxPriceInfos, TRUBppItemInfo pBppItemInfo, CustomizationData pCustomizationData) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateShippingDicountTaxData()");
		}
		if(pObjectFactory == null || pShipItemRel == null || pBppItemInfo == null || pItemMetaInfo == null){
			return;
		}
		String shipGrpId=getShippingGroupId(pShipItemRel);
		if(pTaxPriceInfos!= null && !pTaxPriceInfos.isEmpty()){
			String bppTaxKey = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId
					+TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE
					+pBppItemInfo.getBppItemId()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.MERCHTAX;
			String bppTaxClass = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
					TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE+pBppItemInfo.getBppItemId()+TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.MERCHTAX+
					TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.TAX_CLASS;
			if(isLoggingDebug()){
				vlogDebug("Key to get bpp discount tax data with shipping method : {0}   is    : {1}", pShipItemRel.getShippingGroup().getShippingMethod(), bppTaxKey);
				vlogDebug("bppTaxClass Key : {0}", bppTaxClass);
			}
			if(!StringUtils.isBlank(bppTaxKey)){
				TaxData taxDataObj = pObjectFactory.createTaxData();
				String taxDataString = pTaxPriceInfos.get(bppTaxKey);
				try {
					Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
					taxDataObj.setTaxes(taxes);
					taxDataObj.setTaxClass(pTaxPriceInfos.get(bppTaxClass));
				} catch (JAXBException exc) {
					if(isLoggingError()){
						vlogError("JAXBException : While converting the bpp discount tax data XML string to JAXB Object, with exception : {0} ", exc); 
					}
				}
				
				pCustomizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataTaxData(taxDataObj));
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingDicountTaxData()");
		}
	}
	
	/**
	 * Sets the gift wrap item details.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroupcommerceItemRelationship the shipping groupcommerce item relationship
	 * @param pRepositoryItem the repository item
	 * @param pOrderImpl the order impl
	 * @param pItemMetaInfo the item meta info
	 * @param pCustomizationList  - customizationList
	 */
	@SuppressWarnings("unchecked")
	private void populateGiftwrapCustomizationData(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pShippingGroupcommerceItemRelationship, RepositoryItem pRepositoryItem, TRUOrderImpl pOrderImpl, TRUCommerceItemMetaInfo pItemMetaInfo, CustomizationList pCustomizationList) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateGiftwrapCustomizationData()");
		}
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) getOrderManager().getPromotionTools().getPricingModelProperties();
		double amount = TRUConstants.DOUBLE_ZERO;
		TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		CustomizationData customizationData = pObjectFactory.createCustomizationData();
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataType(getEncodedString(TRURadialOMConstants.GIFT_WRAP_MESSAGE)));
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataQuantity(BigInteger.valueOf((long)pRepositoryItem.getPropertyValue(commercePropertyManager.getGiftWrapQuantity()))));
		RepositoryItem giftItem = (RepositoryItem)pItemMetaInfo.getPropertyValue(commercePropertyManager.getGiftWrapItemPropertyName());
		CommerceItem giftCommItem = null;
		if(giftItem != null) {
			try { 
				giftCommItem = (CommerceItem)pOrderImpl.getCommerceItem(giftItem.getRepositoryId());
				customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataItemId(giftCommItem.getCatalogRefId()));
			} catch (CommerceItemNotFoundException exec) { 
				if(isLoggingError()){
					vlogError("CommerceItemNotFoundException : While Getting the CommerceItem from GiftItem Repository, with exception : {0} ", exec); 
				}
			}
			  catch (InvalidParameterException exec) { 
				if(isLoggingError()){
					vlogError("InvalidParameterException : While Getting the CommerceItem from GiftItem Repository, with exception : {0} ", exec); 
				}
			}
		}
		if(isLoggingDebug()) {
			vlogDebug("GiftItem RepositoryId : {0} GiftCommerceItem : {1}", pRepositoryItem.getRepositoryId(), giftCommItem);
		}
		PromotionalDiscounts promotionalDiscounts = pObjectFactory.createPromotionalDiscounts();
		if(giftCommItem != null){
			ItemPriceInfo priceInfo = giftCommItem.getPriceInfo();
			if(priceInfo != null) {
				amount = priceInfo.getAmount();
				List<PricingAdjustment> adjustments = priceInfo.getAdjustments();
				if(adjustments != null) {
					for(PricingAdjustment adjustment : adjustments) {
						RepositoryItem  promotionltem = adjustment.getPricingModel();
						RepositoryItem  couponItem = adjustment.getCoupon();
						if(promotionltem != null) {
							Discount discount = pObjectFactory.createDiscount();
							double discAmt = -adjustment.getTotalAdjustment();
							discount.setId(promotionltem.getRepositoryId());
							if(couponItem != null) {
								discount.setCode(couponItem.getRepositoryId());
							} else if(promotionltem.getPropertyValue(pricingModelProperties.getCampaignIdPropertyName()) != null) {
								discount.setCode(getEncodedString((String) promotionltem.getPropertyValue(pricingModelProperties.getCampaignIdPropertyName())));
							}
							discount.setAmount(BigDecimal.valueOf(discAmt));
							discount.setDescription(getEncodedString((String) promotionltem.getPropertyValue(pricingModelProperties.getDisplayName())));
							discount.setLongDescription(getEncodedString((String) promotionltem.getPropertyValue(pricingModelProperties.getDescription())));
							discount.setShortDescription(getEncodedString((String) promotionltem.getPropertyValue(pricingModelProperties.getDisplayName())));
							boolean vendorFunded = (boolean) promotionltem.getPropertyValue(pricingModelProperties.getVendorFundedPropertyName());
							if(vendorFunded){
								discount.setPromoType(String.valueOf(TRUConstants.ZERO));
							}else{
								discount.setPromoType(String.valueOf(TRUConstants.ONE));
							}
							discount.setPromoLevel(TRURadialOMConstants.PROMO_LEVEL_ITEM);
							TaxData taxData = populateGiftwrapTaxData(pObjectFactory, pShippingGroupcommerceItemRelationship, pOrderImpl, pItemMetaInfo, TRURadialOMConstants.GWDISCOUNTTAX);
							if(taxData.getTaxes() != null){
								discount.setTaxData(taxData);
							}
							promotionalDiscounts.getDiscount().add(discount);
						}
					}
					if(promotionalDiscounts.getDiscount() != null && !promotionalDiscounts.getDiscount().isEmpty()){
						customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataPromotionalDiscounts(promotionalDiscounts));
					}
				}
			}
		}
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataCharge(BigDecimal.valueOf(amount)));
		TaxData taxData = populateGiftwrapTaxData(pObjectFactory, pShippingGroupcommerceItemRelationship, pOrderImpl, pItemMetaInfo, TRURadialOMConstants.GWTAX);
		if(taxData != null){
			customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataTaxData(taxData));
		}
		pCustomizationList.getCustomizationData().add(customizationData);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateGiftwrapCustomizationData()");
		}
	}

	/**
	 * Gets the tax data details.
	 *
	 * @param pObjectFactory the object factory
	 * @param pShippingGroupcommerceItemRelationship the shipping groupcommerce item relationship
	 * @param pOrderImpl the order impl
	 * @param pItemMetaInfo the item meta info
	 * @param pTaxType the tax type
	 * @return the tax data details
	 */
	private TaxData populateGiftwrapTaxData(ObjectFactory pObjectFactory, TRUShippingGroupCommerceItemRelationship pShippingGroupcommerceItemRelationship,
			TRUOrderImpl pOrderImpl, TRUCommerceItemMetaInfo pItemMetaInfo,	String pTaxType) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateGiftwrapTaxData()");
		}
		TaxData taxDataObj = null;
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
		Map<String, String> taxPriceInfoMap = taxPriceInfo.getRadialTaxPriceInfos();
		String shipGrpId=getShippingGroupId(pShippingGroupcommerceItemRelationship);
		String key =TRURadialOMConstants.SG+ TRUConstants.UNDER_SCORE+shipGrpId
				+ TRUConstants.UNDER_SCORE + pItemMetaInfo.getId() + TRUConstants.UNDER_SCORE + pTaxType;
		String gwTaxKeyTaxClass = TRURadialOMConstants.SG+TRURadialOMConstants.UNDERSCORE+shipGrpId+
				TRURadialOMConstants.UNDERSCORE+pItemMetaInfo.getId()+TRURadialOMConstants.UNDERSCORE	+pTaxType+
				TRURadialOMConstants.UNDERSCORE+TRURadialOMConstants.TAX_CLASS;
		if(isLoggingDebug()){
			vlogDebug("Key to get item tax data for item : {0}" , key);
		}
		if(!StringUtils.isBlank(key) && taxPriceInfoMap != null && !taxPriceInfoMap.isEmpty()){
			taxDataObj = pObjectFactory.createTaxData();
			String taxDataString = taxPriceInfoMap.get(key);
			try {
				Taxes taxes = (Taxes) convertXMLDataToObject(taxDataString, Taxes.class);
				taxDataObj.setTaxes(taxes);
				taxDataObj.setTaxClass(taxPriceInfoMap.get(gwTaxKeyTaxClass));
			} catch (JAXBException exc) {
				if(isLoggingError()){
					vlogError("JAXBException : While converting the gift wrap tax data  XML string to JAXB Object, with exception : {0} ", exc); 
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateGiftwrapTaxData()");
		}
		return taxDataObj;
	}
	
	/**
	 * Convert XML data to Object.
	 *
	 * @param pTaxInfoXML
	 * 				- The TaxInfo XML
	 * @param pType
	 * 			 - The ClassType
	 * @return the object
	 * @throws JAXBException the JAXB exception
	 */
	@SuppressWarnings("rawtypes")
	private Object convertXMLDataToObject(String pTaxInfoXML, Class pType)
			throws JAXBException {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: convertXMLDataToObject()");
		}
		Taxes taxes = null;
		if(pTaxInfoXML != null &&  pType != null){
			StringReader reader = new StringReader(pTaxInfoXML);
			JAXBContext jaxbContext = JAXBContext.newInstance(pType);
			StreamSource s = new StreamSource(reader);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<Taxes> taxes1 = (JAXBElement<Taxes>) jaxbUnmarshaller.unmarshal(s, Taxes.class);
			taxes = taxes1.getValue();
			return taxes;
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: convertXMLDataToObject()");
		}
		return taxes;
	}
	
	/**
	 * Sets the BPP line item details.
	 * @param pObjectFactory
	 * 				- the Object Factory
	 * @param pShippingGroupcommerceItemRelationship
	 * 				- the ShippingGroupcommerceItemRelationship
	 * @param pBppItemInfo
	 * 				- the BppItemInfo
	 * @param pCustomizationList
	 * 				- CustomizationList
	 * @param pItemMetaInfo
	 * 				- ItemMetaInfo
	 * @param pOrderImpl
	 * 				- OrderImpl
	 */
	private void populateBPPCustomizationData(ObjectFactory pObjectFactory,
			TRUShippingGroupCommerceItemRelationship pShippingGroupcommerceItemRelationship,TRUBppItemInfo pBppItemInfo, CustomizationList pCustomizationList, TRUCommerceItemMetaInfo pItemMetaInfo, TRUOrderImpl pOrderImpl) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateBPPCustomizationData()");
			vlogDebug("ShippingGroupCommerceItemRelationship : {0} BppItemInfo {1}", pShippingGroupcommerceItemRelationship, pBppItemInfo);
		}
		if(pObjectFactory == null || pShippingGroupcommerceItemRelationship == null || pBppItemInfo == null  || pCustomizationList == null) {
			return;
		}
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		CustomizationData customizationData = pObjectFactory.createCustomizationData();
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataType(TRURadialOMConstants.WARRANTY));
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataItemId(pBppItemInfo.getBppSkuId()));
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataQuantity(BigInteger.valueOf((long) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName()))));
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataCharge(BigDecimal.valueOf(pricingTools.round((long) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName())*(double)pBppItemInfo.getBppPrice()))));
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrderImpl.getTaxPriceInfo();
		Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();

		populateBPPTaxData(pObjectFactory, pItemMetaInfo, pShippingGroupcommerceItemRelationship, taxPriceInfos, pBppItemInfo, customizationData);
		pCustomizationList.getCustomizationData().add(customizationData);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateBPPCustomizationData()");
		}
	}
	
	/**
	 * Sets the store locator item details.
	 *
	 * @param pAddress the address
	 * @param pLocationRepoItem the location RepositoryItem
	 */
	private void setStoreLocatorItemDetails(Address pAddress, RepositoryItem pLocationRepoItem) {
		if(isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: setStoreLocatorItemDetails()");
			vlogDebug("LocationRepositoryItem : {0}", pLocationRepoItem);
		}
		if(pLocationRepoItem  != null) {
			pAddress.setLine1(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStoreAddress1())));
			pAddress.setCity(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getCityName())));
			pAddress.setState(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStateAddressPropertyName())));
			if(!StringUtils.isEmpty((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStoreCountry()))){
				pAddress.setCountryCode(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStoreCountry())));
			} else {
				pAddress.setCountryCode(getEncodedString(getOmConfiguration().getCountryCodeUS()));
			}
			pAddress.setPostalCode(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStorePostalCode())));
			pAddress.setPhone(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStorePhoneNumber())));
			pAddress.setStoreName(getEncodedString((String)pLocationRepoItem.getPropertyValue(getCommercePropertyManager().getStoreName())));
		}
		if(isLoggingDebug()) {
			logDebug("Exit from class: TRURadialOMUtils method: setStoreLocatorItemDetails()");
		}
	}
	
	/**
	 * Gets the location repository item.
	 *
	 * @param pLocationId the location id
	 * @return the location repository item
	 */
	private RepositoryItem getLocationRepositoryItem(String pLocationId){
		if(isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: getLocationRepositoryItem()");
			vlogDebug("LocationId : {0}", pLocationId);
		}
		RepositoryItem locationRepoItem = null;
		try {
			locationRepoItem = (RepositoryItem)getStoreLocationRepository().getItem(pLocationId,getCommercePropertyManager().getStoreItemDescriptorName());
		} catch (RepositoryException exec) {
			if(isLoggingError()){
				vlogError("RepositoryException : While getting Location RepositoryItem and exception is : {0}", exec);
			}
		}
		if(isLoggingDebug()) {
			logDebug("Exit from class: TRURadialOMUtils method: getLocationRepositoryItem()");
			vlogDebug("LocationRepoItem : {0}", locationRepoItem);
		}
		return locationRepoItem;
	}
	
	/**
	 * This method is used to populate the ISPU details of OrderLine.
	 *
	 * @param pOrder
	 * 			- the order
	 * @param pShippingGroup
	 *  				-  the shipping group
	 * @param pObjectFactory
	 *  				-  the object factory
	 * @param pOrderItem
	 *  				-  the order item xml obJ
	 * @param pLocationRepositoryItem
	 * 					-  the location repository item
	 */
	
	
	private void populateStoreFrontAddress(TRUOrderImpl pOrder, TRUInStorePickupShippingGroup pShippingGroup,
			ObjectFactory pObjectFactory, OrderItem pOrderItem, RepositoryItem pLocationRepositoryItem) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateStoreFrontAddress()");
			vlogDebug("Order : {0} ISPUShippingGroup", pOrder , pShippingGroup);
		}
		if(pOrder== null || pShippingGroup== null || pObjectFactory== null || pOrderItem== null) {
			return;
		}
		Address instoreAddress = pObjectFactory.createAddress();
		instoreAddress.setType(getOmConfiguration().getAddressTypeStoreFront());
		String locationId = pShippingGroup.getLocationId();
		setStoreLocatorItemDetails(instoreAddress, pLocationRepositoryItem);
		//instoreAddress.setDirections(value);
		Hours hours = populateISPUHoursDetails(pObjectFactory, pLocationRepositoryItem);
		if(hours != null) {
			instoreAddress.setHours(hours);
		}
		instoreAddress.setStoreCode(getEncodedString(locationId));
		instoreAddress.setStorePhone(getEncodedString((String)pLocationRepositoryItem.getPropertyValue(getCommercePropertyManager().getStorePhoneNumber())));
		//Populate primary pickup details
		Customer customer = pObjectFactory.createCustomer();
		CustomerName customerName = pObjectFactory.createCustomerName();
		customerName.setFirstName(getEncodedString(pShippingGroup.getFirstName()));
		customerName.setLastName(getEncodedString(pShippingGroup.getLastName()));
		customer.setCustomerName(customerName);
		customer.setCustomerId(pOrder.getProfileId());
		customer.setEmailAddress(getEncodedString(pShippingGroup.getEmail()));
		instoreAddress.setCustomer(customer);
		instoreAddress.setHours(hours);
		pOrderItem.getAddress().add(instoreAddress);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateStoreFrontAddress()");
		}
	}
	
	/**
	 * This method is used to populate the ISPU stores open and close hours details.
	 *
	 * @param pObjectFactory the object factory
	 * @param pLocationRepositoryItem the location repository item
	 * @return the hours
	 */
	@SuppressWarnings("unchecked")
	private Hours populateISPUHoursDetails(ObjectFactory pObjectFactory,
			RepositoryItem pLocationRepositoryItem) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateISPUHoursDetails()");
			vlogDebug("LocationRepositoryItem : {0}", pLocationRepositoryItem);
		}
		Hours hours = null;
		if(pObjectFactory == null  || pLocationRepositoryItem == null) {
			return hours;
		}
		hours = pObjectFactory.createHours();
		List<RepositoryItem> storeHoursRepoItemsList = (List<RepositoryItem>)pLocationRepositoryItem.getPropertyValue(getLocationPropertyManager().getStoreHoursPropertyName());
		if(storeHoursRepoItemsList != null) {
			for(RepositoryItem repositoryItem : storeHoursRepoItemsList) {
				String day = (String)repositoryItem.getPropertyValue(getLocationPropertyManager().getDayPropertyName());
				Double openingHours = (Double)repositoryItem.getPropertyValue(getLocationPropertyManager().getOpeningHoursPropertyName());
				Double closinghours = (Double)repositoryItem.getPropertyValue(getLocationPropertyManager().getClosingHoursPropertyName());
				DateTimeFormatter inputFormatter = DateTimeFormat.forPattern(TRURadialOMConstants.DATE_INPUT_FORMATTER);
				DateTimeFormatter outputFormatter = DateTimeFormat.forPattern(TRURadialOMConstants.DATE_OUTPUT_FORMATTER);
				String openingHrs = openingHours.toString();
				String closingHrs = closinghours.toString();
				StringBuffer openingHrsConcat =  new StringBuffer();
				StringBuffer closingHrsConcat =  new StringBuffer();
				if(openingHrs.length() < TRURadialOMConstants.INT_SIX) {
					if(openingHrs.equals(TRURadialOMConstants.OPENING_HOURS_ZERO_TIME)){
						openingHrsConcat.append(TRURadialOMConstants.HOURS_ZERO);
					}
					openingHrsConcat.append(TRURadialOMConstants.HOURS_ZERO).append(openingHrs);
					openingHrs = openingHrsConcat.toString();
				}
				if(closingHrs.length() < TRURadialOMConstants.INT_SIX){
					if(closingHrs.equals(TRURadialOMConstants.OPENING_HOURS_ZERO_TIME)) {
						closingHrsConcat.append(TRURadialOMConstants.HOURS_ZERO);
					}
					closingHrsConcat.append(TRURadialOMConstants.HOURS_ZERO).append(closingHrs);
					closingHrs = closingHrsConcat.toString();
				}
				DateTime openingTime = inputFormatter.parseDateTime(openingHrs);
				String formattedOpeningHours = outputFormatter.print(openingTime.getMillis());
				DateTime closingTime = inputFormatter.parseDateTime(closingHrs);
				String formattedClosingHours = outputFormatter.print(closingTime.getMillis());
				switch(day) {
				case TRURadialOMConstants.MONDAY : {
					hours.setOpeningTimeMon(formattedOpeningHours);
					hours.setClosingTimeMon(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.TUESDAY : {
					hours.setOpeningTimeTue(formattedOpeningHours);
					hours.setClosingTimeTue(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.WEDNESDAY : {
					hours.setOpeningTimeWed(formattedOpeningHours);
					hours.setClosingTimeWed(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.THURSDAY : {
					hours.setOpeningTimeThu(formattedOpeningHours);
					hours.setClosingTimeThu(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.FRIDAY : {
					hours.setOpeningTimeFri(formattedOpeningHours);
					hours.setClosingTimeFri(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.SATURADAY : {
					hours.setOpeningTimeSat(formattedOpeningHours);
					hours.setClosingTimeSat(formattedClosingHours);
					break;
				}
				case TRURadialOMConstants.SUNDAY : {
					hours.setOpeningTimeSun(formattedOpeningHours);
					hours.setClosingTimeSun(formattedClosingHours);
					break;
				}
				}
			}
		}
		hours.setOpen24HrMonInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedMonInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrTueInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedTueInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrWedInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedWedInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrThuInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedThuInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrFriInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedFriInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrSatInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedSatInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setOpen24HrSunInd(TRURadialOMConstants.HOURS_ZERO);
		hours.setClosedSunInd(TRURadialOMConstants.HOURS_ZERO);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateISPUHoursDetails()");
		}
		return hours;
	}


	
	/**
	 * This method is to populate request for layaway payment.
	 *
	 * @param pOrder the order
	 * @return the string
	 */
	public String populateLayawayPaymentRequestXML(TRULayawayOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateLayawayPaymentRequestXML()");
			vlogDebug("Order : {0}", pOrder);
		}
		String requestXML = null;
		if(pOrder != null){
			ObjectFactory objectFactory = new ObjectFactory();
			OrderCreateRequest createOrderCreateRequest = objectFactory.createOrderCreateRequest();
			OrderRequest orderXMLObj = objectFactory.createOrderRequest();
			TRULayawayOrderImpl order = (TRULayawayOrderImpl) pOrder;
			//Populate order payment details
			populateOrderPaymentsForLayawayPayment(order, objectFactory, orderXMLObj);
			populateOrderItemForLayawayPayment(order, objectFactory, orderXMLObj);
			//Populate Order header attributes
			populateOrderHeaderForLayawayPayment(order, objectFactory, orderXMLObj);
			//Populate Context Attributes
			populateOrderContextAttributesForLayawayPayment(pOrder, objectFactory, createOrderCreateRequest);
			createOrderCreateRequest.setOrder(orderXMLObj);
			JAXBElement<OrderCreateRequest> createOrderCreateRequest2 = objectFactory.createOrderCreateRequest(createOrderCreateRequest);
			requestXML = marshalRequestXML(createOrderCreateRequest2, getOmConfiguration().getOrderCreateXSDPackage());
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateLayawayPaymentRequestXML()");
		}
		return requestXML;
	}

	/**
	 * This method is to populate order items for layaway payment.
	 * 
	 * @param pLayawayOrderImpl
	 *            - Layaway Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Order XML Object
	 */
	private void populateOrderItemForLayawayPayment(TRULayawayOrderImpl pLayawayOrderImpl,
			ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderItemForLayawayPayment()");
		}
		if(pLayawayOrderImpl == null || pObjectFactory == null || pOrderXMLObj == null){
			return;
		}
		OrderItems orderItems = pObjectFactory.createOrderItems();
		OrderItem orderItem = pObjectFactory.createOrderItem();
		orderItem.setWebLineId(BigInteger.valueOf(getLineNumber() + TRUConstants.SIZE_ONE));
		RepositoryItem layawaySKUItem = getLayawayItemUID();
		if(layawaySKUItem != null){
			String layawayItemUID = layawaySKUItem.getRepositoryId();
			String layawayItemName = (String) layawaySKUItem.getPropertyValue(getCommercePropertyManager().getDisplayNamePropertyName());
			orderItem.setItemId(layawayItemUID);
			Description description = pObjectFactory.createDescription();
			description.setDescription(layawayItemName);
			orderItem.setDescription((description));
			orderItem.setShippingMethod(getOmConfiguration().getShippingMethodForLayawayPayment());
			orderItem.setQuantity(BigInteger.valueOf(getLineNumber() + TRUConstants.SIZE_ONE));
		}
		Pricing pricing = pObjectFactory.createPricing();
		Merchandise merchandise = pObjectFactory.createMerchandise();
		merchandise.setUnitPrice(BigDecimal.valueOf(pLayawayOrderImpl.getPaymentAmount()));
		merchandise.setAmount(BigDecimal.valueOf(pLayawayOrderImpl.getPaymentAmount()));
		pricing.setMerchandise(merchandise);
		orderItem.setPricing(pricing);
		populateItemShippingAddressForLayawayItem(orderItem, pLayawayOrderImpl, pObjectFactory);
		orderItems.getOrderItem().add(orderItem);
		pOrderXMLObj.setOrderItems(orderItems);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderItemForLayawayPayment()");
		}
	}
	
	
	/**
	 * This method is to populate shipping address for layaway order item.
	 * 
	 * @param pOrderItem
	 *            - OrderItem
	 * @param pLayawayOrderImpl
	 *            - LayawayOrder
	 * @param pObjectFactory - ObjectFactory
	 */
	private void populateItemShippingAddressForLayawayItem(
			OrderItem pOrderItem, TRULayawayOrderImpl pLayawayOrderImpl, ObjectFactory pObjectFactory) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateItemShippingAddressForLayawayItem()");
		}
		if(pOrderItem == null || pLayawayOrderImpl == null || pObjectFactory == null){
			return;
		}
		TRURadialOMConfiguration omConfiguration = getOmConfiguration();
		Address shippingAddress = pObjectFactory.createAddress();
		shippingAddress.setType(omConfiguration.getAddressTypeShipping());
		shippingAddress.setLine1(pLayawayOrderImpl.getAddress1());
		shippingAddress.setLine2(pLayawayOrderImpl.getAddress2());
		shippingAddress.setCity(pLayawayOrderImpl.getCity());
		shippingAddress.setState(pLayawayOrderImpl.getAddrState());
		shippingAddress.setPostalCode(pLayawayOrderImpl.getPostalCode());
		shippingAddress.setCountryCode(pLayawayOrderImpl.getCountry());
		shippingAddress.setPhone(pLayawayOrderImpl.getPhoneNumber());
		Customer customer = pObjectFactory.createCustomer();
		CustomerName customerName = pObjectFactory.createCustomerName();
		RepositoryItem profile = null;
		try {
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pLayawayOrderImpl);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("RepositoryException : Exce : {0}", e);
			}
		}
		if(profile != null && !profile.isTransient()){	
			customer.setCustomerId(pLayawayOrderImpl.getProfileId());
		}
		customerName.setFirstName(pLayawayOrderImpl.getFirstName());
		customerName.setLastName(pLayawayOrderImpl.getLastName());
		customer.setCustomerName(customerName);
		if(omConfiguration.isIncludeTile()){
			customer.setTitle(omConfiguration.getCustomerTitle());
		}
		customer.setEmailAddress(pLayawayOrderImpl.getEmail());
		shippingAddress.setCustomer(customer);
		pOrderItem.getAddress().add(shippingAddress);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateItemShippingAddressForLayawayItem()");
		}
	}
	
	/**
	 * This method is to populate billing address for layaway order item.
	 * 
	 * @param pOrderXMLObj
	 *            - Order XML Obj
	 * @param pLayawayOrderImpl
	 *            - LayawayOrder
	 * @param pObjectFactory - ObjectFactory
	 */
	private void populateBillingAddressForLayaway(OrderRequest pOrderXMLObj, TRULayawayOrderImpl pLayawayOrderImpl, ObjectFactory pObjectFactory) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateBillingAddressForLayaway()");
		}
		if(pOrderXMLObj == null || pLayawayOrderImpl == null || pObjectFactory == null){
			return;
		}
		TRURadialOMConfiguration omConfiguration = getOmConfiguration();
		Address billingAddress = pObjectFactory.createAddress();
		billingAddress.setType(omConfiguration.getAddressTypeBilling());
		billingAddress.setLine1(pLayawayOrderImpl.getAddress1());
		billingAddress.setLine2(pLayawayOrderImpl.getAddress2());
		billingAddress.setCity(pLayawayOrderImpl.getCity());
		billingAddress.setState(pLayawayOrderImpl.getAddrState());
		billingAddress.setPostalCode(pLayawayOrderImpl.getPostalCode());
		billingAddress.setCountryCode(pLayawayOrderImpl.getCountry());
		billingAddress.setPhone(pLayawayOrderImpl.getPhoneNumber());
		Customer customer = pObjectFactory.createCustomer();
		CustomerName customerName = pObjectFactory.createCustomerName();
		RepositoryItem profile = null;
		try {
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pLayawayOrderImpl);
		} catch (RepositoryException e) {
			if(isLoggingError()){
				vlogError("RepositoryException : Exce : {0}", e);
			}
		}
		if(profile != null && !profile.isTransient()){	
			customer.setCustomerId(pLayawayOrderImpl.getProfileId());
		}
		customerName.setFirstName(pLayawayOrderImpl.getFirstName());
		customerName.setLastName(pLayawayOrderImpl.getLastName());
		customer.setCustomerName(customerName);
		if(omConfiguration.isIncludeTile()){
			customer.setTitle(omConfiguration.getCustomerTitle());
		}
		customer.setEmailAddress(pLayawayOrderImpl.getEmail());
		billingAddress.setCustomer(customer);
		pOrderXMLObj.setAddress(billingAddress);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateBillingAddressForLayaway()");
		}
	}

	/**
	 * Gets the layaway item uid.
	 *
	 * @return String - PaymentSkin Sub Type nonMerchSKU id.
	 */
	public RepositoryItem getLayawayItemUID() {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRURadialOMUtils  method: getLayawayItemUID]");
		}
		TRUCatalogTools catalogTools = getCatalogTools();
		RepositoryItem layawaySKUItem = catalogTools.getDonationOrBPPSKUItem(getOmConfiguration().getLayawaySKUType());
		if (isLoggingDebug()) {
			vlogDebug("layawaySKUItem : {0}" ,layawaySKUItem);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMUtils  method: getLayawayItemUID]");
		}
		return layawaySKUItem;
	}

	/**
	 * This method is to populate the order header parameters.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj - Create Order object
	 */
	private void populateOrderHeader(TRUOrderImpl pOrder, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderHeader()");
			vlogDebug("Order : {0}", pOrder);
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		if(pObjectFactory == null &&  pOrder == null && pOrderXMLObj == null){
			return;
		}
		TRUOrderImpl order = (TRUOrderImpl) pOrder;
		pOrderXMLObj.setWebOrderId(getEncodedString(order.getId()));
		SourceId sourceId = pObjectFactory.createSourceId();
		sourceId.setType(getEncodedString(configuration.getSourceIdType()));
		sourceId.setValue(getEncodedString(configuration.getSourceIdValue()));
		pOrderXMLObj.setSourceId(sourceId);
		pOrderXMLObj.setLocale(getEncodedString(configuration.getLocale()));
		pOrderXMLObj.setSource(getEncodedString(configuration.getSource()));
		pOrderXMLObj.setCurrency(getEncodedString(configuration.getCurrencyUSD()));
		DateFormat df = new SimpleDateFormat(TRURadialOMConstants.EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(configuration.getTimeZoneEST()));
		if(pOrder.getSubmittedDate() != null){
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeZone(getTimeZone());
			XMLGregorianCalendar xgcal;
			try {
				xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				pOrderXMLObj.setCreateTime(xgcal);
			} catch (DatatypeConfigurationException exc) {
				if(isLoggingError()){
					vlogError("DatatypeConfigurationException : Exce : {0}", exc);
				}
			}
		}
		//Populate order custom attributes
		populateOrderHeaderCustomAttributes(pOrder, pObjectFactory, pOrderXMLObj);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderHeader()");
		}
	}

	/**
	 * This method is to populate the order header parameters For Layaway payment..
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj - Create Order object
	 */
	private void populateOrderHeaderForLayawayPayment(TRULayawayOrderImpl pOrder, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderHeaderForLayawayPayment()");
			vlogDebug("Order : {0}", pOrder);
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		if(pObjectFactory == null &&  pOrder == null && pOrderXMLObj == null){
			return;
		}
		TRULayawayOrderImpl order = (TRULayawayOrderImpl) pOrder;
		pOrderXMLObj.setWebOrderId(order.getId());
		SourceId sourceId = pObjectFactory.createSourceId();
		sourceId.setType(configuration.getSourceIdType());
		sourceId.setValue(configuration.getSourceIdValue());
		pOrderXMLObj.setSourceId(sourceId);
		pOrderXMLObj.setLocale(configuration.getLocale());
		pOrderXMLObj.setSource(configuration.getSource());
		pOrderXMLObj.setCurrency(configuration.getCurrencyUSD());
		DateFormat df = new SimpleDateFormat(TRURadialOMConstants.EST_DATE_FORMAT,Locale.US);
		df.setTimeZone(TimeZone.getTimeZone(configuration.getTimeZoneEST()));
		if(pOrder.getSubmittedDate() != null){
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeZone(getTimeZone());
			XMLGregorianCalendar xgcal;
			try {
				xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				pOrderXMLObj.setCreateTime(xgcal);
			} catch (DatatypeConfigurationException exc) {
				if(isLoggingError()){
					vlogError("DatatypeConfigurationException : Exce : {0}", exc);
				}
			}
		}
		//Populate order custom attributes
		populateOrderHeaderCustomAttributesForLayawayPayment(pOrder, pObjectFactory, pOrderXMLObj);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderHeaderForLayawayPayment()");
		}
	}

	/**
	 * This method is to populate the order header custom attributes.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Create Order object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderHeaderCustomAttributes(TRUOrderImpl pOrder,
			ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderHeaderCustomAttributes()");
		}
		double balanceAmount = TRUConstants.DOUBLE_ZERO;
		boolean isSsoOrder = Boolean.FALSE;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		TRUOrderPriceInfo priceInfo = (TRUOrderPriceInfo) pOrder.getPriceInfo();
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		String csrSourceOrderType = getCatalogTools().getPricingTools().getTruConfiguration().getCsrSourceOrderType();
		String sosSourceOrderType = getCatalogTools().getPricingTools().getTruConfiguration().getSosSourceOrderType();
		String sourceChannelRegStore = configuration.getSourceChannelRegStore();//CR-251 changes
		if(pOrder.getSourceOfOrder().equals(sosSourceOrderType)){
			isSsoOrder=getChannelRegStore(pOrder,sourceChannelRegStore);
		}
		if(isLoggingDebug()){
			logDebug("isSsoOrder  value{0}:"+isSsoOrder);
		}
		if(isSsoOrder){
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeOrderChannel(), csrSourceOrderType);
			if(isLoggingDebug()){
				logDebug("Order is placed with SSO-CSCRegistry-SOS channel");
			}
		}else{
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeOrderChannel(), pOrder.getSourceOfOrder());
		}//CR-251 changes
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderTaxErrorCustomAttributeName(), configuration.getCustomAttributeValueNo());
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeTaxTransactionId(), taxPriceInfo.getRadialTaxTransactionId());
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeSubtotal(), String.valueOf(pricingTools.round(priceInfo.getRawSubtotal())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeGiftwrapAmt(), String.valueOf(pricingTools.round(priceInfo.getGiftWrapPrice())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributePromoSavingsAmt(), String.valueOf(pricingTools.round(priceInfo.getDiscountAmount())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeShipAmt(), String.valueOf(pricingTools.round(priceInfo.getShipping())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeShipSurAmt(), String.valueOf(pricingTools.round(priceInfo.getTotalShippingSurcharge())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeSalesTaxAmt(), String.valueOf(pricingTools.round(taxPriceInfo.getEstimatedSalesTax())));
		populateCustomAttribute(pObjectFactory, customAttributes,configuration.getOrderCustomAttributeLocalTaxAmt(), String.valueOf(pricingTools.round(taxPriceInfo.getEstimatedLocalTax())));
		populateCustomAttribute(pObjectFactory, customAttributes,configuration.getOrderCustomAttributeIslandTaxAmt(), String.valueOf(pricingTools.round(taxPriceInfo.getEstimatedIslandTax())));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeEwasteAmt(), String.valueOf(pricingTools.round(priceInfo.getEwasteFees())));
		//populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeMattFeeAmt(), String.valueOf(priceInfo.getEwasteFees())); Mattress Fee
		//populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributePifFeeAmt(), String.valueOf(priceInfo.getEwasteFees())); PIF Fee
		double giftCardAppliedAmount = ((TRUOrderTools)getOrderManager().getOrderTools()).getGiftCardAppliedAmount(pOrder);
		if(giftCardAppliedAmount > TRUConstants.DOUBLE_ZERO){
			balanceAmount = pOrder.getPriceInfo().getTotal() - giftCardAppliedAmount;
			if(balanceAmount < TRUConstants.DOUBLE_ZERO){
				balanceAmount = TRUConstants.DOUBLE_ZERO;
			}
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeGiftCardAmt(), String.valueOf(pricingTools.round(giftCardAppliedAmount)));
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeBalanceAmt(), String.valueOf(pricingTools.round(balanceAmount)));
		}
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeOrderTotal(), String.valueOf(pricingTools.round(priceInfo.getTotal())));
		String rewardNumber = pOrder.getRewardNumber();
		if(!StringUtils.isBlank(rewardNumber)){
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeLoyaltyAccount(), rewardNumber);
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeLoyaltyProgram(), configuration.getOrderCustomAttributeLoyaltyProgramValue());
		}
		//populate Paypal related custom attributes
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		if(paymentGroups != null && !paymentGroups.isEmpty()){
			for (PaymentGroup paymentGroup : paymentGroups) {
				if(paymentGroup instanceof TRUPayPal){
					TRUPayPal paypalPG = (TRUPayPal) paymentGroup;
					populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributePaypalId(), paypalPG.getPayPalTransactionId());
					populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributePaypalPayerId(), paypalPG.getPayerId());
				}
			}
		}
		if(StringUtils.isNotBlank(pOrder.getEnteredBy()) && pOrder.getSourceOfOrder().equals(csrSourceOrderType)){
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeCSRNameID(), pOrder.getEnteredBy());
		}
		if(StringUtils.isNotBlank(pOrder.getEnteredBy()) && pOrder.getSourceOfOrder().equals(sosSourceOrderType)){
			String[] agentValue= pOrder.getEnteredBy().split(TRURadialOMConstants.SPLIT_HYPHEN);
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getSosAgentNamePropertyName(), agentValue[TRURadialOMConstants.INT_ZERO]);
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getSosAgentNumberPropertyName(), agentValue[TRURadialOMConstants.INT_ONE]);
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getSosAgentStoreIdPropertyName(), pOrder.getEnteredLocation());
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrsoCodePropertyName(), pOrder.getEnteredLocation()+TRURadialOMConstants.COLON+agentValue[TRURadialOMConstants.INT_ONE]+TRURadialOMConstants.COLON+agentValue[TRURadialOMConstants.INT_ZERO]);
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrsoTypePropertyName(), configuration.getOrsoTypeValuePropertyName());
		}
		pOrderXMLObj.setCustomAttributes(customAttributes);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderHeaderCustomAttributes()");
		}
	}

	/**
	 * This method is to validate source channel for channel shipping groups.
	 *
	 * @param pOrder - Order
	 * @param pSourceChannelRegStore - Reg store
	 * @return isssoOrder - true/false.
	 */
	@SuppressWarnings("unchecked")
	private boolean getChannelRegStore(TRUOrderImpl pOrder, String pSourceChannelRegStore) {
		if(isLoggingDebug()){
			logDebug("Enter  into  class: TRURadialOMUtils method: getChannelRegStore()");
			vlogDebug("Channel ret store values order : {0} and SourceChannelRegStore value : {1}", pOrder, pSourceChannelRegStore);
		}
		String sourceChannel=null;
		boolean isSsoOrder=Boolean.FALSE;
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		if(!shippingGroups.isEmpty() && shippingGroups!=null){
		for (ShippingGroup shippingGroup : shippingGroups) {
			if(shippingGroup instanceof TRUChannelHardgoodShippingGroup){
				TRUChannelHardgoodShippingGroup chgSG = (TRUChannelHardgoodShippingGroup) shippingGroup;
				sourceChannel = chgSG.getSourceChannel();
				if(isLoggingDebug()){
					logDebug("Source channel  value{0}:"+sourceChannel);
				}
				if(StringUtils.isNotBlank(sourceChannel) && sourceChannel.equals(pSourceChannelRegStore)){
					isSsoOrder = Boolean.TRUE;
					break;
				}
			}else if(shippingGroup instanceof TRUChannelInStorePickupShippingGroup){
				TRUChannelInStorePickupShippingGroup cisSG = (TRUChannelInStorePickupShippingGroup) shippingGroup;
				sourceChannel = cisSG.getSourceChannel();
				if(isLoggingDebug()){
					logDebug("Source channel  value{0}:"+sourceChannel);
				}
				if(StringUtils.isNotBlank(sourceChannel) && sourceChannel.equals(pSourceChannelRegStore)){
					isSsoOrder = Boolean.TRUE;
					break;
				}
			}
		}
	}
		if(isLoggingDebug()){
			logDebug("Exit  from  class: TRURadialOMUtils method: getChannelRegStore()");
		}
		return isSsoOrder;
	}

	/**
	 * This is common method to create custom attribute object and populate the
	 * passed the values.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCustomAttributes
	 *            - CustomAttributes Object
	 * @param pCustomAttributeName
	 *            - Custom attribute Name
	 * @param pCustomAttributeValue
	 *            - custom attribute name
	 */
	public void populateCustomAttribute(ObjectFactory pObjectFactory, CustomAttributes pCustomAttributes, String pCustomAttributeName, String pCustomAttributeValue){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateCustomAttribute()");
			vlogDebug("Creating custom attribute object withCustom Name : {0} and Custom Value : {1}", pCustomAttributeName, pCustomAttributeValue);
		}
		if(pCustomAttributeName == null || pCustomAttributeValue == null){
			return;
		}
		CustomAttribute customAttribute = pObjectFactory.createCustomAttribute();
		customAttribute.setName(getEncodedString(pCustomAttributeName));
		customAttribute.setValue(getEncodedString(pCustomAttributeValue));
		if(pCustomAttributes != null) {
			pCustomAttributes.getCustomAttribute().add(customAttribute);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateCustomAttribute()");
		}
	}

	/**
	 * This method is to populate the order header custom attributes for layaway
	 * payment.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Create Order object
	 */
	public void populateOrderHeaderCustomAttributesForLayawayPayment(TRULayawayOrderImpl pOrder,
			ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderHeaderCustomAttributesForLayawayPayment()");
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderTaxErrorCustomAttributeName(), configuration.getCustomAttributeValueNo());
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeGFSOrderId(), pOrder.getLayawayOMSOrderId());
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeLayaway(), configuration.getOrderCustomAttributeLayawayValue());
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getOrderCustomAttributeLayawayLocId(), pOrder.getEnteredLocation());
		pOrderXMLObj.setCustomAttributes(customAttributes);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderHeaderCustomAttributesForLayawayPayment()");
		}
	}

	/**
	 * This method is to populate order payments.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Order XML Object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderPayments(TRUOrderImpl pOrder,
			ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderPayments()");
		}
		if(pOrder == null || pObjectFactory == null || pOrderXMLObj == null){
			return;
		}
		ContactInfo billingAddress = null;
		Payments payments = pObjectFactory.createPayments();
		TRURadialOMConfiguration configuration = getOmConfiguration();
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		if((paymentGroups == null || paymentGroups.isEmpty()) && (pOrder.getPriceInfo().getTotal() == TRUConstants.DOUBLE_ZERO)){
			if (isLoggingDebug()) {
				vlogDebug("PaymentGroups value is null or Empty: {0} ",paymentGroups);
			}
			populateZeroDollarPaymentDetails(pOrder, pObjectFactory, pOrderXMLObj, payments);
		} else if (paymentGroups != null && !paymentGroups.isEmpty()) {
			for(PaymentGroup paymentGroup : paymentGroups){
				if (isLoggingDebug()) {
					vlogDebug("Payment Group: {0}",paymentGroup);
				}
				if(paymentGroup instanceof InStorePayment){
					InStorePayment instorePG = (InStorePayment) paymentGroup;
					PrePaidPayment prePaidPayment = pObjectFactory.createPrePaidPayment();
					prePaidPayment.setTenderType(getEncodedString(configuration.getTenderTypeMap().get(instorePG.getPaymentGroupClassType())));
					prePaidPayment.setAmount(BigDecimal.valueOf(pricingTools.round(pOrder.getPriceInfo().getTotal())));//XSD has to update
					billingAddress = (ContactInfo) pOrder.getBillingAddress();
					CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
					populateCustomAttribute(pObjectFactory, customAttributes, configuration.getPaymentCustomAttributeName(), configuration.getPaymentCustomAttributeValueMap().get(instorePG.getPaymentGroupClassType()));
					prePaidPayment.setCustomAttributes(customAttributes);
					payments.getCreditCardOrStoredValueCardOrPayPal().add(prePaidPayment);
				} else if(paymentGroup instanceof TRUGiftCard){
					TRUGiftCard gcPG = (TRUGiftCard) paymentGroup;
					populateGiftCardPaymentInformation(pObjectFactory, gcPG, payments, pOrder);
					billingAddress = (ContactInfo) pOrder.getBillingAddress();
				} else if(paymentGroup instanceof TRUCreditCard){
					TRUCreditCard ccPG = (TRUCreditCard) paymentGroup;
					populateCreditCardPaymentInformation(pObjectFactory, ccPG, payments, pOrder);
					billingAddress = (ContactInfo) pOrder.getBillingAddress();
				} else if(paymentGroup instanceof TRUPayPal){
					TRUPayPal paypalPG = (TRUPayPal) paymentGroup;
					populatePaypalPaymentInformation(pObjectFactory, paypalPG, payments, pOrder);
					billingAddress = (ContactInfo) pOrder.getBillingAddress();
				} else if(paymentGroup instanceof TRUApplePay){
					TRUApplePay applePayPG = (TRUApplePay) paymentGroup;
					populateApplePayPaymentInformation(pObjectFactory, applePayPG, payments, pOrder);
				}
			}
		}
		populateBillingAddress(pOrder, billingAddress, pObjectFactory, pOrderXMLObj);
		pOrderXMLObj.setPayments(payments);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderPayments()");
		}
	}

	/**
	 * This method is to populate the Apple Pay payment information.
	 *
	 * @param pObjectFactory - ObjectFactory
	 * @param pApplePayPG - Apple Pay PG
	 * @param pPayments - Payments
	 * @param pOrder - Order
	 */
	@SuppressWarnings("unchecked")
	private void populateApplePayPaymentInformation(ObjectFactory pObjectFactory, TRUApplePay pApplePayPG,
			Payments pPayments, TRUOrderImpl pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateApplePayPaymentInformation()");
		}
		if(pObjectFactory == null || pApplePayPG == null || pPayments == null || pOrder == null){
			return;
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		CreditCard applePay = pObjectFactory.createCreditCard();
		PaymentContext paymentContext = pObjectFactory.createPaymentContext();
		PaymentAccountUniqueId paymentAccountUniqueId = pObjectFactory.createPaymentAccountUniqueId();
		String creditCardNumber = pApplePayPG.getApplePayTransactionId();
		paymentContext.setOrderAppId(getEncodedString(configuration.getSourceIdValue()));
		paymentContext.setPaymentSessionId(getEncodedString(pOrder.getId()));
		if(StringUtils.isNotBlank(creditCardNumber) && StringUtils.isNumericOnly(creditCardNumber)){
			paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.FALSE));
		} else if(StringUtils.isNotBlank(creditCardNumber)){
			paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.TRUE));
		}
		paymentAccountUniqueId.setValue(getEncodedString(creditCardNumber));
		paymentContext.setPaymentAccountUniqueId(paymentAccountUniqueId);
		List<PaymentStatus> authorizationStatuses = pApplePayPG.getAuthorizationStatus();
		for (PaymentStatus paymentStatus : authorizationStatuses) {
			TRUApplePayStatus applePayStatus = (TRUApplePayStatus) paymentStatus;
			Authorization authorization = pObjectFactory.createAuthorization();
			authorization.setAmountAuthorized(BigDecimal.valueOf(pApplePayPG.getAmountAuthorized()));
			authorization.setAVSResponseCode(getEncodedString(applePayStatus.getAvsCode()));
			authorization.setBankAuthorizationCode(applePayStatus.getBankAuthCode());
			authorization.setCVV2ResponseCode(getEncodedString(applePayStatus.getCvvCode()));
			authorization.setResponseCode(getEncodedString(pApplePayPG.getResponseCode()));
			paymentContext.setTenderType(getEncodedString(applePayStatus.getTenderType()));
			ExtendedAuthorizationResponseCodes extendedAuthorizationResponseCodes = pObjectFactory.createExtendedAuthorizationResponseCodes();
			//extendedAuthorizationResponseCodes.setReasonCode(getEncodedString(applePayStatus.getAuthResponseCode()));
			extendedAuthorizationResponseCodes.setResponseCodeDescription(null);
			authorization.setExtendedAuthorizationResponseCodes(extendedAuthorizationResponseCodes);
			applePay.setAuthorization(authorization);
		}
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			applePay.setCreateTimeStamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		applePay.setPaymentRequestId(getEncodedString(pApplePayPG.getTransactionId()));
		//applePay.setExpirationDate(getEncodedString(pApplePayPG.getExpirationYear()+TRUConstants.HYPHEN+pApplePayPG.getExpirationMonth()));
		applePay.setAmount(BigDecimal.valueOf(pApplePayPG.getAmount()));
		applePay.setPaymentContext(paymentContext);
		if(pOrder instanceof TRUOrderImpl){
			TRUOrderImpl order = (TRUOrderImpl) pOrder;
			if(order.isFinanceAgreed()){
				int financingTermsAvailed = order.getFinancingTermsAvailed();
				if(financingTermsAvailed == TRURadialOMConstants.FINANCING_SIX_MONTHS){
					applePay.setPurchasePlanCode(TRURadialOMConstants.STRING_ONE);
					applePay.setPurchasePlanDescription(getEncodedString(getOmConfiguration().getSixMonthsFinancing()));
				} else if(financingTermsAvailed == TRURadialOMConstants.FINANCING_TWELVE_MONTHS){
					applePay.setPurchasePlanCode(TRURadialOMConstants.STRING_TWO);
					applePay.setPurchasePlanDescription(getEncodedString(getOmConfiguration().getTwelveMonthsFinancing()));
				}
			}
		} 		
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getPaymentCustomAttributeNameForApplePay(), configuration.getPaymentCustomAttributeValueForApplePay());
		applePay.setCustomAttributes(customAttributes);
		pPayments.getCreditCardOrStoredValueCardOrPayPal().add(applePay);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateApplePayPaymentInformation()");
		}
	}

	/**
	 * Sets the empty payment group details to order.
	 *
	 * @param pOrder
	 * 			- the order
	 * @param pObjectFactory
	 * 			- the object factory
	 * @param pOrderXMLObj 
	 * 			- the order xml obj
	 * @param pPayments 
	 * 			- the payments
	 */
	private void populateZeroDollarPaymentDetails(TRUOrderImpl pOrder, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj, Payments pPayments) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateZeroDollarPaymentDetails()");
		}
		if(pOrder == null || pObjectFactory == null || pOrderXMLObj == null || pPayments == null){
			return;
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		TRUPricingTools pricingTools = getCatalogTools().getPricingTools();
		PrePaidPayment prePaidPayment = pObjectFactory.createPrePaidPayment();
		prePaidPayment.setTenderType(getEncodedString(configuration.getTenderTypeMap().get(configuration.getZeroDollarTenderTypeKey())));
		prePaidPayment.setAmount(BigDecimal.valueOf(pricingTools.round(pOrder.getPriceInfo().getTotal())));
		ContactInfo billingAddress = (ContactInfo) pOrder.getBillingAddress();
		if(billingAddress != null){
			populateBillingAddress(pOrder, billingAddress, pObjectFactory, pOrderXMLObj);
		}
		if(isLoggingDebug()){
			vlogDebug("Billing address for Zero dollar order is : {0}", billingAddress);
		}
		pPayments.getCreditCardOrStoredValueCardOrPayPal().add(prePaidPayment);
		pOrderXMLObj.setPayments(pPayments);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateZeroDollarPaymentDetails()");
		}
	}

	/**
	 * This method is to populate order payments layaway payment..
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderXMLObj
	 *            - Order XML Object
	 */
	@SuppressWarnings("unchecked")
	private void populateOrderPaymentsForLayawayPayment(TRULayawayOrderImpl pOrder,
			ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderPaymentsForLayawayPayment()");
		}
		if(pOrder == null || pObjectFactory == null || pOrderXMLObj == null){
			return;
		}
		Payments payments = pObjectFactory.createPayments();
		// Calling method to set 1st gift card payment group before other payment group
		List<PaymentGroup>  paymentGroups = orderPaymentGroupBasedOnType(pOrder.getPaymentGroups());
		if (isLoggingDebug()) {
			vlogDebug("paymentGroups: {0}",paymentGroups);
		}
		if(paymentGroups == null || paymentGroups.isEmpty()){
			return;
		}
		for(PaymentGroup paymentGroup : paymentGroups){
			if (isLoggingDebug()) {
				vlogDebug("Payment Group: {0}",paymentGroup);
			}
			if(paymentGroup instanceof TRUGiftCard){
				TRUGiftCard gcPG = (TRUGiftCard) paymentGroup;
				populateGiftCardPaymentInformation(pObjectFactory, gcPG, payments, pOrder);
			} else if(paymentGroup instanceof TRUCreditCard){
				TRUCreditCard ccPG = (TRUCreditCard) paymentGroup;
				populateCreditCardPaymentInformation(pObjectFactory, ccPG, payments, pOrder);
			}
		}
		pOrderXMLObj.setPayments(payments);
		populateBillingAddressForLayaway(pOrderXMLObj, pOrder, pObjectFactory);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderPaymentsForLayawayPayment()");
		}
	}
	/**
	 * This method is to populate the paypal payment information for order.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pPaypalPG
	 *            - Paypal PG
	 * @param pPayments
	 *            - Payments
	 * @param pOrder
	 *            - Order
	 */
	private void populatePaypalPaymentInformation(ObjectFactory pObjectFactory,
			TRUPayPal pPaypalPG, Payments pPayments, Order pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populatePaypalPaymentInformation()");
		}
		if(pObjectFactory == null || pPaypalPG == null || pPayments == null || pOrder == null){
			return;
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		PayPal payPal = pObjectFactory.createPayPal();
		PaymentContext paymentContext = pObjectFactory.createPaymentContext();
		PaymentAccountUniqueId paymentAccountUniqueId = pObjectFactory.createPaymentAccountUniqueId();
		String paymentGroupClassType = pPaypalPG.getPaymentGroupClassType();
		paymentContext.setOrderAppId(getEncodedString(configuration.getSourceIdValue()));
		paymentContext.setPaymentSessionId(getEncodedString(pOrder.getId()));
		paymentContext.setTenderType(getEncodedString(configuration.getTenderTypeMap().get(paymentGroupClassType)));
		paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.TRUE));
		paymentAccountUniqueId.setValue(getEncodedString(configuration.getPaypalPaymentAccountUniqueId()));
		paymentContext.setPaymentAccountUniqueId(paymentAccountUniqueId);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			payPal.setCreateTimeStamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		payPal.setAmount(BigDecimal.valueOf(pPaypalPG.getAmount()));
		payPal.setAmountAuthorized(BigDecimal.valueOf(pPaypalPG.getAmountAuthorized()));
		Authorization authorization = pObjectFactory.createAuthorization();
		authorization.setResponseCode(getEncodedString(TRURadialOMConstants.PAYPAL_SUCCESS_STATUS));
		payPal.setAuthorization(authorization);
		payPal.setPaymentContext(paymentContext);
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getPaymentCustomAttributeName(), configuration.getPaymentCustomAttributeValueMap().get(paymentGroupClassType));
		payPal.setCustomAttributes(customAttributes);
		pPayments.getCreditCardOrStoredValueCardOrPayPal().add(payPal);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populatePaypalPaymentInformation()");
		}
	}

	/**
	 * This method is to populate the Gift card payment information to Order.
	 *
	 * @param pObjectFactory            - Object Factory
	 * @param pGcPG            - Gift card payment group
	 * @param pPayments            - Payments XML Object
	 * @param pOrder - Order
	 */
	private void populateGiftCardPaymentInformation(ObjectFactory pObjectFactory, TRUGiftCard pGcPG, Payments pPayments, Order pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateGiftCardPaymentInformation()");
		}
		if(pObjectFactory == null || pGcPG == null || pPayments == null || pOrder == null){
			return;
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		StoredValueCard storedValueCard = pObjectFactory.createStoredValueCard();
		PaymentContext paymentContext = pObjectFactory.createPaymentContext();
		PaymentAccountUniqueId paymentAccountUniqueId = pObjectFactory.createPaymentAccountUniqueId();
		String paymentGroupClassType = pGcPG.getPaymentGroupClassType();
		paymentContext.setOrderAppId(getEncodedString(configuration.getSourceIdValue()));
		paymentContext.setPaymentSessionId(getEncodedString(pOrder.getId()));
		paymentContext.setTenderType(getEncodedString(configuration.getTenderTypeMap().get(paymentGroupClassType)));
		paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.FALSE));
		paymentAccountUniqueId.setValue(getEncodedString(pGcPG.getGiftCardNumber()));
		paymentContext.setPaymentAccountUniqueId(paymentAccountUniqueId);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			storedValueCard.setCreateTimeStamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		storedValueCard.setPaymentRequestId(getEncodedString(pGcPG.getTransactionId()));
		storedValueCard.setAmount(BigDecimal.valueOf(pGcPG.getAmount()));
		storedValueCard.setPaymentContext(paymentContext);
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getPaymentCustomAttributeName(), configuration.getPaymentCustomAttributeValueMap().get(paymentGroupClassType));
		storedValueCard.setCustomAttributes(customAttributes);
		pPayments.getCreditCardOrStoredValueCardOrPayPal().add(storedValueCard);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateGiftCardPaymentInformation()");
		}
	}

	/**
	 * This method is to populate the credit card payment information.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pCcPG
	 *            - Credit card PG
	 * @param pPayments
	 *            - Payments Object
	 * @param pOrder - Order
	 */
	@SuppressWarnings("unchecked")
	private void populateCreditCardPaymentInformation(
			ObjectFactory pObjectFactory, TRUCreditCard pCcPG, Payments pPayments, Order pOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateCreditCardPaymentInformation()");
		}
		if(pObjectFactory == null || pCcPG == null || pPayments == null || pOrder == null){
			return;
		}
		TRURadialOMConfiguration configuration = getOmConfiguration();
		CreditCard creditCard = pObjectFactory.createCreditCard();
		PaymentContext paymentContext = pObjectFactory.createPaymentContext();
		PaymentAccountUniqueId paymentAccountUniqueId = pObjectFactory.createPaymentAccountUniqueId();
		String creditCardType = pCcPG.getCreditCardType();
		String creditCardNumber = pCcPG.getCreditCardNumber();
		paymentContext.setOrderAppId(getEncodedString(configuration.getSourceIdValue()));
		paymentContext.setPaymentSessionId(getEncodedString(pOrder.getId()));
		paymentContext.setTenderType(getEncodedString(configuration.getTenderTypeMap().get(creditCardType)));
		if(StringUtils.isNumericOnly(creditCardNumber)){
			paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.FALSE));
		} else {
			paymentAccountUniqueId.setIsToken(String.valueOf(Boolean.TRUE));
		}
		paymentAccountUniqueId.setValue(getEncodedString(creditCardNumber));
		paymentContext.setPaymentAccountUniqueId(paymentAccountUniqueId);
		List<PaymentStatus> authorizationStatuses = pCcPG.getAuthorizationStatus();
		for (PaymentStatus paymentStatus : authorizationStatuses) {
			TRUCreditCardStatus ccStatus = (TRUCreditCardStatus) paymentStatus;
			if(ccStatus.getTransactionSuccess()){
				Authorization authorization = pObjectFactory.createAuthorization();
				authorization.setAmountAuthorized(BigDecimal.valueOf(pCcPG.getAmountAuthorized()));
				authorization.setAVSResponseCode(getEncodedString(ccStatus.getAvsCode()));
				authorization.setBankAuthorizationCode(ccStatus.getBankAuthCode());
				authorization.setCVV2ResponseCode(getEncodedString(ccStatus.getCvvCode()));
				authorization.setResponseCode(getEncodedString(pCcPG.getResponseCode()));
				ExtendedAuthorizationResponseCodes extendedAuthorizationResponseCodes = pObjectFactory.createExtendedAuthorizationResponseCodes();
				extendedAuthorizationResponseCodes.setReasonCode(getEncodedString(ccStatus.getAuthResponseCode()));
				extendedAuthorizationResponseCodes.setResponseCodeDescription(null);
				authorization.setExtendedAuthorizationResponseCodes(extendedAuthorizationResponseCodes);
				creditCard.setAuthorization(authorization);
			}
		}
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(getTimeZone());
		XMLGregorianCalendar xgcal;
		try {
			xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			creditCard.setCreateTimeStamp(xgcal);
		} catch (DatatypeConfigurationException exc) {
			if(isLoggingError()){
				vlogError("DatatypeConfigurationException : Exce : {0}", exc);
			}
		}
		creditCard.setPaymentRequestId(getEncodedString(pCcPG.getTransactionId()));
		creditCard.setExpirationDate(getEncodedString(pCcPG.getExpirationYear()+TRUConstants.HYPHEN+pCcPG.getExpirationMonth()));
		creditCard.setAmount(BigDecimal.valueOf(pCcPG.getAmount()));
		creditCard.setPaymentContext(paymentContext);
		if(pOrder instanceof TRUOrderImpl){
			TRUOrderImpl order = (TRUOrderImpl) pOrder;
			if(order.isFinanceAgreed()){
				int financingTermsAvailed = order.getFinancingTermsAvailed();
				if(financingTermsAvailed == TRURadialOMConstants.FINANCING_SIX_MONTHS){
					creditCard.setPurchasePlanCode(TRURadialOMConstants.STRING_ONE);
					creditCard.setPurchasePlanDescription(getEncodedString(getOmConfiguration().getSixMonthsFinancing()));
				} else if(financingTermsAvailed == TRURadialOMConstants.FINANCING_TWELVE_MONTHS){
					creditCard.setPurchasePlanCode(TRURadialOMConstants.STRING_TWO);
					creditCard.setPurchasePlanDescription(getEncodedString(getOmConfiguration().getTwelveMonthsFinancing()));
				}
			}
		} 		
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getPaymentCustomAttributeName(), configuration.getPaymentCustomAttributeValueMap().get(creditCardType));
		creditCard.setCustomAttributes(customAttributes);
		pPayments.getCreditCardOrStoredValueCardOrPayPal().add(creditCard);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateCreditCardPaymentInformation()");
		}
	}

	/**
	 *	This method is to populate the order billing address.
	 *
	 * @param pOrder
	 * 				- Order
	 * @param pBillingAddress
	 * 				- Billing address
	 * @param pObjectFactory
	 * 				- Object factory
	 * @param pOrderXMLObj
	 * 				- Create order object
	 * @return the address
	 */
	private Address populateBillingAddress(Order pOrder, ContactInfo pBillingAddress, ObjectFactory pObjectFactory, OrderRequest pOrderXMLObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateBillingAddress()");
			vlogDebug("Populating billing address of order : {0}", pOrder);
		}
		Address billingAddr = pObjectFactory.createAddress();
		if(pOrder == null || pBillingAddress == null || pObjectFactory == null || pOrderXMLObj == null){
			return billingAddr;
		}
		TRUOrderImpl order = null;
		if(pOrder instanceof TRUOrderImpl){
			order = (TRUOrderImpl) pOrder;
		}
		RepositoryItem billingAddress = (RepositoryItem)order.getRepositoryItem().getPropertyValue(TRUConstants.BILLING_ADDRESS);
		String state = null;
		if(billingAddress != null){
			state = (String) billingAddress.getPropertyValue(getOmConfiguration().getStatePropertyName());
		}
		
		billingAddr.setType(getEncodedString(getOmConfiguration().getAddressTypeBilling()));
		billingAddr.setLine1(getEncodedString(pBillingAddress.getAddress1()));
		billingAddr.setLine2(getEncodedString(pBillingAddress.getAddress2()));
		billingAddr.setLine3(getEncodedString(pBillingAddress.getAddress3()));
		billingAddr.setCity(getEncodedString(pBillingAddress.getCity()));
		billingAddr.setState(getEncodedString(state));
		billingAddr.setCountryCode(getEncodedString(pBillingAddress.getCountry()));
		billingAddr.setPostalCode(getEncodedString(pBillingAddress.getPostalCode()));
		billingAddr.setPhone(getEncodedString(pBillingAddress.getPhoneNumber()));
		populateCustomerInfo(order, billingAddr, pObjectFactory, pBillingAddress);
		pOrderXMLObj.setAddress(billingAddr);
		if(isLoggingDebug()){
			vlogDebug("Billing Address Object : {0}", billingAddr);
			logDebug("Exit from class: TRURadialOMUtils method: populateBillingAddress()");
		}
		return billingAddr;
	}


	/**
	 * This method is to populate the shipping address at line level.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pShipGroup
	 *            - Shipping group
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderItem
	 *            - OderItem
	 */
	public void populateShippingAddress(TRUOrderImpl pOrder, ShippingGroup pShipGroup,ObjectFactory pObjectFactory, OrderItem pOrderItem ){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateShippingAddress()");
		}
		ContactInfo shippingAddress = null;
		if(pShipGroup instanceof InStorePickupShippingGroup || pShipGroup instanceof TRUInStorePickupShippingGroup){
			TRUInStorePickupShippingGroup instoreSG = (TRUInStorePickupShippingGroup) pShipGroup;
			RepositoryItem repositoryItem = getLocationRepositoryItem(instoreSG.getLocationId());
			populateStoreFrontAddress(pOrder, instoreSG, pObjectFactory, pOrderItem, repositoryItem);
			populateISPUProxyAddressDetails(instoreSG,pOrderItem, pObjectFactory, repositoryItem);
		} else if(pShipGroup instanceof TRUHardgoodShippingGroup){
			TRUHardgoodShippingGroup hgSG = (TRUHardgoodShippingGroup) pShipGroup;
			shippingAddress = (ContactInfo) hgSG.getShippingAddress();
		} else if(pShipGroup instanceof TRUChannelHardgoodShippingGroup){
			TRUChannelHardgoodShippingGroup chgSG = (TRUChannelHardgoodShippingGroup) pShipGroup;
			shippingAddress = (ContactInfo) chgSG.getShippingAddress();
		}
		if(isLoggingDebug()) {
			vlogDebug("Shipping Address Object : {0}", shippingAddress);
		}
		if(shippingAddress != null) {
			Address shipAddr = pObjectFactory.createAddress();
			shipAddr.setType(getEncodedString(getOmConfiguration().getAddressTypeShipping()));
			shipAddr.setLine1(getEncodedString(shippingAddress.getAddress1()));
			shipAddr.setLine2(getEncodedString(shippingAddress.getAddress2()));
			shipAddr.setLine3(getEncodedString(shippingAddress.getAddress3()));
			shipAddr.setCity(getEncodedString(shippingAddress.getCity()));
			shipAddr.setState(getEncodedString(shippingAddress.getState()));
			shipAddr.setCountryCode(getEncodedString(shippingAddress.getCountry()));
			shipAddr.setPostalCode(getEncodedString(shippingAddress.getPostalCode()));
			shipAddr.setPhone(getEncodedString(shippingAddress.getPhoneNumber()));
			populateCustomerInfo(pOrder, shipAddr, pObjectFactory, shippingAddress);
			pOrderItem.getAddress().add(shipAddr);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateShippingAddress()");
		}
	}

	/**
	 * Populate ISPU Item Proxy Address Details.
	 *
	 * @param pInstoreSG the TRUInStorePickupShippingGroup
	 * @param pOrderItem the order item
	 * @param pObjectFactory the object factory
	 * @param pLocationRepoItem the location repo item
	 */
	private void populateISPUProxyAddressDetails(TRUInStorePickupShippingGroup pInstoreSG, OrderItem pOrderItem, ObjectFactory pObjectFactory, RepositoryItem pLocationRepoItem) {
		if(isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: populateISPUProxyAddressDetails()");
			vlogDebug("TRUInStorePickupShippingGroup Object : {0}", pInstoreSG);
		}
		if(pInstoreSG.getAltFirstName() != null && pInstoreSG.getAltLastName() != null && pInstoreSG.getAltPhoneNumber() != null &&
				pInstoreSG.getAltPhoneNumber() != null) {
			Address address = pObjectFactory.createAddress();
			address.setType(getEncodedString(getOmConfiguration().getAddressTypeProxy()));
			setStoreLocatorItemDetails(address, pLocationRepoItem);
			Customer customer = pObjectFactory.createCustomer();
			CustomerName customerName = pObjectFactory.createCustomerName();
			customerName.setFirstName(getEncodedString(pInstoreSG.getAltFirstName()));
			customerName.setLastName(getEncodedString(pInstoreSG.getAltLastName()));
			customer.setCustomerName(customerName);
			customer.setEmailAddress(getEncodedString(pInstoreSG.getAltEmail()));
			address.setCustomer(customer);
			pOrderItem.getAddress().add(address);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateISPUProxyAddressDetails()");
		}
	}

	/**
	 * This method is to populate the customer info.
	 * 
	 * @param pOrder
	 *            - Order
	 * @param pAddress
	 *            - Address XML Object
	 * @param pObjectFactory
	 *            - Object Factory
	 * @param pShippingAddress  - Contact Info
	 */
	public void populateCustomerInfo(TRUOrderImpl pOrder, Address pAddress, ObjectFactory pObjectFactory, ContactInfo pShippingAddress){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateCustomerInfo()");
		}
		if(pObjectFactory == null || pOrder == null){
			return;
		}
		RepositoryItem profile;
		Customer customer = null;
		CustomerName customerName = null;
		TRUPropertyManager propertyManager = getPropertyManager();
		try {
			profile = getOrderManager().getOrderTools().getProfileTools().getProfileForOrder(pOrder);
			if(isLoggingDebug()) {
				vlogDebug("Profile RepositoryItem : {0} OrderItem : {1} Shipping Address : {2} ", profile, pOrder, pShippingAddress);
			}
			customer = pObjectFactory.createCustomer();
			customerName = pObjectFactory.createCustomerName();
			if(profile != null && !profile.isTransient()){				
				customer.setCustomerId(getEncodedString(profile.getRepositoryId()));
				customer.setEmailAddress(getEncodedString((String) profile.getPropertyValue(propertyManager.getEmailAddressPropertyName())));
				if(!StringUtils.isBlank(pShippingAddress.getFirstName())){
					customerName.setFirstName(getEncodedString(pShippingAddress.getFirstName()));
				} else {
					customerName.setFirstName(getEncodedString((String) profile.getPropertyValue(propertyManager.getFirstNamePropertyName())));
				}
				if(!StringUtils.isBlank(pShippingAddress.getLastName())){
					customerName.setLastName(getEncodedString(pShippingAddress.getLastName()));
				} else {
					customerName.setLastName(getEncodedString((String) profile.getPropertyValue(propertyManager.getLastNamePropertyName())));
				}
				customer.setCustomerName(customerName);
				if(getOmConfiguration().isIncludeTile()){
					customer.setTitle(getEncodedString(getOmConfiguration().getCustomerTitle()));
				}
				pAddress.setCustomer(customer);
			} else if(profile != null && profile.isTransient()){
				customer.setEmailAddress(getEncodedString(pOrder.getEmail()));
				customerName.setFirstName(getEncodedString(pShippingAddress.getFirstName()));
				customerName.setLastName(getEncodedString(pShippingAddress.getLastName()));
				customer.setCustomerName(customerName);
				if(getOmConfiguration().isIncludeTile()){
					customer.setTitle(getEncodedString(getOmConfiguration().getCustomerTitle()));
				}
				pAddress.setCustomer(customer);
			}
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While getting profile for order : {0} and exception is : {1}", pOrder, exc);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateCustomerInfo()");
		}
	}

	/**
	 * This method is to populate the item level custom attributes.
	 *
	 * @param pOrder            - Order
	 * @param pShipGroup            - ShippingGroup
	 * @param pShipCIRel            - Shipping Group relationship
	 * @param pItemMetaInfo - MetaInfo
	 * @param pObjectFactory            - ObjectFactory
	 * @param pOrderItem            - OrderItem
	 */
	public void populateOrderItemCustomAttributes(TRUOrderImpl pOrder, ShippingGroup pShipGroup, TRUShippingGroupCommerceItemRelationship pShipCIRel,
			TRUCommerceItemMetaInfo pItemMetaInfo, ObjectFactory pObjectFactory, OrderItem pOrderItem){
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderItemCustomAttributes()");
			vlogDebug("Order : {0} and ShippingAddress : {1}", pOrder, pShipGroup);
			vlogDebug("TRUShippingGroupCommerceItemRelationship: {0} and TRUCommerceItemMetaInfo Object :{1}", pShipCIRel, pItemMetaInfo);
			
		}
		if(pObjectFactory == null || pOrder == null || pOrderItem == null){
			return;
		}
		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
		TRURadialOMConfiguration configuration = getOmConfiguration();
		ContactInfo shippingAddress=null;
		TRUCatalogTools catalogTools = getCatalogTools();
		TRUCatalogProperties catalogProperties = (TRUCatalogProperties) catalogTools.getCatalogProperties();
		PricingTools pricingTools = getCatalogTools().getPromotionTools().getPricingTools();
		CustomAttributes customAttributes = pObjectFactory.createCustomAttributes();
		if(pShipGroup instanceof TRUChannelHardgoodShippingGroup || pShipGroup instanceof TRUChannelInStorePickupShippingGroup){
			populateRegistryCustomAttributes(customAttributes, pObjectFactory, pShipGroup);
		}
		if((pShipGroup instanceof TRUInStorePickupShippingGroup || pShipGroup instanceof TRUChannelInStorePickupShippingGroup) && pShipCIRel.isWarehousePickup()){
			TRUInStorePickupShippingGroup instoreSG = (TRUInStorePickupShippingGroup) pShipGroup;
				populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeDCValue(), instoreSG.getWarhouseLocationCode());
		}
		if(pShipGroup instanceof TRUHardgoodShippingGroup){
			TRUHardgoodShippingGroup hgSG = (TRUHardgoodShippingGroup) pShipGroup;
			 shippingAddress=(ContactInfo)hgSG.getShippingAddress();
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeEDDStartDate(), getEstimateShippingDate(pShipCIRel.getEstimateShipWindowMin()));
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeEDDEndDate(), getEstimateShippingDate(pShipCIRel.getEstimateShipWindowMax()));
			if(hgSG.getShippingMethodName() != null && hgSG.getDeliveryTimeFrame() != null){
				String shipMethodMSG = hgSG.getShippingMethodName()+TRUConstants.COLON+TRURadialOMConstants.OPEN_BRACES+hgSG.getDeliveryTimeFrame()+TRURadialOMConstants.CLOSED_BRACES;
				populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeShipMessage(), shipMethodMSG);
			}
		}
		TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) pShipCIRel.getCommerceItem().getPriceInfo();
		double unitPrice = (double) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getUnitPricePropertyName());
		long qty = (long) pItemMetaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName());
		double itemDiscountShare = TRUConstants.DOUBLE_ZERO;
		PromotionalDiscounts promotionalDiscounts = pOrderItem.getPricing().getMerchandise().getPromotionalDiscounts();
		if(promotionalDiscounts != null && promotionalDiscounts.getDiscount() != null && !promotionalDiscounts.getDiscount().isEmpty()){
			List<Discount> discounts = promotionalDiscounts.getDiscount();
			for (Discount discount : discounts) {
				String promoLevel = discount.getPromoLevel();
				double amount2 = discount.getAmount().doubleValue();
				if(promoLevel.equals(TRURadialOMConstants.PROMO_LEVEL_ITEM)){
					itemDiscountShare +=  amount2;
				}
			}
		}
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeListPrice(), String.valueOf(priceInfo.getListPrice()));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeItemPrice(), String.valueOf(pricingTools.round((unitPrice*qty)-itemDiscountShare)));
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributePdpURL(), getProductPageURL(pOrderItem.getItemId()));
		RepositoryItem skuItem = (RepositoryItem) pShipCIRel.getCommerceItem().getAuxiliaryData().getCatalogRef();
		String imageName = (String) skuItem.getPropertyValue(getCommercePropertyManager().getPrimaryImage());
		String imageUrl = imageName+TRUConstants.QUESTION_MARK_STRING+TRUConstants.FIT+
				TRUConstants.EQUALS_SYMBOL_STRING+TRUConstants.INSIDE+TRUConstants.PIPE_STRING+configuration.getImageWidth()+TRUConstants.COLON_STRING+configuration.getImageHeight();
		populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeImageURL(), imageUrl);
		if(priceInfo != null && !StringUtils.isBlank(priceInfo.getDealID())){
			double tprPrice = priceInfo.getListPrice()-priceInfo.getSalePrice();
			tprPrice = pricingTools.round(tprPrice);
			StringBuilder sb = new StringBuilder(priceInfo.getDealID());
			sb.append(TRUConstants.PIPE_STRING).append(priceInfo.getListPrice()).append(TRUConstants.PIPE_STRING).append(priceInfo.getSalePrice()).append(TRUConstants.PIPE_STRING).append(decimalFormat.format(tprPrice));
			populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeTPR(), sb.toString());					
		}
		if(priceInfo != null && priceInfo.getItemShippingSurcharge() > TRUConstants.DOUBLE_ZERO){
			RepositoryItem prodctItem=(RepositoryItem)pShipCIRel.getCommerceItem().getAuxiliaryData().getProductRef();
			String state=shippingAddress.getState();
			if(prodctItem.getPropertyValue(catalogProperties.getEwasteSurchargeStatePropertyName())!=null ){
			if(!((String)prodctItem.getPropertyValue(catalogProperties.getEwasteSurchargeStatePropertyName())).equalsIgnoreCase(state)){
					populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeSurcharge(), String.valueOf(pricingTools.round(priceInfo.getItemShippingSurcharge()*qty/pShipCIRel.getCommerceItem().getQuantity())));
				}
			}else{
				populateCustomAttribute(pObjectFactory, customAttributes, configuration.getItemCustomAttributeSurcharge(), String.valueOf(pricingTools.round(priceInfo.getItemShippingSurcharge()*qty/pShipCIRel.getCommerceItem().getQuantity())));
			}
		}
		pOrderItem.getCustomAttributes().add(customAttributes);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderItemCustomAttributes()");
		}
	}

	/**
	 * This method is to populate the customization data for registry item.
	 * 
	 * @param pShipGroup
	 *            - Shipping Group
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderItem
	 *            - OrderItem
	 * @param pCustomizationList - customizationList
	 */
	private void populateRegistryCustomizationData(ShippingGroup pShipGroup,
			ObjectFactory pObjectFactory, OrderItem pOrderItem, CustomizationList pCustomizationList) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateRegistryCustomizationData()");
		}
		if(pShipGroup == null || pOrderItem == null || pObjectFactory == null || pCustomizationList == null){
			return;
		}
		TRUChannelHardgoodShippingGroup channerHGSG = (TRUChannelHardgoodShippingGroup) pShipGroup;
		CustomizationData customizationData = pObjectFactory.createCustomizationData();
		TRURadialOMConfiguration configuration = getOmConfiguration();
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataType(getEncodedString(getOmConfiguration().getCustomizationDataRegistryMessageType())));
		Message message = pObjectFactory.createMessage();
		String sourceChannel = channerHGSG.getSourceChannel();
		StringBuilder builder = new StringBuilder();
		builder.append(TRURadialOMConstants.CHANNEL_FROM+channerHGSG.getChannelUserName());
		if(!StringUtils.isBlank(channerHGSG.getChannelCoUserName())){
			builder.append(TRURadialOMConstants.CHANNEL_AND+channerHGSG.getChannelCoUserName());
		}
		builder.append(TRUConstants.WHITE_SPACE);
		if(!StringUtils.isBlank(sourceChannel) && sourceChannel.equals(configuration.getSourceChannelRegistry())){
			builder.append(configuration.getSourceChannelRegistryValue());
		}else if(!StringUtils.isBlank(sourceChannel) && sourceChannel.equals(configuration.getSourceChannelWishlist())){
			builder.append(configuration.getSourceChannelWishlistValue());
		}
		builder.append(TRURadialOMConstants.CHANNEL_HASH+channerHGSG.getChannelId());
		message.setBody(getEncodedString(builder.toString()));
		customizationData.getNameOrValueOrType().add(pObjectFactory.createCustomizationDataMessage(message));
		pCustomizationList.getCustomizationData().add(customizationData);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateRegistryCustomizationData()");
		}
	}

	/**
	 * This method is to populate the registry custom attributes.
	 *
	 * @param pCustomAttributes            - CustomAttributes object
	 * @param pObjectFactory            - ObjectFactory
	 * @param pShipGroup            - ShippingGroup
	 */
	private void populateRegistryCustomAttributes(CustomAttributes pCustomAttributes, ObjectFactory pObjectFactory,
			ShippingGroup pShipGroup) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateRegistryCustomAttributes()");
		}
		if(pObjectFactory == null || pCustomAttributes == null || pShipGroup == null){
			return;
		}
		String channelType = null;
		String channelId = null;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		TRUCommercePropertyManager commercePropertyManager = ((TRUOrderTools)getOrderManager().getOrderTools()).getCommercePropertyManager();
		if(pShipGroup instanceof TRUChannelHardgoodShippingGroup){
			TRUChannelHardgoodShippingGroup chgSG = (TRUChannelHardgoodShippingGroup) pShipGroup;
			 channelType = chgSG.getChannelType();
			 channelId =  chgSG.getChannelId();
		}else if(pShipGroup instanceof TRUChannelInStorePickupShippingGroup){
			TRUChannelInStorePickupShippingGroup iSPUChgSG = (TRUChannelInStorePickupShippingGroup) pShipGroup;
			 channelType = iSPUChgSG.getChannelType();
			 channelId =  iSPUChgSG.getChannelId();
		}
		populateCustomAttribute(pObjectFactory, pCustomAttributes, configuration.getItemCustomAttributeRegistryID(), channelId);
		if(!StringUtils.isBlank(channelType)){
			if(channelType.equalsIgnoreCase(commercePropertyManager.getWishlist())){
				populateCustomAttribute(pObjectFactory, pCustomAttributes, configuration.getItemCustomAttributeRegistryType(), configuration.getGiftRegistryCustomAttributeValue());
			}else if(channelType.equalsIgnoreCase(commercePropertyManager.getRegistry())){
				populateCustomAttribute(pObjectFactory, pCustomAttributes, configuration.getItemCustomAttributeRegistryType(), configuration.getBabyRegistryCustomAttributeValue());
			}
		}
		populateCustomAttribute(pObjectFactory, pCustomAttributes, configuration.getItemCustomAttributeRegistryShip(), configuration.getItemCustomAttributeRegistryShipValue());
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateRegistryCustomAttributes()");
		}
	}
	
	/**
	 * This method is to prepare the meta info discount share map.
	 *
	 * @param pOrder - TRUOrderImpl
	 * @return map - Map with discount share details.
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Double> prepareItemDiscountShareMap(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: prepareItemDiscountShareMap()");
		}
		Map<String, Double> itemDiscShareMap = new HashMap<String, Double>();
		PricingTools pricingTools = getCatalogTools().getPromotionTools().getPricingTools();
		if(pOrder != null){
			List <CommerceItem>commerceItems = pOrder.getCommerceItems();
			if(commerceItems != null && !commerceItems.isEmpty()){
				List<TRUShippingGroupCommerceItemRelationship> shippingGroupRelationships = null;
				List<TRUCommerceItemMetaInfo> metaInfoList = null;
				for (CommerceItem commerceItem : commerceItems) {
					metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
					TRUItemPriceInfo priceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
					Map<String, String> truItemDiscountShare = priceInfo.getTruItemDiscountShare();
					shippingGroupRelationships = commerceItem.getShippingGroupRelationships();
					if(shippingGroupRelationships != null && !shippingGroupRelationships.isEmpty()){
						for (TRUShippingGroupCommerceItemRelationship shiItemRel : shippingGroupRelationships) {
							metaInfoList.addAll(shiItemRel.getMetaInfo());
						}
					}
					if(truItemDiscountShare != null && !truItemDiscountShare.isEmpty() && metaInfoList != null && !metaInfoList.isEmpty()){
						Iterator<String> itemDiscountShareIterator = truItemDiscountShare.keySet().iterator();
						if(isLoggingDebug()){
							vlogDebug("TRU ItemDiscountShare Map ::: {0}", truItemDiscountShare);
						}
						while(itemDiscountShareIterator.hasNext()){
							String key = itemDiscountShareIterator.next();
							String value = truItemDiscountShare.get(key);
							String[] keySplit = key.split(PIPE_DELIMETRE);
							String promoId = keySplit[TRURadialOMConstants.INT_ZERO];
							String qualOrTar = keySplit[TRURadialOMConstants.INT_TWO];
							String couponId = keySplit[TRURadialOMConstants.ONE];
							if(isLoggingDebug()){
								vlogDebug("Population discount details with values :: Key :: {0} Value :: {1}", key, value);
							}
							double discShareSoFar = TRUConstants.DOUBLE_ZERO;
							for(TRUCommerceItemMetaInfo metaInfo : metaInfoList){
								String metaInfoPromoId = (String) metaInfo.getPropertyValue(getCommercePropertyManager().getPromotionIdPropertyName());
								boolean isAdjusted = (boolean) metaInfo.getPropertyValue(getCommercePropertyManager().getIsAdjustedPropertyName());
								long qty = (long) metaInfo.getPropertyValue(getCommercePropertyManager().getQuantityPropertyName());
								if(!StringUtils.isBlank(key)){
									if(qualOrTar.equals(PROMOTION_TARGETER_ITEM_LABEL) && !StringUtils.isBlank(metaInfoPromoId) && !StringUtils.isBlank(promoId) && promoId.equals(metaInfoPromoId) && isAdjusted){
										if(!StringUtils.isBlank(value)){
											String[] valueSplit = value.split(PIPE_DELIMETRE);
											if(isLoggingDebug()){
												logDebug("Entered into PROMOTION_TARGETER_ITEM_LABEL if block");
												vlogDebug(" pMetaInfo Id ::: {0}  promoId ::: {1}  meta info promo id ::: {2} isAdjusted : {3} Value :{4}", metaInfo.getId(), promoId, metaInfoPromoId, isAdjusted, value);
											}
											if(!StringUtils.isBlank(valueSplit[TRURadialOMConstants.INT_ZERO])){
												double discountShare = Double.parseDouble(valueSplit[TRURadialOMConstants.INT_ZERO]);
												double metaInfoDiscShare = TRUConstants.DOUBLE_ZERO;
												long discShareQty = Long.parseLong(valueSplit[TRURadialOMConstants.ONE]);
												if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
													metaInfoDiscShare = discountShare - discShareSoFar;
												}else if(discShareQty > TRURadialOMConstants.ONE && discShareQty > qty){
													metaInfoDiscShare = discountShare/discShareQty*qty;
												} else {
													metaInfoDiscShare = discountShare;
												}
												itemDiscShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+promoId+TRUConstants.PIPE_STRING+couponId, pricingTools.round(metaInfoDiscShare));
												discShareSoFar += pricingTools.round(metaInfoDiscShare);
												if(isLoggingDebug()){
													vlogDebug("Discount details added to the meta info with ID : {0}  with promotion details of ID : {1} and discount share is : {2}  Discount Share so Far : {3}", metaInfo.getId(), promoId, metaInfoDiscShare, discShareSoFar);
													logDebug("Exit from PROMOTION_TARGETER_ITEM_LABEL if block");
												}
											}
										}
									}else if(qualOrTar.equals(PROMOTION_QUALIFIER_ITEM_LABEL) && !StringUtils.isBlank(promoId) && StringUtils.isBlank(metaInfoPromoId) && !isAdjusted && !StringUtils.isBlank(value)){
										String[] valueSplit = value.split(PIPE_DELIMETRE);
										if(isLoggingDebug()){
											logDebug("Entered into PROMOTION_QUALIFIER_ITEM_LABEL if block");
											vlogDebug(" pMetaInfo Id ::: {0}  promoId ::: {1}  isAdjusted : {2} Value :{3}", metaInfo.getId(), promoId, isAdjusted, value);
										}
										double discountShare = TRUConstants.DOUBLE_ZERO;
										double metaInfoDiscShare = TRUConstants.DOUBLE_ZERO;
										if(!StringUtils.isBlank(valueSplit[TRURadialOMConstants.INT_ZERO])){
											discountShare = Double.parseDouble(valueSplit[TRURadialOMConstants.INT_ZERO]);
											long discShareQty = Long.parseLong(valueSplit[TRURadialOMConstants.ONE]);
											if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
												metaInfoDiscShare = discountShare - discShareSoFar;
											}else if(discShareQty > TRURadialOMConstants.ONE && discShareQty > qty){
												metaInfoDiscShare = discountShare/discShareQty*qty;
											} else {
												metaInfoDiscShare = discountShare;
											}
											itemDiscShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+promoId+TRUConstants.PIPE_STRING+couponId, pricingTools.round(metaInfoDiscShare));
											discShareSoFar += pricingTools.round(metaInfoDiscShare);
											if(isLoggingDebug()){
												vlogDebug("Discount details added to the meta info with ID : {0}  with promotion details of ID : {1} and discount share is : {2} Discount Share so Far : {3}", metaInfo.getId(), promoId, metaInfoDiscShare, discShareSoFar);
												logDebug("Exit from PROMOTION_QUALIFIER_ITEM_LABEL if block");
											}
										}
									}else if(qualOrTar.equals(PROMOTION_QUALIFIER_ITEM_LABEL) && !StringUtils.isBlank(promoId) && !StringUtils.isNotBlank(metaInfoPromoId) && !promoId.equals(metaInfoPromoId) && isAdjusted && !StringUtils.isBlank(value)){
										String[] valueSplit = value.split(PIPE_DELIMETRE);
										if(isLoggingDebug()){
											logDebug("Entered into PROMOTION_QUALIFIER_ITEM_LABEL if block 2");
											vlogDebug(" pMetaInfo Id ::: {0}  promoId ::: {1}  isAdjusted : {2} Value :{3}", metaInfo.getId(), promoId, isAdjusted, value);
										}
										double discountShare = TRUConstants.DOUBLE_ZERO;
										double metaInfoDiscShare = TRUConstants.DOUBLE_ZERO;
										if(!StringUtils.isBlank(valueSplit[TRURadialOMConstants.INT_ZERO])){
											discountShare = Double.parseDouble(valueSplit[TRURadialOMConstants.INT_ZERO]);
											long discShareQty = Long.parseLong(valueSplit[TRURadialOMConstants.ONE]);
											if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
												metaInfoDiscShare = discountShare - discShareSoFar;
											}else if(discShareQty > TRURadialOMConstants.ONE && discShareQty > qty){
												metaInfoDiscShare = discountShare/discShareQty*qty;
											} else {
												metaInfoDiscShare = discountShare;
											}
											itemDiscShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+promoId+TRUConstants.PIPE_STRING+couponId, pricingTools.round(metaInfoDiscShare));
											discShareSoFar += pricingTools.round(metaInfoDiscShare);
											if(isLoggingDebug()){
												vlogDebug("Discount details added to the meta info with ID : {0}  with promotion details of ID : {1} and discount share is : {2} Discount Share so Far : {3}", metaInfo.getId(), promoId, metaInfoDiscShare, discShareSoFar);
												logDebug("Exit from PROMOTION_QUALIFIER_ITEM_LABEL if block 2");
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: prepareItemDiscountShareMap()");
		}
		return itemDiscShareMap;
	}
	
	/**
	 * This method is to populate the discount details from Item discount share
	 * map.
	 *
	 * @param pObjectFactory            - ObjectFactory
	 * @param pMetaInfo            - TRUCommerceItemMetaInfo
	 * @param pOrder            - TRUOrderImpl
	 * @param pOrderItem            - OrderItem
	 * @param pCommItem            - CommerceItem
	 * @param pDiscounts            - PromotionalDiscounts
	 * @param pShipItemRel            - TRUShippingGroupCommerceItemRelationship
	 * @param pItemDiscountShareMap - Item discount share map
	 */
	public void getItemDiscountShare(ObjectFactory pObjectFactory, TRUCommerceItemMetaInfo pMetaInfo, TRUOrderImpl pOrder, OrderItem pOrderItem, CommerceItem pCommItem,
			PromotionalDiscounts pDiscounts, TRUShippingGroupCommerceItemRelationship pShipItemRel, Map<String, Double> pItemDiscountShareMap){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: getItemDiscountShare()");
		}
		Discount discount = null;
		PricingTools pricingTools = getCatalogTools().getPromotionTools().getPricingTools();
		TRUTaxPriceInfo taxPriceInfo = (TRUTaxPriceInfo) pOrder.getTaxPriceInfo();
		Map<String, String> taxPriceInfos = taxPriceInfo.getRadialTaxPriceInfos();
		if(pItemDiscountShareMap != null && !pItemDiscountShareMap.isEmpty()){
			Iterator<String> itemDiscountShareIterator = pItemDiscountShareMap.keySet().iterator();
			if(isLoggingDebug()){
				vlogDebug("pItemDiscountShareMap : {0}", pItemDiscountShareMap);
			}
			while(itemDiscountShareIterator.hasNext()){
				String key = itemDiscountShareIterator.next();
				double discShare = pItemDiscountShareMap.get(key);
				String[] keySplit = key.split(PIPE_DELIMETRE);
				String metaInfoId = keySplit[TRURadialOMConstants.INT_ZERO];
				String promoId = keySplit[TRURadialOMConstants.ONE];
				String couponId = keySplit[TRURadialOMConstants.INT_TWO];
				if(pMetaInfo.getId().equals(metaInfoId)){
					discount = pObjectFactory.createDiscount();
					populateDiscountDetails(pObjectFactory, promoId, pDiscounts, pricingTools.round(discShare), TRURadialOMConstants.PROMO_LEVEL_ITEM, discount, couponId);
					populateItemDisocuntTaxData(pObjectFactory,pCommItem, pMetaInfo, pShipItemRel,  discount, taxPriceInfos);
					if(isLoggingDebug()){
						vlogDebug("Discount details added to the meta info with ID : {0}  with promotion details of ID : {1} and discount share is : {2}", pMetaInfo.getId(), promoId, discShare);
					}
				}
			}
		}
		
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: getItemDiscountShare()");
		}
	}
	
	/**
	 * This method is to populate the get the promotion item and populate the
	 * promotion details to discount object.
	 * 
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pPromotionId
	 *            - Promotion ID
	 * @param pDiscounts
	 *            - PromotionalDiscounts
	 * @param pDiscShare
	 *            - Discount Share amount
	 * @param pPromoLevel - Promotion Level
	 * @param pDiscount - Discount XML Object
	 * @param pCouponId - Coupon Id
	 */
	public void populateDiscountDetails(ObjectFactory pObjectFactory, String pPromotionId, PromotionalDiscounts pDiscounts, double pDiscShare,  
			String pPromoLevel, Discount pDiscount, String pCouponId){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateDiscountDetails()");
			vlogDebug("Populating discount details of promo level : {0} Promotion Id : {1} Discount Share : {2} Coupon Id : {3}", pPromoLevel, pPromotionId, pDiscShare, pCouponId);
		}
		if(pPromotionId == null || pDiscounts == null || pObjectFactory== null || pDiscount == null){
			return;
		}
		TRUPromotionTools promotionTools = getCatalogTools().getPromotionTools();
		RepositoryItem promoItem = promotionTools.getPromotionFromPromoId(pPromotionId);
		TRUPricingModelProperties pricingModelProperties = (TRUPricingModelProperties) promotionTools.getPricingModelProperties();
		if(promoItem != null){
			if(pDiscShare < 0){
				pDiscount.setAmount(BigDecimal.valueOf(-pDiscShare));
			}else{
				pDiscount.setAmount(BigDecimal.valueOf(pDiscShare));
			}
			pDiscount.setExternalPromoId(getEncodedString(promoItem.getRepositoryId()));
			pDiscount.setDescription(getEncodedString((String) promoItem.getPropertyValue(pricingModelProperties.getDisplayName())));
			pDiscount.setLongDescription(getEncodedString((String) promoItem.getPropertyValue(pricingModelProperties.getDescription())));
			pDiscount.setShortDescription(getEncodedString((String) promoItem.getPropertyValue(pricingModelProperties.getDisplayName())));
			boolean vendorFunded = (boolean) promoItem.getPropertyValue(pricingModelProperties.getVendorFundedPropertyName());
			if(vendorFunded){
				pDiscount.setPromoType(String.valueOf(TRUConstants.ZERO));
			}else{
				pDiscount.setPromoType(String.valueOf(TRUConstants.ONE));
			}
			if(StringUtils.isNotBlank(pCouponId) && !TRUConstants.NULL_STRING.equalsIgnoreCase(pCouponId)){
				pDiscount.setCode(pCouponId);
			}
			if(promoItem.getPropertyValue(pricingModelProperties.getCampaignIdPropertyName()) != null){
				Integer campaignId = (Integer) promoItem.getPropertyValue(pricingModelProperties.getCampaignIdPropertyName());
				pDiscount.setDealNo(BigInteger.valueOf(campaignId));
			} 
			pDiscount.setPromoLevel(getEncodedString(pPromoLevel));
			pDiscounts.getDiscount().add(pDiscount);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateDiscountDetails()");
		}
	}
	
	
	/**
	 * This method is to prepare order discount share map for each metainfo form
	 * Prepare order discount share map.
	 *
	 * @param pOrder the order
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareOrderDiscountShareMap(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderDiscountShare()");
		}
		Map<String, Double> truOrderDiscountShare = null;
		TRUItemPriceInfo itemPriceInfo = null;
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double commerceItemTotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		Map<String, Double> orderDiscountShareMap = new HashMap<String, Double>();
		TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		TRUPricingTools pricingTools = (TRUPricingTools) getCatalogTools().getPricingTools();
		if(pOrder != null){
		List<CommerceItem> commerceItems = pOrder.getCommerceItems();
		for (CommerceItem commerceItem : commerceItems) {
			metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
			itemPriceInfo = (TRUItemPriceInfo) commerceItem.getPriceInfo();
			commerceItemTotal = itemPriceInfo.getRawTotalPrice();
			truOrderDiscountShare = itemPriceInfo.getTruOrderDiscountShare();
			List<TRUShippingGroupCommerceItemRelationship> shipItemRels = commerceItem.getShippingGroupRelationships();
			for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
				List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
				metaInfoList.addAll(metaInfos);
			}
			if(metaInfoList != null && !metaInfoList.isEmpty() && truOrderDiscountShare != null && !truOrderDiscountShare.isEmpty()){
				Iterator<String> truOrderDiscountShareIterator = truOrderDiscountShare.keySet().iterator();
				while(truOrderDiscountShareIterator.hasNext()){
					String key = truOrderDiscountShareIterator.next();
					double value = truOrderDiscountShare.get(key);
					double totalDiscountShare = TRUConstants.DOUBLE_ZERO;
					double discShare = TRUConstants.DOUBLE_ZERO;
					for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
						unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
						metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
						if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
							discShare = value - totalDiscountShare;
						}else{
							discShare = value*unitPrice*metaInfoQty/commerceItemTotal;
						}
						orderDiscountShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+key, pricingTools.round(discShare));
						totalDiscountShare += pricingTools.round(discShare);
						if(isLoggingDebug()){
							vlogDebug("Meta info Id : {0}  Promotion Id key : {1} discount share for this meta info : {2}   Total discount share till now : {3}", metaInfo.getId(), key, pricingTools.round(discShare), totalDiscountShare);
						}
					}
				}
			}
		}
		}
		if(isLoggingDebug()){
			vlogDebug("orderDiscountShareMap : {0}", orderDiscountShareMap);
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderDiscountShare()");
		}
		return orderDiscountShareMap;
	}
	
	/**
	 * This method is to prepare the map of shipping charge share for each item.
	 *
	 * @param pOrder
	 *            - Order
	 * @return shippingChargeShareMap - shipping charge share map
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareShippingChargeMap(TRUOrderImpl pOrder){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: prepareShippingChargeMap()");
		}
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		Map<String, Double> shippingChargeShareMap = new HashMap<String, Double>();
		TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
		TRUPricingTools pricingTools = (TRUPricingTools) getCatalogTools().getPricingTools();
		List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
		for (ShippingGroup shippingGroup : shippingGroups) {
			shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
			
			if(shippingGroup instanceof TRUHardgoodShippingGroup || shippingGroup instanceof TRUChannelHardgoodShippingGroup){
				metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
				TRUHardgoodShippingGroup hardgoodSG = (TRUHardgoodShippingGroup) shippingGroup;
				if(!hardgoodSG.isMarkerShippingGroup()) {
					continue;
				}
				ShippingPriceInfo priceInfo = hardgoodSG.getPriceInfo();
				double amount = priceInfo.getAmount();
				if (isLoggingDebug()) {
					vlogDebug("shipping amount from shipping group : {0}", amount);
				}
				List<TRUShippingGroupCommerceItemRelationship> shipItemRels =new ArrayList<TRUShippingGroupCommerceItemRelationship>();
				List<String> dependencyShippingGruoupIds = hardgoodSG.getDependencyShippingGruoupIds();
				for (String shipID : dependencyShippingGruoupIds) {
					ShippingGroup shippingGroup2;
						try {
							shippingGroup2 = pOrder.getShippingGroup(shipID);
							shipItemRels.addAll(shippingGroup2.getCommerceItemRelationships());
						} catch (ShippingGroupNotFoundException se) {
							vlogError("ShippingGroupNotFoundException in TRURadialTaxwareProcessor.prepareShippingChargeMap() {0}", se);
						} catch (InvalidParameterException ie) {
							vlogError("InvalidParameterException in TRURadialTaxwareProcessor.prepareShippingChargeMap() {0}", ie);
						}
				}
				for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
					if(!(shipItemRel.getCommerceItem() instanceof TRUDonationCommerceItem|| shipItemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem)){
						List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
						if(metaInfos != null && !metaInfos.isEmpty()){
							metaInfoList.addAll(metaInfos);
						}
						shipGrpCITotal += shipItemRel.getQuantity()*shipItemRel.getCommerceItem().getPriceInfo().getSalePrice();
					}
				}
				if (isLoggingDebug()) {
					vlogDebug("shipGrpCITotal : {0}",shipGrpCITotal);
				}
				if(metaInfoList != null && !metaInfoList.isEmpty()){
					double totalShippingCharge = TRUConstants.DOUBLE_ZERO;
					double shippingChargeShare = TRUConstants.DOUBLE_ZERO;
					for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
						unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
						metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
						if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
							shippingChargeShare = amount - totalShippingCharge;
						}else{
							shippingChargeShare = amount*unitPrice*metaInfoQty/shipGrpCITotal;
						}
						shippingChargeShareMap.put(metaInfo.getId(), pricingTools.round(shippingChargeShare));
						totalShippingCharge += pricingTools.round(shippingChargeShare);
						if (isLoggingDebug()) {
							vlogDebug("shipping Charge Share for metainfo with id : {0} is : {1}",metaInfo.getId(),  pricingTools.round(shippingChargeShare));
							vlogDebug("Total shipping Charge  : {0}", totalShippingCharge);
						}
					}
				}
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Shipping Charge share map : {0}", shippingChargeShareMap);
			logDebug("Exit from class: TRURadialOMUtils method: prepareShippingChargeMap()");
		}
		return shippingChargeShareMap;
	}

	
	/**
	 * This method is to prepare the map of shipping discount share for each
	 * item.
	 *
	 * @param pOrderImpl
	 *            - Order
	 * @return shippingDiscountShareMap - Shipping discount share map for each
	 *         order item
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> prepareShippingDiscountShareMap(TRUOrderImpl pOrderImpl){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: prepareShippingDiscountShareMap()");
		}
		List<TRUCommerceItemMetaInfo> metaInfoList = null;
		double shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
		double unitPrice = TRUConstants.DOUBLE_ZERO;
		long metaInfoQty = TRUConstants.LONG_ZERO;
		RepositoryItem couponItem = null;
		String couponCode = null;
		Map<String, Double> shippingDiscountShareMap = new HashMap<String, Double>();
		TRUPricingTools pricingTools = (TRUPricingTools) getCatalogTools().getPricingTools();
		if(pOrderImpl != null){
			TRUCommercePropertyManager commercePropertyManager = getCommercePropertyManager();
			List<ShippingGroup> shippingGroups = pOrderImpl.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				shipGrpCITotal = TRUConstants.DOUBLE_ZERO;
				if(shippingGroup instanceof TRUHardgoodShippingGroup){
					TRUHardgoodShippingGroup hardgoodSG = (TRUHardgoodShippingGroup) shippingGroup;
					if(!hardgoodSG.isMarkerShippingGroup()) {
						continue;
					}
					metaInfoList = new ArrayList<TRUCommerceItemMetaInfo>();
					ShippingPriceInfo priceInfo = hardgoodSG.getPriceInfo();
					List<PricingAdjustment> adjustments = priceInfo.getAdjustments();
						if(adjustments != null && !adjustments.isEmpty()){
						List<String> dependencyShippingGruoupIds = hardgoodSG.getDependencyShippingGruoupIds();
						for (String shipGroupID : dependencyShippingGruoupIds) {
							try {
								ShippingGroup shippingGroup2 = pOrderImpl.getShippingGroup(shipGroupID);
								List<TRUShippingGroupCommerceItemRelationship> shipItemRels = shippingGroup2.getCommerceItemRelationships();
								for (TRUShippingGroupCommerceItemRelationship shipItemRel : shipItemRels) {
									if(!(shipItemRel.getCommerceItem() instanceof TRUDonationCommerceItem|| shipItemRel.getCommerceItem() instanceof TRUGiftWrapCommerceItem)){
										List<TRUCommerceItemMetaInfo> metaInfos = shipItemRel.getMetaInfo();
										metaInfoList.addAll(metaInfos);
										shipGrpCITotal += shipItemRel.getQuantity()*shipItemRel.getCommerceItem().getPriceInfo().getSalePrice();
									}
								}
							} catch (ShippingGroupNotFoundException se) {
								vlogError("ShippingGroupNotFoundException in TRURadialTaxwareProcessor.prepareShippingDiscountShareMap() {0}", se);
							} catch (InvalidParameterException ie) {
								vlogError("InvalidParameterException in TRURadialTaxwareProcessor.prepareShippingDiscountShareMap() {0}", ie);
							}
						}
					}
					if (isLoggingDebug()) {
						vlogDebug("shipping group total amount shipGrpCITotal : {0}", shipGrpCITotal);
					}
					if (isLoggingDebug()) {
						vlogDebug("shipping group total amount shipGrpCITotal : {0}", shipGrpCITotal);
					}
					if(adjustments != null && !adjustments.isEmpty()){
						for (PricingAdjustment pricingAdjustment : adjustments) {
							if(pricingAdjustment.getPricingModel() != null){
								double discountAmount = -pricingAdjustment.getTotalAdjustment();
								couponItem = pricingAdjustment.getCoupon();
								if(couponItem != null){
									couponCode = couponItem.getRepositoryId();
								}
								if(!StringUtils.isBlank(couponCode)){
									Map<String, String>  singleUseCouponMap = pOrderImpl.getSingleUseCoupon();
									if(singleUseCouponMap != null && !singleUseCouponMap.isEmpty() && !StringUtils.isNotBlank(singleUseCouponMap.get(couponCode))){
										couponCode = singleUseCouponMap.get(couponCode);
									}
								}
								String promoId = pricingAdjustment.getPricingModel().getRepositoryId();
								if(metaInfoList != null && !metaInfoList.isEmpty()){
									double totalShippingDiscShare = TRUConstants.DOUBLE_ZERO;
									double shippingDiscShare = TRUConstants.DOUBLE_ZERO;
									for (TRUCommerceItemMetaInfo metaInfo : metaInfoList) {
										unitPrice = (double) metaInfo.getPropertyValue(commercePropertyManager.getUnitPricePropertyName());
										metaInfoQty = (long) metaInfo.getPropertyValue(commercePropertyManager.getQuantityPropertyName());
										if (isLoggingDebug()) {
											vlogDebug("discountAmount : {0} unitPrice{1} metaInfoQty{2}", discountAmount,unitPrice,metaInfoQty);
										}
										if(metaInfoList.indexOf(metaInfo)+TRUConstants.INTEGER_NUMBER_ONE == metaInfoList.size()){
											shippingDiscShare = discountAmount - totalShippingDiscShare;
										}else{
											shippingDiscShare = discountAmount*unitPrice*metaInfoQty/shipGrpCITotal;
										}
										shippingDiscountShareMap.put(metaInfo.getId()+TRUConstants.PIPE_STRING+promoId+TRUConstants.PIPE_STRING+couponCode, pricingTools.round(shippingDiscShare));
										totalShippingDiscShare += pricingTools.round(shippingDiscShare);
										if (isLoggingDebug()) {
											vlogDebug("shippingDiscShare : {0} totalShippingDiscShare{1}", shippingDiscShare,totalShippingDiscShare);
										}
									}
								 }
							 }
						 }
					 }
				 }
			 }
		}
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: prepareShippingDiscountShareMap()");
		}
		return shippingDiscountShareMap;
	}
	
	
	/**
	 * This method is to populate the order discount share for each order item.
	 *
	 * @param pObjectFactory
	 *            - ObjectFactory
	 * @param pOrderDiscountShareMap
	 *            - Map of order discount share
	 * @param pDiscounts
	 *            - Discounts XML Object
	 * @param pItemMetaInfo
	 *            - Meta info item
	 * @param pCommItem
	 *            - Commerce Item
	 * @param pTaxPriceInfos
	 *            - TaxPriceInfos Map
	 * @param pShipItemRel
	 *            - TRUShippingGroupCommerceItemRelationship
	 * @param pPromoLevelOrder
	 *            - Promotion level Order
	 */
	private void populateOrderDiscountsForItem(ObjectFactory pObjectFactory,
			Map<String, Double> pOrderDiscountShareMap, PromotionalDiscounts pDiscounts, TRUCommerceItemMetaInfo pItemMetaInfo, CommerceItem pCommItem, Map<String, String> pTaxPriceInfos, TRUShippingGroupCommerceItemRelationship pShipItemRel, String pPromoLevelOrder) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateOrderDiscountsForItem()");
			vlogDebug("Meta Info Id : {0} :: OrderDiscountShareMap : {1} Promo level : {2}", pItemMetaInfo.getId(), pOrderDiscountShareMap, pPromoLevelOrder);
		}
		if(pDiscounts == null || pOrderDiscountShareMap == null){
			return;
		}
		Discount discount = null;
		if(pOrderDiscountShareMap != null && !pOrderDiscountShareMap.isEmpty()){
			Iterator<String> truOrderDiscountShareIterator = pOrderDiscountShareMap.keySet().iterator();
			while(truOrderDiscountShareIterator.hasNext()){
				String key = truOrderDiscountShareIterator.next();
				double discShare = pOrderDiscountShareMap.get(key);
				String[] keySplit = key.split(PIPE_DELIMETRE);
				String metaInfoId = keySplit[TRURadialOMConstants.INT_ZERO];
				String promoId = keySplit[TRURadialOMConstants.ONE];
				String couponId = keySplit[TRURadialOMConstants.INT_TWO];
				if(metaInfoId.equals(pItemMetaInfo.getId())){
					discount = pObjectFactory.createDiscount();
					populateDiscountDetails(pObjectFactory, promoId, pDiscounts, discShare, pPromoLevelOrder, discount, couponId);
					populateItemDisocuntTaxData(pObjectFactory, pCommItem, pItemMetaInfo, pShipItemRel, discount, pTaxPriceInfos);
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: populateOrderDiscountsForItem()");
		}
	}
	
	/** Holds mShippingManager. */
	private TRUShippingManager mShippingManager;

	/**
	 * This is to get the value for shippingManager.
	 *
	 * @return the shippingManager value
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * This method to set the pShippingManager with shippingManager.
	 *
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}
}
