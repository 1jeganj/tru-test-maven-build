package com.tru.jda.integration.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import atg.commerce.order.InStorePayment;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.endeca.serialization.json.JSONObject;
import com.endeca.serialization.json.XML;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.jda.createOrder.Description;
import com.tru.commerce.TRUCommerceConstants;
import com.tru.commerce.TRUCommercePropertyManager;
import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.order.TRUGiftCard;
import com.tru.commerce.order.TRUOrderManager;
import com.tru.commerce.order.TRUShippingManager;
import com.tru.common.TRUConstants;
import com.tru.common.cml.Constants;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.radialom.TRURadialOMTools;
import com.tru.logging.TRUAlertLogger;
import com.tru.logging.TRUIntegrationInfoLogger;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This is the Utils class.
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUBaseRadialOMUtils extends GenericService {
	
	/**
	 * Holds reference for mOrderManager.
	 */
	private TRUOrderManager mOrderManager;
	/**
	 * Holds reference for mPropertyManager.
	 */
	private TRUPropertyManager mPropertyManager;
	
	/** The Catalog tools. */
	private TRUCatalogTools mCatalogTools;
	
	/**
	 * This property hold reference for mProductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;
	
	/** The Site context manager. */
	private SiteContextManager mSiteContextManager;
	
	/** Property to hold mCatalogRepository. */
	private Repository mCatalogRepository;

	/** This holds OM configuration. */
	private TRURadialOMConfiguration mOmConfiguration;
	
	/** Property to hold mShippingManager. */
	private TRUShippingManager mShippingManager;

	/**
	 * This holds line number.
	 */
	private int mLineNumber;
	
	/**
	 * Holds truRadialOMTools
	 */
	private TRURadialOMTools mTruRadialOMTools;
	
	/** The m xml schema ns uri. */
	private String mXmlSchemaNsURI;
	
	/** The mAlertLogger */
	private TRUAlertLogger mAlertLogger;
	
	/** The mIntegrationInfoLogger. */
	private TRUIntegrationInfoLogger mIntegrationInfoLogger;
	
	/**
	 * @return the mIntegrationInfoLogger
	 */
	public TRUIntegrationInfoLogger getIntegrationInfoLogger() {
		return mIntegrationInfoLogger;
	}

	/**
	 * @param pIntegrationInfoLogger the mIntegrationInfoLogger to set
	 */
	public void setIntegrationInfoLogger(TRUIntegrationInfoLogger pIntegrationInfoLogger) {
		mIntegrationInfoLogger = pIntegrationInfoLogger;
	}
	
	/**
	 * Gets the xml schema ns uri.
	 *
	 * @return the xml schema ns uri
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}
	
	/**
	 * Sets the xml schema ns uri.
	 *
	 * @param pXmlSchemaNsURI the new xml schema ns uri
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		  mXmlSchemaNsURI = pXmlSchemaNsURI;
	}
	
	/** The m schema xsd uri. */
	private File  mSchemaXsdUri;
	
	/**
	 * Gets the schema xsd uri.
	 *
	 * @return the schema xsd uri
	 */
	public File getSchemaXsdUri() {
		return mSchemaXsdUri;
	}

	/**
	 * Sets the schema xsd uri.
	 *
	 * @param pSchemaXsdUri the new schema xsd uri
	 */
	public void setSchemaXsdUri(File pSchemaXsdUri) {
		  mSchemaXsdUri = pSchemaXsdUri;
	}

	/**
	 * This method is to encode string.
	 * @param pString - String
	 * @return encodedString - Encoded String
	 */
	public String getEncodedString(String pString){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: getEncodedString()");
		}
		String encodedString = null;
		if(pString != null){
			try {
				encodedString = URLEncoder.encode(pString,TRURadialOMConstants.UTF_8);
			} catch (UnsupportedEncodingException exc) {
				if(isLoggingError()){
					vlogError("UnsupportedEncodingException : While encoding string : {0} and exception is : {1}",pString,  exc);
				}
			}
			if(isLoggingDebug()){
				logDebug("Exit from class: TRURadialOMUtils method: getEncodedString()");
			}
		}
		return encodedString;
	}
	/**
	 * This method is to call the order services with target URLs.
	 * 
	 * @param pRequestXML
	 *            - Request XML
	 * @param pPackage
	 *            - Package
	 * @param pTargetURL
	 *            - Target service URL
	 * @return object - response object
	 */
	public Object callOrderService(String pRequestXML, String pPackage, String pTargetURL){		
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: callOrderService()");
		}
		Object responseObj = null;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		HttpURLConnection connection = null;
		InputStream inputStream = null;
		try {
			connection = (HttpURLConnection) new URL(pTargetURL).openConnection();
			connection.setRequestMethod(configuration.getHttpPostMethod());
			connection.setRequestProperty(configuration.getApiKey(), configuration.getApiKeyValue());
			connection.setRequestProperty(configuration.getHttpContentType(), configuration.getHttpApplicationType());
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			OutputStream outputStream = connection.getOutputStream();
			outputStream.write(pRequestXML.getBytes());
			outputStream.flush();
			connection.connect();
			inputStream = connection.getInputStream();
			//Unmarshall the Response
			responseObj = unMarshalResponseObject(inputStream, pPackage);
		} catch (IOException exc) {
			if(isLoggingError()){
				vlogError("IOException while getting input stream from connection and exception is : {0}", exc);
			}
		}
		finally{
			if(inputStream != null){
			try {
				inputStream.close();
				} 
			catch (IOException e) {
				if(isLoggingError()){
					vlogError("IOException while closing input stream and exception is : {0}", e);
				}
				}
			}
			if (connection != null) {
				connection.disconnect();
		    }
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: callOrderService()");
		}
		return responseObj;
	}
	
	/**
	 * This method is to convert json object to map
	 *
	 * @param pObject - json object
	 * @return map - map
	 */
	private static Map<String, Object> toMap(JsonObject pObject) {
		Map<String, Object> map = new HashMap<String, Object>();
		for (Entry<String, JsonElement> pair : pObject.entrySet()) {
            map.put(pair.getKey(), toValue(pair.getValue()));
        }
		return map;
	}
	
	/**
	 * This method is to convert to value
	 *
	 * @param pValue - value
	 * @return object - Object
	 */
	private static Object toValue(JsonElement pValue) {
        if (pValue.isJsonNull()) {
            return null;
        } else if (pValue.isJsonArray()) {
            return toList((JsonArray) pValue);
        } else if (pValue.isJsonObject()) {
            return toMap((JsonObject) pValue);
        } else if (pValue.isJsonPrimitive()) {
            return toPrimitive((JsonPrimitive) pValue);
        }
        return null;
	}
	
	/**
	 * This method is to convert json array to list
	 *
	 * @param pArray - json array
	 * @return list - list
	 */
	private static List<Object> toList(JsonArray pArray) {
        List<Object> list = new ArrayList<Object>();
        for (JsonElement element : pArray) {
            list.add(toValue(element));
        }
        return list;
	}
	
    /**
     * This method is to convert primitive
     *
     * @param pValue - value
     * @return string - string
     */
    private static Object toPrimitive(JsonPrimitive pValue) {
        if (pValue.isBoolean()) {
            return pValue.getAsBoolean();
        } else if (pValue.isString()) {
            return pValue.getAsString();
        } else if (pValue.isNumber()){
            return pValue.getAsNumber();
        }
        return null;
    }
	 
    /**
	 * This method will validate the xml with xsd of the respective files.
	 * 
	 * @param pFile
	 *            - File
	 * @return boolean
	 */
	public boolean validateFile(String pFile) {
		boolean isValidFeed = true;

			SchemaFactory schemaFactory = null;
			Source xmlFile = new StreamSource(pFile);
			String lXmlSchemaNsURI = getXmlSchemaNsURI();
			try {
				if (!StringUtils.isEmpty(lXmlSchemaNsURI)) {
					schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
				}
				Schema xsdschema = schemaFactory.newSchema(getSchemaXsdUri().getAbsoluteFile());
				Validator validator = xsdschema.newValidator();
				validator.validate(xmlFile);
			} catch (SAXException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + "file invalid" , ex);
				}
			} catch (IOException ex) {
				isValidFeed = false;
				if(isLoggingError()) {
					logError(xmlFile.getSystemId() + "filenotfound" , ex);
				}
			}
		return isValidFeed;
	}
    
	/**
	 * This method is to parse the response xml.
	 * 
	 * @param pResponseXML
	 *            - Response XML
	 * @return jsonResponseString - String
	 */
	public JsonObject parseOrderDetailLookupResponse(String pResponseXML){
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: parseOrderDetailResponse()");
			vlogDebug("Response XML : {0}", pResponseXML);
		}
		JsonArray displayOrderArray = new JsonArray();
		JsonObject displayObject = null;
		String jsonResponseString = null;
		jsonResponseString = convertXMLtoJSON(pResponseXML);
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(jsonResponseString);
		JsonObject asJsonObject = parse.getAsJsonObject();
		JsonObject shippingInfoAddress = null;
		JsonObject orderLookupResponseListJson = asJsonObject.get(getOmConfiguration().getOrderLookupResponseListProperty()).getAsJsonObject();
		JsonObject orderLookupResponseJson = orderLookupResponseListJson.get(getOmConfiguration().getOrderLookupResponseProperty()).getAsJsonObject();
		JsonObject lineDetailsJsonObj = orderLookupResponseJson.get(getOmConfiguration().getLineDetailsProperty()).getAsJsonObject();
		JsonObject orderHeaderJsonObj = orderLookupResponseJson.get(getOmConfiguration().getOrderHeaderProperty()).getAsJsonObject();
		orderHeaderJsonObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.FALSE);
		formatOrderDate(orderHeaderJsonObj);
		JsonObject billingAddress=orderHeaderJsonObj.get(getOmConfiguration().getBillingAddressPropertyName()).getAsJsonObject();
		String orderStatus=orderHeaderJsonObj.get(getOmConfiguration().getOrderStatusProperty()).getAsString();
		Map<String, String> orderDetailStatusesMap = getOmConfiguration().getOrderDetailStatusesMap();
		String statusMapValue = orderDetailStatusesMap.get(orderStatus);
		orderHeaderJsonObj.remove(getOmConfiguration().getOrderStatusProperty());
		orderHeaderJsonObj.addProperty(getOmConfiguration().getOrderStatusProperty(), statusMapValue);
		String postalCode=billingAddress.get(getOmConfiguration().getPostalCodePropertyName()).getAsString();
		if(postalCode.length()<TRURadialOMConstants.INT_FIVE){
			StringBuffer postal=new StringBuffer();
			billingAddress.remove(getOmConfiguration().getPostalCodePropertyName());
			billingAddress.addProperty(getOmConfiguration().getPostalCodePropertyName(), postal.append(TRURadialOMConstants.INT_ZERO).append(postalCode).toString());
			postal.setLength(TRURadialOMConstants.INT_ZERO);
		}
		JsonObject lineDetailJsonObject = null;
		JsonArray lineDetailArray = null;
        if (lineDetailsJsonObj.get(getOmConfiguration().getLineDetailProperty()) instanceof JsonObject) {
        	lineDetailJsonObject = lineDetailsJsonObj.getAsJsonObject(getOmConfiguration().getLineDetailProperty());
        	lineDetailArray = new JsonArray();
        	lineDetailArray.add(lineDetailJsonObject);
        	lineDetailsJsonObj.remove(getOmConfiguration().getLineDetailProperty());
        	lineDetailsJsonObj.add(getOmConfiguration().getLineDetailProperty(), lineDetailArray);
		} else {
			lineDetailArray = (JsonArray) lineDetailsJsonObj.get(getOmConfiguration().getLineDetailProperty());
		}
		JsonArray shippmentInfoArray = null;
		for (int j = 0; j < lineDetailArray.size(); j++) {
			JsonObject orderLineObj = lineDetailArray.get(j).getAsJsonObject();
			if(orderLineObj.get(getOmConfiguration().getShippingAddressProperty())!=null){
			     shippingInfoAddress = orderLineObj.get(getOmConfiguration().getShippingAddressProperty()).getAsJsonObject();
			}else if(orderLineObj.get(getOmConfiguration().getStoreFrontAddressProperty())!=null){
				 shippingInfoAddress = orderLineObj.get(getOmConfiguration().getStoreFrontAddressProperty()).getAsJsonObject();
				 shippingInfoAddress.add(getOmConfiguration().getStoreCodePropertyNameResponse(), shippingInfoAddress.get(getOmConfiguration().getStoreCodePropertyName()));
				 shippingInfoAddress.add(getOmConfiguration().getStorePhonePropertyNameResponse(), shippingInfoAddress.get(getOmConfiguration().getStorePhonePropertyName()));
				 shippingInfoAddress.add(getOmConfiguration().getStoreNamePropertyNameResponse(), shippingInfoAddress.get(getOmConfiguration().getStoreNamePropertyName()));
				 shippingInfoAddress.remove(getOmConfiguration().getStoreCodePropertyName());
				 shippingInfoAddress.remove(getOmConfiguration().getStorePhonePropertyName());
				 shippingInfoAddress.remove(getOmConfiguration().getStoreNamePropertyName());
				 
			}
			JsonArray orderLineArrayUpdated = null;
			JsonObject  shippingInfo= null;
			JsonArray dispOrderArray = new JsonArray();
			if (displayOrderArray != null && displayOrderArray.size() != 0 && !displayOrderArray.isJsonNull()) {
				int displayOrderArraySize = displayOrderArray.size();
				populateDisplayOrderArray(displayOrderArray, orderLineObj, shippingInfoAddress,
						dispOrderArray, displayOrderArraySize,orderHeaderJsonObj);
				}
			else {
				orderLineArrayUpdated = new JsonArray();
				displayObject = new JsonObject();
				shippmentInfoArray= new JsonArray();
				shippingInfo = new JsonObject();
				shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(),orderLineObj.get(getOmConfiguration().getDeliveryModeProperty()));
				shippingInfo.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.FALSE);
				String deliveryMode = shippingInfo.get(getOmConfiguration().getDeliveryModeProperty()).getAsString();
				if(deliveryMode.equalsIgnoreCase(TRURadialOMConstants.DONATION)){
					shippingInfo.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
					orderHeaderJsonObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
				}
				displayObject.add(getOmConfiguration().getShippingAddressProperty(),shippingInfoAddress);
				displayObject.add(getOmConfiguration().getDeliveryModeProperty(),orderLineObj.get(getOmConfiguration().getDeliveryModeProperty()));
				orderLineObj.remove(getOmConfiguration().getShippingAddressProperty());
				orderLineObj.remove(getOmConfiguration().getStoreFrontAddressProperty());
				displayObject.addProperty(getOmConfiguration().getIsProxyExists(), Boolean.FALSE); 
				if(orderLineObj.get(getOmConfiguration().getProxyAddressProperty())!=null){
					displayObject.add(getOmConfiguration().getProxyAddressProperty(),orderLineObj.get(getOmConfiguration().getProxyAddressProperty()));
					displayObject.addProperty(getOmConfiguration().getIsProxyExists(), Boolean.TRUE); 
					orderLineObj.remove(getOmConfiguration().getProxyAddressProperty());
				}
				orderLineArrayUpdated.add(orderLineObj);
				shippingInfo.add(getOmConfiguration().getLineDetailsProperty(), orderLineArrayUpdated);
				shippmentInfoArray.add(shippingInfo);
				displayObject.add(getOmConfiguration().getShippmentInfoProperty(), shippmentInfoArray);
				dispOrderArray.add(displayObject);
				displayOrderArray.add(displayObject);
			}
		}
		getlineitemcount(orderLookupResponseJson,displayOrderArray);
		orderLookupResponseJson.add(getOmConfiguration().getDisplayOrderArrayProperty(), displayOrderArray);
		orderLookupResponseJson.remove(getOmConfiguration().getLineDetailsProperty());
		getOrdersummary(orderLookupResponseJson);
		orderLookupResponseJson.addProperty(getOmConfiguration().getOrderDetailsHelpAndReturnsJsonElement(), 
				getOmConfiguration().getOrderDetailsHelpAndReturnsURLJsonElement());
		getOrderPaymentListAsArray(orderLookupResponseJson);
		rearrangeDonationItem(orderLookupResponseJson,displayOrderArray);
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUBaseRadialOMUtils method: parseOrderDetailLookupResponse()");
			logDebug("formatted json Response: "+jsonResponseString);
		}
		return orderLookupResponseJson;
	}
	
	 /**
		 * This method is to format the order date
		 *
		 * @param pOrderHeaderJson - Json object
		 */
		private void formatOrderDate(JsonObject pOrderHeaderJson) {
			if(isLoggingDebug()){
				logDebug("Enter into class: TRUBaseRadialOMUtils method: formatOrderDate()");
			}
			if(pOrderHeaderJson == null){
				return;
			}
			String orderDate =null;
			TRURadialOMConfiguration configuration = getOmConfiguration();
			if(isLoggingDebug()){
				logDebug("Formatted Date order json:"+pOrderHeaderJson);
			}
			if(configuration.getOrderDatePropertyName()!=null && pOrderHeaderJson.get(configuration.getOrderDatePropertyName()) !=null){
				 orderDate = pOrderHeaderJson.get(configuration.getOrderDatePropertyName()).getAsString();
				 DateFormat df = new SimpleDateFormat(configuration.getOrderDateFormatPropertyName());
				 df.setTimeZone(TimeZone.getTimeZone(getOmConfiguration().getTimeZoneEST()));
				try {
					Date parseDate = df.parse(orderDate);
					SimpleDateFormat updateFormatter = new SimpleDateFormat(TRUConstants.ORDER_HISTORY_DATE_DISPLAY,Locale.US);
					String format = updateFormatter.format(parseDate);
					pOrderHeaderJson.remove(configuration.getOrderDatePropertyName());
					pOrderHeaderJson.addProperty(configuration.getOrderDatePropertyName(), format);
				} catch (ParseException e) {
					if(isLoggingError()){
						vlogError("ParseException: While parsing the date : {0} and exception details are : {1}", orderDate, e);
					}
				}
			}
				if(isLoggingDebug()){
					logDebug("Exit from class: TRUBaseRadialOMUtils method: formatOrderDate()");
				}
		}
	
	 /**
	 * This method is to convert json array to list
	 *
	 * @param pJsonArray - Json array
	 * @return list - list
	 */
	public static List<Object> jsonToList(JsonArray pJsonArray) {
        if (pJsonArray.isJsonNull()) {
            return new ArrayList<Object>();
        }
        return toList(pJsonArray);
	}
	
	 /**
	 * This method is to convert json object map
	 *
	 * @param pJsonObject - Json object
	 * @return map - map
	 */
	public  Map<String, Object> jsonToMap(JsonObject pJsonObject) {
        if (pJsonObject.isJsonNull()) {
            return new HashMap<String, Object>();
        }
        return toMap(pJsonObject);
	}
	
	/**
	 * This method is to rearrange donation item.
	 *
	 * @param pOrderLookupResponseJson - Response Json
	 * @param pDisplayOrderArray - Display order array
	 */
	private void rearrangeDonationItem(JsonObject pOrderLookupResponseJson, JsonArray pDisplayOrderArray) {
		JsonArray rearrangedArray=new JsonArray();
		boolean donationExists=Boolean.FALSE;
		JsonObject donationObject=null;
		for(int i=TRURadialOMConstants.INT_ZERO;i<pDisplayOrderArray.size();i++){
			JsonObject displayObject = pDisplayOrderArray.get(i).getAsJsonObject();
	    	   if(displayObject.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equalsIgnoreCase(TRURadialOMConstants.DONATION)){
	    		   donationObject=displayObject;
	    		   donationExists=Boolean.TRUE;
	    	   }else{
	    		   rearrangedArray.add(displayObject);
	    	   }
		   } 
			pOrderLookupResponseJson.remove(getOmConfiguration().getDisplayOrderArrayProperty());
			if(donationObject instanceof JsonObject && donationExists){
				rearrangedArray.add(donationObject);
			}
		pOrderLookupResponseJson.add(getOmConfiguration().getDisplayOrderArrayProperty(),rearrangedArray);
	}
	    		   
	    		   

	/**
	 * Populate orderPaymentArray with payment details.
	 * @param pOrderLookupResponseJson
	 *         -the OrderLookupResponseJson
	 */
	private void getOrderPaymentListAsArray(JsonObject pOrderLookupResponseJson) {
		JsonObject orderHeaderJson=pOrderLookupResponseJson.get(getOmConfiguration().getOrderHeaderProperty()).getAsJsonObject();
		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
		if(orderHeaderJson.get(getOmConfiguration().getOrderPaymentListProperty()) instanceof JsonObject){
			JsonObject orderPaymentListJsonObj=orderHeaderJson.get(getOmConfiguration().getOrderPaymentListProperty()).getAsJsonObject();
			   JsonObject orderPaymentJsonObj = null;
			   JsonArray orderPaymentArray=null;
			   if (orderPaymentListJsonObj.get(getOmConfiguration().getOrderPaymentProperty()) instanceof JsonObject) {
				   orderPaymentJsonObj = orderPaymentListJsonObj.get(getOmConfiguration().getOrderPaymentProperty()).getAsJsonObject();
				   orderPaymentArray = new JsonArray();
				   orderPaymentArray.add(orderPaymentJsonObj);
				   orderPaymentListJsonObj.remove(getOmConfiguration().getOrderPaymentProperty());
				   orderPaymentListJsonObj.add(getOmConfiguration().getOrderPaymentProperty(), orderPaymentArray);
			   } else {
				   orderPaymentArray = (JsonArray)orderPaymentListJsonObj.get(getOmConfiguration().getOrderPaymentProperty()).getAsJsonArray();
			   }
			   for(int i=TRURadialOMConstants.INT_ZERO;i<orderPaymentArray.size();i++){
				   JsonObject orderpaymentJsonObj=orderPaymentArray.get(i).getAsJsonObject();
				   String amount=orderpaymentJsonObj.get(getOmConfiguration().getAmountPropertyName()).getAsString();
				   orderpaymentJsonObj.remove(getOmConfiguration().getAmountPropertyName());
				   orderpaymentJsonObj.addProperty(getOmConfiguration().getAmountPropertyName(), decimalFormat.format(Double.parseDouble(amount)));		   
			   }
		}
	}
	
	/**
	 * Populate Order summary data.
	 * @param pOrderLookupResponseJson
	 * 			 the OrderLookupResponseJson
	 */
	private void getOrdersummary(JsonObject pOrderLookupResponseJson) {
		JsonObject orderHeaderJson=pOrderLookupResponseJson.get(getOmConfiguration().getOrderHeaderProperty()).getAsJsonObject();
		JsonObject orderSummary = new JsonObject();
		String partnerId=orderHeaderJson.get(getOmConfiguration().getPartnerIdPropertyName()).getAsString();
		orderHeaderJson.addProperty(getOmConfiguration().getT1OrderExistsPropertyName(), Boolean.FALSE);
		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
		if(orderHeaderJson.get(getOmConfiguration().getCustomAttributesProperty()) instanceof JsonObject){
		JsonObject customAttributes= orderHeaderJson.get(getOmConfiguration().getCustomAttributesProperty()).getAsJsonObject();
		JsonArray customAttributeArray=customAttributes.get(getOmConfiguration().getCustomAttributeProperty()).getAsJsonArray();
		Float value= null;
		double doubleValue;
	    Set<String> ordersummaryKey=getOmConfiguration().getOrderSummaryMap().keySet();
			for (int k = TRURadialOMConstants.INT_ZERO; k < customAttributeArray.size(); k++) {
				  JsonObject customAttribute= customAttributeArray.get(k).getAsJsonObject();
				  String name=customAttribute.get(getOmConfiguration().getNameProperty()).getAsString();
				  String valueCustom=customAttribute.get(getOmConfiguration().getValueProperty()).getAsString();
				  if(ordersummaryKey.contains(name)){
				  value = Float.parseFloat(valueCustom);
				  doubleValue = Double.parseDouble(Float.toString(value));
				  if(name.equals(getOmConfiguration().getOrderCustomAttributePromoSavingsAmt())){ 
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributePromoSavingsAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeShipAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeShipAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeSalesTaxAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeSalesTaxAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeLocalTaxAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeLocalTaxAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeEwasteAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeEwasteAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeMattFeeAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeMattFeeAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeGiftCardAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeGiftCardAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeBalanceAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeBalanceAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeOrderTotal())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeOrderTotal()),decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeSubtotal())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeSubtotal()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeGiftwrapAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeGiftwrapAmt()), decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeShipSurAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeShipSurAmt()),decimalFormat.format(doubleValue));
				  }
				  if(name.equals(getOmConfiguration().getOrderCustomAttributeIslandTaxAmt())){
					  orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeIslandTaxAmt()), decimalFormat.format(doubleValue));
				  }
			   }else if(name.equalsIgnoreCase(getOmConfiguration().getOrderCustomAttributeLoyaltyAccount())){
				   orderHeaderJson.addProperty(getOmConfiguration().getOrderCustomAttributeLoyaltyAccount(), valueCustom);
			   }
		   orderHeaderJson.add(getOmConfiguration().getOrderSummaryProperty(), orderSummary);
		 }
	}else if(partnerId.equals(getOmConfiguration().getPartnerOrderId())){
			orderHeaderJson.addProperty(getOmConfiguration().getT1OrderExistsPropertyName(), Boolean.TRUE);
			if(orderHeaderJson.get(getOmConfiguration().getOrderTotalAmountsPropertyName())!=null){
				JsonObject orderTotalAmounts=orderHeaderJson.get(getOmConfiguration().getOrderTotalAmountsPropertyName()).getAsJsonObject();
				if(getOmConfiguration().getOrderSummaryMerchandiseAmount()!=null){
					orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeSubtotal()),decimalFormat.format(Double.parseDouble(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryMerchandiseAmount()).getAsString())));
				}
				if(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryGrandTotal())!=null){
					orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeOrderTotal()),decimalFormat.format(Double.parseDouble(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryGrandTotal()).getAsString())));
				}
				if(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryTaxAmount())!=null){
					orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeLocalTaxAmt()),decimalFormat.format(Double.parseDouble(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryTaxAmount()).getAsString())));
				}
				if(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryShippingAmount())!=null){
					orderSummary.addProperty(getOmConfiguration().getOrderSummaryMap().get(getOmConfiguration().getOrderCustomAttributeShipAmt()),decimalFormat.format(Double.parseDouble(orderTotalAmounts.get(getOmConfiguration().getOrderSummaryShippingAmount()).getAsString())));
				}
				orderHeaderJson.add(getOmConfiguration().getOrderSummaryProperty(), orderSummary);
			}
	}
		orderHeaderJson.remove(getOmConfiguration().getCustomAttributesProperty());
		orderHeaderJson.remove(getOmConfiguration().getInvoiceTotalAmountsProperty());
	}

	
	/**
	 * get line item details
	 * @param pOrderLookupResponseJson
	 *       - the OrderLookupResponseJson
	 * @param pDisplayOrderArray
	 *       - the DisplayOrderArray
	 */
	private void getlineitemcount(JsonObject pOrderLookupResponseJson, JsonArray pDisplayOrderArray) {
		JsonObject orderHeaderJson=pOrderLookupResponseJson.get(getOmConfiguration().getOrderHeaderProperty()).getAsJsonObject();
		String partnerId=orderHeaderJson.get(getOmConfiguration().getPartnerIdPropertyName()).getAsString();
		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
	  	Format feedFormatter = new SimpleDateFormat(TRURadialOMConstants.DATE_FOMAT, Locale.getDefault());
		Format actuallDateFormat = new SimpleDateFormat(TRURadialOMConstants.FOMATED_DATE,  Locale.getDefault());
		Date date=null;
		int ispuItemCount=TRURadialOMConstants.INT_ZERO;
		int s2hItemCount=TRURadialOMConstants.INT_ZERO;
		Date trackOrderDate=null;
		int totalItemCount=TRURadialOMConstants.INT_ZERO;
       for(int i=TRURadialOMConstants.INT_ZERO;i<pDisplayOrderArray.size();i++){
    	   int lineArrayDetailSize=TRURadialOMConstants.INT_ZERO;
    	   JsonObject shippmentInfoObj = null;
    	   JsonObject lineObject = null;
    	   JsonObject displayObject = pDisplayOrderArray.get(i).getAsJsonObject();
    	   JsonArray shippmentInfoArray=displayObject.get(getOmConfiguration().getShippmentInfoProperty()).getAsJsonArray();
    	   int totalshippmentInfoObjCount= TRURadialOMConstants.INT_ZERO;
    	   for(int j=TRURadialOMConstants.INT_ZERO;j<shippmentInfoArray.size();j++){
    		   Set<String> nameList= new HashSet<String>();
    		   shippmentInfoObj = shippmentInfoArray.get(j).getAsJsonObject();
    		   JsonArray lineDetailArray=shippmentInfoObj.get(getOmConfiguration().getLineDetailsProperty()).getAsJsonArray();
    		    lineArrayDetailSize=lineDetailArray.size();
    		    shippmentInfoObj.addProperty(getOmConfiguration().getDateExistsPropertyName(),Boolean.FALSE);
    		   for(int z=TRURadialOMConstants.INT_ZERO;z<lineArrayDetailSize;z++){
    			   lineObject = lineDetailArray.get(z).getAsJsonObject();
    			   lineObject.remove(getOmConfiguration().getMerchandiseTaxProperty());
    			   lineObject.remove(getOmConfiguration().getShippingAddressProperty());
    			   if(lineObject.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equalsIgnoreCase(TRURadialOMConstants.DONATION)){
    				   shippmentInfoObj.addProperty(getOmConfiguration().getDonationExistsPropertyName(), Boolean.TRUE);
    			   }
    			   lineObject.remove(getOmConfiguration().getShippingAddressProperty());
    			   displayObject.addProperty(getOmConfiguration().getIspuItemProperty(), Boolean.FALSE);
    			   JsonObject shippinAddressObject=displayObject.get(TRURadialOMConstants.SHIPPING_ADDRESS).getAsJsonObject();
    			   if(shippinAddressObject.get(TRURadialOMConstants.ADDRESS)!=null){
    				   displayObject.addProperty(getOmConfiguration().getIspuItemProperty(), Boolean.TRUE);
    				   ispuItemCount++;
    			   }
    			   shippmentInfoObj.addProperty(getOmConfiguration().getTrackingExists(), Boolean.FALSE);
    			   if(lineObject.get(getOmConfiguration().getTrackingInfoProperty())!=null){
    				   shippmentInfoObj.addProperty(getOmConfiguration().getTrackingExists(), Boolean.TRUE);
    				   JsonObject trackingInfo=lineObject.get(getOmConfiguration().getTrackingInfoProperty()).getAsJsonObject();
    			   	   JsonObject trackingList=trackingInfo.get(getOmConfiguration().getTrackingListProperty()).getAsJsonObject();
    			   	   shippmentInfoObj.addProperty(getOmConfiguration().getTrackingNumberProperty(), trackingList.get(getOmConfiguration().getTrackingNumberProperty()).getAsString());
    			   	   shippmentInfoObj.addProperty(getOmConfiguration().getTrackingUrlProperty(), trackingList.get(getOmConfiguration().getTrackingUrlProperty()).getAsString());
    			   }
    			   lineObject.addProperty(getOmConfiguration().getItemColorPropertyName(), Boolean.FALSE);
    			   if(lineObject.get(getOmConfiguration().getColorPropertyName())!=null){
    				   lineObject.addProperty(getOmConfiguration().getItemColorPropertyName(), Boolean.TRUE);
    			   }
    			   lineObject.addProperty(getOmConfiguration().getItemSizeProprtyName(), Boolean.FALSE);
    			   if(lineObject.get(getOmConfiguration().getSizePropertyName())!=null){
    				   lineObject.addProperty(getOmConfiguration().getItemSizeProprtyName(), Boolean.TRUE);
    			   }
    			   lineObject.addProperty(TRURadialOMConstants.BPP_EXISTS, Boolean.FALSE);
    			   lineObject.addProperty(getOmConfiguration().getIsGiftWrapPropertyName(), Boolean.FALSE);
    			   if(lineObject.get(getOmConfiguration().getFeeChargeListProperty()) instanceof JsonObject){
        			   JsonObject feeChargeList=lineObject.get(getOmConfiguration().getFeeChargeListProperty()).getAsJsonObject();
        			   JsonObject feeChargeJsonObj = null;
        			   JsonArray feeChargeArray=null;
        			   if (feeChargeList.get(getOmConfiguration().getFeeChargeProperty()) instanceof JsonObject) {
        				   feeChargeJsonObj = feeChargeList.get(getOmConfiguration().getFeeChargeProperty()).getAsJsonObject();
        				   feeChargeArray = new JsonArray();
        				   feeChargeArray.add(feeChargeJsonObj);
        				   feeChargeList.remove(getOmConfiguration().getFeeChargeProperty());
        				   feeChargeList.add(getOmConfiguration().getFeeChargeProperty(), feeChargeArray);
        			   } else {
        				   feeChargeArray = (JsonArray)feeChargeList.get(getOmConfiguration().getFeeChargeProperty()).getAsJsonArray();
        			   }

        			   for(int k=TRURadialOMConstants.INT_ZERO;k<feeChargeArray.size();k++){
        				  JsonObject feechargeObj= feeChargeArray.get(k).getAsJsonObject();
        				  feechargeObj.remove(getOmConfiguration().getTaxDataProperty());
        				  if(feechargeObj.get(getOmConfiguration().getFeeChargeNamePropertyName()).getAsString().equalsIgnoreCase(getOmConfiguration().getCustomizationDataGiftWrapType())){
        					  lineObject.addProperty(getOmConfiguration().getIsGiftWrapPropertyName(), Boolean.TRUE);
        					  String siteNameForGW=getSiteName(feechargeObj.get(getOmConfiguration().getItemIdPropertyName()).getAsString());
        					  if(siteNameForGW !=null){
        						  lineObject.addProperty(getOmConfiguration().getSiteNameForGWPropertyName(), siteNameForGW);
        					  }
        				  }
        				  if(feechargeObj.get(getOmConfiguration().getFeeChargeNamePropertyName()).getAsString().equalsIgnoreCase(getOmConfiguration().getBppPropertyName())){
        					  lineObject.addProperty(getOmConfiguration().getBppExistsPropertyName(), Boolean.TRUE);
        					  lineObject.addProperty(getOmConfiguration().getBppChargeAmountPropertyName(), feechargeObj.get(getOmConfiguration().getAmountPropertyName()).getAsString());
        					  String bppTerm = getBppTerm(feechargeObj.get(getOmConfiguration().getItemIdPropertyName()).getAsString());
        					  lineObject.addProperty(getOmConfiguration().getBppTermPropertyName(), bppTerm);
        				  }
        			   }
        			  }
    			   
    			   if(lineObject.get(getOmConfiguration().getCustomAttributesProperty()) instanceof JsonObject){
    			   JsonObject customAttributes=lineObject.get(getOmConfiguration().getCustomAttributesProperty()).getAsJsonObject();
    			   JsonObject customAttributesJsonObj = null;
    			   JsonArray CustomAttributeArray=null;
    			   if (customAttributes.get(getOmConfiguration().getCustomAttributeProperty()) instanceof JsonObject) {
    				   customAttributesJsonObj = customAttributes.get(getOmConfiguration().getCustomAttributeProperty()).getAsJsonObject();
    				   CustomAttributeArray = new JsonArray();
    				   CustomAttributeArray.add(customAttributesJsonObj);
    				   customAttributes.remove(getOmConfiguration().getCustomAttributeProperty());
    				   customAttributes.add(getOmConfiguration().getCustomAttributeProperty(), CustomAttributeArray);
    			   } else {
    				   CustomAttributeArray = (JsonArray)customAttributes.get(getOmConfiguration().getCustomAttributeProperty()).getAsJsonArray();
    			   }
    			   if(!partnerId.equalsIgnoreCase(getOmConfiguration().getPartnerOrderId())){
    			   for(int k=TRURadialOMConstants.INT_ZERO;k<CustomAttributeArray.size();k++){
    				  JsonObject customAttribute= CustomAttributeArray.get(k).getAsJsonObject();
    				  String name=customAttribute.get(getOmConfiguration().getNameProperty()).getAsString();
    				  String value=null;
    				  if(customAttribute.get(getOmConfiguration().getValueProperty())!=null){
    					   value=customAttribute.get(getOmConfiguration().getValueProperty()).getAsString();
    				  }
    				  
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeShipMessage())){
    					  String[] shipMethod=value.split(TRURadialOMConstants.COLON_SPLIT);
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getShippingMethodPropertyName(), shipMethod[TRURadialOMConstants.INT_ZERO]);
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributePdpURL())){
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getItemCustomAttributePdpURL(),value);
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeItemPrice())){
    					  nameList.add(name);
    					  double doubleValue;
    					  float floatvalue = Float.parseFloat(value);
    					  doubleValue = Double.parseDouble(Float.toString(floatvalue));
    					  lineObject.addProperty(getOmConfiguration().getItemCustomAttributeItemPricePropertyName(), decimalFormat.format(doubleValue));
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeImageURL())){
    					  nameList.add(name);
    					  String[] image=value.split(TRURadialOMConstants.SPLIT_QUESTION);
    					  if(image[0].contains(TRURadialOMConstants.NULL_STRING)){
    						  lineObject.addProperty(TRURadialOMConstants.IMAGE_EXITS, Boolean.FALSE);
    					  }else{
    						  lineObject.addProperty(getOmConfiguration().getItemCustomAttributeImageURL(), value);
    						  lineObject.addProperty(TRURadialOMConstants.IMAGE_EXITS, Boolean.TRUE);
    					  }
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeRegistryID())){
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getItemCustomAttributeRegistryID(), value);
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeRegistryShip())){
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getItemCustomAttributeRegistryShip(), value);
    				  }
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeRegistryType())){
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getItemCustomAttributeRegistryType(), value);
    				  }
    				  try {
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeEDDStartDate()) && shippmentInfoObj.get(getOmConfiguration().getItemCustomAttributeEDDStartDate())==null){
    					  StringBuffer dateFomated=new StringBuffer();
    					  String startDate = null;
    					  if(value.length()<TRURadialOMConstants.INT_EIGHT){
    						  dateFomated.append(TRURadialOMConstants.INT_ZERO).append(value);
    					  }else{
    						  dateFomated.append(value);
    					  }
						  date = (Date) ((DateFormat) feedFormatter).parse(dateFomated.toString());
						  startDate = actuallDateFormat.format(date);
						  DateFormat df = new SimpleDateFormat(TRURadialOMConstants.DATE_FOMAT);
						  SimpleDateFormat updateFormatter = new SimpleDateFormat(TRUConstants.ORDER_HISTORY_DATE_DISPLAY,Locale.US);
						  trackOrderDate = (Date) ((DateFormat) df).parse(dateFomated.toString());
						  String trackStartDate = updateFormatter.format(trackOrderDate);
						  shippmentInfoObj.addProperty(getOmConfiguration().getTrackOrderShipDateProperty(),trackStartDate);
    					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDStartDate(), startDate);
    					  shippmentInfoObj.addProperty(getOmConfiguration().getDateExistsPropertyName(),Boolean.TRUE);
    					  nameList.add(name);
    					  lineObject.remove(getOmConfiguration().getItemCustomAttributeEDDStartDate());
    					  dateFomated.setLength(TRURadialOMConstants.INT_ZERO);
    				  }
    				  
    				  if(name.equals(getOmConfiguration().getItemCustomAttributeEDDEndDate())  && shippmentInfoObj.get(getOmConfiguration().getItemCustomAttributeEDDEndDate())==null){
    					  StringBuffer dateFomated=new StringBuffer();
    					  String endDate =  null;
    					  if(value.length()<TRURadialOMConstants.INT_EIGHT){
    						  dateFomated.append(TRURadialOMConstants.INT_ZERO).append(value);
    					  }else{
    						  dateFomated.append(value);
    					  }
    					  date = (Date) ((DateFormat) feedFormatter).parse(dateFomated.toString());
    					  endDate = actuallDateFormat.format(date);
    					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDEndDate(), endDate);
    					  nameList.add(name);
    					  lineObject.remove(getOmConfiguration().getItemCustomAttributeEDDEndDate());
    					  dateFomated.setLength(TRURadialOMConstants.INT_ZERO);
    				  }
						} catch (ParseException exception) {
							if(isLoggingError()){
								vlogError("Date format exception", exception);
							}
						}
    				  if(name.equals(getOmConfiguration().getSurchargePropertyName())){
    					  nameList.add(name);
    					  lineObject.addProperty(getOmConfiguration().getSurchargePropertyName(), value);
    				  }
    			   }
    			   if(!nameList.contains(getOmConfiguration().getItemCustomAttributeEDDStartDate())){
     					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDStartDate(), TRURadialOMConstants.EMPTY_STRING);
    			   }
    			   if(!nameList.contains(getOmConfiguration().getItemCustomAttributeEDDEndDate())){
  					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDEndDate(),TRURadialOMConstants.EMPTY_STRING);
 			      }
    			   if(!nameList.contains(getOmConfiguration().getItemCustomAttributeEDDStartDate())){
  					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDStartDate(), TRURadialOMConstants.EMPTY_STRING);
    			   }
    			   if(!nameList.contains(getOmConfiguration().getItemCustomAttributeEDDEndDate())){
					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeEDDEndDate(),TRURadialOMConstants.EMPTY_STRING);
			       }
    			   if(!nameList.contains(getOmConfiguration().getItemCustomAttributeItemPrice())){
 					  shippmentInfoObj.addProperty(getOmConfiguration().getItemCustomAttributeItemPricePropertyName(),decimalFormat.format(Double.parseDouble((lineObject.get(getOmConfiguration().getPricePropertyName()).getAsString()))));
 			       }
    			   lineObject.remove(getOmConfiguration().getCustomAttributesProperty());
    	  }
    			   }
    			   lineObject.addProperty(TRURadialOMConstants.IS_GIFT_MESSAGE, Boolean.FALSE);
    			   if(lineObject.get(getOmConfiguration().getCustomizationListProperty()) instanceof JsonObject){
    				   JsonObject customizationlist=lineObject.get(getOmConfiguration().getCustomizationListProperty()).getAsJsonObject();
        			   JsonObject customizationDataJsonObj = null;
        			   JsonArray customizationDataJsonArray=null;
        			   if (customizationlist.get(getOmConfiguration().getCustomizationDataProperty()) instanceof JsonObject) {
        				   customizationDataJsonObj = customizationlist.get(getOmConfiguration().getCustomizationDataProperty()).getAsJsonObject();
        				   customizationDataJsonArray = new JsonArray();
        				   customizationDataJsonArray.add(customizationDataJsonObj);
        				   customizationlist.remove(getOmConfiguration().getCustomizationDataProperty());
        				   customizationlist.add(getOmConfiguration().getCustomizationDataProperty(), customizationDataJsonArray);
        			   } else {
        				   customizationDataJsonArray = (JsonArray) customizationlist.get(getOmConfiguration().getCustomizationDataProperty()).getAsJsonArray();
        			   }
        			   for(int k=TRURadialOMConstants.INT_ZERO;k<customizationDataJsonArray.size();k++){
        				  JsonObject customizeData= customizationDataJsonArray.get(k).getAsJsonObject();
        				  String type=customizeData.get(getOmConfiguration().getTypePopertyName()).getAsString();
        				  String message=customizeData.get(getOmConfiguration().getMessageProperty()).getAsString();
        				  if(type.equals(getOmConfiguration().getCustomizationDataGiftMessageType())){
        					  lineObject.addProperty(getOmConfiguration().getGiftMessageProperty(),message);
        					  lineObject.addProperty(TRURadialOMConstants.IS_GIFT_MESSAGE, Boolean.TRUE);
        				  }
        				  if(type.equals(getOmConfiguration().getCustomizationDataRegistryMessageType())){
        					  lineObject.addProperty(getOmConfiguration().getRegistryMessageProperty(),message);
        				  }
        			   }
    			   }
    			   lineObject.remove(getOmConfiguration().getCustomizationListProperty());
    			   if(lineObject.get(getOmConfiguration().getPricingProperty())!=null){
    			   JsonObject pricingObject=lineObject.get(getOmConfiguration().getPricingProperty()).getAsJsonObject();
    			   removeTaxDataPricing(pricingObject);
    			   }
    		   }
    		   totalshippmentInfoObjCount++;
    		   shippmentInfoObj.addProperty(getOmConfiguration().getShippmentCountPropertyName(), totalshippmentInfoObjCount);
    		   totalItemCount+=lineArrayDetailSize;
    	   }
    	   displayObject.addProperty(getOmConfiguration().getTotalLineItemCountPropertyName(), lineArrayDetailSize);
    	   displayObject.addProperty(getOmConfiguration().getTotalshippmentInfoCountPropertyName(), totalshippmentInfoObjCount);
    	   }
       	 orderHeaderJson.addProperty(getOmConfiguration().getOnlyDonationExists(), Boolean.FALSE);
         orderHeaderJson.addProperty(getOmConfiguration().getOrderdLineItemsPropertyName(), totalItemCount);
         if(orderHeaderJson.get(TRURadialOMConstants.DONATION_EXISTS).getAsBoolean()){
        	 ispuItemCount+=TRURadialOMConstants.INT_ONE;
           }
         s2hItemCount=totalItemCount-ispuItemCount;
         if(s2hItemCount==TRURadialOMConstants.INT_ZERO){
        	  orderHeaderJson.addProperty(getOmConfiguration().getOnlyDonationExists(), Boolean.TRUE);
          }
	}

	
	/**
	 * To remove tax data pricing form json
	 * @param pPricing
	 * 		- the pricing
	 */
	private void removeTaxDataPricing(JsonObject pPricing) {
		JsonObject marchandise=pPricing.get(getOmConfiguration().getMerchandiseProperty()).getAsJsonObject();
		marchandise.remove(getOmConfiguration().getTaxDataProperty());
		JsonObject shipping=pPricing.get(getOmConfiguration().getShippingProperty()).getAsJsonObject();
		shipping.remove(getOmConfiguration().getTaxDataProperty());
		pPricing.remove(getOmConfiguration().getDutyProperty());
	}

	/**
	 * Populate display order array.
	 *
	 * @param pDisplayOrderArray the display order array
	 * @param pLineObj the line obj
	 * @param pShippingInfoAddress the shipping info address
	 * @param pDispOrderArray the disp order array
	 * @param pDisplayOrderArraySize the display order array size
	 * @param pOrderHeaderJsonObj 
	 */
	public void populateDisplayOrderArray(JsonArray pDisplayOrderArray, JsonObject pLineObj,
			JsonObject pShippingInfoAddress, JsonArray pDispOrderArray,
			int pDisplayOrderArraySize, JsonObject pOrderHeaderJsonObj) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: populateDisplayOrderArray()");
		}
		JsonObject displayObject = new JsonObject();
		JsonArray shippmentInfoArray = new JsonArray();
		JsonArray orderLineArrayUpdated;
		JsonObject shippingInfo;
		JsonElement jsonElementUpdated;
		JsonObject orderLineObjectUpdated;
		JsonElement jsonElementShipInfo;
		JsonObject shipInfoArrayObj;
		JsonObject shippingAddress = new JsonObject();
		JsonArray lineDetailsArray;
		for (int z = TRURadialOMConstants.INT_ZERO; z < pDisplayOrderArraySize; z++) {
			jsonElementUpdated = pDisplayOrderArray.get(z);
			orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
			shippmentInfoArray = orderLineObjectUpdated.get(getOmConfiguration().getShippmentInfoProperty()).getAsJsonArray();
			shippingAddress = orderLineObjectUpdated.get(getOmConfiguration().getShippingAddressProperty()).getAsJsonObject();
			if (pShippingInfoAddress.equals(shippingAddress)) {
				displayObject = orderLineObjectUpdated;
				break;
			}else{
				displayObject = new JsonObject();
			}
		}
		int size=shippmentInfoArray.size();
		displayObject.addProperty(getOmConfiguration().getIsProxyExists(), Boolean.FALSE); 
		if(pLineObj.get(getOmConfiguration().getProxyAddressProperty())!=null){
			displayObject.add(getOmConfiguration().getProxyAddressProperty(),pLineObj.get(getOmConfiguration().getProxyAddressProperty()));
			displayObject.addProperty(getOmConfiguration().getIsProxyExists(), Boolean.TRUE); 
			pLineObj.remove(getOmConfiguration().getProxyAddressProperty());
		}
		if (pShippingInfoAddress.equals(shippingAddress)) {
				for(int n=TRURadialOMConstants.INT_ZERO;n<size;n++){
					jsonElementShipInfo = shippmentInfoArray.get(n);
					shipInfoArrayObj = jsonElementShipInfo.getAsJsonObject();
					String deliveryMode = shipInfoArrayObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString();
					shipInfoArrayObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.FALSE);
					if(deliveryMode.equalsIgnoreCase(TRURadialOMConstants.DONATION)){
						shipInfoArrayObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
						pOrderHeaderJsonObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
					}
					if(pLineObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equals(deliveryMode)){
						lineDetailsArray = shipInfoArrayObj.get(getOmConfiguration().getLineDetailsProperty()).getAsJsonArray();
						lineDetailsArray.add(pLineObj);
						break;
					}
					else {
						shippingInfo = new JsonObject();
						orderLineArrayUpdated = new JsonArray();
						shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(),pLineObj.get(getOmConfiguration().getDeliveryModeProperty()));
						shipInfoArrayObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.FALSE);
						if(pLineObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equalsIgnoreCase(TRURadialOMConstants.DONATION)){
							shipInfoArrayObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
							pOrderHeaderJsonObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
						}
						pLineObj.remove(getOmConfiguration().getShippingAddressProperty());
						orderLineArrayUpdated.add(pLineObj);
						shippingInfo.add(getOmConfiguration().getLineDetailsProperty(), orderLineArrayUpdated);
						shippmentInfoArray.add(shippingInfo);
					}
				}
			}
			else{
					orderLineArrayUpdated = new JsonArray();
					shippmentInfoArray= new JsonArray();
					shippingInfo = new JsonObject();
					displayObject.add(getOmConfiguration().getShippingAddressProperty(),pLineObj.get(getOmConfiguration().getShippingAddressProperty()));
					if(pLineObj.get(getOmConfiguration().getStoreFrontAddressProperty())!=null){
						displayObject.add(getOmConfiguration().getShippingAddressProperty(),pLineObj.get(getOmConfiguration().getStoreFrontAddressProperty()));
						pLineObj.remove(getOmConfiguration().getStoreFrontAddressProperty());
					}
					shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(),pLineObj.get(getOmConfiguration().getDeliveryModeProperty()));
					shippingInfo.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.FALSE);
					if(pLineObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equalsIgnoreCase(TRURadialOMConstants.DONATION)){
						shippingInfo.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
						pOrderHeaderJsonObj.addProperty(TRURadialOMConstants.DONATION_EXISTS, Boolean.TRUE);
					}
					pLineObj.remove(getOmConfiguration().getShippingAddressProperty());
					displayObject.add(getOmConfiguration().getDeliveryModeProperty(), pLineObj.get(getOmConfiguration().getDeliveryModeProperty()));
					orderLineArrayUpdated.add(pLineObj);
					shippingInfo.add(getOmConfiguration().getLineDetailsProperty(), orderLineArrayUpdated);
					shippmentInfoArray.add(shippingInfo);
					displayObject.add(getOmConfiguration().getShippmentInfoProperty(), shippmentInfoArray);
					pDispOrderArray.add(displayObject);
					pDisplayOrderArray.add(displayObject);
			}
				
		if(isLoggingDebug()){
			logDebug("Exit from class: TRUBaseRadialOMUtils method: populateDisplayOrderArray()");
		}
	}
	
	/**
	 * This method is to parse the InvoiceResponse xml.
	 * 
	 * @param pResponseXML
	 *            - Response XML
	 */
	public void parseOrderDetailLookupInvoiceResponse(String pResponseXML){
		if (isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: parseOrderDetailLookupInvoiceResponse()");
			vlogDebug("Response XML : {0}", pResponseXML);
		}
		String jsonResponseString = convertXMLtoJSON(pResponseXML);		
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(jsonResponseString);
		JsonObject asJsonObject = parse.getAsJsonObject();
		JsonObject orderLookupResponseListJson = asJsonObject.get(getOmConfiguration().getOrderLookupResponseListProperty()).getAsJsonObject();
		JsonObject orderLookupResponseJson = orderLookupResponseListJson.get(getOmConfiguration().getOrderLookupResponseProperty()).getAsJsonObject();
		JsonObject orderHeaderJson = orderLookupResponseJson.get(getOmConfiguration().getOrderHeaderProperty()).getAsJsonObject();
		JsonObject invoiceTotalAmounts = orderHeaderJson.get(getOmConfiguration().getInvoiceTotalAmountsProperty()).getAsJsonObject();
		JsonObject invoiceTotalAmountsJsonObj = null;
		JsonArray  invoiceTotalAmountsInvoiceArray=null;
        if (invoiceTotalAmounts.get(getOmConfiguration().getInvoiceProperty()) instanceof JsonObject) {
        	invoiceTotalAmountsJsonObj = invoiceTotalAmounts.getAsJsonObject(getOmConfiguration().getInvoiceProperty());
        	invoiceTotalAmountsInvoiceArray = new JsonArray();
        	invoiceTotalAmountsInvoiceArray.add(invoiceTotalAmountsJsonObj);
        	invoiceTotalAmounts.remove(getOmConfiguration().getInvoiceProperty());
        	invoiceTotalAmounts.add(getOmConfiguration().getInvoiceProperty(), invoiceTotalAmountsInvoiceArray);
        	} else {
        		invoiceTotalAmountsInvoiceArray = (JsonArray) invoiceTotalAmounts.get(getOmConfiguration().getInvoiceProperty());
        	}
		JsonObject lineDetailsJson = orderLookupResponseJson.get(getOmConfiguration().getLineDetailsProperty()).getAsJsonObject();
		JsonObject lineDetailJsonObject = null;
		JsonArray invoiceArrayLineDetails = null;
        if (lineDetailsJson.get(getOmConfiguration().getLineDetailProperty()) instanceof JsonObject) {
        	lineDetailJsonObject = lineDetailsJson.getAsJsonObject(getOmConfiguration().getLineDetailProperty());
        	invoiceArrayLineDetails = new JsonArray();
        	invoiceArrayLineDetails.add(lineDetailJsonObject);
        	lineDetailsJson.remove(getOmConfiguration().getLineDetailProperty());
        	lineDetailsJson.add(getOmConfiguration().getLineDetailProperty(), invoiceArrayLineDetails);
        	} else {
        		invoiceArrayLineDetails = (JsonArray) lineDetailsJson.get(getOmConfiguration().getLineDetailProperty());
        	}
        populateInvoiceDisplayOrderArray(invoiceTotalAmountsInvoiceArray, invoiceArrayLineDetails);
		if (isLoggingDebug()) {
			logDebug("Exit from class: TRUBaseRadialOMUtils method: parseOrderDetailLookupInvoiceResponse()");
		}
	}

	
	/**
	 * This is method is to populate order line details.
	 * @param pInvoiceTotalAmountsInvoiceArray
	 * 			the InvoiceTotalAmountsInvoiceArray
	 * @param pInvoiceArrayLineDetails
	 * 			the InvoiceArrayLineDetails
	 */
	public void populateInvoiceDisplayOrderArray(JsonArray pInvoiceTotalAmountsInvoiceArray, JsonArray pInvoiceArrayLineDetails) {
		if (isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: populateInvoiceDisplayOrderArray()");
		}
		JsonArray displayOrderArray;
		JsonObject displayObject;
		List<String> invoiceArrayIds = new ArrayList<String>();
		JsonArray invoiceTotalAmountsJsonArray = new JsonArray();
		JsonArray shippmentInfoArray = null;
		JsonObject invoiceJsonObj = null;
        for (int i = TRURadialOMConstants.INT_ZERO; i < pInvoiceTotalAmountsInvoiceArray.size(); i++) {
        	JsonObject invoiceObj = pInvoiceTotalAmountsInvoiceArray.get(i).getAsJsonObject();
			String invoiceId = invoiceObj.get(getOmConfiguration().getInvoiceIdProperty()).getAsString();
			int lineSize = pInvoiceArrayLineDetails.size();
		for (int j = TRURadialOMConstants.INT_ZERO; j < lineSize; j++) {
			JsonObject lineObj = pInvoiceArrayLineDetails.get(j).getAsJsonObject();
			if( lineObj.get(getOmConfiguration().getShippingAddressProperty())!=null){
			JsonObject shippingInfoAddress = lineObj.get(getOmConfiguration().getShippingAddressProperty()).getAsJsonObject();
			String lineInvoiceId = lineObj.get(getOmConfiguration().getInvoiceIdProperty()).getAsString();
			JsonArray orderLineArrayUpdated = null;
			JsonObject  shippingInfo= null;
			JsonArray dispOrderArray = new JsonArray();
				JsonElement jsonElementUpdated = null;
				JsonObject orderLineObjectUpdated = null;
				JsonObject shippingAddress = null;
				JsonElement jsonElementDO = null;
				JsonObject jsonObjeDO = null;
				if(invoiceId.equals(lineInvoiceId) && !invoiceArrayIds.contains(lineInvoiceId)){
					invoiceJsonObj = new JsonObject();
					orderLineArrayUpdated = new JsonArray();
					displayOrderArray = new JsonArray();
					displayObject = new JsonObject();
					shippmentInfoArray= new JsonArray();
					shippingInfo = new JsonObject();
					shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(),lineObj.get(getOmConfiguration().getDeliveryModeProperty()));
					displayObject.add(getOmConfiguration().getDeliveryModeProperty(), lineObj.get(getOmConfiguration().getDeliveryModeProperty()));
					displayObject.add(getOmConfiguration().getShippingAddressProperty(),lineObj.get(getOmConfiguration().getShippingAddressProperty()));
					displayObject.add(getOmConfiguration().getProxyAddressProperty(), lineObj.get(getOmConfiguration().getProxyAddressProperty()));
					lineObj.remove(getOmConfiguration().getProxyAddressProperty());
					lineObj.remove(getOmConfiguration().getShippingAddressProperty());
					orderLineArrayUpdated.add(lineObj);
					shippingInfo.add(getOmConfiguration().getLineDetailsProperty(), orderLineArrayUpdated);
					shippmentInfoArray.add(shippingInfo);
					displayObject.add(getOmConfiguration().getShippmentInfoProperty(), shippmentInfoArray);
					dispOrderArray.add(displayObject);
					displayOrderArray.add(displayObject);
					invoiceJsonObj.add(getOmConfiguration().getInvoiceIdProperty(), lineObj.get(getOmConfiguration().getInvoiceIdProperty()));
					invoiceArrayIds.add(lineObj.get(getOmConfiguration().getInvoiceIdProperty()).getAsString());
					invoiceJsonObj.add(getOmConfiguration().getDisplayOrderArrayProperty(), displayOrderArray);
					invoiceTotalAmountsJsonArray.add(invoiceJsonObj);
				}
				else if(invoiceId.equals(lineInvoiceId) && invoiceArrayIds.contains(lineInvoiceId)){
					for (int z = TRURadialOMConstants.INT_ZERO; z < invoiceTotalAmountsJsonArray.size(); z++) {
						jsonElementUpdated = invoiceTotalAmountsJsonArray.get(z);
						orderLineObjectUpdated = jsonElementUpdated.getAsJsonObject();
						displayOrderArray = orderLineObjectUpdated.get(getOmConfiguration().getDisplayOrderArrayProperty()).getAsJsonArray();
						String invoiceObject = orderLineObjectUpdated.get(getOmConfiguration().getInvoiceIdProperty()).getAsString();
						if(invoiceObject.equals(lineInvoiceId)){
						for(int p = TRURadialOMConstants.INT_ZERO; p < displayOrderArray.size();p++){
							jsonElementDO = displayOrderArray.get(p);
							jsonObjeDO = jsonElementDO.getAsJsonObject();
							shippmentInfoArray = jsonObjeDO.get(getOmConfiguration().getShippmentInfoProperty()).getAsJsonArray();
							shippingAddress = jsonObjeDO.get(getOmConfiguration().getShippingAddressProperty()).getAsJsonObject();
							populatOrderLineDetails(displayOrderArray, shippmentInfoArray, lineObj,shippingInfoAddress, shippingAddress);
								}
							}
						}
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class: TRUBaseRadialOMUtils method: populateInvoiceDisplayOrderArray()");
		}
	}	
	/**
	 * This is method is to populate order line details.
	 * @param pDisplayOrderArray
	 * 			the DisplayOrderArray
	 * @param pShippmentInfoArray
	 * 			the ShippmentInfoArray
	 * @param pLineDetailObj
	 * 			the LineDetailObj
	 * @param pShippingInfoAddress
	 * 			the ShippingInfoAddress
	 * @param pShippingAddress
	 * 			the ShippingAddress
	 */
	public void populatOrderLineDetails(JsonArray pDisplayOrderArray, JsonArray pShippmentInfoArray, JsonObject pLineDetailObj,
			JsonObject pShippingInfoAddress, JsonObject pShippingAddress) {
		if (isLoggingDebug()) {
			logDebug("Enter into class: TRURadialOMUtils method: populatOrderLineDetails()");
		}
		JsonObject displayObject = null;
		JsonArray orderLineArrayUpdated = null;
		JsonObject shippingInfo = null;
		JsonElement jsonElementShipInfo = null;
		JsonObject shipInfoArrayObj = null;
		JsonArray lineDetailsArray = null;
		if (pShippingInfoAddress.equals(pShippingAddress)) {
			for (int n = TRURadialOMConstants.INT_ZERO; n < pShippmentInfoArray.size(); n++) {
				jsonElementShipInfo = pShippmentInfoArray.get(n);
				shipInfoArrayObj = jsonElementShipInfo.getAsJsonObject();
				String deliveryMode = shipInfoArrayObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString();
				if (pLineDetailObj.get(getOmConfiguration().getDeliveryModeProperty()).getAsString().equals(deliveryMode)) {
					lineDetailsArray = shipInfoArrayObj.get(getOmConfiguration().getLineDetailsProperty()).getAsJsonArray();
					lineDetailsArray.add(pLineDetailObj);
					break;
				} else {
					shippingInfo = new JsonObject();
					orderLineArrayUpdated = new JsonArray();
					shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(), pLineDetailObj.get(getOmConfiguration().getDeliveryModeProperty()));
					pLineDetailObj.remove(getOmConfiguration().getShippingAddressProperty());
					orderLineArrayUpdated.add(pLineDetailObj);
					shippingInfo.add(getOmConfiguration().getLineDetailsProperty(), orderLineArrayUpdated);
					pShippmentInfoArray.add(shippingInfo);
				}
			}
		} else {
			displayObject = new JsonObject();
			orderLineArrayUpdated = new JsonArray();
			JsonArray  newShippmentInfoArray = new JsonArray();
			shippingInfo = new JsonObject();
			displayObject.add(getOmConfiguration().getShippingAddressProperty(), pLineDetailObj.get(getOmConfiguration().getShippingAddressProperty()));
			shippingInfo.add(getOmConfiguration().getDeliveryModeProperty(),pLineDetailObj.get(getOmConfiguration().getDeliveryModeProperty()));
			displayObject.add(getOmConfiguration().getDeliveryModeProperty(), pLineDetailObj.get(getOmConfiguration().getDeliveryModeProperty()));
			pLineDetailObj.remove(getOmConfiguration().getShippingAddressProperty());
			if(pLineDetailObj.get(getOmConfiguration().getStoreFrontAddressProperty())!=null){
				displayObject.add(getOmConfiguration().getShippingAddressProperty(),pLineDetailObj.get(getOmConfiguration().getStoreFrontAddressProperty()));
				pLineDetailObj.remove(getOmConfiguration().getStoreFrontAddressProperty());
			}
			if(pLineDetailObj.get(getOmConfiguration().getProxyAddressProperty())!=null){
				displayObject.add(getOmConfiguration().getShippingAddressProperty(),pLineDetailObj.get(getOmConfiguration().getProxyAddressProperty()));
				pLineDetailObj.remove(getOmConfiguration().getProxyAddressProperty());
			}
			orderLineArrayUpdated.add(pLineDetailObj);
			shippingInfo.add(getOmConfiguration().getLineDetailsProperty(),orderLineArrayUpdated);
			newShippmentInfoArray.add(shippingInfo);
			displayObject.add(getOmConfiguration().getShippmentInfoProperty(),newShippmentInfoArray);
			pDisplayOrderArray.add(displayObject);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from class: TRUBaseRadialOMUtils method: populatOrderLineDetails()");
		}
	}
	
	/**
	 * This method is to parse the the order look up  response XML to display in the from end.
	 *
	 * @param pResponseXML the response xml
	 * @return the string
	 */
	public String parseOrderLookupResponse(String pResponseXML) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: parseOrderLookupResponse()");
			vlogDebug("Response XML : {0}", pResponseXML);
		}
		String responseJson=convertXMLtoJSON(pResponseXML);
		Gson gson = new Gson();
		double doubleValue;
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(responseJson);
		JsonObject jsonObj = parse.getAsJsonObject();
		JsonArray orderJsonArray =null;
		JsonObject customerLookupResponseJson = jsonObj.get(getOmConfiguration().getCustomerLookupResponseProperty()).getAsJsonObject();
		JsonElement noJsonOrders = customerLookupResponseJson.get(getOmConfiguration().getOrderListProperty());
		if(noJsonOrders instanceof JsonPrimitive){
			customerLookupResponseJson.remove(getOmConfiguration().getOrderListProperty());
			jsonObj.remove(getOmConfiguration().getCustomerLookupResponseProperty());
			JsonObject emptyJsonOrderList = new JsonObject();
			emptyJsonOrderList.addProperty(getOmConfiguration().getMessageProperty(), getOmConfiguration().getOrderSummaryErrorMsgValueProperty());
			jsonObj.add(getOmConfiguration().getOrderListProperty(), emptyJsonOrderList);
		}
		else{
		JsonObject orderListJson = customerLookupResponseJson.get(getOmConfiguration().getOrderListProperty()).getAsJsonObject();
		if(orderListJson.get(getOmConfiguration().getOrderProperty()) instanceof JsonObject){
			orderJsonArray=new JsonArray();
			JsonObject orderJsonObject = orderListJson.get(getOmConfiguration().getOrderProperty()).getAsJsonObject();
			orderJsonArray.add(orderJsonObject);
			orderListJson.remove(getOmConfiguration().getOrderProperty());
			orderListJson.add(getOmConfiguration().getOrderProperty(), orderJsonArray);			
		}
		else{
			orderJsonArray = orderListJson.get(getOmConfiguration().getOrderProperty()).getAsJsonArray();
		}
		for (int j = TRURadialOMConstants.INT_ZERO; j < orderJsonArray.size(); j++) {
			JsonObject orderObj = orderJsonArray.get(j).getAsJsonObject();
        	String orderStatus = orderObj.get(getOmConfiguration().getOrderStatusProperty()).getAsString();
        	String orderTotal = orderObj.get(getOmConfiguration().getOrderTotalPropertyName()).getAsString();
    		DecimalFormat decimalFormat = new DecimalFormat(TRURadialOMConstants.DECIAML_FORMAT);
    		Float value = Float.parseFloat(orderTotal);
			doubleValue = Double.parseDouble(Float.toString(value));
        	orderObj.remove(getOmConfiguration().getOrderTotalPropertyName());
        	orderObj.addProperty(getOmConfiguration().getOrderTotalPropertyName(), decimalFormat.format(doubleValue));
			Map<String, String> orderDetailStatusesMap = getOmConfiguration().getOrderDetailStatusesMap();
			String statusMapValue = orderDetailStatusesMap.get(orderStatus);
			formatOrderDate(orderObj);
			orderObj.remove(getOmConfiguration().getOrderStatusProperty());
			orderObj.addProperty(getOmConfiguration().getOrderStatusProperty(), statusMapValue);
		}
		}
		responseJson = gson.toJson(jsonObj);
		if(isLoggingDebug()){
			vlogDebug("Order look up response in JSON format: {0}", responseJson);
		}		
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: parseOrderLookupResponse()");
		}
		return responseJson;
	}
	
	
	/**
	 * This method is to covert the response xml.to Json.
	 *
	 * @param pResponseXML the response xml
	 * @return the string
	 */
	public String convertXMLtoJSON(String pResponseXML) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: covertXMLtoJSON()");
			vlogDebug("Response XML : {0}", pResponseXML);
		}
		JsonObject responseJSON = null;
		String jsonString = null;
		if(pResponseXML != null){
			JSONObject jsonObject = XML.toJSONObject(pResponseXML);
			String responseJSONObj = jsonObject.toString();
			JsonParser parser = new JsonParser();
			JsonElement parse = parser.parse(responseJSONObj);
			responseJSON = parse.getAsJsonObject();
			Gson gson = new Gson();
			jsonString = gson.toJson(responseJSON);
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: covertXMLtoJSON()");
		}
		return jsonString;
	}
	
	/**
	 * This method is to return the time zone.
	 * 
	 * @return timeZone - TimeZone
	 */
	public TimeZone getTimeZone(){
		Calendar cal = Calendar.getInstance();
		long milliDiff = cal.get(Calendar.ZONE_OFFSET);
		// Got local offset, now loop through available time zone id(s).
		String [] ids = TimeZone.getAvailableIDs();
		String timeZone = null;
		for (String id : ids) {
		  TimeZone tz = TimeZone.getTimeZone(id);
		  if (tz.getRawOffset() == milliDiff) {
		    timeZone = id;
		    break;
		  }
		}
		return TimeZone.getTimeZone(timeZone);
	}
	
	/**
	 * This method will unmarshal the input stream to response object.
	 * 
	 * @param pInputStream
	 *            - InputStream
	 * @param pPackage
	 *            - Package
	 * @return object - Response Object
	 */
	public  Object unMarshalResponseObject(InputStream pInputStream, String pPackage) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: unMarshalResponseObject()");
		}
		Object unmarshalObj = null;
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(pPackage);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			unmarshalObj = jaxbUnmarshaller.unmarshal(pInputStream);
		} catch (JAXBException e) {
			if(isLoggingError()){
				vlogError("JAXBException while unmarshalling the response object and exception details are : {0}", e);
			}
		}
		if(isLoggingDebug()){
			logDebug("Exit from class: TRURadialOMUtils method: unMarshalResponseObject()");
		}
		return unmarshalObj;
	}
	
	/**
	 * This method is to marshal the request object to XML string.
	 * 
	 * @param pJAXBObject
	 *            - Request object
	 * @param pPackage
	 *            - Package
	 * @return request - Request XML
	 */
	public   String marshalRequestXML(Object pJAXBObject, String pPackage) {
		if(isLoggingDebug()){
			logDebug("Enter into class: TRURadialOMUtils method: marshalRequestXML()");
		}
		String xmlRequest = null;
		if(pJAXBObject == null){
			return xmlRequest;
		}
		StringWriter sw = new StringWriter();
		try {
			JAXBContext	context = JAXBContext.newInstance(pPackage);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(getOmConfiguration().getJaxbOutputFormat(),Boolean.TRUE);
			marshaller.marshal(pJAXBObject, sw);
			xmlRequest = sw.toString();
			sw.close();
		} catch (JAXBException exc) {
			if(isLoggingError()){
				vlogError("JAXBException while Marshalling the Request XML and exception is : {0}", exc);
			}
		} catch (IOException exc) {
			if(isLoggingError()){
				vlogError("IOException while Marshalling the Request XML and exception is : {0}", exc);
			}
		}
		if(isLoggingDebug()){
			vlogDebug("Request XML is : {0}", xmlRequest);
			logDebug("Enter into class: TRURadialOMUtils method: marshalRequestXML()");
		}
		return xmlRequest;
	}
	
	/**
	 * Method to set 1st GiftCard Payment Group and then other Payment Groups.
	 * @param pPaymentGroups - List of Payment Group from Order Object
	 * @return : List - List of Payment Group after sorting based on Payment Group type
	 */
	public List<PaymentGroup> orderPaymentGroupBasedOnType(List<PaymentGroup> pPaymentGroups) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseRadialOMUtils  method: orderPaymentGroupBasedOnType]");
		}
		List<PaymentGroup> paymentGroup = null;
		if(pPaymentGroups == null || pPaymentGroups.isEmpty()){
			return paymentGroup;
		}
		for(PaymentGroup lPaymentGroup : pPaymentGroups){
			if(lPaymentGroup instanceof TRUGiftCard){
				if(paymentGroup == null){
					paymentGroup = new ArrayList<PaymentGroup>();
				}
				paymentGroup.add(lPaymentGroup);
			}
		}
		for(PaymentGroup lPaymentGroup : pPaymentGroups){
			if(paymentGroup == null){
				paymentGroup = new ArrayList<PaymentGroup>();
			}
			if(!paymentGroup.contains(lPaymentGroup)){
				paymentGroup.add(lPaymentGroup);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: orderPaymentGroupBasedOnType]");
		}
		return paymentGroup;
	}
	
	/**
	 * Method to check whether Order is Pay In Store or not. 
	 * 
	 * @param pOrder - Order Object
	 * @return boolean - True : If order is Pay in Store.
	 */
	@SuppressWarnings("unchecked")
	public boolean isPayInStoreOrder(Order pOrder) {
		if (isLoggingDebug()) {
			logDebug("Enter intos [Class: TRUBaseRadialOMUtils  method: isPayInStoreOrder]");
			vlogDebug("pOrder : {0}" ,pOrder);
		}
		boolean isPayInStore = Boolean.FALSE;
		if(pOrder == null){
			return isPayInStore;
		}
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		if(paymentGroups == null || paymentGroups.isEmpty()){
			return isPayInStore;
		}
		for(PaymentGroup lPG : paymentGroups){
			if(lPG instanceof InStorePayment){
				isPayInStore = Boolean.TRUE;
				break;
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: isPayInStoreOrder]");
			vlogDebug("Order is Pay In Store : {0}" ,isPayInStore);
		}
		return isPayInStore;
	}
	
	/**
	 * Method to get the Estimated delivery date for OMS.
	 * @param pEstimateShipWindowMin : int
	 * @return String : Date in format MMddYYYY
	 */
	public String getEstimateShippingDate(int pEstimateShipWindowMin) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseRadialOMUtils  method: getEstimateShippingDate]");
			vlogDebug("pEstimateShipWindowMin: {0}",pEstimateShipWindowMin);
		}
		String estDeliveryDate = null;
		DateFormat df = new SimpleDateFormat(TRURadialOMConstants.REFERENCE_DATE_FORMAT,Locale.US);
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, pEstimateShipWindowMin);
		estDeliveryDate = df.format(now.getTime());
		if (isLoggingDebug()) {
			vlogDebug("Returning with estDeliveryDate: {0}",estDeliveryDate);
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: pEstimateShipWindowMin]");
		}
		return estDeliveryDate;
	}
	
	/**
	 * This method is used to get the onlinePId from sku item to construct PDP url. 
	 * @param pItemId - item id
	 * @return ProductPageUrl -  product url
	 */
	public String getProductPageURL(String pItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseRadialOMUtils  method: getProductPageURL]");
		}
		String onlinePID = null;
		String ProductPageUrl = null;
		RepositoryItem skuItem = null;
		if (!StringUtils.isBlank(pItemId)) {
			try {
				 skuItem = getCatalogTools().findSKU(pItemId);
				 if(skuItem != null && skuItem.getPropertyValue(getOmConfiguration().getOnlinePID()) != null) {
					 onlinePID = (String) skuItem.getPropertyValue(getOmConfiguration().getOnlinePID());
					 ProductPageUrl = getProductPageUtil().constructPDPURLWithOnlinePID(onlinePID,TRURadialOMConstants.TOYS_R_US);
					}
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting promo status items : {0}", exc);
				}
			} 
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: getProductPageURL]");
		}
		return ProductPageUrl;
	}
	
	/**
	 * This method processes the order create  XML request.
	 * @param pCustomerOrderImportRequest - order create request XML
	 * @param pOrderId - Order Id
	 */
	public void processOrderCreateRequest(String pCustomerOrderImportRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: processOrderCreateRequest]");
		}
		if(pCustomerOrderImportRequest == null){
			return;
		}
		String customerOrderDetailJSONResponse = null;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		String customerOrderImportTransPrefix = configuration.getCustomerOrderImportTransPrefix();
		if ((!StringUtils.isBlank(pCustomerOrderImportRequest))) {
			// Create Audit repository item
			getTruRadialOMTools().createOMSAuditItem(customerOrderImportTransPrefix + pOrderId,
					pCustomerOrderImportRequest, customerOrderDetailJSONResponse,TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE, null,null);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: processOrderCreateRequest]");
		}
	}
	
	
	/**
	 * This method is to return the gift wrap name.
	 * @param pItemId  - item Id
	 * @return giftWrapName - gift wrap name
	 */
	private String getSiteName(String pItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getSiteName]");
		}
		RepositoryItem skuItem = null;
		String giftWrapName = null;
		if (!StringUtils.isBlank(pItemId)) {
			try {
				skuItem = getCatalogTools().findSKU(pItemId);
				if (skuItem != null) {
					 giftWrapName = (String) skuItem.getPropertyValue(getOmConfiguration().getOrderDetailsDisplayNamePropertyName());
					if (giftWrapName != null) {
						if (giftWrapName.contains(TRURadialOMConstants.TRU)) {
							giftWrapName = getOmConfiguration().getGiftWrapTRUSiteName();
						}
						if (giftWrapName.contains(TRURadialOMConstants.BRU)) {
							giftWrapName = getOmConfiguration().getGiftWrapBRUSiteName();
						}
					}
				}
			} catch (RepositoryException exc) {
				if (isLoggingError()) {
					vlogError("RepositoryException:  While getting promo status items : {0}", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getSiteName]");
		}
		return giftWrapName;
	}
	
	
	/**
	 * This method is to return BPP term value.
	 * @param pBppItemId  - bpp item id
	 * @return bppTerm - bpp term value
	 */
	private String getBppTerm(String pBppItemId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseOMSUtils  method: getBppTerm]");
		}
		String bppTerm = null;
		if (!StringUtils.isBlank(pBppItemId)) {
			Repository catalog = (Repository) getCatalogRepository();
			if (catalog != null) {
				try {
					RepositoryView view = catalog.getView(TRUCommerceConstants.BPP);
					QueryBuilder queryBuilder = view.getQueryBuilder();
					QueryExpression expression1 = queryBuilder.createPropertyQueryExpression(TRURadialOMConstants.SKU);
					QueryExpression expression2 = queryBuilder.createConstantQueryExpression(pBppItemId); 
					Query query = queryBuilder.createComparisonQuery(expression1, expression2, QueryBuilder.EQUALS);
					RepositoryItem[] bppItems = view.executeQuery(query);
					if ((bppItems != null) && (bppItems.length > Constants.VALUE_ZERO)) {
						bppTerm=(String) bppItems[Constants.VALUE_ZERO].getPropertyValue(getOmConfiguration().getOrderDetailsTermPropertyName());
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError(
								"Repository Exception in TRUCatalogTools @method: getBppItemsBasedOnBppName()",
								e);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseOMSUtils  method: getBppTerm]");
		}
		return bppTerm;
	}
	
	
	
	
	/**
	 * This method processes the order update  XML request.
	 * @param pOrderUpdateRequest - Order update request
	 * @param pOrderId - Order Id
	 */
	public void processOrderUpdateRequest(String pOrderUpdateRequest, String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: processOrderUpdateRequest]");
		}
		if(pOrderUpdateRequest == null){
			return;
		}
		String customerOrderDetailJSONResponse = null;
		TRURadialOMConfiguration configuration = getOmConfiguration();
		String orderUpdateTransPrefix = configuration.getCustomerOrderUpdateTransPrefix();
		if ((!StringUtils.isBlank(pOrderUpdateRequest))) {
			// Create Audit repository item
			getTruRadialOMTools().createOMSAuditItem(orderUpdateTransPrefix + pOrderId,
					pOrderUpdateRequest, customerOrderDetailJSONResponse,TRURadialOMConstants.ORDER_UPDATE_SERVICE, null,null);
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: processOrderUpdateRequest]");
		}
	}
	
	/**
	 * This method is to populate the color and size of the item
	 * @param pProductItem - Product Item
	 * @param pSkuItem  SKU Item
	 * @param pDescription - Description XML Object
	 */
	@SuppressWarnings("unchecked")
	public void populateColorAndSize(RepositoryItem pProductItem, RepositoryItem pSkuItem, Description pDescription){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRUBaseRadialOMUtils  method: populateColorAndSize()]");
		}
		if(pProductItem == null || pSkuItem == null || pDescription == null){
			return;
		}
		TRUCommercePropertyManager propertyManager = getCommercePropertyManager();
		List<String> styleDimensions = (List<String>) (pProductItem.getPropertyValue(propertyManager.getStyleDimensionsPropertyName()));
		if (styleDimensions != null && !styleDimensions.isEmpty()) {
			String colorCode = null;
			String size = null;
			if (null != pSkuItem.getPropertyValue(getPropertyManager().getColorUpcLevel())) {
				RepositoryItem clorItem = (RepositoryItem) pSkuItem.getPropertyValue(getPropertyManager().getColorUpcLevel());
				colorCode = String.valueOf(clorItem.getPropertyValue(propertyManager.getColorNamePropertyName())).toLowerCase();
			}
			if (null != pSkuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize())) {
				RepositoryItem sizeItem = (RepositoryItem) pSkuItem.getPropertyValue(getPropertyManager().getJuvenileProductSize());
				size = String.valueOf(sizeItem.getPropertyValue(propertyManager.getSizeDescriptionPropertyName())).toUpperCase();
			}
			boolean colorFlag = Boolean.FALSE;
			boolean sizeFlag = Boolean.FALSE;
			for (String styleDimension : styleDimensions) {
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.COLOR)) {
					colorFlag = Boolean.TRUE;
				}
				if (styleDimension.equalsIgnoreCase(TRUCommerceConstants.SIZE)) {
					sizeFlag = Boolean.TRUE;
				}
			}
			if (colorFlag && sizeFlag) {
				pDescription.setColor(colorCode);
				pDescription.setSize(size);
			} else if (colorFlag && !sizeFlag) {
				pDescription.setColor(colorCode);
			} else if (!colorFlag && sizeFlag) {
				pDescription.setSize(size);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRUBaseRadialOMUtils  method: populateColorAndSize()]");
		}
	}
	
	/**
	 * Convert string json.
	 *
	 * @param pJsonResponseString the json response string
	 * @return the map
	 */
	public Map<String, Object> convertStringJson(String pJsonResponseString) {
		Map<String, Object> formatedObj;
		JsonParser pareser = new JsonParser();
		JsonElement parse = pareser.parse(pJsonResponseString);
		JsonObject asJsonObject = parse.getAsJsonObject();
		formatedObj= jsonToMap(asJsonObject);
		return formatedObj;
	}
	/** The m commerce property manager. */
	private TRUCommercePropertyManager mCommercePropertyManager;

	/**
	 * Gets the commerce property manager.
	 *
	 * @return the commerce property manager
	 */
	public TRUCommercePropertyManager getCommercePropertyManager() {
		return mCommercePropertyManager;
	}

	/**
	 * Sets the commerce property manager.
	 *
	 * @param pCommercePropertyManager the new commerce property manager
	 */
	public void setCommercePropertyManager(TRUCommercePropertyManager pCommercePropertyManager) {
		this.mCommercePropertyManager = pCommercePropertyManager;
	}

	/**
	 * This is to get the Value for omConfiguration.
	 *
	 * @return the omConfiguration Value
	 */
	public TRURadialOMConfiguration getOmConfiguration() {
		return mOmConfiguration;
	}

	/**
	 * This method to set the pOmConfiguration with omConfiguration.
	 *
	 * @param pOmConfiguration the omConfiguration to set
	 */
	public void setOmConfiguration(TRURadialOMConfiguration pOmConfiguration) {
		mOmConfiguration = pOmConfiguration;
	}

	/**
	 * This is to get the Value for orderManager.
	 *
	 * @return the orderManager Value
	 */
	public TRUOrderManager getOrderManager() {
		return mOrderManager;
	}

	/**
	 * This method to set the pOrderManager with orderManager.
	 *
	 * @param pOrderManager the orderManager to set
	 */
	public void setOrderManager(TRUOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * This is to get the Value for propertyManager.
	 *
	 * @return the propertyManager Value
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method to set the pPropertyManager with propertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * This is to get the Value for catalogTools.
	 *
	 * @return the catalogTools Value
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	/**
	 * This method to set the pCatalogTools with catalogTools.
	 *
	 * @param pCatalogTools the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	/**
	 * This is to get the Value for productPageUtil.
	 *
	 * @return the productPageUtil Value
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}

	/**
	 * This method to set the pProductPageUtil with productPageUtil.
	 *
	 * @param pProductPageUtil the productPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}

	/**
	 * This is to get the Value for siteContextManager.
	 *
	 * @return the siteContextManager Value
	 */
	public SiteContextManager getSiteContextManager() {
		return mSiteContextManager;
	}

	/**
	 * This method to set the pSiteContextManager with siteContextManager.
	 *
	 * @param pSiteContextManager the siteContextManager to set
	 */
	public void setSiteContextManager(SiteContextManager pSiteContextManager) {
		mSiteContextManager = pSiteContextManager;
	}

	/**
	 * This is to get the Value for catalogRepository.
	 *
	 * @return the catalogRepository Value
	 */
	public Repository getCatalogRepository() {
		return mCatalogRepository;
	}

	/**
	 * This method to set the pCatalogRepository with catalogRepository.
	 *
	 * @param pCatalogRepository the catalogRepository to set
	 */
	public void setCatalogRepository(Repository pCatalogRepository) {
		mCatalogRepository = pCatalogRepository;
	}

	/**
	 * This is to get the Value for lineNumber
	 *
	 * @return the lineNumber Value
	 */
	public int getLineNumber() {
		return mLineNumber;
	}

	/**
	 * This method to set the pLineNumber with lineNumber
	 * 
	 * @param pLineNumber the lineNumber to set
	 */
	public void setLineNumber(int pLineNumber) {
		mLineNumber = pLineNumber;
	}

	/**
	 * This is to get the value for truRadialOMTools
	 *
	 * @return the truRadialOMTools value
	 */
	public TRURadialOMTools getTruRadialOMTools() {
		return mTruRadialOMTools;
	}

	/**
	 * This method to set the pTruRadialOMTools with truRadialOMTools
	 * 
	 * @param pTruRadialOMTools the truRadialOMTools to set
	 */
	public void setTruRadialOMTools(TRURadialOMTools pTruRadialOMTools) {
		mTruRadialOMTools = pTruRadialOMTools;
	}
	
	/**
	 * @return the alertLogger
	 */
	public TRUAlertLogger getAlertLogger() {
		return mAlertLogger;
	}

	/**
	 * @param pAlertLogger the alertLogger to set
	 */
	public void setAlertLogger(TRUAlertLogger pAlertLogger) {
		mAlertLogger = pAlertLogger;
	}

	/**
	 * This helper method is to call logFailure() of alert logging with service operation data.
	 * @param pProcessName - process name
	 * @param pTask - task name
	 * @param pError - error value
	 * @param pResponseCode - responseCode value
	 * @param pStartTime - startTime
	 * @param pEndTime - endTime
	 * @param pOperation - name of Task
	 */
	public void sendAlertLogFailure(String pProcessName, String pTask, String pOperation, String pResponseCode, String pError, long pStartTime, long pEndTime) {
		Map<String, String> extraParams = new ConcurrentHashMap<String, String>();
		extraParams.put(TRURadialOMConstants.OPERATION_NAME, pOperation);
		extraParams.put(TRURadialOMConstants.TIME_STAMP, String.valueOf(pStartTime));
		extraParams.put(TRURadialOMConstants.TIME_TAKEN, String.valueOf(pEndTime - pStartTime));
		extraParams.put(TRURadialOMConstants.ERROR, pError);
		extraParams.put(TRURadialOMConstants.RESPONSE_CODE, pResponseCode);
		getAlertLogger().logFailure(pProcessName, pTask, extraParams);
	}

	/**
	 * This method is to get shippingManager
	 *
	 * @return the shippingManager
	 */
	public TRUShippingManager getShippingManager() {
		return mShippingManager;
	}

	/**
	 * This method sets shippingManager with pShippingManager
	 *
	 * @param pShippingManager the shippingManager to set
	 */
	public void setShippingManager(TRUShippingManager pShippingManager) {
		mShippingManager = pShippingManager;
	}
}