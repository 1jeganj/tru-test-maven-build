package com.tru.jda.integration.radialom;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.tru.common.cml.SystemException;
import com.tru.errorhandler.mgl.DefaultErrorHandlerManager;
import com.tru.jda.integration.configuration.TRURadialOMConstants;

/**
 * This droplet is to Call the Error Handler Repository for fetching the error values
 * for OMS.
 * 
 * @author Professional Access
 * @version 1.0
 */
public class TRUCustomerOrderErrorMessages extends DynamoServlet {

	/**
	 * Property to Hold Error handler manager.
	 */
	private DefaultErrorHandlerManager mErrorHandlerManager;

	/**
	 * @return the errorHandlerManager
	 */
	public DefaultErrorHandlerManager getErrorHandlerManager() {
		return mErrorHandlerManager;
	}

	/**
	 * @param pErrorHandlerManager
	 *            the errorHandlerManager to set
	 */
	public void setErrorHandlerManager(
			DefaultErrorHandlerManager pErrorHandlerManager) {
		mErrorHandlerManager = pErrorHandlerManager;
	}

	/**
	 * This service method is to call the Error Handler Repository for fetching
	 * the error values for OMS.
	 * 
	 * @param pRequest
	 *            - DynamoHttpServletRequest
	 * @param pResponse
	 *            - DynamoHttpServletResponse
	 * @throws ServletException
	 *             - if any
	 * @throws IOException
	 *             - if any
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug()) {
			logDebug("Enter into Class : TRUCustomerOrderErrorHandlerKeys method : service");
		}
		String layawayNoOrderMessageValue = null;
		String noOrderMessageValue = null;
		String noOrderFoundMessageValue = null;
		String zipCodeNotMatchMessageValue = null;
		String giftCardpayInStoreErrorMessgaeValue = null;
		String layawayNoOrderFoundMessageValue = null;
		String layawayNoOrderMessage = (String) pRequest.getLocalParameter(TRURadialOMConstants.LAYAWAY_NO_ORDER_MESSAGE);
		String noOrderMessage = (String) pRequest.getLocalParameter(TRURadialOMConstants.NO_ORDER_MESSAGE);
		String noOrderFoundMessage = (String) pRequest.getLocalParameter(TRURadialOMConstants.NO_ORDER_FOUND_MESSAGE);
		String layawayNoOrderFoundMessage = (String) pRequest.getLocalParameter(TRURadialOMConstants.LAYAWAY_NO_ORDER_FOUND_MESSAGE);
		String zipCodeNotMatchMessage = (String) pRequest.getLocalParameter(TRURadialOMConstants.ZIP_CODE_NOT_MATCH_MESSAGE);
		String giftCardpayInStoreErrorMessgae = (String) pRequest.getLocalParameter(TRURadialOMConstants.GIFTCARD_PAYINSTORE_ERROR_MESSAGE);
			try {
				layawayNoOrderMessageValue = getErrorHandlerManager().getErrorMessage(layawayNoOrderMessage);
				noOrderMessageValue = getErrorHandlerManager().getErrorMessage(noOrderMessage);
				noOrderFoundMessageValue = getErrorHandlerManager().getErrorMessage(noOrderFoundMessage);
				zipCodeNotMatchMessageValue = getErrorHandlerManager().getErrorMessage(layawayNoOrderFoundMessage);
				giftCardpayInStoreErrorMessgaeValue = getErrorHandlerManager().getErrorMessage(zipCodeNotMatchMessage);
				layawayNoOrderFoundMessageValue = getErrorHandlerManager().getErrorMessage(giftCardpayInStoreErrorMessgae);
			} catch (SystemException sysExp) {
				if (isLoggingError()) {
					vlogError("SystemException at TRUCustomerOrderErrorHandlerKeys.service method",sysExp);
				}
			}
			pRequest.setParameter(TRURadialOMConstants.LAYAWAY_NO_ORDER_MESSAGE, layawayNoOrderMessageValue);
			pRequest.setParameter(TRURadialOMConstants.NO_ORDER_MESSAGE, noOrderMessageValue);
			pRequest.setParameter(TRURadialOMConstants.NO_ORDER_FOUND_MESSAGE, noOrderFoundMessageValue);
			pRequest.setParameter(TRURadialOMConstants.LAYAWAY_NO_ORDER_FOUND_MESSAGE, zipCodeNotMatchMessageValue);
			pRequest.setParameter(TRURadialOMConstants.ZIP_CODE_NOT_MATCH_MESSAGE, giftCardpayInStoreErrorMessgaeValue);
			pRequest.setParameter(TRURadialOMConstants.GIFTCARD_PAYINSTORE_ERROR_MESSAGE, layawayNoOrderFoundMessageValue);
			pRequest.serviceLocalParameter(TRURadialOMConstants.OUTPUT, pRequest,pResponse);
		if (isLoggingDebug()) {
			logDebug("Exit from Class : TRUCustomerOrderErrorHandlerKeys method : service");
		}
	}
}
