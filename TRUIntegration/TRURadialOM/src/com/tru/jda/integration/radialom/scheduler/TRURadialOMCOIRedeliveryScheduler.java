package com.tru.jda.integration.radialom.scheduler;

import java.util.List;
import java.util.Map;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.processor.TRURadialOMSendFulfillmentMessage;
import com.tru.jda.integration.radialom.TRURadialOMTools;

/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository in case of get success response.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMCOIRedeliveryScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds reference for mEnable.
	 */
	private boolean mEnable;
	
	/**
	 * Holds reference for mOrderIds.
	 */
	private List<String> mOrderIds;

	/**
	 * Holds reference for mSendFulfillmentMessage.
	 */
	private TRURadialOMSendFulfillmentMessage mSendFulfillmentMessage;
	
	/**
	 * Holds reference for mDurationToSendRequestAgain 
	 */
	private int mDurationToSendRequestAgain;
	
	/**
	 * Holds reference for mNumberOfTimeToSendRequest 
	 */
	private int mNumberOfTimeToSendRequest;
	
	/**
	 * Holds reference for mTruRadialOMTools 
	 */
	private TRURadialOMTools mTruRadialOMTools;
	
	/**
	 * Overriding OOTB method to re-deliver the failed OMS request.
	 * 
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,ScheduledJob pParamScheduledJob) {
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: doScheduledTask() method :: STARTS ");
		}
		if(isEnable()){
			Map<String, String> allCOIFailedRequest = getTruRadialOMTools().getAllCOIOrCOUFailedRequest(getDurationToSendRequestAgain(),getNumberOfTimeToSendRequest(),true);
			if(allCOIFailedRequest != null && !allCOIFailedRequest.isEmpty()){
				for (Map.Entry<String, String> entry : allCOIFailedRequest.entrySet()) {
					getSendFulfillmentMessage().redeliverOrderMessages(entry.getKey(), entry.getValue());
				}
			}
			//Order update re-delivery
			Map<String, String> allCUFailedRequest = getTruRadialOMTools().getAllCOIOrCOUFailedRequest(getDurationToSendRequestAgain(),getNumberOfTimeToSendRequest(),false);
			if(allCUFailedRequest != null && !allCUFailedRequest.isEmpty()){
				for (Map.Entry<String, String> entry : allCUFailedRequest.entrySet()) {
					getSendFulfillmentMessage().redeliverOrderMessages(entry.getKey(), entry.getValue());
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: doScheduledTask() method :: Ends ");
		}
	}
	
	/**
	 * This method to re-deliver the failed OMS request with orderIds.
	 * 
	 */
	public void postMessageOrder() {
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: postMessageOrder() method :: STARTS ");
		}
		if (getOrderIds() != null && !getOrderIds().isEmpty()) {
			Map<String, String> updateOrderFailedRequest = getTruRadialOMTools().getUpdateOrderFailedRequest(getOrderIds());
			if(updateOrderFailedRequest != null && !updateOrderFailedRequest.isEmpty()){
				for (Map.Entry<String, String> entry : updateOrderFailedRequest.entrySet()) {
					getSendFulfillmentMessage().redeliverOrderMessages(entry.getKey(), entry.getValue());
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRUOMSCOIRedeliveryScheduler :: postMessageOrder() method :: Ends ");
		}
	}
	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * This method gets sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage value
	 */
	public TRURadialOMSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with pSendFulfillmentMessage value
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(
			TRURadialOMSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}

	/**
	 * This method gets durationToSendRequestAgain value
	 *
	 * @return the durationToSendRequestAgain value
	 */
	public int getDurationToSendRequestAgain() {
		return mDurationToSendRequestAgain;
	}

	/**
	 * This method sets the durationToSendRequestAgain with pDurationToSendRequestAgain value
	 *
	 * @param pDurationToSendRequestAgain the durationToSendRequestAgain to set
	 */
	public void setDurationToSendRequestAgain(int pDurationToSendRequestAgain) {
		mDurationToSendRequestAgain = pDurationToSendRequestAgain;
	}

	/**
	 * This method gets mNumberOfTimeToSendRequest value
	 *
	 * @return the numberOfTimeToSendRequest value
	 */
	public int getNumberOfTimeToSendRequest() {
		return mNumberOfTimeToSendRequest;
	}

	/**
	 * This method sets the numberOfTimeToSendRequest with pNumberOfTimeToSendRequest value
	 *
	 * @param pNumberOfTimeToSendRequest the numberOfTimeToSendRequest to set
	 */
	public void setNumberOfTimeToSendRequest(int pNumberOfTimeToSendRequest) {
		mNumberOfTimeToSendRequest = pNumberOfTimeToSendRequest;
	}
	
	/**
	 * This method gets mTruRadialOMTools value
	 *
	 * @return the mTruRadialOMTools value
	 */
	private TRURadialOMTools getTruRadialOMTools() {
		return mTruRadialOMTools;
	}

	/**
	 * This method sets the mTruRadialOMTools with pTruRadialOMTools value
	 *
	 * @param pTruRadialOMTools the mTruRadialOMTools to set
	 */
	public void setTruRadialOMTools(TRURadialOMTools pTruRadialOMTools) {
		this.mTruRadialOMTools = pTruRadialOMTools;
	}
	
	/**
	 * This method gets mOrderIds value
	 *
	 * @return the mOrderIds value
	 */
	public List<String> getOrderIds() {
		return mOrderIds;
	}

	/**
	 * This method sets the mOrderIds with pOrderIds value
	 *
	 * @param pOrderIds the mOrderIds to set
	 */
	public void setOrderIds(List<String> pOrderIds) {
		this.mOrderIds = pOrderIds;
	}
}
