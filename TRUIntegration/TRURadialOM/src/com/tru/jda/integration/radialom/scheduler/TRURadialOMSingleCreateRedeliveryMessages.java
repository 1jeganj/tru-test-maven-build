package com.tru.jda.integration.radialom.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

import com.tru.commerce.order.processor.TRURadialOMSendFulfillmentMessage;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.radialom.TRURadialOMTools;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is TRURadialOMSingleCreateRedeliveryMessages
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository.
 */
public class TRURadialOMSingleCreateRedeliveryMessages extends GenericService {
	
	/** Holds reference for mEnable. */
	private boolean mEnable;
	
	/**
	 * Holds reference for mOrderIds.
	 */
	private List<String> mCreateMessageOrderIds;
	
	private List<String> mRedeliveryOrderIds;
	
	/** mPropertyManager. */
	private TRUPropertyManager mPropertyManager;
	
	/** Holds reference for mSendFulfillmentMessage. */
	private TRURadialOMSendFulfillmentMessage mSendFulfillmentMessage;

	/** Holds reference for mTruRadialOMTools. */
	private TRURadialOMTools mTruRadialOMTools;
	
	/**
	 *  Overriding OOTB method to re-deliver the failed OMS request.
	 *  
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */

	/**
	 * This method to re-deliver the failed OMS request with orderIds.
	 * 
	 */
	public void postMessageOrder() {
		if(isLoggingDebug()){
			logDebug("TRURadialOMSingleCreateRedeliveryMessages :: postMessageOrder() method :: STARTS ");
		}
		if (isEnable()) {
			List<String> redeliverOrderIdsList= getRedeliveryOrderIds();
			if (redeliverOrderIdsList != null && !redeliverOrderIdsList.isEmpty()) {
				Map<String, String> updateOrderFailedRequest = getTruRadialOMTools().getUpdateOrderFailedRequest(redeliverOrderIdsList);
				if(updateOrderFailedRequest != null && !updateOrderFailedRequest.isEmpty()){
					for (Map.Entry<String, String> entry : updateOrderFailedRequest.entrySet()) {
							getSendFulfillmentMessage().redeliverOrderMessages(entry.getKey(), entry.getValue());
					}
				}
			}
		}
		if(isLoggingDebug()){
			logDebug("TRURadialOMSingleCreateRedeliveryMessages :: postMessageOrder() method :: Ends ");
		}
	}
	
	/**
	 * Perform error message redelivery.
	 */
	public  void performErrorMessageRedelivery(){
		if (isLoggingDebug()) {
			logDebug("TRURadialOMSingleCreateRedeliveryMessages :: performErrorMessageRedelivery() method :: STARTS ");
		}
		String type = null;
		String profileId = null;
		String orderId = null;
		if (isEnable()) {
			List<RepositoryItem>  omsMessageItems = getTruRadialOMTools().getErrorMessageItems(getCreateMessageOrderIds());
			if (omsMessageItems != null &&! omsMessageItems.isEmpty()) {
				for (RepositoryItem errorMessage : omsMessageItems) {
					type = (String) errorMessage.getPropertyValue(getPropertyManager().getTypePropertyName());
					if (!StringUtils.isEmpty(type)) {
						if (type.equalsIgnoreCase(TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE) && getSendFulfillmentMessage() != null) {
							orderId = (String) errorMessage.getPropertyValue(getPropertyManager().getMessagePropertyName());
							String requestXML = (String) errorMessage.getPropertyValue(getPropertyManager().getRequestPropertyName());
							if(requestXML != null ){
								getSendFulfillmentMessage().redeliverOrderMessages(TRUConstants.ORDER_ID_PREPEND+orderId, requestXML);
							}else{
								getSendFulfillmentMessage().sendFailedOrders(orderId);
							}
							getTruRadialOMTools().removeItemFromOMSErrorRepo(errorMessage.getRepositoryId());
						}
						if(isLoggingDebug()){
							vlogDebug("type : {0} and profileId : {1} and orderId : {2}", type,profileId,orderId);
						}
					}
				}
			}else if(getCreateMessageOrderIds() != null){
				for (String orderNumber : getCreateMessageOrderIds()) {
					getSendFulfillmentMessage().sendFailedOrders(orderNumber);
					//getTruRadialOMTools().removeItemFromOMSErrorRepo(errorMessage.getRepositoryId());
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRURadialOMSingleCreateRedeliveryMessages :: performErrorMessageRedelivery() method :: END ");
		}
	
		
	}

	/**
	 * Checks if is enable.
	 *
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * Sets the enable.
	 *
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}



	/**
	 * This method returns the sendFulfillmentMessage value.
	 *
	 * @return the sendFulfillmentMessage
	 */
	public TRURadialOMSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with parameter value pSendFulfillmentMessage.
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(TRURadialOMSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}
	
	/**
	 * This method gets mTruRadialOMTools value.
	 *
	 * @return the mTruRadialOMTools value
	 */
	private TRURadialOMTools getTruRadialOMTools() {
		// TODO Auto-generated method stub
		return mTruRadialOMTools;
	}

	/**
	 * This method sets the mTruRadialOMTools with pTruRadialOMTools value.
	 *
	 * @param pTruRadialOMTools the mTruRadialOMTools to set
	 */
	public void setTruRadialOMTools(TRURadialOMTools pTruRadialOMTools) {
		this.mTruRadialOMTools = pTruRadialOMTools;
	}

	/**
	 * This method is to get createMessageOrderIds
	 *
	 * @return the createMessageOrderIds
	 */
	public List<String> getCreateMessageOrderIds() {
		if(mCreateMessageOrderIds==null){
			mCreateMessageOrderIds = new ArrayList<String>();
		}
		return mCreateMessageOrderIds;
	}

	/**
	 * This method sets createMessageOrderIds with pCreateMessageOrderIds
	 *
	 * @param pCreateMessageOrderIds the createMessageOrderIds to set
	 */
	public void setCreateMessageOrderIds(List<String> pCreateMessageOrderIds) {
		mCreateMessageOrderIds = pCreateMessageOrderIds;
	}

	/**
	 * This method is to get redeliveryOrderIds
	 *
	 * @return the redeliveryOrderIds
	 */
	public List<String> getRedeliveryOrderIds() {
		if(mRedeliveryOrderIds==null){
			mRedeliveryOrderIds = new ArrayList<String>();
		}
		return mRedeliveryOrderIds;
	}

	/**
	 * This method sets redeliveryOrderIds with pRedeliveryOrderIds
	 *
	 * @param pRedeliveryOrderIds the redeliveryOrderIds to set
	 */
	public void setRedeliveryOrderIds(List<String> pRedeliveryOrderIds) {
		mRedeliveryOrderIds = pRedeliveryOrderIds;
	}
	

	
}
