package com.tru.jda.integration.radialom.scheduler;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.commerce.order.processor.TRURadialOMSendFulfillmentMessage;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.jda.integration.radialom.TRURadialOMTools;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is overriding OOTB SingletonSchedulableService abstract class.
 * This class is used to get the all the failed OMS request, re-deliver them and delete the entry
 * from OMS Error Repository.
 */
public class TRURadialOMCreateMessageScheduler extends SingletonSchedulableService{
	
	/**
	 * Holds reference for mEnable 
	 */
	private boolean mEnable;
	
	/**
	 * mPropertyManager
	 */
	private TRUPropertyManager mPropertyManager;
	
	/**
	 * Holds reference for mDurationToSendRequestAgain 
	 */
	private int mDurationToSendRequestAgain;
	
	/**
	 * Holds reference for mSendFulfillmentMessage 
	 */
	private TRURadialOMSendFulfillmentMessage mSendFulfillmentMessage;

	/**
	 * Holds reference for mTruRadialOMTools 
	 */
	private TRURadialOMTools mTruRadialOMTools;
	
	/**
	 *  Overriding OOTB method to re-deliver the failed OMS request.
	 *  
	 * @param pParamScheduler - Scheduler Object
	 * @param pParamScheduledJob - ScheduledJob Object
	 * 
	 */
	@Override
	public void doScheduledTask(Scheduler pParamScheduler,
			ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("TRURadialOMCreateMessageScheduler :: doScheduledTask() method :: STARTS ");
		}
		String type = null;
		String profileId = null;
		String orderId = null;
		if (isEnable()) {
			RepositoryItem[] omsMessageItems = getTruRadialOMTools().getOMSMessageItems(getDurationToSendRequestAgain());
			if (omsMessageItems != null && omsMessageItems.length > TRUConstants.INT_ZERO) {
				for (RepositoryItem errorMessage : omsMessageItems) {
					type = (String) errorMessage.getPropertyValue(getPropertyManager().getTypePropertyName());
						if (type.equalsIgnoreCase(TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE) && getSendFulfillmentMessage() != null && !StringUtils.isEmpty(type)) {
							int requestSendCount = TRURadialOMConstants.INT_ZERO;
							orderId = (String) errorMessage.getPropertyValue(getPropertyManager().getMessagePropertyName());
							String requestXML = (String) errorMessage.getPropertyValue(getPropertyManager().getRequestPropertyName());
							Integer requestSendCountValue = (Integer) errorMessage.getPropertyValue(getTruRadialOMTools().getOmsConfiguration().getRequestSendCountPropertyName());
							if(requestSendCountValue != null) {
								 requestSendCount= requestSendCountValue;
							}
							if (requestSendCount > TRURadialOMConstants.INT_THREE) {
								continue;
							} 
							requestSendCount++;
							MutableRepositoryItem errorMessageItem = (MutableRepositoryItem) errorMessage;
							errorMessageItem.setPropertyValue(getTruRadialOMTools().getOmsConfiguration().getRequestSendCountPropertyName(), requestSendCount);
							try {
								getTruRadialOMTools().getOmsErrorRepository().updateItem(errorMessageItem);
							} catch (RepositoryException exc) {
								if (isLoggingError()) {
									logError("RepositoryException in TRURadialOMCreateMessageScheduler.doScheduledTask()", exc);
								}
							}
							if(requestXML != null ){
								getSendFulfillmentMessage().redeliverOrderMessages(TRUConstants.ORDER_ID_PREPEND+orderId, requestXML);
							}else{
								getSendFulfillmentMessage().sendFailedOrders(orderId);
							}
							getTruRadialOMTools().removeItemFromOMSErrorRepo(errorMessage.getRepositoryId());
						}
						if(isLoggingDebug()){
							vlogDebug("type : {0} and profileId : {1} and orderId : {2}", type,profileId,orderId);
						}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRURadialOMCreateMessageScheduler :: doScheduledTask() method :: END ");
		}
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 * This method returns the propertyManager value
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the mDurationToSendRequestAgain
	 */
	public int getDurationToSendRequestAgain() {
		return mDurationToSendRequestAgain;
	}

	/**
	 * @param pDurationToSendRequestAgain the mDurationToSendRequestAgain to set
	 */
	public void setDurationToSendRequestAgain(int pDurationToSendRequestAgain) {
		this.mDurationToSendRequestAgain = pDurationToSendRequestAgain;
	}

	/**
	 * This method returns the sendFulfillmentMessage value
	 *
	 * @return the sendFulfillmentMessage
	 */
	public TRURadialOMSendFulfillmentMessage getSendFulfillmentMessage() {
		return mSendFulfillmentMessage;
	}

	/**
	 * This method sets the sendFulfillmentMessage with parameter value pSendFulfillmentMessage
	 *
	 * @param pSendFulfillmentMessage the sendFulfillmentMessage to set
	 */
	public void setSendFulfillmentMessage(TRURadialOMSendFulfillmentMessage pSendFulfillmentMessage) {
		mSendFulfillmentMessage = pSendFulfillmentMessage;
	}
	
	/**
	 * This method gets mTruRadialOMTools value
	 *
	 * @return the mTruRadialOMTools value
	 */
	private TRURadialOMTools getTruRadialOMTools() {
		// TODO Auto-generated method stub
		return mTruRadialOMTools;
	}

	/**
	 * This method sets the mTruRadialOMTools with pTruRadialOMTools value
	 *
	 * @param pTruRadialOMTools the mTruRadialOMTools to set
	 */
	public void setTruRadialOMTools(TRURadialOMTools pTruRadialOMTools) {
		this.mTruRadialOMTools = pTruRadialOMTools;
	}
	
}
