package com.tru.jda.integration.radialom;


import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.TransactionManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

import com.tru.commerce.profile.TRUProfileTools;
import com.tru.common.TRUConstants;
import com.tru.jda.integration.configuration.TRURadialOMConfiguration;
import com.tru.jda.integration.configuration.TRURadialOMConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class is overriding OOTB GenericService class.
 * 
 * This class is used to update the OMS error repository. 
 *  @author Professional Access
 *  @version 1.0
 */
public class TRURadialOMTools extends GenericService{
	
	/** Holds reference for mOmsErrorRepository. */
	private GSARepository mOmsErrorRepository;
	
	/** Holds reference for mOmsConfiguration. */
	private TRURadialOMConfiguration mOmsConfiguration;
	
	/** Holds reference for mTransactionManager. */
	private TransactionManager mTransactionManager;
	
	/** mPropertyManager. */
	private TRUPropertyManager mPropertyManager;
	
	/** mProfileTools. */
	private TRUProfileTools mProfileTools;
	
	/**
	 * Holds reference for MessageContentRepository. 
	 */
	private GSARepository mAuditRepository;
	
	private GSARepository mOrderRepository;
	
	/**
	 * Method to get the all requests for COI OMS service. Key is a repository item id and value is Request.
	 *
	 * @param pDuration the duration
	 * @param pNoOfReqToSend the no of req to send
	 * @param pIsCOIRequest the is coi request
	 * @return the all coi or cou failed request
	 */
	public Map<String, String> getAllCOIOrCOUFailedRequest(int pDuration, int pNoOfReqToSend,boolean pIsCOIRequest) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getAllCOIFailedRequest]");
		}
		RepositoryItem[] omsTransItems = null;
		RepositoryView transItemView = null;
		Map<String, String> coiOrCouRequestMap = null;
		QueryExpression serviceType = null;
		int i = TRURadialOMConstants.ONE;
		try {
			TRURadialOMConfiguration radialOMConfiguration = getOmsConfiguration();
			transItemView =getAuditRepository().getView(radialOMConfiguration.getOmsTransRecordsItemDescriptorName());
			if(transItemView != null){
				QueryBuilder queryBuilder = transItemView.getQueryBuilder();
				if (pIsCOIRequest) {
					 serviceType = queryBuilder.createConstantQueryExpression(TRURadialOMConstants.CUTOMER_ORDER_IMPORT_SERVICE);
				} else {
					 serviceType = queryBuilder.createConstantQueryExpression(TRURadialOMConstants.ORDER_UPDATE_SERVICE);
				}
				QueryExpression serviceTypeProperty = queryBuilder.createPropertyQueryExpression(radialOMConfiguration.getTypePopertyName());
				Query createServiceTypeQuery = queryBuilder.createComparisonQuery(serviceType, serviceTypeProperty, QueryBuilder.EQUALS);
				
				QueryExpression typeQueryExp = queryBuilder.createConstantQueryExpression(TRURadialOMConstants.ERROR);
				QueryExpression typeConstantExp = queryBuilder.createPropertyQueryExpression(radialOMConfiguration.getStatusPopertyName());
				Query createErrorQuery = queryBuilder.createComparisonQuery(typeQueryExp, typeConstantExp, QueryBuilder.EQUALS);
				
				QueryExpression transactionTimePropertyExp = queryBuilder.createPropertyQueryExpression(radialOMConfiguration.getTransactionTimePopertyName());
				//Get Old date
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, - pDuration);
				Date oldDate = cal.getTime();
				QueryExpression oldDateConstantExp = queryBuilder.createConstantQueryExpression(oldDate);
				Query transactionTimeQuery = queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.GREATER_THAN);
				
				Query[] finalQuery = {createServiceTypeQuery,createErrorQuery,transactionTimeQuery};
				Query createAndQuery = queryBuilder.createAndQuery(finalQuery);
				omsTransItems = transItemView.executeQuery(createAndQuery);
				String coiOrCouRequest = null;
				if(omsTransItems != null && omsTransItems.length > TRURadialOMConstants.INT_ZERO){
					for(RepositoryItem omsTansItem : omsTransItems){
						MutableRepositoryItem omsTansactionItem = (MutableRepositoryItem) omsTansItem;
						int requestSendCount = 0;
						Integer requestSendCountInt = (Integer) omsTansactionItem.getPropertyValue(radialOMConfiguration.getRequestSendCountPropertyName());
						if (requestSendCountInt != null) {
							requestSendCount=requestSendCountInt.intValue();
						}
						if (requestSendCount >= TRURadialOMConstants.ONE ) {
							requestSendCount++;
							omsTansactionItem.setPropertyValue(radialOMConfiguration.getRequestSendCountPropertyName(), requestSendCount);
						} else {
							omsTansactionItem.setPropertyValue(radialOMConfiguration.getRequestSendCountPropertyName(), i);
						}
						if(isLoggingDebug()){
							vlogDebug("Customer order impot XML item to resend : {0}", omsTansactionItem);
						}
						coiOrCouRequest = (String) omsTansactionItem.getPropertyValue(radialOMConfiguration.getRequestPopertyName());
						if(StringUtils.isBlank(coiOrCouRequest)){
							continue;
						}
						if(coiOrCouRequestMap == null){
							coiOrCouRequestMap = new HashMap<String, String>();
						}
						if ((int)omsTansactionItem.getPropertyValue(radialOMConfiguration.getRequestSendCountPropertyName()) <= pNoOfReqToSend) {
							coiOrCouRequestMap.put((String) omsTansactionItem.getPropertyValue(radialOMConfiguration.getTransactionIdPopertyName()), coiOrCouRequest);
						}
					}
				}
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRURadialOMTools.getAllCOIFailedRequest", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRURadialOMTools  method: getAllCOIFailedRequest]");
		}
		return coiOrCouRequestMap;
	}
	
	
	/**
	 * Method to get the repositoryItem based on orderId for Update Order OMS service.
	 * @param pOrderIds -  orderId
	 * @return RepositoryItem - RepositoryItem[] Object
	 */
	public Map<String, String> getUpdateOrderFailedRequest(List<String> pOrderIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getAllUpdateOrderFailedRequest]");
		}
		RepositoryItem[] omsTransItems = null;
		RepositoryView transItemView = null;
		MutableRepositoryItem omsTransItem = null;
		Map<String, String> coiOrCouRequestMap = null;	
		TRURadialOMConfiguration radialOMConfiguration = getOmsConfiguration();
		try {
			TRURadialOMConfiguration configuration = getOmsConfiguration();
			transItemView = getAuditRepository().getView(configuration.getOmsTransRecordsItemDescriptorName());
			if(transItemView != null && pOrderIds != null && !pOrderIds.isEmpty()){
				for (String orderId : pOrderIds) {
					String transactionId1 = TRUConstants.ORDER_ID_PREPEND + orderId;
					QueryBuilder queryBuilder = transItemView.getQueryBuilder();
					QueryExpression transactionId = queryBuilder.createConstantQueryExpression(transactionId1);
					QueryExpression transactionIdProperty = queryBuilder.createPropertyQueryExpression(configuration.getTransactionIdPopertyName());
					Query transactionIdQuery = queryBuilder.createComparisonQuery(transactionId, transactionIdProperty, QueryBuilder.EQUALS);
					omsTransItems = transItemView.executeQuery(transactionIdQuery);
					if(omsTransItems != null && omsTransItems.length > TRURadialOMConstants.INT_ZERO){
						 omsTransItem = (MutableRepositoryItem)omsTransItems[0];
						 if(coiOrCouRequestMap == null){
								coiOrCouRequestMap = new HashMap<String, String>();
							}
								coiOrCouRequestMap.put((String) omsTransItem.getPropertyValue(radialOMConfiguration.getTransactionIdPopertyName()), (String) omsTransItem.getPropertyValue(radialOMConfiguration.getRequestPopertyName()));
					}
				}
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getUpdateOrderFailedRequest", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRURadialOMTools  method: getUpdateOrderFailedRequest]");
		}
		return coiOrCouRequestMap;
	}
	
	
	
	
	/**
	 * Method to get the repositoryItem based on orderId for Update Order OMS service.
	 * @param pOrderIds -  orderId
	 * @return RepositoryItem - RepositoryItem[] Object
	 */
	public List<RepositoryItem>  getErrorMessageItems(List<String> pOrderIds) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getErrorMessageItems]");
		}
		RepositoryItem[] omsErrorMessageItems = null;
		List<RepositoryItem> errorMessageItems=null;
		RepositoryItem errorMessageItem=null;
		MutableRepository errorRepository = getOmsErrorRepository();
		try {
			RepositoryView errorMessageView = errorRepository.getView(getPropertyManager().getOmsErrorMessageItemName());
			if(errorMessageView != null && pOrderIds != null && !pOrderIds.isEmpty()){
				for (String orderId : pOrderIds) {
					QueryBuilder queryBuilder = errorMessageView.getQueryBuilder();
					QueryExpression errorOrderId = queryBuilder.createConstantQueryExpression(orderId);
					QueryExpression errorMessagePropertyName = queryBuilder.createPropertyQueryExpression(getPropertyManager().getMessagePropertyName());
					Query errorRepositoryQuery = queryBuilder.createComparisonQuery(errorOrderId, errorMessagePropertyName, QueryBuilder.EQUALS);
					omsErrorMessageItems = errorMessageView.executeQuery(errorRepositoryQuery);
					if(omsErrorMessageItems != null && omsErrorMessageItems.length > TRURadialOMConstants.INT_ZERO){
						errorMessageItem = (MutableRepositoryItem)omsErrorMessageItems[0];
						 if(errorMessageItems == null){
							 errorMessageItems = new ArrayList<RepositoryItem>();
							}
						 errorMessageItems.add(errorMessageItem);
					}
				}
			}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getErrorMessageItems", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRURadialOMTools  method: getUpdateOrderFailedRequest]");
		}
		return errorMessageItems;
	}
	
	
	
	
	/**
	 * Gets the OMS message items.
	 * @param pDurationToSendRequestAgain  - duration time
	 *
	 * @return errorMessageItems - Repository items
	 */
	public RepositoryItem[] getOMSMessageItems(int pDurationToSendRequestAgain) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getOMSMessageItems]");
		}
		TRURadialOMConfiguration configuration = getOmsConfiguration();
		MutableRepository errorRepository = getOmsErrorRepository();
		RepositoryItem[] errorMessageItems= null;
		try {
			RepositoryView errorMessageView = errorRepository.getView(getPropertyManager().getOmsErrorMessageItemName());
			QueryBuilder queryBuilder = errorMessageView.getQueryBuilder();
			QueryExpression transactionTimePropertyExp = queryBuilder.createPropertyQueryExpression(configuration.getTransactionTimePopertyName());
			//Get Old date
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, - pDurationToSendRequestAgain);
			Date oldDate = cal.getTime();
			QueryExpression oldDateConstantExp = queryBuilder.createConstantQueryExpression(oldDate);
			Query transactionTimeQuery = queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.GREATER_THAN);
			errorMessageItems = errorMessageView.executeQuery(transactionTimeQuery);
			if(isLoggingError() && errorMessageItems != null){
				vlogError("errorMessageItems length : {0} ", errorMessageItems.length);
			}
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: getOMSMessageItems]");
		}
		return errorMessageItems;
	}
		
	/**
	 * Removes the item from oms error repository.
	 *
	 * @param pRepositoryId the repository id
	 */
	public void removeItemFromOMSErrorRepo(String pRepositoryId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: removeItemFromOMSErrorRepo]");
			vlogDebug("pReositoryId : {0}",pRepositoryId);
		}
		if(StringUtils.isBlank(pRepositoryId)){
			return;
		}
		TransactionManager transactionManager  =  getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean isRoolBack = Boolean.FALSE;
		try {
			td.begin(transactionManager, TransactionDemarcation.REQUIRED);
			getOmsErrorRepository().removeItem(pRepositoryId, getPropertyManager().getOmsErrorMessageItemName());
			getOmsErrorRepository().invalidateCaches();
		} catch (RepositoryException exc) {
			isRoolBack = Boolean.TRUE;
			if (isLoggingError()) {
				logError("RepositoryException in TRURadialOMTools.removeItemFromOMSErrorRepo", exc);
			}
		} catch (TransactionDemarcationException exc) {
			isRoolBack = Boolean.TRUE;
			if (isLoggingError()) {
				logError("TransactionDemarcationException in TRURadialOMTools.removeItemFromOMSErrorRepo", exc);
			}
		}finally{
			try {
				td.end(isRoolBack);
			} catch (TransactionDemarcationException exc) {
				if (isLoggingError()) {
					logError("TransactionDemarcationException in TRURadialOMTools.removeItemFromOMSErrorRepo", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRURadialOMTools  method: removeItemFromOMSErrorRepo]");
		}
	}

	/**
	 * Gets the transaction manager.
	 *
	 * @return the mTransactionManager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * Sets the transaction manager.
	 *
	 * @param pTransactionManager the mTransactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}

	/**
	 * Gets the profile tools.
	 *
	 * @return the profileTools
	 */
	public TRUProfileTools getProfileTools() {
		return mProfileTools;
	}

	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools the profileTools to set
	 */
	public void setProfileTools(TRUProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	
	/**
	 * Gets the oms error repository.
	 *
	 * @return the omsErrorRepository
	 */
	public GSARepository getOmsErrorRepository() {
		return mOmsErrorRepository;
	}

	/**
	 * Sets the oms error repository.
	 *
	 * @param pOmsErrorRepository the omsErrorRepository to set
	 */
	public void setOmsErrorRepository(GSARepository pOmsErrorRepository) {
		mOmsErrorRepository = pOmsErrorRepository;
	}

	/**
	 * Gets the oms configuration.
	 *
	 * @return the omsConfiguration
	 */
	public TRURadialOMConfiguration getOmsConfiguration() {
		return mOmsConfiguration;
	}
	
	/**
	 * Sets the oms configuration.
	 *
	 * @param pOmsConfiguration the omsConfiguration to set
	 */
	public void setOmsConfiguration(TRURadialOMConfiguration pOmsConfiguration) {
		mOmsConfiguration = pOmsConfiguration;
	}
	
	/**
	 * @return the auditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}

	/**
	 * @param pAuditRepository the auditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		mAuditRepository = pAuditRepository;
	}
	
	/**
	 * This method will get the oms audit messages of old configured in days and
	 * deletes the same
	 * 
	 * 
	 * @param pDurationToDeleteMessagesInDays
	 *            - Duration in days
	 * @throws RepositoryException - if any
	 */
	public void purgeOmsAuditMessages(int pDurationToDeleteMessagesInDays) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
			vlogDebug("pDurationToDeleteMessagesInDays : {0}",pDurationToDeleteMessagesInDays);
		}
		MutableRepository auditRepository = getAuditRepository();
		TRURadialOMConfiguration configuration = getOmsConfiguration();
		RepositoryItem[] auditMessages = getAuditMessages(pDurationToDeleteMessagesInDays);
		if(auditMessages != null && auditMessages.length > TRURadialOMConstants.INT_ZERO){
			for(RepositoryItem auditMessage : auditMessages){
				auditRepository.removeItem(auditMessage.getRepositoryId(), configuration.getOmsTransRecordsItemDescriptorName());
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
		}
	}
	
	/**
	 * This method returns old audit items based on the configured days.
	 * 
	 * @param pDurationToDeleteMessagesInDays
	 *            - Duration in days
	 * @return auditMessageItems - Repository items
	 */
	private RepositoryItem[] getAuditMessages(int pDurationToDeleteMessagesInDays) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getAuditMessages]");
			vlogDebug("pDurationToDeleteMessagesInDays : {0}", pDurationToDeleteMessagesInDays);
		}
		TRURadialOMConfiguration configuration = getOmsConfiguration();
		MutableRepository auditRepository = getAuditRepository();
		RepositoryItem[] auditMessageItems= null;
		try {
			RepositoryView autMessageView = auditRepository.getView(configuration.getOmsTransRecordsItemDescriptorName());
			QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
			//Get Old date
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, - pDurationToDeleteMessagesInDays);
			Date oldDate = cal.getTime();
			QueryExpression transactionTimePropertyExp=queryBuilder.createPropertyQueryExpression(configuration.getTransactionTimePopertyName());
            QueryExpression oldDateConstantExp=queryBuilder.createConstantQueryExpression(oldDate);
            
            Query transactionTimeQuery=queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.LESS_THAN);
            
            auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError("RepositoryException : While view audit message items with exception : {0}", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: purgeOmsAuditMessages]");
		}
		return auditMessageItems;
	}
	
	/**
	 * this method will create the transaction details audit item.
	 * @param pTransactionId - String
	 * @param pRequest - String
	 * @param pResponse - String
	 * @param pType - String
	 * @param pStatus - String
	 * @param pTransactionTime - Date Object
	 */
	public void createOMSAuditItem(String pTransactionId, String pRequest, String pResponse, String pType, String pStatus, Date pTransactionTime){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: createOMSAuditItem]");
		}
		TRURadialOMConfiguration omsConfiguration = getOmsConfiguration();
		MutableRepository auditRepository = getAuditRepository();
		try {
			Date time = Calendar.getInstance().getTime();
			MutableRepositoryItem transDetailsItem = auditRepository.createItem(omsConfiguration.getOmsTransRecordsItemDescriptorName());
			transDetailsItem.setPropertyValue(omsConfiguration.getTransactionIdPopertyName(), pTransactionId);
			transDetailsItem.setPropertyValue(omsConfiguration.getRequestPopertyName(), pRequest);
			transDetailsItem.setPropertyValue(omsConfiguration.getResponsePopertyName(), pResponse);
			transDetailsItem.setPropertyValue(omsConfiguration.getTypePopertyName(), pType);
			transDetailsItem.setPropertyValue(omsConfiguration.getStatusPopertyName(), pStatus);
			transDetailsItem.setPropertyValue(omsConfiguration.getTransactionTimePopertyName(), time);
			auditRepository.addItem(transDetailsItem);
		} catch (RepositoryException exc) {
			if(isLoggingError()){
				vlogError(exc, "RepositoryException in method createOMSAuditItem");
			}
		}
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: createOMSAuditItem]");
		}
	}
	
	/**
	 * This method is to update the audit message item based on the queue response.
	 * 
	 * @param pCorrelationID - correlationID
	 * @param pXmlResponseMessage - xmlResponseMessage
	 */
	public void updateAuditMessageItem(String pCorrelationID, Serializable pXmlResponseMessage){
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: updateAuditMessageItem]");
			vlogDebug("pCorrelationID : {0} pXmlResponseMessage : {1}", pCorrelationID, pXmlResponseMessage);
		}
		if(pCorrelationID == null || pXmlResponseMessage == null){
			if (isLoggingDebug()) {
				logDebug("Correlation Id or reponse message is null so no item is updatedd.");
			}
			return;
		}
		RepositoryItem[] auditMessageItems= null;
		MutableRepositoryItem auditItem = null;
		String responseStatus = parseResponseXML(pXmlResponseMessage.toString());
		MutableRepository auditRepository = getAuditRepository();
		TRURadialOMConfiguration configuration = getOmsConfiguration();
		try{
			RepositoryView autMessageView = auditRepository.getView(configuration.getOmsTransRecordsItemDescriptorName());
			QueryBuilder queryBuilder = autMessageView.getQueryBuilder();
			QueryExpression transactionTimePropertyExp=queryBuilder.createPropertyQueryExpression(configuration.getTransactionIdPopertyName());
			QueryExpression oldDateConstantExp=queryBuilder.createConstantQueryExpression(pCorrelationID);
			Query transactionTimeQuery=queryBuilder.createComparisonQuery(transactionTimePropertyExp, oldDateConstantExp, QueryBuilder.EQUALS);
			auditMessageItems=autMessageView.executeQuery(transactionTimeQuery);
			if(auditMessageItems != null && auditMessageItems.length > TRUConstants.INT_ZERO){
				auditItem = (MutableRepositoryItem) auditMessageItems[0];
			}
			if(isLoggingDebug()){
				vlogDebug("Audit message Item with trans Id : {0} is : {1}", pCorrelationID, auditItem);
			}
			if(auditItem != null && pXmlResponseMessage != null){
				auditItem.setPropertyValue(configuration.getResponsePopertyName(), pXmlResponseMessage.toString());
				auditItem.setPropertyValue(configuration.getStatusPopertyName(), responseStatus);
				auditRepository.updateItem(auditItem);
			}
		}catch(RepositoryException repExc){
			if(isLoggingError()){
				vlogError("RepositoryException : While getting audit message item with trans id : {0} and exception is : {1}", pCorrelationID, repExc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: updateAuditMessageItem]");
		}
	}
	
	/**
	 * This method is to parse the order create response and return the status.
	 *
	 * @param pResponse - Response
	 * @return responseStatus - success/error
	 */
	private String parseResponseXML(String pResponse) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: parseResponseXML]");
		}
		String responseStatus = null;
		if(pResponse != null){
			try {
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(pResponse));
				Document doc = db.parse(is);
				NodeList nodes = doc.getElementsByTagName(TRURadialOMConstants.ORDER_CREATE_RESPONSE);
				
				int nodesLen = nodes.getLength();
				if(nodesLen ==  0){
					responseStatus = TRURadialOMConstants.ERROR;
				}
				for (int i = 0; i < nodesLen; i++) {
					Element element = (Element) nodes.item(i);
					NodeList name = element.getElementsByTagName(TRURadialOMConstants.RESPONSE_STATUS);
					Element line = (Element) name.item(0);
					if(line != null){
						String responseCode = getCharacterDataFromElement(line);
						if(!StringUtils.isEmpty(responseCode)){
							responseStatus = TRURadialOMConstants.SUCCESS;
						}else{
							responseStatus = TRURadialOMConstants.ERROR;
						}
					}
				}
			} catch (SAXException exc) {
				if(isLoggingError()){
					vlogError("SAXException : while parsing xml and with exception : {0}", exc); 
				}
			} catch (ParserConfigurationException exc) {
				if(isLoggingError()){
					vlogError("ParserConfigurationException : while getting document instance with exception : {0}", exc); 
				}
			} catch (IOException exc) {
				if(isLoggingError()){
					vlogError("IOException : while parsing input stream and  with exception : {0}", exc); 
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: parseResponseXML]");
		}
		return responseStatus;
	}
	
	/**
	 * This method returns the response code.
	 * @param pEle - Element
	 * @return responseCode - reponseCode
	 */
	public String getCharacterDataFromElement(Element pEle) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getCharacterDataFromElement]");
		}
		Node child = pEle.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMTools  method: getCharacterDataFromElement]");
		}
		return TRURadialOMConstants.EMPTY_STRING;
	}
	
	/**
	 * Update oms error item.
	 * @param pOrderId - order id
	 * @param pServiceName  - Service name
	 * @param pMessage  - Exception message
	 * @param pCustomerOrderXML 
	 */
	public void updateOmsErrorItemForOrder(String pOrderId, String pServiceName, Exception pMessage, String pCustomerOrderXML) {
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRURadialOMTools method : updateOmsErrorItemForOrder");
			vlogDebug("pOrderId : {0} pServiceName : {1} pMessage : {3}", pOrderId, pServiceName,pMessage);
		}
		MutableRepositoryItem omsErrorItem = null;
		MutableRepository omsRepository = getOmsErrorRepository();
		if (omsRepository != null) {
			try {
					omsErrorItem = omsRepository.createItem(getPropertyManager().getOmsErrorMessageItemName());
					omsErrorItem.setPropertyValue(getPropertyManager().getTypePropertyName(), pServiceName);
					omsErrorItem.setPropertyValue(getPropertyManager().getMessagePropertyName(), pOrderId);
					if(!StringUtils.isBlank(pCustomerOrderXML) && !StringUtils.isEmpty(pCustomerOrderXML)){
						omsErrorItem.setPropertyValue(getOmsConfiguration().getRequestPopertyName(), pCustomerOrderXML);
					}
					Calendar cal = Calendar.getInstance();
					omsErrorItem.setPropertyValue(getPropertyManager().getTransactionTimePropertyName(), cal.getTime());
					if(pMessage != null && pMessage .fillInStackTrace()  != null){
						omsErrorItem.setPropertyValue(getPropertyManager().getOmsErrorMessagePropertyName(), pMessage.fillInStackTrace().toString());
					}
					omsRepository.addItem(omsErrorItem);
					if(isLoggingDebug()){
						vlogDebug("Added Error Item : {0} to OM Error repository", omsErrorItem);
					}
			} catch (RepositoryException re) {
				if(isLoggingError()){
					vlogError(re, "RepositoryException in method updateOmsErrorItemForOrder");
				}
			} 
		}
		if(isLoggingDebug()){
			logDebug("Enter into Class : TRURadialOMTools method : updateOmsErrorItemForOrder");
		}
	}


	/**
	 * This method is to  get LaywayOrder From ErrorRepoistory.
	 *
	 * @param pOrderId - The OrderId
	 * @return request  - The Request.
	 */
	public String getLaywayOrderFromErrorRepoistory(String pOrderId) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMTools  method: getLaywayOrderFromErrorRepoistory]");
		}
		RepositoryItem[] omsErrorMessageItems = null;
		String request=null;
		RepositoryItem errorMessageItem=null;
		MutableRepository errorRepository = getOmsErrorRepository();
		MutableRepository orderRepository = getOrderRepository();
		try {
			RepositoryView errorMessageView = errorRepository.getView(getPropertyManager().getOmsErrorMessageItemName());
			if(orderRepository!=null){
				RepositoryItem layawayOrderItem=(RepositoryItem)orderRepository.getItem(pOrderId,getPropertyManager().getLayawayItemDescriptorPropertyName());
			if(errorMessageView != null && layawayOrderItem!=null ){
					QueryBuilder queryBuilder = errorMessageView.getQueryBuilder();
					QueryExpression errorOrderId = queryBuilder.createConstantQueryExpression(pOrderId);
					QueryExpression errorMessagePropertyName = queryBuilder.createPropertyQueryExpression(getPropertyManager().getMessagePropertyName());
					Query errorRepositoryQuery = queryBuilder.createComparisonQuery(errorOrderId, errorMessagePropertyName, QueryBuilder.EQUALS);
					omsErrorMessageItems = errorMessageView.executeQuery(errorRepositoryQuery);
					if(omsErrorMessageItems != null && omsErrorMessageItems.length > TRURadialOMConstants.INT_ZERO){
						errorMessageItem = (MutableRepositoryItem)omsErrorMessageItems[0];
						 request=(String)errorMessageItem.getPropertyValue(getOmsConfiguration().getRequestPopertyName());
					}
				}
		}
		} catch (RepositoryException exc) {
			if (isLoggingError()) {
				logError("RepositoryException in TRUOMSErrorTools.getLaywayOrderFromErrorRepoistory", exc);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from [Class: TRURadialOMTools  method: getUpdateOrderFailedRequest]");
		}
		return request;
	}


	/**
	 * This method is to get orderRepository
	 *
	 * @return the orderRepository
	 */
	public GSARepository getOrderRepository() {
		return mOrderRepository;
	}


	/**
	 * This method sets orderRepository with pOrderRepository
	 *
	 * @param pOrderRepository the orderRepository to set
	 */
	public void setOrderRepository(GSARepository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}
	
}
