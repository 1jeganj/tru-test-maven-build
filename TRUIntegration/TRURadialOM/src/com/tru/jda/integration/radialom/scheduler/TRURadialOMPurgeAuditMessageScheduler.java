package com.tru.jda.integration.radialom.scheduler;

import atg.repository.RepositoryException;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.jda.integration.radialom.TRURadialOMTools;

/**
 * This scheduler is to purge the OMS audit message records after a particular.
 * configured date and time.
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMPurgeAuditMessageScheduler extends
		SingletonSchedulableService {
	
	/**
	 * Holds reference for mEnable 
	 */
	private boolean mEnable;
	/**
	 * Holds reference for mDurationToDeleteMessages 
	 */
	private int mDurationToDeleteMessagesInDays;
	
	/**
	 * Holds reference for mTruRadialOMTools 
	 */
	private TRURadialOMTools mTruRadialOMTools;

	/**
	 * This method will run periodically to delete the old oms audit messages.
	 * 
	 * @param pScheduler
	 *            - Scheduler
	 * @param pJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {
		if (isLoggingDebug()) {
			logDebug("Enter into [Class: TRURadialOMPurgeAuditMessageScheduler  method: doScheduledTask]");
			vlogDebug("Scheduler : {0} ScheduledJob :{1}", pScheduler, pJob);
		}
		if(isEnable()){
			try {
				getTruRadialOMTools().purgeOmsAuditMessages(getDurationToDeleteMessagesInDays());
			} catch (RepositoryException exc) {
				if(isLoggingError()){
					vlogError("RepositoryException : wihile removing audit message and exception is : {0}", exc);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from [Class: TRURadialOMPurgeAuditMessageScheduler  method: doScheduledTask]");
		}
	}
	/**
	 * This method returns the enable value
	 *
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * This method sets the enable with parameter value pEnable
	 *
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}
	/**
	 * This method returns the durationToDeleteMessagesInDays value
	 *
	 * @return the durationToDeleteMessagesInDays
	 */
	public int getDurationToDeleteMessagesInDays() {
		return mDurationToDeleteMessagesInDays;
	}
	/**
	 * This method sets the durationToDeleteMessagesInDays with parameter value pDurationToDeleteMessagesInDays
	 *
	 * @param pDurationToDeleteMessagesInDays the durationToDeleteMessagesInDays to set
	 */
	public void setDurationToDeleteMessagesInDays(
			int pDurationToDeleteMessagesInDays) {
		mDurationToDeleteMessagesInDays = pDurationToDeleteMessagesInDays;
	}
	
	/**
	 * This method gets mTruRadialOMTools value
	 *
	 * @return the mTruRadialOMTools value
	 */
	private TRURadialOMTools getTruRadialOMTools() {
		// TODO Auto-generated method stub
		return mTruRadialOMTools;
	}

	/**
	 * This method sets the mTruRadialOMTools with pTruRadialOMTools value
	 *
	 * @param pTruRadialOMTools the mTruRadialOMTools to set
	 */
	public void setTruRadialOMTools(TRURadialOMTools pTruRadialOMTools) {
		this.mTruRadialOMTools = pTruRadialOMTools;
	}

}
