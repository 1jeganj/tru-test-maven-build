package com.tru.jda.integration.configuration;

import java.util.Map;

/**
 * The Class TRURadialOMConfiguration.
 *
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMConfiguration extends TRUBaseRadialOMConfiguration {
	
	/** This holds order create service URL. */
	private String mOrderCreateServiceURL;
	
	/** This holds the lookup service URL. */
	private String mOrderLookupServiceURL;
	
	/** This holds details look up service URL. */
	private String mOrderDetailsLookupServiceURL;
	
	/**
	 * Holds mOrderDetailsTermPropertyName.
	 */
	private String mOrderDetailsTermPropertyName;
	
	/** This holds cancel service URL. */
	private String mOrderCancelServiceURL;
	
	/** This holds order create xsd package. */
	private String mOrderCreateXSDPackage;
	
	/** This holds order lookup XSD package. */
	private String mOrderLookupXSDpackage;
	
	/** This holds order details lookup XSD package. */
	private String mOrderDetailsLookupXSDPackage;
	
	/** This holds order details lookup XSD package. */
	private String mOrderCancelXSDPath;
	
	/** This holds APIKey string. */
	private String mApiKey;
	
	/** This holds APIKey value. */
	private String mApiKeyValue;
	
	/** This hold the POST. */
	private String mHttpPostMethod;
	
	/** This holds the HTTP Content Type. */
	private String mHttpContentType;
	
	/** This method holds HTTP content type. */
	private String mHttpApplicationType;
	
	/** This holds the Jaxb output format. */
	private String mJaxbOutputFormat;
	
	/**
	 * This holds Order Tax Error Custom Attribute Name.
	 */
	private String mOrderTaxErrorCustomAttributeName;
	
	/** This holds Custom Attribute Value Yes. */
	private String mCustomAttributeValueYes;
	
	/** This holds Custom Attribute Value No. */
	private String mCustomAttributeValueNo;
	
	/** This holds Source Id Type. */
	private String mSourceIdType;
	
	/** This holds Source Id Value. */
	private String mSourceIdValue;
	
	/** This holds the Currency USD. */
	private String mCurrencyUSD;
	
	/** This holds Time Zone EST. */
	private String mTimeZoneEST;
	
	/** This holds the order cancel reason code. */
	private String mOrderCancelReasonCode;
	
	/** This holds order cancel reason. */
	private String mOrderCancelReason;
	
	/** This holds tender type map. */
	private Map<String, String> mTenderTypeMap;
	
	/** This holds the Tender type key for zero dollar order. */
	private String mZeroDollarTenderTypeKey;
	
	/** This holds the Paypal payment account unique id. */
	private String mPaypalPaymentAccountUniqueId;
	
	/** This holds the Address type billing. */
	private String mAddressTypeBilling;
	
	/** This holds the locale. */
	private String mLocale;
	
	/** This holds the source. */
	private String mSource;
	
	/** This holds the addressTypeShipping. */
	private String mAddressTypeShipping;
	
	/** This holds the addressTypeStoreFront. */
	private String mAddressTypeStoreFront;
	
	/** This holds mOrderCustomAttributeGiftwrapAmt. */
	private String mOrderCustomAttributeGiftwrapAmt;
	
	/** This holds mOrderCustomAttributePromoSavingsAmt. */
	private String mOrderCustomAttributePromoSavingsAmt;
	
	/** This holds mOrderCustomAttributeShipAmt. */
	private String mOrderCustomAttributeShipAmt;
	
	/** This holds mOrderCustomAttributeShipSurAmt. */
	private String mOrderCustomAttributeShipSurAmt;
	
	/** This holds mOrderCustomAttributeSalesTaxAmt. */
	private String mOrderCustomAttributeSalesTaxAmt;
	
	/** This holds mOrderCustomAttributeLocalTaxAmt. */
	private String mOrderCustomAttributeLocalTaxAmt;
	
	/** This holds mOrderCustomAttributeIslandTaxAmt. */
	private String mOrderCustomAttributeIslandTaxAmt;
	
	/** This holds mOrderCustomAttributeEwasteAmt. */
	private String mOrderCustomAttributeEwasteAmt;
	
	/** This holds mOrderCustomAttributeMattFeeAmt. */
	private String mOrderCustomAttributeMattFeeAmt;
	
	/** This holds mOrderCustomAttributePifFeeAmt. */
	private String mOrderCustomAttributePifFeeAmt;
	
	/** This holds mOrderCustomAttributeGiftCardAmt. */
	private String mOrderCustomAttributeGiftCardAmt;
	
	/** This holds mOrderCustomAttributeOrderTotal. */
	private String mOrderCustomAttributeOrderTotal;
	
	/** This holds mOrderCustomAttributeSubtotal. */
	private String mOrderCustomAttributeSubtotal;
	
	/** This holds mOrderCustomAttributeBalanceAmt. */
	private String mOrderCustomAttributeBalanceAmt;
	
	/** This holds mOrderCustomAttributeLoyaltyAccount. */
	private String mOrderCustomAttributeLoyaltyAccount;
	
	/** This holds orderCustomAttributeLoyaltyProgram. */
	private String mOrderCustomAttributeLoyaltyProgram;
	
	/** This holds orderCustomAttributeLoyaltyProgramValue. */
	private String mOrderCustomAttributeLoyaltyProgramValue;
	
	/** This holds orderCustomAttributeGFSOrderId. */
	private String mOrderCustomAttributeGFSOrderId;
	
	/** This holds orderCustomAttributeLayaway. */
	private String mOrderCustomAttributeLayaway;
	
	/** This holds orderCustomAttributeLayawayValue. */
	private String mOrderCustomAttributeLayawayValue;
	
	/** This holds orderCustomAttributeLayawayLocId. */
	private String mOrderCustomAttributeLayawayLocId;
	
	/** This holds omsOrderDetailLookupRestAPIURL. */
	private String mOmsOrderDetailLookupRestAPIURL;
	
	/** This holds omsOrderLookupRestAPIURL. */
	private String mOmsOrderLookupRestAPIURL;
	
	/** This holds omsCancelOrderRestAPIURL. */
	private String mOmsCancelOrderRestAPIURL;	
	
	/** This holds orderLookupResponseListProperty. */
	private String mOrderLookupResponseListProperty;
	
	/** This holds orderLookupResponseProperty. */
	private String mOrderLookupResponseProperty;
	
	/** This holds lineDetailsProperty. */
	private String mLineDetailsProperty;
	
	/** This holds lineDetailProperty. */
	private String mLineDetailProperty;
	
	/** This holds shippinAddressProperty. */
	private String mShippingAddressProperty;
	
	/** This holds deliveryModeProperty. */
	private String mDeliveryModeProperty;
	
	/** This holds shippmentInfoProperty. */	
	private String mShippmentInfoProperty;
	
	/** This holds invoiceIdProperty. */	
	private String mInvoiceIdProperty;

	/** This holds displayOrderArrayProperty. */
	private String mDisplayOrderArrayProperty;

	/** This holds invoiceProperty. */
	private String mInvoiceProperty;

	/** This holds orderHeaderProperty. */
	private String mOrderHeaderProperty;
	
	/** This holds invoiceTotalAmountsProperty. */
	private String mInvoiceTotalAmountsProperty;
	
	/** This holds displayObject. */	
	private String mDisplayObject;
	
	/** This holds customizationDataGiftMessageType. */
	private String mCustomizationDataGiftMessageType;
	
	/** This holds customizationDataRegistryMessageType. */
	private String mCustomizationDataRegistryMessageType;
	
	/** This holds customizationDataGiftWrapType. */
	private String mCustomizationDataGiftWrapType;
	
	/** This holds customizationDataBPPType. */
	private String mCustomizationDataBPPType;
	
	/** This holds mOnlinePID. */
	private String mOnlinePID;
	
	/** This holds ImageWidth. */
	private String mImageWidth;
	
	/** This holds ImageHeight. */
	private String mImageHeight;
	
	/** This holds customerLookupResponseProperty. */
	private String mCustomerLookupResponseProperty;

	/** This holds orderListProperty. */
	private String mOrderListProperty;

	/** This holds orderStatusProperty. */
	private String mOrderStatusProperty;

	/** This holds orderProperty. */
	private String mOrderProperty;
	
	/** This holds mOrderDetailStatusesMap map. */
	private Map<String, String> mOrderDetailStatusesMap;
	
	/**
	 * Holds transactionIdPopertyName.
	 */
	private String mTransactionIdPopertyName;

	/**
	 * Holds requestPopertyName.
	 */
	private String mRequestPopertyName;

	/**
	 * Holds responsePopertyName.
	 */
	private String mResponsePopertyName;

	/**
	 * Holds typePopertyName.
	 */
	private String mTypePopertyName;

	/**
	 * Holds statusPopertyName.
	 */
	private String mStatusPopertyName;

	/**
	 * Holds transactionTimePopertyName.
	 */
	private String mTransactionTimePopertyName;

	/**
	 * Holds omsTransRecordsItemDescriptorName.
	 */
	private String mOmsTransRecordsItemDescriptorName;
	
	/**
	 * Holds mRequestSendCountPropertyName.
	 * 
	 */
	private String mRequestSendCountPropertyName;
	
	/** mImageFitRatio. */
	private String mImageFitRatio;
	/**
	 * Holds customerOrderProperty.
	 */
	private String mCustomerOrderProperty;
	/**
	 * Holds orderPaymentListProperty.
	 */
	private String mOrderPaymentListProperty;
	/**
	 * Holds orderPaymentProperty.
	 */
	private String mOrderPaymentProperty;
	/**
	 * Holds customAttributesProperty.
	 */
	private String mCustomAttributesProperty;
	/**
	 * Holds customAttributeProperty.
	 */
	private String mCustomAttributeProperty;
	/**
	 * Holds nameProperty.
	 */
	private String mNameProperty;
	/**
	 * Holds valueProperty.
	 */
	private String mValueProperty;
	/**
	 * Holds orderSummaryProperty.
	 */
	private String mOrderSummaryProperty;
	/**
	 * Holds merchandiseTaxProperty.
	 */
	private String mMerchandiseTaxProperty;
	/**
	 * Holds ispuItemProperty.
	 */
	private String mIspuItemProperty;
	/**
	 * Holds storeFrontAddressProperty.
	 */
	private String mStoreFrontAddressProperty;
	/**
	 * Holds proxyAddressProperty.
	 */
	private String mProxyAddressProperty;
	/**
	 * Holds trackingInfoProperty.
	 */
	private String mTrackingInfoProperty;
	/**
	 * Holds trackingNumberProperty.
	 */
	private String mTrackingNumberProperty;
	/**
	 * Holds trackingUrlProperty.
	 */
	private String mTrackingUrlProperty;
	/**
	 * Holds feeChargeListProperty.
	 */
	private String mFeeChargeListProperty;
	/**
	 * Holds feeChargeProperty.
	 */
	private String mFeeChargeProperty;
	/**
	 * Holds taxDataProperty.
	 */
	private String mTaxDataProperty;
	/**
	 * Holds customizationListProperty.
	 */
	private String mCustomizationListProperty;
	/**
	 * Holds customizationDataProperty.
	 */
	private String mCustomizationDataProperty;
	/**
	 * Holds messageProperty.
	 */
	private String mMessageProperty;
	/**
	 * Holds giftMessageProperty.
	 */
	private String mGiftMessageProperty;
	/**
	 * Holds totalLineItemCountProperty.
	 */
	private String mTotalLineItemCountProperty;
	/**
	 * Holds totalShippmentCountProperty.
	 */
	private String mTotalShippmentCountProperty;
	/**
	 * Holds merchandiseProperty.
	 */
	private String mMerchandiseProperty;
	/**
	 * Holds shippingProperty.
	 */
	private String mShippingProperty;
	/**
	 * Holds dutyProperty.
	 */
	private String mDutyProperty;
	/**
	 * Holds pricingProperty.
	 */
	private String mPricingProperty;
	/**
	 * Holds trackingListProperty.
	 */
	private String mTrackingListProperty;
	
	/**  holds orderCustomAttributeTaxTransactionId. */
	private String mOrderCustomAttributeTaxTransactionId;
	
	/**  holds orderSummaryMap. */
	private Map<String, String> mOrderSummaryMap;

	/**  holds gfsOrderDetailsLookupServicePackage. */
	private String mGfsOrderDetailsLookupServicePackage;

	/**  holds gfsOrderLookupServicePackage. */
	private String mGfsOrderLookupServicePackage;
	 
 	/** This holds the mSoapEnvelopeProperty. */
    private String mSoapEnvelopeProperty;
    
    /** This holds the mSoapBodyProperty. */
    private String mSoapBodyProperty;
    
    /** This holds the mOrderDetailResponseProperty. */
    private String mOrderDetailResponseProperty;
    
    /** This holds the mOrderDetailProperty. */
    private String mOrderDetailProperty;
    
    /** This holds the mBillToPropety. */
    private String mBillToPropety;
    
    /** This holds the mFirstNameProperty. */
    private String mFirstNameProperty;
    
    /** This holds the mLastNameProperty. */
    private String mLastNameProperty;
    
    /** This holds the mPaymentHistoryProperty. */
    private String mPaymentHistoryProperty;
    
    /** This holds the mDateProperty. */
    private String mDateProperty;
    
    /** This holds the mAmountPaidProperty. */
    private String mAmountPaidProperty;
    
    /** This holds the mOrderCreationDateProperty. */
    private String mOrderCreationDateProperty;
    
    /** This holds the mPartnerOrderNumberProperty. */
    private String mPartnerOrderNumberProperty;
    
    /** This holds the mLineItemProperty. */
    private String mLineItemProperty;
    
    /** This holds the mDescriptionProperty. */
    private String mDescriptionProperty;
    
    /** This holds the mQuantityProperty. */
    private String mQuantityProperty;
    
    /** This holds the mLineStatusProperty. */
    private String mLineStatusProperty;
    
    /** This holds the mOrderStatusSoapProperty. */
    private String mOrderStatusSoapProperty;

    /** This holds the mOrderSummaryPartnerOrderNumberProperty. */
    private String mOrderSummaryPartnerOrderNumberProperty;
    
    /** This holds the mOrderSummaryOrderCreationDateProperty. */
    private String mOrderSummaryOrderCreationDateProperty;

    /** This holds the mOrderSummaryOriginalOrderTotalProperty. */
    private String mOrderSummaryOriginalOrderTotalProperty;
    
    /** This holds the mOrderSummaryOrderStatusProperty. */
    
    private String mOrderSummaryOrderStatusProperty;
    
    /**
	 * Holds mOrderDetailsDisplayNamePropertyName.
	 */
	private String mOrderDetailsDisplayNamePropertyName;
	
	/**
	 * Holds mWsdlLocation.
	 */
	private String mWsdlLocation;
	
	/**
	 * Holds mNameSpace.
	 */
	private String mNameSpace;
	
	/**
	 * Holds mServiceName.
	 */
	private String mServiceName;
	
	/** Holds mLayawayDeliveryMethod. */
	private String mLayawayDeliveryMethod;
	
	/**  Holds mLayawayOrderSummaryProperty. */
	private String mLayawayOrderSummaryProperty;
	
	/** Holds mOrderSummaryErrorMsgValueProperty. */
	private String mOrderSummaryErrorMsgValueProperty;
	
	/** This holds the mPaymentProperty. */
    private String mPaymentProperty;
    
    /** This holds the mOriginalOrderTotalProperty. */
    private String mOriginalOrderTotalProperty;
    
    /** This holds the mDownPaymentProperty. */
    private String mDownPaymentProperty;
    
    /** This holds the mAdditionalPaymentProperty. */
    private String mAdditionalPaymentProperty;
    
    /** This holds the mLocationIDProperty. */
    private String mLocationIDProperty;
    
    /** This holds the mHowToGetItProperty. */
    private String mHowToGetItProperty;

    /** The m service down. */
	private String mServiceDown;
	
	/** The m invalid customer id dummy order list json. */
	private String mInvalidCustomerIDDummyOrderListJson;
	
	/** The m order lookup service down dummy json. */
	private String mOrderLookupServiceDownDummyJson;
	
	/** The m invalid order id dummy order details json. */
	private String mInvalidOrderIDDummyOrderDetailsJson;
	
	/** The m order details service down dummy json. */
	private String mOrderDetailsServiceDownDummyJson;
    
	/** The m store code property name response. */
	private String mStoreCodePropertyNameResponse;
	
	/** The m store phone property name response. */
	private String mStorePhonePropertyNameResponse;
	
	/** The m store name property name response. */
	private String mStoreNamePropertyNameResponse;
	
	/** The m is gift wrap property name. */
	private String mIsGiftWrapPropertyName;
	
	/** The m track order ship date property. */
	private String mTrackOrderShipDateProperty;
	
	/** The m tracking exists. */
	private String mTrackingExists;
	
	/** The m only donation exists. */
	private String mOnlyDonationExists;
	
	/** The m order date format property name. */
	private String mOrderDateFormatPropertyName;
	
	/** The m is proxy exists. */
	private String mIsProxyExists;
	
	/** The m billing address property name. */
	private String mBillingAddressPropertyName;
	
	/** The m postal code property name. */
	private String mPostalCodePropertyName;
	
	/** The m order channel attribute name. */
	private String mOrderChannelAttributeName;
	
	/** The m sos agent name property name. */
	private String mSosAgentNamePropertyName;
	
	/** The m sos agent number property name. */
	private String mSosAgentNumberPropertyName;
	
	/** The m sos agent store id property name. */
	private String mSosAgentStoreIdPropertyName;
	
	/** The m sos agent mOrderTotalPropertyName. */
	private String mOrderTotalPropertyName;
	
	/** The T 1 order exists property name. */
	private String mT1OrderExistsPropertyName;
	
	/** The Order total amounts property name. */
	private String mOrderTotalAmountsPropertyName;
	
	/** The Order summary merchandise amount. */
	private String mOrderSummaryMerchandiseAmount;
	
	/** The Order summary grand total. */
	private String mOrderSummaryGrandTotal;
	
	/** The Order summary tax amount. */
	private String mOrderSummaryTaxAmount;
	
	/** The Order summary shipping amount. */
	private String mOrderSummaryShippingAmount;
	

	/** The m item custom attribute item price property name. */
	private String mItemCustomAttributeItemPricePropertyName;
	
	/** The giftWrapTRUSiteName property name. */
	private String mGiftWrapTRUSiteName;

	/** The giftWrapBRUSiteName property name. */
	private String mGiftWrapBRUSiteName;
	
	/** The Partner id property name. */
	private String mPartnerIdPropertyName;
	
	/** The Price  property name. */
	private String mPricePropertyName;
	
	/** The invalidCancelOrderAttemptJson  property name. */
	private String mInvalidCancelOrderAttemptJson;
	
	/** The mInvalidCancelOrderJson  property name. */
	private String mInvalidCancelOrderJson;
	
	/** The m item custom attribute registry name message. */
	private String mItemCustomAttributeRegistryNameMessage;
	
	/** The mServiceError  property name. */
	private String mServiceError;
	
	/** The m orsocode property name. */
	private String mOrsoCodePropertyName;
	
	/** The m orso type property name. */
	private String mOrsoTypePropertyName;
	
	/** The m orso type value property name. */
	private String mOrsoTypeValuePropertyName;
	
	/**
	 * Gets the orso type value property name.
	 *
	 * @return the orso type value property name
	 */
	public String getOrsoTypeValuePropertyName() {
		return mOrsoTypeValuePropertyName;
	}

	/**
	 * Sets the orso type value property name.
	 *
	 * @param pOrsoTypeValuePropertyName the new orso type value property name
	 */
	public void setOrsoTypeValuePropertyName(String pOrsoTypeValuePropertyName) {
		 mOrsoTypeValuePropertyName = pOrsoTypeValuePropertyName;
	}

	/**
	 * Gets the orso type property name.
	 *
	 * @return the orso type property name
	 */
	public String getOrsoTypePropertyName() {
		return mOrsoTypePropertyName;
	}

	/**
	 * Sets the orso type property name.
	 *
	 * @param pOrsoTypePropertyName the new orso type property name
	 */
	public void setOrsoTypePropertyName(String pOrsoTypePropertyName) {
		 mOrsoTypePropertyName = pOrsoTypePropertyName;
	}

	/**
	 * Gets the orsocode property name.
	 *
	 * @return the orsocode property name
	 */
	public String getOrsoCodePropertyName() {
		return mOrsoCodePropertyName;
	}

	/**
	 * Sets the orsocode property name.
	 *
	 * @param pOrsoCodePropertyName the new orso code property name
	 */
	public void setOrsoCodePropertyName(String pOrsoCodePropertyName) {
		mOrsoCodePropertyName = pOrsoCodePropertyName;
	}

	/**
	 * Gets the serviceError.
	 *
	 * @return the mServiceError
	 */
	public String getServiceError() {
		return mServiceError;
	}

	/**
	 * Sets the mServiceError.
	 *
	 * @param pServiceError the Service Error
	 */
	public void setServiceError(String pServiceError) {
		this.mServiceError = pServiceError;
	}

	/**
	 * Gets the item custom attribute registry name message.
	 *
	 * @return the item custom attribute registry name message
	 */
	public String getItemCustomAttributeRegistryNameMessage() {
		return mItemCustomAttributeRegistryNameMessage;
	}

	/**
	 * Sets the item custom attribute registry name message.
	 *
	 * @param pItemCustomAttributeRegistryNameMessage the new item custom attribute registry name message
	 */
	public void setItemCustomAttributeRegistryNameMessage(
			String pItemCustomAttributeRegistryNameMessage) {
		 mItemCustomAttributeRegistryNameMessage = pItemCustomAttributeRegistryNameMessage;
	}

	/**
	 * Gets the item custom attribute item price property name.
	 *
	 * @return the item custom attribute item price property name
	 */
	public String getItemCustomAttributeItemPricePropertyName() {
		return mItemCustomAttributeItemPricePropertyName;
	}

	/**
	 * Sets the item custom attribute item price property name.
	 *
	 * @param pItemCustomAttributeItemPricePropertyName the new item custom attribute item price property name
	 */
	public void setItemCustomAttributeItemPricePropertyName(
			String pItemCustomAttributeItemPricePropertyName) {
		 mItemCustomAttributeItemPricePropertyName = pItemCustomAttributeItemPricePropertyName;
	}

	/**
	 * Gets the order total property name.
	 *
	 * @return the order total property name
	 */
	public String getOrderTotalPropertyName() {
		return mOrderTotalPropertyName;
	}

	/**
	 * Sets the order total property name.
	 *
	 * @param pOrderTotalPropertyName the new order total property name
	 */
	public void setOrderTotalPropertyName(String pOrderTotalPropertyName) {
		 mOrderTotalPropertyName = pOrderTotalPropertyName;
	}

	/**
	 * Gets the sos agent store id property name.
	 *
	 * @return the sos agent store id property name
	 */
	public String getSosAgentStoreIdPropertyName() {
		return mSosAgentStoreIdPropertyName;
	}

	/**
	 * Sets the sos agent store id property name.
	 *
	 * @param pSosAgentStoreIdPropertyName the new sos agent store id property name
	 */
	public void setSosAgentStoreIdPropertyName(String pSosAgentStoreIdPropertyName) {
		mSosAgentStoreIdPropertyName = pSosAgentStoreIdPropertyName;
	}

	/**
	 * Gets the sos agent number property name.
	 *
	 * @return the sos agent number property name
	 */
	public String getSosAgentNumberPropertyName() {
		return mSosAgentNumberPropertyName;
	}

	/**
	 * Sets the sos agent number property name.
	 *
	 * @param pSosAgentNumberPropertyName the new sos agent number property name
	 */
	public void setSosAgentNumberPropertyName(String pSosAgentNumberPropertyName) {
		 mSosAgentNumberPropertyName = pSosAgentNumberPropertyName;
	}

	/**
	 * Gets the sos agent name property name.
	 *
	 * @return the sos agent name property name
	 */
	public String getSosAgentNamePropertyName() {
		return mSosAgentNamePropertyName;
	}

	/**
	 * Sets the sos agent name property name.
	 *
	 * @param pSosAgentNamePropertyName the new sos agent name property name
	 */
	public void setSosAgentNamePropertyName(String pSosAgentNamePropertyName) {
		mSosAgentNamePropertyName = pSosAgentNamePropertyName;
	}

	/**
	 * Gets the order channel attribute name.
	 *
	 * @return the order channel attribute name
	 */
	public String getOrderChannelAttributeName() {
		return mOrderChannelAttributeName;
	}

	/**
	 * Sets the order channel attribute name.
	 *
	 * @param pOrderChannelAttributeName the new order channel attribute name
	 */
	public void setOrderChannelAttributeName(String pOrderChannelAttributeName) {
		 mOrderChannelAttributeName = pOrderChannelAttributeName;
	}

	/**
	 * Gets the postal code property name.
	 *
	 * @return the postal code property name
	 */
	public String getPostalCodePropertyName() {
		return mPostalCodePropertyName;
	}

	/**
	 * Sets the postal code property name.
	 *
	 * @param pPostalCodePropertyName the new postal code property name
	 */
	public void setPostalCodePropertyName(String pPostalCodePropertyName) {
		 mPostalCodePropertyName = pPostalCodePropertyName;
	}

	/**
	 * Gets the billing address property name.
	 *
	 * @return the billing address property name
	 */
	public String getBillingAddressPropertyName() {
		return mBillingAddressPropertyName;
	}

	/**
	 * Sets the billing address property name.
	 *
	 * @param pBillingAddressPropertyName the new billing address property name
	 */
	public void setBillingAddressPropertyName(String pBillingAddressPropertyName) {
		 mBillingAddressPropertyName = pBillingAddressPropertyName;
	}

	/**
	 * Gets the checks if is proxy exists.
	 *
	 * @return the checks if is proxy exists
	 */
	public String getIsProxyExists() {
		return mIsProxyExists;
	}

	/**
	 * Sets the checks if is proxy exists.
	 *
	 * @param pIsProxyExists the new checks if is proxy exists
	 */
	public void setIsProxyExists(String pIsProxyExists) {
		 mIsProxyExists = pIsProxyExists;
	}

	/**
	 * Gets the order date format property name.
	 *
	 * @return the order date format property name
	 */
	public String getOrderDateFormatPropertyName() {
		return mOrderDateFormatPropertyName;
	}

	/**
	 * Sets the order date format property name.
	 *
	 * @param pOrderDateFormatPropertyName the new order date format property name
	 */
	public void setOrderDateFormatPropertyName(String pOrderDateFormatPropertyName) {
		 mOrderDateFormatPropertyName = pOrderDateFormatPropertyName;
	}

	/**
	 * Gets the only donation exists.
	 *
	 * @return the only donation exists
	 */
	public String getOnlyDonationExists() {
		return mOnlyDonationExists;
	}

	/**
	 * Sets the only donation exists.
	 *
	 * @param pOnlyDonationExists the new only donation exists
	 */
	public void setOnlyDonationExists(String pOnlyDonationExists) {
		mOnlyDonationExists = pOnlyDonationExists;
	}
	
	
	/**
	 * Gets the tracking exists.
	 *
	 * @return the tracking exists
	 */
	public String getTrackingExists() {
		return mTrackingExists;
	}

	/**
	 * Sets the tracking exists.
	 *
	 * @param pTrackingExists the new tracking exists
	 */
	public void setTrackingExists(String pTrackingExists) {
		 mTrackingExists = pTrackingExists;
	}

	/**
	 * Gets the track order ship date property.
	 *
	 * @return the track order ship date property
	 */
	public String getTrackOrderShipDateProperty() {
		return mTrackOrderShipDateProperty;
	}

	/**
	 * Sets the track order ship date property.
	 *
	 * @param pTrackOrderShipDateProperty the new track order ship date property
	 */
	public void setTrackOrderShipDateProperty(String pTrackOrderShipDateProperty) {
		 mTrackOrderShipDateProperty = pTrackOrderShipDateProperty;
	}
	
	/**
	 * Gets the checks if is gift wrap property name.
	 *
	 * @return the checks if is gift wrap property name
	 */
	public String getIsGiftWrapPropertyName() {
		return mIsGiftWrapPropertyName;
	}

	/**
	 * Sets the checks if is gift wrap property name.
	 *
	 * @param pIsGiftWrapPropertyName the new checks if is gift wrap property name
	 */
	public void setIsGiftWrapPropertyName(String pIsGiftWrapPropertyName) {
		  mIsGiftWrapPropertyName = pIsGiftWrapPropertyName;
	}
	
	/** The m donation exists property name. */
	private String mDonationExistsPropertyName;
    
    /**
     * Gets the donation exists property name.
     *
     * @return the donation exists property name
     */
    public String getDonationExistsPropertyName() {
		return mDonationExistsPropertyName;
	}

	/**
	 * Sets the donation exists property name.
	 *
	 * @param pDonationExistsPropertyName the new donation exists property name
	 */
	public void setDonationExistsPropertyName(String pDonationExistsPropertyName) {
		 mDonationExistsPropertyName = pDonationExistsPropertyName;
	}

    /**
     * Gets the service down.
     *
     * @return the service down
     */
    public String getServiceDown() {
		return mServiceDown;
	}

	/**
	 * Sets the service down.
	 *
	 * @param pServiceDown the new service down
	 */
	public void setServiceDown(String pServiceDown) {
		this.mServiceDown = pServiceDown;
	}

	/**
	 * Gets the payment property.
	 *
	 * @return mPaymentProperty
	 */
    public String getPaymentProperty() {
        return mPaymentProperty;
    }
    
    /**
     * Sets the payment property.
     *
     * @param pPaymentProperty            the mPaymentProperty to set
     */
    public void setPaymentProperty(String pPaymentProperty) {
        this.mPaymentProperty = pPaymentProperty;
    }

    /**
     * Gets the original order total property.
     *
     * @return the mOriginalOrderTotalProperty
     */
    public String getOriginalOrderTotalProperty() {
        return mOriginalOrderTotalProperty;
    }

    /**
     * Sets the original order total property.
     *
     * @param pOriginalOrderTotalProperty            the mOriginalOrderTotalProperty to set
     */
    public void setOriginalOrderTotalProperty(String pOriginalOrderTotalProperty) {
        this.mOriginalOrderTotalProperty = pOriginalOrderTotalProperty;
    }

    /**
     * Gets the down payment property.
     *
     * @return the mDownPaymentProperty
     */
    public String getDownPaymentProperty() {
        return mDownPaymentProperty;
    }

    /**
     * Sets the down payment property.
     *
     * @param pDownPaymentProperty            the mDownPaymentProperty to set
     */
    public void setDownPaymentProperty(String pDownPaymentProperty) {
        mDownPaymentProperty = pDownPaymentProperty;
    }

    /**
     * Gets the additional payment property.
     *
     * @return the mAdditionalPaymentProperty
     */
    public String getAdditionalPaymentProperty() {
        return mAdditionalPaymentProperty;
    }

    /**
     * Sets the additional payment property.
     *
     * @param pAdditionalPaymentProperty            the mAdditionalPaymentProperty to set
     */
    public void setAdditionalPaymentProperty(String pAdditionalPaymentProperty) {
        this.mAdditionalPaymentProperty = pAdditionalPaymentProperty;
    }
	
	/**
	 * Gets the order summary error msg value property.
	 *
	 * @return mOrderSummaryErrorMsgValueProperty
	 */
	public String getOrderSummaryErrorMsgValueProperty() {
		return mOrderSummaryErrorMsgValueProperty;
	}

	/**
	 * Sets the order summary error msg value property.
	 *
	 * @param pOrderSummaryErrorMsgValueProperty 				- the OrderSummaryErrorMsgValueProperty
	 */
	public void setOrderSummaryErrorMsgValueProperty(
			String pOrderSummaryErrorMsgValueProperty) {
		this.mOrderSummaryErrorMsgValueProperty = pOrderSummaryErrorMsgValueProperty;
	}

	/** Holds mStoreCodePropertyName. */
	private String mStoreCodePropertyName;
	
	/** Holds mStorePhonePropertyName. */
	private String mStorePhonePropertyName;
	
	/** Holds mStoreNamePropertyName. */
	private String mStoreNamePropertyName;
	
	/** Holds mItemColorPropertyName. */
	private String mItemColorPropertyName;
	
	/** Holds mLayawayOrderSummaryErrorMsgProperty. */
	private String mLayawayOrderSummaryErrorMsgProperty;
	
	/** Holds mLayawayOrderSummaryErrorProperty. */
	private String mLayawayOrderSummaryErrorProperty;
	
	/** Holds layawayOrderSummaryBalanceDueProperty. */
	private String mLayawayOrderSummaryBalanceDueProperty;
	
	/** The m order details json for invalid zip. */
	private String mOrderDetailsJsonForInvalidZip;
	
	/** The Partner order id. */
	private String mPartnerOrderId;

	/** The m date exists property name. */
	private String mDateExistsPropertyName;
	/**
	 * Gets the order details json for invalid zip.
	 *
	 * @return mOrderDetailsJsonForInvalidZip
	 */
	public String getOrderDetailsJsonForInvalidZip() {
		return mOrderDetailsJsonForInvalidZip;
	}

	/**
	 * Sets the order details json for invalid zip.
	 *
	 * @param pOrderDetailsJsonForInvalidZip the new order details json for invalid zip
	 */
	public void setOrderDetailsJsonForInvalidZip(String pOrderDetailsJsonForInvalidZip) {
		this.mOrderDetailsJsonForInvalidZip = pOrderDetailsJsonForInvalidZip;
	}
	
	/**
	 * Gets the layaway order summary balance due property.
	 *
	 * @return mLayawayOrderSummaryBalanceDueProperty
	 */
	public String getLayawayOrderSummaryBalanceDueProperty() {
		return mLayawayOrderSummaryBalanceDueProperty;
	}

	/**
	 * Sets the layaway order summary balance due property.
	 *
	 * @param pLayawayOrderSummaryBalanceDueProperty    				- the LayawayOrderSummaryBalanceDueProperty
	 */
	public void setLayawayOrderSummaryBalanceDueProperty(
			String pLayawayOrderSummaryBalanceDueProperty) {
		this.mLayawayOrderSummaryBalanceDueProperty = pLayawayOrderSummaryBalanceDueProperty;
	}

	/**
	 * Gets the layaway order summary error property.
	 *
	 * @return mLayawayOrderSummaryErrorProperty
	 */
	public String getLayawayOrderSummaryErrorProperty() {
		return mLayawayOrderSummaryErrorProperty;
	}

	/**
	 * Sets the layaway order summary error property.
	 *
	 * @param pLayawayOrderSummaryErrorProperty 				- the LayawayOrderSummaryErrorProperty
	 */
	public void setLayawayOrderSummaryErrorProperty(
			String pLayawayOrderSummaryErrorProperty) {
		this.mLayawayOrderSummaryErrorProperty = pLayawayOrderSummaryErrorProperty;
	}

	/**
	 * Gets the layaway order summary error msg property.
	 *
	 * @return mLayawayOrderSummaryErrorMsgProperty
	 */
	public String getLayawayOrderSummaryErrorMsgProperty() {
		return mLayawayOrderSummaryErrorMsgProperty;
	}

	/**
	 * Sets the layaway order summary error msg property.
	 *
	 * @param pLayawayOrderSummaryErrorMsgProperty 				- the pLayawayOrderSummaryErrorMsgProperty
	 */
	public void setLayawayOrderSummaryErrorMsgProperty(
			String pLayawayOrderSummaryErrorMsgProperty) {
		this.mLayawayOrderSummaryErrorMsgProperty = pLayawayOrderSummaryErrorMsgProperty;
	}

	/**
	 * Gets the item color property name.
	 *
	 * @return mItemColorPropertyName
	 */
	public String getItemColorPropertyName() {
		return mItemColorPropertyName;
	}

	/**
	 * Sets the item color property name.
	 *
	 * @param pItemColorPropertyName 				- the ItemColorPropertyName
	 */
	public void setItemColorPropertyName(String pItemColorPropertyName) {
		 mItemColorPropertyName = pItemColorPropertyName;
	}

	/**  Holds mColorPropertyName. */
	private String mColorPropertyName;
	
	/**
	 * Gets the color property name.
	 *
	 * @return mColorPropertyName
	 */
	public String getColorPropertyName() {
		return mColorPropertyName;
	}

	/**
	 * Sets the color property name.
	 *
	 * @param pColorPropertyName 				- the ColorPropertyName
	 */
	public void setColorPropertyName(String pColorPropertyName) {
		 mColorPropertyName = pColorPropertyName;
	}

	/** Holds mItemSizeProprtyName. */
	private String mItemSizeProprtyName;
	
	/**
	 * Gets the item size proprty name.
	 *
	 * @return mItemSizeProprtyName
	 */
	public String getItemSizeProprtyName() {
		return mItemSizeProprtyName;
	}

	/**
	 * Sets the item size proprty name.
	 *
	 * @param pItemSizeProprtyName 				- the ItemSizeProprtyName
	 */
	public void setItemSizeProprtyName(String pItemSizeProprtyName) {
		 mItemSizeProprtyName = pItemSizeProprtyName;
	}

	/** Holds mSizePropertyName. */
	private String mSizePropertyName;
	
	/**
	 * Gets the size property name.
	 *
	 * @return mSizePropertyName
	 */
	public String getSizePropertyName() {
		return mSizePropertyName;
	}

	/**
	 * Sets the size property name.
	 *
	 * @param pSizePropertyName 				- the SizePropertyName
	 */
	public void setSizePropertyName(String pSizePropertyName) {
		 mSizePropertyName = pSizePropertyName;
	}

	/** Holds mbppExistsPropertyName. */
	private String mBppExistsPropertyName;
	
	/** Holds mInvalidEmailIDFound. */
	private String mInvalidEmailIDFound;
	
	/**
	 * Gets the invalid email id found.
	 *
	 * @return the invalid email id found
	 */
	public String getInvalidEmailIDFound() {
		return mInvalidEmailIDFound;
	}

	/**
	 * Sets the invalid email id found.
	 *
	 * @param pInvalidEmailIDFound the new invalid email id found
	 */
	public void setInvalidEmailIDFound(String pInvalidEmailIDFound) {
		this.mInvalidEmailIDFound = pInvalidEmailIDFound;
	}

	/**
	 * Gets the bpp exists property name.
	 *
	 * @return mBppExistsPropertyName
	 */
	public String getBppExistsPropertyName() {
		return mBppExistsPropertyName;
	}

	/**
	 * Sets the bpp exists property name.
	 *
	 * @param pBppExistsPropertyName 				- the BppExistsPropertyName
	 */
	public void setBppExistsPropertyName(String pBppExistsPropertyName) {
		 mBppExistsPropertyName = pBppExistsPropertyName;
	}

	/** Holds mFeeChargeNamePropertyName. */
	private String mFeeChargeNamePropertyName;
	
	/**
	 * Gets the fee charge name property name.
	 *
	 * @return mFeeChargeNamePropertyName
	 */
	public String getFeeChargeNamePropertyName() {
		return mFeeChargeNamePropertyName;
	}

	/**
	 * Sets the fee charge name property name.
	 *
	 * @param pFeeChargeNamePropertyName 					- the FeeChargeNamePropertyName
	 */
	public void setFeeChargeNamePropertyName(String pFeeChargeNamePropertyName) {
		mFeeChargeNamePropertyName = pFeeChargeNamePropertyName;
	}

	/** Holds mGiftWrapPropertyName. */
	private String mGiftWrapPropertyName;
	
	/**
	 * Gets the gift wrap property name.
	 *
	 * @return mGiftWrapPropertyName
	 */
	public String getGiftWrapPropertyName() {
		return mGiftWrapPropertyName;
	}

	/**
	 * Sets the gift wrap property name.
	 *
	 * @param pGiftWrapPropertyName 				- the GiftWrapPropertyName
	 */
	public void setGiftWrapPropertyName(String pGiftWrapPropertyName) {
		 mGiftWrapPropertyName = pGiftWrapPropertyName;
	}

	/** Holds mBppChargeAmountPropertyName. */
	private String mBppChargeAmountPropertyName;
	
	/**
	 * Gets the bpp charge amount property name.
	 *
	 * @return mBppChargeAmountPropertyName
	 */
	public String getBppChargeAmountPropertyName() {
		return mBppChargeAmountPropertyName;
	}

	/**
	 * Sets the bpp charge amount property name.
	 *
	 * @param pBppChargeAmountPropertyName 					- the BppChargeAmountPropertyName
	 */
	public void setBppChargeAmountPropertyName(String pBppChargeAmountPropertyName) {
		 mBppChargeAmountPropertyName = pBppChargeAmountPropertyName;
	}

	/** Holds mAmountPropertyName. */
	private String mAmountPropertyName;
	
	/**
	 * Gets the amount property name.
	 *
	 * @return mAmountPropertyName
	 */
	public String getAmountPropertyName() {
		return mAmountPropertyName;
	}

	/**
	 * Sets the amount property name.
	 *
	 * @param pAmountPropertyName 				- the AmountPropertyName
	 */
	public void setAmountPropertyName(String pAmountPropertyName) {
		 mAmountPropertyName = pAmountPropertyName;
	}

	/** Holds mItemIdPropertyName. */
	private String mItemIdPropertyName;
	
	/**
	 * Gets the item id property name.
	 *
	 * @return mItemIdPropertyName
	 */
	public String getItemIdPropertyName() {
		return mItemIdPropertyName;
	}

	/**
	 * Sets the item id property name.
	 *
	 * @param pItemIdPropertyName 					- the ItemIdPropertyName
	 */
	public void setItemIdPropertyName(String pItemIdPropertyName) {
		 mItemIdPropertyName = pItemIdPropertyName;
	}

	/** Holds mBppTermPropertyName. */
	private String mBppTermPropertyName;
	
	/**
	 * Gets the bpp term property name.
	 *
	 * @return mBppTermPropertyName
	 */
	public String getBppTermPropertyName() {
		return mBppTermPropertyName;
	}

	/**
	 * Sets the bpp term property name.
	 *
	 * @param pBppTermPropertyName 				- the pBppTermPropertyName
	 */
	public void setBppTermPropertyName(String pBppTermPropertyName) {
		 mBppTermPropertyName = pBppTermPropertyName;
	}

	/** Holds mSiteNameForGWPropertyName. */
	private String mSiteNameForGWPropertyName;
	
	/**
	 * Gets the site name for gw property name.
	 *
	 * @return mSiteNameForGWPropertyName
	 */
	public String getSiteNameForGWPropertyName() {
		return mSiteNameForGWPropertyName;
	}

	/**
	 * Sets the site name for gw property name.
	 *
	 * @param pSiteNameForGWPropertyName 					- the SiteNameForGWPropertyName
	 */
	public void setSiteNameForGWPropertyName(String pSiteNameForGWPropertyName) {
		  mSiteNameForGWPropertyName = pSiteNameForGWPropertyName;
	}

	/** Holds mBPPPorpertyName. */
	private String mBppPropertyName;

	/**
	 * Gets the bpp property name.
	 *
	 * @return mBppPropertyName
	 */
	public String getBppPropertyName() {
		return mBppPropertyName;
	}

	/**
	 * Sets the bpp property name.
	 *
	 * @param pBppPropertyName the pBppPropertyName to set
	 */
	public void setBppPropertyName(String pBppPropertyName) {
		 mBppPropertyName = pBppPropertyName;
	}

	/** The m pdpurl property name. */
	private String mPDPURLPropertyName;
	
	/**
	 * Gets the PDPURL property name.
	 *
	 * @return the pDPURLPropertyName
	 */
	public String getPDPURLPropertyName() {
		return mPDPURLPropertyName;
	}

	/**
	 * Sets the PDPURL property name.
	 *
	 * @param pPDPURLPropertyName the pDPURLPropertyName to set
	 */
	public void setPDPURLPropertyName(String pPDPURLPropertyName) {
		mPDPURLPropertyName = pPDPURLPropertyName;
	}

	/** The m shipping method property name. */
	private String mShippingMethodPropertyName;
	
	/**
	 * Gets the shipping method property name.
	 *
	 * @return the shippingMethodPropertyName
	 */
	public String getShippingMethodPropertyName() {
		return mShippingMethodPropertyName;
	}

	/**
	 * Sets the shipping method property name.
	 *
	 * @param pShippingMethodPropertyName the shippingMethodPropertyName to set
	 */
	public void setShippingMethodPropertyName(String pShippingMethodPropertyName) {
		mShippingMethodPropertyName = pShippingMethodPropertyName;
	}

	/** The m surcharge property name. */
	private String mSurchargePropertyName;
	
	/**
	 * Gets the surcharge property name.
	 *
	 * @return the surchargePropertyName
	 */
	public String getSurchargePropertyName() {
		return mSurchargePropertyName;
	}

	/**
	 * Sets the surcharge property name.
	 *
	 * @param pSurchargePropertyName the surchargePropertyName to set
	 */
	public void setSurchargePropertyName(String pSurchargePropertyName) {
		mSurchargePropertyName = pSurchargePropertyName;
	}

	/** The m is gift message. */
	private String mIsGiftMessage;
	
	/**
	 * Gets the checks if is gift message.
	 *
	 * @return the isGiftMessage
	 */
	public String getIsGiftMessage() {
		return mIsGiftMessage;
	}

	/**
	 * Sets the checks if is gift message.
	 *
	 * @param pIsGiftMessage the isGiftMessage to set
	 */
	public void setIsGiftMessage(String pIsGiftMessage) {
		mIsGiftMessage = pIsGiftMessage;
	}

	/** The m shippment count property name. */
	private String mShippmentCountPropertyName;
	
	/**
	 * Gets the shippment count property name.
	 *
	 * @return the shippmentCountPropertyName
	 */
	public String getShippmentCountPropertyName() {
		return mShippmentCountPropertyName;
	}

	/**
	 * Sets the shippment count property name.
	 *
	 * @param pShippmentCountPropertyName the shippmentCountPropertyName to set
	 */
	public void setShippmentCountPropertyName(String pShippmentCountPropertyName) {
		mShippmentCountPropertyName = pShippmentCountPropertyName;
	}

	/** The m total line item count property name. */
	private String mTotalLineItemCountPropertyName;
	
	/**
	 * Gets the total line item count property name.
	 *
	 * @return the totalLineItemCountPropertyName
	 */
	public String getTotalLineItemCountPropertyName() {
		return mTotalLineItemCountPropertyName;
	}

	/**
	 * Sets the total line item count property name.
	 *
	 * @param pTotalLineItemCountPropertyName the totalLineItemCountPropertyName to set
	 */
	public void setTotalLineItemCountPropertyName(
			String pTotalLineItemCountPropertyName) {
		mTotalLineItemCountPropertyName = pTotalLineItemCountPropertyName;
	}

	/** The m totalshippment info count property name. */
	private String mTotalshippmentInfoCountPropertyName;
	
	/**
	 * Gets the totalshippment info count property name.
	 *
	 * @return the totalshippmentInfoCountPropertyName
	 */
	public String getTotalshippmentInfoCountPropertyName() {
		return mTotalshippmentInfoCountPropertyName;
	}

	/**
	 * Sets the totalshippment info count property name.
	 *
	 * @param pTotalshippmentInfoCountPropertyName the totalshippmentInfoCountPropertyName to set
	 */
	public void setTotalshippmentInfoCountPropertyName(
			String pTotalshippmentInfoCountPropertyName) {
		mTotalshippmentInfoCountPropertyName = pTotalshippmentInfoCountPropertyName;
	}

	/** The m orderd line items property name. */
	private String mOrderdLineItemsPropertyName;
	
	/**
	 * Gets the orderd line items property name.
	 *
	 * @return the orderdLineItemsPropertyName
	 */
	public String getOrderdLineItemsPropertyName() {
		return mOrderdLineItemsPropertyName;
	}

	/**
	 * Sets the orderd line items property name.
	 *
	 * @param pOrderdLineItemsPropertyName the orderdLineItemsPropertyName to set
	 */
	public void setOrderdLineItemsPropertyName(String pOrderdLineItemsPropertyName) {
		mOrderdLineItemsPropertyName = pOrderdLineItemsPropertyName;
	}

	/**
	 * Gets the store phone property name.
	 *
	 * @return mStorePhonePropertyName
	 */
	public String getStorePhonePropertyName() {
		return mStorePhonePropertyName;
	}

	/**
	 * Sets the store phone property name.
	 *
	 * @param pStorePhonePropertyName 					- the StorePhonePropertyName
	 */
	public void setStorePhonePropertyName(String pStorePhonePropertyName) {
		 mStorePhonePropertyName = pStorePhonePropertyName;
	}
	
	/**
	 * Gets the store name property name.
	 *
	 * @return mStoreNamePropertyName
	 */
	public String getStoreNamePropertyName() {
		return mStoreNamePropertyName;
	}

	/**
	 * Sets the store name property name.
	 *
	 * @param pStoreNamePropertyName 					- the StoreNamePropertyName
	 */
	public void setStoreNamePropertyName(String pStoreNamePropertyName) {
		  mStoreNamePropertyName = pStoreNamePropertyName;
	}

	/**
	 * Gets the store code property name.
	 *
	 * @return mStoreCodePropertyName
	 */
	public String getStoreCodePropertyName() {
		return mStoreCodePropertyName;
	}
	
	/**
	 * Sets the store code property name.
	 *
	 * @param pStoreCodePropertyName 					- the StoreCodePropertyName
	 */
	public void setStoreCodePropertyName(String pStoreCodePropertyName) {
		  mStoreCodePropertyName = pStoreCodePropertyName;
	}
	
	/**
	 * Gets the layaway order summary property.
	 *
	 * @return mLayawayOrderSummaryProperty
	 */
	public String getLayawayOrderSummaryProperty() {
		return mLayawayOrderSummaryProperty;
	}
	
	/**
	 * Sets the layaway order summary property.
	 *
	 * @param pLayawayOrderSummaryProperty 				the mLayawayOrderSummaryProperty
	 */
	public void setLayawayOrderSummaryProperty(String pLayawayOrderSummaryProperty) {
		this.mLayawayOrderSummaryProperty = pLayawayOrderSummaryProperty;
	}

	/**
	 * Gets the layaway delivery method.
	 *
	 * @return mLayawayDeliveryMethod
	 */
	public String getLayawayDeliveryMethod() {
		return mLayawayDeliveryMethod;
	}
	
	/**
	 * Sets the layaway delivery method.
	 *
	 * @param pLayawayDeliveryMethod 				the mLayawayDeliveryMethod to set
	 */
	public void setLayawayDeliveryMethod(String pLayawayDeliveryMethod) {
		this.mLayawayDeliveryMethod = pLayawayDeliveryMethod;
	}

	/**
	 * Gets the service name.
	 *
	 * @return mServiceName
	 */
	public String getServiceName() {
		return mServiceName;
	}
	
	/**
	 * Sets the service name.
	 *
	 * @param pServiceName 			the mServiceName to set
	 */
	public void setServiceName(String pServiceName) {
		this.mServiceName = pServiceName;
	}
	
	/**
	 * Gets the name space.
	 *
	 * @return mNameSpace
	 */
	public String getNameSpace() {
		return mNameSpace;
	}
	
	/**
	 * Sets the name space.
	 *
	 * @param pNameSpace 			the mNameSpace to set
	 */
	public void setNameSpace(String pNameSpace) {
		this.mNameSpace = pNameSpace;
	}
	
	/**
	 * Gets the wsdl location.
	 *
	 * @return mWsdlLocation
	 */
	public String getWsdlLocation() {
		return mWsdlLocation;
	}
	
	/**
	 * Sets the wsdl location.
	 *
	 * @param pWsdlLocation 			the mWsdlLocation to set
	 */
	public void setWsdlLocation(String pWsdlLocation) {
		this.mWsdlLocation = pWsdlLocation;
	}
	
    /**
     * Gets the order summary order status property.
     *
     * @return mOrderSummaryOrderStatusProperty
     */
    public String getOrderSummaryOrderStatusProperty() {
		return mOrderSummaryOrderStatusProperty;
	}
    
    /**
     * Sets the order summary order status property.
     *
     * @param pOrderSummaryOrderStatusProperty 			the mOrderSummaryOrderStatusProperty to set
     */
	public void setOrderSummaryOrderStatusProperty(
			String pOrderSummaryOrderStatusProperty) {
		this.mOrderSummaryOrderStatusProperty = pOrderSummaryOrderStatusProperty;
	}

	/**
	 * Gets the order summary original order total property.
	 *
	 * @return mOrderSummaryOriginalOrderTotalProperty
	 */
    public String getOrderSummaryOriginalOrderTotalProperty() {
		return mOrderSummaryOriginalOrderTotalProperty;
	}
    
    /**
     * Sets the order summary original order total property.
     *
     * @param pOrderSummaryOriginalOrderTotalProperty 			the mOrderSummaryOrderCreationDateProperty to set
     */
	public void setOrderSummaryOriginalOrderTotalProperty(
			String pOrderSummaryOriginalOrderTotalProperty) {
		this.mOrderSummaryOriginalOrderTotalProperty = pOrderSummaryOriginalOrderTotalProperty;
	}
	
	/**
	 * Gets the order summary order creation date property.
	 *
	 * @return mOrderSummaryOrderCreationDateProperty
	 */
    public String getOrderSummaryOrderCreationDateProperty() {
		return mOrderSummaryOrderCreationDateProperty;
	}
    
    /**
     * Sets the order summary order creation date property.
     *
     * @param pOrderSummaryOrderCreationDateProperty 			the mOrderSummaryOrderCreationDateProperty to set
     */
	public void setOrderSummaryOrderCreationDateProperty(
			String pOrderSummaryOrderCreationDateProperty) {
		this.mOrderSummaryOrderCreationDateProperty = pOrderSummaryOrderCreationDateProperty;
	}
	
	/**
	 * Gets the order summary partner order number property.
	 *
	 * @return mOrderSummaryPartnerOrderNumberProperty
	 */
    public String getOrderSummaryPartnerOrderNumberProperty() {
		return mOrderSummaryPartnerOrderNumberProperty;
	}
    
    /**
     * Sets the order summary partner order number property.
     *
     * @param pOrderSummaryPartnerOrderNumberProperty 			the mOrderSummaryPartnerOrderNumberProperty to set
     */
	public void setOrderSummaryPartnerOrderNumberProperty(
			String pOrderSummaryPartnerOrderNumberProperty) {
		this.mOrderSummaryPartnerOrderNumberProperty = pOrderSummaryPartnerOrderNumberProperty;
	}

	/**
	 * Gets the order status soap property.
	 *
	 * @return the mOrderStatusSoapProperty
	 */
    public String getOrderStatusSoapProperty() {
        return mOrderStatusSoapProperty;
    }

    /**
     * Sets the order status soap property.
     *
     * @param pOrderStatusSoapProperty            the mOrderStatusSoapProperty to set
     */
    public void setOrderStatusSoapProperty(String pOrderStatusSoapProperty) {
        this.mOrderStatusSoapProperty = pOrderStatusSoapProperty;
    }

    /**
     * Gets the soap envelope property.
     *
     * @return the mSoapEnvelopeProperty
     */
    public String getSoapEnvelopeProperty() {
        return mSoapEnvelopeProperty;
    }

    /**
     * Sets the soap envelope property.
     *
     * @param pSoapEnvelopeProperty            the mSoapEnvelopeProperty to set
     */
    public void setSoapEnvelopeProperty(String pSoapEnvelopeProperty) {
        this.mSoapEnvelopeProperty = pSoapEnvelopeProperty;
    }

    /**
     * Gets the soap body property.
     *
     * @return the mSoapBodyProperty
     */
    public String getSoapBodyProperty() {
        return mSoapBodyProperty;
    }

    /**
     * Sets the soap body property.
     *
     * @param pSoapBodyProperty            the mSoapBodyProperty to set
     */
    public void setSoapBodyProperty(String pSoapBodyProperty) {
        this.mSoapBodyProperty = pSoapBodyProperty;
    }

    /**
     * Gets the order detail response property.
     *
     * @return the mOrderDetailResponseProperty
     */
    public String getOrderDetailResponseProperty() {
        return mOrderDetailResponseProperty;
    }

    /**
     * Sets the order detail response property.
     *
     * @param pOrderDetailResponseProperty            the mOrderDetailResponseProperty to set
     */
    public void setOrderDetailResponseProperty(
            String pOrderDetailResponseProperty) {
        this.mOrderDetailResponseProperty = pOrderDetailResponseProperty;
    }

    /**
     * Gets the order detail property.
     *
     * @return the mOrderDetailProperty
     */
    public String getOrderDetailProperty() {
        return mOrderDetailProperty;
    }

    /**
     * Sets the order detail property.
     *
     * @param pOrderDetailProperty            the mOrderDetailProperty to set
     */
    public void setOrderDetailProperty(String pOrderDetailProperty) {
        this.mOrderDetailProperty = pOrderDetailProperty;
    }

    /**
     * Gets the bill to propety.
     *
     * @return the mBillToPropety
     */
    public String getBillToPropety() {
        return mBillToPropety;
    }
    
	/**
	 * Gets the order details term property name.
	 *
	 * @return the mOrderDetailsTermPropertyName
	 */
	public String getOrderDetailsTermPropertyName() {
		return mOrderDetailsTermPropertyName;
	}

	/**
	 * Sets the order details term property name.
	 *
	 * @param pOrderDetailsTermPropertyName the mOrderDetailsTermPropertyName to set
	 */
	public void setOrderDetailsTermPropertyName(
			String pOrderDetailsTermPropertyName) {
		this.mOrderDetailsTermPropertyName = pOrderDetailsTermPropertyName;
	}
	
	/**
	 * Sets the order details display name property name.
	 *
	 * @param pOrderDetailsDisplayNamePropertyName 					- the mOrderDetailsTermPropertyName to set
	 */
	public void setOrderDetailsDisplayNamePropertyName(
			String pOrderDetailsDisplayNamePropertyName) {
		 mOrderDetailsDisplayNamePropertyName = pOrderDetailsDisplayNamePropertyName;
	}

	/**
	 * Gets the order details display name property name.
	 *
	 * @return the mOrderDetailsDisplayNamePropertyName
	 */
	public String getOrderDetailsDisplayNamePropertyName() {
		return mOrderDetailsDisplayNamePropertyName;
	}
    
    /**
     * Sets the bill to propety.
     *
     * @param pBillToPropety            the mBillToPropety to set
     */
    public void setBillToPropety(String pBillToPropety) {
        this.mBillToPropety = pBillToPropety;
    }

    /**
     * Gets the first name property.
     *
     * @return the mFirstNameProperty
     */
    public String getFirstNameProperty() {
        return mFirstNameProperty;
    }

    /**
     * Sets the first name property.
     *
     * @param pFirstNameProperty            the mFirstNameProperty to set
     */
    public void setFirstNameProperty(String pFirstNameProperty) {
        this.mFirstNameProperty = pFirstNameProperty;
    }

    /**
     * Gets the last name property.
     *
     * @return the mLastNameProperty
     */
    public String getLastNameProperty() {
        return mLastNameProperty;
    }

    /**
     * Sets the last name property.
     *
     * @param pLastNameProperty            the mLastNameProperty to set
     */
    public void setLastNameProperty(String pLastNameProperty) {
        this.mLastNameProperty = pLastNameProperty;
    }

    /**
     * Gets the payment history property.
     *
     * @return the mPaymentHistoryProperty
     */
    public String getPaymentHistoryProperty() {
        return mPaymentHistoryProperty;
    }

    /**
     * Sets the payment history property.
     *
     * @param pPaymentHistoryProperty            the mPaymentHistoryProperty to set
     */
    public void setPaymentHistoryProperty(String pPaymentHistoryProperty) {
        this.mPaymentHistoryProperty = pPaymentHistoryProperty;
    }

    /**
     * Gets the date property.
     *
     * @return the mDateProperty
     */
    public String getDateProperty() {
        return mDateProperty;
    }

    /**
     * Sets the date property.
     *
     * @param pDateProperty            the mDateProperty to set
     */
    public void setDateProperty(String pDateProperty) {
        this.mDateProperty = pDateProperty;
    }

    /**
     * Gets the amount paid property.
     *
     * @return the mAmountPaidProperty
     */
    public String getAmountPaidProperty() {
        return mAmountPaidProperty;
    }

    /**
     * Sets the amount paid property.
     *
     * @param pAmountPaidProperty            the mAmountPaidProperty to set
     */
    public void setAmountPaidProperty(String pAmountPaidProperty) {
        this.mAmountPaidProperty = pAmountPaidProperty;
    }

    /**
     * Gets the order creation date property.
     *
     * @return the mOrderCreationDateProperty
     */
    public String getOrderCreationDateProperty() {
        return mOrderCreationDateProperty;
    }

    /**
     * Sets the order creation date property.
     *
     * @param pOrderCreationDateProperty            the mOrderCreationDateProperty to set
     */
    public void setOrderCreationDateProperty(String pOrderCreationDateProperty) {
        this.mOrderCreationDateProperty = pOrderCreationDateProperty;
    }

    /**
     * Gets the partner order number property.
     *
     * @return the mPartnerOrderNumberProperty
     */
    public String getPartnerOrderNumberProperty() {
        return mPartnerOrderNumberProperty;
    }

    /**
     * Sets the partner order number property.
     *
     * @param pPartnerOrderNumberProperty            the mPartnerOrderNumberProperty to set
     */
    public void setPartnerOrderNumberProperty(String pPartnerOrderNumberProperty) {
        this.mPartnerOrderNumberProperty = pPartnerOrderNumberProperty;
    }

    /**
     * Gets the line item property.
     *
     * @return the mLineItemProperty
     */
    public String getLineItemProperty() {
        return mLineItemProperty;
    }

    /**
     * Sets the line item property.
     *
     * @param pLineItemProperty            the mLineItemProperty to set
     */
    public void setLineItemProperty(String pLineItemProperty) {
        this.mLineItemProperty = pLineItemProperty;
    }

    /**
     * Gets the description property.
     *
     * @return the mDescriptionProperty
     */
    public String getDescriptionProperty() {
        return mDescriptionProperty;
    }

    /**
     * Sets the description property.
     *
     * @param pDescriptionProperty            the mDescriptionProperty to set
     */
    public void setDescriptionProperty(String pDescriptionProperty) {
        this.mDescriptionProperty = pDescriptionProperty;
    }

    /**
     * Gets the quantity property.
     *
     * @return the mQuantityProperty
     */
    public String getQuantityProperty() {
        return mQuantityProperty;
    }

    /**
     * Sets the quantity property.
     *
     * @param pQuantityProperty            the mQuantityProperty to set
     */
    public void setQuantityProperty(String pQuantityProperty) {
        this.mQuantityProperty = pQuantityProperty;
    }

    /**
     * Gets the line status property.
     *
     * @return the mLineStatusProperty
     */
    public String getLineStatusProperty() {
        return mLineStatusProperty;
    }

    /**
     * Sets the line status property.
     *
     * @param pLineStatusProperty            the mLineStatusProperty to set
     */
    public void setLineStatusProperty(String pLineStatusProperty) {
        this.mLineStatusProperty = pLineStatusProperty;
    }

	/**
	 * Gets the gfs order lookup service package.
	 *
	 * @return the gfsOrderLookupServicePackage
	 */
	public String getGfsOrderLookupServicePackage() {
		return mGfsOrderLookupServicePackage;
	}
	
	/**
	 * Sets the gfs order lookup service package.
	 *
	 * @param pGfsOrderLookupServicePackage            the mGfsOrderLookupServicePackage to set
	 */
	public void setGfsOrderLookupServicePackage(
			String pGfsOrderLookupServicePackage) {
		this.mGfsOrderLookupServicePackage = pGfsOrderLookupServicePackage;
	}
	
	/**
	 * Gets the gfs order details lookup service package.
	 *
	 * @return the gfsOrderDetailsLookupServicePackage
	 */
	public String getGfsOrderDetailsLookupServicePackage() {
		return mGfsOrderDetailsLookupServicePackage;
	}
	
	/**
	 * Sets the gfs order details lookup service package.
	 *
	 * @param pGfsOrderDetailsLookupServicePackage            the mGfsOrderDetailsLookupServicePackage to set
	 */
	public void setGfsOrderDetailsLookupServicePackage(
			String pGfsOrderDetailsLookupServicePackage) {
		this.mGfsOrderDetailsLookupServicePackage = pGfsOrderDetailsLookupServicePackage;
	}
	
	/**
	 * Gets the order summary map.
	 *
	 * @return the orderSummaryMap
	 */
	public Map<String, String> getOrderSummaryMap() {
		return mOrderSummaryMap;
	}

	/**
	 * Sets the order summary map.
	 *
	 * @param pOrderSummaryMap            the mOrderSummaryMap to set
	 */
	public void setOrderSummaryMap(Map<String, String> pOrderSummaryMap) {
		mOrderSummaryMap = pOrderSummaryMap;
	}
	
	/**
	 * Gets the tracking list property.
	 *
	 * @return the mTrackingListProperty
	 */
	public String getTrackingListProperty() {
		return mTrackingListProperty;
	}
	
	/**
	 * Sets the tracking list property.
	 *
	 * @param pTrackingListProperty 				- the mTrackingListProperty to set
	 */
	public void setTrackingListProperty(String pTrackingListProperty) {
		this.mTrackingListProperty = pTrackingListProperty;
	}
	
	/**
	 * Gets the pricing property.
	 *
	 * @return the pricingProperty
	 */
	public String getPricingProperty() {
		return mPricingProperty;
	}
	
	/**
	 * Sets the pricing property.
	 *
	 * @param pPricingProperty            the mPricingProperty to set
	 */
	public void setPricingProperty(String pPricingProperty) {
		this.mPricingProperty = pPricingProperty;
	}
	
	/**
	 * Gets the customer order property.
	 *
	 * @return the mCustomerOrderProperty
	 */
	public String getCustomerOrderProperty() {
		return mCustomerOrderProperty;
	}
	
	/**
	 * Sets the customer order property.
	 *
	 * @param pCustomerOrderProperty            the mCustomerOrderProperty to set
	 */
	public void setCustomerOrderProperty(String pCustomerOrderProperty) {
		this.mCustomerOrderProperty = pCustomerOrderProperty;
	}
	
	/**
	 * Gets the order payment list property.
	 *
	 * @return the mOrderPaymentListProperty
	 */
	public String getOrderPaymentListProperty() {
		return mOrderPaymentListProperty;
	}
	
	/**
	 * Sets the order payment list property.
	 *
	 * @param pOrderPaymentListProperty            the mOrderPaymentListProperty to set
	 */
	public void setOrderPaymentListProperty(String pOrderPaymentListProperty) {
		this.mOrderPaymentListProperty = pOrderPaymentListProperty;
	}
	
	/**
	 * Gets the order payment property.
	 *
	 * @return the mOrderPaymentProperty
	 */
	public String getOrderPaymentProperty() {
		return mOrderPaymentProperty;
	}
	
	/**
	 * Sets the order payment property.
	 *
	 * @param pOrderPaymentProperty            the mOrderPaymentProperty to set
	 */
	public void setOrderPaymentProperty(String pOrderPaymentProperty) {
		this.mOrderPaymentProperty = pOrderPaymentProperty;
	}
	
	/**
	 * Gets the custom attributes property.
	 *
	 * @return the mCustomAttributesProperty
	 */
	public String getCustomAttributesProperty() {
		return mCustomAttributesProperty;
	}
	
	/**
	 * Sets the custom attributes property.
	 *
	 * @param pCustomAttributesProperty            the mCustomAttributesProperty to set
	 */
	public void setCustomAttributesProperty(String pCustomAttributesProperty) {
		this.mCustomAttributesProperty = pCustomAttributesProperty;
	}
	
	/**
	 * Gets the custom attribute property.
	 *
	 * @return the mCustomAttributeProperty
	 */
	public String getCustomAttributeProperty() {
		return mCustomAttributeProperty;
	}
	
	/**
	 * Sets the custom attribute property.
	 *
	 * @param pCustomAttributeProperty            the mCustomAttributeProperty to set
	 */
	public void setCustomAttributeProperty(String pCustomAttributeProperty) {
		this.mCustomAttributeProperty = pCustomAttributeProperty;
	}
	
	/**
	 * Gets the name property.
	 *
	 * @return the mNameProperty
	 */
	public String getNameProperty() {
		return mNameProperty;
	}
	
	/**
	 * Sets the name property.
	 *
	 * @param pNameProperty            the mNameProperty to set
	 */
	public void setNameProperty(String pNameProperty) {
		this.mNameProperty = pNameProperty;
	}
	
	/**
	 * Gets the value property.
	 *
	 * @return the mValueProperty
	 */
	public String getValueProperty() {
		return mValueProperty;
	}
	
	/**
	 * Sets the value property.
	 *
	 * @param pValueProperty            the mValueProperty to set
	 */
	public void setValueProperty(String pValueProperty) {
		this.mValueProperty = pValueProperty;
	}
	
	/**
	 * Gets the order summary property.
	 *
	 * @return the mOrderSummaryProperty
	 */
	public String getOrderSummaryProperty() {
		return mOrderSummaryProperty;
	}
	
	/**
	 * Sets the order summary property.
	 *
	 * @param pOrderSummaryProperty            the mOrderSummaryProperty to set
	 */
	public void setOrderSummaryProperty(String pOrderSummaryProperty) {
		this.mOrderSummaryProperty = pOrderSummaryProperty;
	}
	
	/**
	 * Gets the merchandise tax property.
	 *
	 * @return the mMarchandiseTaxProperty
	 */
	public String getMerchandiseTaxProperty() {
		return mMerchandiseTaxProperty;
	}
	
	/**
	 * Sets the merchandise tax property.
	 *
	 * @param pMerchandiseTaxProperty 					the mMerchandiseTaxProperty to set
	 */
	public void setMerchandiseTaxProperty(String pMerchandiseTaxProperty) {
		this.mMerchandiseTaxProperty = pMerchandiseTaxProperty;
	}
	
	/**
	 * Gets the ispu item property.
	 *
	 * @return the mIspuItemProperty
	 */
	public String getIspuItemProperty() {
		return mIspuItemProperty;
	}
	
	/**
	 * Sets the ispu item property.
	 *
	 * @param pIspuItemProperty            the mIspuItemProperty to set
	 */
	public void setIspuItemProperty(String pIspuItemProperty) {
		this.mIspuItemProperty = pIspuItemProperty;
	}
	
	/**
	 * Gets the store front address property.
	 *
	 * @return the mStoreFrontAddressProperty
	 */
	public String getStoreFrontAddressProperty() {
		return mStoreFrontAddressProperty;
	}
	
	/**
	 * Sets the store front address property.
	 *
	 * @param pStoreFrontAddressProperty            the mStoreFrontAddressProperty to set
	 */
	public void setStoreFrontAddressProperty(String pStoreFrontAddressProperty) {
		this.mStoreFrontAddressProperty = pStoreFrontAddressProperty;
	}
	
	/**
	 * Gets the proxy address property.
	 *
	 * @return the mProxyAddressProperty
	 */
	public String getProxyAddressProperty() {
		return mProxyAddressProperty;
	}
	
	/**
	 * Sets the proxy address property.
	 *
	 * @param pProxyAddressProperty            the mProxyAddressProperty to set
	 */
	public void setProxyAddressProperty(String pProxyAddressProperty) {
		this.mProxyAddressProperty = pProxyAddressProperty;
	}
	
	/**
	 * Gets the tracking info property.
	 *
	 * @return the mTrackingInfoProperty
	 */
	public String getTrackingInfoProperty() {
		return mTrackingInfoProperty;
	}
	
	/**
	 * Sets the tracking info property.
	 *
	 * @param pTrackingInfoProperty            the mTrackingInfoProperty to set
	 */
	public void setTrackingInfoProperty(String pTrackingInfoProperty) {
		this.mTrackingInfoProperty = pTrackingInfoProperty;
	}
	
	/**
	 * Gets the tracking number property.
	 *
	 * @return the mTrackingNumberProperty
	 */
	public String getTrackingNumberProperty() {
		return mTrackingNumberProperty;
	}
	
	/**
	 * Sets the tracking number property.
	 *
	 * @param pTrackingNumberProperty            the mTrackingNumberProperty to set
	 */
	public void setTrackingNumberProperty(String pTrackingNumberProperty) {
		this.mTrackingNumberProperty = pTrackingNumberProperty;
	}
	
	/**
	 * Gets the tracking url property.
	 *
	 * @return the mTrackingUrlProperty
	 */
	public String getTrackingUrlProperty() {
		return mTrackingUrlProperty;
	}
	
	/**
	 * Sets the tracking url property.
	 *
	 * @param pTrackingUrlProperty            the mTrackingUrlProperty to set
	 */
	public void setTrackingUrlProperty(String pTrackingUrlProperty) {
		this.mTrackingUrlProperty = pTrackingUrlProperty;
	}
	
	/**
	 * Gets the fee charge list property.
	 *
	 * @return the mFeeChargeListProperty
	 */
	public String getFeeChargeListProperty() {
		return mFeeChargeListProperty;
	}
	
	/**
	 * Sets the fee charge list property.
	 *
	 * @param pFeeChargeListProperty            the mFeeChargeListProperty to set
	 */
	public void setFeeChargeListProperty(String pFeeChargeListProperty) {
		this.mFeeChargeListProperty = pFeeChargeListProperty;
	}
	
	/**
	 * Gets the fee charge property.
	 *
	 * @return the mFeeChargeProperty
	 */
	public String getFeeChargeProperty() {
		return mFeeChargeProperty;
	}
	
	/**
	 * Sets the fee charge property.
	 *
	 * @param pFeeChargeProperty            the mFeeChargeProperty to set
	 */
	public void setFeeChargeProperty(String pFeeChargeProperty) {
		this.mFeeChargeProperty = pFeeChargeProperty;
	}
	
	/**
	 * Gets the tax data property.
	 *
	 * @return the mTaxDataProperty
	 */
	public String getTaxDataProperty() {
		return mTaxDataProperty;
	}
	
	/**
	 * Sets the tax data property.
	 *
	 * @param pTaxDataProperty            the mTaxDataProperty to set
	 */
	public void setTaxDataProperty(String pTaxDataProperty) {
		this.mTaxDataProperty = pTaxDataProperty;
	}
	
	/**
	 * Gets the customization list property.
	 *
	 * @return the mCustomizationListProperty
	 */
	public String getCustomizationListProperty() {
		return mCustomizationListProperty;
	}
	
	/**
	 * Sets the customization list property.
	 *
	 * @param pCustomizationListProperty            the mCustomizationListProperty to set
	 */
	public void setCustomizationListProperty(String pCustomizationListProperty) {
		this.mCustomizationListProperty = pCustomizationListProperty;
	}
	
	/**
	 * Gets the customization data property.
	 *
	 * @return the mCustomizationDataProperty
	 */
	public String getCustomizationDataProperty() {
		return mCustomizationDataProperty;
	}
	
	/**
	 * Sets the customization data property.
	 *
	 * @param pCustomizationDataProperty            the mCustomizationDataProperty to set
	 */
	public void setCustomizationDataProperty(String pCustomizationDataProperty) {
		this.mCustomizationDataProperty = pCustomizationDataProperty;
	}
	
	/**
	 * Gets the message property.
	 *
	 * @return the mMessageProperty
	 */
	public String getMessageProperty() {
		return mMessageProperty;
	}
	
	/**
	 * Sets the message property.
	 *
	 * @param pMessageProperty            the mMessageProperty to set
	 */
	public void setMessageProperty(String pMessageProperty) {
		this.mMessageProperty = pMessageProperty;
	}
	
	/**
	 * Gets the gift message property.
	 *
	 * @return the mGiftMessageProperty
	 */
	public String getGiftMessageProperty() {
		return mGiftMessageProperty;
	}
	
	/**
	 * Sets the gift message property.
	 *
	 * @param pGiftMessageProperty            the mGiftMessageProperty to set
	 */
	public void setGiftMessageProperty(String pGiftMessageProperty) {
		this.mGiftMessageProperty = pGiftMessageProperty;
	}
	
	/**
	 * Gets the total line item count property.
	 *
	 * @return the mTotalLineItemCountProperty
	 */
	public String getTotalLineItemCountProperty() {
		return mTotalLineItemCountProperty;
	}
	
	/**
	 * Sets the total line item count property.
	 *
	 * @param pTotalLineItemCountProperty            the mTotalLineItemCountProperty to set
	 */
	public void setTotalLineItemCountProperty(String pTotalLineItemCountProperty) {
		this.mTotalLineItemCountProperty = pTotalLineItemCountProperty;
	}
	
	/**
	 * Gets the total shippment count property.
	 *
	 * @return the mTotalShippmentCountProperty
	 */
	public String getTotalShippmentCountProperty() {
		return mTotalShippmentCountProperty;
	}
	
	/**
	 * Sets the total shippment count property.
	 *
	 * @param pTotalShippmentCountProperty            the mTotalShippmentCountProperty to set
	 */
	public void setTotalShippmentCountProperty(
			String pTotalShippmentCountProperty) {
		this.mTotalShippmentCountProperty = pTotalShippmentCountProperty;
	}
	
	/**
	 * Gets the merchandise property.
	 *
	 * @return the mMerchandiseProperty
	 */
	public String getMerchandiseProperty() {
		return mMerchandiseProperty;
	}
	
	/**
	 * Sets the merchandise property.
	 *
	 * @param pMerchandiseProperty            the mMerchandiseProperty to set
	 */
	public void setMerchandiseProperty(String pMerchandiseProperty) {
		this.mMerchandiseProperty = pMerchandiseProperty;
	}
	
	/**
	 * Gets the shipping property.
	 *
	 * @return the mShippingProperty
	 */
	public String getShippingProperty() {
		return mShippingProperty;
	}
	
	/**
	 * Sets the shipping property.
	 *
	 * @param pShippingProperty            the mShippingProperty to set
	 */
	public void setShippingProperty(String pShippingProperty) {
		this.mShippingProperty = pShippingProperty;
	}
	
	/**
	 * Gets the duty property.
	 *
	 * @return the mDutyProperty
	 */
	public String getDutyProperty() {
		return mDutyProperty;
	}
	
	/**
	 * Sets the duty property.
	 *
	 * @param pDutyProperty            the mDutyProperty to set
	 */
	public void setDutyProperty(String pDutyProperty) {
		this.mDutyProperty = pDutyProperty;
	}

	/**
	 * Gets the image fit ratio.
	 *
	 * @return the image fit ratio
	 */
	public String getImageFitRatio() {
		return mImageFitRatio;
	}
	/**
	 * Sets the order details imageFitRatio json element.
	 * 
	 * @param pImageFitRatio order details json element
	 */
	/**
	 * Sets the image fit ratio.
	 *
	 * @param pImageFitRatio the new image fit ratio
	 */
	public void setImageFitRatio(String pImageFitRatio) {
		this.mImageFitRatio = pImageFitRatio;
	}
	
	/**
	 * This is to get the value for orderOMStates.
	 *
	 * @return the orderOMStates value
	 */
	public Map<String, String> getOrderDetailStatusesMap() {
		return mOrderDetailStatusesMap;
	}

	/**
	 * This method to set the pOrderDetailStatusesMap with orderOMStates.
	 *
	 * @param pOrderDetailStatusesMap            to set the orderOMStates to set
	 */
	public void setOrderDetailStatusesMap(
			Map<String, String> pOrderDetailStatusesMap) {
		this.mOrderDetailStatusesMap = pOrderDetailStatusesMap;
	}
	
	/** This holds orderUpdateXSDPackage. */
	private String mOrderUpdateXSDPackage;
	
	/**
	 * This is to get the value for deliveryModeProperty.
	 *
	 * @return the deliveryModeProperty
	 */
	public String getDeliveryModeProperty() {
		return mDeliveryModeProperty;
	}

	/**
	 * Sets the delivery mode property.
	 *
	 * @param pDeliveryModeProperty            the mDeliveryModeProperty to set
	 */
	public void setDeliveryModeProperty(String pDeliveryModeProperty) {
		this.mDeliveryModeProperty = pDeliveryModeProperty;
	}

	/**
	 * This is to get the value for shippmentInfoProperty.
	 *
	 * @return the shippmentInfoProperty
	 */
	public String getShippmentInfoProperty() {
		return mShippmentInfoProperty;
	}
	
	/**
	 * Sets the shippment info property.
	 *
	 * @param pShippmentInfoProperty 					the mShippmentInfoProperty to set
	 */
	public void setShippmentInfoProperty(String pShippmentInfoProperty) {
		this.mShippmentInfoProperty = pShippmentInfoProperty;
	}

	/**
	 * This is to get the value for displayObject.
	 *
	 * @return the displayObject
	 */
	public String getDisplayObjectProperty() {
		return mDisplayObject;
	}

	/**
	 * Sets the display object property.
	 *
	 * @param pDisplayObject 				the pDisplayObject to set
	 */
	public void setDisplayObjectProperty(String pDisplayObject) {
		this.mDisplayObject = pDisplayObject;
	}

	/**
	 * This is to get the value for shippingAddressProperty.
	 *
	 * @return the shippingAddressProperty
	 */
	public String getShippingAddressProperty() {
		return mShippingAddressProperty;
	}
	
	/**
	 * Sets the shipping address property.
	 *
	 * @param pShippingAddressProperty 					the mShippingAddressProperty to set
	 */
	public void setShippingAddressProperty(String pShippingAddressProperty) {
		this.mShippingAddressProperty = pShippingAddressProperty;
	}

	/**
	 * This is to get the value for orderLookupResponseListProperty.
	 *
	 * @return the orderLookupResponseListProperty
	 */
	public String getOrderLookupResponseListProperty() {
		return mOrderLookupResponseListProperty;
	}

	/**
	 * Sets the order lookup response list property.
	 *
	 * @param pOrderLookupResponseListProperty 						the mOrderLookupResponseListProperty to set
	 */
	public void setOrderLookupResponseListProperty(String pOrderLookupResponseListProperty) {
		this.mOrderLookupResponseListProperty = pOrderLookupResponseListProperty;
	}
	
	/**
	 * This is to get the value for orderLookupResponseProperty.
	 *
	 * @return the orderLookupResponseProperty
	 */
	public String getOrderLookupResponseProperty() {
		return mOrderLookupResponseProperty;
	}

	/**
	 * Sets the order lookup response property.
	 *
	 * @param pOrderLookupResponseProperty 					the mOrderLookupResponseProperty to set
	 */
	public void setOrderLookupResponseProperty(String pOrderLookupResponseProperty) {
		this.mOrderLookupResponseProperty = pOrderLookupResponseProperty;
	}
	
	/**
	 * This is to get the value for lineDetailsPropertvalue.
	 *
	 * @return the lineDetailsPropertvalue
	 */
	public String getLineDetailsProperty() {
		return mLineDetailsProperty;
	}

	/**
	 * Sets the line details property.
	 *
	 * @param pLineDetailsProperty 				the mLineDetailsProperty to set
	 */
	public void setLineDetailsProperty(String pLineDetailsProperty) {
		this.mLineDetailsProperty = pLineDetailsProperty;
	}

	/**
	 * This is to get the value for lineDetailProperty.
	 *
	 * @return the lineDetailProperty value
	 */
	public String getLineDetailProperty() {
		return mLineDetailProperty;
	}

	/**
	 * Sets the line detail property.
	 *
	 * @param pLineDetailProperty 				the mLineDetailProperty to set
	 */
	public void setLineDetailProperty(String pLineDetailProperty) {
		this.mLineDetailProperty = pLineDetailProperty;
	}

	/**
	 * This is to get the value for omsOrderDetailLookupRestAPIURL.
	 *
	 * @return the omsOrderDetailLookupRestAPIURL value
	 */
	public String getOmsOrderDetailLookupRestAPIURL() {
		return mOmsOrderDetailLookupRestAPIURL;
	}
	
	/**
	 * Sets the oms order detail lookup rest apiurl.
	 *
	 * @param pOmsOrderDetailLookupRestAPIURL 				the mOmsOrderDetailLookupRestAPIURL to set
	 */
	public void setOmsOrderDetailLookupRestAPIURL(String pOmsOrderDetailLookupRestAPIURL) {
		this.mOmsOrderDetailLookupRestAPIURL = pOmsOrderDetailLookupRestAPIURL;
	}
	
	/**
	 * Gets the oms order lookup rest apiurl.
	 *
	 * @return mOmsOrderLookupRestAPIURL
	 */
	public String getOmsOrderLookupRestAPIURL() {
		return mOmsOrderLookupRestAPIURL;
	}
	
	/**
	 * Sets the oms order lookup rest apiurl.
	 *
	 * @param pOmsOrderLookupRestAPIURL 				the mOmsOrderLookupRestAPIURL to set
	 */
	public void setOmsOrderLookupRestAPIURL(String pOmsOrderLookupRestAPIURL) {
		this.mOmsOrderLookupRestAPIURL = pOmsOrderLookupRestAPIURL;
	}

	/**
	 * Gets the oms cancel order rest apiurl.
	 *
	 * @return mOmsCancelOrderRestAPIURL
	 */
	public String getOmsCancelOrderRestAPIURL() {
		return mOmsCancelOrderRestAPIURL;
	}

	/**
	 * Sets the oms cancel order rest apiurl.
	 *
	 * @param pOmsCancelOrderRestAPIURL 					the mOmsCancelOrderRestAPIURL to set
	 */
	public void setOmsCancelOrderRestAPIURL(String pOmsCancelOrderRestAPIURL) {
		this.mOmsCancelOrderRestAPIURL = pOmsCancelOrderRestAPIURL;
	}

	/**
	 * This is to get the value for orderCreateServiceURL.
	 *
	 * @return the orderCreateServiceURL value
	 */
	public String getOrderCreateServiceURL() {
		return mOrderCreateServiceURL;
	}

	/**
	 * This method to set the pOrderCreateServiceURL with orderCreateServiceURL.
	 *
	 * @param pOrderCreateServiceURL the orderCreateServiceURL to set
	 */
	public void setOrderCreateServiceURL(String pOrderCreateServiceURL) {
		mOrderCreateServiceURL = pOrderCreateServiceURL;
	}

	/**
	 * This is to get the value for orderLookupServiceURL.
	 *
	 * @return the orderLookupServiceURL value
	 */
	public String getOrderLookupServiceURL() {
		return mOrderLookupServiceURL;
	}

	/**
	 * This method to set the pOrderLookupServiceURL with orderLookupServiceURL.
	 *
	 * @param pOrderLookupServiceURL the orderLookupServiceURL to set
	 */
	public void setOrderLookupServiceURL(String pOrderLookupServiceURL) {
		mOrderLookupServiceURL = pOrderLookupServiceURL;
	}

	/**
	 * This is to get the value for orderDetailsLookupServiceURL.
	 *
	 * @return the orderDetailsLookupServiceURL value
	 */
	public String getOrderDetailsLookupServiceURL() {
		return mOrderDetailsLookupServiceURL;
	}

	/**
	 * This method to set the pOrderDetailsLookupServiceURL with orderDetailsLookupServiceURL.
	 *
	 * @param pOrderDetailsLookupServiceURL the orderDetailsLookupServiceURL to set
	 */
	public void setOrderDetailsLookupServiceURL(String pOrderDetailsLookupServiceURL) {
		mOrderDetailsLookupServiceURL = pOrderDetailsLookupServiceURL;
	}

	/**
	 * This is to get the value for orderCancelServiceURL.
	 *
	 * @return the orderCancelServiceURL value
	 */
	public String getOrderCancelServiceURL() {
		return mOrderCancelServiceURL;
	}

	/**
	 * This method to set the pOrderCancelServiceURL with orderCancelServiceURL.
	 *
	 * @param pOrderCancelServiceURL the orderCancelServiceURL to set
	 */
	public void setOrderCancelServiceURL(String pOrderCancelServiceURL) {
		mOrderCancelServiceURL = pOrderCancelServiceURL;
	}

	/**
	 * This is to get the value for orderCreateXSDPackage.
	 *
	 * @return the orderCreateXSDPackage value
	 */
	public String getOrderCreateXSDPackage() {
		return mOrderCreateXSDPackage;
	}

	/**
	 * This method to set the pOrderCreateXSDPackage with orderCreateXSDPackage.
	 *
	 * @param pOrderCreateXSDPackage the orderCreateXSDPackage to set
	 */
	public void setOrderCreateXSDPackage(String pOrderCreateXSDPackage) {
		mOrderCreateXSDPackage = pOrderCreateXSDPackage;
	}

	/**
	 * This is to get the value for orderLookupXSDpackage.
	 *
	 * @return the orderLookupXSDpackage value
	 */
	public String getOrderLookupXSDpackage() {
		return mOrderLookupXSDpackage;
	}

	/**
	 * This method to set the pOrderLookupXSDpackage with orderLookupXSDpackage.
	 *
	 * @param pOrderLookupXSDpackage the orderLookupXSDpackage to set
	 */
	public void setOrderLookupXSDpackage(String pOrderLookupXSDpackage) {
		mOrderLookupXSDpackage = pOrderLookupXSDpackage;
	}

	/**
	 * This is to get the value for orderDetailsLookupXSDPackage.
	 *
	 * @return the orderDetailsLookupXSDPackage value
	 */
	public String getOrderDetailsLookupXSDPackage() {
		return mOrderDetailsLookupXSDPackage;
	}

	/**
	 * This method to set the pOrderDetailsLookupXSDPackage with orderDetailsLookupXSDPackage.
	 *
	 * @param pOrderDetailsLookupXSDPackage the orderDetailsLookupXSDPackage to set
	 */
	public void setOrderDetailsLookupXSDPackage(String pOrderDetailsLookupXSDPackage) {
		mOrderDetailsLookupXSDPackage = pOrderDetailsLookupXSDPackage;
	}

	/**
	 * This is to get the value for orderCancelXSDPath.
	 *
	 * @return the orderCancelXSDPath value
	 */
	public String getOrderCancelXSDPath() {
		return mOrderCancelXSDPath;
	}

	/**
	 * This method to set the pOrderCancelXSDPath with orderCancelXSDPath.
	 *
	 * @param pOrderCancelXSDPath the orderCancelXSDPath to set
	 */
	public void setOrderCancelXSDPath(String pOrderCancelXSDPath) {
		mOrderCancelXSDPath = pOrderCancelXSDPath;
	}

	/**
	 * This is to get the value for apiKey.
	 *
	 * @return the apiKey value
	 */
	public String getApiKey() {
		return mApiKey;
	}

	/**
	 * This method to set the pApiKey with apiKey.
	 *
	 * @param pApiKey the apiKey to set
	 */
	public void setApiKey(String pApiKey) {
		mApiKey = pApiKey;
	}

	/**
	 * This is to get the value for apiKeyValue.
	 *
	 * @return the apiKeyValue value
	 */
	public String getApiKeyValue() {
		return mApiKeyValue;
	}

	/**
	 * This method to set the pApiKeyValue with apiKeyValue.
	 *
	 * @param pApiKeyValue the apiKeyValue to set
	 */
	public void setApiKeyValue(String pApiKeyValue) {
		mApiKeyValue = pApiKeyValue;
	}

	/**
	 * This is to get the value for httpPostMethod.
	 *
	 * @return the httpPostMethod value
	 */
	public String getHttpPostMethod() {
		return mHttpPostMethod;
	}

	/**
	 * This method to set the pHttpPostMethod with httpPostMethod.
	 *
	 * @param pHttpPostMethod the httpPostMethod to set
	 */
	public void setHttpPostMethod(String pHttpPostMethod) {
		mHttpPostMethod = pHttpPostMethod;
	}

	/**
	 * This is to get the value for httpContentType.
	 *
	 * @return the httpContentType value
	 */
	public String getHttpContentType() {
		return mHttpContentType;
	}

	/**
	 * This method to set the pHttpContentType with httpContentType.
	 *
	 * @param pHttpContentType the httpContentType to set
	 */
	public void setHttpContentType(String pHttpContentType) {
		mHttpContentType = pHttpContentType;
	}

	/**
	 * This is to get the value for httpApplicationType.
	 *
	 * @return the httpApplicationType value
	 */
	public String getHttpApplicationType() {
		return mHttpApplicationType;
	}

	/**
	 * This method to set the pHttpApplicationType with httpApplicationType.
	 *
	 * @param pHttpApplicationType the httpApplicationType to set
	 */
	public void setHttpApplicationType(String pHttpApplicationType) {
		mHttpApplicationType = pHttpApplicationType;
	}

	/**
	 * This is to get the value for jaxbOutputFormat.
	 *
	 * @return the jaxbOutputFormat value
	 */
	public String getJaxbOutputFormat() {
		return mJaxbOutputFormat;
	}

	/**
	 * This method to set the pJaxbOutputFormat with jaxbOutputFormat.
	 *
	 * @param pJaxbOutputFormat the jaxbOutputFormat to set
	 */
	public void setJaxbOutputFormat(String pJaxbOutputFormat) {
		mJaxbOutputFormat = pJaxbOutputFormat;
	}

	/**
	 * This is to get the value for orderTaxErrorCustomAttributeName.
	 *
	 * @return the orderTaxErrorCustomAttributeName value
	 */
	public String getOrderTaxErrorCustomAttributeName() {
		return mOrderTaxErrorCustomAttributeName;
	}

	/**
	 * This method to set the pOrderTaxErrorCustomAttributeName with orderTaxErrorCustomAttributeName.
	 *
	 * @param pOrderTaxErrorCustomAttributeName the orderTaxErrorCustomAttributeName to set
	 */
	public void setOrderTaxErrorCustomAttributeName(
			String pOrderTaxErrorCustomAttributeName) {
		mOrderTaxErrorCustomAttributeName = pOrderTaxErrorCustomAttributeName;
	}

	/**
	 * This is to get the value for customAttributeValueYes.
	 *
	 * @return the customAttributeValueYes value
	 */
	public String getCustomAttributeValueYes() {
		return mCustomAttributeValueYes;
	}

	/**
	 * This method to set the pCustomAttributeValueYes with customAttributeValueYes.
	 *
	 * @param pCustomAttributeValueYes the customAttributeValueYes to set
	 */
	public void setCustomAttributeValueYes(String pCustomAttributeValueYes) {
		mCustomAttributeValueYes = pCustomAttributeValueYes;
	}

	/**
	 * This is to get the value for customAttributeValueNo.
	 *
	 * @return the customAttributeValueNo value
	 */
	public String getCustomAttributeValueNo() {
		return mCustomAttributeValueNo;
	}

	/**
	 * This method to set the pCustomAttributeValueNo with customAttributeValueNo.
	 *
	 * @param pCustomAttributeValueNo the customAttributeValueNo to set
	 */
	public void setCustomAttributeValueNo(String pCustomAttributeValueNo) {
		mCustomAttributeValueNo = pCustomAttributeValueNo;
	}

	/**
	 * This is to get the value for sourceIdType.
	 *
	 * @return the sourceIdType value
	 */
	public String getSourceIdType() {
		return mSourceIdType;
	}

	/**
	 * This method to set the pSourceIdType with sourceIdType.
	 *
	 * @param pSourceIdType the sourceIdType to set
	 */
	public void setSourceIdType(String pSourceIdType) {
		mSourceIdType = pSourceIdType;
	}

	/**
	 * This is to get the value for sourceIdValue.
	 *
	 * @return the sourceIdValue value
	 */
	public String getSourceIdValue() {
		return mSourceIdValue;
	}

	/**
	 * This method to set the pSourceIdValue with sourceIdValue.
	 *
	 * @param pSourceIdValue the sourceIdValue to set
	 */
	public void setSourceIdValue(String pSourceIdValue) {
		mSourceIdValue = pSourceIdValue;
	}

	/**
	 * This is to get the value for currencyUSD.
	 *
	 * @return the currencyUSD value
	 */
	public String getCurrencyUSD() {
		return mCurrencyUSD;
	}

	/**
	 * This method to set the pCurrencyUSD with currencyUSD.
	 *
	 * @param pCurrencyUSD the currencyUSD to set
	 */
	public void setCurrencyUSD(String pCurrencyUSD) {
		mCurrencyUSD = pCurrencyUSD;
	}

	/**
	 * This is to get the value for timeZoneEST.
	 *
	 * @return the timeZoneEST value
	 */
	public String getTimeZoneEST() {
		return mTimeZoneEST;
	}

	/**
	 * This method to set the pTimeZoneEST with timeZoneEST.
	 *
	 * @param pTimeZoneEST the timeZoneEST to set
	 */
	public void setTimeZoneEST(String pTimeZoneEST) {
		mTimeZoneEST = pTimeZoneEST;
	}

	/**
	 * This is to get the value for orderCancelReasonCode.
	 *
	 * @return the orderCancelReasonCode value
	 */
	public String getOrderCancelReasonCode() {
		return mOrderCancelReasonCode;
	}

	/**
	 * This method to set the pOrderCancelReasonCode with orderCancelReasonCode.
	 *
	 * @param pOrderCancelReasonCode the orderCancelReasonCode to set
	 */
	public void setOrderCancelReasonCode(String pOrderCancelReasonCode) {
		mOrderCancelReasonCode = pOrderCancelReasonCode;
	}

	/**
	 * This is to get the value for orderCancelReason.
	 *
	 * @return the orderCancelReason value
	 */
	public String getOrderCancelReason() {
		return mOrderCancelReason;
	}

	/**
	 * This method to set the pOrderCancelReason with orderCancelReason.
	 *
	 * @param pOrderCancelReason the orderCancelReason to set
	 */
	public void setOrderCancelReason(String pOrderCancelReason) {
		mOrderCancelReason = pOrderCancelReason;
	}

	/**
	 * This is to get the value for tenderTypeMap.
	 *
	 * @return the tenderTypeMap value
	 */
	public Map<String, String> getTenderTypeMap() {
		return mTenderTypeMap;
	}

	/**
	 * This method to set the pTenderTypeMap with tenderTypeMap.
	 *
	 * @param pTenderTypeMap the tenderTypeMap to set
	 */
	public void setTenderTypeMap(Map<String, String> pTenderTypeMap) {
		mTenderTypeMap = pTenderTypeMap;
	}

	/**
	 * This is to get the value for zeroDollarTenderTypeKey.
	 *
	 * @return the zeroDollarTenderTypeKey value
	 */
	public String getZeroDollarTenderTypeKey() {
		return mZeroDollarTenderTypeKey;
	}

	/**
	 * This method to set the pZeroDollarTenderTypeKey with zeroDollarTenderTypeKey.
	 *
	 * @param pZeroDollarTenderTypeKey the zeroDollarTenderTypeKey to set
	 */
	public void setZeroDollarTenderTypeKey(String pZeroDollarTenderTypeKey) {
		mZeroDollarTenderTypeKey = pZeroDollarTenderTypeKey;
	}

	/**
	 * This is to get the value for paypalPaymentAccountUniqueId.
	 *
	 * @return the paypalPaymentAccountUniqueId value
	 */
	public String getPaypalPaymentAccountUniqueId() {
		return mPaypalPaymentAccountUniqueId;
	}

	/**
	 * This method to set the pPaypalPaymentAccountUniqueId with paypalPaymentAccountUniqueId.
	 *
	 * @param pPaypalPaymentAccountUniqueId the paypalPaymentAccountUniqueId to set
	 */
	public void setPaypalPaymentAccountUniqueId(String pPaypalPaymentAccountUniqueId) {
		mPaypalPaymentAccountUniqueId = pPaypalPaymentAccountUniqueId;
	}

	/**
	 * This is to get the value for addressTypeBilling.
	 *
	 * @return the addressTypeBilling value
	 */
	public String getAddressTypeBilling() {
		return mAddressTypeBilling;
	}

	/**
	 * This method to set the pAddressTypeBilling with addressTypeBilling.
	 *
	 * @param pAddressTypeBilling the addressTypeBilling to set
	 */
	public void setAddressTypeBilling(String pAddressTypeBilling) {
		mAddressTypeBilling = pAddressTypeBilling;
	}

	/**
	 * This is to get the value for locale.
	 *
	 * @return the locale value
	 */
	public String getLocale() {
		return mLocale;
	}

	/**
	 * This method to set the pLocale with locale.
	 *
	 * @param pLocale the locale to set
	 */
	public void setLocale(String pLocale) {
		mLocale = pLocale;
	}

	/**
	 * This is to get the value for source.
	 *
	 * @return the source value
	 */
	public String getSource() {
		return mSource;
	}

	/**
	 * This method to set the pSource with source.
	 *
	 * @param pSource the source to set
	 */
	public void setSource(String pSource) {
		mSource = pSource;
	}

	/**
	 * This is to get the value for addressTypeShipping.
	 *
	 * @return the addressTypeShipping value
	 */
	public String getAddressTypeShipping() {
		return mAddressTypeShipping;
	}

	/**
	 * This method to set the pAddressTypeShipping with addressTypeShipping.
	 *
	 * @param pAddressTypeShipping the addressTypeShipping to set
	 */
	public void setAddressTypeShipping(String pAddressTypeShipping) {
		mAddressTypeShipping = pAddressTypeShipping;
	}

	/**
	 * This is to get the value for addressTypeStoreFront.
	 *
	 * @return the addressTypeStoreFront value
	 */
	public String getAddressTypeStoreFront() {
		return mAddressTypeStoreFront;
	}

	/**
	 * This method to set the pAddressTypeStoreFront with addressTypeStoreFront.
	 *
	 * @param pAddressTypeStoreFront the addressTypeStoreFront to set
	 */
	public void setAddressTypeStoreFront(String pAddressTypeStoreFront) {
		mAddressTypeStoreFront = pAddressTypeStoreFront;
	}

	/**
	 * This is to get the value for orderCustomAttributeGiftwrapAmt.
	 *
	 * @return the orderCustomAttributeGiftwrapAmt value
	 */
	public String getOrderCustomAttributeGiftwrapAmt() {
		return mOrderCustomAttributeGiftwrapAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeGiftwrapAmt with orderCustomAttributeGiftwrapAmt.
	 *
	 * @param pOrderCustomAttributeGiftwrapAmt the orderCustomAttributeGiftwrapAmt to set
	 */
	public void setOrderCustomAttributeGiftwrapAmt(
			String pOrderCustomAttributeGiftwrapAmt) {
		mOrderCustomAttributeGiftwrapAmt = pOrderCustomAttributeGiftwrapAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributePromoSavingsAmt.
	 *
	 * @return the orderCustomAttributePromoSavingsAmt value
	 */
	public String getOrderCustomAttributePromoSavingsAmt() {
		return mOrderCustomAttributePromoSavingsAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributePromoSavingsAmt with orderCustomAttributePromoSavingsAmt.
	 *
	 * @param pOrderCustomAttributePromoSavingsAmt the orderCustomAttributePromoSavingsAmt to set
	 */
	public void setOrderCustomAttributePromoSavingsAmt(
			String pOrderCustomAttributePromoSavingsAmt) {
		mOrderCustomAttributePromoSavingsAmt = pOrderCustomAttributePromoSavingsAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeShipAmt.
	 *
	 * @return the orderCustomAttributeShipAmt value
	 */
	public String getOrderCustomAttributeShipAmt() {
		return mOrderCustomAttributeShipAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeShipAmt with orderCustomAttributeShipAmt.
	 *
	 * @param pOrderCustomAttributeShipAmt the orderCustomAttributeShipAmt to set
	 */
	public void setOrderCustomAttributeShipAmt(String pOrderCustomAttributeShipAmt) {
		mOrderCustomAttributeShipAmt = pOrderCustomAttributeShipAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeShipSurAmt.
	 *
	 * @return the orderCustomAttributeShipSurAmt value
	 */
	public String getOrderCustomAttributeShipSurAmt() {
		return mOrderCustomAttributeShipSurAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeShipSurAmt with orderCustomAttributeShipSurAmt.
	 *
	 * @param pOrderCustomAttributeShipSurAmt the orderCustomAttributeShipSurAmt to set
	 */
	public void setOrderCustomAttributeShipSurAmt(
			String pOrderCustomAttributeShipSurAmt) {
		mOrderCustomAttributeShipSurAmt = pOrderCustomAttributeShipSurAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeSalesTaxAmt.
	 *
	 * @return the orderCustomAttributeSalesTaxAmt value
	 */
	public String getOrderCustomAttributeSalesTaxAmt() {
		return mOrderCustomAttributeSalesTaxAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeSalesTaxAmt with orderCustomAttributeSalesTaxAmt.
	 *
	 * @param pOrderCustomAttributeSalesTaxAmt the orderCustomAttributeSalesTaxAmt to set
	 */
	public void setOrderCustomAttributeSalesTaxAmt(
			String pOrderCustomAttributeSalesTaxAmt) {
		mOrderCustomAttributeSalesTaxAmt = pOrderCustomAttributeSalesTaxAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeLocalTaxAmt.
	 *
	 * @return the orderCustomAttributeLocalTaxAmt value
	 */
	public String getOrderCustomAttributeLocalTaxAmt() {
		return mOrderCustomAttributeLocalTaxAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeLocalTaxAmt with orderCustomAttributeLocalTaxAmt.
	 *
	 * @param pOrderCustomAttributeLocalTaxAmt the orderCustomAttributeLocalTaxAmt to set
	 */
	public void setOrderCustomAttributeLocalTaxAmt(
			String pOrderCustomAttributeLocalTaxAmt) {
		mOrderCustomAttributeLocalTaxAmt = pOrderCustomAttributeLocalTaxAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeIslandTaxAmt.
	 *
	 * @return the orderCustomAttributeIslandTaxAmt value
	 */
	public String getOrderCustomAttributeIslandTaxAmt() {
		return mOrderCustomAttributeIslandTaxAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeIslandTaxAmt with orderCustomAttributeIslandTaxAmt.
	 *
	 * @param pOrderCustomAttributeIslandTaxAmt the orderCustomAttributeIslandTaxAmt to set
	 */
	public void setOrderCustomAttributeIslandTaxAmt(
			String pOrderCustomAttributeIslandTaxAmt) {
		mOrderCustomAttributeIslandTaxAmt = pOrderCustomAttributeIslandTaxAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeEwasteAmt.
	 *
	 * @return the orderCustomAttributeEwasteAmt value
	 */
	public String getOrderCustomAttributeEwasteAmt() {
		return mOrderCustomAttributeEwasteAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeEwasteAmt with orderCustomAttributeEwasteAmt.
	 *
	 * @param pOrderCustomAttributeEwasteAmt the orderCustomAttributeEwasteAmt to set
	 */
	public void setOrderCustomAttributeEwasteAmt(
			String pOrderCustomAttributeEwasteAmt) {
		mOrderCustomAttributeEwasteAmt = pOrderCustomAttributeEwasteAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeMattFeeAmt.
	 *
	 * @return the orderCustomAttributeMattFeeAmt value
	 */
	public String getOrderCustomAttributeMattFeeAmt() {
		return mOrderCustomAttributeMattFeeAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeMattFeeAmt with orderCustomAttributeMattFeeAmt.
	 *
	 * @param pOrderCustomAttributeMattFeeAmt the orderCustomAttributeMattFeeAmt to set
	 */
	public void setOrderCustomAttributeMattFeeAmt(
			String pOrderCustomAttributeMattFeeAmt) {
		mOrderCustomAttributeMattFeeAmt = pOrderCustomAttributeMattFeeAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributePifFeeAmt.
	 *
	 * @return the orderCustomAttributePifFeeAmt value
	 */
	public String getOrderCustomAttributePifFeeAmt() {
		return mOrderCustomAttributePifFeeAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributePifFeeAmt with orderCustomAttributePifFeeAmt.
	 *
	 * @param pOrderCustomAttributePifFeeAmt the orderCustomAttributePifFeeAmt to set
	 */
	public void setOrderCustomAttributePifFeeAmt(
			String pOrderCustomAttributePifFeeAmt) {
		mOrderCustomAttributePifFeeAmt = pOrderCustomAttributePifFeeAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeGiftCardAmt.
	 *
	 * @return the orderCustomAttributeGiftCardAmt value
	 */
	public String getOrderCustomAttributeGiftCardAmt() {
		return mOrderCustomAttributeGiftCardAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeGiftCardAmt with orderCustomAttributeGiftCardAmt.
	 *
	 * @param pOrderCustomAttributeGiftCardAmt the orderCustomAttributeGiftCardAmt to set
	 */
	public void setOrderCustomAttributeGiftCardAmt(
			String pOrderCustomAttributeGiftCardAmt) {
		mOrderCustomAttributeGiftCardAmt = pOrderCustomAttributeGiftCardAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeOrderTotal.
	 *
	 * @return the orderCustomAttributeOrderTotal value
	 */
	public String getOrderCustomAttributeOrderTotal() {
		return mOrderCustomAttributeOrderTotal;
	}

	/**
	 * This method to set the pOrderCustomAttributeOrderTotal with orderCustomAttributeOrderTotal.
	 *
	 * @param pOrderCustomAttributeOrderTotal the orderCustomAttributeOrderTotal to set
	 */
	public void setOrderCustomAttributeOrderTotal(
			String pOrderCustomAttributeOrderTotal) {
		mOrderCustomAttributeOrderTotal = pOrderCustomAttributeOrderTotal;
	}

	/**
	 * This is to get the value for orderCustomAttributeSubtotal.
	 *
	 * @return the orderCustomAttributeSubtotal value
	 */
	public String getOrderCustomAttributeSubtotal() {
		return mOrderCustomAttributeSubtotal;
	}

	/**
	 * This method to set the pOrderCustomAttributeSubtotal with orderCustomAttributeSubtotal.
	 *
	 * @param pOrderCustomAttributeSubtotal the orderCustomAttributeSubtotal to set
	 */
	public void setOrderCustomAttributeSubtotal(String pOrderCustomAttributeSubtotal) {
		mOrderCustomAttributeSubtotal = pOrderCustomAttributeSubtotal;
	}

	/**
	 * This is to get the value for orderCustomAttributeBalanceAmt.
	 *
	 * @return the orderCustomAttributeBalanceAmt value
	 */
	public String getOrderCustomAttributeBalanceAmt() {
		return mOrderCustomAttributeBalanceAmt;
	}

	/**
	 * This method to set the pOrderCustomAttributeBalanceAmt with orderCustomAttributeBalanceAmt.
	 *
	 * @param pOrderCustomAttributeBalanceAmt the orderCustomAttributeBalanceAmt to set
	 */
	public void setOrderCustomAttributeBalanceAmt(
			String pOrderCustomAttributeBalanceAmt) {
		mOrderCustomAttributeBalanceAmt = pOrderCustomAttributeBalanceAmt;
	}

	/**
	 * This is to get the value for orderCustomAttributeLoyaltyAccount.
	 *
	 * @return the orderCustomAttributeLoyaltyAccount value
	 */
	public String getOrderCustomAttributeLoyaltyAccount() {
		return mOrderCustomAttributeLoyaltyAccount;
	}

	/**
	 * This method to set the pOrderCustomAttributeLoyaltyAccount with orderCustomAttributeLoyaltyAccount.
	 *
	 * @param pOrderCustomAttributeLoyaltyAccount the orderCustomAttributeLoyaltyAccount to set
	 */
	public void setOrderCustomAttributeLoyaltyAccount(
			String pOrderCustomAttributeLoyaltyAccount) {
		mOrderCustomAttributeLoyaltyAccount = pOrderCustomAttributeLoyaltyAccount;
	}

	/**
	 * This is to get the value for orderCustomAttributeLoyaltyProgram.
	 *
	 * @return the orderCustomAttributeLoyaltyProgram value
	 */
	public String getOrderCustomAttributeLoyaltyProgram() {
		return mOrderCustomAttributeLoyaltyProgram;
	}

	/**
	 * This method to set the pOrderCustomAttributeLoyaltyProgram with orderCustomAttributeLoyaltyProgram.
	 *
	 * @param pOrderCustomAttributeLoyaltyProgram the orderCustomAttributeLoyaltyProgram to set
	 */
	public void setOrderCustomAttributeLoyaltyProgram(
			String pOrderCustomAttributeLoyaltyProgram) {
		mOrderCustomAttributeLoyaltyProgram = pOrderCustomAttributeLoyaltyProgram;
	}

	/**
	 * This is to get the value for orderCustomAttributeLoyaltyProgramValue.
	 *
	 * @return the orderCustomAttributeLoyaltyProgramValue value
	 */
	public String getOrderCustomAttributeLoyaltyProgramValue() {
		return mOrderCustomAttributeLoyaltyProgramValue;
	}

	/**
	 * This method to set the pOrderCustomAttributeLoyaltyProgramValue with orderCustomAttributeLoyaltyProgramValue.
	 *
	 * @param pOrderCustomAttributeLoyaltyProgramValue the orderCustomAttributeLoyaltyProgramValue to set
	 */
	public void setOrderCustomAttributeLoyaltyProgramValue(
			String pOrderCustomAttributeLoyaltyProgramValue) {
		mOrderCustomAttributeLoyaltyProgramValue = pOrderCustomAttributeLoyaltyProgramValue;
	}

	/**
	 * This is to get the value for orderCustomAttributeGFSOrderId.
	 *
	 * @return the orderCustomAttributeGFSOrderId value
	 */
	public String getOrderCustomAttributeGFSOrderId() {
		return mOrderCustomAttributeGFSOrderId;
	}

	/**
	 * This method to set the pOrderCustomAttributeGFSOrderId with orderCustomAttributeGFSOrderId.
	 *
	 * @param pOrderCustomAttributeGFSOrderId the orderCustomAttributeGFSOrderId to set
	 */
	public void setOrderCustomAttributeGFSOrderId(
			String pOrderCustomAttributeGFSOrderId) {
		mOrderCustomAttributeGFSOrderId = pOrderCustomAttributeGFSOrderId;
	}

	/**
	 * This is to get the value for orderCustomAttributeLayaway.
	 *
	 * @return the orderCustomAttributeLayaway value
	 */
	public String getOrderCustomAttributeLayaway() {
		return mOrderCustomAttributeLayaway;
	}

	/**
	 * This method to set the pOrderCustomAttributeLayaway with orderCustomAttributeLayaway.
	 *
	 * @param pOrderCustomAttributeLayaway the orderCustomAttributeLayaway to set
	 */
	public void setOrderCustomAttributeLayaway(String pOrderCustomAttributeLayaway) {
		mOrderCustomAttributeLayaway = pOrderCustomAttributeLayaway;
	}

	/**
	 * This is to get the value for orderCustomAttributeLayawayValue.
	 *
	 * @return the orderCustomAttributeLayawayValue value
	 */
	public String getOrderCustomAttributeLayawayValue() {
		return mOrderCustomAttributeLayawayValue;
	}

	/**
	 * This method to set the pOrderCustomAttributeLayawayValue with orderCustomAttributeLayawayValue.
	 *
	 * @param pOrderCustomAttributeLayawayValue the orderCustomAttributeLayawayValue to set
	 */
	public void setOrderCustomAttributeLayawayValue(
			String pOrderCustomAttributeLayawayValue) {
		mOrderCustomAttributeLayawayValue = pOrderCustomAttributeLayawayValue;
	}

	/**
	 * This is to get the value for orderCustomAttributeLayawayLocId.
	 *
	 * @return the orderCustomAttributeLayawayLocId value
	 */
	public String getOrderCustomAttributeLayawayLocId() {
		return mOrderCustomAttributeLayawayLocId;
	}

	/**
	 * This method to set the pOrderCustomAttributeLayawayLocId with orderCustomAttributeLayawayLocId.
	 *
	 * @param pOrderCustomAttributeLayawayLocId the orderCustomAttributeLayawayLocId to set
	 */
	public void setOrderCustomAttributeLayawayLocId(
			String pOrderCustomAttributeLayawayLocId) {
		mOrderCustomAttributeLayawayLocId = pOrderCustomAttributeLayawayLocId;
	}

	/**
	 * This is to get the value for displayObject.
	 *
	 * @return the displayObject value
	 */
	public String getDisplayObject() {
		return mDisplayObject;
	}

	/**
	 * This method to set the pDisplayObject with displayObject.
	 *
	 * @param pDisplayObject the displayObject to set
	 */
	public void setDisplayObject(String pDisplayObject) {
		mDisplayObject = pDisplayObject;
	}

	/**
	 * This is to get the value for customizationDataGiftMessageType.
	 *
	 * @return the customizationDataGiftMessageType value
	 */
	public String getCustomizationDataGiftMessageType() {
		return mCustomizationDataGiftMessageType;
	}

	/**
	 * This method to set the pCustomizationDataGiftMessageType with customizationDataGiftMessageType.
	 *
	 * @param pCustomizationDataGiftMessageType the customizationDataGiftMessageType to set
	 */
	public void setCustomizationDataGiftMessageType(
			String pCustomizationDataGiftMessageType) {
		mCustomizationDataGiftMessageType = pCustomizationDataGiftMessageType;
	}

	/**
	 * This is to get the value for customizationDataRegistryMessageType.
	 *
	 * @return the customizationDataRegistryMessageType value
	 */
	public String getCustomizationDataRegistryMessageType() {
		return mCustomizationDataRegistryMessageType;
	}

	/**
	 * This method to set the pCustomizationDataRegistryMessageType with customizationDataRegistryMessageType.
	 *
	 * @param pCustomizationDataRegistryMessageType the customizationDataRegistryMessageType to set
	 */
	public void setCustomizationDataRegistryMessageType(
			String pCustomizationDataRegistryMessageType) {
		mCustomizationDataRegistryMessageType = pCustomizationDataRegistryMessageType;
	}

	/**
	 * This is to get the value for customizationDataGiftWrapType.
	 *
	 * @return the customizationDataGiftWrapType value
	 */
	public String getCustomizationDataGiftWrapType() {
		return mCustomizationDataGiftWrapType;
	}

	/**
	 * This method to set the pCustomizationDataGiftWrapType with customizationDataGiftWrapType.
	 *
	 * @param pCustomizationDataGiftWrapType the customizationDataGiftWrapType to set
	 */
	public void setCustomizationDataGiftWrapType(
			String pCustomizationDataGiftWrapType) {
		mCustomizationDataGiftWrapType = pCustomizationDataGiftWrapType;
	}

	/**
	 * This is to get the value for customizationDataBPPType.
	 *
	 * @return the customizationDataBPPType value
	 */
	public String getCustomizationDataBPPType() {
		return mCustomizationDataBPPType;
	}

	/**
	 * This method to set the pCustomizationDataBPPType with customizationDataBPPType.
	 *
	 * @param pCustomizationDataBPPType the customizationDataBPPType to set
	 */
	public void setCustomizationDataBPPType(String pCustomizationDataBPPType) {
		mCustomizationDataBPPType = pCustomizationDataBPPType;
	}

	/**
	 * This is to get the value for onlinePID.
	 *
	 * @return the onlinePID value
	 */
	public String getOnlinePID() {
		return mOnlinePID;
	}

	/**
	 * This method to set the pOnlinePID with onlinePID.
	 *
	 * @param pOnlinePID the onlinePID to set
	 */
	public void setOnlinePID(String pOnlinePID) {
		mOnlinePID = pOnlinePID;
	}

	/**
	 * This is to get the value for imageWidth.
	 *
	 * @return the imageWidth value
	 */
	public String getImageWidth() {
		return mImageWidth;
	}

	/**
	 * This method to set the pImageWidth with imageWidth.
	 *
	 * @param pImageWidth the imageWidth to set
	 */
	public void setImageWidth(String pImageWidth) {
		mImageWidth = pImageWidth;
	}

	/**
	 * This is to get the value for imageHeight.
	 *
	 * @return the imageHeight value
	 */
	public String getImageHeight() {
		return mImageHeight;
	}

	/**
	 * This method to set the pImageHeight with imageHeight.
	 *
	 * @param pImageHeight the imageHeight to set
	 */
	public void setImageHeight(String pImageHeight) {
		mImageHeight = pImageHeight;
	}
	
	/**
	 * This is to get the value for InvoiceId.
	 *
	 * @return the InvoiceIdProperty value
	 */
	public String getInvoiceIdProperty() {
		return mInvoiceIdProperty;
	}

	/**
	 * This method to set the mInvoiceIdProperty with pInvoiceIdProperty.
	 *
	 * @param pInvoiceIdProperty the invoiceIdProperty to set
	 */
	public void setInvoiceIdProperty(String pInvoiceIdProperty) {
		this.mInvoiceIdProperty = pInvoiceIdProperty;
	}

	/**
	 * This is to get the value for displayOrderArrayProperty.
	 *
	 * @return the displayOrderArrayProperty value
	 */
	public String getDisplayOrderArrayProperty() {
		return mDisplayOrderArrayProperty;
	}

	/**
	 * This method to set the mDisplayOrderArrayProperty with pInvoiceIdProperty.
	 *
	 * @param pDisplayOrderArrayProperty the mDisplayOrderArrayProperty to set
	 */
	public void setDisplayOrderArrayProperty(String pDisplayOrderArrayProperty) {
		this.mDisplayOrderArrayProperty = pDisplayOrderArrayProperty;
	}

	/**
	 * This is to get the value for mInvoiceProperty.
	 *
	 * @return the mInvoiceProperty value
	 */
	public String getInvoiceProperty() {
		return mInvoiceProperty;
	}

	/**
	 * This method to set the mInvoiceProperty with pInvoiceProperty.
	 *
	 * @param pInvoiceProperty the mInvoiceProperty to set
	 */
	public void setInvoiceProperty(String pInvoiceProperty) {
		this.mInvoiceProperty = pInvoiceProperty;
	}

	/**
	 * This is to get the value for mOrderHeaderProperty.
	 *
	 * @return the mOrderHeaderProperty value
	 */
	public String getOrderHeaderProperty() {
		return mOrderHeaderProperty;
	}

	/**
	 * This method to set the mOrderHeaderProperty with pOrderHeaderProperty.
	 *
	 * @param pOrderHeaderProperty the mOrderHeaderProperty to set
	 */
	public void setorderHeaderProperty(String pOrderHeaderProperty) {
		this.mOrderHeaderProperty = pOrderHeaderProperty;
	}

	/**
	 * This is to get the value for mInvoiceTotalAmountsProperty.
	 *
	 * @return the mInvoiceTotalAmountsProperty value
	 */
	public String getInvoiceTotalAmountsProperty() {
		return mInvoiceTotalAmountsProperty;
	}

	/**
	 * This method to set the mInvoiceTotalAmountsProperty with pInvoiceTotalAmountsProperty.
	 *
	 * @param pInvoiceTotalAmountsProperty the mInvoiceTotalAmountsProperty to set
	 */
	public void setInvoiceTotalAmountsProperty(String pInvoiceTotalAmountsProperty) {
		this.mInvoiceTotalAmountsProperty = pInvoiceTotalAmountsProperty;
	}

	/**
	 * This is to get the value for orderUpdateXSDPackage.
	 *
	 * @return the orderUpdateXSDPackage value
	 */
	public String getOrderUpdateXSDPackage() {
		return mOrderUpdateXSDPackage;
	}

	/**
	 * This method to set the pOrderUpdateXSDPackage with orderUpdateXSDPackage.
	 *
	 * @param pOrderUpdateXSDPackage the orderUpdateXSDPackage to set
	 */
	public void setOrderUpdateXSDPackage(String pOrderUpdateXSDPackage) {
		mOrderUpdateXSDPackage = pOrderUpdateXSDPackage;
	}

	/**
	 * This is to get the value for customerLookupResponse.
	 *
	 * @return the customerLookupResponse value
	 */
	public String getCustomerLookupResponseProperty() {
		return mCustomerLookupResponseProperty;
	}

	/**
	 * This method to set the pCustomerLookupResponseProperty with
	 * mCustomerLookupResponseProperty.
	 *
	 * @param pCustomerLookupResponseProperty            the customerLookupResponseProperty to set
	 */
	public void setCustomerLookupResponseProperty(String pCustomerLookupResponseProperty) {
		mCustomerLookupResponseProperty = pCustomerLookupResponseProperty;
	}

	/**
	 * This is to get the value for orderListProperty.
	 *
	 * @return the orderListProperty value
	 */
	public String getOrderListProperty() {
		return mOrderListProperty;
	}

	/**
	 * This method to set the mOrderListProperty with pOrderListProperty.
	 *
	 * @param pOrderListProperty            the orderListProperty to set
	 */
	public void setOrderListProperty(String pOrderListProperty) {
		mOrderListProperty = pOrderListProperty;
	}

	/**
	 * This is to get the value for orderStatusProperty.
	 *
	 * @return the orderStatusProperty value
	 */
	public String getOrderStatusProperty() {
		return mOrderStatusProperty;
	}

	/**
	 * This method to set the mOrderStatusProperty with pOrderStatusProperty.
	 *
	 * @param pOrderStatusProperty            the orderStatusProperty to set
	 */
	public void setOrderStatusProperty(String pOrderStatusProperty) {
		mOrderStatusProperty = pOrderStatusProperty;
	}

	/**
	 * This is to get the value for orderProperty.
	 *
	 * @return the orderProperty value
	 */
	public String getOrderProperty() {
		return mOrderProperty;
	}

	/**
	 * This method to set the mOrderProperty with pOrderProperty.
	 *
	 * @param pOrderProperty            the orderProperty to set
	 */
	public void setOrderProperty(String pOrderProperty) {
		mOrderProperty = pOrderProperty;
	}
	
	
	/** The AddressTypeProxy. */
	private String mAddressTypeProxy;

	/**
	 * Gets the AddressTypeProxy.
	 *
	 * @return the AddressTypeProxy
	 */
	public String getAddressTypeProxy() {
		return mAddressTypeProxy;
	}

	/**
	 * Sets the AddressTypeProxy.
	 *
	 * @param pAddressTypeProxy the new AddressTypeProxy
	 */
	public void setAddressTypeProxy(String pAddressTypeProxy) {
		this.mAddressTypeProxy = pAddressTypeProxy;
	}
	
	/**
	 * Gets the transaction id poperty name.
	 *
	 * @return the transactionIdPopertyName
	 */
	public String getTransactionIdPopertyName() {
		return mTransactionIdPopertyName;
	}

	/**
	 * Sets the transaction id poperty name.
	 *
	 * @param pTransactionIdPopertyName            the transactionIdPopertyName to set
	 */
	public void setTransactionIdPopertyName(String pTransactionIdPopertyName) {
		mTransactionIdPopertyName = pTransactionIdPopertyName;
	}

	/**
	 * Gets the request poperty name.
	 *
	 * @return the requestPopertyName
	 */
	public String getRequestPopertyName() {
		return mRequestPopertyName;
	}

	/**
	 * Sets the request poperty name.
	 *
	 * @param pRequestPopertyName            the requestPopertyName to set
	 */
	public void setRequestPopertyName(String pRequestPopertyName) {
		mRequestPopertyName = pRequestPopertyName;
	}

	/**
	 * Gets the response poperty name.
	 *
	 * @return the responsePopertyName
	 */
	public String getResponsePopertyName() {
		return mResponsePopertyName;
	}

	/**
	 * Sets the response poperty name.
	 *
	 * @param pResponsePopertyName            the responsePopertyName to set
	 */
	public void setResponsePopertyName(String pResponsePopertyName) {
		mResponsePopertyName = pResponsePopertyName;
	}

	/**
	 * Gets the type poperty name.
	 *
	 * @return the typePopertyName
	 */
	public String getTypePopertyName() {
		return mTypePopertyName;
	}

	/**
	 * Sets the type poperty name.
	 *
	 * @param pTypePopertyName            the typePopertyName to set
	 */
	public void setTypePopertyName(String pTypePopertyName) {
		mTypePopertyName = pTypePopertyName;
	}

	/**
	 * Gets the status poperty name.
	 *
	 * @return the statusPopertyName
	 */
	public String getStatusPopertyName() {
		return mStatusPopertyName;
	}

	/**
	 * Sets the status poperty name.
	 *
	 * @param pStatusPopertyName            the statusPopertyName to set
	 */
	public void setStatusPopertyName(String pStatusPopertyName) {
		mStatusPopertyName = pStatusPopertyName;
	}

	/**
	 * Gets the transaction time poperty name.
	 *
	 * @return the transactionTimePopertyName
	 */
	public String getTransactionTimePopertyName() {
		return mTransactionTimePopertyName;
	}

	/**
	 * Sets the transaction time poperty name.
	 *
	 * @param pTransactionTimePopertyName            the transactionTimePopertyName to set
	 */
	public void setTransactionTimePopertyName(String pTransactionTimePopertyName) {
		mTransactionTimePopertyName = pTransactionTimePopertyName;
	}

	/**
	 * Gets the oms trans records item descriptor name.
	 *
	 * @return the omsTransRecordsItemDescriptorName
	 */
	public String getOmsTransRecordsItemDescriptorName() {
		return mOmsTransRecordsItemDescriptorName;
	}

	/**
	 * Sets the oms trans records item descriptor name.
	 *
	 * @param pOmsTransRecordsItemDescriptorName            the omsTransRecordsItemDescriptorName to set
	 */
	public void setOmsTransRecordsItemDescriptorName(
			String pOmsTransRecordsItemDescriptorName) {
		mOmsTransRecordsItemDescriptorName = pOmsTransRecordsItemDescriptorName;
	}
	
	/**
	 * Gets the RequestSendCountPropertyName.
	 *
	 * @return the requestSendCountPropertyName
	 */
	
	public String getRequestSendCountPropertyName() {
		return mRequestSendCountPropertyName;
	}
	
	/**
	 * Sets the requestSendCountPropertyName.
	 *
	 * @param pRequestSendCountPropertyName    requestSendCountPropertyName to set
	 */
	public void setRequestSendCountPropertyName(
			String pRequestSendCountPropertyName) {
		this.mRequestSendCountPropertyName = pRequestSendCountPropertyName;
	}
	
	/**
	 * This is to get the value for orderCustomAttributeTaxTransactionId.
	 *
	 * @return the orderCustomAttributeTaxTransactionId value
	 */
	public String getOrderCustomAttributeTaxTransactionId() {
		return mOrderCustomAttributeTaxTransactionId;
	}
	
	/**
	 * This method to set the pOrderCustomAttributeTaxTransactionId with orderCustomAttributeTaxTransactionId.
	 *
	 * @param pOrderCustomAttributeTaxTransactionId the orderCustomAttributeTaxTransactionId to set
	 */
	public void setOrderCustomAttributeTaxTransactionId(
			String pOrderCustomAttributeTaxTransactionId) {
		mOrderCustomAttributeTaxTransactionId = pOrderCustomAttributeTaxTransactionId;
	}
	
	/**
	 * This method to set the pOrderHeaderProperty with orderHeaderProperty.
	 *
	 * @param pOrderHeaderProperty the orderHeaderProperty to set
	 */
	public void setOrderHeaderProperty(String pOrderHeaderProperty) {
		mOrderHeaderProperty = pOrderHeaderProperty;
	}

	/**
	 * This is to get the value for mocationIDProperty.
	 *
	 * @return the mLocationIDProperty value
	 */
	public String getLocationIDProperty() {
		return mLocationIDProperty;
	}

	/**
	 * This method to set the pLocationIDProperty with mLocationIDProperty.
	 *
	 * @param pLocationIDProperty the mLocationIDProperty to set
	 */
	public void setLocationIDProperty(String pLocationIDProperty) {
		this.mLocationIDProperty = pLocationIDProperty;
	}

	/**
	 * This is to get the value for mHowToGetItProperty.
	 *
	 * @return the mHowToGetItProperty value
	 */
	public String getHowToGetItProperty() {
		return mHowToGetItProperty;
	}

	/**
	 * This method to set the pHowToGetItProperty with mHowToGetItProperty.
	 *
	 * @param pHowToGetItProperty the mHowToGetItProperty to set
	 */
	public void setHowToGetItProperty(String pHowToGetItProperty) {
		this.mHowToGetItProperty = pHowToGetItProperty;
	}
	
	 /**
 	 * Gets the invalid customer id dummy order list json.
 	 *
 	 * @return the invalid customer id dummy order list json
 	 */
 	public String getInvalidCustomerIDDummyOrderListJson() {
	        return mInvalidCustomerIDDummyOrderListJson;
	    }

	/**
	 * Sets the invalid customer id dummy order list json.
	 *
	 * @param pInvalidCustomerIDDummyOrderListJson the new invalid customer id dummy order list json
	 */
	public void setInvalidCustomerIDDummyOrderListJson(
	            String pInvalidCustomerIDDummyOrderListJson) {
	        this.mInvalidCustomerIDDummyOrderListJson = pInvalidCustomerIDDummyOrderListJson;
	    }

	/**
	 * Gets the order lookup service down dummy json.
	 *
	 * @return the order lookup service down dummy json
	 */
	public String getOrderLookupServiceDownDummyJson() {
	        return mOrderLookupServiceDownDummyJson;
	    }

	/**
	 * Sets the order lookup service down dummy json.
	 *
	 * @param pOrderLookupServiceDownDummyJson the new order lookup service down dummy json
	 */
	public void setOrderLookupServiceDownDummyJson(
	            String pOrderLookupServiceDownDummyJson) {
	        this.mOrderLookupServiceDownDummyJson = pOrderLookupServiceDownDummyJson;
	    }

	/**
	 * Gets the invalid order id dummy order details json.
	 *
	 * @return the invalid order id dummy order details json
	 */
	public String getInvalidOrderIDDummyOrderDetailsJson() {
	        return mInvalidOrderIDDummyOrderDetailsJson;
	    }

	/**
	 * Sets the invalid order id dummy order details json.
	 *
	 * @param pInvalidOrderIDDummyOrderDetailsJson the new invalid order id dummy order details json
	 */
	public void setInvalidOrderIDDummyOrderDetailsJson(
	            String pInvalidOrderIDDummyOrderDetailsJson) {
	        this.mInvalidOrderIDDummyOrderDetailsJson = pInvalidOrderIDDummyOrderDetailsJson;
	    }

	/**
	 * Gets the order details service down dummy json.
	 *
	 * @return the order details service down dummy json
	 */
	public String getOrderDetailsServiceDownDummyJson() {
	        return mOrderDetailsServiceDownDummyJson;
	    }

	/**
	 * Sets the order details service down dummy json.
	 *
	 * @param pOrderDetailsServiceDownDummyJson the new order details service down dummy json
	 */
	public void setOrderDetailsServiceDownDummyJson(
	            String pOrderDetailsServiceDownDummyJson) {
	        this.mOrderDetailsServiceDownDummyJson = pOrderDetailsServiceDownDummyJson;
	    }

	/**
	 * Gets the store code property name response.
	 *
	 * @return the store code property name response
	 */
	public String getStoreCodePropertyNameResponse() {
			return mStoreCodePropertyNameResponse;
		}

	/**
	 * Sets the store code property name response.
	 *
	 * @param pStoreCodePropertyNameResponse the new store code property name response
	 */
	public void setStoreCodePropertyNameResponse(
				String pStoreCodePropertyNameResponse) {
			 mStoreCodePropertyNameResponse = pStoreCodePropertyNameResponse;
		}

	/**
	 * Gets the store phone property name response.
	 *
	 * @return the store phone property name response
	 */
	public String getStorePhonePropertyNameResponse() {
			return mStorePhonePropertyNameResponse;
		}

	/**
	 * Sets the store phone property name response.
	 *
	 * @param pStorePhonePropertyNameResponse the new store phone property name response
	 */
	public void setStorePhonePropertyNameResponse(
				String pStorePhonePropertyNameResponse) {
			 mStorePhonePropertyNameResponse = pStorePhonePropertyNameResponse;
		}

	/**
	 * Gets the store name property name response.
	 *
	 * @return the store name property name response
	 */
	public String getStoreNamePropertyNameResponse() {
			return mStoreNamePropertyNameResponse;
		}

	/**
	 * Sets the store name property name response.
	 *
	 * @param pStoreNamePropertyNameResponse the new store name property name response
	 */
	public void setStoreNamePropertyNameResponse(
				String pStoreNamePropertyNameResponse) {
			 mStoreNamePropertyNameResponse = pStoreNamePropertyNameResponse;
		}

	/**
	 * This method is to get partnerOrderId.
	 *
	 * @return the partnerOrderId
	 */
	public String getPartnerOrderId() {
		return mPartnerOrderId;
	}

	/**
	 * This method sets partnerOrderId with pPartnerOrderId.
	 *
	 * @param pPartnerOrderId the partnerOrderId to set
	 */
	public void setPartnerOrderId(String pPartnerOrderId) {
		mPartnerOrderId = pPartnerOrderId;
	}

	/**
	 * This method is to get t1OrderExistsPropertyName.
	 *
	 * @return the t1OrderExistsPropertyName
	 */
	public String getT1OrderExistsPropertyName() {
		return mT1OrderExistsPropertyName;
	}

	/**
	 * This method sets t1OrderExistsPropertyName with pT1OrderExistsPropertyName.
	 *
	 * @param pT1OrderExistsPropertyName the t1OrderExistsPropertyName to set
	 */
	public void setT1OrderExistsPropertyName(String pT1OrderExistsPropertyName) {
		mT1OrderExistsPropertyName = pT1OrderExistsPropertyName;
	}

	/**
	 * This method is to get orderTotalAmountsPropertyName.
	 *
	 * @return the orderTotalAmountsPropertyName
	 */
	public String getOrderTotalAmountsPropertyName() {
		return mOrderTotalAmountsPropertyName;
	}

	/**
	 * This method sets orderTotalAmountsPropertyName with pOrderTotalAmountsPropertyName.
	 *
	 * @param pOrderTotalAmountsPropertyName the orderTotalAmountsPropertyName to set
	 */
	public void setOrderTotalAmountsPropertyName(
			String pOrderTotalAmountsPropertyName) {
		mOrderTotalAmountsPropertyName = pOrderTotalAmountsPropertyName;
	}

	/**
	 * This method is to get orderSummaryMerchandiseAmount.
	 *
	 * @return the orderSummaryMerchandiseAmount
	 */
	public String getOrderSummaryMerchandiseAmount() {
		return mOrderSummaryMerchandiseAmount;
	}

	/**
	 * This method sets orderSummaryMerchandiseAmount with pOrderSummaryMerchandiseAmount.
	 *
	 * @param pOrderSummaryMerchandiseAmount the orderSummaryMerchandiseAmount to set
	 */
	public void setOrderSummaryMerchandiseAmount(
			String pOrderSummaryMerchandiseAmount) {
		mOrderSummaryMerchandiseAmount = pOrderSummaryMerchandiseAmount;
	}

	/**
	 * This method is to get orderSummaryGrandTotal.
	 *
	 * @return the orderSummaryGrandTotal
	 */
	public String getOrderSummaryGrandTotal() {
		return mOrderSummaryGrandTotal;
	}

	/**
	 * This method sets orderSummaryGrandTotal with pOrderSummaryGrandTotal.
	 *
	 * @param pOrderSummaryGrandTotal the orderSummaryGrandTotal to set
	 */
	public void setOrderSummaryGrandTotal(String pOrderSummaryGrandTotal) {
		mOrderSummaryGrandTotal = pOrderSummaryGrandTotal;
	}

	/**
	 * This method is to get orderSummaryTaxAmount.
	 *
	 * @return the orderSummaryTaxAmount
	 */
	public String getOrderSummaryTaxAmount() {
		return mOrderSummaryTaxAmount;
	}

	/**
	 * This method sets orderSummaryTaxAmount with pOrderSummaryTaxAmount.
	 *
	 * @param pOrderSummaryTaxAmount the orderSummaryTaxAmount to set
	 */
	public void setOrderSummaryTaxAmount(String pOrderSummaryTaxAmount) {
		mOrderSummaryTaxAmount = pOrderSummaryTaxAmount;
	}

	/**
	 * This method is to get orderSummaryShippingAmount.
	 *
	 * @return the orderSummaryShippingAmount
	 */
	public String getOrderSummaryShippingAmount() {
		return mOrderSummaryShippingAmount;
	}

	/**
	 * This method sets orderSummaryShippingAmount with pOrderSummaryShippingAmount.
	 *
	 * @param pOrderSummaryShippingAmount the orderSummaryShippingAmount to set
	 */
	public void setOrderSummaryShippingAmount(String pOrderSummaryShippingAmount) {
		mOrderSummaryShippingAmount = pOrderSummaryShippingAmount;
	}

	/**
	 * This method is to get partnerIdPropertyName.
	 *
	 * @return the partnerIdPropertyName
	 */
	public String getPartnerIdPropertyName() {
		return mPartnerIdPropertyName;
	}

	/**
	 * This method sets partnerIdPropertyName with pPartnerIdPropertyName.
	 *
	 * @param pPartnerIdPropertyName the partnerIdPropertyName to set
	 */
	public void setPartnerIdPropertyName(String pPartnerIdPropertyName) {
		mPartnerIdPropertyName = pPartnerIdPropertyName;
	}

	/**
	 * This method is to get dateExistsPropertyName.
	 *
	 * @return the dateExistsPropertyName
	 */
	public String getDateExistsPropertyName() {
		return mDateExistsPropertyName;
	}

	/**
	 * This method sets dateExistsPropertyName with pDateExistsPropertyName.
	 *
	 * @param pDateExistsPropertyName the dateExistsPropertyName to set
	 */
	public void setDateExistsPropertyName(String pDateExistsPropertyName) {
		mDateExistsPropertyName = pDateExistsPropertyName;
	}

	/**
	 * This method is to get pricePropertyName.
	 *
	 * @return the pricePropertyName
	 */
	public String getPricePropertyName() {
		return mPricePropertyName;
	}

	/**
	 * This method sets pricePropertyName with pPricePropertyName.
	 *
	 * @param pPricePropertyName the pricePropertyName to set
	 */
	public void setPricePropertyName(String pPricePropertyName) {
		mPricePropertyName = pPricePropertyName;
	}
	
	/**
	 * Gets the giftWrapTRUSiteName property name.
	 * 
	 * @return the giftWrapTRUSiteName property name.
	 */
	public String getGiftWrapTRUSiteName() {
		return mGiftWrapTRUSiteName;
	}

	/**
	 * Sets the giftWrapTRUSiteName property name.
	 * 
	 * @param pGiftWrapTRUSiteName
	 *            the giftWrapTRUSiteName property name.
	 */
	public void setGiftWrapTRUSiteName(String pGiftWrapTRUSiteName) {
		this.mGiftWrapTRUSiteName = pGiftWrapTRUSiteName;
	}

	/**
	 * Gets the giftWrapBRUSiteName property name.
	 * 
	 * @return the giftWrapBRUSiteName property name.
	 */
	public String getGiftWrapBRUSiteName() {
		return mGiftWrapBRUSiteName;
	}

	/**
	 * Sets the giftWrapBRUSiteName property name.
	 * 
	 * @param pGiftWrapBRUSiteName
	 *           the  giftWrapBRUSiteName property name.
	 */
	public void setGiftWrapBRUSiteName(String pGiftWrapBRUSiteName) {
		this.mGiftWrapBRUSiteName = pGiftWrapBRUSiteName;
	}
	
	/**
	 * Gets the invalidCancelOrderAttemptJson property name.
	 * 
	 * @return the mInvalidCancelOrderAttemptJson property name.
	 */
	public String getInvalidCancelOrderAttemptJson() {
		return mInvalidCancelOrderAttemptJson;
	}

	/**
	 * Sets the invalidCancelOrderAttemptJson property name.
	 * 
	 * @param pInvalidCancelOrderAttemptJson
	 *           the  invalidCancelOrderAttemptJson property name.
	 */
	public void setInvalidCancelOrderAttemptJson(String pInvalidCancelOrderAttemptJson) {
		this.mInvalidCancelOrderAttemptJson = pInvalidCancelOrderAttemptJson;
	}

	/**
	 * Gets the mInvalidCancelOrderJson property name.
	 * 
	 * @return the mInvalidCancelOrderJson property name.
	 */
	public String getInvalidCancelOrderJson() {
		return mInvalidCancelOrderJson;
	}

	/**
	 * Sets the mInvalidCancelOrderJson property name.
	 * 
	 * @param pInvalidCancelOrderJson
	 *           the  invalidCancelOrderAttemptJson property name.
	 */
	public void setInvalidCancelOrderJson(String pInvalidCancelOrderJson) {
		this.mInvalidCancelOrderJson = pInvalidCancelOrderJson;
	}	
	
	/**
	 * Holds mOrderDetailsHelpAndReturnsURLJsonElement.
	 */
	private String mOrderDetailsHelpAndReturnsURLJsonElement;
	/**
	 * Holds mOrderDetailsHelpAndReturnsJsonElement.
	 */
	private String mOrderDetailsHelpAndReturnsJsonElement;
	
	
	/**
	 * Gets the order details help and returns url json element.
	 *
	 * @return the mOrderDetailsHelpAndReturnsURLJsonElement
	 */
	public String getOrderDetailsHelpAndReturnsURLJsonElement() {
		return mOrderDetailsHelpAndReturnsURLJsonElement;
	}

	/**
	 * Sets the order details help and returns url json element.
	 *
	 * @param pOrderDetailsHelpAndReturnsURLJsonElement the mOrderDetailsHelpAndReturnsURLJsonElement to set
	 */
	public void setOrderDetailsHelpAndReturnsURLJsonElement(
			String pOrderDetailsHelpAndReturnsURLJsonElement) {
		this.mOrderDetailsHelpAndReturnsURLJsonElement = pOrderDetailsHelpAndReturnsURLJsonElement;
	}

	/**
	 * Gets the order details help and returns json element.
	 *
	 * @return the mOrderDetailsHelpAndReturnsJsonElement
	 */
	public String getOrderDetailsHelpAndReturnsJsonElement() {
		return mOrderDetailsHelpAndReturnsJsonElement;
	}

	/**
	 * Sets the order details help and returns json element.
	 *
	 * @param pOrderDetailsHelpAndReturnsJsonElement the mOrderDetailsHelpAndReturnsJsonElement to set
	 */
	public void setOrderDetailsHelpAndReturnsJsonElement(
			String pOrderDetailsHelpAndReturnsJsonElement) {
		this.mOrderDetailsHelpAndReturnsJsonElement = pOrderDetailsHelpAndReturnsJsonElement;
	}

	/**
	 * Holds mStatePropertyName.
	 */
	private String mStatePropertyName;

	/**
	 * Gets the mStatePropertyName.
	 *
	 * @return the mStatePropertyName
	 */
	public String getStatePropertyName() {
		return mStatePropertyName;
	}

	/**
	 * Sets the pStatePropertyName to mStatePropertyName.
	 *
	 * @param pStatePropertyName the mStatePropertyName to set
	 */
	public void setStatePropertyName(String pStatePropertyName) {
		this.mStatePropertyName = pStatePropertyName;
	}
}
