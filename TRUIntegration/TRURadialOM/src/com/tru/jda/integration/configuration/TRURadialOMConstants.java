package com.tru.jda.integration.configuration;

/**
 * @author Professional Access
 * @version 1.0
 */
public class TRURadialOMConstants {
	/**  Holds UNDERSCORE. */
	public static final String UNDERSCORE = "_";
	
	/**  Holds FEETYPE_EWASTE. */
	public static final String FEETYPE_EWASTE = "eWaste_Fee";
	
	/**  Holds MERCHTAX. */
	public static final String MERCHTAX = "MERCHTAX";
	
	/**  Holds MERCHDISCOUNTTAX. */
	public static final String MERCHDISCOUNTTAX = "MERCHDISCOUNTTAX";
	
	/**  Holds SHIPPINGTAX. */
	public static final String SHIPPINGTAX = "SHIPPINGTAX";
	
	/**  Holds SHIPPINGDISCOUNTTAX. */
	public static final String SHIPPINGDISCOUNTTAX = "SHIPPINGDISCOUNTTAX";
	
	/**  Holds FEETAX. */
	public static final String FEETAX = "eWaste_Fee";
	
	/**  Holds GWTAX. */
	public static final String GWTAX = "GWTAX";
	
	/**  Holds GWDISCOUNTTAX. */
	public static final String GWDISCOUNTTAX = "GWDISCOUNTTAX";
	/** The Constant EST_DATE_FORMAT. */
	public static final String EST_DATE_FORMAT="MM/dd/yyyy hh:mm z";
	
	/**  Holds SG. */
	public static final String  SG = "SG";
	/**The Constant MONDAY. */
	public static final String MONDAY = "Monday";
	/** The Constant TUESDAY. */
	public static final String TUESDAY = "Tuesday";
	/** The Constant WEDNESDAY. */
	public static final String WEDNESDAY = "Wednesday";
	/** The Constant THURSDAY. */
	public static final String THURSDAY = "Thursday";
	/** The Constant FRIDAY. */
	public static final String FRIDAY = "Friday";
	/** The Constant ONE.*/
	public static final String SATURADAY = "Saturday";
	/** The Constant SUNDAY. */
	public static final String SUNDAY = "Sunday";
	/** The Constant WARRANTY. */
	public static final String WARRANTY = "WARRANTY";
	/** The Constant GIFT_WRAP_MESSAGE. */
	public static final String GIFT_WRAP_MESSAGE = "GIFT-WRAP-MESSAGE";
	
	/**  Holds pipe. */
	public static final char CHAR_PIPE_DELIMETER ='|';
	
	/**  Holds brace. */
	public static final String OPEN_BRACES ="(";
	
	/**  Holds brace. */
	public static final String CLOSED_BRACES =")";
	
	/**  Holds PROMO_LEVEL_ITEM. */
	public static final String PROMO_LEVEL_ITEM = "ITEM";
	
	/**  Holds PROMO_LEVEL_SHIPPING. */
	public static final String PROMO_LEVEL_SHIPPING = "SHIPPING";
	
	/**  Holds PROMO_LEVEL_ORDER. */
	public static final String PROMO_LEVEL_ORDER = "ORDER";
	
	/**  Holds TOYS_R_US. */
	public static final String TOYS_R_US = "ToysRUs";
	
	/**  Holds CHANNEL_FROM. */
	public static final String CHANNEL_FROM ="From ";
	
	/**  Holds CHANNEL_AND. */
	public static final String CHANNEL_AND =" & ";
	
	/**  Holds CHANNEL_HASH. */
	public static final String CHANNEL_HASH =" #";
	
	/** The Constant DONATION_ADDRESS_TYPE_SHIPPING. */
	public static final String DONATION_ADDRESS_TYPE_SHIPPING = "Shipping";
	
	/** The Constant PAYPAL_SUCCESS_STATUS. */
	public static final String PAYPAL_SUCCESS_STATUS = "Success";	
	/**
	 * Holds constant CUTOMER_ORDER_IMPORT_SERVICE.
	 */
	public static final String CUTOMER_ORDER_IMPORT_SERVICE = "OrderCreateService";
	
	/**  constant for : ERROR. */
	public static final String ERROR = "ERROR";
	/** property to hold one. */
	public static final int ONE = 1;
	
	/** Constant for Allow INT_ZERO. */
	public static final int INT_ZERO = 0;
	
	/** The Constant LAYAWAY_NO_ORDER_MESSAGE. */
	public static final String LAYAWAY_NO_ORDER_MESSAGE = "layawayNoOrderMessage";
	
	/** The Constant NO_ORDER_MESSAGE. */
	public static final String NO_ORDER_MESSAGE = "noOrderMessage";
	
	/** The Constant NO_ORDER_FOUND_MESSAGE. */
	public static final String NO_ORDER_FOUND_MESSAGE = "noOrderFoundMessage";
	
	/** The Constant LAYAWAY_NO_ORDER_FOUND_MESSAGE. */
	public static final String LAYAWAY_NO_ORDER_FOUND_MESSAGE = "layawayNoOrderFoundMessage";
	
	/** The Constant ZIP_CODE_NOT_MATCH_MESSAGE. */
	public static final String ZIP_CODE_NOT_MATCH_MESSAGE = "zipCodeNotMatchMessage";
	
	/** The Constant GIFTCARD_PAYINSTORE_ERROR_MESSAGE. */
	public static final String GIFTCARD_PAYINSTORE_ERROR_MESSAGE = "giftCardpayInStoreErrorMessgae";
	
	/** Constant to hold OUTPUT.*/
	public static final String OUTPUT = "output";
	
	/** The Constant PAYPALID. */
	public static final String PAYPALID = "PAYPALID";
	
	/** The Constant SHIPMETHODMSG. */
	public static final String SHIPMETHODMSG = "SHIPMETHODMSG";	
	
	/** The Constant ORDER_UPDATE_SERVICE. */
	public static final String ORDER_UPDATE_SERVICE = "OrderUpdateService";
	
	/** Constant holds six month. */
	public static final int FINANCING_SIX_MONTHS = 6;
	
	/** Constant holds twelve month. */
	public static final int FINANCING_TWELVE_MONTHS = 12;
	
	/** Holds STRING_ONE. */
	public static final String STRING_ONE = "1";

	/** Constant for Allow INT_SIX. */
	public static final int INT_SIX = 6;
	
	/**
	 * Holds STRING_TWO.
	 */
	public static final String STRING_TWO = "2";
	
	/** The Constant DATE_OUTPUT_FORMATTER. */
	public static final String DATE_OUTPUT_FORMATTER = "hh:mm a";
	
	/** The Constant DATE_INPUT_FORMATTER. */
	public static final String DATE_INPUT_FORMATTER = "HHmm.ss";
	
	/** The Constant HOURS_ZERO. */
	public static final String HOURS_ZERO = "0";
	
	 /**
     * Constant to hold the constant for firstName.
     */
    public static final String FIRSTNAME = "FirstName";    
    
    /**
     * Constant to hold the constant for lastName.
     */
    public static final String LASTNAME = "LastName";
    
    /**
     * Constant to hold the constant for OrderList.
     */
    public static final String ORDERLIST = "OrderList";
    
    /**
     * Constant to hold the constant for OrderNumber.
     */
    public static final String ORDERNUMBER = "OrderNumber";
    
    /**
     * Constant to hold the constant for OrderStatus.
     */
    public static final String ORDER_STATUS = "OrderStatus";
    
    /**
     * Constant to hold the constant for OrderTotal.
     */
    public static final String ORDER_TOTAL = "OrderTotal";
    
    /**
     * Constant to hold the constant for CreationDate.
     */
    public static final String CREATION_DATE = "CreationDate";
    
    /** The Constant ORDER_DETAIL. */    
    public static final String ORDER_DETAIL = "OrderDetail";
    
    /** The Constant LINE_ITEM. */
    public static final String LINE_ITEM = "LineItem";
    
    /** The Constant LINE_STATUS. */
    public static final String LINE_STATUS = "LineStatus";
    
    /** The Constant QUANTITY. */
    public static final String QUANTITY = "Quantity";
    
    /** The Constant DESCRIPTION. */
    public static final String DESCRIPTION = "Description";
    
    /** The Constant DATE. */
    public static final String DATE = "Date";
    
    /** The Constant PARTNER_ORDER_NUMBER. */
    public static final String PARTNER_ORDER_NUMBER = "PartnerOrderNumber";
    
    /** The Constant ORDER_CREATION_DATE. */
    public static final String ORDER_CREATION_DATE = "OrderCreationDate";
    
    /** The Constant PAYMENT_HISTORY. */
    public static final String PAYMENT_HISTORY = "PaymentHistory";
    
    /** The Constant AMOUNT_PAID. */
    public static final String AMOUNT_PAID = "AmountPaid";
    
	/** The Constant UTF_8. */
	public static final String UTF_8 = "UTF-8";

	/** The Constant REFERENCE_DATE_FORMAT. */
	public static final String REFERENCE_DATE_FORMAT="MMddYYYY";
	/** The Constant REFERENCE_DATE_FORMAT. */
	public static final String COLON_SPLIT = "\\:";
	/** The Constant OUTSTANDING_BALANCE. */
	public static final String OUTSTANDING_BALANCE = "OutstandingBalance";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";
	/** The Constant LOCATION_ID. */
	public static final String LOCATION_ID = "LocationID";
	/** The Constant HOWTO_GETIT. */
	public static final String HOWTO_GETIT = "HowToGetIt";
	/** The Constant BALANCEDUE. */
	public static final String BALANCE_DUE = "BalanceDue";
	
	/** The Constant MESSAGE. */
	public static final String FAILURE = "FAILURE";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Message";
	
	/** The Constant LAYAWAY_PAGE. */
	public static final String LAYAWAY_PAGE = "layawayPage";

	/** The Constant CUSTOMER_EMAIL. */
	public static final String CUSTOMER_EMAIL = "customerEmail";

	/** The Constant ORDER_ID. */
	public static final String ORDER_ID = "orderId";
	public static final String POSTAL_CODE = "postalCode";
	
	/** The Constant RESPONSE. */
	public static final String RESPONSE = "response";
	
	/** The Constant SUCCESS. */
	public static final String SUCCESS = "Success";
	
	/**  Constant to hold TRUE. */
	public static final String TRUE = "true";
	/**  Constant to hold DOWN_PAYMENT. */
	public static final String DOWN_PAYMENT = "DownPayment";
	/**  Constant to hold DOLLAR. */
	public static final String DOLLAR = "$";
	/**  Constant to hold INT_TWO. */
	public static final int INT_TWO = 2;
	
	/** The Constant TAX_CLASS. */
	public static final String TAX_CLASS = "TAXCLASS";

	/**  Constant to hold MOBILE_RESPONSE. */
	public static final String MOBILE_RESPONSE = "mobileResponse";
	
	/**  Constant to hold LAYAWAY_DATE_FORMAT. */
	public static final String LAYAWAY_DATE_FORMAT = "MM-dd-yyyy";
	
	/**  Constant to hold LAYAWAY_SIMPLE_DATE_FORMAT. */
	public static final String LAYAWAY_SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
	
	/**  Constant to hold LAYAWAY_SIMPLE_DATE_FORMAT. */
	public static final String LAYAWAY_TRANSACTION_ID_DATE_FORMAT = "yyyyMMddhhmmss";
	
	/** The Constant N. */
	public static final String N = "N";
	
	/** The Constant DONATION_EXISTS. */
	public static final String DONATION_EXISTS = "donationExists";
	
	/** The Constant IS_GIFT_MESSAGE. */
	public static final String IS_GIFT_MESSAGE = "isGiftMessage";
	
	/** The Constant BPP_EXISTS. */
	public static final String BPP_EXISTS = "bppExists";
	
	/** The Constant ADDRESS. */
	public static final String ADDRESS = "Address";
	
	/** The Constant SHIPPING_ADDRESS. */
	public static final String SHIPPING_ADDRESS = "ShippingAddress";
	
	/** The Constant DONATION. */
	public static final String DONATION = "DONATION";

	/** The Constant DATE_FOMAT. */
	public static final String DATE_FOMAT = "MMddyyyy";
	/** The Constant FOMATED_DATE. */
	public static final String FOMATED_DATE = "MM/dd/yyyy";
	
	 /**
     * Constant to hold the order date format.
     */
    public static final String ORDER_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    
	/** Constant for Allow INT_FIVE. */
	public static final int INT_FIVE = 5;
	
	/** Constant for Allow INT_EIGHT. */
	public static final int INT_EIGHT = 8;
	
	  /**
     * Constant to hold the INT_ONE.
     */
	public static final int INT_ONE = 1;
	
	  /**
     * Constant to hold the INT_THREE.
     */
	public static final int INT_THREE = 3;
	/** Holds ORDER_CREATE_RESPONSE */
	public static final String ORDER_CREATE_RESPONSE = "OrderCreateResponse";

	/** Holds RESPONSE_STATUS */
	public static final String RESPONSE_STATUS = "ResponseStatus";
	/** Holds SPLIT_HYPHEN */
	public static final String SPLIT_HYPHEN = "\\-";
	/** Holds DECIAML_FORMAT */
	public static final String DECIAML_FORMAT = "##0.00";
	
	/** Holds 0.0 */
	public static final String OPENING_HOURS_ZERO_TIME = "0.0";
	/** Holds TRU */
	public static final String TRU="TRU";
	/** Holds BRU */
	public static final String BRU="BRU";
	/** Holds SKU */
	public static final String SKU="SKU";
	/** Holds RESPONSE_CODE */
	public static final String RESPONSE_CODE = "ResponseCode";
	/** Holds TIME_TAKEN */
	public static final String TIME_TAKEN = "TimeTaken";
	/** Holds TIME_STAMP */
	public static final String TIME_STAMP = "TimeStamp";
	/** Holds OPERATION_NAME */
	public static final String OPERATION_NAME = "operationName";
	public static final double DECIMAL_ZERO = .00;
	/** Holds FORMATED_DECIMAL_ZERO */
	public static final double FORMATED_DECIMAL_ZERO = 0.00;
	/** Holds NULL_STRING */
	public static final String NULL_STRING = "null";
	/** Holds SPLIT_QUESTION */
	public static final String SPLIT_QUESTION = "\\?";
	/** Holds IMAGE_EXITS */
	public static final String IMAGE_EXITS = "imageURLExists";
	/** Holds COLON */
	public static final String COLON = ":";
}