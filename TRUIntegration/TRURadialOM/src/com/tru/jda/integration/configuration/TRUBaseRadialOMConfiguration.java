package com.tru.jda.integration.configuration;

import java.util.Map;

import atg.nucleus.GenericService;

/**
 * @author Professional Access
 * @version 1.0
 *
 */
public class TRUBaseRadialOMConfiguration extends GenericService {
	
	/**
	 * This holds itemCustomAttributeEDDStartDate
	 */
	private String mItemCustomAttributeEDDStartDate;
	
	/**
	 * This holds itemCustomAttributeEDDEndDate
	 */
	private String mItemCustomAttributeEDDEndDate;
	
	/**
	 * This holds itemCustomAttributePdpURL
	 */
	private String mItemCustomAttributePdpURL;
	
	/**
	 * This holds itemCustomAttributeShipMessage
	 */
	private String mItemCustomAttributeShipMessage;
	
	/**
	 * This holds itemCustomAttributeItemPrice
	 */
	private String mItemCustomAttributeItemPrice;
	
	/**
	 * This holds itemCustomAttributeTPR
	 */
	private String mItemCustomAttributeTPR;
	
	/**
	 * This holds itemCustomAttributeSurcharge
	 */
	private String mItemCustomAttributeSurcharge;
	
	/**
	 * This holds itemCustomAttributeSlapperTag
	 */
	private String mItemCustomAttributeSlapperTag;
	
	/**
	 * This holds itemCustomAttributeImageURL
	 */
	private String mItemCustomAttributeImageURL;
	
	/**
	 * This holds itemCustomAttributeDCValue
	 */
	private String mItemCustomAttributeDCValue;
	
	/**
	 * This holds itemCustomAttributeRegistryShip
	 */
	private String mItemCustomAttributeRegistryShip;
	
	/**
	 * This holds itemCustomAttributeRegistryType
	 */
	private String mItemCustomAttributeRegistryType;
	
	/**
	 * This holds itemCustomAttributeRegistryID
	 */
	private String mItemCustomAttributeRegistryID;
	
	/**
	 * This holds shippingMethodForISPU
	 */
	private String mShippingMethodForISPU;
	
	/**
	 * This holds shippingMethodForDonation
	 */
	private String mShippingMethodForDonation;
	
	/**
	 * This holds shippingMethodForLayawayPayment
	 */
	private String mShippingMethodForLayawayPayment;
	
	/**
	 * This holds babyRegistryCustomAttributeValue
	 */
	private String mBabyRegistryCustomAttributeValue;
	
	/**
	 * This holds giftRegistryCustomAttributeValue
	 */
	private String mGiftRegistryCustomAttributeValue;
	
	/**
	 * This holds itemCustomAttributeRegistryShipValue
	 */
	private String mItemCustomAttributeRegistryShipValue;
	
	/**
	 * This holds callCreateOrderRESTService
	 */
	private boolean mCallCreateOrderRESTService;
	
	/**
	 * This holds customerOrderUpdateTransPrefix
	 */
	private String mCustomerOrderUpdateTransPrefix;
	
	/**
	 * This holds customerOrderCancelTransPrefix
	 */
	private String mCustomerOrderCancelTransPrefix;
	
	/**
	 * This holds customerOrderImportTransPrefix
	 */
	private String mCustomerOrderImportTransPrefix;
	
	/** Holds boolean flag to include title*/
	private boolean mIncludeTile;
	
	/** Holds boolean flag to include billing address*/
	private boolean mIncludeBillingAddress;
	
	/** Holds customer title */
	private String mCustomerTitle;
	
	/** Holds mPaymentCustomAttributeName */
	private String mPaymentCustomAttributeName;
	
	/** Holds paymentCustomAttributeNameForApplePay */
	private String mPaymentCustomAttributeNameForApplePay;
	
	/** Holds paymentCustomAttributeValueForApplePay */
	private String mPaymentCustomAttributeValueForApplePay;
	
	/** Holds paymentCustomAttributeValueMap */
	private Map<String, String> mPaymentCustomAttributeValueMap;
	/**
	 * Holds mIncludeDummyCookie
	 */
	private boolean mIncludeDummyCookie;
	
	/**
	 * Holds dummyCookie
	 */
	private String mDummyCookie;
	
	/**
	 * Holds includeJavaScript
	 */
	private boolean mIncludeJavaScript;
	
	/**
	 * Holds javascriptData
	 */
	private String mJavascriptData;
	
	/**
	 * Holds countryCodeUS
	 */
	private String mCountryCodeUS;
	
	/**
	 * Holds layawaySKUType
	 */
	private String mLayawaySKUType;
	
	/**
	 * Holds orderCustomAttributePaypalId
	 */
	private String mOrderCustomAttributePaypalId;
	
	/**
	 * Holds orderCustomAttributePaypalPayerId
	 */
	private String mOrderCustomAttributePaypalPayerId;
	
	/**
	 * Holds sixMonthsFinancing
	 */
	private String mSixMonthsFinancing;
	
	/**
	 * Holds twelveMonthsFinancing
	 */
	private String mTwelveMonthsFinancing;
	
	/**
	 * Holds mPaypalPayerStatus
	 */
	private String mPaypalPayerStatus;
	
	/**
	 * Holds paypalAddressStatus
	 */
	private String mPaypalAddressStatus;
	
	/**
	 * Holds shippingMethodMap
	 */
	private Map<String, String> mShippingMethodMap;
	
	/**
	 * Holds orderCustomAttributeCSRNameID
	 */
	private String mOrderCustomAttributeCSRNameID;
	
	/**
	 * Holds itemFeeNameEWaste
	 */
	private String mItemFeeNameEWaste;
	
	/**
	 * orderDatePropertyName
	 */
	private String mOrderDatePropertyName;
	
	/**
	 * registryMessageProperty
	 */
	private String mRegistryMessageProperty;
	
	/**
	 * Holds itemCustomAttributeListPrice
	 */
	private String mItemCustomAttributeListPrice;
	
	/**
	 * HOlds orderCustomAttributeOrderChannel
	 */
	private String mOrderCustomAttributeOrderChannel;
	
	/**
	 * Holds sourceChannelRegStore
	 */
	private String mSourceChannelRegStore;
	
	/**Holds mApoFposhippingMethodMap */
	private Map<String, String> mApoFposhippingMethodMap;
	/**Holds mSourceChannelRegistry */
	private String mSourceChannelRegistry;
	/**Holds sourceChannelWishlist */
	private String mSourceChannelWishlist;
	/**Holds sourceChannelRegistryValue */
	private String mSourceChannelRegistryValue;
	/**Holds sourceChannelWishlistValue */
	private String mSourceChannelWishlistValue;

	/**
	 * This is to get the value for mOrderCustomAttributeCSRNameID
	 *
	 * @return the mOrderCustomAttributeCSRNameID value
	 */
	public String getOrderCustomAttributeCSRNameID() {
		return mOrderCustomAttributeCSRNameID;
	}

	/**
	 * This method to set the pOrderCustomAttributeCSRNameID with mOrderCustomAttributeCSRNameID
	 * 
	 * @param pOrderCustomAttributeCSRNameID the mOrderCustomAttributeCSRNameID to set
	 */
	public void setOrderCustomAttributeCSRNameID(
			String pOrderCustomAttributeCSRNameID) {
		mOrderCustomAttributeCSRNameID = pOrderCustomAttributeCSRNameID;
	}

	/**
	 * This is to get the value for itemCustomAttributeEDDStartDate
	 *
	 * @return the itemCustomAttributeEDDStartDate value
	 */
	public String getItemCustomAttributeEDDStartDate() {
		return mItemCustomAttributeEDDStartDate;
	}

	/**
	 * This method to set the pItemCustomAttributeEDDStartDate with itemCustomAttributeEDDStartDate
	 * 
	 * @param pItemCustomAttributeEDDStartDate the itemCustomAttributeEDDStartDate to set
	 */
	public void setItemCustomAttributeEDDStartDate(
			String pItemCustomAttributeEDDStartDate) {
		mItemCustomAttributeEDDStartDate = pItemCustomAttributeEDDStartDate;
	}

	/**
	 * This is to get the value for itemCustomAttributeEDDEndDate
	 *
	 * @return the itemCustomAttributeEDDEndDate value
	 */
	public String getItemCustomAttributeEDDEndDate() {
		return mItemCustomAttributeEDDEndDate;
	}

	/**
	 * This method to set the pItemCustomAttributeEDDEndDate with itemCustomAttributeEDDEndDate
	 * 
	 * @param pItemCustomAttributeEDDEndDate the itemCustomAttributeEDDEndDate to set
	 */
	public void setItemCustomAttributeEDDEndDate(
			String pItemCustomAttributeEDDEndDate) {
		mItemCustomAttributeEDDEndDate = pItemCustomAttributeEDDEndDate;
	}

	/**
	 * This is to get the value for itemCustomAttributePdpURL
	 *
	 * @return the itemCustomAttributePdpURL value
	 */
	public String getItemCustomAttributePdpURL() {
		return mItemCustomAttributePdpURL;
	}

	/**
	 * This method to set the pItemCustomAttributePdpURL with itemCustomAttributePdpURL
	 * 
	 * @param pItemCustomAttributePdpURL the itemCustomAttributePdpURL to set
	 */
	public void setItemCustomAttributePdpURL(String pItemCustomAttributePdpURL) {
		mItemCustomAttributePdpURL = pItemCustomAttributePdpURL;
	}

	/**
	 * This is to get the value for itemCustomAttributeShipMessage
	 *
	 * @return the itemCustomAttributeShipMessage value
	 */
	public String getItemCustomAttributeShipMessage() {
		return mItemCustomAttributeShipMessage;
	}

	/**
	 * This method to set the pItemCustomAttributeShipMessage with itemCustomAttributeShipMessage
	 * 
	 * @param pItemCustomAttributeShipMessage the itemCustomAttributeShipMessage to set
	 */
	public void setItemCustomAttributeShipMessage(
			String pItemCustomAttributeShipMessage) {
		mItemCustomAttributeShipMessage = pItemCustomAttributeShipMessage;
	}

	/**
	 * This is to get the value for itemCustomAttributeItemPrice
	 *
	 * @return the itemCustomAttributeItemPrice value
	 */
	public String getItemCustomAttributeItemPrice() {
		return mItemCustomAttributeItemPrice;
	}

	/**
	 * This method to set the pItemCustomAttributeItemPrice with itemCustomAttributeItemPrice
	 * 
	 * @param pItemCustomAttributeItemPrice the itemCustomAttributeItemPrice to set
	 */
	public void setItemCustomAttributeItemPrice(String pItemCustomAttributeItemPrice) {
		mItemCustomAttributeItemPrice = pItemCustomAttributeItemPrice;
	}

	/**
	 * This is to get the value for itemCustomAttributeTPR
	 *
	 * @return the itemCustomAttributeTPR value
	 */
	public String getItemCustomAttributeTPR() {
		return mItemCustomAttributeTPR;
	}

	/**
	 * This method to set the pItemCustomAttributeTPR with itemCustomAttributeTPR
	 * 
	 * @param pItemCustomAttributeTPR the itemCustomAttributeTPR to set
	 */
	public void setItemCustomAttributeTPR(String pItemCustomAttributeTPR) {
		mItemCustomAttributeTPR = pItemCustomAttributeTPR;
	}

	/**
	 * This is to get the value for itemCustomAttributeSurcharge
	 *
	 * @return the itemCustomAttributeSurcharge value
	 */
	public String getItemCustomAttributeSurcharge() {
		return mItemCustomAttributeSurcharge;
	}

	/**
	 * This method to set the pItemCustomAttributeSurcharge with itemCustomAttributeSurcharge
	 * 
	 * @param pItemCustomAttributeSurcharge the itemCustomAttributeSurcharge to set
	 */
	public void setItemCustomAttributeSurcharge(String pItemCustomAttributeSurcharge) {
		mItemCustomAttributeSurcharge = pItemCustomAttributeSurcharge;
	}

	/**
	 * This is to get the value for itemCustomAttributeSlapperTag
	 *
	 * @return the itemCustomAttributeSlapperTag value
	 */
	public String getItemCustomAttributeSlapperTag() {
		return mItemCustomAttributeSlapperTag;
	}

	/**
	 * This method to set the pItemCustomAttributeSlapperTag with itemCustomAttributeSlapperTag
	 * 
	 * @param pItemCustomAttributeSlapperTag the itemCustomAttributeSlapperTag to set
	 */
	public void setItemCustomAttributeSlapperTag(
			String pItemCustomAttributeSlapperTag) {
		mItemCustomAttributeSlapperTag = pItemCustomAttributeSlapperTag;
	}

	/**
	 * This is to get the value for itemCustomAttributeImageURL
	 *
	 * @return the itemCustomAttributeImageURL value
	 */
	public String getItemCustomAttributeImageURL() {
		return mItemCustomAttributeImageURL;
	}

	/**
	 * This method to set the pItemCustomAttributeImageURL with itemCustomAttributeImageURL
	 * 
	 * @param pItemCustomAttributeImageURL the itemCustomAttributeImageURL to set
	 */
	public void setItemCustomAttributeImageURL(String pItemCustomAttributeImageURL) {
		mItemCustomAttributeImageURL = pItemCustomAttributeImageURL;
	}

	/**
	 * This is to get the value for itemCustomAttributeDCValue
	 *
	 * @return the itemCustomAttributeDCValue value
	 */
	public String getItemCustomAttributeDCValue() {
		return mItemCustomAttributeDCValue;
	}

	/**
	 * This method to set the pItemCustomAttributeDCValue with itemCustomAttributeDCValue
	 * 
	 * @param pItemCustomAttributeDCValue the itemCustomAttributeDCValue to set
	 */
	public void setItemCustomAttributeDCValue(String pItemCustomAttributeDCValue) {
		mItemCustomAttributeDCValue = pItemCustomAttributeDCValue;
	}

	/**
	 * This is to get the value for itemCustomAttributeRegistryShip
	 *
	 * @return the itemCustomAttributeRegistryShip value
	 */
	public String getItemCustomAttributeRegistryShip() {
		return mItemCustomAttributeRegistryShip;
	}

	/**
	 * This method to set the pItemCustomAttributeRegistryShip with itemCustomAttributeRegistryShip
	 * 
	 * @param pItemCustomAttributeRegistryShip the itemCustomAttributeRegistryShip to set
	 */
	public void setItemCustomAttributeRegistryShip(
			String pItemCustomAttributeRegistryShip) {
		mItemCustomAttributeRegistryShip = pItemCustomAttributeRegistryShip;
	}

	/**
	 * This is to get the value for itemCustomAttributeRegistryType
	 *
	 * @return the itemCustomAttributeRegistryType value
	 */
	public String getItemCustomAttributeRegistryType() {
		return mItemCustomAttributeRegistryType;
	}

	/**
	 * This method to set the pItemCustomAttributeRegistryType with itemCustomAttributeRegistryType
	 * 
	 * @param pItemCustomAttributeRegistryType the itemCustomAttributeRegistryType to set
	 */
	public void setItemCustomAttributeRegistryType(
			String pItemCustomAttributeRegistryType) {
		mItemCustomAttributeRegistryType = pItemCustomAttributeRegistryType;
	}

	/**
	 * This is to get the value for itemCustomAttributeRegistryID
	 *
	 * @return the itemCustomAttributeRegistryID value
	 */
	public String getItemCustomAttributeRegistryID() {
		return mItemCustomAttributeRegistryID;
	}

	/**
	 * This method to set the pItemCustomAttributeRegistryID with itemCustomAttributeRegistryID
	 * 
	 * @param pItemCustomAttributeRegistryID the itemCustomAttributeRegistryID to set
	 */
	public void setItemCustomAttributeRegistryID(
			String pItemCustomAttributeRegistryID) {
		mItemCustomAttributeRegistryID = pItemCustomAttributeRegistryID;
	}

	/**
	 * This is to get the value for shippingMethodForISPU
	 *
	 * @return the shippingMethodForISPU value
	 */
	public String getShippingMethodForISPU() {
		return mShippingMethodForISPU;
	}

	/**
	 * This method to set the pShippingMethodForISPU with shippingMethodForISPU
	 * 
	 * @param pShippingMethodForISPU the shippingMethodForISPU to set
	 */
	public void setShippingMethodForISPU(String pShippingMethodForISPU) {
		mShippingMethodForISPU = pShippingMethodForISPU;
	}

	/**
	 * This is to get the value for shippingMethodForDonation
	 *
	 * @return the shippingMethodForDonation value
	 */
	public String getShippingMethodForDonation() {
		return mShippingMethodForDonation;
	}

	/**
	 * This method to set the pShippingMethodForDonation with shippingMethodForDonation
	 * 
	 * @param pShippingMethodForDonation the shippingMethodForDonation to set
	 */
	public void setShippingMethodForDonation(String pShippingMethodForDonation) {
		mShippingMethodForDonation = pShippingMethodForDonation;
	}

	/**
	 * This is to get the value for shippingMethodForLayawayPayment
	 *
	 * @return the shippingMethodForLayawayPayment value
	 */
	public String getShippingMethodForLayawayPayment() {
		return mShippingMethodForLayawayPayment;
	}

	/**
	 * This method to set the pShippingMethodForLayawayPayment with shippingMethodForLayawayPayment
	 * 
	 * @param pShippingMethodForLayawayPayment the shippingMethodForLayawayPayment to set
	 */
	public void setShippingMethodForLayawayPayment(
			String pShippingMethodForLayawayPayment) {
		mShippingMethodForLayawayPayment = pShippingMethodForLayawayPayment;
	}

	/**
	 * This is to get the value for babyRegistryCustomAttributeValue
	 *
	 * @return the babyRegistryCustomAttributeValue value
	 */
	public String getBabyRegistryCustomAttributeValue() {
		return mBabyRegistryCustomAttributeValue;
	}

	/**
	 * This method to set the pBabyRegistryCustomAttributeValue with babyRegistryCustomAttributeValue
	 * 
	 * @param pBabyRegistryCustomAttributeValue the babyRegistryCustomAttributeValue to set
	 */
	public void setBabyRegistryCustomAttributeValue(
			String pBabyRegistryCustomAttributeValue) {
		mBabyRegistryCustomAttributeValue = pBabyRegistryCustomAttributeValue;
	}

	/**
	 * This is to get the value for giftRegistryCustomAttributeValue
	 *
	 * @return the giftRegistryCustomAttributeValue value
	 */
	public String getGiftRegistryCustomAttributeValue() {
		return mGiftRegistryCustomAttributeValue;
	}

	/**
	 * This method to set the pGiftRegistryCustomAttributeValue with giftRegistryCustomAttributeValue
	 * 
	 * @param pGiftRegistryCustomAttributeValue the giftRegistryCustomAttributeValue to set
	 */
	public void setGiftRegistryCustomAttributeValue(
			String pGiftRegistryCustomAttributeValue) {
		mGiftRegistryCustomAttributeValue = pGiftRegistryCustomAttributeValue;
	}

	/**
	 * This is to get the value for itemCustomAttributeRegistryShipValue
	 *
	 * @return the itemCustomAttributeRegistryShipValue value
	 */
	public String getItemCustomAttributeRegistryShipValue() {
		return mItemCustomAttributeRegistryShipValue;
	}

	/**
	 * This method to set the pItemCustomAttributeRegistryShipValue with itemCustomAttributeRegistryShipValue
	 * 
	 * @param pItemCustomAttributeRegistryShipValue the itemCustomAttributeRegistryShipValue to set
	 */
	public void setItemCustomAttributeRegistryShipValue(
			String pItemCustomAttributeRegistryShipValue) {
		mItemCustomAttributeRegistryShipValue = pItemCustomAttributeRegistryShipValue;
	}

	/**
	 * This is to get the value for callCreateOrderRESTService
	 *
	 * @return the callCreateOrderRESTService value
	 */
	public boolean isCallCreateOrderRESTService() {
		return mCallCreateOrderRESTService;
	}

	/**
	 * This method to set the pCallCreateOrderRESTService with callCreateOrderRESTService
	 * 
	 * @param pCallCreateOrderRESTService the callCreateOrderRESTService to set
	 */
	public void setCallCreateOrderRESTService(boolean pCallCreateOrderRESTService) {
		mCallCreateOrderRESTService = pCallCreateOrderRESTService;
	}

	/**
	 * This is to get the value for customerOrderUpdateTransPrefix
	 *
	 * @return the customerOrderUpdateTransPrefix value
	 */
	public String getCustomerOrderUpdateTransPrefix() {
		return mCustomerOrderUpdateTransPrefix;
	}

	/**
	 * This method to set the pCustomerOrderUpdateTransPrefix with customerOrderUpdateTransPrefix
	 * 
	 * @param pCustomerOrderUpdateTransPrefix the customerOrderUpdateTransPrefix to set
	 */
	public void setCustomerOrderUpdateTransPrefix(
			String pCustomerOrderUpdateTransPrefix) {
		mCustomerOrderUpdateTransPrefix = pCustomerOrderUpdateTransPrefix;
	}

	/**
	 * This is to get the value for customerOrderCancelTransPrefix
	 *
	 * @return the customerOrderCancelTransPrefix value
	 */
	public String getCustomerOrderCancelTransPrefix() {
		return mCustomerOrderCancelTransPrefix;
	}

	/**
	 * This method to set the pCustomerOrderCancelTransPrefix with customerOrderCancelTransPrefix
	 * 
	 * @param pCustomerOrderCancelTransPrefix the customerOrderCancelTransPrefix to set
	 */
	public void setCustomerOrderCancelTransPrefix(
			String pCustomerOrderCancelTransPrefix) {
		mCustomerOrderCancelTransPrefix = pCustomerOrderCancelTransPrefix;
	}

	/**
	 * This is to get the value for customerOrderImportTransPrefix
	 *
	 * @return the customerOrderImportTransPrefix value
	 */
	public String getCustomerOrderImportTransPrefix() {
		return mCustomerOrderImportTransPrefix;
	}

	/**
	 * This method to set the pCustomerOrderImportTransPrefix with customerOrderImportTransPrefix
	 * 
	 * @param pCustomerOrderImportTransPrefix the customerOrderImportTransPrefix to set
	 */
	public void setCustomerOrderImportTransPrefix(
			String pCustomerOrderImportTransPrefix) {
		mCustomerOrderImportTransPrefix = pCustomerOrderImportTransPrefix;
	}

	/**
	 * This is to get the value for includeTile
	 *
	 * @return the includeTile value
	 */
	public boolean isIncludeTile() {
		return mIncludeTile;
	}

	/**
	 * This method to set the pIncludeTile with includeTile
	 * 
	 * @param pIncludeTile the includeTile to set
	 */
	public void setIncludeTile(boolean pIncludeTile) {
		mIncludeTile = pIncludeTile;
	}

	/**
	 * This is to get the value for includeBillingAddress
	 *
	 * @return the includeBillingAddress value
	 */
	public boolean isIncludeBillingAddress() {
		return mIncludeBillingAddress;
	}

	/**
	 * This method to set the pIncludeBillingAddress with includeBillingAddress
	 * 
	 * @param pIncludeBillingAddress the includeBillingAddress to set
	 */
	public void setIncludeBillingAddress(boolean pIncludeBillingAddress) {
		mIncludeBillingAddress = pIncludeBillingAddress;
	}

	/**
	 * This is to get the value for customerTitle
	 *
	 * @return the customerTitle value
	 */
	public String getCustomerTitle() {
		return mCustomerTitle;
	}

	/**
	 * This method to set the pCustomerTitle with customerTitle
	 * 
	 * @param pCustomerTitle the customerTitle to set
	 */
	public void setCustomerTitle(String pCustomerTitle) {
		mCustomerTitle = pCustomerTitle;
	}

	/**
	 * This is to get the value for paymentCustomAttributeName
	 *
	 * @return the paymentCustomAttributeName value
	 */
	public String getPaymentCustomAttributeName() {
		return mPaymentCustomAttributeName;
	}

	/**
	 * This method to set the pPaymentCustomAttributeName with paymentCustomAttributeName
	 * 
	 * @param pPaymentCustomAttributeName the paymentCustomAttributeName to set
	 */
	public void setPaymentCustomAttributeName(String pPaymentCustomAttributeName) {
		mPaymentCustomAttributeName = pPaymentCustomAttributeName;
	}

	/**
	 * This is to get the value for paymentCustomAttributeNameForApplePay
	 *
	 * @return the paymentCustomAttributeNameForApplePay value
	 */
	public String getPaymentCustomAttributeNameForApplePay() {
		return mPaymentCustomAttributeNameForApplePay;
	}

	/**
	 * This method to set the pPaymentCustomAttributeNameForApplePay with paymentCustomAttributeNameForApplePay
	 * 
	 * @param pPaymentCustomAttributeNameForApplePay the paymentCustomAttributeNameForApplePay to set
	 */
	public void setPaymentCustomAttributeNameForApplePay(
			String pPaymentCustomAttributeNameForApplePay) {
		mPaymentCustomAttributeNameForApplePay = pPaymentCustomAttributeNameForApplePay;
	}

	/**
	 * This is to get the value for paymentCustomAttributeValueForApplePay
	 *
	 * @return the paymentCustomAttributeValueForApplePay value
	 */
	public String getPaymentCustomAttributeValueForApplePay() {
		return mPaymentCustomAttributeValueForApplePay;
	}

	/**
	 * This method to set the pPaymentCustomAttributeValueForApplePay with paymentCustomAttributeValueForApplePay
	 * 
	 * @param pPaymentCustomAttributeValueForApplePay the paymentCustomAttributeValueForApplePay to set
	 */
	public void setPaymentCustomAttributeValueForApplePay(
			String pPaymentCustomAttributeValueForApplePay) {
		mPaymentCustomAttributeValueForApplePay = pPaymentCustomAttributeValueForApplePay;
	}

	/**
	 * This is to get the value for paymentCustomAttributeValueMap
	 *
	 * @return the paymentCustomAttributeValueMap value
	 */
	public Map<String, String> getPaymentCustomAttributeValueMap() {
		return mPaymentCustomAttributeValueMap;
	}

	/**
	 * This method to set the pPaymentCustomAttributeValueMap with paymentCustomAttributeValueMap
	 * 
	 * @param pPaymentCustomAttributeValueMap the paymentCustomAttributeValueMap to set
	 */
	public void setPaymentCustomAttributeValueMap(
			Map<String, String> pPaymentCustomAttributeValueMap) {
		mPaymentCustomAttributeValueMap = pPaymentCustomAttributeValueMap;
	}

	/**
	 * This is to get the value for includeJavaScript
	 *
	 * @return the includeJavaScript value
	 */
	public boolean isIncludeJavaScript() {
		return mIncludeJavaScript;
	}

	/**
	 * This method to set the pIncludeJavaScript with includeJavaScript
	 * 
	 * @param pIncludeJavaScript the includeJavaScript to set
	 */
	public void setIncludeJavaScript(boolean pIncludeJavaScript) {
		mIncludeJavaScript = pIncludeJavaScript;
	}

	/**
	 * This is to get the value for javascriptData
	 *
	 * @return the javascriptData value
	 */
	public String getJavascriptData() {
		return mJavascriptData;
	}

	/**
	 * This method to set the pJavascriptData with javascriptData
	 * 
	 * @param pJavascriptData the javascriptData to set
	 */
	public void setJavascriptData(String pJavascriptData) {
		mJavascriptData = pJavascriptData;
	}

	/**
	 * This is to get the value for includeDummyCookie
	 *
	 * @return the includeDummyCookie value
	 */
	public boolean isIncludeDummyCookie() {
		return mIncludeDummyCookie;
	}

	/**
	 * This method to set the pIncludeDummyCookie with includeDummyCookie
	 * 
	 * @param pIncludeDummyCookie the includeDummyCookie to set
	 */
	public void setIncludeDummyCookie(boolean pIncludeDummyCookie) {
		mIncludeDummyCookie = pIncludeDummyCookie;
	}

	/**
	 * This is to get the value for dummyCookie
	 *
	 * @return the dummyCookie value
	 */
	public String getDummyCookie() {
		return mDummyCookie;
	}

	/**
	 * This method to set the pDummyCookie with dummyCookie
	 * 
	 * @param pDummyCookie the dummyCookie to set
	 */
	public void setDummyCookie(String pDummyCookie) {
		mDummyCookie = pDummyCookie;
	}

	/**
	 * This is to get the value for countryCodeUS
	 *
	 * @return the countryCodeUS value
	 */
	public String getCountryCodeUS() {
		return mCountryCodeUS;
	}

	/**
	 * This method to set the pCountryCodeUS with countryCodeUS
	 * 
	 * @param pCountryCodeUS the countryCodeUS to set
	 */
	public void setCountryCodeUS(String pCountryCodeUS) {
		mCountryCodeUS = pCountryCodeUS;
	}

	/**
	 * This is to get the value for layawaySKUType
	 *
	 * @return the layawaySKUType value
	 */
	public String getLayawaySKUType() {
		return mLayawaySKUType;
	}

	/**
	 * This method to set the pLayawaySKUType with layawaySKUType
	 * 
	 * @param pLayawaySKUType the layawaySKUType to set
	 */
	public void setLayawaySKUType(String pLayawaySKUType) {
		mLayawaySKUType = pLayawaySKUType;
	}

	/**
	 * This is to get the value for orderCustomAttributePaypalId
	 *
	 * @return the orderCustomAttributePaypalId value
	 */
	public String getOrderCustomAttributePaypalId() {
		return mOrderCustomAttributePaypalId;
	}

	/**
	 * This method to set the pOrderCustomAttributePaypalId with orderCustomAttributePaypalId
	 * 
	 * @param pOrderCustomAttributePaypalId the orderCustomAttributePaypalId to set
	 */
	public void setOrderCustomAttributePaypalId(String pOrderCustomAttributePaypalId) {
		mOrderCustomAttributePaypalId = pOrderCustomAttributePaypalId;
	}

	/**
	 * This is to get the value for orderCustomAttributePaypalPayerId
	 *
	 * @return the orderCustomAttributePaypalPayerId value
	 */
	public String getOrderCustomAttributePaypalPayerId() {
		return mOrderCustomAttributePaypalPayerId;
	}

	/**
	 * This method to set the pOrderCustomAttributePaypalPayerId with orderCustomAttributePaypalPayerId
	 * 
	 * @param pOrderCustomAttributePaypalPayerId the orderCustomAttributePaypalPayerId to set
	 */
	public void setOrderCustomAttributePaypalPayerId(
			String pOrderCustomAttributePaypalPayerId) {
		mOrderCustomAttributePaypalPayerId = pOrderCustomAttributePaypalPayerId;
	}

	/**
	 * This is to get the value for sixMonthsFinancing
	 *
	 * @return the sixMonthsFinancing value
	 */
	public String getSixMonthsFinancing() {
		return mSixMonthsFinancing;
	}

	/**
	 * This method to set the pSixMonthsFinancing with sixMonthsFinancing
	 * 
	 * @param pSixMonthsFinancing the sixMonthsFinancing to set
	 */
	public void setSixMonthsFinancing(String pSixMonthsFinancing) {
		mSixMonthsFinancing = pSixMonthsFinancing;
	}

	/**
	 * This is to get the value for twelveMonthsFinancing
	 *
	 * @return the twelveMonthsFinancing value
	 */
	public String getTwelveMonthsFinancing() {
		return mTwelveMonthsFinancing;
	}

	/**
	 * This method to set the pTwelveMonthsFinancing with twelveMonthsFinancing
	 * 
	 * @param pTwelveMonthsFinancing the twelveMonthsFinancing to set
	 */
	public void setTwelveMonthsFinancing(String pTwelveMonthsFinancing) {
		mTwelveMonthsFinancing = pTwelveMonthsFinancing;
	}

	/**
	 * This is to get the value for paypalPayerStatus
	 *
	 * @return the paypalPayerStatus value
	 */
	public String getPaypalPayerStatus() {
		return mPaypalPayerStatus;
	}

	/**
	 * This method to set the pPaypalPayerStatus with paypalPayerStatus
	 * 
	 * @param pPaypalPayerStatus the paypalPayerStatus to set
	 */
	public void setPaypalPayerStatus(String pPaypalPayerStatus) {
		mPaypalPayerStatus = pPaypalPayerStatus;
	}

	/**
	 * This is to get the value for paypalAddressStatus
	 *
	 * @return the paypalAddressStatus value
	 */
	public String getPaypalAddressStatus() {
		return mPaypalAddressStatus;
	}

	/**
	 * This method to set the pPaypalAddressStatus with paypalAddressStatus
	 * 
	 * @param pPaypalAddressStatus the paypalAddressStatus to set
	 */
	public void setPaypalAddressStatus(String pPaypalAddressStatus) {
		mPaypalAddressStatus = pPaypalAddressStatus;
	}

	/**
	 * This is to get the value for shippingMethodMap
	 *
	 * @return the shippingMethodMap value
	 */
	public Map<String, String> getShippingMethodMap() {
		return mShippingMethodMap;
	}

	/**
	 * This method to set the pShippingMethodMap with shippingMethodMap
	 * 
	 * @param pShippingMethodMap the shippingMethodMap to set
	 */
	public void setShippingMethodMap(Map<String, String> pShippingMethodMap) {
		mShippingMethodMap = pShippingMethodMap;
	}

	/**
	 * This method is to get itemFeeNameEWaste
	 *
	 * @return the itemFeeNameEWaste
	 */
	public String getItemFeeNameEWaste() {
		return mItemFeeNameEWaste;
	}

	/**
	 * This method sets itemFeeNameEWaste with pItemFeeNameEWaste
	 *
	 * @param pItemFeeNameEWaste the itemFeeNameEWaste to set
	 */
	public void setItemFeeNameEWaste(String pItemFeeNameEWaste) {
		mItemFeeNameEWaste = pItemFeeNameEWaste;
	}

	/**
	 * This method is to get orderDatePropertyName
	 *
	 * @return the orderDatePropertyName
	 */
	public String getOrderDatePropertyName() {
		return mOrderDatePropertyName;
	}

	/**
	 * This method sets orderDatePropertyName with pOrderDatePropertyName
	 *
	 * @param pOrderDatePropertyName the orderDatePropertyName to set
	 */
	public void setOrderDatePropertyName(String pOrderDatePropertyName) {
		mOrderDatePropertyName = pOrderDatePropertyName;
	}

	/**
	 * This method is to get registryMessageProperty
	 *
	 * @return the registryMessageProperty
	 */
	public String getRegistryMessageProperty() {
		return mRegistryMessageProperty;
	}

	/**
	 * This method sets registryMessageProperty with pRegistryMessageProperty
	 *
	 * @param pRegistryMessageProperty the registryMessageProperty to set
	 */
	public void setRegistryMessageProperty(String pRegistryMessageProperty) {
		mRegistryMessageProperty = pRegistryMessageProperty;
	}

	/**
	 * This method is to get itemCustomAttributeListPrice
	 *
	 * @return the itemCustomAttributeListPrice
	 */
	public String getItemCustomAttributeListPrice() {
		return mItemCustomAttributeListPrice;
	}

	/**
	 * This method sets itemCustomAttributeListPrice with pItemCustomAttributeListPrice
	 *
	 * @param pItemCustomAttributeListPrice the itemCustomAttributeListPrice to set
	 */
	public void setItemCustomAttributeListPrice(String pItemCustomAttributeListPrice) {
		mItemCustomAttributeListPrice = pItemCustomAttributeListPrice;
	}

	/**
	 * This method is to get orderCustomAttributeOrderChannel
	 *
	 * @return the orderCustomAttributeOrderChannel
	 */
	public String getOrderCustomAttributeOrderChannel() {
		return mOrderCustomAttributeOrderChannel;
	}

	/**
	 * This method sets orderCustomAttributeOrderChannel with pOrderCustomAttributeOrderChannel
	 *
	 * @param pOrderCustomAttributeOrderChannel the orderCustomAttributeOrderChannel to set
	 */
	public void setOrderCustomAttributeOrderChannel(
			String pOrderCustomAttributeOrderChannel) {
		mOrderCustomAttributeOrderChannel = pOrderCustomAttributeOrderChannel;
	}

	/**
	 * This method is to get sourceChannelRegStore
	 *
	 * @return the sourceChannelRegStore
	 */
	public String getSourceChannelRegStore() {
		return mSourceChannelRegStore;
	}

	/**
	 * This method sets sourceChannelRegStore with pSourceChannelRegStore
	 *
	 * @param pSourceChannelRegStore the sourceChannelRegStore to set
	 */
	public void setSourceChannelRegStore(String pSourceChannelRegStore) {
		mSourceChannelRegStore = pSourceChannelRegStore;
	}

	/**
	 * This method is to get apoFposhippingMethodMap
	 *
	 * @return the apoFposhippingMethodMap
	 */
	public Map<String, String> getApoFposhippingMethodMap() {
		return mApoFposhippingMethodMap;
	}

	/**
	 * This method sets apoFposhippingMethodMap with pApoFposhippingMethodMap
	 *
	 * @param pApoFposhippingMethodMap the apoFposhippingMethodMap to set
	 */
	public void setApoFposhippingMethodMap(
			Map<String, String> pApoFposhippingMethodMap) {
		mApoFposhippingMethodMap = pApoFposhippingMethodMap;
	}

	/**
	 * This method is to get sourceChannelRegistry
	 *
	 * @return the sourceChannelRegistry
	 */
	public String getSourceChannelRegistry() {
		return mSourceChannelRegistry;
	}

	/**
	 * This method sets sourceChannelRegistry with pSourceChannelRegistry
	 *
	 * @param pSourceChannelRegistry the sourceChannelRegistry to set
	 */
	public void setSourceChannelRegistry(String pSourceChannelRegistry) {
		mSourceChannelRegistry = pSourceChannelRegistry;
	}

	/**
	 * This method is to get sourceChannelWishlist
	 *
	 * @return the sourceChannelWishlist
	 */
	public String getSourceChannelWishlist() {
		return mSourceChannelWishlist;
	}

	/**
	 * This method sets sourceChannelWishlist with pSourceChannelWishlist
	 *
	 * @param pSourceChannelWishlist the sourceChannelWishlist to set
	 */
	public void setSourceChannelWishlist(String pSourceChannelWishlist) {
		mSourceChannelWishlist = pSourceChannelWishlist;
	}

	/**
	 * This method is to get sourceChannelRegistryValue
	 *
	 * @return the sourceChannelRegistryValue
	 */
	public String getSourceChannelRegistryValue() {
		return mSourceChannelRegistryValue;
	}

	/**
	 * This method sets sourceChannelRegistryValue with pSourceChannelRegistryValue
	 *
	 * @param pSourceChannelRegistryValue the sourceChannelRegistryValue to set
	 */
	public void setSourceChannelRegistryValue(String pSourceChannelRegistryValue) {
		mSourceChannelRegistryValue = pSourceChannelRegistryValue;
	}

	/**
	 * This method is to get sourceChannelWishlistValue
	 *
	 * @return the sourceChannelWishlistValue
	 */
	public String getSourceChannelWishlistValue() {
		return mSourceChannelWishlistValue;
	}

	/**
	 * This method sets sourceChannelWishlistValue with pSourceChannelWishlistValue
	 *
	 * @param pSourceChannelWishlistValue the sourceChannelWishlistValue to set
	 */
	public void setSourceChannelWishlistValue(String pSourceChannelWishlistValue) {
		mSourceChannelWishlistValue = pSourceChannelWishlistValue;
	}

}
