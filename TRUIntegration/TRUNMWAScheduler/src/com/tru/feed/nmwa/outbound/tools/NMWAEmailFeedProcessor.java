package com.tru.feed.nmwa.outbound.tools;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;

/**
 * 
 * @author PA
 *
 */
@Component("itemProcessor")
@Scope(value = "step")
public class NMWAEmailFeedProcessor implements ItemProcessor<NMWASkuDetails, NMWASkuDetails> {

	@Value("#{stepExecutionContext[name]}")
	private String mThreadName;

	@Override
	public synchronized NMWASkuDetails process(NMWASkuDetails pItem){
		return pItem;
	}

	/**
	 * 
	 * @return the Thread
	 */
	public String getThreadName() {
		return mThreadName;
	}

	/**
	 * 
	 * @param pThreadName the thread
	 */
	public void setThreadName(String pThreadName) {
		mThreadName = pThreadName;
	}


}
