package com.tru.feed.nmwa.outbound.tools;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;


/**
 * AbstractTasklet is a custom abstract class which will implement the Tasklet interface.
 * This class will be overridden by a further child class that
 * will be used to execute every step of a job.
 *
 * @author PA
 */
public abstract class AbstractTasklet extends GenericService implements Tasklet {

	/** The mphase name. */
	private String mPhaseName;


	/** The m step name. */
	private String mStepName;


	/** The m feed context. */
	private FeedContext mFeedContext;

	/**
	 * Gets the feed context.
	 *
	 * @return the mFeedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * Gets the step name.
	 *
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Sets the step name.
	 *
	 * @param pStepName
	 *            the new step name
	 */
	private void setStepName(String pStepName) {
		this.mStepName = pStepName;
	}


	/**
	 * Save data.
	 *
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	protected void saveData(String pCongifKey, Object pConfigValue) {
		getFeedContext().setData(pCongifKey, pConfigValue);

	}

	/**
	 * Retrive data.
	 *
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	protected Object retriveData(String pDataMapKey) {

		return getFeedContext().getData(pDataMapKey);

	}


	/**
	 * Gets the phase name.
	 *
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Sets the phase name.
	 *
	 * @param pPhaseName
	 *            the new phase name
	 */
	public void setPhaseName(String pPhaseName) {
		mPhaseName = pPhaseName;
	}

	/**
	 * @param pStepContribution - StepContribution
	 * @param pChunkContext - ChunkContext
	 * @throws Exception - Exception
	 * @return RepeatStatus - RepeatStatus
	 */
	@Override
	public RepeatStatus execute(StepContribution pStepContribution,
			ChunkContext pChunkContext) throws Exception {
		// setting the step name
		setStepName(pChunkContext.getStepContext().getStepName());
		// Setting the phase name so that the step name can be accessed for
		// notifying purpose
		pChunkContext.getStepContext().setAttribute(TRUNMWASchedulerConstants.PHASE_NAME,
				getPhaseName());
		   if(isLoggingDebug()){
		   logDebug(getStepName() + " started");
		   }
		try {
			doExecute();
		}
		catch (Exception pExe) {
			if(isLoggingDebug()){
				logDebug("Getting Exception : ",pExe);
			}if(isLoggingError()){
				logError("Getting Error :", pExe);
			}

		}
		return RepeatStatus.FINISHED;
	}

	/**
	 * Common method of all kinds of tasklets in feed process.
	 *
	 */
	protected abstract void doExecute();

}
