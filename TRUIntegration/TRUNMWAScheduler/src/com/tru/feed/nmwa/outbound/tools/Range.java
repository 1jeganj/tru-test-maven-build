package com.tru.feed.nmwa.outbound.tools;

// TODO: Auto-generated Javadoc
/**
 * The Class Range.
 */
public class Range
{
	
	/** The From. */
	private int mFrom;
	
	/** The To. */
	private int mTo;
	
	/**
	 * Instantiates a new range.
	 *
	 * @param pFrom the from
	 * @param pTo the to
	 */
	public Range(int pFrom,int pTo)
	{
		mFrom=pFrom;
		mTo=pTo;
	}
	
	/**
	 * Gets the from.
	 *
	 * @return the from
	 */
	public int getFrom() {
		return mFrom;
	}
	
	/**
	 * Sets the from.
	 *
	 * @param pFrom the new from
	 */
	public void setFrom(int pFrom) {
		mFrom = pFrom;
	}
	
	/**
	 * Gets the to.
	 *
	 * @return the to
	 */
	public int getTo() {
		return mTo;
	}
	
	/**
	 * Sets the to.
	 *
	 * @param pTo the new to
	 */
	public void setTo(int pTo) {
		mTo = pTo;
	}
	
	
}
