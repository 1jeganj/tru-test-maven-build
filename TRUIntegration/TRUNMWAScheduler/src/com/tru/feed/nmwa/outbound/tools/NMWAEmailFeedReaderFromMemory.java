package com.tru.feed.nmwa.outbound.tools;

import java.util.Iterator;
import java.util.List;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;
/**
 * The Class NMWAEmailFeedReaderFromMemory.
 */
public class NMWAEmailFeedReaderFromMemory extends AbstractFeedItemReader{
	/**
	 * The m iterator.
	 */
	Iterator<? extends NMWASkuDetails> mIterator = null;
	/**
	 * Next.
	 *
	 * @return the base feed process vo
	 */
	protected NMWASkuDetails next()
	{
		if (mIterator.hasNext()) {
				return mIterator.next();
			}
		return null;
	}
	/**
	 * (non-Javadoc).
	 *
	 * @see com.liverpool.feed.export.tools.AbstractFeedItemReader#doOpen()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public   void  doOpen()  {
		synchronized(this) {
		List<NMWASkuDetails> NMWASkuDetailsList=(List<NMWASkuDetails>)getFeedContext().getData(TRUNMWASchedulerConstants.EXPORT_LIST);
		mIterator = NMWASkuDetailsList.iterator();
		}
	}

	/**
	 * (non-Javadoc).
	 *
	 * @return the base feed process vo
	 * @see com.liverpool.feed.export.tools.AbstractFeedItemReader#doRead()
	 */
	@Override
	protected NMWASkuDetails doRead() {
		return next();
	}

	/**
	 * (non-Javadoc).
	 *
	 * @return the base feed process vo
	 * @see com.liverpool.feed.export.tools.AbstractFeedItemReader#read()
	 */
	@Override
	public NMWASkuDetails read(){
		return next();
	}

}
