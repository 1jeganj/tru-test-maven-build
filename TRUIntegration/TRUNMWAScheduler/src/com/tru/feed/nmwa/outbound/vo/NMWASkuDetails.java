package com.tru.feed.nmwa.outbound.vo;
/***
 * 
 * The Class NMWASkuDetails.
 *
 */
public class NMWASkuDetails
{
	/**
	 * property to hold the skuId.
	 */
	private String mSkuId;
	/**
	 * property to hold the productId.
	 */
	private String mProductId;
	/**
	 * property to hold the id.
	 */
	private String mId ;
		/**
	 * property to hold the emailID.
	 */
	private String mEmailID;
	/**
	 * property to hold the notifyMeFlag.
	 */
	private String mNotifyMeFlag;
	/**
	 * property to hold the productItemDesc.
	 */
	private String mProductItemDesc;
	/**
	 * property to hold the productItemName.
	 */
	private String mProductItemName;
	/**
	 * property to hold the currentSiteID.
	 */
	private String mCurrentSiteID;
	/**
	 * property to hold the numOfUsersToSend.
	 */
	private int mNumOfUsersToSend;
	/**
	 * property to hold the counter.
	 */
	private int mCounter;
	/**
	 * property to hold the pdpPageURL.
	 */
	private String mPdpPageURL;
	/**
	 * property to hold the helpURLName.
	 */
	private String mHelpURLName ;
	/**
	 * property to hold the helpURLValue.
	 */
	private String mHelpURLValue;
	/***
	 * 
	 * @return mSkuId
	 */
	public String getSkuId() {
		return mSkuId;
	}
	/***
	 * 
	 * @param pSkuId the SkuId to set
	 */
	public void setSkuId(String pSkuId) {
		this.mSkuId = pSkuId;
	}
	/***
	 * 
	 * @return mProductId
	 */
	public String getProductId() {
		return mProductId;
	}
	/***
	 * 
	 * @param pProductId the ProductId to set
	 */
	public void setProductId(String pProductId) {
		this.mProductId = pProductId;
	}
	/***
	 * 
	 * @return mId
	 */
	public String getId() {
		return mId;
	}
	/**
	 * 
	 * @param pId the Id to set
	 */
	public void setId(String pId) {
		this.mId = pId;
	}
	/***
	 * 
	 * @return mEmailID
	 */
	public String getEmailID() {
		return mEmailID;
	}
	/**
	 * 
	 * @param pEmailID the EmailID to set
	 */
	public void setEmailID(String pEmailID) {
		this.mEmailID = pEmailID;
	}
	/***
	 * 
	 * @return mNotifyMeFlag
	 */
	public String getNotifyMeFlag() {
		return mNotifyMeFlag;
	}
	/**
	 * 
	 * @param pNotifyMeFlag the NotifyMeFlag to set
	 */
	public void setNotifyMeFlag(String pNotifyMeFlag) {
		this.mNotifyMeFlag = pNotifyMeFlag;
	}
	/**
	 * 
	 * @return mProductItemDesc
	 */
	public String getProductItemDesc() {
		return mProductItemDesc;
	}
	/***
	 * 
	 * @param pProductItemDesc the ProductItemDesc to set
	 */
	public void setProductItemDesc(String pProductItemDesc) {
		this.mProductItemDesc = pProductItemDesc;
	}
	/**
	 * 
	 * @return mProductItemName
	 */
	public String getProductItemName() {
		return mProductItemName;
	}
	/**
	 * 
	 * @param pProductItemName the ProductItemName to set
	 */
	public void setProductItemName(String pProductItemName) {
		this.mProductItemName = pProductItemName;
	}
	/***
	 * 
	 * @return mCurrentSiteID
	 */
	public String getCurrentSiteID() {
		return mCurrentSiteID;
	}
	/***
	 * 
	 * @param pCurrentSiteID the CurrentSiteID to set
	 */
	public void setCurrentSiteID(String pCurrentSiteID) {
		this.mCurrentSiteID = pCurrentSiteID;
	}
	/***
	 * 
	 * @return mNumOfUsersToSend
	 */
	public int getNumOfUsersToSend() {
		return mNumOfUsersToSend;
	}
	/**
	 * 
	 * @param pNumOfUsersToSend the NumOfUsersToSend to set
	 */
	public void setNumOfUsersToSend(int pNumOfUsersToSend) {
		this.mNumOfUsersToSend = pNumOfUsersToSend;
	}
	/**
	 * 
	 * @return mCounter
	 */
	public int getCounter() {
		return mCounter;
	}
	/**
	 * 
	 * @param pCounter the Counter to set
	 */
	public void setCounter(int pCounter) {
		this.mCounter = pCounter;
	}
	/**
	 * 
	 * @return mPdpPageURL
	 */
	public String getPdpPageURL() {
		return mPdpPageURL;
	}
	/**
	 * 
	 * @param pPdpPageURL the PdpPageURL to set
	 */
	public void setPdpPageURL(String pPdpPageURL) {
		this.mPdpPageURL = pPdpPageURL;
	}
	/**
	 * 
	 * @return mHelpURLName
	 */
	public String getHelpURLName() {
		return mHelpURLName;
	}
	/**
	 * 
	 * @param pHelpURLName the HelpURLName to set
	 */
	public void setHelpURLName(String pHelpURLName) {
		this.mHelpURLName = pHelpURLName;
	}
	/**
	 * 
	 * @return mHelpURLValue
	 */
	public String getHelpURLValue() {
		return mHelpURLValue;
	}
	/**
	 * 
	 * @param pHelpURLValue the HelpURLValue to set
	 */
	public void setHelpURLValue(String pHelpURLValue) {
		this.mHelpURLValue = pHelpURLValue;
	}

}
