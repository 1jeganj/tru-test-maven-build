package com.tru.feed.nmwa.outbound.scheduler;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.feed.nmwa.outbound.tools.FeedExportInvoker;
import com.tru.feed.nmwa.outbound.tools.NMWAEMailItemReader;
import com.tru.feed.nmwa.outbound.tools.NMWAEmailItemWriterToMemory;
/**
 *
 * The Class NMWAEmailScheduler
 *
 */
public class NMWAEmailScheduler extends SingletonSchedulableService {

	/** Property to hold mExportJobInvoker */
	private FeedExportInvoker mExportJobInvoker;
	/**
	 * Property to hold mNmwaEMailItemReader
	 */
	private NMWAEMailItemReader mNmwaEMailItemReader;
	/**
	 * Property to hold mNmwaEmailItemWriterToMemory
	 */
	private NMWAEmailItemWriterToMemory mNmwaEmailItemWriterToMemory;

	/** Property to hold mEnable */

	private boolean mEnable;

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return mEnable;
	}

	/**
	 * @param pEnable the enable to set
	 */
	public void setEnable(boolean pEnable) {
		mEnable = pEnable;
	}

	/**
	 *
	 * @return the Export Job
	 */

	public FeedExportInvoker getExportJobInvoker() {
		return mExportJobInvoker;
	}

	/**
	 *
	 * @param pExportJobInvoker the Job Invoker
	 *
	 */
	public void setExportJobInvoker(FeedExportInvoker pExportJobInvoker) {
		mExportJobInvoker = pExportJobInvoker;
	}
	/**
	 *
	 * @return mNmwaEMailItemReader
	 */
	public NMWAEMailItemReader getNmwaEMailItemReader() {
		return mNmwaEMailItemReader;
	}
    /**
     *
     * @param pNmwaEMailItemReader the NmwaEMailItemReader to set
     */
	public void setNmwaEMailItemReader(NMWAEMailItemReader pNmwaEMailItemReader) {
		this.mNmwaEMailItemReader = pNmwaEMailItemReader;
	}

	/**
	 * <b>This method will invoke the task based on the ScheduledJob name at the
	 * scheduled time</b><br>
	 * .
	 *
	 * @param pScheduler
	 *            - Scheduler
	 * @param pScheduledJob
	 *            - ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAEmailScheduler.doScheduledTask()");
			logDebug("Entering into :: getNmwaEMailItemReader().getNmwaSKUIdMap()=-==="+getNmwaEMailItemReader().getNmwaSKUIdMap().size());
		}
        if(isEnable()){
           doNMWAEmailJob();
           doCleanFinally();

        }
		if (isLoggingDebug()) {
			logDebug("Exit From :: NMWAEmailScheduler.doScheduledTask()");
		}
	}
	/**
	 * This method invokes job scheduling process
	 */
	public void doNMWAEmailJob(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAEmailScheduler.doNMWAEmailJob()");
		}
		getExportJobInvoker().doExportFeedJob();
		if (isLoggingDebug()) {
			logDebug("Exit from :: NMWAEmailScheduler.doNMWAEmailJob()");
		}
	}
	/**
	 * This method invokes job scheduling process manually
	 */
	public void doGenerateManually()
	{
		if (isLoggingDebug()) {
			logDebug("Entering into :: NMWAEmailScheduler.doGenerateManually()");
		}
		 doNMWAEmailJob();
         doCleanFinally();
     	if (isLoggingDebug()) {
			logDebug("Exit from :: NMWAEmailScheduler.doGenerateManually()");
		}
	}
	/**
	 * This method clears empty map after end of process
	 */
	public void  doCleanFinally()
	{
		getNmwaEMailItemReader().getNmwaSKUIdMap().clear();
		getNmwaEmailItemWriterToMemory().getmNMWASkuDetailsList().clear();

	}

	/**
	 * @return the mNmwaEmailItemWriterToMemory
	 */
	public NMWAEmailItemWriterToMemory getNmwaEmailItemWriterToMemory() {
		return mNmwaEmailItemWriterToMemory;
	}

	/**
	 * @param pNmwaEmailItemWriterToMemory the NmwaEmailItemWriterToMemory to set
	 */
	public void setNmwaEmailItemWriterToMemory(
			NMWAEmailItemWriterToMemory pNmwaEmailItemWriterToMemory) {
		this.mNmwaEmailItemWriterToMemory = pNmwaEmailItemWriterToMemory;
	}
}
