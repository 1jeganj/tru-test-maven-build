package com.tru.feed.nmwa.outbound.tools;

import java.util.Iterator;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class PartitionManager.
 */
public class ExportPartitionManager {

	/** The first. */
	private boolean mFirst=true;
	
	/** The Iterator. */
	private Iterator<Range> mIterator = null;
	
	/** The Range. */
	private List<Range> mRange;

	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public List<Range> getRange() {
		return mRange;
	}

	/**
	 * Sets the range.
	 *
	 * @param pRange the new range
	 */
	public void setRange(List<Range> pRange) {
		mRange = pRange;
	}
	
	/**
	 * Gets the next range.
	 *
	 * @return the next range
	 */
	public Range getNextRange()
	{
		if(mFirst)
		{
			mIterator=getRange().iterator();
		}
		return mIterator.next();
		
	}
	
	
}
