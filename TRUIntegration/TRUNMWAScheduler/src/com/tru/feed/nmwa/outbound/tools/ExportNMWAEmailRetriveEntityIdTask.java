package com.tru.feed.nmwa.outbound.tools;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.TransactionManager;

import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;

import com.tru.commerce.inventory.TRUCoherenceInventoryManager;
import com.tru.commerce.inventory.TRUInventoryInfo;
import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * This class retreive the sku details  after firing the query for  nmwa alerts
 *
 *
 */
public class ExportNMWAEmailRetriveEntityIdTask extends AbstractTasklet {


	/** The m entity result set extractor. */
	private EntityResultSetExtractor mEntityResultSetExtractor = new EntityResultSetExtractor();
	/***
	 * Property to hold mNMWARepository
	 */
	private Repository mNMWARepository;
	/**
	 * Property to hold mNMWARepositoryQuery
	 */
	private NMWARepositoryQuery mNMWARepositoryQuery;

	TRUPropertyManager mPropertyManager;
	/**
	 * Property to hold mCoherenceInventoryManager
	 */
	private TRUCoherenceInventoryManager mCoherenceInventoryManager;
	/**
	 * Property to hold mChunkValue;
	 */
	private int mChunkValue;

	/**
	 * @return the entityResultSetExtractor
	 */
	public EntityResultSetExtractor getEntityResultSetExtractor() {
		return mEntityResultSetExtractor;
	}
	/**
	 * @param pEntityResultSetExtractor
	 *            the entityResultSetExtractor to set
	 */
	public void setEntityResultSetExtractor(
			EntityResultSetExtractor pEntityResultSetExtractor) {
		mEntityResultSetExtractor = pEntityResultSetExtractor;
	}
	/**
	 *  Property to hold mTransactionManager
	 */
	private TransactionManager mTransactionManager;
	/**
	 * Property to hold mFeedContext
	 */
	private FeedContext mFeedContext;

	/**
	 * @return the feedContext
	 */
	@Override
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * @param pFeedContext the feedContext to set
	 */
	@Override
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}
	/**
	 * @return the transactionManager
	 */

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	/**
	 * @param pTransactionManager
	 *            the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/** The m data source name. */
	private String mDataSourceName;

	/**
	 * Gets the data source name.
	 *
	 * @return the mDataSource
	 */
	public String getDataSourceName() {
		return mDataSourceName;
	}

	/**
	 * Sets the data source name.
	 *
	 * @param pDataSourceName
	 *            the new data source name
	 */
	public void setDataSourceName(String pDataSourceName) {
		mDataSourceName = pDataSourceName;
	}
	/**
	 * @return the nMWARepository
	 */

	public Repository getNMWARepository() {
		return mNMWARepository;
	}

	/**
	 * @param pNMWARepository
	 *            the nMWARepository to set
	 */
	public void setNMWARepository(Repository pNMWARepository) {
		mNMWARepository = pNMWARepository;
	}

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	/**
	 *
	 * @return  mNMWARepositoryQuery
	 */
	public NMWARepositoryQuery getNMWARepositoryQuery() {
		return mNMWARepositoryQuery;
	}
	/***
	 *
	 * @param pNMWARepositoryQuery the NMWARepositoryQuery to set
	 */
	public void setNMWARepositoryQuery(NMWARepositoryQuery pNMWARepositoryQuery) {
		this.mNMWARepositoryQuery = pNMWARepositoryQuery;
	}
	/**
	 *
	 * @return  mCoherenceInventoryManager
	 */
	public TRUCoherenceInventoryManager getCoherenceInventoryManager() {
		return mCoherenceInventoryManager;
	}
	/***
	 *
	 * @param pCoherenceInventoryManager  the CoherenceInventoryManager to set
	 */
	public void setCoherenceInventoryManager(TRUCoherenceInventoryManager pCoherenceInventoryManager) {
		this.mCoherenceInventoryManager = pCoherenceInventoryManager;
	}

	/**
	 *   This method retrive the productIds from query and get the productItems by calling catalog repository and set the productItems in the context
	 *   to export the feed data
	 */

	@Override
	protected void doExecute() {
		if (isLoggingDebug()) {
			Calendar cal = Calendar.getInstance();
			logDebug("Entered into ExportFeedRetriveEntityIdTask:doExecute() : "+cal.getTime().toString());
		}
		List<String> listOfSku ;
		List<TRUInventoryInfo> coherenceInventoryList;
		Map<String, Long> skuAndQunatityMap =null;
		List<Object> integratedColl = new ArrayList<Object>();
		Set<String> skuIDKeys = null;
		List<Object>SubSetSkuList = null;
		listOfSku = getNMWARepositoryQuery().getSkuValues();
		try {

			coherenceInventoryList = getCoherenceInventoryManager().queryBulkS2HStockLevel(listOfSku);
			if (isLoggingDebug())
			{
				logDebug(" coherenceInventoryList size " + coherenceInventoryList.size());
			}
			skuAndQunatityMap = new HashMap<String, Long>();
			for (TRUInventoryInfo inventoryVO : coherenceInventoryList)
			{
				skuAndQunatityMap.put(inventoryVO.getCatalogRefId(), inventoryVO.getQuantity());
			}
			getFeedContext().setData(TRUNMWASchedulerConstants.NMWA_SKU_QUANTITYMAP, skuAndQunatityMap);
			skuIDKeys = skuAndQunatityMap.keySet();
			if (isLoggingDebug()) {
				logDebug(" skuAndQunatityMap size " + skuAndQunatityMap.size());
				logDebug(" skuAndQunatityMap  " + skuAndQunatityMap);
				logDebug(" skuAndQunatityMap skuIDKeys " + skuIDKeys.size());
			}

			if (skuIDKeys.size()>getChunkValue())
			{
				int numberofLoops = skuIDKeys.size()/getChunkValue();
				double remainingRecords = skuIDKeys.size()%getChunkValue();
				if(remainingRecords>0) 
				{
					numberofLoops=numberofLoops+TRUNMWASchedulerConstants.NUMBER_ONE;
				}
				// skuIDKeys.
				SubSetSkuList = getSubSetColletion(skuIDKeys,numberofLoops);
				Iterator<Object> subCollectionOfSku = SubSetSkuList.iterator();
				while(subCollectionOfSku.hasNext())
				{
					
					Object subListCollection = subCollectionOfSku.next();

					getNmwaRepoList(integratedColl, subListCollection);
				}

			}
			else
			{
				getNmwaRepoList(integratedColl, skuIDKeys);
	
			}
			getFeedContext().setData(TRUNMWASchedulerConstants.NMWA_REPO_ITEMS, integratedColl);
			if (isLoggingDebug()) {
				logDebug("nmwaRepoList ::"+ integratedColl.size());
				logDebug("nmwaRepoList ::"+ integratedColl);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("execute the query and get the result" , e);

			}
		}
		if (isLoggingDebug()) {
			logDebug("ExportFeedRetriveEntityIdTask:doExecute() method :: END");

		}
	}
	/**
	 * 
	 * @param pIntegratedColl the IntegratedColl to set
	 * @param pSubListCollection the SubListCollection to set
	 * @throws RepositoryException - Repository exception
	 */
	private void getNmwaRepoList(List<Object> pIntegratedColl,
			Object pSubListCollection) throws RepositoryException {
		if (isLoggingDebug()) {
			logDebug("Entered into :: ExportNMWAEmailRetriveEntityIdTask:getNmwaRepoList()");
		}
		RepositoryItemDescriptor nmwaDesc;
		RepositoryView nmwaView;
		QueryBuilder nwmaQueryBuilder;
		RepositoryItem[] nmwaRepoItems;
		List nmwaRepoList;
		nmwaDesc = getNMWARepository().getItemDescriptor(getPropertyManager().getNmwaItemDescPropertyName());
		nmwaView = nmwaDesc.getRepositoryView();
		nwmaQueryBuilder = nmwaView.getQueryBuilder();
		QueryExpression emailSentFlag = nwmaQueryBuilder.createPropertyQueryExpression(getPropertyManager().getNmwaEmailSentFlag());
		QueryExpression emailSentFlagValue = nwmaQueryBuilder.createConstantQueryExpression(TRUNMWASchedulerConstants.NMWA_STRING_ZERO);
		Query emailSentFlagQuery = nwmaQueryBuilder.createComparisonQuery(emailSentFlag, emailSentFlagValue,QueryBuilder.EQUALS);
		/* QueryExpression that represents the property Check with collection of SKU's*/
		QueryExpression NmwaSKUIDProperty = nwmaQueryBuilder.createPropertyQueryExpression(getPropertyManager().getNmwaSkuIdPropertyName());
		QueryExpression NMWASkuValueExpression = nwmaQueryBuilder.createConstantQueryExpression(pSubListCollection);
		Query SKUQuery = nwmaQueryBuilder.createIncludesQuery(NMWASkuValueExpression, NmwaSKUIDProperty);
		/* BUILD AND Query that represents the property Check with collection of SKU's*/
		Query[] queryPieces = { SKUQuery, emailSentFlagQuery };
		Query andQuery = nwmaQueryBuilder.createAndQuery(queryPieces);
		String [] precachedPropertyNames = null;
		SortDirectives sortDirectives = new SortDirectives();
		sortDirectives.addDirective(new SortDirective(getPropertyManager().getNmwaReqDatePropertyName(),SortDirective.DIR_ASCENDING));

		/* finally, execute the query and get the results*/
		nmwaRepoItems = nmwaView.executeQuery(andQuery, new QueryOptions(TRUNMWASchedulerConstants.NUMBER_ZERO,TRUNMWASchedulerConstants.NMWA_MINUS_ONE, sortDirectives,precachedPropertyNames));
		if(nmwaRepoItems!=null)
		{
			nmwaRepoList = Arrays.asList(nmwaRepoItems);

			pIntegratedColl.addAll(nmwaRepoList);

		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: ExportNMWAEmailRetriveEntityIdTask:getNmwaRepoList()");
		}
	}
	/**
	 * 
	 * @param pSkuKeys  the SkuKeys to set
	 * @param pNumberofLoops the NumberofLoops to set
	 * @return subSetSkuList collection
	 */
	public   List<Object> getSubSetColletion(Set<String> pSkuKeys,int pNumberofLoops) {
		if (isLoggingDebug()) {
			logDebug("Entered into :: ExportNMWAEmailRetriveEntityIdTask:getSubSetColletion()");
		}
		int limit=getChunkValue();
		int startRange;
		int endrange=0;
		List<Object> subSetSkuList = new ArrayList<Object>(limit);
		int noOfLoop = pNumberofLoops;
		while(noOfLoop>0)
		{
			startRange=endrange;
			endrange=endrange+limit;
			List<String> SubSetSku = new ArrayList<String>(pSkuKeys);
			List<String> SubSetSku1 = null;
			if(noOfLoop==TRUNMWASchedulerConstants.NUMBER_ONE)
			{
				SubSetSku1 = new ArrayList<String>(SubSetSku.subList(startRange,SubSetSku.size()));
			}
			else
			{
				SubSetSku1 = new ArrayList<String>(SubSetSku.subList(startRange,endrange));     
			}
			subSetSkuList.add(SubSetSku1);
			noOfLoop=noOfLoop-TRUNMWASchedulerConstants.NUMBER_ONE;
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: ExportNMWAEmailRetriveEntityIdTask:getSubSetColletion()");
		}
		return subSetSkuList;
	}

	/**
	 * @return the mChunkValue
	 */
	public int getChunkValue() {
		return mChunkValue;
	}
	/**
	 * @param pChunkValue the ChunkValue to set
	 */
	public void setChunkValue(int pChunkValue) {
		this.mChunkValue = pChunkValue;
	}
}
