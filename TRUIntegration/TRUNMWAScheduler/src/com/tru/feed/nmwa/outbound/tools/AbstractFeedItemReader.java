package com.tru.feed.nmwa.outbound.tools;

import java.util.Map;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;


/**
 * AbstractFeedItemReader is an abstract class which will extend GenericService.
 * This class contains all the abstract methods and overridden by class
 * CatFeedItemReader which is used to read data from the memory and assign
 * it to different threads based on chunk size.
 *
 * @author PA
 */
public abstract class AbstractFeedItemReader extends GenericService implements
		ItemReader<NMWASkuDetails>, ItemStream {

 	/** The Individual thread range. */
 	private Map<String,Range> mIndividualThreadRange;

	/** The Exec context. */
	private ExecutionContext mExecContext;


	/** The mphase name. */
	private String mPhaseName;

	/** The mStepName name. */
	private String mStepName;

	/** The m feed context. */
	private FeedContext mFeedContext;

	/**
	 * Gets the exec context.
	 *
	 * @return the exec context
	 */
	public ExecutionContext getExecContext() {
		return mExecContext;
	}

	/**
	 * Sets the exec context.
	 *
	 * @param pExecContext the new exec context
	 */
	public void setExecContext(ExecutionContext pExecContext) {
		mExecContext = pExecContext;
	}

	/**
	 * Gets the feed context.
	 *
	 * @return the mFeedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * Gets the step name.
	 *
	 * @return the mStepName
	 */
	public String getStepName() {
		return mStepName;
	}

	/**
	 * Sets the step name.
	 *
	 * @param pStepName the new step name
	 */
	public void setStepName(String pStepName) {
		mStepName = pStepName;
	}

	/**
	 * Gets the phase name.
	 *
	 * @return the phase name
	 */
	public String getPhaseName() {
		return mPhaseName;
	}

	/**
	 * Sets the phase name.
	 *
	 * @param pPhaseName
	 *            the new phase name
	 */
	public void setPhaseName(String pPhaseName) {
		mPhaseName = pPhaseName;
	}


	/**
	 * Save data.
	 *
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	protected void saveData(String pCongifKey, Object pConfigValue) {
		getFeedContext().setData(pCongifKey, pConfigValue);

	}
	/**
	 * Do open.
	 *
	 */
	protected  void doOpen()  {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFedItemWriter:doOpen() method");
		}
	}

	/**
	 * Do close.
	 */
	protected void doClose()  {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFedItemWriter:doClose() method");
		}
	}
   /**
    *
    * @return NMWASkuDetails
    */
	protected abstract NMWASkuDetails doRead();


	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.batch.item.ItemStream#close()
	 */
	@Override
	public void close() throws ItemStreamException {
			doClose();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.batch.item.ItemStream#open(org.springframework.batch
	 * .item.ExecutionContext)
	 */
	@Override
	public  void  open(ExecutionContext pExecContext) throws ItemStreamException {
		synchronized (this) {
			setExecContext(pExecContext);
			mIndividualThreadRange=(Map<String, Range>) pExecContext.get(TRUNMWASchedulerConstants.RANGE_MAP);
			doOpen();
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.batch.item.ItemStream#update(org.springframework.
	 * batch.item.ExecutionContext)
	 */
	@Override
	public void update(ExecutionContext pArg0) throws ItemStreamException {
			doUpdate();
	}
	/**
	 * Do close.
	 *
	 */
	protected void doUpdate() {
		if(isLoggingDebug()){
			logDebug("Entered into AbstractFedItemWriter:doUpdate() method");
		}
	}

	/**
	 * Retrive data.
	 *
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	protected Object retriveData(String pDataMapKey) {

		return getFeedContext().getData(pDataMapKey);

	}
	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.batch.item.ItemReader#read()
	 */
	@Override
	public NMWASkuDetails read(){
		NMWASkuDetails VO = null;
		 VO = doRead();	
		 return VO;
	}
	/**
	 * Gets the start index.
	 *
	 * @return the start index
	 */
	public  int getStartIndex()
	{
	 synchronized (this) {
		return getExecContext().getInt(TRUNMWASchedulerConstants.FROM,0);
		}
	}
	/**
	 * Gets the last index.
	 *
	 * @param pDefaultSize the default size
	 * @return the last index
	 */
	public  int getLastIndex(int pDefaultSize)
	{
		synchronized (this) {
			return getExecContext().getInt(TRUNMWASchedulerConstants.TO,pDefaultSize);
		}
	 }
	/**
	 * Gets the range.
	 *
	 * @return the range
	 */
	public  Map<String,Range> getRange()
	{
		synchronized(this)
		{
		return mIndividualThreadRange;
		}
	}
}
