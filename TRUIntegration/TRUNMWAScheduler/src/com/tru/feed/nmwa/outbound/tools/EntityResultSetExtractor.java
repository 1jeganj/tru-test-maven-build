package com.tru.feed.nmwa.outbound.tools;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;


/**
 * The Class EntityResultSetExtractor.
 */
public class EntityResultSetExtractor implements ResultSetExtractor<Object> {

	/**
	 * Sets the mapper map.
	 * 
	 * @param pResultSet
	 *            the pResultSet
	 * @throws DataAccessException
	 *             the exception
	 * @throws SQLException
	 *             the exception
	 * @return lHashMap the lHashMap
	 */

	public List<String> extractData(ResultSet pResultSet) throws SQLException,
			DataAccessException {
		String lId = null;
		List<String> fixedChildProductIds = null;
		if (pResultSet != null) {
			pResultSet.next();

			int lCount = pResultSet.getMetaData().getColumnCount();
			int size = pResultSet.getRow();
			if (size > 0) {
				fixedChildProductIds = new ArrayList<String>();
				do {
					for (int i = TRUNMWASchedulerConstants.NUMBER_ONE; i <= lCount; i++) {
						lId = pResultSet.getString(TRUNMWASchedulerConstants.NUMBER_ONE);
						fixedChildProductIds.add(lId);
					}
				} while (pResultSet.next());
			}

		}
		return fixedChildProductIds;
	}

}
