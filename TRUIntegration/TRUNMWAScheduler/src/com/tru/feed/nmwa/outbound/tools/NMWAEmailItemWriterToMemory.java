package com.tru.feed.nmwa.outbound.tools;


import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;

/**
 * The Class NMWAEmailItemWriterToMemory.
 */
public class NMWAEmailItemWriterToMemory extends GenericService implements ItemWriter<NMWASkuDetails>,ItemStream {

	/** The m feed context. */
	private FeedContext mFeedContext;

	/**  The property m productVOs*/
	private final  List<NMWASkuDetails> mNMWASkuDetailsList=new ArrayList<NMWASkuDetails>();
	/**
	 * Gets the feed context.
	 *
	 * @return the mFeedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * Sets the feed context.
	 *
	 * @param pFeedContext
	 *            the new feed context
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * Save data.
	 *
	 * @param pCongifKey
	 *            the congif key
	 * @param pConfigValue
	 *            the config value
	 */
	protected void saveData(String pCongifKey, Object pConfigValue) {
		getFeedContext().setData(pCongifKey, pConfigValue);

	}

	/**
	 * Retrive data.
	 *
	 * @param pDataMapKey
	 *            the data map key
	 * @return the object
	 */
	protected Object retriveData(String pDataMapKey) {

		return getFeedContext().getData(pDataMapKey);

	}
	/**
	 *   The write method add the pVOItems and set it to the list of productVos
	 *   @param pVOItems - VOItems
	 */
	@Override
	public  void write(List<? extends NMWASkuDetails> pVOItems)  {
		if(isLoggingDebug()){
			logDebug("Entered Into ExportFeedItemWriterToMemory:write() method"+pVOItems.size());
		}
			if (pVOItems != null) {
            	 mNMWASkuDetailsList.addAll(pVOItems);
			}
		if(isLoggingDebug()){
			logDebug("Product Vo's List Size :: --->" + mNMWASkuDetailsList.size());
		}
	}

	/**
	 *   The close method holds the listProductVos object and set it to the context level
	 *   @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public  void  close() throws ItemStreamException {


		if(getFeedContext()!=null){

		//mNMWASkuDetailsListFinal.addAll(mNMWASkuDetailsList);
		getFeedContext().setData(TRUNMWASchedulerConstants.EXPORT_LIST, mNMWASkuDetailsList);

		if(isLoggingDebug()){
			logDebug("Entered  NMWAEmailItemWriterToMemory:close()");

			logDebug("Entered  NMWAEmailItemWriterToMemory:close()" + mNMWASkuDetailsList);
		}
		}else{
			if(isLoggingDebug()){
				logDebug("FeedContext is getting null in ExportFeedItemWriterToMemory:close()");
			}
		 }

	}

/**
 *   The open method initializes the list of productVOs
 *   @param pArg0 - Arg0
 *   @throws ItemStreamException - ItemStreamException
 */
	@Override
	public  void  open(ExecutionContext pArg0) throws ItemStreamException {

		if(isLoggingDebug()){
			logDebug("Entered Into ExportFeedItemWriterToMemory:open() method");
		}

	}

	/**
	 *   The update method needs to override but does not have any business logic
	 *   @param pArg0 - Arg0
	 *   @throws ItemStreamException - ItemStreamException
	 */
	@Override
	public   void update(ExecutionContext pArg0) throws ItemStreamException {
		if(isLoggingDebug()){
			logDebug("Entered Into ExportFeedItemWriterToMemory:update() method");
		}
	}
    /**
     *
     * @return the mNMWASkuDetailsList
     */
	public List<NMWASkuDetails> getmNMWASkuDetailsList() {
		return mNMWASkuDetailsList;
	}


}
