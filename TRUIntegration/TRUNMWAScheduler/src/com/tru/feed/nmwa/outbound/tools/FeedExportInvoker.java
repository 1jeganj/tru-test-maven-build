package com.tru.feed.nmwa.outbound.tools;

import java.util.Calendar;

import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;

/**
 * @author PA
 * 
 */
public class FeedExportInvoker extends GenericService {

	/* Property holding mClassPathVariables */
	private String mClassPathVariables;

	/* Property holding mJobName */
	private String mJobName;
	
	Calendar mCal = Calendar.getInstance();

	/**
	 * @return the jobName
	 */

	public String getJobName() {
		return mJobName;
	}

	/**
	 * @param pJobName
	 *            the jobName to set
	 */
	public void setJobName(String pJobName) {
		mJobName = pJobName;
	}

	/**
	 * @return the classPathVariables
	 */

	public String getClassPathVariables() {
		return mClassPathVariables;
	}

	/**
	 * @param pClassPathVariables
	 *            the classPathVariables to set
	 */
	public void setClassPathVariables(String pClassPathVariables) {
		mClassPathVariables = pClassPathVariables;
	}

	/**
	 * the Export Feed JOB
	 */
	public void doExportFeedJob() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: FeedExportInvoker.doExportFeedJob()");
			logDebug("Export Feed Started Time : " + mCal.getTime());
		}
		ExportBatchInvoker lBatchInvoker = new ExportBatchInvoker();
		lBatchInvoker.loadContext(getClassPathVariables().split(
				TRUNMWASchedulerConstants.COMA_SEPERATOR));
		String[] input = { getJobName() };
		lBatchInvoker.startJob(input);
		if (isLoggingDebug()) {
			logDebug("Exit From :: FeedExportInvoker.doExportFeedJob()");
			logDebug("Export Feed End Time : " + mCal.getTime());
		}
	}

}
