package com.tru.feed.nmwa.outbound.tools;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.batch.item.ItemStreamException;




import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;



import com.tru.commerce.catalog.TRUCatalogTools;
import com.tru.commerce.catalog.custom.TRUCatalogProperties;
import com.tru.endeca.utils.TRUProductPageUtil;
import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class NMWAEMailItemReader.
 */
public class NMWAEMailItemReader extends AbstractFeedItemReader {
    /**
     *  property holds mPropertyManager
     */
	TRUPropertyManager mPropertyManager;
	/**
	 * property holds mNmwaSKUIdMap
	 */
	Map<String, Object> mNmwaSKUIdMap = new HashMap<String, Object>();
	/**
	 * property holds mCatalogTools
	 */
	private TRUCatalogTools mCatalogTools;
	/**
	 * property holds TRUCatalogProperties
	 */
	TRUCatalogProperties mCatalogProperties;

	/**
	 * property to hold the mproductPageUtil.
	 */
	private TRUProductPageUtil mProductPageUtil;

	/***
	 * property to hold the mproductPageUtil.
	 */
	private  float mNmwaDefaultPercentage;
	/**
	 * @return the catalogProperties
	 */
	public TRUCatalogProperties getCatalogProperties() {
		return mCatalogProperties;
	}
	/**
	 * @param pCatalogProperties
	 *            the catalogProperties to set
	 */
	public void setCatalogProperties(TRUCatalogProperties pCatalogProperties) {
		mCatalogProperties = pCatalogProperties;
	}
	/**
	 * @return the catalogTools
	 */
	public TRUProductPageUtil getProductPageUtil() {
		return mProductPageUtil;
	}
	/**
	 * @return the catalogTools
	 */
	public TRUCatalogTools getCatalogTools() {
		return mCatalogTools;
	}
	/**
	 * @param pCatalogTools
	 *            the catalogTools to set
	 */
	public void setCatalogTools(TRUCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
	/**
	 * @param pProductPageUtil
	 *            the ProductPageUtil to set
	 */
	public void setProductPageUtil(TRUProductPageUtil pProductPageUtil) {
		mProductPageUtil = pProductPageUtil;
	}
	/**The nmwaSKUCounter */

	/** The Unique sub list. */
	private final ThreadLocal<List<RepositoryItem>> mUniqueSubList = new ThreadLocal<List<RepositoryItem>>();
	/** The Thread index. */
	private final ThreadLocal<Integer> mThreadIndex = new ThreadLocal<Integer>();
	/**
	 * This method gets the  NMWARepositoryItems from the context and based on the
	 * thread range, it partition the list. doOpen method started
	 */
	@Override
	public  void doOpen() {
		if (isLoggingDebug()) {
			logDebug("Entered into NMWAEmailItemReader:doOpen()");
		}
		synchronized(this) {
		mThreadIndex.set(Integer.valueOf(TRUNMWASchedulerConstants.NUMBER_ZERO));
		List<RepositoryItem> repositoryItemList = null;
		repositoryItemList = (List<RepositoryItem>) getFeedContext().getData(TRUNMWASchedulerConstants.NMWA_REPO_ITEMS);

		if (isLoggingDebug()) {
			logDebug("repositoryItemList in doOpen() " + repositoryItemList);
			logDebug("repositoryItemList in doOpen() " + repositoryItemList.size());
		}
		List<RepositoryItem> baseFeedProcessVOList = new ArrayList<RepositoryItem>();
		List<String> lEntityList = new ArrayList<String>();
		lEntityList.add(TRUNMWASchedulerConstants.EXPORT_FEED);
		Map<String, Range> lRange = getRange();
		if (isLoggingDebug()) {

			logDebug("lRange " + lRange);

		}
		for (String currentEntity : lEntityList) {

			Range lCurrentEntityRange = lRange.get(currentEntity);
			if (isLoggingDebug()) {
				logDebug("lCurrentEntityRange.getTo()" + lCurrentEntityRange.getTo());
			}
			if (repositoryItemList != null && !repositoryItemList.isEmpty() && lCurrentEntityRange.getTo() != 0) {
				if (isLoggingDebug()) {logDebug("Entered condition");}

				baseFeedProcessVOList.addAll(repositoryItemList.subList(lCurrentEntityRange.getFrom(), lCurrentEntityRange.getTo()));

				if (isLoggingDebug()) {

					logDebug("mBaseFeedProcessVOList" + baseFeedProcessVOList);
					logDebug("mBaseFeedProcessVOList" + baseFeedProcessVOList.size());

					logDebug("lCurrentEntityRange.getFrom()" + lCurrentEntityRange.getFrom());

					logDebug("lCurrentEntityRange.getTo()" + lCurrentEntityRange.getTo());
				}
			}
		}
		mUniqueSubList.set(baseFeedProcessVOList);
		mThreadIndex.set(Integer.valueOf(TRUNMWASchedulerConstants.NUMBER_ZERO));
		}
		if (isLoggingDebug()) {
			logDebug("Exit From NMWAEmailItemReader:doOpen()");
		}
	}

	/**
	 * This method returns the NMWASkuDetails having all the required
	 * properties which need to export for PriceAuditFeed
	 *
	 * @return PriceAuditFeedVO - NMWASkuDetails
	 */
	@Override
	protected NMWASkuDetails doRead()  {
		return next();
	}

	/**
	 * This method returns the NMWASkuDetails by calling the repositoryItem of NMWASkuDetails
	 *
	 * @return the base price audit feed process vo
	 */
	protected NMWASkuDetails next() {
		if (isLoggingDebug()) {
			logDebug("Entered into NMWAEmailItemReader:next()");
		}

		int lIndex = mThreadIndex.get().intValue();

		List<RepositoryItem> lBaseFeedProcessVOList = mUniqueSubList.get();
		if (lBaseFeedProcessVOList != null && 0 <= lIndex && lIndex < lBaseFeedProcessVOList.size()) {

			mThreadIndex.set(Integer.valueOf(lIndex + TRUNMWASchedulerConstants.NUMBER_ONE));
			NMWASkuDetails skuDetails = null;
			skuDetails = populateNMWASkuDetails(lBaseFeedProcessVOList.get(lIndex));

			return skuDetails;
		}
		if (isLoggingDebug()) {
			logDebug("Exit from NMWAEmailItemReader:next()");
		}
		return null;
	}

	/**
	 * This method returns the populateNMWASkuDetails with all the properties
	 * extracted from DB and set
	 *
	 * @param pNmwaRepoItem the NmwaRepoItem
	 * @return skuDetails
	 */
	private NMWASkuDetails populateNMWASkuDetails(RepositoryItem pNmwaRepoItem) {
		if (isLoggingDebug()) {
			logDebug("Entered into NMWAEMailItemReaderr:populateNMWASkuDetails()");
		}

		NMWASkuDetails skuDetails = new NMWASkuDetails() ;
		NMWASkuDetails skuDetailsFromMap = null;
		String skuId=null;
		String productId=null;
		String  emailID=null;
		String  id = null;
		productId = (String) pNmwaRepoItem.getPropertyValue(getPropertyManager().getNmwaProductIdPropertyName());
		skuId = (String) pNmwaRepoItem.getPropertyValue(getPropertyManager().getNmwaSkuIdPropertyName());
		emailID = (String) pNmwaRepoItem.getPropertyValue(getPropertyManager().getNmwaEmailAddressPropertyName());
		id = (String) pNmwaRepoItem.getPropertyValue(getPropertyManager().getNmwaIdPropertyName());
		if(!getNmwaSKUIdMap().containsKey(skuId))
		{
			try
			{
				skuDetailsFromMap = getStockAvailabilityStatus(skuId, productId);	
			}		
			catch(Exception e)
			{
				if (isLoggingError()) {
					logError("Exception", e);
				}	
			}
			mNmwaSKUIdMap.put(skuId,skuDetailsFromMap);
		}
		else
		{
			skuDetailsFromMap =(NMWASkuDetails)mNmwaSKUIdMap.get(skuId);
		}
		if(skuDetailsFromMap!=null)
		{

			skuDetails.setNumOfUsersToSend(skuDetailsFromMap.getNumOfUsersToSend());
			skuDetails.setCurrentSiteID(skuDetailsFromMap.getCurrentSiteID());
			skuDetails.setProductItemDesc(skuDetailsFromMap.getProductItemDesc());
			skuDetails.setProductItemName(skuDetailsFromMap.getProductItemName());
			skuDetails.setPdpPageURL(skuDetailsFromMap.getPdpPageURL());
			skuDetails.setHelpURLName(skuDetailsFromMap.getHelpURLName());
			skuDetails.setHelpURLValue(skuDetailsFromMap.getHelpURLValue());
			skuDetails.setEmailID(emailID);
			skuDetails.setId(id);
			skuDetails.setSkuId(skuId);
			skuDetails.setProductId(productId);

		}
		if (isLoggingDebug()) {

			logDebug("skuDetails for this : " + skuDetails.getEmailID() + " ***" +skuDetails.getId()+ "**** " + skuDetails.getSkuId()+ "*****" + skuDetails.getNumOfUsersToSend() );
			logDebug("skuDetails for this : " + skuDetails.getProductId() + " ***" +skuDetails.getProductItemName());
			logDebug("exit from into NMWAEMailItemReaderr:populateNMWASkuDetails()");
		}
		return skuDetails;
	}

	/**
	 * this method is used to check StockAvailabilityStatus
	 * @param pSkuId  the skuId
	 * @param pProductId the productId
	 * @return skuDetails the skuDetails
	 */
	@SuppressWarnings("unchecked")
	public NMWASkuDetails getStockAvailabilityStatus(String pSkuId, String pProductId)  {
		if (isLoggingDebug()) {
			logDebug("NMWAEmailItemReader.getStockAvailabilityStatus method :: START");
		}
		NMWASkuDetails skuDetails =null;
		String PdpPageURL=null;
		String helpURLValue=null;
		String onlinePID = null;
		RepositoryItem SKUItem;
		RepositoryItem productItem;
		long  stockLevel;
		Float percentage = new Float(TRUNMWASchedulerConstants.NMWA_PERCENTAGE);
		Float nmwaPercentage = new Float(TRUNMWASchedulerConstants.NMWA_PERCENTAGE);
		int noofusers = TRUNMWASchedulerConstants.NUMBER_ZERO;
		Map<String, Long> skuAndQunatityMap = null;
		String curSite =null;
		try {

			skuAndQunatityMap = (Map<String, Long>) getFeedContext().getData(TRUNMWASchedulerConstants.NMWA_SKU_QUANTITYMAP);
			SKUItem = getCatalogTools().findSKU(pSkuId);
			productItem = getCatalogTools().findProduct(pProductId);
			if (SKUItem != null) {
				//skuInfoVo = getCatalogTools().createSKUInfo(skuItem,productItem);
				//inventoryStatus=getCatalogTools().getInventoryStatus(SKUItem);
				//stockLevel=getCatalogTools().getInventory(SKUItem);
				stockLevel = skuAndQunatityMap.get(pSkuId);
				nmwaPercentage=(Float)SKUItem.getPropertyValue(getCatalogProperties().getNmwaPercentage());
				if(null==nmwaPercentage)
				{
					nmwaPercentage = getNmwaDefaultPercentage();
				}
				skuDetails= new NMWASkuDetails() ;
				percentage = (stockLevel * nmwaPercentage/TRUNMWASchedulerConstants.PERCENTAGE_CAL);
				noofusers = percentage.intValue();
				skuDetails.setNumOfUsersToSend(noofusers);
				if(isLoggingDebug())
				{
					logDebug("pProductId============="+pProductId);
					logDebug("stockLevel Id  ==========="+stockLevel);
					logDebug("nmwaPercentage  ==========="+nmwaPercentage);
					logDebug("Sku Id  ==========="+pSkuId);
					logDebug("percentage value ==========="+percentage);
					logDebug("noofusers value ==========="+noofusers);
					logDebug("skuDetails value ==========="+skuDetails.getNumOfUsersToSend());
				}
			}//skuitemchk
			if (productItem != null)
			{
				String productItemDesc = null;
				String productItemName  = null;
				Set<String> currentSiteID  = null;
				if(productItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName())!=null)
				{
					 productItemDesc = (String) productItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
				}
				// productItemName hold ProductDisplayName
				if(productItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName())!=null)
				{
					 productItemName = (String) productItem.getPropertyValue(getCatalogProperties().getProductDisplayNamePropertyName());
				}	
				if(productItem.getPropertyValue(getCatalogProperties().getSitesPropertyName())!=null)
				{
					 currentSiteID = (Set<String>) productItem.getPropertyValue(getCatalogProperties().getSitesPropertyName());
				}
				// currentSiteID hold SitesPropertyName
				if(isLoggingDebug())
				{
					logDebug("currentSiteID============="+currentSiteID);
				}
				for (String siteId : currentSiteID) {
				  if(!StringUtils.isBlank(siteId))
				  {
					  curSite= siteId;
					  break; 
				  }
				}
				if(skuDetails!=null)
				{
						skuDetails.setProductItemDesc(productItemDesc);
						skuDetails.setProductItemName(productItemName);
						skuDetails.setCurrentSiteID(curSite);
						
					  if(SKUItem!=null && SKUItem.getPropertyValue(getCatalogProperties().getOnlinePID())!=null)
					  {
						  onlinePID = (String)SKUItem.getPropertyValue(getCatalogProperties().getOnlinePID()) ;
					  }
					   PdpPageURL = getProductPageUtil().getProductPageURL(onlinePID, curSite);
								
						if(PdpPageURL !=null)
						{
							helpURLValue = PdpPageURL;
						}
							skuDetails.setPdpPageURL(PdpPageURL);
							skuDetails.setHelpURLName(TRUNMWASchedulerConstants.HELP_URL_NAME);
							skuDetails.setHelpURLValue(helpURLValue);
				}
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException", e);
			}
		}

		if (isLoggingDebug()) {
			logDebug("NMWAEmailItemReader.getStockAvailabilityStatus method :: END");
		}
		return skuDetails;
	}

	/**
	 * @return the mPropertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}
	/**
	 * @return the mNmwaSKUIdMap
	 */
	public Map<String, Object> getNmwaSKUIdMap() {
		return mNmwaSKUIdMap;
	}
	/**
	 * @param pNmwaSKUIdMap
	 *            the NmwaSKUIdMap to set
	 */
	public void setNmwaSKUIdMap(Map<String, Object> pNmwaSKUIdMap) {
		this.mNmwaSKUIdMap = pNmwaSKUIdMap;
	}

	/***
	 *
	 * @return mNmwaDefaultPercentage
	 */
	public float getNmwaDefaultPercentage() {
		return mNmwaDefaultPercentage;
	}
	/**
	 * @param pNmwaDefaultPercentage
	 *            the NmwaDefaultPercentage to set
	 */
	public void setNmwaDefaultPercentage(float pNmwaDefaultPercentage) {
		this.mNmwaDefaultPercentage = pNmwaDefaultPercentage;
	}
	/**
	 * This method invokes at end of chunk read operation
	 * @throws ItemStreamException
	 *             the exception
	 */
	@Override
	public void close() throws ItemStreamException {
		synchronized (this) {
			if (isLoggingDebug()) {
				logDebug("Entered into :: NMWAEMailItemReader:close()");
			}
		}
	}
}
