package com.tru.feed.nmwa.outbound.tools;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;
import com.tru.feed.nmwa.outbound.vo.NMWASkuDetails;
import com.tru.messaging.TRUEpsilonNotifyEmailSource;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * NMWAEmailFeedItemWriter class used to Send the email
 *
 * @author Professional Access
 * @version 1.0
 */
public class NMWAEmailFeedItemWriter extends GenericService implements
		ItemWriter<NMWASkuDetails>, ItemStream {
	/**
	 * property to hold mNMWARepository
	 */
	private Repository mNMWARepository;
	/**
	 * property to hold nmwaSKUIdCounter
	 */

	Map<String, Object> mNmwaSKUIdCounter = new HashMap<String, Object>();

	/**
	 * property to hold mFeedContext
	 */
	private FeedContext mFeedContext;

	/**
	 * property to hold the mEpsilonNotifyEmailSource.
	 */
	TRUEpsilonNotifyEmailSource mEpsilonNotifyEmailSource;

	/**
	 * @return the feedContext
	 */
	public FeedContext getFeedContext() {
		return mFeedContext;
	}

	/**
	 * @param pFeedContext
	 *            the feedContext to set
	 */
	public void setFeedContext(FeedContext pFeedContext) {
		mFeedContext = pFeedContext;
	}

	/**
	 * @return the epsilonNotifyEmailSource
	 */
	public TRUEpsilonNotifyEmailSource getEpsilonNotifyEmailSource() {
		return mEpsilonNotifyEmailSource;
	}

	/**
	 * @param pEpsilonNotifyEmailSource
	 *            the epsilonNotifyEmailSource to set
	 */
	public void setEpsilonNotifyEmailSource(
			TRUEpsilonNotifyEmailSource pEpsilonNotifyEmailSource) {
		mEpsilonNotifyEmailSource = pEpsilonNotifyEmailSource;
	}

	TRUPropertyManager mPropertyManager;

	/**
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * @param pPropertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * @return the nMWARepository
	 */

	public Repository getNMWARepository() {
		return mNMWARepository;
	}

	/**
	 * @param pNMWARepository
	 *            the nMWARepository to set
	 */
	public void setNMWARepository(Repository pNMWARepository) {
		mNMWARepository = pNMWARepository;
	}

	/**
	 * write method which export the data
	 *
	 * @param pSkuDetailsList
	 *            the SkuDetailsList to set
	 * @throws IOException
	 *             the exception
	 */
	@Override
	public void write(List<? extends NMWASkuDetails> pSkuDetailsList)
			throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into :: NMWAEmailFeedItemWriter:write()");
		}
		synchronized (this) {
			export(pSkuDetailsList);
		}
		if (isLoggingDebug()) {
			logDebug("Exit From :: NMWAEmailFeedItemWriter:write()");
		}
	}

	/**
	 * write method which export data into the DB
	 *
	 * @param pSkuDetailsList
	 *            the pSkuDetailsList
	 * @throws IOException
	 *             the Exception
	 */
	public void export(List<? extends NMWASkuDetails> pSkuDetailsList)
			throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entered into NMWAEmailFeedItemWriter:export(List<? extends NMWASkuDetails> pSkuDetailsList)"
					+ pSkuDetailsList);
		}
		// int maxRecords =
		// Integer.parseInt(getTruPriceAuditConfig().getMaxRecords());
		int skuCounter = 0;
		for (NMWASkuDetails skuDetailsVo : pSkuDetailsList) {
			if (StringUtils.isNotBlank(skuDetailsVo.getProductId())
					&& StringUtils.isNotBlank(skuDetailsVo.getSkuId())
					&& StringUtils.isNotBlank(skuDetailsVo.getEmailID())
					&& StringUtils.isNotBlank(skuDetailsVo.getId())
					&& StringUtils
							.isNotBlank(skuDetailsVo.getProductItemName())) {
				try {

					if (getNmwaSKUIdCounter().containsKey(
							skuDetailsVo.getSkuId())) {
						skuCounter = (int) getNmwaSKUIdCounter().get(
								skuDetailsVo.getSkuId());
					}

					if (skuCounter < skuDetailsVo.getNumOfUsersToSend()) {
						getEpsilonNotifyEmailSource().sendNotificationEmail(
								skuDetailsVo.getProductItemName(),
								skuDetailsVo.getProductItemName(),
								skuDetailsVo.getSkuId(),
								skuDetailsVo.getPdpPageURL(),
								skuDetailsVo.getHelpURLName(),
								skuDetailsVo.getHelpURLValue(),
								skuDetailsVo.getEmailID(),
								TRUNMWASchedulerConstants.NMWA_ACKNOWLEDGE);
						skuCounter++;
						writeToSentStatusToDB(skuDetailsVo);
					}

					getNmwaSKUIdCounter().put(skuDetailsVo.getSkuId(),
							skuCounter);

				} catch (JMSException e) {
					if (isLoggingError()) {
						logError("Exception occured while writing to DB", e);
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("NMWAEmailFeedItemWriter:export(List<? extends NMWASkuDetails> pPriceAuditList) "
					+ pSkuDetailsList);
		}

		if (isLoggingDebug()) {
			logDebug("ExitFrom NMWAEmailFeedItemWriter:export(List<? extends NMWASkuDetails> pPriceAuditList)");
		}
	}

	/**
	 * writeToSentStatusToDB method is used to update the send mail in DB
	 *
	 * @param pSkuDetailsVo
	 *            the SkuDetailsVo
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void writeToSentStatusToDB(NMWASkuDetails pSkuDetailsVo)
			throws IOException {

		updateProfileData(pSkuDetailsVo);

	}

	/**
	 * method is used for updateProfileData
	 *
	 * * @param pSkuDetailsList the SkuDetailsList
	 */
	private void updateProfileData(NMWASkuDetails pSkuDetailsList) {
		if (isLoggingDebug()) {
			logDebug("NMWAManager.updateProfileData method :: START");
		}
		/* SimpleDateFormat object */
		DateFormat dateFormat = new SimpleDateFormat(TRUNMWASchedulerConstants.DATE_FORMAT);
		/* get current date time with Date() */
		Date date = new Date();
		MutableRepository mutableRepository = (MutableRepository) getNMWARepository();
		MutableRepositoryItem mutableRepositoryItem = null;
		try {
			if (isLoggingDebug()) {
				logDebug("Print current date :: " + (dateFormat.format(date)));
			}
			mutableRepositoryItem = mutableRepository.getItemForUpdate(
					pSkuDetailsList.getId(), getPropertyManager()
							.getNmwaItemDescPropertyName());
			mutableRepositoryItem.setPropertyValue(getPropertyManager()
					.getNmwaEmailSentDatePropertyName(), date);
			mutableRepositoryItem.setPropertyValue(getPropertyManager()
					.getNmwaEmailSentFlag(), true);
			mutableRepository.updateItem(mutableRepositoryItem);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(
						"Exception in @Class::NMWAEmailFeedItemWriter::@method::updateProfileData()",
						e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("NMWAManager.updateProfileData method :: END");
		}
	}

	/**
	 * This update method does not have any business logic but have to override
	 *
	 * @param pExecutionContext
	 *            the ExecutionContext
	 * @throws ItemStreamException
	 *             the exception
	 */
	@Override
	public void update(ExecutionContext pExecutionContext)
			throws ItemStreamException {
		if (isLoggingDebug()) {
			logDebug("Entered into NMWAEmailFeedItemWriter:update()");
		}
	}

	/**
	 * This method is used to open the initial level tags
	 *
	 * @param pExecutionContext
	 *            the ExecutionContext
	 * @throws ItemStreamException
	 *             the exception
	 */
	@Override
	public  void open(ExecutionContext pExecutionContext)
			throws ItemStreamException {
		synchronized(this) {
			if (isLoggingDebug()) {
				logDebug("Entered into :: NMWAEmailFeedItemWriter:open()");
			}
			if (isLoggingDebug()) {
				logDebug("Exit From :: NMWAEmailFeedItemWriter:open()");
			}
		}
	}

	/**
	 * This method used to close the start tags which opened in open method and
	 * also perform the task to uplaod feed export file to the ftp location.
	 *
	 * @throws ItemStreamException
	 *             the exception
	 */

	@Override
	public  void close() throws ItemStreamException {
		synchronized(this) {
		getNmwaSKUIdCounter().clear();

		if (isLoggingDebug()) {
			logDebug("Entered into :: NMWAEmailFeedItemWriter:close()");
		 }
		}
	}

	/**
	 * @param getNmwaSKUIdCounter
	 * @return the mNmwaSKUIdCounter
	 */

	public Map<String, Object> getNmwaSKUIdCounter() {
		return mNmwaSKUIdCounter;
	}

	/**
	 * @param pNmwaSKUIdCounter
	 *            the pNmwaSKUIdCounter to set
	 */
	public void setNmwaSKUIdCounter(Map<String, Object> pNmwaSKUIdCounter) {
		this.mNmwaSKUIdCounter = pNmwaSKUIdCounter;
	}

}