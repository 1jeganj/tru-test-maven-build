package com.tru.feed.nmwa.outbound.tools;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import atg.adapter.gsa.GSARepository;
import atg.nucleus.GenericService;
import atg.repository.Repository;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;

/**
 * The Class NMWARepositoryQuery
 */
public class NMWARepositoryQuery extends GenericService {
	/**
	 * property holds EntitySQL
	 */
	private String mEntitySQL;
	/***
	 * property holds mNMWARepository
	 */
	private Repository mNMWARepository;
    /***
     * 
     * @return  mEntitySQL
     */
	public String getEntitySQL() {
		return mEntitySQL;
	}
  /***
   * 
   * @param pEntitySQL set the EntitySQL
   */
	public void setEntitySQL(String pEntitySQL) {
		this.mEntitySQL = pEntitySQL;
	}
	/**
	 * 
	 * @return mNMWARepository
	 */
	public Repository getNMWARepository() {
		return mNMWARepository;
	}
	/***
	 * 
	 * @param pNMWARepository set the NMWARepository
	 */
	public void setNMWARepository(Repository pNMWARepository) {
		this.mNMWARepository = pNMWARepository;
	}
	/**
	 * This is method to get sku values 
	 * @return skuList
	 */
	public  List<String> getSkuValues() {
		if(isLoggingDebug()){
			logDebug("Enter into : NMWARepositoryQuery and method : getSkuValues ");
		}
		Connection dbConnection = null;
		Statement stmt = null;
		ResultSet res = null;
		 String skuId=null;
		List<String> skuList = new ArrayList<String>();
		try {
			dbConnection = ((GSARepository)getNMWARepository()).getConnection();

			boolean autoCommit = dbConnection.getAutoCommit();

			dbConnection.setAutoCommit(false);

			stmt = dbConnection.createStatement();

			if (isLoggingInfo()) {
				logInfo("Started Gettting values .");
			}
			res = stmt.executeQuery(getEntitySQL());
		  	
		  	
		  	 while(res.next()){
		         //Retrieve by column name
					if (isLoggingInfo()) {
						logInfo("Iterating row  "+ res.getString(TRUNMWASchedulerConstants.SKU_ID_REPO));
					}
		        
		        skuId = res.getString(TRUNMWASchedulerConstants.SKU_ID_REPO);
		        skuList.add(skuId);

		         //Display values
		      }
			
			if (isLoggingInfo()) {
				logInfo("Completed Getting values ");
			}
			stmt.close();
			dbConnection.commit();
			dbConnection.setAutoCommit(autoCommit);

		} catch (SQLException exc) {
			if(isLoggingError()){
				vlogError("SQLException: While connecting to data base : {0}", exc);
			}
		}
		if (dbConnection != null){
			((GSARepository)getNMWARepository()).close(dbConnection);
		}

		if(isLoggingDebug()){
			logDebug("Exit from : NMWARepositoryQuery and method : getSkuValues ");
		}	
		
		return skuList;
	}

}
