/**
 * 
 */
package com.tru.jda.profilesync.outbound;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.jda.profilesync.TRUProfileSyncManager;

/**
 * @author PA
 *
 */
public class TRUProfileSyncOutBoundScheduler extends SingletonSchedulableService {
	
	/** The m enabled. */
	private boolean mEnabled;
	
	/** The m profile sync manager. */
	private TRUProfileSyncManager mProfileSyncManager;
	
	/**
	 * To generate the data of the profile which is updated in last 15 mins.
	 *
	 * @param pParamScheduler the param scheduler
	 * @param pParamScheduledJob the param scheduled job
	 */
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncOutBoundScheduler.doScheduledTask() method");
		}
		if(isEnabled()){
			generateProfileData();
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncOutBoundScheduler.doScheduledTask() method");
		}
	}

	/**
	 * Generate profile data.
	 */
	public void generateProfileData() {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncOutBoundScheduler.generateProfileData() method");
		}
			getProfileSyncManager().generateProfileDataAndMoveToSpecifiedLocation();
			if (isLoggingDebug()) {
				logDebug("Exiting from TRUProfileSyncOutBoundScheduler.generateProfileData() method");
			}
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		this.mEnabled = pEnabled;
	}

	/**
	 * Gets the profile sync manager.
	 *
	 * @return the profile sync manager
	 */
	public TRUProfileSyncManager getProfileSyncManager() {
		return mProfileSyncManager;
	}

	/**
	 * Sets the profile sync manager.
	 *
	 * @param pProfileSyncManager the new profile sync manager
	 */
	public void setProfileSyncManager(TRUProfileSyncManager pProfileSyncManager) {
		this.mProfileSyncManager = pProfileSyncManager;
	}

}
