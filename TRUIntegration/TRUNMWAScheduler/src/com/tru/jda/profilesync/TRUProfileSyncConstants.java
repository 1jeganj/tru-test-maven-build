package com.tru.jda.profilesync;

// TODO: Auto-generated Javadoc
/**
 * The Class TRUProfileSyncConstants.
 */
public class TRUProfileSyncConstants {
	/**
	 * Constant for SFTP.
	 */
	public static final String SFTP = "sftp://";

	/**
	 * Constant for COLON.
	 */
	public static final String COLON = ":";

	/**
	 * Constant for AT.
	 */
	public static final String AT = "@";

	/**
	 * Constant for SLASH.
	 */
	public static final String SLASH = "/";
	/**
	 * Constant for NO.
	 */
	public static final String NO = "no";
	/** This holds under score. */
	public static final String UNDER_SCORE = "_";

	/** The Constant NO_OFF. */
	public static final String NO_OFF_ITEMS = "noOfItems";
	
	public static final String OUT_BOUND_DECRYPTED_FILE_NAME = "outbounddecryptedfile";

	/** The Constant CASE. */
	public static final String CASE = "case";
	
	/** The Constant DATE_PATTERN. */
	public static final String DATE_PATTERN="yyyy-MM-dd HH:mm:ss";
	
	/** The Constant PROCESSED_FILES. */
	public static final String PROCESSED_FILES="processedFiles";
	
	/** The Constant TYPE. */
	public static final String TYPE="type";
	
	/** The Constant FILE_NOT_FOUND. */
	public static final String FILE_NOT_FOUND="fileNotFound";
	
	/** The Constant IN_BOUND_DESTINATION. */
	public static final String IN_BOUND_DESTINATION="inBoundDestination";
	
	/** The Constant IN_VALID_FILE. */
	public static final String IN_VALID_FILE="invalidFile";
	
	/** The Constant IN_VALID_FILES. */
	public static final String IN_VALID_FILES="invalidFiles";
	
	/** The Constant XML_EXTENSION. */
	public static final String XML_EXTENSION=".xml";
	
	/** The Constant TIMESTAMP_FORMAT_FOR_IMPORT. */
	public static final String TIMESTAMP_FORMAT_FOR_EXPORT = "yyyyMMdd_HHmmss";
	
	/** The Constant SECRET_KEY_NOT_FOUND. */
	public static final String SECRET_KEY_NOT_FOUND="secret key for message not found.";
	
	/** The Constant BC. */
	public static final String BC="BC";
	
	/** The Constant NO_LITERAL_DATA. */
	public static final String NO_LITERAL_DATA="encrypted message contains a signed message - not literal data.";
	
	/** The Constant TYPE_UNKNOWN. */
	public static final String TYPE_UNKNOWN="message is not a simple encrypted file - type unknown.";
	
	/** The Constant ENCRYPTION_KEY. */
	public static final String ENCRYPTION_KEY="Can't find encryption key in key ring.";
	
	/** The Constant SIGNING_KEY. */
	public static final String SIGNING_KEY="Can't find signing key in key ring.";
	
	/**
	 * Constant for Empty String.
	 */
	public static final String EMPTY_STRING = "";
	
	/** property to hold UNIFTP property.*/ 
	public static final String UNIFTP = "uniftp ";
	
	/** property to hold HUNIFTP property.*/ 
	public static final String HUNIFTP = " huniftp"; 
	
	/**
	 * Constant for  EMPTY_SPACE.
	 */
	public static final String EMPTY_SPACE = " ";
	
	/**
	 * Constant for NEW_LINE
	 */
	public static final String NEW_LINE = "\n";
	
	/**
	 * Constant for INT_ZERO
	 */
	public static final int INT_ZERO = 0;
	/**
	 * Constant for PROFILE_ID
	 */
	public static final String PROFILE_ID = "Profile_Id";
	/**
	 * Constant for ERROR_MESSAGE
	 */
	public static final String ERROR_MESSAGE = "Error_Message";
	/**
	 * Constant for UNMARSHALL_EXCEPTION
	 */
	public static final String UNMARSHALL_EXCEPTION = "unmarsall Exception";
	/**
	 * Constant for INVALID_RECORDS
	 */
	public static final String INVALID_RECORDS = "invalidRecords";
	/**
	 * Constant for FILE_NAME
	 */
	public static final String FILE_NAME = "fileName";
}
