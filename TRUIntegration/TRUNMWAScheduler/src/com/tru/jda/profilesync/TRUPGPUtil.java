package com.tru.jda.profilesync;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.util.Iterator;

import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;

/**
 * The Class TRUPGPUtil.
 */
public class TRUPGPUtil {

	/**
	 * Compress file.
	 * 
	 * @param pFileName
	 *            the file name
	 * @param pAlgorithm
	 *            the algorithm
	 * @return the byte[]
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	static byte[] compressFile(String pFileName, int pAlgorithm) throws IOException {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(pAlgorithm);
		PGPUtil.writeFileToLiteralData(comData.open(bOut), PGPLiteralData.BINARY, new File(pFileName));
		comData.close();
		return bOut.toByteArray();
	}

	/**
	 * Search a secret key ring collection for a secret key corresponding to
	 * keyID if it exists.
	 *
	 * @param pPgpSec            a secret key ring collection.
	 * @param pKeyID            keyID we want.
	 * @param pPass            passphrase to decrypt secret key with.
	 * @return the private key.
	 * @throws PGPException the PGP exception
	 * @throws NoSuchProviderException the no such provider exception
	 */
	static PGPPrivateKey findSecretKey(PGPSecretKeyRingCollection pPgpSec, long pKeyID, char[] pPass)
			throws PGPException, NoSuchProviderException {
		PGPSecretKey pgpSecKey = pPgpSec.getSecretKey(pKeyID);

		if (pgpSecKey == null) {
			return null;
		}

		return pgpSecKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
				.setProvider(TRUProfileSyncConstants.BC).build(pPass));
	}

	/**
	 * Read public key.
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the PGP public key
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws PGPException
	 *             the PGP exception
	 */
	static PGPPublicKey readPublicKey(String pFileName) throws IOException, PGPException {
		InputStream keyIn = new BufferedInputStream(new FileInputStream(pFileName));
		PGPPublicKey pubKey = readPublicKey(keyIn);
		keyIn.close();
		return pubKey;
	}

	/**
	 * A simple routine that opens a key ring file and loads the first available
	 * key suitable for encryption.
	 *
	 * @param pInput            data stream containing the public key data
	 * @return the first public key found.
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws PGPException the PGP exception
	 */
	static PGPPublicKey readPublicKey(InputStream pInput) throws IOException, PGPException {
		PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(pInput),
				new JcaKeyFingerprintCalculator());

		//
		// we just loop through the collection till we find a key suitable for
		// encryption, in the real
		// world you would probably want to be a bit smarter about this.
		//

		Iterator keyRingIter = pgpPub.getKeyRings();
		while (keyRingIter.hasNext()) {
			PGPPublicKeyRing keyRing = (PGPPublicKeyRing) keyRingIter.next();

			Iterator keyIter = keyRing.getPublicKeys();
			while (keyIter.hasNext()) {
				PGPPublicKey key = (PGPPublicKey) keyIter.next();

				if (key.isEncryptionKey()) {
					return key;
				}
			}
		}

		throw new IllegalArgumentException(TRUProfileSyncConstants.ENCRYPTION_KEY);
	}

	/**
	 * Read secret key.
	 * 
	 * @param pFileName
	 *            the file name
	 * @return the PGP secret key
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws PGPException
	 *             the PGP exception
	 */
	static PGPSecretKey readSecretKey(String pFileName) throws IOException, PGPException {
		InputStream keyIn = new BufferedInputStream(new FileInputStream(pFileName));
		PGPSecretKey secKey = readSecretKey(keyIn);
		keyIn.close();
		return secKey;
	}

	/**
	 * A simple routine that opens a key ring file and loads the first available
	 * key suitable for signature generation.
	 * 
	 * @param pInput
	 *            stream to read the secret key ring collection from.
	 * @return a secret key.
	 * @throws IOException
	 *             on a problem with using the input stream.
	 * @throws PGPException
	 *             if there is an issue parsing the input stream.
	 */
	static PGPSecretKey readSecretKey(InputStream pInput) throws IOException, PGPException {
		PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(pInput),
				new JcaKeyFingerprintCalculator());

		//
		// we just loop through the collection till we find a key suitable for
		// encryption, in the real
		// world you would probably want to be a bit smarter about this.
		//

		Iterator keyRingIter = pgpSec.getKeyRings();
		while (keyRingIter.hasNext()) {
			PGPSecretKeyRing keyRing = (PGPSecretKeyRing) keyRingIter.next();

			Iterator keyIter = keyRing.getSecretKeys();
			while (keyIter.hasNext()) {
				PGPSecretKey key = (PGPSecretKey) keyIter.next();

				if (key.isSigningKey()) {
					return key;
				}
			}
		}

		throw new IllegalArgumentException(TRUProfileSyncConstants.SIGNING_KEY);
	}

}