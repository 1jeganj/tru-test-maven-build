package com.tru.jda.profilesync;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.bouncycastle.openpgp.PGPException;
import org.xml.sax.SAXException;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.jdbc.WatcherDataSource;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import com.tru.common.TRUConstants;
import com.tru.feedprocessor.tol.FeedConstants;
import com.tru.jda.profilesync.outbound.Data;
import com.tru.jda.profilesync.outbound.ObjectFactory;

/**
 * The Class TRUProfileSyncManager.
 */
public class TRUProfileSyncManager extends GenericService {
	
	/** The m profile sync tools. */
	private TRUProfileSyncTools mProfileSyncTools;
	
	/** The m profile sync configuration. */
	private TRUProfileSyncConfiguration mProfileSyncConfiguration;
	/** The m email configuration. */
	private TRUProfileSyncEmailConfiguration mEmailConfiguration;
	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;
	
	/** The m schema xsd uri. */
	private File mSchemaXsdUri;
	
	/** The m xml schema ns uri. */
	private String mXmlSchemaNsURI;

	/** The m file name prefix. */
	private  String mFileNamePrefix;
	
	/** The m file name suffix. */
	private  int mFileNameSuffix;
	
	/** The file name range. */
	private  int mFileNameRange;
	
	/** The m data source. */
	private WatcherDataSource mDataSource=null;
	
	/** The m sql dual statement. */
	private String mSqlDualStatement;
	
	
	/**
	 * Gets the sql dual statement.
	 *
	 * @return the sql dual statement
	 */
	public String getSqlDualStatement() {
		return mSqlDualStatement;
	}

	/**
	 * Sets the sql dual statement.
	 *
	 * @param pSqlDualStatement the new sql dual statement
	 */
	public void setSqlDualStatement(String pSqlDualStatement) {
		 mSqlDualStatement = pSqlDualStatement;
	}



	/**
	 * Gets the data source.
	 *
	 * @return the data source
	 */
	public WatcherDataSource getDataSource() {
		return mDataSource;
	}

	/**
	 * Sets the data source.
	 *
	 * @param pDataSource the new data source
	 */
	public void setDataSource(WatcherDataSource pDataSource) {
		 mDataSource = pDataSource;
	}

	/**
	 * Generate profile data and move to specified location.
	 */
	public void generateProfileDataAndMoveToSpecifiedLocation() {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.generateProfileDataAndMoveToSpecifiedLocation() method");
		}
		File file=null;
		File outFile=null;
		try {
			List<RepositoryItem> profileItems=getProfileSyncTools().queryUpdatedProfileList(getProfileSyncConfiguration().getTimeForOutBoundInMinutes());
			if(null!=profileItems && !profileItems.isEmpty()){
				Data data=createXMLObject(profileItems);
				if(null!=data){
					if(isLoggingDebug()){
						vlogDebug("Last updated profiles in XML format  : {0}", data.toString());
						}
					JAXBContext jaxbContext=JAXBContext.newInstance(data.getClass().getPackage().getName());
					Marshaller marshaller = jaxbContext.createMarshaller();
					marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
					setFileNameSuffix(getFileSuffixString(Boolean.TRUE));
					file = new File(getOutBoundFilePath(getProfileSyncConfiguration().getOutBoundFilePath(),TRUProfileSyncConstants.OUT_BOUND_DECRYPTED_FILE_NAME));
					if(null!=file){
						marshaller.marshal( data, file );
						if(isLoggingDebug()){
							vlogInfo("file before encryption  : {0}", file);
							BufferedReader in = new BufferedReader(new FileReader(file));
							String line;
							while((line = in.readLine()) != null)
							{
							    logDebug(line);
							}
							in.close();
						}
						String outputName=getOutBoundFilePath(getProfileSyncConfiguration().getOutBoundDestination(),getProfileSyncConfiguration().getOutBoundFileName());
						if (!StringUtils.isEmpty(outputName)) {
							outputName = outputName + getProfileSyncConfiguration().getOutBoundGsiProfileSync();
						}
						TRUKeyBasedLargeFileProcessor.encryptFile(outputName, file.getAbsolutePath(), getProfileSyncConfiguration().getPublicKeyFileStore(), true, false);
						outFile = new File(outputName);
						vlogInfo("file after encryption  : {0}", outFile);
						executeUniFtpCommand(outFile.getPath());
						sendProfileSyncMail(profileItems.size(),TRUConstants.ONE,outFile.getName());
						Files.move(FileSystems.getDefault().getPath(outFile.getAbsolutePath()), FileSystems.getDefault().getPath(getProfileSyncConfiguration().getOutBoundDestination()+outFile.getName()));
						vlogInfo("File moved successfully : {0}",  getProfileSyncConfiguration().getOutBoundDestination()+outFile.getName());
						file.delete();
					}
					
				}
			}
		} catch (RepositoryException repoExc) {
			sendProfileSyncMail(TRUConstants.ZERO,TRUConstants.THREE,outFile.getName());
			vlogError("RepositoryException occured while setting the last updated dated to profile with exception : ", repoExc);
		} catch (JAXBException jaxbException) {
			if(null!=file){
				file.delete();
			}
			sendProfileSyncMail(TRUConstants.ZERO,TRUConstants.THREE,outFile.getName());
			vlogError("JAXBException occured while setting the last updated dated to profile with exception : ", jaxbException);
		} catch (IOException ioException) {
			if(null!=file){
				file.delete();
			}
			sendProfileSyncMail(TRUConstants.ZERO,TRUConstants.TWO,outFile.getName());
			vlogError("ioException occured while setting the last updated dated to profile with exception : ", ioException);
		} catch (NoSuchProviderException e) {
			if(null!=file){
				file.delete();
			}
			vlogError("NoSuchProviderException occured while setting the last updated dated to profile with exception : ", e);
		} catch (PGPException e) {
			if(null!=file){
				file.delete();
			}
			sendProfileSyncMail(TRUConstants.ZERO,TRUConstants.THREE,outFile.getName());
			vlogError("PGPException occured while setting the last updated dated to profile with exception : ", e);
		  } catch (InterruptedException interruptedException) {
			if(null!=file){
				file.delete();
			}
			sendProfileSyncMail(TRUConstants.ZERO,TRUConstants.TWO,outFile.getName());
			vlogError("interruptedException occured while setting the last updated dated to profile with exception : ", interruptedException);
		} 
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.generateProfileDataAndMoveToSpecifiedLocation() method");
		}
	}

	/**
	 * Send success mail.
	 *
	 * @param pSsize the ssize
	 * @param pCase the case
	 * @param pName the name
	 */
	public void sendProfileSyncMail(int pSsize, int pCase,String pName) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendProfileSyncMail() method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUProfileSyncConstants.FILE_NAME, pName);
		templateParameters.put(TRUProfileSyncConstants.CASE, pCase);
		switch(pCase){
		case TRUConstants.ONE:
			templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getMessageSubject());
			templateParameters.put(TRUProfileSyncConstants.NO_OFF_ITEMS, pSsize);
			break;
		case TRUConstants.TWO:
			templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getConnectionExceptionSubject());
			break;
		case TRUConstants.THREE:
			templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getFileExceptionSubject());
			break;
		}
		sendProfileSyncOutBondEmail(templateParameters);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendProfileSyncMail() method");
		}
	
		
	}
	
	/**
	 * Send profile sync email.
	 *
	 * @param pParams the params
	 */
	@SuppressWarnings("unchecked")
	private void sendProfileSyncOutBondEmail(Map<String, Object> pParams) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendProfileSyncOutBondEmail() method");
		}
		int emailCase=(int)pParams.get(TRUProfileSyncConstants.CASE);
		TemplateEmailInfoImpl emailInfo=null;
		try {
			if(emailCase>0){
				switch(emailCase){
				case TRUConstants.ONE:
					 emailInfo = getEmailConfiguration().getOutBoundSuccessTemplateEmailInfo();
					 break;
				case TRUConstants.TWO:
					 emailInfo = getEmailConfiguration().getOutBoundErrorTemplateEmailInfo();
					 break;
				case TRUConstants.THREE:
					 emailInfo = getEmailConfiguration().getOutBoundErrorTemplateEmailInfo();
					 break;
				}
			}
			if (emailInfo == null) {
				vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
				return;
			}
			clearEmailInfo(emailInfo);
			// Checking the emailInfo Parameters
			if (emailInfo.getTemplateParameters() != null) {
				emailInfo.getTemplateParameters().putAll(pParams);
			} else {
				emailInfo.setTemplateParameters(pParams);
			}
			emailInfo.setMessageSubject((String) pParams.get(TRUConstants.MESSAGE_SUBJECT));
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendProfileSyncOutBondEmail() method");
		}
	}

	/**
	 * Gets the zero string.
	 *
	 * @param pZeroCount the zero count
	 * @return the zero string
	 */
	private  String getZeroString(int pZeroCount) {
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.getZeroString() method");
		}
		StringBuffer zeroStringBuff=new StringBuffer();
		for(int i=0;i<pZeroCount;i++){
			zeroStringBuff.append(TRUConstants.INT_ZERO);
		}
		String zeroString=zeroStringBuff.toString();
		zeroStringBuff.setLength(TRUConstants.INT_ZERO);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.getZeroString() method");
		}
		return zeroString;
	}
	/**
	 * Clear email info.
	 *
	 * @param pEmailInfo the email info
	 */
	private void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.clearEmailInfo() method");
		}
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.clearEmailInfo() method");
		}
	}
	
	/**
	 * Gets the out bound file path.
	 *
	 * @param pFolderPath the folder path
	 * @param pFilename the filename
	 * @return the out bound file path
	 */
	private String getOutBoundFilePath(String pFolderPath, String pFilename) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.getOutBoundFilePath() method");
		}
		StringBuilder filePath=new StringBuilder();
		String suffix;
		String zeroString=null;
		StringBuffer fileName=new StringBuffer();
		suffix=Integer.toString(getFileNameSuffix());
		//setFileNameSuffix(suffix+TRUConstants._1);
		int zeroCount=getFileNameRange()-suffix.length();
		if(zeroCount>=TRUConstants.INT_ZERO){
			 zeroString=getZeroString(zeroCount);
			 fileName.append(zeroString).append(suffix);
		}else if(zeroCount==TRUConstants.INT_ZERO){
			 fileName.append(suffix);
		}else{
			setFileNameSuffix(TRUConstants.INT_ZERO);
		}
		if(null!=pFilename&&null!=pFolderPath){
			filePath.append(pFolderPath);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			Date date = calendar.getTime();             
			SimpleDateFormat format1 = new SimpleDateFormat(TRUProfileSyncConstants.TIMESTAMP_FORMAT_FOR_EXPORT);
			String date1 = format1.format(date);     
			filePath.append(getProfileSyncConfiguration().getEnvName()).append(TRUProfileSyncConstants.UNDER_SCORE).append(pFilename).append(date1).append(TRUProfileSyncConstants.UNDER_SCORE).append(fileName).append(TRUProfileSyncConstants.XML_EXTENSION);
			fileName.setLength(0);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.getOutBoundFilePath() method");
		}
		return filePath.toString();
	}

	/**
	 * Gets the file suffix string.
	 *
	 *@param pIsGenerate - true/false
	 * @return the file suffix string
	 */
	private Integer getFileSuffixString(boolean pIsGenerate) {
			if (isLoggingDebug()) {
				logDebug("Entering into TRUProfileSyncManager.getFileSuffixString() method");
			}
				Connection conn = null;
				ResultSet rs=null;
				 int resultCode=0;
				try {
					conn = getDataSource().getConnection();
					Statement stmt = conn.createStatement();
					rs=stmt.executeQuery(getSqlDualStatement());
					while (rs.next()) {
						   resultCode = rs.getInt(TRUConstants.NEXTVAL);
					}
					try {
						stmt.close();
					} catch (SQLException sqe) {
						if(isLoggingError()){
							logError("SQLException: ", sqe);
						}
					} finally {
						conn.close();
					}
				} catch (SQLException e) {
					if(isLoggingError()){
						logError("SQL Exception", e);
					}
				} 
		if (isLoggingDebug()) {
			logDebug("Exiting into TRUProfileSyncManager.getFileSuffixString() method");
		}
		return resultCode;
	}

	/**
	 * Creates the xml object.
	 *
	 * @param pProfileItems the profile items
	 * @return the data
	 */
	public Data createXMLObject(List<RepositoryItem> pProfileItems) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.createXMLObject() method");
		}
		ObjectFactory objectFactory=new ObjectFactory();
		Data data=objectFactory.createData();
		TRUProfileSyncTools profileSyncTools=getProfileSyncTools();
		for(RepositoryItem item :pProfileItems){
			Data.Profile profile = objectFactory.createDataProfile();
			Data.Profile.UserInfo UserInfo = objectFactory.createDataProfileUserInfo();
			Data.Profile.UserInfo.Property idProperty=objectFactory.createDataProfileUserInfoProperty();
			idProperty.setName(profileSyncTools.getIdPropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getIdPropertyName())){
				idProperty.setValue((String)item.getPropertyValue(profileSyncTools.getIdPropertyName()));
			}
			else{
				idProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property emailProperty=objectFactory.createDataProfileUserInfoProperty();
			emailProperty.setName(profileSyncTools.getEmailAddressPropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getEmailAddressPropertyName())){
				emailProperty.setValue((String)item.getPropertyValue(profileSyncTools.getEmailAddressPropertyName()));
			}
			else{
				emailProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property firstNameProperty=objectFactory.createDataProfileUserInfoProperty();
			firstNameProperty.setName(profileSyncTools.getFirstNamePropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getFirstNamePropertyName())){
				firstNameProperty.setValue((String)item.getPropertyValue(profileSyncTools.getFirstNamePropertyName()));
			}
			else{
				firstNameProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property lastNameProperty=objectFactory.createDataProfileUserInfoProperty();
			lastNameProperty.setName(profileSyncTools.getLastNamePropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getLastNamePropertyName())){
				lastNameProperty.setValue((String)item.getPropertyValue(profileSyncTools.getLastNamePropertyName()));
			}
			else{
				lastNameProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property passowrdProperty=objectFactory.createDataProfileUserInfoProperty();
			passowrdProperty.setName(profileSyncTools.getPasswordPropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getRadialPasswordPropertyName())){
				passowrdProperty.setValue((String)item.getPropertyValue(profileSyncTools.getRadialPasswordPropertyName()));
			}
			else{
				passowrdProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property rewardNoProperty=objectFactory.createDataProfileUserInfoProperty(); 
			rewardNoProperty.setName(profileSyncTools.getRewardNumberPropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getRewardNoPropertyName())){
				rewardNoProperty.setValue((String)item.getPropertyValue(profileSyncTools.getRewardNoPropertyName()));
			}
			else{
				rewardNoProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property regDateProperty=objectFactory.createDataProfileUserInfoProperty();
			regDateProperty.setName(profileSyncTools.getRegistrationDatePropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getRegistrationDatePropertyName())){
				regDateProperty.setValue(item.getPropertyValue(profileSyncTools.getRegistrationDatePropertyName()).toString());
			}
			else{
				regDateProperty.setValue(null);
			}
			Data.Profile.UserInfo.Property lastProfileUpdateProperty=objectFactory.createDataProfileUserInfoProperty();
			lastProfileUpdateProperty.setName(profileSyncTools.getLastProfileUpdatePropertyName());
			if(null!=item.getPropertyValue(profileSyncTools.getLastProfileUpdatePropertyName())){
				lastProfileUpdateProperty.setValue(item.getPropertyValue(profileSyncTools.getLastProfileUpdatePropertyName()).toString());
			}
			else{
				lastProfileUpdateProperty.setValue(null);
			}
			UserInfo.getProperty().add(idProperty);
			UserInfo.getProperty().add(emailProperty);
			UserInfo.getProperty().add(firstNameProperty);
			UserInfo.getProperty().add(lastNameProperty);
			UserInfo.getProperty().add(passowrdProperty);
			UserInfo.getProperty().add(rewardNoProperty);
			UserInfo.getProperty().add(regDateProperty);
			UserInfo.getProperty().add(lastProfileUpdateProperty);
			profile.setUserInfo(UserInfo);
			data.getProfile().add(profile);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.createXMLObject() method");
		}
		if(null!=data){
			return data;
		}
		return null;
	}
	
	/**
	 * Unmarshall profile data.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void unmarshallProfileData() throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.unmarshallProfileData() method");
		}
		com.tru.jda.profilesync.inbound.ObjectFactory objectFactory = new com.tru.jda.profilesync.inbound.ObjectFactory();
		String currentFileName = null;
		String exceptionMsg=null;
		File decryptFolder = null;
		List<String> processedFiles = new ArrayList<String>();
		List<String> unProcessedFiles = new ArrayList<String>();
			File folder = new File(getProfileSyncConfiguration().getInBoundEncryptedFilePath());
			File[] listOfFiles = folder.listFiles();
			List<File> sortedList=sortFileProcessing(listOfFiles);
			List<File> validFiles=new ArrayList<File>();
			if (null != sortedList) {
				for(File file:sortedList){
				try { 
				currentFileName = file.getName();
				decryptFiles(file);
			    decryptFolder = new File(getProfileSyncConfiguration().getInBoundDecryptedFilePath()+currentFileName);
				File decryptFolderFiles = new File(decryptFolder.toString());
				validFiles=new ArrayList<File>();
				validFiles.add(decryptFolderFiles);
				if(getProfileSyncConfiguration().isValidateXML()){
					validFiles = filterValidFiles(Arrays.asList(decryptFolderFiles));
				}
				else{
					validFiles = Arrays.asList(decryptFolderFiles);
				}
				
				if (isLoggingDebug()) {
								vlogDebug("ProfileSync inbound File Name in unmarshallProfileData method :{0}", decryptFolderFiles.getName());
				}
				currentFileName = null;
				currentFileName = decryptFolderFiles.getName();
				com.tru.jda.profilesync.inbound.Data data = objectFactory.createData();
				JAXBContext jaxbContext = JAXBContext.newInstance(data.getClass().getPackage().getName());
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				data = (com.tru.jda.profilesync.inbound.Data) unmarshaller.unmarshal(decryptFolderFiles);
				getProfileSyncTools().writeProfileDataToATGRepository(data,decryptFolderFiles.getName());
				processedFiles.add(currentFileName);
				if(null!=folder&&folder.isDirectory()&&folder.list().length>TRUConstants.INT_ZERO){
					for(File lfile: folder.listFiles()){
						if (!lfile.isDirectory()) {
							file.delete();
						}
					}
				 }
			}  catch (UnmarshalException unMarshalExc) {
				if(unMarshalExc.getLinkedException().getMessage()!=null){
						exceptionMsg=unMarshalExc.getLinkedException().getMessage();
				}else{
				     	exceptionMsg=TRUProfileSyncConstants.UNMARSHALL_EXCEPTION;
				}
				sendMailForInvalidFiles(Arrays.asList(currentFileName),exceptionMsg);
				unProcessedFiles.add(currentFileName);
				vlogError("UnmarshalException occured while unmarshalling Profile Data : ", unMarshalExc);
			} catch (JAXBException jaxBException) {
				if(jaxBException.getLinkedException().getMessage()!=null){
					exceptionMsg=jaxBException.getLinkedException().getMessage();
				}else{
					exceptionMsg=jaxBException.getMessage();
					}
				sendMailForInvalidFiles(Arrays.asList(currentFileName),exceptionMsg);
				unProcessedFiles.add(currentFileName);
				vlogError("JAXBException occured while unmarshalling Profile Data : ", jaxBException);
			}  catch (ParseException e) {
				sendMailForInvalidFiles(Arrays.asList(currentFileName),e.getMessage());
				unProcessedFiles.add(currentFileName);
				vlogError("ParseException occured while unmarshalling Profile Data : ", e);
			} catch (NoSuchProviderException noSuchProviderException) {
				vlogError("NoSuchProviderException occured while unmarshalling Profile Data : ", noSuchProviderException);
				sendMailForInvalidFiles(Arrays.asList(currentFileName),noSuchProviderException.getMessage());
				unProcessedFiles.add(currentFileName);
			}
			catch (PGPException pgpException) {
				vlogError("PGPException occured while unmarshalling Profile Data : ", pgpException);
				sendMailForInvalidFiles(Arrays.asList(currentFileName),pgpException.getMessage());
				unProcessedFiles.add(currentFileName);
			}catch(EOFException eofException){
				sendMailForInvalidFiles(Arrays.asList(currentFileName),eofException.getMessage());
				unProcessedFiles.add(currentFileName);
				vlogError("EOFException while decrypting file", eofException);
			 }catch (Exception exception) {
				sendMailForInvalidFiles(Arrays.asList(currentFileName),exception.getMessage());
				unProcessedFiles.add(currentFileName);
				vlogError("Exception occured while unmarshalling Profile Data : ", exception);
			 }
		 } 
			moverBoundFilesToSpecifiedLocation(unProcessedFiles,processedFiles);
		}else {
				sendMailForFilesNotFound(getProfileSyncConfiguration().getInBoundEncryptedFilePath());
			}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.unmarshallProfileData() method");
		}
	}
	
	/**
	 * Sort file processing.
	 *
	 * @param pListOfFiles the list of files
	 * @return the list
	 */
	private List<File> sortFileProcessing(File[] pListOfFiles) {
		if (isLoggingDebug()) {
			logDebug("Entering To TRUProfileSyncManager.sortFileProcessing() method");
		}
		File[] lListOfFiles = pListOfFiles;
		Map<String,File> sortedFilMap=new HashMap<String,File>();
		List<File> sortedList=new ArrayList<File>();
		List<String> sequenceList=new ArrayList<String>();
		for(File file:lListOfFiles){
			String fileName=file.getName();
			if(fileName.contains(TRUConstants.CUSTOMER_PROFILE)){
			String[] fileNameArray=fileName.split(TRUConstants.SPLIT_UNDERSCORE);
			String sequenceNum=fileNameArray[TRUConstants.INT_FIVE];
			sortedFilMap.put(sequenceNum, file);
			}
		}
		for (Entry<String, File> entry : sortedFilMap.entrySet()) {
			sequenceList.add(entry.getKey());
		}
		Collections.sort(sequenceList);

		for(String sequencenum:sequenceList){
			sortedList.add(sortedFilMap.get(sequencenum));
			if (isLoggingDebug()) {
				logDebug("sorted file list :" +sortedFilMap.get(sequencenum));
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sortFileProcessing() method");
		}
		return sortedList;
	}

	/**
	 * Decrypt files.
	 *
	 * @param pFile the list of files
	 * @throws NoSuchProviderException the no such provider exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws PGPException the PGP exception
	 * @throws EOFException the EOF exception 
	 */
	public void decryptFiles(File pFile) throws NoSuchProviderException, IOException, PGPException,EOFException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.decryptFiles() method");
		}
			if (isLoggingDebug()) {
				vlogDebug("ProfileSync inbound File Name in decryptFiles nmethod :{0}", pFile.getName());
			}
			if(pFile.isFile()){
				TRUKeyBasedLargeFileProcessor.decryptFile(pFile.getAbsolutePath(),getProfileSyncConfiguration().getSecretKeyFileStore(), getProfileSyncConfiguration().getPassPhrase().toCharArray(),getProfileSyncConfiguration().getInBoundDecryptedFilePath()+pFile.getName());
			}
			
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.decryptFiles() method");
		}
	}

	/**
	 * Send in error files UnProcessed Folder and Send in success files Processed Folder.
	 *
	 * @param pUnProcessedFiles for the unproccessed files and pProcessedFiles for the processed files.
	 * @param pProcessedFiles the processed files
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void moverBoundFilesToSpecifiedLocation(List<String> pUnProcessedFiles, List<String> pProcessedFiles ) throws IOException {
        File folder = new File(getProfileSyncConfiguration().getInBoundDecryptedFilePath());
        File encryptedFolder = new File(getProfileSyncConfiguration().getInBoundEncryptedFilePath());
        List<Path> sourceFiles=new ArrayList<Path>();
        if(!pUnProcessedFiles.isEmpty())
        {
           File unProcessedFolder = new File(getProfileSyncConfiguration().getInBoundUnProccessedFilePath());
           for (Object unProcessedFile : pUnProcessedFiles.toArray())
           {             
             Files.copy(Paths.get(encryptedFolder+TRUProfileSyncConstants.SLASH+unProcessedFile.toString()), Paths.get(unProcessedFolder+TRUProfileSyncConstants.SLASH+unProcessedFile.toString()),StandardCopyOption.REPLACE_EXISTING);
             sourceFiles.add(Paths.get(encryptedFolder+TRUProfileSyncConstants.SLASH+unProcessedFile.toString()));
             vlogInfo("Files moved to unprocessed folder", unProcessedFile.toString());
          }
        }
        if (!pProcessedFiles.isEmpty())
        {
        File processedFolder = new File(getProfileSyncConfiguration().getInBoundProccessedFilePath());
        sendInBoundSuccessMail(pProcessedFiles);
        for (Object processedFile : pProcessedFiles.toArray())
         {
        	Files.copy(Paths.get(folder+TRUProfileSyncConstants.SLASH+processedFile.toString()), Paths.get(processedFolder+TRUProfileSyncConstants.SLASH+processedFile.toString()),StandardCopyOption.REPLACE_EXISTING);
            sourceFiles.add(Paths.get(folder+TRUProfileSyncConstants.SLASH+processedFile.toString()));
         }
        }
        for(Path file: sourceFiles){
            Files.deleteIfExists(file);
         }
	}
	
	/**
	 * Send in bound success mail.
	 *
	 * @param pProcessedFiles the processed files
	 */
	@SuppressWarnings("unchecked")
	private void sendInBoundSuccessMail(List<String> pProcessedFiles) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendInBoundSuccessMail() method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		try {
		templateParameters.put(TRUProfileSyncConstants.PROCESSED_FILES, pProcessedFiles);
		templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getInBoundSuccessSubject());
		TemplateEmailInfoImpl emailInfo=getEmailConfiguration().getInBoundSuccessTemplateEmailInfo();
		if (emailInfo == null) {
			vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
			return;
		}
		clearEmailInfo(emailInfo);
		// Checking the emailInfo Parameters
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(templateParameters);
		} else {
			emailInfo.setTemplateParameters(templateParameters);
		}
		emailInfo.setMessageSubject((String) templateParameters.get(TRUConstants.MESSAGE_SUBJECT));
		
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendInBoundSuccessMail() method");
		}
	}

	/**
	 * Send mail for files not found.
	 *
	 * @param pInBoundDestination the in bound destination
	 */
	@SuppressWarnings("unchecked")
	private void sendMailForFilesNotFound(String pInBoundDestination) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendMailForFilesNotFound() method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		try {
		templateParameters.put(TRUProfileSyncConstants.TYPE, TRUProfileSyncConstants.FILE_NOT_FOUND);
		templateParameters.put(TRUProfileSyncConstants.IN_BOUND_DESTINATION, pInBoundDestination);
		templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getFileNotFoundSubject());
		TemplateEmailInfoImpl emailInfo=getEmailConfiguration().getInBoundErrorTemplateEmailInfo();
		if (emailInfo == null) {
			vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
			return;
		}
		clearEmailInfo(emailInfo);
		// Checking the emailInfo Parameters
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(templateParameters);
		} else {
			emailInfo.setTemplateParameters(templateParameters);
		}
		emailInfo.setMessageSubject((String) templateParameters.get(TRUConstants.MESSAGE_SUBJECT));
		getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
	
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendMailForFilesNotFound() method");
		}
	}

	/**
	 * Filter valid files.
	 *
	 * @param pFiles the files
	 * @return the list
	 */
	private List<File> filterValidFiles(List<File> pFiles) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.filterValidFiles() method");
		}
		List<File> validFiles=new ArrayList<File>();
		List<String> inValidFiles=new ArrayList<String>();
		for(File file:pFiles){
			if (FilenameUtils.getExtension(file.toString()).equals(FeedConstants.XML)) {
				SchemaFactory schemaFactory = null;
				Source xmlFile = new StreamSource(file);
				
				String lXmlSchemaNsURI = getXmlSchemaNsURI();
				try {
					if (!StringUtils.isEmpty(lXmlSchemaNsURI)) {
						schemaFactory = SchemaFactory.newInstance(lXmlSchemaNsURI);
					}
					Schema xsdschema = schemaFactory.newSchema(new File(getXmlSchemaNsURI()));

					Validator validator = xsdschema.newValidator();
					validator.validate(xmlFile);
					validFiles.add(file);
				} catch (SAXException ex) {
					inValidFiles.add(file.getName());
					vlogError("Exception occured while sending a success email : ", ex);
				} catch (IOException ex) {
					inValidFiles.add(file.getName());
					vlogError("Exception occured while sending a success email : ", ex);
				}
			}
		}
		if(!inValidFiles.isEmpty()){
			sendMailForInvalidFiles(inValidFiles);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.filterValidFiles() method");
		}
		return validFiles;
	}
	
	/**
	 * Send mail for invalid files.
	 *
	 * @param pInValidFiles the in valid files
	 */
	@SuppressWarnings("unchecked")
	private void sendMailForInvalidFiles(List<String> pInValidFiles) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendMailForInvalidFiles() method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		try {
		templateParameters.put(TRUProfileSyncConstants.TYPE, TRUProfileSyncConstants.IN_VALID_FILE);
		templateParameters.put(TRUProfileSyncConstants.IN_VALID_FILES, pInValidFiles);
		templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getInvalidFileSubject());
		templateParameters.put(TRUConstants.ERROR_MESSAGE, TRUConstants.EMPTY_STRING);
		if(isLoggingDebug()){
			vlogDebug("Template Email Parameters : {0}", templateParameters);
		}
		TemplateEmailInfoImpl emailInfo=getEmailConfiguration().getInBoundErrorTemplateEmailInfo();
		if (emailInfo == null) {
			vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
			return;
		}
		clearEmailInfo(emailInfo);
		// Checking the emailInfo Parameters
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(templateParameters);
		} else {
			emailInfo.setTemplateParameters(templateParameters);
		}
		emailInfo.setMessageSubject((String) templateParameters.get(TRUConstants.MESSAGE_SUBJECT));
		
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendMailForInvalidFiles() method");
		}
	}
	

	/**
	 * Send mail for invalid files.
	 *@param pException for exception
	 * @param pInValidFiles the in valid files
	 */
	@SuppressWarnings("unchecked")
	private void sendMailForInvalidFiles(List<String> pInValidFiles,String pException) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncManager.sendMailForInvalidFiles(String, String) method");
		}
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		try {
		templateParameters.put(TRUProfileSyncConstants.TYPE, TRUProfileSyncConstants.IN_VALID_FILE);
		templateParameters.put(TRUProfileSyncConstants.IN_VALID_FILES, pInValidFiles);
		templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getInvalidFileSubject());
		templateParameters.put(TRUConstants.ERROR_MESSAGE, pException);
		if(isLoggingDebug()){
			vlogDebug("Template Email Parameters : {0}", templateParameters);
		}
		TemplateEmailInfoImpl emailInfo=getEmailConfiguration().getInBoundErrorTemplateEmailInfo();
		if (emailInfo == null) {
			if(isLoggingDebug()){
			logDebug("emailInfo is NULL or EMPTY. Cannot Send email.");
			}
			return;
		}
		clearEmailInfo(emailInfo);
		// Checking the emailInfo Parameters
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(templateParameters);
		} else {
			emailInfo.setTemplateParameters(templateParameters);
		}
		emailInfo.setMessageSubject((String) templateParameters.get(TRUConstants.MESSAGE_SUBJECT));
		
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
		} catch (TemplateEmailException e) {
			vlogError("Exception occured while sending a success email : ", e);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncManager.sendMailForInvalidFiles(String, String) method");
		}
	}
	
	/**
	 * Execute uni ftp command.
	 *
	 * @param pFilePath the file path
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	private void executeUniFtpCommand(String pFilePath) throws IOException, InterruptedException{
		if (isLoggingDebug()) {
			logDebug("BEGIN:: TRUProfileSyncManager.executeUniFtpCommand() method.. ");
		}
		StringBuffer output = new StringBuffer();
		String line = TRUProfileSyncConstants.EMPTY_STRING;
		String command = getProfileSyncConfiguration().getOutBoundUniFtp() + TRUProfileSyncConstants.EMPTY_SPACE + pFilePath + TRUProfileSyncConstants.EMPTY_SPACE + getProfileSyncConfiguration().getOutBoundHuniftp();
		vlogInfo("Unix Command  : {0}", command);
		Process p;
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader =  new BufferedReader(new InputStreamReader(p.getInputStream()));      
			
			while ((line = reader.readLine())!= null) {
				output.append(line + TRUProfileSyncConstants.NEW_LINE);
			}

		if (isLoggingDebug()) {
			logDebug("END:: TRUProfileSyncManager.executeUniFtpCommand() method.. ");
		}
	}
	/**
	 * Gets the profile sync tools.
	 *
	 * @return the profile sync tools
	 */
	public TRUProfileSyncTools getProfileSyncTools() {
		return mProfileSyncTools;
	}

	/**
	 * Sets the profile sync tools.
	 *
	 * @param pProfileSyncTools the new profile sync tools
	 */
	public void setProfileSyncTools(TRUProfileSyncTools pProfileSyncTools) {
		this.mProfileSyncTools = pProfileSyncTools;
	}

	/**
	 * Gets the profile sync configuration.
	 *
	 * @return the profile sync configuration
	 */
	public TRUProfileSyncConfiguration getProfileSyncConfiguration() {
		return mProfileSyncConfiguration;
	}

	/**
	 * Sets the profile sync configuration.
	 *
	 * @param pProfileSyncConfiguration the new profile sync configuration
	 */
	public void setProfileSyncConfiguration(TRUProfileSyncConfiguration pProfileSyncConfiguration) {
		this.mProfileSyncConfiguration = pProfileSyncConfiguration;
	}


	/**
	 * Gets the email configuration.
	 *
	 * @return the email configuration
	 */
	public TRUProfileSyncEmailConfiguration getEmailConfiguration() {
		return mEmailConfiguration;
	}

	/**
	 * Sets the email configuration.
	 *
	 * @param pEmailConfiguration the new email configuration
	 */
	public void setEmailConfiguration(TRUProfileSyncEmailConfiguration pEmailConfiguration) {
		this.mEmailConfiguration = pEmailConfiguration;
	}
	/**
	 * Gets the template email sender.
	 * 
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the mTemplateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}

	/**
	 * Gets the schema xsd uri.
	 *
	 * @return the schema xsd uri
	 */
	public File getSchemaXsdUri() {
		return mSchemaXsdUri;
	}

	/**
	 * Sets the schema xsd uri.
	 *
	 * @param pSchemaXsdUri the new schema xsd uri
	 */
	public void setSchemaXsdUri(File pSchemaXsdUri) {
		this.mSchemaXsdUri = pSchemaXsdUri;
	}

	/**
	 * Gets the xml schema ns uri.
	 *
	 * @return the xml schema ns uri
	 */
	public String getXmlSchemaNsURI() {
		return mXmlSchemaNsURI;
	}

	/**
	 * Sets the xml schema ns uri.
	 *
	 * @param pXmlSchemaNsURI the new xml schema ns uri
	 */
	public void setXmlSchemaNsURI(String pXmlSchemaNsURI) {
		this.mXmlSchemaNsURI = pXmlSchemaNsURI;
	}

	/**
	 * Gets the file name prefix.
	 *
	 * @return the file name prefix
	 */
	public String getFileNamePrefix() {
		return mFileNamePrefix;
	}

	/**
	 * Sets the file name prefix.
	 *
	 * @param pFileNamePrefix the new file name prefix
	 */
	public void setFileNamePrefix(String pFileNamePrefix) {
		 mFileNamePrefix = pFileNamePrefix;
	}

	/**
	 * Gets the file name suffix.
	 *
	 * @return the file name suffix
	 */
	public int getFileNameSuffix() {
		return mFileNameSuffix;
	}

	/**
	 * Sets the file name suffix.
	 *
	 * @param pFileNameSuffix the new file name suffix
	 */
	public void setFileNameSuffix(int pFileNameSuffix) {
		 mFileNameSuffix = pFileNameSuffix;
	}

	/**
	 * Gets the file name range.
	 *
	 * @return the file name range
	 */
	public int getFileNameRange() {
		return mFileNameRange;
	}

	/**
	 * Sets the file name range.
	 *
	 * @param pFileNameRange the new file name range
	 */
	public void setFileNameRange(int pFileNameRange) {
		mFileNameRange = pFileNameRange;
	}
}