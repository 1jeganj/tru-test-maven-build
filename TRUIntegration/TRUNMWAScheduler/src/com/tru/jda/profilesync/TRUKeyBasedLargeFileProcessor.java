package com.tru.jda.profilesync;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Iterator;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import org.bouncycastle.util.io.Streams;

import com.tru.common.TRUConstants;

/**
 * A simple utility class that encrypts/decrypts public key based encryption
 * large files.
 * <p>
 * To encrypt a file: KeyBasedLargeFileProcessor -e [-a|-ai] fileName
 * publicKeyFile.<br>
 * If -a is specified the output file will be "ascii-armored". If -i is
 * specified the output file will be have integrity checking added.
 * <p>
 * To decrypt: KeyBasedLargeFileProcessor -d fileName secretKeyFile passPhrase.
 * <p>
 * Note 1: this example will silently overwrite files, nor does it pay any
 * attention to the specification of "_CONSOLE" in the filename. It also expects
 * that a single pass phrase will have been used.
 * <p>
 * Note 2: this example generates partial packets to encode the file, the output
 * it generates will not be readable by older PGP products or products that
 * don't support partial packet encoding.
 * <p>
 * Note 3: if an empty file name has been specified in the literal data object
 * contained in the encrypted packet a file with the name filename.out will be
 * generated in the current working directory.
 */
public class TRUKeyBasedLargeFileProcessor {

	/**
	 * Decrypt file.
	 *
	 * @param pInputFileName            the input file name
	 * @param pKeyFileName            the key file name
	 * @param pPasswd            the passwd
	 * @param pDefaultFileName            the default file name
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws NoSuchProviderException             the no such provider exception
	 * @throws PGPException the PGP exception
	 */
	public static void decryptFile(String pInputFileName, String pKeyFileName, char[] pPasswd, String pDefaultFileName)
			throws IOException, NoSuchProviderException, PGPException {
		InputStream in = new BufferedInputStream(new FileInputStream(pInputFileName));
		InputStream keyIn = new BufferedInputStream(new FileInputStream(pKeyFileName));
		decryptFile(in, keyIn, pPasswd, pDefaultFileName);
		keyIn.close();
		in.close();
	}

	/**
	 * decrypt the passed in message stream.
	 *
	 * @param pIn            the in
	 * @param pKeyIn            the key in
	 * @param pPasswd            the passwd
	 * @param pDefaultFileName            the default file name
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws NoSuchProviderException             the no such provider exception
	 * @throws PGPException the PGP exception
	 */
	private static void decryptFile(InputStream pIn, InputStream pKeyIn, char[] pPasswd, String pDefaultFileName)
			throws IOException, NoSuchProviderException, PGPException {
		OutputStream fOut =null;
		InputStream in = null;
		InputStream lIn = pIn;
		InputStream lKeyIn = pKeyIn;
		try{
			
			in = PGPUtil.getDecoderStream(lIn);
			JcaPGPObjectFactory pgpF = new JcaPGPObjectFactory(in);
			PGPEncryptedDataList enc;

			Object o = pgpF.nextObject();
			//
			// the first object might be a PGP marker packet.
			//
			if (o instanceof PGPEncryptedDataList) {
				enc = (PGPEncryptedDataList) o;
			} else {
				enc = (PGPEncryptedDataList) pgpF.nextObject();
			}

			//
			// find the secret key
			//
			Iterator it = enc.getEncryptedDataObjects();
			PGPPrivateKey sKey = null;
			PGPPublicKeyEncryptedData pbe = null;
			PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(lKeyIn),
					new JcaKeyFingerprintCalculator());

			while (sKey == null && it.hasNext()) {
				pbe = (PGPPublicKeyEncryptedData) it.next();

				sKey = TRUPGPUtil.findSecretKey(pgpSec, pbe.getKeyID(), pPasswd);
			}

			if (sKey == null) {
				throw new IllegalArgumentException(TRUProfileSyncConstants.SECRET_KEY_NOT_FOUND);
			}

			InputStream clear = pbe.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider(
					TRUProfileSyncConstants.BC).build(sKey));

			JcaPGPObjectFactory plainFact = new JcaPGPObjectFactory(clear);

			PGPCompressedData cData = (PGPCompressedData) plainFact.nextObject();

			InputStream compressedStream = new BufferedInputStream(cData.getDataStream());
			JcaPGPObjectFactory pgpFact = new JcaPGPObjectFactory(compressedStream);

			Object message = pgpFact.nextObject();

			if (message instanceof PGPLiteralData) {
				PGPLiteralData ld = (PGPLiteralData) message;
				String outFileName = ld.getFileName();
				outFileName = pDefaultFileName;
				if (outFileName.length() == 0) {
					outFileName = pDefaultFileName;
				}
				
				InputStream unc = ld.getInputStream();
				 fOut = new BufferedOutputStream(new FileOutputStream(outFileName));

				Streams.pipeAll(unc, fOut);

			} else if (message instanceof PGPOnePassSignatureList) {
				throw new PGPException(TRUProfileSyncConstants.NO_LITERAL_DATA);
			} else {
				throw new PGPException(TRUProfileSyncConstants.TYPE_UNKNOWN);
			}
		}finally{
			if(fOut!=null){
				in.close();
				in=null;
				fOut.close();
				fOut.flush();
				fOut=null;
				lIn.close();
				lIn=null;
				lKeyIn.close();
				lKeyIn=null;
			}
		}

	}

	/**
	 * Encrypt file.
	 * 
	 * @param pOutputFileName
	 *            the output file name
	 * @param pInputFileName
	 *            the input file name
	 * @param pEncKeyFileName
	 *            the enc key file name
	 * @param pArmor
	 *            the armor
	 * @param pWithIntegrityCheck
	 *            the with integrity check
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws NoSuchProviderException
	 *             the no such provider exception
	 * @throws PGPException
	 *             the PGP exception
	 */
	public static void encryptFile(String pOutputFileName, String pInputFileName, String pEncKeyFileName,
			boolean pArmor, boolean pWithIntegrityCheck) throws IOException, NoSuchProviderException, PGPException {
		PGPPublicKey encKey = TRUPGPUtil.readPublicKey(pEncKeyFileName);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(pOutputFileName));
		encryptFile(out, pInputFileName, encKey, pArmor, pWithIntegrityCheck);
		out.close();
	}

	/**
	 * Encrypt file.
	 * 
	 * @param pOut
	 *            the out
	 * @param pFileName
	 *            the file name
	 * @param pEncKey
	 *            the enc key
	 * @param pArmor
	 *            the armor
	 * @param pWithIntegrityCheck
	 *            the with integrity check
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws NoSuchProviderException
	 *             the no such provider exception
	 */
	private static void encryptFile(OutputStream pOut, String pFileName, PGPPublicKey pEncKey, boolean pArmor,
			boolean pWithIntegrityCheck) throws IOException, NoSuchProviderException {
		OutputStream lOut =pOut;
		if (pArmor) {
			lOut = new ArmoredOutputStream(lOut);
		}

		try {
			PGPEncryptedDataGenerator cPk = new PGPEncryptedDataGenerator(new JcePGPDataEncryptorBuilder(
					PGPEncryptedData.CAST5).setWithIntegrityPacket(pWithIntegrityCheck)
					.setSecureRandom(new SecureRandom()).setProvider(TRUProfileSyncConstants.BC));

			cPk.addMethod(new JcePublicKeyKeyEncryptionMethodGenerator(pEncKey).setProvider(TRUProfileSyncConstants.BC));

			OutputStream cOut = cPk.open(lOut, new byte[TRUConstants.ONE << TRUConstants.SIXTEEN]);

			PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);

			PGPUtil.writeFileToLiteralData(comData.open(cOut), PGPLiteralData.BINARY, new File(pFileName),
					new byte[TRUConstants.ONE << TRUConstants.SIXTEEN]);

			comData.close();

			cOut.close();

			if (pArmor) {
				lOut.close();
			}
		} catch (PGPException e) {
			if (e.getUnderlyingException() != null) {
				e.getUnderlyingException().printStackTrace();
			}
		}
	}
	 
}