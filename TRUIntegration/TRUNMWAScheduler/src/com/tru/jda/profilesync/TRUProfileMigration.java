package com.tru.jda.profilesync;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.feed.nmwa.outbound.TRUNMWASchedulerConstants;

/**
 * The Class TRUProfileMigration.
 */
public class TRUProfileMigration extends GenericService{

	/** The Profile Migration stage main procedure. */
	private String mProfileMigStageMainProcedure;
	
	/** The Profile Migration target procedure. */
	private String mProfileMigTargetProcedure;
	
	/** The Migration decryption directory. */
	private String mMigrationDecryDirectory;
	
	/** The Data source. */
	private DataSource mDataSource;
	
	/** The Migration directory. */
	private String mMigrationDirectory;
	
	/** The Initial. */
	private String mInitial;
	
	/** The Delta. */
	private String mDelta;
	
	/** The NumThreads. */
	private int mNumThreads;
	
	/** The Email Mask */
	private boolean mEmailMask;
	
	/** The initial Flag */
	private boolean mInitialFlag;
	
	
	/** The tempTruncateFlag. */
	private String mTempTruncateFlag;
	
	
	/**
	 * Gets the migration directory.
	 *
	 * @return the tempTruncateFlag
	 */
	public String getTempTruncateFlag() {
		return mTempTruncateFlag;
	}

	/**
	 * Sets the tempTruncateFlag
	 *
	 * @param pTempTruncateFlag the tempTruncateFlag
	 */
	public void setTempTruncateFlag(String pTempTruncateFlag) {
		mTempTruncateFlag = pTempTruncateFlag;
	}
	
	
	/**
	 * Gets the num threads
	 *
	 * @return the num threads
	 */
	public int getNumThreads() {
		return mNumThreads;
	}

	/**
	 * Sets the NumThreads.
	 *
	 * @param pNumThreads the pNumThreads.
	 */
	public void setNumThreads(int pNumThreads) {
		mNumThreads = pNumThreads;
	}

	/**
	 * Checks if is initial flag.
	 *
	 * @return true, if is initial flag
	 */
	public boolean isInitialFlag() {
		return mInitialFlag;
	}

	/**
	 * Sets the initial flag.
	 *
	 * @param pInitialFlag the new initial flag
	 */
	public void setInitialFlag(boolean pInitialFlag) {
		mInitialFlag = pInitialFlag;
	}

	
	/** The Use FileList */
	private boolean mUseFileList;	
	
	/** The Use TargetData */
	private boolean mUseTargetData;
	
	
	
	/** The Data source jndi name. */
	private String mDataSourceJNDIName;

	/** The migration file list. */
	private String mMigrationFileList;
	
	/** The migration decrypt file */
	private TRUProfileMigrationDecryption mMigrationDecrypt;
	    
	
	/**
	 * property to hold encryptFileList.
	 */
	private List<String> mEncryptFileList;
	
	/**
	 * Gets the address encrypt list
	 * 
	 * @return the encryptFileList
	 */
	public List<String> getEncryptFileList() {
		return mEncryptFileList;
	}

	/**
	 * Sets the address encrypt File List.
	 * 
	 * @param pEncryptFileList
	 *            the encryptFileList to set
	 */
	public void setEncryptFileList(List<String> pEncryptFileList) {
			mEncryptFileList = pEncryptFileList;
	}

	/**
	 * Gets the migration decrypt.
	 *
	 * @return the migration decrypt
	 */
	public TRUProfileMigrationDecryption getMigrationDecrypt() {
		return mMigrationDecrypt;
	}

	/**
	 * Sets the migration decrypt
	 *
	 * @param pMigrationDecrypt the migration decrypt
	 */
	public void setMigrationDecrypt(TRUProfileMigrationDecryption pMigrationDecrypt) {
		mMigrationDecrypt = pMigrationDecrypt;
	}

	/**
	 * Gets the migration file list.
	 *
	 * @return the migration file list
	 */
	public String getMigrationFileList() {
		return mMigrationFileList;
	}

	/**
	 * Sets the migration file list.
	 *
	 * @param pMigrationFileList the migration file list.
	 */
	public void setMigrationFileList(String pMigrationFileList) {
		mMigrationFileList = pMigrationFileList;
	}

	
	
	/**
	 * Checks if is target data.
	 *
	 * @return true, if is use target data
	 */
	public boolean isUseTargetData() {
		return mUseTargetData;
	}

	/**
	 * Sets the target data.
	 *
	 * @param pUseTargetData the use target data
	 */
	public void setUseTargetData(boolean pUseTargetData) {
		mUseTargetData = pUseTargetData;
	}


	
	/**
	 * Checks if is email mask.
	 *
	 * @return true, if is email mask
	 */
	public boolean isUseFileList() {
		return mUseFileList;
	}

	/**
	 * Sets the email mask.
	 *
	 * @param pUseFileList the new email mask
	 */
	public void setUseFileList(boolean pUseFileList) {
		mUseFileList = pUseFileList;
	}


	/**
	 * Checks if is email mask.
	 *
	 * @return true, if is email mask
	 */
	public boolean isEmailMask() {
		return mEmailMask;
	}

	/**
	 * Sets the email mask.
	 *
	 * @param pEmailMask the new email mask
	 */
	public void setEmailMask(boolean pEmailMask) {
		mEmailMask = pEmailMask;
	}

	/**
	 * Gets the profile migration stage main procedure.
	 *
	 * @return the profile migration stage main procedure
	 */
	public String getProfileMigStageMainProcedure() {
		return mProfileMigStageMainProcedure;
	}

	/**
	 * Sets the profile migration stage main procedure.
	 *
	 * @param pProfileMigStageMainProcedure the new profile migration stage main procedure
	 */
	public void setProfileMigStageMainProcedure(String pProfileMigStageMainProcedure) {
		mProfileMigStageMainProcedure = pProfileMigStageMainProcedure;
	}

	/**
	 * Gets the profile migration target procedure.
	 *
	 * @return the profile migration target procedure
	 */
	public String getProfileMigTargetProcedure() {
		return mProfileMigTargetProcedure;
	}

	/**
	 * Sets the profile mig target procedure.
	 *
	 * @param pProfileMigTargetProcedure the new profile mig target procedure
	 */
	public void setProfileMigTargetProcedure(String pProfileMigTargetProcedure) {
		mProfileMigTargetProcedure = pProfileMigTargetProcedure;
	}
	
	/**
	 * Gets the data source jndi name.
	 *
	 * @return the data source jndi name
	 */
	public String getDataSourceJNDIName() {
		return mDataSourceJNDIName;
	}

	/**
	 * Sets the data source jndi name.
	 *
	 * @param pDataSourceJNDIName the new data source jndi name
	 */
	public void setDataSourceJNDIName(String pDataSourceJNDIName) {
		mDataSourceJNDIName = pDataSourceJNDIName;
	}
	
	/**
	 * Gets the migration decry directory.
	 *
	 * @return the migration decry directory
	 */
	public String getMigrationDecryDirectory() {
		return mMigrationDecryDirectory;
	}

	/**
	 * Sets the migration decry directory.
	 *
	 * @param pMigrationDecryDirectory the new migration decry directory
	 */
	public void setMigrationDecryDirectory(String pMigrationDecryDirectory) {
		mMigrationDecryDirectory = pMigrationDecryDirectory;
	}

	/**
	 * Gets the migration directory.
	 *
	 * @return the migration directory
	 */
	public String getMigrationDirectory() {
		return mMigrationDirectory;
	}

	/**
	 * Sets the migration directory.
	 *
	 * @param pMigrationDirectory the new migration directory
	 */
	public void setMigrationDirectory(String pMigrationDirectory) {
		mMigrationDirectory = pMigrationDirectory;
	}
	
	/**
	 * Gets the initial.
	 *
	 * @return the initial
	 */
	public String getInitial() {
		return mInitial;
	}

	/**
	 * Sets the initial.
	 *
	 * @param pInitial the new initial
	 */
	public void setInitial(String pInitial) {
		mInitial = pInitial;
	}
	
	/**
	 * Gets the delta.
	 *
	 * @return the delta
	 */
	public String getDelta() {
		return mDelta;
	}

	/**
	 * Sets the delta.
	 *
	 * @param pDelta the new delta
	 */
	public void setDelta(String pDelta) {
		mDelta = pDelta;
	}
	
	/**
	 * Execute profile migration stage main procedure.
	 */
	public void executeProfileMigStageMainProcedure(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.executeProfileMigStageMainProcedure()");
		}
		String fileName = null;				
		if(isUseFileList()){
			fileName = getMigrationFileList();
			if(isLoggingInfo()){
				logInfo("Using file list:"+fileName);
			}
		}else{
			fileName = getProfileMigrationFileName();
			if(isLoggingInfo()){
				logInfo("Using migration files:"+fileName);
			}
		}
		vlogDebug("Profile Migration file Name: {0}", fileName);
		boolean flag = executeStoredProcedure(getProfileMigStageMainProcedure(),getMigrationDirectory(),fileName,getTempTruncateFlag());
		if(flag){
			deleteProfileMigrationFile();
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.executeProfileMigStageMainProcedure()");
		}
	}
	
	/**
	 * Execute profileMigration Multi thread encryption
	 * @throws InterruptedException signals that interruption has been made
	 */
    public void executeProfileMigration() throws InterruptedException{
    if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.executeProfileMigration()");
	 }	
  	  ExecutorService executor = Executors.newFixedThreadPool(getNumThreads());
  	  List<String> inputEncryptList = getEncryptFileList();
  	  if(inputEncryptList.isEmpty()){
  		if(isLoggingInfo()){
  		  logInfo("inputEncryptList is empty");
  		}
  		inputEncryptList = getMigrationDecrypt().encryptMigrationFileList();
  	  }
  	  if(inputEncryptList.size() > 0 ){
  		if(isLoggingInfo()){
  			logInfo("inputEncryptList size -- > " + inputEncryptList.size());
  		}
  		for (Iterator iterator = inputEncryptList.iterator(); iterator.hasNext();) {
				String inputFile = (String) iterator.next();
				if(isLoggingInfo()){
					logInfo("Intiating thread for file : "+inputFile);
				}
				Runnable worker = this.new MigrationThread(inputFile);
                executor.execute(worker);
		} 
  		//logInfo("Thread shutdown initated");
  		 //executor.shutdown();
         // Wait until all threads are finish
  		//logInfo("Thread wait initated");
         //executor.awaitTermination(2,TimeUnit.MINUTES);
  	  }  			  
  	  
  	}  

	/**
	 * Execute migration thread.
	 */
    private class MigrationThread implements Runnable {
    	private final String fileName; 
    	MigrationThread(String fileName) {            
    		this.fileName = fileName;
        }


    	@Override
    	public void run() {
    		if(isLoggingInfo()){
    		logInfo("Running thread for file : "+fileName);
    		}
    		TRUProfileMigration.this.executeDecryptLoadData(fileName);      
        }
  } 
   
    /**
	 * Execute profile migration Initial.
	 * @param pFileName - for file name
	 */
	public void executeDecryptLoadData(String pFileName){
    	//Decrypt call
		//executeStageLoad
		//ExeciteTargetLoad if taget lod flag is enabled.
		if(isLoggingInfo()){
    	logInfo("executeDecryptLoadData -- > " + pFileName);
		}
		getMigrationDecrypt().decryptMigrationFileInput(pFileName);
		boolean flag = executeStoredProcedure(getProfileMigStageMainProcedure(),getMigrationDirectory(),pFileName,TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG);
		if(isLoggingInfo()){
		logInfo("TRUProfileMigration.executeDecryptLoadData() --> Decrypt and stage load completed for file:"+pFileName);
		}
		if(flag){
			deleteProfileMigrationFile(pFileName);
		}		
		if(flag && isUseTargetData()){
			if(isInitialFlag()){
				if(isLoggingInfo()){
				logInfo("TRUProfileMigration.executeDecryptLoadData() --> Target initial load started for file:"+pFileName);
				}
				executeProfileMigInital(pFileName);
				if(isLoggingInfo()){
				logInfo("TRUProfileMigration.executeDecryptLoadData() --> Target initial load completed for file:"+pFileName);
				}
			}else{
				if(isLoggingInfo()){
				logInfo("TRUProfileMigration.executeDecryptLoadData() --> Target delta load started for file:"+pFileName);
				}
				executeProfileMigDelta(pFileName);
				if(isLoggingInfo()){
				logInfo("TRUProfileMigration.executeDecryptLoadData() --> Target delata load completed for file:"+pFileName);
				}
			}
			
		}
    }
    
	/**
	 * Execute profile migration Initial.
	 * @param pFilename - for file name
	 */
	public void executeProfileMigInital(String pFilename){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.executeProfileMigInital()");
		}
		String emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG_N;
		if(isEmailMask()){
			emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG;
		}
		if(isLoggingInfo()){
		logInfo("TRUProfileMigration.executeProfileMigInital() started--> Email mask:"+emailMask+" File name:" +pFilename);
		}
		executeStoredProcedure(getProfileMigTargetProcedure(), getInitial(),emailMask,pFilename);
		if(isLoggingInfo()){
		logInfo("TRUProfileMigration.executeProfileMigInital() completed");
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.executeProfileMigInital()");
		}
	}
	
	/**
	 * Execute profile migration Delta.
	 * @param pFilename - for file name
	 */

	public void executeProfileMigDelta(String pFilename){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.executeProfileMigDelta()");
		}
		String emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG_N;
		if(isEmailMask()){
			emailMask = TRUNMWASchedulerConstants.SUPER_DISPLAY_FLAG;
		}
		if(isLoggingInfo()){
		logInfo("TRUProfileMigration.executeProfileMigDelta() started--> Email mask:"+emailMask+" File name:" +pFilename);
		}

		executeStoredProcedure(getProfileMigTargetProcedure(), getDelta(),emailMask,pFilename);
		if(isLoggingInfo()){
		logInfo("TRUProfileMigration.executeProfileMigDelta() completed");
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.executeProfileMigDelta()");
		}
	}
	
	/**
	 * Execute stored procedure.
	 *
	 * @param pStoredProcedure the stored procedure
	 * @param pParams - String
	 * @return true, if successful
	 */
	private boolean executeStoredProcedure(String pStoredProcedure, String... pParams) {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.executeStoredProcedure()");
		}
		if (this.mDataSource != null) {
			Connection con = null;
			try {
				con = getDatasource().getConnection();
				CallableStatement callableStatement = con.prepareCall(pStoredProcedure);
				if (pParams != null) {
					for (int i = TRUNMWASchedulerConstants.NUMBER_ZERO; i < pParams.length; i++) {
						callableStatement.setString((i + TRUNMWASchedulerConstants.NUMBER_ONE), pParams[i]);
					}
				}
				callableStatement.execute();
				if (isLoggingDebug()) {
					logDebug("Exit from :: TRUProfileMigration.executeStoredProcedure()");
				}
				return true;

			} catch (SQLException exs) {
				if (isLoggingError()) {
					logError("SQLException while executing stored procedure:", exs);
				}
				return false;
			}
		} else {
			vlogError("SQL DataSource : {0} is not configure ", getDataSourceJNDIName());
			if (isLoggingDebug()) {
				logDebug("Exit from :: TRUProfileMigration.executeStoredProcedure()");
			}
			return false;
		}

	}
	
	
	/**
	 * Gets the profile migration file name.
	 *
	 * @return the profile migration file name
	 */
	private String getProfileMigrationFileName(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.getProfileMigrationFileName()");
		}
		int counter = TRUNMWASchedulerConstants.NUMBER_ZERO;
		String fileName = null;
		File folder = new File(getMigrationDecryDirectory());
		File[] listOfFiles = folder.listFiles();
		if(listOfFiles != null && listOfFiles.length >TRUNMWASchedulerConstants.NUMBER_ZERO){
			fileName =	listOfFiles[TRUNMWASchedulerConstants.NUMBER_ZERO].getName();
			for(counter = TRUNMWASchedulerConstants.NUMBER_ONE; counter < listOfFiles.length; counter++){
				fileName =	fileName + TRUNMWASchedulerConstants.COMA_SEPERATOR + listOfFiles[counter].getName();
			}
		}
		if (isLoggingDebug()) {
			logDebug("TRUProfileMigration.getProfileMigrationFileName()--> filename is " + fileName);
			logDebug("Exit from :: TRUProfileMigration.getProfileMigrationFileName()");
		}
		if(isLoggingInfo()){
		logInfo("File names from directory:"+fileName);
		}
		return fileName;
	}
	
	
	/**
	 * Delete profile migration file.
	 *
	 * @return true, if successful
	 */
	private boolean deleteProfileMigrationFile(){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.deleteProfileMigrationFile()");
		}
		boolean flag = false;
		File folder = new File(getMigrationDecryDirectory());
		File[] listOfFiles = folder.listFiles();
		if(listOfFiles != null){
			for(File file : listOfFiles){
				flag = file.delete();
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.deleteProfileMigrationFile()");
		}
		return flag;
	}
	
	/**
	 * Delete profile migration file.
	 *@param pFileName - for file name
	 * @return true, if successful
	 */
	private boolean deleteProfileMigrationFile(String pFileName){
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.deleteProfileMigrationFile()");
		}
		boolean flag = false;
		File decryptedFile = new File(getMigrationDecryDirectory()+pFileName);
		if(decryptedFile.exists()){
			decryptedFile.delete();
			if(isLoggingInfo()){
			logInfo("Deleted decypted file: "+pFileName);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.deleteProfileMigrationFile()");
		}
		return flag;
	}
	
	
	/**
	 * Gets the datasource.
	 *
	 * @return the datasource
	 */
	public DataSource getDatasource() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigration.getDatasource()");
		}
		if (this.mDataSource == null && !StringUtils.isBlank(this.mDataSourceJNDIName)) {
			try {
				Context ctx = new javax.naming.InitialContext();
				this.mDataSource = (DataSource) ctx.lookup(this.mDataSourceJNDIName);
			} catch (NamingException ene) {
				if (isLoggingError()) {
					logError("NamingException while looking JNDI resource:", ene);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigration.getDatasource()");
		}
		return mDataSource;
	}
}
