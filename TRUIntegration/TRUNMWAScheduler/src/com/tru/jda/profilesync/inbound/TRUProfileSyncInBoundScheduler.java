/**
 * 
 */
package com.tru.jda.profilesync.inbound;

import java.io.IOException;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

import com.tru.jda.profilesync.TRUProfileSyncManager;

/**
 * @author PA
 *
 */
public class TRUProfileSyncInBoundScheduler extends SingletonSchedulableService {
	
	/** The m enabled. */
	private boolean mEnabled;
	
	/** The m profile sync manager. */
	private TRUProfileSyncManager mProfileSyncManager;
	
	/**
	 * To generate the data of the profile which is updated in last 15 mins.
	 *
	 * @param pParamScheduler the param scheduler
	 * @param pParamScheduledJob the param scheduled job
	 */
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncInBoundScheduler.doScheduledTask() method");
		}
		if(isEnabled()){
			try {
				WriteProfileDataToRepository();
			} catch (IOException e) {
				vlogError("Exception occured while processing TRUProfileSyncInBoundScheduler.doScheduledTask() method : ", e);
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncInBoundScheduler.doScheduledTask() method");
		}
	}

	/**
	 * Generate profile data.
	 * @throws IOException 
	 */
	public void WriteProfileDataToRepository() throws IOException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncInBoundScheduler.WriteProfileDataToRepository() method");
		}
		getProfileSyncManager().unmarshallProfileData();
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncInBoundScheduler.WriteProfileDataToRepository() method");
		}
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param pEnabled the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		this.mEnabled = pEnabled;
	}

	/**
	 * Gets the profile sync manager.
	 *
	 * @return the profile sync manager
	 */
	public TRUProfileSyncManager getProfileSyncManager() {
		return mProfileSyncManager;
	}

	/**
	 * Sets the profile sync manager.
	 *
	 * @param pProfileSyncManager the new profile sync manager
	 */
	public void setProfileSyncManager(TRUProfileSyncManager pProfileSyncManager) {
		this.mProfileSyncManager = pProfileSyncManager;
	}

}
