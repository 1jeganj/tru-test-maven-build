/*
 * 
 */
package com.tru.jda.profilesync;

import atg.nucleus.GenericService;

/**
 * The Class TRUProfileSyncConfiguration.
 */
public class TRUProfileSyncConfiguration extends GenericService {
	
	/** The m out bound file path. */
	private String mOutBoundFilePath;
	
	/** The m out bound file name. */
	private String mOutBoundFileName;
	
	/**
	 * Holds the destination.
	 */
	private String mOutBoundDestination;
	
	/** The m in bound encrypted file path. */
	private String mInBoundEncryptedFilePath;
	
	/** The m in bound decrypted file path. */
	private String mInBoundDecryptedFilePath;
	
	/** The m in bound processed file path. */
	private String mInBoundProccessedFilePath;
	
	/** The m in bound unprocessed file path. */
	private String mInBoundUnProccessedFilePath;
	
	/** The m in bound file name. */
	private String mInBoundFileName;
	
	/** The m in bound destination. */
	private String mInBoundDestination;
	
	/** The m time for out bound in minutes. */
	private int mTimeForOutBoundInMinutes;
	
	/** The m public key file store. */
	private String mPublicKeyFileStore;
	
	/** The m secret key file store. */
	private String mSecretKeyFileStore;
	
	/** The m pass phrase. */
	private String mPassPhrase;

	/** The m validate xml. */
	private boolean mValidateXML;
	
	/** The m env name. */
	private String mEnvName;
	
	/** The m uniftp name. */
	private String mOutBoundUniFtp;
	
	/** The m huniftp name. */
	private String mOutBoundHuniftp;
	
	/** The m GsiProfileSync name. */
	private String mOutBoundGsiProfileSync;
	
	/** The m error record file name. */
	private String mErrorRecordFileName;
	
	/** The m email mask. */
	private boolean mMaskEmail;
	
	/** The m mask address. */
	private String mMaskAddress;
	/**
	 * Gets the out bound file path.
	 *
	 * @return the out bound file path
	 */
	public String getOutBoundFilePath() {
		return mOutBoundFilePath;
	}
	
	/**
	 * Sets the out bound file path.
	 *
	 * @param pOutBoundFilePath the new out bound file path
	 */
	public void setOutBoundFilePath(String pOutBoundFilePath) {
		this.mOutBoundFilePath = pOutBoundFilePath;
	}
	
	/**
	 * Gets the out bound file name.
	 *
	 * @return the out bound file name
	 */
	public String getOutBoundFileName() {
		return mOutBoundFileName;
	}
	
	/**
	 * Sets the out bound file name.
	 *
	 * @param pOutBoundFileName the new out bound file name
	 */
	public void setOutBoundFileName(String pOutBoundFileName) {
		this.mOutBoundFileName = pOutBoundFileName;
	}

	/**
	 * Gets the out bound destination.
	 *
	 * @return the out bound destination
	 */
	public String getOutBoundDestination() {
		return mOutBoundDestination;
	}

	/**
	 * Sets the out bound destination.
	 *
	 * @param pOutBoundDestination the new out bound destination
	 */
	public void setOutBoundDestination(String pOutBoundDestination) {
		this.mOutBoundDestination = pOutBoundDestination;
	}

	/**
	 * Gets the m in bound file name.
	 *
	 * @return the m in bound file name
	 */
	public String getInBoundFileName() {
		return mInBoundFileName;
	}

	/**
	 * Sets the m in bound file name.
	 *
	 * @param pInBoundFileName the new m in bound file name
	 */
	public void setInBoundFileName(String pInBoundFileName) {
		this.mInBoundFileName = pInBoundFileName;
	}

	/**
	 * Gets the m in bound destination.
	 *
	 * @return the m in bound destination
	 */
	public String getInBoundDestination() {
		return mInBoundDestination;
	}

	/**
	 * Sets the m in bound destination.
	 *
	 * @param pInBoundDestination the new m in bound destination
	 */
	public void setInBoundDestination(String pInBoundDestination) {
		this.mInBoundDestination = pInBoundDestination;
	}

	/**
	 * Gets the time for out bound in minutes.
	 *
	 * @return the time for out bound in minutes
	 */
	public int getTimeForOutBoundInMinutes() {
		return mTimeForOutBoundInMinutes;
	}

	/**
	 * Sets the time for out bound in minutes.
	 *
	 * @param pTimeForOutBoundInMinutes the new time for out bound in minutes
	 */
	public void setTimeForOutBoundInMinutes(int pTimeForOutBoundInMinutes) {
		this.mTimeForOutBoundInMinutes = pTimeForOutBoundInMinutes;
	}

	/**
	 * Gets the m in bound processed file path.
	 *
	 * @return the m in bound processed file path
	 */
	public String getInBoundProccessedFilePath() {
		return mInBoundProccessedFilePath;
	}

	/**
	 * Sets the m in bound file path.
	 *
	 * @param pInBoundProccessedFilePath the new in bound proccessed file path
	 */
	public void setInBoundProccessedFilePath(String pInBoundProccessedFilePath) {
		this.mInBoundProccessedFilePath = pInBoundProccessedFilePath;
	}

	/**
	 * Gets the m in bound unprocessed file path.
	 *
	 * @return the m in bound unprocessed file path
	 */
	public String getInBoundUnProccessedFilePath() {
		return mInBoundUnProccessedFilePath;
	}

	/**
	 * Sets the m in bound file path.
	 *
	 * @param pInBoundUnProccessedFilePath the new in bound un proccessed file path
	 */
	public void setInBoundUnProccessedFilePath(String pInBoundUnProccessedFilePath) {
		this.mInBoundUnProccessedFilePath = pInBoundUnProccessedFilePath;
	}

	/**
	 * Gets the public key file store.
	 *
	 * @return the public key file store
	 */
	public String getPublicKeyFileStore() {
		return mPublicKeyFileStore;
	}

	/**
	 * Sets the public key file store.
	 *
	 * @param pPublicKeyFileStore the new public key file store
	 */
	public void setPublicKeyFileStore(String pPublicKeyFileStore) {
		this.mPublicKeyFileStore = pPublicKeyFileStore;
	}

	/**
	 * Gets the secret key file store.
	 *
	 * @return the secret key file store
	 */
	public String getSecretKeyFileStore() {
		return mSecretKeyFileStore;
	}

	/**
	 * Sets the secret key file store.
	 *
	 * @param pSecretKeyFileStore the new secret key file store
	 */
	public void setSecretKeyFileStore(String pSecretKeyFileStore) {
		this.mSecretKeyFileStore = pSecretKeyFileStore;
	}

	/**
	 * Gets the pass phrase.
	 *
	 * @return the pass phrase
	 */
	public String getPassPhrase() {
		return mPassPhrase;
	}

	/**
	 * Sets the pass phrase.
	 *
	 * @param pPassPhrase the new pass phrase
	 */
	public void setPassPhrase(String pPassPhrase) {
		this.mPassPhrase = pPassPhrase;
	}

	/**
	 * Gets the in bound encrypted file path.
	 *
	 * @return the in bound encrypted file path
	 */
	public String getInBoundEncryptedFilePath() {
		return mInBoundEncryptedFilePath;
	}

	/**
	 * Sets the in bound encrypted file path.
	 *
	 * @param pInBoundEncryptedFilePath the new in bound encrypted file path
	 */
	public void setInBoundEncryptedFilePath(String pInBoundEncryptedFilePath) {
		this.mInBoundEncryptedFilePath = pInBoundEncryptedFilePath;
	}

	/**
	 * Gets the in bound decrypted file path.
	 *
	 * @return the in bound decrypted file path
	 */
	public String getInBoundDecryptedFilePath() {
		return mInBoundDecryptedFilePath;
	}

	/**
	 * Sets the in bound decrypted file path.
	 *
	 * @param pInBoundDecryptedFilePath the new in bound decrypted file path
	 */
	public void setInBoundDecryptedFilePath(String pInBoundDecryptedFilePath) {
		this.mInBoundDecryptedFilePath = pInBoundDecryptedFilePath;
	}
	/**
	 * Checks if is validate xml.
	 *
	 * @return true, if is validate xml
	 */
	public boolean isValidateXML() {
		return mValidateXML;
	}

	/**
	 * Sets the validate xml.
	 *
	 * @param pValidateXML the new validate xml
	 */
	public void setValidateXML(boolean pValidateXML) {
		this.mValidateXML = pValidateXML;
	}

	/**
	 * Gets the OutBoundUniFtp name.
	 *
	 * @return the OutBoundUniFtp name
	 */
	public String getOutBoundUniFtp() {
		return mOutBoundUniFtp;
	}

	/**
	 * Sets the OutBoundUniFtp name.
	 *
	 * @param pOutBoundUniFtp the new OutBoundUniFtp name
	 */
	public void setOutBoundUniFtp(String pOutBoundUniFtp) {
		this.mOutBoundUniFtp = pOutBoundUniFtp;
	}
	
	/**
	 * Gets the OutBoundHuniftp name.
	 *
	 * @return the OutBoundHuniftp name
	 */
	public String getOutBoundHuniftp() {
		return mOutBoundHuniftp;
	}

	/**
	 * Sets the OutBoundHuniftp name.
	 *
	 * @param pOutBoundHuniftp the new OutBoundHuniftp name
	 */
	public void setOutBoundHuniftp(String pOutBoundHuniftp) {
		this.mOutBoundHuniftp = pOutBoundHuniftp;
	}
	
	/**
	 * Gets the OutBoundGsiProfileSync name.
	 *
	 * @return the OutBoundGsiProfileSync name
	 */
	public String getOutBoundGsiProfileSync() {
		return mOutBoundGsiProfileSync;
	}

	/**
	 * Sets the OutBoundGsiProfileSync name.
	 *
	 * @param pOutBoundGsiProfileSync the new OutBoundGsiProfileSync name
	 */
	public void setOutBoundGsiProfileSync(String pOutBoundGsiProfileSync) {
		this.mOutBoundGsiProfileSync = pOutBoundGsiProfileSync;
	}
	
	/**
	 * Gets the env name.
	 *
	 * @return the env name
	 */
	public String getEnvName() {
		return mEnvName;
	}

	/**
	 * Sets the env name.
	 *
	 * @param pEnvName the new env name
	 */
	public void setEnvName(String pEnvName) {
		this.mEnvName = pEnvName;
	}

	/**
	 * Gets the error record file name.
	 *
	 * @return the error record file name
	 */
	public String getErrorRecordFileName() {
		return mErrorRecordFileName;
	}

	/**
	 * Sets the error record file name.
	 *
	 * @param pErrorRecordFileName the new error record file name
	 */
	public void setErrorRecordFileName(String pErrorRecordFileName) {
		this.mErrorRecordFileName = pErrorRecordFileName;
	}

	/**
	 * Gets the mask address.
	 *
	 * @return the mask address
	 */
	public String getMaskAddress() {
		return mMaskAddress;
	}

	/**
	 * Sets the mask address.
	 *
	 * @param pMaskAddress the new mask address
	 */
	public void setMaskAddress(String pMaskAddress) {
		this.mMaskAddress = pMaskAddress;
	}

	/**
	 * Checks if is mask email.
	 *
	 * @return true, if is mask email
	 */
	public boolean isMaskEmail() {
		return mMaskEmail;
	}

	/**
	 * Sets the mask email.
	 *
	 * @param pMaskEmail the new mask email
	 */
	public void setMaskEmail(boolean pMaskEmail) {
		this.mMaskEmail = pMaskEmail;
	}

	


}
