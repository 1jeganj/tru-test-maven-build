/*
 * 
 */
package com.tru.jda.profilesync;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

import au.com.bytecode.opencsv.CSVWriter;

import com.tru.common.TRUConstants;
import com.tru.jda.profilesync.inbound.Data;
import com.tru.jda.profilesync.inbound.Data.Profile;
import com.tru.jda.profilesync.inbound.Data.Profile.UserInfo;
import com.tru.jda.profilesync.inbound.Data.Profile.UserInfo.Property;
import com.tru.userprofiling.TRUPropertyManager;

/**
 * The Class TRUProfileSyncTools.
 */
public class TRUProfileSyncTools extends GenericService {
	/** The m profile sync configuration. */
	private TRUProfileSyncConfiguration mProfileSyncConfiguration;
	/** The m profile repository. */
	private Repository mProfileRepository;
	
	/** The m profile repository view name. */
	private String mProfileRepositoryViewName;
	
	/** The m password property name. */
	private String mPasswordPropertyName;
	
	/** The m first name property name. */
	private String mFirstNamePropertyName;
    
    /** The m last name property name. */
    private String mLastNamePropertyName;
	
	/** The m email address property name. */
	private String mEmailAddressPropertyName;
	
	/** The m id property name. */
	private String mIdPropertyName;
	
	/** The Registration date property name. */
	private String mRegistrationDatePropertyName;
	
	/** The Reward number property name. */
	private String mRewardNumberPropertyName;
	
	/** The Reward no property name. */
	private String mRewardNoPropertyName;
	
	/** The Last profile update property name. */
	private String mLastProfileUpdatePropertyName;
	
	/** The m login property name. */
	private String mLoginPropertyName;
	
	/** The m password hasher property name. */
	private String mPasswordHasherPropertyName;
	
	/** The m password hasherkey. */
	private String mPasswordHasherkey;
	
	/** The Last radial user status property name. */
	private String mRadialUserPropertyName;
	
	/** The Last radial password property name. */
	private String mRadialPasswordPropertyName;	
	
	/** The m email configuration. */
	private TRUProfileSyncEmailConfiguration mEmailConfiguration;
	
	/** Property to hold mTemplateEmailSender. */
	private TemplateEmailSender mTemplateEmailSender;
	
	/** Holds reference for mOmsErrorRepository. */
	private GSARepository mAuditRepository;
	
	/** mPropertyManager. */
	private TRUPropertyManager mPropertyManager;
	/**
	 * Gets the updated profile list.
	 *
	 * @param pTimeForOutBoundInMinutes the time for out bound in minutes
	 * @return the updated profile list
	 * @throws RepositoryException the repository exception
	 */
	public List<RepositoryItem> queryUpdatedProfileList(int pTimeForOutBoundInMinutes) throws RepositoryException{
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncTools.queryUpdatedProfileList() method");
		}
		Repository profileRep=getProfileRepository();
		RepositoryView profileView=profileRep.getView(getProfileRepositoryViewName());
		QueryBuilder queryBuilder=profileView.getQueryBuilder();
		QueryExpression propertyExpresion=queryBuilder.createPropertyQueryExpression(getLastProfileUpdatePropertyName());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, -pTimeForOutBoundInMinutes);
		QueryExpression constantExpression = queryBuilder.createConstantQueryExpression(calendar.getTime());
		Query dateQuery = queryBuilder.createComparisonQuery(propertyExpresion, constantExpression,
				QueryBuilder.GREATER_THAN_OR_EQUALS);
		RepositoryItem[] items=profileView.executeQuery(dateQuery);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncTools.queryUpdatedProfileList() method");
		}
		if(null!=items && items.length>0){
			return Arrays.asList(items);
		}
		
		return null;
	}
	
	/**
	 * Clear email info.
	 *
	 * @param pEmailInfo the email info
	 */
	public void clearEmailInfo(TemplateEmailInfoImpl pEmailInfo) {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncTools.clearEmailInfo() method");
		}
		pEmailInfo.setTemplateParameters(null);
		pEmailInfo.setMessageAttachments(null);
		pEmailInfo.setMessageSubject(null);
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncTools.clearEmailInfo() method");
		}
	}
	
	
	
	
	/**
	 * Send in bound error mail.
	 *@param pFileName for file name
	 * @param pErrorRecords the error records
	 */
	@SuppressWarnings("unchecked")
	public void sendInBoundErrorMail(Map<String,String> pErrorRecords,String pFileName) {
		vlogDebug("Begin:@Class: TRUProfileSyncTools : @Method: sendInBoundErrorMail()");
		Map<String, Object> templateParameters = new HashMap<String, Object>();
		templateParameters.put(TRUProfileSyncConstants.TYPE, TRUProfileSyncConstants.INVALID_RECORDS);
		templateParameters.put(TRUProfileSyncConstants.FILE_NAME, pFileName);
		templateParameters.put(TRUConstants.MESSAGE_SUBJECT, getEmailConfiguration().getInvalidRecordsSubject());
		TemplateEmailInfoImpl emailInfo=getEmailConfiguration().getInBoundErrorTemplateEmailInfo();
		if (emailInfo == null) {
			vlogError("emailInfo is NULL or EMPTY. Cannot Send email.");
			return;
		}
		clearEmailInfo(emailInfo);
		List<File> attachmentFile = new ArrayList<File>();
		// Checking the emailInfo Parameters
		if (emailInfo.getTemplateParameters() != null) {
			emailInfo.getTemplateParameters().putAll(templateParameters);
		} else {
			emailInfo.setTemplateParameters(templateParameters);
		}
		try {
			attachmentFile.add(createAttachmentFile(pErrorRecords));
			emailInfo.setMessageSubject((String) templateParameters.get(TRUConstants.MESSAGE_SUBJECT));
			if (attachmentFile != null) {
				emailInfo.setMessageAttachments(attachmentFile.toArray(new File[attachmentFile.size()]));
			}
			getTemplateEmailSender().sendEmailMessage(emailInfo, getEmailConfiguration().getRecipients());
			} catch (TemplateEmailException templateEmailException) {
				vlogError("Exception occured while sending a success email : ", templateEmailException);
			}catch (IOException iOException) {
				vlogError("Exception occured while sending a success email : ", iOException);
			}
			vlogDebug("End:@Class: TRUProfileSyncTools : @Method: sendInBoundErrorMail()");
		}
	
	
	/**
	 * Creates the attachment file.
	 *
	 * @param pErrorRecords the error records
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private File createAttachmentFile(Map<String, String> pErrorRecords) throws IOException {
		String csv = getProfileSyncConfiguration().getErrorRecordFileName();
		String[] record = null;
		CSVWriter writer = null;
		writer = new CSVWriter(new FileWriter(csv));
		String header = TRUProfileSyncConstants.PROFILE_ID + TRUConstants.COMMA + TRUProfileSyncConstants.ERROR_MESSAGE;
		record = header.split(TRUConstants.COMMA);
		writer.writeNext(record);
		for (Map.Entry<String, String> entry : pErrorRecords.entrySet()) {
				String source = entry.getKey() + TRUConstants.COMMA + entry.getValue();
				record = source.split(TRUConstants.COMMA);
				writer.writeNext(record);
		}
		writer.close();
		File attachment=new File(csv);
		return attachment;
	}   

	/**
	 * Write profile data to atg repository.
	 *
	 * @param pData the data
	 * @param pFileName for file name
	 * @throws ParseException the parse exception
	 */
	public void writeProfileDataToATGRepository(Data pData, String pFileName) throws  ParseException {
		if (isLoggingDebug()) {
			logDebug("Entering into TRUProfileSyncTools.writeProfileDataToATGRepository() method");
		}
		List<Profile> profiles = pData.getProfile();
		Map<String,String> errorRecords=new HashMap<String, String>();
		MutableRepository mutableRepository = (MutableRepository) getProfileRepository();
		List<UserInfo> errorRecordsList=new ArrayList<UserInfo>();
		for (Profile profile : profiles) {
			MutableRepositoryItem mutableUser =null;
			RepositoryItem repoItem = null;
			MutableRepositoryItem newProfileItem = null;
			UserInfo userInfoTemp=null;
 			try {
			UserInfo userInfo = profile.getUserInfo();
			List<Property> properties = userInfo.getProperty();
			for (Property property : properties) {
					userInfoTemp=userInfo;
					if (property.getName().equalsIgnoreCase(getIdPropertyName())) {
						repoItem = getProfileRepository().getItem(property.getValue(), getProfileRepositoryViewName());
					}
					if (repoItem != null) {
						mutableUser = mutableRepository.getItemForUpdate(repoItem.getRepositoryId(), repoItem
								.getItemDescriptor().getItemDescriptorName());
						if (property.getName().equalsIgnoreCase(getIdPropertyName())
								&&getProfileSyncConfiguration().isMaskEmail()) {
							mutableUser.setPropertyValue(getEmailAddressPropertyName(), StringUtils.toLowerCase(property.getValue()+getProfileSyncConfiguration().getMaskAddress()));
							mutableUser.setPropertyValue(getLoginPropertyName(), StringUtils.toLowerCase(property.getValue()+getProfileSyncConfiguration().getMaskAddress()));
						}
						if (property.getName().equalsIgnoreCase(getEmailAddressPropertyName())
								&&!getProfileSyncConfiguration().isMaskEmail()) {
							mutableUser.setPropertyValue(getEmailAddressPropertyName(),StringUtils.toLowerCase(property.getValue()));
							mutableUser.setPropertyValue(getLoginPropertyName(), StringUtils.toLowerCase(property.getValue()));
						} else if (property.getName().equalsIgnoreCase(getFirstNamePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							mutableUser.setPropertyValue(getFirstNamePropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getLastNamePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							mutableUser.setPropertyValue(getLastNamePropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getLastProfileUpdatePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
							Date date = (Date) formatter.parse(property.getValue());
							mutableUser.setPropertyValue(getLastProfileUpdatePropertyName(), date);
						} else if (property.getName().equalsIgnoreCase(getRegistrationDatePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
							Date date = (Date) formatter.parse(property.getValue());
							mutableUser.setPropertyValue(getRegistrationDatePropertyName(), date);
						} else if (property.getName().equalsIgnoreCase(getRewardNumberPropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							mutableUser.setPropertyValue(getRewardNoPropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getPasswordPropertyName())) {
							mutableUser.setPropertyValue(getPasswordPropertyName(), property.getValue());
							mutableUser.setPropertyValue(getRadialPasswordPropertyName(), property.getValue());
							mutableUser.setPropertyValue(getUserPasswordHasherPropertyName(), getPasswordHasherkey());
						}
						mutableUser.setPropertyValue(getRadialUserPropertyName(), Boolean.TRUE);
					} else {
						if (property.getName().equalsIgnoreCase(getIdPropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							newProfileItem = mutableRepository.createItem(property.getValue(),
									getProfileRepositoryViewName());
							if(getProfileSyncConfiguration().isMaskEmail()){
								newProfileItem.setPropertyValue(getEmailAddressPropertyName(), StringUtils.toLowerCase(property.getValue()+getProfileSyncConfiguration().getMaskAddress()));
								newProfileItem.setPropertyValue(getLoginPropertyName(),  StringUtils.toLowerCase(property.getValue()+getProfileSyncConfiguration().getMaskAddress()));
							}
						} else if (property.getName().equalsIgnoreCase(getEmailAddressPropertyName())
								&& StringUtils.isNotEmpty(property.getValue())
								&&!getProfileSyncConfiguration().isMaskEmail()) {
							newProfileItem.setPropertyValue(getEmailAddressPropertyName(),  StringUtils.toLowerCase(property.getValue()));
							newProfileItem.setPropertyValue(getLoginPropertyName(),  StringUtils.toLowerCase(property.getValue()));
						} else if (property.getName().equalsIgnoreCase(getFirstNamePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							newProfileItem.setPropertyValue(getFirstNamePropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getLastNamePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							newProfileItem.setPropertyValue(getLastNamePropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getLastProfileUpdatePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
							Date date = (Date) formatter.parse(property.getValue());
							newProfileItem.setPropertyValue(getLastProfileUpdatePropertyName(), date);
						} else if (property.getName().equalsIgnoreCase(getRegistrationDatePropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
							Date date = (Date) formatter.parse(property.getValue());
							newProfileItem.setPropertyValue(getRegistrationDatePropertyName(), date);
						} else if (property.getName().equalsIgnoreCase(getRewardNumberPropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							newProfileItem.setPropertyValue(getRewardNoPropertyName(), property.getValue());
						} else if (property.getName().equalsIgnoreCase(getPasswordPropertyName())
								&& StringUtils.isNotEmpty(property.getValue())) {
							newProfileItem.setPropertyValue(getPasswordPropertyName(), property.getValue());
							newProfileItem.setPropertyValue(getRadialPasswordPropertyName(), property.getValue());
							newProfileItem
									.setPropertyValue(getUserPasswordHasherPropertyName(), getPasswordHasherkey());
						}
						newProfileItem.setPropertyValue(getRadialUserPropertyName(), Boolean.TRUE);
					}
			}
			if (mutableUser != null) {
				mutableRepository.updateItem(mutableUser);
			}
			if (newProfileItem != null) {
				mutableRepository.addItem(newProfileItem);
			}
			} catch (RepositoryException repositoryEx) {
				errorRecordsList.add(userInfoTemp);
				if(null!=repoItem){
					errorRecords.put(repoItem.getRepositoryId(), repositoryEx.getMessage());
				}
				else{
					errorRecords.put(newProfileItem.getRepositoryId(), repositoryEx.getMessage());
				}
				vlogError("RepositoryException occured while unmarshalling Profile Data : ", repositoryEx);
			} catch (Exception exception) {
			errorRecordsList.add(userInfoTemp);
			if(null!=repoItem){
				errorRecords.put(repoItem.getRepositoryId(), exception.getMessage());
			}
			else{
				errorRecords.put(newProfileItem.getRepositoryId(), exception.getMessage());
			}
			vlogError("RepositoryException occured while unmarshalling Profile Data : ", exception);
		} 
		}
		if(!errorRecordsList.isEmpty() && errorRecordsList.size()>TRUConstants.INT_ZERO){
			updateErrorProfileSyncData(errorRecordsList,errorRecords,pFileName);
		}
		
		if(null!=errorRecords&&!errorRecords.isEmpty()){
			sendInBoundErrorMail(errorRecords,pFileName);
		}
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncTools.writeProfileDataToATGRepository() method");
		}
	}	


	
	/**
	 * Update error profile sync data.
	 *
	 * @param pErrorRecordsList the error records list
	 * @param pErrorRecords 
	 * @param pFileName 
	 */
	private void updateErrorProfileSyncData(List<UserInfo> pErrorRecordsList, Map<String, String> pErrorRecords, String pFileName) {
		if (isLoggingDebug()) {
			logDebug("Entering to TRUProfileSyncTools.writeErrorProfileSyncData() method");
		}
		try {
		MutableRepositoryItem profileSyncErrorItem = null;
		MutableRepository auditRepository = getAuditRepository();
		String profileId=null;
		if (auditRepository != null) {
			for(UserInfo userInfo:pErrorRecordsList){
				profileSyncErrorItem = auditRepository.createItem(getPropertyManager().getProfileSyncErrorMessagesPropertyName());
				for(Property errorProperty:userInfo.getProperty()){
						if (errorProperty.getName().equalsIgnoreCase(getIdPropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getProfileIdPropertyName(), errorProperty.getValue());
						profileId= errorProperty.getValue();
						} else if (errorProperty.getName().equalsIgnoreCase(getEmailAddressPropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getEmailAddressPropertyName(), errorProperty.getValue());
						} else if (errorProperty.getName().equalsIgnoreCase(getFirstNamePropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getFirstNamePropertyName(), errorProperty.getValue());
						} else if (errorProperty.getName().equalsIgnoreCase(getLastNamePropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getLastNamePropertyName(), errorProperty.getValue());
						} else if (errorProperty.getName().equalsIgnoreCase(getLastProfileUpdatePropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
						Date date = (Date) formatter.parse(errorProperty.getValue());
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getLastProfileUpdatePropertyName(), date);
						} else if (errorProperty.getName().equalsIgnoreCase(getRegistrationDatePropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						DateFormat formatter = new SimpleDateFormat(TRUProfileSyncConstants.DATE_PATTERN);
						Date  date = (Date) formatter.parse(errorProperty.getValue());
						profileSyncErrorItem.setPropertyValue(getRegistrationDatePropertyName(), date);
						} else if (errorProperty.getName().equalsIgnoreCase(getRewardNumberPropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getRewardNumberPropertyName(), errorProperty.getValue());
						} else if (errorProperty.getName().equalsIgnoreCase(getPasswordPropertyName())
							&& StringUtils.isNotEmpty(errorProperty.getValue())) {
						profileSyncErrorItem.setPropertyValue(getPropertyManager().getPasswordPropertyName(), errorProperty.getValue());
						}
						if(pErrorRecords.get(profileId)!=null && profileSyncErrorItem.getPropertyValue(getPropertyManager().getProfileSyncErrorMessagePropertyName())==null ){
							profileSyncErrorItem.setPropertyValue(getPropertyManager().getProfileSyncErrorMessagePropertyName(), pErrorRecords.get(profileId));
						}
						if(profileSyncErrorItem.getPropertyValue(getPropertyManager().getFileNamePropertyName())==null){
							profileSyncErrorItem.setPropertyValue(getPropertyManager().getFileNamePropertyName(),pFileName);
						}
					
						if(isLoggingDebug()){
							vlogDebug("Added Error Item : {0} to profileSync Error repository", profileSyncErrorItem);
						}
					} 
				auditRepository.addItem(profileSyncErrorItem);
				}
			}
		}catch (ParseException parseEx) {
			if(isLoggingError()){
				vlogError(parseEx, "Parse Exception in method updateErrorProfileSyncData");
			}
		} catch (RepositoryException re) {
			if(isLoggingError()){
				vlogError(re, "RepositoryException in method updateErrorProfileSyncData");
			}
		} 
		if (isLoggingDebug()) {
			logDebug("Exiting from TRUProfileSyncTools.writeProfileDataToATGRepository() method");
			}
	}

	/**
	 * Gets the profile repository.
	 *
	 * @return the profile repository
	 */
	public Repository getProfileRepository() {
		return mProfileRepository;
	}



	/**
	 * Sets the profile repository.
	 *
	 * @param pProfileRepository the new profile repository
	 */
	public void setProfileRepository(Repository pProfileRepository) {
		this.mProfileRepository = pProfileRepository;
	}



	/**
	 * Gets the profile repository view name.
	 *
	 * @return the profile repository view name
	 */
	public String getProfileRepositoryViewName() {
		return mProfileRepositoryViewName;
	}



	/**
	 * Sets the profile repository view name.
	 *
	 * @param pProfileRepositoryViewName the new profile repository view name
	 */
	public void setProfileRepositoryViewName(String pProfileRepositoryViewName) {
		this.mProfileRepositoryViewName = pProfileRepositoryViewName;
	}

	/**
	 * Gets the password property name.
	 *
	 * @return the password property name
	 */
	public String getPasswordPropertyName() {
		return mPasswordPropertyName;
	}



	/**
	 * Sets the password property name.
	 *
	 * @param pPasswordPropertyName the new password property name
	 */
	public void setPasswordPropertyName(String pPasswordPropertyName) {
		this.mPasswordPropertyName = pPasswordPropertyName;
	}



	/**
	 * Gets the first name property name.
	 *
	 * @return the first name property name
	 */
	public String getFirstNamePropertyName() {
		return mFirstNamePropertyName;
	}



	/**
	 * Sets the first name property name.
	 *
	 * @param pFirstNamePropertyName the new first name property name
	 */
	public void setFirstNamePropertyName(String pFirstNamePropertyName) {
		this.mFirstNamePropertyName = pFirstNamePropertyName;
	}



	/**
	 * Gets the last name property name.
	 *
	 * @return the last name property name
	 */
	public String getLastNamePropertyName() {
		return mLastNamePropertyName;
	}



	/**
	 * Sets the last name property name.
	 *
	 * @param pLastNamePropertyName the new last name property name
	 */
	public void setLastNamePropertyName(String pLastNamePropertyName) {
		this.mLastNamePropertyName = pLastNamePropertyName;
	}



	/**
	 * Gets the email address property name.
	 *
	 * @return the email address property name
	 */
	public String getEmailAddressPropertyName() {
		return mEmailAddressPropertyName;
	}



	/**
	 * Sets the email address property name.
	 *
	 * @param pEmailAddressPropertyName the new email address property name
	 */
	public void setEmailAddressPropertyName(String pEmailAddressPropertyName) {
		this.mEmailAddressPropertyName = pEmailAddressPropertyName;
	}



	/**
	 * Gets the id property name.
	 *
	 * @return the id property name
	 */
	public String getIdPropertyName() {
		return mIdPropertyName;
	}

	/**
	 * Sets the id property name.
	 *
	 * @param pIdPropertyName the new id property name
	 */
	public void setIdPropertyName(String pIdPropertyName) {
		this.mIdPropertyName = pIdPropertyName;
	}

	/**
	 * Gets the registration date property name.
	 *
	 * @return the registration date property name
	 */
	public String getRegistrationDatePropertyName() {
		return mRegistrationDatePropertyName;
	}

	/**
	 * Sets the registration date property name.
	 *
	 * @param pRegistrationDatePropertyName the new registration date property name
	 */
	public void setRegistrationDatePropertyName(String pRegistrationDatePropertyName) {
		mRegistrationDatePropertyName = pRegistrationDatePropertyName;
	}

	/**
	 * Gets the reward number property name.
	 *
	 * @return the reward number property name
	 */
	public String getRewardNumberPropertyName() {
		return mRewardNumberPropertyName;
	}

	/**
	 * Sets the reward number property name.
	 *
	 * @param pRewardNumberPropertyName the new reward number property name
	 */
	public void setRewardNumberPropertyName(String pRewardNumberPropertyName) {
		mRewardNumberPropertyName = pRewardNumberPropertyName;
	}

	/**
	 * Gets the reward no property name.
	 *
	 * @return the reward no property name
	 */
	public String getRewardNoPropertyName() {
		return mRewardNoPropertyName;
	}

	/**
	 * Sets the reward no property name.
	 *
	 * @param pRewardNoPropertyName the new reward no property name
	 */
	public void setRewardNoPropertyName(String pRewardNoPropertyName) {
		mRewardNoPropertyName = pRewardNoPropertyName;
	}

	/**
	 * Gets the last profile update property name.
	 *
	 * @return the last profile update property name
	 */
	public String getLastProfileUpdatePropertyName() {
		return mLastProfileUpdatePropertyName;
	}

	/**
	 * Sets the last profile update property name.
	 *
	 * @param pLastProfileUpdatePropertyName the new last profile update property name
	 */
	public void setLastProfileUpdatePropertyName(
			String pLastProfileUpdatePropertyName) {
		mLastProfileUpdatePropertyName = pLastProfileUpdatePropertyName;
	}

	/**
	 * Gets the login property name.
	 *
	 * @return the login property name
	 */
	public String getLoginPropertyName() {
		return mLoginPropertyName;
	}

	/**
	 * Sets the login property name.
	 *
	 * @param pLoginPropertyName the new login property name
	 */
	public void setLoginPropertyName(String pLoginPropertyName) {
		this.mLoginPropertyName = pLoginPropertyName;
	}

	/**
	 * Sets the user password hasher property name.
	 *
	 * @param pPasswordHasherPropertyName the new user password hasher property name
	 */
	public void setUserPasswordHasherPropertyName(String pPasswordHasherPropertyName) {
		this.mPasswordHasherPropertyName = pPasswordHasherPropertyName;
	}

	/**
	 * Gets the user password hasher property name.
	 *
	 * @return the user password hasher property name
	 */
	public String getUserPasswordHasherPropertyName() {
		return this.mPasswordHasherPropertyName;
	}

	/**
	 * Gets the password hasherkey.
	 *
	 * @return the password hasherkey
	 */
	public String getPasswordHasherkey() {
		return mPasswordHasherkey;
	}

	/**
	 * Sets the password hasherkey.
	 *
	 * @param pPasswordHasherkey the new password hasherkey
	 */
	public void setPasswordHasherkey(String pPasswordHasherkey) {
		this.mPasswordHasherkey = pPasswordHasherkey;
	}
	/**
	 * Gets the radial user property name.
	 *
	 * @return the radial user property name
	 */
	public String getRadialUserPropertyName() {
		return mRadialUserPropertyName;
	}
	
	/**
	 * Sets the radial user property name.
	 *
	 * @param pRadialUserPropertyName the new radial user property name
	 */
	public void setRadialUserPropertyName(String pRadialUserPropertyName) {
		this.mRadialUserPropertyName = pRadialUserPropertyName;
	}

	/**
	 * Gets the radial password property name.
	 *
	 * @return the radial password property name
	 */
	public String getRadialPasswordPropertyName() {
		return mRadialPasswordPropertyName;
	}

	/**
	 * Sets the radial password property name.
	 *
	 * @param pRadialPasswordPropertyName the new radial password property name
	 */
	public void setRadialPasswordPropertyName(String pRadialPasswordPropertyName) {
		mRadialPasswordPropertyName = pRadialPasswordPropertyName;
	}
	
	/**
	 * Gets the email configuration.
	 *
	 * @return the email configuration
	 */
	public TRUProfileSyncEmailConfiguration getEmailConfiguration() {
		return mEmailConfiguration;
	}

	/**
	 * Sets the email configuration.
	 *
	 * @param pEmailConfiguration the new email configuration
	 */
	public void setEmailConfiguration(TRUProfileSyncEmailConfiguration pEmailConfiguration) {
		this.mEmailConfiguration = pEmailConfiguration;
	}
	
	/**
	 * Gets the template email sender.
	 * 
	 * @return the templateEmailSender
	 */
	public TemplateEmailSender getTemplateEmailSender() {
		return mTemplateEmailSender;
	}

	/**
	 * Sets the template email sender.
	 * 
	 * @param pTemplateEmailSender
	 *            the mTemplateEmailSender to set
	 */
	public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
		mTemplateEmailSender = pTemplateEmailSender;
	}
	/**
	 * Gets the profile sync configuration.
	 *
	 * @return the profile sync configuration
	 */
	public TRUProfileSyncConfiguration getProfileSyncConfiguration() {
		return mProfileSyncConfiguration;
	}

	/**
	 * Sets the profile sync configuration.
	 *
	 * @param pProfileSyncConfiguration the new profile sync configuration
	 */
	public void setProfileSyncConfiguration(TRUProfileSyncConfiguration pProfileSyncConfiguration) {
		this.mProfileSyncConfiguration = pProfileSyncConfiguration;
	}
	
	/**
	 * This method returns the propertyManager value.
	 *
	 * @return the propertyManager
	 */
	public TRUPropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	/**
	 * This method sets the propertyManager with parameter value pPropertyManager.
	 *
	 * @param pPropertyManager the propertyManager to set
	 */
	public void setPropertyManager(TRUPropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

	/**
	 * This method is to get auditRepository
	 *
	 * @return the auditRepository
	 */
	public GSARepository getAuditRepository() {
		return mAuditRepository;
	}

	/**
	 * This method sets auditRepository with pAuditRepository
	 *
	 * @param pAuditRepository the auditRepository to set
	 */
	public void setAuditRepository(GSARepository pAuditRepository) {
		mAuditRepository = pAuditRepository;
	}
}
