package com.tru.jda.profilesync;

import java.util.List;

import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailInfoImpl;

/**
 * The Class TRUProfileSyncEmailConfiguration.
 * @author Professional Access
 * @version 1.0
 */
public class TRUProfileSyncEmailConfiguration extends GenericService {


	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mOutBoundSuccessTemplateEmailInfo;
	/** Property to hold Error Template Email Info. */
	private TemplateEmailInfoImpl mOutBoundErrorTemplateEmailInfo;
	
	/** The m template url. */
	private String mTemplateURL;

	/** Property to hold Recipients. */
	private List<String> mRecipients;

	/** Property to hold mMessageSubject. */
	private String mMessageSubject;
	
	/** The m connection exception subject. */
	private String mConnectionExceptionSubject;
	
	/** The m file exception subject. */
	private String mFileExceptionSubject;
	
	/** The m invalid file subject. */
	private String mInvalidFileSubject;
	
	/** The m file not found subject. */
	private String mFileNotFoundSubject;
	/** Property to hold Success Template Email Info. */
	private TemplateEmailInfoImpl mInBoundSuccessTemplateEmailInfo;
	/** Property to hold Error Template Email Info. */
	private TemplateEmailInfoImpl mInBoundErrorTemplateEmailInfo;
	
	/** The m in bound success subject. */
	private String mInBoundSuccessSubject;
	
	/** The m invalid records subject. */
	private String mInvalidRecordsSubject;
	
	/**
	 * Gets the template url.
	 *
	 * @return the template url
	 */
	public String getTemplateURL() {
		return mTemplateURL;
	}

	/**
	 * Sets the template url.
	 *
	 * @param pTemplateURL the new template url
	 */
	public void setTemplateURL(String pTemplateURL) {
		 mTemplateURL = pTemplateURL;
	}
	/**
	 * Gets the recipients.
	 * 
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return mRecipients;
	}

	/**
	 * Sets the recipients.
	 * 
	 * @param pRecipients
	 *            the mRecipients to set
	 */
	public void setRecipients(List<String> pRecipients) {
		mRecipients = pRecipients;
	}

	/**
	 * Gets the success template email info.
	 * 
	 * @return the successTemplateEmailInfo
	 */
	public TemplateEmailInfoImpl getOutBoundSuccessTemplateEmailInfo() {
		return mOutBoundSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the success template email info.
	 *
	 * @param pOutBoundSuccessTemplateEmailInfo the new out bound success template email info
	 */
	public void setOutBoundSuccessTemplateEmailInfo(TemplateEmailInfoImpl pOutBoundSuccessTemplateEmailInfo) {
		mOutBoundSuccessTemplateEmailInfo = pOutBoundSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the message subject.
	 *
	 * @return the messageSubject
	 */
	public String getMessageSubject() {
		return mMessageSubject;
	}

	/**
	 * Sets the message subject.
	 *
	 * @param pMessageSubject the messageSubject to set
	 */
	public void setMessageSubject(String pMessageSubject) {
		mMessageSubject = pMessageSubject;
	}

	/**
	 * Gets the error template email info.
	 *
	 * @return the error template email info
	 */
	public TemplateEmailInfoImpl getOutBoundErrorTemplateEmailInfo() {
		return mOutBoundErrorTemplateEmailInfo;
	}

	/**
	 * Sets the error template email info.
	 *
	 * @param pOutBoundErrorTemplateEmailInfo the new out bound error template email info
	 */
	public void setOutBoundErrorTemplateEmailInfo(TemplateEmailInfoImpl pOutBoundErrorTemplateEmailInfo) {
		this.mOutBoundErrorTemplateEmailInfo = pOutBoundErrorTemplateEmailInfo;
	}

	/**
	 * Gets the connection exception subject.
	 *
	 * @return the connection exception subject
	 */
	public String getConnectionExceptionSubject() {
		return mConnectionExceptionSubject;
	}

	/**
	 * Sets the connection exception subject.
	 *
	 * @param pConnectionExceptionSubject the new connection exception subject
	 */
	public void setConnectionExceptionSubject(
			String pConnectionExceptionSubject) {
		this.mConnectionExceptionSubject = pConnectionExceptionSubject;
	}

	/**
	 * Gets the file exception subject.
	 *
	 * @return the file exception subject
	 */
	public String getFileExceptionSubject() {
		return mFileExceptionSubject;
	}

	/**
	 * Sets the file exception subject.
	 *
	 * @param pFileExceptionSubject the new file exception subject
	 */
	public void setFileExceptionSubject(String pFileExceptionSubject) {
		this.mFileExceptionSubject = pFileExceptionSubject;
	}

	/**
	 * Gets the invalid file subject.
	 *
	 * @return the invalid file subject
	 */
	public String getInvalidFileSubject() {
		return mInvalidFileSubject;
	}

	/**
	 * Sets the invalid file subject.
	 *
	 * @param pInvalidFileSubject the new invalid file subject
	 */
	public void setInvalidFileSubject(String pInvalidFileSubject) {
		this.mInvalidFileSubject = pInvalidFileSubject;
	}

	/**
	 * Gets the file not found subject.
	 *
	 * @return the file not found subject
	 */
	public String getFileNotFoundSubject() {
		return mFileNotFoundSubject;
	}

	/**
	 * Sets the file not found subject.
	 *
	 * @param pFileNotFoundSubject the new file not found subject
	 */
	public void setFileNotFoundSubject(String pFileNotFoundSubject) {
		this.mFileNotFoundSubject = pFileNotFoundSubject;
	}

	/**
	 * Gets the in bound success template email info.
	 *
	 * @return the in bound success template email info
	 */
	public TemplateEmailInfoImpl getInBoundSuccessTemplateEmailInfo() {
		return mInBoundSuccessTemplateEmailInfo;
	}

	/**
	 * Sets the in bound success template email info.
	 *
	 * @param pInBoundSuccessTemplateEmailInfo the new in bound success template email info
	 */
	public void setInBoundSuccessTemplateEmailInfo(TemplateEmailInfoImpl pInBoundSuccessTemplateEmailInfo) {
		this.mInBoundSuccessTemplateEmailInfo = pInBoundSuccessTemplateEmailInfo;
	}

	/**
	 * Gets the in bound error template email info.
	 *
	 * @return the in bound error template email info
	 */
	public TemplateEmailInfoImpl getInBoundErrorTemplateEmailInfo() {
		return mInBoundErrorTemplateEmailInfo;
	}

	/**
	 * Sets the in bound error template email info.
	 *
	 * @param pInBoundErrorTemplateEmailInfo the new in bound error template email info
	 */
	public void setInBoundErrorTemplateEmailInfo(TemplateEmailInfoImpl pInBoundErrorTemplateEmailInfo) {
		this.mInBoundErrorTemplateEmailInfo = pInBoundErrorTemplateEmailInfo;
	}

	/**
	 * Gets the in bound success subject.
	 *
	 * @return the in bound success subject
	 */
	public String getInBoundSuccessSubject() {
		return mInBoundSuccessSubject;
	}

	/**
	 * Sets the in bound success subject.
	 *
	 * @param pInBoundSuccessSubject the new in bound success subject
	 */
	public void setInBoundSuccessSubject(String pInBoundSuccessSubject) {
		this.mInBoundSuccessSubject = pInBoundSuccessSubject;
	}

	/**
	 * Gets the invalid records subject.
	 *
	 * @return the invalid records subject
	 */
	public String getInvalidRecordsSubject() {
		return mInvalidRecordsSubject;
	}
	/**
	 * Sets the invalid records subject.
	 *
	 * @param pInvalidRecordsSubject the invalid records subject
	 */
	public void setInvalidRecordsSubject(String pInvalidRecordsSubject) {
		this.mInvalidRecordsSubject = pInvalidRecordsSubject;
	}

}
