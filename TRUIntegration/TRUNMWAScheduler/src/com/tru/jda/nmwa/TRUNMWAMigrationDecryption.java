package com.tru.jda.nmwa;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchProviderException;

import org.bouncycastle.openpgp.PGPException;

import atg.core.io.FileUtils;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.tru.jda.profilesync.TRUKeyBasedLargeFileProcessor;

/**
 * The Class TRUNMWAMigrationDecryption.
 */
public class TRUNMWAMigrationDecryption extends GenericService {

	/** The m encrypted file path. */
	private String mEncryptedFilePath;

	/** The m decrypted file path. */
	private String mDecryptedFilePath;

	/** The m secret key file store. */
	private String mSecretKeyFileStore;

	/** The m pass phrase. */
	private String mPassPhrase;

	/** The Processed file path. */
	private String mProcessedFilePath;

	/**
	 * decrypting MigrationFile.
	 */
	public void decryptMigrationFile() {
		if (isLoggingDebug()) {
			logDebug("Entering into :: TRUProfileMigrationDecryption.decryptMigrationFile()");
		}
		if (StringUtils.isNotBlank(getEncryptedFilePath()) && StringUtils.isNotBlank(getDecryptedFilePath())) {
			File folder = new File(getEncryptedFilePath());
			File[] listOfFiles = folder.listFiles();
			if (null != listOfFiles) {
				for (File file : listOfFiles) {
					try {
						if (file.isFile()) {
							TRUKeyBasedLargeFileProcessor.decryptFile(file.getAbsolutePath(), getSecretKeyFileStore(),
									getPassPhrase().toCharArray(), getDecryptedFilePath() + file.getName());
							if (!StringUtils.isBlank(getDecryptedFilePath())
									&& !StringUtils.isBlank(getProcessedFilePath())) {
								FileUtils.copyFile(getDecryptedFilePath() + file.getName(), getProcessedFilePath()
										+ file.getName());
							}
						}
					} catch (NoSuchProviderException e) {
						vlogError("NoSuchProviderException occured while decrypting MigrationFile : ", e);
					} catch (PGPException e) {
						vlogError("PGPException occured while decrypting MigrationFile : ", e);
					} catch (IOException e) {
						vlogError("IOException occured while decrypting MigrationFile : ", e);
					}
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug("Exit from :: TRUProfileMigrationDecryption.decryptMigrationFile()");
		}

	}

	/**
	 * Gets the secret key file store.
	 *
	 * @return the secret key file store
	 */
	public String getSecretKeyFileStore() {
		return mSecretKeyFileStore;
	}

	/**
	 * Sets the secret key file store.
	 *
	 * @param pSecretKeyFileStore
	 *            the new secret key file store
	 */
	public void setSecretKeyFileStore(String pSecretKeyFileStore) {
		this.mSecretKeyFileStore = pSecretKeyFileStore;
	}

	/**
	 * Gets the pass phrase.
	 *
	 * @return the pass phrase
	 */
	public String getPassPhrase() {
		return mPassPhrase;
	}

	/**
	 * Sets the pass phrase.
	 *
	 * @param pPassPhrase
	 *            the new pass phrase
	 */
	public void setPassPhrase(String pPassPhrase) {
		this.mPassPhrase = pPassPhrase;
	}

	/**
	 * Gets the encrypted file path.
	 *
	 * @return the encrypted file path
	 */
	public String getEncryptedFilePath() {
		return mEncryptedFilePath;
	}

	/**
	 * Sets the encrypted file path.
	 *
	 * @param pEncryptedFilePath
	 *            the new encrypted file path
	 */
	public void setEncryptedFilePath(String pEncryptedFilePath) {
		this.mEncryptedFilePath = pEncryptedFilePath;
	}

	/**
	 * Gets the decrypted file path.
	 *
	 * @return the decrypted file path
	 */
	public String getDecryptedFilePath() {
		return mDecryptedFilePath;
	}

	/**
	 * Sets the decrypted file path.
	 *
	 * @param pDecryptedFilePath
	 *            the new decrypted file path
	 */
	public void setDecryptedFilePath(String pDecryptedFilePath) {
		this.mDecryptedFilePath = pDecryptedFilePath;
	}

	/**
	 * Gets the processed file path.
	 *
	 * @return the processed file path
	 */
	public String getProcessedFilePath() {
		return mProcessedFilePath;
	}

	/**
	 * Sets the processed file path.
	 *
	 * @param pProcessedFilePath
	 *            the new processed file path
	 */
	public void setProcessedFilePath(String pProcessedFilePath) {
		mProcessedFilePath = pProcessedFilePath;
	}

}
